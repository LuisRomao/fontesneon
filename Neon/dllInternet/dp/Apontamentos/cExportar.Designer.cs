namespace DP.Apontamentos
{
    partial class cExportar
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(cExportar));
            this.gridControl1 = new DevExpress.XtraGrid.GridControl();
            this.tMPApontamentoDTMBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.gridView1 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colTMPEmpresa = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colTMPRegistro = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colTMPReferencia = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colTMPValor = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colTMPPLP = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colTMPEvento = new DevExpress.XtraGrid.Columns.GridColumn();
            ((System.ComponentModel.ISupportInitialize)(this.BarManager_F)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.StyleController_F)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tMPApontamentoDTMBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).BeginInit();
            this.SuspendLayout();
            // 
            // barComponente_F
            // 
            this.barComponente_F.OptionsBar.AllowQuickCustomization = false;
            this.barComponente_F.OptionsBar.DisableClose = true;
            this.barComponente_F.OptionsBar.DisableCustomization = true;
            this.barComponente_F.OptionsBar.DrawDragBorder = false;
            this.barComponente_F.OptionsBar.UseWholeRow = true;
            // 
            // ToolTipController_F
            // 
            this.ToolTipController_F.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.ToolTipController_F.Appearance.ForeColor = System.Drawing.Color.Black;
            this.ToolTipController_F.Appearance.Options.UseBackColor = true;
            this.ToolTipController_F.Appearance.Options.UseFont = true;
            this.ToolTipController_F.Appearance.Options.UseForeColor = true;
            this.ToolTipController_F.Appearance.Options.UseTextOptions = true;
            this.ToolTipController_F.Appearance.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            // 
            // StyleController_F
            // 
            this.StyleController_F.AppearanceDisabled.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this.StyleController_F.AppearanceDisabled.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this.StyleController_F.AppearanceDisabled.Options.UseBackColor = true;
            this.StyleController_F.AppearanceDisabled.Options.UseForeColor = true;
            this.StyleController_F.AppearanceFocused.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.StyleController_F.AppearanceFocused.Options.UseBackColor = true;
          
            // 
            // gridControl1
            // 
            this.gridControl1.DataSource = this.tMPApontamentoDTMBindingSource;
            this.gridControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridControl1.Location = new System.Drawing.Point(0, 30);
            this.gridControl1.MainView = this.gridView1;
            this.gridControl1.Name = "gridControl1";
            this.gridControl1.Size = new System.Drawing.Size(1230, 544);
            this.gridControl1.TabIndex = 0;
            this.gridControl1.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridView1});
            // 
            // tMPApontamentoDTMBindingSource
            // 
            this.tMPApontamentoDTMBindingSource.DataMember = "TMPApontamentoDTM";
            this.tMPApontamentoDTMBindingSource.DataSource = typeof(DP.impressos.dGravarDTM);
            // 
            // gridView1
            // 
            this.gridView1.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colTMPEmpresa,
            this.colTMPRegistro,
            this.colTMPReferencia,
            this.colTMPValor,
            this.colTMPPLP,
            this.colTMPEvento});
            this.gridView1.CustomizationFormBounds = new System.Drawing.Rectangle(614, 321, 208, 191);
            this.gridView1.GridControl = this.gridControl1;
            this.gridView1.Name = "gridView1";
            this.gridView1.OptionsBehavior.Editable = false;
            this.gridView1.OptionsView.ShowGroupPanel = false;
            // 
            // colTMPEmpresa
            // 
            this.colTMPEmpresa.Caption = "Condomínio";
            this.colTMPEmpresa.FieldName = "TMPEmpresa";
            this.colTMPEmpresa.Name = "colTMPEmpresa";
            this.colTMPEmpresa.Visible = true;
            this.colTMPEmpresa.VisibleIndex = 0;
            // 
            // colTMPRegistro
            // 
            this.colTMPRegistro.Caption = "Registro";
            this.colTMPRegistro.FieldName = "TMPRegistro";
            this.colTMPRegistro.Name = "colTMPRegistro";
            this.colTMPRegistro.Visible = true;
            this.colTMPRegistro.VisibleIndex = 1;
            // 
            // colTMPReferencia
            // 
            this.colTMPReferencia.Caption = "Referência";
            this.colTMPReferencia.FieldName = "TMPReferencia";
            this.colTMPReferencia.Name = "colTMPReferencia";
            this.colTMPReferencia.Visible = true;
            this.colTMPReferencia.VisibleIndex = 3;
            // 
            // colTMPValor
            // 
            this.colTMPValor.Caption = "Valor";
            this.colTMPValor.DisplayFormat.FormatString = "n2";
            this.colTMPValor.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.colTMPValor.FieldName = "TMPValor";
            this.colTMPValor.Name = "colTMPValor";
            this.colTMPValor.Visible = true;
            this.colTMPValor.VisibleIndex = 4;
            // 
            // colTMPPLP
            // 
            this.colTMPPLP.Caption = "TMPPLP";
            this.colTMPPLP.FieldName = "TMPPLP";
            this.colTMPPLP.Name = "colTMPPLP";
            // 
            // colTMPEvento
            // 
            this.colTMPEvento.Caption = "Evento";
            this.colTMPEvento.FieldName = "TMPEvento";
            this.colTMPEvento.Name = "colTMPEvento";
            this.colTMPEvento.Visible = true;
            this.colTMPEvento.VisibleIndex = 2;
            // 
            // cExportar
            // 
            this.Controls.Add(this.gridControl1);
            this.Doc = System.Windows.Forms.DockStyle.Fill;
         
            this.LookAndFeel.UseDefaultLookAndFeel = false;
            this.Name = "cExportar";
            this.Size = new System.Drawing.Size(1230, 574);
            this.Controls.SetChildIndex(this.gridControl1, 0);
            ((System.ComponentModel.ISupportInitialize)(this.BarManager_F)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.StyleController_F)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tMPApontamentoDTMBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraGrid.GridControl gridControl1;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView1;
        private System.Windows.Forms.BindingSource tMPApontamentoDTMBindingSource;
        private DevExpress.XtraGrid.Columns.GridColumn colTMPEmpresa;
        private DevExpress.XtraGrid.Columns.GridColumn colTMPRegistro;
        private DevExpress.XtraGrid.Columns.GridColumn colTMPReferencia;
        private DevExpress.XtraGrid.Columns.GridColumn colTMPValor;
        private DevExpress.XtraGrid.Columns.GridColumn colTMPPLP;
        private DevExpress.XtraGrid.Columns.GridColumn colTMPEvento;
    }
}
