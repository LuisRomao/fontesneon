using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace DP.Apontamentos
{
    /// <summary>
    /// 
    /// </summary>
    public partial class cApontamentoHoras : CompontesBasicos.ComponenteBase
    {
        /// <summary>
        /// 
        /// </summary>
        public cApontamentoHoras()
        {
            InitializeComponent();
            //dPlaPonto.DadosGerais.AddDadosGeraisRow(DateTime.Now);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public override bool CanClose()
        {
            return false;
        }

        /// <summary>
        /// 
        /// </summary>
        public cEmitePlanilha Chamador;

        /// <summary>
        /// 
        /// </summary>
        public impressos.dPonto.PLanilhaPontoRow RowChamadora;

        /// <summary>
        /// 
        /// </summary>
        public impressos.dPlaPonto.DadosGeraisRow LinhaGeral;

        /// <summary>
        /// 
        /// </summary>
        public int Cond;

        /// <summary>
        /// 
        /// </summary>
        public Framework.objetosNeon.Competencia Competencia;

        private void simpleButton1_Click(object sender, EventArgs e)
        {
            Padrao(false);
        }

        /*
        private string NomearquivoXMLVelho {
            get {
                string Diretorio = CompontesBasicos.FormPrincipalBase.strEmProducao ? @"\\neon02\Folha\Virtual\ApontamentoFolha\" : @"c:\lixo\";
                string Nome = Diretorio + CondFolhaX.Value.ToString("000")  + registro.Value.ToString() + ".xml";
               // if (System.IO.Directory.Exists(Nome)) ;
                return Nome;
            }
        }
        */

        private string NomearquivoXML
        {
            get
            {
                string Diretorio = CompontesBasicosProc.FuncoesBasicasProc.EstaEmProducao ? @"\\neon02\Folha\Virtual\ApontamentoFolha\" : @"c:\lixo\";
                //string Nome = Diretorio + "AP" +CondFolha.Value.ToString("000") + registro.Value.ToString() + ".xml";

                return string.Format("{0}AP{3}{1:000}{2}.xml",Diretorio,CondFolha.Text,registro.Value,Competencia.ToStringN());
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="verificaGravado"></param>
        public void Padrao(bool verificaGravado) {
            if (verificaGravado)
            {
                //compatibilidade com arquivos velhos
                int PLP = LinhaGeral.PLR;
                if (System.IO.File.Exists(NomearquivoXML))
                {
                    dPlaPonto.dias.Clear();
                    dPlaPonto.DadosGerais.Clear();
                    dPlaPonto.ReadXml(NomearquivoXML);
                    if (dPlaPonto.DadosGerais.Count > 0)
                    {
                        LinhaGeral = dPlaPonto.DadosGerais[0];
                        dadosGeraisBindingSource.DataSource = LinhaGeral;
                        if (LinhaGeral.IsPLRNull())
                            LinhaGeral.PLR = PLP;
                        return;
                    }
                }

                /*
                if (System.IO.File.Exists(NomearquivoXMLVelho))
                {
                    dPlaPonto.dias.Clear();
                    dPlaPonto.dias.ReadXml(NomearquivoXMLVelho);
                    return;
                }
                */
            };


            DateTime[] par = new DateTime[4];
            DateTime[] Spar = new DateTime[4];
            //if (dPlaPonto.DadosGerais.Rows.Count > 0)
            //{
            par[0] =  LinhaGeral.IsH1Null() ? DateTime.MinValue : LinhaGeral.H1;
            if (!LinhaGeral.IsH2Null() && LinhaGeral.IsH4Null())
            {
                par[1] = LinhaGeral.H2;
                par[2] = DateTime.MinValue;
            }
            else
                if (LinhaGeral.maracarAlmoco)
                {
                    par[1] = LinhaGeral.IsH2Null() ? DateTime.MinValue : LinhaGeral.H2;
                    par[2] = LinhaGeral.IsH3Null() ? DateTime.MinValue : LinhaGeral.H3;
                }
                else
                    par[1] = par[2] = DateTime.MinValue;
            par[3] = LinhaGeral.IsH4Null() ? DateTime.MinValue : LinhaGeral.H4;

            Spar[0] = LinhaGeral.IsSH1Null() ? DateTime.MinValue : LinhaGeral.SH1;
            if (!LinhaGeral.IsSH2Null() && LinhaGeral.IsSH4Null())
            {
                Spar[1] = LinhaGeral.SH2;
                Spar[2] = DateTime.MinValue;
            }
            else
                if (LinhaGeral.maracarAlmoco)
                {
                    Spar[1] = LinhaGeral.IsSH2Null() ? DateTime.MinValue : LinhaGeral.SH2;
                    Spar[2] = LinhaGeral.IsSH3Null() ? DateTime.MinValue : LinhaGeral.SH3;
                }
                else
                    Spar[1] = Spar[2] = DateTime.MinValue;
            Spar[3] = LinhaGeral.IsSH4Null() ? DateTime.MinValue : LinhaGeral.SH4;

            //}
            /*
            else
            {
                par[0] = (H1.EditValue == null) ? DateTime.MinValue : H1.Time;
                if (LinhaGeral.maracarAlmoco)
                {
                    par[1] = (H2.EditValue == null) ? DateTime.MinValue : H2.Time;
                    par[2] = (H3.EditValue == null) ? DateTime.MinValue : H3.Time;
                }
                else
                    par[1] = par[2] = DateTime.MinValue;
                par[3] = (H4.EditValue == null) ? DateTime.MinValue : H4.Time;


                Spar[0] = (SH1.EditValue == null) ? DateTime.MinValue : SH1.Time;
                if (LinhaGeral.maracarAlmoco)
                {
                    Spar[1] = (SH2.EditValue == null) ? DateTime.MinValue : SH2.Time;
                    Spar[2] = (SH3.EditValue == null) ? DateTime.MinValue : SH3.Time;
                }
                else
                    Spar[1] = Spar[2] = DateTime.MinValue;
                Spar[3] = (SH4.EditValue == null) ? DateTime.MinValue : SH4.Time;
            }*/
            //MessageBox.Show("Recuperar");
            
            dPlaPonto.CargaInicial(Competencia, par[0], par[1], par[2], par[3], LinhaGeral.Turno, Spar[0], Spar[1], Spar[2], Spar[3]);
            
            //dPlaPonto.CargaInicial(new Framework.objetosNeon.Competencia(2, 2009), par[0], par[1], par[2], par[3], chTurno.Checked, Spar[0], Spar[1], Spar[2], Spar[3]);
            
            CalculaTotais();
            
        }

        private void simpleButton2_Click(object sender, EventArgs e)
        {
            //bool Tem = false;
            GravaUm(188, LinhaGeral.Ex188, 0);
            GravaUm(173, LinhaGeral.Ex173, 0);
            GravaUm(146, LinhaGeral.Ex146, 0);
            GravaUm(148, LinhaGeral.Ex148, 0);
            GravaUm(422, LinhaGeral.Ex422, 0);
            GravaUm(13, LinhaGeral.Ex013, 0);
            GravaUm(421, LinhaGeral.Ex421, 0);
            GravaUm(180, 0, calc180.Value);
            GravaUm(181, 0, calc181.Value);
            GravaUm(176, 0, calc176.Value);
            GravaUm(543, 0, calc543.Value);
            if (!LinhaGeral.IsNEx1Null())
                GravaUm(LinhaGeral.NEx1, 0, LinhaGeral.Ex1);
            if (!LinhaGeral.IsNEx2Null())
                GravaUm(LinhaGeral.NEx2, 0, LinhaGeral.Ex2);             
            dPlaPonto.WriteXml(NomearquivoXML);
            
            RowChamadora.PLPApontamento_USU = CompontesBasicos.Bancovirtual.UsuarioLogado.UsuarioLogadoSt.USU;
            RowChamadora.PLPDataApontamento = DateTime.Now;
            if (RowChamadora.IsPLPDataRetornoNull())
                    RowChamadora.PLPDataRetorno = DateTime.Now;            
            if (RowChamadora.IsPLPDataRetornoNull())
                RowChamadora.PLPDataRetorno = DateTime.Now;
            impressos.dPonto.PLanilhaPontoTableAdapter.Update(RowChamadora);
            RowChamadora.AcceptChanges();
            if (Chamador != null)
                Chamador.UnRegistraFilhote(this);
            FechaTela(DialogResult.OK);
        }

        private bool GravaUm(int Evento,decimal Referencia,decimal Valor)
        {
            if ((Referencia <= 0) && (Valor <= 0))
                return false;
            DP.impressos.dGravarDTM.TMPApontamentoDTMRow Nova = DP.impressos.dGravarDTM.dGravarDTMSt.TMPApontamentoDTM.NewTMPApontamentoDTMRow();          
            Nova.TMPEmpresa = CondFolha.Text;
            Nova.TMPRegistro = (int)registro.Value;
            Nova.TMPEvento = Evento;
            Nova.TMPReferencia = Referencia;
            Nova.TMPValor = Valor;
            if(!LinhaGeral.IsPLRNull())
                Nova.TMPPLP = LinhaGeral.PLR;
            DP.impressos.dGravarDTM.dGravarDTMSt.TMPApontamentoDTM.AddTMPApontamentoDTMRow(Nova);
            DP.impressos.dGravarDTM.dGravarDTMSt.TMPApontamentoDTMTableAdapter.Update(Nova);
            Nova.AcceptChanges();
            return true;
        }

        bool calculoligado = true;


        private decimal arredonda(double original)
        {
            return arredonda((decimal)original);
        } 

        private decimal arredonda(decimal original) {
            return Math.Round(original * 2) / 2;
        } 

        private void CalculaTotais() {
            dadosGeraisBindingSource.EndEdit();
            int almoco = 0;
            TimeSpan nona = new TimeSpan();
            int adic20 = 0;
            TimeSpan Extra100 = new TimeSpan();
            TimeSpan Extra50 = new TimeSpan();
            //int Injust = 0;
            TimeSpan NTrab = new TimeSpan();
            int falta = 0;
            int util = 0;
            int HorasADCN = 0;
            TimeSpan HorasNoturnasAmanha = new TimeSpan();
            TimeSpan HorasAmanha = new TimeSpan();
            foreach (impressos.dPlaPonto.diasRow drow in dPlaPonto.dias) {
                if (!drow.IsPONEntradaNull())
                {
                    DateTime DiaRef = drow.PONEntrada.Date;
                    if (!drow.IsPONAlmocoNull()) {
                        if (drow.PONAlmoco.Date != DiaRef)
                            drow.PONAlmoco = DiaRef.Add(drow.PONAlmoco.TimeOfDay);
                    }
                    if (!drow.IsPONAlmocoRetNull())
                    {
                        if (drow.PONAlmocoRet.Date != DiaRef)
                            drow.PONAlmocoRet = DiaRef.Add(drow.PONAlmocoRet.TimeOfDay);
                    }
                    if (!drow.IsPONSaidaNull())
                    {
                        if (drow.PONSaida.Date != DiaRef)
                            drow.PONSaida = DiaRef.Add(drow.PONSaida.TimeOfDay);
                    }
                }

                if (drow.IsAbonarNull())
                    drow.Abonar = false;
                if (drow.Abonar) {
                    drow.Almoco = false;
                    drow.Erro = false;
                    drow.SetAdicNotNull();
                    drow.SetExtra100Null();
                    drow.SetExtra50Null();
                    drow.SetExtraEntradaNull();
                    drow.SetExtraSaidaNull();
                    drow.SetFaltaInjNull();
                    drow.SetHNaoTrabNull();
                    drow.SetNonaNull();
                    drow.SetPONAlmocoNull();
                    drow.SetPONAlmocoRetNull();
                    drow.SetPONEntradaNull();
                    drow.SetPONSaidaNull();
                    if(!drow.Folga && !drow.Feriado)
                         util++;
                    continue;
                }

                /////////////////
                if (LinhaGeral.Turno)
                    jornada = new TimeSpan(8, 0, 0);
                    
                else
                {
                    if ((!LinhaGeral.IsH4Null()) && (!LinhaGeral.IsH1Null()))
                    {
                        jornada = LinhaGeral.H4 - LinhaGeral.H1;
                        if ((!LinhaGeral.IsH3Null()) && (!LinhaGeral.IsH2Null()))
                            jornada -= (LinhaGeral.H3 - LinhaGeral.H2);
                    }
                    else {
                        if ((!LinhaGeral.IsH2Null()) && (!LinhaGeral.IsH1Null()))                        
                            jornada = LinhaGeral.H2 - LinhaGeral.H1;                            
                        
                    }
                    Sjornada = jornada;
                    if ((!LinhaGeral.IsSH4Null()) && (!LinhaGeral.IsSH1Null()))
                    {
                        Sjornada = LinhaGeral.SH4 - LinhaGeral.SH1;
                        if ((!LinhaGeral.IsSH3Null()) && (!LinhaGeral.IsSH2Null()))
                            Sjornada -= (LinhaGeral.SH3 - LinhaGeral.SH2);
                    }
                    else {
                        if ((!LinhaGeral.IsSH2Null()) && (!LinhaGeral.IsSH1Null()))
                        {
                            
                            Sjornada = LinhaGeral.SH2.TimeOfDay - LinhaGeral.SH1.TimeOfDay;
                            while(Sjornada.Days < 0)
                                Sjornada = Sjornada.Add(new TimeSpan(1,0,0,0));
                        }           
                    }
                };
                bool naodescontar = false;
                DateTime dini;
                DateTime dfim;
                DateTime dal;
                DateTime dalret;
                bool Vespera = false;
                dini = dfim = dal = dalret = DateTime.Today;
                bool AlmocoApontado;
                bool MeioPeriodo;


                drow.Erro = false;
                drow.SetExtra50Null();
                drow.SetHNaoTrabNull();
                drow.SetAlmocoNull();
                drow.SetAlmocoNull();
                drow.SetNonaNull();
                drow.SetHorasNull();                
                drow.Extra100 = DateTime.Today;
                drow.Extra50 = DateTime.Today;
                //drow.SetExtra100Null();
                drow.FaltaInj = false;
                drow.Erro = false;
                drow.Almoco = false;
                if (drow.IsPONEntradaNull() && drow.IsPONAlmocoNull() && drow.IsPONAlmocoRetNull() && (drow.IsPONSaidaNull()))
                {
                    if (!drow.Feriado && !drow.Folga)
                    {
                        drow.FaltaInj = true;
                        falta++;
                        util++;
                    }
                    HorasNoturnasAmanha = new TimeSpan();
                    HorasAmanha = new TimeSpan();
                    drow.SetExtra100Null();
                    drow.SetExtra50Null();
                    continue;
                }
                AlmocoApontado = (!drow.IsPONAlmocoNull() && !drow.IsPONAlmocoRetNull());
                MeioPeriodo = false;
                if (!drow.IsPONEntradaNull())
                    dini = drow.PONEntrada;
                else
                {
                    if (drow.IsPONAlmocoRetNull())
                        drow.Erro = true;
                    else
                    {
                        AlmocoApontado = false;
                        MeioPeriodo = true;
                        dini = drow.PONAlmocoRet;
                    };
                }
                if (!drow.IsPONSaidaNull())
                    dfim = drow.PONSaida;
                else
                {
                    if (MeioPeriodo)
                        drow.Erro = true;
                    else
                    {
                        if (drow.IsPONAlmocoNull())
                            drow.Erro = true;
                        else
                        {
                            AlmocoApontado = false;
                            MeioPeriodo = true;
                            dfim = drow.PONAlmoco;
                        }
                    }
                }
                if (drow.Erro)
                    return;
                if (AlmocoApontado)
                {
                    dal = drow.PONAlmoco;
                    dalret = drow.PONAlmocoRet;
                    if (dal < dini)
                        dal = dal.AddDays(1);
                    if (dalret < dini)
                        dalret = dalret.AddDays(1);
                    if ((dalret < dal) || (dal < dini))
                        drow.Erro = true;
                }

                if (dfim < dini)
                    dfim = dfim.AddDays(1);

                while(((TimeSpan)(dfim - dini)).Days > 1)
                    dfim = dfim.AddDays(-1);


                if (dfim.TimeOfDay < dini.TimeOfDay)
                {
                    
                    //verificar se � feriado no dia seguinte
                    if (!drow.Folga)
                    {
                        int linhaAtual = dPlaPonto.dias.Rows.IndexOf(drow);
                        if ((linhaAtual + 1) < dPlaPonto.dias.Rows.Count)
                        {
                            DP.impressos.dPlaPonto.diasRow linhaSeguinte = (DP.impressos.dPlaPonto.diasRow)dPlaPonto.dias.Rows[linhaAtual + 1];
                            if (linhaSeguinte.Feriado)
                                Vespera = true;
                        }
                    }
                }
                if ((AlmocoApontado) && (dfim < dalret))
                    drow.Erro = true;

                //correcao da jornada para turno de 12 horas
                if (drow.IsForcarJornadaNull())
                {
                    if (LinhaGeral.Turno)
                    {
                        if (!LinhaGeral.IsH4Null() && !LinhaGeral.IsH1Null())
                        {
                            TimeSpan tempoturno = H4.Time - H1.Time;
                            int horas = tempoturno.Hours;
                            while (horas < 0)
                                horas += 24;
                            if (horas == 12)
                                jornada = new TimeSpan(12, 0, 0);
                        }
                    }
                }
                else
                    jornada = drow.ForcarJornada.TimeOfDay;

                

                DateTime dininot = (((dini.Hour >= 22) || (dini.Hour < 5)) ? dini : (dini.Date.AddHours(22)));
                DateTime dfimnot = (((dfim.Hour < 5) || (dfim.Hour > 22)) ? dfim : (dfim.Date.AddHours(5)));

                if (LinhaGeral.Turno)
                    drow.Almoco = (!AlmocoApontado && !MeioPeriodo);
                else
                    drow.Almoco = (!AlmocoApontado && !MeioPeriodo && !drow.Feriado && !drow.Folga);

                DateTime dfimnodia = (dfim.Date == dini.Date) ? dfim : dfim.Date;
                TimeSpan horasnodia = (dfimnodia - dini);
                //converter ou nao a hora de almo�o
                bool podeconverter = (dfimnodia.Hour > 6) || Vespera;
                if (drow.Feriado && drow.Almoco && podeconverter) {
                    TimeSpan HorasHoje;
                    if (dfim.Date == dini.Date)
                        HorasHoje = (dfim - dini);
                    else
                        HorasHoje = (dfim.Date - dini);
                    HorasHoje = HorasHoje + HorasAmanha;
                    if (HorasHoje.TotalHours >= 7) {
                        drow.Almoco = false;
                        //if (drow.IsExtra100Null())
                        //    drow.Extra100 = DateTime.Today;
                        drow.Extra100 = drow.Extra100.AddHours(1);                        
                    }
                }

                //if (!drow.Folga)
                if (dfimnot > dininot)
                {
                    if (drow.Feriado && podeconverter)
                    {
                        HorasNoturnasAmanha = HorasNoturnasAmanha.Add(dfimnot.Date - dininot);
                        
                        if (HorasNoturnasAmanha.TotalHours >= 7) 
                        {
                            //if (drow.IsExtra100Null())
                            //    drow.Extra100 = DateTime.Today;
                            drow.Extra100 = drow.Extra100.AddHours(1);
                            
                        }
                        else
                            drow.Nona = DateTime.Today + (dfimnot - dininot);
                        
                    }
                    else
                        drow.Nona = DateTime.Today + (dfimnot - dininot);

                    
                }

                if (dfimnot.Date > dini.Date)
                {
                    HorasNoturnasAmanha = dfimnot.TimeOfDay;                    
                }
                else
                    HorasNoturnasAmanha = new TimeSpan();

                if (dfim.Date > dini.Date)
                {
                    HorasAmanha = dfim.TimeOfDay;
                }
                else
                    HorasAmanha = new TimeSpan();

                //Extra 50 / nao trabalhada
                TimeSpan horasEfetivas = (TimeSpan)(dfim - dini);
                if (AlmocoApontado)
                {
                    horasEfetivas -= (TimeSpan)(dalret - dal);
                    if ((LinhaGeral.Turno) && (horasEfetivas.Hours >= 6))

                        horasEfetivas = horasEfetivas.Add(new TimeSpan(1, 0, 0));
                }
                else {
                    if (!LinhaGeral.Turno && drow.Almoco)
                        horasEfetivas = horasEfetivas.Add(new TimeSpan(-1, 0, 0));
                }


                drow.Horas = DateTime.Today + horasEfetivas;

                //Extra 100
                if (drow.Folga)
                {
                    if (!LinhaGeral.Turno)
                        if (horasEfetivas.Hours >= 6)
                            horasEfetivas = horasEfetivas.Add(new TimeSpan(1, 0, 0));
                    //Para s�bados trabalhados � h50 e nao 100
                    if ((!LinhaGeral.Turno) && (drow.semana == "sab"))
                        drow.Extra50 = drow.Extra50 + horasEfetivas;
                    else
                        drow.Extra100 = drow.Extra100 + horasEfetivas;
                    jornada = new TimeSpan(0, 0, 0);
                    Sjornada = jornada;
                    horasEfetivas = new TimeSpan(0, 0, 0);
                    
                }
                else
                {

                    if (drow.Feriado)
                    {
                        if (dfim.Date == dini.Date)
                        {
                            drow.Extra100 = drow.Extra100 + horasEfetivas;
                            jornada = new TimeSpan(0, 0, 0);
                            
                            horasEfetivas = new TimeSpan(0, 0, 0);
                        }
                        else
                        {
                            //if (drow.IsExtra100Null())
                            //    drow.Extra100 = DateTime.Today;
                            drow.Extra100 = drow.Extra100.Add(horasEfetivas - dfim.TimeOfDay);
                            horasEfetivas = dfim.TimeOfDay;

                            jornada = jornada.Subtract(horasEfetivas - dfim.TimeOfDay);
                            naodescontar = true;
                        };
                        Sjornada = jornada;
                    }
                }



                TimeSpan jornadaDia = jornada;

                if (!LinhaGeral.Turno)
                      jornadaDia = (drow.semana == "sab") ? Sjornada : jornada;

                if (horasEfetivas != jornadaDia)
                {
                    if (horasEfetivas > jornadaDia)
                        drow.Extra50 = drow.Extra50 + (horasEfetivas - jornadaDia);
                    else
                        if (!naodescontar)
                            drow.HNaoTrab = DateTime.Today + (jornadaDia - horasEfetivas);
                }
                if (Vespera)
                {
                    TimeSpan converterpara100 = dfim.TimeOfDay;
                    //if (drow.IsExtra100Null())
                    //    drow.Extra100 = DateTime.Today + converterpara100;
                    //else
                    drow.Extra100 += converterpara100;
                    if (!drow.IsExtra50Null())
                    {
                        if (drow.Extra50.TimeOfDay <= converterpara100)
                            drow.Extra50 = DateTime.Today;
                        else
                            drow.Extra50 = drow.Extra50 - converterpara100;
                    }
                };

                if (drow.Folga)
                {
                    if (drow.Almoco)
                    {
                        drow.Almoco = false;
                        //if (drow.IsExtra100Null())
                        //    drow.Extra100 = DateTime.Today;
                        drow.Extra100 = drow.Extra100.AddHours(1);
                    }
                    if (!drow.IsNonaNull() && (drow.Nona.Hour == 7))
                    {
                        drow.SetNonaNull();
                        //if (drow.IsExtra100Null())
                        //    drow.Extra100 = DateTime.Today;
                        drow.Extra100 = drow.Extra100.AddHours(1);
                    }
                }
                /////////////////
                if (!drow.IsAlmocoNull() && drow.Almoco)
                    almoco++;
                if (!drow.IsFaltaInjNull() && drow.FaltaInj)
                   falta++;
                if (!drow.IsNonaNull())
                {
                    nona += drow.Nona.TimeOfDay;
                    if (!drow.Feriado && !drow.Folga)
                         adic20 += drow.Nona.Hour;
                }
                if (!drow.IsExtra100Null())
                    Extra100 += drow.Extra100.TimeOfDay;
                if (!drow.IsExtra50Null())
                    Extra50 += drow.Extra50.TimeOfDay;
                if (!drow.IsHNaoTrabNull())
                    NTrab += drow.HNaoTrab.TimeOfDay;
                if (!drow.Feriado && !drow.Folga)
                    util++;
                if (drow.Extra100 == DateTime.Today)
                    drow.SetExtra100Null();
                if (drow.Extra50 == DateTime.Today)
                    drow.SetExtra50Null();
            };
            if (util > 0)
                HorasADCN = 220 * adic20 / (util * 7);
            
            LinhaGeral.Ex188 = almoco;
            HAlmoco.Value = almoco;
            LinhaGeral.Ex173 = arredonda(nona.TotalHours / 7);
            HNona.Value = arredonda(nona.TotalHours / 7);

            LinhaGeral.Ex013 = HorasADCN;
            HADCN.Value = HorasADCN;
            LinhaGeral.Ex148 = arredonda(Extra100.TotalHours);
            HExtra100.Value = arredonda(Extra100.TotalHours);

            LinhaGeral.Ex146 = arredonda(Extra50.TotalHours);
            HExtra50.Value = arredonda(Extra50.TotalHours);

            LinhaGeral.Ex421 = falta;
            HFalta.Value = falta;

            LinhaGeral.Ex422 = arredonda(NTrab.TotalHours);
            HNTrab.Value = arredonda(NTrab.TotalHours);

            dadosGeraisBindingSource.EndEdit();
            //HExtra100.Value = decimal.Truncate(Extra100 + 0.5m);
            //memoEdit1.Text = "Nona hora : " + nona.ToString("n0") + " -> " + nona.TotalHours.ToString("n4") + " " + arredonda(nona.TotalHours).ToString() + "\r\n";
            //memoEdit1.Text += "Extra 100%: " + Extra100.ToString() + " -> " + Extra100.TotalHours + "\r\n";
            //memoEdit1.Text += "Extra 50%: " + Extra50.ToString() + " -> " + Extra50.TotalHours + "\r\n";

            memoEdit1.Text = "Dias uteis = " + util.ToString() + " = " + (7 * util) + " Horas\r\n";
            decimal por = 100.0M * adic20 / (util * 7M);
            memoEdit1.Text += "Horas not. trabalhadas = " + adic20 + " ("+ por.ToString("n0") +"%)\r\n";
            memoEdit1.Text += "ADCTN = " + HorasADCN.ToString("n0") + "\r\n";  
        }

        private void bandedGridView1_RowUpdated(object sender, DevExpress.XtraGrid.Views.Base.RowObjectEventArgs e)
        {
            if (calculoligado)
                CalculaTotais();
        }

        TimeSpan jornada;
        TimeSpan Sjornada;

        

        

        private void repositoryItemTimeEdit1_ButtonClick(object sender, DevExpress.XtraEditors.Controls.ButtonPressedEventArgs e)
        {
            DevExpress.XtraEditors.TimeEdit editor = (DevExpress.XtraEditors.TimeEdit)sender;
            impressos.dPlaPonto.diasRow drow = (impressos.dPlaPonto.diasRow)bandedGridView1.GetDataRow(bandedGridView1.FocusedRowHandle);
            drow[bandedGridView1.FocusedColumn.FieldName] = DBNull.Value;
            bandedGridView1.CloseEditor();
            
            CalculaTotais();
        }

        private void Limpar(object sender, DevExpress.XtraEditors.Controls.ButtonPressedEventArgs e)
        {
            DevExpress.XtraEditors.TimeEdit Hx = (DevExpress.XtraEditors.TimeEdit)sender;            
            simpleButton1.Focus();
            Application.DoEvents();
            Hx.EditValue = null;
            if (Hx == H1)
                LinhaGeral.SetH1Null();
            if (Hx == H2)
                LinhaGeral.SetH2Null();
            if (Hx == H3)
                LinhaGeral.SetH3Null();
            if (Hx == H4)
                LinhaGeral.SetH4Null();
            if (Hx == SH1)
                LinhaGeral.SetSH1Null();
            if (Hx == SH2)
                LinhaGeral.SetSH2Null();
            if (Hx == SH3)
                LinhaGeral.SetSH3Null();
            if (Hx == SH4)
                LinhaGeral.SetSH4Null();
        }

        /*
        private void simpleButton3_Click(object sender, EventArgs e)
        {
            ArquivosTXT.cTabelaTXT TXT = new ArquivosTXT.cTabelaTXT(DP.impressos.dGravarDTM.dGravarDTMSt.Gravar, ArquivosTXT.ModelosTXT.layout);
            TXT.RegistraMapa(dPlaPonto.Gravar.EmpresaColumn, 1, 3);
            TXT.RegistraMapa(dPlaPonto.Gravar.RegistroColumn, 4, 6);
            TXT.RegistraMapa(dPlaPonto.Gravar.EventoColumn, 10, 3);
            TXT.RegistraMapa(dPlaPonto.Gravar.ReferenciaColumn, 13, 5);           
            TXT.RegistraMapa(dPlaPonto.Gravar.ValorColumn, 18, 11);
            System.Windows.Forms.SaveFileDialog SF = new SaveFileDialog();
            SF.FileName = @"\\neon01\Folha\DTM\Apontamentos\Teste.txt";
            if (SF.ShowDialog() == DialogResult.OK)
            {
                TXT.Salva(SF.FileName);
                DP.impressos.dGravarDTM.dGravarDTMSt.Gravar.Clear();
            }
        }*/

        private void bandedGridView1_RowStyle(object sender, DevExpress.XtraGrid.Views.Grid.RowStyleEventArgs e)
        {
            
            DP.impressos.dPlaPonto.diasRow drow = (DP.impressos.dPlaPonto.diasRow)bandedGridView1.GetDataRow(e.RowHandle);
            if (drow != null)
                if (drow.Folga)
                    e.Appearance.BackColor = Color.Aqua;
                else
                    if (drow.semana == "sab")
                        e.Appearance.BackColor = Color.Silver;
                    else if (drow.semana == "dom")
                        e.Appearance.BackColor = Color.Pink;
                    //else if(drow.dia % 2 == 1)
                        //e.Appearance.BackColor = Color.FromArgb(255,255,200);
            
        }

        private void repositoryItemButtonEdit1_ButtonClick(object sender, DevExpress.XtraEditors.Controls.ButtonPressedEventArgs e)
        {
            bandedGridView1.CloseEditor();
            impressos.dPlaPonto.diasRow linhaClicada = (impressos.dPlaPonto.diasRow)bandedGridView1.GetFocusedDataRow();
            linhaClicada.EndEdit();
            if (e.Button.Caption == "F")
            {                
                bool Ligado = false;
                int contador = 0;
                bool FolgaAnterior = false;
                foreach (impressos.dPlaPonto.diasRow drow in dPlaPonto.dias)
                {
                    bool MarcarFolga = false;
                    if (drow == linhaClicada)
                    {
                        Ligado = true;
                        if (LinhaGeral.Turno)
                            contador = FolgaAnterior ? 8 : 5;
                        else
                            contador = 7;
                    };
                    if (Ligado)
                    {
                        drow.Folga = false;
                        if (LinhaGeral.Turno)
                        {
                            if (!FolgaAnterior)
                            {
                                if (contador == 5)
                                {
                                    contador = 0;
                                    MarcarFolga = true;
                                }
                            }
                            else
                            {
                                if (contador == 7)
                                {
                                    MarcarFolga = true;
                                }
                                if (contador == 8)
                                {
                                    contador = 0;
                                    MarcarFolga = true;
                                }
                            }
                        }
                        else
                        {
                            if (contador == 7)
                            {
                                contador = 0;
                                MarcarFolga = true;
                            }
                        }
                        if (MarcarFolga)
                        {
                            drow.Folga = true;
                            drow.SetPONEntradaNull();
                            drow.SetPONAlmocoNull();
                            drow.SetPONAlmocoRetNull();
                            drow.SetPONSaidaNull();
                        }
                        else
                        {
                            if (H1.EditValue != null)
                                drow.PONEntrada = H1.Time;
                            if (LinhaGeral.maracarAlmoco)
                            {
                                if (H2.EditValue != null)
                                    drow.PONAlmoco = H2.Time;
                                if (H3.EditValue != null)
                                    drow.PONAlmocoRet = H3.Time;
                            }
                            if (H4.EditValue != null)
                                drow.PONSaida = H4.Time;
                        }
                        contador++;
                    }
                    else
                        FolgaAnterior = drow.Folga;
                };
            };
            if (e.Button.Caption == "E") {
                AjustaEscala(linhaClicada);
            };
            
            CalculaTotais();
        }

        private void Calcula(object sender, EventArgs e)
        {
            
            CalculaTotais();
        }

        private void calcEdit3_EditValueChanged(object sender, EventArgs e)
        {

        }

        private void repositoryUp_ButtonClick(object sender, DevExpress.XtraEditors.Controls.ButtonPressedEventArgs e)
        {
            impressos.dPlaPonto.diasRow LinhaMae = (impressos.dPlaPonto.diasRow)bandedGridView1.GetFocusedDataRow();
            if ((LinhaMae == null) || (bandedGridView1.FocusedColumn == null))
                return;
            try
            {
                if (bandedGridView1.FocusedColumn.Name == "U1")
                {
                    if (LinhaMae.IsPONEntradaNull())
                        LinhaMae.PONEntrada = H1.Time;
                    else
                        LinhaMae.PONEntrada = LinhaMae.PONEntrada.AddHours(0.5);
                }
                if (bandedGridView1.FocusedColumn.Name == "D1")
                {
                    if (LinhaMae.IsPONEntradaNull())
                        LinhaMae.PONEntrada = H1.Time;
                    else
                        LinhaMae.PONEntrada = LinhaMae.PONEntrada.AddHours(-0.5);
                };
                if (bandedGridView1.FocusedColumn.Name == "U2")
                {
                    if (LinhaMae.IsPONAlmocoNull())
                        LinhaMae.PONAlmoco = H2.Time;
                    else
                        LinhaMae.PONAlmoco = LinhaMae.PONAlmoco.AddHours(0.5);
                }
                if (bandedGridView1.FocusedColumn.Name == "D2")
                {
                    if (LinhaMae.IsPONAlmocoNull())
                        LinhaMae.PONAlmoco = H2.Time;
                    else
                        LinhaMae.PONAlmoco = LinhaMae.PONAlmoco.AddHours(-0.5);
                }
                if (bandedGridView1.FocusedColumn.Name == "U3")
                {
                    if (LinhaMae.IsPONAlmocoRetNull())
                        LinhaMae.PONAlmocoRet = H3.Time;
                    else
                        LinhaMae.PONAlmocoRet = LinhaMae.PONAlmocoRet.AddHours(0.5);
                };
                if (bandedGridView1.FocusedColumn.Name == "D3")
                {
                    if (LinhaMae.IsPONAlmocoRetNull())
                        LinhaMae.PONAlmocoRet = H3.Time;
                    else
                        LinhaMae.PONAlmocoRet = LinhaMae.PONAlmocoRet.AddHours(-0.5);
                }
                if (bandedGridView1.FocusedColumn.Name == "U4")
                {
                    if (LinhaMae.IsPONSaidaNull())
                        LinhaMae.PONSaida = H4.Time;
                    else
                        LinhaMae.PONSaida = LinhaMae.PONSaida.AddHours(0.5);
                }
                if (bandedGridView1.FocusedColumn.Name == "D4")
                {
                    if (LinhaMae.IsPONSaidaNull())
                        LinhaMae.PONSaida = H4.Time;
                    else
                        LinhaMae.PONSaida = LinhaMae.PONSaida.AddHours(-0.5);
                };
                
                CalculaTotais();
            }
            catch 
            { 
            }
        }

        private void AjustaEscala(impressos.dPlaPonto.diasRow Primeira) {
            bool FeriasAbonar = false;
            bool pular = (Primeira != null);
            TimeSpan jorFor = TimeSpan.MinValue;
            cFolguista cF = new cFolguista();
            cF.H1.Time = H1.Time;
            cF.H4.Time = H4.Time;
            if (chAlmoco.Checked)
            {
                cF.H2.Time = H2.Time;
                cF.H3.Time = H3.Time;
            };
            if (cF.ShowDialog() != DialogResult.OK)
                return;
            int[] seq = null;
            int i = 0;
            if (cF.Resposta == -1)
            {
                FeriasAbonar = true;
            }
            else
                if (cF.Resposta == 0)
                {
                    seq = cF.mascara;
                    i = cF.fase;
                    if (cF.mascara.Length == 2)
                        jorFor = new TimeSpan(12,0,0);
                }
                else
                {
                    if (cF.Resposta < 11)
                    {
                        seq = new int[] { 6, 6, 14, 14, 22, 22, 0, 0 };
                        i = cF.Resposta - 1;
                        jorFor = new TimeSpan(8,0,0);
                    }
                    else
                        if (cF.Resposta < 21)
                        {
                            seq = new int[] { 6, 14, 22, 0, 0 };
                            i = cF.Resposta - 11;
                            jorFor = new TimeSpan(8, 0, 0);
                        }
                        else
                            if (cF.Resposta < 31)
                            {
                                seq = new int[] { 6, 0 };
                                i = cF.Resposta - 21;
                                jorFor = new TimeSpan(12, 0, 0);
                            }
                            else
                                if (cF.Resposta < 41)
                                {
                                    seq = new int[] { 18, 0 };
                                    i = cF.Resposta - 31;
                                    jorFor = new TimeSpan(12, 0, 0);
                                }


                }
            if ((seq != null) || (FeriasAbonar))
            {
                foreach (impressos.dPlaPonto.diasRow drow in dPlaPonto.dias)
                {
                    if (Primeira == drow)
                        pular = false;
                    if (!pular)
                    {
                        if (FeriasAbonar)
                        {
                            drow.Abonar = true;
                            continue;
                        }
                        drow.Folga = false;
                        
                        drow.SetPONEntradaNull();
                        drow.SetPONAlmocoNull();
                        drow.SetPONAlmocoRetNull();
                        drow.SetPONSaidaNull();
                        drow.SetForcarJornadaNull();
                        drow.Abonar = false;
                        if (jorFor != TimeSpan.MinValue)
                            drow.ForcarJornada = DateTime.Today.Add(jorFor);
                        

                        if (seq[i] == -1) 
                        {
                            if (cF.H1.EditValue != null)
                                drow.PONEntrada = cF.H1.Time;
                            if (cF.H2.EditValue != null)
                            {
                                drow.PONAlmoco = cF.H2.Time;
                                if ((cF.H1.EditValue != null) && (cF.H4.EditValue == null))
                                {
                                    DateTime dmanobra = cF.H2.Time;
                                    while (dmanobra < cF.H1.Time)
                                        dmanobra = dmanobra.AddDays(1);
                                    TimeSpan tjor = dmanobra - cF.H1.Time;

                                    drow.ForcarJornada = DateTime.Today.Add(tjor);
                                }
                            }
                            if (cF.H3.EditValue != null)
                                drow.PONAlmocoRet = cF.H3.Time;
                            if (cF.H4.EditValue != null)
                            {
                                drow.PONSaida = cF.H4.Time;
                                if (cF.H1.EditValue != null){
                                    DateTime dmanobra = cF.H4.Time;
                                    while (dmanobra < cF.H1.Time)
                                        dmanobra = dmanobra.AddDays(1);
                                    TimeSpan tjor = dmanobra - cF.H1.Time;

                                    drow.ForcarJornada = DateTime.Today.Add(tjor);
                                    }
                            }
                           
                        }


                        else if (seq[i] == 0)
                            drow.Folga = true;

                        else
                        {
                            drow.PONEntrada = DateTime.Today + new TimeSpan(seq[i], 0, 0);
                            if (chAlmoco.Checked)
                            {
                                drow.PONAlmoco = DateTime.Today + new TimeSpan(seq[i] + cF.jornada-3, 0, 0);
                                drow.PONAlmocoRet = DateTime.Today + new TimeSpan(seq[i] + cF.jornada-2, 0, 0);
                            }
                            drow.PONSaida = DateTime.Today + new TimeSpan(seq[i] + cF.jornada, 0, 0);
                        };
                        
                        i++;
                        if (seq.Length <= i)
                            i = 0;
                    };
                    


                }
            };
            
            CalculaTotais();
        }

        private void simpleButton3_Click_1(object sender, EventArgs e)
        {
            AjustaEscala(null);            
            
        }

        private void simpleButton5_Click(object sender, EventArgs e)
        {
            if (Chamador != null)
                Chamador.UnRegistraFilhote(this);
            FechaTela(DialogResult.Cancel);
        }
        
        private void simpleButton6_Click_1(object sender, EventArgs e)
        {
            H1.EditValue = null;
        }
    }
}

