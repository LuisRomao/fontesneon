﻿using DPProc;
using System;
using CompontesBasicosProc;
using System.Windows.Forms;

namespace DP.FolhaPag
{
    /// <summary>
    /// 
    /// </summary>
    public partial class cDadosParaFolhaPag : CompontesBasicos.ComponenteBaseDialog
    {
        /// <summary>
        /// 
        /// </summary>
        public cDadosParaFolhaPag()
        {
            InitializeComponent();
            radioGroup1.Properties.Items.Clear();
            radioGroup1.Properties.Items.Add(new DevExpress.XtraEditors.Controls.RadioGroupItem(TipoBancaria.Pagamento, "Pagamento"));
            radioGroup1.Properties.Items.Add(new DevExpress.XtraEditors.Controls.RadioGroupItem(TipoBancaria.Adiantamento, "Adiantamento"));
            radioGroup1.Properties.Items.Add(new DevExpress.XtraEditors.Controls.RadioGroupItem(TipoBancaria.DecimoTerceiro, "décimo terceiro"));
            radioGroup1.Properties.Items.Add(new DevExpress.XtraEditors.Controls.RadioGroupItem(TipoBancaria.Avulso, "Avulso"));
        }

        /// <summary>
        /// 
        /// </summary>
        public TipoBancaria? Selecionado
        {
            get
            {
                if (radioGroup1.EditValue == null)
                    return null;
                else
                    return (TipoBancaria)radioGroup1.EditValue;
            }
        }        

        private void radioGroup1_EditValueChanged(object sender, EventArgs e)
        {
            rgFinal.Visible = Selecionado.EstaNoGrupo(TipoBancaria.Adiantamento, TipoBancaria.Pagamento);
        }

        /// <summary>
        /// 
        /// </summary>
        public bool Final;

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public override bool CanClose()
        {
            if (radioGroup1.EditValue == null)
            {
                MessageBox.Show("Tipo de pagamento não definido");
                return false;
            }
            if (((TipoBancaria)radioGroup1.EditValue).EstaNoGrupo(TipoBancaria.Adiantamento, TipoBancaria.Pagamento))
            {
                if (rgFinal.EditValue == null)
                    return false;
                else
                    Final = (bool)rgFinal.EditValue;
            }
            return base.CanClose();
        }
    }    
}
