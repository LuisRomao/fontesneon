﻿using CompontesBasicosProc;
using CompontesBasicos;
using ContaPagar.Follow;
using ContaPagarProc.Follow;
using dllCheques;
using dllClienteServProc;
using DocBacarios;
using DP.Automacao_Bancaria;
using DP.Virtual;
using DPProc;
using Framework;
using Framework.objetosNeon;
using FrameworkProc;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Drawing;
using System.IO;
using System.Threading;
using System.Windows.Forms;
using VirEnumeracoesNeon;

using static DP.Automacao_Bancaria.cAutomacaoBancaria;

namespace DP.FolhaPag
{
    /// <summary>
    /// Pagamentos RH
    /// </summary>
    public partial class cFolhaPag : ComponenteBase
    {

        private cAutomacaoBancaria Chamador;

        private bool? fechamentoCompetencia;


        private int nCheques;
        private int nCreditoConta;
        private int nErros;
        private int nChequeEletronio;
        private SortedList<int, PagamentoPeriodico> Periodicos = new SortedList<int, PagamentoPeriodico>();

        private RetornoServidorProcessos Retorno = null;
        private SRH.RHClient RHClient1;
        private dAutomacaoBancaria.RHTarefaRow rowmaeRHT;
        private dAutomacaoBancaria.RHTarefaRow rowmaeRHTF;
        private bool SemafaroPLA;
        private TipoBancaria tipoBancaria;

        private bool AjustaPeriodicos(CompontesBasicos.Espera.cEspera ESP, DateTime? VencimentoCh, DateTime? VencimentoEL)
        {
            //try
            //{
                //dPagamentosPeriodicos.dPagamentosPeriodicosSt.TravarRecarga = true;
                //dPagamentosPeriodicos.dPagamentosPeriodicosStF.TravarRecarga = true;
                DateTime Vencimento = VencimentoCh.GetValueOrDefault(DateTime.MaxValue) < VencimentoEL.GetValueOrDefault(DateTime.MaxValue) ? VencimentoCh.GetValueOrDefault(DateTime.MaxValue) : VencimentoEL.GetValueOrDefault(DateTime.MaxValue);
                ESP.Espere("Ajustando Periódicos");
                ESP.AtivaGauge(dAutomacaoBancaria.RHTarefaDetalhe.Count);
                int i = 1;
                foreach (dAutomacaoBancaria.RHTarefaDetalheRow RHDrow in dAutomacaoBancaria.RHTarefaDetalhe)
                {
                    i++;
                    if (ESP.GaugeTempo(i))
                        return false;
                    PagamentoPeriodico PagamentoPeriodico = Periodicos[RHDrow.RHD];
                    if (!PagamentoPeriodico.Encontrado)
                        PagamentoPeriodico.CadastraPGF(RHDrow.RHD_CON,
                                                       TipoPagPeriodico1.Value,
                                                       Vencimento,
                                                       RHDrow.Total,
                                                       cCompet1.Comp);
                    else
                    {
                        if (FechamentoCompetencia)
                        {
                            decimal media = RHDrow.ValorAcumulado;
                            int nMedia = 1;
                            if (!RHDrow.IsValorAnterior1Null())
                            {
                                nMedia++;
                                media += RHDrow.ValorAnterior1;
                            }
                            if (!RHDrow.IsValorAnterior2Null())
                            {
                                nMedia++;
                                media += RHDrow.ValorAnterior2;
                            }
                            if (!RHDrow.IsValorAnterior3Null())
                            {
                                nMedia++;
                                media += RHDrow.ValorAnterior3;
                            }
                            PagamentoPeriodico.Atualizar(dateCreditoConta.DateTime.Day, Math.Round(media / nMedia, 2, MidpointRounding.AwayFromZero));
                        }
                    }
                    //PagamentoPeriodico.CadastrarProximo();
                }
            //}
            //finally
            //{
                //dPagamentosPeriodicos.dPagamentosPeriodicosSt.TravarRecarga = false;
                //dPagamentosPeriodicos.dPagamentosPeriodicosStF.TravarRecarga = false;
            //    CompontesBasicos.Performance.Performance.PerformanceST.Registra("CarregaDadosPeriodicos");
                //trocaTipo(ESP, true);
            //}
            return true;
        }

        private void botaoZerar_Click(object sender, EventArgs e)
        {
            using (CompontesBasicos.Espera.cEspera ESP = new CompontesBasicos.Espera.cEspera(this))
            {
                Application.DoEvents();
                foreach (dAutomacaoBancaria.RHTarefaSubdetalheRow RHSrow in dAutomacaoBancaria.RHTarefaSubdetalhe)
                    if (RHSrow.EMP == DSCentral.EMP)
                        RHSrow.RHSValor = 0;
                dAutomacaoBancaria.RHTarefaSubdetalheTableAdapter.Update(dAutomacaoBancaria.RHTarefaSubdetalhe);
                foreach (dAutomacaoBancaria.RHTarefaSubdetalheRow RHSrow in dAutomacaoBancaria.RHTarefaSubdetalhe)
                    if (RHSrow.EMP == DSCentral.EMPF)
                        RHSrow.RHSValor = 0;
                dAutomacaoBancaria.RHTarefaSubdetalheTableAdapterF.Update(dAutomacaoBancaria.RHTarefaSubdetalhe);
            }
        }
        private void btnImprimirPagamentos_Click(object sender, EventArgs e)
        {
            if (TabControlSalarios.SelectedTabPage == TabSAL)
                gridPagamentos.ShowPrintPreview();
            else
                gridControlCondom.ShowPrintPreview();
        }

        private void btnRemessaPagamentos_Click(object sender, EventArgs e)
        {
            Validate(true);
            ValidaPagamentos();
            bool cancelar = false;
            if (nErros != 0)
            {
                ViewPagamentos.ExpandAllGroups();
                cancelar = true;
            };
            if ((nCheques + nChequeEletronio > 0) && (dateDataCheque.DateTime < DateTime.Today))
            {
                dateDataCheque.BackColor = Color.Yellow;
                MessageBox.Show("Data para cheques inválida");
                dateDataCheque.Focus();
                cancelar = true;
            }
            if ((nCreditoConta > 0) && (dateCreditoConta.DateTime < DateTime.Today))
            {
                dateCreditoConta.BackColor = Color.Yellow;
                MessageBox.Show("Data para credito inválida");
                dateCreditoConta.Focus();
                cancelar = true;
            }
            if (!cancelar)
                EmitePagamentos();
            else
                MessageBox.Show("Verifique os erros");
        }

        private bool CadastraNotasCheques(CompontesBasicos.Espera.cEspera ESP, DateTime? VencimentoCh, DateTime? VencimentoEL)
        {
            if (TipoPagPeriodico1.HasValue)
                if (!AjustaPeriodicos(ESP, VencimentoCh, VencimentoEL))
                    return false;
            ESP.TrocaModelo(true);
            ESP.Espere("Cadastrando Pagamentos");
            ESP.AtivaGauge(30);
            List<dAutomacaoBancaria.RHTarefaRow> TarefasGravar = new List<dAutomacaoBancaria.RHTarefaRow>();
            if (rowmaeRHT != null)
            {
                dAutomacaoBancaria.RHTarefaTableAdapter.Update(rowmaeRHT);
                TarefasGravar.Add(rowmaeRHT);
            }
            if (rowmaeRHTF != null)
            {
                dAutomacaoBancaria.RHTarefaTableAdapterF.Update(rowmaeRHTF);
                TarefasGravar.Add(rowmaeRHTF);
            }
            ESP.Espere("Cadastrando Pagamentos 1");
            foreach (dAutomacaoBancaria.RHTarefaRow TarefaGravar in TarefasGravar)
            {
                Thread ThreadRH = null;
                bool terminado = false;
                int tentativas = 0;
                ESP.AtivaGauge(30);
                ESP.Gauge(0);
                while (!terminado && (tentativas < TentavasRecuperar))
                {
                    tentativas++;
                    if (ClienteServProc.TipoServidoSelecionado == TiposServidorProc.Simulador)
                    {
                        ESP.Espere("ATENÇÃO: uso do simulador");
                        ThreadRH = new Thread(() => Retorno = SimulaRH_GravaSalario(TarefaGravar.EMP, Framework.DSCentral.USUX(TarefaGravar.EMP), TarefaGravar.RHT));
                    }
                    else
                    {
                        RHClient1 = new SRH.RHClient(URLServidorProcessos);
                        ThreadRH = new Thread(() => Retorno = ThreadChamadaAoServidor(TarefaGravar.EMP, Framework.DSCentral.USUX(TarefaGravar.EMP), TarefaGravar.RHT));
                    }
                    ThreadRH.Start();
                    DateTime proxima = DateTime.Now.AddSeconds(3);
                    while (ThreadRH.IsAlive)
                    {
                        if (DateTime.Now > proxima)
                        {
                            ESP.Gauge();
                            proxima = DateTime.Now.AddSeconds(1);
                        }
                        System.Windows.Forms.Application.DoEvents();
                    }
                    if (Retorno.ok)
                        terminado = true;
                    else
                        if (Retorno.TipoDeErro != VirExceptionProc.VirTipoDeErro.recuperavel)
                    {
                        terminado = true;
                        break;
                    }
                    else
                    {
                        ESP.Espere(string.Format("Cadastrando Pagamentos (Tentativa {0})", tentativas));
                        ESP.AtivaGauge(30);
                        ESP.Gauge(0);
                    }
                    ESP.Espere(string.Format("Cadastrando Pagamentos {0}", tentativas + 1));
                    ESP.AtivaGauge(30);
                }

                if (!Retorno.ok)
                {
                    if (!CompontesBasicosProc.FuncoesBasicasProc.EstaEmProducao)
                    {
                        System.Windows.Forms.Clipboard.SetText(Retorno.Mensagem);
                        System.Windows.Forms.MessageBox.Show(Retorno.Mensagem);
                    }
                    switch (Retorno.TipoDeErro)
                    {
                        case VirExceptionProc.VirTipoDeErro.concorrencia:
                            MessageBox.Show("Os dados que deveriam ser gravados foram alterados por outro usuário.\r\n\r\nDADOS NÃO GRAVADOS!!", "ATENÇÃO");
                            break;
                        case VirExceptionProc.VirTipoDeErro.efetivo_bug:
                            MessageBox.Show("Dados não gravados!! Erro reportado");
                            VirEmailNeon.EmailDiretoNeon.EmalST.Enviar("luis@virweb.com.br", Retorno.Mensagem, "ERRO NO SERVIDOR DE PROCESSOS Salário");
                            break;
                        case VirExceptionProc.VirTipoDeErro.local:
                        case VirExceptionProc.VirTipoDeErro.recuperavel:
                            MessageBox.Show(string.Format("Erro:{0}\r\n\r\nDados não gravados!!", Retorno.MensagemSimplificada));
                            break;
                    }
                }
            }
            return Retorno == null ? true : Retorno.ok;
        }

        private bool CarregaDadosPeriodicos(CompontesBasicos.Espera.cEspera ESP)
        {
            //PeriodicoCarregado = TipoPagPeriodico;
            Periodicos.Clear();
            if (TipoPagPeriodico1.HasValue)
            {
                if (ESP != null)
                {
                    ESP.Espere("Carregando Periódicos");
                    Application.DoEvents();
                    if (ESP.Abortar)
                        return false;
                }
                //Competencia CompCorte = new Competencia().Add(-3);
                Competencia CompCorte = cCompet1.Comp.CloneCompet(-3);
                PagamentoPeriodico.CarregaPGFAtivos(TipoPagPeriodico1.Value, EMPTipo.Local, CompCorte);
                if (ESP != null)
                {
                    ESP.Espere("Carregando Periódicos Filial");
                    Application.DoEvents();
                    if (ESP.Abortar)
                        return false;
                }
                PagamentoPeriodico.CarregaPGFAtivos(TipoPagPeriodico1.Value, EMPTipo.Filial, CompCorte);
            }
            return true;
        }

        private void CarregaHistoricoPeriodicos()
        {
            if (tipoBancaria.EstaNoGrupo(TipoBancaria.Avulso, TipoBancaria.DecimoTerceiro))
            {
                colValorAcumulado.Visible = false;
                colValorAnterior3.Visible = false;
                colValorAnterior2.Visible = false;
                colValorAnterior1.Visible = false;
            }
            else
            {
                colValorAcumulado.Visible = true;
                colValorAnterior3.Visible = true;
                colValorAnterior2.Visible = true;
                colValorAnterior1.Visible = true;
                Competencia[] CompA = new Competencia[] { cCompet1.Comp.CloneCompet(-3), cCompet1.Comp.CloneCompet(-2), cCompet1.Comp.CloneCompet(-1) };
                colValorAcumulado.Caption = string.Format("Ac {0}", cCompet1.Comp);
                colValorAnterior3.Caption = CompA[0].ToString();
                colValorAnterior2.Caption = CompA[1].ToString();
                colValorAnterior1.Caption = CompA[2].ToString();
                foreach (dAutomacaoBancaria.RHTarefaDetalheRow RHDrow in dAutomacaoBancaria.RHTarefaDetalhe)
                {
                    decimal media = 0;
                    int nMedia = 0;
                    if (!Periodicos[RHDrow.RHD].Encontrado)
                        continue;
                    decimal ValorAcumular;
                    foreach (dPagamentosPeriodicos.NOtAsRow rowNota in Periodicos[RHDrow.RHD].PGFrow.GetNOtAsRows())
                    {                        
                        if (rowNota.NOACompet == CompA[0].CompetenciaBind)
                        {
                            if (RHDrow.IsValorAnterior3Null())
                            {
                                ValorAcumular = 0;
                                nMedia++;
                            }
                            else
                                ValorAcumular = RHDrow.ValorAnterior3;
                            RHDrow.ValorAnterior3 = ValorAcumular + rowNota.NOATotal;                            
                            media += rowNota.NOATotal;
                        }
                        else if (rowNota.NOACompet == CompA[1].CompetenciaBind)
                        {
                            if (RHDrow.IsValorAnterior2Null())
                            {
                                ValorAcumular = 0;
                                nMedia++;
                            }
                            else
                                ValorAcumular = RHDrow.ValorAnterior2;
                            RHDrow.ValorAnterior2 = ValorAcumular + rowNota.NOATotal;
                            media += rowNota.NOATotal;
                        }
                        else if (rowNota.NOACompet == CompA[2].CompetenciaBind)
                        {
                            if (RHDrow.IsValorAnterior1Null())
                            {
                                ValorAcumular = 0;
                                nMedia++;
                            }
                            else
                                ValorAcumular = RHDrow.ValorAnterior1;
                            RHDrow.ValorAnterior1 = ValorAcumular + rowNota.NOATotal;
                            media += rowNota.NOATotal;
                        }
                        else if (rowNota.NOACompet == cCompet1.Comp.CompetenciaBind)
                        {
                            RHDrow.ValorAcumulado = RHDrow.ValorAcumulado + rowNota.NOATotal;
                        }
                    }
                    if (nMedia != 0)
                        RHDrow.ValorAnteriorMedio = Math.Round(media / nMedia, 2, MidpointRounding.AwayFromZero);
                }
            }
        }

        private void dateCreditoConta_Properties_DateTimeChanged(object sender, EventArgs e)
        {
            if ((rowmaeRHT != null) && (rowmaeRHT.RHTDataCredito != dateCreditoConta.DateTime))
                rowmaeRHT.RHTDataCredito = dateCreditoConta.DateTime;
            if ((rowmaeRHTF != null) && (rowmaeRHTF.RHTDataCredito != dateCreditoConta.DateTime))
                rowmaeRHTF.RHTDataCredito = dateCreditoConta.DateTime;
        }

        private void dateDataCheque_Properties_DateTimeChanged(object sender, EventArgs e)
        {
            if ((rowmaeRHT != null) && (rowmaeRHT.RHTDataCheque != dateDataCheque.DateTime))
                rowmaeRHT.RHTDataCheque = dateDataCheque.DateTime;
            if ((rowmaeRHTF != null) && (rowmaeRHTF.RHTDataCheque != dateDataCheque.DateTime))
                rowmaeRHTF.RHTDataCheque = dateDataCheque.DateTime;
        }

        private void EmiteChequesSalarioEletronico(VirDB.Bancovirtual.TiposDeBanco TiposDeBanco, int RHT)
        {
            string comando =
"UPDATE       dbo.CHEques\r\n" +
"SET                CHEEmissao = @P1, CHE_CCT = dbo.ContaCorrenTe.CCT, CHEStatus = 4, CHENumero = dbo.CHEques.CHE\r\n" +
"FROM            dbo.RHTarefaDetalhe INNER JOIN\r\n" +
"                         dbo.NOtAs ON dbo.RHTarefaDetalhe.RHD_NOA = dbo.NOtAs.NOA INNER JOIN\r\n" +
"                         dbo.PAGamentos ON dbo.NOtAs.NOA = dbo.PAGamentos.PAG_NOA INNER JOIN\r\n" +
"                         dbo.CHEques ON dbo.PAGamentos.PAG_CHE = dbo.CHEques.CHE INNER JOIN\r\n" +
"                         dbo.CONDOMINIOS ON dbo.CHEques.CHE_CON = dbo.CONDOMINIOS.CON INNER JOIN\r\n" +
"                         dbo.ContaCorrenTe ON dbo.CONDOMINIOS.CON = dbo.ContaCorrenTe.CCT_CON\r\n" +
"WHERE        (dbo.RHTarefaDetalhe.RHD_RHT = @P2) AND (dbo.CHEques.CHETipo = 28) AND (dbo.CHEques.CHEStatus = 1) AND (dbo.ContaCorrenTe.CCTPrincipal = 1);";
            VirMSSQL.TableAdapter.ST(TiposDeBanco).ExecutarSQLNonQuery(comando, DateTime.Now, RHT);
        }

        private void EmitePagamentos()
        {
            using (CompontesBasicos.Espera.cEspera ESP = new CompontesBasicos.Espera.cEspera(this))
            {
                Application.DoEvents();
                try
                {
                    btnRemessaPagamentos.Enabled = false;
                    ViewPagamentos.BeginDataUpdate();
                    gridViewPagCond.BeginDataUpdate();
                    gridViewPagCondSdet.BeginDataUpdate();
                    Limpazeros();
                    List<dAutomacaoBancaria.RHTarefaRow> RHTgravar = new List<Automacao_Bancaria.dAutomacaoBancaria.RHTarefaRow>();
                    List<dAutomacaoBancaria.RHTarefaRow> RHTgravarF = new List<Automacao_Bancaria.dAutomacaoBancaria.RHTarefaRow>();
                    foreach (dAutomacaoBancaria.RHTarefaRow RHTrow in dAutomacaoBancaria.RHTarefa)
                    {
                        if (RHTrow.RHTCompetencia != cCompet1.Comp.CompetenciaBind)
                            RHTrow.RHTCompetencia = cCompet1.Comp.CompetenciaBind;
                        if ((RHTrow.RHTDataCheque != dateDataCheque.DateTime) && (dateDataCheque.DateTime > DateTime.MinValue))
                            RHTrow.RHTDataCheque = dateDataCheque.DateTime;
                        if ((RHTrow.RHTDataCredito != dateCreditoConta.DateTime) && (dateCreditoConta.DateTime > DateTime.MinValue))
                            RHTrow.RHTDataCredito = dateCreditoConta.DateTime;
                        if (RHTrow.RHTTipo != (int)tipoBancaria)
                            RHTrow.RHTTipo = (int)tipoBancaria;
                        if (RHTrow.RowState != DataRowState.Unchanged)
                            if (RHTrow.EMP == Framework.DSCentral.EMP)
                                RHTgravar.Add(RHTrow);
                            else
                                RHTgravarF.Add(RHTrow);
                    }
                    //int Errar = 6;
                    //string strErar = "";
                    List<dAutomacaoBancaria.RHTarefaDetalheRow> RHDgravar = new List<Automacao_Bancaria.dAutomacaoBancaria.RHTarefaDetalheRow>();
                    List<dAutomacaoBancaria.RHTarefaDetalheRow> RHDgravarF = new List<Automacao_Bancaria.dAutomacaoBancaria.RHTarefaDetalheRow>();
                    foreach (dAutomacaoBancaria.RHTarefaDetalheRow RHDrow in dAutomacaoBancaria.RHTarefaDetalhe)
                        if (RHDrow.RowState != DataRowState.Unchanged)
                            if (RHDrow.EMP == Framework.DSCentral.EMP)
                                RHDgravar.Add(RHDrow);
                            else
                                RHDgravarF.Add(RHDrow);
                    List<dAutomacaoBancaria.RHTarefaSubdetalheRow> RHSgravar = new List<Automacao_Bancaria.dAutomacaoBancaria.RHTarefaSubdetalheRow>();
                    List<dAutomacaoBancaria.RHTarefaSubdetalheRow> RHSgravarF = new List<Automacao_Bancaria.dAutomacaoBancaria.RHTarefaSubdetalheRow>();
                    foreach (dAutomacaoBancaria.RHTarefaSubdetalheRow RHSrow in dAutomacaoBancaria.RHTarefaSubdetalhe)
                        if ((RHSrow.RHSValor != 0) && (RHSrow.RowState != DataRowState.Unchanged))
                            if (RHSrow.EMP == Framework.DSCentral.EMP)
                                RHSgravar.Add(RHSrow);
                            else
                                RHSgravarF.Add(RHSrow);
                    ESP.Espere("Cadastro Tarefa 1/6");
                    try
                    {
                        VirMSSQL.TableAdapter.AbreTrasacaoSQL("Cadastro tarefas cAutomacaoBancaria.cs 3072",
                                                               dAutomacaoBancaria.RHTarefaTableAdapter,
                                                               dAutomacaoBancaria.RHTarefaDetalheTableAdapter,
                                                               dAutomacaoBancaria.RHTarefaSubdetalheTableAdapter);
                        foreach (dAutomacaoBancaria.RHTarefaRow RHTrow in RHTgravar)
                            dAutomacaoBancaria.RHTarefaTableAdapter.Update(RHTrow);
                        ESP.Espere("Cadastro Tarefa 2/6");
                        foreach (dAutomacaoBancaria.RHTarefaDetalheRow RHDrow in RHDgravar)
                            dAutomacaoBancaria.RHTarefaDetalheTableAdapter.Update(RHDrow);
                        ESP.Espere("Cadastro Tarefa 3/6");
                        foreach (dAutomacaoBancaria.RHTarefaSubdetalheRow RHSrow in RHSgravar)
                            dAutomacaoBancaria.RHTarefaSubdetalheTableAdapter.Update(RHSrow);
                        VirMSSQL.TableAdapter.CommitSQL();
                    }
                    catch (Exception e1)
                    {
                        VirMSSQL.TableAdapter.VircatchSQL(e1);
                        throw new Exception("Erro na gravação:" + e1.Message, e1);
                    }
                    ESP.Espere("Cadastro Tarefa 4/6");
                    try
                    {
                        VirMSSQL.TableAdapter.AbreTrasacaoSQL("Cadastro tarefas filial cAutomacaoBancaria.cs 3199",
                                                               dAutomacaoBancaria.RHTarefaTableAdapterF,
                                                               dAutomacaoBancaria.RHTarefaDetalheTableAdapterF,
                                                               dAutomacaoBancaria.RHTarefaSubdetalheTableAdapterF);
                        foreach (dAutomacaoBancaria.RHTarefaRow RHTrow in RHTgravarF)
                            dAutomacaoBancaria.RHTarefaTableAdapterF.Update(RHTrow);
                        ESP.Espere("Cadastro Tarefa 5/6");
                        foreach (dAutomacaoBancaria.RHTarefaDetalheRow RHDrow in RHDgravarF)
                            dAutomacaoBancaria.RHTarefaDetalheTableAdapterF.Update(RHDrow);
                        ESP.Espere("Cadastro Tarefa 6/6");
                        foreach (dAutomacaoBancaria.RHTarefaSubdetalheRow RHSrow in RHSgravarF)
                            dAutomacaoBancaria.RHTarefaSubdetalheTableAdapterF.Update(RHSrow);
                        ESP.Espere("Cadastrando Pagamentos");
                        VirMSSQL.TableAdapter.CommitSQL(VirDB.Bancovirtual.TiposDeBanco.Filial);
                    }
                    catch (Exception e1)
                    {
                        VirMSSQL.TableAdapter.VircatchSQL(e1, VirDB.Bancovirtual.TiposDeBanco.Filial);
                        throw new Exception("Erro na gravação:" + e1.Message, e1);
                    }
                    //}
                    ESP.Espere("Cadastrando Pagamentos");
                    if (CadastraNotasCheques(ESP, dateDataCheque.Visible ? dateDataCheque.DateTime : (DateTime?)null, dateCreditoConta.Visible ? dateCreditoConta.DateTime : (DateTime?)null))
                    {
                        if (rowmaeRHT != null)
                            EmiteChequesSalarioEletronico(VirDB.Bancovirtual.TiposDeBanco.SQL, rowmaeRHT.RHT);
                        if (rowmaeRHTF != null)
                            EmiteChequesSalarioEletronico(VirDB.Bancovirtual.TiposDeBanco.Filial, rowmaeRHTF.RHT);
                        ESP.Espere("Gerando arquivo Bradesco");
                        Application.DoEvents();
                        RemessaCreditoContaBradesco(dateCreditoConta.DateTime);
                        ESP.Espere("Gerando arquivo CAIXA");
                        Application.DoEvents();
                        RemessaCreditoCaixa(dateCreditoConta.DateTime);
                        MessageBox.Show("Geração do(s) arquivo(s) de remessa concluída.", "Automação Bancária", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        if (Chamador != null)
                            Chamador.CarregaTarefas(true, ConteudoTotal != null && ConteudoTotal != "");
                        FechaTela(DialogResult.OK);
                    }
                }
                catch (Exception Erro)
                {
                    btnRemessaPagamentos.Enabled = true;
                    throw new Exception(Erro.Message, Erro);
                }
                finally
                {
                    ViewPagamentos.EndDataUpdate();
                    gridViewPagCond.EndDataUpdate();
                    gridViewPagCondSdet.EndDataUpdate();
                }
            }
        }
        private void gridViewPagCondSdet_FocusedRowChanged(object sender, DevExpress.XtraGrid.Views.Base.FocusedRowChangedEventArgs e)
        {
            if (e.FocusedRowHandle >= 0)
            {
                DevExpress.XtraGrid.Views.Grid.GridView GV = (DevExpress.XtraGrid.Views.Grid.GridView)sender;
                dAutomacaoBancaria.RHTarefaSubdetalheRow rowRHS = (dAutomacaoBancaria.RHTarefaSubdetalheRow)GV.GetDataRow(e.FocusedRowHandle);
                if (rowRHS == null)
                    return;
                GV.Columns["RHSValor"].OptionsColumn.ReadOnly = !rowRHS.IsRHS_CHENull();
            }
        }

        private void Limpazeros()
        {
            bool ApagarMae;
            List<dAutomacaoBancaria.RHTarefaDetalheRow> ApagarD = new List<dAutomacaoBancaria.RHTarefaDetalheRow>();
            List<dAutomacaoBancaria.RHTarefaSubdetalheRow> ApagarS = new List<dAutomacaoBancaria.RHTarefaSubdetalheRow>();
            foreach (int EMPX in new int[] { DSCentral.EMP, DSCentral.EMPF })
            {
                if (rowmaeRHTX(EMPX) == null)
                    continue;
                ApagarMae = true;
                ApagarD.Clear();
                ApagarS.Clear();
                foreach (dAutomacaoBancaria.RHTarefaDetalheRow rowD in dAutomacaoBancaria.RHTarefaDetalhe)
                    if (rowD.EMP == EMPX)
                    {
                        bool ApagarrowD = true;
                        foreach (dAutomacaoBancaria.RHTarefaSubdetalheRow rowS in rowD.GetRHTarefaSubdetalheRows())
                            if ((rowS.RHSValor == 0) || (rowS.RHSTipoPag == (int)nTipoPag.NaoEmitir))
                                ApagarS.Add(rowS);
                            else
                            {
                                ApagarrowD = false;
                                ApagarMae = false;
                            }
                        if (ApagarrowD)
                            ApagarD.Add(rowD);
                    }
                foreach (dAutomacaoBancaria.RHTarefaSubdetalheRow rowS in ApagarS)
                {
                    rowS.Delete();
                    dAutomacaoBancaria.RHTarefaSubdetalheTableAdapterX(EMPX).Update(rowS);
                }
                foreach (dAutomacaoBancaria.RHTarefaDetalheRow rowD in ApagarD)
                {
                    rowD.Delete();
                    dAutomacaoBancaria.RHTarefaDetalheTableAdapterX(EMPX).Update(rowD);
                }
                if (ApagarMae)
                {
                    rowmaeRHTX(EMPX).Delete();
                    dAutomacaoBancaria.RHTarefaTableAdapterX(EMPX).Update(rowmaeRHTX(EMPX));
                    if (EMPX == DSCentral.EMP)
                        rowmaeRHT = null;
                    else
                        rowmaeRHTF = null;
                }
            }
        }

        private void RemessaCreditoCaixa(DateTime DataParaDebito)
        {
            /*** ARQUIVO CAIXA - CREDITO EM C/C ***/
            /*
            String Diretorio = @"J:\Virtual\Credito Conta Caixa\" + DateTime.Now.ToString("MMMyy") + "\\";

            if (!ExisteDiretorio(Diretorio))
            {                                
                Diretorio = @"C:\Virtual\Credito Conta Caixa\" + DateTime.Now.ToString("MMMyy") + "\\";
                if (!ExisteDiretorio(Diretorio))
                    return;
            }

            if (!ExisteDiretorio(Diretorio))
                return;

            String Arquivo = string.Format("{0}Remessa{1:ddMM}_1{2}.txt", Diretorio, DateTime.Today, CompontesBasicos.FormPrincipalBase.strEmProducao ? "" : "TESTE");  

            int idFile = 1;
            while (File.Exists(Arquivo))
                Arquivo = string.Format("{0}Remessa{1:ddMM}_{2}{3}.txt", Diretorio, DateTime.Today, ++idFile, CompontesBasicos.FormPrincipalBase.strEmProducao ? "" : "TESTE");  
            
            
            this.salariosBindingSource.RemoveFilter();
            this.salariosBindingSource.RemoveSort();

            this.salariosBindingSource.Filter = "(CONBanco='104') And ((FUNTipoConta='CRÉDITO C/C') or (FUNTipoConta='CONTA SALÁRIO'))  and (FUNValor > 0)";
            //this.salariosBindingSource.Filter = "CONBanco='104'";
            this.salariosBindingSource.Sort = "CONCodigo ASC,FUNTipoConta ASC";

            if (this.salariosBindingSource.Count > 0)
            {                
                this.labPagamentos.Text = "Gerando remessa de CRÉDITO C/C CAIXA";
                this.pgbPagamentos.Value = 0;

                this.labPagamentos.Visible = true;
                this.pgbPagamentos.Visible = true;

                this.pgbPagamentos.Maximum = this.salariosBindingSource.Count;

                try
                {
                    String ultimoCODCON = "";
                    CaixaSalarioRem RemessaCAIXA = new CaixaSalarioRem();
                    RemessaCAIXA.EmTeste = !CompontesBasicos.FormPrincipalBase.strEmProducao;
                    RemessaCAIXA.EmTeste = true;



                    RemessaCAIXA.IniciaCabecalho(cnpjEmissor, convenioCAIXA, ParametroTr, AgenciaCAIXA, AgenciaCAIXADV, OperacaoContaCAIXA, ContaCAIXA, ContaCAIXADV,
                                               "Neon Imoveis", idFile, idFile.ToString());
                     

                    for (Int32 iPosicao = 0; iPosicao < this.salariosBindingSource.Count; iPosicao++)
                    {
                        this.pgbPagamentos.Value++;
                        this.salariosBindingSource.Position = iPosicao;

                        DataRowView drvCONTA = ((DataRowView)(this.salariosBindingSource.Current));
                        dAutomacaoBancaria.SalariosRow RowSalarios = (dAutomacaoBancaria.SalariosRow)drvCONTA.Row;

                        if (ultimoCODCON != RowSalarios.CONCodigo.ToString())
                        {
                            ultimoCODCON = RowSalarios.CONCodigo.ToString();
                            RemessaCAIXA.AbreLote(new DocBacarios.CPFCNPJ(RowSalarios.CONCNPJ), CodigoCompromisso, int.Parse(RowSalarios.CONAgencia), int.Parse(RowSalarios.CONDigitoAgencia),
                                                OperacaoContaCAIXA, int.Parse(RowSalarios.CONConta), RowSalarios.CONDigitoAgencia, RowSalarios.CONNome, "", 0, "", 0, 0, "SP");                             
                        }

                        string manobraConta = "";
                        for (int i = 0; i < RowSalarios.FUNConta.Length - 2; i++)
                            if (char.IsDigit(RowSalarios.FUNConta[i]))
                                manobraConta = manobraConta + RowSalarios.FUNConta[i];

                        int Agencia = int.Parse(RowSalarios.FUNAgencia.Substring(0, 4));//int.Parse(RowSalarios.FUNAgencia.Substring(0,RowSalarios.FUNAgencia.Length-1));
                        int AgenciaDV = int.Parse(RowSalarios.FUNAgencia.Substring(RowSalarios.FUNAgencia.Length-1));
                        int Conta = int.Parse(manobraConta);//int.Parse(RowSalarios.FUNConta.Substring(0,RowSalarios.FUNConta.Length-1));
                        string ContaDV = RowSalarios.FUNConta.Substring(RowSalarios.FUNConta.Length-1);

                        DocBacarios.CPFCNPJ CPFDEB = new DocBacarios.CPFCNPJ(RowSalarios.FUNCPF);
                        if (CPFDEB.Tipo == DocBacarios.TipoCpfCnpj.INVALIDO)
                            MessageBox.Show("CPF inválido:" + RowSalarios.FUNCPF);
                        if (CPFDEB.Tipo == DocBacarios.TipoCpfCnpj.CNPJ)
                            MessageBox.Show("Crédito para CNPJ:" + RowSalarios.FUNCPF);

                        RemessaCAIXA.incluipagamento(RowSalarios.FUNIDBanco, Agencia, AgenciaDV, Conta, ContaDV, RowSalarios.FUNNome, DataParaDebito,
                                                     RowSalarios.FUNValor, CPFDEB, "", 0, "", 0, 0, "SP");                                                                     
                    }
                    RemessaCAIXA.GravaArquivo(Arquivo);


                }
                catch (Exception ex)
                {
                    MessageBox.Show("erro na gravação do arquivo da caixa");
                    throw ex;
                }
                finally
                {
                    this.labPagamentos.Visible = false;
                    this.pgbPagamentos.Visible = false;
                }
            }

            this.salariosBindingSource.RemoveFilter();
            this.salariosBindingSource.RemoveSort();
            */
        }

        private dAutomacaoBancaria.RHTarefaRow rowmaeRHTX(int EMPX)
        {
            return EMPX == DSCentral.EMP ? rowmaeRHT : rowmaeRHTF;
        }

        private void simpleButton2_Click(object sender, EventArgs e)
        {
            if (MessageBox.Show("Confirma o fechamento da tela", "Confirmação", MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button2) == DialogResult.Yes)
            {
                if (Chamador != null)
                    Chamador.CarregaTarefas(true, ConteudoTotal != null && ConteudoTotal != "");
                FechaTela(DialogResult.Cancel);
            }
        }

        private decimal SomaRHDValor(dAutomacaoBancaria.RHTarefaDetalheRow RHDrow)
        {
            RHDrow.Total = 0;
            foreach (dAutomacaoBancaria.RHTarefaSubdetalheRow RHSrow in RHDrow.GetRHTarefaSubdetalheRows())
                RHDrow.Total += RHSrow.RHSValor;
            return RHDrow.Total;
        }

        private void TrocaPLA(object sender, EventArgs e)
        {
            if (!SemafaroPLA)
                try
                {
                    string NovoPLA = string.Empty;
                    SemafaroPLA = true;
                    if (sender == lookUpEditPLA1)
                    {
                        lookUpEditPLA2.EditValue = lookUpEditPLA1.EditValue;
                        NovoPLA = lookUpEditPLA1.EditValue.ToString();
                    }
                    if (sender == lookUpEditPLA2)
                    {
                        lookUpEditPLA1.EditValue = lookUpEditPLA2.EditValue;
                        NovoPLA = lookUpEditPLA2.EditValue.ToString();
                    }
                    if (NovoPLA != string.Empty)
                    {
                        if ((rowmaeRHT != null) && ((rowmaeRHT["RHT_PLA"] == DBNull.Value) || (rowmaeRHT.RHT_PLA != NovoPLA)))
                            rowmaeRHT.RHT_PLA = NovoPLA;
                        if ((rowmaeRHTF != null) && ((rowmaeRHTF["RHT_PLA"] == DBNull.Value) || (rowmaeRHTF.RHT_PLA != NovoPLA)))
                            rowmaeRHTF.RHT_PLA = NovoPLA;
                    }
                }
                finally
                {
                    SemafaroPLA = false;
                }
        }

        private SortedList<int, bool> EletronicosLocal;
        private SortedList<int, bool> EletronicosFilial;

        /// <summary>
        /// Esta função checa se são permitidos eletrônico o ideal seria o objeto condomínio
        /// </summary>
        /// <param name="EMP"></param>
        /// <param name="CON"></param>
        /// <returns></returns>
        private bool PermitePagamentoEletronico(int EMP,int CON)
        {
            bool Local = DSCentral.EMP == EMP;
            SortedList<int, bool> EletronicosX;
            string comandoBusca = "SELECT TOP (1) CASE WHEN (CONCodigoComunicacao IS NOT NULL) AND (CONCodigoComunicacao <> '') AND (CCTPrincipal = 1) THEN CCTPagamentoEletronico ELSE 0 END AS PagamentoEletronico FROM CONDOMINIOS INNER JOIN ContaCorrenTe ON CONDOMINIOS.CON = ContaCorrenTe.CCT_CON WHERE (((SELECT CONTipoContaPE FROM CONDOMINIOS WHERE CON = @P1) IN (0,1)) AND (CON = @P1))OR (((SELECT CONTipoContaPE FROM CONDOMINIOS WHERE CON = @P1) IN (2)) AND (CCT_CON = 409)) ORDER BY CCTPrincipal DESC";
            if (Local)            
                EletronicosX = (EletronicosLocal == null) ? EletronicosLocal = new SortedList<int, bool>() : EletronicosLocal;
            else
                EletronicosX = (EletronicosFilial == null) ? EletronicosFilial = new SortedList<int, bool>() : EletronicosFilial;
            if (!EletronicosX.ContainsKey(CON))
            {
                bool? booPagamentoEletronico = VirMSSQL.TableAdapter.ST(Local ? VirDB.Bancovirtual.TiposDeBanco.SQL : VirDB.Bancovirtual.TiposDeBanco.Filial).BuscaEscalar_bool(comandoBusca, CON);
                EletronicosX.Add(CON, booPagamentoEletronico.GetValueOrDefault(false));
            }                
            return EletronicosX[CON];
        }

        private bool ValidaPagamento(dAutomacaoBancaria.RHTarefaSubdetalheRow RHSrow)
        {
            DataRowState RowStateOriginal = RHSrow.RowState;
            RHSrow.strStatus = "ok";
            nTipoPag nTipoPag = (nTipoPag)RHSrow.RHSTipoPag;
            dAutomacaoBancaria.FUnCionarioRow FUCrow = dAutomacaoBancaria.FUnCionario.FindByFUC(RHSrow.RHS_FUC);
            if (RHSrow.RHSValor < 0)
                RHSrow.strStatus = "X Valor negativo";
            //nErros++;
            else
                switch (nTipoPag)
                {
                    case nTipoPag.CreditoConta:
                        if (RHSrow.RHTarefaDetalheRow.IsCONRazaoCCNull())
                            RHSrow.strStatus = "! Falta código do banco no cadastro do condomínio";
                        nCreditoConta++;
                        LCreditoConta.Visible = dateCreditoConta.Visible = true;
                        break;
                    case nTipoPag.Cheque:
                        nCheques++;
                        LDataCheque.Visible = dateDataCheque.Visible = true;
                        break;
                    case nTipoPag.Invalido:
                        nErros++;
                        RHSrow.strStatus = "X Tipo de pagamento inválido";
                        break;
                    case nTipoPag.NaoEmitir:
                        break;
                    case nTipoPag.ChequeEletronico:
                        nChequeEletronio++;                        
                        if(!PermitePagamentoEletronico(RHSrow.EMP, RHSrow.RHTarefaDetalheRow.RHD_CON))
                            RHSrow.strStatus = "X Conta / condomínio não configurado para este tipo de pagamento";
                        LDataCheque.Visible = dateDataCheque.Visible = true;
                        break;
                    default:
                        throw new NotImplementedException(string.Format("cAutomacaoBancaria.cs 3043 Tipo não tratado: {0}", nTipoPag));
                }
            if (RowStateOriginal == DataRowState.Unchanged)
                RHSrow.AcceptChanges();
            return RHSrow.strStatus == "ok";
        }

        private void ValidaPagamentos()
        {
            nErros = nCheques = nCreditoConta = nChequeEletronio = 0;
            LDataCheque.Visible = dateDataCheque.Visible = false;
            LCreditoConta.Visible = dateCreditoConta.Visible = false;
            dateDataCheque.BackColor = Color.White;
            dateCreditoConta.BackColor = Color.White;
            foreach (dAutomacaoBancaria.RHTarefaSubdetalheRow RHSrow in dAutomacaoBancaria.RHTarefaSubdetalhe)
                ValidaPagamento(RHSrow);
        }

        private void ViewPagamentos_FocusedRowChanged(object sender, DevExpress.XtraGrid.Views.Base.FocusedRowChangedEventArgs e)
        {
            if (e.FocusedRowHandle >= 0)
            {
                dAutomacaoBancaria.RHTarefaSubdetalheRow rowRHS = (dAutomacaoBancaria.RHTarefaSubdetalheRow)ViewPagamentos.GetDataRow(e.FocusedRowHandle);
                if (rowRHS == null)
                    return;
                colFUNValor.OptionsColumn.ReadOnly = colFUNnTipoPag.OptionsColumn.ReadOnly = !rowRHS.IsRHS_CHENull();
            }

        }
        private void ViewPagamentos_RowUpdated(object sender, DevExpress.XtraGrid.Views.Base.RowObjectEventArgs e)
        {
            if (e == null)
                return;
            if (e.RowHandle < 0)
                return;
            dAutomacaoBancaria.RHTarefaSubdetalheRow RHSrow = (dAutomacaoBancaria.RHTarefaSubdetalheRow)ViewPagamentos.GetDataRow(e.RowHandle);
            ValidaPagamento(RHSrow);
            dAutomacaoBancaria.RHTarefaSubdetalheTableAdapterX(RHSrow.EMP).Update(RHSrow);
            SomaRHDValor(RHSrow.RHTarefaDetalheRow);
        }

        private bool FechamentoCompetencia
        {
            get
            {
                if (!fechamentoCompetencia.HasValue)
                    fechamentoCompetencia = MessageBox.Show("Carga final? Recalcular os periódicos?", "Recalcular periódicos", MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button2) == DialogResult.Yes;
                return fechamentoCompetencia.Value;
            }
        }

        private TipoPagPeriodico? TipoPagPeriodico1
        {
            get
            {
                switch (tipoBancaria)
                {
                    case TipoBancaria.Pagamento:
                        lookUpEditPLA1.EditValue = "220001";
                        lookUpEditPLA1.Enabled = lookUpEditPLA2.Enabled = false;
                        return TipoPagPeriodico.Folha;
                    case TipoBancaria.DecimoTerceiro:
                        lookUpEditPLA1.EditValue = "220017";
                        lookUpEditPLA1.Enabled = lookUpEditPLA2.Enabled = false;
                        return null;
                    case TipoBancaria.Avulso:
                        lookUpEditPLA1.EditValue = "207999";
                        lookUpEditPLA1.Enabled = lookUpEditPLA2.Enabled = true;
                        return null;
                    case TipoBancaria.Adiantamento:
                        lookUpEditPLA1.EditValue = "220002";
                        lookUpEditPLA1.Enabled = lookUpEditPLA2.Enabled = false;
                        return TipoPagPeriodico.Adiantamento;
                    default:
                        throw new NotImplementedException(string.Format("Não tratado cAutomacaoBancaria.cs 320: {0}", tipoBancaria));
                }
            }
        }

        private class virEnumtipoconta : dllVirEnum.VirEnum
        {

            private static virEnumtipoconta _virEnumtipocontaSt;

            /// <summary>
            /// Construtor padrao
            /// </summary>
            /// <param name="Tipo">Tipo</param>
            public virEnumtipoconta(Type Tipo) :
                base(Tipo)
            {
            }

            /// <summary>
            /// VirEnum estático para campo tipoconta
            /// </summary>
            public static virEnumtipoconta virEnumtipocontaSt
            {
                get
                {
                    if (_virEnumtipocontaSt == null)
                    {
                        _virEnumtipocontaSt = new virEnumtipoconta(typeof(tipoconta));
                        _virEnumtipocontaSt.GravaNomes(tipoconta.ContaCorrente, "CRÉDITO C/C", Color.Lime);
                        _virEnumtipocontaSt.GravaNomes(tipoconta.ContaSalario, "CONTA SALÁRIO", Color.Aquamarine);
                        _virEnumtipocontaSt.GravaNomes(tipoconta.Poupanca, "CRÉDITO POUPANÇA", Color.Yellow);
                        _virEnumtipocontaSt.GravaNomes(tipoconta.invalida, "FORMATO INVÁLIDO", Color.Red);
                    }
                    return _virEnumtipocontaSt;
                }
            }
        }

        #region Construtores
        /// <summary>
        /// Construtor
        /// </summary>
        public cFolhaPag(cAutomacaoBancaria _Chamador)
        {
            InitializeComponent();
            dAutomacaoBancaria.VirEnumnTipoPag.CarregaEditorDaGrid(colFUNnTipoPag);
            virEnumTipoBancaria.virEnumTipoBancariaSt.CarregaEditorDaGrid(imageComboTipo);
            pLAnocontasBindingSource.DataSource = Framework.datasets.dPLAnocontas.dPLAnocontasSt25.PLAnocontas;
            virEnumtipoconta.virEnumtipocontaSt.CarregaEditorDaGrid(colFUNTipoConta);
            Enumeracoes.virEnumCHEStatus.virEnumCHEStatusSt.CarregaEditorDaGrid(colCHEStatus);
            Chamador = _Chamador;
        }

        /// <summary>
        /// Construtor
        /// </summary>
        /// <param name="EMP"></param>
        /// <param name="RHT"></param>
        /// <param name="_Chamador"></param>
        /// <param name="ESP"></param>
        public cFolhaPag(int EMP, int RHT, cAutomacaoBancaria _Chamador, CompontesBasicos.Espera.cEspera ESP = null) : this(_Chamador)
        {
            dAutomacaoBancaria.RHTarefaTableAdapterX(EMP).FillByRHT(dAutomacaoBancaria.RHTarefa, RHT);
            dAutomacaoBancaria.RHTarefaRow RHTrow = dAutomacaoBancaria.RHTarefa[0];
            cCompet1.Comp.CompetenciaBind = RHTrow.RHTCompetencia;
            cCompet1.AjustaTela();
            imageComboTipo.EditValue = RHTrow.RHTTipo;
            tipoBancaria = (TipoBancaria)imageComboTipo.EditValue;
            dateCreditoConta.DateTime = RHTrow.RHTDataCredito;
            dateDataCheque.DateTime = RHTrow.RHTDataCheque;
            lookUpEditPLA1.EditValue = lookUpEditPLA2.EditValue = RHTrow.RHT_PLA;
            if (RHTrow.EMP == DSCentral.EMP)
            {
                rowmaeRHT = RHTrow;
                rowmaeRHTF = null;
                dAutomacaoBancaria.RHTarefaDetalheTableAdapter.FillByRHT(dAutomacaoBancaria.RHTarefaDetalhe, RHTrow.RHT);
                dAutomacaoBancaria.RHTarefaSubdetalheTableAdapter.FillByRHT(dAutomacaoBancaria.RHTarefaSubdetalhe, RHTrow.RHT);
            }
            else
            {
                rowmaeRHTF = RHTrow;
                rowmaeRHT = null;
                dAutomacaoBancaria.RHTarefaDetalheTableAdapterF.FillByRHT(dAutomacaoBancaria.RHTarefaDetalhe, RHTrow.RHT);
                dAutomacaoBancaria.RHTarefaSubdetalheTableAdapterF.FillByRHT(dAutomacaoBancaria.RHTarefaSubdetalhe, RHTrow.RHT);
            }
            dateCreditoConta.DateTime = RHTrow.RHTDataCredito;
            dateDataCheque.DateTime = RHTrow.RHTDataCheque;
            dAutomacaoBancaria.RecarregarFuncionarios(false);
            dAutomacaoBancaria.CompletaCampos();
            ValidaPagamentos();
            CarregaDadosPeriodicos(ESP);
            if (TipoPagPeriodico1.HasValue)
            {
                EMPTProc EMPTProc1 = new EMPTProc(EMP == DSCentral.EMP ? EMPTipo.Local : EMPTipo.Filial,
                                                  EMP == DSCentral.EMP ? DSCentral.USU : DSCentral.USUFilial);
                foreach (dAutomacaoBancaria.RHTarefaDetalheRow RHDrow in dAutomacaoBancaria.RHTarefaDetalhe)
                {
                    PagamentoPeriodico per = new PagamentoPeriodico(RHDrow.RHD_CON, TipoPagPeriodico1.Value, EMPTProc1, true);
                    Periodicos.Add(RHDrow.RHD, per);
                }
                CarregaHistoricoPeriodicos();
            }

            Titulo = string.Format("{0} - {1}", RHTrow.RHTArquivo, EMP == 1 ? "SBT" : "SA");
        }
        #endregion


        #region Extração de dados
        private int Inicio;
        private int Tamanho;
        private bool Terminado;
        private int tamanhototal;
        private string ConteudoTotal;
        private string NomeArquivo;

        private string ProximaLinha(CompontesBasicos.Espera.cEspera ESP)
        {
            if (ESP != null)
                ESP.GaugeTempo(Inicio);
            if (Terminado)
                return string.Empty;
            else
            {
                Inicio += Tamanho;
                if (Inicio != 0)
                    for (; (Inicio < tamanhototal) && (ConteudoTotal[Inicio] != Char.Parse("\n")); Inicio++) ;
                else
                    Inicio = -1;
                if ((Inicio + 1) >= tamanhototal)
                {
                    Terminado = true;
                    return string.Empty;
                }
                else
                {
                    Inicio++;
                    for (Tamanho = 1; (Inicio + Tamanho < tamanhototal) && ConteudoTotal[Inicio + Tamanho] != Char.Parse("\n"); Tamanho++) ;
                    return ConteudoTotal.Substring(Inicio, Tamanho);
                };
            };

        }

        private string ProximaLinha(int linhas, CompontesBasicos.Espera.cEspera ESP = null)
        {
            for (int i = 1; i < (linhas); i++)
                ProximaLinha(ESP);
            return ProximaLinha(ESP);
        }

        private nTipoPag GetnTipoPag(string NomeBanco)
        {
            if (NomeBanco == "UNIBANCO S.A.")
                return nTipoPag.Cheque;
            else if (NomeBanco == "BRADESCO CSALARIO")
                return nTipoPag.Invalido;
            else if (NomeBanco == "BRADESCO S/A - CONTA SALARIO")
                return nTipoPag.CreditoConta;
            else if (NomeBanco == "SINDICO EMITE CHEQUE")
                return nTipoPag.Cheque;
            else if (NomeBanco == "BRADESCO CCORRENTE")
                return nTipoPag.CreditoConta;
            else if (NomeBanco == "ITAU")
                return nTipoPag.Cheque;
            else
                return nTipoPag.Invalido;
        }

        private enum tipoconta { invalida = -1, ContaCorrente = 0, ContaSalario = 1, Poupanca = 2 };

        private dAutomacaoBancaria.RHTarefaRow GetrowmaeRHTX(EMPTipo EMPTipo1) { return EMPTipo1 == EMPTipo.Local ? rowmaeRHT : rowmaeRHTF; }

        private void SetrowmaeRHTX(EMPTipo EMPTipo1, dAutomacaoBancaria.RHTarefaRow Valor)
        {
            if (EMPTipo1 == EMPTipo.Local)
                rowmaeRHT = Valor;
            else
                rowmaeRHTF = Valor;
        }

        private dAutomacaoBancaria.RHTarefaRow NovarowmaeRHT(int EMP, Competencia comp)
        {
            EMPTipo EMPTipo1 = EMP == DSCentral.EMP ? EMPTipo.Local : EMPTipo.Filial;
            if (GetrowmaeRHTX(EMPTipo1) == null)
            {
                dAutomacaoBancaria.RHTarefaRow Novarow = EMPTipo1 == EMPTipo.Local ? dAutomacaoBancaria.RHTarefa.NewRHTarefaRow() : dAutomacaoBancaria.RHTarefa.NewRHTarefaRow();
                Novarow.EMP = EMPTipo1 == EMPTipo.Local ? Framework.DSCentral.EMP : Framework.DSCentral.EMPF;
                Novarow.RHTCompetencia = comp.CompetenciaBind;
                Novarow.RHTDataCheque = DateTime.Today;
                Novarow.RHTDataCredito = DateTime.Today;
                Novarow.RHT_PLA = lookUpEditPLA1.EditValue.ToString();
                Novarow.RHTArquivo = NomeArquivo;
                Novarow.RHTDataI = DateTime.Now;
                Novarow.RHTI_USU = DSCentral.USUX(EMPTipo1);
                Novarow.RHTStatus = (int)RHTStatus.Cadastrada;
                Novarow.RHTTipo = (int)tipoBancaria;
                dAutomacaoBancaria.RHTarefa.AddRHTarefaRow(Novarow);
                if (EMPTipo1 == EMPTipo.Local)
                    dAutomacaoBancaria.RHTarefaTableAdapter.Update(Novarow);
                else
                    dAutomacaoBancaria.RHTarefaTableAdapterF.Update(Novarow);
                SetrowmaeRHTX(EMPTipo1, Novarow);
            }
            return GetrowmaeRHTX(EMPTipo1);
        }

        /// <summary>
        /// Lê os dados do RTF e carrega na tabela (RAM) Salários. Cuidado com os campos PGF__EMP e CON__EMP que tem códigos da tabela Local/Filial
        /// </summary>
        internal bool ExtrairPagamentos(Control Chamador, string Titulo, string _ConteudoTotal)
        {
            cDadosParaFolhaPag DadosEntrada = new cDadosParaFolhaPag();
            if (DadosEntrada.ShowEmPopUp() != DialogResult.OK)
                return false;
            TipoBancaria? TipoSelecionado = DadosEntrada.Selecionado;
            if (!TipoSelecionado.HasValue)
                return false;
            fechamentoCompetencia = DadosEntrada.Final;
            //Competencia comp = DadosEntrada.cCompet1.Comp;
            Competencia comp = null;
            //SortedList Alternativas = new SortedList();
            botaoZerar.Visible = true;
            //Alternativas.Add(TipoBancaria.Pagamento, "Pagamento");
            //Alternativas.Add(TipoBancaria.Adiantamento, "Adiantamento");
            //Alternativas.Add(TipoBancaria.DecimoTerceiro, "décimo terceiro");
            //Alternativas.Add(TipoBancaria.Avulso, "Avulso");
            //object Selecionado;
            //if (!VirInput.Input.Execute("Tipo de pagamento", Alternativas, out Selecionado, VirInput.Formularios.cRadioCh.TipoSelecao.radio))
            //    return false;

            imageComboTipo.EditValue = (int)TipoSelecionado.Value;
            tipoBancaria = TipoSelecionado.Value;
            this.Titulo = NomeArquivo = Titulo;
            ConteudoTotal = _ConteudoTotal;
            try
            {
                using (CompontesBasicos.Espera.cEspera ESP = new CompontesBasicos.Espera.cEspera(Chamador))
                {
                    Application.DoEvents();
                    ESP.Espere("Carregando condomínios");
                    dAutomacaoBancaria.RecarregarCondominios();
                    ESP.Espere("Carregando funcionários");
                    dAutomacaoBancaria.RecarregarFuncionarios(true);
                }
                dAutomacaoBancaria.RHTarefaSubdetalhe.Clear();
                dAutomacaoBancaria.RHTarefaDetalhe.Clear();

                rowmaeRHT = rowmaeRHTF = null;
                bool NaoTemBanco = true;
                String sLinha = string.Empty;
                String sLinhaBusca;
                String sLinhadet = string.Empty;
                String sLinhaag = string.Empty;
                Int32 FUNIDBanco;
                String FUNBanco;
                string FUNAgencia; //formato 0000-0
                string TipoBanco;
                nTipoPag nTipoPag;
                TipoBancaria tipoBancariaRTF = TipoBancaria.Avulso;
                Periodicos = new SortedList<int, PagamentoPeriodico>();
                this.ViewPagamentos.BeginDataUpdate();
                tamanhototal = ConteudoTotal.Length;
                if (ConteudoTotal.Contains("A DE PAGAMENTO\n"))
                    tipoBancariaRTF = TipoBancaria.Pagamento;
                else if (ConteudoTotal.Contains("A DE ADIANTAMENTO\n"))
                    tipoBancariaRTF = TipoBancaria.Adiantamento;
                else if (ConteudoTotal.Contains("A DE 13"))
                    tipoBancariaRTF = TipoBancaria.DecimoTerceiro;
                else
                {
                    MessageBox.Show("Bancária não identificada!!\r\nTratada como Avulsa");
                    tipoBancariaRTF = TipoBancaria.Avulso;
                };

                if (tipoBancaria != tipoBancariaRTF)
                    if (MessageBox.Show("Arquivo RTF com tipo diferente do informado\r\nConfirma assim mesmo ?", "AVISO", MessageBoxButtons.YesNo, MessageBoxIcon.Warning, MessageBoxDefaultButton.Button2) == DialogResult.No)
                        return false;
                imageComboTipo.EditValue = (int)tipoBancaria;
                Inicio = 0;
                Tamanho = 0;
                Terminado = false;



                using (CompontesBasicos.Espera.cEspera ESP = new CompontesBasicos.Espera.cEspera(Chamador))
                {
                    //if (!CarregaDadosPeriodicos(ESP))
                    //    return false;
                    ESP.Espere("Carregando arquivo");
                    ESP.AtivaGauge(tamanhototal);
                    Application.DoEvents();
                    ESP.GaugeTempo(0);
                    if (ESP.Abortar)
                        return false;
                    while (!Terminado)
                    {
                        ESP.GaugeTempo(Inicio);
                        if (ESP.Abortar)
                            return false;
                        sLinha = ProximaLinha(ESP);

                        //Competencia
                        if ((comp == null) && sLinha.Contains("DATAMACE") && sLinha.Contains("G.I.P.") && sLinha.Contains("REF:"))
                        {
                            int mes;
                            int ano;
                            int i = sLinha.IndexOf("REF:");
                            if (int.TryParse(sLinha.Substring(i + 4, 2), out mes) && (int.TryParse(sLinha.Substring(i + 7, 4), out ano)))
                            {
                                //if (new Competencia(mes, ano) != comp)
                                //    if (MessageBox.Show("ATENÇÃO: competência indicada diferente do arquivo !!\r\nConfirma ?","ATENÇÃO",MessageBoxButtons.YesNo,MessageBoxIcon.Warning,MessageBoxDefaultButton.Button2) == DialogResult.No)
                                //        return false;
                                comp = InputCompetencia.stInput(new Competencia(mes, ano));
                                if (comp == null)
                                    return false;
                                cCompet1.Comp = comp;
                                cCompet1.AjustaTela();
                                CarregaDadosPeriodicos(ESP);
                            }
                        }

                        //Cabecalho Relatório
                        if ((sLinha.IndexOf("RELACAO BANCARIA / CAIXA") >= 0)
                              ||
                            (sLinha.IndexOf("RELAÇÃO BANCÁRIA / CAIXA") >= 0)
                              ||
                            (sLinha.IndexOf("RELA++O BANC+RIA / CAIXA") >= 0)
                            )
                        {
                            if (comp == null)
                            {
                                comp = InputCompetencia.stInput();
                                if (comp == null)
                                    return false;
                                cCompet1.Comp = comp;
                                cCompet1.AjustaTela();
                                CarregaDadosPeriodicos(ESP);
                            }
                            //Verificando se existem funcionários na pagina do relatório
                            bool ProximoBanco = false;
                            while ((!ProximoBanco) && (!Terminado))
                            {
                                sLinhaag = string.Empty;
                                NaoTemBanco = false;
                                do
                                {
                                    sLinhaBusca = ProximaLinha(ESP);
                                    if (sLinhaBusca.IndexOf("REGISTRO NOME") >= 0)
                                    {
                                        NaoTemBanco = true;
                                        ProximoBanco = false;
                                        sLinhaBusca = ProximaLinha(ESP);
                                    }
                                    else
                                        if ((sLinhaBusca.IndexOf("AGENCIA:") >= 0)
                                              ||
                                            (sLinhaBusca.IndexOf("AGÊNCIA:") >= 0)
                                              ||
                                            (sLinhaBusca.IndexOf("AG+NCIA:") >= 0)
                                           )
                                    {
                                        if (sLinhaBusca.Contains("BANCO: 00000-NAO CADASTRADO")
                                              ||
                                            sLinhaBusca.Contains("BANCO: 00000-NÃO CADASTRADO")
                                              ||
                                            sLinhaBusca.Contains("BANCO: 00000-N+O CADASTRADO")
                                           )
                                        {
                                            NaoTemBanco = true;
                                            ProximoBanco = false;
                                        }
                                        else
                                        {
                                            sLinhaag = sLinhaBusca;
                                            NaoTemBanco = false;
                                        }
                                        do
                                            sLinhaBusca = ProximaLinha(ESP);
                                        while ((sLinhaBusca.IndexOf("REGISTRO NOME                              C.CORRENTE   C.P.F.           VALOR") != 0)
                                                  &&
                                                 (sLinhaBusca.IndexOf("REGISTRO NOME                               C O N T A   C.P.F.           VALOR") != 0)
                                                  &&
                                                 (sLinhaBusca.IndexOf("REGISTRO NOME                              C O N T A    C.P.F.            VALOR") != 0)
                                                  &&
                                                 (sLinhaBusca.IndexOf("REGISTRO NOME                              C O N T A    C.P.F.             VALOR") != 0)
                                                  &&
                                                 (!Terminado));
                                        sLinhaBusca = ProximaLinha(ESP);
                                    }
;
                                    if ((sLinhaBusca.IndexOf("TOTAL DESTE BANCO") >= 0) || (sLinhaBusca.IndexOf("TOTAL DA EMPRESA") >= 0))
                                        ProximoBanco = true;
                                } while ((!NaoTemBanco) && (sLinhaag == string.Empty) && (!ProximoBanco) && (!Terminado));


                                if ((ProximoBanco) || (Terminado))
                                    sLinha = ProximaLinha(ESP);
                                else
                                {

                                    sLinhadet = ProximaLinha(ESP);

                                    if (sLinhadet.Trim() != string.Empty)
                                    {
                                        //Localizando registro do condomínio para colher informações
                                        string CONCodigoFolha2A = sLinha.Substring(49, 3);
                                        string CadastroFolha = sLinha.Substring(49);
                                        if (CONCodigoFolha2A == " - ") //modelo antigo
                                        {
                                            CONCodigoFolha2A = sLinha.Substring(46, 3);
                                            CadastroFolha = sLinha.Substring(46);
                                        }
                                        int CONCodigoFolha = 0;
                                        if (!int.TryParse(CONCodigoFolha2A, out CONCodigoFolha))
                                            CONCodigoFolha = -1;

                                        dAutomacaoBancaria.CONDOMINIOSRow rowCON = dAutomacaoBancaria.BuscaCondominio(CONCodigoFolha, CONCodigoFolha2A, true, CadastroFolha);




                                        if (rowCON != null)
                                        {
                                            EMPTProc EMPTProc1 = new EMPTProc(rowCON.CON_EMP == DSCentral.EMP ? EMPTipo.Local : EMPTipo.Filial,
                                                                                            rowCON.CON_EMP == DSCentral.EMP ? Framework.DSCentral.USU : Framework.DSCentral.USUFilial);

                                            if (rowCON.IsCONAgenciaNull())
                                            {
                                                MessageBox.Show(string.Format("Erro fatal: Cadastro de condomínio incompleto (agência)\r\nCondomínio: {0}", rowCON.CONCodigo), "Automação Bancária", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                                                return false;
                                            }
                                            if (rowCON.IsCONCnpjNull())
                                            {
                                                MessageBox.Show(string.Format("Erro fatal: Cadastro de condomínio incompleto (CNPJ)\r\nCondomínio: {0}", rowCON.CONCodigo), "Automação Bancária", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                                                return false;
                                            }

                                            //Dados do Condomínio                                            
                                            string CodFolha = rowCON.CONCodigoFolha1.ToString().PadLeft(3, '0');

                                            //Dados do Funcionário
                                            if (NaoTemBanco)
                                            {
                                                FUNIDBanco = 0;
                                                FUNBanco = FUNAgencia = TipoBanco = " ";
                                                nTipoPag = nTipoPag.Cheque;
                                            }
                                            else
                                            {
                                                sLinhaag = sLinhaag.ToUpper();
                                                FUNIDBanco = Convert.ToInt32(sLinhaag.Substring(7, 5));
                                                FUNBanco = FUNIDBanco.ToString() + "-" + sLinhaag.Substring(13, sLinhaag.IndexOf(" ", 13)).Trim();
                                                FUNAgencia = sLinhaag.Substring(53, 6);
                                                //correção para quando não vem o separador do digito da agencia - este problema pareceu nos RTF da datamace em 28/09/2011                                                
                                                if ((FUNAgencia[5] == ' ') && (FUNAgencia.IndexOf('-') == -1))
                                                    FUNAgencia = string.Format("{0}-{1}", FUNAgencia.Substring(0, 4), FUNAgencia.Substring(4, 1));
                                                TipoBanco = sLinhaag.Substring(13, 30).Trim().ToUpper();
                                                nTipoPag = GetnTipoPag(TipoBanco);
                                                if (nTipoPag == nTipoPag.Invalido)
                                                    MessageBox.Show("Tipo de pagamento não definido para: " + TipoBanco);
                                            }

                                            //Gerando registro de funcionário
                                            sLinhadet = sLinhadet.ToUpper();
                                            while ((sLinhadet.Substring(0, 10) != "----------") && (sLinhadet.Substring(0, 10) != "          ") && (sLinhadet.Substring(0, 10) != "\fDATAMACE "))
                                            {
                                                string FUNRegistro = sLinhadet.Substring(0, 9).Trim();
                                                String FUNNome = sLinhadet.Substring(9, 32).Trim();
                                                string FUNConta = sLinhadet.Substring(43, 13).Trim();
                                                String FUNCPF = sLinhadet.Substring(56, 11).Trim();
                                                decimal FUNValor = decimal.Parse(sLinhadet.Substring(67, 12).Trim());
                                                tipoconta FUNTipoConta = tipoconta.invalida;

                                                if ((FUNConta.IndexOf(".") == 3))// && (FUNConta.IndexOf("-") == 7))
                                                    FUNTipoConta = tipoconta.ContaCorrente;
                                                else
                                                    if ((FUNConta.IndexOf(":") == 3) && (FUNConta.IndexOf("-") == 7))
                                                    FUNTipoConta = tipoconta.ContaSalario;

                                                int RHT = NovarowmaeRHT(rowCON.CON_EMP, comp).RHT;

                                                dAutomacaoBancaria.RHTarefaDetalheRow rowRHD = dAutomacaoBancaria.Busca_rowRHD(RHT, rowCON.CON);
                                                if (rowRHD == null)
                                                {
                                                    rowRHD = dAutomacaoBancaria.RHTarefaDetalhe.NewRHTarefaDetalheRow();
                                                    rowRHD.RHD_CON = rowCON.CON;
                                                    rowRHD.RHD_RHT = RHT;
                                                    rowRHD.EMP = rowCON.CON_EMP;
                                                    rowRHD.Total = 0;
                                                    rowRHD.CONAgencia = rowCON.CONAgencia;
                                                    rowRHD.CONBanco = rowCON.CON_BCO.ToString("000");
                                                    rowRHD.CONCNPJ = rowCON.CONCnpj;
                                                    rowRHD.CONCodigo = rowCON.CONCodigo;
                                                    rowRHD.CONNome = rowCON.CONNome;
                                                    rowRHD.CONCodigoFolha1 = rowCON.CONCodigoFolha1;
                                                    rowRHD.CONConta = rowCON.CONConta;
                                                    rowRHD.CONDigitoAgencia = rowCON.CONDigitoAgencia;
                                                    rowRHD.CONDigitoConta = rowCON.CONDigitoConta;
                                                    if (!rowCON.IsCONCodigoBancoCCNull())
                                                        rowRHD.CONRazaoCC = rowCON.CONCodigoBancoCC;
                                                    dAutomacaoBancaria.RHTarefaDetalhe.AddRHTarefaDetalheRow(rowRHD);
                                                    dAutomacaoBancaria.RHTarefaDetalheTableAdapterX(EMPTProc1.Tipo).Update(rowRHD);
                                                }
                                                dAutomacaoBancaria.RHTarefaSubdetalheRow rowRHS = dAutomacaoBancaria.Busca_rowRHS();
                                                dAutomacaoBancaria.FUnCionarioRow rowFUC = dAutomacaoBancaria.Busca_rowFUnCionario(rowCON.CON_EMP, rowCON.CON, FUNRegistro);
                                                CPFCNPJ cpf = new CPFCNPJ(FUNCPF);
                                                if (rowFUC == null)
                                                {
                                                    rowFUC = dAutomacaoBancaria.FUnCionario.NewFUnCionarioRow();
                                                    rowFUC.FUC_CON = rowCON.CON;
                                                    rowFUC.FUCAgencia = FUNAgencia;
                                                    rowFUC.FUCAtivo = true;
                                                    rowFUC.FUCBanco = FUNIDBanco;
                                                    rowFUC.FUCConta = FUNConta;
                                                    if (cpf.Tipo == TipoCpfCnpj.CPF)
                                                        rowFUC.FUCCpf = cpf.Valor;
                                                    rowFUC.FUCNome = FUNNome;
                                                    rowFUC.FUCRegistro = FUNRegistro;
                                                    rowFUC.FUCTipoConta = (int)FUNTipoConta;
                                                    if (FUNIDBanco != 0)
                                                    {
                                                        //FrameworkProc.objetosNeon.ContaCorrenteNeon ContaCCG = new FrameworkProc.objetosNeon.ContaCorrenteNeon();
                                                    }
                                                }
                                                else
                                                {
                                                    if (rowFUC.FUCAgencia != FUNAgencia)
                                                        rowFUC.FUCAgencia = FUNAgencia;
                                                    if (!rowFUC.FUCAtivo)
                                                        rowFUC.FUCAtivo = true;
                                                    if (rowFUC.FUCBanco != FUNIDBanco)
                                                        rowFUC.FUCBanco = FUNIDBanco;
                                                    if (rowFUC.FUCConta != FUNConta)
                                                        rowFUC.FUCConta = FUNConta;
                                                    if ((cpf.Tipo == TipoCpfCnpj.CPF) && (rowFUC.FUCCpf != cpf.Valor))
                                                        rowFUC.FUCCpf = cpf.Valor;
                                                    if (rowFUC.FUCNome != FUNNome)
                                                        rowFUC.FUCNome = FUNNome;
                                                    if (rowFUC.FUCTipoConta != (int)FUNTipoConta)
                                                        rowFUC.FUCTipoConta = (int)FUNTipoConta;
                                                }
                                                if (rowFUC.RowState == DataRowState.Detached)
                                                    dAutomacaoBancaria.FUnCionario.AddFUnCionarioRow(rowFUC);
                                                if (rowFUC.RowState != DataRowState.Unchanged)
                                                    dAutomacaoBancaria.FUnCionarioTableAdapterX(EMPTProc1.Tipo).Update(rowFUC);
                                                if (rowRHS == null)
                                                {
                                                    rowRHS = dAutomacaoBancaria.RHTarefaSubdetalhe.NewRHTarefaSubdetalheRow();
                                                    rowRHS.EMP = rowCON.CON_EMP;
                                                    rowRHS.RHS_FUC = rowFUC.FUC;
                                                    rowRHS.RHS_RHD = rowRHD.RHD;
                                                    rowRHS.RHSTipoPag = (int)nTipoPag;
                                                    rowRHS.RHSValor = FUNValor;
                                                    rowRHS.CONNome = rowCON.CONNome;
                                                    rowRHS.RowError = string.Empty;
                                                    rowRHS.strStatus = "Carregando";
                                                    dAutomacaoBancaria.RHTarefaSubdetalhe.AddRHTarefaSubdetalheRow(rowRHS);
                                                    rowRHD.Total += FUNValor;
                                                    dAutomacaoBancaria.RHTarefaSubdetalheTableAdapterX(EMPTProc1.Tipo).Update(rowRHS);
                                                    if (TipoPagPeriodico1.HasValue)
                                                        if (!Periodicos.ContainsKey(rowRHD.RHD))
                                                        {
                                                            PagamentoPeriodico per = new PagamentoPeriodico(rowCON.CON, TipoPagPeriodico1.Value, EMPTProc1, true);
                                                            Periodicos.Add(rowRHD.RHD, per);
                                                            //Console.WriteLine(string.Format("=========> RHD = {0:00000} encontrado:{1}", rowRHD.RHD, per.Encontrado));
                                                        }
                                                }
                                                else
                                                    rowRHS.RHSValor += FUNValor;
                                                sLinhadet = string.Empty;
                                                while (sLinhadet.Length < 10)
                                                    sLinhadet = ProximaLinha(ESP).ToUpper();
                                            }

                                        }
                                        else
                                        {
                                            string men = string.Format("Não foi possível localizar o cadastro do condomínio {0}.\r\nPossivelmente este código não esteja atribuído a nenhum dos condomínios existentes.",
                                                                       CONCodigoFolha != -1 ? CONCodigoFolha.ToString() : CONCodigoFolha2A);
                                            MessageBox.Show(men, "Automação Bancária", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                                            return false;
                                        }
                                    }
                                };
                            };
                            dAutomacaoBancaria.RHTarefaTableAdapter.Update(rowmaeRHT);
                            dAutomacaoBancaria.RHTarefaTableAdapterF.Update(rowmaeRHTF);
                        }
                    };
                    ESP.Espere("Completando Campos");
                    dAutomacaoBancaria.CompletaCampos();
                    CarregaHistoricoPeriodicos();
                }
                ValidaPagamentos();
            }
            finally
            {
                ViewPagamentos.EndDataUpdate();
                ViewPagamentos.RefreshData();

            }
            return true;
        }
        #endregion


        #region Pontes
        private void gridViewPagCond_CustomRowCellEdit(object sender, DevExpress.XtraGrid.Views.Grid.CustomRowCellEditEventArgs e)
        {
            if (e.RowHandle >= 0)
            {
                if (e.Column.Name == gridColumn4Nota.Name)
                {
                    dAutomacaoBancaria.RHTarefaDetalheRow rowRHD = (dAutomacaoBancaria.RHTarefaDetalheRow)gridViewPagCond.GetDataRow(e.RowHandle);
                    if (!rowRHD.IsRHD_NOANull())
                        e.RepositoryItem = repositoryItemButtonNota1;
                }
                if (e.Column.Name == gridColumn4Periodico.Name)
                {
                    dAutomacaoBancaria.RHTarefaDetalheRow rowRHD = (dAutomacaoBancaria.RHTarefaDetalheRow)gridViewPagCond.GetDataRow(e.RowHandle);
                    if ((!rowRHD.IsNOA_PGFNull()) && (rowRHD.EMP == DSCentral.EMP))
                        e.RepositoryItem = repositoryItemButtonPeriodico1;
                }
            }
        }

        private void gridViewPagCondSdet_CustomRowCellEdit(object sender, DevExpress.XtraGrid.Views.Grid.CustomRowCellEditEventArgs e)
        {
            if (e.RowHandle >= 0)
                if (e.Column.Name == gridColumn4CHEque.Name)
                {
                    DevExpress.XtraGrid.Views.Grid.GridView GV = (DevExpress.XtraGrid.Views.Grid.GridView)sender;
                    dAutomacaoBancaria.RHTarefaSubdetalheRow rowRHS = (dAutomacaoBancaria.RHTarefaSubdetalheRow)GV.GetDataRow(e.RowHandle);
                    if (!rowRHS.IsRHS_CHENull())
                        e.RepositoryItem = repositoryItemButtonCHEQUE;
                }
        }

        private void ViewPagamentos_CustomRowCellEdit(object sender, DevExpress.XtraGrid.Views.Grid.CustomRowCellEditEventArgs e)
        {
            if (e.RowHandle >= 0)
                if (e.Column.Name == gridColumnCheque1.Name)
                {
                    dAutomacaoBancaria.RHTarefaSubdetalheRow rowRHS = (dAutomacaoBancaria.RHTarefaSubdetalheRow)ViewPagamentos.GetDataRow(e.RowHandle);
                    if ((rowRHS != null) && (!rowRHS.IsRHS_CHENull()))
                        e.RepositoryItem = repositoryItemButtonCheque1;
                }
        }

        private Font fontBold;

        private Font FontBold(Font Modelo)
        {            
                if (fontBold == null)
                    fontBold = new Font(Modelo, FontStyle.Bold | FontStyle.Italic);
                return fontBold;            
        }

        private Font fontCortado;

        private Font FontCortado(Font Modelo)
        {            
                if (fontCortado == null)
                    fontCortado = new Font(Modelo, FontStyle.Strikeout | FontStyle.Italic);
                return fontCortado;            
        }

        private void gridViewPagCond_RowCellStyle(object sender, DevExpress.XtraGrid.Views.Grid.RowCellStyleEventArgs e)
        {
            if ((e == null) || (e.RowHandle < 0) || (e.Column == null))
                return;
            dAutomacaoBancaria.RHTarefaDetalheRow row = (dAutomacaoBancaria.RHTarefaDetalheRow)gridViewPagCond.GetDataRow(e.RowHandle);
            if (row == null)
                return;
            if (e.Column == colCONNome1)
            {
                e.Appearance.BackColor2 = e.Appearance.BackColor = row.EMP == Framework.DSCentral.EMP ? System.Drawing.Color.PaleGreen : System.Drawing.Color.LemonChiffon;
                e.Appearance.ForeColor = Color.Black;
            }
            else if (e.Column == colSaldoCC)
            {
                if (!(row.IsTotalNull()) && !(row.IsValSaldoCCNull()) && (row.ValSaldoCC < row.Total))
                {
                    e.Appearance.ForeColor = Color.Red;
                    e.Appearance.Font = FontBold(e.Appearance.Font);
                }
            }
            else if (e.Column == colValorAcumulado)
            {
                if (!(row.IsValorAnteriorMedioNull()) && !(row.IsValorAcumuladoNull()))
                {
                    decimal delta = Math.Abs(row.ValorAnteriorMedio - row.ValorAcumulado);
                    if (delta > row.ValorAnteriorMedio * 0.1M)
                    {
                        e.Appearance.ForeColor = Color.Red;
                        e.Appearance.Font = FontBold(e.Appearance.Font);
                    }
                }
            }
        }

        private void ViewPagamentos_RowCellStyle(object sender, DevExpress.XtraGrid.Views.Grid.RowCellStyleEventArgs e)
        {
            if (e == null)
                return;
            if (e.RowHandle < 0)
                return;
            if (e.Column == null)
                return;
            dAutomacaoBancaria.RHTarefaSubdetalheRow row = (dAutomacaoBancaria.RHTarefaSubdetalheRow)ViewPagamentos.GetDataRow(e.RowHandle);
            if (row == null)
                return;
            if ((e.Column == colFUNTipoConta) && (!row.IsFUCTipoContaNull()))
            {
                e.Appearance.BackColor2 = e.Appearance.BackColor = virEnumtipoconta.virEnumtipocontaSt.GetCor(row.FUCTipoConta);
                e.Appearance.ForeColor = Color.Black;
            }
            else if (e.Column == colstrStatus)
            {
                if (row.strStatus.StartsWith("X"))
                    e.Appearance.BackColor2 = e.Appearance.BackColor = Color.Red;
                else if (row.strStatus.StartsWith("!"))
                    e.Appearance.BackColor2 = e.Appearance.BackColor = Color.Yellow;
                e.Appearance.ForeColor = Color.Black;
            }
            else if (e.Column == colFUNnTipoPag)
            {
                e.Appearance.BackColor2 = e.Appearance.BackColor = dAutomacaoBancaria.VirEnumnTipoPag.GetCor(row.RHSTipoPag);
                e.Appearance.ForeColor = Color.Black;
            }
            else if (e.Column == colFUNNome)
            {
                e.Appearance.BackColor2 = e.Appearance.BackColor = row.EMP == DSCentral.EMP ? Color.PaleGreen : Color.LemonChiffon;
                e.Appearance.ForeColor = Color.Black;
            }
            else if (e.Column == colCHEStatus)
            {
                nTipoPag nTipoPag = (nTipoPag)row.RHSTipoPag;
                StatusRHT Status = StatusRHT.NaoAplicavel;
                if (!row.IsCHEStatusNull())
                {
                    if (((CHEStatus)row.CHEStatus).EstaNoGrupo(CHEStatus.Compensado, CHEStatus.CompensadoAguardaCopia))
                        Status = StatusRHT.ok;
                    else if (((CHEStatus)row.CHEStatus).EstaNoGrupo(CHEStatus.Cancelado, CHEStatus.PagamentoCancelado))
                        Status = StatusRHT.cancelado;
                    else
                    {
                        if (nTipoPag == nTipoPag.Cheque)
                        {
                            DateTime DataAvisoCheque = row.RHTarefaDetalheRow.RHTarefaRow.RHTDataCheque.SomaDiasUteis(3);
                            DateTime DataLimiteCheque = row.RHTarefaDetalheRow.RHTarefaRow.RHTDataCredito.SomaDiasUteis(4);
                            DateTime DataLimiteDefCheque = row.RHTarefaDetalheRow.RHTarefaRow.RHTDataCredito.SomaDiasUteis(10);
                            if (DateTime.Today >= DataLimiteDefCheque)
                                Status = StatusRHT.vencidoDefinitivo;
                            else if (DateTime.Today >= DataLimiteCheque)
                                Status = StatusRHT.vencido;
                            else if (DateTime.Today >= DataAvisoCheque)
                                Status = StatusRHT.alerta;
                        }
                        else if (nTipoPag == nTipoPag.CreditoConta)
                        {
                            DateTime DataAvisoCheque = row.RHTarefaDetalheRow.RHTarefaRow.RHTDataCheque.SomaDiasUteis(1);
                            DateTime DataLimiteCheque = row.RHTarefaDetalheRow.RHTarefaRow.RHTDataCredito.SomaDiasUteis(2);
                            DateTime DataLimiteDefCheque = row.RHTarefaDetalheRow.RHTarefaRow.RHTDataCredito.SomaDiasUteis(4);
                            if (DateTime.Today >= DataLimiteDefCheque)
                                Status = StatusRHT.vencidoDefinitivo;
                            else if (DateTime.Today >= DataLimiteCheque)
                                Status = StatusRHT.vencido;
                            else if (DateTime.Today >= DataAvisoCheque)
                                Status = StatusRHT.alerta;
                        }
                    }
                }
                switch (Status)
                {
                    case StatusRHT.NaoAplicavel:
                        e.Appearance.BackColor2 = e.Appearance.BackColor = Color.White;
                        e.Appearance.ForeColor = Color.Black;
                        break;
                    case StatusRHT.ok:
                        e.Appearance.BackColor2 = e.Appearance.BackColor = Color.MediumSpringGreen;
                        e.Appearance.ForeColor = Color.Black;
                        break;
                    case StatusRHT.aguardo:
                        e.Appearance.BackColor2 = e.Appearance.BackColor = Color.White;
                        e.Appearance.ForeColor = Color.Black;
                        break;
                    case StatusRHT.alerta:
                        e.Appearance.BackColor2 = e.Appearance.BackColor = Color.Yellow;
                        e.Appearance.ForeColor = Color.Black;
                        break;
                    case StatusRHT.vencido:
                        e.Appearance.BackColor2 = e.Appearance.BackColor = Color.Pink;
                        e.Appearance.ForeColor = Color.Black;
                        break;
                    case StatusRHT.vencidoDefinitivo:
                        e.Appearance.BackColor2 = e.Appearance.BackColor = Color.Red;
                        e.Appearance.ForeColor = Color.White;
                        break;
                    case StatusRHT.cancelado:
                        e.Appearance.BackColor2 = e.Appearance.BackColor = Color.Silver;
                        e.Appearance.ForeColor = Color.Black;
                        e.Appearance.Font = FontCortado(e.Appearance.Font);
                        break;                    
                }
            }
        }

        private void repositoryItemButtonNota1_ButtonClick(object sender, DevExpress.XtraEditors.Controls.ButtonPressedEventArgs e)
        {
            dAutomacaoBancaria.RHTarefaDetalheRow rowRHD = (dAutomacaoBancaria.RHTarefaDetalheRow)gridViewPagCond.GetFocusedDataRow();
            if (!rowRHD.IsRHD_NOANull())
            {
                ContaPagar.cNotasCampos Novo = new ContaPagar.cNotasCampos(rowRHD.RHD_NOA,
                                                                           string.Format("{0} - Nota ({1:n2})", rowRHD.CONCodigoFolha1, rowRHD.IsTotalNull() ? 0 : rowRHD.Total),
                                                                           EMPTProc.EMPTProcStX(rowRHD.EMP));
            }
        }

        private void repositoryItemButtonPeriodico1_ButtonClick(object sender, DevExpress.XtraEditors.Controls.ButtonPressedEventArgs e)
        {
            dAutomacaoBancaria.RHTarefaDetalheRow rowRHD = (dAutomacaoBancaria.RHTarefaDetalheRow)gridViewPagCond.GetFocusedDataRow();
            if (!rowRHD.IsRHD_NOANull())
            {
                cPagamentosPeriodicosCampos Novo = new cPagamentosPeriodicosCampos(rowRHD.NOA_PGF,
                                                                                   string.Format("{0} - Periódico", rowRHD.CONCodigoFolha1),
                                                                                   EMPTProc.EMPTProcStX(rowRHD.EMP));
            }
        }

        private void AbreCheque(dAutomacaoBancaria.RHTarefaSubdetalheRow rowRHS)
        {
            if (!rowRHS.IsRHS_CHENull())
            {
                Cheque ChequeEd = new Cheque(rowRHS.RHS_CHE, dllChequesProc.ChequeProc.TipoChave.CHE, EMPTProc.EMPTProcStX(rowRHS.EMP));
                CHEStatus CHEStatusAntigo = ChequeEd.CHEStatus;
                ChequeEd.Editar(false);
                if (CHEStatusAntigo != ChequeEd.CHEStatus)
                {
                    if (MessageBox.Show("Cheque alterado. Recalcular?", "Aviso", MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button1) == DialogResult.Yes)
                    {
                        using (CompontesBasicos.Espera.cEspera ESP = new CompontesBasicos.Espera.cEspera(this))
                        {
                            cFolhaPag TarefaReaberta = new cFolhaPag(rowRHS.EMP, rowRHS.RHTarefaDetalheRow.RHD_RHT, Chamador, ESP);
                            TarefaReaberta.VirShowModulo(EstadosDosComponentes.JanelasAtivas);
                        }
                        FechaTela(DialogResult.Yes);
                    }
                }
            }
        }

        private void repositoryItemButtonCHEQUE_ButtonClick(object sender, DevExpress.XtraEditors.Controls.ButtonPressedEventArgs e)
        {
            if (gridControlCondom.FocusedView != null)
            {
                DevExpress.XtraGrid.Views.Grid.GridView GV = (DevExpress.XtraGrid.Views.Grid.GridView)gridControlCondom.FocusedView;
                dAutomacaoBancaria.RHTarefaSubdetalheRow rowRHS = (dAutomacaoBancaria.RHTarefaSubdetalheRow)GV.GetFocusedDataRow();
                AbreCheque(rowRHS);                
            }
        }

        private void repositoryItemButtonCheque1_ButtonClick(object sender, DevExpress.XtraEditors.Controls.ButtonPressedEventArgs e)
        {
            //dAutomacaoBancaria.RHTarefaSubdetalheRow rowRHS = (dAutomacaoBancaria.RHTarefaSubdetalheRow)ViewPagamentos.GetFocusedDataRow();
            AbreCheque((dAutomacaoBancaria.RHTarefaSubdetalheRow)ViewPagamentos.GetFocusedDataRow());
            /*
            if (!rowRHS.IsRHS_CHENull())
            {
                cCheque Novo = new cCheque(rowRHS.RHS_CHE, EMPTProc.EMPTProcStX(rowRHS.EMP));
                Novo.VirShowModulo(EstadosDosComponentes.PopUp);
            }*/
        }

        #endregion


        #region Servidor de processos
        private const int TentavasRecuperar = 10;

        private RetornoServidorProcessos SimulaRH_GravaSalario(int EMP, int USU, int RHT)
        {
            RetornoServidorProcessos Retorno = new RetornoServidorProcessos("OK", "OK");
            try
            {
                VirMSSQL.TableAdapter.Servidor_De_Processos = true;
                EMPTProc EMPTProc1 = new FrameworkProc.EMPTProc(EMP == 1 ? FrameworkProc.EMPTipo.Local : FrameworkProc.EMPTipo.Filial,
                                                                              EMP == 1 ? Framework.DSCentral.USU : Framework.DSCentral.USUFilial);

                DPProc.Salarios Salarios1 = new Salarios();
                if (!Salarios1.Grava(EMPTProc1, RHT))
                {
                    Retorno.ok = false;
                    Retorno.Mensagem = "Erro ao gravar: Checar o motivo";
                    Retorno.MensagemSimplificada = "Erro ao gravar";
                    Retorno.TipoDeErro = VirExceptionProc.VirTipoDeErro.efetivo_bug;
                }
                return Retorno;
            }
            catch (Exception e)
            {
                Retorno.ok = false;
                Retorno.Mensagem = VirExceptionProc.VirExceptionProc.RelatorioDeErro(e, true, true, true);
                Retorno.MensagemSimplificada = VirExceptionProc.VirExceptionProc.RelatorioDeErro(e, false, false, false);
                Retorno.TipoDeErro = VirExceptionProc.VirExceptionProc.IdentificaTipoDeErro(e);
                return Retorno;
            }
            finally
            {
                VirMSSQL.TableAdapter.Servidor_De_Processos = false;
            }
        }

        private string URLServidorProcessos
        {
            get
            {
                switch (ClienteServProc.TipoServidoSelecionado)
                {
                    case TiposServidorProc.Local:
                        return "WCFRHL";
                    case TiposServidorProc.QAS:
                        return "WCFRHH";
                    case TiposServidorProc.Producao:
                        return "WCFRHP";
                    case TiposServidorProc.Indefinido:
                    case TiposServidorProc.Simulador:
                    default:
                        return string.Empty;
                }
            }
        }

        private RetornoServidorProcessos ThreadChamadaAoServidor(int EMP, int USU, int RHT)
        {
            try
            {
                Retorno = RHClient1.RH_GravaSalario(VirCrip.VirCripWS.Crip(Framework.DSCentral.ChaveCriptografar(EMP, USU)), RHT);
            }
            catch (Exception e)
            {
                Retorno = new RetornoServidorProcessos("Erro interno na chamada", string.Format("Erro interno na chamada:\r\n{0}", e.Message));
                Retorno.ok = false;
                Retorno.TipoDeErro = VirExceptionProc.VirTipoDeErro.efetivo_bug;
            }
            return Retorno;
        }
        #endregion


        #region Arquivo remessa
        private void RemessaCreditoContaBradesco(DateTime DataParaDebito)
        {
            /*** ARQUIVO BRADESCO - CREDITO EM C/C ***/
            String Diretorio = @"J:\Virtual\Credito Conta\" + DateTime.Now.ToString("MMMyy") + "\\";
            if (!ExisteDiretorio(Diretorio))
            {
                Diretorio = @"C:\Virtual\Credito Conta\" + DateTime.Now.ToString("MMMyy") + "\\";
                if (!ExisteDiretorio(Diretorio))
                    return;
            }
            if (!ExisteDiretorio(Diretorio))
                return;
            String Arquivo = Diretorio + "FP" + DateTime.Now.ToString("ddMM");
            String[] sFiles = Directory.GetFiles(Diretorio);
            Int32 idFile = 0;
            foreach (string ArqEmTeste in sFiles)
            {
                Int32 idFilePos;
                string NomeArqEmTeste = Path.GetFileNameWithoutExtension(ArqEmTeste);
                if (NomeArqEmTeste.Length > 6)
                    if (Int32.TryParse(NomeArqEmTeste.Substring(6), out idFilePos))
                        if (idFile < idFilePos)
                            idFile = idFilePos;
            }
            Arquivo += Convert.ToString(idFile + 1) + ".REM";

            if (Tem(dAutomacaoBancaria.RHTarefaSubdetalhe, 237))
                using (StreamWriter swArquivo = new StreamWriter(Arquivo))
                {
                    foreach (dAutomacaoBancaria.RHTarefaDetalheRow RHDrow in dAutomacaoBancaria.RHTarefaDetalhe)
                        if (Tem(RHDrow.GetRHTarefaSubdetalheRows(), 237))
                            GravaBloco(swArquivo, RHDrow, DataParaDebito);
                    swArquivo.Flush();
                    swArquivo.Close();
                }
        }

        private Boolean ExisteDiretorio(String Diretorio)
        {
            if (!Directory.Exists(Diretorio))
                try
                {
                    Directory.CreateDirectory(Diretorio);
                    return true;
                }
                catch
                {
                    MessageBox.Show("O diretório destinado a gravação dos arquivos gerados não pode ser encontrado.\r\nVerifique se o diretório " + Diretorio + " existe e tente gerar novamente.", "Automação Bancária", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    return false;
                }
            else
                return true;
        }

        private bool Tem(IEnumerable Testar, int Banco)
        {
            foreach (dAutomacaoBancaria.RHTarefaSubdetalheRow RHSrow in Testar)
            {
                nTipoPag nTipo = (nTipoPag)RHSrow.RHSTipoPag;
                if ((RHSrow.FUCBanco == Banco) && (nTipo == nTipoPag.CreditoConta) && (RHSrow.RHSValor > 0))
                    return true;
            }
            return false;
        }

        private void GravaBloco(StreamWriter swArquivo, dAutomacaoBancaria.RHTarefaDetalheRow RHDrow, DateTime DataParaDebito)
        {
            //HEADER                        
            decimal nVrTotal = 0;
            int iSeqRegistro = 1;
            string sIdHeader = "0";
            string sIdFita = "1";
            string sIdFitaExtenso = FuncoesDP.CampoAlfaNumerico("REMESSA", 7);
            string sIdServico = "03";
            string sIdServicoExtenso = FuncoesDP.CampoAlfaNumerico("CREDITO C/C", 15);
            string sAgenciaDeb = FuncoesDP.CampoNumerico(RHDrow.CONAgencia, 5);
            string sRazaoH = "07050";
            string sContaDeb = FuncoesDP.CampoNumerico(RHDrow.CONConta, 7);
            string sdContaDeb = FuncoesDP.CampoNumerico(RHDrow.CONDigitoConta, 1);
            string sIdCrec = " ";
            string sReservaH1 = " ";
            string sCodEmpresa = FuncoesDP.CampoNumerico(RHDrow.CONRazaoCC, 5);
            string sRazaoSocial = FuncoesDP.CampoAlfaNumerico(RHDrow.CONNome, 25);
            string sCodBanco = "237";
            string sNomeBanco = FuncoesDP.CampoAlfaNumerico("BRADESCO", 15);
            string sDataFita = DateTime.Today.ToString("ddMMyyyy");
            string sDensidade = FuncoesDP.CampoNumerico("01600", 5);
            string sUnidDensidade = "BPI";
            string sDataDebito = DataParaDebito.ToString("ddMMyyyy");
            string sIDMoeda = " ";
            string sIDSeculo = "N";
            string sReservaH2 = FuncoesDP.CampoAlfaNumerico(" ", 74);
            string sSeqHeader = FuncoesDP.CampoNumerico(iSeqRegistro, 6);

            swArquivo.WriteLine(
                                sIdHeader +
                                sIdFita +
                                sIdFitaExtenso +
                                sIdServico +
                                sIdServicoExtenso +
                                sAgenciaDeb +
                                sRazaoH +
                                sContaDeb +
                                sdContaDeb +
                                sIdCrec +
                                sReservaH1 +
                                sCodEmpresa +
                                sRazaoSocial +
                                sCodBanco +
                                sNomeBanco +
                                sDataFita +
                                sDensidade +
                                sUnidDensidade +
                                sDataDebito +
                                sIDMoeda +
                                sIDSeculo +
                                sReservaH2 +
                                sSeqHeader
                               );

            iSeqRegistro++;
            foreach (dAutomacaoBancaria.RHTarefaSubdetalheRow RHSrow in RHDrow.GetRHTarefaSubdetalheRows())
            {
                tipoconta FUCTipoConta = (tipoconta)RHSrow.FUCTipoConta;
                if (!FUCTipoConta.EstaNoGrupo(tipoconta.ContaCorrente, tipoconta.ContaSalario, tipoconta.Poupanca) || (RHSrow.RHSValor <= 0))
                    continue;
                if ((nTipoPag)RHSrow.RHSTipoPag != nTipoPag.CreditoConta)
                    continue;
                string sIdTransacao = "1";
                string sReservaT1 = FuncoesDP.CampoAlfaNumerico(" ", 61);
                string sAgenciaCred = FuncoesDP.CampoNumerico(RHSrow.FUCAgencia.Substring(0, RHSrow.FUCAgencia.IndexOf("-")), 5);
                string sRazaoD = "07050";
                switch (FUCTipoConta)
                {
                    case tipoconta.ContaCorrente:
                        sRazaoD = "07050";
                        break;
                    case tipoconta.ContaSalario:
                        sRazaoD = "07380";
                        break;
                    case tipoconta.Poupanca:
                        sRazaoD = "10510";
                        break;
                    default:
                        throw new NotImplementedException(string.Format("Tipo de conta não prevista: {0}", FUCTipoConta));
                };
                string conta = RHSrow.FUCConta.Trim();
                string sContaCred = FuncoesDP.CampoNumerico(conta.Substring(0, (conta.Length - 1)), 7);
                string sdContaCred = FuncoesDP.CampoNumerico(conta.Substring((conta.Length - 1), 1), 1);
                string sReservaT2 = FuncoesDP.CampoAlfaNumerico(" ", 2);
                string sNomeFunc = FuncoesDP.CampoAlfaNumerico(RHSrow.FUCNome, 38);
                string sRegistroFunc = FuncoesDP.CampoNumerico(RHSrow.FUCRegistro, 6);
                string sValorCred = FuncoesDP.CampoNumerico(RHSrow.RHSValor, 13);
                string sCodServico = "298";
                string sReservaT3 = FuncoesDP.CampoAlfaNumerico(" ", 8);
                string sReservaT4 = FuncoesDP.CampoAlfaNumerico(" ", 44);
                string sSeqTransacao = FuncoesDP.CampoNumerico(iSeqRegistro, 6);

                swArquivo.WriteLine(
                                    sIdTransacao +
                                    sReservaT1 +
                                    sAgenciaCred +
                                    sRazaoD +
                                    sContaCred +
                                    sdContaCred +
                                    sReservaT2 +
                                    sNomeFunc +
                                    sRegistroFunc +
                                    sValorCred +
                                    sCodServico +
                                    sReservaT3 +
                                    sReservaT4 +
                                    sSeqTransacao
                                   );
                nVrTotal += RHSrow.RHSValor;
                iSeqRegistro++;
            }
            //TRAILER        
            string sIdTrailer = "9";
            string sVrTotalTransacao = FuncoesDP.CampoNumerico(nVrTotal, 13);
            string sReservaTL1 = FuncoesDP.CampoAlfaNumerico(" ", 180);
            string sSeqTrailer = FuncoesDP.CampoNumerico(iSeqRegistro, 6);

            swArquivo.WriteLine(
                                sIdTrailer +
                                sVrTotalTransacao +
                                sReservaTL1 +
                                sSeqTrailer
                               );
        }
        #endregion


    }
}
