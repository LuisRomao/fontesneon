﻿namespace DP.FolhaPag
{
    partial class cFolhaPag
    {
        /// <summary>
        /// Variável de designer necessária.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Limpar os recursos que estão sendo usados.
        /// </summary>
        /// <param name="disposing">true se for necessário descartar os recursos gerenciados; caso contrário, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Código gerado pelo Windows Form Designer

        /// <summary>
        /// Método necessário para suporte ao Designer - não modifique 
        /// o conteúdo deste método com o editor de código.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject1 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.XtraGrid.GridLevelNode gridLevelNode1 = new DevExpress.XtraGrid.GridLevelNode();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject2 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject3 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject4 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject5 = new DevExpress.Utils.SerializableAppearanceObject();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(cFolhaPag));
            this.gridViewPagCondSdet = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colFUNNome1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colFUNValor1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.CalcEdit1 = new DevExpress.XtraEditors.Repository.RepositoryItemCalcEdit();
            this.colFUNRegistro1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colFUNCPF1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colstrStatus1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colRHS_CHE = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn4CHEque = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridControlCondom = new DevExpress.XtraGrid.GridControl();
            this.dAutomacaoBancariaBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.dAutomacaoBancaria = new DP.Automacao_Bancaria.dAutomacaoBancaria();
            this.gridViewPagCond = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colCONCodigo = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colCONNome1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colCONRazaoCC = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colCONBanco = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colCONAgencia = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colCONDigitoAgencia = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colCONConta = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colCONDigitoConta = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colCONCNPJ = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colCON__EMP = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colEMP1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colTotal = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colRefLocal = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colRHD_NOA = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn4Nota = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colNOA_PGF1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn4Periodico = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colValorAnterior1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colValorAnterior2 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colValorAnterior3 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colValorAcumulado = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSaldoCC = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colValorAnteriorMedio = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemButtonNota1 = new DevExpress.XtraEditors.Repository.RepositoryItemButtonEdit();
            this.repositoryItemButtonPeriodico1 = new DevExpress.XtraEditors.Repository.RepositoryItemButtonEdit();
            this.repositoryItemButtonCHEQUE = new DevExpress.XtraEditors.Repository.RepositoryItemButtonEdit();
            this.TabControlSalarios = new DevExpress.XtraTab.XtraTabControl();
            this.TabSAL = new DevExpress.XtraTab.XtraTabPage();
            this.gridPagamentos = new DevExpress.XtraGrid.GridControl();
            this.ViewPagamentos = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colCONNome = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colFUNRegistro = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colFUNNome = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colFUNConta = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colFUNBanco = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colFUNAgencia = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colFUNCPF = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colFUNValor = new DevExpress.XtraGrid.Columns.GridColumn();
            this.clcValorPagamentos = new DevExpress.XtraEditors.Repository.RepositoryItemCalcEdit();
            this.colFUNTipoConta = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryTipoConta = new DevExpress.XtraEditors.Repository.RepositoryItemImageComboBox();
            this.colFUNnTipoPag = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemImageComboBox1 = new DevExpress.XtraEditors.Repository.RepositoryItemImageComboBox();
            this.colFUNIDBanco = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colEMP = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colstrStatus = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colTMPFolha = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colTMPFolhaDet = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colCHEStatus = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemImageComboBox8 = new DevExpress.XtraEditors.Repository.RepositoryItemImageComboBox();
            this.colRHS_CHE1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumnCheque1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.LookUpConNome = new DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit();
            this.repositoryItemButtonCheque1 = new DevExpress.XtraEditors.Repository.RepositoryItemButtonEdit();
            this.TabPAGP = new DevExpress.XtraTab.XtraTabPage();
            this.panelControl1 = new DevExpress.XtraEditors.PanelControl();
            this.simpleButton2 = new DevExpress.XtraEditors.SimpleButton();
            this.botaoZerar = new DevExpress.XtraEditors.SimpleButton();
            this.lookUpEditPLA1 = new DevExpress.XtraEditors.LookUpEdit();
            this.pLAnocontasBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.lookUpEditPLA2 = new DevExpress.XtraEditors.LookUpEdit();
            this.lalookUpEditPLA1 = new DevExpress.XtraEditors.LabelControl();
            this.dateDataCheque = new DevExpress.XtraEditors.DateEdit();
            this.LDataCheque = new DevExpress.XtraEditors.LabelControl();
            this.dateCreditoConta = new DevExpress.XtraEditors.DateEdit();
            this.LCreditoConta = new DevExpress.XtraEditors.LabelControl();
            this.labelControl3 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl2 = new DevExpress.XtraEditors.LabelControl();
            this.imageComboTipo = new DevExpress.XtraEditors.ImageComboBoxEdit();
            this.cCompet1 = new Framework.objetosNeon.cCompet();
            this.toolStripPagamentos = new System.Windows.Forms.ToolStrip();
            this.btnRemessaPagamentos = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator2 = new System.Windows.Forms.ToolStripSeparator();
            this.btnImprimirPagamentos = new System.Windows.Forms.ToolStripButton();
            ((System.ComponentModel.ISupportInitialize)(this.StyleController_F)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridViewPagCondSdet)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.CalcEdit1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridControlCondom)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dAutomacaoBancariaBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dAutomacaoBancaria)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridViewPagCond)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemButtonNota1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemButtonPeriodico1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemButtonCHEQUE)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TabControlSalarios)).BeginInit();
            this.TabControlSalarios.SuspendLayout();
            this.TabSAL.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridPagamentos)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ViewPagamentos)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.clcValorPagamentos)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryTipoConta)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemImageComboBox1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemImageComboBox8)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LookUpConNome)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemButtonCheque1)).BeginInit();
            this.TabPAGP.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl1)).BeginInit();
            this.panelControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.lookUpEditPLA1.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pLAnocontasBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lookUpEditPLA2.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateDataCheque.Properties.CalendarTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateDataCheque.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateCreditoConta.Properties.CalendarTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateCreditoConta.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.imageComboTipo.Properties)).BeginInit();
            this.toolStripPagamentos.SuspendLayout();
            this.SuspendLayout();
            // 
            // ToolTipController_F
            // 
            this.ToolTipController_F.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.ToolTipController_F.Appearance.ForeColor = System.Drawing.Color.Black;
            this.ToolTipController_F.Appearance.Options.UseBackColor = true;
            this.ToolTipController_F.Appearance.Options.UseFont = true;
            this.ToolTipController_F.Appearance.Options.UseForeColor = true;
            this.ToolTipController_F.Appearance.Options.UseTextOptions = true;
            this.ToolTipController_F.Appearance.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            // 
            // StyleController_F
            // 
            this.StyleController_F.AppearanceDisabled.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this.StyleController_F.AppearanceDisabled.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this.StyleController_F.AppearanceDisabled.Options.UseBackColor = true;
            this.StyleController_F.AppearanceDisabled.Options.UseForeColor = true;
            this.StyleController_F.AppearanceFocused.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.StyleController_F.AppearanceFocused.Options.UseBackColor = true;
            // 
            // gridViewPagCondSdet
            // 
            this.gridViewPagCondSdet.Appearance.ColumnFilterButton.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(240)))), ((int)(((byte)(177)))), ((int)(((byte)(94)))));
            this.gridViewPagCondSdet.Appearance.ColumnFilterButton.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(240)))), ((int)(((byte)(177)))), ((int)(((byte)(94)))));
            this.gridViewPagCondSdet.Appearance.ColumnFilterButton.ForeColor = System.Drawing.Color.Black;
            this.gridViewPagCondSdet.Appearance.ColumnFilterButton.Options.UseBackColor = true;
            this.gridViewPagCondSdet.Appearance.ColumnFilterButton.Options.UseBorderColor = true;
            this.gridViewPagCondSdet.Appearance.ColumnFilterButton.Options.UseForeColor = true;
            this.gridViewPagCondSdet.Appearance.ColumnFilterButtonActive.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(248)))), ((int)(((byte)(204)))), ((int)(((byte)(124)))));
            this.gridViewPagCondSdet.Appearance.ColumnFilterButtonActive.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(248)))), ((int)(((byte)(204)))), ((int)(((byte)(124)))));
            this.gridViewPagCondSdet.Appearance.ColumnFilterButtonActive.ForeColor = System.Drawing.Color.Black;
            this.gridViewPagCondSdet.Appearance.ColumnFilterButtonActive.Options.UseBackColor = true;
            this.gridViewPagCondSdet.Appearance.ColumnFilterButtonActive.Options.UseBorderColor = true;
            this.gridViewPagCondSdet.Appearance.ColumnFilterButtonActive.Options.UseForeColor = true;
            this.gridViewPagCondSdet.Appearance.Empty.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(251)))), ((int)(((byte)(244)))), ((int)(((byte)(232)))));
            this.gridViewPagCondSdet.Appearance.Empty.BackColor2 = System.Drawing.Color.White;
            this.gridViewPagCondSdet.Appearance.Empty.Options.UseBackColor = true;
            this.gridViewPagCondSdet.Appearance.EvenRow.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(253)))), ((int)(((byte)(248)))), ((int)(((byte)(236)))));
            this.gridViewPagCondSdet.Appearance.EvenRow.ForeColor = System.Drawing.Color.Black;
            this.gridViewPagCondSdet.Appearance.EvenRow.Options.UseBackColor = true;
            this.gridViewPagCondSdet.Appearance.EvenRow.Options.UseForeColor = true;
            this.gridViewPagCondSdet.Appearance.FilterCloseButton.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(240)))), ((int)(((byte)(177)))), ((int)(((byte)(94)))));
            this.gridViewPagCondSdet.Appearance.FilterCloseButton.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(240)))), ((int)(((byte)(177)))), ((int)(((byte)(94)))));
            this.gridViewPagCondSdet.Appearance.FilterCloseButton.ForeColor = System.Drawing.Color.Black;
            this.gridViewPagCondSdet.Appearance.FilterCloseButton.Options.UseBackColor = true;
            this.gridViewPagCondSdet.Appearance.FilterCloseButton.Options.UseBorderColor = true;
            this.gridViewPagCondSdet.Appearance.FilterCloseButton.Options.UseForeColor = true;
            this.gridViewPagCondSdet.Appearance.FilterPanel.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(251)))), ((int)(((byte)(244)))), ((int)(((byte)(232)))));
            this.gridViewPagCondSdet.Appearance.FilterPanel.BackColor2 = System.Drawing.Color.White;
            this.gridViewPagCondSdet.Appearance.FilterPanel.ForeColor = System.Drawing.Color.Black;
            this.gridViewPagCondSdet.Appearance.FilterPanel.Options.UseBackColor = true;
            this.gridViewPagCondSdet.Appearance.FilterPanel.Options.UseForeColor = true;
            this.gridViewPagCondSdet.Appearance.FixedLine.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(218)))), ((int)(((byte)(153)))), ((int)(((byte)(73)))));
            this.gridViewPagCondSdet.Appearance.FixedLine.Options.UseBackColor = true;
            this.gridViewPagCondSdet.Appearance.FocusedCell.BackColor = System.Drawing.Color.White;
            this.gridViewPagCondSdet.Appearance.FocusedCell.ForeColor = System.Drawing.Color.Black;
            this.gridViewPagCondSdet.Appearance.FocusedCell.Options.UseBackColor = true;
            this.gridViewPagCondSdet.Appearance.FocusedCell.Options.UseForeColor = true;
            this.gridViewPagCondSdet.Appearance.FocusedRow.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(217)))), ((int)(((byte)(154)))), ((int)(((byte)(91)))));
            this.gridViewPagCondSdet.Appearance.FocusedRow.ForeColor = System.Drawing.Color.White;
            this.gridViewPagCondSdet.Appearance.FocusedRow.Options.UseBackColor = true;
            this.gridViewPagCondSdet.Appearance.FocusedRow.Options.UseForeColor = true;
            this.gridViewPagCondSdet.Appearance.FooterPanel.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(240)))), ((int)(((byte)(177)))), ((int)(((byte)(94)))));
            this.gridViewPagCondSdet.Appearance.FooterPanel.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(240)))), ((int)(((byte)(177)))), ((int)(((byte)(94)))));
            this.gridViewPagCondSdet.Appearance.FooterPanel.ForeColor = System.Drawing.Color.Black;
            this.gridViewPagCondSdet.Appearance.FooterPanel.Options.UseBackColor = true;
            this.gridViewPagCondSdet.Appearance.FooterPanel.Options.UseBorderColor = true;
            this.gridViewPagCondSdet.Appearance.FooterPanel.Options.UseForeColor = true;
            this.gridViewPagCondSdet.Appearance.GroupButton.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(248)))), ((int)(((byte)(204)))), ((int)(((byte)(124)))));
            this.gridViewPagCondSdet.Appearance.GroupButton.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(248)))), ((int)(((byte)(204)))), ((int)(((byte)(124)))));
            this.gridViewPagCondSdet.Appearance.GroupButton.Options.UseBackColor = true;
            this.gridViewPagCondSdet.Appearance.GroupButton.Options.UseBorderColor = true;
            this.gridViewPagCondSdet.Appearance.GroupFooter.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(248)))), ((int)(((byte)(204)))), ((int)(((byte)(124)))));
            this.gridViewPagCondSdet.Appearance.GroupFooter.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(248)))), ((int)(((byte)(204)))), ((int)(((byte)(124)))));
            this.gridViewPagCondSdet.Appearance.GroupFooter.ForeColor = System.Drawing.Color.Black;
            this.gridViewPagCondSdet.Appearance.GroupFooter.Options.UseBackColor = true;
            this.gridViewPagCondSdet.Appearance.GroupFooter.Options.UseBorderColor = true;
            this.gridViewPagCondSdet.Appearance.GroupFooter.Options.UseForeColor = true;
            this.gridViewPagCondSdet.Appearance.GroupPanel.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(251)))), ((int)(((byte)(244)))), ((int)(((byte)(232)))));
            this.gridViewPagCondSdet.Appearance.GroupPanel.BackColor2 = System.Drawing.Color.White;
            this.gridViewPagCondSdet.Appearance.GroupPanel.ForeColor = System.Drawing.Color.Black;
            this.gridViewPagCondSdet.Appearance.GroupPanel.Options.UseBackColor = true;
            this.gridViewPagCondSdet.Appearance.GroupPanel.Options.UseForeColor = true;
            this.gridViewPagCondSdet.Appearance.GroupRow.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(248)))), ((int)(((byte)(204)))), ((int)(((byte)(124)))));
            this.gridViewPagCondSdet.Appearance.GroupRow.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(248)))), ((int)(((byte)(204)))), ((int)(((byte)(124)))));
            this.gridViewPagCondSdet.Appearance.GroupRow.ForeColor = System.Drawing.Color.Black;
            this.gridViewPagCondSdet.Appearance.GroupRow.Options.UseBackColor = true;
            this.gridViewPagCondSdet.Appearance.GroupRow.Options.UseBorderColor = true;
            this.gridViewPagCondSdet.Appearance.GroupRow.Options.UseForeColor = true;
            this.gridViewPagCondSdet.Appearance.HeaderPanel.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(248)))), ((int)(((byte)(204)))), ((int)(((byte)(124)))));
            this.gridViewPagCondSdet.Appearance.HeaderPanel.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(248)))), ((int)(((byte)(204)))), ((int)(((byte)(124)))));
            this.gridViewPagCondSdet.Appearance.HeaderPanel.ForeColor = System.Drawing.Color.Black;
            this.gridViewPagCondSdet.Appearance.HeaderPanel.Options.UseBackColor = true;
            this.gridViewPagCondSdet.Appearance.HeaderPanel.Options.UseBorderColor = true;
            this.gridViewPagCondSdet.Appearance.HeaderPanel.Options.UseForeColor = true;
            this.gridViewPagCondSdet.Appearance.HideSelectionRow.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(225)))), ((int)(((byte)(183)))), ((int)(((byte)(125)))));
            this.gridViewPagCondSdet.Appearance.HideSelectionRow.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(251)))), ((int)(((byte)(244)))), ((int)(((byte)(232)))));
            this.gridViewPagCondSdet.Appearance.HideSelectionRow.Options.UseBackColor = true;
            this.gridViewPagCondSdet.Appearance.HideSelectionRow.Options.UseForeColor = true;
            this.gridViewPagCondSdet.Appearance.HorzLine.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(240)))), ((int)(((byte)(177)))), ((int)(((byte)(94)))));
            this.gridViewPagCondSdet.Appearance.HorzLine.Options.UseBackColor = true;
            this.gridViewPagCondSdet.Appearance.OddRow.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(251)))), ((int)(((byte)(236)))), ((int)(((byte)(208)))));
            this.gridViewPagCondSdet.Appearance.OddRow.ForeColor = System.Drawing.Color.Black;
            this.gridViewPagCondSdet.Appearance.OddRow.Options.UseBackColor = true;
            this.gridViewPagCondSdet.Appearance.OddRow.Options.UseForeColor = true;
            this.gridViewPagCondSdet.Appearance.Preview.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(254)))), ((int)(((byte)(249)))));
            this.gridViewPagCondSdet.Appearance.Preview.Font = new System.Drawing.Font("Verdana", 7.5F);
            this.gridViewPagCondSdet.Appearance.Preview.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(186)))), ((int)(((byte)(146)))), ((int)(((byte)(78)))));
            this.gridViewPagCondSdet.Appearance.Preview.Options.UseBackColor = true;
            this.gridViewPagCondSdet.Appearance.Preview.Options.UseFont = true;
            this.gridViewPagCondSdet.Appearance.Preview.Options.UseForeColor = true;
            this.gridViewPagCondSdet.Appearance.Row.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(253)))), ((int)(((byte)(248)))), ((int)(((byte)(236)))));
            this.gridViewPagCondSdet.Appearance.Row.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(253)))), ((int)(((byte)(248)))), ((int)(((byte)(236)))));
            this.gridViewPagCondSdet.Appearance.Row.ForeColor = System.Drawing.Color.Black;
            this.gridViewPagCondSdet.Appearance.Row.Options.UseBackColor = true;
            this.gridViewPagCondSdet.Appearance.Row.Options.UseBorderColor = true;
            this.gridViewPagCondSdet.Appearance.Row.Options.UseForeColor = true;
            this.gridViewPagCondSdet.Appearance.RowSeparator.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(251)))), ((int)(((byte)(244)))), ((int)(((byte)(232)))));
            this.gridViewPagCondSdet.Appearance.RowSeparator.BackColor2 = System.Drawing.Color.White;
            this.gridViewPagCondSdet.Appearance.RowSeparator.Options.UseBackColor = true;
            this.gridViewPagCondSdet.Appearance.SelectedRow.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(231)))), ((int)(((byte)(167)))), ((int)(((byte)(103)))));
            this.gridViewPagCondSdet.Appearance.SelectedRow.Options.UseBackColor = true;
            this.gridViewPagCondSdet.Appearance.TopNewRow.BackColor = System.Drawing.Color.White;
            this.gridViewPagCondSdet.Appearance.TopNewRow.Options.UseBackColor = true;
            this.gridViewPagCondSdet.Appearance.VertLine.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(240)))), ((int)(((byte)(177)))), ((int)(((byte)(94)))));
            this.gridViewPagCondSdet.Appearance.VertLine.Options.UseBackColor = true;
            this.gridViewPagCondSdet.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colFUNNome1,
            this.colFUNValor1,
            this.colFUNRegistro1,
            this.colFUNCPF1,
            this.colstrStatus1,
            this.colRHS_CHE,
            this.gridColumn4CHEque});
            this.gridViewPagCondSdet.GridControl = this.gridControlCondom;
            this.gridViewPagCondSdet.Name = "gridViewPagCondSdet";
            this.gridViewPagCondSdet.OptionsView.ColumnAutoWidth = false;
            this.gridViewPagCondSdet.OptionsView.EnableAppearanceEvenRow = true;
            this.gridViewPagCondSdet.OptionsView.EnableAppearanceOddRow = true;
            this.gridViewPagCondSdet.OptionsView.ShowGroupPanel = false;
            this.gridViewPagCondSdet.PaintStyleName = "Flat";
            this.gridViewPagCondSdet.CustomRowCellEdit += new DevExpress.XtraGrid.Views.Grid.CustomRowCellEditEventHandler(this.gridViewPagCondSdet_CustomRowCellEdit);
            this.gridViewPagCondSdet.FocusedRowChanged += new DevExpress.XtraGrid.Views.Base.FocusedRowChangedEventHandler(this.gridViewPagCondSdet_FocusedRowChanged);
            // 
            // colFUNNome1
            // 
            this.colFUNNome1.Caption = "Nome";
            this.colFUNNome1.FieldName = "FUCNome";
            this.colFUNNome1.Name = "colFUNNome1";
            this.colFUNNome1.OptionsColumn.ReadOnly = true;
            this.colFUNNome1.Visible = true;
            this.colFUNNome1.VisibleIndex = 1;
            this.colFUNNome1.Width = 276;
            // 
            // colFUNValor1
            // 
            this.colFUNValor1.Caption = "Valor";
            this.colFUNValor1.ColumnEdit = this.CalcEdit1;
            this.colFUNValor1.FieldName = "RHSValor";
            this.colFUNValor1.Name = "colFUNValor1";
            this.colFUNValor1.Visible = true;
            this.colFUNValor1.VisibleIndex = 3;
            this.colFUNValor1.Width = 131;
            // 
            // CalcEdit1
            // 
            this.CalcEdit1.AutoHeight = false;
            this.CalcEdit1.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo, "", -1, true, false, false, DevExpress.XtraEditors.ImageLocation.MiddleCenter, null, new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), serializableAppearanceObject1, "", null, null, true)});
            this.CalcEdit1.DisplayFormat.FormatString = "n2";
            this.CalcEdit1.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.CalcEdit1.Mask.EditMask = "n2";
            this.CalcEdit1.Mask.UseMaskAsDisplayFormat = true;
            this.CalcEdit1.Name = "CalcEdit1";
            // 
            // colFUNRegistro1
            // 
            this.colFUNRegistro1.Caption = "Registro";
            this.colFUNRegistro1.FieldName = "FUCRegistro";
            this.colFUNRegistro1.Name = "colFUNRegistro1";
            this.colFUNRegistro1.OptionsColumn.ReadOnly = true;
            this.colFUNRegistro1.Visible = true;
            this.colFUNRegistro1.VisibleIndex = 0;
            this.colFUNRegistro1.Width = 117;
            // 
            // colFUNCPF1
            // 
            this.colFUNCPF1.Caption = "CPF";
            this.colFUNCPF1.FieldName = "FUCCpf";
            this.colFUNCPF1.Name = "colFUNCPF1";
            this.colFUNCPF1.OptionsColumn.ReadOnly = true;
            this.colFUNCPF1.Visible = true;
            this.colFUNCPF1.VisibleIndex = 2;
            this.colFUNCPF1.Width = 125;
            // 
            // colstrStatus1
            // 
            this.colstrStatus1.FieldName = "strStatus";
            this.colstrStatus1.Name = "colstrStatus1";
            this.colstrStatus1.OptionsColumn.ReadOnly = true;
            this.colstrStatus1.Visible = true;
            this.colstrStatus1.VisibleIndex = 4;
            this.colstrStatus1.Width = 279;
            // 
            // colRHS_CHE
            // 
            this.colRHS_CHE.Caption = "CHE";
            this.colRHS_CHE.FieldName = "RHS_CHE";
            this.colRHS_CHE.Name = "colRHS_CHE";
            this.colRHS_CHE.OptionsColumn.ReadOnly = true;
            // 
            // gridColumn4CHEque
            // 
            this.gridColumn4CHEque.Caption = "gridColumn4";
            this.gridColumn4CHEque.Fixed = DevExpress.XtraGrid.Columns.FixedStyle.Right;
            this.gridColumn4CHEque.Name = "gridColumn4CHEque";
            this.gridColumn4CHEque.OptionsColumn.ShowCaption = false;
            this.gridColumn4CHEque.Visible = true;
            this.gridColumn4CHEque.VisibleIndex = 5;
            this.gridColumn4CHEque.Width = 25;
            // 
            // gridControlCondom
            // 
            this.gridControlCondom.DataMember = "RHTarefaDetalhe";
            this.gridControlCondom.DataSource = this.dAutomacaoBancariaBindingSource;
            this.gridControlCondom.Dock = System.Windows.Forms.DockStyle.Fill;
            gridLevelNode1.LevelTemplate = this.gridViewPagCondSdet;
            gridLevelNode1.RelationName = "FK_RHTRHTarefaSubdetalhe_RHTarefaDetalhe";
            this.gridControlCondom.LevelTree.Nodes.AddRange(new DevExpress.XtraGrid.GridLevelNode[] {
            gridLevelNode1});
            this.gridControlCondom.Location = new System.Drawing.Point(0, 0);
            this.gridControlCondom.LookAndFeel.Style = DevExpress.LookAndFeel.LookAndFeelStyle.Flat;
            this.gridControlCondom.LookAndFeel.UseDefaultLookAndFeel = false;
            this.gridControlCondom.MainView = this.gridViewPagCond;
            this.gridControlCondom.Name = "gridControlCondom";
            this.gridControlCondom.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.CalcEdit1,
            this.repositoryItemButtonNota1,
            this.repositoryItemButtonPeriodico1,
            this.repositoryItemButtonCHEQUE});
            this.gridControlCondom.ShowOnlyPredefinedDetails = true;
            this.gridControlCondom.Size = new System.Drawing.Size(1432, 641);
            this.gridControlCondom.TabIndex = 0;
            this.gridControlCondom.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridViewPagCond,
            this.gridViewPagCondSdet});
            // 
            // dAutomacaoBancariaBindingSource
            // 
            this.dAutomacaoBancariaBindingSource.DataSource = this.dAutomacaoBancaria;
            this.dAutomacaoBancariaBindingSource.Position = 0;
            // 
            // dAutomacaoBancaria
            // 
            this.dAutomacaoBancaria.DataSetName = "dAutomacaoBancaria";
            this.dAutomacaoBancaria.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // gridViewPagCond
            // 
            this.gridViewPagCond.Appearance.ColumnFilterButton.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(104)))), ((int)(((byte)(184)))), ((int)(((byte)(251)))));
            this.gridViewPagCond.Appearance.ColumnFilterButton.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(104)))), ((int)(((byte)(184)))), ((int)(((byte)(251)))));
            this.gridViewPagCond.Appearance.ColumnFilterButton.ForeColor = System.Drawing.Color.White;
            this.gridViewPagCond.Appearance.ColumnFilterButton.Options.UseBackColor = true;
            this.gridViewPagCond.Appearance.ColumnFilterButton.Options.UseBorderColor = true;
            this.gridViewPagCond.Appearance.ColumnFilterButton.Options.UseForeColor = true;
            this.gridViewPagCond.Appearance.ColumnFilterButtonActive.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(170)))), ((int)(((byte)(216)))), ((int)(((byte)(254)))));
            this.gridViewPagCond.Appearance.ColumnFilterButtonActive.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(170)))), ((int)(((byte)(216)))), ((int)(((byte)(254)))));
            this.gridViewPagCond.Appearance.ColumnFilterButtonActive.ForeColor = System.Drawing.Color.Black;
            this.gridViewPagCond.Appearance.ColumnFilterButtonActive.Options.UseBackColor = true;
            this.gridViewPagCond.Appearance.ColumnFilterButtonActive.Options.UseBorderColor = true;
            this.gridViewPagCond.Appearance.ColumnFilterButtonActive.Options.UseForeColor = true;
            this.gridViewPagCond.Appearance.Empty.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(236)))), ((int)(((byte)(246)))), ((int)(((byte)(255)))));
            this.gridViewPagCond.Appearance.Empty.BackColor2 = System.Drawing.Color.White;
            this.gridViewPagCond.Appearance.Empty.Options.UseBackColor = true;
            this.gridViewPagCond.Appearance.EvenRow.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(247)))), ((int)(((byte)(251)))), ((int)(((byte)(255)))));
            this.gridViewPagCond.Appearance.EvenRow.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(247)))), ((int)(((byte)(251)))), ((int)(((byte)(255)))));
            this.gridViewPagCond.Appearance.EvenRow.ForeColor = System.Drawing.Color.Black;
            this.gridViewPagCond.Appearance.EvenRow.Options.UseBackColor = true;
            this.gridViewPagCond.Appearance.EvenRow.Options.UseBorderColor = true;
            this.gridViewPagCond.Appearance.EvenRow.Options.UseForeColor = true;
            this.gridViewPagCond.Appearance.FilterCloseButton.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(104)))), ((int)(((byte)(184)))), ((int)(((byte)(251)))));
            this.gridViewPagCond.Appearance.FilterCloseButton.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(104)))), ((int)(((byte)(184)))), ((int)(((byte)(251)))));
            this.gridViewPagCond.Appearance.FilterCloseButton.ForeColor = System.Drawing.Color.White;
            this.gridViewPagCond.Appearance.FilterCloseButton.Options.UseBackColor = true;
            this.gridViewPagCond.Appearance.FilterCloseButton.Options.UseBorderColor = true;
            this.gridViewPagCond.Appearance.FilterCloseButton.Options.UseForeColor = true;
            this.gridViewPagCond.Appearance.FilterPanel.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(236)))), ((int)(((byte)(246)))), ((int)(((byte)(255)))));
            this.gridViewPagCond.Appearance.FilterPanel.BackColor2 = System.Drawing.Color.White;
            this.gridViewPagCond.Appearance.FilterPanel.ForeColor = System.Drawing.Color.Black;
            this.gridViewPagCond.Appearance.FilterPanel.Options.UseBackColor = true;
            this.gridViewPagCond.Appearance.FilterPanel.Options.UseForeColor = true;
            this.gridViewPagCond.Appearance.FixedLine.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(59)))), ((int)(((byte)(133)))), ((int)(((byte)(195)))));
            this.gridViewPagCond.Appearance.FixedLine.Options.UseBackColor = true;
            this.gridViewPagCond.Appearance.FocusedCell.BackColor = System.Drawing.Color.White;
            this.gridViewPagCond.Appearance.FocusedCell.ForeColor = System.Drawing.Color.Black;
            this.gridViewPagCond.Appearance.FocusedCell.Options.UseBackColor = true;
            this.gridViewPagCond.Appearance.FocusedCell.Options.UseForeColor = true;
            this.gridViewPagCond.Appearance.FocusedRow.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(38)))), ((int)(((byte)(109)))), ((int)(((byte)(189)))));
            this.gridViewPagCond.Appearance.FocusedRow.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(59)))), ((int)(((byte)(139)))), ((int)(((byte)(206)))));
            this.gridViewPagCond.Appearance.FocusedRow.ForeColor = System.Drawing.Color.White;
            this.gridViewPagCond.Appearance.FocusedRow.Options.UseBackColor = true;
            this.gridViewPagCond.Appearance.FocusedRow.Options.UseBorderColor = true;
            this.gridViewPagCond.Appearance.FocusedRow.Options.UseForeColor = true;
            this.gridViewPagCond.Appearance.FooterPanel.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(104)))), ((int)(((byte)(184)))), ((int)(((byte)(251)))));
            this.gridViewPagCond.Appearance.FooterPanel.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(104)))), ((int)(((byte)(184)))), ((int)(((byte)(251)))));
            this.gridViewPagCond.Appearance.FooterPanel.ForeColor = System.Drawing.Color.Black;
            this.gridViewPagCond.Appearance.FooterPanel.Options.UseBackColor = true;
            this.gridViewPagCond.Appearance.FooterPanel.Options.UseBorderColor = true;
            this.gridViewPagCond.Appearance.FooterPanel.Options.UseForeColor = true;
            this.gridViewPagCond.Appearance.GroupButton.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(104)))), ((int)(((byte)(184)))), ((int)(((byte)(251)))));
            this.gridViewPagCond.Appearance.GroupButton.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(104)))), ((int)(((byte)(184)))), ((int)(((byte)(251)))));
            this.gridViewPagCond.Appearance.GroupButton.Options.UseBackColor = true;
            this.gridViewPagCond.Appearance.GroupButton.Options.UseBorderColor = true;
            this.gridViewPagCond.Appearance.GroupFooter.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(170)))), ((int)(((byte)(216)))), ((int)(((byte)(254)))));
            this.gridViewPagCond.Appearance.GroupFooter.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(170)))), ((int)(((byte)(216)))), ((int)(((byte)(254)))));
            this.gridViewPagCond.Appearance.GroupFooter.ForeColor = System.Drawing.Color.Black;
            this.gridViewPagCond.Appearance.GroupFooter.Options.UseBackColor = true;
            this.gridViewPagCond.Appearance.GroupFooter.Options.UseBorderColor = true;
            this.gridViewPagCond.Appearance.GroupFooter.Options.UseForeColor = true;
            this.gridViewPagCond.Appearance.GroupPanel.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(236)))), ((int)(((byte)(246)))), ((int)(((byte)(255)))));
            this.gridViewPagCond.Appearance.GroupPanel.BackColor2 = System.Drawing.Color.White;
            this.gridViewPagCond.Appearance.GroupPanel.ForeColor = System.Drawing.Color.Black;
            this.gridViewPagCond.Appearance.GroupPanel.Options.UseBackColor = true;
            this.gridViewPagCond.Appearance.GroupPanel.Options.UseForeColor = true;
            this.gridViewPagCond.Appearance.GroupRow.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(170)))), ((int)(((byte)(216)))), ((int)(((byte)(254)))));
            this.gridViewPagCond.Appearance.GroupRow.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(170)))), ((int)(((byte)(216)))), ((int)(((byte)(254)))));
            this.gridViewPagCond.Appearance.GroupRow.ForeColor = System.Drawing.Color.Black;
            this.gridViewPagCond.Appearance.GroupRow.Options.UseBackColor = true;
            this.gridViewPagCond.Appearance.GroupRow.Options.UseBorderColor = true;
            this.gridViewPagCond.Appearance.GroupRow.Options.UseForeColor = true;
            this.gridViewPagCond.Appearance.HeaderPanel.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(139)))), ((int)(((byte)(201)))), ((int)(((byte)(254)))));
            this.gridViewPagCond.Appearance.HeaderPanel.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(139)))), ((int)(((byte)(201)))), ((int)(((byte)(254)))));
            this.gridViewPagCond.Appearance.HeaderPanel.ForeColor = System.Drawing.Color.Black;
            this.gridViewPagCond.Appearance.HeaderPanel.Options.UseBackColor = true;
            this.gridViewPagCond.Appearance.HeaderPanel.Options.UseBorderColor = true;
            this.gridViewPagCond.Appearance.HeaderPanel.Options.UseForeColor = true;
            this.gridViewPagCond.Appearance.HideSelectionRow.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(105)))), ((int)(((byte)(170)))), ((int)(((byte)(225)))));
            this.gridViewPagCond.Appearance.HideSelectionRow.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(83)))), ((int)(((byte)(155)))), ((int)(((byte)(215)))));
            this.gridViewPagCond.Appearance.HideSelectionRow.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(236)))), ((int)(((byte)(246)))), ((int)(((byte)(255)))));
            this.gridViewPagCond.Appearance.HideSelectionRow.Options.UseBackColor = true;
            this.gridViewPagCond.Appearance.HideSelectionRow.Options.UseBorderColor = true;
            this.gridViewPagCond.Appearance.HideSelectionRow.Options.UseForeColor = true;
            this.gridViewPagCond.Appearance.HorzLine.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(104)))), ((int)(((byte)(184)))), ((int)(((byte)(251)))));
            this.gridViewPagCond.Appearance.HorzLine.Options.UseBackColor = true;
            this.gridViewPagCond.Appearance.OddRow.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(236)))), ((int)(((byte)(246)))), ((int)(((byte)(255)))));
            this.gridViewPagCond.Appearance.OddRow.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(236)))), ((int)(((byte)(246)))), ((int)(((byte)(255)))));
            this.gridViewPagCond.Appearance.OddRow.ForeColor = System.Drawing.Color.Black;
            this.gridViewPagCond.Appearance.OddRow.Options.UseBackColor = true;
            this.gridViewPagCond.Appearance.OddRow.Options.UseBorderColor = true;
            this.gridViewPagCond.Appearance.OddRow.Options.UseForeColor = true;
            this.gridViewPagCond.Appearance.Preview.Font = new System.Drawing.Font("Verdana", 7.5F);
            this.gridViewPagCond.Appearance.Preview.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(83)))), ((int)(((byte)(155)))), ((int)(((byte)(215)))));
            this.gridViewPagCond.Appearance.Preview.Options.UseFont = true;
            this.gridViewPagCond.Appearance.Preview.Options.UseForeColor = true;
            this.gridViewPagCond.Appearance.Row.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(247)))), ((int)(((byte)(251)))), ((int)(((byte)(255)))));
            this.gridViewPagCond.Appearance.Row.ForeColor = System.Drawing.Color.Black;
            this.gridViewPagCond.Appearance.Row.Options.UseBackColor = true;
            this.gridViewPagCond.Appearance.Row.Options.UseForeColor = true;
            this.gridViewPagCond.Appearance.RowSeparator.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(236)))), ((int)(((byte)(246)))), ((int)(((byte)(255)))));
            this.gridViewPagCond.Appearance.RowSeparator.BackColor2 = System.Drawing.Color.White;
            this.gridViewPagCond.Appearance.RowSeparator.Options.UseBackColor = true;
            this.gridViewPagCond.Appearance.SelectedRow.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(83)))), ((int)(((byte)(155)))), ((int)(((byte)(215)))));
            this.gridViewPagCond.Appearance.SelectedRow.ForeColor = System.Drawing.Color.White;
            this.gridViewPagCond.Appearance.SelectedRow.Options.UseBackColor = true;
            this.gridViewPagCond.Appearance.SelectedRow.Options.UseForeColor = true;
            this.gridViewPagCond.Appearance.TopNewRow.BackColor = System.Drawing.Color.White;
            this.gridViewPagCond.Appearance.TopNewRow.Options.UseBackColor = true;
            this.gridViewPagCond.Appearance.VertLine.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(104)))), ((int)(((byte)(184)))), ((int)(((byte)(251)))));
            this.gridViewPagCond.Appearance.VertLine.Options.UseBackColor = true;
            this.gridViewPagCond.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colCONCodigo,
            this.colCONNome1,
            this.colCONRazaoCC,
            this.colCONBanco,
            this.colCONAgencia,
            this.colCONDigitoAgencia,
            this.colCONConta,
            this.colCONDigitoConta,
            this.colCONCNPJ,
            this.colCON__EMP,
            this.colID,
            this.colEMP1,
            this.colTotal,
            this.colRefLocal,
            this.colRHD_NOA,
            this.gridColumn4Nota,
            this.colNOA_PGF1,
            this.gridColumn4Periodico,
            this.colValorAnterior1,
            this.colValorAnterior2,
            this.colValorAnterior3,
            this.colValorAcumulado,
            this.colSaldoCC,
            this.colValorAnteriorMedio});
            this.gridViewPagCond.GridControl = this.gridControlCondom;
            this.gridViewPagCond.Name = "gridViewPagCond";
            this.gridViewPagCond.OptionsDetail.ShowDetailTabs = false;
            this.gridViewPagCond.OptionsView.ColumnAutoWidth = false;
            this.gridViewPagCond.OptionsView.EnableAppearanceEvenRow = true;
            this.gridViewPagCond.OptionsView.EnableAppearanceOddRow = true;
            this.gridViewPagCond.OptionsView.ShowAutoFilterRow = true;
            this.gridViewPagCond.OptionsView.ShowFooter = true;
            this.gridViewPagCond.OptionsView.ShowGroupPanel = false;
            this.gridViewPagCond.PaintStyleName = "Flat";
            this.gridViewPagCond.SortInfo.AddRange(new DevExpress.XtraGrid.Columns.GridColumnSortInfo[] {
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colCONNome1, DevExpress.Data.ColumnSortOrder.Ascending)});
            this.gridViewPagCond.RowCellStyle += new DevExpress.XtraGrid.Views.Grid.RowCellStyleEventHandler(this.gridViewPagCond_RowCellStyle);
            this.gridViewPagCond.CustomRowCellEdit += new DevExpress.XtraGrid.Views.Grid.CustomRowCellEditEventHandler(this.gridViewPagCond_CustomRowCellEdit);
            // 
            // colCONCodigo
            // 
            this.colCONCodigo.Caption = "CODCON";
            this.colCONCodigo.FieldName = "CONCodigo";
            this.colCONCodigo.Name = "colCONCodigo";
            this.colCONCodigo.OptionsColumn.ReadOnly = true;
            this.colCONCodigo.Visible = true;
            this.colCONCodigo.VisibleIndex = 0;
            this.colCONCodigo.Width = 98;
            // 
            // colCONNome1
            // 
            this.colCONNome1.Caption = "Nome";
            this.colCONNome1.FieldName = "CONNome";
            this.colCONNome1.Name = "colCONNome1";
            this.colCONNome1.OptionsColumn.ReadOnly = true;
            this.colCONNome1.Visible = true;
            this.colCONNome1.VisibleIndex = 1;
            this.colCONNome1.Width = 361;
            // 
            // colCONRazaoCC
            // 
            this.colCONRazaoCC.Caption = "Código Bancário";
            this.colCONRazaoCC.FieldName = "CONRazaoCC";
            this.colCONRazaoCC.Name = "colCONRazaoCC";
            this.colCONRazaoCC.OptionsColumn.ReadOnly = true;
            this.colCONRazaoCC.Width = 134;
            // 
            // colCONBanco
            // 
            this.colCONBanco.Caption = "Banco";
            this.colCONBanco.FieldName = "CONBanco";
            this.colCONBanco.Name = "colCONBanco";
            this.colCONBanco.OptionsColumn.ReadOnly = true;
            // 
            // colCONAgencia
            // 
            this.colCONAgencia.Caption = "Agência";
            this.colCONAgencia.FieldName = "CONAgencia";
            this.colCONAgencia.Name = "colCONAgencia";
            this.colCONAgencia.OptionsColumn.ReadOnly = true;
            // 
            // colCONDigitoAgencia
            // 
            this.colCONDigitoAgencia.Caption = "Dg Agencia";
            this.colCONDigitoAgencia.FieldName = "CONDigitoAgencia";
            this.colCONDigitoAgencia.Name = "colCONDigitoAgencia";
            this.colCONDigitoAgencia.OptionsColumn.ReadOnly = true;
            // 
            // colCONConta
            // 
            this.colCONConta.Caption = "Conta";
            this.colCONConta.FieldName = "CONConta";
            this.colCONConta.Name = "colCONConta";
            this.colCONConta.OptionsColumn.ReadOnly = true;
            // 
            // colCONDigitoConta
            // 
            this.colCONDigitoConta.Caption = "Dg";
            this.colCONDigitoConta.FieldName = "CONDigitoConta";
            this.colCONDigitoConta.Name = "colCONDigitoConta";
            this.colCONDigitoConta.OptionsColumn.ReadOnly = true;
            // 
            // colCONCNPJ
            // 
            this.colCONCNPJ.Caption = "CNPJ";
            this.colCONCNPJ.FieldName = "CONCNPJ";
            this.colCONCNPJ.Name = "colCONCNPJ";
            this.colCONCNPJ.OptionsColumn.ReadOnly = true;
            // 
            // colCON__EMP
            // 
            this.colCON__EMP.Caption = "EMP";
            this.colCON__EMP.FieldName = "CON__EMP";
            this.colCON__EMP.Name = "colCON__EMP";
            this.colCON__EMP.OptionsColumn.ReadOnly = true;
            // 
            // colID
            // 
            this.colID.FieldName = "TMPFolha";
            this.colID.Name = "colID";
            this.colID.OptionsColumn.ReadOnly = true;
            // 
            // colEMP1
            // 
            this.colEMP1.FieldName = "EMP";
            this.colEMP1.Name = "colEMP1";
            this.colEMP1.OptionsColumn.ReadOnly = true;
            // 
            // colTotal
            // 
            this.colTotal.AppearanceHeader.Options.UseTextOptions = true;
            this.colTotal.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colTotal.Caption = "Gerando";
            this.colTotal.ColumnEdit = this.CalcEdit1;
            this.colTotal.FieldName = "Total";
            this.colTotal.Name = "colTotal";
            this.colTotal.OptionsColumn.ReadOnly = true;
            this.colTotal.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum, "Total", "{0:n2}")});
            this.colTotal.Visible = true;
            this.colTotal.VisibleIndex = 6;
            this.colTotal.Width = 120;
            // 
            // colRefLocal
            // 
            this.colRefLocal.FieldName = "RefLocal";
            this.colRefLocal.Name = "colRefLocal";
            this.colRefLocal.OptionsColumn.ReadOnly = true;
            // 
            // colRHD_NOA
            // 
            this.colRHD_NOA.Caption = "NOA";
            this.colRHD_NOA.FieldName = "RHD_NOA";
            this.colRHD_NOA.Name = "colRHD_NOA";
            this.colRHD_NOA.OptionsColumn.ReadOnly = true;
            // 
            // gridColumn4Nota
            // 
            this.gridColumn4Nota.Caption = "gridColumn4";
            this.gridColumn4Nota.Fixed = DevExpress.XtraGrid.Columns.FixedStyle.Right;
            this.gridColumn4Nota.Name = "gridColumn4Nota";
            this.gridColumn4Nota.OptionsColumn.ShowCaption = false;
            this.gridColumn4Nota.Visible = true;
            this.gridColumn4Nota.VisibleIndex = 8;
            this.gridColumn4Nota.Width = 25;
            // 
            // colNOA_PGF1
            // 
            this.colNOA_PGF1.FieldName = "NOA_PGF";
            this.colNOA_PGF1.Name = "colNOA_PGF1";
            this.colNOA_PGF1.OptionsColumn.ReadOnly = true;
            // 
            // gridColumn4Periodico
            // 
            this.gridColumn4Periodico.Caption = "gridColumn4";
            this.gridColumn4Periodico.Fixed = DevExpress.XtraGrid.Columns.FixedStyle.Right;
            this.gridColumn4Periodico.Name = "gridColumn4Periodico";
            this.gridColumn4Periodico.OptionsColumn.ShowCaption = false;
            this.gridColumn4Periodico.Visible = true;
            this.gridColumn4Periodico.VisibleIndex = 9;
            this.gridColumn4Periodico.Width = 27;
            // 
            // colValorAnterior1
            // 
            this.colValorAnterior1.AppearanceCell.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.colValorAnterior1.AppearanceCell.Options.UseBackColor = true;
            this.colValorAnterior1.AppearanceHeader.Options.UseTextOptions = true;
            this.colValorAnterior1.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colValorAnterior1.DisplayFormat.FormatString = "n2";
            this.colValorAnterior1.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.colValorAnterior1.FieldName = "ValorAnterior1";
            this.colValorAnterior1.Name = "colValorAnterior1";
            this.colValorAnterior1.OptionsColumn.ReadOnly = true;
            this.colValorAnterior1.Visible = true;
            this.colValorAnterior1.VisibleIndex = 4;
            this.colValorAnterior1.Width = 90;
            // 
            // colValorAnterior2
            // 
            this.colValorAnterior2.AppearanceCell.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.colValorAnterior2.AppearanceCell.Options.UseBackColor = true;
            this.colValorAnterior2.AppearanceHeader.Options.UseTextOptions = true;
            this.colValorAnterior2.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colValorAnterior2.DisplayFormat.FormatString = "n2";
            this.colValorAnterior2.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.colValorAnterior2.FieldName = "ValorAnterior2";
            this.colValorAnterior2.Name = "colValorAnterior2";
            this.colValorAnterior2.OptionsColumn.ReadOnly = true;
            this.colValorAnterior2.Visible = true;
            this.colValorAnterior2.VisibleIndex = 3;
            this.colValorAnterior2.Width = 90;
            // 
            // colValorAnterior3
            // 
            this.colValorAnterior3.AppearanceCell.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.colValorAnterior3.AppearanceCell.Options.UseBackColor = true;
            this.colValorAnterior3.AppearanceHeader.Options.UseTextOptions = true;
            this.colValorAnterior3.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colValorAnterior3.DisplayFormat.FormatString = "n2";
            this.colValorAnterior3.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.colValorAnterior3.FieldName = "ValorAnterior3";
            this.colValorAnterior3.Name = "colValorAnterior3";
            this.colValorAnterior3.OptionsColumn.ReadOnly = true;
            this.colValorAnterior3.Visible = true;
            this.colValorAnterior3.VisibleIndex = 2;
            this.colValorAnterior3.Width = 90;
            // 
            // colValorAcumulado
            // 
            this.colValorAcumulado.AppearanceCell.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.colValorAcumulado.AppearanceCell.Options.UseBackColor = true;
            this.colValorAcumulado.AppearanceHeader.Options.UseTextOptions = true;
            this.colValorAcumulado.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colValorAcumulado.Caption = "Acumulado";
            this.colValorAcumulado.DisplayFormat.FormatString = "n2";
            this.colValorAcumulado.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.colValorAcumulado.FieldName = "ValorAcumulado";
            this.colValorAcumulado.Name = "colValorAcumulado";
            this.colValorAcumulado.Visible = true;
            this.colValorAcumulado.VisibleIndex = 5;
            this.colValorAcumulado.Width = 90;
            // 
            // colSaldoCC
            // 
            this.colSaldoCC.AppearanceCell.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(224)))), ((int)(((byte)(192)))));
            this.colSaldoCC.AppearanceCell.Options.UseBackColor = true;
            this.colSaldoCC.AppearanceHeader.Options.UseTextOptions = true;
            this.colSaldoCC.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colSaldoCC.Caption = "Saldo C.C.";
            this.colSaldoCC.DisplayFormat.FormatString = "n2";
            this.colSaldoCC.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.colSaldoCC.FieldName = "ValSaldoCC";
            this.colSaldoCC.Name = "colSaldoCC";
            this.colSaldoCC.Visible = true;
            this.colSaldoCC.VisibleIndex = 7;
            this.colSaldoCC.Width = 90;
            // 
            // colValorAnteriorMedio
            // 
            this.colValorAnteriorMedio.DisplayFormat.FormatString = "n2";
            this.colValorAnteriorMedio.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.colValorAnteriorMedio.FieldName = "ValorAnteriorMedio";
            this.colValorAnteriorMedio.Name = "colValorAnteriorMedio";
            // 
            // repositoryItemButtonNota1
            // 
            this.repositoryItemButtonNota1.AutoHeight = false;
            this.repositoryItemButtonNota1.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Glyph, "", -1, true, true, false, DevExpress.XtraEditors.ImageLocation.MiddleCenter, global::DP.Properties.Resources.Nota1, new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), serializableAppearanceObject2, "", null, null, true)});
            this.repositoryItemButtonNota1.Name = "repositoryItemButtonNota1";
            this.repositoryItemButtonNota1.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.HideTextEditor;
            this.repositoryItemButtonNota1.ButtonClick += new DevExpress.XtraEditors.Controls.ButtonPressedEventHandler(this.repositoryItemButtonNota1_ButtonClick);
            // 
            // repositoryItemButtonPeriodico1
            // 
            this.repositoryItemButtonPeriodico1.AutoHeight = false;
            this.repositoryItemButtonPeriodico1.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Glyph, "", -1, true, true, false, DevExpress.XtraEditors.ImageLocation.MiddleCenter, global::DP.Properties.Resources.perBr1, new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), serializableAppearanceObject3, "", null, null, true)});
            this.repositoryItemButtonPeriodico1.Name = "repositoryItemButtonPeriodico1";
            this.repositoryItemButtonPeriodico1.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.HideTextEditor;
            this.repositoryItemButtonPeriodico1.ButtonClick += new DevExpress.XtraEditors.Controls.ButtonPressedEventHandler(this.repositoryItemButtonPeriodico1_ButtonClick);
            // 
            // repositoryItemButtonCHEQUE
            // 
            this.repositoryItemButtonCHEQUE.AutoHeight = false;
            this.repositoryItemButtonCHEQUE.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Glyph, "", -1, true, true, false, DevExpress.XtraEditors.ImageLocation.MiddleCenter, global::DP.Properties.Resources.impCheque1, new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), serializableAppearanceObject4, "", null, null, true)});
            this.repositoryItemButtonCHEQUE.Name = "repositoryItemButtonCHEQUE";
            this.repositoryItemButtonCHEQUE.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.HideTextEditor;
            this.repositoryItemButtonCHEQUE.ButtonClick += new DevExpress.XtraEditors.Controls.ButtonPressedEventHandler(this.repositoryItemButtonCHEQUE_ButtonClick);
            // 
            // TabControlSalarios
            // 
            this.TabControlSalarios.Dock = System.Windows.Forms.DockStyle.Fill;
            this.TabControlSalarios.Location = new System.Drawing.Point(0, 113);
            this.TabControlSalarios.Name = "TabControlSalarios";
            this.TabControlSalarios.SelectedTabPage = this.TabSAL;
            this.TabControlSalarios.Size = new System.Drawing.Size(1438, 669);
            this.TabControlSalarios.TabIndex = 6;
            this.TabControlSalarios.TabPages.AddRange(new DevExpress.XtraTab.XtraTabPage[] {
            this.TabPAGP,
            this.TabSAL});
            // 
            // TabSAL
            // 
            this.TabSAL.Controls.Add(this.gridPagamentos);
            this.TabSAL.Name = "TabSAL";
            this.TabSAL.Size = new System.Drawing.Size(1432, 641);
            this.TabSAL.Text = "Pagamentos";
            // 
            // gridPagamentos
            // 
            this.gridPagamentos.DataMember = "RHTarefaSubdetalhe";
            this.gridPagamentos.DataSource = this.dAutomacaoBancariaBindingSource;
            this.gridPagamentos.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridPagamentos.Location = new System.Drawing.Point(0, 0);
            this.gridPagamentos.LookAndFeel.Style = DevExpress.LookAndFeel.LookAndFeelStyle.Flat;
            this.gridPagamentos.LookAndFeel.UseDefaultLookAndFeel = false;
            this.gridPagamentos.MainView = this.ViewPagamentos;
            this.gridPagamentos.Name = "gridPagamentos";
            this.gridPagamentos.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.clcValorPagamentos,
            this.repositoryItemImageComboBox1,
            this.LookUpConNome,
            this.repositoryTipoConta,
            this.repositoryItemImageComboBox8,
            this.repositoryItemButtonCheque1});
            this.gridPagamentos.Size = new System.Drawing.Size(1432, 641);
            this.gridPagamentos.TabIndex = 0;
            this.gridPagamentos.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.ViewPagamentos});
            // 
            // ViewPagamentos
            // 
            this.ViewPagamentos.Appearance.ColumnFilterButton.BackColor = System.Drawing.Color.Orange;
            this.ViewPagamentos.Appearance.ColumnFilterButton.BorderColor = System.Drawing.Color.Orange;
            this.ViewPagamentos.Appearance.ColumnFilterButton.ForeColor = System.Drawing.Color.DarkOrange;
            this.ViewPagamentos.Appearance.ColumnFilterButton.Options.UseBackColor = true;
            this.ViewPagamentos.Appearance.ColumnFilterButton.Options.UseBorderColor = true;
            this.ViewPagamentos.Appearance.ColumnFilterButton.Options.UseForeColor = true;
            this.ViewPagamentos.Appearance.ColumnFilterButtonActive.BackColor = System.Drawing.Color.DarkOrange;
            this.ViewPagamentos.Appearance.ColumnFilterButtonActive.BorderColor = System.Drawing.Color.DarkOrange;
            this.ViewPagamentos.Appearance.ColumnFilterButtonActive.ForeColor = System.Drawing.Color.White;
            this.ViewPagamentos.Appearance.ColumnFilterButtonActive.Options.UseBackColor = true;
            this.ViewPagamentos.Appearance.ColumnFilterButtonActive.Options.UseBorderColor = true;
            this.ViewPagamentos.Appearance.ColumnFilterButtonActive.Options.UseForeColor = true;
            this.ViewPagamentos.Appearance.Empty.BackColor = System.Drawing.Color.LightSkyBlue;
            this.ViewPagamentos.Appearance.Empty.BackColor2 = System.Drawing.Color.SkyBlue;
            this.ViewPagamentos.Appearance.Empty.GradientMode = System.Drawing.Drawing2D.LinearGradientMode.BackwardDiagonal;
            this.ViewPagamentos.Appearance.Empty.Options.UseBackColor = true;
            this.ViewPagamentos.Appearance.EvenRow.BackColor = System.Drawing.Color.Linen;
            this.ViewPagamentos.Appearance.EvenRow.BackColor2 = System.Drawing.Color.AntiqueWhite;
            this.ViewPagamentos.Appearance.EvenRow.GradientMode = System.Drawing.Drawing2D.LinearGradientMode.ForwardDiagonal;
            this.ViewPagamentos.Appearance.EvenRow.Options.UseBackColor = true;
            this.ViewPagamentos.Appearance.FilterCloseButton.BackColor = System.Drawing.Color.Orange;
            this.ViewPagamentos.Appearance.FilterCloseButton.BorderColor = System.Drawing.Color.Orange;
            this.ViewPagamentos.Appearance.FilterCloseButton.Options.UseBackColor = true;
            this.ViewPagamentos.Appearance.FilterCloseButton.Options.UseBorderColor = true;
            this.ViewPagamentos.Appearance.FilterPanel.BackColor = System.Drawing.Color.DarkOrange;
            this.ViewPagamentos.Appearance.FilterPanel.BackColor2 = System.Drawing.Color.Orange;
            this.ViewPagamentos.Appearance.FilterPanel.ForeColor = System.Drawing.Color.Black;
            this.ViewPagamentos.Appearance.FilterPanel.GradientMode = System.Drawing.Drawing2D.LinearGradientMode.BackwardDiagonal;
            this.ViewPagamentos.Appearance.FilterPanel.Options.UseBackColor = true;
            this.ViewPagamentos.Appearance.FilterPanel.Options.UseForeColor = true;
            this.ViewPagamentos.Appearance.FocusedRow.BackColor = System.Drawing.Color.RoyalBlue;
            this.ViewPagamentos.Appearance.FocusedRow.BackColor2 = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this.ViewPagamentos.Appearance.FocusedRow.ForeColor = System.Drawing.Color.White;
            this.ViewPagamentos.Appearance.FocusedRow.GradientMode = System.Drawing.Drawing2D.LinearGradientMode.Vertical;
            this.ViewPagamentos.Appearance.FocusedRow.Options.UseBackColor = true;
            this.ViewPagamentos.Appearance.FocusedRow.Options.UseForeColor = true;
            this.ViewPagamentos.Appearance.FooterPanel.BackColor = System.Drawing.Color.Orange;
            this.ViewPagamentos.Appearance.FooterPanel.BorderColor = System.Drawing.Color.Orange;
            this.ViewPagamentos.Appearance.FooterPanel.Options.UseBackColor = true;
            this.ViewPagamentos.Appearance.FooterPanel.Options.UseBorderColor = true;
            this.ViewPagamentos.Appearance.GroupButton.BackColor = System.Drawing.Color.Wheat;
            this.ViewPagamentos.Appearance.GroupButton.BorderColor = System.Drawing.Color.Wheat;
            this.ViewPagamentos.Appearance.GroupButton.Options.UseBackColor = true;
            this.ViewPagamentos.Appearance.GroupButton.Options.UseBorderColor = true;
            this.ViewPagamentos.Appearance.GroupFooter.BackColor = System.Drawing.Color.Wheat;
            this.ViewPagamentos.Appearance.GroupFooter.BorderColor = System.Drawing.Color.Wheat;
            this.ViewPagamentos.Appearance.GroupFooter.Options.UseBackColor = true;
            this.ViewPagamentos.Appearance.GroupFooter.Options.UseBorderColor = true;
            this.ViewPagamentos.Appearance.GroupPanel.BackColor = System.Drawing.Color.RoyalBlue;
            this.ViewPagamentos.Appearance.GroupPanel.ForeColor = System.Drawing.Color.White;
            this.ViewPagamentos.Appearance.GroupPanel.Options.UseBackColor = true;
            this.ViewPagamentos.Appearance.GroupPanel.Options.UseForeColor = true;
            this.ViewPagamentos.Appearance.GroupRow.BackColor = System.Drawing.Color.Wheat;
            this.ViewPagamentos.Appearance.GroupRow.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Bold);
            this.ViewPagamentos.Appearance.GroupRow.Options.UseBackColor = true;
            this.ViewPagamentos.Appearance.GroupRow.Options.UseFont = true;
            this.ViewPagamentos.Appearance.HeaderPanel.BackColor = System.Drawing.Color.Orange;
            this.ViewPagamentos.Appearance.HeaderPanel.BorderColor = System.Drawing.Color.Orange;
            this.ViewPagamentos.Appearance.HeaderPanel.Options.UseBackColor = true;
            this.ViewPagamentos.Appearance.HeaderPanel.Options.UseBorderColor = true;
            this.ViewPagamentos.Appearance.HideSelectionRow.BackColor = System.Drawing.Color.LightSlateGray;
            this.ViewPagamentos.Appearance.HideSelectionRow.Options.UseBackColor = true;
            this.ViewPagamentos.Appearance.HorzLine.BackColor = System.Drawing.Color.Tan;
            this.ViewPagamentos.Appearance.HorzLine.Options.UseBackColor = true;
            this.ViewPagamentos.Appearance.OddRow.BackColor = System.Drawing.Color.White;
            this.ViewPagamentos.Appearance.OddRow.Options.UseBackColor = true;
            this.ViewPagamentos.Appearance.Preview.BackColor = System.Drawing.Color.Khaki;
            this.ViewPagamentos.Appearance.Preview.BackColor2 = System.Drawing.Color.Cornsilk;
            this.ViewPagamentos.Appearance.Preview.Font = new System.Drawing.Font("Tahoma", 7.5F);
            this.ViewPagamentos.Appearance.Preview.GradientMode = System.Drawing.Drawing2D.LinearGradientMode.BackwardDiagonal;
            this.ViewPagamentos.Appearance.Preview.Options.UseBackColor = true;
            this.ViewPagamentos.Appearance.Preview.Options.UseFont = true;
            this.ViewPagamentos.Appearance.Row.BackColor = System.Drawing.Color.White;
            this.ViewPagamentos.Appearance.Row.Options.UseBackColor = true;
            this.ViewPagamentos.Appearance.RowSeparator.BackColor = System.Drawing.Color.LightSkyBlue;
            this.ViewPagamentos.Appearance.RowSeparator.Options.UseBackColor = true;
            this.ViewPagamentos.Appearance.VertLine.BackColor = System.Drawing.Color.Tan;
            this.ViewPagamentos.Appearance.VertLine.Options.UseBackColor = true;
            this.ViewPagamentos.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colCONNome,
            this.colFUNRegistro,
            this.colFUNNome,
            this.colFUNConta,
            this.colFUNBanco,
            this.colFUNAgencia,
            this.colFUNCPF,
            this.colFUNValor,
            this.colFUNTipoConta,
            this.colFUNnTipoPag,
            this.colFUNIDBanco,
            this.colEMP,
            this.colstrStatus,
            this.colTMPFolha,
            this.colTMPFolhaDet,
            this.colCHEStatus,
            this.colRHS_CHE1,
            this.gridColumnCheque1});
            this.ViewPagamentos.GridControl = this.gridPagamentos;
            this.ViewPagamentos.GroupCount = 1;
            this.ViewPagamentos.GroupSummary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridGroupSummaryItem(DevExpress.Data.SummaryItemType.Sum, "RHSValor", null, "(Total={0:c2})"),
            new DevExpress.XtraGrid.GridGroupSummaryItem(DevExpress.Data.SummaryItemType.Count, "FUNRegistro", null, "(Nº Funcionário={0:n0})")});
            this.ViewPagamentos.Name = "ViewPagamentos";
            this.ViewPagamentos.OptionsBehavior.AllowIncrementalSearch = true;
            this.ViewPagamentos.OptionsBehavior.AutoExpandAllGroups = true;
            this.ViewPagamentos.OptionsNavigation.EnterMoveNextColumn = true;
            this.ViewPagamentos.OptionsPrint.PrintFooter = false;
            this.ViewPagamentos.OptionsPrint.PrintGroupFooter = false;
            this.ViewPagamentos.OptionsView.ColumnAutoWidth = false;
            this.ViewPagamentos.OptionsView.EnableAppearanceEvenRow = true;
            this.ViewPagamentos.OptionsView.EnableAppearanceOddRow = true;
            this.ViewPagamentos.OptionsView.ShowAutoFilterRow = true;
            this.ViewPagamentos.OptionsView.ShowFooter = true;
            this.ViewPagamentos.OptionsView.ShowGroupedColumns = true;
            this.ViewPagamentos.OptionsView.ShowGroupPanel = false;
            this.ViewPagamentos.PaintStyleName = "Flat";
            this.ViewPagamentos.PreviewFieldName = "CONInfo";
            this.ViewPagamentos.SortInfo.AddRange(new DevExpress.XtraGrid.Columns.GridColumnSortInfo[] {
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colCONNome, DevExpress.Data.ColumnSortOrder.Ascending)});
            this.ViewPagamentos.RowCellStyle += new DevExpress.XtraGrid.Views.Grid.RowCellStyleEventHandler(this.ViewPagamentos_RowCellStyle);
            this.ViewPagamentos.CustomRowCellEdit += new DevExpress.XtraGrid.Views.Grid.CustomRowCellEditEventHandler(this.ViewPagamentos_CustomRowCellEdit);
            this.ViewPagamentos.FocusedRowChanged += new DevExpress.XtraGrid.Views.Base.FocusedRowChangedEventHandler(this.ViewPagamentos_FocusedRowChanged);
            this.ViewPagamentos.RowUpdated += new DevExpress.XtraGrid.Views.Base.RowObjectEventHandler(this.ViewPagamentos_RowUpdated);
            // 
            // colCONNome
            // 
            this.colCONNome.Caption = "Condomínio";
            this.colCONNome.FieldName = "CONNome";
            this.colCONNome.Name = "colCONNome";
            this.colCONNome.OptionsColumn.ReadOnly = true;
            this.colCONNome.Visible = true;
            this.colCONNome.VisibleIndex = 0;
            this.colCONNome.Width = 140;
            // 
            // colFUNRegistro
            // 
            this.colFUNRegistro.Caption = "Registro";
            this.colFUNRegistro.FieldName = "FUCRegistro";
            this.colFUNRegistro.Name = "colFUNRegistro";
            this.colFUNRegistro.OptionsColumn.ReadOnly = true;
            this.colFUNRegistro.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Count, "FUNRegistro", "{0}")});
            this.colFUNRegistro.Visible = true;
            this.colFUNRegistro.VisibleIndex = 1;
            this.colFUNRegistro.Width = 79;
            // 
            // colFUNNome
            // 
            this.colFUNNome.Caption = "Nome";
            this.colFUNNome.FieldName = "FUCNome";
            this.colFUNNome.Name = "colFUNNome";
            this.colFUNNome.OptionsColumn.ReadOnly = true;
            this.colFUNNome.Visible = true;
            this.colFUNNome.VisibleIndex = 2;
            this.colFUNNome.Width = 278;
            // 
            // colFUNConta
            // 
            this.colFUNConta.Caption = "Conta";
            this.colFUNConta.FieldName = "FUCConta";
            this.colFUNConta.Name = "colFUNConta";
            this.colFUNConta.OptionsColumn.ReadOnly = true;
            this.colFUNConta.Visible = true;
            this.colFUNConta.VisibleIndex = 4;
            // 
            // colFUNBanco
            // 
            this.colFUNBanco.Caption = "Banco";
            this.colFUNBanco.FieldName = "FUCBanco";
            this.colFUNBanco.Name = "colFUNBanco";
            this.colFUNBanco.OptionsColumn.AllowEdit = false;
            this.colFUNBanco.Visible = true;
            this.colFUNBanco.VisibleIndex = 3;
            this.colFUNBanco.Width = 47;
            // 
            // colFUNAgencia
            // 
            this.colFUNAgencia.Caption = "Agência";
            this.colFUNAgencia.FieldName = "FUCAgencia";
            this.colFUNAgencia.Name = "colFUNAgencia";
            this.colFUNAgencia.OptionsColumn.ReadOnly = true;
            this.colFUNAgencia.Visible = true;
            this.colFUNAgencia.VisibleIndex = 5;
            this.colFUNAgencia.Width = 60;
            // 
            // colFUNCPF
            // 
            this.colFUNCPF.Caption = "CPF";
            this.colFUNCPF.FieldName = "FUCCpf";
            this.colFUNCPF.Name = "colFUNCPF";
            this.colFUNCPF.OptionsColumn.ReadOnly = true;
            this.colFUNCPF.Visible = true;
            this.colFUNCPF.VisibleIndex = 6;
            this.colFUNCPF.Width = 117;
            // 
            // colFUNValor
            // 
            this.colFUNValor.Caption = "Valor";
            this.colFUNValor.ColumnEdit = this.clcValorPagamentos;
            this.colFUNValor.FieldName = "RHSValor";
            this.colFUNValor.Name = "colFUNValor";
            this.colFUNValor.OptionsColumn.AllowIncrementalSearch = false;
            this.colFUNValor.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum, "RHSValor", "{0:n2}")});
            this.colFUNValor.Visible = true;
            this.colFUNValor.VisibleIndex = 7;
            this.colFUNValor.Width = 91;
            // 
            // clcValorPagamentos
            // 
            this.clcValorPagamentos.AutoHeight = false;
            this.clcValorPagamentos.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.clcValorPagamentos.DisplayFormat.FormatString = "n2";
            this.clcValorPagamentos.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.clcValorPagamentos.EditFormat.FormatString = "n2";
            this.clcValorPagamentos.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.clcValorPagamentos.Mask.EditMask = "n2";
            this.clcValorPagamentos.Name = "clcValorPagamentos";
            // 
            // colFUNTipoConta
            // 
            this.colFUNTipoConta.Caption = "Tipo Conta";
            this.colFUNTipoConta.ColumnEdit = this.repositoryTipoConta;
            this.colFUNTipoConta.FieldName = "FUCTipoConta";
            this.colFUNTipoConta.Name = "colFUNTipoConta";
            this.colFUNTipoConta.OptionsColumn.AllowEdit = false;
            this.colFUNTipoConta.Visible = true;
            this.colFUNTipoConta.VisibleIndex = 8;
            this.colFUNTipoConta.Width = 121;
            // 
            // repositoryTipoConta
            // 
            this.repositoryTipoConta.AutoHeight = false;
            this.repositoryTipoConta.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryTipoConta.Name = "repositoryTipoConta";
            // 
            // colFUNnTipoPag
            // 
            this.colFUNnTipoPag.Caption = "Tipo Pagamento";
            this.colFUNnTipoPag.ColumnEdit = this.repositoryItemImageComboBox1;
            this.colFUNnTipoPag.FieldName = "RHSTipoPag";
            this.colFUNnTipoPag.Name = "colFUNnTipoPag";
            this.colFUNnTipoPag.Visible = true;
            this.colFUNnTipoPag.VisibleIndex = 9;
            this.colFUNnTipoPag.Width = 124;
            // 
            // repositoryItemImageComboBox1
            // 
            this.repositoryItemImageComboBox1.AutoHeight = false;
            this.repositoryItemImageComboBox1.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemImageComboBox1.Name = "repositoryItemImageComboBox1";
            // 
            // colFUNIDBanco
            // 
            this.colFUNIDBanco.Caption = "Banco destino";
            this.colFUNIDBanco.FieldName = "FUCBanco";
            this.colFUNIDBanco.Name = "colFUNIDBanco";
            this.colFUNIDBanco.OptionsColumn.ReadOnly = true;
            // 
            // colEMP
            // 
            this.colEMP.FieldName = "EMP";
            this.colEMP.Name = "colEMP";
            this.colEMP.OptionsColumn.ReadOnly = true;
            // 
            // colstrStatus
            // 
            this.colstrStatus.Caption = "Obs";
            this.colstrStatus.FieldName = "strStatus";
            this.colstrStatus.Name = "colstrStatus";
            this.colstrStatus.OptionsColumn.ReadOnly = true;
            this.colstrStatus.Visible = true;
            this.colstrStatus.VisibleIndex = 11;
            this.colstrStatus.Width = 253;
            // 
            // colTMPFolha
            // 
            this.colTMPFolha.FieldName = "TMPFolha";
            this.colTMPFolha.Name = "colTMPFolha";
            this.colTMPFolha.OptionsColumn.ReadOnly = true;
            // 
            // colTMPFolhaDet
            // 
            this.colTMPFolhaDet.Caption = "RHS";
            this.colTMPFolhaDet.FieldName = "RHS";
            this.colTMPFolhaDet.Name = "colTMPFolhaDet";
            this.colTMPFolhaDet.OptionsColumn.ReadOnly = true;
            // 
            // colCHEStatus
            // 
            this.colCHEStatus.Caption = "Status";
            this.colCHEStatus.ColumnEdit = this.repositoryItemImageComboBox8;
            this.colCHEStatus.FieldName = "CHEStatus";
            this.colCHEStatus.Name = "colCHEStatus";
            this.colCHEStatus.OptionsColumn.ReadOnly = true;
            this.colCHEStatus.Visible = true;
            this.colCHEStatus.VisibleIndex = 10;
            this.colCHEStatus.Width = 93;
            // 
            // repositoryItemImageComboBox8
            // 
            this.repositoryItemImageComboBox8.AutoHeight = false;
            this.repositoryItemImageComboBox8.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemImageComboBox8.Name = "repositoryItemImageComboBox8";
            // 
            // colRHS_CHE1
            // 
            this.colRHS_CHE1.Caption = "CHE";
            this.colRHS_CHE1.FieldName = "RHS_CHE";
            this.colRHS_CHE1.Name = "colRHS_CHE1";
            this.colRHS_CHE1.OptionsColumn.ReadOnly = true;
            // 
            // gridColumnCheque1
            // 
            this.gridColumnCheque1.Caption = "gridColumn4";
            this.gridColumnCheque1.Fixed = DevExpress.XtraGrid.Columns.FixedStyle.Right;
            this.gridColumnCheque1.Name = "gridColumnCheque1";
            this.gridColumnCheque1.OptionsColumn.ShowCaption = false;
            this.gridColumnCheque1.Visible = true;
            this.gridColumnCheque1.VisibleIndex = 12;
            this.gridColumnCheque1.Width = 25;
            // 
            // LookUpConNome
            // 
            this.LookUpConNome.AutoHeight = false;
            this.LookUpConNome.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LookUpConNome.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("CONCodigo", "CON Codigo", 81, DevExpress.Utils.FormatType.None, "", true, DevExpress.Utils.HorzAlignment.Near),
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("CONNome", "CON Nome", 62, DevExpress.Utils.FormatType.None, "", true, DevExpress.Utils.HorzAlignment.Near),
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("CONCNPJ", "CONCNPJ", 57, DevExpress.Utils.FormatType.None, "", true, DevExpress.Utils.HorzAlignment.Near)});
            this.LookUpConNome.DisplayMember = "CONNome";
            this.LookUpConNome.Name = "LookUpConNome";
            this.LookUpConNome.ReadOnly = true;
            this.LookUpConNome.ValueMember = "TMPFolha";
            // 
            // repositoryItemButtonCheque1
            // 
            this.repositoryItemButtonCheque1.AutoHeight = false;
            this.repositoryItemButtonCheque1.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Glyph, "", -1, true, true, false, DevExpress.XtraEditors.ImageLocation.MiddleCenter, global::DP.Properties.Resources.impCheque1, new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), serializableAppearanceObject5, "", null, null, true)});
            this.repositoryItemButtonCheque1.Name = "repositoryItemButtonCheque1";
            this.repositoryItemButtonCheque1.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.HideTextEditor;
            this.repositoryItemButtonCheque1.ButtonClick += new DevExpress.XtraEditors.Controls.ButtonPressedEventHandler(this.repositoryItemButtonCheque1_ButtonClick);
            // 
            // TabPAGP
            // 
            this.TabPAGP.Controls.Add(this.gridControlCondom);
            this.TabPAGP.Name = "TabPAGP";
            this.TabPAGP.Size = new System.Drawing.Size(1432, 641);
            this.TabPAGP.Text = "Condomínios";
            // 
            // panelControl1
            // 
            this.panelControl1.Controls.Add(this.simpleButton2);
            this.panelControl1.Controls.Add(this.botaoZerar);
            this.panelControl1.Controls.Add(this.lookUpEditPLA1);
            this.panelControl1.Controls.Add(this.lookUpEditPLA2);
            this.panelControl1.Controls.Add(this.lalookUpEditPLA1);
            this.panelControl1.Controls.Add(this.dateDataCheque);
            this.panelControl1.Controls.Add(this.LDataCheque);
            this.panelControl1.Controls.Add(this.dateCreditoConta);
            this.panelControl1.Controls.Add(this.LCreditoConta);
            this.panelControl1.Controls.Add(this.labelControl3);
            this.panelControl1.Controls.Add(this.labelControl2);
            this.panelControl1.Controls.Add(this.imageComboTipo);
            this.panelControl1.Controls.Add(this.cCompet1);
            this.panelControl1.Dock = System.Windows.Forms.DockStyle.Top;
            this.panelControl1.Location = new System.Drawing.Point(0, 0);
            this.panelControl1.Name = "panelControl1";
            this.panelControl1.Size = new System.Drawing.Size(1438, 113);
            this.panelControl1.TabIndex = 5;
            // 
            // simpleButton2
            // 
            this.simpleButton2.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.simpleButton2.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.simpleButton2.Appearance.Options.UseFont = true;
            this.simpleButton2.Image = ((System.Drawing.Image)(resources.GetObject("simpleButton2.Image")));
            this.simpleButton2.Location = new System.Drawing.Point(1235, 3);
            this.simpleButton2.Name = "simpleButton2";
            this.simpleButton2.Size = new System.Drawing.Size(181, 50);
            this.simpleButton2.TabIndex = 15;
            this.simpleButton2.Text = "Fechar";
            this.simpleButton2.Click += new System.EventHandler(this.simpleButton2_Click);
            // 
            // botaoZerar
            // 
            this.botaoZerar.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.botaoZerar.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.botaoZerar.Appearance.Options.UseFont = true;
            this.botaoZerar.Image = ((System.Drawing.Image)(resources.GetObject("botaoZerar.Image")));
            this.botaoZerar.Location = new System.Drawing.Point(1235, 59);
            this.botaoZerar.Name = "botaoZerar";
            this.botaoZerar.Size = new System.Drawing.Size(181, 49);
            this.botaoZerar.TabIndex = 14;
            this.botaoZerar.Text = "Zerar";
            this.botaoZerar.Visible = false;
            this.botaoZerar.Click += new System.EventHandler(this.botaoZerar_Click);
            // 
            // lookUpEditPLA1
            // 
            this.lookUpEditPLA1.Location = new System.Drawing.Point(54, 81);
            this.lookUpEditPLA1.Name = "lookUpEditPLA1";
            this.lookUpEditPLA1.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.lookUpEditPLA1.Properties.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("PLA", "PLA", 37, DevExpress.Utils.FormatType.None, "", true, DevExpress.Utils.HorzAlignment.Near),
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("PLADescricao", "PLADescricao", 70, DevExpress.Utils.FormatType.None, "", true, DevExpress.Utils.HorzAlignment.Near)});
            this.lookUpEditPLA1.Properties.DataSource = this.pLAnocontasBindingSource;
            this.lookUpEditPLA1.Properties.DisplayMember = "PLA";
            this.lookUpEditPLA1.Properties.ValueMember = "PLA";
            this.lookUpEditPLA1.Size = new System.Drawing.Size(155, 20);
            this.lookUpEditPLA1.TabIndex = 12;
            this.lookUpEditPLA1.EditValueChanged += new System.EventHandler(this.TrocaPLA);
            // 
            // pLAnocontasBindingSource
            // 
            this.pLAnocontasBindingSource.DataMember = "PLAnocontas";
            this.pLAnocontasBindingSource.DataSource = typeof(Framework.datasets.dPLAnocontas);
            // 
            // lookUpEditPLA2
            // 
            this.lookUpEditPLA2.Location = new System.Drawing.Point(233, 81);
            this.lookUpEditPLA2.Name = "lookUpEditPLA2";
            this.lookUpEditPLA2.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.lookUpEditPLA2.Properties.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("PLA", "PLA", 37, DevExpress.Utils.FormatType.None, "", true, DevExpress.Utils.HorzAlignment.Near),
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("PLADescricao", "PLADescricao", 70, DevExpress.Utils.FormatType.None, "", true, DevExpress.Utils.HorzAlignment.Near)});
            this.lookUpEditPLA2.Properties.DataSource = this.pLAnocontasBindingSource;
            this.lookUpEditPLA2.Properties.DisplayMember = "PLADescricao";
            this.lookUpEditPLA2.Properties.ValueMember = "PLA";
            this.lookUpEditPLA2.Size = new System.Drawing.Size(499, 20);
            this.lookUpEditPLA2.TabIndex = 13;
            this.lookUpEditPLA2.TabStop = false;
            this.lookUpEditPLA2.EditValueChanged += new System.EventHandler(this.TrocaPLA);
            // 
            // lalookUpEditPLA1
            // 
            this.lalookUpEditPLA1.Appearance.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lalookUpEditPLA1.Appearance.Options.UseFont = true;
            this.lalookUpEditPLA1.Location = new System.Drawing.Point(5, 82);
            this.lalookUpEditPLA1.Name = "lalookUpEditPLA1";
            this.lalookUpEditPLA1.Size = new System.Drawing.Size(43, 16);
            this.lalookUpEditPLA1.TabIndex = 11;
            this.lalookUpEditPLA1.Text = "Conta:";
            // 
            // dateDataCheque
            // 
            this.dateDataCheque.EditValue = null;
            this.dateDataCheque.Location = new System.Drawing.Point(452, 45);
            this.dateDataCheque.Name = "dateDataCheque";
            this.dateDataCheque.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dateDataCheque.Properties.Appearance.Options.UseFont = true;
            this.dateDataCheque.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.dateDataCheque.Properties.CalendarTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.dateDataCheque.Properties.DateTimeChanged += new System.EventHandler(this.dateDataCheque_Properties_DateTimeChanged);
            this.dateDataCheque.Size = new System.Drawing.Size(149, 24);
            this.dateDataCheque.TabIndex = 10;
            this.dateDataCheque.Visible = false;
            // 
            // LDataCheque
            // 
            this.LDataCheque.Appearance.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LDataCheque.Appearance.Options.UseFont = true;
            this.LDataCheque.Location = new System.Drawing.Point(358, 49);
            this.LDataCheque.Name = "LDataCheque";
            this.LDataCheque.Size = new System.Drawing.Size(88, 16);
            this.LDataCheque.TabIndex = 9;
            this.LDataCheque.Text = "Data Cheque:";
            this.LDataCheque.Visible = false;
            // 
            // dateCreditoConta
            // 
            this.dateCreditoConta.EditValue = null;
            this.dateCreditoConta.Location = new System.Drawing.Point(192, 45);
            this.dateCreditoConta.Name = "dateCreditoConta";
            this.dateCreditoConta.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dateCreditoConta.Properties.Appearance.Options.UseFont = true;
            this.dateCreditoConta.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.dateCreditoConta.Properties.CalendarTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.dateCreditoConta.Properties.DateTimeChanged += new System.EventHandler(this.dateCreditoConta_Properties_DateTimeChanged);
            this.dateCreditoConta.Size = new System.Drawing.Size(149, 24);
            this.dateCreditoConta.TabIndex = 8;
            this.dateCreditoConta.Visible = false;
            // 
            // LCreditoConta
            // 
            this.LCreditoConta.Appearance.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LCreditoConta.Appearance.Options.UseFont = true;
            this.LCreditoConta.Location = new System.Drawing.Point(5, 49);
            this.LCreditoConta.Name = "LCreditoConta";
            this.LCreditoConta.Size = new System.Drawing.Size(151, 16);
            this.LCreditoConta.TabIndex = 7;
            this.LCreditoConta.Text = "Data Crédito em conta:";
            this.LCreditoConta.Visible = false;
            // 
            // labelControl3
            // 
            this.labelControl3.Appearance.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl3.Appearance.Options.UseFont = true;
            this.labelControl3.Location = new System.Drawing.Point(358, 16);
            this.labelControl3.Name = "labelControl3";
            this.labelControl3.Size = new System.Drawing.Size(80, 16);
            this.labelControl3.TabIndex = 6;
            this.labelControl3.Text = "Competêcia:";
            // 
            // labelControl2
            // 
            this.labelControl2.Appearance.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl2.Appearance.Options.UseFont = true;
            this.labelControl2.Location = new System.Drawing.Point(5, 16);
            this.labelControl2.Name = "labelControl2";
            this.labelControl2.Size = new System.Drawing.Size(128, 16);
            this.labelControl2.TabIndex = 5;
            this.labelControl2.Text = "Tipo de Pagamento:";
            // 
            // imageComboTipo
            // 
            this.imageComboTipo.Location = new System.Drawing.Point(139, 13);
            this.imageComboTipo.Name = "imageComboTipo";
            this.imageComboTipo.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.imageComboTipo.Properties.Appearance.Options.UseFont = true;
            this.imageComboTipo.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.imageComboTipo.Properties.ReadOnly = true;
            this.imageComboTipo.Size = new System.Drawing.Size(202, 22);
            this.imageComboTipo.TabIndex = 4;
            // 
            // cCompet1
            // 
            this.cCompet1.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.cCompet1.Appearance.Options.UseBackColor = true;
            this.cCompet1.CaixaAltaGeral = true;
            this.cCompet1.Doc = System.Windows.Forms.DockStyle.None;
            this.cCompet1.Estado = CompontesBasicos.EstadosDosComponentes.Registrado;
            this.cCompet1.IsNull = false;
            this.cCompet1.Location = new System.Drawing.Point(444, 11);
            this.cCompet1.LookAndFeel.Style = DevExpress.LookAndFeel.LookAndFeelStyle.Office2003;
            this.cCompet1.LookAndFeel.UseDefaultLookAndFeel = false;
            this.cCompet1.Name = "cCompet1";
            this.cCompet1.PermiteNulo = false;
            this.cCompet1.ReadOnly = true;
            this.cCompet1.Size = new System.Drawing.Size(117, 24);
            this.cCompet1.somenteleitura = true;
            this.cCompet1.TabIndex = 3;
            this.cCompet1.Titulo = null;
            // 
            // toolStripPagamentos
            // 
            this.toolStripPagamentos.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.toolStripPagamentos.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.btnRemessaPagamentos,
            this.toolStripSeparator2,
            this.btnImprimirPagamentos});
            this.toolStripPagamentos.Location = new System.Drawing.Point(0, 782);
            this.toolStripPagamentos.Name = "toolStripPagamentos";
            this.toolStripPagamentos.ShowItemToolTips = false;
            this.toolStripPagamentos.Size = new System.Drawing.Size(1438, 25);
            this.toolStripPagamentos.TabIndex = 7;
            // 
            // btnRemessaPagamentos
            // 
            this.btnRemessaPagamentos.Image = global::DP.Properties.Resources.btnRemessaPagamentos;
            this.btnRemessaPagamentos.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btnRemessaPagamentos.Name = "btnRemessaPagamentos";
            this.btnRemessaPagamentos.Size = new System.Drawing.Size(55, 22);
            this.btnRemessaPagamentos.Text = "Gerar";
            this.btnRemessaPagamentos.Click += new System.EventHandler(this.btnRemessaPagamentos_Click);
            // 
            // toolStripSeparator2
            // 
            this.toolStripSeparator2.Name = "toolStripSeparator2";
            this.toolStripSeparator2.Size = new System.Drawing.Size(6, 25);
            // 
            // btnImprimirPagamentos
            // 
            this.btnImprimirPagamentos.Image = global::DP.Properties.Resources.btnImprimirPagamentos;
            this.btnImprimirPagamentos.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btnImprimirPagamentos.Name = "btnImprimirPagamentos";
            this.btnImprimirPagamentos.Size = new System.Drawing.Size(73, 22);
            this.btnImprimirPagamentos.Text = "Imprimir";
            this.btnImprimirPagamentos.Click += new System.EventHandler(this.btnImprimirPagamentos_Click);
            // 
            // cFolhaPag
            // 
            this.Controls.Add(this.TabControlSalarios);
            this.Controls.Add(this.panelControl1);
            this.Controls.Add(this.toolStripPagamentos);
            this.Doc = System.Windows.Forms.DockStyle.Fill;
            this.LookAndFeel.UseDefaultLookAndFeel = false;
            this.Name = "cFolhaPag";
            this.Size = new System.Drawing.Size(1438, 807);
            ((System.ComponentModel.ISupportInitialize)(this.StyleController_F)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridViewPagCondSdet)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.CalcEdit1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridControlCondom)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dAutomacaoBancariaBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dAutomacaoBancaria)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridViewPagCond)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemButtonNota1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemButtonPeriodico1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemButtonCHEQUE)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TabControlSalarios)).EndInit();
            this.TabControlSalarios.ResumeLayout(false);
            this.TabSAL.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gridPagamentos)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ViewPagamentos)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.clcValorPagamentos)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryTipoConta)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemImageComboBox1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemImageComboBox8)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LookUpConNome)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemButtonCheque1)).EndInit();
            this.TabPAGP.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.panelControl1)).EndInit();
            this.panelControl1.ResumeLayout(false);
            this.panelControl1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.lookUpEditPLA1.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pLAnocontasBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lookUpEditPLA2.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateDataCheque.Properties.CalendarTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateDataCheque.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateCreditoConta.Properties.CalendarTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateCreditoConta.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.imageComboTipo.Properties)).EndInit();
            this.toolStripPagamentos.ResumeLayout(false);
            this.toolStripPagamentos.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.BindingSource dAutomacaoBancariaBindingSource;
        private DevExpress.XtraTab.XtraTabControl TabControlSalarios;
        private DevExpress.XtraTab.XtraTabPage TabSAL;
        private DevExpress.XtraGrid.GridControl gridPagamentos;
        private DevExpress.XtraGrid.Views.Grid.GridView ViewPagamentos;
        private DevExpress.XtraGrid.Columns.GridColumn colCONNome;
        private DevExpress.XtraGrid.Columns.GridColumn colFUNRegistro;
        private DevExpress.XtraGrid.Columns.GridColumn colFUNNome;
        private DevExpress.XtraGrid.Columns.GridColumn colFUNConta;
        private DevExpress.XtraGrid.Columns.GridColumn colFUNBanco;
        private DevExpress.XtraGrid.Columns.GridColumn colFUNAgencia;
        private DevExpress.XtraGrid.Columns.GridColumn colFUNCPF;
        private DevExpress.XtraGrid.Columns.GridColumn colFUNValor;
        private DevExpress.XtraEditors.Repository.RepositoryItemCalcEdit clcValorPagamentos;
        private DevExpress.XtraGrid.Columns.GridColumn colFUNTipoConta;
        private DevExpress.XtraEditors.Repository.RepositoryItemImageComboBox repositoryTipoConta;
        private DevExpress.XtraGrid.Columns.GridColumn colFUNnTipoPag;
        private DevExpress.XtraEditors.Repository.RepositoryItemImageComboBox repositoryItemImageComboBox1;
        private DevExpress.XtraGrid.Columns.GridColumn colFUNIDBanco;
        private DevExpress.XtraGrid.Columns.GridColumn colEMP;
        private DevExpress.XtraGrid.Columns.GridColumn colstrStatus;
        private DevExpress.XtraGrid.Columns.GridColumn colTMPFolha;
        private DevExpress.XtraGrid.Columns.GridColumn colTMPFolhaDet;
        private DevExpress.XtraGrid.Columns.GridColumn colCHEStatus;
        private DevExpress.XtraEditors.Repository.RepositoryItemImageComboBox repositoryItemImageComboBox8;
        private DevExpress.XtraGrid.Columns.GridColumn colRHS_CHE1;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumnCheque1;
        private DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit LookUpConNome;
        private DevExpress.XtraEditors.Repository.RepositoryItemButtonEdit repositoryItemButtonCheque1;
        private DevExpress.XtraTab.XtraTabPage TabPAGP;
        private DevExpress.XtraGrid.GridControl gridControlCondom;
        private DevExpress.XtraGrid.Views.Grid.GridView gridViewPagCondSdet;
        private DevExpress.XtraGrid.Columns.GridColumn colFUNNome1;
        private DevExpress.XtraGrid.Columns.GridColumn colFUNValor1;
        private DevExpress.XtraEditors.Repository.RepositoryItemCalcEdit CalcEdit1;
        private DevExpress.XtraGrid.Columns.GridColumn colFUNRegistro1;
        private DevExpress.XtraGrid.Columns.GridColumn colFUNCPF1;
        private DevExpress.XtraGrid.Columns.GridColumn colstrStatus1;
        private DevExpress.XtraGrid.Columns.GridColumn colRHS_CHE;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn4CHEque;
        private DevExpress.XtraGrid.Views.Grid.GridView gridViewPagCond;
        private DevExpress.XtraGrid.Columns.GridColumn colCONCodigo;
        private DevExpress.XtraGrid.Columns.GridColumn colCONNome1;
        private DevExpress.XtraGrid.Columns.GridColumn colCONRazaoCC;
        private DevExpress.XtraGrid.Columns.GridColumn colCONBanco;
        private DevExpress.XtraGrid.Columns.GridColumn colCONAgencia;
        private DevExpress.XtraGrid.Columns.GridColumn colCONDigitoAgencia;
        private DevExpress.XtraGrid.Columns.GridColumn colCONConta;
        private DevExpress.XtraGrid.Columns.GridColumn colCONDigitoConta;
        private DevExpress.XtraGrid.Columns.GridColumn colCONCNPJ;
        private DevExpress.XtraGrid.Columns.GridColumn colCON__EMP;
        private DevExpress.XtraGrid.Columns.GridColumn colID;
        private DevExpress.XtraGrid.Columns.GridColumn colEMP1;
        private DevExpress.XtraGrid.Columns.GridColumn colTotal;
        private DevExpress.XtraGrid.Columns.GridColumn colRefLocal;
        private DevExpress.XtraGrid.Columns.GridColumn colRHD_NOA;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn4Nota;
        private DevExpress.XtraGrid.Columns.GridColumn colNOA_PGF1;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn4Periodico;
        private DevExpress.XtraGrid.Columns.GridColumn colValorAnterior1;
        private DevExpress.XtraGrid.Columns.GridColumn colValorAnterior2;
        private DevExpress.XtraGrid.Columns.GridColumn colValorAnterior3;
        private DevExpress.XtraEditors.Repository.RepositoryItemButtonEdit repositoryItemButtonNota1;
        private DevExpress.XtraEditors.Repository.RepositoryItemButtonEdit repositoryItemButtonPeriodico1;
        private DevExpress.XtraEditors.Repository.RepositoryItemButtonEdit repositoryItemButtonCHEQUE;
        private DevExpress.XtraEditors.PanelControl panelControl1;
        private DevExpress.XtraEditors.LookUpEdit lookUpEditPLA1;
        private DevExpress.XtraEditors.LookUpEdit lookUpEditPLA2;
        private DevExpress.XtraEditors.LabelControl lalookUpEditPLA1;
        /// <summary>
        /// 
        /// </summary>
        public DevExpress.XtraEditors.DateEdit dateDataCheque;
        private DevExpress.XtraEditors.LabelControl LDataCheque;
        /// <summary>
        /// 
        /// </summary>
        public DevExpress.XtraEditors.DateEdit dateCreditoConta;
        private DevExpress.XtraEditors.LabelControl LCreditoConta;
        private DevExpress.XtraEditors.LabelControl labelControl3;
        private DevExpress.XtraEditors.LabelControl labelControl2;
        private DevExpress.XtraEditors.ImageComboBoxEdit imageComboTipo;
        private Framework.objetosNeon.cCompet cCompet1;
        private Automacao_Bancaria.dAutomacaoBancaria dAutomacaoBancaria;
        private System.Windows.Forms.BindingSource pLAnocontasBindingSource;
        private System.Windows.Forms.ToolStrip toolStripPagamentos;
        private System.Windows.Forms.ToolStripButton btnRemessaPagamentos;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator2;
        private System.Windows.Forms.ToolStripButton btnImprimirPagamentos;
        private DevExpress.XtraEditors.SimpleButton simpleButton2;
        private DevExpress.XtraEditors.SimpleButton botaoZerar;
        private DevExpress.XtraGrid.Columns.GridColumn colValorAcumulado;
        private DevExpress.XtraGrid.Columns.GridColumn colSaldoCC;
        private DevExpress.XtraGrid.Columns.GridColumn colValorAnteriorMedio;
    }
}
