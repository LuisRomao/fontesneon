﻿using FrameworkProc;

namespace DP.Codigos_Bancarios
{
    partial class dCodigosBancariosGrade
    {
        private void Carregar(EMPTipo EMPTipo)
        {
            foreach (FrameworkProc.datasets.dCondominiosAtivos.CONDOMINIOSRow row in FrameworkProc.datasets.dCondominiosAtivos.dCondominiosAtivosSTX(EMPTipo).CONDOMINIOS)
            {
                CondominiosDPRow novarow = CondominiosDP.NewCondominiosDPRow();
                foreach (System.Data.DataColumn DC in CondominiosDP.Columns)
                {                    
                    if (row[DC.ColumnName] != System.DBNull.Value)
                        novarow[DC] = row[DC.ColumnName];
                }
                CondominiosDP.AddCondominiosDPRow(novarow);
            }
        }

        internal void Carregar()
        {
            CondominiosDP.Clear();
            Carregar(EMPTipo.Local);
            Carregar(EMPTipo.Filial);
        }
    }
}

