namespace DP.Codigos_Bancarios
{
    partial class cCodigosBancariosGrade
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.colCONCodigoFolha1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colCONCodigoFolha2 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colCONCodigoBancoCC = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colCONCnpj = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colDados_Bancarios = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colCON = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colCopiaCON = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colCONPlanilhaPonto = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colCONPagEletronico = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colCONGPSEletronico = new DevExpress.XtraGrid.Columns.GridColumn();
            this.dCodigosBancariosGrade = new DP.Codigos_Bancarios.dCodigosBancariosGrade();
            this.colCON_EMP = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colCON1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colCONSefip = new DevExpress.XtraGrid.Columns.GridColumn();
            ((System.ComponentModel.ISupportInitialize)(this.PopImprimir_F)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.BarManager_F)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.PopExportar_F)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.GridControl_F)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.GridView_F)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.BindingSource_F)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.StyleController_F)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dCodigosBancariosGrade)).BeginInit();
            this.SuspendLayout();
            // 
            // barNavegacao_F
            // 
            this.barNavegacao_F.OptionsBar.AllowQuickCustomization = false;
            this.barNavegacao_F.OptionsBar.DisableClose = true;
            this.barNavegacao_F.OptionsBar.DisableCustomization = true;
            this.barNavegacao_F.OptionsBar.DrawDragBorder = false;
            this.barNavegacao_F.OptionsBar.DrawSizeGrip = true;
            this.barNavegacao_F.OptionsBar.RotateWhenVertical = false;
            this.barNavegacao_F.OptionsBar.UseWholeRow = true;
            // 
            // BarManager_F
            // 
            this.BarManager_F.MaxItemId = 20;
            // 
            // BtnIncluir_F
            // 
            this.BtnIncluir_F.Visibility = DevExpress.XtraBars.BarItemVisibility.Never;
            // 
            // BtnAlterar_F
            // 
            this.BtnAlterar_F.Enabled = false;
            // 
            // BtnExcluir_F
            // 
            this.BtnExcluir_F.Enabled = false;
            // 
            // BtnImprimir_F
            // 
            this.BtnImprimir_F.Enabled = false;
            // 
            // BtnExportar_F
            // 
            this.BtnExportar_F.Enabled = false;
            // 
            // barDataBase_F
            // 
            this.barDataBase_F.OptionsBar.AllowQuickCustomization = false;
            this.barDataBase_F.OptionsBar.DisableClose = true;
            this.barDataBase_F.OptionsBar.DisableCustomization = true;
            this.barDataBase_F.OptionsBar.DrawDragBorder = false;
            this.barDataBase_F.OptionsBar.UseWholeRow = true;
            // 
            // GridControl_F
            // 
            this.GridControl_F.Cursor = System.Windows.Forms.Cursors.Default;
            this.GridControl_F.EmbeddedNavigator.Buttons.Append.Visible = false;
            this.GridControl_F.EmbeddedNavigator.Buttons.CancelEdit.Visible = false;
            this.GridControl_F.EmbeddedNavigator.Buttons.Edit.Visible = false;
            this.GridControl_F.EmbeddedNavigator.Buttons.EndEdit.Visible = false;
            this.GridControl_F.EmbeddedNavigator.Buttons.Remove.Visible = false;
            this.GridControl_F.Size = new System.Drawing.Size(1557, 739);
            // 
            // GridView_F
            // 
            this.GridView_F.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colCONCnpj,
            this.colCONCodigoFolha1,
            this.colCONCodigoFolha2,
            this.colCONCodigoBancoCC,
            this.colDados_Bancarios,
            this.colCON,
            this.colCopiaCON,
            this.colCONPlanilhaPonto,
            this.colCONPagEletronico,
            this.colCONGPSEletronico,
            this.colCON_EMP,
            this.colCON1,
            this.colCONSefip});
            this.GridView_F.OptionsBehavior.AllowIncrementalSearch = true;
            this.GridView_F.OptionsBehavior.Editable = false;
            this.GridView_F.OptionsLayout.Columns.StoreAllOptions = true;
            this.GridView_F.OptionsLayout.Columns.StoreAppearance = true;
            this.GridView_F.OptionsLayout.StoreAllOptions = true;
            this.GridView_F.OptionsLayout.StoreAppearance = true;
            this.GridView_F.OptionsNavigation.EnterMoveNextColumn = true;
            this.GridView_F.OptionsPrint.EnableAppearanceEvenRow = true;
            this.GridView_F.OptionsPrint.EnableAppearanceOddRow = true;
            this.GridView_F.OptionsPrint.ExpandAllDetails = true;
            this.GridView_F.OptionsView.ColumnAutoWidth = false;
            this.GridView_F.OptionsView.GroupFooterShowMode = DevExpress.XtraGrid.Views.Grid.GroupFooterShowMode.VisibleAlways;
            this.GridView_F.OptionsView.ShowAutoFilterRow = true;
            this.GridView_F.OptionsView.ShowFooter = true;
            this.GridView_F.OptionsView.ShowGroupPanel = false;
            this.GridView_F.PaintStyleName = "Skin";
            this.GridView_F.PreviewFieldName = "Dados_Bancarios";
            this.GridView_F.SortInfo.AddRange(new DevExpress.XtraGrid.Columns.GridColumnSortInfo[] {
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colCON_EMP, DevExpress.Data.ColumnSortOrder.Ascending),
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colCON, DevExpress.Data.ColumnSortOrder.Ascending)});
            this.GridView_F.RowStyle += new DevExpress.XtraGrid.Views.Grid.RowStyleEventHandler(this.GridView_F_RowStyle);
            this.GridView_F.RowUpdated += new DevExpress.XtraGrid.Views.Base.RowObjectEventHandler(this.GridView_F_RowUpdated);
            // 
            // BindingSource_F
            // 
            this.BindingSource_F.DataMember = "CondominiosDP";
            this.BindingSource_F.DataSource = this.dCodigosBancariosGrade;
            // 
            // ToolTipController_F
            // 
            this.ToolTipController_F.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(222)))), ((int)(((byte)(237)))), ((int)(((byte)(250)))));
            this.ToolTipController_F.Appearance.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(20)))), ((int)(((byte)(69)))), ((int)(((byte)(141)))));
            this.ToolTipController_F.Appearance.Options.UseBackColor = true;
            this.ToolTipController_F.Appearance.Options.UseFont = true;
            this.ToolTipController_F.Appearance.Options.UseForeColor = true;
            this.ToolTipController_F.Appearance.Options.UseTextOptions = true;
            this.ToolTipController_F.Appearance.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            // 
            // StyleController_F
            // 
            this.StyleController_F.AppearanceDisabled.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this.StyleController_F.AppearanceDisabled.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this.StyleController_F.AppearanceDisabled.Options.UseBackColor = true;
            this.StyleController_F.AppearanceDisabled.Options.UseForeColor = true;
            this.StyleController_F.AppearanceFocused.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.StyleController_F.AppearanceFocused.Options.UseBackColor = true;
            // 
            // colCONCodigoFolha1
            // 
            this.colCONCodigoFolha1.Caption = "C�digo 1";
            this.colCONCodigoFolha1.FieldName = "CONCodigoFolha1";
            this.colCONCodigoFolha1.Name = "colCONCodigoFolha1";
            this.colCONCodigoFolha1.OptionsColumn.AllowIncrementalSearch = false;
            this.colCONCodigoFolha1.Visible = true;
            this.colCONCodigoFolha1.VisibleIndex = 3;
            this.colCONCodigoFolha1.Width = 72;
            // 
            // colCONCodigoFolha2
            // 
            this.colCONCodigoFolha2.Caption = "C�digo 2";
            this.colCONCodigoFolha2.FieldName = "CONCodigoFolha2A";
            this.colCONCodigoFolha2.Name = "colCONCodigoFolha2";
            this.colCONCodigoFolha2.OptionsColumn.AllowIncrementalSearch = false;
            this.colCONCodigoFolha2.Visible = true;
            this.colCONCodigoFolha2.VisibleIndex = 4;
            this.colCONCodigoFolha2.Width = 69;
            // 
            // colCONCodigoBancoCC
            // 
            this.colCONCodigoBancoCC.Caption = "Cr�dito em Conta";
            this.colCONCodigoBancoCC.FieldName = "CONCodigoBancoCC";
            this.colCONCodigoBancoCC.Name = "colCONCodigoBancoCC";
            this.colCONCodigoBancoCC.OptionsColumn.AllowIncrementalSearch = false;
            this.colCONCodigoBancoCC.Visible = true;
            this.colCONCodigoBancoCC.VisibleIndex = 5;
            this.colCONCodigoBancoCC.Width = 115;
            // 
            // colCONCnpj
            // 
            this.colCONCnpj.AppearanceCell.BackColor = System.Drawing.Color.WhiteSmoke;
            this.colCONCnpj.AppearanceCell.Options.UseBackColor = true;
            this.colCONCnpj.Caption = "Cnpj";
            this.colCONCnpj.FieldName = "CONCnpj";
            this.colCONCnpj.Name = "colCONCnpj";
            this.colCONCnpj.OptionsColumn.ReadOnly = true;
            this.colCONCnpj.Visible = true;
            this.colCONCnpj.VisibleIndex = 2;
            this.colCONCnpj.Width = 121;
            // 
            // colDados_Bancarios
            // 
            this.colDados_Bancarios.Caption = "Dados Bancarios";
            this.colDados_Bancarios.FieldName = "Dados_Bancarios";
            this.colDados_Bancarios.Name = "colDados_Bancarios";
            this.colDados_Bancarios.OptionsColumn.ReadOnly = true;
            this.colDados_Bancarios.Visible = true;
            this.colDados_Bancarios.VisibleIndex = 10;
            this.colDados_Bancarios.Width = 418;
            // 
            // colCON
            // 
            this.colCON.Caption = "CODCON";
            this.colCON.FieldName = "CONCodigo";
            this.colCON.FilterMode = DevExpress.XtraGrid.ColumnFilterMode.DisplayText;
            this.colCON.Name = "colCON";
            this.colCON.OptionsColumn.ReadOnly = true;
            this.colCON.Visible = true;
            this.colCON.VisibleIndex = 0;
            this.colCON.Width = 73;
            // 
            // colCopiaCON
            // 
            this.colCopiaCON.Caption = "Nome";
            this.colCopiaCON.FieldName = "CONNome";
            this.colCopiaCON.FilterMode = DevExpress.XtraGrid.ColumnFilterMode.DisplayText;
            this.colCopiaCON.Name = "colCopiaCON";
            this.colCopiaCON.OptionsColumn.ReadOnly = true;
            this.colCopiaCON.Visible = true;
            this.colCopiaCON.VisibleIndex = 1;
            this.colCopiaCON.Width = 218;
            // 
            // colCONPlanilhaPonto
            // 
            this.colCONPlanilhaPonto.Caption = "Plan Ponto";
            this.colCONPlanilhaPonto.FieldName = "CONPlanilhaPonto";
            this.colCONPlanilhaPonto.Name = "colCONPlanilhaPonto";
            this.colCONPlanilhaPonto.Visible = true;
            this.colCONPlanilhaPonto.VisibleIndex = 7;
            this.colCONPlanilhaPonto.Width = 86;
            // 
            // colCONPagEletronico
            // 
            this.colCONPagEletronico.Caption = "Sal�rio Eletr�nico";
            this.colCONPagEletronico.FieldName = "CONPagEletronico";
            this.colCONPagEletronico.Name = "colCONPagEletronico";
            this.colCONPagEletronico.Visible = true;
            this.colCONPagEletronico.VisibleIndex = 8;
            this.colCONPagEletronico.Width = 100;
            // 
            // colCONGPSEletronico
            // 
            this.colCONGPSEletronico.Caption = "GPS/DARF/FGTS Eletr�nico";
            this.colCONGPSEletronico.FieldName = "CONGPSEletronico";
            this.colCONGPSEletronico.Name = "colCONGPSEletronico";
            this.colCONGPSEletronico.Visible = true;
            this.colCONGPSEletronico.VisibleIndex = 9;
            this.colCONGPSEletronico.Width = 90;
            // 
            // dCodigosBancariosGrade
            // 
            this.dCodigosBancariosGrade.DataSetName = "dCodigosBancariosGrade";
            this.dCodigosBancariosGrade.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // colCON_EMP
            // 
            this.colCON_EMP.FieldName = "CON_EMP";
            this.colCON_EMP.Name = "colCON_EMP";
            this.colCON_EMP.OptionsColumn.ReadOnly = true;
            // 
            // colCON1
            // 
            this.colCON1.FieldName = "CON";
            this.colCON1.Name = "colCON1";
            this.colCON1.OptionsColumn.ReadOnly = true;
            // 
            // colCONSefip
            // 
            this.colCONSefip.Caption = "SEFIP";
            this.colCONSefip.FieldName = "CONSefip";
            this.colCONSefip.Name = "colCONSefip";
            this.colCONSefip.Visible = true;
            this.colCONSefip.VisibleIndex = 6;
            // 
            // cCodigosBancariosGrade
            // 
            this.BindingSourcePrincipal = this.BindingSource_F;
            this.BotaoIncluir = false;
            this.LookAndFeel.UseDefaultLookAndFeel = false;
            this.Name = "cCodigosBancariosGrade";
            this.Size = new System.Drawing.Size(1557, 799);
            this.cargaInicial += new System.EventHandler(this.cCodigosBancariosGrade_cargaInicial);
            this.cargaPrincipal += new System.EventHandler(this.cCodigosBancariosGrade_cargaPrincipal);
            ((System.ComponentModel.ISupportInitialize)(this.PopImprimir_F)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.BarManager_F)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.PopExportar_F)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.GridControl_F)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.GridView_F)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.BindingSource_F)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.StyleController_F)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dCodigosBancariosGrade)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private DevExpress.XtraGrid.Columns.GridColumn colCONCodigoFolha1;
        private DevExpress.XtraGrid.Columns.GridColumn colCONCodigoFolha2;
        private DevExpress.XtraGrid.Columns.GridColumn colCONCodigoBancoCC;
        private DevExpress.XtraGrid.Columns.GridColumn colCONCnpj;
        private DevExpress.XtraGrid.Columns.GridColumn colDados_Bancarios;
        private DevExpress.XtraGrid.Columns.GridColumn colCON;
        private DevExpress.XtraGrid.Columns.GridColumn colCopiaCON;
        private DevExpress.XtraGrid.Columns.GridColumn colCONPlanilhaPonto;
        private DevExpress.XtraGrid.Columns.GridColumn colCONPagEletronico;
        private DevExpress.XtraGrid.Columns.GridColumn colCONGPSEletronico;
        private DevExpress.XtraGrid.Columns.GridColumn colCON_EMP;
        private DevExpress.XtraGrid.Columns.GridColumn colCON1;
        private dCodigosBancariosGrade dCodigosBancariosGrade;
        private DevExpress.XtraGrid.Columns.GridColumn colCONSefip;
    }
}
