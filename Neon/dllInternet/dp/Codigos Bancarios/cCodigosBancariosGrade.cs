/*
MR - 30/07/2014 16:00            - Ajusta das nomenclaturas de Sal�rio Eletr�nico e GPS Eletr�nico para n�o confundir com Pagamento Eletr�nico
*/

using System;
using CompontesBasicosProc;
using FrameworkProc;

namespace DP.Codigos_Bancarios
{
    /// <summary>
    /// 
    /// </summary>
    public partial class cCodigosBancariosGrade : CompontesBasicos.ComponenteGradeNavegador
    {
        /// <summary>
        /// 
        /// </summary>
        public cCodigosBancariosGrade()
        {
            InitializeComponent();            
        }

        /// <summary>
        /// 
        /// </summary>
        public override void RefreshDados()
        {
            FrameworkProc.datasets.dCondominiosAtivos.dCondominiosTodosST.CarregaCondominios();
            dCodigosBancariosGrade.Carregar();
        }

        private void cCodigosBancariosGrade_cargaInicial(object sender, EventArgs e)
        {
            //BindingSource_F.DataSource = Framework.datasets.dCondominiosAtivos.dCondominiosTodosST;
            //TableAdapterPrincipal = Framework.datasets.dCondominiosAtivos.dCondominiosTodosST.CONDOMINIOSTableAdapter;         
        }

        private void cCodigosBancariosGrade_cargaPrincipal(object sender, EventArgs e)
        {
            dCodigosBancariosGrade.Carregar();
        }

        private void GridView_F_RowStyle(object sender, DevExpress.XtraGrid.Views.Grid.RowStyleEventArgs e)
        {
            dCodigosBancariosGrade.CondominiosDPRow row = (dCodigosBancariosGrade.CondominiosDPRow)GridView_F.GetDataRow(e.RowHandle);
            if (row == null)
                return;
            e.Appearance.BackColor = row.CON_EMP == Framework.DSCentral.EMP ? System.Drawing.Color.PaleGreen : System.Drawing.Color.LemonChiffon;            
        }

        private void GridView_F_RowUpdated(object sender, DevExpress.XtraGrid.Views.Base.RowObjectEventArgs e)
        {
            dCodigosBancariosGrade.CondominiosDPRow row = (dCodigosBancariosGrade.CondominiosDPRow)GridView_F.GetDataRow(e.RowHandle);
            if (row == null)
                return;
            EMPTipo EMPTipo = row.CON_EMP == Framework.DSCentral.EMP ? EMPTipo.Local: EMPTipo.Filial;
            FrameworkProc.datasets.dCondominiosAtivos DS = FrameworkProc.datasets.dCondominiosAtivos.dCondominiosAtivosSTX(EMPTipo);
            FrameworkProc.datasets.dCondominiosAtivos.CONDOMINIOSRow rowCON = DS.CONDOMINIOS.FindByCON(row.CON);
            foreach (System.Data.DataColumn DC in dCodigosBancariosGrade.CondominiosDP.Columns)
                if (DC.ColumnName.EstaNoGrupo("CONCodigoBancoCC", "CONCodigoFolha1", "CONCodigoFolha2A", "CONPagEletronico", "CONPlanilhaPonto", "CONGPSEletronico", "CONSefip"))
                {                    
                    if (row[DC.ColumnName] != System.DBNull.Value)
                        rowCON[DC.ColumnName] = row[DC];
                }
            DS.CONDOMINIOSTableAdapter.Update(rowCON);
        }       
    }
}

