using System;
using System.Drawing;
using System.Windows.Forms;
using DevExpress.XtraReports.UI;

namespace DP.RelPonto
{
    /// <summary>
    /// 
    /// </summary>
    public partial class cRelPonto : CompontesBasicos.ComponenteBase
    {
        /// <summary>
        /// 
        /// </summary>
        public cRelPonto()
        {
            InitializeComponent();
            bindingSourceUSU.DataSource = FrameworkProc.datasets.dUSUarios.dUSUariosSt;
            cCompet1.Comp.Ano = DateTime.Today.Year;
            cCompet1.Comp.Mes = DateTime.Today.Month;
            cCompet1.Comp--;
            ajustaDatas();
        }

        private void simpleButton1_Click(object sender, EventArgs e)
        {
            Calcular();            
        }

        private void Calcular()
        {
            if ((DataI.DateTime > DateTime.MinValue) && (DataF.DateTime > DateTime.MinValue))
            {
                dRelPonto.PONtoTableAdapter.Fill(dRelPonto.PONto, DataI.DateTime, DataF.DateTime);                   
                Completo = false;
            }
        }

        private void gridView1_RowCellStyle(object sender, DevExpress.XtraGrid.Views.Grid.RowCellStyleEventArgs e)
        {
            dRelPonto.PONtoRow LinhaEmFoco = (dRelPonto.PONtoRow)gridView1.GetDataRow(e.RowHandle);
            if (LinhaEmFoco == null)
                return;
            if ((e.Column == colPONEntrada) && (!LinhaEmFoco.IsPONEntradaNull()))
            {
                DateTime Entrada = LinhaEmFoco.PONData.AddHours(8);
                DateTime EntradaTol = Entrada.AddMinutes(15);
                if (LinhaEmFoco.PONEntrada > EntradaTol)
                    e.Appearance.BackColor = Color.Red;
                else
                    if (LinhaEmFoco.PONEntrada > Entrada)
                        e.Appearance.BackColor = Color.Yellow;
            }
            if ((e.Column == colPONAlmocoRet) && (!LinhaEmFoco.IsPONAlmocoNull()) && (!LinhaEmFoco.IsPONAlmocoRetNull()))
            {
                TimeSpan Almoco = LinhaEmFoco.PONAlmocoRet - LinhaEmFoco.PONAlmoco;
                
                if (Almoco.TotalMinutes > 90)
                    e.Appearance.BackColor = Color.Red;
                else
                    if (Almoco.TotalMinutes > 75)
                        e.Appearance.BackColor = Color.Yellow;
            }
        }

        class par 
        {
            public par(string _Nome,DateTime _Data,int _USU,int _Registro)
            {
                Nome = _Nome;
                Data = _Data;
                USU = _USU;
                Registro = _Registro;
            }
            public int USU;
            public string Nome;
            public DateTime Data;
            public int Registro;
        }

        private bool Completo;

        private void Completar()
        {
            if (Completo)
                return;
            Completo = true;
            dRelPonto.PLanilhaPontoTableAdapter.Fill(dRelPonto.PLanilhaPonto, 410, cCompet1.Comp.Ano, cCompet1.Comp.Mes);
            DateTime dataEsperada = DataI.DateTime;
            int USUEsperado = 0;
            int RegistroEsperado = 0;
            string NomeEsperado = "";
            System.Collections.ArrayList Faltantes = new System.Collections.ArrayList();
            foreach (dRelPonto.PONtoRow rowPON in dRelPonto.PONto)
            {
                if (rowPON.IsUSURegistroNull())
                    rowPON.USURegistro = 0;
                if (!rowPON.IsPONEntradaNull() && rowPON.IsPONSaidaNull())
                {
                    rowPON.PONSaida = rowPON.PONEntrada.Date.AddHours(17).AddMinutes(30);
                };
                if ((USUEsperado != 0) && (USUEsperado != rowPON.PON_USU))
                {
                    while (DataF.DateTime >= dataEsperada)
                    {
                        Faltantes.Add(new par(NomeEsperado, dataEsperada, USUEsperado,RegistroEsperado));
                        dataEsperada = dataEsperada.AddDays(1);
                    };
                    dataEsperada = DataI.DateTime;
                };
                USUEsperado = rowPON.PON_USU;
                NomeEsperado = rowPON.USUNome;
                RegistroEsperado = rowPON.USURegistro;
                while (rowPON.PONData > dataEsperada) 
                {                    
                    Faltantes.Add(new par(rowPON.USUNome, dataEsperada,rowPON.PON_USU,rowPON.USURegistro));
                    dataEsperada = dataEsperada.AddDays(1);
                };
                dataEsperada = dataEsperada.AddDays(1);
            }
            while (DataF.DateTime >= dataEsperada)
            {
                Faltantes.Add(new par(NomeEsperado, dataEsperada, USUEsperado,RegistroEsperado));
                dataEsperada = dataEsperada.AddDays(1);
            };
            foreach (par p in Faltantes)
            {
                dRelPonto.PONtoRow novarow = dRelPonto.PONto.NewPONtoRow();
                novarow.PONData = p.Data;
                novarow.USUNome = p.Nome;
                novarow.PON_USU = p.USU;
                novarow.USURegistro = p.Registro;
                dRelPonto.PONto.AddPONtoRow(novarow);
            }
        }

        private void cBotaoImpBol1_clicado(object sender, EventArgs e)
        {            
            Calcular();
            Completar();
            using (impressos.ImpPontoNeon Impresso = new DP.impressos.ImpPontoNeon())
            {
                Impresso.dRelpontobindingSource.DataSource = dRelPonto;
                switch (cBotaoImpBol1.BotaoClicado)
                {
                    case dllImpresso.Botoes.Botao.PDF_frente:
                    case dllImpresso.Botoes.Botao.pdf:
                        using (SaveFileDialog saveFileDialog1 = new SaveFileDialog())
                        {
                            saveFileDialog1.DefaultExt = "pdf";
                            if (saveFileDialog1.ShowDialog() == DialogResult.OK)
                            {
                                Impresso.ExportOptions.Pdf.Compressed = true;
                                Impresso.ExportOptions.Pdf.ImageQuality = DevExpress.XtraPrinting.PdfJpegImageQuality.Lowest;
                                Impresso.ExportToPdf(saveFileDialog1.FileName);
                            }
                        }
                        break;
                                           
                    case dllImpresso.Botoes.Botao.email:
                        if (VirEmailNeon.EmailDiretoNeon.EmalST.Enviar("", "Planilhas", Impresso, "Planilhas em anexo.", "Planilha de ponto"))
                            MessageBox.Show("E.mail enviado");
                        else
                        {
                            if (VirEmailNeon.EmailDiretoNeon.EmalST.UltimoErro != null)
                                MessageBox.Show("Falha no envio");
                            else
                                MessageBox.Show("Cancelado");
                        };
                        break;
                    case dllImpresso.Botoes.Botao.botao:
                    case dllImpresso.Botoes.Botao.imprimir:
                    case dllImpresso.Botoes.Botao.imprimir_frente:
                    case dllImpresso.Botoes.Botao.nulo:
                        Impresso.apontar = true;
                        Impresso.Print();
                        break;                                                               
                    case dllImpresso.Botoes.Botao.tela:
                        Impresso.ShowPreviewDialog();
                        break;
                    default:
                        break;
                }

                
            };
            
            /*
            System.Collections.ArrayList USUs = new System.Collections.ArrayList();
            using (Impresso = new DP.impressos.ImpPonto())
            {
                impressos.dPonto.PLanilhaPontoRow rowPLP;
                foreach (dRelPonto.PONtoRow rowPON in dRelPonto.PONto)
                {
                    if (!USUs.Contains(rowPON.PON_USU))
                    {
                        USUs.Add(rowPON.PON_USU);
                        rowPLP = Impresso.dPonto.PLanilhaPonto.NewPLanilhaPontoRow();
                        rowPLP.PLPDATAI = DateTime.Now;
                        rowPLP.PLPI_USU = 1;
                        rowPLP.calCB = "calCB";
                        rowPLP.Cidade = "cidade";
                        rowPLP.PLP_CON = 0;
                        rowPLP.PLPApontamento_USU = 1;
                        rowPLP.PLPCompetenciaAno = 2010;
                        rowPLP.PLPCompetenciaMes = 1;
                        rowPLP.PLPCONFolha = 0;
                        rowPLP.PLPDataApontamento = DateTime.Today;
                        Framework.datasets.dUSUarios.USUariosRow rowUSU = Framework.datasets.dUSUarios.dUSUariosSt.USUarios.FindByUSU(rowPON.PON_USU);
                        rowPLP.PLPEtiqueta = rowUSU.USUNome;
                        rowPLP.PLPHorario = "00:00 - 00:00";
                        rowPLP.PLPNome = "Nome";
                        rowPLP.PLPRegistro = 1;
                        Impresso.dPonto.PLanilhaPonto.AddPLanilhaPontoRow(rowPLP);
                    }
                };
                Framework.objetosNeon.Competencia comp = new Framework.objetosNeon.Competencia(1,2010);
                Impresso.CargaInicial(comp, new System.Collections.ArrayList());
                */
            
            
        }

        private void Apagar(object sender, EventArgs e)
        {
            dRelPonto.PONto.Clear();            
        }

        private void ajustaDatas()
        {
            DataI.DateTime = new DateTime(cCompet1.Comp.Ano, cCompet1.Comp.Mes, 13);
            DataI.DateTime = DataI.DateTime.AddMonths(-1);
            DataF.DateTime = new DateTime(cCompet1.Comp.Ano, cCompet1.Comp.Mes, 12);
        }

        private void cCompet1_OnChange(object sender, EventArgs e)
        {
            ajustaDatas();            
        }

        
    }
}
