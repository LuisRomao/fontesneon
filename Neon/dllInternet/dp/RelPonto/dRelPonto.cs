﻿namespace DP.RelPonto {


    partial class dRelPonto
    {
        private dRelPontoTableAdapters.PONtoTableAdapter pONtoTableAdapter;
        /// <summary>
        /// TableAdapter padrão para tabela: PONto
        /// </summary>
        public dRelPontoTableAdapters.PONtoTableAdapter PONtoTableAdapter
        {
            get
            {
                if (pONtoTableAdapter == null)
                {
                    pONtoTableAdapter = new dRelPontoTableAdapters.PONtoTableAdapter();
                    pONtoTableAdapter.TrocarStringDeConexao();
                };
                return pONtoTableAdapter;
            }
        }

        private dRelPontoTableAdapters.PLanilhaPontoTableAdapter pLanilhaPontoTableAdapter;
        /// <summary>
        /// TableAdapter padrão para tabela: PLanilhaPonto
        /// </summary>
        public dRelPontoTableAdapters.PLanilhaPontoTableAdapter PLanilhaPontoTableAdapter
        {
            get
            {
                if (pLanilhaPontoTableAdapter == null)
                {
                    pLanilhaPontoTableAdapter = new dRelPontoTableAdapters.PLanilhaPontoTableAdapter();
                    pLanilhaPontoTableAdapter.TrocarStringDeConexao();
                };
                return pLanilhaPontoTableAdapter;
            }
        }
    }
}
