using System;
using System.Collections.Generic;
using System.Text;
using Microsoft.VisualBasic;
using System.Data;
using System.Windows.Forms;
using System.IO;

namespace DP.Virtual
{
    static class FuncoesDP
    {
        static public string CampoNumerico(Object Valor, Int32 Tamanho)
        {
            if (Valor is Decimal)
            {
                decimal V = Convert.ToDecimal(Valor);
                Valor = string.Format("{0:0.00}", V);
            }
            Valor = Valor.ToString().Replace(",", "");
            Valor = Valor.ToString().Replace(".", "");
            Valor = Valor.ToString().Replace("-", "");
            Valor = Valor.ToString().Replace("/", "");
            Valor = Valor.ToString().Replace(":", "");

            String strRetorno = "";

            strRetorno = Strings.RSet(Valor.ToString(), Tamanho);

            strRetorno = strRetorno.Replace(" ", "0");

            return strRetorno;
        }

        static public string CampoAlfaNumerico(Object Texto, Int32 Tamanho)
        {
            return Strings.LSet(RemoverAcento(Texto.ToString()), Tamanho);
        }

        static public string RemoverAcento(String Texto)
        {
            const String CAcento = "�����������������������������������������������";
            const String SAcento = "aaaaaeeeeiiiiooooouuuuAAAAAEEEEIIIOOOOOUUUUcCnN";

            String strRetorno = "";

            for (int cIndex = 0; cIndex < Texto.Length; cIndex++)
            {
                int cPos = CAcento.IndexOf(Texto[cIndex]);

                if (cPos >= 0)
                    strRetorno += SAcento[cPos];
                else
                    strRetorno += Texto[cIndex];
            }

            return strRetorno;
        }

        static public bool StringIsDateTime(String DataHora)
        {
            DataHora = DataHora.Replace(" ", ""); 

            try
            {
                Convert.ToDateTime(DataHora);
                return true;
            }
            catch
            {
                return false;
            }
        }
    }
}
