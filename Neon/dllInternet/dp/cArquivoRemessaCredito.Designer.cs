namespace DP
{
    partial class cArquivoRemessaCredito
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(cArquivoRemessaCredito));
            this.groupControl1 = new DevExpress.XtraEditors.GroupControl();
            this.FileTreeList = new DevExpress.XtraTreeList.TreeList();
            this.colNome = new DevExpress.XtraTreeList.Columns.TreeListColumn();
            this.colTamanho = new DevExpress.XtraTreeList.Columns.TreeListColumn();
            this.colDataCriacao = new DevExpress.XtraTreeList.Columns.TreeListColumn();
            this.ImageList = new System.Windows.Forms.ImageList(this.components);
            this.xtraTabControl1 = new DevExpress.XtraTab.XtraTabControl();
            this.xtraTabPage1 = new DevExpress.XtraTab.XtraTabPage();
            this.RichTextFile = new System.Windows.Forms.RichTextBox();
            ((System.ComponentModel.ISupportInitialize)(this.BindingSource_F)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.StyleController_F)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl1)).BeginInit();
            this.groupControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.FileTreeList)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xtraTabControl1)).BeginInit();
            this.xtraTabControl1.SuspendLayout();
            this.xtraTabPage1.SuspendLayout();
            this.SuspendLayout();
            // 
            // ToolTipController_F
            // 
            this.ToolTipController_F.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.ToolTipController_F.Appearance.ForeColor = System.Drawing.Color.Black;
            this.ToolTipController_F.Appearance.Options.UseBackColor = true;
            this.ToolTipController_F.Appearance.Options.UseFont = true;
            this.ToolTipController_F.Appearance.Options.UseForeColor = true;
            this.ToolTipController_F.Appearance.Options.UseTextOptions = true;
            this.ToolTipController_F.Appearance.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            // 
            // StyleController_F
            // 
            this.StyleController_F.AppearanceFocused.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.StyleController_F.AppearanceFocused.Options.UseBackColor = true;
            // 
            // DefaultLookAndFeel_F
            // 
            this.DefaultLookAndFeel_F.LookAndFeel.SkinName = "The Asphalt World";
            // 
            // groupControl1
            // 
            this.groupControl1.Controls.Add(this.FileTreeList);
            this.groupControl1.Dock = System.Windows.Forms.DockStyle.Top;
            this.groupControl1.Location = new System.Drawing.Point(0, 0);
            this.groupControl1.Name = "groupControl1";
            this.groupControl1.Size = new System.Drawing.Size(600, 131);
            this.groupControl1.TabIndex = 0;
            this.groupControl1.Text = ":: Selecione o arquivo gerado pelo programa da folha ::";
            // 
            // FileTreeList
            // 
            this.FileTreeList.Appearance.Empty.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(236)))), ((int)(((byte)(246)))), ((int)(((byte)(255)))));
            this.FileTreeList.Appearance.Empty.BackColor2 = System.Drawing.Color.White;
            this.FileTreeList.Appearance.Empty.Options.UseBackColor = true;
            this.FileTreeList.Appearance.EvenRow.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(247)))), ((int)(((byte)(251)))), ((int)(((byte)(255)))));
            this.FileTreeList.Appearance.EvenRow.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(247)))), ((int)(((byte)(251)))), ((int)(((byte)(255)))));
            this.FileTreeList.Appearance.EvenRow.ForeColor = System.Drawing.Color.Black;
            this.FileTreeList.Appearance.EvenRow.Options.UseBackColor = true;
            this.FileTreeList.Appearance.EvenRow.Options.UseBorderColor = true;
            this.FileTreeList.Appearance.EvenRow.Options.UseForeColor = true;
            this.FileTreeList.Appearance.FocusedCell.BackColor = System.Drawing.Color.White;
            this.FileTreeList.Appearance.FocusedCell.ForeColor = System.Drawing.Color.Black;
            this.FileTreeList.Appearance.FocusedCell.Options.UseBackColor = true;
            this.FileTreeList.Appearance.FocusedCell.Options.UseForeColor = true;
            this.FileTreeList.Appearance.FocusedRow.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(38)))), ((int)(((byte)(109)))), ((int)(((byte)(189)))));
            this.FileTreeList.Appearance.FocusedRow.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(59)))), ((int)(((byte)(139)))), ((int)(((byte)(206)))));
            this.FileTreeList.Appearance.FocusedRow.ForeColor = System.Drawing.Color.White;
            this.FileTreeList.Appearance.FocusedRow.Options.UseBackColor = true;
            this.FileTreeList.Appearance.FocusedRow.Options.UseBorderColor = true;
            this.FileTreeList.Appearance.FocusedRow.Options.UseForeColor = true;
            this.FileTreeList.Appearance.FooterPanel.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(104)))), ((int)(((byte)(184)))), ((int)(((byte)(251)))));
            this.FileTreeList.Appearance.FooterPanel.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(104)))), ((int)(((byte)(184)))), ((int)(((byte)(251)))));
            this.FileTreeList.Appearance.FooterPanel.ForeColor = System.Drawing.Color.Black;
            this.FileTreeList.Appearance.FooterPanel.Options.UseBackColor = true;
            this.FileTreeList.Appearance.FooterPanel.Options.UseBorderColor = true;
            this.FileTreeList.Appearance.FooterPanel.Options.UseForeColor = true;
            this.FileTreeList.Appearance.GroupButton.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(104)))), ((int)(((byte)(184)))), ((int)(((byte)(251)))));
            this.FileTreeList.Appearance.GroupButton.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(104)))), ((int)(((byte)(184)))), ((int)(((byte)(251)))));
            this.FileTreeList.Appearance.GroupButton.Options.UseBackColor = true;
            this.FileTreeList.Appearance.GroupButton.Options.UseBorderColor = true;
            this.FileTreeList.Appearance.GroupFooter.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(170)))), ((int)(((byte)(216)))), ((int)(((byte)(254)))));
            this.FileTreeList.Appearance.GroupFooter.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(170)))), ((int)(((byte)(216)))), ((int)(((byte)(254)))));
            this.FileTreeList.Appearance.GroupFooter.ForeColor = System.Drawing.Color.Black;
            this.FileTreeList.Appearance.GroupFooter.Options.UseBackColor = true;
            this.FileTreeList.Appearance.GroupFooter.Options.UseBorderColor = true;
            this.FileTreeList.Appearance.GroupFooter.Options.UseForeColor = true;
            this.FileTreeList.Appearance.HeaderPanel.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(139)))), ((int)(((byte)(201)))), ((int)(((byte)(254)))));
            this.FileTreeList.Appearance.HeaderPanel.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(139)))), ((int)(((byte)(201)))), ((int)(((byte)(254)))));
            this.FileTreeList.Appearance.HeaderPanel.ForeColor = System.Drawing.Color.Black;
            this.FileTreeList.Appearance.HeaderPanel.Options.UseBackColor = true;
            this.FileTreeList.Appearance.HeaderPanel.Options.UseBorderColor = true;
            this.FileTreeList.Appearance.HeaderPanel.Options.UseForeColor = true;
            this.FileTreeList.Appearance.HideSelectionRow.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(105)))), ((int)(((byte)(170)))), ((int)(((byte)(225)))));
            this.FileTreeList.Appearance.HideSelectionRow.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(83)))), ((int)(((byte)(155)))), ((int)(((byte)(215)))));
            this.FileTreeList.Appearance.HideSelectionRow.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(236)))), ((int)(((byte)(246)))), ((int)(((byte)(255)))));
            this.FileTreeList.Appearance.HideSelectionRow.Options.UseBackColor = true;
            this.FileTreeList.Appearance.HideSelectionRow.Options.UseBorderColor = true;
            this.FileTreeList.Appearance.HideSelectionRow.Options.UseForeColor = true;
            this.FileTreeList.Appearance.HorzLine.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(104)))), ((int)(((byte)(184)))), ((int)(((byte)(251)))));
            this.FileTreeList.Appearance.HorzLine.Options.UseBackColor = true;
            this.FileTreeList.Appearance.OddRow.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(236)))), ((int)(((byte)(246)))), ((int)(((byte)(255)))));
            this.FileTreeList.Appearance.OddRow.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(236)))), ((int)(((byte)(246)))), ((int)(((byte)(255)))));
            this.FileTreeList.Appearance.OddRow.ForeColor = System.Drawing.Color.Black;
            this.FileTreeList.Appearance.OddRow.Options.UseBackColor = true;
            this.FileTreeList.Appearance.OddRow.Options.UseBorderColor = true;
            this.FileTreeList.Appearance.OddRow.Options.UseForeColor = true;
            this.FileTreeList.Appearance.Preview.Font = new System.Drawing.Font("Verdana", 7.5F);
            this.FileTreeList.Appearance.Preview.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(83)))), ((int)(((byte)(155)))), ((int)(((byte)(215)))));
            this.FileTreeList.Appearance.Preview.Options.UseFont = true;
            this.FileTreeList.Appearance.Preview.Options.UseForeColor = true;
            this.FileTreeList.Appearance.Row.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(247)))), ((int)(((byte)(251)))), ((int)(((byte)(255)))));
            this.FileTreeList.Appearance.Row.ForeColor = System.Drawing.Color.Black;
            this.FileTreeList.Appearance.Row.Options.UseBackColor = true;
            this.FileTreeList.Appearance.Row.Options.UseForeColor = true;
            this.FileTreeList.Appearance.SelectedRow.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(83)))), ((int)(((byte)(155)))), ((int)(((byte)(215)))));
            this.FileTreeList.Appearance.SelectedRow.ForeColor = System.Drawing.Color.White;
            this.FileTreeList.Appearance.SelectedRow.Options.UseBackColor = true;
            this.FileTreeList.Appearance.SelectedRow.Options.UseForeColor = true;
            this.FileTreeList.Appearance.TreeLine.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(59)))), ((int)(((byte)(133)))), ((int)(((byte)(195)))));
            this.FileTreeList.Appearance.TreeLine.Options.UseBackColor = true;
            this.FileTreeList.Appearance.VertLine.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(104)))), ((int)(((byte)(184)))), ((int)(((byte)(251)))));
            this.FileTreeList.Appearance.VertLine.Options.UseBackColor = true;
            this.FileTreeList.Columns.AddRange(new DevExpress.XtraTreeList.Columns.TreeListColumn[] {
            this.colNome,
            this.colTamanho,
            this.colDataCriacao});
            this.FileTreeList.Dock = System.Windows.Forms.DockStyle.Fill;
            this.FileTreeList.Location = new System.Drawing.Point(2, 20);
            this.FileTreeList.LookAndFeel.SkinName = "The Asphalt World";
            this.FileTreeList.LookAndFeel.UseDefaultLookAndFeel = false;
            this.FileTreeList.Name = "FileTreeList";
            this.FileTreeList.OptionsBehavior.Editable = false;
            this.FileTreeList.OptionsBehavior.ImmediateEditor = false;
            this.FileTreeList.OptionsMenu.EnableFooterMenu = false;
            this.FileTreeList.OptionsView.EnableAppearanceEvenRow = true;
            this.FileTreeList.OptionsView.EnableAppearanceOddRow = true;
            this.FileTreeList.OptionsView.ShowHorzLines = false;
            this.FileTreeList.OptionsView.ShowRoot = false;
            this.FileTreeList.SelectImageList = this.ImageList;
            this.FileTreeList.Size = new System.Drawing.Size(596, 109);
            this.FileTreeList.TabIndex = 0;
            this.FileTreeList.GetSelectImage += new DevExpress.XtraTreeList.GetSelectImageEventHandler(this.treeList1_GetSelectImage);
            this.FileTreeList.FocusedNodeChanged += new DevExpress.XtraTreeList.FocusedNodeChangedEventHandler(this.FileTreeList_FocusedNodeChanged);
            // 
            // colNome
            // 
            this.colNome.Caption = "Nome";
            this.colNome.FieldName = "Nome";
            this.colNome.MinWidth = 27;
            this.colNome.Name = "colNome";
            this.colNome.VisibleIndex = 0;
            this.colNome.Width = 359;
            // 
            // colTamanho
            // 
            this.colTamanho.Caption = "Tamanho";
            this.colTamanho.FieldName = "Tamanho";
            this.colTamanho.Name = "colTamanho";
            this.colTamanho.VisibleIndex = 1;
            this.colTamanho.Width = 67;
            // 
            // colDataCriacao
            // 
            this.colDataCriacao.Caption = "Data da Cria��o";
            this.colDataCriacao.FieldName = "Data Criacao";
            this.colDataCriacao.Name = "colDataCriacao";
            this.colDataCriacao.VisibleIndex = 2;
            this.colDataCriacao.Width = 149;
            // 
            // ImageList
            // 
            this.ImageList.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("ImageList.ImageStream")));
            this.ImageList.TransparentColor = System.Drawing.Color.Transparent;
            this.ImageList.Images.SetKeyName(0, "rtf4.jpg");
            // 
            // xtraTabControl1
            // 
            this.xtraTabControl1.Dock = System.Windows.Forms.DockStyle.Top;
            this.xtraTabControl1.Location = new System.Drawing.Point(0, 131);
            this.xtraTabControl1.Name = "xtraTabControl1";
            this.xtraTabControl1.SelectedTabPage = this.xtraTabPage1;
            this.xtraTabControl1.Size = new System.Drawing.Size(600, 334);
            this.xtraTabControl1.TabIndex = 1;
            this.xtraTabControl1.TabPages.AddRange(new DevExpress.XtraTab.XtraTabPage[] {
            this.xtraTabPage1});
            this.xtraTabControl1.Text = "xtraTabControl1";
            // 
            // xtraTabPage1
            // 
            this.xtraTabPage1.Controls.Add(this.RichTextFile);
            this.xtraTabPage1.Name = "xtraTabPage1";
            this.xtraTabPage1.Size = new System.Drawing.Size(591, 304);
            this.xtraTabPage1.Text = "Arquivo RTF";
            // 
            // RichTextFile
            // 
            this.RichTextFile.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.RichTextFile.Dock = System.Windows.Forms.DockStyle.Fill;
            this.RichTextFile.Location = new System.Drawing.Point(0, 0);
            this.RichTextFile.Name = "RichTextFile";
            this.RichTextFile.ReadOnly = true;
            this.RichTextFile.ScrollBars = System.Windows.Forms.RichTextBoxScrollBars.ForcedBoth;
            this.RichTextFile.Size = new System.Drawing.Size(591, 304);
            this.RichTextFile.TabIndex = 0;
            this.RichTextFile.Text = "";
            // 
            // cArquivoRemessaCredito
            // 
            this.Controls.Add(this.xtraTabControl1);
            this.Controls.Add(this.groupControl1);
            this.Cursor = System.Windows.Forms.Cursors.Default;
            this.LookAndFeel.UseDefaultLookAndFeel = false;
            this.Margin = new System.Windows.Forms.Padding(3, 0, 3, 3);
            this.Name = "cArquivoRemessaCredito";
            this.Size = new System.Drawing.Size(600, 500);
            this.Load += new System.EventHandler(this.cArquivoRemessaCredito_Load);
            ((System.ComponentModel.ISupportInitialize)(this.BindingSource_F)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.StyleController_F)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl1)).EndInit();
            this.groupControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.FileTreeList)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xtraTabControl1)).EndInit();
            this.xtraTabControl1.ResumeLayout(false);
            this.xtraTabPage1.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraEditors.GroupControl groupControl1;
        private DevExpress.XtraTreeList.TreeList FileTreeList;
        private DevExpress.XtraTreeList.Columns.TreeListColumn colNome;
        private DevExpress.XtraTreeList.Columns.TreeListColumn colTamanho;
        private DevExpress.XtraTreeList.Columns.TreeListColumn colDataCriacao;
        private System.Windows.Forms.ImageList ImageList;
        private DevExpress.XtraTab.XtraTabControl xtraTabControl1;
        private DevExpress.XtraTab.XtraTabPage xtraTabPage1;
        private System.Windows.Forms.RichTextBox RichTextFile;
    }
}
