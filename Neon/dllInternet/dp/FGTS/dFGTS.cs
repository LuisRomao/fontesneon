﻿/*
MR - 26/12/2014 10:00           - Inclusão dos campos adicionais para Tributo Eletrônico (Condominio, CNPJ, Agencia, Conta, DigAgencia, DigConta, Banco, Endereco, Bairro, Cidade, UF, CEP, CodigoBarra, TipoPag, CodComunicacao) no FGTS
                                  Inclusão de enumeração com tipos de pagamentos disponiveis para FGTS
*/

namespace DP.FGTS {

    using dllVirEnum;
    
    public partial class dFGTS 
    {
        /// <summary>
        /// 
        /// </summary>
        public enum nTipoPag
        {
            /// <summary>
            /// 
            /// </summary>
            Invalido = 0,
            /// <summary>
            /// 
            /// </summary>
            Cheque = 1,
            /// <summary>
            /// 
            /// </summary>
            EletronicoBradesco = 8,
            /// <summary>
            /// 
            /// </summary>
            ChequeFilial = 10,
            /// <summary>
            /// 
            /// </summary>
            NaoEmitir = 11
        }

        private VirEnum virEnumnTipoPag;

        /// <summary>
        /// 
        /// </summary>
        public VirEnum VirEnumnTipoPag
        {
            get
            {
                if (virEnumnTipoPag == null)
                {
                    virEnumnTipoPag = new VirEnum(typeof(nTipoPag));
                    virEnumnTipoPag.CarregarDefault = false;
                    virEnumnTipoPag.GravaNomes(nTipoPag.Cheque, "Cheque");
                    virEnumnTipoPag.GravaNomes(nTipoPag.Invalido, "Inválido");
                    virEnumnTipoPag.GravaNomes(nTipoPag.EletronicoBradesco, "Elet. Bradesco");
                    virEnumnTipoPag.GravaNomes(nTipoPag.ChequeFilial, "Cheque Filial");
                    virEnumnTipoPag.GravaNomes(nTipoPag.NaoEmitir, "Não emitir");
                }
                return virEnumnTipoPag;
            }
        }
    }
}
