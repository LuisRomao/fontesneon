﻿/*
MR - 26/12/2014 10:00 -           - Implementado o pagamento de Tributo Eletrônico para o FGTS com inclusão de periódico (Alterações indicadas por *** MRC - INICIO - TRIBUTOS (26/12/2014 10:00) ***)
                                    Dados de FGTS carregados de um novo layout de arquivo
                                    Permite selecionar o tipo de pagamento (não apenas cheque)
MR - 29/01/2015 12:00 -           - Faz consistências necessárias para Tributo Eletrônico Brandesco novo antes de setar envio (Alterações indicadas por *** MRC - INICIO - TRIBUTOS (29/01/2015 12:00) ***)
*/

using System;
using System.Drawing;
using System.Windows.Forms;
using ContaPagar.Follow;
using Framework;
using FrameworkProc;
using dllImpostoNeonProc;
using DP.Automacao_Bancaria;
using VirEnumeracoes;
using VirEnumeracoesNeon;


namespace DP.FGTS
{
    /// <summary>
    /// 
    /// </summary>
    public partial class cFGTS : CompontesBasicos.ComponenteBase
    {
        /// <summary>
        /// 
        /// </summary>
        public cFGTS()
        {
            InitializeComponent();
            cONDOMINIOSBindingSource.DataSource = FrameworkProc.datasets.dCondominiosAtivos.dCondominiosTodosST.CONDOMINIOS;
            //*** MRC - INICIO - TRIBUTOS (26/12/2014 10:00) ***
            if (!DesignMode)
                dFGTS.VirEnumnTipoPag.CarregaEditorDaGrid(colTipoPag);
            //*** MRC - TERMINO - TRIBUTOS (26/12/2014 10:00) ***
        }

        private void simpleButton1_Click(object sender, EventArgs e)
        {
            this.dFGTS.FGTS.Clear();
            dllSefip.fgts fgts1 = new dllSefip.fgts();
            fgts1.Carrega();
            if (fgts1.Resultado == null)
                return;
            dAutomacaoBancaria.dAutomacaoBancariaSt.CarregaCondominos(true);
            foreach (AbstratosNeon.ABS_DadosPer.UnidadeResposta Un in fgts1.Resultado.Values)
            {
                dFGTS.FGTSRow novarow = dFGTS.FGTS.NewFGTSRow();
                dAutomacaoBancaria.CONDOMINIOSRow rowCON = dAutomacaoBancaria.dAutomacaoBancariaSt.BuscaCondominio(Un.Codigo, "XXX", true);
                if (rowCON != null)
                    novarow.EMP = rowCON.CON_EMP;                
                novarow.CON = rowCON.CON;
                novarow.CONCodigoFolha1 = rowCON.CONCodigoFolha1;
                novarow.CONCodigo = rowCON.CONCodigo;                
                novarow.Condominio = rowCON.CONNome;
                novarow.CodComunicacao = "117045";
                DocBacarios.CPFCNPJ cnpjLido = new DocBacarios.CPFCNPJ(Un.Inscricao);
                if (cnpjLido.Tipo == DocBacarios.TipoCpfCnpj.INVALIDO)
                    novarow.Erro = "CNPJ inválido:" + Un.Inscricao;
                else if (rowCON.IsCONCnpjNull())
                    novarow.Erro = "CNPJ não cadastrado!";
                else if (cnpjLido.Valor != new DocBacarios.CPFCNPJ(rowCON.CONCnpj).Valor)
                    novarow.Erro = "CNPJ não confere:" + Un.Inscricao + " x " + rowCON.CONCnpj;
                else
                    novarow.CNPJ = new DocBacarios.CPFCNPJ(rowCON.CONCnpj).Valor;
                try
                {
                    novarow.Agencia = int.Parse(rowCON.CONAgencia);
                    novarow.Conta = int.Parse(rowCON.CONConta);
                    novarow.DigAgencia = rowCON.CONDigitoAgencia;
                    novarow.DigConta = rowCON.CONDigitoConta;
                }
                catch
                {
                    novarow.Erro = "Erro na Agência/Conta do condomínio";
                    novarow.Agencia = novarow.Conta = 0;
                    novarow.DigAgencia = novarow.DigConta = "";
                }
                if (rowCON.IsCON_BCONull())
                {
                    novarow.Erro = "Erro no Banco do condomínio";
                    novarow.Banco = 0;
                }
                else
                {
                    novarow.Banco = rowCON.CON_BCO;
                    if ((!rowCON.IsCONPagEletronicoNull()) && (rowCON.CONPagEletronico)
                      && (rowCON.CON_BCO == 237) && (rowCON.CONGPSEletronico))
                        novarow.TipoPag = (int)dFGTS.nTipoPag.EletronicoBradesco;
                    else
                        novarow.TipoPag = (int)dFGTS.nTipoPag.Cheque;
                }
                novarow.Endereco = rowCON.CONEndereco;
                novarow.Bairro = rowCON.CONBairro;
                novarow.Cidade = rowCON.CIDNome;
                novarow.UF = rowCON.CIDUf;
                novarow.CEP = rowCON.CONCep;
                if (Un.CodigoBarras == null || Un.CodigoBarras == "")
                    novarow.Erro = "Código de Barra não encontrado";
                else if (Un.CodigoBarras.Length < 48)
                    novarow.Erro = "Código de Barra incompleto";
                else
                    novarow.CodigoBarra = Un.CodigoBarras;                
                if (novarow.IsCONNull())
                    novarow.Erro = "Nenhum Condomínio encontrado com o Código " + Un.Codigo.ToString();

                //Verifica se deu erro e seta como invalido para correcao
                if (!novarow.IsErroNull())
                    novarow.TipoPag = (int)dFGTS.nTipoPag.Invalido;
                novarow.Competencia = Un.Competencia.CompetenciaBind;
                novarow.Valor = Un.Valor;
                novarow.Vencimento = Un.Vencimento;
                dFGTS.FGTS.AddFGTSRow(novarow);
            }
        }

        private void gridView1_CustomColumnDisplayText(object sender, DevExpress.XtraGrid.Views.Base.CustomColumnDisplayTextEventArgs e)
        {
            if(e.Column == colCompetencia)
            {
                try
                {
                    Framework.objetosNeon.Competencia comp = new Framework.objetosNeon.Competencia((int)e.Value);
                    e.DisplayText = comp.ToString();
                }
                catch { }
            }
        }

        private void simpleButton2_Click(object sender, EventArgs e)
        {            
            bool avisar = false;
            int Errados = 0;
            int Eletronicos = 0;
            int Cheques = 0;

            if (dFGTS.FGTS.Count == 0)
                return;

            foreach (dFGTS.FGTSRow row in dFGTS.FGTS)
                if (!row.IsErroNull() || (dFGTS.nTipoPag)row.TipoPag == dFGTS.nTipoPag.Invalido)
                    Errados++;
            if (Errados > 0)
                if (MessageBox.Show("Existem " + Errados.ToString() + " pagamentos que serão ignorados.\r\nConfirma assim mesmo?", "AVISO", MessageBoxButtons.YesNo, MessageBoxIcon.Warning, MessageBoxDefaultButton.Button2) == DialogResult.No)
                    return;

            if (VirInput.Input.Senha("Senha FGTS") != "CUIDADO-FGTS")
            {
                MessageBox.Show("Senha incorreta");
                return;
            };

            DateTime Agendamento = DateTime.Today.AddDays(1);
            while ((Agendamento.DayOfWeek == DayOfWeek.Saturday) || (Agendamento.DayOfWeek == DayOfWeek.Sunday))
                Agendamento = Agendamento.AddDays(1);
            DateTime? DataAgendarSel = Agendamento;
            if (VirInput.Input.Execute("Data de Pagamento", ref DataAgendarSel, DateTime.Today, DateTime.Today.AddDays(30)))
            {
                if(!DataAgendarSel.Value.IsUtil())                
                {
                    MessageBox.Show("Data de Pagamento inválida (não é dia útil)");
                    return;
                }
            }
            else
                return;
            Agendamento = Convert.ToDateTime(DataAgendarSel);
            
            using (CompontesBasicos.Espera.cEspera Esp = new CompontesBasicos.Espera.cEspera(this))
            {
                Esp.Espere("Gerando cheques e eletrônicos");
                Esp.AtivaGauge(dFGTS.FGTS.Count);
                Application.DoEvents();
                int i = 0;
                DateTime prox = DateTime.Now.AddSeconds(3);

                
                dGPS.dGPSSt.GPS.Clear();
                try
                {
                    VirMSSQL.TableAdapter.AbreTrasacaoSQLDupla("DP cFGTS - 190");
                    //VirMSSQL.TableAdapter.AbreTrasacaoSQL("DP cFGTS - 190", dGPS.dGPSSt.GPSTableAdapter, dPagamentosPeriodicos.dPagamentosPeriodicosSt.NOtAsTableAdapter, dPagamentosPeriodicos.dPagamentosPeriodicosSt.PAGamentosTableAdapter);
                    dGPS.dGPSSt.GPSTableAdapter.EmbarcaEmTransST();
                    dGPS.dGPSSt.GPSTableAdapterF.EmbarcaEmTransST();
                    foreach (dFGTS.FGTSRow novarow in dFGTS.FGTS)
                    {
                        i++;
                        if (DateTime.Now > prox)
                        {
                            prox = DateTime.Now.AddSeconds(3);
                            Esp.Gauge(i);
                        }
                        Framework.objetosNeon.Competencia comp = new Framework.objetosNeon.Competencia(novarow.Competencia);
                        switch ((dFGTS.nTipoPag)novarow.TipoPag)
                        {
                            case dFGTS.nTipoPag.EletronicoBradesco:
                                //Insere Tributo na tabela GPS
                                dGPS.GPSRow novalinha = dGPS.dGPSSt.GPS.NewGPSRow();
                                novalinha.EMP = novarow.EMP;
                                DocBacarios.CPFCNPJ Cnpj = new DocBacarios.CPFCNPJ(novarow.CNPJ);
                                novalinha.GPSTipo = (int)TipoImposto.FGTS;
                                novalinha.GPSNomeClientePagador = novarow.Condominio;
                                novalinha.GPS_CON = novarow.CON;
                                //novalinha.GPSMes = Convert.ToInt32(novarow.Competencia.ToString().Substring(4,2));
                                //novalinha.GPSAno = Convert.ToInt32(novarow.Competencia.ToString().Substring(0, 4));
                                novalinha.GPSMes = comp.Mes;
                                novalinha.GPSAno = comp.Ano;
                                novalinha.GPSVencimento = novarow.Vencimento;
                                novalinha.GPSEndereco = (novarow.Endereco.Length > 40) ? novarow.Endereco.Substring(0, 40) : novarow.Endereco;
                                novalinha.GPSCEP = Convert.ToInt32(novarow.CEP.Replace("-",""));
                                novalinha.GPSUF = novarow.UF;
                                novalinha.GPSCidade = (novarow.Cidade.Length > 20) ? novarow.Cidade.Substring(0, 20) : novarow.Cidade;
                                novalinha.GPSBairro = (novarow.Bairro.Length > 20) ? novarow.Bairro.Substring(0, 20) : novarow.Bairro;
                                novalinha.GPSCNPJ = Cnpj.Valor;
                                novalinha.GPSDataPagto = Agendamento;
                                novalinha.GPSValorJuros = 0;
                                novalinha.GPSValorMulta = 0;
                                novalinha.GPSOutras = 0;
                                novalinha.GPSValorPrincipal = novarow.Valor;
                                novalinha.GPSValorTotal = novarow.Valor;
                                novalinha.GPSCodigoReceita = Convert.ToInt32(novarow.CodigoBarra.Substring(16,4));
                                novalinha.GPSCodigoBarra = novarow.CodigoBarra;
                                novalinha.GPSIdentificador = Cnpj.Valor;
                                novalinha.GPSNomeIdentificador = novarow.Condominio;
                                
                                //Framework.datasets.dCondominiosAtivos.CONDOMINIOSRow rowCON = Framework.datasets.dCondominiosAtivos.dCondominiosTodosST.CONDOMINIOS.FindByCON(novarow.CON);                                
                                if (novarow.Banco != 237)
                                    throw new Exception(string.Format("Conta principal do Condomínio não é Bradesco, portanto não pode fazer Pag. Eletr. Bradesco: {0} - {1}", novarow.CONCodigo, novalinha.CONCodigoFolha1));                                
                                novalinha.GPSAgencia = novarow.Agencia;
                                novalinha.GPSDigAgencia = novarow.DigAgencia;
                                novalinha.GPSConta = novarow.Conta;
                                novalinha.GPSDigConta = novarow.DigConta;
                                novalinha.GPSCodComunicacao = novarow.CodComunicacao;                                
                                if (novarow.Valor > 15000)
                                {
                                    novalinha.GPSStatus = (int)StatusArquivo.Liberar;
                                    avisar = true;
                                }
                                else
                                    novalinha.GPSStatus = (int)StatusArquivo.ParaEnviar;
                                novalinha.GPSOrigem = (int)OrigemImposto.Folha;
                                novalinha.GPSI_USU = DSCentral.USUX(novarow.EMP == DSCentral.EMP ? FrameworkProc.EMPTipo.Local : FrameworkProc.EMPTipo.Filial);
                                novalinha.GPSDATAI = DateTime.Now;
                                dGPS.dGPSSt.GPS.AddGPSRow(novalinha);
                                dGPS.dGPSSt.GPSTableAdapterX(novalinha.EMP).Update(novalinha);
                                dGPS.dGPSSt.GPS.AcceptChanges();
                                Eletronicos++;
                                break;
                            case dFGTS.nTipoPag.Cheque:                        
                                //Insere periodico como guia (cheque)
                                FrameworkProc.EMPTProc EMPTProc1 = new FrameworkProc.EMPTProc(novarow.EMP == DSCentral.EMP ? FrameworkProc.EMPTipo.Local : FrameworkProc.EMPTipo.Filial,
                                                                                              novarow.EMP == DSCentral.EMP ? Framework.DSCentral.USU:Framework.DSCentral.USUFilial );
                                PagamentoPeriodico PagamentoPeriodico = new PagamentoPeriodico(novarow.CON, TipoPagPeriodico.FolhaFGTS, EMPTProc1);
                                if (!PagamentoPeriodico.Encontrado)
                                    PagamentoPeriodico.CadastraPGF(novarow.CON,
                                                                   TipoPagPeriodico.FolhaFGTS,
                                                                   novarow.Vencimento,
                                                                   novarow.Valor, new Framework.objetosNeon.Competencia());
                                PagamentoPeriodico.CadastraProximoPagamento(novarow.Vencimento, novarow.Vencimento, novarow.Valor, comp);                        
                                Cheques++;
                                break;
                        };                        
                    };

                
                    dFGTS.FGTS.Clear();
                    VirMSSQL.TableAdapter.CommitSQLDuplo();                    
                    string men = "";
                    if (Eletronicos > 0)
                        men += "Eletrônicos: " + Eletronicos.ToString() + "\r\n";
                    if (Cheques > 0)
                        men += "Cheques: " + Cheques.ToString() + "\r\n";
                    MessageBox.Show(men);
                    if (avisar)
                        MessageBox.Show("ATENÇÃO\r\n\r\nExistem guias retidas aguardando liberação!!!");
                }
                catch (Exception ex)
                {
                    VirMSSQL.TableAdapter.ST().Vircatch(ex);                    
                    string errostr = string.Format("Erro no condomínio: \r\n\r\n{0}", ex.Message);
                    MessageBox.Show(errostr);                    
                }                
            }
        }

        //*** MRC - INICIO - TRIBUTOS (26/12/2014 10:00) ***
        private void gridView1_RowCellStyle(object sender, DevExpress.XtraGrid.Views.Grid.RowCellStyleEventArgs e)
        {
            DevExpress.XtraGrid.Views.Grid.GridView xGrid = (DevExpress.XtraGrid.Views.Grid.GridView)sender;
            object val = null;
            try
            {
                if (e.RowHandle < 0)
                    return;
                dFGTS.FGTSRow FGTSRow = (dFGTS.FGTSRow)xGrid.GetDataRow(e.RowHandle);
                if (FGTSRow == null)
                    return;
                if (e.Column == colTipoPag)
                {                    
                    val = xGrid.GetRowCellValue(e.RowHandle, e.Column);
                    if (val != null)
                    {
                        switch ((dFGTS.nTipoPag)val)
                        {
                            case dFGTS.nTipoPag.Invalido:
                                e.Appearance.BackColor = Color.Red;
                                e.Appearance.BackColor2 = Color.Red;
                                break;
                            case dFGTS.nTipoPag.Cheque:
                                e.Appearance.BackColor = Color.Aqua;
                                e.Appearance.BackColor2 = Color.Aqua;
                                break;
                            case dFGTS.nTipoPag.ChequeFilial:
                                e.Appearance.BackColor = Color.Salmon;
                                e.Appearance.BackColor2 = Color.Salmon;
                                break;
                            case dFGTS.nTipoPag.EletronicoBradesco:
                                e.Appearance.BackColor = Color.Yellow;
                                e.Appearance.BackColor2 = Color.Yellow;
                                break;
                            case dFGTS.nTipoPag.NaoEmitir:
                                e.Appearance.BackColor = Color.Silver;
                                e.Appearance.BackColor2 = Color.Silver;
                                break;

                            default:
                                break;
                        }
                        e.Appearance.ForeColor = Color.Black;
                    };
                }
                else if (e.Column == colEMP)
                {
                    e.Appearance.BackColor2 = e.Appearance.BackColor = FGTSRow.EMP == Framework.DSCentral.EMP ? System.Drawing.Color.PaleGreen : System.Drawing.Color.LemonChiffon;
                    e.Appearance.ForeColor = Color.Black;
                }
            }
            catch (Exception Ex)
            {
                if (xGrid != null)
                {
                    if (val == null)
                        MessageBox.Show(Ex.Message + "\r\n" + e.RowHandle.ToString() + "\r\n Sem valor");
                    else
                        MessageBox.Show(Ex.Message + "\r\n" + e.RowHandle.ToString() + "\r\n" + val.ToString());
                }
            }
        }
        //*** MRC - TERMINO - TRIBUTOS (26/12/2014 10:00) ***
    }
}
