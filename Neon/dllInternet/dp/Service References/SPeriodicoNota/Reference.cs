﻿#pragma warning disable 1591
//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//     Runtime Version:4.0.30319.42000
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace DP.SPeriodicoNota {
    
    
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.ServiceModel", "4.0.0.0")]
    [System.ServiceModel.ServiceContractAttribute(ConfigurationName="SPeriodicoNota.IPeriodicoNota")]
    public interface IPeriodicoNota {
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/IPeriodicoNota/Teste", ReplyAction="http://tempuri.org/IPeriodicoNota/TesteResponse")]
        string Teste(int value);
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/IPeriodicoNota/periodico_CadastrarProximo", ReplyAction="http://tempuri.org/IPeriodicoNota/periodico_CadastrarProximoResponse")]
        string periodico_CadastrarProximo(byte[] Chave, int PGF);
    }
    
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.ServiceModel", "4.0.0.0")]
    public interface IPeriodicoNotaChannel : DP.SPeriodicoNota.IPeriodicoNota, System.ServiceModel.IClientChannel {
    }
    
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.ServiceModel", "4.0.0.0")]
    public partial class PeriodicoNotaClient : System.ServiceModel.ClientBase<DP.SPeriodicoNota.IPeriodicoNota>, DP.SPeriodicoNota.IPeriodicoNota {
        
        public PeriodicoNotaClient() {
        }
        
        public PeriodicoNotaClient(string endpointConfigurationName) : 
                base(endpointConfigurationName) {
        }
        
        public PeriodicoNotaClient(string endpointConfigurationName, string remoteAddress) : 
                base(endpointConfigurationName, remoteAddress) {
        }
        
        public PeriodicoNotaClient(string endpointConfigurationName, System.ServiceModel.EndpointAddress remoteAddress) : 
                base(endpointConfigurationName, remoteAddress) {
        }
        
        public PeriodicoNotaClient(System.ServiceModel.Channels.Binding binding, System.ServiceModel.EndpointAddress remoteAddress) : 
                base(binding, remoteAddress) {
        }
        
        public string Teste(int value) {
            return base.Channel.Teste(value);
        }
        
        public string periodico_CadastrarProximo(byte[] Chave, int PGF) {
            return base.Channel.periodico_CadastrarProximo(Chave, PGF);
        }
    }
}
#pragma warning restore 1591