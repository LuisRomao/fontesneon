﻿namespace DP.impressos
{


    partial class dRecSal
    {
        private dRecSalTableAdapters.HOLeritesTableAdapter hOLeritesTableAdapter;

        private dRecSalTableAdapters.HOLeritesTableAdapter hOLeritesTableAdapterF;

        /// <summary>
        /// TableAdapter padrão para tabela: HOLerites
        /// </summary>
        public dRecSalTableAdapters.HOLeritesTableAdapter HOLeritesTableAdapter
        {
            get
            {
                if (hOLeritesTableAdapter == null)
                {
                    hOLeritesTableAdapter = new dRecSalTableAdapters.HOLeritesTableAdapter();
                    hOLeritesTableAdapter.TrocarStringDeConexao();
                };
                return hOLeritesTableAdapter;
            }
        }

        /// <summary>
        /// TableAdapter padrão para tabela: HOLerites
        /// </summary>
        public dRecSalTableAdapters.HOLeritesTableAdapter HOLeritesTableAdapterF
        {
            get
            {
                if (hOLeritesTableAdapterF == null)
                {
                    hOLeritesTableAdapterF = new dRecSalTableAdapters.HOLeritesTableAdapter();
                    hOLeritesTableAdapterF.TrocarStringDeConexao(VirDB.Bancovirtual.TiposDeBanco.Filial);
                };
                return hOLeritesTableAdapterF;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="EMP"></param>
        /// <returns></returns>
        public dRecSalTableAdapters.HOLeritesTableAdapter HOLeritesTableAdapterX(int EMP)
        {
            if (EMP == Framework.DSCentral.EMP)
                return HOLeritesTableAdapter;
            else
                return HOLeritesTableAdapterF;
        }

        private dRecSalTableAdapters.HOleritTituloTableAdapter hOleritTituloTableAdapter;
        private dRecSalTableAdapters.HOleritTituloTableAdapter hOleritTituloTableAdapterF;

        /// <summary>
        /// TableAdapter padrão para tabela: HOleritTitulo
        /// </summary>
        public dRecSalTableAdapters.HOleritTituloTableAdapter HOleritTituloTableAdapter
        {
            get
            {
                if (hOleritTituloTableAdapter == null)
                {
                    hOleritTituloTableAdapter = new dRecSalTableAdapters.HOleritTituloTableAdapter();
                    hOleritTituloTableAdapter.TrocarStringDeConexao();
                };
                return hOleritTituloTableAdapter;
            }
        }

        /// <summary>
        /// TableAdapter padrão para tabela: HOleritTitulo
        /// </summary>
        public dRecSalTableAdapters.HOleritTituloTableAdapter HOleritTituloTableAdapterF
        {
            get
            {
                if (hOleritTituloTableAdapterF == null)
                {
                    hOleritTituloTableAdapterF = new dRecSalTableAdapters.HOleritTituloTableAdapter();
                    hOleritTituloTableAdapterF.TrocarStringDeConexao(VirDB.Bancovirtual.TiposDeBanco.Filial);
                    hOleritTituloTableAdapterF.ClearBeforeFill = false;
                };
                return hOleritTituloTableAdapterF;
            }
        }
    }
}
