namespace DP.impressos
{
    partial class ImpRecSal
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.dRecSal = new DP.impressos.dRecSal();
            this.xrSubreport2 = new DevExpress.XtraReports.UI.XRSubreport();
            this.xrSubreport1 = new DevExpress.XtraReports.UI.XRSubreport();
            this.xrPageInfo1 = new DevExpress.XtraReports.UI.XRPageInfo();
            this.xrLabelx1 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabelx2 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel3 = new DevExpress.XtraReports.UI.XRLabel();
            ((System.ComponentModel.ISupportInitialize)(this.dImpDestRem1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.bindingSourcePrincipal)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.bindingSource1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dRecSal)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            // 
            // QuadroSegundaPagina
            // 
            this.QuadroSegundaPagina.StylePriority.UseBackColor = false;
            // 
            // bindingSourcePrincipal
            // 
            this.bindingSourcePrincipal.DataMember = "Recibos";
            this.bindingSourcePrincipal.DataSource = this.dRecSal;
            // 
            // Detail
            // 
            this.Detail.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrSubreport1,
            this.xrSubreport2});
            this.Detail.StylePriority.UseTextAlignment = false;
            this.Detail.BeforePrint += new System.Drawing.Printing.PrintEventHandler(this.Detail_BeforePrint);
            this.Detail.Controls.SetChildIndex(this.xrSubreport2, 0);
            this.Detail.Controls.SetChildIndex(this.xrSubreport1, 0);
            this.Detail.Controls.SetChildIndex(this.QuadroEndereco, 0);
            this.Detail.Controls.SetChildIndex(this.QuadroPrincipal, 0);
            this.Detail.Controls.SetChildIndex(this.QuadroSegundaPagina, 0);
            this.Detail.Controls.SetChildIndex(this.xrSigla1, 0);
            // 
            // QuadroEndereco
            // 
            this.QuadroEndereco.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrLabel3,
            this.xrLabelx2,
            this.xrPageInfo1,
            this.xrLabelx1});
            this.QuadroEndereco.Controls.SetChildIndex(this.xrLabelx1, 0);
            this.QuadroEndereco.Controls.SetChildIndex(this.xrPageInfo1, 0);
            this.QuadroEndereco.Controls.SetChildIndex(this.xrLabelx2, 0);
            this.QuadroEndereco.Controls.SetChildIndex(this.xrLabel3, 0);
            // 
            // xrSigla1
            // 
            this.xrSigla1.StylePriority.UseFont = false;
            this.xrSigla1.StylePriority.UseTextAlignment = false;
            this.xrSigla1.Visible = false;
            // 
            // dRecSal
            // 
            this.dRecSal.DataSetName = "dRecSal";
            this.dRecSal.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // xrSubreport2
            // 
            this.xrSubreport2.Dpi = 254F;
            this.xrSubreport2.LocationFloat = new DevExpress.Utils.PointFloat(0F, 256F);
            this.xrSubreport2.Name = "xrSubreport2";
            this.xrSubreport2.ReportSource = new DP.ImpSubRecSalR();
            this.xrSubreport2.SizeF = new System.Drawing.SizeF(1799F, 254F);
            // 
            // xrSubreport1
            // 
            this.xrSubreport1.Dpi = 254F;
            this.xrSubreport1.LocationFloat = new DevExpress.Utils.PointFloat(0F, 21F);
            this.xrSubreport1.Name = "xrSubreport1";
            this.xrSubreport1.ReportSource = new DP.impressos.ImpSubRecSal();
            this.xrSubreport1.SizeF = new System.Drawing.SizeF(1798F, 233F);
            // 
            // xrPageInfo1
            // 
            this.xrPageInfo1.Dpi = 254F;
            this.xrPageInfo1.Font = new System.Drawing.Font("Times New Roman", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrPageInfo1.LocationFloat = new DevExpress.Utils.PointFloat(21F, 826F);
            this.xrPageInfo1.Name = "xrPageInfo1";
            this.xrPageInfo1.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrPageInfo1.SizeF = new System.Drawing.SizeF(169F, 37F);
            this.xrPageInfo1.StylePriority.UseFont = false;
            // 
            // xrLabelx1
            // 
            this.xrLabelx1.Dpi = 254F;
            this.xrLabelx1.Font = new System.Drawing.Font("Times New Roman", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabelx1.LocationFloat = new DevExpress.Utils.PointFloat(635F, 275F);
            this.xrLabelx1.Name = "xrLabelx1";
            this.xrLabelx1.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrLabelx1.SizeF = new System.Drawing.SizeF(381F, 85F);
            this.xrLabelx1.StylePriority.UseFont = false;
            this.xrLabelx1.Text = "HOLERITE";
            // 
            // xrLabelx2
            // 
            this.xrLabelx2.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "UsoInterno")});
            this.xrLabelx2.Dpi = 254F;
            this.xrLabelx2.LocationFloat = new DevExpress.Utils.PointFloat(212F, 826F);
            this.xrLabelx2.Name = "xrLabelx2";
            this.xrLabelx2.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrLabelx2.SizeF = new System.Drawing.SizeF(1376F, 37F);
            this.xrLabelx2.Text = "xrLabelx2";
            // 
            // xrLabel3
            // 
            this.xrLabel3.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "HOL")});
            this.xrLabel3.Dpi = 254F;
            this.xrLabel3.LocationFloat = new DevExpress.Utils.PointFloat(21F, 889F);
            this.xrLabel3.Name = "xrLabel3";
            this.xrLabel3.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrLabel3.SizeF = new System.Drawing.SizeF(169F, 37F);
            this.xrLabel3.Text = "xrLabel3";
            // 
            // ImpRecSal
            // 
            this.Bands.AddRange(new DevExpress.XtraReports.UI.Band[] {
            this.Detail,
            this.bottomMarginBand1});
            this.Version = "15.2";
            ((System.ComponentModel.ISupportInitialize)(this.dImpDestRem1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.bindingSourcePrincipal)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.bindingSource1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dRecSal)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();

        }

        #endregion

        private DevExpress.XtraReports.UI.XRSubreport xrSubreport2;
        private DevExpress.XtraReports.UI.XRSubreport xrSubreport1;
        /// <summary>
        /// 
        /// </summary>
        public dRecSal dRecSal;
        private DevExpress.XtraReports.UI.XRPageInfo xrPageInfo1;
        private DevExpress.XtraReports.UI.XRLabel xrLabelx1;
        private DevExpress.XtraReports.UI.XRLabel xrLabelx2;
        private DevExpress.XtraReports.UI.XRLabel xrLabel3;

    }
}