namespace DP.impressos
{
    partial class ImpSubRecSal
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            DevExpress.XtraPrinting.BarCode.Interleaved2of5Generator interleaved2of5Generator1 = new DevExpress.XtraPrinting.BarCode.Interleaved2of5Generator();
            DevExpress.XtraPrinting.Shape.ShapeArrow shapeArrow1 = new DevExpress.XtraPrinting.Shape.ShapeArrow();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(ImpSubRecSal));
            this.Detail = new DevExpress.XtraReports.UI.DetailBand();
            this.xrPanel1 = new DevExpress.XtraReports.UI.XRPanel();
            this.Qb5 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel13 = new DevExpress.XtraReports.UI.XRLabel();
            this.Qb4 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel12 = new DevExpress.XtraReports.UI.XRLabel();
            this.Qb3 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel11 = new DevExpress.XtraReports.UI.XRLabel();
            this.Qb2 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel9 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel3 = new DevExpress.XtraReports.UI.XRLabel();
            this.Qb1 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrBarCode1 = new DevExpress.XtraReports.UI.XRBarCode();
            this.xrVia = new DevExpress.XtraReports.UI.XRLabel();
            this.Q0 = new DevExpress.XtraReports.UI.XRLabel();
            this.QOutros = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLine1 = new DevExpress.XtraReports.UI.XRLine();
            this.xrLabel7 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrShape1 = new DevExpress.XtraReports.UI.XRShape();
            this.QTot3 = new DevExpress.XtraReports.UI.XRLabel();
            this.QTot2 = new DevExpress.XtraReports.UI.XRLabel();
            this.QTot1 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel2 = new DevExpress.XtraReports.UI.XRLabel();
            this.Q3 = new DevExpress.XtraReports.UI.XRLabel();
            this.Q2 = new DevExpress.XtraReports.UI.XRLabel();
            this.Q1 = new DevExpress.XtraReports.UI.XRLabel();
            this.QDesconto = new DevExpress.XtraReports.UI.XRLabel();
            this.QVenc = new DevExpress.XtraReports.UI.XRLabel();
            this.QRef = new DevExpress.XtraReports.UI.XRLabel();
            this.QDesc = new DevExpress.XtraReports.UI.XRLabel();
            this.QCod = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTitulo = new DevExpress.XtraReports.UI.XRLabel();
            this.xrRecibo = new DevExpress.XtraReports.UI.XRLabel();
            this.topMarginBand1 = new DevExpress.XtraReports.UI.TopMarginBand();
            this.bottomMarginBand1 = new DevExpress.XtraReports.UI.BottomMarginBand();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            // 
            // Detail
            // 
            this.Detail.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.Detail.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrPanel1,
            this.xrBarCode1,
            this.xrVia,
            this.Q0,
            this.QOutros,
            this.xrLine1,
            this.xrLabel7,
            this.xrShape1,
            this.QTot3,
            this.QTot2,
            this.QTot1,
            this.xrLabel2,
            this.Q3,
            this.Q2,
            this.Q1,
            this.QDesconto,
            this.QVenc,
            this.QRef,
            this.QDesc,
            this.QCod,
            this.xrTitulo,
            this.xrRecibo});
            this.Detail.Dpi = 254F;
            this.Detail.HeightF = 1355F;
            this.Detail.Name = "Detail";
            this.Detail.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            this.Detail.StylePriority.UseBorders = false;
            this.Detail.StylePriority.UseTextAlignment = false;
            this.Detail.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight;
            // 
            // xrPanel1
            // 
            this.xrPanel1.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.Qb5,
            this.xrLabel13,
            this.Qb4,
            this.xrLabel12,
            this.Qb3,
            this.xrLabel11,
            this.Qb2,
            this.xrLabel9,
            this.xrLabel3,
            this.Qb1});
            this.xrPanel1.Dpi = 254F;
            this.xrPanel1.LocationFloat = new DevExpress.Utils.PointFloat(381F, 1228F);
            this.xrPanel1.Name = "xrPanel1";
            this.xrPanel1.SizeF = new System.Drawing.SizeF(1164F, 85F);
            // 
            // Qb5
            // 
            this.Qb5.BackColor = System.Drawing.Color.Transparent;
            this.Qb5.Dpi = 254F;
            this.Qb5.LocationFloat = new DevExpress.Utils.PointFloat(1016F, 45F);
            this.Qb5.Multiline = true;
            this.Qb5.Name = "Qb5";
            this.Qb5.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.Qb5.SizeF = new System.Drawing.SizeF(148F, 40F);
            this.Qb5.StylePriority.UseBackColor = false;
            this.Qb5.StylePriority.UseTextAlignment = false;
            this.Qb5.Text = "01";
            this.Qb5.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight;
            // 
            // xrLabel13
            // 
            this.xrLabel13.Dpi = 254F;
            this.xrLabel13.Font = new System.Drawing.Font("Times New Roman", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel13.LocationFloat = new DevExpress.Utils.PointFloat(1016F, 0F);
            this.xrLabel13.Name = "xrLabel13";
            this.xrLabel13.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrLabel13.SizeF = new System.Drawing.SizeF(148F, 42F);
            this.xrLabel13.StylePriority.UseFont = false;
            this.xrLabel13.Text = "Faixa IRRF";
            // 
            // Qb4
            // 
            this.Qb4.BackColor = System.Drawing.Color.Transparent;
            this.Qb4.Dpi = 254F;
            this.Qb4.LocationFloat = new DevExpress.Utils.PointFloat(762F, 45F);
            this.Qb4.Multiline = true;
            this.Qb4.Name = "Qb4";
            this.Qb4.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.Qb4.SizeF = new System.Drawing.SizeF(233F, 40F);
            this.Qb4.StylePriority.UseBackColor = false;
            this.Qb4.StylePriority.UseTextAlignment = false;
            this.Qb4.Text = "2.000,00";
            this.Qb4.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight;
            // 
            // xrLabel12
            // 
            this.xrLabel12.Dpi = 254F;
            this.xrLabel12.Font = new System.Drawing.Font("Times New Roman", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel12.LocationFloat = new DevExpress.Utils.PointFloat(762F, 0F);
            this.xrLabel12.Name = "xrLabel12";
            this.xrLabel12.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrLabel12.SizeF = new System.Drawing.SizeF(233F, 42F);
            this.xrLabel12.StylePriority.UseFont = false;
            this.xrLabel12.Text = "Base C�lc. IRRF";
            // 
            // Qb3
            // 
            this.Qb3.BackColor = System.Drawing.Color.Transparent;
            this.Qb3.Dpi = 254F;
            this.Qb3.LocationFloat = new DevExpress.Utils.PointFloat(529F, 45F);
            this.Qb3.Multiline = true;
            this.Qb3.Name = "Qb3";
            this.Qb3.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.Qb3.SizeF = new System.Drawing.SizeF(212F, 40F);
            this.Qb3.StylePriority.UseBackColor = false;
            this.Qb3.StylePriority.UseTextAlignment = false;
            this.Qb3.Text = "2.000,00";
            this.Qb3.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight;
            // 
            // xrLabel11
            // 
            this.xrLabel11.Dpi = 254F;
            this.xrLabel11.Font = new System.Drawing.Font("Times New Roman", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel11.LocationFloat = new DevExpress.Utils.PointFloat(508F, 0F);
            this.xrLabel11.Name = "xrLabel11";
            this.xrLabel11.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrLabel11.SizeF = new System.Drawing.SizeF(233F, 42F);
            this.xrLabel11.StylePriority.UseFont = false;
            this.xrLabel11.Text = "FGTS do M�s";
            // 
            // Qb2
            // 
            this.Qb2.BackColor = System.Drawing.Color.Transparent;
            this.Qb2.Dpi = 254F;
            this.Qb2.LocationFloat = new DevExpress.Utils.PointFloat(275F, 45F);
            this.Qb2.Multiline = true;
            this.Qb2.Name = "Qb2";
            this.Qb2.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.Qb2.SizeF = new System.Drawing.SizeF(212F, 40F);
            this.Qb2.StylePriority.UseBackColor = false;
            this.Qb2.StylePriority.UseTextAlignment = false;
            this.Qb2.Text = "2.000,00";
            this.Qb2.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight;
            // 
            // xrLabel9
            // 
            this.xrLabel9.Dpi = 254F;
            this.xrLabel9.Font = new System.Drawing.Font("Times New Roman", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel9.LocationFloat = new DevExpress.Utils.PointFloat(254F, 0F);
            this.xrLabel9.Name = "xrLabel9";
            this.xrLabel9.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrLabel9.SizeF = new System.Drawing.SizeF(233F, 42F);
            this.xrLabel9.StylePriority.UseFont = false;
            this.xrLabel9.Text = "Base C�lc. FGTS";
            // 
            // xrLabel3
            // 
            this.xrLabel3.Dpi = 254F;
            this.xrLabel3.Font = new System.Drawing.Font("Times New Roman", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel3.LocationFloat = new DevExpress.Utils.PointFloat(0F, 0F);
            this.xrLabel3.Name = "xrLabel3";
            this.xrLabel3.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrLabel3.SizeF = new System.Drawing.SizeF(233F, 42F);
            this.xrLabel3.StylePriority.UseFont = false;
            this.xrLabel3.Text = "Sal. Contr. I.N.S.S";
            // 
            // Qb1
            // 
            this.Qb1.BackColor = System.Drawing.Color.Transparent;
            this.Qb1.Dpi = 254F;
            this.Qb1.LocationFloat = new DevExpress.Utils.PointFloat(21F, 45F);
            this.Qb1.Multiline = true;
            this.Qb1.Name = "Qb1";
            this.Qb1.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.Qb1.SizeF = new System.Drawing.SizeF(212F, 40F);
            this.Qb1.StylePriority.UseBackColor = false;
            this.Qb1.StylePriority.UseTextAlignment = false;
            this.Qb1.Text = "2.000,00";
            this.Qb1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight;
            // 
            // xrBarCode1
            // 
            this.xrBarCode1.Dpi = 254F;
            this.xrBarCode1.LocationFloat = new DevExpress.Utils.PointFloat(0F, 1206F);
            this.xrBarCode1.Module = 2.25F;
            this.xrBarCode1.Name = "xrBarCode1";
            this.xrBarCode1.Padding = new DevExpress.XtraPrinting.PaddingInfo(25, 25, 0, 0, 254F);
            this.xrBarCode1.ShowText = false;
            this.xrBarCode1.SizeF = new System.Drawing.SizeF(360F, 106F);
            interleaved2of5Generator1.WideNarrowRatio = 3F;
            this.xrBarCode1.Symbology = interleaved2of5Generator1;
            this.xrBarCode1.Text = "101000067";
            // 
            // xrVia
            // 
            this.xrVia.Dpi = 254F;
            this.xrVia.Font = new System.Drawing.Font("Times New Roman", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrVia.LocationFloat = new DevExpress.Utils.PointFloat(1249F, 64.99999F);
            this.xrVia.Name = "xrVia";
            this.xrVia.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrVia.SizeF = new System.Drawing.SizeF(344.4583F, 42F);
            this.xrVia.StylePriority.UseFont = false;
            this.xrVia.Text = "Via condom�nio";
            // 
            // Q0
            // 
            this.Q0.Dpi = 254F;
            this.Q0.LocationFloat = new DevExpress.Utils.PointFloat(21F, 0F);
            this.Q0.Name = "Q0";
            this.Q0.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.Q0.SizeF = new System.Drawing.SizeF(698F, 42F);
            this.Q0.StylePriority.UseTextAlignment = false;
            this.Q0.Text = "AGO 2008";
            this.Q0.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // QOutros
            // 
            this.QOutros.Dpi = 254F;
            this.QOutros.LocationFloat = new DevExpress.Utils.PointFloat(0F, 1058F);
            this.QOutros.Multiline = true;
            this.QOutros.Name = "QOutros";
            this.QOutros.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.QOutros.SizeF = new System.Drawing.SizeF(868F, 127F);
            this.QOutros.StylePriority.UseTextAlignment = false;
            this.QOutros.Text = "DEPOSITO EFETUADO NA C.C. XX.XXXX..XXX\r\nBANCO - BRADESCO S/A - CONTA SAL�RIO\r\nAGE" +
    "NCIA - 01090";
            this.QOutros.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrLine1
            // 
            this.xrLine1.Dpi = 254F;
            this.xrLine1.LineStyle = System.Drawing.Drawing2D.DashStyle.Dash;
            this.xrLine1.LineWidth = 3;
            this.xrLine1.LocationFloat = new DevExpress.Utils.PointFloat(21F, 1334F);
            this.xrLine1.Name = "xrLine1";
            this.xrLine1.SizeF = new System.Drawing.SizeF(1778F, 21F);
            this.xrLine1.Visible = false;
            // 
            // xrLabel7
            // 
            this.xrLabel7.Dpi = 254F;
            this.xrLabel7.Font = new System.Drawing.Font("Times New Roman", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel7.LocationFloat = new DevExpress.Utils.PointFloat(910F, 1164F);
            this.xrLabel7.Name = "xrLabel7";
            this.xrLabel7.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrLabel7.SizeF = new System.Drawing.SizeF(233F, 40F);
            this.xrLabel7.StylePriority.UseFont = false;
            this.xrLabel7.Text = "Total L�quido";
            // 
            // xrShape1
            // 
            this.xrShape1.Angle = 270;
            this.xrShape1.Dpi = 254F;
            this.xrShape1.LocationFloat = new DevExpress.Utils.PointFloat(1164F, 1164F);
            this.xrShape1.Name = "xrShape1";
            this.xrShape1.Shape = shapeArrow1;
            this.xrShape1.SizeF = new System.Drawing.SizeF(106F, 40F);
            // 
            // QTot3
            // 
            this.QTot3.BackColor = System.Drawing.Color.Gainsboro;
            this.QTot3.Dpi = 254F;
            this.QTot3.Font = new System.Drawing.Font("Times New Roman", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.QTot3.LocationFloat = new DevExpress.Utils.PointFloat(1312F, 1143F);
            this.QTot3.Multiline = true;
            this.QTot3.Name = "QTot3";
            this.QTot3.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.QTot3.SizeF = new System.Drawing.SizeF(190F, 60F);
            this.QTot3.StylePriority.UseBackColor = false;
            this.QTot3.StylePriority.UseFont = false;
            this.QTot3.StylePriority.UseTextAlignment = false;
            this.QTot3.Text = "1.500,00";
            this.QTot3.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight;
            // 
            // QTot2
            // 
            this.QTot2.BackColor = System.Drawing.Color.WhiteSmoke;
            this.QTot2.Dpi = 254F;
            this.QTot2.LocationFloat = new DevExpress.Utils.PointFloat(1312F, 1058F);
            this.QTot2.Multiline = true;
            this.QTot2.Name = "QTot2";
            this.QTot2.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.QTot2.SizeF = new System.Drawing.SizeF(190F, 64F);
            this.QTot2.StylePriority.UseBackColor = false;
            this.QTot2.StylePriority.UseTextAlignment = false;
            this.QTot2.Text = "500,00";
            this.QTot2.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight;
            // 
            // QTot1
            // 
            this.QTot1.BackColor = System.Drawing.Color.WhiteSmoke;
            this.QTot1.Dpi = 254F;
            this.QTot1.LocationFloat = new DevExpress.Utils.PointFloat(1080F, 1058F);
            this.QTot1.Multiline = true;
            this.QTot1.Name = "QTot1";
            this.QTot1.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.QTot1.SizeF = new System.Drawing.SizeF(212F, 64F);
            this.QTot1.StylePriority.UseBackColor = false;
            this.QTot1.StylePriority.UseTextAlignment = false;
            this.QTot1.Text = "2.000,00";
            this.QTot1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight;
            // 
            // xrLabel2
            // 
            this.xrLabel2.Dpi = 254F;
            this.xrLabel2.Font = new System.Drawing.Font("Times New Roman", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel2.LocationFloat = new DevExpress.Utils.PointFloat(889F, 1058F);
            this.xrLabel2.Name = "xrLabel2";
            this.xrLabel2.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrLabel2.SizeF = new System.Drawing.SizeF(169F, 42F);
            this.xrLabel2.StylePriority.UseFont = false;
            this.xrLabel2.Text = "Totais";
            // 
            // Q3
            // 
            this.Q3.Dpi = 254F;
            this.Q3.LocationFloat = new DevExpress.Utils.PointFloat(21F, 127F);
            this.Q3.Name = "Q3";
            this.Q3.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.Q3.SizeF = new System.Drawing.SizeF(1524F, 42F);
            this.Q3.StylePriority.UseTextAlignment = false;
            this.Q3.Text = "010020-0    ROG�RIO DE ASSIS                                                     " +
    "                                    PORTEIRO";
            this.Q3.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // Q2
            // 
            this.Q2.Dpi = 254F;
            this.Q2.LocationFloat = new DevExpress.Utils.PointFloat(21F, 85F);
            this.Q2.Name = "Q2";
            this.Q2.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.Q2.SizeF = new System.Drawing.SizeF(1185F, 40F);
            this.Q2.StylePriority.UseTextAlignment = false;
            this.Q2.Text = "RUA ENG LUIZ LA                                                               02." +
    "342.861/0001-83";
            this.Q2.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // Q1
            // 
            this.Q1.Dpi = 254F;
            this.Q1.LocationFloat = new DevExpress.Utils.PointFloat(21F, 42F);
            this.Q1.Name = "Q1";
            this.Q1.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.Q1.SizeF = new System.Drawing.SizeF(698F, 42F);
            this.Q1.StylePriority.UseTextAlignment = false;
            this.Q1.Text = "089 COND ED SAINT ET";
            this.Q1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // QDesconto
            // 
            this.QDesconto.Dpi = 254F;
            this.QDesconto.LocationFloat = new DevExpress.Utils.PointFloat(1312F, 190F);
            this.QDesconto.Multiline = true;
            this.QDesconto.Name = "QDesconto";
            this.QDesconto.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.QDesconto.SizeF = new System.Drawing.SizeF(190F, 847F);
            this.QDesconto.StylePriority.UseTextAlignment = false;
            this.QDesconto.Text = "Descontos\r\n\r\n001\r\n002\r\n003\r\n004";
            this.QDesconto.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight;
            this.QDesconto.Draw += new DevExpress.XtraReports.UI.DrawEventHandler(this.xrLabel2_Draw);
            // 
            // QVenc
            // 
            this.QVenc.Dpi = 254F;
            this.QVenc.LocationFloat = new DevExpress.Utils.PointFloat(1080F, 190F);
            this.QVenc.Multiline = true;
            this.QVenc.Name = "QVenc";
            this.QVenc.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.QVenc.SizeF = new System.Drawing.SizeF(212F, 847F);
            this.QVenc.StylePriority.UseTextAlignment = false;
            this.QVenc.Text = "Vencimentos\r\n\r\n001\r\n002\r\n003\r\n004";
            this.QVenc.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight;
            this.QVenc.Draw += new DevExpress.XtraReports.UI.DrawEventHandler(this.xrLabel2_Draw);
            // 
            // QRef
            // 
            this.QRef.Dpi = 254F;
            this.QRef.LocationFloat = new DevExpress.Utils.PointFloat(931F, 190F);
            this.QRef.Multiline = true;
            this.QRef.Name = "QRef";
            this.QRef.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.QRef.SizeF = new System.Drawing.SizeF(127F, 847F);
            this.QRef.StylePriority.UseTextAlignment = false;
            this.QRef.Text = "Ref\r\n\r\n001\r\n002\r\n003\r\n004";
            this.QRef.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight;
            this.QRef.Draw += new DevExpress.XtraReports.UI.DrawEventHandler(this.xrLabel2_Draw);
            // 
            // QDesc
            // 
            this.QDesc.Dpi = 254F;
            this.QDesc.LocationFloat = new DevExpress.Utils.PointFloat(106F, 190F);
            this.QDesc.Multiline = true;
            this.QDesc.Name = "QDesc";
            this.QDesc.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.QDesc.SizeF = new System.Drawing.SizeF(804F, 847F);
            this.QDesc.StylePriority.UseTextAlignment = false;
            this.QDesc.Text = "Descri��o\r\n\r\ndfasdfas001\r\n00dfsdfasd2\r\n00sdfasd3\r\n004asfasdf";
            this.QDesc.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            this.QDesc.Draw += new DevExpress.XtraReports.UI.DrawEventHandler(this.xrLabel2_Draw);
            // 
            // QCod
            // 
            this.QCod.Dpi = 254F;
            this.QCod.LocationFloat = new DevExpress.Utils.PointFloat(0F, 190F);
            this.QCod.Multiline = true;
            this.QCod.Name = "QCod";
            this.QCod.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.QCod.SizeF = new System.Drawing.SizeF(85F, 847F);
            this.QCod.StylePriority.UseTextAlignment = false;
            this.QCod.Text = "Cod\r\n\r\n001\r\n002\r\n003\r\n004";
            this.QCod.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            this.QCod.Draw += new DevExpress.XtraReports.UI.DrawEventHandler(this.xrLabel2_Draw);
            // 
            // xrTitulo
            // 
            this.xrTitulo.Dpi = 254F;
            this.xrTitulo.Font = new System.Drawing.Font("Times New Roman", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrTitulo.LocationFloat = new DevExpress.Utils.PointFloat(762F, 0F);
            this.xrTitulo.Name = "xrTitulo";
            this.xrTitulo.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTitulo.SizeF = new System.Drawing.SizeF(1016F, 64F);
            this.xrTitulo.StylePriority.UseFont = false;
            this.xrTitulo.Text = "Recibo de Pagamento de Sal�rio.";
            // 
            // xrRecibo
            // 
            this.xrRecibo.Angle = 90F;
            this.xrRecibo.BackColor = System.Drawing.Color.WhiteSmoke;
            this.xrRecibo.Dpi = 254F;
            this.xrRecibo.KeepTogether = true;
            this.xrRecibo.LocationFloat = new DevExpress.Utils.PointFloat(1566F, 127F);
            this.xrRecibo.Multiline = true;
            this.xrRecibo.Name = "xrRecibo";
            this.xrRecibo.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrRecibo.SizeF = new System.Drawing.SizeF(233F, 1122F);
            this.xrRecibo.StylePriority.UseBackColor = false;
            this.xrRecibo.StylePriority.UseTextAlignment = false;
            this.xrRecibo.Text = resources.GetString("xrRecibo.Text");
            this.xrRecibo.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // topMarginBand1
            // 
            this.topMarginBand1.Dpi = 254F;
            this.topMarginBand1.Name = "topMarginBand1";
            // 
            // bottomMarginBand1
            // 
            this.bottomMarginBand1.Dpi = 254F;
            this.bottomMarginBand1.Name = "bottomMarginBand1";
            // 
            // ImpSubRecSal
            // 
            this.Bands.AddRange(new DevExpress.XtraReports.UI.Band[] {
            this.Detail,
            this.topMarginBand1,
            this.bottomMarginBand1});
            this.Dpi = 254F;
            this.PageHeight = 2970;
            this.PageWidth = 2100;
            this.PaperKind = System.Drawing.Printing.PaperKind.A4;
            this.ReportUnit = DevExpress.XtraReports.UI.ReportUnit.TenthsOfAMillimeter;
            this.Version = "13.2";
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();

        }

        #endregion

        /// <summary>
        /// 
        /// </summary>
        public DevExpress.XtraReports.UI.XRLabel QCod;
        /// <summary>
        /// 
        /// </summary>
        public DevExpress.XtraReports.UI.XRLabel QDesc;
        /// <summary>
        /// 
        /// </summary>
        public DevExpress.XtraReports.UI.XRLabel QRef;
        /// <summary>
        /// 
        /// </summary>
        public DevExpress.XtraReports.UI.XRLabel QDesconto;
        /// <summary>
        /// 
        /// </summary>
        public DevExpress.XtraReports.UI.XRLabel QVenc;
        /// <summary>
        /// 
        /// </summary>
        public DevExpress.XtraReports.UI.XRLabel Q3;
        /// <summary>
        /// 
        /// </summary>
        public DevExpress.XtraReports.UI.XRLabel Q2;
        /// <summary>
        /// 
        /// </summary>
        public DevExpress.XtraReports.UI.XRLabel Q1;
        /// <summary>
        /// 
        /// </summary>
        public DevExpress.XtraReports.UI.XRLabel QTot2;
        /// <summary>
        /// 
        /// </summary>
        public DevExpress.XtraReports.UI.XRLabel QTot1;
        /// <summary>
        /// 
        /// </summary>
        public DevExpress.XtraReports.UI.XRLabel QTot3;
        private DevExpress.XtraReports.UI.XRLine xrLine1;
        /// <summary>
        /// 
        /// </summary>
        public DevExpress.XtraReports.UI.XRLabel QOutros;
        private DevExpress.XtraReports.UI.XRLabel xrLabel12;
        private DevExpress.XtraReports.UI.XRLabel xrLabel11;
        /// <summary>
        /// 
        /// </summary>
        public DevExpress.XtraReports.UI.XRLabel Qb2;
        private DevExpress.XtraReports.UI.XRLabel xrLabel9;
        /// <summary>
        /// 
        /// </summary>
        public DevExpress.XtraReports.UI.XRLabel Qb1;
        private DevExpress.XtraReports.UI.XRLabel xrLabel3;
        /// <summary>
        /// 
        /// </summary>
        public DevExpress.XtraReports.UI.XRLabel Qb5;
        /// <summary>
        /// 
        /// </summary>
        public DevExpress.XtraReports.UI.XRLabel Qb4;
        /// <summary>
        /// 
        /// </summary>
        public DevExpress.XtraReports.UI.XRLabel Qb3;
        private DevExpress.XtraReports.UI.XRLabel xrLabel13;
        /// <summary>
        /// 
        /// </summary>
        public DevExpress.XtraReports.UI.XRLabel Q0;
        /// <summary>
        /// 
        /// </summary>
        public DevExpress.XtraReports.UI.XRBarCode xrBarCode1;
        /// <summary>
        /// 
        /// </summary>
        protected DevExpress.XtraReports.UI.XRLabel xrRecibo;
        /// <summary>
        /// 
        /// </summary>
        protected DevExpress.XtraReports.UI.XRPanel xrPanel1;
        /// <summary>
        /// 
        /// </summary>
        protected DevExpress.XtraReports.UI.XRLabel xrTitulo;
        /// <summary>
        /// 
        /// </summary>
        protected DevExpress.XtraReports.UI.XRLabel xrVia;
        /// <summary>
        /// 
        /// </summary>
        protected DevExpress.XtraReports.UI.XRLabel xrLabel2;
        /// <summary>
        /// 
        /// </summary>
        protected DevExpress.XtraReports.UI.XRLabel xrLabel7;
        /// <summary>
        /// 
        /// </summary>
        protected DevExpress.XtraReports.UI.XRShape xrShape1;
        /// <summary>
        /// 
        /// </summary>
        protected DevExpress.XtraReports.UI.DetailBand Detail;
        private DevExpress.XtraReports.UI.TopMarginBand topMarginBand1;
        private DevExpress.XtraReports.UI.BottomMarginBand bottomMarginBand1;
    }
}
