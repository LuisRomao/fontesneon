﻿namespace DP.impressos {


    partial class dGravarDTM
    {
        private static dGravarDTM _dGravarDTMSt;

        /// <summary>
        /// dataset estático:dGravarDTM
        /// </summary>
        public static dGravarDTM dGravarDTMSt
        {
            get
            {
                if (_dGravarDTMSt == null)
                {
                    _dGravarDTMSt = new dGravarDTM();
                    _dGravarDTMSt.TMPApontamentoDTMTableAdapter.Fill(_dGravarDTMSt.TMPApontamentoDTM);
                }
                return _dGravarDTMSt;
            }
        }

        private dGravarDTMTableAdapters.TMPApontamentoDTMTableAdapter tMPApontamentoDTMTableAdapter;
        /// <summary>
        /// TableAdapter padrão para tabela: TMPApontamentoDTM
        /// </summary>
        public dGravarDTMTableAdapters.TMPApontamentoDTMTableAdapter TMPApontamentoDTMTableAdapter
        {
            get
            {
                if (tMPApontamentoDTMTableAdapter == null)
                {
                    tMPApontamentoDTMTableAdapter = new dGravarDTMTableAdapters.TMPApontamentoDTMTableAdapter();
                    tMPApontamentoDTMTableAdapter.TrocarStringDeConexao();
                };
                return tMPApontamentoDTMTableAdapter;
            }
        }
    }
}
