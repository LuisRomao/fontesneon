using System;
using System.Drawing;
using DevExpress.XtraReports.UI;

namespace DP.impressos
{
    /// <summary>
    /// 
    /// </summary>
    public partial class ImpPontoNeon : XtraReport
    {
        /// <summary>
        /// 
        /// </summary>
        public bool apontar;
        private int Linha;

        /// <summary>
        /// 
        /// </summary>
        public ImpPontoNeon()
        {
            InitializeComponent();
            if (!DesignMode)
                try
                {
                    string Sigla = CompontesBasicos.Bancovirtual.UsuarioLogado.UsuarioLogadoSt.USU.ToString();
                    char Anterior = ' ';
                    Array.ForEach(CompontesBasicos.Bancovirtual.UsuarioLogado.UsuarioLogadoSt.USUNome.ToCharArray(), delegate(char c)
                    {
                        if (char.IsWhiteSpace(Anterior))
                            Sigla += c.ToString().ToUpper();
                        Anterior = c;
                    });

                    xrSigla2.Text = Sigla;
                }
                catch 
                {
                    xrSigla2.Text = "";
                }
        }

        private RelPonto.dRelPonto.PONtoRow LinhaMae
        {
            get
            {
                System.Data.DataRowView DRV1 = (System.Data.DataRowView)GetCurrentRow();
                if (DRV1 == null)
                    return null;
                return (RelPonto.dRelPonto.PONtoRow)DRV1.Row;
            }
        }

        private void Detail_BeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e)
        {
            switch (Linha++)
            {
                case 3: 
                    xrTable3.Visible = true;
                    xrTableCell13.Text = "176";
                    xrTableCell14.Text = "Assembl�ia";
                    break;
                case 4:
                    xrTable3.Visible = true;
                    xrTableCell13.Text = "148";
                    xrTableCell14.Text = "H. Extra 100%";
                    break;
                case 5:
                    xrTable3.Visible = true;
                    xrTableCell13.Text = "146";
                    xrTableCell14.Text = "H. Extra 50%";
                    break;
                case 6:
                    xrTable3.Visible = true;
                    xrTableCell13.Text = "421";
                    xrTableCell14.Text = "F. Injustificada";
                    break;
                case 7:
                    xrTable3.Visible = true;
                    xrTableCell13.Text = "422";
                    xrTableCell14.Text = "Hora n�o trabalhada";
                    break;
                default:
                    xrTable3.Visible = false;
                    break;
            }
            
            if (LinhaMae != null)
            {
                string strmes = LinhaMae.PONData.ToString("MMMM").ToUpper();
                if (LinhaMae.PONData.Day >= 13)
                {
                    if (strmes.Length > LinhaMae.PONData.Day - 13)
                        xrTableCell12.Text = strmes.Substring(LinhaMae.PONData.Day - 13, 1);
                    else
                        xrTableCell12.Text = "";
                }
                else
                    if (strmes.Length > LinhaMae.PONData.Day - 1)
                        xrTableCell12.Text = strmes.Substring(LinhaMae.PONData.Day - 1, 1);
                    else
                        xrTableCell12.Text = "";
                Color cor = Color.White;

                if(LinhaMae.PONData.DayOfWeek == DayOfWeek.Saturday)
                    cor = Color.FromArgb(230, 230, 230);
                else
                    if (LinhaMae.PONData.DayOfWeek == DayOfWeek.Sunday)
                        cor = Color.FromArgb(190, 190, 190);                    
                xrTableCell4.BackColor = xrTableCell5.BackColor = xrTableCell6.BackColor = xrTableCell11.BackColor = cor;
            }
        }

        private void CantoRedondo(object sender, DrawEventArgs e)
        {
            dllImpresso.ImpGeradorDeEfeitos.CantosRedondos(sender, e, 30);
        }

        private void ImpPontoNeon_AfterPrint(object sender, EventArgs e)
        {
            if (apontar)
                Framework.DSCentral.IMPressoesTableAdapter.Incluir(410, 37, Pages.Count, DateTime.Now, CompontesBasicos.Bancovirtual.UsuarioLogado.UsuarioLogadoSt.USU, "A - Ponto", false);
        }

        private void GroupHeader1_BeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e)
        {
            Linha = 1;
        }

    }
}
