﻿namespace DP.impressos
{


    partial class dPonto
    {
        private static dPontoTableAdapters.PLanilhaPontoTableAdapter pLanilhaPontoTableAdapter;
        private static dPontoTableAdapters.PLanilhaPontoTableAdapter pLanilhaPontoTableAdapterF;

        /// <summary>
        /// TableAdapter padrão para tabela: PLanilhaPonto
        /// </summary>
        public static dPontoTableAdapters.PLanilhaPontoTableAdapter PLanilhaPontoTableAdapter
        {
            get
            {
                if (pLanilhaPontoTableAdapter == null)
                {
                    pLanilhaPontoTableAdapter = new dPontoTableAdapters.PLanilhaPontoTableAdapter();
                    pLanilhaPontoTableAdapter.TrocarStringDeConexao();
                };
                return pLanilhaPontoTableAdapter;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        public static dPontoTableAdapters.PLanilhaPontoTableAdapter PLanilhaPontoTableAdapterF
        {
            get
            {
                if (pLanilhaPontoTableAdapterF == null)
                {
                    pLanilhaPontoTableAdapterF = new dPontoTableAdapters.PLanilhaPontoTableAdapter();
                    pLanilhaPontoTableAdapterF.TrocarStringDeConexao(VirDB.Bancovirtual.TiposDeBanco.Filial);
                    pLanilhaPontoTableAdapterF.ClearBeforeFill = false;
                };
                return pLanilhaPontoTableAdapterF;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="EMP"></param>
        /// <returns></returns>
        public static dPontoTableAdapters.PLanilhaPontoTableAdapter PLanilhaPontoTableAdapterX(int EMP)
        {
            return EMP == Framework.DSCentral.EMP ? PLanilhaPontoTableAdapter : PLanilhaPontoTableAdapterF;
        }

        private dPontoTableAdapters.ResumoTableAdapter resumoTableAdapter;
        private dPontoTableAdapters.ResumoTableAdapter resumoTableAdapterF;

        /// <summary>
        /// TableAdapter padrão para tabela: Resumo
        /// </summary>
        public dPontoTableAdapters.ResumoTableAdapter ResumoTableAdapter
        {
            get
            {
                if (resumoTableAdapter == null)
                {
                    resumoTableAdapter = new dPontoTableAdapters.ResumoTableAdapter();
                    resumoTableAdapter.TrocarStringDeConexao();
                };
                return resumoTableAdapter;
            }
        }

        /// <summary>
        /// TableAdapter padrão para tabela: Resumo
        /// </summary>
        public dPontoTableAdapters.ResumoTableAdapter ResumoTableAdapterF
        {
            get
            {
                if (resumoTableAdapterF == null)
                {
                    resumoTableAdapterF = new dPontoTableAdapters.ResumoTableAdapter();
                    resumoTableAdapterF.TrocarStringDeConexao(VirDB.Bancovirtual.TiposDeBanco.Filial);
                    resumoTableAdapterF.ClearBeforeFill = false;
                };
                return resumoTableAdapterF;
            }
        }
    }
}
