using System;
using System.Drawing;

namespace DP.impressos
{
    /// <summary>
    /// 
    /// </summary>
    public partial class ImpRecSal : dllImpresso.ImpDestRem
    {
        private ImpSubRecSal Primeiro;
        private ImpSubRecSal Segundo;
        private DevExpress.XtraReports.UI.XtraReport Branco;

        /// <summary>
        /// construtor
        /// </summary>
        public ImpRecSal()
        {
            InitializeComponent();
            Primeiro = (ImpSubRecSal)xrSubreport2.ReportSource;
            Segundo = (ImpSubRecSal)xrSubreport1.ReportSource;
            Branco = new ImpBranco();
            Segundo.Segundo();
            OmitirApartamento();
        }

        private Font FontOriginal = null;

        private void SetFontes(Font F) {
            Primeiro.QCod.Font = Segundo.QCod.Font = F;
            Primeiro.QDesc.Font = Segundo.QDesc.Font = F;
            Primeiro.QRef.Font = Segundo.QRef.Font = F;
            Primeiro.QVenc.Font = Segundo.QVenc.Font = F;
            Primeiro.QDesconto.Font = Segundo.QDesconto.Font = F;            
        }

        private void Detail_BeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e)
        {
            //object o = GetCurrentRow();
            
            impressos.dRecSal.RecibosRow row = dRecSal.Recibos[CurrentRowIndex];
            if (row.IsDuasViasNull())
            {
                string erro = string.Format("Verifique o cadastro do condomínio: HOL: {0} - CON: {1} - Nome Condomínio:",row.HOL,row.CON,row.CONNome);
                VirEmailNeon.EmailDiretoNeon.EmalST.Enviar("luis@virweb.com.br",erro,"Erro em holerit");
                System.Windows.Forms.MessageBox.Show(erro);
                row.DuasVias = false;
            }
            if (row.DuasVias)
            {
                //Segundo.Visible = true;
                xrSubreport2.ReportSource = Primeiro;
                xrSubreport1.ReportSource = Segundo;
            }
            else
            {
                //Segundo.Visible = false;
                //xrSubreport1.Visible = false;
                //Primeiro.TopF = 0;
                xrSubreport2.ReportSource = Branco;
                xrSubreport1.ReportSource = Primeiro;
            }
            
            if (FontOriginal != null) {
                SetFontes(FontOriginal);
                FontOriginal = null;
            };
            int linhas = row.Cod.Split(new string[1] { "\r\n" }, StringSplitOptions.None).Length;
            int Red = 0;
            if (linhas > 31)
                Red = 4;
            else
             if (linhas > 27)
                Red = 3;
             else
              if (linhas > 24)
                  Red = 2;
              else
                  if (linhas > 20)
                      Red = 1;
            if (Red > 0) {
                FontOriginal = Primeiro.QCod.Font;
                SetFontes(new Font(FontOriginal.FontFamily, FontOriginal.Size - Red));
                //Primeiro.QCod.Font = ;
                //Segundo.QDesc.Font = Primeiro.QDesc.Font = Segundo.QCod.Font = Primeiro.QCod.Font;
                //Primeiro.QRef.Font = 
            };            
            Primeiro.QCod.Text = Segundo.QCod.Text = row.Cod;
            Primeiro.QDesc.Text = Segundo.QDesc.Text = row.Descricao;
            Primeiro.QRef.Text = Segundo.QRef.Text = row.Ref;
            Primeiro.QVenc.Text = Segundo.QVenc.Text = row.Venc;
            Primeiro.QDesconto.Text = Segundo.QDesconto.Text = row.Descontos;
            Primeiro.Q0.Text = Segundo.Q0.Text = row.Q0;
            Primeiro.Q1.Text = Segundo.Q1.Text = row.Q1;
            Primeiro.Q2.Text = Segundo.Q2.Text = row.Q2;
            Primeiro.Q3.Text = Segundo.Q3.Text = row.Q3;
            Primeiro.QOutros.Text = Segundo.QOutros.Text = row.QOutros;
            Primeiro.Qb1.Text = Segundo.Qb1.Text = row.Qb1;
            Primeiro.Qb2.Text = Segundo.Qb2.Text = row.Qb2;
            Primeiro.Qb3.Text = Segundo.Qb3.Text = row.Qb3;
            Primeiro.Qb4.Text = Segundo.Qb4.Text = row.Qb4;
            Primeiro.Qb5.Text = Segundo.Qb5.Text = row.Qb5;
            Primeiro.QTot1.Text = Segundo.QTot1.Text = row.Qtot1.ToString("n2");
            Primeiro.QTot2.Text = Segundo.QTot2.Text = row.Qtot2.ToString("n2");
            Primeiro.QTot3.Text = Segundo.QTot3.Text = row.Qtot3.ToString("n2");
            Primeiro.xrBarCode1.Text = "102" + row.HOL.ToString("000");
        }
    }
}

