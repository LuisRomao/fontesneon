using DevExpress.XtraReports.UI;

namespace DP.impressos
{
    /// <summary>
    /// 
    /// </summary>
    public partial class ImpSubRecSal : DevExpress.XtraReports.UI.XtraReport
    {
        /// <summary>
        /// Construtor
        /// </summary>
        public ImpSubRecSal()
        {
            InitializeComponent();
        }

        /// <summary>
        /// Ajusta a segunda c�pia
        /// </summary>
        public void Segundo() {
            xrTitulo.Text = "Demonstrativo de Pagamento de Sal�rio";
            xrVia.Text = "Via Funcion�rio";
            xrRecibo.Visible = xrBarCode1.Visible = false;
            xrLine1.Visible = true;
        }

        private void xrLabel2_Draw(object sender, DrawEventArgs e)
        {
            dllImpresso.ImpGeradorDeEfeitos.Degrade(sender, e);
        }
    }

}
