namespace DP
{
    partial class ImpSubRecSalR
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.xrLabel1 = new DevExpress.XtraReports.UI.XRLabel();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            // 
            // QCod
            // 
            this.QCod.Location = new System.Drawing.Point(0, 85);
            this.QCod.StylePriority.UseTextAlignment = false;
            // 
            // QDesc
            // 
            this.QDesc.Location = new System.Drawing.Point(106, 85);
            this.QDesc.StylePriority.UseTextAlignment = false;
            // 
            // QRef
            // 
            this.QRef.Location = new System.Drawing.Point(931, 85);
            this.QRef.StylePriority.UseTextAlignment = false;
            // 
            // QDesconto
            // 
            this.QDesconto.Location = new System.Drawing.Point(1312, 85);
            this.QDesconto.StylePriority.UseTextAlignment = false;
            // 
            // QVenc
            // 
            this.QVenc.Location = new System.Drawing.Point(1080, 85);
            this.QVenc.StylePriority.UseTextAlignment = false;
            // 
            // Q3
            // 
            this.Q3.Location = new System.Drawing.Point(22, 1122);
            this.Q3.StylePriority.UseTextAlignment = false;
            // 
            // Q2
            // 
            this.Q2.Location = new System.Drawing.Point(22, 1080);
            this.Q2.StylePriority.UseTextAlignment = false;
            // 
            // Q1
            // 
            this.Q1.Location = new System.Drawing.Point(22, 1036);
            this.Q1.StylePriority.UseTextAlignment = false;
            // 
            // QTot2
            // 
            this.QTot2.Location = new System.Drawing.Point(1376, 42);
            this.QTot2.Size = new System.Drawing.Size(169, 42);
            this.QTot2.StylePriority.UseBackColor = false;
            this.QTot2.StylePriority.UseTextAlignment = false;
            // 
            // QTot1
            // 
            this.QTot1.Location = new System.Drawing.Point(1185, 42);
            this.QTot1.Size = new System.Drawing.Size(169, 42);
            this.QTot1.StylePriority.UseBackColor = false;
            this.QTot1.StylePriority.UseTextAlignment = false;
            // 
            // QTot3
            // 
            this.QTot3.Location = new System.Drawing.Point(1588, 42);
            this.QTot3.Size = new System.Drawing.Size(212, 42);
            this.QTot3.StylePriority.UseBackColor = false;
            this.QTot3.StylePriority.UseFont = false;
            this.QTot3.StylePriority.UseTextAlignment = false;
            // 
            // QOutros
            // 
            this.QOutros.Location = new System.Drawing.Point(381, 1186);
            this.QOutros.StylePriority.UseTextAlignment = false;
            // 
            // Qb2
            // 
            this.Qb2.StylePriority.UseBackColor = false;
            this.Qb2.StylePriority.UseTextAlignment = false;
            // 
            // Qb1
            // 
            this.Qb1.StylePriority.UseBackColor = false;
            this.Qb1.StylePriority.UseTextAlignment = false;
            // 
            // Qb5
            // 
            this.Qb5.StylePriority.UseBackColor = false;
            this.Qb5.StylePriority.UseTextAlignment = false;
            // 
            // Qb4
            // 
            this.Qb4.StylePriority.UseBackColor = false;
            this.Qb4.StylePriority.UseTextAlignment = false;
            // 
            // Qb3
            // 
            this.Qb3.StylePriority.UseBackColor = false;
            this.Qb3.StylePriority.UseTextAlignment = false;
            // 
            // Q0
            // 
            this.Q0.Font = new System.Drawing.Font("Times New Roman", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Q0.Location = new System.Drawing.Point(826, 952);
            this.Q0.Size = new System.Drawing.Size(318, 64);
            this.Q0.StylePriority.UseFont = false;
            this.Q0.StylePriority.UseTextAlignment = false;
            // 
            // xrRecibo
            // 
            this.xrRecibo.Location = new System.Drawing.Point(1566, 85);
            this.xrRecibo.Size = new System.Drawing.Size(233, 1228);
            this.xrRecibo.StylePriority.UseBackColor = false;
            this.xrRecibo.StylePriority.UseTextAlignment = false;
            // 
            // xrPanel1
            // 
            this.xrPanel1.Location = new System.Drawing.Point(0, 0);
            // 
            // xrTitulo
            // 
            this.xrTitulo.Location = new System.Drawing.Point(21, 952);
            this.xrTitulo.Size = new System.Drawing.Size(783, 64);
            this.xrTitulo.StylePriority.UseFont = false;
            this.xrTitulo.StylePriority.UseTextAlignment = false;
            this.xrTitulo.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrVia
            // 
            this.xrVia.Location = new System.Drawing.Point(21, 1164);
            this.xrVia.StylePriority.UseFont = false;
            // 
            // xrLabel2
            // 
            this.xrLabel2.Location = new System.Drawing.Point(1164, 0);
            this.xrLabel2.Size = new System.Drawing.Size(190, 40);
            this.xrLabel2.StylePriority.UseFont = false;
            this.xrLabel2.StylePriority.UseTextAlignment = false;
            this.xrLabel2.Text = "Vencimentos";
            this.xrLabel2.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight;
            // 
            // xrLabel7
            // 
            this.xrLabel7.Location = new System.Drawing.Point(1672, 0);
            this.xrLabel7.Size = new System.Drawing.Size(128, 40);
            this.xrLabel7.StylePriority.UseFont = false;
            this.xrLabel7.StylePriority.UseTextAlignment = false;
            this.xrLabel7.Text = "L�quido";
            this.xrLabel7.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight;
            // 
            // xrShape1
            // 
            this.xrShape1.Location = new System.Drawing.Point(1334, 1016);
            this.xrShape1.Visible = false;
            // 
            // Detail
            // 
            this.Detail.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrLabel1});
            this.Detail.StylePriority.UseBorders = false;
            this.Detail.StylePriority.UseTextAlignment = false;
            // 
            // xrLabel1
            // 
            this.xrLabel1.Dpi = 254F;
            this.xrLabel1.Font = new System.Drawing.Font("Times New Roman", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel1.Location = new System.Drawing.Point(1376, 0);
            this.xrLabel1.Name = "xrLabel1";
            this.xrLabel1.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 96F);
            this.xrLabel1.Size = new System.Drawing.Size(169, 40);
            this.xrLabel1.StylePriority.UseFont = false;
            this.xrLabel1.Text = "Descontos";
            // 
            // ImpSubRecSalR
            // 
            this.Version = "8.2";
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();

        }

        #endregion

        private DevExpress.XtraReports.UI.XRLabel xrLabel1;
    }
}