﻿using Framework.objetosNeon;
using System;

namespace DP.impressos
{
    partial class dPlaPonto
    {
        /// <summary>
        /// 
        /// </summary>
        public int teste = 100;

        /// <summary>
        /// 
        /// </summary>
        /// <param name="dia"></param>
        /// <returns></returns>
        public string DiaSemana(DateTime dia)
        {
            switch (dia.DayOfWeek)
            {
                case DayOfWeek.Friday:
                    return "sex";
                case DayOfWeek.Monday:
                    return "seg";
                case DayOfWeek.Saturday:
                    return "sab";
                case DayOfWeek.Sunday:
                    return "dom";
                case DayOfWeek.Thursday:
                    return "qui";
                case DayOfWeek.Tuesday:
                    return "ter";
                case DayOfWeek.Wednesday:
                    return "qua";
                default: return null;
            }

        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="comp"></param>
        public void CargaInicial(Framework.objetosNeon.Competencia comp) {
            CargaInicial(comp, DateTime.MinValue, DateTime.MinValue, DateTime.MinValue, DateTime.MinValue,false);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="comp"></param>
        /// <param name="H1"></param>
        /// <param name="H2"></param>
        /// <param name="H3"></param>
        /// <param name="H4"></param>
        /// <param name="Turno"></param>
        public void CargaInicial(Framework.objetosNeon.Competencia comp, DateTime H1, DateTime H2, DateTime H3, DateTime H4, bool Turno) {
            CargaInicial(comp, H1, H2, H3, H4, Turno, DateTime.MinValue, DateTime.MinValue, DateTime.MinValue, DateTime.MinValue);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="comp"></param>
        /// <param name="H1"></param>
        /// <param name="H2"></param>
        /// <param name="H3"></param>
        /// <param name="H4"></param>
        /// <param name="Turno"></param>
        /// <param name="SH1"></param>
        /// <param name="SH2"></param>
        /// <param name="SH3"></param>
        /// <param name="SH4"></param>
        public void CargaInicial(Framework.objetosNeon.Competencia comp, DateTime H1, DateTime H2, DateTime H3, DateTime H4, bool Turno, DateTime SH1, DateTime SH2, DateTime SH3, DateTime SH4)
        {
            dias.Clear();
            Competencia compA = comp.CloneCompet(-1);
            DateTime di = new DateTime(compA.Ano, compA.Mes, 13);
            DateTime df = new DateTime(comp.Ano, comp.Mes, 12);
            //DateTime df = new DateTime(2009, 1, 20);
            string[] mes = new string[31];
            for (int k = 0; k < 31; k++)
                mes[k] = "";
            string MesSTR = di.ToString("MMMM").ToUpper();
            for (int j = 0; j < MesSTR.Length; j++)
                mes[j] = MesSTR.Substring(j, 1);
            int i = 0;
            for (DateTime d = di; d <= df; d = d.AddDays(1))
            {
                if (d.Day == 1)
                {
                    MesSTR = df.ToString("MMMM").ToUpper();
                    for (int j = 0; j < MesSTR.Length; j++)
                        mes[j + i] = MesSTR.Substring(j, 1);
                };
                dPlaPonto.diasRow Novarow = dias.NewdiasRow();
                Novarow.dia = d.Day;
                Novarow.semana = DiaSemana(d);
                Novarow.mes = mes[i++];
                Novarow.Feriado = false;
                Novarow.Abonar = false;
                if (d.DayOfWeek == DayOfWeek.Sunday)
                    Novarow.Folga = (!Turno);
                else
                    Novarow.Folga = false;
                Novarow.FaltaInj = true;
                Novarow.Erro = false;
                Novarow.Almoco = false;
                Novarow.AdicNot = false;
                if (!Novarow.Folga)
                {
                    if ((Turno) || (d.DayOfWeek != DayOfWeek.Saturday))
                    {
                        if (H1 != DateTime.MinValue)
                            Novarow.PONEntrada = H1;
                        if (H2 != DateTime.MinValue)
                            Novarow.PONAlmoco = H2;
                        if (H3 != DateTime.MinValue)
                            Novarow.PONAlmocoRet = H3;
                        if (H4 != DateTime.MinValue)
                            Novarow.PONSaida = H4;
                    }
                    else {
                        if ((SH1 == DateTime.MinValue) && (SH2 == DateTime.MinValue) && (SH3 == DateTime.MinValue) && (SH4 == DateTime.MinValue))
                            Novarow.Folga = true;
                        if (SH1 != DateTime.MinValue)
                            Novarow.PONEntrada = SH1;
                        if (SH2 != DateTime.MinValue)
                            Novarow.PONAlmoco = SH2;
                        if (SH3 != DateTime.MinValue)
                            Novarow.PONAlmocoRet = SH3;
                        if (SH4 != DateTime.MinValue)
                            Novarow.PONSaida = SH4;
                    }
                }
                dias.AdddiasRow(Novarow);
            };


        }
    }
}

