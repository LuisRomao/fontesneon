using System.Drawing;
using DevExpress.XtraReports.UI;

namespace DP.impressos
{
    /// <summary>
    /// 
    /// </summary>
    public partial class impSubPonto : DevExpress.XtraReports.UI.XtraReport
    {
        /// <summary>
        /// 
        /// </summary>
        public impSubPonto()
        {
            InitializeComponent();
        }

        private void xrTableCell11_BeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e)
        {
            //xrTableCell11.BackColor = Color.Aqua;
        }

        private void CantoRedondo(object sender, DrawEventArgs e)
        {
            dllImpresso.ImpGeradorDeEfeitos.CantosRedondos(sender, e, 30);
        }

        private void Detail_BeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e)
        {
            Color cor = Color.White;
            xrTableCell4.Text = "";
            xrTableCell4.Width = 171;
            if (CurrentRowIndex > 0)
                if (dPlaPonto.dias[CurrentRowIndex].Feriado)
                {
                    cor = Color.FromArgb(50, 50, 50);
                    xrTableCell4.Text = " F E R I A D O";
                    xrTableCell4.Width *= 4;
                }
                else
                    if (dPlaPonto.dias[CurrentRowIndex].semana == "sab")
                        cor = Color.FromArgb(220, 220, 220);
                    else
                        if (dPlaPonto.dias[CurrentRowIndex].semana == "dom")
                            cor = Color.FromArgb(200, 200, 200);
            xrTableCell4.BackColor = xrTableCell5.BackColor = xrTableCell6.BackColor = xrTableCell11.BackColor = cor;
        }

    }
}
