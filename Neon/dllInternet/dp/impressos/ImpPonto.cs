using System;
using System.Collections;
using DevExpress.XtraReports.UI;
using Framework.objetosNeon;

namespace DP.impressos
{
    /// <summary>
    /// 
    /// </summary>
    public partial class ImpPonto : DevExpress.XtraReports.UI.XtraReport
    {
        /// <summary>
        /// 
        /// </summary>
        public ImpPonto()
        {
            InitializeComponent();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="comp"></param>
        /// <param name="Feriados"></param>
        public void CargaInicial(Competencia comp,ArrayList Feriados){

            impSubPonto SubPonto = (impSubPonto)(xrSubreport1.ReportSource);
            ImpSubApontamentos1 SubApontamentos1 = (ImpSubApontamentos1)(xrSubreport2.ReportSource);
            Competencia compA = comp.CloneCompet(-1);            
            DateTime di = new DateTime(compA.Ano, compA.Mes, 13);
            DateTime df = new DateTime(comp.Ano, comp.Mes, 12);
            SubPonto.dPlaPonto.dias.Clear();
            //DateTime df = new DateTime(2009, 1, 20);
            string[] mes = new string[31];
            for (int k = 0; k < 31; k++)
                mes[k] = "";
            string MesSTR = di.ToString("MMMM").ToUpper();
            for (int j = 0; j < MesSTR.Length; j++)
                mes[j] = MesSTR.Substring(j,1); 
            int i = 0;
            for (DateTime d = di; d <= df; d = d.AddDays(1))
            {
                if (d.Day == 1)
                {
                    MesSTR = df.ToString("MMMM").ToUpper();
                    for (int j = 0; j < MesSTR.Length; j++)
                        mes[j+i] = MesSTR.Substring(j, 1);
                };
                dPlaPonto.diasRow Novarow = SubPonto.dPlaPonto.dias.NewdiasRow();
                Novarow.dia = d.Day;
                Novarow.semana = SubPonto.dPlaPonto.DiaSemana(d);
                Novarow.mes = mes[i++];
                Novarow.Feriado = Feriados.Contains(d);
                SubPonto.dPlaPonto.dias.AdddiasRow(Novarow);
            };



            if (SubApontamentos1.dApontamento.Apontamento.Rows.Count == 0)
            {
                SubApontamentos1.dApontamento.Apontamento.AddApontamentoRow(188, "Hora Almo�o");
                SubApontamentos1.dApontamento.Apontamento.AddApontamentoRow(173, "Nona Hora");
                SubApontamentos1.dApontamento.Apontamento.AddApontamentoRow(13, "Adc N 20%");
                SubApontamentos1.dApontamento.Apontamento.AddApontamentoRow(148, "H Extra 100%");
                SubApontamentos1.dApontamento.Apontamento.AddApontamentoRow(146, "H Extra 50%");



                SubApontamentos1.dApontamento.Apontamento.AddApontamentoRow(421, "F Injustificada");
                SubApontamentos1.dApontamento.Apontamento.AddApontamentoRow(422, "Hora n�o Trabalhada");
                SubApontamentos1.dApontamento.Apontamento.AddApontamentoRow(180, "Ac�mulo de Cargo");

                SubApontamentos1.dApontamento.Apontamento.AddApontamentoRow(181, "Reembolso Div");

                SubApontamentos1.dApontamento.Apontamento.AddApontamentoRow(176, "Assembl�ia");

                SubApontamentos1.dApontamento.Apontamento.AddApontamentoRow(543, "Farm�cia");
            };

        }

        private void CantosRedondos(object sender, DrawEventArgs e)
        {
            dllImpresso.ImpGeradorDeEfeitos.CantosRedondos(sender, e, 30);
        }

    }
}
