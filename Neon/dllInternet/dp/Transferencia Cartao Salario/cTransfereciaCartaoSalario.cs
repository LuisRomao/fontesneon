using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using System.Data.SqlClient;
using System.IO;
using Microsoft.VisualBasic;
using DP.Virtual;

namespace DP.Transferencia_Cartao_Salario
{
    public partial class cTransfereciaCartaoSalario : CompontesBasicos.ComponenteGradeNavegador
    {
        public cTransfereciaCartaoSalario()
        {
            InitializeComponent();
        }

        private void cTransfereciaCartaoSalario_Load(object sender, EventArgs e)
        {
            this.cONDOMINIOSTableAdapter.Fill(this.dTransferenciaCartaoSalario.CONDOMINIOS);
        }

        protected override void BtnExcluir_F_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            if (DialogResult.Yes == MessageBox.Show("Confirma a exclus�o do registro?", "Excluir", MessageBoxButtons.YesNo, MessageBoxIcon.Question))
                this.GridView_F.DeleteRow(this.GridView_F.FocusedRowHandle);
        }

        protected override void GridView_F_ValidateRow(object sender, DevExpress.XtraGrid.Views.Base.ValidateRowEventArgs e)
        {
            base.GridView_F_ValidateRow(sender, e);

            if (this.GridView_F.GetFocusedRowCellDisplayText(colANTIGO).Trim() == this.GridView_F.GetFocusedRowCellDisplayText(colNOVO).Trim())
                this.GridView_F.SetColumnError(colNOVO, "O n�mero do novo cart�o n�o pode ser igual ao n�mero do antigo cart�o.");

            e.Valid = (!this.GridView_F.HasColumnErrors);
        }

        private void btnGerarArquivo_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            if (this.BindingSource_F.Count > 0)
            {
                //Verificando existencia do diretorio para gravacao e gerando nome do arquivo remessa
                #if (DEBUG)
                String _DirGravacao = @"c:\lixo\";
                
                #else
                String _DirGravacao=@"J:\Virtual\Cartao Salario\"+ DateTime.Now.ToString("MMMyy") + "\\";// = Properties.Settings.Default.DiretorioGravacaoRemessa;
                #endif
                
                if (!Directory.Exists(_DirGravacao))
                {
                    try
                    {
                        Directory.CreateDirectory(_DirGravacao);
                        return;
                    }
                    catch
                    {
                        MessageBox.Show("A pasta para guardar os arquivos de remessa gerados n�o foi encontrada.\r\nVerifique se a pasta " + _DirGravacao + " existe e tente gerar novamente.", "Arquivo de Remessa", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                        return;
                    }
                }

                String _FileCC = "";

                for (Int32 _IdArquivo = 1; _IdArquivo <= 10; _IdArquivo++)
                {
                    if (_IdArquivo == 10)
                    {
                        MessageBox.Show("O n�mero m�ximo(9) para gera��o de arquivo remessa j� foi atingindo.\r\nProcesso cancelado.", "Arquivo de Remessa", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                        return;
                    }
                    else
                    {
                        //_FileCC = _DirGravacao + "SA" + DateTime.Today.ToString("ddMM") + "0" + _IdArquivo.ToString() + ".REM";

                        int numeroenvio = 0;
                        do
                        {
                            numeroenvio++;
                            _FileCC = _DirGravacao +
                                         "SA" +
                                         DateTime.Today.ToString("ddMM") +
                                         numeroenvio.ToString("00") +
                                         ".REM";
                        }
                        while (File.Exists(_FileCC));


                        if (File.Exists(_FileCC))
                        {
                            if (DialogResult.Yes == MessageBox.Show("O arquivo " + _FileCC + " j� existe. Deseja substituir o arquivo?", "Arquivo Remessa", MessageBoxButtons.YesNo, MessageBoxIcon.Question))
                            {
                                try
                                {
                                    File.Delete(_FileCC);
                                }
                                catch
                                {
                                    MessageBox.Show("O arquivo " + _FileCC + " j� existe e n�o foi poss�vel sobrescreve-lo.", "Arquivo de Remessa", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                                    return;
                                }
                            }
                        }
                        else
                            break;
                    }
                }

                //Gerando dados do arquivo remessa
                this.GridView_F.ClearSorting();
                this.colCONCodigo.SortOrder = DevExpress.Data.ColumnSortOrder.Ascending;

                using (StreamWriter _DataCC = new StreamWriter(_FileCC))
                {
                    String _CODCON = "";

                    Decimal _VrTotal = 0;

                    Int32 _SeqRegistro = 0;

                    for (Int32 _Pos = 0; _Pos < this.BindingSource_F.Count; _Pos++)
                    {
                        this.BindingSource_F.Position = _Pos;

                        DataRowView _drvCARTAO = ((DataRowView)(this.BindingSource_F.Current));

                        if (_CODCON != ((DataRowView)(BindingSource_F.Current))["CONCodigo"].ToString())
                        {
                            //Variavel de Controle
                            _CODCON = ((DataRowView)(BindingSource_F.Current))["CONCodigo"].ToString();

                            //Posicionando no registro do condominio
                            Int32 _PosicaoReg = this.cONDOMINIOSBindingSource.Find("CONCodigo", _CODCON);

                            if (_PosicaoReg < 0)
                            {
                                MessageBox.Show("N�o foi poss�vel achar o registro do condom�nio " + _CODCON + ".", "Registro do Condom�nio", MessageBoxButtons.OK, MessageBoxIcon.Error);
                                return;
                            }
                            else
                                this.cONDOMINIOSBindingSource.Position = _PosicaoReg;

                            DataRowView DRV = ((DataRowView)(this.cONDOMINIOSBindingSource.Current));
                            dTransferenciaCartaoSalario.CONDOMINIOSRow _drvCON = (dTransferenciaCartaoSalario.CONDOMINIOSRow)DRV.Row; 
                            /*** Registro HEADER ***/
                            _SeqRegistro = 1;

                            String _IdHeader = "0";

                            //Codigo Fornecido pelo banco
                            Object _CodBancoCS = _drvCON["CONCodigoBancoCS"];

                            if (_CodBancoCS == null)
                            {
                                MessageBox.Show("O par�metro C�DIGO FORNECIDO PELO BANCO PARA CONTA SAL�RIO n�o foi informado para o condom�nio " + ((DataRowView)(BindingSource_F.Current))["CONCodigo"] + ".\r\nInforme o par�metro e tente gerar o arquivo novamente.", "Arquivo de Remessa", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                                _DataCC.Close();
                                return;
                            }
                            else
                                _CodBancoCS = FuncoesDP.CampoNumerico(_CodBancoCS, 8);

                            String _TipoInscricao = "2";

                            String _CNPJAux = FuncoesDP.CampoNumerico(_drvCON["CONCnpj"].ToString(), 15);

                            if (_CNPJAux.Trim() == "000000000000000")
                            {
                                MessageBox.Show("O campo CNPJ do condom�nio " + _CODCON + " n�o foi informado ou est� incorreto.\r\nVerifique o valor do campo e tente gerar o arquivo novamente.", "Arquivo de Remessa", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                                _DataCC.Close();
                                return;
                            }

                            String _CNPJBase = FuncoesDP.CampoNumerico(_CNPJAux.Substring(0, 9), 9);
                            String _CNPJFilial = FuncoesDP.CampoNumerico(_CNPJAux.Substring(9, 4), 4);
                            String _CNPJControle = FuncoesDP.CampoNumerico(_CNPJAux.Substring(13, 2), 2);
                            String _RazaoSocial = FuncoesDP.CampoAlfaNumerico(_drvCON["CONNome"].ToString(), 40);
                            String _IdServico = "70";
                            String _OrigemArquivo = "1";
                            String _RazaoCS = "00002";
                            String _ReservaH1 = FuncoesDP.CampoAlfaNumerico(" ", 5);
                            String _DataGravacao = DateTime.Now.ToString("yyyyMMdd");
                            String _HoraGravacao = DateTime.Now.ToString("hhmmss");
                            String _ReservaH2 = FuncoesDP.CampoAlfaNumerico(" ", 5);
                            String _ReservaH3 = FuncoesDP.CampoAlfaNumerico(" ", 3);
                            String _ReservaH4 = FuncoesDP.CampoAlfaNumerico(" ", 80);
                            String _ReservaH5 = FuncoesDP.CampoAlfaNumerico(" ", 88);
                            String _AgenciaDeb = FuncoesDP.CampoNumerico(_drvCON.CONAgencia, 5);
                            String _ContaDeb = FuncoesDP.CampoNumerico(_drvCON.CONConta, 7);
                            String _dContaDeb = FuncoesDP.CampoNumerico(_drvCON.CONDigitoConta, 1);
                            String _ReservaH6 = FuncoesDP.CampoAlfaNumerico(" ", 194);
                            String _ReservaH7 = FuncoesDP.CampoAlfaNumerico(" ", 19);
                            String _SeqHeader = FuncoesDP.CampoNumerico(_SeqRegistro, 6);

                            _DataCC.WriteLine(
                                                _IdHeader +
                                                _CodBancoCS +
                                                _TipoInscricao +
                                                _CNPJBase +
                                                _CNPJFilial +
                                                _CNPJControle +
                                                _RazaoSocial +
                                                _IdServico +
                                                _OrigemArquivo +
                                                _RazaoCS +
                                                _ReservaH1 +
                                                _DataGravacao +
                                                _HoraGravacao +
                                                _ReservaH2 +
                                                _ReservaH3 +
                                                _ReservaH4 +
                                                _ReservaH5 +
                                                _AgenciaDeb +
                                                _ContaDeb +
                                                _dContaDeb +
                                                _ReservaH6 +
                                                _ReservaH7 +
                                                _SeqHeader
                                             );
                        }

                        /*** Registro Transa��o ***/
                        /*** Primeiro registro de transacao � extraido da linha atual ( Mesma linha do HEADER)*/
                        _SeqRegistro++;

                        String _IdTransacao = "1";
                        String _ReservaT1 = FuncoesDP.CampoAlfaNumerico(" ", 277);
                        String _ReservaT2 = FuncoesDP.CampoAlfaNumerico(" ", 02);
                        String _ReservaT3 = FuncoesDP.CampoAlfaNumerico(" ", 02);
                        String _ReservaT4 = FuncoesDP.CampoAlfaNumerico(" ", 02);
                        String _ReservaT5 = FuncoesDP.CampoAlfaNumerico(" ", 02);
                        String _ReservaT6 = FuncoesDP.CampoAlfaNumerico(" ", 02);
                        String _TipoMotivo = "3"; //Transferencia de Cartao
                        String _NomeFunc = FuncoesDP.CampoAlfaNumerico(_drvCARTAO["Nome"].ToString(), 26);
                        DocBacarios.CPFCNPJ cpf = new DocBacarios.CPFCNPJ(_drvCARTAO["CPF"].ToString());
                        if (cpf.Tipo == DocBacarios.TipoCpfCnpj.INVALIDO)
                        {
                            MessageBox.Show("CPF invalido");
                            return;
                        };
                        String _CPF = cpf.ToString(false);
                        _CPF = _CPF.Substring(0, 9) + "0000" + _CPF.Substring(9, 2);
                        
                        String _ReservaT7 = FuncoesDP.CampoAlfaNumerico(_CPF, 32);

                        String _ReservaT8 = FuncoesDP.CampoAlfaNumerico(" ", 02);
                        String _ReservaT9 = FuncoesDP.CampoAlfaNumerico(" ", 15);
                        String _NumPagto = FuncoesDP.CampoNumerico(0, 7);//alterado de 1 para 0
                        String _DataPagto = FuncoesDP.CampoNumerico(0, 8);
                        String _NumCartao = FuncoesDP.CampoNumerico(Convert.ToInt32(_drvCARTAO["ANTIGO"]), 7);
                        String _ValorCred = FuncoesDP.CampoNumerico(0, 15);
                        String _ReservaT10 = FuncoesDP.CampoNumerico(0, 15);
                        String _ReservaT11 = FuncoesDP.CampoAlfaNumerico(" ", 8);
                        String _ReservaT12 = FuncoesDP.CampoAlfaNumerico(" ", 8);
                        String _NumNovoCartao = FuncoesDP.CampoNumerico(Convert.ToInt32(_drvCARTAO["NOVO"]), 7);
                        String _ReservaT13 = FuncoesDP.CampoAlfaNumerico(" ", 36);
                        String _ReservaT14 = FuncoesDP.CampoAlfaNumerico(" ", 19);
                        String _SeqTransacao = FuncoesDP.CampoNumerico(_SeqRegistro, 6);

                        _DataCC.WriteLine(
                                            _IdTransacao +
                                            _ReservaT1 +
                                            _ReservaT2 +
                                            _ReservaT3 +
                                            _ReservaT4 +
                                            _ReservaT5 +
                                            _ReservaT6 +
                                            _TipoMotivo +
                                            _NomeFunc +
                                            _ReservaT7 +
                                            _ReservaT8 +
                                            _ReservaT9 +
                                            _NumPagto +
                                            _DataPagto +
                                            _NumCartao +
                                            _ValorCred +
                                            _ReservaT10 +
                                            _ReservaT11 +
                                            _ReservaT12 +
                                            _NumNovoCartao +
                                            _ReservaT13 +
                                            _ReservaT14 +
                                            _SeqTransacao
                                         );

                        /*** Registro TRAILER ***/
                        Boolean _PrintTrailer = false;

                        if (_Pos == (this.BindingSource_F.Count - 1))
                            _PrintTrailer = true;
                        else
                        {
                            this.BindingSource_F.Position++;

                            DataRowView _D = ((DataRowView)(BindingSource_F.Current));

                            if (_CODCON != _D["CONCodigo"].ToString())
                                _PrintTrailer = true;

                            this.BindingSource_F.Position--;
                        }

                        if (_PrintTrailer)
                        {
                            _SeqRegistro++;

                            String _IdTrailer = "9";
                            String _QtdeRegistro = FuncoesDP.CampoNumerico(_SeqRegistro, 6);
                            String _ValorTotal = FuncoesDP.CampoNumerico(_VrTotal, 17);
                            String _ReservaTr1 = FuncoesDP.CampoAlfaNumerico(" ", 451);
                            String _ReservaTr2 = FuncoesDP.CampoAlfaNumerico(" ", 19);
                            String _SeqTrailer = FuncoesDP.CampoNumerico(_SeqRegistro, 6);

                            _DataCC.WriteLine(
                                                _IdTrailer +
                                                _QtdeRegistro +
                                                _ValorTotal +
                                                _ReservaTr1 +
                                                _ReservaTr2 +
                                                _SeqTrailer
                                             );
                        }
                    }

                    _DataCC.Flush();
                    _DataCC.Close();
                    MessageBox.Show("Gerado arquivo:" + _FileCC);
                }
            }
        }


        protected override void GridView_F_RowCountChanged(object sender, EventArgs e)
        {
            base.GridView_F_RowCountChanged(sender, e);

            this.btnGerarArquivo.Enabled = (this.GridView_F.DataRowCount > 0);
        }
    }
}

