namespace DP.Transferencia_Cartao_Salario
{
    partial class cTransfereciaCartaoSalario
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(cTransfereciaCartaoSalario));
            this.dTransferenciaCartaoSalario = new DP.Transferencia_Cartao_Salario.dTransferenciaCartaoSalario();
            this.colCONCodigo = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemLookUpEdit1 = new DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit();
            this.cONDOMINIOSBindingSource = new System.Windows.Forms.BindingSource();
            this.colNOME = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colCPF = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemTextEdit1 = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.colANTIGO = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colNOVO = new DevExpress.XtraGrid.Columns.GridColumn();
            this.cONDOMINIOSTableAdapter = new DP.Transferencia_Cartao_Salario.dTransferenciaCartaoSalarioTableAdapters.CONDOMINIOSTableAdapter();
            this.btnGerarArquivo = new DevExpress.XtraBars.BarButtonItem();
            ((System.ComponentModel.ISupportInitialize)(this.PopImprimir_F)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.BarManager_F)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.PopExportar_F)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.GridControl_F)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.GridView_F)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.BindingSource_F)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.StyleController_F)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dTransferenciaCartaoSalario)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemLookUpEdit1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cONDOMINIOSBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEdit1)).BeginInit();
            this.SuspendLayout();
            // 
            // barNavegacao_F
            // 
            this.barNavegacao_F.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(DevExpress.XtraBars.BarLinkUserDefines.PaintStyle, this.btnGerarArquivo, "", true, true, true, 0, null, DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph)});
            this.barNavegacao_F.OptionsBar.AllowQuickCustomization = false;
            this.barNavegacao_F.OptionsBar.DisableClose = true;
            this.barNavegacao_F.OptionsBar.DisableCustomization = true;
            this.barNavegacao_F.OptionsBar.DrawDragBorder = false;
            this.barNavegacao_F.OptionsBar.DrawSizeGrip = true;
            this.barNavegacao_F.OptionsBar.RotateWhenVertical = false;
            this.barNavegacao_F.OptionsBar.UseWholeRow = true;
            // 
            // BarManager_F
            // 
            this.BarManager_F.Items.AddRange(new DevExpress.XtraBars.BarItem[] {
            this.btnGerarArquivo});
            // 
            // BtnAlterar_F
            // 
            this.BtnAlterar_F.Enabled = false;
            // 
            // BtnExcluir_F
            // 
            this.BtnExcluir_F.Enabled = false;
            // 
            // BtnImprimir_F
            // 
            this.BtnImprimir_F.Enabled = false;
            // 
            // BtnExportar_F
            // 
            this.BtnExportar_F.Enabled = false;
            // 
            // barDataBase_F
            // 
            this.barDataBase_F.OptionsBar.AllowQuickCustomization = false;
            this.barDataBase_F.OptionsBar.DisableClose = true;
            this.barDataBase_F.OptionsBar.DisableCustomization = true;
            this.barDataBase_F.OptionsBar.DrawDragBorder = false;
            this.barDataBase_F.OptionsBar.UseWholeRow = true;
            // 
            // GridControl_F
            // 
            this.GridControl_F.EmbeddedNavigator.Buttons.Append.Visible = false;
            this.GridControl_F.EmbeddedNavigator.Buttons.CancelEdit.Visible = false;
            this.GridControl_F.EmbeddedNavigator.Buttons.Edit.Visible = false;
            this.GridControl_F.EmbeddedNavigator.Buttons.EndEdit.Visible = false;
            this.GridControl_F.EmbeddedNavigator.Buttons.Remove.Visible = false;
            this.GridControl_F.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemLookUpEdit1,
            this.repositoryItemTextEdit1});
            this.GridControl_F.Size = new System.Drawing.Size(1247, 683);
            // 
            // GridView_F
            // 
            this.GridView_F.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colCONCodigo,
            this.colNOME,
            this.colCPF,
            this.colANTIGO,
            this.colNOVO});
            this.GridView_F.NewItemRowText = "Adicionar um novo registro";
            this.GridView_F.OptionsBehavior.AllowIncrementalSearch = true;
            this.GridView_F.OptionsBehavior.Editable = false;
            this.GridView_F.OptionsCustomization.AllowFilter = false;
            this.GridView_F.OptionsLayout.Columns.StoreAllOptions = true;
            this.GridView_F.OptionsLayout.Columns.StoreAppearance = true;
            this.GridView_F.OptionsLayout.StoreAllOptions = true;
            this.GridView_F.OptionsLayout.StoreAppearance = true;
            this.GridView_F.OptionsMenu.EnableGroupPanelMenu = false;
            this.GridView_F.OptionsNavigation.AutoFocusNewRow = true;
            this.GridView_F.OptionsNavigation.EnterMoveNextColumn = true;
            this.GridView_F.OptionsPrint.EnableAppearanceEvenRow = true;
            this.GridView_F.OptionsPrint.EnableAppearanceOddRow = true;
            this.GridView_F.OptionsPrint.ExpandAllDetails = true;
            this.GridView_F.OptionsView.NewItemRowPosition = DevExpress.XtraGrid.Views.Grid.NewItemRowPosition.Bottom;
            this.GridView_F.OptionsView.ShowFooter = true;
            this.GridView_F.OptionsView.ShowGroupPanel = false;
            // 
            // BindingSource_F
            // 
            this.BindingSource_F.DataMember = "CARTAOSALARIO";
            this.BindingSource_F.DataSource = this.dTransferenciaCartaoSalario;
            // 
            // ToolTipController_F
            // 
            this.ToolTipController_F.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(222)))), ((int)(((byte)(237)))), ((int)(((byte)(250)))));
            this.ToolTipController_F.Appearance.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(20)))), ((int)(((byte)(69)))), ((int)(((byte)(141)))));
            this.ToolTipController_F.Appearance.Options.UseBackColor = true;
            this.ToolTipController_F.Appearance.Options.UseFont = true;
            this.ToolTipController_F.Appearance.Options.UseForeColor = true;
            this.ToolTipController_F.Appearance.Options.UseTextOptions = true;
            this.ToolTipController_F.Appearance.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            // 
            // StyleController_F
            // 
            this.StyleController_F.AppearanceDisabled.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this.StyleController_F.AppearanceDisabled.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this.StyleController_F.AppearanceDisabled.Options.UseBackColor = true;
            this.StyleController_F.AppearanceDisabled.Options.UseForeColor = true;
            this.StyleController_F.AppearanceFocused.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.StyleController_F.AppearanceFocused.Options.UseBackColor = true;
            // 
            // spellChecker1
            // 
            this.spellChecker1.OptionsSpelling.CheckSelectedTextFirst = DevExpress.Utils.DefaultBoolean.True;
            this.spellChecker1.OptionsSpelling.IgnoreEmails = DevExpress.Utils.DefaultBoolean.True;
            this.spellChecker1.OptionsSpelling.IgnoreMixedCaseWords = DevExpress.Utils.DefaultBoolean.False;
            this.spellChecker1.OptionsSpelling.IgnoreRepeatedWords = DevExpress.Utils.DefaultBoolean.False;
            this.spellChecker1.OptionsSpelling.IgnoreUpperCaseWords = DevExpress.Utils.DefaultBoolean.False;
            this.spellChecker1.OptionsSpelling.IgnoreUrls = DevExpress.Utils.DefaultBoolean.True;
            this.spellChecker1.OptionsSpelling.IgnoreWordsWithNumbers = DevExpress.Utils.DefaultBoolean.True;
            // 
            // dTransferenciaCartaoSalario
            // 
            this.dTransferenciaCartaoSalario.DataSetName = "dTransferenciaCartaoSalario";
            this.dTransferenciaCartaoSalario.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // colCONCodigo
            // 
            this.colCONCodigo.Caption = "Condom�nio";
            this.colCONCodigo.ColumnEdit = this.repositoryItemLookUpEdit1;
            this.colCONCodigo.FieldName = "CONCodigo";
            this.colCONCodigo.Name = "colCONCodigo";
            this.colCONCodigo.Visible = true;
            this.colCONCodigo.VisibleIndex = 0;
            this.colCONCodigo.Width = 154;
            // 
            // repositoryItemLookUpEdit1
            // 
            this.repositoryItemLookUpEdit1.AutoHeight = false;
            this.repositoryItemLookUpEdit1.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemLookUpEdit1.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("CONCodigo", "CODCON", 61, DevExpress.Utils.FormatType.None, "", true, DevExpress.Utils.HorzAlignment.Near),
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("CONNome", "Nome", 55, DevExpress.Utils.FormatType.None, "", true, DevExpress.Utils.HorzAlignment.Near)});
            this.repositoryItemLookUpEdit1.DataSource = this.cONDOMINIOSBindingSource;
            this.repositoryItemLookUpEdit1.DisplayMember = "Condominio";
            this.repositoryItemLookUpEdit1.Name = "repositoryItemLookUpEdit1";
            this.repositoryItemLookUpEdit1.NullText = "";
            this.repositoryItemLookUpEdit1.ValueMember = "CONCodigo";
            // 
            // cONDOMINIOSBindingSource
            // 
            this.cONDOMINIOSBindingSource.DataMember = "CONDOMINIOS";
            this.cONDOMINIOSBindingSource.DataSource = this.dTransferenciaCartaoSalario;
            // 
            // colNOME
            // 
            this.colNOME.Caption = "Nome";
            this.colNOME.FieldName = "NOME";
            this.colNOME.Name = "colNOME";
            this.colNOME.Visible = true;
            this.colNOME.VisibleIndex = 1;
            this.colNOME.Width = 286;
            // 
            // colCPF
            // 
            this.colCPF.Caption = "CPF";
            this.colCPF.ColumnEdit = this.repositoryItemTextEdit1;
            this.colCPF.FieldName = "CPF";
            this.colCPF.Name = "colCPF";
            this.colCPF.Visible = true;
            this.colCPF.VisibleIndex = 2;
            this.colCPF.Width = 133;
            // 
            // repositoryItemTextEdit1
            // 
            this.repositoryItemTextEdit1.AutoHeight = false;
            this.repositoryItemTextEdit1.Mask.EditMask = "000.000.000-00";
            this.repositoryItemTextEdit1.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Simple;
            this.repositoryItemTextEdit1.Mask.PlaceHolder = ' ';
            this.repositoryItemTextEdit1.Name = "repositoryItemTextEdit1";
            // 
            // colANTIGO
            // 
            this.colANTIGO.Caption = "Cart�o Antigo";
            this.colANTIGO.FieldName = "ANTIGO";
            this.colANTIGO.Name = "colANTIGO";
            this.colANTIGO.Visible = true;
            this.colANTIGO.VisibleIndex = 3;
            this.colANTIGO.Width = 88;
            // 
            // colNOVO
            // 
            this.colNOVO.Caption = "Cart�o Novo";
            this.colNOVO.FieldName = "NOVO";
            this.colNOVO.Name = "colNOVO";
            this.colNOVO.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Count)});
            this.colNOVO.Visible = true;
            this.colNOVO.VisibleIndex = 4;
            this.colNOVO.Width = 106;
            // 
            // cONDOMINIOSTableAdapter
            // 
            this.cONDOMINIOSTableAdapter.ClearBeforeFill = true;
            this.cONDOMINIOSTableAdapter.GetNovosDados = ((System.Collections.ArrayList)(resources.GetObject("cONDOMINIOSTableAdapter.GetNovosDados")));
            this.cONDOMINIOSTableAdapter.TabelaDataTable = null;
            // 
            // btnGerarArquivo
            // 
            this.btnGerarArquivo.Caption = "Gerar Arquivo";
            this.btnGerarArquivo.CategoryGuid = new System.Guid("70b162a1-0e6a-4dea-87b4-fc5b71c53e05");
            this.btnGerarArquivo.Glyph = ((System.Drawing.Image)(resources.GetObject("btnGerarArquivo.Glyph")));
            this.btnGerarArquivo.Id = 18;
            this.btnGerarArquivo.Name = "btnGerarArquivo";
            this.btnGerarArquivo.PaintStyle = DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph;
            this.btnGerarArquivo.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.btnGerarArquivo_ItemClick);
            // 
            // cTransfereciaCartaoSalario
            // 
            this.Autofill = false;
            this.LookAndFeel.UseDefaultLookAndFeel = false;
            this.Name = "cTransfereciaCartaoSalario";
            this.Size = new System.Drawing.Size(1247, 743);
            this.Load += new System.EventHandler(this.cTransfereciaCartaoSalario_Load);
            ((System.ComponentModel.ISupportInitialize)(this.PopImprimir_F)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.BarManager_F)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.PopExportar_F)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.GridControl_F)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.GridView_F)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.BindingSource_F)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.StyleController_F)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dTransferenciaCartaoSalario)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemLookUpEdit1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cONDOMINIOSBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEdit1)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraGrid.Columns.GridColumn colCONCodigo;
        private DevExpress.XtraGrid.Columns.GridColumn colNOME;
        private DevExpress.XtraGrid.Columns.GridColumn colCPF;
        private DevExpress.XtraGrid.Columns.GridColumn colANTIGO;
        private DevExpress.XtraGrid.Columns.GridColumn colNOVO;
        private dTransferenciaCartaoSalario dTransferenciaCartaoSalario;
        private DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit repositoryItemLookUpEdit1;
        private System.Windows.Forms.BindingSource cONDOMINIOSBindingSource;
        private DP.Transferencia_Cartao_Salario.dTransferenciaCartaoSalarioTableAdapters.CONDOMINIOSTableAdapter cONDOMINIOSTableAdapter;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit repositoryItemTextEdit1;
        protected DevExpress.XtraBars.BarButtonItem btnGerarArquivo;
    }
}
