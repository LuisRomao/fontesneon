using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using System.IO;
using DevExpress.XtraTreeList.Nodes;

namespace DP
{
    public partial class cArquivoRemessaCredito : CompontesBasicos.ComponenteBaseBindingSource
    {
        public cArquivoRemessaCredito()
        {
            InitializeComponent();
        }

        String path = @"C:\Virtual\DP\Folha\";

        private void CarregarArquivos()
        {
            if (!Directory.Exists(path))
            {
                try
                {
                Directory.CreateDirectory(path);
                }
                catch(Exception Erro)
                {
                    MessageBox.Show("N�o foi poss�vel encontrar o diret�rio C:\\VIRTUAL\\DP\\FOLHA, o qual deve conter os arquivos de cr�dito gerado pelo programa da folha.\r\n"+
                                    "Tente criar o diret�rio manualmente pelo windows explorer ou entre em contato com a Virtual Software para maiores esclarecimentos.\r\n\r\n"+
                                    "Foi encontrado o seguinte erro ao tentar criar o diret�rio: " + Erro.Message.ToUpper(), 
                                    "Carregar Arquivo de Cr�dito", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    return;
                }
            }

            TreeListNode node;
            FileInfo fi;
            try
            {
                string[] root = Directory.GetFiles(path);
                foreach (string s in root)
                {
                    fi = new FileInfo(s);

                    if (fi.Extension.ToUpper().IndexOf("RTF") >= 0)
                    {
                        node = FileTreeList.AppendNode(new object[] { fi.Name, (fi.Length / 1024) + " KB", fi.CreationTime.ToShortDateString() + " " + fi.CreationTime.ToShortTimeString() }, null);
                        node.HasChildren = false;
                    }
                }
            }
            catch { }
        }

        private void cArquivoRemessaCredito_Load(object sender, EventArgs e)
        {
            this.Dock = DockStyle.Fill;
            CarregarArquivos();
        }

        private void treeList1_GetSelectImage(object sender, DevExpress.XtraTreeList.GetSelectImageEventArgs e)
        {
            e.NodeImageIndex = 0;
        }

        private void FileTreeList_FocusedNodeChanged(object sender, DevExpress.XtraTreeList.FocusedNodeChangedEventArgs e)
        {
            String _File = path +  e.Node.GetDisplayText("Nome").ToString();

            if (File.Exists(_File))
            {
                RichTextFile.Clear();
                RichTextFile.LoadFile(_File);

                String _TxtFile = DateTime.Today.ToShortTimeString() + DateTime.Today.ToShortDateString();

                RichTextFile.SaveFile(path + _TxtFile + ".txt", RichTextBoxStreamType.PlainText);
            }
        }
    }
}

