﻿namespace WSSincBancos
{


    partial class dBAL
    {
        public int EMP = 4;

        internal void AjustaStringDeConexao(System.Data.OleDb.OleDbConnection Connection)
        {
            if (EMP != 1)
                Connection.ConnectionString = Connection.ConnectionString.Replace("BAL1", string.Format("BAL{0}", (EMP == 3) ? 2 : EMP));
        }

        private dBALTableAdapters.BalanceteSaldosTableAdapter balanceteSaldosTableAdapter;
        /// <summary>
        /// TableAdapter padrão para tabela: BalanceteSaldos
        /// </summary>
        public dBALTableAdapters.BalanceteSaldosTableAdapter BalanceteSaldosTableAdapter
        {
            get
            {
                if (balanceteSaldosTableAdapter == null)
                {
                    balanceteSaldosTableAdapter = new dBALTableAdapters.BalanceteSaldosTableAdapter();

                };
                return balanceteSaldosTableAdapter;
            }
        }

        private dBALTableAdapters.BalanceteDetTableAdapter det1TableAdapter;
        /// <summary>
        /// TableAdapter padrão para tabela: Det1
        /// </summary>
        public dBALTableAdapters.BalanceteDetTableAdapter Det1TableAdapter
        {
            get
            {
                if (det1TableAdapter == null)
                    det1TableAdapter = new dBALTableAdapters.BalanceteDetTableAdapter();
                return det1TableAdapter;
            }
        }

        private dBALTableAdapters.BalanceteTotaisTableAdapter totaisTableAdapter;
        /// <summary>
        /// TableAdapter padrão para tabela: Totais
        /// </summary>
        public dBALTableAdapters.BalanceteTotaisTableAdapter TotaisTableAdapter
        {
            get
            {
                if (totaisTableAdapter == null)
                    totaisTableAdapter = new dBALTableAdapters.BalanceteTotaisTableAdapter();
                return totaisTableAdapter;
            }
        }
    }
}
