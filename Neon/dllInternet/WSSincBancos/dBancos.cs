﻿namespace WSSincBancos {


    partial class dBancos
    {
        public int EMP;

        internal void AjustaStringDeConexao(System.Data.OleDb.OleDbConnection Connection)
        {            
            if (EMP != 1)
                Connection.ConnectionString = Connection.ConnectionString.Replace("NEON1", string.Format("NEON{0}",(EMP == 3) ? 2 : EMP));                     
        }

        private dBancosTableAdapters.indicesTableAdapter indicesTableAdapter;
        /// <summary>
        /// TableAdapter padrão para tabela: Indices
        /// </summary>
        public dBancosTableAdapters.indicesTableAdapter IndicesTableAdapter
        {
            get
            {
                if (indicesTableAdapter == null)
                {
                    indicesTableAdapter = new dBancosTableAdapters.indicesTableAdapter();
                    AjustaStringDeConexao(indicesTableAdapter.Connection);                    
                };
                return indicesTableAdapter;
            }
        }

        private dBancosTableAdapters.condominiosTableAdapter condominiosTableAdapter;
        /// <summary>
        /// TableAdapter padrão para tabela: Condominios
        /// </summary>
        public dBancosTableAdapters.condominiosTableAdapter CondominiosTableAdapter
        {
            get
            {
                if (condominiosTableAdapter == null)
                {
                    condominiosTableAdapter = new dBancosTableAdapters.condominiosTableAdapter();
                    AjustaStringDeConexao(condominiosTableAdapter.Connection);
                };
                return condominiosTableAdapter;
            }
        }

        private dBancosTableAdapters.clientesTableAdapter clientesTableAdapter;
        /// <summary>
        /// TableAdapter padrão para tabela: Clientes
        /// </summary>
        public dBancosTableAdapters.clientesTableAdapter ClientesTableAdapter
        {
            get
            {
                if (clientesTableAdapter == null)
                {
                    clientesTableAdapter = new dBancosTableAdapters.clientesTableAdapter();
                    AjustaStringDeConexao(clientesTableAdapter.Connection);
                };
                return clientesTableAdapter;
            }
        }

        private dBancosTableAdapters.DividasTableAdapter dividasTableAdapter;
        /// <summary>
        /// TableAdapter padrão para tabela: Dividas
        /// </summary>
        public dBancosTableAdapters.DividasTableAdapter DividasTableAdapter
        {
            get
            {
                if (dividasTableAdapter == null)
                {
                    dividasTableAdapter = new dBancosTableAdapters.DividasTableAdapter();
                    AjustaStringDeConexao(dividasTableAdapter.Connection);
                };
                return dividasTableAdapter;
            }
        }

        private dBancosTableAdapters.novasTableAdapter novasTableAdapter;
        /// <summary>
        /// TableAdapter padrão para tabela: Novas
        /// </summary>
        public dBancosTableAdapters.novasTableAdapter NovasTableAdapter
        {
            get
            {
                if (novasTableAdapter == null)
                {
                    novasTableAdapter = new dBancosTableAdapters.novasTableAdapter();
                    AjustaStringDeConexao(novasTableAdapter.Connection);
                };
                return novasTableAdapter;
            }
        }

        private dBancosTableAdapters.BoletosTableAdapter boletosTableAdapter;
        /// <summary>
        /// TableAdapter padrão para tabela: Boletos
        /// </summary>
        public dBancosTableAdapters.BoletosTableAdapter BoletosTableAdapter
        {
            get
            {
                if (boletosTableAdapter == null)
                {
                    boletosTableAdapter = new dBancosTableAdapters.BoletosTableAdapter();
                    AjustaStringDeConexao(boletosTableAdapter.Connection);
                };
                return boletosTableAdapter;
            }
        }

    }
}
