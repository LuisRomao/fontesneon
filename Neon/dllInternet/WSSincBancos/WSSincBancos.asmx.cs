using System;
using System.Data;
using System.Web;
using System.Collections;
using System.Web.Services;
using System.Web.Services.Protocols;
using System.ComponentModel;
using VirCrip;
using System.Configuration;
using System.IO;

namespace WSSincBancos
{
    /// <summary>
    /// Summary description for WSSincBancos
    /// </summary>
    [WebService(Namespace = "http://neonimoveis.com.br/WSSincBancos")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [ToolboxItem(false)]
    public class WSSincBancos : System.Web.Services.WebService
    {
        private static System.Collections.ArrayList _Usados;

        private static System.Collections.ArrayList Usados
        { get { return _Usados ?? (_Usados = new ArrayList()); } }

        /*
        private dBancos _dBanco;        

        private dBancos dBanco
        {
            get 
            {
                if (_dBanco == null)
                {
                    _dBanco = new dBancos();                    
                }
                return _dBanco;
            }
        }*/

        private dBAL _dBal;

        private dBAL dBal 
        {
            get{return _dBal ?? (_dBal = new dBAL()) ;}
        }

        private string RetornodeErro(Exception e) 
        {
            string Retorno = string.Format("ERRO {0:dd/MM/yyyy HH:mm:ss}",DateTime.Now);
            while (e != null)
            {
                Retorno += string.Format("\n{0}\n{1}", e.Message, e.StackTrace);
                e = e.InnerException;
            }
            return Retorno;
        }

        private int DecodificaChave(byte[] Chave)
        {
            string Aberto = VirCripWS.Uncrip(Chave);
            if ((Aberto.Length != 32) || (Aberto.Substring(25, 7) != "<-*W*->"))
                return -1;
            if (Usados.Contains(Aberto))
                return -1;
            Usados.Add(Aberto);
            //yyyyMMddHHmmssEMPRRRRRRRR<-*W*->
            DateTime DataHora = new DateTime(int.Parse(Aberto.Substring(0, 4)), int.Parse(Aberto.Substring(4, 2)), int.Parse(Aberto.Substring(6, 2)), int.Parse(Aberto.Substring(8, 2)), int.Parse(Aberto.Substring(10, 2)), int.Parse(Aberto.Substring(12, 2)));
            if (DataHora.AddDays(1).AddHours(2) < DateTime.Now)
                return -1;
            return int.Parse(Aberto.Substring(14,3));
        }

        [WebMethod]
        public string Teste(byte[] Chave)
        {
            try 
            {
                int EMP = DecodificaChave(Chave);
                if (EMP == -1)
                    return "Erro na chave";
                
                return string.Format("ok EMP = {0}",EMP);
            }
            catch (Exception e)
            {
                return RetornodeErro(e);
            }
        }

        /*
        [WebMethod]
        public string INPC(byte[] Chave, DateTime Data,double INPC,double indice)
        {
            try
            {
                string Acao = "";
                int EMP = dBanco.EMP = DecodificaChave(Chave);
                if (EMP == -1)
                    return "Erro na chave";
                int n = dBanco.IndicesTableAdapter.Fill(dBanco.indices, Data);
                if (n == 0)
                {
                    dBanco.indices.AddindicesRow(INPC, indice, Data);
                    Acao = "incluido";
                }
                else
                {
                    if ((dBanco.indices[0].INPC != INPC) || (dBanco.indices[0].Indice != indice))
                    {
                        dBanco.indices[0].INPC = INPC;
                        dBanco.indices[0].Indice = indice;
                        Acao = "Atualizado";
                    }
                    else
                        Acao = "Identico";
                };
                dBanco.IndicesTableAdapter.Update(dBanco.indices);
                return string.Format("ok {0}",Acao);
            }
            catch (Exception e)
            {                 
                return RetornodeErro(e);
            }
        }
        */

        /*
        [WebMethod]
        public dBancos LeCondominios(byte[] Chave)
        {
            try
            {
                int EMP = dBanco.EMP = DecodificaChave(Chave);
                if (EMP == -1)
                {
                    UltimoErro = "Erro na chave";
                    return null;
                };
                dBanco.CondominiosTableAdapter.Fill(dBanco.condominios);
                return dBanco;
            }
            catch (Exception e)
            {
                UltimoErro = RetornodeErro(e);
                return null;
            }
        }*/

        /*
        [WebMethod]
        public string GravaCondominios(byte[] Chave,dBancos dBanco)
        {
            try
            {
                int EMP = dBanco.EMP = DecodificaChave(Chave);
                if (EMP == -1)
                {
                    UltimoErro = "Erro na chave";
                    return null;
                };
                int i = dBanco.CondominiosTableAdapter.Update(dBanco.condominios);
                return string.Format("ok {0}",i);
            }
            catch (Exception e)
            {
                return RetornodeErro(e);                
            }
        }
        */

        /*
        [WebMethod]
        public dBancos LeClientes(byte[] Chave)
        {
            try
            {
                int EMP = dBanco.EMP = DecodificaChave(Chave);
                if (EMP == -1)
                {
                    UltimoErro = "Erro na chave";
                    return null;
                };
                dBanco.ClientesTableAdapter.Fill(dBanco.clientes);
                return dBanco;
            }
            catch (Exception e)
            {
                UltimoErro = RetornodeErro(e);
                return null;
            }
        }
        */

        /*
        [WebMethod]
        public string GravaCliente(byte[] Chave, dBancos dBanco)
        {
            try
            {
                int EMP = dBanco.EMP = DecodificaChave(Chave);
                if (EMP == -1)
                {
                    UltimoErro = "Erro na chave";
                    return null;
                };
                int i = dBanco.ClientesTableAdapter.Update(dBanco.clientes);
                int n = dBanco.NovasTableAdapter.Update(dBanco.novas);
                int d = dBanco.DividasTableAdapter.Update(dBanco.Dividas);
                return string.Format("ok i {0},n {1}, d {2}", i,n,d);
                
            }
            catch (Exception e)
            {
                return RetornodeErro(e);
            }
        }
        */

        [WebMethod]
        public string LeUltimoErro()
        {
            string retorno = UltimoErro;
            UltimoErro = "";
            return retorno;
        }


        /*
        [WebMethod]
        public dBancos LeAcordo(byte[] Chave,string CodCon,string Bloco,string Apartamento)
        {
            try
            {
                int EMP = dBanco.EMP = DecodificaChave(Chave);
                if (EMP == -1)
                {
                    UltimoErro = "Erro na chave";
                    return null;
                };
                dBanco.ClientesTableAdapter.FillByApartamento(dBanco.clientes,CodCon,Bloco,Apartamento);               
                return dBanco;
            }
            catch (Exception e)
            {
                UltimoErro = RetornodeErro(e);
                return null;
            }
        }
        */

        /*
        [WebMethod]
        public string GravaAcordo(byte[] Chave, dBancos dBanco,int ACO)
        {
            try
            {
                int EMP = dBanco.EMP = DecodificaChave(Chave);
                if (EMP == -1)
                {
                    UltimoErro = "Erro na chave";
                    return null;
                };
                dBanco.DividasTableAdapter.DeleteQuery(ACO);
                dBanco.NovasTableAdapter.DeleteQuery(ACO);
                foreach (dBancos.novasRow nvrow in dBanco.novas)
                    dBanco.NovasTableAdapter.DeleteNumero(nvrow.numero);
                int i = dBanco.ClientesTableAdapter.Update(dBanco.clientes);
                int n = dBanco.NovasTableAdapter.Update(dBanco.novas);
                int d = dBanco.DividasTableAdapter.Update(dBanco.Dividas);
                return string.Format("ok ACO:{0} i: {1},n: {2}, d: {3}",ACO, i, n, d);

            }
            catch (Exception e)
            {
                return RetornodeErro(e);
            }
        }
        */

        /*
        [WebMethod]
        public string RemoveAcordo(byte[] Chave,int ACO)
        {
            try
            {
                int EMP = dBanco.EMP = DecodificaChave(Chave);
                if (EMP == -1)
                {
                    UltimoErro = "Erro na chave";
                    return null;
                };
                int i = dBanco.DividasTableAdapter.DeleteQuery(ACO);
                i += dBanco.NovasTableAdapter.DeleteQuery(ACO);
                return string.Format("ok {0}", i);

            }
            catch (Exception e)
            {
                return RetornodeErro(e);
            }
        }*/

        /*
        [WebMethod]
        public string GravaBoletos(byte[] Chave, dBancos dBancoRet)
        {
            string As = "";
            try
            {
                int A = 0;
                int D = 0;
                int U = 0;
                bool NovaLinha;
                dBancos.BoletosRow BolRow;
                int EMP = dBanco.EMP = DecodificaChave(Chave);
                if (EMP == -1)                
                    return "Erro na chave";
                foreach (dBancos.BoletosRow retrow in dBancoRet.Boletos)
                {
                    if (retrow.Cancelar)
                        D += dBanco.BoletosTableAdapter.DeleteQuery((decimal)retrow.Numero);
                    else
                    {
                        NovaLinha = (dBanco.BoletosTableAdapter.Fill(dBanco.Boletos, (decimal)retrow.Numero) == 0);
                        if (NovaLinha)
                        {
                            BolRow = dBanco.Boletos.NewBoletosRow();
                            A++;
                            As += string.Format("\r\n {0}", retrow.Numero);
                        }
                        else
                        {
                            BolRow = dBanco.Boletos[0];
                            U++;
                        }
                        foreach (DataColumn Coluna in dBanco.Boletos.Columns)                           
                                BolRow[Coluna] = retrow[Coluna.ColumnName];
                        if (NovaLinha)
                            dBanco.Boletos.AddBoletosRow(BolRow);
                        dBanco.BoletosTableAdapter.Update(BolRow);
                    }
                }
                
                //int i = dBanco.ClientesTableAdapter.Update(dBanco.clientes);
                return string.Format("ok A: {0} U: {1} D: {2}", A, U, D)+As;

            }
            catch (Exception e)
            {
                return RetornodeErro(e);
            }
        }
        */

        #region ComandoSQL

        /*
        [WebMethod]
        public int SQLExecuteNonQuery(byte[] Chave,int Banco, byte[] Comando)
        {
            string NomeBanco = Banco == 1 ? "NEON1ConnectionString" : "bal1ConnectionString";
            int retorno = -1;
            try
            {
                int EMP = dBanco.EMP = DecodificaChave(Chave);
                if (EMP == -1)
                {
                    UltimoErro = "Erro na chave";
                    return -1;
                };
                string ComandoAberto = VirCripWS.Uncrip(Comando);
                using (System.Data.OleDb.OleDbConnection Cone = new System.Data.OleDb.OleDbConnection(System.Configuration.ConfigurationManager.ConnectionStrings[NomeBanco].ConnectionString))
                {
                    dBanco.AjustaStringDeConexao(Cone);
                    System.Data.OleDb.OleDbCommand Comma = new System.Data.OleDb.OleDbCommand(ComandoAberto, Cone);
                    Cone.Open();
                    retorno = Comma.ExecuteNonQuery();
                    Cone.Close();
                }
                return retorno;

            }
            catch (Exception e)
            {
                UltimoErro = RetornodeErro(e);
                return -1;
            }
        }*/

        /*
        [WebMethod]
        public DataSet SQLExecute(byte[] Chave, int Banco, byte[] Comando)
        {
            string NomeBanco = Banco == 1 ? "NEON1ConnectionString" : "bal1ConnectionString";
            DataSet Retorno = null;
            try
            {
                int EMP = dBanco.EMP = DecodificaChave(Chave);
                if (EMP == -1)
                {
                    UltimoErro = "Erro na chave";
                    return null;
                };
                string ComandoAberto = VirCripWS.Uncrip(Comando);
                using (System.Data.OleDb.OleDbConnection Cone = new System.Data.OleDb.OleDbConnection(System.Configuration.ConfigurationManager.ConnectionStrings[NomeBanco].ConnectionString))
                {
                    dBanco.AjustaStringDeConexao(Cone);
                    System.Data.OleDb.OleDbCommand Comma = new System.Data.OleDb.OleDbCommand(ComandoAberto, Cone);
                    System.Data.OleDb.OleDbDataAdapter AdAP = new System.Data.OleDb.OleDbDataAdapter(Comma);
                    Retorno = new DataSet();
                    Cone.Open();
                    AdAP.Fill(Retorno);
                    Cone.Close();
                }
                return Retorno;

            }
            catch (Exception e)
            {
                UltimoErro = RetornodeErro(e);
                return null;
            }
        }
        */

        [WebMethod]
        public DataSet SQLExecuteNeon(byte[] Chave, byte[] Comando)
        {
            DataSet Retorno = null;
            try
            {
                int EMP = DecodificaChave(Chave);
                if (EMP == -1)
                {
                    UltimoErro = "Erro na chave";
                    return null;
                }
                string ConnString = System.Configuration.ConfigurationManager.ConnectionStrings["NeonConnectionString"].ConnectionString + ";Provider=SQLOLEDB";
                if (EMP == 3)
                    ConnString = ConnString.ToUpper().Replace("INITIAL CATALOG=NEON", "INITIAL CATALOG=NEONSA");
                string ComandoAberto = VirCripWS.Uncrip(Comando);
                using (System.Data.OleDb.OleDbConnection Conn = new System.Data.OleDb.OleDbConnection(ConnString))
                {
                    System.Data.OleDb.OleDbCommand Command = new System.Data.OleDb.OleDbCommand(ComandoAberto, Conn);
                    System.Data.OleDb.OleDbDataAdapter DataAdapter = new System.Data.OleDb.OleDbDataAdapter(Command);
                    Retorno = new DataSet();
                    Conn.Open();
                    DataAdapter.Fill(Retorno);
                    Conn.Close();
                }
                return Retorno;
            }
            catch (Exception e)
            {
                UltimoErro = RetornodeErro(e);
                return null;
            }
        }

        #endregion

        [WebMethod]
        public string PublicaBAL(byte[] Chave, dBAL dBalPublicar,string CODCON,int competencia)
        {
            return PublicaBALM(Chave, dBalPublicar,competencia,CODCON, null, null, null, null);
        }

        [WebMethod]
        public string PublicaDOC(byte[] Chave,string Nome, byte[] Dados)
        {
            try
            {
                int EMP = DecodificaChave(Chave);
                if (EMP == -1)
                    return UltimoErro = "Erro na chave";
                string Arquivo = string.Format(@"{0}{1}",ConfigurationManager.AppSettings["publicacoesIND"],Nome);

                using (MemoryStream ms = new MemoryStream(Dados))
                {
                    using (FileStream fs = new FileStream(Arquivo, FileMode.Create))
                    {
                        ms.WriteTo(fs);
                        fs.Close();
                    };
                };                
                return "ok";

            }
            catch (Exception e)
            {
                return UltimoErro = RetornodeErro(e);
            }
        }

        [WebMethod]
        public string PublicaDOCPasta(byte[] Chave, string Nome, string Pasta, byte[] Dados)
        {
            try
            {
                int EMP = DecodificaChave(Chave);
                if (EMP == -1)
                    return UltimoErro = "Erro na chave";
                string PastaCompleta = string.Format(@"{0}", ConfigurationManager.AppSettings["publicacoesIND"]);
                if (Pasta != "")
                    PastaCompleta += string.Format(@"{0}/", Pasta);
                if (!Directory.Exists(PastaCompleta))
                    Directory.CreateDirectory(PastaCompleta);
                string Arquivo = string.Format(@"{0}{1}", PastaCompleta, Nome);

                using (MemoryStream ms = new MemoryStream(Dados))
                {
                    using (FileStream fs = new FileStream(Arquivo, FileMode.Create))
                    {
                        ms.WriteTo(fs);
                        fs.Close();
                    };
                };
                return "ok";

            }
            catch (Exception e)
            {
                return UltimoErro = RetornodeErro(e);
            }
        }

        [WebMethod]
        public bool ExisteDOC(byte[] Chave, string Nome, string Pasta)
        {
            //string Log = "";
            try
            {
                int EMP = DecodificaChave(Chave);
                if (EMP == -1)
                {
                    UltimoErro = "Erro na chave";
                    return false;
                };
                //Log = "Passou\r\n";
                string PastaCompleta = string.Format(@"{0}", ConfigurationManager.AppSettings["publicacoesIND"]);
                //Log += string.Format("PastaCompleta = {0}\r\n",PastaCompleta);
                if(Pasta != "")
                    PastaCompleta += string.Format(@"{0}/", Pasta);
                //Log += string.Format("PastaCompleta = {0}\r\n", PastaCompleta);
                string Arquivo = string.Format(@"{0}{1}", PastaCompleta, Nome);
                //Log += string.Format("arquivo = {0}\r\n", Arquivo);
                //if (File.Exists(Arquivo))
                //    Log += "Achou";
                //else
                //    Log += "N�o achou";
                //System.IO.File.WriteAllText(string.Format(@"C:\clientes\neon\pagina\publicacoesind\log{0:mm_ss}.log",DateTime.Now), Log);
                return File.Exists(Arquivo);

            }
            catch (Exception e)
            {
                UltimoErro = RetornodeErro(e);
                //System.IO.File.WriteAllText(@"C:\clientes\neon\pagina\publicacoesind\log.log", "Erro: " + e.Message);
                return false;
            }
            
        }

        [WebMethod]
        public byte[] RecuperaDOC(byte[] Chave, string Nome, string Pasta)
        {
            try
            {
                int EMP = DecodificaChave(Chave);
                if (EMP == -1)
                {
                    UltimoErro = "Erro na chave";
                    return null;
                }
                string PastaCompleta = string.Format(@"{0}", ConfigurationManager.AppSettings["publicacoesIND"]);
                if (Pasta != "")
                    PastaCompleta += string.Format(@"{0}/", Pasta);
                string Arquivo = string.Format(@"{0}{1}", PastaCompleta, Nome);
                byte[] bdata;
                using (FileStream fStream = new FileStream(Arquivo, FileMode.Open, FileAccess.Read))
                {
                    using (BinaryReader br = new BinaryReader(fStream))
                    {
                        bdata = br.ReadBytes((int)(new FileInfo(Arquivo).Length));
                        br.Close();
                        fStream.Close();
                    }
                };
                return bdata;
            }
            catch (Exception e)
            {
                UltimoErro = RetornodeErro(e);
                return null;
            }
        }

        private bool GravarLog = false;

        private System.Text.StringBuilder SB;

        private void GravaLog(string Men)
        {
            if (!GravarLog)
                return;
            if (SB == null)
                SB = new System.Text.StringBuilder();
            SB.AppendLine(string.Format("{0:dd/MM/yyyy HH:mm:ss} - {1}",DateTime.Now, Men));
            if (Men == "FIM")
                System.IO.File.WriteAllText("D:\\lixo\\Log.log", SB.ToString());
        }
       

        [WebMethod]
        public string PublicaBALM(byte[] Chave, dBAL dBalPublicar, int competencia,string CODCON, byte[] M1,byte[] M2,byte[] M3,byte[] M4)
        {
            GravaLog("Ponto 1");
            try
            {
                GravaLog("Ponto 2");
                int EMP = dBalPublicar.EMP = DecodificaChave(Chave);
                if (EMP == -1)
                    return UltimoErro = "Erro na chave";
                GravaLog(string.Format("Ponto 3 - EMP {0} LinhasTotal = {1}", EMP, dBalPublicar.BalanceteTotais.Count));
                dBAL.BalanceteTotaisRow rowTot = dBalPublicar.BalanceteTotais[0];
                GravaLog(string.Format("Ponto 4 - Linhas Saldo = {0}", dBalPublicar.BalanceteSaldos.Count));
                if (dBalPublicar.BalanceteSaldos.Count > 0)
                {
                    dBAL.BalanceteSaldosRow rowAmostra = dBalPublicar.BalanceteSaldos[0];
                    dBalPublicar.BalanceteSaldosTableAdapter.Apagar(rowAmostra.CON, rowAmostra.Competencia, EMP);
                    dBalPublicar.BalanceteSaldosTableAdapter.Update(dBalPublicar.BalanceteSaldos);
                }
                GravaLog("Ponto 5");
                dBalPublicar.Det1TableAdapter.Apaga(rowTot.EMP, rowTot.BAL);
                dBalPublicar.TotaisTableAdapter.Apaga(rowTot.BAL, rowTot.EMP);
                dBalPublicar.TotaisTableAdapter.Update(dBalPublicar.BalanceteTotais);
                dBalPublicar.Det1TableAdapter.Update(dBalPublicar.BalanceteDet);
                int modelo = 1;
                GravaLog("Ponto 6");
                foreach (byte[] Mx in new byte[][] { M1, M2, M3, M4 })
                {
                    if (Mx != null)
                    {
                        string Arquivo = string.Format(@"{0}balancetes\{1}{2}{3}{4}{5}.asp",
                            ConfigurationManager.AppSettings["publicacoes"],
                            EMP,
                            CODCON.Replace(".", "_").Replace("/", "_").Replace("\\", "_").Replace(" ", "_"),
                            modelo,
                            competencia % 100,
                            competencia / 100);
                        using (MemoryStream ms = new MemoryStream(Mx))
                        {
                            using (FileStream fs = new FileStream(Arquivo, FileMode.Create))
                            {
                                ms.WriteTo(fs);
                                fs.Close();
                            };
                        }
                    }
                    modelo++;
                }
                GravaLog("Ponto 7");
                return "ok";

            }
            catch (Exception e)
            {
                GravaLog(string.Format("Erro:{0}",e.Message));
                UltimoErro = RetornodeErro(e);
                GravaLog(UltimoErro);
                return UltimoErro;
            }
            finally
            {
                GravaLog("FIM");
            }
        }

        [WebMethod]
        public bool VerificaBALM(byte[] Chave, string CODCON,string Competencia,int modelo)
        {
            try
            {
                int EMP = DecodificaChave(Chave);
                if (EMP == -1)
                {
                    UltimoErro = "Erro na chave";
                    return false;
                }
                string Arquivo = string.Format(@"{0}balancetes\{1}{2}{3}{4}.asp", 
                            System.Configuration.ConfigurationManager.AppSettings["publicacoes"],
                            EMP,
                            CODCON.Replace(".", "_").Replace("\\", "_").Replace("/", "_").Replace(" ", "_"),
                            modelo,
                            Competencia);
                if (System.IO.File.Exists(Arquivo))
                    return true;
                else
                    return false;

            }
            catch (Exception e)
            {
                UltimoErro = RetornodeErro(e);
                return false;
            }
        }

        [WebMethod]
        public string ApagaBAL(byte[] Chave, int BAL)
        {
            try
            {
                dBAL dBalPublicar = new dBAL();
                int EMP = dBalPublicar.EMP = DecodificaChave(Chave);
                if (EMP == -1)
                    return UltimoErro = "Erro na chave";                
                dBalPublicar.Det1TableAdapter.Apaga(EMP, BAL);
                dBalPublicar.TotaisTableAdapter.Apaga(BAL, EMP);                
                return "ok";

            }
            catch (Exception e)
            {
                return UltimoErro = RetornodeErro(e);
            }
        }

        private static string UltimoErro;
    }
}
