﻿/*
MR - 17/04/2014 15:40 -           - Remessa de pagamento eletrônico (Alterações indicadas em *** MRC - INICIO - PAG-FOR (17/04/2014 15:40) ***)
MR - 29/04/2014 15:25 -           - Cancelamento de pagamento eletrônico (Alterações indicadas por *** MRC - INICIO - PAG-FOR (29/04/2014 15:25) ***)
MR - 12/11/2014 10:00 -           - Remessa de pagamento de tributos eletrônico (Alterações indicadas por *** MRC - INICIO - TRIBUTOS (12/11/2014 10:00) ***)
MR - 03/12/2014 10:00 -           - Remessa de pagamento de tributos eletrônico - tratando GPS/INSS (Alterações indicadas por *** MRC - INICIO - TRIBUTOS (03/12/2014 10:00) ***)
MR - 22/12/2014 10:00 -           - Verifica se realmente copiou para pasta de envio, 'move' com confirmação e retentativa (Alterações indicadas por *** MRC - INICIO (22/12/2014 10:00) ***)
MR - 26/12/2014 10:00 -           - Remessa de pagamento de tributos eletrônico - tratando FGTS (Alterações indicadas por *** MRC - INICIO - TRIBUTOS (26/12/2014 10:00) ***)
MR - 10/02/2015 12:00 -           - Tratamento de tipo de envio de arquivos diferentes para remessa de pagamento eletrônico, de acordo com o tipo configurado para o condominio (Alterações indicadas por *** MRC - INICIO - PAG-FOR (10/02/2015 12:00) ***)
MR - 20/02/2015 20:00 -           - Alterações em mensagem para deixar mais genérica
MR - 27/04/2015 12:00 -           - Remessa de pagamento de tributos eletrônico das notas (Alterações indicadas por *** MRC - INICIO - TRIBUTO ELETRONICO (27/04/2015 12:00) ***)
MR - 18/08/2015 10:00 -           - Tempo entre copias de arquivos de pagamento eletronico para evitar sobreposicao (Alterações indicadas por *** MRC - INICIO (18/08/2015 10:00) ***)
                                  - Aumento de tentativas de verificacao de arquivo copiado (boleto, pagamento eletronico e tributo) para evitar perda
                                  - Remessa de exclusão de tributo folha
MR - 14/01/2016 12:30 -           - Reinicia automaticamente o número sequencial de remessa de tributo para 1 quando chega no 99999 (Alterações indicadas por *** MRC - INICIO - TRIBUTOS (14/01/2016 12:30) ***)
                                  - Tempo para verificar sequencial de arquivo para evitar sobreposicao (Alterações indicadas por *** MRC - INICIO (14/01/2016 12:30) ***)
MR - 15/03/2016 10:00 -           - Código de Empresa para remessa Bradesco diferenciado entre SA e SBC 
MR - 21/03/2016 20:00 -           - Registro de boletos separado para Bradesco e Itau (Alterações indicadas por *** MRC - INICIO (21/03/2016 20:00) ***)
MR - 01/04/2016 19:00 -           - Correção na funcao VerificaSequencialRemessaArquivo (Alterações indicadas por *** MRC - INICIO (01/04/2016 19:00) ***)
MR - 28/04/2016 12:00 -           - Seta corretamento caminho default para pagto eletrônico quando não indicado para o condominio, com base no defaul passado na chamada da funcao (Alterações indicadas por *** MRC - INICIO (28/04/2016 12:00) ***)
MR - 08/06/2016 11:00 -           - Trata número sequencial de remessa para pagamentos eletrônicos por banco e não mais por condominio (Alterações indicadas por *** MRC - INICIO (08/06/2016 11:00) ***)
*/

using Boletos.Boleto;
using Boletos.Super;
using CompontesBasicosProc;
using dllCheques;
using dllCNAB;
using EDIBoletos;
using Framework;
using System;
using VirEnumeracoes;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using VirEnumeracoesNeon;


namespace EDIBoletosNeon
{
    /// <summary>
    /// Arquivo
    /// </summary>
    public class Arquivo
    {
        private List<Boleto> BoletosRemessa;
        private dEDIBoletosNeon dEDI = new dEDIBoletosNeon();
        //private int iAtual = 0;
        private List<SuperBoleto> SuperBoletosRemessa;
        /// <summary>
        /// Cancela
        /// </summary>
        public bool Cancelar;

        /*
        /// <summary>
        /// Local
        /// </summary>
        public string strLocal = string.Empty;
        */

        //passar para framework
        private static string RetornoDeErro(Exception e)
        {
            string Retorno = string.Format("ERRO:{0}\r\n", DateTime.Now);
            while (e != null)
            {
                Retorno += string.Format("Mensagem:{0}\r\n{1}\r\n-------------------------------------\r\n", e.Message, e.StackTrace);
                e = e.InnerException;
            }
            return Retorno;
        }

        /// <summary>
        /// Esta função é para gravação intermediaria dos quivos retorno
        /// </summary>
        /// <param name="BoletoRemessa"></param>
        /// <param name="strPathTemp"></param>
        /// <param name="strArquivo"></param>
        /// <param name="CaixaPostal"></param>
        private void SubGravaArquivoRemessa(BoletoRemessaBase BoletoRemessa, string strPathTemp, SortedList<string, string> strArquivo, string CaixaPostal)
        {
            if (BoletoRemessa is BoletoRemessaBra)
                throw new Exception("Uso incorreto de função em SubGravaArquivoRemessa");
            string NovoArquivo = BoletoRemessa.GravaArquivo(strPathTemp);
            if (NovoArquivo == string.Empty)
                throw new Exception("Erro ao gerar arquivo: " + BoletoRemessa.UltimoErro.Message);
            strArquivo.Add(NovoArquivo, CaixaPostal);
        }

        private static int VerificaSequencialRemessaArquivo(string strPathBackup, string strFilter = "")
        {
            int intSequencial = 1;
            //Tempo para termino de copia de arquivos a fim de determinar corretamente a quantidade de arquivos (evitando sobreposicao)
            System.Threading.Thread.Sleep(2000);

            //Verifica se o diretorio de backup ainda nao existe
            if (!Directory.Exists(strPathBackup))
                Directory.CreateDirectory(strPathBackup);
            else
                //Seta sequencial com base na quantidade de arquivos                
                if (strFilter == string.Empty)
                intSequencial = Directory.GetFiles(strPathBackup).Count() + 1;
            else
                intSequencial = Directory.GetFiles(strPathBackup, strFilter).Count() + 1;
            return intSequencial;
        }

        /// <summary>
        /// Gera arquivo de remessa boleto do banco informado (registro) - suporta Bradesco , Itau, Caixa e Santander
        /// </summary>
        /// <param name="max"></param>
        /// <param name="strPathArquivo"></param>
        /// <param name="iTotalref"></param>
        /// <param name="iAtual"></param>
        /// <param name="Banco"></param>
        /// <returns></returns>
        public string Remessa(int max, string strPathArquivo, ref int iTotalref, ref int iAtual, int Banco)
        {
            DateTime Tinicio = DateTime.Now;

            Console.WriteLine(string.Format("Banco:{0}", Banco));
            iTotalref = iAtual = 0;
            SortedList<string, string> strArquivo = new SortedList<string, string>();
            List<string> strArquivoFinal = new List<string>();
            string strPathBackup = string.Empty;
            string strPathTemp = string.Empty;
            string retorno = string.Empty;

            if (Cancelar)
                throw new Exception("Cancelado aguarda LIBERAR");

            //Seleciona boletos para registro
            BoletosProc.Boleto.dBoletos dBoletos = new BoletosProc.Boleto.dBoletos();
            int nBoletos = dBoletos.BOLetosTableAdapter.FillRegistrar(dBoletos.BOLetos, DateTime.Now.AddMinutes(-15), DateTime.Today.AddMonths(1));
            dBoletos.BoletoDEscontoTableAdapter.FillRegistrar(dBoletos.BoletoDEsconto, DateTime.Now.AddMinutes(-15), DateTime.Today.AddMonths(1));
            int nSuper = dEDI.SuperBOletoTableAdapter.FillRegistrar(dEDI.SuperBOleto, DateTime.Now.AddMinutes(-15));
            iTotalref = nBoletos + nSuper;
            //Console.WriteLine(string.Format("Boletos:{0} {1}", nBoletos , nSuper));
            if ((nBoletos + nSuper) == 0)
            {
                retorno = "Remessa: Nenhum boleto\r\n";
                return retorno;
            }

            //Inicia variáveis                        
            strPathTemp = @"C:\Modulo servidor\TMP\";
            if (!Directory.Exists(strPathTemp + "BKP"))
                Directory.CreateDirectory(strPathTemp + "BKP");
            strPathBackup = string.Format(@"{0}BKP\{1:yyyyMMdd}\", strPathTemp, DateTime.Now);
            //strLocal = "Remessa";

            //Inicia remessa 
            BoletoRemessaBase BoletoRemessa = null;
            switch (Banco)
            {
                case 237:
                    BoletoRemessa = new BoletoRemessaBra();
                    break;
                case 341:
                    BoletoRemessa = new BoletoRemessaItau();
                    break;
                case 33:
                    BoletoRemessa = new BoletoRemessaCNAB_SAN();
                    break;
                case 104:
                    BoletoRemessa = new BoletoRemessaCNAB_CX();
                    break;
                default:
                    return string.Format("Remessa: Banco {0} não configurado para registro de boleto\r\n", Banco);
            }
            BoletoRemessa.IniciaRemessa();
            //Insere boletos na remessa (ate a quantidade maxima)            
            BoletosRemessa = new List<Boleto>();
            foreach (BoletosProc.Boleto.dBoletos.BOLetosRow BOLrow in dBoletos.BOLetos)
            {
                if (Cancelar)
                    throw new Exception("Cancelado aguarda LIBERAR");
                //Console.WriteLine(string.Format("Boleto:{0}", BOLrow.BOL));                
                Boleto Bol = new Boleto(BOLrow);
                if (Bol.rowPrincipal.BOLVencto < DateTime.Today)
                    Bol.AlteraStatusRegistro(StatusRegistroBoleto.BoletoSemRegistro);
                else
                {
                    if ((Bol.rowCON.CON_BCO == Banco) && (!Bol.rowCON.IsCONCnpjNull()))
                    {
                        bool Cancela = false;
                        if (Bol.Apartamento != null)
                        {
                            DocBacarios.CPFCNPJ cpf = Bol.Apartamento.PESCpfCnpj(Bol.Proprietario);
                            if ((cpf == null) || (cpf.Tipo == DocBacarios.TipoCpfCnpj.INVALIDO))
                                Cancela = true;
                        }                       
                        if (!Cancela && Bol.Registrar(BoletoRemessa))
                            BoletosRemessa.Add(Bol);
                    }
                }
                iAtual++;
                if (iAtual == max)
                    break;
            };

            //Insere superboletos na remessa (ate a quantidade maxima)
            SuperBoletosRemessa = new List<SuperBoleto>();
            foreach (dEDIBoletosNeon.SuperBOletoRow SBOrow in dEDI.SuperBOleto)
            {                
                if (Cancelar)
                    throw new Exception("Cancelado aguarda LIBERAR");
                SuperBoleto SuperBol = new SuperBoleto(SBOrow.SBO);
                if (SBOrow.SBOVencto < DateTime.Today)
                    SuperBol.AlteraStatusRegistro(StatusRegistroBoleto.BoletoSemRegistro);
                //if(SuperBol.rows)
                else
                {
                    bool Cancela = false;
                    if ((SuperBol.Apartamento == null) || (SuperBol.Apartamento.PESCpfCnpj(SuperBol.Proprietario) == null))
                        Cancela = true;
                    else
                        if (SuperBol.Apartamento.PESCpfCnpj(SuperBol.Proprietario).Tipo == DocBacarios.TipoCpfCnpj.INVALIDO)
                            Cancela = true;
                    if (!Cancela && (SuperBol.rowCON.CON_BCO == Banco) && (!SuperBol.rowCON.IsCONCnpjNull()))
                        if (SuperBol.Registrar(BoletoRemessa))
                            SuperBoletosRemessa.Add(SuperBol);
                }
                iAtual++;
                if (iAtual == max)
                    break;
            };

            //Verifica se tem algum boleto efetivo para registro
            if ((BoletosRemessa.Count + SuperBoletosRemessa.Count) == 0)
            {
                retorno = "Remessa: Nenhum boleto efetivo\r\n";
                return retorno;
            }

            int BOLCON;
            int CNR;
            string CaixaPostal;
            //Processa a remessa de acordo com o banco
            try
            {
                switch (Banco)
                {
                    case 237:

                        //BRADESCO: UM GRUPO DE BOLETOS UNICO PARA NEON IMOVEIS
                        BoletoRemessa.SequencialRemessaArquivo = VerificaSequencialRemessaArquivo(strPathBackup, "CO*.REM");

                        //Seta propriedades gerais na remessa
                        BoletoRemessa.CodEmpresa = DSCentral.EMP == 1 ? 602270 : 4854082;
                        BoletoRemessa.NomeEmpresa = "Neon Imoveis";
                        BoletoRemessa.Carteira = "9";

                        //Verifica o sequencial de remessa do banco 
                        BoletoRemessa.SequencialRemessa = (int)dEDI.BANCOSTableAdapter.VerificaSequenciaRemessa(Banco) + 1;
                        dEDI.BANCOSTableAdapter.IncrementaSequencialRemessa(Banco);
                        //Grava arquivo remessa
                        strArquivo.Add(BoletoRemessa.GravaArquivo(strPathTemp), string.Empty);
                        if (strArquivo.Keys[0] == string.Empty)
                            throw new Exception("Erro ao gerar arquivo", BoletoRemessa.UltimoErro);

                        //Abra a transacao
                        VirMSSQL.TableAdapter.AbreTrasacaoSQL("EDIBoletos Arquivo Remessa Bradesco - 224", dEDI.BANCOSTableAdapter);

                        //Atualiza boletos (StatusRegistro)
                        foreach (Boleto bol in BoletosRemessa)
                        {
                            if (Cancelar)
                                throw new Exception("Cancelado aguarda LIBERAR");
                            switch (bol.BOLStatusRegistro)
                            {
                                case StatusRegistroBoleto.BoletoRegistrando:
                                case StatusRegistroBoleto.AlterarRegistroData:
                                case StatusRegistroBoleto.AlterarRegistroValor:
                                    bol.AlteraStatusRegistro(StatusRegistroBoleto.BoletoAguardandoRetorno);
                                    break;
                                case StatusRegistroBoleto.BoletoRegistrandoDA:
                                    bol.AlteraStatusRegistro(StatusRegistroBoleto.BoletoAguardandoRetornoDA);
                                    break;
                            }                            
                        }

                        //Atualiza super boletos (StatusRegistro)
                        foreach (SuperBoleto sbol in SuperBoletosRemessa)
                        {
                            if (Cancelar)
                                throw new Exception("Cancelado aguarda LIBERAR");
                            sbol.AlteraStatusRegistro(StatusRegistroBoleto.BoletoAguardandoRetorno);
                        }
                        break;


                    case 341:
                    case 33:
                    case 104:
                        //ITAU,CAIXA,Santander: UM GRUPO DE BOLETOS PARA CADA CONDOMINIO                        

                        //Ordena as listas por condominio
                        BoletosRemessa = BoletosRemessa.OrderBy(bol => bol.CON).ToList();
                        SuperBoletosRemessa = SuperBoletosRemessa.OrderBy(bol => bol.CON).ToList();

                        //Abra a transacao
                        VirMSSQL.TableAdapter.AbreTrasacaoSQL("EDIBoletos Arquivo Remessa Itau - 299", dEDI.BANCOSTableAdapter);

                        //Gera remessa de boletos / superboletos por condominio (ate a quantidade maxima)
                        BOLCON = 0;
                        CNR = 0;
                        CaixaPostal = string.Empty;
                        foreach (Boleto bol in BoletosRemessa)
                        {
                            if (Cancelar)
                                throw new Exception("Cancelado aguarda LIBERAR");

                            //Verifica se esta tratando novo condominio
                            if (BOLCON != bol.CON)
                            {
                                //Verifica se existia anterior Fecha condominio na remessa (Grava arquivo)
                                if (BOLCON != 0)
                                    SubGravaArquivoRemessa(BoletoRemessa, strPathTemp, strArquivo, CaixaPostal);
                                //Pega o condominio que esta tratando e ajusta CNR
                                BOLCON = bol.CON;
                                if (!bol.rowCON.IsCONCodigoCNRNull())
                                    int.TryParse(bol.rowCON.CONCodigoCNR.Trim(), out CNR);
                                CaixaPostal = bol.Conta.CaixaPostal;
                                //Seta propriedades especificas do condominio na remessa  
                                dEDI.BANCOSTableAdapter.IncrementaSequencialRemessa(Banco);
                                BoletoRemessa.Configura(int.Parse(bol.rowCON.CONAgencia),
                                                        int.Parse(bol.rowCON.CONConta),
                                                        bol.rowCON.CONDigitoConta,
                                                        bol.rowCON.CONNome,
                                                        new DocBacarios.CPFCNPJ(bol.rowCON.CONCnpj),
                                                        bol.CarteiraBanco.ToString(),
                                                        CNR,
                                                        (int)dEDI.BANCOSTableAdapter.VerificaSequenciaRemessa(Banco));

                                BoletoRemessa.Boletos.Clear();

                            }

                            //Insere boleto na remessa
                            bol.Registrar(BoletoRemessa);

                            //Atualiza boleto (StatusRegistro)
                            bol.AlteraStatusRegistro(StatusRegistroBoleto.BoletoAguardandoRetorno);

                            //Verifica maximo de processamento
                            iAtual++;
                            if (iAtual == max)
                                break;
                        };
                        //Fecha ultimo condominio na remessa (Grava arquivo)
                        if (BoletosRemessa.Count > 0)
                            SubGravaArquivoRemessa(BoletoRemessa, strPathTemp, strArquivo, CaixaPostal);
                        BOLCON = 0;
                        foreach (SuperBoleto sbol in SuperBoletosRemessa)
                        {
                            if (Cancelar)
                                throw new Exception("Cancelado aguarda LIBERAR");

                            //Verifica se esta tratando novo condominio
                            if (BOLCON != sbol.CON)
                            {
                                //Verifica se existia anterior Fecha condominio na remessa (Grava arquivo)
                                if (BOLCON != 0)
                                    SubGravaArquivoRemessa(BoletoRemessa, strPathTemp, strArquivo, CaixaPostal);
                                //Pega o condominio que esta tratando
                                BOLCON = sbol.CON;
                                if (!sbol.rowCON.IsCONCodigoCNRNull())
                                    int.TryParse(sbol.rowCON.CONCodigoCNR.Trim(), out CNR);
                                CaixaPostal = sbol.Conta.CaixaPostal;
                                //Seta propriedades especificas do condominio na remessa
                                dEDI.BANCOSTableAdapter.IncrementaSequencialRemessa(Banco);
                                BoletoRemessa.Configura(int.Parse(sbol.rowCON.CONAgencia),
                                                        int.Parse(sbol.rowCON.CONConta),
                                                        sbol.rowCON.CONDigitoConta,
                                                        sbol.rowCON.CONNome,
                                                        new DocBacarios.CPFCNPJ(sbol.rowCON.CONCnpj),
                                                        ((Boleto)(sbol.BoletosFilhos[0])).CarteiraBanco.ToString(),
                                                        CNR,
                                                        (int)dEDI.BANCOSTableAdapter.VerificaSequenciaRemessa(Banco));
                                BoletoRemessa.Boletos.Clear();

                            }

                            //Insere boleto na remessa
                            sbol.Registrar(BoletoRemessa);

                            //Atualiza boleto (StatusRegistro)
                            sbol.AlteraStatusRegistro(StatusRegistroBoleto.BoletoAguardandoRetorno);

                            //Verifica maximo de processamento
                            iAtual++;
                            if (iAtual == max)
                                break;
                        };
                        //Fecha ultimo condominio na remessa (Grava arquivo)
                        if (SuperBoletosRemessa.Count > 0)
                            SubGravaArquivoRemessa(BoletoRemessa, strPathTemp, strArquivo, CaixaPostal);

                        if (strArquivo.Count == 0)
                            throw new Exception("Erro ao gerar arquivo: " + BoletoRemessa.UltimoErro.Message);
                        break;
                    default:
                        throw new Exception(string.Format("Banco não implementado : {0}", BoletoRemessa.UltimoErro.Message));
                }

                //Copia o arquivo para backup
                foreach (string ArquivoX in strArquivo.Keys)
                    File.Copy(ArquivoX, ArquivoX.Replace(strPathTemp, strPathBackup));

                //Copia o arquivo para envio verificando se foi mesmo para a pasta e remove o original ('move' com confirmação)
                foreach (string ArquivoX in strArquivo.Keys)
                {
                    string Subpasta = string.Empty;
                    if (Banco == 104)
                    {
                        Subpasta = string.Format("{0}\\REMESSA\\", strArquivo[ArquivoX]);
                        string Subpastateste = string.Format("{0}{1}", strPathArquivo, Subpasta);
                        if (!Directory.Exists(Subpastateste))
                            Directory.CreateDirectory(Subpastateste);
                    }
                    string strArquivoFinalX = string.Format("{0}{1}{2}", strPathArquivo, Subpasta, Path.GetFileName(ArquivoX));
                    if (File.Exists(strArquivoFinalX))
                        File.Delete(strArquivoFinalX);
                    File.Move(ArquivoX, strArquivoFinalX);
                    //System.IO.File.Copy(ArquivoX, strArquivoFinalX);
                    //System.IO.File.Delete(ArquivoX);
                    strArquivoFinal.Add(strArquivoFinalX);
                }

                //Completa a transacao
                VirMSSQL.TableAdapter.CommitSQL();

                //Retorna sucesso                
                TimeSpan TempoTotal = DateTime.Now - Tinicio;
                retorno = string.Format("({0:n1} s) Remessa: Boletos {1} Super Boletos {2}\r\n", TempoTotal.TotalSeconds, BoletosRemessa.Count, SuperBoletosRemessa.Count);
                foreach (string ArqF in strArquivoFinal)
                    retorno += string.Format("           - Arquivo {0}\r\n", ArqF);
            }
            catch (Exception e)
            {
                //Roolback da transacao
                VirMSSQL.TableAdapter.VircatchSQL(e);

                //Apaga arquivos de backup e envio
                if (strPathTemp != string.Empty)
                    foreach (string Arquivo in strArquivo.Keys)
                    {
                        if (File.Exists(Arquivo.Replace(strPathTemp, strPathBackup)))
                            File.Delete(Arquivo.Replace(strPathTemp, strPathBackup));
                        if (File.Exists(Arquivo))
                            File.Delete(Arquivo);
                    }

                //Retorna erro
                retorno = string.Format("****Erro**** Remessa:\r\nDados do erro\r\n{0}", RetornoDeErro(e));
            }
            finally
            {
                //strLocal = "--";

                //Apaga arquivo temporario
                foreach (string Arquivo in strArquivo.Keys)
                    if (File.Exists(Arquivo))
                        File.Delete(Arquivo);
            }

            return retorno;
        }

        /// <summary>
        /// Gera arquivo de remessa cheque (pagamento eletrônico)
        /// </summary>
        /// <param name="max">Máximo de registro a processar</param>
        /// <param name="strPathArquivoDefault">Diretório destino default</param>
        /// <param name="iAtual"></param>
        /// <param name="iTotal"></param>
        /// <returns></returns>
        public string RemessaCheque(ref int iTotal, ref int iAtual, int max, string strPathArquivoDefault)        
        {
            DateTime HoraInicio = DateTime.Now;
            string strArquivo;
            string strArquvioBk;
            string strArquivoFinal;
            string strPathBackup = string.Empty;
            string strPathTemp = string.Empty;            
            string strPathArquivo = string.Empty;            
            string retorno = string.Empty;

            if (Cancelar)
                throw new Exception("Cancelado aguarda LIBERAR");

            //Seleciona cheques para registro
            iTotal = dEDI.CHEquesTableAdapter.FillRegistrar(dEDI.CHEques, DateTime.Now.AddMinutes(-5));
            if (iTotal == 0)
            {
                retorno = "Remessa (Pagamento Eletrônico): Nenhum pagamento\r\n";
                return retorno;
            }
                                    
            dEDI.TipoEnvioArquivosTableAdapter.Fill(dEDI.TipoEnvioArquivos);            
            
            //Inicia variáveis
            const string Referencia = @"C:\Modulo servidor\TMP\";
            strPathTemp = Referencia;
            if (!Directory.Exists(Referencia + "BKP_PE"))
                Directory.CreateDirectory(Referencia + "BKP_PE");
            strPathBackup = string.Format(@"{0}BKP_PE\{1:yyyyMMdd}\", Referencia, DateTime.Now);
            //strLocal = "Remessa (Pagamento Eletrônico)";

            //Gera remessa por cheque (até a quantidade máxima)
            iAtual = 0;
            int nOk = 0;
            int iInclusao = 0;
            int iExclusao = 0;
            string ArquivosGerados = "";
            foreach (dEDIBoletosNeon.CHEquesRow CHErow in dEDI.CHEques)
            {
                if (Cancelar)
                    throw new Exception("Cancelado aguarda LIBERAR");
                strArquivo = strArquvioBk = strArquivoFinal = string.Empty;
                try
                {                    
                    Cheque Che = new Cheque(CHErow.CHE, Cheque.TipoChave.CHE);

                    if ((Che.CHEStatus.EstaNoGrupo(CHEStatus.Cancelado, CHEStatus.PagamentoCancelado))
                          &&
                        (Che.PECancelamentoStatus == PECancelamentoStatus.AguardaEnvioCancelamentoPE)
                          &&
                        (Che.Vencimento < DateTime.Today)
                      )
                    {
                        Che.AlteraPECancelamentoStatus(PECancelamentoStatus.ProcessadoCancelamentoPE, "Cheque vencido, não precisa cancelar");
                        continue;
                    }

                    //Verifica destino final do arquivo se indicado especificamente para o condomínio (CONRemessa_TEA indicado), se não mantem o indicado na chamada da função
                    //ATualmente só enviamos pela Nexxera                                        
                    /*
                    if (!Che.rowCON.IsCONRemessa_TEANull())
                        strPathArquivo = dEDI.TipoEnvioArquivos.FindByTEA(Che.rowCON.CONRemessa_TEA).TEASaida;                                            
                    else
                        strPathArquivo = strPathArquivoDefault;                    
                    */
                    strPathArquivo = strPathArquivoDefault;
                    //Gera o arquivo (um por registro)
                    ChequeRemessaBra ChequeRemessaBra = null;
                    int SequencialRemessaArquivo = VerificaSequencialRemessaArquivo(strPathBackup);
                    if (Che.Registrar(null, out strArquivo, strPathTemp, SequencialRemessaArquivo))
                    {
                        //Verifica se gerou o arquivo                        
                        if (strArquivo == string.Empty)
                            throw new Exception("Erro ao gerar arquivo: " + ChequeRemessaBra.UltimoErro.Message);
                        strArquivoFinal = strPathArquivo + Path.GetFileName(strArquivo);
                        //Abra a transacao
                        try
                        {
                            VirMSSQL.TableAdapter.AbreTrasacaoSQL("EDIBoletos Arquivo Remessa Cheque - 295", dEDI.CONDOMINIOSTableAdapter);

                            //Atualiza cheques (Status)                        
                            if ((CHEStatus)CHErow.CHEStatus == CHEStatus.Cancelado || (CHEStatus)CHErow.CHEStatus == CHEStatus.PagamentoCancelado)
                            {
                                Che.AlteraPECancelamentoStatus(PECancelamentoStatus.AguardaConfirmacaoCancelamentoBancoPE, string.Format("Cancelamento enviado para processamento, aguardando confirmação ({0})", Path.GetFileName(strArquivo)));
                                iExclusao++;
                            }
                            else
                            {
                                Che.AlteraStatus(CHEStatus.AguardaConfirmacaoBancoPE, string.Format("Enviado para processamento, aguardando cadastro ({0})", Path.GetFileName(strArquivo)));
                                iInclusao++;
                            }

                            //Copia o arquivo para backup
                            strArquvioBk = strArquivo.Replace(strPathTemp, strPathBackup);
                            File.Copy(strArquivo, strArquvioBk);
                            
                            //Atualiza o sequencial no banco bradesco
                            dEDI.BANCOSTableAdapter.IncrementaSequencialRemessaCheque(237);
                            File.Move(strArquivo, strArquivoFinal);
                            ArquivosGerados += string.Format("\r\n     {0}", strArquivoFinal);
                            //Completa a transação
                            VirMSSQL.TableAdapter.CommitSQL();
                            nOk++;
                        }
                        catch (Exception ex)
                        {
                            VirMSSQL.TableAdapter.VircatchSQL(ex);
                            retorno = retorno + string.Format("****Erro 1 **** Remessa (Pagamento Eletrônico):\r\nCHE - {1}\r\nDados do erro\r\n{0}\r\n\r\n", VirExceptionProc.VirExceptionProc.RelatorioDeErro(ex, false, true, false), CHErow.CHE);
                            if ((strArquivo != string.Empty) && (File.Exists(strArquivo)))
                                File.Delete(strArquivo);
                            if ((strArquvioBk != string.Empty) && (File.Exists(strArquvioBk)))
                                File.Delete(strArquvioBk);
                            if ((strArquivoFinal != string.Empty) && (File.Exists(strArquivoFinal)))
                                File.Delete(strArquivoFinal);                             
                        }
                    }
                }
                catch (Exception e)
                {
                    retorno = retorno + string.Format("****Erro 2 **** Remessa (Pagamento Eletrônico):\r\nCHE - {1}\r\nDados do erro\r\n{0}\r\n\r\n", VirExceptionProc.VirExceptionProc.RelatorioDeErro(e,false,true,false), CHErow.CHE);
                    if ((strArquivo != string.Empty) && (File.Exists(strArquivo)))
                        File.Delete(strArquivo);
                    if ((strArquvioBk != string.Empty) && (File.Exists(strArquvioBk)))
                        File.Delete(strArquvioBk);
                    if ((strArquivoFinal != string.Empty) && (File.Exists(strArquivoFinal)))
                        File.Delete(strArquivoFinal);                    
                }
                finally
                {
                    if ((strArquivo != string.Empty) && (File.Exists(strArquivo)))
                        File.Delete(strArquivo);
                }

                iAtual++;
                if (iAtual == max)
                    break;
            };            
            retorno = retorno + string.Format("({0:mm\\:ss\\.fff}) Remessa (Pagamento Eletrônico): Cheques {1}/{2} (máximo {3}) - Registros: {4} / Cancelamentos: {5}{6}\r\n", 
                                              DateTime.Now - HoraInicio,
                                              nOk, 
                                              dEDI.CHEques.Count, 
                                              max, 
                                              iInclusao, 
                                              iExclusao,
                                              ArquivosGerados);            
            return retorno;
        }
        
        /// <summary>
        /// Gera arquivo de remessa tributo (eletronico)
        /// </summary>
        /// <param name="max"></param>
        /// <param name="strPathArquivo"></param>
        /// <param name="OrigemImposto"></param>
        /// <param name="iAtual"></param>
        /// <param name="iTotal"></param>
        /// <returns></returns> 
        public string RemessaTributo(ref int iTotal, ref int iAtual, int max, string strPathArquivo, OrigemImposto OrigemImposto)        
        {
            string strArquivo = string.Empty;
            string strArquivoFinal = string.Empty;
            string strPathBackup = string.Empty;
            string strPathTemp = string.Empty;
            string retorno = string.Empty;

            if (Cancelar)
                throw new Exception("Cancelado aguarda LIBERAR");

            //Seleciona tributos para registro            
            int nTributos = dEDI.GPSTableAdapter.FillRegistrar(dEDI.GPS, (int)OrigemImposto, DateTime.Now.AddMinutes(-5));
            int nTributosF = dEDI.GPSTableAdapterF.FillRegistrar(dEDI.GPS, (int)OrigemImposto, DateTime.Now.AddMinutes(-5));            
            if (nTributos + nTributosF == 0)
            {
                retorno = string.Format("Remessa (Tributo Eletrônico): Nenhum tributo {0} {1}\r\n", nTributos, nTributosF);
                return retorno;
            }

            //Inicia variaveis
            const string Referencia = @"C:\Modulo servidor\TMP\";
            strPathTemp = Referencia;
            if (!Directory.Exists(Referencia + "BKP_TE"))
                Directory.CreateDirectory(Referencia + "BKP_TE");
            strPathBackup = string.Format(@"{0}BKP_TE\{1:yyyyMMdd}\", Referencia, DateTime.Now);   

            //Inicia remessa
            TributoRemessaBra TributoRemessaBra = new TributoRemessaBra();
            TributoRemessaBra.IniciaRemessa();

            //Seta propriedades gerais na remessa
            TributoRemessaBra.SequencialRemessaArquivo = VerificaSequencialRemessaArquivo(strPathBackup);

            //Inicia transacao
            iAtual = 0;
            int iInclusao = 0;
            //*** MRC - INICIO - TRIBUTO ELETRONICO (18/08/2015 10:00) ***
            int iExclusao = 0;
            bool GPSInclusao = true;
            //*** MRC - TERMINO - TRIBUTO ELETRONICO (18/08/2015 10:00) ***
            int GPS = 0;
            int GPSCON = 0;
            int GPSEMP = 0;
            try
            {
                //Abra a transacao
                VirMSSQL.TableAdapter.AbreTrasacaoSQLDupla("EDIBoletos Arquivo RemessaTributos - 484");
                //VirMSSQL.TableAdapter.AbreTrasacaoSQL("EDIBoletos Arquivo RemessaTributos - 449", dEDI.CONDOMINIOSTableAdapter, dEDI.GPSTableAdapter, dEDI.PAGamentosTableAdapter);
                dEDI.CONDOMINIOSTableAdapter.EmbarcaEmTransST();
                dEDI.GPSTableAdapter.EmbarcaEmTransST();
                dEDI.PAGamentosTableAdapter.EmbarcaEmTransST();

                dEDI.CONDOMINIOSTableAdapterF.EmbarcaEmTransST();
                dEDI.GPSTableAdapterF.EmbarcaEmTransST();

                //Gera remessa unica ordenada por condominio e tributo (ate a quantidade maxima)
                foreach (dEDIBoletosNeon.GPSRow GPSrow in dEDI.GPS)
                {
                    if (Cancelar)
                        throw new Exception("Cancelado aguarda LIBERAR");

                    //*** MRC - INICIO - TRIBUTO ELETRONICO (18/08/2015 10:00) ***
                    //Pega a GPS que vai tratar e o status a ser tratado
                    GPS = GPSrow.GPS;
                    GPSInclusao = (GPSrow.GPSStatus == (int)StatusArquivo.Aguardando_Envio_Exclusao) ? false : true;
                    //*** MRC - TERMINO - TRIBUTO ELETRONICO (18/08/2015 10:00) ***

                    //Verifica se esta tratando novo condominio
                    int intSequenciaRemessaTributos = 0;
                    if ((GPSCON != GPSrow.GPS_CON) || (GPSEMP != GPSrow.EMP))
                    {
                        //Verifica se existia anterior
                        if (GPSCON != 0)
                        {
                            //Fecha condominio na remessa (Grava arquivo)
                            strArquivo = TributoRemessaBra.GravaArquivo(strPathTemp, strArquivo);
                            if (strArquivo == string.Empty)
                                throw new Exception("Erro ao gerar arquivo: " + TributoRemessaBra.UltimoErro.Message);

                            //Atualiza sequencial remessa
                            dEDI.CONDOMINIOSTableAdapterX(GPSEMP).AtualizaSequencialRemessaTributos(TributoRemessaBra.SequencialRemessa, GPSCON);
                        }

                        //Seta propriedades especificas do condominio na remessa
                        TributoRemessaBra.CodComunicacao = Convert.ToInt32(GPSrow.GPSCodComunicacao);
                        TributoRemessaBra.CPFCNPJ = new DocBacarios.CPFCNPJ(GPSrow.GPSCNPJ);
                        TributoRemessaBra.NomeEmpresa = GPSrow.GPSNomeClientePagador;
                        //*** MRC - INICIO - TRIBUTOS (14/01/2016 12:30) ***
                        intSequenciaRemessaTributos = (int)dEDI.CONDOMINIOSTableAdapterX(GPSrow.EMP).VerificaSequencialRemessaTributos(GPSrow.GPS_CON) + 1;
                        TributoRemessaBra.SequencialRemessa = (intSequenciaRemessaTributos <= 99999) ? intSequenciaRemessaTributos : 1;
                        //*** MRC - INICIO - TRIBUTOS (14/01/2016 12:30) ***                        
                        TributoRemessaBra.Tributos.Clear();

                        //Pega o condominio que esta tratando
                        GPSCON = GPSrow.GPS_CON;
                        GPSEMP = GPSrow.EMP;
                    }

                    //Inclui o tributo na remessa
                    TributoEletronico tributo = new TributoEletronico();
                    switch (GPSrow.GPSTipo)
                    {
                        case (int)TipoImposto.PIS:
                        case (int)TipoImposto.IR:                        
                        case (int)TipoImposto.IRPF:
                        case (int)TipoImposto.COFINS:
                        case (int)TipoImposto.CSLL:
                        case (int)TipoImposto.PIS_COFINS_CSLL:                            
                            tributo.TipoTributo = Convert.ToInt32(TributoRemessaBra.TiposTipoTributo.DARF);                            
                            tributo.TipoMovimento = (GPSInclusao) ? "I" : "E";
                            if (!GPSrow.IsGPSIDDocumentoNull()) tributo.IDDocumento = (GPSInclusao) ? "0" : GPSrow.GPSIDDocumento;                            
                            tributo.CPFCNPJContribuinte = new DocBacarios.CPFCNPJ(GPSrow.GPSIdentificador);
                            tributo.NomeContribuinte = GPSrow.GPSNomeIdentificador;
                            tributo.DAAgencia = GPSrow.GPSAgencia;
                            tributo.DADigAgencia = GPSrow.GPSDigAgencia;
                            tributo.DANumConta = GPSrow.GPSConta;
                            tributo.DADigNumConta = GPSrow.GPSDigConta;
                            tributo.DataDebito = GPSrow.GPSDataPagto;
                            tributo.Valor = GPSrow.GPSValorPrincipal;
                            tributo.DataVencimento = GPSrow.GPSVencimento;
                            tributo.CodReceita = GPSrow.GPSCodigoReceita;
                            tributo.PeriodoApuracao = GPSrow.GPSApuracao;
                            tributo.Juros = GPSrow.GPSValorJuros;
                            tributo.Multa = GPSrow.GPSValorMulta;
                            tributo.UsoEmpresa = GPSrow.GPS.ToString();
                            break;
                        case (int)TipoImposto.INSS:                        
                        case (int)TipoImposto.INSSpf:
                        case (int)TipoImposto.INSSpfEmp:
                        case (int)TipoImposto.INSSpfRet:                                                        
                            tributo.TipoTributo = Convert.ToInt32(TributoRemessaBra.TiposTipoTributo.GPS);                            
                            tributo.TipoMovimento = (GPSInclusao) ? "I" : "E";
                            if (!GPSrow.IsGPSIDDocumentoNull()) tributo.IDDocumento = (GPSInclusao) ? "0" : GPSrow.GPSIDDocumento;                            
                            tributo.CPFCNPJContribuinte = new DocBacarios.CPFCNPJ(GPSrow.GPSIdentificador);
                            tributo.NomeContribuinte = GPSrow.GPSNomeIdentificador;
                            tributo.DAAgencia = GPSrow.GPSAgencia;
                            tributo.DADigAgencia = GPSrow.GPSDigAgencia;
                            tributo.DANumConta = GPSrow.GPSConta;
                            tributo.DADigNumConta = GPSrow.GPSDigConta;
                            tributo.DataDebito = GPSrow.GPSDataPagto;
                            tributo.Valor = GPSrow.GPSValorPrincipal;
                            tributo.OutrasEntidades = GPSrow.GPSOutras;
                            tributo.DataVencimento = GPSrow.GPSVencimento;
                            tributo.CodINSS = GPSrow.GPSCodigoReceita;
                            tributo.CompetenciaAno = GPSrow.GPSAno;
                            tributo.CompetenciaMes = GPSrow.GPSMes;
                            tributo.Juros = GPSrow.GPSValorJuros;
                            tributo.EnderecoContribuinte = GPSrow.GPSEndereco;
                            tributo.CEPContribuinte = GPSrow.GPSCEP.ToString();
                            tributo.UsoEmpresa = GPSrow.GPS.ToString();
                            break;                        
                        case (int)TipoImposto.FGTS:
                        case (int)TipoImposto.CONTA_CONSUMO:
                            tributo.TipoTributo = Convert.ToInt32(TributoRemessaBra.TiposTipoTributo.CodigoBarras);                            
                            tributo.TipoMovimento = (GPSInclusao) ? "I" : "E";
                            if (!GPSrow.IsGPSIDDocumentoNull()) tributo.IDDocumento = (GPSInclusao) ? "0" : GPSrow.GPSIDDocumento;                            
                            tributo.CPFCNPJContribuinte = new DocBacarios.CPFCNPJ(GPSrow.GPSIdentificador);
                            tributo.NomeContribuinte = GPSrow.GPSNomeIdentificador;
                            tributo.DAAgencia = GPSrow.GPSAgencia;
                            tributo.DADigAgencia = GPSrow.GPSDigAgencia;
                            tributo.DANumConta = GPSrow.GPSConta;
                            tributo.DADigNumConta = GPSrow.GPSDigConta;
                            tributo.DataDebito = GPSrow.GPSDataPagto;
                            tributo.CodigoBarra = GPSrow.GPSCodigoBarra;
                            tributo.Valor = GPSrow.GPSValorPrincipal;
                            tributo.DataVencimento = GPSrow.GPSVencimento;
                            tributo.UsoEmpresa = GPSrow.GPS.ToString();
                            break;
                        //case (int)TipoImposto.CONTA_CONSUMO:
                            //tributo.TipoTributo = Convert.ToInt32(TributoRemessaBra.TiposTipoTributo.CodigoBarras);
                            /*
                            
                            tributo.TipoMovimento = (GPSInclusao) ? "I" : "E";
                            if (!GPSrow.IsGPSIDDocumentoNull()) tributo.IDDocumento = (GPSInclusao) ? "0" : GPSrow.GPSIDDocumento;                            
                            tributo.CPFCNPJContribuinte = new DocBacarios.CPFCNPJ(GPSrow.GPSIdentificador);
                            tributo.NomeContribuinte = GPSrow.GPSNomeIdentificador;
                            tributo.DAAgencia = GPSrow.GPSAgencia;
                            tributo.DADigAgencia = GPSrow.GPSDigAgencia;
                            tributo.DANumConta = GPSrow.GPSConta;
                            tributo.DADigNumConta = GPSrow.GPSDigConta;
                            tributo.DataDebito = GPSrow.GPSDataPagto;
                            tributo.CodigoBarra = GPSrow.GPSCodigoBarra;
                            tributo.Valor = GPSrow.GPSValorPrincipal;
                            tributo.DataVencimento = GPSrow.GPSVencimento;
                            tributo.UsoEmpresa = GPSrow.GPS.ToString();
                             */
                            //break;
                        case (int)TipoImposto.ISSQN:
                        case (int)TipoImposto.ISS_SA:                        
                        default:
                            throw new NotImplementedException(string.Format("Tipo imposto não implementado = {0}", GPSrow.GPSTipo));                            
                    }
                    TributoRemessaBra.Tributos.Add(tributo);

                    //*** MRC - INICIO - TRIBUTO ELETRONICO (18/08/2015 10:00) ***
                    //Altera o status do tributo e inclui o comentário
                    GPSrow.GPSStatus = (GPSInclusao) ? (int)StatusArquivo.Aguardando_Retorno : (int)StatusArquivo.Aguardando_Retorno_Exclusao;
                    GPSrow.GPSComentario = ((GPSrow.IsGPSComentarioNull()) ? string.Empty : GPSrow.GPSComentario) + String.Format("{0:dd/MM/yy HH:mm:ss} - {1}", DateTime.Now, "Enviado para o banco, aguardando retorno (PTRB" + DateTime.Now.ToString("ddMM") + TributoRemessaBra.SequencialRemessaArquivo.ToString("00") + ".REM)\r\n");
                    //*** MRC - INICIO - TRIBUTO ELETRONICO (18/08/2015 10:00) ***
                    dEDI.GPSTableAdapterFX(GPSrow.EMP).Update(GPSrow);
                    dEDI.GPS.AcceptChanges();

                    //*** MRC - INICIO - TRIBUTO ELETRONICO (27/04/2015 12:00) ***
                    //Verifica se e gps de nota para checagem de status de pagamento e status de cheque
                    if (OrigemImposto == OrigemImposto.Nota)
                    {
                        //Seta status do pagamento
                        //*** MRC - INICIO - TRIBUTO ELETRONICO (18/08/2015 10:00) ***
                        dEDI.PAGamentosTableAdapterX(GPSrow.EMP).SetaStatus((GPSInclusao) ? (int)CHEStatus.AguardaConfirmacaoBancoPE : (int)CHEStatus.Cancelado, GPSrow.GPS);
                        //*** MRC - TERMINO - TRIBUTO ELETRONICO (18/08/2015 10:00) ***
                        dEDI.PAGamentos.AcceptChanges();

                        //Seta status do cheque se todos os pagamentos com mesmo status
                        if (dEDI.PAGamentosTableAdapterX(GPSrow.EMP).VerificaPAGStatusGPS(dEDI.PAGamentos, GPSrow.GPS) == 1)
                        {
                            //*** MRC - INICIO - TRIBUTO ELETRONICO (18/08/2015 10:00) ***
                            dEDI.PAGamentosTableAdapterX(GPSrow.EMP).SetaStatusChequeGPS(dEDI.PAGamentos[0].PAGStatus, String.Format("\r\n{0:dd/MM/yy HH:mm:ss} - {1} enviadas para o banco, aguardando retorno", DateTime.Now, (GPSInclusao) ? "Guias" : "Cancelamento de guias"), GPSrow.GPS);
                            //*** MRC - TERMINO - TRIBUTO ELETRONICO (18/08/2015 10:00) ***
                            dEDI.PAGamentos.AcceptChanges();
                        }
                    }
                    //*** MRC - TERMINO - TRIBUTO ELETRONICO (27/04/2015 12:00) ***

                    //*** MRC - INICIO - TRIBUTO ELETRONICO (18/08/2015 10:00) ***
                    //Incrementa a inclusao ou exclusao
                    if (GPSInclusao)
                        iInclusao++;
                    else
                        iExclusao++;
                    //*** MRC - TERMINO - TRIBUTO ELETRONICO (18/08/2015 10:00) ***    

                    //Verifica maximo de processamento
                    iAtual++;
                    if (iAtual == max)
                        break;
                };

                //Fecha ultimo condominio na remessa (Grava arquivo)
                strArquivo = TributoRemessaBra.GravaArquivo(strPathTemp, strArquivo);
                if (strArquivo == string.Empty)
                    throw new Exception("Erro ao gerar arquivo: " + TributoRemessaBra.UltimoErro.Message);

                //Atualiza sequencial ultimo condominio remessa
                dEDI.CONDOMINIOSTableAdapterX(GPSEMP).AtualizaSequencialRemessaTributos(TributoRemessaBra.SequencialRemessa, GPSCON);

                //Finaliza o arquivo de remessa
                strArquivo = TributoRemessaBra.FinalizaArquivo(strArquivo);
                if (strArquivo == string.Empty)
                    throw new Exception("Erro ao gerar arquivo: " + TributoRemessaBra.UltimoErro.Message);

                //Copia o arquivo para backup
                File.Copy(strArquivo, strArquivo.Replace(strPathTemp, strPathBackup));

                //*** MRC - INICIO (22/12/2014 10:00) ***
                //Copia o arquivo para envio verificando se foi mesmo para a pasta e remove o original ('move' com confirmacao)
                strArquivoFinal = strPathArquivo + Path.GetFileName(strArquivo);
                int i = 1;
                while (!(File.Exists(strArquivoFinal)) && i <= 5)
                {
                    File.Copy(strArquivo, strArquivoFinal);
                    System.Threading.Thread.Sleep(1000);
                    i++;
                }
                File.Delete(strArquivo);
                //*** MRC - TERMINO (22/12/2014 10:00) ***

                //Completa a transacao
                VirMSSQL.TableAdapter.CommitSQLDuplo();
            }
            catch (Exception e)
            {
                //Roolback da transacao
                VirMSSQL.TableAdapter.VircatchSQL(e);
                //VirMSSQL.TableAdapter.ST(VirDB.Bancovirtual.TiposDeBanco.Filial).Vircatch(e);

                //Apaga arquivos de backup e envio
                if (strPathTemp != string.Empty)
                {
                    if (File.Exists(strArquivo.Replace(strPathTemp, strPathBackup)))
                        File.Delete(strArquivo.Replace(strPathTemp, strPathBackup)); if (File.Exists(strArquivoFinal))
                        File.Delete(strArquivoFinal);
                }

                //Retorna erro
                retorno = retorno + string.Format("****Erro**** Remessa (Tributo Eletrônico):\r\nGPS - {1}\r\nDados do erro\r\n{0}\r\n\r\n", VirExceptionProc.VirExceptionProc.RelatorioDeErro(e,true,true), GPS);
            }
            finally
            {
                //Apaga arquivo temporario
                if (File.Exists(strArquivo))
                    File.Delete(strArquivo);
            }
            
            retorno = retorno + string.Format("Remessa (Tributo Eletrônico): Tributo {0}/{1} (máximo {2}) - Registros: {3} / Cancelamentos: {4}\r\n", iAtual, dEDI.GPS.Count, max, iInclusao, iExclusao);

            return retorno;
        }

    }
}
