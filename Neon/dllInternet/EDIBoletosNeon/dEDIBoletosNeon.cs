﻿/*
MR - 29/04/2014 15:25           - Cancelamento de pagamento eletrônico, alteracao no CHEques.FillResgistrar (Alterações indicadas por *** MRC - INICIO - PAG-FOR (29/04/2014 15:25) ***)
MR - 12/11/2014 10:00           - Inclusão de tabelas para pagamento de tributos eletrônico (Alterações indicadas por *** MRC - INICIO - TRIBUTOS (12/11/2014 10:00) ***)
MR - 26/12/2014 10:00           - Inclusão do campo GPSCodigoBarra no GPSTableAdapter
MR - 10/02/2015 12:00           - Inclusão de tabela com os tipo de envio de arquivos e suas configurações de entrada e saída (Alterações indicadas por *** MRC - INICIO - PAG-FOR (10/02/2015 12:00) ***)
MR - 27/04/2015 12:00           - Desconsidera os cheques do tipo "guia de recolhimento - eletrônico" (CHETipo = 27), por se tratar de um cheque de tributos cujo o envio é feito individualmente por pagamentos atravez dos respectivos registros na tabela GPS - ajuste no CHEquesTableAdapter
                                - Separa o processo de remessa de tributos entre os da Folha (GPSOrigem = 1) e os das Notas (GPSOrigem = 2) - ajuste no GPSTableAdapter
                                - Inclusão do PAGamentosTableAdapter (Alterações indicadas por *** MRC - INICIO - TRIBUTO ELETRONICO (27/04/2015 12:00) ***)
MR - 18/08/2015 10:00           - Inclusão dos campos GPSOrigem e GPSIDDocumento no GPSTableAdapter
                                  Ajuste na clausula WHERE do PAGamentosTableAdapter.SetaStatusChequeGPS para status cancelado (0)
MR - 14/01/2016 12:30           - Pega número sequencial de remessa para tributos específico do condominio e não mais o MAX (alteração no VerificaSequencialRemessaTributos do CONDOMINIOSTableAdapter)
MR - 08/06/2016 11:00           - Trata número sequencial de remessa para pagamentos eletrônicos por banco e não mais por condominio (inclusão do IncrementaSequencialRemessaCheque no BANCOSTableAdapter)
*/

namespace EDIBoletosNeon
{


    public partial class dEDIBoletosNeon
    {
        private dEDIBoletosNeonTableAdapters.SuperBOletoTableAdapter superBOletoTableAdapter;
        /// <summary>
        /// TableAdapter padrão para tabela: SuperBOleto
        /// </summary>
        public dEDIBoletosNeonTableAdapters.SuperBOletoTableAdapter SuperBOletoTableAdapter
        {
            get
            {
                if (superBOletoTableAdapter == null)
                {
                    superBOletoTableAdapter = new dEDIBoletosNeonTableAdapters.SuperBOletoTableAdapter();
                    superBOletoTableAdapter.TrocarStringDeConexao();
                };
                return superBOletoTableAdapter;
            }
        }

        private dEDIBoletosNeonTableAdapters.BANCOSTableAdapter bANCOSTableAdapter;
        /// <summary>
        /// TableAdapter padrão para tabela: BANCOS
        /// </summary>
        public dEDIBoletosNeonTableAdapters.BANCOSTableAdapter BANCOSTableAdapter
        {
            get
            {
                if (bANCOSTableAdapter == null)
                {
                    bANCOSTableAdapter = new dEDIBoletosNeonTableAdapters.BANCOSTableAdapter();
                    bANCOSTableAdapter.TrocarStringDeConexao();
                };
                return bANCOSTableAdapter;
            }
        }

        //*** MRC - INICIO - PAG-FOR (2) ***    
        private dEDIBoletosNeonTableAdapters.CHEquesTableAdapter cHEquesTableAdapter;
        /// <summary>
        /// TableAdapter padrão para tabela: CHEques
        /// </summary>
        public dEDIBoletosNeonTableAdapters.CHEquesTableAdapter CHEquesTableAdapter
        {
            get
            {
                if (cHEquesTableAdapter == null)
                {
                    cHEquesTableAdapter = new dEDIBoletosNeonTableAdapters.CHEquesTableAdapter();
                    cHEquesTableAdapter.TrocarStringDeConexao();
                };
                return cHEquesTableAdapter;
            }
        }

        private dEDIBoletosNeonTableAdapters.CONDOMINIOSTableAdapter cONDOMINIOSTableAdapter;
        /// <summary>
        /// TableAdapter padrão para tabela: CONDOMINIOS
        /// </summary>
        public dEDIBoletosNeonTableAdapters.CONDOMINIOSTableAdapter CONDOMINIOSTableAdapter
        {
            get
            {
                if (cONDOMINIOSTableAdapter == null)
                {
                    cONDOMINIOSTableAdapter = new dEDIBoletosNeonTableAdapters.CONDOMINIOSTableAdapter();
                    cONDOMINIOSTableAdapter.TrocarStringDeConexao();
                };
                return cONDOMINIOSTableAdapter;
            }
        }

        private dEDIBoletosNeonTableAdapters.CONDOMINIOSTableAdapter cONDOMINIOSTableAdapterF;
        /// <summary>
        /// TableAdapter padrão para tabela: CONDOMINIOS
        /// </summary>
        public dEDIBoletosNeonTableAdapters.CONDOMINIOSTableAdapter CONDOMINIOSTableAdapterF
        {
            get
            {
                if (cONDOMINIOSTableAdapterF == null)
                {
                    cONDOMINIOSTableAdapterF = new dEDIBoletosNeonTableAdapters.CONDOMINIOSTableAdapter();
                    cONDOMINIOSTableAdapterF.TrocarStringDeConexao(VirDB.Bancovirtual.TiposDeBanco.Filial);
                };
                return cONDOMINIOSTableAdapterF;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="EMP"></param>
        /// <returns></returns>
        public dEDIBoletosNeonTableAdapters.CONDOMINIOSTableAdapter CONDOMINIOSTableAdapterX(int EMP)
        {
            return EMP == Framework.DSCentral.EMP ? CONDOMINIOSTableAdapter : CONDOMINIOSTableAdapterF;
        }

        //*** MRC - TERMINO - PAG-FOR (2) ***

        //*** MRC - INICIO - TRIBUTOS (12/11/2014 10:00) ***
        private dEDIBoletosNeonTableAdapters.GPSTableAdapter gPSTableAdapter;
        private dEDIBoletosNeonTableAdapters.GPSTableAdapter gPSTableAdapterF;
        /// <summary>
        /// TableAdapter padrão para tabela: GPS
        /// </summary>
        public dEDIBoletosNeonTableAdapters.GPSTableAdapter GPSTableAdapter
        {
            get
            {
                if (gPSTableAdapter == null)
                {
                    gPSTableAdapter = new dEDIBoletosNeonTableAdapters.GPSTableAdapter();
                    gPSTableAdapter.TrocarStringDeConexao();
                };
                return gPSTableAdapter;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        public dEDIBoletosNeonTableAdapters.GPSTableAdapter GPSTableAdapterF
        {
            get
            {
                if (gPSTableAdapterF == null)
                {
                    gPSTableAdapterF = new dEDIBoletosNeonTableAdapters.GPSTableAdapter();
                    gPSTableAdapterF.TrocarStringDeConexao(VirDB.Bancovirtual.TiposDeBanco.Filial);
                    gPSTableAdapterF.ClearBeforeFill = false;
                };
                return gPSTableAdapterF;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="EMP"></param>
        /// <returns></returns>
        public dEDIBoletosNeonTableAdapters.GPSTableAdapter GPSTableAdapterFX(int EMP)
        {
            return EMP == Framework.DSCentral.EMP ? GPSTableAdapter : GPSTableAdapterF;
        }

        //*** MRC - TERMINO - TRIBUTOS (12/11/2014 10:00) ***

        //*** MRC - INICIO - TRIBUTO ELETRONICO (27/04/2015 12:00) ***
        private dEDIBoletosNeonTableAdapters.PAGamentosTableAdapter pAGamentosTableAdapter;
        private dEDIBoletosNeonTableAdapters.PAGamentosTableAdapter pAGamentosTableAdapterF;
        /// <summary>
        /// TableAdapter padrão para tabela: PAGamentos
        /// </summary>
        public dEDIBoletosNeonTableAdapters.PAGamentosTableAdapter PAGamentosTableAdapter
        {
            get
            {
                if (pAGamentosTableAdapter == null)
                {
                    pAGamentosTableAdapter = new dEDIBoletosNeonTableAdapters.PAGamentosTableAdapter();
                    pAGamentosTableAdapter.TrocarStringDeConexao();
                };
                return pAGamentosTableAdapter;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        public dEDIBoletosNeonTableAdapters.PAGamentosTableAdapter PAGamentosTableAdapterF
        {
            get
            {
                if (pAGamentosTableAdapterF == null)
                {
                    pAGamentosTableAdapterF = new dEDIBoletosNeonTableAdapters.PAGamentosTableAdapter();
                    pAGamentosTableAdapterF.TrocarStringDeConexao(VirDB.Bancovirtual.TiposDeBanco.Filial);
                };
                return pAGamentosTableAdapterF;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="EMP"></param>
        /// <returns></returns>
        public dEDIBoletosNeonTableAdapters.PAGamentosTableAdapter PAGamentosTableAdapterX(int EMP)
        {
            return EMP == Framework.DSCentral.EMP ? PAGamentosTableAdapter : PAGamentosTableAdapterF;
        }

        //*** MRC - TERMINO - TRIBUTO ELETRONICO (27/04/2015 12:00) ***

        //*** MRC - INICIO (10/02/2015 12:00) ***
        private dEDIBoletosNeonTableAdapters.TipoEnvioArquivosTableAdapter tipoEnvioArquivosTableAdapter;
        /// <summary>
        /// TableAdapter padrão para tabela: TipoEnvioArquivos
        /// </summary>
        public dEDIBoletosNeonTableAdapters.TipoEnvioArquivosTableAdapter TipoEnvioArquivosTableAdapter
        {
            get
            {
                if (tipoEnvioArquivosTableAdapter == null)
                {
                    tipoEnvioArquivosTableAdapter = new dEDIBoletosNeonTableAdapters.TipoEnvioArquivosTableAdapter();
                    tipoEnvioArquivosTableAdapter.TrocarStringDeConexao();
                };
                return tipoEnvioArquivosTableAdapter;
            }
        }
        //*** MRC - TERMINO (10/02/2015 12:00) ***
    }
}


namespace EDIBoletosNeon.dEDIBoletosNeonTableAdapters {
    
    
    public partial class BANCOSTableAdapter {
    }
}
