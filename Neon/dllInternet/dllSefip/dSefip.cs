﻿/*
MR - 03/12/2014 10:00           - Inclusão do campo com cod. de comunicacao para tributos, usando no envio de arquivo para Bradesco (CONCodigoComunicacaoTributos) no CONDOMINIOS
                                - Inclusão do campo cod. de comunicacao (SEFCodComunicacao) no SEFip
*/

using dllVirEnum;
using DocBacarios;

namespace dllSefip {
    [System.Obsolete("centralizar com dAutomaçãoBancaria")]
    public enum nTipoPag
    {
        /// <summary>
        /// 
        /// </summary>
        Invalido = 0,
        /// <summary>
        /// 
        /// </summary>
        Cheque = 1,
        /// <summary>
        /// 
        /// </summary>
        CreditoContaBradesco = 2,
        /// <summary>
        /// 
        /// </summary>        
        CartaoSalarioBradesco = 3,
        /// <summary>
        /// 
        /// </summary>
        CreditoContaSalarioBradesco = 4,
        /// <summary>
        /// 
        /// </summary>
        CreditoContaItau = 5,
        /// <summary>
        /// 
        /// </summary>
        CreditoContaUnibanco = 6,
        /// <summary>
        /// 
        /// </summary>
        Carta = 7,
        /// <summary>
        /// 
        /// </summary>
        EletronicoBradesco = 8,
        /// <summary>
        /// 
        /// </summary>
        EletronicoItau = 9,
        /// <summary>
        /// 
        /// </summary>
        ChequeFilial = 10,
        /// <summary>
        /// 
        /// </summary>
        NaoEmitir = 11
    }


    partial class dSefip
    {
        partial class SefipAcDataTable
        {
        }

        //private static dSefip _dSefipSt;

        /*
        internal int CarregaCondominios(CompontesBasicos.Espera.cEspera ESP = null)
        {
            dSefip.dSefipSt.CONDOMINIOS.Clear();
            if (ESP != null)
                ESP.Espere("Carregando Condomínios Locais");
            System.Windows.Forms.Application.DoEvents();
            dSefip.dSefipSt.CONDOMINIOSTableAdapter.Fill(dSefip.dSefipSt.CONDOMINIOS, Framework.DSCentral.EMP);
            dSefip.dSefipSt.CONDOMINIOSTableAdapterF.ClearBeforeFill = false;
            if (ESP != null)
                ESP.Espere("Carregando Condomínios Filial");
            System.Windows.Forms.Application.DoEvents();
            dSefip.dSefipSt.CONDOMINIOSTableAdapterF.Fill(dSefip.dSefipSt.CONDOMINIOS, Framework.DSCentral.EMPF);
            return dSefip.dSefipSt.CONDOMINIOS.Count;
        }*/

        /// <summary>
        /// 
        /// </summary>
        public System.Collections.Generic.List<string> CONSemSEFIP;

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public bool Recalcular()
        {
            
            if (SEFip.Count == 0)
                return true;
            PAGamentos.Clear();
            PAGamentosTableAdapter.FillByCompet(PAGamentos, SEFip[0].SEFCompetencia);
            PAGamentosTableAdapterF.FillByCompet(PAGamentos, SEFip[0].SEFCompetencia);
            bool OK = true;
            foreach (SEFipRow row in SEFip)
            {
                //if(row.IscalNomeCondominioNull())
                //    row.calNomeCondominio = string.Format("{0}{1}", rowCon.IsCONCodigoFolha1Null() ? "" : rowCon.CONCodigoFolha1 + " ", rowCon.CONNome);
                bool EstaUnchanged = row.RowState == System.Data.DataRowState.Unchanged;
                decimal VSefip = row.IsSEFValorSEFIPNull() ? 0 : row.SEFValorSEFIP;
                decimal VFolha = row.IsSEFValorFolhaNull() ? 0 : row.SEFValorFolha;
                decimal VFolhaOutros = row.IsSEFValorFolhaOutrasNull() ? 0 : row.SEFValorFolhaOutras;
                decimal VTerceiros = row.IsSEFValorNeonNull() ? 0 : row.SEFValorNeon;
                row.Erro = VSefip - VFolha - VFolhaOutros - VTerceiros;
                if (row.Erro == 0)
                    row.SetErroNull();
                if ((!row.IsErroNull()) || (row.SEFTipoPag == (int)nTipoPag.Invalido))
                {
                    row.OK = false;
                    OK = false;
                }
                else
                    row.OK = true;
                if(EstaUnchanged)
                    row.AcceptChanges();
            }            
            return OK;
        }

        /*
        /// <summary>
        /// dataset estático:dSefip
        /// </summary>
        public static dSefip dSefipSt
        {
            get
            {
                if (_dSefipSt == null)
                {
                    _dSefipSt = new dSefip();
                    //if (_dSefipSt.CONDOMINIOSTableAdapter.Configurado())
                    //    _dSefipSt.CONDOMINIOSTableAdapter.Fill(_dSefipSt.CONDOMINIOS);
                }
                return _dSefipSt;
            }
        }*/

        private VirEnum virEnumnTipoPag;

        /// <summary>
        /// 
        /// </summary>
        public VirEnum VirEnumnTipoPag
        {
            get
            {
                if (virEnumnTipoPag == null)
                {
                    virEnumnTipoPag = new VirEnum(typeof(nTipoPag));
                    virEnumnTipoPag.GravaNomes(nTipoPag.CartaoSalarioBradesco, "Cartao Salário Bradesco");
                    virEnumnTipoPag.GravaNomes(nTipoPag.CreditoContaBradesco, "Crédito Conta Bradesco");
                    virEnumnTipoPag.GravaNomes(nTipoPag.CreditoContaSalarioBradesco, "Crédito Conta Salário Bradesco");
                    virEnumnTipoPag.GravaNomes(nTipoPag.Cheque, "Cheque");
                    virEnumnTipoPag.GravaNomes(nTipoPag.CreditoContaItau, "Crédito Conta Itau");
                    virEnumnTipoPag.GravaNomes(nTipoPag.CreditoContaUnibanco, "Crédito Conta Unibanco");
                    virEnumnTipoPag.GravaNomes(nTipoPag.Carta, "Carta");
                    virEnumnTipoPag.GravaNomes(nTipoPag.EletronicoBradesco, "Elet. Bradesco");
                    virEnumnTipoPag.GravaNomes(nTipoPag.EletronicoItau, "Elet. Itaú");
                    virEnumnTipoPag.GravaNomes(nTipoPag.ChequeFilial, "Cheque Filial");
                    virEnumnTipoPag.GravaNomes(nTipoPag.Invalido, "Inválido");
                    virEnumnTipoPag.GravaNomes(nTipoPag.NaoEmitir, "Não emitir");
                }
                return virEnumnTipoPag;
            }
        }

        #region Table Adapters
		private dSefipTableAdapters.PAGamentosTableAdapter pAGamentosTableAdapter;
        private dSefipTableAdapters.PAGamentosTableAdapter pAGamentosTableAdapterF;

        /// <summary>
        /// TableAdapter padrão para tabela: PAGamentos
        /// </summary>
        public dSefipTableAdapters.PAGamentosTableAdapter PAGamentosTableAdapter
        {
            get
            {
                if (pAGamentosTableAdapter == null)
                {
                    pAGamentosTableAdapter = new dSefipTableAdapters.PAGamentosTableAdapter();
                    pAGamentosTableAdapter.TrocarStringDeConexao();
                };
                return pAGamentosTableAdapter;
            }
        }

        /// <summary>
        /// TableAdapter padrão para tabela: PAGamentos
        /// </summary>
        public dSefipTableAdapters.PAGamentosTableAdapter PAGamentosTableAdapterF
        {
            get
            {
                if (pAGamentosTableAdapterF == null)
                {
                    pAGamentosTableAdapterF = new dSefipTableAdapters.PAGamentosTableAdapter();
                    pAGamentosTableAdapterF.TrocarStringDeConexao(VirDB.Bancovirtual.TiposDeBanco.Filial);
                };
                pAGamentosTableAdapterF.ClearBeforeFill = false;
                return pAGamentosTableAdapterF;
            }
        }


        private dSefipTableAdapters.INSSPagoTableAdapter iNSSPagoTableAdapter;
        private dSefipTableAdapters.INSSPagoTableAdapter iNSSPagoTableAdapterF;
        /// <summary>
        /// TableAdapter padrão para tabela: INSSPago
        /// </summary>
        public dSefipTableAdapters.INSSPagoTableAdapter INSSPagoTableAdapter
        {
            get
            {
                if (iNSSPagoTableAdapter == null)
                {
                    iNSSPagoTableAdapter = new dSefipTableAdapters.INSSPagoTableAdapter();
                    iNSSPagoTableAdapter.TrocarStringDeConexao();
                };
                return iNSSPagoTableAdapter;
            }
        }

        /// <summary>
        /// TableAdapter padrão para tabela: INSSPago Filial
        /// </summary>
        public dSefipTableAdapters.INSSPagoTableAdapter INSSPagoTableAdapterF
        {
            get
            {
                if (iNSSPagoTableAdapterF == null)
                {
                    iNSSPagoTableAdapterF = new dSefipTableAdapters.INSSPagoTableAdapter();
                    iNSSPagoTableAdapterF.TrocarStringDeConexao(VirDB.Bancovirtual.TiposDeBanco.Filial);
                    iNSSPagoTableAdapterF.ClearBeforeFill = false;
                };
                return iNSSPagoTableAdapterF;
            }
        }

        
        private dSefipTableAdapters.CONDOMINIOSTableAdapter cONDOMINIOSTableAdapter;
        /// <summary>
        /// TableAdapter padrão para tabela: CONDOMINIOS
        /// </summary>
        
        public dSefipTableAdapters.CONDOMINIOSTableAdapter CONDOMINIOSTableAdapter
        {
            get
            {
                if (cONDOMINIOSTableAdapter == null)
                {
                    cONDOMINIOSTableAdapter = new dSefipTableAdapters.CONDOMINIOSTableAdapter();
                    cONDOMINIOSTableAdapter.TrocarStringDeConexao();
                };
                return cONDOMINIOSTableAdapter;
            }
        }

        private dSefipTableAdapters.CONDOMINIOSTableAdapter cONDOMINIOSTableAdapterF;

        /// <summary>
        /// TableAdapter padrão para tabela: CONDOMINIOS (Filial)
        /// </summary>               
        public dSefipTableAdapters.CONDOMINIOSTableAdapter CONDOMINIOSTableAdapterF
        {
            get
            {
                if (cONDOMINIOSTableAdapterF == null)
                {
                    cONDOMINIOSTableAdapterF = new dSefipTableAdapters.CONDOMINIOSTableAdapter();
                    cONDOMINIOSTableAdapterF.TrocarStringDeConexao(VirDB.Bancovirtual.TiposDeBanco.Filial);
                };
                return cONDOMINIOSTableAdapterF;
            }
        }
                

        private dSefipTableAdapters.SEFipTableAdapter sEFipTableAdapter;
        private dSefipTableAdapters.SEFipTableAdapter sEFipTableAdapterF;

        /// <summary>
        /// TableAdapter padrão para tabela: SEFip
        /// </summary>
        public dSefipTableAdapters.SEFipTableAdapter SEFipTableAdapter
        {
            get
            {
                if (sEFipTableAdapter == null)
                {
                    sEFipTableAdapter = new dSefipTableAdapters.SEFipTableAdapter();
                    sEFipTableAdapter.TrocarStringDeConexao();
                };
                return sEFipTableAdapter;
            }
        }

        
        /// <summary>
        /// TableAdapter padrão para tabela: SEFip
        /// </summary>
        public dSefipTableAdapters.SEFipTableAdapter SEFipTableAdapterF
        {
            get
            {
                if (sEFipTableAdapterF == null)
                {
                    sEFipTableAdapterF = new dSefipTableAdapters.SEFipTableAdapter();
                    sEFipTableAdapterF.TrocarStringDeConexao(VirDB.Bancovirtual.TiposDeBanco.Filial);
                    sEFipTableAdapterF.ClearBeforeFill = false;
                };
                return sEFipTableAdapterF;
            }
        }

        public dSefipTableAdapters.SEFipTableAdapter SEFipTableAdapterX(int EMP)
        {
            return (EMP == Framework.DSCentral.EMP) ? SEFipTableAdapter : SEFipTableAdapterF;
        }

	#endregion

        public int SalvarSefip()
        {
            int Gravados = 0;
            foreach (SEFipRow rowSEF in SEFip)
            {
                if (rowSEF.RowState != System.Data.DataRowState.Unchanged)
                {
                    SEFipTableAdapterX(rowSEF.SEF_EMP).Update(rowSEF);
                    Gravados++;
                }
            }
            return Gravados;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="CON"></param>
        /// <param name="EMP"></param>
        /// <returns></returns>
        public SEFipRow BuscaSEFipRow(int CON,int EMP)
        {
            foreach(SEFipRow rowteste in SEFip)
                if((rowteste.SEF_CON == CON) && (rowteste.SEF_EMP == EMP))
                    return rowteste;
            return null;
        }

        private bool RelatorioFormatado = false;

        /// <summary>
        /// 
        /// </summary>
        public void FormatarRelatorio()
        {
            if (RelatorioFormatado)
                return;
            RelatorioFormatado = true;
            foreach (dSefip.INSSPagoRow rowformatar in INSSPago)
            {
                rowformatar.NomeFormatado = !rowformatar.IsFRNNomeNull() ? rowformatar.FRNNome : !rowformatar.IsPESNomeNull() ? rowformatar.PESNome : "";
                DocBacarios.PIS NIT = null;
                if (!rowformatar.IsFRNRegistroNull())
                    NIT = new DocBacarios.PIS(rowformatar.FRNRegistro);
                else
                    if (!rowformatar.IsPESPisNull())
                        NIT = new DocBacarios.PIS(rowformatar.PESPis);
                if (NIT != null)
                    rowformatar.NITFormatado = NIT.ToString();
                else
                    rowformatar.NITFormatado = "";
                rowformatar.TipoFormatado = rowformatar.IsPAGIMPNull() ? "" : dllImpostoNeon.ImpostoNeon.VirEnumTipoImposto.Descritivo(rowformatar.PAGIMP);
                FrameworkProc.datasets.dCondominiosAtivos.CONDOMINIOSRow rowCON = FrameworkProc.datasets.dCondominiosAtivos.dCondominiosTodosST.CONDOMINIOS.FindByCON(rowformatar.CON);
                if (rowCON != null)
                    rowformatar.calcCONNome = rowCON.CONNome;
                if (rowformatar.IsPESPisNull())
                    rowformatar.Base = rowformatar.NOATotal;
                else
                    if(rowformatar.PAGIMP == 9)
                        rowformatar.Base = System.Math.Round(rowformatar.PAGValor / 0.2M,2);
                    else
                        rowformatar.Base = System.Math.Round(rowformatar.PAGValor / 0.11M,2);
            }
        }

        /// <summary>
        /// Carrega os dados dos condomínios
        /// </summary>
        /// <param name="Forcar"></param>
        public int CarregaCondominios(bool Forcar)
        {
            if(Forcar)
                CONDOMINIOS.Clear();
            if (CONDOMINIOS.Count == 0)
            {
                CONDOMINIOSTableAdapter.Fill(CONDOMINIOS,Framework.DSCentral.EMP);
                CONDOMINIOSTableAdapterF.ClearBeforeFill = false;
                CONDOMINIOSTableAdapterF.Fill(CONDOMINIOS, Framework.DSCentral.EMPF);
            }
            return CONDOMINIOS.Count;
        }
        
    }
}
