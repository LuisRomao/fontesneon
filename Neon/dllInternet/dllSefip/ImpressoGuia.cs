﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using CompontesBasicosProc;
using iTextSharp;
using iTextSharp.text;
using iTextSharp.text.pdf;
using iTextSharp.text.pdf.parser;

namespace dllSefip
{
    /// <summary>
    /// Guia gerada pelo programa da sefip
    /// </summary>
    public class ImpressoGuia
    {
        private string ConteudoPDF;

        private string BuscaParte(string Titulo)
        {
            int Indice = ConteudoPDF.IndexOf(Titulo) + Titulo.Length + 1;
            string retorno = "";
            for (; ; )
            {
                char candidato = ConteudoPDF[Indice++];
                if (candidato.EstaNoGrupo('0', '1', '2', '3', '4', '5', '6', '7', '8', '9', '0', '/', '.', ',','-','+'))
                    retorno += candidato;
                else
                    break;
            }
            return retorno;
        }

        private int BuscaInteiro(string Titulo)
        {
            return int.Parse(BuscaParte(Titulo));
        }

        private decimal BuscaDecimal(string Titulo)
        {
            return decimal.Parse(BuscaParte(Titulo).Replace(".", ""));
        }

        /// <summary>
        /// 
        /// </summary>
        public string Erro;
        /// <summary>
        /// 
        /// </summary>
        public int Codigo = 0;
        /// <summary>
        /// 
        /// </summary>
        public Framework.objetosNeon.Competencia comp;
        /// <summary>
        /// 
        /// </summary>
        public DocBacarios.CPFCNPJ cnpj;
        /// <summary>
        /// 
        /// </summary>
        public decimal ValorINSS;
        /// <summary>
        /// 
        /// </summary>
        public decimal ValorEntidades;
        /// <summary>
        /// 
        /// </summary>
        public decimal ValorMulta;
        /// <summary>
        /// 
        /// </summary>
        public decimal ValorTotal;
        /// <summary>
        /// 
        /// </summary>
        public string NomeArquivo;

        /// <summary>
        /// 
        /// </summary>
        /// <param name="Arquivo"></param>
        public ImpressoGuia(string Arquivo)
        {
            Erro = "";
            NomeArquivo = Arquivo;
            using (PdfReader reader = new PdfReader(Arquivo))
            {
                if (reader.NumberOfPages != 1)
                    Erro = string.Format("Impresso como {0} páginas", reader.NumberOfPages);
                else
                {
                    ConteudoPDF = PdfTextExtractor.GetTextFromPage(reader, 1);
                    Codigo = BuscaInteiro("3 - CÓDIGO DE PAGAMENTO");
                    string compstr = BuscaParte("4 - COMPETÊNCIA");
                    comp = new Framework.objetosNeon.Competencia(int.Parse(compstr.Substring(0, 2)), int.Parse(compstr.Substring(3, 4)));
                    cnpj = new DocBacarios.CPFCNPJ(BuscaParte("5 - IDENTIFICADOR"));
                    ValorINSS = BuscaDecimal("6 - VALOR DO INSS(+)");
                    ValorEntidades = BuscaDecimal("9 - VLR OUTRAS ENTIDADES");
                    ValorMulta = BuscaDecimal("10 - ATUAL.MONETÁRIA/");
                    ValorTotal = BuscaDecimal("11 - VALOR ARRECADADO");

                    //string[] Linhas = ConteudoPDF.Split(new string[] { "\n" }, StringSplitOptions.RemoveEmptyEntries);
                    //foreach (string Linha in Linhas)
                    //    memoEdit1.Text += string.Format("{0}\r\n", Linha);

                    /*
                    Document document = new Document();
                    PdfCopy copy = new PdfCopy(document, new FileStream(outputPath + "\\" + filename, FileMode.Create));
                    document.Open();
                    copy.AddPage(copy.GetImportedPage(reader, pagenumber));
                    document.Close();*/
                }
            }

        }
    }
}
