/*
MR - 03/12/2014 10:00 -           - Inclus�o da coluna cod. de comunicacao como oculta no grid de pagamentos
MR - 29/01/2015 12:00 -           - Retorno do Trib. Eletr. Bradesco apenas como yellow no grid de pagamentos
*/

using System;
using System.Collections.Generic;
using System.Drawing;
using System.Data;
using System.Windows.Forms;
using dllCheques;
using VirMSSQL;
using Framework;
using dllSefipProc;
using CompontesBasicosProc;
using CompontesBasicos;

namespace dllSefip
{
    
    /// <summary>
    /// 
    /// </summary>
    public partial class cSefipPagamentos : ComponenteBase
    {
        /// <summary>
        /// 
        /// </summary>
        public Sefip Sefip1;

        /// <summary>
        /// 
        /// </summary>
        public cSefipPagamentos()
        {
            InitializeComponent();            
        }

        private dllVirEnum.VirEnum virEnumnTipoPag;

        
        /// <summary>
        /// 
        /// </summary>
        public dllVirEnum.VirEnum VirEnumnTipoPag
        {
            get
            {
                if (virEnumnTipoPag == null)
                {
                    virEnumnTipoPag = new dllVirEnum.VirEnum(typeof(nTipoPag));
                    virEnumnTipoPag.GravaNomes(nTipoPag.CartaoSalarioBradesco, "Cartao Sal�rio Bradesco");
                    virEnumnTipoPag.GravaNomes(nTipoPag.CreditoContaBradesco, "Cr�dito Conta Bradesco");
                    virEnumnTipoPag.GravaNomes(nTipoPag.CreditoContaSalarioBradesco, "Cr�dito Conta Sal�rio Bradesco");
                    virEnumnTipoPag.GravaNomes(nTipoPag.Cheque, "Cheque");
                    virEnumnTipoPag.GravaNomes(nTipoPag.CreditoContaItau, "Cr�dito Conta Itau");
                    virEnumnTipoPag.GravaNomes(nTipoPag.CreditoContaUnibanco, "Cr�dito Conta Unibanco");
                    virEnumnTipoPag.GravaNomes(nTipoPag.Carta, "Carta");
                    virEnumnTipoPag.GravaNomes(nTipoPag.EletronicoBradesco, "Elet. Bradesco");
                    virEnumnTipoPag.GravaNomes(nTipoPag.EletronicoItau, "Elet. Ita�");
                    virEnumnTipoPag.GravaNomes(nTipoPag.ChequeFilial, "Cheque Filial");
                    virEnumnTipoPag.GravaNomes(nTipoPag.Invalido, "Inv�lido");
                    virEnumnTipoPag.GravaNomes(nTipoPag.NaoEmitir, "N�o emitir");
                }
                return virEnumnTipoPag;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="Sefip"></param>
        /// <param name="Pai"></param>
        public void ConfiguracaoInicial(Sefip Sefip, cSefipGuia Pai)
        {
            Sefip1 = Sefip;
            cONDOMINIOSBindingSource.DataSource = sEFipBindingSource.DataSource = Sefip1.DSefip;
            VirEnumnTipoPag.CarregaEditorDaGrid(colSEFTipoPag);
            Enumeracoes.virEnumCHEStatus.virEnumCHEStatusSt.CarregaEditorDaGrid(colCHEStatus);
            Enumeracoes.VirEnumPAGTipo.CarregaEditorDaGrid(colCHETipo);
            cSefipGuiaPai = Pai;
        }

        /// <summary>
        /// 
        /// </summary>
        public bool Travar
        {
            set 
            {
                colSEFValorFolha.OptionsColumn.ReadOnly = colSEFValorFolhaOutras.OptionsColumn.ReadOnly = colSEFValorSEFIP.OptionsColumn.ReadOnly = colSEFTipoPag.OptionsColumn.ReadOnly = value;
                panelControl1.Visible = !value;
            }
        }

        

        private void gridView1_RowUpdated(object sender, DevExpress.XtraGrid.Views.Base.RowObjectEventArgs e)
        {
            if (e.Row == null)
                return;
            DataRowView DRV = (DataRowView)e.Row;
            dSefip.SEFipRow rowAlterada = (dSefip.SEFipRow)DRV.Row;
            decimal VSefip = rowAlterada.IsSEFValorSEFIPNull() ? 0 : rowAlterada.SEFValorSEFIP;
            decimal VFolha = rowAlterada.IsSEFValorFolhaNull() ? 0 : rowAlterada.SEFValorFolha;
            decimal VFolhaOutros = rowAlterada.IsSEFValorFolhaOutrasNull() ? 0 : rowAlterada.SEFValorFolhaOutras;
            decimal VTerceiros = rowAlterada.IsSEFValorNeonNull() ? 0 : rowAlterada.SEFValorNeon;
            rowAlterada.Erro = VSefip - VFolha - VFolhaOutros - VTerceiros;
            if (rowAlterada.Erro == 0)
                rowAlterada.SetErroNull();
            if ((!rowAlterada.IsErroNull()) || (rowAlterada.SEFTipoPag == (int)nTipoPag.Invalido))
            {
                rowAlterada.OK = false;
            }
            else
                rowAlterada.OK = true;            
            Sefip1.DSefip.SEFipTableAdapterX(rowAlterada.SEF_EMP).Update(rowAlterada);
            rowAlterada.AcceptChanges();
        }

        private void RowCellStyle(object sender, DevExpress.XtraGrid.Views.Grid.RowCellStyleEventArgs e)
        {

            try
            {
                if (e.RowHandle < 0)
                    return;
                if (e.Column.EstaNoGrupo(colcalNomeCondominio, colCONCnpj, colSEFCodFolha))
                {
                    dSefip.SEFipRow row = (dSefip.SEFipRow)gridView1.GetDataRow(e.RowHandle);
                    if (row == null)
                        return;
                    e.Appearance.BackColor = row.SEF_EMP == DSCentral.EMP ? Color.PaleGreen : Color.LemonChiffon;
                    e.Appearance.ForeColor = Color.Black;
                }
                if (e.Column == colSEFTipoPag)
                {
                    nTipoPag Tipo = (nTipoPag)e.CellValue;


                    switch (Tipo)
                    {
                        case nTipoPag.Invalido:
                            e.Appearance.BackColor = Color.Red;
                            e.Appearance.BackColor2 = Color.Red;
                            break;
                        case nTipoPag.Cheque:
                            e.Appearance.BackColor = Color.Aqua;
                            e.Appearance.BackColor2 = Color.Aqua;
                            break;
                        case nTipoPag.CreditoContaBradesco:
                        case nTipoPag.CreditoContaItau:
                        case nTipoPag.CreditoContaUnibanco:
                        case nTipoPag.CartaoSalarioBradesco:
                        case nTipoPag.CreditoContaSalarioBradesco:
                        case nTipoPag.Carta:
                        case nTipoPag.EletronicoItau:
                            e.Appearance.BackColor = Color.Lime;
                            e.Appearance.BackColor2 = Color.Lime;
                            break;
                        case nTipoPag.EletronicoBradesco:
                            e.Appearance.BackColor = Color.Yellow;
                            e.Appearance.BackColor2 = Color.Yellow;
                            break;
                        case nTipoPag.ChequeFilial:
                            e.Appearance.BackColor = Color.Salmon;
                            e.Appearance.BackColor2 = Color.Salmon;
                            break;
                        case nTipoPag.NaoEmitir:
                            e.Appearance.BackColor = Color.Silver;
                            e.Appearance.BackColor2 = Color.Silver;
                            break;

                        default:
                            break;
                    }
                    e.Appearance.ForeColor = Color.Black;
                }
            }
            catch { }
        }

        private void simpleButton1_Click(object sender, EventArgs e)
        {
            ReiniciarTudo();                                  
        }

        private void ReiniciarTudo()
        {
            if (Sefip1.DSefip.SEFip.Count > 0)
            {
                if (MessageBox.Show("Confirma o re-in�cio", "ATEN��O", MessageBoxButtons.YesNo, MessageBoxIcon.Warning, MessageBoxDefaultButton.Button2) == DialogResult.Yes)
                {
                    try
                    {
                        TableAdapter.AbreTrasacaoSQL("dllSefip cSefipPagamentos - 151", Sefip1.DSefip.SEFipTableAdapter);
                        Sefip1.DSefip.SEFipTableAdapter.preparadelete(Sefip1.DSefip.SEFip[0].SEFCompetencia, DSCentral.EMP);
                        Sefip1.DSefip.SEFipTableAdapter.DeleteQuery(Sefip1.DSefip.SEFip[0].SEFCompetencia);
                        TableAdapter.CommitSQL();
                    }
                    catch (Exception ex)
                    {
                        TableAdapter.VircatchSQL(ex);
                        throw new Exception(ex.Message, ex);
                    };
                    try
                    {
                        TableAdapter.AbreTrasacaoSQL("dllSefip cSefipPagamentos - 162", Sefip1.DSefip.SEFipTableAdapterF);
                        Sefip1.DSefip.SEFipTableAdapterF.preparadelete(Sefip1.DSefip.SEFip[0].SEFCompetencia, DSCentral.EMPF);
                        Sefip1.DSefip.SEFipTableAdapterF.DeleteQuery(Sefip1.DSefip.SEFip[0].SEFCompetencia);
                        TableAdapter.CommitSQL(VirDB.Bancovirtual.TiposDeBanco.Filial);
                    }
                    catch (Exception ex)
                    {
                        TableAdapter.VircatchSQL(ex);
                        throw new Exception(ex.Message, ex);
                    }
                    Sefip1.DSefip.SEFip.Clear();
                }
            }
        }

        private bool UnidadeCorrecaoValor(dSefip.SEFipRow rowcor)
        {
            decimal SEFValorFolhaOutras = rowcor.IsSEFValorFolhaOutrasNull() ? 0 : rowcor.SEFValorFolhaOutras;
            decimal SEFValorFolha = rowcor.IsSEFValorFolhaNull() ? 0 : rowcor.SEFValorFolha;
            decimal SEFValorNeon = rowcor.IsSEFValorNeonNull() ? 0 : rowcor.SEFValorNeon;
            decimal SEFValorSEFIPOutras = rowcor.IsSEFValorSEFIPOutrasNull() ? 0 : rowcor.SEFValorSEFIPOutras;
            if (SEFValorFolhaOutras != SEFValorSEFIPOutras)
                rowcor.SEFValorFolhaOutras = SEFValorSEFIPOutras;
            if ((rowcor.SEFValorSEFIP - SEFValorSEFIPOutras) != SEFValorNeon + SEFValorFolha)
            {
                if (rowcor.SEFValorSEFIP - SEFValorSEFIPOutras - SEFValorNeon >= 0)
                    rowcor.SEFValorFolha = rowcor.SEFValorSEFIP - SEFValorSEFIPOutras - SEFValorNeon;
                else
                {
                    if (Math.Abs(rowcor.Erro) > 1)
                        return false;
                    if (!rowcor.IsSEFValorNeonNull())
                    {
                        if ((rowcor.SEFValorNeon + rowcor.Erro) != 0)
                        {
                            if (rowcor.GetPAGamentosRows().Length > 0)
                            {
                                dSefip.PAGamentosRow rowPag = rowcor.GetPAGamentosRows()[0];
                                foreach (dSefip.PAGamentosRow rowPagTeste in rowcor.GetPAGamentosRows())
                                    if (rowPagTeste.PAGValor > rowPag.PAGValor)
                                        rowPag = rowPagTeste;                                                                
                                VirDB.Bancovirtual.TiposDeBanco Tipo = rowcor.SEF_EMP == DSCentral.EMP ? VirDB.Bancovirtual.TiposDeBanco.SQL : VirDB.Bancovirtual.TiposDeBanco.Filial;
                                try
                                {
                                    decimal ValErro = rowcor.Erro;
                                    TableAdapter.ST(Tipo).AbreTrasacaoLocal("dllSefip cSefipPagamentos - 248", Sefip1.DSefip.SEFipTableAdapterX(rowcor.SEF_EMP));
                                    //TableAdapter.AbreTrasacaoSQL("dllSefip cSefipPagamentos - 165");
                                    TableAdapter.ST(Tipo).ExecutarSQLNonQuery("update pagamentos set PAGValor = @P1 where PAG = @P2", rowPag.PAGValor + rowcor.Erro, rowPag.PAG);
                                    Cheque checor = new Cheque(rowPag.PAG_CHE, Cheque.TipoChave.CHE,FrameworkProc.EMPTProc.EMPTProcStX(rowcor.SEF_EMP));
                                    checor.AjustarTotalPelasParcelas();
                                    rowcor.SetErroNull();
                                    rowcor.OK = true;
                                    rowcor.SEFValorNeon += ValErro;
                                    Sefip1.DSefip.SEFipTableAdapterX(rowcor.SEF_EMP).Update(rowcor);
                                    TableAdapter.ST(Tipo).Commit();
                                    rowPag.PAGValor = rowPag.PAGValor + ValErro;                                    
                                }
                                catch (Exception ex)
                                {
                                    TableAdapter.ST(Tipo).Vircatch(ex);
                                }
                            }
                        }
                    }
                }
            }
            if (rowcor.RowState == DataRowState.Modified)
            {
                rowcor.SetErroNull();
                rowcor.OK = true;
                Sefip1.DSefip.SEFipTableAdapterX(rowcor.SEF_EMP).Update(rowcor);
            }
            return true;
        }

        private void simpleButton2_Click(object sender, EventArgs e)
        {
            foreach (int i in gridView1.GetSelectedRows())
            {
                dSefip.SEFipRow rowcor = (dSefip.SEFipRow)gridView1.GetDataRow(i);                
                if (rowcor != null)
                {
                    UnidadeCorrecaoValor(rowcor);
                    /*
                    decimal SEFValorFolhaOutras = rowcor.IsSEFValorFolhaOutrasNull() ? 0 : rowcor.SEFValorFolhaOutras;
                    decimal SEFValorFolha = rowcor.IsSEFValorFolhaNull() ? 0 : rowcor.SEFValorFolha;
                    decimal SEFValorNeon = rowcor.IsSEFValorNeonNull() ? 0 : rowcor.SEFValorNeon;
                    if (SEFValorFolhaOutras != rowcor.SEFValorSEFIPOutras)
                        rowcor.SEFValorFolhaOutras = rowcor.SEFValorSEFIPOutras;
                    if ((rowcor.SEFValorSEFIP - rowcor.SEFValorSEFIPOutras) != SEFValorNeon + SEFValorFolha)
                    {
                        if (rowcor.SEFValorSEFIP - rowcor.SEFValorSEFIPOutras - SEFValorNeon >= 0)
                            rowcor.SEFValorFolha = rowcor.SEFValorSEFIP - rowcor.SEFValorSEFIPOutras - SEFValorNeon;
                        else
                        {
                            if (rowcor.Erro > 1)
                                continue;
                            if (!rowcor.IsSEFValorNeonNull())
                            {
                                if ((rowcor.SEFValorNeon + rowcor.Erro) > 0)
                                {
                                    if (rowcor.GetPAGamentosRows().Length > 0)
                                    {
                                        dSefip.PAGamentosRow rowPag = rowcor.GetPAGamentosRows()[0];
                                        rowcor.SEFValorNeon += rowcor.Erro;
                                        try
                                        {
                                            TableAdapter.AbreTrasacaoSQL("dllSefip cSefipPagamentos - 165");
                                            TableAdapter.ST().ExecutarSQLNonQuery("update pagamentos set PAGValor = @P1 where PAG = @P2", rowPag.PAGValor + rowcor.Erro, rowPag.PAG);
                                            Cheque checor = new Cheque(rowPag.PAG_CHE, Cheque.TipoChave.CHE);
                                            checor.AjustarTotalPelasParcelas();
                                            TableAdapter.CommitSQL();
                                            rowPag.PAGValor = rowPag.PAGValor + rowcor.Erro;
                                        }
                                        catch (Exception ex)
                                        {
                                            TableAdapter.VircatchSQL(ex);
                                        }
                                    }
                                }
                            }
                        }
                    }
                    if (rowcor.RowState == DataRowState.Modified)
                    {
                        rowcor.SetErroNull();
                        rowcor.OK = true;
                        Sefip1.DSefip.SEFipTableAdapterX(rowcor.SEF_EMP).Update(rowcor);
                    }*/
                }
            }
            //Sefip1.DSefip.SalvarSefip();
            //Sefip1.DSefip.SEFipTableAdapter().Update(Sefip1.DSefip.SEFip);
            //Sefip1.DSefip.Recalcular();
        }

        private void repositoryItemButtonEdit1_ButtonClick(object sender, DevExpress.XtraEditors.Controls.ButtonPressedEventArgs e)
        {
            if (gridControl1.FocusedView != null)
            {
                DevExpress.XtraGrid.Views.Grid.GridView GV = (DevExpress.XtraGrid.Views.Grid.GridView)gridControl1.FocusedView;
                dSefip.PAGamentosRow rowPAG = (dSefip.PAGamentosRow)GV.GetFocusedDataRow();                
                ContaPagar.cNotasCampos Novo = new ContaPagar.cNotasCampos(rowPAG.PAG_NOA,
                                                                           string.Format("{0} - Nota ({1:n2})", rowPAG.SEFipRow.SEFCodFolha, rowPAG.PAGValor),
                                                                           FrameworkProc.EMPTProc.EMPTProcStX(rowPAG.SEFipRow.SEF_EMP));                
            }
        }

        /// <summary>
        /// cSefipGuia Pai
        /// </summary>
        public cSefipGuia cSefipGuiaPai;

        private void simpleButton3_Click(object sender, EventArgs e)
        {
            SaveFileDialog Planilha = new SaveFileDialog() { DefaultExt = ".xlsx"};
            if (Planilha.ShowDialog() == DialogResult.OK)            
                gridView1.ExportToXlsx(Planilha.FileName);            
        }
    }
}
