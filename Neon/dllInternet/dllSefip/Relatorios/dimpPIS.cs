﻿namespace dllSefip.Relatorios {
    
    
    public partial class dimpPIS 
    {
        private dimpPISTableAdapters.INSSporPISTableAdapter iNSSporPISTableAdapter;
        private dimpPISTableAdapters.INSSporPISTableAdapter iNSSporPISTableAdapterF;

        /// <summary>
        /// TableAdapter padrão para tabela: INSSporPIS
        /// </summary>
        public dimpPISTableAdapters.INSSporPISTableAdapter INSSporPISTableAdapter
        {
            get
            {
                if (iNSSporPISTableAdapter == null)
                {
                    iNSSporPISTableAdapter = new dimpPISTableAdapters.INSSporPISTableAdapter();
                    iNSSporPISTableAdapter.TrocarStringDeConexao();
                };
                return iNSSporPISTableAdapter;
            }
        }
        
        /// <summary>
        /// TableAdapter padrão para tabela: INSSporPIS (Filial)
        /// </summary>
        public dimpPISTableAdapters.INSSporPISTableAdapter INSSporPISTableAdapterF
        {
            get
            {
                if (iNSSporPISTableAdapterF == null)
                {
                    iNSSporPISTableAdapterF = new dimpPISTableAdapters.INSSporPISTableAdapter();
                    iNSSporPISTableAdapterF.TrocarStringDeConexao(VirDB.Bancovirtual.TiposDeBanco.Filial);
                };
                return iNSSporPISTableAdapterF;
            }
        }

        /// <summary>
        /// Calcula o acompetência no formato mm/aaaa
        /// </summary>
        public void calculaCompetencia()
        {
            foreach (dimpPIS.INSSporPISRow row in INSSporPIS)
            {
                row.calcCompetencia = string.Format("{0:00}/{1:0000}", row.SEFCompetencia % 100, row.SEFCompetencia / 100);
                if (row.PAGIMP == 9)
                    row.calcBase = row.PAGValor / 0.2M;
            }
        }
    }
}
