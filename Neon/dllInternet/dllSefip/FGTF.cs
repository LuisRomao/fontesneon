﻿/*
MR - 26/12/2014 10:00           - Alteração ao carregar arquivo com dados do FGTS para recolher campos necessários para pagamento de Tributo Eletrônico (Alterações indicadas por *** MRC - INICIO - TRIBUTOS (26/12/2014 10:00) ***)
*/

using System;
using System.Text;
using System.Collections.Generic;
using Framework.objetosNeon;
using CompontesBasicos.ObjetosEstaticos;
using AbstratosNeon;
using System.Windows.Forms;

namespace dllSefip
{
    /// <summary>
    /// 
    /// </summary>
    public class fgts : ABS_DadosPer
    {
        //public List<UnidadeFGTS> Unidades;

        /// <summary>
        /// Carrega os dados
        /// </summary>
        /// <returns></returns>
        public override bool Carrega()
        {
            string arquivo = @"\\neon02\FOLHA\Virtual\fgts.txt";
            if (!CompontesBasicosProc.FuncoesBasicasProc.EstaEmProducao)
            {
                OpenFileDialog op = new OpenFileDialog();
                op.FileName = arquivo;
                if (op.ShowDialog() == DialogResult.OK)
                    arquivo = op.FileName;
                else
                    return false;
            }

            Resultado = new SortedList<int, UnidadeResposta>();
            string original = Limpa(System.IO.File.ReadAllText(arquivo));
            string[] linhas = original.Split(new string[] { "\r\n" }, StringSplitOptions.RemoveEmptyEntries);
            int? codigoFolha = null;
            Competencia Comp = null;
            DateTime? Vencimento = null;
            decimal? Valor = null;
            //*** MRC - INICIO - TRIBUTOS (26/12/2014 10:00) ***          
            string CodigoBarras = null;
            string Inscricao = null;

            bool booEmpresa = false;
            bool booCompet = false; 
            bool booValor = false;
            foreach (string linha in linhas)
            {
                //Layou Novo (cem Codigo de Barras)
                if (linha.Contains("SOCIAL/NOME")) 
                    booEmpresa = true;
                else if (booEmpresa)
                {
                    string[] palavras = linha.Split(new char[] { ' ' }, StringSplitOptions.RemoveEmptyEntries);
                    codigoFolha = int.Parse(StringEdit.Limpa(palavras[0], StringEdit.TiposLimpesa.SoNumeros));
                    booEmpresa = false;
                }
                else if (linha.Contains("COMPET"))
                    booCompet = true;
                else if (booCompet)
                {
                    string[] palavras = linha.Split(new char[] { ' ' }, StringSplitOptions.RemoveEmptyEntries);
                    Comp = Framework.objetosNeon.Competencia.Parse(StringEdit.Limpa(palavras[3], StringEdit.TiposLimpesa.SoNumeros));
                    Vencimento = DateTime.Parse(palavras[4]);
                    Inscricao = StringEdit.Limpa(palavras[2], StringEdit.TiposLimpesa.SoNumeros);
                    booCompet = false;
                }
                else if (linha.Contains("TOTAL A RECOLHER"))
                    booValor = true;
                else if (booValor)
                {
                    string[] palavras = linha.Split(new char[] { ' ' }, StringSplitOptions.RemoveEmptyEntries);
                    Valor = decimal.Parse(StringEdit.Limpa(palavras[2], StringEdit.TiposLimpesa.SoNumeros)) / 100;
                    booValor = false;
                }
                else if (linha.Contains("AUTENTICA"))
                {
                    CodigoBarras = StringEdit.Limpa(linha, StringEdit.TiposLimpesa.SoNumeros);
                    if (Resultado.IndexOfKey(codigoFolha.Value) == -1) 
                        Resultado.Add(codigoFolha.Value, new UnidadeResposta(codigoFolha.Value, Comp, Valor.Value, Vencimento.Value, Inscricao, CodigoBarras));
                    Competencia = Comp;
                    codigoFolha = null;
                    Comp = null;
                    Vencimento = null;
                    Valor = null;
                    CodigoBarras = null;
                    Inscricao = null;
                }
                ////Layou Antigo (sem Codigo de Barras) - Desativado
                //if (linha.Contains("EMPRESA"))
                //{
                //    string[] palavras = linha.Split(new char[] { ' ' }, StringSplitOptions.RemoveEmptyEntries);
                //    string manobra = StringEdit.Limpa(palavras[0], StringEdit.TiposLimpesa.SoNumeros);
                //    codigoFolha = int.Parse(manobra);
                //}
                //else if (linha.Contains("COMPET"))
                //{
                //    string[] palavras = linha.Split(new char[] { ' ' }, StringSplitOptions.RemoveEmptyEntries);
                //    string manobra = StringEdit.Limpa(palavras[0], StringEdit.TiposLimpesa.SoNumeros);
                //    Comp = Framework.objetosNeon.Competencia.Parse(manobra);
                //}
                //else if (linha.Contains("TOTAL A RECOLHER"))
                //{
                //    string[] palavras = linha.Split(new char[] { ' ' }, StringSplitOptions.RemoveEmptyEntries);
                //    Valor = decimal.Parse(StringEdit.Limpa(palavras[0], StringEdit.TiposLimpesa.SoNumeros)) / 100;
                //}
                //else if (linha.Contains("VALIDADE DO C"))
                //{
                //    string[] palavras = linha.Split(new char[] { ' ' }, StringSplitOptions.RemoveEmptyEntries);
                //    string manobra = StringEdit.Limpa(palavras[2], StringEdit.TiposLimpesa.SoNumeros);
                //    int dia = int.Parse(manobra.Substring(0, 2));
                //    int mes = int.Parse(manobra.Substring(2, 2));
                //    int ano = int.Parse(manobra.Substring(4));
                //    Vencimento = new DateTime(ano, mes, dia);
                //    //Unidades.Add(new UnidadeFGTS(codigoFolha.Value, Comp, Valor.Value, Vencimento.Value));
                //    Resultado.Add(codigoFolha.Value, new UnidadeResposta(codigoFolha.Value, Comp, Valor.Value, Vencimento.Value));
                //    Competencia = Comp;
                //    codigoFolha = null;
                //    Comp = null;
                //    Vencimento = null;
                //}
            }
            //*** MRC - INICIO - TRIBUTOS (26/12/2014 10:00) ***            
            return Resultado.Count != 0;
        }

        /// <summary>
        /// Construtor
        /// </summary>
        public fgts():base()
        {
            //Carrega();

            //string teste = "";
            //foreach (UnidadeFGTS un in Unidades)
            //    teste += string.Format("Cond {0} comp {1} Valor {2}\r\n", un.CodFolha, un.Comp, un.Valor);

            //System.Windows.Forms.Clipboard.SetText(teste);
        }

        private string Limpa(string entrada)
        {
            bool pulando = false;
            //bool cancelapulo = false;
            StringBuilder SB = new StringBuilder();
            //string str2 = "";
            foreach (char c in entrada)
                if (!pulando)
                    if (c != '\r')
                        SB.Append(c);
                    //str2 += c;
                    else
                    {
                        pulando = true;
                    }
                else
                    if (c == '\n')
                        SB.Append("\r\n");
                    //str2 += "\r\n";
                    else
                        if (c != ' ')
                        {
                            SB.Append(c);
                            //str2 += c;
                            pulando = false;
                        }
            //return str2;
            return SB.ToString();
        }
    }

    /*
    public class UnidadeFGTS
    { 
        public int CodFolha;
        public Competencia Comp;
        public decimal Valor;
        public DateTime Vencimento;

        public UnidadeFGTS(int _CodFolha,Competencia _Comp,decimal _Valor,DateTime _Vencimento)
        {
            CodFolha = _CodFolha;
            Comp = _Comp;
            Valor = _Valor;
            Vencimento = _Vencimento;
        }
    }
    */
}