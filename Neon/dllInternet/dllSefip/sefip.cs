/*
MR - 03/12/2014 10:00 -           - Salvando os dados adicionais de cod. de comunicacao na tabela SEFip (Altera��es indicadas por *** MRC - INICIO - TRIBUTOS (03/12/2014 10:00) ***)
MR - 29/01/2015 12:00 -           - Seta tipos de pagamento considerando o apenas Tributo Eletr�nico Brandesco novo onde o CONCodigoComunicacaoTributos � obrigatorio (Altera��es indicadas por *** MRC - INICIO - TRIBUTOS (29/01/2015 12:00) ***)
*/

using System;
using ArquivosTXT;
using DocBacarios;
using Framework.objetosNeon;
using VirMSSQL;
using Framework;
using FrameworkProc;
using System.Collections.Generic;
using dllSefipProc;
using System.Threading;
using CompontesBasicosProc;
using CompontesBasicos;
using dllClienteServProc;
using VirEnumeracoes;

namespace dllSefip
{

    internal enum TiposRegistro
    {
        Invalido = -1,
        HeaderArquivo = 0,
        HeaderEmpresa = 10,
        HeaderEmpresaAdd = 12,
        RegistroTrabalhador = 30
    }

    class Reg00 : RegistroBase
    {
        public Reg00(cTabelaTXT _cTabTXT, string Lido)
            : base(_cTabTXT, Lido)
        {
        }

        public Competencia Comp
        {
            get
            {
                return new Framework.objetosNeon.Competencia(Datacompetencia.Month, Datacompetencia.Year);
            }
            set
            {                
                //Competencia comp = (Competencia)value;
                Datacompetencia = new DateTime(value.Ano, value.Mes, 1);
            }
        }

        public DateTime Datacompetencia;

        

        protected override void RegistraMapa()
        {
            //UnidadeMapa Uni;
            base.RegistraMapa();
            RegistraMapa("TipoRegistro", 1, 2, 0);
            RegistraMapa("TipoRemessa", 54, 1, 1);
            RegistraMapa("TipoCNPJ", 55, 1, 1);
            //CPFCNPJ cnpjNeon = new CPFCNPJ(Framework.DSCentral.EMProw.EMPCNPJ);
            CPFCNPJ cnpjNeon = new CPFCNPJ("55055651000170");
            RegistraMapa("CNPJ", 56, 14, cnpjNeon.Valor);
            Sefip.CampoAN(RegistraMapa("Razao", 70, 30, Framework.DSCentral.EMProw.EMPRazao));            
            Sefip.CampoAN(RegistraMapa("Contato", 100, 20, "ANDREA MOREIRA MENDO"));
            Sefip.CampoAN(RegistraMapa("Endereco", 120, 50, "ALAMEDA TEREZA CRISTINA 375"));
            Sefip.CampoAN(RegistraMapa("Bairro", 170, 20, "NOVA PETROPOLIS"));
            RegistraMapa("CEP", 190, 8, 9770330);
            Sefip.CampoAN(RegistraMapa("Cidade", 198, 20, "S B CAMPO"));
            RegistraMapa("UF", 218, 2, "SP");
            RegistraMapa("Fone", 220, 12, 001133767900);
            //RegistraMapa("Email")
            RegistraFortementeTipado("Datacompetencia", 292, 6);
            RegistraMapa("Codigorec", 298, 3, 115);
            RegistraMapa("IndicadorFGTS",301,1,1);
            RegistraMapa("Modalidade", 302, 1, 1);
            RegistraMapa("IndicadorPS", 311, 1, 1);
            RegistraMapa("TipoCNPJVirtual", 327, 1, 1);
            RegistraMapa("CNPJVirtual", 328, 14, 01124173000184);
            RegistraMapa("*", 360, 1, "*");
        }
    }

    internal class Reg10 : RegistroBase
    {
        public Reg10(cTabelaTXT _cTabTXT, string Lido)
            : base(_cTabTXT, Lido)
        {
        }

        private long _CNPJ;
        public long CEP;

        public string CEPstr
        {
            set 
            {
                string manobra = "0";
                foreach (char c in value.ToCharArray())
                {
                    if (char.IsDigit(c))
                        manobra += c;
                    CEP = long.Parse(manobra);
                }
            }
        }

        public long CNPJ
        {
            get 
            {
                return _CNPJ;
            }
            set
            {
                _CNPJ = value;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        public CPFCNPJ cnpj 
        {
            get 
            {
                return new CPFCNPJ(CNPJ);
            }
            set
            {
                CNPJ = value.Valor;
            }
        }

        
        //public Framework.datasets.dCondominiosAtivos.CONDOMINIOSRow rowCON;

        protected override void RegistraMapa()
        {
            base.RegistraMapa();
            RegistraMapa("TipoRegistro", 1, 2, 10);
            RegistraMapa("TipoCNPJ", 3, 1, 1);
            RegistraFortementeTipado("CNPJ", 4, 14);
            RegistraMapa("Zeros1", 18, 36, 0);
            Sefip.CampoAN(RegistraMapa("CONNome", 54, 40,"Nome"));            
            Sefip.CampoAN(RegistraMapa("CONEndereco", 94, 50, "Endereco"));
            Sefip.CampoAN(RegistraMapa("CONBairro", 144, 20, "Bairro"));
            RegistraFortementeTipado("CEP", 164, 8);
            Sefip.CampoAN(RegistraMapa("CIDNome", 172, 20, "cidade"));
            Sefip.CampoAN(RegistraMapa("CIDUf", 192, 2, "SP"));
            RegistraMapa("Fone", 194, 12, 1133767900);
            RegistraMapa("IndEnd", 206, 1, "N");
            RegistraMapa("CNAE", 207, 7, 8112500);
            RegistraMapa("IndCNAE", 214, 1, "N");
            RegistraMapa("RAT", 215, 2, 10);
            RegistraMapa("CodC", 217, 1, 0);
            RegistraMapa("Simples", 218, 1, 1);
            RegistraMapa("FPAS", 219, 3, 566);
            RegistraMapa("CodEnt", 222, 4, 99);
            RegistraMapa("CodGPS", 226, 4, 2100);
            RegistraMapa("Zeros2", 235, 60, 0);
            RegistraMapa("Zeros3", 311, 45, 0);
            RegistraMapa("*", 360, 1, "*");
        }
    }

    internal class Reg30 : RegistroBase
    {
        public Reg30(cTabelaTXT _cTabTXT, string Lido)
            : base(_cTabTXT, Lido)
        {

        }


        public long PIS;

        public string PISstr 
        {
            set 
            {
                string manobra = "0";
                foreach (char c in value.ToCharArray())
                {
                    if (char.IsDigit(c))
                        manobra += c;
                    PIS = long.Parse(manobra);
                }
            }
        }

        private int _CategoriaLida;       

        public int CategoriaLida 
        {
            get { return 13; }
            set
            {
            	_CategoriaLida = value;
            }
        }

        //public int Categoria;
        //private long _CNPJ;
        //public long CNPJ
        //{
        //    get
         //   {
         //       return _CNPJ;
         //   }
         //   set
         //   {
        //        _CNPJ = value;
         //   }
      //  }

    //    public CPFCNPJ cnpj
      //  {
        //    get
          //  {
            //    return new CPFCNPJ(CNPJ);
   //         }
     //       set
       //     {
         //       CNPJ = value.Valor;
           // }
       // }
        //public string Nome;
        //public decimal Valor;
        public string Ocorrencia;

        public TipoImposto TipoImposto 
        {
            set 
            {
                if (value == TipoImposto.INSSpfEmp)
                    Ocorrencia = "05";
                else
                    Ocorrencia = "";
            }
        }

        protected override void RegistraMapa()
        {
            base.RegistraMapa();            
            RegistraMapa("TipoRegistro", 1, 2, 30);
            RegistraMapa("TipoCNPJ", 3, 1, 1);            
            RegistraMapa("CONCNPJ", 4, 14,0);            
            RegistraMapa("PIS", 33, 11,0);
            RegistraFortementeTipado("CategoriaLida", 52, 2);
            Sefip.CampoAN(RegistraMapa("Nome", 54, 70,""));          
            RegistraMapa("CBO", 163, 5, "05101");
            RegistraMapa("ValorBase", 168, 15,0.0M);
            RegistraMapa("Valor13", 183, 15, 0.0M);
            RegistraFortementeTipado("Ocorrencia", 200, 2);
            RegistraMapa("Descontado", 202, 15, 0);
            RegistraMapa("Z2", 217, 15, 0);
            RegistraMapa("Z3", 232, 30, 0);
            RegistraMapa("*", 360, 1, "*");
            //}

        }


    }

    /// <summary>
    /// 
    /// </summary>
    public class Sefip:SefipProc
    {
        private dSefip _dSefip;

        /// <summary>
        /// Dataset internor
        /// </summary>
        public dSefip DSefip
        {
            get {
                if (_dSefip == null)
                    _dSefip = new dSefip();
                return _dSefip; 
            }
            set { _dSefip = value; }
        }

        /// <summary>
        /// 
        /// </summary>
        public string Ultimoerro = "";
        /// <summary>
        /// 
        /// </summary>
        public Competencia Comp;


        #region Arquivo TXT
        internal static void CampoAN(UnidadeMapa Uni)
        {
            if (Uni != null)
            {
                Uni.soAZ09 = true;
                Uni.removeacentos = true;
                Uni.EliminaRepique = 3;
            }
        }

        private cTabelaTXT ArquivoSaida;
        //private RegistroBase RegGeral;
        private Reg00 reg00;
        private Reg30 reg30;
        private Reg10 reg10;
        /// <summary>
        /// 
        /// </summary>
        public string Relatorio;
        /// <summary>
        /// 
        /// </summary>
        public string NomeArquivo;
        /// <summary>
        /// 
        /// </summary>
        public void gerarArquivo()
        {
            //gravanobanco();
            ArquivoSaida = new cTabelaTXT(ModelosTXT.layout);
            ArquivoSaida.Formatodata = FormatoData.yyyyMM;
            NomeArquivo = @"\\Neon02\folha\virtual\re\";
            if (!System.IO.Directory.Exists(NomeArquivo))
                NomeArquivo = @"c:\lixo\";
            NomeArquivo += "sefip.re";
            ArquivoSaida.IniciaGravacao(NomeArquivo);
            reg00 = new Reg00(ArquivoSaida, null);
            reg10 = new Reg10(ArquivoSaida, null);
            reg30 = new Reg30(ArquivoSaida, null);
            reg00.Comp = Comp;
            reg00.GravaLinha();
            long ultimoCnpj = 0;
            bool reg10gravado = false;
            FrameworkProc.datasets.dCondominiosAtivos.CONDOMINIOSRow rowCON = null;
            System.Collections.SortedList Lista = new System.Collections.SortedList();
            foreach (dSefip.SefipAcRow Sefiprow in DSefip.SefipAc)
                Lista.Add(Sefiprow.CONCNPJ.ToString("00000000000000") + Sefiprow.PIS.ToString(), Sefiprow);
            bool PularCondominio = false;
            foreach (System.Collections.DictionaryEntry DE in Lista)
            {
                dSefip.SefipAcRow Sefiprow = (dSefip.SefipAcRow)DE.Value;
                if (ultimoCnpj != Sefiprow.CONCNPJ)
                {
                    ultimoCnpj = Sefiprow.CONCNPJ;
                    reg10gravado = false;
                    rowCON = FrameworkProc.datasets.dCondominiosAtivos.dCondominiosAtivosSTX(Sefiprow.EMP == DSCentral.EMP ? EMPTipo.Local : EMPTipo.Filial).CONDOMINIOS.FindByCON(Sefiprow.CON);
                    if (rowCON == null)
                    {
                        PularCondominio = true;
                        continue;
                    }
                    PularCondominio = false;
                    reg10.CNPJ = Sefiprow.CONCNPJ;
                    //if (reg30.cnpj.Valor == 58163783000104)
                    //    continue;
                    reg10.CEPstr = rowCON.CONCep;
                }
                if (PularCondominio)
                    continue;
                /*
                if (INSSrow.IsFRNRegistroNull() || (INSSrow.FRNRegistro == 0))
                {
                    if (INSSrow.IsPISSindicoNull())
                    {
                        INSSrow.Erro = "sem PIS";
                        continue;
                    }
                    else
                    {
                        INSSrow.FRNRegistro = INSSrow.PISSindico;
                        if (INSSrow.Guias == INSSrow.Notas)
                            INSSrow.NOATotal = INSSrow.Guias * INSSrow.PAGValor / 0.2M;
                        else
                            INSSrow.NOATotal = INSSrow.Guias * INSSrow.PAGValor / 0.31M;
                    }
                }*/
                //if (INSSrow.NOATotal == 0)
                //{
                //    INSSrow.Erro = "Nota com valor 0";
                //    continue;
                //};
                //INSSrow.Calculado = (int)Math.Round((INSSrow.Guias / INSSrow.Notas) * 100 * INSSrow.PAGValor / INSSrow.NOATotal);
                //if ((INSSrow.Calculado != 20) && (INSSrow.Calculado != 31))
                //{
                //    INSSrow.Erro = "Valor incorreto";
                //    continue;
                //};
                //INSSrow.Erro = "ok";

                if ((Sefiprow.Status != "ok") && (Sefiprow.Status != ""))
                    continue;

                Sefiprow.Status = "ok";

                if (!reg10gravado)
                {
                    reg10gravado = true;
                    if (!rowCON.IsCONCodigoFolha1Null() && (rowCON.CONNome.Substring(0, 1) != rowCON.CONCodigoFolha1.ToString().Substring(0, 1)))
                        rowCON.CONNome = string.Format("{0} {1}",rowCON.CONCodigoFolha1,rowCON.CONNome);
                    reg10.GravaLinha(rowCON);
                };


                //reg30.TipoImposto = (INSSrow.Calculado == 31) ? TipoImposto.INSSpf : TipoImposto.INSSpfEmp;
                reg30.TipoImposto = Sefiprow.Retencao ? TipoImposto.INSSpf : TipoImposto.INSSpfEmp;
                //reg30.PIS = INSSrow.FRNRegistro;
                reg30.GravaLinha(Sefiprow);

            }
            ArquivoSaida.GravaLinha("90999999999999999999999999999999999999999999999999999                                                                                                                                                                                                                                                                                                                  *");
            ArquivoSaida.TerminaGravacao();
            
        }
        
        #endregion

        private int TentavasRecuperar = 10;
        private RetornoServidorProcessos Retorno = null;
        private SRH.RHClient RHClient1;
        //private int? PGFPagamentoPeriodicoDestravar = null;

        private void ThreadChamadaAoServidor()
        {
            try
            {                
                Retorno = RHClient1.SefipProc_gravanobancoProc(VirCrip.VirCripWS.Crip(DSCentral.ChaveCriptografar()), dSefipCopia, Comp.CompetenciaBind);
            }
            catch (Exception e)
            {
                Retorno = new RetornoServidorProcessos("Erro interno na chamada", string.Format("Erro interno na chamada:\r\n{0}", e.Message));
                Retorno.ok = false;
                Retorno.TipoDeErro = VirExceptionProc.VirTipoDeErro.efetivo_bug;
            }
        }

        /*
        [Obsolete("unificar RH Notas")]
        public static bool AmbienteDesenvolvimento = false;

        [Obsolete("unificar RH Notas")]
        private static TiposServidorProc _TipoServidoSelecionado = TiposServidorProc.Indefinido;

        [Obsolete("unificar RH Notas")]
        private static TiposServidorProc TipoServidoSelecionado
        {
            get
            {
                if (_TipoServidoSelecionado == TiposServidorProc.Indefinido)
                {
                    if (!AmbienteDesenvolvimento)
                        _TipoServidoSelecionado = FormPrincipalBase.strEmProducao ? TiposServidorProc.Producao : TiposServidorProc.QAS;
                    else
                    {
                        System.Collections.SortedList Alternativas = new System.Collections.SortedList();
                        Alternativas.Add(TiposServidorProc.Simulador, "Simulador");
                        Alternativas.Add(TiposServidorProc.Local, "Seridor local");
                        Alternativas.Add(TiposServidorProc.QAS, "Servidor QAS");
                        Alternativas.Add(TiposServidorProc.Producao, "(cuidado) Servidor Produ��o");
                        Alternativas.Add(TiposServidorProc.SemServidor, "Processo antigo");
                        object oSelecionado;
                        VirInput.Input.Execute("Tipo Sevidor de processos", Alternativas, out oSelecionado, VirInput.Formularios.cRadioCh.TipoSelecao.radio);
                        _TipoServidoSelecionado = (TiposServidorProc)oSelecionado;
                    }
                }
                return _TipoServidoSelecionado;
            }
        }

        [Obsolete("unificar RH Notas")]
        private enum TiposServidorProc
        {
            SemServidor = -1,
            Indefinido = 0,
            Simulador = 1,
            Local = 2,
            QAS = 3,
            Producao = 4
        }*/

        private string URLServidorProcessos
        {
            get
            {
                switch (ClienteServProc.TipoServidoSelecionado)
                {
                    case TiposServidorProc.Local:
                        return "WCFRHL";
                    case TiposServidorProc.QAS:
                        return "WCFRHH";
                    case TiposServidorProc.Producao:
                        return "WCFRHP";
                    case TiposServidorProc.Indefinido:
                    case TiposServidorProc.Simulador:
                    default:
                        return "";
                }
            }
        }

        private dSefip dSefipCopia;

        internal void gravanobanco()
        {

            using (CompontesBasicos.Espera.cEspera ESP = new CompontesBasicos.Espera.cEspera(Chamador, true))
            {

                if (ClienteServProc.TipoServidoSelecionado != TiposServidorProc.SemServidor)
                {

                    ESP.Espere("Gravando");
                    ESP.AtivaGauge(30);
                    System.Windows.Forms.Application.DoEvents();
                    Thread ThreadPeriodicoNota = null;
                    bool terminado = false;
                    int tentativas = 0;
                    dSefipCopia = (dSefip)DSefip.Copy();
                    //dSefipCopia.CONDOMINIOS.Clear();
                    //dSefipCopia.INSSPago.Clear();
                    //dSefipCopia.PAGamentos.Clear();
                    //dSefipCopia.SEFip.Clear();                    
                    while (!terminado && (tentativas < TentavasRecuperar))
                    {
                        tentativas++;
                        if (ClienteServProc.TipoServidoSelecionado == TiposServidorProc.Simulador)
                        {
                            ESP.Espere("ATEN��O: uso do simulador");                            
                            ThreadPeriodicoNota = new Thread(() => Retorno = SimulaSefipProc_gravanobancoProc(Framework.DSCentral.EMP, Framework.DSCentral.USU, dSefipCopia, Comp.CompetenciaBind));
                        }
                        else
                        {
                            RHClient1 = new SRH.RHClient(URLServidorProcessos);
                            ThreadPeriodicoNota = new Thread(new ThreadStart(ThreadChamadaAoServidor));
                        }
                        ThreadPeriodicoNota.Start();
                        DateTime proxima = DateTime.Now.AddSeconds(3);
                        while (ThreadPeriodicoNota.IsAlive)
                        {
                            if (DateTime.Now > proxima)
                            {
                                ESP.Gauge();
                                proxima = DateTime.Now.AddSeconds(1);
                            }
                            System.Windows.Forms.Application.DoEvents();
                        }
                        if (Retorno.ok)                                                    
                            terminado = true;                        
                        else
                        {
                            if (Retorno.TipoDeErro != VirExceptionProc.VirTipoDeErro.recuperavel)
                                 terminado = true;
                            else
                            {
                                ESP.Espere(string.Format("Gravando (Tentativa {0})", tentativas));
                                ESP.AtivaGauge(30);
                                ESP.Gauge(0);
                            }
                        }
                    }
                    if (Retorno.ok)
                    {
                        ESP.Espere("Recuperando dados");
                        System.Windows.Forms.Application.DoEvents();

                        DSefip.PAGamentos.Clear();
                        int Carregados = DSefip.SEFipTableAdapter.FillByCompet(DSefip.SEFip, Comp.CompetenciaBind);
                        Carregados += DSefip.SEFipTableAdapterF.FillByCompet(DSefip.SEFip, Comp.CompetenciaBind);
                        if (Carregados > 0)
                        {
                            DSefip.Recalcular();
                        }

                        //DSefip.PAGamentos.Clear();
                        //DSefip.PAGamentos.Clear();
                        //DSefip.PAGamentosTableAdapter.FillByCompet(DSefip.PAGamentos, Comp.CompetenciaBind);
                        //DSefip.PAGamentosTableAdapterF.FillByCompet(DSefip.PAGamentos, Comp.CompetenciaBind);
                    }
                    else
                    {
                        if (!CompontesBasicosProc.FuncoesBasicasProc.EstaEmProducao)
                        {
                            System.Windows.Forms.Clipboard.SetText(Retorno.Mensagem);
                            System.Windows.Forms.MessageBox.Show(Retorno.Mensagem);
                        }
                        switch (Retorno.TipoDeErro)
                        {
                            case VirExceptionProc.VirTipoDeErro.concorrencia:
                                System.Windows.Forms.MessageBox.Show("Os dados que deveriam ser gravados foram alterados por outro usu�rio.\r\n\r\nDADOS N�O GRAVADOS!!", "ATEN��O");
                                break;
                                //return true;
                            case VirExceptionProc.VirTipoDeErro.efetivo_bug:
                                System.Windows.Forms.MessageBox.Show("Dados n�o gravados!! Erro reportado");
                                VirEmailNeon.EmailDiretoNeon.EmalST.Enviar("luis@virweb.com.br", Retorno.Mensagem, "ERRO NO SERVIDOR DE PROCESSOS");
                                break;
                            case VirExceptionProc.VirTipoDeErro.local:
                                System.Windows.Forms.MessageBox.Show(string.Format("Erro:{0}\r\n\r\nDados n�o gravados!!", Retorno.MensagemSimplificada));
                                break;
                                //return true;
                            case VirExceptionProc.VirTipoDeErro.recuperavel:
                                System.Windows.Forms.MessageBox.Show(string.Format("Erro:{0}\r\n\r\nDados n�o gravados!!", Retorno.MensagemSimplificada));
                                break;
                        }                        
                    }
                }
                else
                {
                    System.Windows.Forms.MessageBox.Show("Processo antigo descontinuado, utilizar o simulado");
                    throw new Exception("Processo antigo descontinuado, utilizar o simulado");
                }                
            }
        }

        /// <summary>
        /// Simula o Servidor de processos para teste
        /// </summary>
        /// <param name="EMP"></param>
        /// <param name="USU"></param>
        /// <param name="DSefip"></param>
        /// <param name="Comp"></param>
        /// <returns></returns>
        internal RetornoServidorProcessos SimulaSefipProc_gravanobancoProc(int EMP, int USU, dSefip DSefip, int Comp)
        {
            RetornoServidorProcessos Retorno = new RetornoServidorProcessos("OK", "OK");
            try
            {
                VirMSSQL.TableAdapter.Servidor_De_Processos = true;
                FrameworkProc.EMPTProc EMPTProcL = new FrameworkProc.EMPTProc(EMP == 1 ? FrameworkProc.EMPTipo.Local : FrameworkProc.EMPTipo.Filial, USU);
                FrameworkProc.EMPTProc EMPTProcF = new FrameworkProc.EMPTProc(EMP == 1 ? FrameworkProc.EMPTipo.Filial : FrameworkProc.EMPTipo.Local, USU);

                SefipProc SefipProc1 = new SefipProc(EMP, USU);
                SefipProc1.EMPTProcL = EMPTProcL;
                SefipProc1.EMPTProcF = EMPTProcF;
                //SefipProc1.gravanobancoProc(DSefip, Comp);
                if (!SefipProc1.gravanobancoProc(DSefip, Comp))
                {
                    Retorno.ok = false;
                    Retorno.Mensagem = "Erro ao gravar: Checar o motivo";
                    Retorno.MensagemSimplificada = "Erro ao gravar";
                    Retorno.TipoDeErro = VirExceptionProc.VirTipoDeErro.efetivo_bug;
                }
                return Retorno;
            }
            catch (Exception e)
            {
                Retorno.ok = false;
                Retorno.Mensagem = VirExceptionProc.VirExceptionProc.RelatorioDeErro(e, true, true, true);
                Retorno.Mensagem = VirExceptionProc.VirExceptionProc.RelatorioDeErro(e, false, false, false);
                Retorno.TipoDeErro = VirExceptionProc.VirExceptionProc.IdentificaTipoDeErro(e);
                return Retorno;
            }
            finally
            {
                VirMSSQL.TableAdapter.Servidor_De_Processos = false;
            }
        }

        private System.Windows.Forms.Control Chamador;


        /// <summary>
        /// Construtor
        /// </summary>
        /// <param name="comp"></param>
        /// <param name="_Chamador"></param>
        public Sefip(Competencia comp, System.Windows.Forms.Control _Chamador)
        {
            Chamador = _Chamador;
            Comp = comp;
            /*
            Acumular();            
            if ((Ultimoerro == "") && (Gerar))
            {
                gerarArquivo();
                gravanobanco();
            }*/
        }


        private DateTime DI;
        private DateTime DF;

        /// <summary>
        /// 
        /// </summary>
        /// <param name="comp"></param>
        /// <returns></returns>
        public bool Acumular(Competencia comp)
        {
            Comp = comp;
            DSefip.CONSemSEFIP = new List<string>();            
            Ultimoerro = "";                        
            using (CompontesBasicos.Espera.cEspera Esp = new CompontesBasicos.Espera.cEspera(Chamador))
            {
                System.Windows.Forms.Application.DoEvents();
                //DSefip.CarregaCondominios(Esp);
                Ultimoerro = "";            
                DSefip.SefipAc.Clear();
                Esp.Espere("Carregando dados dos condom�nios");
                DSefip.CarregaCondominios(true);
                FrameworkProc.datasets.dCondominiosAtivos.CONDOMINIOSRow rowCON = null;
                DI = new DateTime(Comp.Ano, Comp.Mes, 1);
                DF = DI.AddMonths(1).AddDays(-1);                
                DSefip.INSSPago.Clear();
                Esp.Espere("Levantado dados locais");
                DSefip.INSSPagoTableAdapter.Fill(DSefip.INSSPago, DI, DF, Framework.DSCentral.EMP);
                Esp.Espere("Levantado dados filial");
                DSefip.INSSPagoTableAdapterF.Fill(DSefip.INSSPago, DI, DF, Framework.DSCentral.EMPF);
                if (DSefip.INSSPago.Count == 0)                    
                    Ultimoerro = "Nenhum registro";
                else
                {
                    Esp.Espere("Levantado reten��es");
                    Esp.AtivaGauge(DSefip.INSSPago.Count);
                    DateTime TempoAtua = DateTime.Now;
                    int i = 0;
                    foreach (dSefip.INSSPagoRow row in DSefip.INSSPago)
                    {                        
                        i++;
                        if (DateTime.Now > TempoAtua)
                        {
                            Esp.Gauge(i);
                            TempoAtua = DateTime.Now.AddSeconds(3);
                        }
                        long PIS = 0;
                        if (!row.IsFRNRegistroNull())
                            PIS = row.FRNRegistro;
                        else
                            if (!row.IsNOA_PESNull())
                            {
                                if (!row.IsPESPisNull())
                                {
                                    PIS = (new DocBacarios.PIS(row.PESPis)).Valor;                                    
                                }
                                else
                                {
                                    /*
                                    string comando =
    "SELECT     PESSOAS.PESPis\r\n" +
    "FROM         View_SINDICOS INNER JOIN\r\n" +
    "                      PESSOAS ON View_SINDICOS.CDR_PES = PESSOAS.PES\r\n" +
    "WHERE     (View_SINDICOS.CDR_CON = @P1);";
                                    string strPIS = "";
                                    if (TableAdapter.ST(row.CON_EMP == Framework.DSCentral.EMP ? VirDB.Bancovirtual.TiposDeBanco.SQL : VirDB.Bancovirtual.TiposDeBanco.Filial).BuscaEscalar(comando, out strPIS, row.CON))
                                    {
                                        PIS = (new DocBacarios.PIS(strPIS)).Valor;
                                    }*/
                                }
                            }
                        CPFCNPJ cnpj = new CPFCNPJ(row.IsCONCnpjNull() ? "" : row.CONCnpj);
                        
                        dSefip.CONDOMINIOSRow rowCONcomSefip = null;
                        foreach (dSefip.CONDOMINIOSRow rowCONcandidato in DSefip.CONDOMINIOS)
                        {
                            if (rowCONcandidato.CONStatus != 1)
                                continue;
                            if ((!rowCONcandidato.IsCONCnpjNull()) && (rowCONcandidato.CONSefip) && (rowCONcandidato.CONCnpj == cnpj.ToString()) && (rowCONcandidato.CON_EMP == row.CON_EMP))
                            {
                                rowCONcomSefip = rowCONcandidato;
                                break;
                            }
                        };
                        
                        if (rowCONcomSefip == null)
                        {
                            if ((cnpj.Tipo == TipoCpfCnpj.CNPJ) && (!DSefip.CONSemSEFIP.Contains(cnpj.ToString())))
                                DSefip.CONSemSEFIP.Add(cnpj.ToString());
                            continue;
                        }
                        dSefip.SefipAcRow rowSefip = DSefip.SefipAc.FindByCONCNPJPIS(cnpj.Valor, PIS);
                        if (rowSefip == null)
                        {
                            rowSefip = DSefip.SefipAc.NewSefipAcRow();
                            rowSefip.CONCNPJ = cnpj.Valor;

                            rowSefip.PIS = PIS;

                            rowSefip.Status = (PIS == 0) ? "Sem PIS" : "";
                            rowSefip.ValorBase = 0;
                            rowSefip.ValorTerceiros = 0;
                            rowSefip.Retencao = false;
                            rowSefip.CODCON = rowCONcomSefip.CONCodigo;
                            rowSefip.CON = rowCONcomSefip.CON;// row.CON;
                            rowSefip.EMP = rowCONcomSefip.CON_EMP;
                            if (!row.IsFRNNomeNull())
                                rowSefip.Nome = row.FRNNome;
                            else if (!row.IsPESNomeNull())
                                rowSefip.Nome = row.PESNome;
                            rowSefip.Retificar = false;
                            DSefip.SefipAc.AddSefipAcRow(rowSefip);
                        };
                        if (row.IsPAG_SEFNull())
                            rowSefip.Retificar = true;
                        rowSefip.ValorTerceiros += row.PAGValor;
                        rowCON = FrameworkProc.datasets.dCondominiosAtivos.dCondominiosAtivosSTX(row.CON_EMP == Framework.DSCentral.EMP ? EMPTipo.Local : EMPTipo.Filial).CONDOMINIOS.FindByCON(row.CON);
                        if (rowCON == null)
                        {
                            System.Windows.Forms.MessageBox.Show(string.Format("Condom�nio {0} inativo n�o ser� inclu�do", row.CONCodigo));
                            continue;
                        }
                        rowSefip.CONNome = string.Format("{0}{1}", rowCON.IsCONCodigoFolha1Null() ? "" : string.Format("{0:000} - ", rowCON.CONCodigoFolha1), rowCONcomSefip.CONNome);
                        if (!rowSefip.CODCON.Contains(rowCON.CONCodigo))
                            rowSefip.CODCON += string.Format("/{0}",  rowCON.CONCodigo);
                        if ((TipoImposto)row.PAGIMP == TipoImposto.INSSpfEmp)
                            rowSefip.ValorBase += row.PAGValor / 0.2M;
                        else if ((TipoImposto)row.PAGIMP == TipoImposto.INSSpfRet)
                            rowSefip.Retencao = true;
                        else
                            rowSefip.Status = "Erro: reten��o tipos" + row.PAGIMP.ToString();
                        row.Sefip_Chave = rowSefip.Chave;
                        if (PIS == 0)
                        {
                            if (Ultimoerro == "")
                                Ultimoerro = "Registro(s) sem PIS";
                        }
                    }            
                    
                    //Calcular();

                }
            }
            return Ultimoerro == "";
        }

        /*
        public bool Gerar()
        {
            Relatorio = "";
            for (string Lido = Arquivo.ProximaLinha(); Lido != null; Lido = Arquivo.ProximaLinha())
            {
                RegGeral.Decodificalinha(Lido);
                TiposRegistro Tipo;
                try
                {
                    Tipo = (TiposRegistro)RegGeral.Valores["Tipo"];
                }
                catch
                {
                    Tipo = TiposRegistro.Invalido;
                }
                switch (Tipo)
                {
                    case TiposRegistro.HeaderArquivo:
                        Ultimoerro = "Arquivo inv�lido: Registro 00 inesperado";
                        return false;
                        break;
                    case TiposRegistro.RegistroTrabalhador:
                        reg30.Decodificalinha(Lido);
                        //Relatorio += string.Format("Identificado ({0})\r\n", Tipo);
                        //memoEdit1.Text += string.Format("PIS:       {0,30}\r\n", reg30.PIS);
                        //memoEdit1.Text += string.Format("Categoria: {0,30}\r\n", reg30.Categoria);
                        break;
                    case TiposRegistro.HeaderEmpresa:
                        Reg10 reg10 = new Reg10(Arquivo, Lido);
                        Relatorio += string.Format("CNPJ: {0}\r\n", reg10.cnpj);
                        DateTime DI = new DateTime(Comp.Ano,Comp.Mes,1).AddMonths(1);
                        DateTime DF = DI.AddMonths(1).AddDays(-1);
                        int encontrados = dSefip.INSSPagoTableAdapter.Fill(dSefip.INSSPago, DI, DF);
                        Relatorio += string.Format("encontrados: {0}\r\n", encontrados);
                        break;
                    case TiposRegistro.Invalido:
                        Ultimoerro = "Arquivo inv�lido: Registro inesperado\r\n" + Lido;
                        Relatorio += "Arquivo inv�lido: Registro inesperado\r\n" + Lido;
                        return false;
                        break;
                }

                ArquivoSaida.GravaLinha(Lido);
            }
            ArquivoSaida.TerminaGravacao();

            return true;
        }
        */
    }
}