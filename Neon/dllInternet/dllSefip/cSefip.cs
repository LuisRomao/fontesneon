using System;
using CompontesBasicosProc;
using CompontesBasicos;
using Framework.objetosNeon;
using dllImpostoNeon;
using DevExpress.XtraReports.UI;
using System.Drawing;
using FrameworkProc;
using dllSefipProc;

namespace dllSefip
{
    /// <summary>
    /// 
    /// </summary>
    public partial class cSefip : ComponenteBase
    {

        /// <summary>
        /// 
        /// </summary>
        public Sefip Sefip1;

        

        /// <summary>
        /// Construtor
        /// </summary>
        public cSefip()
        {
            InitializeComponent();
            ImpostoNeon.VirEnumTipoImposto.CarregaEditorDaGrid(colPAGIMP);            
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="Sefip"></param>
        /// <param name="Pai"></param>
        public void ConfiguracaoInicial(Sefip Sefip,cSefipGuia Pai)
        {
            Sefip1 = Sefip;
            iNSSPagoBindingSource.DataSource = Sefip1.DSefip;
            cSefipGuiaPai = Pai;
        }

        private Competencia compUsada;

        

        /// <summary>
        /// Carrega os dados sem gerar o arquivo remessa para SEFIP
        /// </summary>
        /// <param name="comp"></param>
        /// <returns></returns>
        public bool Carrega(Competencia comp)
        {
            compUsada = comp;
            return Sefip1.Acumular(comp);
            //NovaSefip = new Sefip(comp, false, this);
            //iNSSPagoBindingSource.DataSource = dSefip1;            
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="comp"></param>
        /// <returns></returns>
        public bool GerarArquivo(Competencia comp)
        {
            compUsada = comp;
            Sefip1.gerarArquivo();
            //NovaSefip = new Sefip(comp, true,this);
            //iNSSPagoBindingSource.DataSource = dSefip.dSefipSt;
            return Sefip1.Ultimoerro == "";            
        }

        
        

        /*
        private void simpleButton1_Click(object sender, EventArgs e)
        {
            if (compUsada != null)
                Carrega(compUsada);
            //GerarArquivo(false);
        }
        */
        /*
        private void simpleButton2_Click(object sender, EventArgs e)
        {
            OpenFileDialog OF = new OpenFileDialog();
            if (OF.ShowDialog() == DialogResult.OK)
            {
                DateTime marca = DateTime.Now;
                Guias Guias1 = new Guias(OF.FileName);
                TimeSpan Delta = DateTime.Now - marca;
                MessageBox.Show(string.Format("Tempo: {0}",Delta.TotalSeconds));
                MessageBox.Show(string.Format("Registros: {0}", Guias1.guias.Count));
                string resposta = "";
                int i = 0;
                foreach (Par par in Guias1.guias)
                {
                    if (i++ > 10)
                        break;
                    resposta += string.Format("CNPJ:{0} Valor(1:n2)",par.cnpj,par.valor);
                }
                MessageBox.Show(string.Format("resposta:\r\n{0}", resposta));
            }
        }*/
        

        private void repositoryItemButtonEdit1_ButtonClick(object sender, DevExpress.XtraEditors.Controls.ButtonPressedEventArgs e)
        {
            if (gridControl1.FocusedView != null)
            {
                DevExpress.XtraGrid.Views.Grid.GridView GV = (DevExpress.XtraGrid.Views.Grid.GridView)gridControl1.FocusedView;
                dSefip.INSSPagoRow rowINSS = (dSefip.INSSPagoRow)GV.GetFocusedDataRow();
                if (rowINSS != null)
                {
                    //EMPTipo EMPTipo = rowINSS.CON_EMP == Framework.DSCentral.EMP ? EMPTipo.Local : EMPTipo.Filial;
                    ContaPagar.cNotasCampos Novo = new ContaPagar.cNotasCampos(rowINSS.NOA, 
                                                                               string.Format("{0} - Nota ({1:n2})", rowINSS.CONCodigo, rowINSS.PAGValor), 
                                                                               EMPTProc.EMPTProcStX(rowINSS.CON_EMP)
                                                                              );
                    /*
                    ContaPagar.cNotasCampos Novo = new ContaPagar.cNotasCampos(new FrameworkProc.EMPTProc(EMPTipo));
                    Novo.pk = rowINSS.NOA;
                    Novo.Titulo = string.Format("{0} - Nota ({1:n2})", rowINSS.CONCodigo, rowINSS.PAGValor);
                    Novo.VirShowModulo(EstadosDosComponentes.JanelasAtivas);                    
                    */

                }
            }                                    
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="cnpj"></param>
        /// <param name="Nome"></param>
        public void imprimeRelatorio(DocBacarios.CPFCNPJ cnpj,string Nome)
        {
            //if (Nome == "")
            //    if (!TableAdapter.ST().BuscaEscalar("select CONNome from condominios where CONCNPJ = @P1 and CONStatus in (1,2,3)", out Nome, cnpj.ToString()))
                    Nome = string.Format("CNPJ:{0}",cnpj);
            ImpSefip ImpSefip1 = new ImpSefip();
            ImpSefip1.SetNome(string.Format("{0}", Nome));
            ImpSefip1.xrcompet.Text = string.Format("{0}", compUsada);
            dSefip dSefip1 = new dSefip();
            DateTime DI = new DateTime(compUsada.Ano, compUsada.Mes, 1);
            DateTime DF = DI.AddMonths(1).AddDays(-1);
            if (dSefip1.INSSPagoTableAdapter.FillByCONCNPJ(dSefip1.INSSPago, DI, DF, cnpj.ToString()) > 0)
            {
                dSefip1.INSSPago.DefaultView.Sort = "calcCONNome";
                RelatorioFormatado = false;
                FormatarRelatorio(dSefip1);
                ImpSefip1.bindingSourceSefip.DataSource = dSefip1;
                //ImpSefip1.bindingSourceSefip.Filter = string.Format("CON = {0}",rowSAR.CON);
                ImpSefip1.CreateDocument();
                ImpSefip1.ShowPreview();
                ImpSefip1.Print();
            }
        }

        private bool RelatorioFormatado = false;

        
        /// <summary>
        /// 
        /// </summary>
        private void FormatarRelatorio(dSefip dSefip1)
        {
            if (RelatorioFormatado)
                return;
            RelatorioFormatado = true;
            foreach (dSefip.INSSPagoRow rowformatar in dSefip1.INSSPago)
            {
                rowformatar.NomeFormatado = !rowformatar.IsFRNNomeNull() ? rowformatar.FRNNome : !rowformatar.IsPESNomeNull() ? rowformatar.PESNome : "";
                DocBacarios.PIS NIT = null;
                if (!rowformatar.IsFRNRegistroNull())
                    NIT = new DocBacarios.PIS(rowformatar.FRNRegistro);
                else
                    if (!rowformatar.IsPESPisNull())
                        NIT = new DocBacarios.PIS(rowformatar.PESPis);
                if (NIT != null)
                    rowformatar.NITFormatado = NIT.ToString();
                else
                    rowformatar.NITFormatado = "";
                rowformatar.TipoFormatado = rowformatar.IsPAGIMPNull() ? "" : dllImpostoNeon.ImpostoNeon.VirEnumTipoImposto.Descritivo(rowformatar.PAGIMP);
                FrameworkProc.datasets.dCondominiosAtivos.CONDOMINIOSRow rowCON = FrameworkProc.datasets.dCondominiosAtivos.dCondominiosTodosST.CONDOMINIOS.FindByCON(rowformatar.CON);
                if (rowCON != null)
                    rowformatar.calcCONNome = rowCON.CONNome;
                if (rowformatar.IsPESPisNull())
                    rowformatar.Base = rowformatar.NOATotal;
                else
                    if(rowformatar.PAGIMP == 9)
                        rowformatar.Base = System.Math.Round(rowformatar.PAGValor / 0.2M,2);
                    else
                        rowformatar.Base = System.Math.Round(rowformatar.PAGValor / 0.11M,2);
            }
        }

        private void repositoryItemButtonEdit2_ButtonClick(object sender, DevExpress.XtraEditors.Controls.ButtonPressedEventArgs e)
        {
            dSefip.SefipAcRow rowSAR = (dSefip.SefipAcRow)gridView2.GetFocusedDataRow();
            if (rowSAR == null)
                return;
            DocBacarios.CPFCNPJ cnpj = new DocBacarios.CPFCNPJ(rowSAR.CONCNPJ);
            imprimeRelatorio(cnpj, rowSAR.CONNome);                                    
        }

        /// <summary>
        /// cSefipGuia Pai
        /// </summary>
        public cSefipGuia cSefipGuiaPai;

        private void gridView2_RowCellStyle(object sender, DevExpress.XtraGrid.Views.Grid.RowCellStyleEventArgs e)
        {
            if (e.RowHandle < 0)
                return;
            dSefip.SefipAcRow row = (dSefip.SefipAcRow)gridView2.GetDataRow(e.RowHandle);
            if (row == null)
                return;
            if (e.Column.EstaNoGrupo(colCONCNPJ1,colCODCON,colCONNome))
            {                                
                e.Appearance.BackColor = row.EMP == Framework.DSCentral.EMP ? System.Drawing.Color.PaleGreen : System.Drawing.Color.LemonChiffon;
                e.Appearance.ForeColor = Color.Black;
            }
            else if(e.Column == colRetificar)
                if ((row.Retificar) && (cSefipGuiaPai.Status == Framework.datasets.AGGStatus.Terminado))
                {
                    e.Appearance.BackColor = Color.Red;
                    e.Appearance.ForeColor = Color.Black;
                }

        }

        private void gridView1_RowCellStyle(object sender, DevExpress.XtraGrid.Views.Grid.RowCellStyleEventArgs e)
        {
            DevExpress.XtraGrid.Views.Grid.GridView GV = (DevExpress.XtraGrid.Views.Grid.GridView)sender;
            dSefip.INSSPagoRow rowPAG = (dSefip.INSSPagoRow)GV.GetFocusedDataRow();
            if (rowPAG == null)
                return;
            if ((rowPAG.IsPAG_SEFNull()) && (cSefipGuiaPai.Status == Framework.datasets.AGGStatus.Terminado))
                e.Appearance.BackColor = Color.Pink;
        }
    }
}
