namespace dllSefip
{
    partial class cSefipGuia
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(cSefipGuia));
            this.BotaoLevantamento = new DevExpress.XtraEditors.SimpleButton();
            this.BotaoRTF = new DevExpress.XtraEditors.SimpleButton();
            this.labelControl1 = new DevExpress.XtraEditors.LabelControl();
            this.openFileDialog = new System.Windows.Forms.OpenFileDialog();
            this.BotaoGuias = new DevExpress.XtraEditors.SimpleButton();
            this.BotaoGera = new DevExpress.XtraEditors.SimpleButton();
            this.panelControlMenu = new DevExpress.XtraEditors.PanelControl();
            this.Button_Gera1 = new DevExpress.XtraEditors.SimpleButton();
            this.simpleButton10 = new DevExpress.XtraEditors.SimpleButton();
            this.BotaoImprimir = new DevExpress.XtraEditors.SimpleButton();
            this.BotaoGeraArquivo = new DevExpress.XtraEditors.SimpleButton();
            this.simpleButton6 = new DevExpress.XtraEditors.SimpleButton();
            this.panelControlSuperior = new DevExpress.XtraEditors.PanelControl();
            this.simpleButton1 = new DevExpress.XtraEditors.SimpleButton();
            this.BTCalcular = new DevExpress.XtraEditors.SimpleButton();
            this.cBotaoImpNeon1 = new VirBotaoNeon.cBotaoImpNeon();
            this.simpleButton7 = new DevExpress.XtraEditors.SimpleButton();
            this.BFechar = new DevExpress.XtraEditors.SimpleButton();
            this.chMenu = new DevExpress.XtraEditors.CheckEdit();
            this.TxStatus = new DevExpress.XtraEditors.LabelControl();
            this.cCompet1 = new Framework.objetosNeon.cCompet();
            this.xtraTabControl1 = new DevExpress.XtraTab.XtraTabControl();
            this.xtraTabPageSefip = new DevExpress.XtraTab.XtraTabPage();
            this.cSefipPagamentos1 = new dllSefip.cSefipPagamentos();
            this.xtraTabPageRetencoes = new DevExpress.XtraTab.XtraTabPage();
            this.cSefip2 = new dllSefip.cSefip();
            this.xtraTabPageHistorico = new DevExpress.XtraTab.XtraTabPage();
            this.memoEdit1 = new DevExpress.XtraEditors.MemoEdit();
            this.panelControl1 = new DevExpress.XtraEditors.PanelControl();
            this.simpleButton5 = new DevExpress.XtraEditors.SimpleButton();
            this.dSefip1 = new dllSefipProc.dSefip();
            ((System.ComponentModel.ISupportInitialize)(this.StyleController_F)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControlMenu)).BeginInit();
            this.panelControlMenu.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.panelControlSuperior)).BeginInit();
            this.panelControlSuperior.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.chMenu.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xtraTabControl1)).BeginInit();
            this.xtraTabControl1.SuspendLayout();
            this.xtraTabPageSefip.SuspendLayout();
            this.xtraTabPageRetencoes.SuspendLayout();
            this.xtraTabPageHistorico.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.memoEdit1.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl1)).BeginInit();
            this.panelControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dSefip1)).BeginInit();
            this.SuspendLayout();
            // 
            // ToolTipController_F
            // 
            this.ToolTipController_F.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.ToolTipController_F.Appearance.ForeColor = System.Drawing.Color.Black;
            this.ToolTipController_F.Appearance.Options.UseBackColor = true;
            this.ToolTipController_F.Appearance.Options.UseFont = true;
            this.ToolTipController_F.Appearance.Options.UseForeColor = true;
            this.ToolTipController_F.Appearance.Options.UseTextOptions = true;
            this.ToolTipController_F.Appearance.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            // 
            // StyleController_F
            // 
            this.StyleController_F.AppearanceDisabled.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this.StyleController_F.AppearanceDisabled.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this.StyleController_F.AppearanceDisabled.Options.UseBackColor = true;
            this.StyleController_F.AppearanceDisabled.Options.UseForeColor = true;
            this.StyleController_F.AppearanceFocused.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.StyleController_F.AppearanceFocused.Options.UseBackColor = true;
            // 
            // BotaoLevantamento
            // 
            this.BotaoLevantamento.Appearance.Font = new System.Drawing.Font("Tahoma", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BotaoLevantamento.Appearance.Options.UseFont = true;
            this.BotaoLevantamento.Appearance.Options.UseTextOptions = true;
            this.BotaoLevantamento.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.BotaoLevantamento.Location = new System.Drawing.Point(5, 21);
            this.BotaoLevantamento.Name = "BotaoLevantamento";
            this.BotaoLevantamento.Size = new System.Drawing.Size(352, 50);
            this.BotaoLevantamento.TabIndex = 0;
            this.BotaoLevantamento.Text = "1 - Levantamento sistema Neon";
            this.BotaoLevantamento.Click += new System.EventHandler(this.simpleButton1_Click);
            // 
            // BotaoRTF
            // 
            this.BotaoRTF.Appearance.Font = new System.Drawing.Font("Tahoma", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BotaoRTF.Appearance.Options.UseFont = true;
            this.BotaoRTF.Appearance.Options.UseTextOptions = true;
            this.BotaoRTF.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.BotaoRTF.Location = new System.Drawing.Point(5, 133);
            this.BotaoRTF.Name = "BotaoRTF";
            this.BotaoRTF.Size = new System.Drawing.Size(352, 50);
            this.BotaoRTF.TabIndex = 1;
            this.BotaoRTF.Text = "3 - Carregar RTF Folha";
            this.BotaoRTF.Click += new System.EventHandler(this.simpleButton2_Click);
            // 
            // labelControl1
            // 
            this.labelControl1.Appearance.Font = new System.Drawing.Font("Tahoma", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl1.Appearance.ForeColor = System.Drawing.Color.Red;
            this.labelControl1.Appearance.Options.UseFont = true;
            this.labelControl1.Appearance.Options.UseForeColor = true;
            this.labelControl1.Location = new System.Drawing.Point(416, 72);
            this.labelControl1.Name = "labelControl1";
            this.labelControl1.Size = new System.Drawing.Size(71, 18);
            this.labelControl1.TabIndex = 3;
            this.labelControl1.Text = "labelCont";
            this.labelControl1.Visible = false;
            // 
            // openFileDialog
            // 
            this.openFileDialog.DefaultExt = "rtf";
            this.openFileDialog.Filter = "Formato Rich Text|*.rtf";
            this.openFileDialog.Title = "Selecione um arquivo";
            // 
            // BotaoGuias
            // 
            this.BotaoGuias.Appearance.Font = new System.Drawing.Font("Tahoma", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BotaoGuias.Appearance.Options.UseFont = true;
            this.BotaoGuias.Appearance.Options.UseTextOptions = true;
            this.BotaoGuias.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.BotaoGuias.Location = new System.Drawing.Point(5, 189);
            this.BotaoGuias.Name = "BotaoGuias";
            this.BotaoGuias.Size = new System.Drawing.Size(352, 50);
            this.BotaoGuias.TabIndex = 4;
            this.BotaoGuias.Text = "4 - Guias Sefip";
            this.BotaoGuias.Click += new System.EventHandler(this.simpleButton3_Click);
            // 
            // BotaoGera
            // 
            this.BotaoGera.Appearance.Font = new System.Drawing.Font("Tahoma", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BotaoGera.Appearance.Options.UseFont = true;
            this.BotaoGera.Appearance.Options.UseTextOptions = true;
            this.BotaoGera.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.BotaoGera.Location = new System.Drawing.Point(5, 245);
            this.BotaoGera.Name = "BotaoGera";
            this.BotaoGera.Size = new System.Drawing.Size(352, 50);
            this.BotaoGera.TabIndex = 5;
            this.BotaoGera.Text = "5 - Gerar Cheques/Eletrônicos";
            this.BotaoGera.Click += new System.EventHandler(this.GerarTodos_Click);
            // 
            // panelControlMenu
            // 
            this.panelControlMenu.Controls.Add(this.Button_Gera1);
            this.panelControlMenu.Controls.Add(this.simpleButton10);
            this.panelControlMenu.Controls.Add(this.BotaoImprimir);
            this.panelControlMenu.Controls.Add(this.BotaoGeraArquivo);
            this.panelControlMenu.Controls.Add(this.simpleButton6);
            this.panelControlMenu.Controls.Add(this.BotaoLevantamento);
            this.panelControlMenu.Controls.Add(this.BotaoGera);
            this.panelControlMenu.Controls.Add(this.BotaoRTF);
            this.panelControlMenu.Controls.Add(this.BotaoGuias);
            this.panelControlMenu.Dock = System.Windows.Forms.DockStyle.Left;
            this.panelControlMenu.Location = new System.Drawing.Point(0, 95);
            this.panelControlMenu.Name = "panelControlMenu";
            this.panelControlMenu.Size = new System.Drawing.Size(364, 712);
            this.panelControlMenu.TabIndex = 6;
            // 
            // Button_Gera1
            // 
            this.Button_Gera1.Appearance.Font = new System.Drawing.Font("Tahoma", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Button_Gera1.Appearance.Options.UseFont = true;
            this.Button_Gera1.Appearance.Options.UseTextOptions = true;
            this.Button_Gera1.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.Button_Gera1.Location = new System.Drawing.Point(5, 657);
            this.Button_Gera1.Name = "Button_Gera1";
            this.Button_Gera1.Size = new System.Drawing.Size(352, 50);
            this.Button_Gera1.TabIndex = 10;
            this.Button_Gera1.Text = "Gerar Cheques/Eletrônicos (1)";
            this.Button_Gera1.Visible = false;
            this.Button_Gera1.Click += new System.EventHandler(this.GerarUm);
            // 
            // simpleButton10
            // 
            this.simpleButton10.Appearance.Font = new System.Drawing.Font("Tahoma", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.simpleButton10.Appearance.Options.UseFont = true;
            this.simpleButton10.Appearance.Options.UseTextOptions = true;
            this.simpleButton10.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.simpleButton10.Location = new System.Drawing.Point(5, 538);
            this.simpleButton10.Name = "simpleButton10";
            this.simpleButton10.Size = new System.Drawing.Size(352, 50);
            this.simpleButton10.TabIndex = 9;
            this.simpleButton10.Text = "Imprimir Guia";
            this.simpleButton10.Click += new System.EventHandler(this.simpleButton10_Click);
            // 
            // BotaoImprimir
            // 
            this.BotaoImprimir.Appearance.Font = new System.Drawing.Font("Tahoma", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BotaoImprimir.Appearance.Options.UseFont = true;
            this.BotaoImprimir.Appearance.Options.UseTextOptions = true;
            this.BotaoImprimir.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.BotaoImprimir.Location = new System.Drawing.Point(5, 301);
            this.BotaoImprimir.Name = "BotaoImprimir";
            this.BotaoImprimir.Size = new System.Drawing.Size(352, 50);
            this.BotaoImprimir.TabIndex = 8;
            this.BotaoImprimir.Text = "6 - Imprimir Guias";
            this.BotaoImprimir.Click += new System.EventHandler(this.simpleButton9_Click);
            // 
            // BotaoGeraArquivo
            // 
            this.BotaoGeraArquivo.Appearance.Font = new System.Drawing.Font("Tahoma", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BotaoGeraArquivo.Appearance.Options.UseFont = true;
            this.BotaoGeraArquivo.Appearance.Options.UseTextOptions = true;
            this.BotaoGeraArquivo.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.BotaoGeraArquivo.Location = new System.Drawing.Point(5, 77);
            this.BotaoGeraArquivo.Name = "BotaoGeraArquivo";
            this.BotaoGeraArquivo.Size = new System.Drawing.Size(352, 50);
            this.BotaoGeraArquivo.TabIndex = 7;
            this.BotaoGeraArquivo.Text = "2 - Arquivo para SEFIP";
            this.BotaoGeraArquivo.Click += new System.EventHandler(this.simpleButton8_Click);
            // 
            // simpleButton6
            // 
            this.simpleButton6.Appearance.Font = new System.Drawing.Font("Tahoma", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.simpleButton6.Appearance.Options.UseFont = true;
            this.simpleButton6.Appearance.Options.UseTextOptions = true;
            this.simpleButton6.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.simpleButton6.Location = new System.Drawing.Point(5, 482);
            this.simpleButton6.Name = "simpleButton6";
            this.simpleButton6.Size = new System.Drawing.Size(352, 50);
            this.simpleButton6.TabIndex = 6;
            this.simpleButton6.Text = "Relat. para condomínios sem SEFIP";
            this.simpleButton6.Click += new System.EventHandler(this.simpleButton6_Click);
            // 
            // panelControlSuperior
            // 
            this.panelControlSuperior.Appearance.Font = new System.Drawing.Font("Tahoma", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.panelControlSuperior.Appearance.Options.UseFont = true;
            this.panelControlSuperior.Controls.Add(this.simpleButton1);
            this.panelControlSuperior.Controls.Add(this.BTCalcular);
            this.panelControlSuperior.Controls.Add(this.cBotaoImpNeon1);
            this.panelControlSuperior.Controls.Add(this.simpleButton7);
            this.panelControlSuperior.Controls.Add(this.BFechar);
            this.panelControlSuperior.Controls.Add(this.chMenu);
            this.panelControlSuperior.Controls.Add(this.labelControl1);
            this.panelControlSuperior.Controls.Add(this.TxStatus);
            this.panelControlSuperior.Controls.Add(this.cCompet1);
            this.panelControlSuperior.Dock = System.Windows.Forms.DockStyle.Top;
            this.panelControlSuperior.Location = new System.Drawing.Point(0, 0);
            this.panelControlSuperior.Name = "panelControlSuperior";
            this.panelControlSuperior.Size = new System.Drawing.Size(1536, 95);
            this.panelControlSuperior.TabIndex = 7;
            // 
            // simpleButton1
            // 
            this.simpleButton1.Appearance.Font = new System.Drawing.Font("Tahoma", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.simpleButton1.Appearance.Options.UseFont = true;
            this.simpleButton1.Location = new System.Drawing.Point(908, 16);
            this.simpleButton1.Name = "simpleButton1";
            this.simpleButton1.Size = new System.Drawing.Size(211, 57);
            this.simpleButton1.TabIndex = 66;
            this.simpleButton1.Text = "SEFIP Negativa";
            this.simpleButton1.Click += new System.EventHandler(this.simpleButton1_Click_1);
            // 
            // BTCalcular
            // 
            this.BTCalcular.Appearance.Font = new System.Drawing.Font("Tahoma", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BTCalcular.Appearance.Options.UseFont = true;
            this.BTCalcular.Location = new System.Drawing.Point(364, 15);
            this.BTCalcular.Name = "BTCalcular";
            this.BTCalcular.Size = new System.Drawing.Size(135, 57);
            this.BTCalcular.TabIndex = 65;
            this.BTCalcular.Text = "Calcular";
            this.BTCalcular.Visible = false;
            this.BTCalcular.Click += new System.EventHandler(this.BTCalcular_Click);
            // 
            // cBotaoImpNeon1
            // 
            this.cBotaoImpNeon1.AbrirArquivoExportado = true;
            this.cBotaoImpNeon1.AcaoBotao = dllBotao.Botao.tela;
            this.cBotaoImpNeon1.Appearance.Font = new System.Drawing.Font("Tahoma", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cBotaoImpNeon1.Appearance.Options.UseFont = true;
            this.cBotaoImpNeon1.AssuntoEmail = null;
            this.cBotaoImpNeon1.BotaoEmail = true;
            this.cBotaoImpNeon1.BotaoImprimir = true;
            this.cBotaoImpNeon1.BotaoPDF = true;
            this.cBotaoImpNeon1.BotaoTela = true;
            this.cBotaoImpNeon1.CorpoEmail = null;
            this.cBotaoImpNeon1.CreateDocAutomatico = false;
            this.cBotaoImpNeon1.Impresso = null;
            this.cBotaoImpNeon1.Location = new System.Drawing.Point(1128, 16);
            this.cBotaoImpNeon1.Margin = new System.Windows.Forms.Padding(6, 5, 6, 5);
            this.cBotaoImpNeon1.Name = "cBotaoImpNeon1";
            this.cBotaoImpNeon1.NaoUsarxlsX = false;
            this.cBotaoImpNeon1.NomeArquivoAnexo = "INSS";
            this.cBotaoImpNeon1.PriComp = null;
            this.cBotaoImpNeon1.Size = new System.Drawing.Size(155, 57);
            this.cBotaoImpNeon1.TabIndex = 64;
            this.cBotaoImpNeon1.Titulo = "Relatório por PIS";
            this.cBotaoImpNeon1.clicado += new dllBotao.BotaoEventHandler(this.cBotaoImpNeon1_clicado);
            // 
            // simpleButton7
            // 
            this.simpleButton7.Appearance.Font = new System.Drawing.Font("Tahoma", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.simpleButton7.Appearance.Options.UseFont = true;
            this.simpleButton7.Location = new System.Drawing.Point(505, 15);
            this.simpleButton7.Name = "simpleButton7";
            this.simpleButton7.Size = new System.Drawing.Size(397, 57);
            this.simpleButton7.TabIndex = 62;
            this.simpleButton7.Text = "Relat. para condomínios sem SEFIP";
            this.simpleButton7.Click += new System.EventHandler(this.simpleButton6_Click);
            // 
            // BFechar
            // 
            this.BFechar.Location = new System.Drawing.Point(145, 44);
            this.BFechar.Name = "BFechar";
            this.BFechar.Size = new System.Drawing.Size(140, 29);
            this.BFechar.TabIndex = 61;
            this.BFechar.Text = "Fechar competência";
            this.BFechar.Visible = false;
            this.BFechar.Click += new System.EventHandler(this.BFechar_Click);
            // 
            // chMenu
            // 
            this.chMenu.Location = new System.Drawing.Point(16, 48);
            this.chMenu.Name = "chMenu";
            this.chMenu.Properties.Caption = "Ocultar menu";
            this.chMenu.Size = new System.Drawing.Size(123, 19);
            this.chMenu.TabIndex = 60;
            this.chMenu.CheckedChanged += new System.EventHandler(this.chMenu_CheckedChanged);
            // 
            // TxStatus
            // 
            this.TxStatus.Appearance.Font = new System.Drawing.Font("Tahoma", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxStatus.Appearance.ForeColor = System.Drawing.Color.Red;
            this.TxStatus.Appearance.Options.UseFont = true;
            this.TxStatus.Appearance.Options.UseForeColor = true;
            this.TxStatus.Location = new System.Drawing.Point(145, 15);
            this.TxStatus.Name = "TxStatus";
            this.TxStatus.Size = new System.Drawing.Size(140, 25);
            this.TxStatus.TabIndex = 59;
            this.TxStatus.Text = "labelControl2";
            // 
            // cCompet1
            // 
            this.cCompet1.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.cCompet1.Appearance.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cCompet1.Appearance.Options.UseBackColor = true;
            this.cCompet1.Appearance.Options.UseFont = true;
            this.cCompet1.CaixaAltaGeral = true;
            this.cCompet1.Doc = System.Windows.Forms.DockStyle.None;
            this.cCompet1.Estado = CompontesBasicos.EstadosDosComponentes.Registrado;
            this.cCompet1.IsNull = false;
            this.cCompet1.Location = new System.Drawing.Point(18, 15);
            this.cCompet1.LookAndFeel.Style = DevExpress.LookAndFeel.LookAndFeelStyle.Office2003;
            this.cCompet1.LookAndFeel.UseDefaultLookAndFeel = false;
            this.cCompet1.Name = "cCompet1";
            this.cCompet1.PermiteNulo = false;
            this.cCompet1.ReadOnly = false;
            this.cCompet1.Size = new System.Drawing.Size(121, 27);
            this.cCompet1.somenteleitura = true;
            this.cCompet1.TabIndex = 58;
            this.cCompet1.Titulo = resources.GetString("cCompet1.Titulo");
            this.cCompet1.OnChange += new System.EventHandler(this.cCompet1_OnChange);
            // 
            // xtraTabControl1
            // 
            this.xtraTabControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.xtraTabControl1.Location = new System.Drawing.Point(364, 95);
            this.xtraTabControl1.Name = "xtraTabControl1";
            this.xtraTabControl1.SelectedTabPage = this.xtraTabPageSefip;
            this.xtraTabControl1.Size = new System.Drawing.Size(1172, 712);
            this.xtraTabControl1.TabIndex = 8;
            this.xtraTabControl1.TabPages.AddRange(new DevExpress.XtraTab.XtraTabPage[] {
            this.xtraTabPageSefip,
            this.xtraTabPageRetencoes,
            this.xtraTabPageHistorico});
            // 
            // xtraTabPageSefip
            // 
            this.xtraTabPageSefip.Controls.Add(this.cSefipPagamentos1);
            this.xtraTabPageSefip.Name = "xtraTabPageSefip";
            this.xtraTabPageSefip.Size = new System.Drawing.Size(1166, 684);
            this.xtraTabPageSefip.Text = "SEFIP";
            // 
            // cSefipPagamentos1
            // 
            this.cSefipPagamentos1.CaixaAltaGeral = true;
            this.cSefipPagamentos1.Doc = System.Windows.Forms.DockStyle.Fill;
            this.cSefipPagamentos1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.cSefipPagamentos1.Estado = CompontesBasicos.EstadosDosComponentes.Registrado;
            this.cSefipPagamentos1.Location = new System.Drawing.Point(0, 0);
            this.cSefipPagamentos1.LookAndFeel.UseDefaultLookAndFeel = false;
            this.cSefipPagamentos1.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.cSefipPagamentos1.Name = "cSefipPagamentos1";
            this.cSefipPagamentos1.Size = new System.Drawing.Size(1166, 684);
            this.cSefipPagamentos1.somenteleitura = false;
            this.cSefipPagamentos1.TabIndex = 0;
            this.cSefipPagamentos1.Titulo = resources.GetString("cSefipPagamentos1.Titulo");
            // 
            // xtraTabPageRetencoes
            // 
            this.xtraTabPageRetencoes.Controls.Add(this.cSefip2);
            this.xtraTabPageRetencoes.Name = "xtraTabPageRetencoes";
            this.xtraTabPageRetencoes.Size = new System.Drawing.Size(612, 684);
            this.xtraTabPageRetencoes.Text = "Retenções";
            // 
            // cSefip2
            // 
            this.cSefip2.CaixaAltaGeral = true;
            this.cSefip2.Doc = System.Windows.Forms.DockStyle.Fill;
            this.cSefip2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.cSefip2.Estado = CompontesBasicos.EstadosDosComponentes.Registrado;
            this.cSefip2.Location = new System.Drawing.Point(0, 0);
            this.cSefip2.LookAndFeel.UseDefaultLookAndFeel = false;
            this.cSefip2.Name = "cSefip2";
            this.cSefip2.Size = new System.Drawing.Size(612, 684);
            this.cSefip2.somenteleitura = false;
            this.cSefip2.TabIndex = 0;
            this.cSefip2.Titulo = resources.GetString("cSefip2.Titulo");
            // 
            // xtraTabPageHistorico
            // 
            this.xtraTabPageHistorico.Controls.Add(this.memoEdit1);
            this.xtraTabPageHistorico.Controls.Add(this.panelControl1);
            this.xtraTabPageHistorico.Name = "xtraTabPageHistorico";
            this.xtraTabPageHistorico.Size = new System.Drawing.Size(612, 684);
            this.xtraTabPageHistorico.Text = "Histórico";
            // 
            // memoEdit1
            // 
            this.memoEdit1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.memoEdit1.Location = new System.Drawing.Point(0, 37);
            this.memoEdit1.Name = "memoEdit1";
            this.memoEdit1.Size = new System.Drawing.Size(612, 647);
            this.memoEdit1.TabIndex = 1;
            // 
            // panelControl1
            // 
            this.panelControl1.Controls.Add(this.simpleButton5);
            this.panelControl1.Dock = System.Windows.Forms.DockStyle.Top;
            this.panelControl1.Location = new System.Drawing.Point(0, 0);
            this.panelControl1.Name = "panelControl1";
            this.panelControl1.Size = new System.Drawing.Size(612, 37);
            this.panelControl1.TabIndex = 0;
            // 
            // simpleButton5
            // 
            this.simpleButton5.Location = new System.Drawing.Point(5, 5);
            this.simpleButton5.Name = "simpleButton5";
            this.simpleButton5.Size = new System.Drawing.Size(165, 23);
            this.simpleButton5.TabIndex = 0;
            this.simpleButton5.Text = "Registra anotação";
            this.simpleButton5.Click += new System.EventHandler(this.simpleButton5_Click);
            // 
            // dSefip1
            // 
            this.dSefip1.DataSetName = "dSefip";
            this.dSefip1.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // cSefipGuia
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.xtraTabControl1);
            this.Controls.Add(this.panelControlMenu);
            this.Controls.Add(this.panelControlSuperior);
            this.Doc = System.Windows.Forms.DockStyle.Fill;
            this.LookAndFeel.UseDefaultLookAndFeel = false;
            this.Name = "cSefipGuia";
            this.Size = new System.Drawing.Size(1536, 807);
            this.Titulo = "Sefip";
            this.Load += new System.EventHandler(this.cSefipGuia_Load);
            ((System.ComponentModel.ISupportInitialize)(this.StyleController_F)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControlMenu)).EndInit();
            this.panelControlMenu.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.panelControlSuperior)).EndInit();
            this.panelControlSuperior.ResumeLayout(false);
            this.panelControlSuperior.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.chMenu.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xtraTabControl1)).EndInit();
            this.xtraTabControl1.ResumeLayout(false);
            this.xtraTabPageSefip.ResumeLayout(false);
            this.xtraTabPageRetencoes.ResumeLayout(false);
            this.xtraTabPageHistorico.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.memoEdit1.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl1)).EndInit();
            this.panelControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dSefip1)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraEditors.SimpleButton BotaoLevantamento;
        private DevExpress.XtraEditors.SimpleButton BotaoRTF;
        private DevExpress.XtraEditors.LabelControl labelControl1;
        private System.Windows.Forms.OpenFileDialog openFileDialog;
        private DevExpress.XtraEditors.SimpleButton BotaoGuias;
        private DevExpress.XtraEditors.SimpleButton BotaoGera;
        private DevExpress.XtraEditors.PanelControl panelControlMenu;
        private DevExpress.XtraEditors.PanelControl panelControlSuperior;
        private DevExpress.XtraTab.XtraTabControl xtraTabControl1;
        private DevExpress.XtraTab.XtraTabPage xtraTabPageSefip;
        private DevExpress.XtraTab.XtraTabPage xtraTabPageRetencoes;
        private dllSefipProc.dSefip dSefip1;
        private cSefipPagamentos cSefipPagamentos1;
        private cSefip cSefip2;
        private Framework.objetosNeon.cCompet cCompet1;
        private DevExpress.XtraEditors.LabelControl TxStatus;
        private DevExpress.XtraEditors.CheckEdit chMenu;
        private DevExpress.XtraTab.XtraTabPage xtraTabPageHistorico;
        private DevExpress.XtraEditors.MemoEdit memoEdit1;
        private DevExpress.XtraEditors.PanelControl panelControl1;
        private DevExpress.XtraEditors.SimpleButton simpleButton5;
        private DevExpress.XtraEditors.SimpleButton simpleButton6;
        private DevExpress.XtraEditors.SimpleButton BFechar;
        private DevExpress.XtraEditors.SimpleButton simpleButton7;
        private DevExpress.XtraEditors.SimpleButton BotaoGeraArquivo;
        private DevExpress.XtraEditors.SimpleButton BotaoImprimir;
        private DevExpress.XtraEditors.SimpleButton simpleButton10;
        private VirBotaoNeon.cBotaoImpNeon cBotaoImpNeon1;
        private DevExpress.XtraEditors.SimpleButton BTCalcular;
        private DevExpress.XtraEditors.SimpleButton Button_Gera1;
        private DevExpress.XtraEditors.SimpleButton simpleButton1;
    }
}
