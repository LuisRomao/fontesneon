using System;
using System.Text;
using System.Collections;
using DocBacarios;

namespace dllSefip
{
    /// <summary>
    /// 
    /// </summary>
    public class Guias
    {
        /// <summary>
        /// 
        /// </summary>
        public ArrayList guias;

        private string Limpa(string entrada)
        {
            bool pulando = false;
            //bool cancelapulo = false;
            StringBuilder SB = new StringBuilder();
            //string str2 = "";
            foreach (char c in entrada)
                if (!pulando)
                    if (c != '\r')
                        SB.Append(c);
                    //str2 += c;
                    else
                    {
                        pulando = true;
                    }
                else
                    if (c == '\n')
                        SB.Append("\r\n");
                    //str2 += "\r\n";
                    else
                        if (c != ' ')
                        {
                            SB.Append(c);
                            //str2 += c;
                            pulando = false;
                        }
            //return str2;
            return SB.ToString();
        }

        
        /// <summary>
        /// 
        /// </summary>
        /// <param name="arquivo"></param>
        public Guias(string arquivo)
        {
            CPFCNPJ cnpj = null;
            guias = new ArrayList();
            string original = Limpa(System.IO.File.ReadAllText(arquivo));
            string[] linhas = original.Split(new string[] { "\r\n" }, StringSplitOptions.RemoveEmptyEntries);
            foreach (string linha in linhas)
            {
                if (linha.Contains("GFIP"))
                    continue;
                if (linha.Contains("RIO DA FAZENDA"))
                    continue;
                if (linha.Contains("RECEITA FEDERAL"))
                    continue;
                if (linha.Contains("TICO DE GPS"))
                    continue;
                if ((linha.Contains("COMPET")) && (linha.Contains("NCI")))
                    continue;
                if ((linha.Contains("OBSERVA")) && (linha.Contains("ALOR COMPENSADO")))
                    continue;
                if (linha.Contains("OUTRAS ENTIDADES"))
                    continue;
                if (linha.Contains("VALORES:"))
                    continue;
                if (linha.Contains("------------"))
                    continue;
                if (linha.Contains("OBSERVA"))
                    continue; 
                string[] palavras = linha.Split(new char[] { ' ' }, StringSplitOptions.RemoveEmptyEntries);

                bool novoCNPJ = false;

                foreach (string palavra in palavras)
                {
                    if ((palavra.Length == 19) && (palavra[10] == '/'))
                    {
                        cnpj = new CPFCNPJ(palavra);
                        if (cnpj.Tipo == TipoCpfCnpj.CNPJ)
                        {
                            novoCNPJ = true;
                            break;
                        }
                    }
                    if ((palavra.Length >= 19) && (palavra.Contains("/")) && (palavra.Contains(".")))
                    {
                        string cnpjteste = "";
                        foreach (char c in palavra)
                            if (char.IsDigit(c))
                                cnpjteste += c;
                        if (cnpjteste.Length > 14)
                            cnpjteste = cnpjteste.Substring(cnpjteste.Length - 14, 14);
                        cnpj = new CPFCNPJ(cnpjteste);
                        if (cnpj.Tipo == TipoCpfCnpj.CNPJ)
                        {
                            novoCNPJ = true;
                            break;
                        }
                    }
                }
                if (!novoCNPJ)
                {
                    if (palavras.Length > 6)
                    {
                        decimal Valor = decimal.Parse(palavras[6].Replace(".",""));
                        if (Valor > 28)
                            guias.Add(new Par(cnpj, Valor));
                    }
                }
            }
        }
    }

    /// <summary>
    /// 
    /// </summary>
    public class Par
    {
        /// <summary>
        /// 
        /// </summary>
        public CPFCNPJ cnpj;
        /// <summary>
        /// 
        /// </summary>
        public decimal valor;

        /// <summary>
        /// 
        /// </summary>
        /// <param name="_cnpj"></param>
        /// <param name="_valor"></param>
        public Par(CPFCNPJ _cnpj, decimal _valor)
        {
            cnpj = _cnpj;
            valor = _valor;
        }
    }
}
