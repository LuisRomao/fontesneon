/*
LH - 08/05/2014       - 13.2.8.46 - FIX - Valor para PAGPermiteAgrupar
MR - 03/12/2014 10:00 -           - Salvando os dados adicionais de cod. de comunicacao na tabela SEFip quando vindo do RTF ou das GUIAS SEFIPs (Altera��es indicadas por *** MRC - INICIO - TRIBUTOS (03/12/2014 10:00) ***)
                                    Salvando os dados adicionais de cod. de comunicacao, dig. agencia e dig. conta na tabela GPS
                                    Salvando NomeIdentificador diferenciado para layout antigo (CNPJ) e layout novo (NOME)
                                    Solicita Data para Pagamento, permitindo escolher uma data diferente da data seguinte
MR - 04/12/2014 10:00 -           - Solicita libera��o apenas para valores acima de 15000,00 e n�o tratar n�vel de libera��o (Altera��es indicadas por *** MRC - INICIO - TRIBUTOS (04/12/2014 10:00) ***)
MR - 29/01/2015 12:00 -           - Seta tipos de pagamento considerando o apenas Tributo Eletr�nico Brandesco novo onde o CONCodigoComunicacaoTributos � obrigatorio (Altera��es indicadas por *** MRC - INICIO - TRIBUTOS (29/01/2015 12:00) ***)
                                    Faz consist�ncias necess�rias para Tributo Eletr�nico Brandesco novo antes de setar envio
                                    Permite o envio eletr�nico mesmo para os casos onde n�o existe valor de folha, s� n�o altera notas, pagamentos e cheques j� criados pois o valor � o mesmo
*/

using System;
using System.IO;
using System.Windows.Forms;
using CompontesBasicosProc;
using CompontesBasicos;
using ContaPagarProc;
using dllCheques;
using dllImpostoNeonProc;
using DocBacarios;
using Framework;
using Framework.objetosNeon;
using FrameworkProc;
using iTextSharp.text;
using iTextSharp.text.pdf;
using dllSefipProc;
using VirEnumeracoes;
using VirEnumeracoesNeon;

namespace dllSefip
{
    /// <summary>
    /// 
    /// </summary>
    public partial class cSefipGuia : ComponenteBase
    {
        
        private Sefip _Sefip1;

        private Sefip Sefip1
        {
            get 
            {
                if (_Sefip1 == null)
                {
                    _Sefip1 = new Sefip(cCompet1.Comp, this);
                }
                return _Sefip1;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        public cSefipGuia()
        {
            InitializeComponent();
            cSefip2.ConfiguracaoInicial(Sefip1, this);
            cSefipPagamentos1.ConfiguracaoInicial(Sefip1, this);
            if (DSCentral.USU == 30)
                Button_Gera1.Visible = true;
            //System.Windows.Forms.MessageBox.Show("TESTE");
            //if (DSCentral.USU == 54)
            //    Button_Gera1.Visible = true;
        }


        private void cSefipGuia_Load(object sender, EventArgs e)
        {
            cCompet1.SetCompetencia(Framework.datasets.dAGendaGeral.dAGendaGeralSt.GetAGBCompetencia(Framework.datasets.AGGTipo.SEFIP));
            cCompet1.CompMaxima = cCompet1.Comp.CloneCompet();
            CompetenciaAlterada();
        }
        

        private void CarregaRetencoes()
        {
            try
            {
                cSefip2.gridView2.BeginDataUpdate();
                //cSefip2.gridView1.BeginDataUpdate();
                //if (!Status.EstaNoGrupo(Framework.datasets.AGGStatus.NaoIniciado, Framework.datasets.AGGStatus.EmProducao))
                //    return;
                using (CompontesBasicos.Espera.cEspera Esp = new CompontesBasicos.Espera.cEspera(this))
                {
                    Application.DoEvents();
                    xtraTabControl1.SelectedTabPage = xtraTabPageRetencoes;
                    if (Status == Framework.datasets.AGGStatus.NaoIniciado)
                    {
                        Status = Framework.datasets.AGGStatus.EmProducao;
                        Framework.datasets.dAGendaGeral.dAGendaGeralSt.SetAGBStatus(Framework.datasets.AGGTipo.SEFIP, Status);
                    }
                    cSefip2.Carrega(cCompet1.Comp);
                    //MostraStatus();
                    //Application.DoEvents();
                }
            }
            finally
            {
                cSefip2.gridView2.EndDataUpdate();
                //cSefip2.gridView1.EndDataUpdate();
            }            
        }

        private void simpleButton1_Click(object sender, EventArgs e)
        {
            CarregaRetencoes();
            AtualizarPaginaAtiva(true);
        }

        //bool UsarServidorDeProcessos = true;

        private void simpleButton8_Click(object sender, EventArgs e)
        {
            bool resultadook;            
            try 
            {
                cSefipPagamentos1.gridView1.BeginDataUpdate();
                Sefip1.gravanobanco();
                resultadook = cSefip2.GerarArquivo(cCompet1.Comp);
            }
            finally 
            {
                cSefipPagamentos1.gridView1.EndDataUpdate();
            }
            if (!resultadook)
            {
                MessageBox.Show("ATEN��O\r\nExiste a necessidade de corre��o nos dados.\r\nErro relatado:" + cSefip2.Sefip1.Ultimoerro);                
                labelControl1.Text = "Verificar";
                chMenu.Checked = true;
            }
            else
            {
                Clipboard.SetText(cSefip2.Sefip1.NomeArquivo);
                labelControl1.Text = "Ok - Carregue na SEFIP - Arquivo na �rea de transfer�ncia";                
                xtraTabControl1.SelectedTabPage = xtraTabPageSefip;
                BotaoRTF.Enabled = BotaoGuias.Enabled = BotaoGera.Enabled = true;
            }
            labelControl1.Visible = true;
        }


        private void simpleButton2_Click(object sender, EventArgs e)
        {            
            if (openFileDialog.ShowDialog() == DialogResult.OK)
            {
                ExtrairGPSdoRTF(openFileDialog.FileName);
            }            
        }

        private void reportaerro(string Erro)
        {
            MessageBox.Show(Erro);
            MessageBox.Show("Carga cancelada");
        }
        

        private void ExtrairGPSdoRTF(string Arquivo)
        {
            Sefip1.DSefip.CarregaCondominios(false);
            bool Cancelado = false;
            xtraTabControl1.SelectedTabPage = xtraTabPageSefip;
            //Sefip1.DSefip.CarregaCondominios();            
            //Sefip1.DSefip.CONDOMINIOSTableAdapter.Fill(Sefip1.DSefip.CONDOMINIOS);            
            try
            {
                cSefipPagamentos1.gridView1.BeginDataUpdate();
                string[] Lines = File.ReadAllLines(Arquivo);
                if (Lines.Length > 0)
                {
                    int iLinha = 0;
                    using (CompontesBasicos.Espera.cEspera Esp = new CompontesBasicos.Espera.cEspera(this))
                    {
                        Esp.Motivo.Text = "Carregando RTF";
                        Application.DoEvents();


                        Esp.AtivaGauge(Lines.Length);


                        bool Terminado = false;
                        DateTime TempoAtua = DateTime.Today;
                        while (!Terminado)
                        {
                            if (DateTime.Now > TempoAtua)
                            {
                                Application.DoEvents();
                                Esp.Gauge(iLinha);
                                TempoAtua = DateTime.Now.AddSeconds(3);
                            }
                            if (Esp.Abortar)
                                return;

                            while (!Lines[iLinha].Contains("3-Cod.pagto"))
                            {
                                iLinha++;
                                if (iLinha >= Lines.Length)
                                {
                                    Terminado = true;
                                    break;
                                }
                            }
                            if (!Terminado)
                            {
                                string strcodigoGPS = Lines[iLinha].Substring(61, 18).Replace(" ", "");
                                string strCondominio = Lines[iLinha + 6].Substring(1, 45);
                                string strCodFolha = strCondominio.Substring(0, 4).Trim();
                                string CNPJ = Lines[iLinha + 4].Substring(61, 18).Replace(" ", "");
                                string Compet = Lines[iLinha + 2].Substring(61, 18).Replace(" ", "");
                                string strValorINSS = Lines[iLinha + 6].Substring(61, 18).Replace(" ", "").Trim();
                                string strValorEntidades = Lines[iLinha + 12].Substring(61, 18).Replace(" ", "").Trim();

                                int codigoGPS = int.Parse(strcodigoGPS);
                                decimal ValorINSS = strValorINSS == "" ? 0 : decimal.Parse(strValorINSS.Trim());
                                decimal ValorEntidades = strValorEntidades == "" ? 0 : decimal.Parse(strValorEntidades.Trim());

                                CPFCNPJ cnpjLido = new CPFCNPJ(CNPJ);

                                int CodFolha = 0;
                                if (!Int32.TryParse(strCodFolha, out CodFolha))
                                {
                                    reportaerro(string.Format("C�digo folha inv�lido:<{0}>\r\nCondom�nio:<{1}>", strCodFolha, strCondominio));
                                    Terminado = true;
                                    Cancelado = true;
                                    MessageBox.Show("corre��o");
                                    //Sefip1.DSefip.SEFipTableAdapter().FillByCompet(Sefip1.DSefip.SEFip, cCompet1.Comp.CompetenciaBind);
                                    return;
                                }

                                //if (cnpjLido.ToString() == "00.255.663/0001-57")
                                //    MessageBox.Show("aten��o");

                                //Framework.datasets.dCondominiosAtivos.CONDOMINIOSRow 
                                dSefip.CONDOMINIOSRow rowCon = null;
                                foreach (dSefip.CONDOMINIOSRow rowConTest in Sefip1.DSefip.CONDOMINIOS)
                                {
                                    CONStatus Status = (CONStatus)rowConTest.CONStatus;
                                    if ((rowConTest.CONSefip) && (!rowConTest.IsCONCnpjNull()) && (rowConTest.CONCnpj == cnpjLido.ToString()) && Status.EstaNoGrupo(CONStatus.Ativado, CONStatus.Desativado, CONStatus.Implantacao))
                                    {
                                        rowCon = rowConTest;
                                        break;
                                    }

                                };

                                if (rowCon == null)
                                    foreach (dSefip.CONDOMINIOSRow rowConTest in Sefip1.DSefip.CONDOMINIOS)
                                    {
                                        if ((rowConTest.CONSefip) && (!rowConTest.IsCONCnpjNull()) && (rowConTest.CONCnpj == cnpjLido.ToString()))
                                        {
                                            rowCon = rowConTest;
                                            break;
                                        }
                                    };

                                if (rowCon == null)
                                {
                                    reportaerro(string.Format("Nenhum condom�nio encontrado com o CNPJ <{0}>\r\nCondom�nio:<{1}>", cnpjLido, strCondominio));
                                    Terminado = true;
                                    Cancelado = true;
                                    //Sefip1.DSefip.SEFipTableAdapter().FillByCompet(Sefip1.DSefip.SEFip, cCompet1.Comp.CompetenciaBind);
                                    return;

                                };


                                //Teste += string.Format("Codcon {0} Valor {1:n2} entidades {2:n2}\r\n", rowCon.CONCodigo, ValorINSS, ValorEntidades);

                                dSefip.SEFipRow rowSEFIP = Sefip1.DSefip.BuscaSEFipRow(rowCon.CON, rowCon.CON_EMP);
                                if (rowSEFIP == null)
                                {
                                    rowSEFIP = Sefip1.DSefip.SEFip.NewSEFipRow();
                                    rowSEFIP.SEF_CON = rowCon.CON;
                                    rowSEFIP.calNomeCondominio = string.Format("{0}{1}", rowCon.IsCONCodigoFolha1Null() ? "" : rowCon.CONCodigoFolha1 + " ", rowCon.CONNome);
                                    rowSEFIP.SEF_EMP = rowCon.CON_EMP;
                                    rowSEFIP.CONCnpj = rowCon.CONCnpj;
                                    if (!rowCon.IsCONCodigoComunicacaoTributosNull())
                                        rowSEFIP.SEFCodComunicacao = "117045";// rowCon.CONCodigoComunicacaoTributos;                                    
                                    rowSEFIP.SEFCompetencia = cCompet1.Comp.CompetenciaBind;
                                    rowSEFIP.SEFTipoPag = (int)nTipoPag.Invalido;
                                    //*** MRC - INICIO - TRIBUTOS (29/01/2015 12:00) ***
                                    if ((rowCon.CONStatus == 1) && (!rowCon.IsCON_BCONull()))
                                    {
                                        if ((!rowCon.IsCONGPSEletronicoNull() && rowCon.CONGPSEletronico))
                                        {
                                            if (rowCon.CON_BCO == 341)
                                                rowSEFIP.SEFTipoPag = (int)nTipoPag.EletronicoItau;
                                            else if ((rowCon.CON_BCO == 237) && (!rowCon.IsCONCodigoComunicacaoTributosNull()))
                                                rowSEFIP.SEFTipoPag = (int)nTipoPag.EletronicoBradesco;
                                            else
                                                if (rowCon.CON_EMP == DSCentral.EMP)
                                                rowSEFIP.SEFTipoPag = (int)nTipoPag.Cheque;
                                            else
                                                rowSEFIP.SEFTipoPag = (int)nTipoPag.ChequeFilial;
                                        }
                                        else
                                            if (rowCon.CON_EMP == DSCentral.EMP)
                                            rowSEFIP.SEFTipoPag = (int)nTipoPag.Cheque;
                                        else
                                            rowSEFIP.SEFTipoPag = (int)nTipoPag.ChequeFilial;
                                    }
                                    //if (rowCon.CONStatus != 1)
                                    //    rowSEFIP.SEFTipoPag = (int)nTipoPag.Invalido;
                                    //else
                                    //    if ((!rowCon.IsCONGPSEletronicoNull() && rowCon.CONGPSEletronico))
                                    //    {
                                    //        if (rowCon.CON_EMP != Framework.DSCentral.EMP)
                                    //            rowSEFIP.SEFTipoPag = (int)nTipoPag.EletronicoBradesco;
                                    //        else
                                    //        {
                                    //            if (rowCon.CON_BCO == 237)
                                    //                rowSEFIP.SEFTipoPag = (int)nTipoPag.EletronicoBradesco;
                                    //            else
                                    //                rowSEFIP.SEFTipoPag = (int)nTipoPag.EletronicoItau;
                                    //        }
                                    //    }
                                    //    else
                                    //        if (rowCon.CON_EMP != Framework.DSCentral.EMP)
                                    //            rowSEFIP.SEFTipoPag = (int)nTipoPag.ChequeFilial;
                                    //        else
                                    //            rowSEFIP.SEFTipoPag = (int)nTipoPag.Cheque;
                                    //*** MRC - TERMINO - TRIBUTOS (29/01/2015 12:00) ***

                                    Sefip1.DSefip.SEFip.AddSEFipRow(rowSEFIP);
                                };
                                if ((rowSEFIP.IsSEFCodFolhaNull()) || (rowSEFIP.SEFCodFolha != CodFolha))
                                    rowSEFIP.SEFCodFolha = CodFolha;
                                if ((rowSEFIP.IsSEFValorFolhaNull()) || (rowSEFIP.SEFValorFolha != ValorINSS))
                                    rowSEFIP.SEFValorFolha = ValorINSS;
                                if ((rowSEFIP.IsSEFValorFolhaOutrasNull()) || (rowSEFIP.SEFValorFolhaOutras != ValorEntidades))
                                    rowSEFIP.SEFValorFolhaOutras = ValorEntidades;
                                if (rowSEFIP.RowState != System.Data.DataRowState.Unchanged)
                                {
                                    if (rowSEFIP.SEF_EMP == DSCentral.EMP)
                                        Sefip1.DSefip.SEFipTableAdapter.Update(rowSEFIP);
                                    else
                                        Sefip1.DSefip.SEFipTableAdapterF.Update(rowSEFIP);
                                }
                                iLinha++;

                                if (iLinha >= Lines.Length)
                                    Terminado = true;
                            };
                            if (!Cancelado)
                            {


                                Sefip1.DSefip.Recalcular();
                                Sefip1.DSefip.SEFip.AcceptChanges();
                            }
                            //Clipboard.SetText(Teste);


                            //if (sLinha.IndexOf("3-Cod.pagto") >= 0)
                            //{
                            //string sLinhaPag = sLinha;
                            //string sLinhacompet = ProximaLinha(2);
                            //string sLinhaIDENT = ProximaLinha(2);
                            //string sLinhaValor = ProximaLinha(2);

                            //Extraindo codigo do condominio na GPS e verificando
                            //se o cadastro do mesmo existe na tabela condominios
                            //atraves do campo CONCodigoFolha1




                            //String sCondominio = sLinhaValor.Substring(1, 45);



                            //String sCodigoFolha1 = Convert.ToInt32(sLinhaValor.Substring(1, 4).Replace(" ", "")).ToString();
                            //string CNPJ = sLinhaIDENT.Substring(61, 18).Replace(" ", "");

                            //DataRowView DRV = ((DataRowView)(this.iNSSBindingSource.AddNew()));
                            //RowGPS = (dAutomacaoBancaria.INSSRow)DRV.Row;
                            //RowGPS.BeginEdit();
                            //RowGPS.SEFIP = 0;
                            //RowGPS.NEON = 0;
                            //Int32 CodFolha = 0;
                            //RowGPS.TipoPag = (int)dAutomacaoBancaria.nTipoPag.Invalido;
                            //string strCodFolha = sLinhaValor.Substring(1, 4).Trim();
                            //if (!Int32.TryParse(strCodFolha, out CodFolha))
                            //    RowGPS["Erro"] = "C�digo folha inv�lido:<" + sLinhaValor.Substring(1, 4) + ">";
                            //else
                            //{
                            //dAutomacaoBancaria.CONDOMINIOSRow CONrow = dAutomacaoBancaria.BuscaCondominio(CodFolha);
                            //if (CONrow == null)
                            //{
                            //    RowGPS["Erro"] = "Nenhum condom�nio encontrado com o c�digo " + CodFolha.ToString();
                            //    RowGPS["BANCO"] = 0;
                            //}
                            //else
                            //{
                            //RowGPS["CON"] = CONrow.CON;
                            //if (CONrow.IsCONCnpjNull())
                            //{
                            //    MessageBox.Show(string.Format("Erro:\r\nCondominio {0} - {1} sem CNPJ", CONrow.CONNome, CONrow.CONCodigo));
                            //    return;
                            //}
                            //RowGPS["nCNPJ"] = new DocBacarios.CPFCNPJ(CONrow.CONCnpj).Valor;
                            //DocBacarios.CPFCNPJ cnpjLido = new DocBacarios.CPFCNPJ(CNPJ);
                            //if (cnpjLido.Tipo == DocBacarios.TipoCpfCnpj.INVALIDO)
                            //    RowGPS["Erro"] = "CNPJ inv�lido:" + CNPJ;
                            //else
                            //{
                            //    if (CONrow.IsCONCnpjNull())
                            //        RowGPS["Erro"] = "CNPJ n�o cadastrado!";
                            //    else
                            //        if (sLinhaPag.Substring(61, 18).Replace(" ", "") != "2631")
                            //            if (cnpjLido.Valor != new DocBacarios.CPFCNPJ(CONrow.CONCnpj).Valor)
                            //                RowGPS["Erro"] = "CNPJ n�o confere:" + CNPJ + " x " + CONrow.CONCnpj;
                            //};
                            //try
                            //{
                            //    RowGPS["Agencia"] = int.Parse(CONrow.CONAgencia);
                            //    RowGPS["Conta"] = int.Parse(CONrow.CONConta);
                            //    RowGPS["BANCO"] = CONrow.CON_BCO;
                            //    if (CONrow.CONCodigoFolha1 != CodFolha)
                            //        RowGPS.TipoPag = (int)dAutomacaoBancaria.nTipoPag.NaoEmitir;
                            //    else
                            //    {
                            //        if ((!CONrow.IsCONPagEletronicoNull()) && (CONrow.CONPagEletronico))
                            //        {
                            //            if (CONrow.CON_BCO == 237)
                            //                RowGPS.TipoPag = (int)dAutomacaoBancaria.nTipoPag.EletronicoBradesco;
                            //            else
                            //                RowGPS.TipoPag = (int)dAutomacaoBancaria.nTipoPag.EletronicoItau;
                            //        }
                            //        else
                            //            if (CONrow.CON_EMP != Framework.DSCentral.EMP)
                            //                RowGPS.TipoPag = (int)dAutomacaoBancaria.nTipoPag.ChequeFilial;
                            //            else
                            //                RowGPS.TipoPag = (int)dAutomacaoBancaria.nTipoPag.Cheque;
                            //    }
                            //}
                            //catch
                            // {
                            //     RowGPS.Erro = "Erro na agencia/conta do condom�nio";
                            //     RowGPS["Agencia"] = RowGPS["Conta"] = RowGPS.Banco = 0;
                            // }



                        }
                        //};

                        //Campo 3-Cod.pagto
                        //RowGPS["Campo3"] = sLinhaPag.Substring(61, 18).Replace(" ", "");

                        //Campo 4-Competenc.
                        //string Compet = sLinhacompet.Substring(61, 18).Replace(" ", "");
                        //if (comp == null)
                        //{
                        //    int mes = int.Parse(Compet.Substring(0, 2));
                        //    int ano = int.Parse(Compet.Substring(3, 4));
                        //    comp = new Framework.objetosNeon.Competencia(mes, ano);
                        //};
                        //RowGPS["Campo4"] = Compet;
                        //RowGPS["MES"] = Compet.Substring(0, 2);
                        //RowGPS["ANO"] = Compet.Substring(3, 4);

                        //Campo 5-Identific.
                        //RowGPS["Campo5"] = sLinhaIDENT.Substring(61, 18).Replace(" ", "");

                        //Campo 6-Valor INSS + Dados da Empresa


                        //RowGPS["Condominio"] = sLinhaValor.Substring(1, 4).Replace(" ", "");
                        //RowGPS["Endereco"] = drFoundRows[0]["CONEndereco"];
                        //RowGPS["Bairro"] = drFoundRows[0]["CONBairro"];
                        //RowGPS["Cidade"] = drFoundRows[0]["CIDNome"];
                        //RowGPS["UF"] = drFoundRows[0]["CIDUF"];
                        //RowGPS["CEP"] = drFoundRows[0]["CONCEP"];
                        //RowGPS["Campo1"] = drFoundRows[0]["CONNome"];

                        //sLinha = ProximaLinha();
                        //RowGPS["Endereco"] = sLinha.Substring(2, 44).Trim();
                        //sLinha = ProximaLinha();
                        //RowGPS["Bairro"] = sLinha.Substring(18, 28).Trim();
                        //sLinha = ProximaLinha();
                        //RowGPS["CEP"] = sLinha.Substring(2, 8).Trim();
                        //RowGPS["Cidade"] = sLinha.Substring(12, 21).Trim();
                        //RowGPS["UF"] = sLinha.Substring(34, 2).Trim();



                        //RowGPS["Campo1"] = sLinhaValor.Substring(6, 39).Trim();
                        //CampoDecimal = sLinhaValor.Substring(61, 18).Replace(" ", "");
                        //CampoDecimal = (CampoDecimal.Trim() == "" ? "0" : CampoDecimal);
                        //RowGPS["Campo6"] = CampoDecimal;

                        //Campo 7 e 8 n�o importado

                        //Campo 9-Val.Outros 
                        //sLinha = ProximaLinha(3);
                        //CampoDecimal = sLinha.Substring(61, 18).Replace(" ", "");
                        //CampoDecimal = (CampoDecimal.Trim() == "" ? "0" : CampoDecimal);
                        //RowGPS["Campo9"] = CampoDecimal;

                        //Campo 10-At.Monet/Juros/multa
                        //sLinha = ProximaLinha(3);
                        //CampoDecimal = sLinha.Substring(61, 18).Replace(" ", "");
                        //CampoDecimal = (CampoDecimal.Trim() == "" ? "0" : CampoDecimal);
                        //RowGPS["Campo10"] = CampoDecimal;

                        //Campo 11-Total
                        //sLinha = ProximaLinha(2);
                        //CampoDecimal = sLinha.Substring(61, 18).Replace(" ", "");
                        //CampoDecimal = (CampoDecimal.Trim() == "" ? "0" : CampoDecimal);
                        //RowGPS["Campo11"] = CampoDecimal;

                        //Campo 12-Autenticacao Bancaria nao importado
                        //if (!RowGPS.IsErroNull())
                        //    RowGPS.TipoPag = (int)dAutomacaoBancaria.nTipoPag.Invalido;
                        //RowGPS.EndEdit();
                        //}
                        //}
                        //sLinha = ProximaLinha();
                        //}

                        //this.ViewGPS.EndDataUpdate();
                        //try
                        //{
                        //    dAutomacaoBancaria.EnforceConstraints = true;
                        //}
                        //catch (System.Data.ConstraintException)
                        //{
                        //    MessageBox.Show("Erros na importa��o dos dados... corrigir o cadastro na folha.");
                        //}
                        //this.btnCancelarExtracao.Visible = false;
                        //this.pgbExtracao.Visible = false;

                        //this.Cursor = Cursors.Default;
                        //this.TabControlAutomacao.SelectedTabPage = TabPageGPS;
                        //if (sCONErro.Trim() != "")
                        //MessageBox.Show("O processo de extra��o de dados do arquivo " + this.openFileDialog.FileName + " foi conclu�do com sucesso.", "Automa��o Banc�ria", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        //else
                        //{
                        //    MessageBox.Show("O processo de extra��o de dados do arquivo " + this.openFileDialog.FileName + " foi conclu�do, mais n�o foi poss�vel localizar o registro de um ou mais condom�nios.\r\nVerifique os dados dos condom�nios abaixo e tente gerar o arquivo novamente.\r\n\r\nCondom�nio(s) n�o localizado(s): \r\n" + sCONErro, "Automa��o Banc�ria", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        //    using (StreamWriter swFile = new StreamWriter("c:\\logdeerro.txt", true))
                        //    {
                        //        swFile.WriteLine("----------------");
                        //        swFile.WriteLine("  " + DateTime.Now.ToString());
                        //        swFile.WriteLine("----------------");
                        //        swFile.WriteLine(sCONErro.ToString());
                        //        swFile.Close();
                        //    }
                        //}




                        /*
                        try
                        {
                            ViewGPS.BeginDataUpdate();
                            dllSefip.Sefip sefip1 = new dllSefip.Sefip(comp, false);
                            Esp.Motivo.Text = "Carregando Notas";
                            Application.DoEvents();
                            Esp.AtivaGauge(sefip1.dSefip.Sefip.Rows.Count);
                            int i = 0;
                            DateTime prox = DateTime.Now;
                            foreach (dllSefip.dSefip.SefipRow rowlocal in sefip1.dSefip.Sefip)
                            {
                                i++;
                                if (prox <= DateTime.Now)
                                {
                                    Esp.Gauge(i);
                                    prox = DateTime.Now.AddSeconds(3);
                                }
                                dAutomacaoBancaria.INSSRow rowINSS = dAutomacaoBancaria.INSS.FindBynCNPJ(rowlocal.CONCNPJ);
                                if (rowINSS == null)
                                {
                                    rowINSS = dAutomacaoBancaria.INSS.NewINSSRow();
                                    rowINSS.nCNPJ = rowlocal.CONCNPJ;
                                    rowINSS.NEON = 0;
                                    //Int32 CodFolha = 0;
                                    rowINSS.CON = rowlocal.CON;
                                    rowINSS.Campo1 = rowlocal.CONNome;
                                    //rowINSS.Banco
                                    //rowINSS.Agencia
                                    //rowINSS.Conta
                                    dAutomacaoBancaria.CONDOMINIOSRow CONrow = dAutomacaoBancaria.CONDOMINIOS.FindByCON(rowlocal.CON);
                                    rowINSS.TipoPag = (int)dAutomacaoBancaria.nTipoPag.Invalido;
                                    if ((!CONrow.IsCONPagEletronicoNull()) && (CONrow.CONPagEletronico))
                                    {
                                        if (CONrow.CON_BCO == 237)
                                            rowINSS.TipoPag = (int)dAutomacaoBancaria.nTipoPag.EletronicoBradesco;
                                        else
                                            rowINSS.TipoPag = (int)dAutomacaoBancaria.nTipoPag.EletronicoItau;
                                    }
                                    else
                                        if (CONrow.CON_EMP != Framework.DSCentral.EMP)
                                            rowINSS.TipoPag = (int)dAutomacaoBancaria.nTipoPag.ChequeFilial;
                                        else
                                            rowINSS.TipoPag = (int)dAutomacaoBancaria.nTipoPag.Cheque;
                                    if (rowINSS.TipoPag == (int)dAutomacaoBancaria.nTipoPag.EletronicoBradesco)
                                    {
                                        try
                                        {
                                            rowINSS.Banco = CONrow.CON_BCO;
                                            rowINSS.Agencia = int.Parse(CONrow.CONAgencia);
                                            rowINSS.Conta = int.Parse(CONrow.CONConta);
                                            rowINSS.CEP = CONrow.CONCep;
                                            rowINSS.Endereco = CONrow.CONEndereco;
                                            rowINSS.UF = CONrow.CIDUf;
                                            rowINSS.Cidade = CONrow.CIDNome;
                                            rowINSS.Bairro = CONrow.CONBairro;
                                        }
                                        catch
                                        {
                                            rowINSS.TipoPag = (int)dAutomacaoBancaria.nTipoPag.Invalido;
                                            rowINSS.Erro = "Erro no cadastro n�o permite pagamento eletr�nico";
                                        }
                                    };
                                    rowINSS.Campo3 = "2100";
                                    rowINSS.Campo4 = comp.ToString();
                                    rowINSS.ANO = comp.Ano.ToString("0000");
                                    rowINSS.MES = comp.Mes.ToString("00");
                                    rowINSS.Campo5 = rowlocal.CONCNPJ.ToString();

                                    dAutomacaoBancaria.INSS.AddINSSRow(rowINSS);

                                }


                                foreach (dllSefip.dSefip.INSSPagoRow rowPAG in rowlocal.GetINSSPagoRows())

                                    rowINSS.NEON += rowPAG.PAGValor;


                            }
                            string Arquivo = @"\\NEON02/folha/virtual/gps.txt";
                            if (!File.Exists(Arquivo))
                            {
                                OpenFileDialog OF = new OpenFileDialog();
                                if (OF.ShowDialog() != DialogResult.OK)
                                {
                                    return;
                                };
                                Arquivo = OF.FileName;
                            }
                            Esp.Motivo.Text = "Lendo SEFIP";
                            Application.DoEvents();
                            dllSefip.Guias Guias1 = new Guias(Arquivo);
                            Esp.Motivo.Text = "Carregando SEFIP";
                            Application.DoEvents();
                            //int i = 0;
                            foreach (Par par in Guias1.guias)
                            {
                                //i++;
                                dAutomacaoBancaria.INSSRow rowINSS = dAutomacaoBancaria.INSS.FindBynCNPJ(par.cnpj.Valor);
                                if (rowINSS == null)
                                {
                                    MessageBox.Show(string.Format("Guia sem par: CNPJ:{0} Valor:{1:n2}", par.cnpj, par.valor));
                                }
                                else
                                {
                                    rowINSS.SEFIP = par.valor;

                                };
                            }
                        }
                        finally
                        {
                            ViewGPS.EndDataUpdate();
                        }
                        */
                    }



                };
            }
            finally
            {
                cSefipPagamentos1.gridView1.EndDataUpdate();
            }

        }

        private System.Collections.Generic.SortedList<Int64,ImpressoGuia> ImpressosGuias;

        private void CarregaImpressosGuias()
        {
            ImpressosGuias = new System.Collections.Generic.SortedList<Int64, ImpressoGuia>();
            string Caminho = @"\\neon02\FOLHA\INSS PDF";
            if (!Directory.Exists(Caminho))
                Caminho = "D:\\INSS PDF";
            foreach (string Arquivo in Directory.GetFiles(Caminho, "*.pdf"))
            {
                ImpressoGuia ImpG = new ImpressoGuia(Arquivo);
                if ((ImpG.Erro == "") && (ImpG.cnpj.Tipo == TipoCpfCnpj.CNPJ) && (ImpG.comp == cCompet1.Comp))
                {
                    ImpressosGuias.Add(ImpG.cnpj.Valor, ImpG);
                }
            }
        }

        private void simpleButton3_Click(object sender, EventArgs e)
        {
            //MessageBox.Show("TESTE");
            //cCompet1.Comp.Mes = 2;
            Sefip1.DSefip.CarregaCondominios(false);
            using (CompontesBasicos.Espera.cEspera Esp = new CompontesBasicos.Espera.cEspera(this))
            {
                Esp.Motivo.Text = "Lendo Guias SEFIP";
                Application.DoEvents();
                CarregaImpressosGuias();
                //string Arquivo = @"\\NEON02/folha/virtual/gps.txt";
                //if (!File.Exists(Arquivo))
                //{
                //    OpenFileDialog OF = new OpenFileDialog();
                //     if (OF.ShowDialog() != DialogResult.OK)
                //     {
                //         return;
                //     };
                //     Arquivo = OF.FileName;
                //}
                //Esp.Motivo.Text = "Lendo SEFIP";
                //Application.DoEvents();
                //dllSefip.Guias Guias1 = new Guias(Arquivo);
                Esp.Motivo.Text = "Carregando SEFIP";
                Application.DoEvents();
                //int i = 0;
                foreach (ImpressoGuia par in ImpressosGuias.Values)
                {
                    dSefip.CONDOMINIOSRow rowCON = null;
                    //Procura pelo CNPJ nos condominos ativos
                    foreach (dSefip.CONDOMINIOSRow rowCONteste in Sefip1.DSefip.CONDOMINIOS)
                        if ((rowCONteste.CONSefip) && (!rowCONteste.IsCONCnpjNull()) && (rowCONteste.CONCnpj == par.cnpj.ToString()) && (rowCONteste.CONStatus == 1))
                        {
                            rowCON = rowCONteste;
                            break;
                        }
                    if (rowCON == null)
                    {
                        //Procura pelo CNPJ em todos os condominos
                        foreach (dSefip.CONDOMINIOSRow rowCONteste in Sefip1.DSefip.CONDOMINIOS)
                            if ((rowCONteste.CONSefip) && (!rowCONteste.IsCONCnpjNull()) && (rowCONteste.CONCnpj == par.cnpj.ToString()))
                            {
                                rowCON = rowCONteste;
                                break;
                            }
                    }
                    if (rowCON == null)
                    {
                        MessageBox.Show(string.Format("Guia da SEFIP para CNPJ n�o encontrado: {0}", par.cnpj));
                        continue;
                    }

                    dSefip.SEFipRow rowSefip = null;

                    //Procura a SEFIP pelo CNPJ
                    foreach (dSefip.SEFipRow rowteste in Sefip1.DSefip.SEFip)
                        if (rowteste.CONCnpj == par.cnpj.ToString())
                        {
                            rowSefip = rowteste;
                            break;
                        }

                    if (rowSefip == null)
                        // Procura a SEFIP pelo condom�nio
                        foreach (dSefip.SEFipRow rowteste in Sefip1.DSefip.SEFip)
                            if (rowteste.SEF_CON == rowCON.CON)
                            {
                                rowSefip = rowteste;
                                break;
                            }
                    if (rowSefip == null)
                    {
                        //MessageBox.Show(string.Format("Guia sem par: CNPJ:{0} Valor:{1:n2}", par.cnpj, par.valor));
                        rowSefip = Sefip1.DSefip.SEFip.NewSEFipRow();
                        rowSefip.SEF_CON = rowCON.CON;
                        rowSefip.CONCnpj = rowCON.CONCnpj;
                        rowSefip.SEF_EMP = rowCON.CON_EMP;
                        if (!rowCON.IsCONCodigoComunicacaoTributosNull())
                            rowSefip.SEFCodComunicacao = "117045";
                        rowSefip.SEFCompetencia = cCompet1.Comp.CompetenciaBind;
                        rowSefip.SEFTipoPag = (int)nTipoPag.Invalido;
                        //*** MRC - INICIO - TRIBUTOS (29/01/2015 12:00) ***
                        if ((rowCON.CONStatus == 1) && (!rowCON.IsCON_BCONull()))
                        {
                            if ((!rowCON.IsCONGPSEletronicoNull() && rowCON.CONGPSEletronico))
                            {
                                if (rowCON.CON_BCO == 341)
                                    rowSefip.SEFTipoPag = (int)nTipoPag.EletronicoItau;
                                else if ((rowCON.CON_BCO == 237) && (!rowCON.IsCONCodigoComunicacaoTributosNull()))
                                    rowSefip.SEFTipoPag = (int)nTipoPag.EletronicoBradesco;
                                else
                                    if (rowCON.CON_EMP == DSCentral.EMP)
                                    rowSefip.SEFTipoPag = (int)nTipoPag.Cheque;
                                else
                                    rowSefip.SEFTipoPag = (int)nTipoPag.ChequeFilial;
                            }
                            else
                                if (rowCON.CON_EMP == DSCentral.EMP)
                                rowSefip.SEFTipoPag = (int)nTipoPag.Cheque;
                            else
                                rowSefip.SEFTipoPag = (int)nTipoPag.ChequeFilial;
                        }
                        //*** MRC - TERMINO - TRIBUTOS (29/01/2015 12:00) ***
                        Sefip1.DSefip.SEFip.AddSEFipRow(rowSefip);
                    }
                    rowSefip.SEFValorSEFIP = par.ValorTotal;
                    rowSefip.SEFValorSEFIPOutras = par.ValorEntidades;
                    rowSefip.calNomeCondominio = string.Format("{0}{1}", rowCON.IsCONCodigoFolha1Null() ? "" : rowCON.CONCodigoFolha1 + " ", rowCON.CONNome);
                    Sefip1.DSefip.SEFipTableAdapterX(rowSefip.SEF_EMP).Update(rowSefip);

                    rowSefip.AcceptChanges();
                }
                Sefip1.DSefip.Recalcular();
                Sefip1.DSefip.SEFip.AcceptChanges();
            }
        }

        private dNOtAs _dNOtAs;
        private dNOtAs _dNOtAsF;

        private dNOtAs dNOtAs
        {
            get
            {
                if (_dNOtAs == null)
                    _dNOtAs = new dNOtAs();
                return _dNOtAs;
            }
        }

        private dNOtAs dNOtAsF
        {
            get
            {
                if (_dNOtAsF == null)
                    _dNOtAsF = new dNOtAs(new FrameworkProc.EMPTProc(FrameworkProc.EMPTipo.Filial));
                return _dNOtAsF;
            }
        }

        private dNOtAs dNOtAsX(int EMP)
        {
            return EMP == DSCentral.EMP ? dNOtAs : dNOtAsF;
        }

        private dNOtAs.NOtAsRow GetrowNOAGPS(dSefip.SEFipRow rowSEF)
        {
            dNOtAs.NOtAsRow rowNOA = dNOtAsX(rowSEF.SEF_EMP).NOtAs.NewNOtAsRow();
            
            rowNOA.NOA_CON = rowSEF.SEF_CON;
            rowNOA.NOA_PLA = "220016";
            rowNOA.NOAServico = "INSS - " + cCompet1.Comp;

            rowNOA.NOADataEmissao = new DateTime(cCompet1.Comp.Ano, cCompet1.Comp.Mes, 1).AddDays(-1);
            rowNOA.NOACompet = rowNOA.NOADataEmissao.Year * 100 + rowNOA.NOADataEmissao.Month;
            rowNOA.NOAAguardaNota = false;
            rowNOA.NOADATAI = DateTime.Now;
            rowNOA.NOAI_USU = DSCentral.USUX(rowSEF.SEF_EMP); 
            rowNOA.NOANumero = 0;

            rowNOA.NOAStatus = (int)NOAStatus.Cadastrada;
            rowNOA.NOATipo = (int)NOATipo.Folha;
            rowNOA.NOATotal = 0;
            rowNOA.NOADefaultPAGTipo = (int)PAGTipo.cheque;
            dNOtAsX(rowSEF.SEF_EMP).NOtAs.AddNOtAsRow(rowNOA);
            dNOtAsX(rowSEF.SEF_EMP).NOtAsTableAdapter.Update(rowNOA);            
            return rowNOA;
        }


        int Eletronicos;
        int Cheques;
        int ComErro;
        bool avisar;
        string UltimoCODCOM;
        DateTime? DataAgendarSel;
        int indLoop;

        private void CriaNotaFolha(dSefip.SEFipRow rowSEF,decimal Valor, Cheque NovoCheque, DateTime VencimentoEfetivo,int? GPS)
        {
            dNOtAs.NOtAsRow rowNOA = GetrowNOAGPS(rowSEF);
            rowNOA.NOATotal += Valor;
            dNOtAsX(rowSEF.SEF_EMP).NOtAsTableAdapter.Update(rowNOA);
            rowNOA.AcceptChanges();

            dNOtAs.PAGamentosRow rowPag = dNOtAsX(rowSEF.SEF_EMP).PAGamentos.NewPAGamentosRow();
            rowPag.PAG_CHE = NovoCheque.CHErow.CHE;
            rowPag.PAG_NOA = rowNOA.NOA;
            rowPag.PAGDATAI = DateTime.Now;
            rowPag.PAGI_USU = DSCentral.USUX(rowSEF.SEF_EMP);
            rowPag.PAGIMP = (int)TipoImposto.INSSpf;
            rowPag.PAGValor = Valor;
            rowPag.PAGVencimento = VencimentoEfetivo;
            rowPag.PAG_SEF = rowSEF.SEF;
            if (GPS.HasValue)
            {
                rowPag.PAG_GPS = GPS.Value;
                rowPag.PAGTipo = (int)PAGTipo.GuiaEletronica;
                rowPag.PAGPermiteAgrupar = false;
            }
            else
            {
                rowPag.PAGTipo = (int)PAGTipo.Guia;
                rowPag.PAGPermiteAgrupar = true; ;
            }            
            dNOtAsX(rowSEF.SEF_EMP).PAGamentos.AddPAGamentosRow(rowPag);
            dNOtAsX(rowSEF.SEF_EMP).PAGamentosTableAdapter.Update(rowPag);
            NovoCheque.AjustarTotalPelasParcelas();
        }

        /// <summary>
        /// Unidade de grava��o do GPS
        /// </summary>
        /// <param name="rowSEF"></param>
        /// <param name="ESP"></param>
        /// <remarks>esta � uma unidade de grava��o de um loop e nao deve ser chamada de outro local a nao ser em teste</remarks>
        private void unidadeCriaGPS(dSefip.SEFipRow rowSEF, CompontesBasicos.Espera.cEspera ESP)
        {
            //ATEN��O esta � uma unidade de grava��o de um loop e nao deve ser chamada de outro local a nao ser em teste.            
            ESP.Gauge();            
            FrameworkProc.EMPTipo EMPTipo = rowSEF.SEF_EMP == DSCentral.EMP ? FrameworkProc.EMPTipo.Local : FrameworkProc.EMPTipo.Filial;
            UltimoCODCOM = string.Format("Contador: {2} - CON: {0} - C�digo Folha: {1}", rowSEF.SEF_CON, rowSEF.IsSEFCodFolhaNull() ? "-" : rowSEF.SEFCodFolha.ToString(), indLoop++);
            //Console.WriteLine(UltimoCODCOM);
            decimal ValFolha = rowSEF.IsSEFValorFolhaNull() ? 0 : rowSEF.SEFValorFolha;
            decimal ValFolhaOutros = rowSEF.IsSEFValorFolhaOutrasNull() ? 0 : rowSEF.SEFValorFolhaOutras;
            decimal ValSefip = rowSEF.IsSEFValorSEFIPNull() ? 0 : rowSEF.SEFValorSEFIP;
            //Verifica se esta tudo ok
            decimal Totalagendado = 0;
            foreach (dSefip.PAGamentosRow rowPAG in rowSEF.GetPAGamentosRows())
                Totalagendado += rowPAG.PAGValor;
            if ((Totalagendado + ValFolha + ValFolhaOutros) != ValSefip)
            {
                ComErro++;
                return;
            }
            Cheque NovoCheque = null;            
            dllImpostoNeon.ImpostoNeon novoImp = new dllImpostoNeon.ImpostoNeon(TipoImposto.INSS) { Competencia = cCompet1.Comp };
            switch ((nTipoPag)rowSEF.SEFTipoPag)
            {
                case nTipoPag.EletronicoBradesco:
                    //Converte cheque em eletronico                                
                    FrameworkProc.datasets.dCondominiosAtivos.CONDOMINIOSRow rowCON = FrameworkProc.datasets.dCondominiosAtivos.dCondominiosTodosSTX(EMPTipo).CONDOMINIOS.FindByCON(rowSEF.SEF_CON);
                    decimal ValChequeConv = 0;
                    FrameworkProc.EMPTProc EMPTProc1 = new FrameworkProc.EMPTProc(EMPTipo);
                    NovoCheque = new Cheque(novoImp.VencimentoEfetivo, "INSS", rowSEF.SEF_CON, PAGTipo.Guia, false, null, null, "", false, EMPTProc1);
                    NovoCheque = NovoCheque.ConverteEletronico2100();
                    ValChequeConv = NovoCheque.CHErow.CHEValor;
                    //Insere Tributo na tabela GPS
                    dGPS.GPSRow novalinha = dGPS.dGPSSt.GPS.NewGPSRow();
                    string CEP = "0";
                    foreach (char c in rowCON.CONCep)
                        if (char.IsDigit(c))
                            CEP += c;
                    CPFCNPJ Cnpj = new CPFCNPJ(rowCON.CONCnpj);
                    novalinha.GPSNomeClientePagador = rowCON.CONNome;
                    novalinha.GPSTipo = (int)TipoImposto.INSS;
                    novalinha.GPS_CON = rowSEF.SEF_CON;
                    novalinha.EMP = rowSEF.SEF_EMP;
                    string manobra = string.Format("{0} - {1}", rowCON.CONCodigo, rowCON.CONEndereco);
                    if (manobra.Length > 40)
                        manobra = manobra.Substring(1, 40);
                    novalinha.GPSEndereco = manobra;
                    novalinha.GPSCEP = int.Parse(CEP);
                    novalinha.GPSUF = rowCON.CIDUf;
                    novalinha.GPSCidade = (rowCON.CIDNome.Length > 20) ? rowCON.CIDNome.Substring(0, 20) : rowCON.CIDNome;
                    novalinha.GPSBairro = rowCON.CONBairro;
                    novalinha.GPSCNPJ = Cnpj.Valor;
                    novalinha.GPSDataPagto = DataAgendarSel.Value;
                    novalinha.GPSValorPrincipal = ValFolha + ValChequeConv;
                    novalinha.GPSValorMulta = 0;
                    novalinha.GPSValorJuros = 0;
                    novalinha.GPSOutras = ValFolhaOutros;
                    novalinha.GPSValorTotal = ValFolha + ValFolhaOutros + ValChequeConv;
                    novalinha.GPSCodigoReceita = 2100;
                    novalinha.GPSIdentificador = Cnpj.Valor;
                    novalinha.GPSAno = cCompet1.Comp.Ano;
                    novalinha.GPSMes = cCompet1.Comp.Mes;
                    novalinha.GPSVencimento = novoImp.Vencimento;
                    novalinha.GPSNomeIdentificador = rowCON.CONNome;
                    
                    if (rowCON.IsCON_BCONull())
                        throw new Exception(string.Format("Condom�nio sem Banco: {0} - {1}", rowCON.CONCodigo, rowCON.CON));
                    if (rowCON.CON_BCO != 237)
                        throw new Exception(string.Format("Conta principal do Condom�nio n�o � Bradesco, portanto n�o pode fazer Pag. Eletr. Bradesco: {0} - {1}", rowCON.CONCodigo, rowCON.CON));
                    if (rowCON.IsCONAgenciaNull())
                        throw new Exception(string.Format("Condom�nio sem Ag�ncia: {0} - {1}", rowCON.CONCodigo, rowCON.CON));
                    novalinha.GPSAgencia = int.Parse(rowCON.CONAgencia);
                    if (rowCON.IsCONDigitoAgenciaNull())
                        throw new Exception(string.Format("Condom�nio sem D�gito Ag�ncia: {0} - {1}", rowCON.CONCodigo, rowCON.CON));
                    novalinha.GPSDigAgencia = rowCON.CONDigitoAgencia;
                    if (rowCON.IsCONContaNull())
                        throw new Exception(string.Format("Condom�nio sem Conta: {0} - {1}", rowCON.CONCodigo, rowCON.CON));
                    novalinha.GPSConta = int.Parse(rowCON.CONConta);
                    if (rowCON.IsCONDigitoContaNull())
                        throw new Exception(string.Format("Condom�nio sem D�gito Conta: {0} - {1}", rowCON.CONCodigo, rowCON.CON));
                    novalinha.GPSDigConta = rowCON.CONDigitoConta;
                    
                    novalinha.GPSCodComunicacao = "117045";

                    if (novalinha.GPSValorTotal > 15000)
                    {
                        novalinha.GPSStatus = (int)StatusArquivo.Liberar;
                        avisar = true;
                    }
                    else
                        novalinha.GPSStatus = (int)StatusArquivo.ParaEnviar;


                    novalinha.GPSOrigem = (int)OrigemImposto.Folha;
                    novalinha.GPSI_USU = DSCentral.USUX(EMPTipo);
                    novalinha.GPSDATAI = DateTime.Now;
                    dGPS.dGPSSt.GPS.AddGPSRow(novalinha);
                    dGPS.dGPSSt.GPSTableAdapterX(novalinha.EMP).Update(novalinha);
                    EMPTProc1.STTA.ExecutarSQLNonQuery("UPDATE PAGamentos SET PAG_GPS = @P3 FROM CHEques INNER JOIN PAGamentos ON CHEques.CHE = PAGamentos.PAG_CHE WHERE CHE = @P1 AND PAG_SEF = @P2",NovoCheque.CHE,rowSEF.SEF,novalinha.GPS);
                    if ((ValFolha + ValFolhaOutros) > 0)                    
                        CriaNotaFolha(rowSEF, ValFolha + ValFolhaOutros, NovoCheque, novoImp.VencimentoEfetivo, novalinha.GPS);                                        
                    Eletronicos++;
                    break;
                case nTipoPag.Cheque:                                        
                    if ((ValFolha + ValFolhaOutros) > 0)
                    {                        
                        NovoCheque = new Cheque(novoImp.VencimentoEfetivo, "INSS", rowSEF.SEF_CON, PAGTipo.Guia, false, 0, null, "", false, new FrameworkProc.EMPTProc(EMPTipo));
                        CriaNotaFolha(rowSEF, ValFolha + ValFolhaOutros, NovoCheque, novoImp.VencimentoEfetivo, null);                        
                    };                    
                    Cheques++;
                    break;
            };            

        }

        /// <summary>
        /// Esta fun��o s� est� disponivel em ambiente de testes
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void GerarUm(object sender, EventArgs e)
        {
            if (DSCentral.USU != 30)
                return;
            DataAgendarSel = DateTime.Today.SomaDiasUteis(1);
            if (VirInput.Input.Execute("Data de Pagamento", ref DataAgendarSel, DateTime.Today, DateTime.Today.AddDays(30)))
            {
                if (!DataAgendarSel.Value.IsUtil())
                {
                    MessageBox.Show("Data de Pagamento inv�lida (n�o � dia �til)");
                    return;
                }
            }
            else
                return;
            try
            {
                VirMSSQL.TableAdapter.AbreTrasacaoSQLDupla("dllSefip cSefipGuia - 927");
                dGPS.dGPSSt.GPSTableAdapter.EmbarcaEmTransST();
                dNOtAs.NOtAsTableAdapter.EmbarcaEmTransST();
                dNOtAs.PAGamentosTableAdapter.EmbarcaEmTransST();
                dGPS.dGPSSt.GPSTableAdapterF.EmbarcaEmTransST();
                dNOtAsF.NOtAsTableAdapter.EmbarcaEmTransST();
                dNOtAsF.PAGamentosTableAdapter.EmbarcaEmTransST();
                using (CompontesBasicos.Espera.cEspera ESP = new CompontesBasicos.Espera.cEspera(this))
                {
                    dSefip.SEFipRow rowSEF = (dSefip.SEFipRow)cSefipPagamentos1.gridView1.GetFocusedDataRow();
                    unidadeCriaGPS(rowSEF, ESP);
                }
                VirMSSQL.TableAdapter.CommitSQLDuplo();
            }
            catch (Exception ex)
            {
                VirMSSQL.TableAdapter.VircatchSQL(ex);
                string errostr = string.Format("Erro no condom�nio: {0}\r\n\r\n{1}", UltimoCODCOM, ex.Message);
                //MessageBox.Show(errostr);
                throw new Exception(errostr, ex);
            }
        }


        private void GerarTodos_Click(object sender, EventArgs e)
        {
            AtualizarPaginaAtiva(true);
            if (CompontesBasicosProc.FuncoesBasicasProc.EstaEmProducao)
                if (VirInput.Input.Senha("Senha GPS") != "CUIDADO-GPS")
                {
                    MessageBox.Show("Senha incorreta");
                    return;
                }
        
            DataAgendarSel = DateTime.Today.SomaDiasUteis(1);
            if (VirInput.Input.Execute("Data de Pagamento", ref DataAgendarSel, DateTime.Today, DateTime.Today.AddDays(30)))
            {
                if (!DataAgendarSel.Value.IsUtil())
                {
                    MessageBox.Show("Data de Pagamento inv�lida (n�o � dia �til)");
                    return;
                }
            }
            else
                return;            

            Eletronicos = 0;
            Cheques = 0;
            ComErro = 0;
            avisar = false;
            UltimoCODCOM = "???";
            indLoop = 0;
            try
            {
                VirMSSQL.TableAdapter.AbreTrasacaoSQLDupla("dllSefip cSefipGuia - 927");                
                dGPS.dGPSSt.GPSTableAdapter.EmbarcaEmTransST();
                dNOtAs.NOtAsTableAdapter.EmbarcaEmTransST();
                dNOtAs.PAGamentosTableAdapter.EmbarcaEmTransST();
                dGPS.dGPSSt.GPSTableAdapterF.EmbarcaEmTransST();
                dNOtAsF.NOtAsTableAdapter.EmbarcaEmTransST();
                dNOtAsF.PAGamentosTableAdapter.EmbarcaEmTransST();
                using (CompontesBasicos.Espera.cEspera ESP = new CompontesBasicos.Espera.cEspera(this))
                {
                    ESP.AtivaGauge(Sefip1.DSefip.SEFip.Count);
                    Application.DoEvents();
                    foreach (dSefip.SEFipRow rowSEF in Sefip1.DSefip.SEFip)                                            
                        unidadeCriaGPS(rowSEF, ESP);                    
                    VirMSSQL.TableAdapter.CommitSQLDuplo();
                }
                string men = "";
                if (Eletronicos > 0)
                    men += string.Format("Eletr�nicos: {0}\r\n", Eletronicos);
                if (Cheques > 0)
                    men += string.Format("Cheques: {0}\r\n", Cheques);
                if (men != "")
                {
                    labelControl1.Text = men;
                    labelControl1.Visible = true;
                }
                MessageBox.Show("ok\r\n" + men);
                if (avisar)
                    MessageBox.Show("ATEN��O\r\n\r\nExistem guias retidas aguardando libera��o!!!");
                Sefip1.DSefip.Recalcular();
                if (ComErro != 0)
                    MessageBox.Show(string.Format("ATEN��O\r\n\r\nExistem {0} guias com erro que foram ignoradas!!!", ComErro));
                //NoasList = null;
                //Framework.objetosNeon.Competencia Comp1 = Comp++;
                //DateTime Vencimeto = new DateTime(Comp1.Ano, Comp1.Mes, 20);
                //Cheque NovoCheque = new Cheque(Vencimeto, "INSS", rowSEF.SEF_CON, dCheque.PAGTipo.Guia, false);

                //dNOtAs.NOtAsRow rowNOA = GetrowNOAGPS(rowSEF);
                //rowNOA.NOATotal = ValFolha + ValFolhaOutros;
                //dNOtAs.NOtAsTableAdapter.Update(rowNOA);
                //rowNOA.AcceptChanges();
                /*
                DESMEMBRAR
                dNOtAs.PAGamentosRow rowPag = dNOtAs.PAGamentos.NewPAGamentosRow();
                rowPag.PAG_CHE = NovoCheque.CHErow.CHE;
                rowPag.PAG_NOA = rowNOA.NOA;
                rowPag.PAGDATAI = DateTime.Now;
                rowPag.PAGI_USU = Framework.usuarioLogado.UsuarioLogadoNeon.UsuarioLogadoStNEON.USU;
                rowPag.PAGTipo = (int)dCheque.PAGTipo.Guia;
                rowPag.PAGValor = row.Campo11;
                //usar o objeto do imposto
                //DateTime USAROIMPOSTO
                rowPag.PAGVencimento = new DateTime(2010, 10, 20);
                        
                dNOtAs.PAGamentos.AddPAGamentosRow(rowPag);
                dNOtAs.PAGamentosTableAdapter.Update(rowPag);
                rowPag.AcceptChanges();
                NovoCheque.AjustarTotalPelasParcelas();
                Cheques++;
                */


            }
            catch (Exception ex)
            {
                VirMSSQL.TableAdapter.VircatchSQL(ex);                
                string errostr = string.Format("Erro no condom�nio: {0}\r\n\r\n{1}", UltimoCODCOM, ex.Message);
                //MessageBox.Show(errostr);                
                throw new Exception(errostr, ex);
            }
        }

        /// <summary>
        /// Status da SEFIP
        /// </summary>
        public Framework.datasets.AGGStatus Status;

        private void cCompet1_OnChange(object sender, EventArgs e)
        {
            CompetenciaAlterada();            
        }
        

        private void CompetenciaAlterada()
        {
            if (cCompet1.Comp < Framework.datasets.dAGendaGeral.dAGendaGeralSt.GetAGBCompetencia(Framework.datasets.AGGTipo.SEFIP))
            {
                Status = Framework.datasets.AGGStatus.Terminado;
                BFechar.Visible = chMenu.Visible = false;
                cSefipPagamentos1.Travar = true;
                chMenu.Checked = true;
            }
            else
            {

                Status = Framework.datasets.dAGendaGeral.dAGendaGeralSt.GetAGBStatus(Framework.datasets.AGGTipo.SEFIP);
                chMenu.Visible = true;
                BFechar.Visible = (Status == Framework.datasets.AGGStatus.EmProducao);
                cSefipPagamentos1.Travar = false;
                chMenu.Checked = false;
                
            }
            MostraStatus();            
        }

        private void MostraStatus()
        {
            panelControlSuperior.BackColor = Framework.datasets.virEnumAGGStatus.virEnumAGGStatusSt.GetCor(Status);
            TxStatus.Text = Framework.datasets.virEnumAGGStatus.virEnumAGGStatusSt.Descritivo(Status);
            AtualizarPaginaAtiva(false);
        }

        private void chMenu_CheckedChanged(object sender, EventArgs e)
        {
            panelControlMenu.Visible = !chMenu.Checked;
        }

        private void AtualizarPaginaAtiva(bool forcar)
        {
            if ((Status == Framework.datasets.AGGStatus.Terminado) && (!forcar))
            {
                Sefip1.DSefip.INSSPago.Clear();
                Sefip1.DSefip.SefipAc.Clear();
                Sefip1.DSefip.PAGamentos.Clear();
                Sefip1.DSefip.SEFip.Clear();
                BTCalcular.Visible = true;
                return;
            }
            BTCalcular.Visible = false;
            CarregaRetencoes();

            Sefip1.DSefip.PAGamentos.Clear();
            int Carregados = Sefip1.DSefip.SEFipTableAdapter.FillByCompet(Sefip1.DSefip.SEFip, cCompet1.Comp.CompetenciaBind);
            Carregados += Sefip1.DSefip.SEFipTableAdapterF.FillByCompet(Sefip1.DSefip.SEFip, cCompet1.Comp.CompetenciaBind);
            if (Carregados > 0)
            {
                Sefip1.DSefip.Recalcular();
                
            }


            if (xtraTabControl1.SelectedTabPage == xtraTabPageRetencoes)
            {                
            }
            else if (xtraTabControl1.SelectedTabPage == xtraTabPageSefip)
            {
                /*
                Sefip1.DSefip.PAGamentos.Clear();
                int Carregados = Sefip1.DSefip.SEFipTableAdapter.FillByCompet(Sefip1.DSefip.SEFip, cCompet1.Comp.CompetenciaBind);
                Carregados += Sefip1.DSefip.SEFipTableAdapterF.FillByCompet(Sefip1.DSefip.SEFip, cCompet1.Comp.CompetenciaBind);
                if (Carregados > 0)
                {
                    Sefip1.DSefip.Recalcular();
                    cSefipPagamentos1.Travar = (Status == Framework.datasets.AGGStatus.Terminado);
                    simpleButton2.Enabled = simpleButton3.Enabled = simpleButton4.Enabled = true;
                }
                else
                {
                    xtraTabControl1.SelectedTabPage = xtraTabPageRetencoes;
                    simpleButton2.Enabled = simpleButton3.Enabled = simpleButton4.Enabled = false;
                }*/
            }
            else if (xtraTabControl1.SelectedTabPage == xtraTabPageHistorico)
            {
                memoEdit1.Text = Framework.datasets.dAGendaGeral.dAGendaGeralSt.GetAGBHistorico(Framework.datasets.AGGTipo.SEFIP);
            }
        }        

        private void simpleButton5_Click(object sender, EventArgs e)
        {
            string Comentario = "";
            if (VirInput.Input.Execute("Anota��o:", ref Comentario) && (Comentario != ""))
                Framework.datasets.dAGendaGeral.dAGendaGeralSt.GravaObs(Framework.datasets.AGGTipo.SEFIP, Comentario);
            AtualizarPaginaAtiva(true);
        }

        private void simpleButton6_Click(object sender, EventArgs e)
        {
            if ((Sefip1.DSefip.CONSemSEFIP == null) && (Status != Framework.datasets.AGGStatus.NaoIniciado))
                cSefip2.Carrega(cCompet1.Comp);

            if (Sefip1.DSefip.CONSemSEFIP != null)
                foreach (string strcnpj in Sefip1.DSefip.CONSemSEFIP)
                {
                    CPFCNPJ cnpj = new CPFCNPJ(strcnpj);
                    cSefip2.imprimeRelatorio(cnpj, "");
                }
        }

        private void BFechar_Click(object sender, EventArgs e)
        {
            Competencia NovaComp = cCompet1.Comp;
            if (NovaComp != Framework.datasets.dAGendaGeral.dAGendaGeralSt.GetAGBCompetencia(Framework.datasets.AGGTipo.SEFIP))
                return;
            NovaComp++;
            Framework.datasets.dAGendaGeral.dAGendaGeralSt.SetAGBCompetencia(Framework.datasets.AGGTipo.SEFIP,NovaComp);
            Framework.datasets.dAGendaGeral.dAGendaGeralSt.SetAGBStatus(Framework.datasets.AGGTipo.SEFIP,Framework.datasets.AGGStatus.NaoIniciado);
            cCompet1.CompMaxima = NovaComp.CloneCompet();
            cCompet1.Comp = NovaComp;
            cCompet1.AjustaTela();
            CompetenciaAlterada();
        }

        //private System.Collections.Generic.SortedList<Int64,dllSefip.ImpressoGuia> ImpressosGuias;

        private bool MergePDF(System.Collections.Generic.SortedList<long, ImpressoGuia> lstArquivos, string strArquivo)
        {
            //Inicia vari�veis
            PdfReader reader = null;
            Document sourceDocument = null;
            PdfCopy pdfCopyProvider = null;
            PdfImportedPage importedPage;

            try
            {
                //Cria um novo documento
                sourceDocument = new Document();
                pdfCopyProvider = new PdfCopy(sourceDocument, new FileStream(strArquivo, FileMode.Create));

                //Abre o documento
                sourceDocument.Open();

                //Varre cada arquivo a ser inserido no novo documento
                foreach (ImpressoGuia impG in lstArquivos.Values)
                {
                    //Insere
                    reader = new PdfReader(impG.NomeArquivo);
                    importedPage = pdfCopyProvider.GetImportedPage(reader, 1);
                    pdfCopyProvider.AddPage(importedPage);
                    reader.Close();
                }

                //Salva o documento
                sourceDocument.Close();
            }
            catch { return false; }
            return true;
        }

        private void simpleButton9_Click(object sender, EventArgs e)
        {
            if ((ImpressosGuias == null) || (ImpressosGuias.Count == 0))
                CarregaImpressosGuias();
            string Path = Application.StartupPath + @"\TMP\";
            string strArquivo = String.Format("{0}Comprovante.pdf", Path);
            if (!Directory.Exists(Path))
                Directory.CreateDirectory(Path);
            if (File.Exists(strArquivo))
                try
                { File.Delete(strArquivo); }
                catch
                { strArquivo = String.Format("{0}Comprovante_{1:ddMMyyhhmmss}.pdf", Path, DateTime.Now); };
            System.Threading.Thread.Sleep(3000);
            System.Collections.Generic.SortedList<long, ImpressoGuia> lstArquivos = new System.Collections.Generic.SortedList<long, ImpressoGuia>();
            int semnumero = -1;
            foreach (dSefip.SEFipRow row in Sefip1.DSefip.SEFip) //dSefip1.SEFip)
            {

                if (row.SEFTipoPag == (int)nTipoPag.Cheque)
                {
                    CPFCNPJ cnpj = new CPFCNPJ(row.CONCnpj);
                    long numero = row.IsSEFCodFolhaNull() ? semnumero-- : row.SEFCodFolha;
                    if ((cnpj.Tipo == TipoCpfCnpj.CNPJ) && (ImpressosGuias.ContainsKey(cnpj.Valor)))
                        lstArquivos.Add(numero, ImpressosGuias[cnpj.Valor]);
                }

            }
            if (MergePDF(lstArquivos, strArquivo))
            {
                //Avisa caso nao tenha encontrado algum comprovante
                //if (booFaltaComprovante)
                //    MessageBox.Show("ATEN��O: Alguns comprovantes selecionados n�o foram encontrados.");

                //Abre o documento 
                System.Diagnostics.Process.Start(strArquivo);
            }
            


        }

        private void simpleButton10_Click(object sender, EventArgs e)
        {
            if ((ImpressosGuias == null) || (ImpressosGuias.Count == 0))
                CarregaImpressosGuias();
            //string Path = System.Windows.Forms.Application.StartupPath + @"\TMP\";
            //string strArquivo = String.Format("{0}Comprovante.pdf", Path);
            //if (!System.IO.Directory.Exists(Path))
            //    System.IO.Directory.CreateDirectory(Path);
            //if (System.IO.File.Exists(strArquivo))
            //    try
            //    { System.IO.File.Delete(strArquivo); }
            //    catch
            //    { strArquivo = String.Format("{0}Comprovante_{1:ddMMyyhhmmss}.pdf", Path, DateTime.Now); };
            //System.Threading.Thread.Sleep(3000);
            //System.Collections.Generic.SortedList<long, dllSefip.ImpressoGuia> lstArquivos = new System.Collections.Generic.SortedList<long, ImpressoGuia>();
            //int semnumero = -1;

            dSefip.SEFipRow row = (dSefip.SEFipRow)cSefipPagamentos1.gridView1.GetFocusedDataRow();
            if (row == null)
                return;
            CPFCNPJ cnpj = new CPFCNPJ(row.CONCnpj);
            if (ImpressosGuias.ContainsKey(cnpj.Valor))
                System.Diagnostics.Process.Start(ImpressosGuias[cnpj.Valor].NomeArquivo);
            
        }        

        private void cBotaoImpNeon1_clicado(object sender, dllBotao.BotaoArgs args)
        {
            string strPIS = "";
            if (VirInput.Input.Execute("PIS", ref strPIS))
            {
                if (!PIS.IsValid(strPIS))
                {
                    MessageBox.Show("PIS inv�lido");
                    return;
                }
                PIS PIS1 = new PIS(strPIS);
                Relatorios.impPIS impresso = new Relatorios.impPIS();
                impresso.dimpPIS1.INSSporPISTableAdapter.ClearBeforeFill = false;
                impresso.dimpPIS1.INSSporPISTableAdapterF.ClearBeforeFill = false;
                //string teste = PIS1.ToString(true);
                impresso.dimpPIS1.INSSporPISTableAdapter.FillFRN(impresso.dimpPIS1.INSSporPIS, PIS1.Valor);
                impresso.dimpPIS1.INSSporPISTableAdapterF.FillFRN(impresso.dimpPIS1.INSSporPIS, PIS1.Valor);
                impresso.dimpPIS1.INSSporPISTableAdapter.FillPES(impresso.dimpPIS1.INSSporPIS, PIS1.ToString(true));
                impresso.dimpPIS1.INSSporPISTableAdapterF.FillPES(impresso.dimpPIS1.INSSporPIS, PIS1.ToString(true));
                impresso.dimpPIS1.calculaCompetencia();
                impresso.CreateDocument();
                cBotaoImpNeon1.Impresso = impresso;
                /*
                switch (args.Botao)
                {
                    case dllBotao.Botao.Planilha:
                        break;                                        
                    case dllBotao.Botao.email:
                        break;
                    case dllBotao.Botao.imprimir:
                        impresso.Print();
                        break;
                    case dllBotao.Botao.nulo:
                        break;
                    case dllBotao.Botao.pdf:
                        break;
                    case dllBotao.Botao.botao:
                    case dllBotao.Botao.tela:
                        impresso.ShowPreviewDialog();
                        break;
                    default:
                        break;
                } */
            }
        }

        private void BTCalcular_Click(object sender, EventArgs e)
        {
            AtualizarPaginaAtiva(true);
        }

        private void simpleButton1_Click_1(object sender, EventArgs e)
        {
            Sefip1.DSefip.SEFIPNegativaTableAdapter.Fill(Sefip1.DSefip.SEFIPNegativa, cCompet1.Comp.CompetenciaBind - 1, DSCentral.EMP, cCompet1.Comp.CompetenciaBind);
            System.Text.StringBuilder Relatorio = new System.Text.StringBuilder("**** Rela��o de condom�nos para SEFIP negativa ****\r\n\r\n * SBC *\r\n");
            foreach (dSefip.SEFIPNegativaRow rowSEFN in Sefip1.DSefip.SEFIPNegativa)
                Relatorio.AppendFormat("Condom�nio: {0:000000} - {1:000} - {2}\r\n",rowSEFN.CONCodigo,rowSEFN.CONCodigoFolha1,rowSEFN.CONNome);
            Sefip1.DSefip.SEFIPNegativaTableAdapterF.Fill(Sefip1.DSefip.SEFIPNegativa, cCompet1.Comp.CompetenciaBind - 1, DSCentral.EMPF, cCompet1.Comp.CompetenciaBind);
            Relatorio.Append("\r\n\r\n==========================\r\n\r\n * SA *\r\n");
            foreach (dSefip.SEFIPNegativaRow rowSEFN in Sefip1.DSefip.SEFIPNegativa)
                Relatorio.AppendFormat("Condom�nio: {0:000000} - {1:000} - {2}", rowSEFN.CONCodigo, rowSEFN.CONCodigoFolha1, rowSEFN.CONNome);
            Clipboard.SetText(Relatorio.ToString());
            MessageBox.Show(Relatorio.ToString());
            MessageBox.Show("Relat�rio na �rea de tranfer�ncia");
        }
    }
}
