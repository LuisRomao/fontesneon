﻿namespace Orcamento
{
    partial class cAbrir
    {
        /// <summary>
        /// Variável de designer necessária.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Limpar os recursos que estão sendo usados.
        /// </summary>
        /// <param name="disposing">true se for necessário descartar os recursos gerenciados; caso contrário, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Código gerado pelo Windows Form Designer

        /// <summary>
        /// Método necessário para suporte ao Designer - não modifique 
        /// o conteúdo deste método com o editor de código.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(cAbrir));
            DevExpress.XtraEditors.Controls.EditorButtonImageOptions editorButtonImageOptions3 = new DevExpress.XtraEditors.Controls.EditorButtonImageOptions();
            DevExpress.XtraEditors.Controls.EditorButtonImageOptions editorButtonImageOptions4 = new DevExpress.XtraEditors.Controls.EditorButtonImageOptions();
            this.radioGroup1 = new DevExpress.XtraEditors.RadioGroup();
            this.xtraTabControl = new DevExpress.XtraTab.XtraTabControl();
            this.xtraTabPageModelo = new DevExpress.XtraTab.XtraTabPage();
            this.gridControl1 = new DevExpress.XtraGrid.GridControl();
            this.dModelosBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.gridView1 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colMBA = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colMBANome = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colMBAOBS = new DevExpress.XtraGrid.Columns.GridColumn();
            this.xtraTabPageORC = new DevExpress.XtraTab.XtraTabPage();
            this.panelControl2 = new DevExpress.XtraEditors.PanelControl();
            this.gridControl2 = new DevExpress.XtraGrid.GridControl();
            this.dOrcamentoBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.gridView2 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colORC = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colORC_CON = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colORCCompI = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemCompetenciaEdit1 = new CompontesBasicos.RepositoryItemCompetenciaEdit();
            this.colORCCompF = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colORCStatus = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemImageComboBox1 = new DevExpress.XtraEditors.Repository.RepositoryItemImageComboBox();
            this.panelControl1 = new DevExpress.XtraEditors.PanelControl();
            this.simpleButton1 = new DevExpress.XtraEditors.SimpleButton();
            this.cCompetF = new Framework.objetosNeon.cCompet();
            this.cCompetI = new Framework.objetosNeon.cCompet();
            this.lookupCondominio_F1 = new Framework.Lookup.LookupCondominio_F();
            this.openFileDialog1 = new System.Windows.Forms.OpenFileDialog();
            ((System.ComponentModel.ISupportInitialize)(this.BarManager_F)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.StyleController_F)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radioGroup1.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xtraTabControl)).BeginInit();
            this.xtraTabControl.SuspendLayout();
            this.xtraTabPageModelo.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dModelosBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).BeginInit();
            this.xtraTabPageORC.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl2)).BeginInit();
            this.panelControl2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dOrcamentoBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCompetenciaEdit1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemImageComboBox1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl1)).BeginInit();
            this.panelControl1.SuspendLayout();
            this.SuspendLayout();
            // 
            // barComponente_F
            // 
            this.barComponente_F.OptionsBar.AllowQuickCustomization = false;
            this.barComponente_F.OptionsBar.DisableClose = true;
            this.barComponente_F.OptionsBar.DisableCustomization = true;
            this.barComponente_F.OptionsBar.DrawDragBorder = false;
            this.barComponente_F.OptionsBar.UseWholeRow = true;
            // 
            // btnConfirmar_F
            // 
            this.btnConfirmar_F.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("btnConfirmar_F.ImageOptions.Image")));
            // 
            // btnCancelar_F
            // 
            this.btnCancelar_F.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("btnCancelar_F.ImageOptions.Image")));
            // 
            // btnFechar_F
            // 
            this.btnFechar_F.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("btnFechar_F.ImageOptions.Image")));
            // 
            // ToolTipController_F
            // 
            this.ToolTipController_F.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.ToolTipController_F.Appearance.ForeColor = System.Drawing.Color.Black;
            this.ToolTipController_F.Appearance.Options.UseBackColor = true;
            this.ToolTipController_F.Appearance.Options.UseFont = true;
            this.ToolTipController_F.Appearance.Options.UseForeColor = true;
            this.ToolTipController_F.Appearance.Options.UseTextOptions = true;
            this.ToolTipController_F.Appearance.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            // 
            // StyleController_F
            // 
            this.StyleController_F.AppearanceDisabled.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this.StyleController_F.AppearanceDisabled.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this.StyleController_F.AppearanceDisabled.Options.UseBackColor = true;
            this.StyleController_F.AppearanceDisabled.Options.UseForeColor = true;
            this.StyleController_F.AppearanceFocused.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.StyleController_F.AppearanceFocused.Options.UseBackColor = true;
            // 
            // radioGroup1
            // 
            this.radioGroup1.Dock = System.Windows.Forms.DockStyle.Left;
            this.radioGroup1.Location = new System.Drawing.Point(0, 35);
            this.radioGroup1.MenuManager = this.BarManager_F;
            this.radioGroup1.Name = "radioGroup1";
            this.radioGroup1.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.radioGroup1.Properties.Appearance.Options.UseFont = true;
            this.radioGroup1.Properties.Items.AddRange(new DevExpress.XtraEditors.Controls.RadioGroupItem[] {
            new DevExpress.XtraEditors.Controls.RadioGroupItem(0, "Arquivo"),
            new DevExpress.XtraEditors.Controls.RadioGroupItem(1, "Modelo"),
            new DevExpress.XtraEditors.Controls.RadioGroupItem(2, "Condomínio")});
            this.radioGroup1.Size = new System.Drawing.Size(176, 614);
            this.radioGroup1.TabIndex = 4;
            this.radioGroup1.EditValueChanged += new System.EventHandler(this.radioGroup1_EditValueChanged);
            // 
            // xtraTabControl
            // 
            this.xtraTabControl.Dock = System.Windows.Forms.DockStyle.Fill;
            this.xtraTabControl.Location = new System.Drawing.Point(176, 35);
            this.xtraTabControl.Name = "xtraTabControl";
            this.xtraTabControl.SelectedTabPage = this.xtraTabPageModelo;
            this.xtraTabControl.ShowTabHeader = DevExpress.Utils.DefaultBoolean.True;
            this.xtraTabControl.Size = new System.Drawing.Size(843, 614);
            this.xtraTabControl.TabIndex = 5;
            this.xtraTabControl.TabPages.AddRange(new DevExpress.XtraTab.XtraTabPage[] {
            this.xtraTabPageModelo,
            this.xtraTabPageORC});
            // 
            // xtraTabPageModelo
            // 
            this.xtraTabPageModelo.Controls.Add(this.gridControl1);
            this.xtraTabPageModelo.Name = "xtraTabPageModelo";
            this.xtraTabPageModelo.PageVisible = false;
            this.xtraTabPageModelo.Size = new System.Drawing.Size(837, 586);
            this.xtraTabPageModelo.Text = "Modelos";
            // 
            // gridControl1
            // 
            this.gridControl1.DataMember = "ModeloBAlancete";
            this.gridControl1.DataSource = this.dModelosBindingSource;
            this.gridControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridControl1.Location = new System.Drawing.Point(0, 0);
            this.gridControl1.MainView = this.gridView1;
            this.gridControl1.MenuManager = this.BarManager_F;
            this.gridControl1.Name = "gridControl1";
            this.gridControl1.Size = new System.Drawing.Size(837, 586);
            this.gridControl1.TabIndex = 0;
            this.gridControl1.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridView1});
            // 
            // dModelosBindingSource
            // 
            this.dModelosBindingSource.DataSource = typeof(OrcamentoProc.dModelos);
            this.dModelosBindingSource.Position = 0;
            // 
            // gridView1
            // 
            this.gridView1.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colMBA,
            this.colMBANome,
            this.colMBAOBS});
            this.gridView1.GridControl = this.gridControl1;
            this.gridView1.Name = "gridView1";
            this.gridView1.OptionsCustomization.AllowGroup = false;
            this.gridView1.OptionsView.ShowAutoFilterRow = true;
            this.gridView1.OptionsView.ShowGroupPanel = false;
            this.gridView1.FocusedRowChanged += new DevExpress.XtraGrid.Views.Base.FocusedRowChangedEventHandler(this.gridView1_FocusedRowChanged);
            // 
            // colMBA
            // 
            this.colMBA.FieldName = "MBA";
            this.colMBA.Name = "colMBA";
            // 
            // colMBANome
            // 
            this.colMBANome.Caption = "Modelo";
            this.colMBANome.FieldName = "MBANome";
            this.colMBANome.Name = "colMBANome";
            this.colMBANome.Visible = true;
            this.colMBANome.VisibleIndex = 0;
            this.colMBANome.Width = 512;
            // 
            // colMBAOBS
            // 
            this.colMBAOBS.Caption = "Obs";
            this.colMBAOBS.FieldName = "MBAOBS";
            this.colMBAOBS.Name = "colMBAOBS";
            this.colMBAOBS.Visible = true;
            this.colMBAOBS.VisibleIndex = 1;
            this.colMBAOBS.Width = 1110;
            // 
            // xtraTabPageORC
            // 
            this.xtraTabPageORC.Controls.Add(this.panelControl2);
            this.xtraTabPageORC.Controls.Add(this.panelControl1);
            this.xtraTabPageORC.Name = "xtraTabPageORC";
            this.xtraTabPageORC.PageVisible = false;
            this.xtraTabPageORC.Size = new System.Drawing.Size(837, 586);
            this.xtraTabPageORC.Text = "Orçamento";
            // 
            // panelControl2
            // 
            this.panelControl2.Controls.Add(this.gridControl2);
            this.panelControl2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelControl2.Location = new System.Drawing.Point(0, 52);
            this.panelControl2.Name = "panelControl2";
            this.panelControl2.Size = new System.Drawing.Size(837, 534);
            this.panelControl2.TabIndex = 19;
            // 
            // gridControl2
            // 
            this.gridControl2.DataMember = "ORCamento";
            this.gridControl2.DataSource = this.dOrcamentoBindingSource;
            this.gridControl2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridControl2.Location = new System.Drawing.Point(2, 2);
            this.gridControl2.MainView = this.gridView2;
            this.gridControl2.MenuManager = this.BarManager_F;
            this.gridControl2.Name = "gridControl2";
            this.gridControl2.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemCompetenciaEdit1,
            this.repositoryItemImageComboBox1});
            this.gridControl2.Size = new System.Drawing.Size(833, 530);
            this.gridControl2.TabIndex = 17;
            this.gridControl2.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridView2});
            // 
            // dOrcamentoBindingSource
            // 
            this.dOrcamentoBindingSource.DataSource = typeof(OrcamentoProc.dOrcamento);
            this.dOrcamentoBindingSource.Position = 0;
            // 
            // gridView2
            // 
            this.gridView2.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colORC,
            this.colORC_CON,
            this.colORCCompI,
            this.colORCCompF,
            this.colORCStatus});
            this.gridView2.GridControl = this.gridControl2;
            this.gridView2.Name = "gridView2";
            this.gridView2.OptionsBehavior.Editable = false;
            this.gridView2.OptionsView.ColumnAutoWidth = false;
            this.gridView2.OptionsView.ShowGroupPanel = false;
            this.gridView2.SortInfo.AddRange(new DevExpress.XtraGrid.Columns.GridColumnSortInfo[] {
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colORCCompI, DevExpress.Data.ColumnSortOrder.Ascending)});
            this.gridView2.RowCellStyle += new DevExpress.XtraGrid.Views.Grid.RowCellStyleEventHandler(this.gridView2_RowCellStyle);
            this.gridView2.FocusedRowChanged += new DevExpress.XtraGrid.Views.Base.FocusedRowChangedEventHandler(this.gridView2_FocusedRowChanged);
            // 
            // colORC
            // 
            this.colORC.FieldName = "ORC";
            this.colORC.Name = "colORC";
            // 
            // colORC_CON
            // 
            this.colORC_CON.FieldName = "ORC_CON";
            this.colORC_CON.Name = "colORC_CON";
            // 
            // colORCCompI
            // 
            this.colORCCompI.Caption = "Início";
            this.colORCCompI.ColumnEdit = this.repositoryItemCompetenciaEdit1;
            this.colORCCompI.FieldName = "ORCCompI";
            this.colORCCompI.Name = "colORCCompI";
            this.colORCCompI.SortMode = DevExpress.XtraGrid.ColumnSortMode.Value;
            this.colORCCompI.Visible = true;
            this.colORCCompI.VisibleIndex = 0;
            this.colORCCompI.Width = 104;
            // 
            // repositoryItemCompetenciaEdit1
            // 
            this.repositoryItemCompetenciaEdit1.AutoHeight = false;
            this.repositoryItemCompetenciaEdit1.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Ellipsis, "", -1, true, false, false, editorButtonImageOptions3),
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Left, "", -1, true, true, true, editorButtonImageOptions4),
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Right)});
            this.repositoryItemCompetenciaEdit1.Mask.EditMask = "00/0000";
            this.repositoryItemCompetenciaEdit1.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Simple;
            this.repositoryItemCompetenciaEdit1.Name = "repositoryItemCompetenciaEdit1";
            // 
            // colORCCompF
            // 
            this.colORCCompF.Caption = "Término";
            this.colORCCompF.ColumnEdit = this.repositoryItemCompetenciaEdit1;
            this.colORCCompF.FieldName = "ORCCompF";
            this.colORCCompF.Name = "colORCCompF";
            this.colORCCompF.Visible = true;
            this.colORCCompF.VisibleIndex = 1;
            this.colORCCompF.Width = 122;
            // 
            // colORCStatus
            // 
            this.colORCStatus.Caption = "Status";
            this.colORCStatus.ColumnEdit = this.repositoryItemImageComboBox1;
            this.colORCStatus.FieldName = "ORCStatus";
            this.colORCStatus.Name = "colORCStatus";
            this.colORCStatus.Visible = true;
            this.colORCStatus.VisibleIndex = 2;
            this.colORCStatus.Width = 194;
            // 
            // repositoryItemImageComboBox1
            // 
            this.repositoryItemImageComboBox1.AutoHeight = false;
            this.repositoryItemImageComboBox1.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemImageComboBox1.Name = "repositoryItemImageComboBox1";
            // 
            // panelControl1
            // 
            this.panelControl1.Controls.Add(this.simpleButton1);
            this.panelControl1.Controls.Add(this.cCompetF);
            this.panelControl1.Controls.Add(this.cCompetI);
            this.panelControl1.Controls.Add(this.lookupCondominio_F1);
            this.panelControl1.Dock = System.Windows.Forms.DockStyle.Top;
            this.panelControl1.Location = new System.Drawing.Point(0, 0);
            this.panelControl1.Name = "panelControl1";
            this.panelControl1.Size = new System.Drawing.Size(837, 52);
            this.panelControl1.TabIndex = 18;
            // 
            // simpleButton1
            // 
            this.simpleButton1.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("simpleButton1.ImageOptions.Image")));
            this.simpleButton1.Location = new System.Drawing.Point(371, 6);
            this.simpleButton1.Name = "simpleButton1";
            this.simpleButton1.Size = new System.Drawing.Size(135, 42);
            this.simpleButton1.TabIndex = 19;
            this.simpleButton1.Text = "Novo";
            this.simpleButton1.Click += new System.EventHandler(this.simpleButton1_Click);
            // 
            // cCompetF
            // 
            this.cCompetF.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.cCompetF.Appearance.Options.UseBackColor = true;
            this.cCompetF.CaixaAltaGeral = true;
            this.cCompetF.Doc = System.Windows.Forms.DockStyle.None;
            this.cCompetF.Estado = CompontesBasicos.EstadosDosComponentes.Registrado;
            this.cCompetF.IsNull = false;
            this.cCompetF.Location = new System.Drawing.Point(690, 14);
            this.cCompetF.LookAndFeel.Style = DevExpress.LookAndFeel.LookAndFeelStyle.Office2003;
            this.cCompetF.LookAndFeel.UseDefaultLookAndFeel = false;
            this.cCompetF.Name = "cCompetF";
            this.cCompetF.PermiteNulo = false;
            this.cCompetF.ReadOnly = false;
            this.cCompetF.Size = new System.Drawing.Size(117, 24);
            this.cCompetF.somenteleitura = false;
            this.cCompetF.TabIndex = 18;
            this.cCompetF.Titulo = null;
            // 
            // cCompetI
            // 
            this.cCompetI.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.cCompetI.Appearance.Options.UseBackColor = true;
            this.cCompetI.CaixaAltaGeral = true;
            this.cCompetI.Doc = System.Windows.Forms.DockStyle.None;
            this.cCompetI.Estado = CompontesBasicos.EstadosDosComponentes.Registrado;
            this.cCompetI.IsNull = false;
            this.cCompetI.Location = new System.Drawing.Point(529, 14);
            this.cCompetI.LookAndFeel.Style = DevExpress.LookAndFeel.LookAndFeelStyle.Office2003;
            this.cCompetI.LookAndFeel.UseDefaultLookAndFeel = false;
            this.cCompetI.Name = "cCompetI";
            this.cCompetI.PermiteNulo = false;
            this.cCompetI.ReadOnly = false;
            this.cCompetI.Size = new System.Drawing.Size(117, 24);
            this.cCompetI.somenteleitura = false;
            this.cCompetI.TabIndex = 17;
            this.cCompetI.Titulo = null;
            // 
            // lookupCondominio_F1
            // 
            this.lookupCondominio_F1.Autofill = true;
            this.lookupCondominio_F1.CaixaAltaGeral = true;
            this.lookupCondominio_F1.CON_sel = -1;
            this.lookupCondominio_F1.CON_selrow = null;
            this.lookupCondominio_F1.Cursor = System.Windows.Forms.Cursors.Default;
            this.lookupCondominio_F1.Doc = System.Windows.Forms.DockStyle.None;
            this.lookupCondominio_F1.Estado = CompontesBasicos.EstadosDosComponentes.Registrado;
            this.lookupCondominio_F1.LinhaMae_F = null;
            this.lookupCondominio_F1.Location = new System.Drawing.Point(24, 18);
            this.lookupCondominio_F1.LookAndFeel.UseDefaultLookAndFeel = false;
            this.lookupCondominio_F1.Name = "lookupCondominio_F1";
            this.lookupCondominio_F1.NivelCONOculto = 2;
            this.lookupCondominio_F1.Size = new System.Drawing.Size(308, 20);
            this.lookupCondominio_F1.somenteleitura = false;
            this.lookupCondominio_F1.TabIndex = 16;
            this.lookupCondominio_F1.TableAdapterPrincipal = null;
            this.lookupCondominio_F1.Titulo = null;
            this.lookupCondominio_F1.alterado += new System.EventHandler(this.lookupCondominio_F1_alterado);
            // 
            // openFileDialog1
            // 
            this.openFileDialog1.DefaultExt = "xml";
            this.openFileDialog1.FileName = "openFileDialog1";
            this.openFileDialog1.Filter = "(Modelos)|*.xml";
            this.openFileDialog1.Title = "Orçamentos (modelo)";
            // 
            // cAbrir
            // 
            this.Controls.Add(this.xtraTabControl);
            this.Controls.Add(this.radioGroup1);
            this.LookAndFeel.UseDefaultLookAndFeel = false;
            this.Name = "cAbrir";
            this.Size = new System.Drawing.Size(1019, 649);
            this.OnClose += new System.EventHandler(this.cAbrir_OnClose);
            this.Controls.SetChildIndex(this.radioGroup1, 0);
            this.Controls.SetChildIndex(this.xtraTabControl, 0);
            ((System.ComponentModel.ISupportInitialize)(this.BarManager_F)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.StyleController_F)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radioGroup1.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xtraTabControl)).EndInit();
            this.xtraTabControl.ResumeLayout(false);
            this.xtraTabPageModelo.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gridControl1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dModelosBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).EndInit();
            this.xtraTabPageORC.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.panelControl2)).EndInit();
            this.panelControl2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gridControl2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dOrcamentoBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCompetenciaEdit1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemImageComboBox1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl1)).EndInit();
            this.panelControl1.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private DevExpress.XtraEditors.RadioGroup radioGroup1;
        private DevExpress.XtraTab.XtraTabControl xtraTabControl;
        private DevExpress.XtraTab.XtraTabPage xtraTabPageModelo;
        private DevExpress.XtraTab.XtraTabPage xtraTabPageORC;
        private System.Windows.Forms.OpenFileDialog openFileDialog1;
        private DevExpress.XtraGrid.GridControl gridControl1;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView1;
        private System.Windows.Forms.BindingSource dModelosBindingSource;
        private DevExpress.XtraGrid.Columns.GridColumn colMBA;
        private DevExpress.XtraGrid.Columns.GridColumn colMBANome;
        private DevExpress.XtraGrid.Columns.GridColumn colMBAOBS;
        private Framework.Lookup.LookupCondominio_F lookupCondominio_F1;
        private DevExpress.XtraEditors.PanelControl panelControl2;
        private DevExpress.XtraGrid.GridControl gridControl2;
        private System.Windows.Forms.BindingSource dOrcamentoBindingSource;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView2;
        private DevExpress.XtraEditors.PanelControl panelControl1;
        private DevExpress.XtraEditors.SimpleButton simpleButton1;
        private Framework.objetosNeon.cCompet cCompetF;
        private Framework.objetosNeon.cCompet cCompetI;
        private DevExpress.XtraGrid.Columns.GridColumn colORC;
        private DevExpress.XtraGrid.Columns.GridColumn colORC_CON;
        private DevExpress.XtraGrid.Columns.GridColumn colORCCompI;
        private CompontesBasicos.RepositoryItemCompetenciaEdit repositoryItemCompetenciaEdit1;
        private DevExpress.XtraGrid.Columns.GridColumn colORCCompF;
        private DevExpress.XtraGrid.Columns.GridColumn colORCStatus;
        private DevExpress.XtraEditors.Repository.RepositoryItemImageComboBox repositoryItemImageComboBox1;
    }
}
