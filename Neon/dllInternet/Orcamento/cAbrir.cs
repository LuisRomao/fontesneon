﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace Orcamento
{
    public partial class cAbrir : CompontesBasicos.ComponenteBaseDialog
    {
        public enum tipoAbre { arquivo,modelo,orc,novo}

        public OrcamentoProc.dModelos dModelos1;

        public int CON;

        public int ORC;

        public Framework.objetosNeon.Competencia CompI;

        public Framework.objetosNeon.Competencia CompF;

        public cAbrir()
        {
            InitializeComponent();
            xtraTabControl.ShowTabHeader = DevExpress.Utils.DefaultBoolean.False;
            Framework.Enumeracoes.VirEnumORCStatus.CarregaEditorDaGrid(colORCStatus);
        }

        public tipoAbre Retorno;

        public OrcamentoProc.dModelos.ModeloBAlanceteRow rowMBA;

        public string NomeArquivo;

        private void radioGroup1_EditValueChanged(object sender, EventArgs e)
        {
            switch ((int)radioGroup1.EditValue)
            {
                case 0:
                    if (openFileDialog1.ShowDialog() == System.Windows.Forms.DialogResult.OK)
                    {
                        NomeArquivo = openFileDialog1.FileName;
                        Retorno = tipoAbre.arquivo;
                        FechaTela(DialogResult.OK);
                    }
                    break;
                case 1:
                    xtraTabControl.SelectedTabPage = xtraTabPageModelo;
                    xtraTabPageModelo.PageVisible = true;
                    dModelos1 = new OrcamentoProc.dModelos();
                    dModelos1.ModeloBAlanceteTableAdapter.Fill(dModelos1.ModeloBAlancete);
                    dModelosBindingSource.DataSource = dModelos1;
                    Retorno = tipoAbre.modelo;
                    break;
                case 2:
                    xtraTabControl.SelectedTabPage = xtraTabPageORC;
                    xtraTabPageORC.PageVisible = true;
                    Retorno = tipoAbre.orc;
                    break;
            }
        }

        private void cAbrir_OnClose(object sender, EventArgs e)
        {
            
        }

        private void gridView1_FocusedRowChanged(object sender, DevExpress.XtraGrid.Views.Base.FocusedRowChangedEventArgs e)
        {
            rowMBA = (OrcamentoProc.dModelos.ModeloBAlanceteRow)gridView1.GetFocusedDataRow();
        }

        private void simpleButton1_Click(object sender, EventArgs e)
        {
            Retorno = tipoAbre.novo;
            FechaTela(DialogResult.OK);
            CON = lookupCondominio_F1.CON_sel;
            CompI = cCompetI.Comp;
            CompF = cCompetF.Comp;
        }

        private OrcamentoProc.dOrcamento DOrcamento;

        private void lookupCondominio_F1_alterado(object sender, EventArgs e)
        {
            if (DOrcamento == null)
            {
                DOrcamento = new OrcamentoProc.dOrcamento();
                dOrcamentoBindingSource.DataSource = DOrcamento;
                //DOrcamento.ORCamentoTableAdapter.Fill(DOrcamento.ORCamento);
                //DOrcamento.ORCamentoTableAdapter.FillByCON(DOrcamento.ORCamento, lookupCondominio_F1.CON_sel);
            }
            DOrcamento.ORCamentoTableAdapter.FillByCON(DOrcamento.ORCamento, lookupCondominio_F1.CON_sel);
            //dOrcamentoBindingSource.Filter = string.Format("ORC_CON = {0}", lookupCondominio_F1.CON_sel);            
        }

        private void gridView2_FocusedRowChanged(object sender, DevExpress.XtraGrid.Views.Base.FocusedRowChangedEventArgs e)
        {
            OrcamentoProc.dOrcamento.ORCamentoRow row = (OrcamentoProc.dOrcamento.ORCamentoRow)gridView2.GetFocusedDataRow();
            if (row != null)
                ORC = row.ORC;
        }

        private void gridView2_RowCellStyle(object sender, DevExpress.XtraGrid.Views.Grid.RowCellStyleEventArgs e)
        {
            if (e.Column == colORCStatus)
            {
                OrcamentoProc.dOrcamento.ORCamentoRow row = (OrcamentoProc.dOrcamento.ORCamentoRow)gridView2.GetDataRow(e.RowHandle);
                Framework.Enumeracoes.VirEnumORCStatus.CellStyle(row.ORCStatus, e);
            }
        }
    }
}
