﻿using System;

namespace Orcamento
{
    public partial class cEditaMerge : CompontesBasicos.ComponenteBaseDialog
    {
        public string strTitulo { get => textEdit1.Text; }
        
        public decimal Valor
        {
            get
            {
                return (bool)radioGroup1.EditValue ? calcEdit1.Value : 0;
            }
        }
        

        public cEditaMerge(string _Titulo, decimal? Valor)
        {
            InitializeComponent();
            textEdit1.Text = _Titulo;
            if (Valor.HasValue)
            {
                radioGroup1.EditValue = true;
                calcEdit1.Visible = true;
                calcEdit1.Value = Valor.Value;
            }
            else
                radioGroup1.EditValue = false;
        }

        private void radioGroup1_SelectedIndexChanged(object sender, EventArgs e)
        {
            calcEdit1.Visible = (bool)radioGroup1.EditValue;                 
        }

        
    }
}
