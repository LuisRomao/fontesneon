﻿namespace Orcamento
{
    partial class cOrcamento
    {
        /// <summary> 
        /// Variável de designer necessária.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Limpar os recursos que estão sendo usados.
        /// </summary>
        /// <param name="disposing">true se for necessário descartar os recursos gerenciados; caso contrário, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Código gerado pelo Designer de Componentes

        /// <summary> 
        /// Método necessário para suporte ao Designer - não modifique 
        /// o conteúdo deste método com o editor de código.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(cOrcamento));
            DevExpress.XtraEditors.Controls.EditorButtonImageOptions editorButtonImageOptions1 = new DevExpress.XtraEditors.Controls.EditorButtonImageOptions();
            DevExpress.XtraEditors.Controls.EditorButtonImageOptions editorButtonImageOptions2 = new DevExpress.XtraEditors.Controls.EditorButtonImageOptions();
            DevExpress.XtraEditors.Controls.EditorButtonImageOptions editorButtonImageOptions3 = new DevExpress.XtraEditors.Controls.EditorButtonImageOptions();
            this.panelControl1 = new DevExpress.XtraEditors.PanelControl();
            this.radioGroupNivel = new DevExpress.XtraEditors.RadioGroup();
            this.groupControl2 = new DevExpress.XtraEditors.GroupControl();
            this.labelControl4 = new DevExpress.XtraEditors.LabelControl();
            this.cCompetF = new Framework.objetosNeon.cCompet();
            this.labelControl3 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl2 = new DevExpress.XtraEditors.LabelControl();
            this.calcEditTotalOrc = new DevExpress.XtraEditors.CalcEdit();
            this.cCompetI = new Framework.objetosNeon.cCompet();
            this.BotSalvaXML = new DevExpress.XtraEditors.SimpleButton();
            this.BotCarregaXML = new DevExpress.XtraEditors.SimpleButton();
            this.groupControl1 = new DevExpress.XtraEditors.GroupControl();
            this.labelDefinido = new DevExpress.XtraEditors.LabelControl();
            this.labelCalculado = new DevExpress.XtraEditors.LabelControl();
            this.labelErro = new DevExpress.XtraEditors.LabelControl();
            this.simpleButtonD = new DevExpress.XtraEditors.SimpleButton();
            this.simpleButtonSG = new DevExpress.XtraEditors.SimpleButton();
            this.simpleButtonG = new DevExpress.XtraEditors.SimpleButton();
            this.gradeOrcamentoBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.repositoryItemCalcEdit1 = new DevExpress.XtraEditors.Repository.RepositoryItemCalcEdit();
            this.bandedGridView1 = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridView();
            this.gridBand7 = new DevExpress.XtraGrid.Views.BandedGrid.GridBand();
            this.colBaseG = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.colBaseSG = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.colBaseD = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.gridBand1 = new DevExpress.XtraGrid.Views.BandedGrid.GridBand();
            this.colTituloG = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.repositoryText50 = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.colValorG = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.colEdValorG = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.colMoveG = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.colAgrGrupo = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.gridBand4 = new DevExpress.XtraGrid.Views.BandedGrid.GridBand();
            this.colOrdem = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.colID = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.colORG1 = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.colORG2 = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.colORD = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.colStatusValorD = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.colStatusValorG = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.colStatusValorSG = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.gridBand2 = new DevExpress.XtraGrid.Views.BandedGrid.GridBand();
            this.colTituloSG = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.colValorSG = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.colEdValorGS = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.colMoveSG = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.colAgrSGrupo = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.gridBand5 = new DevExpress.XtraGrid.Views.BandedGrid.GridBand();
            this.colPLA = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.colTituloD = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.colValorD = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.colMoveD = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.colAgrDetalhe = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.colTituloDetalhe = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.gridBand3 = new DevExpress.XtraGrid.Views.BandedGrid.GridBand();
            this.colPLADescricao = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.gridBand6 = new DevExpress.XtraGrid.Views.BandedGrid.GridBand();
            this.colDel = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.repositoryItemButtonDelete = new DevExpress.XtraEditors.Repository.RepositoryItemButtonEdit();
            this.gridControl1 = new DevExpress.XtraGrid.GridControl();
            this.repositoryEd = new DevExpress.XtraEditors.Repository.RepositoryItemButtonEdit();
            this.repMover = new DevExpress.XtraEditors.Repository.RepositoryItemButtonEdit();
            this.repAbrir = new DevExpress.XtraEditors.Repository.RepositoryItemButtonEdit();
            this.repoFechar = new DevExpress.XtraEditors.Repository.RepositoryItemButtonEdit();
            this.saveFileDialog1 = new System.Windows.Forms.SaveFileDialog();
            ((System.ComponentModel.ISupportInitialize)(this.StyleController_F)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl1)).BeginInit();
            this.panelControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.radioGroupNivel.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl2)).BeginInit();
            this.groupControl2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.calcEditTotalOrc.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl1)).BeginInit();
            this.groupControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gradeOrcamentoBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCalcEdit1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.bandedGridView1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryText50)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemButtonDelete)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryEd)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repMover)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repAbrir)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repoFechar)).BeginInit();
            this.SuspendLayout();
            // 
            // ToolTipController_F
            // 
            this.ToolTipController_F.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.ToolTipController_F.Appearance.ForeColor = System.Drawing.Color.Black;
            this.ToolTipController_F.Appearance.Options.UseBackColor = true;
            this.ToolTipController_F.Appearance.Options.UseFont = true;
            this.ToolTipController_F.Appearance.Options.UseForeColor = true;
            this.ToolTipController_F.Appearance.Options.UseTextOptions = true;
            this.ToolTipController_F.Appearance.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            // 
            // StyleController_F
            // 
            this.StyleController_F.AppearanceDisabled.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this.StyleController_F.AppearanceDisabled.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this.StyleController_F.AppearanceDisabled.Options.UseBackColor = true;
            this.StyleController_F.AppearanceDisabled.Options.UseForeColor = true;
            this.StyleController_F.AppearanceFocused.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.StyleController_F.AppearanceFocused.Options.UseBackColor = true;
            // 
            // panelControl1
            // 
            this.panelControl1.Controls.Add(this.radioGroupNivel);
            this.panelControl1.Controls.Add(this.groupControl2);
            this.panelControl1.Controls.Add(this.BotSalvaXML);
            this.panelControl1.Controls.Add(this.BotCarregaXML);
            this.panelControl1.Controls.Add(this.groupControl1);
            this.panelControl1.Controls.Add(this.simpleButtonD);
            this.panelControl1.Controls.Add(this.simpleButtonSG);
            this.panelControl1.Controls.Add(this.simpleButtonG);
            this.panelControl1.Dock = System.Windows.Forms.DockStyle.Top;
            this.panelControl1.Location = new System.Drawing.Point(0, 0);
            this.panelControl1.Name = "panelControl1";
            this.panelControl1.Size = new System.Drawing.Size(979, 175);
            this.panelControl1.TabIndex = 0;
            // 
            // radioGroupNivel
            // 
            this.radioGroupNivel.EditValue = -1;
            this.radioGroupNivel.Location = new System.Drawing.Point(815, 3);
            this.radioGroupNivel.Name = "radioGroupNivel";
            this.radioGroupNivel.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.radioGroupNivel.Properties.Appearance.Options.UseFont = true;
            this.radioGroupNivel.Properties.Items.AddRange(new DevExpress.XtraEditors.Controls.RadioGroupItem[] {
            new DevExpress.XtraEditors.Controls.RadioGroupItem(-1, "Aberto"),
            new DevExpress.XtraEditors.Controls.RadioGroupItem(0, "Balancete"),
            new DevExpress.XtraEditors.Controls.RadioGroupItem(1, "Boleto"),
            new DevExpress.XtraEditors.Controls.RadioGroupItem(2, "Anual")});
            this.radioGroupNivel.Size = new System.Drawing.Size(138, 129);
            this.radioGroupNivel.TabIndex = 67;
            this.radioGroupNivel.EditValueChanged += new System.EventHandler(this.radioGroupNivel_EditValueChanged);
            // 
            // groupControl2
            // 
            this.groupControl2.Controls.Add(this.labelControl4);
            this.groupControl2.Controls.Add(this.cCompetF);
            this.groupControl2.Controls.Add(this.labelControl3);
            this.groupControl2.Controls.Add(this.labelControl2);
            this.groupControl2.Controls.Add(this.calcEditTotalOrc);
            this.groupControl2.Controls.Add(this.cCompetI);
            this.groupControl2.Location = new System.Drawing.Point(178, 3);
            this.groupControl2.Name = "groupControl2";
            this.groupControl2.Size = new System.Drawing.Size(309, 129);
            this.groupControl2.TabIndex = 60;
            this.groupControl2.Text = "Dados Orçamento";
            // 
            // labelControl4
            // 
            this.labelControl4.Appearance.Font = new System.Drawing.Font("Tahoma", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl4.Appearance.Options.UseFont = true;
            this.labelControl4.Location = new System.Drawing.Point(5, 94);
            this.labelControl4.Name = "labelControl4";
            this.labelControl4.Size = new System.Drawing.Size(87, 18);
            this.labelControl4.TabIndex = 63;
            this.labelControl4.Text = "Valor Total:";
            // 
            // cCompetF
            // 
            this.cCompetF.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.cCompetF.Appearance.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cCompetF.Appearance.Options.UseBackColor = true;
            this.cCompetF.Appearance.Options.UseFont = true;
            this.cCompetF.CaixaAltaGeral = true;
            this.cCompetF.Doc = System.Windows.Forms.DockStyle.None;
            this.cCompetF.Estado = CompontesBasicos.EstadosDosComponentes.Registrado;
            this.cCompetF.IsNull = false;
            this.cCompetF.Location = new System.Drawing.Point(162, 58);
            this.cCompetF.LookAndFeel.Style = DevExpress.LookAndFeel.LookAndFeelStyle.Office2003;
            this.cCompetF.LookAndFeel.UseDefaultLookAndFeel = false;
            this.cCompetF.Name = "cCompetF";
            this.cCompetF.PermiteNulo = false;
            this.cCompetF.ReadOnly = false;
            this.cCompetF.Size = new System.Drawing.Size(142, 27);
            this.cCompetF.somenteleitura = true;
            this.cCompetF.TabIndex = 62;
            this.cCompetF.Titulo = resources.GetString("cCompetF.Titulo");
            this.cCompetF.OnChange += new System.EventHandler(this.cCompetF_OnChange);
            // 
            // labelControl3
            // 
            this.labelControl3.Appearance.Font = new System.Drawing.Font("Tahoma", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl3.Appearance.Options.UseFont = true;
            this.labelControl3.Location = new System.Drawing.Point(5, 61);
            this.labelControl3.Name = "labelControl3";
            this.labelControl3.Size = new System.Drawing.Size(142, 18);
            this.labelControl3.TabIndex = 61;
            this.labelControl3.Text = "Competência Final:";
            // 
            // labelControl2
            // 
            this.labelControl2.Appearance.Font = new System.Drawing.Font("Tahoma", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl2.Appearance.Options.UseFont = true;
            this.labelControl2.Location = new System.Drawing.Point(5, 32);
            this.labelControl2.Name = "labelControl2";
            this.labelControl2.Size = new System.Drawing.Size(151, 18);
            this.labelControl2.TabIndex = 60;
            this.labelControl2.Text = "Competência inicial:";
            // 
            // calcEditTotalOrc
            // 
            this.calcEditTotalOrc.Location = new System.Drawing.Point(162, 92);
            this.calcEditTotalOrc.Name = "calcEditTotalOrc";
            this.calcEditTotalOrc.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.calcEditTotalOrc.Properties.Appearance.Options.UseFont = true;
            this.calcEditTotalOrc.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.calcEditTotalOrc.Properties.Mask.EditMask = "n2";
            this.calcEditTotalOrc.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.calcEditTotalOrc.Size = new System.Drawing.Size(142, 26);
            this.calcEditTotalOrc.TabIndex = 4;
            // 
            // cCompetI
            // 
            this.cCompetI.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.cCompetI.Appearance.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cCompetI.Appearance.Options.UseBackColor = true;
            this.cCompetI.Appearance.Options.UseFont = true;
            this.cCompetI.CaixaAltaGeral = true;
            this.cCompetI.Doc = System.Windows.Forms.DockStyle.None;
            this.cCompetI.Estado = CompontesBasicos.EstadosDosComponentes.Registrado;
            this.cCompetI.IsNull = false;
            this.cCompetI.Location = new System.Drawing.Point(162, 25);
            this.cCompetI.LookAndFeel.Style = DevExpress.LookAndFeel.LookAndFeelStyle.Office2003;
            this.cCompetI.LookAndFeel.UseDefaultLookAndFeel = false;
            this.cCompetI.Name = "cCompetI";
            this.cCompetI.PermiteNulo = false;
            this.cCompetI.ReadOnly = false;
            this.cCompetI.Size = new System.Drawing.Size(147, 27);
            this.cCompetI.somenteleitura = true;
            this.cCompetI.TabIndex = 59;
            this.cCompetI.Titulo = resources.GetString("cCompetI.Titulo");
            this.cCompetI.OnChange += new System.EventHandler(this.cCompetI_OnChange);
            // 
            // BotSalvaXML
            // 
            this.BotSalvaXML.Enabled = false;
            this.BotSalvaXML.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("BotSalvaXML.ImageOptions.Image")));
            this.BotSalvaXML.Location = new System.Drawing.Point(5, 61);
            this.BotSalvaXML.Name = "BotSalvaXML";
            this.BotSalvaXML.Size = new System.Drawing.Size(167, 52);
            this.BotSalvaXML.TabIndex = 14;
            this.BotSalvaXML.Text = "Salvar em Arquivo";
            this.BotSalvaXML.Click += new System.EventHandler(this.BotSalvaXML_Click);
            // 
            // BotCarregaXML
            // 
            this.BotCarregaXML.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("BotCarregaXML.ImageOptions.Image")));
            this.BotCarregaXML.Location = new System.Drawing.Point(5, 3);
            this.BotCarregaXML.Name = "BotCarregaXML";
            this.BotCarregaXML.Size = new System.Drawing.Size(167, 52);
            this.BotCarregaXML.TabIndex = 13;
            this.BotCarregaXML.Text = "Carregar";
            this.BotCarregaXML.Click += new System.EventHandler(this.BotCarregaXML_Click);
            // 
            // groupControl1
            // 
            this.groupControl1.Controls.Add(this.labelDefinido);
            this.groupControl1.Controls.Add(this.labelCalculado);
            this.groupControl1.Controls.Add(this.labelErro);
            this.groupControl1.Location = new System.Drawing.Point(493, 5);
            this.groupControl1.Name = "groupControl1";
            this.groupControl1.Size = new System.Drawing.Size(316, 127);
            this.groupControl1.TabIndex = 12;
            this.groupControl1.Text = "Legenda (Valores)";
            // 
            // labelDefinido
            // 
            this.labelDefinido.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(128)))));
            this.labelDefinido.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelDefinido.Appearance.Options.UseBackColor = true;
            this.labelDefinido.Appearance.Options.UseFont = true;
            this.labelDefinido.Appearance.Options.UseTextOptions = true;
            this.labelDefinido.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.labelDefinido.Appearance.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.labelDefinido.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.labelDefinido.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.Simple;
            this.labelDefinido.Location = new System.Drawing.Point(5, 23);
            this.labelDefinido.Name = "labelDefinido";
            this.labelDefinido.Size = new System.Drawing.Size(304, 27);
            this.labelDefinido.TabIndex = 8;
            this.labelDefinido.Text = "labelDefinido";
            // 
            // labelCalculado
            // 
            this.labelCalculado.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(128)))));
            this.labelCalculado.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelCalculado.Appearance.Options.UseBackColor = true;
            this.labelCalculado.Appearance.Options.UseFont = true;
            this.labelCalculado.Appearance.Options.UseTextOptions = true;
            this.labelCalculado.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.labelCalculado.Appearance.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.labelCalculado.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.labelCalculado.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.Simple;
            this.labelCalculado.Location = new System.Drawing.Point(5, 56);
            this.labelCalculado.Name = "labelCalculado";
            this.labelCalculado.Size = new System.Drawing.Size(304, 27);
            this.labelCalculado.TabIndex = 9;
            this.labelCalculado.Text = "labelCalculado";
            // 
            // labelErro
            // 
            this.labelErro.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(128)))));
            this.labelErro.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelErro.Appearance.Options.UseBackColor = true;
            this.labelErro.Appearance.Options.UseFont = true;
            this.labelErro.Appearance.Options.UseTextOptions = true;
            this.labelErro.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.labelErro.Appearance.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.labelErro.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.labelErro.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.Simple;
            this.labelErro.Location = new System.Drawing.Point(5, 89);
            this.labelErro.Name = "labelErro";
            this.labelErro.Size = new System.Drawing.Size(304, 27);
            this.labelErro.TabIndex = 10;
            this.labelErro.Text = "labelErro";
            // 
            // simpleButtonD
            // 
            this.simpleButtonD.Appearance.BackColor = System.Drawing.Color.Lavender;
            this.simpleButtonD.Appearance.Options.UseBackColor = true;
            this.simpleButtonD.Location = new System.Drawing.Point(1066, 147);
            this.simpleButtonD.LookAndFeel.Style = DevExpress.LookAndFeel.LookAndFeelStyle.Flat;
            this.simpleButtonD.LookAndFeel.UseDefaultLookAndFeel = false;
            this.simpleButtonD.Name = "simpleButtonD";
            this.simpleButtonD.Size = new System.Drawing.Size(96, 23);
            this.simpleButtonD.TabIndex = 2;
            this.simpleButtonD.Text = "Novo Plano";
            this.simpleButtonD.Click += new System.EventHandler(this.simpleButton3_Click);
            // 
            // simpleButtonSG
            // 
            this.simpleButtonSG.Appearance.BackColor = System.Drawing.Color.Cornsilk;
            this.simpleButtonSG.Appearance.Options.UseBackColor = true;
            this.simpleButtonSG.Location = new System.Drawing.Point(611, 147);
            this.simpleButtonSG.LookAndFeel.Style = DevExpress.LookAndFeel.LookAndFeelStyle.Flat;
            this.simpleButtonSG.LookAndFeel.UseDefaultLookAndFeel = false;
            this.simpleButtonSG.Name = "simpleButtonSG";
            this.simpleButtonSG.Size = new System.Drawing.Size(96, 23);
            this.simpleButtonSG.TabIndex = 1;
            this.simpleButtonSG.Text = "Novo Sub Grupo";
            this.simpleButtonSG.Click += new System.EventHandler(this.simpleButton2_Click);
            // 
            // simpleButtonG
            // 
            this.simpleButtonG.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.simpleButtonG.Appearance.Options.UseBackColor = true;
            this.simpleButtonG.Location = new System.Drawing.Point(178, 147);
            this.simpleButtonG.LookAndFeel.Style = DevExpress.LookAndFeel.LookAndFeelStyle.Flat;
            this.simpleButtonG.LookAndFeel.UseDefaultLookAndFeel = false;
            this.simpleButtonG.Name = "simpleButtonG";
            this.simpleButtonG.Size = new System.Drawing.Size(99, 23);
            this.simpleButtonG.TabIndex = 0;
            this.simpleButtonG.Text = "Novo Grupo";
            this.simpleButtonG.Click += new System.EventHandler(this.simpleButton1_Click);
            // 
            // gradeOrcamentoBindingSource
            // 
            this.gradeOrcamentoBindingSource.DataMember = "GradeOrcamento";
            this.gradeOrcamentoBindingSource.DataSource = typeof(OrcamentoProc.dOrcGrade);
            // 
            // repositoryItemCalcEdit1
            // 
            this.repositoryItemCalcEdit1.AutoHeight = false;
            this.repositoryItemCalcEdit1.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemCalcEdit1.Name = "repositoryItemCalcEdit1";
            // 
            // bandedGridView1
            // 
            this.bandedGridView1.Appearance.BandPanel.BackColor = System.Drawing.Color.DarkOrange;
            this.bandedGridView1.Appearance.BandPanel.BorderColor = System.Drawing.Color.DarkOrange;
            this.bandedGridView1.Appearance.BandPanel.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Bold);
            this.bandedGridView1.Appearance.BandPanel.Options.UseBackColor = true;
            this.bandedGridView1.Appearance.BandPanel.Options.UseBorderColor = true;
            this.bandedGridView1.Appearance.BandPanel.Options.UseFont = true;
            this.bandedGridView1.Appearance.BandPanelBackground.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(64)))), ((int)(((byte)(0)))));
            this.bandedGridView1.Appearance.BandPanelBackground.Options.UseBackColor = true;
            this.bandedGridView1.Appearance.ColumnFilterButton.BackColor = System.Drawing.Color.Orange;
            this.bandedGridView1.Appearance.ColumnFilterButton.BorderColor = System.Drawing.Color.Orange;
            this.bandedGridView1.Appearance.ColumnFilterButton.ForeColor = System.Drawing.Color.DarkOrange;
            this.bandedGridView1.Appearance.ColumnFilterButton.Options.UseBackColor = true;
            this.bandedGridView1.Appearance.ColumnFilterButton.Options.UseBorderColor = true;
            this.bandedGridView1.Appearance.ColumnFilterButton.Options.UseForeColor = true;
            this.bandedGridView1.Appearance.ColumnFilterButtonActive.BackColor = System.Drawing.Color.DarkOrange;
            this.bandedGridView1.Appearance.ColumnFilterButtonActive.BorderColor = System.Drawing.Color.DarkOrange;
            this.bandedGridView1.Appearance.ColumnFilterButtonActive.ForeColor = System.Drawing.Color.White;
            this.bandedGridView1.Appearance.ColumnFilterButtonActive.Options.UseBackColor = true;
            this.bandedGridView1.Appearance.ColumnFilterButtonActive.Options.UseBorderColor = true;
            this.bandedGridView1.Appearance.ColumnFilterButtonActive.Options.UseForeColor = true;
            this.bandedGridView1.Appearance.Empty.BackColor = System.Drawing.Color.LightSkyBlue;
            this.bandedGridView1.Appearance.Empty.BackColor2 = System.Drawing.Color.SkyBlue;
            this.bandedGridView1.Appearance.Empty.GradientMode = System.Drawing.Drawing2D.LinearGradientMode.BackwardDiagonal;
            this.bandedGridView1.Appearance.Empty.Options.UseBackColor = true;
            this.bandedGridView1.Appearance.EvenRow.BackColor = System.Drawing.Color.Linen;
            this.bandedGridView1.Appearance.EvenRow.BackColor2 = System.Drawing.Color.AntiqueWhite;
            this.bandedGridView1.Appearance.EvenRow.GradientMode = System.Drawing.Drawing2D.LinearGradientMode.ForwardDiagonal;
            this.bandedGridView1.Appearance.EvenRow.Options.UseBackColor = true;
            this.bandedGridView1.Appearance.FilterCloseButton.BackColor = System.Drawing.Color.Orange;
            this.bandedGridView1.Appearance.FilterCloseButton.BorderColor = System.Drawing.Color.Orange;
            this.bandedGridView1.Appearance.FilterCloseButton.Options.UseBackColor = true;
            this.bandedGridView1.Appearance.FilterCloseButton.Options.UseBorderColor = true;
            this.bandedGridView1.Appearance.FilterPanel.BackColor = System.Drawing.Color.DarkOrange;
            this.bandedGridView1.Appearance.FilterPanel.BackColor2 = System.Drawing.Color.Orange;
            this.bandedGridView1.Appearance.FilterPanel.ForeColor = System.Drawing.Color.Black;
            this.bandedGridView1.Appearance.FilterPanel.GradientMode = System.Drawing.Drawing2D.LinearGradientMode.BackwardDiagonal;
            this.bandedGridView1.Appearance.FilterPanel.Options.UseBackColor = true;
            this.bandedGridView1.Appearance.FilterPanel.Options.UseForeColor = true;
            this.bandedGridView1.Appearance.FocusedRow.BackColor = System.Drawing.Color.RoyalBlue;
            this.bandedGridView1.Appearance.FocusedRow.BackColor2 = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this.bandedGridView1.Appearance.FocusedRow.ForeColor = System.Drawing.Color.White;
            this.bandedGridView1.Appearance.FocusedRow.GradientMode = System.Drawing.Drawing2D.LinearGradientMode.Vertical;
            this.bandedGridView1.Appearance.FocusedRow.Options.UseBackColor = true;
            this.bandedGridView1.Appearance.FocusedRow.Options.UseForeColor = true;
            this.bandedGridView1.Appearance.FooterPanel.BackColor = System.Drawing.Color.Orange;
            this.bandedGridView1.Appearance.FooterPanel.BorderColor = System.Drawing.Color.Orange;
            this.bandedGridView1.Appearance.FooterPanel.Options.UseBackColor = true;
            this.bandedGridView1.Appearance.FooterPanel.Options.UseBorderColor = true;
            this.bandedGridView1.Appearance.GroupButton.BackColor = System.Drawing.Color.Wheat;
            this.bandedGridView1.Appearance.GroupButton.BorderColor = System.Drawing.Color.Wheat;
            this.bandedGridView1.Appearance.GroupButton.Options.UseBackColor = true;
            this.bandedGridView1.Appearance.GroupButton.Options.UseBorderColor = true;
            this.bandedGridView1.Appearance.GroupFooter.BackColor = System.Drawing.Color.Wheat;
            this.bandedGridView1.Appearance.GroupFooter.BorderColor = System.Drawing.Color.Wheat;
            this.bandedGridView1.Appearance.GroupFooter.Options.UseBackColor = true;
            this.bandedGridView1.Appearance.GroupFooter.Options.UseBorderColor = true;
            this.bandedGridView1.Appearance.GroupPanel.BackColor = System.Drawing.Color.RoyalBlue;
            this.bandedGridView1.Appearance.GroupPanel.ForeColor = System.Drawing.Color.White;
            this.bandedGridView1.Appearance.GroupPanel.Options.UseBackColor = true;
            this.bandedGridView1.Appearance.GroupPanel.Options.UseForeColor = true;
            this.bandedGridView1.Appearance.GroupRow.BackColor = System.Drawing.Color.Wheat;
            this.bandedGridView1.Appearance.GroupRow.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Bold);
            this.bandedGridView1.Appearance.GroupRow.Options.UseBackColor = true;
            this.bandedGridView1.Appearance.GroupRow.Options.UseFont = true;
            this.bandedGridView1.Appearance.HeaderPanel.BackColor = System.Drawing.Color.Orange;
            this.bandedGridView1.Appearance.HeaderPanel.BorderColor = System.Drawing.Color.Orange;
            this.bandedGridView1.Appearance.HeaderPanel.Options.UseBackColor = true;
            this.bandedGridView1.Appearance.HeaderPanel.Options.UseBorderColor = true;
            this.bandedGridView1.Appearance.HeaderPanelBackground.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(212)))), ((int)(((byte)(84)))), ((int)(((byte)(20)))));
            this.bandedGridView1.Appearance.HeaderPanelBackground.Options.UseBackColor = true;
            this.bandedGridView1.Appearance.HideSelectionRow.BackColor = System.Drawing.Color.LightSlateGray;
            this.bandedGridView1.Appearance.HideSelectionRow.Options.UseBackColor = true;
            this.bandedGridView1.Appearance.HorzLine.BackColor = System.Drawing.Color.Tan;
            this.bandedGridView1.Appearance.HorzLine.Options.UseBackColor = true;
            this.bandedGridView1.Appearance.OddRow.BackColor = System.Drawing.Color.White;
            this.bandedGridView1.Appearance.OddRow.Options.UseBackColor = true;
            this.bandedGridView1.Appearance.Preview.BackColor = System.Drawing.Color.Khaki;
            this.bandedGridView1.Appearance.Preview.BackColor2 = System.Drawing.Color.Cornsilk;
            this.bandedGridView1.Appearance.Preview.Font = new System.Drawing.Font("Tahoma", 7.5F);
            this.bandedGridView1.Appearance.Preview.GradientMode = System.Drawing.Drawing2D.LinearGradientMode.BackwardDiagonal;
            this.bandedGridView1.Appearance.Preview.Options.UseBackColor = true;
            this.bandedGridView1.Appearance.Preview.Options.UseFont = true;
            this.bandedGridView1.Appearance.Row.BackColor = System.Drawing.Color.White;
            this.bandedGridView1.Appearance.Row.Options.UseBackColor = true;
            this.bandedGridView1.Appearance.RowSeparator.BackColor = System.Drawing.Color.LightSkyBlue;
            this.bandedGridView1.Appearance.RowSeparator.Options.UseBackColor = true;
            this.bandedGridView1.Appearance.VertLine.BackColor = System.Drawing.Color.Tan;
            this.bandedGridView1.Appearance.VertLine.Options.UseBackColor = true;
            this.bandedGridView1.Bands.AddRange(new DevExpress.XtraGrid.Views.BandedGrid.GridBand[] {
            this.gridBand7,
            this.gridBand1,
            this.gridBand4,
            this.gridBand2,
            this.gridBand5,
            this.gridBand3,
            this.gridBand6});
            this.bandedGridView1.Columns.AddRange(new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn[] {
            this.colOrdem,
            this.colTituloG,
            this.colTituloSG,
            this.colValorG,
            this.colValorSG,
            this.colTituloD,
            this.colValorD,
            this.colPLA,
            this.colPLADescricao,
            this.colID,
            this.colORD,
            this.colORG1,
            this.colORG2,
            this.colDel,
            this.colEdValorG,
            this.colEdValorGS,
            this.colBaseG,
            this.colBaseSG,
            this.colMoveG,
            this.colMoveSG,
            this.colMoveD,
            this.colStatusValorD,
            this.colStatusValorG,
            this.colStatusValorSG,
            this.colAgrGrupo,
            this.colAgrSGrupo,
            this.colAgrDetalhe,
            this.colBaseD,
            this.colTituloDetalhe});
            this.bandedGridView1.GridControl = this.gridControl1;
            this.bandedGridView1.Name = "bandedGridView1";
            this.bandedGridView1.OptionsCustomization.AllowGroup = false;
            this.bandedGridView1.OptionsCustomization.AllowSort = false;
            this.bandedGridView1.OptionsView.AllowCellMerge = true;
            this.bandedGridView1.OptionsView.ColumnAutoWidth = false;
            this.bandedGridView1.OptionsView.EnableAppearanceEvenRow = true;
            this.bandedGridView1.OptionsView.EnableAppearanceOddRow = true;
            this.bandedGridView1.OptionsView.ShowGroupPanel = false;
            this.bandedGridView1.PaintStyleName = "Flat";
            this.bandedGridView1.SortInfo.AddRange(new DevExpress.XtraGrid.Columns.GridColumnSortInfo[] {
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colOrdem, DevExpress.Data.ColumnSortOrder.Ascending),
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colID, DevExpress.Data.ColumnSortOrder.Ascending)});
            this.bandedGridView1.CellMerge += new DevExpress.XtraGrid.Views.Grid.CellMergeEventHandler(this.bandedGridView1_CellMerge);
            this.bandedGridView1.CustomDrawCell += new DevExpress.XtraGrid.Views.Base.RowCellCustomDrawEventHandler(this.bandedGridView1_CustomDrawCell);
            this.bandedGridView1.RowCellStyle += new DevExpress.XtraGrid.Views.Grid.RowCellStyleEventHandler(this.bandedGridView1_RowCellStyle);
            this.bandedGridView1.CustomRowCellEdit += new DevExpress.XtraGrid.Views.Grid.CustomRowCellEditEventHandler(this.bandedGridView1_CustomRowCellEdit);
            this.bandedGridView1.FocusedRowChanged += new DevExpress.XtraGrid.Views.Base.FocusedRowChangedEventHandler(this.bandedGridView1_FocusedRowChanged);
            this.bandedGridView1.RowUpdated += new DevExpress.XtraGrid.Views.Base.RowObjectEventHandler(this.bandedGridView1_RowUpdated);
            this.bandedGridView1.MouseDown += new System.Windows.Forms.MouseEventHandler(this.bandedGridView1_MouseDown);
            this.bandedGridView1.MouseMove += new System.Windows.Forms.MouseEventHandler(this.bandedGridView1_MouseMove);
            // 
            // gridBand7
            // 
            this.gridBand7.Caption = "Bases";
            this.gridBand7.Columns.Add(this.colBaseG);
            this.gridBand7.Columns.Add(this.colBaseSG);
            this.gridBand7.Columns.Add(this.colBaseD);
            this.gridBand7.Name = "gridBand7";
            this.gridBand7.Visible = false;
            this.gridBand7.VisibleIndex = -1;
            this.gridBand7.Width = 225;
            // 
            // colBaseG
            // 
            this.colBaseG.FieldName = "BaseG";
            this.colBaseG.Name = "colBaseG";
            this.colBaseG.OptionsColumn.AllowMerge = DevExpress.Utils.DefaultBoolean.False;
            this.colBaseG.Visible = true;
            // 
            // colBaseSG
            // 
            this.colBaseSG.FieldName = "BaseSG";
            this.colBaseSG.Name = "colBaseSG";
            this.colBaseSG.OptionsColumn.AllowMerge = DevExpress.Utils.DefaultBoolean.False;
            this.colBaseSG.Visible = true;
            // 
            // colBaseD
            // 
            this.colBaseD.FieldName = "BaseD";
            this.colBaseD.Name = "colBaseD";
            this.colBaseD.OptionsColumn.AllowEdit = false;
            this.colBaseD.Visible = true;
            // 
            // gridBand1
            // 
            this.gridBand1.AppearanceHeader.Options.UseTextOptions = true;
            this.gridBand1.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridBand1.Caption = "NÍVEL 1";
            this.gridBand1.Columns.Add(this.colTituloG);
            this.gridBand1.Columns.Add(this.colValorG);
            this.gridBand1.Columns.Add(this.colEdValorG);
            this.gridBand1.Columns.Add(this.colMoveG);
            this.gridBand1.Columns.Add(this.colAgrGrupo);
            this.gridBand1.Name = "gridBand1";
            this.gridBand1.VisibleIndex = 0;
            this.gridBand1.Width = 400;
            // 
            // colTituloG
            // 
            this.colTituloG.AppearanceCell.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.colTituloG.AppearanceCell.Font = new System.Drawing.Font("Tahoma", 11.25F);
            this.colTituloG.AppearanceCell.Options.UseBackColor = true;
            this.colTituloG.AppearanceCell.Options.UseFont = true;
            this.colTituloG.Caption = "Título";
            this.colTituloG.ColumnEdit = this.repositoryText50;
            this.colTituloG.FieldName = "TituloG";
            this.colTituloG.Name = "colTituloG";
            this.colTituloG.OptionsColumn.AllowMerge = DevExpress.Utils.DefaultBoolean.True;
            this.colTituloG.Visible = true;
            this.colTituloG.Width = 240;
            // 
            // repositoryText50
            // 
            this.repositoryText50.AutoHeight = false;
            this.repositoryText50.MaxLength = 50;
            this.repositoryText50.Name = "repositoryText50";
            // 
            // colValorG
            // 
            this.colValorG.AppearanceCell.Font = new System.Drawing.Font("Tahoma", 9F);
            this.colValorG.AppearanceCell.Options.UseFont = true;
            this.colValorG.Caption = "Valor";
            this.colValorG.ColumnEdit = this.repositoryItemCalcEdit1;
            this.colValorG.FieldName = "ValorG";
            this.colValorG.Name = "colValorG";
            this.colValorG.OptionsColumn.AllowMerge = DevExpress.Utils.DefaultBoolean.True;
            this.colValorG.Visible = true;
            this.colValorG.Width = 70;
            // 
            // colEdValorG
            // 
            this.colEdValorG.Caption = "bandedGridColumn1";
            this.colEdValorG.Name = "colEdValorG";
            this.colEdValorG.OptionsColumn.ShowCaption = false;
            this.colEdValorG.Visible = true;
            this.colEdValorG.Width = 30;
            // 
            // colMoveG
            // 
            this.colMoveG.Caption = "bandedGridColumn1";
            this.colMoveG.Name = "colMoveG";
            this.colMoveG.OptionsColumn.AllowEdit = false;
            this.colMoveG.OptionsColumn.ReadOnly = true;
            this.colMoveG.OptionsColumn.ShowCaption = false;
            this.colMoveG.Visible = true;
            this.colMoveG.Width = 30;
            // 
            // colAgrGrupo
            // 
            this.colAgrGrupo.Caption = "bandedGridColumn1";
            this.colAgrGrupo.Name = "colAgrGrupo";
            this.colAgrGrupo.OptionsColumn.AllowMerge = DevExpress.Utils.DefaultBoolean.True;
            this.colAgrGrupo.OptionsColumn.FixedWidth = true;
            this.colAgrGrupo.OptionsColumn.ShowCaption = false;
            this.colAgrGrupo.Visible = true;
            this.colAgrGrupo.Width = 30;
            // 
            // gridBand4
            // 
            this.gridBand4.AppearanceHeader.Options.UseTextOptions = true;
            this.gridBand4.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridBand4.Caption = "Interno";
            this.gridBand4.Columns.Add(this.colOrdem);
            this.gridBand4.Columns.Add(this.colID);
            this.gridBand4.Columns.Add(this.colORG1);
            this.gridBand4.Columns.Add(this.colORG2);
            this.gridBand4.Columns.Add(this.colORD);
            this.gridBand4.Columns.Add(this.colStatusValorD);
            this.gridBand4.Columns.Add(this.colStatusValorG);
            this.gridBand4.Columns.Add(this.colStatusValorSG);
            this.gridBand4.Name = "gridBand4";
            this.gridBand4.Visible = false;
            this.gridBand4.VisibleIndex = -1;
            this.gridBand4.Width = 600;
            // 
            // colOrdem
            // 
            this.colOrdem.FieldName = "Ordem";
            this.colOrdem.Name = "colOrdem";
            this.colOrdem.OptionsColumn.AllowGroup = DevExpress.Utils.DefaultBoolean.False;
            this.colOrdem.OptionsColumn.ReadOnly = true;
            this.colOrdem.Visible = true;
            // 
            // colID
            // 
            this.colID.FieldName = "ID";
            this.colID.Name = "colID";
            this.colID.OptionsColumn.AllowMerge = DevExpress.Utils.DefaultBoolean.False;
            this.colID.OptionsColumn.ReadOnly = true;
            this.colID.Visible = true;
            // 
            // colORG1
            // 
            this.colORG1.FieldName = "ORG1";
            this.colORG1.Name = "colORG1";
            this.colORG1.OptionsColumn.AllowMerge = DevExpress.Utils.DefaultBoolean.False;
            this.colORG1.OptionsColumn.ReadOnly = true;
            this.colORG1.Visible = true;
            // 
            // colORG2
            // 
            this.colORG2.FieldName = "ORG2";
            this.colORG2.Name = "colORG2";
            this.colORG2.OptionsColumn.AllowMerge = DevExpress.Utils.DefaultBoolean.False;
            this.colORG2.OptionsColumn.ReadOnly = true;
            this.colORG2.Visible = true;
            // 
            // colORD
            // 
            this.colORD.FieldName = "ORD";
            this.colORD.Name = "colORD";
            this.colORD.OptionsColumn.AllowMerge = DevExpress.Utils.DefaultBoolean.False;
            this.colORD.OptionsColumn.ReadOnly = true;
            this.colORD.Visible = true;
            // 
            // colStatusValorD
            // 
            this.colStatusValorD.FieldName = "StatusValorD";
            this.colStatusValorD.Name = "colStatusValorD";
            this.colStatusValorD.OptionsColumn.AllowMerge = DevExpress.Utils.DefaultBoolean.False;
            this.colStatusValorD.Visible = true;
            // 
            // colStatusValorG
            // 
            this.colStatusValorG.FieldName = "StatusValorG";
            this.colStatusValorG.Name = "colStatusValorG";
            this.colStatusValorG.OptionsColumn.AllowMerge = DevExpress.Utils.DefaultBoolean.False;
            this.colStatusValorG.Visible = true;
            // 
            // colStatusValorSG
            // 
            this.colStatusValorSG.FieldName = "StatusValorSG";
            this.colStatusValorSG.Name = "colStatusValorSG";
            this.colStatusValorSG.OptionsColumn.AllowMerge = DevExpress.Utils.DefaultBoolean.False;
            this.colStatusValorSG.Visible = true;
            // 
            // gridBand2
            // 
            this.gridBand2.AppearanceHeader.Options.UseTextOptions = true;
            this.gridBand2.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridBand2.Caption = "NÍVEL 2";
            this.gridBand2.Columns.Add(this.colTituloSG);
            this.gridBand2.Columns.Add(this.colValorSG);
            this.gridBand2.Columns.Add(this.colEdValorGS);
            this.gridBand2.Columns.Add(this.colMoveSG);
            this.gridBand2.Columns.Add(this.colAgrSGrupo);
            this.gridBand2.Name = "gridBand2";
            this.gridBand2.VisibleIndex = 1;
            this.gridBand2.Width = 400;
            // 
            // colTituloSG
            // 
            this.colTituloSG.Caption = "Título";
            this.colTituloSG.ColumnEdit = this.repositoryText50;
            this.colTituloSG.FieldName = "TituloSG";
            this.colTituloSG.Name = "colTituloSG";
            this.colTituloSG.OptionsColumn.AllowMerge = DevExpress.Utils.DefaultBoolean.True;
            this.colTituloSG.Visible = true;
            this.colTituloSG.Width = 240;
            // 
            // colValorSG
            // 
            this.colValorSG.Caption = "Valor";
            this.colValorSG.ColumnEdit = this.repositoryItemCalcEdit1;
            this.colValorSG.FieldName = "ValorSG";
            this.colValorSG.Name = "colValorSG";
            this.colValorSG.OptionsColumn.AllowMerge = DevExpress.Utils.DefaultBoolean.True;
            this.colValorSG.Visible = true;
            this.colValorSG.Width = 70;
            // 
            // colEdValorGS
            // 
            this.colEdValorGS.Caption = "bandedGridColumn1";
            this.colEdValorGS.Name = "colEdValorGS";
            this.colEdValorGS.OptionsColumn.AllowMerge = DevExpress.Utils.DefaultBoolean.True;
            this.colEdValorGS.OptionsColumn.ShowCaption = false;
            this.colEdValorGS.Visible = true;
            this.colEdValorGS.Width = 30;
            // 
            // colMoveSG
            // 
            this.colMoveSG.Caption = "bandedGridColumn1";
            this.colMoveSG.Name = "colMoveSG";
            this.colMoveSG.OptionsColumn.AllowEdit = false;
            this.colMoveSG.OptionsColumn.AllowMerge = DevExpress.Utils.DefaultBoolean.True;
            this.colMoveSG.OptionsColumn.ReadOnly = true;
            this.colMoveSG.OptionsColumn.ShowCaption = false;
            this.colMoveSG.Visible = true;
            this.colMoveSG.Width = 30;
            // 
            // colAgrSGrupo
            // 
            this.colAgrSGrupo.Caption = "bandedGridColumn2";
            this.colAgrSGrupo.Name = "colAgrSGrupo";
            this.colAgrSGrupo.OptionsColumn.AllowMerge = DevExpress.Utils.DefaultBoolean.True;
            this.colAgrSGrupo.OptionsColumn.FixedWidth = true;
            this.colAgrSGrupo.OptionsColumn.ShowCaption = false;
            this.colAgrSGrupo.Visible = true;
            this.colAgrSGrupo.Width = 30;
            // 
            // gridBand5
            // 
            this.gridBand5.AppearanceHeader.Options.UseTextOptions = true;
            this.gridBand5.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridBand5.Caption = "Contas";
            this.gridBand5.Columns.Add(this.colPLA);
            this.gridBand5.Columns.Add(this.colTituloD);
            this.gridBand5.Columns.Add(this.colValorD);
            this.gridBand5.Columns.Add(this.colMoveD);
            this.gridBand5.Columns.Add(this.colAgrDetalhe);
            this.gridBand5.Columns.Add(this.colTituloDetalhe);
            this.gridBand5.Name = "gridBand5";
            this.gridBand5.VisibleIndex = 2;
            this.gridBand5.Width = 440;
            // 
            // colPLA
            // 
            this.colPLA.Caption = "Conta";
            this.colPLA.FieldName = "PLA";
            this.colPLA.Name = "colPLA";
            this.colPLA.OptionsColumn.ReadOnly = true;
            this.colPLA.Visible = true;
            this.colPLA.Width = 70;
            // 
            // colTituloD
            // 
            this.colTituloD.Caption = "Título";
            this.colTituloD.ColumnEdit = this.repositoryText50;
            this.colTituloD.FieldName = "TituloD";
            this.colTituloD.Name = "colTituloD";
            this.colTituloD.OptionsColumn.AllowMerge = DevExpress.Utils.DefaultBoolean.True;
            this.colTituloD.Visible = true;
            this.colTituloD.Width = 240;
            // 
            // colValorD
            // 
            this.colValorD.Caption = "Valor";
            this.colValorD.ColumnEdit = this.repositoryItemCalcEdit1;
            this.colValorD.FieldName = "ValorD";
            this.colValorD.Name = "colValorD";
            this.colValorD.OptionsColumn.AllowMerge = DevExpress.Utils.DefaultBoolean.True;
            this.colValorD.Visible = true;
            this.colValorD.Width = 70;
            // 
            // colMoveD
            // 
            this.colMoveD.Caption = "bandedGridColumn1";
            this.colMoveD.Name = "colMoveD";
            this.colMoveD.OptionsColumn.AllowEdit = false;
            this.colMoveD.OptionsColumn.AllowMerge = DevExpress.Utils.DefaultBoolean.False;
            this.colMoveD.OptionsColumn.ReadOnly = true;
            this.colMoveD.OptionsColumn.ShowCaption = false;
            this.colMoveD.Visible = true;
            this.colMoveD.Width = 30;
            // 
            // colAgrDetalhe
            // 
            this.colAgrDetalhe.Caption = "bandedGridColumn3";
            this.colAgrDetalhe.Name = "colAgrDetalhe";
            this.colAgrDetalhe.OptionsColumn.AllowMerge = DevExpress.Utils.DefaultBoolean.True;
            this.colAgrDetalhe.OptionsColumn.FixedWidth = true;
            this.colAgrDetalhe.OptionsColumn.ShowCaption = false;
            this.colAgrDetalhe.Visible = true;
            this.colAgrDetalhe.Width = 30;
            // 
            // colTituloDetalhe
            // 
            this.colTituloDetalhe.FieldName = "TituloDetalhe";
            this.colTituloDetalhe.Name = "colTituloDetalhe";
            this.colTituloDetalhe.OptionsColumn.AllowMerge = DevExpress.Utils.DefaultBoolean.False;
            this.colTituloDetalhe.OptionsColumn.ShowCaption = false;
            this.colTituloDetalhe.Width = 240;
            // 
            // gridBand3
            // 
            this.gridBand3.AppearanceHeader.Options.UseTextOptions = true;
            this.gridBand3.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridBand3.Caption = "PLano de Contas";
            this.gridBand3.Columns.Add(this.colPLADescricao);
            this.gridBand3.Name = "gridBand3";
            this.gridBand3.Visible = false;
            this.gridBand3.VisibleIndex = -1;
            this.gridBand3.Width = 300;
            // 
            // colPLADescricao
            // 
            this.colPLADescricao.Caption = "Descrição";
            this.colPLADescricao.FieldName = "PLADescricao";
            this.colPLADescricao.Name = "colPLADescricao";
            this.colPLADescricao.OptionsColumn.AllowMerge = DevExpress.Utils.DefaultBoolean.False;
            this.colPLADescricao.OptionsColumn.ReadOnly = true;
            this.colPLADescricao.Visible = true;
            this.colPLADescricao.Width = 240;
            // 
            // gridBand6
            // 
            this.gridBand6.Caption = "gridBand6";
            this.gridBand6.Columns.Add(this.colDel);
            this.gridBand6.Name = "gridBand6";
            this.gridBand6.OptionsBand.ShowCaption = false;
            this.gridBand6.VisibleIndex = 3;
            this.gridBand6.Width = 30;
            // 
            // colDel
            // 
            this.colDel.Caption = "bandedGridColumn1";
            this.colDel.ColumnEdit = this.repositoryItemButtonDelete;
            this.colDel.Name = "colDel";
            this.colDel.OptionsColumn.AllowMerge = DevExpress.Utils.DefaultBoolean.False;
            this.colDel.OptionsColumn.FixedWidth = true;
            this.colDel.OptionsColumn.ShowCaption = false;
            this.colDel.Visible = true;
            this.colDel.Width = 30;
            // 
            // repositoryItemButtonDelete
            // 
            this.repositoryItemButtonDelete.AutoHeight = false;
            this.repositoryItemButtonDelete.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Delete)});
            this.repositoryItemButtonDelete.Name = "repositoryItemButtonDelete";
            this.repositoryItemButtonDelete.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.HideTextEditor;
            this.repositoryItemButtonDelete.ButtonClick += new DevExpress.XtraEditors.Controls.ButtonPressedEventHandler(this.repositoryItemButtonDelete_ButtonClick);
            // 
            // gridControl1
            // 
            this.gridControl1.AllowDrop = true;
            this.gridControl1.DataSource = this.gradeOrcamentoBindingSource;
            this.gridControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridControl1.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.gridControl1.Location = new System.Drawing.Point(0, 175);
            this.gridControl1.LookAndFeel.Style = DevExpress.LookAndFeel.LookAndFeelStyle.Flat;
            this.gridControl1.LookAndFeel.UseDefaultLookAndFeel = false;
            this.gridControl1.MainView = this.bandedGridView1;
            this.gridControl1.Name = "gridControl1";
            this.gridControl1.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemCalcEdit1,
            this.repositoryItemButtonDelete,
            this.repositoryEd,
            this.repMover,
            this.repositoryText50,
            this.repAbrir,
            this.repoFechar});
            this.gridControl1.Size = new System.Drawing.Size(979, 320);
            this.gridControl1.TabIndex = 1;
            this.gridControl1.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.bandedGridView1});
            this.gridControl1.DragDrop += new System.Windows.Forms.DragEventHandler(this.gridControl1_DragDrop);
            this.gridControl1.DragOver += new System.Windows.Forms.DragEventHandler(this.gridControl1_DragOver);
            this.gridControl1.DragLeave += new System.EventHandler(this.gridControl1_DragLeave);
            this.gridControl1.Paint += new System.Windows.Forms.PaintEventHandler(this.gridControl1_Paint);
            // 
            // repositoryEd
            // 
            this.repositoryEd.AutoHeight = false;
            this.repositoryEd.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.repositoryEd.Name = "repositoryEd";
            this.repositoryEd.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.HideTextEditor;
            this.repositoryEd.ButtonClick += new DevExpress.XtraEditors.Controls.ButtonPressedEventHandler(this.repositoryEd_ButtonClick);
            // 
            // repMover
            // 
            this.repMover.AutoHeight = false;
            editorButtonImageOptions1.Image = ((System.Drawing.Image)(resources.GetObject("editorButtonImageOptions1.Image")));
            this.repMover.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(editorButtonImageOptions1, DevExpress.XtraEditors.Controls.ButtonPredefines.Glyph, null)});
            this.repMover.Name = "repMover";
            this.repMover.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.HideTextEditor;
            // 
            // repAbrir
            // 
            this.repAbrir.AutoHeight = false;
            editorButtonImageOptions2.Image = ((System.Drawing.Image)(resources.GetObject("editorButtonImageOptions2.Image")));
            this.repAbrir.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(editorButtonImageOptions2, DevExpress.XtraEditors.Controls.ButtonPredefines.Glyph, null)});
            this.repAbrir.Name = "repAbrir";
            this.repAbrir.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.HideTextEditor;
            this.repAbrir.ButtonClick += new DevExpress.XtraEditors.Controls.ButtonPressedEventHandler(this.repAbrir_ButtonClick);
            // 
            // repoFechar
            // 
            this.repoFechar.AutoHeight = false;
            editorButtonImageOptions3.Image = ((System.Drawing.Image)(resources.GetObject("editorButtonImageOptions3.Image")));
            this.repoFechar.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(editorButtonImageOptions3, DevExpress.XtraEditors.Controls.ButtonPredefines.Glyph, null)});
            this.repoFechar.Name = "repoFechar";
            this.repoFechar.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.HideTextEditor;
            this.repoFechar.ButtonClick += new DevExpress.XtraEditors.Controls.ButtonPressedEventHandler(this.repoFechar_ButtonClick);
            // 
            // saveFileDialog1
            // 
            this.saveFileDialog1.DefaultExt = "XML";
            this.saveFileDialog1.Filter = "(XML)|*.xml";
            // 
            // cOrcamento
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.CaixaAltaGeral = false;
            this.Controls.Add(this.gridControl1);
            this.Controls.Add(this.panelControl1);
            this.Doc = System.Windows.Forms.DockStyle.Fill;
            this.LookAndFeel.UseDefaultLookAndFeel = false;
            this.Name = "cOrcamento";
            this.Size = new System.Drawing.Size(979, 495);
            ((System.ComponentModel.ISupportInitialize)(this.StyleController_F)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl1)).EndInit();
            this.panelControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.radioGroupNivel.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl2)).EndInit();
            this.groupControl2.ResumeLayout(false);
            this.groupControl2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.calcEditTotalOrc.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl1)).EndInit();
            this.groupControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gradeOrcamentoBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCalcEdit1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.bandedGridView1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryText50)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemButtonDelete)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryEd)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repMover)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repAbrir)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repoFechar)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraEditors.PanelControl panelControl1;
        private System.Windows.Forms.BindingSource gradeOrcamentoBindingSource;
        private DevExpress.XtraEditors.SimpleButton simpleButtonG;
        private DevExpress.XtraEditors.SimpleButton simpleButtonSG;
        private DevExpress.XtraEditors.SimpleButton simpleButtonD;
        private DevExpress.XtraEditors.CalcEdit calcEditTotalOrc;
        private DevExpress.XtraEditors.Repository.RepositoryItemCalcEdit repositoryItemCalcEdit1;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridView bandedGridView1;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn colOrdem;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn colID;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn colORG1;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn colORG2;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn colORD;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn colTituloG;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn colValorG;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn colTituloSG;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn colValorSG;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn colPLA;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn colTituloD;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn colValorD;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn colPLADescricao;
        private DevExpress.XtraGrid.GridControl gridControl1;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn colDel;
        private DevExpress.XtraEditors.Repository.RepositoryItemButtonEdit repositoryItemButtonDelete;
        public DevExpress.XtraEditors.LabelControl labelErro;
        public DevExpress.XtraEditors.LabelControl labelCalculado;
        public DevExpress.XtraEditors.LabelControl labelDefinido;
        private DevExpress.XtraEditors.GroupControl groupControl1;
        private DevExpress.XtraEditors.SimpleButton BotSalvaXML;
        private DevExpress.XtraEditors.SimpleButton BotCarregaXML;
        private System.Windows.Forms.SaveFileDialog saveFileDialog1;
        private DevExpress.XtraEditors.GroupControl groupControl2;
        private Framework.objetosNeon.cCompet cCompetF;
        private DevExpress.XtraEditors.LabelControl labelControl3;
        private DevExpress.XtraEditors.LabelControl labelControl2;
        private Framework.objetosNeon.cCompet cCompetI;
        private DevExpress.XtraEditors.LabelControl labelControl4;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn colEdValorG;
        private DevExpress.XtraEditors.Repository.RepositoryItemButtonEdit repositoryEd;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn colEdValorGS;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn colBaseG;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn colBaseSG;
        private DevExpress.XtraEditors.RadioGroup radioGroupNivel;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn colMoveG;
        private DevExpress.XtraEditors.Repository.RepositoryItemButtonEdit repMover;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn colMoveSG;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn colMoveD;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit repositoryText50;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn colStatusValorD;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn colStatusValorG;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn colStatusValorSG;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn colAgrGrupo;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn colAgrSGrupo;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn colAgrDetalhe;
        private DevExpress.XtraEditors.Repository.RepositoryItemButtonEdit repAbrir;
        private DevExpress.XtraEditors.Repository.RepositoryItemButtonEdit repoFechar;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn colBaseD;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn colTituloDetalhe;
        private DevExpress.XtraGrid.Views.BandedGrid.GridBand gridBand7;
        private DevExpress.XtraGrid.Views.BandedGrid.GridBand gridBand1;
        private DevExpress.XtraGrid.Views.BandedGrid.GridBand gridBand4;
        private DevExpress.XtraGrid.Views.BandedGrid.GridBand gridBand2;
        private DevExpress.XtraGrid.Views.BandedGrid.GridBand gridBand5;
        private DevExpress.XtraGrid.Views.BandedGrid.GridBand gridBand3;
        private DevExpress.XtraGrid.Views.BandedGrid.GridBand gridBand6;
    }
}
