﻿using OrcamentoProc;
using CompontesBasicosProc;
using CompontesBasicos;
using System;
using System.Windows.Forms;
using System.Drawing;
using DevExpress.XtraGrid;
using DevExpress.XtraGrid.Views.Grid;
using DevExpress.XtraGrid.Views.Grid.ViewInfo;
using System.Collections.Generic;
using System.Drawing.Drawing2D;

namespace Orcamento
{
    public partial class cOrcamento : ComponenteBase
    {

        private OrcamentoProc.Orcamento Orcamento1;

        private virEnumStatusValor VirEnumStatusValor;

        public cOrcamento()
        {
            InitializeComponent();
            Orcamento1 = new OrcamentoProc.Orcamento();
            //Orcamento1.CompI = new Framework.objetosNeon.Competencia();
            //Orcamento1.CompF = Orcamento1.CompI.ABS_CloneCompet(12);
            AjustaTelaComOrcamento();            
            gradeOrcamentoBindingSource.DataSource = Orcamento1.dOrcGrade;
            //Orcamento1.Simulador = true;
            //gradeOrcamentoBindingSource.DataSource = Orcamento1.Simula(3, 4, 5,true);
            VirEnumStatusValor = new virEnumStatusValor(this);            
        }

        private void AjustaTelaComOrcamento()
        {
            if ((Orcamento1 == null) || (Orcamento1.CompI == null))
                cCompetI.IsNull = true;
            else
            {
                cCompetI.Comp = (Framework.objetosNeon.Competencia)Orcamento1.CompI;
                cCompetI.IsNull = false;
            }
            if ((Orcamento1 == null) || (Orcamento1.CompF == null))
                cCompetF.IsNull = true;
            else
            {
                cCompetF.Comp = (Framework.objetosNeon.Competencia)Orcamento1.CompF;
                cCompetF.IsNull = false;
            }
            cCompetI.AjustaTela();            
            cCompetF.AjustaTela();
        }

        private void bandedGridView1_CellMerge(object sender, DevExpress.XtraGrid.Views.Grid.CellMergeEventArgs e)
        {
            dOrcGrade.GradeOrcamentoRow rowAnterior = (dOrcGrade.GradeOrcamentoRow)bandedGridView1.GetDataRow(e.RowHandle1);
            dOrcGrade.GradeOrcamentoRow rowAtual = (dOrcGrade.GradeOrcamentoRow)bandedGridView1.GetDataRow(e.RowHandle2);
            if ((rowAnterior == null) || (rowAtual == null))
                return;
            if (e.Column.EstaNoGrupo(colTituloG, colValorG, colTituloSG, colValorSG, colTituloD, colValorD))
            {
                if (rowAnterior.ORG1 != rowAtual.ORG1)
                {
                    e.Merge = false;
                    e.Handled = true;
                }
            }
            if (e.Column.EstaNoGrupo(colMoveG,colEdValorG,colAgrGrupo))
            {
                if ((rowAtual.BaseG) || (rowAnterior.BaseG) ||(rowAnterior.ORG1 != rowAtual.ORG1))
                {
                    e.Merge = false;
                    e.Handled = true;
                }
            }
            if (e.Column.EstaNoGrupo(colTituloSG, colValorSG, colTituloD, colValorD))
            {
                if (rowAnterior.IsORG2Null() || rowAtual.IsORG2Null() || (rowAnterior.ORG2 != rowAtual.ORG2))
                {
                    e.Merge = false;
                    e.Handled = true;
                }
            }
            if (e.Column.EstaNoGrupo(colMoveSG, colEdValorGS,colAgrSGrupo))
            {
                if ((rowAnterior.IsORG2Null()) || (rowAtual.IsORG2Null()) || (rowAtual.BaseSG) || (rowAnterior.BaseSG) || (rowAnterior.ORG2 != rowAtual.ORG2))
                {
                    e.Merge = false;
                    e.Handled = true;
                }
            }

            if (e.Column.EstaNoGrupo(colTituloD, colValorD,colPLA))
            {
                if (rowAnterior.IsORDNull() || rowAtual.IsORDNull() || (rowAnterior.ORD != rowAtual.ORD))
                {
                    e.Merge = false;
                    e.Handled = true;
                }
            }

            if (e.Column.EstaNoGrupo(colAgrDetalhe))
            {
                if ((rowAnterior.IsORDNull()) || (rowAtual.IsORDNull()) || rowAnterior.BaseD || rowAtual.BaseD || (rowAnterior.ORD != rowAtual.ORD))
                {
                    e.Merge = false;
                    e.Handled = true;
                }
            }
        }        

        private int ProcuraLinhaPorID(int ID)
        {
            int Retorno = bandedGridView1.LocateByValue("ID", ID);
            //for (int i = 0; i < bandedGridView1.RowCount; i++)
            //    if (((dOrcGrade.GradeOrcamentoRow)bandedGridView1.GetDataRow(i)).ID == ID)
            //        return i;
            return Retorno >= 0 ? Retorno : 0;
        }

        private void simpleButton1_Click(object sender, System.EventArgs e)
        {
            OrcGrupo GrupoFoco = null;
            dOrcGrade.GradeOrcamentoRow rowFoco = (dOrcGrade.GradeOrcamentoRow)bandedGridView1.GetFocusedDataRow();
            if (rowFoco != null)
                GrupoFoco = Orcamento1.GetGrupo(rowFoco.ORG1);
            OrcGrupo NovoG = Orcamento1.NovoGrupo("Novo Grupo", GrupoFoco);            
            bandedGridView1.FocusedRowHandle = ProcuraLinhaPorID(NovoG.rowBase.ID);
            BotSalvaXML.Enabled = true;
        }

        private void simpleButton2_Click(object sender, System.EventArgs e)
        {
            dOrcGrade.GradeOrcamentoRow rowFoco = (dOrcGrade.GradeOrcamentoRow)bandedGridView1.GetFocusedDataRow();
            if (rowFoco == null)
                return;
            OrcGrupo GrupoFoco = Orcamento1.GetGrupo(rowFoco.ORG1);
            if (GrupoFoco == null)
                return;
            SubOrcGrupo Anterior = null;
            if (!rowFoco.IsORG2Null())
                Anterior = Orcamento1.GetSGrupo(rowFoco.ORG2);
            SubOrcGrupo NovoSG = Orcamento1.NovoSubGrupo(GrupoFoco, "Novo Sub Grupo", Anterior);
            bandedGridView1.FocusedRowHandle = ProcuraLinhaPorID(NovoSG.rowBase.ID);
            colValorSG.OptionsColumn.ReadOnly = colTituloSG.OptionsColumn.ReadOnly = false;
        }
       
        private void simpleButton3_Click(object sender, System.EventArgs e)
        {
            dOrcGrade.GradeOrcamentoRow rowFoco = (dOrcGrade.GradeOrcamentoRow)bandedGridView1.GetFocusedDataRow();
            if ((rowFoco == null) || rowFoco.IsORG2Null())
                return;
            SubOrcGrupo SubGrupoFoco = Orcamento1.GetSGrupo(rowFoco.ORG2);
            if (SubGrupoFoco == null)
                return;
            DetalheGrupo Anterior = null;
            if (!rowFoco.IsORDNull())
                Anterior = Orcamento1.GetDetalheGrupo(rowFoco.ORD);
            //cPLAS cPLAS1 = new cPLAS();
            Orcamento1.DOrcamento.PLAnocontasTableAdapter.Fill(Orcamento1.DOrcamento.PLAnocontas);
            
            foreach (dOrcamento.PLAnocontasRow rowPLA in Orcamento1.DOrcamento.PLAnocontas)
                if (Orcamento1.PLAsUsados.ContainsKey(rowPLA.PLA))
                {
                    rowPLA.Ocupado = true;
                    if (Orcamento1.PLAsUsados[rowPLA.PLA] != 0)
                        rowPLA.Valor = Orcamento1.PLAsUsados[rowPLA.PLA];
                }

            foreach (DetalheGrupo DetJa in SubGrupoFoco.Detalhes.Values)
                Orcamento1.DOrcamento.PLAnocontas.FindByPLA(DetJa.PLA).Marcar = true;
            Orcamento1.DOrcamento.AcceptChanges();
            cPLAS cPLAS1 = new cPLAS(Orcamento1.DOrcamento);
            if (cPLAS1.VirShowModulo(EstadosDosComponentes.PopUp) == System.Windows.Forms.DialogResult.OK)
            {
                Validate();
                SortedList<string, decimal> Selecionados = cPLAS1.Selecionados();
                foreach (string PLA in Selecionados.Keys)
                {
                    dOrcamento.PLAnocontasRow rowPLA = Orcamento1.DOrcamento.PLAnocontas.FindByPLA(PLA);
                    DetalheGrupo NovoD = Orcamento1.NovoDetalhe(SubGrupoFoco, rowPLA.PLADescricao, PLA, Selecionados[PLA], Anterior);
                    Anterior = NovoD;
                }                
                bandedGridView1.FocusedRowHandle = ProcuraLinhaPorID(Anterior.rowBase.ID);
                colValorD.OptionsColumn.ReadOnly = colTituloD.OptionsColumn.ReadOnly = false;
            }
            calcEditTotalOrc.Value = Orcamento1.Valor;
            Orcamento1.AjusteDeValoresTela((NivelDetalhe)radioGroupNivel.EditValue);
        }

        private bool ColunaStringAlterada(dOrcGrade.GradeOrcamentoRow row,String Campo)
        {
            object oNovo = row[Campo, System.Data.DataRowVersion.Current];
            if (oNovo == DBNull.Value)
                return false;
            object oVelho = row[Campo, System.Data.DataRowVersion.Original];
            return !oNovo.Equals(oVelho);
        }

        private void bandedGridView1_RowUpdated(object sender, DevExpress.XtraGrid.Views.Base.RowObjectEventArgs e)
        {
            dOrcGrade.GradeOrcamentoRow row = (dOrcGrade.GradeOrcamentoRow)bandedGridView1.GetDataRow(e.RowHandle);
            if (row == null)
                return;
            bool ValoresAlterados = false;
            DetalheGrupo DetalheAlterado = null;
            SubOrcGrupo SubGrupoAlterado = null;
            OrcGrupo GrupoAlterado = null;
            if (ColunaStringAlterada(row, "TituloD"))
            {
                DetalheAlterado = Orcamento1.GetDetalheGrupo(row.ORD);
                DetalheAlterado.Titulo = row.TituloD;
                ValoresAlterados = true;
            }
            if (!row.IsORDNull())
            {
                object oValorDAntigo = row["ValorD", System.Data.DataRowVersion.Original];
                decimal ValorDAntigo = oValorDAntigo == System.DBNull.Value ? 0 : (decimal)oValorDAntigo;
                decimal ValorNovo = row.IsValorDNull() ? 0 : row.ValorD;
                if (ValorNovo != ValorDAntigo)
                {
                    DetalheAlterado = Orcamento1.GetDetalheGrupo(row.ORD);
                    DetalheAlterado.Valor = ValorNovo;
                    ValoresAlterados = true;
                }
            }
            if (ColunaStringAlterada(row, "TituloSG"))
            {
                SubGrupoAlterado = Orcamento1.GetSGrupo(row.ORG2);
                SubGrupoAlterado.Titulo = row.TituloSG;
                ValoresAlterados = true;
            }
            if (!row.IsORG2Null())
            {
                object oValorSGAntigo = row["ValorSG", System.Data.DataRowVersion.Original];
                decimal ValorSGAntigo = oValorSGAntigo == System.DBNull.Value ? 0 : (decimal)oValorSGAntigo;
                decimal ValorNovo = row.IsValorSGNull() ? 0 : row.ValorSG;
                if (ValorNovo != ValorSGAntigo)
                {
                    SubGrupoAlterado = Orcamento1.GetSGrupo(row.ORG2);
                    SubGrupoAlterado.Valor = ValorNovo;
                    ValoresAlterados = true;
                }
            }
            if (ColunaStringAlterada(row, "TituloG"))
            {
                GrupoAlterado = Orcamento1.GetGrupo(row.ORG1);
                GrupoAlterado.Titulo = row.TituloG;
                ValoresAlterados = true;
            }
            if (!row.IsORG1Null())
            {
                object oValorGAntigo = row["ValorG", System.Data.DataRowVersion.Original];
                decimal ValorGAntigo = oValorGAntigo == System.DBNull.Value ? 0 : (decimal)oValorGAntigo;
                decimal ValorNovo = row.IsValorGNull() ? 0 : row.ValorG;
                if (ValorNovo != ValorGAntigo)
                {
                    GrupoAlterado = Orcamento1.GetGrupo(row.ORG1);                    
                    GrupoAlterado.Valor = ValorNovo;                    
                    ValoresAlterados = true;
                }
            }
            row.AcceptChanges();
            if (ValoresAlterados)
            {
                if (DetalheAlterado != null)
                    DetalheAlterado.GravaDB();
                if (SubGrupoAlterado != null)
                    SubGrupoAlterado.GravaDB();
                if(GrupoAlterado != null)
                    GrupoAlterado.GravaDB();
                calcEditTotalOrc.Value = Orcamento1.Valor;
                Orcamento1.AjusteDeValoresTela((NivelDetalhe)radioGroupNivel.EditValue);
            }
        }

        private void bandedGridView1_RowCellStyle(object sender, DevExpress.XtraGrid.Views.Grid.RowCellStyleEventArgs e)
        {
            dOrcGrade.GradeOrcamentoRow row = (dOrcGrade.GradeOrcamentoRow)bandedGridView1.GetDataRow(e.RowHandle);
            if (row == null)
                return;
            if (e.Column.EstaNoGrupo(colValorG, colValorSG, colValorD))
            {
                StatusValor StValor = StatusValor.NaoDefinido;
                if (e.Column == colValorG)
                    StValor = (StatusValor)row.StatusValorG;
                else if (e.Column == colValorSG)
                    StValor = (StatusValor)row.StatusValorSG;
                else if (e.Column == colValorD)
                    StValor = (StatusValor)row.StatusValorD;
                //e.Appearance.ForeColor = Color.Black;
                VirEnumStatusValor.CellStyle(StValor, e);
            }
            else if ((e.Column == colTituloSG) && (!row.IsORG2Null()))
                e.Appearance.BackColor = Color.Cornsilk;
            else if ((e.Column.EstaNoGrupo(colPLA, colTituloD)) && (!row.IsORDNull()))
                e.Appearance.BackColor = Color.Lavender;
        }

        private void bandedGridView1_FocusedRowChanged(object sender, DevExpress.XtraGrid.Views.Base.FocusedRowChangedEventArgs e)
        {
            dOrcGrade.GradeOrcamentoRow row = (dOrcGrade.GradeOrcamentoRow)bandedGridView1.GetDataRow(e.FocusedRowHandle);
            if (row == null)
                return;
            colValorSG.OptionsColumn.ReadOnly = colTituloSG.OptionsColumn.ReadOnly = row.IsORG2Null();
            colValorD.OptionsColumn.ReadOnly = colTituloD.OptionsColumn.ReadOnly = row.IsORDNull();
        }
        
        private void repositoryItemButtonDelete_ButtonClick(object sender, DevExpress.XtraEditors.Controls.ButtonPressedEventArgs e)
        {
            dOrcGrade.GradeOrcamentoRow row = (dOrcGrade.GradeOrcamentoRow)bandedGridView1.GetFocusedDataRow();
            if (row == null)
                return;
            if (!row.IsORDNull())
            {
                DetalheGrupo Detalhe = Orcamento1.GetDetalheGrupo(row.ORD);
                Detalhe.Delete();
            }
            else if (!row.IsORG2Null())
            {
                SubOrcGrupo SGrupo = Orcamento1.GetSGrupo(row.ORG2);
                SGrupo.Delete();
            }
            else if (!row.IsORG1Null())
            {
                OrcGrupo Grupo = Orcamento1.GetGrupo(row.ORG1);
                Grupo.Delete();
                BotSalvaXML.Enabled = Orcamento1.Grupos.Count != 0;
            }
            calcEditTotalOrc.Value = Orcamento1.Valor;
            Orcamento1.AjusteDeValoresTela((NivelDetalhe)radioGroupNivel.EditValue);
        }

        private void BotSalvaXML_Click(object sender, EventArgs e)
        {
            if (Orcamento1.NomeArquivo != null)
                saveFileDialog1.FileName = Orcamento1.NomeArquivo;
            if (saveFileDialog1.ShowDialog() == System.Windows.Forms.DialogResult.OK)
                Orcamento1.SalvarXML(saveFileDialog1.FileName);
        }

        private void cCompetI_OnChange(object sender, EventArgs e)
        {
            if (cCompetI.IsNull)
            {
                Orcamento1.CompI = null;
                Orcamento1.CompF = null;
            }
            else
            {
                Orcamento1.CompI = cCompetI.Comp;
                if (cCompetF.Comp == null)
                    Orcamento1.CompF = cCompetI.Comp.CloneCompet(12);
            }


        }

        private void cCompetF_OnChange(object sender, EventArgs e)
        {
            Orcamento1.CompF = cCompetF.Comp;
        }

        public void CarregaArquivo(string NomeArquivo)
        {
            radioGroupNivel.EditValue = (int)NivelDetalhe.Aberto;
            if (Orcamento1 != null)
                Orcamento1.dOrcGrade.GradeOrcamento.Clear();
            Orcamento1 = OrcamentoProc.Orcamento.RecuperaXML(NomeArquivo);
            if (Orcamento1 != null)
            {
                AjustaTelaComOrcamento();
                calcEditTotalOrc.Value = Orcamento1.Valor;
                gradeOrcamentoBindingSource.DataSource = Orcamento1.dOrcGrade;
                BotSalvaXML.Enabled = true;
            }
            else
                MessageBox.Show("Erro na carga do arquivo");
        }

        private void BotCarregaXML_Click(object sender, EventArgs e)
        {
            cAbrir cAbrir1 = new cAbrir();
            if (cAbrir1.ShowEmPopUp() == DialogResult.OK)
            {
                Orcamento1.Limpa();
                radioGroupNivel.EditValue = (int)NivelDetalhe.Aberto;
                switch (cAbrir1.Retorno)
                {
                    case cAbrir.tipoAbre.arquivo:               
                        CarregaArquivo(cAbrir1.NomeArquivo);
                        break;
                    case cAbrir.tipoAbre.modelo:
                        if (cAbrir1.rowMBA != null)
                        {
                            
                            //Orcamento1.Simulador = true;
                            cAbrir1.dModelos1.ModeloBalanceteGrupoTableAdapter.FillByMBA(cAbrir1.dModelos1.ModeloBalanceteGrupo, cAbrir1.rowMBA.MBA);
                            cAbrir1.dModelos1.ModeloBalanceteDetalheTableAdapter.FillByMBA(cAbrir1.dModelos1.ModeloBalanceteDetalhe, cAbrir1.rowMBA.MBA);
                            cAbrir1.dModelos1.MBDxPLATableAdapter.FillByMBA(cAbrir1.dModelos1.MBDxPLA, cAbrir1.rowMBA.MBA);
                            OrcGrupo Grupo = null;
                            foreach (OrcamentoProc.dModelos.ModeloBalanceteGrupoRow rowMBG in cAbrir1.rowMBA.GetModeloBalanceteGrupoRows())
                            {
                                Grupo = Orcamento1.NovoGrupo(rowMBG.MBGTitulo, Grupo, null);
                                SubOrcGrupo SGrupo = null;
                                foreach (dModelos.ModeloBalanceteDetalheRow rowMBD in rowMBG.GetModeloBalanceteDetalheRows())
                                {
                                    SGrupo = Orcamento1.NovoSubGrupo(Grupo, rowMBD.MBDTitulo, SGrupo, null, rowMBD.MBDDetalhamento != 0);
                                    DetalheGrupo Detalhe = null;
                                    foreach (dModelos.MBDxPLARow rowPLA in rowMBD.GetMBDxPLARows())
                                        Detalhe = Orcamento1.NovoDetalhe(SGrupo, rowPLA.IsMBDxPLADescricaoNull() ? rowPLA.PLADescricao : rowPLA.MBDxPLADescricao, rowPLA.PLA, null, Detalhe, rowPLA.MBDxPLADescricaoLivre);
                                };
                            }
                            AjustaTelaComOrcamento();
                            Orcamento1.ConstroiDadosTela(NivelDetalhe.Aberto);
                        }
                        break;
                    case cAbrir.tipoAbre.novo:
                        //Orcamento1.Simulador = false;
                        Orcamento1.CriaNovoOrcamento(cAbrir1.CON, cAbrir1.CompI, cAbrir1.CompF);
                        break;
                    case cAbrir.tipoAbre.orc:
                        Orcamento1 = new OrcamentoProc.Orcamento(cAbrir1.ORC);                        
                        AjustaTelaComOrcamento();
                        calcEditTotalOrc.Value = Orcamento1.Valor;
                        gradeOrcamentoBindingSource.DataSource = Orcamento1.dOrcGrade;
                        BotSalvaXML.Enabled = true;
                        break;
                }
            }
        }

        private void repositoryEd_ButtonClick(object sender, DevExpress.XtraEditors.Controls.ButtonPressedEventArgs e)
        {
            dOrcGrade.GradeOrcamentoRow row = (dOrcGrade.GradeOrcamentoRow)bandedGridView1.GetFocusedDataRow();
            if (row == null)
                return;
            bool Alterado = false;
            if (bandedGridView1.FocusedColumn == colEdValorG)
            {
                cEditaMerge cEdit = new cEditaMerge(row.TituloG, row.IsValorGNull() ? (decimal?)null : row.ValorG);
                if (cEdit.VirShowModulo(EstadosDosComponentes.PopUp) == System.Windows.Forms.DialogResult.OK)
                {
                    OrcGrupo Grupo = Orcamento1.GetGrupo(row.ORG1);
                    Grupo.Titulo = cEdit.strTitulo;
                    Grupo.Valor = cEdit.Valor;
                    //row.TituloG = cEdit.strTitulo;
                    //if (cEdit.Valor.HasValue)
                    //    row.ValorG = cEdit.Valor.Value;
                    //else
                    //    row.SetValorGNull();
                    Alterado = true;
                }
            }
            else if (bandedGridView1.FocusedColumn == colEdValorGS)
            {
                if (row.IsORG2Null())
                    return;
                cEditaMerge cEdit = new cEditaMerge(row.TituloSG, row.IsValorSGNull() ? (decimal?)null : row.ValorSG);
                if (cEdit.VirShowModulo(EstadosDosComponentes.PopUp) == System.Windows.Forms.DialogResult.OK)
                {
                    SubOrcGrupo SGrupo = Orcamento1.GetSGrupo(row.ORG2);
                    SGrupo.Titulo = cEdit.strTitulo;
                    SGrupo.Valor = cEdit.Valor;

                    //row.TituloSG = cEdit.strTitulo;
                    //if (cEdit.Valor.HasValue)
                    //    row.ValorSG = cEdit.Valor.Value;
                    //else
                    //    row.SetValorSGNull();
                    Alterado = true;
                }
            }
            if (Alterado)
            {
                row.AcceptChanges();
                Orcamento1.AjusteDeValoresTela((NivelDetalhe)radioGroupNivel.EditValue);
                calcEditTotalOrc.Value = Orcamento1.Valor;
            }
        }
        
        private void bandedGridView1_CustomRowCellEdit(object sender, DevExpress.XtraGrid.Views.Grid.CustomRowCellEditEventArgs e)
        {
            dOrcGrade.GradeOrcamentoRow row = (dOrcGrade.GradeOrcamentoRow)bandedGridView1.GetDataRow(e.RowHandle);
            if (row != null)
            {
                if (e.Column == colAgrGrupo)
                {
                    if ((NivelDetalhe)radioGroupNivel.EditValue != NivelDetalhe.Aberto)
                    {
                        OrcGrupo Grupo = Orcamento1.GetGrupo(row.ORG1);
                        if(Grupo == null)
                            e.RepositoryItem = null;
                        else
                        {
                            if (row.BaseG)
                            {
                                if (Grupo.Aberto((NivelDetalhe)radioGroupNivel.EditValue))
                                    e.RepositoryItem = repoFechar;
                                else
                                    e.RepositoryItem = repAbrir;
                            }
                            else
                                e.RepositoryItem = null;
                        }
                    }
                    else
                        e.RepositoryItem = null;
                }
                else if (e.Column == colAgrSGrupo)
                {
                    if (((NivelDetalhe)radioGroupNivel.EditValue != NivelDetalhe.Aberto) && (!row.IsORG2Null()))
                    {
                        SubOrcGrupo SGrupo = Orcamento1.GetSGrupo(row.ORG2);
                        if (SGrupo == null)
                            e.RepositoryItem = null;
                        else
                        {
                            if (row.BaseSG)
                            {
                                if (SGrupo.Aberto((NivelDetalhe)radioGroupNivel.EditValue))
                                    e.RepositoryItem = repoFechar;
                                else
                                    e.RepositoryItem = repAbrir;
                            }
                            else
                                e.RepositoryItem = null;
                        }
                    }
                    else
                        e.RepositoryItem = null;
                }
                else if (e.Column == colAgrDetalhe)
                {
                    if (((NivelDetalhe)radioGroupNivel.EditValue != NivelDetalhe.Aberto) && (!row.IsORDNull()))
                    {
                        DetalheGrupo Detalhe = Orcamento1.GetDetalheGrupo(row.ORD);
                        if(Detalhe == null)
                            e.RepositoryItem = null;
                        else
                        {
                            if (row.BaseD)
                            {
                                if (Detalhe.Aberto((NivelDetalhe)radioGroupNivel.EditValue))
                                    e.RepositoryItem = repoFechar;
                                else
                                    e.RepositoryItem = repAbrir;
                            }
                            else
                                e.RepositoryItem = null;
                        }
                    }
                    else
                        e.RepositoryItem = null;
                }
                else if (e.Column == colEdValorG)
                {
                    if (row.BaseG)
                        e.RepositoryItem = repositoryEd;
                    else
                        e.RepositoryItem = null;
                }
                else if (e.Column == colEdValorGS)
                {
                    if ((row.BaseSG) && (!row.IsORG2Null()))
                        e.RepositoryItem = repositoryEd;
                    else
                        e.RepositoryItem = null;
                }
                else if (e.Column.EstaNoGrupo(colMoveG))
                {
                    if (row.BaseG)
                        e.RepositoryItem = repMover;
                    else
                        e.RepositoryItem = null;
                }
                else if (e.Column.EstaNoGrupo(colMoveSG))
                {
                    if (row.BaseSG)
                        e.RepositoryItem = repMover;
                    else
                        e.RepositoryItem = null;
                }
                else if (e.Column.EstaNoGrupo(colMoveD))
                {
                    if (!row.IsORDNull())
                        e.RepositoryItem = repMover;
                }                
            }
        }

        private enum ElementoMovendo { nenhum, Grupo, SubGrupo ,Detalhe}
        private ElementoMovendo elementoMovendo = ElementoMovendo.nenhum;
        private int dropTargetRowHandle = -1;
        private NoBase ElementoMover;        
        private GridHitInfo downHitInfoElemento = null;
        
        private int DropTargetRowHandle
        {
            get { return dropTargetRowHandle; }
            set
            {
                dropTargetRowHandle = value;
                gridControl1.Invalidate();
            }
        }

        private void bandedGridView1_MouseDown(object sender, System.Windows.Forms.MouseEventArgs e)
        {
            if ((NivelDetalhe)radioGroupNivel.EditValue == NivelDetalhe.Aberto)
            {
                downHitInfoElemento = null;
                elementoMovendo = ElementoMovendo.nenhum;
                GridHitInfo hitInfo = bandedGridView1.CalcHitInfo(new Point(e.X, e.Y));
                if (ModifierKeys != Keys.None)
                    return;
                if ((e.Button == MouseButtons.Left)
                    &&
                    hitInfo.InRow
                    &&
                    (hitInfo.RowHandle != GridControl.NewItemRowHandle)
                    &&
                    (hitInfo.Column.EstaNoGrupo(colMoveG, colMoveSG, colMoveD)))
                {
                    Validate();
                    downHitInfoElemento = hitInfo;
                    if (hitInfo.Column == colMoveG)
                        elementoMovendo = ElementoMovendo.Grupo;
                    else if (hitInfo.Column == colMoveSG)
                        elementoMovendo = ElementoMovendo.SubGrupo;
                    else if (hitInfo.Column == colMoveD)
                        elementoMovendo = ElementoMovendo.Detalhe;
                }
            }
        }

        private void bandedGridView1_MouseMove(object sender, System.Windows.Forms.MouseEventArgs e)
        {            
            dOrcGrade.GradeOrcamentoRow rowMover = (dOrcGrade.GradeOrcamentoRow)bandedGridView1.GetFocusedDataRow();                        
            if (
                 (e.Button == MouseButtons.Left)
                  &&
                 (downHitInfoElemento != null)
                  &&
                 (rowMover != null)
                )
            {                
                Size dragSize = SystemInformation.DragSize;
                Rectangle dragRect = new Rectangle(new Point(downHitInfoElemento.HitPoint.X - dragSize.Width / 2, downHitInfoElemento.HitPoint.Y - dragSize.Height / 2), dragSize);
                ElementoMover = null;
                if (!dragRect.Contains(new Point(e.X, e.Y)))
                {
                    switch (elementoMovendo)
                    {
                        case ElementoMovendo.Grupo:
                            ElementoMover = Orcamento1.GetGrupo(rowMover.ORG1);
                            break;
                        case ElementoMovendo.SubGrupo:
                            ElementoMover = Orcamento1.GetSGrupo(rowMover.ORG2);
                            break;
                        case ElementoMovendo.Detalhe:
                            if (!rowMover.IsORDNull())
                                ElementoMover = Orcamento1.GetDetalheGrupo(rowMover.ORD);
                            break;
                    }
                    if (ElementoMover != null)
                        bandedGridView1.GridControl.DoDragDrop(ElementoMover, DragDropEffects.All);
                    else
                        elementoMovendo = ElementoMovendo.nenhum;
                    downHitInfoElemento = null;                    
                }
                
            }
        }
               
        private void gridControl1_DragOver(object sender, DragEventArgs e)
        {
            if (elementoMovendo == ElementoMovendo.nenhum)                                      
            {
                e.Effect = DragDropEffects.None;
                return;
            }            
            Point pt = gridControl1.PointToClient(new Point(e.X, e.Y));            
            GridHitInfo hitInfo = bandedGridView1.CalcHitInfo(gridControl1.PointToClient(new Point(e.X, e.Y)));
            if (hitInfo.HitTest == GridHitTest.EmptyRow)
            {
                DropTargetRowHandle = bandedGridView1.DataRowCount;
                if (elementoMovendo == ElementoMovendo.Detalhe)
                {
                    dOrcGrade.GradeOrcamentoRow rowTargetD = (dOrcGrade.GradeOrcamentoRow)bandedGridView1.GetDataRow(DropTargetRowHandle - 1);
                    if (rowTargetD.IsORG2Null())
                        dropTargetRowHandle = -1;
                }
            }
            else
            {
                DropTargetRowHandle = hitInfo.RowHandle;
                switch (elementoMovendo)
                {
                    case ElementoMovendo.Grupo:
                        if (DropTargetRowHandle >= 0)
                            for (;;)
                            {
                                dOrcGrade.GradeOrcamentoRow rowTarget = (dOrcGrade.GradeOrcamentoRow)bandedGridView1.GetDataRow(DropTargetRowHandle);
                                if (rowTarget.BaseG)
                                    break;
                                dropTargetRowHandle--;
                                if (dropTargetRowHandle == 0)
                                    break;
                            }

                        break;
                    case ElementoMovendo.SubGrupo:
                        if (DropTargetRowHandle >= 0)
                            for (;;)
                            {
                                dOrcGrade.GradeOrcamentoRow rowTarget = (dOrcGrade.GradeOrcamentoRow)bandedGridView1.GetDataRow(DropTargetRowHandle);
                                if (rowTarget.IsORG2Null())
                                    break;
                                if (rowTarget.BaseSG)
                                    break;
                                dropTargetRowHandle--;
                                if (dropTargetRowHandle <= 0)
                                    break;
                            }
                        break;
                    case ElementoMovendo.Detalhe:
                        dOrcGrade.GradeOrcamentoRow rowTargetD = (dOrcGrade.GradeOrcamentoRow)bandedGridView1.GetDataRow(DropTargetRowHandle);
                        if ((rowTargetD == null) || (rowTargetD.IsORG2Null()))
                            dropTargetRowHandle = -1;
                        break;
                }
            }                                    
            if (DropTargetRowHandle >= 0)
                e.Effect = DragDropEffects.Move;
            else
                e.Effect = DragDropEffects.None;
        }

        private void gridControl1_Paint(object sender, PaintEventArgs e)
        {
            if ((DropTargetRowHandle < 0) && (elementoMovendo != ElementoMovendo.nenhum))
                return;            
            bool isBottomLine = (DropTargetRowHandle == bandedGridView1.DataRowCount);

            GridViewInfo viewInfo = bandedGridView1.GetViewInfo() as GridViewInfo;
            GridRowInfo rowInfo = viewInfo.GetGridRowInfo(isBottomLine ? DropTargetRowHandle - 1 : DropTargetRowHandle);
            if (rowInfo == null)
                return;
            int FimBanda = 0;
            switch (elementoMovendo)
            {
                case ElementoMovendo.Grupo:
                    FimBanda = viewInfo.GetColumnLeftCoord(colMoveG) + rowInfo.IndicatorRect.Right - bandedGridView1.LeftCoord;
                    break;
                case ElementoMovendo.SubGrupo:
                    FimBanda = viewInfo.GetColumnLeftCoord(colMoveSG) + rowInfo.IndicatorRect.Right - bandedGridView1.LeftCoord;
                    break;
                case ElementoMovendo.Detalhe:
                    FimBanda = viewInfo.GetColumnLeftCoord(colMoveD) + rowInfo.IndicatorRect.Right - bandedGridView1.LeftCoord;
                    break;
            }                                                                    
            Point p1 = (isBottomLine) ? new Point(rowInfo.Bounds.Left, rowInfo.TotalBounds.Bottom - 1) : new Point(rowInfo.Bounds.Left, rowInfo.Bounds.Top - 1);                
            e.Graphics.FillRectangle(Brushes.Red, p1.X, p1.Y-2, FimBanda, 4);
        }

        private void gridControl1_DragLeave(object sender, EventArgs e)
        {
            DropTargetRowHandle = -1;
        }

        private void gridControl1_DragDrop(object sender, DragEventArgs e)
        {
            Point pt = gridControl1.PointToClient(new Point(e.X, e.Y));
            GridHitInfo hitInfo = bandedGridView1.CalcHitInfo(gridControl1.PointToClient(new Point(e.X, e.Y)));
            int TargetRowHandle = hitInfo.RowHandle;
            int NovaOrdem;
            switch (elementoMovendo)
            {
                case ElementoMovendo.Grupo:
                    OrcGrupo GrupoMover = (OrcGrupo)ElementoMover;
                    if (hitInfo.HitTest == GridHitTest.EmptyRow)
                    {
                        dOrcGrade.GradeOrcamentoRow rowUltima = (dOrcGrade.GradeOrcamentoRow)bandedGridView1.GetDataRow(bandedGridView1.RowCount - 1);
                        OrcGrupo GrupoUltimo = Orcamento1.GetGrupo(rowUltima.ORG1);
                        GrupoMover.Ordem = GrupoUltimo.Ordem;
                        GrupoMover.IncOrdem(true);
                    }
                    else
                    {
                        dOrcGrade.GradeOrcamentoRow rowTarget = (dOrcGrade.GradeOrcamentoRow)bandedGridView1.GetDataRow(DropTargetRowHandle);
                        if (rowTarget != null)
                        {
                            OrcGrupo GrupoTarget = Orcamento1.GetGrupo(rowTarget.ORG1);
                            if (GrupoMover != GrupoTarget)
                                Orcamento1.MoveGrupo(GrupoMover, GrupoTarget);
                        }
                    }
                    break;
                case ElementoMovendo.SubGrupo:
                    SubOrcGrupo SubGrupoMover = (SubOrcGrupo)ElementoMover;
                    SubOrcGrupo SubGrupoAlvo = null;
                    OrcGrupo GrupoAlvo = null;
                    NovaOrdem = 0;
                    if (hitInfo.HitTest == GridHitTest.EmptyRow)
                    {
                        dOrcGrade.GradeOrcamentoRow rowUltima = (dOrcGrade.GradeOrcamentoRow)bandedGridView1.GetDataRow(bandedGridView1.RowCount - 1);
                        GrupoAlvo = Orcamento1.GetGrupo(rowUltima.ORG1);
                        if (!rowUltima.IsORG2Null())
                        {
                            SubGrupoAlvo = Orcamento1.GetSGrupo(rowUltima.ORG2);
                            NovaOrdem = SubGrupoAlvo.Ordem + 1;                            
                        }                        
                    }
                    else
                    {                        
                        dOrcGrade.GradeOrcamentoRow rowTarget = (dOrcGrade.GradeOrcamentoRow)bandedGridView1.GetDataRow(DropTargetRowHandle);
                        if (rowTarget == null)
                            break;
                        GrupoAlvo = Orcamento1.GetGrupo(rowTarget.ORG1);
                        if (!rowTarget.IsORG2Null())
                        {
                            SubGrupoAlvo = Orcamento1.GetSGrupo(rowTarget.ORG2);
                            NovaOrdem = SubGrupoAlvo.Ordem;
                        }                        
                    }
                    SubGrupoMover.PaiGrupo.Delete(SubGrupoMover);
                    GrupoAlvo.AddSubGrupo(SubGrupoMover, NovaOrdem);
                    break;
                case ElementoMovendo.Detalhe:
                    DetalheGrupo DeatalheMover = (DetalheGrupo)ElementoMover;
                    DetalheGrupo DetalheAlvo = null;
                    SubOrcGrupo SubGrupoAlvoD = null;
                    NovaOrdem = 0;
                    if (hitInfo.HitTest == GridHitTest.EmptyRow)
                    {
                        dOrcGrade.GradeOrcamentoRow rowUltima = (dOrcGrade.GradeOrcamentoRow)bandedGridView1.GetDataRow(bandedGridView1.RowCount - 1);
                        if (rowUltima.IsORG2Null())
                            break;
                        SubGrupoAlvoD = Orcamento1.GetSGrupo(rowUltima.ORG2);
                        if (!rowUltima.IsORDNull())
                        {
                            DetalheAlvo = Orcamento1.GetDetalheGrupo(rowUltima.ORD);
                            NovaOrdem = DetalheAlvo.Ordem + 1;                            
                        }                       
                    }
                    else
                    {                               
                        dOrcGrade.GradeOrcamentoRow rowTarget = (dOrcGrade.GradeOrcamentoRow)bandedGridView1.GetDataRow(DropTargetRowHandle);
                        if ((rowTarget == null) || (rowTarget.IsORG2Null()))
                            break;
                        SubGrupoAlvoD = Orcamento1.GetSGrupo(rowTarget.ORG2);
                        if (!rowTarget.IsORDNull())
                            DetalheAlvo = Orcamento1.GetDetalheGrupo(rowTarget.ORD);                        
                        if (DetalheAlvo != null)
                        {
                            if (DeatalheMover == DetalheAlvo)
                                break;
                            NovaOrdem = DetalheAlvo.Ordem;                                                           
                        }                                                
                    }
                    DeatalheMover.PaiSubGrupo.Delete(DeatalheMover);
                    SubGrupoAlvoD.AddDetalhe(DeatalheMover, NovaOrdem);
                    break;
            }
            DropTargetRowHandle = -1;
            Orcamento1.AjusteDeValoresTela((NivelDetalhe)radioGroupNivel.EditValue);
        }

        private void ConstroiDadosTela(NivelDetalhe Nivel)
        {
            if (Orcamento1 != null)
            {
                try
                {
                    bandedGridView1.BeginDataUpdate();
                    int? OrdemTop = null;
                    int? OrdemFoco = null;
                    dOrcGrade.GradeOrcamentoRow row = (dOrcGrade.GradeOrcamentoRow)bandedGridView1.GetDataRow(bandedGridView1.TopRowIndex);
                    if (row != null)
                        OrdemTop = row.Ordem;
                    row = (dOrcGrade.GradeOrcamentoRow)bandedGridView1.GetFocusedDataRow();
                    if (row != null)
                        OrdemFoco = row.Ordem;

                    Orcamento1.ConstroiDadosTela(Nivel);
                    bool terminado = false;
                    int Divisor = 10;
                    int rowHandle;
                    if (OrdemTop.HasValue)
                    {
                        while (!terminado && Divisor != 0)
                        {
                            rowHandle = bandedGridView1.LocateByValue("Ordem", OrdemTop.Value);
                            if (rowHandle != GridControl.InvalidRowHandle)
                            {
                                bandedGridView1.TopRowIndex = rowHandle;
                                terminado = true;
                            }
                            else
                            {
                                OrdemTop = (OrdemTop / Divisor) * Divisor;
                                Divisor *= 10;
                            }
                        }
                    }
                    if (!terminado && (OrdemTop.HasValue))
                    {
                        rowHandle = bandedGridView1.LocateByValue("Ordem", OrdemFoco.Value);
                        if (rowHandle != GridControl.InvalidRowHandle)
                            bandedGridView1.FocusedRowHandle = rowHandle;
                    }                    
                }
                finally
                {
                    bandedGridView1.EndDataUpdate();
                }
            }
        }

        private void radioGroupNivel_EditValueChanged(object sender, EventArgs e)
        {
            ConstroiDadosTela((NivelDetalhe)radioGroupNivel.EditValue);
            if ((NivelDetalhe)radioGroupNivel.EditValue == NivelDetalhe.Aberto)
            {
                colMoveG.Visible = colMoveSG.Visible = colMoveD.Visible = true;
                colAgrGrupo.Visible = colAgrSGrupo.Visible = colAgrDetalhe.Visible = false;
                colDel.Visible = true;
                simpleButtonG.Visible = simpleButtonSG.Visible = simpleButtonD.Visible = true;
                colTituloDetalhe.Visible = false;
            }
            else
            {
                colMoveG.Visible = colMoveSG.Visible = colMoveD.Visible = false;
                colAgrGrupo.Visible = colAgrSGrupo.Visible = colAgrDetalhe.Visible = true;
                colDel.Visible = false;
                simpleButtonG.Visible = simpleButtonSG.Visible = simpleButtonD.Visible = false;
                colTituloDetalhe.Visible = true;
            }
        }

        private void AbreFecha(bool Abre)
        {
            dOrcGrade.GradeOrcamentoRow row = (dOrcGrade.GradeOrcamentoRow)bandedGridView1.GetFocusedDataRow();
            if (row == null)
                return;
            if (bandedGridView1.FocusedColumn == colAgrGrupo)
            {
                OrcGrupo Grupo = Orcamento1.GetGrupo(row.ORG1);
                Grupo.SetAberto((NivelDetalhe)radioGroupNivel.EditValue, Abre);
                ConstroiDadosTela((NivelDetalhe)radioGroupNivel.EditValue);
            }
            else if (bandedGridView1.FocusedColumn == colAgrSGrupo)
            {
                if (!row.IsORG2Null())
                {
                    SubOrcGrupo SGrupo = Orcamento1.GetSGrupo(row.ORG2);
                    SGrupo.SetAberto((NivelDetalhe)radioGroupNivel.EditValue, Abre);
                    ConstroiDadosTela((NivelDetalhe)radioGroupNivel.EditValue);
                }
            }
            else if (bandedGridView1.FocusedColumn == colAgrDetalhe)
            {
                if (!row.IsORDNull())
                {
                    DetalheGrupo Detalhe = Orcamento1.GetDetalheGrupo(row.ORD);
                    Detalhe.SetAberto((NivelDetalhe)radioGroupNivel.EditValue, Abre);
                    ConstroiDadosTela((NivelDetalhe)radioGroupNivel.EditValue);
                }
            }            
        }

        private void repoFechar_ButtonClick(object sender, DevExpress.XtraEditors.Controls.ButtonPressedEventArgs e)
        {
            AbreFecha(false);
        }

        private void repAbrir_ButtonClick(object sender, DevExpress.XtraEditors.Controls.ButtonPressedEventArgs e)
        {
            AbreFecha(true);
        }

        private NivelDetalhe NivelAtivo { get => (NivelDetalhe)radioGroupNivel.EditValue; }

        private enum TipoColuna { nTratada,SG,D,SD};

        private void bandedGridView1_CustomDrawCell(object sender, DevExpress.XtraGrid.Views.Base.RowCellCustomDrawEventArgs e)
        {
            if (NivelAtivo == NivelDetalhe.Aberto)
                return;
            TipoColuna Tipo = TipoColuna.nTratada;

            if (e.Column.EstaNoGrupo(colTituloSG, colValorSG, colEdValorGS, colAgrSGrupo))
                Tipo = TipoColuna.SG;
            else if (e.Column.EstaNoGrupo(colPLA, colTituloD, colValorD, colAgrDetalhe))
                Tipo = TipoColuna.D;
            else if (e.Column.EstaNoGrupo(colTituloDetalhe))
                Tipo = TipoColuna.SD;

            if (Tipo == TipoColuna.nTratada)
                return;
            dOrcGrade.GradeOrcamentoRow row = (dOrcGrade.GradeOrcamentoRow)bandedGridView1.GetDataRow(e.RowHandle);
            if ((row == null) || (row.IsORG1Null()))
                return;
            OrcGrupo Grupo = Orcamento1.GetGrupo(row.ORG1);
            if (!Grupo.Aberto(NivelAtivo))
            {
                e.Cache.Paint.FillRectangle(e.Graphics, new HatchBrush(HatchStyle.DiagonalCross, Color.LightGray, Color.White), e.Bounds);
                e.Handled = true;
            }
            else
            {
                if (row.IsORG2Null())
                {
                    if (Tipo.EstaNoGrupo(TipoColuna.SG))
                    {
                        e.Cache.Paint.FillRectangle(e.Graphics, new HatchBrush(HatchStyle.DiagonalCross, Color.Red, Color.White), e.Bounds);
                        e.Handled = true;
                    }
                    return;
                }
                else
                {
                    if (Tipo.EstaNoGrupo(TipoColuna.D,TipoColuna.SD))
                    {
                        SubOrcGrupo SGrupo = Orcamento1.GetSGrupo(row.ORG2);
                        if (!SGrupo.Aberto(NivelAtivo))
                        {
                            e.Cache.Paint.FillRectangle(e.Graphics, new HatchBrush(HatchStyle.DiagonalCross, Color.LightGray, Color.White), e.Bounds);
                            e.Handled = true;
                        }
                        else
                        {
                            if (row.IsORDNull())
                            {
                                if (Tipo.EstaNoGrupo(TipoColuna.D))
                                {
                                    e.Cache.Paint.FillRectangle(e.Graphics, new HatchBrush(HatchStyle.DiagonalCross, Color.Red, Color.White), e.Bounds);
                                    e.Handled = true;
                                }
                                return;
                            }
                            else
                            {
                                if (Tipo.EstaNoGrupo(TipoColuna.SD))
                                {
                                    DetalheGrupo Detalhe = Orcamento1.GetDetalheGrupo(row.ORD);
                                    if (!Detalhe.Aberto(NivelAtivo))
                                    {
                                        e.Cache.Paint.FillRectangle(e.Graphics, new HatchBrush(HatchStyle.DiagonalCross, Color.LightGray, Color.White), e.Bounds);
                                        e.Handled = true;
                                    }
                                }
                            }
                        }
                    }
                }
            }                                                                              
        }

    }



    /// <summary>
    /// virEnum para campo 
    /// </summary>
    public class virEnumStatusValor : dllVirEnum.VirEnum
    {
        /// <summary>
        /// Construtor padrão
        /// </summary>
        /// <param name="Tipo">Tipo</param>
        public virEnumStatusValor(cOrcamento COrcamento) :
            base(typeof(StatusValor))
        {            
            GravaNomes(StatusValor.NaoDefinido, "Sem valor definido", Color.White);
            GravaNomes(StatusValor.DefinidoValido, "Valor definido", Color.Lime, COrcamento.labelDefinido);
            GravaNomes(StatusValor.DefinidoInvalido, "Valor não corresponde a soma dos itens", Color.FromArgb(245, 119, 105), COrcamento.labelErro);
            GravaNomes(StatusValor.Calculado, "Calculado", Color.SandyBrown, COrcamento.labelCalculado);            
        }
    }
}
