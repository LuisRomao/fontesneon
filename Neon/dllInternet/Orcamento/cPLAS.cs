﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using OrcamentoProc;

namespace Orcamento
{
    public partial class cPLAS : CompontesBasicos.ComponenteBaseDialog
    {
        public cPLAS()
        {
            InitializeComponent();
        }

        public cPLAS(OrcamentoProc.dOrcamento _dOrcamento) : this()
        {
            pLAnocontasBindingSource.DataSource = _dOrcamento.PLAnocontas;
            //foreach(dOrcamento.PLAnocontasRow row in _dOrcamento.PLAnocontas)
            //    gridView1.GetRowHandle()
            //gridView1.SelectRow();
        }

        public SortedList<string, decimal> Selecionados()
        {
            SortedList<string, decimal> retorno = new SortedList<string, decimal>();
            foreach (int Sel in gridView1.GetSelectedRows())
            {
                dOrcamento.PLAnocontasRow row = (dOrcamento.PLAnocontasRow)gridView1.GetDataRow(Sel);
                retorno.Add(row.PLA, row.IsValorNull() ? 0 : row.Valor);
            }
            return retorno;
        }

        private Font fontAlt;

        private Font FontAlt(Font Modelo)
        {
            if (fontAlt == null)
                fontAlt = new Font(Modelo, FontStyle.Italic);
            return fontAlt;
        }

        private void gridView1_RowStyle(object sender, DevExpress.XtraGrid.Views.Grid.RowStyleEventArgs e)
        {
            dOrcamento.PLAnocontasRow row = (dOrcamento.PLAnocontasRow)gridView1.GetDataRow(e.RowHandle);
            if ((row != null) && (row.Ocupado))
            {
                //e.Appearance.FontSizeDelta = -1;
                e.Appearance.Font = FontAlt(e.Appearance.Font);
                e.Appearance.ForeColor = Color.Gray;
            }
        }

        private void gridView1_SelectionChanged(object sender, DevExpress.Data.SelectionChangedEventArgs e)
        {
            if (e.Action == CollectionChangeAction.Add)
            {
                dOrcamento.PLAnocontasRow row = (dOrcamento.PLAnocontasRow)gridView1.GetFocusedDataRow();
                if (row != null)
                    if (row.Ocupado)
                        gridView1.UnselectRow(gridView1.FocusedRowHandle);
            }
        }

        private void gridControl1_Load(object sender, EventArgs e)
        {
            gridView1.VisibleColumns[0].Width = 14;            
        }

        private void gridView1_FocusedRowChanged(object sender, DevExpress.XtraGrid.Views.Base.FocusedRowChangedEventArgs e)
        {
            dOrcamento.PLAnocontasRow row = (dOrcamento.PLAnocontasRow)gridView1.GetFocusedDataRow();
            if (row == null)
                return;
            gridView1.OptionsBehavior.Editable = !row.Ocupado;
        }

        

        private void cPLAS_OnClose(object sender, EventArgs e)
        {
            
        }

        private void gridControl1_Leave(object sender, EventArgs e)
        {
            
        }

        private void btnConfirmar_F_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {            
            Validate();
        }
    }
}
