﻿using System.Drawing;
using dllVirEnum;
using Balancete.GrBalancete;
using VirEnumeracoesNeon;

namespace Balancete {

    

    partial class dBalancetes
    {
        partial class BALancetesDataTable
        {
        }

        private dBalancetesTableAdapters.SaldoContaLogicaTableAdapter saldoContaLogicaTableAdapter;
        /// <summary>
        /// TableAdapter padrão para tabela: SaldoContaLogica
        /// </summary>
        public dBalancetesTableAdapters.SaldoContaLogicaTableAdapter SaldoContaLogicaTableAdapter
        {
            get
            {
                if (saldoContaLogicaTableAdapter == null)
                {
                    saldoContaLogicaTableAdapter = new dBalancetesTableAdapters.SaldoContaLogicaTableAdapter();
                    saldoContaLogicaTableAdapter.TrocarStringDeConexao();
                };
                return saldoContaLogicaTableAdapter;
            }
        }
        

        private dBalancetesTableAdapters.BALancetesTableAdapter bALancetesTableAdapter;
        /// <summary>
        /// TableAdapter padrão para tabela: BALancetes
        /// </summary>
        public dBalancetesTableAdapters.BALancetesTableAdapter BALancetesTableAdapter
        {
            get
            {
                if (bALancetesTableAdapter == null)
                {
                    bALancetesTableAdapter = new dBalancetesTableAdapters.BALancetesTableAdapter();
                    bALancetesTableAdapter.TrocarStringDeConexao();
                };
                return bALancetesTableAdapter;
            }
        }

        private dBalancetesTableAdapters.GradeBalancetesTableAdapter gradeBalancetesTableAdapter;
        /// <summary>
        /// TableAdapter padrão para tabela: GradeBalancetes
        /// </summary>
        public dBalancetesTableAdapters.GradeBalancetesTableAdapter GradeBalancetesTableAdapter
        {
            get
            {
                if (gradeBalancetesTableAdapter == null)
                {
                    gradeBalancetesTableAdapter = new dBalancetesTableAdapters.GradeBalancetesTableAdapter();
                    gradeBalancetesTableAdapter.TrocarStringDeConexao();
                };
                return gradeBalancetesTableAdapter;
            }
        }

        private dBalancetesTableAdapters.ContasTableAdapter contasTableAdapter;
        /// <summary>
        /// TableAdapter padrão para tabela: Contas
        /// </summary>
        public dBalancetesTableAdapters.ContasTableAdapter ContasTableAdapter
        {
            get
            {
                if (contasTableAdapter == null)
                {
                    contasTableAdapter = new dBalancetesTableAdapters.ContasTableAdapter();
                    contasTableAdapter.TrocarStringDeConexao();
                };
                return contasTableAdapter;
            }
        }

    }
}
