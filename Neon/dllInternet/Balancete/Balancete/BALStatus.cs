

namespace Balancete.Balancete
{
    public enum BALStatus
    {
        NaoIniciado = 0,
        Producao = 1,
        Termindo = 2,
        ParaPublicar = 3,
        Publicado = 4,
        FechadoDefinitivo = 5
    }
}
