﻿namespace Balancete.Balancete {
    
    
    public partial class dBalancete 
    {
        private dBalanceteTableAdapters.BALancetesTableAdapter bALancetesTableAdapter;
        /// <summary>
        /// TableAdapter padrão para tabela: BALancetes
        /// </summary>
        public dBalanceteTableAdapters.BALancetesTableAdapter BALancetesTableAdapter
        {
            get
            {
                if (bALancetesTableAdapter == null)
                {
                    bALancetesTableAdapter = new dBalanceteTableAdapters.BALancetesTableAdapter();
                    bALancetesTableAdapter.TrocarStringDeConexao();
                };
                return bALancetesTableAdapter;
            }
        }
    }
}
