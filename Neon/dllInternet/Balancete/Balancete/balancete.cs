﻿using System;
using System.Collections.Generic;
using System.Text;
using Framework.objetosNeon;

namespace Balancete.Balancete
{   

    public class balancete
    {
        private dBalancete dBalancete1;
        private dBalancete.BALancetesRow _BALrow = null;

        #region Propriedades
        public Competencia comp
        {
            get { return encontrado ? new Competencia(BALrow.BALCompet,CON,null) : null; }
        }

        public int CON
        {
            get { return encontrado ? BALrow.BAL_CON : -1; }
        }

        public DateTime DataI
        {
            get { return encontrado ? BALrow.BALDataInicio : DateTime.MinValue; }
        }

        public DateTime DataF
        {
            get { return encontrado ? BALrow.BALdataFim : DateTime.MinValue; }
        }

        public BALStatus status
        {
            get { return encontrado ? (BALStatus)BALrow.BALSTATUS : BALStatus.NaoIniciado; }
        }

        public dBalancete.BALancetesRow BALrow
        {
            get { return _BALrow; }
        }

        public dBalancete DBalancete1
        {
            get { return dBalancete1; }
        }

        public bool encontrado
        {
            get { return BALrow != null; }
        }

        private Framework.datasets.dCondominiosAtivos.CONDOMINIOSRow rowCON
        {
            get { return Framework.datasets.dCondominiosAtivos.dCondominiosAtivosST.CONDOMINIOS.FindByCON(CON); }
        }

        #endregion


        #region Construtores
        public balancete(int BAL)
        {
            dBalancete1 = new dBalancete();
            dBalancete1.BALancetesTableAdapter.EmbarcaEmTransST();
            if (dBalancete1.BALancetesTableAdapter.FillByBAL(dBalancete1.BALancetes, BAL) == 1)
                _BALrow = dBalancete1.BALancetes[0];

        }

        public balancete(int CON, Competencia comp)
        {
            dBalancete1 = new dBalancete();
            dBalancete1.BALancetesTableAdapter.EmbarcaEmTransST();
            if (dBalancete1.BALancetesTableAdapter.FillByComp(dBalancete1.BALancetes, CON, comp.CompetenciaBind) == 1)
                _BALrow = dBalancete1.BALancetes[0];

        }

        public balancete(dBalancete.BALancetesRow rowExterna)
        {
            _BALrow = rowExterna;
            try
            {
                dBalancete1 = (dBalancete)rowExterna.Table.DataSet;
            }
            catch { }
        } 
        #endregion

        public void Abrir(bool SomenteLeitura,CompontesBasicos.EstadosDosComponentes Pop)
        {
            if (!encontrado || (rowCON == null))
            {
                System.Windows.Forms.MessageBox.Show("Condomínio não encotrado");
                return;
            }
            cBalancete cBalancete1 = new cBalancete();            
            cBalancete1.Titulo = string.Format("{0}-{1} Balancete {2}",rowCON.IsCONCodigoFolha1Null() ? 0 : rowCON.CONCodigoFolha1,rowCON.CONCodigo,comp);
            cBalancete1.VirShowModulo(Pop);
        }

    }
}