namespace Balancete
{
    partial class ImpAcumulado
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            DevExpress.XtraReports.UI.XRSummary xrSummary1 = new DevExpress.XtraReports.UI.XRSummary();
            DevExpress.XtraReports.UI.XRSummary xrSummary2 = new DevExpress.XtraReports.UI.XRSummary();
            this.bindingdAcumulado = new System.Windows.Forms.BindingSource(this.components);
            this.GroupHeader1 = new DevExpress.XtraReports.UI.GroupHeaderBand();
            this.xrTituloModelo = new DevExpress.XtraReports.UI.XRLabel();
            this.xrtotalTitulo = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel3 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrPanel1 = new DevExpress.XtraReports.UI.XRPanel();
            this.xrLabel2 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel4 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrtotal = new DevExpress.XtraReports.UI.XRLabel();
            this.GroupFooter1 = new DevExpress.XtraReports.UI.GroupFooterBand();
            this.xrTotalModelo = new DevExpress.XtraReports.UI.XRLabel();
            this.xrtotalSoma = new DevExpress.XtraReports.UI.XRLabel();
            this.xrDetalheModelo = new DevExpress.XtraReports.UI.XRLabel();
            ((System.ComponentModel.ISupportInitialize)(this.bindingdAcumulado)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            // 
            // xrTitulo
            // 
            this.xrTitulo.Text = "Despesas e Receitas";
            // 
            // Detail
            // 
            this.Detail.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrDetalheModelo,
            this.xrtotal,
            this.xrLabel4});
            this.Detail.HeightF = 28F;
            this.Detail.KeepTogether = true;
            this.Detail.StylePriority.UseTextAlignment = false;
            // 
            // bindingdAcumulado
            // 
            this.bindingdAcumulado.DataMember = "Acumulado";
            this.bindingdAcumulado.DataSource = typeof(dAcumulado);
            // 
            // GroupHeader1
            // 
            this.GroupHeader1.BackColor = System.Drawing.Color.Transparent;
            this.GroupHeader1.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrTituloModelo,
            this.xrtotalTitulo,
            this.xrLabel3,
            this.xrPanel1});
            this.GroupHeader1.Dpi = 254F;
            this.GroupHeader1.GroupFields.AddRange(new DevExpress.XtraReports.UI.GroupField[] {
            new DevExpress.XtraReports.UI.GroupField("CredDeb", DevExpress.XtraReports.UI.XRColumnSortOrder.Ascending)});
            this.GroupHeader1.HeightF = 159F;
            this.GroupHeader1.Name = "GroupHeader1";
            this.GroupHeader1.StylePriority.UseBackColor = false;
            // 
            // xrTituloModelo
            // 
            this.xrTituloModelo.Dpi = 254F;
            this.xrTituloModelo.Font = new System.Drawing.Font("Times New Roman", 9.75F, System.Drawing.FontStyle.Bold);
            this.xrTituloModelo.LocationFloat = new DevExpress.Utils.PointFloat(428.3958F, 113.4584F);
            this.xrTituloModelo.Name = "xrTituloModelo";
            this.xrTituloModelo.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTituloModelo.SizeF = new System.Drawing.SizeF(124.9169F, 34.60751F);
            this.xrTituloModelo.StylePriority.UseFont = false;
            this.xrTituloModelo.StylePriority.UseTextAlignment = false;
            this.xrTituloModelo.Text = "00/0000";
            this.xrTituloModelo.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight;
            // 
            // xrtotalTitulo
            // 
            this.xrtotalTitulo.Dpi = 254F;
            this.xrtotalTitulo.Font = new System.Drawing.Font("Times New Roman", 9.75F, System.Drawing.FontStyle.Bold);
            this.xrtotalTitulo.LocationFloat = new DevExpress.Utils.PointFloat(1579.396F, 113.4584F);
            this.xrtotalTitulo.Name = "xrtotalTitulo";
            this.xrtotalTitulo.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrtotalTitulo.SizeF = new System.Drawing.SizeF(219.604F, 34.60751F);
            this.xrtotalTitulo.StylePriority.UseFont = false;
            this.xrtotalTitulo.StylePriority.UseTextAlignment = false;
            this.xrtotalTitulo.Text = "Total";
            this.xrtotalTitulo.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter;
            // 
            // xrLabel3
            // 
            this.xrLabel3.Dpi = 254F;
            this.xrLabel3.Font = new System.Drawing.Font("Times New Roman", 9.75F, System.Drawing.FontStyle.Bold);
            this.xrLabel3.LocationFloat = new DevExpress.Utils.PointFloat(0F, 113.4584F);
            this.xrLabel3.Name = "xrLabel3";
            this.xrLabel3.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrLabel3.SizeF = new System.Drawing.SizeF(253.9999F, 34.60751F);
            this.xrLabel3.StylePriority.UseFont = false;
            this.xrLabel3.Text = "Descri��o";
            // 
            // xrPanel1
            // 
            this.xrPanel1.BackColor = System.Drawing.Color.LightGray;
            this.xrPanel1.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrLabel2});
            this.xrPanel1.Dpi = 254F;
            this.xrPanel1.LocationFloat = new DevExpress.Utils.PointFloat(0F, 0F);
            this.xrPanel1.Name = "xrPanel1";
            this.xrPanel1.SizeF = new System.Drawing.SizeF(1799F, 87.00002F);
            this.xrPanel1.StylePriority.UseBackColor = false;
            // 
            // xrLabel2
            // 
            this.xrLabel2.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "CredDeb")});
            this.xrLabel2.Dpi = 254F;
            this.xrLabel2.Font = new System.Drawing.Font("Times New Roman", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel2.LocationFloat = new DevExpress.Utils.PointFloat(63.99997F, 14.16334F);
            this.xrLabel2.Name = "xrLabel2";
            this.xrLabel2.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrLabel2.SizeF = new System.Drawing.SizeF(420.6875F, 58.42F);
            this.xrLabel2.StylePriority.UseFont = false;
            this.xrLabel2.Text = "xrLabel2";
            // 
            // xrLabel4
            // 
            this.xrLabel4.CanGrow = false;
            this.xrLabel4.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "Titulo")});
            this.xrLabel4.Dpi = 254F;
            this.xrLabel4.Font = new System.Drawing.Font("Times New Roman", 6.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel4.LocationFloat = new DevExpress.Utils.PointFloat(0F, 0F);
            this.xrLabel4.Name = "xrLabel4";
            this.xrLabel4.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrLabel4.SizeF = new System.Drawing.SizeF(399.5208F, 28F);
            this.xrLabel4.StylePriority.UseFont = false;
            this.xrLabel4.Text = "[Titulo]";
            // 
            // xrtotal
            // 
            this.xrtotal.BackColor = System.Drawing.Color.Gainsboro;
            this.xrtotal.CanGrow = false;
            this.xrtotal.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "Total", "{0:n2}")});
            this.xrtotal.Dpi = 254F;
            this.xrtotal.Font = new System.Drawing.Font("Times New Roman", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrtotal.LocationFloat = new DevExpress.Utils.PointFloat(1579.396F, 0F);
            this.xrtotal.Name = "xrtotal";
            this.xrtotal.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrtotal.SizeF = new System.Drawing.SizeF(219.6041F, 28F);
            this.xrtotal.StylePriority.UseBackColor = false;
            this.xrtotal.StylePriority.UseFont = false;
            this.xrtotal.StylePriority.UseTextAlignment = false;
            this.xrtotal.Text = "[Total]";
            this.xrtotal.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight;
            // 
            // GroupFooter1
            // 
            this.GroupFooter1.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrTotalModelo,
            this.xrtotalSoma});
            this.GroupFooter1.Dpi = 254F;
            this.GroupFooter1.HeightF = 60F;
            this.GroupFooter1.Name = "GroupFooter1";
            // 
            // xrTotalModelo
            // 
            this.xrTotalModelo.CanGrow = false;
            this.xrTotalModelo.Dpi = 254F;
            this.xrTotalModelo.Font = new System.Drawing.Font("Times New Roman", 6.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrTotalModelo.LocationFloat = new DevExpress.Utils.PointFloat(399.5208F, 0F);
            this.xrTotalModelo.Name = "xrTotalModelo";
            this.xrTotalModelo.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTotalModelo.SizeF = new System.Drawing.SizeF(153.7918F, 27.99999F);
            this.xrTotalModelo.StylePriority.UseFont = false;
            this.xrTotalModelo.StylePriority.UseTextAlignment = false;
            xrSummary1.FormatString = "{0:n2}";
            xrSummary1.Running = DevExpress.XtraReports.UI.SummaryRunning.Group;
            this.xrTotalModelo.Summary = xrSummary1;
            this.xrTotalModelo.Text = "000.000,00";
            this.xrTotalModelo.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight;
            // 
            // xrtotalSoma
            // 
            this.xrtotalSoma.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "Total")});
            this.xrtotalSoma.Dpi = 254F;
            this.xrtotalSoma.Font = new System.Drawing.Font("Times New Roman", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrtotalSoma.LocationFloat = new DevExpress.Utils.PointFloat(1579.396F, 0F);
            this.xrtotalSoma.Name = "xrtotalSoma";
            this.xrtotalSoma.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrtotalSoma.SizeF = new System.Drawing.SizeF(219.6041F, 29.31583F);
            this.xrtotalSoma.StylePriority.UseFont = false;
            this.xrtotalSoma.StylePriority.UseTextAlignment = false;
            xrSummary2.FormatString = "{0:n2}";
            xrSummary2.Running = DevExpress.XtraReports.UI.SummaryRunning.Group;
            this.xrtotalSoma.Summary = xrSummary2;
            this.xrtotalSoma.Text = "xrtotalSoma";
            this.xrtotalSoma.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight;
            // 
            // xrDetalheModelo
            // 
            this.xrDetalheModelo.CanGrow = false;
            this.xrDetalheModelo.Dpi = 254F;
            this.xrDetalheModelo.Font = new System.Drawing.Font("Times New Roman", 6.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrDetalheModelo.LocationFloat = new DevExpress.Utils.PointFloat(399.5208F, 0F);
            this.xrDetalheModelo.Name = "xrDetalheModelo";
            this.xrDetalheModelo.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrDetalheModelo.SizeF = new System.Drawing.SizeF(153.7918F, 27.99999F);
            this.xrDetalheModelo.StylePriority.UseFont = false;
            this.xrDetalheModelo.StylePriority.UseTextAlignment = false;
            this.xrDetalheModelo.Text = "000.000,00";
            this.xrDetalheModelo.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight;
            this.xrDetalheModelo.Draw += new DevExpress.XtraReports.UI.DrawEventHandler(this.DrawDegrade);
            // 
            // ImpAcumulado
            // 
            this.Bands.AddRange(new DevExpress.XtraReports.UI.Band[] {
            this.Detail,
            this.PageHeader,
            this.GroupHeader1,
            this.GroupFooter1});
            this.DataSource = this.bindingdAcumulado;
            this.Version = "10.1";
            this.Controls.SetChildIndex(this.GroupFooter1, 0);
            this.Controls.SetChildIndex(this.GroupHeader1, 0);
            this.Controls.SetChildIndex(this.PageHeader, 0);
            this.Controls.SetChildIndex(this.Detail, 0);
            ((System.ComponentModel.ISupportInitialize)(this.bindingdAcumulado)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();

        }

        #endregion

        private DevExpress.XtraReports.UI.GroupHeaderBand GroupHeader1;
        private DevExpress.XtraReports.UI.XRLabel xrLabel2;
        private DevExpress.XtraReports.UI.XRLabel xrLabel3;
        private DevExpress.XtraReports.UI.XRPanel xrPanel1;
        private DevExpress.XtraReports.UI.XRLabel xrLabel4;
        private DevExpress.XtraReports.UI.XRLabel xrtotalTitulo;
        /// <summary>
        /// 
        /// </summary>
        public System.Windows.Forms.BindingSource bindingdAcumulado;
        private DevExpress.XtraReports.UI.GroupFooterBand GroupFooter1;
        private DevExpress.XtraReports.UI.XRLabel xrtotal;
        private DevExpress.XtraReports.UI.XRLabel xrDetalheModelo;
        private DevExpress.XtraReports.UI.XRLabel xrTituloModelo;
        private DevExpress.XtraReports.UI.XRLabel xrTotalModelo;
        private DevExpress.XtraReports.UI.XRLabel xrtotalSoma;
    }
}
