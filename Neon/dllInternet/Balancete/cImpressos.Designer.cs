namespace Balancete
{
    partial class cImpressos
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(cImpressos));
            DevExpress.XtraEditors.Controls.EditorButtonImageOptions editorButtonImageOptions1 = new DevExpress.XtraEditors.Controls.EditorButtonImageOptions();
            DevExpress.XtraEditors.Controls.EditorButtonImageOptions editorButtonImageOptions2 = new DevExpress.XtraEditors.Controls.EditorButtonImageOptions();
            DevExpress.XtraEditors.Controls.EditorButtonImageOptions editorButtonImageOptions3 = new DevExpress.XtraEditors.Controls.EditorButtonImageOptions();
            DevExpress.XtraEditors.Controls.EditorButtonImageOptions editorButtonImageOptions4 = new DevExpress.XtraEditors.Controls.EditorButtonImageOptions();
            DevExpress.XtraEditors.Controls.EditorButtonImageOptions editorButtonImageOptions5 = new DevExpress.XtraEditors.Controls.EditorButtonImageOptions();
            DevExpress.XtraEditors.Controls.EditorButtonImageOptions editorButtonImageOptions6 = new DevExpress.XtraEditors.Controls.EditorButtonImageOptions();
            this.lookupCondominio_F1 = new Framework.Lookup.LookupCondominio_F();
            this.dataI = new DevExpress.XtraEditors.DateEdit();
            this.dataF = new DevExpress.XtraEditors.DateEdit();
            this.cBotaoFrancesinha = new dllImpresso.Botoes.cBotaoImpBol();
            this.cBotaoImpBol2 = new dllImpresso.Botoes.cBotaoImpBol();
            this.cBotaoImpBol1 = new dllImpresso.Botoes.cBotaoImpBol();
            this.chdadosChe = new DevExpress.XtraEditors.CheckEdit();
            this.cBotaoImpBol4 = new dllImpresso.Botoes.cBotaoImpBol();
            this.chAdvogados = new DevExpress.XtraEditors.CheckEdit();
            this.chCancelados = new DevExpress.XtraEditors.CheckEdit();
            this.cBotaoImpBol5 = new dllImpresso.Botoes.cBotaoImpBol();
            this.radioGroup1 = new DevExpress.XtraEditors.RadioGroup();
            this.simpleButton1 = new DevExpress.XtraEditors.SimpleButton();
            this.simpleButton2 = new DevExpress.XtraEditors.SimpleButton();
            this.panelControl1 = new DevExpress.XtraEditors.PanelControl();
            this.cBotaoImpInad = new VirBotaoNeon.cBotaoImpNeon();
            this.simpleButton4 = new DevExpress.XtraEditors.SimpleButton();
            this.button3 = new System.Windows.Forms.Button();
            this.button2 = new System.Windows.Forms.Button();
            this.chLimpar = new System.Windows.Forms.CheckBox();
            this.simpleButton3 = new DevExpress.XtraEditors.SimpleButton();
            this.chDetalhado = new DevExpress.XtraEditors.CheckEdit();
            this.memoEdit1 = new DevExpress.XtraEditors.MemoEdit();
            this.gridControl1 = new DevExpress.XtraGrid.GridControl();
            this.bALancetesBindingSource = new System.Windows.Forms.BindingSource();
            this.dBalancetes = new Balancete.dBalancetes();
            this.gridView1 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colBALCompet = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colBALDataInicio = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colBALdataFim = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colBALDATAA = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colBALDATAI = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colBALA_USU = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colBALI_USU = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colBALSTATUS = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemImageComboBox1 = new DevExpress.XtraEditors.Repository.RepositoryItemImageComboBox();
            this.gridColumn1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemButtonSeta = new DevExpress.XtraEditors.Repository.RepositoryItemButtonEdit();
            this.colBALInicioProducao = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colBALFimProducao = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colBALObs = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemMemoEdit1 = new DevExpress.XtraEditors.Repository.RepositoryItemMemoEdit();
            this.gridColumn2 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemButtonEdit2 = new DevExpress.XtraEditors.Repository.RepositoryItemButtonEdit();
            this.gridColumn3 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemButtonEdit3 = new DevExpress.XtraEditors.Repository.RepositoryItemButtonEdit();
            this.coldataLimite = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colBAL = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colBALRed = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemButtonBAL = new DevExpress.XtraEditors.Repository.RepositoryItemButtonEdit();
            this.repositoryItemButtonBALRed = new DevExpress.XtraEditors.Repository.RepositoryItemButtonEdit();
            this.botPrimeiro = new DevExpress.XtraEditors.SimpleButton();
            ((System.ComponentModel.ISupportInitialize)(this.StyleController_F)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataI.Properties.CalendarTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataI.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataF.Properties.CalendarTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataF.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chdadosChe.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chAdvogados.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chCancelados.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radioGroup1.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl1)).BeginInit();
            this.panelControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.chDetalhado.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.memoEdit1.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.bALancetesBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dBalancetes)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemImageComboBox1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemButtonSeta)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoEdit1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemButtonEdit2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemButtonEdit3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemButtonBAL)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemButtonBALRed)).BeginInit();
            this.SuspendLayout();
            // 
            // ToolTipController_F
            // 
            this.ToolTipController_F.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.ToolTipController_F.Appearance.ForeColor = System.Drawing.Color.Black;
            this.ToolTipController_F.Appearance.Options.UseBackColor = true;
            this.ToolTipController_F.Appearance.Options.UseFont = true;
            this.ToolTipController_F.Appearance.Options.UseForeColor = true;
            this.ToolTipController_F.Appearance.Options.UseTextOptions = true;
            this.ToolTipController_F.Appearance.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            // 
            // StyleController_F
            // 
            this.StyleController_F.AppearanceDisabled.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this.StyleController_F.AppearanceDisabled.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this.StyleController_F.AppearanceDisabled.Options.UseBackColor = true;
            this.StyleController_F.AppearanceDisabled.Options.UseForeColor = true;
            this.StyleController_F.AppearanceFocused.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.StyleController_F.AppearanceFocused.Options.UseBackColor = true;
            // 
            // lookupCondominio_F1
            // 
            this.lookupCondominio_F1.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.lookupCondominio_F1.Appearance.Options.UseBackColor = true;
            this.lookupCondominio_F1.Autofill = true;
            this.lookupCondominio_F1.CaixaAltaGeral = true;
            this.lookupCondominio_F1.CON_sel = -1;
            this.lookupCondominio_F1.CON_selrow = null;
            this.lookupCondominio_F1.Cursor = System.Windows.Forms.Cursors.Default;
            this.lookupCondominio_F1.Doc = System.Windows.Forms.DockStyle.None;
            this.lookupCondominio_F1.Estado = CompontesBasicos.EstadosDosComponentes.Registrado;
            this.lookupCondominio_F1.LinhaMae_F = null;
            this.lookupCondominio_F1.Location = new System.Drawing.Point(15, 14);
            this.lookupCondominio_F1.LookAndFeel.Style = DevExpress.LookAndFeel.LookAndFeelStyle.Flat;
            this.lookupCondominio_F1.LookAndFeel.UseDefaultLookAndFeel = false;
            this.lookupCondominio_F1.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.lookupCondominio_F1.Name = "lookupCondominio_F1";
            this.lookupCondominio_F1.NivelCONOculto = 2;
            this.lookupCondominio_F1.Size = new System.Drawing.Size(622, 20);
            this.lookupCondominio_F1.somenteleitura = false;
            this.lookupCondominio_F1.TabIndex = 0;
            this.lookupCondominio_F1.TableAdapterPrincipal = null;
            this.lookupCondominio_F1.Titulo = null;
            this.lookupCondominio_F1.alterado += new System.EventHandler(this.dateEdit1_EditValueChanged);
            // 
            // dataI
            // 
            this.dataI.EditValue = null;
            this.dataI.Location = new System.Drawing.Point(495, 44);
            this.dataI.Name = "dataI";
            this.dataI.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.dataI.Properties.CalendarTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.dataI.Size = new System.Drawing.Size(142, 22);
            this.dataI.TabIndex = 1;
            // 
            // dataF
            // 
            this.dataF.EditValue = null;
            this.dataF.Location = new System.Drawing.Point(495, 70);
            this.dataF.Name = "dataF";
            this.dataF.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.dataF.Properties.CalendarTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.dataF.Size = new System.Drawing.Size(142, 22);
            this.dataF.TabIndex = 2;
            // 
            // cBotaoFrancesinha
            // 
            this.cBotaoFrancesinha.Enabled = false;
            this.cBotaoFrancesinha.ItemComprovante = false;
            this.cBotaoFrancesinha.Location = new System.Drawing.Point(183, 105);
            this.cBotaoFrancesinha.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.cBotaoFrancesinha.Name = "cBotaoFrancesinha";
            this.cBotaoFrancesinha.Size = new System.Drawing.Size(160, 52);
            this.cBotaoFrancesinha.TabIndex = 5;
            this.cBotaoFrancesinha.Titulo = "Francesinha";
            this.cBotaoFrancesinha.clicado += new System.EventHandler(this.cBotaoFrancesinha_clicado);
            // 
            // cBotaoImpBol2
            // 
            this.cBotaoImpBol2.Enabled = false;
            this.cBotaoImpBol2.ItemComprovante = false;
            this.cBotaoImpBol2.Location = new System.Drawing.Point(15, 105);
            this.cBotaoImpBol2.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.cBotaoImpBol2.Name = "cBotaoImpBol2";
            this.cBotaoImpBol2.Size = new System.Drawing.Size(160, 52);
            this.cBotaoImpBol2.TabIndex = 4;
            this.cBotaoImpBol2.Titulo = "Inadimpl�ncia";
            this.cBotaoImpBol2.clicado += new System.EventHandler(this.cBotaoImpBol2_clicado);
            // 
            // cBotaoImpBol1
            // 
            this.cBotaoImpBol1.Enabled = false;
            this.cBotaoImpBol1.ItemComprovante = false;
            this.cBotaoImpBol1.Location = new System.Drawing.Point(15, 47);
            this.cBotaoImpBol1.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.cBotaoImpBol1.Name = "cBotaoImpBol1";
            this.cBotaoImpBol1.Size = new System.Drawing.Size(160, 52);
            this.cBotaoImpBol1.TabIndex = 3;
            this.cBotaoImpBol1.Titulo = "Extrato";
            this.cBotaoImpBol1.clicado += new System.EventHandler(this.cBotaoImpBol1_clicado);
            // 
            // chdadosChe
            // 
            this.chdadosChe.EditValue = true;
            this.chdadosChe.Location = new System.Drawing.Point(181, 61);
            this.chdadosChe.Name = "chdadosChe";
            this.chdadosChe.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.chdadosChe.Properties.Appearance.Options.UseFont = true;
            this.chdadosChe.Properties.Caption = "Incluir dados dos cheques";
            this.chdadosChe.Size = new System.Drawing.Size(175, 17);
            this.chdadosChe.TabIndex = 6;
            // 
            // cBotaoImpBol4
            // 
            this.cBotaoImpBol4.Enabled = false;
            this.cBotaoImpBol4.ItemComprovante = false;
            this.cBotaoImpBol4.Location = new System.Drawing.Point(349, 105);
            this.cBotaoImpBol4.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.cBotaoImpBol4.Name = "cBotaoImpBol4";
            this.cBotaoImpBol4.Size = new System.Drawing.Size(160, 52);
            this.cBotaoImpBol4.TabIndex = 7;
            this.cBotaoImpBol4.Titulo = "Acordos";
            this.cBotaoImpBol4.clicado += new System.EventHandler(this.cBotaoImpBol4_clicado);
            // 
            // chAdvogados
            // 
            this.chAdvogados.Location = new System.Drawing.Point(515, 103);
            this.chAdvogados.Name = "chAdvogados";
            this.chAdvogados.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.chAdvogados.Properties.Appearance.Options.UseFont = true;
            this.chAdvogados.Properties.Caption = "Mostrar advogados";
            this.chAdvogados.Size = new System.Drawing.Size(146, 17);
            this.chAdvogados.TabIndex = 8;
            // 
            // chCancelados
            // 
            this.chCancelados.Location = new System.Drawing.Point(515, 122);
            this.chCancelados.Name = "chCancelados";
            this.chCancelados.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.chCancelados.Properties.Appearance.Options.UseFont = true;
            this.chCancelados.Properties.Caption = "Mostrar Cancelados";
            this.chCancelados.Size = new System.Drawing.Size(146, 17);
            this.chCancelados.TabIndex = 9;
            // 
            // cBotaoImpBol5
            // 
            this.cBotaoImpBol5.Enabled = false;
            this.cBotaoImpBol5.ItemComprovante = false;
            this.cBotaoImpBol5.Location = new System.Drawing.Point(15, 163);
            this.cBotaoImpBol5.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.cBotaoImpBol5.Name = "cBotaoImpBol5";
            this.cBotaoImpBol5.Size = new System.Drawing.Size(160, 52);
            this.cBotaoImpBol5.TabIndex = 10;
            this.cBotaoImpBol5.Titulo = "Boletos";
            this.cBotaoImpBol5.clicado += new System.EventHandler(this.cBotaoImpBol5_clicado);
            // 
            // radioGroup1
            // 
            this.radioGroup1.EditValue = "R";
            this.radioGroup1.Location = new System.Drawing.Point(183, 163);
            this.radioGroup1.Name = "radioGroup1";
            this.radioGroup1.Properties.Items.AddRange(new DevExpress.XtraEditors.Controls.RadioGroupItem[] {
            new DevExpress.XtraEditors.Controls.RadioGroupItem("E", "Emitidos"),
            new DevExpress.XtraEditors.Controls.RadioGroupItem("R", "Recebidos")});
            this.radioGroup1.Size = new System.Drawing.Size(128, 52);
            this.radioGroup1.TabIndex = 11;
            // 
            // simpleButton1
            // 
            this.simpleButton1.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("simpleButton1.ImageOptions.Image")));
            this.simpleButton1.Location = new System.Drawing.Point(317, 163);
            this.simpleButton1.Name = "simpleButton1";
            this.simpleButton1.Size = new System.Drawing.Size(114, 23);
            this.simpleButton1.TabIndex = 12;
            this.simpleButton1.Text = "Planilha";
            this.simpleButton1.Click += new System.EventHandler(this.simpleButton1_Click);
            // 
            // simpleButton2
            // 
            this.simpleButton2.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("simpleButton2.ImageOptions.Image")));
            this.simpleButton2.ImageOptions.Location = DevExpress.XtraEditors.ImageLocation.MiddleLeft;
            this.simpleButton2.Location = new System.Drawing.Point(317, 192);
            this.simpleButton2.Name = "simpleButton2";
            this.simpleButton2.Size = new System.Drawing.Size(114, 23);
            this.simpleButton2.TabIndex = 13;
            this.simpleButton2.Text = "s/ formata��o";
            this.simpleButton2.Click += new System.EventHandler(this.simpleButton2_Click);
            // 
            // panelControl1
            // 
            this.panelControl1.Appearance.BackColor = System.Drawing.Color.Wheat;
            this.panelControl1.Appearance.BackColor2 = System.Drawing.Color.White;
            this.panelControl1.Appearance.GradientMode = System.Drawing.Drawing2D.LinearGradientMode.ForwardDiagonal;
            this.panelControl1.Appearance.Options.UseBackColor = true;
            this.panelControl1.Controls.Add(this.cBotaoImpInad);
            this.panelControl1.Controls.Add(this.simpleButton4);
            this.panelControl1.Controls.Add(this.button3);
            this.panelControl1.Controls.Add(this.button2);
            this.panelControl1.Controls.Add(this.chLimpar);
            this.panelControl1.Controls.Add(this.simpleButton3);
            this.panelControl1.Controls.Add(this.chDetalhado);
            this.panelControl1.Controls.Add(this.memoEdit1);
            this.panelControl1.Controls.Add(this.lookupCondominio_F1);
            this.panelControl1.Controls.Add(this.simpleButton2);
            this.panelControl1.Controls.Add(this.dataI);
            this.panelControl1.Controls.Add(this.simpleButton1);
            this.panelControl1.Controls.Add(this.dataF);
            this.panelControl1.Controls.Add(this.radioGroup1);
            this.panelControl1.Controls.Add(this.cBotaoImpBol1);
            this.panelControl1.Controls.Add(this.cBotaoImpBol5);
            this.panelControl1.Controls.Add(this.cBotaoImpBol2);
            this.panelControl1.Controls.Add(this.chCancelados);
            this.panelControl1.Controls.Add(this.cBotaoFrancesinha);
            this.panelControl1.Controls.Add(this.chAdvogados);
            this.panelControl1.Controls.Add(this.chdadosChe);
            this.panelControl1.Controls.Add(this.cBotaoImpBol4);
            this.panelControl1.Dock = System.Windows.Forms.DockStyle.Top;
            this.panelControl1.Location = new System.Drawing.Point(0, 0);
            this.panelControl1.LookAndFeel.Style = DevExpress.LookAndFeel.LookAndFeelStyle.Flat;
            this.panelControl1.LookAndFeel.UseDefaultLookAndFeel = false;
            this.panelControl1.Name = "panelControl1";
            this.panelControl1.Size = new System.Drawing.Size(1424, 347);
            this.panelControl1.TabIndex = 14;
            // 
            // cBotaoImpInad
            // 
            this.cBotaoImpInad.AbrirArquivoExportado = false;
            this.cBotaoImpInad.AcaoBotao = dllBotao.Botao.imprimir;
            this.cBotaoImpInad.Appearance.BackColor = System.Drawing.SystemColors.Control;
            this.cBotaoImpInad.Appearance.Options.UseBackColor = true;
            this.cBotaoImpInad.AssuntoEmail = "Aviso";
            this.cBotaoImpInad.BotaoEmail = true;
            this.cBotaoImpInad.BotaoImprimir = true;
            this.cBotaoImpInad.BotaoPDF = true;
            this.cBotaoImpInad.BotaoTela = true;
            this.cBotaoImpInad.CorpoEmail = "Aviso";
            this.cBotaoImpInad.CreateDocAutomatico = false;
            this.cBotaoImpInad.Enabled = false;
            this.cBotaoImpInad.Impresso = null;
            this.cBotaoImpInad.Location = new System.Drawing.Point(661, 105);
            this.cBotaoImpInad.LookAndFeel.UseDefaultLookAndFeel = false;
            this.cBotaoImpInad.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.cBotaoImpInad.Name = "cBotaoImpInad";
            this.cBotaoImpInad.NaoUsarxlsX = true;
            this.cBotaoImpInad.NomeArquivoAnexo = "Aviso";
            this.cBotaoImpInad.PriComp = null;
            this.cBotaoImpInad.Size = new System.Drawing.Size(196, 52);
            this.cBotaoImpInad.TabIndex = 40;
            this.cBotaoImpInad.Titulo = "Inadimpl�ncia Unidades";
            this.cBotaoImpInad.clicado += new dllBotao.BotaoEventHandler(this.cBotaoImpInad_clicado);
            // 
            // simpleButton4
            // 
            this.simpleButton4.Location = new System.Drawing.Point(644, 14);
            this.simpleButton4.Name = "simpleButton4";
            this.simpleButton4.Size = new System.Drawing.Size(114, 20);
            this.simpleButton4.TabIndex = 21;
            this.simpleButton4.Text = "Calcular";
            this.simpleButton4.Click += new System.EventHandler(this.simpleButton4_Click);
            // 
            // button3
            // 
            this.button3.Location = new System.Drawing.Point(668, 307);
            this.button3.Name = "button3";
            this.button3.Size = new System.Drawing.Size(190, 23);
            this.button3.TabIndex = 20;
            this.button3.Text = "Publicar 12 meses";
            this.button3.UseVisualStyleBackColor = true;
            this.button3.Click += new System.EventHandler(this.button3_Click);
            // 
            // button2
            // 
            this.button2.Location = new System.Drawing.Point(668, 278);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(190, 23);
            this.button2.TabIndex = 19;
            this.button2.Text = "12 meses no hist�rico";
            this.button2.UseVisualStyleBackColor = true;
            this.button2.Click += new System.EventHandler(this.button2_Click);
            // 
            // chLimpar
            // 
            this.chLimpar.AutoSize = true;
            this.chLimpar.Location = new System.Drawing.Point(668, 251);
            this.chLimpar.Name = "chLimpar";
            this.chLimpar.Size = new System.Drawing.Size(137, 17);
            this.chLimpar.TabIndex = 18;
            this.chLimpar.Text = "Limpar antes de gravar";
            this.chLimpar.UseVisualStyleBackColor = true;
            // 
            // simpleButton3
            // 
            this.simpleButton3.Location = new System.Drawing.Point(495, 192);
            this.simpleButton3.Name = "simpleButton3";
            this.simpleButton3.Size = new System.Drawing.Size(142, 23);
            this.simpleButton3.TabIndex = 16;
            this.simpleButton3.Text = "Fechamento Def.";
            this.simpleButton3.Visible = false;
            this.simpleButton3.Click += new System.EventHandler(this.simpleButton3_Click);
            // 
            // chDetalhado
            // 
            this.chDetalhado.Location = new System.Drawing.Point(515, 141);
            this.chDetalhado.Name = "chDetalhado";
            this.chDetalhado.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.chDetalhado.Properties.Appearance.Options.UseFont = true;
            this.chDetalhado.Properties.Caption = "Detalhado";
            this.chDetalhado.Size = new System.Drawing.Size(146, 17);
            this.chDetalhado.TabIndex = 15;
            // 
            // memoEdit1
            // 
            this.memoEdit1.Location = new System.Drawing.Point(15, 221);
            this.memoEdit1.Name = "memoEdit1";
            this.memoEdit1.Properties.ReadOnly = true;
            this.memoEdit1.Size = new System.Drawing.Size(646, 120);
            this.memoEdit1.TabIndex = 14;
            // 
            // gridControl1
            // 
            this.gridControl1.DataSource = this.bALancetesBindingSource;
            this.gridControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridControl1.Location = new System.Drawing.Point(0, 347);
            this.gridControl1.MainView = this.gridView1;
            this.gridControl1.Name = "gridControl1";
            this.gridControl1.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemImageComboBox1,
            this.repositoryItemButtonSeta,
            this.repositoryItemMemoEdit1,
            this.repositoryItemButtonEdit2,
            this.repositoryItemButtonEdit3,
            this.repositoryItemButtonBAL,
            this.repositoryItemButtonBALRed});
            this.gridControl1.ShowOnlyPredefinedDetails = true;
            this.gridControl1.Size = new System.Drawing.Size(1424, 143);
            this.gridControl1.TabIndex = 15;
            this.gridControl1.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridView1});
            // 
            // bALancetesBindingSource
            // 
            this.bALancetesBindingSource.DataMember = "BALancetes";
            this.bALancetesBindingSource.DataSource = this.dBalancetes;
            // 
            // dBalancetes
            // 
            this.dBalancetes.DataSetName = "dBalancetes";
            this.dBalancetes.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // gridView1
            // 
            this.gridView1.Appearance.Empty.BackColor = System.Drawing.Color.Wheat;
            this.gridView1.Appearance.Empty.BackColor2 = System.Drawing.Color.White;
            this.gridView1.Appearance.Empty.GradientMode = System.Drawing.Drawing2D.LinearGradientMode.ForwardDiagonal;
            this.gridView1.Appearance.Empty.Options.UseBackColor = true;
            this.gridView1.Appearance.Row.BackColor = System.Drawing.Color.Silver;
            this.gridView1.Appearance.Row.BackColor2 = System.Drawing.Color.White;
            this.gridView1.Appearance.Row.GradientMode = System.Drawing.Drawing2D.LinearGradientMode.ForwardDiagonal;
            this.gridView1.Appearance.Row.Options.UseBackColor = true;
            this.gridView1.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colBALCompet,
            this.colBALDataInicio,
            this.colBALdataFim,
            this.colBALDATAA,
            this.colBALDATAI,
            this.colBALA_USU,
            this.colBALI_USU,
            this.colBALSTATUS,
            this.gridColumn1,
            this.colBALInicioProducao,
            this.colBALFimProducao,
            this.colBALObs,
            this.gridColumn2,
            this.gridColumn3,
            this.coldataLimite,
            this.colBAL,
            this.colBALRed});
            this.gridView1.CustomizationFormBounds = new System.Drawing.Rectangle(356, 431, 208, 191);
            this.gridView1.GridControl = this.gridControl1;
            this.gridView1.Name = "gridView1";
            this.gridView1.OptionsSelection.EnableAppearanceFocusedRow = false;
            this.gridView1.OptionsView.RowAutoHeight = true;
            this.gridView1.OptionsView.ShowGroupPanel = false;
            this.gridView1.SortInfo.AddRange(new DevExpress.XtraGrid.Columns.GridColumnSortInfo[] {
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colBALCompet, DevExpress.Data.ColumnSortOrder.Descending)});
            this.gridView1.RowCellStyle += new DevExpress.XtraGrid.Views.Grid.RowCellStyleEventHandler(this.gridView1_RowCellStyle);
            this.gridView1.RowStyle += new DevExpress.XtraGrid.Views.Grid.RowStyleEventHandler(this.gridView1_RowStyle);
            this.gridView1.CustomRowCellEdit += new DevExpress.XtraGrid.Views.Grid.CustomRowCellEditEventHandler(this.gridView1_CustomRowCellEdit);
            this.gridView1.CalcRowHeight += new DevExpress.XtraGrid.Views.Grid.RowHeightEventHandler(this.gridView1_CalcRowHeight);
            this.gridView1.FocusedRowChanged += new DevExpress.XtraGrid.Views.Base.FocusedRowChangedEventHandler(this.gridView1_FocusedRowChanged);
            this.gridView1.RowUpdated += new DevExpress.XtraGrid.Views.Base.RowObjectEventHandler(this.gridView1_RowUpdated);
            this.gridView1.CustomColumnDisplayText += new DevExpress.XtraGrid.Views.Base.CustomColumnDisplayTextEventHandler(this.gridView1_CustomColumnDisplayText);
            // 
            // colBALCompet
            // 
            this.colBALCompet.Caption = "Com.";
            this.colBALCompet.FieldName = "BALCompet";
            this.colBALCompet.Name = "colBALCompet";
            this.colBALCompet.OptionsColumn.FixedWidth = true;
            this.colBALCompet.OptionsColumn.ReadOnly = true;
            this.colBALCompet.Visible = true;
            this.colBALCompet.VisibleIndex = 0;
            this.colBALCompet.Width = 50;
            // 
            // colBALDataInicio
            // 
            this.colBALDataInicio.Caption = "De";
            this.colBALDataInicio.FieldName = "BALDataInicio";
            this.colBALDataInicio.Name = "colBALDataInicio";
            this.colBALDataInicio.OptionsColumn.FixedWidth = true;
            this.colBALDataInicio.Visible = true;
            this.colBALDataInicio.VisibleIndex = 1;
            this.colBALDataInicio.Width = 65;
            // 
            // colBALdataFim
            // 
            this.colBALdataFim.Caption = "At�";
            this.colBALdataFim.FieldName = "BALdataFim";
            this.colBALdataFim.Name = "colBALdataFim";
            this.colBALdataFim.OptionsColumn.FixedWidth = true;
            this.colBALdataFim.Visible = true;
            this.colBALdataFim.VisibleIndex = 2;
            this.colBALdataFim.Width = 65;
            // 
            // colBALDATAA
            // 
            this.colBALDATAA.Caption = "�ltima altera��o";
            this.colBALDATAA.FieldName = "BALDATAA";
            this.colBALDATAA.Name = "colBALDATAA";
            this.colBALDATAA.OptionsColumn.FixedWidth = true;
            this.colBALDATAA.OptionsColumn.ReadOnly = true;
            this.colBALDATAA.Width = 166;
            // 
            // colBALDATAI
            // 
            this.colBALDATAI.Caption = "Inclus�o";
            this.colBALDATAI.FieldName = "BALDATAI";
            this.colBALDATAI.Name = "colBALDATAI";
            this.colBALDATAI.OptionsColumn.FixedWidth = true;
            this.colBALDATAI.OptionsColumn.ReadOnly = true;
            this.colBALDATAI.Width = 144;
            // 
            // colBALA_USU
            // 
            this.colBALA_USU.Caption = "Alterado por";
            this.colBALA_USU.FieldName = "BALA_USU";
            this.colBALA_USU.Name = "colBALA_USU";
            this.colBALA_USU.OptionsColumn.FixedWidth = true;
            this.colBALA_USU.OptionsColumn.ReadOnly = true;
            this.colBALA_USU.Width = 162;
            // 
            // colBALI_USU
            // 
            this.colBALI_USU.Caption = "Cadastrado por";
            this.colBALI_USU.FieldName = "BALI_USU";
            this.colBALI_USU.Name = "colBALI_USU";
            this.colBALI_USU.OptionsColumn.FixedWidth = true;
            this.colBALI_USU.OptionsColumn.ReadOnly = true;
            this.colBALI_USU.Width = 158;
            // 
            // colBALSTATUS
            // 
            this.colBALSTATUS.Caption = "Status";
            this.colBALSTATUS.ColumnEdit = this.repositoryItemImageComboBox1;
            this.colBALSTATUS.FieldName = "BALSTATUS";
            this.colBALSTATUS.Name = "colBALSTATUS";
            this.colBALSTATUS.OptionsColumn.FixedWidth = true;
            this.colBALSTATUS.OptionsColumn.ReadOnly = true;
            this.colBALSTATUS.Visible = true;
            this.colBALSTATUS.VisibleIndex = 4;
            this.colBALSTATUS.Width = 98;
            // 
            // repositoryItemImageComboBox1
            // 
            this.repositoryItemImageComboBox1.AutoHeight = false;
            this.repositoryItemImageComboBox1.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemImageComboBox1.Name = "repositoryItemImageComboBox1";
            // 
            // gridColumn1
            // 
            this.gridColumn1.Caption = "BotSeta";
            this.gridColumn1.ColumnEdit = this.repositoryItemButtonSeta;
            this.gridColumn1.Fixed = DevExpress.XtraGrid.Columns.FixedStyle.Right;
            this.gridColumn1.Name = "gridColumn1";
            this.gridColumn1.OptionsColumn.AllowSize = false;
            this.gridColumn1.OptionsColumn.FixedWidth = true;
            this.gridColumn1.OptionsColumn.ShowCaption = false;
            this.gridColumn1.Visible = true;
            this.gridColumn1.VisibleIndex = 11;
            this.gridColumn1.Width = 44;
            // 
            // repositoryItemButtonSeta
            // 
            this.repositoryItemButtonSeta.AutoHeight = false;
            this.repositoryItemButtonSeta.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.SpinLeft),
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.SpinRight)});
            this.repositoryItemButtonSeta.Name = "repositoryItemButtonSeta";
            this.repositoryItemButtonSeta.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.HideTextEditor;
            this.repositoryItemButtonSeta.ButtonClick += new DevExpress.XtraEditors.Controls.ButtonPressedEventHandler(this.repositoryItemButtonEdit1_ButtonClick);
            // 
            // colBALInicioProducao
            // 
            this.colBALInicioProducao.Caption = "In�cio";
            this.colBALInicioProducao.DisplayFormat.FormatString = "dd/MM/yyyy HH:mm";
            this.colBALInicioProducao.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.colBALInicioProducao.FieldName = "BALInicioProducao";
            this.colBALInicioProducao.Name = "colBALInicioProducao";
            this.colBALInicioProducao.OptionsColumn.FixedWidth = true;
            this.colBALInicioProducao.OptionsColumn.ReadOnly = true;
            this.colBALInicioProducao.Visible = true;
            this.colBALInicioProducao.VisibleIndex = 5;
            this.colBALInicioProducao.Width = 100;
            // 
            // colBALFimProducao
            // 
            this.colBALFimProducao.Caption = "T�rmino";
            this.colBALFimProducao.DisplayFormat.FormatString = "dd/MM/yyyy HH:mm";
            this.colBALFimProducao.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.colBALFimProducao.FieldName = "BALFimProducao";
            this.colBALFimProducao.Name = "colBALFimProducao";
            this.colBALFimProducao.OptionsColumn.FixedWidth = true;
            this.colBALFimProducao.OptionsColumn.ReadOnly = true;
            this.colBALFimProducao.Visible = true;
            this.colBALFimProducao.VisibleIndex = 6;
            this.colBALFimProducao.Width = 100;
            // 
            // colBALObs
            // 
            this.colBALObs.Caption = "Obs";
            this.colBALObs.ColumnEdit = this.repositoryItemMemoEdit1;
            this.colBALObs.FieldName = "BALObs";
            this.colBALObs.Name = "colBALObs";
            this.colBALObs.OptionsColumn.ReadOnly = true;
            this.colBALObs.Visible = true;
            this.colBALObs.VisibleIndex = 7;
            this.colBALObs.Width = 265;
            // 
            // repositoryItemMemoEdit1
            // 
            this.repositoryItemMemoEdit1.Name = "repositoryItemMemoEdit1";
            // 
            // gridColumn2
            // 
            this.gridColumn2.Caption = "IMPRESSOS";
            this.gridColumn2.ColumnEdit = this.repositoryItemButtonEdit2;
            this.gridColumn2.Fixed = DevExpress.XtraGrid.Columns.FixedStyle.Right;
            this.gridColumn2.Name = "gridColumn2";
            this.gridColumn2.OptionsColumn.FixedWidth = true;
            this.gridColumn2.Visible = true;
            this.gridColumn2.VisibleIndex = 10;
            this.gridColumn2.Width = 160;
            // 
            // repositoryItemButtonEdit2
            // 
            this.repositoryItemButtonEdit2.AutoHeight = false;
            editorButtonImageOptions1.Location = DevExpress.XtraEditors.ImageLocation.MiddleRight;
            editorButtonImageOptions2.Location = DevExpress.XtraEditors.ImageLocation.MiddleRight;
            editorButtonImageOptions3.Location = DevExpress.XtraEditors.ImageLocation.MiddleRight;
            editorButtonImageOptions4.Location = DevExpress.XtraEditors.ImageLocation.MiddleRight;
            this.repositoryItemButtonEdit2.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Ellipsis, "M1", -1, true, true, false, editorButtonImageOptions1),
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Ellipsis, "M2", -1, true, true, false, editorButtonImageOptions2),
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Ellipsis, "M3", -1, true, true, false, editorButtonImageOptions3),
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Ellipsis, "M4", -1, true, true, false, editorButtonImageOptions4)});
            this.repositoryItemButtonEdit2.Name = "repositoryItemButtonEdit2";
            this.repositoryItemButtonEdit2.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.HideTextEditor;
            this.repositoryItemButtonEdit2.ButtonClick += new DevExpress.XtraEditors.Controls.ButtonPressedEventHandler(this.repositoryItemButtonEdit2_ButtonClick);
            // 
            // gridColumn3
            // 
            this.gridColumn3.Caption = "gridColumn3";
            this.gridColumn3.ColumnEdit = this.repositoryItemButtonEdit3;
            this.gridColumn3.Fixed = DevExpress.XtraGrid.Columns.FixedStyle.Right;
            this.gridColumn3.Name = "gridColumn3";
            this.gridColumn3.OptionsColumn.FixedWidth = true;
            this.gridColumn3.OptionsColumn.ShowCaption = false;
            this.gridColumn3.Visible = true;
            this.gridColumn3.VisibleIndex = 12;
            this.gridColumn3.Width = 24;
            // 
            // repositoryItemButtonEdit3
            // 
            this.repositoryItemButtonEdit3.AutoHeight = false;
            this.repositoryItemButtonEdit3.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.repositoryItemButtonEdit3.Name = "repositoryItemButtonEdit3";
            this.repositoryItemButtonEdit3.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.HideTextEditor;
            this.repositoryItemButtonEdit3.ButtonPressed += new DevExpress.XtraEditors.Controls.ButtonPressedEventHandler(this.repositoryItemButtonEdit1_ButtonClick);
            // 
            // coldataLimite
            // 
            this.coldataLimite.AppearanceCell.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
            this.coldataLimite.AppearanceCell.Options.UseFont = true;
            this.coldataLimite.Caption = "Limite";
            this.coldataLimite.FieldName = "dataLimite";
            this.coldataLimite.Name = "coldataLimite";
            this.coldataLimite.OptionsColumn.FixedWidth = true;
            this.coldataLimite.OptionsColumn.ReadOnly = true;
            this.coldataLimite.Visible = true;
            this.coldataLimite.VisibleIndex = 3;
            this.coldataLimite.Width = 68;
            // 
            // colBAL
            // 
            this.colBAL.Caption = "gridColumn4";
            this.colBAL.Fixed = DevExpress.XtraGrid.Columns.FixedStyle.Right;
            this.colBAL.Name = "colBAL";
            this.colBAL.OptionsColumn.FixedWidth = true;
            this.colBAL.OptionsColumn.ShowCaption = false;
            this.colBAL.Visible = true;
            this.colBAL.VisibleIndex = 9;
            this.colBAL.Width = 30;
            // 
            // colBALRed
            // 
            this.colBALRed.Caption = "gridColumn5";
            this.colBALRed.Fixed = DevExpress.XtraGrid.Columns.FixedStyle.Right;
            this.colBALRed.Name = "colBALRed";
            this.colBALRed.OptionsColumn.FixedWidth = true;
            this.colBALRed.OptionsColumn.ShowCaption = false;
            this.colBALRed.Visible = true;
            this.colBALRed.VisibleIndex = 8;
            this.colBALRed.Width = 30;
            // 
            // repositoryItemButtonBAL
            // 
            this.repositoryItemButtonBAL.AutoHeight = false;
            editorButtonImageOptions5.Image = global::Balancete.Properties.Resources.balanceteR;
            this.repositoryItemButtonBAL.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(editorButtonImageOptions5, DevExpress.XtraEditors.Controls.ButtonPredefines.Glyph, null)});
            this.repositoryItemButtonBAL.Name = "repositoryItemButtonBAL";
            this.repositoryItemButtonBAL.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.HideTextEditor;
            this.repositoryItemButtonBAL.ButtonClick += new DevExpress.XtraEditors.Controls.ButtonPressedEventHandler(this.MostraPDF);
            // 
            // repositoryItemButtonBALRed
            // 
            this.repositoryItemButtonBALRed.AutoHeight = false;
            editorButtonImageOptions6.Image = global::Balancete.Properties.Resources.balancete;
            this.repositoryItemButtonBALRed.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(editorButtonImageOptions6, DevExpress.XtraEditors.Controls.ButtonPredefines.Glyph, null)});
            this.repositoryItemButtonBALRed.Name = "repositoryItemButtonBALRed";
            this.repositoryItemButtonBALRed.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.HideTextEditor;
            this.repositoryItemButtonBALRed.ButtonClick += new DevExpress.XtraEditors.Controls.ButtonPressedEventHandler(this.MostraPDFRed);
            // 
            // botPrimeiro
            // 
            this.botPrimeiro.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("botPrimeiro.ImageOptions.Image")));
            this.botPrimeiro.Location = new System.Drawing.Point(15, 420);
            this.botPrimeiro.Name = "botPrimeiro";
            this.botPrimeiro.Size = new System.Drawing.Size(258, 80);
            this.botPrimeiro.TabIndex = 15;
            this.botPrimeiro.Text = "PRIMEIRO BALANCETE";
            this.botPrimeiro.Visible = false;
            this.botPrimeiro.Click += new System.EventHandler(this.botPrimeiro_Click);
            // 
            // cImpressos
            // 
            this.Controls.Add(this.botPrimeiro);
            this.Controls.Add(this.gridControl1);
            this.Controls.Add(this.panelControl1);
            this.Doc = System.Windows.Forms.DockStyle.Fill;
            this.LookAndFeel.UseDefaultLookAndFeel = false;
            this.Name = "cImpressos";
            this.Size = new System.Drawing.Size(1424, 490);
            ((System.ComponentModel.ISupportInitialize)(this.StyleController_F)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataI.Properties.CalendarTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataI.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataF.Properties.CalendarTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataF.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chdadosChe.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chAdvogados.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chCancelados.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radioGroup1.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl1)).EndInit();
            this.panelControl1.ResumeLayout(false);
            this.panelControl1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.chDetalhado.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.memoEdit1.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.bALancetesBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dBalancetes)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemImageComboBox1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemButtonSeta)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoEdit1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemButtonEdit2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemButtonEdit3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemButtonBAL)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemButtonBALRed)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private Framework.Lookup.LookupCondominio_F lookupCondominio_F1;
        private DevExpress.XtraEditors.DateEdit dataI;
        private DevExpress.XtraEditors.DateEdit dataF;
        private dllImpresso.Botoes.cBotaoImpBol cBotaoImpBol1;
        private dllImpresso.Botoes.cBotaoImpBol cBotaoImpBol2;
        private dllImpresso.Botoes.cBotaoImpBol cBotaoFrancesinha;
        private DevExpress.XtraEditors.CheckEdit chdadosChe;
        private dllImpresso.Botoes.cBotaoImpBol cBotaoImpBol4;
        private DevExpress.XtraEditors.CheckEdit chAdvogados;
        private DevExpress.XtraEditors.CheckEdit chCancelados;
        private dllImpresso.Botoes.cBotaoImpBol cBotaoImpBol5;
        private DevExpress.XtraEditors.RadioGroup radioGroup1;
        private DevExpress.XtraEditors.SimpleButton simpleButton1;
        private DevExpress.XtraEditors.SimpleButton simpleButton2;
        private DevExpress.XtraEditors.PanelControl panelControl1;
        private DevExpress.XtraGrid.GridControl gridControl1;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView1;
        private System.Windows.Forms.BindingSource bALancetesBindingSource;
        private dBalancetes dBalancetes;
        private DevExpress.XtraGrid.Columns.GridColumn colBALCompet;
        private DevExpress.XtraGrid.Columns.GridColumn colBALDataInicio;
        private DevExpress.XtraGrid.Columns.GridColumn colBALdataFim;
        private DevExpress.XtraGrid.Columns.GridColumn colBALDATAA;
        private DevExpress.XtraGrid.Columns.GridColumn colBALDATAI;
        private DevExpress.XtraGrid.Columns.GridColumn colBALA_USU;
        private DevExpress.XtraGrid.Columns.GridColumn colBALI_USU;
        private DevExpress.XtraGrid.Columns.GridColumn colBALSTATUS;
        private DevExpress.XtraEditors.Repository.RepositoryItemImageComboBox repositoryItemImageComboBox1;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn1;
        private DevExpress.XtraEditors.Repository.RepositoryItemButtonEdit repositoryItemButtonSeta;
        private DevExpress.XtraGrid.Columns.GridColumn colBALInicioProducao;
        private DevExpress.XtraGrid.Columns.GridColumn colBALFimProducao;
        private DevExpress.XtraGrid.Columns.GridColumn colBALObs;
        private DevExpress.XtraEditors.Repository.RepositoryItemMemoEdit repositoryItemMemoEdit1;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn2;
        private DevExpress.XtraEditors.Repository.RepositoryItemButtonEdit repositoryItemButtonEdit2;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn3;
        private DevExpress.XtraEditors.Repository.RepositoryItemButtonEdit repositoryItemButtonEdit3;
        private DevExpress.XtraEditors.MemoEdit memoEdit1;
        private DevExpress.XtraGrid.Columns.GridColumn coldataLimite;
        private DevExpress.XtraEditors.SimpleButton botPrimeiro;
        private DevExpress.XtraEditors.CheckEdit chDetalhado;
        private DevExpress.XtraEditors.SimpleButton simpleButton3;
        private DevExpress.XtraGrid.Columns.GridColumn colBAL;
        private DevExpress.XtraGrid.Columns.GridColumn colBALRed;
        private DevExpress.XtraEditors.Repository.RepositoryItemButtonEdit repositoryItemButtonBAL;
        private DevExpress.XtraEditors.Repository.RepositoryItemButtonEdit repositoryItemButtonBALRed;
        private System.Windows.Forms.CheckBox chLimpar;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.Button button3;
        private DevExpress.XtraEditors.SimpleButton simpleButton4;
        private VirBotaoNeon.cBotaoImpNeon cBotaoImpInad;
    }
}
