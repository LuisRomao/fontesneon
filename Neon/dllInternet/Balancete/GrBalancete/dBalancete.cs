﻿namespace Balancete.GrBalancete
{

    public partial class dBalancete
    {
        private dBalanceteTableAdapters.TributosEletronicosTableAdapter tributosEletronicosTableAdapter;
        /// <summary>
        /// TableAdapter padrão para tabela: TributosEletronicos
        /// </summary>
        public dBalanceteTableAdapters.TributosEletronicosTableAdapter TributosEletronicosTableAdapter
        {
            get
            {
                if (tributosEletronicosTableAdapter == null)
                {
                    tributosEletronicosTableAdapter = new dBalanceteTableAdapters.TributosEletronicosTableAdapter();
                    tributosEletronicosTableAdapter.TrocarStringDeConexao();
                };
                return tributosEletronicosTableAdapter;
            }
        }

        private dBalanceteTableAdapters.BoletosOriginalAcordoTableAdapter boletosOriginalAcordoTableAdapter;
        /// <summary>
        /// TableAdapter padrão para tabela: BoletosOriginalAcordo
        /// </summary>
        public dBalanceteTableAdapters.BoletosOriginalAcordoTableAdapter BoletosOriginalAcordoTableAdapter
        {
            get
            {
                if (boletosOriginalAcordoTableAdapter == null)
                {
                    boletosOriginalAcordoTableAdapter = new dBalanceteTableAdapters.BoletosOriginalAcordoTableAdapter();
                    boletosOriginalAcordoTableAdapter.TrocarStringDeConexao();
                };
                return boletosOriginalAcordoTableAdapter;
            }
        }

        private dBalanceteTableAdapters.CTLxCCTTableAdapter cTLxCCTTableAdapter;
        /// <summary>
        /// TableAdapter padrão para tabela: CTLxCCT
        /// </summary>
        public dBalanceteTableAdapters.CTLxCCTTableAdapter CTLxCCTTableAdapter
        {
            get
            {
                if (cTLxCCTTableAdapter == null)
                {
                    cTLxCCTTableAdapter = new dBalanceteTableAdapters.CTLxCCTTableAdapter();
                    cTLxCCTTableAdapter.TrocarStringDeConexao();
                };
                return cTLxCCTTableAdapter;
            }
        }

        private dBalanceteTableAdapters.OBsBalanceteTableAdapter oBsBalanceteTableAdapter;
        /// <summary>
        /// TableAdapter padrão para tabela: OBsBalancete
        /// </summary>
        public dBalanceteTableAdapters.OBsBalanceteTableAdapter OBsBalanceteTableAdapter
        {
            get
            {
                if (oBsBalanceteTableAdapter == null)
                {
                    oBsBalanceteTableAdapter = new dBalanceteTableAdapters.OBsBalanceteTableAdapter();
                    oBsBalanceteTableAdapter.TrocarStringDeConexao();
                };
                return oBsBalanceteTableAdapter;
            }
        }

        private dBalanceteTableAdapters.BalanceteLinKsTableAdapter balanceteLinKsTableAdapter;

        /// <summary>
        /// TableAdapter padrão para tabela: BalanceteLinKs
        /// </summary>
        public dBalanceteTableAdapters.BalanceteLinKsTableAdapter BalanceteLinKsTableAdapter
        {
            get
            {
                if (balanceteLinKsTableAdapter == null)
                {
                    balanceteLinKsTableAdapter = new dBalanceteTableAdapters.BalanceteLinKsTableAdapter();
                    balanceteLinKsTableAdapter.TrocarStringDeConexao();
                };
                return balanceteLinKsTableAdapter;
            }
        }

        private dBalanceteTableAdapters.BalanceteLinkDetalheTableAdapter balanceteLinkDetalheTableAdapter;

        /// <summary>
        /// TableAdapter padrão para tabela: BalanceteLinkDetalhe
        /// </summary>
        public dBalanceteTableAdapters.BalanceteLinkDetalheTableAdapter BalanceteLinkDetalheTableAdapter
        {
            get
            {
                if (balanceteLinkDetalheTableAdapter == null)
                {
                    balanceteLinkDetalheTableAdapter = new dBalanceteTableAdapters.BalanceteLinkDetalheTableAdapter();
                    balanceteLinkDetalheTableAdapter.TrocarStringDeConexao();
                };
                return balanceteLinkDetalheTableAdapter;
            }
        }

        private dBalanceteTableAdapters.ContaCorrenteSubdetalheTableAdapter contaCorrenteSubdetalheTableAdapter;

        /// <summary>
        /// TableAdapter padrão para tabela: ContaCorrenteSubdetalhe
        /// </summary>
        public dBalanceteTableAdapters.ContaCorrenteSubdetalheTableAdapter ContaCorrenteSubdetalheTableAdapter
        {
            get
            {
                if (contaCorrenteSubdetalheTableAdapter == null)
                {
                    contaCorrenteSubdetalheTableAdapter = new dBalanceteTableAdapters.ContaCorrenteSubdetalheTableAdapter();
                    contaCorrenteSubdetalheTableAdapter.TrocarStringDeConexao();
                };
                return contaCorrenteSubdetalheTableAdapter;
            }
        }

        private dBalanceteTableAdapters.BALancetesTableAdapter bALancetesTableAdapter;

        /// <summary>
        /// TableAdapter padrão para tabela: BALancetes
        /// </summary>
        public dBalanceteTableAdapters.BALancetesTableAdapter BALancetesTableAdapter
        {
            get
            {
                if (bALancetesTableAdapter == null)
                {
                    bALancetesTableAdapter = new dBalanceteTableAdapters.BALancetesTableAdapter();
                    bALancetesTableAdapter.TrocarStringDeConexao();
                };
                return bALancetesTableAdapter;
            }
        }

        private dBalanceteTableAdapters.BOletoDetalheBODsTableAdapter bOletoDetalheBODsTableAdapter;
        /// <summary>
        /// TableAdapter padrão para tabela: BOletoDetalheBODs
        /// </summary>
        public dBalanceteTableAdapters.BOletoDetalheBODsTableAdapter BOletoDetalheBODsTableAdapter
        {
            get
            {
                if (bOletoDetalheBODsTableAdapter == null)
                {
                    bOletoDetalheBODsTableAdapter = new dBalanceteTableAdapters.BOletoDetalheBODsTableAdapter();
                    bOletoDetalheBODsTableAdapter.TrocarStringDeConexao();
                };
                return bOletoDetalheBODsTableAdapter;
            }
        }

        private dBalanceteTableAdapters.ContaCorrenTeTableAdapter contaCorrenTeTableAdapter;
        /// <summary>
        /// TableAdapter padrão para tabela: ContaCorrenTe
        /// </summary>
        public dBalanceteTableAdapters.ContaCorrenTeTableAdapter ContaCorrenTeTableAdapter
        {
            get
            {
                if (contaCorrenTeTableAdapter == null)
                {
                    contaCorrenTeTableAdapter = new dBalanceteTableAdapters.ContaCorrenTeTableAdapter();
                    contaCorrenTeTableAdapter.TrocarStringDeConexao();
                };
                return contaCorrenTeTableAdapter;
            }
        }

        private dBalanceteTableAdapters.ConTasLogicasTableAdapter conTasLogicasTableAdapter;
        /// <summary>
        /// TableAdapter padrão para tabela: ConTasLogicas
        /// </summary>
        public dBalanceteTableAdapters.ConTasLogicasTableAdapter ConTasLogicasTableAdapter
        {
            get
            {
                if (conTasLogicasTableAdapter == null)
                {
                    conTasLogicasTableAdapter = new dBalanceteTableAdapters.ConTasLogicasTableAdapter();
                    conTasLogicasTableAdapter.TrocarStringDeConexao();
                };
                return conTasLogicasTableAdapter;
            }
        }

        private dBalanceteTableAdapters.SaldoContaCorrenteTableAdapter saldoContaCorrenteTableAdapter;
        /// <summary>
        /// TableAdapter padrão para tabela: SaldoContaCorrente
        /// </summary>
        public dBalanceteTableAdapters.SaldoContaCorrenteTableAdapter SaldoContaCorrenteTableAdapter
        {
            get
            {
                if (saldoContaCorrenteTableAdapter == null)
                {
                    saldoContaCorrenteTableAdapter = new dBalanceteTableAdapters.SaldoContaCorrenteTableAdapter();
                    saldoContaCorrenteTableAdapter.TrocarStringDeConexao();
                };
                return saldoContaCorrenteTableAdapter;
            }
        }

        private dBalanceteTableAdapters.ContaCorrenteDetalheTableAdapter contaCorrenteDetalheTableAdapter;
        /// <summary>
        /// TableAdapter padrão para tabela: ContaCorrenteDetalhe
        /// </summary>
        public dBalanceteTableAdapters.ContaCorrenteDetalheTableAdapter ContaCorrenteDetalheTableAdapter
        {
            get
            {
                if (contaCorrenteDetalheTableAdapter == null)
                {
                    contaCorrenteDetalheTableAdapter = new dBalanceteTableAdapters.ContaCorrenteDetalheTableAdapter();
                    contaCorrenteDetalheTableAdapter.TrocarStringDeConexao();
                };
                return contaCorrenteDetalheTableAdapter;
            }
        }



        private dBalanceteTableAdapters.BOletoDetalheTableAdapter bOletoDetalheTableAdapter;
        /// <summary>
        /// TableAdapter padrão para tabela: BOletoDetalhe
        /// </summary>
        public dBalanceteTableAdapters.BOletoDetalheTableAdapter BOletoDetalheTableAdapter
        {
            get
            {
                if (bOletoDetalheTableAdapter == null)
                {
                    bOletoDetalheTableAdapter = new dBalanceteTableAdapters.BOletoDetalheTableAdapter();
                    bOletoDetalheTableAdapter.TrocarStringDeConexao();
                };
                return bOletoDetalheTableAdapter;
            }
        }

        private dBalanceteTableAdapters.SaldoContaLogicaTableAdapter saldoContaLogicaTableAdapter;
        /// <summary>
        /// TableAdapter padrão para tabela: SaldoContaLogica
        /// </summary>
        public dBalanceteTableAdapters.SaldoContaLogicaTableAdapter SaldoContaLogicaTableAdapter
        {
            get
            {
                if (saldoContaLogicaTableAdapter == null)
                {
                    saldoContaLogicaTableAdapter = new dBalanceteTableAdapters.SaldoContaLogicaTableAdapter();
                    saldoContaLogicaTableAdapter.TrocarStringDeConexao();
                };
                return saldoContaLogicaTableAdapter;
            }
        }

        private dBalanceteTableAdapters.PAGamentosTableAdapter pAGamentosTableAdapter;

        /// <summary>
        /// TableAdapter padrão para tabela: PAGamentos
        /// </summary>
        public dBalanceteTableAdapters.PAGamentosTableAdapter PAGamentosTableAdapter
        {
            get
            {
                if (pAGamentosTableAdapter == null)
                {
                    pAGamentosTableAdapter = new dBalanceteTableAdapters.PAGamentosTableAdapter();
                    pAGamentosTableAdapter.TrocarStringDeConexao();
                };
                return pAGamentosTableAdapter;
            }
        }

        private dBalanceteTableAdapters.BOLetosRecebidosTableAdapter bOLetosRecebidosTableAdapter;

        /// <summary>
        /// TableAdapter padrão para tabela: BOLetosRecebidos
        /// </summary>
        public dBalanceteTableAdapters.BOLetosRecebidosTableAdapter BOLetosRecebidosTableAdapter
        {
            get
            {
                if (bOLetosRecebidosTableAdapter == null)
                {
                    bOLetosRecebidosTableAdapter = new dBalanceteTableAdapters.BOLetosRecebidosTableAdapter();
                    bOLetosRecebidosTableAdapter.TrocarStringDeConexao();
                };
                return bOLetosRecebidosTableAdapter;
            }
        }

        /// <summary>
        /// Conta lógica CAIXA
        /// </summary>
        public int CTLCAIXA;

        /// <summary>
        /// Conta lógica FUNDO
        /// </summary>
        public int CTLFUNDO;

        private int? _CTLCREDITOS_Anteriores;

        /// <summary>
        /// Conta lógica Créditos anteriores
        /// </summary>
        public int CTLCREDITOS_Anteriores
        {
            get
            {
                if (!_CTLCREDITOS_Anteriores.HasValue)
                {
                    int? CTL = ConTasLogicasTableAdapter.BuscaEscalar_int("SELECT CTL FROM ConTasLogicas WHERE (CTL_CON = @P1) AND (CTLTipo = @P2)", CON, (int)Framework.CTLTipo.CreditosAnteriores);
                    if (CTL.HasValue)
                        _CTLCREDITOS_Anteriores = CTL.Value;
                    else
                    {
                        //int? CCT = ConTasLogicasTableAdapter.BuscaEscalar_int("SELECT CCT FROM ContaCorrenTe WHERE (CCT_CON = @P1) AND (CCTAplicacao = 0) AND (CCTAtiva = 1) ORDER BY CCTPrincipal DESC", CON);
                        dBalancete.ConTasLogicasRow rowCTL = ConTasLogicas.NewConTasLogicasRow();
                        //rowCTL.CTL_CCT = CCT.Value;
                        rowCTL.CTL_CON = CON;
                        rowCTL.CTLAtiva = true;
                        rowCTL.CTLTipo = (int)Framework.CTLTipo.CreditosAnteriores;
                        rowCTL.CTLTitulo = "Créditos não identificados/Antecipados";
                        rowCTL.SALDOI = 0;
                        rowCTL.SALDOF = 0;
                        ConTasLogicas.AddConTasLogicasRow(rowCTL);
                        ConTasLogicasTableAdapter.Update(rowCTL);
                        rowCTL.AcceptChanges();
                        _CTLCREDITOS_Anteriores = rowCTL.CTL;
                    }
                }
                return _CTLCREDITOS_Anteriores.Value;
            }
        }


        internal int CON;

        /// <summary>
        /// CarregaContasLogicas
        /// </summary>
        /// <param name="CON"></param>
        public void CarregaContasLogicas(int CON)
        {
            this.CON = CON;
            ConTasLogicasTableAdapter.Fill(ConTasLogicas, CON);
            foreach (dBalancete.ConTasLogicasRow rowCTL in ConTasLogicas)
            {
                switch ((Framework.CTLTipo)rowCTL.CTLTipo)
                {
                    case Framework.CTLTipo.Caixa:
                        CTLCAIXA = rowCTL.CTL;
                        break;
                    case Framework.CTLTipo.Fundo:
                        CTLFUNDO = rowCTL.CTL;
                        break;
                    case Framework.CTLTipo.CreditosAnteriores:
                        _CTLCREDITOS_Anteriores = rowCTL.CTL;
                        break;
                }
            }
        }

    }
}
