﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using CompontesBasicosProc;
using CompontesBasicos;

namespace Balancete.GrBalancete
{
    /// <summary>
    /// 
    /// </summary>
    public partial class cCreditoCobManual : ComponenteBaseDialog
    {
        private dBalancete dBalancete1;
        private dBalancete.TabelaVirtualRow row;

        /// <summary>
        /// 
        /// </summary>
        public List<dBalancete.BOLetosRecebidosRow> selecionados;

        /// <summary>
        /// 
        /// </summary>
        /// <param name="_dBalancete1"></param>
        /// <param name="_row"></param>
        public cCreditoCobManual(dBalancete _dBalancete1, dBalancete.TabelaVirtualRow _row)
            : this()
        {            
            dBalancete1 = _dBalancete1;
            row = _row;
            dBalanceteBindingSource.DataSource = dBalancete1;
            if (((balancete.TiposLinhas)row.Tipo).EstaNoGrupo(balancete.TiposLinhas.DetalheBOLManual, balancete.TiposLinhas.despesanoboletoManual))
            {
                BotoesOKCancel(false);
                somenteleitura = true;
                dBalanceteBindingSource.Filter = string.Format("[BOL_CCD] = {0}",row.CCD);
                labelTotal.Visible = textAcumulado.Visible = labelErroT.Visible = textErro.Visible = false;
                BotCancelar.Visible = true;
            }
            else
            {
                dBalanceteBindingSource.Filter = "([BOL_CCD] is Null) and ([BOL] > 0) OR (Erro is not Null)";
                
            }
            textMeta.EditValue = textErro.EditValue = row.CCDValor.ToString("n2");
            dateEditCredito.DateTime = row.Data;
            btnConfirmar_F.Enabled = false;
        }

        /// <summary>
        /// 
        /// </summary>
        public cCreditoCobManual()
        {
            InitializeComponent();
        }

        private void gridView1_SelectionChanged(object sender, DevExpress.Data.SelectionChangedEventArgs e)
        {
            decimal Total = 0;
            if (selecionados == null)
                selecionados = new List<dBalancete.BOLetosRecebidosRow>();
            else
                selecionados.Clear();            
            foreach (int i in gridView1.GetSelectedRows())
            {
                if (i < 0)
                    continue;
                dBalancete.BOLetosRecebidosRow rowSel = (dBalancete.BOLetosRecebidosRow)gridView1.GetDataRow(i);
                Total += rowSel.BOLValorPago;
                selecionados.Add(rowSel);
            }
            textAcumulado.EditValue = Total.ToString("n2");
            textErro.EditValue = (row.CCDValor - Total).ToString("n2");
            btnConfirmar_F.Enabled = Total == row.CCDValor;            
        }        

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public override bool CanClose()
        {
            foreach (dBalancete.BOLetosRecebidosRow rowTestar in selecionados)
                if (!rowTestar.IsErroNull())
                    if (MessageBox.Show("Um dos boletos selecionado já está associado a outro crédito.\r\nConfirma a alteração", "ATENÇÃO", MessageBoxButtons.YesNo, MessageBoxIcon.Warning, MessageBoxDefaultButton.Button2) == DialogResult.No)
                        return false;
                    else
                        break;
            return base.CanClose();
        }

        private void gridView1_RowCellStyle(object sender, DevExpress.XtraGrid.Views.Grid.RowCellStyleEventArgs e)
        {
            if (e.RowHandle < 0)
                return;
            dBalancete.BOLetosRecebidosRow rowB = (dBalancete.BOLetosRecebidosRow)gridView1.GetDataRow(e.RowHandle);
            if (rowB == null)
                return;
            if (rowB.BOLPagamento == row.Data)
                e.Appearance.ForeColor = Color.Red;            
        }

        private void BotCancelar_Click(object sender, EventArgs e)
        {
            if (MessageBox.Show("Confima o cancelamento ?", "CONFIEMAÇÃO", MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button2) == DialogResult.Yes)
            {
                try
                {
                    VirMSSQL.TableAdapter.AbreTrasacaoSQL("cCreditoCobManual.cs BotCancelar_Click 96");
                    if (VirMSSQL.TableAdapter.STTableAdapter.ExecutarSQLNonQuery("update contacorrentedetalhe set CCDTipoLancamento = 0, CCDConsolidado = 1 where ccd = @P1 and CCDTipoLancamento = 17", row.CCD) == 0)
                        throw new Exception("Concorrência");
                    VirMSSQL.TableAdapter.STTableAdapter.ExecutarSQLNonQuery("update boletos set bol_ccd = Null where bol_CCD = @P1", row.CCD);
                    VirMSSQL.TableAdapter.CommitSQL();
                }
                catch (Exception ex)
                {
                    VirMSSQL.TableAdapter.VircatchSQL(ex);
                    throw new Exception("Concorrência",ex);
                }
            }
            FechaTela(DialogResult.OK);
        }

    }
}
