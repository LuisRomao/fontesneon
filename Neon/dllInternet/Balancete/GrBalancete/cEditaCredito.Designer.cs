﻿namespace Balancete.GrBalancete
{
    partial class cEditaCredito
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(cEditaCredito));
            DevExpress.XtraGrid.GridLevelNode gridLevelNode1 = new DevExpress.XtraGrid.GridLevelNode();
            DevExpress.XtraEditors.Controls.EditorButtonImageOptions editorButtonImageOptions1 = new DevExpress.XtraEditors.Controls.EditorButtonImageOptions();
            this.gridView2 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colBOD = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colBOD_PLA = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colBODMensagem = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colBODValor = new DevExpress.XtraGrid.Columns.GridColumn();
            this.CalcEditX = new DevExpress.XtraEditors.Repository.RepositoryItemCalcEdit();
            this.gridControl1 = new DevExpress.XtraGrid.GridControl();
            this.dEditaCreditoBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.dEditaCredito = new Balancete.GrBalancete.dEditaCredito();
            this.gridView1 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colARL_BOL = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colARLValor = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colARLStatusN = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemImageComboBox1 = new DevExpress.XtraEditors.Repository.RepositoryItemImageComboBox();
            this.colBOLTipoCRAI = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colBOLValorPago = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colBOLStatus = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemImageComboBox2 = new DevExpress.XtraEditors.Repository.RepositoryItemImageComboBox();
            this.colARL = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colBOL = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colBOLPagamento = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colBotBotao = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colARQIData = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemButtonEditBOL = new DevExpress.XtraEditors.Repository.RepositoryItemButtonEdit();
            this.panelControl1 = new DevExpress.XtraEditors.PanelControl();
            this.BotCancelar = new DevExpress.XtraEditors.SimpleButton();
            ((System.ComponentModel.ISupportInitialize)(this.BarManager_F)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.StyleController_F)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.CalcEditX)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dEditaCreditoBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dEditaCredito)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemImageComboBox1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemImageComboBox2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemButtonEditBOL)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl1)).BeginInit();
            this.panelControl1.SuspendLayout();
            this.SuspendLayout();
            // 
            // barComponente_F
            // 
            this.barComponente_F.OptionsBar.AllowQuickCustomization = false;
            this.barComponente_F.OptionsBar.DisableClose = true;
            this.barComponente_F.OptionsBar.DisableCustomization = true;
            this.barComponente_F.OptionsBar.DrawDragBorder = false;
            this.barComponente_F.OptionsBar.UseWholeRow = true;
            // 
            // btnConfirmar_F
            // 
            this.btnConfirmar_F.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("btnConfirmar_F.ImageOptions.Image")));
            // 
            // btnCancelar_F
            // 
            this.btnCancelar_F.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("btnCancelar_F.ImageOptions.Image")));
            // 
            // btnFechar_F
            // 
            this.btnFechar_F.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("btnFechar_F.ImageOptions.Image")));
            // 
            // ToolTipController_F
            // 
            this.ToolTipController_F.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.ToolTipController_F.Appearance.ForeColor = System.Drawing.Color.Black;
            this.ToolTipController_F.Appearance.Options.UseBackColor = true;
            this.ToolTipController_F.Appearance.Options.UseFont = true;
            this.ToolTipController_F.Appearance.Options.UseForeColor = true;
            this.ToolTipController_F.Appearance.Options.UseTextOptions = true;
            this.ToolTipController_F.Appearance.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            // 
            // StyleController_F
            // 
            this.StyleController_F.AppearanceDisabled.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this.StyleController_F.AppearanceDisabled.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this.StyleController_F.AppearanceDisabled.Options.UseBackColor = true;
            this.StyleController_F.AppearanceDisabled.Options.UseForeColor = true;
            this.StyleController_F.AppearanceFocused.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.StyleController_F.AppearanceFocused.Options.UseBackColor = true;
            // 
            // gridView2
            // 
            this.gridView2.Appearance.ColumnFilterButton.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(221)))), ((int)(((byte)(236)))), ((int)(((byte)(254)))));
            this.gridView2.Appearance.ColumnFilterButton.BackColor2 = System.Drawing.Color.FromArgb(((int)(((byte)(132)))), ((int)(((byte)(171)))), ((int)(((byte)(228)))));
            this.gridView2.Appearance.ColumnFilterButton.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(221)))), ((int)(((byte)(236)))), ((int)(((byte)(254)))));
            this.gridView2.Appearance.ColumnFilterButton.ForeColor = System.Drawing.Color.Black;
            this.gridView2.Appearance.ColumnFilterButton.GradientMode = System.Drawing.Drawing2D.LinearGradientMode.Vertical;
            this.gridView2.Appearance.ColumnFilterButton.Options.UseBackColor = true;
            this.gridView2.Appearance.ColumnFilterButton.Options.UseBorderColor = true;
            this.gridView2.Appearance.ColumnFilterButton.Options.UseForeColor = true;
            this.gridView2.Appearance.ColumnFilterButtonActive.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(247)))), ((int)(((byte)(251)))), ((int)(((byte)(255)))));
            this.gridView2.Appearance.ColumnFilterButtonActive.BackColor2 = System.Drawing.Color.FromArgb(((int)(((byte)(154)))), ((int)(((byte)(190)))), ((int)(((byte)(243)))));
            this.gridView2.Appearance.ColumnFilterButtonActive.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(247)))), ((int)(((byte)(251)))), ((int)(((byte)(255)))));
            this.gridView2.Appearance.ColumnFilterButtonActive.ForeColor = System.Drawing.Color.Black;
            this.gridView2.Appearance.ColumnFilterButtonActive.GradientMode = System.Drawing.Drawing2D.LinearGradientMode.Vertical;
            this.gridView2.Appearance.ColumnFilterButtonActive.Options.UseBackColor = true;
            this.gridView2.Appearance.ColumnFilterButtonActive.Options.UseBorderColor = true;
            this.gridView2.Appearance.ColumnFilterButtonActive.Options.UseForeColor = true;
            this.gridView2.Appearance.Empty.BackColor = System.Drawing.Color.White;
            this.gridView2.Appearance.Empty.Options.UseBackColor = true;
            this.gridView2.Appearance.EvenRow.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(231)))), ((int)(((byte)(242)))), ((int)(((byte)(254)))));
            this.gridView2.Appearance.EvenRow.ForeColor = System.Drawing.Color.Black;
            this.gridView2.Appearance.EvenRow.Options.UseBackColor = true;
            this.gridView2.Appearance.EvenRow.Options.UseForeColor = true;
            this.gridView2.Appearance.FilterCloseButton.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(221)))), ((int)(((byte)(236)))), ((int)(((byte)(254)))));
            this.gridView2.Appearance.FilterCloseButton.BackColor2 = System.Drawing.Color.FromArgb(((int)(((byte)(132)))), ((int)(((byte)(171)))), ((int)(((byte)(228)))));
            this.gridView2.Appearance.FilterCloseButton.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(221)))), ((int)(((byte)(236)))), ((int)(((byte)(254)))));
            this.gridView2.Appearance.FilterCloseButton.ForeColor = System.Drawing.Color.Black;
            this.gridView2.Appearance.FilterCloseButton.GradientMode = System.Drawing.Drawing2D.LinearGradientMode.Vertical;
            this.gridView2.Appearance.FilterCloseButton.Options.UseBackColor = true;
            this.gridView2.Appearance.FilterCloseButton.Options.UseBorderColor = true;
            this.gridView2.Appearance.FilterCloseButton.Options.UseForeColor = true;
            this.gridView2.Appearance.FilterPanel.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(62)))), ((int)(((byte)(109)))), ((int)(((byte)(185)))));
            this.gridView2.Appearance.FilterPanel.ForeColor = System.Drawing.Color.White;
            this.gridView2.Appearance.FilterPanel.Options.UseBackColor = true;
            this.gridView2.Appearance.FilterPanel.Options.UseForeColor = true;
            this.gridView2.Appearance.FixedLine.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(59)))), ((int)(((byte)(97)))), ((int)(((byte)(156)))));
            this.gridView2.Appearance.FixedLine.Options.UseBackColor = true;
            this.gridView2.Appearance.FocusedCell.BackColor = System.Drawing.Color.White;
            this.gridView2.Appearance.FocusedCell.ForeColor = System.Drawing.Color.Black;
            this.gridView2.Appearance.FocusedCell.Options.UseBackColor = true;
            this.gridView2.Appearance.FocusedCell.Options.UseForeColor = true;
            this.gridView2.Appearance.FocusedRow.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(49)))), ((int)(((byte)(106)))), ((int)(((byte)(197)))));
            this.gridView2.Appearance.FocusedRow.ForeColor = System.Drawing.Color.White;
            this.gridView2.Appearance.FocusedRow.Options.UseBackColor = true;
            this.gridView2.Appearance.FocusedRow.Options.UseForeColor = true;
            this.gridView2.Appearance.FooterPanel.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(221)))), ((int)(((byte)(236)))), ((int)(((byte)(254)))));
            this.gridView2.Appearance.FooterPanel.BackColor2 = System.Drawing.Color.FromArgb(((int)(((byte)(132)))), ((int)(((byte)(171)))), ((int)(((byte)(228)))));
            this.gridView2.Appearance.FooterPanel.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(221)))), ((int)(((byte)(236)))), ((int)(((byte)(254)))));
            this.gridView2.Appearance.FooterPanel.ForeColor = System.Drawing.Color.Black;
            this.gridView2.Appearance.FooterPanel.GradientMode = System.Drawing.Drawing2D.LinearGradientMode.Vertical;
            this.gridView2.Appearance.FooterPanel.Options.UseBackColor = true;
            this.gridView2.Appearance.FooterPanel.Options.UseBorderColor = true;
            this.gridView2.Appearance.FooterPanel.Options.UseForeColor = true;
            this.gridView2.Appearance.GroupButton.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(193)))), ((int)(((byte)(216)))), ((int)(((byte)(247)))));
            this.gridView2.Appearance.GroupButton.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(193)))), ((int)(((byte)(216)))), ((int)(((byte)(247)))));
            this.gridView2.Appearance.GroupButton.ForeColor = System.Drawing.Color.Black;
            this.gridView2.Appearance.GroupButton.Options.UseBackColor = true;
            this.gridView2.Appearance.GroupButton.Options.UseBorderColor = true;
            this.gridView2.Appearance.GroupButton.Options.UseForeColor = true;
            this.gridView2.Appearance.GroupFooter.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(193)))), ((int)(((byte)(216)))), ((int)(((byte)(247)))));
            this.gridView2.Appearance.GroupFooter.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(193)))), ((int)(((byte)(216)))), ((int)(((byte)(247)))));
            this.gridView2.Appearance.GroupFooter.ForeColor = System.Drawing.Color.Black;
            this.gridView2.Appearance.GroupFooter.Options.UseBackColor = true;
            this.gridView2.Appearance.GroupFooter.Options.UseBorderColor = true;
            this.gridView2.Appearance.GroupFooter.Options.UseForeColor = true;
            this.gridView2.Appearance.GroupPanel.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(62)))), ((int)(((byte)(109)))), ((int)(((byte)(185)))));
            this.gridView2.Appearance.GroupPanel.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(221)))), ((int)(((byte)(236)))), ((int)(((byte)(254)))));
            this.gridView2.Appearance.GroupPanel.Options.UseBackColor = true;
            this.gridView2.Appearance.GroupPanel.Options.UseForeColor = true;
            this.gridView2.Appearance.GroupRow.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(193)))), ((int)(((byte)(216)))), ((int)(((byte)(247)))));
            this.gridView2.Appearance.GroupRow.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(193)))), ((int)(((byte)(216)))), ((int)(((byte)(247)))));
            this.gridView2.Appearance.GroupRow.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Bold);
            this.gridView2.Appearance.GroupRow.ForeColor = System.Drawing.Color.Black;
            this.gridView2.Appearance.GroupRow.Options.UseBackColor = true;
            this.gridView2.Appearance.GroupRow.Options.UseBorderColor = true;
            this.gridView2.Appearance.GroupRow.Options.UseFont = true;
            this.gridView2.Appearance.GroupRow.Options.UseForeColor = true;
            this.gridView2.Appearance.HeaderPanel.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(221)))), ((int)(((byte)(236)))), ((int)(((byte)(254)))));
            this.gridView2.Appearance.HeaderPanel.BackColor2 = System.Drawing.Color.FromArgb(((int)(((byte)(132)))), ((int)(((byte)(171)))), ((int)(((byte)(228)))));
            this.gridView2.Appearance.HeaderPanel.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(221)))), ((int)(((byte)(236)))), ((int)(((byte)(254)))));
            this.gridView2.Appearance.HeaderPanel.ForeColor = System.Drawing.Color.Black;
            this.gridView2.Appearance.HeaderPanel.GradientMode = System.Drawing.Drawing2D.LinearGradientMode.Vertical;
            this.gridView2.Appearance.HeaderPanel.Options.UseBackColor = true;
            this.gridView2.Appearance.HeaderPanel.Options.UseBorderColor = true;
            this.gridView2.Appearance.HeaderPanel.Options.UseForeColor = true;
            this.gridView2.Appearance.HideSelectionRow.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(106)))), ((int)(((byte)(153)))), ((int)(((byte)(228)))));
            this.gridView2.Appearance.HideSelectionRow.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(208)))), ((int)(((byte)(224)))), ((int)(((byte)(251)))));
            this.gridView2.Appearance.HideSelectionRow.Options.UseBackColor = true;
            this.gridView2.Appearance.HideSelectionRow.Options.UseForeColor = true;
            this.gridView2.Appearance.HorzLine.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(99)))), ((int)(((byte)(127)))), ((int)(((byte)(196)))));
            this.gridView2.Appearance.HorzLine.Options.UseBackColor = true;
            this.gridView2.Appearance.OddRow.BackColor = System.Drawing.Color.White;
            this.gridView2.Appearance.OddRow.ForeColor = System.Drawing.Color.Black;
            this.gridView2.Appearance.OddRow.Options.UseBackColor = true;
            this.gridView2.Appearance.OddRow.Options.UseForeColor = true;
            this.gridView2.Appearance.Preview.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(249)))), ((int)(((byte)(252)))), ((int)(((byte)(255)))));
            this.gridView2.Appearance.Preview.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(88)))), ((int)(((byte)(129)))), ((int)(((byte)(185)))));
            this.gridView2.Appearance.Preview.Options.UseBackColor = true;
            this.gridView2.Appearance.Preview.Options.UseForeColor = true;
            this.gridView2.Appearance.Row.BackColor = System.Drawing.Color.White;
            this.gridView2.Appearance.Row.ForeColor = System.Drawing.Color.Black;
            this.gridView2.Appearance.Row.Options.UseBackColor = true;
            this.gridView2.Appearance.Row.Options.UseForeColor = true;
            this.gridView2.Appearance.RowSeparator.BackColor = System.Drawing.Color.White;
            this.gridView2.Appearance.RowSeparator.Options.UseBackColor = true;
            this.gridView2.Appearance.SelectedRow.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(69)))), ((int)(((byte)(126)))), ((int)(((byte)(217)))));
            this.gridView2.Appearance.SelectedRow.ForeColor = System.Drawing.Color.White;
            this.gridView2.Appearance.SelectedRow.Options.UseBackColor = true;
            this.gridView2.Appearance.SelectedRow.Options.UseForeColor = true;
            this.gridView2.Appearance.VertLine.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(99)))), ((int)(((byte)(127)))), ((int)(((byte)(196)))));
            this.gridView2.Appearance.VertLine.Options.UseBackColor = true;
            this.gridView2.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colBOD,
            this.colBOD_PLA,
            this.colBODMensagem,
            this.colBODValor});
            this.gridView2.GridControl = this.gridControl1;
            this.gridView2.Name = "gridView2";
            this.gridView2.OptionsView.ColumnAutoWidth = false;
            this.gridView2.OptionsView.EnableAppearanceEvenRow = true;
            this.gridView2.OptionsView.EnableAppearanceOddRow = true;
            this.gridView2.OptionsView.ShowGroupPanel = false;
            // 
            // colBOD
            // 
            this.colBOD.FieldName = "BOD";
            this.colBOD.Name = "colBOD";
            this.colBOD.OptionsColumn.ReadOnly = true;
            this.colBOD.Visible = true;
            this.colBOD.VisibleIndex = 0;
            // 
            // colBOD_PLA
            // 
            this.colBOD_PLA.Caption = "Classificação";
            this.colBOD_PLA.FieldName = "BOD_PLA";
            this.colBOD_PLA.Name = "colBOD_PLA";
            this.colBOD_PLA.OptionsColumn.ReadOnly = true;
            this.colBOD_PLA.Visible = true;
            this.colBOD_PLA.VisibleIndex = 1;
            // 
            // colBODMensagem
            // 
            this.colBODMensagem.Caption = "Descrição";
            this.colBODMensagem.FieldName = "BODMensagem";
            this.colBODMensagem.Name = "colBODMensagem";
            this.colBODMensagem.OptionsColumn.ReadOnly = true;
            this.colBODMensagem.Visible = true;
            this.colBODMensagem.VisibleIndex = 2;
            // 
            // colBODValor
            // 
            this.colBODValor.Caption = "Valor";
            this.colBODValor.ColumnEdit = this.CalcEditX;
            this.colBODValor.FieldName = "BODValor";
            this.colBODValor.Name = "colBODValor";
            this.colBODValor.OptionsColumn.ReadOnly = true;
            this.colBODValor.Visible = true;
            this.colBODValor.VisibleIndex = 3;
            // 
            // CalcEditX
            // 
            this.CalcEditX.AutoHeight = false;
            this.CalcEditX.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.CalcEditX.EditFormat.FormatString = "n2";
            this.CalcEditX.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.CalcEditX.Mask.EditMask = "n2";
            this.CalcEditX.Mask.UseMaskAsDisplayFormat = true;
            this.CalcEditX.Name = "CalcEditX";
            // 
            // gridControl1
            // 
            this.gridControl1.DataMember = "BolCred";
            this.gridControl1.DataSource = this.dEditaCreditoBindingSource;
            this.gridControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            gridLevelNode1.LevelTemplate = this.gridView2;
            gridLevelNode1.RelationName = "BolCred_BOletoDetalhe";
            this.gridControl1.LevelTree.Nodes.AddRange(new DevExpress.XtraGrid.GridLevelNode[] {
            gridLevelNode1});
            this.gridControl1.Location = new System.Drawing.Point(0, 102);
            this.gridControl1.MainView = this.gridView1;
            this.gridControl1.Name = "gridControl1";
            this.gridControl1.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.CalcEditX,
            this.repositoryItemButtonEditBOL,
            this.repositoryItemImageComboBox1,
            this.repositoryItemImageComboBox2});
            this.gridControl1.ShowOnlyPredefinedDetails = true;
            this.gridControl1.Size = new System.Drawing.Size(1452, 705);
            this.gridControl1.TabIndex = 0;
            this.gridControl1.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridView1,
            this.gridView2});
            // 
            // dEditaCreditoBindingSource
            // 
            this.dEditaCreditoBindingSource.DataSource = this.dEditaCredito;
            this.dEditaCreditoBindingSource.Position = 0;
            // 
            // dEditaCredito
            // 
            this.dEditaCredito.DataSetName = "dEditaCredito";
            this.dEditaCredito.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // gridView1
            // 
            this.gridView1.Appearance.ColumnFilterButton.BackColor = System.Drawing.Color.Orange;
            this.gridView1.Appearance.ColumnFilterButton.BorderColor = System.Drawing.Color.Orange;
            this.gridView1.Appearance.ColumnFilterButton.ForeColor = System.Drawing.Color.DarkOrange;
            this.gridView1.Appearance.ColumnFilterButton.Options.UseBackColor = true;
            this.gridView1.Appearance.ColumnFilterButton.Options.UseBorderColor = true;
            this.gridView1.Appearance.ColumnFilterButton.Options.UseForeColor = true;
            this.gridView1.Appearance.ColumnFilterButtonActive.BackColor = System.Drawing.Color.DarkOrange;
            this.gridView1.Appearance.ColumnFilterButtonActive.BorderColor = System.Drawing.Color.DarkOrange;
            this.gridView1.Appearance.ColumnFilterButtonActive.ForeColor = System.Drawing.Color.White;
            this.gridView1.Appearance.ColumnFilterButtonActive.Options.UseBackColor = true;
            this.gridView1.Appearance.ColumnFilterButtonActive.Options.UseBorderColor = true;
            this.gridView1.Appearance.ColumnFilterButtonActive.Options.UseForeColor = true;
            this.gridView1.Appearance.Empty.BackColor = System.Drawing.Color.LightSkyBlue;
            this.gridView1.Appearance.Empty.BackColor2 = System.Drawing.Color.SkyBlue;
            this.gridView1.Appearance.Empty.GradientMode = System.Drawing.Drawing2D.LinearGradientMode.BackwardDiagonal;
            this.gridView1.Appearance.Empty.Options.UseBackColor = true;
            this.gridView1.Appearance.EvenRow.BackColor = System.Drawing.Color.Linen;
            this.gridView1.Appearance.EvenRow.BackColor2 = System.Drawing.Color.AntiqueWhite;
            this.gridView1.Appearance.EvenRow.GradientMode = System.Drawing.Drawing2D.LinearGradientMode.ForwardDiagonal;
            this.gridView1.Appearance.EvenRow.Options.UseBackColor = true;
            this.gridView1.Appearance.FilterCloseButton.BackColor = System.Drawing.Color.Orange;
            this.gridView1.Appearance.FilterCloseButton.BorderColor = System.Drawing.Color.Orange;
            this.gridView1.Appearance.FilterCloseButton.Options.UseBackColor = true;
            this.gridView1.Appearance.FilterCloseButton.Options.UseBorderColor = true;
            this.gridView1.Appearance.FilterPanel.BackColor = System.Drawing.Color.DarkOrange;
            this.gridView1.Appearance.FilterPanel.BackColor2 = System.Drawing.Color.Orange;
            this.gridView1.Appearance.FilterPanel.ForeColor = System.Drawing.Color.Black;
            this.gridView1.Appearance.FilterPanel.GradientMode = System.Drawing.Drawing2D.LinearGradientMode.BackwardDiagonal;
            this.gridView1.Appearance.FilterPanel.Options.UseBackColor = true;
            this.gridView1.Appearance.FilterPanel.Options.UseForeColor = true;
            this.gridView1.Appearance.FocusedRow.BackColor = System.Drawing.Color.RoyalBlue;
            this.gridView1.Appearance.FocusedRow.BackColor2 = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this.gridView1.Appearance.FocusedRow.ForeColor = System.Drawing.Color.White;
            this.gridView1.Appearance.FocusedRow.GradientMode = System.Drawing.Drawing2D.LinearGradientMode.Vertical;
            this.gridView1.Appearance.FocusedRow.Options.UseBackColor = true;
            this.gridView1.Appearance.FocusedRow.Options.UseForeColor = true;
            this.gridView1.Appearance.FooterPanel.BackColor = System.Drawing.Color.Orange;
            this.gridView1.Appearance.FooterPanel.BorderColor = System.Drawing.Color.Orange;
            this.gridView1.Appearance.FooterPanel.Options.UseBackColor = true;
            this.gridView1.Appearance.FooterPanel.Options.UseBorderColor = true;
            this.gridView1.Appearance.GroupButton.BackColor = System.Drawing.Color.Wheat;
            this.gridView1.Appearance.GroupButton.BorderColor = System.Drawing.Color.Wheat;
            this.gridView1.Appearance.GroupButton.Options.UseBackColor = true;
            this.gridView1.Appearance.GroupButton.Options.UseBorderColor = true;
            this.gridView1.Appearance.GroupFooter.BackColor = System.Drawing.Color.Wheat;
            this.gridView1.Appearance.GroupFooter.BorderColor = System.Drawing.Color.Wheat;
            this.gridView1.Appearance.GroupFooter.Options.UseBackColor = true;
            this.gridView1.Appearance.GroupFooter.Options.UseBorderColor = true;
            this.gridView1.Appearance.GroupPanel.BackColor = System.Drawing.Color.RoyalBlue;
            this.gridView1.Appearance.GroupPanel.ForeColor = System.Drawing.Color.White;
            this.gridView1.Appearance.GroupPanel.Options.UseBackColor = true;
            this.gridView1.Appearance.GroupPanel.Options.UseForeColor = true;
            this.gridView1.Appearance.GroupRow.BackColor = System.Drawing.Color.Wheat;
            this.gridView1.Appearance.GroupRow.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Bold);
            this.gridView1.Appearance.GroupRow.Options.UseBackColor = true;
            this.gridView1.Appearance.GroupRow.Options.UseFont = true;
            this.gridView1.Appearance.HeaderPanel.BackColor = System.Drawing.Color.Orange;
            this.gridView1.Appearance.HeaderPanel.BorderColor = System.Drawing.Color.Orange;
            this.gridView1.Appearance.HeaderPanel.Options.UseBackColor = true;
            this.gridView1.Appearance.HeaderPanel.Options.UseBorderColor = true;
            this.gridView1.Appearance.HideSelectionRow.BackColor = System.Drawing.Color.LightSlateGray;
            this.gridView1.Appearance.HideSelectionRow.Options.UseBackColor = true;
            this.gridView1.Appearance.HorzLine.BackColor = System.Drawing.Color.Tan;
            this.gridView1.Appearance.HorzLine.Options.UseBackColor = true;
            this.gridView1.Appearance.OddRow.BackColor = System.Drawing.Color.White;
            this.gridView1.Appearance.OddRow.Options.UseBackColor = true;
            this.gridView1.Appearance.Preview.BackColor = System.Drawing.Color.Khaki;
            this.gridView1.Appearance.Preview.BackColor2 = System.Drawing.Color.Cornsilk;
            this.gridView1.Appearance.Preview.Font = new System.Drawing.Font("Tahoma", 7.5F);
            this.gridView1.Appearance.Preview.GradientMode = System.Drawing.Drawing2D.LinearGradientMode.BackwardDiagonal;
            this.gridView1.Appearance.Preview.Options.UseBackColor = true;
            this.gridView1.Appearance.Preview.Options.UseFont = true;
            this.gridView1.Appearance.Row.BackColor = System.Drawing.Color.Linen;
            this.gridView1.Appearance.Row.BackColor2 = System.Drawing.Color.AntiqueWhite;
            this.gridView1.Appearance.Row.Options.UseBackColor = true;
            this.gridView1.Appearance.RowSeparator.BackColor = System.Drawing.Color.LightSkyBlue;
            this.gridView1.Appearance.RowSeparator.Options.UseBackColor = true;
            this.gridView1.Appearance.VertLine.BackColor = System.Drawing.Color.Tan;
            this.gridView1.Appearance.VertLine.Options.UseBackColor = true;
            this.gridView1.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colARL_BOL,
            this.colARLValor,
            this.colARLStatusN,
            this.colBOLTipoCRAI,
            this.colBOLValorPago,
            this.colBOLStatus,
            this.colARL,
            this.colBOL,
            this.colBOLPagamento,
            this.colBotBotao,
            this.colARQIData});
            this.gridView1.GridControl = this.gridControl1;
            this.gridView1.Name = "gridView1";
            this.gridView1.OptionsCustomization.AllowSort = false;
            this.gridView1.OptionsDetail.ShowDetailTabs = false;
            this.gridView1.OptionsView.AllowCellMerge = true;
            this.gridView1.OptionsView.ColumnAutoWidth = false;
            this.gridView1.OptionsView.ShowAutoFilterRow = true;
            this.gridView1.OptionsView.ShowFooter = true;
            this.gridView1.OptionsView.ShowGroupPanel = false;
            this.gridView1.SortInfo.AddRange(new DevExpress.XtraGrid.Columns.GridColumnSortInfo[] {
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colARL_BOL, DevExpress.Data.ColumnSortOrder.Ascending),
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colBOL, DevExpress.Data.ColumnSortOrder.Ascending)});
            this.gridView1.CellMerge += new DevExpress.XtraGrid.Views.Grid.CellMergeEventHandler(this.gridView1_CellMerge);
            this.gridView1.RowCellStyle += new DevExpress.XtraGrid.Views.Grid.RowCellStyleEventHandler(this.gridView1_RowCellStyle);
            this.gridView1.CustomRowCellEdit += new DevExpress.XtraGrid.Views.Grid.CustomRowCellEditEventHandler(this.gridView1_CustomRowCellEdit);
            // 
            // colARL_BOL
            // 
            this.colARL_BOL.Caption = "Nosso Número";
            this.colARL_BOL.FieldName = "ARL_BOL";
            this.colARL_BOL.Name = "colARL_BOL";
            this.colARL_BOL.OptionsColumn.AllowMerge = DevExpress.Utils.DefaultBoolean.True;
            this.colARL_BOL.OptionsColumn.ReadOnly = true;
            this.colARL_BOL.Visible = true;
            this.colARL_BOL.VisibleIndex = 2;
            this.colARL_BOL.Width = 110;
            // 
            // colARLValor
            // 
            this.colARLValor.Caption = "Valor Pago";
            this.colARLValor.ColumnEdit = this.CalcEditX;
            this.colARLValor.FieldName = "ARLValor";
            this.colARLValor.Name = "colARLValor";
            this.colARLValor.OptionsColumn.AllowMerge = DevExpress.Utils.DefaultBoolean.True;
            this.colARLValor.OptionsColumn.ReadOnly = true;
            this.colARLValor.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum, "ARLValor", "{0:n2}")});
            this.colARLValor.Visible = true;
            this.colARLValor.VisibleIndex = 4;
            this.colARLValor.Width = 92;
            // 
            // colARLStatusN
            // 
            this.colARLStatusN.Caption = "Status baixa";
            this.colARLStatusN.ColumnEdit = this.repositoryItemImageComboBox1;
            this.colARLStatusN.FieldName = "ARLStatusN";
            this.colARLStatusN.Name = "colARLStatusN";
            this.colARLStatusN.OptionsColumn.AllowMerge = DevExpress.Utils.DefaultBoolean.False;
            this.colARLStatusN.OptionsColumn.ReadOnly = true;
            this.colARLStatusN.Visible = true;
            this.colARLStatusN.VisibleIndex = 5;
            this.colARLStatusN.Width = 95;
            // 
            // repositoryItemImageComboBox1
            // 
            this.repositoryItemImageComboBox1.AutoHeight = false;
            this.repositoryItemImageComboBox1.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemImageComboBox1.Name = "repositoryItemImageComboBox1";
            // 
            // colBOLTipoCRAI
            // 
            this.colBOLTipoCRAI.Caption = "Tipo Boleto";
            this.colBOLTipoCRAI.FieldName = "BOLTipoCRAI";
            this.colBOLTipoCRAI.Name = "colBOLTipoCRAI";
            this.colBOLTipoCRAI.OptionsColumn.AllowMerge = DevExpress.Utils.DefaultBoolean.False;
            this.colBOLTipoCRAI.OptionsColumn.ReadOnly = true;
            this.colBOLTipoCRAI.Visible = true;
            this.colBOLTipoCRAI.VisibleIndex = 6;
            this.colBOLTipoCRAI.Width = 69;
            // 
            // colBOLValorPago
            // 
            this.colBOLValorPago.Caption = "Valor Boleto";
            this.colBOLValorPago.ColumnEdit = this.CalcEditX;
            this.colBOLValorPago.FieldName = "BOLValorPago";
            this.colBOLValorPago.Name = "colBOLValorPago";
            this.colBOLValorPago.OptionsColumn.AllowMerge = DevExpress.Utils.DefaultBoolean.False;
            this.colBOLValorPago.OptionsColumn.ReadOnly = true;
            this.colBOLValorPago.Visible = true;
            this.colBOLValorPago.VisibleIndex = 7;
            this.colBOLValorPago.Width = 87;
            // 
            // colBOLStatus
            // 
            this.colBOLStatus.Caption = "Staus boleto";
            this.colBOLStatus.ColumnEdit = this.repositoryItemImageComboBox2;
            this.colBOLStatus.FieldName = "BOLStatus";
            this.colBOLStatus.Name = "colBOLStatus";
            this.colBOLStatus.OptionsColumn.AllowMerge = DevExpress.Utils.DefaultBoolean.False;
            this.colBOLStatus.OptionsColumn.ReadOnly = true;
            this.colBOLStatus.Visible = true;
            this.colBOLStatus.VisibleIndex = 8;
            this.colBOLStatus.Width = 128;
            // 
            // repositoryItemImageComboBox2
            // 
            this.repositoryItemImageComboBox2.AutoHeight = false;
            this.repositoryItemImageComboBox2.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemImageComboBox2.Name = "repositoryItemImageComboBox2";
            // 
            // colARL
            // 
            this.colARL.Caption = "ARL";
            this.colARL.FieldName = "ARL";
            this.colARL.Name = "colARL";
            this.colARL.OptionsColumn.AllowMerge = DevExpress.Utils.DefaultBoolean.False;
            this.colARL.OptionsColumn.ReadOnly = true;
            // 
            // colBOL
            // 
            this.colBOL.Caption = "Boleto";
            this.colBOL.FieldName = "BOL";
            this.colBOL.Name = "colBOL";
            this.colBOL.OptionsColumn.AllowMerge = DevExpress.Utils.DefaultBoolean.False;
            this.colBOL.Visible = true;
            this.colBOL.VisibleIndex = 3;
            this.colBOL.Width = 107;
            // 
            // colBOLPagamento
            // 
            this.colBOLPagamento.Caption = "Data Crédito";
            this.colBOLPagamento.FieldName = "BOLPagamento";
            this.colBOLPagamento.Name = "colBOLPagamento";
            this.colBOLPagamento.OptionsColumn.AllowMerge = DevExpress.Utils.DefaultBoolean.False;
            this.colBOLPagamento.Visible = true;
            this.colBOLPagamento.VisibleIndex = 1;
            this.colBOLPagamento.Width = 119;
            // 
            // colBotBotao
            // 
            this.colBotBotao.Caption = "gridColumn1";
            this.colBotBotao.Fixed = DevExpress.XtraGrid.Columns.FixedStyle.Right;
            this.colBotBotao.Name = "colBotBotao";
            this.colBotBotao.OptionsColumn.AllowMerge = DevExpress.Utils.DefaultBoolean.False;
            this.colBotBotao.OptionsColumn.ShowCaption = false;
            this.colBotBotao.Visible = true;
            this.colBotBotao.VisibleIndex = 9;
            this.colBotBotao.Width = 30;
            // 
            // colARQIData
            // 
            this.colARQIData.Caption = "Data retorno";
            this.colARQIData.FieldName = "ARQIData";
            this.colARQIData.Name = "colARQIData";
            this.colARQIData.OptionsColumn.AllowMerge = DevExpress.Utils.DefaultBoolean.False;
            this.colARQIData.OptionsColumn.ReadOnly = true;
            this.colARQIData.Visible = true;
            this.colARQIData.VisibleIndex = 0;
            this.colARQIData.Width = 104;
            // 
            // repositoryItemButtonEditBOL
            // 
            this.repositoryItemButtonEditBOL.AutoHeight = false;
            editorButtonImageOptions1.Image = global::Balancete.Properties.Resources.Bboleto;
            this.repositoryItemButtonEditBOL.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(editorButtonImageOptions1, DevExpress.XtraEditors.Controls.ButtonPredefines.Glyph, null)});
            this.repositoryItemButtonEditBOL.Name = "repositoryItemButtonEditBOL";
            this.repositoryItemButtonEditBOL.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.HideTextEditor;
            this.repositoryItemButtonEditBOL.ButtonClick += new DevExpress.XtraEditors.Controls.ButtonPressedEventHandler(this.repositoryItemButtonEditBOL_ButtonClick);
            // 
            // panelControl1
            // 
            this.panelControl1.Controls.Add(this.BotCancelar);
            this.panelControl1.Dock = System.Windows.Forms.DockStyle.Top;
            this.panelControl1.Location = new System.Drawing.Point(0, 35);
            this.panelControl1.Name = "panelControl1";
            this.panelControl1.Size = new System.Drawing.Size(1452, 67);
            this.panelControl1.TabIndex = 4;
            // 
            // BotCancelar
            // 
            this.BotCancelar.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BotCancelar.Appearance.Options.UseFont = true;
            this.BotCancelar.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("BotCancelar.ImageOptions.Image")));
            this.BotCancelar.Location = new System.Drawing.Point(5, 6);
            this.BotCancelar.Name = "BotCancelar";
            this.BotCancelar.Size = new System.Drawing.Size(237, 52);
            this.BotCancelar.TabIndex = 9;
            this.BotCancelar.Text = "Cancelar Conciliação";
            this.BotCancelar.Click += new System.EventHandler(this.BotCancelar_Click);
            // 
            // cEditaCredito
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.gridControl1);
            this.Controls.Add(this.panelControl1);
            this.Doc = System.Windows.Forms.DockStyle.Fill;
            this.LookAndFeel.UseDefaultLookAndFeel = false;
            this.Name = "cEditaCredito";
            this.Size = new System.Drawing.Size(1452, 807);
            this.Controls.SetChildIndex(this.panelControl1, 0);
            this.Controls.SetChildIndex(this.gridControl1, 0);
            ((System.ComponentModel.ISupportInitialize)(this.BarManager_F)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.StyleController_F)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.CalcEditX)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dEditaCreditoBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dEditaCredito)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemImageComboBox1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemImageComboBox2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemButtonEditBOL)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl1)).EndInit();
            this.panelControl1.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private DevExpress.XtraGrid.GridControl gridControl1;
        private System.Windows.Forms.BindingSource dEditaCreditoBindingSource;
        private dEditaCredito dEditaCredito;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView1;
        private DevExpress.XtraGrid.Columns.GridColumn colARQIData;
        private DevExpress.XtraGrid.Columns.GridColumn colARL_BOL;
        private DevExpress.XtraGrid.Columns.GridColumn colARLValor;
        private DevExpress.XtraGrid.Columns.GridColumn colARLStatusN;
        private DevExpress.XtraGrid.Columns.GridColumn colBOLTipoCRAI;
        private DevExpress.XtraGrid.Columns.GridColumn colBOLValorPago;
        private DevExpress.XtraGrid.Columns.GridColumn colBOLStatus;
        private DevExpress.XtraGrid.Columns.GridColumn colARL;
        private DevExpress.XtraEditors.Repository.RepositoryItemCalcEdit CalcEditX;
        private DevExpress.XtraGrid.Columns.GridColumn colBOL;
        private DevExpress.XtraGrid.Columns.GridColumn colBOLPagamento;
        private DevExpress.XtraGrid.Columns.GridColumn colBotBotao;
        private DevExpress.XtraEditors.Repository.RepositoryItemButtonEdit repositoryItemButtonEditBOL;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView2;
        private DevExpress.XtraGrid.Columns.GridColumn colBOD;
        private DevExpress.XtraGrid.Columns.GridColumn colBOD_PLA;
        private DevExpress.XtraGrid.Columns.GridColumn colBODMensagem;
        private DevExpress.XtraGrid.Columns.GridColumn colBODValor;
        private DevExpress.XtraEditors.Repository.RepositoryItemImageComboBox repositoryItemImageComboBox1;
        private DevExpress.XtraEditors.Repository.RepositoryItemImageComboBox repositoryItemImageComboBox2;
        private DevExpress.XtraEditors.PanelControl panelControl1;
        private DevExpress.XtraEditors.SimpleButton BotCancelar;
    }
}
