﻿namespace Balancete.GrBalancete {
    
    
    public partial class dEditaNota 
    {
        private dEditaNotaTableAdapters.NOtAsTableAdapter nOtAsTableAdapter;
        /// <summary>
        /// TableAdapter padrão para tabela: NOtAs
        /// </summary>
        public dEditaNotaTableAdapters.NOtAsTableAdapter NOtAsTableAdapter
        {
            get
            {
                if (nOtAsTableAdapter == null)
                {
                    nOtAsTableAdapter = new dEditaNotaTableAdapters.NOtAsTableAdapter();
                    nOtAsTableAdapter.TrocarStringDeConexao();
                };
                return nOtAsTableAdapter;
            }
        }

        private dEditaNotaTableAdapters.PAGamentosTableAdapter pAGamentosTableAdapter;
        /// <summary>
        /// TableAdapter padrão para tabela: PAGamentos
        /// </summary>
        public dEditaNotaTableAdapters.PAGamentosTableAdapter PAGamentosTableAdapter
        {
            get
            {
                if (pAGamentosTableAdapter == null)
                {
                    pAGamentosTableAdapter = new dEditaNotaTableAdapters.PAGamentosTableAdapter();
                    pAGamentosTableAdapter.TrocarStringDeConexao();
                };
                return pAGamentosTableAdapter;
            }
        }

    }
}
