﻿namespace Balancete.GrBalancete {
    
    
    public partial class dPeriodicos 
    {
        private dPeriodicosTableAdapters.PagamentosTableAdapter pagamentosTableAdapter;
        /// <summary>
        /// TableAdapter padrão para tabela: Pagamentos
        /// </summary>
        public dPeriodicosTableAdapters.PagamentosTableAdapter PagamentosTableAdapter
        {
            get
            {
                if (pagamentosTableAdapter == null)
                {
                    pagamentosTableAdapter = new dPeriodicosTableAdapters.PagamentosTableAdapter(); 
                    pagamentosTableAdapter.TrocarStringDeConexao();
                };
                return pagamentosTableAdapter;
            }
        }

        private dPeriodicosTableAdapters.detPagamentosTableAdapter detPagamentosTableAdapter;
        /// <summary>
        /// TableAdapter padrão para tabela: DetPagamentos
        /// </summary>
        public dPeriodicosTableAdapters.detPagamentosTableAdapter DetPagamentosTableAdapter
        {
            get
            {
                if (detPagamentosTableAdapter == null)
                {
                    detPagamentosTableAdapter = new dPeriodicosTableAdapters.detPagamentosTableAdapter();
                    detPagamentosTableAdapter.TrocarStringDeConexao();
                };
                return detPagamentosTableAdapter;
            }
        }
    }
}
