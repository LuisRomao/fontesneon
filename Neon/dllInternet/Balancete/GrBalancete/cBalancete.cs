﻿/*
30/04/2014 13.2.8.38 LH FIX - erro no agrumpamento das despesas
07/05/2014 13.2.8.43 LH a chave dos CCSs deve ser a ordem e nao CCD porque gera conflito nos cheques que tem várias linhas com o mesmo CCD
21/05/2014 13.2.9.1  LH Balancete Terminado não é mais editável
07/07/2014 13.2.9.22 LH Cheque não encontrado permite classificar manualmente.
15/07/2014 14.1.4.2  LH Condominios ocultos nao devem publicar;
*/

using Abstratos;
using Balancete.Relatorios.Credito;
using DevExpress.XtraEditors;
using DevExpress.XtraGrid.Views.Grid;
using DevExpress.XtraReports.UI;
using DevExpress.XtraSpellChecker;
using Framework;
using Framework.objetosNeon;
using System;
using System.Collections.Generic;
using System.Data;
using System.Drawing;
using System.IO;
using System.Windows.Forms;
using Transf;
using VirMSSQL;
using DevExpress.Pdf;
using VirEnumeracoesNeon;
using CompontesBasicosProc;

namespace Balancete.GrBalancete
{
    /// <summary>
    /// Componente para o balancete
    /// </summary>
    public partial class cBalancete : CompontesBasicos.ComponenteBase
    {

        private Font _FonteForte;

        private SortedList<decimal, decimal> Agrupador;

        private int AtualizacaoColunasDinamicas = 0;
        private balancete balancete1;


        private List<string> ColunasControladas = new List<string>() { "CTL", "PLA", "DescBalanceteParcial", "Valor", "CTL2", "BotaoEditor" };

        private List<DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn> colunasCriadas = null;

        private BaseEdit inplaceEditor;
        private SortedList<string, DevExpress.XtraGrid.Columns.GridColumn> NovasColunas;
        private List<DevExpress.XtraGrid.GridGroupSummaryItem> SubTotais;

        private dllVirEnum.VirEnum virEnumstatusPGF;

        private void _CarregarDados(bool recarga)
        {
            Agrupador = new SortedList<decimal, decimal>();
            balancete1.ESP.Espere("Lendo Contas Físicas ...");
            Application.DoEvents();
            balancete1.CCDrastrearCarga = (int)CCDRastrarCarga.Value;
            balancete1.CCDrastrearCarregarDadosBalancete = (int)CCDRastrearCDB.Value;
            balancete1.CCDrastrearConc = (int)CCDRastrarConc.Value;
            balancete1.CCDrastrearAcumular = (int)CCDRastrarAc.Value;
            balancete1.CCDrastrearVal = (int)CCDRastrarVal.Value;
            balancete1.CarregarDados(false);

            if (!recarga)
            {
                balancete1.ESP.Espere("Lendo contas lógicas...");
                Application.DoEvents();
                CriaColunas();
                if (balancete1.SalvoXML)
                    CriaColunas_GridBolRec();
            }
            else
                LimpaColuna_GridBolRec();

            //bandedGridView1.BeginDataUpdate();                                
            //balancete1.DBalancete1.TabelaVirtual.Clear();
            //balancete1.ESP.Espere("Lendo saldos...");
            //Application.DoEvents();
            //balancete1.CarregaSaldoInicial();
            balancete1.ESP.Espere("Lendo extratos...");
            Application.DoEvents();
            try
            {
                bandedGridView1.BeginDataUpdate();
                gridView1.BeginDataUpdate();
                gridView2.BeginDataUpdate();
                balancete1.CarregaExtratos(checkTF.Checked);
            }
            finally
            {
                bandedGridView1.EndDataUpdate();
                gridView1.EndDataUpdate();
                gridView2.EndDataUpdate();
            }
            //balancete1.ESP.Espere("Lendo Transferencias...");
            //Application.DoEvents();
            //balancete1.CarregaTransferencias();
            //Carrega Emissão de boletos
            //Carrega saldo final ???
            balancete1.ESP.Espere("Calculando saldos...");
            Application.DoEvents();
            dBalanceteBindingSource.DataSource = balancete1.DBalancete1;
            Acumular();
            //TabErroVisivel();
        }

        private void Acumular()
        {
            balancete1.Acumular();
            TabErroVisivel();
        }

        private void AdDellGrupo(bool Add, dBalancete.TabelaVirtualRow row)
        {
            if (Add)
            {
                if (!Agrupador.ContainsKey(row.Ordem))
                    Agrupador.Add(row.Ordem, row.Valor);
            }
            else
                if (Agrupador.ContainsKey(row.Ordem))
                Agrupador.Remove(row.Ordem);
            decimal Total = 0;
            foreach (decimal Valor in Agrupador.Values)
                Total += Valor;
            if ((Total == 0) && (Agrupador.Count != 0))
            {
                List<dBalancete.TabelaVirtualRow> rows = new List<dBalancete.TabelaVirtualRow>();
                bool CCTUnica = true;
                int? CCTu = null;
                bool SomenteTF = true;
                foreach (decimal Ordem in Agrupador.Keys)
                {
                    dBalancete.TabelaVirtualRow rowch = balancete1.DBalancete1.TabelaVirtual.FindByOrdem(Ordem);
                    balancete.TiposLinhas TipoL = (balancete.TiposLinhas)rowch.Tipo;
                    if ((TipoL != balancete.TiposLinhas.TransferenciaFC) && (TipoL != balancete.TiposLinhas.TransferenciaFD))
                        SomenteTF = false;

                    if (!CCTu.HasValue)
                        CCTu = rowch.CCT;
                    else
                        if (CCTu != rowch.CCT)
                        CCTUnica = false;
                    rows.Add(rowch);
                }
                if (CCTUnica && (MessageBox.Show("Confirma que as linhas selecionadas devem ser estornadas?", "Confirmação", MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button2) == DialogResult.Yes))
                    AmarraLinhas(rows, balancete.BLKTipo.Estorno);
                else if (SomenteTF && (MessageBox.Show("Confirma Tranferência Física?", "Confirmação", MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button2) == DialogResult.Yes))
                    AmarraLinhas(rows, balancete.BLKTipo.TransfF);
            }
        }



        private void AmarraLinhas(List<dBalancete.TabelaVirtualRow> rows, balancete.BLKTipo Tipo)
        {
            //if (Tipo == TiposAgrupador.TransfF)
            //    MessageBox.Show("teste");
            try
            {
                TableAdapter.AbreTrasacaoSQL("Balancete cBalancete - 2330",
                                                                       balancete1.DBalancete1.BalanceteLinKsTableAdapter,
                                                                       balancete1.DBalancete1.BalanceteLinkDetalheTableAdapter);
                dBalancete.BalanceteLinKsRow novoBLKrow = balancete1.DBalancete1.BalanceteLinKs.NewBalanceteLinKsRow();
                novoBLKrow.BLK_BAL = balancete1.BALrow.BAL;
                novoBLKrow.BLKDATAI = DateTime.Now;
                novoBLKrow.BLKI_USU = Framework.usuarioLogado.UsuarioLogadoNeon.UsuarioLogadoStNEON.USU;
                novoBLKrow.BLKTipo = (int)Tipo;
                balancete1.DBalancete1.BalanceteLinKs.AddBalanceteLinKsRow(novoBLKrow);
                balancete1.DBalancete1.BalanceteLinKsTableAdapter.Update(novoBLKrow);
                novoBLKrow.AcceptChanges();
                foreach (dBalancete.TabelaVirtualRow rowch in rows)
                {
                    dBalancete.BalanceteLinkDetalheRow novoBLDrow = balancete1.DBalancete1.BalanceteLinkDetalhe.NewBalanceteLinkDetalheRow();
                    novoBLDrow.BLD_BLK = novoBLKrow.BLK;
                    if (rowch.IsCCSNull())
                        CriaLinhaCCS(rowch);
                    novoBLDrow.BLD_CCS = rowch.CCS;
                    balancete1.DBalancete1.BalanceteLinkDetalhe.AddBalanceteLinkDetalheRow(novoBLDrow);
                    balancete1.DBalancete1.BalanceteLinkDetalheTableAdapter.Update(novoBLDrow);
                    novoBLDrow.AcceptChanges();
                    rowch.Tipo = (int)(Tipo == balancete.BLKTipo.Estorno ? balancete.TiposLinhas.Linkestorno : balancete.TiposLinhas.LinkTransferenciaF);
                    rowch.SetCTLNull();
                    rowch.SetCTL2Null();
                    rowch.SetErroNull();
                }

                TableAdapter.ST().Commit();
                foreach (dBalancete.TabelaVirtualRow rowch in rows)
                    rowch.Agrupar = false;
                Agrupador.Clear();
                balancete1.Acumular();
            }
            catch (Exception ex)
            {
                TableAdapter.ST().Vircatch(ex);
                throw new Exception(ex.Message, ex);
            }
        }

        private void bandedGridView1_CellMerge(object sender, CellMergeEventArgs e)
        {
            dBalancete.TabelaVirtualRow row1 = (dBalancete.TabelaVirtualRow)bandedGridView1.GetDataRow(e.RowHandle1);
            dBalancete.TabelaVirtualRow row2 = (dBalancete.TabelaVirtualRow)bandedGridView1.GetDataRow(e.RowHandle2);

            if ((e.Column == colErro) && (!row1.IsErroNull() || !row2.IsErroNull()))
            {
                e.Merge = false;
                e.Handled = true;
            }
            if (
                ((e.Column.FieldName.Length > 3) && (e.Column.FieldName.Substring(0, 3) == "CTL"))
                 &&
                ((row1.Tipo == (int)balancete.TiposLinhas.Rentabilidade) || (row2.Tipo == (int)balancete.TiposLinhas.Rentabilidade))
               )
            {
                if (row1.Data != row2.Data)
                {
                    e.Merge = false;
                    e.Handled = true;
                }
                if ((row1.Tipo == (int)balancete.TiposLinhas.Rentabilidade) && (balancete1.ContasLogicasRatear(row1.CCT).Contains(e.Column.FieldName)))
                {
                    e.Merge = false;
                    e.Handled = true;
                };
                if ((row2.Tipo == (int)balancete.TiposLinhas.Rentabilidade) && (balancete1.ContasLogicasRatear(row2.CCT).Contains(e.Column.FieldName)))
                {
                    e.Merge = false;
                    e.Handled = true;
                }
            }
            if (row1.Data != row2.Data)
            {
                e.Merge = false;
                e.Handled = true;
            }
            else
                //Agrupa por CCD
                if ((e.Column == colDescricao) || (e.Column == colCCDValor) || (e.Column == colTipo) || (e.Column == colCCDConsolidado))
            {
                if (!row1.IsBODNull() && !row2.IsBODNull() && row1.BOL == row2.BOL)
                {
                    e.Merge = true;
                    e.Handled = true;
                }

                else if (row1.IsCCDNull() || row2.IsCCDNull() || (row1.CCD != row2.CCD))
                {
                    e.Merge = false;
                    e.Handled = true;
                }
            }
            else
            {
                if ((e.Column.FieldName.Length > 3) && (e.Column.FieldName.Substring(0, 3) == "CTL"))
                {
                    if (
                        (!row1.IsCTLNull() && (e.Column.FieldName == balancete.NomeCampo(balancete.TipoConta.Logica, row1.CTL)))
                        ||
                        (!row1.IsCTL2Null() && (e.Column.FieldName == balancete.NomeCampo(balancete.TipoConta.Logica, row1.CTL2)))
                       )
                    {
                        e.Merge = false;
                        e.Handled = true;
                    }
                    /*
                else
                {
                    e.Merge = true;
                    e.Handled = true;
                }*/
                }
                else if ((e.Column.FieldName.Length > 3) && (e.Column.FieldName.Substring(0, 3) == "CCT"))
                    if (row1.Tipo != (int)balancete.TiposLinhas.Linkestorno)
                        if (!row1.IsCCDNull() && !row2.IsCCDNull() && (row1.CCD != row2.CCD))
                        {
                            if (!row1.IsCCTNull() && (e.Column.FieldName == balancete.NomeCampo(balancete.TipoConta.Fisica, row1.CCT)))
                            {
                                e.Merge = false;
                                e.Handled = true;
                            }
                            if (!row1.IsCCT2Null() && (e.Column.FieldName == balancete.NomeCampo(balancete.TipoConta.Fisica, row1.CCT2)))
                            {
                                e.Merge = false;
                                e.Handled = true;
                            }
                        }
                if ((e.Column == colErro) && (!row1.IsErroNull()))
                {
                    e.Merge = false;
                    e.Handled = true;
                }
            }
            //}
        }

        private void bandedGridView1_CustomColumnDisplayText(object sender, DevExpress.XtraGrid.Views.Base.CustomColumnDisplayTextEventArgs e)
        {
            if (e.ListSourceRowIndex < 0)
                return;
            if (e.Column.FieldName == "DescBalanceteParcial")
            {
                //dBalancete.TabelaVirtualRow row = (dBalancete.TabelaVirtualRow)((GridView)sender).GetDataRow(e.RowHandle);
                dBalancete.TabelaVirtualRow row = balancete1.DBalancete1.TabelaVirtual[e.ListSourceRowIndex];
                if ((row != null) && !row.IsDescBalanceteNull())
                    e.DisplayText = row.DescBalancete;
            }
        }

        //public string teste = "";

        private void bandedGridView1_CustomRowCellEdit(object sender, CustomRowCellEditEventArgs e)
        {
            try
            {
                //if (balancete1.SomenteLeitura)
                //    return;
                if (
                   (e.Column.Equals(bandedGridColumn1)) ||
                   (e.Column.Equals(BotChe1)) ||
                   (e.Column.Equals(BotNoa1)) ||
                   (e.Column.Equals(BotPGF1)) ||
                   (e.Column.Equals(bandedGridAgrupa)) ||
                   (e.Column.Equals(BotPlus))
                  )
                {
                    dBalancete.TabelaVirtualRow row = (dBalancete.TabelaVirtualRow)((GridView)sender).GetDataRow(e.RowHandle);
                    if (row == null)
                        return;
                    balancete.TiposLinhas TipoL = (balancete.TiposLinhas)row.Tipo;
                    if (e.Column.Equals(bandedGridColumn1))
                    {
                       // if (balancete1.SomenteLeitura)
                         //   return;
                        if (editavel(TipoL, "BotaoEditor", row))
                        {
                            if ((((balancete.TiposLinhas)row.Tipo == balancete.TiposLinhas.TransferenciaFC)
                                    ||
                                  ((balancete.TiposLinhas)row.Tipo == balancete.TiposLinhas.TransferenciaFD)
                                )
                                   &&
                                ((row.IsPLANull()) || (row.PLA.Substring(0, 1) == "5") || (row.PLA.Substring(0, 1) == "6"))

                               )
                                e.RepositoryItem = null;
                            else
                                e.RepositoryItem = repositoryItemButtonEdit1;
                        }
                        else
                            e.RepositoryItem = null;
                    }
                    else if (e.Column.Equals(BotChe1))
                        e.RepositoryItem = row.IsCHENull() ? null : repositoryItemBotCHE;
                    else if (e.Column.Equals(BotNoa1))
                        e.RepositoryItem = row.IsNOANull() ? null : repositoryItemBotNoa;
                    else if (e.Column.Equals(BotPGF1))
                        e.RepositoryItem = row.IsPGFNull() ? null : repositoryItemBotPGF;
                    else if (e.Column.Equals(BotPlus))
                    {
                        if (balancete1.SomenteLeitura)
                            return;
                        e.RepositoryItem = editavel(TipoL, "BotaoPlus", row) ? repositoryItemButtonPLUS : null;
                    }
                    else if (e.Column.Equals(bandedGridAgrupa))
                    {
                        if (balancete1.SomenteLeitura)
                            return;
                        if ((TipoL == balancete.TiposLinhas.Linkestorno) || (TipoL == balancete.TiposLinhas.LinkTransferenciaF))
                            e.RepositoryItem = repositoryDelEst;
                        else
                            e.RepositoryItem = editavel(TipoL, "Agrupar", row) ? repositoryItemCheckEdit1 : null;
                    }
                }
            }
            catch
            {
            }
        }

        private void bandedGridView1_FocusedRowChanged(object sender, DevExpress.XtraGrid.Views.Base.FocusedRowChangedEventArgs e)
        {
            dBalancete.TabelaVirtualRow row = (dBalancete.TabelaVirtualRow)bandedGridView1.GetDataRow(e.FocusedRowHandle);
            if (row == null)
                return;
            if (balancete1.SomenteLeitura)
            {
                colCTL.OptionsColumn.ReadOnly = true;
                colCTL21.OptionsColumn.ReadOnly = true;
                colDescBalancete.OptionsColumn.ReadOnly = true;
                colPLA.OptionsColumn.ReadOnly = true;
                colValor.OptionsColumn.ReadOnly = true;
            }
            else
            {
                colCTL.OptionsColumn.ReadOnly = !editavel((balancete.TiposLinhas)row.Tipo, "CTL", row);
                colCTL21.OptionsColumn.ReadOnly = !editavel((balancete.TiposLinhas)row.Tipo, "CTL2", row);
                colDescBalancete.OptionsColumn.ReadOnly = !editavel((balancete.TiposLinhas)row.Tipo, "DescBalanceteParcial", row);
                colPLA.OptionsColumn.ReadOnly = !editavel((balancete.TiposLinhas)row.Tipo, "PLA", row);
                colValor.OptionsColumn.ReadOnly = !editavel((balancete.TiposLinhas)row.Tipo, "Valor", row);
            }
            repositoryItemLookUpEditCTL.Buttons[1].Visible = ((balancete.TiposLinhas)row.Tipo == balancete.TiposLinhas.Rentabilidade);
            //repositoryItemLookUpEditCTL1.Buttons[1].Visible = ((balancete.TiposLinhas)row.Tipo == balancete.TiposLinhas.Rentabilidade);
        }



        private void bandedGridView1_RowCellStyle(object sender, RowCellStyleEventArgs e)
        {
            dBalancete.TabelaVirtualRow row = (dBalancete.TabelaVirtualRow)bandedGridView1.GetDataRow(e.RowHandle);
            if (row == null)
                return;


            balancete.TiposLinhas Tp = (balancete.TiposLinhas)row.Tipo;
            if (Tp == balancete.TiposLinhas.saldo)
                return;
            if (ColunasControladas.Contains(e.Column.FieldName))
            {
                if (editavel(Tp, e.Column.FieldName, row))
                    e.Appearance.BackColor = Color.LightGoldenrodYellow;
                else
                    e.Appearance.BackColor = Color.Gainsboro;

                if (e.Column == colCTL)
                    if (editavel(Tp, "CTL", row))
                        e.Appearance.BackColor = Color.LightGoldenrodYellow;
                    else
                        e.Appearance.BackColor = Color.Gainsboro;
            }




            if ((Tp == balancete.TiposLinhas.Rentabilidade) && (row.IsCTLNull()))
            {
                /*
                 if (balancete1.ColunasCalc.Contains(e.Column.FieldName)
                    &&
                    (e.Column.FieldName.Substring(0, 3) == "CTL")
                    &&
                    (e.CellValue != DBNull.Value)
                    &&
                    ((decimal)e.CellValue != 0)
                    &&
                    (e.Column.FieldName != balancete.NomeCampo(balancete.TipoConta.Logica, balancete1.DBalancete1.CTLCAIXA))
                    )
                {
                    e.Appearance.BackColor = Color.NavajoWhite;
                    e.Appearance.Font = FonteForte(e.Appearance.Font);
                }
                */
                if (balancete1.ColunasCalc.Contains(e.Column.FieldName)
                    &&
                    (e.Column.FieldName.Substring(0, 3) == "CTL")
                    &&
                    (e.CellValue != DBNull.Value)
                    )
                {
                    if (balancete1.ContasLogicasRatear(row.CCT).Contains(e.Column.FieldName))
                    {
                        e.Appearance.BackColor = Color.NavajoWhite;
                        e.Appearance.Font = FonteForte(e.Appearance.Font);
                    }
                }
                else
                    if (!row.IsCCTNull() && (e.Column.FieldName == balancete.NomeCampo(balancete.TipoConta.Fisica, row.CCT)))
                {
                    e.Appearance.BackColor = Color.NavajoWhite;
                    e.Appearance.Font = FonteForte(e.Appearance.Font);
                }
            }
            else
            {
                if ((row.Tipo != (int)balancete.TiposLinhas.TransferenciaF)
                        &&
                      (row.Tipo != (int)balancete.TiposLinhas.TransferenciaFC)
                        &&
                      (row.Tipo != (int)balancete.TiposLinhas.TransferenciaFD)
                        &&
                      (row.Tipo != (int)balancete.TiposLinhas.LinkTransferenciaF)
                        &&
                      (row.Tipo != (int)balancete.TiposLinhas.estorno)
                        &&
                      (row.Tipo != (int)balancete.TiposLinhas.Linkestorno)
                   )
                {
                    if (!row.IsCTLNull() && (e.Column.FieldName == balancete.NomeCampo(balancete.TipoConta.Logica, row.CTL)))
                    {
                        e.Appearance.BackColor = Color.NavajoWhite;
                        e.Appearance.Font = FonteForte(e.Appearance.Font);
                    }
                    if (!row.IsCTL2Null() && (e.Column.FieldName == balancete.NomeCampo(balancete.TipoConta.Logica, row.CTL2)))
                    {
                        e.Appearance.BackColor = Color.NavajoWhite;
                        e.Appearance.Font = FonteForte(e.Appearance.Font);
                    }
                }
                if (
                    (Tp != balancete.TiposLinhas.TransferenciaL)
                      &&
                    //(row.Tipo != (int)balancete.TiposLinhas.Linkestorno)
                    //&&
                    (Tp != balancete.TiposLinhas.estorno)
                   )
                {
                    if (!row.IsCCTNull() && (e.Column.FieldName == balancete.NomeCampo(balancete.TipoConta.Fisica, row.CCT)))
                    {
                        e.Appearance.BackColor = Color.NavajoWhite;
                        e.Appearance.Font = FonteForte(e.Appearance.Font);
                    }
                    if (!row.IsCCT2Null() && (e.Column.FieldName == balancete.NomeCampo(balancete.TipoConta.Fisica, row.CCT2)))
                    {
                        e.Appearance.BackColor = Color.NavajoWhite;
                        e.Appearance.Font = FonteForte(e.Appearance.Font);
                    }
                }
            }

        }

        private void bandedGridView1_RowStyle(object sender, RowStyleEventArgs e)
        {
            dBalancete.TabelaVirtualRow row = (dBalancete.TabelaVirtualRow)bandedGridView1.GetDataRow(e.RowHandle);
            if (row == null)
                return;
            balancete.TiposLinhas Tp = (balancete.TiposLinhas)row.Tipo;
            if (Tp == balancete.TiposLinhas.saldo)
                e.Appearance.BackColor = Color.FromArgb(255, 200, 200);
            else
                e.Appearance.BackColor = Color.FromArgb(230, 230, 230);
        }

        private void bandedGridView1_RowUpdated(object sender, DevExpress.XtraGrid.Views.Base.RowObjectEventArgs e)
        {
            dBalancete.TabelaVirtualRow row = (dBalancete.TabelaVirtualRow)bandedGridView1.GetDataRow(e.RowHandle);
            RowUpdated(row);
        }

        private void bandedGridView1_ShownEditor(object sender, EventArgs e)
        {
            BaseEdit activeEditor = ((GridView)sender).ActiveEditor;
            SpellChecker spellChecker1 = CompontesBasicos.virSpell.StstspellChecker;
            if ((spellChecker1 == null) || (!cHDicionario.Checked) || !spellChecker1.CanCheck(activeEditor))
                return;
            spellChecker1.SetShowSpellCheckMenu(activeEditor, true);
            if (spellChecker1.SpellCheckMode == SpellCheckMode.AsYouType && !Object.ReferenceEquals(activeEditor, inplaceEditor))
                spellChecker1.Check(activeEditor);
            inplaceEditor = activeEditor;
        }

        private void bandedGridView2_CustomColumnDisplayText(object sender, DevExpress.XtraGrid.Views.Base.CustomColumnDisplayTextEventArgs e)
        {
            if ((e.Column == colPGFCompetI) || (e.Column == colPGFCompetF))
                try
                {
                    Framework.objetosNeon.Competencia comp = new Framework.objetosNeon.Competencia((int)e.Value);
                    e.DisplayText = comp.ToString();
                }
                catch
                {
                    e.DisplayText = string.Empty;
                }
        }

        private void bandedGridView2_RowCellStyle(object sender, RowCellStyleEventArgs e)
        {
            dPeriodicos.PagamentosRow row = (dPeriodicos.PagamentosRow)bandedGridView2.GetDataRow(e.RowHandle);
            if (row == null)
                return;
            if ((e.Column.FieldName.Length > 8) && (e.Column.FieldName.Substring(0, 8) == "PGF_txt_"))
            {
                if (row["s" + e.Column.FieldName] == DBNull.Value)
                    return;
                balancete.statusPGF status = (balancete.statusPGF)row["s" + e.Column.FieldName];
                e.Appearance.BackColor = virEnumstatusPGF.GetCor(status);
                int ncomp = (int)row["b" + e.Column.FieldName];
                if (balancete1.comp.CompetenciaBind == ncomp)
                {
                    e.Appearance.ForeColor = Color.Red;
                    e.Appearance.Font = FonteForte(e.Appearance.Font);
                }
            }
        }

        private void bandedGridView3_CustomColumnDisplayText(object sender, DevExpress.XtraGrid.Views.Base.CustomColumnDisplayTextEventArgs e)
        {
            if (e.Column.Name == colNOACompet.Name)
                try
                {
                    Framework.objetosNeon.Competencia comp = new Framework.objetosNeon.Competencia((int)e.Value);
                    e.DisplayText = comp.ToString();
                }
                catch
                {
                    e.DisplayText = string.Empty;
                }
        }

        private void bandedGridView3_CustomRowCellEdit(object sender, CustomRowCellEditEventArgs e)
        {
            if (e.Column.Name == colBotCHE.Name)
            {
                GridView GV = (GridView)sender;
                dPeriodicos.detPagamentosRow Prow = (dPeriodicos.detPagamentosRow)GV.GetFocusedDataRow();
                if ((Prow != null) && !Prow.IsCHENull())
                    e.RepositoryItem = BotCHE;
                else
                    e.RepositoryItem = null;
            }
        }

        private void bandedGridView3_RowCellStyle(object sender, RowCellStyleEventArgs e)
        {
            if (e.Column.Name == colStatus1.Name)
            {
                GridView GV = (GridView)sender;
                dPeriodicos.detPagamentosRow row = (dPeriodicos.detPagamentosRow)GV.GetDataRow(e.RowHandle);
                if (row == null)
                    return;
                balancete.statusPGF status = (balancete.statusPGF)row.Status;
                e.Appearance.BackColor = virEnumstatusPGF.GetCor(status);
                if (!row.IsCHEVencimentoNull())
                    if ((balancete1.DataI <= row.CHEVencimento) && (balancete1.DataF >= row.CHEVencimento))
                        e.Appearance.ForeColor = Color.Red;
            }
        }

        private void BotCHE_ButtonClick(object sender, DevExpress.XtraEditors.Controls.ButtonPressedEventArgs e)
        {
            GridView GV = (GridView)gridControl9.FocusedView;
            dPeriodicos.detPagamentosRow Prow = (dPeriodicos.detPagamentosRow)GV.GetFocusedDataRow();
            if ((Prow == null) && (!Prow.IsCHENull()))
                return;
            Framework.Integrador.Integrador.AbreComponente(Framework.Integrador.TiposIntegra.Cheque, Prow.CHE, false, false);
        }

        private void BotDesc_Click(object sender, EventArgs e)
        {
            dCreditos.CreditosRow row = (dCreditos.CreditosRow)gridView3.GetFocusedDataRow();
            if (row == null)
                return;
            string novaDescricao = row.Descricao;
            if (VirInput.Input.Execute("Nova descrição", ref novaDescricao))
            {
                string pergunta = string.Format("Confirma a alteração da descrição de \r\n\"{0}\"\r\npara\r\n\"{1}\"\r\n", row.Descricao, novaDescricao);
                if ((novaDescricao != row.Descricao) && (MessageBox.Show(pergunta, "Confirmação", MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button2) == DialogResult.Yes))
                    foreach (dCreditos.MesRow rowBoleto in row.GetMesRows())
                        if (!rowBoleto.IsBODNull())
                            TableAdapter.ST().ExecutarSQLNonQuery("update BOletoDetalhe set BODMensagem = @P1 where bod = @P2", novaDescricao, rowBoleto.BOD);
            }
        }

        private void BOTNota_ButtonClick(object sender, DevExpress.XtraEditors.Controls.ButtonPressedEventArgs e)
        {
            GridView GV = (GridView)gridControl9.FocusedView;
            dPeriodicos.detPagamentosRow Prow = (dPeriodicos.detPagamentosRow)GV.GetFocusedDataRow();
            if (Prow == null)
                return;
            Framework.Integrador.Integrador.AbreComponente(Framework.Integrador.TiposIntegra.Nota, Prow.NOA, false, false);
        }

        private void BotPGF_ButtonClick(object sender, DevExpress.XtraEditors.Controls.ButtonPressedEventArgs e)
        {
            dPeriodicos.PagamentosRow Prow = (dPeriodicos.PagamentosRow)bandedGridView2.GetFocusedDataRow();
            if (Prow == null)
                return;
            Framework.Integrador.Integrador.AbreComponente(Framework.Integrador.TiposIntegra.Periodicos, Prow.PGF, false, false);
        }

        private void BotSalvar_Click(object sender, EventArgs e)
        {
            balancete1.SalvarXML();
        }

        private void CarregarDados(bool recarga)
        {
            if (balancete1.ESP == null)
            {
                using (balancete1.ESP = new CompontesBasicos.Espera.cEspera(this))
                    _CarregarDados(recarga);
                balancete1.ESP = null;
            }
            else
                _CarregarDados(recarga);
            xtraTabControl1.SelectedTabPage = xtraTabPageDiario;
            xtraTabControlCredito.SelectedTabPage = xtraTabCredResumo;
        }

        private void cBotaoImp1_clicado(object sender, dllBotao.BotaoArgs args)
        {
            SalvarOBS();
            balancete1.CarregarDadosBalancete(AgrupamentoUsarBalancete, false);
            bool Apontar = (args.Botao == dllBotao.Botao.imprimir);
            List<balancete.ParteImpresso> PartesImpresso = new List<balancete.ParteImpresso>();
            if (chCapa.Checked)
                PartesImpresso.Add(balancete.ParteImpresso.Capa);
            if (chDebitosCreditos.Checked)
                PartesImpresso.Add(balancete.ParteImpresso.CreditosDebitos);
            if (chCreditosAntigo.Checked)
                PartesImpresso.Add(balancete.ParteImpresso.Quadradinhos);
            if (chSaldos.Checked)
                PartesImpresso.Add(balancete.ParteImpresso.Saldos);
            if (chDetalhamentoCreditos.Checked)
                PartesImpresso.Add(balancete.ParteImpresso.Colunas);
            if (chPrevisto.Checked)
                PartesImpresso.Add(balancete.ParteImpresso.Previsto);
            if (chInadimplencia.Checked)
                PartesImpresso.Add(balancete.ParteImpresso.Inadimplencia);
            if (chInadDet.Checked)
                PartesImpresso.Add(balancete.ParteImpresso.InadDet);
            if (chExtratos.Checked)
                PartesImpresso.Add(balancete.ParteImpresso.Extratos);
            if (chCPMF.Checked)
                PartesImpresso.Add(balancete.ParteImpresso.CPMF);
            cBotaoImp1.Impresso = balancete1.GeraImpresso(Apontar, AgrupamentoUsarBalancete, PartesImpresso);            
        }

        private void cBotaoImpBol1_clicado(object sender, EventArgs e)
        {
            //dllImpresso.ExtratoBancario.ImpExtratoBal Impresso = new dllImpresso.ExtratoBancario.ImpExtratoBal(
            DataTable Contas = TableAdapter.ST().BuscaSQLTabela("SELECT CCT,CCTTitulo FROM ContaCorrenTe WHERE (CCT_CON = @P1)", balancete1.CON);
            if (Contas.Rows.Count == 0)
                MessageBox.Show("Nenhum lançamento neste período");
            if (cBotaoImpBol1.BotaoClicado == dllImpresso.Botoes.Botao.botao)
            {
                System.Collections.SortedList ListaContas = new System.Collections.SortedList();
                List<int> Selecionados = new List<int>();
                foreach (DataRow Row in Contas.Rows)
                    if (Row["CCTTitulo"] != DBNull.Value)
                        ListaContas.Add((int)Row["CCT"], (string)Row["CCTTitulo"]);
                    else
                        ListaContas.Add((int)Row["CCT"], "?");
                ;
                if (VirInput.Input.Execute("Contas Físicas", "Selecione as contas", ListaContas, out Selecionados))
                    foreach (int CCT in Selecionados)
                    {
                        dllbanco.Extrato.cExtrato Extrato = new dllbanco.Extrato.cExtrato(balancete1.DataI, balancete1.DataF, CCT, false);
                        //string TituloCCT = Row["CCTTitulo"] == DBNull.Value ? "?" : (string)Row["CCTTitulo"];
                        Extrato.Titulo = string.Format("Extrato {0}", ListaContas[CCT]);
                        Extrato.somenteleitura = false;
                        Extrato.VirShowModulo(CompontesBasicos.EstadosDosComponentes.JanelasAtivas);
                    }
            }
            else
                foreach (DataRow Row in Contas.Rows)
                {
                    //para dar mensagem se o extrato estiver com erro
                    new dllbanco.Extrato.cExtrato(balancete1.DataI, balancete1.DataF, (int)Row["CCT"], false);
                    dllImpresso.ExtratoBancario.ImpExtratoBal Impresso = new dllImpresso.ExtratoBancario.ImpExtratoBal(balancete1.rowCON.CONNome, balancete1.DataI, balancete1.DataF, (int)Row["CCT"]);
                    //int carregados = Impresso.dImpExtratoB1.DadosExtratoBTableAdapter.Fill(Impresso.dImpExtratoB1.DadosExtratoB, balancete1.DataI, balancete1.DataF, (int)Row["CCT"]);
                    if (!Impresso.ContemDados)
                        continue;

                    //Impresso.Nomecondominio.Text = balancete1.rowCON.CONNome;
                    //Impresso.DataInicial.Text = balancete1.DataI.ToString("dd/MM/yyyy");
                    //Impresso.DataFinal.Text = balancete1.DataF.ToString("dd/MM/yyyy");
                    //Impresso.dImpExtratoB1.CalculosDadosExtratoB(true);

                    //Impresso.CreateDocument();
                    switch (cBotaoImpBol1.BotaoClicado)
                    {
                        case dllImpresso.Botoes.Botao.email:
                            if (VirEmailNeon.EmailDiretoNeon.EmalST.Enviar(string.Empty, "Extrato", Impresso, "Extrato em anexo.", "Extrato"))
                                MessageBox.Show("E.mail enviado");
                            else
                                if (VirEmailNeon.EmailDiretoNeon.EmalST.UltimoErro != null)
                                MessageBox.Show("Falha no envio");
                            else
                                MessageBox.Show("Cancelado");
                            ;
                            break;
                        default:
                        case dllImpresso.Botoes.Botao.botao:
                        case dllImpresso.Botoes.Botao.imprimir:
                        case dllImpresso.Botoes.Botao.imprimir_frente:
                            Impresso.Print();
                            break;
                        case dllImpresso.Botoes.Botao.pdf:
                        case dllImpresso.Botoes.Botao.PDF_frente:
                            using (SaveFileDialog saveFileDialog1 = new SaveFileDialog())
                            {
                                saveFileDialog1.DefaultExt = "pdf";
                                if (saveFileDialog1.ShowDialog() == DialogResult.OK)
                                {
                                    Impresso.ExportOptions.Pdf.Compressed = true;
                                    Impresso.ExportOptions.Pdf.ImageQuality = DevExpress.XtraPrinting.PdfJpegImageQuality.Lowest;
                                    Impresso.ExportToPdf(saveFileDialog1.FileName);
                                }
                            }
                            break;
                        case dllImpresso.Botoes.Botao.tela:
                            Impresso.ShowPreviewDialog();
                            break;
                    }
                }

        }

        private void cHDicionario_CheckedChanged(object sender, EventArgs e)
        {
            CompontesBasicos.dEstacao.dEstacaoSt.SetValor("Usar Corretor", cHDicionario.Checked ? "1" : "0");
        }

        private void ChecaEditavel()
        {
            bool someteLeituraOrig = balancete1.SomenteLeitura;
            if (
                (balancete1.Status == BALStatus.Termindo)
                ||
                (balancete1.Status == BALStatus.FechadoDefinitivo)
                ||
                (balancete1.Status == BALStatus.ParaPublicar)
                ||
                (balancete1.Status == BALStatus.Publicado)
               )
            {
                balancete1.SomenteLeitura = true;
                rtF_Editor1.somenteleitura = true;
            }
            else
                if (!balancete1.SalvoXML)
            {
                balancete1.SomenteLeitura = false;
                rtF_Editor1.somenteleitura = false;
            }
            if (balancete1.SomenteLeitura != someteLeituraOrig)
                Invalidate(true);
        }

        private void checkTF_CheckedChanged(object sender, EventArgs e)
        {
            CarregarDados(true);
        }

        private bool CreditoCobrancaManual(dBalancete.TabelaVirtualRow row, bool Cancelar)
        {

            //string Comando2;
            using (cCreditoCobManual cCred = new cCreditoCobManual(balancete1.DBalancete1, row))
                if (cCred.VirShowModulo(CompontesBasicos.EstadosDosComponentes.PopUp) == DialogResult.OK)
                {
                    if (Cancelar)
                        return true;
                    const string Comando1 = "update contacorrentedetalhe set CCDTipoLancamento = @P1, CCDConsolidado = 6 where ccd = @P2 and CCDTipoLancamento = @P3";
                    const string Comando2 = "update boletos set bol_ccd = @P1 where bol = @P2 and bol_CCD = @P3";
                    const string Comando3 = "update boletos set bol_ccd = @P1 where bol = @P2 and bol_CCD is null";
                    if (cCred.selecionados.Count == 0)
                        return false;
                    //string ListaBoletos = "";
                    dBalancete.BOLetosRecebidosRow[] VetSelecionados = new dBalancete.BOLetosRecebidosRow[cCred.selecionados.Count];
                    int i = 0;
                    foreach (dBalancete.BOLetosRecebidosRow rowSel in cCred.selecionados)
                        VetSelecionados[i++] = rowSel;
                    //if (ListaBoletos != "")
                    //    ListaBoletos += ",";
                    //ListaBoletos += rowSel.BOL.ToString();
                    try
                    {
                        TableAdapter.AbreTrasacaoSQL("Balancete.cs CredigoCobrancaManual 680");
                        TableAdapter.STTableAdapter.ExecutarSQLNonQuery(Comando1,
                                                                                 (int)TipoLancamentoNeon.creditodecobrancaManual,
                                                                                 row.CCD,
                                                                                 (int)TipoLancamentoNeon.NaoIdentificado);
                        //Comando2 = string.Format(preComando2, ListaBoletos);
                        //VirMSSQL.TableAdapter.STTableAdapter.ExecutarSQLNonQuery(Comando2, row.CCD);
                        foreach (dBalancete.BOLetosRecebidosRow rowSel in cCred.selecionados)
                            if (rowSel.IsBOL_CCDNull())
                            {
                                if (TableAdapter.STTableAdapter.ExecutarSQLNonQuery(Comando3, row.CCD, rowSel.BOL) == 0)
                                    throw new Exception("Erro de concorrência");
                            }
                            else
                                if (TableAdapter.STTableAdapter.ExecutarSQLNonQuery(Comando2, row.CCD, rowSel.BOL, rowSel.BOL_CCD) == 0)
                                throw new Exception("Erro de concorrência");
                        TableAdapter.CommitSQL();
                        row.CCDTipoLancamento = (int)TipoLancamentoNeon.creditodecobrancaManual;
                        row.Tipo = (int)balancete.TiposLinhas.DetalheBOLManual;
                        foreach (dBalancete.BOLetosRecebidosRow rowSel in VetSelecionados)
                        {
                            rowSel.BOL_CCD = row.CCD;
                            rowSel.SetErroNull();
                        }
                        return true;
                    }
                    catch (Exception e)
                    {
                        TableAdapter.VircatchSQL(e);
                        throw new Exception(e.Message, e);
                    }
                }
            return false;
        }

        private void CriaColuna(balancete.TipoConta Tipo, int Chave, string Titulo1, string Titulo2, bool visivel)
        {
            string strNomeCampo = balancete.NomeCampo(Tipo, Chave);
            CriaColuna_Grid(Tipo, Titulo1, Titulo2, strNomeCampo, visivel);
        }

        private void CriaColuna_Grid(balancete.TipoConta Tipo, string Titulo1, string Titulo2, string strNomeCampo, bool visivel)
        {
            DevExpress.XtraGrid.Views.BandedGrid.GridBand Banda = Tipo == balancete.TipoConta.Fisica ? gridBandF : gridBandL;
            DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn Coluna = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            Coluna.AppearanceCell.BackColor = Tipo == balancete.TipoConta.Fisica ? Color.Aqua : Color.Beige;
            Coluna.AppearanceCell.Options.UseBackColor = true;
            Coluna.Caption = Titulo1;
            Coluna.CustomizationCaption = Titulo2;
            Coluna.Name = strNomeCampo;
            Coluna.FieldName = strNomeCampo;
            Coluna.Width = 80;
            Coluna.OptionsColumn.FixedWidth = true;
            //Coluna.OptionsColumn.AllowMerge = DevExpress.Utils.DefaultBoolean.False;
            Coluna.OptionsColumn.AllowMerge = DevExpress.Utils.DefaultBoolean.True;
            Coluna.OptionsColumn.ReadOnly = true;
            Coluna.ColumnEdit = this.repositoryItemCalcEdit1;
            Coluna.Visible = visivel;
            bandedGridView1.Columns.Add(Coluna);
            Banda.Columns.Add(Coluna);
        }

        private void CriaColuna_GridBolRec(string Titulo, string strNomeCampo, bool visivel = true)
        {
            if (NovasColunas == null)
            {
                NovasColunas = new SortedList<string, DevExpress.XtraGrid.Columns.GridColumn>();
                SubTotais = new List<DevExpress.XtraGrid.GridGroupSummaryItem>();
            }
            DevExpress.XtraGrid.Columns.GridColumn Coluna = new DevExpress.XtraGrid.Columns.GridColumn();
            Coluna.AppearanceCell.BackColor = Color.Beige;
            Coluna.AppearanceCell.Options.UseBackColor = true;
            Coluna.Caption = Titulo;
            Coluna.CustomizationCaption = Titulo;
            Coluna.Name = strNomeCampo;
            Coluna.FieldName = strNomeCampo;
            Coluna.Width = 80;
            Coluna.OptionsColumn.FixedWidth = true;
            Coluna.OptionsColumn.AllowMerge = DevExpress.Utils.DefaultBoolean.False;
            Coluna.OptionsColumn.ReadOnly = true;
            Coluna.ColumnEdit = repValor;
            Coluna.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
                                                new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum, strNomeCampo, "{0:n2}")});
            Coluna.Visible = visivel;
            gridView1.Columns.Add(Coluna);
            DevExpress.XtraGrid.GridGroupSummaryItem SubTotal = new DevExpress.XtraGrid.GridGroupSummaryItem(DevExpress.Data.SummaryItemType.Sum, strNomeCampo, Coluna, "{0:n2}");
            gridView1.GroupSummary.Add(SubTotal);
            NovasColunas.Add(strNomeCampo, Coluna);
            SubTotais.Add(SubTotal);
        }

        private void CriaColunas()
        {
            //balancete1.ColunasCalc = new List<string>();
            //balancete1.DBalancete1.ContaCorrenTeTableAdapter.Fill(balancete1.DBalancete1.ContaCorrenTe, balancete1.CON);
            //dBalancete.ContaCorrenTeRow rowCCTMaster = null;
            //dBalancete.ContaCorrenTeRow rowCCTOculta = null;
            foreach (dBalancete.ContaCorrenTeRow rowCCT in balancete1.DBalancete1.ContaCorrenTe)
                switch ((CCTTipo)rowCCT.CCTTipo)
                {
                    case CCTTipo.ContaCorrete_com_Oculta:
                        //rowCCTMaster = rowCCT;
                        break;
                    case CCTTipo.ContaCorreteOculta:
                        //rowCCTOculta = rowCCT;
                        break;
                }
            //if ((rowCCTMaster != null) && (rowCCTOculta != null))
            //{
            //    CriaColuna_Grid(balancete.TipoConta.Fisica, "Conta Corrente (Total)", "Conta Corrente (Total)", "calcTotalCC", true);                
            //}
            foreach (dBalancete.ContaCorrenTeRow rowCCT in balancete1.DBalancete1.ContaCorrenTe)
                //CriaColuna(balancete.TipoConta.Fisica, rowCCT.CCT, rowCCT.CCTTitulo, rowCCT.CCTTitulo, ((rowCCT != rowCCTMaster) && (rowCCT != rowCCTOculta)));                
                CriaColuna(balancete.TipoConta.Fisica, rowCCT.CCT, rowCCT.CCTTitulo, rowCCT.CCTTitulo, true);
            //if ((rowCCTMaster != null) && (rowCCTOculta != null))
            //{                
            //    balancete1.DBalancete1.TabelaVirtual.Columns.Add("calcTotalCC", typeof(decimal), string.Format("{0} + {1}", balancete.NomeCampo(balancete.TipoConta.Fisica, rowCCTMaster.CCT), balancete.NomeCampo(balancete.TipoConta.Fisica, rowCCTOculta.CCT)));
            //}


            foreach (dBalancete.ConTasLogicasRow rowCTL in balancete1.DBalancete1.ConTasLogicas)
                CriaColuna(balancete.TipoConta.Logica, rowCTL.CTL, rowCTL.CTLTitulo, rowCTL.CTLTitulo, true);
        }

        private void CriaColunas_GridBolRec()
        {
            if (AtualizacaoColunasDinamicas != balancete1.AtualizacaoColunasDinamicas)
            {
                LimpaColuna_GridBolRec();
                AtualizacaoColunasDinamicas = balancete1.AtualizacaoColunasDinamicas;
                AgrupadorBalancete.solicitaAgrupamento solicitacao = new AgrupadorBalancete.solicitaAgrupamento(AgrupadorBalancete.TipoClassificacaoDesp.PlanoContas);
                balancete1.CarregarDadosBalancete(solicitacao, false);
                for (int i = 0; i < balancete1.ColunasDinamicas.Count; i++)
                    CriaColuna_GridBolRec(balancete1.ColunasDinamicas.Values[i], balancete1.ColunasDinamicas.Keys[i]);
            }
        }


        private int CriaLinhaCCS(dBalancete.TabelaVirtualRow row, decimal? Valor = null)
        {
            dBalancete.ContaCorrenteSubdetalheRow novaCCS = balancete1.DBalancete1.ContaCorrenteSubdetalhe.NewContaCorrenteSubdetalheRow();
            novaCCS.CCS_CCD = row.CCD;
            if (!row.IsCTLNull())
                novaCCS.CCS_CTL = row.CTL;
            else
                novaCCS.CCS_CTL = balancete1.DBalancete1.CTLCAIXA;
            novaCCS.CCS_PLA = row.PLA;
            novaCCS.CCSDescricaoBAL = row.IsDescBalanceteParcialNull() ? string.Empty : row.DescBalanceteParcial.Trim();
            int tamanhoMax = balancete1.DBalancete1.ContaCorrenteSubdetalhe.Columns["CCSDescricaoBAL"].MaxLength;
            if (novaCCS.CCSDescricaoBAL.Length > tamanhoMax)
                novaCCS.CCSDescricaoBAL = novaCCS.CCSDescricaoBAL.Substring(0, tamanhoMax);
            if (Valor.HasValue)
                novaCCS.CCSValor = Valor.Value;
            else
            {
                novaCCS.CCSValor = row.IsCCSNull() ? row.Valor : 0;
                if (!row.IsPAGNull())
                    novaCCS.CCS_PAG = row.PAG;
            }
            balancete1.DBalancete1.ContaCorrenteSubdetalhe.AddContaCorrenteSubdetalheRow(novaCCS);
            balancete1.DBalancete1.ContaCorrenteSubdetalheTableAdapter.Update(novaCCS);
            balancete1.IncluiCCS(novaCCS);
            //passar para on-the-fly
            //if (row.IsCCSNull())
            //    row.CCS = novaCCS.CCS;
            //else
            //{
            //    nova linha row
            //}
            return novaCCS.CCS;
        }

        /*
        /// <summary>
        /// virEnum para campo 
        /// </summary>
        public class virEnumstatusPGF : dllVirEnum.VirEnum
        {
            /// <summary>
            /// Construtor padrao
            /// </summary>
            /// <param name="Tipo">Tipo</param>
            public virEnumstatusPGF(Type Tipo) :
                base(Tipo)
            {
            }

            private static virEnumstatusPGF _virEnumstatusPGFSt;

            /// <summary>
            /// VirEnum estático para campo statusPGF
            /// </summary>
            public static virEnumstatusPGF virEnumstatusPGFSt
            {
                get
                {
                    if (_virEnumstatusPGFSt == null)
                    {
                        _virEnumstatusPGFSt = new virEnumstatusPGF(typeof(statusPGF));
                        _virEnumstatusPGFSt.GravaNomes(statusPGF.XXX, "xxx");
                        _virEnumstatusPGFSt.GravaCor(statusPGF.XXX, System.Drawing.Color.White);

                    }
                    return _virEnumstatusPGFSt;
                }
            }
        }*/

        private void CriarColunasPGF()
        {
            balancete1.CriaColunasPGF();
            if (colunasCriadas != null)
                return;
            virEnumstatusPGF = new dllVirEnum.VirEnum(typeof(balancete.statusPGF));
            virEnumstatusPGF.GravaNomes(balancete.statusPGF.pago, labelPago.Text, panelPago.BackColor);
            virEnumstatusPGF.GravaNomes(balancete.statusPGF.agendado, labelAgendado.Text, panelAgendado.BackColor);
            virEnumstatusPGF.GravaNomes(balancete.statusPGF.vencido, labelVencido.Text, panelVencido.BackColor);
            virEnumstatusPGF.GravaNomes(balancete.statusPGF.futuro, labelFuturo.Text, panelFuturo.BackColor);
            virEnumstatusPGF.GravaNomes(balancete.statusPGF.manual, labelManual.Text, panelManual.BackColor);
            virEnumstatusPGF.CarregaEditorDaGrid(colStatus1);
            DevExpress.XtraGrid.Views.BandedGrid.GridBand Banda = gridBandCompet;

            foreach (Balancete.GrBalancete.balancete.TColunasPGF kitPGF in balancete1.ColunasPGF.Values)
            //for (Framework.objetosNeon.Competencia c = balancete1.comp.CloneCompet(-4); c <= balancete1.comp.CloneCompet(1); c++)
            {
                DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn Coluna;
                if (colunasCriadas == null)
                {
                    colunasCriadas = new List<DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn>();
                    Coluna = colPGF0;
                }
                else
                {
                    Coluna = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
                    bandedGridView2.Columns.Add(Coluna);
                    Banda.Columns.Add(Coluna);
                };
                //Coluna.AppearanceCell.BackColor =  kitPGF.comp.Mes % 2 == 0 ? Color.Aqua : Color.Beige;
                Coluna.AppearanceCell.BackColor = colPGF0.AppearanceCell.BackColor;
                Coluna.AppearanceCell.Options.UseBackColor = colPGF0.AppearanceCell.Options.UseBackColor;
                Coluna.Caption = kitPGF.comp.ToString();
                Coluna.Name = string.Format("col_per_{0}", kitPGF.comp.CompetenciaBind);
                Coluna.FieldName = Coluna.Name;
                Coluna.Width = colPGF0.Width;
                Coluna.OptionsColumn.ReadOnly = true;
                Coluna.AppearanceHeader.Options.UseTextOptions = colPGF0.AppearanceHeader.Options.UseTextOptions;
                Coluna.AppearanceHeader.TextOptions.HAlignment = colPGF0.AppearanceHeader.TextOptions.HAlignment;
                string campo = string.Format("PGF_txt_{0}", kitPGF.comp.CompetenciaBind);
                Coluna.FieldName = campo;
                //Coluna.ColumnEdit = this.repositoryItemCalcEdit1;
                Coluna.Visible = true;
            }
        }



        private void DelEstorno(dBalancete.TabelaVirtualRow row)
        {
            dBalancete.BalanceteLinkDetalheRow rowBLD = balancete1.BuscaBLDrow(row.CCS);
            dBalancete.BalanceteLinKsRow rowBLK = rowBLD.BalanceteLinKsRow;
            foreach (dBalancete.BalanceteLinkDetalheRow rowBLDx in rowBLK.GetBalanceteLinkDetalheRows())
            {
                //se só houver 1 CCS deve deletar
                if (rowBLDx.ContaCorrenteSubdetalheRow.GetBalanceteLinkDetalheRows().Length == 1)
                    rowBLDx.ContaCorrenteSubdetalheRow.Delete();
                rowBLDx.Delete();
            }
            rowBLD.Delete();
            try
            {
                TableAdapter.AbreTrasacaoSQL("Balancete cBalancete - 2406", balancete1.DBalancete1.BalanceteLinKsTableAdapter,
                                                                                     balancete1.DBalancete1.BalanceteLinkDetalheTableAdapter,
                                                                                     balancete1.DBalancete1.ContaCorrenteSubdetalheTableAdapter);
                balancete1.DBalancete1.BalanceteLinkDetalheTableAdapter.Update(balancete1.DBalancete1.BalanceteLinkDetalhe);
                balancete1.DBalancete1.BalanceteLinKsTableAdapter.Update(balancete1.DBalancete1.BalanceteLinKs);
                balancete1.DBalancete1.ContaCorrenteSubdetalheTableAdapter.Update(balancete1.DBalancete1.ContaCorrenteSubdetalhe);
                balancete1.DBalancete1.ContaCorrenteSubdetalhe.AcceptChanges();
                balancete1.DBalancete1.BalanceteLinkDetalhe.AcceptChanges();
                balancete1.DBalancete1.BalanceteLinKs.AcceptChanges();
                TableAdapter.CommitSQL();
            }
            catch (Exception ex)
            {
                TableAdapter.VircatchSQL(ex);
            };
            CarregarDados(true);
        }

        private void EditaCredito(int CCD)
        {
            cEditaCredito cEditaCredito1 = new cEditaCredito(CCD, balancete1.SomenteLeitura);            
            if (cEditaCredito1.VirShowModulo(CompontesBasicos.EstadosDosComponentes.PopUp) == DialogResult.OK)
                CarregarDados(true);
        }

        /// <summary>
        /// Edita os dados do balancete vidos de uma NF
        /// </summary>
        /// <param name="PAG"></param>
        /// <returns></returns>
        private bool EditarDadosComplementaresDaNota(int PAG)
        {
            cEditaNota cEditaNota1 = new cEditaNota(PAG, balancete1);
            if (cEditaNota1.VirShowModulo(CompontesBasicos.EstadosDosComponentes.PopUp) == DialogResult.OK)
            {
                CarregarDados(true);
                return true;
            }
            else
                return false;
        }

        private bool editavel(balancete.TiposLinhas Tipo, string campo, dBalancete.TabelaVirtualRow row)
        {
            if (campo == "Agrupar")
                switch (Tipo)
                {
                    case balancete.TiposLinhas.DetalhePAG:
                    case balancete.TiposLinhas.estornoC:
                    case balancete.TiposLinhas.estornoD:
                    case balancete.TiposLinhas.Rentabilidade:
                    case balancete.TiposLinhas.Deposito:
                    case balancete.TiposLinhas.LinhaCCD:
                    case balancete.TiposLinhas.despesanoboleto:
                    case balancete.TiposLinhas.pagEletronico:
                        return true;
                    case balancete.TiposLinhas.TransferenciaFD:
                    case balancete.TiposLinhas.TransferenciaFC:
                        return true;
                    case balancete.TiposLinhas.DetalheBOL:
                    case balancete.TiposLinhas.DetalheBOLManual:
                    case balancete.TiposLinhas.despesanoboletoManual:
                    case balancete.TiposLinhas.saldo:
                    case balancete.TiposLinhas.TransferenciaL:
                        return false;
                    default:
                        return false;
                }
            if (campo == "BotaoPlus")
                switch (Tipo)
                {
                    case balancete.TiposLinhas.DetalhePAG:
                    case balancete.TiposLinhas.Rentabilidade:
                    case balancete.TiposLinhas.estornoC:
                    case balancete.TiposLinhas.estornoD:
                    //não testados                    
                    case balancete.TiposLinhas.TransferenciaFD:
                    case balancete.TiposLinhas.TransferenciaFC:
                    case balancete.TiposLinhas.Deposito:
                    case balancete.TiposLinhas.LinhaCCD:
                    case balancete.TiposLinhas.pagEletronico:
                        return true;
                    case balancete.TiposLinhas.DetalheBOL:
                        return !row.IsErroNull();

                    case balancete.TiposLinhas.despesanoboleto:

                    case balancete.TiposLinhas.saldo:
                    case balancete.TiposLinhas.TransferenciaL:
                    case balancete.TiposLinhas.DetalheBOLManual:
                    case balancete.TiposLinhas.despesanoboletoManual:
                        return false;
                    default:
                        return false;
                }
            else if (campo == "BotaoEditor")
                switch (Tipo)
                {
                    case balancete.TiposLinhas.DetalhePAG:                        
                        return !balancete1.SomenteLeitura;
                    case balancete.TiposLinhas.DetalheBOL:
                        return !row.IsCCDNull();
                    case balancete.TiposLinhas.saldo:

                    //não testado
                    case balancete.TiposLinhas.TransferenciaL:
                    case balancete.TiposLinhas.Rentabilidade:
                    case balancete.TiposLinhas.TransferenciaFD:
                    case balancete.TiposLinhas.TransferenciaFC:
                    case balancete.TiposLinhas.Deposito:
                    case balancete.TiposLinhas.estornoC:
                    case balancete.TiposLinhas.estornoD:
                    case balancete.TiposLinhas.LinhaCCD:
                    case balancete.TiposLinhas.pagEletronico:
                    case balancete.TiposLinhas.despesanoboletoManual:
                    case balancete.TiposLinhas.DetalheBOLManual:
                        return !balancete1.SomenteLeitura;

                    case balancete.TiposLinhas.despesanoboleto:
                    default:
                        return false;
                }
            else if (campo == "CTL")
                switch (Tipo)
                {
                    case balancete.TiposLinhas.DetalheBOL:
                    case balancete.TiposLinhas.DetalhePAG:
                    case balancete.TiposLinhas.LinhaCCD:
                    case balancete.TiposLinhas.Deposito:
                    case balancete.TiposLinhas.despesanoboleto:
                    case balancete.TiposLinhas.TransferenciaL:
                    case balancete.TiposLinhas.pagEletronico:
                    case balancete.TiposLinhas.DetalheBOLManual:
                    case balancete.TiposLinhas.despesanoboletoManual:
                    case balancete.TiposLinhas.Rentabilidade:
                        return true;
                    default:
                        return false;
                }
            else if (campo == "CTL2")
                switch (Tipo)
                {
                    case balancete.TiposLinhas.TransferenciaL:
                        return true;
                    case balancete.TiposLinhas.DetalheBOL:
                    case balancete.TiposLinhas.DetalhePAG:
                    case balancete.TiposLinhas.DetalheBOLManual:
                    case balancete.TiposLinhas.despesanoboletoManual:
                    case balancete.TiposLinhas.LinhaCCD:
                    case balancete.TiposLinhas.Deposito:
                    case balancete.TiposLinhas.despesanoboleto:
                    case balancete.TiposLinhas.pagEletronico:
                    default:
                        return false;
                }
            else if (campo == "DescBalanceteParcial")
                switch (Tipo)
                {
                    case balancete.TiposLinhas.DetalhePAG:
                    case balancete.TiposLinhas.LinhaCCD:
                    case balancete.TiposLinhas.Deposito:
                    case balancete.TiposLinhas.despesanoboleto:
                    case balancete.TiposLinhas.pagEletronico:
                        return true;
                    case balancete.TiposLinhas.DetalheBOL:
                        if (row == null)
                            return false;
                        return true;
                        /*
                        if ((row.SaldoCredito) || (!row.IsCCSNull()) || (!row.IsBODNull()))
                            return true;
                        else
                            return false;*/
                    default:
                        //case balancete.TiposLinhas.DetalheBOL:
                        //case balancete.TiposLinhas.despesanoboleto:                    
                        return false;
                }
            else if (campo == "PLA")
                switch (Tipo)
                {
                    case balancete.TiposLinhas.DetalheBOL:
                    case balancete.TiposLinhas.DetalheBOLManual:
                    case balancete.TiposLinhas.despesanoboletoManual:
                    case balancete.TiposLinhas.DetalhePAG:
                    case balancete.TiposLinhas.LinhaCCD:
                    case balancete.TiposLinhas.Deposito:
                    case balancete.TiposLinhas.despesanoboleto:
                    case balancete.TiposLinhas.pagEletronico:
                        return true;
                    case balancete.TiposLinhas.TransferenciaFC:
                    case balancete.TiposLinhas.TransferenciaFD:
                        if ((row != null) && (!row.IsPLANull()) && ((row.PLA.Substring(0, 1) == "5") || (row.PLA.Substring(0, 1) == "6")))
                            return true;
                        else
                            return false;
                    default:
                        return false;
                }
            else if (campo == "Valor")
                switch (Tipo)
                {
                    case balancete.TiposLinhas.LinhaCCD:
                    case balancete.TiposLinhas.DetalhePAG:
                    case balancete.TiposLinhas.Deposito:
                    case balancete.TiposLinhas.Rentabilidade:
                    case balancete.TiposLinhas.estornoC:
                    case balancete.TiposLinhas.estornoD:
                    case balancete.TiposLinhas.pagEletronico:
                        if (row == null)
                            return false;
                        return (!row.IsCCSNull());
                    case balancete.TiposLinhas.TransferenciaL:
                        return true;
                    case balancete.TiposLinhas.DetalheBOL:
                        if (row == null)
                            return false;
                        if ((row.SaldoCredito) || (!row.IsCCSNull()))
                            return true;
                        else
                            return false;


                    case balancete.TiposLinhas.despesanoboleto:
                    case balancete.TiposLinhas.DetalheBOLManual:
                    case balancete.TiposLinhas.despesanoboletoManual:
                    default:
                        return false;
                }
            return false;
        }

        private Font FonteForte(Font prototipo)
        {
            if (_FonteForte == null)
                _FonteForte = new Font(prototipo, FontStyle.Bold);
            return _FonteForte;
        }

        private void GeraArquivosExp(object sender, EventArgs e)
        {
            balancete1.GeraArquivosExp();
        }

        private void gridView1_CustomRowCellEdit(object sender, CustomRowCellEditEventArgs e)
        {
            if (e.Column.Equals(colBotaoBoleto))
            {
                dBalancete.BOLetosRecebidosRow row = (dBalancete.BOLetosRecebidosRow)gridView1.GetDataRow(e.RowHandle);
                if (row == null)
                    return;
                e.RepositoryItem = row.IscalcBOLNull() ? null : repBotaoBoleto;
            }
        }

        private void gridView1_RowCellStyle(object sender, RowCellStyleEventArgs e)
        {
            if (e.Column == colBOLPagamento)
            {
                if (e.RowHandle < 0)
                    return;
                dBalancete.BOLetosRecebidosRow rowBOLr = (dBalancete.BOLetosRecebidosRow)gridView1.GetDataRow(e.RowHandle);
                if (rowBOLr == null)
                    return;
                if (rowBOLr.calcGrupo == (int)balancete.TpDataNoPeriodo.semCredito)
                {
                    e.Appearance.BackColor = Color.Red;
                    e.Appearance.ForeColor = Color.White;
                }
            }
        }

        private void gridView2_CustomRowCellEdit(object sender, CustomRowCellEditEventArgs e)
        {
            try
            {
                if ((e.Column.Equals(BotCHE2)) ||
                    (e.Column.Equals(BotNOA2)) ||
                    (e.Column.Equals(colAgrupar)) ||
                    (e.Column.Equals(BotPGF2)))
                {
                    dBalancete.TabelaVirtualRow row = (dBalancete.TabelaVirtualRow)gridView2.GetDataRow(e.RowHandle);
                    if (row == null)
                        return;
                    balancete.TiposLinhas TipoL = (balancete.TiposLinhas)row.Tipo;
                    if (e.Column.Equals(BotCHE2))
                        e.RepositoryItem = row.IsCHENull() ? null : repositoryItemBotCHE2;
                    else if (e.Column.Equals(BotNOA2))
                        e.RepositoryItem = row.IsNOANull() ? null : repositoryItemBotNOA2;
                    else if (e.Column.Equals(BotPGF2))
                        e.RepositoryItem = row.IsPGFNull() ? null : repositoryItemBotPGF2;
                    else if (e.Column.Equals(colAgrupar))
                        if ((TipoL == balancete.TiposLinhas.Linkestorno) || (TipoL == balancete.TiposLinhas.LinkTransferenciaF))
                            e.RepositoryItem = repositoryDelEst1;
                        else
                            e.RepositoryItem = (!balancete1.SomenteLeitura && editavel(TipoL, "Agrupar", row)) ? repositoryAgrupar : null;
                }
            }
            catch
            {

            }
        }

        private void gridView2_FocusedRowChanged(object sender, DevExpress.XtraGrid.Views.Base.FocusedRowChangedEventArgs e)
        {
            dBalancete.TabelaVirtualRow row = (dBalancete.TabelaVirtualRow)gridView2.GetDataRow(e.FocusedRowHandle);
            if (row == null)
                return;
            if (balancete1.SomenteLeitura)
            {
                colCTL1.OptionsColumn.ReadOnly = true;
                colCTL2.OptionsColumn.ReadOnly = true;
                colDescBalancete1.OptionsColumn.ReadOnly = true;
                colPLA1.OptionsColumn.ReadOnly = true;
                colValor1.OptionsColumn.ReadOnly = true;
            }
            else
            {
                colCTL1.OptionsColumn.ReadOnly = !editavel((balancete.TiposLinhas)row.Tipo, "CTL", row);
                colCTL2.OptionsColumn.ReadOnly = !editavel((balancete.TiposLinhas)row.Tipo, "CTL2", row);
                colDescBalancete1.OptionsColumn.ReadOnly = !editavel((balancete.TiposLinhas)row.Tipo, "DescBalanceteParcial", row);
                colPLA1.OptionsColumn.ReadOnly = !editavel((balancete.TiposLinhas)row.Tipo, "PLA", row);
                colValor1.OptionsColumn.ReadOnly = !editavel((balancete.TiposLinhas)row.Tipo, "Valor", row);
            }
            repositoryItemLookUpEditCTL1.Buttons[1].Visible = ((balancete.TiposLinhas)row.Tipo == balancete.TiposLinhas.Rentabilidade);
        }

        private void gridView2_RowCellStyle(object sender, RowCellStyleEventArgs e)
        {
            dBalancete.TabelaVirtualRow row = (dBalancete.TabelaVirtualRow)gridView2.GetDataRow(e.RowHandle);
            if (row == null)
                return;

            if (ColunasControladas.Contains(e.Column.FieldName))
            {
                if (editavel((balancete.TiposLinhas)row.Tipo, e.Column.FieldName, row))
                    e.Appearance.BackColor = Color.LightGoldenrodYellow;
                else
                    e.Appearance.BackColor = Color.Gainsboro;

                if (e.Column == colCTL)
                    if (editavel((balancete.TiposLinhas)row.Tipo, "CTL", row))
                        e.Appearance.BackColor = Color.LightGoldenrodYellow;
                    else
                        e.Appearance.BackColor = Color.Gainsboro;
            }
            /*
            if (e.Column == colDescBalancete1)
                switch ((balancete.TiposLinhas)row.Tipo)
                {                                            
                    case balancete.TiposLinhas.LinhaCCD:
                    case balancete.TiposLinhas.DetalhePAG:
                    case balancete.TiposLinhas.Rentabilidade:
                    case balancete.TiposLinhas.Deposito:
                        e.Appearance.BackColor = Color.LightGoldenrodYellow;
                        break;
                    case balancete.TiposLinhas.DetalheBOL:                    
                    case balancete.TiposLinhas.saldo:                        
                    case balancete.TiposLinhas.TransferenciaF:
                    case balancete.TiposLinhas.TransferenciaFC:
                    case balancete.TiposLinhas.TransferenciaFD:
                    case balancete.TiposLinhas.TransferenciaL:
                    case balancete.TiposLinhas.despesanoboleto:
                    case balancete.TiposLinhas.estorno:
                    case balancete.TiposLinhas.estornoC:
                    case balancete.TiposLinhas.estornoD:
                        break;                                                                               
                    default:
                        throw new NotImplementedException(string.Format("Tipo não implementado: {0} - {1}", row.Tipo, (balancete.TiposLinhas)row.Tipo));
                }
            if (e.Column == colPLA1)
                switch ((balancete.TiposLinhas)row.Tipo)
                {
                    case balancete.TiposLinhas.LinhaCCD:
                    case balancete.TiposLinhas.DetalhePAG:                    
                    case balancete.TiposLinhas.Deposito:
                        e.Appearance.BackColor = Color.LightGoldenrodYellow;
                        break;
                    case balancete.TiposLinhas.Rentabilidade:
                    case balancete.TiposLinhas.DetalheBOL:
                    case balancete.TiposLinhas.saldo:
                    case balancete.TiposLinhas.TransferenciaF:
                    case balancete.TiposLinhas.TransferenciaFC:
                    case balancete.TiposLinhas.TransferenciaFD:
                    case balancete.TiposLinhas.TransferenciaL:
                    case balancete.TiposLinhas.despesanoboleto:
                    case balancete.TiposLinhas.estorno:
                    case balancete.TiposLinhas.estornoC:
                    case balancete.TiposLinhas.estornoD:
                        break;
                    default:
                        throw new NotImplementedException(string.Format("Tipo não implementado: {0} - {1}", row.Tipo, (balancete.TiposLinhas)row.Tipo));
                }
            if (e.Column == colCTL1)
                if (editavel((balancete.TiposLinhas)row.Tipo, "CTL"))
                    e.Appearance.BackColor = Color.LightGoldenrodYellow;
                else
                    e.Appearance.BackColor = Color.Silver;
                /*
                switch ((balancete.TiposLinhas)row.Tipo)
                {
                    case balancete.TiposLinhas.LinhaCCD:
                    case balancete.TiposLinhas.DetalhePAG:                    
                    case balancete.TiposLinhas.Deposito:
                        e.Appearance.BackColor = Color.LightGoldenrodYellow;
                        break;
                    case balancete.TiposLinhas.Rentabilidade:
                    case balancete.TiposLinhas.DetalheBOL:
                    case balancete.TiposLinhas.saldo:
                    case balancete.TiposLinhas.TransferenciaF:
                    case balancete.TiposLinhas.TransferenciaFC:
                    case balancete.TiposLinhas.TransferenciaFD:
                    case balancete.TiposLinhas.TransferenciaL:
                    case balancete.TiposLinhas.despesanoboleto:
                    case balancete.TiposLinhas.estorno:
                    case balancete.TiposLinhas.estornoC:
                    case balancete.TiposLinhas.estornoD:
                        break;
                    default:
                        throw new NotImplementedException(string.Format("Tipo não implementado: {0} - {1}", row.Tipo, (balancete.TiposLinhas)row.Tipo));
                }*/
        }

        private void gridView2_RowUpdated(object sender, DevExpress.XtraGrid.Views.Base.RowObjectEventArgs e)
        {
            dBalancete.TabelaVirtualRow row = (dBalancete.TabelaVirtualRow)gridView2.GetDataRow(e.RowHandle);
            RowUpdated(row);
        }

        private void gridView3_RowUpdated(object sender, DevExpress.XtraGrid.Views.Base.RowObjectEventArgs e)
        {
            dCreditos.CreditosRow row = (dCreditos.CreditosRow)(((DataRowView)e.Row).Row);
            string novaDescricao = row.Descricao;
            string velhaDescricao = (string)row["Descricao", DataRowVersion.Original];

            string pergunta = string.Format("Confirma a alteração da descrição de \r\n\"{0}\"\r\npara\r\n\"{1}\"\r\n", velhaDescricao, novaDescricao);
            if ((novaDescricao != velhaDescricao) && (MessageBox.Show(pergunta, "Confirmação", MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button2) == DialogResult.Yes))
            {
                //string lista = string.Format("de {0} para {1}", velhaDescricao, novaDescricao);
                foreach (dCreditos.MesRow rowBoleto in row.GetMesRows())
                    //lista += string.Format("{0} - {1}\r\n", rowBoleto.Numero,rowBoleto.BOD);
                    if (!rowBoleto.IsBODNull())
                        TableAdapter.ST().ExecutarSQLNonQuery("update BOletoDetalhe set BODMensagem = @P1 where bod = @P2", novaDescricao, rowBoleto.BOD);
                //MessageBox.Show(lista);
                row.AcceptChanges();
            }
        }

        private void gridView4_CustomRowCellEdit(object sender, CustomRowCellEditEventArgs e)
        {
            if (e.Column.FieldName == colBotaoBoleto1.FieldName)
            {
                GridView GV = (GridView)sender;
                dCreditos.MesRow rowBOLr = (dCreditos.MesRow)GV.GetFocusedDataRow();
                if (rowBOLr == null)
                    return;
                e.RepositoryItem = (rowBOLr.Boleto < 0) ? null : repBotaoBoleto1;
            }
        }

        private void LeOBS()
        {
            string RTF = null;
            if (radioGroup1.EditValue != null)
                RTF = balancete1.GetOBS((enumOBBTipo)radioGroup1.EditValue);
            //System.IO.File.AppendAllText("d:\\log.txt", string.Format("Set antes\r\n"));
            rtF_Editor1.RTFstring = RTF;
            //System.IO.File.AppendAllText("d:\\log.txt", string.Format("Set apos\r\n"));                
        }

        private void LimpaColuna_GridBolRec()
        {
            if (NovasColunas == null)
                return;
            foreach (DevExpress.XtraGrid.Columns.GridColumn Coluna in NovasColunas.Values)
                gridView1.Columns.Remove(Coluna);
            foreach (DevExpress.XtraGrid.GridGroupSummaryItem SubTotal in SubTotais)
                gridView1.GroupSummary.Remove(SubTotal);
            NovasColunas.Clear();
            SubTotais.Clear();
        }

        private void lookMBA_ButtonClick(object sender, DevExpress.XtraEditors.Controls.ButtonPressedEventArgs e)
        {
            lookMBA.EditValue = null;
            if (e.Button.Kind == DevExpress.XtraEditors.Controls.ButtonPredefines.Undo)
                dAgrupadorBalancete.ResetSt();
        }

        private void lookMBA_Properties_EditValueChanged(object sender, EventArgs e)
        {
            if ((xtraTabControl1.SelectedTabPage == xtraTabPageCreditos)
                    ||
                (xtraTabControl1.SelectedTabPage == xtraTabPageDebitos)
                    ||
                (xtraTabControl1.SelectedTabPage == xtraTabPageCCT)
               )
                balancete1.CarregarDadosBalancete(AgrupamentoUsarBalancete, true);
        }

        private void radioGroup1_EditValueChanged(object sender, EventArgs e)
        {
            if (cHDicionario.Checked)
                rtF_Editor1.SpellAutomatico = true;
            LeOBS();
            if (!balancete1.SomenteLeitura)
                rtF_Editor1.somenteleitura = false;
        }

        private void radioGroup1_EditValueChanging(object sender, DevExpress.XtraEditors.Controls.ChangingEventArgs e)
        {
            SalvarOBS();
        }

        private void repBotaoBoleto_ButtonClick(object sender, DevExpress.XtraEditors.Controls.ButtonPressedEventArgs e)
        {
            dBalancete.BOLetosRecebidosRow row = (dBalancete.BOLetosRecebidosRow)gridView1.GetDataRow(gridView1.FocusedRowHandle);
            if (row == null)
                return;

            /*
            //CORREÇÃO BUG ITAÚ
            string correcao1 =
"UPDATE       dbo.BOLetos\r\n" +
"SET                BOLCancelado = 0, BOLPagamento = @P2, BOLValorPago = @P3, BOL_ARL = @P4\r\n" +
"FROM            dbo.ACOxBOL INNER JOIN\r\n" +
"                         dbo.BOLetos ON dbo.ACOxBOL.BOL = dbo.BOLetos.BOL\r\n" +
"WHERE        (dbo.ACOxBOL.ACO = @P1) AND (dbo.ACOxBOL.ACOBOLOriginal = 1);";
            string comandoCancela =
"UPDATE       dbo.BOLetos\r\n" +
"SET                BOL_ARL = NULL, BOLCancelado = 1\r\n" +
"FROM            dbo.ACOxBOL INNER JOIN\r\n" +
"                         dbo.BOLetos ON dbo.ACOxBOL.BOL = dbo.BOLetos.BOL\r\n" +
"WHERE        (dbo.ACOxBOL.ACO = @P1) AND (dbo.ACOxBOL.ACOBOLOriginal = 0);";
            string comandoARL = 
"UPDATE        dbo.ARquivoLido\r\n" + 
"SET                ARLValor = @P1\r\n" + 
"FROM            dbo.ARquivoLido \r\n" + 
"WHERE        (ARL = @P2);";
            string comandoBuscaTotal =
"SELECT        dbo.BOLetos.BOLValorPrevisto\r\n" +
"FROM            dbo.ACOxBOL INNER JOIN\r\n" +
"                         dbo.BOLetos ON dbo.ACOxBOL.BOL = dbo.BOLetos.BOL\r\n" +
"WHERE        (dbo.ACOxBOL.ACOBOLOriginal = 1) AND (dbo.ACOxBOL.ACO = @P1);";

            int BOLPar = row.BOL;            
            int? ACO = VirMSSQL.TableAdapter.STTableAdapter.BuscaEscalar_int("SELECT ACO FROM dbo.ACOxBOL WHERE (BOL = @P1) AND (ACOBOLOriginal = 0)",BOLPar);
            int? BOL_ARL = VirMSSQL.TableAdapter.STTableAdapter.BuscaEscalar_int("SELECT BOL_ARL FROM BOLetos WHERE (BOL = @P1)", BOLPar);
            //decimal? ValTot = VirMSSQL.TableAdapter.STTableAdapter.BuscaEscalar_decimal("SELECT SUM(dbo.BOLetos.BOLValorPrevisto) AS Total FROM dbo.ACOxBOL INNER JOIN dbo.BOLetos ON dbo.ACOxBOL.BOL = dbo.BOLetos.BOL WHERE (dbo.ACOxBOL.ACOBOLOriginal = 0) AND (dbo.ACOxBOL.ACO = @P1)", ACO.Value);
            decimal? ValTot = VirMSSQL.TableAdapter.STTableAdapter.BuscaEscalar_decimal(comandoBuscaTotal, ACO.Value);
            VirMSSQL.TableAdapter.STTableAdapter.ExecutarSQLNonQuery(correcao1, ACO.Value, row.BOLPagamento, ValTot.Value, BOL_ARL);
            VirMSSQL.TableAdapter.STTableAdapter.ExecutarSQLNonQuery(comandoCancela, ACO);
            VirMSSQL.TableAdapter.STTableAdapter.ExecutarSQLNonQuery(comandoARL, ValTot.Value,BOL_ARL.Value);
            MessageBox.Show("ok");
            return;
            //FIM CORREÇÃO BUG ITAÚ
            */

            Boletos.Boleto.cBoleto cBoleto = new Boletos.Boleto.cBoleto(balancete1.PegaBoleto(row.calcBOL).rowPrincipal, false);
            cBoleto.VirShowModulo(CompontesBasicos.EstadosDosComponentes.PopUp);
        }

        private void repositoryAgrupar_CheckedChanged(object sender, EventArgs e)
        {
            Validate();
            dBalancete.TabelaVirtualRow row = (dBalancete.TabelaVirtualRow)gridView2.GetFocusedDataRow();
            AdDellGrupo(((CheckEdit)sender).Checked, row);
        }

        private void repositoryDelEst_ButtonClick(object sender, DevExpress.XtraEditors.Controls.ButtonPressedEventArgs e)
        {
            dBalancete.TabelaVirtualRow row = (dBalancete.TabelaVirtualRow)bandedGridView1.GetFocusedDataRow();
            if ((row == null) || (row.IsCCSNull()))
                return;
            string mensagem = (balancete.TiposLinhas)row.Tipo == balancete.TiposLinhas.Linkestorno ? "Confirma o cancelamento do estorno ?" : "Confirma o cancelamento da Transferência ?";
            if (MessageBox.Show(mensagem, "Confirmação", MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button2) == DialogResult.Yes)
                DelEstorno(row);
        }

        private void repositoryDelEst1_ButtonClick(object sender, DevExpress.XtraEditors.Controls.ButtonPressedEventArgs e)
        {
            dBalancete.TabelaVirtualRow row = (dBalancete.TabelaVirtualRow)gridView2.GetFocusedDataRow();
            if ((row == null) || (row.IsCCSNull()))
                return;
            string mensagem = (balancete.TiposLinhas)row.Tipo == balancete.TiposLinhas.Linkestorno ? "Confirma o cancelamento do estorno ?" : "Confirma o cancelamento da Transferência ?";
            if (MessageBox.Show(mensagem, "Confirmação", MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button2) == DialogResult.Yes)
                DelEstorno(row);
        }

        private void repositoryItemBotCHE_ButtonClick(object sender, DevExpress.XtraEditors.Controls.ButtonPressedEventArgs e)
        {
            dBalancete.TabelaVirtualRow row = (dBalancete.TabelaVirtualRow)bandedGridView1.GetFocusedDataRow();
            if (row == null)
                return;
            Framework.Integrador.Integrador.AbreComponente(Framework.Integrador.TiposIntegra.Cheque, row.CHE, true, true);
        }

        private void repositoryItemBotCHE2_ButtonClick(object sender, DevExpress.XtraEditors.Controls.ButtonPressedEventArgs e)
        {
            dBalancete.TabelaVirtualRow row = (dBalancete.TabelaVirtualRow)gridView2.GetFocusedDataRow();
            if (row == null)
                return;
            Framework.Integrador.Integrador.AbreComponente(Framework.Integrador.TiposIntegra.Cheque, row.CHE, true, true);
        }

        private void repositoryItemBotNoa_ButtonClick(object sender, DevExpress.XtraEditors.Controls.ButtonPressedEventArgs e)
        {
            dBalancete.TabelaVirtualRow row = (dBalancete.TabelaVirtualRow)bandedGridView1.GetFocusedDataRow();
            if (row == null)
                return;
            Framework.Integrador.Integrador.AbreComponente(Framework.Integrador.TiposIntegra.Nota, row.NOA, false, false);
        }

        private void repositoryItemBotNOA2_ButtonClick(object sender, DevExpress.XtraEditors.Controls.ButtonPressedEventArgs e)
        {
            dBalancete.TabelaVirtualRow row = (dBalancete.TabelaVirtualRow)gridView2.GetFocusedDataRow();
            if (row == null)
                return;
            Framework.Integrador.Integrador.AbreComponente(Framework.Integrador.TiposIntegra.Nota, row.NOA, false, false);
        }

        private void repositoryItemBotPGF_ButtonClick(object sender, DevExpress.XtraEditors.Controls.ButtonPressedEventArgs e)
        {
            dBalancete.TabelaVirtualRow row = (dBalancete.TabelaVirtualRow)bandedGridView1.GetFocusedDataRow();
            if (row == null)
                return;
            Framework.Integrador.Integrador.AbreComponente(Framework.Integrador.TiposIntegra.Periodicos, row.PGF, false, false);
        }

        private void repositoryItemBotPGF2_ButtonClick(object sender, DevExpress.XtraEditors.Controls.ButtonPressedEventArgs e)
        {
            dBalancete.TabelaVirtualRow row = (dBalancete.TabelaVirtualRow)gridView2.GetFocusedDataRow();
            if (row == null)
                return;
            Framework.Integrador.Integrador.AbreComponente(Framework.Integrador.TiposIntegra.Periodicos, row.PGF, false, false);
        }

        private void TipoLancamentoNeonrow(dBalancete.TabelaVirtualRow row)
        {
            System.Collections.SortedList Alternativas = new System.Collections.SortedList();
            Alternativas.Add((int)TipoLancamentoNeon.NaoIdentificado, "Identificar automaticamente");
            Alternativas.Add((int)TipoLancamentoNeon.rentabilidade, "Rentabilidade");
            Alternativas.Add((int)TipoLancamentoNeon.resgate, "Transferência física - resgate - depósito");
            Alternativas.Add((int)TipoLancamentoNeon.deposito, "Depósito - Crédito");
            Alternativas.Add((int)TipoLancamentoNeon.Estorno, "Estorno");
            if (row.CCDTipoLancamento == (int)TipoLancamentoNeon.NaoIdentificado)
                Alternativas.Add((int)TipoLancamentoNeon.creditodecobrancaManual, "Crédito de Cobrança Manual");
            int novo;
            if (VirInput.Input.Execute("Tipo de lançamento", Alternativas, out novo))
            {
                dBalancete.ContaCorrenteDetalheRow rowCCD = balancete1.DBalancete1.ContaCorrenteDetalhe.FindByCCD(row.CCD);
                if (novo == (int)TipoLancamentoNeon.creditodecobrancaManual)
                {
                    if (CreditoCobrancaManual(row, false))
                        if (MessageBox.Show("Recalcular agora?", "Classificação alterada", MessageBoxButtons.YesNo) == DialogResult.Yes)
                            CarregarDados(true);
                }
                else
                {
                    rowCCD.CCDTipoLancamento = novo;
                    rowCCD.CCDConsolidado = (int)statusconsiliado.Manual;
                    if ((!rowCCD.IsCCD_PLANull()) && (rowCCD.CCD_PLA == "930000") && (novo != 3))
                        if (rowCCD.CCDValor > 0)
                            rowCCD.CCD_PLA = "198000";
                        else
                            rowCCD.CCD_PLA = "299000";
                    balancete1.DBalancete1.ContaCorrenteDetalheTableAdapter.Update(rowCCD);
                    rowCCD.AcceptChanges();
                    if (MessageBox.Show("Recalcular agora?", "Classificação alterada", MessageBoxButtons.YesNo) == DialogResult.Yes)
                        CarregarDados(true);
                }
            }
        }

        private void repositoryItemButtonEdit1_ButtonClick(object sender, DevExpress.XtraEditors.Controls.ButtonPressedEventArgs e)
        {
            dBalancete.TabelaVirtualRow row = (dBalancete.TabelaVirtualRow)bandedGridView1.GetFocusedDataRow();
            if (row == null)
                return;

            switch ((balancete.TiposLinhas)row.Tipo)
            {
                case balancete.TiposLinhas.despesanoboletoManual:
                case balancete.TiposLinhas.DetalheBOLManual:
                    if (CreditoCobrancaManual(row, true))
                        CarregarDados(true);
                    break;
                case balancete.TiposLinhas.DetalheBOL:
                    EditaCredito(row.CCD);
                    break;
                case balancete.TiposLinhas.DetalhePAG:
                case balancete.TiposLinhas.pagEletronico:
                    if (!row.IsPAGNull())
                        EditarDadosComplementaresDaNota(row.PAG);
                    else
                    {
                        System.Collections.SortedList AlternativasCHE = new System.Collections.SortedList();
                        AlternativasCHE.Add(0, "Identificar automaticamente");
                        //Alternativas.Add(12, "Rentabilidade");
                        AlternativasCHE.Add(10, "Transferência física - resgate - depósito");
                        //Alternativas.Add(8, "Depósito - Crédito");
                        AlternativasCHE.Add(3, "Estorno");
                        int novoCHE;
                        if (VirInput.Input.Execute("Tipo de lançamento", AlternativasCHE, out novoCHE))
                        {
                            dBalancete.ContaCorrenteDetalheRow rowCCD = balancete1.DBalancete1.ContaCorrenteDetalhe.FindByCCD(row.CCD);
                            rowCCD.CCDTipoLancamento = novoCHE;
                            rowCCD.CCDConsolidado = (int)statusconsiliado.Manual;
                            if ((!rowCCD.IsCCD_PLANull()) && (rowCCD.CCD_PLA == "930000") && (novoCHE != 3))
                                if (rowCCD.CCDValor > 0)
                                    rowCCD.CCD_PLA = "198000";
                                else
                                    rowCCD.CCD_PLA = "299000";
                            balancete1.DBalancete1.ContaCorrenteDetalheTableAdapter.Update(rowCCD);
                            rowCCD.AcceptChanges();
                            if (MessageBox.Show("Recalcular agora?", "Classificação alterada", MessageBoxButtons.YesNo) == DialogResult.Yes)
                                CarregarDados(true);
                        }
                    }
                    break;
                case balancete.TiposLinhas.LinhaCCD:
                case balancete.TiposLinhas.Rentabilidade:
                case balancete.TiposLinhas.TransferenciaFC:
                case balancete.TiposLinhas.TransferenciaFD:
                case balancete.TiposLinhas.estornoC:
                case balancete.TiposLinhas.estornoD:
                case balancete.TiposLinhas.Deposito:
                    TipoLancamentoNeonrow(row);
                    break;                                    
                case balancete.TiposLinhas.saldo:

                    cSaldo cSaldoI = new cSaldo(balancete1, row == balancete1.rowI ? cSaldo.TipocSaldo.Inicio : cSaldo.TipocSaldo.termino);
                    if (cSaldoI.VirShowModulo(CompontesBasicos.EstadosDosComponentes.PopUp) == DialogResult.OK)
                        CarregarDados(true);
                    break;
                //case balancete.TiposLinhas.TransferenciaF:
                //    TransfF Tra = new TransfF(TransfF.TpChave.PAG, row.PAG);
                //    if(Tra.Abrir() == DialogResult.OK)
                //        CarregarDados(true);                    
                //    break;
                case balancete.TiposLinhas.TransferenciaL:

                    TransfL TraL = new TransfL(row.TRL);
                    TraL.DataMin = balancete1.DataI;
                    TraL.DataMax = balancete1.DataF;
                    if (TraL.Abrir() == DialogResult.OK)
                    {
                        if (TraL.rowTRL.TRLValor == 0)
                            row.Delete();
                        else
                            row.CCDValor = row.Valor = TraL.rowTRL.TRLValor;
                        Acumular();
                    }
                    //CarregarDados(true);
                    break;
            }
        }

        private void repositoryItemButtonEdit2_ButtonClick(object sender, DevExpress.XtraEditors.Controls.ButtonPressedEventArgs e)
        {
            dBalancete.BOLetosRecebidosRow rowBOLr = (dBalancete.BOLetosRecebidosRow)gridView1.GetFocusedDataRow();
            if (rowBOLr == null)
                return;
            //Boletos.Boleto.Boleto BolAbrir;
            //if((balancete1.BoletosRec != null) && (balancete1.BoletosRec.ContainsKey(rowBOLr.BOL)))
            //    BolAbrir = balancete1.BoletosRec[rowBOLr.BOL];
            //else
            //    BolAbrir = new Boletos.Boleto.Boleto(rowBOLr.BOL);
            Boletos.Boleto.cBoleto cBoleto = new Boletos.Boleto.cBoleto(balancete1.PegaBoleto(rowBOLr.BOL), false);
            cBoleto.VirShowModulo(CompontesBasicos.EstadosDosComponentes.PopUp);
        }

        private void repositoryItemButtonEdit3_ButtonClick(object sender, DevExpress.XtraEditors.Controls.ButtonPressedEventArgs e)
        {
            Relatorios.Previsto.dPrevisto.PrevistoRow rowBOLr = (Relatorios.Previsto.dPrevisto.PrevistoRow)gridView8.GetFocusedDataRow();
            if (rowBOLr == null)
                return;
            Boletos.Boleto.cBoleto cBoleto = new Boletos.Boleto.cBoleto(balancete1.PegaBoleto(rowBOLr.BOL), false);
            cBoleto.VirShowModulo(CompontesBasicos.EstadosDosComponentes.PopUp);
        }

        private void repositoryItemButtonEdit4_ButtonClick(object sender, DevExpress.XtraEditors.Controls.ButtonPressedEventArgs e)
        {
            GridView GV = (GridView)gridControl4.FocusedView;
            dCreditos.MesRow rowBOLr = (dCreditos.MesRow)GV.GetFocusedDataRow();
            if (rowBOLr == null)
                return;
            Boletos.Boleto.cBoleto cBoleto = new Boletos.Boleto.cBoleto(balancete1.PegaBoleto(rowBOLr.Boleto), false);
            cBoleto.VirShowModulo(CompontesBasicos.EstadosDosComponentes.PopUp);
        }

        private void repositoryItemButtonPLUS_ButtonClick(object sender, DevExpress.XtraEditors.Controls.ButtonPressedEventArgs e)
        {
            dBalancete.TabelaVirtualRow row = (dBalancete.TabelaVirtualRow)bandedGridView1.GetFocusedDataRow();
            if (row == null)
                return;
            switch ((balancete.TiposLinhas)row.Tipo)
            {
                case balancete.TiposLinhas.saldo:
                    break;
                case balancete.TiposLinhas.LinhaCCD:
                case balancete.TiposLinhas.DetalheBOL:
                case balancete.TiposLinhas.DetalhePAG:
                case balancete.TiposLinhas.DetalheBOLManual:
                case balancete.TiposLinhas.despesanoboletoManual:
                case balancete.TiposLinhas.Deposito:
                case balancete.TiposLinhas.Rentabilidade:
                case balancete.TiposLinhas.estornoC:
                case balancete.TiposLinhas.estornoD:
                case balancete.TiposLinhas.pagEletronico:
                    CriaLinhaCCS(row);
                    //balancete1.Acumular();
                    Acumular();
                    break;
                case balancete.TiposLinhas.TransferenciaF:
                case balancete.TiposLinhas.TransferenciaFC:
                case balancete.TiposLinhas.TransferenciaFD:
                case balancete.TiposLinhas.TransferenciaL:
                case balancete.TiposLinhas.despesanoboleto:
                case balancete.TiposLinhas.estorno:
                    //case balancete.TiposLinhas.pagEletronico:
                    break;

                default:
                    throw new NotImplementedException(string.Format("Tipo não implementado: {0} - {1}", row.Tipo, (balancete.TiposLinhas)row.Tipo));
            }
        }

        private void repositoryItemCheckEdit1_CheckedChanged(object sender, EventArgs e)
        {
            Validate();
            dBalancete.TabelaVirtualRow row = (dBalancete.TabelaVirtualRow)bandedGridView1.GetFocusedDataRow();
            AdDellGrupo(((CheckEdit)sender).Checked, row);
        }

        private void RowUpdated(dBalancete.TabelaVirtualRow row)
        {
            if (row == null)
                return;

            if (!TemCampoAlterado(row))
                return;



            if (!row.IsCCSNull())
            {
                dBalancete.ContaCorrenteSubdetalheRow rowCCS = balancete1.DBalancete1.ContaCorrenteSubdetalhe.FindByCCS(row.CCS);
                if ((row.IsValorNull()) || (row.Valor == 0))
                {
                    if (balancete1.removerCCS(rowCCS, row))
                        Acumular();
                    else
                        CarregarDados(true);
                    return;
                    //row.Delete();
                    //balancete1.DBalancete1.TabelaVirtual.AcceptChanges();
                    //balancete1.Acumular();
                    //return;
                }
                else
                {
                    rowCCS.CCSDescricaoBAL = row.DescBalanceteParcial;
                    rowCCS.CCS_PLA = row.PLA;
                    rowCCS.CCS_CTL = row.CTL;
                    rowCCS.CCSValor = row.Valor;
                    balancete1.DBalancete1.ContaCorrenteSubdetalheTableAdapter.Update(rowCCS);
                    rowCCS.AcceptChanges();
                }
                row.AcceptChanges();
            }
            else if (!row.IsBODNull())
            {
                if ((row["DescBalanceteParcial", DataRowVersion.Current] != row["DescBalanceteParcial", DataRowVersion.Original]) && !row.IsDescBalanceteParcialNull())
                {
                    TableAdapter.ST().ExecutarSQLNonQuery("update BOletoDetalhe set BODMensagem = @P1 where BOD = @P2", row.DescBalanceteParcial, row.BOD);
                    balancete1.ResetBoletosRec();
                }
                if ((row["PLA", DataRowVersion.Current] != row["PLA", DataRowVersion.Original]) && !row.IsPLANull())
                {
                    TableAdapter.ST().ExecutarSQLNonQuery("update BOletoDetalhe set BOD_PLA = @P1 where BOD = @P2", row.PLA, row.BOD);
                    row.Tipo = row.PLA.Substring(0, 1) == "1" ? (int)balancete.TiposLinhas.DetalheBOL : (int)balancete.TiposLinhas.despesanoboleto;
                    balancete1.ResetBoletosRec();
                }
                if (!row["CTL", DataRowVersion.Current].Equals(row["CTL", DataRowVersion.Original]))
                {
                    TableAdapter.ST().ExecutarSQLNonQuery("update BOletoDetalhe set BOD_CTL = @P1 where BOD = @P2", row.CTL, row.BOD);
                    balancete1.ResetBoletosRec();
                }
            }
            else if (row.SaldoCredito)
            {
                row.SaldoCredito = false;
                row.SetErroNull();
                CriaLinhaCCS(row);
                if (!row["Valor", DataRowVersion.Current].Equals(row["Valor", DataRowVersion.Original]))
                {
                    row.CCSSoma = (decimal)row["Valor", DataRowVersion.Original];
                    decimal Erro = (decimal)row["Valor", DataRowVersion.Original] - (decimal)row["Valor", DataRowVersion.Current];
                    CriaLinhaCCS(row, Erro);
                }
            }
            else
            {

                dBalancete.ContaCorrenteDetalheRow rowCCD = null;
                if (!row.IsCCDNull())
                    rowCCD = balancete1.DBalancete1.ContaCorrenteDetalhe.FindByCCD(row.CCD);

                balancete.TiposLinhas TipoEquivalente = (balancete.TiposLinhas)row.Tipo;

                if (TipoEquivalente == balancete.TiposLinhas.TransferenciaFD)
                    if (row.IsPAGNull())
                        TipoEquivalente = balancete.TiposLinhas.LinhaCCD;
                    else
                        TipoEquivalente = balancete.TiposLinhas.DetalhePAG;

                if (TipoEquivalente == balancete.TiposLinhas.TransferenciaFC)
                    TipoEquivalente = balancete.TiposLinhas.Deposito;

                if ((row["DescBalanceteParcial", DataRowVersion.Current] != row["DescBalanceteParcial", DataRowVersion.Original]) && !row.IsDescBalanceteParcialNull())
                    switch (TipoEquivalente)
                    {
                        case balancete.TiposLinhas.saldo:
                            //case balancete.TiposLinhas.despesanoboleto:
                            break;
                        case balancete.TiposLinhas.LinhaCCD:
                        case balancete.TiposLinhas.Rentabilidade:
                        case balancete.TiposLinhas.Deposito:
                            //case balancete.TiposLinhas.DetalhePAG:
                            if (rowCCD != null)
                            {
                                rowCCD.CCD_DescricaoBAL = row.DescBalanceteParcial;
                                balancete1.DBalancete1.ContaCorrenteDetalheTableAdapter.Update(rowCCD);
                                rowCCD.AcceptChanges();
                            }
                            break;

                        case balancete.TiposLinhas.DetalheBOL:
                        case balancete.TiposLinhas.despesanoboleto:
                        case balancete.TiposLinhas.DetalheBOLManual:
                        case balancete.TiposLinhas.despesanoboletoManual:
                            if (!row.IsCCSNull())
                            {
                                dBalancete.ContaCorrenteSubdetalheRow rowCCS = balancete1.DBalancete1.ContaCorrenteSubdetalhe.FindByCCS(row.CCS);
                                rowCCS.CCSDescricaoBAL = row.DescBalanceteParcial;
                                balancete1.DBalancete1.ContaCorrenteSubdetalheTableAdapter.Update(rowCCS);
                                rowCCS.AcceptChanges();
                            }
                            else
                            {
                                //row.Tipo = (int)(row.PLA.StartsWith("2") ? balancete.TiposLinhas.despesanoboleto : balancete.TiposLinhas.DetalheBOL);
                                List<int> BODs = balancete1.BODsLigados[row.Ordem];
                                if (BODs != null)
                                    foreach (int BOD in BODs)
                                        TableAdapter.ST().ExecutarSQLNonQuery("update BOletoDetalhe set BODMensagem = @P1 where BOD = @P2", row.DescBalanceteParcial, BOD);
                            }
                            break;
                        case balancete.TiposLinhas.DetalhePAG:
                        case balancete.TiposLinhas.pagEletronico:
                            if (row.IsPAGNull())
                                CriaLinhaCCS(row);
                            else
                                TableAdapter.ST().ExecutarSQLNonQuery("UPDATE NOtAs SET NOAServicoBal = @P1 FROM PAGamentos INNER JOIN NOtAs ON PAGamentos.PAG_NOA = NOtAs.NOA WHERE PAGamentos.PAG = @P2", row.DescBalanceteParcial, row.PAG);
                            break;
                        case balancete.TiposLinhas.TransferenciaF:
                        case balancete.TiposLinhas.TransferenciaFC:
                        case balancete.TiposLinhas.TransferenciaFD:
                            break;
                        case balancete.TiposLinhas.TransferenciaL:
                            break;
                        default:
                            throw new NotImplementedException(string.Format("Tipo não implementado: {0}", TipoEquivalente));
                    }



                if ((row["PLA", DataRowVersion.Current] != row["PLA", DataRowVersion.Original]) && !row.IsPLANull())


                    switch (TipoEquivalente)
                    {
                        default:
                            throw new NotImplementedException(string.Format("Tipo não implementado: {0}", TipoEquivalente));
                        case balancete.TiposLinhas.estorno:
                        case balancete.TiposLinhas.saldo:
                            break;
                        case balancete.TiposLinhas.LinhaCCD:
                        case balancete.TiposLinhas.Rentabilidade:
                        case balancete.TiposLinhas.Deposito:
                        case balancete.TiposLinhas.estornoC:
                        case balancete.TiposLinhas.estornoD:

                            if (rowCCD != null)
                            {
                                rowCCD.CCD_PLA = row.PLA;
                                balancete1.DBalancete1.ContaCorrenteDetalheTableAdapter.Update(rowCCD);
                                rowCCD.AcceptChanges();
                            }
                            break;
                        case balancete.TiposLinhas.DetalheBOL:
                        case balancete.TiposLinhas.despesanoboleto:
                        case balancete.TiposLinhas.DetalheBOLManual:
                        case balancete.TiposLinhas.despesanoboletoManual:
                            if (!row.IsCCSNull())
                            {
                                dBalancete.ContaCorrenteSubdetalheRow rowCCS = balancete1.DBalancete1.ContaCorrenteSubdetalhe.FindByCCS(row.CCS);
                                rowCCS.CCS_PLA = row.PLA;
                                balancete1.DBalancete1.ContaCorrenteSubdetalheTableAdapter.Update(rowCCS);
                                rowCCS.AcceptChanges();
                            }
                            else
                            {
                                row.Tipo = (int)(row.PLA.StartsWith("2") ? balancete.TiposLinhas.despesanoboleto : balancete.TiposLinhas.DetalheBOL);
                                List<int> BODs = balancete1.BODsLigados[row.Ordem];
                                if (BODs != null)
                                    foreach (int BOD in BODs)
                                        TableAdapter.ST().ExecutarSQLNonQuery("update BOletoDetalhe set BOD_PLA = @P1 where BOD = @P2", row.PLA, BOD);
                            }
                            break;
                        case balancete.TiposLinhas.DetalhePAG:
                        case balancete.TiposLinhas.pagEletronico:
                            if (row.PLA == "930000")
                                rowCCD.CCD_PLA = row.PLA;
                            else
                                if (!row.IsPAGNull())
                                TableAdapter.ST().ExecutarSQLNonQuery("UPDATE NOtAs SET NOA_PLA = @P1 FROM PAGamentos INNER JOIN NOtAs ON PAGamentos.PAG_NOA = NOtAs.NOA WHERE PAGamentos.PAG = @P2", row.PLA, row.PAG);
                            else
                            {
                                rowCCD.CCD_PLA = row.PLA;
                                balancete1.DBalancete1.ContaCorrenteDetalheTableAdapter.Update(rowCCD);
                                rowCCD.AcceptChanges();
                            }
                            balancete1.DBalancete1.ContaCorrenteDetalheTableAdapter.Update(rowCCD);
                            rowCCD.AcceptChanges();
                            break;
                        case balancete.TiposLinhas.TransferenciaF:
                            break;
                        case balancete.TiposLinhas.TransferenciaFC:
                        case balancete.TiposLinhas.TransferenciaFD:
                            break;
                        case balancete.TiposLinhas.TransferenciaL:
                            break;
                    }

                //if (row["Valor", DataRowVersion.Current] != row["Valor", DataRowVersion.Original])
                if (!row["Valor", DataRowVersion.Current].Equals(row["Valor", DataRowVersion.Original]))
                    switch (TipoEquivalente)
                    {
                        case balancete.TiposLinhas.TransferenciaL:
                            if (!row.IsTRLNull())
                            {
                                TransfL TraL = new TransfL(row.TRL);
                                TraL.rowTRL.TRLValor = row.Valor;
                                TraL.Salvar();
                                if (row.Valor == 0)
                                {
                                    row.Delete();
                                    return;
                                }
                                else
                                    row.CCDValor = row.Valor;
                            }
                            break;
                        case balancete.TiposLinhas.DetalheBOL:
                        case balancete.TiposLinhas.despesanoboleto:
                        case balancete.TiposLinhas.DetalheBOLManual:
                        case balancete.TiposLinhas.despesanoboletoManual:
                            if (!row.IsCCSNull())
                            {
                                dBalancete.ContaCorrenteSubdetalheRow rowCCS = balancete1.DBalancete1.ContaCorrenteSubdetalhe.FindByCCS(row.CCS);
                                rowCCS.CCSValor = row.Valor;
                                balancete1.DBalancete1.ContaCorrenteSubdetalheTableAdapter.Update(rowCCS);
                                rowCCS.AcceptChanges();
                            }
                            break;
                        case balancete.TiposLinhas.DetalhePAG:
                            if (!row.IsCCSNull())
                            {
                                dBalancete.ContaCorrenteSubdetalheRow rowCCS = balancete1.DBalancete1.ContaCorrenteSubdetalhe.FindByCCS(row.CCS);
                                rowCCS.CCSValor = row.Valor;
                                balancete1.DBalancete1.ContaCorrenteSubdetalheTableAdapter.Update(rowCCS);
                                rowCCS.AcceptChanges();
                            }
                            break;
                    }

                if (!row["CTL2", DataRowVersion.Current].Equals(row["CTL2", DataRowVersion.Original]))

                    switch (TipoEquivalente)
                    {
                        case balancete.TiposLinhas.TransferenciaL:
                            if (!row.IsTRLNull())
                            {
                                TransfL TraL = new TransfL(row.TRL);
                                TraL.rowTRL.TRLPara_CTL = row.CTL;
                                TraL.Salvar();
                            }
                            break;
                        case balancete.TiposLinhas.DetalhePAG:
                        case balancete.TiposLinhas.DetalheBOL:
                        case balancete.TiposLinhas.DetalheBOLManual:
                        case balancete.TiposLinhas.despesanoboletoManual:
                        case balancete.TiposLinhas.saldo:
                        case balancete.TiposLinhas.LinhaCCD:
                        case balancete.TiposLinhas.Deposito:
                            break;
                        default:
                            throw new NotImplementedException(string.Format("Tipo não implementado: {0}", TipoEquivalente));
                    }
                //Acumular();

                if (!row["CTL", DataRowVersion.Current].Equals(row["CTL", DataRowVersion.Original]))

                    //if((row["CTL", DataRowVersion.Current] is int)
                    //    &&
                    //    (row["CTL", DataRowVersion.Original] is int)
                    //    &&
                    //    ((int)row["CTL", DataRowVersion.Current] != (int)row["CTL", DataRowVersion.Original]))


                    switch (TipoEquivalente)
                    {
                        case balancete.TiposLinhas.TransferenciaL:
                            if (!row.IsTRLNull())
                            {
                                TransfL TraL = new TransfL(row.TRL);
                                TraL.rowTRL.TRLDe_CTL = row.CTL;
                                TraL.Salvar();
                            }
                            break;
                        case balancete.TiposLinhas.DetalhePAG:
                        case balancete.TiposLinhas.pagEletronico:
                            if (!row.IsPAGNull())
                                TableAdapter.ST().ExecutarSQLNonQuery("update pagamentos set PAG_CTL = @P1 where PAG = @P2", row.CTL, row.PAG);
                            break;
                        case balancete.TiposLinhas.DetalheBOL:
                        case balancete.TiposLinhas.despesanoboleto:
                        case balancete.TiposLinhas.DetalheBOLManual:
                        case balancete.TiposLinhas.despesanoboletoManual:
                            if (!row.IsCCSNull())
                            {
                                dBalancete.ContaCorrenteSubdetalheRow rowCCS = balancete1.DBalancete1.ContaCorrenteSubdetalhe.FindByCCS(row.CCS);
                                rowCCS.CCS_CTL = row.CTL;
                                balancete1.DBalancete1.ContaCorrenteSubdetalheTableAdapter.Update(rowCCS);
                                rowCCS.AcceptChanges();
                            }
                            else
                            {
                                List<int> BODs = balancete1.BODsLigados[row.Ordem];
                                if (BODs != null)
                                    foreach (int BOD in BODs)
                                        TableAdapter.ST().ExecutarSQLNonQuery("update BOletoDetalhe set BOD_CTL = @P1 where BOD = @P2", row.CTL, BOD);
                            }
                            break;
                        case balancete.TiposLinhas.saldo:
                            break;
                        case balancete.TiposLinhas.LinhaCCD:
                        case balancete.TiposLinhas.Deposito:
                        case balancete.TiposLinhas.Rentabilidade:
                            dBalancete.ContaCorrenteDetalheRow CCDrow = balancete1.DBalancete1.ContaCorrenteDetalhe.FindByCCD(row.CCD);
                            if (rowCCD != null)
                            {
                                if (row.IsCTLNull())
                                    rowCCD.SetCCD_CTLNull();
                                else
                                    rowCCD.CCD_CTL = row.CTL;
                                balancete1.DBalancete1.ContaCorrenteDetalheTableAdapter.Update(rowCCD);
                                rowCCD.AcceptChanges();
                            }
                            break;
                        default:
                            throw new NotImplementedException(string.Format("Tipo não implementado: {0}", TipoEquivalente));
                    }
                //Acumular();
                row.AcceptChanges();
            }
            Acumular();
        }

        private void SalvarOBS()
        {
            //System.IO.File.AppendAllText("d:\\log.txt", string.Format("Get antes\r\n"));
            if (radioGroup1.EditValue != null)
                balancete1.SetOBS((enumOBBTipo)radioGroup1.EditValue, rtF_Editor1.RTFstring);
            //System.IO.File.AppendAllText("d:\\log.txt", string.Format("Get após\r\n"));
        }

        private void simpleButton1_Click(object sender, EventArgs e)
        {
            SalvarOBS();
            FechaTela(DialogResult.OK);
            //TiraDoPool();
        }

        private void simpleButton2_Click(object sender, EventArgs e)
        {
            balancete1.CarregarDadosBalancete(AgrupamentoUsarBoleto, false);
            balancete1.simulaBoleto(null, true, false);
            balancete1.CarregarDadosBalancete(AgrupamentoUsarBalancete, false);
        }

        private void simpleButton3_Click(object sender, EventArgs e)
        {
            CarregarDados(true);
        }

        private void simpleButton4_Click(object sender, EventArgs e)
        {
            SaveFileDialog dialogo = new SaveFileDialog();
            dialogo.DefaultExt = ".xlsx";
            if (dialogo.ShowDialog() == DialogResult.OK)
                if (xtraTabControl1.SelectedTabPage == xtraTabPageDiario)
                {
                    bandedGridView1.BeginUpdate();
                    MemoryStream ms = new MemoryStream();
                    bandedGridView1.SaveLayoutToStream(ms);
                    ms.Flush();

                    // change grid view properties...
                    bandedGridView1.OptionsView.AllowCellMerge = false;

                    bandedGridView1.Export(DevExpress.XtraPrinting.ExportTarget.Xlsx, dialogo.FileName);
                    ms.Seek(0, SeekOrigin.Begin);
                    bandedGridView1.RestoreLayoutFromStream(ms);
                    bandedGridView1.EndUpdate();



                }
                else if (xtraTabControl1.SelectedTabPage == xtraTabPageIndividual)
                    gridView2.Export(DevExpress.XtraPrinting.ExportTarget.Xlsx, dialogo.FileName);
                else if (xtraTabControl1.SelectedTabPage == xtraTabPageDebitos)
                    gridView6.Export(DevExpress.XtraPrinting.ExportTarget.Xlsx, dialogo.FileName);
                else if (xtraTabControl1.SelectedTabPage == xtraTabPageCreditos)
                {
                    if (xtraTabControlCredito.SelectedTabPage == xtraTabCredResumo)
                        gridView3.Export(DevExpress.XtraPrinting.ExportTarget.Xlsx, dialogo.FileName);
                    else if (xtraTabControlCredito.SelectedTabPage == xtraTabCredDetalhes)
                        gridView1.Export(DevExpress.XtraPrinting.ExportTarget.Xlsx, dialogo.FileName);
                }
                else if (xtraTabControl1.SelectedTabPage == xtraTabPageCTL)
                    gridView5.Export(DevExpress.XtraPrinting.ExportTarget.Xlsx, dialogo.FileName);
                else if (xtraTabControl1.SelectedTabPage == xtraTabPageCCT)
                    gridView7.Export(DevExpress.XtraPrinting.ExportTarget.Xlsx, dialogo.FileName);
                else if (xtraTabControl1.SelectedTabPage == xtraTabPagePrevisto)
                    gridView8.Export(DevExpress.XtraPrinting.ExportTarget.Xlsx, dialogo.FileName);
                else if (xtraTabControl1.SelectedTabPage == xtraTabPageInad)
                    gridViewInad.Export(DevExpress.XtraPrinting.ExportTarget.Xlsx, dialogo.FileName);

        }

        private void simpleButton5_Click(object sender, EventArgs e)
        {
            if (balancete1.rowRastrear == null)
                memoEdit1.Text = "null";
            else
            {
                memoEdit1.Text = string.Empty;
                if (!balancete1.rowRastrear.IsErroNull())
                    memoEdit1.Text = balancete1.rowRastrear.Erro;
                if (!balancete1.rowRastrear.IsErroTotalNull())
                    memoEdit1.Text += string.Format("\r\nTotal:\r\n{0}", balancete1.rowRastrear.ErroTotal);
            }
        }

        /*
        private void simpleButton5_Click(object sender, EventArgs e)
        {
            dBalancete.TabelaVirtualRow row = (dBalancete.TabelaVirtualRow)bandedGridView1.GetFocusedDataRow();
            if ((row == null) || (row.Valor > 0))
                return;

            TransfF Tra = new TransfF(row.CCT, balancete1.CON, row.Valor);
            if (Tra.Status == TRFStatus.cadastrando)
            {
                Tra = new TransfF(TransfF.TpChave.CON, balancete1.CON);
                //Tra.Status = TRFStatus.cadastrando;
                if (Tra.Abrir() == DialogResult.OK)
                {
                    try
                    {
                        TableAdapter.ST().AbreTrasacaoLocal();
                        Tra.AmarraDebito(row.CCD);
                        //row.CCDTipoLancamento = (int)TipoLancamentoNeon.Cheque;
                        TableAdapter.ST().ExecutarSQLNonQuery("update ContaCorrenteDetalhe set CCDTipoLancamento = @P1 where CCD = @P2", (int)TipoLancamentoNeon.Cheque, row.CCD);
                        
                        TableAdapter.ST().Commit();
                    }
                    catch (Exception ex)
                    {
                        TableAdapter.ST().Vircatch(ex);
                    }
                    CarregarDados(true);
                }
            }
        }*/

        private void simpleButton6_Click(object sender, EventArgs e)
        {
            TransfL novaTransfL = new TransfL(TransfL.TpChave.CON, balancete1.CON);
            novaTransfL.DataMin = balancete1.DataI;
            novaTransfL.DataMax = balancete1.DataF;
            novaTransfL.Abrir();
            balancete1.CarregaTransferencia(novaTransfL);
            Acumular();
        }

        private void simpleButton7_Click(object sender, EventArgs e)
        {
            bool Resposta;
            using (CompontesBasicos.Espera.cEspera ESP = new CompontesBasicos.Espera.cEspera(this))
                Resposta = balancete1.TranfereArrecadado(ESP);
            if (Resposta)
                MessageBox.Show("Cheque gerado");
            else
                MessageBox.Show("Não há valor para transferir");
        }

        private void simpleButton8_Click(object sender, EventArgs e)
        {
            /*
            dBalancete.TabelaVirtualRow row = (dBalancete.TabelaVirtualRow)bandedGridView1.GetFocusedDataRow();
            if (row == null)
                return;
            
            switch ((balancete.TiposLinhas)row.Tipo)
            {
                case balancete.TiposLinhas.saldo:
                    break;
                case balancete.TiposLinhas.Extrato:                    
                    break;
                case balancete.TiposLinhas.DetalhePAG:
                    row.Tipo = (int)balancete.TiposLinhas.TransferenciaF;
                    break;
                
            };
            */
        }

        private void StatusBAL_Properties_ButtonClick(object sender, DevExpress.XtraEditors.Controls.ButtonPressedEventArgs e)
        {
            if (e.Button.Kind == DevExpress.XtraEditors.Controls.ButtonPredefines.Left)
            {
                if (balancete1.SalvoXML)
                {
                    if (MessageBox.Show("O balancete já foi fechado definitivo.\r\nConfirma a reabertura?", "Confirmação", MessageBoxButtons.YesNo, MessageBoxIcon.Warning, MessageBoxDefaultButton.Button2) == DialogResult.Yes)
                    {
                        string Justificativa = string.Empty;
                        if (VirInput.Input.Execute("Justificativa", ref Justificativa, true))
                            if (balancete1.DBalancete1.BALancetesTableAdapter.FillByBAL(balancete1.DBalancete1.BALancetes, balancete1.BALrow.BAL) == 1)
                            {
                                if (((BALStatus)balancete1.DBalancete1.BALancetes[0].BALSTATUS).EstaNoGrupo(BALStatus.FechadoDefinitivo, BALStatus.Enviado))
                                {
                                    balancete1.DBalancete1.BALancetes[0].BALObs += string.Format("\r\n******** BALANCETE REABERTO ********\r\n{0} - {1} Justivicativa:{2}\r\n",
                                                                                                  DateTime.Now,
                                                                                                  Framework.usuarioLogado.UsuarioLogadoNeon.UsuarioLogadoStNEON.USUNome,
                                                                                                  Justificativa);
                                    balancete1.DBalancete1.BALancetes[0].BALSTATUS = (int)BALStatus.Termindo;
                                    balancete1.DBalancete1.BALancetesTableAdapter.Update(balancete1.DBalancete1.BALancetes[0]);
                                    MessageBox.Show("Reaberto. Carregar novamente");

                                }
                                FechaTela(DialogResult.OK);
                                return;
                            }
                    }
                }
                else
                    if (balancete1.MudaStatus(balancete.DirecaoStatus.Retorna, this))
                    StatusBAL.EditValue = (int)balancete1.Status;
            }
            else if (e.Button.Kind == DevExpress.XtraEditors.Controls.ButtonPredefines.Right)
            {
                if (balancete1.SalvoXML)
                {
                    if (balancete1.DBalancete1.BALancetesTableAdapter.FillByBAL(balancete1.DBalancete1.BALancetes, balancete1.BALrow.BAL) == 1)
                    {
                        if (((BALStatus)balancete1.DBalancete1.BALancetes[0].BALSTATUS).EstaNoGrupo(BALStatus.FechadoDefinitivo))
                        {
                            balancete1.DBalancete1.BALancetes[0].BALObs += string.Format("\r\n******** Marcado como ENVIADO manualmente ********\r\n{0} - {1}\r\n",
                                                                                          DateTime.Now,
                                                                                          Framework.usuarioLogado.UsuarioLogadoNeon.UsuarioLogadoStNEON.USUNome);
                            balancete1.DBalancete1.BALancetes[0].BALSTATUS = (int)BALStatus.Enviado;
                            balancete1.DBalancete1.BALancetesTableAdapter.Update(balancete1.DBalancete1.BALancetes[0]);
                            };
                        FechaTela(DialogResult.OK);
                        return;
                    }

                }
                balancete1.CarregarDadosBalancete(balancete1.AgrupamentoUsarBalancete, false);
                if (balancete1.MudaStatus(balancete.DirecaoStatus.Avante, this))
                {
                    StatusBAL.EditValue = (int)balancete1.Status;
                    if (balancete1.Status == BALStatus.FechadoDefinitivo)
                    {
                        //TiraDoPool();
                        FechaTela(DialogResult.OK);
                        return;
                    }
                }
                else
                    if (balancete1.UltimoErro != null)
                    MessageBox.Show(string.Format("Erro: {0}", balancete1.UltimoErro.Message));
                else
                    MessageBox.Show(string.Format("Erro na validação"));
            };
            ChecaEditavel();
            TabErroVisivel();
        }

        private bool TabErroVisivel()
        {
            return xtraTabPageErro.PageVisible = balancete1.DBalancete1.ErrosAvisos.Count > 0;
        }

        private bool TemCampoAlterado(dBalancete.TabelaVirtualRow row)
        {
            foreach (string CampoTeste in new string[] { "DescBalanceteParcial", "PLA", "Valor", "CTL" })
                if ((row[CampoTeste, DataRowVersion.Current] != row[CampoTeste, DataRowVersion.Original]) && (row[CampoTeste, DataRowVersion.Current] != DBNull.Value))
                    return true;
            return false;
        }

        private void TextEditCorretor70_CustomDisplayText(object sender, DevExpress.XtraEditors.Controls.CustomDisplayTextEventArgs e)
        {

            try
            {
                e.DisplayText = sender.ToString() + (string)e.Value;
            }
            catch
            {
            }
        }


        private void xtraTabControl1_SelectedPageChanged(object sender, DevExpress.XtraTab.TabPageChangedEventArgs e)
        {
            using (CompontesBasicos.Espera.cEspera ESP = new CompontesBasicos.Espera.cEspera(this))
            {
                Application.DoEvents();
                if (e.Page == xtraTabPageErro)
                    balancete1.Validado();
                else if ((e.Page == xtraTabPageCreditos)
                    ||
                    (e.Page == xtraTabPageDebitos)
                    ||
                    (e.Page == xtraTabPageCCT))
                {
                    //AgrupadorBalancete.solicitaAgrupamento solicitacao;
                    //if (lookMBA.EditValue == null)
                    //    solicitacao = new AgrupadorBalancete.solicitaAgrupamento(AgrupadorBalancete.TipoClassificacaoDesp.PlanoContas);
                    //else
                    //    solicitacao = new AgrupadorBalancete.solicitaAgrupamento(AgrupadorBalancete.TipoClassificacaoDesp.CadastroMBA,(int)lookMBA.EditValue);
                    balancete1.CarregarDadosBalancete(AgrupamentoUsarBalancete, false);
                    dCreditosBindingSource.DataSource = balancete1.DCreditos1;
                }
                else if ((e.Page == xtraTabPageDiario) && (e.PrevPage == xtraTabPageIndividual))
                {
                    if (gridView2.FocusedRowHandle >= 0)
                    {
                        dBalancete.TabelaVirtualRow row = (dBalancete.TabelaVirtualRow)(gridView2.GetFocusedDataRow());
                        for (int i = 0; i < bandedGridView1.RowCount; i++)
                            if (((dBalancete.TabelaVirtualRow)(bandedGridView1.GetDataRow(i))).Ordem == row.Ordem)
                            {
                                bandedGridView1.FocusedRowHandle = i;
                                break;
                            }
                    }
                }
                else if ((e.Page == xtraTabPageDiario) && (e.PrevPage == xtraTabPageErro))
                {
                    if (gridView9.FocusedRowHandle >= 0)
                    {
                        dBalancete.ErrosAvisosRow row = (dBalancete.ErrosAvisosRow)(gridView9.GetFocusedDataRow());
                        if (row != null)
                            for (int i = 0; i < bandedGridView1.RowCount; i++)
                                if (!row.IsOrdemNull())
                                    if (((dBalancete.TabelaVirtualRow)(bandedGridView1.GetDataRow(i))).Ordem == row.Ordem)
                                    {
                                        bandedGridView1.FocusedRowHandle = i;
                                        break;
                                    }
                    }
                }
                else if ((e.Page == xtraTabPageIndividual) && (e.PrevPage == xtraTabPageDiario))
                {
                    if (bandedGridView1.FocusedRowHandle >= 0)
                    {
                        dBalancete.TabelaVirtualRow row = (dBalancete.TabelaVirtualRow)(bandedGridView1.GetFocusedDataRow());
                        for (int i = 0; i < gridView2.RowCount; i++)
                            if (((dBalancete.TabelaVirtualRow)(gridView2.GetDataRow(i))).Ordem == row.Ordem)
                            {
                                gridView2.FocusedRowHandle = i;
                                break;
                            }
                    }
                }
                else if ((e.Page == xtraTabPageIndividual) && (e.PrevPage == xtraTabPageErro))
                {
                    if (gridView9.FocusedRowHandle >= 0)
                    {
                        dBalancete.ErrosAvisosRow row = (dBalancete.ErrosAvisosRow)(gridView9.GetFocusedDataRow());
                        for (int i = 0; i < gridView2.RowCount; i++)
                            if (!row.IsOrdemNull())
                                if (((dBalancete.TabelaVirtualRow)(gridView2.GetDataRow(i))).Ordem == row.Ordem)
                                {
                                    gridView2.FocusedRowHandle = i;
                                    break;
                                }
                    }
                }
                else if (e.Page == xtraTabPagePrevisto)
                    previstoBindingSource.DataSource = balancete1.DPrevisto1;
                else if (e.Page == xtraTabPagePGF)
                {
                    CriarColunasPGF();
                    balancete1.CarregaDadosPGF();
                    pagamentosBindingSource.DataSource = balancete1.DPeriodicos1;
                }
                else if (e.Page == xtraTabPageOBS)
                    LeOBS();
                if (e.PrevPage == xtraTabPageOBS)
                    SalvarOBS();
                if (e.Page == xtraTabPageInad)
                {
                    inadimpBindingSource.DataSource = balancete1.DInadimplencia;
                    if (balancete1.DInadimplencia.Inadimp.Count == 0)
                    {
                        balancete1.CarregaInad();
                        foreach(string PLA in balancete1.DInadimplencia.ColunasDinamicasInad.Keys)
                        CriaColunasDinamicasInad(balancete1.DInadimplencia.ColunasDinamicasInad[PLA],PLA,true);
                    }
                }
            }
        }

        private void CriaColunasDinamicasInad(string Titulo, string strNomeCampo, bool visivel = true)
        {
            //if (NovasColunas == null)
            //{
            //    NovasColunas = new SortedList<string, DevExpress.XtraGrid.Columns.GridColumn>();
            //    SubTotais = new List<DevExpress.XtraGrid.GridGroupSummaryItem>();
            //}
            DevExpress.XtraGrid.Columns.GridColumn Coluna = new DevExpress.XtraGrid.Columns.GridColumn();
            Coluna.AppearanceCell.BackColor = Color.Beige;
            Coluna.AppearanceCell.Options.UseBackColor = true;
            Coluna.Caption = Titulo;
            Coluna.CustomizationCaption = Titulo;
            Coluna.Name = strNomeCampo;
            Coluna.FieldName = strNomeCampo;
            Coluna.Width = 80;
            Coluna.OptionsColumn.FixedWidth = true;
            Coluna.OptionsColumn.AllowMerge = DevExpress.Utils.DefaultBoolean.False;
            Coluna.OptionsColumn.ReadOnly = true;
            //Coluna.ColumnEdit = repValor;
            Coluna.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
                                                new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum, strNomeCampo, "{0:n2}")});
            Coluna.Visible = visivel;
            gridViewInad.Columns.Add(Coluna);
            DevExpress.XtraGrid.GridGroupSummaryItem SubTotal = new DevExpress.XtraGrid.GridGroupSummaryItem(DevExpress.Data.SummaryItemType.Sum, strNomeCampo, Coluna, "{0:n2}");
            gridViewInad.GroupSummary.Add(SubTotal);
            //NovasColunas.Add(strNomeCampo, Coluna);
            //SubTotais.Add(SubTotal);
        }

        private void xtraTabControl2_SelectedPageChanged(object sender, DevExpress.XtraTab.TabPageChangedEventArgs e)
        {
            if ((e.Page == xtraTabCredDetalhes)
                ||
                (e.Page == xtraTabCredGrafico))
                CriaColunas_GridBolRec();
        }




        private void zoomTrackBarControl1_EditValueChanged(object sender, EventArgs e)
        {
            _FonteForte = null;
            float fontSize;
            fontSize = Convert.ToInt32(zoomTrackBarControl1.EditValue);
            Font fnt = new Font(gridView1.Appearance.Row.Font.Name, fontSize);
            gridView1.Appearance.HeaderPanel.Font = fnt;
            gridView1.Appearance.Row.Font = fnt;
            gridView2.Appearance.HeaderPanel.Font = fnt;
            gridView2.Appearance.Row.Font = fnt;
            gridView3.Appearance.HeaderPanel.Font = fnt;
            gridView3.Appearance.Row.Font = fnt;
            gridView4.Appearance.HeaderPanel.Font = fnt;
            gridView4.Appearance.Row.Font = fnt;
            gridView5.Appearance.HeaderPanel.Font = fnt;
            gridView5.Appearance.Row.Font = fnt;
            gridView6.Appearance.HeaderPanel.Font = fnt;
            gridView6.Appearance.Row.Font = fnt;
            gridView7.Appearance.HeaderPanel.Font = fnt;
            gridView7.Appearance.Row.Font = fnt;
            bandedGridView1.Appearance.HeaderPanel.Font = fnt;
            bandedGridView1.Appearance.Row.Font = fnt;
            bandedGridView2.Appearance.HeaderPanel.Font = fnt;
            bandedGridView2.Appearance.Row.Font = fnt;
            bandedGridView3.Appearance.HeaderPanel.Font = fnt;
            bandedGridView3.Appearance.Row.Font = fnt;
        }

        private AgrupadorBalancete.solicitaAgrupamento AgrupamentoUsarBalancete
        {
            get
            {
                if (lookMBA.EditValue == null)
                    return balancete1.AgrupamentoUsarBalancete;
                else
                    return new AgrupadorBalancete.solicitaAgrupamento(AgrupadorBalancete.TipoClassificacaoDesp.CadastroMBA, (int)lookMBA.EditValue);
            }
        }


        private AgrupadorBalancete.solicitaAgrupamento AgrupamentoUsarBoleto
        {
            get
            {
                if (lookMBA.EditValue == null)
                    return balancete1.AgrupamentoUsarBoleto;
                else
                    return new AgrupadorBalancete.solicitaAgrupamento(AgrupadorBalancete.TipoClassificacaoDesp.CadastroMBA, (int)lookMBA.EditValue);
            }
        }

        enum debcred { debito, credito, transfL, nula }


        #region Construtores

        /// <summary>
        /// Padrão
        /// </summary>
        public cBalancete()
        {
            InitializeComponent();
        }


        /// <summary>
        /// Construtor
        /// </summary>
        /// <param name="_balancete"></param>
        public cBalancete(balancete _balancete):this()
        {            
            balancete1 = _balancete;            
            balancete1.VirEnumTpDataNoPeriodo.CarregaEditorDaGrid(colcalcGrupo);
            Enumeracoes.virEnumstatusconsiliado.virEnumstatusconsiliadoSt.CarregaEditorDaGrid(colCCDConsolidado);
            Enumeracoes.virEnumTipoLancamentoNeon.virEnumTipoLancamentoNeonSt.CarregaEditorDaGrid(colCCDTipoLancamento);
            Enumeracoes.virEnumBALStatus.virEnumBALStatusSt.CarregaEditorDaGrid(StatusBAL);
            Enumeracoes.virEnumCHEStatus.virEnumCHEStatusSt.CarregaEditorDaGrid(colCHEStatus);
            radioGroup1.Properties.Items.Clear();
            radioGroup1.Properties.Items.AddEnum(typeof(enumOBBTipo));            

            balancete1.VirEnumClassePizza.CarregaEditorDaGrid(colClaGrafico);

            pLAnocontasBindingSource.DataSource = Framework.datasets.dPLAnocontas.dPLAnocontasSt;
            modeloBAlanceteBindingSource.DataSource = Framework.datasets.dModelosBAlancete.dModelosBAlanceteSt;

            if (balancete1.encontrado)
            {
                StatusBAL.EditValue = (int)balancete1.Status;


                CarregarDados(false);
            }

            if (CompontesBasicos.FormPrincipalBase.USULogado == 30)            
                panelDesenvolvedor.Visible = true;                
            
            ChecaEditavel();
        }


        #endregion

        private void simpleButton9_Click(object sender, EventArgs e)
        {
            Comprovantes(true);            
        }

        private void Comprovantes(bool Imprimir)
        {
            SortedList<decimal, SortedList<int, ABS_Cheque>> CHEporValor = new SortedList<decimal, SortedList<int, ABS_Cheque>>();
            foreach (dBalancete.TabelaVirtualRow row in balancete1.DBalancete1.TabelaVirtual)
                if (!row.IsCHENull())
                {
                    ABS_Cheque Cheque = ABS_Cheque.GetCheque(row.CHE);
                    if (!CHEporValor.ContainsKey(Cheque.Valor))
                        CHEporValor.Add(Cheque.Valor, new SortedList<int, ABS_Cheque>());
                    
                    if (!CHEporValor[Cheque.Valor].ContainsKey(row.CHE))
                        CHEporValor[Cheque.Valor].Add(row.CHE,Cheque);
                    
                }
            List<ABS_Cheque> CHEs = new List<ABS_Cheque>();
            foreach (SortedList<int, ABS_Cheque> L in CHEporValor.Values)
                CHEs.AddRange(L.Values);

            string NomeArquivoPDF = ABS_Cheque.MergeComprovantes(CHEs);
            if (NomeArquivoPDF != null)
            {
                if (Imprimir)
                    using (PdfDocumentProcessor processor = new PdfDocumentProcessor())
                    {
                        processor.LoadDocument(NomeArquivoPDF);
                        PdfPrinterSettings pdfPrinterSettings = new PdfPrinterSettings();
                        processor.Print(pdfPrinterSettings);
                        DSCentral.IMPressoesTableAdapter.Incluir(balancete1.CON, 159, processor.Document.Pages.Count, DateTime.Now, CompontesBasicos.Bancovirtual.UsuarioLogado.UsuarioLogadoSt.USU, "Comprovantes - Balancete", false);
                    }
                else
                    System.Diagnostics.Process.Start(NomeArquivoPDF);                               
            }
        }

        private void barButtonItemImprimir_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            Comprovantes(true);
        }

        private void barButtonItemTela_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            Comprovantes(false);
        }

        private void repositoryItemLookUpEditCTL_ButtonClick(object sender, DevExpress.XtraEditors.Controls.ButtonPressedEventArgs e)
        {
            if (e.Button.Kind == DevExpress.XtraEditors.Controls.ButtonPredefines.Clear)
            {
                LookUpEdit Botao = (LookUpEdit)sender;
                
                dBalancete.TabelaVirtualRow row = Botao.Name == "repositoryItemLookUpEditCTL" ? (dBalancete.TabelaVirtualRow)bandedGridView1.GetFocusedDataRow() : (dBalancete.TabelaVirtualRow)gridView2.GetFocusedDataRow();
                if (row == null)
                    return;
                row.SetCTLNull();
                Botao.ClosePopup();
                Botao.EditValue = null;
            }
        }

        private void BotaoTeste_Click(object sender, EventArgs e)
        {
            MessageBox.Show("teste");
        }
    }
}
