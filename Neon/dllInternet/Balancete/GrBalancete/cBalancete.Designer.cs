﻿namespace Balancete.GrBalancete
{
    /// <summary>
    /// Componente para o balancete
    /// </summary>
    partial class cBalancete
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            DevExpress.XtraGrid.GridLevelNode gridLevelNode1 = new DevExpress.XtraGrid.GridLevelNode();
            DevExpress.XtraGrid.GridLevelNode gridLevelNode2 = new DevExpress.XtraGrid.GridLevelNode();
            DevExpress.XtraEditors.Controls.EditorButtonImageOptions editorButtonImageOptions1 = new DevExpress.XtraEditors.Controls.EditorButtonImageOptions();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(cBalancete));
            DevExpress.XtraEditors.Controls.EditorButtonImageOptions editorButtonImageOptions2 = new DevExpress.XtraEditors.Controls.EditorButtonImageOptions();
            DevExpress.XtraEditors.Controls.EditorButtonImageOptions editorButtonImageOptions13 = new DevExpress.XtraEditors.Controls.EditorButtonImageOptions();
            DevExpress.XtraEditors.Controls.EditorButtonImageOptions editorButtonImageOptions14 = new DevExpress.XtraEditors.Controls.EditorButtonImageOptions();
            DevExpress.XtraEditors.Controls.EditorButtonImageOptions editorButtonImageOptions3 = new DevExpress.XtraEditors.Controls.EditorButtonImageOptions();
            DevExpress.XtraEditors.Controls.EditorButtonImageOptions editorButtonImageOptions4 = new DevExpress.XtraEditors.Controls.EditorButtonImageOptions();
            DevExpress.XtraEditors.Controls.EditorButtonImageOptions editorButtonImageOptions5 = new DevExpress.XtraEditors.Controls.EditorButtonImageOptions();
            DevExpress.XtraEditors.Controls.EditorButtonImageOptions editorButtonImageOptions6 = new DevExpress.XtraEditors.Controls.EditorButtonImageOptions();
            DevExpress.XtraEditors.Controls.EditorButtonImageOptions editorButtonImageOptions7 = new DevExpress.XtraEditors.Controls.EditorButtonImageOptions();
            DevExpress.XtraEditors.Controls.EditorButtonImageOptions editorButtonImageOptions8 = new DevExpress.XtraEditors.Controls.EditorButtonImageOptions();
            DevExpress.XtraEditors.Controls.EditorButtonImageOptions editorButtonImageOptions9 = new DevExpress.XtraEditors.Controls.EditorButtonImageOptions();
            DevExpress.XtraEditors.Controls.EditorButtonImageOptions editorButtonImageOptions10 = new DevExpress.XtraEditors.Controls.EditorButtonImageOptions();
            DevExpress.XtraEditors.Controls.EditorButtonImageOptions editorButtonImageOptions11 = new DevExpress.XtraEditors.Controls.EditorButtonImageOptions();
            DevExpress.XtraCharts.SimpleDiagram3D simpleDiagram3D1 = new DevExpress.XtraCharts.SimpleDiagram3D();
            DevExpress.XtraCharts.Series series1 = new DevExpress.XtraCharts.Series();
            DevExpress.XtraCharts.Pie3DSeriesLabel pie3DSeriesLabel1 = new DevExpress.XtraCharts.Pie3DSeriesLabel();
            DevExpress.XtraCharts.Pie3DSeriesView pie3DSeriesView1 = new DevExpress.XtraCharts.Pie3DSeriesView();
            DevExpress.XtraCharts.Pie3DSeriesLabel pie3DSeriesLabel2 = new DevExpress.XtraCharts.Pie3DSeriesLabel();
            DevExpress.XtraCharts.Pie3DSeriesView pie3DSeriesView2 = new DevExpress.XtraCharts.Pie3DSeriesView();
            DevExpress.XtraEditors.Controls.EditorButtonImageOptions editorButtonImageOptions12 = new DevExpress.XtraEditors.Controls.EditorButtonImageOptions();
            this.gridView4 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colBloco = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colNumero = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colBoleto = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colCompetencia = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colDescricao3 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colTipo2 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colValor2 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.CalcEdit_n2 = new DevExpress.XtraEditors.Repository.RepositoryItemCalcEdit();
            this.colN = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colBotaoBoleto1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridControl4 = new DevExpress.XtraGrid.GridControl();
            this.dCreditosBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.gridView3 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colDescricao2 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colAtrasado = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colPeriodo = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colAntecipado = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colTotal = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colT1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colT2 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colT3 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colOrdem2 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colPrevisto = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colPLA2 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSoPrevisto = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.BotDesc = new DevExpress.XtraEditors.Repository.RepositoryItemButtonEdit();
            this.repBotaoBoleto1 = new DevExpress.XtraEditors.Repository.RepositoryItemButtonEdit();
            this.bandedGridView3 = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridView();
            this.gridBand7 = new DevExpress.XtraGrid.Views.BandedGrid.GridBand();
            this.colNOA = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.colNOACompet = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.colNOANumero = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.bandedGridColumn4 = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.BOTNota = new DevExpress.XtraEditors.Repository.RepositoryItemButtonEdit();
            this.gridBand8 = new DevExpress.XtraGrid.Views.BandedGrid.GridBand();
            this.colPAG2 = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.colPAGValor = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.colPAGVencimento = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.gridBand5 = new DevExpress.XtraGrid.Views.BandedGrid.GridBand();
            this.colCHE = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.colCHENumero = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.colCHEStatus = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.repositoryItemImageComboBox3 = new DevExpress.XtraEditors.Repository.RepositoryItemImageComboBox();
            this.colCHEValor = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.colCHEVencimento = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.colBotCHE = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.gridBand6 = new DevExpress.XtraGrid.Views.BandedGrid.GridBand();
            this.colSCCData = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.gridBand10 = new DevExpress.XtraGrid.Views.BandedGrid.GridBand();
            this.colStatus1 = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.repositoryItemImageComboBox4 = new DevExpress.XtraEditors.Repository.RepositoryItemImageComboBox();
            this.gridControl9 = new DevExpress.XtraGrid.GridControl();
            this.pagamentosBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.bandedGridView2 = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridView();
            this.gridBand4 = new DevExpress.XtraGrid.Views.BandedGrid.GridBand();
            this.colPGF = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.colPGFDia = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.colPGFDescricao = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.colPGFValor = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.repositoryItemCalcEdit4 = new DevExpress.XtraEditors.Repository.RepositoryItemCalcEdit();
            this.colPGFCompetI = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.colPGFCompetF = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.colFRNNome = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.colFRNFantasia = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.colPGFForma = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.colPGFNominal = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.colPGFRef = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.colPGFProximaCompetencia = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.gridBandCompet = new DevExpress.XtraGrid.Views.BandedGrid.GridBand();
            this.colPGF0 = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.gridBand9 = new DevExpress.XtraGrid.Views.BandedGrid.GridBand();
            this.colBotPGF = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.BotPGF = new DevExpress.XtraEditors.Repository.RepositoryItemButtonEdit();
            this.BotCHE = new DevExpress.XtraEditors.Repository.RepositoryItemButtonEdit();
            this.dBalanceteBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.panelControl1 = new DevExpress.XtraEditors.PanelControl();
            this.chCPMF = new DevExpress.XtraEditors.CheckEdit();
            this.dropDownButton1 = new DevExpress.XtraEditors.DropDownButton();
            this.popupComprovantes = new DevExpress.XtraBars.PopupMenu(this.components);
            this.barButtonItemImprimir = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonItemTela = new DevExpress.XtraBars.BarButtonItem();
            this.barManager1 = new DevExpress.XtraBars.BarManager(this.components);
            this.barDockControlTop = new DevExpress.XtraBars.BarDockControl();
            this.barDockControlBottom = new DevExpress.XtraBars.BarDockControl();
            this.barDockControlLeft = new DevExpress.XtraBars.BarDockControl();
            this.barDockControlRight = new DevExpress.XtraBars.BarDockControl();
            this.chInadDet = new DevExpress.XtraEditors.CheckEdit();
            this.simpleButton8 = new DevExpress.XtraEditors.SimpleButton();
            this.simpleButton7 = new DevExpress.XtraEditors.SimpleButton();
            this.chCapa = new DevExpress.XtraEditors.CheckEdit();
            this.checkEdit1 = new DevExpress.XtraEditors.CheckEdit();
            this.cHDicionario = new DevExpress.XtraEditors.CheckEdit();
            this.simpleButton2 = new DevExpress.XtraEditors.SimpleButton();
            this.lookMBA = new DevExpress.XtraEditors.LookUpEdit();
            this.modeloBAlanceteBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.labelControl1 = new DevExpress.XtraEditors.LabelControl();
            this.StatusBAL = new DevExpress.XtraEditors.ImageComboBoxEdit();
            this.cBotaoImp1 = new VirBotaoNeon.cBotaoImpNeon();
            this.checkTF = new DevExpress.XtraEditors.CheckEdit();
            this.chExtratos = new DevExpress.XtraEditors.CheckEdit();
            this.chInadimplencia = new DevExpress.XtraEditors.CheckEdit();
            this.chPrevisto = new DevExpress.XtraEditors.CheckEdit();
            this.chCreditosAntigo = new DevExpress.XtraEditors.CheckEdit();
            this.chSaldos = new DevExpress.XtraEditors.CheckEdit();
            this.chDetalhamentoCreditos = new DevExpress.XtraEditors.CheckEdit();
            this.chDebitosCreditos = new DevExpress.XtraEditors.CheckEdit();
            this.simpleButton6 = new DevExpress.XtraEditors.SimpleButton();
            this.simpleButton4 = new DevExpress.XtraEditors.SimpleButton();
            this.simpleButton3 = new DevExpress.XtraEditors.SimpleButton();
            this.cBotaoImpBol1 = new dllImpresso.Botoes.cBotaoImpBol();
            this.simpleButton1 = new DevExpress.XtraEditors.SimpleButton();
            this.zoomTrackBarControl1 = new DevExpress.XtraEditors.ZoomTrackBarControl();
            this.gridControl1 = new DevExpress.XtraGrid.GridControl();
            this.bandedGridView1 = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridView();
            this.gridBand2 = new DevExpress.XtraGrid.Views.BandedGrid.GridBand();
            this.bandedGridAgrupa = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.repositoryNULO = new DevExpress.XtraEditors.Repository.RepositoryItemButtonEdit();
            this.colData = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.bandedGridColumn1 = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.BotPlus = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.BotChe1 = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.BotNoa1 = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.BotPGF1 = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.colErro = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.repositoryItemMemoExEdit1 = new DevExpress.XtraEditors.Repository.RepositoryItemMemoExEdit();
            this.imageCollection1 = new DevExpress.Utils.ImageCollection(this.components);
            this.gridBand1 = new DevExpress.XtraGrid.Views.BandedGrid.GridBand();
            this.colOrdem = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.colTipo = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.IMGTipos = new DevExpress.XtraEditors.Repository.RepositoryItemImageComboBox();
            this.imageCollection2 = new DevExpress.Utils.ImageCollection(this.components);
            this.colDescricao = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.colDescBalancete = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.TextEditCorretor70 = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.colCCDValor = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.repositoryItemCalcEdit1 = new DevExpress.XtraEditors.Repository.RepositoryItemCalcEdit();
            this.colCCDConsolidado = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.repositoryItemImageComboBox1 = new DevExpress.XtraEditors.Repository.RepositoryItemImageComboBox();
            this.colCCDTipoLancamento = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.repositoryItemImageComboBox2 = new DevExpress.XtraEditors.Repository.RepositoryItemImageComboBox();
            this.gridBand3 = new DevExpress.XtraGrid.Views.BandedGrid.GridBand();
            this.colPLA = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.repositoryItemLookUpPLA = new DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit();
            this.pLAnocontasBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.colCTL = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.repositoryItemLookUpEditCTL = new DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit();
            this.conTasLogicasBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.colCTL21 = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.colValor = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.gridBandL = new DevExpress.XtraGrid.Views.BandedGrid.GridBand();
            this.gridBandF = new DevExpress.XtraGrid.Views.BandedGrid.GridBand();
            this.colCCT = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.colCCD = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.bandedGridColumn2 = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.colDocumento = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.colCCS = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.colCCSSoma = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.colPAG1 = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.colMestre1 = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.colCHE1 = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.colNOA1 = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.colPGF1 = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.colErro2 = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.colDesp_Cred1 = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.repositoryItemButtonEdit1 = new DevExpress.XtraEditors.Repository.RepositoryItemButtonEdit();
            this.repositoryItemBotCHE = new DevExpress.XtraEditors.Repository.RepositoryItemButtonEdit();
            this.repositoryItemBotNoa = new DevExpress.XtraEditors.Repository.RepositoryItemButtonEdit();
            this.repositoryItemBotPGF = new DevExpress.XtraEditors.Repository.RepositoryItemButtonEdit();
            this.repositoryItemButtonPLUS = new DevExpress.XtraEditors.Repository.RepositoryItemButtonEdit();
            this.repositoryItemCheckEdit1 = new DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit();
            this.repositoryDelEst = new DevExpress.XtraEditors.Repository.RepositoryItemButtonEdit();
            this.xtraTabControl1 = new DevExpress.XtraTab.XtraTabControl();
            this.xtraTabPageDiario = new DevExpress.XtraTab.XtraTabPage();
            this.xtraTabPageIndividual = new DevExpress.XtraTab.XtraTabPage();
            this.gridControl3 = new DevExpress.XtraGrid.GridControl();
            this.tabelaVirtualBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.gridView2 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colOrdem1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colData1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colDescricao1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colDescBalancete1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryTextEdit70 = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.colValor1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.ItemCalcEdit = new DevExpress.XtraEditors.Repository.RepositoryItemCalcEdit();
            this.colPLA1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.lookPLA = new DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit();
            this.colTipo1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.IMGTipos1 = new DevExpress.XtraEditors.Repository.RepositoryItemImageComboBox();
            this.colCTL1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemLookUpEditCTL1 = new DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit();
            this.colCCT1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colCCDConsolidado1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colCCDTipoLancamento1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colCCD1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colCCDValor1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colErro1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemMemoExEdit1A = new DevExpress.XtraEditors.Repository.RepositoryItemMemoExEdit();
            this.colPAG = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colCCT2 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colCTL2 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colMestre = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colValorVenA = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colValorVenD = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colDocumento1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colDesp_Cred = new DevExpress.XtraGrid.Columns.GridColumn();
            this.ImageComboBox_D_C = new DevExpress.XtraEditors.Repository.RepositoryItemImageComboBox();
            this.imageCollectionDC = new DevExpress.Utils.ImageCollection(this.components);
            this.BotCHE2 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.BotNOA2 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.BotPGF2 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colAgrupar = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemButtonNulo = new DevExpress.XtraEditors.Repository.RepositoryItemButtonEdit();
            this.repositoryItemBotCHE2 = new DevExpress.XtraEditors.Repository.RepositoryItemButtonEdit();
            this.repositoryItemBotNOA2 = new DevExpress.XtraEditors.Repository.RepositoryItemButtonEdit();
            this.repositoryItemBotPGF2 = new DevExpress.XtraEditors.Repository.RepositoryItemButtonEdit();
            this.repositoryAgrupar = new DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit();
            this.repositoryDelEst1 = new DevExpress.XtraEditors.Repository.RepositoryItemButtonEdit();
            this.xtraTabPageCreditos = new DevExpress.XtraTab.XtraTabPage();
            this.xtraTabControlCredito = new DevExpress.XtraTab.XtraTabControl();
            this.xtraTabCredResumo = new DevExpress.XtraTab.XtraTabPage();
            this.xtraTabCredDetalhes = new DevExpress.XtraTab.XtraTabPage();
            this.gridControl2 = new DevExpress.XtraGrid.GridControl();
            this.gridView1 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colBOL = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colBOLValorPago = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repValor = new DevExpress.XtraEditors.Repository.RepositoryItemCalcEdit();
            this.colBOL_CCD = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colAPTNumero = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colBLOCodigo = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colBOLCompetenciaAno = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colBOLCompetenciaMes = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colBOLEmissao = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colBOLPagamento = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colBOLProprietario = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repPI = new DevExpress.XtraEditors.Repository.RepositoryItemImageComboBox();
            this.colBOLTipoCRAI = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colBOLVencto = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colBotaoBoleto = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colcalcGrupo = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repGrupos = new DevExpress.XtraEditors.Repository.RepositoryItemImageComboBox();
            this.colBOL2 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colcalcUnidade = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colClaGrafico = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repTipoGrafico = new DevExpress.XtraEditors.Repository.RepositoryItemImageComboBox();
            this.colValCredLiq = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colCCT_CON = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colErro3 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemMemoExEdit3 = new DevExpress.XtraEditors.Repository.RepositoryItemMemoExEdit();
            this.colCCT3 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repBotaoBoleto = new DevExpress.XtraEditors.Repository.RepositoryItemButtonEdit();
            this.xtraTabCredGrafico = new DevExpress.XtraTab.XtraTabPage();
            this.chartControl1 = new DevExpress.XtraCharts.ChartControl();
            this.gridControl11 = new DevExpress.XtraGrid.GridControl();
            this.gridView10 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colTitulo = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colValor4 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colClaGrafico1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.xtraTabPageDebitos = new DevExpress.XtraTab.XtraTabPage();
            this.gridControl6 = new DevExpress.XtraGrid.GridControl();
            this.gridView6 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colGrupo = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colDescricao4 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colValor3 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colOrdem3 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colPLA3 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSuperGrupo = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colnGrupo = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colnSuperGrupo = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colcalSG = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colcalG = new DevExpress.XtraGrid.Columns.GridColumn();
            this.xtraTabPageCTL = new DevExpress.XtraTab.XtraTabPage();
            this.gridControl5 = new DevExpress.XtraGrid.GridControl();
            this.gridView5 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colCTL3 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colCTLAtiva = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colCTLTitulo = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colCTLTipo = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colCTL_CCT = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSALDOI = new DevExpress.XtraGrid.Columns.GridColumn();
            this.CalcEditn2 = new DevExpress.XtraEditors.Repository.RepositoryItemCalcEdit();
            this.colSALDOF = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colCREDITOS = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colDEBITOS = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colTRANSFERENCIA = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colRENTABILIDADE = new DevExpress.XtraGrid.Columns.GridColumn();
            this.xtraTabPageCCT = new DevExpress.XtraTab.XtraTabPage();
            this.gridControl7 = new DevExpress.XtraGrid.GridControl();
            this.gridView7 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colDescricao5 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSaldoI1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemCalcEdit2 = new DevExpress.XtraEditors.Repository.RepositoryItemCalcEdit();
            this.colSaldoF1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.xtraTabPagePrevisto = new DevExpress.XtraTab.XtraTabPage();
            this.gridControl8 = new DevExpress.XtraGrid.GridControl();
            this.previstoBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.gridView8 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colBOL1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colBOLTipoCRAI1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colBOLVencto1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colBOLPagamento1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colBOLValorPrevisto = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemCalcEdit3 = new DevExpress.XtraEditors.Repository.RepositoryItemCalcEdit();
            this.colBOLValorPago1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colBOLCompetenciaAno1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colBOLCompetenciaMes1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colBOL_CCD1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colBOL_ARL = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colAPTNumero1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colBLOCodigo1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colBLOAPT = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colPI = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colBOLProprietario1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colValorPrincipal = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colMultas = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colStatus = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colAntecipado1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn2 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemButtonEdit3 = new DevExpress.XtraEditors.Repository.RepositoryItemButtonEdit();
            this.xtraTabPageInad = new DevExpress.XtraTab.XtraTabPage();
            this.gridControl12 = new DevExpress.XtraGrid.GridControl();
            this.inadimpBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.gridViewInad = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colBLOAPT1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colPI1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colBOLVencto2 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colBOLPagamento2 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colValAnterior = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colValFinal = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colValMes = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colValRec = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colAcordo = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colBOL3 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colBOLTipoCRAI2 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.xtraTabPagePGF = new DevExpress.XtraTab.XtraTabPage();
            this.panelControl3 = new DevExpress.XtraEditors.PanelControl();
            this.panelManual = new DevExpress.XtraEditors.PanelControl();
            this.labelManual = new DevExpress.XtraEditors.LabelControl();
            this.panelVencido = new DevExpress.XtraEditors.PanelControl();
            this.labelVencido = new DevExpress.XtraEditors.LabelControl();
            this.panelFuturo = new DevExpress.XtraEditors.PanelControl();
            this.labelFuturo = new DevExpress.XtraEditors.LabelControl();
            this.panelAgendado = new DevExpress.XtraEditors.PanelControl();
            this.labelAgendado = new DevExpress.XtraEditors.LabelControl();
            this.panelPago = new DevExpress.XtraEditors.PanelControl();
            this.labelPago = new DevExpress.XtraEditors.LabelControl();
            this.xtraTabPageOBS = new DevExpress.XtraTab.XtraTabPage();
            this.rtF_Editor1 = new RtfEditor.RTF_Editor();
            this.panelControl4 = new DevExpress.XtraEditors.PanelControl();
            this.radioGroup1 = new DevExpress.XtraEditors.RadioGroup();
            this.xtraTabPageErro = new DevExpress.XtraTab.XtraTabPage();
            this.gridControl10 = new DevExpress.XtraGrid.GridControl();
            this.gridView9 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colTipo3 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemErro = new DevExpress.XtraEditors.Repository.RepositoryItemImageComboBox();
            this.colNumero1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colDescricao6 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemMemoEdit1 = new DevExpress.XtraEditors.Repository.RepositoryItemMemoEdit();
            this.colData2 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colOrdem4 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemMemoExEdit2 = new DevExpress.XtraEditors.Repository.RepositoryItemMemoExEdit();
            this.panelControl2 = new DevExpress.XtraEditors.PanelControl();
            this.panelDesenvolvedor = new DevExpress.XtraEditors.PanelControl();
            this.BotaoTeste = new DevExpress.XtraEditors.SimpleButton();
            this.CCDRastrarVal = new DevExpress.XtraEditors.SpinEdit();
            this.labelControl6 = new DevExpress.XtraEditors.LabelControl();
            this.memoEdit1 = new DevExpress.XtraEditors.MemoEdit();
            this.simpleButton5 = new DevExpress.XtraEditors.SimpleButton();
            this.CCDRastrarAc = new DevExpress.XtraEditors.SpinEdit();
            this.labelControl5 = new DevExpress.XtraEditors.LabelControl();
            this.CCDRastrarConc = new DevExpress.XtraEditors.SpinEdit();
            this.labelControl4 = new DevExpress.XtraEditors.LabelControl();
            this.CCDRastrearCDB = new DevExpress.XtraEditors.SpinEdit();
            this.labelControl3 = new DevExpress.XtraEditors.LabelControl();
            this.CCDRastrarCarga = new DevExpress.XtraEditors.SpinEdit();
            this.labelControl2 = new DevExpress.XtraEditors.LabelControl();
            this.folderBrowserDialog1 = new System.Windows.Forms.FolderBrowserDialog();
            ((System.ComponentModel.ISupportInitialize)(this.StyleController_F)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.CalcEdit_n2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dCreditosBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.BotDesc)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repBotaoBoleto1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.bandedGridView3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.BOTNota)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemImageComboBox3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemImageComboBox4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl9)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pagamentosBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.bandedGridView2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCalcEdit4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.BotPGF)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.BotCHE)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dBalanceteBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl1)).BeginInit();
            this.panelControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.chCPMF.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.popupComprovantes)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.barManager1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chInadDet.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chCapa.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit1.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cHDicionario.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lookMBA.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.modeloBAlanceteBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.StatusBAL.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkTF.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chExtratos.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chInadimplencia.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chPrevisto.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chCreditosAntigo.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chSaldos.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chDetalhamentoCreditos.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chDebitosCreditos.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.zoomTrackBarControl1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.zoomTrackBarControl1.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.bandedGridView1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryNULO)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoExEdit1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.imageCollection1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.IMGTipos)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.imageCollection2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TextEditCorretor70)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCalcEdit1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemImageComboBox1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemImageComboBox2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemLookUpPLA)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pLAnocontasBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemLookUpEditCTL)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.conTasLogicasBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemButtonEdit1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemBotCHE)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemBotNoa)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemBotPGF)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemButtonPLUS)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckEdit1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryDelEst)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xtraTabControl1)).BeginInit();
            this.xtraTabControl1.SuspendLayout();
            this.xtraTabPageDiario.SuspendLayout();
            this.xtraTabPageIndividual.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tabelaVirtualBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryTextEdit70)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemCalcEdit)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lookPLA)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.IMGTipos1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemLookUpEditCTL1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoExEdit1A)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ImageComboBox_D_C)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.imageCollectionDC)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemButtonNulo)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemBotCHE2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemBotNOA2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemBotPGF2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryAgrupar)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryDelEst1)).BeginInit();
            this.xtraTabPageCreditos.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.xtraTabControlCredito)).BeginInit();
            this.xtraTabControlCredito.SuspendLayout();
            this.xtraTabCredResumo.SuspendLayout();
            this.xtraTabCredDetalhes.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repValor)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repPI)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repGrupos)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repTipoGrafico)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoExEdit3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repBotaoBoleto)).BeginInit();
            this.xtraTabCredGrafico.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.chartControl1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(simpleDiagram3D1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(series1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(pie3DSeriesLabel1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(pie3DSeriesView1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(pie3DSeriesLabel2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(pie3DSeriesView2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl11)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView10)).BeginInit();
            this.xtraTabPageDebitos.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView6)).BeginInit();
            this.xtraTabPageCTL.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.CalcEditn2)).BeginInit();
            this.xtraTabPageCCT.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCalcEdit2)).BeginInit();
            this.xtraTabPagePrevisto.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl8)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.previstoBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView8)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCalcEdit3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemButtonEdit3)).BeginInit();
            this.xtraTabPageInad.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl12)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.inadimpBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridViewInad)).BeginInit();
            this.xtraTabPagePGF.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl3)).BeginInit();
            this.panelControl3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.panelManual)).BeginInit();
            this.panelManual.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.panelVencido)).BeginInit();
            this.panelVencido.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.panelFuturo)).BeginInit();
            this.panelFuturo.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.panelAgendado)).BeginInit();
            this.panelAgendado.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.panelPago)).BeginInit();
            this.panelPago.SuspendLayout();
            this.xtraTabPageOBS.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl4)).BeginInit();
            this.panelControl4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.radioGroup1.Properties)).BeginInit();
            this.xtraTabPageErro.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl10)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView9)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemErro)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoEdit1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoExEdit2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl2)).BeginInit();
            this.panelControl2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.panelDesenvolvedor)).BeginInit();
            this.panelDesenvolvedor.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.CCDRastrarVal.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.memoEdit1.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.CCDRastrarAc.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.CCDRastrarConc.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.CCDRastrearCDB.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.CCDRastrarCarga.Properties)).BeginInit();
            this.SuspendLayout();
            // 
            // ToolTipController_F
            // 
            this.ToolTipController_F.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.ToolTipController_F.Appearance.ForeColor = System.Drawing.Color.Black;
            this.ToolTipController_F.Appearance.Options.UseBackColor = true;
            this.ToolTipController_F.Appearance.Options.UseFont = true;
            this.ToolTipController_F.Appearance.Options.UseForeColor = true;
            this.ToolTipController_F.Appearance.Options.UseTextOptions = true;
            this.ToolTipController_F.Appearance.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            // 
            // StyleController_F
            // 
            this.StyleController_F.AppearanceDisabled.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this.StyleController_F.AppearanceDisabled.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this.StyleController_F.AppearanceDisabled.Options.UseBackColor = true;
            this.StyleController_F.AppearanceDisabled.Options.UseForeColor = true;
            this.StyleController_F.AppearanceFocused.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.StyleController_F.AppearanceFocused.Options.UseBackColor = true;
            // 
            // gridView4
            // 
            this.gridView4.Appearance.ColumnFilterButton.BackColor = System.Drawing.Color.Orange;
            this.gridView4.Appearance.ColumnFilterButton.BorderColor = System.Drawing.Color.Orange;
            this.gridView4.Appearance.ColumnFilterButton.ForeColor = System.Drawing.Color.DarkOrange;
            this.gridView4.Appearance.ColumnFilterButton.Options.UseBackColor = true;
            this.gridView4.Appearance.ColumnFilterButton.Options.UseBorderColor = true;
            this.gridView4.Appearance.ColumnFilterButton.Options.UseForeColor = true;
            this.gridView4.Appearance.ColumnFilterButtonActive.BackColor = System.Drawing.Color.DarkOrange;
            this.gridView4.Appearance.ColumnFilterButtonActive.BorderColor = System.Drawing.Color.DarkOrange;
            this.gridView4.Appearance.ColumnFilterButtonActive.ForeColor = System.Drawing.Color.White;
            this.gridView4.Appearance.ColumnFilterButtonActive.Options.UseBackColor = true;
            this.gridView4.Appearance.ColumnFilterButtonActive.Options.UseBorderColor = true;
            this.gridView4.Appearance.ColumnFilterButtonActive.Options.UseForeColor = true;
            this.gridView4.Appearance.Empty.BackColor = System.Drawing.Color.LightSkyBlue;
            this.gridView4.Appearance.Empty.BackColor2 = System.Drawing.Color.SkyBlue;
            this.gridView4.Appearance.Empty.GradientMode = System.Drawing.Drawing2D.LinearGradientMode.BackwardDiagonal;
            this.gridView4.Appearance.Empty.Options.UseBackColor = true;
            this.gridView4.Appearance.EvenRow.BackColor = System.Drawing.Color.Linen;
            this.gridView4.Appearance.EvenRow.BackColor2 = System.Drawing.Color.AntiqueWhite;
            this.gridView4.Appearance.EvenRow.GradientMode = System.Drawing.Drawing2D.LinearGradientMode.ForwardDiagonal;
            this.gridView4.Appearance.EvenRow.Options.UseBackColor = true;
            this.gridView4.Appearance.FilterCloseButton.BackColor = System.Drawing.Color.Orange;
            this.gridView4.Appearance.FilterCloseButton.BorderColor = System.Drawing.Color.Orange;
            this.gridView4.Appearance.FilterCloseButton.Options.UseBackColor = true;
            this.gridView4.Appearance.FilterCloseButton.Options.UseBorderColor = true;
            this.gridView4.Appearance.FilterPanel.BackColor = System.Drawing.Color.DarkOrange;
            this.gridView4.Appearance.FilterPanel.BackColor2 = System.Drawing.Color.Orange;
            this.gridView4.Appearance.FilterPanel.ForeColor = System.Drawing.Color.Black;
            this.gridView4.Appearance.FilterPanel.GradientMode = System.Drawing.Drawing2D.LinearGradientMode.BackwardDiagonal;
            this.gridView4.Appearance.FilterPanel.Options.UseBackColor = true;
            this.gridView4.Appearance.FilterPanel.Options.UseForeColor = true;
            this.gridView4.Appearance.FocusedRow.BackColor = System.Drawing.Color.RoyalBlue;
            this.gridView4.Appearance.FocusedRow.BackColor2 = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this.gridView4.Appearance.FocusedRow.ForeColor = System.Drawing.Color.White;
            this.gridView4.Appearance.FocusedRow.GradientMode = System.Drawing.Drawing2D.LinearGradientMode.Vertical;
            this.gridView4.Appearance.FocusedRow.Options.UseBackColor = true;
            this.gridView4.Appearance.FocusedRow.Options.UseForeColor = true;
            this.gridView4.Appearance.FooterPanel.BackColor = System.Drawing.Color.Orange;
            this.gridView4.Appearance.FooterPanel.BorderColor = System.Drawing.Color.Orange;
            this.gridView4.Appearance.FooterPanel.Options.UseBackColor = true;
            this.gridView4.Appearance.FooterPanel.Options.UseBorderColor = true;
            this.gridView4.Appearance.GroupButton.BackColor = System.Drawing.Color.Wheat;
            this.gridView4.Appearance.GroupButton.BorderColor = System.Drawing.Color.Wheat;
            this.gridView4.Appearance.GroupButton.Options.UseBackColor = true;
            this.gridView4.Appearance.GroupButton.Options.UseBorderColor = true;
            this.gridView4.Appearance.GroupFooter.BackColor = System.Drawing.Color.Wheat;
            this.gridView4.Appearance.GroupFooter.BorderColor = System.Drawing.Color.Wheat;
            this.gridView4.Appearance.GroupFooter.Options.UseBackColor = true;
            this.gridView4.Appearance.GroupFooter.Options.UseBorderColor = true;
            this.gridView4.Appearance.GroupPanel.BackColor = System.Drawing.Color.RoyalBlue;
            this.gridView4.Appearance.GroupPanel.ForeColor = System.Drawing.Color.White;
            this.gridView4.Appearance.GroupPanel.Options.UseBackColor = true;
            this.gridView4.Appearance.GroupPanel.Options.UseForeColor = true;
            this.gridView4.Appearance.GroupRow.BackColor = System.Drawing.Color.Wheat;
            this.gridView4.Appearance.GroupRow.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Bold);
            this.gridView4.Appearance.GroupRow.Options.UseBackColor = true;
            this.gridView4.Appearance.GroupRow.Options.UseFont = true;
            this.gridView4.Appearance.HeaderPanel.BackColor = System.Drawing.Color.Orange;
            this.gridView4.Appearance.HeaderPanel.BorderColor = System.Drawing.Color.Orange;
            this.gridView4.Appearance.HeaderPanel.Options.UseBackColor = true;
            this.gridView4.Appearance.HeaderPanel.Options.UseBorderColor = true;
            this.gridView4.Appearance.HideSelectionRow.BackColor = System.Drawing.Color.LightSlateGray;
            this.gridView4.Appearance.HideSelectionRow.Options.UseBackColor = true;
            this.gridView4.Appearance.HorzLine.BackColor = System.Drawing.Color.Tan;
            this.gridView4.Appearance.HorzLine.Options.UseBackColor = true;
            this.gridView4.Appearance.OddRow.BackColor = System.Drawing.Color.White;
            this.gridView4.Appearance.OddRow.Options.UseBackColor = true;
            this.gridView4.Appearance.Preview.BackColor = System.Drawing.Color.Khaki;
            this.gridView4.Appearance.Preview.BackColor2 = System.Drawing.Color.Cornsilk;
            this.gridView4.Appearance.Preview.Font = new System.Drawing.Font("Tahoma", 7.5F);
            this.gridView4.Appearance.Preview.GradientMode = System.Drawing.Drawing2D.LinearGradientMode.BackwardDiagonal;
            this.gridView4.Appearance.Preview.Options.UseBackColor = true;
            this.gridView4.Appearance.Preview.Options.UseFont = true;
            this.gridView4.Appearance.Row.BackColor = System.Drawing.Color.White;
            this.gridView4.Appearance.Row.Options.UseBackColor = true;
            this.gridView4.Appearance.RowSeparator.BackColor = System.Drawing.Color.LightSkyBlue;
            this.gridView4.Appearance.RowSeparator.Options.UseBackColor = true;
            this.gridView4.Appearance.VertLine.BackColor = System.Drawing.Color.Tan;
            this.gridView4.Appearance.VertLine.Options.UseBackColor = true;
            this.gridView4.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colBloco,
            this.colNumero,
            this.colBoleto,
            this.colCompetencia,
            this.colDescricao3,
            this.colTipo2,
            this.colValor2,
            this.colN,
            this.colBotaoBoleto1});
            this.gridView4.GridControl = this.gridControl4;
            this.gridView4.Name = "gridView4";
            this.gridView4.OptionsView.ColumnAutoWidth = false;
            this.gridView4.OptionsView.EnableAppearanceEvenRow = true;
            this.gridView4.OptionsView.EnableAppearanceOddRow = true;
            this.gridView4.OptionsView.ShowGroupPanel = false;
            this.gridView4.CustomRowCellEdit += new DevExpress.XtraGrid.Views.Grid.CustomRowCellEditEventHandler(this.gridView4_CustomRowCellEdit);
            // 
            // colBloco
            // 
            this.colBloco.FieldName = "Bloco";
            this.colBloco.Name = "colBloco";
            this.colBloco.OptionsColumn.ReadOnly = true;
            this.colBloco.Visible = true;
            this.colBloco.VisibleIndex = 0;
            // 
            // colNumero
            // 
            this.colNumero.Caption = "Apartamento";
            this.colNumero.FieldName = "Numero";
            this.colNumero.Name = "colNumero";
            this.colNumero.OptionsColumn.ReadOnly = true;
            this.colNumero.Visible = true;
            this.colNumero.VisibleIndex = 1;
            // 
            // colBoleto
            // 
            this.colBoleto.FieldName = "Boleto";
            this.colBoleto.Name = "colBoleto";
            this.colBoleto.OptionsColumn.ReadOnly = true;
            this.colBoleto.Visible = true;
            this.colBoleto.VisibleIndex = 2;
            // 
            // colCompetencia
            // 
            this.colCompetencia.Caption = "Competência";
            this.colCompetencia.FieldName = "Competencia";
            this.colCompetencia.Name = "colCompetencia";
            this.colCompetencia.OptionsColumn.ReadOnly = true;
            this.colCompetencia.Visible = true;
            this.colCompetencia.VisibleIndex = 3;
            // 
            // colDescricao3
            // 
            this.colDescricao3.Caption = "Descrição";
            this.colDescricao3.FieldName = "Descricao";
            this.colDescricao3.Name = "colDescricao3";
            this.colDescricao3.OptionsColumn.ReadOnly = true;
            this.colDescricao3.Visible = true;
            this.colDescricao3.VisibleIndex = 4;
            // 
            // colTipo2
            // 
            this.colTipo2.FieldName = "Tipo";
            this.colTipo2.Name = "colTipo2";
            this.colTipo2.OptionsColumn.ReadOnly = true;
            this.colTipo2.Visible = true;
            this.colTipo2.VisibleIndex = 5;
            // 
            // colValor2
            // 
            this.colValor2.Caption = "Valor";
            this.colValor2.ColumnEdit = this.CalcEdit_n2;
            this.colValor2.FieldName = "Valor";
            this.colValor2.Name = "colValor2";
            this.colValor2.OptionsColumn.ReadOnly = true;
            this.colValor2.Visible = true;
            this.colValor2.VisibleIndex = 6;
            // 
            // CalcEdit_n2
            // 
            this.CalcEdit_n2.AutoHeight = false;
            this.CalcEdit_n2.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.CalcEdit_n2.DisplayFormat.FormatString = "n2";
            this.CalcEdit_n2.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.CalcEdit_n2.Name = "CalcEdit_n2";
            // 
            // colN
            // 
            this.colN.FieldName = "N";
            this.colN.Name = "colN";
            this.colN.OptionsColumn.ReadOnly = true;
            this.colN.Visible = true;
            this.colN.VisibleIndex = 7;
            // 
            // colBotaoBoleto1
            // 
            this.colBotaoBoleto1.Caption = "gridColumn3";
            this.colBotaoBoleto1.Fixed = DevExpress.XtraGrid.Columns.FixedStyle.Right;
            this.colBotaoBoleto1.Name = "colBotaoBoleto1";
            this.colBotaoBoleto1.OptionsColumn.ShowCaption = false;
            this.colBotaoBoleto1.Visible = true;
            this.colBotaoBoleto1.VisibleIndex = 8;
            this.colBotaoBoleto1.Width = 35;
            // 
            // gridControl4
            // 
            this.gridControl4.DataMember = "Creditos";
            this.gridControl4.DataSource = this.dCreditosBindingSource;
            this.gridControl4.Dock = System.Windows.Forms.DockStyle.Fill;
            gridLevelNode1.LevelTemplate = this.gridView4;
            gridLevelNode1.RelationName = "Creditos_Mes";
            this.gridControl4.LevelTree.Nodes.AddRange(new DevExpress.XtraGrid.GridLevelNode[] {
            gridLevelNode1});
            this.gridControl4.Location = new System.Drawing.Point(0, 0);
            this.gridControl4.MainView = this.gridView3;
            this.gridControl4.Name = "gridControl4";
            this.gridControl4.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.CalcEdit_n2,
            this.repBotaoBoleto1,
            this.BotDesc});
            this.gridControl4.ShowOnlyPredefinedDetails = true;
            this.gridControl4.Size = new System.Drawing.Size(1453, 204);
            this.gridControl4.TabIndex = 0;
            this.gridControl4.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridView3,
            this.gridView4});
            // 
            // dCreditosBindingSource
            // 
            this.dCreditosBindingSource.DataSource = typeof(Balancete.Relatorios.Credito.dCreditos);
            this.dCreditosBindingSource.Position = 0;
            // 
            // gridView3
            // 
            this.gridView3.Appearance.ColumnFilterButton.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(221)))), ((int)(((byte)(236)))), ((int)(((byte)(254)))));
            this.gridView3.Appearance.ColumnFilterButton.BackColor2 = System.Drawing.Color.FromArgb(((int)(((byte)(132)))), ((int)(((byte)(171)))), ((int)(((byte)(228)))));
            this.gridView3.Appearance.ColumnFilterButton.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(221)))), ((int)(((byte)(236)))), ((int)(((byte)(254)))));
            this.gridView3.Appearance.ColumnFilterButton.ForeColor = System.Drawing.Color.Black;
            this.gridView3.Appearance.ColumnFilterButton.GradientMode = System.Drawing.Drawing2D.LinearGradientMode.Vertical;
            this.gridView3.Appearance.ColumnFilterButton.Options.UseBackColor = true;
            this.gridView3.Appearance.ColumnFilterButton.Options.UseBorderColor = true;
            this.gridView3.Appearance.ColumnFilterButton.Options.UseForeColor = true;
            this.gridView3.Appearance.ColumnFilterButtonActive.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(247)))), ((int)(((byte)(251)))), ((int)(((byte)(255)))));
            this.gridView3.Appearance.ColumnFilterButtonActive.BackColor2 = System.Drawing.Color.FromArgb(((int)(((byte)(154)))), ((int)(((byte)(190)))), ((int)(((byte)(243)))));
            this.gridView3.Appearance.ColumnFilterButtonActive.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(247)))), ((int)(((byte)(251)))), ((int)(((byte)(255)))));
            this.gridView3.Appearance.ColumnFilterButtonActive.ForeColor = System.Drawing.Color.Black;
            this.gridView3.Appearance.ColumnFilterButtonActive.GradientMode = System.Drawing.Drawing2D.LinearGradientMode.Vertical;
            this.gridView3.Appearance.ColumnFilterButtonActive.Options.UseBackColor = true;
            this.gridView3.Appearance.ColumnFilterButtonActive.Options.UseBorderColor = true;
            this.gridView3.Appearance.ColumnFilterButtonActive.Options.UseForeColor = true;
            this.gridView3.Appearance.Empty.BackColor = System.Drawing.Color.White;
            this.gridView3.Appearance.Empty.Options.UseBackColor = true;
            this.gridView3.Appearance.EvenRow.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(231)))), ((int)(((byte)(242)))), ((int)(((byte)(254)))));
            this.gridView3.Appearance.EvenRow.ForeColor = System.Drawing.Color.Black;
            this.gridView3.Appearance.EvenRow.Options.UseBackColor = true;
            this.gridView3.Appearance.EvenRow.Options.UseForeColor = true;
            this.gridView3.Appearance.FilterCloseButton.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(221)))), ((int)(((byte)(236)))), ((int)(((byte)(254)))));
            this.gridView3.Appearance.FilterCloseButton.BackColor2 = System.Drawing.Color.FromArgb(((int)(((byte)(132)))), ((int)(((byte)(171)))), ((int)(((byte)(228)))));
            this.gridView3.Appearance.FilterCloseButton.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(221)))), ((int)(((byte)(236)))), ((int)(((byte)(254)))));
            this.gridView3.Appearance.FilterCloseButton.ForeColor = System.Drawing.Color.Black;
            this.gridView3.Appearance.FilterCloseButton.GradientMode = System.Drawing.Drawing2D.LinearGradientMode.Vertical;
            this.gridView3.Appearance.FilterCloseButton.Options.UseBackColor = true;
            this.gridView3.Appearance.FilterCloseButton.Options.UseBorderColor = true;
            this.gridView3.Appearance.FilterCloseButton.Options.UseForeColor = true;
            this.gridView3.Appearance.FilterPanel.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(62)))), ((int)(((byte)(109)))), ((int)(((byte)(185)))));
            this.gridView3.Appearance.FilterPanel.ForeColor = System.Drawing.Color.White;
            this.gridView3.Appearance.FilterPanel.Options.UseBackColor = true;
            this.gridView3.Appearance.FilterPanel.Options.UseForeColor = true;
            this.gridView3.Appearance.FixedLine.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(59)))), ((int)(((byte)(97)))), ((int)(((byte)(156)))));
            this.gridView3.Appearance.FixedLine.Options.UseBackColor = true;
            this.gridView3.Appearance.FocusedCell.BackColor = System.Drawing.Color.White;
            this.gridView3.Appearance.FocusedCell.ForeColor = System.Drawing.Color.Black;
            this.gridView3.Appearance.FocusedCell.Options.UseBackColor = true;
            this.gridView3.Appearance.FocusedCell.Options.UseForeColor = true;
            this.gridView3.Appearance.FocusedRow.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(49)))), ((int)(((byte)(106)))), ((int)(((byte)(197)))));
            this.gridView3.Appearance.FocusedRow.ForeColor = System.Drawing.Color.White;
            this.gridView3.Appearance.FocusedRow.Options.UseBackColor = true;
            this.gridView3.Appearance.FocusedRow.Options.UseForeColor = true;
            this.gridView3.Appearance.FooterPanel.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(221)))), ((int)(((byte)(236)))), ((int)(((byte)(254)))));
            this.gridView3.Appearance.FooterPanel.BackColor2 = System.Drawing.Color.FromArgb(((int)(((byte)(132)))), ((int)(((byte)(171)))), ((int)(((byte)(228)))));
            this.gridView3.Appearance.FooterPanel.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(221)))), ((int)(((byte)(236)))), ((int)(((byte)(254)))));
            this.gridView3.Appearance.FooterPanel.ForeColor = System.Drawing.Color.Black;
            this.gridView3.Appearance.FooterPanel.GradientMode = System.Drawing.Drawing2D.LinearGradientMode.Vertical;
            this.gridView3.Appearance.FooterPanel.Options.UseBackColor = true;
            this.gridView3.Appearance.FooterPanel.Options.UseBorderColor = true;
            this.gridView3.Appearance.FooterPanel.Options.UseForeColor = true;
            this.gridView3.Appearance.GroupButton.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(193)))), ((int)(((byte)(216)))), ((int)(((byte)(247)))));
            this.gridView3.Appearance.GroupButton.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(193)))), ((int)(((byte)(216)))), ((int)(((byte)(247)))));
            this.gridView3.Appearance.GroupButton.ForeColor = System.Drawing.Color.Black;
            this.gridView3.Appearance.GroupButton.Options.UseBackColor = true;
            this.gridView3.Appearance.GroupButton.Options.UseBorderColor = true;
            this.gridView3.Appearance.GroupButton.Options.UseForeColor = true;
            this.gridView3.Appearance.GroupFooter.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(193)))), ((int)(((byte)(216)))), ((int)(((byte)(247)))));
            this.gridView3.Appearance.GroupFooter.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(193)))), ((int)(((byte)(216)))), ((int)(((byte)(247)))));
            this.gridView3.Appearance.GroupFooter.ForeColor = System.Drawing.Color.Black;
            this.gridView3.Appearance.GroupFooter.Options.UseBackColor = true;
            this.gridView3.Appearance.GroupFooter.Options.UseBorderColor = true;
            this.gridView3.Appearance.GroupFooter.Options.UseForeColor = true;
            this.gridView3.Appearance.GroupPanel.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(62)))), ((int)(((byte)(109)))), ((int)(((byte)(185)))));
            this.gridView3.Appearance.GroupPanel.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(221)))), ((int)(((byte)(236)))), ((int)(((byte)(254)))));
            this.gridView3.Appearance.GroupPanel.Options.UseBackColor = true;
            this.gridView3.Appearance.GroupPanel.Options.UseForeColor = true;
            this.gridView3.Appearance.GroupRow.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(193)))), ((int)(((byte)(216)))), ((int)(((byte)(247)))));
            this.gridView3.Appearance.GroupRow.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(193)))), ((int)(((byte)(216)))), ((int)(((byte)(247)))));
            this.gridView3.Appearance.GroupRow.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Bold);
            this.gridView3.Appearance.GroupRow.ForeColor = System.Drawing.Color.Black;
            this.gridView3.Appearance.GroupRow.Options.UseBackColor = true;
            this.gridView3.Appearance.GroupRow.Options.UseBorderColor = true;
            this.gridView3.Appearance.GroupRow.Options.UseFont = true;
            this.gridView3.Appearance.GroupRow.Options.UseForeColor = true;
            this.gridView3.Appearance.HeaderPanel.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(221)))), ((int)(((byte)(236)))), ((int)(((byte)(254)))));
            this.gridView3.Appearance.HeaderPanel.BackColor2 = System.Drawing.Color.FromArgb(((int)(((byte)(132)))), ((int)(((byte)(171)))), ((int)(((byte)(228)))));
            this.gridView3.Appearance.HeaderPanel.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(221)))), ((int)(((byte)(236)))), ((int)(((byte)(254)))));
            this.gridView3.Appearance.HeaderPanel.ForeColor = System.Drawing.Color.Black;
            this.gridView3.Appearance.HeaderPanel.GradientMode = System.Drawing.Drawing2D.LinearGradientMode.Vertical;
            this.gridView3.Appearance.HeaderPanel.Options.UseBackColor = true;
            this.gridView3.Appearance.HeaderPanel.Options.UseBorderColor = true;
            this.gridView3.Appearance.HeaderPanel.Options.UseForeColor = true;
            this.gridView3.Appearance.HideSelectionRow.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(106)))), ((int)(((byte)(153)))), ((int)(((byte)(228)))));
            this.gridView3.Appearance.HideSelectionRow.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(208)))), ((int)(((byte)(224)))), ((int)(((byte)(251)))));
            this.gridView3.Appearance.HideSelectionRow.Options.UseBackColor = true;
            this.gridView3.Appearance.HideSelectionRow.Options.UseForeColor = true;
            this.gridView3.Appearance.HorzLine.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(99)))), ((int)(((byte)(127)))), ((int)(((byte)(196)))));
            this.gridView3.Appearance.HorzLine.Options.UseBackColor = true;
            this.gridView3.Appearance.OddRow.BackColor = System.Drawing.Color.White;
            this.gridView3.Appearance.OddRow.ForeColor = System.Drawing.Color.Black;
            this.gridView3.Appearance.OddRow.Options.UseBackColor = true;
            this.gridView3.Appearance.OddRow.Options.UseForeColor = true;
            this.gridView3.Appearance.Preview.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(249)))), ((int)(((byte)(252)))), ((int)(((byte)(255)))));
            this.gridView3.Appearance.Preview.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(88)))), ((int)(((byte)(129)))), ((int)(((byte)(185)))));
            this.gridView3.Appearance.Preview.Options.UseBackColor = true;
            this.gridView3.Appearance.Preview.Options.UseForeColor = true;
            this.gridView3.Appearance.Row.BackColor = System.Drawing.Color.White;
            this.gridView3.Appearance.Row.ForeColor = System.Drawing.Color.Black;
            this.gridView3.Appearance.Row.Options.UseBackColor = true;
            this.gridView3.Appearance.Row.Options.UseForeColor = true;
            this.gridView3.Appearance.RowSeparator.BackColor = System.Drawing.Color.White;
            this.gridView3.Appearance.RowSeparator.Options.UseBackColor = true;
            this.gridView3.Appearance.SelectedRow.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(69)))), ((int)(((byte)(126)))), ((int)(((byte)(217)))));
            this.gridView3.Appearance.SelectedRow.ForeColor = System.Drawing.Color.White;
            this.gridView3.Appearance.SelectedRow.Options.UseBackColor = true;
            this.gridView3.Appearance.SelectedRow.Options.UseForeColor = true;
            this.gridView3.Appearance.VertLine.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(99)))), ((int)(((byte)(127)))), ((int)(((byte)(196)))));
            this.gridView3.Appearance.VertLine.Options.UseBackColor = true;
            this.gridView3.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colDescricao2,
            this.colAtrasado,
            this.colPeriodo,
            this.colAntecipado,
            this.colTotal,
            this.colT1,
            this.colT2,
            this.colT3,
            this.colOrdem2,
            this.colPrevisto,
            this.colPLA2,
            this.colSoPrevisto,
            this.gridColumn1});
            this.gridView3.GridControl = this.gridControl4;
            this.gridView3.Name = "gridView3";
            this.gridView3.OptionsDetail.ShowDetailTabs = false;
            this.gridView3.OptionsView.ColumnAutoWidth = false;
            this.gridView3.OptionsView.EnableAppearanceEvenRow = true;
            this.gridView3.OptionsView.EnableAppearanceOddRow = true;
            this.gridView3.OptionsView.ShowAutoFilterRow = true;
            this.gridView3.OptionsView.ShowFooter = true;
            this.gridView3.OptionsView.ShowGroupPanel = false;
            this.gridView3.SortInfo.AddRange(new DevExpress.XtraGrid.Columns.GridColumnSortInfo[] {
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colOrdem2, DevExpress.Data.ColumnSortOrder.Ascending)});
            this.gridView3.RowUpdated += new DevExpress.XtraGrid.Views.Base.RowObjectEventHandler(this.gridView3_RowUpdated);
            // 
            // colDescricao2
            // 
            this.colDescricao2.Caption = "Descrição";
            this.colDescricao2.FieldName = "Descricao";
            this.colDescricao2.Name = "colDescricao2";
            this.colDescricao2.OptionsColumn.ReadOnly = true;
            this.colDescricao2.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Count)});
            this.colDescricao2.Visible = true;
            this.colDescricao2.VisibleIndex = 1;
            this.colDescricao2.Width = 498;
            // 
            // colAtrasado
            // 
            this.colAtrasado.Caption = "Atrasado";
            this.colAtrasado.ColumnEdit = this.CalcEdit_n2;
            this.colAtrasado.FieldName = "Atrasado";
            this.colAtrasado.Name = "colAtrasado";
            this.colAtrasado.OptionsColumn.ReadOnly = true;
            this.colAtrasado.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum, "Atrasado", "{0:n2}")});
            this.colAtrasado.Visible = true;
            this.colAtrasado.VisibleIndex = 2;
            // 
            // colPeriodo
            // 
            this.colPeriodo.Caption = "Período";
            this.colPeriodo.ColumnEdit = this.CalcEdit_n2;
            this.colPeriodo.FieldName = "Periodo";
            this.colPeriodo.Name = "colPeriodo";
            this.colPeriodo.OptionsColumn.ReadOnly = true;
            this.colPeriodo.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum, "Periodo", "{0:n2}")});
            this.colPeriodo.Visible = true;
            this.colPeriodo.VisibleIndex = 3;
            // 
            // colAntecipado
            // 
            this.colAntecipado.Caption = "Antecipado";
            this.colAntecipado.ColumnEdit = this.CalcEdit_n2;
            this.colAntecipado.FieldName = "Antecipado";
            this.colAntecipado.Name = "colAntecipado";
            this.colAntecipado.OptionsColumn.ReadOnly = true;
            this.colAntecipado.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum, "Antecipado", "{0:n2}")});
            this.colAntecipado.Visible = true;
            this.colAntecipado.VisibleIndex = 4;
            // 
            // colTotal
            // 
            this.colTotal.Caption = "Total";
            this.colTotal.ColumnEdit = this.CalcEdit_n2;
            this.colTotal.FieldName = "Total";
            this.colTotal.Name = "colTotal";
            this.colTotal.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum, "Total", "{0:n2}")});
            this.colTotal.Visible = true;
            this.colTotal.VisibleIndex = 5;
            // 
            // colT1
            // 
            this.colT1.FieldName = "T1";
            this.colT1.Name = "colT1";
            // 
            // colT2
            // 
            this.colT2.FieldName = "T2";
            this.colT2.Name = "colT2";
            // 
            // colT3
            // 
            this.colT3.FieldName = "T3";
            this.colT3.Name = "colT3";
            // 
            // colOrdem2
            // 
            this.colOrdem2.FieldName = "Ordem";
            this.colOrdem2.Name = "colOrdem2";
            // 
            // colPrevisto
            // 
            this.colPrevisto.Caption = "Previsto";
            this.colPrevisto.ColumnEdit = this.CalcEdit_n2;
            this.colPrevisto.FieldName = "Previsto";
            this.colPrevisto.Name = "colPrevisto";
            this.colPrevisto.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum, "Previsto", "{0:n2}")});
            this.colPrevisto.Visible = true;
            this.colPrevisto.VisibleIndex = 6;
            // 
            // colPLA2
            // 
            this.colPLA2.Caption = "Classificação";
            this.colPLA2.FieldName = "PLA";
            this.colPLA2.Name = "colPLA2";
            this.colPLA2.OptionsColumn.ReadOnly = true;
            this.colPLA2.Visible = true;
            this.colPLA2.VisibleIndex = 0;
            this.colPLA2.Width = 102;
            // 
            // colSoPrevisto
            // 
            this.colSoPrevisto.FieldName = "SoPrevisto";
            this.colSoPrevisto.Name = "colSoPrevisto";
            // 
            // gridColumn1
            // 
            this.gridColumn1.Caption = "gridColumn1";
            this.gridColumn1.ColumnEdit = this.BotDesc;
            this.gridColumn1.Name = "gridColumn1";
            // 
            // BotDesc
            // 
            this.BotDesc.AutoHeight = false;
            this.BotDesc.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.BotDesc.Name = "BotDesc";
            this.BotDesc.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.HideTextEditor;
            this.BotDesc.Click += new System.EventHandler(this.BotDesc_Click);
            // 
            // repBotaoBoleto1
            // 
            this.repBotaoBoleto1.AutoHeight = false;
            this.repBotaoBoleto1.Name = "repBotaoBoleto1";
            this.repBotaoBoleto1.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.HideTextEditor;
            this.repBotaoBoleto1.ButtonClick += new DevExpress.XtraEditors.Controls.ButtonPressedEventHandler(this.repositoryItemButtonEdit4_ButtonClick);
            // 
            // bandedGridView3
            // 
            this.bandedGridView3.Appearance.BandPanel.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(244)))), ((int)(((byte)(247)))), ((int)(((byte)(222)))));
            this.bandedGridView3.Appearance.BandPanel.BackColor2 = System.Drawing.Color.FromArgb(((int)(((byte)(184)))), ((int)(((byte)(199)))), ((int)(((byte)(146)))));
            this.bandedGridView3.Appearance.BandPanel.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(244)))), ((int)(((byte)(247)))), ((int)(((byte)(222)))));
            this.bandedGridView3.Appearance.BandPanel.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Bold);
            this.bandedGridView3.Appearance.BandPanel.ForeColor = System.Drawing.Color.Black;
            this.bandedGridView3.Appearance.BandPanel.GradientMode = System.Drawing.Drawing2D.LinearGradientMode.Vertical;
            this.bandedGridView3.Appearance.BandPanel.Options.UseBackColor = true;
            this.bandedGridView3.Appearance.BandPanel.Options.UseBorderColor = true;
            this.bandedGridView3.Appearance.BandPanel.Options.UseFont = true;
            this.bandedGridView3.Appearance.BandPanel.Options.UseForeColor = true;
            this.bandedGridView3.Appearance.BandPanelBackground.BackColor = System.Drawing.Color.White;
            this.bandedGridView3.Appearance.BandPanelBackground.Options.UseBackColor = true;
            this.bandedGridView3.Appearance.ColumnFilterButton.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(244)))), ((int)(((byte)(247)))), ((int)(((byte)(222)))));
            this.bandedGridView3.Appearance.ColumnFilterButton.BackColor2 = System.Drawing.Color.FromArgb(((int)(((byte)(184)))), ((int)(((byte)(199)))), ((int)(((byte)(146)))));
            this.bandedGridView3.Appearance.ColumnFilterButton.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(244)))), ((int)(((byte)(247)))), ((int)(((byte)(222)))));
            this.bandedGridView3.Appearance.ColumnFilterButton.ForeColor = System.Drawing.Color.Black;
            this.bandedGridView3.Appearance.ColumnFilterButton.GradientMode = System.Drawing.Drawing2D.LinearGradientMode.Vertical;
            this.bandedGridView3.Appearance.ColumnFilterButton.Options.UseBackColor = true;
            this.bandedGridView3.Appearance.ColumnFilterButton.Options.UseBorderColor = true;
            this.bandedGridView3.Appearance.ColumnFilterButton.Options.UseForeColor = true;
            this.bandedGridView3.Appearance.ColumnFilterButtonActive.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(254)))), ((int)(((byte)(255)))), ((int)(((byte)(244)))));
            this.bandedGridView3.Appearance.ColumnFilterButtonActive.BackColor2 = System.Drawing.Color.FromArgb(((int)(((byte)(213)))), ((int)(((byte)(222)))), ((int)(((byte)(183)))));
            this.bandedGridView3.Appearance.ColumnFilterButtonActive.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(254)))), ((int)(((byte)(255)))), ((int)(((byte)(244)))));
            this.bandedGridView3.Appearance.ColumnFilterButtonActive.ForeColor = System.Drawing.Color.Black;
            this.bandedGridView3.Appearance.ColumnFilterButtonActive.GradientMode = System.Drawing.Drawing2D.LinearGradientMode.Vertical;
            this.bandedGridView3.Appearance.ColumnFilterButtonActive.Options.UseBackColor = true;
            this.bandedGridView3.Appearance.ColumnFilterButtonActive.Options.UseBorderColor = true;
            this.bandedGridView3.Appearance.ColumnFilterButtonActive.Options.UseForeColor = true;
            this.bandedGridView3.Appearance.Empty.BackColor = System.Drawing.Color.White;
            this.bandedGridView3.Appearance.Empty.Options.UseBackColor = true;
            this.bandedGridView3.Appearance.EvenRow.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(244)))), ((int)(((byte)(247)))), ((int)(((byte)(222)))));
            this.bandedGridView3.Appearance.EvenRow.ForeColor = System.Drawing.Color.Black;
            this.bandedGridView3.Appearance.EvenRow.Options.UseBackColor = true;
            this.bandedGridView3.Appearance.EvenRow.Options.UseForeColor = true;
            this.bandedGridView3.Appearance.FilterCloseButton.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(244)))), ((int)(((byte)(247)))), ((int)(((byte)(222)))));
            this.bandedGridView3.Appearance.FilterCloseButton.BackColor2 = System.Drawing.Color.FromArgb(((int)(((byte)(184)))), ((int)(((byte)(199)))), ((int)(((byte)(146)))));
            this.bandedGridView3.Appearance.FilterCloseButton.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(244)))), ((int)(((byte)(247)))), ((int)(((byte)(222)))));
            this.bandedGridView3.Appearance.FilterCloseButton.ForeColor = System.Drawing.Color.Black;
            this.bandedGridView3.Appearance.FilterCloseButton.GradientMode = System.Drawing.Drawing2D.LinearGradientMode.Vertical;
            this.bandedGridView3.Appearance.FilterCloseButton.Options.UseBackColor = true;
            this.bandedGridView3.Appearance.FilterCloseButton.Options.UseBorderColor = true;
            this.bandedGridView3.Appearance.FilterCloseButton.Options.UseForeColor = true;
            this.bandedGridView3.Appearance.FilterPanel.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(139)))), ((int)(((byte)(158)))), ((int)(((byte)(126)))));
            this.bandedGridView3.Appearance.FilterPanel.ForeColor = System.Drawing.Color.White;
            this.bandedGridView3.Appearance.FilterPanel.Options.UseBackColor = true;
            this.bandedGridView3.Appearance.FilterPanel.Options.UseForeColor = true;
            this.bandedGridView3.Appearance.FixedLine.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(96)))), ((int)(((byte)(128)))), ((int)(((byte)(88)))));
            this.bandedGridView3.Appearance.FixedLine.Options.UseBackColor = true;
            this.bandedGridView3.Appearance.FocusedCell.BackColor = System.Drawing.Color.White;
            this.bandedGridView3.Appearance.FocusedCell.ForeColor = System.Drawing.Color.Black;
            this.bandedGridView3.Appearance.FocusedCell.Options.UseBackColor = true;
            this.bandedGridView3.Appearance.FocusedCell.Options.UseForeColor = true;
            this.bandedGridView3.Appearance.FocusedRow.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(147)))), ((int)(((byte)(160)))), ((int)(((byte)(112)))));
            this.bandedGridView3.Appearance.FocusedRow.ForeColor = System.Drawing.Color.White;
            this.bandedGridView3.Appearance.FocusedRow.Options.UseBackColor = true;
            this.bandedGridView3.Appearance.FocusedRow.Options.UseForeColor = true;
            this.bandedGridView3.Appearance.FooterPanel.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(244)))), ((int)(((byte)(247)))), ((int)(((byte)(222)))));
            this.bandedGridView3.Appearance.FooterPanel.BackColor2 = System.Drawing.Color.FromArgb(((int)(((byte)(184)))), ((int)(((byte)(199)))), ((int)(((byte)(146)))));
            this.bandedGridView3.Appearance.FooterPanel.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(244)))), ((int)(((byte)(247)))), ((int)(((byte)(222)))));
            this.bandedGridView3.Appearance.FooterPanel.ForeColor = System.Drawing.Color.Black;
            this.bandedGridView3.Appearance.FooterPanel.GradientMode = System.Drawing.Drawing2D.LinearGradientMode.Vertical;
            this.bandedGridView3.Appearance.FooterPanel.Options.UseBackColor = true;
            this.bandedGridView3.Appearance.FooterPanel.Options.UseBorderColor = true;
            this.bandedGridView3.Appearance.FooterPanel.Options.UseForeColor = true;
            this.bandedGridView3.Appearance.GroupButton.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(223)))), ((int)(((byte)(230)))), ((int)(((byte)(195)))));
            this.bandedGridView3.Appearance.GroupButton.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(223)))), ((int)(((byte)(230)))), ((int)(((byte)(195)))));
            this.bandedGridView3.Appearance.GroupButton.ForeColor = System.Drawing.Color.Black;
            this.bandedGridView3.Appearance.GroupButton.Options.UseBackColor = true;
            this.bandedGridView3.Appearance.GroupButton.Options.UseBorderColor = true;
            this.bandedGridView3.Appearance.GroupButton.Options.UseForeColor = true;
            this.bandedGridView3.Appearance.GroupFooter.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(223)))), ((int)(((byte)(230)))), ((int)(((byte)(195)))));
            this.bandedGridView3.Appearance.GroupFooter.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(223)))), ((int)(((byte)(230)))), ((int)(((byte)(195)))));
            this.bandedGridView3.Appearance.GroupFooter.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Bold);
            this.bandedGridView3.Appearance.GroupFooter.ForeColor = System.Drawing.Color.Black;
            this.bandedGridView3.Appearance.GroupFooter.Options.UseBackColor = true;
            this.bandedGridView3.Appearance.GroupFooter.Options.UseBorderColor = true;
            this.bandedGridView3.Appearance.GroupFooter.Options.UseFont = true;
            this.bandedGridView3.Appearance.GroupFooter.Options.UseForeColor = true;
            this.bandedGridView3.Appearance.GroupPanel.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(139)))), ((int)(((byte)(158)))), ((int)(((byte)(126)))));
            this.bandedGridView3.Appearance.GroupPanel.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(244)))), ((int)(((byte)(247)))), ((int)(((byte)(222)))));
            this.bandedGridView3.Appearance.GroupPanel.Options.UseBackColor = true;
            this.bandedGridView3.Appearance.GroupPanel.Options.UseForeColor = true;
            this.bandedGridView3.Appearance.GroupRow.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(223)))), ((int)(((byte)(230)))), ((int)(((byte)(195)))));
            this.bandedGridView3.Appearance.GroupRow.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(223)))), ((int)(((byte)(230)))), ((int)(((byte)(195)))));
            this.bandedGridView3.Appearance.GroupRow.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Bold);
            this.bandedGridView3.Appearance.GroupRow.ForeColor = System.Drawing.Color.Black;
            this.bandedGridView3.Appearance.GroupRow.Options.UseBackColor = true;
            this.bandedGridView3.Appearance.GroupRow.Options.UseBorderColor = true;
            this.bandedGridView3.Appearance.GroupRow.Options.UseFont = true;
            this.bandedGridView3.Appearance.GroupRow.Options.UseForeColor = true;
            this.bandedGridView3.Appearance.HeaderPanel.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(244)))), ((int)(((byte)(247)))), ((int)(((byte)(222)))));
            this.bandedGridView3.Appearance.HeaderPanel.BackColor2 = System.Drawing.Color.FromArgb(((int)(((byte)(184)))), ((int)(((byte)(199)))), ((int)(((byte)(146)))));
            this.bandedGridView3.Appearance.HeaderPanel.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(244)))), ((int)(((byte)(247)))), ((int)(((byte)(222)))));
            this.bandedGridView3.Appearance.HeaderPanel.ForeColor = System.Drawing.Color.Black;
            this.bandedGridView3.Appearance.HeaderPanel.GradientMode = System.Drawing.Drawing2D.LinearGradientMode.Vertical;
            this.bandedGridView3.Appearance.HeaderPanel.Options.UseBackColor = true;
            this.bandedGridView3.Appearance.HeaderPanel.Options.UseBorderColor = true;
            this.bandedGridView3.Appearance.HeaderPanel.Options.UseForeColor = true;
            this.bandedGridView3.Appearance.HeaderPanelBackground.BackColor = System.Drawing.Color.White;
            this.bandedGridView3.Appearance.HeaderPanelBackground.Options.UseBackColor = true;
            this.bandedGridView3.Appearance.HideSelectionRow.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(197)))), ((int)(((byte)(207)))), ((int)(((byte)(170)))));
            this.bandedGridView3.Appearance.HideSelectionRow.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(147)))), ((int)(((byte)(160)))), ((int)(((byte)(112)))));
            this.bandedGridView3.Appearance.HideSelectionRow.Options.UseBackColor = true;
            this.bandedGridView3.Appearance.HideSelectionRow.Options.UseForeColor = true;
            this.bandedGridView3.Appearance.HorzLine.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(136)))), ((int)(((byte)(168)))), ((int)(((byte)(128)))));
            this.bandedGridView3.Appearance.HorzLine.Options.UseBackColor = true;
            this.bandedGridView3.Appearance.Preview.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(252)))), ((int)(((byte)(253)))), ((int)(((byte)(247)))));
            this.bandedGridView3.Appearance.Preview.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(147)))), ((int)(((byte)(160)))), ((int)(((byte)(112)))));
            this.bandedGridView3.Appearance.Preview.Options.UseBackColor = true;
            this.bandedGridView3.Appearance.Preview.Options.UseForeColor = true;
            this.bandedGridView3.Appearance.Row.BackColor = System.Drawing.Color.White;
            this.bandedGridView3.Appearance.Row.ForeColor = System.Drawing.Color.Black;
            this.bandedGridView3.Appearance.Row.Options.UseBackColor = true;
            this.bandedGridView3.Appearance.Row.Options.UseForeColor = true;
            this.bandedGridView3.Appearance.RowSeparator.BackColor = System.Drawing.Color.White;
            this.bandedGridView3.Appearance.RowSeparator.Options.UseBackColor = true;
            this.bandedGridView3.Appearance.SelectedRow.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(166)))), ((int)(((byte)(178)))), ((int)(((byte)(133)))));
            this.bandedGridView3.Appearance.SelectedRow.ForeColor = System.Drawing.Color.White;
            this.bandedGridView3.Appearance.SelectedRow.Options.UseBackColor = true;
            this.bandedGridView3.Appearance.SelectedRow.Options.UseForeColor = true;
            this.bandedGridView3.Appearance.TopNewRow.BackColor = System.Drawing.Color.White;
            this.bandedGridView3.Appearance.TopNewRow.Options.UseBackColor = true;
            this.bandedGridView3.Appearance.VertLine.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(136)))), ((int)(((byte)(160)))), ((int)(((byte)(188)))));
            this.bandedGridView3.Appearance.VertLine.Options.UseBackColor = true;
            this.bandedGridView3.Bands.AddRange(new DevExpress.XtraGrid.Views.BandedGrid.GridBand[] {
            this.gridBand7,
            this.gridBand8,
            this.gridBand5,
            this.gridBand6,
            this.gridBand10});
            this.bandedGridView3.Columns.AddRange(new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn[] {
            this.colCHENumero,
            this.colCHEStatus,
            this.colCHEValor,
            this.colCHEVencimento,
            this.colCHE,
            this.colNOA,
            this.colNOACompet,
            this.colNOANumero,
            this.colPAG2,
            this.colPAGValor,
            this.colPAGVencimento,
            this.colSCCData,
            this.colBotCHE,
            this.bandedGridColumn4,
            this.colStatus1});
            this.bandedGridView3.GridControl = this.gridControl9;
            this.bandedGridView3.Name = "bandedGridView3";
            this.bandedGridView3.OptionsView.ColumnAutoWidth = false;
            this.bandedGridView3.OptionsView.EnableAppearanceEvenRow = true;
            this.bandedGridView3.OptionsView.ShowGroupPanel = false;
            this.bandedGridView3.RowCellStyle += new DevExpress.XtraGrid.Views.Grid.RowCellStyleEventHandler(this.bandedGridView3_RowCellStyle);
            this.bandedGridView3.CustomRowCellEdit += new DevExpress.XtraGrid.Views.Grid.CustomRowCellEditEventHandler(this.bandedGridView3_CustomRowCellEdit);
            this.bandedGridView3.CustomColumnDisplayText += new DevExpress.XtraGrid.Views.Base.CustomColumnDisplayTextEventHandler(this.bandedGridView3_CustomColumnDisplayText);
            // 
            // gridBand7
            // 
            this.gridBand7.AppearanceHeader.Options.UseTextOptions = true;
            this.gridBand7.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridBand7.Caption = "Nota";
            this.gridBand7.Columns.Add(this.colNOA);
            this.gridBand7.Columns.Add(this.colNOACompet);
            this.gridBand7.Columns.Add(this.colNOANumero);
            this.gridBand7.Columns.Add(this.bandedGridColumn4);
            this.gridBand7.Name = "gridBand7";
            this.gridBand7.VisibleIndex = 0;
            this.gridBand7.Width = 180;
            // 
            // colNOA
            // 
            this.colNOA.Caption = "NOA";
            this.colNOA.FieldName = "NOA";
            this.colNOA.Name = "colNOA";
            this.colNOA.OptionsColumn.ReadOnly = true;
            // 
            // colNOACompet
            // 
            this.colNOACompet.Caption = "Competência";
            this.colNOACompet.FieldName = "NOACompet";
            this.colNOACompet.Name = "colNOACompet";
            this.colNOACompet.OptionsColumn.ReadOnly = true;
            this.colNOACompet.Visible = true;
            // 
            // colNOANumero
            // 
            this.colNOANumero.Caption = "Número";
            this.colNOANumero.FieldName = "NOANumero";
            this.colNOANumero.Name = "colNOANumero";
            this.colNOANumero.OptionsColumn.ReadOnly = true;
            this.colNOANumero.Visible = true;
            // 
            // bandedGridColumn4
            // 
            this.bandedGridColumn4.Caption = "bandedGridColumn4";
            this.bandedGridColumn4.ColumnEdit = this.BOTNota;
            this.bandedGridColumn4.Name = "bandedGridColumn4";
            this.bandedGridColumn4.OptionsColumn.ShowCaption = false;
            this.bandedGridColumn4.Visible = true;
            this.bandedGridColumn4.Width = 30;
            // 
            // BOTNota
            // 
            this.BOTNota.AutoHeight = false;
            this.BOTNota.Name = "BOTNota";
            this.BOTNota.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.HideTextEditor;
            this.BOTNota.ButtonClick += new DevExpress.XtraEditors.Controls.ButtonPressedEventHandler(this.BOTNota_ButtonClick);
            // 
            // gridBand8
            // 
            this.gridBand8.AppearanceHeader.Options.UseTextOptions = true;
            this.gridBand8.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridBand8.Caption = "Parcela";
            this.gridBand8.Columns.Add(this.colPAG2);
            this.gridBand8.Columns.Add(this.colPAGValor);
            this.gridBand8.Columns.Add(this.colPAGVencimento);
            this.gridBand8.Name = "gridBand8";
            this.gridBand8.VisibleIndex = 1;
            this.gridBand8.Width = 75;
            // 
            // colPAG2
            // 
            this.colPAG2.Caption = "PAG";
            this.colPAG2.FieldName = "PAG";
            this.colPAG2.Name = "colPAG2";
            this.colPAG2.OptionsColumn.ReadOnly = true;
            // 
            // colPAGValor
            // 
            this.colPAGValor.Caption = "Valor";
            this.colPAGValor.CustomizationCaption = "Valor da parcela";
            this.colPAGValor.DisplayFormat.FormatString = "n2";
            this.colPAGValor.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.colPAGValor.FieldName = "PAGValor";
            this.colPAGValor.Name = "colPAGValor";
            this.colPAGValor.OptionsColumn.ReadOnly = true;
            this.colPAGValor.Visible = true;
            // 
            // colPAGVencimento
            // 
            this.colPAGVencimento.FieldName = "PAGVencimento";
            this.colPAGVencimento.Name = "colPAGVencimento";
            this.colPAGVencimento.OptionsColumn.ReadOnly = true;
            // 
            // gridBand5
            // 
            this.gridBand5.AppearanceHeader.Options.UseTextOptions = true;
            this.gridBand5.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridBand5.Caption = "Cheque";
            this.gridBand5.Columns.Add(this.colCHE);
            this.gridBand5.Columns.Add(this.colCHENumero);
            this.gridBand5.Columns.Add(this.colCHEStatus);
            this.gridBand5.Columns.Add(this.colCHEValor);
            this.gridBand5.Columns.Add(this.colCHEVencimento);
            this.gridBand5.Columns.Add(this.colBotCHE);
            this.gridBand5.Name = "gridBand5";
            this.gridBand5.VisibleIndex = 2;
            this.gridBand5.Width = 330;
            // 
            // colCHE
            // 
            this.colCHE.Caption = "CHE";
            this.colCHE.FieldName = "CHE";
            this.colCHE.Name = "colCHE";
            this.colCHE.OptionsColumn.ReadOnly = true;
            // 
            // colCHENumero
            // 
            this.colCHENumero.Caption = "Número";
            this.colCHENumero.CustomizationCaption = "Número do Cheque";
            this.colCHENumero.FieldName = "CHENumero";
            this.colCHENumero.Name = "colCHENumero";
            this.colCHENumero.OptionsColumn.ReadOnly = true;
            this.colCHENumero.Visible = true;
            // 
            // colCHEStatus
            // 
            this.colCHEStatus.Caption = "Status";
            this.colCHEStatus.ColumnEdit = this.repositoryItemImageComboBox3;
            this.colCHEStatus.CustomizationCaption = "Status do Cheque";
            this.colCHEStatus.FieldName = "CHEStatus";
            this.colCHEStatus.Name = "colCHEStatus";
            this.colCHEStatus.OptionsColumn.ReadOnly = true;
            this.colCHEStatus.Visible = true;
            // 
            // repositoryItemImageComboBox3
            // 
            this.repositoryItemImageComboBox3.AutoHeight = false;
            this.repositoryItemImageComboBox3.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemImageComboBox3.Name = "repositoryItemImageComboBox3";
            // 
            // colCHEValor
            // 
            this.colCHEValor.Caption = "Valor";
            this.colCHEValor.CustomizationCaption = "Valor dio cheque";
            this.colCHEValor.DisplayFormat.FormatString = "n2";
            this.colCHEValor.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.colCHEValor.FieldName = "CHEValor";
            this.colCHEValor.Name = "colCHEValor";
            this.colCHEValor.OptionsColumn.ReadOnly = true;
            this.colCHEValor.Visible = true;
            // 
            // colCHEVencimento
            // 
            this.colCHEVencimento.Caption = "Vencimento";
            this.colCHEVencimento.CustomizationCaption = "Vencimento do cheque";
            this.colCHEVencimento.FieldName = "CHEVencimento";
            this.colCHEVencimento.Name = "colCHEVencimento";
            this.colCHEVencimento.OptionsColumn.ReadOnly = true;
            this.colCHEVencimento.Visible = true;
            // 
            // colBotCHE
            // 
            this.colBotCHE.Caption = "bandedGridColumn3";
            this.colBotCHE.Name = "colBotCHE";
            this.colBotCHE.OptionsColumn.ShowCaption = false;
            this.colBotCHE.Visible = true;
            this.colBotCHE.Width = 30;
            // 
            // gridBand6
            // 
            this.gridBand6.AppearanceHeader.Options.UseTextOptions = true;
            this.gridBand6.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridBand6.Caption = "Extrato";
            this.gridBand6.Columns.Add(this.colSCCData);
            this.gridBand6.Name = "gridBand6";
            this.gridBand6.VisibleIndex = 3;
            this.gridBand6.Width = 75;
            // 
            // colSCCData
            // 
            this.colSCCData.Caption = "Data";
            this.colSCCData.CustomizationCaption = "Data Extrato";
            this.colSCCData.FieldName = "SCCData";
            this.colSCCData.Name = "colSCCData";
            this.colSCCData.OptionsColumn.ReadOnly = true;
            this.colSCCData.Visible = true;
            // 
            // gridBand10
            // 
            this.gridBand10.Caption = "gridBand10";
            this.gridBand10.Columns.Add(this.colStatus1);
            this.gridBand10.Name = "gridBand10";
            this.gridBand10.OptionsBand.ShowCaption = false;
            this.gridBand10.VisibleIndex = 4;
            this.gridBand10.Width = 75;
            // 
            // colStatus1
            // 
            this.colStatus1.Caption = "Status";
            this.colStatus1.ColumnEdit = this.repositoryItemImageComboBox4;
            this.colStatus1.FieldName = "Status";
            this.colStatus1.Name = "colStatus1";
            this.colStatus1.OptionsColumn.ReadOnly = true;
            this.colStatus1.OptionsColumn.ShowCaption = false;
            this.colStatus1.Visible = true;
            // 
            // repositoryItemImageComboBox4
            // 
            this.repositoryItemImageComboBox4.AutoHeight = false;
            this.repositoryItemImageComboBox4.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemImageComboBox4.Name = "repositoryItemImageComboBox4";
            // 
            // gridControl9
            // 
            this.gridControl9.DataSource = this.pagamentosBindingSource;
            this.gridControl9.Dock = System.Windows.Forms.DockStyle.Fill;
            gridLevelNode2.LevelTemplate = this.bandedGridView3;
            gridLevelNode2.RelationName = "Pagamentos_DataTable1";
            this.gridControl9.LevelTree.Nodes.AddRange(new DevExpress.XtraGrid.GridLevelNode[] {
            gridLevelNode2});
            this.gridControl9.Location = new System.Drawing.Point(0, 38);
            this.gridControl9.MainView = this.bandedGridView2;
            this.gridControl9.Name = "gridControl9";
            this.gridControl9.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemCalcEdit4,
            this.BotCHE,
            this.BOTNota,
            this.BotPGF,
            this.repositoryItemImageComboBox3,
            this.repositoryItemImageComboBox4});
            this.gridControl9.Size = new System.Drawing.Size(1459, 194);
            this.gridControl9.TabIndex = 0;
            this.gridControl9.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.bandedGridView2,
            this.bandedGridView3});
            // 
            // pagamentosBindingSource
            // 
            this.pagamentosBindingSource.DataMember = "Pagamentos";
            this.pagamentosBindingSource.DataSource = typeof(Balancete.GrBalancete.dPeriodicos);
            // 
            // bandedGridView2
            // 
            this.bandedGridView2.Appearance.BandPanel.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(243)))), ((int)(((byte)(244)))), ((int)(((byte)(250)))));
            this.bandedGridView2.Appearance.BandPanel.BackColor2 = System.Drawing.Color.FromArgb(((int)(((byte)(154)))), ((int)(((byte)(153)))), ((int)(((byte)(182)))));
            this.bandedGridView2.Appearance.BandPanel.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(243)))), ((int)(((byte)(244)))), ((int)(((byte)(250)))));
            this.bandedGridView2.Appearance.BandPanel.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Bold);
            this.bandedGridView2.Appearance.BandPanel.ForeColor = System.Drawing.Color.Black;
            this.bandedGridView2.Appearance.BandPanel.GradientMode = System.Drawing.Drawing2D.LinearGradientMode.Vertical;
            this.bandedGridView2.Appearance.BandPanel.Options.UseBackColor = true;
            this.bandedGridView2.Appearance.BandPanel.Options.UseBorderColor = true;
            this.bandedGridView2.Appearance.BandPanel.Options.UseFont = true;
            this.bandedGridView2.Appearance.BandPanel.Options.UseForeColor = true;
            this.bandedGridView2.Appearance.BandPanelBackground.BackColor = System.Drawing.Color.White;
            this.bandedGridView2.Appearance.BandPanelBackground.Options.UseBackColor = true;
            this.bandedGridView2.Appearance.ColumnFilterButton.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(243)))), ((int)(((byte)(244)))), ((int)(((byte)(250)))));
            this.bandedGridView2.Appearance.ColumnFilterButton.BackColor2 = System.Drawing.Color.FromArgb(((int)(((byte)(154)))), ((int)(((byte)(153)))), ((int)(((byte)(182)))));
            this.bandedGridView2.Appearance.ColumnFilterButton.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(243)))), ((int)(((byte)(244)))), ((int)(((byte)(250)))));
            this.bandedGridView2.Appearance.ColumnFilterButton.ForeColor = System.Drawing.Color.Black;
            this.bandedGridView2.Appearance.ColumnFilterButton.GradientMode = System.Drawing.Drawing2D.LinearGradientMode.Vertical;
            this.bandedGridView2.Appearance.ColumnFilterButton.Options.UseBackColor = true;
            this.bandedGridView2.Appearance.ColumnFilterButton.Options.UseBorderColor = true;
            this.bandedGridView2.Appearance.ColumnFilterButton.Options.UseForeColor = true;
            this.bandedGridView2.Appearance.ColumnFilterButtonActive.BackColor = System.Drawing.Color.White;
            this.bandedGridView2.Appearance.ColumnFilterButtonActive.BackColor2 = System.Drawing.Color.FromArgb(((int)(((byte)(198)))), ((int)(((byte)(198)))), ((int)(((byte)(215)))));
            this.bandedGridView2.Appearance.ColumnFilterButtonActive.BorderColor = System.Drawing.Color.White;
            this.bandedGridView2.Appearance.ColumnFilterButtonActive.ForeColor = System.Drawing.Color.Black;
            this.bandedGridView2.Appearance.ColumnFilterButtonActive.GradientMode = System.Drawing.Drawing2D.LinearGradientMode.Vertical;
            this.bandedGridView2.Appearance.ColumnFilterButtonActive.Options.UseBackColor = true;
            this.bandedGridView2.Appearance.ColumnFilterButtonActive.Options.UseBorderColor = true;
            this.bandedGridView2.Appearance.ColumnFilterButtonActive.Options.UseForeColor = true;
            this.bandedGridView2.Appearance.Empty.BackColor = System.Drawing.Color.White;
            this.bandedGridView2.Appearance.Empty.Options.UseBackColor = true;
            this.bandedGridView2.Appearance.EvenRow.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(243)))), ((int)(((byte)(244)))), ((int)(((byte)(250)))));
            this.bandedGridView2.Appearance.EvenRow.ForeColor = System.Drawing.Color.Black;
            this.bandedGridView2.Appearance.EvenRow.Options.UseBackColor = true;
            this.bandedGridView2.Appearance.EvenRow.Options.UseForeColor = true;
            this.bandedGridView2.Appearance.FilterCloseButton.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(243)))), ((int)(((byte)(244)))), ((int)(((byte)(250)))));
            this.bandedGridView2.Appearance.FilterCloseButton.BackColor2 = System.Drawing.Color.FromArgb(((int)(((byte)(154)))), ((int)(((byte)(153)))), ((int)(((byte)(182)))));
            this.bandedGridView2.Appearance.FilterCloseButton.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(243)))), ((int)(((byte)(244)))), ((int)(((byte)(250)))));
            this.bandedGridView2.Appearance.FilterCloseButton.GradientMode = System.Drawing.Drawing2D.LinearGradientMode.Vertical;
            this.bandedGridView2.Appearance.FilterCloseButton.Options.UseBackColor = true;
            this.bandedGridView2.Appearance.FilterCloseButton.Options.UseBorderColor = true;
            this.bandedGridView2.Appearance.FilterPanel.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(133)))), ((int)(((byte)(131)))), ((int)(((byte)(161)))));
            this.bandedGridView2.Appearance.FilterPanel.ForeColor = System.Drawing.Color.White;
            this.bandedGridView2.Appearance.FilterPanel.Options.UseBackColor = true;
            this.bandedGridView2.Appearance.FilterPanel.Options.UseForeColor = true;
            this.bandedGridView2.Appearance.FixedLine.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(124)))), ((int)(((byte)(124)))), ((int)(((byte)(148)))));
            this.bandedGridView2.Appearance.FixedLine.Options.UseBackColor = true;
            this.bandedGridView2.Appearance.FocusedRow.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(178)))), ((int)(((byte)(180)))), ((int)(((byte)(191)))));
            this.bandedGridView2.Appearance.FocusedRow.ForeColor = System.Drawing.Color.Black;
            this.bandedGridView2.Appearance.FocusedRow.Options.UseBackColor = true;
            this.bandedGridView2.Appearance.FocusedRow.Options.UseForeColor = true;
            this.bandedGridView2.Appearance.FooterPanel.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(243)))), ((int)(((byte)(244)))), ((int)(((byte)(250)))));
            this.bandedGridView2.Appearance.FooterPanel.BackColor2 = System.Drawing.Color.FromArgb(((int)(((byte)(154)))), ((int)(((byte)(153)))), ((int)(((byte)(182)))));
            this.bandedGridView2.Appearance.FooterPanel.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(243)))), ((int)(((byte)(244)))), ((int)(((byte)(250)))));
            this.bandedGridView2.Appearance.FooterPanel.GradientMode = System.Drawing.Drawing2D.LinearGradientMode.Vertical;
            this.bandedGridView2.Appearance.FooterPanel.Options.UseBackColor = true;
            this.bandedGridView2.Appearance.FooterPanel.Options.UseBorderColor = true;
            this.bandedGridView2.Appearance.GroupButton.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(215)))), ((int)(((byte)(215)))), ((int)(((byte)(229)))));
            this.bandedGridView2.Appearance.GroupButton.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(215)))), ((int)(((byte)(215)))), ((int)(((byte)(229)))));
            this.bandedGridView2.Appearance.GroupButton.ForeColor = System.Drawing.Color.Black;
            this.bandedGridView2.Appearance.GroupButton.Options.UseBackColor = true;
            this.bandedGridView2.Appearance.GroupButton.Options.UseBorderColor = true;
            this.bandedGridView2.Appearance.GroupButton.Options.UseForeColor = true;
            this.bandedGridView2.Appearance.GroupFooter.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(215)))), ((int)(((byte)(215)))), ((int)(((byte)(229)))));
            this.bandedGridView2.Appearance.GroupFooter.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(215)))), ((int)(((byte)(215)))), ((int)(((byte)(229)))));
            this.bandedGridView2.Appearance.GroupFooter.ForeColor = System.Drawing.Color.Black;
            this.bandedGridView2.Appearance.GroupFooter.Options.UseBackColor = true;
            this.bandedGridView2.Appearance.GroupFooter.Options.UseBorderColor = true;
            this.bandedGridView2.Appearance.GroupFooter.Options.UseForeColor = true;
            this.bandedGridView2.Appearance.GroupPanel.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(133)))), ((int)(((byte)(131)))), ((int)(((byte)(161)))));
            this.bandedGridView2.Appearance.GroupPanel.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(243)))), ((int)(((byte)(244)))), ((int)(((byte)(250)))));
            this.bandedGridView2.Appearance.GroupPanel.Options.UseBackColor = true;
            this.bandedGridView2.Appearance.GroupPanel.Options.UseForeColor = true;
            this.bandedGridView2.Appearance.GroupRow.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(215)))), ((int)(((byte)(215)))), ((int)(((byte)(229)))));
            this.bandedGridView2.Appearance.GroupRow.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(215)))), ((int)(((byte)(215)))), ((int)(((byte)(229)))));
            this.bandedGridView2.Appearance.GroupRow.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Bold);
            this.bandedGridView2.Appearance.GroupRow.ForeColor = System.Drawing.Color.Black;
            this.bandedGridView2.Appearance.GroupRow.Options.UseBackColor = true;
            this.bandedGridView2.Appearance.GroupRow.Options.UseBorderColor = true;
            this.bandedGridView2.Appearance.GroupRow.Options.UseFont = true;
            this.bandedGridView2.Appearance.GroupRow.Options.UseForeColor = true;
            this.bandedGridView2.Appearance.HeaderPanel.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(243)))), ((int)(((byte)(244)))), ((int)(((byte)(250)))));
            this.bandedGridView2.Appearance.HeaderPanel.BackColor2 = System.Drawing.Color.FromArgb(((int)(((byte)(154)))), ((int)(((byte)(153)))), ((int)(((byte)(182)))));
            this.bandedGridView2.Appearance.HeaderPanel.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(243)))), ((int)(((byte)(244)))), ((int)(((byte)(250)))));
            this.bandedGridView2.Appearance.HeaderPanel.ForeColor = System.Drawing.Color.Black;
            this.bandedGridView2.Appearance.HeaderPanel.GradientMode = System.Drawing.Drawing2D.LinearGradientMode.Vertical;
            this.bandedGridView2.Appearance.HeaderPanel.Options.UseBackColor = true;
            this.bandedGridView2.Appearance.HeaderPanel.Options.UseBorderColor = true;
            this.bandedGridView2.Appearance.HeaderPanel.Options.UseForeColor = true;
            this.bandedGridView2.Appearance.HeaderPanelBackground.BackColor = System.Drawing.Color.White;
            this.bandedGridView2.Appearance.HeaderPanelBackground.Options.UseBackColor = true;
            this.bandedGridView2.Appearance.HideSelectionRow.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(218)))), ((int)(((byte)(219)))), ((int)(((byte)(226)))));
            this.bandedGridView2.Appearance.HideSelectionRow.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(133)))), ((int)(((byte)(131)))), ((int)(((byte)(161)))));
            this.bandedGridView2.Appearance.HideSelectionRow.Options.UseBackColor = true;
            this.bandedGridView2.Appearance.HideSelectionRow.Options.UseForeColor = true;
            this.bandedGridView2.Appearance.HorzLine.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(164)))), ((int)(((byte)(164)))), ((int)(((byte)(188)))));
            this.bandedGridView2.Appearance.HorzLine.Options.UseBackColor = true;
            this.bandedGridView2.Appearance.OddRow.BackColor = System.Drawing.Color.White;
            this.bandedGridView2.Appearance.OddRow.ForeColor = System.Drawing.Color.Black;
            this.bandedGridView2.Appearance.OddRow.Options.UseBackColor = true;
            this.bandedGridView2.Appearance.OddRow.Options.UseForeColor = true;
            this.bandedGridView2.Appearance.Preview.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(250)))), ((int)(((byte)(250)))), ((int)(((byte)(253)))));
            this.bandedGridView2.Appearance.Preview.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(163)))), ((int)(((byte)(165)))), ((int)(((byte)(177)))));
            this.bandedGridView2.Appearance.Preview.Options.UseBackColor = true;
            this.bandedGridView2.Appearance.Preview.Options.UseForeColor = true;
            this.bandedGridView2.Appearance.Row.BackColor = System.Drawing.Color.White;
            this.bandedGridView2.Appearance.Row.ForeColor = System.Drawing.Color.Black;
            this.bandedGridView2.Appearance.Row.Options.UseBackColor = true;
            this.bandedGridView2.Appearance.Row.Options.UseForeColor = true;
            this.bandedGridView2.Appearance.RowSeparator.BackColor = System.Drawing.Color.White;
            this.bandedGridView2.Appearance.RowSeparator.Options.UseBackColor = true;
            this.bandedGridView2.Appearance.SelectedRow.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(195)))), ((int)(((byte)(197)))), ((int)(((byte)(205)))));
            this.bandedGridView2.Appearance.SelectedRow.ForeColor = System.Drawing.Color.Black;
            this.bandedGridView2.Appearance.SelectedRow.Options.UseBackColor = true;
            this.bandedGridView2.Appearance.SelectedRow.Options.UseForeColor = true;
            this.bandedGridView2.Appearance.VertLine.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(164)))), ((int)(((byte)(164)))), ((int)(((byte)(188)))));
            this.bandedGridView2.Appearance.VertLine.Options.UseBackColor = true;
            this.bandedGridView2.Bands.AddRange(new DevExpress.XtraGrid.Views.BandedGrid.GridBand[] {
            this.gridBand4,
            this.gridBandCompet,
            this.gridBand9});
            this.bandedGridView2.Columns.AddRange(new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn[] {
            this.colPGF,
            this.colPGFDia,
            this.colPGFDescricao,
            this.colPGFValor,
            this.colPGFCompetI,
            this.colPGFCompetF,
            this.colPGFForma,
            this.colPGFNominal,
            this.colPGFRef,
            this.colPGFProximaCompetencia,
            this.colFRNFantasia,
            this.colPGF0,
            this.colFRNNome,
            this.colBotPGF});
            this.bandedGridView2.GridControl = this.gridControl9;
            this.bandedGridView2.Name = "bandedGridView2";
            this.bandedGridView2.OptionsDetail.ShowDetailTabs = false;
            this.bandedGridView2.OptionsView.ColumnAutoWidth = false;
            this.bandedGridView2.OptionsView.EnableAppearanceEvenRow = true;
            this.bandedGridView2.OptionsView.EnableAppearanceOddRow = true;
            this.bandedGridView2.OptionsView.ShowAutoFilterRow = true;
            this.bandedGridView2.OptionsView.ShowFooter = true;
            this.bandedGridView2.OptionsView.ShowGroupPanel = false;
            this.bandedGridView2.RowCellStyle += new DevExpress.XtraGrid.Views.Grid.RowCellStyleEventHandler(this.bandedGridView2_RowCellStyle);
            this.bandedGridView2.CustomColumnDisplayText += new DevExpress.XtraGrid.Views.Base.CustomColumnDisplayTextEventHandler(this.bandedGridView2_CustomColumnDisplayText);
            // 
            // gridBand4
            // 
            this.gridBand4.Caption = "Pagamento";
            this.gridBand4.Columns.Add(this.colPGF);
            this.gridBand4.Columns.Add(this.colPGFDia);
            this.gridBand4.Columns.Add(this.colPGFDescricao);
            this.gridBand4.Columns.Add(this.colPGFValor);
            this.gridBand4.Columns.Add(this.colPGFCompetI);
            this.gridBand4.Columns.Add(this.colPGFCompetF);
            this.gridBand4.Columns.Add(this.colFRNNome);
            this.gridBand4.Columns.Add(this.colFRNFantasia);
            this.gridBand4.Columns.Add(this.colPGFForma);
            this.gridBand4.Columns.Add(this.colPGFNominal);
            this.gridBand4.Columns.Add(this.colPGFRef);
            this.gridBand4.Columns.Add(this.colPGFProximaCompetencia);
            this.gridBand4.Fixed = DevExpress.XtraGrid.Columns.FixedStyle.Left;
            this.gridBand4.Name = "gridBand4";
            this.gridBand4.VisibleIndex = 0;
            this.gridBand4.Width = 503;
            // 
            // colPGF
            // 
            this.colPGF.FieldName = "PGF";
            this.colPGF.Name = "colPGF";
            this.colPGF.OptionsColumn.ReadOnly = true;
            // 
            // colPGFDia
            // 
            this.colPGFDia.Caption = "Dia";
            this.colPGFDia.FieldName = "PGFDia";
            this.colPGFDia.Name = "colPGFDia";
            this.colPGFDia.OptionsColumn.ReadOnly = true;
            // 
            // colPGFDescricao
            // 
            this.colPGFDescricao.Caption = "Descrição";
            this.colPGFDescricao.FieldName = "PGFDescricao";
            this.colPGFDescricao.Name = "colPGFDescricao";
            this.colPGFDescricao.OptionsColumn.ReadOnly = true;
            this.colPGFDescricao.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Count)});
            this.colPGFDescricao.Visible = true;
            this.colPGFDescricao.Width = 165;
            // 
            // colPGFValor
            // 
            this.colPGFValor.Caption = "Valor";
            this.colPGFValor.ColumnEdit = this.repositoryItemCalcEdit4;
            this.colPGFValor.FieldName = "PGFValor";
            this.colPGFValor.Name = "colPGFValor";
            this.colPGFValor.OptionsColumn.ReadOnly = true;
            this.colPGFValor.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum, "PGFValor", "{0:n2}")});
            this.colPGFValor.Visible = true;
            this.colPGFValor.Width = 78;
            // 
            // repositoryItemCalcEdit4
            // 
            this.repositoryItemCalcEdit4.AutoHeight = false;
            this.repositoryItemCalcEdit4.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemCalcEdit4.DisplayFormat.FormatString = "n2";
            this.repositoryItemCalcEdit4.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.repositoryItemCalcEdit4.Mask.EditMask = "n2";
            this.repositoryItemCalcEdit4.Mask.UseMaskAsDisplayFormat = true;
            this.repositoryItemCalcEdit4.Name = "repositoryItemCalcEdit4";
            // 
            // colPGFCompetI
            // 
            this.colPGFCompetI.Caption = "Início";
            this.colPGFCompetI.FieldName = "PGFCompetI";
            this.colPGFCompetI.Name = "colPGFCompetI";
            this.colPGFCompetI.OptionsColumn.ReadOnly = true;
            this.colPGFCompetI.Visible = true;
            this.colPGFCompetI.Width = 50;
            // 
            // colPGFCompetF
            // 
            this.colPGFCompetF.Caption = "Término";
            this.colPGFCompetF.FieldName = "PGFCompetF";
            this.colPGFCompetF.Name = "colPGFCompetF";
            this.colPGFCompetF.OptionsColumn.ReadOnly = true;
            this.colPGFCompetF.Visible = true;
            this.colPGFCompetF.Width = 50;
            // 
            // colFRNNome
            // 
            this.colFRNNome.Caption = "Fornecedor";
            this.colFRNNome.FieldName = "FRNNome";
            this.colFRNNome.Name = "colFRNNome";
            this.colFRNNome.OptionsColumn.ReadOnly = true;
            this.colFRNNome.Visible = true;
            this.colFRNNome.Width = 160;
            // 
            // colFRNFantasia
            // 
            this.colFRNFantasia.Caption = "Fornecedor";
            this.colFRNFantasia.FieldName = "FRNFantasia";
            this.colFRNFantasia.Name = "colFRNFantasia";
            this.colFRNFantasia.OptionsColumn.ReadOnly = true;
            this.colFRNFantasia.Width = 229;
            // 
            // colPGFForma
            // 
            this.colPGFForma.FieldName = "PGFForma";
            this.colPGFForma.Name = "colPGFForma";
            this.colPGFForma.OptionsColumn.ReadOnly = true;
            // 
            // colPGFNominal
            // 
            this.colPGFNominal.FieldName = "PGFNominal";
            this.colPGFNominal.Name = "colPGFNominal";
            this.colPGFNominal.OptionsColumn.ReadOnly = true;
            // 
            // colPGFRef
            // 
            this.colPGFRef.FieldName = "PGFRef";
            this.colPGFRef.Name = "colPGFRef";
            this.colPGFRef.OptionsColumn.ReadOnly = true;
            // 
            // colPGFProximaCompetencia
            // 
            this.colPGFProximaCompetencia.FieldName = "PGFProximaCompetencia";
            this.colPGFProximaCompetencia.Name = "colPGFProximaCompetencia";
            this.colPGFProximaCompetencia.OptionsColumn.ReadOnly = true;
            // 
            // gridBandCompet
            // 
            this.gridBandCompet.Caption = "Competência do serviço";
            this.gridBandCompet.Columns.Add(this.colPGF0);
            this.gridBandCompet.Name = "gridBandCompet";
            this.gridBandCompet.VisibleIndex = 1;
            this.gridBandCompet.Width = 150;
            // 
            // colPGF0
            // 
            this.colPGF0.AppearanceHeader.Options.UseTextOptions = true;
            this.colPGF0.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colPGF0.Caption = "colPGF0";
            this.colPGF0.FieldName = "V1";
            this.colPGF0.Name = "colPGF0";
            this.colPGF0.OptionsColumn.ReadOnly = true;
            this.colPGF0.Visible = true;
            this.colPGF0.Width = 150;
            // 
            // gridBand9
            // 
            this.gridBand9.Caption = "gridBand9";
            this.gridBand9.Columns.Add(this.colBotPGF);
            this.gridBand9.Fixed = DevExpress.XtraGrid.Columns.FixedStyle.Right;
            this.gridBand9.Name = "gridBand9";
            this.gridBand9.OptionsBand.ShowCaption = false;
            this.gridBand9.VisibleIndex = 2;
            this.gridBand9.Width = 30;
            // 
            // colBotPGF
            // 
            this.colBotPGF.Caption = "bandedGridColumn5";
            this.colBotPGF.ColumnEdit = this.BotPGF;
            this.colBotPGF.Name = "colBotPGF";
            this.colBotPGF.OptionsColumn.ShowCaption = false;
            this.colBotPGF.Visible = true;
            this.colBotPGF.Width = 30;
            // 
            // BotPGF
            // 
            this.BotPGF.AutoHeight = false;
            editorButtonImageOptions1.Image = ((System.Drawing.Image)(resources.GetObject("editorButtonImageOptions1.Image")));
            this.BotPGF.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(editorButtonImageOptions1, DevExpress.XtraEditors.Controls.ButtonPredefines.Glyph, null)});
            this.BotPGF.Name = "BotPGF";
            this.BotPGF.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.HideTextEditor;
            this.BotPGF.ButtonClick += new DevExpress.XtraEditors.Controls.ButtonPressedEventHandler(this.BotPGF_ButtonClick);
            // 
            // BotCHE
            // 
            this.BotCHE.AutoHeight = false;
            editorButtonImageOptions2.Image = ((System.Drawing.Image)(resources.GetObject("editorButtonImageOptions2.Image")));
            this.BotCHE.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(editorButtonImageOptions2, DevExpress.XtraEditors.Controls.ButtonPredefines.Glyph, null)});
            this.BotCHE.Name = "BotCHE";
            this.BotCHE.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.HideTextEditor;
            this.BotCHE.ButtonClick += new DevExpress.XtraEditors.Controls.ButtonPressedEventHandler(this.BotCHE_ButtonClick);
            // 
            // dBalanceteBindingSource
            // 
            this.dBalanceteBindingSource.DataSource = typeof(Balancete.GrBalancete.dBalancete);
            this.dBalanceteBindingSource.Position = 0;
            // 
            // panelControl1
            // 
            this.panelControl1.Controls.Add(this.chCPMF);
            this.panelControl1.Controls.Add(this.dropDownButton1);
            this.panelControl1.Controls.Add(this.chInadDet);
            this.panelControl1.Controls.Add(this.simpleButton8);
            this.panelControl1.Controls.Add(this.simpleButton7);
            this.panelControl1.Controls.Add(this.chCapa);
            this.panelControl1.Controls.Add(this.checkEdit1);
            this.panelControl1.Controls.Add(this.cHDicionario);
            this.panelControl1.Controls.Add(this.simpleButton2);
            this.panelControl1.Controls.Add(this.lookMBA);
            this.panelControl1.Controls.Add(this.labelControl1);
            this.panelControl1.Controls.Add(this.StatusBAL);
            this.panelControl1.Controls.Add(this.cBotaoImp1);
            this.panelControl1.Controls.Add(this.checkTF);
            this.panelControl1.Controls.Add(this.chExtratos);
            this.panelControl1.Controls.Add(this.chInadimplencia);
            this.panelControl1.Controls.Add(this.chPrevisto);
            this.panelControl1.Controls.Add(this.chCreditosAntigo);
            this.panelControl1.Controls.Add(this.chSaldos);
            this.panelControl1.Controls.Add(this.chDetalhamentoCreditos);
            this.panelControl1.Controls.Add(this.chDebitosCreditos);
            this.panelControl1.Controls.Add(this.simpleButton6);
            this.panelControl1.Controls.Add(this.simpleButton4);
            this.panelControl1.Controls.Add(this.simpleButton3);
            this.panelControl1.Controls.Add(this.cBotaoImpBol1);
            this.panelControl1.Controls.Add(this.simpleButton1);
            this.panelControl1.Dock = System.Windows.Forms.DockStyle.Top;
            this.panelControl1.Location = new System.Drawing.Point(0, 88);
            this.panelControl1.Name = "panelControl1";
            this.panelControl1.Size = new System.Drawing.Size(1452, 116);
            this.panelControl1.TabIndex = 0;
            // 
            // chCPMF
            // 
            this.chCPMF.Location = new System.Drawing.Point(456, 28);
            this.chCPMF.Name = "chCPMF";
            this.chCPMF.Properties.Caption = "CPMF";
            this.chCPMF.Size = new System.Drawing.Size(92, 19);
            this.chCPMF.TabIndex = 32;
            // 
            // dropDownButton1
            // 
            this.dropDownButton1.DropDownArrowStyle = DevExpress.XtraEditors.DropDownArrowStyle.SplitButton;
            this.dropDownButton1.DropDownControl = this.popupComprovantes;
            this.dropDownButton1.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("dropDownButton1.ImageOptions.Image")));
            this.dropDownButton1.Location = new System.Drawing.Point(901, 56);
            this.dropDownButton1.Name = "dropDownButton1";
            this.dropDownButton1.Size = new System.Drawing.Size(160, 46);
            this.dropDownButton1.TabIndex = 31;
            this.dropDownButton1.Text = "Comprovantes";
            this.dropDownButton1.Click += new System.EventHandler(this.simpleButton9_Click);
            // 
            // popupComprovantes
            // 
            this.popupComprovantes.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(this.barButtonItemImprimir),
            new DevExpress.XtraBars.LinkPersistInfo(this.barButtonItemTela)});
            this.popupComprovantes.Manager = this.barManager1;
            this.popupComprovantes.Name = "popupComprovantes";
            // 
            // barButtonItemImprimir
            // 
            this.barButtonItemImprimir.Caption = "Imprimir";
            this.barButtonItemImprimir.Id = 0;
            this.barButtonItemImprimir.ImageOptions.DisabledImage = global::Balancete.Properties.Resources.BtnImprimir_F_Glyph;
            this.barButtonItemImprimir.ImageOptions.DisabledLargeImage = global::Balancete.Properties.Resources.impCheque1;
            this.barButtonItemImprimir.ImageOptions.Image = global::Balancete.Properties.Resources.imprimir;
            this.barButtonItemImprimir.Name = "barButtonItemImprimir";
            this.barButtonItemImprimir.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.barButtonItemImprimir_ItemClick);
            // 
            // barButtonItemTela
            // 
            this.barButtonItemTela.Caption = "Abrir";
            this.barButtonItemTela.Id = 1;
            this.barButtonItemTela.ImageOptions.Image = global::Balancete.Properties.Resources.tela;
            this.barButtonItemTela.Name = "barButtonItemTela";
            this.barButtonItemTela.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.barButtonItemTela_ItemClick);
            // 
            // barManager1
            // 
            this.barManager1.DockControls.Add(this.barDockControlTop);
            this.barManager1.DockControls.Add(this.barDockControlBottom);
            this.barManager1.DockControls.Add(this.barDockControlLeft);
            this.barManager1.DockControls.Add(this.barDockControlRight);
            this.barManager1.Form = this;
            this.barManager1.Items.AddRange(new DevExpress.XtraBars.BarItem[] {
            this.barButtonItemImprimir,
            this.barButtonItemTela});
            this.barManager1.MaxItemId = 2;
            // 
            // barDockControlTop
            // 
            this.barDockControlTop.CausesValidation = false;
            this.barDockControlTop.Dock = System.Windows.Forms.DockStyle.Top;
            this.barDockControlTop.Location = new System.Drawing.Point(0, 0);
            this.barDockControlTop.Manager = this.barManager1;
            this.barDockControlTop.Size = new System.Drawing.Size(1452, 0);
            // 
            // barDockControlBottom
            // 
            this.barDockControlBottom.CausesValidation = false;
            this.barDockControlBottom.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.barDockControlBottom.Location = new System.Drawing.Point(0, 807);
            this.barDockControlBottom.Manager = this.barManager1;
            this.barDockControlBottom.Size = new System.Drawing.Size(1452, 0);
            // 
            // barDockControlLeft
            // 
            this.barDockControlLeft.CausesValidation = false;
            this.barDockControlLeft.Dock = System.Windows.Forms.DockStyle.Left;
            this.barDockControlLeft.Location = new System.Drawing.Point(0, 0);
            this.barDockControlLeft.Manager = this.barManager1;
            this.barDockControlLeft.Size = new System.Drawing.Size(0, 807);
            // 
            // barDockControlRight
            // 
            this.barDockControlRight.CausesValidation = false;
            this.barDockControlRight.Dock = System.Windows.Forms.DockStyle.Right;
            this.barDockControlRight.Location = new System.Drawing.Point(1452, 0);
            this.barDockControlRight.Manager = this.barManager1;
            this.barDockControlRight.Size = new System.Drawing.Size(0, 807);
            // 
            // chInadDet
            // 
            this.chInadDet.Location = new System.Drawing.Point(360, 26);
            this.chInadDet.Name = "chInadDet";
            this.chInadDet.Properties.Caption = "Inad. Det.";
            this.chInadDet.Size = new System.Drawing.Size(90, 19);
            this.chInadDet.TabIndex = 29;
            // 
            // simpleButton8
            // 
            this.simpleButton8.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("simpleButton8.ImageOptions.Image")));
            this.simpleButton8.Location = new System.Drawing.Point(901, 6);
            this.simpleButton8.Name = "simpleButton8";
            this.simpleButton8.Size = new System.Drawing.Size(160, 44);
            this.simpleButton8.TabIndex = 28;
            this.simpleButton8.Text = "Exportar";
            this.simpleButton8.Visible = false;
            this.simpleButton8.Click += new System.EventHandler(this.GeraArquivosExp);
            // 
            // simpleButton7
            // 
            this.simpleButton7.Location = new System.Drawing.Point(735, 88);
            this.simpleButton7.Name = "simpleButton7";
            this.simpleButton7.Size = new System.Drawing.Size(160, 23);
            this.simpleButton7.TabIndex = 27;
            this.simpleButton7.Text = "Tranferência Arrecadado";
            this.simpleButton7.Click += new System.EventHandler(this.simpleButton7_Click);
            // 
            // chCapa
            // 
            this.chCapa.EditValue = true;
            this.chCapa.Location = new System.Drawing.Point(157, 5);
            this.chCapa.Name = "chCapa";
            this.chCapa.Properties.Caption = "Capa";
            this.chCapa.Size = new System.Drawing.Size(109, 19);
            this.chCapa.TabIndex = 26;
            // 
            // checkEdit1
            // 
            this.checkEdit1.EditValue = true;
            this.checkEdit1.Location = new System.Drawing.Point(456, 5);
            this.checkEdit1.Name = "checkEdit1";
            this.checkEdit1.Properties.Caption = "Francesinhas";
            this.checkEdit1.Size = new System.Drawing.Size(92, 19);
            this.checkEdit1.TabIndex = 25;
            this.checkEdit1.Visible = false;
            // 
            // cHDicionario
            // 
            this.cHDicionario.Location = new System.Drawing.Point(5, 28);
            this.cHDicionario.Name = "cHDicionario";
            this.cHDicionario.Properties.Caption = "Usar corretor ortográfico";
            this.cHDicionario.Size = new System.Drawing.Size(142, 19);
            this.cHDicionario.TabIndex = 24;
            this.cHDicionario.CheckedChanged += new System.EventHandler(this.cHDicionario_CheckedChanged);
            // 
            // simpleButton2
            // 
            this.simpleButton2.ImageOptions.Image = global::Balancete.Properties.Resources.Bboleto;
            this.simpleButton2.Location = new System.Drawing.Point(552, 88);
            this.simpleButton2.Name = "simpleButton2";
            this.simpleButton2.Size = new System.Drawing.Size(160, 23);
            this.simpleButton2.TabIndex = 23;
            this.simpleButton2.Text = "Boleto";
            this.simpleButton2.Click += new System.EventHandler(this.simpleButton2_Click);
            // 
            // lookMBA
            // 
            this.lookMBA.Location = new System.Drawing.Point(203, 87);
            this.lookMBA.Name = "lookMBA";
            this.lookMBA.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo),
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Delete),
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Undo)});
            this.lookMBA.Properties.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("MBANome", "MBA Nome", 61, DevExpress.Utils.FormatType.None, "", true, DevExpress.Utils.HorzAlignment.Near)});
            this.lookMBA.Properties.DataSource = this.modeloBAlanceteBindingSource;
            this.lookMBA.Properties.DisplayMember = "MBANome";
            this.lookMBA.Properties.NullText = "Padrão";
            this.lookMBA.Properties.ShowHeader = false;
            this.lookMBA.Properties.ValueMember = "MBA";
            this.lookMBA.Properties.EditValueChanged += new System.EventHandler(this.lookMBA_Properties_EditValueChanged);
            this.lookMBA.Size = new System.Drawing.Size(318, 20);
            this.lookMBA.TabIndex = 22;
            this.lookMBA.ButtonClick += new DevExpress.XtraEditors.Controls.ButtonPressedEventHandler(this.lookMBA_ButtonClick);
            // 
            // modeloBAlanceteBindingSource
            // 
            this.modeloBAlanceteBindingSource.DataMember = "ModeloBAlancete";
            this.modeloBAlanceteBindingSource.DataSource = typeof(Framework.datasets.dModelosBAlancete);
            // 
            // labelControl1
            // 
            this.labelControl1.Location = new System.Drawing.Point(159, 90);
            this.labelControl1.Name = "labelControl1";
            this.labelControl1.Size = new System.Drawing.Size(38, 13);
            this.labelControl1.TabIndex = 21;
            this.labelControl1.Text = "Modelo:";
            // 
            // StatusBAL
            // 
            this.StatusBAL.Location = new System.Drawing.Point(552, 9);
            this.StatusBAL.Name = "StatusBAL";
            this.StatusBAL.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.StatusBAL.Properties.Appearance.Options.UseFont = true;
            this.StatusBAL.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo, "", -1, true, false, false, editorButtonImageOptions13),
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Left, "", -1, true, true, true, editorButtonImageOptions14),
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Right)});
            this.StatusBAL.Properties.ReadOnly = true;
            this.StatusBAL.Properties.ButtonClick += new DevExpress.XtraEditors.Controls.ButtonPressedEventHandler(this.StatusBAL_Properties_ButtonClick);
            this.StatusBAL.Size = new System.Drawing.Size(160, 30);
            this.StatusBAL.TabIndex = 20;
            // 
            // cBotaoImp1
            // 
            this.cBotaoImp1.AbrirArquivoExportado = true;
            this.cBotaoImp1.AcaoBotao = dllBotao.Botao.imprimir;
            this.cBotaoImp1.AssuntoEmail = "Balancete";
            this.cBotaoImp1.BotaoEmail = true;
            this.cBotaoImp1.BotaoImprimir = true;
            this.cBotaoImp1.BotaoPDF = true;
            this.cBotaoImp1.BotaoTela = true;
            this.cBotaoImp1.CorpoEmail = null;
            this.cBotaoImp1.CreateDocAutomatico = false;
            this.cBotaoImp1.Impresso = null;
            this.cBotaoImp1.Location = new System.Drawing.Point(9, 74);
            this.cBotaoImp1.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.cBotaoImp1.Name = "cBotaoImp1";
            this.cBotaoImp1.NaoUsarxlsX = true;
            this.cBotaoImp1.NomeArquivoAnexo = "Balancete";
            this.cBotaoImp1.PriComp = null;
            this.cBotaoImp1.Size = new System.Drawing.Size(131, 29);
            this.cBotaoImp1.TabIndex = 2;
            this.cBotaoImp1.Titulo = "Imprimir";
            this.cBotaoImp1.clicado += new dllBotao.BotaoEventHandler(this.cBotaoImp1_clicado);
            // 
            // checkTF
            // 
            this.checkTF.EditValue = true;
            this.checkTF.Location = new System.Drawing.Point(5, 3);
            this.checkTF.Name = "checkTF";
            this.checkTF.Properties.Caption = "Agrupar transferências";
            this.checkTF.Size = new System.Drawing.Size(135, 19);
            this.checkTF.TabIndex = 18;
            this.checkTF.CheckedChanged += new System.EventHandler(this.checkTF_CheckedChanged);
            // 
            // chExtratos
            // 
            this.chExtratos.EditValue = true;
            this.chExtratos.Location = new System.Drawing.Point(360, 51);
            this.chExtratos.Name = "chExtratos";
            this.chExtratos.Properties.Caption = "Extratos";
            this.chExtratos.Size = new System.Drawing.Size(66, 19);
            this.chExtratos.TabIndex = 17;
            // 
            // chInadimplencia
            // 
            this.chInadimplencia.EditValue = true;
            this.chInadimplencia.Location = new System.Drawing.Point(360, 3);
            this.chInadimplencia.Name = "chInadimplencia";
            this.chInadimplencia.Properties.Caption = "Inadimplência";
            this.chInadimplencia.Size = new System.Drawing.Size(90, 19);
            this.chInadimplencia.TabIndex = 15;
            // 
            // chPrevisto
            // 
            this.chPrevisto.EditValue = true;
            this.chPrevisto.Location = new System.Drawing.Point(272, 52);
            this.chPrevisto.Name = "chPrevisto";
            this.chPrevisto.Properties.Caption = "Previsto";
            this.chPrevisto.Size = new System.Drawing.Size(82, 19);
            this.chPrevisto.TabIndex = 14;
            // 
            // chCreditosAntigo
            // 
            this.chCreditosAntigo.EditValue = true;
            this.chCreditosAntigo.Location = new System.Drawing.Point(156, 28);
            this.chCreditosAntigo.Name = "chCreditosAntigo";
            this.chCreditosAntigo.Properties.Caption = "Créditos Quad.";
            this.chCreditosAntigo.Size = new System.Drawing.Size(110, 19);
            this.chCreditosAntigo.TabIndex = 13;
            // 
            // chSaldos
            // 
            this.chSaldos.EditValue = true;
            this.chSaldos.Location = new System.Drawing.Point(272, 3);
            this.chSaldos.Name = "chSaldos";
            this.chSaldos.Properties.Caption = "Saldos";
            this.chSaldos.Size = new System.Drawing.Size(82, 19);
            this.chSaldos.TabIndex = 12;
            // 
            // chDetalhamentoCreditos
            // 
            this.chDetalhamentoCreditos.EditValue = true;
            this.chDetalhamentoCreditos.Location = new System.Drawing.Point(272, 28);
            this.chDetalhamentoCreditos.Name = "chDetalhamentoCreditos";
            this.chDetalhamentoCreditos.Properties.Caption = "Créditos Det.";
            this.chDetalhamentoCreditos.Size = new System.Drawing.Size(82, 19);
            this.chDetalhamentoCreditos.TabIndex = 11;
            // 
            // chDebitosCreditos
            // 
            this.chDebitosCreditos.EditValue = true;
            this.chDebitosCreditos.Location = new System.Drawing.Point(156, 52);
            this.chDebitosCreditos.Name = "chDebitosCreditos";
            this.chDebitosCreditos.Properties.Caption = "Débitos / Créditos";
            this.chDebitosCreditos.Size = new System.Drawing.Size(110, 19);
            this.chDebitosCreditos.TabIndex = 10;
            // 
            // simpleButton6
            // 
            this.simpleButton6.Location = new System.Drawing.Point(735, 38);
            this.simpleButton6.Name = "simpleButton6";
            this.simpleButton6.Size = new System.Drawing.Size(160, 44);
            this.simpleButton6.TabIndex = 8;
            this.simpleButton6.Text = "Tranferência Lógica";
            this.simpleButton6.Click += new System.EventHandler(this.simpleButton6_Click);
            // 
            // simpleButton4
            // 
            this.simpleButton4.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("simpleButton4.ImageOptions.Image")));
            this.simpleButton4.Location = new System.Drawing.Point(552, 38);
            this.simpleButton4.Name = "simpleButton4";
            this.simpleButton4.Size = new System.Drawing.Size(160, 44);
            this.simpleButton4.TabIndex = 6;
            this.simpleButton4.Text = "Exportar";
            this.simpleButton4.Click += new System.EventHandler(this.simpleButton4_Click);
            // 
            // simpleButton3
            // 
            this.simpleButton3.Location = new System.Drawing.Point(9, 49);
            this.simpleButton3.Name = "simpleButton3";
            this.simpleButton3.Size = new System.Drawing.Size(131, 23);
            this.simpleButton3.TabIndex = 5;
            this.simpleButton3.Text = "Atualizar";
            this.simpleButton3.Click += new System.EventHandler(this.simpleButton3_Click);
            // 
            // cBotaoImpBol1
            // 
            this.cBotaoImpBol1.ItemComprovante = false;
            this.cBotaoImpBol1.Location = new System.Drawing.Point(735, 4);
            this.cBotaoImpBol1.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.cBotaoImpBol1.Name = "cBotaoImpBol1";
            this.cBotaoImpBol1.Size = new System.Drawing.Size(160, 27);
            this.cBotaoImpBol1.TabIndex = 4;
            this.cBotaoImpBol1.Titulo = "Extratos";
            this.cBotaoImpBol1.clicado += new System.EventHandler(this.cBotaoImpBol1_clicado);
            // 
            // simpleButton1
            // 
            this.simpleButton1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.simpleButton1.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("simpleButton1.ImageOptions.Image")));
            this.simpleButton1.Location = new System.Drawing.Point(1374, 7);
            this.simpleButton1.Name = "simpleButton1";
            this.simpleButton1.Size = new System.Drawing.Size(75, 23);
            this.simpleButton1.TabIndex = 0;
            this.simpleButton1.Text = "Fechar";
            this.simpleButton1.Click += new System.EventHandler(this.simpleButton1_Click);
            // 
            // zoomTrackBarControl1
            // 
            this.zoomTrackBarControl1.Dock = System.Windows.Forms.DockStyle.Right;
            this.zoomTrackBarControl1.EditValue = 11;
            this.zoomTrackBarControl1.Location = new System.Drawing.Point(1306, 2);
            this.zoomTrackBarControl1.Name = "zoomTrackBarControl1";
            this.zoomTrackBarControl1.Properties.Maximum = 22;
            this.zoomTrackBarControl1.Properties.Middle = 14;
            this.zoomTrackBarControl1.Properties.Minimum = 6;
            this.zoomTrackBarControl1.Size = new System.Drawing.Size(144, 22);
            this.zoomTrackBarControl1.TabIndex = 19;
            this.zoomTrackBarControl1.Value = 11;
            this.zoomTrackBarControl1.EditValueChanged += new System.EventHandler(this.zoomTrackBarControl1_EditValueChanged);
            // 
            // gridControl1
            // 
            this.gridControl1.DataMember = "TabelaVirtual";
            this.gridControl1.DataSource = this.dBalanceteBindingSource;
            this.gridControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridControl1.Location = new System.Drawing.Point(0, 0);
            this.gridControl1.MainView = this.bandedGridView1;
            this.gridControl1.Name = "gridControl1";
            this.gridControl1.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemCalcEdit1,
            this.repositoryItemImageComboBox1,
            this.repositoryItemImageComboBox2,
            this.repositoryItemLookUpPLA,
            this.repositoryItemMemoExEdit1,
            this.repositoryItemButtonEdit1,
            this.repositoryItemLookUpEditCTL,
            this.IMGTipos,
            this.TextEditCorretor70,
            this.repositoryItemBotCHE,
            this.repositoryItemBotNoa,
            this.repositoryItemBotPGF,
            this.repositoryItemButtonPLUS,
            this.repositoryItemCheckEdit1,
            this.repositoryDelEst,
            this.repositoryNULO});
            this.gridControl1.Size = new System.Drawing.Size(1446, 549);
            this.gridControl1.TabIndex = 1;
            this.gridControl1.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.bandedGridView1});
            // 
            // bandedGridView1
            // 
            this.bandedGridView1.Appearance.BandPanel.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(221)))), ((int)(((byte)(236)))), ((int)(((byte)(254)))));
            this.bandedGridView1.Appearance.BandPanel.BackColor2 = System.Drawing.Color.FromArgb(((int)(((byte)(132)))), ((int)(((byte)(171)))), ((int)(((byte)(228)))));
            this.bandedGridView1.Appearance.BandPanel.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(221)))), ((int)(((byte)(236)))), ((int)(((byte)(254)))));
            this.bandedGridView1.Appearance.BandPanel.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Bold);
            this.bandedGridView1.Appearance.BandPanel.ForeColor = System.Drawing.Color.Black;
            this.bandedGridView1.Appearance.BandPanel.GradientMode = System.Drawing.Drawing2D.LinearGradientMode.Vertical;
            this.bandedGridView1.Appearance.BandPanel.Options.UseBackColor = true;
            this.bandedGridView1.Appearance.BandPanel.Options.UseBorderColor = true;
            this.bandedGridView1.Appearance.BandPanel.Options.UseFont = true;
            this.bandedGridView1.Appearance.BandPanel.Options.UseForeColor = true;
            this.bandedGridView1.Appearance.BandPanelBackground.BackColor = System.Drawing.Color.White;
            this.bandedGridView1.Appearance.BandPanelBackground.Options.UseBackColor = true;
            this.bandedGridView1.Appearance.ColumnFilterButton.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(221)))), ((int)(((byte)(236)))), ((int)(((byte)(254)))));
            this.bandedGridView1.Appearance.ColumnFilterButton.BackColor2 = System.Drawing.Color.FromArgb(((int)(((byte)(132)))), ((int)(((byte)(171)))), ((int)(((byte)(228)))));
            this.bandedGridView1.Appearance.ColumnFilterButton.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(221)))), ((int)(((byte)(236)))), ((int)(((byte)(254)))));
            this.bandedGridView1.Appearance.ColumnFilterButton.ForeColor = System.Drawing.Color.Black;
            this.bandedGridView1.Appearance.ColumnFilterButton.GradientMode = System.Drawing.Drawing2D.LinearGradientMode.Vertical;
            this.bandedGridView1.Appearance.ColumnFilterButton.Options.UseBackColor = true;
            this.bandedGridView1.Appearance.ColumnFilterButton.Options.UseBorderColor = true;
            this.bandedGridView1.Appearance.ColumnFilterButton.Options.UseForeColor = true;
            this.bandedGridView1.Appearance.ColumnFilterButtonActive.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(247)))), ((int)(((byte)(251)))), ((int)(((byte)(255)))));
            this.bandedGridView1.Appearance.ColumnFilterButtonActive.BackColor2 = System.Drawing.Color.FromArgb(((int)(((byte)(154)))), ((int)(((byte)(190)))), ((int)(((byte)(243)))));
            this.bandedGridView1.Appearance.ColumnFilterButtonActive.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(247)))), ((int)(((byte)(251)))), ((int)(((byte)(255)))));
            this.bandedGridView1.Appearance.ColumnFilterButtonActive.ForeColor = System.Drawing.Color.Black;
            this.bandedGridView1.Appearance.ColumnFilterButtonActive.GradientMode = System.Drawing.Drawing2D.LinearGradientMode.Vertical;
            this.bandedGridView1.Appearance.ColumnFilterButtonActive.Options.UseBackColor = true;
            this.bandedGridView1.Appearance.ColumnFilterButtonActive.Options.UseBorderColor = true;
            this.bandedGridView1.Appearance.ColumnFilterButtonActive.Options.UseForeColor = true;
            this.bandedGridView1.Appearance.Empty.BackColor = System.Drawing.Color.White;
            this.bandedGridView1.Appearance.Empty.Options.UseBackColor = true;
            this.bandedGridView1.Appearance.EvenRow.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(231)))), ((int)(((byte)(242)))), ((int)(((byte)(254)))));
            this.bandedGridView1.Appearance.EvenRow.ForeColor = System.Drawing.Color.Black;
            this.bandedGridView1.Appearance.EvenRow.Options.UseBackColor = true;
            this.bandedGridView1.Appearance.EvenRow.Options.UseForeColor = true;
            this.bandedGridView1.Appearance.FilterCloseButton.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(221)))), ((int)(((byte)(236)))), ((int)(((byte)(254)))));
            this.bandedGridView1.Appearance.FilterCloseButton.BackColor2 = System.Drawing.Color.FromArgb(((int)(((byte)(132)))), ((int)(((byte)(171)))), ((int)(((byte)(228)))));
            this.bandedGridView1.Appearance.FilterCloseButton.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(221)))), ((int)(((byte)(236)))), ((int)(((byte)(254)))));
            this.bandedGridView1.Appearance.FilterCloseButton.ForeColor = System.Drawing.Color.Black;
            this.bandedGridView1.Appearance.FilterCloseButton.GradientMode = System.Drawing.Drawing2D.LinearGradientMode.Vertical;
            this.bandedGridView1.Appearance.FilterCloseButton.Options.UseBackColor = true;
            this.bandedGridView1.Appearance.FilterCloseButton.Options.UseBorderColor = true;
            this.bandedGridView1.Appearance.FilterCloseButton.Options.UseForeColor = true;
            this.bandedGridView1.Appearance.FilterPanel.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(62)))), ((int)(((byte)(109)))), ((int)(((byte)(185)))));
            this.bandedGridView1.Appearance.FilterPanel.ForeColor = System.Drawing.Color.White;
            this.bandedGridView1.Appearance.FilterPanel.Options.UseBackColor = true;
            this.bandedGridView1.Appearance.FilterPanel.Options.UseForeColor = true;
            this.bandedGridView1.Appearance.FixedLine.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(59)))), ((int)(((byte)(97)))), ((int)(((byte)(156)))));
            this.bandedGridView1.Appearance.FixedLine.Options.UseBackColor = true;
            this.bandedGridView1.Appearance.FocusedCell.BackColor = System.Drawing.Color.White;
            this.bandedGridView1.Appearance.FocusedCell.ForeColor = System.Drawing.Color.Black;
            this.bandedGridView1.Appearance.FocusedCell.Options.UseBackColor = true;
            this.bandedGridView1.Appearance.FocusedCell.Options.UseForeColor = true;
            this.bandedGridView1.Appearance.FocusedRow.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(49)))), ((int)(((byte)(106)))), ((int)(((byte)(197)))));
            this.bandedGridView1.Appearance.FocusedRow.ForeColor = System.Drawing.Color.White;
            this.bandedGridView1.Appearance.FocusedRow.Options.UseBackColor = true;
            this.bandedGridView1.Appearance.FocusedRow.Options.UseForeColor = true;
            this.bandedGridView1.Appearance.FooterPanel.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(221)))), ((int)(((byte)(236)))), ((int)(((byte)(254)))));
            this.bandedGridView1.Appearance.FooterPanel.BackColor2 = System.Drawing.Color.FromArgb(((int)(((byte)(132)))), ((int)(((byte)(171)))), ((int)(((byte)(228)))));
            this.bandedGridView1.Appearance.FooterPanel.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(221)))), ((int)(((byte)(236)))), ((int)(((byte)(254)))));
            this.bandedGridView1.Appearance.FooterPanel.ForeColor = System.Drawing.Color.Black;
            this.bandedGridView1.Appearance.FooterPanel.GradientMode = System.Drawing.Drawing2D.LinearGradientMode.Vertical;
            this.bandedGridView1.Appearance.FooterPanel.Options.UseBackColor = true;
            this.bandedGridView1.Appearance.FooterPanel.Options.UseBorderColor = true;
            this.bandedGridView1.Appearance.FooterPanel.Options.UseForeColor = true;
            this.bandedGridView1.Appearance.GroupButton.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(193)))), ((int)(((byte)(216)))), ((int)(((byte)(247)))));
            this.bandedGridView1.Appearance.GroupButton.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(193)))), ((int)(((byte)(216)))), ((int)(((byte)(247)))));
            this.bandedGridView1.Appearance.GroupButton.ForeColor = System.Drawing.Color.Black;
            this.bandedGridView1.Appearance.GroupButton.Options.UseBackColor = true;
            this.bandedGridView1.Appearance.GroupButton.Options.UseBorderColor = true;
            this.bandedGridView1.Appearance.GroupButton.Options.UseForeColor = true;
            this.bandedGridView1.Appearance.GroupFooter.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(193)))), ((int)(((byte)(216)))), ((int)(((byte)(247)))));
            this.bandedGridView1.Appearance.GroupFooter.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(193)))), ((int)(((byte)(216)))), ((int)(((byte)(247)))));
            this.bandedGridView1.Appearance.GroupFooter.ForeColor = System.Drawing.Color.Black;
            this.bandedGridView1.Appearance.GroupFooter.Options.UseBackColor = true;
            this.bandedGridView1.Appearance.GroupFooter.Options.UseBorderColor = true;
            this.bandedGridView1.Appearance.GroupFooter.Options.UseForeColor = true;
            this.bandedGridView1.Appearance.GroupPanel.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(62)))), ((int)(((byte)(109)))), ((int)(((byte)(185)))));
            this.bandedGridView1.Appearance.GroupPanel.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(221)))), ((int)(((byte)(236)))), ((int)(((byte)(254)))));
            this.bandedGridView1.Appearance.GroupPanel.Options.UseBackColor = true;
            this.bandedGridView1.Appearance.GroupPanel.Options.UseForeColor = true;
            this.bandedGridView1.Appearance.GroupRow.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(193)))), ((int)(((byte)(216)))), ((int)(((byte)(247)))));
            this.bandedGridView1.Appearance.GroupRow.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(193)))), ((int)(((byte)(216)))), ((int)(((byte)(247)))));
            this.bandedGridView1.Appearance.GroupRow.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Bold);
            this.bandedGridView1.Appearance.GroupRow.ForeColor = System.Drawing.Color.Black;
            this.bandedGridView1.Appearance.GroupRow.Options.UseBackColor = true;
            this.bandedGridView1.Appearance.GroupRow.Options.UseBorderColor = true;
            this.bandedGridView1.Appearance.GroupRow.Options.UseFont = true;
            this.bandedGridView1.Appearance.GroupRow.Options.UseForeColor = true;
            this.bandedGridView1.Appearance.HeaderPanel.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(221)))), ((int)(((byte)(236)))), ((int)(((byte)(254)))));
            this.bandedGridView1.Appearance.HeaderPanel.BackColor2 = System.Drawing.Color.FromArgb(((int)(((byte)(132)))), ((int)(((byte)(171)))), ((int)(((byte)(228)))));
            this.bandedGridView1.Appearance.HeaderPanel.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(221)))), ((int)(((byte)(236)))), ((int)(((byte)(254)))));
            this.bandedGridView1.Appearance.HeaderPanel.ForeColor = System.Drawing.Color.Black;
            this.bandedGridView1.Appearance.HeaderPanel.GradientMode = System.Drawing.Drawing2D.LinearGradientMode.Vertical;
            this.bandedGridView1.Appearance.HeaderPanel.Options.UseBackColor = true;
            this.bandedGridView1.Appearance.HeaderPanel.Options.UseBorderColor = true;
            this.bandedGridView1.Appearance.HeaderPanel.Options.UseForeColor = true;
            this.bandedGridView1.Appearance.HeaderPanelBackground.BackColor = System.Drawing.Color.White;
            this.bandedGridView1.Appearance.HeaderPanelBackground.Options.UseBackColor = true;
            this.bandedGridView1.Appearance.HideSelectionRow.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(106)))), ((int)(((byte)(153)))), ((int)(((byte)(228)))));
            this.bandedGridView1.Appearance.HideSelectionRow.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(208)))), ((int)(((byte)(224)))), ((int)(((byte)(251)))));
            this.bandedGridView1.Appearance.HideSelectionRow.Options.UseBackColor = true;
            this.bandedGridView1.Appearance.HideSelectionRow.Options.UseForeColor = true;
            this.bandedGridView1.Appearance.HorzLine.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(99)))), ((int)(((byte)(127)))), ((int)(((byte)(196)))));
            this.bandedGridView1.Appearance.HorzLine.Options.UseBackColor = true;
            this.bandedGridView1.Appearance.OddRow.BackColor = System.Drawing.Color.White;
            this.bandedGridView1.Appearance.OddRow.ForeColor = System.Drawing.Color.Black;
            this.bandedGridView1.Appearance.OddRow.Options.UseBackColor = true;
            this.bandedGridView1.Appearance.OddRow.Options.UseForeColor = true;
            this.bandedGridView1.Appearance.Preview.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(249)))), ((int)(((byte)(252)))), ((int)(((byte)(255)))));
            this.bandedGridView1.Appearance.Preview.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(88)))), ((int)(((byte)(129)))), ((int)(((byte)(185)))));
            this.bandedGridView1.Appearance.Preview.Options.UseBackColor = true;
            this.bandedGridView1.Appearance.Preview.Options.UseForeColor = true;
            this.bandedGridView1.Appearance.Row.BackColor = System.Drawing.Color.White;
            this.bandedGridView1.Appearance.Row.ForeColor = System.Drawing.Color.Black;
            this.bandedGridView1.Appearance.Row.Options.UseBackColor = true;
            this.bandedGridView1.Appearance.Row.Options.UseFont = true;
            this.bandedGridView1.Appearance.Row.Options.UseForeColor = true;
            this.bandedGridView1.Appearance.RowSeparator.BackColor = System.Drawing.Color.White;
            this.bandedGridView1.Appearance.RowSeparator.Options.UseBackColor = true;
            this.bandedGridView1.Appearance.SelectedRow.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(69)))), ((int)(((byte)(126)))), ((int)(((byte)(217)))));
            this.bandedGridView1.Appearance.SelectedRow.ForeColor = System.Drawing.Color.White;
            this.bandedGridView1.Appearance.SelectedRow.Options.UseBackColor = true;
            this.bandedGridView1.Appearance.SelectedRow.Options.UseForeColor = true;
            this.bandedGridView1.Appearance.VertLine.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(99)))), ((int)(((byte)(127)))), ((int)(((byte)(196)))));
            this.bandedGridView1.Appearance.VertLine.Options.UseBackColor = true;
            this.bandedGridView1.Bands.AddRange(new DevExpress.XtraGrid.Views.BandedGrid.GridBand[] {
            this.gridBand2,
            this.gridBand1,
            this.gridBand3,
            this.gridBandL,
            this.gridBandF});
            this.bandedGridView1.Columns.AddRange(new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn[] {
            this.colOrdem,
            this.colData,
            this.colCCT,
            this.colDescricao,
            this.colDescBalancete,
            this.colValor,
            this.colPLA,
            this.colTipo,
            this.colCTL,
            this.colCCDConsolidado,
            this.colCCDTipoLancamento,
            this.colCCD,
            this.colCCDValor,
            this.colErro,
            this.bandedGridColumn1,
            this.bandedGridColumn2,
            this.colDocumento,
            this.colCCS,
            this.colCCSSoma,
            this.colPAG1,
            this.colMestre1,
            this.colCTL21,
            this.colCHE1,
            this.colNOA1,
            this.BotNoa1,
            this.BotChe1,
            this.colPGF1,
            this.BotPGF1,
            this.BotPlus,
            this.bandedGridAgrupa,
            this.colErro2,
            this.colDesp_Cred1});
            this.bandedGridView1.GridControl = this.gridControl1;
            this.bandedGridView1.Name = "bandedGridView1";
            this.bandedGridView1.OptionsCustomization.AllowGroup = false;
            this.bandedGridView1.OptionsCustomization.AllowSort = false;
            this.bandedGridView1.OptionsView.AllowCellMerge = true;
            this.bandedGridView1.OptionsView.AutoCalcPreviewLineCount = true;
            this.bandedGridView1.OptionsView.ColumnAutoWidth = false;
            this.bandedGridView1.OptionsView.ShowAutoFilterRow = true;
            this.bandedGridView1.OptionsView.ShowGroupPanel = false;
            this.bandedGridView1.SortInfo.AddRange(new DevExpress.XtraGrid.Columns.GridColumnSortInfo[] {
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colOrdem, DevExpress.Data.ColumnSortOrder.Ascending)});
            this.bandedGridView1.CellMerge += new DevExpress.XtraGrid.Views.Grid.CellMergeEventHandler(this.bandedGridView1_CellMerge);
            this.bandedGridView1.RowCellStyle += new DevExpress.XtraGrid.Views.Grid.RowCellStyleEventHandler(this.bandedGridView1_RowCellStyle);
            this.bandedGridView1.RowStyle += new DevExpress.XtraGrid.Views.Grid.RowStyleEventHandler(this.bandedGridView1_RowStyle);
            this.bandedGridView1.CustomRowCellEdit += new DevExpress.XtraGrid.Views.Grid.CustomRowCellEditEventHandler(this.bandedGridView1_CustomRowCellEdit);
            this.bandedGridView1.ShownEditor += new System.EventHandler(this.bandedGridView1_ShownEditor);
            this.bandedGridView1.FocusedRowChanged += new DevExpress.XtraGrid.Views.Base.FocusedRowChangedEventHandler(this.bandedGridView1_FocusedRowChanged);
            this.bandedGridView1.RowUpdated += new DevExpress.XtraGrid.Views.Base.RowObjectEventHandler(this.bandedGridView1_RowUpdated);
            this.bandedGridView1.CustomColumnDisplayText += new DevExpress.XtraGrid.Views.Base.CustomColumnDisplayTextEventHandler(this.bandedGridView1_CustomColumnDisplayText);
            // 
            // gridBand2
            // 
            this.gridBand2.Caption = "Data";
            this.gridBand2.Columns.Add(this.bandedGridAgrupa);
            this.gridBand2.Columns.Add(this.colData);
            this.gridBand2.Columns.Add(this.bandedGridColumn1);
            this.gridBand2.Columns.Add(this.BotPlus);
            this.gridBand2.Columns.Add(this.BotChe1);
            this.gridBand2.Columns.Add(this.BotNoa1);
            this.gridBand2.Columns.Add(this.BotPGF1);
            this.gridBand2.Columns.Add(this.colErro);
            this.gridBand2.Fixed = DevExpress.XtraGrid.Columns.FixedStyle.Left;
            this.gridBand2.MinWidth = 20;
            this.gridBand2.Name = "gridBand2";
            this.gridBand2.VisibleIndex = 0;
            this.gridBand2.Width = 275;
            // 
            // bandedGridAgrupa
            // 
            this.bandedGridAgrupa.Caption = "bandedGridColumn3";
            this.bandedGridAgrupa.ColumnEdit = this.repositoryNULO;
            this.bandedGridAgrupa.FieldName = "Agrupar";
            this.bandedGridAgrupa.Name = "bandedGridAgrupa";
            this.bandedGridAgrupa.OptionsColumn.AllowMerge = DevExpress.Utils.DefaultBoolean.False;
            this.bandedGridAgrupa.OptionsColumn.FixedWidth = true;
            this.bandedGridAgrupa.OptionsColumn.ShowCaption = false;
            this.bandedGridAgrupa.Visible = true;
            this.bandedGridAgrupa.Width = 25;
            // 
            // repositoryNULO
            // 
            this.repositoryNULO.AutoHeight = false;
            this.repositoryNULO.Name = "repositoryNULO";
            this.repositoryNULO.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.HideTextEditor;
            // 
            // colData
            // 
            this.colData.Caption = "Data";
            this.colData.FieldName = "Data";
            this.colData.Name = "colData";
            this.colData.OptionsColumn.AllowEdit = false;
            this.colData.OptionsColumn.AllowMerge = DevExpress.Utils.DefaultBoolean.True;
            this.colData.OptionsColumn.ReadOnly = true;
            this.colData.Visible = true;
            this.colData.Width = 71;
            // 
            // bandedGridColumn1
            // 
            this.bandedGridColumn1.Caption = "Botão";
            this.bandedGridColumn1.FieldName = "BotaoEditor";
            this.bandedGridColumn1.Name = "bandedGridColumn1";
            this.bandedGridColumn1.OptionsColumn.AllowMerge = DevExpress.Utils.DefaultBoolean.False;
            this.bandedGridColumn1.OptionsColumn.FixedWidth = true;
            this.bandedGridColumn1.OptionsColumn.ShowCaption = false;
            this.bandedGridColumn1.Visible = true;
            this.bandedGridColumn1.Width = 30;
            // 
            // BotPlus
            // 
            this.BotPlus.Caption = "BotaoPlus";
            this.BotPlus.Name = "BotPlus";
            this.BotPlus.OptionsColumn.AllowMerge = DevExpress.Utils.DefaultBoolean.False;
            this.BotPlus.OptionsColumn.FixedWidth = true;
            this.BotPlus.OptionsColumn.ShowCaption = false;
            this.BotPlus.Visible = true;
            this.BotPlus.Width = 30;
            // 
            // BotChe1
            // 
            this.BotChe1.Caption = "bCHE";
            this.BotChe1.Name = "BotChe1";
            this.BotChe1.OptionsColumn.AllowMerge = DevExpress.Utils.DefaultBoolean.False;
            this.BotChe1.OptionsColumn.FixedWidth = true;
            this.BotChe1.OptionsColumn.ShowCaption = false;
            this.BotChe1.Visible = true;
            this.BotChe1.Width = 30;
            // 
            // BotNoa1
            // 
            this.BotNoa1.Caption = "bNOA";
            this.BotNoa1.Name = "BotNoa1";
            this.BotNoa1.OptionsColumn.AllowMerge = DevExpress.Utils.DefaultBoolean.False;
            this.BotNoa1.OptionsColumn.FixedWidth = true;
            this.BotNoa1.OptionsColumn.ShowCaption = false;
            this.BotNoa1.Visible = true;
            this.BotNoa1.Width = 30;
            // 
            // BotPGF1
            // 
            this.BotPGF1.Caption = "bPGF";
            this.BotPGF1.Name = "BotPGF1";
            this.BotPGF1.OptionsColumn.AllowMerge = DevExpress.Utils.DefaultBoolean.False;
            this.BotPGF1.OptionsColumn.FixedWidth = true;
            this.BotPGF1.OptionsColumn.ShowCaption = false;
            this.BotPGF1.Visible = true;
            this.BotPGF1.Width = 30;
            // 
            // colErro
            // 
            this.colErro.ColumnEdit = this.repositoryItemMemoExEdit1;
            this.colErro.FieldName = "ErroTotal";
            this.colErro.Name = "colErro";
            this.colErro.OptionsColumn.AllowMerge = DevExpress.Utils.DefaultBoolean.True;
            this.colErro.OptionsColumn.FixedWidth = true;
            this.colErro.OptionsColumn.ReadOnly = true;
            this.colErro.Visible = true;
            this.colErro.Width = 29;
            // 
            // repositoryItemMemoExEdit1
            // 
            this.repositoryItemMemoExEdit1.AppearanceDropDown.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.repositoryItemMemoExEdit1.AppearanceDropDown.BackColor2 = System.Drawing.Color.White;
            this.repositoryItemMemoExEdit1.AppearanceDropDown.Font = new System.Drawing.Font("Courier New", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.repositoryItemMemoExEdit1.AppearanceDropDown.GradientMode = System.Drawing.Drawing2D.LinearGradientMode.ForwardDiagonal;
            this.repositoryItemMemoExEdit1.AppearanceDropDown.Options.UseBackColor = true;
            this.repositoryItemMemoExEdit1.AppearanceDropDown.Options.UseFont = true;
            this.repositoryItemMemoExEdit1.AutoHeight = false;
            this.repositoryItemMemoExEdit1.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemMemoExEdit1.Images = this.imageCollection1;
            this.repositoryItemMemoExEdit1.Name = "repositoryItemMemoExEdit1";
            this.repositoryItemMemoExEdit1.PopupFormMinSize = new System.Drawing.Size(400, 0);
            // 
            // imageCollection1
            // 
            this.imageCollection1.ImageStream = ((DevExpress.Utils.ImageCollectionStreamer)(resources.GetObject("imageCollection1.ImageStream")));
            this.imageCollection1.Images.SetKeyName(0, "Terro.gif");
            this.imageCollection1.Images.SetKeyName(1, "btnConfirmar_F.Glyph.png");
            this.imageCollection1.Images.SetKeyName(2, "avisoP.gif");
            // 
            // gridBand1
            // 
            this.gridBand1.Caption = "Lançamento";
            this.gridBand1.Columns.Add(this.colOrdem);
            this.gridBand1.Columns.Add(this.colTipo);
            this.gridBand1.Columns.Add(this.colDescricao);
            this.gridBand1.Columns.Add(this.colDescBalancete);
            this.gridBand1.Columns.Add(this.colCCDValor);
            this.gridBand1.Columns.Add(this.colCCDConsolidado);
            this.gridBand1.Columns.Add(this.colCCDTipoLancamento);
            this.gridBand1.Fixed = DevExpress.XtraGrid.Columns.FixedStyle.Left;
            this.gridBand1.MinWidth = 20;
            this.gridBand1.Name = "gridBand1";
            this.gridBand1.VisibleIndex = 1;
            this.gridBand1.Width = 506;
            // 
            // colOrdem
            // 
            this.colOrdem.Caption = "Ordem";
            this.colOrdem.FieldName = "Ordem";
            this.colOrdem.Name = "colOrdem";
            this.colOrdem.OptionsColumn.ReadOnly = true;
            // 
            // colTipo
            // 
            this.colTipo.ColumnEdit = this.IMGTipos;
            this.colTipo.FieldName = "Tipo";
            this.colTipo.Name = "colTipo";
            this.colTipo.OptionsColumn.ReadOnly = true;
            this.colTipo.OptionsColumn.ShowCaption = false;
            this.colTipo.Visible = true;
            this.colTipo.Width = 26;
            // 
            // IMGTipos
            // 
            this.IMGTipos.AutoHeight = false;
            this.IMGTipos.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.IMGTipos.Items.AddRange(new DevExpress.XtraEditors.Controls.ImageComboBoxItem[] {
            new DevExpress.XtraEditors.Controls.ImageComboBoxItem("Saldos", 0, 4),
            new DevExpress.XtraEditors.Controls.ImageComboBoxItem("Não identificado", 1, 6),
            new DevExpress.XtraEditors.Controls.ImageComboBoxItem("Cred Cobrança", 2, 1),
            new DevExpress.XtraEditors.Controls.ImageComboBoxItem("Despesa", 3, 0),
            new DevExpress.XtraEditors.Controls.ImageComboBoxItem("Tansf Fisica", 4, 2),
            new DevExpress.XtraEditors.Controls.ImageComboBoxItem("Tranf Lógica", 5, 3),
            new DevExpress.XtraEditors.Controls.ImageComboBoxItem("Rentabilidade", 6, 5),
            new DevExpress.XtraEditors.Controls.ImageComboBoxItem("Deposito", 7, 7),
            new DevExpress.XtraEditors.Controls.ImageComboBoxItem("Tansf Fisica Crédito", 8, 8),
            new DevExpress.XtraEditors.Controls.ImageComboBoxItem("Tansf Fisica Débito", 9, 9),
            new DevExpress.XtraEditors.Controls.ImageComboBoxItem("Despesa no Boleto", 10, 10),
            new DevExpress.XtraEditors.Controls.ImageComboBoxItem("Estorno", 11, 11),
            new DevExpress.XtraEditors.Controls.ImageComboBoxItem("Estorno Crédito", 12, 12),
            new DevExpress.XtraEditors.Controls.ImageComboBoxItem("Estorno Débito", 13, 13),
            new DevExpress.XtraEditors.Controls.ImageComboBoxItem("Estorno", 14, 14),
            new DevExpress.XtraEditors.Controls.ImageComboBoxItem("Transf Física", 15, 15),
            new DevExpress.XtraEditors.Controls.ImageComboBoxItem("Pg Eletrônico", 16, 16),
            new DevExpress.XtraEditors.Controls.ImageComboBoxItem("Crédito Manual", 17, 17),
            new DevExpress.XtraEditors.Controls.ImageComboBoxItem("Desp Cred Man", 18, 18)});
            this.IMGTipos.Name = "IMGTipos";
            this.IMGTipos.SmallImages = this.imageCollection2;
            // 
            // imageCollection2
            // 
            this.imageCollection2.ImageStream = ((DevExpress.Utils.ImageCollectionStreamer)(resources.GetObject("imageCollection2.ImageStream")));
            this.imageCollection2.Images.SetKeyName(0, "BtnExcluir_F.Glyph.png");
            this.imageCollection2.Images.SetKeyName(1, "BtnIncluir_F.Glyph.png");
            this.imageCollection2.Images.SetKeyName(2, "TtransfF.gif");
            this.imageCollection2.Images.SetKeyName(3, "TtransfL.gif");
            this.imageCollection2.Images.SetKeyName(4, "BtnAlterar_F.Glyph.png");
            this.imageCollection2.Images.SetKeyName(5, "Renta2.gif");
            this.imageCollection2.Images.SetKeyName(6, "Tinterrog1.gif");
            this.imageCollection2.Images.SetKeyName(7, "TDeposito.gif");
            this.imageCollection2.Images.SetKeyName(8, "TtransfFC.gif");
            this.imageCollection2.Images.SetKeyName(9, "TtransfFD.gif");
            this.imageCollection2.Images.SetKeyName(10, "TDespBoleto.gif");
            this.imageCollection2.Images.SetKeyName(11, "TEstorno.gif");
            this.imageCollection2.Images.SetKeyName(12, "TEstornoC.gif");
            this.imageCollection2.Images.SetKeyName(13, "TEstornoD.gif");
            this.imageCollection2.Images.SetKeyName(14, "LinkEstorno.gif");
            this.imageCollection2.Images.SetKeyName(15, "LinktransfF.gif");
            this.imageCollection2.Images.SetKeyName(16, "PagEl.gif");
            this.imageCollection2.Images.SetKeyName(17, "creman.png");
            this.imageCollection2.Images.SetKeyName(18, "MDEB2.gif");
            // 
            // colDescricao
            // 
            this.colDescricao.Caption = "Extrato";
            this.colDescricao.FieldName = "Descricao";
            this.colDescricao.Name = "colDescricao";
            this.colDescricao.OptionsColumn.ReadOnly = true;
            this.colDescricao.Visible = true;
            this.colDescricao.Width = 244;
            // 
            // colDescBalancete
            // 
            this.colDescBalancete.Caption = "Balancete";
            this.colDescBalancete.ColumnEdit = this.TextEditCorretor70;
            this.colDescBalancete.FieldName = "DescBalanceteParcial";
            this.colDescBalancete.FilterMode = DevExpress.XtraGrid.ColumnFilterMode.DisplayText;
            this.colDescBalancete.Name = "colDescBalancete";
            this.colDescBalancete.OptionsColumn.AllowMerge = DevExpress.Utils.DefaultBoolean.False;
            this.colDescBalancete.Visible = true;
            this.colDescBalancete.Width = 166;
            // 
            // TextEditCorretor70
            // 
            this.TextEditCorretor70.AutoHeight = false;
            this.TextEditCorretor70.MaxLength = 70;
            this.TextEditCorretor70.Name = "TextEditCorretor70";
            // 
            // colCCDValor
            // 
            this.colCCDValor.Caption = "Valor Extrato";
            this.colCCDValor.ColumnEdit = this.repositoryItemCalcEdit1;
            this.colCCDValor.FieldName = "CCDValor";
            this.colCCDValor.Name = "colCCDValor";
            this.colCCDValor.OptionsColumn.AllowMerge = DevExpress.Utils.DefaultBoolean.True;
            this.colCCDValor.OptionsColumn.ReadOnly = true;
            this.colCCDValor.Visible = true;
            this.colCCDValor.Width = 70;
            // 
            // repositoryItemCalcEdit1
            // 
            this.repositoryItemCalcEdit1.AutoHeight = false;
            this.repositoryItemCalcEdit1.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemCalcEdit1.Mask.EditMask = "n2";
            this.repositoryItemCalcEdit1.Mask.UseMaskAsDisplayFormat = true;
            this.repositoryItemCalcEdit1.Name = "repositoryItemCalcEdit1";
            // 
            // colCCDConsolidado
            // 
            this.colCCDConsolidado.ColumnEdit = this.repositoryItemImageComboBox1;
            this.colCCDConsolidado.FieldName = "CCDConsolidado";
            this.colCCDConsolidado.Name = "colCCDConsolidado";
            // 
            // repositoryItemImageComboBox1
            // 
            this.repositoryItemImageComboBox1.AutoHeight = false;
            this.repositoryItemImageComboBox1.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemImageComboBox1.Name = "repositoryItemImageComboBox1";
            // 
            // colCCDTipoLancamento
            // 
            this.colCCDTipoLancamento.ColumnEdit = this.repositoryItemImageComboBox2;
            this.colCCDTipoLancamento.FieldName = "CCDTipoLancamento";
            this.colCCDTipoLancamento.Name = "colCCDTipoLancamento";
            // 
            // repositoryItemImageComboBox2
            // 
            this.repositoryItemImageComboBox2.AutoHeight = false;
            this.repositoryItemImageComboBox2.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemImageComboBox2.Name = "repositoryItemImageComboBox2";
            // 
            // gridBand3
            // 
            this.gridBand3.Caption = "Classificação";
            this.gridBand3.Columns.Add(this.colPLA);
            this.gridBand3.Columns.Add(this.colCTL);
            this.gridBand3.Columns.Add(this.colCTL21);
            this.gridBand3.Columns.Add(this.colValor);
            this.gridBand3.MinWidth = 20;
            this.gridBand3.Name = "gridBand3";
            this.gridBand3.VisibleIndex = 2;
            this.gridBand3.Width = 336;
            // 
            // colPLA
            // 
            this.colPLA.Caption = "Classificação";
            this.colPLA.ColumnEdit = this.repositoryItemLookUpPLA;
            this.colPLA.FieldName = "PLA";
            this.colPLA.Name = "colPLA";
            this.colPLA.OptionsColumn.AllowMerge = DevExpress.Utils.DefaultBoolean.False;
            this.colPLA.Visible = true;
            this.colPLA.Width = 175;
            // 
            // repositoryItemLookUpPLA
            // 
            this.repositoryItemLookUpPLA.AutoHeight = false;
            this.repositoryItemLookUpPLA.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemLookUpPLA.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("DescricaoPLA", "Descricao PLA", 77, DevExpress.Utils.FormatType.None, "", true, DevExpress.Utils.HorzAlignment.Near)});
            this.repositoryItemLookUpPLA.DataSource = this.pLAnocontasBindingSource;
            this.repositoryItemLookUpPLA.DisplayMember = "DescricaoPLA";
            this.repositoryItemLookUpPLA.Name = "repositoryItemLookUpPLA";
            this.repositoryItemLookUpPLA.NullText = " --";
            this.repositoryItemLookUpPLA.ValueMember = "PLA";
            // 
            // pLAnocontasBindingSource
            // 
            this.pLAnocontasBindingSource.DataMember = "PLAnocontas";
            this.pLAnocontasBindingSource.DataSource = typeof(Framework.datasets.dPLAnocontas);
            // 
            // colCTL
            // 
            this.colCTL.Caption = "Conta";
            this.colCTL.ColumnEdit = this.repositoryItemLookUpEditCTL;
            this.colCTL.FieldName = "CTL";
            this.colCTL.Name = "colCTL";
            this.colCTL.OptionsColumn.AllowMerge = DevExpress.Utils.DefaultBoolean.False;
            this.colCTL.Visible = true;
            this.colCTL.Width = 91;
            // 
            // repositoryItemLookUpEditCTL
            // 
            this.repositoryItemLookUpEditCTL.AutoHeight = false;
            this.repositoryItemLookUpEditCTL.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo),
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Clear)});
            this.repositoryItemLookUpEditCTL.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("CTLTitulo", "CTL Titulo", 57, DevExpress.Utils.FormatType.None, "", true, DevExpress.Utils.HorzAlignment.Near)});
            this.repositoryItemLookUpEditCTL.DataSource = this.conTasLogicasBindingSource;
            this.repositoryItemLookUpEditCTL.DisplayMember = "CTLTitulo";
            this.repositoryItemLookUpEditCTL.Name = "repositoryItemLookUpEditCTL";
            this.repositoryItemLookUpEditCTL.NullText = " --";
            this.repositoryItemLookUpEditCTL.ShowHeader = false;
            this.repositoryItemLookUpEditCTL.ValueMember = "CTL";
            this.repositoryItemLookUpEditCTL.ButtonClick += new DevExpress.XtraEditors.Controls.ButtonPressedEventHandler(this.repositoryItemLookUpEditCTL_ButtonClick);
            // 
            // conTasLogicasBindingSource
            // 
            this.conTasLogicasBindingSource.DataMember = "ConTasLogicas";
            this.conTasLogicasBindingSource.DataSource = this.dBalanceteBindingSource;
            // 
            // colCTL21
            // 
            this.colCTL21.Caption = "Conta 2";
            this.colCTL21.ColumnEdit = this.repositoryItemLookUpEditCTL;
            this.colCTL21.FieldName = "CTL2";
            this.colCTL21.Name = "colCTL21";
            // 
            // colValor
            // 
            this.colValor.Caption = "Valor";
            this.colValor.ColumnEdit = this.repositoryItemCalcEdit1;
            this.colValor.FieldName = "Valor";
            this.colValor.Name = "colValor";
            this.colValor.OptionsColumn.AllowMerge = DevExpress.Utils.DefaultBoolean.False;
            this.colValor.Visible = true;
            this.colValor.Width = 70;
            // 
            // gridBandL
            // 
            this.gridBandL.Caption = "Contas Lógicas";
            this.gridBandL.MinWidth = 20;
            this.gridBandL.Name = "gridBandL";
            this.gridBandL.VisibleIndex = 3;
            this.gridBandL.Width = 108;
            // 
            // gridBandF
            // 
            this.gridBandF.Caption = "Contas Físicas";
            this.gridBandF.MinWidth = 20;
            this.gridBandF.Name = "gridBandF";
            this.gridBandF.VisibleIndex = 4;
            this.gridBandF.Width = 126;
            // 
            // colCCT
            // 
            this.colCCT.FieldName = "CCT";
            this.colCCT.Name = "colCCT";
            this.colCCT.OptionsColumn.ReadOnly = true;
            // 
            // colCCD
            // 
            this.colCCD.FieldName = "CCD";
            this.colCCD.Name = "colCCD";
            // 
            // bandedGridColumn2
            // 
            this.bandedGridColumn2.Caption = "copiaTipo";
            this.bandedGridColumn2.FieldName = "Tipo";
            this.bandedGridColumn2.Name = "bandedGridColumn2";
            this.bandedGridColumn2.Visible = true;
            // 
            // colDocumento
            // 
            this.colDocumento.Caption = "Documento";
            this.colDocumento.FieldName = "Documento";
            this.colDocumento.Name = "colDocumento";
            this.colDocumento.Visible = true;
            // 
            // colCCS
            // 
            this.colCCS.FieldName = "CCS";
            this.colCCS.Name = "colCCS";
            this.colCCS.Visible = true;
            // 
            // colCCSSoma
            // 
            this.colCCSSoma.FieldName = "CCSSoma";
            this.colCCSSoma.Name = "colCCSSoma";
            this.colCCSSoma.Visible = true;
            // 
            // colPAG1
            // 
            this.colPAG1.FieldName = "PAG";
            this.colPAG1.Name = "colPAG1";
            this.colPAG1.Visible = true;
            // 
            // colMestre1
            // 
            this.colMestre1.FieldName = "Mestre";
            this.colMestre1.Name = "colMestre1";
            this.colMestre1.Visible = true;
            // 
            // colCHE1
            // 
            this.colCHE1.FieldName = "CHE";
            this.colCHE1.Name = "colCHE1";
            this.colCHE1.Visible = true;
            // 
            // colNOA1
            // 
            this.colNOA1.FieldName = "NOA";
            this.colNOA1.Name = "colNOA1";
            this.colNOA1.Visible = true;
            // 
            // colPGF1
            // 
            this.colPGF1.FieldName = "PGF";
            this.colPGF1.Name = "colPGF1";
            this.colPGF1.Visible = true;
            // 
            // colErro2
            // 
            this.colErro2.Caption = "Erro";
            this.colErro2.FieldName = "Erro";
            this.colErro2.Name = "colErro2";
            this.colErro2.Visible = true;
            // 
            // colDesp_Cred1
            // 
            this.colDesp_Cred1.FieldName = "Desp_Cred";
            this.colDesp_Cred1.Name = "colDesp_Cred1";
            this.colDesp_Cred1.Visible = true;
            // 
            // repositoryItemButtonEdit1
            // 
            this.repositoryItemButtonEdit1.AutoHeight = false;
            this.repositoryItemButtonEdit1.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.repositoryItemButtonEdit1.Name = "repositoryItemButtonEdit1";
            this.repositoryItemButtonEdit1.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.HideTextEditor;
            this.repositoryItemButtonEdit1.ButtonClick += new DevExpress.XtraEditors.Controls.ButtonPressedEventHandler(this.repositoryItemButtonEdit1_ButtonClick);
            // 
            // repositoryItemBotCHE
            // 
            this.repositoryItemBotCHE.AutoHeight = false;
            editorButtonImageOptions3.Image = ((System.Drawing.Image)(resources.GetObject("editorButtonImageOptions3.Image")));
            this.repositoryItemBotCHE.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(editorButtonImageOptions3, DevExpress.XtraEditors.Controls.ButtonPredefines.Glyph, null)});
            this.repositoryItemBotCHE.Name = "repositoryItemBotCHE";
            this.repositoryItemBotCHE.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.HideTextEditor;
            this.repositoryItemBotCHE.ButtonClick += new DevExpress.XtraEditors.Controls.ButtonPressedEventHandler(this.repositoryItemBotCHE_ButtonClick);
            // 
            // repositoryItemBotNoa
            // 
            this.repositoryItemBotNoa.AutoHeight = false;
            editorButtonImageOptions4.Image = ((System.Drawing.Image)(resources.GetObject("editorButtonImageOptions4.Image")));
            this.repositoryItemBotNoa.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(editorButtonImageOptions4, DevExpress.XtraEditors.Controls.ButtonPredefines.Glyph, null)});
            this.repositoryItemBotNoa.Name = "repositoryItemBotNoa";
            this.repositoryItemBotNoa.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.HideTextEditor;
            this.repositoryItemBotNoa.ButtonClick += new DevExpress.XtraEditors.Controls.ButtonPressedEventHandler(this.repositoryItemBotNoa_ButtonClick);
            // 
            // repositoryItemBotPGF
            // 
            this.repositoryItemBotPGF.AutoHeight = false;
            editorButtonImageOptions5.Image = ((System.Drawing.Image)(resources.GetObject("editorButtonImageOptions5.Image")));
            this.repositoryItemBotPGF.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(editorButtonImageOptions5, DevExpress.XtraEditors.Controls.ButtonPredefines.Glyph, null)});
            this.repositoryItemBotPGF.Name = "repositoryItemBotPGF";
            this.repositoryItemBotPGF.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.HideTextEditor;
            this.repositoryItemBotPGF.ButtonClick += new DevExpress.XtraEditors.Controls.ButtonPressedEventHandler(this.repositoryItemBotPGF_ButtonClick);
            // 
            // repositoryItemButtonPLUS
            // 
            this.repositoryItemButtonPLUS.AutoHeight = false;
            this.repositoryItemButtonPLUS.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Plus)});
            this.repositoryItemButtonPLUS.Name = "repositoryItemButtonPLUS";
            this.repositoryItemButtonPLUS.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.HideTextEditor;
            this.repositoryItemButtonPLUS.ButtonClick += new DevExpress.XtraEditors.Controls.ButtonPressedEventHandler(this.repositoryItemButtonPLUS_ButtonClick);
            // 
            // repositoryItemCheckEdit1
            // 
            this.repositoryItemCheckEdit1.AutoHeight = false;
            this.repositoryItemCheckEdit1.Caption = "Check";
            this.repositoryItemCheckEdit1.Name = "repositoryItemCheckEdit1";
            this.repositoryItemCheckEdit1.CheckedChanged += new System.EventHandler(this.repositoryItemCheckEdit1_CheckedChanged);
            // 
            // repositoryDelEst
            // 
            this.repositoryDelEst.AutoHeight = false;
            editorButtonImageOptions6.Image = ((System.Drawing.Image)(resources.GetObject("editorButtonImageOptions6.Image")));
            this.repositoryDelEst.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(editorButtonImageOptions6, DevExpress.XtraEditors.Controls.ButtonPredefines.Glyph, null)});
            this.repositoryDelEst.Name = "repositoryDelEst";
            this.repositoryDelEst.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.HideTextEditor;
            this.repositoryDelEst.ButtonClick += new DevExpress.XtraEditors.Controls.ButtonPressedEventHandler(this.repositoryDelEst_ButtonClick);
            // 
            // xtraTabControl1
            // 
            this.xtraTabControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.xtraTabControl1.Location = new System.Drawing.Point(0, 204);
            this.xtraTabControl1.Name = "xtraTabControl1";
            this.xtraTabControl1.SelectedTabPage = this.xtraTabPageDiario;
            this.xtraTabControl1.Size = new System.Drawing.Size(1452, 577);
            this.xtraTabControl1.TabIndex = 2;
            this.xtraTabControl1.TabPages.AddRange(new DevExpress.XtraTab.XtraTabPage[] {
            this.xtraTabPageDiario,
            this.xtraTabPageIndividual,
            this.xtraTabPageCreditos,
            this.xtraTabPageDebitos,
            this.xtraTabPageCTL,
            this.xtraTabPageCCT,
            this.xtraTabPagePrevisto,
            this.xtraTabPageInad,
            this.xtraTabPagePGF,
            this.xtraTabPageOBS,
            this.xtraTabPageErro});
            this.xtraTabControl1.SelectedPageChanged += new DevExpress.XtraTab.TabPageChangedEventHandler(this.xtraTabControl1_SelectedPageChanged);
            // 
            // xtraTabPageDiario
            // 
            this.xtraTabPageDiario.Controls.Add(this.gridControl1);
            this.xtraTabPageDiario.Name = "xtraTabPageDiario";
            this.xtraTabPageDiario.Size = new System.Drawing.Size(1446, 549);
            this.xtraTabPageDiario.Text = "Diário";
            // 
            // xtraTabPageIndividual
            // 
            this.xtraTabPageIndividual.Controls.Add(this.gridControl3);
            this.xtraTabPageIndividual.Name = "xtraTabPageIndividual";
            this.xtraTabPageIndividual.Size = new System.Drawing.Size(1459, 232);
            this.xtraTabPageIndividual.Text = "Individual";
            // 
            // gridControl3
            // 
            this.gridControl3.DataSource = this.tabelaVirtualBindingSource;
            this.gridControl3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridControl3.Location = new System.Drawing.Point(0, 0);
            this.gridControl3.MainView = this.gridView2;
            this.gridControl3.Name = "gridControl3";
            this.gridControl3.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.lookPLA,
            this.ItemCalcEdit,
            this.IMGTipos1,
            this.repositoryItemLookUpEditCTL1,
            this.repositoryItemMemoExEdit1A,
            this.ImageComboBox_D_C,
            this.repositoryItemBotCHE2,
            this.repositoryItemBotNOA2,
            this.repositoryItemBotPGF2,
            this.repositoryItemButtonNulo,
            this.repositoryAgrupar,
            this.repositoryDelEst1,
            this.repositoryTextEdit70});
            this.gridControl3.Size = new System.Drawing.Size(1459, 232);
            this.gridControl3.TabIndex = 0;
            this.gridControl3.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridView2});
            // 
            // tabelaVirtualBindingSource
            // 
            this.tabelaVirtualBindingSource.DataMember = "TabelaVirtual";
            this.tabelaVirtualBindingSource.DataSource = this.dBalanceteBindingSource;
            // 
            // gridView2
            // 
            this.gridView2.Appearance.ColumnFilterButton.BackColor = System.Drawing.Color.Orange;
            this.gridView2.Appearance.ColumnFilterButton.BorderColor = System.Drawing.Color.Orange;
            this.gridView2.Appearance.ColumnFilterButton.ForeColor = System.Drawing.Color.DarkOrange;
            this.gridView2.Appearance.ColumnFilterButton.Options.UseBackColor = true;
            this.gridView2.Appearance.ColumnFilterButton.Options.UseBorderColor = true;
            this.gridView2.Appearance.ColumnFilterButton.Options.UseForeColor = true;
            this.gridView2.Appearance.ColumnFilterButtonActive.BackColor = System.Drawing.Color.DarkOrange;
            this.gridView2.Appearance.ColumnFilterButtonActive.BorderColor = System.Drawing.Color.DarkOrange;
            this.gridView2.Appearance.ColumnFilterButtonActive.ForeColor = System.Drawing.Color.White;
            this.gridView2.Appearance.ColumnFilterButtonActive.Options.UseBackColor = true;
            this.gridView2.Appearance.ColumnFilterButtonActive.Options.UseBorderColor = true;
            this.gridView2.Appearance.ColumnFilterButtonActive.Options.UseForeColor = true;
            this.gridView2.Appearance.Empty.BackColor = System.Drawing.Color.LightSkyBlue;
            this.gridView2.Appearance.Empty.BackColor2 = System.Drawing.Color.SkyBlue;
            this.gridView2.Appearance.Empty.GradientMode = System.Drawing.Drawing2D.LinearGradientMode.BackwardDiagonal;
            this.gridView2.Appearance.Empty.Options.UseBackColor = true;
            this.gridView2.Appearance.EvenRow.BackColor = System.Drawing.Color.Linen;
            this.gridView2.Appearance.EvenRow.BackColor2 = System.Drawing.Color.AntiqueWhite;
            this.gridView2.Appearance.EvenRow.GradientMode = System.Drawing.Drawing2D.LinearGradientMode.ForwardDiagonal;
            this.gridView2.Appearance.EvenRow.Options.UseBackColor = true;
            this.gridView2.Appearance.FilterCloseButton.BackColor = System.Drawing.Color.Orange;
            this.gridView2.Appearance.FilterCloseButton.BorderColor = System.Drawing.Color.Orange;
            this.gridView2.Appearance.FilterCloseButton.Options.UseBackColor = true;
            this.gridView2.Appearance.FilterCloseButton.Options.UseBorderColor = true;
            this.gridView2.Appearance.FilterPanel.BackColor = System.Drawing.Color.DarkOrange;
            this.gridView2.Appearance.FilterPanel.BackColor2 = System.Drawing.Color.Orange;
            this.gridView2.Appearance.FilterPanel.ForeColor = System.Drawing.Color.Black;
            this.gridView2.Appearance.FilterPanel.GradientMode = System.Drawing.Drawing2D.LinearGradientMode.BackwardDiagonal;
            this.gridView2.Appearance.FilterPanel.Options.UseBackColor = true;
            this.gridView2.Appearance.FilterPanel.Options.UseForeColor = true;
            this.gridView2.Appearance.FocusedRow.BackColor = System.Drawing.Color.RoyalBlue;
            this.gridView2.Appearance.FocusedRow.BackColor2 = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this.gridView2.Appearance.FocusedRow.ForeColor = System.Drawing.Color.White;
            this.gridView2.Appearance.FocusedRow.GradientMode = System.Drawing.Drawing2D.LinearGradientMode.Vertical;
            this.gridView2.Appearance.FocusedRow.Options.UseBackColor = true;
            this.gridView2.Appearance.FocusedRow.Options.UseForeColor = true;
            this.gridView2.Appearance.FooterPanel.BackColor = System.Drawing.Color.Orange;
            this.gridView2.Appearance.FooterPanel.BorderColor = System.Drawing.Color.Orange;
            this.gridView2.Appearance.FooterPanel.Options.UseBackColor = true;
            this.gridView2.Appearance.FooterPanel.Options.UseBorderColor = true;
            this.gridView2.Appearance.GroupButton.BackColor = System.Drawing.Color.Wheat;
            this.gridView2.Appearance.GroupButton.BorderColor = System.Drawing.Color.Wheat;
            this.gridView2.Appearance.GroupButton.Options.UseBackColor = true;
            this.gridView2.Appearance.GroupButton.Options.UseBorderColor = true;
            this.gridView2.Appearance.GroupFooter.BackColor = System.Drawing.Color.Wheat;
            this.gridView2.Appearance.GroupFooter.BorderColor = System.Drawing.Color.Wheat;
            this.gridView2.Appearance.GroupFooter.Options.UseBackColor = true;
            this.gridView2.Appearance.GroupFooter.Options.UseBorderColor = true;
            this.gridView2.Appearance.GroupPanel.BackColor = System.Drawing.Color.RoyalBlue;
            this.gridView2.Appearance.GroupPanel.ForeColor = System.Drawing.Color.White;
            this.gridView2.Appearance.GroupPanel.Options.UseBackColor = true;
            this.gridView2.Appearance.GroupPanel.Options.UseForeColor = true;
            this.gridView2.Appearance.GroupRow.BackColor = System.Drawing.Color.Wheat;
            this.gridView2.Appearance.GroupRow.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Bold);
            this.gridView2.Appearance.GroupRow.Options.UseBackColor = true;
            this.gridView2.Appearance.GroupRow.Options.UseFont = true;
            this.gridView2.Appearance.HeaderPanel.BackColor = System.Drawing.Color.Orange;
            this.gridView2.Appearance.HeaderPanel.BorderColor = System.Drawing.Color.Orange;
            this.gridView2.Appearance.HeaderPanel.Options.UseBackColor = true;
            this.gridView2.Appearance.HeaderPanel.Options.UseBorderColor = true;
            this.gridView2.Appearance.HideSelectionRow.BackColor = System.Drawing.Color.LightSlateGray;
            this.gridView2.Appearance.HideSelectionRow.Options.UseBackColor = true;
            this.gridView2.Appearance.HorzLine.BackColor = System.Drawing.Color.Tan;
            this.gridView2.Appearance.HorzLine.Options.UseBackColor = true;
            this.gridView2.Appearance.OddRow.BackColor = System.Drawing.Color.White;
            this.gridView2.Appearance.OddRow.Options.UseBackColor = true;
            this.gridView2.Appearance.Preview.BackColor = System.Drawing.Color.Khaki;
            this.gridView2.Appearance.Preview.BackColor2 = System.Drawing.Color.Cornsilk;
            this.gridView2.Appearance.Preview.Font = new System.Drawing.Font("Tahoma", 7.5F);
            this.gridView2.Appearance.Preview.GradientMode = System.Drawing.Drawing2D.LinearGradientMode.BackwardDiagonal;
            this.gridView2.Appearance.Preview.Options.UseBackColor = true;
            this.gridView2.Appearance.Preview.Options.UseFont = true;
            this.gridView2.Appearance.Row.BackColor = System.Drawing.Color.Gainsboro;
            this.gridView2.Appearance.Row.GradientMode = System.Drawing.Drawing2D.LinearGradientMode.ForwardDiagonal;
            this.gridView2.Appearance.Row.Options.UseBackColor = true;
            this.gridView2.Appearance.RowSeparator.BackColor = System.Drawing.Color.LightSkyBlue;
            this.gridView2.Appearance.RowSeparator.Options.UseBackColor = true;
            this.gridView2.Appearance.VertLine.BackColor = System.Drawing.Color.Tan;
            this.gridView2.Appearance.VertLine.Options.UseBackColor = true;
            this.gridView2.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colOrdem1,
            this.colData1,
            this.colDescricao1,
            this.colDescBalancete1,
            this.colValor1,
            this.colPLA1,
            this.colTipo1,
            this.colCTL1,
            this.colCCT1,
            this.colCCDConsolidado1,
            this.colCCDTipoLancamento1,
            this.colCCD1,
            this.colCCDValor1,
            this.colErro1,
            this.colPAG,
            this.colCCT2,
            this.colCTL2,
            this.colMestre,
            this.colValorVenA,
            this.colValorVenD,
            this.colDocumento1,
            this.colDesp_Cred,
            this.BotCHE2,
            this.BotNOA2,
            this.BotPGF2,
            this.colAgrupar});
            this.gridView2.GridControl = this.gridControl3;
            this.gridView2.Name = "gridView2";
            this.gridView2.OptionsView.AllowCellMerge = true;
            this.gridView2.OptionsView.ColumnAutoWidth = false;
            this.gridView2.OptionsView.ShowAutoFilterRow = true;
            this.gridView2.OptionsView.ShowFooter = true;
            this.gridView2.OptionsView.ShowGroupPanel = false;
            this.gridView2.SortInfo.AddRange(new DevExpress.XtraGrid.Columns.GridColumnSortInfo[] {
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colData1, DevExpress.Data.ColumnSortOrder.Ascending)});
            this.gridView2.RowCellStyle += new DevExpress.XtraGrid.Views.Grid.RowCellStyleEventHandler(this.gridView2_RowCellStyle);
            this.gridView2.CustomRowCellEdit += new DevExpress.XtraGrid.Views.Grid.CustomRowCellEditEventHandler(this.gridView2_CustomRowCellEdit);
            this.gridView2.ShownEditor += new System.EventHandler(this.bandedGridView1_ShownEditor);
            this.gridView2.FocusedRowChanged += new DevExpress.XtraGrid.Views.Base.FocusedRowChangedEventHandler(this.gridView2_FocusedRowChanged);
            this.gridView2.RowUpdated += new DevExpress.XtraGrid.Views.Base.RowObjectEventHandler(this.gridView2_RowUpdated);
            this.gridView2.CustomColumnDisplayText += new DevExpress.XtraGrid.Views.Base.CustomColumnDisplayTextEventHandler(this.bandedGridView1_CustomColumnDisplayText);
            // 
            // colOrdem1
            // 
            this.colOrdem1.FieldName = "Ordem";
            this.colOrdem1.Name = "colOrdem1";
            // 
            // colData1
            // 
            this.colData1.Caption = "Data";
            this.colData1.FieldName = "Data";
            this.colData1.Name = "colData1";
            this.colData1.OptionsColumn.AllowMerge = DevExpress.Utils.DefaultBoolean.True;
            this.colData1.OptionsColumn.ReadOnly = true;
            this.colData1.Visible = true;
            this.colData1.VisibleIndex = 2;
            this.colData1.Width = 62;
            // 
            // colDescricao1
            // 
            this.colDescricao1.FieldName = "Descricao";
            this.colDescricao1.Name = "colDescricao1";
            this.colDescricao1.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Count, "Descricao", "{0:n0}")});
            this.colDescricao1.Visible = true;
            this.colDescricao1.VisibleIndex = 9;
            this.colDescricao1.Width = 200;
            // 
            // colDescBalancete1
            // 
            this.colDescBalancete1.ColumnEdit = this.repositoryTextEdit70;
            this.colDescBalancete1.FieldName = "DescBalanceteParcial";
            this.colDescBalancete1.FilterMode = DevExpress.XtraGrid.ColumnFilterMode.DisplayText;
            this.colDescBalancete1.Name = "colDescBalancete1";
            this.colDescBalancete1.OptionsColumn.AllowMerge = DevExpress.Utils.DefaultBoolean.False;
            this.colDescBalancete1.SortMode = DevExpress.XtraGrid.ColumnSortMode.DisplayText;
            this.colDescBalancete1.Visible = true;
            this.colDescBalancete1.VisibleIndex = 12;
            this.colDescBalancete1.Width = 281;
            // 
            // repositoryTextEdit70
            // 
            this.repositoryTextEdit70.AutoHeight = false;
            this.repositoryTextEdit70.MaxLength = 70;
            this.repositoryTextEdit70.Name = "repositoryTextEdit70";
            // 
            // colValor1
            // 
            this.colValor1.Caption = "Valor Item";
            this.colValor1.ColumnEdit = this.ItemCalcEdit;
            this.colValor1.FieldName = "Valor";
            this.colValor1.Name = "colValor1";
            this.colValor1.OptionsColumn.AllowMerge = DevExpress.Utils.DefaultBoolean.False;
            this.colValor1.OptionsColumn.ReadOnly = true;
            this.colValor1.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum, "Valor", "{0:n2}")});
            this.colValor1.Visible = true;
            this.colValor1.VisibleIndex = 13;
            this.colValor1.Width = 80;
            // 
            // ItemCalcEdit
            // 
            this.ItemCalcEdit.AutoHeight = false;
            this.ItemCalcEdit.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.ItemCalcEdit.DisplayFormat.FormatString = "n2";
            this.ItemCalcEdit.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.ItemCalcEdit.Mask.EditMask = "n2";
            this.ItemCalcEdit.Mask.UseMaskAsDisplayFormat = true;
            this.ItemCalcEdit.Name = "ItemCalcEdit";
            // 
            // colPLA1
            // 
            this.colPLA1.Caption = "Classificação";
            this.colPLA1.ColumnEdit = this.lookPLA;
            this.colPLA1.FieldName = "PLA";
            this.colPLA1.Name = "colPLA1";
            this.colPLA1.OptionsColumn.AllowMerge = DevExpress.Utils.DefaultBoolean.False;
            this.colPLA1.Visible = true;
            this.colPLA1.VisibleIndex = 11;
            this.colPLA1.Width = 102;
            // 
            // lookPLA
            // 
            this.lookPLA.AutoHeight = false;
            this.lookPLA.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.lookPLA.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("DescricaoPLA", "Descricao PLA", 77, DevExpress.Utils.FormatType.None, "", true, DevExpress.Utils.HorzAlignment.Near)});
            this.lookPLA.DataSource = this.pLAnocontasBindingSource;
            this.lookPLA.DisplayMember = "DescricaoPLA";
            this.lookPLA.Name = "lookPLA";
            this.lookPLA.NullText = "--";
            this.lookPLA.ValueMember = "PLA";
            // 
            // colTipo1
            // 
            this.colTipo1.ColumnEdit = this.IMGTipos1;
            this.colTipo1.FieldName = "Tipo";
            this.colTipo1.Name = "colTipo1";
            this.colTipo1.OptionsColumn.AllowMerge = DevExpress.Utils.DefaultBoolean.True;
            this.colTipo1.OptionsColumn.ReadOnly = true;
            this.colTipo1.Visible = true;
            this.colTipo1.VisibleIndex = 6;
            this.colTipo1.Width = 28;
            // 
            // IMGTipos1
            // 
            this.IMGTipos1.AutoHeight = false;
            this.IMGTipos1.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.IMGTipos1.GlyphAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.IMGTipos1.Items.AddRange(new DevExpress.XtraEditors.Controls.ImageComboBoxItem[] {
            new DevExpress.XtraEditors.Controls.ImageComboBoxItem("Saldos", 0, 4),
            new DevExpress.XtraEditors.Controls.ImageComboBoxItem("Não identificado", 1, 6),
            new DevExpress.XtraEditors.Controls.ImageComboBoxItem("Cred Cobrança", 2, 1),
            new DevExpress.XtraEditors.Controls.ImageComboBoxItem("Despesa", 3, 0),
            new DevExpress.XtraEditors.Controls.ImageComboBoxItem("Tansf Fisica", 4, 2),
            new DevExpress.XtraEditors.Controls.ImageComboBoxItem("Tranf Lógica", 5, 3),
            new DevExpress.XtraEditors.Controls.ImageComboBoxItem("Rentabilidade", 6, 5),
            new DevExpress.XtraEditors.Controls.ImageComboBoxItem("Deposito", 7, 7),
            new DevExpress.XtraEditors.Controls.ImageComboBoxItem("Tansf Fisica Crédito", 8, 8),
            new DevExpress.XtraEditors.Controls.ImageComboBoxItem("Tansf Fisica Débito", 9, 9),
            new DevExpress.XtraEditors.Controls.ImageComboBoxItem("Despesa no Boleto", 10, 10),
            new DevExpress.XtraEditors.Controls.ImageComboBoxItem("Estorno", 11, 11),
            new DevExpress.XtraEditors.Controls.ImageComboBoxItem("Estorno Crédito", 12, 12),
            new DevExpress.XtraEditors.Controls.ImageComboBoxItem("Estorno Débito", 13, 13),
            new DevExpress.XtraEditors.Controls.ImageComboBoxItem("Estorno Manual", 14, 14),
            new DevExpress.XtraEditors.Controls.ImageComboBoxItem("T. Física Manual", 15, 15),
            new DevExpress.XtraEditors.Controls.ImageComboBoxItem("Pag. Eletrônico", 16, 16),
            new DevExpress.XtraEditors.Controls.ImageComboBoxItem("Credito Manual", 17, 17),
            new DevExpress.XtraEditors.Controls.ImageComboBoxItem("Desp Cred Manual", 18, 18)});
            this.IMGTipos1.Name = "IMGTipos1";
            this.IMGTipos1.SmallImages = this.imageCollection2;
            // 
            // colCTL1
            // 
            this.colCTL1.Caption = "Conta";
            this.colCTL1.ColumnEdit = this.repositoryItemLookUpEditCTL1;
            this.colCTL1.FieldName = "CTL";
            this.colCTL1.Name = "colCTL1";
            this.colCTL1.OptionsColumn.AllowMerge = DevExpress.Utils.DefaultBoolean.False;
            this.colCTL1.Visible = true;
            this.colCTL1.VisibleIndex = 14;
            // 
            // repositoryItemLookUpEditCTL1
            // 
            this.repositoryItemLookUpEditCTL1.AutoHeight = false;
            this.repositoryItemLookUpEditCTL1.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo),
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Clear)});
            this.repositoryItemLookUpEditCTL1.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("CTLTitulo", "CTL Titulo", 57, DevExpress.Utils.FormatType.None, "", true, DevExpress.Utils.HorzAlignment.Near)});
            this.repositoryItemLookUpEditCTL1.DataSource = this.conTasLogicasBindingSource;
            this.repositoryItemLookUpEditCTL1.DisplayMember = "CTLTitulo";
            this.repositoryItemLookUpEditCTL1.Name = "repositoryItemLookUpEditCTL1";
            this.repositoryItemLookUpEditCTL1.NullText = " --";
            this.repositoryItemLookUpEditCTL1.ShowHeader = false;
            this.repositoryItemLookUpEditCTL1.ValueMember = "CTL";
            this.repositoryItemLookUpEditCTL1.ButtonClick += new DevExpress.XtraEditors.Controls.ButtonPressedEventHandler(this.repositoryItemLookUpEditCTL_ButtonClick);
            // 
            // colCCT1
            // 
            this.colCCT1.FieldName = "CCT";
            this.colCCT1.Name = "colCCT1";
            // 
            // colCCDConsolidado1
            // 
            this.colCCDConsolidado1.FieldName = "CCDConsolidado";
            this.colCCDConsolidado1.Name = "colCCDConsolidado1";
            // 
            // colCCDTipoLancamento1
            // 
            this.colCCDTipoLancamento1.FieldName = "CCDTipoLancamento";
            this.colCCDTipoLancamento1.Name = "colCCDTipoLancamento1";
            // 
            // colCCD1
            // 
            this.colCCD1.FieldName = "CCD";
            this.colCCD1.Name = "colCCD1";
            // 
            // colCCDValor1
            // 
            this.colCCDValor1.Caption = "Valor Total";
            this.colCCDValor1.ColumnEdit = this.ItemCalcEdit;
            this.colCCDValor1.FieldName = "CCDValor";
            this.colCCDValor1.Name = "colCCDValor1";
            this.colCCDValor1.OptionsColumn.AllowMerge = DevExpress.Utils.DefaultBoolean.True;
            this.colCCDValor1.Visible = true;
            this.colCCDValor1.VisibleIndex = 10;
            this.colCCDValor1.Width = 82;
            // 
            // colErro1
            // 
            this.colErro1.ColumnEdit = this.repositoryItemMemoExEdit1A;
            this.colErro1.FieldName = "Erro";
            this.colErro1.Name = "colErro1";
            this.colErro1.Visible = true;
            this.colErro1.VisibleIndex = 1;
            this.colErro1.Width = 32;
            // 
            // repositoryItemMemoExEdit1A
            // 
            this.repositoryItemMemoExEdit1A.AppearanceDropDown.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.repositoryItemMemoExEdit1A.AppearanceDropDown.BackColor2 = System.Drawing.Color.White;
            this.repositoryItemMemoExEdit1A.AppearanceDropDown.Font = new System.Drawing.Font("Courier New", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.repositoryItemMemoExEdit1A.AppearanceDropDown.GradientMode = System.Drawing.Drawing2D.LinearGradientMode.ForwardDiagonal;
            this.repositoryItemMemoExEdit1A.AppearanceDropDown.Options.UseBackColor = true;
            this.repositoryItemMemoExEdit1A.AppearanceDropDown.Options.UseFont = true;
            this.repositoryItemMemoExEdit1A.AutoHeight = false;
            this.repositoryItemMemoExEdit1A.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemMemoExEdit1A.Images = this.imageCollection1;
            this.repositoryItemMemoExEdit1A.Name = "repositoryItemMemoExEdit1A";
            this.repositoryItemMemoExEdit1A.PopupFormMinSize = new System.Drawing.Size(400, 0);
            // 
            // colPAG
            // 
            this.colPAG.FieldName = "PAG";
            this.colPAG.Name = "colPAG";
            // 
            // colCCT2
            // 
            this.colCCT2.FieldName = "CCT2";
            this.colCCT2.Name = "colCCT2";
            // 
            // colCTL2
            // 
            this.colCTL2.Caption = "Conta 2";
            this.colCTL2.ColumnEdit = this.repositoryItemLookUpEditCTL1;
            this.colCTL2.FieldName = "CTL2";
            this.colCTL2.Name = "colCTL2";
            this.colCTL2.Visible = true;
            this.colCTL2.VisibleIndex = 15;
            // 
            // colMestre
            // 
            this.colMestre.FieldName = "Mestre";
            this.colMestre.Name = "colMestre";
            // 
            // colValorVenA
            // 
            this.colValorVenA.FieldName = "ValorVenA";
            this.colValorVenA.Name = "colValorVenA";
            // 
            // colValorVenD
            // 
            this.colValorVenD.FieldName = "ValorVenD";
            this.colValorVenD.Name = "colValorVenD";
            // 
            // colDocumento1
            // 
            this.colDocumento1.Caption = "Doc";
            this.colDocumento1.FieldName = "Documento";
            this.colDocumento1.Name = "colDocumento1";
            this.colDocumento1.OptionsColumn.AllowMerge = DevExpress.Utils.DefaultBoolean.True;
            this.colDocumento1.Visible = true;
            this.colDocumento1.VisibleIndex = 8;
            this.colDocumento1.Width = 53;
            // 
            // colDesp_Cred
            // 
            this.colDesp_Cred.Caption = "D/C";
            this.colDesp_Cred.ColumnEdit = this.ImageComboBox_D_C;
            this.colDesp_Cred.FieldName = "Desp_Cred";
            this.colDesp_Cred.Name = "colDesp_Cred";
            this.colDesp_Cred.OptionsColumn.ReadOnly = true;
            this.colDesp_Cred.Visible = true;
            this.colDesp_Cred.VisibleIndex = 7;
            this.colDesp_Cred.Width = 25;
            // 
            // ImageComboBox_D_C
            // 
            this.ImageComboBox_D_C.AutoHeight = false;
            this.ImageComboBox_D_C.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.ImageComboBox_D_C.GlyphAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.ImageComboBox_D_C.Items.AddRange(new DevExpress.XtraEditors.Controls.ImageComboBoxItem[] {
            new DevExpress.XtraEditors.Controls.ImageComboBoxItem("Débito", "D", 0),
            new DevExpress.XtraEditors.Controls.ImageComboBoxItem("Crédito", "C", 1),
            new DevExpress.XtraEditors.Controls.ImageComboBoxItem("Rentabilidade", "R", 2),
            new DevExpress.XtraEditors.Controls.ImageComboBoxItem(" -", "N", -1)});
            this.ImageComboBox_D_C.Name = "ImageComboBox_D_C";
            this.ImageComboBox_D_C.SmallImages = this.imageCollectionDC;
            // 
            // imageCollectionDC
            // 
            this.imageCollectionDC.ImageStream = ((DevExpress.Utils.ImageCollectionStreamer)(resources.GetObject("imageCollectionDC.ImageStream")));
            this.imageCollectionDC.Images.SetKeyName(0, "DT.gif");
            this.imageCollectionDC.Images.SetKeyName(1, "CT.gif");
            this.imageCollectionDC.Images.SetKeyName(2, "TRenta2.gif");
            // 
            // BotCHE2
            // 
            this.BotCHE2.Caption = "bCHE";
            this.BotCHE2.Name = "BotCHE2";
            this.BotCHE2.OptionsColumn.AllowMerge = DevExpress.Utils.DefaultBoolean.False;
            this.BotCHE2.OptionsColumn.FixedWidth = true;
            this.BotCHE2.OptionsColumn.ShowCaption = false;
            this.BotCHE2.Visible = true;
            this.BotCHE2.VisibleIndex = 3;
            this.BotCHE2.Width = 30;
            // 
            // BotNOA2
            // 
            this.BotNOA2.Caption = "bNOA";
            this.BotNOA2.Name = "BotNOA2";
            this.BotNOA2.OptionsColumn.AllowMerge = DevExpress.Utils.DefaultBoolean.False;
            this.BotNOA2.OptionsColumn.FixedWidth = true;
            this.BotNOA2.OptionsColumn.ShowCaption = false;
            this.BotNOA2.Visible = true;
            this.BotNOA2.VisibleIndex = 4;
            this.BotNOA2.Width = 30;
            // 
            // BotPGF2
            // 
            this.BotPGF2.Caption = "bPGF";
            this.BotPGF2.Name = "BotPGF2";
            this.BotPGF2.OptionsColumn.AllowMerge = DevExpress.Utils.DefaultBoolean.False;
            this.BotPGF2.OptionsColumn.FixedWidth = true;
            this.BotPGF2.OptionsColumn.ShowCaption = false;
            this.BotPGF2.Visible = true;
            this.BotPGF2.VisibleIndex = 5;
            this.BotPGF2.Width = 30;
            // 
            // colAgrupar
            // 
            this.colAgrupar.ColumnEdit = this.repositoryItemButtonNulo;
            this.colAgrupar.FieldName = "Agrupar";
            this.colAgrupar.Fixed = DevExpress.XtraGrid.Columns.FixedStyle.Left;
            this.colAgrupar.Name = "colAgrupar";
            this.colAgrupar.OptionsColumn.AllowMerge = DevExpress.Utils.DefaultBoolean.False;
            this.colAgrupar.OptionsColumn.ShowCaption = false;
            this.colAgrupar.Visible = true;
            this.colAgrupar.VisibleIndex = 0;
            this.colAgrupar.Width = 25;
            // 
            // repositoryItemButtonNulo
            // 
            this.repositoryItemButtonNulo.AutoHeight = false;
            this.repositoryItemButtonNulo.Name = "repositoryItemButtonNulo";
            this.repositoryItemButtonNulo.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.HideTextEditor;
            // 
            // repositoryItemBotCHE2
            // 
            this.repositoryItemBotCHE2.AutoHeight = false;
            editorButtonImageOptions7.Image = ((System.Drawing.Image)(resources.GetObject("editorButtonImageOptions7.Image")));
            this.repositoryItemBotCHE2.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(editorButtonImageOptions7, DevExpress.XtraEditors.Controls.ButtonPredefines.Glyph, null)});
            this.repositoryItemBotCHE2.Name = "repositoryItemBotCHE2";
            this.repositoryItemBotCHE2.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.HideTextEditor;
            this.repositoryItemBotCHE2.ButtonClick += new DevExpress.XtraEditors.Controls.ButtonPressedEventHandler(this.repositoryItemBotCHE2_ButtonClick);
            // 
            // repositoryItemBotNOA2
            // 
            this.repositoryItemBotNOA2.AutoHeight = false;
            editorButtonImageOptions8.Image = ((System.Drawing.Image)(resources.GetObject("editorButtonImageOptions8.Image")));
            this.repositoryItemBotNOA2.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(editorButtonImageOptions8, DevExpress.XtraEditors.Controls.ButtonPredefines.Glyph, null)});
            this.repositoryItemBotNOA2.Name = "repositoryItemBotNOA2";
            this.repositoryItemBotNOA2.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.HideTextEditor;
            this.repositoryItemBotNOA2.ButtonClick += new DevExpress.XtraEditors.Controls.ButtonPressedEventHandler(this.repositoryItemBotNOA2_ButtonClick);
            // 
            // repositoryItemBotPGF2
            // 
            this.repositoryItemBotPGF2.AutoHeight = false;
            editorButtonImageOptions9.Image = ((System.Drawing.Image)(resources.GetObject("editorButtonImageOptions9.Image")));
            this.repositoryItemBotPGF2.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(editorButtonImageOptions9, DevExpress.XtraEditors.Controls.ButtonPredefines.Glyph, null)});
            this.repositoryItemBotPGF2.Name = "repositoryItemBotPGF2";
            this.repositoryItemBotPGF2.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.HideTextEditor;
            this.repositoryItemBotPGF2.ButtonClick += new DevExpress.XtraEditors.Controls.ButtonPressedEventHandler(this.repositoryItemBotPGF2_ButtonClick);
            // 
            // repositoryAgrupar
            // 
            this.repositoryAgrupar.AutoHeight = false;
            this.repositoryAgrupar.Caption = "Check";
            this.repositoryAgrupar.Name = "repositoryAgrupar";
            this.repositoryAgrupar.CheckedChanged += new System.EventHandler(this.repositoryAgrupar_CheckedChanged);
            // 
            // repositoryDelEst1
            // 
            this.repositoryDelEst1.AutoHeight = false;
            editorButtonImageOptions10.Image = ((System.Drawing.Image)(resources.GetObject("editorButtonImageOptions10.Image")));
            this.repositoryDelEst1.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(editorButtonImageOptions10, DevExpress.XtraEditors.Controls.ButtonPredefines.Glyph, null)});
            this.repositoryDelEst1.Name = "repositoryDelEst1";
            this.repositoryDelEst1.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.HideTextEditor;
            this.repositoryDelEst1.ButtonClick += new DevExpress.XtraEditors.Controls.ButtonPressedEventHandler(this.repositoryDelEst1_ButtonClick);
            // 
            // xtraTabPageCreditos
            // 
            this.xtraTabPageCreditos.Controls.Add(this.xtraTabControlCredito);
            this.xtraTabPageCreditos.Name = "xtraTabPageCreditos";
            this.xtraTabPageCreditos.Size = new System.Drawing.Size(1459, 232);
            this.xtraTabPageCreditos.Text = "Créditos";
            // 
            // xtraTabControlCredito
            // 
            this.xtraTabControlCredito.Dock = System.Windows.Forms.DockStyle.Fill;
            this.xtraTabControlCredito.Location = new System.Drawing.Point(0, 0);
            this.xtraTabControlCredito.Name = "xtraTabControlCredito";
            this.xtraTabControlCredito.SelectedTabPage = this.xtraTabCredResumo;
            this.xtraTabControlCredito.Size = new System.Drawing.Size(1459, 232);
            this.xtraTabControlCredito.TabIndex = 1;
            this.xtraTabControlCredito.TabPages.AddRange(new DevExpress.XtraTab.XtraTabPage[] {
            this.xtraTabCredResumo,
            this.xtraTabCredDetalhes,
            this.xtraTabCredGrafico});
            this.xtraTabControlCredito.SelectedPageChanged += new DevExpress.XtraTab.TabPageChangedEventHandler(this.xtraTabControl2_SelectedPageChanged);
            // 
            // xtraTabCredResumo
            // 
            this.xtraTabCredResumo.Controls.Add(this.gridControl4);
            this.xtraTabCredResumo.Name = "xtraTabCredResumo";
            this.xtraTabCredResumo.Size = new System.Drawing.Size(1453, 204);
            this.xtraTabCredResumo.Text = "Resumo / Quadrinhos";
            // 
            // xtraTabCredDetalhes
            // 
            this.xtraTabCredDetalhes.Controls.Add(this.gridControl2);
            this.xtraTabCredDetalhes.Name = "xtraTabCredDetalhes";
            this.xtraTabCredDetalhes.Size = new System.Drawing.Size(1453, 204);
            this.xtraTabCredDetalhes.Text = "Detalhes";
            // 
            // gridControl2
            // 
            this.gridControl2.DataMember = "BOLetosRecebidos";
            this.gridControl2.DataSource = this.dBalanceteBindingSource;
            this.gridControl2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridControl2.Location = new System.Drawing.Point(0, 0);
            this.gridControl2.MainView = this.gridView1;
            this.gridControl2.Name = "gridControl2";
            this.gridControl2.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repBotaoBoleto,
            this.repValor,
            this.repGrupos,
            this.repPI,
            this.repTipoGrafico,
            this.repositoryItemMemoExEdit3});
            this.gridControl2.Size = new System.Drawing.Size(1453, 204);
            this.gridControl2.TabIndex = 1;
            this.gridControl2.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridView1});
            // 
            // gridView1
            // 
            this.gridView1.Appearance.ColumnFilterButton.BackColor = System.Drawing.Color.Orange;
            this.gridView1.Appearance.ColumnFilterButton.BorderColor = System.Drawing.Color.Orange;
            this.gridView1.Appearance.ColumnFilterButton.ForeColor = System.Drawing.Color.DarkOrange;
            this.gridView1.Appearance.ColumnFilterButton.Options.UseBackColor = true;
            this.gridView1.Appearance.ColumnFilterButton.Options.UseBorderColor = true;
            this.gridView1.Appearance.ColumnFilterButton.Options.UseForeColor = true;
            this.gridView1.Appearance.ColumnFilterButtonActive.BackColor = System.Drawing.Color.DarkOrange;
            this.gridView1.Appearance.ColumnFilterButtonActive.BorderColor = System.Drawing.Color.DarkOrange;
            this.gridView1.Appearance.ColumnFilterButtonActive.ForeColor = System.Drawing.Color.White;
            this.gridView1.Appearance.ColumnFilterButtonActive.Options.UseBackColor = true;
            this.gridView1.Appearance.ColumnFilterButtonActive.Options.UseBorderColor = true;
            this.gridView1.Appearance.ColumnFilterButtonActive.Options.UseForeColor = true;
            this.gridView1.Appearance.Empty.BackColor = System.Drawing.Color.LightSkyBlue;
            this.gridView1.Appearance.Empty.BackColor2 = System.Drawing.Color.SkyBlue;
            this.gridView1.Appearance.Empty.GradientMode = System.Drawing.Drawing2D.LinearGradientMode.BackwardDiagonal;
            this.gridView1.Appearance.Empty.Options.UseBackColor = true;
            this.gridView1.Appearance.EvenRow.BackColor = System.Drawing.Color.Linen;
            this.gridView1.Appearance.EvenRow.BackColor2 = System.Drawing.Color.AntiqueWhite;
            this.gridView1.Appearance.EvenRow.GradientMode = System.Drawing.Drawing2D.LinearGradientMode.ForwardDiagonal;
            this.gridView1.Appearance.EvenRow.Options.UseBackColor = true;
            this.gridView1.Appearance.FilterCloseButton.BackColor = System.Drawing.Color.Orange;
            this.gridView1.Appearance.FilterCloseButton.BorderColor = System.Drawing.Color.Orange;
            this.gridView1.Appearance.FilterCloseButton.Options.UseBackColor = true;
            this.gridView1.Appearance.FilterCloseButton.Options.UseBorderColor = true;
            this.gridView1.Appearance.FilterPanel.BackColor = System.Drawing.Color.DarkOrange;
            this.gridView1.Appearance.FilterPanel.BackColor2 = System.Drawing.Color.Orange;
            this.gridView1.Appearance.FilterPanel.ForeColor = System.Drawing.Color.Black;
            this.gridView1.Appearance.FilterPanel.GradientMode = System.Drawing.Drawing2D.LinearGradientMode.BackwardDiagonal;
            this.gridView1.Appearance.FilterPanel.Options.UseBackColor = true;
            this.gridView1.Appearance.FilterPanel.Options.UseForeColor = true;
            this.gridView1.Appearance.FocusedRow.BackColor = System.Drawing.Color.RoyalBlue;
            this.gridView1.Appearance.FocusedRow.BackColor2 = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this.gridView1.Appearance.FocusedRow.ForeColor = System.Drawing.Color.White;
            this.gridView1.Appearance.FocusedRow.GradientMode = System.Drawing.Drawing2D.LinearGradientMode.Vertical;
            this.gridView1.Appearance.FocusedRow.Options.UseBackColor = true;
            this.gridView1.Appearance.FocusedRow.Options.UseForeColor = true;
            this.gridView1.Appearance.FooterPanel.BackColor = System.Drawing.Color.Orange;
            this.gridView1.Appearance.FooterPanel.BorderColor = System.Drawing.Color.Orange;
            this.gridView1.Appearance.FooterPanel.Options.UseBackColor = true;
            this.gridView1.Appearance.FooterPanel.Options.UseBorderColor = true;
            this.gridView1.Appearance.GroupButton.BackColor = System.Drawing.Color.Wheat;
            this.gridView1.Appearance.GroupButton.BorderColor = System.Drawing.Color.Wheat;
            this.gridView1.Appearance.GroupButton.Options.UseBackColor = true;
            this.gridView1.Appearance.GroupButton.Options.UseBorderColor = true;
            this.gridView1.Appearance.GroupFooter.BackColor = System.Drawing.Color.Wheat;
            this.gridView1.Appearance.GroupFooter.BorderColor = System.Drawing.Color.Wheat;
            this.gridView1.Appearance.GroupFooter.Options.UseBackColor = true;
            this.gridView1.Appearance.GroupFooter.Options.UseBorderColor = true;
            this.gridView1.Appearance.GroupPanel.BackColor = System.Drawing.Color.RoyalBlue;
            this.gridView1.Appearance.GroupPanel.ForeColor = System.Drawing.Color.White;
            this.gridView1.Appearance.GroupPanel.Options.UseBackColor = true;
            this.gridView1.Appearance.GroupPanel.Options.UseForeColor = true;
            this.gridView1.Appearance.GroupRow.BackColor = System.Drawing.Color.Wheat;
            this.gridView1.Appearance.GroupRow.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Bold);
            this.gridView1.Appearance.GroupRow.Options.UseBackColor = true;
            this.gridView1.Appearance.GroupRow.Options.UseFont = true;
            this.gridView1.Appearance.HeaderPanel.BackColor = System.Drawing.Color.Orange;
            this.gridView1.Appearance.HeaderPanel.BorderColor = System.Drawing.Color.Orange;
            this.gridView1.Appearance.HeaderPanel.Options.UseBackColor = true;
            this.gridView1.Appearance.HeaderPanel.Options.UseBorderColor = true;
            this.gridView1.Appearance.HideSelectionRow.BackColor = System.Drawing.Color.LightSlateGray;
            this.gridView1.Appearance.HideSelectionRow.Options.UseBackColor = true;
            this.gridView1.Appearance.HorzLine.BackColor = System.Drawing.Color.Tan;
            this.gridView1.Appearance.HorzLine.Options.UseBackColor = true;
            this.gridView1.Appearance.OddRow.BackColor = System.Drawing.Color.White;
            this.gridView1.Appearance.OddRow.Options.UseBackColor = true;
            this.gridView1.Appearance.Preview.BackColor = System.Drawing.Color.Khaki;
            this.gridView1.Appearance.Preview.BackColor2 = System.Drawing.Color.Cornsilk;
            this.gridView1.Appearance.Preview.Font = new System.Drawing.Font("Tahoma", 7.5F);
            this.gridView1.Appearance.Preview.GradientMode = System.Drawing.Drawing2D.LinearGradientMode.BackwardDiagonal;
            this.gridView1.Appearance.Preview.Options.UseBackColor = true;
            this.gridView1.Appearance.Preview.Options.UseFont = true;
            this.gridView1.Appearance.Row.BackColor = System.Drawing.Color.White;
            this.gridView1.Appearance.Row.Options.UseBackColor = true;
            this.gridView1.Appearance.RowSeparator.BackColor = System.Drawing.Color.LightSkyBlue;
            this.gridView1.Appearance.RowSeparator.Options.UseBackColor = true;
            this.gridView1.Appearance.VertLine.BackColor = System.Drawing.Color.Tan;
            this.gridView1.Appearance.VertLine.Options.UseBackColor = true;
            this.gridView1.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colBOL,
            this.colBOLValorPago,
            this.colBOL_CCD,
            this.colAPTNumero,
            this.colBLOCodigo,
            this.colBOLCompetenciaAno,
            this.colBOLCompetenciaMes,
            this.colBOLEmissao,
            this.colBOLPagamento,
            this.colBOLProprietario,
            this.colBOLTipoCRAI,
            this.colBOLVencto,
            this.colBotaoBoleto,
            this.colcalcGrupo,
            this.colBOL2,
            this.colcalcUnidade,
            this.colClaGrafico,
            this.colValCredLiq,
            this.colCCT_CON,
            this.colErro3,
            this.colCCT3});
            this.gridView1.GridControl = this.gridControl2;
            this.gridView1.GroupCount = 1;
            this.gridView1.GroupFormat = "[#image]{1} {2}";
            this.gridView1.GroupSummary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridGroupSummaryItem(DevExpress.Data.SummaryItemType.Sum, "BOLValorPago", this.colBOLValorPago, "{0:n2}"),
            new DevExpress.XtraGrid.GridGroupSummaryItem(DevExpress.Data.SummaryItemType.Sum, "ValCredLiq", this.colValCredLiq, "{0:n2}")});
            this.gridView1.Name = "gridView1";
            this.gridView1.OptionsBehavior.AutoExpandAllGroups = true;
            this.gridView1.OptionsView.ColumnAutoWidth = false;
            this.gridView1.OptionsView.EnableAppearanceEvenRow = true;
            this.gridView1.OptionsView.EnableAppearanceOddRow = true;
            this.gridView1.OptionsView.ShowAutoFilterRow = true;
            this.gridView1.OptionsView.ShowFooter = true;
            this.gridView1.OptionsView.ShowGroupPanel = false;
            this.gridView1.SortInfo.AddRange(new DevExpress.XtraGrid.Columns.GridColumnSortInfo[] {
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colcalcGrupo, DevExpress.Data.ColumnSortOrder.Ascending),
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colcalcUnidade, DevExpress.Data.ColumnSortOrder.Ascending),
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colBOL, DevExpress.Data.ColumnSortOrder.Ascending)});
            this.gridView1.RowCellStyle += new DevExpress.XtraGrid.Views.Grid.RowCellStyleEventHandler(this.gridView1_RowCellStyle);
            this.gridView1.CustomRowCellEdit += new DevExpress.XtraGrid.Views.Grid.CustomRowCellEditEventHandler(this.gridView1_CustomRowCellEdit);
            // 
            // colBOL
            // 
            this.colBOL.Caption = "Boleto";
            this.colBOL.FieldName = "calcBOL";
            this.colBOL.Fixed = DevExpress.XtraGrid.Columns.FixedStyle.Left;
            this.colBOL.Name = "colBOL";
            this.colBOL.OptionsColumn.ReadOnly = true;
            this.colBOL.Visible = true;
            this.colBOL.VisibleIndex = 0;
            this.colBOL.Width = 127;
            // 
            // colBOLValorPago
            // 
            this.colBOLValorPago.Caption = "Total Boleto";
            this.colBOLValorPago.ColumnEdit = this.repValor;
            this.colBOLValorPago.DisplayFormat.FormatString = "n2";
            this.colBOLValorPago.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.colBOLValorPago.FieldName = "BOLValorPago";
            this.colBOLValorPago.Fixed = DevExpress.XtraGrid.Columns.FixedStyle.Right;
            this.colBOLValorPago.Name = "colBOLValorPago";
            this.colBOLValorPago.OptionsColumn.ReadOnly = true;
            this.colBOLValorPago.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum, "BOLValorPago", "{0:n2}")});
            this.colBOLValorPago.Visible = true;
            this.colBOLValorPago.VisibleIndex = 11;
            this.colBOLValorPago.Width = 87;
            // 
            // repValor
            // 
            this.repValor.AutoHeight = false;
            this.repValor.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repValor.DisplayFormat.FormatString = "n2";
            this.repValor.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.repValor.Name = "repValor";
            // 
            // colBOL_CCD
            // 
            this.colBOL_CCD.FieldName = "BOL_CCD";
            this.colBOL_CCD.Name = "colBOL_CCD";
            this.colBOL_CCD.OptionsColumn.ReadOnly = true;
            // 
            // colAPTNumero
            // 
            this.colAPTNumero.Caption = "Apt";
            this.colAPTNumero.FieldName = "APTNumero";
            this.colAPTNumero.Fixed = DevExpress.XtraGrid.Columns.FixedStyle.Left;
            this.colAPTNumero.Name = "colAPTNumero";
            this.colAPTNumero.OptionsColumn.ReadOnly = true;
            this.colAPTNumero.Width = 55;
            // 
            // colBLOCodigo
            // 
            this.colBLOCodigo.Caption = "Bloco";
            this.colBLOCodigo.FieldName = "BLOCodigo";
            this.colBLOCodigo.Fixed = DevExpress.XtraGrid.Columns.FixedStyle.Left;
            this.colBLOCodigo.Name = "colBLOCodigo";
            this.colBLOCodigo.OptionsColumn.ReadOnly = true;
            this.colBLOCodigo.Width = 50;
            // 
            // colBOLCompetenciaAno
            // 
            this.colBOLCompetenciaAno.Caption = "Ano";
            this.colBOLCompetenciaAno.FieldName = "BOLCompetenciaAno";
            this.colBOLCompetenciaAno.Fixed = DevExpress.XtraGrid.Columns.FixedStyle.Left;
            this.colBOLCompetenciaAno.Name = "colBOLCompetenciaAno";
            this.colBOLCompetenciaAno.OptionsColumn.ReadOnly = true;
            this.colBOLCompetenciaAno.Visible = true;
            this.colBOLCompetenciaAno.VisibleIndex = 3;
            this.colBOLCompetenciaAno.Width = 30;
            // 
            // colBOLCompetenciaMes
            // 
            this.colBOLCompetenciaMes.Caption = "Mês";
            this.colBOLCompetenciaMes.FieldName = "BOLCompetenciaMes";
            this.colBOLCompetenciaMes.Fixed = DevExpress.XtraGrid.Columns.FixedStyle.Left;
            this.colBOLCompetenciaMes.Name = "colBOLCompetenciaMes";
            this.colBOLCompetenciaMes.OptionsColumn.ReadOnly = true;
            this.colBOLCompetenciaMes.Visible = true;
            this.colBOLCompetenciaMes.VisibleIndex = 4;
            this.colBOLCompetenciaMes.Width = 26;
            // 
            // colBOLEmissao
            // 
            this.colBOLEmissao.Caption = "Emissão";
            this.colBOLEmissao.FieldName = "BOLEmissao";
            this.colBOLEmissao.Fixed = DevExpress.XtraGrid.Columns.FixedStyle.Left;
            this.colBOLEmissao.Name = "colBOLEmissao";
            this.colBOLEmissao.OptionsColumn.ReadOnly = true;
            this.colBOLEmissao.Visible = true;
            this.colBOLEmissao.VisibleIndex = 5;
            this.colBOLEmissao.Width = 66;
            // 
            // colBOLPagamento
            // 
            this.colBOLPagamento.Caption = "Pagamento";
            this.colBOLPagamento.FieldName = "BOLPagamento";
            this.colBOLPagamento.Fixed = DevExpress.XtraGrid.Columns.FixedStyle.Left;
            this.colBOLPagamento.Name = "colBOLPagamento";
            this.colBOLPagamento.OptionsColumn.ReadOnly = true;
            this.colBOLPagamento.Visible = true;
            this.colBOLPagamento.VisibleIndex = 6;
            this.colBOLPagamento.Width = 66;
            // 
            // colBOLProprietario
            // 
            this.colBOLProprietario.AppearanceCell.Options.UseTextOptions = true;
            this.colBOLProprietario.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colBOLProprietario.Caption = "P/I";
            this.colBOLProprietario.ColumnEdit = this.repPI;
            this.colBOLProprietario.FieldName = "BOLProprietario";
            this.colBOLProprietario.Fixed = DevExpress.XtraGrid.Columns.FixedStyle.Left;
            this.colBOLProprietario.Name = "colBOLProprietario";
            this.colBOLProprietario.OptionsColumn.ReadOnly = true;
            this.colBOLProprietario.Visible = true;
            this.colBOLProprietario.VisibleIndex = 7;
            this.colBOLProprietario.Width = 33;
            // 
            // repPI
            // 
            this.repPI.AutoHeight = false;
            this.repPI.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repPI.Items.AddRange(new DevExpress.XtraEditors.Controls.ImageComboBoxItem[] {
            new DevExpress.XtraEditors.Controls.ImageComboBoxItem("P", true, -1),
            new DevExpress.XtraEditors.Controls.ImageComboBoxItem("I", false, -1)});
            this.repPI.Name = "repPI";
            // 
            // colBOLTipoCRAI
            // 
            this.colBOLTipoCRAI.AppearanceCell.Options.UseTextOptions = true;
            this.colBOLTipoCRAI.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colBOLTipoCRAI.Caption = "Tipo";
            this.colBOLTipoCRAI.FieldName = "BOLTipoCRAI";
            this.colBOLTipoCRAI.Fixed = DevExpress.XtraGrid.Columns.FixedStyle.Left;
            this.colBOLTipoCRAI.Name = "colBOLTipoCRAI";
            this.colBOLTipoCRAI.OptionsColumn.ReadOnly = true;
            this.colBOLTipoCRAI.Visible = true;
            this.colBOLTipoCRAI.VisibleIndex = 8;
            this.colBOLTipoCRAI.Width = 33;
            // 
            // colBOLVencto
            // 
            this.colBOLVencto.Caption = "Vencimento";
            this.colBOLVencto.FieldName = "BOLVencto";
            this.colBOLVencto.Fixed = DevExpress.XtraGrid.Columns.FixedStyle.Left;
            this.colBOLVencto.Name = "colBOLVencto";
            this.colBOLVencto.OptionsColumn.ReadOnly = true;
            this.colBOLVencto.Visible = true;
            this.colBOLVencto.VisibleIndex = 9;
            this.colBOLVencto.Width = 66;
            // 
            // colBotaoBoleto
            // 
            this.colBotaoBoleto.Caption = "gridColumn1";
            this.colBotaoBoleto.Fixed = DevExpress.XtraGrid.Columns.FixedStyle.Right;
            this.colBotaoBoleto.Name = "colBotaoBoleto";
            this.colBotaoBoleto.OptionsColumn.ShowCaption = false;
            this.colBotaoBoleto.Visible = true;
            this.colBotaoBoleto.VisibleIndex = 13;
            this.colBotaoBoleto.Width = 28;
            // 
            // colcalcGrupo
            // 
            this.colcalcGrupo.ColumnEdit = this.repGrupos;
            this.colcalcGrupo.FieldName = "calcGrupo";
            this.colcalcGrupo.Name = "colcalcGrupo";
            this.colcalcGrupo.SortMode = DevExpress.XtraGrid.ColumnSortMode.Value;
            this.colcalcGrupo.Visible = true;
            this.colcalcGrupo.VisibleIndex = 10;
            // 
            // repGrupos
            // 
            this.repGrupos.AutoHeight = false;
            this.repGrupos.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repGrupos.Name = "repGrupos";
            // 
            // colBOL2
            // 
            this.colBOL2.Caption = "BOL";
            this.colBOL2.FieldName = "BOL";
            this.colBOL2.Name = "colBOL2";
            // 
            // colcalcUnidade
            // 
            this.colcalcUnidade.Caption = "Apto";
            this.colcalcUnidade.FieldName = "calcUnidade";
            this.colcalcUnidade.Fixed = DevExpress.XtraGrid.Columns.FixedStyle.Left;
            this.colcalcUnidade.Name = "colcalcUnidade";
            this.colcalcUnidade.Visible = true;
            this.colcalcUnidade.VisibleIndex = 2;
            this.colcalcUnidade.Width = 83;
            // 
            // colClaGrafico
            // 
            this.colClaGrafico.Caption = "Gráfico";
            this.colClaGrafico.ColumnEdit = this.repTipoGrafico;
            this.colClaGrafico.FieldName = "ClaGrafico";
            this.colClaGrafico.Name = "colClaGrafico";
            this.colClaGrafico.OptionsColumn.ReadOnly = true;
            this.colClaGrafico.Visible = true;
            this.colClaGrafico.VisibleIndex = 10;
            // 
            // repTipoGrafico
            // 
            this.repTipoGrafico.AutoHeight = false;
            this.repTipoGrafico.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repTipoGrafico.Name = "repTipoGrafico";
            // 
            // colValCredLiq
            // 
            this.colValCredLiq.Caption = "Total Créditos";
            this.colValCredLiq.DisplayFormat.FormatString = "n2";
            this.colValCredLiq.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.colValCredLiq.FieldName = "ValCredLiq";
            this.colValCredLiq.Fixed = DevExpress.XtraGrid.Columns.FixedStyle.Right;
            this.colValCredLiq.Name = "colValCredLiq";
            this.colValCredLiq.OptionsColumn.ReadOnly = true;
            this.colValCredLiq.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum, "ValCredLiq", "{0:n2}")});
            this.colValCredLiq.Visible = true;
            this.colValCredLiq.VisibleIndex = 12;
            this.colValCredLiq.Width = 90;
            // 
            // colCCT_CON
            // 
            this.colCCT_CON.FieldName = "CCT_CON";
            this.colCCT_CON.Name = "colCCT_CON";
            // 
            // colErro3
            // 
            this.colErro3.ColumnEdit = this.repositoryItemMemoExEdit3;
            this.colErro3.FieldName = "Erro";
            this.colErro3.Fixed = DevExpress.XtraGrid.Columns.FixedStyle.Left;
            this.colErro3.Name = "colErro3";
            this.colErro3.OptionsColumn.ReadOnly = true;
            this.colErro3.Visible = true;
            this.colErro3.VisibleIndex = 1;
            this.colErro3.Width = 29;
            // 
            // repositoryItemMemoExEdit3
            // 
            this.repositoryItemMemoExEdit3.AppearanceDropDown.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.repositoryItemMemoExEdit3.AppearanceDropDown.BackColor2 = System.Drawing.Color.White;
            this.repositoryItemMemoExEdit3.AppearanceDropDown.Font = new System.Drawing.Font("Courier New", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.repositoryItemMemoExEdit3.AppearanceDropDown.GradientMode = System.Drawing.Drawing2D.LinearGradientMode.ForwardDiagonal;
            this.repositoryItemMemoExEdit3.AppearanceDropDown.Options.UseBackColor = true;
            this.repositoryItemMemoExEdit3.AppearanceDropDown.Options.UseFont = true;
            this.repositoryItemMemoExEdit3.AutoHeight = false;
            this.repositoryItemMemoExEdit3.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemMemoExEdit3.Images = this.imageCollection1;
            this.repositoryItemMemoExEdit3.Name = "repositoryItemMemoExEdit3";
            this.repositoryItemMemoExEdit3.PopupFormMinSize = new System.Drawing.Size(400, 0);
            // 
            // colCCT3
            // 
            this.colCCT3.FieldName = "CCT";
            this.colCCT3.Name = "colCCT3";
            // 
            // repBotaoBoleto
            // 
            this.repBotaoBoleto.AutoHeight = false;
            editorButtonImageOptions11.Image = ((System.Drawing.Image)(resources.GetObject("editorButtonImageOptions11.Image")));
            this.repBotaoBoleto.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(editorButtonImageOptions11, DevExpress.XtraEditors.Controls.ButtonPredefines.Glyph, null)});
            this.repBotaoBoleto.Name = "repBotaoBoleto";
            this.repBotaoBoleto.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.HideTextEditor;
            this.repBotaoBoleto.ButtonClick += new DevExpress.XtraEditors.Controls.ButtonPressedEventHandler(this.repBotaoBoleto_ButtonClick);
            // 
            // xtraTabCredGrafico
            // 
            this.xtraTabCredGrafico.Controls.Add(this.chartControl1);
            this.xtraTabCredGrafico.Controls.Add(this.gridControl11);
            this.xtraTabCredGrafico.Name = "xtraTabCredGrafico";
            this.xtraTabCredGrafico.Size = new System.Drawing.Size(1453, 204);
            this.xtraTabCredGrafico.Text = "Gráfico";
            // 
            // chartControl1
            // 
            this.chartControl1.DataBindings = null;
            simpleDiagram3D1.LabelsResolveOverlappingMinIndent = 5;
            simpleDiagram3D1.RotationMatrixSerializable = "0.999901095938711;-0.00311194241939914;-0.0137154713715572;0;-0.00767036567099105" +
    ";0.696742991715813;-0.717279840080129;0;0.0117882921171366;0.717314100871633;0.6" +
    "96650211267953;0;0;0;0;1";
            this.chartControl1.Diagram = simpleDiagram3D1;
            this.chartControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.chartControl1.Legend.AlignmentHorizontal = DevExpress.XtraCharts.LegendAlignmentHorizontal.Center;
            this.chartControl1.Legend.AlignmentVertical = DevExpress.XtraCharts.LegendAlignmentVertical.TopOutside;
            this.chartControl1.Legend.BackColor = System.Drawing.Color.White;
            this.chartControl1.Legend.EnableAntialiasing = DevExpress.Utils.DefaultBoolean.True;
            this.chartControl1.Legend.Name = "Default Legend";
            this.chartControl1.Legend.Shadow.Visible = true;
            this.chartControl1.Location = new System.Drawing.Point(347, 0);
            this.chartControl1.Name = "chartControl1";
            series1.ArgumentDataMember = "Grafico.Titulo";
            series1.ArgumentScaleType = DevExpress.XtraCharts.ScaleType.Qualitative;
            series1.DataFilters.ClearAndAddRange(new DevExpress.XtraCharts.DataFilter[] {
            new DevExpress.XtraCharts.DataFilter("Grafico.Valor", "System.Decimal", DevExpress.XtraCharts.DataFilterCondition.NotEqual, new decimal(new int[] {
                            0,
                            0,
                            0,
                            0}))});
            series1.DataSource = this.dBalanceteBindingSource;
            pie3DSeriesLabel1.Border.Visibility = DevExpress.Utils.DefaultBoolean.False;
            pie3DSeriesLabel1.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Bold);
            pie3DSeriesLabel1.Position = DevExpress.XtraCharts.PieSeriesLabelPosition.TwoColumns;
            pie3DSeriesLabel1.ResolveOverlappingMode = DevExpress.XtraCharts.ResolveOverlappingMode.Default;
            pie3DSeriesLabel1.TextAlignment = System.Drawing.StringAlignment.Near;
            pie3DSeriesLabel1.TextColor = System.Drawing.Color.Black;
            series1.Label = pie3DSeriesLabel1;
            series1.LabelsVisibility = DevExpress.Utils.DefaultBoolean.True;
            series1.LegendTextPattern = "{A}: {VP:P2}";
            series1.Name = "Series 1";
            series1.ValueDataMembersSerializable = "Grafico.Valor";
            pie3DSeriesView1.ExplodeMode = DevExpress.XtraCharts.PieExplodeMode.All;
            series1.View = pie3DSeriesView1;
            this.chartControl1.SeriesSerializable = new DevExpress.XtraCharts.Series[] {
        series1};
            pie3DSeriesLabel2.ResolveOverlappingMode = DevExpress.XtraCharts.ResolveOverlappingMode.Default;
            this.chartControl1.SeriesTemplate.Label = pie3DSeriesLabel2;
            this.chartControl1.SeriesTemplate.View = pie3DSeriesView2;
            this.chartControl1.Size = new System.Drawing.Size(1106, 204);
            this.chartControl1.TabIndex = 1;
            // 
            // gridControl11
            // 
            this.gridControl11.DataMember = "Grafico";
            this.gridControl11.DataSource = this.dBalanceteBindingSource;
            this.gridControl11.Dock = System.Windows.Forms.DockStyle.Left;
            this.gridControl11.Location = new System.Drawing.Point(0, 0);
            this.gridControl11.MainView = this.gridView10;
            this.gridControl11.Name = "gridControl11";
            this.gridControl11.ShowOnlyPredefinedDetails = true;
            this.gridControl11.Size = new System.Drawing.Size(347, 204);
            this.gridControl11.TabIndex = 0;
            this.gridControl11.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridView10});
            // 
            // gridView10
            // 
            this.gridView10.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colTitulo,
            this.colValor4,
            this.colClaGrafico1});
            this.gridView10.GridControl = this.gridControl11;
            this.gridView10.Name = "gridView10";
            this.gridView10.OptionsView.ColumnAutoWidth = false;
            this.gridView10.OptionsView.ShowGroupPanel = false;
            this.gridView10.SortInfo.AddRange(new DevExpress.XtraGrid.Columns.GridColumnSortInfo[] {
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colClaGrafico1, DevExpress.Data.ColumnSortOrder.Ascending)});
            // 
            // colTitulo
            // 
            this.colTitulo.Caption = "Grupo";
            this.colTitulo.FieldName = "Titulo";
            this.colTitulo.Name = "colTitulo";
            this.colTitulo.OptionsColumn.ReadOnly = true;
            this.colTitulo.Visible = true;
            this.colTitulo.VisibleIndex = 0;
            this.colTitulo.Width = 218;
            // 
            // colValor4
            // 
            this.colValor4.Caption = "Valor";
            this.colValor4.DisplayFormat.FormatString = "n2";
            this.colValor4.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.colValor4.FieldName = "Valor";
            this.colValor4.Name = "colValor4";
            this.colValor4.OptionsColumn.ReadOnly = true;
            this.colValor4.Visible = true;
            this.colValor4.VisibleIndex = 1;
            this.colValor4.Width = 88;
            // 
            // colClaGrafico1
            // 
            this.colClaGrafico1.FieldName = "ClaGrafico";
            this.colClaGrafico1.Name = "colClaGrafico1";
            this.colClaGrafico1.OptionsColumn.ReadOnly = true;
            // 
            // xtraTabPageDebitos
            // 
            this.xtraTabPageDebitos.Controls.Add(this.gridControl6);
            this.xtraTabPageDebitos.Name = "xtraTabPageDebitos";
            this.xtraTabPageDebitos.Size = new System.Drawing.Size(1459, 232);
            this.xtraTabPageDebitos.Text = "Débitos";
            // 
            // gridControl6
            // 
            this.gridControl6.DataMember = "Debitos";
            this.gridControl6.DataSource = this.dCreditosBindingSource;
            this.gridControl6.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridControl6.Location = new System.Drawing.Point(0, 0);
            this.gridControl6.MainView = this.gridView6;
            this.gridControl6.Name = "gridControl6";
            this.gridControl6.Size = new System.Drawing.Size(1459, 232);
            this.gridControl6.TabIndex = 0;
            this.gridControl6.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridView6});
            // 
            // gridView6
            // 
            this.gridView6.Appearance.ColumnFilterButton.BackColor = System.Drawing.Color.Orange;
            this.gridView6.Appearance.ColumnFilterButton.BorderColor = System.Drawing.Color.Orange;
            this.gridView6.Appearance.ColumnFilterButton.ForeColor = System.Drawing.Color.DarkOrange;
            this.gridView6.Appearance.ColumnFilterButton.Options.UseBackColor = true;
            this.gridView6.Appearance.ColumnFilterButton.Options.UseBorderColor = true;
            this.gridView6.Appearance.ColumnFilterButton.Options.UseForeColor = true;
            this.gridView6.Appearance.ColumnFilterButtonActive.BackColor = System.Drawing.Color.DarkOrange;
            this.gridView6.Appearance.ColumnFilterButtonActive.BorderColor = System.Drawing.Color.DarkOrange;
            this.gridView6.Appearance.ColumnFilterButtonActive.ForeColor = System.Drawing.Color.White;
            this.gridView6.Appearance.ColumnFilterButtonActive.Options.UseBackColor = true;
            this.gridView6.Appearance.ColumnFilterButtonActive.Options.UseBorderColor = true;
            this.gridView6.Appearance.ColumnFilterButtonActive.Options.UseForeColor = true;
            this.gridView6.Appearance.Empty.BackColor = System.Drawing.Color.LightSkyBlue;
            this.gridView6.Appearance.Empty.BackColor2 = System.Drawing.Color.SkyBlue;
            this.gridView6.Appearance.Empty.GradientMode = System.Drawing.Drawing2D.LinearGradientMode.BackwardDiagonal;
            this.gridView6.Appearance.Empty.Options.UseBackColor = true;
            this.gridView6.Appearance.EvenRow.BackColor = System.Drawing.Color.Linen;
            this.gridView6.Appearance.EvenRow.BackColor2 = System.Drawing.Color.AntiqueWhite;
            this.gridView6.Appearance.EvenRow.GradientMode = System.Drawing.Drawing2D.LinearGradientMode.ForwardDiagonal;
            this.gridView6.Appearance.EvenRow.Options.UseBackColor = true;
            this.gridView6.Appearance.FilterCloseButton.BackColor = System.Drawing.Color.Orange;
            this.gridView6.Appearance.FilterCloseButton.BorderColor = System.Drawing.Color.Orange;
            this.gridView6.Appearance.FilterCloseButton.Options.UseBackColor = true;
            this.gridView6.Appearance.FilterCloseButton.Options.UseBorderColor = true;
            this.gridView6.Appearance.FilterPanel.BackColor = System.Drawing.Color.DarkOrange;
            this.gridView6.Appearance.FilterPanel.BackColor2 = System.Drawing.Color.Orange;
            this.gridView6.Appearance.FilterPanel.ForeColor = System.Drawing.Color.Black;
            this.gridView6.Appearance.FilterPanel.GradientMode = System.Drawing.Drawing2D.LinearGradientMode.BackwardDiagonal;
            this.gridView6.Appearance.FilterPanel.Options.UseBackColor = true;
            this.gridView6.Appearance.FilterPanel.Options.UseForeColor = true;
            this.gridView6.Appearance.FocusedRow.BackColor = System.Drawing.Color.RoyalBlue;
            this.gridView6.Appearance.FocusedRow.BackColor2 = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this.gridView6.Appearance.FocusedRow.ForeColor = System.Drawing.Color.White;
            this.gridView6.Appearance.FocusedRow.GradientMode = System.Drawing.Drawing2D.LinearGradientMode.Vertical;
            this.gridView6.Appearance.FocusedRow.Options.UseBackColor = true;
            this.gridView6.Appearance.FocusedRow.Options.UseForeColor = true;
            this.gridView6.Appearance.FooterPanel.BackColor = System.Drawing.Color.Orange;
            this.gridView6.Appearance.FooterPanel.BorderColor = System.Drawing.Color.Orange;
            this.gridView6.Appearance.FooterPanel.Options.UseBackColor = true;
            this.gridView6.Appearance.FooterPanel.Options.UseBorderColor = true;
            this.gridView6.Appearance.GroupButton.BackColor = System.Drawing.Color.Wheat;
            this.gridView6.Appearance.GroupButton.BorderColor = System.Drawing.Color.Wheat;
            this.gridView6.Appearance.GroupButton.Options.UseBackColor = true;
            this.gridView6.Appearance.GroupButton.Options.UseBorderColor = true;
            this.gridView6.Appearance.GroupFooter.BackColor = System.Drawing.Color.Wheat;
            this.gridView6.Appearance.GroupFooter.BorderColor = System.Drawing.Color.Wheat;
            this.gridView6.Appearance.GroupFooter.Options.UseBackColor = true;
            this.gridView6.Appearance.GroupFooter.Options.UseBorderColor = true;
            this.gridView6.Appearance.GroupPanel.BackColor = System.Drawing.Color.RoyalBlue;
            this.gridView6.Appearance.GroupPanel.ForeColor = System.Drawing.Color.White;
            this.gridView6.Appearance.GroupPanel.Options.UseBackColor = true;
            this.gridView6.Appearance.GroupPanel.Options.UseForeColor = true;
            this.gridView6.Appearance.GroupRow.BackColor = System.Drawing.Color.Wheat;
            this.gridView6.Appearance.GroupRow.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Bold);
            this.gridView6.Appearance.GroupRow.Options.UseBackColor = true;
            this.gridView6.Appearance.GroupRow.Options.UseFont = true;
            this.gridView6.Appearance.HeaderPanel.BackColor = System.Drawing.Color.Orange;
            this.gridView6.Appearance.HeaderPanel.BorderColor = System.Drawing.Color.Orange;
            this.gridView6.Appearance.HeaderPanel.Options.UseBackColor = true;
            this.gridView6.Appearance.HeaderPanel.Options.UseBorderColor = true;
            this.gridView6.Appearance.HideSelectionRow.BackColor = System.Drawing.Color.LightSlateGray;
            this.gridView6.Appearance.HideSelectionRow.Options.UseBackColor = true;
            this.gridView6.Appearance.HorzLine.BackColor = System.Drawing.Color.Tan;
            this.gridView6.Appearance.HorzLine.Options.UseBackColor = true;
            this.gridView6.Appearance.OddRow.BackColor = System.Drawing.Color.White;
            this.gridView6.Appearance.OddRow.Options.UseBackColor = true;
            this.gridView6.Appearance.Preview.BackColor = System.Drawing.Color.Khaki;
            this.gridView6.Appearance.Preview.BackColor2 = System.Drawing.Color.Cornsilk;
            this.gridView6.Appearance.Preview.Font = new System.Drawing.Font("Tahoma", 7.5F);
            this.gridView6.Appearance.Preview.GradientMode = System.Drawing.Drawing2D.LinearGradientMode.BackwardDiagonal;
            this.gridView6.Appearance.Preview.Options.UseBackColor = true;
            this.gridView6.Appearance.Preview.Options.UseFont = true;
            this.gridView6.Appearance.Row.BackColor = System.Drawing.Color.White;
            this.gridView6.Appearance.Row.Options.UseBackColor = true;
            this.gridView6.Appearance.RowSeparator.BackColor = System.Drawing.Color.LightSkyBlue;
            this.gridView6.Appearance.RowSeparator.Options.UseBackColor = true;
            this.gridView6.Appearance.VertLine.BackColor = System.Drawing.Color.Tan;
            this.gridView6.Appearance.VertLine.Options.UseBackColor = true;
            this.gridView6.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colGrupo,
            this.colDescricao4,
            this.colValor3,
            this.colOrdem3,
            this.colPLA3,
            this.colSuperGrupo,
            this.colnGrupo,
            this.colnSuperGrupo,
            this.colcalSG,
            this.colcalG});
            this.gridView6.GridControl = this.gridControl6;
            this.gridView6.GroupCount = 2;
            this.gridView6.GroupFormat = "[#image]{1}: {2}";
            this.gridView6.GroupSummary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridGroupSummaryItem(DevExpress.Data.SummaryItemType.Sum, "Valor", null, "{0:n2}")});
            this.gridView6.Name = "gridView6";
            this.gridView6.OptionsBehavior.AutoExpandAllGroups = true;
            this.gridView6.OptionsView.ColumnAutoWidth = false;
            this.gridView6.OptionsView.EnableAppearanceEvenRow = true;
            this.gridView6.OptionsView.EnableAppearanceOddRow = true;
            this.gridView6.OptionsView.ShowAutoFilterRow = true;
            this.gridView6.OptionsView.ShowFooter = true;
            this.gridView6.OptionsView.ShowGroupPanel = false;
            this.gridView6.SortInfo.AddRange(new DevExpress.XtraGrid.Columns.GridColumnSortInfo[] {
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colcalSG, DevExpress.Data.ColumnSortOrder.Ascending),
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colcalG, DevExpress.Data.ColumnSortOrder.Ascending),
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colOrdem3, DevExpress.Data.ColumnSortOrder.Ascending),
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colnSuperGrupo, DevExpress.Data.ColumnSortOrder.Ascending),
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colnGrupo, DevExpress.Data.ColumnSortOrder.Ascending)});
            // 
            // colGrupo
            // 
            this.colGrupo.Caption = "Grupo";
            this.colGrupo.FieldName = "Grupo";
            this.colGrupo.Name = "colGrupo";
            this.colGrupo.OptionsColumn.ReadOnly = true;
            this.colGrupo.Width = 116;
            // 
            // colDescricao4
            // 
            this.colDescricao4.Caption = "Descrição";
            this.colDescricao4.FieldName = "Descricao";
            this.colDescricao4.Name = "colDescricao4";
            this.colDescricao4.OptionsColumn.ReadOnly = true;
            this.colDescricao4.Visible = true;
            this.colDescricao4.VisibleIndex = 1;
            this.colDescricao4.Width = 317;
            // 
            // colValor3
            // 
            this.colValor3.Caption = "Valor";
            this.colValor3.DisplayFormat.FormatString = "n2";
            this.colValor3.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.colValor3.FieldName = "Valor";
            this.colValor3.Name = "colValor3";
            this.colValor3.OptionsColumn.ReadOnly = true;
            this.colValor3.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum, "Valor", "{0:n2}")});
            this.colValor3.Visible = true;
            this.colValor3.VisibleIndex = 2;
            this.colValor3.Width = 118;
            // 
            // colOrdem3
            // 
            this.colOrdem3.Caption = "Ordem";
            this.colOrdem3.FieldName = "Ordem";
            this.colOrdem3.Name = "colOrdem3";
            this.colOrdem3.OptionsColumn.ReadOnly = true;
            // 
            // colPLA3
            // 
            this.colPLA3.Caption = "Classificação";
            this.colPLA3.FieldName = "PLA";
            this.colPLA3.Name = "colPLA3";
            this.colPLA3.OptionsColumn.ReadOnly = true;
            this.colPLA3.Visible = true;
            this.colPLA3.VisibleIndex = 0;
            this.colPLA3.Width = 95;
            // 
            // colSuperGrupo
            // 
            this.colSuperGrupo.Caption = "Super Grupo";
            this.colSuperGrupo.FieldName = "SuperGrupo";
            this.colSuperGrupo.Name = "colSuperGrupo";
            // 
            // colnGrupo
            // 
            this.colnGrupo.Caption = "Ordem Grupo";
            this.colnGrupo.FieldName = "nGrupo";
            this.colnGrupo.Name = "colnGrupo";
            // 
            // colnSuperGrupo
            // 
            this.colnSuperGrupo.Caption = "ordem Super Grupo";
            this.colnSuperGrupo.FieldName = "nSuperGrupo";
            this.colnSuperGrupo.Name = "colnSuperGrupo";
            // 
            // colcalSG
            // 
            this.colcalSG.FieldName = "calSG";
            this.colcalSG.Name = "colcalSG";
            // 
            // colcalG
            // 
            this.colcalG.FieldName = "calG";
            this.colcalG.Name = "colcalG";
            this.colcalG.Visible = true;
            this.colcalG.VisibleIndex = 3;
            // 
            // xtraTabPageCTL
            // 
            this.xtraTabPageCTL.Controls.Add(this.gridControl5);
            this.xtraTabPageCTL.Name = "xtraTabPageCTL";
            this.xtraTabPageCTL.Size = new System.Drawing.Size(1459, 232);
            this.xtraTabPageCTL.Text = "Contas Lógicas";
            // 
            // gridControl5
            // 
            this.gridControl5.DataMember = "ConTasLogicas";
            this.gridControl5.DataSource = this.dBalanceteBindingSource;
            this.gridControl5.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridControl5.Location = new System.Drawing.Point(0, 0);
            this.gridControl5.MainView = this.gridView5;
            this.gridControl5.Name = "gridControl5";
            this.gridControl5.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.CalcEditn2});
            this.gridControl5.ShowOnlyPredefinedDetails = true;
            this.gridControl5.Size = new System.Drawing.Size(1459, 232);
            this.gridControl5.TabIndex = 0;
            this.gridControl5.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridView5});
            // 
            // gridView5
            // 
            this.gridView5.Appearance.ColumnFilterButton.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(244)))), ((int)(((byte)(247)))), ((int)(((byte)(222)))));
            this.gridView5.Appearance.ColumnFilterButton.BackColor2 = System.Drawing.Color.FromArgb(((int)(((byte)(184)))), ((int)(((byte)(199)))), ((int)(((byte)(146)))));
            this.gridView5.Appearance.ColumnFilterButton.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(244)))), ((int)(((byte)(247)))), ((int)(((byte)(222)))));
            this.gridView5.Appearance.ColumnFilterButton.ForeColor = System.Drawing.Color.Black;
            this.gridView5.Appearance.ColumnFilterButton.GradientMode = System.Drawing.Drawing2D.LinearGradientMode.Vertical;
            this.gridView5.Appearance.ColumnFilterButton.Options.UseBackColor = true;
            this.gridView5.Appearance.ColumnFilterButton.Options.UseBorderColor = true;
            this.gridView5.Appearance.ColumnFilterButton.Options.UseForeColor = true;
            this.gridView5.Appearance.ColumnFilterButtonActive.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(254)))), ((int)(((byte)(255)))), ((int)(((byte)(244)))));
            this.gridView5.Appearance.ColumnFilterButtonActive.BackColor2 = System.Drawing.Color.FromArgb(((int)(((byte)(213)))), ((int)(((byte)(222)))), ((int)(((byte)(183)))));
            this.gridView5.Appearance.ColumnFilterButtonActive.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(254)))), ((int)(((byte)(255)))), ((int)(((byte)(244)))));
            this.gridView5.Appearance.ColumnFilterButtonActive.ForeColor = System.Drawing.Color.Black;
            this.gridView5.Appearance.ColumnFilterButtonActive.GradientMode = System.Drawing.Drawing2D.LinearGradientMode.Vertical;
            this.gridView5.Appearance.ColumnFilterButtonActive.Options.UseBackColor = true;
            this.gridView5.Appearance.ColumnFilterButtonActive.Options.UseBorderColor = true;
            this.gridView5.Appearance.ColumnFilterButtonActive.Options.UseForeColor = true;
            this.gridView5.Appearance.Empty.BackColor = System.Drawing.Color.White;
            this.gridView5.Appearance.Empty.Options.UseBackColor = true;
            this.gridView5.Appearance.EvenRow.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(244)))), ((int)(((byte)(247)))), ((int)(((byte)(222)))));
            this.gridView5.Appearance.EvenRow.ForeColor = System.Drawing.Color.Black;
            this.gridView5.Appearance.EvenRow.Options.UseBackColor = true;
            this.gridView5.Appearance.EvenRow.Options.UseForeColor = true;
            this.gridView5.Appearance.FilterCloseButton.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(244)))), ((int)(((byte)(247)))), ((int)(((byte)(222)))));
            this.gridView5.Appearance.FilterCloseButton.BackColor2 = System.Drawing.Color.FromArgb(((int)(((byte)(184)))), ((int)(((byte)(199)))), ((int)(((byte)(146)))));
            this.gridView5.Appearance.FilterCloseButton.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(244)))), ((int)(((byte)(247)))), ((int)(((byte)(222)))));
            this.gridView5.Appearance.FilterCloseButton.ForeColor = System.Drawing.Color.Black;
            this.gridView5.Appearance.FilterCloseButton.GradientMode = System.Drawing.Drawing2D.LinearGradientMode.Vertical;
            this.gridView5.Appearance.FilterCloseButton.Options.UseBackColor = true;
            this.gridView5.Appearance.FilterCloseButton.Options.UseBorderColor = true;
            this.gridView5.Appearance.FilterCloseButton.Options.UseForeColor = true;
            this.gridView5.Appearance.FilterPanel.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(139)))), ((int)(((byte)(158)))), ((int)(((byte)(126)))));
            this.gridView5.Appearance.FilterPanel.ForeColor = System.Drawing.Color.White;
            this.gridView5.Appearance.FilterPanel.Options.UseBackColor = true;
            this.gridView5.Appearance.FilterPanel.Options.UseForeColor = true;
            this.gridView5.Appearance.FixedLine.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(96)))), ((int)(((byte)(128)))), ((int)(((byte)(88)))));
            this.gridView5.Appearance.FixedLine.Options.UseBackColor = true;
            this.gridView5.Appearance.FocusedCell.BackColor = System.Drawing.Color.White;
            this.gridView5.Appearance.FocusedCell.ForeColor = System.Drawing.Color.Black;
            this.gridView5.Appearance.FocusedCell.Options.UseBackColor = true;
            this.gridView5.Appearance.FocusedCell.Options.UseForeColor = true;
            this.gridView5.Appearance.FocusedRow.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(147)))), ((int)(((byte)(160)))), ((int)(((byte)(112)))));
            this.gridView5.Appearance.FocusedRow.ForeColor = System.Drawing.Color.White;
            this.gridView5.Appearance.FocusedRow.Options.UseBackColor = true;
            this.gridView5.Appearance.FocusedRow.Options.UseForeColor = true;
            this.gridView5.Appearance.FooterPanel.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(244)))), ((int)(((byte)(247)))), ((int)(((byte)(222)))));
            this.gridView5.Appearance.FooterPanel.BackColor2 = System.Drawing.Color.FromArgb(((int)(((byte)(184)))), ((int)(((byte)(199)))), ((int)(((byte)(146)))));
            this.gridView5.Appearance.FooterPanel.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(244)))), ((int)(((byte)(247)))), ((int)(((byte)(222)))));
            this.gridView5.Appearance.FooterPanel.ForeColor = System.Drawing.Color.Black;
            this.gridView5.Appearance.FooterPanel.GradientMode = System.Drawing.Drawing2D.LinearGradientMode.Vertical;
            this.gridView5.Appearance.FooterPanel.Options.UseBackColor = true;
            this.gridView5.Appearance.FooterPanel.Options.UseBorderColor = true;
            this.gridView5.Appearance.FooterPanel.Options.UseForeColor = true;
            this.gridView5.Appearance.GroupButton.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(223)))), ((int)(((byte)(230)))), ((int)(((byte)(195)))));
            this.gridView5.Appearance.GroupButton.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(223)))), ((int)(((byte)(230)))), ((int)(((byte)(195)))));
            this.gridView5.Appearance.GroupButton.ForeColor = System.Drawing.Color.Black;
            this.gridView5.Appearance.GroupButton.Options.UseBackColor = true;
            this.gridView5.Appearance.GroupButton.Options.UseBorderColor = true;
            this.gridView5.Appearance.GroupButton.Options.UseForeColor = true;
            this.gridView5.Appearance.GroupFooter.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(223)))), ((int)(((byte)(230)))), ((int)(((byte)(195)))));
            this.gridView5.Appearance.GroupFooter.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(223)))), ((int)(((byte)(230)))), ((int)(((byte)(195)))));
            this.gridView5.Appearance.GroupFooter.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Bold);
            this.gridView5.Appearance.GroupFooter.ForeColor = System.Drawing.Color.Black;
            this.gridView5.Appearance.GroupFooter.Options.UseBackColor = true;
            this.gridView5.Appearance.GroupFooter.Options.UseBorderColor = true;
            this.gridView5.Appearance.GroupFooter.Options.UseFont = true;
            this.gridView5.Appearance.GroupFooter.Options.UseForeColor = true;
            this.gridView5.Appearance.GroupPanel.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(139)))), ((int)(((byte)(158)))), ((int)(((byte)(126)))));
            this.gridView5.Appearance.GroupPanel.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(244)))), ((int)(((byte)(247)))), ((int)(((byte)(222)))));
            this.gridView5.Appearance.GroupPanel.Options.UseBackColor = true;
            this.gridView5.Appearance.GroupPanel.Options.UseForeColor = true;
            this.gridView5.Appearance.GroupRow.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(223)))), ((int)(((byte)(230)))), ((int)(((byte)(195)))));
            this.gridView5.Appearance.GroupRow.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(223)))), ((int)(((byte)(230)))), ((int)(((byte)(195)))));
            this.gridView5.Appearance.GroupRow.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Bold);
            this.gridView5.Appearance.GroupRow.ForeColor = System.Drawing.Color.Black;
            this.gridView5.Appearance.GroupRow.Options.UseBackColor = true;
            this.gridView5.Appearance.GroupRow.Options.UseBorderColor = true;
            this.gridView5.Appearance.GroupRow.Options.UseFont = true;
            this.gridView5.Appearance.GroupRow.Options.UseForeColor = true;
            this.gridView5.Appearance.HeaderPanel.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(244)))), ((int)(((byte)(247)))), ((int)(((byte)(222)))));
            this.gridView5.Appearance.HeaderPanel.BackColor2 = System.Drawing.Color.FromArgb(((int)(((byte)(184)))), ((int)(((byte)(199)))), ((int)(((byte)(146)))));
            this.gridView5.Appearance.HeaderPanel.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(244)))), ((int)(((byte)(247)))), ((int)(((byte)(222)))));
            this.gridView5.Appearance.HeaderPanel.ForeColor = System.Drawing.Color.Black;
            this.gridView5.Appearance.HeaderPanel.GradientMode = System.Drawing.Drawing2D.LinearGradientMode.Vertical;
            this.gridView5.Appearance.HeaderPanel.Options.UseBackColor = true;
            this.gridView5.Appearance.HeaderPanel.Options.UseBorderColor = true;
            this.gridView5.Appearance.HeaderPanel.Options.UseForeColor = true;
            this.gridView5.Appearance.HideSelectionRow.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(197)))), ((int)(((byte)(207)))), ((int)(((byte)(170)))));
            this.gridView5.Appearance.HideSelectionRow.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(147)))), ((int)(((byte)(160)))), ((int)(((byte)(112)))));
            this.gridView5.Appearance.HideSelectionRow.Options.UseBackColor = true;
            this.gridView5.Appearance.HideSelectionRow.Options.UseForeColor = true;
            this.gridView5.Appearance.HorzLine.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(136)))), ((int)(((byte)(168)))), ((int)(((byte)(128)))));
            this.gridView5.Appearance.HorzLine.Options.UseBackColor = true;
            this.gridView5.Appearance.Preview.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(252)))), ((int)(((byte)(253)))), ((int)(((byte)(247)))));
            this.gridView5.Appearance.Preview.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(147)))), ((int)(((byte)(160)))), ((int)(((byte)(112)))));
            this.gridView5.Appearance.Preview.Options.UseBackColor = true;
            this.gridView5.Appearance.Preview.Options.UseForeColor = true;
            this.gridView5.Appearance.Row.BackColor = System.Drawing.Color.White;
            this.gridView5.Appearance.Row.ForeColor = System.Drawing.Color.Black;
            this.gridView5.Appearance.Row.Options.UseBackColor = true;
            this.gridView5.Appearance.Row.Options.UseForeColor = true;
            this.gridView5.Appearance.RowSeparator.BackColor = System.Drawing.Color.White;
            this.gridView5.Appearance.RowSeparator.Options.UseBackColor = true;
            this.gridView5.Appearance.SelectedRow.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(166)))), ((int)(((byte)(178)))), ((int)(((byte)(133)))));
            this.gridView5.Appearance.SelectedRow.ForeColor = System.Drawing.Color.White;
            this.gridView5.Appearance.SelectedRow.Options.UseBackColor = true;
            this.gridView5.Appearance.SelectedRow.Options.UseForeColor = true;
            this.gridView5.Appearance.TopNewRow.BackColor = System.Drawing.Color.White;
            this.gridView5.Appearance.TopNewRow.Options.UseBackColor = true;
            this.gridView5.Appearance.VertLine.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(136)))), ((int)(((byte)(160)))), ((int)(((byte)(188)))));
            this.gridView5.Appearance.VertLine.Options.UseBackColor = true;
            this.gridView5.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colCTL3,
            this.colCTLAtiva,
            this.colCTLTitulo,
            this.colCTLTipo,
            this.colCTL_CCT,
            this.colSALDOI,
            this.colSALDOF,
            this.colCREDITOS,
            this.colDEBITOS,
            this.colTRANSFERENCIA,
            this.colRENTABILIDADE});
            this.gridView5.GridControl = this.gridControl5;
            this.gridView5.Name = "gridView5";
            this.gridView5.OptionsView.ColumnAutoWidth = false;
            this.gridView5.OptionsView.EnableAppearanceEvenRow = true;
            this.gridView5.OptionsView.ShowFooter = true;
            this.gridView5.OptionsView.ShowGroupPanel = false;
            // 
            // colCTL3
            // 
            this.colCTL3.FieldName = "CTL";
            this.colCTL3.Name = "colCTL3";
            this.colCTL3.OptionsColumn.ReadOnly = true;
            // 
            // colCTLAtiva
            // 
            this.colCTLAtiva.FieldName = "CTLAtiva";
            this.colCTLAtiva.Name = "colCTLAtiva";
            this.colCTLAtiva.OptionsColumn.ReadOnly = true;
            // 
            // colCTLTitulo
            // 
            this.colCTLTitulo.Caption = "Título";
            this.colCTLTitulo.FieldName = "CTLTitulo";
            this.colCTLTitulo.Name = "colCTLTitulo";
            this.colCTLTitulo.OptionsColumn.ReadOnly = true;
            this.colCTLTitulo.Visible = true;
            this.colCTLTitulo.VisibleIndex = 0;
            this.colCTLTitulo.Width = 323;
            // 
            // colCTLTipo
            // 
            this.colCTLTipo.FieldName = "CTLTipo";
            this.colCTLTipo.Name = "colCTLTipo";
            this.colCTLTipo.OptionsColumn.ReadOnly = true;
            // 
            // colCTL_CCT
            // 
            this.colCTL_CCT.FieldName = "CTL_CCT";
            this.colCTL_CCT.Name = "colCTL_CCT";
            this.colCTL_CCT.OptionsColumn.ReadOnly = true;
            // 
            // colSALDOI
            // 
            this.colSALDOI.Caption = "Saldo Inicial";
            this.colSALDOI.ColumnEdit = this.CalcEditn2;
            this.colSALDOI.FieldName = "SALDOI";
            this.colSALDOI.Name = "colSALDOI";
            this.colSALDOI.OptionsColumn.ReadOnly = true;
            this.colSALDOI.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum, "SALDOI", "{0:n2}")});
            this.colSALDOI.Visible = true;
            this.colSALDOI.VisibleIndex = 1;
            this.colSALDOI.Width = 100;
            // 
            // CalcEditn2
            // 
            this.CalcEditn2.AutoHeight = false;
            this.CalcEditn2.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.CalcEditn2.DisplayFormat.FormatString = "n2";
            this.CalcEditn2.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.CalcEditn2.EditFormat.FormatString = "n2";
            this.CalcEditn2.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.CalcEditn2.Name = "CalcEditn2";
            // 
            // colSALDOF
            // 
            this.colSALDOF.Caption = "Saldo Final";
            this.colSALDOF.ColumnEdit = this.CalcEditn2;
            this.colSALDOF.FieldName = "SALDOF";
            this.colSALDOF.Name = "colSALDOF";
            this.colSALDOF.OptionsColumn.ReadOnly = true;
            this.colSALDOF.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum, "SALDOF", "{0:n2}")});
            this.colSALDOF.Visible = true;
            this.colSALDOF.VisibleIndex = 6;
            this.colSALDOF.Width = 100;
            // 
            // colCREDITOS
            // 
            this.colCREDITOS.Caption = "Créditos";
            this.colCREDITOS.ColumnEdit = this.CalcEditn2;
            this.colCREDITOS.FieldName = "CREDITOS";
            this.colCREDITOS.Name = "colCREDITOS";
            this.colCREDITOS.OptionsColumn.ReadOnly = true;
            this.colCREDITOS.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum, "CREDITOS", "{0:n2}")});
            this.colCREDITOS.Visible = true;
            this.colCREDITOS.VisibleIndex = 2;
            this.colCREDITOS.Width = 100;
            // 
            // colDEBITOS
            // 
            this.colDEBITOS.Caption = "Débitos";
            this.colDEBITOS.ColumnEdit = this.CalcEditn2;
            this.colDEBITOS.FieldName = "DEBITOS";
            this.colDEBITOS.Name = "colDEBITOS";
            this.colDEBITOS.OptionsColumn.ReadOnly = true;
            this.colDEBITOS.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum, "DEBITOS", "{0:n2}")});
            this.colDEBITOS.Visible = true;
            this.colDEBITOS.VisibleIndex = 3;
            this.colDEBITOS.Width = 100;
            // 
            // colTRANSFERENCIA
            // 
            this.colTRANSFERENCIA.Caption = "Transferências";
            this.colTRANSFERENCIA.ColumnEdit = this.CalcEditn2;
            this.colTRANSFERENCIA.FieldName = "TRANSFERENCIA";
            this.colTRANSFERENCIA.Name = "colTRANSFERENCIA";
            this.colTRANSFERENCIA.OptionsColumn.ReadOnly = true;
            this.colTRANSFERENCIA.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum, "TRANSFERENCIA", "{0:n2}")});
            this.colTRANSFERENCIA.Visible = true;
            this.colTRANSFERENCIA.VisibleIndex = 4;
            this.colTRANSFERENCIA.Width = 88;
            // 
            // colRENTABILIDADE
            // 
            this.colRENTABILIDADE.Caption = "Rentabilidade";
            this.colRENTABILIDADE.ColumnEdit = this.CalcEditn2;
            this.colRENTABILIDADE.FieldName = "RENTABILIDADE";
            this.colRENTABILIDADE.Name = "colRENTABILIDADE";
            this.colRENTABILIDADE.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum, "RENTABILIDADE", "{0:n2}")});
            this.colRENTABILIDADE.Visible = true;
            this.colRENTABILIDADE.VisibleIndex = 5;
            this.colRENTABILIDADE.Width = 88;
            // 
            // xtraTabPageCCT
            // 
            this.xtraTabPageCCT.Controls.Add(this.gridControl7);
            this.xtraTabPageCCT.Name = "xtraTabPageCCT";
            this.xtraTabPageCCT.Size = new System.Drawing.Size(1459, 232);
            this.xtraTabPageCCT.Text = "Contas Físicas";
            // 
            // gridControl7
            // 
            this.gridControl7.DataMember = "ContasFisicas";
            this.gridControl7.DataSource = this.dCreditosBindingSource;
            this.gridControl7.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridControl7.Location = new System.Drawing.Point(0, 0);
            this.gridControl7.MainView = this.gridView7;
            this.gridControl7.Name = "gridControl7";
            this.gridControl7.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemCalcEdit2});
            this.gridControl7.Size = new System.Drawing.Size(1459, 232);
            this.gridControl7.TabIndex = 0;
            this.gridControl7.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridView7});
            // 
            // gridView7
            // 
            this.gridView7.Appearance.ColumnFilterButton.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(221)))), ((int)(((byte)(236)))), ((int)(((byte)(254)))));
            this.gridView7.Appearance.ColumnFilterButton.BackColor2 = System.Drawing.Color.FromArgb(((int)(((byte)(132)))), ((int)(((byte)(171)))), ((int)(((byte)(228)))));
            this.gridView7.Appearance.ColumnFilterButton.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(221)))), ((int)(((byte)(236)))), ((int)(((byte)(254)))));
            this.gridView7.Appearance.ColumnFilterButton.ForeColor = System.Drawing.Color.Black;
            this.gridView7.Appearance.ColumnFilterButton.GradientMode = System.Drawing.Drawing2D.LinearGradientMode.Vertical;
            this.gridView7.Appearance.ColumnFilterButton.Options.UseBackColor = true;
            this.gridView7.Appearance.ColumnFilterButton.Options.UseBorderColor = true;
            this.gridView7.Appearance.ColumnFilterButton.Options.UseForeColor = true;
            this.gridView7.Appearance.ColumnFilterButtonActive.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(247)))), ((int)(((byte)(251)))), ((int)(((byte)(255)))));
            this.gridView7.Appearance.ColumnFilterButtonActive.BackColor2 = System.Drawing.Color.FromArgb(((int)(((byte)(154)))), ((int)(((byte)(190)))), ((int)(((byte)(243)))));
            this.gridView7.Appearance.ColumnFilterButtonActive.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(247)))), ((int)(((byte)(251)))), ((int)(((byte)(255)))));
            this.gridView7.Appearance.ColumnFilterButtonActive.ForeColor = System.Drawing.Color.Black;
            this.gridView7.Appearance.ColumnFilterButtonActive.GradientMode = System.Drawing.Drawing2D.LinearGradientMode.Vertical;
            this.gridView7.Appearance.ColumnFilterButtonActive.Options.UseBackColor = true;
            this.gridView7.Appearance.ColumnFilterButtonActive.Options.UseBorderColor = true;
            this.gridView7.Appearance.ColumnFilterButtonActive.Options.UseForeColor = true;
            this.gridView7.Appearance.Empty.BackColor = System.Drawing.Color.White;
            this.gridView7.Appearance.Empty.Options.UseBackColor = true;
            this.gridView7.Appearance.EvenRow.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(231)))), ((int)(((byte)(242)))), ((int)(((byte)(254)))));
            this.gridView7.Appearance.EvenRow.ForeColor = System.Drawing.Color.Black;
            this.gridView7.Appearance.EvenRow.Options.UseBackColor = true;
            this.gridView7.Appearance.EvenRow.Options.UseForeColor = true;
            this.gridView7.Appearance.FilterCloseButton.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(221)))), ((int)(((byte)(236)))), ((int)(((byte)(254)))));
            this.gridView7.Appearance.FilterCloseButton.BackColor2 = System.Drawing.Color.FromArgb(((int)(((byte)(132)))), ((int)(((byte)(171)))), ((int)(((byte)(228)))));
            this.gridView7.Appearance.FilterCloseButton.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(221)))), ((int)(((byte)(236)))), ((int)(((byte)(254)))));
            this.gridView7.Appearance.FilterCloseButton.ForeColor = System.Drawing.Color.Black;
            this.gridView7.Appearance.FilterCloseButton.GradientMode = System.Drawing.Drawing2D.LinearGradientMode.Vertical;
            this.gridView7.Appearance.FilterCloseButton.Options.UseBackColor = true;
            this.gridView7.Appearance.FilterCloseButton.Options.UseBorderColor = true;
            this.gridView7.Appearance.FilterCloseButton.Options.UseForeColor = true;
            this.gridView7.Appearance.FilterPanel.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(62)))), ((int)(((byte)(109)))), ((int)(((byte)(185)))));
            this.gridView7.Appearance.FilterPanel.ForeColor = System.Drawing.Color.White;
            this.gridView7.Appearance.FilterPanel.Options.UseBackColor = true;
            this.gridView7.Appearance.FilterPanel.Options.UseForeColor = true;
            this.gridView7.Appearance.FixedLine.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(59)))), ((int)(((byte)(97)))), ((int)(((byte)(156)))));
            this.gridView7.Appearance.FixedLine.Options.UseBackColor = true;
            this.gridView7.Appearance.FocusedCell.BackColor = System.Drawing.Color.White;
            this.gridView7.Appearance.FocusedCell.ForeColor = System.Drawing.Color.Black;
            this.gridView7.Appearance.FocusedCell.Options.UseBackColor = true;
            this.gridView7.Appearance.FocusedCell.Options.UseForeColor = true;
            this.gridView7.Appearance.FocusedRow.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(49)))), ((int)(((byte)(106)))), ((int)(((byte)(197)))));
            this.gridView7.Appearance.FocusedRow.ForeColor = System.Drawing.Color.White;
            this.gridView7.Appearance.FocusedRow.Options.UseBackColor = true;
            this.gridView7.Appearance.FocusedRow.Options.UseForeColor = true;
            this.gridView7.Appearance.FooterPanel.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(221)))), ((int)(((byte)(236)))), ((int)(((byte)(254)))));
            this.gridView7.Appearance.FooterPanel.BackColor2 = System.Drawing.Color.FromArgb(((int)(((byte)(132)))), ((int)(((byte)(171)))), ((int)(((byte)(228)))));
            this.gridView7.Appearance.FooterPanel.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(221)))), ((int)(((byte)(236)))), ((int)(((byte)(254)))));
            this.gridView7.Appearance.FooterPanel.ForeColor = System.Drawing.Color.Black;
            this.gridView7.Appearance.FooterPanel.GradientMode = System.Drawing.Drawing2D.LinearGradientMode.Vertical;
            this.gridView7.Appearance.FooterPanel.Options.UseBackColor = true;
            this.gridView7.Appearance.FooterPanel.Options.UseBorderColor = true;
            this.gridView7.Appearance.FooterPanel.Options.UseForeColor = true;
            this.gridView7.Appearance.GroupButton.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(193)))), ((int)(((byte)(216)))), ((int)(((byte)(247)))));
            this.gridView7.Appearance.GroupButton.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(193)))), ((int)(((byte)(216)))), ((int)(((byte)(247)))));
            this.gridView7.Appearance.GroupButton.ForeColor = System.Drawing.Color.Black;
            this.gridView7.Appearance.GroupButton.Options.UseBackColor = true;
            this.gridView7.Appearance.GroupButton.Options.UseBorderColor = true;
            this.gridView7.Appearance.GroupButton.Options.UseForeColor = true;
            this.gridView7.Appearance.GroupFooter.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(193)))), ((int)(((byte)(216)))), ((int)(((byte)(247)))));
            this.gridView7.Appearance.GroupFooter.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(193)))), ((int)(((byte)(216)))), ((int)(((byte)(247)))));
            this.gridView7.Appearance.GroupFooter.ForeColor = System.Drawing.Color.Black;
            this.gridView7.Appearance.GroupFooter.Options.UseBackColor = true;
            this.gridView7.Appearance.GroupFooter.Options.UseBorderColor = true;
            this.gridView7.Appearance.GroupFooter.Options.UseForeColor = true;
            this.gridView7.Appearance.GroupPanel.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(62)))), ((int)(((byte)(109)))), ((int)(((byte)(185)))));
            this.gridView7.Appearance.GroupPanel.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(221)))), ((int)(((byte)(236)))), ((int)(((byte)(254)))));
            this.gridView7.Appearance.GroupPanel.Options.UseBackColor = true;
            this.gridView7.Appearance.GroupPanel.Options.UseForeColor = true;
            this.gridView7.Appearance.GroupRow.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(193)))), ((int)(((byte)(216)))), ((int)(((byte)(247)))));
            this.gridView7.Appearance.GroupRow.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(193)))), ((int)(((byte)(216)))), ((int)(((byte)(247)))));
            this.gridView7.Appearance.GroupRow.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Bold);
            this.gridView7.Appearance.GroupRow.ForeColor = System.Drawing.Color.Black;
            this.gridView7.Appearance.GroupRow.Options.UseBackColor = true;
            this.gridView7.Appearance.GroupRow.Options.UseBorderColor = true;
            this.gridView7.Appearance.GroupRow.Options.UseFont = true;
            this.gridView7.Appearance.GroupRow.Options.UseForeColor = true;
            this.gridView7.Appearance.HeaderPanel.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(221)))), ((int)(((byte)(236)))), ((int)(((byte)(254)))));
            this.gridView7.Appearance.HeaderPanel.BackColor2 = System.Drawing.Color.FromArgb(((int)(((byte)(132)))), ((int)(((byte)(171)))), ((int)(((byte)(228)))));
            this.gridView7.Appearance.HeaderPanel.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(221)))), ((int)(((byte)(236)))), ((int)(((byte)(254)))));
            this.gridView7.Appearance.HeaderPanel.ForeColor = System.Drawing.Color.Black;
            this.gridView7.Appearance.HeaderPanel.GradientMode = System.Drawing.Drawing2D.LinearGradientMode.Vertical;
            this.gridView7.Appearance.HeaderPanel.Options.UseBackColor = true;
            this.gridView7.Appearance.HeaderPanel.Options.UseBorderColor = true;
            this.gridView7.Appearance.HeaderPanel.Options.UseForeColor = true;
            this.gridView7.Appearance.HideSelectionRow.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(106)))), ((int)(((byte)(153)))), ((int)(((byte)(228)))));
            this.gridView7.Appearance.HideSelectionRow.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(208)))), ((int)(((byte)(224)))), ((int)(((byte)(251)))));
            this.gridView7.Appearance.HideSelectionRow.Options.UseBackColor = true;
            this.gridView7.Appearance.HideSelectionRow.Options.UseForeColor = true;
            this.gridView7.Appearance.HorzLine.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(99)))), ((int)(((byte)(127)))), ((int)(((byte)(196)))));
            this.gridView7.Appearance.HorzLine.Options.UseBackColor = true;
            this.gridView7.Appearance.OddRow.BackColor = System.Drawing.Color.White;
            this.gridView7.Appearance.OddRow.ForeColor = System.Drawing.Color.Black;
            this.gridView7.Appearance.OddRow.Options.UseBackColor = true;
            this.gridView7.Appearance.OddRow.Options.UseForeColor = true;
            this.gridView7.Appearance.Preview.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(249)))), ((int)(((byte)(252)))), ((int)(((byte)(255)))));
            this.gridView7.Appearance.Preview.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(88)))), ((int)(((byte)(129)))), ((int)(((byte)(185)))));
            this.gridView7.Appearance.Preview.Options.UseBackColor = true;
            this.gridView7.Appearance.Preview.Options.UseForeColor = true;
            this.gridView7.Appearance.Row.BackColor = System.Drawing.Color.White;
            this.gridView7.Appearance.Row.ForeColor = System.Drawing.Color.Black;
            this.gridView7.Appearance.Row.Options.UseBackColor = true;
            this.gridView7.Appearance.Row.Options.UseForeColor = true;
            this.gridView7.Appearance.RowSeparator.BackColor = System.Drawing.Color.White;
            this.gridView7.Appearance.RowSeparator.Options.UseBackColor = true;
            this.gridView7.Appearance.SelectedRow.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(69)))), ((int)(((byte)(126)))), ((int)(((byte)(217)))));
            this.gridView7.Appearance.SelectedRow.ForeColor = System.Drawing.Color.White;
            this.gridView7.Appearance.SelectedRow.Options.UseBackColor = true;
            this.gridView7.Appearance.SelectedRow.Options.UseForeColor = true;
            this.gridView7.Appearance.VertLine.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(99)))), ((int)(((byte)(127)))), ((int)(((byte)(196)))));
            this.gridView7.Appearance.VertLine.Options.UseBackColor = true;
            this.gridView7.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colDescricao5,
            this.colSaldoI1,
            this.colSaldoF1});
            this.gridView7.GridControl = this.gridControl7;
            this.gridView7.Name = "gridView7";
            this.gridView7.OptionsView.ColumnAutoWidth = false;
            this.gridView7.OptionsView.EnableAppearanceEvenRow = true;
            this.gridView7.OptionsView.EnableAppearanceOddRow = true;
            this.gridView7.OptionsView.ShowFooter = true;
            this.gridView7.OptionsView.ShowGroupPanel = false;
            // 
            // colDescricao5
            // 
            this.colDescricao5.Caption = "Título";
            this.colDescricao5.FieldName = "Descricao";
            this.colDescricao5.Name = "colDescricao5";
            this.colDescricao5.OptionsColumn.ReadOnly = true;
            this.colDescricao5.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Count)});
            this.colDescricao5.Visible = true;
            this.colDescricao5.VisibleIndex = 0;
            this.colDescricao5.Width = 355;
            // 
            // colSaldoI1
            // 
            this.colSaldoI1.Caption = "Saldo inicial";
            this.colSaldoI1.ColumnEdit = this.repositoryItemCalcEdit2;
            this.colSaldoI1.FieldName = "SaldoI";
            this.colSaldoI1.Name = "colSaldoI1";
            this.colSaldoI1.OptionsColumn.ReadOnly = true;
            this.colSaldoI1.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum, "SaldoI", "{0:n2}")});
            this.colSaldoI1.Visible = true;
            this.colSaldoI1.VisibleIndex = 1;
            this.colSaldoI1.Width = 91;
            // 
            // repositoryItemCalcEdit2
            // 
            this.repositoryItemCalcEdit2.AutoHeight = false;
            this.repositoryItemCalcEdit2.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemCalcEdit2.DisplayFormat.FormatString = "n2";
            this.repositoryItemCalcEdit2.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.repositoryItemCalcEdit2.Name = "repositoryItemCalcEdit2";
            // 
            // colSaldoF1
            // 
            this.colSaldoF1.Caption = "Saldo final";
            this.colSaldoF1.ColumnEdit = this.repositoryItemCalcEdit2;
            this.colSaldoF1.FieldName = "SaldoF";
            this.colSaldoF1.Name = "colSaldoF1";
            this.colSaldoF1.OptionsColumn.ReadOnly = true;
            this.colSaldoF1.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum, "SaldoF", "{0:n2}")});
            this.colSaldoF1.Visible = true;
            this.colSaldoF1.VisibleIndex = 2;
            this.colSaldoF1.Width = 94;
            // 
            // xtraTabPagePrevisto
            // 
            this.xtraTabPagePrevisto.Controls.Add(this.gridControl8);
            this.xtraTabPagePrevisto.Name = "xtraTabPagePrevisto";
            this.xtraTabPagePrevisto.Size = new System.Drawing.Size(1459, 232);
            this.xtraTabPagePrevisto.Text = "Previsto";
            // 
            // gridControl8
            // 
            this.gridControl8.DataSource = this.previstoBindingSource;
            this.gridControl8.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridControl8.Location = new System.Drawing.Point(0, 0);
            this.gridControl8.MainView = this.gridView8;
            this.gridControl8.Name = "gridControl8";
            this.gridControl8.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemButtonEdit3,
            this.repositoryItemCalcEdit3});
            this.gridControl8.Size = new System.Drawing.Size(1459, 232);
            this.gridControl8.TabIndex = 1;
            this.gridControl8.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridView8});
            // 
            // previstoBindingSource
            // 
            this.previstoBindingSource.DataMember = "Previsto";
            this.previstoBindingSource.DataSource = typeof(Balancete.Relatorios.Previsto.dPrevisto);
            // 
            // gridView8
            // 
            this.gridView8.Appearance.ColumnFilterButton.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(244)))), ((int)(((byte)(247)))), ((int)(((byte)(222)))));
            this.gridView8.Appearance.ColumnFilterButton.BackColor2 = System.Drawing.Color.FromArgb(((int)(((byte)(184)))), ((int)(((byte)(199)))), ((int)(((byte)(146)))));
            this.gridView8.Appearance.ColumnFilterButton.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(244)))), ((int)(((byte)(247)))), ((int)(((byte)(222)))));
            this.gridView8.Appearance.ColumnFilterButton.ForeColor = System.Drawing.Color.Black;
            this.gridView8.Appearance.ColumnFilterButton.GradientMode = System.Drawing.Drawing2D.LinearGradientMode.Vertical;
            this.gridView8.Appearance.ColumnFilterButton.Options.UseBackColor = true;
            this.gridView8.Appearance.ColumnFilterButton.Options.UseBorderColor = true;
            this.gridView8.Appearance.ColumnFilterButton.Options.UseForeColor = true;
            this.gridView8.Appearance.ColumnFilterButtonActive.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(254)))), ((int)(((byte)(255)))), ((int)(((byte)(244)))));
            this.gridView8.Appearance.ColumnFilterButtonActive.BackColor2 = System.Drawing.Color.FromArgb(((int)(((byte)(213)))), ((int)(((byte)(222)))), ((int)(((byte)(183)))));
            this.gridView8.Appearance.ColumnFilterButtonActive.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(254)))), ((int)(((byte)(255)))), ((int)(((byte)(244)))));
            this.gridView8.Appearance.ColumnFilterButtonActive.ForeColor = System.Drawing.Color.Black;
            this.gridView8.Appearance.ColumnFilterButtonActive.GradientMode = System.Drawing.Drawing2D.LinearGradientMode.Vertical;
            this.gridView8.Appearance.ColumnFilterButtonActive.Options.UseBackColor = true;
            this.gridView8.Appearance.ColumnFilterButtonActive.Options.UseBorderColor = true;
            this.gridView8.Appearance.ColumnFilterButtonActive.Options.UseForeColor = true;
            this.gridView8.Appearance.Empty.BackColor = System.Drawing.Color.White;
            this.gridView8.Appearance.Empty.Options.UseBackColor = true;
            this.gridView8.Appearance.EvenRow.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(244)))), ((int)(((byte)(247)))), ((int)(((byte)(222)))));
            this.gridView8.Appearance.EvenRow.ForeColor = System.Drawing.Color.Black;
            this.gridView8.Appearance.EvenRow.Options.UseBackColor = true;
            this.gridView8.Appearance.EvenRow.Options.UseForeColor = true;
            this.gridView8.Appearance.FilterCloseButton.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(244)))), ((int)(((byte)(247)))), ((int)(((byte)(222)))));
            this.gridView8.Appearance.FilterCloseButton.BackColor2 = System.Drawing.Color.FromArgb(((int)(((byte)(184)))), ((int)(((byte)(199)))), ((int)(((byte)(146)))));
            this.gridView8.Appearance.FilterCloseButton.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(244)))), ((int)(((byte)(247)))), ((int)(((byte)(222)))));
            this.gridView8.Appearance.FilterCloseButton.ForeColor = System.Drawing.Color.Black;
            this.gridView8.Appearance.FilterCloseButton.GradientMode = System.Drawing.Drawing2D.LinearGradientMode.Vertical;
            this.gridView8.Appearance.FilterCloseButton.Options.UseBackColor = true;
            this.gridView8.Appearance.FilterCloseButton.Options.UseBorderColor = true;
            this.gridView8.Appearance.FilterCloseButton.Options.UseForeColor = true;
            this.gridView8.Appearance.FilterPanel.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(139)))), ((int)(((byte)(158)))), ((int)(((byte)(126)))));
            this.gridView8.Appearance.FilterPanel.ForeColor = System.Drawing.Color.White;
            this.gridView8.Appearance.FilterPanel.Options.UseBackColor = true;
            this.gridView8.Appearance.FilterPanel.Options.UseForeColor = true;
            this.gridView8.Appearance.FixedLine.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(96)))), ((int)(((byte)(128)))), ((int)(((byte)(88)))));
            this.gridView8.Appearance.FixedLine.Options.UseBackColor = true;
            this.gridView8.Appearance.FocusedCell.BackColor = System.Drawing.Color.White;
            this.gridView8.Appearance.FocusedCell.ForeColor = System.Drawing.Color.Black;
            this.gridView8.Appearance.FocusedCell.Options.UseBackColor = true;
            this.gridView8.Appearance.FocusedCell.Options.UseForeColor = true;
            this.gridView8.Appearance.FocusedRow.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(147)))), ((int)(((byte)(160)))), ((int)(((byte)(112)))));
            this.gridView8.Appearance.FocusedRow.ForeColor = System.Drawing.Color.White;
            this.gridView8.Appearance.FocusedRow.Options.UseBackColor = true;
            this.gridView8.Appearance.FocusedRow.Options.UseForeColor = true;
            this.gridView8.Appearance.FooterPanel.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(244)))), ((int)(((byte)(247)))), ((int)(((byte)(222)))));
            this.gridView8.Appearance.FooterPanel.BackColor2 = System.Drawing.Color.FromArgb(((int)(((byte)(184)))), ((int)(((byte)(199)))), ((int)(((byte)(146)))));
            this.gridView8.Appearance.FooterPanel.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(244)))), ((int)(((byte)(247)))), ((int)(((byte)(222)))));
            this.gridView8.Appearance.FooterPanel.ForeColor = System.Drawing.Color.Black;
            this.gridView8.Appearance.FooterPanel.GradientMode = System.Drawing.Drawing2D.LinearGradientMode.Vertical;
            this.gridView8.Appearance.FooterPanel.Options.UseBackColor = true;
            this.gridView8.Appearance.FooterPanel.Options.UseBorderColor = true;
            this.gridView8.Appearance.FooterPanel.Options.UseForeColor = true;
            this.gridView8.Appearance.GroupButton.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(223)))), ((int)(((byte)(230)))), ((int)(((byte)(195)))));
            this.gridView8.Appearance.GroupButton.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(223)))), ((int)(((byte)(230)))), ((int)(((byte)(195)))));
            this.gridView8.Appearance.GroupButton.ForeColor = System.Drawing.Color.Black;
            this.gridView8.Appearance.GroupButton.Options.UseBackColor = true;
            this.gridView8.Appearance.GroupButton.Options.UseBorderColor = true;
            this.gridView8.Appearance.GroupButton.Options.UseForeColor = true;
            this.gridView8.Appearance.GroupFooter.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(223)))), ((int)(((byte)(230)))), ((int)(((byte)(195)))));
            this.gridView8.Appearance.GroupFooter.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(223)))), ((int)(((byte)(230)))), ((int)(((byte)(195)))));
            this.gridView8.Appearance.GroupFooter.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Bold);
            this.gridView8.Appearance.GroupFooter.ForeColor = System.Drawing.Color.Black;
            this.gridView8.Appearance.GroupFooter.Options.UseBackColor = true;
            this.gridView8.Appearance.GroupFooter.Options.UseBorderColor = true;
            this.gridView8.Appearance.GroupFooter.Options.UseFont = true;
            this.gridView8.Appearance.GroupFooter.Options.UseForeColor = true;
            this.gridView8.Appearance.GroupPanel.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(139)))), ((int)(((byte)(158)))), ((int)(((byte)(126)))));
            this.gridView8.Appearance.GroupPanel.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(244)))), ((int)(((byte)(247)))), ((int)(((byte)(222)))));
            this.gridView8.Appearance.GroupPanel.Options.UseBackColor = true;
            this.gridView8.Appearance.GroupPanel.Options.UseForeColor = true;
            this.gridView8.Appearance.GroupRow.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(223)))), ((int)(((byte)(230)))), ((int)(((byte)(195)))));
            this.gridView8.Appearance.GroupRow.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(223)))), ((int)(((byte)(230)))), ((int)(((byte)(195)))));
            this.gridView8.Appearance.GroupRow.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Bold);
            this.gridView8.Appearance.GroupRow.ForeColor = System.Drawing.Color.Black;
            this.gridView8.Appearance.GroupRow.Options.UseBackColor = true;
            this.gridView8.Appearance.GroupRow.Options.UseBorderColor = true;
            this.gridView8.Appearance.GroupRow.Options.UseFont = true;
            this.gridView8.Appearance.GroupRow.Options.UseForeColor = true;
            this.gridView8.Appearance.HeaderPanel.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(244)))), ((int)(((byte)(247)))), ((int)(((byte)(222)))));
            this.gridView8.Appearance.HeaderPanel.BackColor2 = System.Drawing.Color.FromArgb(((int)(((byte)(184)))), ((int)(((byte)(199)))), ((int)(((byte)(146)))));
            this.gridView8.Appearance.HeaderPanel.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(244)))), ((int)(((byte)(247)))), ((int)(((byte)(222)))));
            this.gridView8.Appearance.HeaderPanel.ForeColor = System.Drawing.Color.Black;
            this.gridView8.Appearance.HeaderPanel.GradientMode = System.Drawing.Drawing2D.LinearGradientMode.Vertical;
            this.gridView8.Appearance.HeaderPanel.Options.UseBackColor = true;
            this.gridView8.Appearance.HeaderPanel.Options.UseBorderColor = true;
            this.gridView8.Appearance.HeaderPanel.Options.UseForeColor = true;
            this.gridView8.Appearance.HideSelectionRow.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(197)))), ((int)(((byte)(207)))), ((int)(((byte)(170)))));
            this.gridView8.Appearance.HideSelectionRow.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(147)))), ((int)(((byte)(160)))), ((int)(((byte)(112)))));
            this.gridView8.Appearance.HideSelectionRow.Options.UseBackColor = true;
            this.gridView8.Appearance.HideSelectionRow.Options.UseForeColor = true;
            this.gridView8.Appearance.HorzLine.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(136)))), ((int)(((byte)(168)))), ((int)(((byte)(128)))));
            this.gridView8.Appearance.HorzLine.Options.UseBackColor = true;
            this.gridView8.Appearance.Preview.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(252)))), ((int)(((byte)(253)))), ((int)(((byte)(247)))));
            this.gridView8.Appearance.Preview.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(147)))), ((int)(((byte)(160)))), ((int)(((byte)(112)))));
            this.gridView8.Appearance.Preview.Options.UseBackColor = true;
            this.gridView8.Appearance.Preview.Options.UseForeColor = true;
            this.gridView8.Appearance.Row.BackColor = System.Drawing.Color.White;
            this.gridView8.Appearance.Row.ForeColor = System.Drawing.Color.Black;
            this.gridView8.Appearance.Row.Options.UseBackColor = true;
            this.gridView8.Appearance.Row.Options.UseForeColor = true;
            this.gridView8.Appearance.RowSeparator.BackColor = System.Drawing.Color.White;
            this.gridView8.Appearance.RowSeparator.Options.UseBackColor = true;
            this.gridView8.Appearance.SelectedRow.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(166)))), ((int)(((byte)(178)))), ((int)(((byte)(133)))));
            this.gridView8.Appearance.SelectedRow.ForeColor = System.Drawing.Color.White;
            this.gridView8.Appearance.SelectedRow.Options.UseBackColor = true;
            this.gridView8.Appearance.SelectedRow.Options.UseForeColor = true;
            this.gridView8.Appearance.TopNewRow.BackColor = System.Drawing.Color.White;
            this.gridView8.Appearance.TopNewRow.Options.UseBackColor = true;
            this.gridView8.Appearance.VertLine.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(136)))), ((int)(((byte)(160)))), ((int)(((byte)(188)))));
            this.gridView8.Appearance.VertLine.Options.UseBackColor = true;
            this.gridView8.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colBOL1,
            this.colBOLTipoCRAI1,
            this.colBOLVencto1,
            this.colBOLPagamento1,
            this.colBOLValorPrevisto,
            this.colBOLValorPago1,
            this.colBOLCompetenciaAno1,
            this.colBOLCompetenciaMes1,
            this.colBOL_CCD1,
            this.colBOL_ARL,
            this.colAPTNumero1,
            this.colBLOCodigo1,
            this.colBLOAPT,
            this.colPI,
            this.colBOLProprietario1,
            this.colValorPrincipal,
            this.colMultas,
            this.colStatus,
            this.colAntecipado1,
            this.gridColumn2});
            this.gridView8.GridControl = this.gridControl8;
            this.gridView8.Name = "gridView8";
            this.gridView8.OptionsView.ColumnAutoWidth = false;
            this.gridView8.OptionsView.EnableAppearanceEvenRow = true;
            this.gridView8.OptionsView.ShowAutoFilterRow = true;
            this.gridView8.OptionsView.ShowFooter = true;
            this.gridView8.OptionsView.ShowGroupPanel = false;
            // 
            // colBOL1
            // 
            this.colBOL1.Caption = "Boleto";
            this.colBOL1.FieldName = "BOL";
            this.colBOL1.Name = "colBOL1";
            this.colBOL1.OptionsColumn.ReadOnly = true;
            this.colBOL1.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Count, "BOL", "{0:n0}")});
            this.colBOL1.Visible = true;
            this.colBOL1.VisibleIndex = 0;
            // 
            // colBOLTipoCRAI1
            // 
            this.colBOLTipoCRAI1.Caption = "Tipo";
            this.colBOLTipoCRAI1.FieldName = "BOLTipoCRAI";
            this.colBOLTipoCRAI1.Name = "colBOLTipoCRAI1";
            this.colBOLTipoCRAI1.OptionsColumn.ReadOnly = true;
            this.colBOLTipoCRAI1.Visible = true;
            this.colBOLTipoCRAI1.VisibleIndex = 3;
            this.colBOLTipoCRAI1.Width = 33;
            // 
            // colBOLVencto1
            // 
            this.colBOLVencto1.Caption = "Vencimento";
            this.colBOLVencto1.FieldName = "BOLVencto";
            this.colBOLVencto1.Name = "colBOLVencto1";
            this.colBOLVencto1.OptionsColumn.ReadOnly = true;
            this.colBOLVencto1.Visible = true;
            this.colBOLVencto1.VisibleIndex = 4;
            // 
            // colBOLPagamento1
            // 
            this.colBOLPagamento1.FieldName = "BOLPagamento";
            this.colBOLPagamento1.Name = "colBOLPagamento1";
            this.colBOLPagamento1.OptionsColumn.ReadOnly = true;
            this.colBOLPagamento1.Visible = true;
            this.colBOLPagamento1.VisibleIndex = 5;
            // 
            // colBOLValorPrevisto
            // 
            this.colBOLValorPrevisto.Caption = "Valor Previsto";
            this.colBOLValorPrevisto.ColumnEdit = this.repositoryItemCalcEdit3;
            this.colBOLValorPrevisto.FieldName = "BOLValorPrevisto";
            this.colBOLValorPrevisto.Name = "colBOLValorPrevisto";
            this.colBOLValorPrevisto.OptionsColumn.ReadOnly = true;
            this.colBOLValorPrevisto.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum, "BOLValorPrevisto", "{0:n2}")});
            this.colBOLValorPrevisto.Visible = true;
            this.colBOLValorPrevisto.VisibleIndex = 6;
            // 
            // repositoryItemCalcEdit3
            // 
            this.repositoryItemCalcEdit3.AutoHeight = false;
            this.repositoryItemCalcEdit3.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemCalcEdit3.DisplayFormat.FormatString = "n2";
            this.repositoryItemCalcEdit3.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.repositoryItemCalcEdit3.Name = "repositoryItemCalcEdit3";
            // 
            // colBOLValorPago1
            // 
            this.colBOLValorPago1.Caption = "Valor Pago";
            this.colBOLValorPago1.ColumnEdit = this.repositoryItemCalcEdit3;
            this.colBOLValorPago1.FieldName = "BOLValorPago";
            this.colBOLValorPago1.Name = "colBOLValorPago1";
            this.colBOLValorPago1.OptionsColumn.ReadOnly = true;
            this.colBOLValorPago1.Visible = true;
            this.colBOLValorPago1.VisibleIndex = 7;
            // 
            // colBOLCompetenciaAno1
            // 
            this.colBOLCompetenciaAno1.FieldName = "BOLCompetenciaAno";
            this.colBOLCompetenciaAno1.Name = "colBOLCompetenciaAno1";
            this.colBOLCompetenciaAno1.OptionsColumn.ReadOnly = true;
            // 
            // colBOLCompetenciaMes1
            // 
            this.colBOLCompetenciaMes1.FieldName = "BOLCompetenciaMes";
            this.colBOLCompetenciaMes1.Name = "colBOLCompetenciaMes1";
            this.colBOLCompetenciaMes1.OptionsColumn.ReadOnly = true;
            // 
            // colBOL_CCD1
            // 
            this.colBOL_CCD1.Caption = "CCD";
            this.colBOL_CCD1.FieldName = "BOL_CCD";
            this.colBOL_CCD1.Name = "colBOL_CCD1";
            this.colBOL_CCD1.OptionsColumn.ReadOnly = true;
            // 
            // colBOL_ARL
            // 
            this.colBOL_ARL.Caption = "ARL";
            this.colBOL_ARL.FieldName = "BOL_ARL";
            this.colBOL_ARL.Name = "colBOL_ARL";
            this.colBOL_ARL.OptionsColumn.ReadOnly = true;
            // 
            // colAPTNumero1
            // 
            this.colAPTNumero1.Caption = "Apartamento";
            this.colAPTNumero1.FieldName = "APTNumero";
            this.colAPTNumero1.Name = "colAPTNumero1";
            this.colAPTNumero1.OptionsColumn.ReadOnly = true;
            this.colAPTNumero1.Visible = true;
            this.colAPTNumero1.VisibleIndex = 2;
            this.colAPTNumero1.Width = 79;
            // 
            // colBLOCodigo1
            // 
            this.colBLOCodigo1.Caption = "Bloco";
            this.colBLOCodigo1.FieldName = "BLOCodigo";
            this.colBLOCodigo1.Name = "colBLOCodigo1";
            this.colBLOCodigo1.OptionsColumn.ReadOnly = true;
            this.colBLOCodigo1.Visible = true;
            this.colBLOCodigo1.VisibleIndex = 1;
            this.colBLOCodigo1.Width = 43;
            // 
            // colBLOAPT
            // 
            this.colBLOAPT.Caption = "APT";
            this.colBLOAPT.FieldName = "BLOAPT";
            this.colBLOAPT.Name = "colBLOAPT";
            this.colBLOAPT.OptionsColumn.ReadOnly = true;
            // 
            // colPI
            // 
            this.colPI.Caption = "P/I";
            this.colPI.FieldName = "PI";
            this.colPI.Name = "colPI";
            this.colPI.OptionsColumn.ReadOnly = true;
            this.colPI.Visible = true;
            this.colPI.VisibleIndex = 8;
            this.colPI.Width = 34;
            // 
            // colBOLProprietario1
            // 
            this.colBOLProprietario1.FieldName = "BOLProprietario";
            this.colBOLProprietario1.Name = "colBOLProprietario1";
            this.colBOLProprietario1.OptionsColumn.ReadOnly = true;
            // 
            // colValorPrincipal
            // 
            this.colValorPrincipal.Caption = "Valor Principal";
            this.colValorPrincipal.ColumnEdit = this.repositoryItemCalcEdit3;
            this.colValorPrincipal.FieldName = "ValorPrincipal";
            this.colValorPrincipal.Name = "colValorPrincipal";
            this.colValorPrincipal.OptionsColumn.ReadOnly = true;
            this.colValorPrincipal.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum, "ValorPrincipal", "{0:n2}")});
            this.colValorPrincipal.Visible = true;
            this.colValorPrincipal.VisibleIndex = 9;
            // 
            // colMultas
            // 
            this.colMultas.Caption = "Multas";
            this.colMultas.ColumnEdit = this.repositoryItemCalcEdit3;
            this.colMultas.FieldName = "Multas";
            this.colMultas.Name = "colMultas";
            this.colMultas.OptionsColumn.ReadOnly = true;
            this.colMultas.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum, "Multas", "{0:n2}")});
            this.colMultas.Visible = true;
            this.colMultas.VisibleIndex = 10;
            this.colMultas.Width = 50;
            // 
            // colStatus
            // 
            this.colStatus.FieldName = "Status";
            this.colStatus.Name = "colStatus";
            this.colStatus.OptionsColumn.ReadOnly = true;
            this.colStatus.Visible = true;
            this.colStatus.VisibleIndex = 11;
            this.colStatus.Width = 121;
            // 
            // colAntecipado1
            // 
            this.colAntecipado1.ColumnEdit = this.repositoryItemCalcEdit3;
            this.colAntecipado1.FieldName = "Antecipado";
            this.colAntecipado1.Name = "colAntecipado1";
            this.colAntecipado1.OptionsColumn.ReadOnly = true;
            this.colAntecipado1.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum, "Antecipado", "{0:n2}")});
            this.colAntecipado1.Visible = true;
            this.colAntecipado1.VisibleIndex = 12;
            // 
            // gridColumn2
            // 
            this.gridColumn2.Caption = "gridColumn2";
            this.gridColumn2.ColumnEdit = this.repositoryItemButtonEdit3;
            this.gridColumn2.Fixed = DevExpress.XtraGrid.Columns.FixedStyle.Right;
            this.gridColumn2.Name = "gridColumn2";
            this.gridColumn2.OptionsColumn.FixedWidth = true;
            this.gridColumn2.OptionsColumn.ShowCaption = false;
            this.gridColumn2.Visible = true;
            this.gridColumn2.VisibleIndex = 13;
            this.gridColumn2.Width = 28;
            // 
            // repositoryItemButtonEdit3
            // 
            this.repositoryItemButtonEdit3.AutoHeight = false;
            this.repositoryItemButtonEdit3.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.repositoryItemButtonEdit3.Name = "repositoryItemButtonEdit3";
            this.repositoryItemButtonEdit3.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.HideTextEditor;
            this.repositoryItemButtonEdit3.ButtonClick += new DevExpress.XtraEditors.Controls.ButtonPressedEventHandler(this.repositoryItemButtonEdit3_ButtonClick);
            // 
            // xtraTabPageInad
            // 
            this.xtraTabPageInad.Controls.Add(this.gridControl12);
            this.xtraTabPageInad.Name = "xtraTabPageInad";
            this.xtraTabPageInad.Size = new System.Drawing.Size(1459, 232);
            this.xtraTabPageInad.Text = "Inadimplência";
            // 
            // gridControl12
            // 
            this.gridControl12.DataSource = this.inadimpBindingSource;
            this.gridControl12.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridControl12.Location = new System.Drawing.Point(0, 0);
            this.gridControl12.LookAndFeel.Style = DevExpress.LookAndFeel.LookAndFeelStyle.Flat;
            this.gridControl12.LookAndFeel.UseDefaultLookAndFeel = false;
            this.gridControl12.MainView = this.gridViewInad;
            this.gridControl12.Name = "gridControl12";
            this.gridControl12.Size = new System.Drawing.Size(1459, 232);
            this.gridControl12.TabIndex = 0;
            this.gridControl12.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridViewInad});
            // 
            // inadimpBindingSource
            // 
            this.inadimpBindingSource.DataMember = "Inadimp";
            this.inadimpBindingSource.DataSource = typeof(Balancete.Relatorios.Inadimplencia.dInadimplencia);
            // 
            // gridViewInad
            // 
            this.gridViewInad.Appearance.ColumnFilterButton.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(240)))), ((int)(((byte)(177)))), ((int)(((byte)(94)))));
            this.gridViewInad.Appearance.ColumnFilterButton.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(240)))), ((int)(((byte)(177)))), ((int)(((byte)(94)))));
            this.gridViewInad.Appearance.ColumnFilterButton.ForeColor = System.Drawing.Color.Black;
            this.gridViewInad.Appearance.ColumnFilterButton.Options.UseBackColor = true;
            this.gridViewInad.Appearance.ColumnFilterButton.Options.UseBorderColor = true;
            this.gridViewInad.Appearance.ColumnFilterButton.Options.UseForeColor = true;
            this.gridViewInad.Appearance.ColumnFilterButtonActive.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(248)))), ((int)(((byte)(204)))), ((int)(((byte)(124)))));
            this.gridViewInad.Appearance.ColumnFilterButtonActive.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(248)))), ((int)(((byte)(204)))), ((int)(((byte)(124)))));
            this.gridViewInad.Appearance.ColumnFilterButtonActive.ForeColor = System.Drawing.Color.Black;
            this.gridViewInad.Appearance.ColumnFilterButtonActive.Options.UseBackColor = true;
            this.gridViewInad.Appearance.ColumnFilterButtonActive.Options.UseBorderColor = true;
            this.gridViewInad.Appearance.ColumnFilterButtonActive.Options.UseForeColor = true;
            this.gridViewInad.Appearance.Empty.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(251)))), ((int)(((byte)(244)))), ((int)(((byte)(232)))));
            this.gridViewInad.Appearance.Empty.BackColor2 = System.Drawing.Color.White;
            this.gridViewInad.Appearance.Empty.Options.UseBackColor = true;
            this.gridViewInad.Appearance.EvenRow.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(253)))), ((int)(((byte)(248)))), ((int)(((byte)(236)))));
            this.gridViewInad.Appearance.EvenRow.ForeColor = System.Drawing.Color.Black;
            this.gridViewInad.Appearance.EvenRow.Options.UseBackColor = true;
            this.gridViewInad.Appearance.EvenRow.Options.UseForeColor = true;
            this.gridViewInad.Appearance.FilterCloseButton.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(240)))), ((int)(((byte)(177)))), ((int)(((byte)(94)))));
            this.gridViewInad.Appearance.FilterCloseButton.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(240)))), ((int)(((byte)(177)))), ((int)(((byte)(94)))));
            this.gridViewInad.Appearance.FilterCloseButton.ForeColor = System.Drawing.Color.Black;
            this.gridViewInad.Appearance.FilterCloseButton.Options.UseBackColor = true;
            this.gridViewInad.Appearance.FilterCloseButton.Options.UseBorderColor = true;
            this.gridViewInad.Appearance.FilterCloseButton.Options.UseForeColor = true;
            this.gridViewInad.Appearance.FilterPanel.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(251)))), ((int)(((byte)(244)))), ((int)(((byte)(232)))));
            this.gridViewInad.Appearance.FilterPanel.BackColor2 = System.Drawing.Color.White;
            this.gridViewInad.Appearance.FilterPanel.ForeColor = System.Drawing.Color.Black;
            this.gridViewInad.Appearance.FilterPanel.Options.UseBackColor = true;
            this.gridViewInad.Appearance.FilterPanel.Options.UseForeColor = true;
            this.gridViewInad.Appearance.FixedLine.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(218)))), ((int)(((byte)(153)))), ((int)(((byte)(73)))));
            this.gridViewInad.Appearance.FixedLine.Options.UseBackColor = true;
            this.gridViewInad.Appearance.FocusedCell.BackColor = System.Drawing.Color.White;
            this.gridViewInad.Appearance.FocusedCell.ForeColor = System.Drawing.Color.Black;
            this.gridViewInad.Appearance.FocusedCell.Options.UseBackColor = true;
            this.gridViewInad.Appearance.FocusedCell.Options.UseForeColor = true;
            this.gridViewInad.Appearance.FocusedRow.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(217)))), ((int)(((byte)(154)))), ((int)(((byte)(91)))));
            this.gridViewInad.Appearance.FocusedRow.ForeColor = System.Drawing.Color.White;
            this.gridViewInad.Appearance.FocusedRow.Options.UseBackColor = true;
            this.gridViewInad.Appearance.FocusedRow.Options.UseForeColor = true;
            this.gridViewInad.Appearance.FooterPanel.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(240)))), ((int)(((byte)(177)))), ((int)(((byte)(94)))));
            this.gridViewInad.Appearance.FooterPanel.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(240)))), ((int)(((byte)(177)))), ((int)(((byte)(94)))));
            this.gridViewInad.Appearance.FooterPanel.ForeColor = System.Drawing.Color.Black;
            this.gridViewInad.Appearance.FooterPanel.Options.UseBackColor = true;
            this.gridViewInad.Appearance.FooterPanel.Options.UseBorderColor = true;
            this.gridViewInad.Appearance.FooterPanel.Options.UseForeColor = true;
            this.gridViewInad.Appearance.GroupButton.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(248)))), ((int)(((byte)(204)))), ((int)(((byte)(124)))));
            this.gridViewInad.Appearance.GroupButton.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(248)))), ((int)(((byte)(204)))), ((int)(((byte)(124)))));
            this.gridViewInad.Appearance.GroupButton.Options.UseBackColor = true;
            this.gridViewInad.Appearance.GroupButton.Options.UseBorderColor = true;
            this.gridViewInad.Appearance.GroupFooter.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(248)))), ((int)(((byte)(204)))), ((int)(((byte)(124)))));
            this.gridViewInad.Appearance.GroupFooter.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(248)))), ((int)(((byte)(204)))), ((int)(((byte)(124)))));
            this.gridViewInad.Appearance.GroupFooter.ForeColor = System.Drawing.Color.Black;
            this.gridViewInad.Appearance.GroupFooter.Options.UseBackColor = true;
            this.gridViewInad.Appearance.GroupFooter.Options.UseBorderColor = true;
            this.gridViewInad.Appearance.GroupFooter.Options.UseForeColor = true;
            this.gridViewInad.Appearance.GroupPanel.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(251)))), ((int)(((byte)(244)))), ((int)(((byte)(232)))));
            this.gridViewInad.Appearance.GroupPanel.BackColor2 = System.Drawing.Color.White;
            this.gridViewInad.Appearance.GroupPanel.ForeColor = System.Drawing.Color.Black;
            this.gridViewInad.Appearance.GroupPanel.Options.UseBackColor = true;
            this.gridViewInad.Appearance.GroupPanel.Options.UseForeColor = true;
            this.gridViewInad.Appearance.GroupRow.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(248)))), ((int)(((byte)(204)))), ((int)(((byte)(124)))));
            this.gridViewInad.Appearance.GroupRow.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(248)))), ((int)(((byte)(204)))), ((int)(((byte)(124)))));
            this.gridViewInad.Appearance.GroupRow.ForeColor = System.Drawing.Color.Black;
            this.gridViewInad.Appearance.GroupRow.Options.UseBackColor = true;
            this.gridViewInad.Appearance.GroupRow.Options.UseBorderColor = true;
            this.gridViewInad.Appearance.GroupRow.Options.UseForeColor = true;
            this.gridViewInad.Appearance.HeaderPanel.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(248)))), ((int)(((byte)(204)))), ((int)(((byte)(124)))));
            this.gridViewInad.Appearance.HeaderPanel.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(248)))), ((int)(((byte)(204)))), ((int)(((byte)(124)))));
            this.gridViewInad.Appearance.HeaderPanel.ForeColor = System.Drawing.Color.Black;
            this.gridViewInad.Appearance.HeaderPanel.Options.UseBackColor = true;
            this.gridViewInad.Appearance.HeaderPanel.Options.UseBorderColor = true;
            this.gridViewInad.Appearance.HeaderPanel.Options.UseForeColor = true;
            this.gridViewInad.Appearance.HideSelectionRow.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(225)))), ((int)(((byte)(183)))), ((int)(((byte)(125)))));
            this.gridViewInad.Appearance.HideSelectionRow.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(251)))), ((int)(((byte)(244)))), ((int)(((byte)(232)))));
            this.gridViewInad.Appearance.HideSelectionRow.Options.UseBackColor = true;
            this.gridViewInad.Appearance.HideSelectionRow.Options.UseForeColor = true;
            this.gridViewInad.Appearance.HorzLine.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(240)))), ((int)(((byte)(177)))), ((int)(((byte)(94)))));
            this.gridViewInad.Appearance.HorzLine.Options.UseBackColor = true;
            this.gridViewInad.Appearance.OddRow.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(251)))), ((int)(((byte)(236)))), ((int)(((byte)(208)))));
            this.gridViewInad.Appearance.OddRow.ForeColor = System.Drawing.Color.Black;
            this.gridViewInad.Appearance.OddRow.Options.UseBackColor = true;
            this.gridViewInad.Appearance.OddRow.Options.UseForeColor = true;
            this.gridViewInad.Appearance.Preview.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(254)))), ((int)(((byte)(249)))));
            this.gridViewInad.Appearance.Preview.Font = new System.Drawing.Font("Verdana", 7.5F);
            this.gridViewInad.Appearance.Preview.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(186)))), ((int)(((byte)(146)))), ((int)(((byte)(78)))));
            this.gridViewInad.Appearance.Preview.Options.UseBackColor = true;
            this.gridViewInad.Appearance.Preview.Options.UseFont = true;
            this.gridViewInad.Appearance.Preview.Options.UseForeColor = true;
            this.gridViewInad.Appearance.Row.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(253)))), ((int)(((byte)(248)))), ((int)(((byte)(236)))));
            this.gridViewInad.Appearance.Row.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(253)))), ((int)(((byte)(248)))), ((int)(((byte)(236)))));
            this.gridViewInad.Appearance.Row.ForeColor = System.Drawing.Color.Black;
            this.gridViewInad.Appearance.Row.Options.UseBackColor = true;
            this.gridViewInad.Appearance.Row.Options.UseBorderColor = true;
            this.gridViewInad.Appearance.Row.Options.UseForeColor = true;
            this.gridViewInad.Appearance.RowSeparator.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(251)))), ((int)(((byte)(244)))), ((int)(((byte)(232)))));
            this.gridViewInad.Appearance.RowSeparator.BackColor2 = System.Drawing.Color.White;
            this.gridViewInad.Appearance.RowSeparator.Options.UseBackColor = true;
            this.gridViewInad.Appearance.SelectedRow.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(231)))), ((int)(((byte)(167)))), ((int)(((byte)(103)))));
            this.gridViewInad.Appearance.SelectedRow.Options.UseBackColor = true;
            this.gridViewInad.Appearance.TopNewRow.BackColor = System.Drawing.Color.White;
            this.gridViewInad.Appearance.TopNewRow.Options.UseBackColor = true;
            this.gridViewInad.Appearance.VertLine.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(240)))), ((int)(((byte)(177)))), ((int)(((byte)(94)))));
            this.gridViewInad.Appearance.VertLine.Options.UseBackColor = true;
            this.gridViewInad.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colBLOAPT1,
            this.colPI1,
            this.colBOLVencto2,
            this.colBOLPagamento2,
            this.colValAnterior,
            this.colValFinal,
            this.colValMes,
            this.colValRec,
            this.colAcordo,
            this.colBOL3,
            this.colBOLTipoCRAI2});
            this.gridViewInad.GridControl = this.gridControl12;
            this.gridViewInad.GroupCount = 1;
            this.gridViewInad.GroupSummary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridGroupSummaryItem(DevExpress.Data.SummaryItemType.Sum, "ValFinal", this.colValFinal, "{0:n2}")});
            this.gridViewInad.Name = "gridViewInad";
            this.gridViewInad.OptionsBehavior.AutoExpandAllGroups = true;
            this.gridViewInad.OptionsView.ColumnAutoWidth = false;
            this.gridViewInad.OptionsView.EnableAppearanceEvenRow = true;
            this.gridViewInad.OptionsView.EnableAppearanceOddRow = true;
            this.gridViewInad.OptionsView.ShowAutoFilterRow = true;
            this.gridViewInad.OptionsView.ShowFooter = true;
            this.gridViewInad.OptionsView.ShowGroupedColumns = true;
            this.gridViewInad.OptionsView.ShowGroupPanel = false;
            this.gridViewInad.PaintStyleName = "Flat";
            this.gridViewInad.SortInfo.AddRange(new DevExpress.XtraGrid.Columns.GridColumnSortInfo[] {
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colBLOAPT1, DevExpress.Data.ColumnSortOrder.Ascending),
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colBOLVencto2, DevExpress.Data.ColumnSortOrder.Ascending),
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colBOLPagamento2, DevExpress.Data.ColumnSortOrder.Ascending)});
            // 
            // colBLOAPT1
            // 
            this.colBLOAPT1.Caption = "Apto";
            this.colBLOAPT1.FieldName = "BLOAPT";
            this.colBLOAPT1.Name = "colBLOAPT1";
            this.colBLOAPT1.Visible = true;
            this.colBLOAPT1.VisibleIndex = 0;
            this.colBLOAPT1.Width = 84;
            // 
            // colPI1
            // 
            this.colPI1.Caption = "P/I";
            this.colPI1.FieldName = "PI";
            this.colPI1.Name = "colPI1";
            this.colPI1.Visible = true;
            this.colPI1.VisibleIndex = 3;
            this.colPI1.Width = 37;
            // 
            // colBOLVencto2
            // 
            this.colBOLVencto2.Caption = "Vencimento";
            this.colBOLVencto2.DisplayFormat.FormatString = "dd/MM/yyyy";
            this.colBOLVencto2.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.colBOLVencto2.FieldName = "BOLVencto";
            this.colBOLVencto2.Name = "colBOLVencto2";
            this.colBOLVencto2.Visible = true;
            this.colBOLVencto2.VisibleIndex = 4;
            // 
            // colBOLPagamento2
            // 
            this.colBOLPagamento2.Caption = "Pag.";
            this.colBOLPagamento2.DisplayFormat.FormatString = "dd/MM/yyyy";
            this.colBOLPagamento2.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.colBOLPagamento2.FieldName = "BOLPagamento";
            this.colBOLPagamento2.Name = "colBOLPagamento2";
            this.colBOLPagamento2.Visible = true;
            this.colBOLPagamento2.VisibleIndex = 5;
            // 
            // colValAnterior
            // 
            this.colValAnterior.Caption = "Anterior";
            this.colValAnterior.DisplayFormat.FormatString = "n2";
            this.colValAnterior.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.colValAnterior.FieldName = "ValAnterior";
            this.colValAnterior.Name = "colValAnterior";
            this.colValAnterior.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum, "ValAnterior", "{0:n2}")});
            this.colValAnterior.Visible = true;
            this.colValAnterior.VisibleIndex = 6;
            // 
            // colValFinal
            // 
            this.colValFinal.Caption = "Atual";
            this.colValFinal.DisplayFormat.FormatString = "n2";
            this.colValFinal.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.colValFinal.FieldName = "ValFinal";
            this.colValFinal.Name = "colValFinal";
            this.colValFinal.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum, "ValFinal", "{0:n2}")});
            this.colValFinal.Visible = true;
            this.colValFinal.VisibleIndex = 7;
            // 
            // colValMes
            // 
            this.colValMes.Caption = "mês";
            this.colValMes.DisplayFormat.FormatString = "n2";
            this.colValMes.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.colValMes.FieldName = "ValMes";
            this.colValMes.Name = "colValMes";
            this.colValMes.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum, "ValMes", "{0:n2}")});
            this.colValMes.Visible = true;
            this.colValMes.VisibleIndex = 8;
            // 
            // colValRec
            // 
            this.colValRec.Caption = "Rec.";
            this.colValRec.DisplayFormat.FormatString = "n2";
            this.colValRec.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.colValRec.FieldName = "ValRec";
            this.colValRec.Name = "colValRec";
            this.colValRec.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum, "ValRec", "{0:n2}")});
            this.colValRec.Visible = true;
            this.colValRec.VisibleIndex = 9;
            // 
            // colAcordo
            // 
            this.colAcordo.Caption = "Acordo";
            this.colAcordo.FieldName = "Acordo";
            this.colAcordo.Name = "colAcordo";
            this.colAcordo.Visible = true;
            this.colAcordo.VisibleIndex = 10;
            // 
            // colBOL3
            // 
            this.colBOL3.Caption = "Boleto";
            this.colBOL3.FieldName = "BOL";
            this.colBOL3.Name = "colBOL3";
            this.colBOL3.Visible = true;
            this.colBOL3.VisibleIndex = 1;
            // 
            // colBOLTipoCRAI2
            // 
            this.colBOLTipoCRAI2.FieldName = "BOLTipoCRAI";
            this.colBOLTipoCRAI2.Name = "colBOLTipoCRAI2";
            this.colBOLTipoCRAI2.OptionsColumn.ShowCaption = false;
            this.colBOLTipoCRAI2.Visible = true;
            this.colBOLTipoCRAI2.VisibleIndex = 2;
            this.colBOLTipoCRAI2.Width = 24;
            // 
            // xtraTabPagePGF
            // 
            this.xtraTabPagePGF.Controls.Add(this.gridControl9);
            this.xtraTabPagePGF.Controls.Add(this.panelControl3);
            this.xtraTabPagePGF.Name = "xtraTabPagePGF";
            this.xtraTabPagePGF.Size = new System.Drawing.Size(1459, 232);
            this.xtraTabPagePGF.Text = "Pagamentos Periódicos";
            // 
            // panelControl3
            // 
            this.panelControl3.Controls.Add(this.panelManual);
            this.panelControl3.Controls.Add(this.panelVencido);
            this.panelControl3.Controls.Add(this.panelFuturo);
            this.panelControl3.Controls.Add(this.panelAgendado);
            this.panelControl3.Controls.Add(this.panelPago);
            this.panelControl3.Dock = System.Windows.Forms.DockStyle.Top;
            this.panelControl3.Location = new System.Drawing.Point(0, 0);
            this.panelControl3.Name = "panelControl3";
            this.panelControl3.Size = new System.Drawing.Size(1459, 38);
            this.panelControl3.TabIndex = 1;
            // 
            // panelManual
            // 
            this.panelManual.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
            this.panelManual.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.panelManual.Appearance.Options.UseBackColor = true;
            this.panelManual.Controls.Add(this.labelManual);
            this.panelManual.Location = new System.Drawing.Point(406, 6);
            this.panelManual.LookAndFeel.Style = DevExpress.LookAndFeel.LookAndFeelStyle.Flat;
            this.panelManual.LookAndFeel.UseDefaultLookAndFeel = false;
            this.panelManual.Name = "panelManual";
            this.panelManual.Size = new System.Drawing.Size(94, 25);
            this.panelManual.TabIndex = 4;
            // 
            // labelManual
            // 
            this.labelManual.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
            this.labelManual.Appearance.Options.UseFont = true;
            this.labelManual.Appearance.Options.UseTextOptions = true;
            this.labelManual.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.labelManual.Appearance.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.labelManual.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.labelManual.Dock = System.Windows.Forms.DockStyle.Fill;
            this.labelManual.Location = new System.Drawing.Point(3, 3);
            this.labelManual.Name = "labelManual";
            this.labelManual.Size = new System.Drawing.Size(88, 19);
            this.labelManual.TabIndex = 1;
            this.labelManual.Text = "Manual";
            // 
            // panelVencido
            // 
            this.panelVencido.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
            this.panelVencido.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(192)))), ((int)(((byte)(192)))));
            this.panelVencido.Appearance.Options.UseBackColor = true;
            this.panelVencido.Controls.Add(this.labelVencido);
            this.panelVencido.Location = new System.Drawing.Point(206, 6);
            this.panelVencido.LookAndFeel.Style = DevExpress.LookAndFeel.LookAndFeelStyle.Flat;
            this.panelVencido.LookAndFeel.UseDefaultLookAndFeel = false;
            this.panelVencido.Name = "panelVencido";
            this.panelVencido.Size = new System.Drawing.Size(94, 25);
            this.panelVencido.TabIndex = 3;
            // 
            // labelVencido
            // 
            this.labelVencido.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
            this.labelVencido.Appearance.Options.UseFont = true;
            this.labelVencido.Appearance.Options.UseTextOptions = true;
            this.labelVencido.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.labelVencido.Appearance.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.labelVencido.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.labelVencido.Dock = System.Windows.Forms.DockStyle.Fill;
            this.labelVencido.Location = new System.Drawing.Point(3, 3);
            this.labelVencido.Name = "labelVencido";
            this.labelVencido.Size = new System.Drawing.Size(88, 19);
            this.labelVencido.TabIndex = 1;
            this.labelVencido.Text = "Vencido";
            // 
            // panelFuturo
            // 
            this.panelFuturo.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
            this.panelFuturo.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.panelFuturo.Appearance.Options.UseBackColor = true;
            this.panelFuturo.Controls.Add(this.labelFuturo);
            this.panelFuturo.Location = new System.Drawing.Point(306, 6);
            this.panelFuturo.LookAndFeel.Style = DevExpress.LookAndFeel.LookAndFeelStyle.Flat;
            this.panelFuturo.LookAndFeel.UseDefaultLookAndFeel = false;
            this.panelFuturo.Name = "panelFuturo";
            this.panelFuturo.Size = new System.Drawing.Size(94, 25);
            this.panelFuturo.TabIndex = 2;
            // 
            // labelFuturo
            // 
            this.labelFuturo.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
            this.labelFuturo.Appearance.Options.UseFont = true;
            this.labelFuturo.Appearance.Options.UseTextOptions = true;
            this.labelFuturo.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.labelFuturo.Appearance.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.labelFuturo.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.labelFuturo.Dock = System.Windows.Forms.DockStyle.Fill;
            this.labelFuturo.Location = new System.Drawing.Point(3, 3);
            this.labelFuturo.Name = "labelFuturo";
            this.labelFuturo.Size = new System.Drawing.Size(88, 19);
            this.labelFuturo.TabIndex = 1;
            this.labelFuturo.Text = "Futuro";
            // 
            // panelAgendado
            // 
            this.panelAgendado.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
            this.panelAgendado.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.panelAgendado.Appearance.Options.UseBackColor = true;
            this.panelAgendado.Controls.Add(this.labelAgendado);
            this.panelAgendado.Location = new System.Drawing.Point(106, 6);
            this.panelAgendado.LookAndFeel.Style = DevExpress.LookAndFeel.LookAndFeelStyle.Flat;
            this.panelAgendado.LookAndFeel.UseDefaultLookAndFeel = false;
            this.panelAgendado.Name = "panelAgendado";
            this.panelAgendado.Size = new System.Drawing.Size(94, 25);
            this.panelAgendado.TabIndex = 1;
            // 
            // labelAgendado
            // 
            this.labelAgendado.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
            this.labelAgendado.Appearance.Options.UseFont = true;
            this.labelAgendado.Appearance.Options.UseTextOptions = true;
            this.labelAgendado.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.labelAgendado.Appearance.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.labelAgendado.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.labelAgendado.Dock = System.Windows.Forms.DockStyle.Fill;
            this.labelAgendado.Location = new System.Drawing.Point(3, 3);
            this.labelAgendado.Name = "labelAgendado";
            this.labelAgendado.Size = new System.Drawing.Size(88, 19);
            this.labelAgendado.TabIndex = 1;
            this.labelAgendado.Text = "Agendado";
            // 
            // panelPago
            // 
            this.panelPago.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
            this.panelPago.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.panelPago.Appearance.Options.UseBackColor = true;
            this.panelPago.Controls.Add(this.labelPago);
            this.panelPago.Location = new System.Drawing.Point(6, 6);
            this.panelPago.LookAndFeel.Style = DevExpress.LookAndFeel.LookAndFeelStyle.Flat;
            this.panelPago.LookAndFeel.UseDefaultLookAndFeel = false;
            this.panelPago.Name = "panelPago";
            this.panelPago.Size = new System.Drawing.Size(94, 25);
            this.panelPago.TabIndex = 0;
            // 
            // labelPago
            // 
            this.labelPago.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
            this.labelPago.Appearance.Options.UseFont = true;
            this.labelPago.Appearance.Options.UseTextOptions = true;
            this.labelPago.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.labelPago.Appearance.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.labelPago.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.labelPago.Dock = System.Windows.Forms.DockStyle.Fill;
            this.labelPago.Location = new System.Drawing.Point(3, 3);
            this.labelPago.Name = "labelPago";
            this.labelPago.Size = new System.Drawing.Size(88, 19);
            this.labelPago.TabIndex = 0;
            this.labelPago.Text = "Pago";
            // 
            // xtraTabPageOBS
            // 
            this.xtraTabPageOBS.Controls.Add(this.rtF_Editor1);
            this.xtraTabPageOBS.Controls.Add(this.panelControl4);
            this.xtraTabPageOBS.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.xtraTabPageOBS.Name = "xtraTabPageOBS";
            this.xtraTabPageOBS.Size = new System.Drawing.Size(1459, 232);
            this.xtraTabPageOBS.Text = "Obs";
            // 
            // rtF_Editor1
            // 
            this.rtF_Editor1.CaixaAltaGeral = true;
            this.rtF_Editor1.Doc = System.Windows.Forms.DockStyle.Fill;
            this.rtF_Editor1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.rtF_Editor1.Estado = CompontesBasicos.EstadosDosComponentes.Registrado;
            this.rtF_Editor1.Location = new System.Drawing.Point(184, 0);
            this.rtF_Editor1.LookAndFeel.UseDefaultLookAndFeel = false;
            this.rtF_Editor1.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.rtF_Editor1.Name = "rtF_Editor1";
            this.rtF_Editor1.RTFstring = resources.GetString("rtF_Editor1.RTFstring");
            this.rtF_Editor1.Size = new System.Drawing.Size(1275, 232);
            this.rtF_Editor1.somenteleitura = true;
            this.rtF_Editor1.TabIndex = 1;
            this.rtF_Editor1.Titulo = null;
            // 
            // panelControl4
            // 
            this.panelControl4.Controls.Add(this.radioGroup1);
            this.panelControl4.Dock = System.Windows.Forms.DockStyle.Left;
            this.panelControl4.Location = new System.Drawing.Point(0, 0);
            this.panelControl4.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.panelControl4.Name = "panelControl4";
            this.panelControl4.Size = new System.Drawing.Size(184, 232);
            this.panelControl4.TabIndex = 0;
            // 
            // radioGroup1
            // 
            this.radioGroup1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.radioGroup1.Location = new System.Drawing.Point(2, 2);
            this.radioGroup1.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.radioGroup1.Name = "radioGroup1";
            this.radioGroup1.Properties.Items.AddRange(new DevExpress.XtraEditors.Controls.RadioGroupItem[] {
            new DevExpress.XtraEditors.Controls.RadioGroupItem(null, "Capa")});
            this.radioGroup1.Size = new System.Drawing.Size(180, 228);
            this.radioGroup1.TabIndex = 0;
            this.radioGroup1.EditValueChanged += new System.EventHandler(this.radioGroup1_EditValueChanged);
            this.radioGroup1.EditValueChanging += new DevExpress.XtraEditors.Controls.ChangingEventHandler(this.radioGroup1_EditValueChanging);
            // 
            // xtraTabPageErro
            // 
            this.xtraTabPageErro.Appearance.Header.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
            this.xtraTabPageErro.Appearance.Header.ForeColor = System.Drawing.Color.Red;
            this.xtraTabPageErro.Appearance.Header.Options.UseFont = true;
            this.xtraTabPageErro.Appearance.Header.Options.UseForeColor = true;
            this.xtraTabPageErro.Controls.Add(this.gridControl10);
            this.xtraTabPageErro.Name = "xtraTabPageErro";
            this.xtraTabPageErro.Size = new System.Drawing.Size(1459, 232);
            this.xtraTabPageErro.Text = "Erros / Avisos";
            // 
            // gridControl10
            // 
            this.gridControl10.DataMember = "ErrosAvisos";
            this.gridControl10.DataSource = this.dBalanceteBindingSource;
            this.gridControl10.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridControl10.Location = new System.Drawing.Point(0, 0);
            this.gridControl10.MainView = this.gridView9;
            this.gridControl10.Name = "gridControl10";
            this.gridControl10.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemErro,
            this.repositoryItemMemoExEdit2,
            this.repositoryItemMemoEdit1});
            this.gridControl10.Size = new System.Drawing.Size(1459, 232);
            this.gridControl10.TabIndex = 0;
            this.gridControl10.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridView9});
            // 
            // gridView9
            // 
            this.gridView9.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colTipo3,
            this.colNumero1,
            this.colDescricao6,
            this.colData2,
            this.colOrdem4});
            this.gridView9.GridControl = this.gridControl10;
            this.gridView9.Name = "gridView9";
            this.gridView9.OptionsView.RowAutoHeight = true;
            this.gridView9.OptionsView.ShowGroupPanel = false;
            this.gridView9.SortInfo.AddRange(new DevExpress.XtraGrid.Columns.GridColumnSortInfo[] {
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colTipo3, DevExpress.Data.ColumnSortOrder.Descending),
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colNumero1, DevExpress.Data.ColumnSortOrder.Ascending)});
            // 
            // colTipo3
            // 
            this.colTipo3.Caption = "Tipo";
            this.colTipo3.ColumnEdit = this.repositoryItemErro;
            this.colTipo3.FieldName = "Tipo";
            this.colTipo3.Name = "colTipo3";
            this.colTipo3.OptionsColumn.FixedWidth = true;
            this.colTipo3.OptionsColumn.ReadOnly = true;
            this.colTipo3.OptionsColumn.ShowCaption = false;
            this.colTipo3.SortMode = DevExpress.XtraGrid.ColumnSortMode.Value;
            this.colTipo3.Visible = true;
            this.colTipo3.VisibleIndex = 0;
            this.colTipo3.Width = 34;
            // 
            // repositoryItemErro
            // 
            this.repositoryItemErro.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo, "", -1, true, false, false, editorButtonImageOptions12)});
            this.repositoryItemErro.Items.AddRange(new DevExpress.XtraEditors.Controls.ImageComboBoxItem[] {
            new DevExpress.XtraEditors.Controls.ImageComboBoxItem("", 0, 2),
            new DevExpress.XtraEditors.Controls.ImageComboBoxItem("", 1, 0)});
            this.repositoryItemErro.LargeImages = this.imageCollection1;
            this.repositoryItemErro.Name = "repositoryItemErro";
            this.repositoryItemErro.SmallImages = this.imageCollection1;
            // 
            // colNumero1
            // 
            this.colNumero1.Caption = "n°";
            this.colNumero1.FieldName = "Numero";
            this.colNumero1.Name = "colNumero1";
            this.colNumero1.OptionsColumn.FixedWidth = true;
            this.colNumero1.OptionsColumn.ReadOnly = true;
            this.colNumero1.Visible = true;
            this.colNumero1.VisibleIndex = 1;
            this.colNumero1.Width = 47;
            // 
            // colDescricao6
            // 
            this.colDescricao6.Caption = "Descrição";
            this.colDescricao6.ColumnEdit = this.repositoryItemMemoEdit1;
            this.colDescricao6.FieldName = "Descricao";
            this.colDescricao6.Name = "colDescricao6";
            this.colDescricao6.OptionsColumn.ReadOnly = true;
            this.colDescricao6.Visible = true;
            this.colDescricao6.VisibleIndex = 3;
            this.colDescricao6.Width = 452;
            // 
            // repositoryItemMemoEdit1
            // 
            this.repositoryItemMemoEdit1.Name = "repositoryItemMemoEdit1";
            // 
            // colData2
            // 
            this.colData2.FieldName = "Data";
            this.colData2.Name = "colData2";
            this.colData2.OptionsColumn.FixedWidth = true;
            this.colData2.OptionsColumn.ReadOnly = true;
            this.colData2.Visible = true;
            this.colData2.VisibleIndex = 2;
            // 
            // colOrdem4
            // 
            this.colOrdem4.FieldName = "Ordem";
            this.colOrdem4.Name = "colOrdem4";
            this.colOrdem4.OptionsColumn.FixedWidth = true;
            this.colOrdem4.OptionsColumn.ReadOnly = true;
            // 
            // repositoryItemMemoExEdit2
            // 
            this.repositoryItemMemoExEdit2.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemMemoExEdit2.Name = "repositoryItemMemoExEdit2";
            this.repositoryItemMemoExEdit2.ShowIcon = false;
            // 
            // panelControl2
            // 
            this.panelControl2.Controls.Add(this.zoomTrackBarControl1);
            this.panelControl2.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panelControl2.Location = new System.Drawing.Point(0, 781);
            this.panelControl2.Name = "panelControl2";
            this.panelControl2.Size = new System.Drawing.Size(1452, 26);
            this.panelControl2.TabIndex = 3;
            // 
            // panelDesenvolvedor
            // 
            this.panelDesenvolvedor.Controls.Add(this.BotaoTeste);
            this.panelDesenvolvedor.Controls.Add(this.CCDRastrarVal);
            this.panelDesenvolvedor.Controls.Add(this.labelControl6);
            this.panelDesenvolvedor.Controls.Add(this.memoEdit1);
            this.panelDesenvolvedor.Controls.Add(this.simpleButton5);
            this.panelDesenvolvedor.Controls.Add(this.CCDRastrarAc);
            this.panelDesenvolvedor.Controls.Add(this.labelControl5);
            this.panelDesenvolvedor.Controls.Add(this.CCDRastrarConc);
            this.panelDesenvolvedor.Controls.Add(this.labelControl4);
            this.panelDesenvolvedor.Controls.Add(this.CCDRastrearCDB);
            this.panelDesenvolvedor.Controls.Add(this.labelControl3);
            this.panelDesenvolvedor.Controls.Add(this.CCDRastrarCarga);
            this.panelDesenvolvedor.Controls.Add(this.labelControl2);
            this.panelDesenvolvedor.Dock = System.Windows.Forms.DockStyle.Top;
            this.panelDesenvolvedor.Location = new System.Drawing.Point(0, 0);
            this.panelDesenvolvedor.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.panelDesenvolvedor.Name = "panelDesenvolvedor";
            this.panelDesenvolvedor.Size = new System.Drawing.Size(1452, 88);
            this.panelDesenvolvedor.TabIndex = 4;
            this.panelDesenvolvedor.Visible = false;
            // 
            // BotaoTeste
            // 
            this.BotaoTeste.Location = new System.Drawing.Point(1070, 15);
            this.BotaoTeste.Name = "BotaoTeste";
            this.BotaoTeste.Size = new System.Drawing.Size(160, 44);
            this.BotaoTeste.TabIndex = 32;
            this.BotaoTeste.Text = "Teste";
            this.BotaoTeste.Click += new System.EventHandler(this.BotaoTeste_Click);
            // 
            // CCDRastrarVal
            // 
            this.CCDRastrarVal.EditValue = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.CCDRastrarVal.Location = new System.Drawing.Point(513, 12);
            this.CCDRastrarVal.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.CCDRastrarVal.Name = "CCDRastrarVal";
            this.CCDRastrarVal.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.CCDRastrarVal.Properties.IsFloatValue = false;
            this.CCDRastrarVal.Properties.Mask.EditMask = "n0";
            this.CCDRastrarVal.Size = new System.Drawing.Size(111, 20);
            this.CCDRastrarVal.TabIndex = 12;
            // 
            // labelControl6
            // 
            this.labelControl6.Location = new System.Drawing.Point(442, 15);
            this.labelControl6.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.labelControl6.Name = "labelControl6";
            this.labelControl6.Size = new System.Drawing.Size(64, 13);
            this.labelControl6.TabIndex = 11;
            this.labelControl6.Text = "CCD Validado";
            // 
            // memoEdit1
            // 
            this.memoEdit1.Location = new System.Drawing.Point(630, 6);
            this.memoEdit1.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.memoEdit1.Name = "memoEdit1";
            this.memoEdit1.Size = new System.Drawing.Size(421, 78);
            this.memoEdit1.TabIndex = 10;
            // 
            // simpleButton5
            // 
            this.simpleButton5.Location = new System.Drawing.Point(494, 58);
            this.simpleButton5.Name = "simpleButton5";
            this.simpleButton5.Size = new System.Drawing.Size(131, 23);
            this.simpleButton5.TabIndex = 8;
            this.simpleButton5.Text = "Checa erro";
            this.simpleButton5.Click += new System.EventHandler(this.simpleButton5_Click);
            // 
            // CCDRastrarAc
            // 
            this.CCDRastrarAc.EditValue = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.CCDRastrarAc.Location = new System.Drawing.Point(319, 32);
            this.CCDRastrarAc.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.CCDRastrarAc.Name = "CCDRastrarAc";
            this.CCDRastrarAc.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.CCDRastrarAc.Properties.IsFloatValue = false;
            this.CCDRastrarAc.Properties.Mask.EditMask = "n0";
            this.CCDRastrarAc.Size = new System.Drawing.Size(111, 20);
            this.CCDRastrarAc.TabIndex = 7;
            // 
            // labelControl5
            // 
            this.labelControl5.Location = new System.Drawing.Point(235, 35);
            this.labelControl5.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.labelControl5.Name = "labelControl5";
            this.labelControl5.Size = new System.Drawing.Size(68, 13);
            this.labelControl5.TabIndex = 6;
            this.labelControl5.Text = "CCD Acumular";
            // 
            // CCDRastrarConc
            // 
            this.CCDRastrarConc.EditValue = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.CCDRastrarConc.Location = new System.Drawing.Point(319, 10);
            this.CCDRastrarConc.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.CCDRastrarConc.Name = "CCDRastrarConc";
            this.CCDRastrarConc.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.CCDRastrarConc.Properties.IsFloatValue = false;
            this.CCDRastrarConc.Properties.Mask.EditMask = "n0";
            this.CCDRastrarConc.Size = new System.Drawing.Size(111, 20);
            this.CCDRastrarConc.TabIndex = 5;
            // 
            // labelControl4
            // 
            this.labelControl4.Location = new System.Drawing.Point(235, 12);
            this.labelControl4.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.labelControl4.Name = "labelControl4";
            this.labelControl4.Size = new System.Drawing.Size(77, 13);
            this.labelControl4.TabIndex = 4;
            this.labelControl4.Text = "CCD Conciliação";
            // 
            // CCDRastrearCDB
            // 
            this.CCDRastrearCDB.EditValue = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.CCDRastrearCDB.Location = new System.Drawing.Point(118, 32);
            this.CCDRastrearCDB.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.CCDRastrearCDB.Name = "CCDRastrearCDB";
            this.CCDRastrearCDB.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.CCDRastrearCDB.Properties.IsFloatValue = false;
            this.CCDRastrearCDB.Properties.Mask.EditMask = "n0";
            this.CCDRastrearCDB.Size = new System.Drawing.Size(111, 20);
            this.CCDRastrearCDB.TabIndex = 3;
            // 
            // labelControl3
            // 
            this.labelControl3.Location = new System.Drawing.Point(9, 35);
            this.labelControl3.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.labelControl3.Name = "labelControl3";
            this.labelControl3.Size = new System.Drawing.Size(104, 13);
            this.labelControl3.TabIndex = 2;
            this.labelControl3.Text = "CCD Dados Balancete";
            // 
            // CCDRastrarCarga
            // 
            this.CCDRastrarCarga.EditValue = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.CCDRastrarCarga.Location = new System.Drawing.Point(118, 10);
            this.CCDRastrarCarga.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.CCDRastrarCarga.Name = "CCDRastrarCarga";
            this.CCDRastrarCarga.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.CCDRastrarCarga.Properties.IsFloatValue = false;
            this.CCDRastrarCarga.Properties.Mask.EditMask = "n0";
            this.CCDRastrarCarga.Size = new System.Drawing.Size(111, 20);
            this.CCDRastrarCarga.TabIndex = 1;
            // 
            // labelControl2
            // 
            this.labelControl2.Location = new System.Drawing.Point(9, 12);
            this.labelControl2.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.labelControl2.Name = "labelControl2";
            this.labelControl2.Size = new System.Drawing.Size(98, 13);
            this.labelControl2.TabIndex = 0;
            this.labelControl2.Text = "CCD Rastrear Carga";
            // 
            // cBalancete
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.CaixaAltaGeral = false;
            this.Controls.Add(this.xtraTabControl1);
            this.Controls.Add(this.panelControl1);
            this.Controls.Add(this.panelControl2);
            this.Controls.Add(this.panelDesenvolvedor);
            this.Controls.Add(this.barDockControlLeft);
            this.Controls.Add(this.barDockControlRight);
            this.Controls.Add(this.barDockControlBottom);
            this.Controls.Add(this.barDockControlTop);
            this.Doc = System.Windows.Forms.DockStyle.Fill;
            this.LookAndFeel.UseDefaultLookAndFeel = false;
            this.Name = "cBalancete";
            this.Size = new System.Drawing.Size(1452, 807);
            this.Titulo = "Balancete";
            ((System.ComponentModel.ISupportInitialize)(this.StyleController_F)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.CalcEdit_n2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dCreditosBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.BotDesc)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repBotaoBoleto1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.bandedGridView3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.BOTNota)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemImageComboBox3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemImageComboBox4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl9)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pagamentosBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.bandedGridView2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCalcEdit4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.BotPGF)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.BotCHE)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dBalanceteBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl1)).EndInit();
            this.panelControl1.ResumeLayout(false);
            this.panelControl1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.chCPMF.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.popupComprovantes)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.barManager1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chInadDet.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chCapa.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit1.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cHDicionario.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lookMBA.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.modeloBAlanceteBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.StatusBAL.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkTF.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chExtratos.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chInadimplencia.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chPrevisto.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chCreditosAntigo.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chSaldos.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chDetalhamentoCreditos.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chDebitosCreditos.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.zoomTrackBarControl1.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.zoomTrackBarControl1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.bandedGridView1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryNULO)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoExEdit1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.imageCollection1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.IMGTipos)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.imageCollection2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TextEditCorretor70)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCalcEdit1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemImageComboBox1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemImageComboBox2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemLookUpPLA)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pLAnocontasBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemLookUpEditCTL)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.conTasLogicasBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemButtonEdit1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemBotCHE)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemBotNoa)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemBotPGF)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemButtonPLUS)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckEdit1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryDelEst)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xtraTabControl1)).EndInit();
            this.xtraTabControl1.ResumeLayout(false);
            this.xtraTabPageDiario.ResumeLayout(false);
            this.xtraTabPageIndividual.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gridControl3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tabelaVirtualBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryTextEdit70)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemCalcEdit)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lookPLA)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.IMGTipos1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemLookUpEditCTL1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoExEdit1A)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ImageComboBox_D_C)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.imageCollectionDC)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemButtonNulo)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemBotCHE2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemBotNOA2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemBotPGF2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryAgrupar)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryDelEst1)).EndInit();
            this.xtraTabPageCreditos.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.xtraTabControlCredito)).EndInit();
            this.xtraTabControlCredito.ResumeLayout(false);
            this.xtraTabCredResumo.ResumeLayout(false);
            this.xtraTabCredDetalhes.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gridControl2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repValor)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repPI)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repGrupos)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repTipoGrafico)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoExEdit3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repBotaoBoleto)).EndInit();
            this.xtraTabCredGrafico.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(simpleDiagram3D1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(pie3DSeriesLabel1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(pie3DSeriesView1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(series1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(pie3DSeriesLabel2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(pie3DSeriesView2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chartControl1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl11)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView10)).EndInit();
            this.xtraTabPageDebitos.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gridControl6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView6)).EndInit();
            this.xtraTabPageCTL.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gridControl5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.CalcEditn2)).EndInit();
            this.xtraTabPageCCT.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gridControl7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCalcEdit2)).EndInit();
            this.xtraTabPagePrevisto.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gridControl8)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.previstoBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView8)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCalcEdit3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemButtonEdit3)).EndInit();
            this.xtraTabPageInad.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gridControl12)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.inadimpBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridViewInad)).EndInit();
            this.xtraTabPagePGF.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.panelControl3)).EndInit();
            this.panelControl3.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.panelManual)).EndInit();
            this.panelManual.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.panelVencido)).EndInit();
            this.panelVencido.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.panelFuturo)).EndInit();
            this.panelFuturo.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.panelAgendado)).EndInit();
            this.panelAgendado.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.panelPago)).EndInit();
            this.panelPago.ResumeLayout(false);
            this.xtraTabPageOBS.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.panelControl4)).EndInit();
            this.panelControl4.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.radioGroup1.Properties)).EndInit();
            this.xtraTabPageErro.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gridControl10)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView9)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemErro)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoEdit1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoExEdit2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl2)).EndInit();
            this.panelControl2.ResumeLayout(false);
            this.panelControl2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.panelDesenvolvedor)).EndInit();
            this.panelDesenvolvedor.ResumeLayout(false);
            this.panelDesenvolvedor.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.CCDRastrarVal.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.memoEdit1.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.CCDRastrarAc.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.CCDRastrarConc.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.CCDRastrearCDB.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.CCDRastrarCarga.Properties)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private DevExpress.XtraEditors.PanelControl panelControl1;
        private DevExpress.XtraGrid.GridControl gridControl1;
        private System.Windows.Forms.BindingSource dBalanceteBindingSource;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridView bandedGridView1;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn colOrdem;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn colData;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn colDescricao;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn colDescBalancete;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn colPLA;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn colValor;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn colTipo;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn colCTL;
        private DevExpress.XtraEditors.SimpleButton simpleButton1;
        private DevExpress.XtraEditors.Repository.RepositoryItemCalcEdit repositoryItemCalcEdit1;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn colCCT;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn colCCDConsolidado;
        private DevExpress.XtraEditors.Repository.RepositoryItemImageComboBox repositoryItemImageComboBox1;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn colCCDTipoLancamento;
        private DevExpress.XtraEditors.Repository.RepositoryItemImageComboBox repositoryItemImageComboBox2;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn colCCD;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn colCCDValor;
        private DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit repositoryItemLookUpPLA;
        private System.Windows.Forms.BindingSource pLAnocontasBindingSource;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn colErro;
        private DevExpress.XtraEditors.Repository.RepositoryItemMemoExEdit repositoryItemMemoExEdit1;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn bandedGridColumn1;
        private DevExpress.XtraEditors.Repository.RepositoryItemButtonEdit repositoryItemButtonEdit1;
        private DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit repositoryItemLookUpEditCTL;
        private System.Windows.Forms.BindingSource conTasLogicasBindingSource;
        private DevExpress.Utils.ImageCollection imageCollection1;
        private dllImpresso.Botoes.cBotaoImpBol cBotaoImpBol1;
        private DevExpress.XtraEditors.SimpleButton simpleButton3;
        private DevExpress.XtraEditors.SimpleButton simpleButton4;
        private DevExpress.XtraEditors.SimpleButton simpleButton6;
        private DevExpress.XtraEditors.Repository.RepositoryItemImageComboBox IMGTipos;
        private DevExpress.Utils.ImageCollection imageCollection2;
        private DevExpress.XtraEditors.CheckEdit chDebitosCreditos;
        private DevExpress.XtraEditors.CheckEdit chDetalhamentoCreditos;
        private DevExpress.XtraEditors.CheckEdit chSaldos;
        private DevExpress.XtraEditors.CheckEdit chCreditosAntigo;
        private DevExpress.XtraTab.XtraTabControl xtraTabControl1;
        private DevExpress.XtraTab.XtraTabPage xtraTabPageDiario;
        private DevExpress.XtraEditors.CheckEdit chPrevisto;
        private DevExpress.XtraEditors.CheckEdit chInadimplencia;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn bandedGridColumn2;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn colDocumento;
        private DevExpress.XtraTab.XtraTabPage xtraTabPageIndividual;
        private DevExpress.XtraGrid.GridControl gridControl3;
        private System.Windows.Forms.BindingSource tabelaVirtualBindingSource;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView2;
        private DevExpress.XtraGrid.Columns.GridColumn colOrdem1;
        private DevExpress.XtraGrid.Columns.GridColumn colData1;
        private DevExpress.XtraGrid.Columns.GridColumn colDescricao1;
        private DevExpress.XtraGrid.Columns.GridColumn colDescBalancete1;
        private DevExpress.XtraGrid.Columns.GridColumn colValor1;
        private DevExpress.XtraGrid.Columns.GridColumn colPLA1;
        private DevExpress.XtraGrid.Columns.GridColumn colTipo1;
        private DevExpress.XtraGrid.Columns.GridColumn colCTL1;
        private DevExpress.XtraGrid.Columns.GridColumn colCCT1;
        private DevExpress.XtraGrid.Columns.GridColumn colCCDConsolidado1;
        private DevExpress.XtraGrid.Columns.GridColumn colCCDTipoLancamento1;
        private DevExpress.XtraGrid.Columns.GridColumn colCCD1;
        private DevExpress.XtraGrid.Columns.GridColumn colCCDValor1;
        private DevExpress.XtraGrid.Columns.GridColumn colErro1;
        private DevExpress.XtraGrid.Columns.GridColumn colPAG;
        private DevExpress.XtraGrid.Columns.GridColumn colCCT2;
        private DevExpress.XtraGrid.Columns.GridColumn colCTL2;
        private DevExpress.XtraGrid.Columns.GridColumn colMestre;
        private DevExpress.XtraGrid.Columns.GridColumn colValorVenA;
        private DevExpress.XtraGrid.Columns.GridColumn colValorVenD;
        private DevExpress.XtraGrid.Columns.GridColumn colDocumento1;
        private DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit lookPLA;
        private DevExpress.XtraEditors.Repository.RepositoryItemCalcEdit ItemCalcEdit;
        private DevExpress.XtraEditors.Repository.RepositoryItemImageComboBox IMGTipos1;
        private DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit repositoryItemLookUpEditCTL1;
        private DevExpress.XtraEditors.Repository.RepositoryItemMemoExEdit repositoryItemMemoExEdit1A;
        private DevExpress.XtraEditors.CheckEdit chExtratos;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit TextEditCorretor70;
        private DevExpress.XtraEditors.CheckEdit checkTF;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn colCCS;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn colCCSSoma;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn colPAG1;
        private DevExpress.XtraTab.XtraTabPage xtraTabPageCreditos;
        private DevExpress.XtraGrid.GridControl gridControl4;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView3;
        private System.Windows.Forms.BindingSource dCreditosBindingSource;
        private DevExpress.XtraGrid.Columns.GridColumn colDescricao2;
        private DevExpress.XtraGrid.Columns.GridColumn colAtrasado;
        private DevExpress.XtraGrid.Columns.GridColumn colPeriodo;
        private DevExpress.XtraGrid.Columns.GridColumn colAntecipado;
        private DevExpress.XtraGrid.Columns.GridColumn colTotal;
        private DevExpress.XtraGrid.Columns.GridColumn colT1;
        private DevExpress.XtraGrid.Columns.GridColumn colT2;
        private DevExpress.XtraGrid.Columns.GridColumn colT3;
        private DevExpress.XtraGrid.Columns.GridColumn colOrdem2;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView4;
        private DevExpress.XtraGrid.Columns.GridColumn colBloco;
        private DevExpress.XtraGrid.Columns.GridColumn colNumero;
        private DevExpress.XtraGrid.Columns.GridColumn colBoleto;
        private DevExpress.XtraGrid.Columns.GridColumn colCompetencia;
        private DevExpress.XtraGrid.Columns.GridColumn colDescricao3;
        private DevExpress.XtraGrid.Columns.GridColumn colTipo2;
        private DevExpress.XtraGrid.Columns.GridColumn colValor2;
        private DevExpress.XtraEditors.Repository.RepositoryItemCalcEdit CalcEdit_n2;
        private DevExpress.XtraGrid.Columns.GridColumn colN;
        private DevExpress.XtraTab.XtraTabPage xtraTabPageCTL;
        private DevExpress.XtraGrid.GridControl gridControl5;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView5;
        private DevExpress.XtraGrid.Columns.GridColumn colCTL3;
        private DevExpress.XtraGrid.Columns.GridColumn colCTLAtiva;
        private DevExpress.XtraGrid.Columns.GridColumn colCTLTitulo;
        private DevExpress.XtraGrid.Columns.GridColumn colCTLTipo;
        private DevExpress.XtraGrid.Columns.GridColumn colCTL_CCT;
        private DevExpress.XtraGrid.Columns.GridColumn colSALDOI;
        private DevExpress.XtraEditors.Repository.RepositoryItemCalcEdit CalcEditn2;
        private DevExpress.XtraGrid.Columns.GridColumn colSALDOF;
        private DevExpress.XtraGrid.Columns.GridColumn colCREDITOS;
        private DevExpress.XtraGrid.Columns.GridColumn colDEBITOS;
        private DevExpress.XtraGrid.Columns.GridColumn colDesp_Cred;
        private DevExpress.XtraEditors.Repository.RepositoryItemImageComboBox ImageComboBox_D_C;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn colMestre1;
        private DevExpress.XtraTab.XtraTabPage xtraTabPageDebitos;
        private DevExpress.XtraGrid.GridControl gridControl6;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView6;
        private DevExpress.XtraGrid.Columns.GridColumn colGrupo;
        private DevExpress.XtraGrid.Columns.GridColumn colDescricao4;
        private DevExpress.XtraGrid.Columns.GridColumn colValor3;
        private DevExpress.XtraGrid.Columns.GridColumn colOrdem3;
        private DevExpress.XtraTab.XtraTabPage xtraTabPageCCT;
        private DevExpress.XtraGrid.GridControl gridControl7;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView7;
        private DevExpress.XtraGrid.Columns.GridColumn colDescricao5;
        private DevExpress.XtraGrid.Columns.GridColumn colSaldoI1;
        private DevExpress.XtraEditors.Repository.RepositoryItemCalcEdit repositoryItemCalcEdit2;
        private DevExpress.XtraGrid.Columns.GridColumn colSaldoF1;
        private VirBotaoNeon.cBotaoImpNeon cBotaoImp1;
        private DevExpress.XtraEditors.ZoomTrackBarControl zoomTrackBarControl1;
        private DevExpress.XtraEditors.PanelControl panelControl2;
        private DevExpress.XtraGrid.Columns.GridColumn colTRANSFERENCIA;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn colCTL21;
        private DevExpress.XtraEditors.ImageComboBoxEdit StatusBAL;
        private DevExpress.XtraTab.XtraTabPage xtraTabPagePrevisto;
        private DevExpress.XtraGrid.GridControl gridControl8;
        private System.Windows.Forms.BindingSource previstoBindingSource;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView8;
        private DevExpress.XtraGrid.Columns.GridColumn colBOL1;
        private DevExpress.XtraGrid.Columns.GridColumn colBOLTipoCRAI1;
        private DevExpress.XtraGrid.Columns.GridColumn colBOLVencto1;
        private DevExpress.XtraGrid.Columns.GridColumn colBOLPagamento1;
        private DevExpress.XtraGrid.Columns.GridColumn colBOLValorPrevisto;
        private DevExpress.XtraGrid.Columns.GridColumn colBOLValorPago1;
        private DevExpress.XtraGrid.Columns.GridColumn colBOLCompetenciaAno1;
        private DevExpress.XtraGrid.Columns.GridColumn colBOLCompetenciaMes1;
        private DevExpress.XtraGrid.Columns.GridColumn colBOL_CCD1;
        private DevExpress.XtraGrid.Columns.GridColumn colBOL_ARL;
        private DevExpress.XtraGrid.Columns.GridColumn colAPTNumero1;
        private DevExpress.XtraGrid.Columns.GridColumn colBLOCodigo1;
        private DevExpress.XtraGrid.Columns.GridColumn colBLOAPT;
        private DevExpress.XtraGrid.Columns.GridColumn colPI;
        private DevExpress.XtraGrid.Columns.GridColumn colBOLProprietario1;
        private DevExpress.XtraGrid.Columns.GridColumn colValorPrincipal;
        private DevExpress.XtraGrid.Columns.GridColumn colMultas;
        private DevExpress.XtraGrid.Columns.GridColumn colStatus;
        private DevExpress.XtraGrid.Columns.GridColumn colAntecipado1;
        private DevExpress.XtraEditors.Repository.RepositoryItemButtonEdit repositoryItemButtonEdit3;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn2;
        private DevExpress.XtraEditors.Repository.RepositoryItemCalcEdit repositoryItemCalcEdit3;
        private DevExpress.XtraGrid.Columns.GridColumn colPrevisto;
        private DevExpress.XtraGrid.Columns.GridColumn colPLA2;
        private DevExpress.XtraGrid.Columns.GridColumn colPLA3;
        private DevExpress.XtraTab.XtraTabPage xtraTabPagePGF;
        private DevExpress.XtraGrid.GridControl gridControl9;
        private System.Windows.Forms.BindingSource pagamentosBindingSource;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridView bandedGridView2;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn colPGF;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn colPGFDia;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn colPGFDescricao;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn colPGFValor;
        private DevExpress.XtraEditors.Repository.RepositoryItemCalcEdit repositoryItemCalcEdit4;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn colPGFCompetI;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn colPGFCompetF;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn colPGFForma;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn colPGFNominal;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn colPGFRef;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn colPGFProximaCompetencia;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn colFRNFantasia;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn colPGF0;
        private DevExpress.Utils.ImageCollection imageCollectionDC;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn colFRNNome;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridView bandedGridView3;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn colNOA;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn colNOACompet;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn colNOANumero;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn colCHE;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn bandedGridColumn4;
        private DevExpress.XtraEditors.Repository.RepositoryItemButtonEdit BOTNota;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn colCHENumero;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn colCHEStatus;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn colCHEValor;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn colCHEVencimento;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn colBotCHE;
        private DevExpress.XtraEditors.Repository.RepositoryItemButtonEdit BotCHE;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn colSCCData;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn colPAG2;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn colPAGValor;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn colPAGVencimento;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn colBotPGF;
        private DevExpress.XtraEditors.Repository.RepositoryItemButtonEdit BotPGF;
        private DevExpress.XtraEditors.Repository.RepositoryItemImageComboBox repositoryItemImageComboBox3;
        private DevExpress.XtraEditors.PanelControl panelControl3;
        private DevExpress.XtraEditors.PanelControl panelVencido;
        private DevExpress.XtraEditors.LabelControl labelVencido;
        private DevExpress.XtraEditors.PanelControl panelFuturo;
        private DevExpress.XtraEditors.LabelControl labelFuturo;
        private DevExpress.XtraEditors.PanelControl panelAgendado;
        private DevExpress.XtraEditors.LabelControl labelAgendado;
        private DevExpress.XtraEditors.PanelControl panelPago;
        private DevExpress.XtraEditors.LabelControl labelPago;
        private DevExpress.XtraGrid.Views.BandedGrid.GridBand gridBand7;
        private DevExpress.XtraGrid.Views.BandedGrid.GridBand gridBand8;
        private DevExpress.XtraGrid.Views.BandedGrid.GridBand gridBand5;
        private DevExpress.XtraGrid.Views.BandedGrid.GridBand gridBand6;
        private DevExpress.XtraGrid.Views.BandedGrid.GridBand gridBand10;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn colStatus1;
        private DevExpress.XtraEditors.Repository.RepositoryItemImageComboBox repositoryItemImageComboBox4;
        private DevExpress.XtraEditors.PanelControl panelManual;
        private DevExpress.XtraEditors.LabelControl labelManual;
        private DevExpress.XtraGrid.Views.BandedGrid.GridBand gridBand4;
        private DevExpress.XtraGrid.Views.BandedGrid.GridBand gridBandCompet;
        private DevExpress.XtraGrid.Views.BandedGrid.GridBand gridBand9;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn colCHE1;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn colNOA1;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn BotNoa1;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn BotChe1;
        private DevExpress.XtraEditors.Repository.RepositoryItemButtonEdit repositoryItemBotCHE;
        private DevExpress.XtraEditors.Repository.RepositoryItemButtonEdit repositoryItemBotNoa;
        private DevExpress.XtraEditors.Repository.RepositoryItemButtonEdit repositoryItemBotPGF;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn BotPGF1;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn colPGF1;
        private DevExpress.XtraGrid.Columns.GridColumn BotCHE2;
        private DevExpress.XtraEditors.Repository.RepositoryItemButtonEdit repositoryItemBotCHE2;
        private DevExpress.XtraGrid.Columns.GridColumn BotNOA2;
        private DevExpress.XtraEditors.Repository.RepositoryItemButtonEdit repositoryItemBotNOA2;
        private DevExpress.XtraGrid.Columns.GridColumn BotPGF2;
        private DevExpress.XtraEditors.Repository.RepositoryItemButtonEdit repositoryItemBotPGF2;
        private DevExpress.XtraGrid.Columns.GridColumn colSoPrevisto;
        private DevExpress.XtraTab.XtraTabPage xtraTabPageErro;
        private DevExpress.XtraGrid.GridControl gridControl10;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView9;
        private DevExpress.XtraGrid.Columns.GridColumn colTipo3;
        private DevExpress.XtraGrid.Columns.GridColumn colNumero1;
        private DevExpress.XtraGrid.Columns.GridColumn colDescricao6;
        private DevExpress.XtraGrid.Columns.GridColumn colData2;
        private DevExpress.XtraGrid.Columns.GridColumn colOrdem4;
        private DevExpress.XtraEditors.Repository.RepositoryItemImageComboBox repositoryItemErro;
        private DevExpress.XtraEditors.Repository.RepositoryItemMemoExEdit repositoryItemMemoExEdit2;
        private DevExpress.XtraEditors.Repository.RepositoryItemMemoEdit repositoryItemMemoEdit1;
        private DevExpress.XtraGrid.Columns.GridColumn colBotaoBoleto1;
        private DevExpress.XtraEditors.Repository.RepositoryItemButtonEdit repBotaoBoleto1;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn BotPlus;
        private DevExpress.XtraEditors.Repository.RepositoryItemButtonEdit repositoryItemButtonPLUS;
        private DevExpress.XtraGrid.Views.BandedGrid.GridBand gridBand2;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn bandedGridAgrupa;
        private DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit repositoryItemCheckEdit1;
        private DevExpress.XtraGrid.Views.BandedGrid.GridBand gridBand1;
        private DevExpress.XtraGrid.Views.BandedGrid.GridBand gridBand3;
        private DevExpress.XtraGrid.Views.BandedGrid.GridBand gridBandL;
        private DevExpress.XtraGrid.Views.BandedGrid.GridBand gridBandF;
        private DevExpress.XtraGrid.Columns.GridColumn colAgrupar;
        private DevExpress.XtraEditors.Repository.RepositoryItemButtonEdit repositoryDelEst;
        private DevExpress.XtraEditors.Repository.RepositoryItemButtonEdit repositoryNULO;
        private DevExpress.XtraEditors.Repository.RepositoryItemButtonEdit repositoryItemButtonNulo;
        private DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit repositoryAgrupar;
        private DevExpress.XtraEditors.Repository.RepositoryItemButtonEdit repositoryDelEst1;
        private DevExpress.XtraTab.XtraTabControl xtraTabControlCredito;
        private DevExpress.XtraTab.XtraTabPage xtraTabCredResumo;
        private DevExpress.XtraTab.XtraTabPage xtraTabCredGrafico;
        private DevExpress.XtraGrid.GridControl gridControl11;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView10;
        private DevExpress.XtraGrid.Columns.GridColumn colTitulo;
        private DevExpress.XtraGrid.Columns.GridColumn colValor4;
        private DevExpress.XtraGrid.Columns.GridColumn colClaGrafico1;
        private DevExpress.XtraTab.XtraTabPage xtraTabCredDetalhes;
        private DevExpress.XtraCharts.ChartControl chartControl1;
        private DevExpress.XtraGrid.GridControl gridControl2;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView1;
        private DevExpress.XtraGrid.Columns.GridColumn colBOL;
        private DevExpress.XtraGrid.Columns.GridColumn colBOLValorPago;
        private DevExpress.XtraEditors.Repository.RepositoryItemCalcEdit repValor;
        private DevExpress.XtraGrid.Columns.GridColumn colBOL_CCD;
        private DevExpress.XtraGrid.Columns.GridColumn colAPTNumero;
        private DevExpress.XtraGrid.Columns.GridColumn colBLOCodigo;
        private DevExpress.XtraGrid.Columns.GridColumn colBOLCompetenciaAno;
        private DevExpress.XtraGrid.Columns.GridColumn colBOLCompetenciaMes;
        private DevExpress.XtraGrid.Columns.GridColumn colBOLEmissao;
        private DevExpress.XtraGrid.Columns.GridColumn colBOLPagamento;
        private DevExpress.XtraGrid.Columns.GridColumn colBOLProprietario;
        private DevExpress.XtraEditors.Repository.RepositoryItemImageComboBox repPI;
        private DevExpress.XtraGrid.Columns.GridColumn colBOLTipoCRAI;
        private DevExpress.XtraGrid.Columns.GridColumn colBOLVencto;
        private DevExpress.XtraGrid.Columns.GridColumn colBotaoBoleto;
        private DevExpress.XtraEditors.Repository.RepositoryItemButtonEdit repBotaoBoleto;
        private DevExpress.XtraGrid.Columns.GridColumn colcalcGrupo;
        private DevExpress.XtraEditors.Repository.RepositoryItemImageComboBox repGrupos;
        private DevExpress.XtraGrid.Columns.GridColumn colBOL2;
        private DevExpress.XtraGrid.Columns.GridColumn colcalcUnidade;
        private DevExpress.XtraGrid.Columns.GridColumn colClaGrafico;
        private DevExpress.XtraEditors.Repository.RepositoryItemImageComboBox repTipoGrafico;
        private DevExpress.XtraEditors.LookUpEdit lookMBA;
        private System.Windows.Forms.BindingSource modeloBAlanceteBindingSource;
        private DevExpress.XtraEditors.LabelControl labelControl1;
        private DevExpress.XtraEditors.SimpleButton simpleButton2;
        private DevExpress.XtraEditors.CheckEdit cHDicionario;
        private DevExpress.XtraGrid.Columns.GridColumn colValCredLiq;
        private DevExpress.XtraGrid.Columns.GridColumn colSuperGrupo;
        private DevExpress.XtraGrid.Columns.GridColumn colRENTABILIDADE;
        private DevExpress.XtraTab.XtraTabPage xtraTabPageOBS;
        private DevExpress.XtraEditors.PanelControl panelControl4;
        private DevExpress.XtraEditors.RadioGroup radioGroup1;
        private RtfEditor.RTF_Editor rtF_Editor1;
        private DevExpress.XtraEditors.CheckEdit chCapa;
        private DevExpress.XtraEditors.CheckEdit checkEdit1;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn colErro2;
        private DevExpress.XtraEditors.PanelControl panelDesenvolvedor;
        private DevExpress.XtraEditors.SpinEdit CCDRastrarCarga;
        private DevExpress.XtraEditors.LabelControl labelControl2;
        private DevExpress.XtraEditors.SpinEdit CCDRastrearCDB;
        private DevExpress.XtraEditors.LabelControl labelControl3;
        private DevExpress.XtraEditors.SpinEdit CCDRastrarConc;
        private DevExpress.XtraEditors.LabelControl labelControl4;
        private DevExpress.XtraEditors.SpinEdit CCDRastrarAc;
        private DevExpress.XtraEditors.LabelControl labelControl5;
        private DevExpress.XtraEditors.MemoEdit memoEdit1;
        private DevExpress.XtraEditors.SimpleButton simpleButton5;
        private DevExpress.XtraEditors.SpinEdit CCDRastrarVal;
        private DevExpress.XtraEditors.LabelControl labelControl6;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn1;
        private DevExpress.XtraEditors.Repository.RepositoryItemButtonEdit BotDesc;
        private DevExpress.XtraEditors.SimpleButton simpleButton7;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit repositoryTextEdit70;
        private DevExpress.XtraGrid.Columns.GridColumn colnGrupo;
        private DevExpress.XtraGrid.Columns.GridColumn colnSuperGrupo;
        private DevExpress.XtraGrid.Columns.GridColumn colcalSG;
        private DevExpress.XtraGrid.Columns.GridColumn colcalG;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn colDesp_Cred1;
        private DevExpress.XtraEditors.SimpleButton simpleButton8;
        private System.Windows.Forms.FolderBrowserDialog folderBrowserDialog1;
        private DevExpress.XtraGrid.Columns.GridColumn colCCT_CON;
        private DevExpress.XtraGrid.Columns.GridColumn colErro3;
        private DevExpress.XtraEditors.Repository.RepositoryItemMemoExEdit repositoryItemMemoExEdit3;
        private DevExpress.XtraGrid.Columns.GridColumn colCCT3;
        private DevExpress.XtraTab.XtraTabPage xtraTabPageInad;
        private DevExpress.XtraGrid.GridControl gridControl12;
        private System.Windows.Forms.BindingSource inadimpBindingSource;
        private DevExpress.XtraGrid.Views.Grid.GridView gridViewInad;
        private DevExpress.XtraGrid.Columns.GridColumn colBLOAPT1;
        private DevExpress.XtraGrid.Columns.GridColumn colPI1;
        private DevExpress.XtraGrid.Columns.GridColumn colBOLVencto2;
        private DevExpress.XtraGrid.Columns.GridColumn colBOLPagamento2;
        private DevExpress.XtraGrid.Columns.GridColumn colValAnterior;
        private DevExpress.XtraGrid.Columns.GridColumn colValFinal;
        private DevExpress.XtraGrid.Columns.GridColumn colValMes;
        private DevExpress.XtraGrid.Columns.GridColumn colValRec;
        private DevExpress.XtraGrid.Columns.GridColumn colAcordo;
        private DevExpress.XtraGrid.Columns.GridColumn colBOL3;
        private DevExpress.XtraGrid.Columns.GridColumn colBOLTipoCRAI2;
        private DevExpress.XtraEditors.CheckEdit chInadDet;
        private DevExpress.XtraEditors.DropDownButton dropDownButton1;
        private DevExpress.XtraBars.PopupMenu popupComprovantes;
        private DevExpress.XtraBars.BarManager barManager1;
        private DevExpress.XtraBars.BarDockControl barDockControlTop;
        private DevExpress.XtraBars.BarDockControl barDockControlBottom;
        private DevExpress.XtraBars.BarDockControl barDockControlLeft;
        private DevExpress.XtraBars.BarDockControl barDockControlRight;
        private DevExpress.XtraBars.BarButtonItem barButtonItemImprimir;
        private DevExpress.XtraBars.BarButtonItem barButtonItemTela;
        private DevExpress.XtraEditors.SimpleButton BotaoTeste;
        private DevExpress.XtraEditors.CheckEdit chCPMF;
    }
}
