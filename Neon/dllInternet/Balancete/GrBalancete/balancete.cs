﻿/*
22/04/2014 13.2.8.28 LH retirado "-" do campo calcunidade
28/04/2014 13.2.8.34 LH Ajuste no simulador
07/05/2014 13.2.8.43 LH a chave dos CCSs deve ser a CCD + PAG nao CCD porque gera conflito nos cheques que tem várias linhas com o mesmo CCD
13/05/2014 13.2.8.49 LH reativação do XML
21/05/2014 13.2.9.1  LH Balancete Terminado não é mais editável
27/05/2014 13.2.9.4  LH Nova estrura para rentabilidade e transferir arrecadado
15/07/2014 14.1.4.1  LH retirado SomaTotalBoletos no boletos previstos
*/

using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Runtime.Serialization;
using Abstratos;
using AbstratosNeon;
using Balancete.Relatorios.Credito;
using Balancete.Relatorios.Previsto;
using DevExpress.XtraReports.UI;
using Framework;
using Framework.objetosNeon;
using FrameworkProc.datasets;
using Transf;
using CompontesBasicosProc;
using CompontesBasicos;
using VirMSSQL;
using VirEnumeracoes;
using VirEnumeracoesNeon;


namespace Balancete.GrBalancete
{   
    /// <summary>
    /// Objeto para o balancete
    /// </summary>
    [DataContract(Name = "balancete", Namespace = "Balancete.GrBalancete")]
    public class balancete : ABS_balancete
    {
        /// <summary>
        /// Último erro
        /// </summary>
        public Exception UltimoErro = null;
        
        private dllVirEnum.VirEnum virEnumTpDataNoPeriodo;

        /// <summary>
        /// virEnum para o tipo TpDataNoPeriodo
        /// </summary>
        public dllVirEnum.VirEnum VirEnumTpDataNoPeriodo
        {
            get
            {
                if (virEnumTpDataNoPeriodo == null)
                {
                    virEnumTpDataNoPeriodo = new dllVirEnum.VirEnum(typeof(balancete.TpDataNoPeriodo));
                    virEnumTpDataNoPeriodo.GravaNomes(balancete.TpDataNoPeriodo.atrasado, string.Format("Atrasados (Vencidos antes de {0:dd/MM/yyyy})", DataI));
                    virEnumTpDataNoPeriodo.GravaNomes(balancete.TpDataNoPeriodo.antecipado, string.Format("Adiantados (Vencidos após de {0:dd/MM/yyyy})", DataF));
                    virEnumTpDataNoPeriodo.GravaNomes(balancete.TpDataNoPeriodo.periodo, string.Format("No período ({0:dd/MM/yyyy} a {1:dd/MM/yyyy})", DataI, DataF));
                    virEnumTpDataNoPeriodo.GravaNomes(balancete.TpDataNoPeriodo.outrosCreditos, "Outros Créditos");
                    virEnumTpDataNoPeriodo.GravaNomes(balancete.TpDataNoPeriodo.semCredito, "Erro. Crédito não encontrado");
                };
                return virEnumTpDataNoPeriodo;
            }
        }

        /// <summary>
        /// Método estático que recupera um balancete gravado
        /// </summary>
        /// <param name="CON"></param>
        /// <param name="comp"></param>
        /// <param name="Tipo"></param>
        /// <returns></returns>
        public static balancete VirConstrutor(int CON, ABS_Competencia comp, TipoBalancete Tipo)
        {
            balancete Retorno = RecuperaXML(CON, comp, Tipo);
            if (Retorno == null)
                Retorno = new balancete(CON, comp);
            return Retorno;
        }

        internal static DataContractSerializer Serializador()
        {
            return new DataContractSerializer(typeof(balancete), new List<Type>() { typeof(Competencia) });
        }

        internal static balancete RecuperaXML(int CON, ABS_Competencia comp,TipoBalancete Tipo)
        {
            //return null;
            balancete Retorno = null;
            try
            {
                string Arquivo = NomeArquivo(CON, comp, Tipo);
                if(!File.Exists(Arquivo))
                    Arquivo = Arquivo.Replace(string.Format("{0}\\", comp.CompetenciaBind), "");
                if (File.Exists(Arquivo))
                {
                    using (Stream oStream = new FileStream(Arquivo, FileMode.Open))
                    {
                        Retorno = (balancete)Serializador().ReadObject(oStream);
                        oStream.Close();
                    }
                }
                else
                {
                    byte[] dados = WS.RecuperaDOC(Criptografa(), Path.GetFileName(Arquivo), comp.CompetenciaBind.ToString());
                    if (dados != null)
                    {
                        using (MemoryStream oStream = new MemoryStream(dados))
                        {
                            Retorno = (balancete)Serializador().ReadObject(oStream);
                            oStream.Close();
                        }
                    }
                    else
                        return null;
                }
                Retorno.AjustePosRecuperarXML();
                Retorno.SomenteLeitura = true;
                Retorno.SalvoXML = true;
                return Retorno;
            }
            catch(Exception)
            {
                return null;
            }
        }

        private static WSSincBancos.WSSincBancos _WS;

        private static WSSincBancos.WSSincBancos WS
        {
            get
            {
                if (_WS == null)
                {
                    _WS = new WSSincBancos.WSSincBancos();

                    //EMPArquivo = EMP;
                    if (CompontesBasicosProc.FuncoesBasicasProc.EstaEmProducao)
                    {
                        _WS.Url = @"http://177.190.193.218/Neon23/WSSincBancos.asmx";
                        if (Framework.usuarioLogado.UsuarioLogadoNeon.UsuarioLogadoStNEON != null)
                            _WS.Proxy = Framework.usuarioLogado.UsuarioLogadoNeon.UsuarioLogadoStNEON.Proxy;
                    }
                }
                return _WS;
            }
        }

        internal bool TranfereArrecadado(CompontesBasicos.Espera.cEspera ESP)
        {
            bool retorno = false;
            if (ESP != null)
                ESP.AtivaGauge(BoletosRec.Count);
            DateTime prox = DateTime.Now.AddSeconds(3);
            int i = 0;
            foreach (Boletos.Boleto.Boleto Bol in BoletosRec.Values)
            {
                //if (Bol.BOL == 22027748)
                //    System.Console.WriteLine("Ratreado");
                i++;
                if (ESP != null)
                {
                    if (DateTime.Now > prox)
                    {
                        ESP.Gauge(i);
                        prox = DateTime.Now.AddSeconds(3);
                    }
                }
                if (Bol.rowPrincipal.BOLCancelado)
                    continue;
                if (Bol.rowPrincipal.IsBOL_CCDNull())
                    continue;
                if (Bol.TransfereArrecadado())
                    retorno = true;
            }
            return retorno;
        }



        private static string PathXML()
        {
            string Retorno = CompontesBasicosProc.FuncoesBasicasProc.EstaEmProducao ? System.Configuration.ConfigurationManager.AppSettings["Publicacoes_Balancetes"] : @"c:\virtual\balancetes\";
            if ((Retorno == null) || (Retorno == ""))
                Retorno = DSCentral.EMP == 1 ? @"\\neon02\Publicacoes\Balancetes\" : @"\\neon40\Publicacoes\Balancetes\";
            if(!System.IO.Directory.Exists(Retorno))
                Retorno = string.Format(@"{0}\TMP\", System.IO.Path.GetDirectoryName(System.Windows.Forms.Application.ExecutablePath));
            return Retorno;
        }

        /// <summary>
        /// Nome do arquivo XML com o balancete
        /// </summary>
        /// <param name="CON"></param>
        /// <param name="comp"></param>
        /// <param name="Tipo"></param>
        /// <returns></returns>
        internal static string NomeArquivo(int CON, ABS_Competencia comp, TipoBalancete Tipo)
        {
            string Pasta = string.Format("{0}{1}\\", PathXML(), comp.CompetenciaBind);
            if (!System.IO.Directory.Exists(Pasta))
                System.IO.Directory.CreateDirectory(Pasta);
            return string.Format("{0}{1}_{2}_{3}_{4}.xml", Pasta, DSCentral.EMP, CON, comp.CompetenciaBind, (int)Tipo);
        }

        private AgrupadorBalancete.solicitaAgrupamento _AgrupamentoUsarBoleto;
        private AgrupadorBalancete.solicitaAgrupamento _AgrupamentoUsarBalancete;


        internal AgrupadorBalancete.solicitaAgrupamento AgrupamentoUsarBoleto
        {
            get
            {
                if (_AgrupamentoUsarBoleto == null)
                {
                    int? MBA = TableAdapter.ST().BuscaEscalar_int("select CONBoleto_MBA from CONDOMINIOS where CON = @P1", CON);
                    if (MBA.HasValue)
                        _AgrupamentoUsarBoleto = new AgrupadorBalancete.solicitaAgrupamento(AgrupadorBalancete.TipoClassificacaoDesp.CadastroMBA, MBA.Value);
                    else
                        _AgrupamentoUsarBoleto = new AgrupadorBalancete.solicitaAgrupamento(AgrupadorBalancete.TipoClassificacaoDesp.Grupos);
                }
                return _AgrupamentoUsarBoleto;
            }
        }

        internal AgrupadorBalancete.solicitaAgrupamento AgrupamentoUsarBalancete
        {
            get
            {
                if (_AgrupamentoUsarBalancete == null)
                {
                    int? MBA = TableAdapter.ST().BuscaEscalar_int("select CONBalancete_MBA from CONDOMINIOS where CON = @P1", CON);
                    if (MBA.HasValue)
                        _AgrupamentoUsarBalancete = new AgrupadorBalancete.solicitaAgrupamento(AgrupadorBalancete.TipoClassificacaoDesp.CadastroMBA, MBA.Value);
                    else
                        _AgrupamentoUsarBalancete = new AgrupadorBalancete.solicitaAgrupamento(AgrupadorBalancete.TipoClassificacaoDesp.PlanoContas);
                }
                return _AgrupamentoUsarBalancete;
            }
        }

        private ClientWCFFtp.ClientFTP _clientFTP;

        private ClientWCFFtp.ClientFTP clientFTP
        {
            get
            {
                if (_clientFTP == null)
                    _clientFTP = new ClientWCFFtp.ClientFTP(CompontesBasicosProc.FuncoesBasicasProc.EstaEmProducao, Framework.DSCentral.USU, Framework.DSCentral.EMP);
                return _clientFTP;
            }
        }

        /// <summary>
        /// Salva XML
        /// </summary>
        /// <returns></returns>
        public bool SalvarXML()
        {
            CarregarDadosBalancete(AgrupamentoUsarBoleto, false);
            string NArquivo = NomeArquivo(CON, comp, TipoBalancete.Boleto);
            using (Stream StrArquivo = new FileStream(NArquivo, FileMode.Create))
            {
                Serializador().WriteObject(StrArquivo, this);
                StrArquivo.Close();
            }
            clientFTP.PutPasta(NArquivo, comp.CompetenciaBind.ToString());
            //Publicah121.Publicar(NArquivo, comp.CompetenciaBind.ToString());
            CarregarDadosBalancete(AgrupamentoUsarBalancete, false);
            NArquivo = NomeArquivo(CON, comp, TipoBalancete.Balancete);
            using (Stream StrArquivo = new FileStream(NArquivo, FileMode.Create))
            {
                Serializador().WriteObject(StrArquivo, this);
                StrArquivo.Close();
            }
            clientFTP.PutPasta(NArquivo, comp.CompetenciaBind.ToString());
            PreparaXMLSiteGrafico();
            NArquivo = NomeArquivo(CON, comp, TipoBalancete.SiteGrafico);
            using (Stream StrArquivo = new FileStream(NArquivo, FileMode.Create))
            {
                Serializador().WriteObject(StrArquivo, this);
                StrArquivo.Close();
            }
            clientFTP.PutPasta(NArquivo, comp.CompetenciaBind.ToString());
            return true;
        }

        private void PreparaXMLSiteGrafico()
        {
            //Carrega inadimplencia
            if (DInadimplencia.Inadimp.Count == 0)
                CarregaInad();

            //Apaga dados do balancete nao necessarios para o arquivo do site grafico
            dBalancete1.ErrosAvisos.Clear();
            dBalancete1.BalanceteLinKs.Clear();
            dBalancete1.BalanceteLinkDetalhe.Clear();
            dBalancete1.BOletoDetalhe.Clear();
            dBalancete1.BOletoDetalheBODs.Clear();
            dBalancete1.CTLxCCT.Clear();
            dBalancete1.ContaCorrenTe.Clear();
            dBalancete1.ContaCorrenteDetalhe.Clear();
            dBalancete1.ContaCorrenteSubdetalhe.Clear();
            dBalancete1.SaldoContaCorrente.Clear();
            dBalancete1.SaldoContaLogica.Clear();
            dBalancete1.OBsBalancete.Clear();
            dBalancete1.TributosEletronicos.Clear();
            dBalancete1.PAGamentos.Clear();
            dBalancete1.BOLetosRecebidos.Clear();
            dBalancete1.BoletosOriginalAcordo.Clear();
            dBalancete1.Grafico.Clear();
            DCreditos1.Creditos.Clear();
            DCreditos1.Debitos.Clear();
            DCreditos1.ContasFisicas.Clear();
            DCreditos1.Mes.Clear();
            DPrevisto1.Previsto.Clear();
        }

        private void AjustePosRecuperarXML()
        {
            _BALrow = dBalancete1.BALancetes[0];
            DBalancete1.CON = BALrow.BAL_CON;
        }

        /// <summary>
        /// Mostra o balancete publicado no site
        /// </summary>
        /// <param name="CON"></param>
        /// <param name="CONCodigo"></param>
        /// <param name="comp"></param>
        /// <param name="resumido"></param>
        public static bool MostraBalancetePDF(int CON, string CONCodigo, Competencia comp, bool resumido)
        {
            bool chamarArquivoLocal = false;
            comp.CON = CON;
            ComponenteWEB novoWeb = new ComponenteWEB() { MostraEndereco = false, Doc = System.Windows.Forms.DockStyle.Fill };
            string NArquivo = string.Format("{0}_{1}_{2}{3}",
                                             DSCentral.EMP,
                                             CON,
                                             resumido ? "B" : "C",
                                             comp.CompetenciaBind);
            if (WS.ExisteDOC(Criptografa(), NArquivo+".pdf", comp.CompetenciaBind.ToString()))
                NArquivo = string.Format("{0}//{1}",comp.CompetenciaBind,NArquivo);
            else
                if (!WS.ExisteDOC(Criptografa(), NArquivo + ".pdf", ""))
                {
                    NArquivo = GeraNomeArquivo(CON, comp, resumido ? TiposImpressos.PublicadoSite : TiposImpressos.Compeleto);
                    if (File.Exists(NArquivo))
                        chamarArquivoLocal = true;
                    else
                    {
                        NArquivo = NArquivo.Replace(string.Format("{0}\\",comp.CompetenciaBind), "");
                        if (File.Exists(NArquivo))
                            chamarArquivoLocal = true;
                        else
                            return false;
                    }
                }
            string URL = chamarArquivoLocal ? NArquivo : string.Format("www.neonimoveis.com.br/neon23/neononline/pdfdoc.aspx?ARQUIVO={0}",NArquivo);                            
            novoWeb.Navigate(URL);
            novoWeb.Titulo = string.Format("Bal {0} {1}", CONCodigo, comp);
            novoWeb.VirShowModulo(EstadosDosComponentes.JanelasAtivas);
            ((DevExpress.XtraTab.XtraTabPage)novoWeb.Parent).ShowCloseButton = DevExpress.Utils.DefaultBoolean.True;
            return true;
        }



        /// <summary>
        /// Indica que o balancete não pode mais ser editado
        /// </summary>
        internal bool SomenteLeitura = false;

        /// <summary>
        /// Balancet salvo em XML
        /// </summary>
        internal bool SalvoXML = false;

        private static int staticR;
        private dBalancete dBalancete1;
        private dPrevisto dPrevisto1;
        private dCreditos dCreditos_Local;
        private dPeriodicos dPeriodicos_Local;
        private dBalancete.BALancetesRow _BALrow = null;
        private SortedList<int, ContaLogica> _ContasLogicas;
        private SortedList<string, ABS_balancete.TotCredito> _TotCreditos;
        private ABS_balancete.TotPrevXRel _totPrevXRel;
        //private SortedList<GrupoDebito, TotDebito> _TotDebitos;

        bool silencioso;

        /// <summary>
        /// Não usa messageBox
        /// </summary>
        public bool Silencioso
        {
            get { return silencioso; }
            set { silencioso = value; }
        }

        #region Contrato XML
        /// <summary>
        /// Total previsto
        /// </summary>    
        [DataMember]
        public decimal TPrevisto;
        /// <summary>
        /// Total de inadimplência do mês
        /// </summary>
        [DataMember]
        public decimal TInad;
        /// <summary>
        /// Total recebido do mês
        /// </summary>
        [DataMember]
        public decimal TRecebidoMes;
        /// <summary>
        /// Total recebido antecipado
        /// </summary>
        [DataMember]
        public decimal TAntecipado;

        /// <summary>
        /// Rentabilidade total
        /// </summary>
        [DataMember]
        public decimal TRentabilidade;

        #endregion

        #region Propriedades

        /// <summary>
        /// Status do balancete
        /// </summary>
        public override BALStatus Status
        {
            get
            {
                if (!encontrado)
                    return BALStatus.Erro;
                else
                    return (BALStatus)BALrow.BALSTATUS;

            }
        }

        /// <summary>
        /// Competência no formato ABS
        /// </summary>
        public override ABS_Competencia ABS_comp
        {
            get { return comp; }
        }

        /// <summary>
        /// Contas lógicar com saldos
        /// </summary>
        public override SortedList<int, ABS_balancete.ContaLogica> ContasLogicas
        {
            get
            {
                if (_ContasLogicas == null)
                {
                    _ContasLogicas = new SortedList<int, ContaLogica>();
                    foreach (dBalancete.ConTasLogicasRow rowCTL in dBalancete1.ConTasLogicas)
                        if ((rowCTL.CTLAtiva && (rowCTL.CTLTitulo != "")) || (rowCTL.SALDOI != 0) || (rowCTL.SALDOF != 0))
                            _ContasLogicas.Add(rowCTL.CTL, new ContaLogica(rowCTL.CTLTitulo, rowCTL.SALDOI, rowCTL.SALDOF, rowCTL.CREDITOS, rowCTL.DEBITOS, rowCTL.TRANSFERENCIA, rowCTL.RENTABILIDADE));
                }
                return _ContasLogicas;
            }
        }

        /// <summary>
        /// Totais de créditos
        /// </summary>
        public override SortedList<string, ABS_balancete.TotCredito> TotCreditos
        {
            get
            {
                if (_TotCreditos == null)
                {
                    _TotCreditos = new SortedList<string, TotCredito>();                    
                    foreach (dCreditos.CreditosRow rowCRED in DCreditos1.Creditos)
                    {
                        _TotCreditos.Add(rowCRED.Descricao,
                                         new TotCredito(rowCRED.Antecipado,
                                                        rowCRED.Periodo,
                                                        rowCRED.Atrasado,
                                                        rowCRED.IsPrevistoNull() ? 0 : rowCRED.Previsto,
                                                        0,
                                                        rowCRED.Descricao,
                                                        rowCRED.PLA));
                    };
                }
                return _TotCreditos;
            }

        }

        /// <summary>
        /// Previsto X realizado
        /// </summary>
        public override ABS_balancete.TotPrevXRel totPrevXRel
        {
            get
            {
                if (_totPrevXRel == null)
                {
                    _totPrevXRel = new TotPrevXRel() { Previsto = 0, RecPrevisto = 0, RecAnterior = 0 };
                    foreach (dPrevisto.PrevistoRow rowP in dPrevisto1.Previsto)
                    {
                        _totPrevXRel.Previsto += rowP.BOLValorPrevisto;
                        if (!rowP.IsValorPrincipalNull())
                            _totPrevXRel.RecPrevisto += rowP.ValorPrincipal;
                        if (!rowP.IsAntecipadoNull())
                            _totPrevXRel.RecAnterior += rowP.Antecipado;
                    }
                    _totPrevXRel.Inad = _totPrevXRel.Previsto - _totPrevXRel.RecPrevisto - _totPrevXRel.RecAnterior;
                    _totPrevXRel.perInad = (_totPrevXRel.Previsto == 0) ? 0 : (100 * (_totPrevXRel.Inad / _totPrevXRel.Previsto));
                }
                return _totPrevXRel;
            }
        }

        private Dictionary<string, Dictionary<string, ABS_balancete.TotDebito>> _TotDebitosGrupo;



        /// <summary>
        /// Total de despesas - formato novo
        /// </summary>
        public override Dictionary<string, Dictionary<string, ABS_balancete.TotDebito>> TotDebitosGrupo
        {
            get
            {
                if (TpClassUsado != null)
                    if ((TpClassUsado.Tipo != AgrupadorBalancete.TipoClassificacaoDesp.CadastroMBA)
                         &&
                        (TpClassUsado.Tipo != AgrupadorBalancete.TipoClassificacaoDesp.Grupos))
                        return null;
                if (_TotDebitosGrupo == null)
                {
                    _TotDebitosGrupo = new Dictionary<string, Dictionary<string, TotDebito>>();
                    DCreditos1.Debitos.DefaultView.Sort = "Ordem,Descricao";

                    foreach (DataRowView DRV in DCreditos1.Debitos.DefaultView)
                    {
                        dCreditos.DebitosRow rowDeb = (dCreditos.DebitosRow)DRV.Row;
                        if (!_TotDebitosGrupo.ContainsKey(rowDeb.SuperGrupo))
                            _TotDebitosGrupo.Add(rowDeb.SuperGrupo, new Dictionary<string, TotDebito>());
                        TotDebito TotDebitoX;
                        if (_TotDebitosGrupo[rowDeb.SuperGrupo].ContainsKey(rowDeb.Grupo))
                            TotDebitoX = _TotDebitosGrupo[rowDeb.SuperGrupo][rowDeb.Grupo];
                        else
                        {
                            TotDebitoX = new TotDebito();
                            _TotDebitosGrupo[rowDeb.SuperGrupo].Add(rowDeb.Grupo, TotDebitoX);
                        }
                        if (TotDebitoX.Descricao != "")
                        {
                            TotDebitoX.Descricao += Environment.NewLine;
                            TotDebitoX.strValores += Environment.NewLine;
                        };
                        TotDebitoX.Descricao += string.Format("    {0}", rowDeb.Descricao);
                        TotDebitoX.strValores += string.Format("{0:n2}", rowDeb.Valor);
                        TotDebitoX.Desc_Valor.Add(rowDeb.Descricao, rowDeb.Valor);
                        TotDebitoX.Total += rowDeb.Valor;
                    }
                }
                return _TotDebitosGrupo;
            }
        }


        /*
        /// <summary>
        /// Total de despesas
        /// </summary>
        [ObsoleteAttribute("Substituído por TotDebitosGrupo", true)]
        public override SortedList<ABS_balancete.GrupoDebito, ABS_balancete.TotDebito> TotDebitos
        {
            get 
            {
                if (_TotDebitos == null)
                {
                    _TotDebitos = new SortedList<GrupoDebito, TotDebito>();
                    foreach (dCreditos.DebitosRow rowDeb in DCreditos1.Debitos)
                    {
                        ABS_balancete.GrupoDebito Grupo = GrupoDebito.gerais;
                        switch (rowDeb.Ordem)
                        {
                            case "00": 
                            case "0":
                                Grupo = GrupoDebito.gerais;
                                break;
                            case "10": Grupo = GrupoDebito.MaoObra;
                                break;
                            case "30": Grupo = GrupoDebito.Administradora;
                                break;
                            case "20": Grupo = GrupoDebito.Administrativas;
                                break;
                            case "40": Grupo = GrupoDebito.Bancarias;
                                break;
                            case "50": Grupo = GrupoDebito.Honorarios;
                                break;
                        }
                        TotDebito TotDebitoX;
                        if (_TotDebitos.ContainsKey(Grupo))
                            TotDebitoX = _TotDebitos[Grupo];
                        else
                        {
                            TotDebitoX = new TotDebito();                           
                            _TotDebitos.Add(Grupo, TotDebitoX);
                        }
                        if (TotDebitoX.Descricao != "")
                        {
                            TotDebitoX.Descricao += "\r\n";
                            TotDebitoX.strValores += "\r\n";
                        };
                        TotDebitoX.Descricao += string.Format("    {0}", rowDeb.Descricao);
                        TotDebitoX.strValores += string.Format("{0:n2}", rowDeb.Valor);                                                 
                        TotDebitoX.Total += rowDeb.Valor;                        
                    };
                    foreach (TotDebito TotDebitoX in _TotDebitos.Values)
                    {
                        TotDebitoX.strValores += string.Format("\r\n{0:n2}       ",TotDebitoX.Total);
                    }
                }
                return _TotDebitos;
            }
        }
        */

        //private Boletos.Boleto.Boleto BolSimulador;

        /// <summary>
        /// Simula um boleto com o balancete (em tela)
        /// </summary>
        //public override bool simulaBoleto(int? PBO = null, bool ForcarBalancete = false)
        public override bool simulaBoleto(int? PBO = null, bool ForcarBalancete = false, bool ReCarregarDados = true)
        {
            if (!encontrado)
                return false;
            if (ReCarregarDados)
            {
                CarregarDados(true);
                CarregarDadosBalancete();
            }
            //if (BolSimulador == null)
            //{
            Boletos.Boleto.Boleto BolSimulador = new Boletos.Boleto.Boleto(CON, null, 1, 100, DateTime.Today, false, "C", ForcarBalancete);
            if (PBO.HasValue)
                BolSimulador.PBOsimulador = PBO;
            BolSimulador.Balancete = this;
            //}
            //if(Framework.usuarioLogado.UsuarioLogadoNeon.UsuarioLogadoSt.USU == 30)
            //    BolSimulador.Imprimir(dllImpresso.Botoes.Botao.imprimir, false);          
            //else 
            BolSimulador.Imprimir(dllImpresso.Botoes.Botao.tela, false);
            return true;
        }

        /// <summary>
        /// Competência
        /// </summary>
        public Competencia comp
        {
            get { return encontrado ? new Competencia(BALrow.BALCompet, CON, null) : null; }
        }

        /// <summary>
        /// CON
        /// </summary>
        public int CON
        {
            get { return encontrado ? BALrow.BAL_CON : -1; }
        }

        /// <summary>
        /// Data inicial
        /// </summary>
        public override DateTime DataI
        {
            get { return encontrado ? BALrow.BALDataInicio : DateTime.MinValue; }
        }

        /// <summary>
        /// Data Final
        /// </summary>
        public override DateTime DataF
        {
            get { return encontrado ? BALrow.BALdataFim : DateTime.MinValue; }
        }

        /// <summary>
        /// Linha mãe
        /// </summary>
        public dBalancete.BALancetesRow BALrow
        {
            get { return _BALrow; }
            //set => _BALrow = value;
        }

        /// <summary>
        /// Dataset principal com todos os dados
        /// </summary>
        [DataMember]
        public dBalancete DBalancete1
        {
            get { return dBalancete1; }
            set { dBalancete1 = value; }
        }

        /// <summary>
        /// Dataset com a previsão de créditos (boletos com vencimento no período)
        /// </summary>
        [DataMember]
        public dPrevisto DPrevisto1
        {
            get { return dPrevisto1; }
            set { dPrevisto1 = value; }
        }

        /// <summary>
        /// Dataset com os dados de Débitos e Créditos
        /// </summary>
        [DataMember]
        public dCreditos DCreditos1
        {
            get { return dCreditos_Local; }
            set { dCreditos_Local = value; }
        }

        /// <summary>
        /// Dataset da Inadimplência
        /// </summary>
        [DataMember]        
        public Relatorios.Inadimplencia.dInadimplencia DInadimplencia
        {
            get { return dInadimplencia ?? (dInadimplencia = new Relatorios.Inadimplencia.dInadimplencia()); }
            set { dInadimplencia = value; }
        }

        /// <summary>
        /// Carrega a inadimplência
        /// </summary>
        /// <param name="Forcar"></param>
        /// <returns></returns>
        public int CarregaInad(bool Forcar = false)
        {
            if ((DInadimplencia.Inadimp.Count == 0) || Forcar)
            {
                //DInadimplencia.InadimpTableAdapter.Fill(DInadimplencia.Inadimp, CON, DataF, DataI);
                DInadimplencia.Carregar(CON, DataI, DataF);
            }
            return dInadimplencia.Inadimp.Count;
        }

        private Relatorios.Inadimplencia.dInadimplencia dInadimplencia;

        /// <summary>
        /// DataSet Periódicos
        /// </summary>
        public dPeriodicos DPeriodicos1
        {
            get { return dPeriodicos_Local ?? (dPeriodicos_Local = new dPeriodicos()); }
        }

        /// <summary>
        /// Indica se está cadastrado no banco de dados
        /// </summary>
        public override bool encontrado
        {
            get { return BALrow != null; }
        }

        /// <summary>
        /// Dados do condomínio
        /// </summary>
        public FrameworkProc.datasets.dCondominiosAtivos.CONDOMINIOSRow rowCON
        {
            get { return FrameworkProc.datasets.dCondominiosAtivos.dCondominiosAtivosST.CONDOMINIOS.FindByCON(CON); }
        }

        /// <summary>
        /// Transferências lógicas
        /// </summary>
        public SortedList<int, TransfL> TransfLs;

        #endregion

        private void AjustesIniciais()
        {
            dBalancete1 = new dBalancete();
            dPrevisto1 = new dPrevisto();
            dCreditos_Local = new dCreditos();
        }

        #region Construtores

        /// <summary>
        /// Construtor
        /// </summary>
        /// <param name="BAL"></param>
        public balancete(int BAL)
        {
            AjustesIniciais();

            dBalancete1.BALancetesTableAdapter.EmbarcaEmTransST();
            if (dBalancete1.BALancetesTableAdapter.FillByBAL(dBalancete1.BALancetes, BAL) == 1)
                _BALrow = dBalancete1.BALancetes[0];

        }

        /// <summary>
        /// Construtor
        /// </summary>
        /// <param name="CON"></param>
        /// <param name="comp"></param>
        public balancete(int CON, ABS_Competencia comp)
        {
            AjustesIniciais();
            dBalancete1.BALancetesTableAdapter.EmbarcaEmTransST();
            if (dBalancete1.BALancetesTableAdapter.FillByComp(dBalancete1.BALancetes, CON, comp.CompetenciaBind) == 1)
                _BALrow = dBalancete1.BALancetes[0];
        }

        /// <summary>
        /// Construtor
        /// </summary>
        /// <param name="rowExterna"></param>
        public balancete(dBalancete.BALancetesRow rowExterna)
        {
            _BALrow = rowExterna;
            try
            {
                dBalancete1 = (dBalancete)rowExterna.Table.DataSet;
            }
            catch { }
        }
        #endregion

        #region Funções de status

        /// <summary>
        /// Direção
        /// </summary>
        public enum DirecaoStatus
        {
            /// <summary>
            /// 
            /// </summary>
            Avante,
            /// <summary>
            /// 
            /// </summary>
            Retorna
        }

        private void AddObs(string obs)
        {
            BALrow.BALObs = string.Format("{0}{1:dd/MM/yyyy HH:mm} ({2})\r\n    {3}\r\n\r\n",
                                           BALrow.IsBALObsNull() ? "" : BALrow.BALObs + Environment.NewLine,
                                           DateTime.Now,
                                           Framework.usuarioLogado.UsuarioLogadoNeon.UsuarioLogadoStNEON.USUNome,
                                           obs);
        }

        private static byte[] Criptografa()
        {
            string Aberto = string.Format("{0:yyyyMMddHHmmss}{1:000}{2:00000000}<-*W*->", DateTime.Now, DSCentral.EMP, staticR++);
            return VirCrip.VirCripWS.Crip(Aberto);
        }

        /// <summary>
        /// Voltar para privado após a correção do histórioco em 21/12/2015
        /// </summary>
        public void GravaHistorico()
        {
            AgrupadorBalancete.solicitaAgrupamento solicitacao = new AgrupadorBalancete.solicitaAgrupamento(AgrupadorBalancete.TipoClassificacaoDesp.PlanoContas);
            CarregarDadosBalancete(solicitacao, false);
            VirOleDb.TableAdapter TA = new VirOleDb.TableAdapter(VirDB.Bancovirtual.TiposDeBanco.AccessH);
            dHistorico dHistorico1 = new dHistorico();
            foreach (dCreditos.CreditosRow rowC in DCreditos1.Creditos)
            {
                dHistorico.Histórico_LançamentosRow novarow = dHistorico1.Histórico_Lançamentos.NewHistórico_LançamentosRow();
                novarow.Codcon = rowCON.CONCodigo;
                novarow.Crédito = true;
                novarow.Data_de_Lançamento = novarow.Data_de_Leitura = DataI;
                if (rowC.Descricao.Length > 60)
                    novarow.Descrição = rowC.Descricao.Substring(0, 60);
                else
                    novarow.Descrição = rowC.Descricao;
                novarow.Boleto_Gerado = true;
                novarow.Mês_Competência = comp.ToStringN();
                novarow.Tipopag = rowC.PLA;
                novarow.Valor_Pago = (float)rowC.Total;
                dHistorico1.Histórico_Lançamentos.AddHistórico_LançamentosRow(novarow);
            }
            foreach (dCreditos.DebitosRow rowD in DCreditos1.Debitos)
            {
                dHistorico.Histórico_LançamentosRow novarow = dHistorico1.Histórico_Lançamentos.NewHistórico_LançamentosRow();
                novarow.Codcon = rowCON.CONCodigo;
                novarow.Crédito = false;
                novarow.Data_de_Lançamento = novarow.Data_de_Leitura = DataI;
                if (rowD.Descricao.Length > 60)
                    novarow.Descrição = rowD.Descricao.Substring(0, 60);
                else
                    novarow.Descrição = rowD.Descricao;
                novarow.Boleto_Gerado = true;
                novarow.Mês_Competência = comp.ToStringN();
                novarow.Tipopag = rowD.PLA;
                novarow.Valor_Pago = (float)rowD.Valor;
                dHistorico1.Histórico_Lançamentos.AddHistórico_LançamentosRow(novarow);
            };
            dHistorico1.Histórico_LançamentosTableAdapter.Apagar(rowCON.CONCodigo, DataI, DataF);
            dHistorico1.Histórico_LançamentosTableAdapter.Update(dHistorico1.Histórico_Lançamentos);
            for (Competencia compDel = comp.CloneCompet(-24); compDel <= comp.CloneCompet(-12); compDel++)
                dHistorico1.Histórico_LançamentosTableAdapter.ApagaComp(rowCON.CONCodigo, compDel.ToStringN());


        }

        /// <summary>
        /// partes do impresso
        /// </summary>
        public enum ParteImpresso
        {
            /// <summary>
            /// Capa do balancete
            /// </summary>
            Capa,
            /// <summary>
            /// Quadradinhos
            /// </summary>
            Quadradinhos,
            /// <summary>
            /// CreditosDebitos
            /// </summary>
            CreditosDebitos,
            /// <summary>
            /// Saldos
            /// </summary>
            Saldos,
            /// <summary>
            /// Colunas
            /// </summary>
            Colunas,
            /// <summary>
            /// Previsto
            /// </summary>
            Previsto,
            /// <summary>
            /// Inadimplencia
            /// </summary>
            Inadimplencia,
            /// <summary>
            /// Inadimplência aberta
            /// </summary>
            InadDet,
            /// <summary>
            /// Extratos
            /// </summary>
            Extratos,
            /// <summary>
            /// Relatorio antigo (Legado)
            /// </summary>
            CPMF
        }

        /// <summary>
        /// Indica se o balancete está aberto
        /// </summary>
        public bool Aberto
        {
            get { return (Status == BALStatus.Erro) || (Status == BALStatus.NaoIniciado) || (Status == BALStatus.Producao); }
        }

        private void MarcaPreliminar(XtraReport Impresso)
        {
            if (Aberto)
            {
                Impresso.Watermark.Font = new System.Drawing.Font("Verdana", 66F, System.Drawing.FontStyle.Bold);
                Impresso.Watermark.ForeColor = System.Drawing.Color.Gray;
                Impresso.Watermark.Text = "Preliminar";
                Impresso.Watermark.TextTransparency = 95;
                Impresso.Watermark.ShowBehind = false;
            }
        }

        /// <summary>
        /// Retorna a path do pdf com o balancete se estiver disponível
        /// </summary>
        /// <param name="CON"></param>
        /// <param name="_comp"></param>
        /// <param name="CompetenciaBoleto">indica se a competincia fornecida é a do balancete ou do boleto</param>
        /// <returns></returns>
        public static new string ImpressoGravado(int CON, ABS_Competencia _comp, bool CompetenciaBoleto)
        {
            Competencia comp = ((Competencia)_comp).CloneCompet();
            if (CompetenciaBoleto)
            {
                int? CONBalBoleto = TableAdapter.ST().BuscaEscalar_int("select CONBalBoleto from condominios where CON = @P1", CON);
                if (CONBalBoleto.HasValue)
                    comp.Add(CONBalBoleto.Value);
                else
                    return null;
            }
            string nomeArquivo = GeraNomeArquivo(CON, comp, TiposImpressos.Anexo);
            //string nomeArquivo = string.Format(@"{0}{1}_{2}_B{3}.pdf",
            //                                             PathXML(),
            //                                             Framework.DSCentral.EMP,
            //                                             CON,
            //                                             comp.CompetenciaBind);
            if (File.Exists(nomeArquivo))
                return nomeArquivo;
            else
                return null;
        }

        private enum TiposImpressos
        {
            PublicadoSite,
            Compeleto,
            Anexo,
        }

        private static string GeraNomeArquivo(int CON, Competencia comp, TiposImpressos Tipo)
        {
            string Pasta = string.Format(@"{0}{1}", PathXML(), comp.CompetenciaBind);
            if (!Directory.Exists(Pasta))
                Directory.CreateDirectory(Pasta);
            string cod;
            switch (Tipo)
            {
                case TiposImpressos.PublicadoSite:
                    cod = "B";
                    break;
                case TiposImpressos.Compeleto:
                    cod = "C";
                    break;
                case TiposImpressos.Anexo:
                    cod = "D";
                    break;
                default:
                    throw new NotImplementedException(string.Format("Tipo não implementado: {0}", Tipo));
            }
            return string.Format(@"{0}\{1}_{2}_{3}{4}.pdf",
                                                         Pasta,
                                                         DSCentral.EMP,
                                                         CON,
                                                         cod,
                                                         comp.CompetenciaBind);
        }

        /// <summary>
        /// Gera impresso
        /// </summary>
        /// <param name="Apontar"></param>
        /// <param name="PartesImpresso"></param>
        /// <param name="Agrupamento"></param>
        /// <returns></returns>
        public XtraReport GeraImpresso(bool Apontar, AgrupadorBalancete.solicitaAgrupamento Agrupamento, List<ParteImpresso> PartesImpresso)
        {
            if (Agrupamento == null)
                Agrupamento = AgrupamentoUsarBalancete;
            XtraReport ImpGeral = null;
            if (PartesImpresso.Contains(ParteImpresso.Capa))
            {
                Relatorios.Capa.ImpCapa ImpCapa = new Relatorios.Capa.ImpCapa(this);
                if (Apontar)
                    ImpCapa.Apontamento(CON, 27);
                if (ImpGeral == null)
                    MarcaPreliminar(ImpCapa);
                string Obs = GetOBS(enumOBBTipo.Capa);
                if (Obs != null)
                {
                    ImpCapa.xrRichTextCapa.Rtf = Obs;
                    ImpCapa.xrRichTextCapa.Visible = true;
                }
                System.Collections.SortedList DadosCapa = new System.Collections.SortedList();
                DadosCapa.Add("Condominio", rowCON.CONNome);
                DadosCapa.Add("Data I", DataI.ToString("dd/MM/yyyy"));
                DadosCapa.Add("Data F", DataF.ToString("dd/MM/yyyy"));
                DadosCapa.Add("Operador", "Op");
                ImpCapa.CarregarImpresso("Capa Balancete", DadosCapa);
                ImpCapa.CreateDocument();
                if (ImpGeral == null)
                    ImpGeral = ImpCapa;
                else
                    ImpGeral.Pages.AddRange(ImpCapa.Pages);
            }
            if ((PartesImpresso.Contains(ParteImpresso.Quadradinhos)) || (PartesImpresso.Contains(ParteImpresso.CreditosDebitos)))
            {
                ImpCreditos ImpC = new ImpCreditos(this, TpClassUsado);
                ImpC.Quadradinhos.Value = PartesImpresso.Contains(ParteImpresso.Quadradinhos);
                if (Apontar)
                    ImpC.Apontamento(CON, 27);
                MarcaPreliminar(ImpC);
                if (PartesImpresso.Contains(ParteImpresso.CreditosDebitos))
                {

                    //ImpC.parSuperGrupo.Value = testeSuperGrupo;
                    string Obs = GetOBS(enumOBBTipo.Credito);
                    if (Obs != null)
                    {
                        ImpC.xrRichTextCred.Rtf = Obs;
                        ImpC.xrRichTextCred.Visible = true;
                    }
                    Obs = GetOBS(enumOBBTipo.Debito);
                    if (Obs != null)
                    {
                        ImpC.xrRichTextDeb.Rtf = Obs;
                        ImpC.xrRichTextDeb.Visible = true;
                    }
                }
                if (PartesImpresso.Contains(ParteImpresso.Quadradinhos))
                {
                    string Obs = GetOBS(enumOBBTipo.Quadradinhos);
                    if (Obs != null)
                    {
                        ImpC.xrRichTextQuad.Rtf = Obs;
                        ImpC.xrRichTextQuad.Visible = true;
                    }
                }
                ImpC.CreateDocument();
                if (ImpGeral == null)
                    ImpGeral = ImpC;
                else
                    ImpGeral.Pages.AddRange(ImpC.Pages);
            }
            if (PartesImpresso.Contains(ParteImpresso.Saldos))
            {
                ImpSaldos ImpSaldos = new ImpSaldos(this, Agrupamento);
                if (Apontar)
                    ImpSaldos.Apontamento(CON, 27);
                if (ImpGeral == null)
                    MarcaPreliminar(ImpSaldos);
                string Obs = GetOBS(enumOBBTipo.Saldos);
                if (Obs != null)
                {
                    ImpSaldos.xrRichTextSaldos.Rtf = Obs;
                    ImpSaldos.xrRichTextSaldos.Visible = true;
                }
                ImpSaldos.CreateDocument();
                if (ImpGeral == null)
                    ImpGeral = ImpSaldos;
                else
                    ImpGeral.Pages.AddRange(ImpSaldos.Pages);
            }
            if (PartesImpresso.Contains(ParteImpresso.Colunas))
            {
                CarregarDadosBalancete(Agrupamento);
                ImpCredColunas ImpCol = new ImpCredColunas(this) { DataSource = DBalancete1 };
                //Imp2.Pizza.Value = true;
                //Imp2.Prepara(Boletos.Boleto.Impresso.ImpBoletosColunas.Tratadados.completar);
                if (Apontar)
                    ImpCol.Apontamento(CON, 27);
                if (ImpGeral == null)
                    MarcaPreliminar(ImpCol);
                string Obs = GetOBS(enumOBBTipo.Colunas);
                if (Obs != null)
                {
                    ImpCol.xrRichTextCol.Rtf = Obs;
                    ImpCol.xrRichTextCol.Visible = true;
                }
                ImpCol.CreateDocument();
                if (ImpGeral == null)
                    ImpGeral = ImpCol;
                else
                    ImpGeral.Pages.AddRange(ImpCol.Pages);
                /*
                /////////APAGAR
                Boletos.Boleto.Impresso.ImpBoletosColunas Imp2 = GeraRelatorioColunas();
                Imp2.Pizza.Value = true;
                Imp2.Prepara(Boletos.Boleto.Impresso.ImpBoletosColunas.Tratadados.completar);
                if (Apontar)
                    Imp2.Apontamento(CON, 27);
                if (ImpGeral == null)
                    MarcaPreliminar(Imp2);
                Imp2.CreateDocument();
                if (ImpGeral == null)
                    ImpGeral = Imp2;
                else
                    ImpGeral.Pages.AddRange(Imp2.Pages);
                /////////APAGAR
                */
            }

            if (PartesImpresso.Contains(ParteImpresso.Previsto))
            {
                ImpPrevisto ImpPrevisto = new ImpPrevisto(this);
                if (Apontar)
                    ImpPrevisto.Apontamento(CON, 27);
                if (ImpGeral == null)
                    MarcaPreliminar(ImpPrevisto);
                string Obs = GetOBS(enumOBBTipo.Previsto);
                if (Obs != null)
                {
                    ImpPrevisto.xrRichTextPre.Rtf = Obs;
                    ImpPrevisto.xrRichTextPre.Visible = true;
                }
                ImpPrevisto.CreateDocument();
                if (ImpGeral == null)
                    ImpGeral = ImpPrevisto;
                else
                    ImpGeral.Pages.AddRange(ImpPrevisto.Pages);
            }
            if (PartesImpresso.Contains(ParteImpresso.Inadimplencia))
            {
                Relatorios.Inadimplencia.ImpInadimplencia ImpInadimplencia = new Relatorios.Inadimplencia.ImpInadimplencia(this);
                if (Apontar)
                    ImpInadimplencia.Apontamento(CON, 27);
                if (ImpGeral == null)
                    MarcaPreliminar(ImpInadimplencia);
                string Obs = GetOBS(enumOBBTipo.Inadimplencia);
                if (Obs != null)
                {
                    ImpInadimplencia.xrRichTextInad.Rtf = Obs;
                    ImpInadimplencia.xrRichTextInad.Visible = true;
                }
                ImpInadimplencia.CreateDocument();
                if (ImpGeral == null)
                    ImpGeral = ImpInadimplencia;
                else
                    ImpGeral.Pages.AddRange(ImpInadimplencia.Pages);
            }
            if (PartesImpresso.Contains(ParteImpresso.InadDet))
            {
                Relatorios.Inadimplencia.ImpInadDet ImpInadDet = new Relatorios.Inadimplencia.ImpInadDet(this);
                if (Apontar)
                    ImpInadDet.Apontamento(CON, 27);
                if (ImpGeral == null)
                    MarcaPreliminar(ImpInadDet);
                //string Obs = GetOBS(enumOBBTipo.Inadimplencia);
                //if (Obs != null)
                //{
                //    ImpInadimplencia.xrRichTextInad.Rtf = Obs;
                //    ImpInadimplencia.xrRichTextInad.Visible = true;
                //}
                ImpInadDet.CreateDocument();
                if (ImpGeral == null)
                    ImpGeral = ImpInadDet;
                else
                    ImpGeral.Pages.AddRange(ImpInadDet.Pages);
            }
            if (PartesImpresso.Contains(ParteImpresso.Extratos))
            {
                DataTable Contas = TableAdapter.ST().BuscaSQLTabela("SELECT CCT,CCTTitulo FROM ContaCorrenTe WHERE (CCT_CON = @P1)", CON);
                foreach (DataRow Row in Contas.Rows)
                {
                    //dllImpresso.ExtratoBancario.ImpExtratoB Impresso = new dllImpresso.ExtratoBancario.ImpExtratoB();
                    dllImpresso.ExtratoBancario.ImpExtratoBal Impresso = new dllImpresso.ExtratoBancario.ImpExtratoBal(rowCON.CONNome, DataI, DataF, (int)Row["CCT"]);
                    if (!Impresso.ContemDados)
                        continue;
                    //para dar mensagem se o extrato estiver com erro
                    dllbanco.Extrato.cExtrato Extrato1 = new dllbanco.Extrato.cExtrato(DataI, DataF, (int)Row["CCT"], Silencioso);
                    if (Silencioso && (Extrato1.UltimoErro != null))
                    {
                        UltimoErro = Extrato1.UltimoErro;
                        return null;
                    }

                    //int carregados = Impresso.dImpExtratoB1.DadosExtratoBTableAdapter.Fill(Impresso.dImpExtratoB1.DadosExtratoB, DataI, DataF, (int)Row["CCT"]);
                    //if (carregados == 0)
                    //    continue;
                    //Impresso.Nomecondominio.Text = rowCON.CONNome;
                    //Impresso.DataInicial.Text = DataI.ToString("dd/MM/yyyy");
                    //Impresso.DataFinal.Text = DataF.ToString("dd/MM/yyyy");
                    //Impresso.dImpExtratoB1.CalculosDadosExtratoB(true);
                    if (Apontar)
                        Impresso.Apontamento(CON, 27);
                    if (ImpGeral == null)
                        MarcaPreliminar(Impresso);
                    //Impresso.CreateDocument();
                    if (ImpGeral == null)
                        ImpGeral = Impresso;
                    else
                        ImpGeral.Pages.AddRange(Impresso.Pages);
                }
            }
            if (PartesImpresso.Contains(ParteImpresso.CPMF))
            {
                Relatorios.CPMF.impCPMF ImpCPMF = new Relatorios.CPMF.impCPMF(this);
                if (Apontar)
                    ImpCPMF.Apontamento(CON, 27);
                if (ImpGeral == null)
                    MarcaPreliminar(ImpCPMF);
                ImpCPMF.CreateDocument();
                if (ImpGeral == null)
                    ImpGeral = ImpCPMF;
                else
                    ImpGeral.Pages.AddRange(ImpCPMF.Pages);
            }
            return ImpGeral;
        }

        /*
        private dllpublicaBalancete.Publicah12 _Publicah121;

        private dllpublicaBalancete.Publicah12 Publicah121
        {
            get 
            {
                if(_Publicah121 == null)
                    _Publicah121 = new dllpublicaBalancete.Publicah12(FormPrincipalBase.strEmProducao, DSCentral.EMP);
                return _Publicah121;
            }
        }*/

        private bool PublicaImpressos(System.Windows.Forms.Control Chamador, bool Impresso1 = true, bool Impresso2 = true)
        {
            using (CompontesBasicos.Espera.cEspera Esp = new CompontesBasicos.Espera.cEspera(Chamador))
            {
                //AgrupadorBalancete.solicitaAgrupamento solicitacao = new AgrupadorBalancete.solicitaAgrupamento(AgrupadorBalancete.TipoClassificacaoDesp.PlanoContas);
                CarregarDadosBalancete(AgrupamentoUsarBalancete, false);
                XtraReport Impresso;
                string nomeArquivo;
                //dllpublicaBalancete.Publicah12 Publicah121 = new dllpublicaBalancete.Publicah12(FormPrincipalBase.strEmProducao, DSCentral.EMP);
                if (Impresso1)
                {

                    Esp.Espere("Preparando impresso 1");
                    System.Windows.Forms.Application.DoEvents();
                    Impresso = GeraImpresso(false, AgrupamentoUsarBalancete, new List<ParteImpresso>() { ParteImpresso.Quadradinhos, ParteImpresso.CreditosDebitos, ParteImpresso.Saldos });
                    nomeArquivo = GeraNomeArquivo(CON, comp, TiposImpressos.PublicadoSite);
                    Impresso.ExportOptions.Pdf.Compressed = true;
                    Impresso.ExportOptions.Pdf.ImageQuality = DevExpress.XtraPrinting.PdfJpegImageQuality.Lowest;
                    Impresso.ExportToPdf(nomeArquivo);
                    Esp.Espere("Publicando impresso 1");
                    System.Windows.Forms.Application.DoEvents();
                    if (!clientFTP.PutPasta(nomeArquivo, comp.CompetenciaBind.ToString()))
                        return false;
                }
                if (Impresso2)
                {
                    Esp.Espere("Preparando impresso 2");
                    System.Windows.Forms.Application.DoEvents();
                    Impresso = GeraImpresso(false, AgrupamentoUsarBalancete, new List<ParteImpresso>() {
                                                                               ParteImpresso.Capa, 
                                                                               ParteImpresso.Quadradinhos, 
                                                                               ParteImpresso.CreditosDebitos, 
                                                                               ParteImpresso.Saldos,
                                                                               ParteImpresso.Colunas,
                                                                               ParteImpresso.Extratos,
                                                                               ParteImpresso.Inadimplencia,
                                                                               ParteImpresso.Previsto                                                                               
                                                                               });
                    /*
                    nomeArquivo = string.Format(@"{0}\TMP\{1}_{2}_C{3}.pdf",
                                                        System.IO.Path.GetDirectoryName(System.Windows.Forms.Application.ExecutablePath),
                                                        Framework.DSCentral.EMP,
                                                        CON,
                                                        comp.CompetenciaBind);
                    */
                    //nomeArquivo = string.Format("{0}{1}_{2}.pdf", PathXML(), CON, comp.CompetenciaBind);
                    nomeArquivo = GeraNomeArquivo(CON, comp, TiposImpressos.Compeleto);
                    //nomeArquivo = string.Format("{0}{1}_{2}_C{3}.pdf",
                    //                                  PathXML(),
                    //System.IO.Path.GetDirectoryName(System.Windows.Forms.Application.ExecutablePath),
                    //                                Framework.DSCentral.EMP,
                    //                              CON,
                    //                            comp.CompetenciaBind);
                    Impresso.ExportOptions.Pdf.Compressed = true;
                    Impresso.ExportOptions.Pdf.ImageQuality = DevExpress.XtraPrinting.PdfJpegImageQuality.Highest;
                    Impresso.ExportToPdf(nomeArquivo);
                    if (!clientFTP.PutPasta(nomeArquivo, comp.CompetenciaBind.ToString()))
                        return false;
                }

                Esp.Espere("Preparando impresso 3");
                System.Windows.Forms.Application.DoEvents();
                Impresso = GeraImpresso(false, AgrupamentoUsarBalancete, new List<ParteImpresso>() { ParteImpresso.CreditosDebitos, ParteImpresso.Saldos });
                nomeArquivo = GeraNomeArquivo(CON, comp, TiposImpressos.Anexo);
                //nomeArquivo = string.Format(@"{0}{1}_{2}_D{3}.pdf",
                //                                   PathXML(),
                //                                 Framework.DSCentral.EMP,
                //                               CON,
                //                             comp.CompetenciaBind);
                Impresso.ExportOptions.Pdf.Compressed = true;
                Impresso.ExportOptions.Pdf.ImageQuality = DevExpress.XtraPrinting.PdfJpegImageQuality.Lowest;
                Impresso.ExportToPdf(nomeArquivo);
                if (!clientFTP.PutPasta(nomeArquivo, comp.CompetenciaBind.ToString()))
                    return false;
                return true;
            }
        }

        private string PublicarNoSite()
        {
            WSSincBancos.dBAL dPublicar = new WSSincBancos.dBAL();
            WSSincBancos.dBAL.BalanceteTotaisRow rowTotal = dPublicar.BalanceteTotais.NewBalanceteTotaisRow();
            rowTotal.BAL = BALrow.BAL;
            rowTotal.EMP = DSCentral.EMP;
            rowTotal.acc = (decimal)0;
            rowTotal.s1cc = (decimal)0;
            rowTotal.s1fr = (decimal)0;
            rowTotal.s113 = (decimal)0;
            rowTotal.s1po = (decimal)0;
            rowTotal.s1psf = (decimal)0;
            rowTotal.s1acp = (decimal)0;
            rowTotal.s1af = (decimal)0;
            rowTotal.totrec = 0; //Receitas
            rowTotal.totdesp = 0; //TotDebitos                        
            dPublicar.BalanceteTotais.AddBalanceteTotaisRow(rowTotal);

            CarregarDadosBalancete(AgrupamentoUsarBalancete, false);

            //Saldos
            foreach (ABS_balancete.ContaLogica ContaL in ContasLogicas.Values)
            {
                WSSincBancos.dBAL.BalanceteSaldosRow NovaSaldos = dPublicar.BalanceteSaldos.NewBalanceteSaldosRow();
                NovaSaldos.Competencia = comp.CompetenciaBind;
                NovaSaldos.CON = CON;
                NovaSaldos.CTLTitulo = ContaL.CTLNome;
                NovaSaldos.DataI = DataI;
                NovaSaldos.DataF = DataF;
                NovaSaldos.SaldoI = ContaL.SaldoI;
                NovaSaldos.SaldoF = ContaL.SaldoF;
                NovaSaldos.EMP = DSCentral.EMP;
                dPublicar.BalanceteSaldos.AddBalanceteSaldosRow(NovaSaldos);
            };

            //Creditos no quadro 1
            foreach (TotCredito TCred in TotCreditos.Values)
            {
                rowTotal.totrec += TCred.TotRec;
                WSSincBancos.dBAL.BalanceteDetRow rowCred = dPublicar.BalanceteDet.NewBalanceteDetRow();
                rowCred.BAL = BALrow.BAL;
                rowCred.EMP = DSCentral.EMP;
                rowCred.negrito = false;
                rowCred.quadro = 1;
                rowCred.titulo = TCred.Descricao.Length <= 60 ? TCred.Descricao : TCred.Descricao.Substring(0, 60);
                rowCred.valor = TCred.TotRec;
                dPublicar.BalanceteDet.AddBalanceteDetRow(rowCred);
            }

            if (TRentabilidade != 0)
            {
                rowTotal.totrec += TRentabilidade;
                WSSincBancos.dBAL.BalanceteDetRow rowCredRentabilidade = dPublicar.BalanceteDet.NewBalanceteDetRow();
                rowCredRentabilidade.BAL = BALrow.BAL;
                rowCredRentabilidade.EMP = DSCentral.EMP;
                rowCredRentabilidade.negrito = false;
                rowCredRentabilidade.quadro = 1;
                rowCredRentabilidade.titulo = "Rentabilidade";
                rowCredRentabilidade.valor = TRentabilidade;
                dPublicar.BalanceteDet.AddBalanceteDetRow(rowCredRentabilidade);
            }
            

            //Debitos Analítico
            string GrupoAnterior = "";
            WSSincBancos.dBAL.BalanceteDetRow rowTitulo = null;
            DCreditos1.Debitos.DefaultView.Sort = "nSuperGrupo,nGrupo,Ordem";
            foreach (DataRowView DRV in DCreditos1.Debitos.DefaultView)
            {
                dCreditos.DebitosRow rowDeb = (dCreditos.DebitosRow)DRV.Row;
                rowTotal.totdesp += rowDeb.Valor;
                if (rowDeb.Grupo != GrupoAnterior)
                {
                    rowTitulo = dPublicar.BalanceteDet.NewBalanceteDetRow();
                    rowTitulo.BAL = BALrow.BAL;
                    rowTitulo.EMP = DSCentral.EMP;
                    rowTitulo.negrito = true;
                    rowTitulo.quadro = 2;
                    rowTitulo.titulo = rowDeb.Grupo;
                    rowTitulo.valor = 0;
                    dPublicar.BalanceteDet.AddBalanceteDetRow(rowTitulo);
                    GrupoAnterior = rowDeb.Grupo;
                }
                WSSincBancos.dBAL.BalanceteDetRow rowDet = dPublicar.BalanceteDet.NewBalanceteDetRow();
                rowDet.BAL = BALrow.BAL;
                rowDet.EMP = DSCentral.EMP;
                rowDet.negrito = false;
                rowDet.quadro = 2;
                if (rowDeb.Descricao.Length <= 60)
                    rowDet.titulo = rowDeb.Descricao;
                else
                    rowDet.titulo = rowDeb.Descricao.Substring(0, 60);
                rowDet.valor = rowDeb.Valor;
                rowTitulo.valor += rowDeb.Valor;
                dPublicar.BalanceteDet.AddBalanceteDetRow(rowDet);
            }


            /*
            AgrupadorBalancete.solicitaAgrupamento solicitacao1 = new AgrupadorBalancete.solicitaAgrupamento(AgrupadorBalancete.TipoClassificacaoDesp.Grupos);
            CarregarDadosBalancete(solicitacao1, false);
            foreach (DataRowView DRV in DCreditos1.Debitos.DefaultView)
            {
                dCreditos.DebitosRow rowDeb = (dCreditos.DebitosRow)DRV.Row;
                WSSincBancos.dBAL.BalanceteDetRow rowDet = dPublicar.BalanceteDet.NewBalanceteDetRow();
                rowDet.BAL = BALrow.BAL;
                rowDet.EMP = DSCentral.EMP;
                rowDet.negrito = false;
                switch (rowDeb.Ordem)
                {
                    case "00":
                    case "0":
                        //Grupo = GrupoDebito.gerais;
                        rowDet.quadro = 3;
                        //rowTotal.totdg += rowDeb.Valor;                
                        break;
                    case "10":
                        //Grupo = GrupoDebito.MaoObra;
                        rowDet.quadro = 4;
                        //rowTotal.totmo += rowDeb.Valor;
                        break;
                    case "20": //Grupo = GrupoDebito.Administrativas;
                    case "30": //Grupo = GrupoDebito.Administradora;                        
                    case "40": //Grupo = GrupoDebito.Bancarias;
                    case "50": //Grupo = GrupoDebito.Honorarios;
                        rowDet.quadro = 5;
                        //rowTotal.totad += rowDeb.Valor;
                        break;
                    default:
                        rowDet.quadro = 0;
                        break;

                }
                if (rowDeb.Descricao.Length <= 60)
                    rowDet.titulo = rowDeb.Descricao;
                else
                    rowDet.titulo = rowDeb.Descricao.Substring(0, 60);
                rowDet.valor = rowDeb.Valor;
                dPublicar.BalanceteDet.AddBalanceteDetRow(rowDet);
            }
            */

            

            return WS.PublicaBAL(Criptografa(), dPublicar, rowCON.CONCodigo, BALrow.BALCompet);

            /*
            foreach (dsBalLocal.rxrec2Row rowrec2 in dsBalLocal1.rxrec2)
            {
                WSSincBancos.dBAL.det1Row rowdet = dPublicar.det1.Newdet1Row();
                rowdet.titulo = rowrec2.descricao;
                rowdet.quadro = 1;
                rowdet.valor = rowrec2.subtotal;
                rowdet.negrito = false;
                rowdet.Codcon = CODCON;
                rowdet.competencia = Compet.ToStringN();
                dPublicar.det1.Adddet1Row(rowdet);
            };
            SortedList Ordenado = new SortedList();
            int n = 1;
            foreach (dsBalLocal.rxrec1Row row in dsBalLocal1.rxrec1)
                Ordenado.Add(string.Format("{0:000}{1:00000}", row.ordem, n++), row);
            foreach (dsBalLocal.rxrec1Row row in Ordenado.Values)
            {
                WSSincBancos.dBAL.det1Row rowdet = dPublicar.det1.Newdet1Row();
                rowdet.titulo = row.titulo;
                rowdet.quadro = 2;
                rowdet.valor = row.valor;
                rowdet.negrito = row.negrito;
                rowdet.Codcon = CODCON;
                rowdet.competencia = Compet.ToStringN();
                dPublicar.det1.Adddet1Row(rowdet);
            };
            */
            //listadespexp(despesasdg, 3);
            //listadespexp(despesasmo, 4);
            //listadespexp(despesasad, 5);

            /*
            try
            {
                if (modelo == 0)
                {
                    byte[][] Impressos = GeraPublicao();
                    return WS.PublicaBALM(Criptografa(), dPublicar, Impressos[0], Impressos[1], Impressos[2], Impressos[3]);
                }
                else
                {



                    string Arquivo = Arquivo = String.Format("{0}{1:ddMMyyhhmmss}.html", Path, DateTime.Now);
                    if (!System.IO.Directory.Exists(Path))
                        System.IO.Directory.CreateDirectory(Path);
                    GravaTMP(PublicaHTML.Mx((Modelos)(modelo - 1)), Arquivo);
                    return Arquivo;
                }
#if DEBUG
                System.IO.File.WriteAllBytes(@"c:\clientes\neon\pagina\balancetes\m1.html", Impressos[0]);
                System.IO.File.WriteAllBytes(@"c:\clientes\neon\pagina\balancetes\m1.txt", Impressos[0]);
                System.IO.File.WriteAllBytes(@"c:\clientes\neon\pagina\balancetes\m2.html", Impressos[1]);
                System.IO.File.WriteAllBytes(@"c:\clientes\neon\pagina\balancetes\m2.txt", Impressos[1]);
                System.IO.File.WriteAllBytes(@"c:\clientes\neon\pagina\balancetes\m3.html", Impressos[2]);
                System.IO.File.WriteAllBytes(@"c:\clientes\neon\pagina\balancetes\m3.txt", Impressos[2]);
                System.IO.File.WriteAllBytes(@"c:\clientes\neon\pagina\balancetes\m4.html", Impressos[3]);
                System.IO.File.WriteAllBytes(@"c:\clientes\neon\pagina\balancetes\m4.txt", Impressos[3]);
#endif


            }
            catch (Exception e)
            {
                return "Não Publicado\r\n" + e.Message;
            }
            return "";*/
        }

        private string resutadoPub;

        private bool Publicar()
        {
            resutadoPub = PublicarNoSite();
            if (resutadoPub == "ok")
            {
                if (!TableAdapter.ST().BuscaEscalar_int("select CONBALBoleto from condominios where CON = @P1", BALrow.BAL_CON).HasValue)
                {
                    const string comando = "select PBOCompetencia from PrevisaoBOletos where PBO = (select MAX(PBO) from PrevisaoBOletos where PBO_CON = @P1)";
                    int? icompetenciaBoleto = TableAdapter.ST().BuscaEscalar_int(comando, BALrow.BAL_CON);
                    int Delta = -1;
                    if (icompetenciaBoleto.HasValue)
                        Delta = comp.CompetenciaBind - icompetenciaBoleto.Value - 1;
                    TableAdapter.ST().ExecutarSQLNonQuery("update condominios set CONBALBoleto = @P1 where CON = @P2",
                                                                          Delta,
                                                                          BALrow.BAL_CON);
                }
                TableAdapter.ST().ExecutarSQLNonQuery("update condominios set CONCompFec = @P1 where CON = @P2",
                                                                          comp.CompetenciaBind,
                                                                          BALrow.BAL_CON);
                return true;
            }
            else
            {
                //tratar erro
                return false;
            }
        }

        /// <summary>
        /// Salva em XML um balancete já fechado
        /// </summary>
        /// <param name="Chamador"></param>
        /// <returns></returns>
        public bool SalvarRetroativo(System.Windows.Forms.Control Chamador)
        {
            try
            {
                CarregarDados(true);
                if (Validado())
                {
                    //Publicar();
                    return (PublicaImpressos(Chamador, false, true) && SalvarXML());
                }
                else
                    return false;
            }
            catch
            {
                return false;
            }
        }

        /// <summary>
        /// Muda o status do balancete
        /// </summary>
        /// <param name="Dire"></param>
        /// <param name="Chamador"></param>
        /// <returns></returns>
        public bool MudaStatus(DirecaoStatus Dire,System.Windows.Forms.Control Chamador)
        {
            bool retorno = false;
            if (Dire == DirecaoStatus.Avante)
                switch (Status)
                {
                    case BALStatus.Erro:
                        break;
                    case BALStatus.NaoIniciado:
                        BALrow.BALSTATUS = (int)BALStatus.Producao;
                        AddObs("Em produção");
                        retorno = true;
                        break;
                    case BALStatus.Producao:
                        if (!Validado())
                            UltimoErro = new Exception("Erro na validação");                        
                        else if (GravaSaldosFinais())
                        {
                            if (TableAdapter.ST().BuscaEscalar_bool("select CONOculto from CONDOMINIOS where CON = @P1", CON).GetValueOrDefault(false))
                                BALrow.BALSTATUS = (int)BALStatus.FechadoDefinitivo;
                            else
                                BALrow.BALSTATUS = (int)BALStatus.Termindo;
                            if (BALrow.IsBALFimProducaoNull())
                                BALrow.BALFimProducao = DateTime.Now;
                            AddObs("Terminado");
                            retorno = true;
                        }
                        else
                            UltimoErro = new Exception("Erro na gravação de saldos");                        
                        break;                    
                    
                    case BALStatus.ParaPublicar:                                                               
                    case BALStatus.Termindo:
                        if (Validado())
                        {
                            resutadoPub = "??";
                            //System.Windows.Forms.MessageBox.Show("1"); 
                            if (Publicar() && PublicaImpressos(Chamador))
                            {
                                //System.Windows.Forms.MessageBox.Show("1.1"); 
                                BALrow.BALSTATUS = (int)BALStatus.Publicado;
                                AddObs("Publicado");
                                retorno = true;
                            }
                            else
                            {
                                //System.Windows.Forms.MessageBox.Show("1.2");
                                //System.Windows.Forms.MessageBox.Show(resutadoPub);
                                //System.Windows.Forms.Clipboard.SetText(resutadoPub);
                                
                                

                                BALrow.BALSTATUS = (int)BALStatus.ParaPublicar;
                                AddObs("Publicação Solicitada");
                                retorno = true;
                            }
                        }
                        else
                        {
                            //System.Windows.Forms.MessageBox.Show("Balancete contém erros");
                        }
                        break;
                    case BALStatus.Publicado:
                        if (Validado())
                        {
                            using (CompontesBasicos.Espera.cEspera ESP1 = new CompontesBasicos.Espera.cEspera(Chamador))
                            {
                                ESP1.Espere("Gravando Histórico");
                                System.Windows.Forms.Application.DoEvents();
                                GravaHistorico();
                                ESP1.Espere("Gravando Balancete");
                                System.Windows.Forms.Application.DoEvents();
                                ESP1.Espere("Acumulado 12 meses");
                                System.Windows.Forms.Application.DoEvents();
                                cAcumulado cAcumulado1 = new cAcumulado();
                                cAcumulado1.publicah12(CON);
                                System.Windows.Forms.Application.DoEvents();
                                BALrow.BALSTATUS = (int)BALStatus.FechadoDefinitivo;
                                AddObs("Fechado Definitivo");
                                SalvarXML();
                                DBalancete1.BALancetesTableAdapter.Update(BALrow);
                                BALrow.AcceptChanges();                                                                                                
                                retorno = true;
                                SomenteLeitura = true;
                            }
                        }
                        break;
                    case BALStatus.FechadoDefinitivo:
                        BALrow.BALSTATUS = (int)BALStatus.Enviado;
                        AddObs("Enviado para o condomínio");
                        retorno = true;
                        break;
                    default:
                        break;
                }
            else
                switch (Status)
                {
                    case BALStatus.Erro:
                    case BALStatus.NaoIniciado:
                    case BALStatus.Producao:
                        break;
                    case BALStatus.Enviado:
                    case BALStatus.FechadoDefinitivo:
                    case BALStatus.ParaPublicar:
                    case BALStatus.Publicado:
                        if (TableAdapter.ST().BuscaEscalar_bool("select CONOculto from CONDOMINIOS where CON = @P1", CON).GetValueOrDefault(false))
                            BALrow.BALSTATUS = (int)BALStatus.Producao;
                        else
                            BALrow.BALSTATUS = (int)BALStatus.Termindo;
                        AddObs("Reaberto (Terminado)");
                        retorno = true;
                        break;
                    case BALStatus.Termindo:
                        BALrow.BALSTATUS = (int)BALStatus.Producao;
                        AddObs("Reaberto (Produção)");
                        retorno = true;                                            
                        break;
                    default:
                        break;
                }
            if (retorno)
            {
                dBalancete1.BALancetesTableAdapter.Update(BALrow);
                BALrow.AcceptChanges();
                return true;
            }
            else
                return false;
        }

        /// <summary>
        /// Tipos de erro
        /// </summary>
        public enum tiposErroAviso
        {
            /// <summary>
            /// Aviso
            /// </summary>
            Aviso = 0,
            /// <summary>
            /// Erro
            /// </summary>
            Erro = 1
        }

        private dChequesPendentes _dChequesPendentes1;

        private dChequesPendentes dChequesPendentes1
        {
            get
            {
                if (_dChequesPendentes1 == null)
                    _dChequesPendentes1 = new dChequesPendentes();
                return _dChequesPendentes1;
            }
        }

        /// <summary>
        /// Contem ou não erros
        /// </summary>
        /// <returns></returns>
        public bool Validado()
        {            
            int nAvisos = 0;
            int nErros = 0;
            dBalancete1.ErrosAvisos.Clear();
            foreach (dBalancete.TabelaVirtualRow row in dBalancete1.TabelaVirtual)
            {
                if (!row.IsCCDNull())
                    if ((CCDrastrearVal != 0) && (CCDrastrearVal == row.CCD))
                    {
                        rowRastrear = row;
                        System.Windows.Forms.MessageBox.Show("Rastreador");
                    }
                if (((!row.IsErroNull()) && (row.Erro != ""))
                    ||
                    ((!row.IsErroTotalNull()) && (row.ErroTotal != ""))
                    )
                {
                    bool SomenteAviso = true;
                    if ((row.Tipo == (int)TiposLinhas.saldo))
                        SomenteAviso = false;
                    if ((!row.IsErroTotalNull()) && (row.ErroTotal.Contains("(Crítico)")))
                        SomenteAviso = false;
                    dBalancete.ErrosAvisosRow nova = dBalancete1.ErrosAvisos.NewErrosAvisosRow();
                    if (!SomenteAviso)
                    {
                        nova.Tipo = (int)tiposErroAviso.Erro;
                        nova.Numero = ++nErros;
                    }
                    else
                    {
                        nova.Tipo = (int)tiposErroAviso.Aviso;
                        nova.Numero = ++nAvisos;
                    }
                    nova.Data = row.Data;
                    if (!row.IsErroNull())
                        nova.Descricao = row.Erro;
                    else
                        nova.Descricao = row.ErroTotal;
                    nova.Ordem = row.Ordem;
                    DBalancete1.ErrosAvisos.AddErrosAvisosRow(nova);
                }
                if ((row.Tipo == (int)TiposLinhas.DetalhePAG) && (row.IsDescBalanceteParcialNull()))
                {
                    dBalancete.ErrosAvisosRow nova = dBalancete1.ErrosAvisos.NewErrosAvisosRow();
                    nova.Tipo = (int)tiposErroAviso.Erro;
                    nova.Numero = ++nErros;
                    nova.Data = row.Data;
                    nova.Descricao = string.Format("Descrição em branco");
                    nova.Ordem = row.Ordem;
                    DBalancete1.ErrosAvisos.AddErrosAvisosRow(nova);
                }
            }

            
            foreach(dBalancete.BOLetosRecebidosRow rowRec in DBalancete1.BOLetosRecebidos)
                if (rowRec.calcGrupo == (int)TpDataNoPeriodo.semCredito)
                {
                    dBalancete.ErrosAvisosRow nova = dBalancete1.ErrosAvisos.NewErrosAvisosRow();
                    //nova.Tipo = (int)tiposErroAviso.Erro;
                    //nova.Numero = ++nErros;
                    nova.Tipo = (int)tiposErroAviso.Aviso;
                    nova.Numero = ++nAvisos;
                    nova.Data = rowRec.BOLPagamento;
                    nova.Descricao = string.Format("Boleto {0} baixado dia {1:dd/MM/yyyy} sem o crédito no extrato.\r\nVerifique em Créditos/Detalhes",rowRec.BOL,rowRec.BOLPagamento);                    
                    DBalancete1.ErrosAvisos.AddErrosAvisosRow(nova);
                }

            decimal AcCreditos = 0;
            decimal AcDebitos = 0;
            decimal AcSaldoIF = 0;
            decimal AcSaldoFF = 0;
            decimal AcSaldoIL = 0;
            decimal AcSaldoFL = 0;
            decimal AcCreditosL = 0;
            decimal AcDebitosL = 0;
            foreach (dCreditos.CreditosRow rowC in DCreditos1.Creditos)
                AcCreditos += rowC.Total;
            foreach (dCreditos.DebitosRow rowD in DCreditos1.Debitos)
                AcDebitos += rowD.Valor;
            foreach (dCreditos.ContasFisicasRow rowCo in DCreditos1.ContasFisicas)
            {
                AcSaldoIF += rowCo.SaldoI;
                AcSaldoFF += rowCo.SaldoF;
            }
            foreach (dBalancete.ConTasLogicasRow rowL in DBalancete1.ConTasLogicas)
            {
                AcSaldoIL += rowL.SALDOI;
                AcSaldoFL += rowL.SALDOF;
                AcCreditosL += rowL.CREDITOS;
                AcDebitosL += rowL.DEBITOS;
            }
            if (AcSaldoIF + AcCreditos - AcDebitos + TRentabilidade != AcSaldoFF)
            {
                dBalancete.ErrosAvisosRow nova = dBalancete1.ErrosAvisos.NewErrosAvisosRow();
                nova.Tipo = (int)tiposErroAviso.Erro;
                nova.Numero = ++nErros;
                nova.Descricao = string.Format("Erro na Validação Saldo inicial + Créditos - débitos + rentabilidade = Saldo final\r\n{0:n2} + {1:n2} - {2:n2} + {5:n2} <> {3:n2}\r\nErro: {4:n2}", 
                                                                            AcSaldoIF, 
                                                                            AcCreditos, 
                                                                            AcDebitos, 
                                                                            AcSaldoFF,
                                                                            AcSaldoIF + AcCreditos - AcDebitos + TRentabilidade - AcSaldoFF,
                                                                            TRentabilidade);                
                DBalancete1.ErrosAvisos.AddErrosAvisosRow(nova);
            };
            if ((DBalancete1.ConTasLogicas.Count != 0) && (DCreditos1.ContasFisicas.Count != 0))
                if ((AcSaldoIF != AcSaldoIL) || (AcSaldoFF != AcSaldoFL))
                {
                    dBalancete.ErrosAvisosRow nova = dBalancete1.ErrosAvisos.NewErrosAvisosRow();
                    nova.Tipo = (int)tiposErroAviso.Erro;
                    nova.Numero = ++nErros;
                    nova.Descricao = string.Format("Erro na Validação Saldos físicos X Lógicos\r\n{0:n2} X {1:n2} erro: {2:n2}\r\n{3:n2} X {4:n2} erro: {5:n2}",
                                                                                AcSaldoIF,
                                                                                AcSaldoIL,
                                                                                AcSaldoIF - AcSaldoIL,
                                                                                AcSaldoFF,
                                                                                AcSaldoFL,
                                                                                AcSaldoFF - AcSaldoFL);
                    DBalancete1.ErrosAvisos.AddErrosAvisosRow(nova);
                }
            if ((DBalancete1.ConTasLogicas.Count != 0) && (TpClassUsado != null) && (AcCreditosL != AcCreditos))
            {
                dBalancete.ErrosAvisosRow nova = dBalancete1.ErrosAvisos.NewErrosAvisosRow();
                nova.Tipo = (int)tiposErroAviso.Erro;
                nova.Numero = ++nErros;
                nova.Descricao = string.Format("Erro na Validação dos créditos: Créditos X Lógicos\r\n{0:n2} X {1:n2} erro: {2:n2}\r\n",
                                                                            AcCreditos,
                                                                            AcCreditosL,
                                                                            AcCreditos - AcCreditosL);
                DBalancete1.ErrosAvisos.AddErrosAvisosRow(nova);
            }
            if ((DBalancete1.ConTasLogicas.Count != 0) && (TpClassUsado != null) && (AcDebitosL != AcDebitos))
            {
                dBalancete.ErrosAvisosRow nova = dBalancete1.ErrosAvisos.NewErrosAvisosRow();
                nova.Tipo = (int)tiposErroAviso.Erro;
                nova.Numero = ++nErros;
                nova.Descricao = string.Format("Erro na Validação dos Débitos: Débitos X Lógicos\r\n{0:n2} X {1:n2} erro: {2:n2}\r\n",
                                                                            AcDebitos,
                                                                            AcDebitosL,
                                                                            AcDebitos - AcDebitosL);
                DBalancete1.ErrosAvisos.AddErrosAvisosRow(nova);
            }
            int Teste = dChequesPendentes1.ChequePendenteTableAdapter.Fill(dChequesPendentes1.ChequePendente, CON, DataI, DataF);
            foreach (dChequesPendentes.ChequePendenteRow rowCHE in dChequesPendentes1.ChequePendente)
            {
                //No momento só estamos verificando os da folha
                if (rowCHE.IsRHSNull())
                    continue;
                dBalancete.ErrosAvisosRow nova = dBalancete1.ErrosAvisos.NewErrosAvisosRow();
                nova.Tipo = (int)tiposErroAviso.Erro;
                nova.Numero = ++nErros;
                nova.Descricao = string.Format("Cheque da folha de pagamentos pendente. Deve ser reagendado ou cancelado.\r\n  Emissão: {0}\r\n  Favorecido: {1}\r\n  Valor: {2:n2}\r\n Vencimento {3:dd/MM/yyyy}",
                                                                            rowCHE.IsCHEEmissaoNull() ? "Não emitido" : rowCHE.CHEEmissao.ToString("dd/MM/yyyy"),
                                                                            rowCHE.CHEFavorecido,
                                                                            rowCHE.CHEValor,
                                                                            rowCHE.CHEVencimento);
                nova.Data = rowCHE.CHEVencimento;
                DBalancete1.ErrosAvisos.AddErrosAvisosRow(nova);
            }
            return nErros == 0;            
        }

        /// <summary>
        /// Grava os saldos finais para o proximo balancete
        /// </summary>
        /// <returns></returns>
        public bool GravaSaldosFinais()
        {
            if (!Validado())
                return false;
            dBalancete.SaldoContaLogicaDataTable SaldosFinais = DBalancete1.SaldoContaLogicaTableAdapter.GetDataByBAL(CON, comp.CompetenciaBind);
            List<dBalancete.SaldoContaLogicaRow> Apagar = new List<dBalancete.SaldoContaLogicaRow>();
            foreach (dBalancete.SaldoContaLogicaRow row in SaldosFinais)
                Apagar.Add(row);
            foreach (dBalancete.SaldoContaLogicaRow row in Apagar)
                row.Delete();
            foreach (dBalancete.ConTasLogicasRow rowCTL in dBalancete1.ConTasLogicas)
            {
                dBalancete.SaldoContaLogicaRow rowSCL = SaldosFinais.NewSaldoContaLogicaRow();
                //rowSCL = DBalancete1.SaldoContaLogica.NewSaldoContaLogicaRow();
                rowSCL.SCL_BAL = BALrow.BAL;
                rowSCL.SCL_CTL = rowCTL.CTL;
                rowSCL.SCLSaldo = rowCTL.SALDOF;
                //DBalancete1.SaldoContaLogica.AddSaldoContaLogicaRow(rowSCL);
                SaldosFinais.AddSaldoContaLogicaRow(rowSCL);
                //rowSCL.AcceptChanges();
            };
            DBalancete1.SaldoContaLogicaTableAdapter.Update(SaldosFinais);
            return true;
        }

        #endregion

        /*
        /// <summary>
        /// 
        /// </summary>
        [Obsolete("usar objetoneon",true)]
        public enum TipoClassificacaoDesp 
        {
            /// <summary>
            /// 
            /// </summary>
            PlanoContas = 0,
            /// <summary>
            /// 
            /// </summary>
            Grupos = 1,            
            /// <summary>
            /// 
            /// </summary>
            ContasLogicas = 2
        }
        */

        internal class TColunasPGF
        {
            public Competencia comp;
            public DataColumn Texto;
            public DataColumn Status;
            public DataColumn compBalancete;
            public TColunasPGF(Competencia _comp, DataColumn _Texto, DataColumn _Status, DataColumn _compBalancete)
            {
                comp = _comp.CloneCompet();
                Texto = _Texto;
                Status = _Status;
                compBalancete = _compBalancete;
            }
        }

        internal SortedList<int, TColunasPGF> ColunasPGF;

        private Competencia PrimeiracompPGF;
        private Competencia UltimacompPGF;

        internal enum statusPGF
        {
            pago = 0,
            agendado = 1,
            vencido = 2,
            futuro = 3,
            manual = 4
        }

        internal void CriaColunasPGF()
        {
            if (ColunasPGF == null)
            {
                ColunasPGF = new SortedList<int, TColunasPGF>();
                PrimeiracompPGF = comp.CloneCompet(-4);
                UltimacompPGF = comp.CloneCompet(1);
                for (Competencia c = PrimeiracompPGF; c <= UltimacompPGF; c++)
                {
                    string campo = string.Format("PGF_txt_{0}", c.CompetenciaBind);
                    string campostatus = string.Format("sPGF_txt_{0}", c.CompetenciaBind);
                    string camposbalancete = string.Format("bPGF_txt_{0}", c.CompetenciaBind);
                    ColunasPGF.Add(c.CompetenciaBind, new TColunasPGF(c,
                                                                      DPeriodicos1.Pagamentos.Columns.Add(campo, typeof(string)),
                                                                      DPeriodicos1.Pagamentos.Columns.Add(campostatus, typeof(int)),
                                                                      DPeriodicos1.Pagamentos.Columns.Add(camposbalancete, typeof(int))));
                }
            }
        }

        /// <summary>
        /// Carrega os dados dos PGFs
        /// </summary>
        /// <param name="Forcar">forçar o recálculo</param>
        public void CarregaDadosPGF(bool Forcar = false)
        {
            CriaColunasPGF();
            if (Forcar)
            {
                DPeriodicos1.Pagamentos.Clear();
            }
            if (DPeriodicos1.Pagamentos.Count == 0)
            {
                DPeriodicos1.PagamentosTableAdapter.FillByCON(DPeriodicos1.Pagamentos, CON);
                DPeriodicos1.DetPagamentosTableAdapter.Fill(DPeriodicos1.detPagamentos, CON, PrimeiracompPGF.CompetenciaBind, UltimacompPGF.CompetenciaBind);
                foreach (dPeriodicos.PagamentosRow rowPFG in DPeriodicos1.Pagamentos)
                {
                    foreach (dPeriodicos.detPagamentosRow detrow in rowPFG.GetdetPagamentosRows())
                    {
                        TColunasPGF ColunasPGF1 = ColunasPGF[detrow.NOACompet];                        
                        DateTime Vencimento;
                        if (!detrow.IsSCCDataNull())
                        {
                            Vencimento = detrow.SCCData;                                                        
                            detrow.Status = (int)statusPGF.pago;
                        }
                        else if (!detrow.IsCHEVencimentoNull())
                        {
                            Vencimento = detrow.CHEVencimento;
                            CHEStatus CHEStatus = (CHEStatus)detrow.CHEStatus;
                            if (CHEStatus == CHEStatus.BaixaManual)
                                detrow.Status = (int)statusPGF.manual;
                            else
                            {                                
                                if (Vencimento > DataF)
                                    detrow.Status = (int)statusPGF.agendado;
                                else
                                    detrow.Status = (int)statusPGF.vencido;
                            }
                        }
                        else
                        {                            
                            Vencimento = detrow.PAGVencimento;
                            if (Vencimento > DataF)
                                detrow.Status = (int)statusPGF.futuro;
                            else
                                detrow.Status = (int)statusPGF.vencido;
                        }
                        rowPFG[ColunasPGF1.Status] = detrow.Status;
                        Competencia compBalancete = new Competencia(Vencimento, CON);
                        rowPFG[ColunasPGF1.compBalancete] = compBalancete.CompetenciaBind;
                        rowPFG[ColunasPGF1.Texto] = string.Format("{0:dd/MM/yyyy} - bal {1}", Vencimento,compBalancete);
                    }

                    //foreach (TColunasPGF teste in ColunasPGF.Values)
                    //    rowPFG[teste.Texto] = string.Format("PGF {0} - comp {1}", rowPFG.PGF, teste.comp);
                }
            }
        }

        private void CriaColunas()
        {
            if (ColunasCalc == null)
            {
                ColunasCalc = new List<string>();
                //DBalancete1.ContaCorrenTeTableAdapter.Fill(DBalancete1.ContaCorrenTe, CON);
                DBalancete1.ContaCorrenTeTableAdapter.FillByAtivas(DBalancete1.ContaCorrenTe, CON);
                DBalancete1.SaldoContaCorrenteTableAdapter.Fill(DBalancete1.SaldoContaCorrente,DataI,DataF,CON);
                foreach(dBalancete.SaldoContaCorrenteRow rowSCC in DBalancete1.SaldoContaCorrente)
                {
                    if ((rowSCC.calcValorInicial != 0) && (DBalancete1.ContaCorrenTe.FindByCCT(rowSCC.SCC_CCT) == null))
                    {
                        try 
                        {
                            DBalancete1.ContaCorrenTeTableAdapter.ClearBeforeFill = false;
                            DBalancete1.ContaCorrenTeTableAdapter.FillByCCT(DBalancete1.ContaCorrenTe, rowSCC.SCC_CCT);
                        }
                        finally 
                        {
                            DBalancete1.ContaCorrenTeTableAdapter.ClearBeforeFill = true;
                        }
                    }
                }
                
                foreach (dBalancete.ContaCorrenTeRow rowCCT in DBalancete1.ContaCorrenTe)
                {
                    CriaColuna(balancete.TipoConta.Fisica, rowCCT.CCT, rowCCT.CCTTitulo, rowCCT.CCTTitulo);
                }
                DBalancete1.CarregaContasLogicas(CON);
                foreach (dBalancete.ConTasLogicasRow rowCTL in DBalancete1.ConTasLogicas)
                {
                    CriaColuna(balancete.TipoConta.Logica, rowCTL.CTL, rowCTL.CTLTitulo, rowCTL.CTLTitulo);
                }
            }
        }

        private void CriaColuna(balancete.TipoConta Tipo, int Chave, string Titulo1, string Titulo2)
        {
            string strNomeCampo = NomeCampo(Tipo, Chave);
            ColunasCalc.Add(strNomeCampo);
            DBalancete1.TabelaVirtual.Columns.Add(strNomeCampo, typeof(decimal));            
        }

        /// <summary>
        /// Carrega os dados
        /// </summary>
        /// <param name="Completo"></param>
        public override void CarregarDados(bool Completo)
        {
            if (SalvoXML)
                return;
            TpClassUsado = null;
            CriaColunas();
            DBalancete1.TabelaVirtual.Clear();
            CarregaSaldoInicial();
            if (Completo)
            {
                CarregaExtratos(true);
                Acumular();
            }
        }

        /*
        /// <summary>
        /// ClassificaDespesa
        /// </summary>
        /// <param name="TClas"></param>
        /// <param name="PLA"></param>
        /// <returns></returns>
        [Obsolete("usar objetoneon", true)]
        public static string[] ClassificaDespesa(TipoClassificacaoDesp TClas,string PLA)
        {
            string[] Retorno_Ordem_Grupo = new string[2];
            if (TClas == TipoClassificacaoDesp.Grupos)
            {
                int planonumerico = 0;
                int.TryParse(PLA, out planonumerico);
                if (planonumerico == 230000)
                {
                    Retorno_Ordem_Grupo[0] = "50";
                    Retorno_Ordem_Grupo[1] = "Advocatíceas";
                }
                else if ((planonumerico == 240005) || (planonumerico == 240006))
                {
                    Retorno_Ordem_Grupo[0] = "40";
                    Retorno_Ordem_Grupo[1] = "Bancárias";
                }
                else if ((planonumerico < 219999) || (planonumerico >= 250000) || (planonumerico == 230009))
                {
                    Retorno_Ordem_Grupo[0] = "00";
                    Retorno_Ordem_Grupo[1] = "Despesas Gerais";
                }
                else if ((planonumerico >= 220000) && (planonumerico < 230000))
                {
                    Retorno_Ordem_Grupo[0] = "10";
                    Retorno_Ordem_Grupo[1] = "Mão de obra";
                }
                else if ((planonumerico > 230000) && (planonumerico < 230003))
                {
                    Retorno_Ordem_Grupo[0] = "30";
                    Retorno_Ordem_Grupo[1] = "Administradora";
                }
                else //if ((planonumerico > 230003) && (planonumerico < 250000))                                    
                {
                    Retorno_Ordem_Grupo[0] = "20";
                    Retorno_Ordem_Grupo[1] = "Depesas Administrativas";
                }

            }
            else
            {
                string PLAGrupo = string.Format("{0}000", PLA.Substring(0, 3));
                Framework.datasets.dPLAnocontas.PLAnocontasRow PLArow = Framework.datasets.dPLAnocontas.dPLAnocontasSt.PLAnocontas.FindByPLA(PLAGrupo);
                if (PLArow == null)                
                    PLArow = Framework.datasets.dPLAnocontas.dPLAnocontasSt.PLAnocontas.FindByPLA(PLA);                                
                Retorno_Ordem_Grupo[0] = PLAGrupo;
                Retorno_Ordem_Grupo[1] = PLArow == null ? PLA : PLArow.PLADescricaoRes;
            }
            return Retorno_Ordem_Grupo;
        }*/

        private static string ComandoSaldo =
"SELECT     TOP (1) SCCValorF + SCCValorApF as SCCValorFTOTAL, SCCValorF\r\n" +
"FROM         SaldoContaCorrente\r\n" +
"WHERE     (SCC_CCT = @P1) AND (SCCData < @P2)\r\n" +
"ORDER BY SCCData DESC;";

        /// <summary>
        /// 
        /// </summary>
        public dBalancete.TabelaVirtualRow rowI;

        /// <summary>
        /// 
        /// </summary>
        public dBalancete.TabelaVirtualRow rowF;

        /// <summary>
        /// Tipos de linha da tabela virtual
        /// </summary>
        public enum TiposLinhas
        {
            /// <summary>
            /// 
            /// </summary>
            saldo = 0,
            /// <summary>
            /// 
            /// </summary>
            LinhaCCD = 1,
            /// <summary>
            /// 
            /// </summary>
            DetalheBOL = 2,
            /// <summary>
            /// 
            /// </summary>
            DetalhePAG = 3,
            /// <summary>
            /// 
            /// </summary>
            TransferenciaF = 4,
            /// <summary>
            /// 
            /// </summary>
            TransferenciaFC = 8,
            /// <summary>
            /// 
            /// </summary>
            TransferenciaFD = 9,
            /// <summary>
            /// 
            /// </summary>
            TransferenciaL = 5,
            /// <summary>
            /// 
            /// </summary>
            Rentabilidade = 6,
            /// <summary>
            /// 
            /// </summary>
            Deposito = 7,
            /// <summary>
            /// 
            /// </summary>
            despesanoboleto = 10,
            /// <summary>
            /// 
            /// </summary>
            estorno = 11,
            /// <summary>
            /// 
            /// </summary>
            estornoC = 12,
            /// <summary>
            /// 
            /// </summary>
            estornoD = 13,
            /// <summary>
            /// 
            /// </summary>
            Linkestorno = 14,            
            /// <summary>
            /// 
            /// </summary>
            LinkTransferenciaF = 15,
            /// <summary>
            /// 
            /// </summary>
            pagEletronico = 16,
            /// <summary>
            /// 
            /// </summary>
            DetalheBOLManual = 17,
            /// <summary>
            /// 
            /// </summary>
            despesanoboletoManual = 18,
        }

        /// <summary>
        /// TipoConta
        /// </summary>
        public enum TipoConta 
        { 
            /// <summary>
            /// 
            /// </summary>
            Fisica, 
            /// <summary>
            /// 
            /// </summary>
            Logica 
        };

        /// <summary>
        /// Nome do campo
        /// </summary>
        /// <param name="Tipo"></param>
        /// <param name="Chave"></param>
        /// <returns></returns>
        public static string NomeCampo(TipoConta Tipo, int Chave)
        {
            return string.Format("{0}{1}", Tipo == TipoConta.Fisica ? "CCT" : "CTL", Chave);
        }

        /// <summary>
        /// Chave
        /// </summary>
        /// <param name="NomeCampo"></param>
        /// <returns></returns>
        public static int ChaveNomeCampo(string NomeCampo)
        {
            return int.Parse(NomeCampo.Substring(3));
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="NomeCampo"></param>
        /// <returns></returns>
        public static TipoConta TipoNomeCampo(string NomeCampo)
        {
            return NomeCampo.Substring(0, 3) == "CCT" ? TipoConta.Fisica : TipoConta.Logica;
        }

        private enum TipoTotalCCT { gravado = 0 , somado = 1}
        //public SortedList<string, decimal?[]> TotaisSaldo;

        /// <summary>
        /// Carrega Saldo inicial
        /// </summary>
        public void CarregaSaldoInicial()
        {
            decimal TotalF = 0;
            decimal TotalL = 0;
            foreach (dBalancete.ConTasLogicasRow rowCTL in DBalancete1.ConTasLogicas)
                rowCTL.SALDOI = rowCTL.SALDOF = rowCTL.DEBITOS = rowCTL.CREDITOS = 0;
            rowI = DBalancete1.TabelaVirtual.NewTabelaVirtualRow();
            rowI.Mestre = true;
            rowI.Data = DataI.AddDays(-1);
            rowI.Descricao = "SALDO INICIAL";
            rowI.DescBalanceteParcial = "SALDO INICIAL";
            rowI.Ordem = 0;
            rowI.Tipo = (int)TiposLinhas.saldo;
            string strCCT;
            foreach (dBalancete.ContaCorrenTeRow rowCCT in DBalancete1.ContaCorrenTe)
            {
                DataTable DT = TableAdapter.ST().BuscaSQLTabela(ComandoSaldo, rowCCT.CCT, DataI);
                strCCT = NomeCampo(TipoConta.Fisica, rowCCT.CCT);
                if (DT.Rows.Count > 0)
                {
                    if ((rowCCT.CCTTipo == (int)CCTTipo.ContaCorrete_com_Oculta) && (rowCCT.IsCCT_CCTPlusNull()))
                    {
                        rowI[strCCT] = DT.Rows[0]["SCCValorFTOTAL"];
                        TotalF += (decimal)DT.Rows[0]["SCCValorFTOTAL"];
                    }
                    else
                    {
                        rowI[strCCT] = DT.Rows[0]["SCCValorF"];
                        TotalF += (decimal)DT.Rows[0]["SCCValorF"];
                    }
                }
                else
                {
                    rowI[strCCT] = 0;
                }
            }

            DBalancete1.SaldoContaLogicaTableAdapter.FillByBAL(DBalancete1.SaldoContaLogica, CON, comp.CloneCompet(-1).CompetenciaBind);
            foreach (dBalancete.SaldoContaLogicaRow rowSCL in DBalancete1.SaldoContaLogica)
            {
                if (dBalancete1.ConTasLogicas.FindByCTL(rowSCL.SCL_CTL) != null)
                {
                    rowI[NomeCampo(TipoConta.Logica, rowSCL.SCL_CTL)] = rowSCL.SCLSaldo;
                    //na Tabela CTL
                    dBalancete.ConTasLogicasRow rowCTL = DBalancete1.ConTasLogicas.FindByCTL(rowSCL.SCL_CTL);
                    //rowCTL.SALDOI = rowSCL.SCLSaldo;
                    //rowCTL.SALDOF = rowCTL.SALDOI;
                    rowCTL.SALDOI = rowCTL.SALDOF = rowSCL.SCLSaldo;
                    rowCTL.CREDITOS = rowCTL.DEBITOS = 0;
                    TotalL += rowSCL.SCLSaldo;
                }
                else
                {
                    if (rowSCL.SCLSaldo != 0)
                    {
                        throw new Exception(string.Format("Conta Lógica com saldo inativa"));
                    }
                }
            }
            DBalancete1.SaldoContaLogica.AcceptChanges();

            if (TotalF != TotalL)
            {
                rowI.Erro = string.Format("Erro nos saldos:\r\n  Contas Fisicas: {0,9:n2}" +
                                                          "\r\n  Contas Lógicas: {1,9:n2}"
                                                 , TotalF
                                                 , TotalL);
            }

            /*
            foreach (dBalancete.ContaCorrenTeRow rowCCT in DBalancete1.ContaCorrenTe)
            {
                strCCT = NomeCampo(TipoConta.Fisica, rowCCT.CCT);
                decimal?[] Valores = TotaisSaldo[strCCT];
                if (Valores[(int)TipoTotalCCT.gravado].GetValueOrDefault(0) != Valores[(int)TipoTotalCCT.somado].GetValueOrDefault(0))
                    rowI.Erro = string.Format("{0}\r\n  Conta Fisica: {1}"+
                                                 "\r\n    Valor da conta física  : {2,9:n2}"+
                                                 "\r\n    Soma das contas lógicas: {3,9:n2}"
                                                 , rowI.IsErroNull() ? "" : rowI.Erro
                                                 , rowCCT.CCTTitulo
                                                 , Valores[(int)TipoTotalCCT.gravado].GetValueOrDefault(0)
                                                 , Valores[(int)TipoTotalCCT.somado].GetValueOrDefault(0));
            }
            if (!rowI.IsErroNull())
                rowI.Erro = "Erro nos saldos" + rowI.Erro;*/
            DBalancete1.TabelaVirtual.AddTabelaVirtualRow(rowI);



            rowF = DBalancete1.TabelaVirtual.NewTabelaVirtualRow();
            //rowF.CCD = 999999999;
            rowF.Mestre = true;
            rowF.Data = DataF;
            rowF.Descricao = "SALDO FINAL";
            rowF.DescBalanceteParcial = "SALDO FINAL";
            rowF.Ordem = 999999999;
            rowF.Tipo = (int)TiposLinhas.saldo;
            DBalancete1.TabelaVirtual.AddTabelaVirtualRow(rowF);

        }

        private enum debcred { debito, credito, transfL, nula,creditodistribuido }

        /// <summary>
        /// Colunas criadas dinamicamente na tabela TabelaVirtual
        /// </summary>
        [DataMember]
        public List<string> ColunasCalc;

        private Dictionary<int, dBalancete.CTLxCCTDataTable> RateiosPorCCT;       

        internal List<string> ContasLogicasRatear(int CCT)
        {
            if (RateiosPorCCT == null)
                RateiosPorCCT = new Dictionary<int, dBalancete.CTLxCCTDataTable>();
            dBalancete.CTLxCCTDataTable RateioPorCCT;
            if (RateiosPorCCT.ContainsKey(CCT))
                RateioPorCCT = RateiosPorCCT[CCT];
            else
            {
                RateioPorCCT = DBalancete1.CTLxCCTTableAdapter.GetDataRentCCT(CCT);
                RateiosPorCCT.Add(CCT, RateioPorCCT);
            }
            List<string> retorno = new List<string>();            
            foreach (dBalancete.CTLxCCTRow CTLxCCTRow in RateioPorCCT)
                retorno.Add(NomeCampo(TipoConta.Logica, CTLxCCTRow.CTL));
            if(retorno.Count == 0)
                retorno.Add(NomeCampo(TipoConta.Logica, DBalancete1.CTLCAIXA));
            return retorno;
        }

        /// <summary>
        /// Acumula o valor
        /// </summary>
        public void Acumular()
        {
            if (SalvoXML)
                return;
            TpClassUsado = null;

            dBalancete.TabelaVirtualRow rowAnterior = null;
            DBalancete1.TabelaVirtual.DefaultView.Sort = "Data,Ordem";

            foreach (dBalancete.ConTasLogicasRow rowCTL in DBalancete1.ConTasLogicas)
            {
                rowCTL.CREDITOS = rowCTL.DEBITOS = 0;
                rowCTL.SALDOF = rowCTL.SALDOI;
                rowCTL.TRANSFERENCIA = 0;
                rowCTL.RENTABILIDADE = 0;
            }            

            foreach (DataRowView DRV in DBalancete1.TabelaVirtual.DefaultView)
            {
                dBalancete.TabelaVirtualRow row = (dBalancete.TabelaVirtualRow)DRV.Row;

                if (!row.IsCCDNull())
                    if ((CCDrastrearAcumular != 0) && (CCDrastrearAcumular == row.CCD))
                    {
                        rowRastrear = row;
                        System.Windows.Forms.MessageBox.Show("Rastreador");
                    }

                if (row.IsErroNull())
                    row.SetErroTotalNull();
                else
                    row.ErroTotal = row.Erro;                

                if (row.IsTipoNull())
                    continue;

                debcred tipo;

                switch ((balancete.TiposLinhas)row.Tipo)
                {
                    case balancete.TiposLinhas.saldo:
                    case balancete.TiposLinhas.TransferenciaF:
                    case balancete.TiposLinhas.TransferenciaFC:
                    case balancete.TiposLinhas.TransferenciaFD:
                    case balancete.TiposLinhas.estorno:
                    case balancete.TiposLinhas.estornoC:
                    case balancete.TiposLinhas.estornoD:
                    case balancete.TiposLinhas.Linkestorno:
                    case balancete.TiposLinhas.LinkTransferenciaF:
                        tipo = debcred.nula;
                        break;
                    case balancete.TiposLinhas.LinhaCCD:
                        tipo = row.Valor < 0 ? debcred.debito : debcred.credito;
                        break;
                    case balancete.TiposLinhas.DetalheBOL:
                    case balancete.TiposLinhas.Deposito:
                    case balancete.TiposLinhas.DetalheBOLManual:
                        tipo = debcred.credito;
                        break;
                    case balancete.TiposLinhas.DetalhePAG:
                    case balancete.TiposLinhas.despesanoboleto:
                    case balancete.TiposLinhas.despesanoboletoManual:
                    case balancete.TiposLinhas.pagEletronico:
                        tipo = debcred.debito;
                        break;
                    case balancete.TiposLinhas.TransferenciaL:
                        tipo = debcred.transfL;
                        break;
                    case balancete.TiposLinhas.Rentabilidade:
                        //é distribuido em várias contas lógicas
                        tipo = debcred.creditodistribuido;
                        break;
                    default:
                        throw new NotImplementedException(string.Format("erro de tipo :", (balancete.TiposLinhas)row.Tipo));
                }



                dBalancete.ConTasLogicasRow rowCTL1 = null;
                dBalancete.ConTasLogicasRow rowCTL2 = null;
                if (!row.IsCTLNull())
                    rowCTL1 = DBalancete1.ConTasLogicas.FindByCTL(row.CTL);
                if (!row.IsCTL2Null())
                    rowCTL2 = DBalancete1.ConTasLogicas.FindByCTL(row.CTL2);
                switch (tipo)
                {
                    case debcred.nula:
                        row.Desp_Cred = "N";
                        break;
                    case debcred.credito:
                        row.Desp_Cred = "C";
                        if (rowCTL1 == null)                        
                            row.ErroTotal = "Conta lógica desativada (Crítico)";                                                    
                        else
                        {
                            rowCTL1.CREDITOS += row.Valor;
                            rowCTL1.SALDOF += row.Valor;
                        }
                        break;
                    case debcred.debito:
                        row.Desp_Cred = "D";
                        if (rowCTL1 == null)
                            row.ErroTotal = "Conta lógica desativada (Crítico)";
                        else
                        {
                            rowCTL1.DEBITOS -= row.Valor;
                            rowCTL1.SALDOF += row.Valor;
                        }
                        break;
                    case debcred.transfL:
                        row.Desp_Cred = "N";
                        if ((rowCTL1 == null) || (rowCTL2 == null))
                            row.ErroTotal = "Conta lógica desativada (Crítico)";
                        else
                        {
                            rowCTL1.TRANSFERENCIA += row.Valor;
                            rowCTL1.SALDOF += row.Valor;
                            rowCTL2.TRANSFERENCIA -= row.Valor;
                            rowCTL2.SALDOF -= row.Valor;
                        }
                        break;
                    case debcred.creditodistribuido:
                        row.Desp_Cred = "R";
                        break;
                }


                switch ((balancete.TiposLinhas)row.Tipo)
                {
                    default:
                        throw new NotImplementedException(string.Format("erro de tipo :", (balancete.TiposLinhas)row.Tipo));                                                   
                    case balancete.TiposLinhas.saldo:
                        if (rowAnterior == null)
                            rowAnterior = row;
                        else
                        {
                            foreach (string ColunaCalc in ColunasCalc)
                                row[ColunaCalc] = rowAnterior[ColunaCalc];
                        }
                        break;
                    case balancete.TiposLinhas.Rentabilidade:
                        

                        //Prepara                        
                        //string ColunaCaixa = balancete.NomeCampo(balancete.TipoConta.Logica, DBalancete1.CTLCAIXA);
                        decimal Total = 0;
                        int Contas = 0;
                        if (rowCTL1 != null)
                        {
                            //copia dia anterior
                            foreach (string ColunaCalc in ColunasCalc)
                                row[ColunaCalc] = rowAnterior[ColunaCalc];
                            decimal ValorLogicaAnterior = 0;
                            string CampoL = balancete.NomeCampo(balancete.TipoConta.Logica, row.CTL);
                            if (ColunasCalc.Contains(CampoL))
                            {
                                if ((rowAnterior[CampoL] != null) && (rowAnterior[CampoL] != DBNull.Value))
                                    ValorLogicaAnterior = (decimal)rowAnterior[CampoL];
                                row[CampoL] = ValorLogicaAnterior + row.Valor;
                                rowCTL1.RENTABILIDADE += row.Valor;
                                rowCTL1.SALDOF += row.Valor;
                            }
                            else
                                row.ErroTotal = "Conta lógica desativada (Crítico)";
                        }
                        else
                        {
                            decimal MaiorValor = decimal.MinValue;
                            string MaiorCampo = balancete.NomeCampo(balancete.TipoConta.Logica, DBalancete1.CTLCAIXA);
                            foreach (string ColunaCalc in ColunasCalc)
                                row[ColunaCalc] = rowAnterior[ColunaCalc];
                            foreach (string ColunaCalc in ContasLogicasRatear(row.CCT))
                            {
                                //if (balancete.TipoNomeCampo(ColunaCalc) != balancete.TipoConta.Logica)
                                //    continue;
                                //if (ColunaCalc == ColunaCaixa)
                                //    continue;
                                decimal Valor = (rowAnterior[ColunaCalc] == DBNull.Value ? 0 : (decimal)rowAnterior[ColunaCalc]);
                                if (Valor >= MaiorValor)
                                {
                                    MaiorValor = Valor;
                                    MaiorCampo = ColunaCalc;
                                }
                                if (Valor < 0)
                                    continue;
                                Total += Valor;
                                Contas++;
                            }



                            //Logicas
                            decimal ValorDistribuido = 0;
                            decimal parcela;
                            //foreach (string ColunaCalc in ColunasCalc)
                            foreach (string ColunaCalc in ContasLogicasRatear(row.CCT))
                            {
                                if (balancete.TipoNomeCampo(ColunaCalc) != balancete.TipoConta.Logica)
                                    continue;
                                //if (ColunaCalc == ColunaCaixa)
                                //    continue;
                                decimal Valor = (rowAnterior[ColunaCalc] == DBNull.Value ? 0 : (decimal)rowAnterior[ColunaCalc]);
                                if (Valor < 0)
                                    continue;
                                //decimal parcela = Math.Truncate(100 * (row.Valor * Valor / Total)) / 100;
                                if (Total == 0)
                                    parcela = Math.Round(row.Valor / Contas, 2, MidpointRounding.AwayFromZero);
                                else
                                    parcela = Math.Round(row.Valor * Valor / Total, 2, MidpointRounding.AwayFromZero);
                                row[ColunaCalc] = Valor + parcela;
                                ValorDistribuido += parcela;

                                rowCTL1 = DBalancete1.ConTasLogicas.FindByCTL(balancete.ChaveNomeCampo(ColunaCalc));
                                if (rowCTL1 == null)
                                    row.Erro = string.Format("Erro no rateio da rentabilidade. Verifique o cadastro");
                                else
                                {
                                    rowCTL1.RENTABILIDADE += parcela;
                                    rowCTL1.SALDOF += parcela;
                                }

                            }
                            decimal Erro = (row.Valor - ValorDistribuido);
                            if (Erro != 0)
                            {
                                if (MaiorCampo != "")
                                {
                                    rowCTL1 = DBalancete1.ConTasLogicas.FindByCTL(balancete.ChaveNomeCampo(MaiorCampo));
                                    rowCTL1.RENTABILIDADE += Erro;
                                    rowCTL1.SALDOF += Erro;
                                    row[MaiorCampo] = (decimal)row[MaiorCampo] + Erro;
                                }
                                else
                                    row.Erro = "Nenhuma conta lógica cadastrada para crédito";
                            }
                        }
                        //Fisica
                        decimal ValorAnteriorCCT = 0;
                        if (row.Mestre)
                        {
                            string CampoCCT = balancete.NomeCampo(balancete.TipoConta.Fisica, row.CCT);
                            if ((rowAnterior[CampoCCT] != null) && (rowAnterior[CampoCCT] != DBNull.Value))
                                ValorAnteriorCCT = (decimal)rowAnterior[CampoCCT];
                            row[CampoCCT] = ValorAnteriorCCT + row.CCDValor;
                        }
                        rowAnterior = row;
                        break;
                    case balancete.TiposLinhas.estorno:
                        //case balancete.TiposLinhas.Linkestorno:
                        foreach (string ColunaCalc in ColunasCalc)
                            row[ColunaCalc] = rowAnterior[ColunaCalc];
                        break;
                    case balancete.TiposLinhas.Linkestorno:
                    case balancete.TiposLinhas.LinhaCCD:
                    case balancete.TiposLinhas.DetalheBOL:
                    case balancete.TiposLinhas.DetalheBOLManual:
                    case balancete.TiposLinhas.DetalhePAG:
                    case balancete.TiposLinhas.pagEletronico:
                    case balancete.TiposLinhas.TransferenciaF:
                    case balancete.TiposLinhas.TransferenciaFC:
                    case balancete.TiposLinhas.TransferenciaFD:
                    case balancete.TiposLinhas.TransferenciaL:
                    case balancete.TiposLinhas.Deposito:
                    case balancete.TiposLinhas.despesanoboleto:
                    case balancete.TiposLinhas.despesanoboletoManual:
                    case balancete.TiposLinhas.LinkTransferenciaF:
                    case balancete.TiposLinhas.estornoC:
                    case balancete.TiposLinhas.estornoD:
                        //copia dia anterior
                        foreach (string ColunaCalc in ColunasCalc)
                            row[ColunaCalc] = rowAnterior[ColunaCalc];
                        //saldo conta Fisica - no caso de tranferencia fisica fazemos para cada linha e nao pelo total (mestre) porque podemos ter créditos em contas diferentes
                        if ((balancete.TiposLinhas)row.Tipo == balancete.TiposLinhas.TransferenciaF)
                        {                            
                            if (!row.IsCCTNull())
                            {
                                if (row.IsCCT2Null() || (row.CCT != row.CCT2))
                                {
                                    decimal ValorAnterior = 0;
                                    string Campo = balancete.NomeCampo(balancete.TipoConta.Fisica, row.CCT);
                                    if ((rowAnterior[Campo] != null) && (rowAnterior[Campo] != DBNull.Value))
                                        ValorAnterior = (decimal)rowAnterior[Campo];
                                    row[Campo] = ValorAnterior + row.Valor;

                                    if (!row.IsCCT2Null())
                                    {
                                        Campo = balancete.NomeCampo(balancete.TipoConta.Fisica, row.CCT2);
                                        if ((rowAnterior[Campo] != null) && (rowAnterior[Campo] != DBNull.Value))
                                            ValorAnterior = (decimal)rowAnterior[Campo];
                                        row[Campo] = ValorAnterior - row.Valor;
                                    }
                                }
                            }
                        }
                        else if ((balancete.TiposLinhas)row.Tipo != balancete.TiposLinhas.TransferenciaL)
                        {
                            if ((row.Mestre) && (!row.IsCCTNull()))                            
                            {
                                if (row.IsCCT2Null() || (row.CCT != row.CCT2))
                                {
                                    decimal ValorAnterior = 0;
                                    string Campo = balancete.NomeCampo(balancete.TipoConta.Fisica, row.CCT);
                                    if ((rowAnterior[Campo] != null) && (rowAnterior[Campo] != DBNull.Value))
                                        ValorAnterior = (decimal)rowAnterior[Campo];
                                    row[Campo] = ValorAnterior + row.CCDValor;

                                    if (!row.IsCCT2Null())
                                    {
                                        Campo = balancete.NomeCampo(balancete.TipoConta.Fisica, row.CCT2);
                                        if ((rowAnterior[Campo] != null) && (rowAnterior[Campo] != DBNull.Value))
                                            ValorAnterior = (decimal)rowAnterior[Campo];
                                        row[Campo] = ValorAnterior - row.CCDValor;
                                    }
                                }
                            }
                        }
                        //saldo logica
                        if (((balancete.TiposLinhas)row.Tipo != balancete.TiposLinhas.TransferenciaF)
                             &&
                              ((balancete.TiposLinhas)row.Tipo != balancete.TiposLinhas.TransferenciaFC)
                             &&
                              ((balancete.TiposLinhas)row.Tipo != balancete.TiposLinhas.TransferenciaFD)
                             &&
                              ((balancete.TiposLinhas)row.Tipo != balancete.TiposLinhas.LinkTransferenciaF)
                            &&
                              ((balancete.TiposLinhas)row.Tipo != balancete.TiposLinhas.Linkestorno)
                           )
                        {
                            decimal ValorLogicaAnterior = 0;
                            string CampoL = balancete.NomeCampo(balancete.TipoConta.Logica, row.CTL);
                            if (ColunasCalc.Contains(CampoL))
                            {
                                if ((rowAnterior[CampoL] != null) && (rowAnterior[CampoL] != DBNull.Value))
                                    ValorLogicaAnterior = (decimal)rowAnterior[CampoL];
                                row[CampoL] = ValorLogicaAnterior + row.Valor;
                            }
                            else
                                row.ErroTotal = "Conta lógica desativada (Crítico)";
                            //saldo logica2
                            if (!row.IsCTL2Null())
                            {
                                ValorLogicaAnterior = 0;
                                CampoL = balancete.NomeCampo(balancete.TipoConta.Logica, row.CTL2);
                                if (ColunasCalc.Contains(CampoL))
                                {
                                    if ((rowAnterior[CampoL] != null) && (rowAnterior[CampoL] != DBNull.Value))
                                        ValorLogicaAnterior = (decimal)rowAnterior[CampoL];
                                    row[CampoL] = ValorLogicaAnterior - row.Valor;
                                }
                                else
                                    row.ErroTotal = "Conta lógica desativada (Crítico)";
                            }
                        }
                        rowAnterior = row;
                        //saldo parcial da conta logica por conta fisica
                        //dBalancete.ConTasLogicasRow rowCTL = balancete1.DBalancete1.ConTasLogicas.FindByCTL(row.CTL);
                        //string CampoSaldo = balancete.NomeCampo(balancete.TipoConta.Fisica, row.CCT) + "F";
                        //decimal SaldoParcial = rowCTL[CampoSaldo] == DBNull.Value ? 0 : (decimal)rowCTL[CampoSaldo];
                        //rowCTL[CampoSaldo] = SaldoParcial + row.CCDValor;
                        //saldo conta logica - redundante


                        if (!row.IsCTL2Null())
                        {
                            //saldo parcial da conta logica 2 por conta fisica
                            //rowCTL = balancete1.DBalancete1.ConTasLogicas.FindByCTL(row.CTL2);
                            //CampoSaldo = balancete.NomeCampo(balancete.TipoConta.Fisica, row.CCT) + "F";
                            //SaldoParcial = rowCTL[CampoSaldo] == DBNull.Value ? 0 : (decimal)rowCTL[CampoSaldo];
                            //rowCTL[CampoSaldo] = SaldoParcial - row.CCDValor;
                            //saldo conta logica - redundante
                            //rowCTL.SALDOF = (rowCTL.IsSALDOFNull() ? 0 : rowCTL.SALDOF) - row.Valor;
                        }
                        break;
                }                                
            }

            foreach (List<dBalancete.TabelaVirtualRow> GrupoCCS in CCSs.Values)
            {
                decimal Acumulado = 0;
                foreach (dBalancete.TabelaVirtualRow rowVirtual in GrupoCCS)
                    Acumulado += rowVirtual.Valor;
                if (Acumulado != GrupoCCS[0].CCSSoma)
                {
                    string erroCCS = string.Format("Erro no Valor (Crítico)\r\n" +
                                                          "   Valor total do item :{0:n2}\r\n" +
                                                          "   Valor acumulado     :{1:n2}\r\n" +
                                                          "   Erro                :{2:n2}\r\n",
                                                          GrupoCCS[0].CCSSoma,
                                                          Acumulado,
                                                          GrupoCCS[0].CCSSoma - Acumulado);
                    foreach (dBalancete.TabelaVirtualRow rowVirtual in GrupoCCS)
                        if (rowVirtual.IsErroNull())
                            rowVirtual.ErroTotal = erroCCS;
                        else
                            rowVirtual.ErroTotal = string.Format("{0}\r\n\r\n{1}",rowVirtual.Erro,erroCCS);
                }
                    
                else
                    foreach (dBalancete.TabelaVirtualRow rowVirtual in GrupoCCS)
                    {
                        if (rowVirtual.IsErroNull())
                            rowVirtual.SetErroTotalNull();
                        else
                            rowVirtual.ErroTotal = rowVirtual.Erro;
                    }
            }

            /*
            foreach (dBalancete.TabelaVirtualRow rowchecaCCS in balancete1.DBalancete1.TabelaVirtual)
            {
                if (!rowchecaCCS.IsCCSNull())
                {
                    if (rowchecaCCS.IsCCSSomaNull())
                        continue;
                    if (rowchecaCCS.CCSSoma != AcumulaCCS[ChaveMaeCCS(rowchecaCCS)])
                        rowchecaCCS.Erro = string.Format("Erro no Valor\r\n" +
                                                          "   Valor total do item :{0:n2}\r\n" +
                                                          "   Valor acumulado     :{1:n2}\r\n" +
                                                          "   Erro                :{2:n2}\r\n",
                                                          rowchecaCCS.CCSSoma,
                                                          AcumulaCCS[ChaveMaeCCS(rowchecaCCS)],
                                                          rowchecaCCS.CCSSoma - AcumulaCCS[ChaveMaeCCS(rowchecaCCS)]);
                    else
                        rowchecaCCS.SetErroNull();
                }
            }*/

            DBalancete1.TabelaVirtual.AcceptChanges();
            Validado();
        }

        private int nCCT = 0;

        private int nSeq = 0;

        private int nSeqTL = 0;
        
        private int Ordem(DateTime Data,bool TL = false)
        {
            //Mascara
            //DDDCCCSSS
            //DDD = dia CCC = Numero da Conta (CCT) SSS = sequencial
            int Dia = (Data - DataI).Days;
            if(TL)
                return Dia * 10000000 + 998 * 10000 + 10 * nSeqTL++;
            else
                return Dia * 10000000 + nCCT * 10000 + 10 * nSeq++;
        }

        /// <summary>
        /// 
        /// </summary>
        public SortedList<decimal, List<int>> BODsLigados;

        //private int CTLCAIXA;
        //private int CTLFUNDO;

        /// <summary>
        /// CarregaSaldosfisicosBAL
        /// </summary>
        /// <param name="dCreditos1"></param>
        public void CarregaSaldosfisicosBAL(dCreditos dCreditos1)
        {
            foreach(dBalancete.ContaCorrenTeRow rowCCT in dBalancete1.ContaCorrenTe)
            {
                string strCCT = NomeCampo(TipoConta.Fisica, rowCCT.CCT); 
                if ((!rowCCT.IsCCTAtivaNull() && rowCCT.CCTAtiva) || (rowCCT.GetSaldoContaCorrenteRows().Length > 0))
                    dCreditos1.ContasFisicas.AddContasFisicasRow(rowCCT.CCTTitulo, (decimal)rowI[strCCT], (decimal)rowF[strCCT]);
            }            
        }

        private void GeraRelatorioColunasLinha(Boletos.Boleto.Impresso.ImpBoletosColunas Imp,dBalancete.TabelaVirtualRow row,string PLA, ref int BOLRenta)
        {
            Imp.dBoletosColunas1.Boletos.DefaultView.Sort = "Agrupamento,BOLPagamento";
            int iBOL = Imp.dBoletosColunas1.Boletos.DefaultView.Find(new object[] {"Outros Créditos", row.Data});
            Boletos.Boleto.Impresso.dBoletosColunas.BoletosRow rowBoleto;
            if (iBOL < 0)
            {
                rowBoleto = Imp.dBoletosColunas1.Boletos.NewBoletosRow();
                rowBoleto.Agrupamento = "Outros Créditos";
                rowBoleto.APTNumero = "-";
                rowBoleto.BLOAPT = "-";
                rowBoleto.BLOCodigo = "-";
                rowBoleto.BOL = BOLRenta--;
                rowBoleto.BOL_APT = 0;
                rowBoleto.BOLPagamento = row.Data;
                rowBoleto.BOLProprietario = true;
                rowBoleto.BOLValorPago = 0;
                rowBoleto.BOLValorPrevisto = 0;
                rowBoleto.BOLVencto = row.Data;
                rowBoleto.Ordem = 4;
                rowBoleto.PI = "-";
            }
            else
                rowBoleto = Imp.dBoletosColunas1.Boletos[iBOL];
            rowBoleto.BOLValorPago += row.Valor;
            rowBoleto.BOLValorPrevisto += row.Valor;

            if (rowBoleto.RowState == DataRowState.Detached)
                Imp.dBoletosColunas1.Boletos.AddBoletosRow(rowBoleto);

            Boletos.Boleto.Impresso.dBoletosColunas.BoletosColunasRow novarowDet = Imp.dBoletosColunas1.BoletosColunas.NewBoletosColunasRow();
            novarowDet.BOD_PLA = PLA;
            novarowDet.BODMensagem = "";
            novarowDet.BODValor = row.Valor;
            novarowDet.BOL = rowBoleto.BOL;            
            Imp.dBoletosColunas1.BoletosColunas.AddBoletosColunasRow(novarowDet);
        }

        /*
        /// <summary>
        /// Carregas os dados no relatório de colunas (detalhamento dos boletos)
        /// </summary>
        /// <returns></returns>
        public Boletos.Boleto.Impresso.ImpBoletosColunas GeraRelatorioColunas()
        {
            Boletos.Boleto.Impresso.ImpBoletosColunas Imp = new Boletos.Boleto.Impresso.ImpBoletosColunas(CON, DataI, DataF, Boletos.Boleto.Impresso.ImpBoletosColunas.TipoRelatorio.DetalhamentoCredito, false, false);
            int BOLRenta = -1;
            foreach (DataRowView DRV in DBalancete1.TabelaVirtual.DefaultView)
            {
                dBalancete.TabelaVirtualRow row = (dBalancete.TabelaVirtualRow)DRV.Row;
                switch ((balancete.TiposLinhas)row.Tipo)
                {                             
                    case balancete.TiposLinhas.Rentabilidade:
                        GeraRelatorioColunasLinha(Imp,row,"170002", ref BOLRenta);                        
                        break;
                    case balancete.TiposLinhas.DetalheBOL:
                        if (row.CTL == DBalancete1.CTLCREDITOS_Anteriores)                        
                            GeraRelatorioColunasLinha(Imp, row, "910002", ref BOLRenta);                                                                        
                        break;
                    case balancete.TiposLinhas.TransferenciaF:
                    case balancete.TiposLinhas.TransferenciaFD:
                    case balancete.TiposLinhas.TransferenciaFC:
                    case balancete.TiposLinhas.TransferenciaL:                        
                    case balancete.TiposLinhas.DetalhePAG:
                    case balancete.TiposLinhas.pagEletronico:
                    case balancete.TiposLinhas.saldo:
                    case balancete.TiposLinhas.LinhaCCD:
                    case TiposLinhas.despesanoboleto:
                    case TiposLinhas.estorno:
                    case TiposLinhas.estornoC:
                    case TiposLinhas.estornoD:
                    case TiposLinhas.Deposito:
                    case TiposLinhas.Linkestorno:
                    case TiposLinhas.LinkTransferenciaF:
                        break;
                    default:
                        throw new NotImplementedException(string.Format("Tipo não implementado: {0} - {1}", row.Tipo, (balancete.TiposLinhas)row.Tipo));
                }
            }
            return Imp;
        }*/

        internal void GeraArquivosExp()
        {
            DataTable TabelaSaida = new DataTable();
            ArquivosTXT.cTabelaTXT ArqEmitidas = new ArquivosTXT.cTabelaTXT(TabelaSaida, ArquivosTXT.ModelosTXT.layout);
            ArqEmitidas.PontoDecima = ",";
            ArqEmitidas.Formatodata = ArquivosTXT.FormatoData.ddMMyyyy;
            ArqEmitidas.RegistraMapa(TabelaSaida.Columns.Add("CODIGO", typeof(string)), 1, 15);
            ArqEmitidas.RegistraMapa(TabelaSaida.Columns.Add("CODIGOCLIENTE", typeof(string)), 16, 10);
            ArqEmitidas.RegistraMapa(TabelaSaida.Columns.Add("EMISSAO", typeof(DateTime)), 26, 6);
            ArqEmitidas.RegistraMapa(TabelaSaida.Columns.Add("VENCTO", typeof(DateTime)), 32, 6);
            ArqEmitidas.RegistraMapa(TabelaSaida.Columns.Add("PAGAMENTO", typeof(DateTime)), 38, 6);
            ArqEmitidas.RegistraMapa(TabelaSaida.Columns.Add("VALOR", typeof(decimal)), 44, 8);
            ArqEmitidas.RegistraMapa(TabelaSaida.Columns.Add("TESTE", typeof(string)), 60, 20);
            foreach (dBalancete.BOLetosRecebidosRow rowCarregar in DBalancete1.BOLetosRecebidos)
            {
                foreach (string PLA in ColunasDinamicas.Keys)
                    if ((rowCarregar[PLA] != DBNull.Value) && (rowCarregar.BOL > 0))
                        TabelaSaida.Rows.Add(rowCarregar.BOL.ToString() + PLA,
                                             "0001",
                                             rowCarregar.BOLEmissao,
                                             rowCarregar.BOLVencto,
                                             rowCarregar.BOLPagamento,
                                             rowCarregar[PLA],
                                             ColunasDinamicas[PLA]);
            }
            ArqEmitidas.Salva("D:\\1\\exp\\1.txt");
        }

        private string DescLimpa(string original,string PLA)
        {
            string descLimpa = original;

            Framework.datasets.dPLAnocontas.PLAnocontasRow PLArow = Framework.datasets.dPLAnocontas.dPLAnocontasSt.PLAnocontas.FindByPLA(PLA);
            if (PLArow != null)
            {
                if (!PLArow.PLADescricaoLivre)
                    descLimpa = PLArow.PLADescricao;
            }
            if (descLimpa.Contains(" (Pagamento Parcial)"))
                descLimpa = descLimpa.Replace(" (Pagamento Parcial)", "").Trim();

            if (descLimpa.Contains("Desconto (SÍNDICO) - "))
                descLimpa = descLimpa.Replace("Desconto (SÍNDICO) - ", "").Trim();

            if (descLimpa.Contains("(") && descLimpa.Contains(")"))
            {
                bool manter = false;
                int de = descLimpa.IndexOf('(');
                int ate = descLimpa.IndexOf(')');
                for (int i = de + 1; i < ate; i++)
                    if (!char.IsDigit(descLimpa[i]) && (descLimpa[i] != '/') && (descLimpa[i] != ' '))
                    {
                        manter = true;
                        break;
                    }
                if (!manter)
                {
                    if (descLimpa.Length > ate)
                        descLimpa = (descLimpa.Substring(0, de) + descLimpa.Substring(ate + 1)).Trim();
                    else
                        descLimpa = descLimpa.Substring(0, de).Trim();
                }
            }
            return descLimpa;
        }

        /// <summary>
        /// usarBlocos
        /// </summary>
        public bool usarBlocos = false;

        private DataView _DV_CreditosDesc;

        private DataView DV_CreditosDesc
        {
            get
            {
                if (_DV_CreditosDesc == null) 
                {
                    _DV_CreditosDesc = new DataView(DCreditos1.Creditos);
                    _DV_CreditosDesc.Sort = "Descricao";
                }
                return _DV_CreditosDesc;
            }
        }

        private dCreditos.CreditosRow PegaCriaLinhaCredito(string Titulo,string PLA)
        {            
            int ind = DV_CreditosDesc.Find(Titulo);
            dCreditos.CreditosRow rowC = null;
            if (ind == -1)
            //dCreditos.CreditosRow rowC = dCreditos1.Creditos.FindByDescricao(Titulo);
            //if (rowC == null)
            {
                rowC = DCreditos1.Creditos.NewCreditosRow();
                rowC.Descricao = Titulo;
                rowC.Total = 0;
                rowC.Atrasado = 0;
                rowC.Antecipado = 0;
                rowC.Periodo = 0;
                rowC.T1 = rowC.T2 = rowC.T3 = false;
                rowC.PLA = PLA;
                rowC.SoPrevisto = true;
                DCreditos1.Creditos.AddCreditosRow(rowC);
                rowC.AcceptChanges();
            }
            else
                rowC = (dCreditos.CreditosRow)DV_CreditosDesc[ind].Row;
                //rowC = DCreditos1.Creditos[ind];
            return rowC;
        }

        /// <summary>
        /// Tipo de Data
        /// </summary>
        internal enum TpDataNoPeriodo 
        {
            /// <summary>
            /// Crédito não localizado nos extratos
            /// </summary>
            semCredito = 0,
            /// <summary>
            /// periodo
            /// </summary>
            periodo = 1,
            /// <summary>
            /// atrasado
            /// </summary>
            atrasado = 2,
            /// <summary>
            /// antecipado
            /// </summary>
            antecipado = 3,
            /// <summary>
            /// Outros Créditos
            /// </summary>
            outrosCreditos = 4
        }

        /// <summary>
        /// PosicaoData
        /// </summary>
        /// <param name="Data"></param>
        /// <returns></returns>
        private TpDataNoPeriodo PosicaoData(DateTime Data)
        {
            if (Data < DataI)
                return TpDataNoPeriodo.atrasado;
            else if (Data <= DataF)
                return TpDataNoPeriodo.periodo;
            else
                return TpDataNoPeriodo.antecipado;
        }

        private TpDataNoPeriodo SomarowC(DateTime Data, dCreditos.CreditosRow rowC,decimal Valor,bool Quadrinho)
        {
            //if (Valor == -415.94M)
            //    System.Windows.Forms.MessageBox.Show("aqui");
            TpDataNoPeriodo retorno = PosicaoData(Data);
            switch (retorno)
            {
                case TpDataNoPeriodo.antecipado:
                    rowC.Antecipado += Valor;
                    if (Quadrinho)
                        rowC.T3 = true;
                    break;
                case TpDataNoPeriodo.periodo:
                    rowC.Periodo += Valor;
                    if (Quadrinho)
                        rowC.T1 = true;
                    break;
                case TpDataNoPeriodo.atrasado:
                    rowC.Atrasado += Valor;
                    if (Quadrinho)
                        rowC.T2 = true;
                    break;
            }
            rowC.Total += Valor;
            rowC.SoPrevisto = false;
            return retorno;
        }

        private void AcumulaDebito(dBalancete.TabelaVirtualRow row, AgrupadorBalancete.solicitaAgrupamento TpClass)
        {
            string Descricao = row.IsDescBalanceteNull() ? row.Descricao : row.DescBalancete;
            int? CTL = null;
            if (!row.IsCTLNull())
                CTL = row.CTL;
            AcumulaDebito(Descricao, TpClass, CTL ,row.IsCTLNull() ? "" : row.PLA,row.Valor);                        
        }

        private void AcumulaDebito(string Descricao, AgrupadorBalancete.solicitaAgrupamento TpClass, int? CTL, string PLA, decimal Valor)
        {
            string SuperGrupo = "Total Geral";
            string Grupo = "??";
            string Ordemimp = "";
            int nGrupo = 0;
            int nSuperGrupo = 0;
            if (TpClass.Tipo == AgrupadorBalancete.TipoClassificacaoDesp.ContasLogicas)
            {
                if (CTL.HasValue)
                {
                    dBalancete.ConTasLogicasRow rowCTL = dBalancete1.ConTasLogicas.FindByCTL(CTL.Value);
                    Grupo = rowCTL.CTLTitulo;
                    Ordemimp = Grupo;                    
                }
            }
            else
            {
                AgrupadorBalancete.retornoClassificacao ClassD = AgrupadorBalancete.ClassificaDespesa(TpClass, PLA == null ? "299000" : PLA,Descricao);
                Ordemimp = ClassD.Ordem.ToString("0000000000");
                Grupo = ClassD.Titulo;
                Descricao = ClassD.Descricao;
                if (ClassD.Grupo != "")
                    SuperGrupo = ClassD.Grupo;
                nGrupo = ClassD.nGrupo;
                nSuperGrupo = ClassD.nSuperGrupo;
            };
            dCreditos.DebitosRow rowDeb = DCreditos1.Debitos.FindByGrupoDescricao(Grupo, Descricao);
            if (rowDeb == null)
            {
                rowDeb = DCreditos1.Debitos.NewDebitosRow();
                rowDeb.Grupo = Grupo;
                rowDeb.Descricao = Descricao;
                rowDeb.Valor = -Valor;
                rowDeb.Ordem = Ordemimp;
                rowDeb.PLA = PLA;
                rowDeb.SuperGrupo = SuperGrupo;
                rowDeb.nGrupo = nGrupo;
                rowDeb.nSuperGrupo = nSuperGrupo;
                rowDeb.calSG = string.Format("{0:00} - {1}",nSuperGrupo,SuperGrupo);
                rowDeb.calG = string.Format("{0:00} - {1}", nGrupo, Grupo);
                DCreditos1.Debitos.AddDebitosRow(rowDeb);
            }
            else
                rowDeb.Valor -= Valor;
        }


        private AgrupadorBalancete.solicitaAgrupamento TpClassUsado;                

        /// <summary>
        /// Carrega os dados nas saídas do balancete
        /// </summary>
        public override void CarregarDadosBalancete()
        {
            if (!SalvoXML)
                CarregarDadosBalancete(AgrupamentoUsarBoleto);
                //CarregarDadosBalancete(new AgrupadorBalancete.solicitaAgrupamento(AgrupadorBalancete.TipoClassificacaoDesp.Grupos));
        }

        /// <summary>
        /// Calculas os dados dos relatórios
        /// </summary>        
        /// <param name="TpClass">Forma de agrupar</param>
        /// <param name="Forcar">Força o calculo</param>
        public override void CarregarDadosBalancete(ABS_solicitaAgrupamento TpClass, bool Forcar = false)
        {
            CarregarDadosBalancete((AgrupadorBalancete.solicitaAgrupamento)TpClass, Forcar);
        }

        
        private void CarregarTabelaVirtual()
        {
            DCreditos1.Creditos.Clear();
            DCreditos1.Debitos.Clear();
            DCreditos1.ContasFisicas.Clear();            
            DCreditos1.Mes.Clear();
            RemoveColunasDinamicas();
            PreparaPizza();
            foreach (dBalancete.BOLetosRecebidosRow rowBolRec in DBalancete1.BOLetosRecebidos)
                rowBolRec.ValCredLiq = 0;
            if (LinhasExtraCredito != null)
            {
                foreach (dBalancete.BOLetosRecebidosRow rowBolRecExtra in LinhasExtraCredito.Values)
                    rowBolRecExtra.Delete();
                LinhasExtraCredito.Clear();
            }
            TRentabilidade = 0;

            foreach (Boletos.Boleto.Boleto Boleto in BoletosRec.Values)
            {                
                dBalancete.BOLetosRecebidosRow rowBolRec = DBalancete1.BOLetosRecebidos.FindByBOL(Boleto.rowPrincipal.BOL);
                AcumulaPizza((ClassePizza)rowBolRec.ClaGrafico, rowBolRec.BOLValorPago);
                foreach (BoletosProc.Boleto.dBoletos.BOletoDetalheRow rowBOD in Boleto.rowPrincipal.GetBOletoDetalheRows())
                {
                    SomaEmCreditosDetalhados(rowBOD.BOD_PLA, rowBOD.BODValor, rowBolRec);

                    if (rowBOD.BOD_PLA.Substring(0, 1) == "2")
                    {
                        int CTL;
                        if (!rowBOD.IsBOD_CTLNull())
                            CTL = rowBOD.BOD_CTL;
                        else
                            if (rowBOD.BOD_PLA == "110000")
                                CTL = DBalancete1.CTLFUNDO;
                            else
                                CTL = DBalancete1.CTLCAIXA;
                        //string MensagemBalancete = rowBOD.IsBODMensagemBalanceteNull() ? rowBOD.BODMensagem : rowBOD.BODMensagemBalancete;
                        //AcumulaDebito(MensagemBalancete, TpClass, CTL, rowBOD.BOD_PLA, rowBOD.BODValor);
                    }
                    else
                    {
                        string descLimpa = DescLimpa(rowBOD.BODMensagem, rowBOD.BOD_PLA);

                        dCreditos.CreditosRow rowC = PegaCriaLinhaCredito(descLimpa, rowBOD.BOD_PLA);

                        TpDataNoPeriodo Tipo = SomarowC(Boleto.rowPrincipal.BOLVencto, rowC, rowBOD.BODValor, !rowBOD.IsBOD_APTNull());

                        if ((Boleto.Apartamento != null) && (Boleto.Apartamento.BLOCodigo.ToString() != "SB") && (Boleto.Apartamento.BLOCodigo.ToString() != ""))
                            usarBlocos = true;
                        dCreditos.MesRow mesrow = DCreditos1.Mes.FindByDescricaoBoleto(rowC.Descricao, Boleto.rowPrincipal.BOL);
                        if (mesrow != null)
                            mesrow.Valor += rowBOD.BODValor;
                        else
                        {
                            //if (!Boleto.rowComplementar.IsBLONomeNull())

                            //{
                            string BLOCodigo;
                            string BLONome;

                            if (Boleto.Apartamento != null)
                            {
                                BLOCodigo = Boleto.Apartamento.BLOCodigo;
                                BLONome = string.Format("{0} - {1}", BLOCodigo, Boleto.Apartamento.BLONome);
                            }
                            else
                            {
                                BLOCodigo = "";
                                BLONome = "";
                            }

                            
                            dCreditos.MesRow novaMesRow = DCreditos1.Mes.NewMesRow();
                            novaMesRow.Bloco = BLOCodigo;
                            novaMesRow.BOD = rowBOD.BOD;
                            novaMesRow.Boleto = Boleto.rowPrincipal.BOL;
                            novaMesRow.Competencia = string.Format("{0:00}/{1:00}", Boleto.rowPrincipal.BOLCompetenciaMes,
                                                                                    Boleto.rowPrincipal.BOLCompetenciaAno);
                            novaMesRow.CreditosID = rowC.ID;
                            novaMesRow.Descricao = rowC.Descricao;
                            novaMesRow.Manual = false;
                            novaMesRow.NomeBloco = BLONome;
                            novaMesRow.Numero = Boleto.Apartamento == null ? "" : Boleto.Apartamento.APTNumero;
                            novaMesRow.Tipo = (int)Tipo;
                            novaMesRow.Valor = rowBOD.BODValor;
                            DCreditos1.Mes.AddMesRow(novaMesRow);
                            novaMesRow.AcceptChanges();
                            //}
                        }
                    }
                }
            }

            //decimal SomaTotalBoletos = 0;
            //decimal SomaItens = 0;

            foreach (Boletos.Boleto.Boleto Boleto in BoletosPrevistos.Values)
            {

                //SomaTotalBoletos += Boleto.rowPrincipal.BOLValorPrevisto;
                foreach (BoletosProc.Boleto.dBoletos.BOletoDetalheRow rowBOD in Boleto.rowPrincipal.GetBOletoDetalheRows())
                {
                    //SomaItens += rowBOD.BODValor;
                    if ((rowBOD.IsBODPrevistoNull()) || (!rowBOD.BODPrevisto) || (rowBOD.BOD_PLA.Substring(0, 1) == "2"))
                        continue;
                    //if ((rowBOD.BOD_PLA == "120001") || (rowBOD.BOD_PLA.Substring(0, 1) == "2"))
                    //    continue;
                    string descLimpa = DescLimpa(rowBOD.BODMensagem, rowBOD.BOD_PLA);
                    dCreditos.CreditosRow rowC = PegaCriaLinhaCredito(descLimpa, rowBOD.BOD_PLA);
                    if (rowC.IsPrevistoNull())
                        rowC.Previsto = 0;
                    rowC.Previsto += rowBOD.BODValor;
                }
            }

            //System.Windows.Forms.MessageBox.Show(string.Format("{0:n2} {1:n2} {2:n2} {3:n2}",SomaTotalBoletos,SomaItens,SomaSem12,SomaSem2e12));

            CarregaSaldosfisicosBAL(DCreditos1);

        }
        


        /// <summary>
        /// Calculas os dados dos relatórios
        /// </summary>        
        /// <param name="TpClass">Forma de agrupar</param>
        /// <param name="Forcar">Força o calculo</param>
        public void CarregarDadosBalancete(AgrupadorBalancete.solicitaAgrupamento TpClass, bool Forcar = false)
        {
            if (SalvoXML)
                return;
            if (BoletosRec == null)
                LeBoletos();

            if (!Forcar && (TpClass == TpClassUsado))
                return;
            TpClassUsado = TpClass;

            _TotCreditos = null;
            _TotDebitosGrupo = null;

            CarregarTabelaVirtual();

            /*
            ////////////////////////////////////////////////////////////////////////////////////////
            ////////////////////////////////////////////////////////////////////////////////////////
            ////////////////////////////////////////////////////////////////////////////////////////
            ////////////////////////////////////////////////////////////////////////////////////////
            ////////////////////////////////////////////////////////////////////////////////////////



            DCreditos1.Creditos.Clear();
            DCreditos1.Debitos.Clear();
            DCreditos1.ContasFisicas.Clear();
            //DCreditos1.ContasLogicas.Clear();
            DCreditos1.Mes.Clear();
            RemoveColunasDinamicas();
            PreparaPizza();
            foreach (dBalancete.BOLetosRecebidosRow rowBolRec in DBalancete1.BOLetosRecebidos)
                rowBolRec.ValCredLiq = 0;
            if (LinhasExtraCredito != null)
            {
                foreach (dBalancete.BOLetosRecebidosRow rowBolRecExtra in LinhasExtraCredito.Values)
                    rowBolRecExtra.Delete();
                LinhasExtraCredito.Clear();
            }
            TRentabilidade = 0;

            foreach (Boletos.Boleto.Boleto Boleto in BoletosRec.Values)
            {

                
                dBalancete.BOLetosRecebidosRow rowBolRec = DBalancete1.BOLetosRecebidos.FindByBOL(Boleto.rowPrincipal.BOL);
                AcumulaPizza((ClassePizza)rowBolRec.ClaGrafico, rowBolRec.BOLValorPago);
                foreach (BoletosProc.Boleto.dBoletos.BOletoDetalheRow rowBOD in Boleto.rowPrincipal.GetBOletoDetalheRows())
                {
                    SomaEmCreditosDetalhados(rowBOD.BOD_PLA, rowBOD.BODValor, rowBolRec);

                    if (rowBOD.BOD_PLA.Substring(0, 1) == "2")
                    {
                        int CTL;
                        if (!rowBOD.IsBOD_CTLNull())
                            CTL = rowBOD.BOD_CTL;
                        else
                            if (rowBOD.BOD_PLA == "110000")
                                CTL = DBalancete1.CTLFUNDO;
                            else
                                CTL = DBalancete1.CTLCAIXA;
                        //string MensagemBalancete = rowBOD.IsBODMensagemBalanceteNull() ? rowBOD.BODMensagem : rowBOD.BODMensagemBalancete;
                        //AcumulaDebito(MensagemBalancete, TpClass, CTL, rowBOD.BOD_PLA, rowBOD.BODValor);
                    }
                    else
                    {
                        string descLimpa = DescLimpa(rowBOD.BODMensagem, rowBOD.BOD_PLA);

                        dCreditos.CreditosRow rowC = PegaCriaLinhaCredito(descLimpa, rowBOD.BOD_PLA);

                        TpDataNoPeriodo Tipo = SomarowC(Boleto.rowPrincipal.BOLVencto, rowC, rowBOD.BODValor, !rowBOD.IsBOD_APTNull());

                        if ((Boleto.Apartamento != null) && (Boleto.Apartamento.BLOCodigo.ToString() != "SB") && (Boleto.Apartamento.BLOCodigo.ToString() != ""))
                            usarBlocos = true;
                        dCreditos.MesRow mesrow = DCreditos1.Mes.FindByDescricaoBoleto(rowC.Descricao, Boleto.rowPrincipal.BOL);
                        if (mesrow != null)
                            mesrow.Valor += rowBOD.BODValor;
                        else
                        {
                            //if (!Boleto.rowComplementar.IsBLONomeNull())

                            //{
                            string BLOCodigo;
                            string BLONome;

                            if (Boleto.Apartamento != null)
                            {
                                BLOCodigo = Boleto.Apartamento.BLOCodigo;
                                BLONome = string.Format("{0} - {1}", BLOCodigo, Boleto.Apartamento.BLONome);
                            }
                            else
                            {
                                BLOCodigo = "";
                                BLONome = "";
                            }

                            
                            dCreditos.MesRow novaMesRow = DCreditos1.Mes.NewMesRow();
                            novaMesRow.Bloco = BLOCodigo;
                            novaMesRow.BOD = rowBOD.BOD;
                            novaMesRow.Boleto = Boleto.rowPrincipal.BOL;
                            novaMesRow.Competencia = string.Format("{0:00}/{1:00}", Boleto.rowPrincipal.BOLCompetenciaMes,
                                                                                    Boleto.rowPrincipal.BOLCompetenciaAno);
                            novaMesRow.CreditosID = rowC.ID;
                            novaMesRow.Descricao = rowC.Descricao;
                            novaMesRow.Manual = false;
                            novaMesRow.NomeBloco = BLONome;
                            novaMesRow.Numero = Boleto.Apartamento == null ? "" : Boleto.Apartamento.APTNumero;
                            novaMesRow.Tipo = (int)Tipo;
                            novaMesRow.Valor = rowBOD.BODValor;
                            DCreditos1.Mes.AddMesRow(novaMesRow);
                            novaMesRow.AcceptChanges();
                            //}
                        }
                    }
                }
            }

            //decimal SomaTotalBoletos = 0;
            //decimal SomaItens = 0;

            foreach (Boletos.Boleto.Boleto Boleto in BoletosPrevistos.Values)
            {

                //SomaTotalBoletos += Boleto.rowPrincipal.BOLValorPrevisto;
                foreach (BoletosProc.Boleto.dBoletos.BOletoDetalheRow rowBOD in Boleto.rowPrincipal.GetBOletoDetalheRows())
                {
                    //SomaItens += rowBOD.BODValor;
                    if ((rowBOD.IsBODPrevistoNull()) || (!rowBOD.BODPrevisto) || (rowBOD.BOD_PLA.Substring(0, 1) == "2"))
                        continue;
                    //if ((rowBOD.BOD_PLA == "120001") || (rowBOD.BOD_PLA.Substring(0, 1) == "2"))
                    //    continue;
                    string descLimpa = DescLimpa(rowBOD.BODMensagem, rowBOD.BOD_PLA);
                    dCreditos.CreditosRow rowC = PegaCriaLinhaCredito(descLimpa, rowBOD.BOD_PLA);
                    if (rowC.IsPrevistoNull())
                        rowC.Previsto = 0;
                    rowC.Previsto += rowBOD.BODValor;
                }
            }

            //System.Windows.Forms.MessageBox.Show(string.Format("{0:n2} {1:n2} {2:n2} {3:n2}",SomaTotalBoletos,SomaItens,SomaSem12,SomaSem2e12));

            CarregaSaldosfisicosBAL(DCreditos1);

            ////////////////////////////////////////////////////////////////////////////////////////
            ////////////////////////////////////////////////////////////////////////////////////////
            ////////////////////////////////////////////////////////////////////////////////////////
            ////////////////////////////////////////////////////////////////////////////////////////
            ////////////////////////////////////////////////////////////////////////////////////////
            */

            foreach (DataRowView DRV in DBalancete1.TabelaVirtual.DefaultView)
            {
                dBalancete.TabelaVirtualRow row = (dBalancete.TabelaVirtualRow)DRV.Row;
                if (!row.IsCCDNull())
                    if ((CCDrastrearCarregarDadosBalancete != 0) && (CCDrastrearCarregarDadosBalancete == row.CCD))
                        System.Windows.Forms.MessageBox.Show("Rastreador");

                switch ((balancete.TiposLinhas)row.Tipo)
                {
                    case balancete.TiposLinhas.saldo:
                    case balancete.TiposLinhas.TransferenciaF:
                    case balancete.TiposLinhas.TransferenciaFD:
                    case balancete.TiposLinhas.TransferenciaFC:
                    case balancete.TiposLinhas.TransferenciaL:
                    case TiposLinhas.estorno:
                    case TiposLinhas.estornoC:
                    case TiposLinhas.estornoD:
                    case TiposLinhas.Linkestorno:
                    case TiposLinhas.LinkTransferenciaF:
                        break;
                    default:
                        throw new NotImplementedException(string.Format("Tipo não implementado: {0} - {1}", row.Tipo, (balancete.TiposLinhas)row.Tipo));
                    case balancete.TiposLinhas.LinhaCCD:
                    case TiposLinhas.Deposito:
                        if (row.PLA.StartsWith("2"))// (row.CCDValor < 0)
                            AcumulaDebito(row, TpClass);
                        else
                        {
                            string Descricao = row.IsDescBalanceteNull() ? row.Descricao : row.DescBalancete;
                            dCreditos.CreditosRow rowCnD = PegaCriaLinhaCredito(Descricao, row.PLA);
                            SomarowC(row.Data, rowCnD, row.Valor, false);
                            SomaEmCreditosDetalhados(row.PLA, row.Valor, null, row.Data);
                        }
                        break;
                    case balancete.TiposLinhas.DetalheBOL:
                    case balancete.TiposLinhas.DetalheBOLManual:
                        if (row.PLA.StartsWith("2"))// (row.CCDValor < 0)
                            AcumulaDebito(row, TpClass);
                        else
                            if (!row.IsCCSNull()) // se CSS for nulo o crédito ja foi carregado no carga dos boletos
                            {
                                dBalancete.ContaCorrenteSubdetalheRow rowCCS = DBalancete1.ContaCorrenteSubdetalhe.FindByCCS(row.CCS);
                                dCreditos.CreditosRow rowCnD = PegaCriaLinhaCredito(rowCCS.CCSDescricaoBAL, rowCCS.CCS_PLA);
                                TpDataNoPeriodo Tipo = SomarowC(row.Data, rowCnD, rowCCS.CCSValor, true);
                                /*
                                dCreditos.MesRow novaMesRow = DCreditos1.Mes.AddMesRow(rowCnD.Descricao,
                                                         "-",
                                                         "-",
                                                         (int)Tipo,
                                                         "-",
                                                         -rowCCS.CCS,
                                                         rowCCS.CCSValor, 
                                                         "",
                                                         0,
                                                         false,
                                                         rowCnD.ID);*/
                                dCreditos.MesRow novaMesRow = DCreditos1.Mes.NewMesRow();
                                novaMesRow.Descricao = rowCnD.Descricao;
                                novaMesRow.Bloco = "-";
                                novaMesRow.Numero = "-";
                                novaMesRow.Tipo = (int)Tipo;
                                novaMesRow.Competencia = "-";
                                novaMesRow.Boleto = -rowCCS.CCS;
                                novaMesRow.Valor = rowCCS.CCSValor;
                                novaMesRow.NomeBloco = "";
                                novaMesRow.Manual = false;
                                novaMesRow.CreditosID = rowCnD.ID;
                                DCreditos1.Mes.AddMesRow(novaMesRow);
                                novaMesRow.AcceptChanges();
                                SomaEmCreditosDetalhados(rowCCS.CCS_PLA, rowCCS.CCSValor, null, row.Data);

                            }
                        break;
                    case balancete.TiposLinhas.Rentabilidade:
                        TRentabilidade += row.Valor;
                        //dCreditos.CreditosRow rowC = PegaCriaLinhaCredito(DCreditos1, "Rendimentos Financeiros", row.PLA);
                        //SomarowC(row.Data, rowC, row.Valor, false);
                        //SomaEmCreditosDetalhados(row.PLA, row.Valor, null, row.Data);
                        break;
                    case balancete.TiposLinhas.DetalhePAG:
                    case balancete.TiposLinhas.pagEletronico:
                        AcumulaDebito(row, TpClass);
                        break;
                    case balancete.TiposLinhas.despesanoboleto:
                    case balancete.TiposLinhas.despesanoboletoManual:
                        //if (!row.IsCCSNull())
                        //{
                            if (row.PLA.StartsWith("2"))// (row.CCDValor < 0)
                                AcumulaDebito(row, TpClass);
                        //}
                        //O valor vindo dos boletos já é somado durante a carga dos boletos recebidos ou zerados
                        break;
                }
            }

        }

        /// <summary>
        /// Controla quando as colunas foram atualizadas para que a tela possa acompanhar.
        /// </summary>
        [DataMember]
        internal int AtualizacaoColunasDinamicas = 1;

        private void RemoveColunasDinamicas()
        {
            if (ColunasDinamicas != null)
            {
                AtualizacaoColunasDinamicas++;
                foreach (string ColunaDinamica in ColunasDinamicas.Keys)
                    dBalancete1.BOLetosRecebidos.Columns.Remove(ColunaDinamica);
            };
            ColunasDinamicas = new SortedList<string, string>();
        }

        /// <summary>
        /// Colunas dinâmicas da tabela BOLetosRecebidos
        /// </summary>
        [DataMember]        
        public SortedList<string, string> ColunasDinamicas;

        private SortedList<DateTime,dBalancete.BOLetosRecebidosRow> LinhasExtraCredito;

        private void SomaEmCreditosDetalhados(string PLA, decimal Valor, dBalancete.BOLetosRecebidosRow _rowBolRec,DateTime? Data = null)
        {
            if (PLA.Substring(0, 1) == "2")
                return;
            dBalancete.BOLetosRecebidosRow rowBolRec;
            if (_rowBolRec != null)
                rowBolRec = _rowBolRec;
            else
            {
                if (LinhasExtraCredito == null)
                    LinhasExtraCredito = new SortedList<DateTime, dBalancete.BOLetosRecebidosRow>();
                if (LinhasExtraCredito.ContainsKey(Data.Value))
                {
                    rowBolRec = LinhasExtraCredito[Data.Value];
                    rowBolRec.BOLValorPago = rowBolRec.BOLValorPago + Valor;
                }
                else
                {
                    rowBolRec = dBalancete1.BOLetosRecebidos.NewBOLetosRecebidosRow();
                    LinhasExtraCredito.Add(Data.Value, rowBolRec);
                    rowBolRec.BOL = -LinhasExtraCredito.Count;
                    // bla bla bla
                    rowBolRec.calcGrupo = (int)TpDataNoPeriodo.outrosCreditos;
                    rowBolRec.BOLValorPago = Valor;
                    rowBolRec.BOLVencto = Data.Value;
                    rowBolRec.BOLPagamento = Data.Value;
                    rowBolRec.BOLEmissao = Data.Value;
                    rowBolRec.BOLTipoCRAI = " ";
                    rowBolRec.BOLCompetenciaMes = (short)comp.Mes;
                    rowBolRec.BOLCompetenciaAno = (short)comp.Ano;
                    dBalancete1.BOLetosRecebidos.AddBOLetosRecebidosRow(rowBolRec);
                }
            }
            string ColunaDinamica = PLA;
            Framework.datasets.dPLAnocontas.PLAnocontasRow PLArow = Framework.datasets.dPLAnocontas.dPLAnocontasSt.PLAnocontas.FindByPLA(PLA);
            string TituloColunaDinamica = PLArow.PLADescricaoRes;
            //TituloColunaDinamica = DescLimpa(rowBOD.BODMensagem, rowBOD.BOD_PLA);
            //ColunaDinamica = DescLimpa(rowBOD.BODMensagem, rowBOD.BOD_PLA, LetrasNumeros);
            if (!ColunasDinamicas.ContainsKey(ColunaDinamica))
            {
                ColunasDinamicas.Add(ColunaDinamica, TituloColunaDinamica);
                DBalancete1.BOLetosRecebidos.Columns.Add(ColunaDinamica, typeof(decimal));                
            }
            decimal ValorAnterior = rowBolRec[ColunaDinamica] == DBNull.Value ? 0 : (decimal)rowBolRec[ColunaDinamica];
            rowBolRec[ColunaDinamica] = ValorAnterior + Valor;
            rowBolRec.ValCredLiq = (rowBolRec.IsValCredLiqNull() ? 0 : rowBolRec.ValCredLiq) + Valor;
            if (rowBolRec.IsClaGraficoNull())
            {
                ClassePizza ClaGrafico;
                if (PLA == "910002")
                    ClaGrafico = ClassePizza.NIdentificado;
                else if (PLA == "170002")
                    ClaGrafico = ClassePizza.Rentabilidade;
                else
                    ClaGrafico = ClassePizza.Outros;
                AcumulaPizza(ClaGrafico, rowBolRec.BOLValorPago);
                rowBolRec.ClaGrafico = (int)ClaGrafico;
            }
        }       

        private string VerificaFrancesinha()
        {
            dllbanco.ImpFran.dImpFranc dImpFranc1 = new dllbanco.ImpFran.dImpFranc();
            return dImpFranc1.Verifica(CON, DataI, DataF);
        }

        /// <summary>
        /// Boletos Previstos
        /// </summary>
        private SortedList<int, Boletos.Boleto.Boleto> BoletosPrevistos;

        /// <summary>
        /// Boletos quitados
        /// </summary>
        private SortedList<int, Boletos.Boleto.Boleto> BoletosRec = null;        

        /// <summary>
        /// Retora boleto Previamente carregado no BoletosRec ou um novo
        /// </summary>
        /// <param name="BOL"></param>
        /// <returns></returns>
        public Boletos.Boleto.Boleto PegaBoleto(int BOL)
        {
            if ((BoletosRec != null) && (BoletosRec.ContainsKey(BOL)))
                return BoletosRec[BOL];
            else if ((BoletosPrevistos != null) && (BoletosPrevistos.ContainsKey(BOL)))
                return BoletosPrevistos[BOL];
            else
                return new Boletos.Boleto.Boleto(BOL);
        }

        /// <summary>
        /// Limpa a coleção de boletos recebidos para ser gerada novamente
        /// </summary>
        public void ResetBoletosRec()
        {
            BoletosRec = null;
        }

        private SortedList<int, Boletos.Boleto.Boleto> BoletosZerados = null;

        private List<dBalancete.TabelaVirtualRow> TransfFPendentes;

        private decimal amarraTransfF(dBalancete.ContaCorrenteDetalheRow CCDrow, int CCT, bool AgruparTF)
        {            
            dBalancete.TabelaVirtualRow Remover = null;
            foreach (dBalancete.TabelaVirtualRow rowEspera in TransfFPendentes)
                if ((rowEspera.Valor == -CCDrow.CCDValor) && (rowEspera.Data == CCDrow.SaldoContaCorrenteRow.SCCData))
                {
                    Remover = rowEspera;
                    if (AgruparTF)
                    {
                        rowEspera.CCT2 = CCT;                        
                        rowEspera.Tipo = (int)TiposLinhas.TransferenciaF;
                    }
                    rowEspera.SetErroNull();
                    break;
                }
            if (Remover != null)
            {
                TransfFPendentes.Remove(Remover);
                return Remover.Ordem;
            }
            else
            {                
                return 0;
            }            
                
        }

        private List<dBalancete.TabelaVirtualRow> EstornosPendentes;

        private decimal amarraEstornos(dBalancete.ContaCorrenteDetalheRow CCDrow, bool AgruparTF)
        {
            dBalancete.TabelaVirtualRow Remover = null;
            foreach (dBalancete.TabelaVirtualRow rowEspera in EstornosPendentes)
                if ((rowEspera.Valor == -CCDrow.CCDValor) && (rowEspera.Data == CCDrow.SaldoContaCorrenteRow.SCCData))
                {
                    Remover = rowEspera;
                    if (AgruparTF)                                            
                        rowEspera.Tipo = (int)TiposLinhas.estorno;
                    
                    rowEspera.SetErroNull();
                    break;
                }
            if (Remover != null)
            {
                EstornosPendentes.Remove(Remover);
                return Remover.Ordem;
            }
            else
            {
                return 0;
            }

        }
        
        /// <summary>
        /// Rastrear Conciliação
        /// </summary>
        internal int CCDrastrearConc = 0;

        /// <summary>
        /// Rastrear na carga dos extratos
        /// </summary>
        internal int CCDrastrearCarga = 0;

        /// <summary>
        /// Rastrear CarregarDadosBalancete
        /// </summary>
        internal int CCDrastrearCarregarDadosBalancete = 0;

        /// <summary>
        /// Rastrear Acumular
        /// </summary>
        internal int CCDrastrearAcumular = 0;

        internal int CCDrastrearVal = 0;

        internal dBalancete.TabelaVirtualRow rowRastrear = null;

        private enum ClassePizza
        {            
            Prazo = 0,            
            Atrasado = 1,            
            Acordo = 2,
            Juridico = 3,
            Rentabilidade = 4,
            Outros = 5,
            NIdentificado = 6,
            Extra = 7,
        }

        private dllVirEnum.VirEnum virEnumClassePizza;

        internal dllVirEnum.VirEnum VirEnumClassePizza
        {
            get 
            {
                if (virEnumClassePizza == null)
                {
                    virEnumClassePizza = new dllVirEnum.VirEnum(typeof(ClassePizza));                    
                    virEnumClassePizza.GravaNomes(ClassePizza.Prazo, "Boletos no prazo");
                    virEnumClassePizza.GravaNomes(ClassePizza.Atrasado, "Boletos atrasados");
                    virEnumClassePizza.GravaNomes(ClassePizza.Acordo, "Acordos Neon");
                    virEnumClassePizza.GravaNomes(ClassePizza.Juridico, "Acordos jurídico");
                    virEnumClassePizza.GravaNomes(ClassePizza.Rentabilidade, "Rentabilidade");
                    virEnumClassePizza.GravaNomes(ClassePizza.Outros, "Outros Créditos");
                    virEnumClassePizza.GravaNomes(ClassePizza.NIdentificado, "Não identificado");
                    virEnumClassePizza.GravaNomes(ClassePizza.Extra, "Antecipado/Duplicidade");
                }                
                return virEnumClassePizza;
            }
        }
        
        private void PreparaPizza()
        {
            DBalancete1.Grafico.Clear();
            foreach(int Codigo in VirEnumClassePizza.Nomes.Keys)
                DBalancete1.Grafico.AddGraficoRow(VirEnumClassePizza.Nomes[Codigo], 0, Codigo);                
            
        }

        private void AcumulaPizza(ClassePizza Classe, decimal Valor)
        {
            DBalancete1.Grafico[(int)Classe].Valor += Valor;
        }

        private BoletosProc.Boleto.dBoletos dBoletosGeral;

        private void LeBoletos()
        {
            Boletos.Boleto.Boleto Boleto;
            BoletosProc.Boleto.dBoletos.BOLetosRow BOLrow;
            CompontesBasicos.Performance.Performance.PerformanceST.Zerar();
            CompontesBasicos.Performance.Performance.PerformanceST.Registra("Marca 1");
            dBoletosGeral = new BoletosProc.Boleto.dBoletos();
            if (SalvoXML)
                return;
            DBalancete1.BOLetosRecebidos.Clear();
            //PreparaPizza();
            RemoveColunasDinamicas();
            LinhasExtraCredito = null;

            //CompontesBasicos.Performance.Performance.PerformanceST.Registra("Teste 1");
            //BoletosProc.Boleto.dBoletos dBoletos1 = new BoletosProc.Boleto.dBoletos();
            /*
             bOLetosTableAdapter.FillVencidosAte(dBoletos.BOLetos, dateEditVencimento.DateTime, dateEditVencimento.DateTime, lookupBlocosAptos_F1.CON_sel);                                                                                
             dBoletos.BOletoDetalheTableAdapter.FillByCON(dBoletos.BOletoDetalhe, lookupBlocosAptos_F1.CON_sel);
            */

            CompontesBasicos.Performance.Performance.PerformanceST.Registra("fill Boletos Recebidos");
            DBalancete1.BOLetosRecebidosTableAdapter.Fill(DBalancete1.BOLetosRecebidos, CON, DataI, DataF);
            CompontesBasicos.Performance.Performance.PerformanceST.Registra("fill 1 A");
            dBoletosGeral.BOLetosTableAdapter.FillGeralBalancete(dBoletosGeral.BOLetos, CON, DataI, DataF);
            CompontesBasicos.Performance.Performance.PerformanceST.Registra("fill 1 B");
            dBoletosGeral.BOletoDetalheTableAdapter.FillGeralBalancete(dBoletosGeral.BOletoDetalhe, CON, DataI, DataF);
            CompontesBasicos.Performance.Performance.PerformanceST.Registra("fill 2");
            DPrevisto1.PrevistoTableAdapter.Fill(DPrevisto1.Previsto, DataI, DataF, CON);
            CompontesBasicos.Performance.Performance.PerformanceST.Registra("fill Acordos");
            DBalancete1.BoletosOriginalAcordoTableAdapter.Fill(DBalancete1.BoletosOriginalAcordo, CON, DataI, DataF);
            CompontesBasicos.Performance.Performance.PerformanceST.Registra("Carrega bloco APTs");
            dBoletosGeral.CarregaAPTs(CON);
            CompontesBasicos.Performance.Performance.PerformanceST.Registra("Marca 2");
            BoletosRec = new SortedList<int, Boletos.Boleto.Boleto>();
            BoletosPrevistos = new SortedList<int, Boletos.Boleto.Boleto>();
            //Boletos zerados que não aparecem no extrato e devem ser encarados como tranferencias lógicas porém são lançados como créditos e débitos
            BoletosZerados = new SortedList<int, Boletos.Boleto.Boleto>();

            foreach (dBalancete.BOLetosRecebidosRow rowBOLRec in DBalancete1.BOLetosRecebidos)
            {
                if (!rowBOLRec.IsCCT_CONNull() && (rowBOLRec.CCT_CON != CON))
                {
                    dContasLogicas.ContaCorrenTeRow CCTrow = dContasLogicas.dContasLogicasSt.ContaCorrenTe.FindByCCT(rowBOLRec.CCT);
                    FrameworkProc.datasets.dCondominiosAtivos.CONDOMINIOSRow CONrow = FrameworkProc.datasets.dCondominiosAtivos.dCondominiosTodosST.CONDOMINIOS.FindByCON(rowBOLRec.CCT_CON);
                    rowBOLRec.Erro = string.Format("Boleto ligado a crédito de conta correte de outro condomínio\r\nConta: {0} {1} {2} - {3}\r\nCondomínio: {4} - {5}",
                                                    CCTrow.CCT_BCO,
                                                    CCTrow.CCTAgencia,
                                                    CCTrow.CCTConta,
                                                    CCTrow.CCTTitulo,
                                                    CONrow.CONCodigo,
                                                    CONrow.CONNome
                                                    );
                }
                else if (!rowBOLRec.IsSCCDataNull() && ((rowBOLRec.SCCData < DataI) || (rowBOLRec.SCCData > DataF)))
                {
                    rowBOLRec.Erro = string.Format("Boleto ligado a crédito de conta correte com data fora do período do balancete: {0:dd/MM/yyyy}", rowBOLRec.SCCData);
                }
                rowBOLRec.calcBOL = rowBOLRec.BOL;

                if (!BoletosRec.ContainsKey(rowBOLRec.BOL))
                {
                    CompontesBasicos.Performance.Performance.PerformanceST.Registra("new Boleto");
                    BOLrow = dBoletosGeral.BOLetos.FindByBOL(rowBOLRec.BOL);
                    if (BOLrow != null)
                        Boleto = new Boletos.Boleto.Boleto(BOLrow);
                    else
                    {
                        CompontesBasicos.Performance.Performance.PerformanceST.Registra("BOLETO NAO ENCONTRADO");
                        Boleto = new Boletos.Boleto.Boleto(rowBOLRec.BOL);
                    }
                    CompontesBasicos.Performance.Performance.PerformanceST.Registra("loop");
                    ClassePizza ClaGrafico = ClassePizza.Prazo;
                    if (Boleto.Encontrado)
                    {
                        CompontesBasicos.Performance.Performance.PerformanceST.Registra("interno 1");
                        if (Boleto.Apartamento != null)
                        {
                            if ((Boleto.Apartamento.BLONome != "") && (Boleto.Apartamento.BLOCodigo != "SB"))
                                //rowBOLRec.calcUnidade = string.Format("{0} {1}", Boleto.Apartamento.BLOCodigo, Boleto.Apartamento.APTNumero);
                                rowBOLRec.calcUnidade = string.Format("{0} {1}", Boleto.Apartamento.BLOCodigo, Boleto.Apartamento.APTNumero);
                            else
                                rowBOLRec.calcUnidade = string.Format("{0}", Boleto.Apartamento.APTNumero);
                        }
                        //if (rowBOLRec.IsBOLValorPagoNull())
                        //    System.Windows.Forms.MessageBox.Show(string.Format("BOL = {0}",rowBOLRec.BOL));
                        if (rowBOLRec.IsBOL_CCDNull() && (rowBOLRec.BOLValorPago != 0))
                            rowBOLRec.calcGrupo = (int)TpDataNoPeriodo.semCredito;
                        else
                            rowBOLRec.calcGrupo = (int)PosicaoData(rowBOLRec.BOLVencto);
                        BoletosRec.Add(rowBOLRec.BOL, Boleto);
                        CompontesBasicos.Performance.Performance.PerformanceST.Registra("interno 2");
                        Boleto.CorrecaoMulta();
                        CompontesBasicos.Performance.Performance.PerformanceST.Registra("interno 3");
                        if ((Boleto.rowPrincipal.IsBOL_CCDNull()) && (Boleto.rowPrincipal.BOLValorPago == 0) && (!Boleto.rowPrincipal.BOLCancelado))
                            BoletosZerados.Add(Boleto.rowPrincipal.BOL, Boleto);
                        if (Boleto.rowPrincipal.BOLTipoCRAI == "E")
                            ClaGrafico = ClassePizza.Extra;
                        else
                        {
                            if (Boleto.rowPrincipal.BOLValorPrevisto != Boleto.rowPrincipal.BOLValorPago)
                            {

                                CompontesBasicos.Performance.Performance.PerformanceST.Registra("Carrega Acordo");
                                //Boletos.Acordo.Acordo Acordo = new Boletos.Acordo.Acordo(Boleto, true);
                                //CompontesBasicos.Performance.Performance.PerformanceST.Registra("interno 5");

                                //if (Acordo.Gerado)
                                dBalancete.BoletosOriginalAcordoRow rowAcordo = DBalancete1.BoletosOriginalAcordo.FindByBOL(Boleto.BOL);
                                if (rowAcordo != null)
                                {
                                    if (rowAcordo.ACOJudicial || rowAcordo.ACOJuridico)
                                        //if (Acordo.Judicial || Acordo.Juridico)
                                        ClaGrafico = ClassePizza.Juridico;
                                    else
                                        ClaGrafico = ClassePizza.Acordo;
                                }
                                else
                                {
                                    ClaGrafico = ClassePizza.Atrasado;
                                }
                            }
                        }
                        CompontesBasicos.Performance.Performance.PerformanceST.Registra("loop");
                    }
                    else
                        ClaGrafico = ClassePizza.NIdentificado;
                    rowBOLRec.ClaGrafico = (int)ClaGrafico;
                    //AcumulaPizza(ClaGrafico, rowBOLRec.BOLValorPago);
                }
                CompontesBasicos.Performance.Performance.PerformanceST.Registra("loop");

            }

            CompontesBasicos.Performance.Performance.PerformanceST.Registra("loop 2");

            foreach (dPrevisto.PrevistoRow rowBOLprev in DPrevisto1.Previsto)
            {
                if (BoletosRec.ContainsKey(rowBOLprev.BOL))
                    Boleto = BoletosRec[rowBOLprev.BOL];
                else
                {
                    BOLrow = dBoletosGeral.BOLetos.FindByBOL(rowBOLprev.BOL);
                    if (BOLrow != null)
                        Boleto = new Boletos.Boleto.Boleto(BOLrow);
                    else
                    {
                        CompontesBasicos.Performance.Performance.PerformanceST.Registra("BOLETO NAO ENCONTRADO");
                        Boleto = new Boletos.Boleto.Boleto(rowBOLprev.BOL);
                    }
                    Boleto.CorrecaoMulta();
                }
                BoletosPrevistos.Add(rowBOLprev.BOL, Boleto);
                //Boleto.CorrecaoPrevisto();
            }
            CompontesBasicos.Performance.Performance.PerformanceST.Registra("Fim");
            //if (CompontesBasicos.FormPrincipalBase.USULogado == 30)
            //{
            //    string resultado = CompontesBasicos.Performance.Performance.PerformanceST.Relatorio();
            //    System.Windows.Forms.Clipboard.SetText(resultado);
            //    System.Windows.Forms.MessageBox.Show(resultado);
            //}
        }

        
        private DataView DV_BalanceteLinkDetalhe_CCS;
         
        
        /// <summary>
        /// BuscaBLDrow
        /// </summary>
        /// <param name="CCS">CCS</param>
        /// <returns></returns>
        public dBalancete.BalanceteLinkDetalheRow BuscaBLDrow(int CCS)
        {             
            if(DV_BalanceteLinkDetalhe_CCS == null)
            {
                DV_BalanceteLinkDetalhe_CCS = new DataView(DBalancete1.BalanceteLinkDetalhe);                
                DV_BalanceteLinkDetalhe_CCS.Sort = "BLD_CCS";                
            }
            int i = DV_BalanceteLinkDetalhe_CCS.Find(CCS);
            return (i < 0) ? null : (dBalancete.BalanceteLinkDetalheRow)(DV_BalanceteLinkDetalhe_CCS[i].Row);            
        }

        /*
        private DataView DV_TabelaVirtual_CCS;


        /// <summary>
        /// BuscaBLDrow
        /// </summary>
        /// <param name="CCS">CCS</param>
        /// <returns></returns>
        public dBalancete.TabelaVirtualRow Busca_TabelaVirtual_CCS(int CCS)
        {
            if (DV_TabelaVirtual_CCS == null)
            {
                DV_TabelaVirtual_CCS = new DataView(DBalancete1.TabelaVirtual);
                DV_TabelaVirtual_CCS.Sort = "CCS";
            }
            int i = DV_TabelaVirtual_CCS.Find(CCS);
            return (i < 0) ? null : (dBalancete.TabelaVirtualRow)(DV_TabelaVirtual_CCS[i].Row);
        }
        */

        /// <summary>
        /// Tipos de link
        /// </summary>
        public enum BLKTipo 
        {
            /// <summary>
            /// Estorno
            /// </summary>
            Estorno = 1,
            /// <summary>
            /// Tranferência física
            /// </summary>
            TransfF = 2,
        }

        private List<BALStatus> StatusEditaveis
        {
            get { return new List<BALStatus> { BALStatus.NaoIniciado,BALStatus.Producao }; }
        }

        private string RepEx(Exception e)
        {
            string Retorno = string.Format("Erro: {0}\r\n\r\n{1}\r\n\r\n", e.Message, e.StackTrace);
            if (e.InnerException != null)
                Retorno += RepEx(e.InnerException);
            return Retorno;
        }        

        /// <summary>
        /// Le todos os extratos e gera tabela virtual.
        /// </summary>
        public void CarregaExtratos(bool AgruparTF)
        {

            if (SalvoXML)
            {
                if (FormPrincipalBase.USULogado == 30)
                    System.Windows.Forms.MessageBox.Show("Salvo XML");
                return;
            }
            if (CON == 774)
            {
                TableAdapter.ST().ExecutarSQLNonQuery("update boletos set BOL_CON = 774 where BOL_CON = 795");
                TableAdapter.ST().ExecutarSQLNonQuery("update BOletoDetalhe set BOD_CON = 774 where BOD_CON = 795");
            }
            foreach (dBalancete.ContaCorrenTeRow rowCCT in DBalancete1.ContaCorrenTe)
                if (!rowCCT.IsRCC_CCTDestinoNull())
                    TableAdapter.ST().ExecutarSQLNonQuery("update ARquivoLido set ARL_CCT = @P1 where ARL_CCT = @P2 and ARL_CCD is null", rowCCT.RCC_CCTDestino, rowCCT.CCT);

            RateiosPorCCT = null;
            string ErroFrancesinha = VerificaFrancesinha();


            if (ErroFrancesinha != "")
            {
                ErroFrancesinha = VerificaFrancesinha();
                if (ErroFrancesinha != "")
                    if (!Silencioso)
                        System.Windows.Forms.MessageBox.Show("Erro crédito de francesinhas não encontrado:\r\n" + ErroFrancesinha);
                    else
                    {
                        UltimoErro = new Exception("Erro crédito de francesinhas não encontrado:\r\n" + ErroFrancesinha);
                        return;
                    }
            }
            //List<int> CCDPular = new List<int>();
            TransfFPendentes = new List<dBalancete.TabelaVirtualRow>();
            EstornosPendentes = new List<dBalancete.TabelaVirtualRow>();
            BODsLigados = new SortedList<decimal, List<int>>();

            if (ESP != null)
                ESP.Espere("Lendo extratos... (1)");
            DBalancete1.SaldoContaCorrenteTableAdapter.Fill(DBalancete1.SaldoContaCorrente, DataI, DataF, CON);
            if (ESP != null)
                ESP.Espere("Lendo extratos... (2)");
            DBalancete1.ContaCorrenteDetalheTableAdapter.FillBy(DBalancete1.ContaCorrenteDetalhe, DataI, DataF, CON);
            if (ESP != null)
                ESP.Espere("Lendo extratos... (3)");
            DBalancete1.ContaCorrenteSubdetalheTableAdapter.FillBy(DBalancete1.ContaCorrenteSubdetalhe, CON, DataI, DataF);
            if (ESP != null)
                ESP.Espere("Lendo extratos... (4)");
            DBalancete1.BalanceteLinKsTableAdapter.Fill(DBalancete1.BalanceteLinKs, BALrow.BAL);
            if (ESP != null)
                ESP.Espere("Lendo extratos... (5)");
            DBalancete1.BalanceteLinkDetalheTableAdapter.FillBy(DBalancete1.BalanceteLinkDetalhe, BALrow.BAL);
            if (ESP != null)
                ESP.Espere("Lendo extratos... (6)");
            DBalancete1.PAGamentosTableAdapter.FillBy(DBalancete1.PAGamentos, CON, DataI, DataF);
            if (ESP != null)
                ESP.Espere("Lendo Tributos eletrônicos...");
            DBalancete1.TributosEletronicosTableAdapter.Fill(DBalancete1.TributosEletronicos, CON, DataI, DataF);
            if (ESP != null)
                ESP.Espere("Lendo Boletos...");
            LeBoletos();
            if (ESP != null)
                ESP.Espere("Calculando previsto...");
            DPrevisto1.Ajuste(this);



            //CCDrastrear = 0;
            //if (!CompontesBasicos.FormPrincipalBase.strEmProducao && !Silencioso)
            //    VirInput.Input.Execute("CCD ratrear", out CCDrastrear);


            //Verifica se tem linha não conciliadas
            bool TemPendentes = false;
            if (ESP != null)
                ESP.Espere("Resolvendo linhas não conciliadas...");
            if (StatusEditaveis.Contains(Status))
                foreach (dBalancete.ContaCorrenteDetalheRow row in DBalancete1.ContaCorrenteDetalhe)
                {
                    if (row.CCDValidado && (((statusconsiliado)row.CCDConsolidado) == statusconsiliado.Aguardando))
                    {
                        dllbanco.Conciliacao.ConciliacaoSt.Detravado = true;
                        if ((CCDrastrearConc != 0) && (CCDrastrearConc == row.CCD))
                            System.Windows.Forms.MessageBox.Show("Rastreador");
                        try
                        {                           
                                dllbanco.Conciliacao.ConciliacaoSt.Consolidar(row.CCD);
                        }
                        catch (Exception e)
                        {
                            VirEmailNeon.EmailDiretoNeon.EmalST.Enviar("luis@virweb.com.br", string.Format("CCD:{0}\r\n{1}", row.CCD, RepEx(e)), "ERRO NO BALANCETE");
                        }
                        TemPendentes = true;
                    }
                }
            if (TemPendentes)
                DBalancete1.ContaCorrenteDetalheTableAdapter.FillBy(DBalancete1.ContaCorrenteDetalhe, DataI, DataF, CON);


            if (ESP != null)
                ESP.Espere("Resolvendo linhas não identificadas...");

            foreach (dBalancete.ContaCorrenteDetalheRow rowCCD in DBalancete1.ContaCorrenteDetalhe)
            {
                if (rowCCD.IsCCDTipoLancamentoNull() || (rowCCD.CCDTipoLancamento == (int)TipoLancamentoNeon.NaoIdentificado))
                {
                    dBalancete.SaldoContaCorrenteRow SCCrow = dBalancete1.SaldoContaCorrente.FindBySCC(rowCCD.CCD_SCC);
                    dBalancete.ContaCorrenTeRow CCTrow = dBalancete1.ContaCorrenTe.FindByCCT(SCCrow.SCC_CCT);
                    if ((CCTrow.CCT_BCO == 237) && (rowCCD.CCDCategoria == 205) && (rowCCD.CCDCodHistorico == 617))
                    {
                        rowCCD.CCDTipoLancamento = (int)TipoLancamentoNeon.rentabilidade;
                        dBalancete1.ContaCorrenteDetalheTableAdapter.Update(rowCCD);
                        rowCCD.AcceptChanges();
                    }
                }
            }


            if (ESP != null)
                ESP.Espere("Carregando Extratos... (1)");

            CompontesBasicos.Performance.Performance.PerformanceST.Zerar();

            nCCT = 0;
            foreach (dBalancete.ContaCorrenTeRow CCTrow in DBalancete1.ContaCorrenTe)
            {
                nCCT++;
                foreach (dBalancete.SaldoContaCorrenteRow SCCrow in CCTrow.GetSaldoContaCorrenteRows())
                {

                    nSeq = 0;
                    foreach (dBalancete.ContaCorrenteDetalheRow CCDrow in SCCrow.GetContaCorrenteDetalheRows())
                    {
                        CompontesBasicos.Performance.Performance.PerformanceST.Registra("Marca 1");
                        if ((CCDrastrearCarga != 0) && (CCDrastrearCarga == CCDrow.CCD))
                        {
                            //rowRastrear = CCDrow;
                            System.Windows.Forms.MessageBox.Show("Rastreador");
                        }

                        decimal EstornoPareado = 0;
                        TipoLancamentoNeon? TipoLancamentoNeon1 = null;


                        //dBalancete.BalanceteLinkDetalheRow BLDrow = BuscaBLDrow(CCDrow.CCD);
                        dBalancete.BalanceteLinkDetalheRow BLDrow = null;

                        decimal TranfPareada = 0;

                        //if (BLDrow == null)
                        //{

                        if (!CCDrow.IsCCD_PLANull() && (CCDrow.CCD_PLA == "930000"))
                            TipoLancamentoNeon1 = TipoLancamentoNeon.Estorno;
                        else
                            if (!CCDrow.IsCCDTipoLancamentoNull())
                            TipoLancamentoNeon1 = ((TipoLancamentoNeon)CCDrow.CCDTipoLancamento);

                        //Correção                        
                        if (dllbanco.Conciliacao.TransferenciaInterna(CCTrow.CCT_BCO, CCDrow.CCDDescricao, CCDrow.CCDCategoria, CCDrow.CCDCodHistorico) && (CCTrow.CCTTipo == (int)CCTTipo.ContaCorrete_com_Oculta))
                        {
                            TableAdapter.ST().ExecutarSQLNonQuery("update ContaCorrenteDetalhe set CCDTipoLancamento = @P2 where CCD = @P1", CCDrow.CCD, (int)TipoLancamentoNeon.TransferenciaInterna);
                            CCDrow.CCDTipoLancamento = (int)TipoLancamentoNeon.TransferenciaInterna;
                            TipoLancamentoNeon1 = TipoLancamentoNeon.TransferenciaInterna;
                        }

                        if ((TipoLancamentoNeon1 == TipoLancamentoNeon.TransferenciaInterna) && (CCTrow.CCTTipo == (int)CCTTipo.ContaCorrete_com_Oculta) && (CCTrow.IsCCT_CCTPlusNull()))
                            continue;

                        if ((TipoLancamentoNeon1 == TipoLancamentoNeon.RendimentoContaInvestimento) && (CCTrow.CCTTipo == (int)CCTTipo.ContaCorrete))
                            continue;

                        if (TipoLancamentoNeon1 == TipoLancamentoNeon.Estorno)
                        {
                            EstornoPareado = amarraEstornos(CCDrow, AgruparTF);
                            if (AgruparTF && (EstornoPareado != 0))
                                continue;
                        }
                        bool CandidatoaTF = (TipoLancamentoNeon1.GetValueOrDefault(TipoLancamentoNeon.NaoIdentificado).EstaNoGrupo(TipoLancamentoNeon.resgate, TipoLancamentoNeon.TransferenciaInterna));

                        if (TipoLancamentoNeon1.GetValueOrDefault(TipoLancamentoNeon.NaoIdentificado) == TipoLancamentoNeon.deposito)
                        {

                            //if (DBalancete1.BOletoDetalheTableAdapter.Fill(DBalancete1.BOletoDetalhe, CCDrow.CCD) > 0)
                            if (TableAdapter.ST().EstaCadastrado("SELECT BOL FROM BOLetos WHERE (BOL_CCD = @P1)", CCDrow.CCD))
                            {
                                //correção os depositos que quitam boletos deveriam ter o código trocado
                                TipoLancamentoNeon1 = TipoLancamentoNeon.creditodecobranca;
                                TableAdapter.ST().ExecutarSQLNonQuery("update ContaCorrenteDetalhe set CCDTipoLancamento = 9 where CCD = @P1", CCDrow.CCD);
                            }

                            if (CCDrow.IsCCD_PLANull())
                                CandidatoaTF = true;
                            else
                                if ((CCDrow.CCD_PLA.Substring(0, 1) == "5") || (CCDrow.CCD_PLA.Substring(0, 1) == "6"))
                                {
                                    CandidatoaTF = true;
                                }
                        }
                        if (CandidatoaTF)
                        {
                            TranfPareada = amarraTransfF(CCDrow, CCTrow.CCT, AgruparTF);
                            if (AgruparTF && (TranfPareada != 0))
                                continue;
                        }
                        CompontesBasicos.Performance.Performance.PerformanceST.Registra("Marca 3");

                        dBalancete.TabelaVirtualRow NovaLinha = DBalancete1.TabelaVirtual.NewTabelaVirtualRow();
                        NovaLinha.Data = SCCrow.SCCData;
                        NovaLinha.Mestre = true;
                        NovaLinha.CCD = CCDrow.CCD;
                        NovaLinha.CCDValor = CCDrow.CCDValor;
                        NovaLinha.Descricao = string.Format("{0}{1}", CCDrow.CCDDescricao.Trim(), (CCDrow.IsCCDDescComplementoNull() || (CCDrow.CCDDescComplemento == "")) ? "" : " | " + CCDrow.CCDDescComplemento.Trim());
                        NovaLinha.DescBalanceteParcial = "";
                        NovaLinha.Ordem = Ordem(SCCrow.SCCData);
                        NovaLinha.Tipo = (int)balancete.TiposLinhas.LinhaCCD;
                        NovaLinha.Valor = CCDrow.CCDValor;
                        NovaLinha.ValorVenA = 0;
                        NovaLinha.ValorVenD = 0;
                        NovaLinha.CCT = CCTrow.CCT;
                        NovaLinha.CTL = DBalancete1.CTLCAIXA;
                        if (!CCDrow.IsCCD_CTLNull())
                            NovaLinha.CTL = CCDrow.CCD_CTL;
                        NovaLinha.Documento = CCDrow.CCDDocumento;
                        if (!CCDrow.IsCCD_PLANull())
                            NovaLinha.PLA = CCDrow.CCD_PLA;
                        else
                        {
                            if (CCDrow.CCDValor > 0)
                                NovaLinha.PLA = "198000";
                            else
                                NovaLinha.PLA = "299000";
                        }
                        if (!CCDrow.IsCCD_DescricaoBALNull())
                            NovaLinha.DescBalanceteParcial = CCDrow.CCD_DescricaoBAL;
                        NovaLinha.CCDConsolidado = CCDrow.CCDConsolidado;
                        if (!CCDrow.IsCCDTipoLancamentoNull())
                            NovaLinha.CCDTipoLancamento = CCDrow.CCDTipoLancamento;
                        DBalancete1.TabelaVirtual.AddTabelaVirtualRow(NovaLinha);
                        dBalancete.TabelaVirtualRow NovaLinhaDet;
                        decimal AcumuladoChecar;

                        CompontesBasicos.Performance.Performance.PerformanceST.Registra("Marca 4");

                        if (TipoLancamentoNeon1.HasValue)
                            switch (TipoLancamentoNeon1.Value)
                            {
                                case TipoLancamentoNeon.Estorno:
                                    CompontesBasicos.Performance.Performance.PerformanceST.Registra("Estorno");
                                    if (BLDrow != null)
                                    {
                                        NovaLinha.Tipo = (int)TiposLinhas.Linkestorno;
                                    }
                                    else
                                    {
                                        if (CCDrow.CCDValor > 0)
                                            NovaLinha.Tipo = (int)TiposLinhas.estornoC;
                                        else
                                            NovaLinha.Tipo = (int)TiposLinhas.estornoD;
                                        if (EstornoPareado == 0)
                                        {
                                            NovaLinha.Erro = CCDrow.CCDValor > 0 ? "Débito não encontrado (Crítico)" : "Crédito não encontrado (Crítico)";
                                            EstornosPendentes.Add(NovaLinha);
                                        }
                                        else
                                        {
                                            NovaLinha.Ordem = EstornoPareado + 1;
                                        }
                                        if (CCDrow.IsCCD_DescricaoBALNull())
                                            NovaLinha.DescBalanceteParcial = string.Format("{0} {1}", CCDrow.CCDDescricao, CCDrow.IsCCDDescComplementoNull() ? "" : CCDrow.CCDDescComplemento);
                                        else
                                            NovaLinha.DescBalanceteParcial = CCDrow.CCD_DescricaoBAL;
                                    }
                                    break;
                                case TipoLancamentoNeon.ChequeDevolvido_Condominio:
                                case TipoLancamentoNeon.ChequeDevolvido_Terceiro:
                                case TipoLancamentoNeon.NaoIdentificado:
                                case TipoLancamentoNeon.PelaDescricao:
                                    CompontesBasicos.Performance.Performance.PerformanceST.Registra("case geral");
                                    if (CCDrow.IsCCD_DescricaoBALNull())
                                        NovaLinha.DescBalanceteParcial = string.Format("{0} {1}", CCDrow.CCDDescricao, CCDrow.IsCCDDescComplementoNull() ? "" : CCDrow.CCDDescComplemento);
                                    break;
                                case TipoLancamentoNeon.Tributo:
                                    bool PrimeiroDetalheTrib = true;
                                    NovaLinhaDet = NovaLinha;
                                    AcumuladoChecar = 0;
                                    dBalancete.TributosEletronicosRow[] Tributos = CCDrow.GetTributosEletronicosRows();
                                    if (Tributos.Length > 0)
                                    {
                                        //CORREÇÂO DE DADOS
                                        foreach (dBalancete.TributosEletronicosRow rowTributo in Tributos)
                                            AcumuladoChecar -= rowTributo.PAGValor;
                                        if (AcumuladoChecar != CCDrow.CCDValor)
                                        {
                                            string ComandoChecar =
"SELECT        SUM(PAGamentos_1.PAGValor) AS Total\r\n" +
"FROM            dbo.GPS INNER JOIN\r\n" +
"                         dbo.PAGamentos ON dbo.GPS.GPS = dbo.PAGamentos.PAG_GPS INNER JOIN\r\n" +
"                         dbo.CHEques ON dbo.PAGamentos.PAG_CHE = dbo.CHEques.CHE INNER JOIN\r\n" +
"                         dbo.PAGamentos AS PAGamentos_1 ON dbo.CHEques.CHE = PAGamentos_1.PAG_CHE\r\n" +
"WHERE        (dbo.GPS.GPS_CCD = @P1);";
                                            string ComandoCorrecao =
"UPDATE       dbo.PAGamentos\r\n" +
"SET                PAG_GPS = GPS.GPS\r\n" +
"FROM            dbo.GPS INNER JOIN\r\n" +
"                         dbo.PAGamentos AS PAGamentos_1 ON dbo.GPS.GPS = PAGamentos_1.PAG_GPS INNER JOIN\r\n" +
"                         dbo.CHEques ON PAGamentos_1.PAG_CHE = dbo.CHEques.CHE INNER JOIN\r\n" +
"                         dbo.PAGamentos ON dbo.CHEques.CHE = dbo.PAGamentos.PAG_CHE\r\n" +
"WHERE        (dbo.GPS.GPS_CCD = @P1);";
                                            decimal? ValorTeste = TableAdapter.ST().BuscaEscalar_decimal(ComandoChecar, CCDrow.CCD);
                                            if (ValorTeste.GetValueOrDefault(0) == -CCDrow.CCDValor)
                                            {
                                                TableAdapter.ST().ExecutarSQLNonQuery(ComandoCorrecao, CCDrow.CCD);
                                                DBalancete1.TributosEletronicosTableAdapter.Fill(DBalancete1.TributosEletronicos, CON, DataI, DataF);
                                                Tributos = CCDrow.GetTributosEletronicosRows();
                                            }
                                        }
                                        AcumuladoChecar = 0;
                                        //CORREÇÂO DE DADOS
                                        foreach (dBalancete.TributosEletronicosRow rowTributo in Tributos)
                                        {
                                            if (!PrimeiroDetalheTrib)
                                            {
                                                NovaLinhaDet = DBalancete1.TabelaVirtual.NewTabelaVirtualRow();
                                                NovaLinhaDet.Mestre = false;
                                                NovaLinhaDet.Data = SCCrow.SCCData;
                                                NovaLinhaDet.CCD = CCDrow.CCD;
                                                NovaLinhaDet.CCDValor = CCDrow.CCDValor;
                                                NovaLinhaDet.Descricao = string.Format("{0}{1}{2}", CCDrow.CCDDescricao.Trim(), (CCDrow.IsCCDDescComplementoNull() || (CCDrow.CCDDescComplemento == "")) ? "" : " | ", CCDrow.CCDDescComplemento.Trim());
                                                NovaLinhaDet.Ordem = Ordem(SCCrow.SCCData);
                                                NovaLinhaDet.CCT = CCTrow.CCT;
                                                NovaLinhaDet.CCDConsolidado = CCDrow.CCDConsolidado;
                                                NovaLinhaDet.CCDTipoLancamento = CCDrow.CCDTipoLancamento;
                                                NovaLinhaDet.Documento = CCDrow.CCDDocumento;
                                                DBalancete1.TabelaVirtual.AddTabelaVirtualRow(NovaLinhaDet);
                                            };
                                            NovaLinhaDet.Tipo = (int)balancete.TiposLinhas.pagEletronico;
                                            TipoImposto Timp = (TipoImposto)rowTributo.PAGIMP;
                                            NovaLinhaDet.CHE = rowTributo.PAG_CHE;
                                            NovaLinhaDet.NOA = rowTributo.NOA;
                                            if (!rowTributo.IsNOA_PGFNull())
                                                NovaLinhaDet.PGF = rowTributo.NOA_PGF;
                                            NovaLinhaDet.Valor = -rowTributo.PAGValor;
                                            NovaLinhaDet.PLA = rowTributo.NOA_PLA;
                                            if (!rowTributo.IsPAG_CTLNull())
                                                NovaLinhaDet.CTL = rowTributo.PAG_CTL;
                                            else
                                                NovaLinhaDet.CTL = DBalancete1.CTLCAIXA;
                                            NovaLinhaDet.PAG = rowTributo.PAG;
                                            if (!rowTributo.IsNOAServicoBalNull())
                                                NovaLinhaDet.DescBalanceteParcial = rowTributo.NOAServicoBal;
                                            else
                                                NovaLinhaDet.DescBalanceteParcial = rowTributo.NOAServico;
                                            NovaLinhaDet.Prefixo = PrefixoTributo(Timp); ;
                                            PrimeiroDetalheTrib = false;
                                            AcumuladoChecar += NovaLinhaDet.Valor;
                                        }
                                        if (AcumuladoChecar != CCDrow.CCDValor)
                                        {
                                            NovaLinhaDet.Erro = string.Format("Valor incorreto\r\n" +
                                                                              "  Soma dos itens: {0,6:n2}\r\n" +
                                                                              "  Extrato       : {1,6:n2}",
                                                              AcumuladoChecar,
                                                              CCDrow.CCDValor);
                                        }
                                    }
                                    else
                                    {
                                        if (string.IsNullOrEmpty(NovaLinha.DescBalanceteParcial))
                                            NovaLinha.DescBalanceteParcial = string.Format("{0} {1}", CCDrow.CCDDescricao, CCDrow.IsCCDDescComplementoNull() ? "" : CCDrow.CCDDescComplemento);
                                    }
                                    break;
                                case TipoLancamentoNeon.TransferenciaInterna:
                                    if (CCDrow.IsCCD_DescricaoBALNull())
                                        NovaLinha.DescBalanceteParcial = string.Format("Transf. Interna: {0} {1}", CCDrow.CCDDescricao, CCDrow.IsCCDDescComplementoNull() ? "" : CCDrow.CCDDescComplemento);
                                    break;
                                /*
                               case TipoLancamentoNeon.Salarios:                                    
                                   NovaLinha.Tipo = (int)balancete.TiposLinhas.DetalhePAG;
                                   if (CCDrow.IsCCD_PLANull())
                                       NovaLinha.PLA = "220001";
                                   if (CCDrow.IsCCD_DescricaoBALNull())
                                       NovaLinha.DescBalanceteParcial = "Salarios";
                                   break;*/
                                case TipoLancamentoNeon.DespesasBancarias:
                                    NovaLinha.Tipo = (int)balancete.TiposLinhas.DetalhePAG;
                                    if (CCDrow.IsCCD_PLANull())
                                        NovaLinha.PLA = "240005";
                                    if (CCDrow.IsCCD_DescricaoBALNull())
                                        NovaLinha.DescBalanceteParcial = "Despesas Bancárias";
                                    break;
                                case TipoLancamentoNeon.resgate:
                                    NovaLinha.Tipo = (int)(CCDrow.CCDValor > 0 ? balancete.TiposLinhas.TransferenciaFC : balancete.TiposLinhas.TransferenciaFD);
                                    if (TranfPareada == 0)
                                    {
                                        NovaLinha.Erro = CCDrow.CCDValor > 0 ? "Débito não encontrado (Crítico)" : "Crédito não encontrado (Crítico)";
                                        TransfFPendentes.Add(NovaLinha);
                                    }
                                    else
                                        NovaLinha.Ordem = TranfPareada + 1;
                                    break;
                                case TipoLancamentoNeon.rentabilidade:
                                case TipoLancamentoNeon.RendimentoContaInvestimento:
                                    NovaLinha.Tipo = (int)balancete.TiposLinhas.Rentabilidade;
                                    NovaLinha.PLA = "170002";
                                    if (CCDrow.IsCCD_CTLNull())
                                        NovaLinha.SetCTLNull();
                                    break;
                                case TipoLancamentoNeon.deposito:
                                    CompontesBasicos.Performance.Performance.PerformanceST.Registra("case deposito");
                                    NovaLinha.Tipo = (int)balancete.TiposLinhas.Deposito;
                                    if (TranfPareada != 0)
                                    {
                                        NovaLinha.Ordem = TranfPareada + 1;
                                        NovaLinha.Tipo = (int)TiposLinhas.TransferenciaFC;
                                    }
                                    else
                                        if (!CCDrow.IsCCD_PLANull() && ((CCDrow.CCD_PLA.Substring(0, 1) == "6") || (CCDrow.CCD_PLA.Substring(0, 1) == "5")))
                                        {
                                            NovaLinha.Tipo = (int)TiposLinhas.TransferenciaFC;
                                            NovaLinha.Erro = "Débito não encontrado (Crítico)";
                                            TransfFPendentes.Add(NovaLinha);
                                        }
                                    break;
                                case TipoLancamentoNeon.creditodecobranca:
                                case TipoLancamentoNeon.creditodecobrancaManual:
                                    CompontesBasicos.Performance.Performance.PerformanceST.Registra("case cobrança");
                                    DBalancete1.BOletoDetalheBODs.Clear();

                                    DBalancete1.BOletoDetalhe.Clear();

                                    //DBalancete1.BOletoDetalheTableAdapter.Fill(DBalancete1.BOletoDetalhe, CCDrow.CCD);
                                    DBalancete1.BOletoDetalheBODsTableAdapter.Fill(DBalancete1.BOletoDetalheBODs, CCDrow.CCD);

                                    SortedList<int, List<int>> BodsNovasLinhas = new SortedList<int, List<int>>();

                                    foreach (dBalancete.BOletoDetalheBODsRow rowBOD in DBalancete1.BOletoDetalheBODs)
                                    {
                                        string BODMensagemBal = rowBOD.IsBODMensagemBalanceteNull() ? rowBOD.BODMensagem : rowBOD.BODMensagemBalancete;
                                        int CTL = DBalancete1.CTLCAIXA;
                                        if (!rowBOD.IsBOD_CTLNull())
                                            CTL = rowBOD.BOD_CTL;
                                        else
                                            if (rowBOD.BOD_PLA == "110000")
                                                CTL = DBalancete1.CTLFUNDO;
                                        if (rowBOD.BODValor > 0)
                                            BODMensagemBal = DescLimpa(rowBOD.BODMensagem, rowBOD.BOD_PLA);
                                        //rowBOD.BODMensagem = DescLimpa(rowBOD.BODMensagem, rowBOD.BOD_PLA);
                                        dBalancete.BOletoDetalheRow rowBODAc = DBalancete1.BOletoDetalhe.FindByBOD_PLABOD_CTLBODMensagem(rowBOD.BOD_PLA, CTL, BODMensagemBal);
                                        if (rowBODAc == null)
                                        {
                                            rowBODAc = DBalancete1.BOletoDetalhe.AddBOletoDetalheRow(rowBOD.BOD_PLA, CTL, rowBOD.BODValor, BODMensagemBal);
                                            BodsNovasLinhas.Add(rowBODAc.ID, new List<int>());

                                        }
                                        else
                                            rowBODAc.ValorParcial = rowBODAc.ValorParcial + rowBOD.BODValor;
                                        BodsNovasLinhas[rowBODAc.ID].Add(rowBOD.BOD);
                                    }

                                    AcumuladoChecar = 0;

                                    List<dBalancete.TabelaVirtualRow> LinhasDoGrupo = new List<dBalancete.TabelaVirtualRow>();



                                    foreach (dBalancete.BOletoDetalheRow rowBODAc in DBalancete1.BOletoDetalhe)
                                    {

                                        //int CTL = DBalancete1.CTLCAIXA;
                                        //if (!rowBODAc.IsBOD_CTLNull())
                                        //    CTL = rowBODAc.BOD_CTL;
                                        //else
                                        //    if (rowBODAc.BOD_PLA == "110000")
                                        //        CTL = DBalancete1.CTLFUNDO;


                                        NovaLinhaDet = CriaLinhaCredito(NovaLinha, CCTrow, SCCrow, CCDrow, LinhasDoGrupo, rowBODAc.BOD_PLA, rowBODAc.ValorParcial, rowBODAc.BOD_CTL, rowBODAc.BODMensagem);
                                        AcumuladoChecar += NovaLinhaDet.Valor;


                                        //List<int> BODs = new List<int>();
                                        foreach (dBalancete.BOletoDetalheBODsRow rowBOD in rowBODAc.GetBOletoDetalheBODsRows())
                                        {
                                            if (rowBOD.BOD_PLA == "")
                                                continue;
                                            //BODs.Add(rowBOD.BOD);
                                            if (rowBOD.BOLVencto < DataI)
                                                NovaLinhaDet.ValorVenA += rowBOD.BODValor;
                                            else if (rowBOD.BOLVencto > DataF)
                                                NovaLinhaDet.ValorVenD += rowBOD.BODValor;

                                        }
                                        //BODsLigados.Add(NovaLinhaDet.Ordem, BODs);
                                        BODsLigados.Add(NovaLinhaDet.Ordem, BodsNovasLinhas[rowBODAc.ID]);
                                    }
                                    //foreach(dBalancete.ContaCorrenteSubdetalheRow CCSrow in CCDrow.GetContaCorrenteSubdetalheRows())
                                    //{
                                    //    dBalancete.TabelaVirtualRow LinhaSub = CriaLinhaCredito(NovaLinha, CCTrow, SCCrow, CCDrow, LinhasDoGrupo, CCSrow.CCS_PLA,CCSrow.CCSValor, CCSrow.CCS_CTL, CCSrow.CCSDescricaoBAL);
                                    //    LinhaSub.CCS = CCSrow.CCS;
                                    //    AcumuladoChecar += CCSrow.CCSValor;
                                    //}
                                    if (AcumuladoChecar != CCDrow.CCDValor)
                                    {

                                        string Erro = string.Format("Valor incorreto\r\n" +
                                            //"  Soma dos itens dos boletos: {0,6:n2}\r\n" +
                                                                          "  Soma dos boletos          : {0,6:n2}\r\n" +
                                                                          "  Extrato                   : {1,6:n2}\r\n" +
                                                                          "  Crédito não identificado  : {2,6:n2}",
                                                          AcumuladoChecar,
                                                          CCDrow.CCDValor,
                                                          NovaLinha.CCDValor - AcumuladoChecar);


                                        //dBalancete.TabelaVirtualRow LinhaNaoIdentificado = CriaLinhaCredito(NovaLinha, CCTrow, SCCrow, CCDrow, LinhasDoGrupo, "198000", NovaLinha.CCDValor - AcumuladoChecar, DBalancete1.CTLCAIXA, "Crédito não identificado");
                                        dBalancete.TabelaVirtualRow LinhaNaoIdentificado = CriaLinhaCredito(NovaLinha, CCTrow, SCCrow, CCDrow, LinhasDoGrupo, "198000", NovaLinha.CCDValor - AcumuladoChecar, DBalancete1.CTLCREDITOS_Anteriores, "Crédito não identificado");
                                        LinhaNaoIdentificado.Erro = Erro;
                                        LinhaNaoIdentificado.SaldoCredito = true;
                                    }

                                    //incluir o não identificado nos créditos
                                    break;
                                case TipoLancamentoNeon.debitoautomatico:
                                case TipoLancamentoNeon.Cheque:
                                case TipoLancamentoNeon.PagamentoEletronico:
                                case TipoLancamentoNeon.Salarios:
                                    bool PrimeiroDetalhe = true;
                                    NovaLinhaDet = NovaLinha;
                                    AcumuladoChecar = 0;
                                    dBalancete.PAGamentosRow[] PAGacrows = CCDrow.GetPAGamentosRows();
                                    if (PAGacrows.Length != 0)
                                    {
                                        foreach (dBalancete.PAGamentosRow PAGacrow in PAGacrows)
                                        {
                                            if (!PrimeiroDetalhe)
                                            {
                                                NovaLinhaDet = DBalancete1.TabelaVirtual.NewTabelaVirtualRow();
                                                NovaLinhaDet.Mestre = false;
                                                NovaLinhaDet.Data = SCCrow.SCCData;
                                                NovaLinhaDet.CCD = CCDrow.CCD;
                                                NovaLinhaDet.CCDValor = CCDrow.CCDValor;
                                                NovaLinhaDet.Descricao = string.Format("{0}{1}{2}", CCDrow.CCDDescricao.Trim(), (CCDrow.IsCCDDescComplementoNull() || (CCDrow.CCDDescComplemento == "")) ? "" : " | ", CCDrow.CCDDescComplemento.Trim());
                                                NovaLinhaDet.Ordem = Ordem(SCCrow.SCCData);
                                                NovaLinhaDet.CCT = CCTrow.CCT;
                                                NovaLinhaDet.CCDConsolidado = CCDrow.CCDConsolidado;
                                                NovaLinhaDet.CCDTipoLancamento = CCDrow.CCDTipoLancamento;
                                                NovaLinhaDet.Documento = CCDrow.CCDDocumento;
                                                DBalancete1.TabelaVirtual.AddTabelaVirtualRow(NovaLinhaDet);
                                            };

                                            NovaLinhaDet.CHE = PAGacrow.CHE;
                                            NovaLinhaDet.NOA = PAGacrow.NOA;
                                            if (!PAGacrow.IsNOA_PGFNull())
                                                NovaLinhaDet.PGF = PAGacrow.NOA_PGF;

                                            AcumuladoChecar -= PAGacrow.Valorparcial;
                                            NovaLinhaDet.Valor = -PAGacrow.Valorparcial;
                                            NovaLinhaDet.PLA = PAGacrow.NOA_PLA;
                                            if ((NovaLinhaDet.PLA.Substring(0, 1) == "5") || (NovaLinhaDet.PLA.Substring(0, 1) == "6"))
                                            {
                                                NovaLinhaDet.Tipo = (int)balancete.TiposLinhas.TransferenciaFD;
                                                TransfFPendentes.Add(NovaLinhaDet);
                                                NovaLinhaDet.Erro = "Crédito não encontrado (Crítico)";

                                            }
                                            else
                                                NovaLinhaDet.Tipo = TipoLancamentoNeon1.Value == TipoLancamentoNeon.PagamentoEletronico ? (int)balancete.TiposLinhas.pagEletronico : (int)balancete.TiposLinhas.DetalhePAG;
                                            if (!PAGacrow.IsPAG_CTLNull())
                                                NovaLinhaDet.CTL = PAGacrow.PAG_CTL;
                                            else
                                                NovaLinhaDet.CTL = DBalancete1.CTLCAIXA;
                                            NovaLinhaDet.PAG = PAGacrow.PAG;
                                            if (!PAGacrow.IsNOAServicoBalNull())
                                                NovaLinhaDet.DescBalanceteParcial = PAGacrow.NOAServicoBal;
                                            else
                                                NovaLinhaDet.DescBalanceteParcial = PAGacrow.NOAServico;

                                            string Prefixo = "";
                                            if (!PAGacrow.IsPAGNNull())
                                            {
                                                if ((PAGacrow.PAGN != "1/1") && (PAGacrow.PAGN != ""))
                                                    Prefixo = PAGacrow.PAGN.Replace("/", "ª/") + " ";
                                            }

                                            if (!PAGacrow.IsPAGIMPNull())
                                            {
                                                TipoImposto Timp = (TipoImposto)PAGacrow.PAGIMP;
                                                Prefixo += PrefixoTributo(Timp);
                                                /*
                                                switch (Timp)
                                                {
                                                    case TipoImposto.COFINS:
                                                        Prefixo += "COFINS - ";
                                                        break;
                                                    case TipoImposto.CSLL:
                                                        Prefixo += "CSLL - ";
                                                        break;
                                                    case TipoImposto.INSS:
                                                    case TipoImposto.INSSpf:
                                                    case TipoImposto.INSSpfEmp:
                                                    case TipoImposto.INSSpfRet:
                                                        Prefixo += "INSS - ";
                                                        break;
                                                    case TipoImposto.IR:
                                                        Prefixo += "IR - ";
                                                        break;
                                                    case TipoImposto.ISSQN:
                                                    case TipoImposto.ISS_SA:
                                                        Prefixo += "ISS - ";
                                                        break;
                                                    case TipoImposto.PIS:
                                                        Prefixo += "PIS - ";
                                                        break;
                                                    case TipoImposto.PIS_COFINS_CSLL:
                                                        Prefixo += "PIS/COFINS/CSLL - ";
                                                        break;
                                                    default:
                                                        break;
                                                }*/
                                            }
                                            NovaLinhaDet.Prefixo = Prefixo;


                                            PrimeiroDetalhe = false;
                                        }
                                    }
                                    else
                                        NovaLinhaDet.Tipo = (int)balancete.TiposLinhas.DetalhePAG;

                                    if (AcumuladoChecar != CCDrow.CCDValor)
                                        NovaLinhaDet.Erro = string.Format("Valor incorreto\r\n" +
                                                                              "  Soma dos itens do cheque: {0,6:n2}\r\n" +
                                                                              "  Extrato                 : {1,6:n2}",
                                                              AcumuladoChecar,
                                                              CCDrow.CCDValor);



                                    break;
                                default:
                                    throw new NotImplementedException(string.Format("Tipo não tratado {0}", TipoLancamentoNeon1.Value));
                            }
                    }
                }
            }
            CompontesBasicos.Performance.Performance.PerformanceST.Relatorio();

            if (ESP != null)
                ESP.Espere("Carregando Extratos... (2)");

            CCSs = new SortedList<string, List<dBalancete.TabelaVirtualRow>>();
            foreach (dBalancete.ContaCorrenteSubdetalheRow CCSrow in dBalancete1.ContaCorrenteSubdetalhe)
            {
                //if(CCSrow.CCS_PLA.StartsWith("2"))
                //    CCDrastrear = 0;
                IncluiCCS(CCSrow);
            }
            foreach (dBalancete.ContaCorrenteSubdetalheRow CCSrow in dBalancete1.ContaCorrenteSubdetalhe)
            {
                if (CCSrow.GetBalanceteLinkDetalheRows().Length > 0)
                {
                    dBalancete.BalanceteLinkDetalheRow rowBLD = CCSrow.GetBalanceteLinkDetalheRows()[0];
                    balancete.BLKTipo BLKTipo = (balancete.BLKTipo)rowBLD.BalanceteLinKsRow.BLKTipo;
                    dBalancete.TabelaVirtualRow rowEstornar = BuscaporCCS(CCSrow.CCS);
                    if (rowEstornar != null)
                    {
                        rowEstornar.Tipo = (int)(BLKTipo == balancete.BLKTipo.Estorno ? TiposLinhas.Linkestorno : TiposLinhas.LinkTransferenciaF);
                        rowEstornar.SetCTLNull();
                        rowEstornar.SetCTL2Null();
                        rowEstornar.SetErroNull();
                    }
                }
            }

            if (ESP != null)
                ESP.Espere("Tranferências...");

            CarregaTransferencias();

            if (ESP != null)
                ESP.Espere("Boletos zerados...");

            CarregaBoleosZerados();

            if (ESP != null)
                ESP.Espere("Validando...");

            Validado();
        }

        private string PrefixoTributo(TipoImposto Timp)
        {            
            switch (Timp)
            {
                case TipoImposto.COFINS:
                    return "COFINS - ";                    
                case TipoImposto.CSLL:
                    return "CSLL - ";                  
                case TipoImposto.INSS:
                case TipoImposto.INSSpf:
                case TipoImposto.INSSpfEmp:
                case TipoImposto.INSSpfRet:
                    return "INSS - ";                
                case TipoImposto.IR:
                    return "IR - ";                  
                case TipoImposto.ISSQN:
                case TipoImposto.ISS_SA:
                    return "ISS - ";                    
                case TipoImposto.PIS:
                    return "PIS - ";                    
                case TipoImposto.PIS_COFINS_CSLL:
                    return "PIS/COFINS/CSLL - ";                    
                default:
                    return "";
            }
        }

        private DataView DV_TabelaVirtual_CCS;

        /// <summary>
        /// BuscaBLDrow
        /// </summary>
        /// <param name="CCS"></param>
        /// <returns></returns>
        public dBalancete.TabelaVirtualRow BuscaporCCS(int CCS)
        {
            if (DV_TabelaVirtual_CCS == null)
            {
                DV_TabelaVirtual_CCS = new DataView(DBalancete1.TabelaVirtual);
                DV_TabelaVirtual_CCS.Sort = "CCS";
            }
            int i = DV_TabelaVirtual_CCS.Find(CCS);
            return (i < 0) ? null : (dBalancete.TabelaVirtualRow)(DV_TabelaVirtual_CCS[i].Row);
        }

        private void CarregaBoleosZerados()
        {
            foreach (Boletos.Boleto.Boleto Bolz in BoletosZerados.Values)
            {
                bool PrimeiraLinha = true;
                //SortedList<decimal, BoletosProc.Boleto.dBoletos.BOletoDetalheRow> rowBODs = new SortedList<decimal, BoletosProc.Boleto.dBoletos.BOletoDetalheRow>();
                 List<BoletosProc.Boleto.dBoletos.BOletoDetalheRow> rowBODs = new List<BoletosProc.Boleto.dBoletos.BOletoDetalheRow>();
                foreach (BoletosProc.Boleto.dBoletos.BOletoDetalheRow rowBOD in Bolz.rowPrincipal.GetBOletoDetalheRows())
                    rowBODs.Add(rowBOD);

                //foreach (BoletosProc.Boleto.dBoletos.BOletoDetalheRow rowBOD in Bolz.rowPrincipal.GetBOletoDetalheRows())
                //foreach (BoletosProc.Boleto.dBoletos.BOletoDetalheRow rowBOD in rowBODs.Values)
                foreach (BoletosProc.Boleto.dBoletos.BOletoDetalheRow rowBOD in rowBODs)
                {
                    dBalancete.TabelaVirtualRow Novarow = DBalancete1.TabelaVirtual.NewTabelaVirtualRow();
                    Novarow.Mestre = PrimeiraLinha;
                    Novarow.Data = Bolz.rowPrincipal.BOLPagamento.Date;
                    Novarow.CCDValor = 0;
                    Novarow.Descricao = "Boleto Zerado";
                    Novarow.Ordem = Ordem(Novarow.Data, true);
                    if (!rowBOD.IsBOD_CTLNull())
                        Novarow.CTL = rowBOD.BOD_CTL;
                    else
                        if (rowBOD.BOD_PLA == "110000")
                            Novarow.CTL = DBalancete1.CTLFUNDO;
                        else
                            Novarow.CTL = DBalancete1.CTLCAIXA;
                    Novarow.DescBalancete = rowBOD.BODMensagem;
                    Novarow.DescBalanceteParcial = rowBOD.BODMensagem;
                    if(rowBOD.BOD_PLA.Substring(0, 1) == "2")
                    {
                        Novarow.Desp_Cred = "D"; 
                        Novarow.Tipo = (int)TiposLinhas.despesanoboleto;
                    }
                    else
                    {
                        Novarow.Desp_Cred = "C"; 
                        Novarow.Tipo = (int)TiposLinhas.DetalheBOL;
                    }
                    Novarow.PLA = rowBOD.BOD_PLA;
                    Novarow.Valor = rowBOD.BODValor;
                    //Novarow.CCD = -Bolz.rowPrincipal.BOL;
                    Novarow.BOD = rowBOD.BOD;
                    Novarow.BOL = rowBOD.BOD_BOL;
                    //Novarow.CCDTipoLancamento = 
                    //NovaLinhaDet.Documento = CCDrow.CCDDocumento;
                    DBalancete1.TabelaVirtual.AddTabelaVirtualRow(Novarow);
                    PrimeiraLinha = false;
                }
            }
        }

        /// <summary>
        /// Conjunto dos CCSs. ATENÇÃO a key é CCD-PAG
        /// </summary>
        private SortedList<string, List<dBalancete.TabelaVirtualRow>> CCSs;

        private string ChaveCCSs(dBalancete.TabelaVirtualRow row)
        {
            return row.IsPAGNull() ? string.Format("{0}", row.CCD) : string.Format("{0}-{1}", row.CCD, row.PAG);            
        }

        internal bool removerCCS(dBalancete.ContaCorrenteSubdetalheRow rowCCS, dBalancete.TabelaVirtualRow row)
        {
            bool retorno = true;
            List<dBalancete.TabelaVirtualRow> Linhas = CCSs[ChaveCCSs(row)];
            if (Linhas.Count > 1)
            {
                Linhas.Remove(row);
                row.Delete();                
            }
            else
            {
                retorno = false;                
            }
            rowCCS.Delete();
            DBalancete1.ContaCorrenteSubdetalheTableAdapter.Update(rowCCS);
            DBalancete1.ContaCorrenteSubdetalhe.AcceptChanges();
            return retorno;
        }

        /// <summary>
        /// Inclui CCS
        /// </summary>
        /// <param name="CCSrow"></param>
        public void IncluiCCS(dBalancete.ContaCorrenteSubdetalheRow CCSrow)
        {
            
            dBalancete.TabelaVirtualRow rowBase = null;
            if (CCSrow.IsCCS_PAGNull())
            {
                foreach (dBalancete.TabelaVirtualRow Candidato in dBalancete1.TabelaVirtual)
                    if ((!Candidato.IsCCDNull()) && (CCSrow.CCS_CCD == Candidato.CCD) && (Candidato.IsPAGNull()))
                    {
                        if ((rowBase == null) || (rowBase.Ordem < Candidato.Ordem))
                            rowBase = Candidato;
                    }                
            }
            else
            {
                foreach(dBalancete.TabelaVirtualRow Candidato in dBalancete1.TabelaVirtual)
                    if ((!Candidato.IsCCDNull()) && (CCSrow.CCS_CCD == Candidato.CCD) && (!Candidato.IsPAGNull()) && (CCSrow.CCS_PAG == Candidato.PAG))
                    {
                        if ((rowBase == null) || (rowBase.Ordem < Candidato.Ordem))
                            rowBase = Candidato;
                    }                
            }
            if (rowBase == null)
                return;
            if (!rowBase.IsCCSNull())
            {
                //clonando
                dBalancete.TabelaVirtualRow novarowVirtual = dBalancete1.TabelaVirtual.NewTabelaVirtualRow();
                foreach (DataColumn DC in rowBase.Table.Columns)
                    if (!DC.ReadOnly)
                        novarowVirtual[DC] = rowBase[DC];
                novarowVirtual.Mestre = false;
                novarowVirtual.Ordem = novarowVirtual.Ordem + 0.1M;
                novarowVirtual.CCS = CCSrow.CCS;           
                //if(rowBase.CCD == 1854494)
                //    novarowVirtual.Tipo = TiposLinhas.Linkestorno
                dBalancete1.TabelaVirtual.AddTabelaVirtualRow(novarowVirtual);
                rowBase = novarowVirtual;
            }
            else
            {
                //O valor original é guardado no campo rowBase.CCSSoma para ser validado
                rowBase.CCSSoma = rowBase.Valor;
            }
            List<dBalancete.TabelaVirtualRow> CCSs_CCD;
            /*
            if (CCSs.ContainsKey(CCSrow.CCS_CCD))
                CCSs_CCD = CCSs[CCSrow.CCS_CCD];
            else
            {
                CCSs_CCD = new List<dBalancete.TabelaVirtualRow>();
                CCSs.Add(CCSrow.CCS_CCD, CCSs_CCD);
            };
            */
            string chave = ChaveCCSs(rowBase);
            if (CCSs.ContainsKey(chave))
                CCSs_CCD = CCSs[chave];
            else
            {
                CCSs_CCD = new List<dBalancete.TabelaVirtualRow>();
                CCSs.Add(chave, CCSs_CCD);
            };


            CCSs_CCD.Add(rowBase);
            rowBase.CCS = CCSrow.CCS;            
            rowBase.CTL = CCSrow.CCS_CTL;
            rowBase.PLA = CCSrow.CCS_PLA;
            rowBase.DescBalanceteParcial = CCSrow.CCSDescricaoBAL;
            rowBase.Valor = CCSrow.CCSValor;
            if (rowBase.PLA.StartsWith("2"))
            {
                rowBase.Desp_Cred = "D";
                if(rowBase.Tipo == (int)TiposLinhas.DetalheBOL)
                    rowBase.Tipo = (int)TiposLinhas.despesanoboleto;
            }
            //if (CCSrow.GetBalanceteLinkDetalheRows().Length > 0)
            //    rowBase.Tipo = (int)TiposLinhas.Linkestorno;
        }

        //dBalancete.SaldoContaCorrenteRow SCCrow,dBalancete.ContaCorrenteDetalheRow CCDrow

        private dBalancete.TabelaVirtualRow CriaLinhaCredito(    dBalancete.TabelaVirtualRow primeiraLinha, 
                                                                 dBalancete.ContaCorrenTeRow CCTrow,
                                                                 dBalancete.SaldoContaCorrenteRow SCCrow,
                                                                 dBalancete.ContaCorrenteDetalheRow CCDrow,
                                                                 List<dBalancete.TabelaVirtualRow> LinhasDoGrupo,
                                                                 string PLA,
                                                                 decimal Valor,
                                                                 int CTL,
                                                                 string Descricao)
        {
            dBalancete.TabelaVirtualRow NovaLinhaDet;
            if (LinhasDoGrupo.Count != 0)
            {
                NovaLinhaDet = DBalancete1.TabelaVirtual.NewTabelaVirtualRow();
                NovaLinhaDet.Mestre = false;
                NovaLinhaDet.Data = SCCrow.SCCData;
                NovaLinhaDet.CCD = CCDrow.CCD;
                NovaLinhaDet.CCDValor = CCDrow.CCDValor;
                if (CCDrow.IsCCDDescComplementoNull())
                {
                    CCDrow.CCDDescComplemento = "";
                    TableAdapter.ST().ExecutarSQLNonQuery("update contacorrentedetalhe set CCDDescComplemento = '' where ccd = @P1",CCDrow.CCD);
                }
                NovaLinhaDet.Descricao = string.Format("{0}{1}{2}", CCDrow.CCDDescricao.Trim(), (CCDrow.IsCCDDescComplementoNull() || (CCDrow.CCDDescComplemento == "")) ? "" : " | ", CCDrow.CCDDescComplemento.Trim());
                NovaLinhaDet.DescBalanceteParcial = "";
                NovaLinhaDet.Ordem = Ordem(SCCrow.SCCData);
                //NovaLinhaDet.Tipo = (int)TiposLinhas.DetalheBOL;
                NovaLinhaDet.CCT = CCTrow.CCT;
                NovaLinhaDet.CCDConsolidado = CCDrow.CCDConsolidado;
                NovaLinhaDet.CCDTipoLancamento = CCDrow.CCDTipoLancamento;
                DBalancete1.TabelaVirtual.AddTabelaVirtualRow(NovaLinhaDet);
            }
            else
                NovaLinhaDet = primeiraLinha;
            NovaLinhaDet.ValorVenA = 0;
            NovaLinhaDet.ValorVenD = 0;
            LinhasDoGrupo.Add(NovaLinhaDet);            
            //AcumuladoChecar += rowBODAc.ValorParcial;
            NovaLinhaDet.Valor = Valor;
            NovaLinhaDet.PLA = PLA;
            if ((PLA != "") && (PLA.Substring(0, 1) == "2"))
            {
                if (CCDrow.CCDTipoLancamento == (int)TipoLancamentoNeon.creditodecobrancaManual)
                    NovaLinhaDet.Tipo = (int)balancete.TiposLinhas.despesanoboletoManual;
                else
                    NovaLinhaDet.Tipo = (int)balancete.TiposLinhas.despesanoboleto;
            }
            else
            {
                if (CCDrow.CCDTipoLancamento == (int)TipoLancamentoNeon.creditodecobrancaManual)
                    NovaLinhaDet.Tipo = (int)balancete.TiposLinhas.DetalheBOLManual;
                else
                    NovaLinhaDet.Tipo = (int)balancete.TiposLinhas.DetalheBOL;
            }
            NovaLinhaDet.CTL = CTL;
            //if (rowBODAc.BOD_PLA == "110000")
            //    NovaLinhaDet.CTL = DBalancete1.CTLFUNDO;
            //else
            //    NovaLinhaDet.CTL = DBalancete1.CTLCAIXA;
            //if (!rowBODAc.IsBOD_CTLNull())
            //    NovaLinhaDet.CTL = rowBODAc.BOD_CTL;
            NovaLinhaDet.DescBalanceteParcial = Descricao;
            ///////////////////

            return NovaLinhaDet;
        }

        /// <summary>
        /// Carrega as tranferências lógicas
        /// </summary>
        public void CarregaTransferencia(TransfL TransfLx)
        {                        
                dBalancete.TabelaVirtualRow rowTx = DBalancete1.TabelaVirtual.NewTabelaVirtualRow();
                rowTx.Mestre = true;
                rowTx.Ordem = Ordem(TransfLx.rowTRL.TRLData, true);
                rowTx.Valor = TransfLx.rowTRL.TRLValor;
                rowTx.CCDValor = TransfLx.rowTRL.TRLValor;
                rowTx.CTL = TransfLx.rowTRL.TRLDe_CTL;
                rowTx.CTL2 = TransfLx.rowTRL.TRLPara_CTL;
                rowTx.Data = TransfLx.rowTRL.TRLData;
                rowTx.Descricao = "TRANFERENCIA LÓGICA";
                //rowTx.Valor = -TLDrow.TLDValor;
                //rowTx.Ordem = Ordem(TransfLx.rowTRL.TRLData);
                rowTx.Tipo = (int)TiposLinhas.TransferenciaL;
                rowTx.TRL = TransfLx.rowTRL.TRL;
                DBalancete1.TabelaVirtual.AddTabelaVirtualRow(rowTx);
                rowTx.AcceptChanges();
        }

        

        private void CarregaTransferencias()
        {
            TransfLs = TransfL.Transf_Datas(CON, DataI, DataF);            
            nSeqTL = 0;
            foreach (TransfL TransfLx in TransfLs.Values)
                CarregaTransferencia(TransfLx);
        }

        /// <summary>
        /// Tela de espera
        /// </summary>
        public CompontesBasicos.Espera.cEspera ESP;

        /// <summary>
        /// Abrir
        /// </summary>
        /// <param name="SomenteLeitura"></param>
        /// <param name="Pop"></param>
        public void Abrir(bool SomenteLeitura, EstadosDosComponentes Pop)
        {

            if (!encontrado || (rowCON == null))
            {
                System.Windows.Forms.MessageBox.Show("Condomínio não encotrado");
                return;
            }

            cBalancete cBalancete1 = new cBalancete(this);

            //cBalancete cBalancete1 = cBalancete.GetDoPool(BALrow.BAL);

            //if (cBalancete1 == null)
            //{

            //cBalancete1 = new cBalancete(this);

            cBalancete1.Titulo = string.Format("{0}-{1} Balancete {2}", rowCON.IsCONCodigoFolha1Null() ? 0 : rowCON.CONCodigoFolha1, rowCON.CONCodigo, comp);

            cBalancete1.VirShowModulo(Pop);

            //}

        }

        /// <summary>
        /// Pega OBS do banco de dados
        /// </summary>
        /// <param name="Tipo"></param>
        /// <returns></returns>
        public string GetOBS(enumOBBTipo Tipo)
        {
            if ((DBalancete1.OBsBalancete.Count == 0) && (!SalvoXML))            
                DBalancete1.OBsBalanceteTableAdapter.FillByBAL(DBalancete1.OBsBalancete, BALrow.BAL);
            
            dBalancete.OBsBalanceteRow rowOBB = DBalancete1.OBsBalancete.FindByOBBTipo((int)Tipo);
            return rowOBB == null ? null : rowOBB.OBBrtf;
        }        

        /// <summary>
        /// Salva obs no banco de dados
        /// </summary>
        /// <param name="Tipo"></param>
        /// <param name="RTF"></param>
        public void SetOBS(enumOBBTipo Tipo, string RTF)
        {
            if (SalvoXML)
                return;
            if (DBalancete1.OBsBalancete.Count == 0)
                DBalancete1.OBsBalanceteTableAdapter.FillByBAL(DBalancete1.OBsBalancete, BALrow.BAL);
            dBalancete.OBsBalanceteRow rowOBB = DBalancete1.OBsBalancete.FindByOBBTipo((int)Tipo);
            if (RTF == null)
            {
                if (rowOBB != null)
                {
                    rowOBB.Delete();
                    DBalancete1.OBsBalanceteTableAdapter.Update(rowOBB);
                    DBalancete1.OBsBalancete.AcceptChanges();
                }
            }
            else
            {                
                if (rowOBB == null)
                {
                    rowOBB = DBalancete1.OBsBalancete.NewOBsBalanceteRow();
                    rowOBB.OBB_BAL = BALrow.BAL;
                    rowOBB.OBBTipo = (int)Tipo;
                    rowOBB.OBBrtf = RTF;
                    DBalancete1.OBsBalancete.AddOBsBalanceteRow(rowOBB);
                    DBalancete1.OBsBalanceteTableAdapter.Update(rowOBB);
                    rowOBB.AcceptChanges();
                }
                else
                {
                    if (rowOBB.OBBrtf != RTF)
                    {
                        rowOBB.OBBrtf = RTF;
                        DBalancete1.OBsBalanceteTableAdapter.Update(rowOBB);
                        rowOBB.AcceptChanges();
                    }
                }
            }
        }
    }
}