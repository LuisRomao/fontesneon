﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using CompontesBasicos;

namespace Balancete.GrBalancete
{
    /// <summary>
    /// Editor dos dados do balancete da NF
    /// </summary>
    public partial class cEditaNota : ComponenteBaseDialog
    {
        /// <summary>
        /// Construtor Padrão
        /// </summary>
        public cEditaNota()
        {
            InitializeComponent();
        }

        private dEditaNota.NOtAsRow rowNOA;

        /// <summary>
        /// Indica se a nota foi encotrada
        /// </summary>
        public bool encontrado
        {
            get { return rowNOA != null; }
        }

        /// <summary>
        /// Construtor
        /// </summary>
        /// <param name="PAG"></param>
        /// <param name="balancete1"></param>
        public cEditaNota(int PAG, balancete balancete1)
        {
            InitializeComponent();
            if (dEditaNota.NOtAsTableAdapter.FillByPAG(dEditaNota.NOtAs, PAG) != 1)
                return;
            rowNOA = dEditaNota.NOtAs[0];
            dEditaNota.PAGamentosTableAdapter.FillByNOA(dEditaNota.PAGamentos, rowNOA.NOA);
        }

        /// <summary>
        /// Fecha tela
        /// </summary>
        /// <param name="Resultado"></param>
        protected override void FechaTela(DialogResult Resultado)
        {
            if (encontrado && (Resultado == DialogResult.OK))
            {
                gridView1.CloseEditor();
                if (Validate())
                {                    
                    dEditaNota.PAGamentosTableAdapter.Update(dEditaNota.PAGamentos);
                    base.FechaTela(Resultado);
                }
            }
            else
                base.FechaTela(Resultado);
        }
    }
}
