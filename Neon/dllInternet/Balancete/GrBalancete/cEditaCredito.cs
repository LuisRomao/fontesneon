﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using CompontesBasicos;
using VirEnumeracoesNeon;

namespace Balancete.GrBalancete
{
    /// <summary>
    /// Componete para editar amarração de créditos
    /// </summary>
    public partial class cEditaCredito : ComponenteBaseDialog
    {
        private int CCD;

        /// <summary>
        /// Construtor padrão - não usar
        /// </summary>
        public cEditaCredito()
        {
            InitializeComponent();
        }

        /// <summary>
        /// Construtor
        /// </summary>
        /// <param name="_CCD"></param>
        /// <param name="_somenteleitura"></param>
        public cEditaCredito(int _CCD, bool _somenteleitura):base()
        {
            InitializeComponent();
            somenteleitura = _somenteleitura;
            panelControl1.Visible = !somenteleitura;
            CCD = _CCD;
            Framework.Enumeracoes.virEnumARLStatusN.virEnumARLStatusNSt.CarregaEditorDaGrid(colARLStatusN);
            Boletos.Boleto.Boleto.VirEnumStatusBoleto.CarregaEditorDaGrid(colBOLStatus);
            CarregaDados(CCD);
        }

        private void CarregaDados(int CCD)
        {
            if (dEditaCredito.BolCredTableAdapter.Fill(dEditaCredito.BolCred, CCD) == 0)                        
                dEditaCredito.BolCredTableAdapter.FillByDeposito(dEditaCredito.BolCred, CCD);            
            dEditaCredito.BOletoDetalheTableAdapter.ClearBeforeFill = false;
            dEditaCredito.BOletoDetalhe.Clear();
            foreach (dEditaCredito.BolCredRow row in dEditaCredito.BolCred)
                if (!row.IsBOLCanceladoNull())
                    dEditaCredito.BOletoDetalheTableAdapter.FillRapido(dEditaCredito.BOletoDetalhe, row.BOL);
        }

        private void gridView1_CellMerge(object sender, DevExpress.XtraGrid.Views.Grid.CellMergeEventArgs e)
        {
            if (e.Column.Equals(colARLValor) || e.Column.Equals(colARL_BOL) || e.Column.Equals(colBOLValorPago) || e.Column.Equals(colBOLPagamento))
            {
                dEditaCredito.BolCredRow row1 = (dEditaCredito.BolCredRow)gridView1.GetDataRow(e.RowHandle1);
                dEditaCredito.BolCredRow row2 = (dEditaCredito.BolCredRow)gridView1.GetDataRow(e.RowHandle2);
                e.Merge = ((row1 != null) && (row2 != null) && (row1.ARL == row2.ARL));
                e.Handled = true;
            }
                        
        }

        private void repositoryItemButtonEditBOL_ButtonClick(object sender, DevExpress.XtraEditors.Controls.ButtonPressedEventArgs e)
        {
            dEditaCredito.BolCredRow row = (dEditaCredito.BolCredRow)gridView1.GetFocusedDataRow();
            if (row == null)
                return;
            Boletos.Boleto.Boleto BoletoEd = new Boletos.Boleto.Boleto(row.BOL);
            Boletos.Boleto.cBoleto cBoleto = new Boletos.Boleto.cBoleto(BoletoEd, false);
            cBoleto.VirShowModulo(CompontesBasicos.EstadosDosComponentes.PopUp);
        }

        private void gridView1_CustomRowCellEdit(object sender, DevExpress.XtraGrid.Views.Grid.CustomRowCellEditEventArgs e)
        {
            if (e.Column.Equals(colBotBotao))
            {
                dEditaCredito.BolCredRow row = (dEditaCredito.BolCredRow)gridView1.GetDataRow(e.RowHandle);
                if((row != null) && (!row.IsBOLNull()))
                {
                    e.RepositoryItem = repositoryItemButtonEditBOL;
                }
            }
        }

        private void gridView1_RowCellStyle(object sender, DevExpress.XtraGrid.Views.Grid.RowCellStyleEventArgs e)
        {
            if (e.Column.Equals(colARLStatusN))
            {
                dEditaCredito.BolCredRow row = (dEditaCredito.BolCredRow)gridView1.GetDataRow(e.RowHandle);
                if ((row != null) && (!row.IsARLStatusNNull()))
                {
                    ARLStatusN status = (ARLStatusN)row.ARLStatusN;
                    e.Appearance.BackColor = Framework.Enumeracoes.virEnumARLStatusN.virEnumARLStatusNSt.GetCor(status);
                }
            }
        }

        private void BotCancelar_Click(object sender, EventArgs e)
        {
            if (MessageBox.Show("Confirma o cancelamento da conciliação", "CONFIRMAÇÃO", MessageBoxButtons.YesNo, MessageBoxIcon.Warning, MessageBoxDefaultButton.Button2) == DialogResult.Yes)
            {
                dllbanco.Conciliacao.CancelaConcEstratoFran(CCD,false);
                FechaTela(DialogResult.OK);
            }
        }
    }
}
