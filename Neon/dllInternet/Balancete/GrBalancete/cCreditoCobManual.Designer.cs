﻿namespace Balancete.GrBalancete
{
    partial class cCreditoCobManual
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(cCreditoCobManual));
            this.panelControl1 = new DevExpress.XtraEditors.PanelControl();
            this.BotCancelar = new DevExpress.XtraEditors.SimpleButton();
            this.dateEditCredito = new DevExpress.XtraEditors.DateEdit();
            this.labelControl3 = new DevExpress.XtraEditors.LabelControl();
            this.textErro = new DevExpress.XtraEditors.TextEdit();
            this.labelErroT = new DevExpress.XtraEditors.LabelControl();
            this.textAcumulado = new DevExpress.XtraEditors.TextEdit();
            this.textMeta = new DevExpress.XtraEditors.TextEdit();
            this.labelControl1 = new DevExpress.XtraEditors.LabelControl();
            this.labelTotal = new DevExpress.XtraEditors.LabelControl();
            this.gridControl1 = new DevExpress.XtraGrid.GridControl();
            this.dBalanceteBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.gridView1 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colBOL = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colBOLValorPago = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemCalcEdit1 = new DevExpress.XtraEditors.Repository.RepositoryItemCalcEdit();
            this.colBOLTipoCRAI = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colBOLEmissao = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colBOLVencto = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colBOLPagamento = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colBOLCompetenciaAno = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colBOLCompetenciaMes = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colcalcBOL = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colcalcUnidade = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colBOL_CCD = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colErro = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemMemoExEdit1 = new DevExpress.XtraEditors.Repository.RepositoryItemMemoExEdit();
            this.imageCollection1 = new DevExpress.Utils.ImageCollection(this.components);
            ((System.ComponentModel.ISupportInitialize)(this.BarManager_F)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.StyleController_F)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl1)).BeginInit();
            this.panelControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dateEditCredito.Properties.CalendarTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEditCredito.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textErro.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textAcumulado.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textMeta.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dBalanceteBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCalcEdit1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoExEdit1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.imageCollection1)).BeginInit();
            this.SuspendLayout();
            // 
            // barComponente_F
            // 
            this.barComponente_F.OptionsBar.AllowQuickCustomization = false;
            this.barComponente_F.OptionsBar.DisableClose = true;
            this.barComponente_F.OptionsBar.DisableCustomization = true;
            this.barComponente_F.OptionsBar.DrawDragBorder = false;
            this.barComponente_F.OptionsBar.UseWholeRow = true;
            // 
            // btnConfirmar_F
            // 
            this.btnConfirmar_F.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("btnConfirmar_F.ImageOptions.Image")));
            // 
            // btnCancelar_F
            // 
            this.btnCancelar_F.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("btnCancelar_F.ImageOptions.Image")));
            // 
            // btnFechar_F
            // 
            this.btnFechar_F.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("btnFechar_F.ImageOptions.Image")));
            // 
            // ToolTipController_F
            // 
            this.ToolTipController_F.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.ToolTipController_F.Appearance.ForeColor = System.Drawing.Color.Black;
            this.ToolTipController_F.Appearance.Options.UseBackColor = true;
            this.ToolTipController_F.Appearance.Options.UseFont = true;
            this.ToolTipController_F.Appearance.Options.UseForeColor = true;
            this.ToolTipController_F.Appearance.Options.UseTextOptions = true;
            this.ToolTipController_F.Appearance.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            // 
            // StyleController_F
            // 
            this.StyleController_F.AppearanceDisabled.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this.StyleController_F.AppearanceDisabled.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this.StyleController_F.AppearanceDisabled.Options.UseBackColor = true;
            this.StyleController_F.AppearanceDisabled.Options.UseForeColor = true;
            this.StyleController_F.AppearanceFocused.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.StyleController_F.AppearanceFocused.Options.UseBackColor = true;
            // 
            // panelControl1
            // 
            this.panelControl1.Controls.Add(this.BotCancelar);
            this.panelControl1.Controls.Add(this.dateEditCredito);
            this.panelControl1.Controls.Add(this.labelControl3);
            this.panelControl1.Controls.Add(this.textErro);
            this.panelControl1.Controls.Add(this.labelErroT);
            this.panelControl1.Controls.Add(this.textAcumulado);
            this.panelControl1.Controls.Add(this.textMeta);
            this.panelControl1.Controls.Add(this.labelControl1);
            this.panelControl1.Controls.Add(this.labelTotal);
            this.panelControl1.Dock = System.Windows.Forms.DockStyle.Top;
            this.panelControl1.Location = new System.Drawing.Point(0, 35);
            this.panelControl1.Name = "panelControl1";
            this.panelControl1.Size = new System.Drawing.Size(1452, 88);
            this.panelControl1.TabIndex = 4;
            // 
            // BotCancelar
            // 
            this.BotCancelar.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BotCancelar.Appearance.Options.UseFont = true;
            this.BotCancelar.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("BotCancelar.ImageOptions.Image")));
            this.BotCancelar.Location = new System.Drawing.Point(792, 17);
            this.BotCancelar.Name = "BotCancelar";
            this.BotCancelar.Size = new System.Drawing.Size(230, 52);
            this.BotCancelar.TabIndex = 8;
            this.BotCancelar.Text = "Cancelar Conciliação";
            this.BotCancelar.Visible = false;
            this.BotCancelar.Click += new System.EventHandler(this.BotCancelar_Click);
            // 
            // dateEditCredito
            // 
            this.dateEditCredito.EditValue = null;
            this.dateEditCredito.Location = new System.Drawing.Point(614, 10);
            this.dateEditCredito.MenuManager = this.BarManager_F;
            this.dateEditCredito.Name = "dateEditCredito";
            this.dateEditCredito.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.dateEditCredito.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dateEditCredito.Properties.Appearance.ForeColor = System.Drawing.Color.Red;
            this.dateEditCredito.Properties.Appearance.Options.UseBackColor = true;
            this.dateEditCredito.Properties.Appearance.Options.UseFont = true;
            this.dateEditCredito.Properties.Appearance.Options.UseForeColor = true;
            this.dateEditCredito.Properties.CalendarTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.dateEditCredito.Properties.ReadOnly = true;
            this.dateEditCredito.Size = new System.Drawing.Size(145, 30);
            this.dateEditCredito.TabIndex = 7;
            // 
            // labelControl3
            // 
            this.labelControl3.Appearance.Font = new System.Drawing.Font("Tahoma", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl3.Appearance.Options.UseFont = true;
            this.labelControl3.Location = new System.Drawing.Point(455, 13);
            this.labelControl3.Name = "labelControl3";
            this.labelControl3.Size = new System.Drawing.Size(153, 23);
            this.labelControl3.TabIndex = 6;
            this.labelControl3.Text = "Data de crédito:";
            // 
            // textErro
            // 
            this.textErro.EditValue = "0,00";
            this.textErro.Location = new System.Drawing.Point(614, 46);
            this.textErro.MenuManager = this.BarManager_F;
            this.textErro.Name = "textErro";
            this.textErro.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.textErro.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textErro.Properties.Appearance.ForeColor = System.Drawing.Color.Red;
            this.textErro.Properties.Appearance.Options.UseBackColor = true;
            this.textErro.Properties.Appearance.Options.UseFont = true;
            this.textErro.Properties.Appearance.Options.UseForeColor = true;
            this.textErro.Properties.ReadOnly = true;
            this.textErro.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.textErro.Size = new System.Drawing.Size(145, 30);
            this.textErro.TabIndex = 5;
            // 
            // labelErroT
            // 
            this.labelErroT.Appearance.Font = new System.Drawing.Font("Tahoma", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelErroT.Appearance.Options.UseFont = true;
            this.labelErroT.Location = new System.Drawing.Point(561, 49);
            this.labelErroT.Name = "labelErroT";
            this.labelErroT.Size = new System.Drawing.Size(47, 23);
            this.labelErroT.TabIndex = 4;
            this.labelErroT.Text = "Erro:";
            // 
            // textAcumulado
            // 
            this.textAcumulado.EditValue = "0,00";
            this.textAcumulado.Location = new System.Drawing.Point(249, 46);
            this.textAcumulado.MenuManager = this.BarManager_F;
            this.textAcumulado.Name = "textAcumulado";
            this.textAcumulado.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.textAcumulado.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textAcumulado.Properties.Appearance.ForeColor = System.Drawing.Color.Red;
            this.textAcumulado.Properties.Appearance.Options.UseBackColor = true;
            this.textAcumulado.Properties.Appearance.Options.UseFont = true;
            this.textAcumulado.Properties.Appearance.Options.UseForeColor = true;
            this.textAcumulado.Properties.ReadOnly = true;
            this.textAcumulado.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.textAcumulado.Size = new System.Drawing.Size(145, 30);
            this.textAcumulado.TabIndex = 3;
            // 
            // textMeta
            // 
            this.textMeta.EditValue = "0,00";
            this.textMeta.Location = new System.Drawing.Point(249, 10);
            this.textMeta.MenuManager = this.BarManager_F;
            this.textMeta.Name = "textMeta";
            this.textMeta.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.textMeta.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textMeta.Properties.Appearance.ForeColor = System.Drawing.Color.Blue;
            this.textMeta.Properties.Appearance.Options.UseBackColor = true;
            this.textMeta.Properties.Appearance.Options.UseFont = true;
            this.textMeta.Properties.Appearance.Options.UseForeColor = true;
            this.textMeta.Properties.ReadOnly = true;
            this.textMeta.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.textMeta.Size = new System.Drawing.Size(145, 30);
            this.textMeta.TabIndex = 2;
            // 
            // labelControl1
            // 
            this.labelControl1.Appearance.Font = new System.Drawing.Font("Tahoma", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl1.Appearance.Options.UseFont = true;
            this.labelControl1.Location = new System.Drawing.Point(32, 17);
            this.labelControl1.Name = "labelControl1";
            this.labelControl1.Size = new System.Drawing.Size(211, 23);
            this.labelControl1.TabIndex = 1;
            this.labelControl1.Text = "Valor Total Creditado:";
            // 
            // labelTotal
            // 
            this.labelTotal.Appearance.Font = new System.Drawing.Font("Tahoma", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelTotal.Appearance.Options.UseFont = true;
            this.labelTotal.Location = new System.Drawing.Point(68, 49);
            this.labelTotal.Name = "labelTotal";
            this.labelTotal.Size = new System.Drawing.Size(175, 23);
            this.labelTotal.TabIndex = 0;
            this.labelTotal.Text = "Total selecionado:";
            // 
            // gridControl1
            // 
            this.gridControl1.DataSource = this.dBalanceteBindingSource;
            this.gridControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridControl1.Location = new System.Drawing.Point(0, 123);
            this.gridControl1.LookAndFeel.Style = DevExpress.LookAndFeel.LookAndFeelStyle.UltraFlat;
            this.gridControl1.LookAndFeel.UseDefaultLookAndFeel = false;
            this.gridControl1.MainView = this.gridView1;
            this.gridControl1.MenuManager = this.BarManager_F;
            this.gridControl1.Name = "gridControl1";
            this.gridControl1.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemCalcEdit1,
            this.repositoryItemMemoExEdit1});
            this.gridControl1.ShowOnlyPredefinedDetails = true;
            this.gridControl1.Size = new System.Drawing.Size(1452, 684);
            this.gridControl1.TabIndex = 5;
            this.gridControl1.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridView1});
            // 
            // dBalanceteBindingSource
            // 
            this.dBalanceteBindingSource.DataMember = "BOLetosRecebidos";
            this.dBalanceteBindingSource.DataSource = typeof(Balancete.GrBalancete.dBalancete);
            // 
            // gridView1
            // 
            this.gridView1.Appearance.ColumnFilterButton.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(240)))), ((int)(((byte)(177)))), ((int)(((byte)(94)))));
            this.gridView1.Appearance.ColumnFilterButton.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(240)))), ((int)(((byte)(177)))), ((int)(((byte)(94)))));
            this.gridView1.Appearance.ColumnFilterButton.ForeColor = System.Drawing.Color.Black;
            this.gridView1.Appearance.ColumnFilterButton.Options.UseBackColor = true;
            this.gridView1.Appearance.ColumnFilterButton.Options.UseBorderColor = true;
            this.gridView1.Appearance.ColumnFilterButton.Options.UseForeColor = true;
            this.gridView1.Appearance.ColumnFilterButtonActive.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(248)))), ((int)(((byte)(204)))), ((int)(((byte)(124)))));
            this.gridView1.Appearance.ColumnFilterButtonActive.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(248)))), ((int)(((byte)(204)))), ((int)(((byte)(124)))));
            this.gridView1.Appearance.ColumnFilterButtonActive.ForeColor = System.Drawing.Color.Black;
            this.gridView1.Appearance.ColumnFilterButtonActive.Options.UseBackColor = true;
            this.gridView1.Appearance.ColumnFilterButtonActive.Options.UseBorderColor = true;
            this.gridView1.Appearance.ColumnFilterButtonActive.Options.UseForeColor = true;
            this.gridView1.Appearance.Empty.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(251)))), ((int)(((byte)(244)))), ((int)(((byte)(232)))));
            this.gridView1.Appearance.Empty.BackColor2 = System.Drawing.Color.White;
            this.gridView1.Appearance.Empty.Options.UseBackColor = true;
            this.gridView1.Appearance.EvenRow.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(253)))), ((int)(((byte)(248)))), ((int)(((byte)(236)))));
            this.gridView1.Appearance.EvenRow.ForeColor = System.Drawing.Color.Black;
            this.gridView1.Appearance.EvenRow.Options.UseBackColor = true;
            this.gridView1.Appearance.EvenRow.Options.UseForeColor = true;
            this.gridView1.Appearance.FilterCloseButton.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(240)))), ((int)(((byte)(177)))), ((int)(((byte)(94)))));
            this.gridView1.Appearance.FilterCloseButton.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(240)))), ((int)(((byte)(177)))), ((int)(((byte)(94)))));
            this.gridView1.Appearance.FilterCloseButton.ForeColor = System.Drawing.Color.Black;
            this.gridView1.Appearance.FilterCloseButton.Options.UseBackColor = true;
            this.gridView1.Appearance.FilterCloseButton.Options.UseBorderColor = true;
            this.gridView1.Appearance.FilterCloseButton.Options.UseForeColor = true;
            this.gridView1.Appearance.FilterPanel.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(251)))), ((int)(((byte)(244)))), ((int)(((byte)(232)))));
            this.gridView1.Appearance.FilterPanel.BackColor2 = System.Drawing.Color.White;
            this.gridView1.Appearance.FilterPanel.ForeColor = System.Drawing.Color.Black;
            this.gridView1.Appearance.FilterPanel.Options.UseBackColor = true;
            this.gridView1.Appearance.FilterPanel.Options.UseForeColor = true;
            this.gridView1.Appearance.FixedLine.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(218)))), ((int)(((byte)(153)))), ((int)(((byte)(73)))));
            this.gridView1.Appearance.FixedLine.Options.UseBackColor = true;
            this.gridView1.Appearance.FocusedCell.BackColor = System.Drawing.Color.White;
            this.gridView1.Appearance.FocusedCell.ForeColor = System.Drawing.Color.Black;
            this.gridView1.Appearance.FocusedCell.Options.UseBackColor = true;
            this.gridView1.Appearance.FocusedCell.Options.UseForeColor = true;
            this.gridView1.Appearance.FocusedRow.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(217)))), ((int)(((byte)(154)))), ((int)(((byte)(91)))));
            this.gridView1.Appearance.FocusedRow.ForeColor = System.Drawing.Color.White;
            this.gridView1.Appearance.FocusedRow.Options.UseBackColor = true;
            this.gridView1.Appearance.FocusedRow.Options.UseForeColor = true;
            this.gridView1.Appearance.FooterPanel.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(240)))), ((int)(((byte)(177)))), ((int)(((byte)(94)))));
            this.gridView1.Appearance.FooterPanel.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(240)))), ((int)(((byte)(177)))), ((int)(((byte)(94)))));
            this.gridView1.Appearance.FooterPanel.ForeColor = System.Drawing.Color.Black;
            this.gridView1.Appearance.FooterPanel.Options.UseBackColor = true;
            this.gridView1.Appearance.FooterPanel.Options.UseBorderColor = true;
            this.gridView1.Appearance.FooterPanel.Options.UseForeColor = true;
            this.gridView1.Appearance.GroupButton.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(248)))), ((int)(((byte)(204)))), ((int)(((byte)(124)))));
            this.gridView1.Appearance.GroupButton.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(248)))), ((int)(((byte)(204)))), ((int)(((byte)(124)))));
            this.gridView1.Appearance.GroupButton.Options.UseBackColor = true;
            this.gridView1.Appearance.GroupButton.Options.UseBorderColor = true;
            this.gridView1.Appearance.GroupFooter.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(248)))), ((int)(((byte)(204)))), ((int)(((byte)(124)))));
            this.gridView1.Appearance.GroupFooter.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(248)))), ((int)(((byte)(204)))), ((int)(((byte)(124)))));
            this.gridView1.Appearance.GroupFooter.ForeColor = System.Drawing.Color.Black;
            this.gridView1.Appearance.GroupFooter.Options.UseBackColor = true;
            this.gridView1.Appearance.GroupFooter.Options.UseBorderColor = true;
            this.gridView1.Appearance.GroupFooter.Options.UseForeColor = true;
            this.gridView1.Appearance.GroupPanel.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(251)))), ((int)(((byte)(244)))), ((int)(((byte)(232)))));
            this.gridView1.Appearance.GroupPanel.BackColor2 = System.Drawing.Color.White;
            this.gridView1.Appearance.GroupPanel.ForeColor = System.Drawing.Color.Black;
            this.gridView1.Appearance.GroupPanel.Options.UseBackColor = true;
            this.gridView1.Appearance.GroupPanel.Options.UseForeColor = true;
            this.gridView1.Appearance.GroupRow.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(248)))), ((int)(((byte)(204)))), ((int)(((byte)(124)))));
            this.gridView1.Appearance.GroupRow.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(248)))), ((int)(((byte)(204)))), ((int)(((byte)(124)))));
            this.gridView1.Appearance.GroupRow.ForeColor = System.Drawing.Color.Black;
            this.gridView1.Appearance.GroupRow.Options.UseBackColor = true;
            this.gridView1.Appearance.GroupRow.Options.UseBorderColor = true;
            this.gridView1.Appearance.GroupRow.Options.UseForeColor = true;
            this.gridView1.Appearance.HeaderPanel.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(248)))), ((int)(((byte)(204)))), ((int)(((byte)(124)))));
            this.gridView1.Appearance.HeaderPanel.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(248)))), ((int)(((byte)(204)))), ((int)(((byte)(124)))));
            this.gridView1.Appearance.HeaderPanel.ForeColor = System.Drawing.Color.Black;
            this.gridView1.Appearance.HeaderPanel.Options.UseBackColor = true;
            this.gridView1.Appearance.HeaderPanel.Options.UseBorderColor = true;
            this.gridView1.Appearance.HeaderPanel.Options.UseForeColor = true;
            this.gridView1.Appearance.HideSelectionRow.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(225)))), ((int)(((byte)(183)))), ((int)(((byte)(125)))));
            this.gridView1.Appearance.HideSelectionRow.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(251)))), ((int)(((byte)(244)))), ((int)(((byte)(232)))));
            this.gridView1.Appearance.HideSelectionRow.Options.UseBackColor = true;
            this.gridView1.Appearance.HideSelectionRow.Options.UseForeColor = true;
            this.gridView1.Appearance.HorzLine.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(240)))), ((int)(((byte)(177)))), ((int)(((byte)(94)))));
            this.gridView1.Appearance.HorzLine.Options.UseBackColor = true;
            this.gridView1.Appearance.OddRow.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(251)))), ((int)(((byte)(236)))), ((int)(((byte)(208)))));
            this.gridView1.Appearance.OddRow.ForeColor = System.Drawing.Color.Black;
            this.gridView1.Appearance.OddRow.Options.UseBackColor = true;
            this.gridView1.Appearance.OddRow.Options.UseForeColor = true;
            this.gridView1.Appearance.Preview.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(254)))), ((int)(((byte)(249)))));
            this.gridView1.Appearance.Preview.Font = new System.Drawing.Font("Verdana", 7.5F);
            this.gridView1.Appearance.Preview.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(186)))), ((int)(((byte)(146)))), ((int)(((byte)(78)))));
            this.gridView1.Appearance.Preview.Options.UseBackColor = true;
            this.gridView1.Appearance.Preview.Options.UseFont = true;
            this.gridView1.Appearance.Preview.Options.UseForeColor = true;
            this.gridView1.Appearance.Row.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(253)))), ((int)(((byte)(248)))), ((int)(((byte)(236)))));
            this.gridView1.Appearance.Row.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(253)))), ((int)(((byte)(248)))), ((int)(((byte)(236)))));
            this.gridView1.Appearance.Row.ForeColor = System.Drawing.Color.Black;
            this.gridView1.Appearance.Row.Options.UseBackColor = true;
            this.gridView1.Appearance.Row.Options.UseBorderColor = true;
            this.gridView1.Appearance.Row.Options.UseForeColor = true;
            this.gridView1.Appearance.RowSeparator.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(251)))), ((int)(((byte)(244)))), ((int)(((byte)(232)))));
            this.gridView1.Appearance.RowSeparator.BackColor2 = System.Drawing.Color.White;
            this.gridView1.Appearance.RowSeparator.Options.UseBackColor = true;
            this.gridView1.Appearance.SelectedRow.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(231)))), ((int)(((byte)(167)))), ((int)(((byte)(103)))));
            this.gridView1.Appearance.SelectedRow.Options.UseBackColor = true;
            this.gridView1.Appearance.TopNewRow.BackColor = System.Drawing.Color.White;
            this.gridView1.Appearance.TopNewRow.Options.UseBackColor = true;
            this.gridView1.Appearance.VertLine.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(240)))), ((int)(((byte)(177)))), ((int)(((byte)(94)))));
            this.gridView1.Appearance.VertLine.Options.UseBackColor = true;
            this.gridView1.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colBOL,
            this.colBOLValorPago,
            this.colBOLTipoCRAI,
            this.colBOLEmissao,
            this.colBOLVencto,
            this.colBOLPagamento,
            this.colBOLCompetenciaAno,
            this.colBOLCompetenciaMes,
            this.colcalcBOL,
            this.colcalcUnidade,
            this.colBOL_CCD,
            this.colErro});
            this.gridView1.GridControl = this.gridControl1;
            this.gridView1.GroupCount = 1;
            this.gridView1.GroupSummary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridGroupSummaryItem(DevExpress.Data.SummaryItemType.Sum, "BOLValorPago", this.colBOLValorPago, "{0:n2}")});
            this.gridView1.Name = "gridView1";
            this.gridView1.OptionsBehavior.AutoExpandAllGroups = true;
            this.gridView1.OptionsSelection.MultiSelect = true;
            this.gridView1.OptionsSelection.MultiSelectMode = DevExpress.XtraGrid.Views.Grid.GridMultiSelectMode.CheckBoxRowSelect;
            this.gridView1.OptionsView.ColumnAutoWidth = false;
            this.gridView1.OptionsView.EnableAppearanceEvenRow = true;
            this.gridView1.OptionsView.EnableAppearanceOddRow = true;
            this.gridView1.OptionsView.ShowAutoFilterRow = true;
            this.gridView1.OptionsView.ShowFooter = true;
            this.gridView1.OptionsView.ShowGroupedColumns = true;
            this.gridView1.OptionsView.ShowGroupPanel = false;
            this.gridView1.PaintStyleName = "Flat";
            this.gridView1.SortInfo.AddRange(new DevExpress.XtraGrid.Columns.GridColumnSortInfo[] {
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colBOLPagamento, DevExpress.Data.ColumnSortOrder.Ascending)});
            this.gridView1.RowCellStyle += new DevExpress.XtraGrid.Views.Grid.RowCellStyleEventHandler(this.gridView1_RowCellStyle);
            this.gridView1.SelectionChanged += new DevExpress.Data.SelectionChangedEventHandler(this.gridView1_SelectionChanged);
            // 
            // colBOL
            // 
            this.colBOL.Caption = "Boleto Quitado";
            this.colBOL.FieldName = "BOL";
            this.colBOL.Name = "colBOL";
            this.colBOL.OptionsColumn.ReadOnly = true;
            this.colBOL.Visible = true;
            this.colBOL.VisibleIndex = 7;
            this.colBOL.Width = 96;
            // 
            // colBOLValorPago
            // 
            this.colBOLValorPago.AppearanceCell.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.colBOLValorPago.AppearanceCell.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
            this.colBOLValorPago.AppearanceCell.ForeColor = System.Drawing.Color.Black;
            this.colBOLValorPago.AppearanceCell.Options.UseBackColor = true;
            this.colBOLValorPago.AppearanceCell.Options.UseFont = true;
            this.colBOLValorPago.AppearanceCell.Options.UseForeColor = true;
            this.colBOLValorPago.Caption = "Valor Pago";
            this.colBOLValorPago.ColumnEdit = this.repositoryItemCalcEdit1;
            this.colBOLValorPago.FieldName = "BOLValorPago";
            this.colBOLValorPago.Name = "colBOLValorPago";
            this.colBOLValorPago.OptionsColumn.ReadOnly = true;
            this.colBOLValorPago.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum, "BOLValorPago", "{0:n2}")});
            this.colBOLValorPago.Visible = true;
            this.colBOLValorPago.VisibleIndex = 2;
            // 
            // repositoryItemCalcEdit1
            // 
            this.repositoryItemCalcEdit1.AutoHeight = false;
            this.repositoryItemCalcEdit1.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemCalcEdit1.Mask.EditMask = "n2";
            this.repositoryItemCalcEdit1.Mask.UseMaskAsDisplayFormat = true;
            this.repositoryItemCalcEdit1.Name = "repositoryItemCalcEdit1";
            // 
            // colBOLTipoCRAI
            // 
            this.colBOLTipoCRAI.Caption = "Tipo Boleto";
            this.colBOLTipoCRAI.FieldName = "BOLTipoCRAI";
            this.colBOLTipoCRAI.Name = "colBOLTipoCRAI";
            this.colBOLTipoCRAI.OptionsColumn.ReadOnly = true;
            this.colBOLTipoCRAI.Visible = true;
            this.colBOLTipoCRAI.VisibleIndex = 3;
            this.colBOLTipoCRAI.Width = 63;
            // 
            // colBOLEmissao
            // 
            this.colBOLEmissao.Caption = "Emissão";
            this.colBOLEmissao.FieldName = "BOLEmissao";
            this.colBOLEmissao.Name = "colBOLEmissao";
            this.colBOLEmissao.OptionsColumn.ReadOnly = true;
            this.colBOLEmissao.Visible = true;
            this.colBOLEmissao.VisibleIndex = 9;
            // 
            // colBOLVencto
            // 
            this.colBOLVencto.Caption = "Vencimento";
            this.colBOLVencto.FieldName = "BOLVencto";
            this.colBOLVencto.Name = "colBOLVencto";
            this.colBOLVencto.OptionsColumn.ReadOnly = true;
            this.colBOLVencto.Visible = true;
            this.colBOLVencto.VisibleIndex = 10;
            // 
            // colBOLPagamento
            // 
            this.colBOLPagamento.Caption = "Pagamento";
            this.colBOLPagamento.FieldName = "BOLPagamento";
            this.colBOLPagamento.Name = "colBOLPagamento";
            this.colBOLPagamento.OptionsColumn.ReadOnly = true;
            this.colBOLPagamento.Visible = true;
            this.colBOLPagamento.VisibleIndex = 1;
            // 
            // colBOLCompetenciaAno
            // 
            this.colBOLCompetenciaAno.Caption = "Ano";
            this.colBOLCompetenciaAno.FieldName = "BOLCompetenciaAno";
            this.colBOLCompetenciaAno.Name = "colBOLCompetenciaAno";
            this.colBOLCompetenciaAno.OptionsColumn.ReadOnly = true;
            this.colBOLCompetenciaAno.Visible = true;
            this.colBOLCompetenciaAno.VisibleIndex = 4;
            this.colBOLCompetenciaAno.Width = 35;
            // 
            // colBOLCompetenciaMes
            // 
            this.colBOLCompetenciaMes.Caption = "Mês";
            this.colBOLCompetenciaMes.FieldName = "BOLCompetenciaMes";
            this.colBOLCompetenciaMes.Name = "colBOLCompetenciaMes";
            this.colBOLCompetenciaMes.OptionsColumn.ReadOnly = true;
            this.colBOLCompetenciaMes.Visible = true;
            this.colBOLCompetenciaMes.VisibleIndex = 5;
            this.colBOLCompetenciaMes.Width = 30;
            // 
            // colcalcBOL
            // 
            this.colcalcBOL.Caption = "Boleto Pago";
            this.colcalcBOL.FieldName = "calcBOL";
            this.colcalcBOL.Name = "colcalcBOL";
            this.colcalcBOL.OptionsColumn.ReadOnly = true;
            this.colcalcBOL.Visible = true;
            this.colcalcBOL.VisibleIndex = 6;
            this.colcalcBOL.Width = 88;
            // 
            // colcalcUnidade
            // 
            this.colcalcUnidade.Caption = "Unidade";
            this.colcalcUnidade.FieldName = "calcUnidade";
            this.colcalcUnidade.Name = "colcalcUnidade";
            this.colcalcUnidade.OptionsColumn.ReadOnly = true;
            this.colcalcUnidade.Visible = true;
            this.colcalcUnidade.VisibleIndex = 8;
            // 
            // colBOL_CCD
            // 
            this.colBOL_CCD.FieldName = "BOL_CCD";
            this.colBOL_CCD.Name = "colBOL_CCD";
            // 
            // colErro
            // 
            this.colErro.ColumnEdit = this.repositoryItemMemoExEdit1;
            this.colErro.FieldName = "Erro";
            this.colErro.Name = "colErro";
            this.colErro.Visible = true;
            this.colErro.VisibleIndex = 11;
            this.colErro.Width = 33;
            // 
            // repositoryItemMemoExEdit1
            // 
            this.repositoryItemMemoExEdit1.AppearanceDropDown.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.repositoryItemMemoExEdit1.AppearanceDropDown.BackColor2 = System.Drawing.Color.White;
            this.repositoryItemMemoExEdit1.AppearanceDropDown.Font = new System.Drawing.Font("Courier New", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.repositoryItemMemoExEdit1.AppearanceDropDown.GradientMode = System.Drawing.Drawing2D.LinearGradientMode.ForwardDiagonal;
            this.repositoryItemMemoExEdit1.AppearanceDropDown.Options.UseBackColor = true;
            this.repositoryItemMemoExEdit1.AppearanceDropDown.Options.UseFont = true;
            this.repositoryItemMemoExEdit1.AutoHeight = false;
            this.repositoryItemMemoExEdit1.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemMemoExEdit1.Images = this.imageCollection1;
            this.repositoryItemMemoExEdit1.Name = "repositoryItemMemoExEdit1";
            this.repositoryItemMemoExEdit1.PopupFormMinSize = new System.Drawing.Size(400, 0);
            // 
            // imageCollection1
            // 
            this.imageCollection1.ImageStream = ((DevExpress.Utils.ImageCollectionStreamer)(resources.GetObject("imageCollection1.ImageStream")));
            this.imageCollection1.Images.SetKeyName(0, "Terro.gif");
            this.imageCollection1.Images.SetKeyName(1, "btnConfirmar_F.Glyph.png");
            this.imageCollection1.Images.SetKeyName(2, "avisoP.gif");
            // 
            // cCreditoCobManual
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.gridControl1);
            this.Controls.Add(this.panelControl1);
            this.Doc = System.Windows.Forms.DockStyle.Fill;
            this.LookAndFeel.UseDefaultLookAndFeel = false;
            this.Name = "cCreditoCobManual";
            this.Size = new System.Drawing.Size(1452, 807);
            this.Controls.SetChildIndex(this.panelControl1, 0);
            this.Controls.SetChildIndex(this.gridControl1, 0);
            ((System.ComponentModel.ISupportInitialize)(this.BarManager_F)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.StyleController_F)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl1)).EndInit();
            this.panelControl1.ResumeLayout(false);
            this.panelControl1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dateEditCredito.Properties.CalendarTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEditCredito.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textErro.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textAcumulado.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textMeta.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dBalanceteBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCalcEdit1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoExEdit1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.imageCollection1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private DevExpress.XtraEditors.PanelControl panelControl1;
        private DevExpress.XtraGrid.GridControl gridControl1;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView1;
        private System.Windows.Forms.BindingSource dBalanceteBindingSource;
        private DevExpress.XtraGrid.Columns.GridColumn colBOL;
        private DevExpress.XtraGrid.Columns.GridColumn colBOLValorPago;
        private DevExpress.XtraGrid.Columns.GridColumn colBOLTipoCRAI;
        private DevExpress.XtraGrid.Columns.GridColumn colBOLEmissao;
        private DevExpress.XtraGrid.Columns.GridColumn colBOLVencto;
        private DevExpress.XtraGrid.Columns.GridColumn colBOLPagamento;
        private DevExpress.XtraGrid.Columns.GridColumn colBOLCompetenciaAno;
        private DevExpress.XtraGrid.Columns.GridColumn colBOLCompetenciaMes;
        private DevExpress.XtraGrid.Columns.GridColumn colcalcBOL;
        private DevExpress.XtraGrid.Columns.GridColumn colcalcUnidade;
        private DevExpress.XtraEditors.LabelControl labelTotal;
        private DevExpress.XtraEditors.TextEdit textAcumulado;
        private DevExpress.XtraEditors.TextEdit textMeta;
        private DevExpress.XtraEditors.LabelControl labelControl1;
        private DevExpress.XtraEditors.TextEdit textErro;
        private DevExpress.XtraEditors.LabelControl labelErroT;
        private DevExpress.XtraEditors.Repository.RepositoryItemCalcEdit repositoryItemCalcEdit1;
        private DevExpress.XtraEditors.DateEdit dateEditCredito;
        private DevExpress.XtraEditors.LabelControl labelControl3;
        private DevExpress.XtraGrid.Columns.GridColumn colBOL_CCD;
        private DevExpress.XtraGrid.Columns.GridColumn colErro;
        private DevExpress.XtraEditors.Repository.RepositoryItemMemoExEdit repositoryItemMemoExEdit1;
        private DevExpress.Utils.ImageCollection imageCollection1;
        private DevExpress.XtraEditors.SimpleButton BotCancelar;
    }
}
