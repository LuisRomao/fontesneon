﻿namespace Balancete.GrBalancete
{
    using System;
    /// <summary>
    /// Enumeração para OBBTipo 
    /// </summary>
    public enum enumOBBTipo
    {
        /// <summary>
        /// Interna
        /// </summary>
        interna = -1,
        /// <summary>
        /// 
        /// </summary>
        Capa = 0,
        /// <summary>
        /// 
        /// </summary>
        Quadradinhos = 1,
        /// <summary>
        /// 
        /// </summary>
        Credito =2,
        /// <summary>
        /// 
        /// </summary>
        Debito =3,
        /// <summary>
        /// 
        /// </summary>
        Saldos =4,
        /// <summary>
        /// 
        /// </summary>
        Colunas = 5,
        /// <summary>
        /// 
        /// </summary>
        Previsto = 6,
        /// <summary>
        /// 
        /// </summary>
        Inadimplencia = 7
    }

    /// <summary>
    /// virEnum para campo OBBTipo
    /// </summary>
    public class virEnumOBBTipo : dllVirEnum.VirEnum
    {
        /// <summary>
        /// Construtor padrao
        /// </summary>
        /// <param name="Tipo">Tipo</param>
        public virEnumOBBTipo(Type Tipo) :
            base(Tipo)
        {
        }

        private static virEnumOBBTipo _virEnumOBBTipoSt;

        /// <summary>
        /// VirEnum estático para campo enumOBS
        /// </summary>
        public static virEnumOBBTipo virEnumOBBTipoSt
        {
            get
            {
                if (_virEnumOBBTipoSt == null)
                {
                    _virEnumOBBTipoSt = new virEnumOBBTipo(typeof(enumOBBTipo));
                    _virEnumOBBTipoSt.GravaNomes(enumOBBTipo.interna, "Intena");
                    _virEnumOBBTipoSt.GravaNomes(enumOBBTipo.Capa, "Capa");
                    _virEnumOBBTipoSt.GravaNomes(enumOBBTipo.Quadradinhos, "Após Créditos (quadradinhos)");
                    _virEnumOBBTipoSt.GravaNomes(enumOBBTipo.Credito, "Após Créditos");
                    _virEnumOBBTipoSt.GravaNomes(enumOBBTipo.Debito, "Após Débitos");
                    _virEnumOBBTipoSt.GravaNomes(enumOBBTipo.Saldos, "Após Saldos");
                    _virEnumOBBTipoSt.GravaNomes(enumOBBTipo.Colunas, "Após Detalhamento crédito");
                    _virEnumOBBTipoSt.GravaNomes(enumOBBTipo.Previsto, "Após previsto");
                    _virEnumOBBTipoSt.GravaNomes(enumOBBTipo.Inadimplencia, "Após inadimplência");
                    //_virEnumOBBTipoSt.GravaNomes(enumOBBTipo.interna, "Intena");
                    //_virEnumOBBTipoSt.GravaNomes(enumOBBTipo.interna, "Intena");
                }
                return _virEnumOBBTipoSt;
            }
        }
    }
}