﻿namespace Balancete.GrBalancete
{


    public partial class dEditaCredito
    {
        private dEditaCreditoTableAdapters.BolCredTableAdapter bolCredTableAdapter;
        /// <summary>
        /// TableAdapter padrão para tabela: BolCred
        /// </summary>
        public dEditaCreditoTableAdapters.BolCredTableAdapter BolCredTableAdapter
        {
            get
            {
                if (bolCredTableAdapter == null)
                {
                    bolCredTableAdapter = new dEditaCreditoTableAdapters.BolCredTableAdapter();
                    bolCredTableAdapter.TrocarStringDeConexao();
                };
                return bolCredTableAdapter;
            }
        }

        private dEditaCreditoTableAdapters.BOletoDetalheTableAdapter bOletoDetalheTableAdapter;

        /// <summary>
        /// TableAdapter padrão para tabela: BOletoDetalhe
        /// </summary>
        public dEditaCreditoTableAdapters.BOletoDetalheTableAdapter BOletoDetalheTableAdapter
        {
            get
            {
                if (bOletoDetalheTableAdapter == null)
                {
                    bOletoDetalheTableAdapter = new dEditaCreditoTableAdapters.BOletoDetalheTableAdapter();
                    bOletoDetalheTableAdapter.TrocarStringDeConexao();
                };
                return bOletoDetalheTableAdapter;
            }
        }
    }
}
