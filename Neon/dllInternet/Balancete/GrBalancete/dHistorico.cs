﻿namespace Balancete.GrBalancete {
    
    
    public partial class dHistorico 
    {
        private dHistoricoTableAdapters.Histórico_LançamentosTableAdapter histórico_LançamentosTableAdapter;
        /// <summary>
        /// TableAdapter padrão para tabela: Histórico_Lançamentos
        /// </summary>
        public dHistoricoTableAdapters.Histórico_LançamentosTableAdapter Histórico_LançamentosTableAdapter
        {
            get
            {
                if (histórico_LançamentosTableAdapter == null)
                {
                    histórico_LançamentosTableAdapter = new dHistoricoTableAdapters.Histórico_LançamentosTableAdapter();
                    histórico_LançamentosTableAdapter.TrocarStringDeConexao(VirDB.Bancovirtual.TiposDeBanco.AccessH);
                };
                return histórico_LançamentosTableAdapter;
            }
        }
    }
}
