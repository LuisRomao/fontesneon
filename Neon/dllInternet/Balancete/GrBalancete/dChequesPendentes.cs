﻿namespace Balancete.GrBalancete
{

    partial class dChequesPendentes
    {
        private dChequesPendentesTableAdapters.ChequePendenteTableAdapter chequePendenteTableAdapter;
        /// <summary>
        /// TableAdapter padrão para tabela: ChequePendente
        /// </summary>
        public dChequesPendentesTableAdapters.ChequePendenteTableAdapter ChequePendenteTableAdapter
        {
            get
            {
                if (chequePendenteTableAdapter == null)
                {
                    chequePendenteTableAdapter = new dChequesPendentesTableAdapters.ChequePendenteTableAdapter();
                    chequePendenteTableAdapter.TrocarStringDeConexao();
                };
                return chequePendenteTableAdapter;
            }
        }
    }
}
