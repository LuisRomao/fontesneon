﻿namespace Balancete.GrBalancete
{
    partial class cEditaNota
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.Windows.Forms.Label nOANumeroLabel;
            System.Windows.Forms.Label nOADataEmissaoLabel;
            System.Windows.Forms.Label nOAServicoLabel;
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling1 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            this.panelControl1 = new DevExpress.XtraEditors.PanelControl();
            this.nOAServicoTextEdit = new DevExpress.XtraEditors.TextEdit();
            this.nOtAsBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.dEditaNota = new Balancete.GrBalancete.dEditaNota();
            this.nOADataEmissaoDateEdit = new DevExpress.XtraEditors.DateEdit();
            this.nOANumeroSpinEdit = new DevExpress.XtraEditors.SpinEdit();
            this.gridControl1 = new DevExpress.XtraGrid.GridControl();
            this.gridView1 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colPAG = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colPAGValor = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colPAGVencimento = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colPAGN = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colPAG_NOA = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colPAGTipo = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colPAGDOC = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colPAG_CTL = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colPAG_CHE = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colPAGIMP = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colCHENumero = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colCHEValor = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colCHEVencimento = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colCHEEmissao = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colCHEStatus = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colCHETipo = new DevExpress.XtraGrid.Columns.GridColumn();
            nOANumeroLabel = new System.Windows.Forms.Label();
            nOADataEmissaoLabel = new System.Windows.Forms.Label();
            nOAServicoLabel = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.BarManager_F)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.StyleController_F)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl1)).BeginInit();
            this.panelControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.nOAServicoTextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.nOtAsBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dEditaNota)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.nOADataEmissaoDateEdit.Properties.VistaTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.nOADataEmissaoDateEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.nOANumeroSpinEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).BeginInit();
            this.SuspendLayout();
            // 
            // barComponente_F
            // 
            this.barComponente_F.OptionsBar.AllowQuickCustomization = false;
            this.barComponente_F.OptionsBar.DisableClose = true;
            this.barComponente_F.OptionsBar.DisableCustomization = true;
            this.barComponente_F.OptionsBar.DrawDragBorder = false;
            this.barComponente_F.OptionsBar.UseWholeRow = true;
            // 
            // ToolTipController_F
            // 
            this.ToolTipController_F.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.ToolTipController_F.Appearance.ForeColor = System.Drawing.Color.Black;
            this.ToolTipController_F.Appearance.Options.UseBackColor = true;
            this.ToolTipController_F.Appearance.Options.UseFont = true;
            this.ToolTipController_F.Appearance.Options.UseForeColor = true;
            this.ToolTipController_F.Appearance.Options.UseTextOptions = true;
            this.ToolTipController_F.Appearance.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            // 
            // StyleController_F
            // 
            this.StyleController_F.AppearanceDisabled.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this.StyleController_F.AppearanceDisabled.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this.StyleController_F.AppearanceDisabled.Options.UseBackColor = true;
            this.StyleController_F.AppearanceDisabled.Options.UseForeColor = true;
            this.StyleController_F.AppearanceFocused.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.StyleController_F.AppearanceFocused.Options.UseBackColor = true;
       
            // 
            // nOANumeroLabel
            // 
            nOANumeroLabel.AutoSize = true;
            nOANumeroLabel.Location = new System.Drawing.Point(33, 24);
            nOANumeroLabel.Name = "nOANumeroLabel";
            nOANumeroLabel.Size = new System.Drawing.Size(34, 13);
            nOANumeroLabel.TabIndex = 0;
            nOANumeroLabel.Text = "Nota:";
            // 
            // nOADataEmissaoLabel
            // 
            nOADataEmissaoLabel.AutoSize = true;
            nOADataEmissaoLabel.Location = new System.Drawing.Point(188, 24);
            nOADataEmissaoLabel.Name = "nOADataEmissaoLabel";
            nOADataEmissaoLabel.Size = new System.Drawing.Size(49, 13);
            nOADataEmissaoLabel.TabIndex = 2;
            nOADataEmissaoLabel.Text = "Emissão:";
            // 
            // nOAServicoLabel
            // 
            nOAServicoLabel.AutoSize = true;
            nOAServicoLabel.Location = new System.Drawing.Point(362, 24);
            nOAServicoLabel.Name = "nOAServicoLabel";
            nOAServicoLabel.Size = new System.Drawing.Size(46, 13);
            nOAServicoLabel.TabIndex = 4;
            nOAServicoLabel.Text = "Serviço:";
            // 
            // panelControl1
            // 
            this.panelControl1.Controls.Add(nOAServicoLabel);
            this.panelControl1.Controls.Add(this.nOAServicoTextEdit);
            this.panelControl1.Controls.Add(nOADataEmissaoLabel);
            this.panelControl1.Controls.Add(this.nOADataEmissaoDateEdit);
            this.panelControl1.Controls.Add(nOANumeroLabel);
            this.panelControl1.Controls.Add(this.nOANumeroSpinEdit);
            this.panelControl1.Dock = System.Windows.Forms.DockStyle.Top;
            this.panelControl1.Location = new System.Drawing.Point(0, 35);
            this.panelControl1.Name = "panelControl1";
            this.panelControl1.Size = new System.Drawing.Size(834, 64);
            this.panelControl1.TabIndex = 4;
            // 
            // nOAServicoTextEdit
            // 
            this.nOAServicoTextEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.nOtAsBindingSource, "NOAServico", true));
            this.nOAServicoTextEdit.Location = new System.Drawing.Point(414, 21);
            this.nOAServicoTextEdit.MenuManager = this.BarManager_F;
            this.nOAServicoTextEdit.Name = "nOAServicoTextEdit";
            this.nOAServicoTextEdit.Properties.ReadOnly = true;
            
            this.nOAServicoTextEdit.Size = new System.Drawing.Size(401, 20);
            
            this.nOAServicoTextEdit.TabIndex = 5;
            // 
            // nOtAsBindingSource
            // 
            this.nOtAsBindingSource.DataMember = "NOtAs";
            this.nOtAsBindingSource.DataSource = this.dEditaNota;
            // 
            // dEditaNota
            // 
            this.dEditaNota.DataSetName = "dEditaNota";
            this.dEditaNota.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // nOADataEmissaoDateEdit
            // 
            this.nOADataEmissaoDateEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.nOtAsBindingSource, "NOADataEmissao", true));
            this.nOADataEmissaoDateEdit.EditValue = null;
            this.nOADataEmissaoDateEdit.Location = new System.Drawing.Point(243, 21);
            this.nOADataEmissaoDateEdit.MenuManager = this.BarManager_F;
            this.nOADataEmissaoDateEdit.Name = "nOADataEmissaoDateEdit";
            this.nOADataEmissaoDateEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.nOADataEmissaoDateEdit.Properties.ReadOnly = true;
            this.nOADataEmissaoDateEdit.Properties.VistaTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.nOADataEmissaoDateEdit.Size = new System.Drawing.Size(100, 20);
            this.nOADataEmissaoDateEdit.TabIndex = 3;
            // 
            // nOANumeroSpinEdit
            // 
            this.nOANumeroSpinEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.nOtAsBindingSource, "NOANumero", true));
            this.nOANumeroSpinEdit.EditValue = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.nOANumeroSpinEdit.Location = new System.Drawing.Point(73, 21);
            this.nOANumeroSpinEdit.MenuManager = this.BarManager_F;
            this.nOANumeroSpinEdit.Name = "nOANumeroSpinEdit";
            this.nOANumeroSpinEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.nOANumeroSpinEdit.Properties.ReadOnly = true;
            this.nOANumeroSpinEdit.Size = new System.Drawing.Size(100, 20);
            this.nOANumeroSpinEdit.TabIndex = 1;
            // 
            // gridControl1
            // 
            this.gridControl1.DataMember = "PAGamentos";
            this.gridControl1.DataSource = this.dEditaNota;
            this.gridControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridControl1.Location = new System.Drawing.Point(0, 99);
            this.gridControl1.MainView = this.gridView1;
            this.gridControl1.MenuManager = this.BarManager_F;
            this.gridControl1.Name = "gridControl1";
            this.gridControl1.Size = new System.Drawing.Size(834, 512);
            this.gridControl1.TabIndex = 5;
            this.gridControl1.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridView1});
            // 
            // gridView1
            // 
            this.gridView1.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colPAG,
            this.colPAGValor,
            this.colPAGVencimento,
            this.colPAGN,
            this.colPAG_NOA,
            this.colPAGTipo,
            this.colPAGDOC,
            this.colPAG_CTL,
            this.colPAG_CHE,
            this.colPAGIMP,
            this.colCHENumero,
            this.colCHEValor,
            this.colCHEVencimento,
            this.colCHEEmissao,
            this.colCHEStatus,
            this.colCHETipo});
            this.gridView1.GridControl = this.gridControl1;
            this.gridView1.Name = "gridView1";
            this.gridView1.OptionsView.ColumnAutoWidth = false;
            this.gridView1.OptionsView.ShowGroupPanel = false;
            this.gridView1.SortInfo.AddRange(new DevExpress.XtraGrid.Columns.GridColumnSortInfo[] {
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colPAGVencimento, DevExpress.Data.ColumnSortOrder.Ascending)});
            // 
            // colPAG
            // 
            this.colPAG.FieldName = "PAG";
            this.colPAG.Name = "colPAG";
            this.colPAG.OptionsColumn.ReadOnly = true;
            // 
            // colPAGValor
            // 
            this.colPAGValor.FieldName = "PAGValor";
            this.colPAGValor.Name = "colPAGValor";
            this.colPAGValor.OptionsColumn.ReadOnly = true;
            // 
            // colPAGVencimento
            // 
            this.colPAGVencimento.Caption = "Vencimento";
            this.colPAGVencimento.FieldName = "PAGVencimento";
            this.colPAGVencimento.Name = "colPAGVencimento";
            this.colPAGVencimento.OptionsColumn.ReadOnly = true;
            this.colPAGVencimento.Visible = true;
            this.colPAGVencimento.VisibleIndex = 0;
            this.colPAGVencimento.Width = 142;
            // 
            // colPAGN
            // 
            this.colPAGN.Caption = "Parcela";
            this.colPAGN.FieldName = "PAGN";
            this.colPAGN.Name = "colPAGN";
            this.colPAGN.Visible = true;
            this.colPAGN.VisibleIndex = 1;
            this.colPAGN.Width = 143;
            // 
            // colPAG_NOA
            // 
            this.colPAG_NOA.FieldName = "PAG_NOA";
            this.colPAG_NOA.Name = "colPAG_NOA";
            this.colPAG_NOA.OptionsColumn.ReadOnly = true;
            // 
            // colPAGTipo
            // 
            this.colPAGTipo.FieldName = "PAGTipo";
            this.colPAGTipo.Name = "colPAGTipo";
            this.colPAGTipo.OptionsColumn.ReadOnly = true;
            // 
            // colPAGDOC
            // 
            this.colPAGDOC.FieldName = "PAGDOC";
            this.colPAGDOC.Name = "colPAGDOC";
            this.colPAGDOC.OptionsColumn.ReadOnly = true;
            // 
            // colPAG_CTL
            // 
            this.colPAG_CTL.FieldName = "PAG_CTL";
            this.colPAG_CTL.Name = "colPAG_CTL";
            this.colPAG_CTL.OptionsColumn.ReadOnly = true;
            // 
            // colPAG_CHE
            // 
            this.colPAG_CHE.FieldName = "PAG_CHE";
            this.colPAG_CHE.Name = "colPAG_CHE";
            this.colPAG_CHE.OptionsColumn.ReadOnly = true;
            // 
            // colPAGIMP
            // 
            this.colPAGIMP.FieldName = "PAGIMP";
            this.colPAGIMP.Name = "colPAGIMP";
            this.colPAGIMP.OptionsColumn.ReadOnly = true;
            // 
            // colCHENumero
            // 
            this.colCHENumero.FieldName = "CHENumero";
            this.colCHENumero.Name = "colCHENumero";
            this.colCHENumero.OptionsColumn.ReadOnly = true;
            // 
            // colCHEValor
            // 
            this.colCHEValor.FieldName = "CHEValor";
            this.colCHEValor.Name = "colCHEValor";
            this.colCHEValor.OptionsColumn.ReadOnly = true;
            // 
            // colCHEVencimento
            // 
            this.colCHEVencimento.FieldName = "CHEVencimento";
            this.colCHEVencimento.Name = "colCHEVencimento";
            this.colCHEVencimento.OptionsColumn.ReadOnly = true;
            // 
            // colCHEEmissao
            // 
            this.colCHEEmissao.FieldName = "CHEEmissao";
            this.colCHEEmissao.Name = "colCHEEmissao";
            this.colCHEEmissao.OptionsColumn.ReadOnly = true;
            // 
            // colCHEStatus
            // 
            this.colCHEStatus.FieldName = "CHEStatus";
            this.colCHEStatus.Name = "colCHEStatus";
            this.colCHEStatus.OptionsColumn.ReadOnly = true;
            // 
            // colCHETipo
            // 
            this.colCHETipo.FieldName = "CHETipo";
            this.colCHETipo.Name = "colCHETipo";
            this.colCHETipo.OptionsColumn.ReadOnly = true;
            // 
            // cEditaNota
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.gridControl1);
            this.Controls.Add(this.panelControl1);
            this.LookAndFeel.UseDefaultLookAndFeel = false;
            this.Name = "cEditaNota";
            this.Size = new System.Drawing.Size(834, 611);
            this.Controls.SetChildIndex(this.panelControl1, 0);
            this.Controls.SetChildIndex(this.gridControl1, 0);
            ((System.ComponentModel.ISupportInitialize)(this.BarManager_F)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.StyleController_F)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl1)).EndInit();
            this.panelControl1.ResumeLayout(false);
            this.panelControl1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.nOAServicoTextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.nOtAsBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dEditaNota)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.nOADataEmissaoDateEdit.Properties.VistaTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.nOADataEmissaoDateEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.nOANumeroSpinEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraEditors.PanelControl panelControl1;
        private DevExpress.XtraGrid.GridControl gridControl1;
        private dEditaNota dEditaNota;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView1;
        private DevExpress.XtraGrid.Columns.GridColumn colPAG;
        private DevExpress.XtraGrid.Columns.GridColumn colPAGValor;
        private DevExpress.XtraGrid.Columns.GridColumn colPAGVencimento;
        private DevExpress.XtraGrid.Columns.GridColumn colPAGN;
        private DevExpress.XtraGrid.Columns.GridColumn colPAG_NOA;
        private DevExpress.XtraGrid.Columns.GridColumn colPAGTipo;
        private DevExpress.XtraGrid.Columns.GridColumn colPAGDOC;
        private DevExpress.XtraGrid.Columns.GridColumn colPAG_CTL;
        private DevExpress.XtraGrid.Columns.GridColumn colPAG_CHE;
        private DevExpress.XtraGrid.Columns.GridColumn colPAGIMP;
        private DevExpress.XtraGrid.Columns.GridColumn colCHENumero;
        private DevExpress.XtraGrid.Columns.GridColumn colCHEValor;
        private DevExpress.XtraGrid.Columns.GridColumn colCHEVencimento;
        private DevExpress.XtraGrid.Columns.GridColumn colCHEEmissao;
        private DevExpress.XtraGrid.Columns.GridColumn colCHEStatus;
        private DevExpress.XtraGrid.Columns.GridColumn colCHETipo;
        private DevExpress.XtraEditors.SpinEdit nOANumeroSpinEdit;
        private System.Windows.Forms.BindingSource nOtAsBindingSource;
        private DevExpress.XtraEditors.TextEdit nOAServicoTextEdit;
        private DevExpress.XtraEditors.DateEdit nOADataEmissaoDateEdit;
    }
}
