﻿using System;
using System.Drawing;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using DevExpress.XtraReports.UI;

namespace Balancete.GrBalancete
{
    /// <summary>
    /// Impresso de saldos
    /// </summary>
    public partial class impSaldo : dllImpresso.ImpLogoCond
    {
        public impSaldo()
        {
            InitializeComponent();
        }

        public void CriaColunaImp(string Campo,string Titulo)
        {
            float TamanhoRef = xrTableCell2.WidthF;
            double WeightRef = xrTableCell2.Weight;
            xrTable1.SizeF = new System.Drawing.SizeF(xrTable1.WidthF + TamanhoRef, 63.5F);
            xrTable2.SizeF = new System.Drawing.SizeF(xrTable2.WidthF + TamanhoRef, 63.5F);
            xrTable3.SizeF = new System.Drawing.SizeF(xrTable3.WidthF + TamanhoRef, 63.5F);

            DevExpress.XtraReports.UI.XRTableCell XTitulo = new DevExpress.XtraReports.UI.XRTableCell();
            xrTableRow1.Cells.Add(XTitulo);
            XTitulo.CanGrow = true;
            XTitulo.Dpi = 254F;
            XTitulo.Multiline = true;
            XTitulo.Name = "Titulo" + Campo;
            XTitulo.Text = Titulo;
            XTitulo.Weight = WeightRef;
            XTitulo.StylePriority.UseTextAlignment = false;
            XTitulo.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            XTitulo.Font = xrTableCell2.Font;

            DevExpress.XtraReports.UI.XRTableCell XDetalhe = new DevExpress.XtraReports.UI.XRTableCell();
            xrTableRow2.Cells.Add(XDetalhe);
            XDetalhe.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {new DevExpress.XtraReports.UI.XRBinding("Text", null, string.Format("ConTasLogicas.{0}",Campo), "{0:n2}")});
            XDetalhe.Dpi = 254F;
            XDetalhe.Name = Campo;
            XDetalhe.StylePriority.UseTextAlignment = false;
            XDetalhe.Text = Campo;
            XDetalhe.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight;
            XDetalhe.Weight = WeightRef;
            XDetalhe.Draw += new DevExpress.XtraReports.UI.DrawEventHandler(this.xrTableCell5_Draw);

            DevExpress.XtraReports.UI.XRTableCell XTotal = new DevExpress.XtraReports.UI.XRTableCell();
            xrTableRow3.Cells.Add(XTotal);
            XTotal.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] { new DevExpress.XtraReports.UI.XRBinding("Text", null, string.Format("ConTasLogicas.{0}", Campo)) });
            XTotal.Dpi = 254F;
            XTotal.Name = "Total" + Campo;
            XTotal.StylePriority.UseTextAlignment = false;
            DevExpress.XtraReports.UI.XRSummary XSummary = new DevExpress.XtraReports.UI.XRSummary();            
            XSummary.FormatString = "{0:n2}";
            XSummary.IgnoreNullValues = true;
            XSummary.Running = DevExpress.XtraReports.UI.SummaryRunning.Report;
            XTotal.Summary = XSummary;
            XTotal.Text = "xrTableCell8";
            XTotal.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight;
            XTotal.Weight = WeightRef;

        }

        private void xrTableCell5_Draw(object sender, DrawEventArgs e)
        {
            Degrade(sender, e);
        }

    }
}
