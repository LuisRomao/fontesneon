﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Text;
using System.Windows.Forms;
using CompontesBasicos;
using VirEnumeracoesNeon;
using VirMSSQL;

namespace Balancete.GrBalancete
{
    /// <summary>
    /// Tela de saldos
    /// </summary>
    public partial class cSaldo : ComponenteBaseDialog
    {
        /// <summary>
        /// TipocSaldo
        /// </summary>
        public enum TipocSaldo 
        {
            /// <summary>
            /// Inicio
            /// </summary>
            Inicio,
            /// <summary>
            /// termino
            /// </summary>
            termino
        }

        private TipocSaldo tipoIF;

        /// <summary>
        /// Construtor padrao
        /// </summary>
        public cSaldo()
        {
            InitializeComponent();
        }

        private balancete balancete1;


        /// <summary>
        /// Construtor
        /// </summary>
        /// <param name="_balancete"></param>
        /// <param name="_tipoIF"></param>
        /// <param name="_somenteleitura"></param>
        public cSaldo(balancete _balancete, TipocSaldo _tipoIF,bool _somenteleitura = false)
        {            
            InitializeComponent();
            somenteleitura = _somenteleitura;
            balancete1 = _balancete;
            dBalanceteBindingSource.DataSource = balancete1.DBalancete1;
            Framework.virEnumCTLTipo.virEnumCTLTipoSt.CarregaEditorDaGrid(colCTLTipo);
            tipoIF = _tipoIF;            
            balancete1.DBalancete1.SaldoContaLogica.AcceptChanges();
            colSALDOI.Visible = tipoIF == TipocSaldo.Inicio;
            colSALDOF.Visible = !colSALDOI.Visible;
        }

        

        

        /// <summary>
        /// Fechar
        /// </summary>
        /// <param name="Resultado"></param>
        protected override void FechaTela(DialogResult Resultado)
        {            
            gridView1.CloseEditor();
            gridView1.ValidateEditor();
            Validate();
            if (Resultado == DialogResult.OK)
            {
                string comando = "SELECT BAL FROM BALancetes WHERE (BAL_CON = @P1) AND (BALCompet = @P2)";
                int? BAL = 0;
                BAL = TableAdapter.ST().BuscaEscalar_int(comando, balancete1.CON, balancete1.comp.CloneCompet(-1).CompetenciaBind);
                if (!BAL.HasValue)
                {
                    //dBalancete.BALancetesRow BALAnterior = balancete1.DBalancete1.BALancetes.NewBALancetesRow();
                    string Comandoin = "INSERT INTO BALancetes (BAL_CON, BALCompet, BALDataInicio, BALdataFim, BALSTATUS, BALObs) VALUES (@P1,@P2,@P3,@P4,@P5,@P6)";
                    try
                    {
                        VirMSSQL.TableAdapter.AbreTrasacaoSQL("Balancte cSaldo FechaTela - 89");
                        BAL = TableAdapter.ST().IncluirAutoInc(Comandoin,
                                                                                  balancete1.CON,
                                                                                  balancete1.comp.CloneCompet(-1).CompetenciaBind,
                                                                                  balancete1.DataI.AddMonths(-1),
                                                                                  balancete1.DataI.AddDays(-1),
                                                                                  (int)BALStatus.FechadoDefinitivo,
                                                                                  "Balantece inicial incluído somente para registro dos saldos");
                        VirMSSQL.TableAdapter.CommitSQL();
                    }
                    catch (Exception ex)
                    {
                        VirMSSQL.TableAdapter.VircatchSQL(ex);
                    }
                }
                foreach (dBalancete.ConTasLogicasRow rowCTL in balancete1.DBalancete1.ConTasLogicas)
                {
                    if (rowCTL["SALDOI", DataRowVersion.Current] != rowCTL["SALDOI", DataRowVersion.Original])
                        {
                            dBalancete.SaldoContaLogicaRow rowSCL = null;
                            foreach (dBalancete.SaldoContaLogicaRow candidato in balancete1.DBalancete1.SaldoContaLogica)
                                if ((candidato.SCL_CTL == rowCTL.CTL))
                                {
                                    rowSCL = candidato;
                                    break;
                                }
                            if (rowSCL == null)
                            {
                                rowSCL = balancete1.DBalancete1.SaldoContaLogica.NewSaldoContaLogicaRow();
                                rowSCL.SCL_BAL = BAL.Value;                                
                                rowSCL.SCL_CTL = rowCTL.CTL;
                                rowSCL.SCLSaldo = 0;
                                balancete1.DBalancete1.SaldoContaLogica.AddSaldoContaLogicaRow(rowSCL);
                            };
                            rowSCL.SCLSaldo = rowCTL.SALDOI;
                            balancete1.DBalancete1.SaldoContaLogicaTableAdapter.Update(rowSCL);
                            rowSCL.AcceptChanges();
                        }                                            
                }
            }
            base.FechaTela(Resultado);
        }

        

        //private impSaldo Impresso;        

    }
}
