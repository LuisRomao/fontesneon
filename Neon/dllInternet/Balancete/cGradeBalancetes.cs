using Framework;
using Balancete.GrBalancete;
using CompontesBasicosProc;
using CompontesBasicos;
using Framework.objetosNeon;
using FrameworkProc.datasets;
using System;
using System.Collections;
using System.Data;
using System.Drawing;
using System.Windows.Forms;
using VirMSSQL;
using VirEnumeracoesNeon;

namespace Balancete
{
    /// <summary>
    /// Grade balancetes
    /// </summary>
    public partial class cGradeBalancetes : ComponenteBase
    {
        private SortedList Chs;

        /// <summary>
        /// Construtor
        /// </summary>
        public cGradeBalancetes()
        {
            InitializeComponent();
            Chs = new SortedList();
            Chs.Add(BALStatus.NaoIniciado, checkE1);
            Chs.Add(BALStatus.Producao, checkE2);
            Chs.Add(BALStatus.Termindo, checkE3);
            Chs.Add(BALStatus.ParaPublicar, checkE4);
            Chs.Add(BALStatus.Publicado, checkE5);
            Chs.Add(BALStatus.FechadoDefinitivo, checkE6);
            Chs.Add(BALStatus.Enviado, checkE7);
            foreach (DictionaryEntry DE in Chs)
            {
                BALStatus status = (BALStatus)DE.Key;
                DevExpress.XtraEditors.CheckEdit Editor = (DevExpress.XtraEditors.CheckEdit)DE.Value;
                Editor.BackColor = Enumeracoes.virEnumBALStatus.virEnumBALStatusSt.GetCor(status);
                Editor.Text = Enumeracoes.virEnumBALStatus.virEnumBALStatusSt.Descritivo(status);
            };
            Enumeracoes.virEnumBALStatus.virEnumBALStatusSt.CarregaEditorDaGrid(colBALSTATUS);
            uSUariosBindingSource.DataSource = dUSUarios.dUSUariosStTodos;
            //Carrega();            
            BExp.Visible = (CompontesBasicos.Bancovirtual.UsuarioLogado.UsuarioLogadoSt.VerificaFuncionalidade("EXPORTAR") > 1);
            if (FormPrincipalBase.USULogado == 30)
                simpleButton2.Visible = memoEdit1.Visible = spinEdit1.Visible = true;
        }

        private bool Reentrada = false;

        private void Carrega()
        {
            if (Reentrada)
                return;
            try
            {
                Reentrada = true;
                using (CompontesBasicos.Espera.cEspera ESP = new CompontesBasicos.Espera.cEspera(this))
                {
                    ESP.Espere("Carregando dados");
                    Application.DoEvents();
                    try
                    {
                        gridView1.BeginDataUpdate();
                        bool Ocultos = (CompontesBasicos.Bancovirtual.UsuarioLogado.UsuarioLogadoSt.VerificaFuncionalidade("CONDOCULTOS") > 1);
                        dBalancetes.SaldoContaLogicaTableAdapter.Fill(dBalancetes.SaldoContaLogica);
                        dBalancetes.GradeBalancetes.Clear();
                        dBalancetes.GradeBalancetesTableAdapter.ClearBeforeFill = false;
                        foreach (DictionaryEntry DE in Chs)
                        {
                            BALStatus status = (BALStatus)DE.Key;
                            DevExpress.XtraEditors.CheckEdit Editor = (DevExpress.XtraEditors.CheckEdit)DE.Value;
                            if (Editor.Checked)
                            {
                                if (lookupCondominio_F1.CON_sel == -1)
                                {
                                    if (Ocultos)
                                        dBalancetes.GradeBalancetesTableAdapter.Fill(dBalancetes.GradeBalancetes, Framework.DSCentral.EMP, (int)status);
                                    else
                                        dBalancetes.GradeBalancetesTableAdapter.FillByLivres(dBalancetes.GradeBalancetes, Framework.DSCentral.EMP, (int)status);
                                }
                                else
                                    dBalancetes.GradeBalancetesTableAdapter.FillByCON(dBalancetes.GradeBalancetes, (int)status, lookupCondominio_F1.CON_sel);
                            }                            
                        };
                        gridView1.ExpandAllGroups();
                        foreach (dBalancetes.GradeBalancetesRow rowGrade in dBalancetes.GradeBalancetes)
                            if (!((BALStatus)rowGrade.BALSTATUS).EstaNoGrupo(BALStatus.Enviado))
                                if (rowGrade.IsdataLimiteNull())
                                    if (!rowGrade.IsCONLimiteBalNull())
                                    {
                                        DateTime datacorte;
                                        try
                                        {
                                            datacorte = new DateTime(rowGrade.BALdataFim.Year, rowGrade.BALdataFim.Month, rowGrade.CONLimiteBal);
                                        }
                                        catch
                                        {
                                            datacorte = new DateTime(rowGrade.BALdataFim.Year, rowGrade.BALdataFim.Month, 1).AddMonths(1).AddDays(-1);
                                        }
                                        if (datacorte <= rowGrade.BALdataFim)
                                            datacorte = datacorte.AddMonths(1);
                                        rowGrade.dataLimite = datacorte;
                                        TimeSpan TS = datacorte - rowGrade.BALdataFim;
                                        rowGrade.Folga = TS.Days;
                                    }
                    }
                    finally
                    {
                        gridView1.EndDataUpdate();
                    }
                }
            }
            finally
            {
                Reentrada = false;
            }
        }

        private void checkE1_CheckedChanged(object sender, EventArgs e)
        {
            if (blocCh)
                return;
            dBalancetes.GradeBalancetes.Clear();         
        }

        private void gridView1_RowStyle(object sender, DevExpress.XtraGrid.Views.Grid.RowStyleEventArgs e)
        {
            
        }

        private void gridView1_CustomColumnDisplayText(object sender, DevExpress.XtraGrid.Views.Base.CustomColumnDisplayTextEventArgs e)
        {
            if (e.Column == colBALCompet)
                if ((e.Value != null) && (e.Value != DBNull.Value) && (e.Value is int))
                {
                    int iValor = (int)e.Value;
                    e.DisplayText = string.Format("{0:00}/{1:0000}", iValor % 100, iValor / 100);
                }
        }

        private void gridView1_CalcRowHeight(object sender, DevExpress.XtraGrid.Views.Grid.RowHeightEventArgs e)
        {
            if (e.RowHandle >= 0)
            {                
                dBalancetes.GradeBalancetesRow rowBAL = (dBalancetes.GradeBalancetesRow)gridView1.GetDataRow(e.RowHandle);
                if (rowBAL == null)
                    return;
                Graphics g = CreateGraphics();
                SizeF size;
                int H1 = e.RowHeight;
                if (!rowBAL.IsBALObsNull() && gridView1.Columns[colBALObs.FieldName].Visible)
                {                    
                    size = g.MeasureString(rowBAL.BALObs, colBALObs.AppearanceCell.Font, colBALObs.Width);
                    double completeHeight = Math.Round(size.Height) + 10;
                    double lineHeight = colBALObs.AppearanceCell.Font.Height;
                    double correctedHeight = completeHeight;

                    if (completeHeight > lineHeight * 10)
                        correctedHeight = lineHeight * 10 + 3;
                    H1 = (int)Math.Truncate(correctedHeight);
                };
                int H2 = e.RowHeight;
                if (!rowBAL.IsCONObsBalNull() && gridView1.Columns[colCONObsBal.FieldName].Visible)
                {

                    size = g.MeasureString(rowBAL.CONObsBal, colCONObsBal.AppearanceCell.Font, colCONObsBal.Width);
                    double completeHeight = Math.Round(size.Height) + 10;
                    double lineHeight = colCONObsBal.AppearanceCell.Font.Height;
                    double correctedHeight = completeHeight;

                    if (completeHeight > lineHeight * 10)
                        correctedHeight = lineHeight * 10 + 3;
                    H2 = (int)Math.Truncate(correctedHeight);
                };
                e.RowHeight = (H1 > H2) ? H1:H2;
            }
        }

        private void repositoryItemButtonEdit1_ButtonClick(object sender, DevExpress.XtraEditors.Controls.ButtonPressedEventArgs e)
        {
            dBalancetes.GradeBalancetesRow rowBAL = (dBalancetes.GradeBalancetesRow)gridView1.GetFocusedDataRow();
            if (rowBAL != null)
            {
                int modelo = int.Parse(e.Button.Caption.Substring(1));
                Competencia comp = new Competencia(rowBAL.BALCompet);
                comp.CON = rowBAL.CON;
                dllpublicaBalancete.InterBal Bal = new dllpublicaBalancete.InterBal(CompontesBasicosProc.FuncoesBasicasProc.EstaEmProducao, Framework.DSCentral.EMP, comp, rowBAL.BALDataInicio, rowBAL.BALdataFim, rowBAL.BAL);
                if (Bal.URLPublicacao(modelo))
                {

                    CompontesBasicos.ComponenteWEB novoWeb = new CompontesBasicos.ComponenteWEB();
                    //proxy
                    novoWeb.MostraEndereco = false;
                    novoWeb.Doc = DockStyle.Fill;

                    /*
                    string teste = string.Format("www.neonimoveis.com.br/balancetes/{0}{1}{2}{3}.asp",
                        Framework.DSCentral.EMP,
                        rowBAL.CONCodigo.Replace(".", "_").Replace("\\", "_").Replace("/", "_").Replace(" ", "_"),
                        modelo,
                        comp.ToStringN());
                    Clipboard.SetText(teste);
                    MessageBox.Show(teste);*/


                    novoWeb.Navigate(string.Format("www.neonimoveis.com.br/balancetes/{0}{1}{2}{3}.asp",
                        Framework.DSCentral.EMP,
                        rowBAL.CONCodigo.Replace(".", "_").Replace("\\", "_").Replace("/", "_").Replace(" ", "_"),
                        modelo,
                        comp.ToStringN()));
                    novoWeb.Titulo = string.Format("{0} {1} M{2}",
                        rowBAL.CONCodigo,
                        rowBAL.BALCompet,
                        modelo);
                    novoWeb.VirShowModulo(CompontesBasicos.EstadosDosComponentes.JanelasAtivas);
                    ((DevExpress.XtraTab.XtraTabPage)novoWeb.Parent).ShowCloseButton = DevExpress.Utils.DefaultBoolean.True;
                }
                else
                    MessageBox.Show("Balancete n�o dispon�vel");
            }
        }

        private void MostraBalancete(bool resumido)
        {
            dBalancetes.GradeBalancetesRow rowBAL = (dBalancetes.GradeBalancetesRow)gridView1.GetFocusedDataRow();
            if (rowBAL != null)
            {
                Competencia comp = new Competencia(rowBAL.BALCompet);
                balancete.MostraBalancetePDF(rowBAL.CON, rowBAL.CONCodigo, comp, resumido);                
            }
        }

        private void repositoryItemButtonBalRed_ButtonClick(object sender, DevExpress.XtraEditors.Controls.ButtonPressedEventArgs e)
        {
            MostraBalancete(true);            
        }

        private void repositoryItemButtonBal_ButtonClick(object sender, DevExpress.XtraEditors.Controls.ButtonPressedEventArgs e)
        {
            MostraBalancete(false);
        }

        private void gridView1_RowCellStyle(object sender, DevExpress.XtraGrid.Views.Grid.RowCellStyleEventArgs e)
        {
            try
            {
                dBalancetes.GradeBalancetesRow rowBAL = (dBalancetes.GradeBalancetesRow)gridView1.GetDataRow(e.RowHandle);
                if (rowBAL == null)
                    return;
                if (e.Column == coldataLimite)
                {
                    e.Appearance.BackColor = Color.White;
                    e.Appearance.ForeColor = Color.Black;
                    if (rowBAL.IsdataLimiteNull())
                        e.Appearance.BackColor = e.Appearance.BackColor2 = Color.Lime;
                    else
                    {
                        BALStatus status = (BALStatus)rowBAL.BALSTATUS;
                        ArrayList Stok = new ArrayList(new BALStatus[] { BALStatus.ParaPublicar, BALStatus.Publicado, BALStatus.Termindo, BALStatus.FechadoDefinitivo, BALStatus.Enviado });
                        if (Stok.Contains(status))
                            e.Appearance.BackColor = e.Appearance.BackColor2 = Color.Lime;
                        else
                        {
                            if (rowBAL.dataLimite < DateTime.Today)
                                e.Appearance.BackColor = e.Appearance.BackColor2 = Color.Red;
                            else if (rowBAL.dataLimite < DateTime.Today.AddDays(3))
                                e.Appearance.BackColor = e.Appearance.BackColor2 = Color.Yellow;
                            else
                                e.Appearance.BackColor = e.Appearance.BackColor2 = Color.Wheat;
                        }
                    }
                }
                else
                {
                    e.Appearance.BackColor = Enumeracoes.virEnumBALStatus.virEnumBALStatusSt.GetCor(rowBAL.BALSTATUS);
                    e.Appearance.BackColor2 = Enumeracoes.virEnumBALStatus.virEnumBALStatusSt.GetCor(rowBAL.BALSTATUS);
                }
            }
            catch 
            {
                
            }
        }

        bool blocCh = false;

        /// <summary>
        /// Chamada gen�rica. O ideal seria usar o abstrato
        /// </summary>
        /// <param name="args"></param>
        /// <returns></returns>
        public override object[] ChamadaGenerica(params object[] args)
        {
            if (args.Length == 1)
            {
                blocCh = true;
                lookupGerente1.USUSel = (int)args[0];
                foreach (DictionaryEntry DE in Chs)
                {
                    BALStatus status = (BALStatus)DE.Key;
                    DevExpress.XtraEditors.CheckEdit Editor = (DevExpress.XtraEditors.CheckEdit)DE.Value;
                    //Editor.Checked = ((status == BALStatus.NaoIniciado) || (status == BALStatus.Producao));                    
                    Editor.Checked = status.EstaNoGrupo(BALStatus.NaoIniciado, BALStatus.Producao, BALStatus.Termindo, BALStatus.ParaPublicar, BALStatus.Publicado, BALStatus.FechadoDefinitivo);
                };
                blocCh = false;
                Carrega();
                if (lookupGerente1.USUSel == -1)                
                    gridView1.ActiveFilterString = string.Format("dataLimite < #{0:yyyy-MM-dd}# and dataLimite is not null", DateTime.Today);
                else
                    gridView1.ActiveFilterString = string.Format("dataLimite < #{0:yyyy-MM-dd}# and dataLimite is not null and [CONAuditor1_USU] = {1}", DateTime.Today, lookupGerente1.USUSel);
            }
            return null;
        }

        private void lookupGerente1_alterado(object sender, EventArgs e)
        {
            if (lookupGerente1.USUSel == -1)
            {
                if (gridView1.ActiveFilterString.Contains("[CONAuditor1_USU]"))
                    gridView1.ActiveFilterString = "";
            }
            else
                gridView1.ActiveFilterString = string.Format("[CONAuditor1_USU] = {0}", lookupGerente1.USUSel);
        }

        private void repositoryItemButtonEdit2_ButtonClick(object sender, DevExpress.XtraEditors.Controls.ButtonPressedEventArgs e)
        {
            dBalancetes.GradeBalancetesRow rowBAL = (dBalancetes.GradeBalancetesRow)gridView1.GetFocusedDataRow();            
            if (rowBAL != null)
            {
                using (CompontesBasicos.Espera.cEspera ESP = new CompontesBasicos.Espera.cEspera(this))
                {
                    ESP.Espere("Carregando...");
                    
                    System.Windows.Forms.Application.DoEvents();
                    balancete balanceteAbrir = null;
                    //pegar direto do banco de dados para evitar informa��o desatualizada
                    BALStatus Status = (BALStatus)TableAdapter.ST().BuscaEscalar_int("select BALSTATUS from balancetes where BAL = @P1",rowBAL.BAL).Value;

                    if (Status.EstaNoGrupo(BALStatus.FechadoDefinitivo, BALStatus.Enviado))
                    {
                        balanceteAbrir = balancete.VirConstrutor(rowBAL.CON, new Competencia(rowBAL.BALCompet), TipoBalancete.Balancete);
                        if (!balanceteAbrir.SalvoXML)
                        {
                            if (MessageBox.Show("ATEN��O:\r\nO arquivo com as dados do balancete n�o est� dispon�vel. Recalcular o balancete? Os dados podem n�o ser fieis ao impresso", "ERRO", MessageBoxButtons.YesNo, MessageBoxIcon.Warning, MessageBoxDefaultButton.Button2) == DialogResult.No)
                                return;
                        }
                        else
                            balanceteAbrir.BALrow.BALSTATUS = rowBAL.BALSTATUS;
                    }
                    if(balanceteAbrir == null)
                        balanceteAbrir = new balancete(rowBAL.BAL);
                    
                    GrBalancete.balancete balancete1 = balanceteAbrir;
                    
                    balancete1.ESP = ESP;
                    if (balancete1.encontrado)
                    {
                        
                        balancete1.Abrir(false, EstadosDosComponentes.JanelasAtivas);
                        
                    }
                    balancete1.ESP = null;
                }
                
            }
        }

        private void gridView1_CustomRowCellEdit(object sender, DevExpress.XtraGrid.Views.Grid.CustomRowCellEditEventArgs e)
        {
            dBalancetes.GradeBalancetesRow rowBAL = (dBalancetes.GradeBalancetesRow)gridView1.GetDataRow(e.RowHandle);
            if (rowBAL != null)
            {
                if (e.Column.Name == colBALN.Name)
                {
                    if ((!rowBAL.IsCONDivideSaldosNull()) && rowBAL.CONDivideSaldos)
                        e.RepositoryItem = repositoryItemButtonBalN;
                }
                else if (rowBAL.GetSaldoContaLogicaRows().Length > 0)
                {
                    BALStatus Status = (BALStatus)rowBAL.BALSTATUS;
                    if (Status.EstaNoGrupo(BALStatus.FechadoDefinitivo, BALStatus.Publicado, BALStatus.Enviado))
                    {
                        if (e.Column.Name == colBALred.Name)
                        {
                            e.RepositoryItem = repositoryItemButtonBalRed;
                        }
                        else if (e.Column.Name == colBAL.Name)
                        {
                            e.RepositoryItem = repositoryItemButtonBal;
                        }
                    }
                }
            }
        }

        /// <summary>
        /// Atualiza os dados
        /// </summary>
        public override void RefreshDados()
        {
            dBalancetes.GradeBalancetes.Clear();
            //timerCalcular.Enabled = true;
        }

        
        /*
        private void BotRetro_Click(object sender, EventArgs e)
        {
            if (!BotCanRetro.Visible)                            
            {
                BotCanRetro.Visible = true;
                StringBuilder Resposta = new StringBuilder();
                int i = 1;
                Resposta.AppendLine(string.Format("{0}", DateTime.Now));
                foreach (dBalancetes.GradeBalancetesRow rowGrade in dBalancetes.GradeBalancetes)
                {
                    i++;
                    if (((BALStatus)rowGrade.BALSTATUS == BALStatus.FechadoDefinitivo) && (rowGrade.GetSaldoContaLogicaRows().Length > 0))
                    {
                        //if (i++ > 3)
                        //    break;
                        Competencia comp = new Competencia(rowGrade.BALCompet);
                        balancete balancete1 = balancete.VirConstrutor(rowGrade.CON, comp);
                        balancete1.Silencioso = true;
                        if (!balancete1.SomenteLeitura)
                            if (balancete1.SalvarRetroativo(this) && (balancete1.UltimoErro == null))
                                Resposta.AppendLine(string.Format("{0}\t{1}\tsalvo", rowGrade.CONCodigo, comp));
                            else
                                Resposta.AppendLine(string.Format("{0}\t{1}\terro", rowGrade.CONCodigo, comp));
                        else
                            Resposta.AppendLine(string.Format("{0}\t{1}\tok", rowGrade.CONCodigo, comp));
                        BotRetro.Text = string.Format("{0:n0}/{1:n0}", i, dBalancetes.GradeBalancetes.Count);
                        Application.DoEvents();
                        if (!BotCanRetro.Visible)
                        {
                            Resposta.AppendLine(string.Format("Interrompido"));
                            break;
                        }
                    };
                }
                Resposta.AppendLine(string.Format("{0}", DateTime.Now));
                Clipboard.SetText(Resposta.ToString());
                MessageBox.Show(Resposta.ToString());
            }            
        }
        */
        
        /*
        private void timerCalcular_Tick(object sender, EventArgs e)
        {
            timerCalcular.Enabled = false;
            Carrega();
        }*/

        private void simpleButton4_Click(object sender, EventArgs e)
        {
            SaveFileDialog dialogo = new SaveFileDialog();
            dialogo.DefaultExt = ".xlsx";
            if (dialogo.ShowDialog() == DialogResult.OK)
            {
                gridView1.Export(DevExpress.XtraPrinting.ExportTarget.Xlsx, dialogo.FileName);                
            }
        }

        private void simpleButton1_Click(object sender, EventArgs e)
        {
            Carrega();
        }

        private void simpleButton2_Click(object sender, EventArgs e)
        {
            VirOleDb.TableAdapter TATeste = new VirOleDb.TableAdapter();
            TATeste.TrocarStringDeConexao(VirDB.Bancovirtual.TiposDeBanco.AccessH);
            string ComandoBuscaAtualizar =
"SELECT        dbo.BALancetes.BALCompet, dbo.CONDOMINIOS.CON, dbo.CONDOMINIOS.CONCodigo\r\n" +
"FROM            dbo.BALancetes INNER JOIN\r\n" +
"                         dbo.CONDOMINIOS ON dbo.BALancetes.BAL_CON = dbo.CONDOMINIOS.CON\r\n" +
"WHERE        (dbo.BALancetes.BALCompet >= 201412) AND (dbo.BALancetes.BALSTATUS = 5) and CONStatus = 1 and CON_EMP = 1 and con = @P1\r\n" +
"order by CON,BALCompet;";

            string ComandoChecar =
"SELECT        Codcon\r\n" +
"FROM            [Hist�rico Lan�amentos]\r\n" +
"WHERE        (Codcon = @P1) AND ([M�s Compet�ncia] = '102015');";

            DataTable DT = VirMSSQL.TableAdapter.STTableAdapter.BuscaSQLTabela(ComandoBuscaAtualizar,spinEdit1.Value);

            //int CON = 829;
            //int nCompetencia = 201412;
            //int BAL = 20340;

            memoEdit1.Text = "Inicio\r\n";
            string Pular = "";
            string NaoPular = "";
            foreach (DataRow DR in DT.Rows)
            {
                try
                {
                    Competencia Comp = new Competencia((int)DR["BALCompet"]);
                    int CON = (int)DR["CON"];
                    memoEdit1.Text = string.Format("Con = {0} ({1}) comp = {2}\r\n", DR["CONCodigo"], CON, Comp) + memoEdit1.Text;
                    Application.DoEvents();
                    string CODCON = (string)DR["CONCodigo"];
                    if (CODCON != NaoPular)
                    {
                        if ((CODCON == Pular) || (TATeste.EstaCadastrado(ComandoChecar, CODCON)))
                        {
                            memoEdit1.Text = string.Format("Pular\r\n") + memoEdit1.Text;
                            Pular = CODCON;
                            continue;
                        }
                        else
                            NaoPular = CODCON;
                    }

                    //dBalancetes.GradeBalancetesTableAdapter.FillByCON(dBalancetes.GradeBalancetes, 5, lookupCondominio_F1.CON_sel);


                    //for (Competencia Comp = new Competencia(201412); Comp < new Competencia(201512); Comp++)
                    //{
                    balancete balanceteAbrir = null;
                    balanceteAbrir = balancete.VirConstrutor(CON, Comp, TipoBalancete.Balancete);
                    //if (balanceteAbrir == null)
                    //    balanceteAbrir = new balancete(BAL);
                    if (balanceteAbrir != null)
                        balanceteAbrir.GravaHistorico();
                    //}
                }
                catch (Exception ex)
                {
                    memoEdit1.Text = string.Format("Erro: {0}\r\n",ex.Message) + memoEdit1.Text;
                }
            }
            memoEdit1.Text = "Terminado";
        }
    }
}
