using System;
using CompontesBasicos;
using dllpublicaBalancete;
using Framework.datasets;
using Framework.objetosNeon;
using System.Data;
using System.Windows.Forms;
using System.Collections;
using DevExpress.XtraReports.UI;

namespace Balancete
{
    /// <summary>
    /// Acumulado
    /// </summary>
    public partial class cAcumulado : ComponenteBase
    {
        enum Detalhamento
        {
            Plano,
            Descricao,
            Documento
        }

        private Detalhamento SelDet 
        {
            get 
            {
                switch (RadDet.EditValue.ToString())
                {
                    default:
                    case "P":
                        return Detalhamento.Plano;
                    case "Det":
                        return Detalhamento.Descricao;
                    case "SuperDet":
                        return Detalhamento.Documento;
                }

            }
        }

        private bool BcolAc;
        private bool BcolA;
        private bool BcolM;
        private bool BcolD;

        private void SelColunas()
        {
            BcolAc = BcolA = BcolM = BcolD = false;
            
            switch (RadColunas.EditValue.ToString())
                {
                    default:
                    case "T":
                        BcolM = true;
                        break;
                    case "AMD":
                        BcolA = BcolM = BcolD = true;
                        break;
                    case "AcAMD":
                        BcolAc = BcolA = BcolM = BcolD = true;
                        break;
                    case "Ac":
                        BcolAc = true;
                        break;
                }
            
        }

        /// <summary>
        /// construtor
        /// </summary>
        public cAcumulado()
        {
            InitializeComponent();
            cCompetI.IsNull = cCompetF.IsNull = true;
        }

        private dPublicah12 dPub;

        private dPublicah12 DPub
        {
            get
            {
                return dPub ?? (dPub = new dPublicah12());
            }           
        }

        private bool Calculando;
        private bool Abortar;

        private void CalculaColunaImpresso()
        {
            foreach (dAcumulado.AcumuladoRow row in dAcumulado.Acumulado)
            {
              
                if (row.CredDeb.StartsWith("1"))
                    foreach (Competencia TColuna in Competencias)
                    {
                        
                        {
                            decimal Ac = 0;
                            decimal A = (row[TColuna.ToString() + "A"] == DBNull.Value) ? 0 : (decimal)row[TColuna.ToString() + "A"];
                            decimal M = (row[TColuna.ToString() + "M"] == DBNull.Value) ? 0 : (decimal)row[TColuna.ToString() + "M"];
                            decimal D = (row[TColuna.ToString() + "D"] == DBNull.Value) ? 0 : (decimal)row[TColuna.ToString() + "D"];
                            if (BcolAc)
                            {
                                Ac = (row[TColuna.ToString() + "AC"] == DBNull.Value) ? 0 : (decimal)row[TColuna.ToString() + "AC"];
                                row[TColuna.ToString() + "IMP"] = string.Format("Ac. {0,9:n2}\r\n", Ac);
                            }
                            else
                                row[TColuna.ToString() + "IMP"] = "";
                            row[TColuna.ToString() + "IMP"] += string.Format("At. {0,9:n2}\r\n", A);
                            row[TColuna.ToString() + "IMP"] += string.Format("M�s {0,9:n2}\r\n", M);
                            row[TColuna.ToString() + "IMP"] += string.Format("Ad. {0,9:n2}", D);
                            row[TColuna.ToString() + "IMPT"] = Ac + A + M + D;
                        }
                        
                    }
                else
                    foreach (Competencia TColuna in Competencias)
                        {
                            row[TColuna.ToString() + "IMPT"] = row[TColuna.ToString() + "M"];
                        }
            }
        }

        private ImpAcumulado CriaImpresso(bool linhas,bool apontar)
        {
            if (BcolA)
                CalculaColunaImpresso();
            ImpAcumulado.TTipo Tipo;
            if (BcolAc && !BcolM)
                Tipo = ImpAcumulado.TTipo.acordo;
            else
                if (BcolA)
                    Tipo = ImpAcumulado.TTipo.dividido;
                else
                    Tipo = ImpAcumulado.TTipo.totalmes;
            return new ImpAcumulado(dAcumulado, linhas, apontar ? lookupCondominio_F1.CON_sel : -1, lookupCondominio_F1.CON_selrow.CONNome, Competencias, Tipo);               
        }

        /*
        private dllpublicaBalancete.Publicah12 _Publicah121;

        private dllpublicaBalancete.Publicah12 Publicah121
        {
            get 
            {
                return _Publicah121 ?? (_Publicah121 = new Publicah12(CompontesBasicos.FormPrincipalBase.strEmProducao, Framework.DSCentral.EMP));
                //return _Publicah121 ?? (_Publicah121 = new Publicah12(true, Framework.DSCentral.EMP));
            }
        }*/

        private ClientWCFFtp.ClientFTP _clientFTP;

        private ClientWCFFtp.ClientFTP clientFTP
        {
            get
            {
                if (_clientFTP == null)
                    _clientFTP = new ClientWCFFtp.ClientFTP(CompontesBasicosProc.FuncoesBasicasProc.EstaEmProducao, Framework.DSCentral.USU, Framework.DSCentral.EMP);
                return _clientFTP;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="CON"></param>
        /// <returns></returns>
        public bool publicah12(int CON)
        {
            string nomeArquivo = string.Format(@"{0}\TMP\{1}_{2}_h12.pdf", System.IO.Path.GetDirectoryName(Application.ExecutablePath), Framework.DSCentral.EMP, CON);            
            lookupCondominio_F1.CON_sel = CON;
            chOutros.Checked = false;
            Calcular();
            ImpAcumulado Impresso = CriaImpresso(true,false);
            Impresso.CreateDocument();
            Impresso.ExportOptions.Pdf.Compressed = true;
            Impresso.ExportOptions.Pdf.ImageQuality = DevExpress.XtraPrinting.PdfJpegImageQuality.Lowest;
            Impresso.ExportToPdf(nomeArquivo);
            //return Publicah121.Publicar(nomeArquivo);
            return clientFTP.Put(nomeArquivo);
        }

        private void cBotaoImpBol1_clicado(object sender, EventArgs e)
        {
            if (dAcumulado.Acumulado.Count == 0)
                Calcular();
            if (cBotaoImpBol1.BotaoClicado != dllImpresso.Botoes.Botao.botao)
            {
                
                ImpAcumulado Impresso;
                switch (cBotaoImpBol1.BotaoClicado)
                {
                    case dllImpresso.Botoes.Botao.botao:
                    case dllImpresso.Botoes.Botao.nulo:
                        break;
                    case dllImpresso.Botoes.Botao.email:
                        Impresso = CriaImpresso(true,false);
                        Impresso.CreateDocument();
                        if (VirEmailNeon.EmailDiretoNeon.EmalST.Enviar("", "Debito_Credito", Impresso, "Relat�rio em anexo", "Relat�rio D�bito - Cr�dito"))
                            MessageBox.Show("E.mail enviado");
                        else
                        {
                            if (VirEmailNeon.EmailDiretoNeon.EmalST.UltimoErro != null)
                                MessageBox.Show("Falha no envio");
                            else
                                MessageBox.Show("Cancelado");
                        };
                        break;
                    case dllImpresso.Botoes.Botao.imprimir:                        
                    case dllImpresso.Botoes.Botao.imprimir_frente:
                        Impresso = CriaImpresso(true, true);
                        Impresso.CreateDocument();
                        Impresso.Print();
                        break;
                    case dllImpresso.Botoes.Botao.PDF_frente:                    
                    case dllImpresso.Botoes.Botao.pdf:
                        Impresso = CriaImpresso(true, false);
                        Impresso.CreateDocument();
                        using (SaveFileDialog saveFileDialog1 = new SaveFileDialog())
                        {
                            saveFileDialog1.DefaultExt = "pdf";
                            if (saveFileDialog1.ShowDialog() == DialogResult.OK)
                            {
                                Impresso.ExportOptions.Pdf.Compressed = true;
                                Impresso.ExportOptions.Pdf.ImageQuality = DevExpress.XtraPrinting.PdfJpegImageQuality.Lowest;
                                Impresso.ExportToPdf(saveFileDialog1.FileName);
                            }
                        }
                        break;
                    case dllImpresso.Botoes.Botao.tela:
                        Impresso = CriaImpresso(true, false);
                        Impresso.CreateDocument();
                        Impresso.ShowPreviewDialog();
                        break;
                    default:
                        break;
                }
            }
        }

        private void Calcular()
        {            
            if (Calculando)
                return;
            if (!Mostrar)
                return;
            Competencias = new ArrayList();
            SelColunas();
            object DataSourceGuardado = null;
            try
            {
                Calculando = true;
                Abortar = false;
                //bandedGridView1.BeginDataUpdate();
                DataSourceGuardado = gridControl1.DataSource;
                gridControl1.DataSource = null;
                Competencia comp = null;
                DateTime De = DateTime.Now;
                DateTime Ate = DateTime.Now;
                dAcumulado.Acumulado.Clear();
                colTotalA.Visible = BcolA;
                colTotalD.Visible = BcolD;
                colTotalAC.Visible = BcolAc && BcolM;
                colTotalM.Visible = BcolM && BcolA;
                Limpa();
                DPub.Historico12CredTableAdapter.ApagaNulo();
                int n = DPub.Historico12CredTableAdapter.Fill(dPub.Historico12Cred, lookupCondominio_F1.CON_selrow.CONCodigo);
                using (CompontesBasicos.Espera.cEspera Espera = new CompontesBasicos.Espera.cEspera(this))
                {
                    Espera.Espere("Carregando dados");
                    Espera.AtivaGauge(n);
                    foreach (dPublicah12.Historico12CredRow row in DPub.Historico12Cred)
                    {
                        Espera.Gauge();
                        if ((Espera.Abortar) || Abortar)
                        {
                            Abortar = true;
                            break;
                        }
                        if ((!row.Tipopag.StartsWith("1")) && (!BcolM))
                            continue;
                        if (row.Tipopag.StartsWith("5") && (!row.IsValor_PagoNull()))
                            row.Valor_Pago = -row.Valor_Pago;
                        int ano = int.Parse(row.ANO);
                        int mes = int.Parse(row.MES);
                        if ((comp == null) || (comp.Mes != mes) || (comp.Ano != ano))
                        {
                            comp = new Competencia(mes, ano);
                            if (!cCompetI.IsNull && (comp < cCompetI.Comp))
                                continue;
                            if (!cCompetF.IsNull && (comp > cCompetF.Comp))
                                continue;
                            comp.CON = lookupCondominio_F1.CON_sel;
                            comp.PeriodoBalancete(out De, out Ate);
                            if (BcolA || BcolAc)
                            {
                                dPub.BOLetosTableAdapter.FillBy(dPub.BOLetos, De, Ate, lookupCondominio_F1.CON_sel);
                                if (BcolAc)
                                    dPub.BOLetosAcordoTableAdapter.Fill(dPub.BOLetosAcordo, De, Ate, lookupCondominio_F1.CON_sel);
                            }
                            CriaCompetencia(comp);
                        };
                        string Titulo;
                        if (!cCompetI.IsNull && (comp < cCompetI.Comp))
                            continue;
                        if (!cCompetF.IsNull && (comp > cCompetF.Comp))
                            continue;
                        dPLAnocontas.PLAnocontasRow rowPLA = dPLAnocontas.dPLAnocontasSt.PLAnocontas.FindByPLA(row.Tipopag);

                        if ((SelDet == Detalhamento.Documento) && !row.IsN�mero_DocumentoNull())
                            Titulo = String.Format("{0} - {1}", row.N�mero_Documento, row.Descri��o);
                        else
                            if (SelDet != Detalhamento.Plano)
                                Titulo = row.Descri��o;
                            else
                            {
                                
                                Titulo = rowPLA == null ? row.Tipopag : rowPLA.PLADescricao;
                            }
                        dPublicah12.BOLetosRow rowBOL = null;
                        if ((row.Cr�dito) && (row.Tipopag.StartsWith("1")))
                        {
                            if ((BcolA || BcolAc) && !row.IsN�mero_DocumentoNull())
                                rowBOL = dPub.BOLetos.FindByBOL((int)row.N�mero_Documento);
                            if ((BcolA || BcolAc) && (rowBOL == null) && (SelDet == Detalhamento.Documento))
                                    Titulo = "* " + Titulo;

                        }
                        dAcumulado.AcumuladoRow rowAc = dAcumulado.Acumulado.FindByPLATitulo(row.Tipopag,Titulo);
                        if (rowAc == null)
                        {
                            rowAc = dAcumulado.Acumulado.NewAcumuladoRow();
                            rowAc.Titulo = Titulo;
                            if (rowPLA != null)
                                rowAc.PLADescricao = rowPLA.PLADescricao;
                            rowAc.PLA = row.Tipopag;
                            rowAc.TotalA = rowAc.TotalM = rowAc.TotalD = rowAc.TotalAC = rowAc.Total = 0;

                            if (row.Tipopag.StartsWith("1"))
                            {
                                rowAc.CredDeb = "1 - RECEITAS";
                                if (!chCredito.Checked)
                                    continue;
                            }
                            else
                                if (row.Tipopag.StartsWith("2"))
                                {
                                    rowAc.CredDeb = "2 - DESPESAS";
                                    if (!chDebito.Checked)
                                        continue;
                                }
                                else
                                {
                                    rowAc.CredDeb = "3 - OUTROS";
                                    if (!chOutros.Checked)
                                        continue;
                                }
                            //dAcumulado.Acumulado.AddAcumuladoRow(rowAc);
                        };

                        

                        DataColumn campoX = campoM;
                        string campoTX = "TotalM";



                        if (rowBOL != null)
                        {
                            if (rowBOL.BOLVencto < De)
                            {
                                campoX = campoA;
                                campoTX = "TotalA";
                            }
                            else
                                if (rowBOL.BOLVencto > Ate)
                                {
                                    campoX = campoD;
                                    campoTX = "TotalD";
                                }

                            if (BcolAc)
                            {
                                if ((rowBOL.BOLTipoCRAI == "A") || (dPub.BOLetosAcordo.FindByBOL(rowBOL.BOL) != null))
                                {
                                    campoX = campoAC;
                                    campoTX = "TotalAC";
                                }
                            }
                        }
                        

                        if ((campoX == campoM) && !BcolM)
                            continue;
                        if ((campoX == campoA) && !BcolA)
                            continue;
                        if ((campoX == campoD) && !BcolD)
                            continue;

                        if (rowAc.RowState == DataRowState.Detached)
                            dAcumulado.Acumulado.AddAcumuladoRow(rowAc);

                        if (rowAc[campoX] == DBNull.Value)
                            rowAc[campoX] = (decimal)row.Valor_Pago;
                        else
                            rowAc[campoX] = (decimal)rowAc[campoX] + (decimal)row.Valor_Pago;
                        rowAc[campoTX] = (decimal)rowAc[campoTX] + (decimal)row.Valor_Pago;
                        rowAc.Total += (decimal)row.Valor_Pago;
                    };
                };
                if (Competencias.Count == 0)
                {
                    cCompetI.IsNull = cCompetF.IsNull = true;
                }
                else
                {
                    if (cCompetI.IsNull)
                        cCompetI.Comp = (Competencia)Competencias[0];
                    if (cCompetF.IsNull)
                        cCompetF.Comp = (Competencia)Competencias[Competencias.Count - 1];
                    foreach (dAcumulado.AcumuladoRow row in dAcumulado.Acumulado)
                        row.Media = row.Total / Competencias.Count;
                    
                }
            }
            finally 
            {
                if (Abortar)
                    dAcumulado.Acumulado.Clear();
                Abortar = false;
                //bandedGridView1.EndDataUpdate();
                Calculando = false;
                gridControl1.DataSource = DataSourceGuardado; 
            }        
        }

        private bool? _MostraOcultos;

        private bool Mostrar
        {
            get 
            {
                if (!_MostraOcultos.HasValue)
                    _MostraOcultos = (CompontesBasicos.Bancovirtual.UsuarioLogado.UsuarioLogadoSt.VerificaFuncionalidade("CONDOCULTOS") > 1);
                if (_MostraOcultos.Value)
                    return true;
                else
                    return lookupCondominio_F1.CON_selrow.IsCONOcultoNull() || !lookupCondominio_F1.CON_selrow.CONOculto;
            }
        }        

        private void lookupCondominio_F1_alterado(object sender, EventArgs e)
        {
            if (Calculando)
                Abortar = true;
            if (lookupCondominio_F1.CON_sel != -1)
                cBotaoImpBol1.Enabled = Mostrar;
            else
                cBotaoImpBol1.Enabled = false;
            LimpaTela();
        }

        private DataColumn campoA;
        private DataColumn campoM;
        private DataColumn campoD;        
        private DataColumn campoAC;
        private ArrayList _NovasBandas;
        private ArrayList NovasBandas
        {
            get 
            {
                if (_NovasBandas == null)
                {
                    _NovasBandas = new ArrayList();
                    NovosTotais = new ArrayList();
                }
                return _NovasBandas; 
            }
        }
        private ArrayList NovosTotais;
        private ArrayList Competencias;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn NovaColunaA;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn NovaColunaM;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn NovaColunaD;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn NovaColunaAC;

        private void CriaCampos(Competencia comp)
        {
            if (BcolM)
                if (dAcumulado.Acumulado.Columns.Contains(comp.ToString() + "M"))
                    campoM = dAcumulado.Acumulado.Columns[comp.ToString() + "M"];
                else
                    campoM = dAcumulado.Acumulado.Columns.Add(comp.ToString() + "M", typeof(decimal));
            if (BcolAc)
            {
                if (dAcumulado.Acumulado.Columns.Contains(comp.ToString() + "AC"))
                    campoAC = dAcumulado.Acumulado.Columns[comp.ToString() + "AC"];
                else
                    campoAC = dAcumulado.Acumulado.Columns.Add(comp.ToString() + "AC", typeof(decimal));
            }


            if (BcolA)
            {

                if (dAcumulado.Acumulado.Columns.Contains(comp.ToString() + "A"))
                {
                    campoA = dAcumulado.Acumulado.Columns[comp.ToString() + "A"];
                    campoD = dAcumulado.Acumulado.Columns[comp.ToString() + "D"];
                }
                else
                {
                    campoA = dAcumulado.Acumulado.Columns.Add(comp.ToString() + "A", typeof(decimal));
                    campoD = dAcumulado.Acumulado.Columns.Add(comp.ToString() + "D", typeof(decimal));
                    dAcumulado.Acumulado.Columns.Add(comp.ToString() + "IMP", typeof(string));
                    dAcumulado.Acumulado.Columns.Add(comp.ToString() + "IMPT", typeof(decimal));
                }
            }
        }

        private void CriaCompetencia(Competencia comp)
        {
            CriaCampos(comp);
            Competencias.Add(comp);
            DevExpress.XtraGrid.GridGroupSummaryItem Novo;                        
            DevExpress.XtraGrid.Views.BandedGrid.GridBand NovaBanda = bandedGridView1.Bands.AddBand(comp.ToString());
            NovasBandas.Add(NovaBanda);
            if (BcolM)
            {
                NovaColunaM = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
                NovaColunaM.FieldName = comp.ToString() + "M";
                NovaColunaM.Caption = (BcolA) ? "M�s" : comp.ToString();
                NovaColunaM.Visible = true;
                NovaColunaM.OptionsColumn.FixedWidth = true;
                NovaColunaM.Width = 65;
                NovaColunaM.ColumnEdit = repositoryItemCalcEdit1;
                Novo = new DevExpress.XtraGrid.GridGroupSummaryItem(DevExpress.Data.SummaryItemType.Sum, comp.ToString() + "M", NovaColunaM, "{0:n2}");
                bandedGridView1.GroupSummary.Add(Novo);
                NovosTotais.Add(Novo);                
            }
            else
                NovaColunaM = null;

            if (BcolAc)
            {
                NovaColunaAC = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
                NovaColunaAC.FieldName = comp.ToString() + "AC";
                NovaColunaAC.Caption = (BcolA) ? "Acordo" : " ";
                NovaColunaAC.Visible = true;
                NovaColunaAC.OptionsColumn.FixedWidth = true;
                NovaColunaAC.Width = 65;
                NovaColunaAC.ColumnEdit = repositoryItemCalcEdit1;
                Novo = new DevExpress.XtraGrid.GridGroupSummaryItem(DevExpress.Data.SummaryItemType.Sum, comp.ToString() + "AC", NovaColunaAC, "{0:n2}");
                bandedGridView1.GroupSummary.Add(Novo);
                NovosTotais.Add(Novo);                
            }
            else
                NovaColunaAC = null;


            if (BcolA)
            {                
                NovaColunaA = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
                NovaColunaD = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
                NovaColunaA.FieldName = comp.ToString() + "A";
                NovaColunaD.FieldName = comp.ToString() + "D";
                NovaColunaA.Caption = "Atrasado";
                NovaColunaD.Caption = "Antecipado";
                NovaColunaA.Visible = NovaColunaD.Visible = true;
                NovaColunaA.OptionsColumn.FixedWidth = NovaColunaD.OptionsColumn.FixedWidth = true;
                NovaColunaA.Width = NovaColunaD.Width = 65;
                NovaColunaA.ColumnEdit = NovaColunaD.ColumnEdit = repositoryItemCalcEdit1;
                Novo = new DevExpress.XtraGrid.GridGroupSummaryItem(DevExpress.Data.SummaryItemType.Sum, comp.ToString() + "A", NovaColunaA, "{0:n2}");
                bandedGridView1.GroupSummary.Add(Novo);
                NovosTotais.Add(Novo);
                Novo = new DevExpress.XtraGrid.GridGroupSummaryItem(DevExpress.Data.SummaryItemType.Sum, comp.ToString() + "D", NovaColunaD, "{0:n2}");
                //Novo = new DevExpress.XtraGrid.GridGroupSummaryItem(DevExpress.Data.SummaryItemType.Sum, comp.ToString() + "D", NovaColunaA, "{0:n2}");
                bandedGridView1.GroupSummary.Add(Novo);
                NovosTotais.Add(Novo);
            }
            else            
                NovaColunaA = NovaColunaD = null;
            if(BcolAc)
                NovaBanda.Columns.Add(NovaColunaAC);
            if (BcolA)
                NovaBanda.Columns.Add(NovaColunaA);
            if (BcolM)
                NovaBanda.Columns.Add(NovaColunaM);
            if (BcolA)
                NovaBanda.Columns.Add(NovaColunaD);            
        }

        private void Limpa()
        {
            foreach (DevExpress.XtraGrid.Views.BandedGrid.GridBand NovaBanda in NovasBandas)
            {
                NovaBanda.Columns.Clear();
                bandedGridView1.Bands.Remove(NovaBanda);
            }
            
            foreach (DevExpress.XtraGrid.GridGroupSummaryItem NovoTot in NovosTotais)
                bandedGridView1.GroupSummary.Remove(NovoTot);
            NovasBandas.Clear();
            NovosTotais.Clear();
        }

        private void simpleButton1_Click(object sender, EventArgs e)
        {
            using (SaveFileDialog saveFileDialog1 = new SaveFileDialog())
            {
                saveFileDialog1.DefaultExt = "xls";
                if (saveFileDialog1.ShowDialog() == DialogResult.OK)
                {
                    bandedGridView1.ExportToXls(saveFileDialog1.FileName);
                    
                    if (MessageBox.Show(string.Format("Arquivo salvo: {0}\r\nVoc� gostaria de abrir o arquivo?", saveFileDialog1.FileName), "Exportar Para...", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
                        try
                        {
                            using (System.Diagnostics.Process process = new System.Diagnostics.Process())
                            {
                                process.StartInfo.FileName = saveFileDialog1.FileName;
                                process.StartInfo.Verb = "Open";
                                process.StartInfo.WindowStyle = System.Diagnostics.ProcessWindowStyle.Maximized;
                                process.Start();
                            }
                        }
                        catch
                        {
                            MessageBox.Show(this, "N�o foi encontrado em seu sistema operacional um aplicativo apropriado para abrir o arquivo com os dados exportados.", Application.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Error);
                        }
                }
            }
        }

        private void simpleButton2_Click(object sender, EventArgs e)
        {            
            using (SaveFileDialog saveFileDialog1 = new SaveFileDialog())
            {
                saveFileDialog1.DefaultExt = "xls";
                if (saveFileDialog1.ShowDialog() == DialogResult.OK)
                {
                    if (dAcumulado.Acumulado.Count == 0)
                        Calcular();
                    using (ImpAcumulado Impresso = CriaImpresso(false,false))            
                    {
                        Impresso.CreateDocument();
                        Impresso.ExportOptions.Xls.SheetName = "Boletos";
                        Impresso.ExportOptions.Xls.ShowGridLines = false;
                        Impresso.ExportOptions.Xls.TextExportMode = DevExpress.XtraPrinting.TextExportMode.Value;
                        Impresso.ExportToXls(saveFileDialog1.FileName);
                    }
                    if (MessageBox.Show(string.Format("Arquivo salvo: {0}\r\nVoc� gostaria de abrir o arquivo?", saveFileDialog1.FileName), "Exportar Para...", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
                        try
                        {
                            using (System.Diagnostics.Process process = new System.Diagnostics.Process())
                            {
                                process.StartInfo.FileName = saveFileDialog1.FileName;
                                process.StartInfo.Verb = "Open";
                                process.StartInfo.WindowStyle = System.Diagnostics.ProcessWindowStyle.Maximized;
                                process.Start();
                            }
                        }
                        catch
                        {
                            MessageBox.Show(this, "N�o foi encontrado em seu sistema operacional um aplicativo apropriado para abrir o arquivo com os dados exportados.", Application.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Error);
                        }
                }
            }
        }

        private void LimpaTela()
        {
            dAcumulado.Acumulado.Clear();
            //cCompetI.IsNull = cCompetF.IsNull = true;
        }

        private void checkEdit2_CheckedChanged(object sender, EventArgs e)
        {
            LimpaTela();
        }

        private void chCredito_CheckedChanged(object sender, EventArgs e)
        {
            LimpaTela();
        }

        private void chOutros_CheckedChanged(object sender, EventArgs e)
        {
            LimpaTela();
        }

        private void RadColunas_SelectedIndexChanged(object sender, EventArgs e)
        {
            LimpaTela();
        }

        private void RadDet_SelectedIndexChanged(object sender, EventArgs e)
        {
            LimpaTela();
        }

        private void cCompetI_OnChange(object sender, EventArgs e)
        {
            LimpaTela();
        }

        private void cCompetF_OnChange(object sender, EventArgs e)
        {
            LimpaTela();
        }

        
       
    }
}
