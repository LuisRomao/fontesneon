namespace Balancete
{
    partial class cAcumulado
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(cAcumulado));
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject1 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject2 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject3 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject4 = new DevExpress.Utils.SerializableAppearanceObject();
            this.panelControl1 = new DevExpress.XtraEditors.PanelControl();
            this.cCompetF = new Framework.objetosNeon.cCompet();
            this.cCompetI = new Framework.objetosNeon.cCompet();
            this.chOutros = new DevExpress.XtraEditors.CheckEdit();
            this.chDebito = new DevExpress.XtraEditors.CheckEdit();
            this.chCredito = new DevExpress.XtraEditors.CheckEdit();
            this.simpleButton2 = new DevExpress.XtraEditors.SimpleButton();
            this.labelControl2 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl1 = new DevExpress.XtraEditors.LabelControl();
            this.RadDet = new DevExpress.XtraEditors.RadioGroup();
            this.RadColunas = new DevExpress.XtraEditors.RadioGroup();
            this.simpleButton1 = new DevExpress.XtraEditors.SimpleButton();
            this.cBotaoImpBol1 = new dllImpresso.Botoes.cBotaoImpBol();
            this.lookupCondominio_F1 = new Framework.Lookup.LookupCondominio_F();
            this.gridControl1 = new DevExpress.XtraGrid.GridControl();
            this.acumuladoBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.dAcumulado = new Balancete.dAcumulado();
            this.bandedGridView1 = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridView();
            this.gridBand1 = new DevExpress.XtraGrid.Views.BandedGrid.GridBand();
            this.colCredDeb = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.colPLA = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.colPLADescricao = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.colTitulo = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.gridBand2 = new DevExpress.XtraGrid.Views.BandedGrid.GridBand();
            this.colTotalAC = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.repositoryItemCalcEdit1 = new DevExpress.XtraEditors.Repository.RepositoryItemCalcEdit();
            this.colTotalA = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.colTotalM = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.colTotalD = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.colTotal = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.colMedia = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.colColunaImpresso = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            ((System.ComponentModel.ISupportInitialize)(this.StyleController_F)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl1)).BeginInit();
            this.panelControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.chOutros.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chDebito.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chCredito.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.RadDet.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.RadColunas.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.acumuladoBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dAcumulado)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.bandedGridView1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCalcEdit1)).BeginInit();
            this.SuspendLayout();
            // 
            // ToolTipController_F
            // 
            this.ToolTipController_F.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.ToolTipController_F.Appearance.ForeColor = System.Drawing.Color.Black;
            this.ToolTipController_F.Appearance.Options.UseBackColor = true;
            this.ToolTipController_F.Appearance.Options.UseFont = true;
            this.ToolTipController_F.Appearance.Options.UseForeColor = true;
            this.ToolTipController_F.Appearance.Options.UseTextOptions = true;
            this.ToolTipController_F.Appearance.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            // 
            // StyleController_F
            // 
            this.StyleController_F.AppearanceDisabled.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this.StyleController_F.AppearanceDisabled.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this.StyleController_F.AppearanceDisabled.Options.UseBackColor = true;
            this.StyleController_F.AppearanceDisabled.Options.UseForeColor = true;
            this.StyleController_F.AppearanceFocused.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.StyleController_F.AppearanceFocused.Options.UseBackColor = true;
            // 
            // panelControl1
            // 
            this.panelControl1.Controls.Add(this.cCompetF);
            this.panelControl1.Controls.Add(this.cCompetI);
            this.panelControl1.Controls.Add(this.chOutros);
            this.panelControl1.Controls.Add(this.chDebito);
            this.panelControl1.Controls.Add(this.chCredito);
            this.panelControl1.Controls.Add(this.simpleButton2);
            this.panelControl1.Controls.Add(this.labelControl2);
            this.panelControl1.Controls.Add(this.labelControl1);
            this.panelControl1.Controls.Add(this.RadDet);
            this.panelControl1.Controls.Add(this.RadColunas);
            this.panelControl1.Controls.Add(this.simpleButton1);
            this.panelControl1.Controls.Add(this.cBotaoImpBol1);
            this.panelControl1.Controls.Add(this.lookupCondominio_F1);
            this.panelControl1.Dock = System.Windows.Forms.DockStyle.Top;
            this.panelControl1.Location = new System.Drawing.Point(0, 0);
            this.panelControl1.Name = "panelControl1";
            this.panelControl1.Size = new System.Drawing.Size(1544, 143);
            this.panelControl1.TabIndex = 0;
            // 
            // cCompetF
            // 
            this.cCompetF.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.cCompetF.Appearance.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cCompetF.Appearance.Options.UseBackColor = true;
            this.cCompetF.Appearance.Options.UseFont = true;
            this.cCompetF.CaixaAltaGeral = true;
            this.cCompetF.Doc = System.Windows.Forms.DockStyle.None;
            this.cCompetF.Estado = CompontesBasicos.EstadosDosComponentes.Registrado;
            this.cCompetF.IsNull = false;
            this.cCompetF.Location = new System.Drawing.Point(633, 116);
            this.cCompetF.LookAndFeel.Style = DevExpress.LookAndFeel.LookAndFeelStyle.Office2003;
            this.cCompetF.LookAndFeel.UseDefaultLookAndFeel = false;
            this.cCompetF.Name = "cCompetF";
            this.cCompetF.PermiteNulo = true;
            this.cCompetF.ReadOnly = false;
            this.cCompetF.Size = new System.Drawing.Size(160, 27);
            this.cCompetF.somenteleitura = false;
            this.cCompetF.TabIndex = 57;
            this.cCompetF.Titulo = "Framework.objetosNeon.cCompet - ";
            this.cCompetF.OnChange += new System.EventHandler(this.cCompetF_OnChange);
            // 
            // cCompetI
            // 
            this.cCompetI.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.cCompetI.Appearance.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cCompetI.Appearance.Options.UseBackColor = true;
            this.cCompetI.Appearance.Options.UseFont = true;
            this.cCompetI.CaixaAltaGeral = true;
            this.cCompetI.Doc = System.Windows.Forms.DockStyle.None;
            this.cCompetI.Estado = CompontesBasicos.EstadosDosComponentes.Registrado;
            this.cCompetI.IsNull = false;
            this.cCompetI.Location = new System.Drawing.Point(633, 90);
            this.cCompetI.LookAndFeel.Style = DevExpress.LookAndFeel.LookAndFeelStyle.Office2003;
            this.cCompetI.LookAndFeel.UseDefaultLookAndFeel = false;
            this.cCompetI.Name = "cCompetI";
            this.cCompetI.PermiteNulo = true;
            this.cCompetI.ReadOnly = false;
            this.cCompetI.Size = new System.Drawing.Size(160, 27);
            this.cCompetI.somenteleitura = false;
            this.cCompetI.TabIndex = 56;
            this.cCompetI.Titulo = "Framework.objetosNeon.cCompet - ";
            this.cCompetI.OnChange += new System.EventHandler(this.cCompetI_OnChange);
            // 
            // chOutros
            // 
            this.chOutros.EditValue = true;
            this.chOutros.Location = new System.Drawing.Point(487, 102);
            this.chOutros.Name = "chOutros";
            this.chOutros.Properties.Caption = "Outros";
            this.chOutros.Size = new System.Drawing.Size(75, 19);
            this.chOutros.TabIndex = 23;
            this.chOutros.CheckedChanged += new System.EventHandler(this.chOutros_CheckedChanged);
            // 
            // chDebito
            // 
            this.chDebito.EditValue = true;
            this.chDebito.Location = new System.Drawing.Point(487, 77);
            this.chDebito.Name = "chDebito";
            this.chDebito.Properties.Caption = "D�bito";
            this.chDebito.Size = new System.Drawing.Size(75, 19);
            this.chDebito.TabIndex = 22;
            this.chDebito.CheckedChanged += new System.EventHandler(this.checkEdit2_CheckedChanged);
            // 
            // chCredito
            // 
            this.chCredito.EditValue = true;
            this.chCredito.Location = new System.Drawing.Point(487, 52);
            this.chCredito.Name = "chCredito";
            this.chCredito.Properties.Caption = "Cr�dito";
            this.chCredito.Size = new System.Drawing.Size(75, 19);
            this.chCredito.TabIndex = 21;
            this.chCredito.CheckedChanged += new System.EventHandler(this.chCredito_CheckedChanged);
            // 
            // simpleButton2
            // 
            this.simpleButton2.Image = ((System.Drawing.Image)(resources.GetObject("simpleButton2.Image")));
            this.simpleButton2.Location = new System.Drawing.Point(714, 61);
            this.simpleButton2.Name = "simpleButton2";
            this.simpleButton2.Size = new System.Drawing.Size(79, 23);
            this.simpleButton2.TabIndex = 20;
            this.simpleButton2.Text = "Planilha 2";
            this.simpleButton2.Click += new System.EventHandler(this.simpleButton2_Click);
            // 
            // labelControl2
            // 
            this.labelControl2.Location = new System.Drawing.Point(259, 52);
            this.labelControl2.Name = "labelControl2";
            this.labelControl2.Size = new System.Drawing.Size(71, 13);
            this.labelControl2.TabIndex = 19;
            this.labelControl2.Text = "Detalhamento:";
            // 
            // labelControl1
            // 
            this.labelControl1.Location = new System.Drawing.Point(7, 52);
            this.labelControl1.Name = "labelControl1";
            this.labelControl1.Size = new System.Drawing.Size(42, 13);
            this.labelControl1.TabIndex = 18;
            this.labelControl1.Text = "Colunas:";
            // 
            // RadDet
            // 
            this.RadDet.EditValue = "P";
            this.RadDet.Location = new System.Drawing.Point(336, 52);
            this.RadDet.Name = "RadDet";
            this.RadDet.Properties.Items.AddRange(new DevExpress.XtraEditors.Controls.RadioGroupItem[] {
            new DevExpress.XtraEditors.Controls.RadioGroupItem("P", "Plano de contas"),
            new DevExpress.XtraEditors.Controls.RadioGroupItem("Det", "Detalhado"),
            new DevExpress.XtraEditors.Controls.RadioGroupItem("SuperDet", "Sem Agrupamento")});
            this.RadDet.Size = new System.Drawing.Size(133, 84);
            this.RadDet.TabIndex = 17;
            this.RadDet.SelectedIndexChanged += new System.EventHandler(this.RadDet_SelectedIndexChanged);
            // 
            // RadColunas
            // 
            this.RadColunas.EditValue = "M";
            this.RadColunas.Location = new System.Drawing.Point(55, 52);
            this.RadColunas.Name = "RadColunas";
            this.RadColunas.Properties.Items.AddRange(new DevExpress.XtraEditors.Controls.RadioGroupItem[] {
            new DevExpress.XtraEditors.Controls.RadioGroupItem("M", "Total do m�s"),
            new DevExpress.XtraEditors.Controls.RadioGroupItem("AMD", "Atrasado/M�s/Adiantado"),
            new DevExpress.XtraEditors.Controls.RadioGroupItem("AcAMD", "Acordo/Atrasado/M�s/Adiantado"),
            new DevExpress.XtraEditors.Controls.RadioGroupItem("Ac", "Somente acordo")});
            this.RadColunas.Size = new System.Drawing.Size(198, 84);
            this.RadColunas.TabIndex = 16;
            this.RadColunas.SelectedIndexChanged += new System.EventHandler(this.RadColunas_SelectedIndexChanged);
            // 
            // simpleButton1
            // 
            this.simpleButton1.Image = ((System.Drawing.Image)(resources.GetObject("simpleButton1.Image")));
            this.simpleButton1.Location = new System.Drawing.Point(633, 61);
            this.simpleButton1.Name = "simpleButton1";
            this.simpleButton1.Size = new System.Drawing.Size(75, 23);
            this.simpleButton1.TabIndex = 13;
            this.simpleButton1.Text = "Planilha 1";
            this.simpleButton1.Click += new System.EventHandler(this.simpleButton1_Click);
            // 
            // cBotaoImpBol1
            // 
            this.cBotaoImpBol1.Enabled = false;
            this.cBotaoImpBol1.ItemComprovante = false;
            this.cBotaoImpBol1.Location = new System.Drawing.Point(633, 3);
            this.cBotaoImpBol1.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.cBotaoImpBol1.Name = "cBotaoImpBol1";
            this.cBotaoImpBol1.Size = new System.Drawing.Size(160, 52);
            this.cBotaoImpBol1.TabIndex = 4;
            this.cBotaoImpBol1.Titulo = "Calcular";
            this.cBotaoImpBol1.clicado += new System.EventHandler(this.cBotaoImpBol1_clicado);
            // 
            // lookupCondominio_F1
            // 
            this.lookupCondominio_F1.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.lookupCondominio_F1.Appearance.Options.UseBackColor = true;
            this.lookupCondominio_F1.Autofill = true;
            this.lookupCondominio_F1.CaixaAltaGeral = true;
            this.lookupCondominio_F1.CON_sel = -1;
            this.lookupCondominio_F1.CON_selrow = null;
            this.lookupCondominio_F1.Cursor = System.Windows.Forms.Cursors.Default;
            this.lookupCondominio_F1.Doc = System.Windows.Forms.DockStyle.None;
            this.lookupCondominio_F1.Estado = CompontesBasicos.EstadosDosComponentes.Registrado;
            this.lookupCondominio_F1.LinhaMae_F = null;
            this.lookupCondominio_F1.Location = new System.Drawing.Point(5, 14);
            this.lookupCondominio_F1.LookAndFeel.Style = DevExpress.LookAndFeel.LookAndFeelStyle.Flat;
            this.lookupCondominio_F1.LookAndFeel.UseDefaultLookAndFeel = false;
            this.lookupCondominio_F1.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.lookupCondominio_F1.Name = "lookupCondominio_F1";
            this.lookupCondominio_F1.NivelCONOculto = 2;
            this.lookupCondominio_F1.Size = new System.Drawing.Size(622, 20);
            this.lookupCondominio_F1.somenteleitura = false;
            this.lookupCondominio_F1.TabIndex = 1;
            this.lookupCondominio_F1.TableAdapterPrincipal = null;
            this.lookupCondominio_F1.Titulo = "Framework.Lookup.LookupCondominio_F - ";
            this.lookupCondominio_F1.alterado += new System.EventHandler(this.lookupCondominio_F1_alterado);
            // 
            // gridControl1
            // 
            this.gridControl1.DataSource = this.acumuladoBindingSource;
            this.gridControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridControl1.Location = new System.Drawing.Point(0, 143);
            this.gridControl1.MainView = this.bandedGridView1;
            this.gridControl1.Name = "gridControl1";
            this.gridControl1.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemCalcEdit1});
            this.gridControl1.Size = new System.Drawing.Size(1544, 656);
            this.gridControl1.TabIndex = 1;
            this.gridControl1.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.bandedGridView1});
            // 
            // acumuladoBindingSource
            // 
            this.acumuladoBindingSource.DataMember = "Acumulado";
            this.acumuladoBindingSource.DataSource = this.dAcumulado;
            // 
            // dAcumulado
            // 
            this.dAcumulado.DataSetName = "dAcumulado";
            this.dAcumulado.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // bandedGridView1
            // 
            this.bandedGridView1.Appearance.BandPanel.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(104)))), ((int)(((byte)(184)))), ((int)(((byte)(251)))));
            this.bandedGridView1.Appearance.BandPanel.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(104)))), ((int)(((byte)(184)))), ((int)(((byte)(251)))));
            this.bandedGridView1.Appearance.BandPanel.ForeColor = System.Drawing.Color.Black;
            this.bandedGridView1.Appearance.BandPanel.GradientMode = System.Drawing.Drawing2D.LinearGradientMode.ForwardDiagonal;
            this.bandedGridView1.Appearance.BandPanel.Options.UseBackColor = true;
            this.bandedGridView1.Appearance.BandPanel.Options.UseBorderColor = true;
            this.bandedGridView1.Appearance.BandPanel.Options.UseForeColor = true;
            this.bandedGridView1.Appearance.BandPanel.Options.UseTextOptions = true;
            this.bandedGridView1.Appearance.BandPanel.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.bandedGridView1.Appearance.BandPanelBackground.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(236)))), ((int)(((byte)(246)))), ((int)(((byte)(255)))));
            this.bandedGridView1.Appearance.BandPanelBackground.BackColor2 = System.Drawing.Color.White;
            this.bandedGridView1.Appearance.BandPanelBackground.Options.UseBackColor = true;
            this.bandedGridView1.Appearance.ColumnFilterButton.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(104)))), ((int)(((byte)(184)))), ((int)(((byte)(251)))));
            this.bandedGridView1.Appearance.ColumnFilterButton.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(104)))), ((int)(((byte)(184)))), ((int)(((byte)(251)))));
            this.bandedGridView1.Appearance.ColumnFilterButton.ForeColor = System.Drawing.Color.White;
            this.bandedGridView1.Appearance.ColumnFilterButton.Options.UseBackColor = true;
            this.bandedGridView1.Appearance.ColumnFilterButton.Options.UseBorderColor = true;
            this.bandedGridView1.Appearance.ColumnFilterButton.Options.UseForeColor = true;
            this.bandedGridView1.Appearance.ColumnFilterButtonActive.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(170)))), ((int)(((byte)(216)))), ((int)(((byte)(254)))));
            this.bandedGridView1.Appearance.ColumnFilterButtonActive.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(170)))), ((int)(((byte)(216)))), ((int)(((byte)(254)))));
            this.bandedGridView1.Appearance.ColumnFilterButtonActive.ForeColor = System.Drawing.Color.Black;
            this.bandedGridView1.Appearance.ColumnFilterButtonActive.Options.UseBackColor = true;
            this.bandedGridView1.Appearance.ColumnFilterButtonActive.Options.UseBorderColor = true;
            this.bandedGridView1.Appearance.ColumnFilterButtonActive.Options.UseForeColor = true;
            this.bandedGridView1.Appearance.Empty.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(236)))), ((int)(((byte)(246)))), ((int)(((byte)(255)))));
            this.bandedGridView1.Appearance.Empty.BackColor2 = System.Drawing.Color.White;
            this.bandedGridView1.Appearance.Empty.Options.UseBackColor = true;
            this.bandedGridView1.Appearance.EvenRow.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(247)))), ((int)(((byte)(251)))), ((int)(((byte)(255)))));
            this.bandedGridView1.Appearance.EvenRow.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(247)))), ((int)(((byte)(251)))), ((int)(((byte)(255)))));
            this.bandedGridView1.Appearance.EvenRow.ForeColor = System.Drawing.Color.Black;
            this.bandedGridView1.Appearance.EvenRow.Options.UseBackColor = true;
            this.bandedGridView1.Appearance.EvenRow.Options.UseBorderColor = true;
            this.bandedGridView1.Appearance.EvenRow.Options.UseForeColor = true;
            this.bandedGridView1.Appearance.FilterCloseButton.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(104)))), ((int)(((byte)(184)))), ((int)(((byte)(251)))));
            this.bandedGridView1.Appearance.FilterCloseButton.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(104)))), ((int)(((byte)(184)))), ((int)(((byte)(251)))));
            this.bandedGridView1.Appearance.FilterCloseButton.ForeColor = System.Drawing.Color.White;
            this.bandedGridView1.Appearance.FilterCloseButton.Options.UseBackColor = true;
            this.bandedGridView1.Appearance.FilterCloseButton.Options.UseBorderColor = true;
            this.bandedGridView1.Appearance.FilterCloseButton.Options.UseForeColor = true;
            this.bandedGridView1.Appearance.FilterPanel.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(236)))), ((int)(((byte)(246)))), ((int)(((byte)(255)))));
            this.bandedGridView1.Appearance.FilterPanel.BackColor2 = System.Drawing.Color.White;
            this.bandedGridView1.Appearance.FilterPanel.ForeColor = System.Drawing.Color.Black;
            this.bandedGridView1.Appearance.FilterPanel.Options.UseBackColor = true;
            this.bandedGridView1.Appearance.FilterPanel.Options.UseForeColor = true;
            this.bandedGridView1.Appearance.FixedLine.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(59)))), ((int)(((byte)(133)))), ((int)(((byte)(195)))));
            this.bandedGridView1.Appearance.FixedLine.Options.UseBackColor = true;
            this.bandedGridView1.Appearance.FocusedCell.BackColor = System.Drawing.Color.White;
            this.bandedGridView1.Appearance.FocusedCell.ForeColor = System.Drawing.Color.Black;
            this.bandedGridView1.Appearance.FocusedCell.Options.UseBackColor = true;
            this.bandedGridView1.Appearance.FocusedCell.Options.UseForeColor = true;
            this.bandedGridView1.Appearance.FocusedRow.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(38)))), ((int)(((byte)(109)))), ((int)(((byte)(189)))));
            this.bandedGridView1.Appearance.FocusedRow.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(59)))), ((int)(((byte)(139)))), ((int)(((byte)(206)))));
            this.bandedGridView1.Appearance.FocusedRow.ForeColor = System.Drawing.Color.White;
            this.bandedGridView1.Appearance.FocusedRow.Options.UseBackColor = true;
            this.bandedGridView1.Appearance.FocusedRow.Options.UseBorderColor = true;
            this.bandedGridView1.Appearance.FocusedRow.Options.UseForeColor = true;
            this.bandedGridView1.Appearance.FooterPanel.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(104)))), ((int)(((byte)(184)))), ((int)(((byte)(251)))));
            this.bandedGridView1.Appearance.FooterPanel.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(104)))), ((int)(((byte)(184)))), ((int)(((byte)(251)))));
            this.bandedGridView1.Appearance.FooterPanel.ForeColor = System.Drawing.Color.Black;
            this.bandedGridView1.Appearance.FooterPanel.Options.UseBackColor = true;
            this.bandedGridView1.Appearance.FooterPanel.Options.UseBorderColor = true;
            this.bandedGridView1.Appearance.FooterPanel.Options.UseForeColor = true;
            this.bandedGridView1.Appearance.GroupButton.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(104)))), ((int)(((byte)(184)))), ((int)(((byte)(251)))));
            this.bandedGridView1.Appearance.GroupButton.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(104)))), ((int)(((byte)(184)))), ((int)(((byte)(251)))));
            this.bandedGridView1.Appearance.GroupButton.Options.UseBackColor = true;
            this.bandedGridView1.Appearance.GroupButton.Options.UseBorderColor = true;
            this.bandedGridView1.Appearance.GroupFooter.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(170)))), ((int)(((byte)(216)))), ((int)(((byte)(254)))));
            this.bandedGridView1.Appearance.GroupFooter.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(170)))), ((int)(((byte)(216)))), ((int)(((byte)(254)))));
            this.bandedGridView1.Appearance.GroupFooter.ForeColor = System.Drawing.Color.Black;
            this.bandedGridView1.Appearance.GroupFooter.Options.UseBackColor = true;
            this.bandedGridView1.Appearance.GroupFooter.Options.UseBorderColor = true;
            this.bandedGridView1.Appearance.GroupFooter.Options.UseForeColor = true;
            this.bandedGridView1.Appearance.GroupPanel.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(236)))), ((int)(((byte)(246)))), ((int)(((byte)(255)))));
            this.bandedGridView1.Appearance.GroupPanel.BackColor2 = System.Drawing.Color.White;
            this.bandedGridView1.Appearance.GroupPanel.ForeColor = System.Drawing.Color.Black;
            this.bandedGridView1.Appearance.GroupPanel.Options.UseBackColor = true;
            this.bandedGridView1.Appearance.GroupPanel.Options.UseForeColor = true;
            this.bandedGridView1.Appearance.GroupRow.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(170)))), ((int)(((byte)(216)))), ((int)(((byte)(254)))));
            this.bandedGridView1.Appearance.GroupRow.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(170)))), ((int)(((byte)(216)))), ((int)(((byte)(254)))));
            this.bandedGridView1.Appearance.GroupRow.ForeColor = System.Drawing.Color.Black;
            this.bandedGridView1.Appearance.GroupRow.Options.UseBackColor = true;
            this.bandedGridView1.Appearance.GroupRow.Options.UseBorderColor = true;
            this.bandedGridView1.Appearance.GroupRow.Options.UseForeColor = true;
            this.bandedGridView1.Appearance.HeaderPanel.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(139)))), ((int)(((byte)(201)))), ((int)(((byte)(254)))));
            this.bandedGridView1.Appearance.HeaderPanel.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(139)))), ((int)(((byte)(201)))), ((int)(((byte)(254)))));
            this.bandedGridView1.Appearance.HeaderPanel.ForeColor = System.Drawing.Color.Black;
            this.bandedGridView1.Appearance.HeaderPanel.Options.UseBackColor = true;
            this.bandedGridView1.Appearance.HeaderPanel.Options.UseBorderColor = true;
            this.bandedGridView1.Appearance.HeaderPanel.Options.UseForeColor = true;
            this.bandedGridView1.Appearance.HeaderPanelBackground.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(236)))), ((int)(((byte)(246)))), ((int)(((byte)(255)))));
            this.bandedGridView1.Appearance.HeaderPanelBackground.BackColor2 = System.Drawing.Color.White;
            this.bandedGridView1.Appearance.HeaderPanelBackground.Options.UseBackColor = true;
            this.bandedGridView1.Appearance.HideSelectionRow.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(105)))), ((int)(((byte)(170)))), ((int)(((byte)(225)))));
            this.bandedGridView1.Appearance.HideSelectionRow.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(83)))), ((int)(((byte)(155)))), ((int)(((byte)(215)))));
            this.bandedGridView1.Appearance.HideSelectionRow.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(236)))), ((int)(((byte)(246)))), ((int)(((byte)(255)))));
            this.bandedGridView1.Appearance.HideSelectionRow.Options.UseBackColor = true;
            this.bandedGridView1.Appearance.HideSelectionRow.Options.UseBorderColor = true;
            this.bandedGridView1.Appearance.HideSelectionRow.Options.UseForeColor = true;
            this.bandedGridView1.Appearance.HorzLine.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(104)))), ((int)(((byte)(184)))), ((int)(((byte)(251)))));
            this.bandedGridView1.Appearance.HorzLine.Options.UseBackColor = true;
            this.bandedGridView1.Appearance.OddRow.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(236)))), ((int)(((byte)(246)))), ((int)(((byte)(255)))));
            this.bandedGridView1.Appearance.OddRow.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(236)))), ((int)(((byte)(246)))), ((int)(((byte)(255)))));
            this.bandedGridView1.Appearance.OddRow.ForeColor = System.Drawing.Color.Black;
            this.bandedGridView1.Appearance.OddRow.Options.UseBackColor = true;
            this.bandedGridView1.Appearance.OddRow.Options.UseBorderColor = true;
            this.bandedGridView1.Appearance.OddRow.Options.UseForeColor = true;
            this.bandedGridView1.Appearance.Preview.Font = new System.Drawing.Font("Verdana", 7.5F);
            this.bandedGridView1.Appearance.Preview.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(83)))), ((int)(((byte)(155)))), ((int)(((byte)(215)))));
            this.bandedGridView1.Appearance.Preview.Options.UseFont = true;
            this.bandedGridView1.Appearance.Preview.Options.UseForeColor = true;
            this.bandedGridView1.Appearance.Row.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(247)))), ((int)(((byte)(251)))), ((int)(((byte)(255)))));
            this.bandedGridView1.Appearance.Row.ForeColor = System.Drawing.Color.Black;
            this.bandedGridView1.Appearance.Row.Options.UseBackColor = true;
            this.bandedGridView1.Appearance.Row.Options.UseForeColor = true;
            this.bandedGridView1.Appearance.RowSeparator.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(236)))), ((int)(((byte)(246)))), ((int)(((byte)(255)))));
            this.bandedGridView1.Appearance.RowSeparator.BackColor2 = System.Drawing.Color.White;
            this.bandedGridView1.Appearance.RowSeparator.Options.UseBackColor = true;
            this.bandedGridView1.Appearance.SelectedRow.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(83)))), ((int)(((byte)(155)))), ((int)(((byte)(215)))));
            this.bandedGridView1.Appearance.SelectedRow.ForeColor = System.Drawing.Color.White;
            this.bandedGridView1.Appearance.SelectedRow.Options.UseBackColor = true;
            this.bandedGridView1.Appearance.SelectedRow.Options.UseForeColor = true;
            this.bandedGridView1.Appearance.TopNewRow.BackColor = System.Drawing.Color.White;
            this.bandedGridView1.Appearance.TopNewRow.Options.UseBackColor = true;
            this.bandedGridView1.Appearance.VertLine.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(104)))), ((int)(((byte)(184)))), ((int)(((byte)(251)))));
            this.bandedGridView1.Appearance.VertLine.Options.UseBackColor = true;
            this.bandedGridView1.Bands.AddRange(new DevExpress.XtraGrid.Views.BandedGrid.GridBand[] {
            this.gridBand1,
            this.gridBand2});
            this.bandedGridView1.Columns.AddRange(new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn[] {
            this.colPLA,
            this.colTitulo,
            this.colTotalA,
            this.colTotalM,
            this.colTotalD,
            this.colTotal,
            this.colTotalAC,
            this.colCredDeb,
            this.colColunaImpresso,
            this.colPLADescricao,
            this.colMedia});
            this.bandedGridView1.GridControl = this.gridControl1;
            this.bandedGridView1.GroupCount = 1;
            this.bandedGridView1.GroupFormat = "[#image]{1} {2}";
            this.bandedGridView1.GroupSummary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridGroupSummaryItem(DevExpress.Data.SummaryItemType.Sum, "TotalA", this.colTotalA, "{0:n2}"),
            new DevExpress.XtraGrid.GridGroupSummaryItem(DevExpress.Data.SummaryItemType.Sum, "TotalM", this.colTotalM, "{0:n2}"),
            new DevExpress.XtraGrid.GridGroupSummaryItem(DevExpress.Data.SummaryItemType.Sum, "TotalD", this.colTotalD, "{0:n2}"),
            new DevExpress.XtraGrid.GridGroupSummaryItem(DevExpress.Data.SummaryItemType.Sum, "TotalAC", this.colTotalAC, "{0:n2}"),
            new DevExpress.XtraGrid.GridGroupSummaryItem(DevExpress.Data.SummaryItemType.Sum, "Total", this.colTotal, "{0:n2}")});
            this.bandedGridView1.Name = "bandedGridView1";
            this.bandedGridView1.OptionsBehavior.AutoExpandAllGroups = true;
            this.bandedGridView1.OptionsSelection.MultiSelect = true;
            this.bandedGridView1.OptionsView.ColumnAutoWidth = false;
            this.bandedGridView1.OptionsView.EnableAppearanceEvenRow = true;
            this.bandedGridView1.OptionsView.EnableAppearanceOddRow = true;
            this.bandedGridView1.OptionsView.GroupFooterShowMode = DevExpress.XtraGrid.Views.Grid.GroupFooterShowMode.VisibleAlways;
            this.bandedGridView1.OptionsView.ShowAutoFilterRow = true;
            this.bandedGridView1.OptionsView.ShowGroupPanel = false;
            this.bandedGridView1.SortInfo.AddRange(new DevExpress.XtraGrid.Columns.GridColumnSortInfo[] {
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colCredDeb, DevExpress.Data.ColumnSortOrder.Ascending),
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colPLA, DevExpress.Data.ColumnSortOrder.Ascending)});
            // 
            // gridBand1
            // 
            this.gridBand1.Caption = "Receita";
            this.gridBand1.Columns.Add(this.colCredDeb);
            this.gridBand1.Columns.Add(this.colPLA);
            this.gridBand1.Columns.Add(this.colPLADescricao);
            this.gridBand1.Columns.Add(this.colTitulo);
            this.gridBand1.Fixed = DevExpress.XtraGrid.Columns.FixedStyle.Left;
            this.gridBand1.MinWidth = 20;
            this.gridBand1.Name = "gridBand1";
            this.gridBand1.VisibleIndex = 0;
            this.gridBand1.Width = 299;
            // 
            // colCredDeb
            // 
            this.colCredDeb.Caption = "D/C";
            this.colCredDeb.FieldName = "CredDeb";
            this.colCredDeb.GroupInterval = DevExpress.XtraGrid.ColumnGroupInterval.Value;
            this.colCredDeb.Name = "colCredDeb";
            this.colCredDeb.OptionsColumn.ReadOnly = true;
            // 
            // colPLA
            // 
            this.colPLA.Caption = "Plano";
            this.colPLA.FieldName = "PLA";
            this.colPLA.Name = "colPLA";
            this.colPLA.OptionsColumn.ReadOnly = true;
            this.colPLA.Visible = true;
            this.colPLA.Width = 70;
            // 
            // colPLADescricao
            // 
            this.colPLADescricao.Caption = "Conta";
            this.colPLADescricao.FieldName = "PLADescricao";
            this.colPLADescricao.Name = "colPLADescricao";
            // 
            // colTitulo
            // 
            this.colTitulo.Caption = "T�tulo";
            this.colTitulo.FieldName = "Titulo";
            this.colTitulo.Name = "colTitulo";
            this.colTitulo.OptionsColumn.ReadOnly = true;
            this.colTitulo.Visible = true;
            this.colTitulo.Width = 229;
            // 
            // gridBand2
            // 
            this.gridBand2.Caption = "Totais";
            this.gridBand2.Columns.Add(this.colTotalAC);
            this.gridBand2.Columns.Add(this.colTotalA);
            this.gridBand2.Columns.Add(this.colTotalM);
            this.gridBand2.Columns.Add(this.colTotalD);
            this.gridBand2.Columns.Add(this.colTotal);
            this.gridBand2.Columns.Add(this.colMedia);
            this.gridBand2.Fixed = DevExpress.XtraGrid.Columns.FixedStyle.Right;
            this.gridBand2.MinWidth = 20;
            this.gridBand2.Name = "gridBand2";
            this.gridBand2.VisibleIndex = 1;
            this.gridBand2.Width = 400;
            // 
            // colTotalAC
            // 
            this.colTotalAC.Caption = "Acordo";
            this.colTotalAC.ColumnEdit = this.repositoryItemCalcEdit1;
            this.colTotalAC.FieldName = "TotalAC";
            this.colTotalAC.Name = "colTotalAC";
            this.colTotalAC.Visible = true;
            this.colTotalAC.Width = 80;
            // 
            // repositoryItemCalcEdit1
            // 
            this.repositoryItemCalcEdit1.AutoHeight = false;
            this.repositoryItemCalcEdit1.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo, "", -1, true, false, false, DevExpress.XtraEditors.ImageLocation.MiddleCenter, null, new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), serializableAppearanceObject1, serializableAppearanceObject2, serializableAppearanceObject3, serializableAppearanceObject4, "", null, null, true)});
            this.repositoryItemCalcEdit1.DisplayFormat.FormatString = "n2";
            this.repositoryItemCalcEdit1.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.repositoryItemCalcEdit1.Mask.EditMask = "n2";
            this.repositoryItemCalcEdit1.Mask.UseMaskAsDisplayFormat = true;
            this.repositoryItemCalcEdit1.Name = "repositoryItemCalcEdit1";
            this.repositoryItemCalcEdit1.ReadOnly = true;
            // 
            // colTotalA
            // 
            this.colTotalA.Caption = "Atrasado";
            this.colTotalA.ColumnEdit = this.repositoryItemCalcEdit1;
            this.colTotalA.DisplayFormat.FormatString = "n2";
            this.colTotalA.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.colTotalA.FieldName = "TotalA";
            this.colTotalA.Name = "colTotalA";
            this.colTotalA.OptionsColumn.ReadOnly = true;
            this.colTotalA.Visible = true;
            this.colTotalA.Width = 80;
            // 
            // colTotalM
            // 
            this.colTotalM.Caption = "no m�s";
            this.colTotalM.ColumnEdit = this.repositoryItemCalcEdit1;
            this.colTotalM.DisplayFormat.FormatString = "n2";
            this.colTotalM.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.colTotalM.FieldName = "TotalM";
            this.colTotalM.Name = "colTotalM";
            this.colTotalM.OptionsColumn.ReadOnly = true;
            this.colTotalM.Visible = true;
            this.colTotalM.Width = 80;
            // 
            // colTotalD
            // 
            this.colTotalD.Caption = "adiantado";
            this.colTotalD.ColumnEdit = this.repositoryItemCalcEdit1;
            this.colTotalD.DisplayFormat.FormatString = "n2";
            this.colTotalD.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.colTotalD.FieldName = "TotalD";
            this.colTotalD.Name = "colTotalD";
            this.colTotalD.OptionsColumn.ReadOnly = true;
            this.colTotalD.Visible = true;
            this.colTotalD.Width = 80;
            // 
            // colTotal
            // 
            this.colTotal.AppearanceCell.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
            this.colTotal.AppearanceCell.Options.UseFont = true;
            this.colTotal.ColumnEdit = this.repositoryItemCalcEdit1;
            this.colTotal.FieldName = "Total";
            this.colTotal.Name = "colTotal";
            this.colTotal.OptionsColumn.ReadOnly = true;
            this.colTotal.Visible = true;
            this.colTotal.Width = 80;
            // 
            // colMedia
            // 
            this.colMedia.Caption = "M�dia";
            this.colMedia.ColumnEdit = this.repositoryItemCalcEdit1;
            this.colMedia.FieldName = "Media";
            this.colMedia.Name = "colMedia";
            // 
            // colColunaImpresso
            // 
            this.colColunaImpresso.FieldName = "ColunaImpresso";
            this.colColunaImpresso.Name = "colColunaImpresso";
            this.colColunaImpresso.Visible = true;
            // 
            // cAcumulado
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.gridControl1);
            this.Controls.Add(this.panelControl1);
            this.Doc = System.Windows.Forms.DockStyle.Fill;
            this.LookAndFeel.UseDefaultLookAndFeel = false;
            this.Name = "cAcumulado";
            this.Size = new System.Drawing.Size(1544, 799);
            ((System.ComponentModel.ISupportInitialize)(this.StyleController_F)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl1)).EndInit();
            this.panelControl1.ResumeLayout(false);
            this.panelControl1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.chOutros.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chDebito.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chCredito.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.RadDet.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.RadColunas.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.acumuladoBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dAcumulado)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.bandedGridView1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCalcEdit1)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraEditors.PanelControl panelControl1;
        private DevExpress.XtraGrid.GridControl gridControl1;
        private Framework.Lookup.LookupCondominio_F lookupCondominio_F1;
        private dllImpresso.Botoes.cBotaoImpBol cBotaoImpBol1;
        private System.Windows.Forms.BindingSource acumuladoBindingSource;
        private dAcumulado dAcumulado;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridView bandedGridView1;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn colPLA;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn colTitulo;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn colTotalA;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn colTotalM;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn colTotalD;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn colTotal;
        private DevExpress.XtraEditors.Repository.RepositoryItemCalcEdit repositoryItemCalcEdit1;
        private DevExpress.XtraEditors.SimpleButton simpleButton1;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn colTotalAC;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn colCredDeb;
        private DevExpress.XtraEditors.RadioGroup RadColunas;
        private DevExpress.XtraEditors.LabelControl labelControl2;
        private DevExpress.XtraEditors.LabelControl labelControl1;
        private DevExpress.XtraEditors.RadioGroup RadDet;
        private DevExpress.XtraEditors.SimpleButton simpleButton2;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn colColunaImpresso;
        private DevExpress.XtraEditors.CheckEdit chOutros;
        private DevExpress.XtraEditors.CheckEdit chDebito;
        private DevExpress.XtraEditors.CheckEdit chCredito;
        private Framework.objetosNeon.cCompet cCompetF;
        private Framework.objetosNeon.cCompet cCompetI;
        private DevExpress.XtraGrid.Views.BandedGrid.GridBand gridBand1;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn colPLADescricao;
        private DevExpress.XtraGrid.Views.BandedGrid.GridBand gridBand2;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn colMedia;
    }
}
