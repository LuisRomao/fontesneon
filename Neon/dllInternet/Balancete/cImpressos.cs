using System;
using System.Data;
using System.Drawing;
using System.Windows.Forms;
using Framework.objetosNeon;
using System.Collections;
using Balancete.GrBalancete;
using DevExpress.XtraReports.UI;
using VirEnumeracoesNeon;
using VirMSSQL;
using Framework;
using CompontesBasicosProc;

namespace Balancete
{
    /// <summary>
    /// Impressos do balancete
    /// </summary>
    public partial class cImpressos : CompontesBasicos.ComponenteBase
    {
        /// <summary>
        /// Construtor
        /// </summary>
        public cImpressos()
        {
            InitializeComponent();
            Enumeracoes.virEnumBALStatus.virEnumBALStatusSt.CarregaEditorDaGrid(colBALSTATUS);
            gridColumn1.Visible = (CompontesBasicos.Bancovirtual.UsuarioLogado.UsuarioLogadoSt.VerificaFuncionalidade("BALANCETE") > 1);
            dBalancetes.SaldoContaLogicaTableAdapter.Fill(dBalancetes.SaldoContaLogica);
        }

        /// <summary>
        /// Recalcula os dados
        /// </summary>
        public override void RefreshDados()
        {
            base.RefreshDados();
            dBalancetes.SaldoContaLogicaTableAdapter.Fill(dBalancetes.SaldoContaLogica);
        }

        bool evitaReentrada;
        //int UltimoCarregado = -1;

        private ArrayList _StatusFechados;

        private ArrayList StatusFechados
        {
            get
            {
                if (_StatusFechados == null)
                {
                    _StatusFechados = new ArrayList(new BALStatus[] { BALStatus.FechadoDefinitivo, BALStatus.ParaPublicar, BALStatus.Publicado,BALStatus.Enviado });
                }
                return _StatusFechados;
            }
        }

        private void Limpa()
        {
            dataI.EditValue = null;
            dataF.EditValue = null;
            dBalancetes.BALancetes.Clear();
            memoEdit1.Text = "";
            simpleButton1.Enabled = simpleButton2.Enabled = cBotaoImpBol5.Enabled = cBotaoImpBol1.Enabled = cBotaoFrancesinha.Enabled = cBotaoImpBol4.Enabled = false;
            cBotaoImpInad.Enabled = cBotaoImpBol2.Enabled = false;
        }

        private void simpleButton4_Click(object sender, EventArgs e)
        {
            Limpa();                
            if (lookupCondominio_F1.CON_sel != -1)            
            {
                //if ((!evitaReentrada) && (UltimoCarregado != lookupCondominio_F1.CON_sel))
                if (!evitaReentrada)
                {
                    try
                    {
                        evitaReentrada = true;
                        botPrimeiro.Visible = false;
                        //UltimoCarregado = lookupCondominio_F1.CON_sel;
                        int lidos = dBalancetes.BALancetesTableAdapter.Fill(dBalancetes.BALancetes, lookupCondominio_F1.CON_sel);
                        string Maximo = "";
                        string Comando = "select max(mid(HL.[M�s Compet�ncia],3,4) + mid(HL.[M�s Compet�ncia],1,2)) as maxcomp from [Hist�rico Lan�amentos] HL where codcon = @P1";
                        string ComandoDatas = "SELECT Codcon, [Data Inicial], [Data Final] FROM [Saldos e Poupan�a] WHERE (Codcon = @P1)";
                        int CompetenciaFechadaDef = 0;
                        bool? Novo = TableAdapter.ST().BuscaEscalar_bool("select CONDivideSaldos from CONDOMINIOS where CON = @P1", lookupCondominio_F1.CON_sel);
                        if (!Novo.GetValueOrDefault(false))
                        {
                            if (VirOleDb.TableAdapter.STTableAdapterTipo(VirDB.Bancovirtual.TiposDeBanco.AccessH).BuscaEscalar(Comando, out Maximo, lookupCondominio_F1.CON_selrow.CONCodigo))
                            {
                                if (int.TryParse(Maximo, out CompetenciaFechadaDef))
                                    foreach (dBalancetes.BALancetesRow rowBAL in dBalancetes.BALancetes)
                                        if (rowBAL.BALCompet == CompetenciaFechadaDef)
                                        {
                                            if (!((BALStatus)rowBAL.BALSTATUS).EstaNoGrupo(BALStatus.FechadoDefinitivo, BALStatus.Enviado))
                                            {
                                                cAcumulado cAcumulado1 = new cAcumulado();
                                                if (cAcumulado1.publicah12(lookupCondominio_F1.CON_sel))
                                                {
                                                    rowBAL.BALSTATUS = (int)BALStatus.FechadoDefinitivo;
                                                    dBalancetes.BALancetesTableAdapter.Update(rowBAL);
                                                    rowBAL.AcceptChanges();
                                                }

                                            }
                                            break;
                                        }
                            }
                        }

                        if (lidos == 0)
                            if (CompetenciaFechadaDef != 0)
                            {
                                DataRow rowDatas = VirOleDb.TableAdapter.STTableAdapterTipo(VirDB.Bancovirtual.TiposDeBanco.AccessH).BuscaSQLRow(ComandoDatas, lookupCondominio_F1.CON_selrow.CONCodigo);
                                DateTime DI = (DateTime)rowDatas["Data Inicial"];
                                DateTime DF = (DateTime)rowDatas["Data Final"];
                                Competencia UltimaCompet = new Competencia(int.Parse(Maximo));
                                UltimaCompet.CON = lookupCondominio_F1.CON_sel;
                                dBalancetes.BALancetesRow novarow = dBalancetes.BALancetes.NewBALancetesRow();
                                novarow.BAL_CON = lookupCondominio_F1.CON_sel;
                                novarow.BALCompet = UltimaCompet.CompetenciaBind;
                                novarow.BALDataInicio = DI;
                                novarow.BALdataFim = DF;
                                novarow.BALSTATUS = (int)BALStatus.FechadoDefinitivo;
                                dBalancetes.BALancetes.AddBALancetesRow(novarow);
                                dBalancetes.BALancetesTableAdapter.Update(novarow);
                                novarow.AcceptChanges();
                            }
                            else
                            {
                                dataI.DateTime = DateTime.Today.AddMonths(-1);
                                dataF.DateTime = DateTime.Today;
                                botPrimeiro.Visible = true;
                            }
                        if (dBalancetes.BALancetes.Count > 0)
                        {
                            if (StatusFechados.Contains((BALStatus)dBalancetes.BALancetes[0].BALSTATUS))
                            {
                                Competencia Compet = new Competencia(dBalancetes.BALancetes[0].BALCompet).Add(1);
                                Compet.CON = lookupCondominio_F1.CON_sel;
                                dBalancetes.BALancetesRow novarow = dBalancetes.BALancetes.NewBALancetesRow();
                                novarow.BAL_CON = lookupCondominio_F1.CON_sel;
                                novarow.BALCompet = Compet.CompetenciaBind;
                                DateTime DI;
                                DateTime DF;
                                Compet.PeriodoBalancete(out DI, out DF);
                                novarow.BALDataInicio = DI;
                                novarow.BALdataFim = DF;
                                novarow.BALSTATUS = (int)BALStatus.NaoIniciado;
                                dBalancetes.BALancetes.AddBALancetesRow(novarow);
                                dBalancetes.BALancetesTableAdapter.Update(novarow);
                                novarow.AcceptChanges();
                            }
                        };

                        string comandoBusca =
    "SELECT     CONOculto, CONObsBal, CONLimiteBal,CONRelAcordoDet, PESSOAS.PESFone1, PESSOAS.PESNome\r\n" +
    "FROM         PESSOAS INNER JOIN\r\n" +
    "                      View_SINDICOS ON PESSOAS.PES = View_SINDICOS.CDR_PES RIGHT OUTER JOIN\r\n" +
    "                      CONDOMINIOS ON View_SINDICOS.CDR_CON = CONDOMINIOS.CON\r\n" +
    "WHERE     (CONDOMINIOS.CON = @P1);";
                        DataRow DRCon = TableAdapter.ST().BuscaSQLRow(comandoBusca, lookupCondominio_F1.CON_sel);

                        //bool apagar = DRCon["conoculto"] == DBNull.Value ? false : (bool)DRCon["conoculto"];
                        memoEdit1.Text = DRCon["CONObsBal"] == DBNull.Value ? "" : DRCon["CONObsBal"].ToString();

                        chDetalhado.Checked = (bool)DRCon["CONRelAcordoDet"];

                        if (DRCon["PESNome"] != DBNull.Value)
                            memoEdit1.Text += string.Format("\r\n\r\n Sindico: {0} - {1}", DRCon["PESNome"], DRCon["PESFone1"] == DBNull.Value ? "" : DRCon["PESFone1"]);

                        if (dBalancetes.ContasTableAdapter.Fill(dBalancetes.Contas, lookupCondominio_F1.CON_sel) > 0)
                        {
                            memoEdit1.Text += "\r\n\r\n Contas:";
                            foreach (dBalancetes.ContasRow rowContas in dBalancetes.Contas)
                            {
                                if (rowContas.IsCCTTituloNull())
                                    rowContas.CCTTitulo = "Conta corrente";
                                if (rowContas.IsCCTAgenciaDgNull() || rowContas.IsCCTAgenciaDgNull() || rowContas.IsCCTContaDgNull())
                                    continue;
                                memoEdit1.Text += string.Format("\r\n  Titulo:{6}  BANCO: {0} CONTA: {1}-{2} {3}-{4} {5}", rowContas.BCONome, rowContas.CCTAgencia, rowContas.CCTAgenciaDg, rowContas.CCTConta, rowContas.CCTContaDg, rowContas.CCTAplicacao ? "APLICACAO" : "CC", rowContas.CCTTitulo);
                            }
                        }


                        if (DRCon["CONLimiteBal"] != DBNull.Value)
                        {
                            int dia = (int)DRCon["CONLimiteBal"];
                            foreach (dBalancetes.BALancetesRow rowBAL in dBalancetes.BALancetes)
                            {
                                if (DateTime.DaysInMonth(rowBAL.BALdataFim.Year, rowBAL.BALdataFim.Month) < dia)
                                    dia = DateTime.DaysInMonth(rowBAL.BALdataFim.Year, rowBAL.BALdataFim.Month);
                                DateTime datacorte = new DateTime(rowBAL.BALdataFim.Year, rowBAL.BALdataFim.Month, dia);
                                if (datacorte <= rowBAL.BALdataFim)
                                    datacorte = datacorte.AddMonths(1);
                                rowBAL.dataLimite = datacorte;
                            }
                        }

                        //if ((lookupCondominio_F1.CON_sel != -1))
                        //    if (Framework.usuarioLogado.UsuarioLogadoNeon.UsuarioLogadoSt.VerificaFuncionalidade("GERE�NCIA") > 1)
                        //        apagar = false;
                        simpleButton1.Enabled = simpleButton2.Enabled = cBotaoImpBol5.Enabled = cBotaoImpBol1.Enabled = cBotaoFrancesinha.Enabled = cBotaoImpBol4.Enabled = ((dataI.EditValue != null) && ((dataF.EditValue != null)));
                        cBotaoImpInad.Enabled = cBotaoImpBol2.Enabled = (dataF.EditValue != null);// && (!apagar);
                    }
                    finally
                    {
                        evitaReentrada = false;
                    }
                }
            }
        }

        private void dateEdit1_EditValueChanged(object sender, EventArgs e)
        {
            Limpa();
        }

        private void cBotaoImpBol1_clicado(object sender, EventArgs e)
        {
            if (lookupCondominio_F1.CON_sel == -1)
                return;
            //dllImpresso.ExtratoBancario.ImpExtratoB Impresso = new dllImpresso.ExtratoBancario.ImpExtratoB();
            DataTable Contas = TableAdapter.ST().BuscaSQLTabela("SELECT CCT FROM ContaCorrenTe WHERE (CCT_CON = @P1)", lookupCondominio_F1.CON_sel);
            if (Contas.Rows.Count == 0)
                MessageBox.Show("Nenhum lan�amento neste per�odo");
            foreach (DataRow Row in Contas.Rows)
            {
                if (cBotaoImpBol1.BotaoClicado == dllImpresso.Botoes.Botao.botao)
                {

                    dllbanco.Extrato.cExtrato Extrato = new dllbanco.Extrato.cExtrato(dataI.DateTime, dataF.DateTime, (int)Row["CCT"], false);
                    
                    Extrato.Titulo = "Extrato " + lookupCondominio_F1.CON_selrow.CONNome;
                    Extrato.somenteleitura = false;
                    Extrato.VirShowModulo(CompontesBasicos.EstadosDosComponentes.JanelasAtivas);
                    
                }
                else
                {
                    //para dar mensagem se o extrato estiver com erro
                    new dllbanco.Extrato.cExtrato(dataI.DateTime, dataF.DateTime, (int)Row["CCT"], false);
                    dllImpresso.ExtratoBancario.ImpExtratoBal Impresso = new dllImpresso.ExtratoBancario.ImpExtratoBal(lookupCondominio_F1.CON_selrow.CONNome, dataI.DateTime, dataF.DateTime, (int)Row["CCT"]);
                    //int carregados = Impresso.dImpExtratoB1.DadosExtratoBTableAdapter.Fill(Impresso.dImpExtratoB1.DadosExtratoB, dataI.DateTime, dataF.DateTime, (int)Row["CCT"]);
                    if (!Impresso.ContemDados)                    
                        continue;
                    
                    //Impresso.Nomecondominio.Text = lookupCondominio_F1.CON_selrow.CONNome;
                    //Impresso.DataInicial.Text = dataI.DateTime.ToString("dd/MM/yyyy");
                    //Impresso.DataFinal.Text = dataF.DateTime.ToString("dd/MM/yyyy");
                    //Impresso.dImpExtratoB1.CalculosDadosExtratoB(chdadosChe.Checked);
                    
                    //Impresso.CreateDocument();
                    switch (cBotaoImpBol1.BotaoClicado)
                    {
                        case dllImpresso.Botoes.Botao.email:
                            if (VirEmailNeon.EmailDiretoNeon.EmalST.Enviar("", "Extrato", Impresso, "Extrato em anexo.", "Extrato"))
                                MessageBox.Show("E.mail enviado");
                            else
                            {
                                if (VirEmailNeon.EmailDiretoNeon.EmalST.UltimoErro != null)
                                    MessageBox.Show("Falha no envio");
                                else
                                    MessageBox.Show("Cancelado");
                            };
                            break;
                        default:                          
                        case dllImpresso.Botoes.Botao.botao:
                        case dllImpresso.Botoes.Botao.imprimir:
                        case dllImpresso.Botoes.Botao.imprimir_frente:
                            Impresso.Print();
                            break;
                        case dllImpresso.Botoes.Botao.pdf:
                        case dllImpresso.Botoes.Botao.PDF_frente:
                            using (SaveFileDialog saveFileDialog1 = new SaveFileDialog())
                            {
                                saveFileDialog1.DefaultExt = "pdf";
                                if (saveFileDialog1.ShowDialog() == DialogResult.OK)
                                {
                                    Impresso.ExportOptions.Pdf.Compressed = true;
                                    Impresso.ExportOptions.Pdf.ImageQuality = DevExpress.XtraPrinting.PdfJpegImageQuality.Lowest;
                                    Impresso.ExportToPdf(saveFileDialog1.FileName);
                                }
                            }
                            break;
                        case dllImpresso.Botoes.Botao.tela:
                            Impresso.ShowPreviewDialog();
                            break;
                    }
                }
            }
        }

        private BoletosProc.Boleto.dBoletos _dBoletos;
        private BoletosProc.Boleto.dBoletos dBoletos {
            get {
                if (_dBoletos == null)
                    _dBoletos = new BoletosProc.Boleto.dBoletos();
                return _dBoletos;
            }
        }

        
       

        private void cBotaoImpBol2_clicado(object sender, EventArgs e)
        {
            //sincronismo
            //dBoletos.BOLetosTableAdapter.FillByCONCompleto(dBoletos.BOLetos, lookupCondominio_F1.CON_sel);
            //dBoletos.BoletoAccessTableAdapter.FillByCodcon(dBoletos.BoletoAccess, lookupCondominio_F1.CON_selrow.CONCodigo);
            //SincAccess();
            //

            DateTime DataCortePag = Framework.datasets.dFERiados.DiasUteis(dataF.DateTime, 3, Framework.datasets.dFERiados.Sentido.avanca,Framework.datasets.dFERiados.Sabados.naoUteis,Framework.datasets.dFERiados.Feriados.naoUteis);
            //dBoletos.BOLetosTableAdapter.FillVencidosAte(dBoletos.BOLetos, dataF.DateTime, dataF.DateTime, lookupCondominio_F1.CON_sel);
            dBoletos.BOLetosTableAdapter.FillVencidosAte(dBoletos.BOLetos, dataF.DateTime, DataCortePag, lookupCondominio_F1.CON_sel);

            //if (dBoletos.BOLetos.FindByBOL(21727382) == null)
            //    MessageBox.Show("n�o est�");

            dBoletos.RecuperaOsBolDoAcordo(dataF.DateTime, lookupCondominio_F1.CON_sel);
            dBoletos.CamposComplementares(dataF.DateTime);

            //if (dBoletos.BOLetos.FindByBOL(21727382) == null)
            //    MessageBox.Show("n�o est�");
            //else
            //{
            //    BoletosProc.Boleto.dBoletos.BOLetosRow row = dBoletos.BOLetos.FindByBOL(21727382);
            //    MessageBox.Show(row.APTNumero + row.Total.ToString());
            //}

            Boletos.Boleto.Impresso.ImpInadimplencia Impresso = new Boletos.Boleto.Impresso.ImpInadimplencia();
            Impresso.bindingSource1.DataSource = dBoletos.BOLetos;
            Impresso.SetaDados(dataF.DateTime, lookupCondominio_F1.CON_selrow.CONNome);
            Impresso.parCorrecao.Value = true;
            Impresso.parMultaJuros.Value = true;
            Impresso.parHonorarios.Value = true;
            Impresso.CreateDocument();
            switch (cBotaoImpBol2.BotaoClicado)
            {                
                case dllImpresso.Botoes.Botao.email:
                    if (VirEmailNeon.EmailDiretoNeon.EmalST.Enviar("", "Relatorio de inadimpl�ncia", Impresso, "Relatorio de inadimpl�ncia.", "Relatorio de inadimpl�ncia"))
                        MessageBox.Show("E.mail enviado");
                    else
                    {
                        if (VirEmailNeon.EmailDiretoNeon.EmalST.UltimoErro != null)
                            MessageBox.Show("Falha no envio");
                        else
                            MessageBox.Show("Cancelado");
                    };
                    break;
                case dllImpresso.Botoes.Botao.imprimir:
                case dllImpresso.Botoes.Botao.imprimir_frente:
                case dllImpresso.Botoes.Botao.botao:
                case dllImpresso.Botoes.Botao.nulo:
                default:
                    Impresso.Print();
                    break;
                case dllImpresso.Botoes.Botao.pdf:
                case dllImpresso.Botoes.Botao.PDF_frente:
                    using (SaveFileDialog saveFileDialog1 = new SaveFileDialog())
                    {
                        saveFileDialog1.DefaultExt = "pdf";
                        if (saveFileDialog1.ShowDialog() == DialogResult.OK)
                        {
                            Impresso.ExportOptions.Pdf.Compressed = true;
                            Impresso.ExportOptions.Pdf.ImageQuality = DevExpress.XtraPrinting.PdfJpegImageQuality.Lowest;
                            Impresso.ExportToPdf(saveFileDialog1.FileName);
                        }
                    }
                    break;
                case dllImpresso.Botoes.Botao.tela:
                    Impresso.ShowPreviewDialog();
                    break;
            }

        }

        

        DateTime DataFinalCheia 
        {
            get 
            {
                return dataF.DateTime.AddDays(1).AddMilliseconds(-1);
            }
        }

        private void cBotaoImpBol4_clicado(object sender, EventArgs e)
        {
            using (Boletos.ImpAcordo Impresso = new Boletos.ImpAcordo(chDetalhado.Checked))
            {
                Impresso.OcultarAdvogado = !chAdvogados.Checked;
                Impresso.CarregaDados(lookupCondominio_F1.CON_sel, dataI.DateTime, DataFinalCheia, chCancelados.Checked);
                Impresso.SetNome(string.Format("{0} ({1:dd/MM/yy} - {2:dd/MM/yy})", lookupCondominio_F1.CON_selrow.CONNome, dataI.DateTime, dataF.DateTime));
                Impresso.CreateDocument();
                switch (cBotaoImpBol4.BotaoClicado)
                {
                    case dllImpresso.Botoes.Botao.email:
                        if (VirEmailNeon.EmailDiretoNeon.EmalST.Enviar("", "Acordos", Impresso, "Acordos.", "Acordos"))
                            MessageBox.Show("E.mail enviado");
                        else
                        {
                            if (VirEmailNeon.EmailDiretoNeon.EmalST.UltimoErro != null)
                                MessageBox.Show("Falha no envio");
                            else
                                MessageBox.Show("Cancelado");
                        };
                        break;
                    case dllImpresso.Botoes.Botao.imprimir:
                    case dllImpresso.Botoes.Botao.imprimir_frente:
                    case dllImpresso.Botoes.Botao.botao:
                        Impresso.Print();
                        break;
                    case dllImpresso.Botoes.Botao.pdf:
                    case dllImpresso.Botoes.Botao.PDF_frente:
                        using (SaveFileDialog saveFileDialog1 = new SaveFileDialog())
                        {
                            saveFileDialog1.DefaultExt = "pdf";
                            if (saveFileDialog1.ShowDialog() == DialogResult.OK)
                            {
                                Impresso.ExportOptions.Pdf.Compressed = true;
                                Impresso.ExportOptions.Pdf.ImageQuality = DevExpress.XtraPrinting.PdfJpegImageQuality.Lowest;
                                Impresso.ExportToPdf(saveFileDialog1.FileName);
                            }
                        }
                        break;
                    case dllImpresso.Botoes.Botao.tela:
                        Impresso.ShowPreviewDialog();
                        break;
                }
            }
        }

        private void cBotaoImpBol5_clicado(object sender, EventArgs e)
        {
            Boletos.Boleto.Impresso.ImpBoletosColunas Impresso = new Boletos.Boleto.Impresso.ImpBoletosColunas(lookupCondominio_F1.CON_sel, dataI.DateTime, dataF.DateTime, radioGroup1.EditValue.ToString() == "E" ? Boletos.Boleto.Impresso.ImpBoletosColunas.TipoRelatorio.Emissao : Boletos.Boleto.Impresso.ImpBoletosColunas.TipoRelatorio.Recebimento, true);
            Impresso.CreateDocument();
            switch (cBotaoImpBol5.BotaoClicado)
            {
                case dllImpresso.Botoes.Botao.email:
                    if (VirEmailNeon.EmailDiretoNeon.EmalST.Enviar("", "Boletos", Impresso, "Boletos", "Boletos"))
                        MessageBox.Show("E.mail enviado");
                    else
                    {
                        if (VirEmailNeon.EmailDiretoNeon.EmalST.UltimoErro != null)
                            MessageBox.Show("Falha no envio");
                        else
                            MessageBox.Show("Cancelado");
                    };
                    break;
                case dllImpresso.Botoes.Botao.imprimir:
                case dllImpresso.Botoes.Botao.imprimir_frente:
                case dllImpresso.Botoes.Botao.botao:
                    Impresso.Print();
                    break;
                case dllImpresso.Botoes.Botao.pdf:
                case dllImpresso.Botoes.Botao.PDF_frente:
                    using (SaveFileDialog saveFileDialog1 = new SaveFileDialog())
                    {
                        saveFileDialog1.DefaultExt = "pdf";
                        if (saveFileDialog1.ShowDialog() == DialogResult.OK)
                        {
                            Impresso.ExportOptions.Pdf.Compressed = true;
                            Impresso.ExportOptions.Pdf.ImageQuality = DevExpress.XtraPrinting.PdfJpegImageQuality.Lowest;
                            Impresso.ExportToPdf(saveFileDialog1.FileName);
                        }
                    }
                    break;
                case dllImpresso.Botoes.Botao.tela:
                    Impresso.ShowPreviewDialog();
                    break;
            }
        }

        private void simpleButton1_Click(object sender, EventArgs e)
        {
            using (SaveFileDialog saveFileDialog1 = new SaveFileDialog())
            {
                saveFileDialog1.DefaultExt = "xls";
                if (saveFileDialog1.ShowDialog() == DialogResult.OK)
                {
                    using (Boletos.Boleto.Impresso.ImpBoletosColunas Impresso = new Boletos.Boleto.Impresso.ImpBoletosColunas(lookupCondominio_F1.CON_sel, dataI.DateTime, dataF.DateTime, radioGroup1.EditValue.ToString() == "E" ? Boletos.Boleto.Impresso.ImpBoletosColunas.TipoRelatorio.Emissao : Boletos.Boleto.Impresso.ImpBoletosColunas.TipoRelatorio.Recebimento, false))
                    {
                        Impresso.CreateDocument();
                        Impresso.ExportOptions.Xls.SheetName = "Boletos";
                        Impresso.ExportOptions.Xls.ShowGridLines = false;
                        Impresso.ExportOptions.Xls.TextExportMode = DevExpress.XtraPrinting.TextExportMode.Value;
                        Impresso.ExportToXls(saveFileDialog1.FileName);
                    }
                    if (MessageBox.Show(string.Format("Arquivo salvo: {0}\r\nVoc� gostaria de abrir o arquivo?", saveFileDialog1.FileName), "Exportar Para...", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
                        try
                        {
                            using (System.Diagnostics.Process process = new System.Diagnostics.Process())
                            {
                                process.StartInfo.FileName = saveFileDialog1.FileName;
                                process.StartInfo.Verb = "Open";
                                process.StartInfo.WindowStyle = System.Diagnostics.ProcessWindowStyle.Maximized;
                                process.Start();
                            }
                        }
                        catch
                        {
                            MessageBox.Show(this, "N�o foi encontrado em seu sistema operacional um aplicativo apropriado para abrir o arquivo com os dados exportados.", Application.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Error);
                        }
                }
            }
        }

        private void simpleButton2_Click(object sender, EventArgs e)
        {
            //
            //System.Windows.Forms.SaveFileDialog saveFileDialog1 = new System.Windows.Forms.SaveFileDialog();
            //saveFileDialog1.DefaultExt = "xls";
            //if (saveFileDialog1.ShowDialog() == System.Windows.Forms.DialogResult.OK)
            //{
            using (Boletos.Boleto.Impresso.ImpBoletosColunas Impresso = new Boletos.Boleto.Impresso.ImpBoletosColunas(lookupCondominio_F1.CON_sel, dataI.DateTime, dataF.DateTime, radioGroup1.EditValue.ToString() == "E" ? Boletos.Boleto.Impresso.ImpBoletosColunas.TipoRelatorio.Emissao : Boletos.Boleto.Impresso.ImpBoletosColunas.TipoRelatorio.Recebimento, false))
            {
                Impresso.CreateDocument();
                Impresso.ExportOptions.Csv.TextExportMode = DevExpress.XtraPrinting.TextExportMode.Text;
                Impresso.ExportOptions.Csv.Separator = ";";                                
                using (System.IO.MemoryStream Mem = new System.IO.MemoryStream())
                {
                    Impresso.ExportToCsv(Mem);                    
                    Mem.Position = 0;
                    Clipboard.SetData(DataFormats.CommaSeparatedValue, Mem);
                }

                try
                {
                    MessageBox.Show("Teste 1");
                    Impresso.ExportOptions.Csv.Separator = "\t";
                    string DirTMP = string.Format(@"{0}\TMP", System.IO.Path.GetDirectoryName(Application.ExecutablePath));
                    if (!System.IO.Directory.Exists(DirTMP))
                        System.IO.Directory.CreateDirectory(DirTMP);
                    string ArquivoTMP = string.Format(@"{0}\boletos_{1:dd_MM_yyyy}_a_{2:dd_MM_yyyy}.csv", DirTMP, dataI.DateTime, dataF.DateTime);
                    Impresso.ExportToCsv(ArquivoTMP);
                    MessageBox.Show(string.Format("Arquivo teste:{0}", ArquivoTMP));
                }
                catch { MessageBox.Show("erro 1"); }
                try
                {
                    MessageBox.Show("Teste 2");
                    Impresso.ExportOptions.Csv.Separator = ";";
                    string DirTMP = string.Format(@"{0}\TMP", System.IO.Path.GetDirectoryName(Application.ExecutablePath));                    
                    string ArquivoTMP = string.Format(@"{0}\boletos_{1:dd_MM_yyyy}_a_{2:dd_MM_yyyy}_1.csv", DirTMP, dataI.DateTime, dataF.DateTime);
                    Impresso.ExportToCsv(ArquivoTMP);
                    MessageBox.Show(string.Format("Arquivo teste:{0}", ArquivoTMP));
                }
                catch { MessageBox.Show("erro 2"); }
                try
                {
                    MessageBox.Show("Teste 3");
                    Impresso.ExportOptions.Xls.ExportMode = DevExpress.XtraPrinting.XlsExportMode.SingleFile;
                    string DirTMP = string.Format(@"{0}\TMP", System.IO.Path.GetDirectoryName(Application.ExecutablePath));                    
                    string ArquivoTMP = string.Format(@"{0}\boletos_{1:dd_MM_yyyy}_a_{2:dd_MM_yyyy}.xls", DirTMP, dataI.DateTime, dataF.DateTime);
                    Impresso.ExportToXls(ArquivoTMP);
                    MessageBox.Show(string.Format("Arquivo teste:{0}", ArquivoTMP));
                }
                catch { MessageBox.Show("erro 3"); }
            }
            try
            {
                //System.Diagnostics.Process process = new System.Diagnostics.Process();
                //process.StartInfo.FileName = ArquivoTMP;
                //process.StartInfo.Verb = "Open";
                //process.StartInfo.WindowStyle = System.Diagnostics.ProcessWindowStyle.Maximized;
                //process.Start("");        
                MessageBox.Show("Dados na �rea de transferenica para serem colados na planilha");
                System.Diagnostics.Process.Start("excel.exe");
            }
            catch
            {
                MessageBox.Show(this, "N�o foi encontrado em seu sistema operacional um aplicativo apropriado para abrir o arquivo com os dados exportados.", Application.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Error);
                MessageBox.Show("Dados na �rea de transferenica para serem colodos na planilha");
            }

        }

        private void gridView1_RowStyle(object sender, DevExpress.XtraGrid.Views.Grid.RowStyleEventArgs e)
        {
            dBalancetes.BALancetesRow row = (dBalancetes.BALancetesRow)gridView1.GetDataRow(e.RowHandle);
            if (row != null)
            {
                e.Appearance.BackColor = Enumeracoes.virEnumBALStatus.virEnumBALStatusSt.GetCor(row.BALSTATUS);
            }
        }

        private void gridView1_CustomColumnDisplayText(object sender, DevExpress.XtraGrid.Views.Base.CustomColumnDisplayTextEventArgs e)
        {
            if (e.Column == colBALCompet)
                if ((e.Value != null) && (e.Value != DBNull.Value))
                {
                    int iValor = (int)e.Value;
                    e.DisplayText = string.Format("{0:00}/{1:0000}", iValor % 100, iValor / 100);
                }
        }

        private void gridView1_FocusedRowChanged(object sender, DevExpress.XtraGrid.Views.Base.FocusedRowChangedEventArgs e)
        {
            dBalancetes.BALancetesRow rowBAL = (dBalancetes.BALancetesRow)gridView1.GetDataRow(e.FocusedRowHandle);
            if (rowBAL != null)
            {
                dataI.DateTime = rowBAL.BALDataInicio;
                dataF.DateTime = rowBAL.BALdataFim;
            }
        }


        private void AddObs(dBalancetes.BALancetesRow rowBAL,string obs)
        {
            rowBAL.BALObs = string.Format("{0}{1:dd/MM/yyyy HH:mm} ({2})\r\n    {3}\r\n\r\n",
                rowBAL.IsBALObsNull()?"":rowBAL.BALObs+Environment.NewLine,
                DateTime.Now,
                Framework.usuarioLogado.UsuarioLogadoNeon.UsuarioLogadoStNEON.USUNome,obs);
        }
        

        private void repositoryItemButtonEdit1_ButtonClick(object sender, DevExpress.XtraEditors.Controls.ButtonPressedEventArgs e)
        {
            dBalancetes.BALancetesRow rowBAL = (dBalancetes.BALancetesRow)gridView1.GetFocusedDataRow();
            if (rowBAL != null)
            {
                rowBAL.EndEdit();
                //bool Novo = false;
                //if (!CompontesBasicos.FormPrincipalBase.strEmProducao)
                //    Novo = true;
                balancete balancete1 = null;
                //if (TableAdapter.ST().EstaCadastrado("select CONBalBoleto from condominios where CON = @P1", rowBAL.BAL_CON))
                //    balancete1 = new balancete(rowBAL.BAL);
                switch (e.Button.Kind)
                {
                    case DevExpress.XtraEditors.Controls.ButtonPredefines.Ellipsis:
                        string obs = "";
                        if (VirInput.Input.Execute("Obs:", ref obs, true))
                            AddObs(rowBAL, string.Format("OBS:\r\n.....\r\n{0}\r\n.....", obs));
                        break;
                    case DevExpress.XtraEditors.Controls.ButtonPredefines.Left:
                    case DevExpress.XtraEditors.Controls.ButtonPredefines.SpinLeft:
                        switch ((BALStatus)rowBAL.BALSTATUS)
                        {
                            case BALStatus.Termindo:
                            case BALStatus.Publicado:
                            case BALStatus.ParaPublicar:
                            //case BALStatus.FechadoDefinitivo:
                                rowBAL.BALSTATUS = (int)BALStatus.Producao;
                                AddObs(rowBAL, "Re-aberto");
                                Competencia comp = new Competencia(rowBAL.BALCompet);
                                comp.CON = rowBAL.BAL_CON;
                                dllpublicaBalancete.InterBal Bal = new dllpublicaBalancete.InterBal(CompontesBasicosProc.FuncoesBasicasProc.EstaEmProducao, Framework.DSCentral.EMP, comp, rowBAL.BALDataInicio, rowBAL.BALdataFim,rowBAL.BAL);
                                string resPuclicacao = Bal.CancelaPublicacao();
                                if (resPuclicacao.ToUpper() == "OK")
                                    AddObs(rowBAL, "cancelada publica��o");
                                break;
                            case BALStatus.Producao:
                                rowBAL.BALSTATUS = (int)BALStatus.NaoIniciado;
                                AddObs(rowBAL, "Marcado como \"N�o iniciado\"");
                                break;
                            case BALStatus.NaoIniciado:
                                if (dBalancetes.BALancetes.Count == 1)
                                {
                                    rowBAL.Delete();
                                    dBalancetes.BALancetesTableAdapter.Update(rowBAL);
                                    botPrimeiro.Visible = true;
                                }
                                break;
                        }
                        break;
                    case DevExpress.XtraEditors.Controls.ButtonPredefines.SpinRight:
                    case DevExpress.XtraEditors.Controls.ButtonPredefines.Right:
                        if (balancete1 != null)
                            if (balancete1.MudaStatus(balancete.DirecaoStatus.Avante,this))
                                break;

                        switch ((BALStatus)rowBAL.BALSTATUS)
                        {
                            case BALStatus.NaoIniciado:
                                DateTime DIcad;
                                DateTime DFcad;
                                Competencia Compet = new Competencia(rowBAL.BALCompet);
                                Compet.CON = rowBAL.BAL_CON;
                                Compet.PeriodoBalancete(out DIcad, out DFcad);
                                if ((rowBAL.BALDataInicio != DIcad) || (rowBAL.BALdataFim != DFcad))
                                {
                                    string strpercad = string.Format("{0:dd/MM/yyyy} a {1:dd/MM/yyyy}", DIcad, DFcad);
                                    string strperbal = string.Format("{0:dd/MM/yyyy} a {1:dd/MM/yyyy}", rowBAL.BALDataInicio, rowBAL.BALdataFim);
                                    using (cPerguntaPer cPerg = new cPerguntaPer(strperbal, strpercad))
                                    {
                                        if (rowBAL.BALDataInicio == DIcad)
                                            cPerg.desabilitacorrecaodecadastro();
                                        if (cPerg.VirShowModulo(CompontesBasicos.EstadosDosComponentes.PopUp) == DialogResult.OK)
                                            switch (cPerg.radioGroup1.SelectedIndex)
                                            {
                                                case -1:
                                                    return;
                                                case 0:
                                                    rowBAL.BALDataInicio = DIcad;
                                                    rowBAL.BALdataFim = DFcad;
                                                    break;
                                                case 1:
                                                    break;
                                                case 2:
                                                    TableAdapter.ST().ExecutarSQLNonQuery("UPDATE CONDOMINIOS SET CONDiaContabilidade = @P1 WHERE (CON = @P2)", rowBAL.BALDataInicio.Day, rowBAL.BAL_CON);
                                                    break;
                                            }
                                        else
                                        {
                                            MessageBox.Show("Cancelado");
                                            return;
                                        }
                                    }
                                }
                                rowBAL.BALSTATUS = (int)BALStatus.Producao;
                                if (rowBAL.IsBALInicioProducaoNull())
                                    rowBAL.BALInicioProducao = DateTime.Now;
                                AddObs(rowBAL, "Iniciado");
                                break;
                            case BALStatus.Producao:
                                
                                    rowBAL.BALSTATUS = (int)BALStatus.Termindo;
                                    if (rowBAL.IsBALFimProducaoNull())
                                        rowBAL.BALFimProducao = DateTime.Now;
                                    AddObs(rowBAL, "Terminado");
                                
                                break;
                            case BALStatus.Termindo:
                            case BALStatus.ParaPublicar:

                                rowBAL.BALSTATUS = (int)BALStatus.ParaPublicar;
                                Competencia comp = new Competencia(rowBAL.BALCompet);
                                comp.CON = rowBAL.BAL_CON;
                                string resPuclicacao;
                                
                                dllpublicaBalancete.InterBal Bal = new dllpublicaBalancete.InterBal(CompontesBasicosProc.FuncoesBasicasProc.EstaEmProducao, Framework.DSCentral.EMP, comp, rowBAL.BALDataInicio, rowBAL.BALdataFim,rowBAL.BAL);
                                resPuclicacao = Bal.Publica();
                                

                                if (resPuclicacao.ToUpper() == "OK")
                                {
                                    rowBAL.BALSTATUS = (int)BALStatus.Publicado;
                                    AddObs(rowBAL, "Publicado");
                                }
                                else
                                {
                                    AddObs(rowBAL, "Publica��o solicitada\r\n" + resPuclicacao);
                                }
                                break;
                            case BALStatus.Publicado:

                                rowBAL.BALSTATUS = (int)BALStatus.FechadoDefinitivo;                                
                                AddObs(rowBAL, "Marcado como Fechado definitivo manualmente");

                                break;
                        }

                        break;

                }
                if (rowBAL.RowState == DataRowState.Modified)
                {
                    rowBAL.EndEdit();
                    dBalancetes.BALancetesTableAdapter.Update(rowBAL);
                    rowBAL.AcceptChanges();
                    gridView1.RefreshData();                    
                }
            }
        }

        private void gridView1_RowUpdated(object sender, DevExpress.XtraGrid.Views.Base.RowObjectEventArgs e)
        {
            DataColumn colI = dBalancetes.BALancetes.Columns["BALDataInicio"];
            DataColumn colF = dBalancetes.BALancetes.Columns["BALdataFim"];
            dBalancetes.BALancetesRow rowBAL = (dBalancetes.BALancetesRow)((DataRowView)(e.Row)).Row;
            if (rowBAL.RowState == DataRowState.Modified)
            {
                rowBAL.EndEdit();
                if((rowBAL[colI,DataRowVersion.Original] != rowBAL[colI,DataRowVersion.Current])
                    ||
                   (rowBAL[colF,DataRowVersion.Original] != rowBAL[colF,DataRowVersion.Current]))
                    AddObs(rowBAL, string.Format("Per�odo do balancete alterado de {0:dd/MM/yyyy} - {1:dd/MM/yyyy} para {2:dd/MM/yyyy} - {3:dd/MM/yyyy}",
                        rowBAL[colI,DataRowVersion.Original],
                        rowBAL[colF,DataRowVersion.Original],
                        rowBAL[colI,DataRowVersion.Current],                        
                        rowBAL[colF,DataRowVersion.Current])                        
                        );                                
                dBalancetes.BALancetesTableAdapter.Update(rowBAL);
                rowBAL.AcceptChanges();
                gridView1.RefreshData();
            }
        }

        private void gridView1_CalcRowHeight(object sender, DevExpress.XtraGrid.Views.Grid.RowHeightEventArgs e)
        {
            if (e.RowHandle >= 0)
            {
                dBalancetes.BALancetesRow rowBAL = (dBalancetes.BALancetesRow)gridView1.GetDataRow(e.RowHandle);
                if (rowBAL.IsBALObsNull())
                    return;
                Graphics g = CreateGraphics();
                SizeF size = g.MeasureString(rowBAL.BALObs, colBALObs.AppearanceCell.Font, colBALObs.Width);
                double completeHeight = Math.Round(size.Height) + 10;
                double lineHeight = colBALObs.AppearanceCell.Font.Height;
                double correctedHeight = completeHeight;

                if (completeHeight > lineHeight * 10)
                    correctedHeight = lineHeight * 10 + 3;

                e.RowHeight = (int)Math.Truncate(correctedHeight);
            }

        }

        private void repositoryItemButtonEdit2_ButtonClick(object sender, DevExpress.XtraEditors.Controls.ButtonPressedEventArgs e)
        {            
            dBalancetes.BALancetesRow rowBAL = (dBalancetes.BALancetesRow)gridView1.GetFocusedDataRow();
            bool Parcial = false;

            if (rowBAL != null)
            {
                
                int modelo = int.Parse(e.Button.Caption.Substring(1));
                Competencia comp = new Competencia(rowBAL.BALCompet);
                comp.CON = rowBAL.BAL_CON;
                string URL = "";
                if (StatusFechados.Contains((BALStatus)rowBAL.BALSTATUS))
                {
                    dllpublicaBalancete.InterBal Bal = new dllpublicaBalancete.InterBal(CompontesBasicosProc.FuncoesBasicasProc.EstaEmProducao, Framework.DSCentral.EMP, comp, rowBAL.BALDataInicio, rowBAL.BALdataFim, rowBAL.BAL);
                    if (Bal.URLPublicacao(modelo))
                    {
                        URL = string.Format("www.neonimoveis.com.br/balancetes/{0}{1}{2}{3}.asp",
                                Framework.DSCentral.EMP,
                                lookupCondominio_F1.CON_selrow.CONCodigo.Replace(".", "_").Replace("\\", "_").Replace("/", "_").Replace(" ", "_"),
                                modelo,
                                comp.ToStringN());                        
                    }
                    else
                        MessageBox.Show("Balancete n�o dispon�vel");
                }
                else
                {
                    dllpublicaBalancete.InterBal Bal = new dllpublicaBalancete.InterBal(CompontesBasicosProc.FuncoesBasicasProc.EstaEmProducao, Framework.DSCentral.EMP, comp, dataI.DateTime, dataF.DateTime, rowBAL.BAL);
                    if ((BALStatus)rowBAL.BALSTATUS == BALStatus.NaoIniciado)
                    {    
                        Parcial = Bal.parcial = true;
                    }
                    URL = Bal.Publica(System.Windows.Forms.Application.StartupPath,modelo);
                };
                if (URL != "")
                {
                    if (URL == "Condominio n�o encontrado")
                        MessageBox.Show(URL);
                    else
                    {
                        CompontesBasicos.ComponenteWEB novoWeb = new CompontesBasicos.ComponenteWEB();

                        novoWeb.MostraEndereco = false;
                        novoWeb.PermirtirImpressao(!Parcial);
                        novoWeb.Doc = DockStyle.Fill;
                        novoWeb.Navigate(URL);
                        novoWeb.Titulo = string.Format("{0} {1} M{2}", lookupCondominio_F1.CON_selrow.CONCodigo, rowBAL.BALCompet, modelo);
                        novoWeb.VirShowModulo(CompontesBasicos.EstadosDosComponentes.JanelasAtivas);
                        ((DevExpress.XtraTab.XtraTabPage)novoWeb.Parent).ShowCloseButton = DevExpress.Utils.DefaultBoolean.True;
                    }
                };
            }
        }

        private void gridView1_RowCellStyle(object sender, DevExpress.XtraGrid.Views.Grid.RowCellStyleEventArgs e)
        {
            dBalancetes.BALancetesRow rowBAL = (dBalancetes.BALancetesRow)gridView1.GetDataRow(e.RowHandle);
            if (rowBAL == null)
                return;
            if (e.Column == coldataLimite)
            {
                e.Appearance.BackColor = Color.White;
                e.Appearance.ForeColor = Color.Black;
                if (rowBAL.IsdataLimiteNull())
                    return;
                BALStatus status = (BALStatus)rowBAL.BALSTATUS;
                ArrayList Stok = new ArrayList(new BALStatus[] { BALStatus.ParaPublicar, BALStatus.Publicado, BALStatus.Termindo, BALStatus.FechadoDefinitivo,BALStatus.Enviado });
                if (Stok.Contains(status))
                    e.Appearance.BackColor = e.Appearance.BackColor2 = Color.Lime;
                else
                {
                    if (rowBAL.dataLimite < DateTime.Today)
                        e.Appearance.BackColor = e.Appearance.BackColor2 = Color.Red;
                    else if (rowBAL.dataLimite < DateTime.Today.AddDays(3))
                        e.Appearance.BackColor = e.Appearance.BackColor2 = Color.Yellow;
                    else
                        e.Appearance.BackColor = e.Appearance.BackColor2 = Color.Wheat;
                }

            }
        }

        private void botPrimeiro_Click(object sender, EventArgs e)
        {
            Competencia Compet = InputCompetencia.stInput();
            if (Compet != null)
            {
                Compet.CON = lookupCondominio_F1.CON_sel;
                dBalancetes.BALancetesRow novarow = dBalancetes.BALancetes.NewBALancetesRow();
                novarow.BAL_CON = lookupCondominio_F1.CON_sel;
                novarow.BALCompet = Compet.CompetenciaBind;
                DateTime DI;
                DateTime DF;
                Compet.PeriodoBalancete(out DI, out DF);
                novarow.BALDataInicio = DI;
                novarow.BALdataFim = DF;
                novarow.BALSTATUS = (int)BALStatus.NaoIniciado;
                dBalancetes.BALancetes.AddBALancetesRow(novarow);
                dBalancetes.BALancetesTableAdapter.Update(novarow);
                novarow.AcceptChanges();
                botPrimeiro.Visible = false;
            }
        }

        private void simpleButton3_Click(object sender, EventArgs e)
        {            
            //if (lookupCondominio_F1.CON_sel == -1)
            //    return;
            //fechamento.cFechamento cFechamento1 = new fechamento.cFechamento(lookupCondominio_F1.CON_selrow.CONCodigo,lookupCondominio_F1.CON_sel);
            //cFechamento1.VirShowModulo(CompontesBasicos.EstadosDosComponentes.PopUp);            
        }

        private void cBotaoFrancesinha_clicado(object sender, EventArgs e)
        {
            if (lookupCondominio_F1.CON_sel == -1)
                return;
            dllbanco.ImpFran.ImpFranc Impresso = new dllbanco.ImpFran.ImpFranc();
            Impresso.dImpFranc1.FrancTableAdapter.FillNovo(Impresso.dImpFranc1.Franc, dataI.DateTime, dataF.DateTime, lookupCondominio_F1.CON_sel);
            string Erros = Impresso.dImpFranc1.Verifica(lookupCondominio_F1.CON_sel, dataI.DateTime, dataF.DateTime);
            if (Erros != "")
                MessageBox.Show("ATEN��O:\r\nErro nas datas:\r\n" + Erros);

            if (cBotaoFrancesinha.BotaoClicado == dllImpresso.Botoes.Botao.botao)
            {
                dllbanco.ImpFran.cFranc cFranc1 = new dllbanco.ImpFran.cFranc();
                cFranc1.Titulo = String.Format("{0} {1:dd/MM/yyyy} - {2:dd/MM/yyyy}", lookupCondominio_F1.CON_selrow.CONNome, dataI.DateTime, dataF.DateTime);
                cFranc1.somenteleitura = true;
                cFranc1.dImpFrancBindingSource.DataSource = Impresso.dImpFranc1;
                cFranc1.VirShowModulo(CompontesBasicos.EstadosDosComponentes.JanelasAtivas);
            }
            else
            {
                Impresso.SetNome(lookupCondominio_F1.CON_selrow.CONNome);
                Impresso.xrTitulo.Text = String.Format("Extrato de Movimenta��o da Carteira de Cobran�a {0:dd/MM/yyyy} - {1:dd/MM/yyyy}", dataI.DateTime, dataF.DateTime);
                Impresso.CreateDocument();
                switch (cBotaoFrancesinha.BotaoClicado)
                {
                    case dllImpresso.Botoes.Botao.email:
                        if (VirEmailNeon.EmailDiretoNeon.EmalST.Enviar("", "Francesinha", Impresso, "Francesinha.", "Francesinha"))
                            MessageBox.Show("E.mail enviado");
                        else
                        {
                            if (VirEmailNeon.EmailDiretoNeon.EmalST.UltimoErro != null)
                                MessageBox.Show("Falha no envio");
                            else
                                MessageBox.Show("Cancelado");
                        };
                        break;
                    case dllImpresso.Botoes.Botao.imprimir:
                    case dllImpresso.Botoes.Botao.imprimir_frente:
                    case dllImpresso.Botoes.Botao.botao:
                        Impresso.Print();
                        break;
                    case dllImpresso.Botoes.Botao.pdf:
                    case dllImpresso.Botoes.Botao.PDF_frente:
                        using (SaveFileDialog saveFileDialog1 = new SaveFileDialog())
                        {
                            saveFileDialog1.DefaultExt = "pdf";
                            if (saveFileDialog1.ShowDialog() == DialogResult.OK)
                            {
                                Impresso.ExportOptions.Pdf.Compressed = true;
                                Impresso.ExportOptions.Pdf.ImageQuality = DevExpress.XtraPrinting.PdfJpegImageQuality.Lowest;
                                Impresso.ExportToPdf(saveFileDialog1.FileName);
                            }
                        }
                        break;
                    case dllImpresso.Botoes.Botao.tela:
                        Impresso.ShowPreviewDialog();
                        break;
                }

            }
        }

        private void MostraPDF(object sender, DevExpress.XtraEditors.Controls.ButtonPressedEventArgs e)
        {
            MostraBalancete(false);
        }

        private void MostraBalancete(bool resumido)
        {
            if (lookupCondominio_F1.CON_sel == -1)
                return;
            dBalancetes.BALancetesRow rowBAL = (dBalancetes.BALancetesRow)gridView1.GetFocusedDataRow();
            if (rowBAL != null)
            {
                Competencia comp = new Competencia(rowBAL.BALCompet);
                balancete.MostraBalancetePDF(lookupCondominio_F1.CON_sel, lookupCondominio_F1.CON_selrow.CONCodigo, comp, resumido);                
            }
        }

        private void MostraPDFRed(object sender, DevExpress.XtraEditors.Controls.ButtonPressedEventArgs e)
        {
            MostraBalancete(true);
        }

        private void gridView1_CustomRowCellEdit(object sender, DevExpress.XtraGrid.Views.Grid.CustomRowCellEditEventArgs e)
        {
            dBalancetes.BALancetesRow rowBAL = (dBalancetes.BALancetesRow)gridView1.GetDataRow(e.RowHandle);
            if (rowBAL != null)
            {
                //if (e.Column.Name == colBALN.Name)
                //{
                //    if ((!rowBAL.IsCONDivideSaldosNull()) && rowBAL.CONDivideSaldos)
                //        e.RepositoryItem = repositoryItemButtonBalN;
                //}
                if (rowBAL.GetSaldoContaLogicaRows().Length > 0)
                {
                    BALStatus Status = (BALStatus)rowBAL.BALSTATUS;
                    if (Status.EstaNoGrupo(BALStatus.FechadoDefinitivo, BALStatus.Publicado, BALStatus.Enviado))
                    {
                        if (e.Column.Name == colBALRed.Name)
                        {
                            e.RepositoryItem = repositoryItemButtonBALRed;
                        }
                        else if (e.Column.Name == colBAL.Name)
                        {
                            e.RepositoryItem = repositoryItemButtonBAL;
                        }
                    }
                }
            }
        }

        /*
        private void button1_Click(object sender, EventArgs e)
        {
            if (lookupCondominio_F1.CON_sel < 0)
                return;
            string comando = "SELECT BOL, BOL_CON FROM BOLetos WHERE (BOLPagamento BETWEEN @P1 AND @P2) AND (BOL_CON = @P3)";
            DataTable DT = TableAdapter.ST().BuscaSQLTabela(comando,dataI.DateTime,dataF.DateTime,lookupCondominio_F1.CON_sel);
            using (CompontesBasicos.Espera.cEspera ESP = new CompontesBasicos.Espera.cEspera(this))
            {
                ESP.Espere("Gravando");
                ESP.AtivaGauge(DT.Rows.Count);
                foreach (DataRow DR in DT.Rows)
                {
                    ESP.Gauge();
                    int BOL = (int)DR[0];
                    Boletos.Boleto.Boleto Boleto = new Boletos.Boleto.Boleto(BOL);
                    Boleto.GravaNoBalanceteVelho(chLimpar.Checked);
                    Boleto.ExportaAccesNovo();
                }
            }
        }*/

        private void button2_Click(object sender, EventArgs e)
        {
            string comando1 =
"SELECT DISTINCT Codcon, [M�s Compet�ncia], mid([M�s Compet�ncia], 3, 4) AS ano, mid([M�s Compet�ncia], 1, 2) AS mes\r\n" +
"FROM     [Hist�rico Lan�amentos]\r\n" +
"ORDER BY Codcon, mid([M�s Compet�ncia], 3, 4) DESC, mid([M�s Compet�ncia], 1, 2) DESC;";
            string comando2 =
"DELETE FROM [Hist�rico Lan�amentos]\r\n" +
"WHERE  ([Hist�rico Lan�amentos].Codcon = ?) AND ([Hist�rico Lan�amentos].[M�s Compet�ncia] = ?);";
            VirOleDb.TableAdapter TA = new VirOleDb.TableAdapter(VirDB.Bancovirtual.TiposDeBanco.AccessH);
            DataTable DT = TA.BuscaSQLTabela(comando1);
            string CODCON = "";
            int Contador = 0;
            string Relatorio = "Relat�rio hist�rico\r\n\r\n";
            foreach (DataRow DR in DT.Rows)
            {
                if (CODCON == DR["CODCON"].ToString())
                {
                    Contador++;
                    if (Contador > 12)
                    {
                        Relatorio += string.Format("Apagar {0} - {1}\r\n", CODCON, DR[1]);
                        TA.ExecutarSQLNonQuery(comando2, CODCON, DR[1]);
                    }
                }
                else
                {
                    CODCON = DR["CODCON"].ToString();
                    if((Contador < 13) && (Contador > 0))
                        Relatorio += string.Format("OK     {0} - {1}\r\n", CODCON, Contador);
                    Contador = 1;
                }
            }
            Clipboard.SetText(Relatorio);
            MessageBox.Show("Terminado");
        }

        private void button3_Click(object sender, EventArgs e)
        {
            cAcumulado cAcumulado1 = new cAcumulado();
            if ((lookupCondominio_F1.CON_sel != -1) && (cAcumulado1.publicah12(lookupCondominio_F1.CON_sel)))
                MessageBox.Show("ok");                                            
        }

        

        private void cBotaoImpInad_clicado(object sender, dllBotao.BotaoArgs args)
        {

            Boletos.Boleto.Impresso.ImpInadUnidades Impresso = new Boletos.Boleto.Impresso.ImpInadUnidades(AbstratosNeon.ABS_Condominio.GetCondominio(lookupCondominio_F1.CON_sel), dataF.DateTime);
            cBotaoImpInad.Impresso = Impresso;
        }
    }
}

