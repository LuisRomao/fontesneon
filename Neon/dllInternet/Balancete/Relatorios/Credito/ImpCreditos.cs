﻿using System;
using System.Collections.Generic;
using DevExpress.XtraReports.UI;
using Balancete.GrBalancete;
using Framework.objetosNeon;

namespace Balancete.Relatorios.Credito
{    
    /// <summary>
    /// Impresso Créditos
    /// </summary>
    public partial class ImpCreditos : dllImpresso.ImpLogoCond
    {

        balancete balancete1;

        private AgrupadorBalancete.solicitaAgrupamento ClassificacaoCarregada;

        /// <summary>
        /// Construtor padrão. Não utilizar
        /// </summary>
        public ImpCreditos()
        {
            InitializeComponent();
        }

        private AgrupadorBalancete.solicitaAgrupamento solicitacao;

        /// <summary>
        /// Construtor
        /// </summary>
        /// <param name="_balancete"></param>
        /// <param name="_solicitacao"></param>
        public ImpCreditos(balancete _balancete, AgrupadorBalancete.solicitaAgrupamento _solicitacao = null)
            : this()
        {
            //InitializeComponent();
            //solicitacao = _solicitacao ?? new AgrupadorBalancete.solicitaAgrupamento(AgrupadorBalancete.TipoClassificacaoDesp.PlanoContas);
            balancete1 = _balancete;
            solicitacao = _solicitacao ?? balancete1.AgrupamentoUsarBalancete;
            PRentabilidade.Value = balancete1.TRentabilidade;
            SetNome(balancete1.rowCON.CONNome);
            xrTitulo.Text = string.Format("DÉBITOS / CRÉDITOS\r\n({0:dd/MM/yyyy} a {1:dd/MM/yyyy})", balancete1.DataI, balancete1.DataF);
            //ClassificacaoCarregada = (balancete.TipoClassificacaoDesp)AgrupaDebitos.Value;             
            ClassificacaoCarregada = solicitacao;
            dCreditos1 = balancete1.DCreditos1;
            DetailReport.DataSource = balancete1.DCreditos1;
            DetailReport1.DataSource = balancete1.DCreditos1;
            DetailReport2.DataSource = balancete1.DCreditos1;
            balancete1.CarregarDadosBalancete(ClassificacaoCarregada, false);
            bool ComSuperGrupo = false;
            string SuperGrupoUnico = "";
            foreach (dCreditos.DebitosRow rowEmTeste in balancete1.DCreditos1.Debitos)
            {
                if (SuperGrupoUnico != rowEmTeste.SuperGrupo)
                {
                    if (SuperGrupoUnico == "")
                        SuperGrupoUnico = rowEmTeste.SuperGrupo;
                    else
                    {
                        ComSuperGrupo = true;
                        break;
                    }
                }
            }
            GroupHeaderDebSuper.Visible = ComSuperGrupo;
            ReportHeader1.PageBreak = (balancete1.DCreditos1.Creditos.Count > 18) ? DevExpress.XtraReports.UI.PageBreak.BeforeBand : DevExpress.XtraReports.UI.PageBreak.None;
            //dCreditos1.Debitos.DefaultView.Sort = "Ordem";
            //balancete1.usarBlocos = false;

            ImpDetCredito Filhote = ((ImpDetCredito)xrSubreport1.ReportSource);
            Filhote.DataSource = balancete1.DCreditos1; //dCreditos1;
            Filhote.DataI.Value = balancete1.DataI;
            Filhote.DataF.Value = balancete1.DataF;
            Filhote.Tipo.Value = 1;
            if (balancete1.usarBlocos)
                Filhote.AjustaModelo(ImpDetCredito.modelos.numero_bloco);
            else
                Filhote.AjustaModelo(ImpDetCredito.modelos.numero);
            Filhote = ((ImpDetCredito)xrSubreport2.ReportSource);
            Filhote.DataSource = balancete1.DCreditos1; //dCreditos1;
            Filhote.DataI.Value = balancete1.DataI;
            Filhote.DataF.Value = balancete1.DataF;
            Filhote.Tipo.Value = 2;
            if (balancete1.usarBlocos)
                Filhote.AjustaModelo(ImpDetCredito.modelos.numero_bloco_competencia);
            else
                Filhote.AjustaModelo(ImpDetCredito.modelos.numero_competencia);
            Filhote = ((ImpDetCredito)xrSubreport3.ReportSource);
            Filhote.DataSource = balancete1.DCreditos1; //dCreditos1;
            Filhote.DataI.Value = balancete1.DataI;
            Filhote.DataF.Value = balancete1.DataF;
            Filhote.Tipo.Value = 3;
            if (balancete1.usarBlocos)
                Filhote.AjustaModelo(ImpDetCredito.modelos.numero_bloco_competencia);
            else
                Filhote.AjustaModelo(ImpDetCredito.modelos.numero_competencia);

        }

        private void xrLabel3_Draw(object sender, DrawEventArgs e)
        {
            Degrade(sender, e);
        }

        /*
        public void ImplantaRTF(string RTF)
        {            
            if (RTF == null)
                return;

            XRControl Pai = ReportFooter1;
            DevExpress.XtraReports.UI.XRRichText nXRRichText = new DevExpress.XtraReports.UI.XRRichText();
            Pai.Controls.Add(nXRRichText);
            nXRRichText.Dpi = 254F;
            //nXRRichText.Font = new System.Drawing.Font("Times New Roman", 9.75F);
            nXRRichText.LocationFloat = new DevExpress.Utils.PointFloat(0, Pai.HeightF);
            Pai.Height = Pai.Height + 10;
            nXRRichText.Name = "xrRichText_dinamico";
            //this.xrRichText1.SerializableRtfString = resources.GetString("xrRichText1.SerializableRtfString");
            nXRRichText.SizeF = new System.Drawing.SizeF(1799F, 1);
            nXRRichText.Visible = true;
            nXRRichText.Rtf = RTF;
        }
        */

        /// <summary>
        /// Cálculos
        /// </summary>
        public void Calculos()
        {
            
            //AgrupadorBalancete.solicitaAgrupamento solicitacao = new AgrupadorBalancete.solicitaAgrupamento(AgrupadorBalancete.TipoClassificacaoDesp.PlanoContas);
            balancete1.CarregarDadosBalancete(solicitacao, false);
            decimal[] Vx = new decimal[] { 0, 0, 0, 0 };
            foreach (dCreditos.CreditosRow rowC in dCreditos1.Creditos)
                Vx[0] += rowC.Total;
            foreach (dCreditos.DebitosRow rowD in dCreditos1.Debitos)
                Vx[1] += rowD.Valor;
            foreach (dCreditos.ContasFisicasRow rowCo in dCreditos1.ContasFisicas)
            {
                Vx[2] += rowCo.SaldoI;
                Vx[3] += rowCo.SaldoF;
            }
            V1.Value = Vx[0];
            V2.Value = Vx[1];
            V3.Value = Vx[2];
            V4.Value = Vx[3];
            PTotalGeral.Value = balancete1.TRentabilidade + Vx[0];
            if (balancete1.TRentabilidade == 0)
            {
                xrRentabilidade.Visible = xrRentabilidadeValor.Visible = xrMarcador3.Visible = xrTotalGeral.Visible = xrTotalGeralValor.Visible = false;
                ReportFooter1.HeightF -= xrRentabilidade.HeightF;
                ReportFooter1.HeightF -= xrTotalGeral.HeightF;
            }
        }

        private void ImpCreditos_BeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e)
        {
            Calculos();
        }

        private void xrSubreport1_BeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e)
        {
            ImpDetCredito Filhote1 = ((ImpDetCredito)((XRSubreport)sender).ReportSource);                       
            System.Data.DataRowView DRV = (System.Data.DataRowView)DetailReport2.GetCurrentRow();
            if ((DRV == null) || (DRV.Row == null))
                return;
            dCreditos.CreditosRow rowMae = (dCreditos.CreditosRow)DRV.Row;            
            Filhote1.descricao.Value = rowMae.Descricao;
            if ((int)Filhote1.Tipo.Value == 1)
                Filhote1.Visible = (rowMae.T1);
            if ((int)Filhote1.Tipo.Value == 2)
                Filhote1.Visible = (rowMae.T2);
            if ((int)Filhote1.Tipo.Value == 3)
                Filhote1.Visible = (rowMae.T3);
        }
        
    }
}
