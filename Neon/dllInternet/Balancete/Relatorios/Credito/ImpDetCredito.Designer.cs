﻿namespace Balancete.Relatorios.Credito
{
    partial class ImpDetCredito
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            DevExpress.XtraPrinting.Shape.ShapeArrow shapeArrow1 = new DevExpress.XtraPrinting.Shape.ShapeArrow();
            DevExpress.XtraReports.UI.XRSummary xrSummary1 = new DevExpress.XtraReports.UI.XRSummary();
            this.Detail = new DevExpress.XtraReports.UI.DetailBand();
            this.xrShapeSeta = new DevExpress.XtraReports.UI.XRShape();
            this.xrPanelQuadrinho = new DevExpress.XtraReports.UI.XRPanel();
            this.QValor = new DevExpress.XtraReports.UI.XRLabel();
            this.Qcomp = new DevExpress.XtraReports.UI.XRLabel();
            this.QApt = new DevExpress.XtraReports.UI.XRLabel();
            this.TopMargin = new DevExpress.XtraReports.UI.TopMarginBand();
            this.BottomMargin = new DevExpress.XtraReports.UI.BottomMarginBand();
            this.GroupHeader1 = new DevExpress.XtraReports.UI.GroupHeaderBand();
            this.xrLabel1 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel3 = new DevExpress.XtraReports.UI.XRLabel();
            this.descricao = new DevExpress.XtraReports.Parameters.Parameter();
            this.dCreditos1 = new Balancete.Relatorios.Credito.dCreditos();
            this.Tipo = new DevExpress.XtraReports.Parameters.Parameter();
            this.DataI = new DevExpress.XtraReports.Parameters.Parameter();
            this.DataF = new DevExpress.XtraReports.Parameters.Parameter();
            this.GroupHeader2 = new DevExpress.XtraReports.UI.GroupHeaderBand();
            this.xrLabel4 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel2 = new DevExpress.XtraReports.UI.XRLabel();
            ((System.ComponentModel.ISupportInitialize)(this.dCreditos1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            // 
            // Detail
            // 
            this.Detail.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrShapeSeta,
            this.xrPanelQuadrinho});
            this.Detail.HeightF = 18F;
            this.Detail.MultiColumn.ColumnCount = 4;
            this.Detail.MultiColumn.Layout = DevExpress.XtraPrinting.ColumnLayout.AcrossThenDown;
            this.Detail.MultiColumn.Mode = DevExpress.XtraReports.UI.MultiColumnMode.UseColumnCount;
            this.Detail.Name = "Detail";
            this.Detail.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
            this.Detail.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrShapeSeta
            // 
            this.xrShapeSeta.Angle = 270;
            this.xrShapeSeta.FillColor = System.Drawing.Color.DarkGray;
            this.xrShapeSeta.LocationFloat = new DevExpress.Utils.PointFloat(153F, 6F);
            this.xrShapeSeta.Name = "xrShapeSeta";
            this.xrShapeSeta.Shape = shapeArrow1;
            this.xrShapeSeta.SizeF = new System.Drawing.SizeF(15F, 9.999999F);
            this.xrShapeSeta.BeforePrint += new System.Drawing.Printing.PrintEventHandler(this.xrShapeSeta_BeforePrint);
            // 
            // xrPanelQuadrinho
            // 
            this.xrPanelQuadrinho.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrPanelQuadrinho.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.QValor,
            this.Qcomp,
            this.QApt});
            this.xrPanelQuadrinho.LocationFloat = new DevExpress.Utils.PointFloat(0F, 2.000002F);
            this.xrPanelQuadrinho.Name = "xrPanelQuadrinho";
            this.xrPanelQuadrinho.SizeF = new System.Drawing.SizeF(115.3462F, 16F);
            this.xrPanelQuadrinho.StylePriority.UseBorders = false;
            // 
            // QValor
            // 
            this.QValor.BackColor = System.Drawing.Color.Transparent;
            this.QValor.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.QValor.CanGrow = false;
            this.QValor.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "Mes.Valor", "{0:n2}")});
            this.QValor.Font = new System.Drawing.Font("Times New Roman", 6.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.QValor.LocationFloat = new DevExpress.Utils.PointFloat(69.93674F, 5.000012F);
            this.QValor.Name = "QValor";
            this.QValor.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.QValor.SizeF = new System.Drawing.SizeF(43.51488F, 8.999996F);
            this.QValor.StylePriority.UseBackColor = false;
            this.QValor.StylePriority.UseBorders = false;
            this.QValor.StylePriority.UseFont = false;
            this.QValor.StylePriority.UseTextAlignment = false;
            this.QValor.Text = "QValor";
            this.QValor.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            // 
            // Qcomp
            // 
            this.Qcomp.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.Qcomp.CanGrow = false;
            this.Qcomp.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "Mes.Competencia")});
            this.Qcomp.Font = new System.Drawing.Font("Times New Roman", 6.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Qcomp.LocationFloat = new DevExpress.Utils.PointFloat(35.00003F, 5.000012F);
            this.Qcomp.Name = "Qcomp";
            this.Qcomp.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.Qcomp.SizeF = new System.Drawing.SizeF(34.9367F, 8.99999F);
            this.Qcomp.StylePriority.UseBorders = false;
            this.Qcomp.StylePriority.UseFont = false;
            this.Qcomp.StylePriority.UseTextAlignment = false;
            this.Qcomp.Text = "Qcomp";
            this.Qcomp.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight;
            // 
            // QApt
            // 
            this.QApt.BackColor = System.Drawing.Color.Transparent;
            this.QApt.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.QApt.CanGrow = false;
            this.QApt.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "Mes.Numero")});
            this.QApt.LocationFloat = new DevExpress.Utils.PointFloat(2.000046F, 2F);
            this.QApt.Name = "QApt";
            this.QApt.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.QApt.SizeF = new System.Drawing.SizeF(33F, 12F);
            this.QApt.StylePriority.UseBackColor = false;
            this.QApt.StylePriority.UseBorders = false;
            this.QApt.Text = "QApt";
            // 
            // TopMargin
            // 
            this.TopMargin.HeightF = 0F;
            this.TopMargin.Name = "TopMargin";
            this.TopMargin.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
            this.TopMargin.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // BottomMargin
            // 
            this.BottomMargin.HeightF = 0F;
            this.BottomMargin.Name = "BottomMargin";
            this.BottomMargin.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
            this.BottomMargin.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // GroupHeader1
            // 
            this.GroupHeader1.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrLabel1,
            this.xrLabel3});
            this.GroupHeader1.GroupUnion = DevExpress.XtraReports.UI.GroupUnion.WithFirstDetail;
            this.GroupHeader1.HeightF = 38.43068F;
            this.GroupHeader1.KeepTogether = true;
            this.GroupHeader1.Level = 1;
            this.GroupHeader1.Name = "GroupHeader1";
            // 
            // xrLabel1
            // 
            this.xrLabel1.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "Mes.Valor")});
            this.xrLabel1.Font = new System.Drawing.Font("Times New Roman", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel1.LocationFloat = new DevExpress.Utils.PointFloat(506.2708F, 14.62863F);
            this.xrLabel1.Name = "xrLabel1";
            this.xrLabel1.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel1.SizeF = new System.Drawing.SizeF(151.0624F, 23F);
            this.xrLabel1.StylePriority.UseFont = false;
            this.xrLabel1.StylePriority.UseTextAlignment = false;
            xrSummary1.FormatString = "R$ {0:n2}";
            xrSummary1.Running = DevExpress.XtraReports.UI.SummaryRunning.Report;
            this.xrLabel1.Summary = xrSummary1;
            this.xrLabel1.Text = "xrLabel1";
            this.xrLabel1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight;
            // 
            // xrLabel3
            // 
            this.xrLabel3.Font = new System.Drawing.Font("Times New Roman", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel3.LocationFloat = new DevExpress.Utils.PointFloat(2.000046F, 14.62863F);
            this.xrLabel3.Name = "xrLabel3";
            this.xrLabel3.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel3.SizeF = new System.Drawing.SizeF(504.2708F, 23F);
            this.xrLabel3.StylePriority.UseFont = false;
            this.xrLabel3.Text = "xrLabel3";
            this.xrLabel3.BeforePrint += new System.Drawing.Printing.PrintEventHandler(this.xrLabel3_BeforePrint);
            // 
            // descricao
            // 
            this.descricao.Description = "descricao";
            this.descricao.Name = "descricao";
            this.descricao.Visible = false;
            // 
            // dCreditos1
            // 
            this.dCreditos1.DataSetName = "dCreditos";
            this.dCreditos1.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // Tipo
            // 
            this.Tipo.Description = "Tipo";
            this.Tipo.Name = "Tipo";
            this.Tipo.Type = typeof(short);
            this.Tipo.ValueInfo = "0";
            // 
            // DataI
            // 
            this.DataI.Description = "Data Inicial";
            this.DataI.Name = "DataI";
            this.DataI.Type = typeof(System.DateTime);
            this.DataI.ValueInfo = "02/28/2013 10:29:40";
            this.DataI.Visible = false;
            // 
            // DataF
            // 
            this.DataF.Description = "Data Final";
            this.DataF.Name = "DataF";
            this.DataF.Type = typeof(System.DateTime);
            this.DataF.ValueInfo = "02/28/2013 10:30:18";
            this.DataF.Visible = false;
            // 
            // GroupHeader2
            // 
            this.GroupHeader2.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrLabel4,
            this.xrLabel2});
            this.GroupHeader2.GroupFields.AddRange(new DevExpress.XtraReports.UI.GroupField[] {
            new DevExpress.XtraReports.UI.GroupField("Bloco", DevExpress.XtraReports.UI.XRColumnSortOrder.Ascending)});
            this.GroupHeader2.GroupUnion = DevExpress.XtraReports.UI.GroupUnion.WholePage;
            this.GroupHeader2.HeightF = 15.62939F;
            this.GroupHeader2.KeepTogether = true;
            this.GroupHeader2.Name = "GroupHeader2";
            // 
            // xrLabel4
            // 
            this.xrLabel4.Font = new System.Drawing.Font("Times New Roman", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel4.LocationFloat = new DevExpress.Utils.PointFloat(2.000046F, 2.629387F);
            this.xrLabel4.Name = "xrLabel4";
            this.xrLabel4.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel4.SizeF = new System.Drawing.SizeF(37.6214F, 13F);
            this.xrLabel4.StylePriority.UseFont = false;
            this.xrLabel4.Text = "Bloco:";
            // 
            // xrLabel2
            // 
            this.xrLabel2.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel2.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "Mes.NomeBloco")});
            this.xrLabel2.LocationFloat = new DevExpress.Utils.PointFloat(39.62159F, 2.629387F);
            this.xrLabel2.Name = "xrLabel2";
            this.xrLabel2.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel2.SizeF = new System.Drawing.SizeF(649.3784F, 13F);
            this.xrLabel2.StylePriority.UseBorders = false;
            this.xrLabel2.Text = "xrLabel2";
            // 
            // ImpDetCredito
            // 
            this.Bands.AddRange(new DevExpress.XtraReports.UI.Band[] {
            this.Detail,
            this.TopMargin,
            this.BottomMargin,
            this.GroupHeader1,
            this.GroupHeader2});
            this.DataMember = "Mes";
            this.DataSource = this.dCreditos1;
            this.FilterString = "[Descricao] = ?descricao And [Tipo] = ?Tipo And Not IsNullOrEmpty([Numero])";
            this.Margins = new System.Drawing.Printing.Margins(0, 151, 0, 0);
            this.Parameters.AddRange(new DevExpress.XtraReports.Parameters.Parameter[] {
            this.descricao,
            this.Tipo,
            this.DataI,
            this.DataF});
            this.SnapGridSize = 3F;
            this.Version = "12.2";
            ((System.ComponentModel.ISupportInitialize)(this.dCreditos1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();

        }

        #endregion

        private DevExpress.XtraReports.UI.TopMarginBand TopMargin;
        private DevExpress.XtraReports.UI.BottomMarginBand BottomMargin;
        private DevExpress.XtraReports.UI.XRShape xrShapeSeta;
        private DevExpress.XtraReports.UI.XRPanel xrPanelQuadrinho;
        private DevExpress.XtraReports.UI.GroupHeaderBand GroupHeader1;
        private DevExpress.XtraReports.UI.XRLabel xrLabel3;
        /// <summary>
        /// 
        /// </summary>
        public DevExpress.XtraReports.Parameters.Parameter descricao;
        private dCreditos dCreditos1;
        /// <summary>
        /// 
        /// </summary>
        public DevExpress.XtraReports.UI.DetailBand Detail;
        /// <summary>
        /// 
        /// </summary>
        public DevExpress.XtraReports.Parameters.Parameter Tipo;
        private DevExpress.XtraReports.UI.XRLabel QApt;
        /// <summary>
        /// 
        /// </summary>
        public DevExpress.XtraReports.Parameters.Parameter DataI;
        /// <summary>
        /// 
        /// </summary>
        public DevExpress.XtraReports.Parameters.Parameter DataF;
        private DevExpress.XtraReports.UI.XRLabel QValor;
        private DevExpress.XtraReports.UI.XRLabel Qcomp;
        private DevExpress.XtraReports.UI.GroupHeaderBand GroupHeader2;
        private DevExpress.XtraReports.UI.XRLabel xrLabel1;
        private DevExpress.XtraReports.UI.XRLabel xrLabel4;
        private DevExpress.XtraReports.UI.XRLabel xrLabel2;
    }
}
