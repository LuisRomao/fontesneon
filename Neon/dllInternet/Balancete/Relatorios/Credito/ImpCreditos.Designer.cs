﻿namespace Balancete.Relatorios.Credito
{
    partial class ImpCreditos
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            DevExpress.XtraReports.Parameters.StaticListLookUpSettings staticListLookUpSettings1 = new DevExpress.XtraReports.Parameters.StaticListLookUpSettings();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(ImpCreditos));
            DevExpress.XtraReports.UI.XRSummary xrSummary1 = new DevExpress.XtraReports.UI.XRSummary();
            DevExpress.XtraReports.UI.XRSummary xrSummary2 = new DevExpress.XtraReports.UI.XRSummary();
            DevExpress.XtraReports.UI.XRSummary xrSummary3 = new DevExpress.XtraReports.UI.XRSummary();
            DevExpress.XtraReports.UI.XRSummary xrSummary4 = new DevExpress.XtraReports.UI.XRSummary();
            DevExpress.XtraReports.UI.XRSummary xrSummary5 = new DevExpress.XtraReports.UI.XRSummary();
            DevExpress.XtraReports.UI.XRSummary xrSummary6 = new DevExpress.XtraReports.UI.XRSummary();
            DevExpress.XtraReports.UI.XRSummary xrSummary7 = new DevExpress.XtraReports.UI.XRSummary();
            this.DetDebitos = new DevExpress.XtraReports.Parameters.Parameter();
            this.Marcadores = new DevExpress.XtraReports.Parameters.Parameter();
            this.V1 = new DevExpress.XtraReports.Parameters.Parameter();
            this.V2 = new DevExpress.XtraReports.Parameters.Parameter();
            this.V3 = new DevExpress.XtraReports.Parameters.Parameter();
            this.V4 = new DevExpress.XtraReports.Parameters.Parameter();
            this.PTotalGeral = new DevExpress.XtraReports.Parameters.Parameter();
            this.PRentabilidade = new DevExpress.XtraReports.Parameters.Parameter();
            this.RuleDetalheDebitos = new DevExpress.XtraReports.UI.FormattingRule();
            this.RuleNDetalheDebitos = new DevExpress.XtraReports.UI.FormattingRule();
            this.AgrupaDebitos = new DevExpress.XtraReports.Parameters.Parameter();
            this.TituloSubDetalhe = new DevExpress.XtraReports.UI.XRControlStyle();
            this.xrCrossBandBox1 = new DevExpress.XtraReports.UI.XRCrossBandBox();
            this.xrLine2 = new DevExpress.XtraReports.UI.XRLine();
            this.ParLinhas = new DevExpress.XtraReports.Parameters.Parameter();
            this.Fraco = new DevExpress.XtraReports.UI.XRControlStyle();
            this.Forte = new DevExpress.XtraReports.UI.XRControlStyle();
            this.marcadorpequeno = new DevExpress.XtraReports.UI.XRControlStyle();
            this.OcultaQuadradinhos = new DevExpress.XtraReports.UI.FormattingRule();
            this.Quadradinhos = new DevExpress.XtraReports.Parameters.Parameter();
            this.TotalNegrito = new DevExpress.XtraReports.UI.XRControlStyle();
            this.DetailReport = new DevExpress.XtraReports.UI.DetailReportBand();
            this.Detail1 = new DevExpress.XtraReports.UI.DetailBand();
            this.xrTable2 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow2 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell10 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell9 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell4 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell5 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell6 = new DevExpress.XtraReports.UI.XRTableCell();
            this.ReportHeader = new DevExpress.XtraReports.UI.ReportHeaderBand();
            this.xrLabel4 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTable1 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow1 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell8 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell1 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell7 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell2 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell3 = new DevExpress.XtraReports.UI.XRTableCell();
            this.ReportFooter1 = new DevExpress.XtraReports.UI.ReportFooterBand();
            this.xrRichTextCred = new DevExpress.XtraReports.UI.XRRichText();
            this.xrMarcador3 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTotalGeralValor = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTotalGeral = new DevExpress.XtraReports.UI.XRLabel();
            this.xrRentabilidadeValor = new DevExpress.XtraReports.UI.XRLabel();
            this.xrRentabilidade = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel12 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrSubTotal = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel3 = new DevExpress.XtraReports.UI.XRLabel();
            this.dCreditos1 = new Balancete.Relatorios.Credito.dCreditos();
            this.DetailReport1 = new DevExpress.XtraReports.UI.DetailReportBand();
            this.Detail2 = new DevExpress.XtraReports.UI.DetailBand();
            this.xrTable4 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow4 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell14 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell16 = new DevExpress.XtraReports.UI.XRTableCell();
            this.ReportHeader1 = new DevExpress.XtraReports.UI.ReportHeaderBand();
            this.xrLabel5 = new DevExpress.XtraReports.UI.XRLabel();
            this.ReportFooter = new DevExpress.XtraReports.UI.ReportFooterBand();
            this.xrRichTextDeb = new DevExpress.XtraReports.UI.XRRichText();
            this.xrLabel13 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel9 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel6 = new DevExpress.XtraReports.UI.XRLabel();
            this.GroupHeader1 = new DevExpress.XtraReports.UI.GroupHeaderBand();
            this.xrLabel8 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel7 = new DevExpress.XtraReports.UI.XRLabel();
            this.GroupFooter1 = new DevExpress.XtraReports.UI.GroupFooterBand();
            this.xrLine1 = new DevExpress.XtraReports.UI.XRLine();
            this.GroupHeaderDebSuper = new DevExpress.XtraReports.UI.GroupHeaderBand();
            this.xrPanel1 = new DevExpress.XtraReports.UI.XRPanel();
            this.xrLabel17 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel2 = new DevExpress.XtraReports.UI.XRLabel();
            this.DetailReport2 = new DevExpress.XtraReports.UI.DetailReportBand();
            this.Detail3 = new DevExpress.XtraReports.UI.DetailBand();
            this.xrSubreport3 = new DevExpress.XtraReports.UI.XRSubreport();
            this.xrSubreport2 = new DevExpress.XtraReports.UI.XRSubreport();
            this.xrSubreport1 = new DevExpress.XtraReports.UI.XRSubreport();
            this.GroupHeader2 = new DevExpress.XtraReports.UI.GroupHeaderBand();
            this.xrLine3 = new DevExpress.XtraReports.UI.XRLine();
            this.xrLabel10 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel11 = new DevExpress.XtraReports.UI.XRLabel();
            this.ReportFooter2 = new DevExpress.XtraReports.UI.ReportFooterBand();
            this.xrLabel15 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel14 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel16 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrRichTextQuad = new DevExpress.XtraReports.UI.XRRichText();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrRichTextCred)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dCreditos1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrRichTextDeb)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrRichTextQuad)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            // 
            // xrTitulo
            // 
            this.xrTitulo.Multiline = true;
            this.xrTitulo.Text = "Créditos / Débitos (1/1/2013 a 1/2/2013)";
            // 
            // PageHeader
            // 
            this.PageHeader.BorderDashStyle = DevExpress.XtraPrinting.BorderDashStyle.Dot;
            this.PageHeader.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrLine2});
            this.PageHeader.HeightF = 382.6673F;
            this.PageHeader.StylePriority.UseBackColor = false;
            this.PageHeader.StylePriority.UseBorderColor = false;
            this.PageHeader.StylePriority.UseBorderDashStyle = false;
            this.PageHeader.StylePriority.UseBorders = false;
            this.PageHeader.Controls.SetChildIndex(this.xrLine2, 0);
            this.PageHeader.Controls.SetChildIndex(this.xrTitulo, 0);
            // 
            // Detail
            // 
            this.Detail.Expanded = false;
            this.Detail.HeightF = 42.40582F;
            // 
            // DetDebitos
            // 
            this.DetDebitos.Description = "Detalhar os débitos";
            this.DetDebitos.Name = "DetDebitos";
            this.DetDebitos.Type = typeof(bool);
            this.DetDebitos.ValueInfo = "True";
            // 
            // Marcadores
            // 
            this.Marcadores.Description = "Marcadores";
            this.Marcadores.Name = "Marcadores";
            this.Marcadores.Type = typeof(bool);
            this.Marcadores.ValueInfo = "True";
            // 
            // V1
            // 
            this.V1.Description = "V1";
            this.V1.Name = "V1";
            this.V1.Type = typeof(decimal);
            this.V1.ValueInfo = "0";
            this.V1.Visible = false;
            // 
            // V2
            // 
            this.V2.Description = "V2";
            this.V2.Name = "V2";
            this.V2.Type = typeof(decimal);
            this.V2.ValueInfo = "0";
            this.V2.Visible = false;
            // 
            // V3
            // 
            this.V3.Description = "V3";
            this.V3.Name = "V3";
            this.V3.Type = typeof(decimal);
            this.V3.ValueInfo = "0";
            this.V3.Visible = false;
            // 
            // V4
            // 
            this.V4.Description = "V4";
            this.V4.Name = "V4";
            this.V4.Type = typeof(decimal);
            this.V4.ValueInfo = "0";
            this.V4.Visible = false;
            // 
            // PTotalGeral
            // 
            this.PTotalGeral.Name = "PTotalGeral";
            this.PTotalGeral.Type = typeof(decimal);
            this.PTotalGeral.ValueInfo = "0";
            this.PTotalGeral.Visible = false;
            // 
            // PRentabilidade
            // 
            this.PRentabilidade.Name = "PRentabilidade";
            this.PRentabilidade.Type = typeof(decimal);
            this.PRentabilidade.ValueInfo = "0";
            this.PRentabilidade.Visible = false;
            // 
            // RuleDetalheDebitos
            // 
            this.RuleDetalheDebitos.Condition = "[Parameters.DetDebitos] == True";
            // 
            // 
            // 
            this.RuleDetalheDebitos.Formatting.Visible = DevExpress.Utils.DefaultBoolean.True;
            this.RuleDetalheDebitos.Name = "RuleDetalheDebitos";
            // 
            // RuleNDetalheDebitos
            // 
            this.RuleNDetalheDebitos.Condition = "[Parameters.DetDebitos] == False";
            // 
            // 
            // 
            this.RuleNDetalheDebitos.Formatting.Visible = DevExpress.Utils.DefaultBoolean.False;
            this.RuleNDetalheDebitos.Name = "RuleNDetalheDebitos";
            // 
            // AgrupaDebitos
            // 
            this.AgrupaDebitos.Description = "Agrupamento de débitos";
            staticListLookUpSettings1.LookUpValues.Add(new DevExpress.XtraReports.Parameters.LookUpValue(0, "Plano de contas"));
            staticListLookUpSettings1.LookUpValues.Add(new DevExpress.XtraReports.Parameters.LookUpValue(1, "Grupos"));
            staticListLookUpSettings1.LookUpValues.Add(new DevExpress.XtraReports.Parameters.LookUpValue(2, "Contas Lógicas"));
            this.AgrupaDebitos.LookUpSettings = staticListLookUpSettings1;
            this.AgrupaDebitos.Name = "AgrupaDebitos";
            this.AgrupaDebitos.Type = typeof(int);
            this.AgrupaDebitos.ValueInfo = "0";
            this.AgrupaDebitos.Visible = false;
            // 
            // TituloSubDetalhe
            // 
            this.TituloSubDetalhe.Font = new System.Drawing.Font("Times New Roman", 12.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TituloSubDetalhe.Name = "TituloSubDetalhe";
            // 
            // xrCrossBandBox1
            // 
            this.xrCrossBandBox1.BorderWidth = 2F;
            this.xrCrossBandBox1.Dpi = 254F;
            this.xrCrossBandBox1.EndBand = null;
            this.xrCrossBandBox1.EndPointFloat = new DevExpress.Utils.PointFloat(102.977F, 90.48637F);
            this.xrCrossBandBox1.LocationFloat = new DevExpress.Utils.PointFloat(102.977F, 0F);
            this.xrCrossBandBox1.Name = "xrCrossBandBox1";
            this.xrCrossBandBox1.StartBand = null;
            this.xrCrossBandBox1.StartPointFloat = new DevExpress.Utils.PointFloat(102.977F, 0F);
            this.xrCrossBandBox1.WidthF = 1600.246F;
            // 
            // xrLine2
            // 
            this.xrLine2.Dpi = 254F;
            this.xrLine2.ForeColor = System.Drawing.Color.Gray;
            this.xrLine2.LineStyle = System.Drawing.Drawing2D.DashStyle.Dash;
            this.xrLine2.LocationFloat = new DevExpress.Utils.PointFloat(0F, 310.8469F);
            this.xrLine2.Name = "xrLine2";
            this.xrLine2.SizeF = new System.Drawing.SizeF(1799F, 5F);
            this.xrLine2.StylePriority.UseBorderColor = false;
            this.xrLine2.StylePriority.UseForeColor = false;
            // 
            // ParLinhas
            // 
            this.ParLinhas.Description = "Linhas";
            this.ParLinhas.Name = "ParLinhas";
            this.ParLinhas.Type = typeof(bool);
            this.ParLinhas.ValueInfo = "True";
            // 
            // Fraco
            // 
            this.Fraco.Font = new System.Drawing.Font("Times New Roman", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Fraco.Name = "Fraco";
            // 
            // Forte
            // 
            this.Forte.Font = new System.Drawing.Font("Times New Roman", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Forte.Name = "Forte";
            // 
            // marcadorpequeno
            // 
            this.marcadorpequeno.Font = new System.Drawing.Font("Times New Roman", 6.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.marcadorpequeno.Name = "marcadorpequeno";
            // 
            // OcultaQuadradinhos
            // 
            this.OcultaQuadradinhos.Condition = "[Parameters.Quadradinhos] == False";
            // 
            // 
            // 
            this.OcultaQuadradinhos.Formatting.Visible = DevExpress.Utils.DefaultBoolean.False;
            this.OcultaQuadradinhos.Name = "OcultaQuadradinhos";
            // 
            // Quadradinhos
            // 
            this.Quadradinhos.Description = "Relatório de quadradinhos";
            this.Quadradinhos.Name = "Quadradinhos";
            this.Quadradinhos.Type = typeof(bool);
            this.Quadradinhos.ValueInfo = "False";
            // 
            // TotalNegrito
            // 
            this.TotalNegrito.BackColor = System.Drawing.Color.Silver;
            this.TotalNegrito.Font = new System.Drawing.Font("Times New Roman", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TotalNegrito.Name = "TotalNegrito";
            this.TotalNegrito.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            // 
            // DetailReport
            // 
            this.DetailReport.Bands.AddRange(new DevExpress.XtraReports.UI.Band[] {
            this.Detail1,
            this.ReportHeader,
            this.ReportFooter1});
            this.DetailReport.DataMember = "Creditos";
            this.DetailReport.DataSource = this.dCreditos1;
            this.DetailReport.Dpi = 254F;
            this.DetailReport.FilterString = "[SoPrevisto] = False";
            this.DetailReport.Level = 1;
            this.DetailReport.Name = "DetailReport";
            // 
            // Detail1
            // 
            this.Detail1.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrTable2});
            this.Detail1.Dpi = 254F;
            this.Detail1.HeightF = 52.20068F;
            this.Detail1.Name = "Detail1";
            this.Detail1.SortFields.AddRange(new DevExpress.XtraReports.UI.GroupField[] {
            new DevExpress.XtraReports.UI.GroupField("Descricao", DevExpress.XtraReports.UI.XRColumnSortOrder.Ascending)});
            // 
            // xrTable2
            // 
            this.xrTable2.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrTable2.Dpi = 254F;
            this.xrTable2.LocationFloat = new DevExpress.Utils.PointFloat(87.31248F, 0F);
            this.xrTable2.Name = "xrTable2";
            this.xrTable2.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow2});
            this.xrTable2.SizeF = new System.Drawing.SizeF(1656.292F, 44.47175F);
            this.xrTable2.StylePriority.UseBorders = false;
            // 
            // xrTableRow2
            // 
            this.xrTableRow2.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell10,
            this.xrTableCell9,
            this.xrTableCell4,
            this.xrTableCell5,
            this.xrTableCell6});
            this.xrTableRow2.Dpi = 254F;
            this.xrTableRow2.Name = "xrTableRow2";
            this.xrTableRow2.Weight = 1D;
            // 
            // xrTableCell10
            // 
            this.xrTableCell10.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "Creditos.Descricao")});
            this.xrTableCell10.Dpi = 254F;
            this.xrTableCell10.Font = new System.Drawing.Font("Times New Roman", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrTableCell10.Name = "xrTableCell10";
            this.xrTableCell10.StylePriority.UseBorders = false;
            this.xrTableCell10.StylePriority.UseFont = false;
            this.xrTableCell10.StylePriority.UseTextAlignment = false;
            this.xrTableCell10.Text = "xrTableCell10";
            this.xrTableCell10.Weight = 1.1091402532247119D;
            // 
            // xrTableCell9
            // 
            this.xrTableCell9.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "Creditos.Atrasado", "{0:n2}")});
            this.xrTableCell9.Dpi = 254F;
            this.xrTableCell9.Font = new System.Drawing.Font("Times New Roman", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrTableCell9.Name = "xrTableCell9";
            this.xrTableCell9.StylePriority.UseBorders = false;
            this.xrTableCell9.StylePriority.UseFont = false;
            this.xrTableCell9.StylePriority.UseTextAlignment = false;
            this.xrTableCell9.Text = "xrTableCell9";
            this.xrTableCell9.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomRight;
            this.xrTableCell9.Weight = 0.4592868702326533D;
            // 
            // xrTableCell4
            // 
            this.xrTableCell4.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "Creditos.Periodo", "{0:n2}")});
            this.xrTableCell4.Dpi = 254F;
            this.xrTableCell4.Font = new System.Drawing.Font("Times New Roman", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrTableCell4.Name = "xrTableCell4";
            this.xrTableCell4.StylePriority.UseBorders = false;
            this.xrTableCell4.StylePriority.UseFont = false;
            this.xrTableCell4.StylePriority.UseTextAlignment = false;
            this.xrTableCell4.Text = "xrTableCell4";
            this.xrTableCell4.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomRight;
            this.xrTableCell4.Weight = 0.45713174356428454D;
            // 
            // xrTableCell5
            // 
            this.xrTableCell5.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "Creditos.Antecipado", "{0:n2}")});
            this.xrTableCell5.Dpi = 254F;
            this.xrTableCell5.Font = new System.Drawing.Font("Times New Roman", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrTableCell5.Name = "xrTableCell5";
            this.xrTableCell5.StylePriority.UseBorders = false;
            this.xrTableCell5.StylePriority.UseFont = false;
            this.xrTableCell5.StylePriority.UseTextAlignment = false;
            this.xrTableCell5.Text = "xrTableCell5";
            this.xrTableCell5.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomRight;
            this.xrTableCell5.Weight = 0.43995363398805332D;
            // 
            // xrTableCell6
            // 
            this.xrTableCell6.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "Creditos.Total", "{0:n2}")});
            this.xrTableCell6.Dpi = 254F;
            this.xrTableCell6.Font = new System.Drawing.Font("Times New Roman", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrTableCell6.Name = "xrTableCell6";
            this.xrTableCell6.StylePriority.UseBorders = false;
            this.xrTableCell6.StylePriority.UseFont = false;
            this.xrTableCell6.StylePriority.UseTextAlignment = false;
            this.xrTableCell6.Text = "xrTableCell6";
            this.xrTableCell6.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight;
            this.xrTableCell6.Weight = 0.53448749899029679D;
            // 
            // ReportHeader
            // 
            this.ReportHeader.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrLabel4,
            this.xrTable1});
            this.ReportHeader.Dpi = 254F;
            this.ReportHeader.HeightF = 136.7498F;
            this.ReportHeader.Name = "ReportHeader";
            // 
            // xrLabel4
            // 
            this.xrLabel4.Dpi = 254F;
            this.xrLabel4.Font = new System.Drawing.Font("Times New Roman", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel4.LocationFloat = new DevExpress.Utils.PointFloat(0F, 0F);
            this.xrLabel4.Name = "xrLabel4";
            this.xrLabel4.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrLabel4.SizeF = new System.Drawing.SizeF(474.5154F, 58.42F);
            this.xrLabel4.StyleName = "TituloSubDetalhe";
            this.xrLabel4.StylePriority.UseFont = false;
            this.xrLabel4.StylePriority.UseTextAlignment = false;
            this.xrLabel4.Text = "Créditos";
            this.xrLabel4.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrTable1
            // 
            this.xrTable1.AnchorVertical = DevExpress.XtraReports.UI.VerticalAnchorStyles.Bottom;
            this.xrTable1.Dpi = 254F;
            this.xrTable1.LocationFloat = new DevExpress.Utils.PointFloat(87.3125F, 75.89563F);
            this.xrTable1.Name = "xrTable1";
            this.xrTable1.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow1});
            this.xrTable1.SizeF = new System.Drawing.SizeF(1656.292F, 60.85416F);
            // 
            // xrTableRow1
            // 
            this.xrTableRow1.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell8,
            this.xrTableCell1,
            this.xrTableCell7,
            this.xrTableCell2,
            this.xrTableCell3});
            this.xrTableRow1.Dpi = 254F;
            this.xrTableRow1.Name = "xrTableRow1";
            this.xrTableRow1.Weight = 1D;
            // 
            // xrTableCell8
            // 
            this.xrTableCell8.CanGrow = false;
            this.xrTableCell8.Dpi = 254F;
            this.xrTableCell8.Name = "xrTableCell8";
            this.xrTableCell8.Weight = 1.1091402532247119D;
            // 
            // xrTableCell1
            // 
            this.xrTableCell1.CanGrow = false;
            this.xrTableCell1.Dpi = 254F;
            this.xrTableCell1.Name = "xrTableCell1";
            this.xrTableCell1.StylePriority.UseTextAlignment = false;
            this.xrTableCell1.Text = "Atrasado";
            this.xrTableCell1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight;
            this.xrTableCell1.Weight = 0.45928687023265319D;
            // 
            // xrTableCell7
            // 
            this.xrTableCell7.CanGrow = false;
            this.xrTableCell7.Dpi = 254F;
            this.xrTableCell7.Name = "xrTableCell7";
            this.xrTableCell7.StylePriority.UseTextAlignment = false;
            this.xrTableCell7.Text = "no Período";
            this.xrTableCell7.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight;
            this.xrTableCell7.Weight = 0.4571315224614D;
            // 
            // xrTableCell2
            // 
            this.xrTableCell2.CanGrow = false;
            this.xrTableCell2.Dpi = 254F;
            this.xrTableCell2.Name = "xrTableCell2";
            this.xrTableCell2.StylePriority.UseTextAlignment = false;
            this.xrTableCell2.Text = "Antecipado";
            this.xrTableCell2.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight;
            this.xrTableCell2.Weight = 0.43995385509093815D;
            // 
            // xrTableCell3
            // 
            this.xrTableCell3.CanGrow = false;
            this.xrTableCell3.Dpi = 254F;
            this.xrTableCell3.Name = "xrTableCell3";
            this.xrTableCell3.Weight = 0.53448749899029679D;
            // 
            // ReportFooter1
            // 
            this.ReportFooter1.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrRichTextCred,
            this.xrMarcador3,
            this.xrTotalGeralValor,
            this.xrTotalGeral,
            this.xrRentabilidadeValor,
            this.xrRentabilidade,
            this.xrLabel12,
            this.xrSubTotal,
            this.xrLabel3});
            this.ReportFooter1.Dpi = 254F;
            this.ReportFooter1.HeightF = 209.5651F;
            this.ReportFooter1.KeepTogether = true;
            this.ReportFooter1.Name = "ReportFooter1";
            // 
            // xrRichTextCred
            // 
            this.xrRichTextCred.Dpi = 254F;
            this.xrRichTextCred.Font = new System.Drawing.Font("Times New Roman", 9.75F);
            this.xrRichTextCred.LocationFloat = new DevExpress.Utils.PointFloat(0F, 204.5651F);
            this.xrRichTextCred.Name = "xrRichTextCred";
            this.xrRichTextCred.SerializableRtfString = resources.GetString("xrRichTextCred.SerializableRtfString");
            this.xrRichTextCred.SizeF = new System.Drawing.SizeF(1799F, 5F);
            this.xrRichTextCred.Visible = false;
            // 
            // xrMarcador3
            // 
            this.xrMarcador3.Dpi = 254F;
            this.xrMarcador3.Font = new System.Drawing.Font("Times New Roman", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrMarcador3.LocationFloat = new DevExpress.Utils.PointFloat(1753.139F, 71.61581F);
            this.xrMarcador3.Name = "xrMarcador3";
            this.xrMarcador3.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrMarcador3.SizeF = new System.Drawing.SizeF(45.86133F, 33.42F);
            this.xrMarcador3.StylePriority.UseFont = false;
            this.xrMarcador3.Text = "(3)";
            // 
            // xrTotalGeralValor
            // 
            this.xrTotalGeralValor.BackColor = System.Drawing.Color.Transparent;
            this.xrTotalGeralValor.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding(this.PTotalGeral, "Text", "{0:n2}")});
            this.xrTotalGeralValor.Dpi = 254F;
            this.xrTotalGeralValor.LocationFloat = new DevExpress.Utils.PointFloat(1460.13F, 130.0358F);
            this.xrTotalGeralValor.Name = "xrTotalGeralValor";
            this.xrTotalGeralValor.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTotalGeralValor.SizeF = new System.Drawing.SizeF(283.4747F, 58.42F);
            this.xrTotalGeralValor.StyleName = "TotalNegrito";
            this.xrTotalGeralValor.StylePriority.UseBackColor = false;
            xrSummary1.FormatString = "{0:n2}";
            this.xrTotalGeralValor.Summary = xrSummary1;
            this.xrTotalGeralValor.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight;
            // 
            // xrTotalGeral
            // 
            this.xrTotalGeral.Dpi = 254F;
            this.xrTotalGeral.Font = new System.Drawing.Font("Times New Roman", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrTotalGeral.LocationFloat = new DevExpress.Utils.PointFloat(1015.089F, 130.0358F);
            this.xrTotalGeral.Name = "xrTotalGeral";
            this.xrTotalGeral.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTotalGeral.SizeF = new System.Drawing.SizeF(433.4265F, 58.41999F);
            this.xrTotalGeral.StylePriority.UseFont = false;
            this.xrTotalGeral.StylePriority.UseTextAlignment = false;
            this.xrTotalGeral.Text = "Total geral:";
            this.xrTotalGeral.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight;
            // 
            // xrRentabilidadeValor
            // 
            this.xrRentabilidadeValor.BackColor = System.Drawing.Color.Transparent;
            this.xrRentabilidadeValor.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding(this.PRentabilidade, "Text", "{0:n2}")});
            this.xrRentabilidadeValor.Dpi = 254F;
            this.xrRentabilidadeValor.LocationFloat = new DevExpress.Utils.PointFloat(1460.129F, 71.61577F);
            this.xrRentabilidadeValor.Name = "xrRentabilidadeValor";
            this.xrRentabilidadeValor.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrRentabilidadeValor.SizeF = new System.Drawing.SizeF(283.4756F, 58.41998F);
            this.xrRentabilidadeValor.StyleName = "TotalNegrito";
            this.xrRentabilidadeValor.StylePriority.UseBackColor = false;
            xrSummary2.FormatString = "{0:n2}";
            this.xrRentabilidadeValor.Summary = xrSummary2;
            this.xrRentabilidadeValor.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight;
            // 
            // xrRentabilidade
            // 
            this.xrRentabilidade.Dpi = 254F;
            this.xrRentabilidade.Font = new System.Drawing.Font("Times New Roman", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrRentabilidade.LocationFloat = new DevExpress.Utils.PointFloat(1015.089F, 71.61581F);
            this.xrRentabilidade.Name = "xrRentabilidade";
            this.xrRentabilidade.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrRentabilidade.SizeF = new System.Drawing.SizeF(433.4265F, 58.41999F);
            this.xrRentabilidade.StylePriority.UseFont = false;
            this.xrRentabilidade.StylePriority.UseTextAlignment = false;
            this.xrRentabilidade.Text = "Rentabilidade:";
            this.xrRentabilidade.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight;
            // 
            // xrLabel12
            // 
            this.xrLabel12.Dpi = 254F;
            this.xrLabel12.Font = new System.Drawing.Font("Times New Roman", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel12.LocationFloat = new DevExpress.Utils.PointFloat(1753.139F, 13.19583F);
            this.xrLabel12.Name = "xrLabel12";
            this.xrLabel12.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrLabel12.SizeF = new System.Drawing.SizeF(45.86133F, 33.42F);
            this.xrLabel12.StylePriority.UseFont = false;
            this.xrLabel12.Text = "(1)";
            // 
            // xrSubTotal
            // 
            this.xrSubTotal.Dpi = 254F;
            this.xrSubTotal.Font = new System.Drawing.Font("Times New Roman", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrSubTotal.LocationFloat = new DevExpress.Utils.PointFloat(1015.089F, 13.19582F);
            this.xrSubTotal.Name = "xrSubTotal";
            this.xrSubTotal.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrSubTotal.SizeF = new System.Drawing.SizeF(433.4265F, 58.41999F);
            this.xrSubTotal.StylePriority.UseFont = false;
            this.xrSubTotal.StylePriority.UseTextAlignment = false;
            this.xrSubTotal.Text = "Total de créditos:";
            this.xrSubTotal.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight;
            // 
            // xrLabel3
            // 
            this.xrLabel3.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "Creditos.Total")});
            this.xrLabel3.Dpi = 254F;
            this.xrLabel3.LocationFloat = new DevExpress.Utils.PointFloat(1460.129F, 13.19583F);
            this.xrLabel3.Name = "xrLabel3";
            this.xrLabel3.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrLabel3.SizeF = new System.Drawing.SizeF(283.4749F, 58.41999F);
            this.xrLabel3.StyleName = "TotalNegrito";
            xrSummary3.FormatString = "{0:n2}";
            xrSummary3.Running = DevExpress.XtraReports.UI.SummaryRunning.Report;
            this.xrLabel3.Summary = xrSummary3;
            this.xrLabel3.Text = "xrLabel3";
            this.xrLabel3.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight;
            this.xrLabel3.Draw += new DevExpress.XtraReports.UI.DrawEventHandler(this.xrLabel3_Draw);
            // 
            // dCreditos1
            // 
            this.dCreditos1.DataSetName = "dCreditos";
            this.dCreditos1.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // DetailReport1
            // 
            this.DetailReport1.Bands.AddRange(new DevExpress.XtraReports.UI.Band[] {
            this.Detail2,
            this.ReportHeader1,
            this.ReportFooter,
            this.GroupHeader1,
            this.GroupFooter1,
            this.GroupHeaderDebSuper});
            this.DetailReport1.DataMember = "Debitos";
            this.DetailReport1.DataSource = this.dCreditos1;
            this.DetailReport1.Dpi = 254F;
            this.DetailReport1.Level = 2;
            this.DetailReport1.Name = "DetailReport1";
            // 
            // Detail2
            // 
            this.Detail2.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrTable4});
            this.Detail2.Dpi = 254F;
            this.Detail2.FormattingRules.Add(this.RuleDetalheDebitos);
            this.Detail2.FormattingRules.Add(this.RuleNDetalheDebitos);
            this.Detail2.HeightF = 33.42F;
            this.Detail2.KeepTogether = true;
            this.Detail2.KeepTogetherWithDetailReports = true;
            this.Detail2.Name = "Detail2";
            this.Detail2.SortFields.AddRange(new DevExpress.XtraReports.UI.GroupField[] {
            new DevExpress.XtraReports.UI.GroupField("Ordem", DevExpress.XtraReports.UI.XRColumnSortOrder.Ascending)});
            // 
            // xrTable4
            // 
            this.xrTable4.Dpi = 254F;
            this.xrTable4.LocationFloat = new DevExpress.Utils.PointFloat(287.9224F, 0F);
            this.xrTable4.Name = "xrTable4";
            this.xrTable4.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow4});
            this.xrTable4.SizeF = new System.Drawing.SizeF(1254.5F, 33.41779F);
            // 
            // xrTableRow4
            // 
            this.xrTableRow4.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell14,
            this.xrTableCell16});
            this.xrTableRow4.Dpi = 254F;
            this.xrTableRow4.Name = "xrTableRow4";
            this.xrTableRow4.StylePriority.UseBorders = false;
            this.xrTableRow4.Weight = 0.60590214812095944D;
            // 
            // xrTableCell14
            // 
            this.xrTableCell14.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "Debitos.Descricao")});
            this.xrTableCell14.Dpi = 254F;
            this.xrTableCell14.Font = new System.Drawing.Font("Times New Roman", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrTableCell14.Name = "xrTableCell14";
            this.xrTableCell14.StylePriority.UseFont = false;
            this.xrTableCell14.Text = "xrTableCell14";
            this.xrTableCell14.Weight = 1.7629933603261629D;
            // 
            // xrTableCell16
            // 
            this.xrTableCell16.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "Debitos.Valor", "{0:n2}")});
            this.xrTableCell16.Dpi = 254F;
            this.xrTableCell16.Font = new System.Drawing.Font("Times New Roman", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrTableCell16.Name = "xrTableCell16";
            this.xrTableCell16.StylePriority.UseFont = false;
            this.xrTableCell16.StylePriority.UseTextAlignment = false;
            this.xrTableCell16.Text = "xrTableCell16";
            this.xrTableCell16.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight;
            this.xrTableCell16.Weight = 0.32158584994164313D;
            // 
            // ReportHeader1
            // 
            this.ReportHeader1.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrLabel5});
            this.ReportHeader1.Dpi = 254F;
            this.ReportHeader1.HeightF = 66.87072F;
            this.ReportHeader1.KeepTogether = true;
            this.ReportHeader1.Name = "ReportHeader1";
            // 
            // xrLabel5
            // 
            this.xrLabel5.Dpi = 254F;
            this.xrLabel5.Font = new System.Drawing.Font("Times New Roman", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel5.LocationFloat = new DevExpress.Utils.PointFloat(0F, 0F);
            this.xrLabel5.Name = "xrLabel5";
            this.xrLabel5.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrLabel5.SizeF = new System.Drawing.SizeF(474.5154F, 58.42F);
            this.xrLabel5.StyleName = "TituloSubDetalhe";
            this.xrLabel5.StylePriority.UseFont = false;
            this.xrLabel5.StylePriority.UseTextAlignment = false;
            this.xrLabel5.Text = "Débitos";
            this.xrLabel5.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // ReportFooter
            // 
            this.ReportFooter.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrRichTextDeb,
            this.xrLabel13,
            this.xrLabel9,
            this.xrLabel6});
            this.ReportFooter.Dpi = 254F;
            this.ReportFooter.HeightF = 77.85278F;
            this.ReportFooter.KeepTogether = true;
            this.ReportFooter.Name = "ReportFooter";
            // 
            // xrRichTextDeb
            // 
            this.xrRichTextDeb.Dpi = 254F;
            this.xrRichTextDeb.Font = new System.Drawing.Font("Times New Roman", 9.75F);
            this.xrRichTextDeb.LocationFloat = new DevExpress.Utils.PointFloat(0F, 73.85278F);
            this.xrRichTextDeb.Name = "xrRichTextDeb";
            this.xrRichTextDeb.SerializableRtfString = resources.GetString("xrRichTextDeb.SerializableRtfString");
            this.xrRichTextDeb.SizeF = new System.Drawing.SizeF(1799F, 5F);
            this.xrRichTextDeb.Visible = false;
            // 
            // xrLabel13
            // 
            this.xrLabel13.Dpi = 254F;
            this.xrLabel13.Font = new System.Drawing.Font("Times New Roman", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel13.LocationFloat = new DevExpress.Utils.PointFloat(1753.139F, 10.29535F);
            this.xrLabel13.Name = "xrLabel13";
            this.xrLabel13.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrLabel13.SizeF = new System.Drawing.SizeF(45.86133F, 33.42F);
            this.xrLabel13.StylePriority.UseFont = false;
            this.xrLabel13.Text = "(2)";
            // 
            // xrLabel9
            // 
            this.xrLabel9.BackColor = System.Drawing.Color.Silver;
            this.xrLabel9.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "Debitos.Valor")});
            this.xrLabel9.Dpi = 254F;
            this.xrLabel9.Font = new System.Drawing.Font("Times New Roman", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel9.LocationFloat = new DevExpress.Utils.PointFloat(1489.604F, 10.29535F);
            this.xrLabel9.Name = "xrLabel9";
            this.xrLabel9.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrLabel9.SizeF = new System.Drawing.SizeF(254F, 58.41999F);
            this.xrLabel9.StylePriority.UseBackColor = false;
            this.xrLabel9.StylePriority.UseFont = false;
            this.xrLabel9.StylePriority.UseTextAlignment = false;
            xrSummary4.FormatString = "{0:n2}";
            xrSummary4.Running = DevExpress.XtraReports.UI.SummaryRunning.Report;
            this.xrLabel9.Summary = xrSummary4;
            this.xrLabel9.Text = "xrLabel9";
            this.xrLabel9.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight;
            this.xrLabel9.Draw += new DevExpress.XtraReports.UI.DrawEventHandler(this.xrLabel3_Draw);
            // 
            // xrLabel6
            // 
            this.xrLabel6.Dpi = 254F;
            this.xrLabel6.Font = new System.Drawing.Font("Times New Roman", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel6.LocationFloat = new DevExpress.Utils.PointFloat(1015.089F, 10.29535F);
            this.xrLabel6.Name = "xrLabel6";
            this.xrLabel6.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrLabel6.SizeF = new System.Drawing.SizeF(474.5154F, 58.42F);
            this.xrLabel6.StylePriority.UseFont = false;
            this.xrLabel6.StylePriority.UseTextAlignment = false;
            this.xrLabel6.Text = "Total de débitos:";
            this.xrLabel6.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight;
            // 
            // GroupHeader1
            // 
            this.GroupHeader1.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrLabel8,
            this.xrLabel7});
            this.GroupHeader1.Dpi = 254F;
            this.GroupHeader1.GroupFields.AddRange(new DevExpress.XtraReports.UI.GroupField[] {
            new DevExpress.XtraReports.UI.GroupField("nGrupo", DevExpress.XtraReports.UI.XRColumnSortOrder.Ascending)});
            this.GroupHeader1.GroupUnion = DevExpress.XtraReports.UI.GroupUnion.WithFirstDetail;
            this.GroupHeader1.HeightF = 45.23288F;
            this.GroupHeader1.KeepTogether = true;
            this.GroupHeader1.Name = "GroupHeader1";
            // 
            // xrLabel8
            // 
            this.xrLabel8.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "Debitos.Valor")});
            this.xrLabel8.Dpi = 254F;
            this.xrLabel8.Font = new System.Drawing.Font("Times New Roman", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel8.LocationFloat = new DevExpress.Utils.PointFloat(1348.892F, 0F);
            this.xrLabel8.Name = "xrLabel8";
            this.xrLabel8.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrLabel8.SizeF = new System.Drawing.SizeF(394.7123F, 44.47F);
            this.xrLabel8.StylePriority.UseFont = false;
            this.xrLabel8.StylePriority.UseTextAlignment = false;
            xrSummary5.FormatString = "{0:n2}";
            xrSummary5.Running = DevExpress.XtraReports.UI.SummaryRunning.Group;
            this.xrLabel8.Summary = xrSummary5;
            this.xrLabel8.Text = "xrLabel8";
            this.xrLabel8.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight;
            // 
            // xrLabel7
            // 
            this.xrLabel7.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "Debitos.Grupo")});
            this.xrLabel7.Dpi = 254F;
            this.xrLabel7.Font = new System.Drawing.Font("Times New Roman", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel7.LocationFloat = new DevExpress.Utils.PointFloat(87.31247F, 0F);
            this.xrLabel7.Name = "xrLabel7";
            this.xrLabel7.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrLabel7.SizeF = new System.Drawing.SizeF(1261.58F, 44.47F);
            this.xrLabel7.StylePriority.UseFont = false;
            this.xrLabel7.Text = "xrLabel7";
            // 
            // GroupFooter1
            // 
            this.GroupFooter1.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrLine1});
            this.GroupFooter1.Dpi = 254F;
            this.GroupFooter1.GroupUnion = DevExpress.XtraReports.UI.GroupFooterUnion.WithLastDetail;
            this.GroupFooter1.HeightF = 9.490202F;
            this.GroupFooter1.KeepTogether = true;
            this.GroupFooter1.Name = "GroupFooter1";
            // 
            // xrLine1
            // 
            this.xrLine1.Dpi = 254F;
            this.xrLine1.LineWidth = 3;
            this.xrLine1.LocationFloat = new DevExpress.Utils.PointFloat(87.31249F, 0F);
            this.xrLine1.Name = "xrLine1";
            this.xrLine1.SizeF = new System.Drawing.SizeF(1711.687F, 9.490202F);
            // 
            // GroupHeaderDebSuper
            // 
            this.GroupHeaderDebSuper.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrPanel1});
            this.GroupHeaderDebSuper.Dpi = 254F;
            this.GroupHeaderDebSuper.GroupFields.AddRange(new DevExpress.XtraReports.UI.GroupField[] {
            new DevExpress.XtraReports.UI.GroupField("nSuperGrupo", DevExpress.XtraReports.UI.XRColumnSortOrder.Ascending)});
            this.GroupHeaderDebSuper.GroupUnion = DevExpress.XtraReports.UI.GroupUnion.WithFirstDetail;
            this.GroupHeaderDebSuper.HeightF = 91.01667F;
            this.GroupHeaderDebSuper.KeepTogether = true;
            this.GroupHeaderDebSuper.Level = 1;
            this.GroupHeaderDebSuper.Name = "GroupHeaderDebSuper";
            // 
            // xrPanel1
            // 
            this.xrPanel1.BackColor = System.Drawing.Color.Silver;
            this.xrPanel1.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrLabel17,
            this.xrLabel2});
            this.xrPanel1.Dpi = 254F;
            this.xrPanel1.LocationFloat = new DevExpress.Utils.PointFloat(47.8395F, 0F);
            this.xrPanel1.Name = "xrPanel1";
            this.xrPanel1.SizeF = new System.Drawing.SizeF(1753.161F, 91.01667F);
            this.xrPanel1.StylePriority.UseBackColor = false;
            // 
            // xrLabel17
            // 
            this.xrLabel17.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "Debitos.Valor")});
            this.xrLabel17.Dpi = 254F;
            this.xrLabel17.Font = new System.Drawing.Font("Times New Roman", 13.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel17.LocationFloat = new DevExpress.Utils.PointFloat(1323.976F, 24.99999F);
            this.xrLabel17.Name = "xrLabel17";
            this.xrLabel17.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrLabel17.SizeF = new System.Drawing.SizeF(387.7113F, 44.47F);
            this.xrLabel17.StylePriority.UseFont = false;
            this.xrLabel17.StylePriority.UseTextAlignment = false;
            xrSummary6.FormatString = "{0:n2}";
            xrSummary6.Running = DevExpress.XtraReports.UI.SummaryRunning.Group;
            this.xrLabel17.Summary = xrSummary6;
            this.xrLabel17.Text = "xrLabel8";
            this.xrLabel17.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight;
            // 
            // xrLabel2
            // 
            this.xrLabel2.BackColor = System.Drawing.Color.Transparent;
            this.xrLabel2.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "Debitos.SuperGrupo")});
            this.xrLabel2.Dpi = 254F;
            this.xrLabel2.Font = new System.Drawing.Font("Times New Roman", 16.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel2.ForeColor = System.Drawing.Color.White;
            this.xrLabel2.LocationFloat = new DevExpress.Utils.PointFloat(24.99999F, 0F);
            this.xrLabel2.Name = "xrLabel2";
            this.xrLabel2.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrLabel2.SizeF = new System.Drawing.SizeF(1298.976F, 82.57F);
            this.xrLabel2.StylePriority.UseBackColor = false;
            this.xrLabel2.StylePriority.UseFont = false;
            this.xrLabel2.StylePriority.UseForeColor = false;
            this.xrLabel2.StylePriority.UseTextAlignment = false;
            this.xrLabel2.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // DetailReport2
            // 
            this.DetailReport2.Bands.AddRange(new DevExpress.XtraReports.UI.Band[] {
            this.Detail3,
            this.GroupHeader2,
            this.ReportFooter2});
            this.DetailReport2.DataMember = "Creditos";
            this.DetailReport2.DataSource = this.dCreditos1;
            this.DetailReport2.Dpi = 254F;
            this.DetailReport2.FilterString = "[SoPrevisto] = False";
            this.DetailReport2.FormattingRules.Add(this.OcultaQuadradinhos);
            this.DetailReport2.Level = 0;
            this.DetailReport2.Name = "DetailReport2";
            // 
            // Detail3
            // 
            this.Detail3.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrSubreport3,
            this.xrSubreport2,
            this.xrSubreport1});
            this.Detail3.Dpi = 254F;
            this.Detail3.HeightF = 139.1768F;
            this.Detail3.Name = "Detail3";
            // 
            // xrSubreport3
            // 
            this.xrSubreport3.CanShrink = true;
            this.xrSubreport3.Dpi = 254F;
            this.xrSubreport3.LocationFloat = new DevExpress.Utils.PointFloat(0F, 62.5F);
            this.xrSubreport3.Name = "xrSubreport3";
            this.xrSubreport3.ReportSource = new Balancete.Relatorios.Credito.ImpDetCredito();
            this.xrSubreport3.SizeF = new System.Drawing.SizeF(1799F, 30F);
            this.xrSubreport3.BeforePrint += new System.Drawing.Printing.PrintEventHandler(this.xrSubreport1_BeforePrint);
            // 
            // xrSubreport2
            // 
            this.xrSubreport2.CanShrink = true;
            this.xrSubreport2.Dpi = 254F;
            this.xrSubreport2.LocationFloat = new DevExpress.Utils.PointFloat(0F, 31F);
            this.xrSubreport2.Name = "xrSubreport2";
            this.xrSubreport2.ReportSource = new Balancete.Relatorios.Credito.ImpDetCredito();
            this.xrSubreport2.SizeF = new System.Drawing.SizeF(1799F, 30F);
            this.xrSubreport2.BeforePrint += new System.Drawing.Printing.PrintEventHandler(this.xrSubreport1_BeforePrint);
            // 
            // xrSubreport1
            // 
            this.xrSubreport1.CanShrink = true;
            this.xrSubreport1.Dpi = 254F;
            this.xrSubreport1.LocationFloat = new DevExpress.Utils.PointFloat(0F, 0F);
            this.xrSubreport1.Name = "xrSubreport1";
            this.xrSubreport1.ReportSource = new Balancete.Relatorios.Credito.ImpDetCredito();
            this.xrSubreport1.SizeF = new System.Drawing.SizeF(1799F, 30F);
            this.xrSubreport1.BeforePrint += new System.Drawing.Printing.PrintEventHandler(this.xrSubreport1_BeforePrint);
            // 
            // GroupHeader2
            // 
            this.GroupHeader2.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrLine3,
            this.xrLabel10,
            this.xrLabel11});
            this.GroupHeader2.Dpi = 254F;
            this.GroupHeader2.GroupFields.AddRange(new DevExpress.XtraReports.UI.GroupField[] {
            new DevExpress.XtraReports.UI.GroupField("Descricao", DevExpress.XtraReports.UI.XRColumnSortOrder.Ascending),
            new DevExpress.XtraReports.UI.GroupField("Ordem", DevExpress.XtraReports.UI.XRColumnSortOrder.Ascending)});
            this.GroupHeader2.GroupUnion = DevExpress.XtraReports.UI.GroupUnion.WholePage;
            this.GroupHeader2.HeightF = 111.125F;
            this.GroupHeader2.KeepTogether = true;
            this.GroupHeader2.Name = "GroupHeader2";
            // 
            // xrLine3
            // 
            this.xrLine3.Borders = DevExpress.XtraPrinting.BorderSide.Top;
            this.xrLine3.Dpi = 254F;
            this.xrLine3.LineWidth = 3;
            this.xrLine3.LocationFloat = new DevExpress.Utils.PointFloat(0F, 0F);
            this.xrLine3.Name = "xrLine3";
            this.xrLine3.SizeF = new System.Drawing.SizeF(1799F, 15.00494F);
            this.xrLine3.StylePriority.UseBorders = false;
            // 
            // xrLabel10
            // 
            this.xrLabel10.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "Creditos.Descricao")});
            this.xrLabel10.Dpi = 254F;
            this.xrLabel10.LocationFloat = new DevExpress.Utils.PointFloat(0F, 49.84491F);
            this.xrLabel10.Name = "xrLabel10";
            this.xrLabel10.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrLabel10.SizeF = new System.Drawing.SizeF(1438.457F, 58.42F);
            this.xrLabel10.StyleName = "TituloSubDetalhe";
            this.xrLabel10.StylePriority.UseFont = false;
            this.xrLabel10.Text = "xrLabel10";
            // 
            // xrLabel11
            // 
            this.xrLabel11.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "Creditos.Total", "R$ {0:n2}")});
            this.xrLabel11.Dpi = 254F;
            this.xrLabel11.LocationFloat = new DevExpress.Utils.PointFloat(1438.457F, 49.84491F);
            this.xrLabel11.Name = "xrLabel11";
            this.xrLabel11.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrLabel11.SizeF = new System.Drawing.SizeF(360.5428F, 58.42F);
            this.xrLabel11.StyleName = "TituloSubDetalhe";
            this.xrLabel11.StylePriority.UseFont = false;
            this.xrLabel11.StylePriority.UseTextAlignment = false;
            this.xrLabel11.Text = "xrLabel11";
            this.xrLabel11.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight;
            // 
            // ReportFooter2
            // 
            this.ReportFooter2.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrLabel15,
            this.xrLabel14,
            this.xrLabel16,
            this.xrRichTextQuad});
            this.ReportFooter2.Dpi = 254F;
            this.ReportFooter2.HeightF = 71.43751F;
            this.ReportFooter2.KeepTogether = true;
            this.ReportFooter2.Name = "ReportFooter2";
            this.ReportFooter2.PageBreak = DevExpress.XtraReports.UI.PageBreak.AfterBand;
            // 
            // xrLabel15
            // 
            this.xrLabel15.Dpi = 254F;
            this.xrLabel15.Font = new System.Drawing.Font("Times New Roman", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel15.LocationFloat = new DevExpress.Utils.PointFloat(953.2369F, 0F);
            this.xrLabel15.Name = "xrLabel15";
            this.xrLabel15.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrLabel15.SizeF = new System.Drawing.SizeF(417.3544F, 58.42F);
            this.xrLabel15.StylePriority.UseFont = false;
            this.xrLabel15.Text = "Total de Créditos";
            // 
            // xrLabel14
            // 
            this.xrLabel14.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "Creditos.Total")});
            this.xrLabel14.Dpi = 254F;
            this.xrLabel14.Font = new System.Drawing.Font("Times New Roman", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel14.LocationFloat = new DevExpress.Utils.PointFloat(1387.658F, 0F);
            this.xrLabel14.Name = "xrLabel14";
            this.xrLabel14.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrLabel14.SizeF = new System.Drawing.SizeF(368.4801F, 58.42F);
            this.xrLabel14.StyleName = "TotalNegrito";
            this.xrLabel14.StylePriority.UseFont = false;
            xrSummary7.FormatString = "R$ {0:n2}";
            xrSummary7.Running = DevExpress.XtraReports.UI.SummaryRunning.Report;
            this.xrLabel14.Summary = xrSummary7;
            this.xrLabel14.Text = "xrLabel14";
            // 
            // xrLabel16
            // 
            this.xrLabel16.Dpi = 254F;
            this.xrLabel16.Font = new System.Drawing.Font("Times New Roman", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel16.LocationFloat = new DevExpress.Utils.PointFloat(1756.139F, 0F);
            this.xrLabel16.Name = "xrLabel16";
            this.xrLabel16.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrLabel16.SizeF = new System.Drawing.SizeF(44F, 33.42F);
            this.xrLabel16.StylePriority.UseFont = false;
            this.xrLabel16.Text = "(1)";
            // 
            // xrRichTextQuad
            // 
            this.xrRichTextQuad.Dpi = 254F;
            this.xrRichTextQuad.Font = new System.Drawing.Font("Times New Roman", 9.75F);
            this.xrRichTextQuad.LocationFloat = new DevExpress.Utils.PointFloat(0F, 67.43751F);
            this.xrRichTextQuad.Name = "xrRichTextQuad";
            this.xrRichTextQuad.SerializableRtfString = resources.GetString("xrRichTextQuad.SerializableRtfString");
            this.xrRichTextQuad.SizeF = new System.Drawing.SizeF(1799F, 5F);
            this.xrRichTextQuad.Visible = false;
            // 
            // ImpCreditos
            // 
            this.Bands.AddRange(new DevExpress.XtraReports.UI.Band[] {
            this.Detail,
            this.PageHeader,
            this.DetailReport,
            this.DetailReport1,
            this.DetailReport2});
            this.CrossBandControls.AddRange(new DevExpress.XtraReports.UI.XRCrossBandControl[] {
            this.xrCrossBandBox1});
            this.FormattingRuleSheet.AddRange(new DevExpress.XtraReports.UI.FormattingRule[] {
            this.RuleDetalheDebitos,
            this.RuleNDetalheDebitos,
            this.OcultaQuadradinhos});
            this.Margins = new System.Drawing.Printing.Margins(151, 148, 51, 51);
            this.Parameters.AddRange(new DevExpress.XtraReports.Parameters.Parameter[] {
            this.DetDebitos,
            this.Marcadores,
            this.V1,
            this.V2,
            this.V3,
            this.V4,
            this.AgrupaDebitos,
            this.ParLinhas,
            this.Quadradinhos,
            this.PRentabilidade,
            this.PTotalGeral});
            this.StyleSheet.AddRange(new DevExpress.XtraReports.UI.XRControlStyle[] {
            this.TituloSubDetalhe,
            this.Fraco,
            this.Forte,
            this.marcadorpequeno,
            this.TotalNegrito});
            this.Version = "16.1";
            this.BeforePrint += new System.Drawing.Printing.PrintEventHandler(this.ImpCreditos_BeforePrint);
            this.Controls.SetChildIndex(this.DetailReport2, 0);
            this.Controls.SetChildIndex(this.DetailReport1, 0);
            this.Controls.SetChildIndex(this.DetailReport, 0);
            this.Controls.SetChildIndex(this.PageHeader, 0);
            this.Controls.SetChildIndex(this.Detail, 0);
            ((System.ComponentModel.ISupportInitialize)(this.xrTable2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrRichTextCred)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dCreditos1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrRichTextDeb)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrRichTextQuad)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();

        }

        #endregion

        private DevExpress.XtraReports.UI.XRTable xrTable1;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow1;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell1;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell2;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell3;
        private DevExpress.XtraReports.UI.XRTable xrTable2;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow2;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell4;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell5;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell6;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell8;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell7;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell10;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell9;
        private DevExpress.XtraReports.UI.XRLabel xrLabel3;
        private DevExpress.XtraReports.UI.XRLabel xrSubTotal;
        private DevExpress.XtraReports.UI.DetailReportBand DetailReport;
        private DevExpress.XtraReports.UI.DetailBand Detail1;
        private DevExpress.XtraReports.UI.ReportHeaderBand ReportHeader;
        private DevExpress.XtraReports.UI.ReportFooterBand ReportFooter1;
        private DevExpress.XtraReports.UI.XRLabel xrLabel4;
        private DevExpress.XtraReports.UI.DetailReportBand DetailReport1;
        private DevExpress.XtraReports.UI.DetailBand Detail2;
        private DevExpress.XtraReports.UI.ReportHeaderBand ReportHeader1;
        private DevExpress.XtraReports.UI.XRLabel xrLabel5;
        private DevExpress.XtraReports.UI.ReportFooterBand ReportFooter;
        private DevExpress.XtraReports.UI.XRLabel xrLabel6;
        private DevExpress.XtraReports.UI.XRTable xrTable4;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow4;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell14;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell16;
        private DevExpress.XtraReports.UI.XRLabel xrLabel9;
        private DevExpress.XtraReports.UI.GroupHeaderBand GroupHeader1;
        private DevExpress.XtraReports.UI.XRLabel xrLabel8;
        private DevExpress.XtraReports.UI.XRLabel xrLabel7;
        private DevExpress.XtraReports.UI.GroupFooterBand GroupFooter1;
        private DevExpress.XtraReports.UI.XRLine xrLine1;
        private DevExpress.XtraReports.UI.FormattingRule RuleDetalheDebitos;
        private DevExpress.XtraReports.Parameters.Parameter DetDebitos;
        private DevExpress.XtraReports.UI.FormattingRule RuleNDetalheDebitos;
        private DevExpress.XtraReports.UI.XRLabel xrLabel12;
        private DevExpress.XtraReports.UI.XRLabel xrLabel13;
        private DevExpress.XtraReports.Parameters.Parameter Marcadores;
        private DevExpress.XtraReports.Parameters.Parameter V1;
        private DevExpress.XtraReports.Parameters.Parameter V2;
        private DevExpress.XtraReports.Parameters.Parameter V4;
        private DevExpress.XtraReports.Parameters.Parameter V3;
        private DevExpress.XtraReports.Parameters.Parameter AgrupaDebitos;
        private DevExpress.XtraReports.UI.XRControlStyle TituloSubDetalhe;
        private DevExpress.XtraReports.UI.XRCrossBandBox xrCrossBandBox1;
        private DevExpress.XtraReports.UI.XRLine xrLine2;
        private DevExpress.XtraReports.Parameters.Parameter ParLinhas;
        private DevExpress.XtraReports.UI.XRControlStyle Fraco;
        private DevExpress.XtraReports.UI.XRControlStyle Forte;
        private DevExpress.XtraReports.UI.XRControlStyle marcadorpequeno;
        private DevExpress.XtraReports.UI.DetailReportBand DetailReport2;
        private DevExpress.XtraReports.UI.DetailBand Detail3;
        private DevExpress.XtraReports.UI.XRLabel xrLabel11;
        private DevExpress.XtraReports.UI.XRLabel xrLabel10;
        private DevExpress.XtraReports.UI.XRSubreport xrSubreport1;
        private DevExpress.XtraReports.UI.XRSubreport xrSubreport2;
        private DevExpress.XtraReports.UI.FormattingRule OcultaQuadradinhos;
        private DevExpress.XtraReports.UI.XRLabel xrLabel15;
        private DevExpress.XtraReports.UI.XRLabel xrLabel14;
        private DevExpress.XtraReports.UI.XRLine xrLine3;
        private DevExpress.XtraReports.UI.XRLabel xrLabel16;
        private DevExpress.XtraReports.UI.XRControlStyle TotalNegrito;
        private DevExpress.XtraReports.UI.GroupHeaderBand GroupHeader2;
        private DevExpress.XtraReports.UI.ReportFooterBand ReportFooter2;
        private dCreditos dCreditos1;
        private DevExpress.XtraReports.UI.XRSubreport xrSubreport3;
        private DevExpress.XtraReports.UI.XRLabel xrMarcador3;
        private DevExpress.XtraReports.UI.XRLabel xrTotalGeralValor;
        private DevExpress.XtraReports.Parameters.Parameter PTotalGeral;
        private DevExpress.XtraReports.UI.XRLabel xrTotalGeral;
        private DevExpress.XtraReports.UI.XRLabel xrRentabilidadeValor;
        private DevExpress.XtraReports.Parameters.Parameter PRentabilidade;
        private DevExpress.XtraReports.UI.XRLabel xrRentabilidade;
        /// <summary>
        /// OBS
        /// </summary>
        public DevExpress.XtraReports.UI.XRRichText xrRichTextCred;
        /// <summary>
        /// OBS
        /// </summary>
        public DevExpress.XtraReports.UI.XRRichText xrRichTextQuad;
        /// <summary>
        /// OBS
        /// </summary>
        public DevExpress.XtraReports.UI.XRRichText xrRichTextDeb;
        private DevExpress.XtraReports.UI.GroupHeaderBand GroupHeaderDebSuper;
        private DevExpress.XtraReports.UI.XRLabel xrLabel2;
        private DevExpress.XtraReports.UI.XRPanel xrPanel1;
        private DevExpress.XtraReports.UI.XRLabel xrLabel17;
        /// <summary>
        /// 
        /// </summary>
        public DevExpress.XtraReports.Parameters.Parameter Quadradinhos;
    }
}
