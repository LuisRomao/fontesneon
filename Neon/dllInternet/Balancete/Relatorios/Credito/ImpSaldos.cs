﻿using System;
using System.Drawing;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using DevExpress.XtraReports.UI;
using Balancete.GrBalancete;
using Framework.objetosNeon;

namespace Balancete.Relatorios.Credito
{
    /// <summary>
    /// Impresso dos saldos
    /// </summary>
    public partial class ImpSaldos : dllImpresso.ImpLogoCond
    {

        private balancete balancete1;

        //private balancete.TipoClassificacaoDesp ClassificacaoCarregada;

        /// <summary>
        /// Construtor padrão. Não utilizar
        /// </summary>
        public ImpSaldos()
        {
            InitializeComponent();
        }


        /// <summary>
        /// Construtor
        /// </summary>
        /// <param name="_balancete"></param>
        /// <param name="_Agrupamento"></param>
        public ImpSaldos(balancete _balancete, AgrupadorBalancete.solicitaAgrupamento _Agrupamento = null)
        {
            InitializeComponent();
            balancete1 = _balancete;
            dCreditos1 = balancete1.DCreditos1;
            DetailReport2.DataSource = balancete1.DCreditos1;
            DetailReport3.DataSource = balancete1.DBalancete1;
            SetNome(balancete1.rowCON.CONNome);
            xrTitulo.Text = string.Format("Saldos\r\n({0:dd/MM/yyyy} a {1:dd/MM/yyyy})", balancete1.DataI, balancete1.DataF);
            

            //deveria ser assim: so que para tanto tem que refazer os binding
            /*
            if (_dCreditos1 == null)
                balancete1.CarregarDadosBalancete(dCreditos1, balancete.TipoClassificacaoDesp.ContasLogicas);
            else
                dCreditos1 = _dCreditos1;*/

            AgrupadorBalancete.solicitaAgrupamento Agrupamento = _Agrupamento ?? balancete1.AgrupamentoUsarBalancete;
            balancete1.CarregarDadosBalancete( Agrupamento,false);

        }

        private void xrLabel3_Draw(object sender, DrawEventArgs e)
        {
            Degrade(sender, e);
        }

        /// <summary>
        /// Cálculos
        /// </summary>
        public void Calculos()
        {            
            decimal[] Vx = new decimal[] { 0, 0, 0, 0 ,0};
            foreach (dCreditos.CreditosRow rowC in dCreditos1.Creditos)
                Vx[0] += rowC.Total;
            foreach (dCreditos.DebitosRow rowD in dCreditos1.Debitos)
                Vx[1] += rowD.Valor;
            foreach (dBalancete.ConTasLogicasRow rowCTL in balancete1.DBalancete1.ConTasLogicas)
                Vx[2] += rowCTL.RENTABILIDADE;
            foreach (dCreditos.ContasFisicasRow rowCo in dCreditos1.ContasFisicas)
            {               
                Vx[3] += rowCo.SaldoI;
                Vx[4] += rowCo.SaldoF;
            }
            V1.Value = Vx[0];
            V2.Value = Vx[1];
            V3.Value = Vx[2];
            V4.Value = Vx[3];
            V5.Value = Vx[4];
        }

        private void ImpCreditos_BeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e)
        {
            Calculos();
        }

        
            

    }
}
