﻿using System;
using System.Drawing;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using DevExpress.XtraReports.UI;
using Balancete.GrBalancete;

namespace Balancete.Relatorios.Credito
{
    /// <summary>
    /// Impresso de boletos por colunas
    /// </summary>
    public partial class ImpCredColunas : dllImpresso.ImpLogoCond
    {
        #region Construtores
        /// <summary>
        /// Construtor padrão (não usar)
        /// </summary>
        public ImpCredColunas()
        {
            InitializeComponent();
        }

        /// <summary>
        /// Construtor
        /// </summary>
        /// <param name="_balancete"></param>
        public ImpCredColunas(balancete _balancete)
        {
            InitializeComponent();
            balancete = _balancete;
            DataSource = balancete.DBalancete1;            

            foreach (dBalancete.GraficoRow rowG in balancete.DBalancete1.Grafico)
            {
                if (rowG.Valor != 0)
                {
                    //string Titulo = string.Format("{0}: {1:p2}", rowG.Titulo, Total == 0 ? 1 : rowG.Valor / Total);
                    xrChart1.Series[0].Points.Add(new DevExpress.XtraCharts.SeriesPoint(rowG.Titulo, rowG.Valor));
                }
            }
            SetNome(FrameworkProc.datasets.dCondominiosAtivos.dCondominiosTodosST.CONDOMINIOS.FindByCON(balancete.CON).CONNome);
            xrTitulo.Text = string.Format("Detalhamento de créditos {0:dd/MM/yy} a {1:dd/MM/yy} ", balancete.DataI, balancete.DataF);            
        } 
        #endregion

        private balancete balancete;

        private List<XRLabel> NovosComponentes;

        private static int nColunaTeste = 1;

        private Font fonte0 = null;

        private bool BoletoRemovido = false;

        private void CriaColunaNoImpressoEfetivo(bool ComLinhas)
        {
            if (NovosComponentes == null)
                NovosComponentes = new List<XRLabel>();
            else
            {
                foreach (XRLabel Apagar in NovosComponentes)
                    Apagar.Parent.Controls.Remove(Apagar);
                NovosComponentes.Clear();
            }
            XRLabel clone;
            XRLabel cloneT;
            XRLabel cloneTot;
            XRLabel cloneSubTot;

            int deltaAcumulado = 0;
            int larguraColunas = xrX0.Width;
            bool NumeroBol = (bool)NumeroBoleto.Value;
            int ColunasInc = 0;
                       
            int testacolunas = (int)Partestacolunas.Value;

            int CorrigePosicao = 0;

            

            if (!NumeroBol && !BoletoRemovido)
                CorrigePosicao = -larguraColunas;
            else if (NumeroBol && BoletoRemovido)
                CorrigePosicao = larguraColunas;
            BoletoRemovido = !NumeroBol;

            if (!NumeroBol)
            {
                xrNumeroBol.Visible = xrNumeroBolTit.Visible = false;                
                ColunasInc--;                
            }
            else
                xrNumeroBol.Visible = xrNumeroBolTit.Visible = true;

            if (CorrigePosicao != 0)
            {
                xrResp.Location = new Point(xrResp.Location.X + CorrigePosicao, xrResp.Location.Y);
                xrRespTit.Location = new Point(xrRespTit.Location.X + CorrigePosicao, xrRespTit.Location.Y);
                xrVenc.Location = new Point(xrVenc.Location.X + CorrigePosicao, xrVenc.Location.Y);
                xrVencTit.Location = new Point(xrVencTit.Location.X + CorrigePosicao, xrVencTit.Location.Y);
                xrSubTotalTit.Location = new Point(xrSubTotalTit.Location.X + CorrigePosicao, xrSubTotalTit.Location.Y);
                xrSubTotalSum.Location = new Point(xrSubTotalSum.Location.X + CorrigePosicao, xrSubTotalSum.Location.Y);
                xrX0.Location = new Point(xrX0.Location.X + CorrigePosicao, xrX0.Location.Y);
                xrX0Tit.Location = new Point(xrX0Tit.Location.X + CorrigePosicao, xrX0Tit.Location.Y);
                xrX0SutTot.Location = new Point(xrX0SutTot.Location.X + CorrigePosicao, xrX0SutTot.Location.Y);
                xrX0Tot.Location = new Point(xrX0Tot.Location.X + CorrigePosicao, xrX0Tot.Location.Y);
                xrPag.Location = new Point(xrPag.Location.X + CorrigePosicao, xrPag.Location.Y);
                xrPagT.Location = new Point(xrPagT.Location.X + CorrigePosicao, xrPagT.Location.Y);
            }

            
            if(fonte0 == null)
                fonte0 = xrX0.Font;
            Font fonte = fonte0;
            Font fonteT;
            XRLabel ModeloDetalhe = xrX0;
            XRLabel ModeloDetalheTot = xrX0Tot;
            XRLabel ModeloTitulo = xrX0Tit;
            XRLabel ModeloSubTot = xrX0SutTot;
            clone = ModeloDetalhe;
            cloneT = ModeloTitulo;
            cloneTot = ModeloDetalheTot;
            cloneSubTot = ModeloSubTot;
            if (testacolunas != 0)
            {
                while (balancete.ColunasDinamicas.Count > testacolunas)
                    balancete.ColunasDinamicas.RemoveAt(0);
                
                while (balancete.ColunasDinamicas.Count < testacolunas)
                    balancete.ColunasDinamicas.Add(string.Format("Teste{0}", nColunaTeste), string.Format("Coluna Teste: {0}", nColunaTeste++));
            }

            ColunasInc += balancete.ColunasDinamicas.Count;

            int W30 = 17;

            if (ColunasInc > 6)
                this.Landscape = true;


            if (ColunasInc > 21)
            {
                larguraColunas = larguraColunas - 80;
                fonte = new Font(fonte.FontFamily, fonte.Size - 5, FontStyle.Bold);
                ModeloTitulo.Angle = 30;
                ModeloTitulo.Size = new Size(ModeloTitulo.Width, ModeloTitulo.Height + W30);
                ModeloTitulo.Band.Height += W30;
                Font f = ModeloTitulo.Font;
                ModeloTitulo.Font = new Font(f.FontFamily, f.Size - 2, FontStyle.Bold);
            }
            else if (ColunasInc > 19)
            {
                larguraColunas = larguraColunas - 65;
                fonte = new Font(fonte.FontFamily, fonte.Size - 4, FontStyle.Bold);
                ModeloTitulo.Angle = 30;
                ModeloTitulo.Size = new System.Drawing.Size(ModeloTitulo.Width, ModeloTitulo.Height + W30);
                ModeloTitulo.Band.Height += W30;
                Font f = ModeloTitulo.Font;
                ModeloTitulo.Font = new Font(f.FontFamily, f.Size - 2, FontStyle.Bold);
            }
            else if (ColunasInc > 16)
            {
                larguraColunas = larguraColunas - 55;
                fonte = new Font(fonte.FontFamily, fonte.Size - 3, FontStyle.Bold);
                ModeloTitulo.Angle = 30;
                ModeloTitulo.Size = new System.Drawing.Size(ModeloTitulo.Width, ModeloTitulo.Height + W30);
                ModeloTitulo.Band.Height += W30;
                Font f = ModeloTitulo.Font;
                ModeloTitulo.Font = new Font(f.FontFamily, f.Size - 2, FontStyle.Bold);
            }
            else if (ColunasInc > 15)
            {
                larguraColunas = larguraColunas - 40;
                fonte = new Font(fonte.FontFamily, fonte.Size - 3, FontStyle.Bold);
                ModeloTitulo.Angle = 30;
                ModeloTitulo.Size = new System.Drawing.Size(ModeloTitulo.Width, ModeloTitulo.Height + W30);
                ModeloTitulo.Band.Height += W30;
                Font f = ModeloTitulo.Font;
                ModeloTitulo.Font = new Font(f.FontFamily, f.Size - 2, FontStyle.Bold);
            }
            else
                if (ColunasInc > 13)
                {
                    larguraColunas = larguraColunas - 30;
                    fonte = new Font(fonte.FontFamily, fonte.Size - 3, FontStyle.Bold);
                    ModeloTitulo.Angle = 30;
                    ModeloTitulo.Size = new System.Drawing.Size(ModeloTitulo.Width, ModeloTitulo.Height + W30);
                    ModeloTitulo.Band.Height += W30;
                    Font f = ModeloTitulo.Font;
                    ModeloTitulo.Font = new Font(f.FontFamily, f.Size - 2, FontStyle.Bold);
                }
                else
                    if (ColunasInc > 12)
                    {
                        larguraColunas = larguraColunas - 15;
                        fonte = new Font(fonte.FontFamily, fonte.Size - 2, FontStyle.Bold);
                        ModeloTitulo.Angle = 30;
                        ModeloTitulo.Size = new System.Drawing.Size(ModeloTitulo.Width, ModeloTitulo.Height + W30);
                        ModeloTitulo.Band.Height += W30;
                        Font f = ModeloTitulo.Font;
                        ModeloTitulo.Font = new Font(f.FontFamily, f.Size - 2, FontStyle.Bold);
                    }

            //else if (ColunasInc < 10)
            //{
            //    larguraColunas = larguraColunas + 15;                        
            //}
            fonteT = new Font(fonte.FontFamily, fonte.Size - 1, FontStyle.Bold);
            foreach (string NovaColuna in balancete.ColunasDinamicas.Keys)
            {
                string TituloColuna = NovaColuna;
                string TituloImpresso = balancete.ColunasDinamicas[NovaColuna];
                Type TipoDado = typeof(decimal);
                if (deltaAcumulado != 0)
                {
                    clone = new XRLabel();
                    NovosComponentes.Add(clone);
                    clone.TextAlignment = ModeloDetalhe.TextAlignment;
                    clone.Size = new Size(0, 0);
                    clone.CanGrow = ModeloDetalhe.CanGrow;
                    clone.WordWrap = ModeloDetalhe.WordWrap;
                    Bands[BandKind.Detail].Controls.Add(clone);
                    Bands[BandKind.Detail].Height = 40;
                    cloneTot = new XRLabel();
                    NovosComponentes.Add(cloneTot);
                    //cloneTot.BackColor = Color.Red;
                    GroupFooter2.Controls.Add(cloneTot);
                    //Bands[BandKind.ReportFooter].Controls.Add(cloneTot);
                    cloneTot.TextAlignment = ModeloDetalheTot.TextAlignment;

                    cloneTot.Summary.FormatString = ModeloDetalheTot.Summary.FormatString;
                    cloneTot.Summary.Func = ModeloDetalheTot.Summary.Func;
                    cloneTot.Summary.Running = ModeloDetalheTot.Summary.Running;

                    cloneSubTot = new XRLabel();
                    NovosComponentes.Add(cloneSubTot);
                    Bands[BandKind.GroupFooter].Controls.Add(cloneSubTot);
                    cloneSubTot.TextAlignment = ModeloSubTot.TextAlignment;

                    cloneSubTot.Summary.FormatString = ModeloSubTot.Summary.FormatString;
                    cloneSubTot.Summary.Func = ModeloSubTot.Summary.Func;
                    cloneSubTot.Summary.Running = ModeloSubTot.Summary.Running;

                    cloneT = new XRLabel();
                    NovosComponentes.Add(cloneT);
                    //Bands[BandKind.PageHeader].Controls.Add(cloneT);
                    ModeloTitulo.Band.Controls.Add(cloneT);
                    cloneT.TextAlignment = ModeloTitulo.TextAlignment;
                    cloneT.Angle = ModeloTitulo.Angle;
                    cloneT.Multiline = true;
                    cloneT.WordWrap = true;
                    cloneT.Font = ModeloTitulo.Font;

                }
                clone.Draw += Degrade;
                clone.Size = new Size(larguraColunas, ModeloDetalhe.Height);
                clone.Location = new Point(xrX0.Location.X + deltaAcumulado, ModeloDetalhe.Location.Y);
                clone.Font = fonte;

                cloneTot.Location = new Point(xrX0.Location.X + deltaAcumulado, ModeloDetalheTot.Location.Y);
                cloneTot.Size = new Size(larguraColunas, ModeloDetalheTot.Height);
                cloneTot.Font = fonteT;

                cloneSubTot.Location = new Point(xrX0.Location.X + deltaAcumulado, ModeloSubTot.Location.Y);
                cloneSubTot.Size = new Size(larguraColunas, ModeloSubTot.Height);
                cloneSubTot.Font = fonteT;

                cloneT.Location = new Point(xrX0.Location.X + deltaAcumulado, ModeloTitulo.Location.Y);
                cloneT.Size = new Size(larguraColunas, ModeloTitulo.Height);

                string Mascara = "{0:n2}";
                if (TipoDado == typeof(DateTime))
                    Mascara = "{0:dd/MM}";
                clone.DataBindings.Add("Text", null, string.Format("BOLetosRecebidos.{0}", TituloColuna), Mascara);
                //clone.DataBindings.Add(new DevExpress.XtraReports.UI.XRBinding("Text", null, "BOLetosRecebidos.calcBOL"));
                if (TipoDado == typeof(decimal))
                {
                    cloneTot.DataBindings.Add("Text", null, string.Format("BOLetosRecebidos.{0}", TituloColuna), Mascara);
                    cloneSubTot.DataBindings.Add("Text", null, string.Format("BOLetosRecebidos.{0}", TituloColuna), Mascara);
                }
                else
                    cloneSubTot.Visible = cloneTot.Visible = false;
                if (TituloImpresso == "")
                    TituloImpresso = Framework.datasets.dPLAnocontas.dPLAnocontasSt.PLAnocontas.FindByPLA(TituloColuna).PLADescricaoRes;
                cloneT.Text = TituloImpresso;
                deltaAcumulado += larguraColunas;
            };
            xrtotal.Location = new Point(xrX0.Location.X + deltaAcumulado, ModeloDetalhe.Location.Y);
            if ((bool)Marcadores.Value)
            {
                xrTotalG.Location = new Point(xrX0.Location.X + deltaAcumulado, xrTotalG.Location.Y);
                xrmarcador.Location = new Point(xrTotalG.Location.X + xrTotalG.Width, xrmarcador.Location.Y);
            }
            else
                xrTotalG.Location = new Point(xrX0.Location.X + deltaAcumulado, xrTotalG.Location.Y);
            xrSubTotalG.Location = new Point(xrX0.Location.X + deltaAcumulado, xrSubTotalG.Location.Y);
            xrTotalTitulo.Location = new Point(xrX0.Location.X + deltaAcumulado, xrTotalTitulo.Location.Y);

            if (ComLinhas)
            {
                XRLine Novalinha = new XRLine();
                Novalinha.Size = new Size(10, 5);
                Novalinha.Width = 1;
                Bands[BandKind.Detail].Controls.Add(Novalinha);
                Novalinha.Location = new Point(0, 42);
                Bands[BandKind.Detail].Height = 42;
                Novalinha.Width = xrtotal.Location.X + xrtotal.Width;
            };
            xrPanel1.Width = xrtotal.Location.X + xrtotal.Width;
            xrAgrupa.Width = xrPanel1.Width;
            //if (Tipo == TipoRelatorio.Emissao)
            //{
            //    GroupFooter1.Visible = GroupHeader1.Visible = false;
            //}
        }
        
        private void xrTotalG_Draw(object sender, DrawEventArgs e)
        {
            Degrade(sender, e);
        }

        private void calculadoGrupo_GetValue(object sender, GetValueEventArgs e)
        {
            System.Data.DataRowView DRV = (System.Data.DataRowView)e.Row;
            GrBalancete.dBalancete.BOLetosRecebidosRow row = (GrBalancete.dBalancete.BOLetosRecebidosRow)DRV.Row;
            e.Value = balancete.VirEnumTpDataNoPeriodo.Descritivo(row.calcGrupo);
        }

        private void ImpCredColunas_BeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e)
        {
            CriaColunaNoImpressoEfetivo((bool)ComLinhas.Value);
        }

    }
}
