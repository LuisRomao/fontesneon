﻿using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using DevExpress.XtraReports.UI;

namespace Balancete.Relatorios.Credito
{
    /// <summary>
    /// Det Crédito
    /// </summary>
    public partial class ImpDetCredito : DevExpress.XtraReports.UI.XtraReport
    {
        /// <summary>
        /// Construtor
        /// </summary>
        public ImpDetCredito()
        {
            InitializeComponent();
        }

        /// <summary>
        /// modelos
        /// </summary>
        public enum modelos 
        { 
            /// <summary>
            /// 
            /// </summary>
            numero, 
            /// <summary>
            /// 
            /// </summary>
            numero_bloco, 
            /// <summary>
            /// 
            /// </summary>
            numero_bloco_competencia, 
            /// <summary>
            /// 
            /// </summary>
            numero_competencia, 
            /// <summary>
            /// 
            /// </summary>
            numero_bloco_boleto 
        }
        
        /// <summary>
        /// 
        /// </summary>
        /// <param name="modelo"></param>
        public void AjustaModelo(modelos modelo)
        {
            
            switch (modelo)
            {
                case modelos.numero:
                    GroupHeader2.Visible = false;
                    Qcomp.Visible = false;
                    QValor.Location = Qcomp.Location;                    
                    Detail.MultiColumn.ColumnCount = 7;
                    xrPanelQuadrinho.Width = xrPanelQuadrinho.Width - Qcomp.Width;                 
                    break;
                case modelos.numero_bloco:
                    Qcomp.Visible = false;
                    QValor.Location = Qcomp.Location;                    
                    Detail.MultiColumn.ColumnCount = 7;
                    xrPanelQuadrinho.Width = xrPanelQuadrinho.Width - Qcomp.Width;                   
                    break;
                case modelos.numero_bloco_competencia:                    
                    //QValor.Visible = false;                    
                    Detail.MultiColumn.ColumnCount = 5;
                    //xrPanelQuadrinho.Width = xrPanelQuadrinho.Width - QValor.Width;                   
                    break;
                case modelos.numero_competencia:
                    GroupHeader2.Visible = false;
                    //QValor.Visible = false;                    
                    Detail.MultiColumn.ColumnCount = 5;
                    //xrPanelQuadrinho.Width = xrPanelQuadrinho.Width - QValor.Width;// - QBloco.Width;                    
                    break;
                case modelos.numero_bloco_boleto:
                    //TituloComp.Visible = false;
                    break;                    
            }
            xrShapeSeta.Location = new Point(xrPanelQuadrinho.Location.X + xrPanelQuadrinho.Width + 2, xrShapeSeta.Location.Y);
            /*
            string Mascara = "";
            string Campo = "";
            switch ((int)Tipo.Value)
            {
                case 1:
                    //xrLabel3.Text = string.Format("Mês - Boletos com vencimento no período: {0:dd/MM/yyyy} a {1:dd/MM/yyyy}",DataI.Value,DataF.Value);
                    Mascara = string.Format("Mês - Boletos com vencimento no período: {0:dd/MM/yyyy} a {1:dd/MM/yyyy}", DataI.Value, DataF.Value);
                    //xrLabel3.DataBindings.Add(new DevExpress.XtraReports.UI.XRBinding("Text", null, "Creditos.Antecipado", Mascara + "(R$ {0:n2})"));
                    Campo = "Creditos.Periodo";
                    break;
                case 2:
                    //xrLabel3.Text = string.Format("Atrasados - Vencimentos anterior a {0:dd/MM/yyyy}", DataI.Value);
                    Mascara = string.Format("Atrasados - Vencimentos anterior a {0:dd/MM/yyyy}", DataI.Value);
                    Campo = "Creditos.Atrasado";
                    break;
                case 3:
                    //xrLabel3.Text = string.Format("Antecipados - Vencimento superior a {0:dd/MM/yyyy}", DataF.Value);
                    Mascara = string.Format("Antecipados - Vencimento superior a {0:dd/MM/yyyy}", DataF.Value);
                    Campo = "Creditos.Antecipado";
                    break;
            }
            xrLabel3.DataBindings.Add(new DevExpress.XtraReports.UI.XRBinding("Text", null, Campo, Mascara + "(R$ {0:n2})"));
*/            
        }

        private void xrLabel3_BeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e)
        {
            
            
            switch ((int)Tipo.Value)
            {
                case 1:
                    xrLabel3.Text = string.Format("Mês - Boletos com vencimento no período: {0:dd/MM/yyyy} a {1:dd/MM/yyyy}",DataI.Value,DataF.Value);                    
                    break;
                case 2:
                    xrLabel3.Text = string.Format("Atrasados - Vencimento anterior a {0:dd/MM/yyyy}", DataI.Value);                    
                    break;
                case 3:
                    xrLabel3.Text = string.Format("Antecipados - Vencimento superior a {0:dd/MM/yyyy}", DataF.Value);                    
                    break;
            }
            
            
        }

        private void xrShapeSeta_BeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e)
        {
            xrShapeSeta.Visible = ((GetCurrentColumnValue<string>("Bloco") == GetNextColumnValue<string>("Bloco"))
                                   &&
                                   (GetCurrentColumnValue<int>("N") != GetNextColumnValue<int>("N")));
            
        }

        

    }
}
