﻿namespace Balancete.Relatorios.Credito
{
    partial class ImpCredColunas
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            DevExpress.XtraReports.UI.XRSummary xrSummary1 = new DevExpress.XtraReports.UI.XRSummary();
            DevExpress.XtraReports.UI.XRSummary xrSummary2 = new DevExpress.XtraReports.UI.XRSummary();
            DevExpress.XtraReports.UI.XRSummary xrSummary3 = new DevExpress.XtraReports.UI.XRSummary();
            DevExpress.XtraReports.UI.XRSummary xrSummary4 = new DevExpress.XtraReports.UI.XRSummary();
            DevExpress.XtraReports.UI.XRSummary xrSummary5 = new DevExpress.XtraReports.UI.XRSummary();
            DevExpress.XtraReports.UI.XRSummary xrSummary6 = new DevExpress.XtraReports.UI.XRSummary();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(ImpCredColunas));
            DevExpress.XtraCharts.SimpleDiagram3D simpleDiagram3D1 = new DevExpress.XtraCharts.SimpleDiagram3D();
            DevExpress.XtraCharts.Series series1 = new DevExpress.XtraCharts.Series();
            DevExpress.XtraCharts.Pie3DSeriesLabel pie3DSeriesLabel1 = new DevExpress.XtraCharts.Pie3DSeriesLabel();
            DevExpress.XtraCharts.Pie3DSeriesView pie3DSeriesView1 = new DevExpress.XtraCharts.Pie3DSeriesView();
            DevExpress.XtraCharts.Pie3DSeriesLabel pie3DSeriesLabel2 = new DevExpress.XtraCharts.Pie3DSeriesLabel();
            DevExpress.XtraCharts.Pie3DSeriesView pie3DSeriesView2 = new DevExpress.XtraCharts.Pie3DSeriesView();
            DevExpress.XtraReports.Parameters.StaticListLookUpSettings staticListLookUpSettings1 = new DevExpress.XtraReports.Parameters.StaticListLookUpSettings();
            DevExpress.XtraReports.Parameters.StaticListLookUpSettings staticListLookUpSettings2 = new DevExpress.XtraReports.Parameters.StaticListLookUpSettings();
            DevExpress.XtraReports.Parameters.StaticListLookUpSettings staticListLookUpSettings3 = new DevExpress.XtraReports.Parameters.StaticListLookUpSettings();
            this.GroupHeader1 = new DevExpress.XtraReports.UI.GroupHeaderBand();
            this.xrPagT = new DevExpress.XtraReports.UI.XRLabel();
            this.xrVencTit = new DevExpress.XtraReports.UI.XRLabel();
            this.xrX0Tit = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTotalTitulo = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel34 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrNumeroBolTit = new DevExpress.XtraReports.UI.XRLabel();
            this.xrRespTit = new DevExpress.XtraReports.UI.XRLabel();
            this.xrPanel1 = new DevExpress.XtraReports.UI.XRPanel();
            this.xrAgrupa = new DevExpress.XtraReports.UI.XRLabel();
            this.GroupHeader2 = new DevExpress.XtraReports.UI.GroupHeaderBand();
            this.GroupFooter1 = new DevExpress.XtraReports.UI.GroupFooterBand();
            this.xrSubTotalG = new DevExpress.XtraReports.UI.XRLabel();
            this.xrX0SutTot = new DevExpress.XtraReports.UI.XRLabel();
            this.xrSubTotalTit = new DevExpress.XtraReports.UI.XRLabel();
            this.GroupFooter2 = new DevExpress.XtraReports.UI.GroupFooterBand();
            this.xrTotalG = new DevExpress.XtraReports.UI.XRLabel();
            this.xrmarcador = new DevExpress.XtraReports.UI.XRLabel();
            this.xrSubTotalSum = new DevExpress.XtraReports.UI.XRLabel();
            this.xrX0Tot = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel3 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrNumeroBol = new DevExpress.XtraReports.UI.XRLabel();
            this.xrResp = new DevExpress.XtraReports.UI.XRLabel();
            this.xrVenc = new DevExpress.XtraReports.UI.XRLabel();
            this.xrX0 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrtotal = new DevExpress.XtraReports.UI.XRLabel();
            this.ReportFooter = new DevExpress.XtraReports.UI.ReportFooterBand();
            this.xrRichTextCol = new DevExpress.XtraReports.UI.XRRichText();
            this.xrChart1 = new DevExpress.XtraReports.UI.XRChart();
            this.dBalancete1 = new Balancete.GrBalancete.dBalancete();
            this.calculatedField1 = new DevExpress.XtraReports.UI.CalculatedField();
            this.calculadoGrupo = new DevExpress.XtraReports.UI.CalculatedField();
            this.NumeroBoleto = new DevExpress.XtraReports.Parameters.Parameter();
            this.Partestacolunas = new DevExpress.XtraReports.Parameters.Parameter();
            this.Marcadores = new DevExpress.XtraReports.Parameters.Parameter();
            this.ComLinhas = new DevExpress.XtraReports.Parameters.Parameter();
            this.xrPag = new DevExpress.XtraReports.UI.XRLabel();
            this.bALancetesTableAdapter = new Balancete.GrBalancete.dBalanceteTableAdapters.BALancetesTableAdapter();
            ((System.ComponentModel.ISupportInitialize)(this.xrRichTextCol)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrChart1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(simpleDiagram3D1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(series1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(pie3DSeriesLabel1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(pie3DSeriesView1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(pie3DSeriesLabel2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(pie3DSeriesView2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dBalancete1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            // 
            // PageHeader
            // 
            this.PageHeader.HeightF = 309.4583F;
            // 
            // Detail
            // 
            this.Detail.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrPag,
            this.xrVenc,
            this.xrX0,
            this.xrtotal,
            this.xrLabel3,
            this.xrNumeroBol,
            this.xrResp});
            this.Detail.HeightF = 42F;
            this.Detail.SortFields.AddRange(new DevExpress.XtraReports.UI.GroupField[] {
            new DevExpress.XtraReports.UI.GroupField("calcGrupo", DevExpress.XtraReports.UI.XRColumnSortOrder.Ascending),
            new DevExpress.XtraReports.UI.GroupField("calcUnidade", DevExpress.XtraReports.UI.XRColumnSortOrder.Ascending),
            new DevExpress.XtraReports.UI.GroupField("BOLPagamento", DevExpress.XtraReports.UI.XRColumnSortOrder.Ascending)});
            // 
            // GroupHeader1
            // 
            this.GroupHeader1.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrPagT,
            this.xrVencTit,
            this.xrX0Tit,
            this.xrTotalTitulo,
            this.xrLabel34,
            this.xrNumeroBolTit,
            this.xrRespTit,
            this.xrPanel1});
            this.GroupHeader1.Dpi = 254F;
            this.GroupHeader1.GroupFields.AddRange(new DevExpress.XtraReports.UI.GroupField[] {
            new DevExpress.XtraReports.UI.GroupField("calcGrupo", DevExpress.XtraReports.UI.XRColumnSortOrder.Ascending)});
            this.GroupHeader1.GroupUnion = DevExpress.XtraReports.UI.GroupUnion.WholePage;
            this.GroupHeader1.HeightF = 131F;
            this.GroupHeader1.KeepTogether = true;
            this.GroupHeader1.Name = "GroupHeader1";
            this.GroupHeader1.RepeatEveryPage = true;
            // 
            // xrPagT
            // 
            this.xrPagT.Dpi = 254F;
            this.xrPagT.Font = new System.Drawing.Font("Times New Roman", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrPagT.LocationFloat = new DevExpress.Utils.PointFloat(497.9999F, 86.99999F);
            this.xrPagT.Name = "xrPagT";
            this.xrPagT.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrPagT.SizeF = new System.Drawing.SizeF(105.4688F, 44.00001F);
            this.xrPagT.StylePriority.UseTextAlignment = false;
            this.xrPagT.Text = "Pag.";
            this.xrPagT.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter;
            // 
            // xrVencTit
            // 
            this.xrVencTit.Dpi = 254F;
            this.xrVencTit.Font = new System.Drawing.Font("Times New Roman", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrVencTit.LocationFloat = new DevExpress.Utils.PointFloat(371.9999F, 87F);
            this.xrVencTit.Name = "xrVencTit";
            this.xrVencTit.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrVencTit.SizeF = new System.Drawing.SizeF(101F, 44F);
            this.xrVencTit.Text = "Venc.";
            this.xrVencTit.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrX0Tit
            // 
            this.xrX0Tit.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrX0Tit.Dpi = 254F;
            this.xrX0Tit.Font = new System.Drawing.Font("Times New Roman", 6.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrX0Tit.LocationFloat = new DevExpress.Utils.PointFloat(613.9028F, 81.12836F);
            this.xrX0Tit.Multiline = true;
            this.xrX0Tit.Name = "xrX0Tit";
            this.xrX0Tit.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 5, 0, 0, 254F);
            this.xrX0Tit.SizeF = new System.Drawing.SizeF(154F, 49.87164F);
            this.xrX0Tit.StylePriority.UseBorders = false;
            this.xrX0Tit.StylePriority.UseFont = false;
            this.xrX0Tit.StylePriority.UsePadding = false;
            this.xrX0Tit.StylePriority.UseTextAlignment = false;
            this.xrX0Tit.Text = "Condomínio";
            this.xrX0Tit.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.xrX0Tit.WordWrap = false;
            // 
            // xrTotalTitulo
            // 
            this.xrTotalTitulo.Dpi = 254F;
            this.xrTotalTitulo.Font = new System.Drawing.Font("Times New Roman", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrTotalTitulo.LocationFloat = new DevExpress.Utils.PointFloat(1613F, 87F);
            this.xrTotalTitulo.Name = "xrTotalTitulo";
            this.xrTotalTitulo.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTotalTitulo.SizeF = new System.Drawing.SizeF(184F, 44F);
            this.xrTotalTitulo.StylePriority.UseTextAlignment = false;
            this.xrTotalTitulo.Text = "Total";
            this.xrTotalTitulo.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter;
            // 
            // xrLabel34
            // 
            this.xrLabel34.Dpi = 254F;
            this.xrLabel34.Font = new System.Drawing.Font("Times New Roman", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel34.LocationFloat = new DevExpress.Utils.PointFloat(3F, 87F);
            this.xrLabel34.Name = "xrLabel34";
            this.xrLabel34.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrLabel34.SizeF = new System.Drawing.SizeF(128F, 44F);
            this.xrLabel34.Text = "Apto";
            this.xrLabel34.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrNumeroBolTit
            // 
            this.xrNumeroBolTit.Dpi = 254F;
            this.xrNumeroBolTit.Font = new System.Drawing.Font("Times New Roman", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrNumeroBolTit.LocationFloat = new DevExpress.Utils.PointFloat(144.9999F, 87F);
            this.xrNumeroBolTit.Name = "xrNumeroBolTit";
            this.xrNumeroBolTit.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrNumeroBolTit.SizeF = new System.Drawing.SizeF(107F, 44F);
            this.xrNumeroBolTit.Text = "Boleto";
            this.xrNumeroBolTit.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrRespTit
            // 
            this.xrRespTit.Dpi = 254F;
            this.xrRespTit.Font = new System.Drawing.Font("Times New Roman", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrRespTit.LocationFloat = new DevExpress.Utils.PointFloat(262.9999F, 87F);
            this.xrRespTit.Name = "xrRespTit";
            this.xrRespTit.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrRespTit.SizeF = new System.Drawing.SizeF(84F, 44F);
            this.xrRespTit.Text = "Resp";
            this.xrRespTit.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrPanel1
            // 
            this.xrPanel1.BackColor = System.Drawing.Color.Silver;
            this.xrPanel1.CanGrow = false;
            this.xrPanel1.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrAgrupa});
            this.xrPanel1.Dpi = 254F;
            this.xrPanel1.LocationFloat = new DevExpress.Utils.PointFloat(0F, 0F);
            this.xrPanel1.Name = "xrPanel1";
            this.xrPanel1.SizeF = new System.Drawing.SizeF(1786F, 77F);
            this.xrPanel1.StylePriority.UseBackColor = false;
            // 
            // xrAgrupa
            // 
            this.xrAgrupa.AutoWidth = true;
            this.xrAgrupa.BackColor = System.Drawing.Color.Transparent;
            this.xrAgrupa.CanGrow = false;
            this.xrAgrupa.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "BOLetosRecebidos.calculadoGrupo")});
            this.xrAgrupa.Dpi = 254F;
            this.xrAgrupa.Font = new System.Drawing.Font("Times New Roman", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrAgrupa.ForeColor = System.Drawing.Color.White;
            this.xrAgrupa.LocationFloat = new DevExpress.Utils.PointFloat(0F, 13F);
            this.xrAgrupa.Name = "xrAgrupa";
            this.xrAgrupa.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrAgrupa.SizeF = new System.Drawing.SizeF(1730F, 53F);
            this.xrAgrupa.StylePriority.UseBorderColor = false;
            this.xrAgrupa.StylePriority.UseFont = false;
            this.xrAgrupa.StylePriority.UseForeColor = false;
            this.xrAgrupa.StylePriority.UseTextAlignment = false;
            this.xrAgrupa.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter;
            this.xrAgrupa.WordWrap = false;
            // 
            // GroupHeader2
            // 
            this.GroupHeader2.Dpi = 254F;
            this.GroupHeader2.GroupUnion = DevExpress.XtraReports.UI.GroupUnion.WholePage;
            this.GroupHeader2.HeightF = 23.8125F;
            this.GroupHeader2.Level = 1;
            this.GroupHeader2.Name = "GroupHeader2";
            // 
            // GroupFooter1
            // 
            this.GroupFooter1.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrSubTotalG,
            this.xrX0SutTot,
            this.xrSubTotalTit});
            this.GroupFooter1.Dpi = 254F;
            this.GroupFooter1.GroupUnion = DevExpress.XtraReports.UI.GroupFooterUnion.WithLastDetail;
            this.GroupFooter1.HeightF = 63.5F;
            this.GroupFooter1.KeepTogether = true;
            this.GroupFooter1.Name = "GroupFooter1";
            // 
            // xrSubTotalG
            // 
            this.xrSubTotalG.BackColor = System.Drawing.Color.Transparent;
            this.xrSubTotalG.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "BOLetosRecebidos.ValCredLiq")});
            this.xrSubTotalG.Dpi = 254F;
            this.xrSubTotalG.Font = new System.Drawing.Font("Times New Roman", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrSubTotalG.LocationFloat = new DevExpress.Utils.PointFloat(1506.208F, 0F);
            this.xrSubTotalG.Name = "xrSubTotalG";
            this.xrSubTotalG.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrSubTotalG.SizeF = new System.Drawing.SizeF(185F, 44F);
            this.xrSubTotalG.StylePriority.UseBackColor = false;
            xrSummary1.FormatString = "{0:n2}";
            xrSummary1.Running = DevExpress.XtraReports.UI.SummaryRunning.Group;
            this.xrSubTotalG.Summary = xrSummary1;
            this.xrSubTotalG.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight;
            // 
            // xrX0SutTot
            // 
            this.xrX0SutTot.BackColor = System.Drawing.Color.Transparent;
            this.xrX0SutTot.CanGrow = false;
            this.xrX0SutTot.Dpi = 254F;
            this.xrX0SutTot.Font = new System.Drawing.Font("Times New Roman", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrX0SutTot.LocationFloat = new DevExpress.Utils.PointFloat(613.9029F, 0F);
            this.xrX0SutTot.Name = "xrX0SutTot";
            this.xrX0SutTot.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrX0SutTot.SizeF = new System.Drawing.SizeF(154F, 42F);
            this.xrX0SutTot.StylePriority.UseBackColor = false;
            this.xrX0SutTot.StylePriority.UseFont = false;
            xrSummary2.FormatString = "{0:n2}";
            xrSummary2.Running = DevExpress.XtraReports.UI.SummaryRunning.Group;
            this.xrX0SutTot.Summary = xrSummary2;
            this.xrX0SutTot.Text = "xrLabel2";
            this.xrX0SutTot.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight;
            this.xrX0SutTot.WordWrap = false;
            // 
            // xrSubTotalTit
            // 
            this.xrSubTotalTit.BackColor = System.Drawing.Color.Transparent;
            this.xrSubTotalTit.CanGrow = false;
            this.xrSubTotalTit.Dpi = 254F;
            this.xrSubTotalTit.Font = new System.Drawing.Font("Times New Roman", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrSubTotalTit.LocationFloat = new DevExpress.Utils.PointFloat(98.20847F, 0F);
            this.xrSubTotalTit.Name = "xrSubTotalTit";
            this.xrSubTotalTit.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrSubTotalTit.SizeF = new System.Drawing.SizeF(256F, 42F);
            this.xrSubTotalTit.StylePriority.UseBackColor = false;
            this.xrSubTotalTit.StylePriority.UseFont = false;
            xrSummary3.FormatString = "{0:n2}";
            this.xrSubTotalTit.Summary = xrSummary3;
            this.xrSubTotalTit.Text = "Sub TOTAL";
            this.xrSubTotalTit.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight;
            this.xrSubTotalTit.WordWrap = false;
            // 
            // GroupFooter2
            // 
            this.GroupFooter2.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrTotalG,
            this.xrmarcador,
            this.xrSubTotalSum,
            this.xrX0Tot});
            this.GroupFooter2.Dpi = 254F;
            this.GroupFooter2.GroupUnion = DevExpress.XtraReports.UI.GroupFooterUnion.WithLastDetail;
            this.GroupFooter2.HeightF = 79.5834F;
            this.GroupFooter2.Level = 1;
            this.GroupFooter2.Name = "GroupFooter2";
            // 
            // xrTotalG
            // 
            this.xrTotalG.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "BOLetosRecebidos.ValCredLiq")});
            this.xrTotalG.Dpi = 254F;
            this.xrTotalG.Font = new System.Drawing.Font("Times New Roman", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrTotalG.LocationFloat = new DevExpress.Utils.PointFloat(1566.469F, 25.00001F);
            this.xrTotalG.Name = "xrTotalG";
            this.xrTotalG.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTotalG.SizeF = new System.Drawing.SizeF(182.0001F, 44F);
            xrSummary4.FormatString = "{0:n2}";
            xrSummary4.Running = DevExpress.XtraReports.UI.SummaryRunning.Report;
            this.xrTotalG.Summary = xrSummary4;
            this.xrTotalG.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight;
            this.xrTotalG.Draw += new DevExpress.XtraReports.UI.DrawEventHandler(this.xrTotalG_Draw);
            // 
            // xrmarcador
            // 
            this.xrmarcador.Dpi = 254F;
            this.xrmarcador.Font = new System.Drawing.Font("Times New Roman", 6.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrmarcador.LocationFloat = new DevExpress.Utils.PointFloat(1527.433F, 35.58003F);
            this.xrmarcador.Name = "xrmarcador";
            this.xrmarcador.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrmarcador.SizeF = new System.Drawing.SizeF(39.03601F, 33.42F);
            this.xrmarcador.StylePriority.UseFont = false;
            this.xrmarcador.Text = "(1)";
            // 
            // xrSubTotalSum
            // 
            this.xrSubTotalSum.BackColor = System.Drawing.Color.Transparent;
            this.xrSubTotalSum.CanGrow = false;
            this.xrSubTotalSum.Dpi = 254F;
            this.xrSubTotalSum.Font = new System.Drawing.Font("Times New Roman", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrSubTotalSum.LocationFloat = new DevExpress.Utils.PointFloat(51.53109F, 27.00005F);
            this.xrSubTotalSum.Name = "xrSubTotalSum";
            this.xrSubTotalSum.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrSubTotalSum.SizeF = new System.Drawing.SizeF(312F, 42F);
            this.xrSubTotalSum.StylePriority.UseBackColor = false;
            this.xrSubTotalSum.StylePriority.UseFont = false;
            xrSummary5.FormatString = "{0:n2}";
            this.xrSubTotalSum.Summary = xrSummary5;
            this.xrSubTotalSum.Text = "TOTAL GERAL";
            this.xrSubTotalSum.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight;
            this.xrSubTotalSum.WordWrap = false;
            // 
            // xrX0Tot
            // 
            this.xrX0Tot.BackColor = System.Drawing.Color.Transparent;
            this.xrX0Tot.CanGrow = false;
            this.xrX0Tot.Dpi = 254F;
            this.xrX0Tot.Font = new System.Drawing.Font("Times New Roman", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrX0Tot.LocationFloat = new DevExpress.Utils.PointFloat(449.4687F, 27.00005F);
            this.xrX0Tot.Name = "xrX0Tot";
            this.xrX0Tot.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrX0Tot.SizeF = new System.Drawing.SizeF(154F, 42F);
            this.xrX0Tot.StylePriority.UseBackColor = false;
            this.xrX0Tot.StylePriority.UseFont = false;
            xrSummary6.FormatString = "{0:n2}";
            xrSummary6.Running = DevExpress.XtraReports.UI.SummaryRunning.Report;
            this.xrX0Tot.Summary = xrSummary6;
            this.xrX0Tot.Text = "xrX0Tot";
            this.xrX0Tot.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight;
            this.xrX0Tot.WordWrap = false;
            // 
            // xrLabel3
            // 
            this.xrLabel3.BackColor = System.Drawing.Color.Transparent;
            this.xrLabel3.CanGrow = false;
            this.xrLabel3.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "BOLetosRecebidos.calcUnidade")});
            this.xrLabel3.Dpi = 254F;
            this.xrLabel3.Font = new System.Drawing.Font("Times New Roman", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel3.LocationFloat = new DevExpress.Utils.PointFloat(0F, 0F);
            this.xrLabel3.Name = "xrLabel3";
            this.xrLabel3.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrLabel3.SizeF = new System.Drawing.SizeF(140F, 42F);
            this.xrLabel3.StylePriority.UseBackColor = false;
            this.xrLabel3.StylePriority.UseFont = false;
            this.xrLabel3.StylePriority.UseTextAlignment = false;
            this.xrLabel3.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.xrLabel3.WordWrap = false;
            // 
            // xrNumeroBol
            // 
            this.xrNumeroBol.BackColor = System.Drawing.Color.Transparent;
            this.xrNumeroBol.CanGrow = false;
            this.xrNumeroBol.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "BOLetosRecebidos.calcBOL")});
            this.xrNumeroBol.Dpi = 254F;
            this.xrNumeroBol.Font = new System.Drawing.Font("Times New Roman", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrNumeroBol.LocationFloat = new DevExpress.Utils.PointFloat(141.9999F, 0F);
            this.xrNumeroBol.Name = "xrNumeroBol";
            this.xrNumeroBol.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 0, 0, 0, 254F);
            this.xrNumeroBol.SizeF = new System.Drawing.SizeF(134F, 42F);
            this.xrNumeroBol.StylePriority.UseBackColor = false;
            this.xrNumeroBol.StylePriority.UseFont = false;
            this.xrNumeroBol.StylePriority.UsePadding = false;
            this.xrNumeroBol.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight;
            this.xrNumeroBol.WordWrap = false;
            // 
            // xrResp
            // 
            this.xrResp.BackColor = System.Drawing.Color.Transparent;
            this.xrResp.CanGrow = false;
            this.xrResp.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "BOLetosRecebidos.calculatedField1")});
            this.xrResp.Dpi = 254F;
            this.xrResp.LocationFloat = new DevExpress.Utils.PointFloat(283.9999F, 0F);
            this.xrResp.Name = "xrResp";
            this.xrResp.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrResp.SizeF = new System.Drawing.SizeF(46F, 42F);
            this.xrResp.StylePriority.UseBackColor = false;
            this.xrResp.StylePriority.UseTextAlignment = false;
            this.xrResp.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter;
            this.xrResp.WordWrap = false;
            // 
            // xrVenc
            // 
            this.xrVenc.BackColor = System.Drawing.Color.Transparent;
            this.xrVenc.CanGrow = false;
            this.xrVenc.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "BOLetosRecebidos.BOLVencto", "{0:dd/MM/yyyy}")});
            this.xrVenc.Dpi = 254F;
            this.xrVenc.Font = new System.Drawing.Font("Times New Roman", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrVenc.LocationFloat = new DevExpress.Utils.PointFloat(336.9999F, 0F);
            this.xrVenc.Name = "xrVenc";
            this.xrVenc.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 5, 0, 0, 254F);
            this.xrVenc.SizeF = new System.Drawing.SizeF(157F, 42F);
            this.xrVenc.StylePriority.UseBackColor = false;
            this.xrVenc.StylePriority.UseFont = false;
            this.xrVenc.StylePriority.UsePadding = false;
            this.xrVenc.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            this.xrVenc.WordWrap = false;
            // 
            // xrX0
            // 
            this.xrX0.BackColor = System.Drawing.Color.Transparent;
            this.xrX0.CanGrow = false;
            this.xrX0.Dpi = 254F;
            this.xrX0.LocationFloat = new DevExpress.Utils.PointFloat(613.9029F, 0F);
            this.xrX0.Name = "xrX0";
            this.xrX0.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrX0.SizeF = new System.Drawing.SizeF(154F, 42F);
            this.xrX0.StylePriority.UseBackColor = false;
            this.xrX0.Text = "xrX0";
            this.xrX0.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight;
            this.xrX0.WordWrap = false;
            // 
            // xrtotal
            // 
            this.xrtotal.BackColor = System.Drawing.Color.Gainsboro;
            this.xrtotal.CanGrow = false;
            this.xrtotal.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "BOLetosRecebidos.ValCredLiq", "{0:n2}")});
            this.xrtotal.Dpi = 254F;
            this.xrtotal.LocationFloat = new DevExpress.Utils.PointFloat(1612F, 0F);
            this.xrtotal.Name = "xrtotal";
            this.xrtotal.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrtotal.SizeF = new System.Drawing.SizeF(185F, 42F);
            this.xrtotal.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight;
            this.xrtotal.WordWrap = false;
            // 
            // ReportFooter
            // 
            this.ReportFooter.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrRichTextCol,
            this.xrChart1});
            this.ReportFooter.Dpi = 254F;
            this.ReportFooter.HeightF = 629.8749F;
            this.ReportFooter.KeepTogether = true;
            this.ReportFooter.Name = "ReportFooter";
            // 
            // xrRichTextCol
            // 
            this.xrRichTextCol.Dpi = 254F;
            this.xrRichTextCol.Font = new System.Drawing.Font("Times New Roman", 9.75F);
            this.xrRichTextCol.LocationFloat = new DevExpress.Utils.PointFloat(0F, 625.8749F);
            this.xrRichTextCol.Name = "xrRichTextCol";
            this.xrRichTextCol.SerializableRtfString = resources.GetString("xrRichTextCol.SerializableRtfString");
            this.xrRichTextCol.SizeF = new System.Drawing.SizeF(1800F, 5F);
            // 
            // xrChart1
            // 
            this.xrChart1.AppearanceNameSerializable = "Chameleon";
            this.xrChart1.BorderColor = System.Drawing.Color.Black;
            this.xrChart1.Borders = DevExpress.XtraPrinting.BorderSide.None;
            simpleDiagram3D1.LabelsResolveOverlappingMinIndent = 5;
            simpleDiagram3D1.RotationMatrixSerializable = "0.999901095938711;-0.00311194241939914;-0.0137154713715572;0;-0.00767036567099105" +
    ";0.696742991715813;-0.717279840080129;0;0.0117882921171366;0.717314100871633;0.6" +
    "96650211267953;0;0;0;0;1";
            this.xrChart1.Diagram = simpleDiagram3D1;
            this.xrChart1.Dpi = 254F;
            this.xrChart1.Legend.AlignmentVertical = DevExpress.XtraCharts.LegendAlignmentVertical.Center;
            this.xrChart1.Legend.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.xrChart1.Legend.EnableAntialiasing = DevExpress.Utils.DefaultBoolean.True;
            this.xrChart1.Legend.Name = "Default Legend";
            this.xrChart1.Legend.Shadow.Visible = true;
            this.xrChart1.LocationFloat = new DevExpress.Utils.PointFloat(5.999957F, 0F);
            this.xrChart1.Name = "xrChart1";
            this.xrChart1.PaletteName = "Grayscale";
            series1.ArgumentScaleType = DevExpress.XtraCharts.ScaleType.Qualitative;
            pie3DSeriesLabel1.Border.Visibility = DevExpress.Utils.DefaultBoolean.False;
            pie3DSeriesLabel1.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Bold);
            pie3DSeriesLabel1.Position = DevExpress.XtraCharts.PieSeriesLabelPosition.TwoColumns;
            pie3DSeriesLabel1.ResolveOverlappingMode = DevExpress.XtraCharts.ResolveOverlappingMode.Default;
            pie3DSeriesLabel1.TextAlignment = System.Drawing.StringAlignment.Near;
            pie3DSeriesLabel1.TextColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            series1.Label = pie3DSeriesLabel1;
            series1.LabelsVisibility = DevExpress.Utils.DefaultBoolean.True;
            series1.LegendTextPattern = "{A}: {VP:P2}";
            series1.Name = "Series 1";
            pie3DSeriesView1.ExplodeMode = DevExpress.XtraCharts.PieExplodeMode.All;
            series1.View = pie3DSeriesView1;
            this.xrChart1.SeriesSerializable = new DevExpress.XtraCharts.Series[] {
        series1};
            pie3DSeriesLabel2.ResolveOverlappingMode = DevExpress.XtraCharts.ResolveOverlappingMode.Default;
            this.xrChart1.SeriesTemplate.Label = pie3DSeriesLabel2;
            this.xrChart1.SeriesTemplate.View = pie3DSeriesView2;
            this.xrChart1.SizeF = new System.Drawing.SizeF(1793F, 625.8749F);
            // 
            // dBalancete1
            // 
            this.dBalancete1.DataSetName = "dBalancete";
            this.dBalancete1.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // calculatedField1
            // 
            this.calculatedField1.DataMember = "BOLetosRecebidos";
            this.calculatedField1.DisplayName = "calculadoPI";
            this.calculatedField1.Expression = "Iif([BOLProprietario] == ?, \' \',Iif([BOLProprietario],\'P\' , \'I\') )";
            this.calculatedField1.Name = "calculatedField1";
            // 
            // calculadoGrupo
            // 
            this.calculadoGrupo.DataMember = "BOLetosRecebidos";
            this.calculadoGrupo.Name = "calculadoGrupo";
            this.calculadoGrupo.GetValue += new DevExpress.XtraReports.UI.GetValueEventHandler(this.calculadoGrupo_GetValue);
            // 
            // NumeroBoleto
            // 
            this.NumeroBoleto.Description = "Contem ou nao o numero do boleto";
            staticListLookUpSettings1.FilterString = null;
            staticListLookUpSettings1.LookUpValues.Add(new DevExpress.XtraReports.Parameters.LookUpValue(true, "Sim"));
            staticListLookUpSettings1.LookUpValues.Add(new DevExpress.XtraReports.Parameters.LookUpValue(false, "Não"));
            this.NumeroBoleto.LookUpSettings = staticListLookUpSettings1;
            this.NumeroBoleto.Name = "NumeroBoleto";
            this.NumeroBoleto.Type = typeof(bool);
            this.NumeroBoleto.ValueInfo = "False";
            // 
            // Partestacolunas
            // 
            this.Partestacolunas.Description = "Teste de número de colunas";
            this.Partestacolunas.Name = "Partestacolunas";
            this.Partestacolunas.Type = typeof(int);
            this.Partestacolunas.ValueInfo = "0";
            // 
            // Marcadores
            // 
            this.Marcadores.Description = "Mostrar identificador";
            staticListLookUpSettings2.FilterString = null;
            staticListLookUpSettings2.LookUpValues.Add(new DevExpress.XtraReports.Parameters.LookUpValue(true, "Sim"));
            staticListLookUpSettings2.LookUpValues.Add(new DevExpress.XtraReports.Parameters.LookUpValue(false, "Não"));
            this.Marcadores.LookUpSettings = staticListLookUpSettings2;
            this.Marcadores.Name = "Marcadores";
            this.Marcadores.Type = typeof(bool);
            this.Marcadores.ValueInfo = "True";
            // 
            // ComLinhas
            // 
            this.ComLinhas.Description = "Com linhas";
            staticListLookUpSettings3.FilterString = null;
            staticListLookUpSettings3.LookUpValues.Add(new DevExpress.XtraReports.Parameters.LookUpValue(true, "Sim"));
            staticListLookUpSettings3.LookUpValues.Add(new DevExpress.XtraReports.Parameters.LookUpValue(false, "Não"));
            this.ComLinhas.LookUpSettings = staticListLookUpSettings3;
            this.ComLinhas.Name = "ComLinhas";
            this.ComLinhas.Type = typeof(bool);
            this.ComLinhas.ValueInfo = "False";
            // 
            // xrPag
            // 
            this.xrPag.BackColor = System.Drawing.Color.Transparent;
            this.xrPag.CanGrow = false;
            this.xrPag.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "BOLetosRecebidos.BOLPagamento", "{0:dd/MM}")});
            this.xrPag.Dpi = 254F;
            this.xrPag.LocationFloat = new DevExpress.Utils.PointFloat(497.9999F, 0F);
            this.xrPag.Name = "xrPag";
            this.xrPag.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrPag.SizeF = new System.Drawing.SizeF(105.4688F, 42F);
            this.xrPag.StylePriority.UseBackColor = false;
            this.xrPag.StylePriority.UseTextAlignment = false;
            this.xrPag.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter;
            this.xrPag.WordWrap = false;
            // 
            // bALancetesTableAdapter
            // 
            this.bALancetesTableAdapter.ClearBeforeFill = true;
            this.bALancetesTableAdapter.GetNovosDados = ((System.Collections.ArrayList)(resources.GetObject("bALancetesTableAdapter.GetNovosDados")));
            this.bALancetesTableAdapter.TabelaDataTable = null;
            // 
            // ImpCredColunas
            // 
            this.Bands.AddRange(new DevExpress.XtraReports.UI.Band[] {
            this.Detail,
            this.PageHeader,
            this.GroupHeader1,
            this.GroupHeader2,
            this.GroupFooter1,
            this.GroupFooter2,
            this.ReportFooter});
            this.CalculatedFields.AddRange(new DevExpress.XtraReports.UI.CalculatedField[] {
            this.calculatedField1,
            this.calculadoGrupo});
            this.DataMember = "BOLetosRecebidos";
            this.DataSource = this.dBalancete1;
            this.Parameters.AddRange(new DevExpress.XtraReports.Parameters.Parameter[] {
            this.NumeroBoleto,
            this.Partestacolunas,
            this.Marcadores,
            this.ComLinhas});
            this.Version = "16.2";
            this.BeforePrint += new System.Drawing.Printing.PrintEventHandler(this.ImpCredColunas_BeforePrint);
            this.Controls.SetChildIndex(this.ReportFooter, 0);
            this.Controls.SetChildIndex(this.GroupFooter2, 0);
            this.Controls.SetChildIndex(this.GroupFooter1, 0);
            this.Controls.SetChildIndex(this.GroupHeader2, 0);
            this.Controls.SetChildIndex(this.GroupHeader1, 0);
            this.Controls.SetChildIndex(this.PageHeader, 0);
            this.Controls.SetChildIndex(this.Detail, 0);
            ((System.ComponentModel.ISupportInitialize)(this.xrRichTextCol)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(simpleDiagram3D1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(pie3DSeriesLabel1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(pie3DSeriesView1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(series1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(pie3DSeriesLabel2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(pie3DSeriesView2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrChart1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dBalancete1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();

        }

        #endregion

        private DevExpress.XtraReports.UI.GroupHeaderBand GroupHeader1;
        private DevExpress.XtraReports.UI.XRPanel xrPanel1;
        private DevExpress.XtraReports.UI.XRLabel xrAgrupa;
        private DevExpress.XtraReports.UI.GroupHeaderBand GroupHeader2;
        private DevExpress.XtraReports.UI.GroupFooterBand GroupFooter1;
        private DevExpress.XtraReports.UI.GroupFooterBand GroupFooter2;
        private DevExpress.XtraReports.UI.XRLabel xrVenc;
        private DevExpress.XtraReports.UI.XRLabel xrX0;
        private DevExpress.XtraReports.UI.XRLabel xrtotal;
        private DevExpress.XtraReports.UI.XRLabel xrLabel3;
        private DevExpress.XtraReports.UI.XRLabel xrNumeroBol;
        private DevExpress.XtraReports.UI.XRLabel xrResp;
        private DevExpress.XtraReports.UI.XRLabel xrVencTit;
        private DevExpress.XtraReports.UI.XRLabel xrX0Tit;
        private DevExpress.XtraReports.UI.XRLabel xrTotalTitulo;
        private DevExpress.XtraReports.UI.XRLabel xrLabel34;
        private DevExpress.XtraReports.UI.XRLabel xrNumeroBolTit;
        private DevExpress.XtraReports.UI.XRLabel xrRespTit;
        private DevExpress.XtraReports.UI.XRLabel xrSubTotalG;
        private DevExpress.XtraReports.UI.XRLabel xrX0SutTot;
        private DevExpress.XtraReports.UI.XRLabel xrSubTotalTit;
        private DevExpress.XtraReports.UI.XRLabel xrTotalG;
        private DevExpress.XtraReports.UI.XRLabel xrmarcador;
        private DevExpress.XtraReports.UI.XRLabel xrSubTotalSum;
        private DevExpress.XtraReports.UI.XRLabel xrX0Tot;
        private DevExpress.XtraReports.UI.ReportFooterBand ReportFooter;
        private DevExpress.XtraReports.UI.XRChart xrChart1;
        private GrBalancete.dBalancete dBalancete1;
        private DevExpress.XtraReports.UI.CalculatedField calculatedField1;
        private DevExpress.XtraReports.UI.CalculatedField calculadoGrupo;
        private DevExpress.XtraReports.Parameters.Parameter NumeroBoleto;
        private DevExpress.XtraReports.Parameters.Parameter Partestacolunas;
        private DevExpress.XtraReports.Parameters.Parameter Marcadores;
        private DevExpress.XtraReports.Parameters.Parameter ComLinhas;
        private DevExpress.XtraReports.UI.XRLabel xrPag;
        private DevExpress.XtraReports.UI.XRLabel xrPagT;
        private GrBalancete.dBalanceteTableAdapters.BALancetesTableAdapter bALancetesTableAdapter;
        /// <summary>
        /// Obs
        /// </summary>
        public DevExpress.XtraReports.UI.XRRichText xrRichTextCol;        
    }
}
