﻿using System;
using System.Drawing;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using DevExpress.XtraReports.UI;
using Balancete.GrBalancete;

namespace Balancete.Relatorios.Inadimplencia
{
    /// <summary>
    /// Impresso inadimplência
    /// </summary>
    public partial class ImpInadimplencia : dllImpresso.ImpLogoCond
    {
        private balancete balancete1;

        /// <summary>
        /// Construtor
        /// </summary>
        public ImpInadimplencia()
        {
            InitializeComponent();
        }

        /// <summary>
        /// Construitor
        /// </summary>
        /// <param name="_balancete"></param>
        public ImpInadimplencia(GrBalancete.balancete _balancete)
        {
            InitializeComponent();

            balancete1 = _balancete;
                                   
            SetNome(balancete1.rowCON.CONNome);
            xrTitulo.Text = string.Format("Inadimplência Total\r\n({0:dd/MM/yyyy} a {1:dd/MM/yyyy})", balancete1.DataI, balancete1.DataF);

            dInadimplencia1.Carregar(balancete1.CON, balancete1.DataI, balancete1.DataF);
            dInadimplencia1.Inadimp.DefaultView.Sort = "BLOAPT,BOLVencto";
            xrDataI.Text = string.Format("{0:dd/MM/yyyy}", balancete1.DataI);
            xrDataF.Text = string.Format("{0:dd/MM/yyyy}", balancete1.DataF);
            xrTitA.Text = string.Format("Inadimplência em {0:dd/MM/yyyy}:",balancete1.DataI);
            xrTitF.Text = string.Format("Inadimplência em {0:dd/MM/yyyy}:",balancete1.DataF);
            xrV7.Text = string.Format("{0:n2}",dInadimplencia1.V7);
            xrV8.Text = string.Format("{0:n2}", dInadimplencia1.V8);
            xrV9.Text = string.Format("{0:n2}", dInadimplencia1.V9);
            xrV10.Text = string.Format("{0:n2}", dInadimplencia1.V10);
        }

        private void DrawDegrade(object sender, DrawEventArgs e)
        {
            Degrade(sender, e);
        }

    }
}
