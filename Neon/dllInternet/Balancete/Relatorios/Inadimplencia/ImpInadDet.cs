﻿using Balancete.GrBalancete;
using System;

namespace Balancete.Relatorios.Inadimplencia
{
    /// <summary>
    /// Impresso para inadimplência detalhada
    /// </summary>
    public partial class ImpInadDet : dllImpresso.ImpLogoCond
    {
        private balancete balancete1;

        /// <summary>
        /// Construtor básico
        /// </summary>
        public ImpInadDet()
        {
            InitializeComponent();
        }

        /// <summary>
        /// Construtor
        /// </summary>
        /// <param name="_balancete"></param>
        public ImpInadDet(GrBalancete.balancete _balancete)
        {
            InitializeComponent();
            balancete1 = _balancete;
            SetNome(balancete1.rowCON.CONNome);
            xrTitulo.Text = string.Format("Inadimplência Detalhada em {0:dd/MM/yyyy})", balancete1.DataF);
            _balancete.DInadimplencia.Carregar(balancete1.CON, balancete1.DataI, balancete1.DataF);
            objectDataSource1.DataSource = _balancete.DInadimplencia;
            CriaColunasDinamicas(_balancete.DInadimplencia.ColunasDinamicasInad);
        }

    }
}
