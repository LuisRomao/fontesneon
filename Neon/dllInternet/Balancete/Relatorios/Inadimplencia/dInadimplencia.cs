﻿using System.Collections.Generic;
using VirMSSQL;
using VirEnumeracoesNeon;

namespace Balancete.Relatorios.Inadimplencia
{


    public partial class dInadimplencia
    {
        private dInadimplenciaTableAdapters.InadimpTableAdapter inadimpTableAdapter;
        /// <summary>
        /// TableAdapter padrão para tabela: Inadimp
        /// </summary>
        public dInadimplenciaTableAdapters.InadimpTableAdapter InadimpTableAdapter
        {
            get
            {
                if (inadimpTableAdapter == null)
                {
                    inadimpTableAdapter = new dInadimplenciaTableAdapters.InadimpTableAdapter();
                    inadimpTableAdapter.TrocarStringDeConexao();
                };
                return inadimpTableAdapter;
            }
        }

        private dInadimplenciaTableAdapters.ColunasTableAdapter colunasTableAdapter;

        /// <summary>
        /// TableAdapter padrão para tabela: Colunas
        /// </summary>
        public dInadimplenciaTableAdapters.ColunasTableAdapter ColunasTableAdapter
        {
            get
            {
                if (colunasTableAdapter == null)
                {
                    colunasTableAdapter = new dInadimplenciaTableAdapters.ColunasTableAdapter();
                    colunasTableAdapter.TrocarStringDeConexao();
                };
                return colunasTableAdapter;
            }
        }

        private bool VerificaErro(int BOL, InadimpRow row)
        {
            string comando =
"SELECT     ACOxBOL.ACO, ACOxBOL.BOL, ACOxBOL.ACOBOLOriginal, ACOrdos.ACOStatus, BOLetos.BOLVencto, \r\n" +
"                      BOletoDetalhe.BODAcordo_BOL\r\n" +
"FROM         ACOxBOL INNER JOIN\r\n" +
"                      ACOrdos ON ACOxBOL.ACO = ACOrdos.ACO INNER JOIN\r\n" +
"                      ACOxBOL AS ACOxBOL_1 ON ACOrdos.ACO = ACOxBOL_1.ACO INNER JOIN\r\n" +
"                      BOLetos ON ACOxBOL_1.BOL = BOLetos.BOL INNER JOIN\r\n" +
"                      BOletoDetalhe ON BOLetos.BOL = BOletoDetalhe.BOD_BOL\r\n" +
"WHERE     (ACOxBOL.BOL = @P1) AND (ACOxBOL_1.ACOBOLOriginal = 0) AND \r\n" +
"(BOletoDetalhe.BODAcordo_BOL = @P1) AND (ACOrdos.ACOStatus <> 3);";
            System.Data.DataRow DR = TableAdapter.ST().BuscaSQLRow(comando, BOL);
            if (DR == null)
                return true;
            else
            {
                row.Acordo = (int)DR["ACO"];
                row.DataAcordo = (System.DateTime)DR["BOLVencto"];
                //TableAdapter.ST().ExecutarSQLNonQuery("update boletos set bolstatus = -1 where bol = @P1", row.BOL);
                return false;
            }
            //return !TableAdapter.ST().EstaCadastrado(comando, BOL);
        }

        /// <summary>
        /// 
        /// </summary>
        public decimal V7;
        /// <summary>
        /// 
        /// </summary>
        public decimal V8;
        /// <summary>
        /// 
        /// </summary>
        public decimal V9;
        /// <summary>
        /// 
        /// </summary>
        public decimal V10;


        private bool gravarArquivoVerificador = false;

        /// <summary>
        /// Carrega
        /// </summary>
        /// <param name="CON"></param>
        /// <param name="DI"></param>
        /// <param name="DF"></param>
        /// <returns></returns>
        public int Carregar(int CON, System.DateTime DI, System.DateTime DF)
        {
            if ((CompontesBasicos.FormPrincipalBase.USULogado == 30) && !CompontesBasicosProc.FuncoesBasicasProc.EstaEmProducao)
                gravarArquivoVerificador = true;
            string verificador = "";
            V7 = V8 = V9 = V10 = 0;
            DF = DF.AddDays(1);
            InadimpTableAdapter.Fill(Inadimp, CON, DF, DI);
            ColunasTableAdapter.Fill(Colunas, CON, DF);
            List<InadimpRow> Errados = new System.Collections.Generic.List<InadimpRow>();
            List<InadimpRow> Quitados = new System.Collections.Generic.List<InadimpRow>();
            foreach (InadimpRow row in Inadimp)
            {
                if ((row.BOLStatus == 0) && (row.BOLCancelado))
                {
                    TableAdapter.ST().ExecutarSQLNonQuery("update boletos set BOLStatus = -1 where BOLStatus = 0 and BOLCancelado = 1 and bol = @P1", row.BOL);
                    Errados.Add(row);
                    continue;
                }

                if (row.BOLStatus == (int)StatusBoleto.OriginalAcordo)
                {
                    if (VerificaErro(row.BOL, row))
                    {
                        //row.Acordo = 0;
                        //Errados.Add(row);
                        //continue;
                    }

                }

                if (row.BOLStatus == (int)StatusBoleto.ParcialAcordo)
                {
                    string comandoAchaACO =
"SELECT     ACOrdos_1.ACO\r\n" +
"FROM         ACOxBOL INNER JOIN\r\n" +
"                      ACOrdos ON ACOxBOL.ACO = ACOrdos.ACO INNER JOIN\r\n" +
"                      ACOxBOL AS ACOxBOL_1 ON ACOrdos.ACO = ACOxBOL_1.ACO INNER JOIN\r\n" +
"                      ACOxBOL AS ACOxBOL_2 ON ACOxBOL_1.BOL = ACOxBOL_2.BOL INNER JOIN\r\n" +
"                      ACOrdos AS ACOrdos_1 ON ACOxBOL_2.ACO = ACOrdos_1.ACO\r\n" +
"WHERE     (ACOxBOL.BOL = @P1) AND (ACOxBOL_1.ACOBOLOriginal = 1) AND (ACOrdos.ACOStatus = 10) AND (ACOrdos_1.ACOStatus IN (1, 2)) AND \r\n" +
"                      (ACOxBOL_2.ACOBOLOriginal = 1);";
                    int? ACO = TableAdapter.ST().BuscaEscalar_int(comandoAchaACO, row.BOL);
                    if (ACO.HasValue)
                        row.Acordo = ACO.Value;
                }

                row.BLOAPT = row.BLOCodigo == "SB" ? "" : row.BLOCodigo + " ";
                row.BLOAPT += row.APTNumero;
                row.PI = row.BOLProprietario ? "P" : "I";

                if (row.IsBOLPagamentoNull() || (row.BOLPagamento >= DF))
                {
                    if (!row.IsBOLPagamentoNull())
                        row.SetBOLPagamentoNull();
                    row.ValFinal = row.BOLValorPrevisto;
                    V9 += row.BOLValorPrevisto;
                    if (row.BOLVencto < DI)
                    {
                        row.ValAnterior = row.BOLValorPrevisto;
                        V8 += row.BOLValorPrevisto;
                    }
                    else
                    {
                        row.ValMes = row.BOLValorPrevisto;
                        V7 += row.BOLValorPrevisto;
                        if (gravarArquivoVerificador)
                            verificador += string.Format("{0};{1}\r\n", row.BLOAPT, row.BOLValorPrevisto);
                    }
                }
                else
                {
                    if (row.BOLVencto < DI)
                    {
                        row.ValAnterior = row.BOLValorPrevisto;
                        row.ValRec = row.BOLValorPrevisto;
                        V8 += row.BOLValorPrevisto;
                        V10 += row.BOLValorPrevisto;
                    }
                    else
                        Quitados.Add(row);
                }
                PopulaColunas(row);
            }
            foreach (InadimpRow row in Errados)
                row.Delete();
            foreach (InadimpRow row in Quitados)
                row.Delete();
            Inadimp.AcceptChanges();
            if (gravarArquivoVerificador)
                System.IO.File.WriteAllText(@"c:\lixo\v2.csv", verificador);
            return Inadimp.Count;
        }

        private void PopulaColunas(InadimpRow row)
        {
            if (ColunasDinamicasInad == null)
                ColunasDinamicasInad = new SortedList<string, string>();
            decimal TotColunas = 0;
            foreach (ColunasRow rowBOD in row.GetColunasRows())
            {
                if (!ColunasDinamicasInad.ContainsKey(rowBOD.BOD_PLA))
                {
                    Framework.datasets.dPLAnocontas.PLAnocontasRow PLArow = Framework.datasets.dPLAnocontas.dPLAnocontasSt.PLAnocontas.FindByPLA(rowBOD.BOD_PLA);
                    ColunasDinamicasInad.Add(rowBOD.BOD_PLA, PLArow.PLADescricaoRes);
                    Inadimp.Columns.Add(rowBOD.BOD_PLA, typeof(decimal));                    
                }
                decimal ValAnte = row[rowBOD.BOD_PLA] == System.DBNull.Value ? 0 : (decimal)row[rowBOD.BOD_PLA];
                row[rowBOD.BOD_PLA] = ValAnte + rowBOD.BODValor;
                TotColunas += rowBOD.BODValor;
            }
            if ((TotColunas != 0) && (TotColunas != row.BOLValorPrevisto))
            {
                System.Console.Write("******************\r\nChecar Boleto {0}\r\n\r\n", row.BOL);
                string PrimeiraColuna = ColunasDinamicasInad.Keys[0];
                decimal ValAnte = row[PrimeiraColuna] == System.DBNull.Value ? 0 : (decimal)row[PrimeiraColuna];
                    row[PrimeiraColuna] = ValAnte + (row.BOLValorPrevisto - TotColunas);
                
            }
        }

        /// <summary>
        /// Colunas dinâmicas
        /// </summary>
        public SortedList<string, string> ColunasDinamicasInad;

    }
}
