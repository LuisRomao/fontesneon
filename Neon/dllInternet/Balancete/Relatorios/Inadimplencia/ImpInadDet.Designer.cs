﻿namespace Balancete.Relatorios.Inadimplencia
{
    partial class ImpInadDet
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            DevExpress.XtraReports.UI.XRSummary xrSummary1 = new DevExpress.XtraReports.UI.XRSummary();
            DevExpress.XtraReports.UI.XRSummary xrSummary2 = new DevExpress.XtraReports.UI.XRSummary();
            this.xrLabelBloAPT = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel3 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel4 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel5 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel6 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel7 = new DevExpress.XtraReports.UI.XRLabel();
            this.DirTotal = new DevExpress.XtraReports.UI.XRLabel();
            this.DirTituloTotal = new DevExpress.XtraReports.UI.XRLabel();
            this.Modelo = new DevExpress.XtraReports.UI.XRLabel();
            this.ModeloTitulo = new DevExpress.XtraReports.UI.XRLabel();
            this.GroupFooter1 = new DevExpress.XtraReports.UI.GroupFooterBand();
            this.ModeloTotal = new DevExpress.XtraReports.UI.XRLabel();
            this.DirTotalSoma = new DevExpress.XtraReports.UI.XRLabel();
            this.objectDataSource1 = new DevExpress.DataAccess.ObjectBinding.ObjectDataSource(this.components);
            this.Numerinho = new DevExpress.XtraReports.UI.XRLabel();
            ((System.ComponentModel.ISupportInitialize)(this.objectDataSource1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            // 
            // PageHeader
            // 
            this.PageHeader.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.ModeloTitulo,
            this.DirTituloTotal,
            this.xrLabel7,
            this.xrLabel5,
            this.xrLabel3});
            this.PageHeader.HeightF = 394.125F;
            this.PageHeader.Controls.SetChildIndex(this.xrLabel3, 0);
            this.PageHeader.Controls.SetChildIndex(this.xrLabel5, 0);
            this.PageHeader.Controls.SetChildIndex(this.xrLabel7, 0);
            this.PageHeader.Controls.SetChildIndex(this.DirTituloTotal, 0);
            this.PageHeader.Controls.SetChildIndex(this.ModeloTitulo, 0);
            this.PageHeader.Controls.SetChildIndex(this.xrTitulo, 0);
            // 
            // Detail
            // 
            this.Detail.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.Modelo,
            this.DirTotal,
            this.xrLabel6,
            this.xrLabel4,
            this.xrLabelBloAPT});
            this.Detail.HeightF = 42.0001F;
            this.Detail.SortFields.AddRange(new DevExpress.XtraReports.UI.GroupField[] {
            new DevExpress.XtraReports.UI.GroupField("BLOAPT", DevExpress.XtraReports.UI.XRColumnSortOrder.Ascending),
            new DevExpress.XtraReports.UI.GroupField("BOLVencto", DevExpress.XtraReports.UI.XRColumnSortOrder.Ascending)});
            // 
            // xrLabelBloAPT
            // 
            this.xrLabelBloAPT.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "BLOAPT")});
            this.xrLabelBloAPT.Dpi = 254F;
            this.xrLabelBloAPT.Font = new System.Drawing.Font("Times New Roman", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabelBloAPT.LocationFloat = new DevExpress.Utils.PointFloat(0F, 0F);
            this.xrLabelBloAPT.Name = "xrLabelBloAPT";
            this.xrLabelBloAPT.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrLabelBloAPT.SizeF = new System.Drawing.SizeF(140F, 42F);
            this.xrLabelBloAPT.StylePriority.UseFont = false;
            this.xrLabelBloAPT.Text = "xrLabelBloAPT";
            // 
            // xrLabel3
            // 
            this.xrLabel3.Dpi = 254F;
            this.xrLabel3.LocationFloat = new DevExpress.Utils.PointFloat(1.629372E-05F, 323.9342F);
            this.xrLabel3.Name = "xrLabel3";
            this.xrLabel3.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrLabel3.SizeF = new System.Drawing.SizeF(120.5219F, 58.41998F);
            this.xrLabel3.StylePriority.UseTextAlignment = false;
            this.xrLabel3.Text = "Apto";
            this.xrLabel3.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrLabel4
            // 
            this.xrLabel4.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "PI")});
            this.xrLabel4.Dpi = 254F;
            this.xrLabel4.Font = new System.Drawing.Font("Times New Roman", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel4.LocationFloat = new DevExpress.Utils.PointFloat(140F, 0F);
            this.xrLabel4.Name = "xrLabel4";
            this.xrLabel4.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrLabel4.SizeF = new System.Drawing.SizeF(26.59636F, 41.99999F);
            this.xrLabel4.StylePriority.UseFont = false;
            this.xrLabel4.Text = "xrLabel4";
            // 
            // xrLabel5
            // 
            this.xrLabel5.Dpi = 254F;
            this.xrLabel5.LocationFloat = new DevExpress.Utils.PointFloat(120.5219F, 323.9342F);
            this.xrLabel5.Name = "xrLabel5";
            this.xrLabel5.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrLabel5.SizeF = new System.Drawing.SizeF(82.02084F, 58.42001F);
            this.xrLabel5.StylePriority.UseTextAlignment = false;
            this.xrLabel5.Text = "Resp";
            this.xrLabel5.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrLabel6
            // 
            this.xrLabel6.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "BOLVencto", "{0:dd/MM/yyyy}")});
            this.xrLabel6.Dpi = 254F;
            this.xrLabel6.Font = new System.Drawing.Font("Times New Roman", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel6.LocationFloat = new DevExpress.Utils.PointFloat(166.5963F, 0.0001605967F);
            this.xrLabel6.Name = "xrLabel6";
            this.xrLabel6.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrLabel6.SizeF = new System.Drawing.SizeF(172.4037F, 41.99994F);
            this.xrLabel6.StylePriority.UseFont = false;
            this.xrLabel6.Text = "xrLabel6";
            // 
            // xrLabel7
            // 
            this.xrLabel7.Dpi = 254F;
            this.xrLabel7.LocationFloat = new DevExpress.Utils.PointFloat(202.5428F, 323.9342F);
            this.xrLabel7.Name = "xrLabel7";
            this.xrLabel7.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrLabel7.SizeF = new System.Drawing.SizeF(136.4572F, 58.41998F);
            this.xrLabel7.StylePriority.UseTextAlignment = false;
            this.xrLabel7.Text = "Vencto";
            this.xrLabel7.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // DirTotal
            // 
            this.DirTotal.BackColor = System.Drawing.Color.Gainsboro;
            this.DirTotal.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "BOLValorPrevisto", "{0:n2}")});
            this.DirTotal.Dpi = 254F;
            this.DirTotal.Font = new System.Drawing.Font("Times New Roman", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DirTotal.LocationFloat = new DevExpress.Utils.PointFloat(493F, 0F);
            this.DirTotal.Name = "DirTotal";
            this.DirTotal.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.DirTotal.SizeF = new System.Drawing.SizeF(186.7271F, 41.99993F);
            this.DirTotal.StylePriority.UseBackColor = false;
            this.DirTotal.StylePriority.UseFont = false;
            this.DirTotal.StylePriority.UseTextAlignment = false;
            this.DirTotal.Text = "DirTotal";
            this.DirTotal.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight;
            // 
            // DirTituloTotal
            // 
            this.DirTituloTotal.Dpi = 254F;
            this.DirTituloTotal.LocationFloat = new DevExpress.Utils.PointFloat(518.6328F, 323.9341F);
            this.DirTituloTotal.Name = "DirTituloTotal";
            this.DirTituloTotal.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.DirTituloTotal.SizeF = new System.Drawing.SizeF(146.4344F, 58.42001F);
            this.DirTituloTotal.StylePriority.UseTextAlignment = false;
            this.DirTituloTotal.Text = "Total";
            this.DirTituloTotal.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // Modelo
            // 
            this.Modelo.Dpi = 254F;
            this.Modelo.Font = new System.Drawing.Font("Times New Roman", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Modelo.LocationFloat = new DevExpress.Utils.PointFloat(339F, 0F);
            this.Modelo.Name = "Modelo";
            this.Modelo.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.Modelo.SizeF = new System.Drawing.SizeF(154F, 41.99986F);
            this.Modelo.StylePriority.UseFont = false;
            this.Modelo.StylePriority.UseTextAlignment = false;
            this.Modelo.Text = "Valor";
            this.Modelo.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight;
            // 
            // ModeloTitulo
            // 
            this.ModeloTitulo.Dpi = 254F;
            this.ModeloTitulo.Font = new System.Drawing.Font("Times New Roman", 6.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ModeloTitulo.LocationFloat = new DevExpress.Utils.PointFloat(339F, 323.9341F);
            this.ModeloTitulo.Name = "ModeloTitulo";
            this.ModeloTitulo.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.ModeloTitulo.SizeF = new System.Drawing.SizeF(154F, 58.42001F);
            this.ModeloTitulo.StylePriority.UseFont = false;
            this.ModeloTitulo.StylePriority.UseTextAlignment = false;
            this.ModeloTitulo.Text = "Valor";
            this.ModeloTitulo.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // GroupFooter1
            // 
            this.GroupFooter1.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.Numerinho,
            this.ModeloTotal,
            this.DirTotalSoma});
            this.GroupFooter1.Dpi = 254F;
            this.GroupFooter1.HeightF = 75.91798F;
            this.GroupFooter1.Name = "GroupFooter1";
            // 
            // ModeloTotal
            // 
            this.ModeloTotal.Dpi = 254F;
            this.ModeloTotal.Font = new System.Drawing.Font("Times New Roman", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ModeloTotal.LocationFloat = new DevExpress.Utils.PointFloat(339.0001F, 8.918013F);
            this.ModeloTotal.Name = "ModeloTotal";
            this.ModeloTotal.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.ModeloTotal.SizeF = new System.Drawing.SizeF(153.9999F, 41.99986F);
            this.ModeloTotal.StylePriority.UseFont = false;
            this.ModeloTotal.StylePriority.UseTextAlignment = false;
            xrSummary1.FormatString = "{0:n2}";
            xrSummary1.Running = DevExpress.XtraReports.UI.SummaryRunning.Group;
            this.ModeloTotal.Summary = xrSummary1;
            this.ModeloTotal.Text = "Valor";
            this.ModeloTotal.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight;
            // 
            // DirTotalSoma
            // 
            this.DirTotalSoma.BackColor = System.Drawing.Color.Gainsboro;
            this.DirTotalSoma.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "BOLValorPrevisto")});
            this.DirTotalSoma.Dpi = 254F;
            this.DirTotalSoma.Font = new System.Drawing.Font("Times New Roman", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DirTotalSoma.LocationFloat = new DevExpress.Utils.PointFloat(493F, 8.917933F);
            this.DirTotalSoma.Name = "DirTotalSoma";
            this.DirTotalSoma.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.DirTotalSoma.SizeF = new System.Drawing.SizeF(186.7271F, 41.99993F);
            this.DirTotalSoma.StylePriority.UseBackColor = false;
            this.DirTotalSoma.StylePriority.UseFont = false;
            this.DirTotalSoma.StylePriority.UseTextAlignment = false;
            xrSummary2.FormatString = "{0:n2}";
            xrSummary2.Running = DevExpress.XtraReports.UI.SummaryRunning.Group;
            this.DirTotalSoma.Summary = xrSummary2;
            this.DirTotalSoma.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight;
            // 
            // objectDataSource1
            // 
            this.objectDataSource1.DataMember = "Inadimp";
            this.objectDataSource1.DataSource = typeof(Balancete.Relatorios.Inadimplencia.dInadimplencia);
            this.objectDataSource1.Name = "objectDataSource1";
            // 
            // Numerinho
            // 
            this.Numerinho.Dpi = 254F;
            this.Numerinho.Font = new System.Drawing.Font("Times New Roman", 6.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Numerinho.LocationFloat = new DevExpress.Utils.PointFloat(679.7271F, 0F);
            this.Numerinho.Name = "Numerinho";
            this.Numerinho.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.Numerinho.SizeF = new System.Drawing.SizeF(52.14545F, 33.42F);
            this.Numerinho.StylePriority.UseFont = false;
            this.Numerinho.Text = "(10)";
            // 
            // ImpInadDet
            // 
            this.Bands.AddRange(new DevExpress.XtraReports.UI.Band[] {
            this.Detail,
            this.PageHeader,
            this.GroupFooter1});
            this.ComponentStorage.AddRange(new System.ComponentModel.IComponent[] {
            this.objectDataSource1});
            this.DataSource = this.objectDataSource1;
            this.FilterString = "[ValFinal] Is Not Null";
            this.ModeloBind = this.xrLabelBloAPT;
            this.ModeloDin = this.Modelo;
            this.ModeloDinTitulo = this.ModeloTitulo;
            this.ModeloDinTotal = this.ModeloTotal;
            this.MoveDireita = new DevExpress.XtraReports.UI.XRControl[] {
        ((DevExpress.XtraReports.UI.XRControl)(this.DirTituloTotal)),
        ((DevExpress.XtraReports.UI.XRControl)(this.DirTotal)),
        ((DevExpress.XtraReports.UI.XRControl)(this.DirTotalSoma)),
        ((DevExpress.XtraReports.UI.XRControl)(this.Numerinho))};
            this.NCorte = new int[] {
        7,
        13,
        14,
        16,
        18,
        20};
            this.Version = "16.2";
            this.Controls.SetChildIndex(this.GroupFooter1, 0);
            this.Controls.SetChildIndex(this.PageHeader, 0);
            this.Controls.SetChildIndex(this.Detail, 0);
            ((System.ComponentModel.ISupportInitialize)(this.objectDataSource1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();

        }


        #endregion

        /// <summary>
        /// 
        /// </summary>
        public DevExpress.DataAccess.ObjectBinding.ObjectDataSource objectDataSource1;
        private DevExpress.XtraReports.UI.XRLabel DirTituloTotal;
        private DevExpress.XtraReports.UI.XRLabel xrLabel7;
        private DevExpress.XtraReports.UI.XRLabel xrLabel5;
        private DevExpress.XtraReports.UI.XRLabel xrLabel3;
        private DevExpress.XtraReports.UI.XRLabel DirTotal;
        private DevExpress.XtraReports.UI.XRLabel xrLabel6;
        private DevExpress.XtraReports.UI.XRLabel xrLabel4;
        private DevExpress.XtraReports.UI.XRLabel xrLabelBloAPT;
        private DevExpress.XtraReports.UI.XRLabel ModeloTitulo;
        private DevExpress.XtraReports.UI.XRLabel Modelo;
        private DevExpress.XtraReports.UI.GroupFooterBand GroupFooter1;
        private DevExpress.XtraReports.UI.XRLabel ModeloTotal;
        private DevExpress.XtraReports.UI.XRLabel DirTotalSoma;
        private DevExpress.XtraReports.UI.XRLabel Numerinho;
    }
}
