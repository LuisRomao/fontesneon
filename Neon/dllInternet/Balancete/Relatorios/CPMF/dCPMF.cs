﻿namespace Balancete.Relatorios.CPMF
{


    partial class dCPMF
    {
        private dCPMFTableAdapters.CPMFTableAdapter cPMFTableAdapter;
        /// <summary>
        /// TableAdapter padrão para tabela: CPMF
        /// </summary>
        public dCPMFTableAdapters.CPMFTableAdapter CPMFTableAdapter
        {
            get
            {
                if (cPMFTableAdapter == null)
                {
                    cPMFTableAdapter = new dCPMFTableAdapters.CPMFTableAdapter();
                    cPMFTableAdapter.TrocarStringDeConexao();
                };
                return cPMFTableAdapter;
            }
        }
    }
}
