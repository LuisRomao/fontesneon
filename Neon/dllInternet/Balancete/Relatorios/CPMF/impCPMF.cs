﻿using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using DevExpress.XtraReports.UI;
using Balancete.GrBalancete;

namespace Balancete.Relatorios.CPMF
{
    /// <summary>
    /// Relatório de despesas bancárias - antigo CPMF
    /// </summary>
    public partial class impCPMF : dllImpresso.ImpLogoCond
    {
        private balancete balancete1;

        /// <summary>
        /// Construtor
        /// </summary>
        public impCPMF()
        {
            InitializeComponent();
        }

        /// <summary>
        /// Construtor
        /// </summary>
        /// <param name="_balancete"></param>
        public impCPMF(balancete _balancete):this()
        {
            balancete1 = _balancete;            
            SetNome(balancete1.rowCON.CONNome);
            xrTitulo.Text = string.Format("Demonstrativo de despesas bancárias\r\n({0:dd/MM/yyyy} a {1:dd/MM/yyyy})", balancete1.DataI, balancete1.DataF);
            dCPMF1.CPMFTableAdapter.Fill(dCPMF1.CPMF, balancete1.CON, balancete1.DataI, balancete1.DataF);            
        }

    }
}
