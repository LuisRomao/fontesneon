﻿namespace Balancete.Relatorios.Capa
{
    partial class ImpCapa
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            DevExpress.XtraPrinting.BarCode.Interleaved2of5Generator interleaved2of5Generator1 = new DevExpress.XtraPrinting.BarCode.Interleaved2of5Generator();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(ImpCapa));
            this.ReportFooter = new DevExpress.XtraReports.UI.ReportFooterBand();
            this.xrBarCode1 = new DevExpress.XtraReports.UI.XRBarCode();
            this.xrRichTextCapa = new DevExpress.XtraReports.UI.XRRichText();
            this.xrRichText1 = new DevExpress.XtraReports.UI.XRRichText();
            ((System.ComponentModel.ISupportInitialize)(this.xrRichTextCapa)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrRichText1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            // 
            // Detail
            // 
            this.Detail.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrRichText1});
            this.Detail.HeightF = 21.33736F;
            // 
            // ReportFooter
            // 
            this.ReportFooter.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrBarCode1,
            this.xrRichTextCapa});
            this.ReportFooter.Dpi = 254F;
            this.ReportFooter.HeightF = 141.5833F;
            this.ReportFooter.Name = "ReportFooter";
            this.ReportFooter.PrintAtBottom = true;
            // 
            // xrBarCode1
            // 
            this.xrBarCode1.Dpi = 254F;
            this.xrBarCode1.LocationFloat = new DevExpress.Utils.PointFloat(20.99997F, 35.58334F);
            this.xrBarCode1.Module = 2.25F;
            this.xrBarCode1.Name = "xrBarCode1";
            this.xrBarCode1.Padding = new DevExpress.XtraPrinting.PaddingInfo(25, 25, 0, 0, 254F);
            this.xrBarCode1.ShowText = false;
            this.xrBarCode1.SizeF = new System.Drawing.SizeF(318F, 106F);
            interleaved2of5Generator1.WideNarrowRatio = 3F;
            this.xrBarCode1.Symbology = interleaved2of5Generator1;
            this.xrBarCode1.Text = "1";
            // 
            // xrRichTextCapa
            // 
            this.xrRichTextCapa.Dpi = 254F;
            this.xrRichTextCapa.Font = new System.Drawing.Font("Times New Roman", 9.75F);
            this.xrRichTextCapa.LocationFloat = new DevExpress.Utils.PointFloat(0F, 0F);
            this.xrRichTextCapa.Name = "xrRichTextCapa";
            this.xrRichTextCapa.SerializableRtfString = resources.GetString("xrRichTextCapa.SerializableRtfString");
            this.xrRichTextCapa.SizeF = new System.Drawing.SizeF(1799F, 5.291656F);
            // 
            // xrRichText1
            // 
            this.xrRichText1.Dpi = 254F;
            this.xrRichText1.Font = new System.Drawing.Font("Times New Roman", 9.75F);
            this.xrRichText1.LocationFloat = new DevExpress.Utils.PointFloat(0F, 0F);
            this.xrRichText1.Name = "xrRichText1";
            this.xrRichText1.SerializableRtfString = resources.GetString("xrRichText1.SerializableRtfString");
            this.xrRichText1.SizeF = new System.Drawing.SizeF(1799F, 5F);
            // 
            // ImpCapa
            // 
            this.Bands.AddRange(new DevExpress.XtraReports.UI.Band[] {
            this.Detail,
            this.PageHeader,
            this.ReportFooter,
            this.bottomMarginBand1});
            this.Version = "17.1";
            this.Controls.SetChildIndex(this.bottomMarginBand1, 0);
            this.Controls.SetChildIndex(this.ReportFooter, 0);
            this.Controls.SetChildIndex(this.PageHeader, 0);
            this.Controls.SetChildIndex(this.Detail, 0);
            ((System.ComponentModel.ISupportInitialize)(this.xrRichTextCapa)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrRichText1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();

        }

        #endregion

        private DevExpress.XtraReports.UI.ReportFooterBand ReportFooter;
        /// <summary>
        /// Obs
        /// </summary>
        public DevExpress.XtraReports.UI.XRRichText xrRichTextCapa;
        /// <summary>
        /// 
        /// </summary>
        public DevExpress.XtraReports.UI.XRRichText xrRichText1;
        /// <summary>
        /// Código de barras
        /// </summary>
        public DevExpress.XtraReports.UI.XRBarCode xrBarCode1;
    }
}
