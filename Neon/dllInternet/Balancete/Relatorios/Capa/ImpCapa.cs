﻿using Balancete.GrBalancete;

namespace Balancete.Relatorios.Capa
{
    /// <summary>
    /// Impresso da capa do balancete
    /// </summary>
    public partial class ImpCapa : dllImpresso.ImpLogoCond
    {
        private balancete balancete;

        /// <summary>
        /// Construtor padrão (não usar)
        /// </summary>
        public ImpCapa()
        {
            InitializeComponent();
            QuadroRTFBase = xrRichText1;
        }

        /// <summary>
        /// Construtor
        /// </summary>
        /// <param name="_balancete"></param>
        public ImpCapa(balancete _balancete):this()
        {            
            balancete = _balancete;
            SetNome(FrameworkProc.datasets.dCondominiosAtivos.dCondominiosTodosST.CONDOMINIOS.FindByCON(balancete.CON).CONNome);
            xrTitulo.Text = string.Format("Balancete {0:dd/MM/yy} a {1:dd/MM/yy} ", balancete.DataI, balancete.DataF);
            xrBarCode1.Text = string.Format("{0}{1:00}{2}",Framework.DSCentral.EMP,(int)VirEnumeracoesNeon.TipoCodigoBarras.Balancete, balancete.BALrow.BAL);
        } 

    }
}
