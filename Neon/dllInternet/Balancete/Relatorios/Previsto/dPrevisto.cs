﻿namespace Balancete.Relatorios.Previsto 
{
    
    /// <summary>
    /// dPrevisto
    /// </summary>
    public partial class dPrevisto {
        partial class PrevistoDataTable
        {
        }

        //public Balancete.GrBalancete.balancete balancete1;

        private dPrevistoTableAdapters.PrevistoTableAdapter previstoTableAdapter;
        /// <summary>
        /// TableAdapter padrão para tabela: Previsto
        /// </summary>
        public dPrevistoTableAdapters.PrevistoTableAdapter PrevistoTableAdapter
        {
            get
            {
                if (previstoTableAdapter == null)
                {
                    previstoTableAdapter = new dPrevistoTableAdapters.PrevistoTableAdapter();
                    previstoTableAdapter.TrocarStringDeConexao();
                };
                return previstoTableAdapter;
            }
        }

        private enum status { aberto, pagoP}
        /*
        /// <summary>
        /// 
        /// </summary>        
        public decimal TPrevisto;
        /// <summary>
        /// 
        /// </summary>
        public decimal TInad;
        /// <summary>
        /// 
        /// </summary>
        public decimal TRecebidoMes;
        /// <summary>
        /// 
        /// </summary>
        public decimal TAntecipado;
        */
        //private bool gravarArquivoVerificador = false;

        /// <summary>
        /// 
        /// </summary>                
        public void Ajuste(Balancete.GrBalancete.balancete balancete1)
        {
            //string verificador = "";
            balancete1.TPrevisto = balancete1.TInad = balancete1.TRecebidoMes = balancete1.TAntecipado = 0;
            foreach (PrevistoRow rowBOL in Previsto)
            {
                balancete1.TPrevisto += rowBOL.BOLValorPrevisto;
                rowBOL.BLOAPT = rowBOL.BLOCodigo == "SB" ? "" : rowBOL.BLOCodigo + " ";
                rowBOL.BLOAPT += rowBOL.APTNumero;
                rowBOL.PI = rowBOL.BOLProprietario ? "P" : "I";
                if ((rowBOL.IsBOLPagamentoNull()) || (rowBOL.BOLPagamento > balancete1.DataF))
                {
                    rowBOL.Status = "Inadimplente";
                    balancete1.TInad += rowBOL.BOLValorPrevisto;
                    //if (gravarArquivoVerificador)
                    //  verificador += string.Format("{0};{1}\r\n", rowBOL.BLOAPT, rowBOL.BOLValorPrevisto);
                    rowBOL.SetBOLPagamentoNull();
                }
                else if (rowBOL.BOLPagamento >= balancete1.DataI)
                {

                    rowBOL.Status = "Recebido no período";
                    balancete1.TRecebidoMes += rowBOL.BOLValorPrevisto;
                    rowBOL.ValorPrincipal = rowBOL.BOLValorPrevisto;
                    if (rowBOL.BOLValorPago != rowBOL.BOLValorPrevisto)
                        rowBOL.Multas = rowBOL.BOLValorPago - rowBOL.BOLValorPrevisto;
                }
                else
                {
                    rowBOL.Status = "Recebido antecipado";
                    balancete1.TAntecipado += rowBOL.BOLValorPrevisto;
                    rowBOL.Antecipado = rowBOL.BOLValorPrevisto;
                }
            }
            //if(gravarArquivoVerificador)
            //  System.IO.File.WriteAllText(@"c:\lixo\v1.csv", verificador);
        }
    }
}

