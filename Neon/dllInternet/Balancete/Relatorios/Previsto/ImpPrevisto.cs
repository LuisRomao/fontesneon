﻿using System;
using System.Drawing;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using DevExpress.XtraReports.UI;
using Balancete.GrBalancete;

namespace Balancete.Relatorios.Previsto
{
    /// <summary>
    /// Impesso Previsto
    /// </summary>
    public partial class ImpPrevisto : dllImpresso.ImpLogoCond
    {
        balancete balancete1;

        /// <summary>
        /// Construtor
        /// </summary>
        public ImpPrevisto()
        {
            InitializeComponent();
        }

        /// <summary>
        /// Construtor
        /// </summary>
        /// <param name="_balancete"></param>
        public ImpPrevisto(GrBalancete.balancete _balancete)
        {
            InitializeComponent();

            balancete1 = _balancete;
                                   
            SetNome(balancete1.rowCON.CONNome);
            xrTitulo.Text = string.Format("Previsto / Realizado\r\n({0:dd/MM/yyyy} a {1:dd/MM/yyyy})", balancete1.DataI, balancete1.DataF);
            DataSource = balancete1.DPrevisto1;
            //dPrevisto1.PrevistoTableAdapter.Fill(dPrevisto1.Previsto, balancete1.DataI, balancete1.DataF, balancete1.CON);
            //dPrevisto1.balancete1 = balancete1;
            //dPrevisto1.Ajuste();

            VPrevisto.Value = balancete1.TPrevisto;
            VInad.Value = balancete1.TInad;
            VRealizado.Value = balancete1.TAntecipado + balancete1.TRecebidoMes;
            if (balancete1.TPrevisto != 0)
                PInad.Value = balancete1.TInad / balancete1.TPrevisto;
            else
                PInad.Value = 0;
            xrChart1.Series[0].Points[0].Argument = string.Format("Rec. antes de {0:dd/MM/yyyy}",balancete1.DataI);
            xrChart1.Series[0].Points[2].Argument = string.Format("Aberto em {0:dd/MM/yyyy}", balancete1.DataF);
            xrChart1.Series[0].Points[0].Values[0] = (double)balancete1.TAntecipado;
            xrChart1.Series[0].Points[1].Values[0] = (double)balancete1.TRecebidoMes;
            xrChart1.Series[0].Points[2].Values[0] = (double)balancete1.TInad;
        }

        private void xrLabel4_Draw(object sender, DrawEventArgs e)
        {
            Degrade(sender, e);
        }

    }
}
