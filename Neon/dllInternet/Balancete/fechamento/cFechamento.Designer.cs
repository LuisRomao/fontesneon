﻿namespace Balancete.fechamento
{
    partial class cFechamento
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.dFechamento1 = new Balancete.fechamento.dFechamento();
            this.gridControl1 = new DevExpress.XtraGrid.GridControl();
            this.cardView1 = new DevExpress.XtraGrid.Views.Card.CardView();
            this.colCodcon = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSaldoGeral = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSaldoDisponível = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSaldoPoupança = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colAplicações = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colResgates = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colRentabilidade = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridControl2 = new DevExpress.XtraGrid.GridControl();
            this.cardView2 = new DevExpress.XtraGrid.Views.Card.CardView();
            this.colCodcon1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSaldoAnterior = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSaldoAplicaçãoFechamento = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSaldoPoupança13Fechamento = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSaldoPoupançaFRFechamento = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSaldoPoupançaObrasFechamento = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSaldoPoupançaSalãoFechamento = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSaldoAplicaçãoAutomáticaFechamento = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSaldoFechamento = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colDataInicial = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colDataFinal = new DevExpress.XtraGrid.Columns.GridColumn();
            this.simpleButton1 = new DevExpress.XtraEditors.SimpleButton();
            this.spinEditmes = new DevExpress.XtraEditors.SpinEdit();
            this.spinEditano = new DevExpress.XtraEditors.SpinEdit();
            ((System.ComponentModel.ISupportInitialize)(this.StyleController_F)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dFechamento1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cardView1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cardView2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.spinEditmes.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.spinEditano.Properties)).BeginInit();
            this.SuspendLayout();
            // 
            // ToolTipController_F
            // 
            this.ToolTipController_F.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.ToolTipController_F.Appearance.ForeColor = System.Drawing.Color.Black;
            this.ToolTipController_F.Appearance.Options.UseBackColor = true;
            this.ToolTipController_F.Appearance.Options.UseFont = true;
            this.ToolTipController_F.Appearance.Options.UseForeColor = true;
            this.ToolTipController_F.Appearance.Options.UseTextOptions = true;
            this.ToolTipController_F.Appearance.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            // 
            // StyleController_F
            // 
            this.StyleController_F.AppearanceDisabled.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this.StyleController_F.AppearanceDisabled.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this.StyleController_F.AppearanceDisabled.Options.UseBackColor = true;
            this.StyleController_F.AppearanceDisabled.Options.UseForeColor = true;
            this.StyleController_F.AppearanceFocused.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.StyleController_F.AppearanceFocused.Options.UseBackColor = true;
            // 
            // spellChecker1
            // 
            this.spellChecker1.OptionsSpelling.CheckSelectedTextFirst = DevExpress.Utils.DefaultBoolean.True;
            this.spellChecker1.OptionsSpelling.IgnoreEmails = DevExpress.Utils.DefaultBoolean.True;
            this.spellChecker1.OptionsSpelling.IgnoreMixedCaseWords = DevExpress.Utils.DefaultBoolean.False;
            this.spellChecker1.OptionsSpelling.IgnoreRepeatedWords = DevExpress.Utils.DefaultBoolean.False;
            this.spellChecker1.OptionsSpelling.IgnoreUpperCaseWords = DevExpress.Utils.DefaultBoolean.False;
            this.spellChecker1.OptionsSpelling.IgnoreUrls = DevExpress.Utils.DefaultBoolean.True;
            this.spellChecker1.OptionsSpelling.IgnoreWordsWithNumbers = DevExpress.Utils.DefaultBoolean.True;
            // 
            // dFechamento1
            // 
            this.dFechamento1.DataSetName = "dFechamento";
            this.dFechamento1.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // gridControl1
            // 
            this.gridControl1.DataMember = "Saldos de Fechamento";
            this.gridControl1.DataSource = this.dFechamento1;
            this.gridControl1.Dock = System.Windows.Forms.DockStyle.Top;
            this.gridControl1.Location = new System.Drawing.Point(0, 0);
            this.gridControl1.MainView = this.cardView1;
            this.gridControl1.Name = "gridControl1";
            this.gridControl1.Size = new System.Drawing.Size(894, 248);
            this.gridControl1.TabIndex = 0;
            this.gridControl1.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.cardView1});
            // 
            // cardView1
            // 
            this.cardView1.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colCodcon,
            this.colSaldoGeral,
            this.colSaldoDisponível,
            this.colSaldoPoupança,
            this.colAplicações,
            this.colResgates,
            this.colRentabilidade});
            this.cardView1.FocusedCardTopFieldIndex = 0;
            this.cardView1.GridControl = this.gridControl1;
            this.cardView1.MaximumCardColumns = 2;
            this.cardView1.MaximumCardRows = 1;
            this.cardView1.Name = "cardView1";
            this.cardView1.OptionsBehavior.AutoHorzWidth = true;
            this.cardView1.VertScrollVisibility = DevExpress.XtraGrid.Views.Base.ScrollVisibility.Auto;
            // 
            // colCodcon
            // 
            this.colCodcon.FieldName = "Codcon";
            this.colCodcon.Name = "colCodcon";
            this.colCodcon.Visible = true;
            this.colCodcon.VisibleIndex = 0;
            // 
            // colSaldoGeral
            // 
            this.colSaldoGeral.FieldName = "Saldo Geral";
            this.colSaldoGeral.Name = "colSaldoGeral";
            this.colSaldoGeral.Visible = true;
            this.colSaldoGeral.VisibleIndex = 1;
            // 
            // colSaldoDisponível
            // 
            this.colSaldoDisponível.FieldName = "Saldo Disponível";
            this.colSaldoDisponível.Name = "colSaldoDisponível";
            this.colSaldoDisponível.Visible = true;
            this.colSaldoDisponível.VisibleIndex = 2;
            // 
            // colSaldoPoupança
            // 
            this.colSaldoPoupança.FieldName = "Saldo Poupança";
            this.colSaldoPoupança.Name = "colSaldoPoupança";
            this.colSaldoPoupança.Visible = true;
            this.colSaldoPoupança.VisibleIndex = 3;
            // 
            // colAplicações
            // 
            this.colAplicações.FieldName = "Aplicações";
            this.colAplicações.Name = "colAplicações";
            this.colAplicações.Visible = true;
            this.colAplicações.VisibleIndex = 4;
            // 
            // colResgates
            // 
            this.colResgates.FieldName = "Resgates";
            this.colResgates.Name = "colResgates";
            this.colResgates.Visible = true;
            this.colResgates.VisibleIndex = 5;
            // 
            // colRentabilidade
            // 
            this.colRentabilidade.FieldName = "Rentabilidade";
            this.colRentabilidade.Name = "colRentabilidade";
            this.colRentabilidade.Visible = true;
            this.colRentabilidade.VisibleIndex = 6;
            // 
            // gridControl2
            // 
            this.gridControl2.DataMember = "Saldos e Poupança";
            this.gridControl2.DataSource = this.dFechamento1;
            this.gridControl2.Dock = System.Windows.Forms.DockStyle.Top;
            this.gridControl2.Location = new System.Drawing.Point(0, 248);
            this.gridControl2.MainView = this.cardView2;
            this.gridControl2.Name = "gridControl2";
            this.gridControl2.Size = new System.Drawing.Size(894, 297);
            this.gridControl2.TabIndex = 1;
            this.gridControl2.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.cardView2});
            // 
            // cardView2
            // 
            this.cardView2.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colCodcon1,
            this.colSaldoAnterior,
            this.colSaldoAplicaçãoFechamento,
            this.colSaldoPoupança13Fechamento,
            this.colSaldoPoupançaFRFechamento,
            this.colSaldoPoupançaObrasFechamento,
            this.colSaldoPoupançaSalãoFechamento,
            this.colSaldoAplicaçãoAutomáticaFechamento,
            this.colSaldoFechamento,
            this.colDataInicial,
            this.colDataFinal});
            this.cardView2.FocusedCardTopFieldIndex = 0;
            this.cardView2.GridControl = this.gridControl2;
            this.cardView2.MaximumCardColumns = 2;
            this.cardView2.MaximumCardRows = 1;
            this.cardView2.Name = "cardView2";
            this.cardView2.OptionsBehavior.AutoHorzWidth = true;
            this.cardView2.VertScrollVisibility = DevExpress.XtraGrid.Views.Base.ScrollVisibility.Auto;
            // 
            // colCodcon1
            // 
            this.colCodcon1.FieldName = "Codcon";
            this.colCodcon1.Name = "colCodcon1";
            this.colCodcon1.Visible = true;
            this.colCodcon1.VisibleIndex = 0;
            // 
            // colSaldoAnterior
            // 
            this.colSaldoAnterior.FieldName = "Saldo Anterior";
            this.colSaldoAnterior.Name = "colSaldoAnterior";
            this.colSaldoAnterior.Visible = true;
            this.colSaldoAnterior.VisibleIndex = 1;
            // 
            // colSaldoAplicaçãoFechamento
            // 
            this.colSaldoAplicaçãoFechamento.FieldName = "Saldo Aplicação Fechamento";
            this.colSaldoAplicaçãoFechamento.Name = "colSaldoAplicaçãoFechamento";
            this.colSaldoAplicaçãoFechamento.Visible = true;
            this.colSaldoAplicaçãoFechamento.VisibleIndex = 2;
            // 
            // colSaldoPoupança13Fechamento
            // 
            this.colSaldoPoupança13Fechamento.FieldName = "Saldo Poupança 13 Fechamento";
            this.colSaldoPoupança13Fechamento.Name = "colSaldoPoupança13Fechamento";
            this.colSaldoPoupança13Fechamento.Visible = true;
            this.colSaldoPoupança13Fechamento.VisibleIndex = 3;
            // 
            // colSaldoPoupançaFRFechamento
            // 
            this.colSaldoPoupançaFRFechamento.FieldName = "Saldo Poupança FR Fechamento";
            this.colSaldoPoupançaFRFechamento.Name = "colSaldoPoupançaFRFechamento";
            this.colSaldoPoupançaFRFechamento.Visible = true;
            this.colSaldoPoupançaFRFechamento.VisibleIndex = 4;
            // 
            // colSaldoPoupançaObrasFechamento
            // 
            this.colSaldoPoupançaObrasFechamento.FieldName = "Saldo Poupança Obras Fechamento";
            this.colSaldoPoupançaObrasFechamento.Name = "colSaldoPoupançaObrasFechamento";
            this.colSaldoPoupançaObrasFechamento.Visible = true;
            this.colSaldoPoupançaObrasFechamento.VisibleIndex = 5;
            // 
            // colSaldoPoupançaSalãoFechamento
            // 
            this.colSaldoPoupançaSalãoFechamento.FieldName = "Saldo Poupança Salão Fechamento";
            this.colSaldoPoupançaSalãoFechamento.Name = "colSaldoPoupançaSalãoFechamento";
            this.colSaldoPoupançaSalãoFechamento.Visible = true;
            this.colSaldoPoupançaSalãoFechamento.VisibleIndex = 6;
            // 
            // colSaldoAplicaçãoAutomáticaFechamento
            // 
            this.colSaldoAplicaçãoAutomáticaFechamento.FieldName = "Saldo Aplicação Automática Fechamento";
            this.colSaldoAplicaçãoAutomáticaFechamento.Name = "colSaldoAplicaçãoAutomáticaFechamento";
            this.colSaldoAplicaçãoAutomáticaFechamento.Visible = true;
            this.colSaldoAplicaçãoAutomáticaFechamento.VisibleIndex = 7;
            // 
            // colSaldoFechamento
            // 
            this.colSaldoFechamento.FieldName = "Saldo Fechamento";
            this.colSaldoFechamento.Name = "colSaldoFechamento";
            this.colSaldoFechamento.Visible = true;
            this.colSaldoFechamento.VisibleIndex = 8;
            // 
            // colDataInicial
            // 
            this.colDataInicial.FieldName = "Data Inicial";
            this.colDataInicial.Name = "colDataInicial";
            this.colDataInicial.Visible = true;
            this.colDataInicial.VisibleIndex = 9;
            // 
            // colDataFinal
            // 
            this.colDataFinal.FieldName = "Data Final";
            this.colDataFinal.Name = "colDataFinal";
            this.colDataFinal.Visible = true;
            this.colDataFinal.VisibleIndex = 10;
            // 
            // simpleButton1
            // 
            this.simpleButton1.Location = new System.Drawing.Point(260, 560);
            this.simpleButton1.Name = "simpleButton1";
            this.simpleButton1.Size = new System.Drawing.Size(394, 45);
            this.simpleButton1.TabIndex = 2;
            this.simpleButton1.Text = "Fechar - OK";
            this.simpleButton1.Click += new System.EventHandler(this.simpleButton1_Click);
            // 
            // spinEditmes
            // 
            this.spinEditmes.EditValue = new decimal(new int[] {
            3,
            0,
            0,
            0});
            this.spinEditmes.Location = new System.Drawing.Point(31, 570);
            this.spinEditmes.Name = "spinEditmes";
            this.spinEditmes.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.spinEditmes.Size = new System.Drawing.Size(100, 20);
            this.spinEditmes.TabIndex = 3;
            // 
            // spinEditano
            // 
            this.spinEditano.EditValue = new decimal(new int[] {
            2012,
            0,
            0,
            0});
            this.spinEditano.Location = new System.Drawing.Point(137, 570);
            this.spinEditano.Name = "spinEditano";
            this.spinEditano.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.spinEditano.Size = new System.Drawing.Size(100, 20);
            this.spinEditano.TabIndex = 4;
            // 
            // cFechamento
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.spinEditano);
            this.Controls.Add(this.spinEditmes);
            this.Controls.Add(this.simpleButton1);
            this.Controls.Add(this.gridControl2);
            this.Controls.Add(this.gridControl1);
            this.LookAndFeel.UseDefaultLookAndFeel = false;
            this.Name = "cFechamento";
            this.Size = new System.Drawing.Size(894, 617);
            ((System.ComponentModel.ISupportInitialize)(this.StyleController_F)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dFechamento1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cardView1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cardView2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.spinEditmes.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.spinEditano.Properties)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private dFechamento dFechamento1;
        private DevExpress.XtraGrid.GridControl gridControl1;
        private DevExpress.XtraGrid.GridControl gridControl2;
        private DevExpress.XtraGrid.Views.Card.CardView cardView1;
        private DevExpress.XtraGrid.Columns.GridColumn colCodcon;
        private DevExpress.XtraGrid.Columns.GridColumn colSaldoGeral;
        private DevExpress.XtraGrid.Columns.GridColumn colSaldoDisponível;
        private DevExpress.XtraGrid.Columns.GridColumn colSaldoPoupança;
        private DevExpress.XtraGrid.Columns.GridColumn colAplicações;
        private DevExpress.XtraGrid.Columns.GridColumn colResgates;
        private DevExpress.XtraGrid.Columns.GridColumn colRentabilidade;
        private DevExpress.XtraGrid.Views.Card.CardView cardView2;
        private DevExpress.XtraGrid.Columns.GridColumn colCodcon1;
        private DevExpress.XtraGrid.Columns.GridColumn colSaldoAnterior;
        private DevExpress.XtraGrid.Columns.GridColumn colSaldoAplicaçãoFechamento;
        private DevExpress.XtraGrid.Columns.GridColumn colSaldoPoupança13Fechamento;
        private DevExpress.XtraGrid.Columns.GridColumn colSaldoPoupançaFRFechamento;
        private DevExpress.XtraGrid.Columns.GridColumn colSaldoPoupançaObrasFechamento;
        private DevExpress.XtraGrid.Columns.GridColumn colSaldoPoupançaSalãoFechamento;
        private DevExpress.XtraGrid.Columns.GridColumn colSaldoAplicaçãoAutomáticaFechamento;
        private DevExpress.XtraGrid.Columns.GridColumn colSaldoFechamento;
        private DevExpress.XtraGrid.Columns.GridColumn colDataInicial;
        private DevExpress.XtraGrid.Columns.GridColumn colDataFinal;
        private DevExpress.XtraEditors.SimpleButton simpleButton1;
        private DevExpress.XtraEditors.SpinEdit spinEditmes;
        private DevExpress.XtraEditors.SpinEdit spinEditano;
    }
}
