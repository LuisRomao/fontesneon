﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Text;
using System.Windows.Forms;
using CompontesBasicos;

namespace Balancete.fechamento
{
    public partial class cFechamento : ComponenteBase
    {
        private dFechamento.Saldos_de_FechamentoRow S_anteriror;
        private dFechamento.Saldos_e_PoupançaRow P_anteriro;
        private dFechamento.Saldos_de_FechamentoRow S_nova;
        private dFechamento.Saldos_e_PoupançaRow P_Nova;
        private Balancete.GrBalancete.balancete balancete1;

        private int CON;
        private string CODCON;

        /// <summary>
        /// Construtor
        /// </summary>
        /// <param name="CODCON"></param>
        public cFechamento(string CODCON,int _CON,Balancete.GrBalancete.balancete _bal = null)
        {

            InitializeComponent();
            this.CODCON = CODCON;
            CON = _CON;
            if (_bal != null)
            {
                balancete1 = _bal;
                spinEditano.Value = balancete1.comp.Ano;
                spinEditmes.Value = balancete1.comp.Mes;
            }
            dFechamento1.Saldos_de_FechamentoTableAdapter.Fill(dFechamento1.Saldos_de_Fechamento, CODCON);
            dFechamento1.Saldos_e_PoupançaTableAdapter.Fill(dFechamento1.Saldos_e_Poupança, CODCON);
            S_anteriror = dFechamento1.Saldos_de_Fechamento[0];
            S_nova = dFechamento1.Saldos_de_Fechamento.NewSaldos_de_FechamentoRow();
            S_nova.Aplicações = S_anteriror.Aplicações;
            S_nova.Codcon = "NOVA";
            S_nova.Rentabilidade = S_anteriror.Rentabilidade;
            S_nova.Resgates = S_anteriror.Resgates;
            S_nova.Saldo_Disponível = S_anteriror.Saldo_Disponível;
            S_nova.Saldo_Geral = S_anteriror.Saldo_Geral;
            S_nova.Saldo_Poupança = S_anteriror.Saldo_Poupança;
            dFechamento1.Saldos_de_Fechamento.AddSaldos_de_FechamentoRow(S_nova);
            P_anteriro = dFechamento1.Saldos_e_Poupança[0];
            P_Nova = dFechamento1.Saldos_e_Poupança.NewSaldos_e_PoupançaRow();
            P_Nova.Codcon = "NOVA";
            if (balancete1 != null)
            {
                P_Nova.Data_Final = balancete1.DataF;
                P_Nova.Data_Inicial = balancete1.DataI;
            }
            else
            {
                P_Nova.Data_Final = P_anteriro.Data_Final.AddMonths(1);
                P_Nova.Data_Inicial = P_anteriro.Data_Inicial.AddMonths(1);
            }
            
            P_Nova.Saldo_Anterior = P_anteriro.Saldo_Anterior;
            P_Nova.Saldo_Aplicação_Automática_Fechamento = P_anteriro.Saldo_Aplicação_Automática_Fechamento;
            P_Nova.Saldo_Aplicação_Fechamento = P_anteriro.Saldo_Aplicação_Fechamento;
            P_Nova.Saldo_Fechamento = P_anteriro.Saldo_Fechamento;
            P_Nova.Saldo_Poupança_13_Fechamento = P_anteriro.Saldo_Poupança_13_Fechamento;
            P_Nova.Saldo_Poupança_FR_Fechamento = P_anteriro.Saldo_Poupança_FR_Fechamento;
            P_Nova.Saldo_Poupança_Obras_Fechamento = P_anteriro.Saldo_Poupança_Obras_Fechamento;
            P_Nova.Saldo_Poupança_Salão_Fechamento = P_anteriro.Saldo_Poupança_Salão_Fechamento;
            dFechamento1.Saldos_e_Poupança.AddSaldos_e_PoupançaRow(P_Nova);
        }

        private void simpleButton1_Click(object sender, EventArgs e)
        {
            
            if (balancete1 == null)
            {
                balancete1 = new GrBalancete.balancete(CON, new Framework.objetosNeon.Competencia((int)spinEditmes.Value, (int)spinEditano.Value, CON));
                balancete1.CarregaExtratos(true);
                balancete1.CarregarDadosBalancete(GrBalancete.balancete.TipoClassificacaoDesp.Grupos, true);
            }
            if (balancete1 != null)
            {
                VirOleDb.TableAdapter TA = new VirOleDb.TableAdapter(VirDB.Bancovirtual.TiposDeBanco.AccessH);
                TA.ExecutarSQLNonQuery("DELETE FROM [Histórico Lançamentos] WHERE ([Histórico Lançamentos].[Data de Lançamento] BETWEEN @P1 AND @P2 and [CODCON] = @P3)", balancete1.DataI, balancete1.DataF,balancete1.rowCON.CONCodigo);
            }
            dFechamento1.Saldos_de_FechamentoTableAdapter.AbreTrasacaoLocal(dFechamento1.Saldos_de_FechamentoTableAdapter,
                                                                            dFechamento1.Saldos_e_PoupançaTableAdapter,
                                                                            dFechamento1.Histórico_LançamentosTableAdapter);
            try
            {                
                S_anteriror.Aplicações = S_nova.Aplicações;
                S_anteriror.Rentabilidade = S_nova.Rentabilidade;
                S_anteriror.Resgates = S_nova.Resgates;
                S_anteriror.Saldo_Disponível = S_nova.Saldo_Disponível;
                S_anteriror.Saldo_Geral = S_nova.Saldo_Geral;
                S_anteriror.Saldo_Poupança = S_nova.Saldo_Poupança;
                dFechamento1.Saldos_de_FechamentoTableAdapter.Update(S_anteriror);

                P_anteriro.Data_Final = P_Nova.Data_Final;
                P_anteriro.Data_Inicial = P_Nova.Data_Inicial;
                P_anteriro.Saldo_Anterior = P_Nova.Saldo_Anterior;
                P_anteriro.Saldo_Aplicação_Automática_Fechamento = P_Nova.Saldo_Aplicação_Automática_Fechamento;
                P_anteriro.Saldo_Aplicação_Fechamento = P_Nova.Saldo_Aplicação_Fechamento;
                P_anteriro.Saldo_Fechamento = P_Nova.Saldo_Fechamento;
                P_anteriro.Saldo_Poupança_13_Fechamento = P_Nova.Saldo_Poupança_13_Fechamento;
                P_anteriro.Saldo_Poupança_FR_Fechamento = P_Nova.Saldo_Poupança_FR_Fechamento;
                P_anteriro.Saldo_Poupança_Obras_Fechamento = P_Nova.Saldo_Poupança_Obras_Fechamento;
                P_anteriro.Saldo_Poupança_Salão_Fechamento = P_Nova.Saldo_Poupança_Salão_Fechamento;
                dFechamento1.Saldos_e_PoupançaTableAdapter.Update(P_anteriro);

                //dFechamento1.Lancamentos_BradescoTableAdapter.Fill(dFechamento1.Lancamentos_Bradesco, P_anteriro.Codcon, P_anteriro.Data_Inicial, P_anteriro.Data_Final);

                

                string compet = string.Format("{0:00}{1:0000}", spinEditmes.Value, spinEditano.Value);

                
                foreach (Balancete.Relatorios.Credito.dCreditos.DebitosRow rowDeb in balancete1.DCreditos1.Debitos)
                {
                    if (rowDeb.Valor == 0)
                        continue;
                    dFechamento.Histórico_LançamentosRow rownovaH = dFechamento1.Histórico_Lançamentos.NewHistórico_LançamentosRow();                    
                    rownovaH.Codcon = CODCON;                    
                    rownovaH.Crédito = false;
                    rownovaH.Data_de_Lançamento = balancete1.DataI;
                    if (rowDeb.Descricao == "")
                        rownovaH.Descrição = " ";
                    else if (rowDeb.Descricao.Length > 60)
                        rownovaH.Descrição = rowDeb.Descricao.Substring(0, 60);
                    else
                        rownovaH.Descrição = rowDeb.Descricao;
                    rownovaH.Mês_Competência = compet;
                    switch (rowDeb.Grupo)
                    {
                        case "Advocatíceas":
                            rownovaH.Tipopag = "230000";
                            break;
                        case "Bancárias":
                            rownovaH.Tipopag = "240005";
                            break;
                        case "Despesas Gerais":
                            rownovaH.Tipopag = "230009";
                            break;
                        case "Mão de obra":
                            rownovaH.Tipopag = "220000";
                            break;
                        case "Administradora":
                            rownovaH.Tipopag = "230001";
                            break;
                        default:
                            rownovaH.Tipopag = "230000";
                            break;
                    }                                                                                               
                    
                    rownovaH.Valor_Pago = (float)rowDeb.Valor;                    
                    dFechamento1.Histórico_Lançamentos.AddHistórico_LançamentosRow(rownovaH);
 
                }

                foreach (Balancete.Relatorios.Credito.dCreditos.CreditosRow rowC in balancete1.DCreditos1.Creditos)
                {
                    if (rowC.Antecipado != 0)
                    {
                        dFechamento.Histórico_LançamentosRow rownovaHC = dFechamento1.Histórico_Lançamentos.NewHistórico_LançamentosRow();
                        rownovaHC.Codcon = CODCON;
                        rownovaHC.Crédito = true;
                        rownovaHC.Data_de_Lançamento = balancete1.DataI;
                        rownovaHC.Descrição = rowC.Descricao;
                        rownovaHC.Mês_Competência = compet;
                        rownovaHC.Tipopag = "101000";
                        rownovaHC.Valor_Pago = (float)rowC.Antecipado;
                        rownovaHC.Versão = 2;
                        dFechamento1.Histórico_Lançamentos.AddHistórico_LançamentosRow(rownovaHC);
                    }
                    if (rowC.Periodo != 0)
                    {
                        dFechamento.Histórico_LançamentosRow rownovaHC = dFechamento1.Histórico_Lançamentos.NewHistórico_LançamentosRow();
                        rownovaHC.Codcon = CODCON;
                        rownovaHC.Crédito = true;
                        rownovaHC.Data_de_Lançamento = balancete1.DataI;
                        rownovaHC.Descrição = rowC.Descricao;
                        rownovaHC.Mês_Competência = compet;
                        rownovaHC.Tipopag = "101000";
                        rownovaHC.Valor_Pago = (float)rowC.Periodo;
                        rownovaHC.Versão = 1;
                        dFechamento1.Histórico_Lançamentos.AddHistórico_LançamentosRow(rownovaHC);
                    }
                    if (rowC.Atrasado != 0)
                    {
                        dFechamento.Histórico_LançamentosRow rownovaHC = dFechamento1.Histórico_Lançamentos.NewHistórico_LançamentosRow();
                        rownovaHC.Codcon = CODCON;
                        rownovaHC.Crédito = true;
                        rownovaHC.Data_de_Lançamento = balancete1.DataI;
                        rownovaHC.Descrição = rowC.Descricao;
                        rownovaHC.Mês_Competência = compet;
                        rownovaHC.Tipopag = "101000";
                        rownovaHC.Valor_Pago = (float)rowC.Atrasado;
                        rownovaHC.Versão = 0;
                        dFechamento1.Histórico_Lançamentos.AddHistórico_LançamentosRow(rownovaHC);
                    }
                }


                /*
                foreach (dFechamento.Lancamentos_BradescoRow rowAtiva in dFechamento1.Lancamentos_Bradesco)
                {
                    dFechamento.Histórico_LançamentosRow rownovaH = dFechamento1.Histórico_Lançamentos.NewHistórico_LançamentosRow();
                    if (!rowAtiva.IsApartamentoNull())
                        rownovaH.Apartamento = rowAtiva.Apartamento;
                    if (!rowAtiva.IsBlocoNull())
                        rownovaH.Bloco = rowAtiva.Bloco;
                    //rownovaH.Boleto_Gerado = rowAtiva.bole
                    rownovaH.Codcon = rowAtiva.Codcon;
                    if (!rowAtiva.IsCódigo_CategoriaNull())
                        rownovaH.Código_Categoria = rowAtiva.Código_Categoria;
                    if (!rowAtiva.IsCréditoNull())
                    rownovaH.Crédito = rowAtiva.Crédito;
                    rownovaH.Data_de_Lançamento = rowAtiva.Data_de_Lançamento;
                    if (!rowAtiva.IsData_de_LeituraNull())
                    rownovaH.Data_de_Leitura = rowAtiva.Data_de_Leitura;
                    if (!rowAtiva.IsDescriçãoNull())
                    rownovaH.Descrição = rowAtiva.Descrição;
                    rownovaH.Mês_Competência = compet;
                    if (!rowAtiva.IsNúmero_DocumentoNull())
                        rownovaH.Número_Documento = rowAtiva.Número_Documento;
                    rownovaH.Tipopag = rowAtiva.Tipopag;
                    if (!rowAtiva.IsValor_PagoNull())
                        rownovaH.Valor_Pago = rowAtiva.Valor_Pago;
                    //rownovaH.Versão = rowAtiva.Versão;                    
                    dFechamento1.Histórico_Lançamentos.AddHistórico_LançamentosRow(rownovaH);
                }*/
                dFechamento1.Histórico_LançamentosTableAdapter.Update(dFechamento1.Histórico_Lançamentos);
                dFechamento1.Saldos_de_FechamentoTableAdapter.Commit();
                dFechamento1.Lancamentos_BradescoTableAdapter.DeleteQuery(P_anteriro.Codcon, P_anteriro.Data_Inicial, P_anteriro.Data_Final);
                MessageBox.Show("OK");
                FechaTela(DialogResult.OK);
            }
            catch (Exception ex)
            {
                dFechamento1.Saldos_de_FechamentoTableAdapter.Vircatch(ex);
                MessageBox.Show(string.Format("Falha: {0}", ex.Message));
                FechaTela(DialogResult.Cancel);
            }
        }
    }
}
