﻿namespace Balancete.fechamento {
    
    
    public partial class dFechamento 
    {
        private dFechamentoTableAdapters.Lancamentos_BradescoTableAdapter lancamentos_BradescoTableAdapter;
        /// <summary>
        /// TableAdapter padrão para tabela: Lancamentos_Bradesco
        /// </summary>
        public dFechamentoTableAdapters.Lancamentos_BradescoTableAdapter Lancamentos_BradescoTableAdapter
        {
            get
            {
                if (lancamentos_BradescoTableAdapter == null)
                {
                    lancamentos_BradescoTableAdapter = new dFechamentoTableAdapters.Lancamentos_BradescoTableAdapter();
                    lancamentos_BradescoTableAdapter.TrocarStringDeConexao();
                };
                return lancamentos_BradescoTableAdapter;
            }
        }

        private dFechamentoTableAdapters.Histórico_LançamentosTableAdapter histórico_LançamentosTableAdapter;
        /// <summary>
        /// TableAdapter padrão para tabela: Histórico_Lançamentos
        /// </summary>
        public dFechamentoTableAdapters.Histórico_LançamentosTableAdapter Histórico_LançamentosTableAdapter
        {
            get
            {
                if (histórico_LançamentosTableAdapter == null)
                {
                    histórico_LançamentosTableAdapter = new dFechamentoTableAdapters.Histórico_LançamentosTableAdapter();
                    histórico_LançamentosTableAdapter.TrocarStringDeConexao(VirDB.Bancovirtual.TiposDeBanco.AccessH);
                };
                return histórico_LançamentosTableAdapter;
            }
        }

        private dFechamentoTableAdapters.Saldos_de_FechamentoTableAdapter saldos_de_FechamentoTableAdapter;
        /// <summary>
        /// TableAdapter padrão para tabela: Saldos_de_Fechamento
        /// </summary>
        public dFechamentoTableAdapters.Saldos_de_FechamentoTableAdapter Saldos_de_FechamentoTableAdapter
        {
            get
            {
                if (saldos_de_FechamentoTableAdapter == null)
                {
                    saldos_de_FechamentoTableAdapter = new dFechamentoTableAdapters.Saldos_de_FechamentoTableAdapter();
                    saldos_de_FechamentoTableAdapter.TrocarStringDeConexao(VirDB.Bancovirtual.TiposDeBanco.AccessH);
                };
                return saldos_de_FechamentoTableAdapter;
            }
        }

        private dFechamentoTableAdapters.Saldos_e_PoupançaTableAdapter saldos_e_PoupançaTableAdapter;
        /// <summary>
        /// TableAdapter padrão para tabela: Saldos_e_Poupança
        /// </summary>
        public dFechamentoTableAdapters.Saldos_e_PoupançaTableAdapter Saldos_e_PoupançaTableAdapter
        {
            get
            {
                if (saldos_e_PoupançaTableAdapter == null)
                {
                    saldos_e_PoupançaTableAdapter = new dFechamentoTableAdapters.Saldos_e_PoupançaTableAdapter();
                    saldos_e_PoupançaTableAdapter.TrocarStringDeConexao(VirDB.Bancovirtual.TiposDeBanco.AccessH);
                };
                return saldos_e_PoupançaTableAdapter;
            }
        }
    }
}
