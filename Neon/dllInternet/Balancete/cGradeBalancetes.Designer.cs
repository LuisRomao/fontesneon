namespace Balancete
{
    partial class cGradeBalancetes
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            DevExpress.XtraEditors.Controls.EditorButtonImageOptions editorButtonImageOptions1 = new DevExpress.XtraEditors.Controls.EditorButtonImageOptions();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject1 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject2 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject3 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject4 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.XtraEditors.Controls.EditorButtonImageOptions editorButtonImageOptions2 = new DevExpress.XtraEditors.Controls.EditorButtonImageOptions();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject5 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject6 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject7 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject8 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.XtraEditors.Controls.EditorButtonImageOptions editorButtonImageOptions3 = new DevExpress.XtraEditors.Controls.EditorButtonImageOptions();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject9 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject10 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject11 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject12 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.XtraEditors.Controls.EditorButtonImageOptions editorButtonImageOptions4 = new DevExpress.XtraEditors.Controls.EditorButtonImageOptions();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject13 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject14 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject15 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject16 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.XtraEditors.Controls.EditorButtonImageOptions editorButtonImageOptions5 = new DevExpress.XtraEditors.Controls.EditorButtonImageOptions();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject17 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject18 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject19 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject20 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.XtraEditors.Controls.EditorButtonImageOptions editorButtonImageOptions6 = new DevExpress.XtraEditors.Controls.EditorButtonImageOptions();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject21 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject22 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject23 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject24 = new DevExpress.Utils.SerializableAppearanceObject();
            this.panelControl1 = new DevExpress.XtraEditors.PanelControl();
            this.checkE7 = new DevExpress.XtraEditors.CheckEdit();
            this.spinEdit1 = new DevExpress.XtraEditors.SpinEdit();
            this.lookupCondominio_F1 = new Framework.Lookup.LookupCondominio_F();
            this.simpleButton2 = new DevExpress.XtraEditors.SimpleButton();
            this.simpleButton1 = new DevExpress.XtraEditors.SimpleButton();
            this.BExp = new DevExpress.XtraEditors.SimpleButton();
            this.lookupGerente1 = new Framework.Lookup.LookupGerente();
            this.checkE6 = new DevExpress.XtraEditors.CheckEdit();
            this.checkE5 = new DevExpress.XtraEditors.CheckEdit();
            this.checkE4 = new DevExpress.XtraEditors.CheckEdit();
            this.checkE3 = new DevExpress.XtraEditors.CheckEdit();
            this.checkE2 = new DevExpress.XtraEditors.CheckEdit();
            this.checkE1 = new DevExpress.XtraEditors.CheckEdit();
            this.gridControl1 = new DevExpress.XtraGrid.GridControl();
            this.dBalancetesBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.dBalancetes = new Balancete.dBalancetes();
            this.gridView1 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colBALCompet = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colBALDataInicio = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colBALdataFim = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colBALSTATUS = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemImageComboBox2 = new DevExpress.XtraEditors.Repository.RepositoryItemImageComboBox();
            this.colBALObs = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemMemoEdit1 = new DevExpress.XtraEditors.Repository.RepositoryItemMemoEdit();
            this.colBALInicioProducao = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colBALFimProducao = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colCONCodigo = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colCONNome = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colUSUNome = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemButtonEdit1 = new DevExpress.XtraEditors.Repository.RepositoryItemButtonEdit();
            this.colCONObsBal = new DevExpress.XtraGrid.Columns.GridColumn();
            this.coldataLimite = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colPESNome = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colPESFone1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colFolga = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colCONAuditor1_USU = new DevExpress.XtraGrid.Columns.GridColumn();
            this.LookUpUSU = new DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit();
            this.uSUariosBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.colBALN = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colBAL = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colBALred = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colCONLimiteBal = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colBAL1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemButtonBalN = new DevExpress.XtraEditors.Repository.RepositoryItemButtonEdit();
            this.repositoryItemButtonBal = new DevExpress.XtraEditors.Repository.RepositoryItemButtonEdit();
            this.repositoryItemButtonBalRed = new DevExpress.XtraEditors.Repository.RepositoryItemButtonEdit();
            this.memoEdit1 = new DevExpress.XtraEditors.MemoEdit();
            ((System.ComponentModel.ISupportInitialize)(this.StyleController_F)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl1)).BeginInit();
            this.panelControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.checkE7.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.spinEdit1.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkE6.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkE5.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkE4.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkE3.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkE2.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkE1.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dBalancetesBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dBalancetes)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemImageComboBox2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoEdit1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemButtonEdit1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LookUpUSU)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uSUariosBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemButtonBalN)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemButtonBal)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemButtonBalRed)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.memoEdit1.Properties)).BeginInit();
            this.SuspendLayout();
            // 
            // ToolTipController_F
            // 
            this.ToolTipController_F.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.ToolTipController_F.Appearance.ForeColor = System.Drawing.Color.Black;
            this.ToolTipController_F.Appearance.Options.UseBackColor = true;
            this.ToolTipController_F.Appearance.Options.UseFont = true;
            this.ToolTipController_F.Appearance.Options.UseForeColor = true;
            this.ToolTipController_F.Appearance.Options.UseTextOptions = true;
            this.ToolTipController_F.Appearance.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            // 
            // StyleController_F
            // 
            this.StyleController_F.AppearanceDisabled.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this.StyleController_F.AppearanceDisabled.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this.StyleController_F.AppearanceDisabled.Options.UseBackColor = true;
            this.StyleController_F.AppearanceDisabled.Options.UseForeColor = true;
            this.StyleController_F.AppearanceFocused.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.StyleController_F.AppearanceFocused.Options.UseBackColor = true;
            // 
            // panelControl1
            // 
            this.panelControl1.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(192)))), ((int)(((byte)(128)))));
            this.panelControl1.Appearance.BackColor2 = System.Drawing.Color.White;
            this.panelControl1.Appearance.Options.UseBackColor = true;
            this.panelControl1.Controls.Add(this.checkE7);
            this.panelControl1.Controls.Add(this.spinEdit1);
            this.panelControl1.Controls.Add(this.lookupCondominio_F1);
            this.panelControl1.Controls.Add(this.simpleButton2);
            this.panelControl1.Controls.Add(this.simpleButton1);
            this.panelControl1.Controls.Add(this.BExp);
            this.panelControl1.Controls.Add(this.lookupGerente1);
            this.panelControl1.Controls.Add(this.checkE6);
            this.panelControl1.Controls.Add(this.checkE5);
            this.panelControl1.Controls.Add(this.checkE4);
            this.panelControl1.Controls.Add(this.checkE3);
            this.panelControl1.Controls.Add(this.checkE2);
            this.panelControl1.Controls.Add(this.checkE1);
            this.panelControl1.Dock = System.Windows.Forms.DockStyle.Top;
            this.panelControl1.Location = new System.Drawing.Point(0, 0);
            this.panelControl1.LookAndFeel.Style = DevExpress.LookAndFeel.LookAndFeelStyle.Flat;
            this.panelControl1.LookAndFeel.UseDefaultLookAndFeel = false;
            this.panelControl1.Name = "panelControl1";
            this.panelControl1.Size = new System.Drawing.Size(1477, 89);
            this.panelControl1.TabIndex = 0;
            // 
            // checkE7
            // 
            this.checkE7.Location = new System.Drawing.Point(827, 5);
            this.checkE7.Name = "checkE7";
            this.checkE7.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.checkE7.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.checkE7.Properties.Appearance.Options.UseBackColor = true;
            this.checkE7.Properties.Appearance.Options.UseFont = true;
            this.checkE7.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.Style3D;
            this.checkE7.Properties.Caption = "XXX";
            this.checkE7.Size = new System.Drawing.Size(131, 22);
            this.checkE7.TabIndex = 17;
            // 
            // spinEdit1
            // 
            this.spinEdit1.EditValue = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.spinEdit1.Location = new System.Drawing.Point(842, 33);
            this.spinEdit1.Name = "spinEdit1";
            this.spinEdit1.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.spinEdit1.Size = new System.Drawing.Size(100, 22);
            this.spinEdit1.TabIndex = 16;
            this.spinEdit1.Visible = false;
            // 
            // lookupCondominio_F1
            // 
            this.lookupCondominio_F1.Autofill = true;
            this.lookupCondominio_F1.CaixaAltaGeral = true;
            this.lookupCondominio_F1.CON_sel = -1;
            this.lookupCondominio_F1.CON_selrow = null;
            this.lookupCondominio_F1.Cursor = System.Windows.Forms.Cursors.Default;
            this.lookupCondominio_F1.Doc = System.Windows.Forms.DockStyle.None;
            this.lookupCondominio_F1.Estado = CompontesBasicos.EstadosDosComponentes.Registrado;
            this.lookupCondominio_F1.LinhaMae_F = null;
            this.lookupCondominio_F1.Location = new System.Drawing.Point(301, 33);
            this.lookupCondominio_F1.LookAndFeel.UseDefaultLookAndFeel = false;
            this.lookupCondominio_F1.Name = "lookupCondominio_F1";
            this.lookupCondominio_F1.NivelCONOculto = 2;
            this.lookupCondominio_F1.Size = new System.Drawing.Size(308, 20);
            this.lookupCondominio_F1.somenteleitura = false;
            this.lookupCondominio_F1.TabIndex = 15;
            this.lookupCondominio_F1.TableAdapterPrincipal = null;
            this.lookupCondominio_F1.Titulo = null;
            // 
            // simpleButton2
            // 
            this.simpleButton2.Location = new System.Drawing.Point(692, 37);
            this.simpleButton2.Name = "simpleButton2";
            this.simpleButton2.Size = new System.Drawing.Size(110, 19);
            this.simpleButton2.TabIndex = 14;
            this.simpleButton2.Text = "Hist�rico";
            this.simpleButton2.Visible = false;
            this.simpleButton2.Click += new System.EventHandler(this.simpleButton2_Click);
            // 
            // simpleButton1
            // 
            this.simpleButton1.Location = new System.Drawing.Point(5, 64);
            this.simpleButton1.Name = "simpleButton1";
            this.simpleButton1.Size = new System.Drawing.Size(110, 19);
            this.simpleButton1.TabIndex = 13;
            this.simpleButton1.Text = "Calcular";
            this.simpleButton1.Click += new System.EventHandler(this.simpleButton1_Click);
            // 
            // BExp
            // 
            this.BExp.Location = new System.Drawing.Point(121, 64);
            this.BExp.Name = "BExp";
            this.BExp.Size = new System.Drawing.Size(110, 19);
            this.BExp.TabIndex = 12;
            this.BExp.Text = "Exportar";
            this.BExp.Visible = false;
            this.BExp.Click += new System.EventHandler(this.simpleButton4_Click);
            // 
            // lookupGerente1
            // 
            this.lookupGerente1.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.lookupGerente1.Appearance.Options.UseBackColor = true;
            this.lookupGerente1.CaixaAltaGeral = true;
            this.lookupGerente1.Doc = System.Windows.Forms.DockStyle.None;
            this.lookupGerente1.Estado = CompontesBasicos.EstadosDosComponentes.Registrado;
            this.lookupGerente1.Location = new System.Drawing.Point(5, 33);
            this.lookupGerente1.LookAndFeel.UseDefaultLookAndFeel = false;
            this.lookupGerente1.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.lookupGerente1.Name = "lookupGerente1";
            this.lookupGerente1.Size = new System.Drawing.Size(290, 20);
            this.lookupGerente1.somenteleitura = false;
            this.lookupGerente1.TabIndex = 9;
            this.lookupGerente1.Tipo = Framework.Lookup.LookupGerente.TipoUsuario.gerente;
            this.lookupGerente1.Titulo = null;
            this.lookupGerente1.USUSel = -1;
            this.lookupGerente1.alterado += new System.EventHandler(this.lookupGerente1_alterado);
            // 
            // checkE6
            // 
            this.checkE6.EditValue = true;
            this.checkE6.Location = new System.Drawing.Point(690, 5);
            this.checkE6.Name = "checkE6";
            this.checkE6.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.checkE6.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.checkE6.Properties.Appearance.Options.UseBackColor = true;
            this.checkE6.Properties.Appearance.Options.UseFont = true;
            this.checkE6.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.Style3D;
            this.checkE6.Properties.Caption = "XXX";
            this.checkE6.Size = new System.Drawing.Size(131, 22);
            this.checkE6.TabIndex = 8;
            this.checkE6.CheckedChanged += new System.EventHandler(this.checkE1_CheckedChanged);
            // 
            // checkE5
            // 
            this.checkE5.EditValue = true;
            this.checkE5.Location = new System.Drawing.Point(553, 5);
            this.checkE5.Name = "checkE5";
            this.checkE5.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.checkE5.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.checkE5.Properties.Appearance.Options.UseBackColor = true;
            this.checkE5.Properties.Appearance.Options.UseFont = true;
            this.checkE5.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.Style3D;
            this.checkE5.Properties.Caption = "XXX";
            this.checkE5.Size = new System.Drawing.Size(131, 22);
            this.checkE5.TabIndex = 7;
            this.checkE5.CheckedChanged += new System.EventHandler(this.checkE1_CheckedChanged);
            // 
            // checkE4
            // 
            this.checkE4.EditValue = true;
            this.checkE4.Location = new System.Drawing.Point(416, 5);
            this.checkE4.Name = "checkE4";
            this.checkE4.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.checkE4.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.checkE4.Properties.Appearance.Options.UseBackColor = true;
            this.checkE4.Properties.Appearance.Options.UseFont = true;
            this.checkE4.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.Style3D;
            this.checkE4.Properties.Caption = "XXX";
            this.checkE4.Size = new System.Drawing.Size(131, 22);
            this.checkE4.TabIndex = 6;
            this.checkE4.CheckedChanged += new System.EventHandler(this.checkE1_CheckedChanged);
            // 
            // checkE3
            // 
            this.checkE3.EditValue = true;
            this.checkE3.Location = new System.Drawing.Point(279, 5);
            this.checkE3.Name = "checkE3";
            this.checkE3.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.checkE3.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.checkE3.Properties.Appearance.Options.UseBackColor = true;
            this.checkE3.Properties.Appearance.Options.UseFont = true;
            this.checkE3.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.Style3D;
            this.checkE3.Properties.Caption = "XXX";
            this.checkE3.Size = new System.Drawing.Size(131, 22);
            this.checkE3.TabIndex = 5;
            this.checkE3.CheckedChanged += new System.EventHandler(this.checkE1_CheckedChanged);
            // 
            // checkE2
            // 
            this.checkE2.EditValue = true;
            this.checkE2.Location = new System.Drawing.Point(142, 5);
            this.checkE2.Name = "checkE2";
            this.checkE2.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.checkE2.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.checkE2.Properties.Appearance.Options.UseBackColor = true;
            this.checkE2.Properties.Appearance.Options.UseFont = true;
            this.checkE2.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.Style3D;
            this.checkE2.Properties.Caption = "XXX";
            this.checkE2.Size = new System.Drawing.Size(131, 22);
            this.checkE2.TabIndex = 4;
            this.checkE2.CheckedChanged += new System.EventHandler(this.checkE1_CheckedChanged);
            // 
            // checkE1
            // 
            this.checkE1.EditValue = true;
            this.checkE1.Location = new System.Drawing.Point(5, 5);
            this.checkE1.Name = "checkE1";
            this.checkE1.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.checkE1.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.checkE1.Properties.Appearance.Options.UseBackColor = true;
            this.checkE1.Properties.Appearance.Options.UseFont = true;
            this.checkE1.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.Style3D;
            this.checkE1.Properties.Caption = "XXX";
            this.checkE1.Size = new System.Drawing.Size(131, 22);
            this.checkE1.TabIndex = 3;
            this.checkE1.CheckedChanged += new System.EventHandler(this.checkE1_CheckedChanged);
            // 
            // gridControl1
            // 
            this.gridControl1.DataMember = "GradeBalancetes";
            this.gridControl1.DataSource = this.dBalancetesBindingSource;
            this.gridControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridControl1.Location = new System.Drawing.Point(0, 89);
            this.gridControl1.MainView = this.gridView1;
            this.gridControl1.Name = "gridControl1";
            this.gridControl1.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemMemoEdit1,
            this.repositoryItemImageComboBox2,
            this.repositoryItemButtonEdit1,
            this.LookUpUSU,
            this.repositoryItemButtonBalN,
            this.repositoryItemButtonBal,
            this.repositoryItemButtonBalRed});
            this.gridControl1.ShowOnlyPredefinedDetails = true;
            this.gridControl1.Size = new System.Drawing.Size(1477, 718);
            this.gridControl1.TabIndex = 1;
            this.gridControl1.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridView1});
            // 
            // dBalancetesBindingSource
            // 
            this.dBalancetesBindingSource.DataSource = this.dBalancetes;
            this.dBalancetesBindingSource.Position = 0;
            // 
            // dBalancetes
            // 
            this.dBalancetes.DataSetName = "dBalancetes";
            this.dBalancetes.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // gridView1
            // 
            this.gridView1.Appearance.Empty.BackColor = System.Drawing.Color.Wheat;
            this.gridView1.Appearance.Empty.BackColor2 = System.Drawing.Color.White;
            this.gridView1.Appearance.Empty.GradientMode = System.Drawing.Drawing2D.LinearGradientMode.ForwardDiagonal;
            this.gridView1.Appearance.Empty.Options.UseBackColor = true;
            this.gridView1.Appearance.Row.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(192)))), ((int)(((byte)(128)))));
            this.gridView1.Appearance.Row.BackColor2 = System.Drawing.Color.White;
            this.gridView1.Appearance.Row.GradientMode = System.Drawing.Drawing2D.LinearGradientMode.ForwardDiagonal;
            this.gridView1.Appearance.Row.Options.UseBackColor = true;
            this.gridView1.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colBALCompet,
            this.colBALDataInicio,
            this.colBALdataFim,
            this.colBALSTATUS,
            this.colBALObs,
            this.colBALInicioProducao,
            this.colBALFimProducao,
            this.colCONCodigo,
            this.colCONNome,
            this.colUSUNome,
            this.gridColumn1,
            this.colCONObsBal,
            this.coldataLimite,
            this.colPESNome,
            this.colPESFone1,
            this.colFolga,
            this.colCONAuditor1_USU,
            this.colBALN,
            this.colBAL,
            this.colBALred,
            this.colCONLimiteBal,
            this.colBAL1});
            this.gridView1.GridControl = this.gridControl1;
            this.gridView1.GroupSummary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridGroupSummaryItem(DevExpress.Data.SummaryItemType.Count, "CONCodigo", this.colCONCodigo, "{0}")});
            this.gridView1.Name = "gridView1";
            this.gridView1.OptionsView.ColumnAutoWidth = false;
            this.gridView1.OptionsView.GroupFooterShowMode = DevExpress.XtraGrid.Views.Grid.GroupFooterShowMode.VisibleAlways;
            this.gridView1.OptionsView.ShowAutoFilterRow = true;
            this.gridView1.OptionsView.ShowFooter = true;
            this.gridView1.OptionsView.ShowGroupPanel = false;
            this.gridView1.SortInfo.AddRange(new DevExpress.XtraGrid.Columns.GridColumnSortInfo[] {
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colBALCompet, DevExpress.Data.ColumnSortOrder.Descending)});
            this.gridView1.RowCellStyle += new DevExpress.XtraGrid.Views.Grid.RowCellStyleEventHandler(this.gridView1_RowCellStyle);
            this.gridView1.RowStyle += new DevExpress.XtraGrid.Views.Grid.RowStyleEventHandler(this.gridView1_RowStyle);
            this.gridView1.CustomRowCellEdit += new DevExpress.XtraGrid.Views.Grid.CustomRowCellEditEventHandler(this.gridView1_CustomRowCellEdit);
            this.gridView1.CalcRowHeight += new DevExpress.XtraGrid.Views.Grid.RowHeightEventHandler(this.gridView1_CalcRowHeight);
            this.gridView1.CustomColumnDisplayText += new DevExpress.XtraGrid.Views.Base.CustomColumnDisplayTextEventHandler(this.gridView1_CustomColumnDisplayText);
            // 
            // colBALCompet
            // 
            this.colBALCompet.Caption = "Competencia";
            this.colBALCompet.FieldName = "BALCompet";
            this.colBALCompet.FilterMode = DevExpress.XtraGrid.ColumnFilterMode.DisplayText;
            this.colBALCompet.Name = "colBALCompet";
            this.colBALCompet.OptionsColumn.FixedWidth = true;
            this.colBALCompet.OptionsColumn.ReadOnly = true;
            this.colBALCompet.Visible = true;
            this.colBALCompet.VisibleIndex = 0;
            this.colBALCompet.Width = 60;
            // 
            // colBALDataInicio
            // 
            this.colBALDataInicio.Caption = "De";
            this.colBALDataInicio.FieldName = "BALDataInicio";
            this.colBALDataInicio.Name = "colBALDataInicio";
            this.colBALDataInicio.OptionsColumn.FixedWidth = true;
            this.colBALDataInicio.OptionsColumn.ReadOnly = true;
            this.colBALDataInicio.Visible = true;
            this.colBALDataInicio.VisibleIndex = 5;
            this.colBALDataInicio.Width = 68;
            // 
            // colBALdataFim
            // 
            this.colBALdataFim.Caption = "At�";
            this.colBALdataFim.FieldName = "BALdataFim";
            this.colBALdataFim.Name = "colBALdataFim";
            this.colBALdataFim.OptionsColumn.FixedWidth = true;
            this.colBALdataFim.OptionsColumn.ReadOnly = true;
            this.colBALdataFim.Visible = true;
            this.colBALdataFim.VisibleIndex = 6;
            // 
            // colBALSTATUS
            // 
            this.colBALSTATUS.Caption = "Status";
            this.colBALSTATUS.ColumnEdit = this.repositoryItemImageComboBox2;
            this.colBALSTATUS.FieldName = "BALSTATUS";
            this.colBALSTATUS.Name = "colBALSTATUS";
            this.colBALSTATUS.OptionsColumn.FixedWidth = true;
            this.colBALSTATUS.OptionsColumn.ReadOnly = true;
            this.colBALSTATUS.Width = 108;
            // 
            // repositoryItemImageComboBox2
            // 
            this.repositoryItemImageComboBox2.AutoHeight = false;
            this.repositoryItemImageComboBox2.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemImageComboBox2.Name = "repositoryItemImageComboBox2";
            // 
            // colBALObs
            // 
            this.colBALObs.Caption = "Obs";
            this.colBALObs.ColumnEdit = this.repositoryItemMemoEdit1;
            this.colBALObs.FieldName = "BALObs";
            this.colBALObs.Name = "colBALObs";
            this.colBALObs.OptionsColumn.ReadOnly = true;
            this.colBALObs.Visible = true;
            this.colBALObs.VisibleIndex = 9;
            this.colBALObs.Width = 200;
            // 
            // repositoryItemMemoEdit1
            // 
            this.repositoryItemMemoEdit1.Name = "repositoryItemMemoEdit1";
            // 
            // colBALInicioProducao
            // 
            this.colBALInicioProducao.Caption = "Inicio";
            this.colBALInicioProducao.FieldName = "BALInicioProducao";
            this.colBALInicioProducao.Name = "colBALInicioProducao";
            this.colBALInicioProducao.OptionsColumn.FixedWidth = true;
            this.colBALInicioProducao.OptionsColumn.ReadOnly = true;
            this.colBALInicioProducao.Visible = true;
            this.colBALInicioProducao.VisibleIndex = 7;
            // 
            // colBALFimProducao
            // 
            this.colBALFimProducao.Caption = "T�rmino";
            this.colBALFimProducao.FieldName = "BALFimProducao";
            this.colBALFimProducao.Name = "colBALFimProducao";
            this.colBALFimProducao.OptionsColumn.FixedWidth = true;
            this.colBALFimProducao.OptionsColumn.ReadOnly = true;
            this.colBALFimProducao.Visible = true;
            this.colBALFimProducao.VisibleIndex = 8;
            // 
            // colCONCodigo
            // 
            this.colCONCodigo.Caption = "Condom�nio";
            this.colCONCodigo.FieldName = "CONCodigo";
            this.colCONCodigo.Name = "colCONCodigo";
            this.colCONCodigo.OptionsColumn.ReadOnly = true;
            this.colCONCodigo.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Count)});
            this.colCONCodigo.Visible = true;
            this.colCONCodigo.VisibleIndex = 1;
            this.colCONCodigo.Width = 70;
            // 
            // colCONNome
            // 
            this.colCONNome.Caption = "Nome";
            this.colCONNome.FieldName = "CONNome";
            this.colCONNome.Name = "colCONNome";
            this.colCONNome.OptionsColumn.ReadOnly = true;
            this.colCONNome.Visible = true;
            this.colCONNome.VisibleIndex = 2;
            this.colCONNome.Width = 85;
            // 
            // colUSUNome
            // 
            this.colUSUNome.Caption = "Respons�vel";
            this.colUSUNome.FieldName = "USUNome";
            this.colUSUNome.Name = "colUSUNome";
            this.colUSUNome.OptionsColumn.FixedWidth = true;
            this.colUSUNome.Visible = true;
            this.colUSUNome.VisibleIndex = 4;
            this.colUSUNome.Width = 122;
            // 
            // gridColumn1
            // 
            this.gridColumn1.Caption = "IMPRESSOS";
            this.gridColumn1.ColumnEdit = this.repositoryItemButtonEdit1;
            this.gridColumn1.Fixed = DevExpress.XtraGrid.Columns.FixedStyle.Right;
            this.gridColumn1.Name = "gridColumn1";
            this.gridColumn1.OptionsColumn.FixedWidth = true;
            this.gridColumn1.Visible = true;
            this.gridColumn1.VisibleIndex = 12;
            this.gridColumn1.Width = 150;
            // 
            // repositoryItemButtonEdit1
            // 
            this.repositoryItemButtonEdit1.AutoHeight = false;
            editorButtonImageOptions1.Location = DevExpress.XtraEditors.ImageLocation.MiddleRight;
            editorButtonImageOptions2.Location = DevExpress.XtraEditors.ImageLocation.MiddleRight;
            editorButtonImageOptions3.Location = DevExpress.XtraEditors.ImageLocation.MiddleRight;
            editorButtonImageOptions4.Location = DevExpress.XtraEditors.ImageLocation.MiddleRight;
            this.repositoryItemButtonEdit1.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Ellipsis, "M1", -1, true, true, false, editorButtonImageOptions1, new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), serializableAppearanceObject1, serializableAppearanceObject2, serializableAppearanceObject3, serializableAppearanceObject4, "", null, null, DevExpress.Utils.ToolTipAnchor.Default),
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Ellipsis, "M2", -1, true, true, false, editorButtonImageOptions2, new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), serializableAppearanceObject5, serializableAppearanceObject6, serializableAppearanceObject7, serializableAppearanceObject8, "", null, null, DevExpress.Utils.ToolTipAnchor.Default),
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Ellipsis, "M3", -1, true, true, false, editorButtonImageOptions3, new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), serializableAppearanceObject9, serializableAppearanceObject10, serializableAppearanceObject11, serializableAppearanceObject12, "", null, null, DevExpress.Utils.ToolTipAnchor.Default),
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Ellipsis, "M4", -1, true, true, false, editorButtonImageOptions4, new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), serializableAppearanceObject13, serializableAppearanceObject14, serializableAppearanceObject15, serializableAppearanceObject16, "", null, null, DevExpress.Utils.ToolTipAnchor.Default)});
            this.repositoryItemButtonEdit1.Name = "repositoryItemButtonEdit1";
            this.repositoryItemButtonEdit1.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.HideTextEditor;
            this.repositoryItemButtonEdit1.ButtonClick += new DevExpress.XtraEditors.Controls.ButtonPressedEventHandler(this.repositoryItemButtonEdit1_ButtonClick);
            // 
            // colCONObsBal
            // 
            this.colCONObsBal.Caption = "Obs Bal";
            this.colCONObsBal.ColumnEdit = this.repositoryItemMemoEdit1;
            this.colCONObsBal.FieldName = "CONObsBal";
            this.colCONObsBal.Name = "colCONObsBal";
            this.colCONObsBal.OptionsColumn.ReadOnly = true;
            this.colCONObsBal.Width = 200;
            // 
            // coldataLimite
            // 
            this.coldataLimite.AppearanceCell.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
            this.coldataLimite.AppearanceCell.Options.UseFont = true;
            this.coldataLimite.Caption = "Data Limite";
            this.coldataLimite.FieldName = "dataLimite";
            this.coldataLimite.Name = "coldataLimite";
            this.coldataLimite.OptionsColumn.ReadOnly = true;
            this.coldataLimite.Visible = true;
            this.coldataLimite.VisibleIndex = 3;
            this.coldataLimite.Width = 80;
            // 
            // colPESNome
            // 
            this.colPESNome.Caption = "S�ndico";
            this.colPESNome.FieldName = "PESNome";
            this.colPESNome.Name = "colPESNome";
            this.colPESNome.OptionsColumn.ReadOnly = true;
            // 
            // colPESFone1
            // 
            this.colPESFone1.Caption = "Fone S�ndico";
            this.colPESFone1.FieldName = "PESFone1";
            this.colPESFone1.Name = "colPESFone1";
            this.colPESFone1.OptionsColumn.ReadOnly = true;
            // 
            // colFolga
            // 
            this.colFolga.Caption = "Folga";
            this.colFolga.FieldName = "Folga";
            this.colFolga.Name = "colFolga";
            // 
            // colCONAuditor1_USU
            // 
            this.colCONAuditor1_USU.Caption = "Gerente";
            this.colCONAuditor1_USU.ColumnEdit = this.LookUpUSU;
            this.colCONAuditor1_USU.FieldName = "CONAuditor1_USU";
            this.colCONAuditor1_USU.Name = "colCONAuditor1_USU";
            // 
            // LookUpUSU
            // 
            this.LookUpUSU.AutoHeight = false;
            this.LookUpUSU.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LookUpUSU.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("USUNome", "USU Nome", 60, DevExpress.Utils.FormatType.None, "", true, DevExpress.Utils.HorzAlignment.Near, DevExpress.Data.ColumnSortOrder.None, DevExpress.Utils.DefaultBoolean.Default)});
            this.LookUpUSU.DataSource = this.uSUariosBindingSource;
            this.LookUpUSU.DisplayMember = "USUNome";
            this.LookUpUSU.Name = "LookUpUSU";
            this.LookUpUSU.ShowHeader = false;
            this.LookUpUSU.ValueMember = "USU";
            // 
            // uSUariosBindingSource
            // 
            this.uSUariosBindingSource.DataMember = "USUarios";
            this.uSUariosBindingSource.DataSource = typeof(FrameworkProc.datasets.dUSUarios);
            // 
            // colBALN
            // 
            this.colBALN.Caption = "gridColumn2";
            this.colBALN.Fixed = DevExpress.XtraGrid.Columns.FixedStyle.Right;
            this.colBALN.Name = "colBALN";
            this.colBALN.OptionsColumn.ShowCaption = false;
            this.colBALN.Visible = true;
            this.colBALN.VisibleIndex = 13;
            this.colBALN.Width = 30;
            // 
            // colBAL
            // 
            this.colBAL.Caption = "gridColumn3";
            this.colBAL.Fixed = DevExpress.XtraGrid.Columns.FixedStyle.Right;
            this.colBAL.Name = "colBAL";
            this.colBAL.OptionsColumn.ShowCaption = false;
            this.colBAL.Visible = true;
            this.colBAL.VisibleIndex = 11;
            this.colBAL.Width = 30;
            // 
            // colBALred
            // 
            this.colBALred.Caption = "gridColumn4";
            this.colBALred.Fixed = DevExpress.XtraGrid.Columns.FixedStyle.Right;
            this.colBALred.Name = "colBALred";
            this.colBALred.OptionsColumn.ShowCaption = false;
            this.colBALred.Visible = true;
            this.colBALred.VisibleIndex = 10;
            this.colBALred.Width = 30;
            // 
            // colCONLimiteBal
            // 
            this.colCONLimiteBal.Caption = "Dia Limite";
            this.colCONLimiteBal.CustomizationCaption = "Dia Limite";
            this.colCONLimiteBal.FieldName = "CONLimiteBal";
            this.colCONLimiteBal.Name = "colCONLimiteBal";
            this.colCONLimiteBal.Width = 67;
            // 
            // colBAL1
            // 
            this.colBAL1.FieldName = "BAL";
            this.colBAL1.Name = "colBAL1";
            // 
            // repositoryItemButtonBalN
            // 
            this.repositoryItemButtonBalN.AutoHeight = false;
            this.repositoryItemButtonBalN.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.repositoryItemButtonBalN.Name = "repositoryItemButtonBalN";
            this.repositoryItemButtonBalN.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.HideTextEditor;
            this.repositoryItemButtonBalN.ButtonClick += new DevExpress.XtraEditors.Controls.ButtonPressedEventHandler(this.repositoryItemButtonEdit2_ButtonClick);
            // 
            // repositoryItemButtonBal
            // 
            this.repositoryItemButtonBal.AutoHeight = false;
            editorButtonImageOptions5.Image = global::Balancete.Properties.Resources.balanceteR;
            this.repositoryItemButtonBal.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Glyph, "", -1, true, true, false, editorButtonImageOptions5, new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), serializableAppearanceObject17, serializableAppearanceObject18, serializableAppearanceObject19, serializableAppearanceObject20, "", null, null, DevExpress.Utils.ToolTipAnchor.Default)});
            this.repositoryItemButtonBal.Name = "repositoryItemButtonBal";
            this.repositoryItemButtonBal.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.HideTextEditor;
            this.repositoryItemButtonBal.ButtonClick += new DevExpress.XtraEditors.Controls.ButtonPressedEventHandler(this.repositoryItemButtonBal_ButtonClick);
            // 
            // repositoryItemButtonBalRed
            // 
            this.repositoryItemButtonBalRed.AutoHeight = false;
            editorButtonImageOptions6.Image = global::Balancete.Properties.Resources.balancete;
            this.repositoryItemButtonBalRed.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Glyph, "", -1, true, true, false, editorButtonImageOptions6, new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), serializableAppearanceObject21, serializableAppearanceObject22, serializableAppearanceObject23, serializableAppearanceObject24, "", null, null, DevExpress.Utils.ToolTipAnchor.Default)});
            this.repositoryItemButtonBalRed.Name = "repositoryItemButtonBalRed";
            this.repositoryItemButtonBalRed.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.HideTextEditor;
            this.repositoryItemButtonBalRed.ButtonClick += new DevExpress.XtraEditors.Controls.ButtonPressedEventHandler(this.repositoryItemButtonBalRed_ButtonClick);
            // 
            // memoEdit1
            // 
            this.memoEdit1.Location = new System.Drawing.Point(968, 3);
            this.memoEdit1.Name = "memoEdit1";
            this.memoEdit1.Size = new System.Drawing.Size(409, 80);
            this.memoEdit1.TabIndex = 15;
            this.memoEdit1.Visible = false;
            // 
            // cGradeBalancetes
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.memoEdit1);
            this.Controls.Add(this.gridControl1);
            this.Controls.Add(this.panelControl1);
            this.Doc = System.Windows.Forms.DockStyle.Fill;
            this.LookAndFeel.UseDefaultLookAndFeel = false;
            this.Name = "cGradeBalancetes";
            this.Size = new System.Drawing.Size(1477, 807);
            this.Titulo = "Balancetes";
            ((System.ComponentModel.ISupportInitialize)(this.StyleController_F)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl1)).EndInit();
            this.panelControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.checkE7.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.spinEdit1.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkE6.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkE5.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkE4.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkE3.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkE2.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkE1.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dBalancetesBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dBalancetes)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemImageComboBox2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoEdit1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemButtonEdit1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LookUpUSU)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uSUariosBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemButtonBalN)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemButtonBal)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemButtonBalRed)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.memoEdit1.Properties)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraEditors.PanelControl panelControl1;
        private DevExpress.XtraGrid.GridControl gridControl1;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView1;
        private DevExpress.XtraEditors.CheckEdit checkE1;
        private DevExpress.XtraEditors.CheckEdit checkE3;
        private DevExpress.XtraEditors.CheckEdit checkE2;
        private DevExpress.XtraEditors.CheckEdit checkE6;
        private DevExpress.XtraEditors.CheckEdit checkE5;
        private DevExpress.XtraEditors.CheckEdit checkE4;
        private System.Windows.Forms.BindingSource dBalancetesBindingSource;
        private dBalancetes dBalancetes;
        private DevExpress.XtraGrid.Columns.GridColumn colBALCompet;
        private DevExpress.XtraGrid.Columns.GridColumn colBALDataInicio;
        private DevExpress.XtraGrid.Columns.GridColumn colBALdataFim;
        private DevExpress.XtraGrid.Columns.GridColumn colBALSTATUS;
        private DevExpress.XtraGrid.Columns.GridColumn colBALObs;
        private DevExpress.XtraEditors.Repository.RepositoryItemMemoEdit repositoryItemMemoEdit1;
        private DevExpress.XtraGrid.Columns.GridColumn colBALInicioProducao;
        private DevExpress.XtraGrid.Columns.GridColumn colBALFimProducao;
        private DevExpress.XtraEditors.Repository.RepositoryItemImageComboBox repositoryItemImageComboBox2;
        private DevExpress.XtraGrid.Columns.GridColumn colCONCodigo;
        private DevExpress.XtraGrid.Columns.GridColumn colCONNome;
        private DevExpress.XtraGrid.Columns.GridColumn colUSUNome;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn1;
        private DevExpress.XtraEditors.Repository.RepositoryItemButtonEdit repositoryItemButtonEdit1;
        private DevExpress.XtraGrid.Columns.GridColumn colCONObsBal;
        private DevExpress.XtraGrid.Columns.GridColumn coldataLimite;
        private DevExpress.XtraGrid.Columns.GridColumn colPESNome;
        private DevExpress.XtraGrid.Columns.GridColumn colPESFone1;
        private DevExpress.XtraGrid.Columns.GridColumn colFolga;
        private DevExpress.XtraGrid.Columns.GridColumn colCONAuditor1_USU;
        private DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit LookUpUSU;
        private System.Windows.Forms.BindingSource uSUariosBindingSource;
        private Framework.Lookup.LookupGerente lookupGerente1;
        private DevExpress.XtraGrid.Columns.GridColumn colBALN;
        private DevExpress.XtraEditors.Repository.RepositoryItemButtonEdit repositoryItemButtonBalN;
        private DevExpress.XtraEditors.Repository.RepositoryItemButtonEdit repositoryItemButtonBal;
        private DevExpress.XtraEditors.Repository.RepositoryItemButtonEdit repositoryItemButtonBalRed;
        private DevExpress.XtraGrid.Columns.GridColumn colBAL;
        private DevExpress.XtraGrid.Columns.GridColumn colBALred;
        private DevExpress.XtraEditors.SimpleButton BExp;
        private DevExpress.XtraGrid.Columns.GridColumn colCONLimiteBal;
        private DevExpress.XtraEditors.SimpleButton simpleButton2;
        private DevExpress.XtraEditors.SimpleButton simpleButton1;
        private DevExpress.XtraEditors.MemoEdit memoEdit1;
        private Framework.Lookup.LookupCondominio_F lookupCondominio_F1;
        private DevExpress.XtraEditors.SpinEdit spinEdit1;
        private DevExpress.XtraEditors.CheckEdit checkE7;
        private DevExpress.XtraGrid.Columns.GridColumn colBAL1;
    }
}
