using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Text;
using System.Windows.Forms;

namespace Balancete
{
    /// <summary>
    /// Pegunta sobra a��o quando o per�odo n�o bate
    /// </summary>
    public partial class cPerguntaPer : CompontesBasicos.ComponenteBaseDialog
    {
        /// <summary>
        /// Construtor padrao (n�o usar)
        /// </summary>
        public cPerguntaPer()
        {
            InitializeComponent();
        }

        /// <summary>
        /// Construtor
        /// </summary>
        /// <param name="periodoBal"></param>
        /// <param name="periodoCad"></param>
        public cPerguntaPer(string periodoBal, string periodoCad)
        {
            InitializeComponent();
            labelCAD.Text = periodoCad;
            labelBAL.Text = periodoBal;
        }

        /// <summary>
        /// Can Close
        /// </summary>
        /// <returns></returns>
        public override bool CanClose()
        {
            return (radioGroup1.SelectedIndex != -1);
        }

        /// <summary>
        /// desabilitacorrecaodecadastro
        /// </summary>
        public void desabilitacorrecaodecadastro()
        {
            radioGroup1.Properties.Items[2].Enabled = false;
        }
    }
}
