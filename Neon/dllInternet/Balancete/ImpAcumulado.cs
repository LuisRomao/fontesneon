using System;
using System.Drawing;
using System.Collections;
using DevExpress.XtraReports.UI;

namespace Balancete
{
    /// <summary>
    /// 
    /// </summary>
    public partial class ImpAcumulado : dllImpresso.ImpLogoCond
    {
        /// <summary>
        /// 
        /// </summary>
        public enum TTipo
        {
            /// <summary>
            /// 
            /// </summary>
            totalmes,
            /// <summary>
            /// 
            /// </summary>
            acordo,
            /// <summary>
            /// 
            /// </summary>
            dividido
        }

        private TTipo Tipo;
        private int deltaAcumulado;
        private int HeightDetalhe = 28;
        private int PosLinha = 28;

        XRLabel clone;
        XRLabel cloneT;
        //XRLabel cloneTot;
        XRLabel cloneSubTot;

        private void CriaColunaNoImpresso(string TituloColuna)
        {                        
            if (deltaAcumulado == 0)
            {
                clone = xrDetalheModelo;
                cloneT = xrTituloModelo;
                //cloneTot = ModeloDetalheTot;
                cloneSubTot = xrTotalModelo;
            }
            else
            {
                clone = new XRLabel();
                clone.TextAlignment = xrDetalheModelo.TextAlignment;
                clone.Size = new Size(0, 0);
                clone.CanGrow = false;
                clone.WordWrap = false;
                clone.Font = xrDetalheModelo.Font;
                Bands[BandKind.Detail].Controls.Add(clone);
                clone.Size = new Size(xrDetalheModelo.Width, xrDetalheModelo.Height);
                clone.Location = new Point(xrDetalheModelo.Location.X + deltaAcumulado, xrDetalheModelo.Location.Y);
                Bands[BandKind.Detail].Height = HeightDetalhe;// *(Tipo == TTipo.dividido ? 10 : 1);

                if (Tipo == TTipo.dividido)
                {
                    XRLabel cloneD = new XRLabel();
                    cloneD.TextAlignment = xrDetalheModelo.TextAlignment;
                    cloneD.Size = new Size(0, 0);
                    cloneD.CanGrow = true;
                    cloneD.WordWrap = true;
                    cloneD.Font = xrDetalheModelo.Font;
                    cloneD.Multiline = true;
                    Bands[BandKind.Detail].Controls.Add(cloneD);
                    //cloneD.Size = new Size(xrDetalheModelo.Width, xrDetalheModelo.Height);
                    cloneD.Size = new Size(xrDetalheModelo.Width, 0);
                    cloneD.Location = new Point(xrDetalheModelo.Location.X + deltaAcumulado, xrDetalheModelo.Location.Y + +clone.Height + 5);
                    Bands[BandKind.Detail].Height = HeightDetalhe;// *(Tipo == TTipo.dividido ? 4 : 1);
                    cloneD.DataBindings.Add("Text", bindingdAcumulado, TituloColuna + "IMP", "{0}");
                    PosLinha = cloneD.Location.Y + cloneD.Height;
                }
                

                cloneSubTot = new XRLabel();
                Bands[BandKind.GroupFooter].Controls.Add(cloneSubTot);
                cloneSubTot.TextAlignment = xrTotalModelo.TextAlignment;
                cloneSubTot.Location = new Point(xrTotalModelo.Location.X + deltaAcumulado, xrTotalModelo.Location.Y);
                cloneSubTot.Size = new Size(xrTotalModelo.Width, xrTotalModelo.Height);
                cloneSubTot.Font = xrTotalModelo.Font;
                cloneSubTot.Summary.FormatString = xrTotalModelo.Summary.FormatString;
                cloneSubTot.Summary.Func = xrTotalModelo.Summary.Func;
                cloneSubTot.Summary.Running = xrTotalModelo.Summary.Running;

                cloneT = new XRLabel();
                Bands[BandKind.GroupHeader].Controls.Add(cloneT);
                cloneT.TextAlignment = xrTituloModelo.TextAlignment;
                cloneT.Location = new Point(xrTituloModelo.Location.X + deltaAcumulado, xrTituloModelo.Location.Y);
                cloneT.Size = new Size(xrTituloModelo.Width, xrTituloModelo.Height);
                cloneT.Font = xrTituloModelo.Font;
                cloneT.Multiline = true;
                cloneT.WordWrap = true;
                cloneT.Font = xrTituloModelo.Font;

            }
            clone.Draw += Degrade;
            //clone.Visible = false;
            switch (Tipo)
            {
                case TTipo.acordo :
                    clone.DataBindings.Add("Text", bindingdAcumulado, TituloColuna + "AC", "{0:n2}");
                    break;
                case TTipo.dividido:
                    clone.DataBindings.Add("Text", bindingdAcumulado, TituloColuna + "IMPT", "{0:n2}");
                    break;
                default:
                case TTipo.totalmes:
                    clone.DataBindings.Add("Text", bindingdAcumulado, TituloColuna + "M", "{0:n2}");
                    break;
            }
            
            cloneSubTot.DataBindings.Add("Text", bindingdAcumulado, TituloColuna + ((Tipo == TTipo.acordo) ? "AC" : "M"), "{0:n2}");
            
            
            cloneT.Text = TituloColuna;
            deltaAcumulado += xrDetalheModelo.Width + 10;
            
        }


        private void ColocaLinha()
        {
            XRLine Novalinha = new XRLine();
            Novalinha.Size = new Size(10, 5);
            Novalinha.Width = 1;
            Bands[BandKind.Detail].Controls.Add(Novalinha);
            //Novalinha.Location = new Point(0, HeightDetalhe);
            Novalinha.Location = new Point(0, PosLinha);
            Bands[BandKind.Detail].Height = HeightDetalhe;
            Novalinha.Width = xrtotal.Location.X + xrtotal.Width;                         
        }

        private void AjusteDinamico(dAcumulado _dAcumulado,bool comLinhas,ArrayList Competencias)
        {
            //HeightDetalhe = Bands[BandKind.Detail].Height;
            bindingdAcumulado.DataSource = _dAcumulado;
            
            foreach (Framework.objetosNeon.Competencia TColuna in Competencias)            
                CriaColunaNoImpresso(TColuna.ToString());

            if (clone.Location.X + clone.Width + 10 > xrtotal.Location.X)
                Landscape = true;
            if (Landscape)
            {
                int DeltaFinal = clone.Location.X + clone.Width + 10 - xrtotal.Location.X;
                xrtotal.Location = new Point(clone.Location.X + clone.Width + 10, xrtotal.Location.Y);
                xrtotalTitulo.Location = new Point(clone.Location.X + clone.Width + 10, xrtotalTitulo.Location.Y);
                xrtotalSoma.Location = new Point(clone.Location.X + clone.Width + 10, xrtotalSoma.Location.Y);
                xrPanel1.Size = new Size(xrPanel1.Width + DeltaFinal, xrPanel1.Height); 
                
            };
            
            if (comLinhas)
                ColocaLinha();
        }

        /// <summary>
        /// Construtor
        /// </summary>
        /// <param name="_dAcumulado"></param>
        /// <param name="comLinhas"></param>
        /// <param name="CON"></param>
        /// <param name="CONNome"></param>
        /// <param name="Competencias"></param>
        /// <param name="_Tipo"></param>
        public ImpAcumulado(dAcumulado _dAcumulado, bool comLinhas, int CON, string CONNome, ArrayList Competencias,TTipo _Tipo)
        {
            InitializeComponent();            
            if (Framework.DSCentral.EMP == 1)
                Apontamento(CON, 142);
            Tipo = _Tipo;
            if (Tipo == TTipo.acordo)
                xrTitulo.Text = xrTitulo.Text + " - ACORDOS";
            SetNome(CONNome);
            AjusteDinamico(_dAcumulado,comLinhas,Competencias);
        }

        private void DrawDegrade(object sender, DrawEventArgs e)
        {
            Degrade(sender, e);
        }

    }
}
