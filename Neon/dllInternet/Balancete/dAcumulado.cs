﻿namespace Balancete {


    partial class dAcumulado
    {
        private static dAcumulado _dAcumuladoSt;

        /// <summary>
        /// dataset estático:dAcumulado
        /// </summary>
        public static dAcumulado dAcumuladoSt
        {
            get
            {
                if (_dAcumuladoSt == null)
                    _dAcumuladoSt = new dAcumulado();
                return _dAcumuladoSt;
            }
        }
    }
}
