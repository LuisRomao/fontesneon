using System;
using System.Collections.Generic;
using System.Windows.Forms;
using VirDB.Bancovirtual;

namespace TesteWSSincBancos
{
    static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main()
        {
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);            
            BancoVirtual.PopularInternet("INTERNET Local 1", @"Data Source=Lapvirtual2017\sqlexpress;Initial Catalog=Neonimoveis_1;User ID=sa;Password=venus", 1);
            BancoVirtual.PopularInternet("INTERNET Local 2", @"Data Source=Lapvirtual2017\sqlexpress;Initial Catalog=Neonimoveis_2;User ID=sa;Password=venus", 2);
            BancoVirtual.PopularInternet("INTERNET REAL 1", @"Data Source=cp.neonimoveis.com.br;Initial Catalog=neonimoveis_1;Persist Security Info=True;User ID=NEONP;Password=TPC4P2DP", 1);
            BancoVirtual.PopularInternet("INTERNET REAL 2", @"Data Source=cp.neonimoveis.com.br;Initial Catalog=neonimoveis_2;Persist Security Info=True;User ID=NEONP;Password=TPC4P2DP", 2);
            Application.Run(new Form1());
        }
    }
}