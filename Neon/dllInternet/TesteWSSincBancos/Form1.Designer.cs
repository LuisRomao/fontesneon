namespace TesteWSSincBancos
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.button1 = new System.Windows.Forms.Button();
            this.textBox1 = new System.Windows.Forms.TextBox();
            this.xtraTabControl1 = new DevExpress.XtraTab.XtraTabControl();
            this.xtraTabPage1 = new DevExpress.XtraTab.XtraTabPage();
            this.xtraTabPage2 = new DevExpress.XtraTab.XtraTabPage();
            this.gridControl1 = new DevExpress.XtraGrid.GridControl();
            this.gridView1 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colCODCON = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colNome = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colNumBanco = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colcc = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colAgencia = new DevExpress.XtraGrid.Columns.GridColumn();
            this.coldgagencia = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colMultasql = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colEnd = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colBairro = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colCEP = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colCidade = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colEstado = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colCNPJ = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSindico = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colmandato = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colOBS = new DevExpress.XtraGrid.Columns.GridColumn();
            this.xtraTabPage3 = new DevExpress.XtraTab.XtraTabPage();
            this.gridControl2 = new DevExpress.XtraGrid.GridControl();
            this.gridView2 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colNome1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colUsername = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSenha = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colEmail = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colCODCON1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colApartamento = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colbloco = new DevExpress.XtraGrid.Columns.GridColumn();
            this.coltipousuario = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colAcordo = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colJudicial = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colendereco = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colbairro1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colcep1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colcidade1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colestado1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.coltelefone1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.coltelefone2 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.coltelefone3 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colCorreio = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colbemail = new DevExpress.XtraGrid.Columns.GridColumn();
            this.coljuridico = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colProprietario = new DevExpress.XtraGrid.Columns.GridColumn();
            this.xtraTabPage4 = new DevExpress.XtraTab.XtraTabPage();
            this.simpleButton5 = new DevExpress.XtraEditors.SimpleButton();
            this.simpleButton4 = new DevExpress.XtraEditors.SimpleButton();
            this.chBAL = new DevExpress.XtraEditors.CheckEdit();
            this.spinEdit1 = new DevExpress.XtraEditors.SpinEdit();
            this.memoEdit1 = new DevExpress.XtraEditors.MemoEdit();
            this.xtraTabPage5 = new DevExpress.XtraTab.XtraTabPage();
            this.gridResultado = new DevExpress.XtraGrid.GridControl();
            this.gridView3 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.xtraTabPage6 = new DevExpress.XtraTab.XtraTabPage();
            this.checkEdit1 = new DevExpress.XtraEditors.CheckEdit();
            this.labelControl1 = new DevExpress.XtraEditors.LabelControl();
            this.progressBarControl1 = new DevExpress.XtraEditors.ProgressBarControl();
            this.textEditPARA = new DevExpress.XtraEditors.TextEdit();
            this.textEditDE = new DevExpress.XtraEditors.TextEdit();
            this.simpleButton6 = new DevExpress.XtraEditors.SimpleButton();
            this.panelControl1 = new DevExpress.XtraEditors.PanelControl();
            this.simpleButton2 = new DevExpress.XtraEditors.SimpleButton();
            ((System.ComponentModel.ISupportInitialize)(this.xtraTabControl1)).BeginInit();
            this.xtraTabControl1.SuspendLayout();
            this.xtraTabPage1.SuspendLayout();
            this.xtraTabPage2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).BeginInit();
            this.xtraTabPage3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView2)).BeginInit();
            this.xtraTabPage4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.chBAL.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.spinEdit1.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.memoEdit1.Properties)).BeginInit();
            this.xtraTabPage5.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridResultado)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView3)).BeginInit();
            this.xtraTabPage6.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit1.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.progressBarControl1.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEditPARA.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEditDE.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl1)).BeginInit();
            this.panelControl1.SuspendLayout();
            this.SuspendLayout();
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(5, 5);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(75, 23);
            this.button1.TabIndex = 0;
            this.button1.Text = "Teste WS";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // textBox1
            // 
            this.textBox1.AcceptsReturn = true;
            this.textBox1.AcceptsTab = true;
            this.textBox1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.textBox1.Location = new System.Drawing.Point(0, 0);
            this.textBox1.Multiline = true;
            this.textBox1.Name = "textBox1";
            this.textBox1.ScrollBars = System.Windows.Forms.ScrollBars.Both;
            this.textBox1.Size = new System.Drawing.Size(735, 552);
            this.textBox1.TabIndex = 1;
            this.textBox1.WordWrap = false;
            // 
            // xtraTabControl1
            // 
            this.xtraTabControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.xtraTabControl1.Location = new System.Drawing.Point(0, 40);
            this.xtraTabControl1.Name = "xtraTabControl1";
            this.xtraTabControl1.SelectedTabPage = this.xtraTabPage1;
            this.xtraTabControl1.Size = new System.Drawing.Size(741, 580);
            this.xtraTabControl1.TabIndex = 3;
            this.xtraTabControl1.TabPages.AddRange(new DevExpress.XtraTab.XtraTabPage[] {
            this.xtraTabPage1,
            this.xtraTabPage2,
            this.xtraTabPage3,
            this.xtraTabPage4,
            this.xtraTabPage5,
            this.xtraTabPage6});
            // 
            // xtraTabPage1
            // 
            this.xtraTabPage1.Controls.Add(this.textBox1);
            this.xtraTabPage1.Name = "xtraTabPage1";
            this.xtraTabPage1.Size = new System.Drawing.Size(735, 552);
            this.xtraTabPage1.Text = "xtraTabPage1";
            // 
            // xtraTabPage2
            // 
            this.xtraTabPage2.Controls.Add(this.gridControl1);
            this.xtraTabPage2.Name = "xtraTabPage2";
            this.xtraTabPage2.Size = new System.Drawing.Size(735, 552);
            this.xtraTabPage2.Text = "xtraTabPage2";
            // 
            // gridControl1
            // 
            this.gridControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridControl1.Location = new System.Drawing.Point(0, 0);
            this.gridControl1.MainView = this.gridView1;
            this.gridControl1.Name = "gridControl1";
            this.gridControl1.Size = new System.Drawing.Size(735, 552);
            this.gridControl1.TabIndex = 0;
            this.gridControl1.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridView1});
            // 
            // gridView1
            // 
            this.gridView1.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colCODCON,
            this.colNome,
            this.colNumBanco,
            this.colcc,
            this.colAgencia,
            this.coldgagencia,
            this.colMultasql,
            this.colEnd,
            this.colBairro,
            this.colCEP,
            this.colCidade,
            this.colEstado,
            this.colCNPJ,
            this.colSindico,
            this.colmandato,
            this.colOBS});
            this.gridView1.GridControl = this.gridControl1;
            this.gridView1.Name = "gridView1";
            // 
            // colCODCON
            // 
            this.colCODCON.FieldName = "CODCON";
            this.colCODCON.Name = "colCODCON";
            this.colCODCON.Visible = true;
            this.colCODCON.VisibleIndex = 0;
            // 
            // colNome
            // 
            this.colNome.FieldName = "Nome";
            this.colNome.Name = "colNome";
            this.colNome.Visible = true;
            this.colNome.VisibleIndex = 1;
            // 
            // colNumBanco
            // 
            this.colNumBanco.FieldName = "NumBanco";
            this.colNumBanco.Name = "colNumBanco";
            this.colNumBanco.Visible = true;
            this.colNumBanco.VisibleIndex = 2;
            // 
            // colcc
            // 
            this.colcc.FieldName = "cc";
            this.colcc.Name = "colcc";
            this.colcc.Visible = true;
            this.colcc.VisibleIndex = 3;
            // 
            // colAgencia
            // 
            this.colAgencia.FieldName = "Agencia";
            this.colAgencia.Name = "colAgencia";
            this.colAgencia.Visible = true;
            this.colAgencia.VisibleIndex = 4;
            // 
            // coldgagencia
            // 
            this.coldgagencia.FieldName = "dgagencia";
            this.coldgagencia.Name = "coldgagencia";
            this.coldgagencia.Visible = true;
            this.coldgagencia.VisibleIndex = 5;
            // 
            // colMultasql
            // 
            this.colMultasql.FieldName = "Multasql";
            this.colMultasql.Name = "colMultasql";
            this.colMultasql.Visible = true;
            this.colMultasql.VisibleIndex = 6;
            // 
            // colEnd
            // 
            this.colEnd.FieldName = "End";
            this.colEnd.Name = "colEnd";
            this.colEnd.Visible = true;
            this.colEnd.VisibleIndex = 7;
            // 
            // colBairro
            // 
            this.colBairro.FieldName = "Bairro";
            this.colBairro.Name = "colBairro";
            this.colBairro.Visible = true;
            this.colBairro.VisibleIndex = 8;
            // 
            // colCEP
            // 
            this.colCEP.FieldName = "CEP";
            this.colCEP.Name = "colCEP";
            this.colCEP.Visible = true;
            this.colCEP.VisibleIndex = 9;
            // 
            // colCidade
            // 
            this.colCidade.FieldName = "Cidade";
            this.colCidade.Name = "colCidade";
            this.colCidade.Visible = true;
            this.colCidade.VisibleIndex = 10;
            // 
            // colEstado
            // 
            this.colEstado.FieldName = "Estado";
            this.colEstado.Name = "colEstado";
            this.colEstado.Visible = true;
            this.colEstado.VisibleIndex = 11;
            // 
            // colCNPJ
            // 
            this.colCNPJ.FieldName = "CNPJ";
            this.colCNPJ.Name = "colCNPJ";
            this.colCNPJ.Visible = true;
            this.colCNPJ.VisibleIndex = 12;
            // 
            // colSindico
            // 
            this.colSindico.FieldName = "Sindico";
            this.colSindico.Name = "colSindico";
            this.colSindico.Visible = true;
            this.colSindico.VisibleIndex = 13;
            // 
            // colmandato
            // 
            this.colmandato.FieldName = "mandato";
            this.colmandato.Name = "colmandato";
            this.colmandato.Visible = true;
            this.colmandato.VisibleIndex = 14;
            // 
            // colOBS
            // 
            this.colOBS.FieldName = "OBS";
            this.colOBS.Name = "colOBS";
            this.colOBS.Visible = true;
            this.colOBS.VisibleIndex = 15;
            // 
            // xtraTabPage3
            // 
            this.xtraTabPage3.Controls.Add(this.gridControl2);
            this.xtraTabPage3.Name = "xtraTabPage3";
            this.xtraTabPage3.Size = new System.Drawing.Size(735, 552);
            this.xtraTabPage3.Text = "xtraTabPage3";
            // 
            // gridControl2
            // 
            this.gridControl2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridControl2.Location = new System.Drawing.Point(0, 0);
            this.gridControl2.MainView = this.gridView2;
            this.gridControl2.Name = "gridControl2";
            this.gridControl2.Size = new System.Drawing.Size(735, 552);
            this.gridControl2.TabIndex = 0;
            this.gridControl2.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridView2});
            // 
            // gridView2
            // 
            this.gridView2.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colID,
            this.colNome1,
            this.colUsername,
            this.colSenha,
            this.colEmail,
            this.colCODCON1,
            this.colApartamento,
            this.colbloco,
            this.coltipousuario,
            this.colAcordo,
            this.colJudicial,
            this.colendereco,
            this.colbairro1,
            this.colcep1,
            this.colcidade1,
            this.colestado1,
            this.coltelefone1,
            this.coltelefone2,
            this.coltelefone3,
            this.colCorreio,
            this.colbemail,
            this.coljuridico,
            this.colProprietario});
            this.gridView2.GridControl = this.gridControl2;
            this.gridView2.Name = "gridView2";
            this.gridView2.OptionsView.ShowAutoFilterRow = true;
            // 
            // colID
            // 
            this.colID.FieldName = "ID";
            this.colID.Name = "colID";
            this.colID.Visible = true;
            this.colID.VisibleIndex = 0;
            // 
            // colNome1
            // 
            this.colNome1.FieldName = "Nome";
            this.colNome1.Name = "colNome1";
            this.colNome1.Visible = true;
            this.colNome1.VisibleIndex = 1;
            // 
            // colUsername
            // 
            this.colUsername.FieldName = "Username";
            this.colUsername.Name = "colUsername";
            this.colUsername.Visible = true;
            this.colUsername.VisibleIndex = 2;
            // 
            // colSenha
            // 
            this.colSenha.FieldName = "Senha";
            this.colSenha.Name = "colSenha";
            this.colSenha.Visible = true;
            this.colSenha.VisibleIndex = 3;
            // 
            // colEmail
            // 
            this.colEmail.FieldName = "Email";
            this.colEmail.Name = "colEmail";
            this.colEmail.Visible = true;
            this.colEmail.VisibleIndex = 4;
            // 
            // colCODCON1
            // 
            this.colCODCON1.FieldName = "CODCON";
            this.colCODCON1.Name = "colCODCON1";
            this.colCODCON1.Visible = true;
            this.colCODCON1.VisibleIndex = 5;
            // 
            // colApartamento
            // 
            this.colApartamento.FieldName = "Apartamento";
            this.colApartamento.Name = "colApartamento";
            this.colApartamento.Visible = true;
            this.colApartamento.VisibleIndex = 6;
            // 
            // colbloco
            // 
            this.colbloco.FieldName = "bloco";
            this.colbloco.Name = "colbloco";
            this.colbloco.Visible = true;
            this.colbloco.VisibleIndex = 7;
            // 
            // coltipousuario
            // 
            this.coltipousuario.FieldName = "tipousuario";
            this.coltipousuario.Name = "coltipousuario";
            this.coltipousuario.Visible = true;
            this.coltipousuario.VisibleIndex = 8;
            // 
            // colAcordo
            // 
            this.colAcordo.FieldName = "Acordo";
            this.colAcordo.Name = "colAcordo";
            this.colAcordo.Visible = true;
            this.colAcordo.VisibleIndex = 9;
            // 
            // colJudicial
            // 
            this.colJudicial.FieldName = "Judicial";
            this.colJudicial.Name = "colJudicial";
            this.colJudicial.Visible = true;
            this.colJudicial.VisibleIndex = 10;
            // 
            // colendereco
            // 
            this.colendereco.FieldName = "endereco";
            this.colendereco.Name = "colendereco";
            this.colendereco.Visible = true;
            this.colendereco.VisibleIndex = 11;
            // 
            // colbairro1
            // 
            this.colbairro1.FieldName = "bairro";
            this.colbairro1.Name = "colbairro1";
            this.colbairro1.Visible = true;
            this.colbairro1.VisibleIndex = 12;
            // 
            // colcep1
            // 
            this.colcep1.FieldName = "cep";
            this.colcep1.Name = "colcep1";
            this.colcep1.Visible = true;
            this.colcep1.VisibleIndex = 13;
            // 
            // colcidade1
            // 
            this.colcidade1.FieldName = "cidade";
            this.colcidade1.Name = "colcidade1";
            this.colcidade1.Visible = true;
            this.colcidade1.VisibleIndex = 14;
            // 
            // colestado1
            // 
            this.colestado1.FieldName = "estado";
            this.colestado1.Name = "colestado1";
            this.colestado1.Visible = true;
            this.colestado1.VisibleIndex = 15;
            // 
            // coltelefone1
            // 
            this.coltelefone1.FieldName = "telefone1";
            this.coltelefone1.Name = "coltelefone1";
            this.coltelefone1.Visible = true;
            this.coltelefone1.VisibleIndex = 16;
            // 
            // coltelefone2
            // 
            this.coltelefone2.FieldName = "telefone2";
            this.coltelefone2.Name = "coltelefone2";
            this.coltelefone2.Visible = true;
            this.coltelefone2.VisibleIndex = 17;
            // 
            // coltelefone3
            // 
            this.coltelefone3.FieldName = "telefone3";
            this.coltelefone3.Name = "coltelefone3";
            this.coltelefone3.Visible = true;
            this.coltelefone3.VisibleIndex = 18;
            // 
            // colCorreio
            // 
            this.colCorreio.FieldName = "Correio";
            this.colCorreio.Name = "colCorreio";
            this.colCorreio.Visible = true;
            this.colCorreio.VisibleIndex = 19;
            // 
            // colbemail
            // 
            this.colbemail.FieldName = "bemail";
            this.colbemail.Name = "colbemail";
            this.colbemail.Visible = true;
            this.colbemail.VisibleIndex = 20;
            // 
            // coljuridico
            // 
            this.coljuridico.FieldName = "juridico";
            this.coljuridico.Name = "coljuridico";
            this.coljuridico.Visible = true;
            this.coljuridico.VisibleIndex = 21;
            // 
            // colProprietario
            // 
            this.colProprietario.FieldName = "Proprietario";
            this.colProprietario.Name = "colProprietario";
            this.colProprietario.Visible = true;
            this.colProprietario.VisibleIndex = 22;
            // 
            // xtraTabPage4
            // 
            this.xtraTabPage4.Controls.Add(this.simpleButton5);
            this.xtraTabPage4.Controls.Add(this.simpleButton4);
            this.xtraTabPage4.Controls.Add(this.chBAL);
            this.xtraTabPage4.Controls.Add(this.spinEdit1);
            this.xtraTabPage4.Controls.Add(this.memoEdit1);
            this.xtraTabPage4.Name = "xtraTabPage4";
            this.xtraTabPage4.Size = new System.Drawing.Size(735, 552);
            this.xtraTabPage4.Text = "SQL";
            // 
            // simpleButton5
            // 
            this.simpleButton5.Location = new System.Drawing.Point(450, 14);
            this.simpleButton5.Name = "simpleButton5";
            this.simpleButton5.Size = new System.Drawing.Size(99, 23);
            this.simpleButton5.TabIndex = 6;
            this.simpleButton5.Text = "Net Execute";
            this.simpleButton5.Click += new System.EventHandler(this.simpleButton5_Click);
            // 
            // simpleButton4
            // 
            this.simpleButton4.Location = new System.Drawing.Point(555, 14);
            this.simpleButton4.Name = "simpleButton4";
            this.simpleButton4.Size = new System.Drawing.Size(126, 23);
            this.simpleButton4.TabIndex = 5;
            this.simpleButton4.Text = "NET Select";
            this.simpleButton4.Click += new System.EventHandler(this.simpleButton4_Click);
            // 
            // chBAL
            // 
            this.chBAL.Location = new System.Drawing.Point(389, 17);
            this.chBAL.Name = "chBAL";
            this.chBAL.Properties.Caption = "BAL";
            this.chBAL.Size = new System.Drawing.Size(75, 19);
            this.chBAL.TabIndex = 4;
            // 
            // spinEdit1
            // 
            this.spinEdit1.EditValue = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.spinEdit1.Location = new System.Drawing.Point(283, 17);
            this.spinEdit1.Name = "spinEdit1";
            this.spinEdit1.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.spinEdit1.Size = new System.Drawing.Size(100, 20);
            this.spinEdit1.TabIndex = 2;
            // 
            // memoEdit1
            // 
            this.memoEdit1.Location = new System.Drawing.Point(19, 43);
            this.memoEdit1.Name = "memoEdit1";
            this.memoEdit1.Size = new System.Drawing.Size(697, 501);
            this.memoEdit1.TabIndex = 1;
            // 
            // xtraTabPage5
            // 
            this.xtraTabPage5.Controls.Add(this.gridResultado);
            this.xtraTabPage5.Name = "xtraTabPage5";
            this.xtraTabPage5.Size = new System.Drawing.Size(735, 552);
            this.xtraTabPage5.Text = "Resultado";
            // 
            // gridResultado
            // 
            this.gridResultado.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridResultado.Location = new System.Drawing.Point(0, 0);
            this.gridResultado.MainView = this.gridView3;
            this.gridResultado.Name = "gridResultado";
            this.gridResultado.Size = new System.Drawing.Size(735, 552);
            this.gridResultado.TabIndex = 0;
            this.gridResultado.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridView3});
            // 
            // gridView3
            // 
            this.gridView3.GridControl = this.gridResultado;
            this.gridView3.Name = "gridView3";
            // 
            // xtraTabPage6
            // 
            this.xtraTabPage6.Controls.Add(this.checkEdit1);
            this.xtraTabPage6.Controls.Add(this.labelControl1);
            this.xtraTabPage6.Controls.Add(this.progressBarControl1);
            this.xtraTabPage6.Controls.Add(this.textEditPARA);
            this.xtraTabPage6.Controls.Add(this.textEditDE);
            this.xtraTabPage6.Controls.Add(this.simpleButton6);
            this.xtraTabPage6.Name = "xtraTabPage6";
            this.xtraTabPage6.Size = new System.Drawing.Size(735, 552);
            this.xtraTabPage6.Text = "Transfere Banco";
            // 
            // checkEdit1
            // 
            this.checkEdit1.Location = new System.Drawing.Point(122, 77);
            this.checkEdit1.Name = "checkEdit1";
            this.checkEdit1.Properties.Caption = "Producao";
            this.checkEdit1.Size = new System.Drawing.Size(75, 19);
            this.checkEdit1.TabIndex = 5;
            // 
            // labelControl1
            // 
            this.labelControl1.Location = new System.Drawing.Point(23, 155);
            this.labelControl1.Name = "labelControl1";
            this.labelControl1.Size = new System.Drawing.Size(63, 13);
            this.labelControl1.TabIndex = 4;
            this.labelControl1.Text = "labelControl1";
            // 
            // progressBarControl1
            // 
            this.progressBarControl1.Location = new System.Drawing.Point(21, 174);
            this.progressBarControl1.Name = "progressBarControl1";
            this.progressBarControl1.Size = new System.Drawing.Size(671, 18);
            this.progressBarControl1.TabIndex = 3;
            // 
            // textEditPARA
            // 
            this.textEditPARA.EditValue = "Data Source=Lapvirtual2017\\sqlexpress;Initial Catalog=Neonimoveis_2copia;User ID=sa;" +
    "Password=venus";
            this.textEditPARA.Location = new System.Drawing.Point(124, 51);
            this.textEditPARA.Name = "textEditPARA";
            this.textEditPARA.Size = new System.Drawing.Size(568, 20);
            this.textEditPARA.TabIndex = 2;
            // 
            // textEditDE
            // 
            this.textEditDE.EditValue = "Data Source=Lapvirtual2017\\sqlexpress;Initial Catalog=Neonimoveis_2;User ID=sa;Passw" +
    "ord=venus";
            this.textEditDE.Location = new System.Drawing.Point(124, 25);
            this.textEditDE.Name = "textEditDE";
            this.textEditDE.Size = new System.Drawing.Size(568, 20);
            this.textEditDE.TabIndex = 1;
            // 
            // simpleButton6
            // 
            this.simpleButton6.Location = new System.Drawing.Point(11, 22);
            this.simpleButton6.Name = "simpleButton6";
            this.simpleButton6.Size = new System.Drawing.Size(75, 23);
            this.simpleButton6.TabIndex = 0;
            this.simpleButton6.Text = "simpleButton6";
            this.simpleButton6.Click += new System.EventHandler(this.simpleButton6_Click);
            // 
            // panelControl1
            // 
            this.panelControl1.Controls.Add(this.simpleButton2);
            this.panelControl1.Controls.Add(this.button1);
            this.panelControl1.Dock = System.Windows.Forms.DockStyle.Top;
            this.panelControl1.Location = new System.Drawing.Point(0, 0);
            this.panelControl1.Name = "panelControl1";
            this.panelControl1.Size = new System.Drawing.Size(741, 40);
            this.panelControl1.TabIndex = 4;
            // 
            // simpleButton2
            // 
            this.simpleButton2.Location = new System.Drawing.Point(86, 5);
            this.simpleButton2.Name = "simpleButton2";
            this.simpleButton2.Size = new System.Drawing.Size(75, 23);
            this.simpleButton2.TabIndex = 5;
            this.simpleButton2.Text = "Teste WS 2";
            this.simpleButton2.Click += new System.EventHandler(this.simpleButton2_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(741, 620);
            this.Controls.Add(this.xtraTabControl1);
            this.Controls.Add(this.panelControl1);
            this.Name = "Form1";
            this.Text = "Form1";
            ((System.ComponentModel.ISupportInitialize)(this.xtraTabControl1)).EndInit();
            this.xtraTabControl1.ResumeLayout(false);
            this.xtraTabPage1.ResumeLayout(false);
            this.xtraTabPage1.PerformLayout();
            this.xtraTabPage2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gridControl1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).EndInit();
            this.xtraTabPage3.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gridControl2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView2)).EndInit();
            this.xtraTabPage4.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.chBAL.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.spinEdit1.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.memoEdit1.Properties)).EndInit();
            this.xtraTabPage5.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gridResultado)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView3)).EndInit();
            this.xtraTabPage6.ResumeLayout(false);
            this.xtraTabPage6.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit1.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.progressBarControl1.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEditPARA.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEditDE.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl1)).EndInit();
            this.panelControl1.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.TextBox textBox1;
        private DevExpress.XtraTab.XtraTabControl xtraTabControl1;
        private DevExpress.XtraTab.XtraTabPage xtraTabPage1;
        private DevExpress.XtraTab.XtraTabPage xtraTabPage2;
        private DevExpress.XtraGrid.GridControl gridControl1;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView1;
        private DevExpress.XtraEditors.PanelControl panelControl1;
        private DevExpress.XtraGrid.Columns.GridColumn colCODCON;
        private DevExpress.XtraGrid.Columns.GridColumn colNome;
        private DevExpress.XtraGrid.Columns.GridColumn colNumBanco;
        private DevExpress.XtraGrid.Columns.GridColumn colcc;
        private DevExpress.XtraGrid.Columns.GridColumn colAgencia;
        private DevExpress.XtraGrid.Columns.GridColumn coldgagencia;
        private DevExpress.XtraGrid.Columns.GridColumn colMultasql;
        private DevExpress.XtraGrid.Columns.GridColumn colEnd;
        private DevExpress.XtraGrid.Columns.GridColumn colBairro;
        private DevExpress.XtraGrid.Columns.GridColumn colCEP;
        private DevExpress.XtraGrid.Columns.GridColumn colCidade;
        private DevExpress.XtraGrid.Columns.GridColumn colEstado;
        private DevExpress.XtraGrid.Columns.GridColumn colCNPJ;
        private DevExpress.XtraGrid.Columns.GridColumn colSindico;
        private DevExpress.XtraGrid.Columns.GridColumn colmandato;
        private DevExpress.XtraGrid.Columns.GridColumn colOBS;
        private DevExpress.XtraTab.XtraTabPage xtraTabPage3;
        private DevExpress.XtraGrid.GridControl gridControl2;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView2;
        private DevExpress.XtraGrid.Columns.GridColumn colID;
        private DevExpress.XtraGrid.Columns.GridColumn colNome1;
        private DevExpress.XtraGrid.Columns.GridColumn colUsername;
        private DevExpress.XtraGrid.Columns.GridColumn colSenha;
        private DevExpress.XtraGrid.Columns.GridColumn colEmail;
        private DevExpress.XtraGrid.Columns.GridColumn colCODCON1;
        private DevExpress.XtraGrid.Columns.GridColumn colApartamento;
        private DevExpress.XtraGrid.Columns.GridColumn colbloco;
        private DevExpress.XtraGrid.Columns.GridColumn coltipousuario;
        private DevExpress.XtraGrid.Columns.GridColumn colAcordo;
        private DevExpress.XtraGrid.Columns.GridColumn colJudicial;
        private DevExpress.XtraGrid.Columns.GridColumn colendereco;
        private DevExpress.XtraGrid.Columns.GridColumn colbairro1;
        private DevExpress.XtraGrid.Columns.GridColumn colcep1;
        private DevExpress.XtraGrid.Columns.GridColumn colcidade1;
        private DevExpress.XtraGrid.Columns.GridColumn colestado1;
        private DevExpress.XtraGrid.Columns.GridColumn coltelefone1;
        private DevExpress.XtraGrid.Columns.GridColumn coltelefone2;
        private DevExpress.XtraGrid.Columns.GridColumn coltelefone3;
        private DevExpress.XtraGrid.Columns.GridColumn colCorreio;
        private DevExpress.XtraGrid.Columns.GridColumn colbemail;
        private DevExpress.XtraGrid.Columns.GridColumn coljuridico;
        private DevExpress.XtraGrid.Columns.GridColumn colProprietario;
        private DevExpress.XtraTab.XtraTabPage xtraTabPage4;
        private DevExpress.XtraEditors.SpinEdit spinEdit1;
        private DevExpress.XtraEditors.MemoEdit memoEdit1;
        private DevExpress.XtraEditors.SimpleButton simpleButton2;
        private DevExpress.XtraEditors.CheckEdit chBAL;
        private DevExpress.XtraTab.XtraTabPage xtraTabPage5;
        private DevExpress.XtraGrid.GridControl gridResultado;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView3;
        private DevExpress.XtraEditors.SimpleButton simpleButton4;
        private DevExpress.XtraEditors.SimpleButton simpleButton5;
        private DevExpress.XtraTab.XtraTabPage xtraTabPage6;
        private DevExpress.XtraEditors.SimpleButton simpleButton6;
        private DevExpress.XtraEditors.TextEdit textEditPARA;
        private DevExpress.XtraEditors.TextEdit textEditDE;
        private DevExpress.XtraEditors.ProgressBarControl progressBarControl1;
        private DevExpress.XtraEditors.LabelControl labelControl1;
        private DevExpress.XtraEditors.CheckEdit checkEdit1;
    }
}

