using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using VirCrip;
using System.Data.SqlClient;

namespace TesteWSSincBancos
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private int staticR = 0;

        private void Teste()
        {
            DateTime hora = new DateTime(2010, 3, 5).AddMinutes(-10).AddSeconds(-25).AddMilliseconds(2);
            DateTime horateto = hora.AddMinutes(20);
            while (hora < horateto)
            {
                hora = hora.AddSeconds(1);
                string Aberto = string.Format("{0:yyyyMMddHHmmss}{1:000}{2:00000000}<-*W*->", hora, 1, staticR++);
                byte[] x = VirCripWS.Crip(Aberto);
                string reaberto = VirCrip.VirCripWS.Uncrip(x);
                WSSincBancos.WSSincBancos WS = new TesteWSSincBancos.WSSincBancos.WSSincBancos();
                textBox1.Text += string.Format("\r\n{0}\r\n    ", reaberto, WS.Teste(x));
            }
        }

        private byte[] Criptografa(int EMP)
        {
            DateTime hora = DateTime.Now;
            string Aberto = string.Format("{0:yyyyMMddHHmmss}{1:000}{2:00000000}<-*W*->",hora,EMP,staticR++);            
            return VirCripWS.Crip(Aberto);
        }

        private void button1_Click(object sender, EventArgs e)
        {
            WSSincBancos.WSSincBancos WS = new TesteWSSincBancos.WSSincBancos.WSSincBancos();
            if (MessageBox.Show("Usar REAL", "CUIDADO", MessageBoxButtons.YesNo, MessageBoxIcon.Warning, MessageBoxDefaultButton.Button2) == DialogResult.Yes)
                WS.Url = @"http://177.190.193.218/Neon23/WSSincBancos.asmx";
                
            
            for (int i = 0; i <= 3; i++)
            {
                //string resultado = string.Format("Teste para {0}:\r\n{1}",i, WS.INPC(i, DateTime.Today, i*10, i).Replace("\n","\r\n"));
                string resultado = string.Format("Teste para {0}:\r\n{1}", i, WS.Teste(Criptografa(i)).Replace("\n", "\r\n"));                
                textBox1.Text += "\r\n"+resultado;
            }

            
        }

        private int i;
        

        private void button2_Click(object sender, EventArgs e)
        {
            /*
            WSSincBancos.WSSincBancos WS = new TesteWSSincBancos.WSSincBancos.WSSincBancos();
            if (MessageBox.Show("Usar REAL", "CUIDADO", MessageBoxButtons.YesNo, MessageBoxIcon.Warning, MessageBoxDefaultButton.Button2) == DialogResult.Yes)
                WS.Url = @"http://177.190.193.218/neononline2/WSSincBancos.asmx";
            i = -1;
            if (VirInput.Input.Execute("neonX", out i))
            {
                dBanco = WS.LeCondominios(Criptografa(i));
                if (dBanco != null)
                    condominiosBindingSource.DataSource = dBanco;
                else
                    textBox1.Text = WS.LeUltimoErro();
            }
            */
        }

        private void button3_Click(object sender, EventArgs e)
        {
            /*
            WSSincBancos.WSSincBancos WS = new TesteWSSincBancos.WSSincBancos.WSSincBancos();
            if (MessageBox.Show("Usar REAL", "CUIDADO", MessageBoxButtons.YesNo, MessageBoxIcon.Warning, MessageBoxDefaultButton.Button2) == DialogResult.Yes)
                WS.Url = @"http://177.190.193.218/neononline2/WSSincBancos.asmx";
            WS.GravaCondominios(Criptografa(i), dBanco);*/
        }

        private void button4_Click(object sender, EventArgs e)
        {
            /*
            WSSincBancos.WSSincBancos WS = new TesteWSSincBancos.WSSincBancos.WSSincBancos();
            if (MessageBox.Show("Usar REAL", "CUIDADO", MessageBoxButtons.YesNo, MessageBoxIcon.Warning, MessageBoxDefaultButton.Button2) == DialogResult.Yes)
                WS.Url = @"http://177.190.193.218/neononline2/WSSincBancos.asmx";
            i = -1;
            if (VirInput.Input.Execute("neonX", out i))
            {
                dBanco = WS.LeClientes(Criptografa(i));
                if (dBanco != null)
                {
                    condominiosBindingSource.DataSource = dBanco;
                }
                else
                    textBox1.Text = WS.LeUltimoErro();
            }
            */

           

        }

        enum TipoSQL 
        {
            select,
            update
        }

        

        private void ComandoSQLNeonX(TipoSQL Tipo)
        {            
            i = -1;
            int Banco = chBAL.Checked ? 2 : 1;
            if (VirInput.Input.Execute("neonX", out i))
            {
                string Comando;
                if (memoEdit1.SelectionLength > 0)
                    Comando = memoEdit1.SelectedText;
                else
                    Comando = memoEdit1.Text;
                VirDB.Bancovirtual.TiposDeBanco TBanco = i == 1 ? VirDB.Bancovirtual.TiposDeBanco.Internet1 : VirDB.Bancovirtual.TiposDeBanco.Internet2;

                if (Tipo == TipoSQL.update)
                {
                    spinEdit1.Value = VirMSSQL.TableAdapter.ST(TBanco).ExecutarSQLNonQuery(Comando);
    //                if (spinEdit1.Value == -1)
    //                    memoEdit1.Text += "\r\n" + WS.LeUltimoErro();
                }
                else
                {
                    DataTable DT = VirMSSQL.TableAdapter.ST(TBanco).BuscaSQLTabela(Comando);                    
                    if (DT == null)
                        memoEdit1.Text += "\r\nRetorno nulo";
                    else
                    {
                        
                            gridView3.Columns.Clear();
                            gridResultado.DataSource = DT;
                        
                    }
                }


            }
        }

        

        private void simpleButton2_Click(object sender, EventArgs e)
        {
            Teste();
        }

        

        private void simpleButton4_Click(object sender, EventArgs e)
        {
            ComandoSQLNeonX(TipoSQL.select);
        }

        private void simpleButton5_Click(object sender, EventArgs e)
        {
            ComandoSQLNeonX(TipoSQL.update);
        }

        private void simpleButton6_Click(object sender, EventArgs e)
        {
            if (checkEdit1.Checked)
                textEditDE.Text = @"Data Source=200.234.197.12;Initial Catalog=neonimoveis_2;Persist Security Info=True;User ID=neonimoveis_2;Password=formiga9999";
            /*
            string[] Tabelas = new string[]{"SaldoContaCorrente",
                                            "Protesto",
                                            "FORNECEDORES",
                                            "EMPRESAS",
                                            "DOCumentacao",
                                            "ContaCorrenteDetalhe",
                                            "ContaCorrenTe",
                                            "CONDOMINIOS",
                                            "CHEques",
                                            "BOLetos",
                                            "BOletoDetalhe",
                                            "BLOCOS",
                                            "APARTAMENTOS",
                                            "ACOxBOL",
                                            "ACOrdos"};
            */

            string[] Tabelas = new string[]{"BOLetos"};

            SqlConnection ConeOrigem = new System.Data.SqlClient.SqlConnection(textEditDE.Text);
            SqlConnection ConeDestino = new System.Data.SqlClient.SqlConnection(textEditPARA.Text);            
            SqlCommand ComandoTamanho = new SqlCommand("select count(*) from acordos",ConeOrigem);
            SqlCommand ComandoLe = new SqlCommand("select * from acordos",ConeOrigem);
            SqlCommand ComandoLimpa = new SqlCommand("", ConeDestino);
            SqlCommand ComandoGrava = new SqlCommand("insert into acordos values (@P0,@P1,@P2,@P3,@P4,@P5,@P6,@P7)", ConeDestino);
            SqlDataAdapter Leitor = new SqlDataAdapter(ComandoLe);
            ConeOrigem.Open();
            ConeDestino.Open();
            
            
            
            
            //object[] Valores = null;
            int ContaTabelas = 1;
            int Conta;
            foreach (string Tabela in Tabelas)
            {
                labelControl1.Text = string.Format("Tabela: {0} {1}/{2}", Tabela ,ContaTabelas++,Tabelas.Length);
                Application.DoEvents();
                ComandoTamanho.CommandText = string.Format("select count(*) from {0}",Tabela);
                ComandoLe.CommandText = string.Format("select * from {0}", Tabela);
                ComandoLimpa.CommandText = string.Format("delete from {0}", Tabela);
                Conta = 0;
                int Tamanho = (int)ComandoTamanho.ExecuteScalar();
                ComandoLimpa.ExecuteNonQuery();
                SqlDataReader DadosLidos = Leitor.SelectCommand.ExecuteReader();
                DateTime DataTela = DateTime.Now;
                while (DadosLidos.Read())
                {

                    if (Conta++ == 0)
                    {
                        string virgula = "";
                        string comando = string.Format("insert into {0} values (",Tabela);
                        for (int i = 0; i < DadosLidos.FieldCount; i++)
                        {
                            comando += string.Format("{0}@P{1}", virgula, i);
                            virgula = ",";
                        }
                        //Valores = new object[DadosLidos.FieldCount];
                        comando += ")";
                        ComandoGrava.CommandText = comando;
                        progressBarControl1.Position = 0;
                        progressBarControl1.Properties.Maximum = Tamanho;
                    };

                    if (DataTela < DateTime.Now)
                    {
                        progressBarControl1.Position = Conta;
                        Application.DoEvents();
                        DataTela = DateTime.Now.AddSeconds(3);
                    }
                    //DadosLidos.GetSqlValues(Valores);
                    ComandoGrava.Parameters.Clear();
                    for (int i = 0; i < DadosLidos.FieldCount; i++)
                        ComandoGrava.Parameters.AddWithValue(string.Format("P{0}", i), DadosLidos[i]);
                    ComandoGrava.ExecuteNonQuery();
                };
                DadosLidos.Close();
            }
            ConeOrigem.Close();
            ConeDestino.Close();
            MessageBox.Show("ok");
        }
    }
}