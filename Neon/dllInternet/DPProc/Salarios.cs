﻿using System;
using System.Collections.Generic;
using VirEnumeracoesNeon;
using FrameworkProc;
using ContaPagarProc;
using Framework.objetosNeon;

namespace DPProc
{
    /// <summary>
    /// Salários
    /// </summary>
    public class Salarios
    {                
        private TipoPagPeriodico? TipoPagPeriodico1(TipoBancaria tipoBancaria)
        {            
                switch (tipoBancaria)
                {
                    //case TipoBancaria.Adiantamento:
                    //return TipoPagPeriodico.Adiantamento;                        
                    case TipoBancaria.Pagamento:                        
                        return TipoPagPeriodico.Folha;
                    case TipoBancaria.DecimoTerceiro:                        
                        return null;
                    case TipoBancaria.Avulso:                        
                        return null;
                    case TipoBancaria.Adiantamento:                        
                        return TipoPagPeriodico.Adiantamento;
                    default:
                        throw new NotImplementedException(string.Format("Não tratado DPProc.salarios.cs 70: {0}", tipoBancaria));
                }            
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="EMPTProc1"></param>
        /// <param name="RHT"></param>
        /// <returns></returns>
        public bool Grava(EMPTProc EMPTProc1, int RHT)
        {            
            dSalarios dSalarios1 = new dSalarios(EMPTProc1);
            if (dSalarios1.RHTarefaTableAdapter.FillByRHT(dSalarios1.RHTarefa, RHT) == 0)
                return false;
            dSalarios1.RHTarefaDetalheTableAdapter.FillByRHT(dSalarios1.RHTarefaDetalhe, RHT);
            dSalarios1.RHTarefaSubdetalheTableAdapter.FillByRHT(dSalarios1.RHTarefaSubdetalhe, RHT);
            dSalarios.RHTarefaRow RHTrow = dSalarios1.RHTarefa[0];
            Competencia Comp = new Competencia(RHTrow.RHTCompetencia);
            dSalarios.RHTarefaDetalheRow[] RHDrows = RHTrow.GetRHTarefaDetalheRows();
            int? SPL = EMPTProc1.STTA.BuscaEscalar_int("SELECT TOP 1 SPL FROM SubPLano WHERE (SPL_PLA = @P1)", RHTrow.RHT_PLA);
            TipoPagPeriodico? TipoPagPeriodicoL = TipoPagPeriodico1((TipoBancaria)RHTrow.RHTTipo);
            PagamentoPeriodicoProc PagamentoPeriodico;
            foreach (dSalarios.RHTarefaDetalheRow RHDrow in RHDrows)
            {
                if (TipoPagPeriodicoL.HasValue)
                    PagamentoPeriodico = new PagamentoPeriodicoProc(RHDrow.RHD_CON, TipoPagPeriodicoL.Value, EMPTProc1, !VirMSSQL.TableAdapter.Servidor_De_Processos);
                else
                    PagamentoPeriodico = null;
                try
                {
                    EMPTProc1.AbreTrasacaoSQL("Salarios.cs Grava 15", dSalarios1.RHTarefaDetalheTableAdapter, dSalarios1.RHTarefaSubdetalheTableAdapter);
                    NotaProc Nota1;
                    if (PagamentoPeriodico != null)
                    {
                        Nota1 = PagamentoPeriodico.BuscaCriaNotaProc(Comp);
                    }
                    else
                    {
                        PagamentoPeriodico = null;
                        Nota1 = new NotaProc(RHDrow.RHD_CON,
                                                    null,
                                                    0,
                                                    DateTime.Now,
                                                    0,
                                                    NOATipo.Folha,
                                                    RHTrow.RHT_PLA,
                                                    SPL.Value,
                                                    RHTrow.PLADescricao,
                                                    Comp,
                                                    "",
                                                    "",
                                                    PAGTipo.cheque,//este é o default da nota 
                                                    NOAStatus.SemPagamentos,
                                                    null,
                                                    null,
                                                    null,
                                                    EMPTProc1);
                    }
                    List<PreCheque> Dados = new List<PreCheque>();
                    SortedList<int, PreCheque> recuperar = new SortedList<int, PreCheque>();
                    foreach (dSalarios.RHTarefaSubdetalheRow RHSrow in RHDrow.GetRHTarefaSubdetalheRows())
                    {
                        if (!RHSrow.IsRHS_CHENull())
                            continue;
                        nTipoPag TipoPag = (nTipoPag)RHSrow.RHSTipoPag;
                        if ((TipoPag == nTipoPag.NaoEmitir) || (RHSrow.RHSValor <= 0))
                            continue;
                        PreCheque Pre = (new PreCheque()
                        {
                            Nominal = RHSrow.FUCNome,
                            Valor = RHSrow.RHSValor,
                            DataVencimento = TipoPag == nTipoPag.Cheque ? RHTrow.RHTDataCheque : RHTrow.RHTDataCredito,
                            Tipo = TipoPag == nTipoPag.Cheque ? PAGTipo.sindico : PAGTipo.Folha
                        });
                        switch (TipoPag)
                        {
                            case nTipoPag.Cheque:
                                Pre.Tipo = PAGTipo.sindico;
                                break;
                            case nTipoPag.CreditoConta:
                                Pre.Tipo = PAGTipo.Folha;
                                break;
                            case nTipoPag.ChequeEletronico:
                                Pre.Tipo = PAGTipo.PECreditoConta;
                                Abstratos.ABS_ContaCorrente CCGF = Abstratos.ABS_ContaCorrente.GetContaCorrenteNeon(0,VirEnumeracoes.TipoChaveContaCorrenteNeon.CCG,false);
                                Pre.CCG = RHSrow.FUC_CCG;
                                break;
                        }
                        Dados.Add(Pre);
                        recuperar.Add(RHSrow.RHS, Pre);
                    }                    
                    RHDrow.RHD_NOA = Nota1.DestravaCadastrar(DateTime.Now, Dados);
                    foreach (dSalarios.RHTarefaSubdetalheRow RHSrow in RHDrow.GetRHTarefaSubdetalheRows())
                    {
                        if (recuperar.ContainsKey(RHSrow.RHS))
                        {
                            RHSrow.RHS_CHE = recuperar[RHSrow.RHS].CHE;
                            dSalarios1.RHTarefaSubdetalheTableAdapter.Update(RHSrow);
                        }
                    }

                    if (PagamentoPeriodico != null)                                            
                        PagamentoPeriodico.CadastrarProximo();
                    

                    
                    dSalarios1.RHTarefaDetalheTableAdapter.Update(RHDrow);                    
                    EMPTProc1.Commit();
                    //i++;
                    //Erros = 0;
                }
                catch (Exception e)
                {                    
                    EMPTProc1.Vircatch(e);
                    int? CONFolha = EMPTProc1.STTA.BuscaEscalar_int("select CONCodigoFolha1 from CONDOMINIOS where con = @P1",RHDrow.RHD_CON);
                    throw new Exception(string.Format("Erro no cadastro dos pagamentos.\r\nErro:{0}\r\nCodigo Folha:{1}", e.Message,CONFolha.GetValueOrDefault(0)), e);
                }
            }
            return true;
        }
    }

    /// <summary>
    /// Melhorar
    /// </summary>
    public enum nTipoPag
    {
        /// <summary>
        /// 
        /// </summary>
        Invalido = 0,
        /// <summary>
        /// 
        /// </summary>
        Cheque = 1,
        /// <summary>
        /// 
        /// </summary>
        CreditoConta = 2,
        /*
        /// <summary>
        /// 
        /// </summary>
        Carta = 3,*/
        /// <summary>
        /// 
        /// </summary>
        NaoEmitir = 4,
        /// <summary>
        /// pagamento com cheque eletrônico
        /// </summary>
        ChequeEletronico = 5
    }

    /// <summary>
    /// Enumeração
    /// </summary>
    public enum TipoBancaria
    {
        /// <summary>
        /// 
        /// </summary>
        Pagamento,
        /// <summary>
        /// 
        /// </summary>
        Adiantamento,
        /// <summary>
        /// 
        /// </summary>
        DecimoTerceiro,
        /// <summary>
        /// 
        /// </summary>
        Avulso
    }
}
