﻿using System;
using FrameworkProc;

namespace DPProc
{


    public partial class dSalarios : IEMPTProc
    {
        private EMPTProc _EMPTProc1;

        /// <summary>
        /// Tipo de EMP: local ou filial
        /// </summary>
        public EMPTProc EMPTProc1
        {
            get
            {
                if (_EMPTProc1 == null)
                    _EMPTProc1 = new EMPTProc(FrameworkProc.EMPTipo.Local);
                return _EMPTProc1;
            }
            set
            {
                _EMPTProc1 = value;
            }
        }

        /// <summary>
        /// Construitor com EMPTipo
        /// </summary>
        /// <param name="_EMPTProc1"></param>
        public dSalarios(EMPTProc _EMPTProc1)
            : this()
        {
            if (_EMPTProc1 == null)
            {
                if (VirMSSQL.TableAdapter.Servidor_De_Processos)
                    throw new Exception("uso de construtor sem EMPTProc em Servidor de Processos");
                EMPTProc1 = new EMPTProc(EMPTipo.Local);
            }
            EMPTProc1 = _EMPTProc1;
        }

        private dSalariosTableAdapters.RHTarefaTableAdapter rHTarefaTableAdapter;
        /// <summary>
        /// TableAdapter padrão para tabela: RHTarefa
        /// </summary>
        public dSalariosTableAdapters.RHTarefaTableAdapter RHTarefaTableAdapter
        {
            get
            {
                if (rHTarefaTableAdapter == null)
                {
                    rHTarefaTableAdapter = new dSalariosTableAdapters.RHTarefaTableAdapter();
                    rHTarefaTableAdapter.TrocarStringDeConexao(EMPTProc1.TBanco);
                };
                return rHTarefaTableAdapter;
            }
        }

        private dSalariosTableAdapters.RHTarefaDetalheTableAdapter rHTarefaDetalheTableAdapter;
        /// <summary>
        /// TableAdapter padrão para tabela: RHTarefaDetalhe
        /// </summary>
        public dSalariosTableAdapters.RHTarefaDetalheTableAdapter RHTarefaDetalheTableAdapter
        {
            get
            {
                if (rHTarefaDetalheTableAdapter == null)
                {
                    rHTarefaDetalheTableAdapter = new dSalariosTableAdapters.RHTarefaDetalheTableAdapter();
                    rHTarefaDetalheTableAdapter.TrocarStringDeConexao(EMPTProc1.TBanco);
                };
                return rHTarefaDetalheTableAdapter;
            }
        }

        private dSalariosTableAdapters.RHTarefaSubdetalheTableAdapter rHTarefaSubdetalheTableAdapter;
        /// <summary>
        /// TableAdapter padrão para tabela: RHTarefaSubdetalhe
        /// </summary>
        public dSalariosTableAdapters.RHTarefaSubdetalheTableAdapter RHTarefaSubdetalheTableAdapter
        {
            get
            {
                if (rHTarefaSubdetalheTableAdapter == null)
                {
                    rHTarefaSubdetalheTableAdapter = new dSalariosTableAdapters.RHTarefaSubdetalheTableAdapter();
                    rHTarefaSubdetalheTableAdapter.TrocarStringDeConexao(EMPTProc1.TBanco);
                };
                return rHTarefaSubdetalheTableAdapter;
            }
        }
    }
}
