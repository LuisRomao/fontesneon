﻿namespace dllMalotes
{

    partial class dMalotes
    {
        private dMalotesTableAdapters.BALancetesTableAdapter bALancetesTableAdapter;
        /// <summary>
        /// TableAdapter padrão para tabela: BALancetes
        /// </summary>
        public dMalotesTableAdapters.BALancetesTableAdapter BALancetesTableAdapter
        {
            get
            {
                if (bALancetesTableAdapter == null)
                {
                    bALancetesTableAdapter = new dMalotesTableAdapters.BALancetesTableAdapter();
                    bALancetesTableAdapter.TrocarStringDeConexao();
                };
                return bALancetesTableAdapter;
            }
        }
    }
}
