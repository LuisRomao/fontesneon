﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using CompontesBasicos;
using VirEnumeracoesNeon;

namespace dllMalotes
{
    /// <summary>
    /// Classe para registo de entrada e saída de malotes
    /// </summary>
    public partial class cMalote : ComponenteBase
    {
        /// <summary>
        /// Construtor
        /// </summary>
        public cMalote()
        {
            InitializeComponent();
            DMalotes = new dMalotes();
        }

        private dMalotes DMalotes;

        private void textEdit1_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                string Original = textEdit1.Text;
                try
                {
                    if (Original.Length > 4)
                    {
                        string Limpo = (Original[0] == '0') ? Original.Substring(1, Original.Length - 2) : Original.Substring(0, Original.Length - 1);
                        int EMP = int.Parse(Limpo.Substring(0, 1));
                        TipoCodigoBarras Tipo = (TipoCodigoBarras)int.Parse(Limpo.Substring(1, 2));
                        switch (Tipo)
                        {
                            case TipoCodigoBarras.Balancete:
                                int BAL = int.Parse(Limpo.Substring(3));
                                if (DMalotes.BALancetesTableAdapter.FillByBAL(DMalotes.BALancetes, BAL) == 1)
                                {
                                    dMalotes.BALancetesRow row = DMalotes.BALancetes[0];
                                    BALStatus Status = (BALStatus)row.BALSTATUS;
                                    Framework.objetosNeon.Competencia comp = new Framework.objetosNeon.Competencia(row.BALCompet);
                                    string Titulo = string.Format("{0} - {1} - ({2})", row.CONCodigo, row.CONNome, comp);
                                    switch (Status)
                                    {
                                        case BALStatus.Producao:
                                        case BALStatus.Termindo:
                                        case BALStatus.ParaPublicar:
                                        case BALStatus.Publicado:
                                            bool Confimado = false;
                                            string Mensagem = string.Format(string.Format("O balancete {0} consta no sistema como não estando pronto!!\r\nVerificar com o balancete.", Titulo));
                                            while (!Confimado)
                                                Confimado = MessageBox.Show(Mensagem, "A T E N Ç Ã O", MessageBoxButtons.OKCancel, MessageBoxIcon.Error, MessageBoxDefaultButton.Button2) == DialogResult.OK;
                                            break;
                                        case BALStatus.FechadoDefinitivo:
                                            row.BALSTATUS = (int)BALStatus.Enviado;
                                            row.BALObs = string.Format("{0}{1:dd/MM/yyyy HH:mm} ({2})\r\n    Enviado\r\n\r\n",
                                                                       row.IsBALObsNull() ? "" : row.BALObs + Environment.NewLine,
                                                                       DateTime.Now,
                                                                       Framework.usuarioLogado.UsuarioLogadoNeon.UsuarioLogadoStNEON.USUNome);
                                            MessageBox.Show("OK - " + Titulo, "ok");
                                            DMalotes.BALancetesTableAdapter.Update(row);
                                            break;
                                    }
                                }
                                else
                                    throw new Exception(string.Format("Erro: Balancete não encontrado!\r\nCódigo de barras: <{0}>", Original));
                                break;
                        }


                    }
                }
                catch
                {
                }
                finally
                {
                    textEdit1.Text = string.Empty;
                    e.Handled = true;
                }                
            }
        }

        private void textEdit1_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!char.IsDigit(e.KeyChar) && (e.KeyChar != '\b'))
                e.Handled = true;
        }
    }
}
