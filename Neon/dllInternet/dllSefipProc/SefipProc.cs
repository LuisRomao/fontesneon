﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using FrameworkProc;

namespace dllSefipProc
{
    /// <summary>
    /// Calculos SEFIP
    /// </summary>
    public class SefipProc 
    {
        private EMPTProc _EMPTProcL;
        private EMPTProc _EMPTProcF;

        /// <summary>
        /// Tipo de EMP: local
        /// </summary>
        public EMPTProc EMPTProcL
        {
            get
            {
                if (_EMPTProcL == null)
                    _EMPTProcL = new EMPTProc(FrameworkProc.EMPTipo.Local);
                return _EMPTProcL;
            }
            set
            {
                _EMPTProcL = value;
            }
        }
        
        /// <summary>
        /// Tipo de EMP: filial
        /// </summary>
        public EMPTProc EMPTProcF
        {
            get
            {
                if (_EMPTProcF == null)
                    _EMPTProcF= new EMPTProc(FrameworkProc.EMPTipo.Filial);
                return _EMPTProcF;
            }
            set
            {
                _EMPTProcF = value;
            }
        }

        /// <summary>
        /// Construtor padrão chamado pelo herdeiro Sefip
        /// </summary>
        public SefipProc()
        { 
        }

        /// <summary>
        /// Construtor chamado pelo servidor de processos
        /// </summary>
        /// <param name="EMP"></param>
        /// <param name="USULocalChamandor"></param>
        public SefipProc(int EMP,int USULocalChamandor)
        {
            if (VirMSSQL.TableAdapter.Servidor_De_Processos)
            {
                string comando = "SELECT USU__USUfilial FROM USUARIOS WHERE USU = @P1";
                if (EMP == 1)
                {
                    EMPTProcL = new EMPTProc(EMPTipo.Local, USULocalChamandor);
                    int? USUF = EMPTProcL.STTA.BuscaEscalar_int(comando,USULocalChamandor);
                    if (USUF.HasValue)
                        EMPTProcF = new EMPTProc(EMPTipo.Filial, USUF.Value);
                }
                else
                {                    
                    EMPTProcF = new EMPTProc(EMPTipo.Filial, USULocalChamandor);
                    int? USUL = EMPTProcF.STTA.BuscaEscalar_int(comando, USULocalChamandor);
                    if (USUL.HasValue)
                        EMPTProcL = new EMPTProc(EMPTipo.Local, USUL.Value);
                }
            }
            else
            {
                EMPTProcL = new EMPTProc(EMPTipo.Local, Framework.DSCentral.USU);
                EMPTProcF = new EMPTProc(EMPTipo.Filial, Framework.DSCentral.USUFilial);
            }
            
        }

        /// <summary>
        /// Grava os dados do dataset - usado no servidor de processos
        /// </summary>
        /// <param name="DSefip"></param>
        /// <param name="Comp"></param>
        /// <returns></returns>
        public bool gravanobancoProc(dSefip DSefip, int Comp)
        {                        
            DSefip.PAGamentos.Clear();
            DSefip.SEFip.Clear();
            DSefip.SEFipTableAdapter.FillByCompet(DSefip.SEFip, Comp);
            DSefip.SEFipTableAdapterF.FillByCompet(DSefip.SEFip, Comp);
            //try
            //{
            //VirMSSQL.TableAdapter.AbreTrasacaoSQLDupla("dllSefip Sefip - 381");
            //VirMSSQL.TableAdapter.AbreTrasacaoSQL("dllSefip Sefip - 381", DSefip.SEFipTableAdapter, DSefip.INSSPagoTableAdapter);
            //DSefip.SEFipTableAdapter.EmbarcaEmTransST();
            //DSefip.INSSPagoTableAdapter.EmbarcaEmTransST();
            //DSefip.SEFipTableAdapterF.EmbarcaEmTransST();
            //DSefip.INSSPagoTableAdapterF.EmbarcaEmTransST();
            System.Collections.ArrayList JaCadastrado = new System.Collections.ArrayList();            
            int i = 0;            
            foreach (dSefip.SefipAcRow rowsefip in DSefip.SefipAc)
            {
                i++;
                dSefip.SEFipRow rowSEFIP = DSefip.BuscaSEFipRow(rowsefip.CON, rowsefip.EMP);
                dSefip.CONDOMINIOSRow rowCon = DSefip.CONDOMINIOS.FindByCONCON_EMP(rowsefip.CON, rowsefip.EMP);
                FrameworkProc.EMPTProc EMPTProcX = rowsefip.EMP == EMPTProc.EMPLocal ? EMPTProcL : EMPTProcF;
                try
                {
                    if (rowsefip.EMP == EMPTProc.EMPLocal)
                        EMPTProcX.AbreTrasacaoSQL("dllSefip Sefip - 381", DSefip.SEFipTableAdapter, DSefip.INSSPagoTableAdapter);
                    else
                        EMPTProcX.AbreTrasacaoSQL("dllSefip Sefip - 381", DSefip.SEFipTableAdapterF, DSefip.INSSPagoTableAdapterF);
                    if (rowSEFIP == null)
                    {
                        rowSEFIP = DSefip.SEFip.NewSEFipRow();
                        rowSEFIP.SEF_CON = rowsefip.CON;
                        rowSEFIP.CONCnpj = rowCon.CONCnpj;
                        rowSEFIP.calNomeCondominio = string.Format("{0}{1}", rowCon.IsCONCodigoFolha1Null() ? "" : rowCon.CONCodigoFolha1 + " ", rowCon.CONNome);
                        if (!rowCon.IsCONCodigoFolha1Null())
                            rowSEFIP.SEFCodFolha = rowCon.CONCodigoFolha1;
                        rowSEFIP.SEF_EMP = rowsefip.EMP;
                        if (!rowCon.IsCONCodigoComunicacaoTributosNull())
                            rowSEFIP.SEFCodComunicacao = rowCon.CONCodigoComunicacaoTributos;
                        rowSEFIP.SEFCompetencia = Comp;
                        rowSEFIP.SEFTipoPag = (int)nTipoPag.Invalido;
                        //rowSEFIP.con
                        //*** MRC - INICIO - TRIBUTOS (29/01/2015 12:00) ***
                        if ((rowCon.CONStatus == 1) && (!rowCon.IsCON_BCONull()))
                        {
                            if ((!rowCon.IsCONGPSEletronicoNull() && rowCon.CONGPSEletronico))
                            {
                                if (rowCon.CON_BCO == 341)
                                    rowSEFIP.SEFTipoPag = (int)nTipoPag.EletronicoItau;
                                else if ((rowCon.CON_BCO == 237) && (!rowCon.IsCONCodigoComunicacaoTributosNull()))
                                    rowSEFIP.SEFTipoPag = (int)nTipoPag.EletronicoBradesco;
                                else
                                    if (rowCon.CON_EMP == EMPTProc.EMPLocal)
                                        rowSEFIP.SEFTipoPag = (int)nTipoPag.Cheque;
                                    else
                                        rowSEFIP.SEFTipoPag = (int)nTipoPag.ChequeFilial;
                            }
                            else
                                //if (rowCon.CON_EMP == Framework.DSCentral.EMP)
                                rowSEFIP.SEFTipoPag = (int)nTipoPag.Cheque;
                            //else
                            //    rowSEFIP.SEFTipoPag = (int)nTipoPag.ChequeFilial;
                        }
                        DSefip.SEFip.AddSEFipRow(rowSEFIP);

                    }
                    rowsefip.ValorTerceiros = Math.Round(rowsefip.ValorTerceiros, 2);
                    if (JaCadastrado.Contains(rowSEFIP))
                        rowSEFIP.SEFValorNeon += rowsefip.ValorTerceiros;
                    else
                    {
                        rowSEFIP.SEFValorNeon = rowsefip.ValorTerceiros;
                        JaCadastrado.Add(rowSEFIP);
                    };
                    if (rowSEFIP.SEF_EMP == EMPTProc.EMPLocal)
                        DSefip.SEFipTableAdapter.Update(rowSEFIP);
                    else
                        DSefip.SEFipTableAdapterF.Update(rowSEFIP);                    
                    foreach (dSefip.INSSPagoRow rowINSSPago in rowsefip.GetINSSPagoRows())
                    {
                        rowINSSPago.PAG_SEF = rowSEFIP.SEF;
                        if (rowINSSPago.CON_EMP == EMPTProc.EMPLocal)
                            DSefip.INSSPagoTableAdapter.UpdateQuerySetSEF(rowSEFIP.SEF, rowINSSPago.PAG);
                        else
                            DSefip.INSSPagoTableAdapterF.UpdateQuerySetSEF(rowSEFIP.SEF, rowINSSPago.PAG);
                    };
                    EMPTProcX.Commit();                    
                }
                catch (Exception e)
                {
                    EMPTProcX.Vircatch(e);                    
                    throw new Exception(e.Message, e);                    
                }                           }
            return true;
            //DSefip.PAGamentos.Clear();
            //DSefip.PAGamentosTableAdapter.FillByCompet(DSefip.PAGamentos, Comp);
            //DSefip.PAGamentosTableAdapterF.FillByCompet(DSefip.PAGamentos, Comp);
        }
        
    }
}
