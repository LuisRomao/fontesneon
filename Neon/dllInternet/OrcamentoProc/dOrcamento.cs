﻿namespace OrcamentoProc
{


    partial class dOrcamento
    {
        private dOrcamentoTableAdapters.ORCamentoTableAdapter oRCamentoTableAdapter;
        /// <summary>
        /// TableAdapter padrão para tabela: ORCamento
        /// </summary>
        public dOrcamentoTableAdapters.ORCamentoTableAdapter ORCamentoTableAdapter
        {
            get
            {
                if (oRCamentoTableAdapter == null)
                {
                    oRCamentoTableAdapter = new dOrcamentoTableAdapters.ORCamentoTableAdapter();
                    oRCamentoTableAdapter.TrocarStringDeConexao();
                };
                return oRCamentoTableAdapter;
            }
        }

        private dOrcamentoTableAdapters.ORcamentoGrupoTableAdapter oRcamentoGrupoTableAdapter;
        /// <summary>
        /// TableAdapter padrão para tabela: ORcamentoGrupo
        /// </summary>
        public dOrcamentoTableAdapters.ORcamentoGrupoTableAdapter ORcamentoGrupoTableAdapter
        {
            get
            {
                if (oRcamentoGrupoTableAdapter == null)
                {
                    oRcamentoGrupoTableAdapter = new dOrcamentoTableAdapters.ORcamentoGrupoTableAdapter();
                    oRcamentoGrupoTableAdapter.TrocarStringDeConexao();
                };
                return oRcamentoGrupoTableAdapter;
            }
        }

        private dOrcamentoTableAdapters.ORcamentoDetalheTableAdapter oRcamentoDetalheTableAdapter;
        /// <summary>
        /// TableAdapter padrão para tabela: ORcamentoDetalhe
        /// </summary>
        public dOrcamentoTableAdapters.ORcamentoDetalheTableAdapter ORcamentoDetalheTableAdapter
        {
            get
            {
                if (oRcamentoDetalheTableAdapter == null)
                {
                    oRcamentoDetalheTableAdapter = new dOrcamentoTableAdapters.ORcamentoDetalheTableAdapter();
                    oRcamentoDetalheTableAdapter.TrocarStringDeConexao();
                };
                return oRcamentoDetalheTableAdapter;
            }
        }

        private dOrcamentoTableAdapters.PLAnocontasTableAdapter pLAnocontasTableAdapter;
        /// <summary>
        /// TableAdapter padrão para tabela: PLAnocontas
        /// </summary>
        public dOrcamentoTableAdapters.PLAnocontasTableAdapter PLAnocontasTableAdapter
        {
            get
            {
                if (pLAnocontasTableAdapter == null)
                {
                    pLAnocontasTableAdapter = new dOrcamentoTableAdapters.PLAnocontasTableAdapter();
                    pLAnocontasTableAdapter.TrocarStringDeConexao();
                };
                return pLAnocontasTableAdapter;
            }
        }
    }
}
