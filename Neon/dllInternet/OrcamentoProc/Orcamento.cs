﻿using System;
using System.Collections.Generic;
using Framework.objetosNeon;
using System.Runtime.Serialization;
using System.IO;
using AbstratosNeon;
using VirEnumeracoesNeon;
using Abstratos;

namespace OrcamentoProc
{
    [DataContract(Name = "Orcamento", Namespace = "OrcamentoProc")]
    public class Orcamento : ABS_Orcamento
    {
        [DataMember]
        public int ORC
        {
            get => real ? rowORC.ORC : 0;
            set { }
        }

        private dOrcamento.ORCamentoRow rowORC;

        /// <summary>
        /// Indica se é um orçamento real
        /// </summary>
        public override bool real { get => rowORC != null; }

        /// <summary>
        /// Status do orçamento
        /// </summary>
        public override ORCStatus Status
        {
            get => real ? (ORCStatus)rowORC.ORCStatus : ORCStatus.Ativo;
            set => rowORC.ORCStatus = (int)value;
        }

        public override ABS_Competencia CompI
        {
            get => _CompI ?? (real ? _CompI = new Competencia(rowORC.ORCCompI, rowORC.ORC_CON,null) : null);
            set
            {
                _CompI = (Competencia)value;
                if (real)
                    rowORC.ORCCompI = _CompI.CompetenciaBind;
            }
        }

        public override ABS_Competencia CompF
        {
            get => _CompF ?? (real ? _CompF = new Competencia(rowORC.ORCCompF, rowORC.ORC_CON,null) : null);
            set
            {
                _CompF = (Competencia)value;
                if (real)
                    rowORC.ORCCompF = _CompF.CompetenciaBind;
            }
        }

        public override ABS_Condominio Condominio
        {
            get => condominio ?? (real ? (condominio = ABS_Condominio.GetCondominio(rowORC.ORC_CON)) : null);
            set
            {
                condominio = value;
                if (real)
                    rowORC.ORC_CON = condominio.CON;
            }
        }

        [DataMember]
        private int? CON
        {
            get => Condominio == null ? (int?)null : Condominio.CON;
            set
            {
                if (value.HasValue)
                    Condominio = ABS_Condominio.GetCondominio(value.Value);
            }
        }

        [DataMember]
        private Competencia _CompI;
        [DataMember]
        private Competencia _CompF;        

        private ABS_Condominio condominio;
        

        [DataMember]
        public string NomeArquivo;

        
        ////////////////////////
        

        //private int _ORC;
        [DataMember]
        private decimal? _Valor;
        [DataMember]
        private string _Titulo;
        private dOrcGrade _dOrcGrade;
        private dOrcamento _dOrcamento;
        private SortedList<string, decimal> _PLAsUsados;

        public SortedList<string,decimal> PLAsUsados
        {
            get
            {
                if (_PLAsUsados == null)
                {
                    _PLAsUsados = new SortedList<string, decimal>();
                    foreach (OrcGrupo Grupo in Grupos.Values)
                        foreach (SubOrcGrupo SubGrupo in Grupo.SubGrupos.Values)
                            foreach (DetalheGrupo Detalhe in SubGrupo.Detalhes.Values)
                                _PLAsUsados.Add(Detalhe.PLA,Detalhe.Valor);
                }
                return _PLAsUsados;
            }
        }

        public dOrcGrade dOrcGrade { get => _dOrcGrade ?? (_dOrcGrade = new dOrcGrade()); }

        public dOrcamento DOrcamento { get => _dOrcamento ?? (_dOrcamento = new dOrcamento()); }

        

        
        //public bool Simulador = false;
        
        internal int ORGSimulador = 0;
        
        //public int ORDSimulador = 0;

        public NivelDetalhe NDetalhe;

        
        

        

        [DataMember]
        public SortedList<int, OrcGrupo> Grupos;

        public Orcamento(int _ORC):this()
        {
            DOrcamento.ORCamentoTableAdapter.FillByORC(DOrcamento.ORCamento,_ORC);
            rowORC = DOrcamento.ORCamento[0];
            DOrcamento.ORcamentoGrupoTableAdapter.FillByORC(DOrcamento.ORcamentoGrupo, _ORC);
            DOrcamento.ORcamentoDetalheTableAdapter.FillByORC(DOrcamento.ORcamentoDetalhe, _ORC);
            foreach (dOrcamento.ORcamentoGrupoRow rowORG1 in rowORC.GetORcamentoGrupoRows())
            {
                if (rowORG1.ORGNivel != 1)
                    continue;                
                OrcGrupo NovoGrupo = new OrcGrupo(this, rowORG1);
                Grupos.Add(NovoGrupo.ORG, NovoGrupo);
                foreach (dOrcamento.ORcamentoGrupoRow rowORG2 in rowORG1.GetORcamentoGrupoRows())
                {
                    if (rowORG2.ORGNivel != 2)
                        continue;
                    SubOrcGrupo NovoSGrupo = new SubOrcGrupo(NovoGrupo, rowORG2);
                    NovoGrupo.SubGrupos.Add(NovoSGrupo.ORSG, NovoSGrupo);
                    foreach (dOrcamento.ORcamentoDetalheRow rowORD in rowORG2.GetORcamentoDetalheRows())
                    {                        
                        DetalheGrupo NovoDetalhe = new DetalheGrupo(NovoSGrupo,rowORD);
                        NovoSGrupo.Detalhes.Add(NovoDetalhe.ORD, NovoDetalhe);
                    }
                }
            }
            AjusteDeValoresTela(NivelDetalhe.Aberto);
            //DOrcamento.ORcamentoDetalheTableAdapter.FillByORC(DOrcamento.ORcamentoDetalhe, _ORC);
            //DOrcamento.PLAnocontasTableAdapter.Fill(DOrcamento.PLAnocontas);
        }

        public Orcamento()
        {            
            Grupos = new SortedList<int, OrcGrupo>();
        }

        public decimal Valor
        {
            get
            {
                if (_Valor.HasValue)
                    return _Valor.Value;
                else
                {
                    decimal Acumulado = 0;
                    foreach (OrcGrupo Grupo in Grupos.Values)
                        Acumulado += Grupo.ValorEfetivo;
                    return Acumulado;
                }
            }
            set => _Valor = value;
        }

        public int CriaNovoOrcamento(int _CON,Competencia _CompI, Competencia _CompF)
        {
            rowORC = DOrcamento.ORCamento.NewORCamentoRow();
            CON = _CON;
            CompI = _CompI;
            CompF = _CompF;            
            //rowORC.ORC_CON = _CON;
            //rowORC.ORCCompI = CompI.CompetenciaBind;
            //rowORC.ORCCompF = CompF.CompetenciaBind;
            rowORC.ORCStatus = (int)ORCStatus.Cadastro;
            DOrcamento.ORCamento.AddORCamentoRow(rowORC);
            DOrcamento.ORCamentoTableAdapter.Update(rowORC);
            //_ORC = rowORC.ORC;
            return rowORC.ORC;
        }

        public void AjusteDeValoresTela(NivelDetalhe NivelD)
        {            
            foreach (OrcGrupo Grupo in Grupos.Values)
                Grupo.AjusteDeValoresTela(NivelD);            
        }

        public void Limpa()
        {
            dOrcGrade.GradeOrcamento.Clear();            
            foreach (OrcGrupo Grupo in Grupos.Values)
            {
                foreach (SubOrcGrupo SGrupo in Grupo.SubGrupos.Values)
                    SGrupo.Detalhes.Clear();
                Grupo.SubGrupos.Clear();
            }
            Grupos.Clear();
            PLAsUsados.Clear();
        }

        /*
        public dOrcGrade Popula()
        {
            dOrcGrade.GradeOrcamento.Clear();
            int ordem = 1;
            foreach (OrcGrupo Grupo in Grupos.Values)
                foreach(SubOrcGrupo SGrupo in Grupo.SubGrupos.Values)
                    foreach(DetalheGrupo Detalhe in SGrupo.Detalhes.Values)
                    {
                        dOrcGrade.GradeOrcamentoRow novaRow = dOrcGrade.GradeOrcamento.NewGradeOrcamentoRow();
                        novaRow.Ordem = ordem++;
                        novaRow.ORG1 = Grupo.ORG;
                        novaRow.ORG2 = SGrupo.ORSG;
                        novaRow.ORD = Detalhe.ORD;
                        novaRow.TituloG = Grupo.Titulo;
                        novaRow.ValorG = Grupo.Valor;
                        novaRow.TituloSG = SGrupo.Titulo;
                        novaRow.ValorSG = SGrupo.Valor;
                        novaRow.TituloD = Detalhe.Titulo;
                        novaRow.ValorD = Detalhe.Valor;
                        novaRow.PLA = Detalhe.PLA;
                        novaRow.PLADescricao = Detalhe.Titulo;
                        dOrcGrade.GradeOrcamento.AddGradeOrcamentoRow(novaRow);
                    }            
            return dOrcGrade;
        }

        public dOrcGrade Simula(int nG, int nSG , int nD, bool RepeteDescricao)
        {
            decimal ValorG = 0;
            decimal ValorS = 0;
            decimal ValorD = 0;
            int OrdemG = 0;
            int OrdemS = 0;
            int OrdemD = 0;
            for (int iG = 1; iG <= nG; iG++)
            {
                if (!RepeteDescricao)
                {
                    ValorG += 100;
                    OrdemG ++;
                }
                OrcGrupo NovoG = new OrcGrupo(this, iG, string.Format("Grupo {0}", RepeteDescricao ? 0 : iG), ValorG, OrdemG);
                Grupos.Add(OrdemG,NovoG );
                for (int iSG = 1; iSG <= nSG; iSG++)
                {
                    if (!RepeteDescricao)
                    {
                        ValorS += 10;
                        OrdemS++;
                    }
                    SubOrcGrupo NovoS = new SubOrcGrupo(NovoG, iSG, string.Format("Sub-Grupo {0}", RepeteDescricao ? 0 : iSG), ValorS, OrdemS);
                    NovoG.SubGrupos.Add(OrdemS, NovoS);
                    for (int iD = 1; iD <= nD; iD++)
                    {
                        if (!RepeteDescricao)
                        {
                            ValorD++;
                            OrdemD++;
                        }
                        NovoS.Detalhes.Add(OrdemD, new DetalheGrupo(NovoS, iD, RepeteDescricao ? "000000" : string.Format("{0:00}{1:00}{2:00}",iG,iSG, iD), string.Format("Descrição {0}", RepeteDescricao ? 0 : iD), ValorD, OrdemD));
                    }
                }
            }
            return Popula();
        }*/

        #region Edição

        

        public OrcGrupo NovoGrupo(string Titulo = "Novo Grupo",OrcGrupo Anterior = null,decimal? Valor = null)
        {
            int OrdemNovo = 0;
            if (Anterior != null)
                OrdemNovo = Anterior.Ordem + 1;
            else
                foreach (OrcGrupo Candidato in Grupos.Values)
                    if (Candidato.Ordem >= OrdemNovo)
                        OrdemNovo = Candidato.Ordem + 1;
            OrcGrupo NovoG;            
            NovoG = new OrcGrupo(this, null, Titulo, null, OrdemNovo);
            foreach (OrcGrupo Candidato in Grupos.Values)
                if (Candidato.Ordem >= OrdemNovo)
                    Candidato.IncOrdem(true);
            Grupos.Add(NovoG.ORG, NovoG);
            return NovoG;
        }

        public void MoveGrupo(OrcGrupo Origem, OrcGrupo Destino)
        {
            int OrdemDestino = Destino.Ordem;
            foreach (OrcGrupo Candidato in Grupos.Values)
                if (Candidato.Ordem >= OrdemDestino)
                    Candidato.IncOrdem(true);
            Origem.Ordem = OrdemDestino;
            Origem.IncOrdem(false);
        }

        public SubOrcGrupo NovoSubGrupo(OrcGrupo Grupo, string Titulo = "Novo Sub Grupo", SubOrcGrupo Anterior = null, decimal? Valor = null, bool Aberto = true)
        {
            int OrdemNovo = 0;
            if (Anterior != null)
                OrdemNovo = Anterior.Ordem + 1;
            else
                foreach (SubOrcGrupo Candidato in Grupo.SubGrupos.Values)
                    if (Candidato.Ordem >= OrdemNovo)
                        OrdemNovo = Candidato.Ordem + 1;
            SubOrcGrupo NovoS = new SubOrcGrupo(Grupo, null, Titulo, Valor, OrdemNovo, Aberto);
            foreach (SubOrcGrupo Candidato in Grupo.SubGrupos.Values)
                if (Candidato.Ordem >= OrdemNovo)
                    Candidato.IncOrdem(true);
            Grupo.SubGrupos.Add(NovoS.ORSG, NovoS);
            return NovoS;
        }

        public DetalheGrupo NovoDetalhe(SubOrcGrupo SubGrupo, string Titulo,string PLA, decimal? valor, DetalheGrupo Anterior = null,bool Aberto = false)
        {
            int OrdemNovo = 0;
            if (Anterior != null)
                OrdemNovo = Anterior.Ordem + 1;
            else
                foreach (DetalheGrupo Candidato in SubGrupo.Detalhes.Values)
                    if (Candidato.Ordem >= OrdemNovo)
                        OrdemNovo = Candidato.Ordem + 1;
            if (valor.HasValue && (valor.Value == 0))
                valor = null;
            DetalheGrupo NovoDetalheGrupo = new DetalheGrupo(SubGrupo, null, PLA, Titulo, valor, OrdemNovo,Aberto);            
            foreach (DetalheGrupo Candidato in SubGrupo.Detalhes.Values)
                if (Candidato.Ordem >= OrdemNovo)
                    Candidato.IncOrdem(true);
            SubGrupo.Detalhes.Add(NovoDetalheGrupo.ORD, NovoDetalheGrupo);
            return NovoDetalheGrupo;
        }

        public OrcGrupo GetGrupo(int ORG)
        {
            foreach (OrcGrupo Candidato in Grupos.Values)
                if (Candidato.ORG == ORG)
                    return Candidato;
            return null;
        }

        public SubOrcGrupo GetSGrupo(int ORSG)
        {
            foreach (OrcGrupo Candidato in Grupos.Values)
                foreach (SubOrcGrupo CandidatoS in Candidato.SubGrupos.Values)
                    if (CandidatoS.ORSG == ORSG)
                        return CandidatoS;
            return null;
        }

        public DetalheGrupo GetDetalheGrupo(int ORD)
        {
            foreach (OrcGrupo Candidato in Grupos.Values)
                foreach (SubOrcGrupo CandidatoS in Candidato.SubGrupos.Values)
                    foreach (DetalheGrupo CandidatoD in CandidatoS.Detalhes.Values)
                        if (CandidatoD.ORD == ORD)
                            return CandidatoD;
            return null;
        }

        #endregion

        #region Serializador
        internal static DataContractSerializer Serializador()
        {
            return new DataContractSerializer(typeof(Orcamento), new List<Type>() { typeof(Competencia) });
        }

        /// <summary>
        /// Salva XML
        /// </summary>
        /// <returns></returns>
        public bool SalvarXML(string _ARQNomeArquivo = null)
        {
            if (_ARQNomeArquivo != null)
            {
                NomeArquivo = _ARQNomeArquivo;
            }
            if (NomeArquivo == null)
                return false;            
            using (Stream StrArquivo = new FileStream(NomeArquivo, FileMode.Create))
            {
                Serializador().WriteObject(StrArquivo, this);
                StrArquivo.Close();
            }            
            return true;
        }

        public static Orcamento RecuperaXML(string _ARQNomeArquivo)
        {            
            Orcamento Retorno = null;
            try
            {                                
                if (File.Exists(_ARQNomeArquivo))
                {
                    using (Stream oStream = new FileStream(_ARQNomeArquivo, FileMode.Open))
                    {
                        Retorno = (Orcamento)Serializador().ReadObject(oStream);
                        oStream.Close();
                    }
                }
                Retorno.NomeArquivo = _ARQNomeArquivo;
                foreach (OrcGrupo Grupo in Retorno.Grupos.Values)
                {
                    Grupo.PaiOrc = Retorno;
                    foreach (SubOrcGrupo SGrupo in Grupo.SubGrupos.Values)
                    {
                        SGrupo.PaiGrupo = Grupo;
                        foreach (DetalheGrupo Detalhe in SGrupo.Detalhes.Values)
                            Detalhe.PaiSubGrupo = SGrupo;
                    }
                }
                Retorno.ConstroiDadosTela(NivelDetalhe.Aberto);                
                return Retorno;
            }
            catch (Exception)
            {                
                return null;
            }
        }

        public void ConstroiDadosTela(NivelDetalhe NivelD)
        {
            dOrcGrade.GradeOrcamento.Clear();
            foreach (OrcGrupo Grupo in Grupos.Values)
                Grupo.ConstroiDadosTela(NivelD);
            AjusteDeValoresTela(NivelD);
        }
        #endregion
    }

    [DataContract(Name = "NoBase", Namespace = "OrcamentoProc")]
    public abstract class NoBase
    {
        [DataMember]
        protected int pk;

        [DataMember]
        protected int _Ordem;

        public dOrcGrade.GradeOrcamentoRow rowBase;

        protected System.Data.DataRow rowDB;

        public int Ordem { get => _Ordem; set => _Ordem = value; }
        [DataMember]
        public string Titulo;
        [DataMember]
        private bool[] _Abrir;        
        public abstract int? OrdemGeral { get; }
        [DataMember]
        private decimal? _Valor;
        public decimal Valor
        {
            get => _Valor.GetValueOrDefault(0);
            set
            {
                if (value == 0)
                    _Valor = null;
                else
                    _Valor = value;
            }
        }
        public abstract decimal ValorAcumulado { get; }
        public decimal ValorEfetivo { get => _Valor.GetValueOrDefault(ValorAcumulado); }

        public bool Aberto(NivelDetalhe Nivel)
        {
            if (Nivel == NivelDetalhe.Aberto)
                return true;
            return _Abrir[(int)Nivel];
        }

        public bool PaiFechado = false;

        public void SetAberto(NivelDetalhe Nivel,bool value)
        {
            if (Nivel != NivelDetalhe.Aberto)
                _Abrir[(int)Nivel] = value;
            GravaDB();
        }

        public NoBase(Orcamento __PaiOrc, int? _pk, string _Titulo, decimal? _Valor, int _Ordem, bool AbBalancete = true, bool? AbBoleto = null, bool? AbAno = null)
        {
            PaiOrc = __PaiOrc;
            if (_pk.HasValue)
                pk = _pk.Value;
            Titulo = _Titulo;
            if(_Valor.HasValue)
               Valor = _Valor.Value;
            _Abrir = new bool[3] { AbBalancete, AbBoleto.GetValueOrDefault(AbBalancete), AbAno.GetValueOrDefault(AbBalancete) };
            Titulo = _Titulo;
            this._Ordem = _Ordem;            
        }

        private Orcamento _PaiOrc;
        public Orcamento PaiOrc { get => _PaiOrc; set => _PaiOrc = value; }

        public abstract void IncOrdem(bool SubNivel);

        public abstract void ConstroiDadosTela(NivelDetalhe NivelD);

        public StatusValor statusValor
        {
            get
            {
                decimal ValorCalculado = ValorAcumulado;
                if (_Valor.HasValue)
                {
                    return ((ValorCalculado == 0) || (ValorCalculado == _Valor.Value)) ? StatusValor.DefinidoValido : StatusValor.DefinidoInvalido;
                }
                else
                {
                    return ValorCalculado == 0 ? StatusValor.NaoDefinido : StatusValor.Calculado;
                }
            }

        }

        public abstract bool Delete();

        public abstract bool GravaDB();
    }

    [DataContract(Name = "GrupoBase", Namespace = "OrcamentoProc")]
    public abstract class GrupoBase: NoBase
    {
        protected dOrcamento.ORcamentoGrupoRow rowORG { get => (dOrcamento.ORcamentoGrupoRow)rowDB; set => rowDB = value; }

        public GrupoBase(Orcamento _PaiOrc, dOrcamento.ORcamentoGrupoRow _rowORG):base(_PaiOrc, _rowORG.ORG, _rowORG.ORGTitulo, _rowORG.ORGValor, _rowORG.ORGOrdem, _rowORG.ORGAbertoBal, _rowORG.ORGAbertoBol, _rowORG.ORGAbertoAno)
        {
            rowORG = _rowORG;
        }

        public GrupoBase(Orcamento _PaiOrc, int? _ORG, string _Titulo, decimal? _Valor, int _Ordem ,int? ORGNivelSup, bool Aberto = true):base(_PaiOrc, _ORG, _Titulo,_Valor, _Ordem, Aberto)
        {
            Ordem = _Ordem;            
            if (!_ORG.HasValue)
            {
                if (!_PaiOrc.real)
                    pk = _PaiOrc.ORGSimulador++;
                else
                {
                    rowORG = _PaiOrc.DOrcamento.ORcamentoGrupo.NewORcamentoGrupoRow();
                    rowORG.ORG_ORC = _PaiOrc.ORC;
                    rowORG.ORGAbertoAno = Aberto;
                    rowORG.ORGAbertoBol = Aberto;
                    rowORG.ORGAbertoBal = Aberto;
                    if (ORGNivelSup.HasValue)
                    {
                        rowORG.ORGNivel = 2;
                        rowORG.ORG_ORG = ORGNivelSup.Value;
                    }
                    else
                        rowORG.ORGNivel = 1;
                    rowORG.ORGTitulo = _Titulo;
                    rowORG.ORGOrdem = _Ordem;
                    rowORG.ORGValor = _Valor.GetValueOrDefault(0);
                    _PaiOrc.DOrcamento.ORcamentoGrupo.AddORcamentoGrupoRow(rowORG);
                    _PaiOrc.DOrcamento.ORcamentoGrupoTableAdapter.Update(rowORG);
                    pk = rowORG.ORG;
                }
            }
            else
                pk = _ORG.Value;
        }

        public override bool GravaDB()
        {
            if (!PaiOrc.real)
                return false;
            bool Alterado = false;
            if (Titulo != rowORG.ORGTitulo)
            {
                rowORG.ORGTitulo = Titulo;
                Alterado = true;
            }
            if (Valor != rowORG.ORGValor)
            {
                rowORG.ORGValor = Valor;
                Alterado = true;
            }
            if (Aberto(NivelDetalhe.Anual) != rowORG.ORGAbertoAno)
            {
                rowORG.ORGAbertoAno = Aberto(NivelDetalhe.Anual);
                Alterado = true;
            }
            if (Aberto(NivelDetalhe.Boleto) != rowORG.ORGAbertoBol)
            {
                rowORG.ORGAbertoAno = Aberto(NivelDetalhe.Boleto);
                Alterado = true;
            }
            if (Aberto(NivelDetalhe.Balancete) != rowORG.ORGAbertoBal)
            {
                rowORG.ORGAbertoBal = Aberto(NivelDetalhe.Balancete);
                Alterado = true;
            }
            if (Ordem != rowORG.ORGOrdem)
            {
                rowORG.ORGOrdem = Ordem;
                Alterado = true;
            }
            if (Alterado)
            {
                PaiOrc.DOrcamento.ORcamentoGrupoTableAdapter.Update(rowORG);   
                return true;   
            }
            return false;
        }
    }

    [DataContract(Name = "OrcGrupo", Namespace = "OrcamentoProc")]
    public class OrcGrupo : GrupoBase
    {                
        public int ORG { get => pk; }
        [DataMember]
        public SortedList<int, SubOrcGrupo> SubGrupos;
        public override int? OrdemGeral => Ordem * 100000;
                
        public override decimal ValorAcumulado
        {
            get
            {
                decimal Acumulado = 0;
                foreach (SubOrcGrupo SubGrupo in SubGrupos.Values)
                    Acumulado += SubGrupo.ValorEfetivo;
                return Acumulado;
            }
        }

        public override void ConstroiDadosTela(NivelDetalhe NivelD)
        {
            GravarowBase(PaiOrc);

            foreach (SubOrcGrupo SubGrupo in SubGrupos.Values)
                if (Aberto(NivelD))
                {
                    SubGrupo.PaiFechado = false;
                    SubGrupo.ConstroiDadosTela(NivelD);
                }
                else
                {
                    SubGrupo.PaiFechado = true;
                    foreach (DetalheGrupo Det in SubGrupo.Detalhes.Values)
                        Det.PaiFechado = true;
                }
        }

        public void GravarowBase(Orcamento _PaiOrc)
        {
            PaiOrc = _PaiOrc;
            rowBase = PaiOrc.dOrcGrade.GradeOrcamento.NewGradeOrcamentoRow();
            rowBase.Ordem = OrdemGeral.Value;
            rowBase.ORG1 = ORG;
            rowBase.TituloG = Titulo;
            if (Valor != 0)
            {
                rowBase.ValorG = Valor;
                rowBase.StatusValorG = (int)StatusValor.DefinidoValido;
            }
            else
                rowBase.StatusValorG = (int)StatusValor.NaoDefinido;
            rowBase.StatusValorSG = (int)StatusValor.NaoDefinido;
            rowBase.StatusValorD = (int)StatusValor.NaoDefinido;
            rowBase.BaseG = true;
            rowBase.BaseSG = false;
            rowBase.BaseD = false;
            PaiOrc.dOrcGrade.GradeOrcamento.AddGradeOrcamentoRow(rowBase);
            rowBase.AcceptChanges();
        }       

        public OrcGrupo(Orcamento _PaiOrc, int? _ORG, string _Titulo, decimal? _Valor,int _Ordem):base(_PaiOrc,_ORG, _Titulo,_Valor, _Ordem,null)
        {
            OrcGrupoInicio(_PaiOrc);
        }

        public OrcGrupo(Orcamento _PaiOrc, dOrcamento.ORcamentoGrupoRow _rowORG):base(_PaiOrc,_rowORG)
        {
            OrcGrupoInicio(_PaiOrc);
        }

        private void OrcGrupoInicio(Orcamento _PaiOrc)
        {
            SubGrupos = new SortedList<int, SubOrcGrupo>();
            GravarowBase(_PaiOrc);
        }

        public override void IncOrdem(bool IncOrdem)
        {
            if (IncOrdem)
                Ordem++;
            if (SubGrupos.Count == 0)
                rowBase.Ordem = OrdemGeral.Value;
            else
                foreach (SubOrcGrupo SubGrupo in SubGrupos.Values)
                    SubGrupo.IncOrdem(false);
            GravaDB();
        }

        public override bool Delete()
        {
            if (SubGrupos.Count != 0)
                return false;
            rowBase.Delete();
            PaiOrc.dOrcGrade.AcceptChanges();
            PaiOrc.Grupos.Remove(ORG);
            if (PaiOrc.real)
            {
                rowORG.Delete();
                PaiOrc.DOrcamento.ORcamentoGrupoTableAdapter.Update(rowORG);
            }
            return true;
        }

        public void AddSubGrupo(SubOrcGrupo Origem, int? Ordem)
        {
            int OrdemDestino = Ordem.GetValueOrDefault(0);
            foreach (SubOrcGrupo Candidato in SubGrupos.Values)
                if (Candidato.Ordem >= OrdemDestino)
                    Candidato.IncOrdem(true);
            Origem.Ordem = OrdemDestino;
            Origem.PaiGrupo = this;
            SubGrupos.Add(Origem.ORSG, Origem);
            Origem.ConstroiDadosTela(NivelDetalhe.Aberto);            
            Origem.IncOrdem(false);
        }

        /// <summary>
        /// Remove o subgrupo do grupo
        /// </summary>
        /// <param name="SubGrupoApagar"></param>
        /// <returns></returns>
        public bool Delete(SubOrcGrupo SubGrupoApagar)
        {
            if (SubGrupos.Count == 1)
            {                
                rowBase.SetValorSGNull();
                rowBase.SetTituloSGNull();
                rowBase.SetORG2Null();
                rowBase.StatusValorSG = (int)StatusValor.NaoDefinido;
                rowBase.SetORDNull();                
                rowBase.BaseSG = false;
                rowBase.SetTituloDNull();
                rowBase.SetValorDNull();
                rowBase.SetPLANull();
                rowBase.SetPLADescricaoNull();
                foreach (DetalheGrupo DetAp in SubGrupoApagar.Detalhes.Values)
                    if (DetAp.rowBase != rowBase)
                        DetAp.rowBase.Delete();
            }
            else
            {
                if (rowBase.ORG2 != SubGrupoApagar.ORSG)               
                    SubGrupoApagar.Deleterow();                                    
                else
                {
                    SubOrcGrupo MenorOrdem = null;
                    foreach (SubOrcGrupo Candidato in SubGrupos.Values)
                        if (Candidato.ORSG != SubGrupoApagar.ORSG)                        
                            if ((MenorOrdem == null) || (MenorOrdem.Ordem > Candidato.Ordem))
                                MenorOrdem = Candidato;                        
                    rowBase = MenorOrdem.rowBase;
                    rowBase.BaseG = true;
                    SubGrupoApagar.Deleterow();
                }
            }
            PaiOrc.dOrcGrade.AcceptChanges();
            SubGrupos.Remove(SubGrupoApagar.ORSG);
            return true;
        }

        public void AjusteDeValoresTela(NivelDetalhe NivelD)
        {            
            StatusValor StatusV = statusValor;
            rowBase.StatusValorG = (int)StatusV;
            switch (StatusV)
            {
                case StatusValor.NaoDefinido:
                    rowBase.SetValorGNull();
                    break;
                case StatusValor.Calculado:
                    rowBase.ValorG = ValorAcumulado;
                    break;
                case StatusValor.DefinidoInvalido:
                case StatusValor.DefinidoValido:
                    rowBase.ValorG = Valor;
                    break;
            }
            rowBase.TituloG = Titulo;
            rowBase.AcceptChanges();
            foreach (SubOrcGrupo SubGrupo in SubGrupos.Values)
                SubGrupo.AjusteDeValoresTela(NivelD, Titulo, ValorEfetivo, StatusV);

        }
        
    }

    [DataContract(Name = "SubOrcGrupo", Namespace = "OrcamentoProc")]
    public class SubOrcGrupo : GrupoBase
    {
        public OrcGrupo PaiGrupo;
        public Orcamento PaiOrc { get => PaiGrupo.PaiOrc; }
        public int ORSG { get => pk; }
        [DataMember]
        public SortedList<int, DetalheGrupo> Detalhes;
        public override int? OrdemGeral => PaiGrupo.Ordem * 100000 + Ordem * 1000;
        public override decimal ValorAcumulado
        {
            get
            {
                decimal Acumulado = 0;
                foreach (DetalheGrupo Detalhe in Detalhes.Values)
                    Acumulado += Detalhe.ValorEfetivo;
                return Acumulado;
            }            
        }

        public override void ConstroiDadosTela(NivelDetalhe NivelD)
        {
            if (!PaiFechado)
                GravarowBase(PaiGrupo);

            foreach (DetalheGrupo Detalhe in Detalhes.Values)
                if ((Aberto(NivelD)) && (!PaiFechado))
                {
                    Detalhe.PaiFechado = false;
                    Detalhe.ConstroiDadosTela(NivelD);
                }
                else
                    Detalhe.PaiFechado = true;
        }

        public void GravarowBase(OrcGrupo _PaiGrupo)
        {
            PaiGrupo = _PaiGrupo;
            rowBase = PaiGrupo.rowBase;
            if (!rowBase.IsORG2Null())
            {
                dOrcGrade.GradeOrcamentoRow ModeloRow = rowBase;
                rowBase = PaiOrc.dOrcGrade.GradeOrcamento.NewGradeOrcamentoRow();
                rowBase.ORG1 = ModeloRow.ORG1;
                rowBase.TituloG = ModeloRow.TituloG;
                if (!ModeloRow.IsValorGNull())
                    rowBase.ValorG = ModeloRow.ValorG;
                rowBase.StatusValorG = ModeloRow.StatusValorG;
                if ((ModeloRow.BaseG) && (OrdemGeral < ModeloRow.Ordem))
                {
                    rowBase.BaseG = true;
                    ModeloRow.BaseG = false;
                    PaiGrupo.rowBase = rowBase;
                }
                else
                    rowBase.BaseG = false;
                rowBase.BaseSG = false;
                rowBase.BaseD = false;
                rowBase.StatusValorSG = (int)StatusValor.NaoDefinido;
                rowBase.StatusValorD = (int)StatusValor.NaoDefinido;
                PaiOrc.dOrcGrade.GradeOrcamento.AddGradeOrcamentoRow(rowBase);
            }
            rowBase.Ordem = OrdemGeral.Value;
            rowBase.ORG2 = ORSG;
            rowBase.TituloSG = Titulo;
            rowBase.BaseSG = true;
            if (Valor != 0)
            {
                rowBase.ValorSG = Valor;
                rowBase.StatusValorSG = (int)StatusValor.DefinidoValido;
            }
            rowBase.AcceptChanges();
            //foreach (DetalheGrupo Detalhe in Detalhes.Values)
            //    Detalhe.GravarowBase();
        }

        public SubOrcGrupo(OrcGrupo _PaiORG, int? _ORSG, string _Titulo, decimal? _Valor, int _Ordem,bool Aberto = true) :base(_PaiORG.PaiOrc, _ORSG, _Titulo, _Valor, _Ordem, _PaiORG.ORG, Aberto)
        {
            SubOrcGrupoInicio(_PaiORG);
        }

        public SubOrcGrupo(OrcGrupo _PaiORG, dOrcamento.ORcamentoGrupoRow _rowORG):base(_PaiORG.PaiOrc,_rowORG)
        {
            SubOrcGrupoInicio(_PaiORG);
        }

        private void SubOrcGrupoInicio(OrcGrupo _PaiORG)
        {
            Detalhes = new SortedList<int, DetalheGrupo>();
            GravarowBase(_PaiORG);
        }

        public override void IncOrdem(bool IncOrdem)
        {
            if (IncOrdem)
                Ordem++;
            if (Detalhes.Count == 0)
                rowBase.Ordem = OrdemGeral.Value;
            else
                foreach (DetalheGrupo Detalhe in Detalhes.Values)
                    Detalhe.IncOrdem(false);
            GravaDB();
        }

        public override bool Delete()
        {
            if (Detalhes.Count != 0)
                return false;
            PaiGrupo.Delete(this);
            if (PaiOrc.real)
            {
                rowORG.Delete();
                PaiOrc.DOrcamento.ORcamentoGrupoTableAdapter.Update(rowORG);
            }
            return true;
        }

        public bool Delete(DetalheGrupo DetalheApagar)
        {
            if (Detalhes.Count == 1)
            {
                rowBase.SetValorDNull();
                rowBase.SetTituloDNull();
                rowBase.SetORDNull();
                rowBase.SetPLADescricaoNull();
                rowBase.SetPLANull();
                rowBase.StatusValorD = (int)StatusValor.NaoDefinido;                
            }
            else
            {
                if (rowBase.ORD != DetalheApagar.ORD)
                    DetalheApagar.rowBase.Delete();
                else
                {
                    DetalheGrupo MenorOrdem = null;
                    foreach (DetalheGrupo Candidato in Detalhes.Values)
                        if (Candidato.ORD != DetalheApagar.ORD)
                            if ((MenorOrdem == null) || (MenorOrdem.Ordem > Candidato.Ordem))
                                MenorOrdem = Candidato;
                    rowBase = MenorOrdem.rowBase;
                    rowBase.BaseSG = true;
                    if (DetalheApagar.rowBase.BaseG)
                    {
                        rowBase.BaseG = true;
                        PaiGrupo.rowBase = rowBase;
                    }
                    DetalheApagar.rowBase.Delete();
                    DetalheApagar.rowBase.AcceptChanges();
                }
            }
            PaiOrc.dOrcGrade.AcceptChanges();
            Detalhes.Remove(DetalheApagar.ORD);
            return true;
        }

        public void AddDetalhe(DetalheGrupo Origem, int? Ordem)
        {
            int OrdemDestino = Ordem.GetValueOrDefault(0);
            foreach (DetalheGrupo Candidato in Detalhes.Values)
                if (Candidato.Ordem >= OrdemDestino)
                    Candidato.IncOrdem(true);
            Origem.Ordem = OrdemDestino;
            Origem.PaiSubGrupo = this;            
            Detalhes.Add(Origem.ORD, Origem);
            Origem.ConstroiDadosTela(NivelDetalhe.Aberto);            
            Origem.IncOrdem(false);
        }

        public void Deleterow()
        {
            if (Detalhes.Count == 0)
                rowBase.Delete();
            else
                foreach (DetalheGrupo det in Detalhes.Values)
                    det.rowBase.Delete();
        }

        public void AjusteDeValoresTela(NivelDetalhe NivelD, string TituloG, decimal ValorG, StatusValor StatusGrupoPai)
        {
            if (PaiFechado)
                return;
            if (ValorG != 0)
                rowBase.ValorG = ValorG;
            else
                rowBase.SetValorGNull();
            StatusValor StatusV = statusValor;
            if ((StatusGrupoPai == StatusValor.DefinidoInvalido) && (StatusV == StatusValor.DefinidoValido))
                StatusV = StatusValor.DefinidoInvalido;
            rowBase.StatusValorSG = (int)StatusV;
            switch (StatusV)
            {
                case StatusValor.NaoDefinido:
                    rowBase.SetValorSGNull();
                    break;
                case StatusValor.Calculado:
                    rowBase.ValorSG = ValorAcumulado;
                    break;
                case StatusValor.DefinidoInvalido:
                case StatusValor.DefinidoValido:
                    rowBase.ValorSG = Valor;
                    break;
            }
            rowBase.TituloG = TituloG;
            rowBase.TituloSG = Titulo;
            rowBase.StatusValorG = (int)StatusGrupoPai;
            rowBase.AcceptChanges();
            foreach (DetalheGrupo Detalhe in Detalhes.Values)
                Detalhe.AjusteDeValoresTela(NivelD, TituloG, Titulo, ValorG, ValorEfetivo, StatusGrupoPai, StatusV);
        }

        public void MoveDetalhe(DetalheGrupo Origem, DetalheGrupo Destino, inserir posicao)
        {
            int OrdemDestino = Destino.Ordem;
            if (posicao == inserir.depois)
                OrdemDestino++;
            DetalheGrupo CandidatoABase = Origem;
            Origem.Ordem = OrdemDestino;
            foreach (DetalheGrupo Candidato in Detalhes.Values)
            {
                if ((Origem != Candidato) && (Candidato.Ordem >= OrdemDestino))
                    Candidato.IncOrdem(true);
                if (Candidato.Ordem < CandidatoABase.Ordem)
                    CandidatoABase = Candidato;
            }
            //Origem.Ordem = OrdemDestino;
            Origem.IncOrdem(false);            
            if (rowBase != CandidatoABase.rowBase)
            {
                if(rowBase.BaseG)
                {
                    rowBase.BaseG = false;
                    CandidatoABase.rowBase.BaseG = true;
                    Origem.PaiSubGrupo.PaiGrupo.rowBase = CandidatoABase.rowBase;
                }
                rowBase.BaseSG = false;
                rowBase.AcceptChanges();
                rowBase = CandidatoABase.rowBase;
                rowBase.BaseSG = true;
                rowBase.AcceptChanges();
            }
        }
    }

    [DataContract(Name = "DetalheGrupo", Namespace = "OrcamentoProc")]
    public class DetalheGrupo : NoBase
    {
        private dOrcamento.ORcamentoDetalheRow rowORD { get => (dOrcamento.ORcamentoDetalheRow)rowDB; set => rowDB = value; }

        public SubOrcGrupo PaiSubGrupo;
        [DataMember]
        public string PLA;
        [DataMember]
        public string PLADescricao;
        public int ORD { get => pk; }
        public Orcamento PaiOrc { get => PaiSubGrupo.PaiOrc; }
        public override int? OrdemGeral => PaiSubGrupo.PaiGrupo.Ordem * 100000 + PaiSubGrupo.Ordem * 1000 + Ordem * 10;
        public override decimal ValorAcumulado { get => 0; }

        private dOrcGrade.GradeOrcamentoRow[] rowFantasma;

        /// <summary>
        /// Linhas criadas para demonstrar o detalhe aberto
        /// </summary>
        /// <param name="NivelD"></param>
        /// <param name="Criar"></param>
        private void CriaAtrowFantasma(NivelDetalhe NivelD,bool Criar)
        {
            if (!Aberto(NivelD) || (NivelD == NivelDetalhe.Aberto))
                return;
            if (rowFantasma == null)
                rowFantasma = new dOrcGrade.GradeOrcamentoRow[2];
            for (int i = 0; i < 2; i++)
            {
                if (Criar)
                    rowFantasma[i] = PaiOrc.dOrcGrade.GradeOrcamento.NewGradeOrcamentoRow();
                rowFantasma[i].ORG1 = rowBase.ORG1;
                rowFantasma[i].TituloG = rowBase.TituloG;
                rowFantasma[i].ORG2 = rowBase.ORG2;
                rowFantasma[i].TituloSG = rowBase.TituloSG;
                rowFantasma[i].PLA = rowBase.PLA;
                rowFantasma[i].TituloD = rowBase.TituloD;
                rowFantasma[i].BaseG = false;
                rowFantasma[i].BaseSG = false;
                rowFantasma[i].BaseD = false;
                rowFantasma[i].TituloDetalhe = string.Format("{0} Nota XXXX{1}", rowBase.IsTituloDNull() ? "" : rowBase.TituloD, i + 1);
                rowFantasma[i].StatusValorG = rowBase.StatusValorG;
                rowFantasma[i].StatusValorSG = rowBase.StatusValorSG;
                rowFantasma[i].StatusValorD = rowBase.StatusValorD;
                rowFantasma[i].ORD = rowBase.ORD;
                if (!rowBase.IsValorGNull())
                    rowFantasma[i].ValorG = rowBase.ValorG;
                if (!rowBase.IsValorSGNull())
                    rowFantasma[i].ValorSG = rowBase.ValorSG;
                if (!rowBase.IsValorDNull())
                    rowFantasma[i].ValorD = rowBase.ValorD;
                rowFantasma[i].Ordem = OrdemGeral.Value + 1 + i;
                if (Criar)
                    PaiOrc.dOrcGrade.GradeOrcamento.AddGradeOrcamentoRow(rowFantasma[i]);
            }
        }

        public override void ConstroiDadosTela(NivelDetalhe NivelD)
        {
            GravarowBase(PaiSubGrupo);
            if (Aberto(NivelD))
                rowBase.TituloDetalhe = string.Format("{0} Nota XXXX{1}", rowBase.IsTituloDNull() ? "" : rowBase.TituloD, 1);
            else
                rowBase.TituloDetalhe = "";
            CriaAtrowFantasma(NivelD,true);                        
        }

        public void GravarowBase(SubOrcGrupo _PaiSubGrupo)
        {
            PaiSubGrupo = _PaiSubGrupo;
            rowBase = PaiSubGrupo.rowBase;
            if (!rowBase.IsORDNull())
            {
                dOrcGrade.GradeOrcamentoRow ModeloRow = rowBase;
                rowBase = PaiOrc.dOrcGrade.GradeOrcamento.NewGradeOrcamentoRow();
                rowBase.ORG1 = ModeloRow.ORG1;
                rowBase.TituloG = ModeloRow.TituloG;
                if (!ModeloRow.IsValorGNull())
                    rowBase.ValorG = ModeloRow.ValorG;
                rowBase.StatusValorG = ModeloRow.StatusValorG;
                rowBase.ORG2 = ModeloRow.ORG2;
                rowBase.TituloSG = ModeloRow.TituloSG;
                if (!ModeloRow.IsValorSGNull())
                    rowBase.ValorSG = ModeloRow.ValorSG;
                if ((ModeloRow.BaseG) && (OrdemGeral < ModeloRow.Ordem))
                {
                    rowBase.BaseG = true;
                    ModeloRow.BaseG = false;
                    PaiSubGrupo.PaiGrupo.rowBase = rowBase;
                }
                else
                    rowBase.BaseG = false;
                if ((ModeloRow.BaseSG) && (OrdemGeral < ModeloRow.Ordem))
                {
                    rowBase.BaseSG = true;
                    ModeloRow.BaseSG = false;
                    PaiSubGrupo.rowBase = rowBase;
                }
                else
                    rowBase.BaseSG = false;
                rowBase.BaseD = true;
                rowBase.TituloDetalhe = string.Format("{0} Nota XXXX", rowBase.IsTituloDNull() ? "" : rowBase.TituloD);
                rowBase.StatusValorG = ModeloRow.StatusValorG;
                rowBase.StatusValorSG = ModeloRow.StatusValorSG;
                rowBase.StatusValorD = (int)StatusValor.NaoDefinido;
                PaiOrc.dOrcGrade.GradeOrcamento.AddGradeOrcamentoRow(rowBase);
            }
            rowBase.BaseD = true;
            rowBase.ORD = ORD;
            rowBase.Ordem = OrdemGeral.Value;
            rowBase.TituloD = Titulo;
            if (Valor != 0)
            {
                rowBase.ValorD = Valor;
                rowBase.StatusValorD = (int)StatusValor.DefinidoValido;
            }
            rowBase.PLA = PLA;
            rowBase.PLADescricao = PLADescricao;            
            rowBase.AcceptChanges();
        }

        public DetalheGrupo(SubOrcGrupo _PaiSubGrupo, dOrcamento.ORcamentoDetalheRow _rowORD):base(_PaiSubGrupo.PaiOrc, _rowORD.ORD,_rowORD.ORDTitulo, _rowORD.ORDValor, _rowORD.ORDOrdem, _rowORD.ORDAbertoBal, _rowORD.ORDAbertoBOL, _rowORD.ORDAbertoAno)
        {
            rowORD = _rowORD;            
            DetalheGrupoInicio(_PaiSubGrupo, rowORD.ORD_PLA, rowORD.ORDTitulo, _rowORD.ORD, _rowORD.ORDAbertoAno, _rowORD.ORDAbertoBOL, _rowORD.ORDAbertoBal);
        }

        public DetalheGrupo(SubOrcGrupo _PaiSubGrupo, int? _ORD, string _PLA, string _Titulo,decimal? _Valor, int _Ordem,bool Aberto = false) :base(_PaiSubGrupo.PaiOrc, _ORD, _Titulo, _Valor,_Ordem,Aberto)
        {
            DetalheGrupoInicio(_PaiSubGrupo,_PLA, _Titulo, _ORD, Aberto, Aberto, Aberto);
        }

        private void DetalheGrupoInicio(SubOrcGrupo _PaiSubGrupo, string _PLA, string _Titulo, int? _ORD,bool AbertoAno, bool AbertoBOL, bool AbertoBal)
        {
            PaiSubGrupo = _PaiSubGrupo;
            PLA = _PLA;            
            Titulo = _Titulo;
            if (!_ORD.HasValue)
            {
                if (!PaiOrc.real)
                {
                    pk = PaiOrc.ORGSimulador++;
                    PLADescricao = Titulo;
                }
                else
                {
                    dOrcamento.PLAnocontasRow rowPLA = PaiOrc.DOrcamento.PLAnocontas.FindByPLA(PLA);
                    if (rowPLA != null)
                        PLADescricao = rowPLA.PLADescricao;
                    rowORD = PaiOrc.DOrcamento.ORcamentoDetalhe.NewORcamentoDetalheRow();
                    rowORD.ORD_ORG = PaiSubGrupo.ORSG;
                    rowORD.ORDOrdem = Ordem;
                    rowORD.ORDAbertoAno = AbertoAno;
                    rowORD.ORDAbertoBOL = AbertoBOL;
                    rowORD.ORDAbertoBal = AbertoBal;
                    rowORD.ORDLegado = false;
                    rowORD.ORDTitulo = Titulo;
                    rowORD.ORDValor = Valor;
                    rowORD.ORD_PLA = PLA;
                    PaiOrc.DOrcamento.ORcamentoDetalhe.AddORcamentoDetalheRow(rowORD);
                    PaiOrc.DOrcamento.ORcamentoDetalheTableAdapter.Update(rowORD);
                    pk = rowORD.ORD;
                }
            }
            else
                pk = _ORD.Value;
            GravarowBase(_PaiSubGrupo);
            PaiOrc.PLAsUsados.Add(PLA, Valor);
        }

        public override void IncOrdem(bool IncOrdem)
        {
            if (IncOrdem)
                Ordem++;
            rowBase.Ordem = OrdemGeral.Value;
            GravaDB();
        }

        public override bool Delete()
        {
            if (PaiOrc.PLAsUsados.ContainsKey(PLA))
                PaiOrc.PLAsUsados.Remove(PLA);
            PaiSubGrupo.Delete(this);
            if (PaiOrc.real)
            {
                rowORD.Delete();
                PaiOrc.DOrcamento.ORcamentoDetalheTableAdapter.Update(rowORD);
            }
            return true;
        }

        public override bool GravaDB()
        {
            if (!PaiOrc.real)
                return false;
            bool Alterado = false;
            if (Titulo != rowORD.ORDTitulo)
            {
                rowORD.ORDTitulo = Titulo;
                Alterado = true;
            }
            if (Valor != rowORD.ORDValor)
            {
                rowORD.ORDValor = Valor;
                Alterado = true;
            }
            if (Aberto(NivelDetalhe.Anual) != rowORD.ORDAbertoAno)
            {
                rowORD.ORDAbertoAno = Aberto(NivelDetalhe.Anual);
                Alterado = true;
            }
            if (Aberto(NivelDetalhe.Boleto) != rowORD.ORDAbertoBOL)
            {
                rowORD.ORDAbertoAno = Aberto(NivelDetalhe.Boleto);
                Alterado = true;
            }
            if (Aberto(NivelDetalhe.Balancete) != rowORD.ORDAbertoBal)
            {
                rowORD.ORDAbertoBal = Aberto(NivelDetalhe.Balancete);
                Alterado = true;
            }
            if (Ordem != rowORD.ORDOrdem)
            {
                rowORD.ORDOrdem = Ordem;
                Alterado = true;
            }
            if (PaiSubGrupo.ORSG != rowORD.ORD_ORG)
            {
                rowORD.ORD_ORG = PaiSubGrupo.ORSG;
                Alterado = true;
            }
            if (Alterado)
            {
                PaiOrc.DOrcamento.ORcamentoDetalheTableAdapter.Update(rowORD);   
                return true;
            }
            return false;
        }

        public void AjusteDeValoresTela(NivelDetalhe NivelD,string TituloG, string TituloSG, decimal ValorG, decimal ValorSG, StatusValor StatusGrupoPai, StatusValor StatusSGrupoPai)
        {
            if (PaiFechado)
                return;
            if (ValorG != 0)
            {
                rowBase.ValorG = ValorG;
                //if (rowFantasma != null)
                //{
                //    rowFantasma[0].ValorG = ValorG;
                //    rowFantasma[1].ValorG = ValorG;
                //}
            }
            else
            {
                rowBase.SetValorGNull();
                //if (rowFantasma != null)
                //{
                //    rowFantasma[0].SetValorGNull();
                //    rowFantasma[1].SetValorGNull();
                //}
            }
            if (ValorSG != 0)
            {
                rowBase.ValorSG = ValorSG;
                //if (rowFantasma != null)
                //{
                //    rowFantasma[0].ValorSG = ValorSG;
                //    rowFantasma[1].ValorSG = ValorSG;
                //}
            }
            else
            {
                rowBase.SetValorSGNull();
                //if (rowFantasma != null)
                //{
                //    rowFantasma[0].SetValorSGNull();
                //    rowFantasma[1].SetValorSGNull();
                //}
            }

            StatusValor StatusV = statusValor;
            if ((StatusSGrupoPai == StatusValor.DefinidoInvalido) && (StatusV == StatusValor.DefinidoValido))
                StatusV = StatusValor.DefinidoInvalido;
            rowBase.StatusValorD = (int)StatusV;
            switch (StatusV)
            {
                case StatusValor.NaoDefinido:
                    rowBase.SetValorDNull();
                    //if (rowFantasma != null)
                    //{
                    //    rowFantasma[0].SetValorDNull();
                    //    rowFantasma[1].SetValorDNull();
                    //}
                    break;
                case StatusValor.Calculado:
                    rowBase.ValorD = ValorAcumulado;
                    //if (rowFantasma != null)
                    //{
                    //    rowFantasma[0].ValorD = ValorAcumulado;
                     //   rowFantasma[1].ValorD = ValorAcumulado;
                    //}
                    break;
                case StatusValor.DefinidoInvalido:
                case StatusValor.DefinidoValido:
                    rowBase.ValorD = Valor;
                    //if (rowFantasma != null)
                    //{
                    //    rowFantasma[0].ValorD = Valor;
                    //    rowFantasma[1].ValorD = Valor;
                    //}
                    break;
            }
            rowBase.TituloG = TituloG;
            rowBase.TituloSG = TituloSG;
            rowBase.TituloD = Titulo;
            rowBase.StatusValorG = (int)StatusGrupoPai;
            rowBase.StatusValorSG = (int)StatusSGrupoPai;            
            rowBase.AcceptChanges();
            CriaAtrowFantasma(NivelD, false);
        }
    }

    public enum inserir { antes, depois }

    public enum NivelDetalhe { Aberto = -1, Balancete = 0, Boleto = 1, Anual = 2}

    public enum StatusValor { NaoDefinido = 0, DefinidoValido = 1, DefinidoInvalido = 2, Calculado = 3}

}
