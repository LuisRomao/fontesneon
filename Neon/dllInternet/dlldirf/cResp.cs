using System.ComponentModel;

namespace dlldirf
{
    /// <summary>
    /// 
    /// </summary>
    public partial class cResp : CompontesBasicos.ComponenteCamposBase
    {
        /// <summary>
        /// 
        /// </summary>
        public cResp()
        {
            InitializeComponent();
            TResNome.Text = CompontesBasicos.dEstacao.dEstacaoSt.GetValor("DIF NOME");
            DocBacarios.CPFCNPJ cpf = new DocBacarios.CPFCNPJ(CompontesBasicos.dEstacao.dEstacaoSt.GetValor("DIF CPF"));
            if(cpf.Tipo != DocBacarios.TipoCpfCnpj.INVALIDO)
                textEdit1.Text = cpf.ToString();
            TRespDDD.Text = CompontesBasicos.dEstacao.dEstacaoSt.GetValor("DIF DDD");
            TRespTelefone.Text = CompontesBasicos.dEstacao.dEstacaoSt.GetValor("DIF FONE");
            TRespRamal.Text = CompontesBasicos.dEstacao.dEstacaoSt.GetValor("DIF RAMAL");
            TRespFax.Text = CompontesBasicos.dEstacao.dEstacaoSt.GetValor("DIF FAX");
            TRespEMail.Text = CompontesBasicos.dEstacao.dEstacaoSt.GetValor("DIF EMAIL");
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public override bool Update_F()
        {
            if (!Validate())
                return false;
            if (textEdit1.Text == "")
                return false;
            if (TResNome.Text == "")
            {
                TResNome.ErrorText = "Campo obrigatório";
                return false;
            }
            CompontesBasicos.dEstacao.dEstacaoSt.SetValor("DIF NOME", TResNome.Text);
            DocBacarios.CPFCNPJ cpf = new DocBacarios.CPFCNPJ(textEdit1.Text);  
            if (cpf.Tipo != DocBacarios.TipoCpfCnpj.INVALIDO)
                 CompontesBasicos.dEstacao.dEstacaoSt.SetValor("DIF CPF", cpf.ToString(false));
            CompontesBasicos.dEstacao.dEstacaoSt.SetValor("DIF DDD", TRespDDD.Text);
            CompontesBasicos.dEstacao.dEstacaoSt.SetValor("DIF FONE", TRespTelefone.Text);
            CompontesBasicos.dEstacao.dEstacaoSt.SetValor("DIF RAMAL", TRespRamal.Text);
            CompontesBasicos.dEstacao.dEstacaoSt.SetValor("DIF FAX", TRespFax.Text);
            CompontesBasicos.dEstacao.dEstacaoSt.SetValor("DIF EMAIL", TRespEMail.Text);
            return true;
        }
        
        private void textEdit1_Validating(object sender, CancelEventArgs e)
        {
            DocBacarios.CPFCNPJ Validando = new DocBacarios.CPFCNPJ((string)textEdit1.EditValue);
            if (Validando.Tipo != DocBacarios.TipoCpfCnpj.CPF)
                e.Cancel = true;
            else
                textEdit1.EditValue = Validando.ToString();
        }
        
        private void ValidaInt(object sender, CancelEventArgs e)
        {
           
            int Validando;
            DevExpress.XtraEditors.TextEdit Editor = (DevExpress.XtraEditors.TextEdit)sender;
            if ((Editor.EditValue == null) || ((string)Editor.EditValue == ""))
                return;
            if (!int.TryParse((string)Editor.EditValue, out Validando))
                e.Cancel = true;
            else
                Editor.EditValue = Validando;
        }

        private void ValidaFone(object sender, CancelEventArgs e)
        {
            int Validando;
            DevExpress.XtraEditors.TextEdit Editor = (DevExpress.XtraEditors.TextEdit)sender;
            if ((Editor.EditValue == null) || ((string)Editor.EditValue == ""))
                return;
            if (!int.TryParse((string)Editor.EditValue, out Validando))
                e.Cancel = true;
            else
                if (Validando > 999)
                    Editor.EditValue = Validando;
                else {
                    Editor.ErrorText = "Valor inválido";
                    e.Cancel = true;
                }
        }
    }
}

