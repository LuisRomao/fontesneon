namespace dlldirf
{
    partial class cResp
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling1 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling2 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling3 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling4 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling5 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling6 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling7 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(cResp));
            this.labelControl1 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl2 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl3 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl4 = new DevExpress.XtraEditors.LabelControl();
            this.textEdit1 = new DevExpress.XtraEditors.TextEdit();
            this.TResNome = new DevExpress.XtraEditors.TextEdit();
            this.TRespDDD = new DevExpress.XtraEditors.TextEdit();
            this.TRespTelefone = new DevExpress.XtraEditors.TextEdit();
            this.labelControl5 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl6 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl7 = new DevExpress.XtraEditors.LabelControl();
            this.TRespRamal = new DevExpress.XtraEditors.TextEdit();
            this.TRespFax = new DevExpress.XtraEditors.TextEdit();
            this.TRespEMail = new DevExpress.XtraEditors.TextEdit();
            ((System.ComponentModel.ISupportInitialize)(this.BarManager_F)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.BarAndDockingController_F)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.BindingSource_F)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.StyleController_F)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit1.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TResNome.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TRespDDD.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TRespTelefone.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TRespRamal.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TRespFax.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TRespEMail.Properties)).BeginInit();
            this.SuspendLayout();
            // 
            // barStatus_F
            // 
            this.barStatus_F.OptionsBar.AllowQuickCustomization = false;
            this.barStatus_F.OptionsBar.DisableClose = true;
            this.barStatus_F.OptionsBar.DisableCustomization = true;
            this.barStatus_F.OptionsBar.DrawDragBorder = false;
            this.barStatus_F.OptionsBar.DrawSizeGrip = true;
            this.barStatus_F.OptionsBar.UseWholeRow = true;
            // 
            // barComponente_F
            // 
            this.barComponente_F.OptionsBar.AllowQuickCustomization = false;
            this.barComponente_F.OptionsBar.DisableClose = true;
            this.barComponente_F.OptionsBar.DisableCustomization = true;
            this.barComponente_F.OptionsBar.DrawDragBorder = false;
            this.barComponente_F.OptionsBar.UseWholeRow = true;
            // 
            // btnFechar_F
            // 
            this.btnFechar_F.Visibility = DevExpress.XtraBars.BarItemVisibility.Never;
            // 
            // BarAndDockingController_F
            // 
            this.BarAndDockingController_F.PropertiesBar.AllowLinkLighting = false;
            // 
            // ToolTipController_F
            // 
            this.ToolTipController_F.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.ToolTipController_F.Appearance.ForeColor = System.Drawing.Color.Black;
            this.ToolTipController_F.Appearance.Options.UseBackColor = true;
            this.ToolTipController_F.Appearance.Options.UseFont = true;
            this.ToolTipController_F.Appearance.Options.UseForeColor = true;
            this.ToolTipController_F.Appearance.Options.UseTextOptions = true;
            this.ToolTipController_F.Appearance.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            // 
            // StyleController_F
            // 
            this.StyleController_F.AppearanceDisabled.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this.StyleController_F.AppearanceDisabled.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this.StyleController_F.AppearanceDisabled.Options.UseBackColor = true;
            this.StyleController_F.AppearanceDisabled.Options.UseForeColor = true;
            this.StyleController_F.AppearanceFocused.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.StyleController_F.AppearanceFocused.Options.UseBackColor = true;
  
            // 
            // labelControl1
            // 
            this.labelControl1.Location = new System.Drawing.Point(19, 42);
            this.labelControl1.Name = "labelControl1";
            this.labelControl1.Size = new System.Drawing.Size(19, 13);
            this.labelControl1.TabIndex = 0;
            this.labelControl1.Text = "CPF";
            // 
            // labelControl2
            // 
            this.labelControl2.Location = new System.Drawing.Point(125, 42);
            this.labelControl2.Name = "labelControl2";
            this.labelControl2.Size = new System.Drawing.Size(27, 13);
            this.labelControl2.TabIndex = 1;
            this.labelControl2.Text = "Nome";
            // 
            // labelControl3
            // 
            this.labelControl3.Location = new System.Drawing.Point(430, 42);
            this.labelControl3.Name = "labelControl3";
            this.labelControl3.Size = new System.Drawing.Size(21, 13);
            this.labelControl3.TabIndex = 2;
            this.labelControl3.Text = "DDD";
            // 
            // labelControl4
            // 
            this.labelControl4.Location = new System.Drawing.Point(483, 42);
            this.labelControl4.Name = "labelControl4";
            this.labelControl4.Size = new System.Drawing.Size(42, 13);
            this.labelControl4.TabIndex = 3;
            this.labelControl4.Text = "Telefone";
            // 
            // textEdit1
            // 
            this.textEdit1.EditValue = "";
            this.textEdit1.Location = new System.Drawing.Point(19, 61);
            this.textEdit1.Name = "textEdit1";
            this.textEdit1.Properties.MaxLength = 14;
            
            this.textEdit1.Size = new System.Drawing.Size(100, 20);
            
            this.textEdit1.TabIndex = 4;
            this.textEdit1.Validating += new System.ComponentModel.CancelEventHandler(this.textEdit1_Validating);
            // 
            // TResNome
            // 
            this.TResNome.EditValue = "";
            this.TResNome.Location = new System.Drawing.Point(125, 61);
            this.TResNome.Name = "TResNome";
            this.TResNome.Properties.MaxLength = 60;
            
            this.TResNome.Size = new System.Drawing.Size(299, 20);
            
            this.TResNome.TabIndex = 5;
            // 
            // TRespDDD
            // 
            this.TRespDDD.EditValue = "11";
            this.TRespDDD.Location = new System.Drawing.Point(430, 61);
            this.TRespDDD.Name = "TRespDDD";
            this.TRespDDD.Properties.MaxLength = 60;
            
            this.TRespDDD.Size = new System.Drawing.Size(27, 20);
            
            this.TRespDDD.TabIndex = 6;
            this.TRespDDD.Validating += new System.ComponentModel.CancelEventHandler(this.ValidaInt);
            // 
            // TRespTelefone
            // 
            this.TRespTelefone.EditValue = "33767900";
            this.TRespTelefone.Location = new System.Drawing.Point(471, 61);
            this.TRespTelefone.Name = "TRespTelefone";
            this.TRespTelefone.Properties.MaxLength = 8;
            
            this.TRespTelefone.Size = new System.Drawing.Size(87, 20);
            
            this.TRespTelefone.TabIndex = 7;
            this.TRespTelefone.Validating += new System.ComponentModel.CancelEventHandler(this.ValidaFone);
            // 
            // labelControl5
            // 
            this.labelControl5.Location = new System.Drawing.Point(19, 93);
            this.labelControl5.Name = "labelControl5";
            this.labelControl5.Size = new System.Drawing.Size(29, 13);
            this.labelControl5.TabIndex = 8;
            this.labelControl5.Text = "Ramal";
            // 
            // labelControl6
            // 
            this.labelControl6.Location = new System.Drawing.Point(125, 93);
            this.labelControl6.Name = "labelControl6";
            this.labelControl6.Size = new System.Drawing.Size(18, 13);
            this.labelControl6.TabIndex = 9;
            this.labelControl6.Text = "Fax";
            // 
            // labelControl7
            // 
            this.labelControl7.Location = new System.Drawing.Point(235, 93);
            this.labelControl7.Name = "labelControl7";
            this.labelControl7.Size = new System.Drawing.Size(28, 13);
            this.labelControl7.TabIndex = 10;
            this.labelControl7.Text = "e.mail";
            // 
            // TRespRamal
            // 
            this.TRespRamal.Location = new System.Drawing.Point(19, 112);
            this.TRespRamal.Name = "TRespRamal";
            this.TRespRamal.Properties.MaxLength = 6;
            
            this.TRespRamal.Size = new System.Drawing.Size(100, 20);
            
            this.TRespRamal.TabIndex = 11;
            this.TRespRamal.Validating += new System.ComponentModel.CancelEventHandler(this.ValidaInt);
            // 
            // TRespFax
            // 
            this.TRespFax.Location = new System.Drawing.Point(125, 112);
            this.TRespFax.Name = "TRespFax";
            this.TRespFax.Properties.MaxLength = 8;
            
            this.TRespFax.Size = new System.Drawing.Size(97, 20);
            
            this.TRespFax.TabIndex = 12;
            this.TRespFax.Validating += new System.ComponentModel.CancelEventHandler(this.ValidaFone);
            // 
            // TRespEMail
            // 
            this.TRespEMail.Location = new System.Drawing.Point(228, 112);
            this.TRespEMail.Name = "TRespEMail";
            this.TRespEMail.Properties.MaxLength = 50;
            
            this.TRespEMail.Size = new System.Drawing.Size(330, 20);
            
            this.TRespEMail.TabIndex = 13;
            // 
            // cResp
            // 
            this.Controls.Add(this.TRespEMail);
            this.Controls.Add(this.TRespFax);
            this.Controls.Add(this.TRespRamal);
            this.Controls.Add(this.labelControl7);
            this.Controls.Add(this.labelControl6);
            this.Controls.Add(this.labelControl5);
            this.Controls.Add(this.TRespTelefone);
            this.Controls.Add(this.TRespDDD);
            this.Controls.Add(this.TResNome);
            this.Controls.Add(this.textEdit1);
            this.Controls.Add(this.labelControl4);
            this.Controls.Add(this.labelControl3);
            this.Controls.Add(this.labelControl2);
            this.Controls.Add(this.labelControl1);
           
            this.LookAndFeel.UseDefaultLookAndFeel = false;
            this.Name = "cResp";
            this.Size = new System.Drawing.Size(572, 171);
            this.Controls.SetChildIndex(this.labelControl1, 0);
            this.Controls.SetChildIndex(this.labelControl2, 0);
            this.Controls.SetChildIndex(this.labelControl3, 0);
            this.Controls.SetChildIndex(this.labelControl4, 0);
            this.Controls.SetChildIndex(this.textEdit1, 0);
            this.Controls.SetChildIndex(this.TResNome, 0);
            this.Controls.SetChildIndex(this.TRespDDD, 0);
            this.Controls.SetChildIndex(this.TRespTelefone, 0);
            this.Controls.SetChildIndex(this.labelControl5, 0);
            this.Controls.SetChildIndex(this.labelControl6, 0);
            this.Controls.SetChildIndex(this.labelControl7, 0);
            this.Controls.SetChildIndex(this.TRespRamal, 0);
            this.Controls.SetChildIndex(this.TRespFax, 0);
            this.Controls.SetChildIndex(this.TRespEMail, 0);
            ((System.ComponentModel.ISupportInitialize)(this.BarManager_F)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.BarAndDockingController_F)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.BindingSource_F)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.StyleController_F)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit1.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TResNome.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TRespDDD.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TRespTelefone.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TRespRamal.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TRespFax.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TRespEMail.Properties)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private DevExpress.XtraEditors.LabelControl labelControl1;
        private DevExpress.XtraEditors.LabelControl labelControl2;
        private DevExpress.XtraEditors.LabelControl labelControl3;
        private DevExpress.XtraEditors.LabelControl labelControl4;
        private DevExpress.XtraEditors.LabelControl labelControl5;
        private DevExpress.XtraEditors.LabelControl labelControl6;
        private DevExpress.XtraEditors.LabelControl labelControl7;
        /// <summary>
        /// 
        /// </summary>
        public DevExpress.XtraEditors.TextEdit TRespDDD;
        /// <summary>
        /// 
        /// </summary>
        public DevExpress.XtraEditors.TextEdit TRespTelefone;
        /// <summary>
        /// 
        /// </summary>
        public DevExpress.XtraEditors.TextEdit TRespRamal;
        /// <summary>
        /// 
        /// </summary>
        public DevExpress.XtraEditors.TextEdit TRespFax;
        /// <summary>
        /// 
        /// </summary>
        public DevExpress.XtraEditors.TextEdit TRespEMail;
        /// <summary>
        /// 
        /// </summary>
        public DevExpress.XtraEditors.TextEdit textEdit1;
        /// <summary>
        /// 
        /// </summary>
        public DevExpress.XtraEditors.TextEdit TResNome;
    }
}
