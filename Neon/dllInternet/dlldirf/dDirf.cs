﻿namespace dlldirf
{


    partial class dDirf
    {
        partial class NotasDataTable
        {
        }

        private dDirfTableAdapters.PLAnocontasTableAdapter pLAnocontasTableAdapter;
        /// <summary>
        /// TableAdapter padrão para tabela: PLAnocontas
        /// </summary>
        public dDirfTableAdapters.PLAnocontasTableAdapter PLAnocontasTableAdapter
        {
            get
            {
                if (pLAnocontasTableAdapter == null)
                {
                    pLAnocontasTableAdapter = new dDirfTableAdapters.PLAnocontasTableAdapter();
                    pLAnocontasTableAdapter.TrocarStringDeConexao();
                };
                return pLAnocontasTableAdapter;
            }
        }

        private dDirfTableAdapters.DescontosCorpoTableAdapter descontosCorpoTableAdapter;
        /// <summary>
        /// TableAdapter padrão para tabela: DescontosCorpo
        /// </summary>
        public dDirfTableAdapters.DescontosCorpoTableAdapter DescontosCorpoTableAdapter
        {
            get
            {
                if (descontosCorpoTableAdapter == null)
                {
                    descontosCorpoTableAdapter = new dDirfTableAdapters.DescontosCorpoTableAdapter();
                    descontosCorpoTableAdapter.TrocarStringDeConexao();
                };
                return descontosCorpoTableAdapter;
            }
        }

        private dDirfTableAdapters.NotasTableAdapter notasTableAdapter;
        /// <summary>
        /// TableAdapter padrão para tabela: Notas
        /// </summary>
        public dDirfTableAdapters.NotasTableAdapter NotasTableAdapter
        {
            get
            {
                if (notasTableAdapter == null)
                {
                    notasTableAdapter = new dDirfTableAdapters.NotasTableAdapter();
                    notasTableAdapter.TrocarStringDeConexao();
                };
                return notasTableAdapter;
            }
        }

        private dDirfTableAdapters.RetencoesTableAdapter retencoesTableAdapter;
        /// <summary>
        /// TableAdapter padrão para tabela: Retencoes
        /// </summary>
        public dDirfTableAdapters.RetencoesTableAdapter RetencoesTableAdapter
        {
            get
            {
                if (retencoesTableAdapter == null)
                {
                    retencoesTableAdapter = new dDirfTableAdapters.RetencoesTableAdapter();
                    retencoesTableAdapter.TrocarStringDeConexao();
                };
                return retencoesTableAdapter;
            }
        }


        private dDirfTableAdapters.CONDOMINIOSTableAdapter cONDOMINIOSTableAdapter;

        /// <summary>
        /// 
        /// </summary>
        public dDirfTableAdapters.CONDOMINIOSTableAdapter CONDOMINIOSTableAdapter
        {
            get
            {
                if (cONDOMINIOSTableAdapter == null)
                {
                    cONDOMINIOSTableAdapter = new dlldirf.dDirfTableAdapters.CONDOMINIOSTableAdapter();
                    cONDOMINIOSTableAdapter.TrocarStringDeConexao();
                };
                return cONDOMINIOSTableAdapter;
            }
        }

        /*
        private dDirfTableAdapters.RecuperaCNPJTableAdapter recuperaCNPJTableAdapter;
        public dDirfTableAdapters.RecuperaCNPJTableAdapter RecuperaCNPJTableAdapter
        {
            get
            {
                if (recuperaCNPJTableAdapter == null)
                {
                    recuperaCNPJTableAdapter = new dlldirf.dDirfTableAdapters.RecuperaCNPJTableAdapter();
                    recuperaCNPJTableAdapter.TrocarStringDeConexao();
                };
                return recuperaCNPJTableAdapter;
            }
        }*/

        /*
        private dDirfTableAdapters.CODCONTableAdapter cODCONTableAdapter;
        public dDirfTableAdapters.CODCONTableAdapter CODCONTableAdapter {
            get {
                if (cODCONTableAdapter == null) {
                    cODCONTableAdapter = new dlldirf.dDirfTableAdapters.CODCONTableAdapter();
                    cODCONTableAdapter.TrocarStringDeConexao();
                };
                return cODCONTableAdapter;
            }
        }*/

        /*
        private dDirfTableAdapters.DIRfTableAdapter dIRfTableAdapter;
        public dDirfTableAdapters.DIRfTableAdapter DIRfTableAdapter {
            get {
                if (dIRfTableAdapter == null)
                {
                    dIRfTableAdapter = new dDirfTableAdapters.DIRfTableAdapter();
                    dIRfTableAdapter.TrocarStringDeConexao();
                }
                return dIRfTableAdapter;
            }
        }*/
        /*
        private dDirfTableAdapters.DIRf1TableAdapter dIRf1TableAdapter;
        public dDirfTableAdapters.DIRf1TableAdapter DIRf1TableAdapter
        {
            get
            {
                if (dIRf1TableAdapter == null)
                {
                    dIRf1TableAdapter = new dDirfTableAdapters.DIRf1TableAdapter();
                    dIRf1TableAdapter.TrocarStringDeConexao();
                }
                return dIRf1TableAdapter;
            }
        }*/

        /*
        private dDirfTableAdapters.HistoricoTableAdapter historicoTableAdapter;
        public dDirfTableAdapters.HistoricoTableAdapter HistoricoTableAdapter {
            get {
                if (historicoTableAdapter == null) {
                    historicoTableAdapter = new dlldirf.dDirfTableAdapters.HistoricoTableAdapter();
                    historicoTableAdapter.TrocarStringDeConexao(VirDB.Bancovirtual.TiposDeBanco.AccessH);
                }
                return historicoTableAdapter;
            }
        }*/
    }
}

namespace dlldirf.dDirfTableAdapters {
    
    
    public partial class PLAnocontasTableAdapter {
    }
}
