using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace dlldirf
{
    public partial class cCNPJ : CompontesBasicos.ComponenteGradeNavegador
    {
        private dlldirf.dDirf dDirf {
            get {
                return dlldirf.cDirf.cDirfST.dDirf;
            }
        }

        public cCNPJ()
        {
            InitializeComponent();            
        }

        private void cCNPJ_cargaInicial(object sender, EventArgs e)
        {
            cNPJDIRFBindingSource.DataSource = dDirf;
            cNPJDIRFBindingSource.DataMember = "CNPJDIRF";
            TableAdapterPrincipal = cNPJDIRFTableAdapter;
            //cNPJDIRFTableAdapter.Fill(dDirf.CNPJDIRF);
        }

        private void cCNPJ_cargaFinal(object sender, EventArgs e)
        {
            
        }

        private void Validar(){
        dDirf.RecuperaCNPJTableAdapter.Fill(dDirf.RecuperaCNPJ);
            foreach (dDirf.RecuperaCNPJRow rowNovo in dDirf.RecuperaCNPJ) {
                if (!rowNovo.IsDIRCNPJNull())
                {
                    DocBacarios.CPFCNPJ NovoCNPJ = new DocBacarios.CPFCNPJ(rowNovo.DIRCNPJ);
                    if (NovoCNPJ.Tipo == DocBacarios.TipoCpfCnpj.INVALIDO)
                        MessageBox.Show("CNPJ inv�lido encontrado:" + rowNovo.DIRCNPJ.ToString());
                    else {
                        try
                        {
                            dDirf.CNPJDIRF.AddCNPJDIRFRow(NovoCNPJ.ToString(), "");
                            cNPJDIRFTableAdapter.Update(dDirf.CNPJDIRF);
                            dDirf.CNPJDIRF.AcceptChanges();
                        }
                        catch { 
                        
                        }
                        if (NovoCNPJ.ToString() != rowNovo.DIRCNPJ)
                            VirMSSQL.TableAdapter.STTableAdapter.ExecutarSQLNonQuery("update DIRF set DIRCNPJ = @P1 where DIRCNPJ = @P2", NovoCNPJ.ToString() , rowNovo.DIRCNPJ);
                    }
                }
            };
        }

        private void simpleButton1_Click(object sender, EventArgs e)
        {
            Validar();
        }

        /*
        public override bool Update_F()
        {
            cNPJDIRFTableAdapter.Update(dDirf.CNPJDIRF);
            dDirf.CNPJDIRF.AcceptChanges();
            return true;
            
        }
        */ 
    }
}

