using System;
using System.IO;
using System.Windows.Forms;
using System.Collections.Generic;
using DocBacarios;


namespace dlldirf
{
    /// <summary>
    /// Componente para gerar DIRF
    /// </summary>
    public partial class cDirf : CompontesBasicos.ComponenteBase
    {
        private static cDirf _cDirfST;

        /// <summary>
        /// Est�tico
        /// </summary>
        public static cDirf cDirfST {
            get {
                if (_cDirfST == null)
                    _cDirfST = new cDirf();
                return _cDirfST;
            }
        }

        private void lePLA()
        {
            string Q2 = CompontesBasicos.dEstacao.dEstacaoSt.GetValor("DirfQ2");
            //string Q3 = CompontesBasicos.dEstacao.dEstacaoSt.GetValor("DirfQ3");
            if (Q2 != "")
                foreach (string Codigo in Q2.Split(new string[] { Environment.NewLine, "\t" }, StringSplitOptions.RemoveEmptyEntries))
                {
                    dDirf.PLAnocontasRow rowPLA = dDirf.PLAnocontas.FindByPLA(Codigo);
                    if (rowPLA != null)
                        rowPLA.USAR = false;
                }
            //memoEdit2.Text = Q2;            
        }

        private void gravaPLA()
        {
            System.Text.StringBuilder Q2 = new System.Text.StringBuilder();
            bool primeriro = true;
            foreach (dDirf.PLAnocontasRow rowPLA in dDirf.PLAnocontas)
                if (!rowPLA.USAR)
                {                    
                    if(!primeriro)
                        Q2.Append("\t");
                    primeriro = false;
                    Q2.Append(rowPLA.PLA);
                }
            CompontesBasicos.dEstacao.dEstacaoSt.SetValor("DirfQ2", Q2.ToString());
        }

        private CadastrosProc.Fornecedores.dFornecedoresLookup dFornecedoresLookup;

        /// <summary>
        /// Construtor
        /// </summary>
        public cDirf()
        {
            InitializeComponent();
            dFornecedoresLookup = new CadastrosProc.Fornecedores.dFornecedoresLookup();
            dFornecedoresLookup.FRNLookupTableAdapter.FillSemRAV(dFornecedoresLookup.FRNLookup);
            bindingSourceFornecedores.DataSource = dFornecedoresLookup;
            dDirf.PLAnocontasTableAdapter.Fill(dDirf.PLAnocontas);
            lePLA();            
        }
        
        //bool ligaIR;     

        private cResp cResp1;

        private void simpleButton5_Click(object sender, EventArgs e)
        {
            try
            {
                gridView1.BeginDataUpdate();
                GerarNovo();
                gravaPLA();
                //CompontesBasicos.dEstacao.dEstacaoSt.SetValor("DirfQ2", memoEdit2.Text);
                //CompontesBasicos.dEstacao.dEstacaoSt.SetValor("DirfQ3", memoEdit3.Text);
                B_Gerar.Enabled = B_filtrar.Enabled = true;
            }
            finally
            {
                gridView1.EndDataUpdate();
            }
        }

        private bool GerarNovoEfetivo(dllDirf2011.DIRF dirf2011, dllDirf2011.dDirf dDirf2011,string CONCodigo,int folha,int ano)
        {
            string codFolha = (folha == 0 ? "" : "_" + folha);
            string Arquivo = string.Format("{0}\\DIRF_{3}_{1}_{2}.txt", caminho, CONCodigo.Replace('.', '_'), codFolha,ano);
            return dirf2011.gerarArquivo(Arquivo, dDirf2011);                   
        }

        private string caminho;
        //private Framework.datasets.dCondominiosAtivos.CONDOMINIOSRow rowCON;

        private dllDirf2011.dDirf PreparoInicial(int ano)
        {
            //if (dDirf2011 == null)
            //    return null;
            dirf2011 = new dllDirf2011.DIRF();
            if (cResp1 == null)
            {
                cResp1 = new cResp();
                if (cResp.ShowModuloSTA(CompontesBasicos.EstadosDosComponentes.PopUp, cResp1) != DialogResult.OK)
                {
                    cResp1 = null;
                    return null;
                }
            }
            CPFCNPJ CPFResp = new CPFCNPJ(cResp1.textEdit1.Text);
            string NomeResp = cResp1.TResNome.Text;
            int DDDResp = 0;
            int.TryParse(cResp1.TRespDDD.Text, out DDDResp);
            int FoneResp = 0;
            int.TryParse(cResp1.TRespTelefone.Text, out FoneResp);
            int RamalResp = 0;
            int.TryParse(cResp1.TRespRamal.Text, out RamalResp);
            int FaxResp = 0;
            int.TryParse(cResp1.TRespFax.Text, out FaxResp);
            string EmailResp = cResp1.TRespEMail.Text;

            caminho = string.Format("{0}\\DIRF\\{1}\\", Path.GetDirectoryName(Application.ExecutablePath), Framework.DSCentral.EMP == 1 ? "SBC" : "SA");
            if (!Directory.Exists(caminho))
                Directory.CreateDirectory(caminho);
            try
            {
                foreach (string nArq in Directory.GetFiles(caminho))
                    File.Delete(nArq);
            }
            catch
            {
                MessageBox.Show(string.Format("Arquivo em uso em {0}", caminho));
            };
            return new dllDirf2011.dDirf()
            {
                NomeResponsavel = NomeResp,
                cpfResponsavel = CPFResp,
                DDD = DDDResp,
                Fone = FoneResp,
                Fax = FaxResp,
                Ramal = RamalResp,
                Email = EmailResp,
                AnoCalendario = ano,
                AnoReferencia = ano + 1
            };
        }

        private dDirf.NotasRow BuscarowNOTA(int NOA)
        {
            dDirf.NotasRow rowNOTA = dDirf.Notas.FindByNOA(NOA);
            if (rowNOTA == null)
            {
                ErrosCPF += string.Format("Nota Faltante Busca: {0}\r\n", NOA);
                //CompontesBasicos.Performance.Performance.PerformanceST.Registra("Carrega Nota Nova", true);
                //dDirf.NotasTableAdapter.FillByNOA(dDirf.Notas, NOA);
                //CompontesBasicos.Performance.Performance.PerformanceST.StackPop();
                //rowNOTA = dDirf.Notas.FindByNOA(NOA);
            }
            return rowNOTA;
        }

        dllDirf2011.dDirf dDirf2011 = null;
        dllDirf2011.DIRF dirf2011 = null;
        int ano;
        string ErrosCPF;

        private void GerarNovo()
        {            
            ErrosCPF = "";

            using (CompontesBasicos.Espera.cEspera Esp = new CompontesBasicos.Espera.cEspera(this))
            {                
                ano = DateTime.Today.Year - 1;
                DateTime DataIEmissao = new DateTime(ano, 1, 1);
                DateTime DataFEmissao = new DateTime(ano, 12, 31);
                DateTime DataICof = new DateTime(ano, 2, 1);
                DateTime DataFCof = new DateTime(ano + 1, 1, 31);
                //dDirf2011 � o dataset que ser� exportado
                /*
                dDirf2011 = PreparoInicial(ano);
                if (dDirf2011 == null)
                    return;             
                dirf2011 = new dllDirf2011.DIRF();
                */

                //dDirf � um dataset local
                dDirf.NotasTableAdapter.ClearBeforeFill = false;
                CompontesBasicos.Performance.Performance.Ativado = false;
                CompontesBasicos.Performance.Performance.PerformanceST.Zerar();
                CompontesBasicos.Performance.Performance.PerformanceST.Registra("In�cio");
                dDirf.Notas.Clear();                
                Esp.Espere("Carregando Notas");                
                CompontesBasicos.Performance.Performance.PerformanceST.Registra("Carrega Notas do ano");
                dDirf.NotasTableAdapter.FillBySEMPLANO(dDirf.Notas, DataIEmissao, DataFEmissao);
                foreach (dDirf.NotasRow rowNotas in dDirf.Notas)
                    rowNotas.MES = rowNotas.NOADataEmissao.Month;
                CompontesBasicos.Performance.Performance.PerformanceST.Registra("Carrega Notas Anteriores COF");
                Esp.Espere("Carregando Notas Complemento Cofins");
                dDirf.NotasTableAdapter.FillByAnteriores(dDirf.Notas, DataICof, DataFCof,5,DataIEmissao);
                CompontesBasicos.Performance.Performance.PerformanceST.Registra("Notas INSS");
                Esp.Espere("Carregando Notas Complemento INSS");
                //inclui as notas das isen��es que n�o tem fornecedor !!
                //A inclus�o � feita pelo c�digo 9 que � 20% e sempre � recolhido ja o valor ser� pelo codigo 8
                dDirf.DescontosCorpoDataTable Descontos = dDirf.DescontosCorpoTableAdapter.GetData(DataICof, DataFCof);
                
                foreach (dDirf.DescontosCorpoRow rowSindico in Descontos)
                {
                    //if ((rowSindico.NOA == 1134336) || (rowSindico.NOA == 1116852) || (rowSindico.NOA == 1066874))
                    //    Console.WriteLine(string.Format("carregada nota {0} *****************************************************************", rowSindico.NOA));
                    dDirf.NotasRow rowNOTA = dDirf.Notas.FindByNOA(rowSindico.NOA);
                    if (rowNOTA == null)
                    {
                        rowNOTA = dDirf.Notas.NewNotasRow();
                        if (rowSindico.IsCONCnpjNull())
                        {
                            MessageBox.Show(string.Format("Erro: condominio {0} sem CNPJ",rowSindico.CONCodigo));
                            continue;
                        }
                        rowNOTA.CONCnpj = rowSindico.CONCnpj;
                        rowNOTA.CONCodigo = rowSindico.CONCodigo;
                        if (rowSindico.IsCONCodigoFolha1Null())
                            rowNOTA.CONCodigoFolha1 = 0;
                        else
                            rowNOTA.CONCodigoFolha1 = rowSindico.CONCodigoFolha1;
                        rowNOTA.CONNome = rowSindico.CONNome;
                        if (rowSindico.IscpfRecebedorNull())
                        {
                            ErrosCPF += string.Format("SEM CPF Condominio:  {0} - {1}\r\n    Nome: {2}\r\n", rowSindico.CONCodigo, rowSindico.CONNome, rowSindico.PESNome);
                            continue;
                        }
                        rowNOTA.FRNCnpj = rowSindico.cpfRecebedor;
                        rowNOTA.FRNNome = rowSindico.NomeRecebedor;
                        rowNOTA.NOA = rowSindico.NOA;
                        rowNOTA.NOA_CON = rowSindico.NOA_CON;
                        rowNOTA.NOA_PLA = rowSindico.NOA_PLA;                        
                        //rowNOTA.NOA_FRN = 
                        rowNOTA.NOADataEmissao = rowSindico.NOADataEmissao;
                        rowNOTA.MES = rowSindico.NOADataEmissao.Month;
                        rowNOTA.NOANumero = rowSindico.NOANumero;
                        rowNOTA.NOAServico = rowSindico.NOAServico;
                        rowNOTA.NOATotal = rowSindico.PAGValor / 0.2M;
                        rowNOTA.PESCpfCnpj = rowSindico.PESCpfCnpj;
                        rowNOTA.PESNome = rowSindico.PESNome;                        
                        dDirf.Notas.AddNotasRow(rowNOTA);
                    }
                }
                
                dDirf.Retencoes.Clear();
                CompontesBasicos.Performance.Performance.PerformanceST.Registra("IR");
                dDirf.RetencoesTableAdapter.Fill(dDirf.Retencoes,1, DataICof, DataFCof);
                Esp.AtivaGauge(dDirf.Retencoes.Count);
                Esp.Espere("IR");
                int Pos = 0;                
                foreach (dDirf.RetencoesRow row in dDirf.Retencoes)
                {
                    Pos++;                    
                    Esp.GaugeTempo(Pos);                                            
                    dDirf.NotasRow rowNOTA = BuscarowNOTA(row.NOA);
                    if (rowNOTA != null)
                    {
                        rowNOTA.IR = row.PAGValor;
                        //rowNOTA.MES = row.PAGVencimento.AddMonths(-1).Month;
                    }
                };

                CompontesBasicos.Performance.Performance.PerformanceST.Registra("Cof");
                Esp.Espere("PIS/COFINS/CSSLL");
                dDirf.RetencoesTableAdapter.Fill(dDirf.Retencoes,5, DataICof , DataFCof);                
                Esp.AtivaGauge(dDirf.Retencoes.Count);
                Esp.Gauge(0);                
                Pos = 0;                
                foreach (dDirf.RetencoesRow row in dDirf.Retencoes)
                {
                    Pos++;
                    Esp.GaugeTempo(Pos);                                                
                    dDirf.NotasRow rowNOTA = BuscarowNOTA(row.NOA);
                    if (rowNOTA != null)
                    {
                        rowNOTA.Cof = row.PAGValor;                        
                        rowNOTA.MESCOF = row.PAGVencimento.AddMonths(-1).Month;
                    }
                };

                CompontesBasicos.Performance.Performance.PerformanceST.Registra("INSS");
                Esp.Espere("INSS");
                dDirf.RetencoesTableAdapter.FillBySEMFornecedor(dDirf.Retencoes, 8, new DateTime(ano, 1, 21), new DateTime(ano + 1, 1, 20));
                Esp.AtivaGauge(dDirf.Retencoes.Count);
                Esp.Gauge(0);
                Pos = 0;
                foreach (dDirf.RetencoesRow row in dDirf.Retencoes)
                {
                    Pos++;
                    Esp.GaugeTempo(Pos);
                    //if ((row.NOA == 1134336) || (row.NOA == 1116852) || (row.NOA == 1066874))
                    //    Console.WriteLine(string.Format("BUSCA nota {0} *****************************************************************",row.NOA));
                    dDirf.NotasRow rowNOTA = BuscarowNOTA(row.NOA);
                    if (rowNOTA != null)                        
                    {
                        rowNOTA.INSS = row.PAGValor;
                        rowNOTA.MES = row.PAGVencimento.AddMonths(-1).Month;
                    }
                };                                                                
            }

            CompontesBasicos.Performance.Performance.PerformanceST.Relatorio(true, true);
            //File.WriteAllText("D:\\lixo\\NOAs.txt", ErrosCPF);
        }

        private dllDirf2011.dDirf.IRCompRow rowComplementar(dllDirf2011.dDirf.IRRow rowPrincipal, string Tipo)
        {
            dllDirf2011.dDirf.IRCompRow retorno = dDirf2011.IRComp.FindByCNPJCodigoTipo(rowPrincipal.CNPJ, rowPrincipal.Codigo, Tipo);
            if (retorno == null)
            {
                retorno = dDirf2011.IRComp.NewIRCompRow();
                retorno.CNPJ = rowPrincipal.CNPJ;
                retorno.Codigo = rowPrincipal.Codigo;
                retorno.Tipo = Tipo;
                for (int i = 1; i < 13; i++)
                    retorno[string.Format("V{0}", i)] = 0;
                dDirf2011.IRComp.AddIRCompRow(retorno);
            }
            return retorno;
        }

        private void Filtra()
        {
            List<dDirf.NotasRow> Apagar = new List<dDirf.NotasRow>();
            foreach (dDirf.NotasRow rowNota in dDirf.Notas)
            {
                if (!rowNota.PLAnocontasRow.USAR)
                    Apagar.Add(rowNota);
            }
            foreach (dDirf.NotasRow rowNota in Apagar)
                rowNota.Delete();
            dDirf.Notas.AcceptChanges();
        }

        private SortedList<string, List<dDirf.NotasRow>> AgrupaNotaPorCond(CompontesBasicos.Espera.cEspera Esp)
        {
            CompontesBasicos.Performance.Performance.PerformanceST.Registra("Distribui por Condom�nios");
            SortedList<string, List<dDirf.NotasRow>> conds = new SortedList<string, List<dDirf.NotasRow>>();
            List<dDirf.NotasRow> linhas;
            int Pos = 0;
            Esp.AtivaGauge(dDirf.Notas.Count);
            foreach (dDirf.NotasRow rowNotas in dDirf.Notas)
            {
                Esp.GaugeTempo(Pos++);                
                if (conds.ContainsKey(rowNotas.CONCodigo))
                    linhas = conds[rowNotas.CONCodigo];
                else
                {
                    linhas = new List<dDirf.NotasRow>();
                    conds.Add(rowNotas.CONCodigo, linhas);
                };
                linhas.Add(rowNotas);
            }
            return conds;
        }

        private void GerarArquivos()
        {
            string Relatorio = string.Format("Relat�rio\r\n{0}\r\n", ErrosCPF);
            using (CompontesBasicos.Espera.cEspera Esp = new CompontesBasicos.Espera.cEspera(this))
            {
                Esp.Espere("Aplicando filtro");
                Application.DoEvents();
                Filtra();
                Esp.Espere("Preparo Inicial");
                dDirf2011 = PreparoInicial(ano);
                if (dDirf2011 == null)
                    return;
                SortedList<string, List<dDirf.NotasRow>> conds = AgrupaNotaPorCond(Esp);
                bool comErro = false;
                //para cada condominio grava o arquivo
                Esp.Espere("Gerando");
                Esp.AtivaGauge(conds.Count);
                int Pos = 0;
                foreach (List<dDirf.NotasRow> linhas1 in conds.Values)
                {
                    Esp.GaugeTempo(Pos++);
                    dDirf2011.IR.Clear();
                    comErro = false;
                    dDirf.NotasRow row = (dDirf.NotasRow)linhas1[0];
                    dDirf2011.NomeDeclarante = row.PESNome;
                    Relatorio += string.Format("\r\n{0}: ", row.CONCodigo);
                    //if (row.CONCodigo == "MICHEL")
                    //    Console.WriteLine("Acompanhar");
                    if (row.IsPESCpfCnpjNull())
                    {
                        Relatorio += "Erro - sem CPF do s�ndico";
                        comErro = true;
                        continue;
                    };
                    CPFCNPJ cpfDeclarante = new CPFCNPJ(row.PESCpfCnpj);
                    if (cpfDeclarante.Tipo != TipoCpfCnpj.CPF)
                    {
                        Relatorio += string.Format("Erro - CPF do respons�vel inv�lido :{0} ({1})", cpfDeclarante, cpfDeclarante.Tipo);
                        comErro = true;
                        continue;
                    }
                    if (row.IsCONCnpjNull())
                    {
                        Relatorio += "Erro - sem CNPJ do condom�nio";
                        comErro = true;
                        continue;
                    }
                    if (!comErro)
                    {
                        dDirf2011.cpfDeclarante = cpfDeclarante;
                        dDirf2011.cnpjDeclarante = new CPFCNPJ(row.CONCnpj);
                        dDirf2011.NomeDeclarante = row.CONNome;
                    }
                    else
                        continue;

                    foreach (dDirf.NotasRow rowNotas in linhas1)
                    {
                        CPFCNPJ cnpj = new CPFCNPJ(rowNotas.FRNCnpj);

                        if (row.CONCnpj == rowNotas.FRNCnpj)
                            continue;
                        if (cnpj.Valor == 0)
                            continue;
                        //Reten��o do IR
                        //Aten��o inclui notas sem reten��o
                        //Inclui o INSS
                        int Codigo = cnpj.Tipo == TipoCpfCnpj.CNPJ ? 1708 : 588;
                        dllDirf2011.dDirf.IRRow rowIR = dDirf2011.IR.FindByCNPJCodigo(cnpj.Valor, Codigo);  //1708 0588
                        if (rowIR == null)
                        {
                            rowIR = dDirf2011.IR.NewIRRow();
                            rowIR.CNPJ = cnpj.Valor;
                            rowIR.Codigo = Codigo;
                            rowIR.Nome = rowNotas.FRNNome.Replace(".", "");
                            dDirf2011.IR.AddIRRow(rowIR);
                            for (int i = 1; i <= 12; i++)
                            {
                                rowIR["V" + i] = 0;
                                rowIR["I" + i] = 0;
                            }
                        }

                        if (!rowNotas.IsMESNull())
                        {
                            rowIR["V" + rowNotas.MES] = (decimal)rowIR["V" + rowNotas.MES] + rowNotas.NOATotal;
                            rowIR["I" + rowNotas.MES] = (decimal)rowIR["I" + rowNotas.MES] + (rowNotas.IsIRNull() ? 0 : rowNotas.IR);
                        }
                        if (cnpj.Tipo == TipoCpfCnpj.CNPJ)
                        {
                            rowIR = dDirf2011.IR.FindByCNPJCodigo(cnpj.Valor, 5952);
                            if (rowIR == null)
                            {
                                rowIR = dDirf2011.IR.NewIRRow();
                                rowIR.CNPJ = cnpj.Valor;
                                rowIR.Codigo = 5952;
                                rowIR.Nome = rowNotas.FRNNome; //row.FRNNome;
                                dDirf2011.IR.AddIRRow(rowIR);
                                for (int i = 1; i <= 12; i++)
                                {
                                    rowIR["V" + i] = 0;
                                    rowIR["I" + i] = 0;
                                }
                            }

                            if (!rowNotas.IsMESNull())
                                rowIR["V" + rowNotas.MES] = (decimal)rowIR["V" + rowNotas.MES] + rowNotas.NOATotal;
                            if (!rowNotas.IsMESCOFNull())
                                rowIR["I" + rowNotas.MESCOF] = (decimal)rowIR["I" + rowNotas.MESCOF] + (rowNotas.IsCofNull() ? 0 : rowNotas.Cof);
                        }
                        else
                        {
                            if (!rowNotas.IsINSSNull())
                            {
                                dllDirf2011.dDirf.IRCompRow rowC = rowComplementar(rowIR, "RTPO");
                                rowC[string.Format("V{0}", rowNotas.MES)] = (decimal)rowC[string.Format("V{0}", rowNotas.MES)] + rowNotas.INSS;
                            }
                        }
                    }

                    CompontesBasicos.Performance.Performance.PerformanceST.Registra("Remove notas vazias");
                    //Remover quem nao tem rete��o e nao tem um valor m�nimo

                    List<dllDirf2011.dDirf.IRRow> Remover = new List<dllDirf2011.dDirf.IRRow>();
                    foreach (dllDirf2011.dDirf.IRRow rowIR in dDirf2011.IR)
                    {
                        if (rowIR.GetIRCompRows().Length != 0)
                        {
                            if (!chPFabaixo.Checked)
                                continue;                         
                        }
                        bool remover = true;
                        decimal Total = 0;
                        CPFCNPJ cnpj = new CPFCNPJ(rowIR.CNPJ);
                        //if (cnpj.Valor == 11086542827)
                        //    Console.WriteLine("Acompanhar");
                        for (int i = 1; i <= 12; i++)
                        {
                            if ((decimal)rowIR["I" + i.ToString()] != 0)
                            {
                                //se tem reten��o n�o remove para PJ ou   PF se n�o marcar o chPFabaixo
                                if ((cnpj.Tipo == TipoCpfCnpj.CNPJ) || (!chPFabaixo.Checked))
                                {
                                    remover = false;
                                    continue;
                                }
                            };
                            Total += (decimal)rowIR["V" + i.ToString()];
                        };



                        if (cnpj.Tipo == TipoCpfCnpj.CNPJ)
                        {
                            if ((rowIR.Codigo == 5952) && (ch5952PJ.Checked) && (Total > ValorPJ.Value))
                                remover = false;
                            if ((rowIR.Codigo == 1708) && (ch1708.Checked) && (Total > ValorPJ.Value))
                                remover = false;
                        }
                        else
                        {
                            if (Total > ValorPF.Value)
                                remover = false;
                        }
                        if (remover)
                            Remover.Add(rowIR);
                    }
                    foreach (dllDirf2011.dDirf.IRRow rowIR in Remover)
                        rowIR.Delete();
                    Relatorio += string.Format("Lan�amentos: {0}\t", dDirf2011.IR.Count);
                    CompontesBasicos.Performance.Performance.PerformanceST.Registra("Gera", true);
                    if (dDirf2011.IR.Count > 0)
                        Relatorio += (GerarNovoEfetivo(dirf2011, dDirf2011, row.CONCodigo, row.IsCONCodigoFolha1Null() ? 0 : row.CONCodigoFolha1, ano) ? "ok" : "ERRO na grava��o do arquivo.");
                    CompontesBasicos.Performance.Performance.PerformanceST.StackPop();
                }
            };
            cRelatorioSimples Rel = new cRelatorioSimples();
            Rel.Mostra(string.Format("Arquivos gerados na pasta: {0}\r\n\r\n{1}", caminho, Relatorio));
        }

        private void simpleButton1_Click(object sender, EventArgs e)
        {
            SaveFileDialog SF = new SaveFileDialog() { DefaultExt = ".xls" };
            if (SF.ShowDialog() == DialogResult.OK)
                gridView1.ExportToXls(SF.FileName);
        }

        private void repositoryItemButtonEdit2_ButtonClick(object sender, DevExpress.XtraEditors.Controls.ButtonPressedEventArgs e)
        {
            using (CompontesBasicos.Espera.cEspera ESP = new CompontesBasicos.Espera.cEspera(this))
            {
                ESP.Espere("Procurando nota...");
                if (gridControl1.FocusedView != null)
                {
                    DevExpress.XtraGrid.Views.Grid.GridView GV = (DevExpress.XtraGrid.Views.Grid.GridView)gridControl1.FocusedView;
                    dDirf.NotasRow rowNotas = (dDirf.NotasRow)GV.GetFocusedDataRow();
                    if (rowNotas != null)
                        CompontesBasicos.FormPrincipalBase.FormPrincipal.ShowMoludoCampos("ContaPagar.cNotasCampos", "NOTA", rowNotas.NOA, false, null, CompontesBasicos.EstadosDosComponentes.JanelasAtivas);
                }
            }
        }

        private void simpleButton2_Click(object sender, EventArgs e)
        {
            try
            {
                gridView1.BeginDataUpdate();
                GerarArquivos();
                gravaPLA();
            }
            finally
            {
                gridView1.EndDataUpdate();
            }            
        }

        private void simpleButton2_Click_1(object sender, EventArgs e)
        {
            using (CompontesBasicos.Espera.cEspera ESP = new CompontesBasicos.Espera.cEspera(this))
            {
                Application.DoEvents();
                try
                {
                    gridView1.BeginDataUpdate();
                    Filtra();                    
                }
                finally
                {
                    gridView1.EndDataUpdate();
                }
            }
        }

        private void gridView2_RowUpdated(object sender, DevExpress.XtraGrid.Views.Base.RowObjectEventArgs e)
        {
            System.Data.DataRowView DRV = (System.Data.DataRowView)e.Row;
            dDirf.PLAnocontasRow rowPLA = (dDirf.PLAnocontasRow)DRV.Row;
            if (rowPLA.USAR)
            {
                B_filtrar.Enabled = false;
                B_Gerar.Enabled = false;
            }
        }

        private void checkEdit1_CheckedChanged(object sender, EventArgs e)
        {
            gridControl2.Visible = !checkEdit1.Checked;
        }
        
    }
}

