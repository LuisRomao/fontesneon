
namespace dlldirf
{
    /// <summary>
    /// 
    /// </summary>
    public partial class cRelatorioSimples : CompontesBasicos.ComponenteBase
    {
        /// <summary>
        /// 
        /// </summary>
        public cRelatorioSimples()
        {
            InitializeComponent();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="Conteudo"></param>
        public void Mostra(string Conteudo)
        {
            memoEdit1.Text = Conteudo;
            VirShowModulo(CompontesBasicos.EstadosDosComponentes.PopUp);
        }
    }
}
