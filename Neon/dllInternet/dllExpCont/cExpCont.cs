﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using CompontesBasicos;

namespace dllExpCont
{
    /// <summary>
    /// Exportar dados contabeis
    /// </summary>
    public partial class cExpCont : ComponenteBase
    {
        /// <summary>
        /// Construtor
        /// </summary>
        public cExpCont()
        {
            InitializeComponent();
            dExpCont1.APARTAMENTOSTableAdapter.FillByCON(dExpCont1.APARTAMENTOS, 770);            
            dExpCont1.APARTAMENTOS.DefaultView.Sort = "BLOCodigo,APTNumero";
        }

        /*
        private DataTable _DT;

        private DataTable DT
        {
            get
            {
                if (_DT == null)
                {
                    string comando =
"SELECT        BLOCOS.BLOCodigo, APARTAMENTOS.APTNumero, APARTAMENTOS.APT\r\n" +
"FROM            BLOCOS INNER JOIN\r\n" +
"                         APARTAMENTOS ON BLOCOS.BLO = APARTAMENTOS.APT_BLO\r\n" +
"WHERE        (BLOCOS.BLO_CON = 770);";
                    _DT = TableAdapter.ST().BuscaSQLTabela(comando);
                    _DT.DefaultView.Sort = "BLOCodigo,APTNumero";

                }
                return _DT;
            }
        }*/

        private void simpleButton1_Click(object sender, EventArgs e)
        {
            VirInput.Importador.cImportador Imp = new VirInput.Importador.cImportador("EXP");
            Imp.AddColunaImp("BLOAPT", "Bloco APT", typeof(string), true);
            //Imp.AddColunaImp("APTNumero", "Apartamento", typeof(string), true);
            Imp.AddColunaImp("Codigo", "Codigo", typeof(int), true);
            Imp.Carrega();
            System.Text.StringBuilder SB = new StringBuilder();
            int N = 0;
            int I = 0;
            int R = 0;            
            if (Imp.ShowEmPopUp() == DialogResult.OK)
            {
                foreach (DataRow DR in Imp._DataTable.Rows)
                {
                    if ((bool)DR["OK"])
                    {
                        int Codigo = (int)DR["Codigo"];
                        string Tratar = (string)DR["BLOAPT"];
                        string[] partes = Tratar.Split(' ');                        
                        //int key = DT.DefaultView.Find(partes);
                        int key = dExpCont1.APARTAMENTOS.DefaultView.Find(partes);
                        if (key < 0)
                        {
                            N++;
                            SB.AppendFormat("N Apartamento não encontrado : {0} {1}\r\n", partes);
                            continue;
                        }
                        //DataRow DR1 = DT.DefaultView[key].Row;
                        dExpCont.APARTAMENTOSRow rowAPT = (dExpCont.APARTAMENTOSRow)dExpCont1.APARTAMENTOS.DefaultView[key].Row;
                        if (rowAPT.IsAPTCodExterno1Null())
                        {
                            I++;
                            SB.AppendFormat("IP Incluido : {0} {1} - {2}\r\n", partes[0], partes[1], Codigo);
                            rowAPT.APTCodExterno1 = Codigo;
                            dExpCont1.APARTAMENTOSTableAdapter.Update(rowAPT);
                        }
                        else
                        {
                            R++;
                            SB.AppendFormat("RP Repetido : {0} {1} - {2}\r\n", partes[0], partes[1], Codigo);                                                        
                        }

                        /*
                        //int APT = (int)DR1["APT"];
                        //int key1 = dExpCont1.EXportarClientes.DefaultView.Find(new object[] { partes[0], partes[1], true });
                        int key1 = dExpCont1.EXportarClientes.DefaultView.Find(partes);
                        if (key1 < 0)
                        {
                            I++;
                            SB.AppendFormat("IP Incluido : {0} {1} - {2}\r\n", partes[0], partes[1], Codigo);
                            dExpCont.EXportarClientesRow rowEXC = dExpCont1.EXportarClientes.NewEXportarClientesRow();
                            rowEXC.EXC_APT = APT;
                            rowEXC.EXCCod = Codigo;                            
                            rowEXC.BLOCodigo = partes[0];
                            rowEXC.APTNumero = partes[1];
                            dExpCont1.EXportarClientes.AddEXportarClientesRow(rowEXC);
                            dExpCont1.EXportarClientesTableAdapter.Update(rowEXC);
                        }
                        else
                        {
                            dExpCont.EXportarClientesRow rowProp = (dExpCont.EXportarClientesRow)dExpCont1.EXportarClientes.DefaultView[key1].Row;
                            R++;
                            SB.AppendFormat("RP Repetido : {0} {1} - {2}\r\n", partes[0], partes[1], Codigo);                                                        
                        }*/
                    }
                }
                SB.AppendFormat("N = {0} I = {1} R = {2}", N, I, R);
                MessageBox.Show(string.Format("N = {0} I = {1} R = {2}", N, I, R));
                System.Windows.Forms.Clipboard.SetText(SB.ToString());
            }
        }

        private void simpleButton2_Click(object sender, EventArgs e)
        {
            if ((dateEditI.EditValue == null) || (dateEditF.EditValue == null))
                return;
            switch ((int)radioGroup1.EditValue)
            {
                case 0:
                    dExpCont1.TitulosTableAdapter.FillEmitidos(dExpCont1.Titulos, 770, dateEditI.DateTime, dateEditF.DateTime);
                    break;
                case 1:
                    dExpCont1.TitulosTableAdapter.FillRecebidos(dExpCont1.Titulos, 770, dateEditI.DateTime, dateEditF.DateTime);                    
                    break;
                case 2:
                    dExpCont1.TitulosTableAdapter.FillInad(dExpCont1.Titulos, 770, dateEditI.DateTime);
                    break;
            }
            List<dExpCont.TitulosRow> Apagar = new List<dExpCont.TitulosRow>();
            foreach (dExpCont.TitulosRow rowTeste in dExpCont1.Titulos)
                if (rowTeste.BOD_PLA.StartsWith("9"))
                    Apagar.Add(rowTeste);
            foreach (dExpCont.TitulosRow rowTeste in Apagar)
                rowTeste.Delete();
            if ((int)radioGroup1.EditValue == 1)
                SomaJuros();
            dExpCont1.Titulos.AcceptChanges();
            BotGrava.Enabled = true;
            BotExporta.Enabled = true;
        }

        /*
        private DataView _EXportarClientes_DV_APT;

        private DataView EXportarClientes_DV_APT
        {
            get
            {
                if (_EXportarClientes_DV_APT == null)
                {
                    _EXportarClientes_DV_APT = new DataView(dExpCont1.EXportarClientes);
                    _EXportarClientes_DV_APT.Sort = "EXC_APT";
                }
                return _EXportarClientes_DV_APT_P;
            }
        }
        */
        private DataView _APARTAMENTOS_DV_APTCodExterno1;

        private DataView APARTAMENTOS_DV_APTCodExterno1
        {
            get
            {
                if (_APARTAMENTOS_DV_APTCodExterno1 == null)
                {
                    _APARTAMENTOS_DV_APTCodExterno1 = new DataView(dExpCont1.APARTAMENTOS);
                    _APARTAMENTOS_DV_APTCodExterno1.Sort = "APTCodExterno1";
                }
                return _APARTAMENTOS_DV_APTCodExterno1;
            }
        }

        
        private DataView _TITULOS_DV_BOD_PLA;

        private DataView TITULOS_DV_BOD_PLA
        {
            get
            {
                if (_TITULOS_DV_BOD_PLA == null)
                {
                    _TITULOS_DV_BOD_PLA = new DataView(dExpCont1.Titulos);
                    _TITULOS_DV_BOD_PLA.Sort = "BOD_PLA";
                }
                return _TITULOS_DV_BOD_PLA;
            }
        }

        private DataView _TITULOS_DV_BOL_PLA;

        private DataView TITULOS_DV_BOL_PLA
        {
            get
            {
                if (_TITULOS_DV_BOL_PLA == null)
                {
                    _TITULOS_DV_BOL_PLA = new DataView(dExpCont1.Titulos);
                    _TITULOS_DV_BOL_PLA.Sort = "BOL,BOD_PLA";
                }
                return _TITULOS_DV_BOL_PLA;
            }
        }

        private DataView _TITULOS_DV_BOL;

        private DataView TITULOS_DV_BOL
        {
            get
            {
                if (_TITULOS_DV_BOL == null)
                {
                    _TITULOS_DV_BOL = new DataView(dExpCont1.Titulos);
                    _TITULOS_DV_BOL.Sort = "BOL";
                }
                return _TITULOS_DV_BOL;
            }
        }

        private void SomaJuros()
        {
            List<dExpCont.TitulosRow> Apagar = new List<dExpCont.TitulosRow>();
            System.Data.DataRowView[] DRVs = TITULOS_DV_BOD_PLA.FindRows("120001");
            foreach (System.Data.DataRowView DRV in DRVs)
            {
                dExpCont.TitulosRow rowSomar = null;
                dExpCont.TitulosRow rowTitulo = (dExpCont.TitulosRow)DRV.Row;
                int irowCondominio = TITULOS_DV_BOL_PLA.Find(new object[] { rowTitulo.BOL , "101000" });
                if (irowCondominio >= 0)
                    rowSomar = (dExpCont.TitulosRow)TITULOS_DV_BOL_PLA[irowCondominio].Row;
                else
                {
                    System.Data.DataRowView[] DRVsCandidatos = TITULOS_DV_BOL.FindRows(rowTitulo.BOL );
                    for (int i = 0; i < DRVsCandidatos.Length; i++)
                    {
                        dExpCont.TitulosRow rowCandidato = (dExpCont.TitulosRow)DRVsCandidatos[i].Row;
                        if (rowCandidato.BOD_PLA != "120001")
                        {
                            rowSomar = rowCandidato;
                            break;
                        }
                    }
                }
                if (rowSomar != null)
                {
                    rowSomar.Juros = rowTitulo.BODValor;
                    Apagar.Add(rowTitulo);
                }
            }
            foreach (dExpCont.TitulosRow rowApagar in Apagar)
                rowApagar.Delete();
        }

        private bool CodigoJaCadastrado(int Codigo)
        {
            return APARTAMENTOS_DV_APTCodExterno1.Find(Codigo) > 0;
        }

        private void gridView2_RowUpdated(object sender, DevExpress.XtraGrid.Views.Base.RowObjectEventArgs e)
        {
            dExpCont.TitulosRow rowTIT = (dExpCont.TitulosRow) ((DataRowView)e.Row).Row;
            if (
                (rowTIT == null) 
                   || 
                (rowTIT.IsAPTCodExterno1Null()) 
                   ||
                (
                  (rowTIT["APTCodExterno1", DataRowVersion.Original] != DBNull.Value) 
                     &&
                  (rowTIT.APTCodExterno1 == (int)rowTIT["APTCodExterno1", DataRowVersion.Original]))
                )
                return;
            if (CodigoJaCadastrado(rowTIT.APTCodExterno1))
            {
                rowTIT.RowError = "Código já existe";
                return;
            }

            dExpCont.APARTAMENTOSRow rowAPT = rowTIT.APARTAMENTOSRow;
            rowAPT.APTCodExterno1 = rowTIT.APTCodExterno1;                        
            dExpCont1.APARTAMENTOSTableAdapter.Update(rowAPT);
            rowTIT.AcceptChanges();
        }

        private void gridView2_RowCellStyle(object sender, DevExpress.XtraGrid.Views.Grid.RowCellStyleEventArgs e)
        {
            if (e.Column.Caption == colAPTCodExterno11.Caption)
            {
                dExpCont.TitulosRow row = (dExpCont.TitulosRow)gridView2.GetDataRow(e.RowHandle);
                //if ((row!=null) && (e.CellValue == DBNull.Value))
                if (row != null) 
                    if (row.IsAPTCodExterno1Null())
                        e.Appearance.BackColor = Color.Red;
            }
        }

        private string caminho
        {
            get 
            {
                string retorno = string.Format("{0}\\EXP\\", System.IO.Path.GetDirectoryName(Environment.CommandLine.Replace("\"", "")));
                if (!System.IO.Directory.Exists(retorno))
                    System.IO.Directory.CreateDirectory(retorno);
                return retorno;
            }
        }

        private void simpleButton3_Click(object sender, EventArgs e)
        {            
            string nome = "";
            switch ((int)radioGroup1.EditValue)
            {
                case 0:
                    nome = string.Format("EMITIDAS {0:dd_MM_yyyy} {1:dd_MM_yyyy}", dateEditI.DateTime, dateEditF.DateTime);
                    break;
                case 1:
                    nome = string.Format("QUITADAS {0:dd_MM_yyyy} {1:dd_MM_yyyy}", dateEditI.DateTime, dateEditF.DateTime);
                    break;
                case 2:
                    nome = string.Format("ABERTAS {0:dd_MM_yyyy}", dateEditI.DateTime);
                    break;
            }
            string Arquivo = string.Format("{0}{1}.txt", caminho, nome);
            using (DataTable TabelaSaida = new DataTable())
            {
                ArquivosTXT.cTabelaTXT ArqEmitidas = new ArquivosTXT.cTabelaTXT(TabelaSaida, ArquivosTXT.ModelosTXT.layout);
                ArqEmitidas.PontoDecima = ",";
                ArqEmitidas.Formatodata = ArquivosTXT.FormatoData.ddMMyyyy;
                ArqEmitidas.RegistraMapa(TabelaSaida.Columns.Add("CODIGO", typeof(string)), 1, 10);
                ArqEmitidas.RegistraMapa(TabelaSaida.Columns.Add("CODIGOCLIENTE", typeof(int)), 11, 6);
                ArqEmitidas.RegistraMapa(TabelaSaida.Columns.Add("EMISSAO", typeof(DateTime)), 17, 8);
                ArqEmitidas.RegistraMapa(TabelaSaida.Columns.Add("VENCTO", typeof(DateTime)), 25, 8);
                DataColumn ColunaPAGAMENTO = TabelaSaida.Columns.Add("PAGAMENTO", typeof(DateTime));
                if ((int)radioGroup1.EditValue == 1)
                    ArqEmitidas.RegistraMapa(ColunaPAGAMENTO, 33, 8);
                ArqEmitidas.RegistraMapa(TabelaSaida.Columns.Add("VALOR", typeof(decimal)), 41, 9);
                ArqEmitidas.RegistraMapa(TabelaSaida.Columns.Add("Juros", typeof(decimal)), 50, 9);
                ArqEmitidas.RegistraMapa(TabelaSaida.Columns.Add("OBS", typeof(string)), 59, 50);


                foreach (dExpCont.TitulosRow rowExp in dExpCont1.Titulos)
                {
                    if (rowExp.IsAPTCodExterno1Null())
                    {
                        //MessageBox.Show(string.Format("ERRO: Apartamento sem código externo:{0} {1} {2}", rowExp.BLOCodigo, rowExp.APTNumero, rowExp.BOLProprietario ? "P" : "I"));
                        //break;
                        continue;
                    }
                    TabelaSaida.Rows.Add(rowExp.BOD,
                                         rowExp.APTCodExterno1,
                                         rowExp.BOLEmissao,
                                         rowExp.BOLVencto,
                                         rowExp.IsBOLPagamentoNull() ? (object)DBNull.Value : rowExp.BOLPagamento,
                                         rowExp.BODValor,
                                         rowExp.Juros,
                                         rowExp.BODMensagem);
                }
                ArqEmitidas.Salva(Arquivo);
            };
            Clipboard.SetText(Arquivo);
            MessageBox.Show(string.Format("Salvou em: {0}\r\nNome na área de transferência",Arquivo));            
        }

        private void radioGroup1_EditValueChanged(object sender, EventArgs e)
        {
            BotGrava.Enabled = false;
            BotExporta.Enabled = false;
        }

        private void simpleButton3_Click_1(object sender, EventArgs e)
        {
            string nome = "";
            switch ((int)radioGroup1.EditValue)
            {
                case 0:
                    nome = string.Format("EMITIDAS {0:dd_MM_yyyy} {1:dd_MM_yyyy}", dateEditI.DateTime, dateEditF.DateTime);
                    break;
                case 1:
                    nome = string.Format("QUITADAS {0:dd_MM_yyyy} {1:dd_MM_yyyy}", dateEditI.DateTime, dateEditF.DateTime);
                    break;
                case 2:
                    nome = string.Format("ABERTAS {0:dd_MM_yyyy}", dateEditI.DateTime);
                    break;
            }
            string Arquivo = string.Format("{0}{1}.xls", caminho, nome);
            gridView2.ExportToXls(Arquivo);
            Clipboard.SetText(Arquivo);
            MessageBox.Show(string.Format("Salvou em: {0}\r\nNome na área de transferência", Arquivo));
        }

    }
}
