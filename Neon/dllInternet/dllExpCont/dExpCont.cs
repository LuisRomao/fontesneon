﻿namespace dllExpCont {
    
    
    public partial class dExpCont {
        private dExpContTableAdapters.APARTAMENTOSTableAdapter aPARTAMENTOSTableAdapter;
        /// <summary>
        /// TableAdapter padrão para tabela: APARTAMENTOS
        /// </summary>
        public dExpContTableAdapters.APARTAMENTOSTableAdapter APARTAMENTOSTableAdapter
        {
            get
            {
                if (aPARTAMENTOSTableAdapter == null)
                {
                    aPARTAMENTOSTableAdapter = new dExpContTableAdapters.APARTAMENTOSTableAdapter();
                    aPARTAMENTOSTableAdapter.TrocarStringDeConexao();
                };
                return aPARTAMENTOSTableAdapter;
            }
        }

        private dExpContTableAdapters.TitulosTableAdapter titulosTableAdapter;
        /// <summary>
        /// TableAdapter padrão para tabela: Titulos
        /// </summary>
        public dExpContTableAdapters.TitulosTableAdapter TitulosTableAdapter
        {
            get
            {
                if (titulosTableAdapter == null)
                {
                    titulosTableAdapter = new dExpContTableAdapters.TitulosTableAdapter();
                    titulosTableAdapter.TrocarStringDeConexao();
                };
                return titulosTableAdapter;
            }
        }
    }
}
