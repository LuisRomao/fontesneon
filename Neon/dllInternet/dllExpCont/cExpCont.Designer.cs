﻿namespace dllExpCont
{
    partial class cExpCont
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            DevExpress.XtraGrid.GridLevelNode gridLevelNode1 = new DevExpress.XtraGrid.GridLevelNode();
            this.gridControl1 = new DevExpress.XtraGrid.GridControl();
            this.dExpCont1 = new dllExpCont.dExpCont();
            this.gridView1 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colAPT = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colAPTNumero = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colAPTCodExterno1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colBLOCodigo = new DevExpress.XtraGrid.Columns.GridColumn();
            this.simpleButton1 = new DevExpress.XtraEditors.SimpleButton();
            this.xtraTabControl1 = new DevExpress.XtraTab.XtraTabControl();
            this.xtraTabPage1 = new DevExpress.XtraTab.XtraTabPage();
            this.panelControl1 = new DevExpress.XtraEditors.PanelControl();
            this.xtraTabPage2 = new DevExpress.XtraTab.XtraTabPage();
            this.gridControl2 = new DevExpress.XtraGrid.GridControl();
            this.dExpCont1BindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.gridView2 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colBOL = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colBOD = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colBODMensagem = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colBODValor = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colBOLEmissao = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colBOLVencto = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colBOLPagamento = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colBOLTipoCRAI = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colBOLStatus = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colBOLCancelado = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colBLOCodigo1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colAPTNumero1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colAPTCodExterno11 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colBOL_APT = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colBOD_PLA = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colJuros = new DevExpress.XtraGrid.Columns.GridColumn();
            this.panelControl2 = new DevExpress.XtraEditors.PanelControl();
            this.BotExporta = new DevExpress.XtraEditors.SimpleButton();
            this.BotGrava = new DevExpress.XtraEditors.SimpleButton();
            this.radioGroup1 = new DevExpress.XtraEditors.RadioGroup();
            this.dateEditF = new DevExpress.XtraEditors.DateEdit();
            this.dateEditI = new DevExpress.XtraEditors.DateEdit();
            this.simpleButton2 = new DevExpress.XtraEditors.SimpleButton();
            ((System.ComponentModel.ISupportInitialize)(this.StyleController_F)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dExpCont1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xtraTabControl1)).BeginInit();
            this.xtraTabControl1.SuspendLayout();
            this.xtraTabPage1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl1)).BeginInit();
            this.panelControl1.SuspendLayout();
            this.xtraTabPage2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dExpCont1BindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl2)).BeginInit();
            this.panelControl2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.radioGroup1.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEditF.Properties.CalendarTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEditF.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEditI.Properties.CalendarTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEditI.Properties)).BeginInit();
            this.SuspendLayout();
            // 
            // ToolTipController_F
            // 
            this.ToolTipController_F.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.ToolTipController_F.Appearance.ForeColor = System.Drawing.Color.Black;
            this.ToolTipController_F.Appearance.Options.UseBackColor = true;
            this.ToolTipController_F.Appearance.Options.UseFont = true;
            this.ToolTipController_F.Appearance.Options.UseForeColor = true;
            this.ToolTipController_F.Appearance.Options.UseTextOptions = true;
            this.ToolTipController_F.Appearance.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            // 
            // StyleController_F
            // 
            this.StyleController_F.AppearanceDisabled.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this.StyleController_F.AppearanceDisabled.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this.StyleController_F.AppearanceDisabled.Options.UseBackColor = true;
            this.StyleController_F.AppearanceDisabled.Options.UseForeColor = true;
            this.StyleController_F.AppearanceFocused.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.StyleController_F.AppearanceFocused.Options.UseBackColor = true;
            // 
            // gridControl1
            // 
            this.gridControl1.DataMember = "APARTAMENTOS";
            this.gridControl1.DataSource = this.dExpCont1;
            this.gridControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            gridLevelNode1.RelationName = "APARTAMENTOS_Titulos";
            this.gridControl1.LevelTree.Nodes.AddRange(new DevExpress.XtraGrid.GridLevelNode[] {
            gridLevelNode1});
            this.gridControl1.Location = new System.Drawing.Point(0, 54);
            this.gridControl1.MainView = this.gridView1;
            this.gridControl1.Name = "gridControl1";
            this.gridControl1.ShowOnlyPredefinedDetails = true;
            this.gridControl1.Size = new System.Drawing.Size(1527, 717);
            this.gridControl1.TabIndex = 0;
            this.gridControl1.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridView1});
            // 
            // dExpCont1
            // 
            this.dExpCont1.DataSetName = "dExpCont";
            this.dExpCont1.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // gridView1
            // 
            this.gridView1.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colAPT,
            this.colAPTNumero,
            this.colAPTCodExterno1,
            this.colBLOCodigo});
            this.gridView1.GridControl = this.gridControl1;
            this.gridView1.Name = "gridView1";
            this.gridView1.OptionsView.ColumnAutoWidth = false;
            this.gridView1.OptionsView.ShowAutoFilterRow = true;
            this.gridView1.OptionsView.ShowGroupPanel = false;
            this.gridView1.SortInfo.AddRange(new DevExpress.XtraGrid.Columns.GridColumnSortInfo[] {
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colAPTNumero, DevExpress.Data.ColumnSortOrder.Ascending)});
            // 
            // colAPT
            // 
            this.colAPT.FieldName = "APT";
            this.colAPT.Name = "colAPT";
            // 
            // colAPTNumero
            // 
            this.colAPTNumero.Caption = "Apartamento";
            this.colAPTNumero.FieldName = "APTNumero";
            this.colAPTNumero.Name = "colAPTNumero";
            this.colAPTNumero.Visible = true;
            this.colAPTNumero.VisibleIndex = 1;
            this.colAPTNumero.Width = 103;
            // 
            // colAPTCodExterno1
            // 
            this.colAPTCodExterno1.Caption = "Código";
            this.colAPTCodExterno1.FieldName = "APTCodExterno1";
            this.colAPTCodExterno1.Name = "colAPTCodExterno1";
            this.colAPTCodExterno1.Visible = true;
            this.colAPTCodExterno1.VisibleIndex = 2;
            this.colAPTCodExterno1.Width = 152;
            // 
            // colBLOCodigo
            // 
            this.colBLOCodigo.Caption = "Bloco";
            this.colBLOCodigo.FieldName = "BLOCodigo";
            this.colBLOCodigo.Name = "colBLOCodigo";
            this.colBLOCodigo.Visible = true;
            this.colBLOCodigo.VisibleIndex = 0;
            this.colBLOCodigo.Width = 80;
            // 
            // simpleButton1
            // 
            this.simpleButton1.Location = new System.Drawing.Point(5, 15);
            this.simpleButton1.Name = "simpleButton1";
            this.simpleButton1.Size = new System.Drawing.Size(75, 23);
            this.simpleButton1.TabIndex = 1;
            this.simpleButton1.Text = "Importar";
            this.simpleButton1.Click += new System.EventHandler(this.simpleButton1_Click);
            // 
            // xtraTabControl1
            // 
            this.xtraTabControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.xtraTabControl1.Location = new System.Drawing.Point(0, 0);
            this.xtraTabControl1.Name = "xtraTabControl1";
            this.xtraTabControl1.SelectedTabPage = this.xtraTabPage1;
            this.xtraTabControl1.Size = new System.Drawing.Size(1533, 799);
            this.xtraTabControl1.TabIndex = 2;
            this.xtraTabControl1.TabPages.AddRange(new DevExpress.XtraTab.XtraTabPage[] {
            this.xtraTabPage1,
            this.xtraTabPage2});
            // 
            // xtraTabPage1
            // 
            this.xtraTabPage1.Controls.Add(this.gridControl1);
            this.xtraTabPage1.Controls.Add(this.panelControl1);
            this.xtraTabPage1.Name = "xtraTabPage1";
            this.xtraTabPage1.Size = new System.Drawing.Size(1527, 771);
            this.xtraTabPage1.Text = "Códigos";
            // 
            // panelControl1
            // 
            this.panelControl1.Controls.Add(this.simpleButton1);
            this.panelControl1.Dock = System.Windows.Forms.DockStyle.Top;
            this.panelControl1.Location = new System.Drawing.Point(0, 0);
            this.panelControl1.Name = "panelControl1";
            this.panelControl1.Size = new System.Drawing.Size(1527, 54);
            this.panelControl1.TabIndex = 1;
            // 
            // xtraTabPage2
            // 
            this.xtraTabPage2.Controls.Add(this.gridControl2);
            this.xtraTabPage2.Controls.Add(this.panelControl2);
            this.xtraTabPage2.Name = "xtraTabPage2";
            this.xtraTabPage2.Size = new System.Drawing.Size(1498, 771);
            this.xtraTabPage2.Text = "Exportar";
            // 
            // gridControl2
            // 
            this.gridControl2.DataMember = "Titulos";
            this.gridControl2.DataSource = this.dExpCont1BindingSource;
            this.gridControl2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridControl2.Location = new System.Drawing.Point(0, 96);
            this.gridControl2.LookAndFeel.Style = DevExpress.LookAndFeel.LookAndFeelStyle.Flat;
            this.gridControl2.LookAndFeel.UseDefaultLookAndFeel = false;
            this.gridControl2.MainView = this.gridView2;
            this.gridControl2.Name = "gridControl2";
            this.gridControl2.Size = new System.Drawing.Size(1498, 675);
            this.gridControl2.TabIndex = 1;
            this.gridControl2.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridView2});
            // 
            // dExpCont1BindingSource
            // 
            this.dExpCont1BindingSource.DataSource = this.dExpCont1;
            this.dExpCont1BindingSource.Position = 0;
            // 
            // gridView2
            // 
            this.gridView2.Appearance.ColumnFilterButton.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(240)))), ((int)(((byte)(177)))), ((int)(((byte)(94)))));
            this.gridView2.Appearance.ColumnFilterButton.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(240)))), ((int)(((byte)(177)))), ((int)(((byte)(94)))));
            this.gridView2.Appearance.ColumnFilterButton.ForeColor = System.Drawing.Color.Black;
            this.gridView2.Appearance.ColumnFilterButton.Options.UseBackColor = true;
            this.gridView2.Appearance.ColumnFilterButton.Options.UseBorderColor = true;
            this.gridView2.Appearance.ColumnFilterButton.Options.UseForeColor = true;
            this.gridView2.Appearance.ColumnFilterButtonActive.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(248)))), ((int)(((byte)(204)))), ((int)(((byte)(124)))));
            this.gridView2.Appearance.ColumnFilterButtonActive.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(248)))), ((int)(((byte)(204)))), ((int)(((byte)(124)))));
            this.gridView2.Appearance.ColumnFilterButtonActive.ForeColor = System.Drawing.Color.Black;
            this.gridView2.Appearance.ColumnFilterButtonActive.Options.UseBackColor = true;
            this.gridView2.Appearance.ColumnFilterButtonActive.Options.UseBorderColor = true;
            this.gridView2.Appearance.ColumnFilterButtonActive.Options.UseForeColor = true;
            this.gridView2.Appearance.Empty.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(251)))), ((int)(((byte)(244)))), ((int)(((byte)(232)))));
            this.gridView2.Appearance.Empty.BackColor2 = System.Drawing.Color.White;
            this.gridView2.Appearance.Empty.Options.UseBackColor = true;
            this.gridView2.Appearance.EvenRow.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(253)))), ((int)(((byte)(248)))), ((int)(((byte)(236)))));
            this.gridView2.Appearance.EvenRow.ForeColor = System.Drawing.Color.Black;
            this.gridView2.Appearance.EvenRow.Options.UseBackColor = true;
            this.gridView2.Appearance.EvenRow.Options.UseForeColor = true;
            this.gridView2.Appearance.FilterCloseButton.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(240)))), ((int)(((byte)(177)))), ((int)(((byte)(94)))));
            this.gridView2.Appearance.FilterCloseButton.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(240)))), ((int)(((byte)(177)))), ((int)(((byte)(94)))));
            this.gridView2.Appearance.FilterCloseButton.ForeColor = System.Drawing.Color.Black;
            this.gridView2.Appearance.FilterCloseButton.Options.UseBackColor = true;
            this.gridView2.Appearance.FilterCloseButton.Options.UseBorderColor = true;
            this.gridView2.Appearance.FilterCloseButton.Options.UseForeColor = true;
            this.gridView2.Appearance.FilterPanel.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(251)))), ((int)(((byte)(244)))), ((int)(((byte)(232)))));
            this.gridView2.Appearance.FilterPanel.BackColor2 = System.Drawing.Color.White;
            this.gridView2.Appearance.FilterPanel.ForeColor = System.Drawing.Color.Black;
            this.gridView2.Appearance.FilterPanel.Options.UseBackColor = true;
            this.gridView2.Appearance.FilterPanel.Options.UseForeColor = true;
            this.gridView2.Appearance.FixedLine.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(218)))), ((int)(((byte)(153)))), ((int)(((byte)(73)))));
            this.gridView2.Appearance.FixedLine.Options.UseBackColor = true;
            this.gridView2.Appearance.FocusedCell.BackColor = System.Drawing.Color.White;
            this.gridView2.Appearance.FocusedCell.ForeColor = System.Drawing.Color.Black;
            this.gridView2.Appearance.FocusedCell.Options.UseBackColor = true;
            this.gridView2.Appearance.FocusedCell.Options.UseForeColor = true;
            this.gridView2.Appearance.FocusedRow.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(217)))), ((int)(((byte)(154)))), ((int)(((byte)(91)))));
            this.gridView2.Appearance.FocusedRow.ForeColor = System.Drawing.Color.White;
            this.gridView2.Appearance.FocusedRow.Options.UseBackColor = true;
            this.gridView2.Appearance.FocusedRow.Options.UseForeColor = true;
            this.gridView2.Appearance.FooterPanel.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(240)))), ((int)(((byte)(177)))), ((int)(((byte)(94)))));
            this.gridView2.Appearance.FooterPanel.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(240)))), ((int)(((byte)(177)))), ((int)(((byte)(94)))));
            this.gridView2.Appearance.FooterPanel.ForeColor = System.Drawing.Color.Black;
            this.gridView2.Appearance.FooterPanel.Options.UseBackColor = true;
            this.gridView2.Appearance.FooterPanel.Options.UseBorderColor = true;
            this.gridView2.Appearance.FooterPanel.Options.UseForeColor = true;
            this.gridView2.Appearance.GroupButton.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(248)))), ((int)(((byte)(204)))), ((int)(((byte)(124)))));
            this.gridView2.Appearance.GroupButton.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(248)))), ((int)(((byte)(204)))), ((int)(((byte)(124)))));
            this.gridView2.Appearance.GroupButton.Options.UseBackColor = true;
            this.gridView2.Appearance.GroupButton.Options.UseBorderColor = true;
            this.gridView2.Appearance.GroupFooter.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(248)))), ((int)(((byte)(204)))), ((int)(((byte)(124)))));
            this.gridView2.Appearance.GroupFooter.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(248)))), ((int)(((byte)(204)))), ((int)(((byte)(124)))));
            this.gridView2.Appearance.GroupFooter.ForeColor = System.Drawing.Color.Black;
            this.gridView2.Appearance.GroupFooter.Options.UseBackColor = true;
            this.gridView2.Appearance.GroupFooter.Options.UseBorderColor = true;
            this.gridView2.Appearance.GroupFooter.Options.UseForeColor = true;
            this.gridView2.Appearance.GroupPanel.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(251)))), ((int)(((byte)(244)))), ((int)(((byte)(232)))));
            this.gridView2.Appearance.GroupPanel.BackColor2 = System.Drawing.Color.White;
            this.gridView2.Appearance.GroupPanel.ForeColor = System.Drawing.Color.Black;
            this.gridView2.Appearance.GroupPanel.Options.UseBackColor = true;
            this.gridView2.Appearance.GroupPanel.Options.UseForeColor = true;
            this.gridView2.Appearance.GroupRow.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(248)))), ((int)(((byte)(204)))), ((int)(((byte)(124)))));
            this.gridView2.Appearance.GroupRow.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(248)))), ((int)(((byte)(204)))), ((int)(((byte)(124)))));
            this.gridView2.Appearance.GroupRow.ForeColor = System.Drawing.Color.Black;
            this.gridView2.Appearance.GroupRow.Options.UseBackColor = true;
            this.gridView2.Appearance.GroupRow.Options.UseBorderColor = true;
            this.gridView2.Appearance.GroupRow.Options.UseForeColor = true;
            this.gridView2.Appearance.HeaderPanel.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(248)))), ((int)(((byte)(204)))), ((int)(((byte)(124)))));
            this.gridView2.Appearance.HeaderPanel.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(248)))), ((int)(((byte)(204)))), ((int)(((byte)(124)))));
            this.gridView2.Appearance.HeaderPanel.ForeColor = System.Drawing.Color.Black;
            this.gridView2.Appearance.HeaderPanel.Options.UseBackColor = true;
            this.gridView2.Appearance.HeaderPanel.Options.UseBorderColor = true;
            this.gridView2.Appearance.HeaderPanel.Options.UseForeColor = true;
            this.gridView2.Appearance.HideSelectionRow.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(225)))), ((int)(((byte)(183)))), ((int)(((byte)(125)))));
            this.gridView2.Appearance.HideSelectionRow.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(251)))), ((int)(((byte)(244)))), ((int)(((byte)(232)))));
            this.gridView2.Appearance.HideSelectionRow.Options.UseBackColor = true;
            this.gridView2.Appearance.HideSelectionRow.Options.UseForeColor = true;
            this.gridView2.Appearance.HorzLine.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(240)))), ((int)(((byte)(177)))), ((int)(((byte)(94)))));
            this.gridView2.Appearance.HorzLine.Options.UseBackColor = true;
            this.gridView2.Appearance.OddRow.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(251)))), ((int)(((byte)(236)))), ((int)(((byte)(208)))));
            this.gridView2.Appearance.OddRow.ForeColor = System.Drawing.Color.Black;
            this.gridView2.Appearance.OddRow.Options.UseBackColor = true;
            this.gridView2.Appearance.OddRow.Options.UseForeColor = true;
            this.gridView2.Appearance.Preview.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(254)))), ((int)(((byte)(249)))));
            this.gridView2.Appearance.Preview.Font = new System.Drawing.Font("Verdana", 7.5F);
            this.gridView2.Appearance.Preview.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(186)))), ((int)(((byte)(146)))), ((int)(((byte)(78)))));
            this.gridView2.Appearance.Preview.Options.UseBackColor = true;
            this.gridView2.Appearance.Preview.Options.UseFont = true;
            this.gridView2.Appearance.Preview.Options.UseForeColor = true;
            this.gridView2.Appearance.Row.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(253)))), ((int)(((byte)(248)))), ((int)(((byte)(236)))));
            this.gridView2.Appearance.Row.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(253)))), ((int)(((byte)(248)))), ((int)(((byte)(236)))));
            this.gridView2.Appearance.Row.ForeColor = System.Drawing.Color.Black;
            this.gridView2.Appearance.Row.Options.UseBackColor = true;
            this.gridView2.Appearance.Row.Options.UseBorderColor = true;
            this.gridView2.Appearance.Row.Options.UseForeColor = true;
            this.gridView2.Appearance.RowSeparator.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(251)))), ((int)(((byte)(244)))), ((int)(((byte)(232)))));
            this.gridView2.Appearance.RowSeparator.BackColor2 = System.Drawing.Color.White;
            this.gridView2.Appearance.RowSeparator.Options.UseBackColor = true;
            this.gridView2.Appearance.SelectedRow.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(231)))), ((int)(((byte)(167)))), ((int)(((byte)(103)))));
            this.gridView2.Appearance.SelectedRow.Options.UseBackColor = true;
            this.gridView2.Appearance.TopNewRow.BackColor = System.Drawing.Color.White;
            this.gridView2.Appearance.TopNewRow.Options.UseBackColor = true;
            this.gridView2.Appearance.VertLine.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(240)))), ((int)(((byte)(177)))), ((int)(((byte)(94)))));
            this.gridView2.Appearance.VertLine.Options.UseBackColor = true;
            this.gridView2.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colBOL,
            this.colBOD,
            this.colBODMensagem,
            this.colBODValor,
            this.colBOLEmissao,
            this.colBOLVencto,
            this.colBOLPagamento,
            this.colBOLTipoCRAI,
            this.colBOLStatus,
            this.colBOLCancelado,
            this.colBLOCodigo1,
            this.colAPTNumero1,
            this.colAPTCodExterno11,
            this.colBOL_APT,
            this.colBOD_PLA,
            this.colJuros});
            this.gridView2.GridControl = this.gridControl2;
            this.gridView2.GroupFormat = "{0}: [#image]{1} TOTAL: R$ {2}";
            this.gridView2.GroupSummary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridGroupSummaryItem(DevExpress.Data.SummaryItemType.Sum, "BODValor", this.colBODValor, "{0:n2}"),
            new DevExpress.XtraGrid.GridGroupSummaryItem(DevExpress.Data.SummaryItemType.Sum, "BODValor", null, "{0:n2}")});
            this.gridView2.Name = "gridView2";
            this.gridView2.OptionsView.ColumnAutoWidth = false;
            this.gridView2.OptionsView.EnableAppearanceEvenRow = true;
            this.gridView2.OptionsView.EnableAppearanceOddRow = true;
            this.gridView2.OptionsView.ShowAutoFilterRow = true;
            this.gridView2.OptionsView.ShowFooter = true;
            this.gridView2.OptionsView.ShowGroupPanel = false;
            this.gridView2.PaintStyleName = "Flat";
            this.gridView2.RowCellStyle += new DevExpress.XtraGrid.Views.Grid.RowCellStyleEventHandler(this.gridView2_RowCellStyle);
            this.gridView2.RowUpdated += new DevExpress.XtraGrid.Views.Base.RowObjectEventHandler(this.gridView2_RowUpdated);
            // 
            // colBOL
            // 
            this.colBOL.Caption = "Boleto";
            this.colBOL.FieldName = "BOL";
            this.colBOL.Name = "colBOL";
            this.colBOL.OptionsColumn.ReadOnly = true;
            this.colBOL.Visible = true;
            this.colBOL.VisibleIndex = 3;
            // 
            // colBOD
            // 
            this.colBOD.Caption = "Codigo";
            this.colBOD.FieldName = "BOD";
            this.colBOD.Name = "colBOD";
            this.colBOD.OptionsColumn.ReadOnly = true;
            this.colBOD.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Count, "BOD", "{0:n0}")});
            this.colBOD.Visible = true;
            this.colBOD.VisibleIndex = 2;
            // 
            // colBODMensagem
            // 
            this.colBODMensagem.Caption = "Descrição";
            this.colBODMensagem.FieldName = "BODMensagem";
            this.colBODMensagem.Name = "colBODMensagem";
            this.colBODMensagem.OptionsColumn.ReadOnly = true;
            this.colBODMensagem.Visible = true;
            this.colBODMensagem.VisibleIndex = 4;
            // 
            // colBODValor
            // 
            this.colBODValor.Caption = "Valor";
            this.colBODValor.DisplayFormat.FormatString = "n2";
            this.colBODValor.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.colBODValor.FieldName = "BODValor";
            this.colBODValor.Name = "colBODValor";
            this.colBODValor.OptionsColumn.ReadOnly = true;
            this.colBODValor.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum, "BODValor", "{0:n2}")});
            this.colBODValor.Visible = true;
            this.colBODValor.VisibleIndex = 5;
            // 
            // colBOLEmissao
            // 
            this.colBOLEmissao.Caption = "Emissão";
            this.colBOLEmissao.FieldName = "BOLEmissao";
            this.colBOLEmissao.Name = "colBOLEmissao";
            this.colBOLEmissao.OptionsColumn.ReadOnly = true;
            this.colBOLEmissao.Visible = true;
            this.colBOLEmissao.VisibleIndex = 7;
            // 
            // colBOLVencto
            // 
            this.colBOLVencto.Caption = "Vencimento";
            this.colBOLVencto.FieldName = "BOLVencto";
            this.colBOLVencto.Name = "colBOLVencto";
            this.colBOLVencto.OptionsColumn.ReadOnly = true;
            this.colBOLVencto.Visible = true;
            this.colBOLVencto.VisibleIndex = 8;
            // 
            // colBOLPagamento
            // 
            this.colBOLPagamento.Caption = "Pagamento";
            this.colBOLPagamento.FieldName = "BOLPagamento";
            this.colBOLPagamento.Name = "colBOLPagamento";
            this.colBOLPagamento.OptionsColumn.ReadOnly = true;
            this.colBOLPagamento.Visible = true;
            this.colBOLPagamento.VisibleIndex = 9;
            // 
            // colBOLTipoCRAI
            // 
            this.colBOLTipoCRAI.Caption = "Tipo";
            this.colBOLTipoCRAI.FieldName = "BOLTipoCRAI";
            this.colBOLTipoCRAI.Name = "colBOLTipoCRAI";
            this.colBOLTipoCRAI.OptionsColumn.ReadOnly = true;
            this.colBOLTipoCRAI.Visible = true;
            this.colBOLTipoCRAI.VisibleIndex = 10;
            // 
            // colBOLStatus
            // 
            this.colBOLStatus.Caption = "Status";
            this.colBOLStatus.FieldName = "BOLStatus";
            this.colBOLStatus.Name = "colBOLStatus";
            this.colBOLStatus.OptionsColumn.ReadOnly = true;
            this.colBOLStatus.Visible = true;
            this.colBOLStatus.VisibleIndex = 11;
            // 
            // colBOLCancelado
            // 
            this.colBOLCancelado.Caption = "Ativo";
            this.colBOLCancelado.FieldName = "BOLCancelado";
            this.colBOLCancelado.Name = "colBOLCancelado";
            this.colBOLCancelado.OptionsColumn.ReadOnly = true;
            this.colBOLCancelado.Visible = true;
            this.colBOLCancelado.VisibleIndex = 12;
            // 
            // colBLOCodigo1
            // 
            this.colBLOCodigo1.Caption = "Bloco";
            this.colBLOCodigo1.FieldName = "BLOCodigo";
            this.colBLOCodigo1.Name = "colBLOCodigo1";
            this.colBLOCodigo1.Visible = true;
            this.colBLOCodigo1.VisibleIndex = 0;
            // 
            // colAPTNumero1
            // 
            this.colAPTNumero1.Caption = "Apartamento";
            this.colAPTNumero1.FieldName = "APTNumero";
            this.colAPTNumero1.Name = "colAPTNumero1";
            this.colAPTNumero1.Visible = true;
            this.colAPTNumero1.VisibleIndex = 1;
            // 
            // colAPTCodExterno11
            // 
            this.colAPTCodExterno11.Caption = "Códiigo do cliente";
            this.colAPTCodExterno11.FieldName = "APTCodExterno1";
            this.colAPTCodExterno11.Name = "colAPTCodExterno11";
            this.colAPTCodExterno11.Visible = true;
            this.colAPTCodExterno11.VisibleIndex = 13;
            this.colAPTCodExterno11.Width = 144;
            // 
            // colBOL_APT
            // 
            this.colBOL_APT.FieldName = "BOL_APT";
            this.colBOL_APT.Name = "colBOL_APT";
            // 
            // colBOD_PLA
            // 
            this.colBOD_PLA.Caption = "Plano de contas";
            this.colBOD_PLA.FieldName = "BOD_PLA";
            this.colBOD_PLA.Name = "colBOD_PLA";
            this.colBOD_PLA.Visible = true;
            this.colBOD_PLA.VisibleIndex = 14;
            // 
            // colJuros
            // 
            this.colJuros.Caption = "Juros";
            this.colJuros.DisplayFormat.FormatString = "n2";
            this.colJuros.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.colJuros.FieldName = "Juros";
            this.colJuros.Name = "colJuros";
            this.colJuros.Visible = true;
            this.colJuros.VisibleIndex = 6;
            // 
            // panelControl2
            // 
            this.panelControl2.Controls.Add(this.BotExporta);
            this.panelControl2.Controls.Add(this.BotGrava);
            this.panelControl2.Controls.Add(this.radioGroup1);
            this.panelControl2.Controls.Add(this.dateEditF);
            this.panelControl2.Controls.Add(this.dateEditI);
            this.panelControl2.Controls.Add(this.simpleButton2);
            this.panelControl2.Dock = System.Windows.Forms.DockStyle.Top;
            this.panelControl2.Location = new System.Drawing.Point(0, 0);
            this.panelControl2.Name = "panelControl2";
            this.panelControl2.Size = new System.Drawing.Size(1498, 96);
            this.panelControl2.TabIndex = 0;
            // 
            // BotExporta
            // 
            this.BotExporta.Enabled = false;
            this.BotExporta.Location = new System.Drawing.Point(186, 63);
            this.BotExporta.Name = "BotExporta";
            this.BotExporta.Size = new System.Drawing.Size(104, 23);
            this.BotExporta.TabIndex = 7;
            this.BotExporta.Text = "Exportar planinha";
            this.BotExporta.Click += new System.EventHandler(this.simpleButton3_Click_1);
            // 
            // BotGrava
            // 
            this.BotGrava.Enabled = false;
            this.BotGrava.Location = new System.Drawing.Point(186, 34);
            this.BotGrava.Name = "BotGrava";
            this.BotGrava.Size = new System.Drawing.Size(104, 23);
            this.BotGrava.TabIndex = 6;
            this.BotGrava.Text = "Gerar Arquivo";
            this.BotGrava.Click += new System.EventHandler(this.simpleButton3_Click);
            // 
            // radioGroup1
            // 
            this.radioGroup1.EditValue = 0;
            this.radioGroup1.Location = new System.Drawing.Point(6, 6);
            this.radioGroup1.Name = "radioGroup1";
            this.radioGroup1.Properties.Items.AddRange(new DevExpress.XtraEditors.Controls.RadioGroupItem[] {
            new DevExpress.XtraEditors.Controls.RadioGroupItem(0, "Emitidos"),
            new DevExpress.XtraEditors.Controls.RadioGroupItem(1, "Quitados"),
            new DevExpress.XtraEditors.Controls.RadioGroupItem(2, "Abertos")});
            this.radioGroup1.Size = new System.Drawing.Size(148, 75);
            this.radioGroup1.TabIndex = 5;
            this.radioGroup1.EditValueChanged += new System.EventHandler(this.radioGroup1_EditValueChanged);
            // 
            // dateEditF
            // 
            this.dateEditF.EditValue = null;
            this.dateEditF.Location = new System.Drawing.Point(319, 29);
            this.dateEditF.Name = "dateEditF";
            this.dateEditF.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.dateEditF.Properties.CalendarTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.dateEditF.Size = new System.Drawing.Size(100, 20);
            this.dateEditF.TabIndex = 4;
            // 
            // dateEditI
            // 
            this.dateEditI.EditValue = null;
            this.dateEditI.Location = new System.Drawing.Point(319, 3);
            this.dateEditI.Name = "dateEditI";
            this.dateEditI.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.dateEditI.Properties.CalendarTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.dateEditI.Size = new System.Drawing.Size(100, 20);
            this.dateEditI.TabIndex = 3;
            // 
            // simpleButton2
            // 
            this.simpleButton2.Location = new System.Drawing.Point(186, 5);
            this.simpleButton2.Name = "simpleButton2";
            this.simpleButton2.Size = new System.Drawing.Size(104, 23);
            this.simpleButton2.TabIndex = 2;
            this.simpleButton2.Text = "Calcular";
            this.simpleButton2.Click += new System.EventHandler(this.simpleButton2_Click);
            // 
            // cExpCont
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.xtraTabControl1);
            this.Doc = System.Windows.Forms.DockStyle.Fill;
            this.LookAndFeel.UseDefaultLookAndFeel = false;
            this.Name = "cExpCont";
            this.Size = new System.Drawing.Size(1533, 799);
            ((System.ComponentModel.ISupportInitialize)(this.StyleController_F)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dExpCont1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xtraTabControl1)).EndInit();
            this.xtraTabControl1.ResumeLayout(false);
            this.xtraTabPage1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.panelControl1)).EndInit();
            this.panelControl1.ResumeLayout(false);
            this.xtraTabPage2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gridControl2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dExpCont1BindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl2)).EndInit();
            this.panelControl2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.radioGroup1.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEditF.Properties.CalendarTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEditF.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEditI.Properties.CalendarTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEditI.Properties)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraGrid.GridControl gridControl1;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView1;
        private dExpCont dExpCont1;
        private DevExpress.XtraEditors.SimpleButton simpleButton1;
        private DevExpress.XtraTab.XtraTabControl xtraTabControl1;
        private DevExpress.XtraTab.XtraTabPage xtraTabPage1;
        private DevExpress.XtraEditors.PanelControl panelControl1;
        private DevExpress.XtraTab.XtraTabPage xtraTabPage2;
        private DevExpress.XtraGrid.GridControl gridControl2;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView2;
        private DevExpress.XtraEditors.PanelControl panelControl2;
        private DevExpress.XtraEditors.SimpleButton simpleButton2;
        private System.Windows.Forms.BindingSource dExpCont1BindingSource;
        private DevExpress.XtraEditors.DateEdit dateEditF;
        private DevExpress.XtraEditors.DateEdit dateEditI;
        private DevExpress.XtraGrid.Columns.GridColumn colBOL;
        private DevExpress.XtraGrid.Columns.GridColumn colBOD;
        private DevExpress.XtraGrid.Columns.GridColumn colBODMensagem;
        private DevExpress.XtraGrid.Columns.GridColumn colBODValor;
        private DevExpress.XtraGrid.Columns.GridColumn colBOLEmissao;
        private DevExpress.XtraGrid.Columns.GridColumn colBOLVencto;
        private DevExpress.XtraGrid.Columns.GridColumn colBOLPagamento;
        private DevExpress.XtraGrid.Columns.GridColumn colBOLTipoCRAI;
        private DevExpress.XtraGrid.Columns.GridColumn colBOLStatus;
        private DevExpress.XtraGrid.Columns.GridColumn colBOLCancelado;
        private DevExpress.XtraGrid.Columns.GridColumn colBLOCodigo1;
        private DevExpress.XtraGrid.Columns.GridColumn colAPTNumero1;
        private DevExpress.XtraEditors.RadioGroup radioGroup1;
        private DevExpress.XtraEditors.SimpleButton BotGrava;
        private DevExpress.XtraGrid.Columns.GridColumn colAPT;
        private DevExpress.XtraGrid.Columns.GridColumn colAPTNumero;
        private DevExpress.XtraGrid.Columns.GridColumn colAPTCodExterno1;
        private DevExpress.XtraGrid.Columns.GridColumn colAPTCodExterno11;
        private DevExpress.XtraGrid.Columns.GridColumn colBOL_APT;
        private DevExpress.XtraGrid.Columns.GridColumn colBLOCodigo;
        private DevExpress.XtraGrid.Columns.GridColumn colBOD_PLA;
        private DevExpress.XtraEditors.SimpleButton BotExporta;
        private DevExpress.XtraGrid.Columns.GridColumn colJuros;
    }
}
