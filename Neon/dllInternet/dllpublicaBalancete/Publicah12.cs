using System;
using System.IO;

namespace dllpublicaBalancete
{
    
    /// <summary>
    /// 
    /// </summary>
    public class Publicah12
    {
        private static int staticR;

        /// <summary>
        /// 
        /// </summary>
        /// <param name="EmProducao"></param>
        /// <param name="_EMP"></param>
        public Publicah12(bool EmProducao, int _EMP)
        {
            WS = new WSSincBancos.WSSincBancos();
            EMP = _EMP;
            if (EmProducao)
            {                
                WS.Url = @"http://177.190.193.218/Neon23/WSSincBancos.asmx";
                if (Framework.usuarioLogado.UsuarioLogadoNeon.UsuarioLogadoStNEON != null)
                    WS.Proxy = Framework.usuarioLogado.UsuarioLogadoNeon.UsuarioLogadoStNEON.Proxy;
            }
        }

        private WSSincBancos.WSSincBancos WS;

        private int EMP;

        private byte[] Criptografa()
        {
            string Aberto = string.Format("{0:yyyyMMddHHmmss}{1:000}{2:00000000}<-*W*->", DateTime.Now, EMP, staticR++);
            return VirCrip.VirCripWS.Crip(Aberto);
        }

        private ClientWCFFtp.ClientFTP _clientFTP;

        private ClientWCFFtp.ClientFTP clientFTP
        {
            get
            {
                if (_clientFTP == null)
                    _clientFTP = new ClientWCFFtp.ClientFTP(CompontesBasicos.FormPrincipalBase.strEmProducao, Framework.DSCentral.USU, Framework.DSCentral.EMP);
                return _clientFTP;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="Arquivo"></param>
        /// <param name="Competencia">CompetÍncia</param>
        /// <returns></returns>
        public bool Publicar(string Arquivo, int Competencia)
        {
            return clientFTP.PutPasta(Arquivo, Competencia.ToString());

            /*
            byte[] bdata;
            using (FileStream fStream = new FileStream(Arquivo, FileMode.Open, FileAccess.Read))
            {
                        using (BinaryReader br = new BinaryReader(fStream))
                        {
                            bdata = br.ReadBytes((int)(new FileInfo(Arquivo).Length));
                            br.Close();
                            fStream.Close();
                        }
            };
            
            bool retorno;
            if (pasta == "")
                retorno = (WS.PublicaDOC(Criptografa(), Path.GetFileName(Arquivo), bdata).ToUpper() == "OK");
            else
                retorno = (WS.PublicaDOCPasta(Criptografa(), Path.GetFileName(Arquivo), pasta, bdata).ToUpper() == "OK");
            return retorno;*/
        }

        
    }
}
