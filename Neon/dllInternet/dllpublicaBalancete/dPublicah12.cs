﻿namespace dllpublicaBalancete {


    partial class dPublicah12
    {
        private dPublicah12TableAdapters.Historico12CredTableAdapter historico12CredTableAdapter;
        /// <summary>
        /// TableAdapter padrão para tabela: Historico12Cred
        /// </summary>
        public dPublicah12TableAdapters.Historico12CredTableAdapter Historico12CredTableAdapter
        {
            get
            {
                if (historico12CredTableAdapter == null)
                {
                    historico12CredTableAdapter = new dPublicah12TableAdapters.Historico12CredTableAdapter();
                    historico12CredTableAdapter.TrocarStringDeConexao(VirDB.Bancovirtual.TiposDeBanco.AccessH);
                };
                return historico12CredTableAdapter;
            }
        }

        private dPublicah12TableAdapters.BOLetosTableAdapter bOLetosTableAdapter;
        /// <summary>
        /// TableAdapter padrão para tabela: BOLetos
        /// </summary>
        public dPublicah12TableAdapters.BOLetosTableAdapter BOLetosTableAdapter
        {
            get
            {
                if (bOLetosTableAdapter == null)
                {
                    bOLetosTableAdapter = new dPublicah12TableAdapters.BOLetosTableAdapter();
                    bOLetosTableAdapter.TrocarStringDeConexao();
                };
                return bOLetosTableAdapter;
            }
        }

        private dPublicah12TableAdapters.BOLetosAcordoTableAdapter bOLetosAcordoTableAdapter;
        /// <summary>
        /// TableAdapter padrão para tabela: BOLetosAcordo
        /// </summary>
        public dPublicah12TableAdapters.BOLetosAcordoTableAdapter BOLetosAcordoTableAdapter
        {
            get
            {
                if (bOLetosAcordoTableAdapter == null)
                {
                    bOLetosAcordoTableAdapter = new dPublicah12TableAdapters.BOLetosAcordoTableAdapter();
                    bOLetosAcordoTableAdapter.TrocarStringDeConexao();
                };
                return bOLetosAcordoTableAdapter;
            }
        }

    }
}
