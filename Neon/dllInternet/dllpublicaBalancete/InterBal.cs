using System;
using System.Collections;
using System.Text;
using System.Web;
using Framework.objetosNeon;
using VirMSSQL;



namespace dllpublicaBalancete
{
    /// <summary>
    /// 
    /// </summary>
    public class InterBal
    {
        /// <summary>
        /// 
        /// </summary>
        public bool parcial;
        private static int staticR;
        private WSSincBancos.WSSincBancos WS;
        private int EMPArquivo;
        private Competencia Compet;
        private DateTime DI;
        private DateTime DF;
        private string CODCON;
        private int BAL;

        private decimal subtotal;
        private string descanterior;
        private string descant;
        private dsBalLocal dsBalLocal1;
        private pcsvalor Pcsvalor;
        private int grupoant;
        private decimal totant;
        private SortedList despesasdg;
        private SortedList despesasmo;
        private SortedList despesasad;
        private string ultimoglanc;
        private decimal totalgrupolanc;
        private int ordem;
        private string ultimogrupo3d;
        private decimal totalgrupo3d;
        private decimal totalrec;
        private decimal totaldesp;
        private decimal totdg;
        private decimal totmo;
        private decimal totad;
        private string titulobloco;
        private string blocoap;
        private dsBalLocal.CondominiosRow rowCon;
        private FrameworkProc.datasets.dCondominiosAtivos.CONDOMINIOSRow rowCON;
        
        /// <summary>
        /// 
        /// </summary>
        public WSSincBancos.dBAL dPublicar;

        private string larguracolunas23 = "width=\"140\"";

        /// <summary>
        /// 
        /// </summary>
        /// <param name="EmProducao"></param>
        /// <param name="EMP"></param>
        /// <param name="_Compet"></param>
        /// <param name="_DI"></param>
        /// <param name="_DF"></param>
        /// <param name="_BAL"></param>
        public InterBal(bool EmProducao,int EMP,Competencia _Compet,DateTime _DI,DateTime _DF,int _BAL)
        {
            Compet = _Compet;
            DI = _DI;
            DF = _DF;
            BAL = _BAL; 
            //deletar anteriormente publicado

            if (!TableAdapter.ST().BuscaEscalar("select concodigo from condominios where con = @P1", out CODCON, Compet.CON))
                return;
            WS = new WSSincBancos.WSSincBancos();
            
            EMPArquivo = EMP;
            if (EmProducao)
            {

                WS.Url = @"http://177.190.193.218/Neon23/WSSincBancos.asmx";
                if (Framework.usuarioLogado.UsuarioLogadoNeon.UsuarioLogadoStNEON != null)
                    WS.Proxy = Framework.usuarioLogado.UsuarioLogadoNeon.UsuarioLogadoStNEON.Proxy;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="modelo"></param>
        /// <returns></returns>
        public bool URLPublicacao(int modelo)
        {
            return WS.VerificaBALM(Criptografa(), CODCON, Compet.ToStringN(), modelo);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public string CancelaPublicacao()
        {
            try
            {                
                return WS.ApagaBAL(Criptografa(), BAL);
            }
            catch (Exception e)
            {
                return "N�o apagado\r\n" + e.Message;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public string Publica()
        {
            return Publica("",0);
        }

        private string Path;

        /// <summary>
        /// Publica tudo
        /// </summary>
        /// <returns></returns>
        public string Publica(string _Path, int modelo)
        {
            Path = _Path + @"\TMP\";
            rowCON = FrameworkProc.datasets.dCondominiosAtivos.dCondominiosAtivosST.CONDOMINIOS.FindByCON(Compet.CON);
            dsBalLocal1 = new dsBalLocal();
            if (dsBalLocal1.CondominiosTableAdapter.Fill(dsBalLocal1.Condominios, CODCON) == 0)
                return "Condominio n�o encontrado";
            rowCon = dsBalLocal1.Condominios[0];
            grupoant = -1;
            totant = 0;
            descant = ultimogrupo3d = ultimoglanc = "";
            totdg = totmo = totad = totalrec = totaldesp = totalgrupo3d = totalgrupolanc = 0;                        
            Pcsvalor = new pcsvalor();
            //dsBalLocal.BalanceteTableAdapter.Fill(dsBalLocal1.Balancete,CODCON,DI,DF);
            string ComandoBOL = "SELECT BOLVencto, BOLValorPrevisto FROM BOLetos WHERE (BOL = @P1)";
            dsBalLocal.BalanceteTableAdapter.FillBy(dsBalLocal1.Balancete, CODCON, DI, DF);

            /*
            if (System.IO.File.Exists(@"c:\lixo\erropublica.xml"))
            {
                dsBalLocal1.Balancete.Clear();
                System.Data.DataSet DS = new System.Data.DataSet();
                DS.ReadXml(@"c:\lixo\erropublica.xml");
                System.Data.DataTable DT = DS.Tables[0];
                foreach (System.Data.DataRow DR in DT.Rows)
                {
                    dsBalLocal.BalanceteRow NovaDR = dsBalLocal1.Balancete.NewBalanceteRow();
                    foreach (System.Data.DataColumn DC in DR.Table.Columns)
                        NovaDR[DC.ColumnName] = DR[DC.ColumnName];
                    dsBalLocal1.Balancete.AddBalanceteRow(NovaDR);
                }
            }
            */


            foreach (dsBalLocal.BalanceteRow rowBAL in dsBalLocal1.Balancete)
            {
                if(!rowBAL.IsnumdocNull() && (rowBAL.numdoc != 0))
                {
                    System.Data.DataRow DR = TableAdapter.ST().BuscaSQLRow(ComandoBOL, rowBAL.numdoc);
                    if (DR != null)
                    {
                        if (DR["BOLValorPrevisto"] != DBNull.Value)
                            rowBAL.valorprevisto = (float)((decimal)DR["BOLValorPrevisto"]);
                        if (DR["BOLVencto"] != DBNull.Value)
                            rowBAL.vencto = (DateTime)DR["BOLVencto"];
                    }
                }
            }

            




            Gerarecebimentos();
            foreach(Modelos Mx in new Modelos[] {Modelos.M1,Modelos.M3})
            {
                PublicaHTML PublicaX = PublicaHTML.Mx(Mx);
                PublicaX.npaginad = PublicaX.npaginar + 1;
                PublicaX.andamentopaginad = 45;
            };
            foreach(Modelos Mx in new Modelos[] {Modelos.M2,Modelos.M4})
            {
                PublicaHTML PublicaX = PublicaHTML.Mx(Mx);
                PublicaX.npaginad = PublicaX.npaginar;
                PublicaX.andamentopaginad = PublicaX.andamentopaginar;
            };
 
     
 
            despesasdg = new SortedList();
            despesasmo = new SortedList();
            despesasad = new SortedList();
            GeralDespesas();
            foreach (dsBalLocal.BalanceteRow rowBal in dsBalLocal1.Balancete)
            {
                if (rowBal.Tipopag.StartsWith("1"))
                    continue;
                if (rowBal.Tipopag.StartsWith("2"))
                    continue;
                Pcsvalor.soma(rowBal.Tipopag, (decimal)rowBal.valorpago);
            };

            
   
 
            
            dPublicar = new dllpublicaBalancete.WSSincBancos.dBAL();
            WSSincBancos.dBAL.BalanceteTotaisRow rowTotal = dPublicar.BalanceteTotais.NewBalanceteTotaisRow();
            rowTotal.BAL = BAL;
            rowTotal.EMP = EMPArquivo;
            rowTotal.acc = (decimal)rowCon.saldoant;
            rowTotal.s1cc = (decimal)rowCon.saldoant + Pcsvalor.TotalAcumulado();
            rowTotal.s1fr = (decimal)rowCon.saldofrfec;
            rowTotal.s113 = (decimal)rowCon.saldo13fec;
            rowTotal.s1po = (decimal)rowCon.saldoofec;
            rowTotal.s1psf = (decimal)rowCon.saldosafec;
            rowTotal.s1acp = (decimal)rowCon.saldoafec;
            rowTotal.s1af = (decimal)rowCon.saldoapfec;
            rowTotal.totrec = totalrec;
            rowTotal.totdesp= totaldesp;            
            //rowTotal.totdg  = totdg;
            //rowTotal.totmo  = totmo;
            //rowTotal.totad  = totad;
            //rowTotal.codcon = CODCON;
            //rowTotal.competencia = Compet.ToStringN();
            dPublicar.BalanceteTotais.AddBalanceteTotaisRow(rowTotal);
            //rowTotal.anoi   .AsInteger:= anoi;
            //rowTotal.mesi   .AsInteger:= mesi;
            foreach(dsBalLocal.rxrec2Row rowrec2 in dsBalLocal1.rxrec2)
            {
                WSSincBancos.dBAL.BalanceteDetRow rowdet = dPublicar.BalanceteDet.NewBalanceteDetRow();
                rowdet.BAL = BAL;
                rowdet.EMP = EMPArquivo;                
                rowdet.titulo   = rowrec2.descricao;
                rowdet.quadro   = 1;
                rowdet.valor = rowrec2.subtotal;
                rowdet.negrito  = false;                
                dPublicar.BalanceteDet.AddBalanceteDetRow(rowdet);
            };
            SortedList Ordenado = new SortedList();
            int n =1;
            foreach (dsBalLocal.rxrec1Row row in dsBalLocal1.rxrec1)
                Ordenado.Add(string.Format("{0:000}{1:00000}", row.ordem, n++), row);
            foreach (dsBalLocal.rxrec1Row row in Ordenado.Values)
            {
                WSSincBancos.dBAL.BalanceteDetRow rowdet = dPublicar.BalanceteDet.NewBalanceteDetRow();
                rowdet.BAL = BAL;
                rowdet.EMP = EMPArquivo;                
                rowdet.titulo = row.titulo;
                rowdet.quadro = 2;
                rowdet.valor = row.valor;
                rowdet.negrito = row.negrito;
                dPublicar.BalanceteDet.AddBalanceteDetRow(rowdet);
            };
                                
            listadespexp(despesasdg,3);
            listadespexp(despesasmo,4);
            listadespexp(despesasad,5);
            try
            {
                if (modelo == 0)
                {
                    byte[][] Impressos = GeraPublicao();
                    return WS.PublicaBALM(Criptografa(), dPublicar, Compet.CompetenciaBind, CODCON, Impressos[0], Impressos[1], Impressos[2], Impressos[3]);
                }
                else
                {
                    

                    
                    string Arquivo = Arquivo = String.Format("{0}{1:ddMMyyhhmmss}.html", Path, DateTime.Now);
                    if (!System.IO.Directory.Exists(Path))
                        System.IO.Directory.CreateDirectory(Path);
                    GravaTMP(PublicaHTML.Mx((Modelos)(modelo - 1)),Arquivo);
                    return Arquivo;
                }


                    
            }
            catch (Exception e)
            {
                return "N�o Publicado\r\n"+e.Message;
            }
        }

        private bool EmTeste = false;

        private void carregaCreditosQuadradinhos(int CON,DateTime DI, DateTime DF)
        {
            dsBalLocal1.CreditosTableAdapter.Fill(dsBalLocal1.Creditos, CON, DI, DF);
            foreach (dsBalLocal.CreditosRow row in dsBalLocal1.Creditos)
            {
                dsBalLocal.BalanceteRow rowCredito = dsBalLocal1.Balancete.NewBalanceteRow();
                rowCredito.Apartamento = row.APTNumero;
                rowCredito.Bloco = row.BLOCodigo;
                rowCredito.competencia = string.Format("{0:00}{1:0000}",row.BOLCompetenciaMes,row.BOLCompetenciaAno);
                rowCredito.datalancamento = row.BOLPagamento;
                if (!row.PLADescricaoLivre)
                    rowCredito.descricao = row.PLADescricao;
                else
                    if (row.BODMensagem.Contains(" (Pagamento Parcial)"))
                        rowCredito.descricao = row.BODMensagem.Replace(" (Pagamento Parcial)", "").Trim();
                    else
                        rowCredito.descricao = row.BODMensagem;
                rowCredito.numeroboleto = row.BOL;
                rowCredito.Tipopag = row.BOD_PLA;
                rowCredito.valorpago = (float)row.BODValor;
                rowCredito.valorprevisto = (float)row.BOLValorPrevisto;
                rowCredito.vencto = row.BOLVencto;
                dsBalLocal1.Balancete.AddBalanceteRow(rowCredito);
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="_Path"></param>
        /// <param name="modelo"></param>
        /// <returns></returns>
        public string TesteNovosDados(string _Path, int modelo)
        {
            EmTeste = true;
            Path = _Path + @"\TMP\";
            rowCON = FrameworkProc.datasets.dCondominiosAtivos.dCondominiosAtivosST.CONDOMINIOS.FindByCON(Compet.CON);
            dsBalLocal1 = new dsBalLocal();


            //saldos
            //if (dsBalLocal1.CondominiosTableAdapter.Fill(dsBalLocal1.Condominios, CODCON) == 0)
            //    return "Condominio n�o encontrado";
            //rowCon = dsBalLocal1.Condominios[0];
            grupoant = -1;
            totant = 0;
            descant = ultimogrupo3d = ultimoglanc = "";
            totdg = totmo = totad = totalrec = totaldesp = totalgrupo3d = totalgrupolanc = 0;
            Pcsvalor = new pcsvalor();
            
            //string ComandoBOL = "SELECT BOLVencto, BOLValorPrevisto FROM BOLetos WHERE (BOL = @P1)";

            carregaCreditosQuadradinhos(Compet.CON, DI, DF);

            /*
            dsBalLocal.BalanceteTableAdapter.FillBy(dsBalLocal1.Balancete, CODCON, DI, DF);
            foreach (dsBalLocal.BalanceteRow rowBAL in dsBalLocal1.Balancete)
            {
                if (!rowBAL.IsnumdocNull() && (rowBAL.numdoc != 0))
                {
                    System.Data.DataRow DR = TableAdapter.ST().BuscaSQLRow(ComandoBOL, rowBAL.numdoc);
                    if (DR != null)
                    {
                        if (DR["BOLValorPrevisto"] != DBNull.Value)
                            rowBAL.valorprevisto = (float)((decimal)DR["BOLValorPrevisto"]);
                        if (DR["BOLVencto"] != DBNull.Value) ;
                        rowBAL.vencto = (DateTime)DR["BOLVencto"];
                    }
                }
            }
            */





            Gerarecebimentos();
            foreach (Modelos Mx in new Modelos[] { Modelos.M1, Modelos.M3 })
            {
                PublicaHTML PublicaX = PublicaHTML.Mx(Mx);
                PublicaX.npaginad = PublicaX.npaginar + 1;
                PublicaX.andamentopaginad = 45;
            };
            foreach (Modelos Mx in new Modelos[] { Modelos.M2, Modelos.M4 })
            {
                PublicaHTML PublicaX = PublicaHTML.Mx(Mx);
                PublicaX.npaginad = PublicaX.npaginar;
                PublicaX.andamentopaginad = PublicaX.andamentopaginar;
            };



            despesasdg = new SortedList();
            despesasmo = new SortedList();
            despesasad = new SortedList();
            //despesas
            /*
            GeralDespesas();
            foreach (dsBalLocal.BalanceteRow rowBal in dsBalLocal1.Balancete)
            {
                if (rowBal.Tipopag.StartsWith("1"))
                    continue;
                if (rowBal.Tipopag.StartsWith("2"))
                    continue;
                Pcsvalor.soma(rowBal.Tipopag, (decimal)rowBal.valorpago);
            };
            */




            dPublicar = new dllpublicaBalancete.WSSincBancos.dBAL();
            //total
            /*
            WSSincBancos.dBAL.totaisRow rowTotal = dPublicar.totais.NewtotaisRow();
            rowTotal.status = 3;
            rowTotal.acc = (decimal)rowCon.saldoant;
            rowTotal.s1cc = (decimal)rowCon.saldoant + Pcsvalor.TotalAcumulado();
            rowTotal.s1fr = (decimal)rowCon.saldofrfec;
            rowTotal.s113 = (decimal)rowCon.saldo13fec;
            rowTotal.s1po = (decimal)rowCon.saldoofec;
            rowTotal.s1psf = (decimal)rowCon.saldosafec;
            rowTotal.s1acp = (decimal)rowCon.saldoafec;
            rowTotal.s1af = (decimal)rowCon.saldoapfec;
            rowTotal.totrec = totalrec;
            rowTotal.totdesp = totaldesp;
            rowTotal.totdg = totdg;
            rowTotal.totmo = totmo;
            rowTotal.totad = totad;
            rowTotal.codcon = CODCON;
            rowTotal.competencia = Compet.ToStringN();
            dPublicar.totais.AddtotaisRow(rowTotal);
            */


            foreach (dsBalLocal.rxrec2Row rowrec2 in dsBalLocal1.rxrec2)
            {
                WSSincBancos.dBAL.BalanceteDetRow rowdet = dPublicar.BalanceteDet.NewBalanceteDetRow();
                rowdet.BAL = BAL;
                rowdet.EMP = EMPArquivo;                
                rowdet.titulo = rowrec2.descricao;
                rowdet.quadro = 1;
                rowdet.valor = rowrec2.subtotal;
                rowdet.negrito = false;                
                dPublicar.BalanceteDet.AddBalanceteDetRow(rowdet);
            };
            SortedList Ordenado = new SortedList();
            int n = 1;
            foreach (dsBalLocal.rxrec1Row row in dsBalLocal1.rxrec1)
                Ordenado.Add(string.Format("{0:000}{1:00000}", row.ordem, n++), row);
            foreach (dsBalLocal.rxrec1Row row in Ordenado.Values)
            {
                WSSincBancos.dBAL.BalanceteDetRow rowdet = dPublicar.BalanceteDet.NewBalanceteDetRow();
                rowdet.BAL = BAL;
                rowdet.EMP = EMPArquivo;                
                rowdet.titulo = row.titulo;
                rowdet.quadro = 2;
                rowdet.valor = row.valor;
                rowdet.negrito = row.negrito;
                dPublicar.BalanceteDet.AddBalanceteDetRow(rowdet);
            };

            listadespexp(despesasdg, 3);
            listadespexp(despesasmo, 4);
            listadespexp(despesasad, 5);
            try
            {
                if (modelo == 0)
                {
                    byte[][] Impressos = GeraPublicao();
                    //return WS.PublicaBALM(Criptografa(), dPublicar, Impressos[0], Impressos[1], Impressos[2], Impressos[3]);
                    System.IO.File.WriteAllBytes(@"c:\clientes\neon\pagina\balancetes\m1.html", Impressos[0]);
                    System.IO.File.WriteAllBytes(@"c:\clientes\neon\pagina\balancetes\m1.txt", Impressos[0]);
                    System.IO.File.WriteAllBytes(@"c:\clientes\neon\pagina\balancetes\m2.html", Impressos[1]);
                    System.IO.File.WriteAllBytes(@"c:\clientes\neon\pagina\balancetes\m2.txt", Impressos[1]);
                    System.IO.File.WriteAllBytes(@"c:\clientes\neon\pagina\balancetes\m3.html", Impressos[2]);
                    System.IO.File.WriteAllBytes(@"c:\clientes\neon\pagina\balancetes\m3.txt", Impressos[2]);
                    System.IO.File.WriteAllBytes(@"c:\clientes\neon\pagina\balancetes\m4.html", Impressos[3]);
                    System.IO.File.WriteAllBytes(@"c:\clientes\neon\pagina\balancetes\m4.txt", Impressos[3]);
                    return "ok";
                }
                else
                {



                    string Arquivo = Arquivo = String.Format("{0}{1:ddMMyyhhmmss}.html", Path, DateTime.Now);
                    if (!System.IO.Directory.Exists(Path))
                        System.IO.Directory.CreateDirectory(Path);
                    GravaTMP(PublicaHTML.Mx((Modelos)(modelo - 1)), Arquivo);
                    return Arquivo;
                }

                



            }
            catch (Exception e)
            {
                return "N�o Publicado\r\n" + e.Message;
            }
        }

        private byte[][] GeraPublicao()
        {
            byte[][] retorno = new byte[4][];
            foreach (PublicaHTML PublicaX in PublicaHTML.PublicaHTMLs.Values)
            {
                string strPagina = CodigosHTML(PublicaX.ppModelo, PublicaX);               
                retorno[(int)PublicaX.M] = strPagina == "" ? null : Encoding.GetEncoding(1252).GetBytes(strPagina);
            }
            return retorno;
        }

        private void GravaTMP(PublicaHTML PublicaX,string Arquivo)
        {
            string strPagina = CodigosHTML(PublicaX.ppModelo, PublicaX);
            System.IO.File.WriteAllText(Arquivo, strPagina);
        }

        private string CodigosHTML(CodigosPreDefinidos CodigoModelo, PublicaHTML PublicaX)
        {
            string retorno;
            switch (CodigoModelo)
            {
                case CodigosPreDefinidos.ppresumo:
                    retorno =
" \r\n" +
"  <p align=\"center\"><b><font size=\"3\">Resumo dos Saldos</font></b></p> \r\n" +
"  <hr />\r\n" +
"\r\n" +
"<div align=\"left\"><b><font size=\"3\">Saldo Anterior</font></b></div> \r\n" +
"  <table width=\"511\" border=\"0\" cellspacing=\"0\" cellpadding=\"2\">\r\n" +
"    <tr> \r\n" +
"      \r\n" +
"    <td width=\"31\">&nbsp;</td>\r\n" +
"      \r\n" +
"    <td width=\"293\"><font size=\"2\">Conta Corrente:</font></td>\r\n" +
"      <td width=\"175\"> \r\n" +
"        <div align=\"right\"><font size=\"2\">R$<#ACC></font></div>\r\n" +
"      </td>\r\n" +
"    </tr>\r\n" +
"    <tr> \r\n" +
"      \r\n" +
"    <td width=\"31\">&nbsp;</td>\r\n" +
"      \r\n" +
"    <td width=\"293\"><font size=\"2\">Poupan&ccedil;a Fundo Reserva:</font></td>\r\n" +
"      <td width=\"175\"> \r\n" +
"        <div align=\"right\"><font size=\"2\">R$<#Afr></font></div>\r\n" +
"      </td>\r\n" +
"    </tr>\r\n" +
"    <tr> \r\n" +
"      \r\n" +
"    <td width=\"31\">&nbsp;</td>\r\n" +
"      \r\n" +
"    <td width=\"293\"><font size=\"2\">Poupan&ccedil;a 13&deg; Sal&aacute;rio:</font></td>\r\n" +
"      <td width=\"175\"> \r\n" +
"        <div align=\"right\"><font size=\"2\">R$<#a13></font></div>\r\n" +
"      </td>\r\n" +
"    </tr>\r\n" +
"    <tr> \r\n" +
"      \r\n" +
"    <td width=\"31\">&nbsp;</td>\r\n" +
"      \r\n" +
"    <td width=\"293\"><font size=\"2\">Poupan&ccedil;a Obras:</font></td>\r\n" +
"      <td width=\"175\"> \r\n" +
"        <div align=\"right\"><font size=\"2\">R$<#apo></font></div>\r\n" +
"      </td>\r\n" +
"    </tr>\r\n" +
"    <tr> \r\n" +
"      \r\n" +
"    <td width=\"31\">&nbsp;</td>\r\n" +
"      \r\n" +
"    <td width=\"293\"><font size=\"2\">Poupan&ccedil;a S.Festas/Churrasqueira:</font></td>\r\n" +
"      <td width=\"175\"> \r\n" +
"        <div align=\"right\"><font size=\"2\">R$<#apsf></font></div>\r\n" +
"      </td>\r\n" +
"    </tr>\r\n" +
"    <tr> \r\n" +
"      \r\n" +
"    <td width=\"31\">&nbsp;</td>\r\n" +
"      \r\n" +
"    <td width=\"293\"><font size=\"2\">Aplica&ccedil;&atilde;o de curto prazo:</font></td>\r\n" +
"      <td width=\"175\"> \r\n" +
"        <div align=\"right\"><font size=\"2\">R$<#aacp></font></div>\r\n" +
"      </td>\r\n" +
"    </tr>\r\n" +
"    <tr> \r\n" +
"      \r\n" +
"    <td width=\"31\">&nbsp;</td>\r\n" +
"      \r\n" +
"    <td width=\"293\"><font size=\"2\">Aplica&ccedil;&atilde;o Financeira:</font></td>\r\n" +
"      <td width=\"175\"> \r\n" +
"        <div align=\"right\"><font size=\"2\">R$<#aaf></font></div>\r\n" +
"      </td>\r\n" +
"    </tr>\r\n" +
"  </table>\r\n"+
"<div align=\"center\"> \r\n" + 
"    <hr />\r\n" + 
"  </div>\r\n" + 
"  <div align=\"left\"><b><font size=\"3\">Movimenta&ccedil;&atilde;o do M&ecirc;s \r\n" + 
"    - Conta Corrente</font></b> \r\n" + 
"    \r\n" + 
"  <table  border=\"0\" cellspacing=\"0\" cellpadding=\"2\" width=\"650\">\r\n" + 
"    <tr> \r\n" + 
"        \r\n" + 
"      <td width=\"10\">&nbsp;</td>\r\n" + 
"        \r\n" + 
"      <td width=\"211\"><i><font size=\"3\"><b><u>D&Eacute;BITO</u></b></font></i></td>\r\n" + 
"        \r\n" + 
"      <td width=\"88\">&nbsp;</td>\r\n" + 
"        \r\n" + 
"      <td width=\"9\">&nbsp;</td>\r\n" + 
"        \r\n" + 
"      <td width=\"213\"><i><font size=\"3\"><b><u>CR&Eacute;DITO</u></b></font></i></td>\r\n" + 
"        \r\n" + 
"      <td width=\"95\">&nbsp;</td>\r\n" + 
"      </tr>\r\n" + 
"      <tr> \r\n" + 
"        \r\n" + 
"      <td width=\"10\">&nbsp;</td>\r\n" + 
"        \r\n" + 
"      <td width=\"211\"><font size=\"2\">Despesas:</font></td>\r\n" + 
"        \r\n" + 
"      <td width=\"88\"> \r\n" + 
"        <div align=\"right\"><font size=\"2\">R$<#m1CC></font></div>\r\n" + 
"        </td>\r\n" + 
"        \r\n" + 
"      <td width=\"9\">&nbsp;</td>\r\n" + 
"        \r\n" + 
"      <td width=\"213\"><font size=\"2\">Recebimentos:</font></td>\r\n" + 
"        \r\n" + 
"      <td width=\"95\"> \r\n" + 
"        <div align=\"right\"><font size=\"2\">R$<#m2CC></font></div>\r\n" + 
"        </td>\r\n" + 
"      </tr>\r\n" + 
"      <tr> \r\n" + 
"        \r\n" + 
"      <td width=\"10\">&nbsp;</td>\r\n" + 
"        \r\n" + 
"      <td width=\"211\"><font size=\"2\">Aplica&ccedil;&atilde;o Fundo Reserva:</font></td>\r\n" + 
"        \r\n" + 
"      <td width=\"88\"> \r\n" + 
"        <div align=\"right\"><font size=\"2\">R$<#m1fr></font></div>\r\n" + 
"        </td>\r\n" + 
"        \r\n" + 
"      <td width=\"9\">&nbsp;</td>\r\n" + 
"        \r\n" + 
"      <td width=\"213\"><font size=\"2\">Resgate Poupan&ccedil;a F. Reserva:</font></td>\r\n" + 
"        \r\n" + 
"      <td width=\"95\"> \r\n" + 
"        <div align=\"right\"><font size=\"2\">R$<#m2fr></font></div>\r\n" + 
"        </td>\r\n" + 
"      </tr>\r\n" + 
"      <tr> \r\n" + 
"        \r\n" + 
"      <td width=\"10\">&nbsp;</td>\r\n" + 
"        \r\n" + 
"      <td width=\"211\"><font size=\"2\">Aplica&ccedil;&atilde;o Poupan&ccedil;a 13&deg; \r\n" + 
"        Sal.:</font></td>\r\n" + 
"        \r\n" + 
"      <td width=\"88\"> \r\n" + 
"        <div align=\"right\"><font size=\"2\">R$<#m113></font></div>\r\n" + 
"        </td>\r\n" + 
"        \r\n" + 
"      <td width=\"9\">&nbsp;</td>\r\n" + 
"        \r\n" + 
"      <td width=\"213\"><font size=\"2\">Resgate Poupan&ccedil;a 13&deg; Sal.:</font></td>\r\n" + 
"        \r\n" + 
"      <td width=\"95\"> \r\n" + 
"        <div align=\"right\"><font size=\"2\">R$<#m213></font></div>\r\n" + 
"        </td>\r\n" + 
"      </tr>\r\n" + 
"      <tr> \r\n" + 
"        \r\n" + 
"      <td width=\"10\">&nbsp;</td>\r\n" + 
"        \r\n" + 
"      <td width=\"211\"><font size=\"2\">Aplica&ccedil;&atilde;o Poupan&ccedil;a Obras:</font></td>\r\n" + 
"        \r\n" + 
"      <td width=\"88\"> \r\n" + 
"        <div align=\"right\"><font size=\"2\">R$<#m1po></font></div>\r\n" + 
"        </td>\r\n" + 
"        \r\n" + 
"      <td width=\"9\">&nbsp;</td>\r\n" + 
"        \r\n" + 
"      <td width=\"213\"><font size=\"2\">Resgate Poupan&ccedil;a Obras:</font></td>\r\n" + 
"        \r\n" + 
"      <td width=\"95\"> \r\n" + 
"        <div align=\"right\"><font size=\"2\">R$<#m2po></font></div>\r\n" + 
"        </td>\r\n" + 
"      </tr>\r\n" + 
"      <tr> \r\n" + 
"        \r\n" + 
"      <td width=\"10\">&nbsp;</td>\r\n" + 
"        \r\n" + 
"      <td width=\"211\"><font size=\"2\">Aplica&ccedil;&atilde;o Poupan&ccedil;a S.Festas:</font></td>\r\n" + 
"        \r\n" + 
"      <td width=\"88\"> \r\n" + 
"        <div align=\"right\"><font size=\"2\">R$<#m1psf></font></div>\r\n" + 
"        </td>\r\n" + 
"        \r\n" + 
"      <td width=\"9\">&nbsp;</td>\r\n" + 
"        \r\n" + 
"      <td width=\"213\"><font size=\"2\">Resgate Poupan&ccedil;a S.Festas:</font></td>\r\n" + 
"        \r\n" + 
"      <td width=\"95\"> \r\n" + 
"        <div align=\"right\"><font size=\"2\">R$<#m2psf></font></div>\r\n" + 
"        </td>\r\n" + 
"      </tr>\r\n" + 
"      <tr> \r\n" + 
"        \r\n" + 
"      <td width=\"10\">&nbsp;</td>\r\n" + 
"        \r\n" + 
"      <td width=\"211\"><font size=\"2\">Aplica&ccedil;&atilde;o de curto prazo:</font></td>\r\n" + 
"        \r\n" + 
"      <td width=\"88\"> \r\n" + 
"        <div align=\"right\"><font size=\"2\">R$<#m1acp></font></div>\r\n" + 
"        </td>\r\n" + 
"        \r\n" + 
"      <td width=\"9\">&nbsp;</td>\r\n" + 
"        \r\n" + 
"      <td width=\"213\"><font size=\"2\">Resgate Curto prazo:</font></td>\r\n" + 
"        \r\n" + 
"      <td width=\"95\"> \r\n" + 
"        <div align=\"right\"><font size=\"2\">R$<#m2acp></font></div>\r\n" + 
"        </td>\r\n" + 
"      </tr>\r\n" + 
"      <tr> \r\n" + 
"        \r\n" + 
"      <td width=\"10\">&nbsp;</td>\r\n" + 
"        \r\n" + 
"      <td width=\"211\"><font size=\"2\">Aplica&ccedil;&atilde;o Financeira:</font></td>\r\n" + 
"        \r\n" + 
"      <td width=\"88\"> \r\n" + 
"        <div align=\"right\"><font size=\"2\">R$<#m1af></font></div>\r\n" + 
"        </td>\r\n" + 
"        \r\n" + 
"      <td width=\"9\">&nbsp;</td>\r\n" + 
"        \r\n" + 
"      <td width=\"213\"><font size=\"2\">Resgate Financeira:</font></td>\r\n" + 
"        \r\n" + 
"      <td width=\"95\"> \r\n" + 
"        <div align=\"right\"><font size=\"2\">R$<#m2af></font></div>\r\n" + 
"        </td>\r\n" + 
"      </tr>\r\n" + 
"    </table>\r\n" + 
"    <hr />\r\n" + 
"  </div>\r\n" + 
"  <font size=\"3\"><b>Saldo Fechamento</b></font>\r\n" + 
"  \r\n" + 
"<table  border=\"0\" cellspacing=\"0\" cellpadding=\"2\" width=\"650\">\r\n" + 
"  <tr> \r\n" + 
"      <td width=\"44\">&nbsp;</td>\r\n" + 
"      <td width=\"280\"><i><font size=\"3\"><b><u>SALDO</u></b></font></i></td>\r\n" + 
"      <td width=\"153\">&nbsp;</td>\r\n" + 
"      <td colspan=\"2\"> \r\n" + 
"        <div align=\"right\"><i><font size=\"3\"><b><u>RENTABILIDADE DO M&Ecirc;S</u></b></font></i></div>\r\n" + 
"      </td>\r\n" + 
"    </tr>\r\n" + 
"    <tr> \r\n" + 
"      <td width=\"44\">&nbsp;</td>\r\n" + 
"      <td width=\"280\"><font size=\"2\">Conta Corrente:</font></td>\r\n" + 
"      <td width=\"153\"> \r\n" + 
"        <div align=\"right\"><font size=\"2\">R$<#s1CC></font></div>\r\n" + 
"      </td>\r\n" + 
"      <td width=\"68\">&nbsp;</td>\r\n" + 
"      <td width=\"188\"> \r\n" + 
"        <div align=\"right\"></div>\r\n" + 
"      </td>\r\n" + 
"    </tr>\r\n" + 
"    <tr> \r\n" + 
"      <td width=\"44\">&nbsp;</td>\r\n" + 
"      <td width=\"280\"><font size=\"2\">Poupan&ccedil;a Fundo Reserva:</font></td>\r\n" + 
"      <td width=\"153\"> \r\n" + 
"        <div align=\"right\"><font size=\"2\">R$<#s1fr></font></div>\r\n" + 
"      </td>\r\n" + 
"      <td width=\"68\">&nbsp;</td>\r\n" + 
"      <td width=\"188\"> \r\n" + 
"        <div align=\"right\"><font size=\"2\">R$<#r1fr></font></div>\r\n" + 
"      </td>\r\n" + 
"    </tr>\r\n" + 
"    <tr> \r\n" + 
"      <td width=\"44\">&nbsp;</td>\r\n" + 
"      <td width=\"280\"><font size=\"2\">Poupan&ccedil;a 13&deg; Sal&aacute;rio:</font></td>\r\n" + 
"      <td width=\"153\"> \r\n" + 
"        <div align=\"right\"><font size=\"2\">R$<#s113></font></div>\r\n" + 
"      </td>\r\n" + 
"      <td width=\"68\">&nbsp;</td>\r\n" + 
"      <td width=\"188\"> \r\n" + 
"        <div align=\"right\"><font size=\"2\">R$<#r113></font></div>\r\n" + 
"      </td>\r\n" + 
"    </tr>\r\n" + 
"    <tr> \r\n" + 
"      <td width=\"44\">&nbsp;</td>\r\n" + 
"      <td width=\"280\"><font size=\"2\">Poupan&ccedil;a Obras:</font></td>\r\n" + 
"      <td width=\"153\"> \r\n" + 
"        <div align=\"right\"><font size=\"2\">R$<#s1po></font></div>\r\n" + 
"      </td>\r\n" + 
"      <td width=\"68\">&nbsp;</td>\r\n" + 
"      <td width=\"188\"> \r\n" + 
"        <div align=\"right\"><font size=\"2\">R$<#r1po></font></div>\r\n" + 
"      </td>\r\n" + 
"    </tr>\r\n" + 
"    <tr> \r\n" + 
"      <td width=\"44\">&nbsp;</td>\r\n" + 
"      <td width=\"280\"><font size=\"2\">Poupan&ccedil;a S.Festas/Churrasqueira:</font></td>\r\n" + 
"      <td width=\"153\"> \r\n" + 
"        <div align=\"right\"><font size=\"2\">R$<#s1psf></font></div>\r\n" + 
"      </td>\r\n" + 
"      <td width=\"68\">&nbsp;</td>\r\n" + 
"      <td width=\"188\"> \r\n" + 
"        <div align=\"right\"><font size=\"2\">R$<#r1psf></font></div>\r\n" + 
"      </td>\r\n" + 
"    </tr>\r\n" + 
"    <tr> \r\n" + 
"      <td width=\"44\">&nbsp;</td>\r\n" + 
"      <td width=\"280\"><font size=\"2\">Aplica&ccedil;&atilde;o de curto prazo:</font></td>\r\n" + 
"      <td width=\"153\"> \r\n" + 
"        <div align=\"right\"><font size=\"2\">R$<#s1acp></font></div>\r\n" + 
"      </td>\r\n" + 
"      <td width=\"68\">&nbsp;</td>\r\n" + 
"      <td width=\"188\"> \r\n" + 
"        <div align=\"right\"><font size=\"2\">R$<#r1acp></font></div>\r\n" + 
"      </td>\r\n" + 
"    </tr>\r\n" + 
"    <tr> \r\n" + 
"      <td width=\"44\">&nbsp;</td>\r\n" + 
"      <td width=\"280\"><font size=\"2\">Aplica&ccedil;&atilde;o Financeira:</font></td>\r\n" + 
"      <td width=\"153\"> \r\n" + 
"        <div align=\"right\"><font size=\"2\">R$<#s1af></font></div>\r\n" + 
"      </td>\r\n" + 
"      <td width=\"68\">&nbsp;</td>\r\n" + 
"      <td width=\"188\"> \r\n" + 
"        <div align=\"right\"><font size=\"2\">R$<#r1af></font></div>\r\n" + 
"      </td>\r\n" + 
"    </tr>\r\n" + 
"  </table>\r\n" + 
"  <hr />\r\n" + 
"  <div align=\"left\"></div>\r\n" + 
"  <font size=\"3\"><b>Saldo Gera</b></font><b>l</b>\r\n" + 
"  \r\n" + 
"<table border=\"0\" cellspacing=\"0\" cellpadding=\"2\" width=\"650\">\r\n" + 
"  <tr> \r\n" + 
"      <td width=\"36\">&nbsp;</td>\r\n" + 
"      <td width=\"553\"><b><font size=\"2\">Saldo Dispon&iacute;vel (C/C + Aplic. \r\n" + 
"        Curto prazo):</font></b></td>\r\n" + 
"      <td width=\"164\"> \r\n" + 
"        <div align=\"right\"><font size=\"2\"><b>R$ <#scp></b></font></div>\r\n" + 
"      </td>\r\n" + 
"    </tr>\r\n" + 
"    <tr> \r\n" + 
"      <td width=\"36\">&nbsp;</td>\r\n" + 
"      <td width=\"553\"><b><font size=\"2\">Poupan&ccedil;a (FR+13&deg;+Obras+Sal&atilde;o/Churras)+Aplic.Financeiras:</font></b></td>\r\n" + 
"      <td width=\"164\"> \r\n" + 
"        <div align=\"right\"><font size=\"2\"><b>R$ <#pcp></b></font></div>\r\n" + 
"      </td>\r\n" + 
"    </tr>\r\n" + 
"  </table>";
                    retorno = retorno.Replace("<#ACC>", rowCon.saldoant.ToString("n2"));
                    retorno = retorno.Replace("<#Afr>", rowCon.saldofrant.ToString("n2"));
                    retorno = retorno.Replace("<#a13>", rowCon.saldo13ant.ToString("n2"));
                    retorno = retorno.Replace("<#apo>", rowCon.saldooant.ToString("n2"));
                    retorno = retorno.Replace("<#apsf>", rowCon.saldosaant.ToString("n2"));
                    retorno = retorno.Replace("<#aacp>", rowCon.saldoaant.ToString("n2"));
                    retorno = retorno.Replace("<#aaf>", rowCon.saldoapant.ToString("n2"));

                    retorno = retorno.Replace("<#m1CC>", Pcsvalor.Valor(pcs.pc2).ToString("n2"));
                    retorno = retorno.Replace("<#m1fr>", Pcsvalor.Valor(pcs.pc51).ToString("n2"));
                    retorno = retorno.Replace("<#m113>", Pcsvalor.Valor(pcs.pc52).ToString("n2"));
                    retorno = retorno.Replace("<#m1po>", Pcsvalor.Valor(pcs.pc53).ToString("n2"));
                    retorno = retorno.Replace("<#m1psf>", Pcsvalor.Valor(pcs.pc54).ToString("n2"));
                    retorno = retorno.Replace("<#m1acp>", Pcsvalor.Valor(pcs.pc3).ToString("n2"));
                    retorno = retorno.Replace("<#m1af>", Pcsvalor.Valor(pcs.pc7).ToString("n2"));

                    retorno = retorno.Replace("<#m2CC>", Pcsvalor.Valor(pcs.pc1).ToString("n2"));
                    retorno = retorno.Replace("<#m2fr>", Pcsvalor.Valor(pcs.pc61).ToString("n2"));
                    retorno = retorno.Replace("<#m213>", Pcsvalor.Valor(pcs.pc62).ToString("n2"));
                    retorno = retorno.Replace("<#m2po>", Pcsvalor.Valor(pcs.pc63).ToString("n2"));
                    retorno = retorno.Replace("<#m2psf>", Pcsvalor.Valor(pcs.pc64).ToString("n2"));
                    retorno = retorno.Replace("<#m2acp>", Pcsvalor.Valor(pcs.pc4).ToString("n2"));
                    retorno = retorno.Replace("<#m2af>", Pcsvalor.Valor(pcs.pc8).ToString("n2"));

                    retorno = retorno.Replace("<#s1CC>", string.Format("{0:n2}",(decimal)rowCon.saldoant + Pcsvalor.TotalAcumulado()));
                    retorno = retorno.Replace("<#s1fr>", rowCon.saldofrfec.ToString("n2"));
                    retorno = retorno.Replace("<#s113>", rowCon.saldo13fec.ToString("n2"));
                    retorno = retorno.Replace("<#s1po>", rowCon.saldoofec.ToString("n2"));
                    retorno = retorno.Replace("<#s1psf>", rowCon.saldosafec.ToString("n2"));
                    retorno = retorno.Replace("<#s1acp>", rowCon.saldoafec.ToString("n2"));
                    retorno = retorno.Replace("<#s1af>", rowCon.saldoapfec.ToString("n2"));

                    retorno = retorno.Replace("<#r1fr>", string.Format("{0:n2}",(decimal)(rowCon.saldofrfec - rowCon.saldofrant) + Pcsvalor.Valor(pcs.pc61) - Pcsvalor.Valor(pcs.pc51)));
                    retorno = retorno.Replace("<#r113>", string.Format("{0:n2}",(decimal)(rowCon.saldo13fec - rowCon.saldo13ant) + Pcsvalor.Valor(pcs.pc62) - Pcsvalor.Valor(pcs.pc52)));
                    retorno = retorno.Replace("<#r1po>", string.Format("{0:n2}",(decimal)(rowCon.saldoofec - rowCon.saldooant) + Pcsvalor.Valor(pcs.pc63) - Pcsvalor.Valor(pcs.pc53)));
                    retorno = retorno.Replace("<#r1psf>",string.Format("{0:n2}",(decimal)(rowCon.saldosafec - rowCon.saldosaant) + Pcsvalor.Valor(pcs.pc64) - Pcsvalor.Valor(pcs.pc54)));
                    retorno = retorno.Replace("<#r1acp>",string.Format("{0:n2}",(decimal)(rowCon.saldoafec - rowCon.saldoaant) + Pcsvalor.Valor(pcs.pc4) - Pcsvalor.Valor(pcs.pc3)));
                    retorno = retorno.Replace("<#r1af>", string.Format("{0:n2}", (decimal)(rowCon.saldoapfec - rowCon.saldoapant) + Pcsvalor.Valor(pcs.pc8) - Pcsvalor.Valor(pcs.pc7)));

                    retorno = retorno.Replace("<#scp>", string.Format("{0:n2}", (decimal)(rowCon.saldoant + rowCon.saldoafec) + Pcsvalor.TotalAcumulado()));
                    retorno = retorno.Replace("<#pcp>", string.Format("{0:n2}", rowCon.saldofrfec + rowCon.saldo13fec + rowCon.saldoofec + rowCon.saldosafec + rowCon.saldoapfec));
                                                                                
                    return retorno;

                case CodigosPreDefinidos.ppresumo2:
                    retorno =
"\r\n<!-- RESUMO 2 -->\r\n" +
"  <tr> \r\n" +
"    <td width=\"186\"><font size=\"2\"><b><font size=\"1\">Resumo Saldos</font></b></font></td>\r\n" +
"    <td width=\"99\">&nbsp;</td>\r\n" +
"    <td width=\"40\">&nbsp;</td>\r\n" +
"    <td width=\"210\">&nbsp;</td>\r\n" +
"    <td width=\"93\">&nbsp;</td>\r\n" +
"  </tr>\r\n" +
"  <tr> \r\n" +
"    <td width=\"186\"><font size=\"1\">Conta Corrente:</font></td>\r\n" +
"    <td width=\"99\"> \r\n" +
"      <div align=\"right\"><font size=\"1\">R$<#ACC></font></div>\r\n" +
"    </td>\r\n" +
"    <td width=\"40\"><font size=\"1\"></font></td>\r\n" +
"    <td width=\"210\"><font size=\"1\">Conta Corrente:</font></td>\r\n" +
"    <td width=\"93\"> \r\n" +
"      <div align=\"right\"><font size=\"1\">R$<#s1CC></font></div>\r\n" +
"    </td>\r\n" +
"  </tr>\r\n" +
"  <tr> \r\n" +
"    <td width=\"186\"><font size=\"1\">Poupan&ccedil;a Fundo Reserva:</font></td>\r\n" +
"    <td width=\"99\"> \r\n" +
"      <div align=\"right\"><font size=\"1\">R$<#s1fr></font></div>\r\n" +
"    </td>\r\n" +
"    <td width=\"40\"><font size=\"1\"></font></td>\r\n" +
"    <td width=\"210\"><font size=\"1\">Poupan&ccedil;a S.Festas/Churrasqueira:</font></td>\r\n" +
"    <td width=\"93\"> \r\n" +
"      <div align=\"right\"><font size=\"1\">R$<#s1psf></font></div>\r\n" +
"    </td>\r\n" +
"  </tr>\r\n" +
"  <tr> \r\n" +
"    <td width=\"186\"><font size=\"1\">Poupan&ccedil;a 13&deg; Sal&aacute;rio:</font></td>\r\n" +
"    <td width=\"99\"> \r\n" +
"      <div align=\"right\"><font size=\"1\">R$<#s113></font></div>\r\n" +
"    </td>\r\n" +
"    <td width=\"40\"><font size=\"1\"></font></td>\r\n" +
"    <td width=\"210\"><font size=\"1\">Aplica&ccedil;&atilde;o de curto prazo:</font></td>\r\n" +
"    <td width=\"93\"> \r\n" +
"      <div align=\"right\"><font size=\"1\">R$<#s1acp></font></div>\r\n" +
"    </td>\r\n" +
"  </tr>\r\n" +
"  <tr> \r\n" +
"    <td width=\"186\"><font size=\"1\">Poupan&ccedil;a Obras:</font></td>\r\n" +
"    <td width=\"99\"> \r\n" +
"      <div align=\"right\"><font size=\"1\">R$<#s1po></font></div>\r\n" +
"    </td>\r\n" +
"    <td width=\"40\"><font size=\"1\"></font></td>\r\n" +
"    <td width=\"210\"><font size=\"1\">Aplica&ccedil;&atilde;o Financeira:</font></td>\r\n" +
"    <td width=\"93\"> \r\n" +
"      <div align=\"right\"><font size=\"1\">R$<#s1af></font></div>\r\n" +
"    </td>\r\n" +
"  </tr>\r\n" +
"</table>\r\n" +
"</body>\r\n" +
"</html>\r\n";
                    retorno = retorno.Replace("<#ACC>", rowCon.saldoant.ToString("n2"));
                    retorno = retorno.Replace("<#s1CC>", string.Format("{0:n2}", (decimal)rowCon.saldoant + Pcsvalor.TotalAcumulado()));
                    retorno = retorno.Replace("<#s1fr>", rowCon.saldofrfec.ToString("n2"));
                    retorno = retorno.Replace("<#s113>", rowCon.saldo13fec.ToString("n2"));
                    retorno = retorno.Replace("<#s1po>", rowCon.saldoofec.ToString("n2"));
                    retorno = retorno.Replace("<#s1psf>", rowCon.saldosafec.ToString("n2"));
                    retorno = retorno.Replace("<#s1acp>", rowCon.saldoafec.ToString("n2"));
                    retorno = retorno.Replace("<#s1af>", rowCon.saldoapfec.ToString("n2"));
                    return retorno;

                case CodigosPreDefinidos.ppdespesasalfa:
                    retorno =
"  <tr>\r\n" +
"    <td colspan=\"2\">\r\n" +
"      <hr />\r\n" +
"      <b><font size=\"1\">Despesas Gerais</font></b></td>\r\n" +
"  </tr>\r\n" +
"  <#despesasG> \r\n" +
"  <tr> \r\n" +
"    <td> \r\n" +
"      <div align=\"right\"><font size=\"2\"><b><font size=\"1\">SUB-TOTAL</font></b></font></div>\r\n" +
"    </td>\r\n" +
"    <td width=\"150\"> \r\n" +
"      <div align=\"right\"><font size=\"2\"><b><font size=\"1\">R$<#totdg></font></b></font></div>\r\n" +
"    </td>\r\n" +
"  </tr>\r\n" +
"  <#quebra1>\r\n" +
"  <tr><td colspan=\"2\"><font size=\"1\"><b>Despesas M&atilde;o de Obra</b></font></td></tr>\r\n" +
"  <#despesasMO> \r\n" +
"  <tr> \r\n" +
"    <td> \r\n" +
"      <div align=\"right\"><font size=\"1\"><b>SUB-TOTAL</b></font></div>\r\n" +
"    </td>\r\n" +
"    <td width=\"150\"> \r\n" +
"      <div align=\"right\"><font size=\"1\"><b>R$<#totmo></b></font></div>\r\n" +
"    </td>\r\n" +
"  </tr>\r\n" +
"  <#quebra2>\r\n" +
"  <tr><td colspan=\"2\"><font size=\"1\"><b>Despesas Administrativas</b></font></td></tr>\r\n" +
"  <#despesasAD> \r\n" +
"  <tr> \r\n" +
"    <td> \r\n" +
"      <div align=\"right\"><font size=\"1\"><b>SUB-TOTAL</b></font></div>\r\n" +
"    </td>\r\n" +
"    <td width=\"150\"> \r\n" +
"      <div align=\"right\"><font size=\"1\"><b>R$<#totad></b></font></div>\r\n" +
"    </td>\r\n" +
"  </tr>\r\n" +
"  \r\n" +
"  <tr>\r\n" +
"    <td colspan=\"2\"> \r\n" +
"      <hr />\r\n" +
"    </td>\r\n" +
"  </tr>\r\n" +
"  <tr> \r\n" +
"    <td> \r\n" +
"      <div align=\"right\"><font size=\"1\"><b>Total de despesas</b></font></div>\r\n" +
"    </td>\r\n" +
"    <td width=\"150\"> \r\n" +
"      <div align=\"right\"><font size=\"1\"><b>R$<#totdesp></b></font></div>\r\n" +
"    </td>\r\n" +
"  </tr>\r\n" +
"</table>\r\n" +
"<table width=\"650\" border=\"0\" class=\"impresso\">\r\n" +
"  <#quebra3>\r\n";
                    bool quebrar = false;
                    PublicaX.andamentogeral += PublicaX.andamentopaginar;
                    retorno = retorno.Replace("<#despesasG>", listadesphtml(despesasdg));
                    PublicaX.andamentogeral += 7 + (4 * despesasdg.Count);
                    retorno = retorno.Replace("<#totdg>", totdg.ToString("n2"));
                    quebrar = PublicaX.andamentogeral + 7 + (4 * despesasmo.Count) > 250;
                    retorno = retorno.Replace("<#quebra1>", QuebraDepaginaM2(quebrar,PublicaX,2));
                    retorno = retorno.Replace("<#despesasMO>", listadesphtml(despesasmo));
                    PublicaX.andamentogeral += 7 + (4 * despesasmo.Count);
                    retorno = retorno.Replace("<#totmo>", totmo.ToString("n2"));
                    quebrar = PublicaX.andamentogeral + 7 + (4 * despesasad.Count) > 250;
                    retorno = retorno.Replace("<#quebra2>", QuebraDepaginaM2(quebrar, PublicaX, 2));
                    retorno = retorno.Replace("<#despesasAD>", listadesphtml(despesasad));
                    PublicaX.andamentogeral += 7 + (4 * despesasad.Count);
                    retorno = retorno.Replace("<#totad>", totad.ToString("n2"));
                    retorno = retorno.Replace("<#totdesp>", totaldesp.ToString("n2"));
                    quebrar = PublicaX.andamentogeral + 7 + 9 + 25 > 250;
                    retorno = retorno.Replace("<#quebra3>", QuebraDepaginaM2(quebrar, PublicaX, 5));
                    return retorno;
                case CodigosPreDefinidos.ppbalancete2:
                    retorno =
"<#ppInicioarquivo>" +
"    <tr><td colspan=\"2\"><#recebimentos></td></tr>\r\n" +
"    <tr><td colspan=\"2\"><hr /><div align=\"center\"><b><font size=\"3\">Despesas</font></b></div><hr /></td></tr>" +
"<#ppdespesasalfa>\r\n" +
"<#resumo2>";                    
                    retorno = retorno.Replace("<#ppInicioarquivo>", CodigosHTML(CodigosPreDefinidos.ppInicioarquivo, PublicaX));
                    retorno = retorno.Replace("<#recebimentos>", PublicaX.htmlrecebimentos);
                    retorno = retorno.Replace("<#ppdespesasalfa>", CodigosHTML(CodigosPreDefinidos.ppdespesasalfa, PublicaX));
                    if (!EmTeste)
                        retorno = retorno.Replace("<#resumo2>", CodigosHTML(CodigosPreDefinidos.ppresumo2, PublicaX));
                    
                    
                    return retorno;
                case CodigosPreDefinidos.ppInicioarquivo:
                    retorno =
"<html xmlns=\"http://www.w3.org/1999/xhtml\">\r\n" +
"<head>\r\n" +
"<title>Balancete M<#modelo></title>\r\n" +
"<meta http-equiv=\"Content-Type\" content=\"text/html; charset=iso-8859-1\" />\r\n" +

"</head>\r\n" +
"\r\n" +
(parcial ? "<body style=\"background-image: URL(../IMAGENS/PARCIALorg.jpg)\">" : "<body>") +
"\r\n" +
"<table width=\"650\" cellspacing=\"0\" cellpadding=\"0\" "+
#if DEBUG
"border=\"1\" "+
#endif
"class=\"impresso\">\r\n";
                    retorno = retorno.Replace("<#modelo>", PublicaX.NomeModelo);
                    return retorno;

                case CodigosPreDefinidos.ppbalancete3:
                    retorno =
"<#ppInicioarquivo>" +
"    <tr><td><#recebimentos>" +
"    </td></tr>\r\n" +
"</table>\r\n\r\n" +
"<br style=\"page-break-before:always\" />\r\n\r\n" +
"<table width=\"650\" border="+
#if DEBUG
 "\"2\"" +
#else
"\"0\""+
#endif
" cellspacing=\"0\" cellpadding=\"0\" class=\"impresso\">\r\n" +
"  <tr><td colspan = \"2\"><#cabecalho></td></tr>\r\n" +
"<#ppdespesasalfa>\r\n" +
"</table>\r\n\r\n"+
"<table width=\"650\" border=" +
#if DEBUG
 "\"2\"" +
#else
"\"0\""+
#endif
" cellspacing=\"0\" cellpadding=\"0\"  class=\"impresso\">\r\n" +
"<#resumo2>";
                    retorno = retorno.Replace("<#ppInicioarquivo>", CodigosHTML(CodigosPreDefinidos.ppInicioarquivo, PublicaX));
                    retorno = retorno.Replace("<#recebimentos>", PublicaX.htmlrecebimentos);
                    retorno = retorno.Replace("<#cabecalho>", CodigosHTML(CodigosPreDefinidos.ppcabecalho, PublicaX));
                    PublicaX.andamentopaginar = 45;
                    retorno = retorno.Replace("<#ppdespesasalfa>", CodigosHTML(CodigosPreDefinidos.ppdespesasalfa, PublicaX));
                    if (!EmTeste)
                        retorno = retorno.Replace("<#resumo2>", CodigosHTML(CodigosPreDefinidos.ppresumo2, PublicaX));
                    return retorno;
                case CodigosPreDefinidos.ppbalancete4:
                    retorno =
"<#ppInicioarquivo>" +
"    <tr><td colspan=\"2\"><#recebimentos></td></tr>\r\n" +
"</table><!-- FIM inicio -->\r\n\r\n"+
"<br style=\"page-break-before:always\" />\r\n\r\n"+
"<table width=\"650\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\" class=\"impresso\">\r\n" +
"    <tr><td colspan=\"2\"><#despesas></td></tr>"+
"</table>\r\n\r\n"+
//"</table>\r\n" +
"<table width=\"650\" border=\"0\" class=\"impresso\">\r\n" +
"  <#quebra4>\r\n" +
"<#resumo2>";
                    retorno = retorno.Replace("<#ppInicioarquivo>", CodigosHTML(CodigosPreDefinidos.ppInicioarquivo, PublicaX));
                    retorno = retorno.Replace("<#recebimentos>", PublicaX.htmlrecebimentos);
                    retorno = retorno.Replace("<#despesas>", PublicaX.htmldespesas);
                    quebrar = PublicaX.andamentogeral + 7 + 9 + 25 > 250;
                    retorno = retorno.Replace("<#quebra4>", QuebraDepaginaM2(quebrar, PublicaX, 5));
                    if (!EmTeste)
                        retorno = retorno.Replace("<#resumo2>", CodigosHTML(CodigosPreDefinidos.ppresumo2, PublicaX));
                    return retorno;
                case CodigosPreDefinidos.ppbalancete1:
                    retorno =
"<#ppInicioarquivo>" +
"    <tr><td><#recebimentos>"+
"    </td></tr>\r\n" +
"</table>\r\n\r\n" +
"<br style=\"page-break-before:always\" />\r\n\r\n" +
//"<img src=\"../imagens/PARCIAL.gif\" style=\"left: -752px; position: relative; top: 106px; float: right;\" alt=\"\" />" +
"<table width=\"650\" border=" +
#if DEBUG
 "\"2\"" +
#else
"\"0\""+
#endif
" cellspacing=\"0\" cellpadding=\"0\" class=\"impresso\">\r\n" +
//"  <tr><td><#cabecalho></td></tr>\r\n" +
//"  <tr><td><div align=\"center\"><b><font size=\"3\">Despesas</font></b></div><hr /></td></tr> \r\n" +
"    <tr><td><#despesas>"+
"    </td></tr>\r\n" +
"</table>\r\n\r\n" +
"<br style=\"page-break-before:always\" />\r\n\r\n" +
//"<img src=\"../imagens/PARCIAL.gif\" style=\"left: -752px; position: relative; top: 106px; float: right;\" alt=\"\" />" +
"<table width=\"650\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\" class=\"impresso\">\r\n" +
"  <tr><td><#cabecalho></td></tr>\r\n" +
//"<img src=\"../imagens/PARCIAL.gif\" style=\"left: -140px; position: relative; top: 70px; float: right;\" alt=\"\" />" +
"  <tr><td><#saldos></td></tr>\r\n" +
"</table>\r\n" +
"</body>\r\n" +
"</html>";
                    //PublicaX.proxnumeropagina = 1;
                    
                    retorno = retorno.Replace("<#ppInicioarquivo>",CodigosHTML(CodigosPreDefinidos.ppInicioarquivo,PublicaX));
                    retorno = retorno.Replace("<#recebimentos>",PublicaX.htmlrecebimentos);
                    PublicaX.proxnumeropagina = PublicaX.npaginar + 1;
                    PublicaX.andamentogeral += PublicaX.andamentopaginar;
                    retorno = retorno.Replace("<#despesas>", PublicaX.htmldespesas);
                    //PublicaX.proxnumeropagina = PublicaX.npaginad + 1;
                    if (!EmTeste)
                    {
                        retorno = retorno.Replace("<#cabecalho>", CodigosHTML(CodigosPreDefinidos.ppcabecalho, PublicaX));
                        retorno = retorno.Replace("<#saldos>", CodigosHTML(CodigosPreDefinidos.ppresumo, PublicaX));
                    }
                    return retorno;
                case CodigosPreDefinidos.ppcabecalho:
                    retorno =
"\r\n"+
"<!-- ppcabecalho -->\r\n" +
"<table width=\"100%\" border=\"1\" cellspacing=\"0\" cellpadding=\"0\">\r\n" +
"  <tr><td>\r\n" +
"  <table width=\"100%\">\r\n" +
"   <tr>\r\n" +
"    <td width=\"120\" valign=\"top\"><img src=\"../imagens/neon02.jpg\" width=\"92\" height=\"45\" alt=\"\" /></td>\r\n" +
"    <td>\r\n" +
"      <div align=\"center\">\r\n" +
"              <p><i><font size=\"5\"><u><b><font size=\"4\">BALANCETE</font></b></u></font></i> \r\n" +
"                (modelo <#modelo>)</p>\r\n" +
"        <p align=\"left\"><font size=\"2\">Condom&iacute;nio:</font><font size=\"3\"><#condominio></font></p>\r\n" +
"        <p align=\"left\"><font size=\"2\">Per&iacute;odo:<#periodo></font></p>\r\n" +
"      </div>\r\n" +
"    </td>\r\n" +
"    <td width=\"120\"> \r\n" +
"      <div align=\"right\"><font size=\"2\">Cliente desde <#datacad><br /><br />\r\n" +
"              P&aacute;gina:<#pagina></font></div>\r\n" +
"    </td>\r\n" +
"   </tr>\r\n" +
"  </table>\r\n" +
"  </td></tr>\r\n" +
"</table>\r\n" +

"<!-- ppcabecalho -->\r\n\r\n";
                    retorno = retorno.Replace("<#periodo>",HttpUtility.HtmlEncode(string.Format("{0:dd/MM/yyyy} at� {1:dd/MM/yyyy}{2}",DI,DF,(parcial ? " PARCIAL" : ""))));
                    retorno = retorno.Replace("<#condominio>", HttpUtility.HtmlEncode(rowCON.CONNome));
                    retorno = retorno.Replace("<#datacad>", rowCON.CONDataInclusao.ToString("dd/MM/yyyy"));
                    retorno = retorno.Replace("<#pagina>", PublicaX.proxnumeropagina.ToString());
                    PublicaX.proxnumeropagina++;
                    PublicaX.andamentogeral = 45;
                    retorno = retorno.Replace("<#modelo>", PublicaX.NomeModelo);                    
                    return retorno;
            };
            return "";
        }

        private string QuebraDepaginaM2(bool quebra,PublicaHTML PublicaX,int colspan)
        {
            if (!quebra)
                return string.Format(
                                "\r\n<!-- Quebra M2 -->\r\n" +
                                //"</table>\r\n" +
                                //"<table width=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\">\r\n" +
                                "<tr><td colspan=\"{0}\"><hr /></td></tr>"+
                                "<!-- Quebra M2 FIM-->\r\n", 
                                colspan);
            else
                return string.Format(
                                "\r\n<!-- Quebra M2 -->\r\n" +
                                "</table>\r\n" +
                                //"    </td></tr>\r\n" +
                                //"</table>\r\n\r\n" +
                                "<br style=\"page-break-before:always\" />\r\n\r\n" +
                                //"<img src=\"../imagens/PARCIAL.gif\" style=\"left: -752px; position: relative; top: 106px; float: right;\" alt=\"\" />" +
                                "<table width=\"650\" class=\"impresso\" cellspacing=\"0\" cellpadding=\"0\" border = \""+
                                #if DEBUG
                                "1"+
                                #else
                                "0"+
                                #endif
                                "\">\r\n" +
                                "    <tr><td colspan=\"{1}\">\r\n" +
                                "{0}" +
                                "    </td></tr>\r\n" +
                                "</table>\r\n"+
                                "<table width=\"650\" class=\"impresso\" cellspacing=\"0\" cellpadding=\"0\" border = \"" +
                                #if DEBUG
                                "1" +
                                #else
                                "0"+
                                #endif
                                "\">\r\n" +                                
                                "<!-- Quebra M2 FIM-->\r\n",
                                CodigosHTML(CodigosPreDefinidos.ppcabecalho, PublicaX),
                                colspan);
        }

        private string listadesphtml(SortedList lista)
        {
            string retorno = "\r\n<!-- DESPESAS agrupadas alpha -->";
            foreach (DictionaryEntry DE in lista)
                retorno += string.Format("\r\n<tr><td><font size=\"1\">{0}</font></td><td width = \"150\"><div align=\"right\"><font size=\"1\">{1:n2}</font></div></td></tr>", 
                    HttpUtility.HtmlEncode((string)DE.Key), 
                    DE.Value);
            retorno += "\r\n<!-- FIM DESPESAS agrupadas alpha -->\r\n";
            return retorno;
        }

        private byte[] Criptografa()
        {
            string Aberto = string.Format("{0:yyyyMMddHHmmss}{1:000}{2:00000000}<-*W*->", DateTime.Now, EMPArquivo, staticR++);
            return VirCrip.VirCripWS.Crip(Aberto);
        }

        private void listadespexp(SortedList lista,int quadro)
        {
            foreach (DictionaryEntry DE in lista)
            {
                WSSincBancos.dBAL.BalanceteDetRow rowdet = dPublicar.BalanceteDet.NewBalanceteDetRow();
                rowdet.BAL = BAL;
                rowdet.EMP = EMPArquivo;
                rowdet.titulo = DE.Key.ToString();
                rowdet.quadro = quadro;
                rowdet.valor = (decimal)DE.Value;
                rowdet.negrito = false;                
                dPublicar.BalanceteDet.AddBalanceteDetRow(rowdet);
            }
            //lista.Sort;
            /*
  for i:=0 to pred(lista.Count) do
   begin
    NDM_Net.aedet1.Append;
    NDM_Net.aedet1titulo.AsString    :=lista[i];
    NDM_Net.aedet1quadro.AsInteger   :=quadro;
    NDM_Net.aedet1valor .AsFloat     :=integer(lista.Objects[i])/100;
    NDM_Net.aedet1negrito.AsBoolean  :=false;
    NDM_Net.aedet1.post;
   end;
 */
        }


        private void GeralDespesas()
        {
            foreach (Modelos Mx in Enum.GetValues(typeof(Modelos)))
            {
                PublicaHTML PublicaX = PublicaHTML.Mx(Mx);
                if ((Mx != Modelos.M2) && (Mx != Modelos.M3))
                    PublicaX.htmldespesas = string.Format(
                        "\r\n\r\n\r\n" +
                        "<!-- XX DESPESAS XX -->\r\n" +
                        "{0}" +
                        "    </td></tr>\r\n\r\n" +

                        "    <tr><td><div align=\"center\"><b><font size=\"3\">Despesas</font></b></div>\r\n" +
                        "      <hr />\r\n" +
                        "    </td></tr>\r\n\r\n" +
                        "    <tr><td>\r\n\r\n" +
                        "<table width=\"100%\" cellspacing=\"0\" cellpadding=\"0\"><tr><td><font size=\"3\">Item</font></td><td {1} align=\"right\"><font size=\"3\">Valor da Conta</font></td><td {1} align=\"right\"><b><font size=\"3\">Total</font></b></td></tr><tr><td colspan=\"3\"><hr /></td></tr>",
                        CodigosHTML(CodigosPreDefinidos.ppcabecalho, PublicaX),
                        larguracolunas23);
                else
                    PublicaX.htmldespesas = "";
                PublicaX.andamentopaginad = 64;
                PublicaX.acumulapagina = 0;
                PublicaX.itensacumulados = "";                
            }

            dsBalLocal1.rxrec1.Clear();

            //acumpagina     :=0;
            ordem=1;
            
            
            foreach(dsBalLocal.BalanceteRow rowBal in dsBalLocal1.Balancete)
            {
                if (rowBal.Tipopag.StartsWith("1"))
                    continue;
                if (!rowBal.Tipopag.StartsWith("2"))
                    break;
                somatot(rowBal.Tipopag,(decimal)rowBal.valorpago,rowBal.descricao);
                Pcsvalor.soma(rowBal.Tipopag,(decimal)rowBal.valorpago);   
                string grupoatual3d = rowBal.Tipopag.Substring(0,3);

                if (ultimoglanc != rowBal.descricao)
                {
                    imprime();
                    ultimoglanc = rowBal.descricao;
                }

                if (grupoatual3d != ultimogrupo3d)
                {
                    imprime3d();
                    ultimogrupo3d=grupoatual3d;
                }


                totalgrupo3d += (decimal)rowBal.valorpago;
                totalgrupolanc += (decimal)rowBal.valorpago;
                totaldesp += (decimal)rowBal.valorpago;
   
                
            }
            somatot("", 0, "");
            imprime();         //saldos
            imprime3d();  //saldos
            foreach (Modelos Mx in Enum.GetValues(typeof(Modelos)))
                PublicaHTML.Mx(Mx).htmldespesas += string.Format("<tr><td align=\"right\" colspan=\"2\"><b>Total das despesas</b></td><td align=\"right\"><b><font size=\"3\">R${0:n2}</font></b></td></tr><tr><td></td><td colspan=\"2\"><hr /></td></tr></table>\r\n<!-- FIM DESPESAS -->\r\n", totaldesp);
                        
        }

        private void imprime()
        {
            if (ultimoglanc != "")
            {
                foreach (Modelos Mx in Enum.GetValues(typeof(Modelos)))
                {
                    PublicaHTML PublicaX = PublicaHTML.Mx(Mx);
                    PublicaX.itensacumulados += string.Format("<tr><td><font size=\"{0}\">&nbsp;&nbsp;&nbsp;{1}</font></td><td align=\"right\"{2}><font size=\"{0}\">R${3:n2}&nbsp;&nbsp;</font></td></tr>",
                        PublicaX.fonte1,
                        HttpUtility.HtmlEncode(ultimoglanc),
                        larguracolunas23,
                        totalgrupolanc);
                    PublicaX.acumulapagina += PublicaX.tamlinha; //linha
                }
                dsBalLocal1.rxrec1.Addrxrec1Row(ultimoglanc, totalgrupolanc, false, ordem);
                totalgrupolanc = 0;
            }
        }

        private void imprime3d()
        {
            if (ultimogrupo3d != "") 
            {
                string comando = "SELECT Descricao FROM Plano_contas WHERE TipoPag = @P1";
                string DescricaoPLA = "";
                VirOleDb.TableAdapter.STTableAdapter.BuscaEscalar(comando,out DescricaoPLA,ultimogrupo3d+"000");
                foreach (Modelos Mx in Enum.GetValues(typeof(Modelos)))
                {
                    PublicaHTML PublicaX = PublicaHTML.Mx(Mx);
                    PublicaX.acumulapagina += PublicaX.tamgrupo;
                    if (PublicaX.andamentopaginad + 11 > 250)
                    {
                        PublicaX.npaginad++;
                        //proxnumeropagina = PublicaX.npaginad;
                        PublicaX.andamentopaginad=64;
                        string colspan = (Mx == Modelos.M1) ? "" : " colspan=\"2\"";
                        PublicaX.htmldespesas += string.Format(
                            "\r\n"+
                            "</table>\r\n"+
                            "    </td></tr>\r\n"+
                            "</table>\r\n\r\n" +
                            "<br style=\"page-break-before:always\" />\r\n\r\n" +                            
                            "<table width=\"650\" class=\"impresso\">\r\n" +
                            "    <tr><td"+colspan+">\r\n"+
                            "{0}"+
                            "    </td></tr>\r\n\r\n" +
                            "    <tr><td>\r\n\r\n"+                            
                            "<table width=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\">\r\n", 
                            CodigosHTML(CodigosPreDefinidos.ppcabecalho, PublicaX));                                              
                    }
                    PublicaX.htmldespesas += string.Format("\r\n<tr><td><b><font size=\"{0}\">{1}</font></b></td><td></td><td align=\"right\" {2}><font size=\"{0}\">R${3:n2}</font></td></tr>{4}<tr><td colspan=\"3\"><hr /></td></tr>",
                        PublicaX.fonte1,
                        HttpUtility.HtmlEncode(DescricaoPLA),
                        larguracolunas23,
                        totalgrupo3d,
                        PublicaX.itensacumulados);
                    PublicaX.andamentopaginad += PublicaX.acumulapagina;
                    PublicaX.acumulapagina = 0;
                    PublicaX.itensacumulados = "";
                }
                dsBalLocal1.rxrec1.Addrxrec1Row(DescricaoPLA,totalgrupo3d,true,ordem-1);                                      
                totalgrupo3d=0;
                ordem+=2;
            };
        }

        private void somatot(string tipo, decimal valor,string descricao)
        {
            int planonumerico = (tipo == "") ? 0 : int.Parse(tipo);

            //totdesp:=totdesp+valor;
            if (descricao != descant)
            {
                SortedList Lista = null;
                switch (grupoant)
                {
                    case 1:
                        Lista = despesasdg;
                        break;
                    case 2:
                        Lista = despesasmo;
                        break;
                    case 3:
                        Lista = despesasad;
                        break;
                };
                if (Lista != null)
                    if (Lista.ContainsKey(descant))
                        Lista[descant] = (decimal)Lista[descant] + totant;
                    else
                        Lista.Add(descant, totant);
                totant = 0;
                descant = descricao;
            }
            
            totant += valor;
            if((planonumerico <  219999) || (planonumerico >= 250000))
            {  
                grupoant=1;
                totdg+=valor;
            }
            else if ((planonumerico >= 220000) &&  (planonumerico <  230000))
            {
                grupoant = 2;
                totmo += valor;
            }
            else if ((planonumerico >= 230000) && (planonumerico < 250000))
            {
                grupoant = 3;
                totad += valor;
            }
        }

        

        private void Gerarecebimentos()
        {
            descanterior = "";
            
            //string campos = "";
            //int ci = 1;


            //string ultimotipodepag="";

            foreach (Modelos Mx in Enum.GetValues(typeof(Modelos)))
            {
                PublicaHTML PublicaX = PublicaHTML.Mx(Mx);
                PublicaX.proxnumeropagina = 1;
                PublicaX.andamentopaginar = 45;
                string colspan = (Mx == Modelos.M1) ? "" : " colspan=\"2\"";
                PublicaX.htmlrecebimentos = string.Format("\r\n\r\n\r\n\r\n{0}\r\n{1}{2}",
                    "<!-- XXX htmlrecebimentos XXX -->",
                    CodigosHTML(CodigosPreDefinidos.ppcabecalho,PublicaX),                    
                    "    </td></tr>\r\n\r\n"+                    
                    "    <tr><td"+colspan+">\r\n"+ 
                    "      <div align=\"center\"><b><font color=\"3\">Recebimentos</font></b></div>\r\n" +
                    "      <hr />\r\n"+
                    "    </td></tr>\r\n\r\n"
                );
                if ((Mx == Modelos.M1) || (Mx == Modelos.M3))
                    PublicaX.htmlrecebimentos += "    <tr><td>\r\n";


                foreach (SubGrupo Subg in Enum.GetValues(typeof(SubGrupo)))
                {
                    PublicaX.SetConteudoSubgrupo(Subg, "", PublicaHTML.TipoSet.Substituir);
                    PublicaX.SetEspacoDoSubgrupo(Subg, 0, PublicaHTML.TipoSet.Substituir);
                }
            }
            subtotal = 0;
            
            dsBalLocal1.rxrec2.Clear();
                                    
            foreach (dsBalLocal.BalanceteRow rowBal in dsBalLocal1.Balancete)
            {
                if (!rowBal.Tipopag.StartsWith("1"))
                    break;
                Pcsvalor.soma(rowBal.Tipopag, (decimal)rowBal.valorpago);
                blocoap = (rowBal.IsBlocoNull() || (rowBal.Bloco == "SB")) ? "" : rowBal.Bloco;
                //ci:=titulos.IndexOf(NDM_Net.BALANCETEdescricao.AsString);  { TODO : verifiacar se deveria ser else }
                if (descanterior != rowBal.descricao)
                {
                        if(descanterior != "")
                            gerabloco();   //gera um bloco do recebimento (at� o total)
                        descanterior = rowBal.descricao;
                        //ultimotipodepag = rowBalTipopag;
                        titulobloco = string.Format("\r\n<u><b><i><font size=\"{0}\">Recebimentos - {1}</font></i></b></u><br />\r\n",
                            PublicaHTML.Mx(Modelos.M1).Ctamanhofonte,
                            HttpUtility.HtmlEncode(rowBal.descricao));                                       
                };
                totalrec += (decimal)rowBal.valorpago;
                subtotal += (decimal)rowBal.valorpago;

                //if (rowBal.Tipopag == "198000")
                //{
                //    rowBal.Tipopag = "198000";
                //}
                                                                                                            
                if(!rowBal.IsvenctoNull()) //(recebimento nao de boleto - no bloco s� vai o total)
                {
                    SubGrupo Subg;
                    if (rowBal.vencto < DI)
                     Subg = SubGrupo.trasado;
                    else if (rowBal.vencto <= DF)
                     Subg = SubGrupo.mes;
                    else
                     Subg = SubGrupo.antecipado;

                    foreach(Modelos Mx in Enum.GetValues(typeof(Modelos)))
                    {
                        PublicaHTML PublicaX = PublicaHTML.Mx(Mx);
                        if(PublicaX.Getcolunaquadrinho(Subg) == PublicaX.Getmaxatrasados(Subg,blocoap))
                        {
                            PublicaX.Setcolunaquadrinho(Subg,0,PublicaHTML.TipoSet.Substituir);
                            PublicaX.SetConteudoSubgrupo(Subg,"</tr>\r\n<tr>",PublicaHTML.TipoSet.Somar);
                            //PublicaX.acumulapagina += PublicaX.incporlinha;                         
                            PublicaX.SetEspacoDoSubgrupo(Subg, PublicaX.incporlinha, PublicaHTML.TipoSet.Somar);
                        }

                        string conteudo;
                        if (rowBal.competencia.Length < 6)
                            conteudo = "ACORDO";
                        else
                            conteudo = string.Format("{0} {1}{2}",
                                blocoap,
                                rowBal.IsApartamentoNull() ? "XXXXXXXXXXXXXX" : rowBal.Apartamento,
                                Subg == SubGrupo.mes ? "" : string.Format(" {0}.{1}", rowBal.competencia.Substring(0, 2), rowBal.competencia.Substring(2, 4)));
                        PublicaX.SetConteudoSubgrupo(Subg, quadrinho(Mx,conteudo,Mx != Modelos.M1 || PublicaX.Getcolunaquadrinho(Subg) == 0), PublicaHTML.TipoSet.Somar);
                        PublicaX.Setcolunaquadrinho(Subg, 1, PublicaHTML.TipoSet.Somar);                        
                    }
                }
                  
                
            }
            gerabloco();
            foreach(Modelos Mx in Enum.GetValues(typeof(Modelos)))
            {
                PublicaHTML PublicaX = PublicaHTML.Mx(Mx);
                if((Mx == Modelos.M1) || (Mx == Modelos.M3))
                    PublicaX.htmlrecebimentos += string.Format("<div align=\"right\"><b><u><font size=\"{0}\">TOTAL: R${1:n2}</font></u></b></div>",
                        PublicaX.Ctamanhofonte,
                        totalrec);
                else
                {
                    PublicaX.htmlrecebimentos += "\r\n<!-- RECEBIMENTOS ALPHA -->\r\n<tr><td colspan=\"2\"><b><font size=\"1\">Recebimentos</font></b></td></tr>";
                    PublicaX.andamentopaginar = 45+7; //titulototal
                    SortedList Ordenado = new SortedList();
                    int n = 1;
                    foreach (dsBalLocal.rxrec2Row row in dsBalLocal1.rxrec2)
                        Ordenado.Add(string.Format("{0}{1:00000}", row.descricao, n++), row);
                    foreach (dsBalLocal.rxrec2Row row in Ordenado.Values)
                    {
                       PublicaX.htmlrecebimentos += string.Format("\r\n<tr><td><font size=\"1\">{0}</font></td><td width=\"150\"><div align=\"right\"><font size=\"1\">R${1:n2}</font></div></td></tr>",
                           HttpUtility.HtmlEncode(row.descricao),
                           row.subtotal);        
                        PublicaX.andamentopaginar += 4;
                    };
                    PublicaX.htmlrecebimentos += string.Format("<tr><td><div align=\"right\"><b><font size=\"1\">Total de Recebimentos:</font></b></div></td><td width=\"150\"><div align=\"right\"><b><font size=\"1\">R${0:n2}</font></b></div>\r\n<!-- FIM RECEBIMENTOS ALPHA -->", totalrec);
                  }
                  PublicaX.htmlrecebimentos += "\r\n<!-- XXX htmlrecebimentos FIM XXX -->\r\n\r\n\r\n\r\n";
            }
        }

        private string quadrinho(Modelos Mx, string conteudo,bool primeiro)
        {
            return string.Format("{0}<td><table border=\"1\" cellspacing=\"0\"><tr><td><font size=\"{1}\">{2}</font></td></tr></table></td>",
                primeiro ? "" : "<td><img src=\"../imagens/seta.gif\" width=\"13\" height=\"12\" alt=\"\" /></td>",
                PublicaHTML.Mx(Mx).Ctamanhofonteq,
                conteudo);
        }

        private void gerabloco()
        {             
            foreach(Modelos Mx in new Modelos[]{Modelos.M1,Modelos.M3})
            {
                PublicaHTML Publicax = PublicaHTML.Mx(Mx);
                //Publicax.acumulapagina += 15; //titulo e subtotal do bloco
                



                /*
                foreach(SubGrupo Subg in Enum.GetValues(typeof(SubGrupo)))
                {
                    if (Publicax.GetConteudoSubgrupo(Subg) != "")
                        Publicax.acumulapagina += Publicax.incporsubg;
                 
                };
                
                
                */

                bool TituloJaColocado = false;
                
                
                foreach(SubGrupo Subg in Enum.GetValues(typeof(SubGrupo)))                
                    if(Publicax.GetConteudoSubgrupo(Subg) != "")
                    {
                        int TamanhoEstimado = Publicax.andamentopaginar + Publicax.incporsubg + Publicax.GetEspacoDoSubgrupo(Subg) + Publicax.TamanhoSubTotal;
                        if (!TituloJaColocado)
                            TamanhoEstimado += 8;
                        if ((TamanhoEstimado > 250) && (Publicax.htmlrecebimentos != ""))
                        {
                            Publicax.npaginar += 1;                            
                            Publicax.andamentopaginar = 45;                            
                            string colspan = (Mx == Modelos.M1) ? "" : " colspan=\"2\"";
                            Publicax.htmlrecebimentos += string.Format(
                                "\r\n<!-- Quebra Rec -->\r\n" +
                                "    </td></tr>\r\n" +
                                "</table>\r\n\r\n\r\n" +
                                "<br style=\"page-break-before:always\" />\r\n\r\n\r\n" +                                
                                "<table width=\"650\" " +
                                "class=\"impresso\">\r\n" +
                                "    <tr><td" + colspan + ">\r\n" +
                                "{0}" +
                                "    </td></tr>\r\n\r\n" +
                                "    <tr><td" + colspan + ">\r\n",
                                CodigosHTML(CodigosPreDefinidos.ppcabecalho, Publicax));
                        };
                        Publicax.andamentopaginar += (Publicax.incporsubg + Publicax.GetEspacoDoSubgrupo(Subg));

                        if (!TituloJaColocado)
                        {
                            Publicax.htmlrecebimentos += titulobloco;
                            Publicax.andamentopaginar += 8; //titulo e subtotal do bloco
                        };

                        Publicax.htmlrecebimentos += string.Format("<b><font size=\"{0}\">{1}:</font></b><br />\r\n<font size=\"1\">{2}Apto{3}</font><br />\r\n\r\n<table>\r\n<tr>{4}</tr>\r\n</table>\r\n\r\n",
                            Publicax.Ctamanhofonte,
                            HttpUtility.HtmlEncode(PublicaHTML.DescSubGrupo(Subg)),
                            blocoap == "" ? "" :"bloco&nbsp;&nbsp;",
                            Subg == SubGrupo.mes ? "" : "&nbsp;&nbsp;M&#234;s Comp.",
                            Publicax.GetConteudoSubgrupo(Subg));
                        Publicax.SetConteudoSubgrupo(Subg, "", PublicaHTML.TipoSet.Substituir);
                        Publicax.Setcolunaquadrinho(Subg,0,PublicaHTML.TipoSet.Substituir);
                        Publicax.SetEspacoDoSubgrupo(Subg, 0, PublicaHTML.TipoSet.Substituir);
                    }
                if (subtotal != 0)
                {
                    Publicax.htmlrecebimentos += string.Format("<div align=\"right\"><font size=\"{0}\">SUBTOTAL: R${1:n2}</font></div><hr />",
                        Publicax.Ctamanhofonte,
                        subtotal);
                    Publicax.andamentopaginar += Publicax.TamanhoSubTotal;
                }                           
            }                                                 
                                 
            if (subtotal != 0)                              
               dsBalLocal1.rxrec2.Addrxrec2Row(descanterior, subtotal);                                                              
            subtotal=0;                            
        }




    }

    /// <summary>
    /// 
    /// </summary>
    public enum Modelos 
    {
        /// <summary>
        /// 
        /// </summary>
        M1,
        /// <summary>
        /// 
        /// </summary>
        M2,
        /// <summary>
        /// 
        /// </summary>
        M3,
        /// <summary>
        /// 
        /// </summary>
        M4
    }

    enum SubGrupo
    {
        mes, trasado, antecipado
    }


    /// <summary>
    /// 
    /// </summary>
    public enum CodigosPreDefinidos
    {
        /// <summary>
        /// 
        /// </summary>
        /// <summary>
        /// 
        /// </summary>
        ppInicioarquivo,
        /// <summary>
        /// 
        /// </summary>
        ppcabecalho,
        /// <summary>
        /// 
        /// </summary>
        ppbalancete1,
        /// <summary>
        /// 
        /// </summary>
        ppbalancete2,
        /// <summary>
        /// 
        /// </summary>
        ppbalancete3,
        /// <summary>
        /// 
        /// </summary>
        ppbalancete4,
        /// <summary>
        /// 
        /// </summary>
        ppresumo,
        /// <summary>
        /// 
        /// </summary>
        ppdespesasalfa,
        /// <summary>
        /// 
        /// </summary>
        ppresumo2
    }

    class PublicaHTML
    {
        /// <summary>
        /// 
        /// </summary>
        public Modelos M;
        /// <summary>
        /// 
        /// </summary>
        public string htmlrecebimentos;
        /// <summary>
        /// 
        /// </summary>
        public string htmldespesas;
        /// <summary>
        /// 
        /// </summary>
        public int acumulapagina;
        private SortedList EspacoDoSubgrupo;
        private SortedList ConteudoSubgrupos;
        private SortedList colunaquadrinho;
        private SortedList maxatrasadossb;
        private SortedList maxatrasados;
        /// <summary>
        /// 
        /// </summary>
        public int npaginar;
        /// <summary>
        /// 
        /// </summary>
        public int npaginad;
        /// <summary>
        /// 
        /// </summary>
        public int andamentopaginar;
        /// <summary>
        /// 
        /// </summary>
        public int andamentopaginad;

        /// <summary>
        /// 
        /// </summary>
        public static SortedList PublicaHTMLs;

        /// <summary>
        /// 
        /// </summary>
        public int incporsubg;
        /// <summary>
        /// 
        /// </summary>
        public int TamanhoSubTotal;
        /// <summary>
        /// 
        /// </summary>
        public int incporlinha;
        /// <summary>
        /// 
        /// </summary>
        public string Ctamanhofonte;
        /// <summary>
        /// 
        /// </summary>
        public string Ctamanhofonteq;
        /// <summary>
        /// 
        /// </summary>
        public string itensacumulados;
        /// <summary>
        /// 
        /// </summary>
        public string fonte1;
        /// <summary>
        /// 
        /// </summary>
        public int tamlinha;
        /// <summary>
        /// 
        /// </summary>
        public int tamgrupo;
        /// <summary>
        /// 
        /// </summary>
        public int andamentogeral;
        /// <summary>
        /// 
        /// </summary>
        public int proxnumeropagina;
        /// <summary>
        /// 
        /// </summary>
        public CodigosPreDefinidos ppModelo;
        /// <summary>
        /// 
        /// </summary>
        public string NomeModelo;

        

        /*
        public static string CodigosHTML(CodigosPreDefinidos CodigoModelo,PublicaHTML PublicaX)
        {
            string retorno;
            switch (CodigoModelo)
            {
                case CodigosPreDefinidos.ppbalancete1:
                    retorno = 
"<html>\r\n" +
"<head>\r\n" +
"<title>Balancete M1</title>\r\n" +
"<meta http-equiv=\"Content-Type\" content=\"text/html; charset=iso-8859-1\">\r\n" +
"<link rel=\"stylesheet\" href=\"../include/neon.css\" type=\"text/css\">\r\n" +
"</head>\r\n" +
"<script language=\"JavaScript\">\r\n" +
"\r\n" +
"function carrega() { \r\n" +
"  eval(\"parent.leftFrame.location=\\'../menuprin.asp\\'\");\r\n" +
"}\r\n" +
"\r\n" +
"</script>\r\n" +
"<body bgcolor=\"#FFFFFF\" text=\"#000000\" width=\"650\" onload=\"carrega()\">\r\n" +
"<table width=\"650\" cellspacing=\"0\" cellpadding=\"0\" class=\"impresso\">\r\n" +
"  <tr><td><#cabecalho></td></tr>\r\n" +
"  <tr><td>\r\n" +
"      <div align=\"center\"><b><font color=\"3\">Recebimentos</font></b></div>\r\n" +
"      <hr /></td></tr>\r\n" +
"  <tr><td><#recebimentos></td></tr>\r\n" +
"</table>\r\n" +
"<br style=\"page-break-before:always\">\r\n" +
"<table width=\"650\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\" class=\"impresso\">\r\n" +
"  <tr><td><#cabecalho></td></tr>\r\n" +
"  <tr><td><div align=\"center\"><b><font size=\"3\">Despesas</font></b></div><hr /></td></tr> \r\n" +
"  <tr><td><#despesas></td></tr>\r\n" +
"</table>\r\n" +
"<br style=\"page-break-before:always\">\r\n" +
"<table width=\"650\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\" class=\"impresso\">\r\n" +
"  <tr><td><#cabecalho></td></tr>\r\n" +
"  <tr><td><#saldos></td></tr>\r\n" +
"</table>\r\n" +
"</body>\r\n" +
"</html>;";
                    retorno = retorno.Replace("<#cabecalho>", PublicaHTML.CodigosHTML(CodigosPreDefinidos.ppcabecalho, PublicaX));
                    retorno = retorno.Replace("<#recebimentos>", PublicaX.htmlrecebimentos);
                    PublicaX.proxnumeropagina = PublicaX.npaginar + 1;
                    PublicaX.andamentogeral += PublicaX.andamentopaginar;
                    retorno = retorno.Replace("<#despesas>", PublicaX.htmldespesas);
                    PublicaX.proxnumeropagina = PublicaX.npaginad + 1;
                    //retorno = retorno.Replace("#saldos", PublicaHTML.CodigosHTML(CodigosPreDefinidos.ppresumo, PublicaX));                                            
                    return retorno;
                case CodigosPreDefinidos.ppcabecalho:
                    retorno = 
"<table width=\"100%\" border=\"1\" cellspacing=\"0\" cellpadding=\"0\">\r\n" +
" <tr><td>\r\n" +
" <table width=\"100%\">\r\n" +
"  <tr>\r\n" +
"    <td width=\"120\" valign=\"top\"><img src=\"../imagens/neon10.jpg\" width=\"92\" height=\"45\"></td>\r\n" +
"    <td>\r\n" +
"      <div align=\"center\">\r\n" +
"              <p><i><font size=\"5\"><u><b><font size=\"4\">BALANCETE</font></b></u></font></i> \r\n" +
"                (modelo <#modelo>)</p>\r\n" +
"        <p align=\"left\"><font size=\"2\">Condom&iacute;nio:</font><font size=\"3\"><#condominio></font></p>\r\n" +
"        <p align=\"left\"><font size=\"2\">Per&iacute;odo:<#periodo></font></p>\r\n" +
"      </div>\r\n" +
"    </td>\r\n" +
"    <td width=\"120\"> \r\n" +
"      <div align=\"right\"><font size=\"2\">Cliente desde <#datacad><br /><br />\r\n" +
"              P&aacute;gina:<#pagina></font></div>\r\n" +
"    </td>\r\n" +
"  </tr>\r\n" +
" </table>\r\n" +
" </td></tr>\r\n" +
"</table>;";
                    /*
                    retorno = retorno.Replace("<%periodo>",string.Format("{0:dd/MM/yyyy} at� {1:dd/MM/yyyy}",
                    DI
                    
  ReplaceText:= 
 else if uppercase(TagString) = 'CONDOMINIO' then
  ReplaceText:= NDM_Net.SQLCondominios1Nome.AsString
 else if uppercase(TagString) = 'DATACAD' then
  ReplaceText:= NDM_Net.SQLCondominios1datacliente.AsString
 else if uppercase(TagString) = 'PAGINA' then
  begin
   ReplaceText:= inttostr(proxnumeropagina);//inttostr(dadosmodelo[modelo].npagina)
   inc(proxnumeropagina);
   andamentogeral:=45;
  end
 else if uppercase(TagString) = 'MODELO' then
  case modelo of
   1:ReplaceText:= 'I';
   2:ReplaceText:= 'II';
   3:ReplaceText:= 'III';
   4:ReplaceText:= 'IV';
  end*//*
                    return retorno;
            };
            return "";
        }
        */

        static PublicaHTML()
        {
            PublicaHTMLs = new SortedList();
            foreach (Modelos Mx in Enum.GetValues(typeof(Modelos)))
                PublicaHTMLs.Add(Mx, new PublicaHTML(Mx));            
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="Subg"></param>
        /// <returns></returns>
        public static string DescSubGrupo(SubGrupo Subg)
        {
            switch(Subg)
            {
                case SubGrupo.antecipado:
                    return "Antecipados";
                case SubGrupo.mes:
                    return "M�s";
                case SubGrupo.trasado:
                    return "Atrasados";
            }
            return "";
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="M"></param>
        /// <returns></returns>
        public static PublicaHTML Mx(Modelos M)
        {
            return (PublicaHTML)PublicaHTMLs[M];
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="Mx"></param>
        public PublicaHTML(Modelos Mx)
        {
            M = Mx;
            htmlrecebimentos = htmldespesas = "";
            npaginar = acumulapagina = andamentopaginar = 0;
            ConteudoSubgrupos = new SortedList();
            colunaquadrinho = new SortedList();
            maxatrasadossb = new SortedList();
            maxatrasados = new SortedList();
            EspacoDoSubgrupo = new SortedList();
            foreach (SubGrupo Subg in Enum.GetValues(typeof(SubGrupo)))
            {
                EspacoDoSubgrupo.Add(Subg, 0);
                ConteudoSubgrupos.Add(Subg, "");
                colunaquadrinho.Add(Subg, 0);
            }
            switch (Mx)
            {
                case Modelos.M1:
                    incporsubg = 0;
                    incporlinha = 7;
                    TamanhoSubTotal = 8;
                    Ctamanhofonte = "3";
                    Ctamanhofonteq = "2";
                    
                    //maxatrasados.Add(SubGrupo.mes,8);
                    maxatrasados.Add(SubGrupo.mes, 9);
                    //maxatrasados.Add(SubGrupo.trasado,4);
                    maxatrasados.Add(SubGrupo.trasado, 5);
                    //maxatrasados.Add(SubGrupo.antecipado,4);
                    maxatrasados.Add(SubGrupo.antecipado, 5);
                    maxatrasadossb.Add(SubGrupo.mes,10);
                    maxatrasadossb.Add(SubGrupo.trasado,5);
                    maxatrasadossb.Add(SubGrupo.antecipado,5);
                    fonte1 = "2";
                    tamlinha = 5;
                    tamgrupo = 10;
                    ppModelo = CodigosPreDefinidos.ppbalancete1;
                    NomeModelo = "I";
                    break;
                case Modelos.M2:
                    incporsubg = 0;
                    incporlinha = 0;
                    TamanhoSubTotal = 0;
                    Ctamanhofonte = "";
                    Ctamanhofonteq = "";
                    maxatrasados.Add(SubGrupo.mes,0);
                    maxatrasados.Add(SubGrupo.trasado,0);
                    maxatrasados.Add(SubGrupo.antecipado,0);
                    maxatrasadossb.Add(SubGrupo.mes,0);
                    maxatrasadossb.Add(SubGrupo.trasado,0);
                    maxatrasadossb.Add(SubGrupo.antecipado,0);
                    fonte1 = "";
                    tamlinha = 0;
                    tamgrupo = 0;
                    ppModelo = CodigosPreDefinidos.ppbalancete2;
                    NomeModelo = "II";
                    break;
                case Modelos.M3:
                    incporsubg = 0;
                    incporlinha = 6;
                    TamanhoSubTotal = 7;
                    Ctamanhofonte = "1";
                    Ctamanhofonteq = "1";
                    //maxatrasados.Add(SubGrupo.mes,12);
                    maxatrasados.Add(SubGrupo.mes, 14);
                    //maxatrasados.Add(SubGrupo.trasado,6);
                    maxatrasados.Add(SubGrupo.trasado, 7);
                    //maxatrasados.Add(SubGrupo.antecipado,6);
                    maxatrasados.Add(SubGrupo.antecipado, 7);
                    maxatrasadossb.Add(SubGrupo.mes,15);
                    maxatrasadossb.Add(SubGrupo.trasado,7);
                    maxatrasadossb.Add(SubGrupo.antecipado,7);
                    fonte1 = "";
                    tamlinha = 0;
                    tamgrupo = 0;
                    ppModelo = CodigosPreDefinidos.ppbalancete3;
                    NomeModelo = "III";
                    break;
                case Modelos.M4:
                    incporsubg = 0;
                    incporlinha = 0;
                    TamanhoSubTotal = 0;
                    Ctamanhofonte = "";
                    Ctamanhofonteq = "";
                    maxatrasados.Add(SubGrupo.mes,0);
                    maxatrasados.Add(SubGrupo.trasado,0);
                    maxatrasados.Add(SubGrupo.antecipado,0);
                    maxatrasadossb.Add(SubGrupo.mes,0);
                    maxatrasadossb.Add(SubGrupo.trasado,0);
                    maxatrasadossb.Add(SubGrupo.antecipado,0);
                    fonte1 = "1";
                    tamlinha = 4;
                    tamgrupo = 8;
                    ppModelo = CodigosPreDefinidos.ppbalancete4;
                    NomeModelo = "IV";
                    break;
            }
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="Subg"></param>
        /// <returns></returns>
        public string GetConteudoSubgrupo(SubGrupo Subg)
        {
            return (string)ConteudoSubgrupos[Subg];
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="Subg"></param>
        /// <returns></returns>
        public int GetEspacoDoSubgrupo(SubGrupo Subg)
        {
            return (int)EspacoDoSubgrupo[Subg];
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="Subg"></param>
        /// <returns></returns>
        public int Getcolunaquadrinho(SubGrupo Subg)
        {
            return (int)colunaquadrinho[Subg];
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="Subg"></param>
        /// <param name="Bloco"></param>
        /// <returns></returns>
        public int Getmaxatrasados(SubGrupo Subg,string Bloco)
        {
            SortedList maxatrasadosX = (Bloco == "") ? maxatrasadossb : maxatrasados;
            return (int)maxatrasadosX[Subg];
        }
        /// <summary>
        /// 
        /// </summary>
        public enum TipoSet {Somar,Substituir }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="Subg"></param>
        /// <param name="Valor"></param>
        /// <param name="Tipo"></param>
        public void SetConteudoSubgrupo(SubGrupo Subg,string Valor,TipoSet Tipo)
        {            
            ConteudoSubgrupos[Subg] = string.Format("{0}{1}", (Tipo == TipoSet.Somar) ? (string)ConteudoSubgrupos[Subg] : "", Valor);
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="Subg"></param>
        /// <param name="Valor"></param>
        /// <param name="Tipo"></param>
        public void SetEspacoDoSubgrupo(SubGrupo Subg, int Valor, TipoSet Tipo)
        {
            EspacoDoSubgrupo[Subg] = ((Tipo == TipoSet.Somar) ? Valor + (int)EspacoDoSubgrupo[Subg] : Valor);
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="Subg"></param>
        /// <param name="Valor"></param>
        /// <param name="Tipo"></param>
        public void Setcolunaquadrinho(SubGrupo Subg, int Valor, TipoSet Tipo)
        {
            colunaquadrinho[Subg] = ((Tipo == TipoSet.Somar) ? (int)colunaquadrinho[Subg] : 0) + Valor;
        }

        
        
    }

    enum pcs
    {
        errado,
        pc1,
        pc2,
        pc3,
        pc4,
        pc51,
        pc52,
        pc53,
        pc54,
        pc61,
        pc62,
        pc63,
        pc64,
        pc7,
        pc8
    }

    class pcsvalor
    {
        public SortedList Valores;
        private ArrayList Somar;
        public pcsvalor()
        {
            Valores = new SortedList();
            foreach (pcs xpc in Enum.GetValues(typeof(pcs)))
                Valores.Add(xpc, 0.0M);
            Somar = new ArrayList(new pcs[] { pcs.pc1,pcs.pc61,pcs.pc62,pcs.pc63,pcs.pc64,pcs.pc4,pcs.pc8});
        }

        public decimal Valor(pcs pc)
        {
            return (decimal)Valores[pc];
        }

        internal decimal TotalAcumulado()
        {            
            decimal Total = 0;
            foreach (DictionaryEntry DE in Valores)
                if (Somar.Contains(DE.Key))
                    Total += (decimal)DE.Value;
                else
                    Total -= (decimal)DE.Value;
            return Total;
        }

        public void soma(string PLA, decimal valor)
        {
            pcs xpc = pcs.errado;
            int planonumerico = int.Parse(PLA);
            if ((planonumerico > 0) && (planonumerico < 200000))
                xpc = pcs.pc1;
            else if ((planonumerico >= 200000) && (planonumerico < 300000))
                xpc = pcs.pc2;
            else if ((planonumerico >= 300000) && (planonumerico < 400000))
                xpc = pcs.pc3;
            else if ((planonumerico >= 400000) && (planonumerico < 500000))
                xpc = pcs.pc4;
            else if (planonumerico == 500001)
                xpc = pcs.pc51;
            else if (planonumerico == 500002)
                xpc = pcs.pc52;
            else if (planonumerico == 500003)
                xpc = pcs.pc53;
            else if (planonumerico == 500004)
                xpc = pcs.pc54;
            else if (planonumerico == 600001)
                xpc = pcs.pc61;
            else if (planonumerico == 600002)
                xpc = pcs.pc62;
            else if (planonumerico == 600003)
                xpc = pcs.pc63;
            else if (planonumerico == 600004)
                xpc = pcs.pc64;
            else if ((planonumerico >= 700000) && (planonumerico < 800000))
                xpc = pcs.pc7;
            else if ((planonumerico >= 800000) && (planonumerico < 900000))
                xpc = pcs.pc8;
            Valores[xpc] = (decimal)Valores[xpc] + valor;
        }
    }
}
