﻿namespace dllpublicaBalancete {


    partial class dsBalLocal
    {
        private static dsBalLocalTableAdapters.BalanceteTableAdapter balanceteTableAdapter;
        /// <summary>
        /// TableAdapter padrão para tabela: Balancete
        /// </summary>
        public static dsBalLocalTableAdapters.BalanceteTableAdapter BalanceteTableAdapter
        {
            get
            {
                if (balanceteTableAdapter == null)
                {
                    balanceteTableAdapter = new dsBalLocalTableAdapters.BalanceteTableAdapter();
                    balanceteTableAdapter.TrocarStringDeConexao();
                };
                return balanceteTableAdapter;
            }
        }

        private dsBalLocalTableAdapters.CondominiosTableAdapter condominiosTableAdapter;
        /// <summary>
        /// TableAdapter padrão para tabela: Condominios
        /// </summary>
        public dsBalLocalTableAdapters.CondominiosTableAdapter CondominiosTableAdapter
        {
            get
            {
                if (condominiosTableAdapter == null)
                {
                    condominiosTableAdapter = new dsBalLocalTableAdapters.CondominiosTableAdapter();
                    condominiosTableAdapter.TrocarStringDeConexao();
                };
                return condominiosTableAdapter;
            }
        }


        private dsBalLocalTableAdapters.CreditosTableAdapter creditosTableAdapter;
        /// <summary>
        /// TableAdapter padrão para tabela: Creditos
        /// </summary>
        public dsBalLocalTableAdapters.CreditosTableAdapter CreditosTableAdapter
        {
            get
            {
                if (creditosTableAdapter == null)
                {
                    creditosTableAdapter = new dsBalLocalTableAdapters.CreditosTableAdapter();
                    creditosTableAdapter.TrocarStringDeConexao();
                };
                return creditosTableAdapter;
            }
        }
    }
}
