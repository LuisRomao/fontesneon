﻿using System;
using System.IO;
using System.Windows.Forms;
using CompontesBasicos;

namespace dllcontabil
{
    /// <summary>
    /// 
    /// </summary>
    public partial class cGeraArquivos : ComponenteBase
    {
        /// <summary>
        /// 
        /// </summary>
        public cGeraArquivos()
        {
            InitializeComponent();            
            cvirCompetencia1.Comp = new virCompet.virCompetencia(DateTime.Today.AddMonths(-1), virCompet.Regime.mes);
            cvirCompetencia1.CompMaxima = cvirCompetencia1.Comp.CloneCompet(1);
            cvirCompetencia1.AjustaTela();
            
        }

        private defiscal defiscal1;

        private void PreparaDados()
        {
            defiscal1 = new defiscal();
            defiscal1.CondominioTableAdapter.FillByComNotas(defiscal1.Condominio, cvirCompetencia1.Comp.DataI, cvirCompetencia1.Comp.DataF);
            defiscal1.NOTASTableAdapter.FillBy(defiscal1.NOTAS, cvirCompetencia1.Comp.DataI, cvirCompetencia1.Comp.DataF);
            defiscal1.AjustarCamposCalculadosCON();
            defiscal1.AjustarCamposCalculadosNOA();
            defiscalBindingSource.DataSource = defiscal1;
            caminhoFolhaMatic = string.Format("{0}\\FOLHAMATIC", Path.GetDirectoryName(Application.ExecutablePath));
            if (!Directory.Exists(caminhoFolhaMatic))
                Directory.CreateDirectory(caminhoFolhaMatic);
            caminhoMXM = string.Format("{0}\\MXM", Path.GetDirectoryName(Application.ExecutablePath));
            if (!Directory.Exists(caminhoMXM))
                Directory.CreateDirectory(caminhoMXM);
        }

        private string caminhoFolhaMatic;
        private string caminhoMXM;

        private void GeraFolhamatic()
        {
            efiscal arquivo = new efiscal(cvirCompetencia1.Comp, defiscal1);                        
            string NomeArquivo = string.Format("{0}\\{1}_{2}_{3}.FML", caminhoFolhaMatic, Framework.DSCentral.EMP == 1 ? 100 : 101, cvirCompetencia1.Comp.Mes, cvirCompetencia1.Comp.Ano);
            if (!arquivo.gerarArquivo(NomeArquivo))
            {
                MessageBox.Show(arquivo.UltimoErro.Message);
                if (arquivo.UltimoErro != null)
                    throw new Exception("Erro na geração do arquivo", arquivo.UltimoErro);
            }
            else
            {
                labelArq.Visible = textEdit1.Visible = true;
                textEdit1.Text = NomeArquivo;
            }
        }

        private void GeraMXM()
        {
            efiscalMXMc arquivo = new efiscalMXMc(cvirCompetencia1.Comp, defiscal1);
            string NomeArquivo = string.Format("{0}\\{1}_{2}_{3}.txt", caminhoMXM, Framework.DSCentral.EMP == 1 ? 100 : 101, cvirCompetencia1.Comp.Mes, cvirCompetencia1.Comp.Ano);
            if (!arquivo.gerarArquivo(NomeArquivo))
            {
                MessageBox.Show(arquivo.UltimoErro.Message);
                if (arquivo.UltimoErro != null)
                    throw new Exception("Erro na geração do arquivo", arquivo.UltimoErro);
            }
            else
            {
                labelArq.Visible = textEdit1.Visible = true;
                textEdit1.Text = NomeArquivo;
            }
        }

        private void simpleButton1_Click(object sender, EventArgs e)
        {
            PreparaDados();
            GeraFolhamatic();
            GeraMXM();
        }


        
        private void gridView2_CustomColumnDisplayText(object sender, DevExpress.XtraGrid.Views.Base.CustomColumnDisplayTextEventArgs e)
        {
            if (e.Column.FieldName == colRPSComp.FieldName)
            {
                virCompet.virCompetencia comp = new virCompet.virCompetencia((int)e.Value);
                e.DisplayText = comp.ToString();
            }
        }
    }
}
