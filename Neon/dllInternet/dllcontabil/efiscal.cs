﻿using System;
using ArquivosTXT;

namespace dllcontabil
{
    /// <summary>
    /// Classe para geração de arquivos efiscal versão 2.0.05
    /// </summary>
    public class efiscal
    {
        private cTabelaTXT ArquivoSaida;

        /// <summary>
        /// 
        /// </summary>
        private defiscal defiscal;

        /// <summary>
        /// 
        /// </summary>
        public static virCompet.virCompetencia comp;

        /// <summary>
        /// 
        /// </summary>
        /// <param name="_comp"></param>
        public efiscal(virCompet.virCompetencia _comp, defiscal defiscal1)
        {
            comp = _comp;
            defiscal = defiscal1;
        }

        /// <summary>
        /// último erro
        /// </summary>
        public Exception UltimoErro;

        private RegE001 regE001;
        private RegE010 regE010;
        //private RegE200 regE200;
        //private RegE201 regE201;
        private RegE600 regE600;
        private RegE602 regE602;
        private RegE603 regE603;
        private RegE604 regE604;
        private RegE605 regE605;

        /// <summary>
        /// Gera o arquivo
        /// </summary>
        /// <param name="Nome"></param>
        /// <returns></returns>
        public bool gerarArquivo(string Nome)
        {
            bool retorno = true;
            try
            {
                ArquivoSaida = new cTabelaTXT(ModelosTXT.layout);                
                ArquivoSaida.Formatodata = FormatoData.yyyyMMdd;
                ArquivoSaida.IniciaGravacao(Nome);
                regE001 = new RegE001(ArquivoSaida);
                regE010 = new RegE010(ArquivoSaida);
                //regE200 = new RegE200(ArquivoSaida);
                //regE201 = new RegE201(ArquivoSaida);
                regE600 = new RegE600(ArquivoSaida);
                regE602 = new RegE602(ArquivoSaida);
                regE603 = new RegE603(ArquivoSaida);
                regE604 = new RegE604(ArquivoSaida);
                regE605 = new RegE605(ArquivoSaida);
                regE001.GravaLinha();
                

                foreach (System.Data.DataRow DR in defiscal.Condominio.Rows)
                    regE010.GravaLinha(DR);


                int iTeto = defiscal.NOTAS.Rows.Count - 1;
                int icursor = 0;
                while(icursor <= iTeto)
                {
                    defiscal.NOTASRow NotaBase = defiscal.NOTAS[icursor];
                    NotaBase.TOTALNOATotal = NotaBase.NOATotal;
                    NotaBase.TOTALISSRetido = NotaBase.ISSRetido;
                    NotaBase.TOTALISSValor = NotaBase.ISSValor;
                    NotaBase.TOTALRPSRetIR = NotaBase.RPSRetIR;
                    NotaBase.nOrdemServ = 1;
                    int iFimNota = icursor;
                    int nOrdemServ = 2;
                    while ((iFimNota < iTeto) && (NotaBase.RPS.Equals(defiscal.NOTAS[iFimNota + 1].RPS)))
                    {
                        iFimNota++;
                        defiscal.NOTASRow NotaDet = defiscal.NOTAS[iFimNota];
                        NotaDet.nOrdemServ = nOrdemServ++;
                        NotaBase.TOTALNOATotal += NotaDet.NOATotal;
                        NotaBase.TOTALISSRetido += NotaDet.ISSRetido;
                        NotaBase.TOTALISSValor += NotaDet.ISSValor;
                        NotaBase.TOTALRPSRetIR += NotaDet.RPSRetIR;
                    }
                    //regE200.GravaLinha(DR);
                    //regE201.GravaLinha(DR);
                    regE600.GravaLinha(NotaBase);
                    regE602.GravaLinha(NotaBase);
                    for (int kcursor = icursor; kcursor <= iFimNota; kcursor++)
                        regE603.GravaLinha(defiscal.NOTAS[kcursor]);
                    regE604.GravaLinha(NotaBase);
                    regE605.GravaLinha(NotaBase);
                    icursor = iFimNota + 1;
                }                                
            }
            catch (Exception e)
            {
                UltimoErro = e;
                return false;                           
            }
            finally
            {
                if (!ArquivoSaida.TerminaGravacao())
                    retorno = false;
            }
            
            return retorno;
        }
    }

    internal class RegE001 : RegistroBase
    {
        /// <summary>
        /// 
        /// </summary>
        /// <param name="_cTabTXT"></param>
        public RegE001(cTabelaTXT _cTabTXT)
            : base(_cTabTXT, null)
        {
        }

        /// <summary>
        /// 
        /// </summary>
        protected override void RegistraMapa()
        {
            base.RegistraMapa();
            RegistraMapa("ID", 1, 4, "E001");
            RegistraMapa("EMO", 5, 4, Framework.DSCentral.EMP == 1 ? 100 : 101);
            RegistraMapa("VERSAO", 9, 6, "2.0.06");
            RegistraMapa("CONTROLE", 15, 1, 0);
        }

    }

    internal class RegE010 : RegistroBase
    {
        /// <summary>
        /// 
        /// </summary>
        /// <param name="_cTabTXT"></param>
        public RegE010(cTabelaTXT _cTabTXT)
            : base(_cTabTXT, null)
        {            
        }
         
        /// <summary>
        /// 
        /// </summary>
        protected override void RegistraMapa()
        {
            base.RegistraMapa();
            RegistraMapa("ID", 1, 4, "E010");
            RegistraMapa("CONCodigo", 5, 10, "");
            RegistraMapa("CONNome", 25, 100,"");
            RegistraMapa("CONDataInclusao", 125, 8, DateTime.Today);
            RegistraMapa("Logradouro", 133, 15, "RUA");
            RegistraMapa("CONEndereco2", 148, 100, "");
            RegistraMapa("CONNumero", 248, 10, "");
            RegistraMapa("CONBairro", 308, 30, "");
            RegistraMapa("CIDUF", 338, 2, "");
            RegistraMapa("CIDIBGE", 340, 7, 0);
            RegistraMapa("CEPN", 347, 8, 0);          
            RegistraMapa("PAIS", 355, 5, 1058);
            RegistraMapa("CNPJN", 360, 14,"");
            RegistraMapa("CONUsuPrefN", 392, 14, "");
            RegistraMapa("FIXOS"  , 490, 6, "SNNNNN");
            RegistraMapa("FIXOS1" , 696, 1, "N");
            RegistraMapa("GATEWAY", 828, 1, "N");
            RegistraMapa("LojaV"  , 845, 1, "N");
            RegistraMapa("Zero"   , 977, 1, "0");
        }
    }

    internal class RegE200 : RegistroBase
    {
        /// <summary>
        /// 
        /// </summary>
        /// <param name="_cTabTXT"></param>
        public RegE200(cTabelaTXT _cTabTXT)
            : base(_cTabTXT, null)
        {
        }

        /// <summary>
        /// 
        /// </summary>
        protected override void RegistraMapa()
        {
            base.RegistraMapa();
            RegistraMapa("ID", 1, 4, "E200");
            RegistraMapa("ES", 5, 1, "S");
            RegistraMapa("ESPECIE", 6, 5, "NFS");
            RegistraMapa("SERIE", 11, 3, "001");
            RegistraMapa("SUBSERIE", 14, 2, "");//era 01
            RegistraMapa("NOANumero", 16, 10, 0);
            RegistraMapa("copyNOANumero", 26, 10, 0);
            RegistraMapa("CONCodigo", 36, 20, "0");
            RegistraMapa("NOADataEmissao", 56, 8, DateTime.Today);
            RegistraMapa("copyNOADataEmissao", 64, 8, DateTime.Today);
            RegistraMapa("UF", 72, 2, "SP");
            RegistraMapa("MODELONF", 74, 2, "55"); //53
            RegistraMapa("FIXO", 76, 3, "P00");
            RegistraMapa("Zeros", 333, 12, 0);
            RegistraMapa("Frete", 345, 1, 1);
            RegistraMapa("TOTALNOATotal", 346, 14, 0M);
            RegistraMapa("LINHAICMS", 413, 2, 1);
            RegistraMapa("copy1NOADataEmissao", 415, 8, DateTime.Today);
        }
    }

    #region Não utilizados
    /// <summary>
    /// NÃO UTILIZADO
    /// </summary>
    internal class RegE201 : RegistroBase
    {
        /// <summary>
        /// 
        /// </summary>
        /// <param name="_cTabTXT"></param>
        public RegE201(cTabelaTXT _cTabTXT)
            : base(_cTabTXT, null)
        {
        }

        /// <summary>
        /// impostos da nota
        /// </summary>
        protected override void RegistraMapa()
        {
            base.RegistraMapa();
            RegistraMapa("ID", 1, 4, "E201");
            RegistraMapa("ES", 5, 1, "S");
            RegistraMapa("ESPECIE", 6, 5, "NFS");
            RegistraMapa("SERIE", 11, 3, "001");
            RegistraMapa("SUBSERIE", 14, 2, "");//era 01
            RegistraMapa("NOANumero", 16, 10, 0);
            RegistraMapa("CONCodigo", 26, 20, "0");
            RegistraMapa("CFOP", 46, 4, 5933);
            RegistraMapa("TIPOPAG", 52, 1, 1);
            RegistraMapa("Zeros", 53, 14, 0);
            RegistraMapa("Turbo", 67, 3, 0);
            RegistraMapa("RedBase", 70, 27, 0);
            RegistraMapa("TOTALNOATotal", 97, 14, 0);
            RegistraMapa("ICMSOUTROS", 111, 56, 0);
            RegistraMapa("ValIPI", 168, 42, 0);
            RegistraMapa("ContribICMS", 210, 1, "N");
            RegistraMapa("SEQ", 241, 2, 1);
            RegistraMapa("DEDBASE", 246, 28, 0);
            RegistraMapa("FIXO", 274, 1, "S");
            RegistraMapa("CONTROLE", 284, 1, 0);
        }
    }

    /// <summary>
    /// NÃO UTILIZADO Registro retenção de PIS/COFINS/CSLL
    /// </summary>
    internal class RegE209 : RegistroBase
    {
        /// <summary>
        /// Construtor
        /// </summary>
        /// <param name="_cTabTXT"></param>
        public RegE209(cTabelaTXT _cTabTXT)
            : base(_cTabTXT, null)
        {
        }

        /// <summary>
        /// Mapeamento
        /// </summary>
        protected override void RegistraMapa()
        {
            base.RegistraMapa();
            RegistraMapa("ID", 1, 4, "E209");
            RegistraMapa("ES", 5, 1, "S");
            RegistraMapa("ESPECIE", 6, 5, "NFS");
            RegistraMapa("SERIE", 11, 3, "001");
            RegistraMapa("SUBSERIE", 14, 2, "");//era 01
            RegistraMapa("NOANumero", 16, 10, 0);
            RegistraMapa("CONCodigo", 26, 20, "0");
            RegistraMapa("TOTALNOATotal", 46, 14, 0M);
            RegistraMapa("DataComp", 60, 10, DateTime.Today);
            RegistraMapa("ValPIS", 70, 14, 0M);
            RegistraMapa("ValCofins", 84, 14, 0M);
            RegistraMapa("ValCSLL", 98, 14, 0M);
        }
    }

    /// <summary>
    /// NÃO UTILIZADO Registro retenção de IR
    /// </summary>
    internal class RegE210 : RegistroBase
    {
        /// <summary>
        /// Retenção de IR
        /// </summary>
        /// <param name="_cTabTXT"></param>
        public RegE210(cTabelaTXT _cTabTXT)
            : base(_cTabTXT, null)
        {
        }

        /// <summary>
        /// Mapeamento
        /// </summary>
        protected override void RegistraMapa()
        {
            base.RegistraMapa();
            RegistraMapa("ID", 1, 4, "E210");
            RegistraMapa("ES", 5, 1, "S");
            RegistraMapa("ESPECIE", 6, 5, "NFS");
            RegistraMapa("SERIE", 11, 3, "001");
            RegistraMapa("SUBSERIE", 14, 2, "");//era 01
            RegistraMapa("NOANumero", 16, 10, 0);
            RegistraMapa("CONCodigo", 26, 20, "0");
            RegistraMapa("TOTALNOATotal", 46, 14, 0M);
            RegistraMapa("ALIQUOTA", 60, 4, 0M);
            RegistraMapa("ValIR", 64, 14, 0M);
        }
    }
    
    #endregion

    internal class RegE600 : RegistroBase
    {
        /// <summary>
        /// 
        /// </summary>
        /// <param name="_cTabTXT"></param>
        public RegE600(cTabelaTXT _cTabTXT)
            : base(_cTabTXT, null)
        {
        }

        /// <summary>
        /// 
        /// </summary>
        protected override void RegistraMapa()
        {
            base.RegistraMapa();
            RegistraMapa("ID", 1, 4, "E600");
            RegistraMapa("CODARQ", 5, 3, 1);
            RegistraMapa("MODLivro", 8, 2, 51);
            //RegistraMapa("PLA", 10, 10, "");            
            RegistraMapa("Zerado", 10, 10, "");            
            RegistraMapa("NOANumero", 20, 7, 0);
            RegistraMapa("copyNOANumero", 27, 7, 0);            
            RegistraMapa("NOADataEmissao", 34, 8, DateTime.Today);
            RegistraMapa("SERIE", 42, 5, "001");
            RegistraMapa("SUBSERIE", 47, 2, "");//era 01
            RegistraMapa("CodInt", 49, 3, "");
            RegistraMapa("Atividade", 52, 15, "");
            RegistraMapa("CONCodigo", 67, 20, "0");
            RegistraMapa("Valzerado", 87, 42, 0);
            RegistraMapa("TOTALISSRetido", 129, 14, 0M);
            RegistraMapa("TOTALNOATotal", 143, 14, 0M);
            RegistraMapa("ALIQUOTA", 157, 4, Framework.DSCentral.EMP == 1 ? 2M : 3M);
            RegistraMapa("TOTALISSValor", 161, 14, 0M);
            RegistraMapa("Zeroas", 175, 28, 0M);
            RegistraMapa("IRPJ", 203, 3, 32);  //  ????????????
            RegistraMapa("VENDA", 206, 1, 1);
            RegistraMapa("CSLL", 207, 1, "N");
            RegistraMapa("EXT", 208, 1, "N");
            RegistraMapa("PAIS", 209, 2, "");
            RegistraMapa("ISSRETSNTravadoS", 211, 1, "N");
            RegistraMapa("TIPONOTA", 212, 1, 0); // 0=tributada 5=retida
            RegistraMapa("CIVIL", 213, 1, "N"); // 
            RegistraMapa("CIVIL1", 214, 10, "");
            RegistraMapa("FORA", 224, 1, "1"); //1=dentro 2=fora
            RegistraMapa("OBS", 225, 40, "");
            RegistraMapa("Verificador", 265, 2, "N0");
            RegistraMapa("IBGEPREST", 267, 7,"");// Framework.DSCentral.EMP == 1 ? 3548708 : 3547809);
            RegistraMapa("Fixos"    , 282, 4, "0000");
            RegistraMapa("FIM"      , 750, 1, 0);
        }
    }

    internal class RegE602 : RegistroBase
    {
        /// <summary>
        /// 
        /// </summary>
        /// <param name="_cTabTXT"></param>
        public RegE602(cTabelaTXT _cTabTXT)
            : base(_cTabTXT, null)
        {
        }

        /// <summary>
        /// 
        /// </summary>
        protected override void RegistraMapa()
        {
            base.RegistraMapa();
            RegistraMapa("ID", 1, 4, "E602");            
            RegistraMapa("MODLivro", 5, 2, 51);            
            RegistraMapa("NOANumero", 7, 7, 0);
            RegistraMapa("SERIE", 14, 5, "001");
            RegistraMapa("TOTALNOATotal", 19, 14, 0M);
            RegistraMapa("TOTALBaseISS", 33, 14, 0M); 
            RegistraMapa("Desconto", 47, 14, 0M);
            RegistraMapa("TOTALISSValor", 61, 14, 0M);
            RegistraMapa("CODARQ", 75, 3, 1);           
        }
    }

    internal class RegE603 : RegistroBase
    {       
        /// <summary>
        /// 
        /// </summary>
        /// <param name="_cTabTXT"></param>
        public RegE603(cTabelaTXT _cTabTXT)
            : base(_cTabTXT, null)
        {            
        }

        /// <summary>
        /// 
        /// </summary>
        protected override void RegistraMapa()
        {
            base.RegistraMapa();
            RegistraMapa("ID", 1, 4, "E603");
            RegistraMapa("MODLivro", 5, 2, 51);
            RegistraMapa("NOANumero", 7, 7, 0);
            RegistraMapa("SERIE", 14, 5, "001");
            RegistraMapa("nOrdemServ", 19, 3, 1);
            RegistraMapa("PLA", 22, 20, "");
            RegistraMapa("PLADescricao", 42, 45, "");
            RegistraMapa("NOATotal", 87, 14, 0M);
            RegistraMapa("Desconto", 101, 14, 0M);
            RegistraMapa("BaseISS", 115, 14, 0M);
            RegistraMapa("ALIQUOTA", 129, 5 , Framework.DSCentral.EMP == 1 ? 2M : 3M);
            RegistraMapa("ISSValor", 134, 14, 0M);
            RegistraMapa("SitPIS", 148, 2, 1);
            RegistraMapa("BasePIS", 150, 14, 0M);
            //RegistraMapa("AliqPIS", 164, 5, Framework.DSCentral.EMP == 1 ? 1.65M : 0.65M);//065
            RegistraMapa("AliqPIS", 164, 5, 1.65M);
            RegistraMapa("UniPis", 169, 14, 0M);
            RegistraMapa("AliqPISR", 183, 8, 0M);
            //RegistraMapa(Framework.DSCentral.EMP == 1 ? "ValPIS" : "ValPISSA", 191, 14, 0M);
            RegistraMapa("ValPIS", 191, 14, 0M);
            RegistraMapa("SitCOF", 205, 2, 1);
            RegistraMapa("BaseCOF", 207, 14, 0M);
            RegistraMapa("AliqCOF", 221, 5, 7.6M);//3
            RegistraMapa("UniCOF", 226, 14, 0M);
            RegistraMapa("AliqCOFR", 240, 8, 0M);
            //RegistraMapa(Framework.DSCentral.EMP == 1 ? "ValCOF" : "ValCOFSA", 248, 14, 0M);
            RegistraMapa("ValCOF", 248, 14, 0M);
            RegistraMapa("DataAprop", 262, 8, efiscal.comp.DataF); //confirmar
            RegistraMapa("CODARQ", 270, 3, 1);
            RegistraMapa("Zero", 334, 1, "0");            
        }
    }
    
    /// <summary>
    /// Retenção de impostos
    /// </summary>
    internal class RegE604 : RegistroBase
    {
        /// <summary>
        /// Construtor
        /// </summary>
        /// <param name="_cTabTXT"></param>
        public RegE604(cTabelaTXT _cTabTXT)
            : base(_cTabTXT, null)
        {
        }

        /// <summary>
        /// Mapeamento
        /// </summary>
        protected override void RegistraMapa()
        {
            base.RegistraMapa();
            RegistraMapa("ID"       ,  1,  4, "E604");
            RegistraMapa("NOANumero",  5,  7, 0);
            RegistraMapa("SERIE"    , 12,  5, "001");            
            RegistraMapa("TOTALNOATotal", 17, 14, 0M);
            RegistraMapa("DataComp", 31, 10, efiscal.comp.DataF.ToString("dd/MM/yyyy"));
            RegistraMapa("ValretCSLL", 41, 14, 0M);
            RegistraMapa("ValretPIS", 55, 14, 0M);
            RegistraMapa("ValretCOF", 69, 14, 0M);
            RegistraMapa("CODARQ"   , 83,  3, 1);
            RegistraMapa("CODREC"   , 86,  4, "5952");
            RegistraMapa("BENEF"    , 90,  1, 0);
            RegistraMapa("Zero"     , 92,  1, 0);
        }
    }

    /// <summary>
    /// Registro retenção de IR
    /// </summary>
    internal class RegE605 : RegistroBase
    {
        /// <summary>
        /// Retenção de IR
        /// </summary>
        /// <param name="_cTabTXT"></param>
        public RegE605(cTabelaTXT _cTabTXT)
            : base(_cTabTXT, null)
        {
        }

        /// <summary>
        /// Mapeamento
        /// </summary>
        protected override void RegistraMapa()
        {
            base.RegistraMapa();
            RegistraMapa("ID"       ,  1,  4, "E605");
            RegistraMapa("MODLivro" ,  5,  2, 51);
            RegistraMapa("NOANumero",  7,  7, 0);
            RegistraMapa("SERIE"    , 14,  5, "001");                        
            RegistraMapa("TOTALNOATotal", 19, 14, 0M);
            RegistraMapa("ALIQUOTA" , 33,  4, 0M);
            RegistraMapa("TOTALRPSRetIR", 37, 14, 0M);
            RegistraMapa("BaseINSS" , 51, 14, 0M);
            RegistraMapa("ValINSS"  , 65, 14, 0M);
            RegistraMapa("OrgPub"   , 79,  1, "N");
            RegistraMapa("CodIR"    , 80,  4, "1708");
            RegistraMapa("CodContr" , 84,  4, "5952");
            RegistraMapa("CODARQ"   , 88,  3, 1);
            RegistraMapa("Nat"      , 91,  2, "03");
            RegistraMapa("Zero"     , 93,  1, 0);
        }
    }
     
}
