﻿namespace dllcontabil
{
    partial class cGeraArquivos
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            DevExpress.XtraGrid.GridLevelNode gridLevelNode1 = new DevExpress.XtraGrid.GridLevelNode();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(cGeraArquivos));
            virCompet.virCompetencia virCompetencia1 = new virCompet.virCompetencia();
            virCompet.virBuscaLimitesComp virBuscaLimitesComp1 = new virCompet.virBuscaLimitesComp();
            virCompet.virCompetencia virCompetencia2 = new virCompet.virCompetencia();
            this.gridView2 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colNOANumero = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colRPSComp = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colNOADataEmissao = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colNOATotal = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colPLA = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colPLADescricao = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colRPSReterISS = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colRPSISS = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colISSValor = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridControl1 = new DevExpress.XtraGrid.GridControl();
            this.defiscalBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.gridView1 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colCONNome = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colCON = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colCONCodigo = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colCONBairro = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colCONCep = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colCONCnpj = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colCIDNome = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colCIDUf = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colCONDataInclusao = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colCONEndereco2 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colCONNumero = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colCIDIBGE = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colCONUsuPref = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colLogradouro = new DevExpress.XtraGrid.Columns.GridColumn();
            this.simpleButton1 = new DevExpress.XtraEditors.SimpleButton();
            this.cvirCompetencia1 = new virCompetComp.cvirCompetencia();
            this.labelControl1 = new DevExpress.XtraEditors.LabelControl();
            this.panelControl1 = new DevExpress.XtraEditors.PanelControl();
            this.textEdit1 = new DevExpress.XtraEditors.TextEdit();
            this.labelArq = new DevExpress.XtraEditors.LabelControl();
            ((System.ComponentModel.ISupportInitialize)(this.StyleController_F)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.defiscalBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl1)).BeginInit();
            this.panelControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit1.Properties)).BeginInit();
            this.SuspendLayout();
            // 
            // ToolTipController_F
            // 
            this.ToolTipController_F.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.ToolTipController_F.Appearance.ForeColor = System.Drawing.Color.Black;
            this.ToolTipController_F.Appearance.Options.UseBackColor = true;
            this.ToolTipController_F.Appearance.Options.UseFont = true;
            this.ToolTipController_F.Appearance.Options.UseForeColor = true;
            this.ToolTipController_F.Appearance.Options.UseTextOptions = true;
            this.ToolTipController_F.Appearance.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            // 
            // StyleController_F
            // 
            this.StyleController_F.AppearanceDisabled.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this.StyleController_F.AppearanceDisabled.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this.StyleController_F.AppearanceDisabled.Options.UseBackColor = true;
            this.StyleController_F.AppearanceDisabled.Options.UseForeColor = true;
            this.StyleController_F.AppearanceFocused.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.StyleController_F.AppearanceFocused.Options.UseBackColor = true;
            // 
            // gridView2
            // 
            this.gridView2.Appearance.ColumnFilterButton.BackColor = System.Drawing.Color.Orange;
            this.gridView2.Appearance.ColumnFilterButton.BorderColor = System.Drawing.Color.Orange;
            this.gridView2.Appearance.ColumnFilterButton.ForeColor = System.Drawing.Color.DarkOrange;
            this.gridView2.Appearance.ColumnFilterButton.Options.UseBackColor = true;
            this.gridView2.Appearance.ColumnFilterButton.Options.UseBorderColor = true;
            this.gridView2.Appearance.ColumnFilterButton.Options.UseForeColor = true;
            this.gridView2.Appearance.ColumnFilterButtonActive.BackColor = System.Drawing.Color.DarkOrange;
            this.gridView2.Appearance.ColumnFilterButtonActive.BorderColor = System.Drawing.Color.DarkOrange;
            this.gridView2.Appearance.ColumnFilterButtonActive.ForeColor = System.Drawing.Color.White;
            this.gridView2.Appearance.ColumnFilterButtonActive.Options.UseBackColor = true;
            this.gridView2.Appearance.ColumnFilterButtonActive.Options.UseBorderColor = true;
            this.gridView2.Appearance.ColumnFilterButtonActive.Options.UseForeColor = true;
            this.gridView2.Appearance.Empty.BackColor = System.Drawing.Color.LightSkyBlue;
            this.gridView2.Appearance.Empty.BackColor2 = System.Drawing.Color.SkyBlue;
            this.gridView2.Appearance.Empty.GradientMode = System.Drawing.Drawing2D.LinearGradientMode.BackwardDiagonal;
            this.gridView2.Appearance.Empty.Options.UseBackColor = true;
            this.gridView2.Appearance.EvenRow.BackColor = System.Drawing.Color.Linen;
            this.gridView2.Appearance.EvenRow.BackColor2 = System.Drawing.Color.AntiqueWhite;
            this.gridView2.Appearance.EvenRow.GradientMode = System.Drawing.Drawing2D.LinearGradientMode.ForwardDiagonal;
            this.gridView2.Appearance.EvenRow.Options.UseBackColor = true;
            this.gridView2.Appearance.FilterCloseButton.BackColor = System.Drawing.Color.Orange;
            this.gridView2.Appearance.FilterCloseButton.BorderColor = System.Drawing.Color.Orange;
            this.gridView2.Appearance.FilterCloseButton.Options.UseBackColor = true;
            this.gridView2.Appearance.FilterCloseButton.Options.UseBorderColor = true;
            this.gridView2.Appearance.FilterPanel.BackColor = System.Drawing.Color.DarkOrange;
            this.gridView2.Appearance.FilterPanel.BackColor2 = System.Drawing.Color.Orange;
            this.gridView2.Appearance.FilterPanel.ForeColor = System.Drawing.Color.Black;
            this.gridView2.Appearance.FilterPanel.GradientMode = System.Drawing.Drawing2D.LinearGradientMode.BackwardDiagonal;
            this.gridView2.Appearance.FilterPanel.Options.UseBackColor = true;
            this.gridView2.Appearance.FilterPanel.Options.UseForeColor = true;
            this.gridView2.Appearance.FocusedRow.BackColor = System.Drawing.Color.RoyalBlue;
            this.gridView2.Appearance.FocusedRow.BackColor2 = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this.gridView2.Appearance.FocusedRow.ForeColor = System.Drawing.Color.White;
            this.gridView2.Appearance.FocusedRow.GradientMode = System.Drawing.Drawing2D.LinearGradientMode.Vertical;
            this.gridView2.Appearance.FocusedRow.Options.UseBackColor = true;
            this.gridView2.Appearance.FocusedRow.Options.UseForeColor = true;
            this.gridView2.Appearance.FooterPanel.BackColor = System.Drawing.Color.Orange;
            this.gridView2.Appearance.FooterPanel.BorderColor = System.Drawing.Color.Orange;
            this.gridView2.Appearance.FooterPanel.Options.UseBackColor = true;
            this.gridView2.Appearance.FooterPanel.Options.UseBorderColor = true;
            this.gridView2.Appearance.GroupButton.BackColor = System.Drawing.Color.Wheat;
            this.gridView2.Appearance.GroupButton.BorderColor = System.Drawing.Color.Wheat;
            this.gridView2.Appearance.GroupButton.Options.UseBackColor = true;
            this.gridView2.Appearance.GroupButton.Options.UseBorderColor = true;
            this.gridView2.Appearance.GroupFooter.BackColor = System.Drawing.Color.Wheat;
            this.gridView2.Appearance.GroupFooter.BorderColor = System.Drawing.Color.Wheat;
            this.gridView2.Appearance.GroupFooter.Options.UseBackColor = true;
            this.gridView2.Appearance.GroupFooter.Options.UseBorderColor = true;
            this.gridView2.Appearance.GroupPanel.BackColor = System.Drawing.Color.RoyalBlue;
            this.gridView2.Appearance.GroupPanel.ForeColor = System.Drawing.Color.White;
            this.gridView2.Appearance.GroupPanel.Options.UseBackColor = true;
            this.gridView2.Appearance.GroupPanel.Options.UseForeColor = true;
            this.gridView2.Appearance.GroupRow.BackColor = System.Drawing.Color.Wheat;
            this.gridView2.Appearance.GroupRow.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Bold);
            this.gridView2.Appearance.GroupRow.Options.UseBackColor = true;
            this.gridView2.Appearance.GroupRow.Options.UseFont = true;
            this.gridView2.Appearance.HeaderPanel.BackColor = System.Drawing.Color.Orange;
            this.gridView2.Appearance.HeaderPanel.BorderColor = System.Drawing.Color.Orange;
            this.gridView2.Appearance.HeaderPanel.Options.UseBackColor = true;
            this.gridView2.Appearance.HeaderPanel.Options.UseBorderColor = true;
            this.gridView2.Appearance.HideSelectionRow.BackColor = System.Drawing.Color.LightSlateGray;
            this.gridView2.Appearance.HideSelectionRow.Options.UseBackColor = true;
            this.gridView2.Appearance.HorzLine.BackColor = System.Drawing.Color.Tan;
            this.gridView2.Appearance.HorzLine.Options.UseBackColor = true;
            this.gridView2.Appearance.OddRow.BackColor = System.Drawing.Color.White;
            this.gridView2.Appearance.OddRow.Options.UseBackColor = true;
            this.gridView2.Appearance.Preview.BackColor = System.Drawing.Color.Khaki;
            this.gridView2.Appearance.Preview.BackColor2 = System.Drawing.Color.Cornsilk;
            this.gridView2.Appearance.Preview.Font = new System.Drawing.Font("Tahoma", 7.5F);
            this.gridView2.Appearance.Preview.GradientMode = System.Drawing.Drawing2D.LinearGradientMode.BackwardDiagonal;
            this.gridView2.Appearance.Preview.Options.UseBackColor = true;
            this.gridView2.Appearance.Preview.Options.UseFont = true;
            this.gridView2.Appearance.Row.BackColor = System.Drawing.Color.White;
            this.gridView2.Appearance.Row.Options.UseBackColor = true;
            this.gridView2.Appearance.RowSeparator.BackColor = System.Drawing.Color.LightSkyBlue;
            this.gridView2.Appearance.RowSeparator.Options.UseBackColor = true;
            this.gridView2.Appearance.VertLine.BackColor = System.Drawing.Color.Tan;
            this.gridView2.Appearance.VertLine.Options.UseBackColor = true;
            this.gridView2.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colNOANumero,
            this.colRPSComp,
            this.colNOADataEmissao,
            this.colNOATotal,
            this.colPLA,
            this.colPLADescricao,
            this.colRPSReterISS,
            this.colRPSISS,
            this.colISSValor});
            this.gridView2.GridControl = this.gridControl1;
            this.gridView2.Name = "gridView2";
            this.gridView2.OptionsView.ColumnAutoWidth = false;
            this.gridView2.OptionsView.EnableAppearanceEvenRow = true;
            this.gridView2.OptionsView.EnableAppearanceOddRow = true;
            this.gridView2.OptionsView.ShowGroupPanel = false;
            this.gridView2.SortInfo.AddRange(new DevExpress.XtraGrid.Columns.GridColumnSortInfo[] {
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colNOANumero, DevExpress.Data.ColumnSortOrder.Ascending)});
            this.gridView2.CustomColumnDisplayText += new DevExpress.XtraGrid.Views.Base.CustomColumnDisplayTextEventHandler(this.gridView2_CustomColumnDisplayText);
            // 
            // colNOANumero
            // 
            this.colNOANumero.Caption = "Nota";
            this.colNOANumero.FieldName = "NOANumero";
            this.colNOANumero.Name = "colNOANumero";
            this.colNOANumero.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.True;
            this.colNOANumero.OptionsColumn.ReadOnly = true;
            this.colNOANumero.SortMode = DevExpress.XtraGrid.ColumnSortMode.Value;
            this.colNOANumero.Visible = true;
            this.colNOANumero.VisibleIndex = 0;
            // 
            // colRPSComp
            // 
            this.colRPSComp.Caption = "Competência";
            this.colRPSComp.FieldName = "RPSComp";
            this.colRPSComp.Name = "colRPSComp";
            this.colRPSComp.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.True;
            this.colRPSComp.OptionsColumn.ReadOnly = true;
            this.colRPSComp.Visible = true;
            this.colRPSComp.VisibleIndex = 2;
            this.colRPSComp.Width = 95;
            // 
            // colNOADataEmissao
            // 
            this.colNOADataEmissao.Caption = "Emissão";
            this.colNOADataEmissao.DisplayFormat.FormatString = "dd/MM/yyyy";
            this.colNOADataEmissao.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.colNOADataEmissao.FieldName = "NOADataEmissao";
            this.colNOADataEmissao.Name = "colNOADataEmissao";
            this.colNOADataEmissao.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.True;
            this.colNOADataEmissao.OptionsColumn.ReadOnly = true;
            this.colNOADataEmissao.Visible = true;
            this.colNOADataEmissao.VisibleIndex = 1;
            this.colNOADataEmissao.Width = 94;
            // 
            // colNOATotal
            // 
            this.colNOATotal.Caption = "Valor";
            this.colNOATotal.DisplayFormat.FormatString = "n2";
            this.colNOATotal.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.colNOATotal.FieldName = "NOATotal";
            this.colNOATotal.Name = "colNOATotal";
            this.colNOATotal.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.True;
            this.colNOATotal.OptionsColumn.ReadOnly = true;
            this.colNOATotal.Visible = true;
            this.colNOATotal.VisibleIndex = 3;
            this.colNOATotal.Width = 116;
            // 
            // colPLA
            // 
            this.colPLA.Caption = "Conta";
            this.colPLA.FieldName = "PLA";
            this.colPLA.Name = "colPLA";
            this.colPLA.Visible = true;
            this.colPLA.VisibleIndex = 4;
            this.colPLA.Width = 83;
            // 
            // colPLADescricao
            // 
            this.colPLADescricao.Caption = "Descrição";
            this.colPLADescricao.FieldName = "PLADescricao";
            this.colPLADescricao.Name = "colPLADescricao";
            this.colPLADescricao.Visible = true;
            this.colPLADescricao.VisibleIndex = 5;
            this.colPLADescricao.Width = 276;
            // 
            // colRPSReterISS
            // 
            this.colRPSReterISS.Caption = "Reter ISS";
            this.colRPSReterISS.FieldName = "RPSReterISS";
            this.colRPSReterISS.Name = "colRPSReterISS";
            this.colRPSReterISS.Visible = true;
            this.colRPSReterISS.VisibleIndex = 6;
            // 
            // colRPSISS
            // 
            this.colRPSISS.Caption = "ISS";
            this.colRPSISS.DisplayFormat.FormatString = "{0:n2} %";
            this.colRPSISS.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.colRPSISS.FieldName = "RPSISS";
            this.colRPSISS.Name = "colRPSISS";
            this.colRPSISS.Visible = true;
            this.colRPSISS.VisibleIndex = 7;
            // 
            // colISSValor
            // 
            this.colISSValor.Caption = "Valor ISS";
            this.colISSValor.DisplayFormat.FormatString = "n2";
            this.colISSValor.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.colISSValor.FieldName = "ISSValor";
            this.colISSValor.Name = "colISSValor";
            this.colISSValor.Visible = true;
            this.colISSValor.VisibleIndex = 8;
            // 
            // gridControl1
            // 
            this.gridControl1.DataMember = "Condominio";
            this.gridControl1.DataSource = this.defiscalBindingSource;
            this.gridControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            gridLevelNode1.LevelTemplate = this.gridView2;
            gridLevelNode1.RelationName = "Condominio_NOTAS";
            this.gridControl1.LevelTree.Nodes.AddRange(new DevExpress.XtraGrid.GridLevelNode[] {
            gridLevelNode1});
            this.gridControl1.Location = new System.Drawing.Point(0, 64);
            this.gridControl1.MainView = this.gridView1;
            this.gridControl1.Name = "gridControl1";
            this.gridControl1.Size = new System.Drawing.Size(1613, 743);
            this.gridControl1.TabIndex = 4;
            this.gridControl1.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridView1,
            this.gridView2});
            // 
            // defiscalBindingSource
            // 
            this.defiscalBindingSource.DataSource = typeof(dllcontabil.defiscal);
            this.defiscalBindingSource.Position = 0;
            // 
            // gridView1
            // 
            this.gridView1.Appearance.ColumnFilterButton.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(104)))), ((int)(((byte)(184)))), ((int)(((byte)(251)))));
            this.gridView1.Appearance.ColumnFilterButton.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(104)))), ((int)(((byte)(184)))), ((int)(((byte)(251)))));
            this.gridView1.Appearance.ColumnFilterButton.ForeColor = System.Drawing.Color.White;
            this.gridView1.Appearance.ColumnFilterButton.Options.UseBackColor = true;
            this.gridView1.Appearance.ColumnFilterButton.Options.UseBorderColor = true;
            this.gridView1.Appearance.ColumnFilterButton.Options.UseForeColor = true;
            this.gridView1.Appearance.ColumnFilterButtonActive.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(170)))), ((int)(((byte)(216)))), ((int)(((byte)(254)))));
            this.gridView1.Appearance.ColumnFilterButtonActive.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(170)))), ((int)(((byte)(216)))), ((int)(((byte)(254)))));
            this.gridView1.Appearance.ColumnFilterButtonActive.ForeColor = System.Drawing.Color.Black;
            this.gridView1.Appearance.ColumnFilterButtonActive.Options.UseBackColor = true;
            this.gridView1.Appearance.ColumnFilterButtonActive.Options.UseBorderColor = true;
            this.gridView1.Appearance.ColumnFilterButtonActive.Options.UseForeColor = true;
            this.gridView1.Appearance.Empty.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(236)))), ((int)(((byte)(246)))), ((int)(((byte)(255)))));
            this.gridView1.Appearance.Empty.BackColor2 = System.Drawing.Color.White;
            this.gridView1.Appearance.Empty.Options.UseBackColor = true;
            this.gridView1.Appearance.EvenRow.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(247)))), ((int)(((byte)(251)))), ((int)(((byte)(255)))));
            this.gridView1.Appearance.EvenRow.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(247)))), ((int)(((byte)(251)))), ((int)(((byte)(255)))));
            this.gridView1.Appearance.EvenRow.ForeColor = System.Drawing.Color.Black;
            this.gridView1.Appearance.EvenRow.Options.UseBackColor = true;
            this.gridView1.Appearance.EvenRow.Options.UseBorderColor = true;
            this.gridView1.Appearance.EvenRow.Options.UseForeColor = true;
            this.gridView1.Appearance.FilterCloseButton.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(104)))), ((int)(((byte)(184)))), ((int)(((byte)(251)))));
            this.gridView1.Appearance.FilterCloseButton.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(104)))), ((int)(((byte)(184)))), ((int)(((byte)(251)))));
            this.gridView1.Appearance.FilterCloseButton.ForeColor = System.Drawing.Color.White;
            this.gridView1.Appearance.FilterCloseButton.Options.UseBackColor = true;
            this.gridView1.Appearance.FilterCloseButton.Options.UseBorderColor = true;
            this.gridView1.Appearance.FilterCloseButton.Options.UseForeColor = true;
            this.gridView1.Appearance.FilterPanel.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(236)))), ((int)(((byte)(246)))), ((int)(((byte)(255)))));
            this.gridView1.Appearance.FilterPanel.BackColor2 = System.Drawing.Color.White;
            this.gridView1.Appearance.FilterPanel.ForeColor = System.Drawing.Color.Black;
            this.gridView1.Appearance.FilterPanel.Options.UseBackColor = true;
            this.gridView1.Appearance.FilterPanel.Options.UseForeColor = true;
            this.gridView1.Appearance.FixedLine.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(59)))), ((int)(((byte)(133)))), ((int)(((byte)(195)))));
            this.gridView1.Appearance.FixedLine.Options.UseBackColor = true;
            this.gridView1.Appearance.FocusedCell.BackColor = System.Drawing.Color.White;
            this.gridView1.Appearance.FocusedCell.ForeColor = System.Drawing.Color.Black;
            this.gridView1.Appearance.FocusedCell.Options.UseBackColor = true;
            this.gridView1.Appearance.FocusedCell.Options.UseForeColor = true;
            this.gridView1.Appearance.FocusedRow.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(38)))), ((int)(((byte)(109)))), ((int)(((byte)(189)))));
            this.gridView1.Appearance.FocusedRow.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(59)))), ((int)(((byte)(139)))), ((int)(((byte)(206)))));
            this.gridView1.Appearance.FocusedRow.ForeColor = System.Drawing.Color.White;
            this.gridView1.Appearance.FocusedRow.Options.UseBackColor = true;
            this.gridView1.Appearance.FocusedRow.Options.UseBorderColor = true;
            this.gridView1.Appearance.FocusedRow.Options.UseForeColor = true;
            this.gridView1.Appearance.FooterPanel.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(104)))), ((int)(((byte)(184)))), ((int)(((byte)(251)))));
            this.gridView1.Appearance.FooterPanel.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(104)))), ((int)(((byte)(184)))), ((int)(((byte)(251)))));
            this.gridView1.Appearance.FooterPanel.ForeColor = System.Drawing.Color.Black;
            this.gridView1.Appearance.FooterPanel.Options.UseBackColor = true;
            this.gridView1.Appearance.FooterPanel.Options.UseBorderColor = true;
            this.gridView1.Appearance.FooterPanel.Options.UseForeColor = true;
            this.gridView1.Appearance.GroupButton.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(104)))), ((int)(((byte)(184)))), ((int)(((byte)(251)))));
            this.gridView1.Appearance.GroupButton.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(104)))), ((int)(((byte)(184)))), ((int)(((byte)(251)))));
            this.gridView1.Appearance.GroupButton.Options.UseBackColor = true;
            this.gridView1.Appearance.GroupButton.Options.UseBorderColor = true;
            this.gridView1.Appearance.GroupFooter.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(170)))), ((int)(((byte)(216)))), ((int)(((byte)(254)))));
            this.gridView1.Appearance.GroupFooter.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(170)))), ((int)(((byte)(216)))), ((int)(((byte)(254)))));
            this.gridView1.Appearance.GroupFooter.ForeColor = System.Drawing.Color.Black;
            this.gridView1.Appearance.GroupFooter.Options.UseBackColor = true;
            this.gridView1.Appearance.GroupFooter.Options.UseBorderColor = true;
            this.gridView1.Appearance.GroupFooter.Options.UseForeColor = true;
            this.gridView1.Appearance.GroupPanel.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(236)))), ((int)(((byte)(246)))), ((int)(((byte)(255)))));
            this.gridView1.Appearance.GroupPanel.BackColor2 = System.Drawing.Color.White;
            this.gridView1.Appearance.GroupPanel.ForeColor = System.Drawing.Color.Black;
            this.gridView1.Appearance.GroupPanel.Options.UseBackColor = true;
            this.gridView1.Appearance.GroupPanel.Options.UseForeColor = true;
            this.gridView1.Appearance.GroupRow.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(170)))), ((int)(((byte)(216)))), ((int)(((byte)(254)))));
            this.gridView1.Appearance.GroupRow.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(170)))), ((int)(((byte)(216)))), ((int)(((byte)(254)))));
            this.gridView1.Appearance.GroupRow.ForeColor = System.Drawing.Color.Black;
            this.gridView1.Appearance.GroupRow.Options.UseBackColor = true;
            this.gridView1.Appearance.GroupRow.Options.UseBorderColor = true;
            this.gridView1.Appearance.GroupRow.Options.UseForeColor = true;
            this.gridView1.Appearance.HeaderPanel.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(139)))), ((int)(((byte)(201)))), ((int)(((byte)(254)))));
            this.gridView1.Appearance.HeaderPanel.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(139)))), ((int)(((byte)(201)))), ((int)(((byte)(254)))));
            this.gridView1.Appearance.HeaderPanel.ForeColor = System.Drawing.Color.Black;
            this.gridView1.Appearance.HeaderPanel.Options.UseBackColor = true;
            this.gridView1.Appearance.HeaderPanel.Options.UseBorderColor = true;
            this.gridView1.Appearance.HeaderPanel.Options.UseForeColor = true;
            this.gridView1.Appearance.HideSelectionRow.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(105)))), ((int)(((byte)(170)))), ((int)(((byte)(225)))));
            this.gridView1.Appearance.HideSelectionRow.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(83)))), ((int)(((byte)(155)))), ((int)(((byte)(215)))));
            this.gridView1.Appearance.HideSelectionRow.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(236)))), ((int)(((byte)(246)))), ((int)(((byte)(255)))));
            this.gridView1.Appearance.HideSelectionRow.Options.UseBackColor = true;
            this.gridView1.Appearance.HideSelectionRow.Options.UseBorderColor = true;
            this.gridView1.Appearance.HideSelectionRow.Options.UseForeColor = true;
            this.gridView1.Appearance.HorzLine.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(104)))), ((int)(((byte)(184)))), ((int)(((byte)(251)))));
            this.gridView1.Appearance.HorzLine.Options.UseBackColor = true;
            this.gridView1.Appearance.OddRow.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(236)))), ((int)(((byte)(246)))), ((int)(((byte)(255)))));
            this.gridView1.Appearance.OddRow.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(236)))), ((int)(((byte)(246)))), ((int)(((byte)(255)))));
            this.gridView1.Appearance.OddRow.ForeColor = System.Drawing.Color.Black;
            this.gridView1.Appearance.OddRow.Options.UseBackColor = true;
            this.gridView1.Appearance.OddRow.Options.UseBorderColor = true;
            this.gridView1.Appearance.OddRow.Options.UseForeColor = true;
            this.gridView1.Appearance.Preview.Font = new System.Drawing.Font("Verdana", 7.5F);
            this.gridView1.Appearance.Preview.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(83)))), ((int)(((byte)(155)))), ((int)(((byte)(215)))));
            this.gridView1.Appearance.Preview.Options.UseFont = true;
            this.gridView1.Appearance.Preview.Options.UseForeColor = true;
            this.gridView1.Appearance.Row.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(247)))), ((int)(((byte)(251)))), ((int)(((byte)(255)))));
            this.gridView1.Appearance.Row.ForeColor = System.Drawing.Color.Black;
            this.gridView1.Appearance.Row.Options.UseBackColor = true;
            this.gridView1.Appearance.Row.Options.UseForeColor = true;
            this.gridView1.Appearance.RowSeparator.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(236)))), ((int)(((byte)(246)))), ((int)(((byte)(255)))));
            this.gridView1.Appearance.RowSeparator.BackColor2 = System.Drawing.Color.White;
            this.gridView1.Appearance.RowSeparator.Options.UseBackColor = true;
            this.gridView1.Appearance.SelectedRow.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(83)))), ((int)(((byte)(155)))), ((int)(((byte)(215)))));
            this.gridView1.Appearance.SelectedRow.ForeColor = System.Drawing.Color.White;
            this.gridView1.Appearance.SelectedRow.Options.UseBackColor = true;
            this.gridView1.Appearance.SelectedRow.Options.UseForeColor = true;
            this.gridView1.Appearance.TopNewRow.BackColor = System.Drawing.Color.White;
            this.gridView1.Appearance.TopNewRow.Options.UseBackColor = true;
            this.gridView1.Appearance.VertLine.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(104)))), ((int)(((byte)(184)))), ((int)(((byte)(251)))));
            this.gridView1.Appearance.VertLine.Options.UseBackColor = true;
            this.gridView1.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colCONNome,
            this.colCON,
            this.colCONCodigo,
            this.colCONBairro,
            this.colCONCep,
            this.colCONCnpj,
            this.colCIDNome,
            this.colCIDUf,
            this.colCONDataInclusao,
            this.colCONEndereco2,
            this.colCONNumero,
            this.colCIDIBGE,
            this.colCONUsuPref,
            this.colLogradouro});
            this.gridView1.GridControl = this.gridControl1;
            this.gridView1.Name = "gridView1";
            this.gridView1.OptionsDetail.ShowDetailTabs = false;
            this.gridView1.OptionsView.ColumnAutoWidth = false;
            this.gridView1.OptionsView.EnableAppearanceEvenRow = true;
            this.gridView1.OptionsView.EnableAppearanceOddRow = true;
            this.gridView1.OptionsView.ShowAutoFilterRow = true;
            this.gridView1.OptionsView.ShowGroupPanel = false;
            this.gridView1.SortInfo.AddRange(new DevExpress.XtraGrid.Columns.GridColumnSortInfo[] {
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colCONCodigo, DevExpress.Data.ColumnSortOrder.Ascending)});
            // 
            // colCONNome
            // 
            this.colCONNome.Caption = "Nome";
            this.colCONNome.FieldName = "CONNome";
            this.colCONNome.Name = "colCONNome";
            this.colCONNome.OptionsColumn.ReadOnly = true;
            this.colCONNome.Visible = true;
            this.colCONNome.VisibleIndex = 2;
            this.colCONNome.Width = 236;
            // 
            // colCON
            // 
            this.colCON.FieldName = "CON";
            this.colCON.Name = "colCON";
            this.colCON.OptionsColumn.ReadOnly = true;
            // 
            // colCONCodigo
            // 
            this.colCONCodigo.Caption = "CODCON";
            this.colCONCodigo.FieldName = "CONCodigo";
            this.colCONCodigo.Name = "colCONCodigo";
            this.colCONCodigo.OptionsColumn.ReadOnly = true;
            this.colCONCodigo.Visible = true;
            this.colCONCodigo.VisibleIndex = 0;
            // 
            // colCONBairro
            // 
            this.colCONBairro.Caption = "Bairro";
            this.colCONBairro.FieldName = "CONBairro";
            this.colCONBairro.Name = "colCONBairro";
            this.colCONBairro.OptionsColumn.ReadOnly = true;
            this.colCONBairro.Visible = true;
            this.colCONBairro.VisibleIndex = 3;
            this.colCONBairro.Width = 93;
            // 
            // colCONCep
            // 
            this.colCONCep.Caption = "CEP";
            this.colCONCep.FieldName = "CONCep";
            this.colCONCep.Name = "colCONCep";
            this.colCONCep.OptionsColumn.ReadOnly = true;
            this.colCONCep.Visible = true;
            this.colCONCep.VisibleIndex = 4;
            // 
            // colCONCnpj
            // 
            this.colCONCnpj.Caption = "CNPJ";
            this.colCONCnpj.FieldName = "CONCnpj";
            this.colCONCnpj.Name = "colCONCnpj";
            this.colCONCnpj.OptionsColumn.ReadOnly = true;
            this.colCONCnpj.Visible = true;
            this.colCONCnpj.VisibleIndex = 1;
            this.colCONCnpj.Width = 123;
            // 
            // colCIDNome
            // 
            this.colCIDNome.Caption = "CIDADE";
            this.colCIDNome.FieldName = "CIDNome";
            this.colCIDNome.Name = "colCIDNome";
            this.colCIDNome.OptionsColumn.ReadOnly = true;
            this.colCIDNome.Visible = true;
            this.colCIDNome.VisibleIndex = 5;
            // 
            // colCIDUf
            // 
            this.colCIDUf.Caption = "UF";
            this.colCIDUf.FieldName = "CIDUf";
            this.colCIDUf.Name = "colCIDUf";
            this.colCIDUf.OptionsColumn.ReadOnly = true;
            this.colCIDUf.Visible = true;
            this.colCIDUf.VisibleIndex = 6;
            // 
            // colCONDataInclusao
            // 
            this.colCONDataInclusao.Caption = "Inclusão";
            this.colCONDataInclusao.FieldName = "CONDataInclusao";
            this.colCONDataInclusao.Name = "colCONDataInclusao";
            this.colCONDataInclusao.OptionsColumn.ReadOnly = true;
            this.colCONDataInclusao.Visible = true;
            this.colCONDataInclusao.VisibleIndex = 7;
            // 
            // colCONEndereco2
            // 
            this.colCONEndereco2.Caption = "Endereço";
            this.colCONEndereco2.FieldName = "CONEndereco2";
            this.colCONEndereco2.Name = "colCONEndereco2";
            this.colCONEndereco2.OptionsColumn.ReadOnly = true;
            this.colCONEndereco2.Visible = true;
            this.colCONEndereco2.VisibleIndex = 9;
            this.colCONEndereco2.Width = 188;
            // 
            // colCONNumero
            // 
            this.colCONNumero.Caption = "Número";
            this.colCONNumero.FieldName = "CONNumero";
            this.colCONNumero.Name = "colCONNumero";
            this.colCONNumero.OptionsColumn.ReadOnly = true;
            this.colCONNumero.Visible = true;
            this.colCONNumero.VisibleIndex = 10;
            // 
            // colCIDIBGE
            // 
            this.colCIDIBGE.Caption = "IBGE";
            this.colCIDIBGE.FieldName = "CIDIBGE";
            this.colCIDIBGE.Name = "colCIDIBGE";
            this.colCIDIBGE.OptionsColumn.ReadOnly = true;
            this.colCIDIBGE.Visible = true;
            this.colCIDIBGE.VisibleIndex = 11;
            // 
            // colCONUsuPref
            // 
            this.colCONUsuPref.Caption = "I.M.";
            this.colCONUsuPref.FieldName = "CONUsuPref";
            this.colCONUsuPref.Name = "colCONUsuPref";
            this.colCONUsuPref.OptionsColumn.ReadOnly = true;
            this.colCONUsuPref.Visible = true;
            this.colCONUsuPref.VisibleIndex = 12;
            // 
            // colLogradouro
            // 
            this.colLogradouro.Caption = "Log.";
            this.colLogradouro.FieldName = "Logradouro";
            this.colLogradouro.Name = "colLogradouro";
            this.colLogradouro.Visible = true;
            this.colLogradouro.VisibleIndex = 8;
            this.colLogradouro.Width = 62;
            // 
            // simpleButton1
            // 
            this.simpleButton1.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("simpleButton1.ImageOptions.Image")));
            this.simpleButton1.ImageOptions.Location = DevExpress.XtraEditors.ImageLocation.MiddleLeft;
            this.simpleButton1.Location = new System.Drawing.Point(193, 5);
            this.simpleButton1.Name = "simpleButton1";
            this.simpleButton1.Size = new System.Drawing.Size(140, 53);
            this.simpleButton1.TabIndex = 0;
            this.simpleButton1.Text = "Gerar Arquivo";
            this.simpleButton1.Click += new System.EventHandler(this.simpleButton1_Click);
            // 
            // cvirCompetencia1
            // 
            this.cvirCompetencia1.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.cvirCompetencia1.Appearance.Options.UseBackColor = true;
            this.cvirCompetencia1.CaixaAltaGeral = true;
            this.cvirCompetencia1.CompetenciaBind = 200001;
            virCompetencia1.Ano = 2014;
            virCompetencia1.BuscaLimitesComp = virBuscaLimitesComp1;
            virCompetencia1.CompetenciaBind = 201402;
            virCompetencia1.Mes = 2;
            this.cvirCompetencia1.CompMaxima = virCompetencia1;
            virCompetencia2.Ano = 2000;
            virCompetencia2.BuscaLimitesComp = virBuscaLimitesComp1;
            virCompetencia2.CompetenciaBind = 200001;
            virCompetencia2.Mes = 1;
            this.cvirCompetencia1.CompMinima = virCompetencia2;
            this.cvirCompetencia1.Doc = System.Windows.Forms.DockStyle.None;
            this.cvirCompetencia1.Estado = CompontesBasicos.EstadosDosComponentes.Registrado;
            this.cvirCompetencia1.IsNull = false;
            this.cvirCompetencia1.Location = new System.Drawing.Point(70, 23);
            this.cvirCompetencia1.LookAndFeel.UseDefaultLookAndFeel = false;
            this.cvirCompetencia1.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.cvirCompetencia1.Name = "cvirCompetencia1";
            this.cvirCompetencia1.PermiteNulo = false;
            this.cvirCompetencia1.Size = new System.Drawing.Size(117, 24);
            this.cvirCompetencia1.somenteleitura = false;
            this.cvirCompetencia1.TabIndex = 1;
            this.cvirCompetencia1.Titulo = null;
            // 
            // labelControl1
            // 
            this.labelControl1.Appearance.Font = new System.Drawing.Font("Tahoma", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl1.Appearance.Options.UseFont = true;
            this.labelControl1.Location = new System.Drawing.Point(22, 29);
            this.labelControl1.Name = "labelControl1";
            this.labelControl1.Size = new System.Drawing.Size(30, 18);
            this.labelControl1.TabIndex = 2;
            this.labelControl1.Text = "Mês";
            // 
            // panelControl1
            // 
            this.panelControl1.Controls.Add(this.textEdit1);
            this.panelControl1.Controls.Add(this.labelArq);
            this.panelControl1.Controls.Add(this.simpleButton1);
            this.panelControl1.Controls.Add(this.labelControl1);
            this.panelControl1.Controls.Add(this.cvirCompetencia1);
            this.panelControl1.Dock = System.Windows.Forms.DockStyle.Top;
            this.panelControl1.Location = new System.Drawing.Point(0, 0);
            this.panelControl1.Name = "panelControl1";
            this.panelControl1.Size = new System.Drawing.Size(1613, 64);
            this.panelControl1.TabIndex = 3;
            // 
            // textEdit1
            // 
            this.textEdit1.Location = new System.Drawing.Point(464, 26);
            this.textEdit1.Name = "textEdit1";
            this.textEdit1.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textEdit1.Properties.Appearance.Options.UseFont = true;
            this.textEdit1.Properties.ReadOnly = true;
            this.textEdit1.Size = new System.Drawing.Size(489, 24);
            this.textEdit1.TabIndex = 4;
            this.textEdit1.Visible = false;
            // 
            // labelArq
            // 
            this.labelArq.Appearance.Font = new System.Drawing.Font("Tahoma", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelArq.Appearance.Options.UseFont = true;
            this.labelArq.Location = new System.Drawing.Point(339, 29);
            this.labelArq.Name = "labelArq";
            this.labelArq.Size = new System.Drawing.Size(119, 18);
            this.labelArq.TabIndex = 3;
            this.labelArq.Text = "Arquivo gerado:";
            this.labelArq.Visible = false;
            // 
            // cGeraArquivos
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.gridControl1);
            this.Controls.Add(this.panelControl1);
            this.Doc = System.Windows.Forms.DockStyle.Fill;
            this.LookAndFeel.UseDefaultLookAndFeel = false;
            this.Name = "cGeraArquivos";
            this.Size = new System.Drawing.Size(1613, 807);
            this.Titulo = "Geração de arquivos para SPED";
            ((System.ComponentModel.ISupportInitialize)(this.StyleController_F)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.defiscalBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl1)).EndInit();
            this.panelControl1.ResumeLayout(false);
            this.panelControl1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit1.Properties)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraEditors.SimpleButton simpleButton1;
        private virCompetComp.cvirCompetencia cvirCompetencia1;
        private DevExpress.XtraEditors.LabelControl labelControl1;
        private DevExpress.XtraEditors.PanelControl panelControl1;
        private DevExpress.XtraGrid.GridControl gridControl1;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView1;
        private System.Windows.Forms.BindingSource defiscalBindingSource;
        private DevExpress.XtraGrid.Columns.GridColumn colCONNome;
        private DevExpress.XtraGrid.Columns.GridColumn colCON;
        private DevExpress.XtraGrid.Columns.GridColumn colCONCodigo;
        private DevExpress.XtraGrid.Columns.GridColumn colCONBairro;
        private DevExpress.XtraGrid.Columns.GridColumn colCONCep;
        private DevExpress.XtraGrid.Columns.GridColumn colCONCnpj;
        private DevExpress.XtraGrid.Columns.GridColumn colCIDNome;
        private DevExpress.XtraGrid.Columns.GridColumn colCIDUf;
        private DevExpress.XtraGrid.Columns.GridColumn colCONDataInclusao;
        private DevExpress.XtraGrid.Columns.GridColumn colCONEndereco2;
        private DevExpress.XtraGrid.Columns.GridColumn colCONNumero;
        private DevExpress.XtraGrid.Columns.GridColumn colCIDIBGE;
        private DevExpress.XtraGrid.Columns.GridColumn colCONUsuPref;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView2;
        private DevExpress.XtraGrid.Columns.GridColumn colNOANumero;
        private DevExpress.XtraGrid.Columns.GridColumn colRPSComp;
        private DevExpress.XtraGrid.Columns.GridColumn colNOADataEmissao;
        private DevExpress.XtraGrid.Columns.GridColumn colNOATotal;
        private DevExpress.XtraGrid.Columns.GridColumn colPLA;
        private DevExpress.XtraGrid.Columns.GridColumn colPLADescricao;
        private DevExpress.XtraGrid.Columns.GridColumn colLogradouro;
        private DevExpress.XtraEditors.TextEdit textEdit1;
        private DevExpress.XtraEditors.LabelControl labelArq;
        private DevExpress.XtraGrid.Columns.GridColumn colRPSReterISS;
        private DevExpress.XtraGrid.Columns.GridColumn colRPSISS;
        private DevExpress.XtraGrid.Columns.GridColumn colISSValor;
    }
}
