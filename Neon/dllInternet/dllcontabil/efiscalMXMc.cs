﻿using System;
using ArquivosTXT;

namespace dllcontabil
{
    /// <summary>
    /// Classe para geração de arquivos efiscal para MXM
    /// </summary>
    public class efiscalMXMc
    {        
        private defiscal defiscal;

        private cTabelaTXT ArquivoSaida;

        private virCompet.virCompetencia comp;

        private RegMXM02 RegMXM02;

        /// <summary>
        /// último erro
        /// </summary>
        public Exception UltimoErro;

        public efiscalMXMc(virCompet.virCompetencia _comp, defiscal defiscal1)
        {
            comp = _comp;
            defiscal = defiscal1;
        }

        /// <summary>
        /// Gera o arquivo
        /// </summary>
        /// <param name="Nome"></param>
        /// <returns></returns>
        public bool gerarArquivo(string Nome)
        {
            bool retorno = true;
            try
            {
                ArquivoSaida = new cTabelaTXT(ModelosTXT.layout);
                ArquivoSaida.Formatodata = FormatoData.ddMMyyyy;
                ArquivoSaida.IniciaGravacao(Nome);
                RegMXM02 = new RegMXM02(ArquivoSaida);
                /*
                regE001 = new RegE001(ArquivoSaida);
                regE010 = new RegE010(ArquivoSaida);
                //regE200 = new RegE200(ArquivoSaida);
                //regE201 = new RegE201(ArquivoSaida);
                regE600 = new RegE600(ArquivoSaida);
                regE602 = new RegE602(ArquivoSaida);
                regE603 = new RegE603(ArquivoSaida);
                regE604 = new RegE604(ArquivoSaida);
                regE605 = new RegE605(ArquivoSaida);
                regE001.GravaLinha();
                */

                foreach (System.Data.DataRow DR in defiscal.Condominio.Rows)
                    RegMXM02.GravaLinha(DR);
                

                int iTeto = defiscal.NOTAS.Rows.Count - 1;
                int icursor = 0;
                while (icursor <= iTeto)
                {
                    defiscal.NOTASRow NotaBase = defiscal.NOTAS[icursor];
                    NotaBase.TOTALNOATotal = NotaBase.NOATotal;
                    NotaBase.TOTALISSRetido = NotaBase.ISSRetido;
                    NotaBase.TOTALISSValor = NotaBase.ISSValor;
                    NotaBase.TOTALRPSRetIR = NotaBase.RPSRetIR;
                    NotaBase.nOrdemServ = 1;
                    int iFimNota = icursor;
                    int nOrdemServ = 2;
                    while ((iFimNota < iTeto) && (NotaBase.RPS.Equals(defiscal.NOTAS[iFimNota + 1].RPS)))
                    {
                        iFimNota++;
                        defiscal.NOTASRow NotaDet = defiscal.NOTAS[iFimNota];
                        NotaDet.nOrdemServ = nOrdemServ++;
                        NotaBase.TOTALNOATotal += NotaDet.NOATotal;
                        NotaBase.TOTALISSRetido += NotaDet.ISSRetido;
                        NotaBase.TOTALISSValor += NotaDet.ISSValor;
                        NotaBase.TOTALRPSRetIR += NotaDet.RPSRetIR;
                    }
                    /*
                    regE600.GravaLinha(NotaBase);
                    regE602.GravaLinha(NotaBase);
                    for (int kcursor = icursor; kcursor <= iFimNota; kcursor++)
                        regE603.GravaLinha(defiscal.NOTAS[kcursor]);
                    regE604.GravaLinha(NotaBase);
                    regE605.GravaLinha(NotaBase);
                    */
                    icursor = iFimNota + 1;
                }
            }
            catch (Exception e)
            {
                UltimoErro = e;
                return false;
            }
            finally
            {
                if (!ArquivoSaida.TerminaGravacao())
                    retorno = false;
            }

            return retorno;
        }
    }

    internal class RegMXM02 : RegistroBase
    {
        /// <summary>
        /// 
        /// </summary>
        /// <param name="_cTabTXT"></param>
        public RegMXM02(cTabelaTXT _cTabTXT)
            : base(_cTabTXT)
        {
            CarregaLinha(null);
        }

        /// <summary>
        /// 
        /// </summary>
        protected override void RegistraMapa()
        {
            base.RegistraMapa();
            RegistraMapa("ID", 1, 2, "02");
            RegistraMapa("CONCodigo", 3, 15, "");
            RegistraMapa("TipoPJ", 18, 1, "J");
            RegistraMapa("CONNome", 19, 40, "");
            RegistraMapa("CONDataInclusao", 59, 8, DateTime.Today);
            RegistraMapa("CNPJN", 67, 18, "");
            RegistraMapa("CONUsuPref", 103, 18, "");
            RegistraMapa("CONEndereco", 157, 80, "");
            RegistraMapa("CONBairro", 237, 20, "");
            RegistraMapa("CONCep", 257, 9, 0);
            RegistraMapa("CIDNome", 266, 20, 0);
            RegistraMapa("CIDUF", 286, 2, "");
            RegistraMapa("email", 288, 50, "");
            RegistraMapa("NASC", 443, 3, "01 ");
            RegistraMapa("ATIVO", 456, 1, "A");
            RegistraMapa("copiaCONNome", 458, 40, "");
            RegistraMapa("CONNumero", 498, 5, "");
            RegistraMapa("PAIS1", 553, 20, "BRA");
            RegistraMapa("PAIS2", 573, 20, "BRA");
            RegistraMapa("CIDIBGEstr", 593, 20, "");            
        }
    }
}
