﻿using CompontesBasicos.ObjetosEstaticos;

namespace dllcontabil
{

    /// <summary>
    /// 
    /// </summary>
    public partial class defiscal
    {

        private defiscalTableAdapters.CondominioTableAdapter condominioTableAdapter;
        /// <summary>
        /// TableAdapter padrão para tabela: Condominio
        /// </summary>
        public defiscalTableAdapters.CondominioTableAdapter CondominioTableAdapter
        {
            get
            {
                if (condominioTableAdapter == null)
                {
                    condominioTableAdapter = new defiscalTableAdapters.CondominioTableAdapter();
                    condominioTableAdapter.TrocarStringDeConexao();
                };
                return condominioTableAdapter;
            }
        }

        private defiscalTableAdapters.NOTASTableAdapter nOTASTableAdapter;
        /// <summary>
        /// TableAdapter padrão para tabela: NOTAS
        /// </summary>
        public defiscalTableAdapters.NOTASTableAdapter NOTASTableAdapter
        {
            get
            {
                if (nOTASTableAdapter == null)
                {
                    nOTASTableAdapter = new defiscalTableAdapters.NOTASTableAdapter();
                    nOTASTableAdapter.TrocarStringDeConexao();
                };
                return nOTASTableAdapter;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        public int IBGELocal
        {
            get { return Framework.DSCentral.EMP == 1 ? 3548708 : 3547809; }
        }

        /// <summary>
        /// 
        /// </summary>
        public void AjustarCamposCalculadosNOA()
        {
            foreach (defiscal.NOTASRow rowNota in NOTAS)
            {
                rowNota.ISSValor = System.Math.Round(rowNota.NOATotal * rowNota.RPSISS / 100, 2);
                rowNota.ISSRetido = rowNota.RPSReterISS ? rowNota.ISSValor : 0;
                rowNota.ISSRETSN = rowNota.RPSReterISS ? "S" : "N";
                rowNota.FORA = (rowNota.CondominioRow.CIDIBGE == IBGELocal) ? 1 : 2; //1=dentro 2=fora
                if (rowNota.IsRPSRetIRNull())
                    rowNota.RPSRetIR = 0;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        public void AjustarCamposCalculadosCON()
        {
            System.Collections.SortedList CNPJsATIVOS = new System.Collections.SortedList();
            System.Collections.ArrayList apagar = new System.Collections.ArrayList();
            foreach (defiscal.CondominioRow rowCON in Condominio)
            {

                if (!rowCON.IsCONCepNull())
                    rowCON.CEPN = int.Parse(StringEdit.Limpa(rowCON.CONCep, CompontesBasicos.ObjetosEstaticos.StringEdit.TiposLimpesa.SoNumeros));
                DocBacarios.CPFCNPJ cnpj = new DocBacarios.CPFCNPJ(rowCON.CONCnpj);
                if (CNPJsATIVOS.ContainsKey(cnpj.Valor))
                {
                    apagar.Add(rowCON);
                    foreach (defiscal.NOTASRow rown in rowCON.GetNOTASRows())
                        rown.CONCodigo = (string)CNPJsATIVOS[cnpj.Valor];
                }
                else
                    CNPJsATIVOS.Add(cnpj.Valor, rowCON.CONCodigo);
                rowCON.CNPJN = cnpj.ToString(false);
                rowCON.CONNome = CompontesBasicos.ObjetosEstaticos.StringEdit.Limpa(rowCON.CONNome, StringEdit.TiposLimpesa.RemoveAcentos);
                if (!rowCON.IsCONUsuPrefNull())
                {
                    string manobra = StringEdit.Limpa(rowCON.CONUsuPref, StringEdit.TiposLimpesa.SoNumeros);
                    if (manobra != "")
                        rowCON.CONUsuPrefN = long.Parse(manobra);
                }
                if (rowCON.CONEndereco2.ToUpper().StartsWith("R."))
                {
                    rowCON.CONEndereco2 = rowCON.CONEndereco2.Substring(2).Trim();
                    rowCON.Logradouro = "RUA";
                }
                else if (rowCON.CONEndereco2.ToUpper().StartsWith("RUA "))
                {
                    rowCON.CONEndereco2 = rowCON.CONEndereco2.Substring(4).Trim();
                    rowCON.Logradouro = "RUA";
                }
                else if (rowCON.CONEndereco2.ToUpper().StartsWith("R "))
                {
                    rowCON.CONEndereco2 = rowCON.CONEndereco2.Substring(2).Trim();
                    rowCON.Logradouro = "RUA";
                }
                else if (rowCON.CONEndereco2.ToUpper().StartsWith("AV."))
                {
                    rowCON.CONEndereco2 = rowCON.CONEndereco2.Substring(3).Trim();
                    rowCON.Logradouro = "AVENIDA";
                }
                else if (rowCON.CONEndereco2.ToUpper().StartsWith("AVENIDA"))
                {
                    rowCON.CONEndereco2 = rowCON.CONEndereco2.Substring(7).Trim();
                    rowCON.Logradouro = "AVENIDA";
                }
                else if (rowCON.CONEndereco2.ToUpper().StartsWith("AV "))
                {
                    rowCON.CONEndereco2 = rowCON.CONEndereco2.Substring(3).Trim();
                    rowCON.Logradouro = "AVENIDA";
                }
                else if (rowCON.CONEndereco2.ToUpper().StartsWith("ALAMEDA"))
                {
                    rowCON.CONEndereco2 = rowCON.CONEndereco2.Substring(7).Trim();
                    rowCON.Logradouro = "ALAMEDA";
                }
                else if (rowCON.CONEndereco2.ToUpper().StartsWith("AL."))
                {
                    rowCON.CONEndereco2 = rowCON.CONEndereco2.Substring(3).Trim();
                    rowCON.Logradouro = "ALAMEDA";
                }
                else if (rowCON.CONEndereco2.ToUpper().StartsWith("AL "))
                {
                    rowCON.CONEndereco2 = rowCON.CONEndereco2.Substring(3).Trim();
                    rowCON.Logradouro = "ALAMEDA";
                }
                else if (rowCON.CONEndereco2.ToUpper().StartsWith("TRAVESSA"))
                {
                    rowCON.CONEndereco2 = rowCON.CONEndereco2.Substring(8).Trim();
                    rowCON.Logradouro = "TRAVESSA";
                }
                else if (rowCON.CONEndereco2.ToUpper().StartsWith("PRAÇA"))
                {
                    rowCON.CONEndereco2 = rowCON.CONEndereco2.Substring(5).Trim();
                    rowCON.Logradouro = "PRAÇA";
                }
                else
                    rowCON.Logradouro = "RUA";
            }
            foreach (defiscal.CondominioRow rowCON in apagar)
                rowCON.Delete();
            Condominio.AcceptChanges();
        }
    }
}
