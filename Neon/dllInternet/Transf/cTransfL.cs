﻿using System;
using Framework.ComponentesNeon;
using Framework.objetosNeon;

namespace Transf
{
    /// <summary>
    /// 
    /// </summary>
    public partial class cTransfL : CamposCondominio
    {
        private TransfL transfL;

        dTransfL _dTransfL
        {
            get { return transfL.DTransfL; }
        }

         #region Construtores
        /// <summary>
        /// Contrutor Padrão
        /// </summary>
        public cTransfL()
        {
            InitializeComponent();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="_transfL"></param>
        public cTransfL(TransfL _transfL)
        {
            transfL = _transfL;
            //_dTransfF = transfF.DTransfF;
            InitializeComponent();            
            prepara();
        }        

        #endregion

        bool calculotravado = true;

        private void prepara()
        {
            if (transfL.rowTRL != null)
            {
                lookUpEdit1.EditValue = transfL.rowTRL.TRLDe_CTL;
                lookUpEdit2.EditValue = transfL.rowTRL.TRLPara_CTL;
                calcEdit1.Value = -transfL.rowTRL.TRLValor;
                dateEdit1.DateTime = transfL.rowTRL.TRLData;
                memoEdit1.EditValue = transfL.rowTRL.TRLJustificativa;
            }
            else
            {
                dateEdit1.DateTime = DateTime.Today;
                if (transfL.DataMax.HasValue && transfL.DataMin.HasValue)
                    if ((dateEdit1.DateTime < transfL.DataMin) || (dateEdit1.DateTime > transfL.DataMax))
                        dateEdit1.DateTime = transfL.DataMin.Value;
            }            
            conTasLogicasBindingSource.DataSource = _dTransfL;
            if (transfL.DataMin.HasValue)
                dateEdit1.Properties.MinValue = transfL.DataMin.Value;
            if (transfL.DataMax.HasValue)
                dateEdit1.Properties.MaxValue = transfL.DataMax.Value;
            calculotravado = false;
            ValidaTudo();
        }

        bool reentrada = false;

        private bool ValidaTudo()
        {
            if (calculotravado || reentrada)
                return true;
            try
            {
                reentrada = true;
                bool Valido = true;               
                dateEdit1.ErrorText = lookUpEdit1.ErrorText = lookUpEdit2.ErrorText = calcEdit1.ErrorText = "";
                
                if (calcEdit1.Value == 0)
                {
                    Valido = false;
                    calcEdit1.ErrorText = string.Format("Valor inválido");
                }
                



                if (lookUpEdit1.EditValue == null)
                {
                    Valido = false;
                    lookUpEdit1.ErrorText = "Conta origem não definida";
                }

                if (lookUpEdit2.EditValue == null)
                {
                    Valido = false;
                    lookUpEdit2.ErrorText = "Conta destino não definida";
                }

                if ((lookUpEdit1.EditValue != null) && (lookUpEdit2.EditValue != null) && ((int)lookUpEdit1.EditValue == (int)lookUpEdit2.EditValue))
                {
                    Valido = false;
                    lookUpEdit2.ErrorText = "A conta origem e destino são iguais";
                }

                /*
                if (!dateEdit1.Properties.ReadOnly)
                    if (transfF.Status == TRFStatus.cadastrando)
                    {
                        if ((dateEdit1.DateTime == null) || (dateEdit1.DateTime < DateTime.Today.AddDays(-1)) || (dateEdit1.DateTime > DateTime.Today.AddDays(60)))
                        {
                            Valido = false;
                            dateEdit1.ErrorText = "Data inválida";
                        }
                    }*/

                

                btnConfirmar_F.Enabled = Valido;
                return Valido;
            }
            finally
            {
                reentrada = false;
            }
        }

        private void lookUpEdit1_EditValueChanged(object sender, EventArgs e)
        {
            ValidaTudo();
        }

        private void lookUpEdit2_EditValueChanged(object sender, EventArgs e)
        {
            ValidaTudo();
        }

        private void calcEdit1_EditValueChanged(object sender, EventArgs e)
        {
            ValidaTudo();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public override bool Update_F()
        {
            if (base.Update_F())
            {
                if (_dTransfL.TRansferenciaLogica.Count == 0)
                    return transfL.Criar(dateEdit1.DateTime.Date, (int)lookUpEdit1.EditValue,
                                         (int)lookUpEdit2.EditValue, 
                                         new Competencia(dateEdit1.DateTime, transfL.CON), 
                                         memoEdit1.Text,
                                         -calcEdit1.Value);
                else
                {
                    transfL.rowTRL.TRLDe_CTL = (int)lookUpEdit1.EditValue;
                    transfL.rowTRL.TRLPara_CTL = (int)lookUpEdit2.EditValue;
                    transfL.rowTRL.TRLValor = -calcEdit1.Value;
                    transfL.rowTRL.TRLData = dateEdit1.DateTime;
                    transfL.DTransfL.TRansferenciaLogicaTableAdapter.Update(transfL.rowTRL);
                    transfL.rowTRL.AcceptChanges();
                    return true;
                    //return Editar();
                }
            }
            else
                return false;
        }
    }
}
