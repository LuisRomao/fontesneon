﻿using System;
using System.Collections.Generic;
using System.Text;
using Framework.objetosNeon;
using VirMSSQL;
//using dllCheques;
//using Boletos.Boleto;

namespace Transf
{
    /// <summary>
    /// 
    /// </summary>
    public class TransfL
    {   
        /// <summary>
        /// 
        /// </summary>
        public enum TpChave 
        { 
            /// <summary>
            /// 
            /// </summary>
            CON 
        }

        /// <summary>
        /// 
        /// </summary>
        public DateTime? DataMin;

        /// <summary>
        /// 
        /// </summary>
        public DateTime? DataMax;

        /// <summary>
        /// 
        /// </summary>
        public dTransfL.TRansferenciaLogicaRow rowTRL
        {
            get
            {
                if (DTransfL.TRansferenciaLogica.Count > 0)
                    return DTransfL.TRansferenciaLogica[0];
                else
                    return null;
            }
        }

        //private dTransfL.ConTasLogicasRow rowCTLde;
        //private dTransfL.ConTasLogicasRow rowCTLpara;

        private dTransfL _dTransfL;

        /// <summary>
        /// Dataset interno
        /// </summary>
        public dTransfL DTransfL
        {
            get
            {
                if (_dTransfL == null)
                    _dTransfL = new dTransfL();
                return _dTransfL;
            }
            set { _dTransfL = value; }
        }
        

        private int? _CON;

        /// <summary>
        /// 
        /// </summary>
        public int CON
        {
            get
            {
                if (rowTRL != null)
                    return rowTRL.TRL_CON;
                else
                    return _CON.Value;
            }
        }

        private void FillInicial()
        {            
            DTransfL.ConTasLogicasTableAdapter.Fill(DTransfL.ConTasLogicas, CON);
        }

        

        
        
        #region Cosntrutor

        /// <summary>
        /// 
        /// </summary>
        /// <param name="TRL"></param>
        public TransfL(int TRL)
        {
            if (DTransfL.TRansferenciaLogicaTableAdapter.FillByTRL(DTransfL.TRansferenciaLogica, TRL) > 0)
            {               
                FillInicial();                
            }            
        }

        

        /// <summary>
        /// Construtor
        /// </summary>
        /// <param name="TpChave1">Tipo de chave</param>
        /// <param name="Chave">Chave</param>
        public TransfL(TpChave TpChave1, int Chave)
        {
            
            switch (TpChave1)
            {
                case TpChave.CON:
                    _CON = Chave;
                    FillInicial();
                    
                    break;                
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="CON"></param>
        /// <param name="De"></param>
        /// <param name="Ate"></param>
        /// <returns></returns>
        public static SortedList<int,TransfL> Transf_Datas(int CON,DateTime De, DateTime Ate)
        {
            SortedList<int, TransfL> retorno = new SortedList<int, TransfL>();
            System.Data.DataTable DT = TableAdapter.ST().BuscaSQLTabela("SELECT TRL FROM TRansferenciaLogica WHERE (TRL_CON = @P1) and (TRLData BETWEEN @P2 AND @P3)", CON, De, Ate);
            foreach (System.Data.DataRow DR in DT.Rows)
                retorno.Add((int)DR[0], new TransfL((int)DR[0]));
            return retorno;
        }

        #endregion

        /// <summary>
        /// 
        /// </summary>
        public void Salvar()
        {
            if (rowTRL.RowState == System.Data.DataRowState.Modified)
            {
                if (rowTRL.TRLValor != 0)
                {
                    DTransfL.TRansferenciaLogicaTableAdapter.Update(rowTRL);
                    rowTRL.AcceptChanges();
                }
                else
                {
                    rowTRL.Delete();
                    DTransfL.TRansferenciaLogicaTableAdapter.Update(rowTRL);
                }
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="Data"></param>
        /// <param name="TRLDe_CTL"></param>
        /// <param name="TRLPara_CTL"></param>
        /// <param name="Competencia1"></param>
        /// <param name="Justificativa"></param>
        /// <param name="Valor"></param>
        /// <returns></returns>
        public bool Criar(DateTime Data,int TRLDe_CTL,int TRLPara_CTL, Competencia Competencia1, string Justificativa,decimal Valor)
        {
            try 
            {
                VirMSSQL.TableAdapter.AbreTrasacaoSQL("Transf TransfL - 133",DTransfL.TRansferenciaLogicaTableAdapter);
                dTransfL.TRansferenciaLogicaRow novaTRL = DTransfL.TRansferenciaLogica.NewTRansferenciaLogicaRow();
                novaTRL.TRL_CON = CON;
                //novaTRL.TRL_DOC = 0;
                novaTRL.TRLI_USU = Framework.usuarioLogado.UsuarioLogadoNeon.UsuarioLogadoSt.USU;
                novaTRL.TRLData = Data;
                novaTRL.TRLDataI = DateTime.Now;
                novaTRL.TRLCompetenica = Competencia1.CompetenciaBind;
                novaTRL.TRLDe_CTL = TRLDe_CTL;
                novaTRL.TRLJustificativa = Justificativa;
                novaTRL.TRLPara_CTL = TRLPara_CTL;
                novaTRL.TRLStatus = 0;
                novaTRL.TRLValor = Valor;
                DTransfL.TRansferenciaLogica.AddTRansferenciaLogicaRow(novaTRL);
                DTransfL.TRansferenciaLogicaTableAdapter.Update(novaTRL);
                novaTRL.AcceptChanges();                                                                             
                VirMSSQL.TableAdapter.ST().Commit();
                return true;
            }
            catch (Exception e)
            {
                TableAdapter.ST().Vircatch(e);
                return false;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public System.Windows.Forms.DialogResult Abrir()
        {
            cTransfL cTransfL = new cTransfL(this);
            return cTransfL.ShowEmPopUp();
        }

    }
}