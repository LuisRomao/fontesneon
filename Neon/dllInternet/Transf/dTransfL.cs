﻿namespace Transf {
    
    
    public partial class dTransfL 
    {
        
        

        private dTransfLTableAdapters.ConTasLogicasTableAdapter conTasLogicasTableAdapter;
        /// <summary>
        /// TableAdapter padrão para tabela: ConTasLogicas
        /// </summary>
        public dTransfLTableAdapters.ConTasLogicasTableAdapter ConTasLogicasTableAdapter
        {
            get
            {
                if (conTasLogicasTableAdapter == null)
                {
                    conTasLogicasTableAdapter = new dTransfLTableAdapters.ConTasLogicasTableAdapter();
                    conTasLogicasTableAdapter.TrocarStringDeConexao();
                };
                return conTasLogicasTableAdapter;
            }
        }        

        private dTransfLTableAdapters.TRansferenciaLogicaTableAdapter tRansferenciaLogicaTableAdapter;
        /// <summary>
        /// TableAdapter padrão para tabela: TRansferenciaLogica
        /// </summary>
        public dTransfLTableAdapters.TRansferenciaLogicaTableAdapter TRansferenciaLogicaTableAdapter
        {
            get
            {
                if (tRansferenciaLogicaTableAdapter == null)
                {
                    tRansferenciaLogicaTableAdapter = new dTransfLTableAdapters.TRansferenciaLogicaTableAdapter();
                    tRansferenciaLogicaTableAdapter.TrocarStringDeConexao();
                };
                return tRansferenciaLogicaTableAdapter;
            }
        }
    }
}
