﻿using System;
using Boletos.Boleto;
using dllCheques;
using Framework.objetosNeon;
using VirMSSQL;

namespace Transf
{
    /// <summary>
    /// 
    /// </summary>
    public class TransfF
    {
        /// <summary>
        /// Tipo de chave
        /// </summary>
        public enum TpChave 
        {
            /// <summary>
            /// 
            /// </summary>
            CON,
            /// <summary>
            /// 
            /// </summary>
            NOA,
            /// <summary>
            /// 
            /// </summary>
            PAG 
        }

        private TRFStatus _Status = TRFStatus.naoencontrada;

        /// <summary>
        /// 
        /// </summary>
        public dTransfF.TRansferenciaFisicaRow rowTRF 
        {
            get
            {
                if (DTransfF.TRansferenciaFisica.Count > 0)
                    return DTransfF.TRansferenciaFisica[0];
                else
                    return null;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        public Cheque Cheque;

        /// <summary>
        /// 
        /// </summary>
        public Boleto Boleto;

        /// <summary>
        /// 
        /// </summary>
        public TRFStatus Status
        {
            get 
            {               
                return _Status;
            }
        }

        private TRFStatus VerificaStatus()
        {
            _Status = TRFStatus.naoencontrada;
            if (rowTRF != null)
            {
                bool CHEconc = !Cheque.CHErow.IsCHE_CCDNull();
                bool BOLconc = !Boleto.rowPrincipal.IsBOL_CCDNull();
                if (CHEconc && BOLconc)
                    _Status = TRFStatus.conciliada;
                else
                    if (CHEconc)
                        _Status = TRFStatus.comPAGconc;
                    else
                        _Status = BOLconc ? TRFStatus.comBODconc : TRFStatus.cadastrada;
                if ((int)_Status != rowTRF.TRFStatus)
                {
                    DTransfF.TRansferenciaFisicaTableAdapter.EmbarcaEmTransST();
                    rowTRF.TRFStatus = (int)_Status;
                    DTransfF.TRansferenciaFisicaTableAdapter.Update(rowTRF);
                    rowTRF.AcceptChanges();
                }
            }
            else
                if (_CON.HasValue)
                    _Status = TRFStatus.cadastrando;

            return Status;
        }

        private dTransfF _dTransfF;

        /// <summary>
        /// Dataset interno
        /// </summary>
        public dTransfF DTransfF
        {
            get 
            {
                if (_dTransfF == null)
                    _dTransfF = new dTransfF();
                return _dTransfF; 
            }
            set { _dTransfF = value; }
        }

        /// <summary>
        /// Indica se a tranferencia foi encontrada
        /// </summary>
        

        private int? _CON;

        /// <summary>
        /// 
        /// </summary>
        public int CON
        {
            get 
            {
                if (rowTRF != null)
                    return rowTRF.TRF_CON;
                else
                    return _CON.Value;
            }
        }

        private void FillInicial()
        {
            DTransfF.ContaCorrenTeTableAdapter.Fill(DTransfF.ContaCorrenTe, CON);
            //DTransfF.ConTasLogicasTableAdapter.Fill(DTransfF.ConTasLogicas, CON);
        }

        private int CTLdoCaixa()
        {
            //foreach (dTransfF.ConTasLogicasRow rowCTL in DTransfF.ConTasLogicas)            
              //  if((Framework.CTLTipo)rowCTL.CTLTipo == Framework.CTLTipo.Caixa)
                //    return rowCTL.CTL;
            return 0;
        }


        private void FillCompementar(int NOA)
        {/*
            //if (rowTRF != null)
            //{
                DTransfF.NOtAsTableAdapter.FillByNOA(DTransfF.NOtAs, NOA);
                DTransfF.PAGamentosTableAdapter.FillByNOA(DTransfF.PAGamentos, NOA);
                foreach (dTransfF.PAGamentosRow rowPAG in DTransfF.PAGamentos)
                {
                    int CTL = rowPAG.IsPAG_CTLNull() ? CTLdoCaixa() : rowPAG.PAG_CTL;
                    dTransfF.ConTasLogicasRow rowCTL = DTransfF.ConTasLogicas.FindByCTL(CTL);
                    rowCTL.Valor = rowPAG.PAGValor;
                    rowCTL.PAG = rowPAG.PAG;
                }
                DTransfF.BODxPAGTableAdapter.FillByNOA(DTransfF.BODxPAG, NOA);
                //DTransfF.BOletoDetalheTableAdapter.FillByNOA(DTransfF.BOletoDetalhe, NOA);
                Cheque = new Cheque(DTransfF.PAGamentos[0].PAG_CHE);
                //if (DTransfF.BOletoDetalhe.Count > 0)
                  //  Boleto = new Boleto(DTransfF.BOletoDetalhe[0].BOD_BOL);
            //}*/
        }
        
        #region Cosntrutor

        /// <summary>
        /// 
        /// </summary>
        /// <param name="TRF"></param>
        public TransfF(int TRF)
        {
            if (DTransfF.TRansferenciaFisicaTableAdapter.FillByTRF(DTransfF.TRansferenciaFisica, TRF) > 0)
            {
                FillInicial();
                FillCompementar(rowTRF.TRF_NOA);
            }
            VerificaStatus();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="CCT"></param>
        /// <param name="CON"></param>
        /// <param name="Valor"></param>
        public TransfF(int CCT, int CON,decimal Valor)
        {
            if (DTransfF.TRansferenciaFisicaTableAdapter.FillByCredito(DTransfF.TRansferenciaFisica, CCT, CON, Valor) > 0)
            {
                FillInicial();
                FillCompementar(rowTRF.TRF_NOA);
            }
            else
                _CON = CON;
            VerificaStatus();
        }

        /// <summary>
        /// Construtor
        /// </summary>
        /// <param name="TpChave1">Tipo de chave</param>
        /// <param name="Chave">Chave</param>
        public TransfF(TpChave TpChave1, int Chave)
        {
            
            switch (TpChave1)
            {
                case TpChave.CON:
                    _CON = Chave;
                    FillInicial();                    
                    VerificaStatus();
                    break;
                case TpChave.PAG:
                    
                    if (DTransfF.TRansferenciaFisicaTableAdapter.FillByPAG(DTransfF.TRansferenciaFisica, Chave) == 1)
                    {
                        
                        FillInicial();
                        FillCompementar(rowTRF.TRF_NOA);
                    }
                    else
                    {
                        
                        string comandoOrig =
"SELECT     NOtAs.NOA, NOtAs.NOA_CON, PAGamentos.PAGVencimento\r\n" + 
"FROM         PAGamentos INNER JOIN\r\n" + 
"                      NOtAs ON PAGamentos.PAG_NOA = NOtAs.NOA\r\n" + 
"WHERE     (PAGamentos.PAG = @P1);";
                        System.Data.DataRow DR = TableAdapter.ST().BuscaSQLRow(comandoOrig, Chave);
                        _CON = (int)DR["NOA_CON"];
                        int NOA = (int)DR["NOA"];
                        DateTime Data = (DateTime)DR["PAGVencimento"];
                        //decimal Valor;
                        FillInicial();
                        FillCompementar(NOA);                        
                        Competencia Comp = new Competencia(Data,_CON.Value);
                        Criar(DTransfF.NOtAs[0].NOA_PLA, Comp, NOA, DTransfF.NOtAs[0].NOATotal, Data, Cheque.CHErow.CHE_CCT, Cheque.CHErow.CHE_CCT, "Tranferência entre contas");
                    }
                    VerificaStatus();
                    break;
            }
        } 

        #endregion

        /// <summary>
        /// 
        /// </summary>
        /// <param name="CCD"></param>
        public void AmarraCredito(int CCD)
        {
            try
            {
                VirMSSQL.TableAdapter.AbreTrasacaoSQL("TranfL - 218");
                Boleto.rowPrincipal.BOL_CCD = CCD;
                //Boleto.rowPrincipal.BOLPagamento 
                Boleto.GravaJustificativa("Tranferencia entre contas", "");
                TableAdapter.ST().ExecutarSQLNonQuery("UPDATE ContaCorrenteDetalhe SET CCDTipoLancamento = 9 WHERE (CCD = @P1)", CCD);
                VirMSSQL.TableAdapter.ST().Commit();
            }
            catch (Exception e) 
            {
                TableAdapter.ST().Vircatch(e);
                throw e;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="CCD"></param>
        public void AmarraDebito(int CCD)
        {
            try
            {
                VirMSSQL.TableAdapter.AbreTrasacaoSQL("Transf - 236");
                Cheque.AmarrarCCD(CCD);                                
                VirMSSQL.TableAdapter.ST().Commit();
            }
            catch (Exception e)
            {
                TableAdapter.ST().Vircatch(e);
                throw e;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="PLAO"></param>
        /// <param name="Competencia1"></param>
        /// <param name="ValorTotal"></param>
        /// <param name="DataTransferencia"></param>
        /// <param name="De_CCT"></param>
        /// <param name="Para_CCT"></param>
        /// <param name="TRFDescricao"></param>
        /// <returns></returns>
        public bool Criar(string PLAO, Competencia Competencia1, decimal ValorTotal, DateTime DataTransferencia, int De_CCT, int Para_CCT, string TRFDescricao)
        {
            return Criar(PLAO, Competencia1, null, ValorTotal, DataTransferencia, De_CCT, Para_CCT, TRFDescricao);
        }

        private bool Criar(string PLAO, Competencia Competencia1, int? NOA, decimal ValorTotal, DateTime DataTransf, int De_CCT, int Para_CCT, string TRFDescricao)
        {     
            /*
            TRFStatus Status = TRFStatus.cadastrada;
            string PLAD = string.Format("{0}{1}", (PLAO.Substring(0, 1) == "5") ? "6" : "5", PLAO.Substring(1));
            int? SPLO = TableAdapter.ST().BuscaEscalar_int("SELECT SPL FROM SubPLano where SPL_PLA = @P1", PLAO);            
            Framework.datasets.dCondominiosAtivos.CONDOMINIOSRow rowCON = Framework.datasets.dCondominiosAtivos.dCondominiosTodosST.CONDOMINIOS.FindByCON(CON);
            if (rowCON == null)
                return false;
            try
            {
                
                TableAdapter.ST().AbreTrasacaoLocal(DTransfF.TRansferenciaFisicaTableAdapter,
                                                                       DTransfF.NOtAsTableAdapter,
                                                                       DTransfF.PAGamentosTableAdapter);
                if (!SPLO.HasValue)
                {
                    Framework.datasets.dPLAnocontas.dPLAnocontasSt56.SubPLanoTableAdapter.EmbarcaEmTransST();
                    Framework.datasets.dPLAnocontas.SubPLanoRow rowSPL = Framework.datasets.dPLAnocontas.dPLAnocontasSt56.SubPLano.NewSubPLanoRow();
                    rowSPL.SPL_PLA = PLAO;
                    rowSPL.SPLDescricao = "Transferência entre contas";
                    rowSPL.SPLCodigo = "0";
                    rowSPL.SPLServico = true;
                    rowSPL.SPLNoCondominio = true;
                    rowSPL.SPLReterISS = false;
                    rowSPL.SPLValorISS = 2;                
                    Framework.datasets.dPLAnocontas.dPLAnocontasSt56.SubPLano.AddSubPLanoRow(rowSPL);
                    Framework.datasets.dPLAnocontas.dPLAnocontasSt56.SubPLanoTableAdapter.Update(rowSPL);
                    rowSPL.AcceptChanges();
                    SPLO = rowSPL.SPL;
                }
                if (!NOA.HasValue)
                {
                    dTransfF.NOtAsRow novaNOA = _dTransfF.NOtAs.NewNOtAsRow();
                    novaNOA.NOA_CON = CON;
                    novaNOA.NOA_PLA = PLAO;
                    novaNOA.NOA_SPL = SPLO.Value;
                    novaNOA.NOAAguardaNota = false;
                    novaNOA.NOACompet = Competencia1.CompetenciaBind;
                    /*Corrigir
                    novaNOA.NOADadosPag = "??";
                    novaNOA.NOADataEmissao = DateTime.Today;
                    novaNOA.NOADATAI = DateTime.Now;
                    novaNOA.NOADefaultPAGTipo = (int)dCheque.PAGTipo.cheque;
                    novaNOA.NOAI_USU = Framework.usuarioLogado.UsuarioLogadoNeon.UsuarioLogadoStNEON.USU;
                    novaNOA.NOANumero = 0;
                    novaNOA.NOAObs = "Transferência entre contas";
                    novaNOA.NOAServico = "Transferência entre contas";
                    novaNOA.NOAStatus = (int)ContaPagar.dNOtAs.NOAStatus.Cadastrada;
                    novaNOA.NOATipo = (int)ContaPagar.dNOtAs.NOATipo.TransferenciaF;
                    novaNOA.NOATotal = ValorTotal;
                    _dTransfF.NOtAs.AddNOtAsRow(novaNOA);
                    _dTransfF.NOtAsTableAdapter.Update(novaNOA);
                    novaNOA.AcceptChanges();
                    NOA = novaNOA.NOA;
                }
                //else
                //    _Status = TRFStatus.comPAGconc;
                dTransfF.TRansferenciaFisicaRow novarow = _dTransfF.TRansferenciaFisica.NewTRansferenciaFisicaRow();
                novarow.TRF_CON = CON;
                novarow.TRF_NOA = NOA.Value;
                novarow.TRFData = DataTransf;
                novarow.TRFDATAI = DateTime.Now;
                novarow.TRFDe_CCT = De_CCT;
                novarow.TRFPara_CCT = Para_CCT;
                novarow.TRFDescricao = TRFDescricao;
                novarow.TRFI_USU = Framework.usuarioLogado.UsuarioLogadoNeon.UsuarioLogadoStNEON.USU;
                novarow.TRFValor = ValorTotal;
                novarow.TRFStatus = (int)Status;
                DTransfF.TRansferenciaFisica.AddTRansferenciaFisicaRow(novarow);
                DTransfF.TRansferenciaFisicaTableAdapter.Update(novarow);
                novarow.AcceptChanges();
                Boleto = new Boleto(CON, "", Competencia1, DataTransf, BOLTipoCRAI.Transferencia, StatusBoleto.Valido, rowCON.CONNome);
                foreach (dTransfF.ConTasLogicasRow rowCTL in DTransfF.ConTasLogicas)
                    if ((!rowCTL.IsValorNull()) && (rowCTL.Valor != 0))
                    {
                        dTransfF.PAGamentosRow novaPAG;
                        if (rowCTL.IsPAGNull())
                        {
                            novaPAG = DTransfF.PAGamentos.NewPAGamentosRow();
                            novaPAG.PAG_CTL = rowCTL.CTL;
                            novaPAG.PAG_NOA = NOA.Value;
                            novaPAG.PAGDATAI = DateTime.Now;
                            novaPAG.PAGI_USU = Framework.usuarioLogado.UsuarioLogadoNeon.UsuarioLogadoStNEON.USU;
                            novaPAG.PAGTipo = (int)dCheque.PAGTipo.cheque;
                            novaPAG.PAGValor = rowCTL.Valor;
                            novaPAG.PAGVencimento = DataTransf;
                            _dTransfF.PAGamentos.AddPAGamentosRow(novaPAG);
                            _dTransfF.PAGamentosTableAdapter.Update(novaPAG);
                            novaPAG.AcceptChanges();
                            rowCTL.PAG = novaPAG.PAG;
                            //colocar no cheque
                            Cheque = new Cheque(DataTransf, rowCON.CONNome, CON, (dCheque.PAGTipo)novaPAG.PAGTipo, false, 0);                            
                            novaPAG.PAG_CHE = Cheque.CHErow.CHE;
                            _dTransfF.PAGamentosTableAdapter.Update(novaPAG);
                            novaPAG.AcceptChanges();
                            Cheque.AjustarTotalPelasParcelas();
                        }
                        else
                        {
                            DTransfF.PAGamentosTableAdapter.FillByPAG(DTransfF.PAGamentos,rowCTL.PAG);
                            novaPAG = DTransfF.PAGamentos[0];
                            //Cheque = new Cheque(novaPAG.PAG_CHE,dllCheques.Cheque.TipoChave.CHE);
                        }
                        if (DTransfF.BODxPAGTableAdapter.FillByPAG(DTransfF.BODxPAG,novaPAG.PAG) == 0)
                            Boleto.IncluiBOD(PLAD, "Transferência entre contas", rowCTL.Valor, novaPAG.PAG,rowCTL.CTL);                        
                    }

                VerificaStatus();
                VirMSSQL.TableAdapter.ST().Commit();
            }
            catch (Exception e)
            {
                TableAdapter.ST().Vircatch(e);
                throw e;
            }*/
            return true;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public System.Windows.Forms.DialogResult Abrir()
        {
            cTransfF cTransfF = new cTransfF(this);
            return cTransfF.ShowEmPopUp();
        }

    }
}
