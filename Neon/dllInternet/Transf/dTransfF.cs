﻿namespace Transf {
    
    
    public partial class dTransfF 
    {
        private dTransfFTableAdapters.ContaCorrenTeTableAdapter contaCorrenTeTableAdapter;
        /// <summary>
        /// TableAdapter padrão para tabela: ContaCorrenTe
        /// </summary>
        public dTransfFTableAdapters.ContaCorrenTeTableAdapter ContaCorrenTeTableAdapter
        {
            get
            {
                if (contaCorrenTeTableAdapter == null)
                {
                    contaCorrenTeTableAdapter = new dTransfFTableAdapters.ContaCorrenTeTableAdapter();
                    contaCorrenTeTableAdapter.TrocarStringDeConexao();
                };
                return contaCorrenTeTableAdapter;
            }
        }

        

        private dTransfFTableAdapters.TRansferenciaFisicaTableAdapter tRansferenciaFisicaTableAdapter;
        /// <summary>
        /// TableAdapter padrão para tabela: TRansferenciaFisica
        /// </summary>
        public dTransfFTableAdapters.TRansferenciaFisicaTableAdapter TRansferenciaFisicaTableAdapter
        {
            get
            {
                if (tRansferenciaFisicaTableAdapter == null)
                {
                    tRansferenciaFisicaTableAdapter = new dTransfFTableAdapters.TRansferenciaFisicaTableAdapter();
                    tRansferenciaFisicaTableAdapter.TrocarStringDeConexao();
                };
                return tRansferenciaFisicaTableAdapter;
            }
        }

        private dTransfFTableAdapters.NOtAsTableAdapter nOtAsTableAdapter;
        /// <summary>
        /// TableAdapter padrão para tabela: NOtAs
        /// </summary>
        public dTransfFTableAdapters.NOtAsTableAdapter NOtAsTableAdapter
        {
            get
            {
                if (nOtAsTableAdapter == null)
                {
                    nOtAsTableAdapter = new dTransfFTableAdapters.NOtAsTableAdapter();
                    nOtAsTableAdapter.TrocarStringDeConexao();
                };
                return nOtAsTableAdapter;
            }
        }

        private dTransfFTableAdapters.PAGamentosTableAdapter pAGamentosTableAdapter;
        /// <summary>
        /// TableAdapter padrão para tabela: PAGamentos
        /// </summary>
        public dTransfFTableAdapters.PAGamentosTableAdapter PAGamentosTableAdapter
        {
            get
            {
                if (pAGamentosTableAdapter == null)
                {
                    pAGamentosTableAdapter = new dTransfFTableAdapters.PAGamentosTableAdapter();
                    pAGamentosTableAdapter.TrocarStringDeConexao();
                };
                return pAGamentosTableAdapter;
            }
        }


        
        private dTransfFTableAdapters.BODxPAGTableAdapter bODxPAGTableAdapter;
        /// <summary>
        /// TableAdapter padrão para tabela: BODxPAG
        /// </summary>
        public dTransfFTableAdapters.BODxPAGTableAdapter BODxPAGTableAdapter
        {
            get
            {
                if (bODxPAGTableAdapter == null)
                {
                    bODxPAGTableAdapter = new dTransfFTableAdapters.BODxPAGTableAdapter();
                    bODxPAGTableAdapter.TrocarStringDeConexao();
                };
                return bODxPAGTableAdapter;
            }
        }

        

    }
}
