﻿using dllVirEnum;
using System;

namespace Transf
{

    /// <summary>
    /// Enumeração para campo 
    /// </summary>
    public enum TRFStatus
    {
        /// <summary>
        /// 
        /// </summary>
        naoencontrada = -1,
        /// <summary>
        /// 
        /// </summary>
        cadastrando = 0,
        /// <summary>
        /// 
        /// </summary>
        cadastrada = 1,
        /// <summary>
        /// 
        /// </summary>
        comPAGconc = 2,
        /// <summary>
        /// 
        /// </summary>
        comBODconc = 3,
        /// <summary>
        /// 
        /// </summary>
        conciliada = 4
    }

    /// <summary>
    /// virEnum para campo 
    /// </summary>
    public class virEnumTRFStatus : dllVirEnum.VirEnum
    {
        /// <summary>
        /// Construtor padrao
        /// </summary>
        /// <param name="Tipo">Tipo</param>
        public virEnumTRFStatus(Type Tipo) :
            base(Tipo)
        {
        }

        private static virEnumTRFStatus _virEnumTRFStatusSt;

        /// <summary>
        /// VirEnum estático para campo TRFStatus
        /// </summary>
        public static virEnumTRFStatus virEnumTRFStatusSt
        {
            get
            {
                if (_virEnumTRFStatusSt == null)
                {
                    _virEnumTRFStatusSt = new virEnumTRFStatus(typeof(TRFStatus));
                    _virEnumTRFStatusSt.GravaNomes(TRFStatus.naoencontrada, "Nao encontrada");
                    _virEnumTRFStatusSt.GravaNomes(TRFStatus.cadastrada, "Cadastrada");
                    _virEnumTRFStatusSt.GravaNomes(TRFStatus.comPAGconc, "Somente débito conciliado");
                    _virEnumTRFStatusSt.GravaNomes(TRFStatus.comBODconc, "Somente crédito conciliado");
                    _virEnumTRFStatusSt.GravaNomes(TRFStatus.conciliada, "Conciliada");
                }
                return _virEnumTRFStatusSt;
            }
        }
    }
}