﻿using System;
using System.ComponentModel;
using Framework.ComponentesNeon;
using Framework.objetosNeon;


namespace Transf
{
    /// <summary>
    /// 
    /// </summary>
    public partial class cTransfF : CamposCondominio
    {
        private TransfF transfF;
        private dTransfF _dTransfF;
        //private int? _CON;
        private malote malote1;

        #region Construtores
        /// <summary>
        /// Contrutor Padrão
        /// </summary>
        public cTransfF()
        {
            InitializeComponent();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="_transfF"></param>
        public cTransfF(TransfF _transfF)
        {
            transfF = _transfF;
            _dTransfF = transfF.DTransfF;
            InitializeComponent();
            virEnumTRFStatus.virEnumTRFStatusSt.CarregaEditorDaGrid(ComboStatus);
            prepara();
        } 
        #endregion

        private void prepara()
        {
            if (transfF.Status != TRFStatus.cadastrando) 
            {
                lookUpEdit1.EditValue = transfF.rowTRF.TRFDe_CCT;
                lookUpEdit2.EditValue = transfF.rowTRF.TRFPara_CCT;
                lookUpEdit3.EditValue = _dTransfF.NOtAs[0].NOA_PLA;
                calcEdit1.Value = transfF.rowTRF.TRFValor;
                dateEdit1.DateTime = transfF.rowTRF.TRFData;
                memoEdit1.EditValue = transfF.rowTRF.TRFDescricao;
            }
            else
            {              
                
                malote1 = new malote(transfF.CON, DateTime.Now);
                while (malote1.DataEnvio < DateTime.Now)
                    malote1++;
                dateEdit1.DateTime = malote1.DataRetorno;
            }            
            contaCorrenTeBindingSource.DataSource = _dTransfF;
            //conTasLogicasBindingSource.DataSource = _dTransfF;
            dPLAnocontasBindingSource.DataSource = Framework.datasets.dPLAnocontas.dPLAnocontasSt56;            
            calculotravado = false;
            ValidaTudo();
            switch (transfF.Status)
            {
                case TRFStatus.naoencontrada:
                    break;
                case TRFStatus.cadastrando:
                    break;
                case TRFStatus.cadastrada:
                    break;
                case TRFStatus.comPAGconc:
                    lookUpEdit1.Properties.ReadOnly = true;
                    lookUpEdit3.Properties.ReadOnly = true;
                    calcEdit1.Properties.ReadOnly = true;
                    dateEdit1.Properties.ReadOnly = true;
                    break;
                case TRFStatus.comBODconc:
                    lookUpEdit2.Properties.ReadOnly = true;
                    calcEdit1.Properties.ReadOnly = true;
                    dateEdit1.Properties.ReadOnly = true;
                    break;
                case TRFStatus.conciliada:
                    lookUpEdit1.Properties.ReadOnly = true;
                    lookUpEdit2.Properties.ReadOnly = true;
                    lookUpEdit3.Properties.ReadOnly = true;
                    calcEdit1.Properties.ReadOnly = true;
                    dateEdit1.Properties.ReadOnly = true;
                    break;
                default:
                    break;
            }
            ComboStatus.EditValue = (int)transfF.Status;
        }

        private bool calculotravado = true;

        private void lookUpEdit1_EditValueChanged(object sender, EventArgs e)
        {
            ValidaTudo();            
        }

        private void gridView1_RowUpdated(object sender, DevExpress.XtraGrid.Views.Base.RowObjectEventArgs e)
        {
            ValidaTudo();                        
        }

        bool reentrada = false;

        private bool ValidaTudo()
        {
            if (calculotravado || reentrada)
                return true;
            try
            {
                reentrada = true;
                bool Valido = true;
                decimal Total = 0;
                dateEdit1.ErrorText = lookUpEdit1.ErrorText = lookUpEdit2.ErrorText = lookUpEdit3.ErrorText = calcEdit1.ErrorText = "";
                //foreach (dTransfF.ConTasLogicasRow rowCTL in _dTransfF.ConTasLogicas)
                  //  if (!rowCTL.IsValorNull())
                    //    Total += rowCTL.Valor;
                if (calcEdit1.Value == 0)
                {
                    Valido = false;
                    calcEdit1.ErrorText = string.Format("Valor inválido");
                }
                else
                    if (Total != calcEdit1.Value)
                    {
                        Valido = false;
                        calcEdit1.ErrorText = string.Format("Valor total incorreto {0:n2} x {1:n2}", Total, calcEdit1.Value);
                    }



                if (lookUpEdit1.EditValue == null)
                {
                    Valido = false;
                    lookUpEdit1.ErrorText = "Conta origem não definida";
                }

                if (lookUpEdit2.EditValue == null)
                {
                    Valido = false;
                    lookUpEdit2.ErrorText = "Conta destino não definida";
                }

                if ((lookUpEdit1.EditValue != null) && (lookUpEdit2.EditValue != null) && ((int)lookUpEdit1.EditValue == (int)lookUpEdit2.EditValue))
                {
                    Valido = false;
                    lookUpEdit2.ErrorText = "A conta origem e destino são iguais";
                }

                if(!dateEdit1.Properties.ReadOnly)
                    if (transfF.Status == TRFStatus.cadastrando)
                    {
                        if ((dateEdit1.DateTime == null) || (dateEdit1.DateTime < DateTime.Today.AddDays(-1)) || (dateEdit1.DateTime > DateTime.Today.AddDays(60)))
                        {
                            Valido = false;
                            dateEdit1.ErrorText = "Data inválida";
                        }
                    }

                if (lookUpEdit3.EditValue == null)
                {
                    Valido = false;
                    lookUpEdit3.ErrorText = "Plano de contas não definido";
                }

                btnConfirmar_F.Enabled = Valido;
                return Valido;
            }
            finally 
            {
                reentrada = false;
            }
        }

        private void calcEdit1_EditValueChanged(object sender, EventArgs e)
        {
            ValidaTudo();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public override bool Update_F()
        {
            if (base.Update_F())
            {
                if (transfF.Status == TRFStatus.cadastrando)
                    return transfF.Criar((string)lookUpEdit3.EditValue, new Competencia(dateEdit1.DateTime, transfF.CON), calcEdit1.Value, dateEdit1.DateTime.Date,
                           (int)lookUpEdit1.EditValue, (int)lookUpEdit2.EditValue, memoEdit1.Text);
                else
                    return Editar();            
            }
            else
                return false;
        }

        private bool Editar()
        {
            /*
            if ((int)lookUpEdit1.EditValue != transfF.rowTRF.TRFDe_CCT)
            {
                if (!transfF.Cheque.Editavel())
                    return false;                
            }
            try
            {
                TableAdapter.ST().AbreTrasacaoLocal(_dTransfF.TRansferenciaFisicaTableAdapter,
                                                                       _dTransfF.NOtAsTableAdapter,
                                                                       _dTransfF.PAGamentosTableAdapter);
                if ((int)lookUpEdit1.EditValue != transfF.rowTRF.TRFDe_CCT)
                {                                                                              
                    transfF.Cheque.CHErow.CHE_CCT = transfF.rowTRF.TRFDe_CCT;
                    transfF.Cheque.dCheque.CHEquesTableAdapter.EmbarcaEmTransST();
                    transfF.Cheque.dCheque.CHEquesTableAdapter.Update(transfF.Cheque.CHErow);
                }

                if (
                    (calcEdit1.Value != transfF.rowTRF.TRFValor) 
                    ||
                    ((int)lookUpEdit2.EditValue != transfF.rowTRF.TRFPara_CCT)
                    ||
                    ((int)lookUpEdit1.EditValue != transfF.rowTRF.TRFDe_CCT)
                   )
                {
                    transfF.rowTRF.TRFPara_CCT = (int)lookUpEdit2.EditValue;
                    transfF.rowTRF.TRFValor = calcEdit1.Value;
                    transfF.rowTRF.TRFDe_CCT = (int)lookUpEdit1.EditValue;
                    transfF.DTransfF.TRansferenciaFisicaTableAdapter.Update(transfF.rowTRF);
                    transfF.rowTRF.AcceptChanges();                    
                }

                dTransfF.PAGamentosRow rowPAG;
                foreach (dTransfF.ConTasLogicasRow rowCTL in transfF.DTransfF.ConTasLogicas)
                {
                    if (!rowCTL.IsPAGNull()) 
                    {
                        rowPAG = transfF.DTransfF.PAGamentos.FindByPAG(rowCTL.PAG);
                        if (rowCTL.IsValorNull() || (rowCTL.Valor == 0))
                        {
                            transfF.Boleto.RemoverBOD(rowPAG.GetBODxPAGRows()[0].BOD);
                            rowPAG.Delete();                            
                        }
                        else
                        {
                            if (rowCTL.Valor != rowPAG.PAGValor)
                            {
                                rowPAG.PAGValor = rowCTL.Valor;
                                rowPAG.PAGDATAA = DateTime.Now;
                                rowPAG.PAGA_USU = Framework.usuarioLogado.UsuarioLogadoNeon.UsuarioLogadoStNEON.USU;
                                transfF.Boleto.RemoverBOD(rowPAG.GetBODxPAGRows()[0].BOD);
                                string PLAO = (string)lookUpEdit3.EditValue;
                                string PLAD = string.Format("{0}{1}", (PLAO.Substring(0, 1) == "5") ? "6" : "5", PLAO.Substring(1));
                                transfF.Boleto.IncluiBOD(PLAD, "Transferência entre contas", rowCTL.Valor, rowPAG.PAG, rowCTL.CTL); 
                            }
                        }
                    }
                    else
                    {
                        if (!rowCTL.IsValorNull() && (rowCTL.Valor != 0))
                        {
                            rowPAG = transfF.DTransfF.PAGamentos.NewPAGamentosRow();
                            rowPAG.PAG_CTL = rowCTL.CTL;
                            rowPAG.PAG_NOA = transfF.rowTRF.TRF_NOA;
                            rowPAG.PAGDATAI = DateTime.Now;
                            rowPAG.PAGI_USU = Framework.usuarioLogado.UsuarioLogadoNeon.UsuarioLogadoStNEON.USU;
                            rowPAG.PAGTipo = (int)dllCheques.dCheque.PAGTipo.cheque;
                            rowPAG.PAGValor = rowCTL.Valor;
                            rowPAG.PAGVencimento = transfF.rowTRF.TRFData;
                            rowPAG.PAG_CHE = transfF.Cheque.CHErow.CHE;
                            _dTransfF.PAGamentos.AddPAGamentosRow(rowPAG);
                            transfF.DTransfF.PAGamentosTableAdapter.Update(rowPAG);
                            rowPAG.AcceptChanges();
                            string PLAO = (string)lookUpEdit3.EditValue;
                            string PLAD = string.Format("{0}{1}", (PLAO.Substring(0, 1) == "5") ? "6" : "5", PLAO.Substring(1));
                            transfF.Boleto.IncluiBOD(PLAD, "Transferência entre contas", rowCTL.Valor, rowPAG.PAG,rowCTL.CTL);                        
                            //colocar no cheque
                            
                            
                        }
                    }
                }
                transfF.DTransfF.PAGamentosTableAdapter.Update(transfF.DTransfF.PAGamentos);
                transfF.DTransfF.PAGamentos.AcceptChanges();
                if (transfF.Cheque.CHErow.CHEValor != transfF.rowTRF.TRFValor)
                    transfF.Cheque.AjustarTotalPelasParcelas();
                VirMSSQL.TableAdapter.ST().Commit();
            }
            catch (Exception e)
            {
                TableAdapter.ST().Vircatch(e);
                return false;
            }*/
            return true;
        }

        private void lookUpEdit1_Validating(object sender, CancelEventArgs e)
        {
            ValidaTudo();
        }

        

        
    }
}
