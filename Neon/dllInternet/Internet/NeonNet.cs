/*
LH - 29/04/2014       -            - Erro na exporta��o de condom�nio sem banco 13.2.8.37
LH - 06/05/2014       -            - Acordo via internet da erro 13.2.8.42
MR - 28/05/2014 12:30 -            - Publica��o de regras dos Equipamento (Altera��es indicadas por *** MRC - INICIO - RESERVA (28/05/2014 12:30) ***)
LH - 04/07/2014 10:55 - 13.2.9.24  - N�o exporta mais os boletos.
MR - 18/03/2015 10:00 -            - Tratamento adequado de EMP para tabelas de RESERVA, visando habilitar reservas em Santo Andr� (Altera��es indicadas por *** MRC - INICIO - RESERVA (18/03/2015 10:00) ***)
                                     Alterado o nome da funcao ReGrasEquipamento para PublicaReGrasEquipamento para manter padr�o de nomenclatura 
MR - 31/03/2015 21:30 -            - Tratamento adequado de EMP ao apagar dados das tabelas ReservaEQuipamento e ReGrasEquipamento, visando habilitar reservas em Santo Andr� (Altera��es indicadas por *** MRC - INICIO - RESERVA (31/05/2015 21:30) ***)
MR - 05/01/2016 13:30 -            - Atualiza��o da tabela de salario m�nimo apenas se valor do BC for maior que o configurado (Altera��es indicadas por *** MRC - INICIO (05/01/2016 13:30) ***)
                                     Desabilitado o envio e tratamento (apagar) dos ramos de atividades e do relacionamento FRNxRAV (n�o usado) para o site
MR - 14/01/2016 12:30 -            - Cobran�a de reserva por menor ou maior de condom�nio ao seta valor de boletor (Altera��es indicadas por *** MRC - INICIO - RESERVA (14/01/2016 12:30) ***)
MR - 21/03/2016 20:00 -            - Registro de boletos separado para Bradesco e Itau (Altera��es indicadas por *** MRC - INICIO (21/03/2016 20:00) ***)
MR - 13/05/2016 12:00 -            - Corre��o da classifica��o do boletos de reserva que estava fixo em 104000 e passa a ser o que est� configurado para o equipamento (Altera��es indicadas por *** MRC - INICIO (13/05/2016 12:00) ***)
MR - 27/05/2016 12:00 - 13.2.9.41  - Nova fun��o ProcessaReservas que processa as reservas e cancelamentos do site separadamente (anteriormente feito em ReservaEQuipamento) (Altera��es indicadas por *** MRC - INICIO (27/05/2016 12:00) ***)
                                     Corre��o da duplica��o de grade quando ocorre altera��o da hora de in�cio, n�o enviando nenhuma grade (nova ou alterada) se j� houve reserva para o dia
MR - 08/06/2016 11:00 -            - Corre��o da duplica��o de grade quando horario termino menor que horario incio indicand dia seguinte (Altera��es indicadas por *** MRC - INICIO (08/06/2016 11:00) ***)
MR - 08/08/2016 11:00 -            - Envio de grade com D+1 para disponibilizar imediatamante na virada (Altera��es indicadas por *** MRC - INICIO (08/08/2016 11:00) ***)
MR - 15/08/2016 14:00 -            - Gera��o de grade do tipo "meses completos" (Altera��es indicadas por *** MRC - INICIO (15/08/2016 14:00) - Pacote 2 ***)
*/

using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using Boletos.Rateios;
using CompontesBasicosProc;
using CompontesBasicos;
using Framework;
using VirMSSQL;
using ContaPagarProc.Follow;
using dllClienteServProc;
using System.Text;
using VirEnumeracoes;
using VirEnumeracoesNeon;

namespace Internet
{
    /// <summary>
    /// Objeto para publica��o internet
    /// </summary>
    public class NeonNet
    {


        private string strLocal = "";
        private int iTotal = 0;
        private int iAtual = 0;

        /// <summary>
        /// Reporta onde esta o processo (para status)
        /// </summary>
        /// <returns></returns>
        public string Local()
        {
            return string.Format("{0}:{1}/{2}", strLocal, iAtual, iTotal);
        }

        /// <summary>
        /// Cancela todos os processos em antamento
        /// </summary>
        public bool Cancelar;

        #region Standard
        private static NeonNet _NeonNetSt;

        /// <summary>
        /// cNeonNet est�tico
        /// </summary>
        public static NeonNet NeonNetSt
        {
            get { return _NeonNetSt ?? (_NeonNetSt = new NeonNet(CompontesBasicosProc.FuncoesBasicasProc.EstaEmProducao, DSCentral.EMP)); }
        }
        #endregion

        //private dLocal dLocal1;
        private dIntenet dInternet1;
        int EMPArquivo;

        /// <summary>
        /// Log das opera��es
        /// </summary>
        public ArrayList Retornos;

        private WSSincBancos.WSSincBancos WS;
        private WSPublica3.WSPublica3 wsP3;
        //private WSSincBancos.dBancos dBancoLido;

        private bool EmProducao;

        /// <summary>
        /// Construtor
        /// </summary>
        public NeonNet(bool _EmProducao, int EMP)
        {
            EmProducao = _EmProducao;
            dInternet1 = new dIntenet();
            Retornos = new ArrayList();
            WS = new WSSincBancos.WSSincBancos();
            wsP3 = new WSPublica3.WSPublica3();
            EMPArquivo = EMP;
            if (EmProducao)
            {
                WS.Url = @"http://177.190.193.218/neononline2/WSSincBancos.asmx";
                wsP3.Url = @"http://177.190.193.218/Neon23/neononline/WSPublica3.asmx";
                WS.Proxy = Framework.usuarioLogado.UsuarioLogadoNeon.UsuarioLogadoStNEON.Proxy;
                wsP3.Proxy = Framework.usuarioLogado.UsuarioLogadoNeon.UsuarioLogadoStNEON.Proxy;
            }

        }

        private static int staticR;

        /// <summary>
        /// Faz a baixa da cobran�a quando os boletos n�o est�o mais em aberto. esta fun��o cobre falhas ou altera��es diretas na base.
        /// </summary>
        /// <returns></returns>
        public string CobrancaNova()
        {
            DateTime dataInicio = DateTime.Now;
            iTotal = 0;
            iAtual = 0;
            string strErro = "";
            strLocal = "Cobran�a Nova incluir";
            CobrancaProc.CobrancaProc CobrancaProc1 = new CobrancaProc.CobrancaProc();
            int iInclusao = CobrancaProc1.IncluirNovos();
            if(CobrancaProc1.UltimoErro != null)
                strErro = VirExcepitionNeon.VirExceptionNeon.RelatorioDeErro(CobrancaProc1.UltimoErro, false, true, false);
            strLocal = "Cobran�a Nova exclus�o";
            int iExclusao = CobrancaProc1.RemoverPagos();
            if (CobrancaProc1.UltimoErro != null)
                strErro += VirExcepitionNeon.VirExceptionNeon.RelatorioDeErro(CobrancaProc1.UltimoErro, false, true, false);
            strLocal = "Cobran�a - Terminado";
            TimeSpan TempoTotal = DateTime.Now - dataInicio;
            return string.Format("({0}.{1:000} s) Cobran�a Nova: {2} inclu�dos {3} - exclu�dos: {4}\r\n{5}\r\n",
                                 TempoTotal.Seconds,
                                 TempoTotal.Milliseconds,
                                 strErro == "" ? "ok" : "ERRO",
                                 iInclusao,
                                 iExclusao,
                                 strErro);
        }

        private byte[] Criptografa()
        {
            string Aberto = string.Format("{0:yyyyMMddHHmmss}{1:000}{2:00000000}<-*W*->", DateTime.Now, EMPArquivo, staticR++);
            return VirCrip.VirCripWS.Crip(Aberto);
        }



        #region Publicacoes

        private bool FloatsIguais(float F1, float F2)
        {
            return Math.Abs(F1 - F2) < 0.00001F;
        }

        /// <summary>
        /// INPC
        /// </summary>
        /// <returns></returns>
        public string PublicaIndices()
        {
            dLocal dLocal1 = new dLocal();
            strLocal = "PublicaIndices";
            if (Cancelar)
                throw new Exception("Cancelado aguarda LIBERAR");
            string retorno = "";
            WS_BCB.BuscaIndices WS_BCB1 = new WS_BCB.BuscaIndices(EmProducao ? Framework.usuarioLogado.UsuarioLogadoNeon.UsuarioLogadoStNEON.Proxy : null);
            dLocal1.INPCTableAdapter.Fill(dLocal1.INPC);
            try
            {
                TableAdapter.AbreTrasacaoSQL("Internet NeonNet PublicaIndices - 446",
                                                       dLocal1.INPCTableAdapter);

                if (WS_BCB1.BuscaUltimo(WS_BCB.BuscaIndices.TiposBusca.INPC))
                {
                    virCompet.virCompetencia CompProx1 = WS_BCB1.Comp.CloneCompet(1);
                    virCompet.virCompetencia CompProx2 = WS_BCB1.Comp.CloneCompet(2);
                    dLocal.INPCRow rowAtual = dLocal1.INPC.FindByINPanoINPmes(WS_BCB1.Comp.Ano, WS_BCB1.Comp.Mes);
                    dLocal.INPCRow rowProx1 = dLocal1.INPC.FindByINPanoINPmes(CompProx1.Ano, CompProx1.Mes);
                    dLocal.INPCRow rowProx2 = dLocal1.INPC.FindByINPanoINPmes(CompProx2.Ano, CompProx2.Mes);
                    if ((rowAtual == null) || (rowProx1 == null))
                    {
                        TableAdapter.CommitSQL();
                        return string.Format("******** E R R O ***********\r\nsequencia INPC inv�lida. Compet�ncia {0}", WS_BCB1.Comp);
                    }
                    decimal ValorINPC = 1 + WS_BCB1.Valor / 100;
                    if (!FloatsIguais(rowAtual.INPInpc, (float)ValorINPC))
                    {
                        rowAtual.INPInpc = (float)ValorINPC;
                        dLocal1.INPCTableAdapter.Update(rowAtual);
                        rowAtual.AcceptChanges();
                    }
                    float Valorproxind1 = rowAtual.INPInpc * rowAtual.INPIndice;
                    if ((!FloatsIguais(rowProx1.INPInpc, (float)ValorINPC)) || (!FloatsIguais(rowProx1.INPIndice, Valorproxind1)))
                    {
                        rowProx1.INPInpc = (float)ValorINPC;
                        rowProx1.INPIndice = Valorproxind1;
                        dLocal1.INPCTableAdapter.Update(rowProx1);
                        rowProx1.AcceptChanges();
                    }
                    float Valorproxind2 = rowProx1.INPInpc * rowProx1.INPIndice;
                    if (rowProx2 == null)
                    {
                        rowProx2 = dLocal1.INPC.NewINPCRow();
                        rowProx2.INPano = CompProx2.Ano;
                        rowProx2.INPmes = CompProx2.Mes;
                        rowProx2.INPInpc = 1;
                        rowProx2.INPIndice = Valorproxind2;
                        dLocal1.INPC.AddINPCRow(rowProx2);
                    }
                    else if ((!FloatsIguais(rowProx2.INPInpc, 1)) || (!FloatsIguais(rowProx2.INPIndice, Valorproxind2)))
                    {
                        rowProx2.INPInpc = 1;
                        rowProx2.INPIndice = Valorproxind2;
                    }
                    if (rowProx2.RowState != DataRowState.Unchanged)
                    {
                        dLocal1.INPCTableAdapter.Update(rowProx2);
                        rowProx2.AcceptChanges();
                    }

                }
                
                retorno = string.Format("�ndices: {0}", retorno);
                Retornos.Add(retorno);
                TableAdapter.CommitSQL();
            }
            catch (Exception e)
            {
                TableAdapter.ST().Vircatch(e);
                retorno = VirExcepitionNeon.VirExceptionNeon.RelatorioDeErro(e, false, true, false);
                Retornos.Add(retorno);
            }
            finally
            {
                strLocal = "--";
            }
            if (WS_BCB1.BuscaUltimo(WS_BCB.BuscaIndices.TiposBusca.SalarioMinimo))
            {
                for (virCompet.virCompetencia comp = WS_BCB1.Comp; comp <= new virCompet.virCompetencia().CloneCompet(2); comp++)
                {
                    dLocal.SAlarioMinimoRow rowSAL;
                    if (dLocal1.SAlarioMinimoTableAdapter.FillByComp(dLocal1.SAlarioMinimo, comp.CompetenciaBind) == 0)
                    {
                        rowSAL = dLocal1.SAlarioMinimo.NewSAlarioMinimoRow();
                        rowSAL.SAMCompetencia = comp.CompetenciaBind;
                        rowSAL.SAMValor = WS_BCB1.Valor;
                        dLocal1.SAlarioMinimo.AddSAlarioMinimoRow(rowSAL);
                        retorno += string.Format("\r\nSal�rio m�nimo inclu�do {0} {1:n2}", rowSAL.SAMCompetencia, rowSAL.SAMValor);
                    }
                    else
                    {
                        rowSAL = dLocal1.SAlarioMinimo[0];
                        //*** MRC - INICIO (05/01/2016 13:30) ***
                        //if (rowSAL.SAMValor != WS_BCB1.Valor)
                        if (rowSAL.SAMValor < WS_BCB1.Valor)
                        //*** MRC - TERMINO (05/01/2016 13:30) ***
                        {
                            rowSAL.SAMValor = WS_BCB1.Valor;
                            retorno += string.Format("\r\nSal�rio m�nimo corrigido {0} {1:n2}", rowSAL.SAMCompetencia, rowSAL.SAMValor);
                        }
                    }
                    if (rowSAL.RowState != DataRowState.Unchanged)
                    {
                        dLocal1.SAlarioMinimoTableAdapter.Update(rowSAL);
                        rowSAL.AcceptChanges();
                    }
                }
            }
            return retorno;
        }

        /*
        /// <summary>
        /// Publica o corpo diretivo
        /// </summary>
        /// <returns></returns>
        public string PublicaCorpo()
        {
            dLocal dLocal1 = new dLocal();
            strLocal = "PublicaCorpo";
            if (Cancelar)
                throw new Exception("Cancelado aguarda LIBERAR");
            string retorno = "";
            try 
            {
                dLocal1.CorpoDiretivoTableAdapter.Fill(dLocal1.CorpoDiretivo);
                WSPublica3.dAPartamentoDoc dsEnviar = new WSPublica3.dAPartamentoDoc();
                foreach (dLocal.CorpoDiretivoRow row in dLocal1.CorpoDiretivo)                
                    dsEnviar.CORPODIRETIVO.AddCORPODIRETIVORow(EMPArquivo,
                                                               row.CDR,
                                                               row.CDR_CGO,
                                                               row.IsAPTNumeroNull() ? "" : row.APTNumero,
                                                               row.IsBLOCodigoNull() ? "" : row.BLOCodigo,
                                                               row.PESNome,
                                                               row.IsPESFone1Null() ? "" : row.PESFone1,
                                                               row.IsPESEmailNull() ? "" : row.PESEmail,
                                                               row.CDR_CON);

                retorno = wsP3.PublicaCDR(Criptografa(), dsEnviar);
                if(retorno.StartsWith("ok"))
                {
                    foreach (dLocal.CorpoDiretivoRow row in dLocal1.CorpoDiretivo)
                        dLocal1.CorpoDiretivoTableAdapter.MarcaL(row.CDR);
                }
                return "Corpo diretivo: " + retorno;
            }
            catch (Exception e)
            {
                retorno = VirExcepitionNeon.VirExceptionNeon.RelatorioDeErro(e, false, true, false);                
                Retornos.Add(retorno);
                return retorno;
            }
            finally
            {
                strLocal = "--";
            }
        }*/

        /// <summary>
        /// Condominios
        /// </summary>
        /// <returns></returns>
        public string PublicaCondominios()
        {
            if (Cancelar)
                throw new Exception("Cancelado aguarda LIBERAR");
            dLocal dLocal1 = new dLocal();
            string retorno;
            bool Inclusao;
            string Novos = "";
            dIntenet.CONDOMINIOSRow RowRemota;
            int alterados = dLocal1.CONDOMINIOSTableAdapter.FillBy(dLocal1.CONDOMINIOS, EMPArquivo);
            if (alterados == 0)
            {
                return "Condom�nios: Sem altera��es\r\n";
            };
            strLocal = "PublicaCondominios";
            iTotal = alterados;
            iAtual = 0;
            int nCondominioI = 0;
            int nCondominioU = 0;
            int nNaoPublicaveis = 0;
            bool DevePublicar;


            try
            {
                dInternet1.CONDOMINIOSTableAdapter.FillBy(dInternet1.CONDOMINIOS, EMPArquivo);
                //WSSincBancos.dBancos dBanco = WS.LeCondominios(Criptografa());
                TableAdapter.AbreTrasacaoSQL("Internet Neonnet - 530");
                foreach (dLocal.CONDOMINIOSRow CONDOMINIOSRow in dLocal1.CONDOMINIOS)
                {
                    if (Cancelar)
                        throw new Exception("Cancelado aguarda LIBERAR");
                    iAtual++;
                    RowRemota = dInternet1.CONDOMINIOS.FindByCON_EMPCON(EMPArquivo, CONDOMINIOSRow.CON);
                    Inclusao = (RowRemota == null);
                    if (Inclusao)
                    {
                        RowRemota = dInternet1.CONDOMINIOS.NewCONDOMINIOSRow();
                        RowRemota.CON_EMP = EMPArquivo;
                        Novos += string.Format("SQL inclu�do ({0} - {1})\r\n", CONDOMINIOSRow.CONCodigo, CONDOMINIOSRow.CON);
                    }

                    foreach (DataColumn Coluna in dInternet1.CONDOMINIOS.Columns)
                        //if(Coluna.ColumnName != "CON_EMP")
                        RowRemota[Coluna] = CONDOMINIOSRow[Coluna.ColumnName];
                    if (Inclusao)
                        dInternet1.CONDOMINIOS.AddCONDOMINIOSRow(RowRemota);

                    //Antiga
                    //WSSincBancos.dBancos.condominiosRow WSrow = dBanco.condominios.FindByCODCON(CONDOMINIOSRow.CONCodigo);
                    //Inclusao = (WSrow == null);
                    DevePublicar = (!CONDOMINIOSRow.IsCONExportaInternetNull()) && (CONDOMINIOSRow.CONExportaInternet);
                    if (CONDOMINIOSRow.CONStatus != 1)
                        DevePublicar = false;
                    /*
                    if (DevePublicar)
                    {
                        if (Inclusao)
                        {
                            WSrow = dBanco.condominios.NewcondominiosRow();
                            WSrow.CODCON = CONDOMINIOSRow.CONCodigo;
                            Novos += string.Format("OLE inclu�do {0}\r\n", CONDOMINIOSRow.CONCodigo);
                        };

                        WSrow.Nome = CONDOMINIOSRow.CONNome;
                        if (!CONDOMINIOSRow.IsCON_BCONull())
                            WSrow.NumBanco = CONDOMINIOSRow.CON_BCO.ToString("000");
                        if ((!CONDOMINIOSRow.IsCONValorMultaNull()) && (!CONDOMINIOSRow.IsCONTipoMultaNull()))
                            WSrow.Multasql = string.Format("{0},'{1}'", CONDOMINIOSRow.CONValorMulta.ToString("0.0").Replace(",", "."), CONDOMINIOSRow.CONTipoMulta);
                        if ((!CONDOMINIOSRow.IsCONContaNull()) && (!CONDOMINIOSRow.IsCONDigitoContaNull()))
                            WSrow.cc = CONDOMINIOSRow.CONConta + CONDOMINIOSRow.CONDigitoConta;
                        if (!CONDOMINIOSRow.IsCONAgenciaNull())
                            WSrow.Agencia = CONDOMINIOSRow.CONAgencia;
                        if (!CONDOMINIOSRow.IsCONDigitoAgenciaNull())
                            WSrow.dgagencia = CONDOMINIOSRow.CONDigitoAgencia;
                        WSrow.End = CONDOMINIOSRow.CONEndereco;
                        WSrow.Bairro = CONDOMINIOSRow.CONBairro;
                        if (!CONDOMINIOSRow.IsCIDNomeNull())
                            WSrow.Cidade = CONDOMINIOSRow.CIDNome;
                        if (!CONDOMINIOSRow.IsCIDUfNull())
                            WSrow.Estado = CONDOMINIOSRow.CIDUf;
                        if (!CONDOMINIOSRow.IsCONCepNull())
                            WSrow.CEP = CONDOMINIOSRow.CONCep;
                        if (!CONDOMINIOSRow.IsCONCnpjNull())
                            WSrow.CNPJ = CONDOMINIOSRow.CONCnpj;
                        if (!CONDOMINIOSRow.IsPESNomeNull())
                            WSrow.Sindico = CONDOMINIOSRow.PESNome;
                        if (!CONDOMINIOSRow.IsCONDataMandatoNull())
                            WSrow.mandato = CONDOMINIOSRow.CONDataMandato;
                        if (!CONDOMINIOSRow.IsCONObservacaoInternetNull())
                            WSrow.OBS = CONDOMINIOSRow.CONObservacaoInternet;
                        if (Inclusao)
                            dBanco.condominios.AddcondominiosRow(WSrow);
                    }
                    else
                    {
                        if (!Inclusao)
                        {
                            WSrow.Delete();
                            Novos += string.Format("OLE removido {0}\r\n", CONDOMINIOSRow.CONCodigo);
                            // remover restante
                        }                        
                    }*/
                    CONDOMINIOSRow.CON__INTERNET = "L";

                    //progressBarControl1.Position++;
                    if (Inclusao)
                        if (DevePublicar)
                            nCondominioI++;
                        else
                            nNaoPublicaveis++;
                    else
                        nCondominioU++;
                    //Application.DoEvents();
                };

                //string WSretorno = WS.GravaCondominios(Criptografa(), dBanco);
                //if (WSretorno.StartsWith("ok"))
                //{
                //    retorno = WSretorno;
                //    dLocal1.BOLetosTableAdapter.Update(dLocal1.BOLetos);
                //    VirMSSQL.TableAdapter.ST().Commit();
                //}
                //else
                //    throw new Exception(WSretorno);
                dInternet1.CONDOMINIOSTableAdapter.Update(dInternet1.CONDOMINIOS);
                dLocal1.CONDOMINIOSTableAdapter.Update(dLocal1.CONDOMINIOS);
                TableAdapter.ST().Commit();
            }
            catch (Exception e)
            {
                TableAdapter.ST().Vircatch(e);
                retorno = VirExcepitionNeon.VirExceptionNeon.RelatorioDeErro(e, false, true, false);
                Retornos.Add(retorno);
                return retorno;
            }
            retorno = string.Format("Condom�nios: alterados {0} - Inclu�dos: {1} - N�o publicados: {2}\r\n{3}\r\n", nCondominioU, nCondominioI, nNaoPublicaveis, Novos);
            Retornos.Add(retorno);
            strLocal = "--";
            return retorno;
        }

        //*** MRC - INICIO (27/05/2016 12:00) ***
        /// <summary>
        /// Verifica as Reservas e Cancelamentos efetuados no site
        /// </summary>
        /// <returns></returns>
        public string VerificaReservasSite()
        {
            int nRec = 0;
            DateTime dataInicio = DateTime.Now;
            string strErro = "";
            try
            {
                strLocal = "Processa Reservas - Recuperando Dados do site";
                iTotal = 0;
                iAtual = 0;
                if (Cancelar)
                    throw new Exception("Cancelado aguarda LIBERAR");
                dLocal dLocal1 = new dLocal();
                dIntenet dIntenet = new dIntenet();

                //Processa reservas (ou cancelamentos) efetuados no site   
                iTotal = dInternet1.ReservaEQuipamentoTableAdapter.FillRecuperar(dInternet1.ReservaEQuipamento, EMPArquivo);
                if (iTotal != 0)
                {
                    dLocal1.ReservaEQuipamentoTableAdapter.FillByData(dLocal1.ReservaEQuipamento, DateTime.Today.AddMonths(-1));
                    foreach (dIntenet.ReservaEQuipamentoRow rowREQ in dInternet1.ReservaEQuipamento)
                    {
                        dLocal.ReservaEQuipamentoRow rowREQlocal = dLocal1.ReservaEQuipamento.FindByREQ(rowREQ.REQ);
                        if (rowREQlocal == null)
                            continue;
                        if (rowREQ.REQ_APT == 0)
                        {
                            //Apaga dados da reserva
                            rowREQlocal.SetREQ_APTNull();
                            rowREQlocal.SetREQDataHoraNull();
                            //Verifica se apaga BOD
                            if (!rowREQlocal.IsREQ_BODNull())
                            {
                                dLocal1.BOletoDetalheTableAdapter.FillByBOD(dLocal1.BOletoDetalhe, rowREQlocal.REQ_BOD);
                                dLocal.BOletoDetalheRow rowBODlocal = dLocal1.BOletoDetalhe.FindByBOD(rowREQlocal.REQ_BOD);
                                if (rowBODlocal.IsBOD_BOLNull())
                                    rowBODlocal.Delete();
                                rowREQlocal.SetREQ_BODNull();
                            }
                        }
                        else
                        {
                            //Seta dados da reserva
                            rowREQlocal.REQ_APT = rowREQ.REQ_APT;
                            rowREQlocal.REQDataHora = rowREQ.REQDataHora;
                        }
                        rowREQ.REQ__INTERNETR = "L";
                        iAtual++;
                        nRec++;
                    }
                    try
                    {
                        dLocal1.ReservaEQuipamentoTableAdapter.Update(dLocal1.ReservaEQuipamento);
                        dLocal1.BOletoDetalheTableAdapter.Update(dLocal1.BOletoDetalhe);
                        dInternet1.ReservaEQuipamentoTableAdapter.Update(dInternet1.ReservaEQuipamento);
                    }
                    catch (Exception e)
                    {
                        strErro += VirExcepitionNeon.VirExceptionNeon.RelatorioDeErro(e, false, true, false);
                    }
                };
            }
            catch (Exception e)
            {
                strErro += VirExcepitionNeon.VirExceptionNeon.RelatorioDeErro(e, false, true, false);
            }

            iTotal = iAtual = 0;
            strLocal = "Processa Reservas - Terminado";
            TimeSpan TempoTotal = DateTime.Now - dataInicio;
            if (strErro != "")
                strErro += Environment.NewLine;
            return string.Format("({0}.{1:000} s) Processa Reservas: {2} Recebidos: {3} \r\n{4}\r\n",
                                 TempoTotal.Seconds,
                                 TempoTotal.Milliseconds,
                                 strErro == "" ? "ok" : "ERRO",
                                 nRec,
                                 strErro);
        }
        //*** MRC - TERMINO (27/05/2016 12:00) *** 

        /// <summary>
        /// Gera as reservas (dias) a partir da grade
        /// </summary>        
        /// <returns></returns>
        public string GeraReserva()
        {
            DateTime dataInicio = DateTime.Now;
            string strErro = "";
            int nReqI = 0;
            int nReqR = 0;
            try
            {
                strLocal = "GeraReserva - Terminado";
                iTotal = 0;
                iAtual = 0;
                dLocal dLocal1 = new dLocal();
                List<int> Apagar = new List<int>();

                iTotal = dLocal1.GradeEQuipamentoTableAdapter.FillComJanela(dLocal1.GradeEQuipamento);
                dLocal1.ReservaEQuipamentoTableAdapter.FillByData(dLocal1.ReservaEQuipamento, DateTime.Today);
                foreach (dLocal.ReservaEQuipamentoRow rowDel in dLocal1.ReservaEQuipamento)
                    if (rowDel.IsREQ_APTNull())
                        Apagar.Add(rowDel.REQ);

                //*** MRC - INICIO (27/05/2016 12:00) ***
                //dLocal1.ReservaEQuipamento.DefaultView.Sort = "REQ_EQP,REQInicio";
                dLocal1.ReservaEQuipamento.DefaultView.Sort = "REQ_EQP,REQInicio,REQTermino";
                //*** MRC - TERMINO (27/05/2016 12:00) ***

                foreach (dLocal.GradeEQuipamentoRow rowGEQ in dLocal1.GradeEQuipamento)
                {
                    iAtual++;
                    AbstratosNeon.GradeReserva Tipo = (AbstratosNeon.GradeReserva)rowGEQ.GEQDiaSemana;

                    //*** MRC - INICIO (15/08/2016 14:00) - Pacote 2 ***
                    //Verifica o tipo de grade para determinar a data de corte
                    if (rowGEQ.GEQ_EQP == 138 || rowGEQ.GEQ_EQP == 139)
                        iAtual = iAtual;

                    DateTime DataCorte;
                    if ((int)rowGEQ.EQPTipoGrade == (int)AbstratosNeon.EQPTipoGrade.DiasCorridos)
                    {
                        //*** MRC - INICIO (08/08/2016 11:00) ***
                        DataCorte = DateTime.Today.AddDays(rowGEQ.EQPJanela + 1);
                        //*** MRC - FIM (08/08/2016 11:00) ***
                    }
                    else
                    {
                        DateTime DataInicioGrade = Convert.ToDateTime(string.Format("01/{0:00}/{1:0000}", DateTime.Today.Month, DateTime.Today.Year));
                        DataCorte = DataInicioGrade.AddMonths(rowGEQ.EQPJanela + 1);
                    }
                    //*** MRC - TERMINO (15/08/2016 14:00) - Pacote 2 ***


                    for (DateTime Dia = DateTime.Today; Dia < DataCorte; Dia = Dia.AddDays(1))
                    {
                        bool Contem = (Tipo == AbstratosNeon.GradeReserva.Todos);
                        switch (Dia.DayOfWeek)
                        {
                            case DayOfWeek.Friday:
                                if (Tipo == AbstratosNeon.GradeReserva.Segunda_Sexta)
                                    Contem = true;
                                if (Tipo == AbstratosNeon.GradeReserva.Terca_Sexta)
                                    Contem = true;
                                if (Tipo == AbstratosNeon.GradeReserva.Sexta)
                                    Contem = true;
                                break;
                            case DayOfWeek.Monday:
                                if (Tipo == AbstratosNeon.GradeReserva.Segunda_Sexta)
                                    Contem = true;
                                if (Tipo == AbstratosNeon.GradeReserva.Segunda)
                                    Contem = true;
                                break;
                            case DayOfWeek.Saturday:
                                if (Tipo == AbstratosNeon.GradeReserva.Sabado_Domingo)
                                    Contem = true;
                                if (Tipo == AbstratosNeon.GradeReserva.Sabado)
                                    Contem = true;
                                break;
                            case DayOfWeek.Sunday:
                                if (Tipo == AbstratosNeon.GradeReserva.Sabado_Domingo)
                                    Contem = true;
                                if (Tipo == AbstratosNeon.GradeReserva.Domingo)
                                    Contem = true;
                                break;
                            case DayOfWeek.Thursday:
                                if (Tipo == AbstratosNeon.GradeReserva.Segunda_Sexta)
                                    Contem = true;
                                if (Tipo == AbstratosNeon.GradeReserva.Terca_Sexta)
                                    Contem = true;
                                if (Tipo == AbstratosNeon.GradeReserva.Quinta)
                                    Contem = true;
                                break;
                            case DayOfWeek.Tuesday:
                                if (Tipo == AbstratosNeon.GradeReserva.Segunda_Sexta)
                                    Contem = true;
                                if (Tipo == AbstratosNeon.GradeReserva.Terca_Sexta)
                                    Contem = true;
                                if (Tipo == AbstratosNeon.GradeReserva.Terca)
                                    Contem = true;
                                break;
                            case DayOfWeek.Wednesday:
                                if (Tipo == AbstratosNeon.GradeReserva.Segunda_Sexta)
                                    Contem = true;
                                if (Tipo == AbstratosNeon.GradeReserva.Terca_Sexta)
                                    Contem = true;
                                if (Tipo == AbstratosNeon.GradeReserva.Quarta)
                                    Contem = true;
                                break;
                            default:
                                break;
                        }
                        if (Contem)
                        {
                            DateTime Inicio = Dia.AddHours(rowGEQ.GEQInicio.Hour).AddMinutes(rowGEQ.GEQInicio.Minute);
                            DateTime Termino = Dia.AddHours(rowGEQ.GEQTermino.Hour).AddMinutes(rowGEQ.GEQTermino.Minute);
                            //*** MRC - INICIO (08/06/2016 11:00) ***
                            if (Termino < Inicio) Termino = Termino.AddDays(1);
                            //*** MRC - TERMINO (08/06/2016 11:00) ***

                            //*** MRC - INICIO (27/05/2016 12:00) ***
                            //int i = dLocal1.ReservaEQuipamento.DefaultView.Find(new object[] { rowGEQ.GEQ_EQP, Inicio });
                            int i = dLocal1.ReservaEQuipamento.DefaultView.Find(new object[] { rowGEQ.GEQ_EQP, Inicio, Termino });
                            int jareservado = 0;
                            if (rowGEQ.GEQ_EQP == 168)
                                jareservado = 0;
                            TableAdapter.ST().BuscaEscalar(String.Format("SELECT COUNT(*) FROM ReservaEQuipamento WHERE REQ_EQP = {0}" +
                                                                         " AND ((CONVERT(DATETIME,REQInicio,103) <= CONVERT(DATETIME,'{1}',103) AND CONVERT(DATETIME,'{1}',103) < CONVERT(DATETIME,REQTermino,103))" +
                                                                         "   OR (CONVERT(DATETIME,REQInicio,103) < CONVERT(DATETIME,'{2}',103) AND CONVERT(DATETIME,'{2}',103) <= CONVERT(DATETIME,REQTermino,103))" +
                                                                         "   OR (CONVERT(DATETIME,'{1}',103) <= CONVERT(DATETIME,REQInicio,103) AND  CONVERT(DATETIME,REQTermino,103) <= CONVERT(DATETIME,'{2}',103)))" +
                                                                         " AND REQ_APT IS NOT NULL AND REQ_APT > 0", rowGEQ.GEQ_EQP, Inicio, Termino), out jareservado);
                            if (jareservado == 0)
                            {
                                //*** MRC - TERMINO (27/05/2016 12:00) ***
                                if (i < 0)
                                {
                                    dLocal.ReservaEQuipamentoRow novaREQ = dLocal1.ReservaEQuipamento.NewReservaEQuipamentoRow();
                                    novaREQ.REQ_EQP = rowGEQ.GEQ_EQP;
                                    novaREQ.REQInicio = Inicio;
                                    novaREQ.REQTermino = Termino;
                                    dLocal1.ReservaEQuipamento.AddReservaEQuipamentoRow(novaREQ);
                                    nReqI++;
                                }
                                else
                                    Apagar.Remove(((dLocal.ReservaEQuipamentoRow)(dLocal1.ReservaEQuipamento.DefaultView[i].Row)).REQ);
                                //*** MRC - INICIO (27/05/2016 12:00) ***
                            }
                            //*** MRC - TERMINO (27/05/2016 12:00) ***
                        }
                    }
                }
                //Limpar
                foreach (int REQ in Apagar)
                {
                    dLocal.ReservaEQuipamentoRow rowREG = dLocal1.ReservaEQuipamento.FindByREQ(REQ);
                    rowREG.Delete();
                    nReqR++;
                };
                dLocal1.ReservaEQuipamentoTableAdapter.Update(dLocal1.ReservaEQuipamento);
                dLocal1.GradeEQuipamentoTableAdapter.Update(dLocal1.GradeEQuipamento);
            }
            catch (Exception e)
            {
                strErro += VirExcepitionNeon.VirExceptionNeon.RelatorioDeErro(e, false, true, false);
            }
            iTotal = iAtual = 0;
            strLocal = "GeraReserva - Terminado";
            TimeSpan TempoTotal = DateTime.Now - dataInicio;
            return string.Format("({0}.{1:000} s) GeraReserva: {2} Inclu�dos: {3} Removidos: {4}\r\n{5}\r\n",
                                 TempoTotal.Seconds,
                                 TempoTotal.Milliseconds,
                                 strErro == "" ? "ok" : "ERRO",
                                 nReqI,
                                 nReqR,
                                 strErro);
        }

        /// <summary>
        /// Publica os ReservaEQuipamento
        /// </summary>
        /// <returns></returns>
        public string ReservaEQuipamento()
        {
            //*** MRC - INICIO (27/05/2016 12:00) ***       
            //int nRec = 0;
            //*** MRC - TERMINO (27/05/2016 12:00) ***       
            int nReqI = 0;
            int nReqU = 0;
            int nAntigos = 0;
            int nBODs = 0;
            DateTime dataInicio = DateTime.Now;
            string strErro = "";
            try
            {
                strLocal = "Publica ReservaEQuipamento - Recuperando Dados do site";
                iTotal = 0;
                iAtual = 0;
                if (Cancelar)
                    throw new Exception("Cancelado aguarda LIBERAR");
                dLocal dLocal1 = new dLocal();
                dIntenet dIntenet = new dIntenet();
                //*** MRC - INICIO - RESERVA (3) ***
                dRateios dRateio = new dRateios();
                //*** MRC - FIM - RESERVA (3) ***                            

                //*** MRC - INICIO (27/05/2016 12:00) ***       
                ////Processa reservas (ou cancelamentos) efetuados no site   
                ////*** MRC - INICIO - RESERVA (18/03/2015 10:00) ***               
                //iTotal = dInternet1.ReservaEQuipamentoTableAdapter.FillRecuperar(dInternet1.ReservaEQuipamento, EMPArquivo);
                ////*** MRC - TERMINO - RESERVA (18/03/2015 10:00) ***
                //if (iTotal != 0)
                //{
                //    dLocal1.ReservaEQuipamentoTableAdapter.FillByData(dLocal1.ReservaEQuipamento, DateTime.Today.AddMonths(-1));
                //    foreach (dIntenet.ReservaEQuipamentoRow rowREQ in dInternet1.ReservaEQuipamento)
                //    {
                //        dLocal.ReservaEQuipamentoRow rowREQlocal = dLocal1.ReservaEQuipamento.FindByREQ(rowREQ.REQ);
                //        if (rowREQlocal == null)                        
                //            continue;                        
                //        if (rowREQ.REQ_APT == 0)
                //        {
                //            rowREQlocal.SetREQ_APTNull();
                //            //remover BOD
                //        }
                //        else
                //        {
                //            if (!rowREQlocal.IsREQ_APTNull() && (rowREQlocal.REQ_APT != rowREQ.REQ_APT))
                //            {
                //                //remover BOD
                //            }
                //            rowREQlocal.REQ_APT = rowREQ.REQ_APT;
                //        }
                //        //pegar data e hora do cadastro
                //        rowREQ.REQ__INTERNETR = "L";
                //        iAtual++;
                //        nRec++;
                //    }
                //    try
                //    {
                //        dLocal1.ReservaEQuipamentoTableAdapter.Update(dLocal1.ReservaEQuipamento);
                //        dInternet1.ReservaEQuipamentoTableAdapter.Update(dInternet1.ReservaEQuipamento);                        
                //    }
                //    catch (Exception e)
                //    {
                //        strErro += VirExcepitionNeon.VirExceptionNeon.RelatorioDeErro(e, false, true, false);                
                //    }
                //};
                //*** MRC - TERMINO (27/05/2016 12:00) ***

                //Apaga os registros vencidos
                //*** MRC - INICIO - RESERVA (18/03/2015 10:00) ***               
                nAntigos = dInternet1.ReservaEQuipamentoTableAdapter.DeleteVencidos(DateTime.Today, EMPArquivo);
                //*** MRC - TERMINO - RESERVA (18/03/2015 10:00) ***               

                //Envia Grade
                iTotal = iAtual = 0;
                strLocal = "Publica ReservaEQuipamento - Enviando Dados do site";
                iTotal = dLocal1.ReservaEQuipamentoTableAdapter.FillPublicar(dLocal1.ReservaEQuipamento);
                dInternet1.ReservaEQuipamentoTableAdapter.Fill(dInternet1.ReservaEQuipamento);
                foreach (dLocal.ReservaEQuipamentoRow rowREQ in dLocal1.ReservaEQuipamento)
                {
                    if (Cancelar)
                        throw new Exception("Cancelado aguarda LIBERAR");
                    iAtual++;
                    if (rowREQ.REQInicio >= DateTime.Today)
                    {
                        //*** MRC - INICIO - RESERVA (18/03/2015 10:00) ***
                        dIntenet.ReservaEQuipamentoRow rowREQremota = dInternet1.ReservaEQuipamento.FindByREQREQ_EMP(rowREQ.REQ, EMPArquivo);
                        //*** MRC - TERMINO - RESERVA (18/03/2015 10:00) ***
                        bool Inclusao = rowREQremota == null;
                        if (Inclusao)
                        {
                            nReqI++;
                            rowREQremota = dInternet1.ReservaEQuipamento.NewReservaEQuipamentoRow();
                            rowREQremota.REQ_APT = 0;
                            //*** MRC - INICIO - RESERVA (18/03/2015 10:00) ***
                            rowREQremota.REQ_EMP = EMPArquivo;
                            //*** MRC - TERMINO - RESERVA (18/03/2015 10:00) ***
                        }
                        else
                            nReqU++;
                        //*** MRC - INICIO - RESERVA (18/03/2015 10:00) ***
                        List<string> CamposNaoPublicaveis = new List<string>() { "REQ__INTERNETR", "REQ_APT", "REQ_EMP", "REQDataHora" };
                        //*** MRC - TERMINO - RESERVA (18/03/2015 10:00) ***
                        foreach (DataColumn Coluna in dInternet1.ReservaEQuipamento.Columns)
                        {
                            if (!CamposNaoPublicaveis.Contains(Coluna.ColumnName))
                                rowREQremota[Coluna] = rowREQ[Coluna.ColumnName];
                        }
                        if (Inclusao)
                            dInternet1.ReservaEQuipamento.AddReservaEQuipamentoRow(rowREQremota);
                    }
                    rowREQ.REQ__INTERNET = "L";
                }

                if ((nReqI != 0) || (nReqU != 0))
                {
                    dInternet1.ReservaEQuipamentoTableAdapter.Update(dInternet1.ReservaEQuipamento);
                    dLocal1.ReservaEQuipamentoTableAdapter.Update(dLocal1.ReservaEQuipamento);
                }

                //Gera os BODs - primeiro os bods apos data da festa (intGravarBodTipo = 0), e depois os bods apos prazo de cancelamento (intGravarBodTipo = 1)
                int intGravarBodTipo = 0;
                while (intGravarBodTipo <= 1)
                {
                    iAtual = 0;
                    if (intGravarBodTipo == 0)
                    {
                        iTotal = dLocal1.ReservaEQuipamentoTableAdapter.FillByGravarBod(dLocal1.ReservaEQuipamento, DateTime.Today);
                        strLocal = "Publica ReservaEQuipamento - Gerando BOD (ap�s a data da festa)";
                    }
                    else
                    {
                        iTotal += dLocal1.ReservaEQuipamentoTableAdapter.FillByGravarBodPrazo(dLocal1.ReservaEQuipamento, DateTime.Today);
                        strLocal = "Publica ReservaEQuipamento - Gerando BOD Parcial (ap�s finalizado o prazo de cancelamento)";
                    }
                    foreach (dLocal.ReservaEQuipamentoRow rowREQ in dLocal1.ReservaEQuipamento)
                    {
                        if (Cancelar)
                            throw new Exception("Cancelado aguarda LIBERAR");
                        iAtual++;
                        nBODs++;

                        //Verifica valor da cobranca, forma de cobranca, tipo da cobranca, tipo cancelamento
                        decimal? Valor = TableAdapter.ST().BuscaEscalar_decimal("SELECT EQPValor FROM EQuiPamentos WHERE (EQP = @P1)", rowREQ.REQ_EQP);
                        int? FormaCobranca = TableAdapter.ST().BuscaEscalar_int("SELECT EQPFormaCobrar FROM EQuiPamentos WHERE (EQP = @P1)", rowREQ.REQ_EQP);
                        int? TipoCobranca = TableAdapter.ST().BuscaEscalar_int("SELECT EQPTipoCobranca FROM EQuiPamentos WHERE (EQP = @P1)", rowREQ.REQ_EQP);
                        int? TipoCancelamento = TableAdapter.ST().BuscaEscalar_int("SELECT EQPTipoCancelamento FROM EQuiPamentos WHERE (EQP = @P1)", rowREQ.REQ_EQP);
                        if (!Valor.HasValue || !FormaCobranca.HasValue || !TipoCobranca.HasValue || !TipoCancelamento.HasValue)
                        {
                            throw new Exception("EQP n�o encontrado");
                        }

                        //Verifica data de referencia da cobranca
                        DateTime datReferencia = rowREQ.REQInicio;
                        if (intGravarBodTipo == 1)
                        {
                            DateTime? datPrazoCancelamento = (TipoCancelamento == 0)
                                                             ? TableAdapter.ST().BuscaEscalar_DateTime("SELECT DATEADD(d, - EQuiPamentos.EQPDiasCancelar, CONVERT(DATETIME, @P1, 103)) AS PrazoCancelamento FROM EQuiPamentos WHERE (EQP = @P2)", rowREQ.REQInicio, rowREQ.REQ_EQP)
                                                             : TableAdapter.ST().BuscaEscalar_DateTime("SELECT DATEADD(d, EQuiPamentos.EQPDiasCancelar, CONVERT(DATETIME, @P1, 103)) AS PrazoCancelamento FROM EQuiPamentos WHERE (EQP = @P2)", rowREQ.REQDataHora, rowREQ.REQ_EQP);
                            if (datPrazoCancelamento < datReferencia)
                                datReferencia = Convert.ToDateTime(datPrazoCancelamento);
                        }

                        //Verifica apartamento e proxima competencia
                        Framework.objetosNeon.Apartamento Apart = new Framework.objetosNeon.Apartamento(rowREQ.REQ_APT);
                        Framework.objetosNeon.Competencia compet1 = Framework.objetosNeon.Competencia.Proxima(Apart.CON);

                        //Verifica valor de acordo com a forma de cobranca
                        switch (FormaCobranca)
                        {
                            case (int)AbstratosNeon.EQPFormaCobrar.Salario:
                                {
                                    //Calcula porcentagem do salario minimo atual         
                                    decimal? SalarioMinimo = TableAdapter.ST().BuscaEscalar_decimal("select SAMValor from SAlarioMinimo where SAMCompetencia = @p1", rowREQ.REQInicio.ToString("yyyyMM"));
                                    if (!SalarioMinimo.HasValue)
                                    {
                                        throw new Exception(string.Format("Sal�rio m�nimo n�o encontrado para ({0})", compet1));
                                    }
                                    Valor = SalarioMinimo.Value * (Valor / 100);
                                    break;
                                }
                            case (int)AbstratosNeon.EQPFormaCobrar.Condominio:
                                {
                                    //Calcula porcentagem do valor do condominio                           
                                    dRateios.ListaFracaoDataTable TabLista = dRateios.ListaFracaoTableAdapter.GetData(Apart.CON);
                                    dRateios.AjustaDescontos(dRateios.TiposDesconto.Condominio, TabLista);
                                    dRateio.CalculaFracaoCorrigida(TableAdapter.ST().BuscaEscalar_bool("select CONFracaoIdeal from CONDOMINIOS where CON = @P1", Apart.CON).Value ? dRateios.TipoSimula.ComFracao : dRateios.TipoSimula.SemFracao, TabLista, TableAdapter.ST().BuscaEscalar_decimal("select CONValorCondominio from CONDOMINIOS where CON = @P1", Apart.CON).Value);
                                    foreach (dRateios.ListaFracaoRow linhaLista in TabLista)
                                    {
                                        if (linhaLista.APT == Apart.APT)
                                        {
                                            Valor = Convert.ToDecimal(linhaLista.ValorNominal) * (Valor / 100);
                                            break;
                                        }
                                    }
                                    break;
                                }
                            //*** MRC - INICIO - RESERVA (14/01/2016 12:30) ***
                            case (int)AbstratosNeon.EQPFormaCobrar.CondominioMenor:
                                {
                                    //Calcula porcentagem do valor do menor condominio                           
                                    dRateios.ListaFracaoDataTable TabLista = dRateios.ListaFracaoTableAdapter.GetData(Apart.CON);
                                    dRateios.AjustaDescontos(dRateios.TiposDesconto.Condominio, TabLista);
                                    dRateio.CalculaFracaoCorrigida(TableAdapter.ST().BuscaEscalar_bool("select CONFracaoIdeal from CONDOMINIOS where CON = @P1", Apart.CON).Value ? dRateios.TipoSimula.ComFracao : dRateios.TipoSimula.SemFracao, TabLista, TableAdapter.ST().BuscaEscalar_decimal("select CONValorCondominio from CONDOMINIOS where CON = @P1", Apart.CON).Value);
                                    decimal ValorMenor = 0;
                                    foreach (dRateios.ListaFracaoRow linhaLista in TabLista)
                                        if (ValorMenor == 0 || ValorMenor > Convert.ToDecimal(linhaLista.ValorNominal))
                                        {
                                            if (linhaLista.APTTipo == (int)VirEnumeracoesNeon.APTTipo.Casa || 
                                                linhaLista.APTTipo == (int)VirEnumeracoesNeon.APTTipo.Apartamento || 
                                                linhaLista.APTTipo == (int)VirEnumeracoesNeon.APTTipo.DoCondominio)
                                                    ValorMenor = Convert.ToDecimal(linhaLista.ValorNominal);
                                        }
                                    Valor = ValorMenor * (Valor / 100);
                                    break;
                                }
                            case (int)AbstratosNeon.EQPFormaCobrar.CondominioMaior:
                                {
                                    //Calcula porcentagem do valor do maior condominio                           
                                    dRateios.ListaFracaoDataTable TabLista = dRateios.ListaFracaoTableAdapter.GetData(Apart.CON);
                                    dRateios.AjustaDescontos(dRateios.TiposDesconto.Condominio, TabLista);
                                    dRateio.CalculaFracaoCorrigida(TableAdapter.ST().BuscaEscalar_bool("select CONFracaoIdeal from CONDOMINIOS where CON = @P1", Apart.CON).Value ? dRateios.TipoSimula.ComFracao : dRateios.TipoSimula.SemFracao, TabLista, TableAdapter.ST().BuscaEscalar_decimal("select CONValorCondominio from CONDOMINIOS where CON = @P1", Apart.CON).Value);
                                    decimal ValorMaior = 0;
                                    foreach (dRateios.ListaFracaoRow linhaLista in TabLista)
                                        if (ValorMaior < Convert.ToDecimal(linhaLista.ValorNominal))
                                            ValorMaior = Convert.ToDecimal(linhaLista.ValorNominal);
                                    Valor = ValorMaior * (Valor / 100);
                                    break;
                                }
                                //*** MRC - TERMINO - RESERVA (14/01/2016 12:30) ***
                        }
                        try
                        {
                            TableAdapter.AbreTrasacaoSQL("internet neonnet - 1160", dLocal1.ReservaEQuipamentoTableAdapter);
                            string strEQPNome = "Reserva de Espa�o";
                            TableAdapter.ST().BuscaEscalar("select EQPNome from EQuiPamentos where EQP = " + rowREQ.REQ_EQP, out strEQPNome);
                            string Mensagem = string.Format("{0} - {1:dd/MM/yyyy}", strEQPNome, rowREQ.REQInicio);
                            if (Mensagem.Length > 50)
                                Mensagem = string.Format("{0} {1:dd/MM/yyyy}", strEQPNome, rowREQ.REQInicio);
                            if (Mensagem.Length > 50)
                                Mensagem = string.Format("{0} {1:dd/MM/yyyy}", strEQPNome.Substring(0, 50 - 11), rowREQ.REQInicio);
                            //*** MRC - INICIO (13/05/2016 12:00) ***
                            string strEQPPLA = "104000";
                            TableAdapter.ST().BuscaEscalar("select EQP_PLA from EQuiPamentos where EQP = " + rowREQ.REQ_EQP, out strEQPPLA);
                            BoletosProc.Boleto.dBoletos.BOletoDetalheRow BODrow = Boletos.Boleto.Boleto.GerarBOD(Apart.CON, rowREQ.REQ_APT, false, strEQPPLA, true, compet1, datReferencia, Mensagem, Math.Round(Valor.Value, 2), true);
                            //*** MRC - TERMINO (13/05/2016 12:00) ***
                            rowREQ.REQ_BOD = BODrow.BOD;
                            dLocal1.ReservaEQuipamentoTableAdapter.Update(rowREQ);
                            TableAdapter.CommitSQL();
                            rowREQ.AcceptChanges();
                        }
                        catch (Exception e1)
                        {
                            TableAdapter.VircatchSQL(e1);
                            strErro += VirExcepitionNeon.VirExceptionNeon.RelatorioDeErro(e1, false, true, false);
                        }
                    }
                    intGravarBodTipo++;
                }
            }
            catch (Exception e)
            {
                strErro += VirExcepitionNeon.VirExceptionNeon.RelatorioDeErro(e, false, true, false);
            }

            iTotal = iAtual = 0;
            strLocal = "Publica ReservaEQuipamento - Terminado";
            TimeSpan TempoTotal = DateTime.Now - dataInicio;
            if (strErro != "")
                strErro += Environment.NewLine;
            //*** MRC - INICIO (27/05/2016 12:00) ***
            //return string.Format("({0}.{1:000} s) Publica ReservaEQuipamento: {2} Recebidos: {3} Novos: {4} Alterados: {5} Antigos:{6} BODs:{7}\r\n{8}",
            return string.Format("({0}.{1:000} s) Publica ReservaEQuipamento: {2} Novos: {3} Alterados: {4} Antigos:{5} BODs:{6}\r\n{7}\r\n",
                                 TempoTotal.Seconds,
                                 TempoTotal.Milliseconds,
                                 strErro == "" ? "ok" : "ERRO",
                                 //nRec,
                                 nReqI,
                                 nReqU,
                                 nAntigos,
                                 nBODs,
                                 strErro);
            //*** MRC - TERMINO (27/05/2016 12:00) ***                         
        }

        /// <summary>
        /// Publica os equipamentos
        /// </summary>
        /// <returns></returns>
        public string PublicaEQuipamentos()
        {
            DateTime dataInicio = DateTime.Now;
            string strErro = "";
            int nEqI = 0;
            int nEqU = 0;
            bool recriarEx = false;
            try
            {
                strLocal = "Publica Equipamentos - Lendo dados";
                iTotal = 0;
                iAtual = 0;
                if (Cancelar)
                    throw new Exception("Cancelado aguarda LIBERAR");
                dLocal dLocal1 = new dLocal();
                dIntenet dIntenet = new dIntenet();
                dIntenet.EQuiPamentosRow RowRemota;
                string retorno;
                bool Inclusao;

                int alterados = dLocal1.EQuiPamentosTableAdapter.FillAtivos(dLocal1.EQuiPamentos);
                if (alterados != 0)
                {
                    strLocal = "Publica Equipamentos";
                    iTotal = alterados;
                    iAtual = 0;

                    dInternet1.EQuiPamentosTableAdapter.Fill(dInternet1.EQuiPamentos);

                    foreach (dLocal.EQuiPamentosRow rowEQP in dLocal1.EQuiPamentos)
                    {
                        if (Cancelar)
                            throw new Exception("Cancelado aguarda LIBERAR");
                        iAtual++;
                        //*** MRC - INICIO - RESERVA (18/03/2015 10:00) ***
                        RowRemota = dInternet1.EQuiPamentos.FindByEQPEQP_EMP(rowEQP.EQP, EMPArquivo);
                        //*** MRC - TERMINO - RESERVA (18/03/2015 10:00) ***
                        Inclusao = (RowRemota == null);
                        if (Inclusao)
                        {
                            RowRemota = dInternet1.EQuiPamentos.NewEQuiPamentosRow();
                            RowRemota.EQP_EMP = EMPArquivo;
                        }
                        foreach (DataColumn Coluna in dInternet1.EQuiPamentos.Columns)
                        {
                            if (Coluna.ColumnName != "EQP_EMP")
                                RowRemota[Coluna] = rowEQP[Coluna.ColumnName];
                        }
                        if (Inclusao)
                            dInternet1.EQuiPamentos.AddEQuiPamentosRow(RowRemota);
                        rowEQP.EQP__INTERNET = "L";
                        if (Inclusao)
                            nEqI++;
                        else
                            nEqU++;
                    }
                }

                try
                {
                    TableAdapter.AbreTrasacaoSQL("internet neonnet - 1271", dLocal1.EQuiPamentosTableAdapter);
                    dInternet1.EQuiPamentosTableAdapter.Update(dInternet1.EQuiPamentos);
                    dLocal1.EQuiPamentosTableAdapter.Update(dLocal1.EQuiPamentos);
                    TableAdapter.ST().Commit();
                }
                catch (Exception e)
                {
                    recriarEx = true;
                    //caso o Vircatch dipare uma excess�o ela deve ser recriada em nosso cath porque deve cancelar a transa��o m�e!
                    TableAdapter.ST().Vircatch(e);
                    recriarEx = false;
                    throw (e);
                }
                retorno = string.Format("Equipamentos: Alterados {0} - Inclu�dos: {1}\r\n", nEqU, nEqI);
                Retornos.Add(retorno);
            }
            catch (Exception e)
            {
                if (recriarEx)
                    throw (e);
                strErro += VirExcepitionNeon.VirExceptionNeon.RelatorioDeErro(e, false, true, false);
            }

            iTotal = iAtual = 0;
            strLocal = "Publica Equipamentos - Terminado";
            TimeSpan TempoTotal = DateTime.Now - dataInicio;
            return string.Format("({0}.{1:000} s) Publica EQuipamentos: {2} Alterados {3} - Inclu�dos: {4}\r\n{5}\r\n",
                                 TempoTotal.Seconds,
                                 TempoTotal.Milliseconds,
                                 strErro == "" ? "ok" : "ERRO",
                                 nEqU,
                                 nEqI,
                                 strErro);
        }

        //*** MRC - INICIO - RESERVA (28/05/2014 12:30) ***
        /// <summary>
        /// Publica as ReGrasEquipamento
        /// </summary>
        /// <returns></returns>
        public string PublicaReGrasEquipamento()
        {
            DateTime dataInicio = DateTime.Now;
            string strErro = "";
            int nREqI = 0;
            int nREqU = 0;
            bool recriarEx = false;
            try
            {
                strLocal = "Publica Regras dos Equipamentos - Lendo dados";
                iTotal = 0;
                iAtual = 0;
                if (Cancelar)
                    throw new Exception("Cancelado aguarda LIBERAR");
                dLocal dLocal1 = new dLocal();
                dIntenet dIntenet = new dIntenet();
                dIntenet.ReGrasEquipamentoRow RowRemota;
                string retorno;
                bool Inclusao;

                int alterados = dLocal1.ReGrasEquipamentoTableAdapter.FillAtivos(dLocal1.ReGrasEquipamento);
                if (alterados != 0)
                {
                    strLocal = "Publica Regras dos Equipamentos";
                    iTotal = alterados;
                    iAtual = 0;

                    dInternet1.ReGrasEQuipamentoTableAdapter.Fill(dInternet1.ReGrasEquipamento);

                    foreach (dLocal.ReGrasEquipamentoRow rowRGE in dLocal1.ReGrasEquipamento)
                    {
                        if (Cancelar)
                            throw new Exception("Cancelado aguarda LIBERAR");
                        iAtual++;
                        //*** MRC - INICIO - RESERVA (18/03/2015 10:00) ***
                        RowRemota = dInternet1.ReGrasEquipamento.FindByRGERGE_EMP(rowRGE.RGE, EMPArquivo);
                        Inclusao = (RowRemota == null);
                        if (Inclusao)
                        {
                            RowRemota = dInternet1.ReGrasEquipamento.NewReGrasEquipamentoRow();
                            RowRemota.RGE_EMP = EMPArquivo;
                        }
                        foreach (DataColumn Coluna in dInternet1.ReGrasEquipamento.Columns)
                        {
                            if (Coluna.ColumnName != "RGE_EMP")
                                RowRemota[Coluna] = rowRGE[Coluna.ColumnName];
                        }
                        //*** MRC - TERMINO - RESERVA (18/03/2015 10:00) ***
                        if (Inclusao)
                            dInternet1.ReGrasEquipamento.AddReGrasEquipamentoRow(RowRemota);
                        rowRGE.RGE__INTERNET = "L";
                        if (Inclusao)
                            nREqI++;
                        else
                            nREqU++;
                    }
                }

                try
                {
                    TableAdapter.AbreTrasacaoSQL("internet neonnet - 1371", dLocal1.ReGrasEquipamentoTableAdapter);
                    dInternet1.ReGrasEQuipamentoTableAdapter.Update(dInternet1.ReGrasEquipamento);
                    dLocal1.ReGrasEquipamentoTableAdapter.Update(dLocal1.ReGrasEquipamento);
                    TableAdapter.ST().Commit();
                }
                catch (Exception e)
                {
                    recriarEx = true;
                    //caso o Vircatch dipare uma excess�o ela deve ser recriada em nosso cath porque deve cancelar a transa��o m�e!
                    TableAdapter.ST().Vircatch(e);
                    recriarEx = false;
                    throw (e);
                }
                retorno = string.Format("Regras dos Equipamentos: Alterados {0} - Inclu�dos: {1}\r\n", nREqU, nREqI);
                Retornos.Add(retorno);
            }
            catch (Exception e)
            {
                if (recriarEx)
                    throw (e);
                strErro += VirExcepitionNeon.VirExceptionNeon.RelatorioDeErro(e, false, true, false);
            }

            iTotal = iAtual = 0;
            strLocal = "Publica Regras dos Equipamentos - Terminado";
            TimeSpan TempoTotal = DateTime.Now - dataInicio;
            return string.Format("({0}.{1:000} s) Publica ReGrasEquipamentos: {2} Alterados {3} - Inclu�dos: {4}\r\n{5}\r\n",
                                 TempoTotal.Seconds,
                                 TempoTotal.Milliseconds,
                                 strErro == "" ? "ok" : "ERRO",
                                 nREqU,
                                 nREqI,
                                 strErro);
        }
        //*** MRC - TERMINO - RESERVA (28/05/2014 12:30) ***

        /// <summary>
        /// Blocos
        /// </summary>
        /// <returns></returns>
        public string PublicaBlocos()
        {
            if (Cancelar)
                throw new Exception("Cancelado aguarda LIBERAR");
            dLocal dLocal1 = new dLocal();
            string retorno;
            bool Inclusao;
            dIntenet.BLOCOSRow RowRemota;
            int alterados = dLocal1.BLOCOSTableAdapter.Fill(dLocal1.BLOCOS);
            if (alterados == 0)
            {
                return "Bloco: Sem altera��es\r\n";
            };
            strLocal = "PublicaBlocos";
            iTotal = alterados;
            iAtual = 0;
            int nBlocoI = 0;
            int nBlocosU = 0;

            try
            {
                foreach (dLocal.BLOCOSRow BLORow in dLocal1.BLOCOS)
                {
                    if (Cancelar)
                        throw new Exception("Cancelado aguarda LIBERAR");
                    iAtual++;
                    if (dInternet1.BLOCOSTableAdapter.Fill(dInternet1.BLOCOS, EMPArquivo, BLORow.BLO) == 1)
                    {
                        RowRemota = dInternet1.BLOCOS[0];
                        Inclusao = false;
                    }
                    else
                    {
                        Inclusao = true;
                        RowRemota = dInternet1.BLOCOS.NewBLOCOSRow();
                        RowRemota.BLO_EMP = EMPArquivo;
                    }

                    foreach (DataColumn Coluna in dInternet1.BLOCOS.Columns)
                        if (Coluna.ColumnName != "BLO_EMP")
                            RowRemota[Coluna] = BLORow[Coluna.ColumnName];
                    if (Inclusao)
                        dInternet1.BLOCOS.AddBLOCOSRow(RowRemota);
                    BLORow.BLO__INTERNET = "L";

                    if (Inclusao)
                        nBlocoI++;
                    else
                        nBlocosU++;
                    dInternet1.BLOCOSTableAdapter.Update(RowRemota);
                    RowRemota.AcceptChanges();
                    dLocal1.BLOCOSTableAdapter.Update(BLORow);
                    BLORow.AcceptChanges();
                };
            }
            catch (Exception e)
            {
                retorno = string.Format("****Erro**** parcial - Blocos: alterados {0} - Inclu�dos {1}\r\n{2}\r\n", nBlocosU, nBlocoI, VirExcepitionNeon.VirExceptionNeon.RelatorioDeErro(e, false, true, false));
                Retornos.Add(retorno);
                return retorno;
            }
            retorno = string.Format("Blocos: alterados {0} - Inclu�dos {1}\r\n", nBlocosU, nBlocoI);
            Retornos.Add(retorno);
            strLocal = "--";
            return retorno;
        }

        /*
        /// <summary>
        /// Imobiliarias
        /// </summary>
        /// <returns></returns>
        public string PublicaImobiliarias()
        {
            dLocal dLocal1 = new dLocal();
            if (Cancelar)
                throw new Exception("Cancelado aguarda LIBERAR");

            string retorno = "";
            bool Inclusao;
            //string Novos = "";
            dIntenet.FORNECEDORESRow RowRemota;
            int alterados = dLocal1.FORNECEDORESTableAdapter.Fill(dLocal1.FORNECEDORES);
            if (alterados == 0)
            {
                return "Fornecedores: Sem altera��es\r\n";
            };
            strLocal = "PublicaImobiliarias";
            iTotal = alterados;
            iAtual = 0;
            int nImoI = 0;
            int nImoU = 0;
            
            try
            {
                foreach (dLocal.FORNECEDORESRow FRNRow in dLocal1.FORNECEDORES)
                {
                    if (Cancelar)
                        throw new Exception("Cancelado aguarda LIBERAR");
                    iAtual++;
                    if (dInternet1.FORNECEDORESTableAdapter.Fill(dInternet1.FORNECEDORES, EMPArquivo, FRNRow.FRN) == 1)
                    {
                        RowRemota = dInternet1.FORNECEDORES[0];
                        Inclusao = false;
                    }
                    else
                    {
                        Inclusao = true;
                        RowRemota = dInternet1.FORNECEDORES.NewFORNECEDORESRow();
                        RowRemota.FRN_EMP = EMPArquivo;
                    }

                    foreach (DataColumn Coluna in dInternet1.FORNECEDORES.Columns)
                        if (Coluna.ColumnName != "FRN_EMP")
                            RowRemota[Coluna] = FRNRow[Coluna.ColumnName];
                    if (Inclusao)
                        dInternet1.FORNECEDORES.AddFORNECEDORESRow(RowRemota);
                    FRNRow.FRN__INTERNET = "L";

                    if (Inclusao)
                        nImoI++;
                    else
                        nImoU++;
                    dInternet1.FORNECEDORESTableAdapter.Update(RowRemota);
                    RowRemota.AcceptChanges();
                    //*** MRC - INICIO (05/01/2016 13:30) ***
                    //dInternet1.FORNECEDORESTableAdapter.DeleteFRNxRAV(FRNRow.FRN);
                    //dLocal1.FRNxRAVTableAdapter.Fill(dLocal1.FRNxRAV, FRNRow.FRN);
                    //foreach (dLocal.FRNxRAVRow rowRAV in dLocal1.FRNxRAV)
                    //    dInternet1.FORNECEDORESTableAdapter.InsertFRNxRAV(rowRAV.FRN, rowRAV.RAV);
                    //*** MRC - TERMINO (05/01/2016 13:30) ***
                    dLocal1.FORNECEDORESTableAdapter.Update(FRNRow);
                    FRNRow.AcceptChanges();
                    //*** MRC - INICIO (05/01/2016 13:30) ***
                    //dLocal1.RamoAtiVidadeTableAdapter.Fill(dLocal1.RamoAtiVidade);
                    //foreach (dLocal.RamoAtiVidadeRow rowRAV in dLocal1.RamoAtiVidade)
                    //{
                    //    dIntenet.RamoAtiVidadeRow novaRow;
                    //    if (dInternet1.RamoAtiVidadeTableAdapter.Fill(dInternet1.RamoAtiVidade, rowRAV.RAV) > 0)
                    //        novaRow = dInternet1.RamoAtiVidade[0];
                    //    else
                    //    {
                    //        novaRow = dInternet1.RamoAtiVidade.NewRamoAtiVidadeRow();
                    //        novaRow.RAV = rowRAV.RAV;
                    //    }
                    //    novaRow.RAVDescricao = rowRAV.RAVDescricao;
                    //    if (novaRow.RowState == DataRowState.Detached)
                    //        dInternet1.RamoAtiVidade.AddRamoAtiVidadeRow(novaRow);
                    //    dInternet1.RamoAtiVidadeTableAdapter.Update(novaRow);
                    //    novaRow.AcceptChanges();
                    //    rowRAV.RAV__Internet = "L";
                    //}
                    //dLocal1.RamoAtiVidadeTableAdapter.Update(dLocal1.RamoAtiVidade);
                    //*** MRC - TERMINO (05/01/2016 13:30) ***
                };
            }
            catch (Exception e)
            {                
                retorno = string.Format("****Erro**** parcial - Imobiliarias: alteradas {0} - Inclu�das {1}\r\n{2}\r\n", nImoU, nImoI,VirExcepitionNeon.VirExceptionNeon.RelatorioDeErro(e,false,true,false));
                Retornos.Add(retorno);
                return retorno;
            }
            retorno = string.Format("Imobiliarias: alteradas {0} - Inclu�das {1}\r\n", nImoU, nImoI);
            Retornos.Add(retorno);
            strLocal = "--";
            return retorno;
        }*/


        /*
        /// <summary>
        /// PUBLICA OS CHEQUES
        /// </summary>
        /// <param name="max"></param>
        /// <returns></returns>
        public string PublicaCheques(int max)
        {
            if (Cancelar)
                throw new Exception("Cancelado aguarda LIBERAR");
            dLocal dLocal1 = new dLocal();
            string retorno;
            int cheu = 0;
            int chea = 0;
            int chep = 0;
            try
            {
                bool nova;

                dIntenet.CHEquesRow CHErowI;

                iTotal = dLocal1.CHEquesTableAdapter.FillcomData(dLocal1.CHEques);
                if (iTotal == 0)
                {
                    retorno = "Cheques: Sem altera��es\r\n";
                    return retorno;
                }
                strLocal = "PublicaCheques";
                int CHEMinimo = dLocal1.CHEques[0].CHE;
                dInternet1.CHEquesTableAdapter.Fill(dInternet1.CHEques, DSCentral.EMP, CHEMinimo);

                iAtual = 0;
                foreach (dLocal.CHEquesRow CHErow in dLocal1.CHEques)
                {
                    if (Cancelar)
                        throw new Exception("Cancelado aguarda LIBERAR");
                    iAtual++;
                    max--;
                    if (max == 0)
                        break;
                    if (CHErow.IsCHENumeroNull())
                        chep++;
                    else
                    {
                        dLocal1.DadosChequeTableAdapter.Fill(dLocal1.DadosCheque, CHErow.CHE);
                        CHErow.CHEdescritivo = "";
                        foreach (dLocal.DadosChequeRow rowch in dLocal1.DadosCheque)
                        {
                            string manobra = "";
                            if (rowch.NOA_PLA != "213000")
                                manobra = rowch.PLADescricao + " - ";
                            CHErow.CHEdescritivo += string.Format("{0}{1}<br />\r\n", manobra, rowch.NOAServico);
                        };
                        CHErowI = dInternet1.CHEques.FindByCHE_EMPCHE(DSCentral.EMP, CHErow.CHE);
                        if (CHErowI == null)
                        {
                            nova = true;
                            CHErowI = dInternet1.CHEques.NewCHEquesRow();
                        }
                        else
                            nova = false;


                        foreach (DataColumn DC in dInternet1.CHEques.Columns)
                            if ((DC.ColumnName != "CHE_EMP"))
                                CHErowI[DC] = CHErow[DC.ColumnName];

                        if (nova)
                        {
                            CHErowI.CHE_EMP = DSCentral.EMP;
                            dInternet1.CHEques.AddCHEquesRow(CHErowI);
                            chea++;
                        }
                        else
                            cheu++;
                    }
                    CHErow.CHE__INTERNET = "L";

                };
                dInternet1.CHEquesTableAdapter.Update(dInternet1.CHEques);
                dLocal1.CHEquesTableAdapter.Update(dLocal1.CHEques);
                retorno = string.Format("Cheques ({3:n0}) : a {0:n0} u {1:n0} p {2:n0}\r\n", chea, cheu, chep, iTotal);
                //Retornos.Add(retorno);
                return retorno;
            }
            catch (Exception e)
            {
                retorno = string.Format("****Erro**** Parcial - Cheques: a {0:n0} u {1:n0}\r\nDados do erro\r\n{2}", chea, cheu, VirExcepitionNeon.VirExceptionNeon.RelatorioDeErro(e, false, true, false));
                return retorno;
            }
            finally
            {
                strLocal = "--";
            }
        }*/

        /*
        /// <summary>
        /// Publica extratos na internet
        /// </summary>
        /// <returns></returns>
        public string PublicaExtratos()
        {
            dLocal dLocal1 = new dLocal();
            int pendentes = 0;
            TableAdapter.ST().BuscaEscalar("SELECT COUNT(SCC) FROM SaldoContaCorrente WHERE (SCC__Internet = 'U')", out pendentes);
            if (pendentes == 0)
                return "Extrato: Sem altera��es - CANCELADO";
            if (Cancelar)
                throw new Exception("Cancelado aguarda LIBERAR");
            strLocal = "PublicaExtratos";
            string retorno = "";
            int sccu = 0;
            int scca = 0;
            int ccdu = 0;
            int ccda = 0;
            try
            {
                dInternet1.ContaCorrenteDetalheTableAdapter.Apagar(DateTime.Today.AddMonths(-3));
                dInternet1.SaldoContaCorrenteTableAdapter.Apaga(DateTime.Today.AddMonths(-3));
                dInternet1.CHEquesTableAdapter.Apagar(DateTime.Today.AddMonths(-3));
                dInternet1.ContaCorrenteDetalhe.Clear();
                dInternet1.SaldoContaCorrente.Clear();
                dInternet1.ContaCorrenTe.Clear();



                //int i = 0;
                bool nova;
                //CCTs                
                int cctu = 0;
                int ccta = 0;
                dIntenet.ContaCorrenTeRow CCTrowI;

                int nCCT = dLocal1.ContaCorrenTeTableAdapter.Fill(dLocal1.ContaCorrenTe);
                if (nCCT > 0)
                {
                    iTotal = dInternet1.ContaCorrenTeTableAdapter.FillByEMP(dInternet1.ContaCorrenTe, DSCentral.EMP);
                    iAtual = 0;
                    foreach (dLocal.ContaCorrenTeRow CCTrow in dLocal1.ContaCorrenTe)
                    {
                        iAtual++;
                        if (Cancelar)
                            throw new Exception("Cancelado aguarda LIBERAR");
                        CCTrowI = dInternet1.ContaCorrenTe.FindByCCT_EMPCCT(DSCentral.EMP, CCTrow.CCT);
                        if (CCTrowI == null)
                        {
                            nova = true;
                            CCTrowI = dInternet1.ContaCorrenTe.NewContaCorrenTeRow();
                        }
                        else
                            nova = false;
                        foreach (DataColumn DC in dInternet1.ContaCorrenTe.Columns)
                            if (DC.ColumnName != "CCT_EMP")
                                CCTrowI[DC] = CCTrow[DC.ColumnName];
                        if (nova)
                        {
                            CCTrowI.CCT_EMP = DSCentral.EMP;
                            dInternet1.ContaCorrenTe.AddContaCorrenTeRow(CCTrowI);
                            ccta++;
                        }
                        else
                            cctu++;
                        CCTrow.CCT__Internet = "L";
                    };
                    dInternet1.ContaCorrenTeTableAdapter.Update(dInternet1.ContaCorrenTe);
                    dLocal1.ContaCorrenTeTableAdapter.Update(dLocal1.ContaCorrenTe);
                };

                retorno = string.Format("{0,18}: a {1:n0} u {2:n0}\r\n", "ContaCorrenTe", ccta, cctu);
                //Fim CCTs



                // SCCs - CCD

                dIntenet.SaldoContaCorrenteRow SCCrowI;
                dIntenet.ContaCorrenteDetalheRow CCDrowI;

                // antecipado
                iTotal = dLocal1.SaldoContaCorrenteTableAdapter.FillcomPLUS(dLocal1.SaldoContaCorrente);
                if (iTotal == 0)
                {
                    retorno += "Extrato: Sem altera��es";
                    return retorno;
                };
                iAtual = 0;
                dLocal1.ContaCorrenteDetalheTableAdapter.FillBy(dLocal1.ContaCorrenteDetalhe);
                DateTime DataCorte = dLocal1.SaldoContaCorrente[0].SCCData;
                dInternet1.SaldoContaCorrenteTableAdapter.FillByDataCorte(dInternet1.SaldoContaCorrente, DSCentral.EMP, DataCorte);
                dInternet1.ContaCorrenteDetalheTableAdapter.FillByDataCorte(dInternet1.ContaCorrenteDetalhe, DataCorte, DSCentral.EMP);
                foreach (dLocal.SaldoContaCorrenteRow SCCrow in dLocal1.SaldoContaCorrente)
                {
                    iAtual++;
                    if (Cancelar)
                        throw new Exception("Cancelado aguarda LIBERAR");
                    SCCrowI = dInternet1.SaldoContaCorrente.FindBySCC_EMPSCC(DSCentral.EMP, SCCrow.SCC);
                    if (SCCrowI == null)
                    {
                        nova = true;
                        SCCrowI = dInternet1.SaldoContaCorrente.NewSaldoContaCorrenteRow();
                    }
                    else
                        nova = false;
                    if (SCCrow.IsCCT_CCTPlusNull())
                    {
                        SCCrowI.SCCValorI = SCCrow.SCCValorI + SCCrow.SCCValorApI;
                        SCCrowI.SCCValorF = SCCrow.SCCValorF + SCCrow.SCCValorApF;
                    }
                    else
                    {
                        SCCrowI.SCCValorI = SCCrow.SCCValorI;
                        SCCrowI.SCCValorF = SCCrow.SCCValorF;
                    }
                    foreach (DataColumn DC in dInternet1.SaldoContaCorrente.Columns)
                        if (!DC.ColumnName.EstaNoGrupo("SCC_EMP", "SCCValorI", "SCCValorF"))
                            SCCrowI[DC] = SCCrow[DC.ColumnName];
                    if (nova)
                    {
                        SCCrowI.SCC_EMP = DSCentral.EMP;
                        dInternet1.SaldoContaCorrente.AddSaldoContaCorrenteRow(SCCrowI);
                        scca++;
                    }
                    else
                        sccu++;
                    SCCrow.SCC__Internet = "L";

                    //CCDs
                    foreach (dLocal.ContaCorrenteDetalheRow CCDrow in SCCrow.GetContaCorrenteDetalheRows())
                    {
                        CCDrowI = dInternet1.ContaCorrenteDetalhe.FindByCCD_EMPCCD(DSCentral.EMP, CCDrow.CCD);
                        if ((!CCDrow.IsCCDTipoLancamentoNull()) && (CCDrow.CCDTipoLancamento == (int)Enumeracoes.TipoLancamentoNeon.TransferenciaInterna))
                        {
                            if (CCDrowI != null)
                                CCDrowI.Delete();
                        }
                        else
                        {
                            if (CCDrowI == null)
                            {
                                nova = true;
                                CCDrowI = dInternet1.ContaCorrenteDetalhe.NewContaCorrenteDetalheRow();
                            }
                            else
                                nova = false;

                            foreach (DataColumn DC in dInternet1.ContaCorrenteDetalhe.Columns)
                                if (DC.ColumnName != "CCD_EMP")
                                    CCDrowI[DC] = CCDrow[DC.ColumnName];
                            if (nova)
                            {
                                CCDrowI.CCD_EMP = DSCentral.EMP;
                                dInternet1.ContaCorrenteDetalhe.AddContaCorrenteDetalheRow(CCDrowI);
                                ccda++;
                            }
                            else
                                ccdu++;
                        }
                    };
                    dInternet1.SaldoContaCorrenteTableAdapter.Update(dInternet1.SaldoContaCorrente);
                    dInternet1.ContaCorrenteDetalheTableAdapter.Update(dInternet1.ContaCorrenteDetalhe);
                    dLocal1.SaldoContaCorrenteTableAdapter.Update(dLocal1.SaldoContaCorrente);

                };
                //dInternet1.SaldoContaCorrenteTableAdapter.Update(dInternet1.SaldoContaCorrente);
                //dInternet1.ContaCorrenteDetalheTableAdapter.Update(dInternet1.ContaCorrenteDetalhe);
                //dLocal1.SaldoContaCorrenteTableAdapter.Update(dLocal1.SaldoContaCorrente);
                retorno += string.Format("{0,20}: a {1:n0} u {2:n0}\r\n", "SaldoContaCorrente", scca, sccu);
                retorno += string.Format("{0,20}: a {1:n0} u {2:n0}\r\n", "ContaCorrenteDetalhe", ccda, ccdu);
                // FIM SCCs - CCD

                //Retornos.Add(retorno);
                return retorno;
            }
            catch (Exception e)
            {
                retorno += string.Format("{0,20} (parcial): a {1:n0} u {2:n0} pendentes {3:n0}\r\n", "SaldoContaCorrente", scca, sccu, iTotal - iAtual);
                retorno += string.Format("{0,20} (parcial): a {1:n0} u {2:n0}\r\n", "ContaCorrenteDetalhe", ccda, ccdu);
                return string.Format("ERRO:\r\n{0}\r\nParcial:{1}", VirExcepitionNeon.VirExceptionNeon.RelatorioDeErro(e, false, true, false), retorno);
            }
            finally
            {
                strLocal = "--";
            }
        }*/


        /// <summary>
        /// Processa a Francesinha
        /// </summary>
        /// <returns></returns>
        public string ProcessaFrancesinha(dllbanco.Francesinha.TipoRetorno TipoRetorno = dllbanco.Francesinha.TipoRetorno.resumo)
        {
            dllbanco.Francesinha Fr = new dllbanco.Francesinha();
            Fr.DivideSuperBoleto();
            Fr.Carregar();            
            return Fr.ProcessarDados(TipoRetorno);
        }

    

        /// <summary>
        /// 
        /// </summary>
        /// <param name="Titulo"></param>
        /// <param name="teto"></param>
        /// <param name="pasta"></param>
        /// <returns></returns>
        public string EmiteChequeEletronico(string Titulo, int teto,string pasta)
        {
            try
            {
                strLocal = Titulo;
                iTotal = 0;
                iAtual = 0;
                return new EDIBoletosNeon.Arquivo().RemessaCheque(ref iTotal, ref iAtual, teto, pasta);                 
            }
            finally
            {
                strLocal = string.Format("{0} - Terminado", Titulo);
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="Titulo"></param>
        /// <param name="Teto"></param>
        /// <param name="Pasta"></param>
        /// <param name="Tipo"></param>
        /// <returns></returns>
        public string RemessaTributo(string Titulo,int Teto,string Pasta, OrigemImposto Tipo)
        {            
            try
            {
                strLocal = Titulo;
                iTotal = 0;
                iAtual = 0;
                return new EDIBoletosNeon.Arquivo().RemessaTributo(ref iTotal, ref iAtual, Teto, Pasta, Tipo);                
            }
            finally
            {
                strLocal = string.Format("{0} - Terminado", Titulo);
            }
        }

        /// <summary>
        /// Publica os apartamentos
        /// </summary>
        /// <returns></returns>
        public string PublicaApartamentos(int EMP)
        {
            dLocal dLocal1 = new dLocal();
            string TESTEPROX = "(0)";

            strLocal = "PublicaApartamentos";
            if (Cancelar)
                throw new Exception("Cancelado aguarda LIBERAR");
            string retorno;

            int nAPARTAMENTOSI = 0;
            int nAPARTAMENTOSU = 0;
            int nAPARTAMENTOSErro = 0;
            iTotal = 0;
            string Errosstr = "";


            TESTEPROX += "(1)";
            if (WS.Proxy == null)
                TESTEPROX += "proxnull";
            else
                TESTEPROX += WS.Proxy.ToString();
            //dBancoLido = WS.LeClientes(Criptografa());
            strLocal = "PublicaApartamentos - Carregando 1";
            //cRowsCli.Carrega(dBancoLido);

            bool Inclusao;
            strLocal = "PublicaApartamentos - Carregando 2";
            iTotal = dLocal1.APARTAMENTOSTableAdapter.FillcomCONBLO(dLocal1.APARTAMENTOS, EMP);
            strLocal = "PublicaApartamentos - Carregando 3";
            dInternet1.APARTAMENTOSTableAdapter.FillBy(dInternet1.APARTAMENTOS, EMPArquivo);
            iAtual = 0;
            strLocal = "PublicaApartamentos - Loop";

            foreach (dLocal.APARTAMENTOSRow APARTAMENTOSRow in dLocal1.APARTAMENTOS)
            {
                iAtual++;
                dIntenet.APARTAMENTOSRow RowRemota = dInternet1.APARTAMENTOS.FindByAPT_EMPAPT(EMPArquivo, APARTAMENTOSRow.APT);
                Inclusao = (RowRemota == null);
                if (Inclusao)
                {
                    RowRemota = dInternet1.APARTAMENTOS.NewAPARTAMENTOSRow();
                    RowRemota.APT_EMP = EMPArquivo;
                }
                foreach (DataColumn Coluna in dInternet1.APARTAMENTOS.Columns)
                    if (Coluna.ColumnName != "APT_EMP")
                        RowRemota[Coluna] = APARTAMENTOSRow[Coluna.ColumnName];
                if (Inclusao)
                    dInternet1.APARTAMENTOS.AddAPARTAMENTOSRow(RowRemota);
                dInternet1.APARTAMENTOSTableAdapter.Update(RowRemota);
                

                //
                //

                //antigo (tabela cliente do site)
                bool Proprietario = true;
                for (int i = 1; i < 3; i++)
                {
                    //cRowsCli RowsCli = cRowsCli.BuscaRegistro(APARTAMENTOSRow.CONCodigo, APARTAMENTOSRow.BLOCodigo, APARTAMENTOSRow.APTNumero, Proprietario, true);

                    bool Remover = Proprietario ? APARTAMENTOSRow.IsAPTProprietario_PESNull() : APARTAMENTOSRow.IsAPTInquilino_PESNull();
                    if (Remover)
                    {
                        //if (RowsCli != null)
                        //    RowsCli.Delete();
                    }
                    else
                    {
                        //bool Novo = false;
                        //if (RowsCli == null)
                        //    RowsCli = new cRowsCli(APARTAMENTOSRow.CONCodigo, APARTAMENTOSRow.BLOCodigo, APARTAMENTOSRow.APTNumero, Proprietario);


                        int PES = Proprietario ? APARTAMENTOSRow.APTProprietario_PES : APARTAMENTOSRow.APTInquilino_PES;
                        dLocal.PESSOASRow rowPES = dLocal1.PESSOASTableAdapter.GetDataByPES(PES)[0];

                        //RowsCli.rowAtiva.Nome = rowPES.PESNome;

                        string Usuario;
                        int Tentativa;

                        if (Proprietario)
                        {
                            if (APARTAMENTOSRow.IsAPTUsuarioInternetProprietarioNull() || (APARTAMENTOSRow.APTUsuarioInternetProprietario == ""))
                            {
                                Tentativa = 1;
                                string strBase = (rowPES.PESNome.Length >= 6) ? rowPES.PESNome.Substring(0, 6).Trim().ToUpper() : rowPES.PESNome.Trim().ToUpper();
                                if (strBase.Contains(" "))
                                    strBase = strBase.Substring(0, strBase.IndexOf(" "));

                                do
                                {
                                    Usuario = string.Format("{0}{1}{2}", EMPArquivo, strBase, Tentativa);
                                    Tentativa++;
                                }
                                while (TableAdapter.ST().EstaCadastrado("select APT from AParTamentos where APTUsuarioInternetProprietario = @P1 or APTUsuarioInternetInquilino  = @P1", Usuario));
                                //MessageBox.Show(Usuario);
                                APARTAMENTOSRow.APTUsuarioInternetProprietario = Usuario;
                                APARTAMENTOSRow.APTSenhaInternetProprietario = string.Format("{0:0000}", (777.76743M * (rowPES.PES + APARTAMENTOSRow.APT)) % 10000);
                            };
                            //RowsCli.rowAtiva.Username = APARTAMENTOSRow.APTUsuarioInternetProprietario;
                            //RowsCli.rowAtiva.Senha = APARTAMENTOSRow.APTSenhaInternetProprietario;

                            //if (!APARTAMENTOSRow.IsAPTFormaPagtoInquilino_FPGNull())
                            //    RowsCli.rowAtiva.Correio = APARTAMENTOSRow.APTFormaPagtoProprietario_FPG <= 2;
                            //if (!APARTAMENTOSRow.IsAPTBEmailPNull())
                            //    RowsCli.rowAtiva.bemail = APARTAMENTOSRow.APTBEmailP;
                            //else
                            //    RowsCli.rowAtiva.bemail = false;

                        }
                        else
                        {
                            if (APARTAMENTOSRow.IsAPTUsuarioInternetInquilinoNull() || (APARTAMENTOSRow.APTUsuarioInternetInquilino == ""))
                            {
                                Tentativa = 1;
                                string strBase = rowPES.PESNome.Substring(0, 6).Trim().ToUpper();
                                if (strBase.Contains(" "))
                                    strBase = strBase.Substring(0, strBase.IndexOf(" "));
                                do
                                {
                                    Usuario = string.Format("{0}{1}{2}", EMPArquivo, strBase, Tentativa);
                                    Tentativa++;
                                }
                                while (TableAdapter.ST().EstaCadastrado("select APT from AParTamentos where APTUsuarioInternetProprietario = @P1 or APTSenhaInternetInquilino  = @P1", Usuario));
                                APARTAMENTOSRow.APTUsuarioInternetInquilino = Usuario;
                                APARTAMENTOSRow.APTSenhaInternetInquilino = string.Format("{0:0000}", (777.76743M * (rowPES.PES + APARTAMENTOSRow.APT)) % 10000);
                            };
                            //RowsCli.rowAtiva.Username = APARTAMENTOSRow.APTUsuarioInternetInquilino;
                            //RowsCli.rowAtiva.Senha = APARTAMENTOSRow.APTSenhaInternetInquilino;

                            //if (!APARTAMENTOSRow.IsAPTFormaPagtoInquilino_FPGNull())
                            //    RowsCli.rowAtiva.Correio = APARTAMENTOSRow.APTFormaPagtoInquilino_FPG <= 2;
                            //if (!APARTAMENTOSRow.IsAPTBEmailINull())
                            //    RowsCli.rowAtiva.bemail = APARTAMENTOSRow.APTBEmailI;
                            //else
                            //    RowsCli.rowAtiva.bemail = false;

                        };
                        //rowAPTLido.CODCON = APARTAMENTOSRow.CONCodigo;
                        //rowAPTLido.Apartamento = APARTAMENTOSRow.APTNumero;
                        //rowAPTLido.bloco = APARTAMENTOSRow.BLOCodigo;


                    };
                    Proprietario = false;

                };
            };


            
            

            dLocal1.SindicosTableAdapter.Fill(dLocal1.Sindicos);



            try
            {
                TableAdapter.AbreTrasacaoSQL("internet neonnet - 1830", dLocal1.APARTAMENTOSTableAdapter);
                dLocal1.APARTAMENTOSTableAdapter.Update(dLocal1.APARTAMENTOS);
                TableAdapter.ST().Commit();
            }
            catch (Exception e)
            {
                TableAdapter.ST().Vircatch(e);
                retorno = string.Format("Apartamentos: *****Erro******{3} Resultado parcial: alterados {0} - Inclu�dos: {1}\r\nDados do erro\r\n{2}\r\n", nAPARTAMENTOSU, nAPARTAMENTOSI, VirExcepitionNeon.VirExceptionNeon.RelatorioDeErro(e, false, true, false), TESTEPROX);
                Retornos.Add(retorno);
                return retorno;
            }
            retorno = string.Format("Apartamentos: alterados {0} - Inclu�dos: {1} Erros:{2}\r\n", nAPARTAMENTOSU, nAPARTAMENTOSI, nAPARTAMENTOSErro);
            if (nAPARTAMENTOSErro > 0)
            {
                retorno += string.Format("ERROS NO ACCESS ({0}){1}\r\n", nAPARTAMENTOSErro, Errosstr);
            }
            Retornos.Add(retorno);
            strLocal = "--";
            return retorno;
        }

        
        /// <summary>
        /// Gera remessa de registro
        /// </summary>
        /// <param name="Titulo"></param>
        /// <param name="Caminho"></param>
        /// <param name="maximo"></param>
        /// <param name="Banco"></param>
        /// <returns></returns>        
        public string PublicaRegistroBoletos(string Titulo, string Caminho, int maximo, int Banco)        
        {
            try
            {
                strLocal = Titulo;
                EDIBoletosNeon.Arquivo EDIBoletos = new EDIBoletosNeon.Arquivo();                
                string retorno = EDIBoletos.Remessa(maximo, Caminho, ref iTotal, ref iAtual, Banco);                
                strLocal = string.Format("Terminado: {0}",Titulo);
                return retorno;
            }
            catch (Exception ex)
            {
                return VirExcepitionNeon.VirExceptionNeon.RelatorioDeErro(ex, false, true, false);
            }
        }

        private class Tabela_Limpar
        {
            public string Tabela;
            public string ColunaID;
            public bool comEMP;
            public int Apagados;
            public Tabela_Limpar(string Tabela, string ColunaID,bool comEMP = true)
            { 
                this.Tabela = Tabela;
                this.ColunaID = ColunaID;
                this.comEMP = comEMP;
                Apagados = 0;
            }
            public void Apagar(int Chave, int EMPArquivo)
            {
                if (comEMP)
                    TableAdapter.ST(VirDB.Bancovirtual.TiposDeBanco.Internet2).ExecutarSQLNonQuery(string.Format("delete from {0} where {1} = @P1 and {1}_EMP = @P2", Tabela, ColunaID), Chave, EMPArquivo);
                else
                    TableAdapter.ST(VirDB.Bancovirtual.TiposDeBanco.Internet2).ExecutarSQLNonQuery(string.Format("delete from {0} where {1} = @P1", Tabela, ColunaID), Chave);
                TableAdapter.ST().ExecutarSQLNonQuery("delete from apagar where tabela = @P1 and Chave = @P2", ColunaID, Chave);
                Apagados++;
            }
        };

        /// <summary>
        /// Apaga os marcados para apagar
        /// </summary>
        /// <returns></returns>
        public string Apagar()
        {
            DateTime dataInicio = DateTime.Now;
            string strErro = "";
            SortedList<string, Tabela_Limpar> Tabelas = new SortedList<string, Tabela_Limpar>();
            Tabelas.Add("CHE", new Tabela_Limpar("CHEques", "CHE"));
            Tabelas.Add("CCD", new Tabela_Limpar("ContaCorrenteDetalhe", "CCD"));
            Tabelas.Add("CDR", new Tabela_Limpar("CorpoDiretivo", "CDR"));
            Tabelas.Add("SCC", new Tabela_Limpar("SaldoContaCorrente", "SCC"));
            //*** MRC - INICIO (05/01/2016 13:30) ***
            //Tabelas.Add("RAV", new Tabela_Limpar("RamoAtiVidade", "RAV"));
            //*** MRC - TERMINO (05/01/2016 13:30) ***
            Tabelas.Add("FRN", new Tabela_Limpar("FORNECEDORES", "FRN"));
            Tabelas.Add("EQP", new Tabela_Limpar("EQuiPamentos", "EQP"));
            //*** MRC - INICIO - RESERVA (31/03/2015 21:30) ***
            Tabelas.Add("REQ", new Tabela_Limpar("ReservaEQuipamento", "REQ"));
            //*** MRC - INICIO - RESERVA (28/05/2014 12:30) ***
            Tabelas.Add("RGE", new Tabela_Limpar("ReGrasEquipamento", "RGE"));
            //*** MRC - TERMINO - RESERVA (28/05/2014 12:30) ***
            //*** MRC - TERMINO - RESERVA (31/03/2015 21:30) ***
            try
            {
                strLocal = "Apagar";
                iTotal = 0;
                iAtual = 0;
                if (Cancelar)
                    throw new Exception("Cancelado aguarda LIBERAR");
                dLocal dLocal1 = new dLocal();


                iTotal = dLocal1.ApagarTableAdapter.Fill(dLocal1.Apagar);
                if (iTotal != 0)
                {
                    foreach (dLocal.ApagarRow arow in dLocal1.Apagar)
                    {
                        if (Cancelar)
                            throw new Exception("Cancelado aguarda LIBERAR");
                        iAtual++;
                        if (Tabelas.ContainsKey(arow.Tabela))
                            Tabelas[arow.Tabela].Apagar(arow.Chave, EMPArquivo);
                        else
                            strErro += string.Format("Tabela n�o cadastrada: {0}", arow.Tabela);

                    };
                }
            }
            catch (Exception e)
            {
                strErro += VirExcepitionNeon.VirExceptionNeon.RelatorioDeErro(e, false, true, false);
            }

            iTotal = iAtual = 0;
            strLocal = "Apaga - Terminado";
            TimeSpan TempoTotal = DateTime.Now - dataInicio;
            string Contagem = "";
            foreach (Tabela_Limpar Tab in Tabelas.Values)
                if (Tab.Apagados > 0)
                    Contagem += string.Format("{0}: {1} ", Tab.Tabela, Tab.Apagados);
            return string.Format("({0}.{1:000} s) Apagar: {2} {3}\r\n{4}\r\n",
                                 TempoTotal.Seconds,
                                 TempoTotal.Milliseconds,
                                 strErro == "" ? "ok" : "ERRO",
                                 Contagem == "" ? "sem registros" : Contagem,
                                 strErro);
        }

        


        
        /*
        /// <summary>
        /// Recebe todos os cadastros
        /// </summary>
        /// <returns></returns>                
        public string RecebCadastros(){
            return "Rec Cadastros desativado";
            Relatorio = "";
            int ok = 0;
            int erro = 0;
            try
            {
                strLocal = "RecebCadastros";
                if (Cancelar)
                    throw new Exception("Cancelado aguarda LIBERAR");
                iTotal = dInternet1.RetClienteTableAdapter.Fill(dInternet1.RetCliente, (short)((EMPArquivo == 3) ? 2 : EMPArquivo));
                iAtual = 0;
                
                System.Collections.ArrayList Apagar = new System.Collections.ArrayList();
                foreach (dIntenet.RetClienteRow RetClienteRow in dInternet1.RetCliente)
                {
                    if (Cancelar)
                        throw new Exception("Cancelado aguarda LIBERAR");
                    if (EfetuaCadastro(RetClienteRow, false))
                    {
                        ok++;
                        Apagar.Add(RetClienteRow);
                    }
                    else
                        erro++;
                    iAtual++;
                }
                foreach (dIntenet.RetClienteRow RetClienteRowAp in Apagar)
                {
                    RetClienteRowAp.Delete();
                    dInternet1.RetClienteTableAdapter.Update(RetClienteRowAp);
                };
                
            }
            catch (Exception e)
            {
                Relatorio += string.Format("****Erro****\r\n{0}\r\nResultado parcial - ", VirExcepitionNeon.VirExceptionNeon.RelatorioDeErro(e, false, true, false));
            }
            strLocal = "-";
            return Relatorio + string.Format("Rec Cadastros: ok {0} erro {1}{2}\r\n", ok, erro, Relatorio);
        }*/

        /// <summary>
        /// Recebe todos os acordos
        /// </summary>
        /// <returns></returns>
        public string RecebAcordos()
        {
            int ok = 0;
            int erro = 0;
            Relatorio = "";
            strLocal = "RecebAcordos";
            try
            {
                if (Cancelar)
                    throw new Exception("Cancelado aguarda LIBERAR");

                iTotal = dInternet1.RetAcordoTableAdapter.Fill(dInternet1.RetAcordo, (short)((EMPArquivo == 3) ? 2 : EMPArquivo));
                dInternet1.RetNovosBTableAdapter.FillBy(dInternet1.RetNovosB, (short)((EMPArquivo == 3) ? 2 : EMPArquivo));
                dInternet1.RetOriginaisTableAdapter.FillBy(dInternet1.RetOriginais, (short)((EMPArquivo == 3) ? 2 : EMPArquivo));
                iAtual = 0;

                foreach (dIntenet.RetAcordoRow RetAcordoRow in dInternet1.RetAcordo)
                {
                    if (Cancelar)
                        throw new Exception("Cancelado aguarda LIBERAR");

                    Relatorio += "Acordo: " + RetAcordoRow.codcon + " - " + RetAcordoRow.bloco + " - " + RetAcordoRow.apartamento + "\r\n";
                    try
                    {
                        if (EfetuaAcordo(RetAcordoRow))
                            ok++;
                        else
                            erro++;
                    }
                    catch (Exception e)
                    {
                        erro++;
                        Relatorio += string.Format("\r\nACORDO 1 ****Erro****\r\n{0}\r\nResultado parcial - ", VirExcepitionNeon.VirExceptionNeon.RelatorioDeErro(e, false, true, false));
                    }
                }
                strLocal = "-";
                return string.Format("Rec Acordos: lidos:{3} ok {0} erro {1}{2}\r\n", ok, erro, (erro == 0) ? "" : "\r\n" + Relatorio, iTotal);

            }
            catch (Exception e1)
            {
                Relatorio += string.Format("\r\nACORDO 2 ****Erro****\r\n{0}\r\nResultado parcial - ", VirExcepitionNeon.VirExceptionNeon.RelatorioDeErro(e1, false, true, false));
                return Relatorio;
            }
            
        }

        private string URLServidorProcessos
        {
            get
            {
                switch (ClienteServProc.TipoServidoSelecionado)
                {
                    case TiposServidorProc.Local:
                        return "WCFPeriodicoNotaL";
                    case TiposServidorProc.QAS:
                        return "WCFPeriodicoNotaH";
                    case TiposServidorProc.Producao:
                        return "WCFPeriodicoNotaP";
                    case TiposServidorProc.Indefinido:
                    case TiposServidorProc.Simulador:
                    default:
                        return "";
                }
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public string Teste_WS()
        {
            SPeriodicoNota.PeriodicoNotaClient PeriodicoNotaClient1 = new SPeriodicoNota.PeriodicoNotaClient(URLServidorProcessos);
            RetornoServidorProcessos Ret = PeriodicoNotaClient1.Teste(VirCrip.VirCripWS.Crip(DSCentral.ChaveCriptografar(EMPArquivo, DSCentral.USU)));
            return Ret.Mensagem;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="Teto"></param>
        /// <returns></returns>
        public string RevisaPeriodicos_WS(int Teto = 0)
        {
            DateTime dataInicio = DateTime.Now;
            DateTime dataTESTE = DateTime.Now;
            strLocal = "Verificando Peri�dicos WS";
            int ok = 0;
            int erro = 0;
            int Ajustados = 0;
            StringBuilder SBRelatorio = new StringBuilder();
            iAtual = 0;
            dPagamentosPeriodicos dPagamentosPeriodicos1 = new dPagamentosPeriodicos();
            dPagamentosPeriodicos1.CarregaAtivos(false);            
            iTotal = dPagamentosPeriodicos1.PaGamentosFixos.Count;
            SPeriodicoNota.PeriodicoNotaClient PeriodicoNotaClient1 = new SPeriodicoNota.PeriodicoNotaClient(URLServidorProcessos);
            foreach (dPagamentosPeriodicos.PaGamentosFixosRow rowPGF in dPagamentosPeriodicos1.PaGamentosFixos)
            {
                iAtual++;
                if ((Teto != 0) && (iAtual > Teto))
                    break;
                try
                {
                    RetornoServidorProcessos Ret = PeriodicoNotaClient1.periodico_CadastrarProximo(VirCrip.VirCripWS.Crip(DSCentral.ChaveCriptografar(EMPArquivo, DSCentral.USU)), rowPGF.PGF);
                    if (Ret.ok)
                    {
                        if(Ret.Mensagem.StartsWith("Retorno ok"))
                        {
                            Ajustados++;
                            SBRelatorio.AppendFormat("\r\n Ajustado PGF = {0}", rowPGF.PGF);
                        }
                        else
                        {
                            ok++;
                        }
                    }
                    else
                    {
                        erro++;
                        SBRelatorio.AppendFormat("\r\nPGF = {0} ****Erro****\r\n{1}\r\n", rowPGF.PGF, Ret.Mensagem);
                    }
                }
                catch (Exception ex)
                {
                    erro++;
                    SBRelatorio.AppendFormat("\r\nWS PGF = {0} ****Erro****\r\n{1}\r\n", rowPGF.PGF, VirExcepitionNeon.VirExceptionNeon.RelatorioDeErro(ex, false, true, false));
                }
                
                dataTESTE = DateTime.Now;
                //Console.WriteLine(string.Format("PGF {0} - {1}/{2}", rowPGF.PGF, iAtual, iTotal));
                if (erro > 20)
                {
                    SBRelatorio.AppendFormat("\r\n****Cancelado: Erro****\r\n\r\n");
                    break;
                }

            }
            TimeSpan TempoTotal = DateTime.Now - dataInicio;
            strLocal = "Terminado - V. Peri�dicos WS";
            return string.Format("({0:n0}.{1:000} s) Peri�dicos: lidos:{2} ok {3} ajustados:{4} erros:{5}\r\n{6}\r\n",
                                                          TempoTotal.TotalSeconds,
                                                          TempoTotal.Milliseconds,
                                                          iTotal,
                                                          ok,
                                                          Ajustados,
                                                          erro,
                                                          SBRelatorio.ToString());
        }

        /// <summary>
        /// Faz a revisao do peri�dicos
        /// </summary>
        /// <returns></returns>
        public string RevisaPeriodicos()
        {
            DateTime dataInicio = DateTime.Now;
            DateTime dataTESTE = DateTime.Now; 
            strLocal = "Verificando Peri�dicos";
            int ok = 0;
            int erro = 0;
            int Ajustados = 0;
            Relatorio = "";
            iAtual = 0;
            dPagamentosPeriodicos dPagamentosPeriodicos1 = new dPagamentosPeriodicos();
            dPagamentosPeriodicos1.CarregaAtivos(true);
            //DataTable DT = TableAdapter.ST().BuscaSQLTabela("select PGF FROM PaGamentosFixos INNER JOIN CONDOMINIOS ON PaGamentosFixos.PGF_CON = CONDOMINIOS.CON WHERE (CONStatus = 1) AND (PGFStatus IN (1, 2))");
            iTotal = dPagamentosPeriodicos1.PaGamentosFixos.Count;
            foreach (dPagamentosPeriodicos.PaGamentosFixosRow rowPGF in dPagamentosPeriodicos1.PaGamentosFixos)
            {
                iAtual++;
                ContaPagar.Follow.PagamentoPeriodico periodico = new ContaPagar.Follow.PagamentoPeriodico(rowPGF);

                try
                {
                    //if (rowPGF.PGF == 2604)
                    //    Console.WriteLine("Checar");
                    if (periodico.CadastrarProximo())
                    {                        
                        Ajustados++;
                        Relatorio += string.Format("\r\nAjustado PGF = {0}", rowPGF.PGF);
                    }
                    else
                        ok++;
                }
                catch (Exception ex)
                {
                    erro++;
                    Relatorio += string.Format("\r\nPGF = {0} ****Erro****\r\n{1}\r\n", rowPGF.PGF, VirExcepitionNeon.VirExceptionNeon.RelatorioDeErro(ex, false, true, false));
                }

                TimeSpan TempoParcial = DateTime.Now - dataTESTE;
                dataTESTE = DateTime.Now;
                Console.WriteLine(string.Format("PGF {0} - {1}/{2} - Notas: {3:n0}", rowPGF.PGF, iAtual, iTotal, dPagamentosPeriodicos1.NOtAs.Count));


            }
            TimeSpan TempoTotal = DateTime.Now - dataInicio;
            return string.Format("({0:n0}.{1:000} s) Peri�dicos: lidos:{2} ok {3} ajustados:{4} erros:{5}{6}\r\n",
                                                          TempoTotal.TotalSeconds,
                                                          TempoTotal.Milliseconds, 
                                                          iTotal, 
                                                          ok, 
                                                          Ajustados,
                                                          erro, 
                                                          (erro == 0) ? "" : "\r\n" + Relatorio);
        }

        /// <summary>
        /// Transfere os dados das tabelas TMP para SCC e CCD fazendo tranctions individuais
        /// </summary>
        /// <returns></returns>
        public string CarregaTMP_para_SCC()
        {            
            dllbanco.CarregaExtrato.carga carga1 = new dllbanco.CarregaExtrato.carga();
            return carga1.CarregarSaldos(ref strLocal, ref iTotal,ref iAtual);
        }

        /*
        private int? ValidaCorrecao(int BOL)
        {
            string comandoverifica =
"SELECT     BOLetos_1.BOL AS Expr1\r\n" +
"FROM         BOLetos AS BOLetos_1 INNER JOIN\r\n" +
"                      BOletoDetalhe ON BOLetos_1.BOL = BOletoDetalhe.BOD_BOL\r\n" +
"WHERE     (BOLetos_1.BOLCancelado = 0) AND (BOLetos_1.BOLPagamento IS NULL) AND (BOletoDetalhe.BODAcordo_BOL = @P1);";
            return TableAdapter.ST().BuscaEscalar_int(comandoverifica, BOL);
        }*/

        /// <summary>
        /// Fun��o para corrigir boletos antigos na inadimpl�ncia
        /// </summary>
        /// <param name="TempoMax"></param>
        /// <param name="Corrigir"></param>
        /// <returns></returns>
        public string CorrecaoBoletosAntigosInad(int TempoMax, bool Corrigir)
        {
            string coandoLevantamento =
"SELECT     BOL,BOLJustificativa\r\n" +
"FROM         BOLetos\r\n" +
"WHERE     (BOLPagamento IS NULL) AND (BOLCancelado = 1)\r\n" +
"and \r\n" +
"(BOLStatus in  (0, 2, 3, 4));";
            string comandoEquivalentes =
"SELECT     BOLetos_1.BOL AS BOLEQ, BOletoDetalhe.BODAcordo_BOL AS BOLORIG\r\n" + 
"FROM         BOLetos AS BOLetos_1 INNER JOIN\r\n" + 
"                      BOletoDetalhe ON BOLetos_1.BOL = BOletoDetalhe.BOD_BOL\r\n" + 
"WHERE     (BOLetos_1.BOLCancelado = 0) AND (BOLetos_1.BOLPagamento IS NULL) AND (NOT (BOletoDetalhe.BODAcordo_BOL IS NULL))\r\n" + 
"ORDER BY BOLORIG;";
            
            
            System.Text.StringBuilder SB = new System.Text.StringBuilder(string.Format("{0:HH:mm:ss}\r\n",DateTime.Now));
            try
            {
                DataTable DT = TableAdapter.ST().BuscaSQLTabela(coandoLevantamento);
                DataTable DTEq = TableAdapter.ST().BuscaSQLTabela(comandoEquivalentes);
                DTEq.DefaultView.Sort = "BOLORIG";

                SB.AppendLine(string.Format("{0:HH:mm:ss} Boletos para testar: {1:n2}", DateTime.Now, DT.Rows.Count));
                DateTime inicio = DateTime.Now;
                foreach (DataRow DR in DT.Rows)
                {
                    SB.Append(string.Format("{0:HH:mm:ss} BOL:\t{1}\t", DateTime.Now, DR["BOL"]));
                    //int? BOLEquivalente = ValidaCorrecao((int)DR["BOL"]);
                    //if (BOLEquivalente.HasValue)
                    int chave = DTEq.DefaultView.Find(DR["BOL"]);
                    if (chave != -1)
                        SB.AppendLine(string.Format("->{0}", DTEq.Rows[chave]["BOLEQ"]));
                    else
                    {
                        SB.AppendLine(string.Format("******CORRIGIR*******"));
                        if (Corrigir)
                        {
                            string novaJustificativa;
                            if (DR["BOLJustificativa"] != DBNull.Value)
                                novaJustificativa = string.Format("{0}\r\nBoleto antigo cancelado por n�o ser v�lido", DR["BOLJustificativa"]);
                            else
                                novaJustificativa = string.Format("Boleto antigo cancelado por n�o ser v�lido");
                            TableAdapter.ST().ExecutarSQLNonQuery("update boletos set bolstatus = -1,boljustificativa = @P1 where bol = @P2", novaJustificativa, DR["BOL"]);
                        }
                    }
                    TimeSpan TS = DateTime.Now - inicio;
                    if (TS.TotalSeconds > TempoMax)
                    {
                        SB.AppendLine(string.Format("******TEMPO*******"));
                        break;
                    }
                }
            }
            catch (Exception e)
            {
                SB.Append(VirExcepitionNeon.VirExceptionNeon.RelatorioDeErro(e, false, true, false));
            }
            SB.Append(string.Format("\r\nFIM"));
            return SB.ToString();
        }


        /*
        /// <summary>
        /// Efetua 1 cadastro
        /// </summary>
        /// <param name="retClienteRow"></param>
        /// <param name="Apagar"></param>
        /// <returns></returns>
        public bool EfetuaCadastro(dIntenet.RetClienteRow retClienteRow, bool Apagar)
        {
            dLocal dLocal1 = new dLocal();
            try
            {
                int n;
                int PES;
                if (retClienteRow.proprietario)
                    n = dLocal1.PESSOASTableAdapter.BuscaProprietario(dLocal1.PESSOAS, retClienteRow.CODCON, retClienteRow.bloco, retClienteRow.apartamento);
                else
                    n = dLocal1.PESSOASTableAdapter.BuscaInquilino(dLocal1.PESSOAS, retClienteRow.CODCON, retClienteRow.bloco, retClienteRow.apartamento);
                if (n == 0)
                {
                    Relatorio += string.Format("Apartamento n�o encontrado: <{0}> <{1}> <{2}>\r\n", retClienteRow.CODCON, retClienteRow.bloco, retClienteRow.apartamento);
                    return false;
                }
                else
                    if (n > 1)
                    {
                        Relatorio += string.Format("Apartamento n�o encontrado: <{0}> <{1}> <{2}>\r\n", retClienteRow.CODCON, retClienteRow.bloco, retClienteRow.apartamento);
                        return false;
                    }
                    else
                    {
                        FormaPagamento FPG;
                        
                        //bool novo = false;
                        dLocal.PESSOASRow PESSOASRow = (dLocal.PESSOASRow)dLocal1.PESSOAS.Rows[0];
                        if (PESSOASRow.PESNome == retClienteRow.Nome.ToUpper().Trim())
                        {
                            if (!retClienteRow.IsbairroNull())
                                PESSOASRow.PESBairro = retClienteRow.bairro;
                            if (!retClienteRow.IscepNull())
                                PESSOASRow.PESCep = retClienteRow.cep;
                            if (!retClienteRow.IsEmailNull())
                                PESSOASRow.PESEmail = retClienteRow.Email;

                            PESSOASRow.PESEndereco = retClienteRow.endereco;
                            if (!retClienteRow.Istelefone1Null())
                                PESSOASRow.PESFone1 = retClienteRow.telefone1;
                            if (!retClienteRow.Istelefone2Null())
                                PESSOASRow.PESFone2 = retClienteRow.telefone2;
                            if (!retClienteRow.Istelefone3Null())
                                PESSOASRow.PESFone3 = retClienteRow.telefone3;

                            PESSOASRow.PESSolicitante = "INTERNET";
                            if (retClienteRow.IscidadeNull())
                                retClienteRow.cidade = "";
                            object objCID = dLocal1.CIDADESTableAdapter.BuscaCidade(retClienteRow.cidade.Trim());
                            int CID;
                            if (objCID == null)
                            {
                                dLocal1.CIDADES.Clear();
                                dLocal.CIDADESRow novaCidade = dLocal1.CIDADES.NewCIDADESRow();
                                novaCidade.CIDDataInclusao = DateTime.Now;
                                if (!retClienteRow.IscidadeNull())
                                    novaCidade.CIDNome = retClienteRow.cidade.Trim();
                                if (!retClienteRow.IsestadoNull())
                                    novaCidade.CIDUf = retClienteRow.estado;
                                dLocal1.CIDADES.AddCIDADESRow(novaCidade);
                                dLocal1.CIDADESTableAdapter.Update(novaCidade);
                                CID = novaCidade.CID;
                            }
                            else
                                CID = (int)objCID;
                            PESSOASRow.PES_CID = CID;
                            PESSOASRow.PESDataAtualizacao = DateTime.Now;

                            dLocal1.PESSOASTableAdapter.Update(PESSOASRow);
                            PES = PESSOASRow.PES;
                            dLocal1.APARTAMENTOSTableAdapter.BuscaAPT(dLocal1.APARTAMENTOS, retClienteRow.bloco, retClienteRow.CODCON, retClienteRow.apartamento);
                            dLocal.APARTAMENTOSRow ApartamentosRow = (dLocal.APARTAMENTOSRow)dLocal1.APARTAMENTOS.Rows[0];

                            if (!retClienteRow.IsAPTFormaPagto_FPGNull())
                            {
                                FPG = (FormaPagamento)retClienteRow.APTFormaPagto_FPG;
                                if (retClienteRow.proprietario)
                                    ApartamentosRow.APTFormaPagtoProprietario_FPG = (int)retClienteRow.APTFormaPagto_FPG;
                                else
                                    ApartamentosRow.APTFormaPagtoInquilino_FPG = (int)retClienteRow.APTFormaPagto_FPG;
                            }
                            else
                            {
                                if (retClienteRow.proprietario)
                                {
                                    if (!retClienteRow.bemail)
                                        FPG = (retClienteRow.correio ? FormaPagamento.Correio : FormaPagamento.EntreigaPessoal);
                                    else
                                    {
                                        FormaPagamento FPGAnterior = ApartamentosRow.IsAPTFormaPagtoProprietario_FPGNull() ? FormaPagamento.EntreigaPessoal : (FormaPagamento)ApartamentosRow.APTFormaPagtoProprietario_FPG;
                                        if (FPGAnterior.EstaNoGrupo(FormaPagamento.email_Correio, FormaPagamento.email_EntreigaPessoal))
                                            FPG = (retClienteRow.correio ? FormaPagamento.email_Correio : FormaPagamento.email_EntreigaPessoal);
                                        else
                                            FPG = (retClienteRow.correio ? FormaPagamento.CorreioEemail : FormaPagamento.EntreigaPessoalEemail);
                                    }
                                    ApartamentosRow.APTFormaPagtoProprietario_FPG = (int)FPG;
                                    //ApartamentosRow.APTBEmailP = retClienteRow.bemail;                                
                                    //FPG = ApartamentosRow.APTFormaPagtoProprietario_FPG = (retClienteRow.correio ? 2 : 4);
                                }
                                else
                                {
                                    if (!retClienteRow.bemail)
                                        FPG = (retClienteRow.correio ? FormaPagamento.Correio : FormaPagamento.EntreigaPessoal);
                                    else
                                    {
                                        FormaPagamento FPGAnterior = ApartamentosRow.IsAPTFormaPagtoInquilino_FPGNull() ? FormaPagamento.EntreigaPessoal : (FormaPagamento)ApartamentosRow.APTFormaPagtoInquilino_FPG;
                                        if (FPGAnterior.EstaNoGrupo(FormaPagamento.email_Correio, FormaPagamento.email_EntreigaPessoal))
                                            FPG = (retClienteRow.correio ? FormaPagamento.email_Correio : FormaPagamento.email_EntreigaPessoal);
                                        else
                                            FPG = (retClienteRow.correio ? FormaPagamento.CorreioEemail : FormaPagamento.EntreigaPessoalEemail);
                                    }
                                    ApartamentosRow.APTFormaPagtoProprietario_FPG = (int)FPG;
                                    //ApartamentosRow.APTBEmailI = retClienteRow.bemail;                                
                                    //FPG = ApartamentosRow.APTFormaPagtoInquilino_FPG = (retClienteRow.correio ? 2 : 4);                                    
                                };
                            }


                            
                            if (!retClienteRow.IsAPTSeguroNull())
                            {                                
                                ApartamentosRow.APTSeguro = retClienteRow.APTSeguro;                                
                            }
                            dLocal1.APARTAMENTOSTableAdapter.Update(ApartamentosRow);
                            int APT;
                            string ComandoBuscaAPT = "SELECT     APARTAMENTOS.APT\r\n" +
                                                     "FROM         APARTAMENTOS INNER JOIN\r\n" +
                                                     "BLOCOS ON APARTAMENTOS.APT_BLO = BLOCOS.BLO INNER JOIN\r\n" +
                                                     "CONDOMINIOS ON BLOCOS.BLO_CON = CONDOMINIOS.CON\r\n" +
                                                     "WHERE     (CONDOMINIOS.CONCodigo = @P1) AND (BLOCOS.BLOCodigo = @P2) AND (APARTAMENTOS.APTNumero = @P3)";
                            if (TableAdapter.ST().BuscaEscalar(ComandoBuscaAPT, out APT, retClienteRow.CODCON, retClienteRow.bloco, retClienteRow.apartamento))
                            {
                                string Comando;
                                
                                if (retClienteRow.proprietario)
                                {
                                    Comando = "UPDATE APARTAMENTOS SET APTUsuarioInternetProprietario = @P1 , APTSenhaInternetProprietario =@P2 Where APT = @P3";
                                    
                                }
                                else
                                {
                                    Comando = "UPDATE APARTAMENTOS SET APTUsuarioInternetInquilino = @P1, APTSenhaInternetInquilino = @P2 Where APT = @P3";
                                    
                                }
                                TableAdapter.ST().ExecutarSQLNonQuery(Comando, retClienteRow.usuariointernet, retClienteRow.senhainternet, APT);
                                
                            };
                            //retClienteRow.usuariointernet
                            //    Trans.Complete();
                            //};
                            Exp.ExportarInquilinoProprietarioParaMsAccess(retClienteRow.proprietario, PES, retClienteRow.CODCON, retClienteRow.bloco, retClienteRow.apartamento, FPG.EstaNoGrupo(FormaPagamento.Correio,FormaPagamento.CorreioEemail,FormaPagamento.email_Correio)?2:4);
                        }


                        if (Apagar)
                        {
                            retClienteRow.Delete();                            
                            dInternet1.RetClienteTableAdapter.Update(retClienteRow);
                        };



                        return true;
                    };
            }
            catch (Exception e)
            {
                Relatorio += VirExcepitionNeon.VirExceptionNeon.RelatorioDeErro(e, false, true, false);
                return false;
            };

        }*/

        /// <summary>
        /// Efetua 1 acordo
        /// </summary>
        /// <param name="retacordirow"></param>
        /// <returns></returns>
        public bool EfetuaAcordo(dIntenet.RetAcordoRow retacordirow)
        {
            Framework.objetosNeon.Apartamento Apartamento = new Framework.objetosNeon.Apartamento(retacordirow.codcon, retacordirow.bloco, retacordirow.apartamento);

            if (!Apartamento.Encontrado)
            {
                Relatorio += "Apartamento n�o Cadastrado";
                return false;
            }


            bool Dojuridico = false;
            if (!retacordirow.IsJuridicoNull() && retacordirow.Juridico)
                Dojuridico = true;
            Boletos.Acordo.Acordo NovoAcordo = new Boletos.Acordo.Acordo(Apartamento.APT, true, Dojuridico);
            if (NovoAcordo.Gerado)
            {
                Relatorio += "Apartamento j� possui acordo:" + NovoAcordo.LinhaMae.ACO.ToString() + " Status:" + NovoAcordo.LinhaMae.ACOStatus.ToString();
                return false;
            };

            if (retacordirow.IsJuridicoNull())
                NovoAcordo.Juridico = false;
            else
            {
                if (retacordirow.Juridico)
                {
                    NovoAcordo.Juridico = true;
                    int FRN = -1;
                    if (!retacordirow.IsFRNNull())
                        FRN = retacordirow.FRN;
                    else
                    {
                        if (!TableAdapter.ST().BuscaEscalar("select APTJur_FRN from AParTamentos where APT = @P1", out FRN, Apartamento.APT))
                            TableAdapter.ST().BuscaEscalar("select CONEscrit_FRN from condominios where CON = @P1", out FRN, Apartamento.CON);
                    }
                    if (FRN > 0)
                        NovoAcordo.FRN = FRN;
                }
                else
                    NovoAcordo.Juridico = false;
            };

            if (retacordirow.IsJudicialNull())
                NovoAcordo.Judicial = false;
            else
                NovoAcordo.Judicial = retacordirow.Judicial;


            //carregando os dados ORIGINAIS


            foreach (BoletosProc.Acordo.dAcordo.BOLetosRow rowOrigL in NovoAcordo.dAcordo.BOLetos)
                rowOrigL.Incluir = false;

            foreach (dIntenet.RetOriginaisRow Origrow in retacordirow.GetRetOriginaisRows())
            {
                BoletosProc.Acordo.dAcordo.BOLetosRow rowOrig = NovoAcordo.dAcordo.BOLetos.FindByBOL(Origrow.numero);
                if (rowOrig == null)
                {
                    Boletos.Boleto.Boleto BolSumido = new Boletos.Boleto.Boleto(Origrow.numero);
                    if (!BolSumido.Encontrado)
                        Relatorio += "Boleto " + Origrow.numero + " n�o encontrado";
                    else
                        if (!BolSumido.rowPrincipal.IsBOLPagamentoNull())
                            Relatorio += "Boleto " + Origrow.numero + " pago em " + BolSumido.rowPrincipal.BOLPagamento.ToString();
                        else
                        {
                            Relatorio += "Boleto " + Origrow.numero + " n�o est� presente (comunique o suporte)\r\n";
                            Relatorio += "Status do boleto:" + BolSumido.rowPrincipal.BOLCancelado.ToString() + "\r\n";
                            Relatorio += "Apartamento:" + Apartamento.APT.ToString() + "\r\nBoletos encontrados:\r\n";

                            foreach (BoletosProc.Acordo.dAcordo.BOLetosRow rowOrigL in NovoAcordo.dAcordo.BOLetos)
                                Relatorio += rowOrigL.BOL.ToString() + "\r\n";
                        }
                    return false;
                };
                rowOrig.Incluir = true;
                rowOrig.SubTotal = Origrow.total;
                rowOrig.incValOriginal = rowOrig.BOLValorPrevisto;
                NovoAcordo.CadastraBoletoOrig(rowOrig.BOL);
            };

            decimal TotalAcordado = 0;
            foreach (dIntenet.RetNovosBRow Novrow in retacordirow.GetRetNovosBRows())
            {
                BoletosProc.Acordo.dAcordo.GeraNovosRow NovaLinha = NovoAcordo.dAcordo.GeraNovos.NewGeraNovosRow();
                NovaLinha.Data = Novrow.vencto;
                if (!Novrow.IsHonorariosNull())
                {
                    NovaLinha.Honorarios = Novrow.Honorarios;
                    if (Novrow.valor < Novrow.Honorarios)
                    {
                        Relatorio += string.Format("Valor incorreto de honorarios: parcela: {0:n2} honor�rios {1:n2}", Novrow.valor, Novrow.Honorarios);
                        return false;
                    }
                }
                else
                    NovaLinha.Honorarios = 0;
                TotalAcordado += Novrow.valor - NovaLinha.Honorarios;
                NovaLinha.Valor = Novrow.valor;
                NovaLinha.BOL = Novrow.nn;
                NovoAcordo.dAcordo.GeraNovos.AddGeraNovosRow(NovaLinha);

            }

            NovoAcordo.dAcordo.DistribuiEncargos(TotalAcordado, false);

            if (NovoAcordo.Gerar(dllImpresso.Botoes.Botao.nulo))
            {
                retacordirow.Efetuado = true;
                dInternet1.RetAcordoTableAdapter.Update(retacordirow);
                retacordirow.AcceptChanges();
                return true;
            }
            else
            {
                Relatorio += NovoAcordo.UltimoErro + "(*)\r\n";
                return false;
            };
        }

        /*
        public bool ExportarBoleto(int BOL)
        {
            Boletos.Boleto.Boleto BoletoExp = new Boletos.Boleto.Boleto(BOL);
            if (BoletoExp.Encontrado)
            {
                BoletoExp.ReExportaTh();
                return true;
            }
            else
                return false;
        }
        */

        #endregion

            /*
        private Cadastros.Utilitarios.MSAccess.fExportToMsAccess exp;

        private Cadastros.Utilitarios.MSAccess.fExportToMsAccess Exp
        {
            get
            {
                if (exp == null)
                    exp = new Cadastros.Utilitarios.MSAccess.fExportToMsAccess();
                return exp;
            }
        }*/

        /// <summary>
        /// Relat�rio
        /// </summary>
        public string Relatorio = "";


        



        #region Auditoria
        

        

        #endregion

       
    


        

    }
}
