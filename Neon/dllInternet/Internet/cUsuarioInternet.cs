using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace Internet
{
    public partial class cUsuarioInternet : CompontesBasicos.ComponenteBase
    {
        public cUsuarioInternet()
        {
            InitializeComponent();
        }

        private int FilialInternet
        {
            get
            {
                return (Framework.DSCentral.EMP == 1) ? 1 : 2;
            }
        }

        private void simpleButton1_Click(object sender, EventArgs e)
        {            
            string URL = 
                @"www.neonimoveis.com.br/neononline2/UsuSenha.aspx?filial="+
                FilialInternet.ToString()+
                "&Ap="+
                lookupBlocosAptos_F1.APT_Selrow.APTNumero+
                "&Bloco="+
                lookupBlocosAptos_F1.BLO_Selrow.BLOCodigo+
                "&CodCon="+
                lookupBlocosAptos_F1.CON_selrow.CONCodigo;            
            componenteWEB1.Navigate(URL);
            NovoU.Text = NovaS.Text = "";
        }

        private void AjustaBotA(){
            BotA.Enabled = (lookupBlocosAptos_F1.APT_Sel != -1) && (NovoU.Text != "") && (NovaS.Text != "") && (imageComboBoxEdit1.EditValue != null);
        }

        private void NovoU_EditValueChanged(object sender, EventArgs e)
        {
            AjustaBotA();
        }

        private void NovaS_EditValueChanged(object sender, EventArgs e)
        {
            AjustaBotA();
        }

        private void lookupBlocosAptos_F1_alteradoAPT(object sender, EventArgs e)
        {
            AjustaBotA();
        }

        private void BotA_Click(object sender, EventArgs e)
        {
            string URL =
                            @"www.neonimoveis.com.br/neononline2/UsuSenha.aspx?filial=" +
                            FilialInternet.ToString() +
                            "&Ap=" +
                            lookupBlocosAptos_F1.APT_Selrow.APTNumero +
                            "&Bloco=" +
                            lookupBlocosAptos_F1.BLO_Selrow.BLOCodigo +
                            "&CodCon=" +
                            lookupBlocosAptos_F1.CON_selrow.CONCodigo +
                            "&NovoUsuario=" +
                            NovoU.Text +
                            "&NovaSenha=" +
                            NovaS.Text +
                            "&Proprietario=" +
                            imageComboBoxEdit1.EditValue.ToString();
            
            
            componenteWEB1.Navigate(URL);
            string ComandoMSSQL = "";
            string ComandoAccess;
            if (imageComboBoxEdit1.EditValue.ToString() == "P")
            {
                ComandoMSSQL =
"UPDATE    APARTAMENTOS\r\n" +
"SET              APTUsuarioInternetProprietario = @P1, APTSenhaInternetProprietario = @P2\r\n" +
"WHERE     (APT = @P3);";
                ComandoAccess =
"UPDATE    Proprietário\r\n" +
"SET              [usuario internet] = @P1, [senha internet] = @P2\r\n" +
"WHERE     (Proprietário.Codcon = @P3) AND (Proprietário.Apartamento = @P4) AND (Proprietário.Bloco = @P5);";
            }
            else
            {
                ComandoMSSQL =
"UPDATE    APARTAMENTOS\r\n" +
"SET              APTUsuarioInternetInquilino = @P1, APTSenhaInternetInquilino = @P2\r\n" +
"WHERE     (APT = @P3);";
                ComandoAccess =
"UPDATE    Inquilino\r\n" +
"SET              [usuario internet] = @P1, [senha internet] = @P2\r\n" +
"WHERE     (Codcon = @P3) AND (Apartamento = @P4) AND (Bloco = @P5);";
            }
            VirMSSQL.TableAdapter.STTableAdapter.ExecutarSQLNonQuery(ComandoMSSQL,NovoU.Text,NovaS.Text,lookupBlocosAptos_F1.APT_Sel);
            VirOleDb.TableAdapter.STTableAdapter.ExecutarSQLNonQuery(ComandoAccess, NovoU.Text, NovaS.Text,
                lookupBlocosAptos_F1.CON_selrow.CONCodigo, lookupBlocosAptos_F1.APT_Selrow.APTNumero, lookupBlocosAptos_F1.BLO_Selrow.BLOCodigo);
        }

        private void imageComboBoxEdit1_EditValueChanged(object sender, EventArgs e)
        {
            AjustaBotA();
        }
    }
}

