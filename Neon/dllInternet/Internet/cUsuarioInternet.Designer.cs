namespace Internet
{
    partial class cUsuarioInternet
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(cUsuarioInternet));
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling1 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling2 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            this.panelControl1 = new DevExpress.XtraEditors.PanelControl();
            this.imageComboBoxEdit1 = new DevExpress.XtraEditors.ImageComboBoxEdit();
            this.imagensPI1 = new Framework.objetosNeon.ImagensPI();
            this.BotA = new DevExpress.XtraEditors.SimpleButton();
            this.NovaS = new DevExpress.XtraEditors.TextEdit();
            this.NovoU = new DevExpress.XtraEditors.TextEdit();
            this.labelControl2 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl1 = new DevExpress.XtraEditors.LabelControl();
            this.lookupBlocosAptos_F1 = new Framework.Lookup.LookupBlocosAptos_F();
            this.simpleButton1 = new DevExpress.XtraEditors.SimpleButton();
            this.componenteWEB1 = new CompontesBasicos.ComponenteWEB();
            ((System.ComponentModel.ISupportInitialize)(this.StyleController_F)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl1)).BeginInit();
            this.panelControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.imageComboBoxEdit1.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.imagensPI1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.NovaS.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.NovoU.Properties)).BeginInit();
            this.SuspendLayout();
            // 
            // ToolTipController_F
            // 
            this.ToolTipController_F.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.ToolTipController_F.Appearance.ForeColor = System.Drawing.Color.Black;
            this.ToolTipController_F.Appearance.Options.UseBackColor = true;
            this.ToolTipController_F.Appearance.Options.UseFont = true;
            this.ToolTipController_F.Appearance.Options.UseForeColor = true;
            this.ToolTipController_F.Appearance.Options.UseTextOptions = true;
            this.ToolTipController_F.Appearance.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            // 
            // StyleController_F
            // 
            this.StyleController_F.AppearanceDisabled.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this.StyleController_F.AppearanceDisabled.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this.StyleController_F.AppearanceDisabled.Options.UseBackColor = true;
            this.StyleController_F.AppearanceDisabled.Options.UseForeColor = true;
            this.StyleController_F.AppearanceFocused.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.StyleController_F.AppearanceFocused.Options.UseBackColor = true;
            // 
            // spellChecker1
            // 
            this.spellChecker1.OptionsSpelling.CheckSelectedTextFirst = DevExpress.Utils.DefaultBoolean.True;
            this.spellChecker1.OptionsSpelling.IgnoreEmails = DevExpress.Utils.DefaultBoolean.True;
            this.spellChecker1.OptionsSpelling.IgnoreMixedCaseWords = DevExpress.Utils.DefaultBoolean.False;
            this.spellChecker1.OptionsSpelling.IgnoreRepeatedWords = DevExpress.Utils.DefaultBoolean.False;
            this.spellChecker1.OptionsSpelling.IgnoreUpperCaseWords = DevExpress.Utils.DefaultBoolean.False;
            this.spellChecker1.OptionsSpelling.IgnoreUrls = DevExpress.Utils.DefaultBoolean.True;
            this.spellChecker1.OptionsSpelling.IgnoreWordsWithNumbers = DevExpress.Utils.DefaultBoolean.True;
            // 
            // panelControl1
            // 
            this.panelControl1.Controls.Add(this.imageComboBoxEdit1);
            this.panelControl1.Controls.Add(this.BotA);
            this.panelControl1.Controls.Add(this.NovaS);
            this.panelControl1.Controls.Add(this.NovoU);
            this.panelControl1.Controls.Add(this.labelControl2);
            this.panelControl1.Controls.Add(this.labelControl1);
            this.panelControl1.Controls.Add(this.lookupBlocosAptos_F1);
            this.panelControl1.Controls.Add(this.simpleButton1);
            this.panelControl1.Dock = System.Windows.Forms.DockStyle.Top;
            this.panelControl1.Location = new System.Drawing.Point(0, 0);
            this.panelControl1.Name = "panelControl1";
            this.panelControl1.Size = new System.Drawing.Size(985, 111);
            this.panelControl1.TabIndex = 0;
            // 
            // imageComboBoxEdit1
            // 
            this.imageComboBoxEdit1.Location = new System.Drawing.Point(352, 71);
            this.imageComboBoxEdit1.Name = "imageComboBoxEdit1";
            this.imageComboBoxEdit1.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.imageComboBoxEdit1.Properties.GlyphAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.imageComboBoxEdit1.Properties.Items.AddRange(new DevExpress.XtraEditors.Controls.ImageComboBoxItem[] {
            new DevExpress.XtraEditors.Controls.ImageComboBoxItem("Proprietário", "P", 0),
            new DevExpress.XtraEditors.Controls.ImageComboBoxItem("Inquilino", "I", 1)});
            this.imageComboBoxEdit1.Properties.SmallImages = this.imagensPI1;
            this.imageComboBoxEdit1.Size = new System.Drawing.Size(77, 18);
            this.imageComboBoxEdit1.TabIndex = 7;
            this.imageComboBoxEdit1.EditValueChanged += new System.EventHandler(this.imageComboBoxEdit1_EditValueChanged);
            // 
            // imagensPI1
            // 
            this.imagensPI1.ImageStream = ((DevExpress.Utils.ImageCollectionStreamer)(resources.GetObject("imagensPI1.ImageStream")));
            // 
            // BotA
            // 
            this.BotA.Enabled = false;
            this.BotA.Location = new System.Drawing.Point(452, 68);
            this.BotA.Name = "BotA";
            this.BotA.Size = new System.Drawing.Size(145, 23);
            this.BotA.TabIndex = 6;
            this.BotA.Text = "Alteara usuário/senha";
            this.BotA.Click += new System.EventHandler(this.BotA_Click);
            // 
            // NovaS
            // 
            this.NovaS.Location = new System.Drawing.Point(246, 71);
            this.NovaS.Name = "NovaS";
            this.spellChecker1.SetShowSpellCheckMenu(this.NovaS, true);
            this.NovaS.Size = new System.Drawing.Size(100, 20);
            this.spellChecker1.SetSpellCheckerOptions(this.NovaS, optionsSpelling1);
            this.NovaS.TabIndex = 5;
            this.NovaS.EditValueChanged += new System.EventHandler(this.NovaS_EditValueChanged);
            // 
            // NovoU
            // 
            this.NovoU.Location = new System.Drawing.Point(76, 71);
            this.NovoU.Name = "NovoU";
            this.spellChecker1.SetShowSpellCheckMenu(this.NovoU, true);
            this.NovoU.Size = new System.Drawing.Size(100, 20);
            this.spellChecker1.SetSpellCheckerOptions(this.NovoU, optionsSpelling2);
            this.NovoU.TabIndex = 4;
            this.NovoU.EditValueChanged += new System.EventHandler(this.NovoU_EditValueChanged);
            // 
            // labelControl2
            // 
            this.labelControl2.Location = new System.Drawing.Point(188, 80);
            this.labelControl2.Name = "labelControl2";
            this.labelControl2.Size = new System.Drawing.Size(58, 13);
            this.labelControl2.TabIndex = 3;
            this.labelControl2.Text = "Nova Senha";
            // 
            // labelControl1
            // 
            this.labelControl1.Location = new System.Drawing.Point(12, 80);
            this.labelControl1.Name = "labelControl1";
            this.labelControl1.Size = new System.Drawing.Size(64, 13);
            this.labelControl1.TabIndex = 2;
            this.labelControl1.Text = "Novo Usuário";
            // 
            // lookupBlocosAptos_F1
            // 
            this.lookupBlocosAptos_F1.APT_Sel = -1;
            this.lookupBlocosAptos_F1.Autofill = true;
            this.lookupBlocosAptos_F1.BLO_Sel = -1;
            this.lookupBlocosAptos_F1.CaixaAltaGeral = true;
            this.lookupBlocosAptos_F1.CON_sel = -1;
            this.lookupBlocosAptos_F1.Cursor = System.Windows.Forms.Cursors.Default;
            this.lookupBlocosAptos_F1.Doc = System.Windows.Forms.DockStyle.None;
            this.lookupBlocosAptos_F1.Estado = CompontesBasicos.EstadosDosComponentes.Registrado;
            this.lookupBlocosAptos_F1.Icone = null;
            this.lookupBlocosAptos_F1.ImagemG = ((System.Drawing.Image)(resources.GetObject("lookupBlocosAptos_F1.ImagemG")));
            this.lookupBlocosAptos_F1.ImagemP = ((System.Drawing.Image)(resources.GetObject("lookupBlocosAptos_F1.ImagemP")));
            this.lookupBlocosAptos_F1.LinhaMae_F = null;
            this.lookupBlocosAptos_F1.Location = new System.Drawing.Point(5, 8);
            this.lookupBlocosAptos_F1.LookAndFeel.UseDefaultLookAndFeel = false;
            this.lookupBlocosAptos_F1.Name = "lookupBlocosAptos_F1";
            this.lookupBlocosAptos_F1.Size = new System.Drawing.Size(424, 46);
            this.lookupBlocosAptos_F1.somenteleitura = false;
            this.lookupBlocosAptos_F1.TabIndex = 1;
            this.lookupBlocosAptos_F1.TableAdapterPrincipal = null;
            this.lookupBlocosAptos_F1.Titulo = null;
            this.lookupBlocosAptos_F1.Total = false;
            this.lookupBlocosAptos_F1.alteradoAPT += new System.EventHandler(this.lookupBlocosAptos_F1_alteradoAPT);
            // 
            // simpleButton1
            // 
            this.simpleButton1.Location = new System.Drawing.Point(452, 8);
            this.simpleButton1.Name = "simpleButton1";
            this.simpleButton1.Size = new System.Drawing.Size(145, 23);
            this.simpleButton1.TabIndex = 0;
            this.simpleButton1.Text = "Verificar";
            this.simpleButton1.Click += new System.EventHandler(this.simpleButton1_Click);
            // 
            // componenteWEB1
            // 
            this.componenteWEB1.CaixaAltaGeral = true;
            this.componenteWEB1.Doc = System.Windows.Forms.DockStyle.Fill;
            this.componenteWEB1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.componenteWEB1.Estado = CompontesBasicos.EstadosDosComponentes.Registrado;
            this.componenteWEB1.Icone = null;
            this.componenteWEB1.ImagemG = ((System.Drawing.Image)(resources.GetObject("componenteWEB1.ImagemG")));
            this.componenteWEB1.ImagemP = ((System.Drawing.Image)(resources.GetObject("componenteWEB1.ImagemP")));
            this.componenteWEB1.Location = new System.Drawing.Point(0, 111);
            this.componenteWEB1.LookAndFeel.UseDefaultLookAndFeel = false;
            this.componenteWEB1.MostraEndereco = false;
            this.componenteWEB1.Name = "componenteWEB1";
            this.componenteWEB1.Size = new System.Drawing.Size(985, 629);
            this.componenteWEB1.somenteleitura = false;
            this.componenteWEB1.TabIndex = 1;
            this.componenteWEB1.Titulo = null;
            // 
            // cUsuarioInternet
            // 
            this.CaixaAltaGeral = false;
            this.Controls.Add(this.componenteWEB1);
            this.Controls.Add(this.panelControl1);
            this.Doc = System.Windows.Forms.DockStyle.Fill;
            this.ImagemG = ((System.Drawing.Image)(resources.GetObject("$this.ImagemG")));
            this.ImagemP = ((System.Drawing.Image)(resources.GetObject("$this.ImagemP")));
            this.LookAndFeel.UseDefaultLookAndFeel = false;
            this.Name = "cUsuarioInternet";
            this.Size = new System.Drawing.Size(985, 740);
            ((System.ComponentModel.ISupportInitialize)(this.StyleController_F)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl1)).EndInit();
            this.panelControl1.ResumeLayout(false);
            this.panelControl1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.imageComboBoxEdit1.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.imagensPI1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.NovaS.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.NovoU.Properties)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraEditors.PanelControl panelControl1;
        private DevExpress.XtraEditors.SimpleButton simpleButton1;
        private CompontesBasicos.ComponenteWEB componenteWEB1;
        private Framework.Lookup.LookupBlocosAptos_F lookupBlocosAptos_F1;
        private DevExpress.XtraEditors.SimpleButton BotA;
        private DevExpress.XtraEditors.TextEdit NovaS;
        private DevExpress.XtraEditors.TextEdit NovoU;
        private DevExpress.XtraEditors.LabelControl labelControl2;
        private DevExpress.XtraEditors.LabelControl labelControl1;
        private DevExpress.XtraEditors.ImageComboBoxEdit imageComboBoxEdit1;
        private Framework.objetosNeon.ImagensPI imagensPI1;
    }
}
