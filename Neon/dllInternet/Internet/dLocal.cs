﻿/*
MR - 28/05/2014 12:30 - Publicação de regras dos Equipamento (Alterações indicadas por *** MRC - INICIO - RESERVA (28/05/2014 12:30) ***)
MR - 01/07/2014 19:00 - Publicação do parametro CONJurosPrimerioMes dos Condominios (Alterações no xsd)
MR - 26/08/2014 15:00 - Publicação do parametro EQPReservaUnitaria dos Equipamentos (Alterações no xsd)
MR - 02/09/2014 16:00 - Publicação do parametro EQPMaxConvidados dos Equipamentos (Alterações no xsd)
MR - 14/01/2016 12:30 - Publicação do parametro EQPReservaUnitariaIndividual dos Equipamentos (Alterações no xsd)
MR - 25/02/2016 11:00 - Publicação do parametro CONBoletoComRegistro dos Condomínios (Alterações no xsd)
MR - 27/05/2016 12:00 - Correção no ReservaEQuipamentoTableAdapter.FillByData para pegar o horario 00:00:00 (Alterações no xsd)
*/

namespace Internet
{

    /// <summary>
    /// dLocal
    /// </summary>
    partial class dLocal
    {

        #region Table Adapters



        private dLocalTableAdapters.SAlarioMinimoTableAdapter sAlarioMinimoTableAdapter;
        /// <summary>
        /// TableAdapter padrão para tabela: SAlarioMinimo
        /// </summary>
        public dLocalTableAdapters.SAlarioMinimoTableAdapter SAlarioMinimoTableAdapter
        {
            get
            {
                if (sAlarioMinimoTableAdapter == null)
                {
                    sAlarioMinimoTableAdapter = new dLocalTableAdapters.SAlarioMinimoTableAdapter();
                    sAlarioMinimoTableAdapter.TrocarStringDeConexao();
                };
                return sAlarioMinimoTableAdapter;
            }
        }



        private dLocalTableAdapters.BOletoDetalheTableAdapter bOletoDetalheTableAdapter;
        /// <summary>
        /// TableAdapter padrão para tabela: BOletoDetalhe
        /// </summary>
        public dLocalTableAdapters.BOletoDetalheTableAdapter BOletoDetalheTableAdapter
        {
            get
            {
                if (bOletoDetalheTableAdapter == null)
                {
                    bOletoDetalheTableAdapter = new dLocalTableAdapters.BOletoDetalheTableAdapter();
                    bOletoDetalheTableAdapter.TrocarStringDeConexao();
                };
                return bOletoDetalheTableAdapter;
            }
        }

        private dLocalTableAdapters.ReservaEQuipamentoTableAdapter reservaEQuipamentoTableAdapter;
        /// <summary>
        /// TableAdapter padrão para tabela: ReservaEQuipamento
        /// </summary>
        public dLocalTableAdapters.ReservaEQuipamentoTableAdapter ReservaEQuipamentoTableAdapter
        {
            get
            {
                if (reservaEQuipamentoTableAdapter == null)
                {
                    reservaEQuipamentoTableAdapter = new dLocalTableAdapters.ReservaEQuipamentoTableAdapter();
                    reservaEQuipamentoTableAdapter.TrocarStringDeConexao();
                };
                return reservaEQuipamentoTableAdapter;
            }
        }

        private dLocalTableAdapters.GradeEQuipamentoTableAdapter gradeEQuipamentoTableAdapter;
        /// <summary>
        /// TableAdapter padrão para tabela: GradeEQuipamento
        /// </summary>
        public dLocalTableAdapters.GradeEQuipamentoTableAdapter GradeEQuipamentoTableAdapter
        {
            get
            {
                if (gradeEQuipamentoTableAdapter == null)
                {
                    gradeEQuipamentoTableAdapter = new dLocalTableAdapters.GradeEQuipamentoTableAdapter();
                    gradeEQuipamentoTableAdapter.TrocarStringDeConexao();
                };
                return gradeEQuipamentoTableAdapter;
            }
        }

        //*** MRC - INICIO - RESERVA (28/05/2014 12:30) ***
        private dLocalTableAdapters.ReGrasEquipamentoTableAdapter reGrasEquipamentoTableAdapter;
        /// <summary>
        /// TableAdapter padrão para tabela: ReGrasEquipamento
        /// </summary>
        public dLocalTableAdapters.ReGrasEquipamentoTableAdapter ReGrasEquipamentoTableAdapter
        {
            get
            {
                if (reGrasEquipamentoTableAdapter == null)
                {
                    reGrasEquipamentoTableAdapter = new dLocalTableAdapters.ReGrasEquipamentoTableAdapter();
                    reGrasEquipamentoTableAdapter.TrocarStringDeConexao();
                };
                return reGrasEquipamentoTableAdapter;
            }
        }
        //*** MRC - TERMINO - RESERVA (28/05/2014 12:30) ***

        private dLocalTableAdapters.EQuiPamentosTableAdapter eQuiPamentosTableAdapter;
        /// <summary>
        /// TableAdapter padrão para tabela: EQuiPamentos
        /// </summary>
        public dLocalTableAdapters.EQuiPamentosTableAdapter EQuiPamentosTableAdapter
        {
            get
            {
                if (eQuiPamentosTableAdapter == null)
                {
                    eQuiPamentosTableAdapter = new dLocalTableAdapters.EQuiPamentosTableAdapter();
                    eQuiPamentosTableAdapter.TrocarStringDeConexao();
                };
                return eQuiPamentosTableAdapter;
            }
        }








        private dLocalTableAdapters.BoletosAbertosTableAdapter boletosAbertosTableAdapter;
        /// <summary>
        /// TableAdapter padrão para tabela: BoletosAbertos
        /// </summary>
        public dLocalTableAdapters.BoletosAbertosTableAdapter BoletosAbertosTableAdapter
        {
            get
            {
                if (boletosAbertosTableAdapter == null)
                {
                    boletosAbertosTableAdapter = new dLocalTableAdapters.BoletosAbertosTableAdapter();
                    boletosAbertosTableAdapter.TrocarStringDeConexao();
                };
                return boletosAbertosTableAdapter;
            }
        }

        private dLocalTableAdapters.RamoAtiVidadeTableAdapter ramoAtiVidadeTableAdapter;
        /// <summary>
        /// TableAdapter padrão para tabela: RamoAtiVidade
        /// </summary>
        public dLocalTableAdapters.RamoAtiVidadeTableAdapter RamoAtiVidadeTableAdapter
        {
            get
            {
                if (ramoAtiVidadeTableAdapter == null)
                {
                    ramoAtiVidadeTableAdapter = new dLocalTableAdapters.RamoAtiVidadeTableAdapter();
                    ramoAtiVidadeTableAdapter.TrocarStringDeConexao();
                };
                return ramoAtiVidadeTableAdapter;
            }
        }

        private dLocalTableAdapters.FRNxRAVTableAdapter fRNxRAVTableAdapter;
        /// <summary>
        /// TableAdapter padrão para tabela: FRNxRAV
        /// </summary>
        public dLocalTableAdapters.FRNxRAVTableAdapter FRNxRAVTableAdapter
        {
            get
            {
                if (fRNxRAVTableAdapter == null)
                {
                    fRNxRAVTableAdapter = new dLocalTableAdapters.FRNxRAVTableAdapter();
                    fRNxRAVTableAdapter.TrocarStringDeConexao();
                };
                return fRNxRAVTableAdapter;
            }
        }

        private dLocalTableAdapters.CorpoDiretivoTableAdapter corpoDiretivoTableAdapter;
        /// <summary>
        /// TableAdapter padrão para tabela: CorpoDiretivo
        /// </summary>
        public dLocalTableAdapters.CorpoDiretivoTableAdapter CorpoDiretivoTableAdapter
        {
            get
            {
                if (corpoDiretivoTableAdapter == null)
                {
                    corpoDiretivoTableAdapter = new dLocalTableAdapters.CorpoDiretivoTableAdapter();
                    corpoDiretivoTableAdapter.TrocarStringDeConexao();
                };
                return corpoDiretivoTableAdapter;
            }
        }

        private dLocalTableAdapters.CONDOMINIOSTableAdapter cONDOMINIOSTableAdapter;
        private dLocalTableAdapters.BLOCOSTableAdapter bLOCOSTableAdapter;
        private dLocalTableAdapters.APARTAMENTOSTableAdapter aPARTAMENTOSTableAdapter;
        private dLocalTableAdapters.BOLetosTableAdapter bOLetosTableAdapter;

        //private dLocalTableAdapters.Boleto1TableAdapter boleto1TableAdapter;
        private dLocalTableAdapters.PESSOASTableAdapter pESSOASTableAdapter;
        private dLocalTableAdapters.CIDADESTableAdapter cIDADESTableAdapter;

        

        //private dLocalTableAdapters.PropInqTableAdapter propInqTableAdapter;
        private dLocalTableAdapters.FORNECEDORESTableAdapter fORNECEDORESTableAdapter;
        //private dLocalTableAdapters.AcordoErradoTableAdapter acordoErradoTableAdapter;






        private dLocalTableAdapters.ACORDOTableAdapter aCORDOTableAdapter;
        /// <summary>
        /// TableAdapter padrão para tabela: ACORDO
        /// </summary>
        public dLocalTableAdapters.ACORDOTableAdapter ACORDOTableAdapter
        {
            get
            {
                if (aCORDOTableAdapter == null)
                {
                    aCORDOTableAdapter = new dLocalTableAdapters.ACORDOTableAdapter();
                    aCORDOTableAdapter.TrocarStringDeConexao();
                };
                return aCORDOTableAdapter;
            }
        }

        private dLocalTableAdapters.OriginaisTableAdapter originaisTableAdapter;
        /// <summary>
        /// TableAdapter padrão para tabela: Originais
        /// </summary>
        public dLocalTableAdapters.OriginaisTableAdapter OriginaisTableAdapter
        {
            get
            {
                if (originaisTableAdapter == null)
                {
                    originaisTableAdapter = new dLocalTableAdapters.OriginaisTableAdapter();
                    originaisTableAdapter.TrocarStringDeConexao();
                };
                return originaisTableAdapter;
            }
        }

        private dLocalTableAdapters.NovasTableAdapter novasTableAdapter;
        /// <summary>
        /// TableAdapter padrão para tabela: Novas
        /// </summary>
        public dLocalTableAdapters.NovasTableAdapter NovasTableAdapter
        {
            get
            {
                if (novasTableAdapter == null)
                {
                    novasTableAdapter = new dLocalTableAdapters.NovasTableAdapter();
                    novasTableAdapter.TrocarStringDeConexao();
                };
                return novasTableAdapter;
            }
        }

        private dLocalTableAdapters.ContaCorrenteDetalheTableAdapter contaCorrenteDetalheTableAdapter;
        /// <summary>
        /// TableAdapter padrão para tabela: ContaCorrenteDetalhe
        /// </summary>
        public dLocalTableAdapters.ContaCorrenteDetalheTableAdapter ContaCorrenteDetalheTableAdapter
        {
            get
            {
                if (contaCorrenteDetalheTableAdapter == null)
                {
                    contaCorrenteDetalheTableAdapter = new dLocalTableAdapters.ContaCorrenteDetalheTableAdapter();
                    contaCorrenteDetalheTableAdapter.TrocarStringDeConexao();
                };
                return contaCorrenteDetalheTableAdapter;
            }
        }

        private dLocalTableAdapters.ContaCorrenTeTableAdapter contaCorrenTeTableAdapter;
        /// <summary>
        /// TableAdapter padrão para tabela: ContaCorrenTe
        /// </summary>
        public dLocalTableAdapters.ContaCorrenTeTableAdapter ContaCorrenTeTableAdapter
        {
            get
            {
                if (contaCorrenTeTableAdapter == null)
                {
                    contaCorrenTeTableAdapter = new dLocalTableAdapters.ContaCorrenTeTableAdapter();
                    contaCorrenTeTableAdapter.TrocarStringDeConexao();
                };
                return contaCorrenTeTableAdapter;
            }
        }

        private dLocalTableAdapters.SaldoContaCorrenteTableAdapter saldoContaCorrenteTableAdapter;
        /// <summary>
        /// TableAdapter padrão para tabela: SaldoContaCorrente
        /// </summary>
        public dLocalTableAdapters.SaldoContaCorrenteTableAdapter SaldoContaCorrenteTableAdapter
        {
            get
            {
                if (saldoContaCorrenteTableAdapter == null)
                {
                    saldoContaCorrenteTableAdapter = new dLocalTableAdapters.SaldoContaCorrenteTableAdapter();
                    saldoContaCorrenteTableAdapter.TrocarStringDeConexao();
                };
                return saldoContaCorrenteTableAdapter;
            }
        }

        private dLocalTableAdapters.CHEquesTableAdapter cHEquesTableAdapter;
        /// <summary>
        /// TableAdapter padrão para tabela: CHEques
        /// </summary>
        public dLocalTableAdapters.CHEquesTableAdapter CHEquesTableAdapter
        {
            get
            {
                if (cHEquesTableAdapter == null)
                {
                    cHEquesTableAdapter = new dLocalTableAdapters.CHEquesTableAdapter();
                    cHEquesTableAdapter.TrocarStringDeConexao();
                };
                return cHEquesTableAdapter;
            }
        }

        private dLocalTableAdapters.DadosChequeTableAdapter dadosChequeTableAdapter;
        /// <summary>
        /// TableAdapter padrão para tabela: DadosCheque
        /// </summary>
        public dLocalTableAdapters.DadosChequeTableAdapter DadosChequeTableAdapter
        {
            get
            {
                if (dadosChequeTableAdapter == null)
                {
                    dadosChequeTableAdapter = new dLocalTableAdapters.DadosChequeTableAdapter();
                    dadosChequeTableAdapter.TrocarStringDeConexao();
                };
                return dadosChequeTableAdapter;
            }
        }

        private dLocalTableAdapters.SindicosTableAdapter sindicosTableAdapter;
        /// <summary>
        /// TableAdapter padrão para tabela: Sindicos
        /// </summary>
        public dLocalTableAdapters.SindicosTableAdapter SindicosTableAdapter
        {
            get
            {
                if (sindicosTableAdapter == null)
                {
                    sindicosTableAdapter = new dLocalTableAdapters.SindicosTableAdapter();
                    sindicosTableAdapter.TrocarStringDeConexao();
                };
                return sindicosTableAdapter;
            }
        }

        private dLocalTableAdapters.ApagarTableAdapter apagarTableAdapter;
        /// <summary>
        /// TableAdapter padrão para tabela: Apagar
        /// </summary>
        public dLocalTableAdapters.ApagarTableAdapter ApagarTableAdapter
        {
            get
            {
                if (apagarTableAdapter == null)
                {
                    apagarTableAdapter = new dLocalTableAdapters.ApagarTableAdapter();
                    apagarTableAdapter.TrocarStringDeConexao();
                };
                return apagarTableAdapter;
            }
        }

        #endregion

        #region Cria TableAdapters

        private dLocalTableAdapters.INPCTableAdapter iNPCTableAdapter;
        /// <summary>
        /// TableAdapter padrão para tabela: INPC
        /// </summary>
        public dLocalTableAdapters.INPCTableAdapter INPCTableAdapter
        {
            get
            {
                if (iNPCTableAdapter == null)
                {
                    iNPCTableAdapter = new dLocalTableAdapters.INPCTableAdapter();
                    iNPCTableAdapter.TrocarStringDeConexao();
                };
                return iNPCTableAdapter;
            }
        }

        /// <summary>
        /// FORNECEDORESTableAdapter
        /// </summary>
        public dLocalTableAdapters.FORNECEDORESTableAdapter FORNECEDORESTableAdapter
        {
            get
            {
                if (fORNECEDORESTableAdapter == null)
                {
                    fORNECEDORESTableAdapter = new Internet.dLocalTableAdapters.FORNECEDORESTableAdapter();
                    fORNECEDORESTableAdapter.TrocarStringDeConexao(VirDB.Bancovirtual.TiposDeBanco.SQL);
                };
                return fORNECEDORESTableAdapter;
            }
        }

        /*
        public dLocalTableAdapters.PropInqTableAdapter PropInqTableAdapter
        {
            get {
                if (propInqTableAdapter == null)
                {
                    propInqTableAdapter = new Internet.dLocalTableAdapters.PropInqTableAdapter();
                    propInqTableAdapter.TrocarStringDeConexao(VirDB.Bancovirtual.TiposDeBanco.Access);
                };
                return propInqTableAdapter; 
            }
        }*/

        

        /// <summary>
        /// 
        /// </summary>
        public dLocalTableAdapters.CIDADESTableAdapter CIDADESTableAdapter
        {
            get
            {
                if (cIDADESTableAdapter == null)
                {
                    cIDADESTableAdapter = new Internet.dLocalTableAdapters.CIDADESTableAdapter();
                    cIDADESTableAdapter.TrocarStringDeConexao(VirDB.Bancovirtual.TiposDeBanco.SQL);
                };
                return cIDADESTableAdapter;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        public dLocalTableAdapters.PESSOASTableAdapter PESSOASTableAdapter
        {
            get
            {
                if (pESSOASTableAdapter == null)
                {
                    pESSOASTableAdapter = new Internet.dLocalTableAdapters.PESSOASTableAdapter();
                    pESSOASTableAdapter.TrocarStringDeConexao(VirDB.Bancovirtual.TiposDeBanco.SQL);
                };
                return pESSOASTableAdapter;
            }
        }

        /*
        public dLocalTableAdapters.Boleto1TableAdapter Boleto1TableAdapter
        {
            get
            {
                if (boleto1TableAdapter == null)
                {
                    boleto1TableAdapter = new Internet.dLocalTableAdapters.Boleto1TableAdapter();
                    boleto1TableAdapter.TrocarStringDeConexao(VirDB.Bancovirtual.TiposDeBanco.Access);
                };
                return boleto1TableAdapter;
            }
        }*/

        /// <summary>
        /// 
        /// </summary>
        public dLocalTableAdapters.BLOCOSTableAdapter BLOCOSTableAdapter
        {
            get
            {
                if (bLOCOSTableAdapter == null)
                {
                    bLOCOSTableAdapter = new Internet.dLocalTableAdapters.BLOCOSTableAdapter();
                    bLOCOSTableAdapter.TrocarStringDeConexao(VirDB.Bancovirtual.TiposDeBanco.SQL);
                };
                return bLOCOSTableAdapter;
            }

        }

        /// <summary>
        /// 
        /// </summary>
        public dLocalTableAdapters.APARTAMENTOSTableAdapter APARTAMENTOSTableAdapter
        {
            get
            {
                if (aPARTAMENTOSTableAdapter == null)
                {
                    aPARTAMENTOSTableAdapter = new Internet.dLocalTableAdapters.APARTAMENTOSTableAdapter();
                    aPARTAMENTOSTableAdapter.TrocarStringDeConexao(VirDB.Bancovirtual.TiposDeBanco.SQL);
                };
                return aPARTAMENTOSTableAdapter;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        public dLocalTableAdapters.BOLetosTableAdapter BOLetosTableAdapter
        {
            get
            {
                if (bOLetosTableAdapter == null)
                {
                    bOLetosTableAdapter = new Internet.dLocalTableAdapters.BOLetosTableAdapter();
                    bOLetosTableAdapter.TrocarStringDeConexao(VirDB.Bancovirtual.TiposDeBanco.SQL);
                }
                return bOLetosTableAdapter;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        public dLocalTableAdapters.CONDOMINIOSTableAdapter CONDOMINIOSTableAdapter
        {
            get
            {
                if (cONDOMINIOSTableAdapter == null)
                {
                    cONDOMINIOSTableAdapter = new Internet.dLocalTableAdapters.CONDOMINIOSTableAdapter();
                    cONDOMINIOSTableAdapter.TrocarStringDeConexao(VirDB.Bancovirtual.TiposDeBanco.SQL);
                }
                return cONDOMINIOSTableAdapter;
            }
        }
        #endregion

    }
}


