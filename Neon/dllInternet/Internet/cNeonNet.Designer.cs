namespace Internet
{
    partial class cNeonNet
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            DevExpress.XtraGrid.GridLevelNode gridLevelNode1 = new DevExpress.XtraGrid.GridLevelNode();
            DevExpress.XtraGrid.GridLevelNode gridLevelNode2 = new DevExpress.XtraGrid.GridLevelNode();
            this.gridView3 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colnn = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colvencto = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colvalor = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colHonorarios = new DevExpress.XtraGrid.Columns.GridColumn();
            this.retAcordoGridControl = new DevExpress.XtraGrid.GridControl();
            this.retAcordoBindingSource = new System.Windows.Forms.BindingSource();
            this.dIntenet = new Internet.dIntenet();
            this.gridView4 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colnumero = new DevExpress.XtraGrid.Columns.GridColumn();
            this.coltotal = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridView1 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colid = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colcodcon = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colapartamento = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colbloco = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colDESTINATARIO = new DevExpress.XtraGrid.Columns.GridColumn();
            this.coldata = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colJuridico = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colJudicial = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colEfetuado = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colFRN = new DevExpress.XtraGrid.Columns.GridColumn();
            this.FRNLookUpEdit1 = new DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit();
            this.FRNbindingSource = new System.Windows.Forms.BindingSource();
            this.colMenJuridico = new DevExpress.XtraGrid.Columns.GridColumn();
            this.button1 = new System.Windows.Forms.Button();
            this.textBox1 = new System.Windows.Forms.TextBox();
            this.xtraTabControl1 = new DevExpress.XtraTab.XtraTabControl();
            this.xtraTabPage1 = new DevExpress.XtraTab.XtraTabPage();
            this.panelControl1 = new DevExpress.XtraEditors.PanelControl();
            this.button3 = new System.Windows.Forms.Button();
            this.button2 = new System.Windows.Forms.Button();
            this.xtraTabPage2 = new DevExpress.XtraTab.XtraTabPage();
            this.panelControl5 = new DevExpress.XtraEditors.PanelControl();
            this.retClienteBindingSource = new System.Windows.Forms.BindingSource();
            this.dLocal1 = new Internet.dLocal();
            this.simpleButton1 = new DevExpress.XtraEditors.SimpleButton();
            this.labelStatus = new DevExpress.XtraEditors.LabelControl();
            this.progressBarControl1 = new DevExpress.XtraEditors.ProgressBarControl();
            this.simpleButton2 = new DevExpress.XtraEditors.SimpleButton();
            this.simpleButton4 = new DevExpress.XtraEditors.SimpleButton();
            this.panelControl2 = new DevExpress.XtraEditors.PanelControl();
            this.panelControl3 = new DevExpress.XtraEditors.PanelControl();
            this.panelControl4 = new DevExpress.XtraEditors.PanelControl();
            this.panel_de_Teste = new DevExpress.XtraEditors.PanelControl();
            this.simpleButton3 = new DevExpress.XtraEditors.SimpleButton();
            this.simpleButton15 = new DevExpress.XtraEditors.SimpleButton();
            this.simpleButton13 = new DevExpress.XtraEditors.SimpleButton();
            this.simpleButton12 = new DevExpress.XtraEditors.SimpleButton();
            this.simpleButton11 = new DevExpress.XtraEditors.SimpleButton();
            this.simpleButton10 = new DevExpress.XtraEditors.SimpleButton();
            this.simpleButton9 = new DevExpress.XtraEditors.SimpleButton();
            this.simpleButton7 = new DevExpress.XtraEditors.SimpleButton();
            this.simpleButton6 = new DevExpress.XtraEditors.SimpleButton();
            this.button8 = new System.Windows.Forms.Button();
            this.button7 = new System.Windows.Forms.Button();
            this.button6 = new System.Windows.Forms.Button();
            this.button5 = new System.Windows.Forms.Button();
            this.button4 = new System.Windows.Forms.Button();
            this.simpleButton8 = new DevExpress.XtraEditors.SimpleButton();
            this.BtEquipamentos = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.StyleController_F)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.retAcordoGridControl)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.retAcordoBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dIntenet)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.FRNLookUpEdit1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.FRNbindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xtraTabControl1)).BeginInit();
            this.xtraTabControl1.SuspendLayout();
            this.xtraTabPage1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl1)).BeginInit();
            this.panelControl1.SuspendLayout();
            this.xtraTabPage2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.retClienteBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dLocal1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.progressBarControl1.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl2)).BeginInit();
            this.panelControl2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl3)).BeginInit();
            this.panelControl3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl4)).BeginInit();
            this.panelControl4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.panel_de_Teste)).BeginInit();
            this.panel_de_Teste.SuspendLayout();
            this.SuspendLayout();
            // 
            // ToolTipController_F
            // 
            this.ToolTipController_F.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.ToolTipController_F.Appearance.ForeColor = System.Drawing.Color.Black;
            this.ToolTipController_F.Appearance.Options.UseBackColor = true;
            this.ToolTipController_F.Appearance.Options.UseFont = true;
            this.ToolTipController_F.Appearance.Options.UseForeColor = true;
            this.ToolTipController_F.Appearance.Options.UseTextOptions = true;
            this.ToolTipController_F.Appearance.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            // 
            // StyleController_F
            // 
            this.StyleController_F.AppearanceDisabled.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this.StyleController_F.AppearanceDisabled.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this.StyleController_F.AppearanceDisabled.Options.UseBackColor = true;
            this.StyleController_F.AppearanceDisabled.Options.UseForeColor = true;
            this.StyleController_F.AppearanceFocused.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.StyleController_F.AppearanceFocused.Options.UseBackColor = true;
            // 
            // gridView3
            // 
            this.gridView3.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colnn,
            this.colvencto,
            this.colvalor,
            this.colHonorarios});
            this.gridView3.GridControl = this.retAcordoGridControl;
            this.gridView3.Name = "gridView3";
            this.gridView3.OptionsView.ColumnAutoWidth = false;
            this.gridView3.OptionsView.ShowGroupPanel = false;
            this.gridView3.ViewCaption = "Novos Boletos";
            // 
            // colnn
            // 
            this.colnn.Caption = "N�mero";
            this.colnn.FieldName = "nn";
            this.colnn.Name = "colnn";
            this.colnn.Visible = true;
            this.colnn.VisibleIndex = 0;
            // 
            // colvencto
            // 
            this.colvencto.Caption = "Vencimento";
            this.colvencto.FieldName = "vencto";
            this.colvencto.Name = "colvencto";
            this.colvencto.Visible = true;
            this.colvencto.VisibleIndex = 1;
            // 
            // colvalor
            // 
            this.colvalor.Caption = "Valor";
            this.colvalor.DisplayFormat.FormatString = "n2";
            this.colvalor.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.colvalor.FieldName = "valor";
            this.colvalor.Name = "colvalor";
            this.colvalor.Visible = true;
            this.colvalor.VisibleIndex = 2;
            // 
            // colHonorarios
            // 
            this.colHonorarios.Caption = "Honor�rios";
            this.colHonorarios.DisplayFormat.FormatString = "n2";
            this.colHonorarios.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.colHonorarios.FieldName = "Honorarios";
            this.colHonorarios.Name = "colHonorarios";
            this.colHonorarios.Visible = true;
            this.colHonorarios.VisibleIndex = 3;
            // 
            // retAcordoGridControl
            // 
            this.retAcordoGridControl.DataSource = this.retAcordoBindingSource;
            this.retAcordoGridControl.Dock = System.Windows.Forms.DockStyle.Fill;
            gridLevelNode1.LevelTemplate = this.gridView3;
            gridLevelNode1.RelationName = "FK_RetNovosB_RetAcordo";
            gridLevelNode2.LevelTemplate = this.gridView4;
            gridLevelNode2.RelationName = "FK_RetOriginais_RetAcordo";
            this.retAcordoGridControl.LevelTree.Nodes.AddRange(new DevExpress.XtraGrid.GridLevelNode[] {
            gridLevelNode1,
            gridLevelNode2});
            this.retAcordoGridControl.Location = new System.Drawing.Point(0, 0);
            this.retAcordoGridControl.MainView = this.gridView1;
            this.retAcordoGridControl.Name = "retAcordoGridControl";
            this.retAcordoGridControl.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.FRNLookUpEdit1});
            this.retAcordoGridControl.ShowOnlyPredefinedDetails = true;
            this.retAcordoGridControl.Size = new System.Drawing.Size(1471, 492);
            this.retAcordoGridControl.TabIndex = 0;
            this.retAcordoGridControl.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridView4,
            this.gridView1,
            this.gridView3});
            // 
            // retAcordoBindingSource
            // 
            this.retAcordoBindingSource.DataMember = "RetAcordo";
            this.retAcordoBindingSource.DataSource = this.dIntenet;
            // 
            // dIntenet
            // 
            this.dIntenet.DataSetName = "dIntenet";
            this.dIntenet.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // gridView4
            // 
            this.gridView4.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colnumero,
            this.coltotal});
            this.gridView4.GridControl = this.retAcordoGridControl;
            this.gridView4.Name = "gridView4";
            this.gridView4.OptionsView.ColumnAutoWidth = false;
            this.gridView4.OptionsView.ShowGroupPanel = false;
            this.gridView4.ViewCaption = "Boletos Antigos";
            // 
            // colnumero
            // 
            this.colnumero.Caption = "N�mero";
            this.colnumero.FieldName = "numero";
            this.colnumero.Name = "colnumero";
            this.colnumero.Visible = true;
            this.colnumero.VisibleIndex = 0;
            this.colnumero.Width = 125;
            // 
            // coltotal
            // 
            this.coltotal.Caption = "Valor";
            this.coltotal.DisplayFormat.FormatString = "n2";
            this.coltotal.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.coltotal.FieldName = "total";
            this.coltotal.Name = "coltotal";
            this.coltotal.Visible = true;
            this.coltotal.VisibleIndex = 1;
            this.coltotal.Width = 134;
            // 
            // gridView1
            // 
            this.gridView1.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colid,
            this.colcodcon,
            this.colapartamento,
            this.colbloco,
            this.colDESTINATARIO,
            this.coldata,
            this.colJuridico,
            this.colJudicial,
            this.colEfetuado,
            this.colFRN,
            this.colMenJuridico});
            this.gridView1.GridControl = this.retAcordoGridControl;
            this.gridView1.Name = "gridView1";
            this.gridView1.OptionsCustomization.AllowGroup = false;
            this.gridView1.OptionsView.ColumnAutoWidth = false;
            this.gridView1.OptionsView.ShowGroupPanel = false;
            // 
            // colid
            // 
            this.colid.Caption = "id";
            this.colid.FieldName = "id";
            this.colid.Name = "colid";
            this.colid.OptionsColumn.ReadOnly = true;
            this.colid.Visible = true;
            this.colid.VisibleIndex = 1;
            this.colid.Width = 70;
            // 
            // colcodcon
            // 
            this.colcodcon.Caption = "codcon";
            this.colcodcon.FieldName = "codcon";
            this.colcodcon.Name = "colcodcon";
            this.colcodcon.Visible = true;
            this.colcodcon.VisibleIndex = 2;
            this.colcodcon.Width = 90;
            // 
            // colapartamento
            // 
            this.colapartamento.Caption = "apartamento";
            this.colapartamento.FieldName = "apartamento";
            this.colapartamento.Name = "colapartamento";
            this.colapartamento.Visible = true;
            this.colapartamento.VisibleIndex = 4;
            this.colapartamento.Width = 85;
            // 
            // colbloco
            // 
            this.colbloco.Caption = "bloco";
            this.colbloco.FieldName = "bloco";
            this.colbloco.Name = "colbloco";
            this.colbloco.Visible = true;
            this.colbloco.VisibleIndex = 3;
            this.colbloco.Width = 61;
            // 
            // colDESTINATARIO
            // 
            this.colDESTINATARIO.Caption = "DESTINATARIO";
            this.colDESTINATARIO.FieldName = "DESTINATARIO";
            this.colDESTINATARIO.Name = "colDESTINATARIO";
            this.colDESTINATARIO.Visible = true;
            this.colDESTINATARIO.VisibleIndex = 5;
            this.colDESTINATARIO.Width = 97;
            // 
            // coldata
            // 
            this.coldata.Caption = "data";
            this.coldata.FieldName = "data";
            this.coldata.Name = "coldata";
            this.coldata.Visible = true;
            this.coldata.VisibleIndex = 6;
            this.coldata.Width = 98;
            // 
            // colJuridico
            // 
            this.colJuridico.Caption = "Jur�dico";
            this.colJuridico.FieldName = "Juridico";
            this.colJuridico.Name = "colJuridico";
            this.colJuridico.Visible = true;
            this.colJuridico.VisibleIndex = 7;
            this.colJuridico.Width = 62;
            // 
            // colJudicial
            // 
            this.colJudicial.Caption = "Judicial";
            this.colJudicial.FieldName = "Judicial";
            this.colJudicial.Name = "colJudicial";
            this.colJudicial.Visible = true;
            this.colJudicial.VisibleIndex = 8;
            this.colJudicial.Width = 60;
            // 
            // colEfetuado
            // 
            this.colEfetuado.Caption = "Efetuado";
            this.colEfetuado.FieldName = "Efetuado";
            this.colEfetuado.Name = "colEfetuado";
            this.colEfetuado.Visible = true;
            this.colEfetuado.VisibleIndex = 0;
            this.colEfetuado.Width = 62;
            // 
            // colFRN
            // 
            this.colFRN.Caption = "Advogado";
            this.colFRN.ColumnEdit = this.FRNLookUpEdit1;
            this.colFRN.FieldName = "FRN";
            this.colFRN.Name = "colFRN";
            this.colFRN.Visible = true;
            this.colFRN.VisibleIndex = 9;
            this.colFRN.Width = 160;
            // 
            // FRNLookUpEdit1
            // 
            this.FRNLookUpEdit1.AutoHeight = false;
            this.FRNLookUpEdit1.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.FRNLookUpEdit1.DataSource = this.FRNbindingSource;
            this.FRNLookUpEdit1.DisplayMember = "FRNNome";
            this.FRNLookUpEdit1.Name = "FRNLookUpEdit1";
            this.FRNLookUpEdit1.ShowHeader = false;
            this.FRNLookUpEdit1.ValueMember = "FRN";
            // 
            // FRNbindingSource
            // 
            this.FRNbindingSource.DataMember = "FORNECEDORES";
            this.FRNbindingSource.DataSource = typeof(Framework.datasets.dFornecedores);
            // 
            // colMenJuridico
            // 
            this.colMenJuridico.Caption = "Mensagem";
            this.colMenJuridico.FieldName = "MenJuridico";
            this.colMenJuridico.Name = "colMenJuridico";
            this.colMenJuridico.Visible = true;
            this.colMenJuridico.VisibleIndex = 10;
            this.colMenJuridico.Width = 191;
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(5, 5);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(208, 23);
            this.button1.TabIndex = 0;
            this.button1.Text = "Ler dados.";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // textBox1
            // 
            this.textBox1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.textBox1.Location = new System.Drawing.Point(918, 2);
            this.textBox1.Multiline = true;
            this.textBox1.Name = "textBox1";
            this.textBox1.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.textBox1.Size = new System.Drawing.Size(557, 100);
            this.textBox1.TabIndex = 1;
            // 
            // xtraTabControl1
            // 
            this.xtraTabControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.xtraTabControl1.Location = new System.Drawing.Point(0, 104);
            this.xtraTabControl1.Name = "xtraTabControl1";
            this.xtraTabControl1.SelectedTabPage = this.xtraTabPage1;
            this.xtraTabControl1.Size = new System.Drawing.Size(1477, 563);
            this.xtraTabControl1.TabIndex = 2;
            this.xtraTabControl1.TabPages.AddRange(new DevExpress.XtraTab.XtraTabPage[] {
            this.xtraTabPage1,
            this.xtraTabPage2});
            // 
            // xtraTabPage1
            // 
            this.xtraTabPage1.AutoScroll = true;
            this.xtraTabPage1.Controls.Add(this.retAcordoGridControl);
            this.xtraTabPage1.Controls.Add(this.panelControl1);
            this.xtraTabPage1.Name = "xtraTabPage1";
            this.xtraTabPage1.Size = new System.Drawing.Size(1471, 535);
            this.xtraTabPage1.Text = "Acordos";
            // 
            // panelControl1
            // 
            this.panelControl1.Controls.Add(this.button3);
            this.panelControl1.Controls.Add(this.button2);
            this.panelControl1.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panelControl1.Location = new System.Drawing.Point(0, 492);
            this.panelControl1.Name = "panelControl1";
            this.panelControl1.Size = new System.Drawing.Size(1471, 43);
            this.panelControl1.TabIndex = 4;
            // 
            // button3
            // 
            this.button3.Location = new System.Drawing.Point(283, 5);
            this.button3.Name = "button3";
            this.button3.Size = new System.Drawing.Size(272, 23);
            this.button3.TabIndex = 4;
            this.button3.Text = "Cancelar Acordo";
            this.button3.UseVisualStyleBackColor = true;
            this.button3.Click += new System.EventHandler(this.button3_Click);
            // 
            // button2
            // 
            this.button2.Location = new System.Drawing.Point(5, 5);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(272, 23);
            this.button2.TabIndex = 3;
            this.button2.Text = "Efetuar Acordo";
            this.button2.UseVisualStyleBackColor = true;
            this.button2.Click += new System.EventHandler(this.button2_Click);
            // 
            // xtraTabPage2
            // 
            this.xtraTabPage2.Controls.Add(this.panelControl5);
            this.xtraTabPage2.Name = "xtraTabPage2";
            this.xtraTabPage2.Size = new System.Drawing.Size(1582, 535);
            this.xtraTabPage2.Text = "Cadastros";
            // 
            // panelControl5
            // 
            this.panelControl5.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panelControl5.Location = new System.Drawing.Point(0, 489);
            this.panelControl5.Name = "panelControl5";
            this.panelControl5.Size = new System.Drawing.Size(1582, 46);
            this.panelControl5.TabIndex = 3;
            // 
            // retClienteBindingSource
            // 
            this.retClienteBindingSource.DataMember = "RetCliente";
            this.retClienteBindingSource.DataSource = this.dIntenet;
            // 
            // dLocal1
            // 
            this.dLocal1.DataSetName = "dLocal";
            this.dLocal1.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // simpleButton1
            // 
            this.simpleButton1.Location = new System.Drawing.Point(5, 34);
            this.simpleButton1.Name = "simpleButton1";
            this.simpleButton1.Size = new System.Drawing.Size(208, 23);
            this.simpleButton1.TabIndex = 4;
            this.simpleButton1.Text = "Enviar / Receber";
            this.simpleButton1.Visible = false;
            this.simpleButton1.Click += new System.EventHandler(this.simpleButton1_Click);
            // 
            // labelStatus
            // 
            this.labelStatus.Appearance.Font = new System.Drawing.Font("Verdana", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelStatus.Appearance.ForeColor = System.Drawing.Color.Red;
            this.labelStatus.Appearance.Options.UseFont = true;
            this.labelStatus.Appearance.Options.UseForeColor = true;
            this.labelStatus.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.Horizontal;
            this.labelStatus.Location = new System.Drawing.Point(28, 26);
            this.labelStatus.Name = "labelStatus";
            this.labelStatus.Size = new System.Drawing.Size(75, 23);
            this.labelStatus.TabIndex = 6;
            this.labelStatus.Text = "Parado";
            // 
            // progressBarControl1
            // 
            this.progressBarControl1.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.progressBarControl1.Location = new System.Drawing.Point(2, 47);
            this.progressBarControl1.Name = "progressBarControl1";
            this.progressBarControl1.Size = new System.Drawing.Size(1473, 18);
            this.progressBarControl1.TabIndex = 7;
            // 
            // simpleButton2
            // 
            this.simpleButton2.Location = new System.Drawing.Point(394, 5);
            this.simpleButton2.Name = "simpleButton2";
            this.simpleButton2.Size = new System.Drawing.Size(142, 23);
            this.simpleButton2.TabIndex = 9;
            this.simpleButton2.Text = "Concilia��o";
            this.simpleButton2.Click += new System.EventHandler(this.simpleButton2_Click);
            // 
            // simpleButton4
            // 
            this.simpleButton4.Location = new System.Drawing.Point(200, 63);
            this.simpleButton4.Name = "simpleButton4";
            this.simpleButton4.Size = new System.Drawing.Size(91, 23);
            this.simpleButton4.TabIndex = 11;
            this.simpleButton4.Text = "NeonNet";
            this.simpleButton4.Click += new System.EventHandler(this.simpleButton4_Click);
            // 
            // panelControl2
            // 
            this.panelControl2.Controls.Add(this.progressBarControl1);
            this.panelControl2.Controls.Add(this.labelStatus);
            this.panelControl2.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panelControl2.Location = new System.Drawing.Point(0, 667);
            this.panelControl2.Name = "panelControl2";
            this.panelControl2.Size = new System.Drawing.Size(1477, 67);
            this.panelControl2.TabIndex = 12;
            // 
            // panelControl3
            // 
            this.panelControl3.Controls.Add(this.button1);
            this.panelControl3.Controls.Add(this.simpleButton1);
            this.panelControl3.Dock = System.Windows.Forms.DockStyle.Left;
            this.panelControl3.Location = new System.Drawing.Point(2, 2);
            this.panelControl3.Name = "panelControl3";
            this.panelControl3.Size = new System.Drawing.Size(222, 100);
            this.panelControl3.TabIndex = 13;
            // 
            // panelControl4
            // 
            this.panelControl4.Controls.Add(this.textBox1);
            this.panelControl4.Controls.Add(this.panel_de_Teste);
            this.panelControl4.Controls.Add(this.panelControl3);
            this.panelControl4.Dock = System.Windows.Forms.DockStyle.Top;
            this.panelControl4.Location = new System.Drawing.Point(0, 0);
            this.panelControl4.Name = "panelControl4";
            this.panelControl4.Size = new System.Drawing.Size(1477, 104);
            this.panelControl4.TabIndex = 14;
            // 
            // panel_de_Teste
            // 
            this.panel_de_Teste.Controls.Add(this.simpleButton3);
            this.panel_de_Teste.Controls.Add(this.simpleButton15);
            this.panel_de_Teste.Controls.Add(this.simpleButton13);
            this.panel_de_Teste.Controls.Add(this.simpleButton12);
            this.panel_de_Teste.Controls.Add(this.simpleButton11);
            this.panel_de_Teste.Controls.Add(this.simpleButton2);
            this.panel_de_Teste.Controls.Add(this.simpleButton10);
            this.panel_de_Teste.Controls.Add(this.simpleButton9);
            this.panel_de_Teste.Controls.Add(this.simpleButton7);
            this.panel_de_Teste.Controls.Add(this.simpleButton6);
            this.panel_de_Teste.Controls.Add(this.button8);
            this.panel_de_Teste.Controls.Add(this.button7);
            this.panel_de_Teste.Controls.Add(this.simpleButton4);
            this.panel_de_Teste.Controls.Add(this.button6);
            this.panel_de_Teste.Controls.Add(this.button5);
            this.panel_de_Teste.Controls.Add(this.button4);
            this.panel_de_Teste.Controls.Add(this.simpleButton8);
            this.panel_de_Teste.Controls.Add(this.BtEquipamentos);
            this.panel_de_Teste.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel_de_Teste.Location = new System.Drawing.Point(224, 2);
            this.panel_de_Teste.Name = "panel_de_Teste";
            this.panel_de_Teste.Size = new System.Drawing.Size(694, 100);
            this.panel_de_Teste.TabIndex = 16;
            this.panel_de_Teste.Visible = false;
            // 
            // simpleButton3
            // 
            this.simpleButton3.Location = new System.Drawing.Point(542, 63);
            this.simpleButton3.Name = "simpleButton3";
            this.simpleButton3.Size = new System.Drawing.Size(142, 23);
            this.simpleButton3.TabIndex = 30;
            this.simpleButton3.Text = "Tributo Retorno";
            this.simpleButton3.Click += new System.EventHandler(this.simpleButton3_Click);
            // 
            // simpleButton15
            // 
            this.simpleButton15.Location = new System.Drawing.Point(542, 33);
            this.simpleButton15.Name = "simpleButton15";
            this.simpleButton15.Size = new System.Drawing.Size(142, 23);
            this.simpleButton15.TabIndex = 29;
            this.simpleButton15.Text = "Tributo el. rem.";
            this.simpleButton15.Click += new System.EventHandler(this.simpleButton15_Click);
            // 
            // simpleButton13
            // 
            this.simpleButton13.Location = new System.Drawing.Point(542, 5);
            this.simpleButton13.Name = "simpleButton13";
            this.simpleButton13.Size = new System.Drawing.Size(142, 23);
            this.simpleButton13.TabIndex = 27;
            this.simpleButton13.Text = "Peri�dico";
            this.simpleButton13.Click += new System.EventHandler(this.simpleButton13_Click);
            // 
            // simpleButton12
            // 
            this.simpleButton12.Location = new System.Drawing.Point(394, 63);
            this.simpleButton12.Name = "simpleButton12";
            this.simpleButton12.Size = new System.Drawing.Size(142, 23);
            this.simpleButton12.TabIndex = 26;
            this.simpleButton12.Text = "Purge Access";
            this.simpleButton12.Click += new System.EventHandler(this.simpleButton12_Click);
            // 
            // simpleButton11
            // 
            this.simpleButton11.Location = new System.Drawing.Point(394, 33);
            this.simpleButton11.Name = "simpleButton11";
            this.simpleButton11.Size = new System.Drawing.Size(142, 23);
            this.simpleButton11.TabIndex = 25;
            this.simpleButton11.Text = "Pag Eletronico";
            this.simpleButton11.Click += new System.EventHandler(this.simpleButton11_Click);
            // 
            // simpleButton10
            // 
            this.simpleButton10.Location = new System.Drawing.Point(297, 63);
            this.simpleButton10.Name = "simpleButton10";
            this.simpleButton10.Size = new System.Drawing.Size(91, 23);
            this.simpleButton10.TabIndex = 24;
            this.simpleButton10.Text = "Corre��o inad";
            this.simpleButton10.Click += new System.EventHandler(this.simpleButton10_Click);
            // 
            // simpleButton9
            // 
            this.simpleButton9.Location = new System.Drawing.Point(297, 34);
            this.simpleButton9.Name = "simpleButton9";
            this.simpleButton9.Size = new System.Drawing.Size(91, 23);
            this.simpleButton9.TabIndex = 23;
            this.simpleButton9.Text = "Registro";
            this.simpleButton9.Click += new System.EventHandler(this.simpleButton9_Click);
            // 
            // simpleButton7
            // 
            this.simpleButton7.Location = new System.Drawing.Point(297, 5);
            this.simpleButton7.Name = "simpleButton7";
            this.simpleButton7.Size = new System.Drawing.Size(91, 23);
            this.simpleButton7.TabIndex = 22;
            this.simpleButton7.Text = "Francesinha";
            this.simpleButton7.Click += new System.EventHandler(this.simpleButton7_Click);
            // 
            // simpleButton6
            // 
            this.simpleButton6.Location = new System.Drawing.Point(200, 34);
            this.simpleButton6.Name = "simpleButton6";
            this.simpleButton6.Size = new System.Drawing.Size(91, 23);
            this.simpleButton6.TabIndex = 21;
            this.simpleButton6.Text = "INPC Sal�rio";
            this.simpleButton6.Click += new System.EventHandler(this.simpleButton6_Click);
            // 
            // button8
            // 
            this.button8.Location = new System.Drawing.Point(200, 5);
            this.button8.Name = "button8";
            this.button8.Size = new System.Drawing.Size(91, 23);
            this.button8.TabIndex = 20;
            this.button8.Text = "E-mail";
            this.button8.UseVisualStyleBackColor = true;
            this.button8.Click += new System.EventHandler(this.button8_Click);
            // 
            // button7
            // 
            this.button7.Location = new System.Drawing.Point(103, 63);
            this.button7.Name = "button7";
            this.button7.Size = new System.Drawing.Size(91, 23);
            this.button7.TabIndex = 19;
            this.button7.Text = "Boletos";
            this.button7.UseVisualStyleBackColor = true;
            this.button7.Click += new System.EventHandler(this.button7_Click);
            // 
            // button6
            // 
            this.button6.Location = new System.Drawing.Point(103, 34);
            this.button6.Name = "button6";
            this.button6.Size = new System.Drawing.Size(91, 23);
            this.button6.TabIndex = 18;
            this.button6.Text = "Cobran�a";
            this.button6.UseVisualStyleBackColor = true;
            this.button6.Click += new System.EventHandler(this.button6_Click);
            // 
            // button5
            // 
            this.button5.Location = new System.Drawing.Point(103, 5);
            this.button5.Name = "button5";
            this.button5.Size = new System.Drawing.Size(91, 23);
            this.button5.TabIndex = 17;
            this.button5.Text = "Apagar";
            this.button5.UseVisualStyleBackColor = true;
            this.button5.Click += new System.EventHandler(this.button5_Click);
            // 
            // button4
            // 
            this.button4.Location = new System.Drawing.Point(6, 34);
            this.button4.Name = "button4";
            this.button4.Size = new System.Drawing.Size(91, 23);
            this.button4.TabIndex = 16;
            this.button4.Text = "Condom�nios";
            this.button4.UseVisualStyleBackColor = true;
            this.button4.Click += new System.EventHandler(this.button4_Click_1);
            // 
            // simpleButton8
            // 
            this.simpleButton8.Location = new System.Drawing.Point(6, 63);
            this.simpleButton8.Name = "simpleButton8";
            this.simpleButton8.Size = new System.Drawing.Size(91, 23);
            this.simpleButton8.TabIndex = 15;
            this.simpleButton8.Text = "Apartamentos";
            this.simpleButton8.Click += new System.EventHandler(this.simpleButton8_Click);
            // 
            // BtEquipamentos
            // 
            this.BtEquipamentos.Location = new System.Drawing.Point(6, 5);
            this.BtEquipamentos.Name = "BtEquipamentos";
            this.BtEquipamentos.Size = new System.Drawing.Size(91, 23);
            this.BtEquipamentos.TabIndex = 1;
            this.BtEquipamentos.Text = "Equipamentos";
            this.BtEquipamentos.UseVisualStyleBackColor = true;
            this.BtEquipamentos.Click += new System.EventHandler(this.button4_Click);
            // 
            // cNeonNet
            // 
            this.Controls.Add(this.xtraTabControl1);
            this.Controls.Add(this.panelControl4);
            this.Controls.Add(this.panelControl2);
            this.Doc = System.Windows.Forms.DockStyle.Fill;
            this.LookAndFeel.UseDefaultLookAndFeel = false;
            this.Name = "cNeonNet";
            this.Size = new System.Drawing.Size(1477, 734);
            ((System.ComponentModel.ISupportInitialize)(this.StyleController_F)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.retAcordoGridControl)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.retAcordoBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dIntenet)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.FRNLookUpEdit1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.FRNbindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xtraTabControl1)).EndInit();
            this.xtraTabControl1.ResumeLayout(false);
            this.xtraTabPage1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.panelControl1)).EndInit();
            this.panelControl1.ResumeLayout(false);
            this.xtraTabPage2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.panelControl5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.retClienteBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dLocal1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.progressBarControl1.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl2)).EndInit();
            this.panelControl2.ResumeLayout(false);
            this.panelControl2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl3)).EndInit();
            this.panelControl3.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.panelControl4)).EndInit();
            this.panelControl4.ResumeLayout(false);
            this.panelControl4.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.panel_de_Teste)).EndInit();
            this.panel_de_Teste.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.TextBox textBox1;
        private DevExpress.XtraTab.XtraTabControl xtraTabControl1;
        private DevExpress.XtraTab.XtraTabPage xtraTabPage1;
        private DevExpress.XtraTab.XtraTabPage xtraTabPage2;
        private dIntenet dIntenet;
        private System.Windows.Forms.BindingSource retAcordoBindingSource;
        private DevExpress.XtraGrid.GridControl retAcordoGridControl;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView1;
        private DevExpress.XtraGrid.Columns.GridColumn colid;
        private DevExpress.XtraGrid.Columns.GridColumn colcodcon;
        private DevExpress.XtraGrid.Columns.GridColumn colapartamento;
        private DevExpress.XtraGrid.Columns.GridColumn colbloco;
        private DevExpress.XtraGrid.Columns.GridColumn colDESTINATARIO;
        private DevExpress.XtraGrid.Columns.GridColumn coldata;
        private System.Windows.Forms.Button button2;
        private dLocal dLocal1;
        private DevExpress.XtraEditors.SimpleButton simpleButton1;
        private DevExpress.XtraEditors.LabelControl labelStatus;
        private DevExpress.XtraEditors.ProgressBarControl progressBarControl1;
        private DevExpress.XtraEditors.SimpleButton simpleButton2;
        private System.Windows.Forms.BindingSource retClienteBindingSource;
        private DevExpress.XtraEditors.SimpleButton simpleButton4;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView3;
        private DevExpress.XtraGrid.Columns.GridColumn colnn;
        private DevExpress.XtraGrid.Columns.GridColumn colvencto;
        private DevExpress.XtraGrid.Columns.GridColumn colvalor;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView4;
        private DevExpress.XtraGrid.Columns.GridColumn colnumero;
        private DevExpress.XtraGrid.Columns.GridColumn coltotal;
        private DevExpress.XtraEditors.PanelControl panelControl2;
        private DevExpress.XtraEditors.PanelControl panelControl3;
        private DevExpress.XtraEditors.PanelControl panelControl4;
        private DevExpress.XtraEditors.PanelControl panelControl1;
        private DevExpress.XtraEditors.PanelControl panelControl5;
        private DevExpress.XtraGrid.Columns.GridColumn colJuridico;
        private DevExpress.XtraGrid.Columns.GridColumn colJudicial;
        private DevExpress.XtraGrid.Columns.GridColumn colEfetuado;
        private System.Windows.Forms.Button button3;
        private DevExpress.XtraGrid.Columns.GridColumn colFRN;
        private System.Windows.Forms.BindingSource FRNbindingSource;
        private DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit FRNLookUpEdit1;
        private DevExpress.XtraGrid.Columns.GridColumn colHonorarios;
        private DevExpress.XtraGrid.Columns.GridColumn colMenJuridico;
        private DevExpress.XtraEditors.SimpleButton simpleButton8;
        private DevExpress.XtraEditors.PanelControl panel_de_Teste;
        private System.Windows.Forms.Button BtEquipamentos;
        private System.Windows.Forms.Button button4;
        private System.Windows.Forms.Button button5;
        private System.Windows.Forms.Button button6;
        private System.Windows.Forms.Button button7;
        private System.Windows.Forms.Button button8;
        private DevExpress.XtraEditors.SimpleButton simpleButton6;
        private DevExpress.XtraEditors.SimpleButton simpleButton7;
        private DevExpress.XtraEditors.SimpleButton simpleButton9;
        private DevExpress.XtraEditors.SimpleButton simpleButton10;
        private DevExpress.XtraEditors.SimpleButton simpleButton11;
        private DevExpress.XtraEditors.SimpleButton simpleButton12;
        private DevExpress.XtraEditors.SimpleButton simpleButton13;
        private DevExpress.XtraEditors.SimpleButton simpleButton15;
        private DevExpress.XtraEditors.SimpleButton simpleButton3;
    }
}
