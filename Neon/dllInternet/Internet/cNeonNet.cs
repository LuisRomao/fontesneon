/*
LH - 09/05/2014       - 13.2.8.47  - Purge
MR - 28/05/2014 12:30 -            - Publica��o de regras dos Equipamento (Altera��es indicadas por *** MRC - INICIO - RESERVA (28/05/2014 12:30) ***)
MR - 18/03/2015 10:00 -            - Alterado o nome da funcao ReGrasEquipamento para PublicaReGrasEquipamento para manter padr�o de nomenclatura (Altera��es indicadas por *** MRC - INICIO - RESERVA (18/03/2015 10:00) ***)
MR - 21/03/2016 20:00 -            - Registro de boletos separado para Bradesco e Itau (Altera��es indicadas por *** MRC - INICIO (21/03/2016 20:00) ***)
MR - 27/05/2016 12:00 - 15.2.9.41  - Chamada da nova fun��o VerificaReservasSite que recolhe as reservas e cancelamentos do site (anteriormente feito em ReservaEQuipamento) antes de gerar novas grades para evitar duplicadade de grades alteradas que j� est�o reservadas (Altera��es indicadas por *** MRC - INICIO (27/05/2016 12:00) ***)
*/

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using Cadastros;
using Framework;
using System.Transactions;
using System.Data.OleDb;
using VirEnumeracoes;

namespace Internet
{
    /// <summary>
    /// cNeonNet
    /// </summary>
    public partial class cNeonNet : CompontesBasicos.ComponenteBase
    {
        private string Relatorio;
        //private dLocal.PropInqRow PIrow = null;
        //private dLocal.ApartamentoRow Apatarow = null;        
        private int EMPx; 

        /// <summary>
        /// Construtor
        /// </summary>
        public cNeonNet()
        {
            InitializeComponent();
            EMPx = (Framework.DSCentral.EMP == 1) ? 1 : 2;
            FRNbindingSource.DataSource = Framework.datasets.dFornecedores.GetdFornecedoresST("ADV");
            if ((!CompontesBasicosProc.FuncoesBasicasProc.EstaEmProducao) || (Framework.usuarioLogado.UsuarioLogadoNeon.UsuarioLogadoSt.USU == 30))
            {               
                panel_de_Teste.Visible = true;
            }
        }

        private bool LerDadosInternet() {
            textBox1.Text = string.Format("Acordos: {0}\r\n", dIntenet.RetAcordoTableAdapter.Fill(dIntenet.RetAcordo, (short)EMPx));
            dIntenet.RetNovosBTableAdapter.FillBy(dIntenet.RetNovosB, (short)EMPx);
            dIntenet.RetOriginaisTableAdapter.FillBy(dIntenet.RetOriginais, (short)EMPx);
            textBox1.Text += string.Format("Cadastros: {0}\r\n", dIntenet.RetClienteTableAdapter.Fill(dIntenet.RetCliente, (short)EMPx));            
            return true;
        }

        private void button1_Click(object sender, EventArgs e)
        {
            LerDadosInternet();
            
        }

        /*
        private String VerificaIntegridade(dIntenet.RetAcordoRow retacordirow)
        {
            //correcao de acordo errado
            if (dLocal1.AcordoErradoTableAdapter.Fill(dLocal1.AcordoErrado, retacordirow.codcon, retacordirow.bloco, retacordirow.apartamento) == 1) {
                dLocal.AcordoErradoRow eRow = dLocal1.AcordoErrado[0];
                if (!eRow.IsN�mero_AcordoNull() && eRow.IsT1Null() && eRow.IsT2Null() && eRow.IsT3Null())
                    dLocal1.AcordoErradoTableAdapter.MataAcordoErrado(eRow.N�mero_Acordo);
            };
            //

            String Retorno = "";
            //Verifica se o apartamento est� cadastrado
            dLocal1.ApartamentoTableAdapter.Fill(dLocal1.Apartamento, retacordirow.codcon,retacordirow.bloco,retacordirow.apartamento);
            if (dLocal1.Apartamento.Rows.Count == 0)
            {
                Retorno = "Apartamento n�o Cadastrado";
            }
            else
                if (dLocal1.Apartamento.Rows.Count > 1)
                    Retorno = "Mais de um apartamento cadastrado";
                else
                    Apatarow = (dLocal.ApartamentoRow)dLocal1.Apartamento.Rows[0];

            if (Retorno == "")
            {
                //O acordo � para o inquilino mas n�o tem inquilino?                
                if((retacordirow.DESTINATARIO.ToUpper() == "I")
                        &&
                   (dLocal1.ApartamentoTableAdapter.TemInquilino(retacordirow.codcon, retacordirow.bloco, retacordirow.apartamento) != null))
                    Retorno = "Acordo cancelado, Acordo n�o pode ser realizado pois apto n�o tem inquilino";                    
            };

            if (Retorno == "") {
                //j� tem acordo?                  
                if (dLocal1.ApartamentoTableAdapter.JatemAcordo(retacordirow.codcon, retacordirow.apartamento, retacordirow.bloco) != null)                
                    Retorno = "Acordo cancelado, apartamento j� possui acordo no sistema";
            };

            if ((retacordirow.IsJuridicoNull()) || (!retacordirow.Juridico))
              if (Retorno == "") {
                if (dLocal1.ApartamentoTableAdapter.EstaNoJuridico(retacordirow.codcon,retacordirow.apartamento, retacordirow.bloco) != null)                
                    Retorno = "Acordo cancelado, cobran�a no jur�dico";
              };

            if (Retorno == "")
            {
                //Verificar parcelas originais
                foreach (dIntenet.RetOriginaisRow Origrow in retacordirow.GetRetOriginaisRows()) { 
                    //Verifica se a parcela original existe e se esta paga
                    object retornoPago = dLocal1.ApartamentoTableAdapter.ParcelaOriginalPaga(Origrow.numero);
                    if ((retornoPago == null) || (retornoPago == DBNull.Value))
                        Retorno = "Acordo cancelado, pois boleto original n�o foi encontrado (" + Origrow.numero.ToString() + ")";
                    else
                        if ((Boolean)retornoPago)
                            Retorno = "Acordo cancelado, pois boleto original est� pago (" + Origrow.numero.ToString() + ")";
                    if (Retorno != "")
                        break;
                };
            }

            return Retorno;
        }
        */

        /*
        private bool EfetuaCadastro(dIntenet.RetClienteRow retClienteRow,bool Apagar)
        {
            try
            {
                int n;
                int PES;
                if (retClienteRow.proprietario)
                    n = dLocal1.PESSOASTableAdapter.BuscaProprietario(dLocal1.PESSOAS, retClienteRow.CODCON, retClienteRow.bloco, retClienteRow.apartamento);
                else
                    n = dLocal1.PESSOASTableAdapter.BuscaInquilino(dLocal1.PESSOAS, retClienteRow.CODCON, retClienteRow.bloco, retClienteRow.apartamento);
                if (n == 0)
                {
                    Relatorio = "Apartamento n�o encontrado";
                    return false;
                }
                else
                    if (n > 1)
                    {
                        Relatorio = "Apartamento n�o encontrado (2)";
                        return false;
                    }
                    else
                    {
                        int FPG;
                        //using (System.Transactions.TransactionScope Trans = new System.Transactions.TransactionScope(System.Transactions.TransactionScopeOption.RequiresNew))
                        //{
                        bool novo = false;
                        dLocal.PESSOASRow PESSOASRow = (dLocal.PESSOASRow)dLocal1.PESSOAS.Rows[0];
                        if (PESSOASRow.PESNome != retClienteRow.Nome.ToUpper().Trim())
                        {
                            PESSOASRow = (dLocal.PESSOASRow)dLocal1.PESSOAS.NewPESSOASRow();
                            PESSOASRow.PESNome = retClienteRow.Nome.ToUpper().Trim();
                            novo = true;
                        };


                        if (!retClienteRow.IsbairroNull())
                            PESSOASRow.PESBairro = retClienteRow.bairro;
                        if (!retClienteRow.IscepNull())
                            PESSOASRow.PESCep = retClienteRow.cep;
                        if (!retClienteRow.IsEmailNull())
                            PESSOASRow.PESEmail = retClienteRow.Email;

                        PESSOASRow.PESEndereco = retClienteRow.endereco;
                        if (!retClienteRow.Istelefone1Null())
                            PESSOASRow.PESFone1 = retClienteRow.telefone1;
                        if (!retClienteRow.Istelefone2Null())
                            PESSOASRow.PESFone2 = retClienteRow.telefone2;
                        if (!retClienteRow.Istelefone3Null())
                            PESSOASRow.PESFone3 = retClienteRow.telefone3;

                        PESSOASRow.PESSolicitante = "INTERNET";
                        if (retClienteRow.IscidadeNull())
                            retClienteRow.cidade = "";
                        object objCID = dLocal1.CIDADESTableAdapter.BuscaCidade(retClienteRow.cidade.Trim());
                        int CID;
                        if (objCID == null)
                        {
                            dLocal1.CIDADES.Clear();
                            dLocal.CIDADESRow novaCidade = dLocal1.CIDADES.NewCIDADESRow();
                            novaCidade.CIDDataInclusao = DateTime.Now;
                            if (retClienteRow.IscidadeNull())
                                novaCidade.CIDNome = retClienteRow.cidade.Trim();
                            if (retClienteRow.IsestadoNull())
                                novaCidade.CIDUf = retClienteRow.estado;
                            dLocal1.CIDADES.AddCIDADESRow(novaCidade);
                            dLocal1.CIDADESTableAdapter.Update(novaCidade);
                            CID = novaCidade.CID;
                        }
                        else
                            CID = (int)objCID;
                        PESSOASRow.PES_CID = CID;
                        PESSOASRow.PESDataAtualizacao = DateTime.Now;
                        if (novo)
                            dLocal1.PESSOAS.AddPESSOASRow(PESSOASRow);
                        dLocal1.PESSOASTableAdapter.Update(PESSOASRow);
                        PES = PESSOASRow.PES;
                        dLocal1.APARTAMENTOSTableAdapter.BuscaAPT(dLocal1.APARTAMENTOS, retClienteRow.bloco, retClienteRow.CODCON, retClienteRow.apartamento);
                        dLocal.APARTAMENTOSRow ApartamentosRow = (dLocal.APARTAMENTOSRow)dLocal1.APARTAMENTOS.Rows[0];



                        if (retClienteRow.proprietario)
                        {
                            ApartamentosRow.APTBEmailP = retClienteRow.bemail;
                            ApartamentosRow.APTProprietario_PES = PES;
                            FPG = ApartamentosRow.APTFormaPagtoProprietario_FPG = (retClienteRow.correio ? 2 : 4);
                        }
                        else
                        {
                            ApartamentosRow.APTBEmailI = retClienteRow.bemail;
                            ApartamentosRow.APTInquilino_PES = PES;
                            FPG = ApartamentosRow.APTFormaPagtoInquilino_FPG = (retClienteRow.correio ? 2 : 4);
                        };
                        dLocal1.APARTAMENTOSTableAdapter.Update(ApartamentosRow);
                        int APT;
                        string ComandoBuscaAPT = "SELECT     APARTAMENTOS.APT\r\n" +
                                                 "FROM         APARTAMENTOS INNER JOIN\r\n" +
                                                 "BLOCOS ON APARTAMENTOS.APT_BLO = BLOCOS.BLO INNER JOIN\r\n" +
                                                 "CONDOMINIOS ON BLOCOS.BLO_CON = CONDOMINIOS.CON\r\n" +
                                                 "WHERE     (CONDOMINIOS.CONCodigo = @P1) AND (BLOCOS.BLOCodigo = @P2) AND (APARTAMENTOS.APTNumero = @P3)";
                        if (TableAdapter.ST().BuscaEscalar(ComandoBuscaAPT, out APT, retClienteRow.CODCON, retClienteRow.bloco, retClienteRow.apartamento))
                        {
                            string Comando;
                            string Comando1;
                            if (retClienteRow.proprietario)
                            {
                                Comando = "UPDATE APARTAMENTOS SET APTUsuarioInternetProprietario = @P1 , APTSenhaInternetProprietario =@P2 Where APT = @P3";
                                Comando1 = "UPDATE [PROPRIET�RIO] SET [usuario internet] = @P1 , [senha internet] = @P2 Where CODCON = @P3 and BLOCO = @P4 and APARTAMENTO = @P5";
                            }
                            else
                            {
                                Comando = "UPDATE APARTAMENTOS SET APTUsuarioInternetInquilino = @P1, APTSenhaInternetInquilino = @P2 Where APT = @P3";
                                Comando1 = "UPDATE [INQUILINO] SET [usuario internet] = @P1 , [senha internet] = @P2 Where CODCON = @P3 and BLOCO = @P4 and APARTAMENTO = @P5";
                            }
                            TableAdapter.ST().ExecutarSQLNonQuery(Comando, retClienteRow.usuariointernet, retClienteRow.senhainternet, APT);
                            VirOleDb.TableAdapter.STTableAdapter.ExecutarSQLNonQuery(Comando1, retClienteRow.usuariointernet, retClienteRow.senhainternet, retClienteRow.CODCON, retClienteRow.bloco, retClienteRow.apartamento);
                        };
                        //retClienteRow.usuariointernet
                        //    Trans.Complete();
                        //};
                        Exp.ExportarInquilinoProprietarioParaMsAccess(retClienteRow.proprietario, PES, retClienteRow.CODCON, retClienteRow.bloco, retClienteRow.apartamento, FPG);



                        if (Apagar)
                        {
                            retClienteRow.Delete();
                            //MessageBox.Show("Destravar");
                            dIntenet.RetClienteTableAdapter.Update(retClienteRow);
                        };



                        return true;
                    };
            }
            catch (Exception e)
            {
                Relatorio = e.Message;
                return false;
            };
               
        }

        
        private Cadastros.Utilitarios.MSAccess.fExportToMsAccess exp;

        private Cadastros.Utilitarios.MSAccess.fExportToMsAccess Exp {
            get {
                if (exp == null)
                    exp = new Cadastros.Utilitarios.MSAccess.fExportToMsAccess();
                return exp;
            }
        }
        */

        //System.Collections.ArrayList Apagar = new System.Collections.ArrayList();
        //private OleDbConnection ConexaoLocal;

        private object BrancoParaNull(Object Entrada)
        {
            if (Entrada == null)
                return DBNull.Value;
            else if (Entrada.ToString().Trim() == "")
                return DBNull.Value;
            else
                return Entrada;
        }

        /*
        private bool PossuiAcordoAntigo(Framework.objetosNeon.Apartamento Ap)
        {
            string BuscaAcordoAntigo = "SELECT     [N�mero Acordo]\r\n" +
                                       "FROM         Acordo\r\n" +
                                       "WHERE     (Codcon = @P1 ) AND (Bloco = @P3 ) AND (Apartamento = @P2 )";
            return (VirOleDb.TableAdapter.STTableAdapter.EstaCadastrado(BuscaAcordoAntigo, Ap.CONCodigo, Ap.BLOCodigo, Ap.APTNumero));               
        }
        */
        /*
        private bool EfetuaAcordoManual(dIntenet.RetAcordoRow retacordirow)
        {
            Framework.objetosNeon.Apartamento Apartamento = new Framework.objetosNeon.Apartamento(retacordirow.codcon, retacordirow.bloco, retacordirow.apartamento);
            
            if (!Apartamento.Encontrado)
            {
                Relatorio += "Apartamento n�o Cadastrado";
                return false;
            }

            
            bool Dojuridico = false;
            if(!retacordirow.IsJuridicoNull() && retacordirow.Juridico)
                Dojuridico = true;
            Boletos.Acordo.Acordo NovoAcordo = new Boletos.Acordo.Acordo(Apartamento.APT,true,Dojuridico);
            if(NovoAcordo.Gerado){
                Relatorio += "Apartamento j� possui acordo:"+NovoAcordo.LinhaMae.ACO.ToString()+" Status:"+NovoAcordo.LinhaMae.ACOStatus.ToString();
                return false;
            };

            if (retacordirow.IsJuridicoNull())
                NovoAcordo.Juridico = false;
            else
            {
                if(retacordirow.Juridico){
                    NovoAcordo.Juridico = true;
                    int FRN = -1;
                    if (!retacordirow.IsFRNNull())
                        FRN = retacordirow.FRN;
                    else
                    {
                        if (!TableAdapter.ST().BuscaEscalar("select APTJur_FRN from AParTamentos where APT = @P1", out FRN, Apartamento.APT))
                            TableAdapter.ST().BuscaEscalar("select CONEscrit_FRN from condominios where CON = @P1", out FRN, Apartamento.CON);                        
                    }
                    if (FRN > 0)
                        NovoAcordo.FRN = FRN;
                }
                else
                    NovoAcordo.Juridico = false;
            };
            
            if (retacordirow.IsJudicialNull())
                NovoAcordo.Judicial = false;
            else
                NovoAcordo.Judicial = retacordirow.Judicial;
            

            //carregando os dados ORIGINAIS

                    
            foreach (Boletos.Acordo.dAcordo.BOLetosRow rowOrigL in NovoAcordo.dAcordo.BOLetos)
                rowOrigL.Incluir = false;

            foreach (dIntenet.RetOriginaisRow Origrow in retacordirow.GetRetOriginaisRows()) {
                Boletos.Acordo.dAcordo.BOLetosRow rowOrig = NovoAcordo.dAcordo.BOLetos.FindByBOL(Origrow.numero);
                if (rowOrig == null)
                {
                    Boletos.Boleto.Boleto BolSumido = new Boletos.Boleto.Boleto(Origrow.numero);
                    if(!BolSumido.Encontrado)
                           Relatorio += "Boleto "+Origrow.numero+" n�o encontrado";
                    else
                           if (!BolSumido.rowPrincipal.IsBOLPagamentoNull())
                               Relatorio += "Boleto " + Origrow.numero + " pago em " + BolSumido.rowPrincipal.BOLPagamento.ToString();
                           else
                           {
                               Relatorio += "Boleto " + Origrow.numero + " n�o est� presente (comunique o suporte)\r\n";                               
                               Relatorio += "Status do boleto:" + BolSumido.rowPrincipal.BOLCancelado.ToString() + "\r\n";
                               Relatorio += "Apartamento:" + Apartamento.APT.ToString() + "\r\nBoletos encontrados:\r\n";

                               foreach (Boletos.Acordo.dAcordo.BOLetosRow rowOrigL in NovoAcordo.dAcordo.BOLetos)
                                   Relatorio += rowOrigL.BOL.ToString() + "\r\n";
                           }
                    return false;
                };               
                rowOrig.Incluir = true;
                rowOrig.SubTotal = Origrow.total;
                rowOrig.incValOriginal = rowOrig.BOLValorPrevisto;
            };

            decimal TotalAcordado = 0;
            foreach (dIntenet.RetNovosBRow Novrow in retacordirow.GetRetNovosBRows())
            {                 
                 Boletos.Acordo.dAcordo.GeraNovosRow NovaLinha = NovoAcordo.dAcordo.GeraNovos.NewGeraNovosRow();
                 NovaLinha.Data = Novrow.vencto;
                 if (!Novrow.IsHonorariosNull())
                 {
                     NovaLinha.Honorarios = Novrow.Honorarios;
                     if (Novrow.valor < Novrow.Honorarios)
                     {
                         Relatorio += string.Format("Valor incorreto de honorarios: parcela: {0:n2} honor�rios {1:n2}", Novrow.valor, Novrow.Honorarios);
                         return false;
                     }                     
                 }
                 else                 
                     NovaLinha.Honorarios = 0;
                 TotalAcordado += Novrow.valor - NovaLinha.Honorarios;
                 NovaLinha.Valor = Novrow.valor;
                 NovaLinha.BOL = Novrow.nn;                 
                 NovoAcordo.dAcordo.GeraNovos.AddGeraNovosRow(NovaLinha);
                 
            }

            NovoAcordo.dAcordo.DistribuiEncargos(TotalAcordado, false); 

            if (NovoAcordo.Gerar(dllImpresso.Botoes.Botao.nulo))
            {
                retacordirow.Efetuado = true;
                dIntenet.RetAcordoTableAdapter.Update(retacordirow);
                retacordirow.AcceptChanges();
                return true;
            }
            else
            {
                Relatorio += NovoAcordo.UltimoErro+"(*)";
                return false;
            };            
        }
        */
        /*
        private bool EfetuaAcordoAntigo(dIntenet.RetAcordoRow retacordirow) {
            string Retorno = VerificaIntegridade(retacordirow);
      //      System.Data.OleDb.OleDbTransaction Trans = null;
            try
            {
                if (Retorno == "")
                {
                    
//                    if (PropInqTableAdapter1.Connection.State == ConnectionState.Closed)
  //                      PropInqTableAdapter1.Connection.Open();
    //                Trans = PropInqTableAdapter1.Connection.BeginTransaction();
                    if (retacordirow.DESTINATARIO.ToUpper() == "P")
                        dLocal1.PropInqTableAdapter.FillProprietario(dLocal1.PropInq, retacordirow.codcon, retacordirow.apartamento, retacordirow.bloco);
                    else
                        dLocal1.PropInqTableAdapter.FilInquilino(dLocal1.PropInq, retacordirow.codcon, retacordirow.apartamento, retacordirow.bloco);
                    if (dLocal1.PropInq.Rows.Count != 1)
                        Retorno = "Acordo cancelado, Dados do propriet�rio/inquilino n�o encontrados no sistema";
                    else
                    {
                        PIrow = (dLocal.PropInqRow)dLocal1.PropInq.Rows[0];
                    };
                    if (Retorno == "")
                    {
                        string ender;
                        if (PIrow.correio)
                            ender = PIrow.ender + " CEP: " + PIrow.cep + " " + PIrow.bairro + " " + PIrow.cidade + " - " + PIrow.estado;
                        else
                            ender = "Entrega Pessoal";
                        DateTime datagravar = Convert.ToDateTime(retacordirow.data);
                        dLocal.AcordoRow novaArow = dLocal1.Acordo.AddAcordoRow(retacordirow.codcon, retacordirow.apartamento, retacordirow.bloco, retacordirow.DESTINATARIO, false, false);
                        dLocal1.ApartamentoTableAdapter.Connection.Open();
                        if (AcordoTableAdapter1 == null)
                        {
                            AcordoTableAdapter1 = new dLocalTableAdapters.AcordoTableAdapter();
                            AcordoTableAdapter1.Tipo = VirDB.Bancovirtual.TiposDeBanco.Access;
                            AcordoTableAdapter1.TrocarStringDeConexao();
                            AcordoDividasTableAdapter1 = new Internet.dLocalTableAdapters.AcordoDividasTableAdapter();
                            AcordoDividasTableAdapter1.Tipo = VirDB.Bancovirtual.TiposDeBanco.Access;
                            AcordoDividasTableAdapter1.TrocarStringDeConexao();
                            AcordoParcelasTableAdapter1 = new dLocalTableAdapters.AcordoParcelasTableAdapter();
                            AcordoParcelasTableAdapter1.Tipo = VirDB.Bancovirtual.TiposDeBanco.Access;
                            AcordoParcelasTableAdapter1.TrocarStringDeConexao();
                            BoletoTableAdapter1 = new dLocalTableAdapters.BoletoTableAdapter();
                            BoletoTableAdapter1.Tipo = VirDB.Bancovirtual.TiposDeBanco.Access;
                            BoletoTableAdapter1.TrocarStringDeConexao();
                            BoletoPagamentosTableAdapter1 = new dLocalTableAdapters.BoletoPagamentosTableAdapter();
                            BoletoPagamentosTableAdapter1.Tipo = VirDB.Bancovirtual.TiposDeBanco.Access;
                            BoletoPagamentosTableAdapter1.TrocarStringDeConexao();
                        };
                        AcordoTableAdapter1.Connection.Open();
                        AcordoDividasTableAdapter1.Connection.Open();
                        AcordoParcelasTableAdapter1.Connection.Open();
                        BoletoTableAdapter1.Connection.Open();
                        BoletoPagamentosTableAdapter1.Connection.Open();
                        using (System.Transactions.TransactionScope Trans = new TransactionScope())
                        {
                            foreach (dIntenet.RetOriginaisRow Origrow in retacordirow.GetRetOriginaisRows())
                            {
                                double valor = Convert.ToDouble(Origrow.total);
                                dLocal1.AcordoDividas.AddAcordoDividasRow(valor, novaArow, Origrow.numero);
                                dLocal1.ApartamentoTableAdapter.InsertBoletosRec(Origrow.numero);
                                dLocal1.ApartamentoTableAdapter.InsertBoletosPagRec(Origrow.numero);
                                dLocal1.ApartamentoTableAdapter.ApagaBoletos(Origrow.numero);
                                dLocal1.ApartamentoTableAdapter.ApagaBoletosPag(Origrow.numero);

                            };
                            int prestacao = 1;
                            dIntenet.RetNovosBRow[] NovosBoletos = retacordirow.GetRetNovosBRows();
                            int totprest = NovosBoletos.Length;
                            foreach (dIntenet.RetNovosBRow Novrow in NovosBoletos)
                            {
                                float valor = (float)(Novrow.valor);
                                String mensagem = "- Banco n�o est� autorizado a receber ap�s 30 dias da data de vencimento.\r\n" +
                                                  "-N�o conceder abatimento.\r\n" +
                                                  "-Quita��o v�lida apenas ap�s compensa��o do cheque.\r\n" +
                                                  "-Ap�s o vencimento cobrar multa de" +
                                                  ((Apatarow.TipoDeMulta.ToUpper() == "D") ? " 0" : " ") +
                                                  Apatarow.Multa +
                                                  ((Apatarow.TipoDeMulta.ToUpper() == "D" ? "% ao Dia." : " %."));
                                dLocal1.AcordoParcelas.AddAcordoParcelasRow(prestacao, novaArow, Novrow.valor, Novrow.vencto, Novrow.nn);
                                dLocal1.Boleto.AddBoletoRow(Novrow.nn, Novrow.nn, retacordirow.codcon, retacordirow.apartamento, retacordirow.bloco,
                                    retacordirow.DESTINATARIO, "A", datagravar, Novrow.vencto, Apatarow.Multa, valor, "0101", PIrow.correio,
                                    ender, PIrow.nome, mensagem, Apatarow.TipoDeMulta, Novrow.cc, Novrow.ag, Novrow.banco);
                                String Titulo = "Parcela:" + prestacao.ToString() + " de:" + totprest.ToString();
                                dLocal1.BoletoPagamentos.AddBoletoPagamentosRow(Novrow.nn, "111000", valor, retacordirow.DESTINATARIO, Titulo);
                                prestacao++;
                            };

                            

                            AcordoTableAdapter1.Update(dLocal1.Acordo);
                            novaArow.numero = (int)AcordoTableAdapter1.RecuperaNumero(novaArow.Codcon, novaArow.Apartamento, novaArow.Bloco);

                            novaArow.EndEdit();


                            AcordoDividasTableAdapter1.Update(dLocal1.AcordoDividas);
                            AcordoParcelasTableAdapter1.Update(dLocal1.AcordoParcelas);
                            BoletoTableAdapter1.Update(dLocal1.Boleto);
                            BoletoPagamentosTableAdapter1.Update(dLocal1.BoletoPagamentos);

                            dLocal1.Acordo.AcceptChanges();
                            dLocal1.AcordoDividas.AcceptChanges();
                            dLocal1.AcordoParcelas.AcceptChanges();
                            dLocal1.BoletoPagamentos.AcceptChanges();
                            dLocal1.Boleto.AcceptChanges();
                            Trans.Complete();
                        }/////////////
                    }
            //        Trans.Commit();
                    if (Apagar)
                    {
                        retacordirow.Delete();
                        //MessageBox.Show("destravar");
                        dIntenet.RetAcordoTableAdapter.Update(retacordirow);
                    };
                    return true;
                }
                else
                {
                    Relatorio += Retorno;
                    return false;
                }
            }
            catch (Exception e)
            {
                Relatorio = e.Message;
        //        if (Trans != null)
          //          Trans.Rollback();
                return false;
            };

        }

        */
        private void EfetuarAcordoManual() {

            if (retAcordoBindingSource.Current == null)
                return;
            DataRowView DRV = (DataRowView)retAcordoBindingSource.Current;
            if (DRV != null)
            {

                dIntenet.RetAcordoRow RetAcordoRow = (dIntenet.RetAcordoRow)DRV.Row;
                if (RetAcordoRow.IsEfetuadoNull() || RetAcordoRow.Efetuado == false)
                {
                    Relatorio = RetAcordoRow.codcon + " - " + RetAcordoRow.bloco + " - " + RetAcordoRow.apartamento + " ";
                    if (NeonNet.NeonNetSt.EfetuaAcordo(RetAcordoRow))
                        textBox1.Text += "Acordo: " + Relatorio + " OK\r\n";
                    else
                        textBox1.Text += "Acordo: " + Relatorio + " ====================> Erro\r\n";
                }
            }
                
                
               
            
        }



        private void button2_Click(object sender, EventArgs e)
        {
            EfetuarAcordoManual();
            
        }

        /*
        private void RecebAcordos(){
            progressBarControl1.Properties.Maximum = dIntenet.RetAcordoTableAdapter.Fill(dIntenet.RetAcordo,(short)EMPx);
            dIntenet.RetNovosBTableAdapter.FillBy(dIntenet.RetNovosB,EMPx);
            dIntenet.RetOriginaisTableAdapter.FillBy(dIntenet.RetOriginais, EMPx);
            progressBarControl1.Position = 0;
            progressBarControl1.Visible = true;
            labelStatus.Text = "Recebendo Acordos";
            Application.DoEvents();
            System.Collections.ArrayList Apagar = new System.Collections.ArrayList();
            foreach (dIntenet.RetAcordoRow RetAcordoRow in dIntenet.RetAcordo)
            {
                if (RetAcordoRow.IsEfetuadoNull() || RetAcordoRow.Efetuado == false)
                {
                    Relatorio = "Acordo: " + RetAcordoRow.codcon + " - " + RetAcordoRow.bloco + " - " + RetAcordoRow.apartamento;
                    if (EfetuaAcordoManual(RetAcordoRow))
                    {
                        textBox1.Text += "Acordo: " + Relatorio + " OK\r\n";
                        //Apagar.Add(RetAcordoRow);
                    }
                    else
                        textBox1.Text += "Cadatro: " + Relatorio + " ====================> Erro\r\n";
                    progressBarControl1.Position++;
                    Application.DoEvents();
                }
            }            
        }
        */
        /*
        private void RecebCadastros(){
            progressBarControl1.Properties.Maximum = dIntenet.RetClienteTableAdapter.Fill(dIntenet.RetCliente, EMPx);
            progressBarControl1.Position = 0;
            progressBarControl1.Visible = true;
            labelStatus.Text = "Recebendo Cadastros";
            Application.DoEvents();
            System.Collections.ArrayList Apagar = new System.Collections.ArrayList();
            foreach (dIntenet.RetClienteRow RetClienteRow in dIntenet.RetCliente)
            {
                Relatorio = RetClienteRow.CODCON + " - " + RetClienteRow.bloco + " - " + RetClienteRow.apartamento;
                if (EfetuaCadastro(RetClienteRow, false))
                {
                    textBox1.Text += "Cadatro: " + Relatorio + " OK\r\n";
                    Apagar.Add(RetClienteRow);
                }
                else
                    textBox1.Text += "Cadatro: " + Relatorio + " ====================> Erro\r\n";
                progressBarControl1.Position++;
                Application.DoEvents();
            }
            foreach (dIntenet.RetClienteRow RetClienteRowAp in Apagar)
            {
                RetClienteRowAp.Delete();
                //MessageBox.Show("Destravar");
                dIntenet.RetClienteTableAdapter.Update(RetClienteRowAp);
            };
        }
        */

        #region Envio de dados

        private void AtFRN()
        {
            bool Inclusao;
            dIntenet.FORNECEDORESRow RowRemota;
            labelStatus.Text = "Imobili�rias";
            dLocal1.FORNECEDORESTableAdapter.Fill(dLocal1.FORNECEDORES);
            int nCondominioI = 0;
            int nCondominioU = 0;
            progressBarControl1.Properties.Maximum = dLocal1.FORNECEDORES.Rows.Count;
            progressBarControl1.Position = 0;
            progressBarControl1.Visible = true;
            foreach (dLocal.FORNECEDORESRow FORNECEDORESRow in dLocal1.FORNECEDORES)
            {
                dIntenet.FORNECEDORESTableAdapter.Fill(dIntenet.FORNECEDORES, Framework.DSCentral.EMP,FORNECEDORESRow.FRN);
                Inclusao = (dIntenet.FORNECEDORES.Rows.Count == 0);
                if (Inclusao)
                    RowRemota = dIntenet.FORNECEDORES.NewFORNECEDORESRow();
                else
                    RowRemota = (dIntenet.FORNECEDORESRow)dIntenet.FORNECEDORES.Rows[0];
                RowRemota.FRN_EMP = Framework.DSCentral.EMP;
                foreach (DataColumn Coluna in dIntenet.FORNECEDORES.Columns)
                    if (Coluna.ColumnName != "FRN_EMP")
                        RowRemota[Coluna] = FORNECEDORESRow[Coluna.ColumnName];
                if (Inclusao)
                    dIntenet.FORNECEDORES.AddFORNECEDORESRow(RowRemota);
                dIntenet.FORNECEDORESTableAdapter.Update(RowRemota);
                FORNECEDORESRow.FRN__INTERNET = "L";
                dLocal1.FORNECEDORESTableAdapter.Update(FORNECEDORESRow);
                progressBarControl1.Position++;
                if (Inclusao)
                    nCondominioI++;
                else
                    nCondominioU++;
                Application.DoEvents();
            };
            textBox1.Text += "Fornecedores:" + nCondominioI.ToString() + "/" + nCondominioU.ToString() + "\r\n";
        }

        /*
        private void AtCON()
        {            
            //dIntenet.CONDOMINIOSRow RowRemota;
            labelStatus.Text = "Condom�nios";

            dLocal1.CONDOMINIOSTableAdapter.FillBy(dLocal1.CONDOMINIOS, Framework.DSCentral.EMP);
            int nCondominioI = 0;
            int nCondominioU = 0;
            progressBarControl1.Properties.Maximum = dLocal1.CONDOMINIOS.Rows.Count;
            progressBarControl1.Position = 0;
            progressBarControl1.Visible = true;
            dIntenet.CONDOMINIOSTableAdapter.FillBy(dIntenet.CONDOMINIOS, Framework.DSCentral.EMP);
            WSSincBancos.dBancos dBanco = WS.LeCondominios(Criptografa());
            try
            {
                TableAdapter.ST().IniciaTrasacaoST(dLocal1.CONDOMINIOSTableAdapter);
                foreach (dLocal.CONDOMINIOSRow CONDOMINIOSRow in dLocal1.CONDOMINIOS)
                {
                    RowRemota = dIntenet.CONDOMINIOS.FindByCON_EMPCON(Framework.DSCentral.EMP, CONDOMINIOSRow.CON);
                    Inclusao = (RowRemota == null);
                    if (Inclusao)
                        RowRemota = dIntenet.CONDOMINIOS.NewCONDOMINIOSRow();                    
                    RowRemota.CON_EMP = Framework.DSCentral.EMP;
                    foreach (DataColumn Coluna in dIntenet.CONDOMINIOS.Columns)
                        //if(Coluna.ColumnName != "CON_EMP")
                        RowRemota[Coluna] = CONDOMINIOSRow[Coluna.ColumnName];
                    if (Inclusao)
                        dIntenet.CONDOMINIOS.AddCONDOMINIOSRow(RowRemota);

                    //Antiga
                    WSSincBancos.dBancos.condominiosRow WSrow = dBanco.condominios.FindByCODCON(CONDOMINIOSRow.CONCodigo);
                    Inclusao = (WSrow == null);
                    if (CONDOMINIOSRow.CONExportaInternet)
                    {
                        if (Inclusao)
                        {
                            WSrow = dBanco.condominios.NewcondominiosRow();
                            WSrow.CODCON = CONDOMINIOSRow.CONCodigo;
                        };

                        WSrow.Nome = CONDOMINIOSRow.CONNome;
                        if (!CONDOMINIOSRow.IsCON_BCONull())
                            WSrow.NumBanco = CONDOMINIOSRow.CON_BCO.ToString("000");
                        if ((!CONDOMINIOSRow.IsCONValorMultaNull()) && (!CONDOMINIOSRow.IsCONTipoMultaNull()))
                            WSrow.Multasql = string.Format("{0},'{1}'", CONDOMINIOSRow.CONValorMulta.ToString("0.0").Replace(",", "."), CONDOMINIOSRow.CONTipoMulta);
                        if ((!CONDOMINIOSRow.IsCONContaNull()) && (!CONDOMINIOSRow.IsCONDigitoContaNull()))
                            WSrow.cc = CONDOMINIOSRow.CONConta + CONDOMINIOSRow.CONDigitoConta;
                        if (!CONDOMINIOSRow.IsCONAgenciaNull())
                            WSrow.Agencia = CONDOMINIOSRow.CONAgencia;
                        if (!CONDOMINIOSRow.IsCONDigitoAgenciaNull())
                            WSrow.dgagencia = CONDOMINIOSRow.CONDigitoAgencia;
                        WSrow.End = CONDOMINIOSRow.CONEndereco;
                        WSrow.Bairro = CONDOMINIOSRow.CONBairro;
                        if (!CONDOMINIOSRow.IsCIDNomeNull())
                            WSrow.Cidade = CONDOMINIOSRow.CIDNome;
                        if (!CONDOMINIOSRow.IsCIDUfNull())
                            WSrow.Estado = CONDOMINIOSRow.CIDUf;
                        if (!CONDOMINIOSRow.IsCONCepNull())
                            WSrow.CEP = CONDOMINIOSRow.CONCep;
                        if (!CONDOMINIOSRow.IsCONCnpjNull())
                            WSrow.CNPJ = CONDOMINIOSRow.CONCnpj;
                        if (!CONDOMINIOSRow.IsPESNomeNull())
                            WSrow.Sindico = CONDOMINIOSRow.PESNome;
                        if (!CONDOMINIOSRow.IsCONDataMandatoNull())
                            WSrow.mandato = CONDOMINIOSRow.CONDataMandato;
                        if (!CONDOMINIOSRow.IsCONObservacaoInternetNull())
                            WSrow.OBS = CONDOMINIOSRow.CONObservacaoInternet;
                        if (Inclusao)
                            dBanco.condominios.AddcondominiosRow(WSrow);
                    }
                    else
                    {
                        //remover os que nao estao mais publicados
                    }
                    CONDOMINIOSRow.CON__INTERNET = "L";
                    dLocal1.CONDOMINIOSTableAdapter.Update(CONDOMINIOSRow);
                    progressBarControl1.Position++;
                    if (Inclusao)
                        nCondominioI++;
                    else
                        nCondominioU++;
                    Application.DoEvents();
                };
                dIntenet.CONDOMINIOSTableAdapter.Update(dIntenet.CONDOMINIOS);
                VirMSSQL.TableAdapter.ST().Commit();
            }
            catch (Exception e)
            {
                VirMSSQL.TableAdapter.ST().Vircatch(e);               
                textBox1.Text += "Erro";
            }
            textBox1.Text += string.Format("Condom�nos alterados: {0} Inclu�dos: {1}\r\n", nCondominioU,nCondominioI);
        }
        */

        private void AtBLO()
        {
            bool Inclusao;
            dIntenet.BLOCOSRow RowRemota;
            labelStatus.Text = "Blocos";

            //dLocalTableAdapters.BLOCOSTableAdapter x = new Internet.dLocalTableAdapters.BLOCOSTableAdapter();
            //x.Fill(dLocal1.BLOCOS);

            dLocal1.BLOCOSTableAdapter.Fill(dLocal1.BLOCOS);
            int nBlocosI = 0;
            int nBlocosU = 0;
            progressBarControl1.Properties.Maximum = dLocal1.BLOCOS.Rows.Count;
            progressBarControl1.Position = 0;
            progressBarControl1.Visible = true;
            foreach (dLocal.BLOCOSRow BLOCOSRow in dLocal1.BLOCOS)
            {
                dIntenet.BLOCOSTableAdapter.Fill(dIntenet.BLOCOS, Framework.DSCentral.EMP, BLOCOSRow.BLO);
                Inclusao = (dIntenet.BLOCOS.Rows.Count == 0);
                if (Inclusao)
                    RowRemota = dIntenet.BLOCOS.NewBLOCOSRow();
                else
                    RowRemota = (dIntenet.BLOCOSRow)dIntenet.BLOCOS.Rows[0];
                RowRemota.BLO_EMP = Framework.DSCentral.EMP;
                foreach (DataColumn Coluna in dIntenet.BLOCOS.Columns)
                    if (Coluna.ColumnName != "BLO_EMP")
                        RowRemota[Coluna] = BLOCOSRow[Coluna.ColumnName];
                if (Inclusao)
                    dIntenet.BLOCOS.AddBLOCOSRow(RowRemota);
                dIntenet.BLOCOSTableAdapter.Update(RowRemota);
                BLOCOSRow.BLO__INTERNET = "L";
                dLocal1.BLOCOSTableAdapter.Update(BLOCOSRow);
                progressBarControl1.Position++;
                if (Inclusao)
                    nBlocosI++;
                else
                    nBlocosU++;
                Application.DoEvents();
            };
            textBox1.Text += "BLOCOS:" + nBlocosI.ToString() + "/" + nBlocosU.ToString() + "\r\n";
        }

        /*
        private string comandoBuscaHonorarios =
"SELECT     BOletoDetalhe.BODValor, CHEques.CHEEmissao\r\n" +
"FROM         BODxPAG INNER JOIN\r\n" +
"                      PAGamentos ON BODxPAG.PAG = PAGamentos.PAG INNER JOIN\r\n" +
"                      CHEques ON PAGamentos.PAG_CHE = CHEques.CHE RIGHT OUTER JOIN\r\n" +
"                      BOletoDetalhe ON BODxPAG.BOD = BOletoDetalhe.BOD RIGHT OUTER JOIN\r\n" +
"                      BOLetos ON BOletoDetalhe.BOD_BOL = BOLetos.BOL\r\n" +
"WHERE     (BOletoDetalhe.BOD_PLA = '119000') AND (BOLetos.BOL = @P1);";

        private string comandoBusacaValorCorrigido =
"SELECT     Valor\r\n" +
"FROM         [Acordo - Dividas]\r\n" +
"WHERE     ([N�mero Acordo] = @P1) AND ([N�mero Boleto] = @P2);";

        private void AtAPT()
        {
            dBancoLido = WS.LeClientes(Criptografa());
            WSSincBancos.dBancos dBancoRetorno = new Internet.WSSincBancos.dBancos();

            //WSSincBancos.dBancos.clientesRow rowALT = dBancoLido.clientes[0];
            //WSSincBancos.dBancos.clientesRow rowDEL = dBancoLido.clientes[1];
            
            //dBancoRetorno.clientes.ImportRow(rowALT);
            //rowDEL.Delete();
            //dBancoRetorno.clientes.ImportRow(rowDEL);
            
            
            
            //dLocal1.ClientesExpTableAdapter.Fill();
            //foreach(dLocal.ClientesExpRow rowExp in dLocal1.ClientesExp)
            
            
            bool Inclusao;
            
            labelStatus.Text = "Apartamentos";
            dLocal1.APARTAMENTOSTableAdapter.FillcomCONBLO(dLocal1.APARTAMENTOS);
            int nAPARTAMENTOSI = 0;
            int nAPARTAMENTOSU = 0;
            progressBarControl1.Properties.Maximum = dLocal1.APARTAMENTOS.Rows.Count;
            progressBarControl1.Position = 0;
            progressBarControl1.Visible = true;
            dIntenet.APARTAMENTOSTableAdapter.FillBy(dIntenet.APARTAMENTOS, Framework.DSCentral.EMP);
            iCursor = 0;
            dLocal1.CobrancaTableAdapter.Fill(dLocal1.Cobranca);
            try
            {
                TableAdapter.ST().IniciaTrasacaoST(dLocal1.APARTAMENTOSTableAdapter);
                foreach (dLocal.APARTAMENTOSRow APARTAMENTOSRow in dLocal1.APARTAMENTOS)
                {
                    dIntenet.APARTAMENTOSRow RowRemota = dIntenet.APARTAMENTOS.FindByAPT_EMPAPT(Framework.DSCentral.EMP, APARTAMENTOSRow.APT);
                    Inclusao = (RowRemota == null);
                    if (Inclusao)
                    {
                        RowRemota = dIntenet.APARTAMENTOS.NewAPARTAMENTOSRow();
                        RowRemota.APT_EMP = Framework.DSCentral.EMP;                        
                    }
                    foreach (DataColumn Coluna in dIntenet.APARTAMENTOS.Columns)
                        if (Coluna.ColumnName != "APT_EMP")
                            RowRemota[Coluna] = APARTAMENTOSRow[Coluna.ColumnName];
                    if (Inclusao)
                        dIntenet.APARTAMENTOS.AddAPARTAMENTOSRow(RowRemota);
                    dIntenet.APARTAMENTOSTableAdapter.Update(RowRemota);
                    APARTAMENTOSRow.APT__INTERNET = "L";
                    dLocal1.APARTAMENTOSTableAdapter.Update(APARTAMENTOSRow);
                    progressBarControl1.Position++;
                    if (Inclusao)
                        nAPARTAMENTOSI++;
                    else
                        nAPARTAMENTOSU++;
                    Application.DoEvents();

                    //antigo
                    bool Proprietario = true;
                    for (int i = 1; i < 3; i++)
                    {
                        WSSincBancos.dBancos.clientesRow rowAPTLido = BuscaRegistro(APARTAMENTOSRow.CONCodigo, APARTAMENTOSRow.BLOCodigo, APARTAMENTOSRow.APTNumero, Proprietario);
                        bool Remover = Proprietario ? APARTAMENTOSRow.IsAPTProprietario_PESNull() : APARTAMENTOSRow.IsAPTInquilino_PESNull();
                        if (Remover)
                        {
                            if (rowAPTLido != null)
                            {
                                rowAPTLido.Delete();
                                dBancoRetorno.clientes.ImportRow(rowAPTLido);
                            }
                        }
                        else
                        {
                            bool Novo = false;
                            if (rowAPTLido == null)
                            {
                                Novo = true;
                                rowAPTLido = dBancoRetorno.clientes.NewclientesRow();                                
                                dLocal.CobrancaRow rowCob = dLocal1.Cobranca.FindByCodconBlocoApartamento(APARTAMENTOSRow.CONCodigo, APARTAMENTOSRow.BLOCodigo, APARTAMENTOSRow.APTNumero);
                                if ((rowCob != null) && (!rowCob.IsStatusNull()))
                                    rowAPTLido.juridico = (rowCob.Status.ToUpper() == "J");
                            };
                            
                            int PES = Proprietario ? APARTAMENTOSRow.APTProprietario_PES : APARTAMENTOSRow.APTInquilino_PES;
                            
                            
                            dLocal.PESSOASRow rowPES = dLocal1.PESSOASTableAdapter.GetDataByPES(PES)[0];

                            //rowAPTLido.Judicial = false;
                            
                            rowAPTLido.Nome = rowPES.PESNome;

                            string Usuario;
                            int Tentativa;

                            if (Proprietario)
                            {
                                if (APARTAMENTOSRow.IsAPTUsuarioInternetProprietarioNull() || (APARTAMENTOSRow.APTUsuarioInternetProprietario == ""))
                                {
                                    Tentativa = 1;
                                    string strBase = rowPES.PESNome.Substring(0,6).Trim().ToUpper();
                                    if(strBase.Contains(" "))
                                        strBase = strBase.Substring(0,strBase.IndexOf(" "));
                                    
                                    do
                                    {
                                        Usuario = string.Format("{0}{1}{2}",Framework.DSCentral.EMP,strBase,Tentativa);
                                        Tentativa++;
                                    }
                                    while (TableAdapter.ST().EstaCadastrado("select APT from AParTamentos where APTUsuarioInternetProprietario = @P1 or APTSenhaInternetInquilino  = @P1", Usuario));
                                    //MessageBox.Show(Usuario);
                                    APARTAMENTOSRow.APTUsuarioInternetProprietario = Usuario;
                                    APARTAMENTOSRow.APTSenhaInternetProprietario = string.Format("{0:0000}", (777.76743M * (rowPES.PES + APARTAMENTOSRow.APT)) % 10000);
                                };
                                rowAPTLido.Username = APARTAMENTOSRow.APTUsuarioInternetProprietario;
                                rowAPTLido.Senha = APARTAMENTOSRow.APTSenhaInternetProprietario;
                                if (!APARTAMENTOSRow.IsAPTFormaPagtoInquilino_FPGNull())
                                    rowAPTLido.Correio = APARTAMENTOSRow.APTFormaPagtoProprietario_FPG <= 2;
                                if (!APARTAMENTOSRow.IsAPTBEmailPNull())
                                    rowAPTLido.bemail = APARTAMENTOSRow.APTBEmailP;
                                else
                                    rowAPTLido.bemail = false;
                                if ((!APARTAMENTOSRow.IsCDRProprietarioNull()) && (APARTAMENTOSRow.CDRProprietario))
                                    rowAPTLido.tipousuario = 1;
                                else
                                    rowAPTLido.tipousuario = 3;

                            }
                            else
                            {
                                if (APARTAMENTOSRow.IsAPTUsuarioInternetInquilinoNull() || (APARTAMENTOSRow.APTUsuarioInternetInquilino == ""))
                                {
                                    Tentativa = 1;
                                    string strBase = rowPES.PESNome.Substring(0, 6).Trim().ToUpper();
                                    if (strBase.Contains(" "))
                                        strBase = strBase.Substring(0, strBase.IndexOf(" "));                                    
                                    do
                                    {
                                        Usuario = string.Format("{0}{1}{2}", Framework.DSCentral.EMP, strBase, Tentativa);
                                        Tentativa++;
                                    }
                                    while (TableAdapter.ST().EstaCadastrado("select APT from AParTamentos where APTUsuarioInternetProprietario = @P1 or APTSenhaInternetInquilino  = @P1",Usuario));
                                    APARTAMENTOSRow.APTUsuarioInternetInquilino = Usuario;
                                    APARTAMENTOSRow.APTSenhaInternetInquilino = string.Format("{0:0000}", (777.76743M * (rowPES.PES + APARTAMENTOSRow.APT)) % 10000);
                                };
                                rowAPTLido.Username = APARTAMENTOSRow.APTUsuarioInternetInquilino;
                                rowAPTLido.Senha = APARTAMENTOSRow.APTSenhaInternetInquilino;
                                if (!APARTAMENTOSRow.IsAPTFormaPagtoInquilino_FPGNull())
                                    rowAPTLido.Correio = APARTAMENTOSRow.APTFormaPagtoInquilino_FPG <= 2;
                                if (!APARTAMENTOSRow.IsAPTBEmailINull())
                                    rowAPTLido.bemail = APARTAMENTOSRow.APTBEmailI;
                                else
                                    rowAPTLido.bemail = false;
                                if ((!APARTAMENTOSRow.IsCDRProprietarioNull()) && (!APARTAMENTOSRow.CDRProprietario))
                                    rowAPTLido.tipousuario = 1;
                                else
                                    rowAPTLido.tipousuario = 4;
                            };
                            rowAPTLido.CODCON = APARTAMENTOSRow.CONCodigo;                            
                            rowAPTLido.Apartamento = APARTAMENTOSRow.APTNumero;
                            rowAPTLido.bloco = APARTAMENTOSRow.BLOCodigo;
                            if (!rowPES.IsPESEnderecoNull())
                                rowAPTLido.endereco = rowPES.PESEndereco;
                            if (!rowPES.IsPESBairroNull())
                                rowAPTLido.bairro = rowPES.PESBairro;
                            if (!rowPES.IsPESCepNull())
                                rowAPTLido.cep = rowPES.PESCep;
                            if (!rowPES.IsCIDNomeNull())
                                rowAPTLido.cidade = rowPES.CIDNome;
                            if (!rowPES.IsCIDUfNull())
                                rowAPTLido.estado = rowPES.CIDUf;
                            if (!rowPES.IsPESFone1Null())
                                rowAPTLido.telefone1 = rowPES.PESFone1;
                            if (!rowPES.IsPESFone2Null())
                                rowAPTLido.telefone2 = rowPES.PESFone2;
                            if (!rowPES.IsPESFone3Null())
                                rowAPTLido.telefone3 = rowPES.PESFone3;
                            if (!rowPES.IsPESEmailNull())
                                rowAPTLido.Email = rowPES.PESEmail;                            
                            rowAPTLido.Proprietario = Proprietario;                                                       
                            if (Novo)
                                dBancoRetorno.clientes.AddclientesRow(rowAPTLido);
                            else
                                dBancoRetorno.clientes.ImportRow(rowAPTLido);

                        };
                        Proprietario = false;
                    };
                };

                //Revisar cobran�a

                dLocal1.CobrancaTableAdapter.Fill(dLocal1.Cobranca);
                iCursor = -1;
                string SequenciadorCur;
                WSSincBancos.dBancos.clientesRow rowRetorno;
                foreach (dLocal.CobrancaRow rowCob in dLocal1.Cobranca)
                {                                        
                    bool juridico = (rowCob.Status.ToUpper() == "J");
                    string Sequenciador = string.Format("{0} {1} {2}", rowCob.Codcon, rowCob.Bloco, rowCob.Apartamento);
                    do
                    {
                        iCursor++;
                        while ((iCursor < dBancoLido.clientes.Rows.Count) && (dBancoLido.clientes[iCursor].RowState == DataRowState.Deleted))
                            iCursor++;
                        if (iCursor >= dBancoLido.clientes.Rows.Count)
                            break;
                        
                        SequenciadorCur = string.Format("{0} {1} {2}", dBancoLido.clientes[iCursor].CODCON, dBancoLido.clientes[iCursor].bloco, dBancoLido.clientes[iCursor].Apartamento);

                        //se pulou
                        if ((string.Compare(SequenciadorCur, Sequenciador) < 0) && (dBancoLido.clientes[iCursor].juridico))
                        {
                            rowRetorno = dBancoRetorno.clientes.FindByID(dBancoLido.clientes[iCursor].ID);
                            if (rowRetorno != null)
                                rowRetorno.juridico = false;
                            else
                            {
                                dBancoLido.clientes[iCursor].juridico = false;
                                dBancoRetorno.clientes.ImportRow(dBancoLido.clientes[iCursor]);
                            }
                        }
                        else
                        {
                            if ((Sequenciador == SequenciadorCur) && (juridico != dBancoLido.clientes[iCursor].juridico))
                            {
                                rowRetorno = dBancoRetorno.clientes.FindByID(dBancoLido.clientes[iCursor].ID);
                                if (rowRetorno != null)
                                    rowRetorno.juridico = juridico;
                                else
                                {
                                    dBancoLido.clientes[iCursor].juridico = juridico;
                                    dBancoRetorno.clientes.ImportRow(dBancoLido.clientes[iCursor]);
                                }
                            }
                        }
                    }
                    while (string.Compare(SequenciadorCur, Sequenciador) <= 0);
                    iCursor--;
                }

                //remover do juridico os que sobraram
                iCursor++;
                while (iCursor < dBancoLido.clientes.Rows.Count)
                {
                    if (dBancoLido.clientes[iCursor].RowState != DataRowState.Deleted)
                        if (dBancoLido.clientes[iCursor].juridico)
                        {
                            rowRetorno = dBancoRetorno.clientes.FindByID(dBancoLido.clientes[iCursor].ID);
                            if (rowRetorno != null)
                                rowRetorno.juridico = false;
                            else
                            {
                                dBancoLido.clientes[iCursor].juridico = false;
                                dBancoRetorno.clientes.ImportRow(dBancoLido.clientes[iCursor]);
                            }
                        };   
                    iCursor++;
                };

                //Acordos

                iCursor = 0;
                TableAdapter.ST().EntraEmSTTrans(dLocal1.ACORDOTableAdapter, dLocal1.OriginaisTableAdapter, dLocal1.NovasTableAdapter);
                dLocal1.Novas.Clear();
                dLocal1.Originais.Clear();
                dLocal1.ACORDOTableAdapter.FillcomCodcon(dLocal1.ACORDO);
                dLocal1.OriginaisTableAdapter.Fill(dLocal1.Originais);
                dLocal1.NovasTableAdapter.Fill(dLocal1.Novas);
                foreach (dLocal.ACORDORow ACORow in dLocal1.ACORDO)
                {
                    WSSincBancos.dBancos.clientesRow rowLido;                                        
                    Boletos.Acordo.StatusACO StatusACO = (Boletos.Acordo.StatusACO)ACORow.ACOStatus;
                    switch (StatusACO)
                    {
                        case Boletos.Acordo.StatusACO.Ativo:
                        case Boletos.Acordo.StatusACO.Cancelado:
                        case Boletos.Acordo.StatusACO.Terminado:
                            bool Remove = (StatusACO != Boletos.Acordo.StatusACO.Ativo);
                            bool Proprietario = true;
                            for (int i = 1; i < 3; i++)
                            {
                                rowLido = BuscaRegistro(ACORow.CONCodigo, ACORow.BLOCodigo, ACORow.APTNumero, Proprietario);
                                if ((rowLido != null) && (rowLido.RowState != DataRowState.Deleted))
                                {
                                    rowRetorno = dBancoRetorno.clientes.FindByID(rowLido.ID);
                                    if (rowRetorno == null)
                                    {
                                        dBancoRetorno.clientes.ImportRow(rowLido);
                                        rowRetorno = dBancoRetorno.clientes.FindByID(rowLido.ID);
                                    }
                                    rowRetorno.Judicial = ACORow.ACOJudicial;
                                    if ((Proprietario) && (!rowRetorno.IsAcordoNull()) && (rowRetorno.Acordo != 0))
                                        WS.RemoveAcordo(Criptografa(), rowRetorno.Acordo);
                                    if (Remove)
                                        rowRetorno.Acordo = 0;
                                    else
                                    {
                                        rowRetorno.Acordo = ACORow.ACO;
                                        if (Proprietario)
                                        {
                                            foreach (dLocal.OriginaisRow BOLOrow in ACORow.GetOriginaisRows())
                                            {
                                                WSSincBancos.dBancos.DividasRow Divrow = dBancoRetorno.Dividas.NewDividasRow();
                                                Divrow.Acordo = ACORow.ACO;
                                                Divrow.boleto = BOLOrow.BOL;
                                                Divrow.competencia = string.Format("{0:00}{1:0000}", BOLOrow.BOLCompetenciaMes, BOLOrow.BOLCompetenciaAno);
                                                double ValorCorrigido = (double)BOLOrow.BOLValorPrevisto;
                                                DataRow DR = VirOleDb.TableAdapter.STTableAdapter.BuscaSQLRow(comandoBusacaValorCorrigido, ACORow.ACOAntigo, BOLOrow.BOL);
                                                if ((DR != null) && (DR["Valor"] != DBNull.Value))
                                                    ValorCorrigido = (double)DR["Valor"];
                                                Divrow.valor = (decimal)ValorCorrigido;
                                                Divrow.valororiginal = BOLOrow.BOLValorPrevisto;
                                                Divrow.vencto = BOLOrow.BOLVencto;
                                                dBancoRetorno.Dividas.AddDividasRow(Divrow);
                                            }
                                            short N = 1;
                                            foreach (dLocal.NovasRow BOLNrow in ACORow.GetNovasRows())
                                            {
                                                WSSincBancos.dBancos.novasRow Novrow = dBancoRetorno.novas.NewnovasRow();
                                                Novrow.Acordo = ACORow.ACO;
                                                if (!BOLNrow.IsBOLPagamentoNull())
                                                    Novrow.dtpagto = BOLNrow.BOLPagamento;
                                                Novrow.dtvencto = BOLNrow.BOLVencto;
                                                DataRow DRH = TableAdapter.ST().BuscaSQLRow(comandoBuscaHonorarios, BOLNrow.BOL);
                                                if (DRH == null)
                                                {
                                                    Novrow.HValor = 0;
                                                    Novrow.HPag = false;
                                                }
                                                else
                                                {
                                                    if (DRH["BODValor"] == DBNull.Value)
                                                        Novrow.HValor = 0;
                                                    else
                                                        Novrow.HValor = (decimal)DRH["BODValor"];
                                                    Novrow.HPag = (DRH["CHEEmissao"] != DBNull.Value);                                                                                                           
                                                }                                                
                                                Novrow.numero = BOLNrow.BOL;
                                                Novrow.prest = N;
                                                if (!BOLNrow.IsBOLValorPagoNull())
                                                    Novrow.valpago = BOLNrow.BOLValorPago;
                                                Novrow.valprev = BOLNrow.BOLValorPrevisto;
                                                dBancoRetorno.novas.AddnovasRow(Novrow);
                                                N++;
                                            }
                                        }
                                    }
                                }
                                Proprietario = false;
                            }
                            

                            break;
                        
                            
                        case Boletos.Acordo.StatusACO.Divide:                            
                        case Boletos.Acordo.StatusACO.NaoGerado:                                                                    
                        default:
                            break;
                    }
                    ACORow.ACO__Internet = "L";
                    dLocal1.ACORDOTableAdapter.Update(ACORow);
                }



                WS.GravaCliente(Criptografa(), dBancoRetorno);
                VirMSSQL.TableAdapter.ST().Commit();
            }
            catch (Exception e)
            {
                VirMSSQL.TableAdapter.ST().Vircatch(e);
                string erro = "\r\n** Erro **";
                while (e != null)
                {
                    erro += string.Format("\r\n{0}\r\n{1}\r\n",e.Message,e.StackTrace);
                    e = e.InnerException;
                    textBox1.Text += erro;
                }
            }
            textBox1.Text += string.Format("APARTAMENTOS - Alterados: {1} Incluidos {0}\r\n" ,nAPARTAMENTOSI, nAPARTAMENTOSU);
        }

        */ 


        //int iCursor;
        //WSSincBancos.dBancos dBancoLido;
        
        /*
        WSSincBancos.dBancos.clientesRow BuscaRegistro(string CODCON,string BLOCO,string NUMERO,bool Proprietario)
        {
            
            string Sequenciador = string.Format("{0} {1} {2} {3}",CODCON,BLOCO,NUMERO,Proprietario ? "A":"B");
            string CurSequenciador = "";
            //iCursor = 0;
            do
            {
                WSSincBancos.dBancos.clientesRow candidato = dBancoLido.clientes[iCursor];
                CurSequenciador = string.Format("{0} {1} {2} {3}", candidato.CODCON, candidato.bloco, candidato.Apartamento, candidato.Proprietario ? "A" : "B");
                if (Sequenciador == CurSequenciador)
                    return candidato;
                else
                    iCursor++;
                
            }
            while((string.Compare(Sequenciador , CurSequenciador) > 0) && (iCursor < dBancoLido.clientes.Rows.Count));
            iCursor--;
            return null;
        }
        */

        /*
        private void AtBOL()
        {
            
            bool Inclusao;
            dIntenet.BOLetosRow RowRemota;
            labelStatus.Text = "Boletos";
            dLocal1.BOLetosTableAdapter.FillcomCodcon(dLocal1.BOLetos);
            int nBOLetosI = 0;
            int nBOLetosU = 0;
            progressBarControl1.Properties.Maximum = dLocal1.BOLetos.Rows.Count;
            progressBarControl1.Position = 0;
            progressBarControl1.Visible = true;
            WSSincBancos.dBancos dBancosRetorno = new Internet.WSSincBancos.dBancos();
            WSSincBancos.dBancos.BoletosRow rowRetorno;
            int Numero = 0;
            foreach (dLocal.BOLetosRow BOLetosRow in dLocal1.BOLetos)
            {
                try
                {
                    VirMSSQL.TableAdapter.AbreTrasacaoSQL("Internet cneonnet - 1265");
                    Inclusao = (dIntenet.BOLetosTableAdapter.Fill(dIntenet.BOLetos, BOLetosRow.BOL, Framework.DSCentral.EMP) == 0);
                    if (!BOLetosRow.BOLCancelado)
                    {
                        if (Inclusao)
                            RowRemota = dIntenet.BOLetos.NewBOLetosRow();
                        else
                            RowRemota = (dIntenet.BOLetosRow)dIntenet.BOLetos.Rows[0];
                        RowRemota.BOL_EMP = Framework.DSCentral.EMP;
                        foreach (DataColumn Coluna in dIntenet.BOLetos.Columns)
                            if (Coluna.ColumnName != "BOL_EMP")
                                RowRemota[Coluna] = BOLetosRow[Coluna.ColumnName];
                        if (Inclusao)
                            dIntenet.BOLetos.AddBOLetosRow(RowRemota);
                        dIntenet.BOLetosTableAdapter.Update(RowRemota);
                    }
                    else
                        if (!Inclusao)
                        {
                            RowRemota = (dIntenet.BOLetosRow)dIntenet.BOLetos.Rows[0];
                            RowRemota.Delete();
                            dIntenet.BOLetosTableAdapter.Update(RowRemota);
                        }

                    rowRetorno = dBancosRetorno.Boletos.NewBoletosRow();
                    rowRetorno.Numero = Numero++;
                    rowRetorno.Chave = string.Format("{0}{1}{2}",BOLetosRow.CONCodigo,BOLetosRow.BLOCodigo,BOLetosRow.APTNumero);
                    if (BOLetosRow.BOLCancelado)
                        rowRetorno.Cancelar = true;
                    else
                    {
                        rowRetorno.competencia = string.Format("{0:00}{1:0000}",BOLetosRow.BOLCompetenciaMes,BOLetosRow.BOLCompetenciaAno);                        
                        rowRetorno.dtvencto = BOLetosRow.BOLVencto;
                        rowRetorno.Multa = (double)BOLetosRow.BOLMulta;
                        rowRetorno.Numero = rowRetorno.nn = BOLetosRow.BOL;
                        rowRetorno.pago = (!BOLetosRow.IsBOLPagamentoNull());
                        rowRetorno.Resp = BOLetosRow.BOLProprietario ? "P" : "I";
                        rowRetorno.Tipo = BOLetosRow.BOLTipoMulta;
                        rowRetorno.tipoboleto = BOLetosRow.BOLTipoCRAI;
                        rowRetorno.valor = BOLetosRow.BOLValorPrevisto;
                    }
                    dBancosRetorno.Boletos.AddBoletosRow(rowRetorno);

                    BOLetosRow.BOL__INTERNET = "L";
                    dLocal1.BOLetosTableAdapter.Update(BOLetosRow);
                    progressBarControl1.Position++;
                    if (Inclusao)
                        nBOLetosI++;
                    else
                        nBOLetosU++;
                    VirMSSQL.TableAdapter.ST().Commit();
                }
                catch(Exception e)
                {
                    VirMSSQL.TableAdapter.ST().Vircatch(e);
                    string erro = "\r\n** Erro **";
                    while (e != null)
                    {
                        erro += string.Format("\r\n{0}\r\n{1}\r\n", e.Message, e.StackTrace);
                        e = e.InnerException;
                        textBox1.Text += erro;
                    }
                }
                Application.DoEvents();
            };
            textBox1.Text += "BOLETOS:" + nBOLetosI.ToString() + "/" + nBOLetosU.ToString() + "\r\n";
        }*/

        private void AtTodos() { 
            //AtCON();
            AtBLO();
            //AtAPT();
            //AtBOL();
            AtFRN();
        }
        #endregion

        private void simpleButton1_Click(object sender, EventArgs e)
        {
            //RecebCadastros();
            //RecebAcordos();
            AtTodos();                       
            labelStatus.Text = "Parado";
            progressBarControl1.Visible = false;
        }

        private void simpleButton2_Click(object sender, EventArgs e)
        {
            int total = dllbanco.Conciliacao.ConciliacaoSt.CarregaPendentes();
            dllbanco.Conciliacao.ConciliacaoSt.Consolidar(20, true);
            Clipboard.SetText(dllbanco.Conciliacao.ConciliacaoSt.Retorno);
            MessageBox.Show("ok");
        }

        private void retClienteGridControl_DoubleClick(object sender, EventArgs e)
        {
            
        }

        

       


        private WSSincBancos.WSSincBancos _WS;

        private WSSincBancos.WSSincBancos WS
        {
            get{
                if (_WS == null)
                {
                    _WS = new WSSincBancos.WSSincBancos();
                    if (CompontesBasicosProc.FuncoesBasicasProc.EstaEmProducao)
                        //WS.Url = @"https://ssl493.websiteseguro.com/neonimoveis/neononline2/WSSincBancos.asmx";
                        WS.Url = @"http://177.190.193.218/neononline2/WSSincBancos.asmx";
                    else
                        if (MessageBox.Show("Usar REAL", "CUIDADO", MessageBoxButtons.YesNo, MessageBoxIcon.Warning, MessageBoxDefaultButton.Button2) == DialogResult.Yes)
                            WS.Url = @"http://177.190.193.218/neononline2/WSSincBancos.asmx";
                }
                return _WS;
            }
        }   


        //remover
        private int staticR = 0;

        private byte[] Criptografa()
        {
            int EMPArquivo = Framework.DSCentral.EMP;
            if (!CompontesBasicosProc.FuncoesBasicasProc.EstaEmProducao)
            {
 
            }            
            string Aberto = string.Format("{0:yyyyMMddHHmmss}{1:000}{2:00000000}<-*W*->", DateTime.Now, EMPArquivo, staticR++);
            return VirCrip.VirCripWS.Crip(Aberto);
        }
        //fim remover

        private void simpleButton4_Click(object sender, EventArgs e)
        {
            try
            {
                simpleButton4.Enabled = false;
                textBox1.Text = "";
                textBox1.Text += string.Format("{0:HH:mm:ss} Inico NeonNet\r\n", DateTime.Now);
                Application.DoEvents();
                textBox1.Text += string.Format("{0:HH:mm:ss} Indices:\r\n{1}\r\n", DateTime.Now,NeonNet.NeonNetSt.PublicaIndices());
                Application.DoEvents();
                textBox1.Text += string.Format("{0:HH:mm:ss} Condom�os:\r\n{1}\r\n", DateTime.Now, NeonNet.NeonNetSt.PublicaCondominios());
                Application.DoEvents();
                textBox1.Text += string.Format("{0:HH:mm:ss} Apartamentos:\r\n{1}\r\n", DateTime.Now, NeonNet.NeonNetSt.PublicaApartamentos(Framework.DSCentral.EMP));
                
                
            }
            finally 
            {
                simpleButton4.Enabled = true;
            }
        }

        

        private string Condominios()
        {

            return "?";
        }

        private void button3_Click(object sender, EventArgs e)
        {
            if (retAcordoBindingSource.Current == null)
                return;
            DataRowView DRV = (DataRowView)retAcordoBindingSource.Current;
            if (DRV != null)
            {

                dIntenet.RetAcordoRow RetAcordoRow = (dIntenet.RetAcordoRow)DRV.Row;
                if (RetAcordoRow.IsEfetuadoNull() || RetAcordoRow.Efetuado == false)
                {
                    RetAcordoRow.Efetuado = true;
                    dIntenet.RetAcordoTableAdapter.Update(RetAcordoRow);
                    RetAcordoRow.AcceptChanges();
                    Relatorio = RetAcordoRow.codcon + " - " + RetAcordoRow.bloco + " - " + RetAcordoRow.apartamento + " CANCELADO";
                    textBox1.Text += "ACORDO: " + Relatorio + "\r\n";
                }
            }
        }

        
               
        private void simpleButton8_Click(object sender, EventArgs e)
        {            
            textBox1.Text += NeonNet.NeonNetSt.PublicaApartamentos(Framework.DSCentral.EMP);
        }

        private void button4_Click(object sender, EventArgs e)
        {
            textBox1.Text = "";
            //*** MRC - INICIO (27/05/2016 12:00) ***
            textBox1.Text += NeonNet.NeonNetSt.VerificaReservasSite();
            textBox1.Text += NeonNet.NeonNetSt.GeraReserva();
            textBox1.Text += NeonNet.NeonNetSt.PublicaEQuipamentos();
            //*** MRC - INICIO - RESERVA (28/05/2014 12:30) / (18/03/2015 10:00) ***
            textBox1.Text += NeonNet.NeonNetSt.PublicaReGrasEquipamento();
            //*** MRC - TERMINO - RESERVA (28/05/2014 12:30) / (18/03/2015 10:00) ***
            textBox1.Text += NeonNet.NeonNetSt.ReservaEQuipamento();
            textBox1.Text += NeonNet.NeonNetSt.Apagar();
            //*** MRC - INICIO (27/05/2016 12:00) ***
            Clipboard.SetText(textBox1.Text);
        }

        private void button4_Click_1(object sender, EventArgs e)
        {
            textBox1.Text = "";
            textBox1.Text += NeonNet.NeonNetSt.PublicaCondominios();
            Clipboard.SetText(textBox1.Text);
        }

        private void button5_Click(object sender, EventArgs e)
        {
            textBox1.Text = "";
            textBox1.Text += NeonNet.NeonNetSt.Apagar();
            Clipboard.SetText(textBox1.Text);
        }

        private void button6_Click(object sender, EventArgs e)
        {
            
        }

        private void button7_Click(object sender, EventArgs e)
        {
            
        }

        private void button8_Click(object sender, EventArgs e)
        {
            //textBox1.Text = "";
            //textBox1.Text += NeonNet.NeonNetSt.Emails();
            //Clipboard.SetText(textBox1.Text);
        }

        private void simpleButton6_Click(object sender, EventArgs e)
        {
            try
            {
                simpleButton6.Enabled = false;
                textBox1.Text = "";
                textBox1.Text += string.Format("{0:HH:mm:ss} Inico NeonNet\r\n", DateTime.Now);
                Application.DoEvents();
                textBox1.Text += string.Format("{0:HH:mm:ss} Indices:\r\n{1}\r\n", DateTime.Now, NeonNet.NeonNetSt.PublicaIndices());                                
            }
            finally
            {
                simpleButton6.Enabled = true;
            }
        }

        private void simpleButton7_Click(object sender, EventArgs e)
        {
            textBox1.Text = "";
            textBox1.Text += NeonNet.NeonNetSt.ProcessaFrancesinha(dllbanco.Francesinha.TipoRetorno.Log);
            Clipboard.SetText(textBox1.Text);
        }

        private void simpleButton9_Click(object sender, EventArgs e)
        {
            textBox1.Text = "";
            int iTotal=0;
            int iAtual=0;            
            textBox1.Text = new EDIBoletosNeon.Arquivo().Remessa(1000, @"C:\Temp\Neon\ArquivosBradesco\Real\", ref iTotal, ref iAtual, 237);
            textBox1.Text += new EDIBoletosNeon.Arquivo().Remessa(1000, @"C:\Temp\Neon\ArquivosItau\Real\", ref iTotal, ref iAtual, 341);
            textBox1.Text += new EDIBoletosNeon.Arquivo().Remessa(1000, @"C:\CAIXA\COBRANCA\", ref iTotal, ref iAtual, 104);
            textBox1.Text += new EDIBoletosNeon.Arquivo().Remessa(1000, @"C:\Temp\Neon\ArquivosSantander\Real\", ref iTotal, ref iAtual, 33);
            Clipboard.SetText(textBox1.Text);
        }

        private void simpleButton10_Click(object sender, EventArgs e)
        {
            
        }

        private void simpleButton11_Click(object sender, EventArgs e)
        {
            //textBox1.Text += NeonNet.NeonNetSt.PublicaApartamentos(Framework.DSCentral.EMP);
            textBox1.Text = NeonNet.NeonNetSt.EmiteChequeEletronico("Emite Cheque eletr�nico",10000, @"C:\lixo\");            
            //textBox1.Text = new EDIBoletosNeon.Arquivo().RemessaCheque(10000, @"D:\teste\edi\");
            Clipboard.SetText(textBox1.Text);
        }

        private void simpleButton12_Click(object sender, EventArgs e)
        {
            
        }

        private void simpleButton13_Click(object sender, EventArgs e)
        {
            textBox1.Text = "";
            //textBox1.Text += NeonNet.NeonNetSt.RevisaPeriodicos();            
            textBox1.Text += NeonNet.NeonNetSt.RevisaPeriodicos_WS(10);
            Clipboard.SetText(textBox1.Text);
        }

        /*
        private void simpleButton14_Click(object sender, EventArgs e)
        {
            //textBox1.Text = "";
            //textBox1.Text += NeonNet.NeonNetSt.PublicaExtratos();
            //Clipboard.SetText(textBox1.Text);
        }*/

        private void simpleButton15_Click(object sender, EventArgs e)
        {
            textBox1.Text = NeonNet.NeonNetSt.RemessaTributo("Tributo Folha", 1000, "C:\\lixo\\", OrigemImposto.Folha);            
            textBox1.Text += NeonNet.NeonNetSt.RemessaTributo("Tributo Nota", 1000, "C:\\lixo\\", OrigemImposto.Nota);
            Clipboard.SetText(textBox1.Text);
        }

        private void simpleButton3_Click(object sender, EventArgs e)
        {
            dllbanco.TributoEletronico Tri = new dllbanco.TributoEletronico();
            textBox1.Text = Tri.ProcessarDados();
        }
    }
}

