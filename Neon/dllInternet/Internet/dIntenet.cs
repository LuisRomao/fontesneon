﻿/*
MR - 28/05/2014 12:30 - Publicação de regras dos Equipamento (Alterações indicadas por *** MRC - INICIO - RESERVA (28/05/2014 12:30) ***)
MR - 01/07/2014 19:00 - Publicação do parametro CONJurosPrimerioMes dos Condominios (Alterações no xsd)
MR - 26/08/2014 15:00 - Publicação do parametro EQPReservaUnitaria dos Equipamentos (Alterações no xsd)
MR - 02/09/2014 16:00 - Publicação do parametro EQPMaxConvidados dos Equipamentos (Alterações no xsd)
MR - 14/01/2016 12:30 - Publicação do parametro EQPReservaUnitariaIndividual dos Equipamentos (Alterações no xsd)
MR - 25/02/2016 11:00 - Publicação do parametro CONBoletoComRegistro dos Condomínios (Alterações no xsd)
*/

namespace Internet
{


    partial class dIntenet
    {
        partial class ContaCorrenteDetalheDataTable
        {
        }
        #region TableAdapters

        private dIntenetTableAdapters.RamoAtiVidadeTableAdapter ramoAtiVidadeTableAdapter;
        /// <summary>
        /// TableAdapter padrão para tabela: RamoAtiVidade
        /// </summary>
        public dIntenetTableAdapters.RamoAtiVidadeTableAdapter RamoAtiVidadeTableAdapter
        {
            get
            {
                if (ramoAtiVidadeTableAdapter == null)
                {
                    ramoAtiVidadeTableAdapter = new dIntenetTableAdapters.RamoAtiVidadeTableAdapter();
                    ramoAtiVidadeTableAdapter.TrocarStringDeConexao(VirDB.Bancovirtual.TiposDeBanco.Internet2);
                };
                return ramoAtiVidadeTableAdapter;
            }
        }

        private dIntenetTableAdapters.EQuiPamentosTableAdapter eQuiPamentosTableAdapter;
        /// <summary>
        /// TableAdapter padrão para tabela: EQuiPamentos
        /// </summary>
        public dIntenetTableAdapters.EQuiPamentosTableAdapter EQuiPamentosTableAdapter
        {
            get
            {
                if (eQuiPamentosTableAdapter == null)
                {
                    eQuiPamentosTableAdapter = new dIntenetTableAdapters.EQuiPamentosTableAdapter();
                    eQuiPamentosTableAdapter.TrocarStringDeConexao(VirDB.Bancovirtual.TiposDeBanco.Internet2);
                };
                return eQuiPamentosTableAdapter;
            }
        }

        private dIntenetTableAdapters.ReservaEQuipamentoTableAdapter reservaEQuipamentoTableAdapter;
        /// <summary>
        /// TableAdapter padrão para tabela: ReservaEQuipamento
        /// </summary>
        public dIntenetTableAdapters.ReservaEQuipamentoTableAdapter ReservaEQuipamentoTableAdapter
        {
            get
            {
                if (reservaEQuipamentoTableAdapter == null)
                {
                    reservaEQuipamentoTableAdapter = new dIntenetTableAdapters.ReservaEQuipamentoTableAdapter();
                    reservaEQuipamentoTableAdapter.TrocarStringDeConexao(VirDB.Bancovirtual.TiposDeBanco.Internet2);
                };
                return reservaEQuipamentoTableAdapter;
            }
        }

        //*** MRC - INICIO - RESERVA (28/05/2014 12:30) ***
        private dIntenetTableAdapters.ReGrasEquipamentoTableAdapter reGrasEQuipamentoTableAdapter;
        /// <summary>
        /// TableAdapter padrão para tabela: ReGrasEquipamento
        /// </summary>
        public dIntenetTableAdapters.ReGrasEquipamentoTableAdapter ReGrasEQuipamentoTableAdapter
        {
            get
            {
                if (reGrasEQuipamentoTableAdapter == null)
                {
                    reGrasEQuipamentoTableAdapter = new dIntenetTableAdapters.ReGrasEquipamentoTableAdapter();
                    reGrasEQuipamentoTableAdapter.TrocarStringDeConexao(VirDB.Bancovirtual.TiposDeBanco.Internet2);
                };
                return reGrasEQuipamentoTableAdapter;
            }
        }
        //*** MRC - TERMINO - RESERVA (28/05/2014 12:30) ***

        dIntenetTableAdapters.CONDOMINIOSTableAdapter cONDOMINIOSTableAdapter;
        dIntenetTableAdapters.BLOCOSTableAdapter bLOCOSTableAdapter;
        dIntenetTableAdapters.APARTAMENTOSTableAdapter aPARTAMENTOSTableAdapter;
        dIntenetTableAdapters.BOLetosTableAdapter bOLetosTableAdapter;
        dIntenetTableAdapters.FORNECEDORESTableAdapter fORNECEDORESTableAdapter;



        dIntenetTableAdapters.RetAcordoTableAdapter retAcordoTableAdapter;
        dIntenetTableAdapters.RetOriginaisTableAdapter retOriginaisTableAdapter;
        dIntenetTableAdapters.RetNovosBTableAdapter retNovosBTableAdapter;
        dIntenetTableAdapters.RetClienteTableAdapter retClienteTableAdapter;

        private dIntenetTableAdapters.ContaCorrenTeTableAdapter contaCorrenTeTableAdapter;
        /// <summary>
        /// TableAdapter padrão para tabela: ContaCorrenTe
        /// </summary>
        public dIntenetTableAdapters.ContaCorrenTeTableAdapter ContaCorrenTeTableAdapter
        {
            get
            {
                if (contaCorrenTeTableAdapter == null)
                {
                    contaCorrenTeTableAdapter = new dIntenetTableAdapters.ContaCorrenTeTableAdapter();
                    contaCorrenTeTableAdapter.TrocarStringDeConexao(VirDB.Bancovirtual.TiposDeBanco.Internet2);
                };
                return contaCorrenTeTableAdapter;
            }
        }

        private dIntenetTableAdapters.SaldoContaCorrenteTableAdapter saldoContaCorrenteTableAdapter;
        /// <summary>
        /// TableAdapter padrão para tabela: SaldoContaCorrente
        /// </summary>
        public dIntenetTableAdapters.SaldoContaCorrenteTableAdapter SaldoContaCorrenteTableAdapter
        {
            get
            {
                if (saldoContaCorrenteTableAdapter == null)
                {
                    saldoContaCorrenteTableAdapter = new dIntenetTableAdapters.SaldoContaCorrenteTableAdapter();
                    saldoContaCorrenteTableAdapter.TrocarStringDeConexao(VirDB.Bancovirtual.TiposDeBanco.Internet2);
                };
                return saldoContaCorrenteTableAdapter;
            }
        }

        private dIntenetTableAdapters.ContaCorrenteDetalheTableAdapter contaCorrenteDetalheTableAdapter;
        /// <summary>
        /// TableAdapter padrão para tabela: ContaCorrenteDetalhe
        /// </summary>
        public dIntenetTableAdapters.ContaCorrenteDetalheTableAdapter ContaCorrenteDetalheTableAdapter
        {
            get
            {
                if (contaCorrenteDetalheTableAdapter == null)
                {
                    contaCorrenteDetalheTableAdapter = new dIntenetTableAdapters.ContaCorrenteDetalheTableAdapter();
                    contaCorrenteDetalheTableAdapter.TrocarStringDeConexao(VirDB.Bancovirtual.TiposDeBanco.Internet2);
                };
                return contaCorrenteDetalheTableAdapter;
            }
        }

        private dIntenetTableAdapters.ACOrdosTableAdapter aCOrdosTableAdapter;
        /// <summary>
        /// TableAdapter padrão para tabela: ACOrdos
        /// </summary>
        public dIntenetTableAdapters.ACOrdosTableAdapter ACOrdosTableAdapter
        {
            get
            {
                if (aCOrdosTableAdapter == null)
                {
                    aCOrdosTableAdapter = new dIntenetTableAdapters.ACOrdosTableAdapter();
                    aCOrdosTableAdapter.TrocarStringDeConexao(VirDB.Bancovirtual.TiposDeBanco.Internet2);
                };
                return aCOrdosTableAdapter;
            }
        }

        private dIntenetTableAdapters.ACOxBOLTableAdapter aCOxBOLTableAdapter;
        /// <summary>
        /// TableAdapter padrão para tabela: ACOxBOL
        /// </summary>
        public dIntenetTableAdapters.ACOxBOLTableAdapter ACOxBOLTableAdapter
        {
            get
            {
                if (aCOxBOLTableAdapter == null)
                {
                    aCOxBOLTableAdapter = new dIntenetTableAdapters.ACOxBOLTableAdapter();
                    aCOxBOLTableAdapter.TrocarStringDeConexao(VirDB.Bancovirtual.TiposDeBanco.Internet2);
                };
                return aCOxBOLTableAdapter;
            }
        }

        private dIntenetTableAdapters.CHEquesTableAdapter cHEquesTableAdapter;
        /// <summary>
        /// TableAdapter padrão para tabela: CHEques
        /// </summary>
        public dIntenetTableAdapters.CHEquesTableAdapter CHEquesTableAdapter
        {
            get
            {
                if (cHEquesTableAdapter == null)
                {
                    cHEquesTableAdapter = new dIntenetTableAdapters.CHEquesTableAdapter();
                    cHEquesTableAdapter.TrocarStringDeConexao(VirDB.Bancovirtual.TiposDeBanco.Internet2);
                };
                return cHEquesTableAdapter;
            }
        }

        #endregion

        #region Get TableAdapter

        /// <summary>
        /// 
        /// </summary>
        public dIntenetTableAdapters.RetClienteTableAdapter RetClienteTableAdapter
        {
            get
            {
                if (retClienteTableAdapter == null)
                {
                    retClienteTableAdapter = new Internet.dIntenetTableAdapters.RetClienteTableAdapter();
                    retClienteTableAdapter.TrocarStringDeConexao(VirDB.Bancovirtual.TiposDeBanco.Internet1);
                };
                return retClienteTableAdapter;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        public dIntenetTableAdapters.FORNECEDORESTableAdapter FORNECEDORESTableAdapter
        {
            get
            {
                if (fORNECEDORESTableAdapter == null)
                {
                    fORNECEDORESTableAdapter = new Internet.dIntenetTableAdapters.FORNECEDORESTableAdapter();
                    fORNECEDORESTableAdapter.TrocarStringDeConexao(VirDB.Bancovirtual.TiposDeBanco.Internet2);
                };
                return fORNECEDORESTableAdapter;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        public dIntenetTableAdapters.RetAcordoTableAdapter RetAcordoTableAdapter
        {
            get
            {
                if (retAcordoTableAdapter == null)
                {
                    retAcordoTableAdapter = new Internet.dIntenetTableAdapters.RetAcordoTableAdapter();
                    retAcordoTableAdapter.TrocarStringDeConexao(VirDB.Bancovirtual.TiposDeBanco.Internet1);
                };
                return retAcordoTableAdapter;
            }
        }


        /// <summary>
        /// 
        /// </summary>
        public dIntenetTableAdapters.RetOriginaisTableAdapter RetOriginaisTableAdapter
        {
            get
            {
                if (retOriginaisTableAdapter == null)
                {
                    retOriginaisTableAdapter = new Internet.dIntenetTableAdapters.RetOriginaisTableAdapter();
                    RetOriginaisTableAdapter.TrocarStringDeConexao(VirDB.Bancovirtual.TiposDeBanco.Internet1);
                };
                return retOriginaisTableAdapter;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        public dIntenetTableAdapters.RetNovosBTableAdapter RetNovosBTableAdapter
        {
            get
            {
                if (retNovosBTableAdapter == null)
                {
                    retNovosBTableAdapter = new Internet.dIntenetTableAdapters.RetNovosBTableAdapter();
                    retNovosBTableAdapter.TrocarStringDeConexao(VirDB.Bancovirtual.TiposDeBanco.Internet1);
                };
                return retNovosBTableAdapter;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        public dIntenetTableAdapters.BLOCOSTableAdapter BLOCOSTableAdapter
        {
            get
            {
                if (bLOCOSTableAdapter == null)
                {
                    bLOCOSTableAdapter = new Internet.dIntenetTableAdapters.BLOCOSTableAdapter();
                    bLOCOSTableAdapter.TrocarStringDeConexao(VirDB.Bancovirtual.TiposDeBanco.Internet2);
                };
                return bLOCOSTableAdapter;
            }

        }

        /// <summary>
        /// 
        /// </summary>
        public dIntenetTableAdapters.APARTAMENTOSTableAdapter APARTAMENTOSTableAdapter
        {
            get
            {
                if (aPARTAMENTOSTableAdapter == null)
                {
                    aPARTAMENTOSTableAdapter = new Internet.dIntenetTableAdapters.APARTAMENTOSTableAdapter();
                    aPARTAMENTOSTableAdapter.TrocarStringDeConexao(VirDB.Bancovirtual.TiposDeBanco.Internet2);
                };
                return aPARTAMENTOSTableAdapter;
            }

        }

        /// <summary>
        /// 
        /// </summary>
        public dIntenetTableAdapters.BOLetosTableAdapter BOLetosTableAdapter
        {
            get
            {
                if (bOLetosTableAdapter == null)
                {
                    bOLetosTableAdapter = new Internet.dIntenetTableAdapters.BOLetosTableAdapter();
                    bOLetosTableAdapter.TrocarStringDeConexao(VirDB.Bancovirtual.TiposDeBanco.Internet2);
                };
                return bOLetosTableAdapter;
            }

        }
        /// <summary>
        /// 
        /// </summary>
        public dIntenetTableAdapters.CONDOMINIOSTableAdapter CONDOMINIOSTableAdapter
        {
            get
            {
                if (cONDOMINIOSTableAdapter == null)
                {
                    cONDOMINIOSTableAdapter = new Internet.dIntenetTableAdapters.CONDOMINIOSTableAdapter();
                    cONDOMINIOSTableAdapter.TrocarStringDeConexao(VirDB.Bancovirtual.TiposDeBanco.Internet2);
                };
                return cONDOMINIOSTableAdapter;
            }
        }
        #endregion
    }
}
