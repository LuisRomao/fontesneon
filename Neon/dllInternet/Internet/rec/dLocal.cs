﻿namespace Internet {


    partial class dLocal
    {
        
        #region Table Adapters
        
        private dLocalTableAdapters.CONDOMINIOSTableAdapter cONDOMINIOSTableAdapter;
        private dLocalTableAdapters.BLOCOSTableAdapter bLOCOSTableAdapter;
        private dLocalTableAdapters.APARTAMENTOSTableAdapter aPARTAMENTOSTableAdapter;
        private dLocalTableAdapters.BOLetosTableAdapter bOLetosTableAdapter;

        private dLocalTableAdapters.Boleto1TableAdapter boleto1TableAdapter;
        private dLocalTableAdapters.PESSOASTableAdapter pESSOASTableAdapter;
        private dLocalTableAdapters.CIDADESTableAdapter cIDADESTableAdapter;

        private dLocalTableAdapters.ApartamentoTableAdapter apartamentoTableAdapter;

        private dLocalTableAdapters.PropInqTableAdapter propInqTableAdapter;
        private dLocalTableAdapters.FORNECEDORESTableAdapter fORNECEDORESTableAdapter;

        

        

        

        #endregion

        #region Cria TableAdapters
        
        public dLocalTableAdapters.FORNECEDORESTableAdapter FORNECEDORESTableAdapter
        {
            get {
                if (fORNECEDORESTableAdapter == null) {
                    fORNECEDORESTableAdapter = new Internet.dLocalTableAdapters.FORNECEDORESTableAdapter();
                    fORNECEDORESTableAdapter.TrocarStringDeConexao(CompontesBasicos.Bancovirtual.TiposDeBanco.SQL);
                };
                return fORNECEDORESTableAdapter; 
            }
        }

        public dLocalTableAdapters.PropInqTableAdapter PropInqTableAdapter
        {
            get {
                if (propInqTableAdapter == null)
                {
                    propInqTableAdapter = new Internet.dLocalTableAdapters.PropInqTableAdapter();
                    propInqTableAdapter.TrocarStringDeConexao(CompontesBasicos.Bancovirtual.TiposDeBanco.Access);
                };
                return propInqTableAdapter; 
            }
        }

        public dLocalTableAdapters.ApartamentoTableAdapter ApartamentoTableAdapter
        {
            get {
                if (apartamentoTableAdapter == null) {
                    apartamentoTableAdapter = new Internet.dLocalTableAdapters.ApartamentoTableAdapter();
                    apartamentoTableAdapter.TrocarStringDeConexao(CompontesBasicos.Bancovirtual.TiposDeBanco.Access);
                };
                return apartamentoTableAdapter; 
            }
        }

        public dLocalTableAdapters.CIDADESTableAdapter CIDADESTableAdapter
        {
            get {
                if (cIDADESTableAdapter == null) {
                    cIDADESTableAdapter = new Internet.dLocalTableAdapters.CIDADESTableAdapter();
                    cIDADESTableAdapter.TrocarStringDeConexao(CompontesBasicos.Bancovirtual.TiposDeBanco.SQL);
                };
                return cIDADESTableAdapter; 
            }
        }

        public dLocalTableAdapters.PESSOASTableAdapter PESSOASTableAdapter
        {
            get
            {
                if (pESSOASTableAdapter == null)
                {
                    pESSOASTableAdapter = new Internet.dLocalTableAdapters.PESSOASTableAdapter();
                    pESSOASTableAdapter.TrocarStringDeConexao(CompontesBasicos.Bancovirtual.TiposDeBanco.SQL);
                };
                return pESSOASTableAdapter;
            }
        }


        public dLocalTableAdapters.Boleto1TableAdapter Boleto1TableAdapter
        {
            get
            {
                if (boleto1TableAdapter == null)
                {
                    boleto1TableAdapter = new Internet.dLocalTableAdapters.Boleto1TableAdapter();
                    boleto1TableAdapter.TrocarStringDeConexao(CompontesBasicos.Bancovirtual.TiposDeBanco.Access);
                };
                return boleto1TableAdapter;
            }
        }

        public dLocalTableAdapters.BLOCOSTableAdapter BLOCOSTableAdapter
        {
            get
            {
                if (bLOCOSTableAdapter == null)
                {
                    bLOCOSTableAdapter = new Internet.dLocalTableAdapters.BLOCOSTableAdapter();
                    bLOCOSTableAdapter.TrocarStringDeConexao(CompontesBasicos.Bancovirtual.TiposDeBanco.SQL);
                };
                return bLOCOSTableAdapter;
            }

        }


        public dLocalTableAdapters.APARTAMENTOSTableAdapter APARTAMENTOSTableAdapter
        {
            get
            {
                if (aPARTAMENTOSTableAdapter == null)
                {
                    aPARTAMENTOSTableAdapter = new Internet.dLocalTableAdapters.APARTAMENTOSTableAdapter();
                    aPARTAMENTOSTableAdapter.TrocarStringDeConexao(CompontesBasicos.Bancovirtual.TiposDeBanco.SQL);
                };
                return aPARTAMENTOSTableAdapter;
            }
        }


        public dLocalTableAdapters.BOLetosTableAdapter BOLetosTableAdapter
        {
            get
            {
                if (bOLetosTableAdapter == null)
                {
                    bOLetosTableAdapter = new Internet.dLocalTableAdapters.BOLetosTableAdapter();
                    bOLetosTableAdapter.TrocarStringDeConexao(CompontesBasicos.Bancovirtual.TiposDeBanco.SQL);
                }
                return bOLetosTableAdapter;
            }
        }

        public dLocalTableAdapters.CONDOMINIOSTableAdapter CONDOMINIOSTableAdapter
        {
            get
            {
                if (cONDOMINIOSTableAdapter == null)
                {
                    cONDOMINIOSTableAdapter = new Internet.dLocalTableAdapters.CONDOMINIOSTableAdapter();
                    cONDOMINIOSTableAdapter.TrocarStringDeConexao(CompontesBasicos.Bancovirtual.TiposDeBanco.SQL);
                }
                return cONDOMINIOSTableAdapter;
            }
        } 
        #endregion
        
    }
}
