﻿namespace TesteDLL
{
    partial class TestaInadDet
    {
        /// <summary>
        /// Variável de designer necessária.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Limpar os recursos que estão sendo usados.
        /// </summary>
        /// <param name="disposing">true se for necessário descartar os recursos gerenciados; caso contrário, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Código gerado pelo Windows Form Designer

        /// <summary>
        /// Método necessário para suporte ao Designer - não modifique 
        /// o conteúdo deste método com o editor de código.
        /// </summary>
        private void InitializeComponent()
        {
            this.simpleButton2 = new DevExpress.XtraEditors.SimpleButton();
            this.S5 = new DevExpress.XtraEditors.SpinEdit();
            this.S4 = new DevExpress.XtraEditors.SpinEdit();
            this.S3 = new DevExpress.XtraEditors.SpinEdit();
            this.S2 = new DevExpress.XtraEditors.SpinEdit();
            this.S1 = new DevExpress.XtraEditors.SpinEdit();
            this.S0 = new DevExpress.XtraEditors.SpinEdit();
            this.simpleButton1 = new DevExpress.XtraEditors.SimpleButton();
            this.NTestesI = new DevExpress.XtraEditors.SpinEdit();
            this.NTestesF = new DevExpress.XtraEditors.SpinEdit();
            ((System.ComponentModel.ISupportInitialize)(this.StyleController_F)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.S5.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.S4.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.S3.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.S2.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.S1.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.S0.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.NTestesI.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.NTestesF.Properties)).BeginInit();
            this.SuspendLayout();
            // 
            // ToolTipController_F
            // 
            this.ToolTipController_F.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.ToolTipController_F.Appearance.ForeColor = System.Drawing.Color.Black;
            this.ToolTipController_F.Appearance.Options.UseBackColor = true;
            this.ToolTipController_F.Appearance.Options.UseFont = true;
            this.ToolTipController_F.Appearance.Options.UseForeColor = true;
            this.ToolTipController_F.Appearance.Options.UseTextOptions = true;
            this.ToolTipController_F.Appearance.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            // 
            // StyleController_F
            // 
            this.StyleController_F.AppearanceDisabled.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this.StyleController_F.AppearanceDisabled.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this.StyleController_F.AppearanceDisabled.Options.UseBackColor = true;
            this.StyleController_F.AppearanceDisabled.Options.UseForeColor = true;
            this.StyleController_F.AppearanceFocused.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.StyleController_F.AppearanceFocused.Options.UseBackColor = true;
            // 
            // simpleButton2
            // 
            this.simpleButton2.Location = new System.Drawing.Point(227, 18);
            this.simpleButton2.Name = "simpleButton2";
            this.simpleButton2.Size = new System.Drawing.Size(94, 23);
            this.simpleButton2.TabIndex = 15;
            this.simpleButton2.Text = "Teste Modificado";
            this.simpleButton2.Click += new System.EventHandler(this.simpleButton2_Click);
            // 
            // S5
            // 
            this.S5.EditValue = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.S5.Location = new System.Drawing.Point(121, 150);
            this.S5.Name = "S5";
            this.S5.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.S5.Size = new System.Drawing.Size(100, 20);
            this.S5.TabIndex = 14;
            // 
            // S4
            // 
            this.S4.EditValue = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.S4.Location = new System.Drawing.Point(121, 124);
            this.S4.Name = "S4";
            this.S4.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.S4.Size = new System.Drawing.Size(100, 20);
            this.S4.TabIndex = 13;
            // 
            // S3
            // 
            this.S3.EditValue = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.S3.Location = new System.Drawing.Point(121, 98);
            this.S3.Name = "S3";
            this.S3.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.S3.Size = new System.Drawing.Size(100, 20);
            this.S3.TabIndex = 12;
            // 
            // S2
            // 
            this.S2.EditValue = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.S2.Location = new System.Drawing.Point(121, 72);
            this.S2.Name = "S2";
            this.S2.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.S2.Size = new System.Drawing.Size(100, 20);
            this.S2.TabIndex = 11;
            // 
            // S1
            // 
            this.S1.EditValue = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.S1.Location = new System.Drawing.Point(121, 46);
            this.S1.Name = "S1";
            this.S1.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.S1.Size = new System.Drawing.Size(100, 20);
            this.S1.TabIndex = 10;
            // 
            // S0
            // 
            this.S0.EditValue = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.S0.Location = new System.Drawing.Point(121, 20);
            this.S0.Name = "S0";
            this.S0.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.S0.Size = new System.Drawing.Size(100, 20);
            this.S0.TabIndex = 9;
            // 
            // simpleButton1
            // 
            this.simpleButton1.Location = new System.Drawing.Point(21, 18);
            this.simpleButton1.Name = "simpleButton1";
            this.simpleButton1.Size = new System.Drawing.Size(94, 23);
            this.simpleButton1.TabIndex = 8;
            this.simpleButton1.Text = "Teste Original";
            this.simpleButton1.Click += new System.EventHandler(this.simpleButton1_Click);
            // 
            // NTestesI
            // 
            this.NTestesI.EditValue = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.NTestesI.Location = new System.Drawing.Point(121, 245);
            this.NTestesI.Name = "NTestesI";
            this.NTestesI.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.NTestesI.Size = new System.Drawing.Size(100, 20);
            this.NTestesI.TabIndex = 16;
            // 
            // NTestesF
            // 
            this.NTestesF.EditValue = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.NTestesF.Location = new System.Drawing.Point(227, 245);
            this.NTestesF.Name = "NTestesF";
            this.NTestesF.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.NTestesF.Size = new System.Drawing.Size(100, 20);
            this.NTestesF.TabIndex = 17;
            // 
            // TestaInadDet
            // 
            this.Controls.Add(this.NTestesF);
            this.Controls.Add(this.NTestesI);
            this.Controls.Add(this.simpleButton2);
            this.Controls.Add(this.S5);
            this.Controls.Add(this.S4);
            this.Controls.Add(this.S3);
            this.Controls.Add(this.S2);
            this.Controls.Add(this.S1);
            this.Controls.Add(this.S0);
            this.Controls.Add(this.simpleButton1);
            this.LookAndFeel.UseDefaultLookAndFeel = false;
            this.Name = "TestaInadDet";
            ((System.ComponentModel.ISupportInitialize)(this.StyleController_F)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.S5.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.S4.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.S3.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.S2.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.S1.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.S0.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.NTestesI.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.NTestesF.Properties)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraEditors.SimpleButton simpleButton2;
        private DevExpress.XtraEditors.SpinEdit S5;
        private DevExpress.XtraEditors.SpinEdit S4;
        private DevExpress.XtraEditors.SpinEdit S3;
        private DevExpress.XtraEditors.SpinEdit S2;
        private DevExpress.XtraEditors.SpinEdit S1;
        private DevExpress.XtraEditors.SpinEdit S0;
        private DevExpress.XtraEditors.SimpleButton simpleButton1;
        private DevExpress.XtraEditors.SpinEdit NTestesI;
        private DevExpress.XtraEditors.SpinEdit NTestesF;
    }
}
