﻿namespace TesteDLL
{
    partial class CorrecaoFolha
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            DevExpress.XtraGrid.GridLevelNode gridLevelNode1 = new DevExpress.XtraGrid.GridLevelNode();
            DevExpress.XtraGrid.GridLevelNode gridLevelNode2 = new DevExpress.XtraGrid.GridLevelNode();
            this.gridControl1 = new DevExpress.XtraGrid.GridControl();
            this.dCorrecaoFolha1 = new TesteDLL.dCorrecaoFolha();
            this.gridView1 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colPGF = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colPGF_CON = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colPGFDia = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colPGFDescricao = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colPGFValor = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colPGFProximaCompetencia = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colResultado = new DevExpress.XtraGrid.Columns.GridColumn();
            this.panelControl1 = new DevExpress.XtraEditors.PanelControl();
            this.simpleButton3 = new DevExpress.XtraEditors.SimpleButton();
            this.simpleButton2 = new DevExpress.XtraEditors.SimpleButton();
            this.simpleButton1 = new DevExpress.XtraEditors.SimpleButton();
            this.simpleButton4 = new DevExpress.XtraEditors.SimpleButton();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dCorrecaoFolha1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl1)).BeginInit();
            this.panelControl1.SuspendLayout();
            this.SuspendLayout();
            // 
            // gridControl1
            // 
            this.gridControl1.DataMember = "PaGamentosFixos";
            this.gridControl1.DataSource = this.dCorrecaoFolha1;
            this.gridControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            gridLevelNode2.RelationName = "NOtAs_PAG_CHE";
            gridLevelNode1.Nodes.AddRange(new DevExpress.XtraGrid.GridLevelNode[] {
            gridLevelNode2});
            gridLevelNode1.RelationName = "FK_NOtAs_PaGamentosFixos";
            this.gridControl1.LevelTree.Nodes.AddRange(new DevExpress.XtraGrid.GridLevelNode[] {
            gridLevelNode1});
            this.gridControl1.Location = new System.Drawing.Point(0, 100);
            this.gridControl1.MainView = this.gridView1;
            this.gridControl1.Name = "gridControl1";
            this.gridControl1.Size = new System.Drawing.Size(740, 414);
            this.gridControl1.TabIndex = 1;
            this.gridControl1.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridView1});
            // 
            // dCorrecaoFolha1
            // 
            this.dCorrecaoFolha1.DataSetName = "dCorrecaoFolha";
            this.dCorrecaoFolha1.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // gridView1
            // 
            this.gridView1.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colPGF,
            this.colPGF_CON,
            this.colPGFDia,
            this.colPGFDescricao,
            this.colPGFValor,
            this.colPGFProximaCompetencia,
            this.colResultado});
            this.gridView1.GridControl = this.gridControl1;
            this.gridView1.Name = "gridView1";
            this.gridView1.OptionsView.ColumnAutoWidth = false;
            this.gridView1.OptionsView.ShowAutoFilterRow = true;
            // 
            // colPGF
            // 
            this.colPGF.FieldName = "PGF";
            this.colPGF.Name = "colPGF";
            this.colPGF.Visible = true;
            this.colPGF.VisibleIndex = 0;
            // 
            // colPGF_CON
            // 
            this.colPGF_CON.FieldName = "PGF_CON";
            this.colPGF_CON.Name = "colPGF_CON";
            this.colPGF_CON.Visible = true;
            this.colPGF_CON.VisibleIndex = 1;
            // 
            // colPGFDia
            // 
            this.colPGFDia.FieldName = "PGFDia";
            this.colPGFDia.Name = "colPGFDia";
            this.colPGFDia.Visible = true;
            this.colPGFDia.VisibleIndex = 2;
            // 
            // colPGFDescricao
            // 
            this.colPGFDescricao.FieldName = "PGFDescricao";
            this.colPGFDescricao.Name = "colPGFDescricao";
            this.colPGFDescricao.Visible = true;
            this.colPGFDescricao.VisibleIndex = 3;
            // 
            // colPGFValor
            // 
            this.colPGFValor.FieldName = "PGFValor";
            this.colPGFValor.Name = "colPGFValor";
            this.colPGFValor.Visible = true;
            this.colPGFValor.VisibleIndex = 4;
            // 
            // colPGFProximaCompetencia
            // 
            this.colPGFProximaCompetencia.FieldName = "PGFProximaCompetencia";
            this.colPGFProximaCompetencia.Name = "colPGFProximaCompetencia";
            this.colPGFProximaCompetencia.Visible = true;
            this.colPGFProximaCompetencia.VisibleIndex = 5;
            // 
            // colResultado
            // 
            this.colResultado.FieldName = "Resultado";
            this.colResultado.Name = "colResultado";
            this.colResultado.Visible = true;
            this.colResultado.VisibleIndex = 6;
            this.colResultado.Width = 652;
            // 
            // panelControl1
            // 
            this.panelControl1.Controls.Add(this.simpleButton4);
            this.panelControl1.Controls.Add(this.simpleButton3);
            this.panelControl1.Controls.Add(this.simpleButton2);
            this.panelControl1.Controls.Add(this.simpleButton1);
            this.panelControl1.Dock = System.Windows.Forms.DockStyle.Top;
            this.panelControl1.Location = new System.Drawing.Point(0, 0);
            this.panelControl1.Name = "panelControl1";
            this.panelControl1.Size = new System.Drawing.Size(740, 100);
            this.panelControl1.TabIndex = 2;
            // 
            // simpleButton3
            // 
            this.simpleButton3.Location = new System.Drawing.Point(264, 13);
            this.simpleButton3.Name = "simpleButton3";
            this.simpleButton3.Size = new System.Drawing.Size(320, 23);
            this.simpleButton3.TabIndex = 2;
            this.simpleButton3.Text = "Correção";
            this.simpleButton3.Click += new System.EventHandler(this.simpleButton3_Click);
            // 
            // simpleButton2
            // 
            this.simpleButton2.Location = new System.Drawing.Point(13, 53);
            this.simpleButton2.Name = "simpleButton2";
            this.simpleButton2.Size = new System.Drawing.Size(169, 23);
            this.simpleButton2.TabIndex = 1;
            this.simpleButton2.Text = "Carrega Pagamento";
            this.simpleButton2.Click += new System.EventHandler(this.simpleButton2_Click);
            // 
            // simpleButton1
            // 
            this.simpleButton1.Location = new System.Drawing.Point(13, 13);
            this.simpleButton1.Name = "simpleButton1";
            this.simpleButton1.Size = new System.Drawing.Size(169, 23);
            this.simpleButton1.TabIndex = 0;
            this.simpleButton1.Text = "Carrega Adiantamento";
            this.simpleButton1.Click += new System.EventHandler(this.simpleButton1_Click);
            // 
            // simpleButton4
            // 
            this.simpleButton4.Location = new System.Drawing.Point(264, 53);
            this.simpleButton4.Name = "simpleButton4";
            this.simpleButton4.Size = new System.Drawing.Size(320, 23);
            this.simpleButton4.TabIndex = 3;
            this.simpleButton4.Text = "Nomenclatura";
            this.simpleButton4.Click += new System.EventHandler(this.simpleButton4_Click);
            // 
            // CorrecaoFolha
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(740, 514);
            this.Controls.Add(this.gridControl1);
            this.Controls.Add(this.panelControl1);
            this.Name = "CorrecaoFolha";
            this.Text = "CorrecaoFolha";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            ((System.ComponentModel.ISupportInitialize)(this.gridControl1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dCorrecaoFolha1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl1)).EndInit();
            this.panelControl1.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraGrid.GridControl gridControl1;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView1;
        private DevExpress.XtraEditors.PanelControl panelControl1;
        private dCorrecaoFolha dCorrecaoFolha1;
        private DevExpress.XtraEditors.SimpleButton simpleButton1;
        private DevExpress.XtraEditors.SimpleButton simpleButton2;
        private DevExpress.XtraGrid.Columns.GridColumn colPGF;
        private DevExpress.XtraGrid.Columns.GridColumn colPGF_CON;
        private DevExpress.XtraGrid.Columns.GridColumn colPGFDia;
        private DevExpress.XtraGrid.Columns.GridColumn colPGFDescricao;
        private DevExpress.XtraGrid.Columns.GridColumn colPGFValor;
        private DevExpress.XtraGrid.Columns.GridColumn colPGFProximaCompetencia;
        private DevExpress.XtraEditors.SimpleButton simpleButton3;
        private DevExpress.XtraGrid.Columns.GridColumn colResultado;
        private DevExpress.XtraEditors.SimpleButton simpleButton4;

    }
}