﻿namespace TesteDLL
{


    partial class dInadAberta
    {
        private dInadAbertaTableAdapters.BoletosTableAdapter boletosTableAdapter;
        /// <summary>
        /// TableAdapter padrão para tabela: Boletos
        /// </summary>
        public dInadAbertaTableAdapters.BoletosTableAdapter BoletosTableAdapter
        {
            get
            {
                if (boletosTableAdapter == null)
                {
                    boletosTableAdapter = new dInadAbertaTableAdapters.BoletosTableAdapter();
                    boletosTableAdapter.TrocarStringDeConexao();
                };
                return boletosTableAdapter;
            }
        }

        private dInadAbertaTableAdapters.BoletoDetalheTableAdapter boletoDetalheTableAdapter;
        /// <summary>
        /// TableAdapter padrão para tabela: BoletoDetalhe
        /// </summary>
        public dInadAbertaTableAdapters.BoletoDetalheTableAdapter BoletoDetalheTableAdapter
        {
            get
            {
                if (boletoDetalheTableAdapter == null)
                {
                    boletoDetalheTableAdapter = new dInadAbertaTableAdapters.BoletoDetalheTableAdapter();
                    boletoDetalheTableAdapter.TrocarStringDeConexao();
                };
                return boletoDetalheTableAdapter;
            }
        }
    }
}
