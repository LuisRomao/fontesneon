namespace TesteDLL
{
    partial class Teste12m
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.panelControl1 = new DevExpress.XtraEditors.PanelControl();
            this.simpleButton3 = new DevExpress.XtraEditors.SimpleButton();
            this.simpleButton2 = new DevExpress.XtraEditors.SimpleButton();
            this.simpleButton1 = new DevExpress.XtraEditors.SimpleButton();
            this.xtraTabControl1 = new DevExpress.XtraTab.XtraTabControl();
            this.xtraTabPage1 = new DevExpress.XtraTab.XtraTabPage();
            this.gridControl1 = new DevExpress.XtraGrid.GridControl();
            this.historico12CredBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.gridView1 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colCodcon = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colValorPago = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colCr�dito = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colDatadeLan�amento = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colTipopag = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colM�sCompet�ncia = new DevExpress.XtraGrid.Columns.GridColumn();
            this.xtraTabPage2 = new DevExpress.XtraTab.XtraTabPage();
            this.gridControl2 = new DevExpress.XtraGrid.GridControl();
            this.gridView2 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.resumoBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.colano = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colmes = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colAcordo = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colNoMes = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colAntecipado = new DevExpress.XtraGrid.Columns.GridColumn();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl1)).BeginInit();
            this.panelControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.xtraTabControl1)).BeginInit();
            this.xtraTabControl1.SuspendLayout();
            this.xtraTabPage1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.historico12CredBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).BeginInit();
            this.xtraTabPage2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.resumoBindingSource)).BeginInit();
            this.SuspendLayout();
            // 
            // panelControl1
            // 
            this.panelControl1.Controls.Add(this.simpleButton3);
            this.panelControl1.Controls.Add(this.simpleButton2);
            this.panelControl1.Controls.Add(this.simpleButton1);
            this.panelControl1.Dock = System.Windows.Forms.DockStyle.Top;
            this.panelControl1.Location = new System.Drawing.Point(0, 0);
            this.panelControl1.Name = "panelControl1";
            this.panelControl1.Size = new System.Drawing.Size(761, 76);
            this.panelControl1.TabIndex = 0;
            // 
            // simpleButton3
            // 
            this.simpleButton3.Location = new System.Drawing.Point(187, 13);
            this.simpleButton3.Name = "simpleButton3";
            this.simpleButton3.Size = new System.Drawing.Size(168, 23);
            this.simpleButton3.TabIndex = 2;
            this.simpleButton3.Text = "Tratar";
            
            // 
            // simpleButton2
            // 
            this.simpleButton2.Location = new System.Drawing.Point(13, 42);
            this.simpleButton2.Name = "simpleButton2";
            this.simpleButton2.Size = new System.Drawing.Size(168, 23);
            this.simpleButton2.TabIndex = 1;
            this.simpleButton2.Text = "Importa";
            this.simpleButton2.Click += new System.EventHandler(this.simpleButton2_Click);
            // 
            // simpleButton1
            // 
            this.simpleButton1.Location = new System.Drawing.Point(13, 13);
            this.simpleButton1.Name = "simpleButton1";
            this.simpleButton1.Size = new System.Drawing.Size(168, 23);
            this.simpleButton1.TabIndex = 0;
            this.simpleButton1.Text = "Carrega";
            this.simpleButton1.Click += new System.EventHandler(this.simpleButton1_Click);
            // 
            // xtraTabControl1
            // 
            this.xtraTabControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.xtraTabControl1.Location = new System.Drawing.Point(0, 76);
            this.xtraTabControl1.Name = "xtraTabControl1";
            this.xtraTabControl1.SelectedTabPage = this.xtraTabPage1;
            this.xtraTabControl1.Size = new System.Drawing.Size(761, 307);
            this.xtraTabControl1.TabIndex = 1;
            this.xtraTabControl1.TabPages.AddRange(new DevExpress.XtraTab.XtraTabPage[] {
            this.xtraTabPage1,
            this.xtraTabPage2});
            // 
            // xtraTabPage1
            // 
            this.xtraTabPage1.Controls.Add(this.gridControl1);
            this.xtraTabPage1.Name = "xtraTabPage1";
            this.xtraTabPage1.Size = new System.Drawing.Size(753, 279);
            this.xtraTabPage1.Text = "Puro";
            // 
            // gridControl1
            // 
            this.gridControl1.DataMember = "Historico12Cred";
            this.gridControl1.DataSource = this.historico12CredBindingSource;
            this.gridControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridControl1.Location = new System.Drawing.Point(0, 0);
            this.gridControl1.MainView = this.gridView1;
            this.gridControl1.Name = "gridControl1";
            this.gridControl1.Size = new System.Drawing.Size(753, 279);
            this.gridControl1.TabIndex = 0;
            this.gridControl1.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridView1});
            // 
            // historico12CredBindingSource
            // 
            this.historico12CredBindingSource.DataSource = typeof(dllpublicaBalancete.dPublicah12);
            this.historico12CredBindingSource.Position = 0;
            // 
            // gridView1
            // 
            this.gridView1.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colCodcon,
            this.colValorPago,
            this.colCr�dito,
            this.colDatadeLan�amento,
            this.colTipopag,
            this.colM�sCompet�ncia});
            this.gridView1.GridControl = this.gridControl1;
            this.gridView1.Name = "gridView1";
            // 
            // colCodcon
            // 
            this.colCodcon.FieldName = "Codcon";
            this.colCodcon.Name = "colCodcon";
            this.colCodcon.Visible = true;
            this.colCodcon.VisibleIndex = 0;
            // 
            // colValorPago
            // 
            this.colValorPago.FieldName = "Valor Pago";
            this.colValorPago.Name = "colValorPago";
            this.colValorPago.Visible = true;
            this.colValorPago.VisibleIndex = 1;
            // 
            // colCr�dito
            // 
            this.colCr�dito.FieldName = "Cr�dito";
            this.colCr�dito.Name = "colCr�dito";
            this.colCr�dito.Visible = true;
            this.colCr�dito.VisibleIndex = 2;
            // 
            // colDatadeLan�amento
            // 
            this.colDatadeLan�amento.FieldName = "Data de Lan�amento";
            this.colDatadeLan�amento.Name = "colDatadeLan�amento";
            this.colDatadeLan�amento.Visible = true;
            this.colDatadeLan�amento.VisibleIndex = 3;
            // 
            // colTipopag
            // 
            this.colTipopag.FieldName = "Tipopag";
            this.colTipopag.Name = "colTipopag";
            this.colTipopag.Visible = true;
            this.colTipopag.VisibleIndex = 4;
            // 
            // colM�sCompet�ncia
            // 
            this.colM�sCompet�ncia.FieldName = "M�s Compet�ncia";
            this.colM�sCompet�ncia.Name = "colM�sCompet�ncia";
            this.colM�sCompet�ncia.Visible = true;
            this.colM�sCompet�ncia.VisibleIndex = 5;
            // 
            // xtraTabPage2
            // 
            this.xtraTabPage2.Controls.Add(this.gridControl2);
            this.xtraTabPage2.Name = "xtraTabPage2";
            this.xtraTabPage2.Size = new System.Drawing.Size(753, 279);
            this.xtraTabPage2.Text = "Tratado";
            // 
            // gridControl2
            // 
            this.gridControl2.DataSource = this.resumoBindingSource;
            this.gridControl2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridControl2.Location = new System.Drawing.Point(0, 0);
            this.gridControl2.MainView = this.gridView2;
            this.gridControl2.Name = "gridControl2";
            this.gridControl2.Size = new System.Drawing.Size(753, 279);
            this.gridControl2.TabIndex = 0;
            this.gridControl2.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridView2});
            // 
            // gridView2
            // 
            this.gridView2.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colano,
            this.colmes,
            this.colAcordo,
            this.colNoMes,
            this.colAntecipado});
            this.gridView2.GridControl = this.gridControl2;
            this.gridView2.Name = "gridView2";
            // 
            // resumoBindingSource
            // 
            this.resumoBindingSource.DataMember = "Resumo";
            this.resumoBindingSource.DataSource = this.historico12CredBindingSource;
            // 
            // colano
            // 
            this.colano.FieldName = "ano";
            this.colano.Name = "colano";
            this.colano.Visible = true;
            this.colano.VisibleIndex = 0;
            // 
            // colmes
            // 
            this.colmes.FieldName = "mes";
            this.colmes.Name = "colmes";
            this.colmes.Visible = true;
            this.colmes.VisibleIndex = 1;
            // 
            // colAcordo
            // 
            this.colAcordo.FieldName = "Acordo";
            this.colAcordo.Name = "colAcordo";
            this.colAcordo.Visible = true;
            this.colAcordo.VisibleIndex = 2;
            // 
            // colNoMes
            // 
            this.colNoMes.FieldName = "NoMes";
            this.colNoMes.Name = "colNoMes";
            this.colNoMes.Visible = true;
            this.colNoMes.VisibleIndex = 3;
            // 
            // colAntecipado
            // 
            this.colAntecipado.FieldName = "Antecipado";
            this.colAntecipado.Name = "colAntecipado";
            this.colAntecipado.Visible = true;
            this.colAntecipado.VisibleIndex = 4;
            // 
            // Teste12m
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(761, 383);
            this.Controls.Add(this.xtraTabControl1);
            this.Controls.Add(this.panelControl1);
            this.Name = "Teste12m";
            this.Text = "Teste12m";
            ((System.ComponentModel.ISupportInitialize)(this.panelControl1)).EndInit();
            this.panelControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.xtraTabControl1)).EndInit();
            this.xtraTabControl1.ResumeLayout(false);
            this.xtraTabPage1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gridControl1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.historico12CredBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).EndInit();
            this.xtraTabPage2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gridControl2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.resumoBindingSource)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraEditors.PanelControl panelControl1;
        private DevExpress.XtraEditors.SimpleButton simpleButton2;
        private DevExpress.XtraEditors.SimpleButton simpleButton1;
        private DevExpress.XtraTab.XtraTabControl xtraTabControl1;
        private DevExpress.XtraTab.XtraTabPage xtraTabPage1;
        private DevExpress.XtraGrid.GridControl gridControl1;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView1;
        private DevExpress.XtraTab.XtraTabPage xtraTabPage2;
        private DevExpress.XtraGrid.GridControl gridControl2;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView2;
        private System.Windows.Forms.BindingSource historico12CredBindingSource;
        private DevExpress.XtraGrid.Columns.GridColumn colCodcon;
        private DevExpress.XtraGrid.Columns.GridColumn colValorPago;
        private DevExpress.XtraGrid.Columns.GridColumn colCr�dito;
        private DevExpress.XtraGrid.Columns.GridColumn colDatadeLan�amento;
        private DevExpress.XtraGrid.Columns.GridColumn colTipopag;
        private DevExpress.XtraGrid.Columns.GridColumn colM�sCompet�ncia;
        private DevExpress.XtraEditors.SimpleButton simpleButton3;
        private System.Windows.Forms.BindingSource resumoBindingSource;
        private DevExpress.XtraGrid.Columns.GridColumn colano;
        private DevExpress.XtraGrid.Columns.GridColumn colmes;
        private DevExpress.XtraGrid.Columns.GridColumn colAcordo;
        private DevExpress.XtraGrid.Columns.GridColumn colNoMes;
        private DevExpress.XtraGrid.Columns.GridColumn colAntecipado;
    }
}