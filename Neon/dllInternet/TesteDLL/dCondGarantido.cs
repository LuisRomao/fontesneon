﻿namespace TesteDLL
{


    partial class dCondGarantido
    {
        private dCondGarantidoTableAdapters.CondominioTableAdapter condominioTableAdapter;
        /// <summary>
        /// TableAdapter padrão para tabela: Condominio
        /// </summary>
        public dCondGarantidoTableAdapters.CondominioTableAdapter CondominioTableAdapter
        {
            get
            {
                if (condominioTableAdapter == null)
                {
                    condominioTableAdapter = new dCondGarantidoTableAdapters.CondominioTableAdapter();
                    condominioTableAdapter.TrocarStringDeConexao();
                };
                return condominioTableAdapter;
            }
        }

        private dCondGarantidoTableAdapters.BOLetosTableAdapter bOLetosTableAdapter;
        /// <summary>
        /// TableAdapter padrão para tabela: BOLetos
        /// </summary>
        public dCondGarantidoTableAdapters.BOLetosTableAdapter BOLetosTableAdapter
        {
            get
            {
                if (bOLetosTableAdapter == null)
                {
                    bOLetosTableAdapter = new dCondGarantidoTableAdapters.BOLetosTableAdapter();
                    bOLetosTableAdapter.TrocarStringDeConexao();
                };
                return bOLetosTableAdapter;
            }
        }

        private dCondGarantidoTableAdapters.TaxaTableAdapter taxaTableAdapter;
        /// <summary>
        /// TableAdapter padrão para tabela: Taxa
        /// </summary>
        public dCondGarantidoTableAdapters.TaxaTableAdapter TaxaTableAdapter
        {
            get
            {
                if (taxaTableAdapter == null)
                {
                    taxaTableAdapter = new dCondGarantidoTableAdapters.TaxaTableAdapter();
                    taxaTableAdapter.TrocarStringDeConexao();
                };
                return taxaTableAdapter;
            }
        }

        private dCondGarantidoTableAdapters.SaldoTableAdapter saldoTableAdapter;
        /// <summary>
        /// TableAdapter padrão para tabela: Saldo
        /// </summary>
        public dCondGarantidoTableAdapters.SaldoTableAdapter SaldoTableAdapter
        {
            get
            {
                if (saldoTableAdapter == null)
                {
                    saldoTableAdapter = new dCondGarantidoTableAdapters.SaldoTableAdapter();
                    saldoTableAdapter.TrocarStringDeConexao();
                };
                return saldoTableAdapter;
            }
        }
    }
}
