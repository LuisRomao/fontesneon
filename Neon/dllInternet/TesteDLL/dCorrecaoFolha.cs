﻿namespace TesteDLL {
    
    
    public partial class dCorrecaoFolha 
    {
        private dCorrecaoFolhaTableAdapters.PaGamentosFixosTableAdapter paGamentosFixosTableAdapter;
        /// <summary>
        /// TableAdapter padrão para tabela: PaGamentosFixos
        /// </summary>
        public dCorrecaoFolhaTableAdapters.PaGamentosFixosTableAdapter PaGamentosFixosTableAdapter
        {
            get
            {
                if (paGamentosFixosTableAdapter == null)
                {
                    paGamentosFixosTableAdapter = new dCorrecaoFolhaTableAdapters.PaGamentosFixosTableAdapter();
                    paGamentosFixosTableAdapter.TrocarStringDeConexao();
                };
                return paGamentosFixosTableAdapter;
            }
        }

        private dCorrecaoFolhaTableAdapters.NOtAsTableAdapter nOtAsTableAdapter;
        /// <summary>
        /// TableAdapter padrão para tabela: NOtAs
        /// </summary>
        public dCorrecaoFolhaTableAdapters.NOtAsTableAdapter NOtAsTableAdapter
        {
            get
            {
                if (nOtAsTableAdapter == null)
                {
                    nOtAsTableAdapter = new dCorrecaoFolhaTableAdapters.NOtAsTableAdapter();
                    nOtAsTableAdapter.TrocarStringDeConexao();
                };
                return nOtAsTableAdapter;
            }
        }

        private dCorrecaoFolhaTableAdapters.PAG_CHETableAdapter pAG_CHETableAdapter;
        /// <summary>
        /// TableAdapter padrão para tabela: PAG_CHE
        /// </summary>
        public dCorrecaoFolhaTableAdapters.PAG_CHETableAdapter PAG_CHETableAdapter
        {
            get
            {
                if (pAG_CHETableAdapter == null)
                {
                    pAG_CHETableAdapter = new dCorrecaoFolhaTableAdapters.PAG_CHETableAdapter();
                    pAG_CHETableAdapter.TrocarStringDeConexao();
                };
                return pAG_CHETableAdapter;
            }
        }
    }
}
