﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using CompontesBasicos;

namespace TesteDLL
{
    public partial class CorrecaoFolha : Form
    {
        public CorrecaoFolha()
        {
            InitializeComponent();
        }

        private void Carrega(int Forma)
        {
            dCorrecaoFolha1.PAG_CHE.Clear();
            dCorrecaoFolha1.NOtAs.Clear();
            dCorrecaoFolha1.PaGamentosFixosTableAdapter.Fill(dCorrecaoFolha1.PaGamentosFixos, Forma);
            dCorrecaoFolha1.NOtAsTableAdapter.FillByForma(dCorrecaoFolha1.NOtAs, Forma);
            dCorrecaoFolha1.PAG_CHETableAdapter.FillByForma(dCorrecaoFolha1.PAG_CHE, Forma);
        }

        private void simpleButton1_Click(object sender, EventArgs e)
        {
            Carrega(25);
        }

        private void simpleButton2_Click(object sender, EventArgs e)
        {
            Carrega(9);
        }

        private void simpleButton3_Click(object sender, EventArgs e)
        {
            using (CompontesBasicos.Espera.cEspera ESP = new CompontesBasicos.Espera.cEspera(this))
            {
                ESP.AtivaGauge(dCorrecaoFolha1.PaGamentosFixos.Count);
                foreach (dCorrecaoFolha.PaGamentosFixosRow PGFTeste in dCorrecaoFolha1.PaGamentosFixos)
                {
                    ESP.Gauge();
                    Application.DoEvents();
                    dCorrecaoFolha.NOtAsRow[] Notas = PGFTeste.GetNOtAsRows();
                    if (Notas.Length == 0)                    
                        PGFTeste.Resultado = "Zerado";                    
                    else if (Notas.Length == 1)
                    {
                        if (Notas[0].NOACompet == 201512)
                        {
                            ContaPagar.Follow.PagamentoPeriodico periodico = new ContaPagar.Follow.PagamentoPeriodico(PGFTeste.PGF);
                            try
                            {
                                periodico.CadastrarProximo();
                                PGFTeste.Resultado = "PGF recalculado";
                            }
                            catch (Exception e3)
                            {
                                PGFTeste.Resultado = e3.Message;
                            }
                        }
                        else
                        {
                            Notas[0].NOACompet = 201512;
                            dCorrecaoFolha1.NOtAsTableAdapter.Update(Notas[0]);
                            PGFTeste.Resultado = "Corrigido";
                        }
                    }
                    else if (Notas.Length == 2)
                    {
                        if (Notas[0].NOACompet == 201512)
                        {
                            if (Notas[0].NOAStatus != 5)
                            {
                                if ((Notas[1].NOAStatus == 5) && (Notas[1].NOACompet == 201601))
                                {
                                    if (PGFTeste.PGFProximaCompetencia == 201602)
                                    {
                                        PGFTeste.PGFProximaCompetencia = 201601;
                                        dCorrecaoFolha1.PaGamentosFixosTableAdapter.Update(PGFTeste);
                                        PGFTeste.Resultado = "Competência corrigida";
                                    }
                                    else
                                        PGFTeste.Resultado = "ok 2";
                                }
                                else
                                    PGFTeste.Resultado = "CHECAR";
                            }
                            else
                            {
                                dCorrecaoFolha.PAG_CHERow[] Pags = Notas[0].GetPAG_CHERows();
                                if (Pags.Length != 1)
                                    PGFTeste.Resultado = "Número de PAGs <> 1";
                                else
                                {
                                    if (Pags[0].PAGStatus != 20)
                                        PGFTeste.Resultado = "PagStatus <> 20";
                                    else
                                    {
                                        string Resultado = string.Format("Correção PAG {0} CHE {1} NOA {2}", Pags[0].PAG, Pags[0].CHE, Notas[0].NOA);
                                        VirMSSQL.TableAdapter.AbreTrasacaoSQL("Correcao");
                                        try
                                        {
                                            VirMSSQL.TableAdapter.STTableAdapter.ExecutarSQLNonQuery("delete pagamentos where PAG = @P1", Pags[0].PAG);
                                            VirMSSQL.TableAdapter.STTableAdapter.ExecutarSQLNonQuery("delete Notas where NOA = @P1", Notas[0].NOA);
                                            dllCheques.Cheque Cheque = new dllCheques.Cheque(Pags[0].CHE, dllCheques.Cheque.TipoChave.CHE);
                                            Cheque.AjustarTotalPelasParcelas();
                                            VirMSSQL.TableAdapter.CommitSQL();
                                        }
                                        catch (Exception ex)
                                        {
                                            VirMSSQL.TableAdapter.VircatchSQL(ex);
                                            Resultado = ex.Message;
                                        }
                                        PGFTeste.Resultado = Resultado;
                                    }
                                }
                            }
                        }
                        else
                        {
                            VirMSSQL.TableAdapter.AbreTrasacaoSQL("Correcao", dCorrecaoFolha1.NOtAsTableAdapter);
                            try
                            {
                                Notas[0].NOACompet = 201512;
                                dCorrecaoFolha1.NOtAsTableAdapter.Update(Notas[0]);
                                Notas[1].NOACompet = 201601;
                                dCorrecaoFolha1.NOtAsTableAdapter.Update(Notas[1]);
                                PGFTeste.Resultado = "Corrigido";
                                VirMSSQL.TableAdapter.CommitSQL();
                            }
                            catch (Exception ex)
                            {
                                VirMSSQL.TableAdapter.VircatchSQL(ex);
                                PGFTeste.Resultado = ex.Message;
                            }
                        }
                    }
                    else
                    {
                        if ((Notas[0].NOAStatus != 5) &&
                           (Notas[1].NOAStatus != 5) &&
                           (Notas[2].NOAStatus == 5) &&
                           (Notas[0].NOACompet == 201512) &&
                           (Notas[1].NOACompet == 201601) &&
                           (Notas[2].NOACompet == 201602))
                        {
                            VirMSSQL.TableAdapter.AbreTrasacaoSQL("Correcao", dCorrecaoFolha1.NOtAsTableAdapter);
                            try
                            {
                                foreach (dCorrecaoFolha.PAG_CHERow rowPAG in Notas[1].GetPAG_CHERows())                                
                                    VirMSSQL.TableAdapter.STTableAdapter.ExecutarSQLNonQuery("update pagamentos set pag_noa = @P1 where PAG = @P2", Notas[0].NOA,rowPAG.PAG);                                
                                VirMSSQL.TableAdapter.STTableAdapter.ExecutarSQLNonQuery("delete Notas where NOA = @P1", Notas[1].NOA);                                
                                Notas[2].NOACompet = 201601;
                                dCorrecaoFolha1.NOtAsTableAdapter.Update(Notas[2]);
                                VirMSSQL.TableAdapter.CommitSQL();
                                PGFTeste.Resultado = "Corrigido";                                
                            }
                            catch (Exception ex)
                            {
                                VirMSSQL.TableAdapter.VircatchSQL(ex);
                                PGFTeste.Resultado = ex.Message;
                            }
                        }
                        else
                            PGFTeste.Resultado = "CHECAR";
                    }
                }
            }
        }

        private void simpleButton4_Click(object sender, EventArgs e)
        {
            foreach (dCorrecaoFolha.NOtAsRow rowNOA in dCorrecaoFolha1.NOtAs)
            {
                Framework.objetosNeon.Competencia comp = new Framework.objetosNeon.Competencia(rowNOA.NOACompet);
                string descricao = string.Format("Salário ref.{0}",comp);
                if (descricao != rowNOA.NOAServico)
                {
                    rowNOA.NOAServico = descricao;
                    dCorrecaoFolha1.NOtAsTableAdapter.Update(rowNOA);
                }
            }
            MessageBox.Show("Terminado");
        }
    }
}
