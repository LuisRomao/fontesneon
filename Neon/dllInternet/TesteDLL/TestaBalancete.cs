﻿using System;
using System.Collections.Generic;
using System.Linq;
using DevExpress.XtraReports.UI;
using VirEnumeracoesNeon;

namespace TesteDLL
{
    public partial class TestaBalancete : CompontesBasicos.ComponenteBase
    {
        private XtraReport _XtraReport_teste;

        private Balancete.GrBalancete.balancete _Balancete_Teste;

        public TestaBalancete()
        {
            InitializeComponent();
        }

        private void ImprimeNovo( TipoBalancete Modelo)
        {
            Balancete.GrBalancete.balancete Balancete_Local =  (Balancete.GrBalancete.balancete)Balancete.GrBalancete.balancete.GetBalancete((int)spin_CON.Value, new Framework.objetosNeon.Competencia((int)spin_comp.Value), Modelo);           
            _XtraReport_teste = Balancete_Local.GeraImpresso(false, null, new List<Balancete.GrBalancete.balancete.ParteImpresso>(new Balancete.GrBalancete.balancete.ParteImpresso[] { Balancete.GrBalancete.balancete.ParteImpresso.CreditosDebitos }));
            _XtraReport_teste.Print();
        }

        private void simpleButton1_Click(object sender, EventArgs e)
        {            
            ImprimeNovo(TipoBalancete.Balancete);
            ImprimeNovo(TipoBalancete.Boleto);            

        }

        private void simpleButton2_Click(object sender, EventArgs e)
        {
            Balancete.GrBalancete.balancete.MostraBalancetePDF((int)spin_CON.Value, "", new Framework.objetosNeon.Competencia((int)spin_comp.Value), false);
            Balancete.GrBalancete.balancete.MostraBalancetePDF((int)spin_CON.Value, "", new Framework.objetosNeon.Competencia((int)spin_comp.Value), true);
        }

        private void ImprimePorMBA(int MBA)
        {
            Framework.objetosNeon.AgrupadorBalancete.solicitaAgrupamento solicitaAgrupamento_1 = new Framework.objetosNeon.AgrupadorBalancete.solicitaAgrupamento(Framework.objetosNeon.AgrupadorBalancete.TipoClassificacaoDesp.CadastroMBA, MBA);
            _Balancete_Teste.CarregarDadosBalancete(solicitaAgrupamento_1);
            _XtraReport_teste = _Balancete_Teste.GeraImpresso(false,solicitaAgrupamento_1, new List<Balancete.GrBalancete.balancete.ParteImpresso>(new Balancete.GrBalancete.balancete.ParteImpresso[] { Balancete.GrBalancete.balancete.ParteImpresso.CreditosDebitos }));
            _XtraReport_teste.Print();
        }

        private void simpleButton3_Click(object sender, EventArgs e)
        {
            _Balancete_Teste = (Balancete.GrBalancete.balancete)Balancete.GrBalancete.balancete.GetBalancete((int)spin_CON.Value, new Framework.objetosNeon.Competencia((int)spin_comp.Value), TipoBalancete.Balancete);
            ImprimePorMBA(3);
            ImprimePorMBA(7);
            ImprimePorMBA(3);
            ImprimePorMBA(7);
            //_Balancete_Teste.
            //_XtraReport_teste = _Balancete_Teste.GeraImpresso(false, new List<Balancete.GrBalancete.balancete.ParteImpresso>(new Balancete.GrBalancete.balancete.ParteImpresso[] { Balancete.GrBalancete.balancete.ParteImpresso.CreditosDebitos }));
            //_XtraReport_teste.ShowPreviewDialog();
            //ImprimeNovo(TipoBalancete.Acumulado);
        }

        private void simpleButton4_Click(object sender, EventArgs e)
        {
            _Balancete_Teste = (Balancete.GrBalancete.balancete)Balancete.GrBalancete.balancete.GetBalancete((int)spin_CON.Value, new Framework.objetosNeon.Competencia((int)spin_comp.Value), TipoBalancete.Balancete);
            _Balancete_Teste.CarregaInad();
            _Balancete_Teste.SalvarXML();
        }

        private void simpleButton5_Click(object sender, EventArgs e)
        {
            _Balancete_Teste = (Balancete.GrBalancete.balancete)Balancete.GrBalancete.balancete.GetBalancete((int)spin_CON.Value, new Framework.objetosNeon.Competencia((int)spin_comp.Value), TipoBalancete.Boleto);            
        }
    }
}
