﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using CompontesBasicos;
using Framework.objetosNeon;

namespace TesteDLL
{
    public partial class CondGarantido : ComponenteBase
    {
        public CondGarantido()
        {
            InitializeComponent();
        }

        private dCondGarantido dCondGarantido1;

        private void simpleButton1_Click(object sender, EventArgs e)
        {
            bool erro = false;
            Console.WriteLine("{0} INICIO", DateTime.Now);
            dCondGarantido1 = new dCondGarantido();
            Console.WriteLine("{0} carga 1/3", DateTime.Now);
            dCondGarantido1.CondominioTableAdapter.Fill(dCondGarantido1.Condominio);
            Console.WriteLine("{0} carga 2/3", DateTime.Now);
            dCondGarantido1.TaxaTableAdapter.Fill(dCondGarantido1.Taxa);
            Console.WriteLine("{0} carga 3/3", DateTime.Now);
            dCondGarantido1.SaldoTableAdapter.Fill(dCondGarantido1.Saldo);            
            Competencia CompI = new Competencia(6,2016);
            Competencia CompF = new Competencia(2, 2018);
            dCondGarantido1.BOLetos.Clear();
            dCondGarantido1.BOLetosTableAdapter.ClearBeforeFill = false;
            for (Competencia Comp = CompI; Comp <= CompF; Comp++)
            {
                Console.WriteLine("{0} Popula {1}", DateTime.Now,Comp);
                Popula(Comp);
            }
            StringBuilder SB = new StringBuilder();
            Console.WriteLine("{0} Cabeçalhos", DateTime.Now);
            foreach (dCondGarantido.CondominioRow Crow in dCondGarantido1.Condominio)
            {
                decimal Taxa = 0;
                decimal Saldo = 0;
                if (Crow.TaxaRow != null)
                    Taxa = Crow.TaxaRow.PGFValor;
                if (Crow.SaldoRow != null)
                    Saldo = Crow.SaldoRow.saldo;
                Console.WriteLine("{0} Cabeçalhos => {1}", DateTime.Now, Crow.CONCodigo);
                string DadosCond = string.Format("{0}\t{1}\t{2}\t{3}\t{4}\t{5}\t{6}\t{7}\t{8}\t{9:dd/MM/yyyy}\t{10:n2}\t{11:n2}",
                                      Crow.CONCodigo,
                                      Crow.CONNome,
                                      //"Neon SBC",
                                      "Neon SA",
                                      Crow.CONBairro,
                                      Crow.CIDNome,
                                      Crow.CIDUf,
                                      Crow.CONTotalApartamento,
                                      Crow.USUNome,
                                      Crow.CCTTipo == 6 ? "Pool" : "Individual",
                                      Crow.CONDataInclusao,
                                      Taxa,
                                      Saldo);
                for (Competencia Comp = CompI; Comp <= CompF; Comp++)
                {
                    Console.WriteLine("{0} Dados => {1} {2}", DateTime.Now, Crow.CONCodigo,Comp);
                    SortedList<string, DadosDeUmalinha> Grupos = MontaLinhas(Comp,Crow.GetBOLetosRows());
                    foreach (string TipoBoleto in Grupos.Keys)
                    {
                        int Verificacao = Grupos[TipoBoleto].GetQTD(Periodo.Emitido);
                        if ((TipoBoleto == "H") || (TipoBoleto == "E"))
                            continue;
                        SB.AppendFormat("{0}\t{1}\t{2}", DadosCond, Comp, TipoBoleto);
                        SB.AppendFormat("\t{0}", Grupos[TipoBoleto].GetQTD(Periodo.Emitido));
                        SB.AppendFormat("\t{0}", Grupos[TipoBoleto].GetValor(Periodo.Emitido));
                        SB.AppendFormat("\t{0}", Grupos[TipoBoleto].GetQTD(Periodo.REC));
                        SB.AppendFormat("\t{0}", Grupos[TipoBoleto].GetValor(Periodo.REC));
                        Verificacao -= Grupos[TipoBoleto].GetQTD(Periodo.REC);
                        SB.AppendFormat("\t{0}", Grupos[TipoBoleto].GetQTD(Periodo.REC30));
                        SB.AppendFormat("\t{0}", Grupos[TipoBoleto].GetValor(Periodo.REC30));
                        Verificacao -= Grupos[TipoBoleto].GetQTD(Periodo.REC30);
                        SB.AppendFormat("\t{0}", Grupos[TipoBoleto].GetQTD(Periodo.REC60));
                        SB.AppendFormat("\t{0}", Grupos[TipoBoleto].GetValor(Periodo.REC60));
                        Verificacao -= Grupos[TipoBoleto].GetQTD(Periodo.REC60);
                        SB.AppendFormat("\t{0}", Grupos[TipoBoleto].GetQTD(Periodo.REC90));
                        SB.AppendFormat("\t{0}", Grupos[TipoBoleto].GetValor(Periodo.REC90));
                        Verificacao -= Grupos[TipoBoleto].GetQTD(Periodo.REC90);
                        SB.AppendFormat("\t{0}", Grupos[TipoBoleto].GetQTD(Periodo.REC120));
                        SB.AppendFormat("\t{0}", Grupos[TipoBoleto].GetValor(Periodo.REC120));
                        Verificacao -= Grupos[TipoBoleto].GetQTD(Periodo.REC120);
                        SB.AppendFormat("\t{0}", Grupos[TipoBoleto].GetQTD(Periodo.REC150));
                        SB.AppendFormat("\t{0}", Grupos[TipoBoleto].GetValor(Periodo.REC150));
                        Verificacao -= Grupos[TipoBoleto].GetQTD(Periodo.REC150);
                        SB.AppendFormat("\t{0}", Grupos[TipoBoleto].GetQTD(Periodo.REC180));
                        SB.AppendFormat("\t{0}", Grupos[TipoBoleto].GetValor(Periodo.REC180));
                        Verificacao -= Grupos[TipoBoleto].GetQTD(Periodo.REC180);
                        SB.AppendFormat("\t{0}", Grupos[TipoBoleto].GetQTD(Periodo.REC210));
                        SB.AppendFormat("\t{0}", Grupos[TipoBoleto].GetValor(Periodo.REC210));
                        Verificacao -= Grupos[TipoBoleto].GetQTD(Periodo.REC210);
                        SB.AppendFormat("\t{0}", Grupos[TipoBoleto].GetQTD(Periodo.REC240));
                        SB.AppendFormat("\t{0}", Grupos[TipoBoleto].GetValor(Periodo.REC240));
                        Verificacao -= Grupos[TipoBoleto].GetQTD(Periodo.REC240);
                        SB.AppendFormat("\t{0}", Grupos[TipoBoleto].GetQTD(Periodo.REC270));
                        SB.AppendFormat("\t{0}", Grupos[TipoBoleto].GetValor(Periodo.REC270));
                        Verificacao -= Grupos[TipoBoleto].GetQTD(Periodo.REC270);
                        SB.AppendFormat("\t{0}", Grupos[TipoBoleto].GetQTD(Periodo.REC300));
                        SB.AppendFormat("\t{0}", Grupos[TipoBoleto].GetValor(Periodo.REC300));
                        Verificacao -= Grupos[TipoBoleto].GetQTD(Periodo.REC300);
                        SB.AppendFormat("\t{0}", Grupos[TipoBoleto].GetQTD(Periodo.REC330));
                        SB.AppendFormat("\t{0}", Grupos[TipoBoleto].GetValor(Periodo.REC330));
                        Verificacao -= Grupos[TipoBoleto].GetQTD(Periodo.REC330);
                        SB.AppendFormat("\t{0}", Grupos[TipoBoleto].GetQTD(Periodo.REC365));
                        SB.AppendFormat("\t{0}", Grupos[TipoBoleto].GetValor(Periodo.REC365));
                        Verificacao -= Grupos[TipoBoleto].GetQTD(Periodo.REC365);
                        SB.AppendFormat("\t{0}", Grupos[TipoBoleto].GetQTD(Periodo.RECMais));
                        SB.AppendFormat("\t{0}", Grupos[TipoBoleto].GetValor(Periodo.RECMais));
                        Verificacao -= Grupos[TipoBoleto].GetQTD(Periodo.RECMais);
                        SB.AppendFormat("\t{0}", Grupos[TipoBoleto].GetQTD(Periodo.Aberto));
                        SB.AppendFormat("\t{0}\r\n", Grupos[TipoBoleto].GetValor(Periodo.Aberto));
                        Verificacao -= Grupos[TipoBoleto].GetQTD(Periodo.Aberto);
                        if (Verificacao == 0)
                            Console.WriteLine("ok {0} {1}", Crow.CONCodigo, Comp);
                        else
                        {
                            Console.WriteLine("************* ==>ERRO: {0} {1}", Crow.CONCodigo, Comp);
                            SB.AppendFormat("ERRO\r\n");
                            erro = true;
                        }
                    }

                }
            }
            Console.WriteLine("{0} Fim", DateTime.Now);
            System.IO.File.WriteAllText("c:\\lixo\\Garantido.txt", SB.ToString());
            Clipboard.SetText(SB.ToString());
            if (erro)
                MessageBox.Show("ERRO");
            else
                MessageBox.Show("ok");
            //}
            //catch (Exception ex)
            //{
            //    MessageBox.Show("Erro:",ex.Message);
            //}
            gridControl1.DataSource = dCondGarantido1;
        }

        enum Periodo
        {
            Emitido = 0,
            REC = 1,
            REC30 = 2,
            REC60 = 3,
            REC90 = 4,
            REC120 = 5,
            REC150 = 6,
            REC180 = 7,
            REC210 = 8,
            REC240 = 9,
            REC270 = 10,
            REC300 = 11,
            REC330 = 12,
            REC365 = 13,
            RECMais = 14,
            Aberto = 15,            
        }

        private SortedList<string, DadosDeUmalinha> MontaLinhas(Competencia Comp, dCondGarantido.BOLetosRow[] BoletosDaComp)
        {
            SortedList<string, DadosDeUmalinha> retorno = new SortedList<string, DadosDeUmalinha>();
            foreach (dCondGarantido.BOLetosRow BoletoDaComp in BoletosDaComp)
            {
                if (BoletoDaComp.Competencia != string.Format("{0:00}-{1:0000}", Comp.Mes, Comp.Ano))
                    continue;
                if (!retorno.ContainsKey(BoletoDaComp.BOLTipoCRAI))                
                    retorno.Add(BoletoDaComp.BOLTipoCRAI, new DadosDeUmalinha());                                    
                retorno[BoletoDaComp.BOLTipoCRAI].Grava(BoletoDaComp.Tipo, new Par(BoletoDaComp.QTD, BoletoDaComp.TotValor));
            }
            return retorno;
        }



        class DadosDeUmalinha
        {
            public DadosDeUmalinha()
            {
                Dados = new SortedList<Periodo, Par>();
            }

            private SortedList<Periodo, Par> Dados;

            public void Grava(string Tipo, Par _Par)
            {
                Periodo _Periodo = (Periodo)Enum.Parse(typeof(Periodo),Tipo);
                Grava(_Periodo, _Par);
            }

            public void Grava(Periodo _Periodo, Par _Par)
            {
                Dados.Add(_Periodo, _Par);
            }

            public int GetQTD(Periodo _Periodo)
            {
                if (Dados.ContainsKey(_Periodo))
                    return Dados[_Periodo].Qtd;
                else
                    return 0;
            }

            public decimal GetValor(Periodo _Periodo)
            {
                if (Dados.ContainsKey(_Periodo))
                    return Dados[_Periodo].Valor;
                else
                    return 0;
            }
        }

        class Par
        {
            public int Qtd;
            public decimal Valor;
            public Par(int _Qtd,decimal _Valor)
            {
                Qtd = _Qtd;
                Valor = _Valor;
            }
        }

        private void Popula(Competencia Comp)
        {            
            DateTime DI = new DateTime(Comp.Ano, Comp.Mes, 1);
            DateTime DF = new DateTime(Comp.Ano, Comp.Mes, 1).AddMonths(1).AddDays(-1);
            dCondGarantido1.BOLetosTableAdapter.Fill(dCondGarantido1.BOLetos, string.Format("{0:00}-{1:0000}", Comp.Mes, Comp.Ano), Periodo.Emitido.ToString(), DI, DF);
            dCondGarantido1.BOLetosTableAdapter.FillByREC(dCondGarantido1.BOLetos, string.Format("{0:00}-{1:0000}", Comp.Mes, Comp.Ano), Periodo.REC.ToString(), DI, DF,-1000000,3);
            dCondGarantido1.BOLetosTableAdapter.FillByREC(dCondGarantido1.BOLetos, string.Format("{0:00}-{1:0000}", Comp.Mes, Comp.Ano), Periodo.REC30.ToString(), DI, DF, 4, 33);
            dCondGarantido1.BOLetosTableAdapter.FillByREC(dCondGarantido1.BOLetos, string.Format("{0:00}-{1:0000}", Comp.Mes, Comp.Ano), Periodo.REC60.ToString(), DI, DF, 34, 63);
            dCondGarantido1.BOLetosTableAdapter.FillByREC(dCondGarantido1.BOLetos, string.Format("{0:00}-{1:0000}", Comp.Mes, Comp.Ano), Periodo.REC90.ToString(), DI, DF, 64, 93);
            dCondGarantido1.BOLetosTableAdapter.FillByREC(dCondGarantido1.BOLetos, string.Format("{0:00}-{1:0000}", Comp.Mes, Comp.Ano), Periodo.REC120.ToString(), DI, DF,94, 123);
            dCondGarantido1.BOLetosTableAdapter.FillByREC(dCondGarantido1.BOLetos, string.Format("{0:00}-{1:0000}", Comp.Mes, Comp.Ano), Periodo.REC150.ToString(), DI, DF,124,153);
            dCondGarantido1.BOLetosTableAdapter.FillByREC(dCondGarantido1.BOLetos, string.Format("{0:00}-{1:0000}", Comp.Mes, Comp.Ano), Periodo.REC180.ToString(), DI, DF, 154, 183);
            dCondGarantido1.BOLetosTableAdapter.FillByREC(dCondGarantido1.BOLetos, string.Format("{0:00}-{1:0000}", Comp.Mes, Comp.Ano), Periodo.REC210.ToString(), DI, DF, 184, 213);
            dCondGarantido1.BOLetosTableAdapter.FillByREC(dCondGarantido1.BOLetos, string.Format("{0:00}-{1:0000}", Comp.Mes, Comp.Ano), Periodo.REC240.ToString(), DI, DF, 214, 243);
            dCondGarantido1.BOLetosTableAdapter.FillByREC(dCondGarantido1.BOLetos, string.Format("{0:00}-{1:0000}", Comp.Mes, Comp.Ano), Periodo.REC270.ToString(), DI, DF, 244, 273);
            dCondGarantido1.BOLetosTableAdapter.FillByREC(dCondGarantido1.BOLetos, string.Format("{0:00}-{1:0000}", Comp.Mes, Comp.Ano), Periodo.REC300.ToString(), DI, DF, 274, 303);
            dCondGarantido1.BOLetosTableAdapter.FillByREC(dCondGarantido1.BOLetos, string.Format("{0:00}-{1:0000}", Comp.Mes, Comp.Ano), Periodo.REC330.ToString(), DI, DF, 304, 333);
            dCondGarantido1.BOLetosTableAdapter.FillByREC(dCondGarantido1.BOLetos, string.Format("{0:00}-{1:0000}", Comp.Mes, Comp.Ano), Periodo.REC365.ToString(), DI, DF, 334, 368);
            dCondGarantido1.BOLetosTableAdapter.FillByREC(dCondGarantido1.BOLetos, string.Format("{0:00}-{1:0000}", Comp.Mes, Comp.Ano), Periodo.RECMais.ToString(), DI, DF, 369, 1000000);
            dCondGarantido1.BOLetosTableAdapter.FillByAberto(dCondGarantido1.BOLetos, string.Format("{0:00}-{1:0000}", Comp.Mes, Comp.Ano), Periodo.Aberto.ToString(), DI, DF);
        }
    }
}
