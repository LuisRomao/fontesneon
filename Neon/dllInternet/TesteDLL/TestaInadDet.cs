﻿using DevExpress.XtraReports.UI;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace TesteDLL
{
    public partial class TestaInadDet : CompontesBasicos.ComponenteBase
    {
        public TestaInadDet()
        {
            InitializeComponent();
        }

        private void simpleButton1_Click(object sender, EventArgs e)
        {
            Teste(true);
        }

        private void simpleButton2_Click(object sender, EventArgs e)
        {
            Teste(false);
        }

        private void Teste(bool orinal)
        {
            int CON = 1021;
            Balancete.Relatorios.Inadimplencia.dInadimplencia d1;
            for (int c = (int)NTestesF.Value; c >= (int)NTestesI.Value; c--)
            {
                Balancete.Relatorios.Inadimplencia.ImpInadDet T1 = new Balancete.Relatorios.Inadimplencia.ImpInadDet();
                d1 = new Balancete.Relatorios.Inadimplencia.dInadimplencia();                
                d1.Carregar(CON, new DateTime(2017, 03, 1), new DateTime(2017, 03, 31));
                T1.objectDataSource1.DataSource = d1;
                if (orinal)
                {
                    S0.Value = T1.NCorte[0];
                    S1.Value = T1.NCorte[1];
                    S2.Value = T1.NCorte[2];
                    S3.Value = T1.NCorte[3];
                    S4.Value = T1.NCorte[4];
                    S5.Value = T1.NCorte[5];
                }
                else
                {
                    T1.NCorte[0] = (int)S0.Value;
                    T1.NCorte[1] = (int)S1.Value;
                    T1.NCorte[2] = (int)S2.Value;
                    T1.NCorte[3] = (int)S3.Value;
                    T1.NCorte[4] = (int)S4.Value;
                    T1.NCorte[5] = (int)S5.Value;
                }
                T1.xrTitulo.Text = string.Format("Colunas: {0}", c);
                SortedList<string, string> ColunasTeste = new SortedList<string, string>();                                
                for (int i = 0; i < c; i++)
                {
                    if (i >= d1.ColunasDinamicasInad.Count)
                        ColunasTeste.Add(i.ToString("00000"), string.Format("Coluna {0:00}",i));
                    else
                        ColunasTeste.Add(d1.ColunasDinamicasInad.Keys[i], d1.ColunasDinamicasInad.Values[i]);                    
                }
                T1.CriaColunasDinamicas(ColunasTeste);
                T1.CreateDocument();
                T1.ShowPreview();
            }
        }
    }
}
