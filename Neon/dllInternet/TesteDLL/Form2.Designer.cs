namespace TesteDLL
{
    partial class Form2
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            CompontesBasicos.ModuleInfo moduleInfo1 = new CompontesBasicos.ModuleInfo();
            CompontesBasicos.ModuleInfo moduleInfo2 = new CompontesBasicos.ModuleInfo();
            CompontesBasicos.ModuleInfo moduleInfo3 = new CompontesBasicos.ModuleInfo();
            CompontesBasicos.ModuleInfo moduleInfo4 = new CompontesBasicos.ModuleInfo();
            CompontesBasicos.ModuleInfo moduleInfo5 = new CompontesBasicos.ModuleInfo();
            CompontesBasicos.ModuleInfo moduleInfo6 = new CompontesBasicos.ModuleInfo();
            CompontesBasicos.ModuleInfo moduleInfo7 = new CompontesBasicos.ModuleInfo();
            CompontesBasicos.ModuleInfo moduleInfo8 = new CompontesBasicos.ModuleInfo();
            CompontesBasicos.ModuleInfo moduleInfo9 = new CompontesBasicos.ModuleInfo();
            CompontesBasicos.ModuleInfo moduleInfo10 = new CompontesBasicos.ModuleInfo();
            CompontesBasicos.ModuleInfo moduleInfo11 = new CompontesBasicos.ModuleInfo();
            CompontesBasicos.ModuleInfo moduleInfo12 = new CompontesBasicos.ModuleInfo();
            CompontesBasicos.ModuleInfo moduleInfo13 = new CompontesBasicos.ModuleInfo();
            CompontesBasicos.ModuleInfo moduleInfo14 = new CompontesBasicos.ModuleInfo();
            CompontesBasicos.ModuleInfo moduleInfo15 = new CompontesBasicos.ModuleInfo();
            CompontesBasicos.ModuleInfo moduleInfo16 = new CompontesBasicos.ModuleInfo();
            CompontesBasicos.ModuleInfo moduleInfo17 = new CompontesBasicos.ModuleInfo();
            CompontesBasicos.ModuleInfo moduleInfo18 = new CompontesBasicos.ModuleInfo();
            CompontesBasicos.ModuleInfo moduleInfo19 = new CompontesBasicos.ModuleInfo();
            CompontesBasicos.ModuleInfo moduleInfo20 = new CompontesBasicos.ModuleInfo();
            CompontesBasicos.ModuleInfo moduleInfo21 = new CompontesBasicos.ModuleInfo();
            this.navBarGroup1 = new DevExpress.XtraNavBar.NavBarGroup();
            this.navBarItem1 = new DevExpress.XtraNavBar.NavBarItem();
            this.navBarGroup2 = new DevExpress.XtraNavBar.NavBarGroup();
            this.navBarItem3 = new DevExpress.XtraNavBar.NavBarItem();
            this.navBarItem2 = new DevExpress.XtraNavBar.NavBarItem();
            this.navBarItem4 = new DevExpress.XtraNavBar.NavBarItem();
            this.navBarGroup3 = new DevExpress.XtraNavBar.NavBarGroup();
            this.navBarItem10 = new DevExpress.XtraNavBar.NavBarItem();
            this.navBarItem12 = new DevExpress.XtraNavBar.NavBarItem();
            this.navBarItem9 = new DevExpress.XtraNavBar.NavBarItem();
            this.navBarItem8 = new DevExpress.XtraNavBar.NavBarItem();
            this.navBarItem7 = new DevExpress.XtraNavBar.NavBarItem();
            this.navBarItem6 = new DevExpress.XtraNavBar.NavBarItem();
            this.navBarItem5 = new DevExpress.XtraNavBar.NavBarItem();
            this.navBarGroup4 = new DevExpress.XtraNavBar.NavBarGroup();
            this.navBarItem11 = new DevExpress.XtraNavBar.NavBarItem();
            this.navBarGroup5 = new DevExpress.XtraNavBar.NavBarGroup();
            this.navBarItem13 = new DevExpress.XtraNavBar.NavBarItem();
            ((System.ComponentModel.ISupportInitialize)(this.PictureEdit_F.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.NavBarControl_F)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.BarAndDockingController_F)).BeginInit();
            this.DockPanel_FContainer.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.TabControl_F)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.BarManager_F)).BeginInit();
            this.DockPanel_F.SuspendLayout();
            this.SuspendLayout();
            // 
            // PictureEdit_F
            // 
            this.PictureEdit_F.Cursor = System.Windows.Forms.Cursors.Default;
            this.PictureEdit_F.Margin = new System.Windows.Forms.Padding(2);
            this.PictureEdit_F.EditValueChanged += new System.EventHandler(this.PictureEdit_F_EditValueChanged);
            // 
            // NavBarControl_F
            // 
            this.NavBarControl_F.ActiveGroup = this.navBarGroup1;
            this.NavBarControl_F.Groups.AddRange(new DevExpress.XtraNavBar.NavBarGroup[] {
            this.navBarGroup1,
            this.navBarGroup2,
            this.navBarGroup3,
            this.navBarGroup4,
            this.navBarGroup5});
            this.NavBarControl_F.Items.AddRange(new DevExpress.XtraNavBar.NavBarItem[] {
            this.navBarItem1,
            this.navBarItem2,
            this.navBarItem3,
            this.navBarItem4,
            this.navBarItem5,
            this.navBarItem6,
            this.navBarItem7,
            this.navBarItem8,
            this.navBarItem9,
            this.navBarItem10,
            this.navBarItem11,
            this.navBarItem12,
            this.navBarItem13});
            this.NavBarControl_F.Margin = new System.Windows.Forms.Padding(2);
            this.NavBarControl_F.OptionsNavPane.ExpandedWidth = 194;
            this.NavBarControl_F.Padding = new System.Windows.Forms.Padding(2);
            this.NavBarControl_F.Size = new System.Drawing.Size(194, 430);
            // 
            // BarAndDockingController_F
            // 
            this.BarAndDockingController_F.PropertiesBar.AllowLinkLighting = false;
            this.BarAndDockingController_F.PropertiesBar.DefaultGlyphSize = new System.Drawing.Size(16, 16);
            this.BarAndDockingController_F.PropertiesBar.DefaultLargeGlyphSize = new System.Drawing.Size(32, 32);
            // 
            // DefaultLookAndFeel_F
            // 
            this.DefaultLookAndFeel_F.LookAndFeel.SkinName = "The Asphalt World";
            this.DefaultLookAndFeel_F.LookAndFeel.Style = DevExpress.LookAndFeel.LookAndFeelStyle.Office2003;
            this.DefaultLookAndFeel_F.LookAndFeel.UseWindowsXPTheme = true;
            // 
            // DefaultToolTipController_F
            // 
            // 
            // 
            // 
            this.DefaultToolTipController_F.DefaultController.AutoPopDelay = 10000;
            this.DefaultToolTipController_F.DefaultController.Rounded = true;
            // 
            // barStatus_F
            // 
            this.barStatus_F.OptionsBar.AllowQuickCustomization = false;
            this.barStatus_F.OptionsBar.DisableClose = true;
            this.barStatus_F.OptionsBar.DisableCustomization = true;
            this.barStatus_F.OptionsBar.DrawDragBorder = false;
            this.barStatus_F.OptionsBar.DrawSizeGrip = true;
            this.barStatus_F.OptionsBar.UseWholeRow = true;
            // 
            // DockPanel_FContainer
            // 
            this.DefaultToolTipController_F.SetAllowHtmlText(this.DockPanel_FContainer, DevExpress.Utils.DefaultBoolean.Default);
            this.DockPanel_FContainer.Location = new System.Drawing.Point(3, 20);
            this.DockPanel_FContainer.Margin = new System.Windows.Forms.Padding(2);
            this.DockPanel_FContainer.Size = new System.Drawing.Size(194, 511);
            // 
            // TabControl_F
            // 
            this.TabControl_F.ClosePageButtonShowMode = DevExpress.XtraTab.ClosePageButtonShowMode.InActiveTabPageHeader;
            // 
            // DockPanel_F
            // 
            this.DockPanel_F.Appearance.BackColor = System.Drawing.Color.White;
            this.DockPanel_F.Appearance.Options.UseBackColor = true;
            this.DockPanel_F.Options.AllowDockFill = false;
            this.DockPanel_F.Options.ShowCloseButton = false;
            this.DockPanel_F.Options.ShowMaximizeButton = false;
            // 
            // navBarGroup1
            // 
            this.navBarGroup1.Caption = "navBarGroup1";
            this.navBarGroup1.Name = "navBarGroup1";
            // 
            // navBarItem1
            // 
            this.navBarItem1.Caption = "Publica Balancete";
            this.navBarItem1.Name = "navBarItem1";
            this.navBarItem1.LinkClicked += new DevExpress.XtraNavBar.NavBarLinkEventHandler(this.navBarItem1_LinkClicked);
            // 
            // navBarGroup2
            // 
            this.navBarGroup2.Caption = "Testes";
            this.navBarGroup2.ItemLinks.AddRange(new DevExpress.XtraNavBar.NavBarItemLink[] {
            new DevExpress.XtraNavBar.NavBarItemLink(this.navBarItem3),
            new DevExpress.XtraNavBar.NavBarItemLink(this.navBarItem2),
            new DevExpress.XtraNavBar.NavBarItemLink(this.navBarItem4),
            new DevExpress.XtraNavBar.NavBarItemLink(this.navBarItem1)});
            this.navBarGroup2.Name = "navBarGroup2";
            // 
            // navBarItem3
            // 
            this.navBarItem3.Caption = "Corre��o Folha";
            this.navBarItem3.Name = "navBarItem3";
            this.navBarItem3.LinkClicked += new DevExpress.XtraNavBar.NavBarLinkEventHandler(this.navBarItem3_LinkClicked);
            // 
            // navBarItem2
            // 
            this.navBarItem2.Caption = "Teste 12m";
            this.navBarItem2.Name = "navBarItem2";
            this.navBarItem2.LinkClicked += new DevExpress.XtraNavBar.NavBarLinkEventHandler(this.navBarItem2_LinkClicked);
            // 
            // navBarItem4
            // 
            this.navBarItem4.Caption = "publica acordos";
            this.navBarItem4.Name = "navBarItem4";
            this.navBarItem4.LinkClicked += new DevExpress.XtraNavBar.NavBarLinkEventHandler(this.navBarItem4_LinkClicked);
            // 
            // navBarGroup3
            // 
            this.navBarGroup3.Caption = "navBarGroup3";
            this.navBarGroup3.ItemLinks.AddRange(new DevExpress.XtraNavBar.NavBarItemLink[] {
            new DevExpress.XtraNavBar.NavBarItemLink(this.navBarItem10),
            new DevExpress.XtraNavBar.NavBarItemLink(this.navBarItem12),
            new DevExpress.XtraNavBar.NavBarItemLink(this.navBarItem9),
            new DevExpress.XtraNavBar.NavBarItemLink(this.navBarItem8),
            new DevExpress.XtraNavBar.NavBarItemLink(this.navBarItem7),
            new DevExpress.XtraNavBar.NavBarItemLink(this.navBarItem6),
            new DevExpress.XtraNavBar.NavBarItemLink(this.navBarItem5)});
            this.navBarGroup3.Name = "navBarGroup3";
            // 
            // navBarItem10
            // 
            this.navBarItem10.Caption = "Remessa Boletos";
            this.navBarItem10.Name = "navBarItem10";
            this.navBarItem10.LinkClicked += new DevExpress.XtraNavBar.NavBarLinkEventHandler(this.navBarItem10_LinkClicked);
            // 
            // navBarItem12
            // 
            this.navBarItem12.Caption = "Remessa pag Eletr�nico";
            this.navBarItem12.Name = "navBarItem12";
            this.navBarItem12.LinkClicked += new DevExpress.XtraNavBar.NavBarLinkEventHandler(this.navBarItem12_LinkClicked);
            // 
            // navBarItem9
            // 
            this.navBarItem9.Caption = "Cobran�a";
            this.navBarItem9.Name = "navBarItem9";
            this.navBarItem9.LinkClicked += new DevExpress.XtraNavBar.NavBarLinkEventHandler(this.navBarItem9_LinkClicked);
            // 
            // navBarItem8
            // 
            this.navBarItem8.Caption = "corre��o";
            this.navBarItem8.Name = "navBarItem8";
            this.navBarItem8.LinkClicked += new DevExpress.XtraNavBar.NavBarLinkEventHandler(this.navBarItem8_LinkClicked);
            // 
            // navBarItem7
            // 
            this.navBarItem7.Caption = "Balancete novo";
            this.navBarItem7.Name = "navBarItem7";
            this.navBarItem7.LinkClicked += new DevExpress.XtraNavBar.NavBarLinkEventHandler(this.navBarItem7_LinkClicked);
            // 
            // navBarItem6
            // 
            this.navBarItem6.Caption = "Transfer�ncia L / quad";
            this.navBarItem6.Name = "navBarItem6";
            this.navBarItem6.LinkClicked += new DevExpress.XtraNavBar.NavBarLinkEventHandler(this.navBarItem1_LinkClicked);
            // 
            // navBarItem5
            // 
            this.navBarItem5.Caption = "Transferencia F";
            this.navBarItem5.Name = "navBarItem5";
            this.navBarItem5.LinkClicked += new DevExpress.XtraNavBar.NavBarLinkEventHandler(this.navBarItem5_LinkClicked);
            // 
            // navBarGroup4
            // 
            this.navBarGroup4.Caption = "FGTS";
            this.navBarGroup4.ItemLinks.AddRange(new DevExpress.XtraNavBar.NavBarItemLink[] {
            new DevExpress.XtraNavBar.NavBarItemLink(this.navBarItem11)});
            this.navBarGroup4.Name = "navBarGroup4";
            // 
            // navBarItem11
            // 
            this.navBarItem11.Caption = "FGTS";
            this.navBarItem11.Name = "navBarItem11";
            this.navBarItem11.LinkClicked += new DevExpress.XtraNavBar.NavBarLinkEventHandler(this.navBarItem11_LinkClicked);
            // 
            // navBarGroup5
            // 
            this.navBarGroup5.Caption = "Balancete";
            this.navBarGroup5.Expanded = true;
            this.navBarGroup5.ItemLinks.AddRange(new DevExpress.XtraNavBar.NavBarItemLink[] {
            new DevExpress.XtraNavBar.NavBarItemLink(this.navBarItem13)});
            this.navBarGroup5.Name = "navBarGroup5";
            // 
            // navBarItem13
            // 
            this.navBarItem13.Caption = "Inad Detalhada";
            this.navBarItem13.Name = "navBarItem13";
            this.navBarItem13.LinkClicked += new DevExpress.XtraNavBar.NavBarLinkEventHandler(this.navBarItem13_LinkClicked);
            // 
            // Form2
            // 
            this.DefaultToolTipController_F.SetAllowHtmlText(this, DevExpress.Utils.DefaultBoolean.Default);
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.ClientSize = new System.Drawing.Size(782, 556);
            this.Margin = new System.Windows.Forms.Padding(5);
            moduleInfo1.Grupo = 0;
            moduleInfo1.ImagemG = null;
            moduleInfo1.ImagemP = null;
            moduleInfo1.ModuleType = null;
            moduleInfo1.ModuleTypestr = "Internet.cNeonNet";
            moduleInfo1.Name = "neonnet";
            moduleInfo2.Grupo = 0;
            moduleInfo2.ImagemG = null;
            moduleInfo2.ImagemP = null;
            moduleInfo2.ModuleType = null;
            moduleInfo2.ModuleTypestr = "DP.Automacao_Bancaria.cAutomacaoBancaria";
            moduleInfo2.Name = "DP";
            moduleInfo3.Grupo = 0;
            moduleInfo3.ImagemG = null;
            moduleInfo3.ImagemP = null;
            moduleInfo3.ModuleType = null;
            moduleInfo3.ModuleTypestr = "DP.ChequesDTM.cChequesDTM";
            moduleInfo3.Name = "ChequesLote";
            moduleInfo4.Grupo = 0;
            moduleInfo4.ImagemG = null;
            moduleInfo4.ImagemP = null;
            moduleInfo4.ModuleType = null;
            moduleInfo4.ModuleTypestr = "DP.Apontamentos.cEmitePlanilha";
            moduleInfo4.Name = "Planilha";
            moduleInfo5.Grupo = 0;
            moduleInfo5.ImagemG = null;
            moduleInfo5.ImagemP = null;
            moduleInfo5.ModuleType = null;
            moduleInfo5.ModuleTypestr = "DP.RelPonto.cRelPonto";
            moduleInfo5.Name = "ponto neon";
            moduleInfo6.Grupo = 0;
            moduleInfo6.ImagemG = null;
            moduleInfo6.ImagemP = null;
            moduleInfo6.ModuleType = null;
            moduleInfo6.ModuleTypestr = "dlldirf.cDirf";
            moduleInfo6.Name = "DIRF";
            moduleInfo7.Grupo = 4;
            moduleInfo7.ImagemG = null;
            moduleInfo7.ImagemP = null;
            moduleInfo7.ModuleType = null;
            moduleInfo7.ModuleTypestr = "Balancete.cImpressos";
            moduleInfo7.Name = "balancete";
            moduleInfo8.Grupo = 4;
            moduleInfo8.ImagemG = null;
            moduleInfo8.ImagemP = null;
            moduleInfo8.ModuleType = null;
            moduleInfo8.ModuleTypestr = "Balancete.cGradeBalancetes";
            moduleInfo8.Name = "GradeBalancetes";
            moduleInfo9.Grupo = 0;
            moduleInfo9.ImagemG = null;
            moduleInfo9.ImagemP = null;
            moduleInfo9.ModuleType = null;
            moduleInfo9.ModuleTypestr = "Balancete.cAcumulado";
            moduleInfo9.Name = "Acumulado";
            moduleInfo10.Grupo = 1;
            moduleInfo10.ImagemG = null;
            moduleInfo10.ImagemP = null;
            moduleInfo10.ModuleType = null;
            moduleInfo10.ModuleTypestr = "dllSefip.cSefip";
            moduleInfo10.Name = "Sefip";
            moduleInfo11.Grupo = 0;
            moduleInfo11.ImagemG = null;
            moduleInfo11.ImagemP = null;
            moduleInfo11.ModuleType = null;
            moduleInfo11.ModuleTypestr = "dllSefip.cSefipGuia";
            moduleInfo11.Name = "Guia Sefip";
            moduleInfo12.Grupo = 0;
            moduleInfo12.ImagemG = null;
            moduleInfo12.ImagemP = null;
            moduleInfo12.ModuleType = null;
            moduleInfo12.ModuleTypestr = "DP.Codigos_Bancarios.cCodigosBancariosGrade";
            moduleInfo12.Name = "banco (DP)";
            moduleInfo13.Grupo = 0;
            moduleInfo13.ImagemG = null;
            moduleInfo13.ImagemP = null;
            moduleInfo13.ModuleType = null;
            moduleInfo13.ModuleTypestr = "dllcontabil.cGeraArquivos";
            moduleInfo13.Name = "SPED";
            moduleInfo14.Grupo = 0;
            moduleInfo14.ImagemG = null;
            moduleInfo14.ImagemP = null;
            moduleInfo14.ModuleType = null;
            moduleInfo14.ModuleTypestr = "DP.FGTS.cFGTS";
            moduleInfo14.Name = "FGTS";
            moduleInfo15.Grupo = 0;
            moduleInfo15.ImagemG = null;
            moduleInfo15.ImagemP = null;
            moduleInfo15.ModuleType = null;
            moduleInfo15.ModuleTypestr = "dllExpCont.cExpCont";
            moduleInfo15.Name = "Exporta Dados";
            moduleInfo16.Grupo = 4;
            moduleInfo16.ImagemG = null;
            moduleInfo16.ImagemP = null;
            moduleInfo16.ModuleType = null;
            moduleInfo16.ModuleTypestr = "TesteDLL.TestaBalancete";
            moduleInfo16.Name = "objeto Balancete";
            moduleInfo17.Grupo = 4;
            moduleInfo17.ImagemG = null;
            moduleInfo17.ImagemP = null;
            moduleInfo17.ModuleType = null;
            moduleInfo17.ModuleTypestr = "TesteDLL.inadAberta";
            moduleInfo17.Name = "TesteInad";
            moduleInfo18.Grupo = 4;
            moduleInfo18.ImagemG = null;
            moduleInfo18.ImagemP = null;
            moduleInfo18.ModuleType = null;
            moduleInfo18.ModuleTypestr = "TesteDLL.TestaInadDet";
            moduleInfo18.Name = "Teste Inad det";
            moduleInfo19.Grupo = 4;
            moduleInfo19.ImagemG = null;
            moduleInfo19.ImagemP = null;
            moduleInfo19.ModuleType = null;
            moduleInfo19.ModuleTypestr = "Orcamento.cOrcamento";
            moduleInfo19.Name = "Or�amento";
            moduleInfo20.Grupo = 4;
            moduleInfo20.ImagemG = null;
            moduleInfo20.ImagemP = null;
            moduleInfo20.ModuleType = typeof(dllMalotes.cMalote);
            moduleInfo20.ModuleTypestr = "dllMalotes.cMalote";
            moduleInfo20.Name = "Malote";
            moduleInfo21.Grupo = 1;
            moduleInfo21.ImagemG = null;
            moduleInfo21.ImagemP = null;
            moduleInfo21.ModuleType = typeof(TesteDLL.CondGarantido);            
            moduleInfo21.Name = "Condominio Garantido";
            this.ModuleInfoCollection.AddRange(new CompontesBasicos.ModuleInfo[] {
            moduleInfo1,
            moduleInfo2,
            moduleInfo3,
            moduleInfo4,
            moduleInfo5,
            moduleInfo6,
            moduleInfo7,
            moduleInfo8,
            moduleInfo9,
            moduleInfo10,
            moduleInfo11,
            moduleInfo12,
            moduleInfo13,
            moduleInfo14,
            moduleInfo15,
            moduleInfo16,
            moduleInfo17,
            moduleInfo18,
            moduleInfo19,
            moduleInfo20,
            moduleInfo21});
            this.Name = "Form2";
            this.OcultarTabs = false;
            ((System.ComponentModel.ISupportInitialize)(this.PictureEdit_F.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.NavBarControl_F)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.BarAndDockingController_F)).EndInit();
            this.DockPanel_FContainer.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.TabControl_F)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.BarManager_F)).EndInit();
            this.DockPanel_F.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private DevExpress.XtraNavBar.NavBarGroup navBarGroup1;
        private DevExpress.XtraNavBar.NavBarItem navBarItem1;
        private DevExpress.XtraNavBar.NavBarGroup navBarGroup2;
        private DevExpress.XtraNavBar.NavBarItem navBarItem2;
        private DevExpress.XtraNavBar.NavBarItem navBarItem3;
        private DevExpress.XtraNavBar.NavBarItem navBarItem4;
        private DevExpress.XtraNavBar.NavBarGroup navBarGroup3;
        private DevExpress.XtraNavBar.NavBarItem navBarItem5;
        private DevExpress.XtraNavBar.NavBarItem navBarItem6;
        private DevExpress.XtraNavBar.NavBarItem navBarItem7;
        private DevExpress.XtraNavBar.NavBarItem navBarItem8;
        private DevExpress.XtraNavBar.NavBarItem navBarItem9;
        private DevExpress.XtraNavBar.NavBarItem navBarItem10;
        private DevExpress.XtraNavBar.NavBarGroup navBarGroup4;
        private DevExpress.XtraNavBar.NavBarItem navBarItem11;
        private DevExpress.XtraNavBar.NavBarItem navBarItem12;
        private DevExpress.XtraNavBar.NavBarGroup navBarGroup5;
        private DevExpress.XtraNavBar.NavBarItem navBarItem13;
    }
}
