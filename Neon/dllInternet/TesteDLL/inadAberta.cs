﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace TesteDLL
{
    public partial class inadAberta : CompontesBasicos.ComponenteBase
    {
        public inadAberta()
        {
            InitializeComponent();
        }

        private List<string> NovasColunas;

        private void simpleButton1_Click(object sender, EventArgs e)
        {
            dInadAberta.BoletosTableAdapter.Fill(dInadAberta.Boletos,DateTime.Today.AddDays(-3),845);
            dInadAberta.BoletoDetalheTableAdapter.Fill(dInadAberta.BoletoDetalhe, DateTime.Today.AddDays(-3), 845);
            NovasColunas = new List<string>();
            foreach (dInadAberta.BoletosRow rowBOL in dInadAberta.Boletos)
            {
                foreach (dInadAberta.BoletoDetalheRow rowBOD in rowBOL.GetBoletoDetalheRows())
                {
                    if(!NovasColunas.Contains(rowBOD.BOD_PLA))
                    {
                        Console.WriteLine(rowBOD.BOD_PLA);
                        NovasColunas.Add(rowBOD.BOD_PLA);
                        dInadAberta.Boletos.Columns.Add(rowBOD.BOD_PLA, typeof(decimal));
                        //rowBOL[rowBOD.BOD_PLA] = 0;
                        CriaColuna_GridBolRec(rowBOD.PLADescricaoRes, rowBOD.BOD_PLA, true);
                    }
                    decimal ValAnte = rowBOL[rowBOD.BOD_PLA] == DBNull.Value ? 0 : (decimal)rowBOL[rowBOD.BOD_PLA];
                    rowBOL[rowBOD.BOD_PLA] = ValAnte + rowBOD.BODValor;
                }
            }
        }

        private void CriaColuna_GridBolRec(string Titulo, string strNomeCampo, bool visivel = true)
        {
            //if (NovasColunas == null)
            //{
            //    NovasColunas = new SortedList<string, DevExpress.XtraGrid.Columns.GridColumn>();
            //    SubTotais = new List<DevExpress.XtraGrid.GridGroupSummaryItem>();
            //}
            DevExpress.XtraGrid.Columns.GridColumn Coluna = new DevExpress.XtraGrid.Columns.GridColumn();
            Coluna.AppearanceCell.BackColor = Color.Beige;
            Coluna.AppearanceCell.Options.UseBackColor = true;
            Coluna.Caption = Titulo;
            Coluna.CustomizationCaption = Titulo;
            Coluna.Name = strNomeCampo;
            Coluna.FieldName = strNomeCampo;
            Coluna.Width = 80;
            Coluna.OptionsColumn.FixedWidth = true;
            Coluna.OptionsColumn.AllowMerge = DevExpress.Utils.DefaultBoolean.False;
            Coluna.OptionsColumn.ReadOnly = true;
            //Coluna.ColumnEdit = repValor;
            Coluna.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
                                                new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum, strNomeCampo, "{0:n2}")});
            Coluna.Visible = visivel;
            gridView1.Columns.Add(Coluna);
            DevExpress.XtraGrid.GridGroupSummaryItem SubTotal = new DevExpress.XtraGrid.GridGroupSummaryItem(DevExpress.Data.SummaryItemType.Sum, strNomeCampo, Coluna, "{0:n2}");
            gridView1.GroupSummary.Add(SubTotal);
            //NovasColunas.Add(strNomeCampo, Coluna);
            //SubTotais.Add(SubTotal);
        }


        private void simpleButton2_Click(object sender, EventArgs e)
        {
            gridView1.ExportToXlsx("c:\\lixo\\inad.xlsx");
        }
    }
}
