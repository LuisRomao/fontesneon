namespace TesteDLL
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.gridControl1 = new DevExpress.XtraGrid.GridControl();
            this.gridView1 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.gridControl2 = new DevExpress.XtraGrid.GridControl();
            this.gridView2 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.totaisBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.colcontrole = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colCodcon1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colcompetencia1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.coltitulo = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colvalor = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colquadro = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colnegrito = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colcodcon = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colcompetencia = new DevExpress.XtraGrid.Columns.GridColumn();
            this.coltotrec = new DevExpress.XtraGrid.Columns.GridColumn();
            this.coltotdesp = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colacc = new DevExpress.XtraGrid.Columns.GridColumn();
            this.cols1fr = new DevExpress.XtraGrid.Columns.GridColumn();
            this.cols113 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.cols1po = new DevExpress.XtraGrid.Columns.GridColumn();
            this.cols1cc = new DevExpress.XtraGrid.Columns.GridColumn();
            this.cols1psf = new DevExpress.XtraGrid.Columns.GridColumn();
            this.cols1acp = new DevExpress.XtraGrid.Columns.GridColumn();
            this.cols1af = new DevExpress.XtraGrid.Columns.GridColumn();
            this.coltotdg = new DevExpress.XtraGrid.Columns.GridColumn();
            this.coltotmo = new DevExpress.XtraGrid.Columns.GridColumn();
            this.coltotad = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colmesi = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colanoi = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colstatus = new DevExpress.XtraGrid.Columns.GridColumn();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.totaisBindingSource)).BeginInit();
            this.SuspendLayout();
            // 
            // gridControl1
            // 
            this.gridControl1.DataMember = "totais";
            this.gridControl1.DataSource = this.totaisBindingSource;
            this.gridControl1.Dock = System.Windows.Forms.DockStyle.Top;
            this.gridControl1.Location = new System.Drawing.Point(0, 0);
            this.gridControl1.MainView = this.gridView1;
            this.gridControl1.Name = "gridControl1";
            this.gridControl1.Size = new System.Drawing.Size(824, 316);
            this.gridControl1.TabIndex = 0;
            this.gridControl1.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridView1});
            // 
            // gridView1
            // 
            this.gridView1.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colcodcon,
            this.colcompetencia,
            this.coltotrec,
            this.coltotdesp,
            this.colacc,
            this.cols1fr,
            this.cols113,
            this.cols1po,
            this.cols1cc,
            this.cols1psf,
            this.cols1acp,
            this.cols1af,
            this.coltotdg,
            this.coltotmo,
            this.coltotad,
            this.colmesi,
            this.colanoi,
            this.colstatus});
            this.gridView1.GridControl = this.gridControl1;
            this.gridView1.Name = "gridView1";
            // 
            // gridControl2
            // 
            this.gridControl2.DataMember = "det1";
            this.gridControl2.DataSource = this.totaisBindingSource;
            this.gridControl2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridControl2.Location = new System.Drawing.Point(0, 316);
            this.gridControl2.MainView = this.gridView2;
            this.gridControl2.Name = "gridControl2";
            this.gridControl2.Size = new System.Drawing.Size(824, 285);
            this.gridControl2.TabIndex = 1;
            this.gridControl2.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridView2});
            // 
            // gridView2
            // 
            this.gridView2.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colcontrole,
            this.colCodcon1,
            this.colcompetencia1,
            this.coltitulo,
            this.colvalor,
            this.colquadro,
            this.colnegrito});
            this.gridView2.GridControl = this.gridControl2;
            this.gridView2.Name = "gridView2";
            // 
            // totaisBindingSource
            // 
            this.totaisBindingSource.DataSource = typeof(dllpublicaBalancete.WSSincBancos.dBAL);
            this.totaisBindingSource.Position = 0;
            // 
            // colcontrole
            // 
            this.colcontrole.FieldName = "controle";
            this.colcontrole.Name = "colcontrole";
            this.colcontrole.Visible = true;
            this.colcontrole.VisibleIndex = 0;
            // 
            // colCodcon1
            // 
            this.colCodcon1.FieldName = "Codcon";
            this.colCodcon1.Name = "colCodcon1";
            this.colCodcon1.Visible = true;
            this.colCodcon1.VisibleIndex = 1;
            // 
            // colcompetencia1
            // 
            this.colcompetencia1.FieldName = "competencia";
            this.colcompetencia1.Name = "colcompetencia1";
            this.colcompetencia1.Visible = true;
            this.colcompetencia1.VisibleIndex = 2;
            // 
            // coltitulo
            // 
            this.coltitulo.FieldName = "titulo";
            this.coltitulo.Name = "coltitulo";
            this.coltitulo.Visible = true;
            this.coltitulo.VisibleIndex = 3;
            // 
            // colvalor
            // 
            this.colvalor.FieldName = "valor";
            this.colvalor.Name = "colvalor";
            this.colvalor.Visible = true;
            this.colvalor.VisibleIndex = 4;
            // 
            // colquadro
            // 
            this.colquadro.FieldName = "quadro";
            this.colquadro.Name = "colquadro";
            this.colquadro.Visible = true;
            this.colquadro.VisibleIndex = 5;
            // 
            // colnegrito
            // 
            this.colnegrito.FieldName = "negrito";
            this.colnegrito.Name = "colnegrito";
            this.colnegrito.Visible = true;
            this.colnegrito.VisibleIndex = 6;
            // 
            // colcodcon
            // 
            this.colcodcon.FieldName = "codcon";
            this.colcodcon.Name = "colcodcon";
            this.colcodcon.Visible = true;
            this.colcodcon.VisibleIndex = 0;
            // 
            // colcompetencia
            // 
            this.colcompetencia.FieldName = "competencia";
            this.colcompetencia.Name = "colcompetencia";
            this.colcompetencia.Visible = true;
            this.colcompetencia.VisibleIndex = 1;
            // 
            // coltotrec
            // 
            this.coltotrec.FieldName = "totrec";
            this.coltotrec.Name = "coltotrec";
            this.coltotrec.Visible = true;
            this.coltotrec.VisibleIndex = 2;
            // 
            // coltotdesp
            // 
            this.coltotdesp.FieldName = "totdesp";
            this.coltotdesp.Name = "coltotdesp";
            this.coltotdesp.Visible = true;
            this.coltotdesp.VisibleIndex = 3;
            // 
            // colacc
            // 
            this.colacc.FieldName = "acc";
            this.colacc.Name = "colacc";
            this.colacc.Visible = true;
            this.colacc.VisibleIndex = 4;
            // 
            // cols1fr
            // 
            this.cols1fr.FieldName = "s1fr";
            this.cols1fr.Name = "cols1fr";
            this.cols1fr.Visible = true;
            this.cols1fr.VisibleIndex = 5;
            // 
            // cols113
            // 
            this.cols113.FieldName = "s113";
            this.cols113.Name = "cols113";
            this.cols113.Visible = true;
            this.cols113.VisibleIndex = 6;
            // 
            // cols1po
            // 
            this.cols1po.FieldName = "s1po";
            this.cols1po.Name = "cols1po";
            this.cols1po.Visible = true;
            this.cols1po.VisibleIndex = 7;
            // 
            // cols1cc
            // 
            this.cols1cc.FieldName = "s1cc";
            this.cols1cc.Name = "cols1cc";
            this.cols1cc.Visible = true;
            this.cols1cc.VisibleIndex = 8;
            // 
            // cols1psf
            // 
            this.cols1psf.FieldName = "s1psf";
            this.cols1psf.Name = "cols1psf";
            this.cols1psf.Visible = true;
            this.cols1psf.VisibleIndex = 9;
            // 
            // cols1acp
            // 
            this.cols1acp.FieldName = "s1acp";
            this.cols1acp.Name = "cols1acp";
            this.cols1acp.Visible = true;
            this.cols1acp.VisibleIndex = 10;
            // 
            // cols1af
            // 
            this.cols1af.FieldName = "s1af";
            this.cols1af.Name = "cols1af";
            this.cols1af.Visible = true;
            this.cols1af.VisibleIndex = 11;
            // 
            // coltotdg
            // 
            this.coltotdg.FieldName = "totdg";
            this.coltotdg.Name = "coltotdg";
            this.coltotdg.Visible = true;
            this.coltotdg.VisibleIndex = 12;
            // 
            // coltotmo
            // 
            this.coltotmo.FieldName = "totmo";
            this.coltotmo.Name = "coltotmo";
            this.coltotmo.Visible = true;
            this.coltotmo.VisibleIndex = 13;
            // 
            // coltotad
            // 
            this.coltotad.FieldName = "totad";
            this.coltotad.Name = "coltotad";
            this.coltotad.Visible = true;
            this.coltotad.VisibleIndex = 14;
            // 
            // colmesi
            // 
            this.colmesi.FieldName = "mesi";
            this.colmesi.Name = "colmesi";
            this.colmesi.Visible = true;
            this.colmesi.VisibleIndex = 15;
            // 
            // colanoi
            // 
            this.colanoi.FieldName = "anoi";
            this.colanoi.Name = "colanoi";
            this.colanoi.Visible = true;
            this.colanoi.VisibleIndex = 16;
            // 
            // colstatus
            // 
            this.colstatus.FieldName = "status";
            this.colstatus.Name = "colstatus";
            this.colstatus.Visible = true;
            this.colstatus.VisibleIndex = 17;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(824, 601);
            this.Controls.Add(this.gridControl2);
            this.Controls.Add(this.gridControl1);
            this.Name = "Form1";
            this.Text = "Form1";
            ((System.ComponentModel.ISupportInitialize)(this.gridControl1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.totaisBindingSource)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraGrid.GridControl gridControl1;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView1;
        private DevExpress.XtraGrid.GridControl gridControl2;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView2;
        private DevExpress.XtraGrid.Columns.GridColumn colcodcon;
        private DevExpress.XtraGrid.Columns.GridColumn colcompetencia;
        private DevExpress.XtraGrid.Columns.GridColumn coltotrec;
        private DevExpress.XtraGrid.Columns.GridColumn coltotdesp;
        private DevExpress.XtraGrid.Columns.GridColumn colacc;
        private DevExpress.XtraGrid.Columns.GridColumn cols1fr;
        private DevExpress.XtraGrid.Columns.GridColumn cols113;
        private DevExpress.XtraGrid.Columns.GridColumn cols1po;
        private DevExpress.XtraGrid.Columns.GridColumn cols1cc;
        private DevExpress.XtraGrid.Columns.GridColumn cols1psf;
        private DevExpress.XtraGrid.Columns.GridColumn cols1acp;
        private DevExpress.XtraGrid.Columns.GridColumn cols1af;
        private DevExpress.XtraGrid.Columns.GridColumn coltotdg;
        private DevExpress.XtraGrid.Columns.GridColumn coltotmo;
        private DevExpress.XtraGrid.Columns.GridColumn coltotad;
        private DevExpress.XtraGrid.Columns.GridColumn colmesi;
        private DevExpress.XtraGrid.Columns.GridColumn colanoi;
        private DevExpress.XtraGrid.Columns.GridColumn colstatus;
        private DevExpress.XtraGrid.Columns.GridColumn colcontrole;
        private DevExpress.XtraGrid.Columns.GridColumn colCodcon1;
        private DevExpress.XtraGrid.Columns.GridColumn colcompetencia1;
        private DevExpress.XtraGrid.Columns.GridColumn coltitulo;
        private DevExpress.XtraGrid.Columns.GridColumn colvalor;
        private DevExpress.XtraGrid.Columns.GridColumn colquadro;
        private DevExpress.XtraGrid.Columns.GridColumn colnegrito;
        public System.Windows.Forms.BindingSource totaisBindingSource;
    }
}