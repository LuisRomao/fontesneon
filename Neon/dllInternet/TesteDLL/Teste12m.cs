using System;
using System.Windows.Forms;
using dllpublicaBalancete;

namespace TesteDLL
{
    public partial class Teste12m : Form
    {
        public Teste12m()
        {
            InitializeComponent();
        }

        private dPublicah12 dPub;

        private void simpleButton1_Click(object sender, EventArgs e)
        {
            dPub = new dPublicah12();
            historico12CredBindingSource.DataSource = dPub;
            //int lidos = dPub.Historico12CredTableAdapter.Fill(dPub.Historico12Cred, "ACACIA");
            int lidos = dPub.Historico12CredTableAdapter.Fill(dPub.Historico12Cred, "ESPAN2");
        }

        private void simpleButton2_Click(object sender, EventArgs e)
        {
            dPub = new dPublicah12();
            //dPub.ReadXml(@"c:\lixo\dados.xml");
            dPub.Historico12Cred.ReadXml(@"c:\lixo\dados.xml");

            historico12CredBindingSource.DataSource = dPub;
            //int lidos = dPub.Historico12CredTableAdapter.Fill(dPub.Historico12Cred, "ACACIA");
            //MessageBox.Show(lidos.ToString());
        }

        //int CON = 327;

        
    }
}