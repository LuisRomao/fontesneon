﻿namespace TesteDLL
{
    partial class CondGarantido
    {
        /// <summary> 
        /// Variável de designer necessária.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Limpar os recursos que estão sendo usados.
        /// </summary>
        /// <param name="disposing">true se for necessário descartar os recursos gerenciados; caso contrário, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Código gerado pelo Designer de Componentes

        /// <summary> 
        /// Método necessário para suporte ao Designer - não modifique 
        /// o conteúdo deste método com o editor de código.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            DevExpress.XtraGrid.GridLevelNode gridLevelNode1 = new DevExpress.XtraGrid.GridLevelNode();
            this.panelControl1 = new DevExpress.XtraEditors.PanelControl();
            this.simpleButton1 = new DevExpress.XtraEditors.SimpleButton();
            this.gridControl1 = new DevExpress.XtraGrid.GridControl();
            this.dCondGarantidoBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.gridView1 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colCONCodigo = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colCONNome = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colCONBairro = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colCIDNome = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colCIDUf = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colCONTotalApartamento = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colUSUNome = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colCONDataInclusao = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colCCTTipo = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colCON = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridView2 = new DevExpress.XtraGrid.Views.Grid.GridView();
            ((System.ComponentModel.ISupportInitialize)(this.StyleController_F)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl1)).BeginInit();
            this.panelControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dCondGarantidoBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView2)).BeginInit();
            this.SuspendLayout();
            // 
            // ToolTipController_F
            // 
            this.ToolTipController_F.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.ToolTipController_F.Appearance.ForeColor = System.Drawing.Color.Black;
            this.ToolTipController_F.Appearance.Options.UseBackColor = true;
            this.ToolTipController_F.Appearance.Options.UseFont = true;
            this.ToolTipController_F.Appearance.Options.UseForeColor = true;
            this.ToolTipController_F.Appearance.Options.UseTextOptions = true;
            this.ToolTipController_F.Appearance.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            // 
            // StyleController_F
            // 
            this.StyleController_F.AppearanceDisabled.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this.StyleController_F.AppearanceDisabled.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this.StyleController_F.AppearanceDisabled.Options.UseBackColor = true;
            this.StyleController_F.AppearanceDisabled.Options.UseForeColor = true;
            this.StyleController_F.AppearanceFocused.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.StyleController_F.AppearanceFocused.Options.UseBackColor = true;
            // 
            // panelControl1
            // 
            this.panelControl1.Controls.Add(this.simpleButton1);
            this.panelControl1.Dock = System.Windows.Forms.DockStyle.Top;
            this.panelControl1.Location = new System.Drawing.Point(0, 0);
            this.panelControl1.Name = "panelControl1";
            this.panelControl1.Size = new System.Drawing.Size(1614, 100);
            this.panelControl1.TabIndex = 0;
            // 
            // simpleButton1
            // 
            this.simpleButton1.Location = new System.Drawing.Point(6, 6);
            this.simpleButton1.Name = "simpleButton1";
            this.simpleButton1.Size = new System.Drawing.Size(178, 89);
            this.simpleButton1.TabIndex = 0;
            this.simpleButton1.Text = "Calcula";
            this.simpleButton1.Click += new System.EventHandler(this.simpleButton1_Click);
            // 
            // gridControl1
            // 
            this.gridControl1.DataMember = "Condominio";
            this.gridControl1.DataSource = this.dCondGarantidoBindingSource;
            this.gridControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            gridLevelNode1.RelationName = "Condominio_BOLetos";
            this.gridControl1.LevelTree.Nodes.AddRange(new DevExpress.XtraGrid.GridLevelNode[] {
            gridLevelNode1});
            this.gridControl1.Location = new System.Drawing.Point(0, 100);
            this.gridControl1.MainView = this.gridView1;
            this.gridControl1.Name = "gridControl1";
            this.gridControl1.Size = new System.Drawing.Size(1614, 690);
            this.gridControl1.TabIndex = 1;
            this.gridControl1.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridView1,
            this.gridView2});
            // 
            // dCondGarantidoBindingSource
            // 
            this.dCondGarantidoBindingSource.DataSource = typeof(TesteDLL.dCondGarantido);
            this.dCondGarantidoBindingSource.Position = 0;
            // 
            // gridView1
            // 
            this.gridView1.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colCONCodigo,
            this.colCONNome,
            this.colCONBairro,
            this.colCIDNome,
            this.colCIDUf,
            this.colCONTotalApartamento,
            this.colUSUNome,
            this.colCONDataInclusao,
            this.colCCTTipo,
            this.colCON});
            this.gridView1.GridControl = this.gridControl1;
            this.gridView1.Name = "gridView1";
            // 
            // colCONCodigo
            // 
            this.colCONCodigo.FieldName = "CONCodigo";
            this.colCONCodigo.Name = "colCONCodigo";
            this.colCONCodigo.Visible = true;
            this.colCONCodigo.VisibleIndex = 0;
            // 
            // colCONNome
            // 
            this.colCONNome.FieldName = "CONNome";
            this.colCONNome.Name = "colCONNome";
            this.colCONNome.Visible = true;
            this.colCONNome.VisibleIndex = 1;
            // 
            // colCONBairro
            // 
            this.colCONBairro.FieldName = "CONBairro";
            this.colCONBairro.Name = "colCONBairro";
            this.colCONBairro.Visible = true;
            this.colCONBairro.VisibleIndex = 2;
            // 
            // colCIDNome
            // 
            this.colCIDNome.FieldName = "CIDNome";
            this.colCIDNome.Name = "colCIDNome";
            this.colCIDNome.Visible = true;
            this.colCIDNome.VisibleIndex = 3;
            // 
            // colCIDUf
            // 
            this.colCIDUf.FieldName = "CIDUf";
            this.colCIDUf.Name = "colCIDUf";
            this.colCIDUf.Visible = true;
            this.colCIDUf.VisibleIndex = 4;
            // 
            // colCONTotalApartamento
            // 
            this.colCONTotalApartamento.FieldName = "CONTotalApartamento";
            this.colCONTotalApartamento.Name = "colCONTotalApartamento";
            this.colCONTotalApartamento.Visible = true;
            this.colCONTotalApartamento.VisibleIndex = 5;
            // 
            // colUSUNome
            // 
            this.colUSUNome.FieldName = "USUNome";
            this.colUSUNome.Name = "colUSUNome";
            this.colUSUNome.Visible = true;
            this.colUSUNome.VisibleIndex = 6;
            // 
            // colCONDataInclusao
            // 
            this.colCONDataInclusao.FieldName = "CONDataInclusao";
            this.colCONDataInclusao.Name = "colCONDataInclusao";
            this.colCONDataInclusao.Visible = true;
            this.colCONDataInclusao.VisibleIndex = 7;
            // 
            // colCCTTipo
            // 
            this.colCCTTipo.FieldName = "CCTTipo";
            this.colCCTTipo.Name = "colCCTTipo";
            this.colCCTTipo.Visible = true;
            this.colCCTTipo.VisibleIndex = 8;
            // 
            // colCON
            // 
            this.colCON.FieldName = "CON";
            this.colCON.Name = "colCON";
            this.colCON.Visible = true;
            this.colCON.VisibleIndex = 9;
            // 
            // gridView2
            // 
            this.gridView2.GridControl = this.gridControl1;
            this.gridView2.Name = "gridView2";
            // 
            // CondGarantido
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.gridControl1);
            this.Controls.Add(this.panelControl1);
            this.Doc = System.Windows.Forms.DockStyle.Fill;
            this.LookAndFeel.UseDefaultLookAndFeel = false;
            this.Name = "CondGarantido";
            this.Size = new System.Drawing.Size(1614, 790);
            ((System.ComponentModel.ISupportInitialize)(this.StyleController_F)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl1)).EndInit();
            this.panelControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gridControl1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dCondGarantidoBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView2)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraEditors.PanelControl panelControl1;
        private DevExpress.XtraEditors.SimpleButton simpleButton1;
        private DevExpress.XtraGrid.GridControl gridControl1;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView1;
        private System.Windows.Forms.BindingSource dCondGarantidoBindingSource;
        private DevExpress.XtraGrid.Columns.GridColumn colCONCodigo;
        private DevExpress.XtraGrid.Columns.GridColumn colCONNome;
        private DevExpress.XtraGrid.Columns.GridColumn colCONBairro;
        private DevExpress.XtraGrid.Columns.GridColumn colCIDNome;
        private DevExpress.XtraGrid.Columns.GridColumn colCIDUf;
        private DevExpress.XtraGrid.Columns.GridColumn colCONTotalApartamento;
        private DevExpress.XtraGrid.Columns.GridColumn colUSUNome;
        private DevExpress.XtraGrid.Columns.GridColumn colCONDataInclusao;
        private DevExpress.XtraGrid.Columns.GridColumn colCCTTipo;
        private DevExpress.XtraGrid.Columns.GridColumn colCON;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView2;
    }
}
