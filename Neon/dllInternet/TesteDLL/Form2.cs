/*
MR - 21/03/2016 20:00 -            - Registro de boletos separado para Bradesco e Itau (Altera��es indicadas por *** MRC - INICIO (21/03/2016 20:00) ***)
*/
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using DevExpress.XtraReports.UI;

namespace TesteDLL
{
    public partial class Form2 : CompontesBasicos.FormPrincipalBase
    {
        public Form2()
        {
            InitializeComponent();
        }

        private void navBarItem1_LinkClicked(object sender, DevExpress.XtraNavBar.NavBarLinkEventArgs e)
        {
            MessageBox.Show("desligado");
            /*
            //Framework.objetosNeon.Competencia Comp = new Framework.objetosNeon.Competencia(3,2010,387);
            //DateTime DI = new DateTime(2010,3,1);
            //DateTime DF = new DateTime(2010,3,31);
            //Framework.objetosNeon.Competencia Comp = new Framework.objetosNeon.Competencia(11,2012,233);
            Framework.objetosNeon.Competencia Comp = new Framework.objetosNeon.Competencia(12, 2012, 532);
            DateTime DI = new DateTime(2012,12,1);
            DateTime DF = new DateTime(2012,12,31);
            dllpublicaBalancete.InterBal Bal = new dllpublicaBalancete.InterBal(false,0,Comp, DI, DF);
            //MessageBox.Show(Bal.Publica());
            MessageBox.Show(Bal.TesteNovosDados("",0));
            Form1 F = new Form1();
            F.totaisBindingSource.DataSource = Bal.dPublicar;
            F.ShowDialog();*/
        }

        private void navBarItem2_LinkClicked(object sender, DevExpress.XtraNavBar.NavBarLinkEventArgs e)
        {
            Teste12m Teste = new Teste12m();
            Teste.Show();
        }

        private void navBarItem3_LinkClicked(object sender, DevExpress.XtraNavBar.NavBarLinkEventArgs e)
        {
            CorrecaoFolha CF = new CorrecaoFolha();
            CF.Show();
        }

        private void navBarItem4_LinkClicked(object sender, DevExpress.XtraNavBar.NavBarLinkEventArgs e)
        {
            Internet.NeonNet N = new Internet.NeonNet(false, 4);
            //MessageBox.Show(N.PublicarAcordos(200));
            MessageBox.Show(N.PublicaBlocos());
            MessageBox.Show(N.PublicaApartamentos(4));
            MessageBox.Show(N.PublicaCondominios());
        }

        private void navBarItem5_LinkClicked(object sender, DevExpress.XtraNavBar.NavBarLinkEventArgs e)
        {
            Transf.TransfF TransfF1 = new Transf.TransfF(774);            
            TransfF1 = new Transf.TransfF(Transf.TransfF.TpChave.CON, 774);
            if (TransfF1.Abrir() == System.Windows.Forms.DialogResult.OK)
            {
                int TRF = TransfF1.rowTRF.TRF;
                MessageBox.Show(string.Format("Criado: {0}", TRF));
                Transf.TransfF TransfF2 = new Transf.TransfF(TRF);
                if (TransfF2.Abrir() == System.Windows.Forms.DialogResult.OK)
                {
                    TransfF2 = new Transf.TransfF(TRF);
                    TransfF2.Abrir();
                }
            }            
        }

        private void navBarItem6_LinkClicked(object sender, DevExpress.XtraNavBar.NavBarLinkEventArgs e)
        {

        }

        private void navBarItem7_LinkClicked(object sender, DevExpress.XtraNavBar.NavBarLinkEventArgs e)
        {
            //Balancete.GrBalancete.balancete balancete1 = new Balancete.GrBalancete.balancete(10846);
            //if (balancete1.encontrado)
            //{
            //    balancete1.Abrir(false, CompontesBasicos.EstadosDosComponentes.JanelasAtivas);
            //}
        }

        private void navBarItem8_LinkClicked(object sender, DevExpress.XtraNavBar.NavBarLinkEventArgs e)
        {
            string[] arquivos = System.IO.Directory.GetFiles(@"C:\Lixo\DIRF SA\");
            foreach (string arquivo in arquivos)
            {
                string[] linhas = System.IO.File.ReadAllLines(arquivo, Encoding.GetEncoding(1252));
                if (linhas[0].Contains("|1A4MA1R|"))
                {
                    linhas[0] = linhas[0].Replace("|1A4MA1R|", "|7C2DE7J|");
                    for (int i = 0; i < linhas.Length;i++ )
                    {
                        if (linhas[i].Contains("DECPJ|"))
                            linhas[i] = linhas[i].Replace("|N|N|N|N|N|N|", "|N|N|N|N|N|N|N|");

                    }
                    string arqnovo = string.Format(@"C:\Lixo\dirfsa2013\{0}",System.IO.Path.GetFileName(arquivo));
                    System.IO.File.WriteAllLines(arqnovo,linhas, Encoding.GetEncoding(1252));
                }
            }

            MessageBox.Show("ok");
            return;
            /*
            string busca = 
"SELECT        [Boleto - Pagamentos].[N�mero Boleto] as BOL\r\n" + 
"FROM            ([Boleto - Pagamentos] INNER JOIN\r\n" + 
"                         Boleto ON [Boleto - Pagamentos].[N�mero Boleto] = Boleto.[N�mero Boleto])\r\n" + 
"WHERE        ([Boleto - Pagamentos].[Tipo Pagamento] = '910001') AND (Boleto.[Data Vencimento] <= #1/31/2013#) AND (Boleto.[Data Pagamento] IS NULL)\r\n";
            string buscaBOL = "select bol from boletos where bol = @P1";
            string exporta = "update boletos set bolexportar = 1 where bol = @P1";
            string apaga1 = "DELETE FROM [Boleto - Pagamentos] WHERE ([N�mero Boleto] = @P1)";
            string apaga2 = "DELETE FROM Boleto WHERE (Boleto.[N�mero Boleto] = @P1)";

            System.Data.DataTable DT = VirOleDb.TableAdapter.STTableAdapter.BuscaSQLTabela(busca);
            foreach (DataRow DR in DT.Rows)
            {
                if ((double)DR["BOL"] == 21402435)
                    MessageBox.Show("Aten��o");
                if (TableAdapter.ST().EstaCadastrado(buscaBOL, DR["BOL"]))
                {
                    Boletos.Boleto.Boleto boleto = new Boletos.Boleto.Boleto((int)((double)DR["BOL"]));
                    if (boleto.Encontrado)
                    {
                        if (boleto.rowPrincipal.BOLCancelado)
                            boleto.Cancelar();
                        else
                        {
                            VirOleDb.TableAdapter.STTableAdapter.ExecutarSQLNonQuery(apaga1, DR["BOL"]);
                            VirOleDb.TableAdapter.STTableAdapter.ExecutarSQLNonQuery(apaga2, DR["BOL"]);
                            //boleto.ExportaAcces();
                        }
                    }
                }
                else
                {
                    VirOleDb.TableAdapter.STTableAdapter.ExecutarSQLNonQuery(apaga1, DR["BOL"]);
                    VirOleDb.TableAdapter.STTableAdapter.ExecutarSQLNonQuery(apaga2, DR["BOL"]);
                }
            }*/
        }

        private void navBarItem9_LinkClicked(object sender, DevExpress.XtraNavBar.NavBarLinkEventArgs e)
        {
            
        }

        private void PictureEdit_F_EditValueChanged(object sender, EventArgs e)
        {

        }

        private void navBarItem10_LinkClicked(object sender, DevExpress.XtraNavBar.NavBarLinkEventArgs e)
        {
            EDIBoletosNeon.Arquivo Arquivo = new EDIBoletosNeon.Arquivo();
            int itotal = 0;
            int iatual = 0;

            //*** MRC - INICIO (21/03/2016 20:00) ***
            MessageBox.Show(Arquivo.Remessa(500, @"C:\Temp\Neon\ArquivosBradesco\Real\", ref itotal, ref iatual, 237));
            MessageBox.Show(Arquivo.Remessa(500, @"C:\Temp\Neon\ArquivosItau\Real\", ref itotal, ref iatual, 341));
            //*** MRC - TERMINO (21/03/2016 20:00) ***
        }

        private void navBarItem11_LinkClicked(object sender, DevExpress.XtraNavBar.NavBarLinkEventArgs e)
        {
            OpenFileDialog OF = new OpenFileDialog();
            if (OF.ShowDialog() == System.Windows.Forms.DialogResult.OK)
            {
                //dllSefip.fgts fgts = new dllSefip.fgts(OF.FileName);
            }
        }

        private void navBarItem12_LinkClicked(object sender, DevExpress.XtraNavBar.NavBarLinkEventArgs e)
        {
            EDIBoletosNeon.Arquivo Arquivo = new EDIBoletosNeon.Arquivo();
            int iTotal = 0;
            int iAtual = 0;
            MessageBox.Show(Arquivo.RemessaCheque(ref iTotal, ref iAtual, 500, @"C:\Temp\Neon\ArquivosBradesco\Real\"));
        }

        private void navBarItem13_LinkClicked(object sender, DevExpress.XtraNavBar.NavBarLinkEventArgs e)
        {
            Balancete.GrBalancete.dBalancete DBalancete1 = new Balancete.GrBalancete.dBalancete();

            Balancete.Relatorios.Credito.ImpCredColunas ImpCol = new Balancete.Relatorios.Credito.ImpCredColunas() { DataSource = DBalancete1 };
            ImpCol.CreateDocument();
            ImpCol.ShowPreviewDialog();
        }
    }
}

