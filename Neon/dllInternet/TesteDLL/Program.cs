using System;
using System.Collections.Generic;
using System.Windows.Forms;
using VirDB.Bancovirtual;
//using System.Data.SqlClient;

namespace TesteDLL
{
    static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main()
        {
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            CompontesBasicosProc.FuncoesBasicasProc.AmbienteDesenvolvimento = true;
            //CompontesBasicos.FormPrincipalBase.strEmProducao = true;
            //if (BancoVirtual.Strings == null)
            //{

                BancoVirtual.Popular("QAS - SBC CTI", @"Data Source=177.190.193.218\NEON;Initial Catalog=NeonH;Persist Security Info=True;User ID=NEONT;Connect Timeout=120;Password=TPC4P2DT");
                BancoVirtual.Popular("QAS - SA  CTI", @"Data Source=177.190.193.218\NEON;Initial Catalog=NeonSAH;Persist Security Info=True;User ID=NEONT;Connect Timeout=120;Password=TPC4P2DT");
                BancoVirtual.PopularFilial("QAS - SBC CTI", @"Data Source=177.190.193.218\NEON,49259;Initial Catalog=NeonH;Persist Security Info=True;User ID=NEONT;Connect Timeout=120;Password=TPC4P2DT");
                BancoVirtual.PopularFilial("QAS - SA  CTI", @"Data Source=177.190.193.218\NEON;Initial Catalog=NeonSAH;Persist Security Info=True;User ID=NEONT;Connect Timeout=120;Password=TPC4P2DT");
            BancoVirtual.PopularFilial("NEON SA  (cuidado)", @"Data Source=177.190.193.218\NEON,49259;Initial Catalog=NEONSA;User ID=NEONP;Password=TPC4P2DP;Connect Timeout=30");
            BancoVirtual.Popular("NEON SBC (cuidado)", @"Data Source=177.190.193.218\NEON,49259;Initial Catalog=NEON;User ID=NEONP;Password=TPC4P2DP;Connect Timeout=30");
                BancoVirtual.Popular("NEON SA  (cuidado)", @"Data Source=177.190.193.218\NEON,49259;Initial Catalog=NEONSA;User ID=NEONP;Password=TPC4P2DP;Connect Timeout=30");
                //BancoVirtual.PopularFilial("NEON SBC (cuidado)", @"Data Source=177.190.193.218\NEON;Initial Catalog=NEON;User ID=NEONP;Password=TPC4P2DP;Connect Timeout=30");
                BancoVirtual.PopularFilial("NEON SA (cuidado)", @"Data Source=177.190.193.218\NEON;Initial Catalog=NEONSA;User ID=NEONP;Password=TPC4P2DP;Connect Timeout=30");

                BancoVirtual.PopularInternet("INTERNET Local 1", @"Data Source=Lapvirtual2017\sqlexpress;Initial Catalog=Neonimoveis_1;User ID=sa;Password=venus", 1);
                BancoVirtual.PopularInternet("INTERNET Local 2", @"Data Source=Lapvirtual2017\sqlexpress;Initial Catalog=Neonimoveis_2;User ID=sa;Password=venus", 2);
            BancoVirtual.PopularInternet("INTERNET REAL1", @"Data Source=177.190.193.218\NEON,49259;Initial Catalog=neonimoveis_1;Persist Security Info=True;User ID=NEONP;Password=TPC4P2DP", 1);
            BancoVirtual.PopularInternet("INTERNET REAL2", @"Data Source=177.190.193.218\NEON,49259;Initial Catalog=neonimoveis_2;Persist Security Info=True;User ID=NEONP;Password=TPC4P2DP", 2);

            BancoVirtual.PopularAcces("SBC LOCAL", @"Provider=Microsoft.Jet.OLEDB.4.0;Data Source=C:\clientes\neon\sbc\dados.mdb;Persist Security Info=True;Password=96333018;User ID=NEON;Jet OLEDB:System database=C:\clientes\neon\sbc\neon.mda;Jet OLEDB:Database Password=96333018");
                BancoVirtual.PopularAcces("SA LOCAL", @"Provider=Microsoft.Jet.OLEDB.4.0;Data Source=c:\Clientes\neon\SA\dados.MDB;Persist Security Info=True;User ID=CPD;Password=7778;Jet OLEDB:System database=c:\Clientes\neon\SA\neon.MDA;Jet OLEDB:Database Password=7778");                
                BancoVirtual.PopularAcces("NEON Access (CUIDADO G)", @"Provider=Microsoft.Jet.OLEDB.4.0;Data Source=G:\dados.mdb;Persist Security Info=True;Password=96333018;User ID=NEON;Jet OLEDB:System database=G:\neon.mda;Jet OLEDB:Database Password=96333018");
                BancoVirtual.PopularAccesH("SBC LOCAL", @"Provider=Microsoft.Jet.OLEDB.4.0;Data Source=C:\clientes\neon\sbc\hist.mdb;Persist Security Info=True;Password=96333018;User ID=NEON;Jet OLEDB:System database=C:\clientes\neon\sbc\neon.mda;Jet OLEDB:Database Password=96333018");
                //BancoVirtual.PopularAccesH("SBC (CUIDADO) G", @"Provider=Microsoft.Jet.OLEDB.4.0;Data Source=G:\hist.mdb;Persist Security Info=True;Password=96333018;User ID=NEON;Jet OLEDB:System database=G:\neon.mda;Jet OLEDB:Database Password=96333018");

                BancoVirtual.Popular("MRC - NEON SBC Local", @"Data Source=MARCIA-NOTE\SQL2012;Initial Catalog=Neon;Persist Security Info=True;User ID=sa;Connect Timeout=30;Password=130306");
                BancoVirtual.PopularFilial("MRC - NEON SA Local", @"Data Source=MARCIA-NOTE\SQL2012;Initial Catalog=NeonSA;Persist Security Info=True;User ID=sa;Connect Timeout=30;Password=130306");
                BancoVirtual.PopularInternet("MRC - INTERNET Local 1", @"Data Source=MARCIA-NOTE\SQL2012;Initial Catalog=Neonimoveis_1;User ID=sa;Password=130306", 1);
                BancoVirtual.PopularInternet("MRC - INTERNET Local 2", @"Data Source=MARCIA-NOTE\SQL2012;Initial Catalog=Neonimoveis_2;User ID=sa;Password=130306", 2);
            //};


            //CompontesBasicos.FormPrincipalBase.strEmProducao = true;

            Login.fLogin _Form = new Login.fLogin();

            _Form.ShowDialog();
            //Teste componentes
            Application.Run(new Form2());
            //Application.Run(new FtesteSinc());
            //System.Data.SqlClient.SqlConnection
        }
    }
}