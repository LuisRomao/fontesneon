using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using LoginMenu.NeonOnLine;
using LoginMenu;

namespace Boletos
{
    public partial class _Default : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        private bool SimulaLogin(string Usuario, string Senha)
        {
            VirMSSQL.TableAdapter TA = new VirMSSQL.TableAdapter(VirDB.Bancovirtual.TiposDeBanco.SQL);
            TA.Cone.ConnectionString = System.Configuration.ConfigurationManager.ConnectionStrings["NeonConnectionString"].ConnectionString;
            for (int EMP = 1; EMP != 0; )
            {
                bool proprietario = true;
                int? APT = TA.BuscaEscalar_int("select APT from APARTAMENTOS where APTUsuarioInternetProprietario = @P1 and APTSenhaInternetProprietario = @P2", Usuario, Senha);
                if (!APT.HasValue)
                {
                    proprietario = false;
                    APT = TA.BuscaEscalar_int("select APT from APARTAMENTOS where APTUsuarioInternetInquilino = @P1 and APTSenhaInternetInquilino = @P2", Usuario, Senha);
                }
                if (APT.HasValue)
                {
                    DataRow DR = TA.BuscaSQLRow("SELECT CON,CONCodigo,CONNome,BLOCodigo,APTNumero FROM CONDOMINIOS INNER JOIN BLOCOS ON CON = BLO_CON INNER JOIN APARTAMENTOS ON BLO = APT_BLO WHERE APT = @P1", APT.Value);
                    dNeon2 dNeon = new dNeon2();
                    dNeon2.clientesRow clirow = dNeon.clientes.NewclientesRow();
                    clirow.Nome = "";
                    clirow.tipousuario = 1;
                    clirow.CODCON = (string)DR["CONCodigo"];
                    clirow.CONNome = (string)DR["CONNome"];
                    clirow.bloco = (string)DR["BLOCodigo"];                    
                    clirow.Apartamento = (string)DR["APTNumero"];
                    clirow.Proprietario = proprietario;
                    clirow.juridico = false;
                    clirow.Judicial = false;
                    clirow.APT = APT.Value;
                    Credenciais Cred = new Credenciais(TipoAut.MSSQL1, clirow, EMP, (int)DR["CON"]);
                    Session["Cred"] = Cred;
                    //Session["APT"] = APT.Value;
                    return true;
                };
                if (EMP == 1)
                {
                    EMP = 3;
                    TA.Cone.ConnectionString = TA.Cone.ConnectionString.Replace("Catalog=NEON", "Catalog=NEONSA");
                }
                else
                {
                    EMP = 0;
                }
            }
            return false;
        }


        protected void Button1_Click(object sender, EventArgs e)
        {
            Session["FRN"] = int.Parse(TextBoxFRN.Text);
            Session["EMP"] = int.Parse(TextBoxEMP.Text);
            Response.Redirect("NeonOnLine/locacao.aspx"); ;
        }

        protected void Button2_Click(object sender, EventArgs e)
        {
            if (SimulaLogin(TextBoxUSU.Text, TextBoxSenha.Text))
            {
                Credenciais Cred = (Credenciais)Session["Cred"];
                Cred.rowCli.tipousuario = (int)TipoUsuario.advogado;
                Session["FRNADV"] = 149;
                Response.Redirect("NeonOnLine/Boletos.aspx"); ;
            }                                                                                                        
        }

        protected void Button3_Click(object sender, EventArgs e)
        {
            if (SimulaLogin(TextBoxUSU.Text, TextBoxSenha.Text))
            {
                Credenciais Cred = (Credenciais)Session["Cred"];
                Session["Cred" + Cred.rowCli.APT.ToString()] = Cred;
                Response.Redirect(string.Format("NeonOnLine/Boletos.aspx?IDCred={0}", Cred.rowCli.APT));
            }            
        }

        protected void Button3_Click1(object sender, EventArgs e)
        {
            if (SimulaLogin(TextBoxUSU.Text, TextBoxSenha.Text))
                Response.Redirect("NeonOnLine/Boletos.aspx"); ;
        }

    }
}
