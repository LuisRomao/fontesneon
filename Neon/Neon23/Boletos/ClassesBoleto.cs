/*
MR - 25/02/2016 11:00 -           - Carteira com registro Ita� (Altera��es indicadas por *** MRC - INICIO - REMESSA (25/02/2016 11:00) 
                                    Ajustes de nomenclaturas e textos padr�es para boleto Ita� 
*/

namespace Boletos
{
    public class TVia
    {
        public string TextoInterno;
        public string CodigoBarras;
        public string NomeVia;
        public TVia(string _TextoInterno, string _CodigoBarras,string _NomeVia)
        {
            TextoInterno = _TextoInterno;
            CodigoBarras = _CodigoBarras;
            NomeVia = _NomeVia;
        }
    }

    public enum TipoMulta { diaria, unica }

    public class TDadosBanco
    {
        public TipoMulta TMulta
        {get{return rowBanco.CONTipoMulta.ToUpper() == "M" ? TipoMulta.unica : TipoMulta.diaria;}}
        public decimal Multa
        {get{return rowBanco.CONValorMulta;}}
        public string Numbanco 
        { get { return rowBanco.CON_BCO.ToString("000"); }}
        public int BCO
        { get { return rowBanco.CON_BCO; } }
        public string cc
        { get { return rowBanco.CONConta.PadLeft(6,'0'); } }
        public string dgcc
        { get { return rowBanco.CONDigitoConta; } }
        public string agencia
        { get { return rowBanco.CONAgencia; } }
        public string dgagencia
        { get { return rowBanco.CONDigitoAgencia; } }
        public string CNR
        { get { return rowBanco.IsCONCodigoCNRNull() ? "" : rowBanco.CONCodigoCNR.PadLeft(7, '0'); ; } }
        //*** MRC - INICIO - REMESSA (25/02/2016 11:00)
        public bool BoletoComRegistro
        { get { return rowBanco.CONBoletoComRegistro; } }
        //*** MRC - TERMINO - REMESSA (25/02/2016 11:00)
        public string Local
        { get { return local; } }
        public string EspecieDOC
        { get { return especieDOC; } }
        public string Carteira
        { get { return carteira; } }
        public string FonteNN
        { get { return fonteNN; } }

        //*** MRC - INICIO - REMESSA (25/02/2016 11:00) ***
        public int CarteiraBanco
        {
            get
            {
                if (CNR == "0000016")
                    return 16;
                switch (BCO)
                {
                    case 341:
                        return (ComRegistro ? 109 : 175);
                    default:
                        return (ComRegistro ? 9 : 6);
                }
            }
        }

        private bool ComRegistro
        {
            get
            {
                switch (BCO)
                {
                    case 237:
                        if (!(CNR == "" || CNR == "0000000" || CNR == "0000016"))
                            return BoletoComRegistro;
                        else
                            return false;
                    default:
                        return BoletoComRegistro;
                }
            }
        }
        //*** MRC - TERMINO - REMESSA (25/02/2016 11:00) ***

        public string Nome;
        public string Numero;        
        public string CodigoCedente;
        private string local;
        private string especieDOC;
        private string carteira;
        private string fonteNN;

        private objetosNeon.dBoletos.DadosBancariosRow rowBanco;

        public TDadosBanco(objetosNeon.dBoletos.DadosBancariosRow _rowBanco)
        {
            rowBanco = _rowBanco;
            switch (rowBanco.CON_BCO)
            {
                case 237:
                    Nome = "<b><font face=\"Arial\" size=\"2\">BRADESCO S/A</font><br></b>" +
                            "<font face=\"Arial, Helvetica, sans-serif\" size=\"1\">C.N.P.J 60.748.948</font>";
                    Numero = "237-2";
                    local = System.Web.HttpUtility.HtmlEncode("AT� O VENCIMENTO, EM	QUALQUER AG. DA REDE BANC�RIA");
                    CodigoCedente = string.Format("{0}-{1}/{2}-{3}",
                        rowBanco.CONAgencia,
                        rowBanco.CONDigitoAgencia,
                        rowBanco.CONConta,
                        rowBanco.CONDigitoConta);
                    especieDOC = "";
                    //*** MRC - INICIO - REMESSA (25/02/2016 11:00)
                    carteira = CarteiraBanco.ToString(); //(CNR == "" || CNR == "0000000") ? "6" : (CNR == "0000016" ? "16" : "9");
                    //*** MRC - TERMINO - REMESSA (25/02/2016 11:00)
                    fonteNN = "N1";
                    break;
                case 341:
                    Nome = "<b><font face=\"Arial\" size=\"2\">Banco Ita&uacute; S.A.</font></b>";
                    Numero = "341-7";
                    //*** MRC - INICIO - REMESSA (25/02/2016 11:00)
                    local = "At� o vencimento pague preferencialmente no Ita�, ap�s o vencimento pague somente no Ita�";
                    //*** MRC - TERMINO - REMESSA (25/02/2016 11:00)
                    CodigoCedente = string.Format("{0}-{1}/{2}-{3}",
                        rowBanco.CONAgencia,
                        rowBanco.CONDigitoAgencia,
                        rowBanco.CONConta,
                        rowBanco.CONDigitoConta);
                    especieDOC = "";
                    //*** MRC - INICIO - REMESSA (25/02/2016 11:00)
                    carteira = CarteiraBanco.ToString(); //"6";
                    //*** MRC - TERMINO - REMESSA (25/02/2016 11:00)
                    fonteNN = "N1";
                    break;
                case 409:
                    Nome = "<b><font face=\"Arial\" size=\"2\">UNIBANCO</font></b>";
                    Numero = "409-0";
                    local = "At� o vencimento, pag�vel em qualquer banco. Ap�s o vencimento, em qualquer ag�ncia do Unibanco mediante consulta no sistema VC";
                    CodigoCedente = string.Format("{0}-{1}/{2}-{3}",
                        rowBanco.CONAgencia,
                        rowBanco.CONDigitoAgencia,
                        rowBanco.CONConta,
                        rowBanco.CONDigitoConta);
                    especieDOC = "REC";
                    //*** MRC - INICIO - REMESSA (25/02/2016 11:00)
                    carteira = CarteiraBanco.ToString(); //"6";
                    //*** MRC - TERMINO - REMESSA (25/02/2016 11:00)
                    fonteNN = "N1";
                    break;
                case 104://124
                    Nome = "<img src=\"../util/img/caixa.gif\" width=\"100\" height=\"27\" alt = \"CAIXA\">";
                    Numero = "104-0";
                    local = "PREFERENCIALMENTE NAS CASAS LOT�RICAS E AG�NCIAS DA CAIXA";
                    CodigoCedente = string.Format("{0}/{1}-{2}",
                        rowBanco.CONAgencia,
                        rowBanco.CONCodigoCNR,
                        BoletosCalc.BoletoCalc.Modulo11_2_9(rowBanco.CONCodigoCNR, true));
                    especieDOC = "RC";
                    //*** MRC - INICIO - REMESSA (25/02/2016 11:00)
                    carteira = (CarteiraBanco == 6 ? "SR" : CarteiraBanco.ToString()); //"SR";
                    //*** MRC - TERMINO - REMESSA (25/02/2016 11:00)
                    fonteNN = "N0";
                    break;
            }
        }

    }
}