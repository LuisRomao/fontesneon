﻿/*
MR - 25/02/2016 11:00           - Inclusão do campo de boleto com registro (CONBoletoComRegistro) no DadosBancariosTableAdapter
*/

namespace Boletos.objetosNeon
{


    partial class dBoletos
    {
        public int EMP = -1;

        #region TableAdapters
        private dBoletosTableAdapters.INPCTableAdapter iNPCTableAdapter;

        /// <summary>
        /// TableAdapter padrão para tabela: INPC
        /// </summary>
        public dBoletosTableAdapters.INPCTableAdapter INPCTableAdapter
        {
            get
            {
                if (iNPCTableAdapter == null)
                {
                    iNPCTableAdapter = new dBoletosTableAdapters.INPCTableAdapter();
                    if (EMP != 1)
                        iNPCTableAdapter.Connection.ConnectionString = iNPCTableAdapter.Connection.ConnectionString.Replace("Catalog=NEON", "Catalog=NEONSA");
                };
                return iNPCTableAdapter;
            }
        }

        private dBoletosTableAdapters.AParTamentoTableAdapter aParTamentoTableAdapter;

        /// <summary>
        /// TableAdapter padrão para tabela: AParTamento
        /// </summary>
        public dBoletosTableAdapters.AParTamentoTableAdapter AParTamentoTableAdapter
        {
            get
            {
                if (aParTamentoTableAdapter == null)
                {
                    aParTamentoTableAdapter = new dBoletosTableAdapters.AParTamentoTableAdapter();
                    if (EMP != 1)
                        aParTamentoTableAdapter.Connection.ConnectionString = aParTamentoTableAdapter.Connection.ConnectionString.Replace("Catalog=NEON", "Catalog=NEONSA");
                };
                return aParTamentoTableAdapter;
            }
        }

        private dBoletosTableAdapters.ACOrdosTableAdapter aCOrdosTableAdapter;

        /// <summary>
        /// TableAdapter padrão para tabela: ACOrdos
        /// </summary>
        public dBoletosTableAdapters.ACOrdosTableAdapter ACOrdosTableAdapter
        {
            get
            {
                if (aCOrdosTableAdapter == null)
                {
                    aCOrdosTableAdapter = new dBoletosTableAdapters.ACOrdosTableAdapter();
                    if (EMP != 1)
                        aCOrdosTableAdapter.Connection.ConnectionString = aCOrdosTableAdapter.Connection.ConnectionString.Replace("Catalog=NEON", "Catalog=NEONSA");
                };
                return aCOrdosTableAdapter;
            }
        }

        private dBoletosTableAdapters.OriginalTableAdapter originalTableAdapter;

        /// <summary>
        /// TableAdapter padrão para tabela: Original
        /// </summary>
        public dBoletosTableAdapters.OriginalTableAdapter OriginalTableAdapter
        {
            get
            {
                if (originalTableAdapter == null)
                {
                    originalTableAdapter = new dBoletosTableAdapters.OriginalTableAdapter();
                    if (EMP != 1)
                        originalTableAdapter.Connection.ConnectionString = originalTableAdapter.Connection.ConnectionString.Replace("Catalog=NEON", "Catalog=NEONSA");
                };
                return originalTableAdapter;
            }
        }

        private dBoletosTableAdapters.NovosTableAdapter novosTableAdapter;

        /// <summary>
        /// TableAdapter padrão para tabela: Novos
        /// </summary>
        public dBoletosTableAdapters.NovosTableAdapter NovosTableAdapter
        {
            get
            {
                if (novosTableAdapter == null)
                {
                    novosTableAdapter = new dBoletosTableAdapters.NovosTableAdapter();
                    if (EMP != 1)
                        novosTableAdapter.Connection.ConnectionString = novosTableAdapter.Connection.ConnectionString.Replace("Catalog=NEON", "Catalog=NEONSA");
                };
                return novosTableAdapter;
            }
        }

        private dBoletosTableAdapters.CONDOMINIOSTableAdapter cONDOMINIOSTableAdapter;

        /// <summary>
        /// TableAdapter padrão para tabela: CONDOMINIOS
        /// </summary>
        public dBoletosTableAdapters.CONDOMINIOSTableAdapter CONDOMINIOSTableAdapter
        {
            get
            {
                if (cONDOMINIOSTableAdapter == null)
                {
                    cONDOMINIOSTableAdapter = new dBoletosTableAdapters.CONDOMINIOSTableAdapter();
                    if (EMP != 1)
                        cONDOMINIOSTableAdapter.Connection.ConnectionString = cONDOMINIOSTableAdapter.Connection.ConnectionString.Replace("Catalog=NEON", "Catalog=NEONSA");
                };
                return cONDOMINIOSTableAdapter;
            }
        }

        private dBoletosTableAdapters.BoletosTableAdapter boletosTableAdapter;

        /// <summary>
        /// TableAdapter padrão para tabela: Boletos
        /// </summary>
        public dBoletosTableAdapters.BoletosTableAdapter BoletosTableAdapter
        {
            get
            {
                if (boletosTableAdapter == null)
                {
                    boletosTableAdapter = new dBoletosTableAdapters.BoletosTableAdapter();
                    if (EMP != 1)
                        boletosTableAdapter.Connection.ConnectionString = boletosTableAdapter.Connection.ConnectionString.Replace("Catalog=NEON", "Catalog=NEONSA");
                };
                return boletosTableAdapter;
            }
        }

        private dBoletosTableAdapters.QueriesTableAdapter queriesTableAdapter;
        /// <summary>
        /// TableAdapter padrão para tabela: Queries
        /// </summary>
        public dBoletosTableAdapters.QueriesTableAdapter QueriesTableAdapter
        {
            get
            {
                if (queriesTableAdapter == null)
                {
                    queriesTableAdapter = new dBoletosTableAdapters.QueriesTableAdapter();
                };
                return queriesTableAdapter;
            }
        }

        private dBoletosTableAdapters.DadosBancariosTableAdapter dadosBancariosTableAdapter;

        /// <summary>
        /// TableAdapter padrão para tabela: DadosBancarios
        /// </summary>
        public dBoletosTableAdapters.DadosBancariosTableAdapter DadosBancariosTableAdapter
        {
            get
            {
                if (dadosBancariosTableAdapter == null)
                {
                    dadosBancariosTableAdapter = new dBoletosTableAdapters.DadosBancariosTableAdapter();
                    dadosBancariosTableAdapter.Connection.ConnectionString = dadosBancariosTableAdapter.Connection.ConnectionString.Replace("NEON1", "NEON" + EMP.ToString());
                };
                return dadosBancariosTableAdapter;
            }
        }

        #endregion

        internal void CorrorecaoProvisorios(string CODCON, string BLOCodigo, string APTNumero, short EMP, int APT, bool Proprietario)
        {
            dAcordoRet dAcordoRet1 = new dAcordoRet();
            dAcordoRet1.RetAcordoTableAdapter.FillByAPT(dAcordoRet1.RetAcordo, CODCON, APTNumero, BLOCodigo, EMP);
            dAcordoRet1.RetNovosBTableAdapter.FillByAPT(dAcordoRet1.RetNovosB, CODCON, APTNumero, BLOCodigo, EMP);
            dAcordoRet1.RetOriginaisTableAdapter.FillByAPT(dAcordoRet1.RetOriginais, CODCON, APTNumero, BLOCodigo, EMP);
            foreach (dAcordoRet.RetAcordoRow rowA in dAcordoRet1.RetAcordo)
            {
                dBoletos.ACOrdosRow rowNova = ACOrdos.NewACOrdosRow();
                rowNova.ACO_APT = APT;
                rowNova.ACOJuridico = rowA.Juridico;
                rowNova.ACOProprietario = rowA.DESTINATARIO == "P";
                ACOrdos.AddACOrdosRow(rowNova);
                int prest = 1;
                foreach (dAcordoRet.RetNovosBRow rowN in rowA.GetRetNovosBRows())
                {
                    incluirBolotoAProvisorio(rowN, Proprietario);
                    dBoletos.NovosRow rowNovo = Novos.NewNovosRow();
                    rowNovo.ACO = rowNova.ACO;
                    rowNovo.ACO_APT = APT;
                    rowNovo.dtvencto = rowN.vencto;
                    rowNovo.prest = prest++;
                    rowNovo.valprev = rowN.valor;
                    rowNovo.HValor = rowN.Honorarios;
                    Novos.AddNovosRow(rowNovo);
                }
                foreach (dAcordoRet.RetOriginaisRow rowO in rowA.GetRetOriginaisRows())
                {
                    dBoletos.BoletosRow rowBOL = Boletos.FindByNumero(rowO.numero);
                    if (rowBOL != null)
                    {
                        dBoletos.OriginalRow rowOrig = Original.NewOriginalRow();
                        rowOrig.ACO = rowNova.ACO;
                        rowOrig.ACO_APT = APT;
                        rowOrig.ACOStatus = 1;
                        rowOrig.competencia = string.Format("{0:00}/{1:0000}", rowBOL.BOLCompetenciaMes, rowBOL.BOLCompetenciaAno);
                        rowOrig.valor = rowBOL.valor;
                        rowOrig.valororiginal = rowBOL.valor;
                        rowOrig.vencto = rowBOL.dtvencto;
                        rowOrig.BOLCompetenciaMes = rowBOL.BOLCompetenciaMes;
                        rowOrig.BOLCompetenciaAno = rowBOL.BOLCompetenciaAno;
                        Original.AddOriginalRow(rowOrig);
                        rowBOL.Delete();
                    }

                }
            }

            Boletos.AcceptChanges();
        }

        private void incluirBolotoAProvisorio(dAcordoRet.RetNovosBRow rowNovos, bool Proprietario)
        {
            dBoletos.BoletosRow rowBolNovo = Boletos.NewBoletosRow();
            rowBolNovo.competencia = "0101";
            rowBolNovo.dtvencto = rowNovos.vencto;
            rowBolNovo.Multa = 2;
            rowBolNovo.Numero = rowNovos.nn;
            rowBolNovo.Resp = Proprietario ? "P" : "I";
            rowBolNovo.Tipo = "M";
            rowBolNovo.tipoboleto = "A";
            rowBolNovo.valor = rowNovos.valor;
            rowBolNovo.BOLCompetenciaMes = (short)rowNovos.vencto.Month;
            rowBolNovo.BOLCompetenciaAno = (short)rowNovos.vencto.Year;
            rowBolNovo.competencia = string.Format("{0:00}/{1:0000}", rowBolNovo.BOLCompetenciaMes, rowBolNovo.BOLCompetenciaAno);
            rowBolNovo.Resp = (rowBolNovo.IsBOLProprietarioNull() || rowBolNovo.BOLProprietario) ? "P" : "I";
            rowBolNovo.pago = !rowBolNovo.IsBOLPagamentoNull();
            rowBolNovo.BLOCodigo = "";
            Boletos.AddBoletosRow(rowBolNovo);
            rowBolNovo.AcceptChanges();

        }
    }
}
