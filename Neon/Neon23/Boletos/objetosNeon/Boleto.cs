/*
MR - 25/02/2016 11:00 -           - Carteira com registro Ita� (Altera��es indicadas por *** MRC - INICIO - REMESSA (25/02/2016 11:00) 
*/

using System;
using BoletosCalc;

namespace Boletos.objetosNeon
{
    public class VirBoleto
    {
        public decimal valororiginal;        
        public decimal TxMulta;
        public string TipoMulta;
        public bool CONJurosPrimeiroMes;
        public string ValorCorrigidostr { get { return ValorCorrigido == valororiginal ? "" : ValorCorrigido.ToString("n2"); } }
        public DateTime Vencto;
        public DateTime VenctoEfetivo;
        public DateTime data;
        //public Statusparcela stparcela;
        public decimal honor;
        public decimal totcomhonor;
        public int numero;
        public string competencia;
        public string Agencia { get { return DadosBanco.agencia; } }        
        public string DigitoAg;
        public int NossoNumero;
        public string CC { get { return DadosBanco.cc; } }
        public string dgCC { get { return DadosBanco.dgcc; } }
        public string CNR { get { return DadosBanco.CNR; } }
        //*** MRC - INICIO - REMESSA (25/02/2016 11:00)
        public bool BoletoComRegistro { get { return DadosBanco.BoletoComRegistro; } }
        //*** MRC - TERMINO - REMESSA (25/02/2016 11:00)      
        public string[] Campos;
        public string CodigoBarras;
        private string NossoNumerostrAntigo;

        //*** MRC - INICIO - REMESSA (25/02/2016 11:00) ***
        public int CarteiraBanco
        {
            get 
            {
                if (CNR == "0000016") 
                    return 16;
                switch (DadosBanco.BCO)
                {
                    case 341:
                        return (ComRegistro ? 109 : 175);
                    default:
                        return (ComRegistro ? 9 : 6);
                }
            }
        }

        private bool ComRegistro
        {
            get
            {
                switch (DadosBanco.BCO)
                {
                    case 237:
                        if (CNR != "" && CNR != "0000000" && CNR != "0000016")
                            return BoletoComRegistro;
                        return false;
                    default:
                        return BoletoComRegistro;
                }
            }
        }
        //*** MRC - TERMINO - REMESSA (25/02/2016 11:00) ***

        public string NossoNumerostr
        {
            get 
            {
                return boletoCalculado.NossoNumeroStr;
            }
        }

        public string Erro;
        public bool pago;
        public decimal ValorCorrigido;
        public decimal multa;
        public decimal juros;
        public decimal valorfinal;

        private dBoletos dBoletos1;
        //private dBoletos.indicesRow rowIndicesFinal;
        //private dBoletos.indicesRow rowIndicesInicial;
        private dBoletos.INPCRow rowIndicesFinal;
        private dBoletos.INPCRow rowIndicesInicial;
        private TDadosBanco DadosBanco;        
        private dBoletos.BoletosRow BoletosRow;
        //private dBoletos.BOLetosRow BOLetosRow;

        public Statusparcela stparcela
        {
            get 
            {
                if (pago)
                    return Statusparcela.Stparok;
                if (VenctoEfetivo >= data)
                    return Statusparcela.Stparaberto;
                if(VenctoEfetivo >= (data.AddDays(-3)))
                    return Statusparcela.Stparabertotolerancia;
                if(Vencto > (data.AddMonths(-1)))
                    return Statusparcela.Stparatrasadodentro;
                return Statusparcela.Stparatrasadofora;
            }
        }

        public VirBoleto(dBoletos _dBoletos1, bool _JurosCompostos)
        {
            jurosCompostos = _JurosCompostos;
            dBoletos1 = _dBoletos1;
        }

        public VirBoleto(dBoletos.BoletosRow _BoletosRow, TDadosBanco _DadosBanco)
        {
            dBoletos1 = (dBoletos)_BoletosRow.Table.DataSet;
            DadosBanco = _DadosBanco;
            CarregaDados(_BoletosRow);
            BoletosRow = _BoletosRow;
        }

        public void CarregaDados(dBoletos.BoletosRow rowBOL)
        {
            BoletosRow = rowBOL;
            valororiginal = rowBOL.valor;
            Vencto = rowBOL.dtvencto;
            VenctoEfetivo = Vencto.DayOfWeek == DayOfWeek.Saturday ? Vencto.AddDays(2) : Vencto.DayOfWeek == DayOfWeek.Sunday ? Vencto.AddDays(1) : Vencto;
            TipoMulta = rowBOL.Tipo;
            TxMulta = (decimal)rowBOL.Multa / 100;
            NossoNumero = (int)rowBOL.Numero;
            if (!rowBOL.IspagoNull()) pago = rowBOL.pago;
            if (dBoletos1.CONDOMINIOS.Count > 0)
               CONJurosPrimeiroMes = dBoletos1.CONDOMINIOS[0].CONJurosPrimeiroMes;
            else
               CONJurosPrimeiroMes = false;
        }

        /*
        public void CarregaDados(dBoletos.BOLetosRow rowBOL)
        {
            BOLetosRow = rowBOL;
            valororiginal = rowBOL.valor;
            Vencto = rowBOL.dtvencto;
            VenctoEfetivo = Vencto.DayOfWeek == DayOfWeek.Saturday ? Vencto.AddDays(2) : Vencto.DayOfWeek == DayOfWeek.Sunday ? Vencto.AddDays(1) : Vencto;
            TipoMulta = rowBOL.Tipo;
            TxMulta = (decimal)rowBOL.Multa / 100;
            NossoNumero = (int)rowBOL.Numero;
            //pago = !rowBOL.IsBOLpa
            if (dBoletos1.CONDOMINIOS.Count > 0)
                CONJurosPrimeiroMes = dBoletos1.CONDOMINIOS[0].CONJurosPrimeiroMes;
            else
                CONJurosPrimeiroMes = false;
        }*/


        private void Buscaindices()
        {
            if (dBoletos1.INPC.Count == 0)
                dBoletos1.INPCTableAdapter.Fill(dBoletos1.INPC);
            rowIndicesFinal = dBoletos1.INPC.FindByINPanoINPmes(data.Year, data.Month);
            rowIndicesInicial = dBoletos1.INPC.FindByINPanoINPmes(Vencto.Year, Vencto.Month);
            if (rowIndicesFinal == null)            
                rowIndicesFinal = dBoletos1.INPC[0];
            if (rowIndicesInicial == null)
                rowIndicesInicial = dBoletos1.INPC[0];                
        }

        public void Calcula()
        {
            Calcula(Vencto,false);
        }

        public bool jurosCompostos = true;

        public decimal Calcula(DateTime novadata,bool Parcelamento)
        {                                    
            data = novadata;
            int diascorridos = ((TimeSpan)(data - Vencto)).Days;
            decimal valorcorrigidomanobra;
            if (stparcela != Statusparcela.Stparatrasadofora)
                valorcorrigidomanobra = valororiginal;
            else
            {
                int diasinicio = Vencto.Day;
                int diasfinal = 30 - data.Day;
                Buscaindices();
                valorcorrigidomanobra = valororiginal * (decimal)((rowIndicesFinal.INPIndice / rowIndicesInicial.INPIndice) * rowIndicesFinal.INPInpc);
                //   Corre��o dos dias iniciais
                valorcorrigidomanobra = valorcorrigidomanobra / (decimal)Math.Pow(rowIndicesInicial.INPInpc, (diasinicio / 30D));
                //   Corre��o dos dias finais
                valorcorrigidomanobra = valorcorrigidomanobra / (decimal)Math.Pow(rowIndicesFinal.INPInpc, (diasfinal / 30D));
            };
            decimal multamanobra = 0;
            decimal jurosmanobra = 0;
            //No caso do site os boletos incluisos em parcelamento so incluidos com multa (sempre)
            bool CobrarEncargos;
            if (Parcelamento)
                CobrarEncargos = (stparcela != Statusparcela.Stparok);
            else
                CobrarEncargos = ((stparcela != Statusparcela.Stparaberto) && (stparcela != Statusparcela.Stparok));
            if(CobrarEncargos)                
                {
                    multamanobra = String.Compare(TipoMulta, "M", true) == 0 ? valorcorrigidomanobra * TxMulta : diascorridos > 30 ? valorcorrigidomanobra * 30 * TxMulta : valorcorrigidomanobra * diascorridos * TxMulta;

                    if (diascorridos > 30 || (CONJurosPrimeiroMes))
                    {
                        
                        if (jurosCompostos)
                            jurosmanobra = (valorcorrigidomanobra + multamanobra) * (decimal)(Math.Pow(1.01, (diascorridos / 30D)) - 1);                            
                        else
                            jurosmanobra = (valorcorrigidomanobra + multamanobra) * (decimal)((1.01 - 1) * (diascorridos / 30D));
                    }
                    else
                        jurosmanobra = 0;
                }


            multa = Math.Round(multamanobra, 2, MidpointRounding.AwayFromZero);
            juros = Math.Round(jurosmanobra, 2, MidpointRounding.AwayFromZero);


            valorfinal = Math.Round(valorcorrigidomanobra, 2, MidpointRounding.AwayFromZero) + multa + juros;
            ValorCorrigido = valorfinal - multa - juros;
            //if ((stparcela == Statusparcela.Stparatrasadodentro) || (stparcela == Statusparcela.Stparatrasadofora))
            // valorcorrigidostr=valorcorrigido.to
            //else
            // valorcorrigidostr:='';
            honor = diascorridos > 30 ? Math.Round((100 * valorfinal * 0.2M) / 100, 2, MidpointRounding.AwayFromZero) : 0;
            totcomhonor = valorfinal + honor;
            //if (BOLetosRow != null)
            //{
            //    BOLetosRow.ValorCorrigido = ValorCorrigido;
            //    BOLetosRow.ValorMulta = multa;
            //    BOLetosRow.ValorJuros = juros;
            //    BOLetosRow.ValorFinal = valorfinal;
            //};
            if (BoletosRow != null)
            {
                BoletosRow.ValorCorrigido = ValorCorrigido;
                BoletosRow.ValorMulta = multa;
                BoletosRow.ValorJuros = juros;
                BoletosRow.ValorFinal = valorfinal;
            }
            return valorfinal;
        }

        
        private string DigitoNossoNumero;

        private void CalculaNossoNumero()
        {
            switch (DadosBanco.BCO)
            {
                case 237:
                    //*** MRC - INICIO - REMESSA (25/02/2016 11:00)
                    //NossoNumerostrAntigo = ((CNR == "" || CNR == "0000000") ? "6" : (CNR == "0000016" ? "16" : "9")) + "00" + NossoNumero.ToString("000000000");
                    NossoNumerostrAntigo = CarteiraBanco.ToString() + "00" + NossoNumero.ToString("000000000");
                    //*** MRC - TERMINO - REMESSA (25/02/2016 11:00)
                    DigitoNossoNumero = BoletoCalc.DigitoBradesco(NossoNumerostrAntigo);
                    NossoNumerostrAntigo = String.Format("{0}-{1}", CNR != "0000016" ? NossoNumerostrAntigo.Insert(1, "/") : NossoNumerostrAntigo.Insert(2, "/"), BoletoCalc.DigitoBradesco(NossoNumerostrAntigo));
                    break;
                case 341:
                    //*** MRC - INICIO - REMESSA (25/02/2016 11:00)
                    //NossoNumerostrAntigo = String.Format("{0}{1}175{2:00000000}", Agencia, CC.Substring(1, 5), NossoNumero);                    
                    NossoNumerostrAntigo = String.Format("{0}{1}{2}{3:00000000}", Agencia, CC.Substring(1, 5), CarteiraBanco.ToString(), NossoNumero);
                    DigitoNossoNumero = BoletoCalc.Modulo10(NossoNumerostrAntigo);
                    //NossoNumerostrAntigo = String.Format("175-{0:00000000}-{1}", NossoNumero, DigitoNossoNumero);
                    NossoNumerostrAntigo = String.Format("{0:000}-{1:00000000}-{2}", CarteiraBanco.ToString(), NossoNumero, DigitoNossoNumero);
                    //*** MRC - TERMINO - REMESSA (25/02/2016 11:00)
                    break;
                case 409:
                    NossoNumerostrAntigo = NossoNumero.ToString("00000000000000");
                    DigitoNossoNumero = BoletoCalc.Modulo11_2_9(NossoNumerostrAntigo, true);
                    NossoNumerostrAntigo = String.Format("{0:00000000000000}/{1}", NossoNumero, DigitoNossoNumero);
                    break;
                case 399:
                default:
                    /*
                    NossoNumerostrAntigo = NossoNumero.ToString("0");                    
                    NossoNumerostrAntigo += (Modulo11_2_9HSBC(NossoNumerostrAntigo) + "4");
                    DateTime Vencimento = rowPrincipal.BOLVencto;
                    //Vencimento = new DateTime(2000, 7, 4);
                    Int64 manobravenc = Vencimento.Day * 10000 + Vencimento.Month * 100 + (Vencimento.Year % 100);
                    Int64 manobra = Int64.Parse(NossoNumerostrAntigo) + 3693872 + manobravenc;
                    NossoNumerostrAntigo += Modulo11_2_9HSBC(manobra.ToString());
                    //NossoNumerostrAntigo = "175-" + NN.ToString("00000000") + "-" + DigitoNossoNumero;
                    */
                    break;
            }
        }

        private void CalculaBarras(string numeroDg)
        {
            string[] a = new string[] { "00110",
                                        "10001",
                                        "01001",
                                        "11000",
                                        "00101",
                                        "10100",
                                        "01100",
                                        "00011",
                                        "10010",
                                        "01010" };
            //var Manobra  :String;
            //  Num1,Num2:String;
            //Saida    :String;
            //  Retorno  :StringBuilder;
            //b,c,m    :Int16;

            string Retorno = "";
            string Manobra = numeroDg.Trim();
            if ((Manobra.Length % 2) != 0)
                Manobra = "0" + Manobra;

            string Saida = "0000";
            int indice1;
            int indice2;
            char base0 = '0';// Convert.ToChar("0");
            for (int c = 1; c <= Manobra.Length / 2; c++)
            {
                indice1 = Manobra[2 * (c - 1) + 1 - 1] - base0;
                indice2 = Manobra[2 * (c - 1) + 2 - 1] - base0;
                string NUM1 = a[indice1];
                string NUM2 = a[indice2];
                //string NUM1 = a[Manobra[2 * (c - 1) + 1]];
                //string NUM2 = a[Manobra[2 * (c - 1) + 2]];
                for (int b = 0; b < 5; b++)
                    Saida += (NUM1.Substring(b, 1) + NUM2.Substring(b, 1));
            }
            Saida += "100";
            //binario = Saida;
            for (int M = 1; M <= Saida.Length; M++)
            {
                if ((M % 2) != 0)  //impar
                    Retorno += String.Compare(Saida[M - 1].ToString(), '1'.ToString(), false) == 0 ? "<img src=\"../util/img/p.gif\" width=3 height=47>" : "<img src=\"../util/img/p.gif\" width=1 height=47>";
                else
                    if (String.Compare(Saida[M - 1].ToString(), '1'.ToString(), false) == 0)
                        Retorno += "<img src=\"../util/img/b.gif\" width=3 height=47>";
                    else
                        Retorno += "<img src=\"../util/img/b.gif\" width=1 height=47>";
            }

            CodigoBarras = Retorno;
        }

        private BoletoCalc boletoCalculado;

        private BoletoCalc BoletoCalculado
        {
            get
            {
                if (boletoCalculado == null)
                {
                    //*** MRC - INICIO - REMESSA (25/02/2016 11:00) ***
                    //boletoCalculado = new BoletoCalc(DadosBanco.BCO, NossoNumero, DadosBanco.agencia, DadosBanco.BCO == 341 ? DadosBanco.cc.Substring(1, 5) : DadosBanco.cc, DadosBanco.dgcc, CNR, (CNR == "" || CNR == "0000000") ? 6 : (CNR == "0000016" ? 16 : 9));
                    boletoCalculado = new BoletoCalc(DadosBanco.BCO, NossoNumero, DadosBanco.agencia, DadosBanco.BCO == 341 ? DadosBanco.cc.Substring(1, 5) : DadosBanco.cc, DadosBanco.dgcc, CNR, CarteiraBanco);
                    //*** MRC - TERMINO - REMESSA (25/02/2016 11:00) ***
                }
                return boletoCalculado;
            }
        }

        public string LinhaDigitavel()
        {
            string retorno = BoletoCalculado.CalculaLinhaDigitavel(data, valorfinal);
            CalculaBarras(BoletoCalculado.codigoBarras);
            if (DadosBanco.BCO != 104)
            {
                if (retorno != LinhaDigitavelAntigoValidacao())
                    throw new Exception("Erro Validacao");
            }
            return retorno;
        }

        private string LinhaDigitavelAntigoValidacao()
        {
            Campos = new string[5];
            DateTime DataRef = new DateTime(1997, 10, 7);
            Int32 Fatorvenc = ((TimeSpan)(data - DataRef)).Days;
            CalculaNossoNumero();
            string numeroDg = "";
            switch (DadosBanco.BCO)
            {
                default:
                    break;
                case 237:
                    //*** MRC - INICIO - RESERVA (3.1) ***
                    numeroDg = string.Format("2379{0:0000}{1:0000000000}{2}{5}{3}0{4}0",
                                            Fatorvenc,
                                            100 * valorfinal,
                                            Agencia,
                                            CNR != "0000016" ? NossoNumerostrAntigo.Substring(2, 11) : NossoNumerostrAntigo.Substring(3, 11),
                                            CC.Substring(0, 6),
                                            //*** MRC - INICIO - REMESSA (25/02/2016 11:00)
                                            CarteiraBanco.ToString("00")); //(CNR == "" || CNR == "0000000") ? "06" : (CNR == "0000016" ? "16" : "09"));
                                            //*** MRC - TERMINO - REMESSA (25/02/2016 11:00)
                    //*** MRC - FIM - RESERVA (3.1) ***                    
                    numeroDg = numeroDg.Insert(4, BoletoCalc.Modulo11_2_9(numeroDg, false));
                    Campos[0] = String.Format("2379{0}{1}", Agencia, numeroDg.Substring(23, 1));
                    Campos[1] = numeroDg.Substring(24, 10);
                    Campos[2] = numeroDg.Substring(34, 10);
                    Campos[3] = numeroDg.Substring(4, 1);
                    Campos[4] = numeroDg.Substring(5, 14);
                    break;
                case 341:
                    numeroDg = string.Format("3419{0:0000}{1:0000000000}{2}{3:00000000}{4}{5}{6}{7}000",
                                                 Fatorvenc,
                                                 100 * valorfinal,
                                                 //*** MRC - INICIO - REMESSA (25/02/2016 11:00) ***
                                                 CarteiraBanco.ToString(), //175,
                                                 //*** MRC - TERMINO - REMESSA (25/02/2016 11:00) ***
                                                 NossoNumero,
                                                 DigitoNossoNumero,
                                                 Agencia,
                                                 CC.Substring(1, 5),
                                                 dgCC);
                    numeroDg = numeroDg.Insert(4, BoletoCalc.Modulo11_2_9(numeroDg, false));
                    //*** MRC - INICIO - REMESSA (25/02/2016 11:00) ***
                    Campos[0] = "3419" + CarteiraBanco.ToString("000") + numeroDg.Substring(22, 2); //"3419175" + numeroDg.Substring(22, 2);
                    //*** MRC - TERMINO - REMESSA (25/02/2016 11:00) ***
                    Campos[1] = numeroDg.Substring(24, 6) + DigitoNossoNumero + Agencia.Substring(0, 3);
                    Campos[2] = String.Format("{0}{1}{2}000", Agencia.Substring(3, 1), CC.Substring(1, 5),dgCC);
                    Campos[3] = numeroDg.Substring(4, 1);
                    Campos[4] = numeroDg.Substring(5, 14);
                    break;
                case 409:                    
                    numeroDg = string.Format("4099{0:0000}{1:0000000000}5{2:0000000}000000{3:0000000000}{4:0}",
                                                 Fatorvenc,
                                                 100 * valorfinal,
                                                 CNR,
                                                 NossoNumero,
                                                 DigitoNossoNumero);
                    numeroDg = numeroDg.Insert(4, BoletoCalc.Modulo11_2_9(numeroDg, false));
                    Campos[0] = "40995" + CNR.Substring(0, 4);
                    Campos[1] = String.Format("{0}00{1}", CNR.Substring(4, 3), numeroDg.Substring(29, 5));
                    Campos[2] = numeroDg.Substring(34, 10);
                    Campos[3] = numeroDg.Substring(4, 1);
                    Campos[4] = numeroDg.Substring(5, 14);
                    break;
            }

            for (int i = 0; i <= 2; i++)
                Campos[i] += BoletoCalc.Modulo10(Campos[i]);

            string CodigoBarrasValidar = CodigoBarras;
            CalculaBarras(numeroDg);
            if (CodigoBarrasValidar != CodigoBarras)
                throw new Exception("VALIDACAO");
            return string.Format("{0}  {1}  {2}  {3}  {4}",
                                            Campos[0].Insert(5, "."),
                                            Campos[1].Insert(5, "."),
                                            Campos[2].Insert(5, "."),
                                            Campos[3].Substring(0, 1),
                                            Campos[4]);
        }
    }

    public enum Statusparcela 
    {
        Stparok, 
        Stparaberto, 
        Stparabertotolerancia, 
        Stparatrasadodentro, 
        Stparatrasadofora
    }




}


/*
 
 unit Boleto;

interface

uses
System.Data.OleDb,math,System.Text;

type
  
  VirBoleto = class
  private
    comando        :OLEDBCommand;
    parametro      :OleDbParameter;
    tabela         :OleDbDataReader;
    inpcmesfinal   :Double;
    inpcmesinicial :Double;
    indicefinal    :Double;
    indiceinicial  :Double;
    primeirachamada:boolean;
    binario :string;
    procedure buscaindices;
    procedure CalculaBarras(numero:String);
  public (ja copiada) 
    
    //Falha            :boolean;

    constructor Create;
    procedure Calcula(novadata:datetime);
    function saidastr(valor:currency):string;
    Function LinhaDigitavel:String;
  end;

implementation






constructor VirBoleto.Create;
begin
  inherited Create;
  primeirachamada:=true;
  Conexao:=nil;
  trans:=nil;
end;

function VirBoleto.saidastr(valor: currency): string;
begin
 if valor=0 then
  result:=''
 else
  result:=system.String.Format('{0:#,##0.00}',valor);
end;

end.
 
 */