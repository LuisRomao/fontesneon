﻿namespace Boletos.objetosNeon
{


    partial class dAcordoRet
    {
        private dAcordoRetTableAdapters.RetAcordoTableAdapter retAcordoTableAdapter;
        /// <summary>
        /// TableAdapter padrão para tabela: RetAcordo
        /// </summary>
        public dAcordoRetTableAdapters.RetAcordoTableAdapter RetAcordoTableAdapter
        {
            get
            {
                if (retAcordoTableAdapter == null)
                {
                    retAcordoTableAdapter = new dAcordoRetTableAdapters.RetAcordoTableAdapter();
                };
                return retAcordoTableAdapter;
            }
        }

        private dAcordoRetTableAdapters.RetNovosBTableAdapter retNovosBTableAdapter;
        /// <summary>
        /// TableAdapter padrão para tabela: RetNovosB
        /// </summary>
        public dAcordoRetTableAdapters.RetNovosBTableAdapter RetNovosBTableAdapter
        {
            get
            {
                if (retNovosBTableAdapter == null)
                {
                    retNovosBTableAdapter = new dAcordoRetTableAdapters.RetNovosBTableAdapter();
                };
                return retNovosBTableAdapter;
            }
        }

        private dAcordoRetTableAdapters.RetOriginaisTableAdapter retOriginaisTableAdapter;
        /// <summary>
        /// TableAdapter padrão para tabela: RetOriginais
        /// </summary>
        public dAcordoRetTableAdapters.RetOriginaisTableAdapter RetOriginaisTableAdapter
        {
            get
            {
                if (retOriginaisTableAdapter == null)
                {
                    retOriginaisTableAdapter = new dAcordoRetTableAdapters.RetOriginaisTableAdapter();
                };
                return retOriginaisTableAdapter;
            }
        }

        private dAcordoRetTableAdapters.BOLETOSTableAdapter bOLETOSTableAdapter;
        /// <summary>
        /// TableAdapter padrão para tabela: BOLETOS
        /// </summary>
        public dAcordoRetTableAdapters.BOLETOSTableAdapter BOLETOSTableAdapter
        {
            get
            {
                if (bOLETOSTableAdapter == null)
                {
                    bOLETOSTableAdapter = new dAcordoRetTableAdapters.BOLETOSTableAdapter();
                };
                return bOLETOSTableAdapter;
            }
        }
    }
}
