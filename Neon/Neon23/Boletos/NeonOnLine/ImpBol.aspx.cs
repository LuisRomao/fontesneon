/*
MR - 25/02/2016 11:00 -           - Ajustes de nomenclaturas e textos padr�es para boleto Ita� (Altera��es indicadas por *** MRC - INICIO - REMESSA (25/02/2016 11:00) 
*/

using System;
using System.Collections;

namespace Boletos
{
    public partial class ImpBol : System.Web.UI.Page
    {
        public TDadosBanco DadosBanco;
        public string LinhaDigitavel;
        //public int NossoNumero;
        //public string Valor;
        public string[] TextoInterno;
        public objetosNeon.VirBoleto CurBoleto;
        //public DateTime DtPag;
        //public string apartamento;
        //public string bloco;
        //public string nome;
        //public bool proprietario;
        //public string sacado;

        //*** MRC - INICIO - REMESSA (25/02/2016 11:00)
        public string NomenclaturaCedente = "Cedente";
        public string NomenclaturaSacado = "Sacado";
        public string CedenteNome = "";
        public string CedenteEndereco = "";
        //*** MRC - INICIO - REMESSA (25/02/2016 11:00)

        public LoginMenu.NeonOnLine.Credenciais Cred;
        private objetosNeon.dBoletos.BoletosRow rowBoleto;
        private objetosNeon.dBoletos dBoletos;

        public string Bloco
        {
            get
            {
                if (Cred.rowCli.bloco != "SB")
                    return String.Format("bloco:{0}&nbsp;", Cred.rowCli.bloco);
                else
                    return "";
            }
        }

        private void AjustaDadosBanco()
        {
            dBoletos.DadosBancariosTableAdapter.Fill(dBoletos.DadosBancarios, (Cred.EMP==2) ? 3 : Cred.EMP, Cred.CON);
            objetosNeon.dBoletos.DadosBancariosRow rowBanco = dBoletos.DadosBancarios[0];
            DadosBanco = new TDadosBanco(rowBanco);            
        }

        private void AjustaDadosBoleto()
        { 
            CurBoleto = new objetosNeon.VirBoleto(rowBoleto,DadosBanco);
            if (Session["Boletopara"] == null)
                CurBoleto.Calcula();
            else
                CurBoleto.Calcula((DateTime)Session["Boletopara"],false);
            LinhaDigitavel = CurBoleto.LinhaDigitavel();      
        }

        private void AjustaDadosInternos()
        {
            TextoInterno = new string[2];
            if (CurBoleto.data > CurBoleto.Vencto)
                TextoInterno[0] = string.Format("<div class=\"B1\">Data de vencimento original: {0:dd/MM/yyyy}<br/></div>", CurBoleto.Vencto);
            else
                TextoInterno[0] = "";
            if(rowBoleto.competencia != "0101")
                TextoInterno[0] += string.Format("<div class=\"B1\">{0}: {1}/{2}</div>",
                    Server.HtmlEncode("Compet�ncia"),
                    rowBoleto.competencia.Substring(0,2),
                    rowBoleto.competencia.Substring(2));                   
            TextoInterno[1] = "<div class=\"B1\">&nbsp;Instru&ccedil;&otilde;es:</div><div class=\"N1\">"; //<font size="1">-',
            if (CurBoleto.data == CurBoleto.Vencto)                       
                TextoInterno[1] += "- Banco n&atilde;o est&aacute; autorizado a receber ap&oacute;s 30 dias da data do vencimento.<br/>";
            else
                TextoInterno[1] += "- Banco n&atilde;o est&aacute; autorizado a receber ap&oacute;s o vencimento.<br/>";
            TextoInterno[1]     += "- N&atilde;o conceder abatimento.<br/>";
            TextoInterno[1]     += "- Quita&ccedil;&atilde;o v&aacute;lida apenas ap&oacute;s compensa&ccedil;&atilde;o do cheque.<br/>";
            if (CurBoleto.data == CurBoleto.Vencto)
            {
                if(DadosBanco.Multa > 0)
                    if (DadosBanco.TMulta == TipoMulta.unica)                    
                        TextoInterno[1] += string.Format("- Ap&oacute;s o vencimento cobrar multa de {0:n}%.<br/>", DadosBanco.Multa);
                    else                    
                        TextoInterno[1] += string.Format("- Ap&oacute;s o vencimento cobrar juros di&aacute;rios de {0}%.<br/>", DadosBanco.Multa);
            };
            TextoInterno[1] += "</div>";
        }

        //*** MRC - INICIO - REMESSA (25/02/2016 11:00)
        private void AjustaDadosCamposBoleto()
        {
            //Nomenclaturas
            if (DadosBanco.BCO == 341)
            {
                NomenclaturaCedente = "Benefici�rio";
                NomenclaturaSacado = "Pagador";
            }

            //Cedente/Benefici�rio com endere�o
            LoginMenu.NeonOnLine.dCondominios dCondominiosLogado = (LoginMenu.NeonOnLine.dCondominios)Session["dCondominios"];
            LoginMenu.NeonOnLine.dCondominios.CONDOMINIOSRow rowCON = null;
            if (dCondominiosLogado == null)
            {
                LoginMenu.NeonOnLine.dCondominios dCondominiosX = new LoginMenu.NeonOnLine.dCondominios();
                using (LoginMenu.NeonOnLine.dCondominiosTableAdapters.CONDOMINIOSTableAdapter Ta = new LoginMenu.NeonOnLine.dCondominiosTableAdapters.CONDOMINIOSTableAdapter())
                {
                    if (Ta.FillByCODCON(dCondominiosX.CONDOMINIOS, (Cred.EMP == 2 ? 3 : Cred.EMP), Cred.CON) > 0)
                        rowCON = dCondominiosX.CONDOMINIOS[0];
                }
            }
            else
                rowCON = dCondominiosLogado.CONDOMINIOS.FindByCON_EMPCON((Cred.EMP == 2 ? 3 : Cred.EMP), Cred.CON);
            CedenteNome = Cred.rowCli.CONNome + " - CNPJ: " + rowCON.CONCnpj;
            CedenteEndereco = rowCON.CONEndereco.Trim() + " - " + rowCON.CONBairro.Trim() + (!(rowCON.IsCONUfNull()) ? " " + rowCON.CONUf : "");
        }
        //*** MRC - TERMINO - REMESSA (25/02/2016 11:00)

        protected void Page_Load(object sender, EventArgs e)
        {
            Cred = (LoginMenu.NeonOnLine.Credenciais)Session["CredBOL"];
            rowBoleto = (objetosNeon.dBoletos.BoletosRow)Session["rowBOL"];
            dBoletos = (objetosNeon.dBoletos)rowBoleto.Table.DataSet;
            if (!IsPostBack)
            {
                if (Cred.TUsuario == LoginMenu.NeonOnLine.TipoUsuario.Desenvolvimento)
                {
                    //usuarios de teste
                };
                AjustaDadosBanco();
                AjustaDadosBoleto();
                AjustaDadosInternos();
                //*** MRC - INICIO - REMESSA (25/02/2016 11:00)
                AjustaDadosCamposBoleto();
                //*** MRC - TERMINO - REMESSA (25/02/2016 11:00)
                ArrayList Lista = new ArrayList();
                Lista.Add(new TVia(TextoInterno[0],"","Recibo do sacado"));
                Lista.Add(new TVia(TextoInterno[1],CurBoleto.CodigoBarras,Server.HtmlEncode("Ficha de Compensa��o")));
                Repeater1.DataSource=Lista;
                Repeater1.DataBind();
            }
        }
    }  
}


/*

procedure TImpBol.Page_Load(sender: System.Object; e: System.EventArgs);
var vez          :Int32;
    nfilial      :Int32;
    DtVento      :DateTime;
    competencia  :String;
    multa        :Double;
    erro         :string;

begin
  if not page.IsPostBack then
   begin
      
      
      CurBoleto:=VirBoleto.Create;
      CurBoleto.Agencia        :=DadosBanco.agencia;
      CurBoleto.CC             :=DadosBanco.cc;
      CurBoleto.CNR            :=DadosBanco.CNR;
      CurBoleto.NossoNumero    :=NossoNumero;

      

      DtVento:=DateTime(tabela['dtvencto']);
      if Session['Boletopara'] = nil then
       DtPag  :=DtVento
      else
       DtPag  :=DateTime(Session['Boletopara']);

     erro:='62 ';

      if DtPag > DtVento then
       CurBoleto.stparcela     :=Stparatrasadodentro
      else
       CurBoleto.stparcela     :=Stparaberto;
       erro:='621 ';
      CurBoleto.VALORORIGINAL  :=Decimal(Tabela['valor']);
      erro:='622 ';
      CurBoleto.Vencto         :=DateTime(tabela['dtvencto']);
      erro:='623 ';
      CurBoleto.Conexao        :=Conexao;
      erro:='624 ';
      CurBoleto.tipomulta      :=convert.ToChar(tabela['Tipo']);
      erro:='625 ';
      CurBoleto.NumBanco       :=DadosBanco.NumBanco;
      erro:='626 ';
      CurBoleto.TxMulta        :=Double(Tabela['multa']);
      erro:='627 ';
      multa                    :=Double(tabela['Multa']);
      erro:='628 ';
      //verificar multa X mora
      CurBoleto.TxMulta        :=multa/100;
      erro:='629 ';
      competencia              :=tabela['competencia'].ToString;
      erro:='6210 ';
      tabela.Close;
      erro:='6211 ';
      CurBoleto.Calcula(DateTime(DtPag));
      erro:='6212 ';
      Valor:=System.String.Format('{0:#,##0.00}&nbsp;&nbsp;&nbsp;&nbsp;',[CurBoleto.valorfinal]);
      erro:='6213 ';
      LinhaDigitavel:=CurBoleto.LinhaDigitavel;

      erro:='6214 ';

      erro:='63 ';

      if (Session['Tipo'].ToString ='10')
          or
         (Session['Tipo'].ToString ='11') then
       sacado:='SACADO SACADO SACADO SACADO'
      else
       begin
        comando.CommandText:='Select nome from clientes '+
                             'where codcon=@codcon and '+
                             'apartamento=@apartamento and '+
                             'bloco = @bloco and '+
                             'proprietario=@proprietario';
        comando.Parameters.Clear;
        Comando.Parameters.Add('codcon'      ,Session['codcon']);
        Comando.Parameters.Add('apartamento' ,apartamento);
        Comando.Parameters.Add('bloco'       ,bloco);
        Comando.Parameters.Add('proprietario',System.Object(proprietario));
        sacado:=comando.ExecuteScalar.ToString;
        Conexao.Close;
        erro:='64 ';
       end;
      erro:='65 ';
      
      erro:='66 ';
      Lista:=ArrayList.Create;
      Lista.add(Tvia.Create(TextoInterno[1],'','Recibo do sacado'));
      Lista.add(Tvia.Create(TextoInterno[2],CurBoleto.CodigoBarras,'Ficha de Compensa��o'));
      Repeater1.DataSource:=Lista;
      erro:='67 ';
      Repeater1.DataBind;
               erro:='68 ';


      except
      response.Write(CurBoleto.Erro);
      end
   end;
end;


*/