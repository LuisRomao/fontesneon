﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Boletos.NeonOnLine
{
    public partial class LocacaoBol : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if ((int)Session["EMP"] == 3)
                SqlNeon.ConnectionString = SqlNeon.ConnectionString.Replace("Catalog=NEON", "Catalog=NEONSA");            
            GridView1.DataBind();
        }
    }
}