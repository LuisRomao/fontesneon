using System;
using System.Collections;
using System.Data;
using System.Web.UI;
using System.Web.UI.WebControls;
using Boletos.objetosNeon;
using LoginMenu.NeonOnLine;
using System.Drawing;

namespace Boletos
{
    public partial class Boletos : Page
    {
        public VirBoleto CurBoleto;
        public bool BotaoVisivel;
        //public string ImagemBotao;
        public string ComandoBotao;
        //comandobol       :string;
        public decimal totalavencer;
        public decimal totaltolerancia;
        public decimal totalvencido;
        public decimal totalvencidocor;
        public decimal totalmulta;
        public decimal totaljuros;
        public decimal totalmultaag;
        //totaljurosag     :currency;
        public decimal totalvalorfinal;
        public decimal totalvalorfinalag;

        public decimal redir;
        public decimal redirag;
        public bool Permiteacordo
        {
            get
            {
                if (!permiteacordo)
                    return false;
                if (BoletosVencidosPagaveis > 1)
                    return true;
                if ((BoletosVencidosPagaveis == 1) && (rowCON.CON_BCO == 341) && (rowCON.CONBoletoComRegistro))
                    return true;
                else
                    return false;
            }
        }

        private bool permiteacordo;

        private dBoletos dBoletos1;
        private dBoletos.CONDOMINIOSRow rowCON;
        private DateTime datacal;
        private Credenciais Cred;
        private bool nojuridico;
        //private int numeroacordo;
        //private string chave;
        private int BoletosVencidosPagaveis;

        private void enviarEmail()
        {

            string titulo = Server.HtmlEncode("SOLICITA��O DE NADA CONSTA");
            /*
            string quadro =
                          "    <p>\r\n" +
                          "      <table bordercolor=\"#75aadb\" cellspacing=\"2\" cellpadding=\"2\"\r\n" +
                          "        border=\"4\" frame=\"border\">\r\n" +
                          "        <tbody>\r\n" +
                          "		  <tr>\r\n" +
                          "			<td>\r\n" +
                          Server.HtmlEncode(conteudo.Text).Replace("\r\n", "<br />") + "\r\n" +
                          "			</td>\r\n" +
                          "		  </tr>\r\n" +
                          "		</tbody>\r\n" +
                          "	  </table>\r\n" +
                          "	</p>\r\n";
            */

            System.Net.Mail.SmtpClient SmtpClient1 = new System.Net.Mail.SmtpClient();
            using (System.Net.Mail.MailMessage email = new System.Net.Mail.MailMessage())
            {
                string Bcc;
                if (System.Configuration.ConfigurationManager.AppSettings["CopiarEmail" + Cred.EMP.ToString()] != null)
                    Bcc = System.Configuration.ConfigurationManager.AppSettings["CopiarEmail" + Cred.EMP.ToString()];
                else
                    Bcc = "";
                if (Bcc != "")
                    email.Bcc.Add(Bcc);
                if (SmtpClient1.Host == "smtp.virweb.com.br")
                {
                    email.From = new System.Net.Mail.MailAddress("luis@virweb.com.br");
                    email.To.Add("luis@virweb.com.br");
                }
                else

                    if (Cred.EMP == 1)
                    {
                        email.From = new System.Net.Mail.MailAddress("neon@neonimoveis.com.br");
                    }
                    else
                    {

                        email.From = new System.Net.Mail.MailAddress("neonsa@neonimoveis.com.br");
                    };
                email.To.Add(System.Configuration.ConfigurationManager.AppSettings["EmailCircular" + Cred.EMP.ToString()]);
                email.Subject = "Solicita��o de Nada Consta";
                if (SmtpClient1.Host == "smtp.virweb.com.br")
                    email.Subject = "TESTE " + email.Subject;
                email.IsBodyHtml = true;
                email.Body =
        "<html>\r\n" +
        "  <head>\r\n" +
        "  </head>\r\n" +
        "  <body>\r\n" +
        "	<table style=\"WIDTH: 100%; HEIGHT: 126px\" cellspacing=\"0\" cellpadding=\"0\" border=\"0\">\r\n" +
        "	  <tbody>\r\n" +
        "		<tr>\r\n" +
        "		  <td width=\"6\"><img height=\"114\" hspace=\"0\"\r\n" +
        "			  src=\"http://www.neonimoveis.com.br/neononline/imagens/barraazul.gif\"\r\n" +
        "                                                                width=\"6\" border=\"0\">\r\n" +
        "		  </td>\r\n" +
        "		  <td width=\"232\"><img hspace=\"0\"\r\n" +
        "              src=\"http://www.neonimoveis.com.br/neononline/imagens/neon1c.gif\"\r\n" +
        "			  width=\"232\" border=\"0\">\r\n" +
        "		  </td>\r\n" +
        "		  <td><img height=\"114\" hspace=\"0\"\r\n" +
        "             src=\"http://www.neonimoveis.com.br/neononline/imagens/barraazul.gif\"\r\n" +
        "			  width=\"100%\" border=\"0\">\r\n" +
        "		  </td>\r\n" +
        "     <td width=\"232\"><img hspace=\"0\"\r\n" +
        "              src=\"http://www.neonimoveis.com.br/neononline/imagens/conceito.gif\"\r\n" +
        "              width=\"232\" border=\"0\">\r\n" +
        "     </td>\r\n" +
        "   </tr>\r\n" +
        "   </tbody>\r\n" +
        " </table>\r\n" +
        "    <p>Data:" + DateTime.Now.ToString() + "</p>\r\n" +
        "    <p>Condom�nio:" + Cred.rowCli.CODCON + "</p>" +
        "    <p>Bloco:" + Cred.rowCli.bloco + "</p>" +
        "    <p>Apartamento:" + Cred.rowCli.Apartamento + "</p>" +
        "    <p>Tipo:<B>" +
        titulo +
        "</b></p>\r\n" +        
        "  </body>\r\n" +
        "</html>";
                SmtpClient1.Send(email);
            };
        }



        public string SN(string entrada)
        {
            return Server.HtmlEncode((String.Compare(entrada, "", false) == 0) ? "N�o" : "Sim");
        }


        private void zerar()
        {
            totalavencer = 0;
            totaltolerancia = 0;
            totalvencido = 0;
            totalvencidocor = 0;
            totalmulta = 0;
            totaljuros = 0;
            totalmultaag = 0;
            BoletosVencidosPagaveis = 0;
        }

        private string strNadaConsta = "";
        private string retornostrNadaConsta = "";

        public bool OcultarBoletosJuridico = false;

        public string strAviso()
        {
            
            if (retornostrNadaConsta != "")
                return String.Format("\"{0}\"",retornostrNadaConsta);
            if ((Session["TipoAbertura"] == null) || (Session["TipoAbertura"].ToString() != "2"))
                return "\"\"";
            else
                if (Cred.rowCli.juridico)
                    strNadaConsta = "Unidade no departamento jur�dico.";
                else
                    if (Cred.rowCli.Acordo != 0)
                        strNadaConsta = "Unidade com parecelamento.";
            if (strNadaConsta == "")
            {
                strNadaConsta = "Sua solicita��o de nada consta foi enviada para Neon. O documento ser� enviado pelo malote";
                enviarEmail();
            };
            Session["TipoAbertura"] = null;
            retornostrNadaConsta = strNadaConsta;
            return String.Format("\"{0}\"", strNadaConsta);
        }

        private int APT;
        private dBoletos.AParTamentoRow rowAPT;

        private void Ajustavariaveis()
        {
            Cred = Credenciais.CredST(this, true);            
            if (Request.QueryString["IDCred"] != null)
                Cred = (Credenciais)Session["Cred" + Request.QueryString["IDCred"]];
            dBoletos1 = new dBoletos();
            dBoletos1.EMP = Cred.EMP;
            Session["dBoletos1"] = dBoletos1;                        
            APT = Cred.rowCli.APT;
            dBoletos1.ACOrdosTableAdapter.Fill(dBoletos1.ACOrdos, APT);
            dBoletos1.OriginalTableAdapter.Fill(dBoletos1.Original, APT);
            dBoletos1.NovosTableAdapter.Fill(dBoletos1.Novos, APT);
            dBoletos1.AParTamentoTableAdapter.Fill(dBoletos1.AParTamento, APT);
            rowAPT = dBoletos1.AParTamento[0];

            nojuridico = rowAPT.APTStatusCobranca == -1 ;

            //nojuridico = dBoletos1.;

            //chave = string.Format("{0}{1}{2}", Cred.rowCli.CODCON, Cred.rowCli.Apartamento, Cred.rowCli.bloco);
            Label1.Text = string.Format("BOLETOS - {0} - {3} {1} - {2}",
                Cred.rowCli.Nome,
                (rowAPT.BLOCodigo.ToUpper() == "SB") ? "" : string.Format("- {0}", rowAPT.BLOCodigo),
                rowAPT.APTNumero,
                Cred.rowCli.CONNome
                );
        }

        private void CorrorecaoProvisorios()
        {
            dAcordoRet dAcordoRet1 = new dAcordoRet();
            short EMPAntigo = Cred.EMP == 1 ? (short)1 : (short)2;
            if (dAcordoRet1.RetAcordoTableAdapter.FillByAPT(dAcordoRet1.RetAcordo, Cred.rowCli.CODCON, Cred.rowCli.Apartamento, Cred.rowCli.bloco, EMPAntigo) > 0)
                Button1.Visible = Button2.Visible = false;
            dAcordoRet1.RetNovosBTableAdapter.FillByAPT(dAcordoRet1.RetNovosB, Cred.rowCli.CODCON, Cred.rowCli.Apartamento, Cred.rowCli.bloco, EMPAntigo);
            dAcordoRet1.RetOriginaisTableAdapter.FillByAPT(dAcordoRet1.RetOriginais, Cred.rowCli.CODCON, Cred.rowCli.Apartamento, Cred.rowCli.bloco, EMPAntigo);
            foreach (dAcordoRet.RetAcordoRow rowA in dAcordoRet1.RetAcordo)
            {
                dBoletos.ACOrdosRow rowNova = dBoletos1.ACOrdos.NewACOrdosRow();
                rowNova.ACO_APT = APT;
                rowNova.ACOJuridico = rowA.Juridico;
                rowNova.ACOProprietario = rowA.DESTINATARIO == "P";
                dBoletos1.ACOrdos.AddACOrdosRow(rowNova);               
                int prest = 1;
                foreach (dAcordoRet.RetNovosBRow rowN in rowA.GetRetNovosBRows())
                {
                    incluirBolotoAProvisorio(rowN);
                    dBoletos.NovosRow rowNovo = dBoletos1.Novos.NewNovosRow();
                    rowNovo.ACO = rowNova.ACO;
                    rowNovo.ACO_APT = APT;
                    rowNovo.dtvencto = rowN.vencto;
                    rowNovo.prest = prest++;
                    rowNovo.valprev = rowN.valor;
                    rowNovo.HValor = rowN.Honorarios;
                    dBoletos1.Novos.AddNovosRow(rowNovo);
                }
                foreach (dAcordoRet.RetOriginaisRow rowO in rowA.GetRetOriginaisRows())
                {
                    dBoletos.BoletosRow rowBOL = dBoletos1.Boletos.FindByNumero(rowO.numero);
                    if (rowBOL != null)
                    {
                        dBoletos.OriginalRow rowOrig = dBoletos1.Original.NewOriginalRow();
                        rowOrig.ACO = rowNova.ACO;
                        rowOrig.ACO_APT = APT;
                        rowOrig.ACOStatus = 1;
                        rowOrig.competencia = string.Format("{0:00}{1:0000}", rowBOL.BOLCompetenciaMes,rowBOL.BOLCompetenciaAno);
                        rowOrig.valor = rowBOL.valor;
                        rowOrig.valororiginal = rowBOL.valor;
                        rowOrig.vencto = rowBOL.dtvencto;
                        rowOrig.BOLCompetenciaMes = rowBOL.BOLCompetenciaMes;
                        rowOrig.BOLCompetenciaAno = rowBOL.BOLCompetenciaAno;
                        rowOrig.BOLTipoCRAI = "";
                        dBoletos1.Original.AddOriginalRow(rowOrig);
                        rowBOL.Delete();
                    }
                    
                }
            }
            
            dBoletos1.Boletos.AcceptChanges();
        }

        private void incluirBolotoAProvisorio(dAcordoRet.RetNovosBRow rowNovos)
        {
            dBoletos.BoletosRow rowBolNovo = dBoletos1.Boletos.NewBoletosRow();
            rowBolNovo.competencia = "0101";
            rowBolNovo.dtvencto = rowNovos.vencto;
            rowBolNovo.Multa = 2;
            rowBolNovo.Numero = rowNovos.nn;
            rowBolNovo.Resp = Cred.rowCli.Proprietario ? "P" : "I";
            rowBolNovo.Tipo = "M";
            rowBolNovo.tipoboleto = "A";
            rowBolNovo.valor = rowNovos.valor;
            rowBolNovo.BOLCompetenciaMes = (short)rowNovos.vencto.Month;
            rowBolNovo.BOLCompetenciaAno = (short)rowNovos.vencto.Year;
            rowBolNovo.competencia = string.Format("{0:00}{1:0000}", rowBolNovo.BOLCompetenciaMes, rowBolNovo.BOLCompetenciaAno);
            rowBolNovo.Resp = (rowBolNovo.IsBOLProprietarioNull() || rowBolNovo.BOLProprietario) ? "P" : "I";
            rowBolNovo.pago = !rowBolNovo.IsBOLPagamentoNull();
            rowBolNovo.BLOCodigo = "";
            rowBolNovo.APTNumero = "";            
            rowBolNovo.BOLProtestado = false;
            dBoletos1.Boletos.AddBoletosRow(rowBolNovo);
            rowBolNovo.AcceptChanges();

        }

        protected void Page_Load(object sender, EventArgs e)
        {            
            if (Session["Cred"] == null)
                Response.Redirect("Login.aspx?Ex=1");
            if (!IsPostBack)
            {                
                zerar();
                Panel1.Style["POSITION"] = "absolute";
                Panel1.Style["TOP"] = "80px";
                Panel1.Style["LEFT"] = "40px";
                datacal = DateTime.Today;
                DataCalculo.Text = datacal.ToString("dd/MM/yyyy");
                Ajustavariaveis();                
                
                if (dBoletos1.CONDOMINIOSTableAdapter.Fill(dBoletos1.CONDOMINIOS, Cred.CON) == 0)
                    Response.Redirect("WVazia.aspx");
                rowCON = dBoletos1.CONDOMINIOS[0];

                CarregaBoletos(OcultarBoletosJuridico);

                CorrorecaoProvisorios();

                Labeljuridico.Visible = nojuridico;
                Labeljuridico1.Visible = nojuridico;
                try
                {
                    if ((Session["FRNADV"] == null) && (Cred.rowCli.CODCON.ToUpper() == "GRENOB") && (Cred.rowCli.Apartamento == "0061"))
                        OcultarBoletosJuridico = true;
                }
                catch 
                {
                    OcultarBoletosJuridico = false;
                };
                if (OcultarBoletosJuridico)
                {
                    Labeljuridico1.Text = "* Listados apenas os boletos em aberto ou vencidos a menos de 30 dias. Para lista completa contatar o jur�dico.";
                }
                permiteacordo = !nojuridico;
                if (!rowCON.IsCONPermiteAcordoInternetNull())
                    if (!rowCON.CONPermiteAcordoInternet)
                        permiteacordo = false;
                if (dBoletos1.ACOrdos.Count != 0)
                {
                    HyperLink1.Visible = true;
                    permiteacordo = false;
                    Labelandamento.Visible = true;
                    redir = 0;
                    redirag = 0;
                    //dBoletos1.DividasTableAdapter.Fill(dBoletos1.Dividas, numeroacordo);
                    //dBoletos1.OriginalTableAdapter.Fill(dBoletos1.Original, APT);
                    foreach (dBoletos.OriginalRow rowO in dBoletos1.Original)
                    {
                        rowO.competencia = string.Format("{0:00}/{1:0000}", rowO.BOLCompetenciaMes, rowO.BOLCompetenciaAno);
                        rowO.valor = rowO.valororiginal;
                    }
                    Repeater1.DataSource = dBoletos1.Original;
                    Repeater1.DataBind();
                    //dBoletos1.OriginalTableAdapter.Fill(dBoletos1.Original, APT);
                    //dBoletos1.NovosTableAdapter.Fill(dBoletos1.Novos, APT);
                    int ACOAnt = 0;
                    int prest = 0;
                    foreach (dBoletos.NovosRow rowD in dBoletos1.Novos)
                    {
                        if (ACOAnt != rowD.ACO)
                        {
                            ACOAnt = rowD.ACO;
                            prest = 1;
                        }
                        rowD.prest = prest++;
                    }
                    Repeater2.DataSource = dBoletos1.Novos;
                    Repeater2.DataBind();                            
                }
                else
                {
                    HyperLink1.Visible = false;
                    Labelandamento.Visible = false;
                };
                

                //if (!OcultarBoletosJuridico)
                //    dBoletos1.BoletosTableAdapter.Fill(dBoletos1.Boletos, DateTime.Today.AddMonths(-6), APT);
                //else
                //    dBoletos1.BoletosTableAdapter.FillByPagaveis(dBoletos1.Boletos,DateTime.Today.AddMonths(-6), APT, datacal.AddMonths(-1));
                CurBoleto = new VirBoleto(dBoletos1,true);
                if ((Cred.CON == 732) && (Cred.EMP == 1))
                    CurBoleto.jurosCompostos = false;
                Repeater3.DataSource = dBoletos1.Boletos;
                Repeater3.DataBind();
                //PanelJuridico.Visible = ((Cred.TUsuario == TipoUsuario.Desenvolvimento) || (Cred.TUsuario == TipoUsuario.advogado));
                PanelJuridico.Visible = (Session["FRNADV"] != null);
            }
            else
            {
                dBoletos1 = (dBoletos)Session["dBoletos1"];
                rowCON = dBoletos1.CONDOMINIOS[0];
            }            
        }

        private void CarregaBoletos(bool OcultarBoletosJuridico)
        {
            if (!OcultarBoletosJuridico)
                dBoletos1.BoletosTableAdapter.Fill(dBoletos1.Boletos, DateTime.Today.AddMonths(-6), APT);
            else
                dBoletos1.BoletosTableAdapter.FillByPagaveis(dBoletos1.Boletos, DateTime.Today.AddMonths(-6), APT, datacal.AddMonths(-1));
            foreach (dBoletos.BoletosRow BolAjustar in dBoletos1.Boletos)
            {
                BolAjustar.competencia = string.Format("{0:00}{1:0000}",BolAjustar.BOLCompetenciaMes,BolAjustar.BOLCompetenciaAno);
                BolAjustar.Resp = (BolAjustar.IsBOLProprietarioNull() || BolAjustar.BOLProprietario) ? "P" : "I";
                BolAjustar.pago = !BolAjustar.IsBOLPagamentoNull();
            }
        }

        public static string propinq(string entrada)
        {
            return String.Compare(entrada, "P", true) == 0 ? "Prop." : "Inq.";
        }

        private bool TravaItauRegistrado30(dBoletos.BoletosRow rowBOL)
        {
            return (rowCON.CON_BCO == 341) && (rowCON.CONBoletoComRegistro) && (rowBOL.dtvencto.AddMonths(1) < DateTime.Today);
        }

        public string calculos(object oDRV)
        {
            bool Protesto = false;
            DataRowView DRV = (DataRowView)oDRV;
            dBoletos.BoletosRow rowBOL = (dBoletos.BoletosRow)DRV.Row;
            if (rowBOL.BOLProtestado)
            {
                permiteacordo = false;
                Protesto = true;
                strNadaConsta = "Unidade com boleto protestado.";
            }
            if (string.Compare(rowBOL.tipoboleto, "A", true) == 0)
            {
                permiteacordo = false;
                strNadaConsta = "Unidade com parcelamento.";
            }
            CurBoleto.CarregaDados(rowBOL);            
            DateTime venctoEfetivo = rowBOL.dtvencto;
            while ((venctoEfetivo.DayOfWeek == DayOfWeek.Sunday) || (venctoEfetivo.DayOfWeek == DayOfWeek.Saturday))
                venctoEfetivo = venctoEfetivo.AddDays(1);
            CurBoleto.Calcula(datacal,false);

            if ((rowBOL.pago) || (Protesto))
            {
                BotaoVisivel = false;
            }
            else
            {
                if (nojuridico && (CurBoleto.stparcela == Statusparcela.Stparatrasadofora))
                    BotaoVisivel = false;
                else
                {
                    if (!TravaItauRegistrado30(rowBOL))
                    {
                        BotaoVisivel = true;
                        ComandoBotao = "IMP";
                        if (CurBoleto.stparcela != Statusparcela.Stparaberto)
                            BoletosVencidosPagaveis++;
                    }
                    else
                        BoletosVencidosPagaveis++;
                }
            }
            

            switch (CurBoleto.stparcela)
            {
                default:
                case Statusparcela.Stparok:
                    break;
                case Statusparcela.Stparaberto:
                    totalavencer += CurBoleto.valororiginal;
                    break;
                case Statusparcela.Stparabertotolerancia:
                    strNadaConsta = "Existem boletos em aberto.";
                    redirag = totalvalorfinal > 0 ? 0 : totalvalorfinalag == 0 ? (int)rowBOL.Numero : 0;
                    totaltolerancia += CurBoleto.valororiginal;
                    //totalmultaag += CurBoleto.multa;
                    totalmultaag += rowBOL.ValorMulta;
                    //totalvalorfinalag += CurBoleto.valorfinal;
                    totalvalorfinalag += rowBOL.ValorFinal;
                    break;
                case Statusparcela.Stparatrasadodentro:
                    strNadaConsta = "Existem boletos em aberto.";
                    redir = totalvalorfinal == 0 ? (int)rowBOL.Numero : 0;
                    totalvencido += CurBoleto.valororiginal;
                    //totalvencidocor += CurBoleto.ValorCorrigido;
                    totalvencidocor += rowBOL.ValorCorrigido;
                    //totalmulta += CurBoleto.multa;
                    totalmulta += rowBOL.ValorMulta;
                    //totalmulta += CurBoleto.multa;
                    //totalmulta += rowBOL.ValorMulta;
                    //totalvalorfinal += CurBoleto.valorfinal;
                    totalvalorfinal += rowBOL.ValorFinal;
                    break;
                case Statusparcela.Stparatrasadofora:
                    strNadaConsta = "Existem boletos em aberto.";
                    totalvencido += CurBoleto.valororiginal;
                    totalvencidocor += rowBOL.ValorCorrigido;
                    //totalmulta += CurBoleto.multa;
                    totalmulta += rowBOL.ValorMulta;
                    //totalmulta += CurBoleto.multa;
                    //totalmulta += rowBOL.ValorMulta;
                    //totalvalorfinal += CurBoleto.valorfinal;
                    totalvalorfinal += rowBOL.ValorFinal;
                    break;
            }
            
            return "";
        }
       
        public static string saidastr(decimal valor, string zero)
        {
            return (valor == 0) ? zero : valor.ToString("n2");            
        }
            
        public string StatusBoleto()
        {
            switch (CurBoleto.stparcela)
            {
                case Statusparcela.Stparok:
                    return "PAGO";
                case Statusparcela.Stparaberto:
                    return "A&nbsp;Vencer";
                case Statusparcela.Stparabertotolerancia:
                    return "Aguardo";
                case Statusparcela.Stparatrasadodentro:                    
                case Statusparcela.Stparatrasadofora:
                    return "Vencido";
                default:
                    return "";
            };                   
        }

        public Color CorStatus()
        {
            switch (CurBoleto.stparcela)
            {
                case Statusparcela.Stparok:
                case Statusparcela.Stparaberto:
                case Statusparcela.Stparabertotolerancia:
                    return Color.Black;
                case Statusparcela.Stparatrasadodentro:
                case Statusparcela.Stparatrasadofora:
                    return Color.Red;
                default:
                    return Color.Black;
            }
        }

        protected void Repeater3_ItemCommand(object source, RepeaterCommandEventArgs e)
        {
            if (e.CommandName == "ACORDO")
            {
                ArrayList Apagar = new ArrayList();
                foreach (dBoletos.BoletosRow row in dBoletos1.Boletos)
                    if (row.pago)
                        Apagar.Add(row);
                foreach (dBoletos.BoletosRow row in Apagar)
                    row.Delete();
                dBoletos1.Boletos.AcceptChanges();
                Session["AcordoFeito"] = null;
                if (Request.QueryString["IDCred"] == null)
                    Response.Redirect("Parcelar.aspx");
                else
                    Response.Redirect(string.Format("Parcelar.aspx?IDCred={0}", Request.QueryString["IDCred"]));
            }
            else if (e.CommandName == "IMP")
            {
                int BOL = int.Parse(e.CommandArgument.ToString());
                if (Request.QueryString["IDCred"] != null)
                    Session["CredBOL"] = Session["Cred" + Request.QueryString["IDCred"]];
                else
                    Session["CredBOL"] = Session["Cred"];

                dBoletos.BoletosRow rowBOL = dBoletos1.Boletos.FindByNumero(BOL);
                Session["rowBOL"] = rowBOL;
                if (rowBOL.dtvencto > DateTime.Today.AddDays(5))
                {
                    Session["Boletopara"] = null;
                    Response.Redirect("ImpBol.aspx");
                }
                else
                {
                    CurBoleto = new VirBoleto(dBoletos1,true);
                    Cred = Credenciais.CredST(this, true);
                    if ((Cred.CON == 732) && (Cred.EMP == 1))
                        CurBoleto.jurosCompostos = false;
                    CurBoleto.CarregaDados(rowBOL);
                    SortedList Alternativas = new SortedList();
                    decimal ValorAlter = 0;
                    for (DateTime dataAlter = DateTime.Today.AddDays(7); dataAlter >= DateTime.Today; dataAlter = dataAlter.AddDays(-1))
                    {
                        if ((dataAlter.DayOfWeek == DayOfWeek.Saturday) || (dataAlter.DayOfWeek == DayOfWeek.Sunday))
                            continue;
                        decimal NovoValorAlter = CurBoleto.Calcula(dataAlter,false);
                        if (NovoValorAlter != ValorAlter)
                        {
                            ValorAlter = NovoValorAlter;
                            Alternativas.Add(dataAlter, ValorAlter);
                        };
                    }
                    if (Alternativas.Count == 1)
                    {
                        Session["Boletopara"] = (DateTime)Alternativas.GetKey(0) ;
                        Response.Redirect("ImpBol.aspx");
                    }
                    else
                    {
                        Session["SelDatas"] = Alternativas;
                        Response.Redirect("SelDatas.aspx");
                    }
                }

            }           
        }

        protected void Button1_Click(object sender, EventArgs e)
        {
            if (Request.QueryString["IDCred"] != null)
                Cred = (Credenciais)Session["Cred" + Request.QueryString["IDCred"]];
            else
                Cred = Credenciais.CredST(this, true);            
            Session["CredAcordoAbrir"] = Cred;
            string URL = string.Format("Acordo.aspx");
            Response.Redirect(URL);
        }

        protected void Button2_Click(object sender, EventArgs e)
        {
            if (Request.QueryString["IDCred"] != null)
                Cred = (Credenciais)Session["Cred" + Request.QueryString["IDCred"]];
            else
                Cred = Credenciais.CredST(this, true);
            Session["CredAcordoAbrir"] = Cred;
            string URL = string.Format("NovoAcordo.aspx");
            //string URL = string.Format("BoletosAbertos.aspx?ID=0&CHAVE={0}{1}{2}&CHAVE1={0}&CHAVE2={1}&CHAVE3={2}&FILIAL={3}&FRN={4}",
            //                                    Cred.rowCli.CODCON,
            //                                    Cred.rowCli.Apartamento,
            //                                    Cred.rowCli.bloco,
            //                                    EMP,
            //                                    Session["FRNADV"]);
            Response.Redirect(URL);

        }        
    }
}










