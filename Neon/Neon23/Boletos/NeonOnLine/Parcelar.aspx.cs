using System;
using System.Data;
using System.Collections;
using System.Web.UI.WebControls;
using Boletos.objetosNeon;
using LoginMenu.NeonOnLine;

namespace Boletos.NeonOnLine
{
    public partial class Parcelar : System.Web.UI.Page
    {
        public static string saidastr(decimal valor, string zero)
        {
            return (valor == 0) ? zero : valor.ToString("n2");
        }

        public VirBoleto CurBoleto;
        public decimal totalvencido;
        public decimal totalvencidocor;
        public decimal totalmulta;
        public decimal totaljuros;
        public decimal totalvalorfinal;

        
        private int vezes;

        private dBoletos _dBoletos1;

        public dBoletos dBoletos1
        {
            get
            {
                return _dBoletos1 ?? (_dBoletos1 = (dBoletos)Session["dBoletos1"]);
            }            
        }

        private void CarregaDados()
        {
            //int teste = 0;
            //try
            //{
                //teste = 1;
                CurBoleto = new VirBoleto(dBoletos1, true);
                //teste = 2;
                Cred = Request.QueryString["IDCred"] != null ? (Credenciais)Session["Cred" + Request.QueryString["IDCred"]] : (Credenciais)Session["Cred"];
                if ((Cred.CON == 732) && (Cred.EMP == 1))
                {
                    //teste = 3;
                    CurBoleto.jurosCompostos = false;
                    //teste = 4;
                }
                //teste = 5;
                Repeater3.DataSource = dBoletos1.Boletos;
                //teste = 6;
                Repeater3.DataBind();
                //teste = 7;
            //}
            //catch 
            //{
            //    Label1.Text = string.Format("Erro {0}",teste);
            //}
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (Session["AcordoFeito"] != null)
            {
                if (Request.QueryString["IDCred"] == null)
                    Response.Redirect("boletos.aspx");
                else
                    Response.Redirect(string.Format("boletos.aspx?IDCred={0}", Request.QueryString["IDCred"]));
            }
            if (!IsPostBack)
            {
                CarregaDados();                
            }
        }

        public string Totalizar()
        {
            totalvencido = 0;
            totalvencidocor = 0;
            totalmulta = 0;
            totaljuros = 0;
            totalvalorfinal = 0;
            int nparcelas = 0;
            foreach (dBoletos.BoletosRow rowBoleto in dBoletos1.Boletos)
                if (rowBoleto.Parcelar)
                {
                    nparcelas++;
                    totalvencido += rowBoleto.valor;
                    totalvencidocor += rowBoleto.ValorCorrigido;
                    totalmulta += rowBoleto.ValorMulta;
                    totaljuros += rowBoleto.ValorJuros;
                    totalvalorfinal += rowBoleto.ValorFinal;
                };
            if (nparcelas == 0)
                vezes = 0;
            else
            {
                vezes = Math.Min(3, nparcelas);
                decimal ValParcel = totalvalorfinal / vezes;
                while ((ValParcel < 50) && (vezes > 1))
                    ValParcel = totalvalorfinal / --vezes;
                VALOR_1_1.Text = totalvalorfinal.ToString("n2");
                VALOR_2_1.Text = Math.Round(totalvalorfinal / 2, 2, MidpointRounding.AwayFromZero).ToString("n2");
                VALOR_2_2.Text = (totalvalorfinal - Math.Round(totalvalorfinal / 2, 2, MidpointRounding.AwayFromZero)).ToString("n2");
                VALOR_3_1.Text = VALOR_3_2.Text = Math.Round(totalvalorfinal / 3, 2, MidpointRounding.AwayFromZero).ToString("n2");
                VALOR_3_3.Text = (totalvalorfinal - 2 * Math.Round(totalvalorfinal / 3, 2, MidpointRounding.AwayFromZero)).ToString("n2");
                DateTime d1 = DateTime.Today.AddDays(7);
                if (d1.DayOfWeek == DayOfWeek.Saturday)
                    d1 = d1.AddDays(-1);
                if (d1.DayOfWeek == DayOfWeek.Sunday)
                    d1 = d1.AddDays(-2);
                DATA_1_1.Text = DATA_2_1.Text = DATA_3_1.Text = d1.ToString("dd/MM/yyyy");
                d1 = DateTime.Today.AddDays(7).AddMonths(1);
                if (d1.DayOfWeek == DayOfWeek.Saturday)
                    d1 = d1.AddDays(-1);
                if (d1.DayOfWeek == DayOfWeek.Sunday)
                    d1 = d1.AddDays(-2);
                DATA_2_2.Text = DATA_3_2.Text = d1.ToString("dd/MM/yyyy");
                d1 = DateTime.Today.AddDays(7).AddMonths(2);
                if (d1.DayOfWeek == DayOfWeek.Saturday)
                    d1 = d1.AddDays(-1);
                if (d1.DayOfWeek == DayOfWeek.Sunday)
                    d1 = d1.AddDays(-2);
                DATA_3_3.Text = d1.ToString("dd/MM/yyyy");
            };
            Panel1.Visible = (vezes > 0);
            Panel2.Visible = (vezes > 1);
            Panel3.Visible = (vezes > 2);
            if (vezes == 1)
            {
                RadioButton1.Checked = true;
                Button1.Enabled = true;
            }
            else
            {
                RadioButton1.Checked = false;
                RadioButton2.Checked = false;
                RadioButton3.Checked = false;
                Button1.Enabled = false;
            }
            RadioButton1.Attributes["onclick"] = "javascript:mostra()";
            RadioButton2.Attributes["onclick"] = "javascript:mostra()";
            RadioButton3.Attributes["onclick"] = "javascript:mostra()";
            return "";
        }

        public string calculos(object oDRV) 
        {                        
            DataRowView DRV = (DataRowView)oDRV;
            dBoletos.BoletosRow rowBOL = (dBoletos.BoletosRow)DRV.Row;
            CurBoleto.CarregaDados(rowBOL);
            CurBoleto.Calcula(DateTime.Now,true);
            if (!IsPostBack)
                if (CurBoleto.stparcela == Statusparcela.Stparaberto)
                    rowBOL.Parcelar = false;
                else
                    rowBOL.Parcelar = true;
            return "";
        }

        public static bool Marcar(object oDRV)
        {
            DataRowView DRV = (DataRowView)oDRV;
            dBoletos.BoletosRow rowBOL = (dBoletos.BoletosRow)DRV.Row;
            return rowBOL.Parcelar;
        }

        public string GeraID() 
        {
            return CurBoleto.NossoNumero.ToString();
        }

        protected void CheckBox1_CheckedChanged(object sender, EventArgs e)
        {
            CheckBox CheckBoxsender = (CheckBox)sender;                        
            int BOL = int.Parse(CheckBoxsender.Attributes["BOL"]) ;
            dBoletos.BoletosRow row = dBoletos1.Boletos.FindByNumero(BOL);
            row.Parcelar = CheckBoxsender.Checked;
            CarregaDados();
            
        }

        

        private void ProcessoAntigo()
        {            
            ArrayList BoletosOrigem = new ArrayList();
            foreach (dBoletos.BoletosRow rowBoletos in dBoletos1.Boletos)
                if (rowBoletos.Parcelar)
                    BoletosOrigem.Add(rowBoletos);
            SortedList BoletosNovos = new SortedList();
            if (RadioButton1.Checked)
                BoletosNovos.Add(DateTime.Parse(DATA_1_1.Text), decimal.Parse(VALOR_1_1.Text));
            else
                if (RadioButton2.Checked)
                {
                    BoletosNovos.Add(DateTime.Parse(DATA_2_1.Text), decimal.Parse(VALOR_2_1.Text));
                    BoletosNovos.Add(DateTime.Parse(DATA_2_2.Text), decimal.Parse(VALOR_2_2.Text));
                }
                else
                    if (RadioButton3.Checked)
                    {
                        BoletosNovos.Add(DateTime.Parse(DATA_3_1.Text), decimal.Parse(VALOR_3_1.Text));
                        BoletosNovos.Add(DateTime.Parse(DATA_3_2.Text), decimal.Parse(VALOR_3_2.Text));
                        BoletosNovos.Add(DateTime.Parse(DATA_3_3.Text), decimal.Parse(VALOR_3_3.Text));
                    };            

            using (dAcordoRet dRetorno = new dAcordoRet())
            {
                VirMSSQL.TableAdapter TATrans = new VirMSSQL.TableAdapter(VirDB.Bancovirtual.TiposDeBanco.SQL, true);
                try
                {                    
                    TATrans.Cone.ConnectionString = System.Configuration.ConfigurationManager.ConnectionStrings["ConnectionStringN1"].ConnectionString;
                    TATrans.AbreTrasacaoLocal("ProcessoAntigo", dRetorno.RetOriginaisTableAdapter, dRetorno.RetNovosBTableAdapter, dRetorno.BOLETOSTableAdapter);
                    // Criar acordo na tabela ret
                    dAcordoRet.RetAcordoRow rowAcordo = dRetorno.RetAcordo.NewRetAcordoRow();
                    rowAcordo.apartamento = Cred.rowCli.Apartamento;
                    rowAcordo.bloco = Cred.rowCli.bloco;
                    rowAcordo.codcon = Cred.rowCli.CODCON;
                    rowAcordo.data = DateTime.Now;
                    rowAcordo.DESTINATARIO = Cred.rowCli.Proprietario ? "P" : "I";
                    rowAcordo.Efetuado = false;
                    rowAcordo.filial = Cred.EMP == 3 ? (short)2 : (short)Cred.EMP;
                    rowAcordo.Judicial = false;
                    rowAcordo.Juridico = false;
                    dRetorno.RetAcordo.AddRetAcordoRow(rowAcordo);
                    dRetorno.RetAcordoTableAdapter.Update(rowAcordo);
                    rowAcordo.AcceptChanges();
                    Cred.rowCli.Acordo = -rowAcordo.id;                    
                    foreach (dBoletos.BoletosRow rowBoletos in BoletosOrigem)
                    {
                        dAcordoRet.RetOriginaisRow rowOrig = dRetorno.RetOriginais.NewRetOriginaisRow();
                        //rowOrig.Acordo = rowAcordo.id;                        
                        rowOrig.RetAcordoRow = rowAcordo;
                        rowOrig.numero = (int)rowBoletos.Numero;
                        rowOrig.total = rowBoletos.ValorFinal;
                        dRetorno.RetOriginais.AddRetOriginaisRow(rowOrig);
                        dRetorno.RetOriginaisTableAdapter.Update(rowOrig);
                        rowOrig.AcceptChanges();                        
                        rowBoletos.Delete();                        
                        dBoletos1.Boletos.AcceptChanges();
                    }
                    //Criando Novos
                    //short i = 1;
                    foreach (DictionaryEntry DE in BoletosNovos)
                    {
                        dAcordoRet.BOLETOSRow rowBoletos = dRetorno.BOLETOS.NewBOLETOSRow();
                        rowBoletos.apartamento = Cred.rowCli.Apartamento;
                        rowBoletos.bloco = Cred.rowCli.bloco;
                        rowBoletos.CODCON = Cred.rowCli.CODCON;                        
                        rowBoletos.vencto = (DateTime)DE.Key;                        
                        dRetorno.BOLETOS.AddBOLETOSRow(rowBoletos);
                        dRetorno.BOLETOSTableAdapter.Update(rowBoletos);
                        rowBoletos.AcceptChanges();
                        dAcordoRet.RetNovosBRow rowNovos = dRetorno.RetNovosB.NewRetNovosBRow();                        
                        rowNovos.RetAcordoRow = rowAcordo;
                        rowNovos.Honorarios = 0;                        
                        rowNovos.valor = (decimal)DE.Value;
                        rowNovos.vencto = (DateTime)DE.Key;
                        rowNovos.nn = rowBoletos.NN;
                        dRetorno.RetNovosB.AddRetNovosBRow(rowNovos);
                        dRetorno.RetNovosBTableAdapter.Update(rowNovos);
                        rowNovos.AcceptChanges();                        
                    };
                    TATrans.Commit();                    
                }
                catch (Exception e)
                {
                    TATrans.Vircatch(e);                    
                    throw e;
                }
            };            
        }

        LoginMenu.NeonOnLine.Credenciais Cred;

        protected void Button1_Click(object sender, EventArgs e)
        {
            //Cred = (LoginMenu.NeonOnLine.Credenciais)Session["Cred"];
            Cred = Request.QueryString["IDCred"] != null ? (Credenciais)Session["Cred" + Request.QueryString["IDCred"]] : (Credenciais)Session["Cred"];
            ProcessoAntigo();
            Session["AcordoFeito"] = 1;
            if (Request.QueryString["IDCred"] == null)
                Response.Redirect("boletos.aspx");
            else
                Response.Redirect(string.Format("boletos.aspx?IDCred={0}", Request.QueryString["IDCred"]));
        }
    }
}
