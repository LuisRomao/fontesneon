﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using LoginMenu.NeonOnLine;
using System.Data;

namespace Boletos.NeonOnLine
{
    public partial class Acordo : System.Web.UI.Page
    {
        private Credenciais Cred
        {
            get 
            {
                if(Session["CredAcordoAbrir"] == null)
                    Response.Redirect("Login.aspx?Ex=1");
                return (Credenciais)Session["CredAcordoAbrir"];
            }
        }

        #region strings
        private string comandoBuscaHonorarios =
"SELECT     BOletoDetalhe.BODValor, CHEques.CHEEmissao\r\n" +
"FROM         BODxPAG INNER JOIN\r\n" +
"                      PAGamentos ON BODxPAG.PAG = PAGamentos.PAG INNER JOIN\r\n" +
"                      CHEques ON PAGamentos.PAG_CHE = CHEques.CHE RIGHT OUTER JOIN\r\n" +
"                      BOletoDetalhe ON BODxPAG.BOD = BOletoDetalhe.BOD RIGHT OUTER JOIN\r\n" +
"                      BOLetos ON BOletoDetalhe.BOD_BOL = BOLetos.BOL\r\n" +
"WHERE     (BOletoDetalhe.BOD_PLA = '119000') AND (BOLetos.BOL = @P1);";

        private string comandoBuscaHonorarios2 =
"SELECT     BOletoDetalhe_1.BODValor, CHEques.CHEEmissao, BOletoDetalhe.BOD_BOL\r\n" +
"FROM         BOletoDetalhe AS BOletoDetalhe_1 FULL OUTER JOIN\r\n" +
"                      BODxPAG INNER JOIN\r\n" +
"                      PAGamentos ON BODxPAG.PAG = PAGamentos.PAG INNER JOIN\r\n" +
"                      CHEques ON PAGamentos.PAG_CHE = CHEques.CHE ON BOletoDetalhe_1.BOD = BODxPAG.BOD FULL OUTER JOIN\r\n" +
"                      BOLetos INNER JOIN\r\n" +
"                      BOletoDetalhe ON BOLetos.BOL = BOletoDetalhe.BODAcordo_BOL ON BOletoDetalhe_1.BOD_BOL = BOLetos.BOL\r\n" +
"WHERE     (BOletoDetalhe_1.BOD_PLA = '119000') AND (BOletoDetalhe.BOD_BOL = @P1);";

        
        #endregion

        private decimal TotalH;

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {                                
                objetosNeon.dBoletos dBoletos1 = new objetosNeon.dBoletos();
                dBoletos1.EMP = Cred.EMP;
                dBoletos1.ACOrdosTableAdapter.Fill(dBoletos1.ACOrdos, Cred.rowCli.APT);
                dBoletos1.OriginalTableAdapter.Fill(dBoletos1.Original, Cred.rowCli.APT);
                dBoletos1.NovosTableAdapter.Fill(dBoletos1.Novos, Cred.rowCli.APT);
                dBoletos1.CorrorecaoProvisorios(Cred.rowCli.CODCON, Cred.rowCli.bloco, Cred.rowCli.Apartamento, (short)Cred.EMP, Cred.rowCli.APT, Cred.rowCli.Proprietario);
                foreach (objetosNeon.dBoletos.OriginalRow rowO in dBoletos1.Original)
                {
                    rowO.competencia = string.Format("{0:00}/{1:0000}", rowO.BOLCompetenciaMes, rowO.BOLCompetenciaAno);
                    rowO.valor = rowO.valororiginal;
                }
                int ACOAnt = 0;
                int prest = 0;
                VirMSSQL.TableAdapter TA = new VirMSSQL.TableAdapter();
                TA.Cone.ConnectionString = System.Configuration.ConfigurationManager.ConnectionStrings["NeonConnectionString"].ConnectionString;
                if (Cred.EMP != 1)
                    TA.Cone.ConnectionString = TA.Cone.ConnectionString.Replace("Catalog=NEON", "Catalog=NEONSA");
                TotalH = 0;
                foreach (objetosNeon.dBoletos.NovosRow rowD in dBoletos1.Novos)
                {
                    if (rowD.ACO == -1)
                        continue;
                    if (ACOAnt != rowD.ACO)
                    {
                        ACOAnt = rowD.ACO;
                        prest = 1;
                    }
                    rowD.prest = prest++;
                    DataRow DRH = TA.BuscaSQLRow(comandoBuscaHonorarios2, rowD.BOL);
                    if (DRH == null)
                        DRH = TA.BuscaSQLRow(comandoBuscaHonorarios, rowD.BOL);
                    if (DRH == null)
                    {
                        rowD.HValor = 0;
                        rowD.HPag = false;
                    }
                    else
                    {
                        if (DRH["BODValor"] == DBNull.Value)
                            rowD.HValor = 0;
                        else
                        {
                            rowD.HValor = (decimal)DRH["BODValor"];
                            TotalH += rowD.HValor;
                        }
                        rowD.HPag = (DRH["CHEEmissao"] != DBNull.Value);
                    }
                }
                GridView1.DataSource = dBoletos1.Original;
                GridView1.DataBind();
                //GridView2.Columns[2].FooterText = "TESTE 2";
                GridView2.Columns[4].FooterText = TotalH.ToString("n2");
                //GridView2.Columns[1].FooterText = "TESTE 1";
                GridView2.DataSource = dBoletos1.Novos;
                GridView2.DataBind();
                
            }
        }
    }
}