using System;
using System.Web.UI;
using System.Collections;

namespace Boletos.NeonOnLine
{
    public partial class SelDatas : Page
    {
        private SortedList Alternativas;

        protected void Page_Load(object sender, EventArgs e)
        {
            Alternativas = (SortedList)Session["SelDatas"];
            if (!IsPostBack)
            {                
                foreach (DictionaryEntry DE in Alternativas)                
                    RadioButtonList1.Items.Add(string.Format("At� {0:dd/MM/yyyy} - Valor = R$ {1:n2}", DE.Key, DE.Value));                
            }
        }

        protected void RadioButtonList1_SelectedIndexChanged(object sender, EventArgs e)
        {
            DateTime DataSel = (DateTime)Alternativas.GetKey(RadioButtonList1.SelectedIndex);
            Session["Boletopara"] = DataSel;
            Response.Redirect("ImpBol.aspx");
        }
    }
}
