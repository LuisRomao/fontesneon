using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using LoginMenu.NeonOnLine;

namespace Balancetes
{
    /// <summary>
    /// 
    /// </summary>
    public partial class _Default : System.Web.UI.Page
    {
        /// <summary>
        /// Load
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void Button1_Click(object sender, EventArgs e)
        {
            using (LoginMenu.NeonOnLine.dNeon2 dNeon = new LoginMenu.NeonOnLine.dNeon2())
            {
                //LoginMenu.NeonOnLine.dNeon2TableAdapters.clientesTableAdapter TACli = new LoginMenu.NeonOnLine.dNeon2TableAdapters.clientesTableAdapter();
                dNeon2.clientesRow rowcli = dNeon.clientes.NewclientesRow();
                rowcli.CODCON = "PALMAS";
                rowcli.bloco = "";
                rowcli.Apartamento = "";
                rowcli.ID = 0;
                rowcli.tipousuario = 0;
                rowcli.Proprietario = true;                
                int CON = 419;
                    Credenciais Cred = new Credenciais(TipoAut.Neon1, rowcli, 1, CON);
                    Session["Cred"] = Cred;
                    Response.Redirect("NeonOnLine/Balancetes.aspx");                
            };                                                
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void Button2_Click(object sender, EventArgs e)
        {
            using (LoginMenu.NeonOnLine.dNeon2 dNeon = new LoginMenu.NeonOnLine.dNeon2())
            {
                //LoginMenu.NeonOnLine.dNeon2TableAdapters.clientesTableAdapter TACli = new LoginMenu.NeonOnLine.dNeon2TableAdapters.clientesTableAdapter();
                dNeon2.clientesRow rowcli = dNeon.clientes.NewclientesRow();
                rowcli.CODCON = "ACACIA";
                rowcli.bloco = "";
                rowcli.Apartamento = "";
                rowcli.ID = 0;
                rowcli.tipousuario = 0;
                rowcli.Proprietario = true;
                int CON = 233;
                Credenciais Cred = new Credenciais(TipoAut.Neon1, rowcli, 1, CON);
                Session["Cred"] = Cred;
                Response.Redirect("NeonOnLine/Balancetes.aspx");
            };
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void Button3_Click(object sender, EventArgs e)
        {
            using (LoginMenu.NeonOnLine.dNeon2 dNeon = new LoginMenu.NeonOnLine.dNeon2())
            {
                //LoginMenu.NeonOnLine.dNeon2TableAdapters.clientesTableAdapter TACli = new LoginMenu.NeonOnLine.dNeon2TableAdapters.clientesTableAdapter();
                dNeon2.clientesRow rowcli = dNeon.clientes.NewclientesRow();
                rowcli.CODCON = "000205";
                rowcli.bloco = "";
                rowcli.Apartamento = "";
                rowcli.ID = 0;
                rowcli.tipousuario = 0;
                rowcli.Proprietario = true;
                int CON = 829;
                Credenciais Cred = new Credenciais(TipoAut.Neon1, rowcli, 1, CON);
                Session["Cred"] = Cred;
                Response.Redirect("NeonOnLine/Balancetes.aspx");
            };
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void Button4_Click(object sender, EventArgs e)
        {
            using (LoginMenu.NeonOnLine.dNeon2 dNeon = new LoginMenu.NeonOnLine.dNeon2())
            {
                //LoginMenu.NeonOnLine.dNeon2TableAdapters.clientesTableAdapter TACli = new LoginMenu.NeonOnLine.dNeon2TableAdapters.clientesTableAdapter();
                dNeon2.clientesRow rowcli = dNeon.clientes.NewclientesRow();
                rowcli.CODCON = "ACACI.";
                rowcli.bloco = "";
                rowcli.Apartamento = "";
                rowcli.ID = 0;
                rowcli.tipousuario = 0;
                rowcli.Proprietario = true;
                int CON = 546;
                Credenciais Cred = new Credenciais(TipoAut.Neon1, rowcli, 3, CON);
                Session["Cred"] = Cred;
                Response.Redirect("NeonOnLine/Balancetes.aspx");
            };
        }
    }
}
