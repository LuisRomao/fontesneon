﻿namespace Balancetes
{


    partial class dBalancetes
    {
        /// <summary>
        /// 
        /// </summary>
        public int EMP = -1;

        #region TableAdapters

        private dBalancetesTableAdapters.BALancetesTableAdapter bALancetesTableAdapter;
        /// <summary>
        /// TableAdapter padrão para tabela: BALancetes
        /// </summary>
        public dBalancetesTableAdapters.BALancetesTableAdapter BALancetesTableAdapter
        {
            get
            {
                if (bALancetesTableAdapter == null)
                {
                    bALancetesTableAdapter = new dBalancetesTableAdapters.BALancetesTableAdapter();
                    if (EMP != 1)
                        bALancetesTableAdapter.Connection.ConnectionString = bALancetesTableAdapter.Connection.ConnectionString.Replace("Catalog=NEON", "Catalog=NEONSA");
                };
                return bALancetesTableAdapter;
            }
        }

        private dBalancetesTableAdapters.BalanceteTotaisTableAdapter balanceteTotaisTableAdapter;
        /// <summary>
        /// TableAdapter padrão para tabela: BalanceteTotais
        /// </summary>
        public dBalancetesTableAdapters.BalanceteTotaisTableAdapter BalanceteTotaisTableAdapter
        {
            get
            {
                if (balanceteTotaisTableAdapter == null)
                    balanceteTotaisTableAdapter = new dBalancetesTableAdapters.BalanceteTotaisTableAdapter();
                return balanceteTotaisTableAdapter;
            }
        }

        private dBalancetesTableAdapters.BalanceteDetTableAdapter balanceteDetTableAdapter;
        /// <summary>
        /// TableAdapter padrão para tabela: BalanceteDet
        /// </summary>
        public dBalancetesTableAdapters.BalanceteDetTableAdapter BalanceteDetTableAdapter
        {
            get
            {
                if (balanceteDetTableAdapter == null)
                    balanceteDetTableAdapter = new dBalancetesTableAdapters.BalanceteDetTableAdapter();
                return balanceteDetTableAdapter;
            }
        }

        #endregion



        /// <summary>
        /// Carrega a tabela balancetes
        /// </summary>
        /// <param name="CON"></param>
        /// <returns></returns>
        public int Carregatotais(int CON)
        {
            int retorno = BALancetesTableAdapter.Fill(BALancetes, CON); ;
            populaCompetenciaSTR();
            return retorno;
        }

        private void populaCompetenciaSTR()
        {
            foreach (BALancetesRow BALrow in BALancetes)
            {
                int mes = BALrow.BALCompet % 100;
                int ano = BALrow.BALCompet / 100; ;
                System.DateTime data = new System.DateTime(ano, mes, 1);
                BALrow.competenciaSTR = string.Format("{0:MMMM - yyyy} ({0:MM/yyyy})", data);
            }
        }
    }
}
