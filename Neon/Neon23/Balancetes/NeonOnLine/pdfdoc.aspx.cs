using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

namespace Balancetes.NeonOnLine
{
    /// <summary>
    /// 
    /// </summary>
    public partial class pdfdoc : System.Web.UI.Page
    {
        
        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void Page_Load(object sender, EventArgs e)
        {
            string Arquivo = null;
            if (Request.QueryString["ARQUIVO"] != null)
                Arquivo = string.Format(@"{0}{1}.pdf",
                                ConfigurationManager.AppSettings["publicacoesIND"],
                                Request.QueryString["ARQUIVO"]);
            else
                if (Session["ArquivoAbrir"] != null)
                    Arquivo = (string)Session["ArquivoAbrir"];
            

            //return string.Format(@"{0}{1}_{2}_h12.pdf",
            //    ConfigurationManager.AppSettings["publicacoesIND"],
            //    Cred.EMP,
            //    Cred.CON);




            if (Arquivo != null)
            {
                Response.ContentType = "Application/pdf";
                Response.WriteFile(Arquivo);
                Response.End();
            }
        }
    }
}
