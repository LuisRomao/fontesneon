using System;
using LoginMenu.NeonOnLine;
using System.Configuration;

namespace Balancetes.NeonOnLine
{
    /// <summary>
    /// 
    /// </summary>
    public partial class Balancetes : System.Web.UI.Page
    {
        enum Destino { arquivo, link };

        private string NomeArq(int modelo,int competencia,Destino dest)
        {
            return string.Format(@"{0}balancetes\{1}{2}{3}{4}{5}.asp",                         
                dest == Destino.arquivo ? ConfigurationManager.AppSettings["publicacoes"] : "\\",
                Cred.EMP == 2 ? 3:Cred.EMP,
                Cred.rowCli.CODCON.Replace(".","_").Replace("/", "_").Replace("\\", "_").Replace(" ", "_"),
                modelo,
                competencia % 100,
                competencia / 100);
        }

        private string NomeArqH12()
        {
            return string.Format(@"{0}{1}_{2}_h12.pdf",
                ConfigurationManager.AppSettings["publicacoesIND"],
                Cred.EMP == 2 ? 3 : Cred.EMP,
                Cred.CON);
        }

        private string NomeArqBAL()
        {
            string NomeArquivo = string.Format(@"{0}{3:0000}{4:00}\{1}_{2}_B{3:0000}{4:00}.pdf",
                                               ConfigurationManager.AppSettings["publicacoesIND"],
                                               Cred.EMP == 2 ? 3 : Cred.EMP,
                                               Cred.CON,
                                               CompAno,
                                               CompMes);
            if(!System.IO.File.Exists(NomeArquivo))
                NomeArquivo = string.Format(@"{0}{1}_{2}_B{3:0000}{4:00}.pdf",
                ConfigurationManager.AppSettings["publicacoesIND"],
                Cred.EMP == 2 ? 3 : Cred.EMP,
                Cred.CON,
                CompAno,
                CompMes);
            return NomeArquivo;
        }

        private dBalancetes dBalancetes1;
        
        private VirBind Bindtotaisrow;

        private Credenciais Cred
        {
            get
            {
                return Credenciais.CredST(this, true);
            }
        }

        /// <summary>
        /// Load
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void Page_Load(object sender, EventArgs e)
        {
            Bindtotaisrow = new VirBind(LTotrec, LTotdesp, Lacc, Ls113, Ls1acp, Ls1af, Ls1cc, Ls1fr, Ls1po, Ls1psf) { FormatadorPadraodecimal = "n2" };
            if (!IsPostBack)
            {
                dBalancetes1 = new dBalancetes();
                Session["dBalancetes1"] = dBalancetes1;
                dBalancetes1.EMP = Cred.EMP;
                if (dBalancetes1.Carregatotais(Cred.CON) > 0)
                {
                    dBalancetes.BALancetesRow rowBAL = dBalancetes1.BALancetes[0];
                    ViewState["BAL"] = rowBAL.BAL;
                    PopulaTela(rowBAL);
                }
                DropDownList1.DataSource = dBalancetes1.BALancetes;
                DropDownList1.DataBind();

            }
            else
            {
                dBalancetes1 = (dBalancetes)Session["dBalancetes1"];
            }
        }

        private int CompMes;
        private int CompAno;        

        private void PopulaTela(dBalancetes.BALancetesRow rowBAL)
        {
            if (dBalancetes1.BalanceteTotaisTableAdapter.Fill(dBalancetes1.BalanceteTotais, rowBAL.BAL, Cred.EMP) > 0)
                Bindtotaisrow.Popula(dBalancetes1.BalanceteTotais[0]);
            CompAno = rowBAL.BALCompet / 100;
            CompMes = rowBAL.BALCompet % 100;            
            ViewState["CompMes"] = CompMes;
            ViewState["CompAno"] = CompAno;
            LinkButton1.Visible = System.IO.File.Exists(NomeArq(1, rowBAL.BALCompet, Destino.arquivo));
            LinkButton2.Visible = System.IO.File.Exists(NomeArq(2, rowBAL.BALCompet, Destino.arquivo));
            LinkButton3.Visible = System.IO.File.Exists(NomeArq(3, rowBAL.BALCompet, Destino.arquivo));
            LinkButton4.Visible = System.IO.File.Exists(NomeArq(4, rowBAL.BALCompet, Destino.arquivo));            
            LinkButtonBalancete.Visible = System.IO.File.Exists(NomeArqBAL());
            
            LinkButton5.Visible = System.IO.File.Exists(NomeArqH12());

            PanelLinks.Visible = (LinkButton1.Visible || LinkButton2.Visible || LinkButton3.Visible || LinkButton4.Visible || LinkButton5.Visible || LinkButtonBalancete.Visible);

            Session["rowtotcompetencia"] = rowBAL.BALCompet;
            //int CompetBind = int.Parse(rowtot.competencia.Substring(0,2)) + 100*(int.Parse(rowtot.competencia.Substring(2)));
            //LinkButton1.Text = (LinkButton1.Visible || LinkButton2.Visible || LinkButton3.Visible || LinkButton4.Visible) ? "sim" : "nao";
            dBalancetesTableAdapters.BalanceteSaldosTableAdapter BalanceteSaldosTableAdapter1 = new dBalancetesTableAdapters.BalanceteSaldosTableAdapter();
            int EMPverdadeiro = Cred.EMP == 2 ? 3 : Cred.EMP;
            if (BalanceteSaldosTableAdapter1.Fill(dBalancetes1.BalanceteSaldos, Cred.CON, rowBAL.BALCompet, EMPverdadeiro) > 0)
            {
                PanelSaldosNovos.Visible = true;
                PanelSaldosAntigos.Visible = false;
                GridView1.Columns[1].HeaderText = string.Format(" Saldo em {0:dd/MM/yyyy} ",dBalancetes1.BalanceteSaldos[0].DataI);
                GridView1.Columns[2].HeaderText = string.Format(" Saldo em {0:dd/MM/yyyy} ", dBalancetes1.BalanceteSaldos[0].DataF);
                GridView1.DataSource = dBalancetes1.BalanceteSaldos;
                GridView1.DataBind();
            }
            else
            {
                PanelSaldosNovos.Visible = false;
                PanelSaldosAntigos.Visible = true;
            }
        }

        /// <summary>
        /// SelectedIndexChanged
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void DropDownList1_SelectedIndexChanged(object sender, EventArgs e)
        {
            dBalancetes.BALancetesRow rowBAL = dBalancetes1.BALancetes.FindByBALCompet(int.Parse(DropDownList1.SelectedValue));
            ViewState["BAL"] = rowBAL.BAL;
            PopulaTela(rowBAL);
            detalhaRec(true);
            //detalhaDesp(true);
        }

        private void detalhaRec(bool ForcaFechar)
        {
            int BAL = (int)ViewState["BAL"];
            bool abrir = (!ForcaFechar) && (Literal1.Text == "");            
            if (abrir)
            {                
                dBalancetes1.BalanceteDetTableAdapter.Fill(dBalancetes1.BalanceteDet, BAL, Cred.EMP , 1);
                Literal1.Text = "<tr><td></td><td><table class=\"TabelasNeonOnLine\">";
                foreach(dBalancetes.BalanceteDetRow detrow in dBalancetes1.BalanceteDet)                
                    Literal1.Text += string.Format("<tr><td class=\"Titulo\">{0}</td><td class=\"Celulas\" align = \"right\">{1:n2}</td></tr>",Server.HtmlEncode(detrow.titulo).Replace(" ","&nbsp;"),detrow.valor);
                Literal1.Text += "</table></td></tr>";
                LinkButtonDRec.Text = "Detalhar&nbsp;<<";
            }
            else
            {
                Literal1.Text = "";
                LinkButtonDRec.Text = "Detalhar&nbsp;>>";
            }
        }

        private void detalhaDespA(bool ForcaFechar)
        {
            int BAL = (int)ViewState["BAL"];            
            bool abrir = (!ForcaFechar) && (Literal2.Text == "");
            if (abrir)
            {
                //string comp = dBalancetes1.totais.FindBycompetencia(DropDownList1.SelectedValue).competencia;
                dBalancetes1.BalanceteDetTableAdapter.Fill(dBalancetes1.BalanceteDet, BAL, Cred.EMP, 2);
                Literal2.Text = "<tr><td></td><td><table class=\"TabelasNeonOnLine\">";
                foreach (dBalancetes.BalanceteDetRow detrow in dBalancetes1.BalanceteDet)
                {
                    string classe = detrow.negrito ? "Titulo" : "Celulas";
                    string neg = detrow.negrito ? "<b>" : "";
                    string neg1 = detrow.negrito ? "</b>" : "";
                    Literal2.Text += string.Format("<tr><td class=\"{2}\">{0}</td><td class=\"Celulas\" align = \"right\">{3}{1:n2}{4}</td></tr>", Server.HtmlEncode(detrow.titulo).Replace(" ", "&nbsp;"), detrow.valor,classe,neg,neg1);
                }
                Literal2.Text += "</table></td></tr>";
                LinkButtonDDesp.Text = "Detalhar&nbsp;<<";
            }
            else
            {
                Literal2.Text = "";
                LinkButtonDDesp.Text = "Detalhar&nbsp;>>";     
            }
        }

        /*
        private void detalhaDesp(bool ForcaFechar)
        {
            int BAL = (int)ViewState["BAL"];            
            bool abrir = (!ForcaFechar) && (Literal2.Text == "");
            if (abrir)
            {
                Literal2.Text = "<tr><td></td><td><table class=\"TabelasNeonOnLine\">";
                //string comp = dBalancetes1.totais.FindBycompetencia(DropDownList1.SelectedValue).competencia;
                string[] NomeQuadros = new string[] { "Despesas Gerais", "M�o de obra", "Administrativas"};
                for (int Q = 0; Q < 3; Q++)
                {
                    dBalancetes1.BalanceteDetTableAdapter.Fill(dBalancetes1.BalanceteDet, BAL, Cred.EMP, Q + 3);
                    decimal tot = 0;
                    foreach (dBalancetes.BalanceteDetRow detrow in dBalancetes1.BalanceteDet)
                        tot += detrow.valor;
                    Literal2.Text += string.Format("<tr><td class=\"Titulo\">{0}</td><td class=\"Celulas\" align = \"right\"><b>{1:n2}</b></td></tr>", NomeQuadros[Q], tot);
                    foreach (dBalancetes.BalanceteDetRow detrow in dBalancetes1.BalanceteDet)
                    {                        
                        Literal2.Text += string.Format("<tr><td class=\"Celulas\">{0}</td><td class=\"Celulas\" align = \"right\">{1:n2}</td></tr>", Server.HtmlEncode(detrow.titulo).Replace(" ", "&nbsp;"), detrow.valor);
                    }
                }
                Literal2.Text += "</table></td></tr>";
                LinkButtonDDesp.Text = "Detalhar&nbsp;<<";
                
            }
            else
            {
                Literal2.Text = "";
                LinkButtonDDesp.Text = "Detalhar&nbsp;>>";                
            }
        }*/

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void LinkButtonDRec_Click(object sender, EventArgs e)
        {
            detalhaRec(false);
        }


        
        //protected void LinkButtonDDesp_Click(object sender, EventArgs e)
        //{
        //    detalhaDespA(true);
        //    detalhaDesp(LinkButtonDDesp.Text.Contains("<<"));
        //}

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void LinkButtonDespA_Click(object sender, EventArgs e)
        {
            //detalhaDespA(true);
            detalhaDespA(LinkButtonDDesp.Text.Contains("<<"));
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void LinkButton1_Click(object sender, EventArgs e)
        {
            if (sender == LinkButton5)
            {
                Session["ArquivoAbrir"] = NomeArqH12();
                Response.Redirect("pdfdoc.aspx", true);                
            }
            else if (sender == LinkButtonBalancete)
            {
                CompMes = (int)ViewState["CompMes"];
                CompAno = (int)ViewState["CompAno"];
                Session["ArquivoAbrir"] = NomeArqBAL();
                Response.Redirect("pdfdoc.aspx", true);
            }
            else
            {
                int M = 0;
                if (sender == LinkButton1)
                    M = 1;
                if (sender == LinkButton2)
                    M = 2;
                if (sender == LinkButton3)
                    M = 3;
                if (sender == LinkButton4)
                    M = 4;
                Response.Redirect(NomeArq(M, (int)Session["rowtotcompetencia"], Destino.link));
            }
        }

           
        
    }
}
