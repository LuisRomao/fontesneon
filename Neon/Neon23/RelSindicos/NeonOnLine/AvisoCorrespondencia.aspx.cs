﻿using System;
using System.Data;
using System.Web.UI;
using System.Web.UI.WebControls;
using LoginMenu.NeonOnLine;
using System.IO;

namespace RelSindicos.NeonOnLine
{
    public partial class AvisoCorrespondencia : System.Web.UI.Page
    {
        private Credenciais Cred
        {
            get
            {
                return Credenciais.CredST(this, true);
            }
        }

        private dRelSindicos dRel = new dRelSindicos();

        protected void Page_Load(object sender, EventArgs e)
        {
            //Seta EMP
            dRel.EMP = Cred.EMP;

            if (!IsPostBack)
            {
                //Seleciona Clientes
                dRel.ClientesTableAdapter.Fill(dRel.Clientes, Cred.CON);

                //Preenche grid com apenas os que tem email
                DataView dCli = dRel.Clientes.DefaultView;
                dCli.RowFilter = "Email <> ''";
                grdCondominos.DataSource = dCli;
                grdCondominos.DataBind();
            }
        }

        protected void grdCondominos_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            DataRowView drv = (DataRowView)e.Row.DataItem;
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                switch (Convert.ToInt16(drv["TipoUsuario"]))
                {
                    case 1: e.Row.Cells[3].Text = "Síndico"; break;
                    case 2: e.Row.Cells[3].Text = "Sub-Síndico"; break;
                    case 3: e.Row.Cells[3].Text = "Proprietário"; break;
                    case 4: e.Row.Cells[3].Text = "Inquilino"; break;
                    default: e.Row.Cells[3].Text = ""; break;
                };
            }
        }

        private void enviarEmail()
        {
            //Dados do email
            string titulo = Server.HtmlEncode("Aviso de Correspondência");
            string quadro = "";
            if (conteudo.Text != "")
                quadro = "    <p>\r\n" +
                         "      <table bordercolor=\"#75aadb\" cellspacing=\"2\" cellpadding=\"2\"\r\n" +
                         "        border=\"4\" frame=\"border\">\r\n" +
                         "        <tbody>\r\n" +
                         "		  <tr>\r\n" +
                         "			<td>\r\n" +
                         Server.HtmlEncode(conteudo.Text).Replace("\r\n", "<br />") + "\r\n" +
                         "			</td>\r\n" +
                         "		  </tr>\r\n" +
                         "		</tbody>\r\n" +
                         "	  </table>\r\n" +
                         "	</p>\r\n";

            //Cria e-mail
            System.Net.Mail.SmtpClient SmtpClient1 = new System.Net.Mail.SmtpClient();

            //Verifica SMTP de dominio proprio
            VirMSSQL.TableAdapter TA = new VirMSSQL.TableAdapter();
            TA.Cone.ConnectionString = System.Configuration.ConfigurationManager.ConnectionStrings["ConnectionStringN2"].ConnectionString;
            System.Data.DataTable SMTP = TA.BuscaSQLTabela("SELECT CONSMTPHost, CONSMTPUser, CONSMTPPass, CONSMTPPort FROM CONDOMINIOS WHERE (CON = @P1) AND (CON_EMP = @P2) AND CONSMTPHost IS NOT NULL AND CONSMTPHost <> ''", Cred.CON, Cred.EMP);
            foreach (System.Data.DataRow DR in SMTP.Rows)
            {
                SmtpClient1.Host = DR[0].ToString();
                SmtpClient1.Credentials = new System.Net.NetworkCredential(DR[1].ToString(), DR[2].ToString());
                SmtpClient1.Port = Convert.ToInt32(DR[3]);
            }

            using (System.Net.Mail.MailMessage email = new System.Net.Mail.MailMessage())
            {
                //Bcc
                string Bcc = "";
                if (System.Configuration.ConfigurationManager.AppSettings["CopiarEmail" + Cred.EMP.ToString()] != null)
                    Bcc = System.Configuration.ConfigurationManager.AppSettings["CopiarEmail" + Cred.EMP.ToString()];
                if (Bcc != "")
                    email.Bcc.Add(Bcc);

                //From
                string strFrom = "";
                TA.BuscaEscalar("SELECT CONRemetentePadrao FROM CONDOMINIOS WHERE (CON = @P1) AND (CON_EMP = @P2) AND CONSMTPHost IS NOT NULL AND CONSMTPHost <> ''", out strFrom, Cred.CON, Cred.EMP);
                if (strFrom != "")
                    email.From = new System.Net.Mail.MailAddress(strFrom);
                else
                    if (Cred.EMP == 1)
                        email.From = new System.Net.Mail.MailAddress("neon@neonimoveis.com.br");
                    else
                        email.From = new System.Net.Mail.MailAddress("neonsa@neonimoveis.com.br");

                //Corpo do email
                email.Subject = "Aviso de Correspondência";
                email.IsBodyHtml = true;
                string strMsg = "<html>\r\n" +
                                "  <head>\r\n" +
                                "  </head>\r\n" +
                                "  <body>\r\n" +
                                "    <p>Data:" + DateTime.Now.ToString() + "</p>\r\n" +
                                "    <p>Condomínio:" + Cred.rowCli.CODCON + "</p>" +
                                "    [DESTINATARIO]\r\n" +
                                "    <p>Tipo:<B>" + titulo + "</b></p>\r\n" +
                                "    <p><b>FAVOR RETIRAR ENCOMENDA NA PORTARIA.</b></p>\r\n" +
                                quadro +
                                "  </body>\r\n" +
                                "</html>";

                //To - Verifica destinatarios e manda um por um
                bool Cancelar = true;
                int index = 0;
                foreach (TableRow row in grdCondominos.Rows)
                {
                    CheckBox ch = (CheckBox)row.Cells[0].FindControl("CheckBox1");
                    if (ch.Checked)
                    {
                        //Adiciona apenas este e-mail
                        email.To.Clear();
                        email.To.Add(grdCondominos.DataKeys[index].Value.ToString());
                        Cancelar = false;

                        //Seta corpo da mensagem
                        email.Body = strMsg.Replace("[DESTINATARIO]", 
                                                    "<p>Bloco:" + row.Cells[1].Text + "</p>" +
                                                    "<p>Apartamento:" + row.Cells[2].Text + "</p>");

                        //Envia o e-mail
                        SmtpClient1.Send(email);
                    }
                    index ++;
                }

                //Verifica se nao selecionou e-mails
                if (Cancelar)
                {
                    Confirmado.Text = "Nenhum destinatário selecionado";
                    Confirmado.Visible = true;
                    return;
                }

                //Limpa dados
                conteudo.Text = "";
            };

            //Enviou
            Confirmado.Visible = true;
            Session["txtConfirmar"] = "E-mails enviados";
            Response.Redirect("Confirmacao.aspx");
        }

        protected void Button1_Click(object sender, EventArgs e)
        {
            enviarEmail();
        }
    }
}