﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using LoginMenu.NeonOnLine;
using System.IO;
using System.Threading;

namespace RelSindicos.NeonOnLine
{
    public partial class Circulares : System.Web.UI.Page
    {
        private Credenciais Cred
        {
            get
            {
                return Credenciais.CredST(this, true);
            }
        }

        private dRelSindicos dRel = new dRelSindicos();

        protected void Page_Load(object sender, EventArgs e)
        {
            //Sessao para grids
            Session["CON"] = Cred.CON;
            Session["EMP"] = Cred.EMP;

            //Seta EMP
            dRel.EMP = Cred.EMP;

            if (!IsPostBack)
            {
                //Seleciona Clientes
                dRel.ClientesTableAdapter.Fill(dRel.Clientes, Cred.CON);

                //Preenche grid com todos
                grdCondominos.DataSource = dRel.Clientes;
                grdCondominos.DataBind();
            }
        }

        private void enviarEmail()
        {
            //Dados do email
            string titulo = Server.HtmlEncode("Circular");
            string quadro =
                          "    <p>\r\n" +
                          "      <table bordercolor=\"#75aadb\" cellspacing=\"2\" cellpadding=\"2\"\r\n" +
                          "        border=\"4\" frame=\"border\">\r\n" +
                          "        <tbody>\r\n" +
                          "		  <tr>\r\n" +
                          "			<td>\r\n" +
                          Server.HtmlEncode(conteudo.Text).Replace("\r\n", "<br />") + "\r\n" +
                          "			</td>\r\n" +
                          "		  </tr>\r\n" +
                          "		</tbody>\r\n" +
                          "	  </table>\r\n" +
                          "	</p>\r\n";

            //Cria e-mail
            System.Net.Mail.SmtpClient SmtpClient1 = new System.Net.Mail.SmtpClient();

            //Verifica SMTP de dominio proprio
            VirMSSQL.TableAdapter TA = new VirMSSQL.TableAdapter();
            TA.Cone.ConnectionString = System.Configuration.ConfigurationManager.ConnectionStrings["ConnectionStringN2"].ConnectionString;
            System.Data.DataTable SMTP = TA.BuscaSQLTabela("SELECT CONSMTPHost, CONSMTPUser, CONSMTPPass, CONSMTPPort FROM CONDOMINIOS WHERE (CON = @P1) AND (CON_EMP = @P2) AND CONSMTPHost IS NOT NULL AND CONSMTPHost <> ''", Cred.CON, Cred.EMP);
            foreach (System.Data.DataRow DR in SMTP.Rows)
            {
                SmtpClient1.Host = DR[0].ToString();
                SmtpClient1.Credentials = new System.Net.NetworkCredential(DR[1].ToString(), DR[2].ToString());
                SmtpClient1.Port = Convert.ToInt32(DR[3]);
            }

            using (System.Net.Mail.MailMessage email = new System.Net.Mail.MailMessage())
            {
                //Bcc
                string Bcc = "";
                if (System.Configuration.ConfigurationManager.AppSettings["CopiarEmail" + Cred.EMP.ToString()] != null)
                    Bcc = System.Configuration.ConfigurationManager.AppSettings["CopiarEmail" + Cred.EMP.ToString()];
                if (Bcc != "")
                    email.Bcc.Add(Bcc);

                //From
                string strFrom = "";
                TA.BuscaEscalar("SELECT CONRemetentePadrao FROM CONDOMINIOS WHERE (CON = @P1) AND (CON_EMP = @P2) AND CONSMTPHost IS NOT NULL AND CONSMTPHost <> ''", out strFrom, Cred.CON, Cred.EMP);
                if (strFrom != "")
                    email.From = new System.Net.Mail.MailAddress(strFrom);
                else
                    if (Cred.EMP == 1)
                        email.From = new System.Net.Mail.MailAddress("neon@neonimoveis.com.br");
                    else
                        email.From = new System.Net.Mail.MailAddress("neonsa@neonimoveis.com.br");

                //Verifica se nao tem destino e nao envia
                if (!chkCorpoDiretivo.Checked && !chkCondominos.Checked)
                {
                    Confirmado.Text = "Nenhum destinatário selecionado";
                    Confirmado.Visible = true;
                    return;
                }

                //Corpo do email
                email.Subject = "Circular";
                if (SmtpClient1.Host == "ignored")
                    email.Subject = "TESTE " + email.Subject;
                email.IsBodyHtml = true;
                email.Body = "<html>\r\n" +
                             "  <head>\r\n" +
                             "  </head>\r\n" +
                             "  <body>\r\n" +
                             "    <p>Data:" + DateTime.Now.ToString() + "</p>\r\n" +
                             "    <p>Condomínio:" + Cred.rowCli.CODCON + "</p>" +
                             "    <p>Tipo:<B>" + titulo + "</b></p>\r\n" +
                             quadro +
                             "  </body>\r\n" +
                             "</html>";

                //Inclui anexos
                foreach (objetos.dAnexos.AnexosRow rowANE in dAnexos.Anexos)
                {
                    MemoryStream ms = (MemoryStream)Session[NomeSessinAnexo(rowANE.ID)];
                    email.Attachments.Add(new System.Net.Mail.Attachment(ms, rowANE.NomeAnexo));
                }

                //To - Verifica destinatarios e manda um por um
                if (chkCorpoDiretivo.Checked)
                {
                    foreach (TableRow row in grdCorpoDiretivo.Rows)
                    {
                        if (row.Cells[0].Text != "" && row.Cells[0].Text != "&nbsp;")
                        {
                            //Adiciona apenas este e-mail
                            email.To.Clear();
                            email.To.Add(row.Cells[0].Text);

                            //Envia o e-mail
                            SmtpClient1.Send(email);
                        }
                    }
                }
                if (chkCondominos.Checked)
                {
                    int i = 0;
                    foreach (TableRow row in grdCondominos.Rows)
                    {
                        if (row.Cells[0].Text != "" && row.Cells[0].Text != "&nbsp;")
                        {
                            //Adiciona apenas este e-mail
                            email.To.Clear();
                            email.To.Add(row.Cells[0].Text.Replace(";", ","));

                            //Envia o e-mail
                            SmtpClient1.Send(email);

                            //Controla envio
                            i++;
                            if (i == 50)
                            {
                                Thread.Sleep(5000);
                                i = 0;
                            }
                        }
                    }
                }

                //Limpa dados
                dAnexos.Anexos.Clear();
                conteudo.Text = "";
                GridView2.DataBind(); 
            };

            //Enviou
            Session["txtConfirmar"] = "E-mails enviados";
            Response.Redirect("Confirmacao.aspx");
        }

        protected void cmdEnviar_Click(object sender, EventArgs e)
        {
            enviarEmail();
        }

        protected void Button2_Click(object sender, EventArgs e)
        {
            byte[] Dados = FileUpload1.FileBytes;
            string NomeArquivo = FileUpload1.FileName;
            dAnexos.Anexos.AddAnexosRow(++nAnexos, NomeArquivo);
            Session[NomeSessinAnexo(nAnexos)] = new MemoryStream(Dados);
            GridView2.DataBind();
        }

        private int nAnexos;        

        private objetos.dAnexos dAnexos
        {
            get 
            {
                objetos.dAnexos _dAnexos = (objetos.dAnexos)Session["Anexos"];
                if (_dAnexos == null)                
                    Session["Anexos"] = _dAnexos = new objetos.dAnexos();
                nAnexos = _dAnexos.Anexos.Count == 0 ? 0 : _dAnexos.Anexos[_dAnexos.Anexos.Count - 1].ID;
                return _dAnexos;
            }
        }

        private string NomeSessinAnexo(int n)
        {
            return string.Format("An_{0}", n);
        }

        public objetos.dAnexos.AnexosDataTable AnexosCadastrados()
        {
            return dAnexos.Anexos;
        }

        public void ApagaAnexo(int ID)
        {
            objetos.dAnexos.AnexosRow rowdel = dAnexos.Anexos.FindByID(ID);
            if (rowdel != null)            
                rowdel.Delete();                
        }
    }
}