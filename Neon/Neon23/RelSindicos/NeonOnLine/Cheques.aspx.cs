using System;
using LoginMenu.NeonOnLine;

namespace RelSindicos.NeonOnLine
{
    public partial class Cheques : System.Web.UI.Page
    {
        private Credenciais Cred
        {
            get
            {
                return Credenciais.CredST(this, true);
            }
        }

        enum tipoEnvio { talao, cheque }

        protected void Page_Load(object sender, EventArgs e)
        {

        }

        private void enviarEmail(tipoEnvio tipo)
        {
            string titulo;
            string quadro;
            if (tipo == tipoEnvio.cheque)
            {
                titulo= Server.HtmlEncode("SOLICITA��O DE CHEQUE");
                quadro= "    <p>\r\n"+
                        "      <table bordercolor=\"#75aadb\" cellspacing=\"2\" cellpadding=\"2\"\r\n"+
                        "        border=\"4\" frame=\"border\">\r\n"+
                        "        <tbody>\r\n"+
                        "		  <tr>\r\n"+
                        "			<td>\r\n"+
                        Server.HtmlEncode(conteudo.Text).Replace("\r\n","<br />")+"\r\n"+                      
                        "			</td>\r\n"+
                        "		  </tr>\r\n"+
                        "		</tbody>\r\n"+
                        "	  </table>\r\n"+
                        "	</p>\r\n";
            }
            else
            {
                titulo= Server.HtmlEncode("SOLICITA��O DE TAL�O DE CHEQUE");
                quadro="";
            };

            //Cria email
            System.Net.Mail.SmtpClient SmtpClient1 = new System.Net.Mail.SmtpClient();
            using (System.Net.Mail.MailMessage email = new System.Net.Mail.MailMessage())
            {
                //Bcc
                string Bcc;
                if (System.Configuration.ConfigurationManager.AppSettings["CopiarEmail" + Cred.EMP.ToString()] != null)
                    Bcc = System.Configuration.ConfigurationManager.AppSettings["CopiarEmail" + Cred.EMP.ToString()];
                else
                    Bcc = "";
                if (Bcc != "")
                    email.Bcc.Add(Bcc);

                //From
                if (Cred.EMP == 1)
                    email.From = new System.Net.Mail.MailAddress("neon@neonimoveis.com.br");                        
                else
                    email.From = new System.Net.Mail.MailAddress("neonsa@neonimoveis.com.br");                        

                //To
                email.To.Add(System.Configuration.ConfigurationManager.AppSettings["EmailCheque" + Cred.EMP.ToString()]);

                //Corpo do email
                if (tipo == tipoEnvio.cheque)
                    email.Subject = "Solicita��o de Cheque";
                else
                    email.Subject = "Solicita��o de Tal�o de Cheque";
                email.IsBodyHtml = true;
                email.Body = "<html>\r\n" +
                             "  <head>\r\n" +
                             "  </head>\r\n" +
                             "  <body>\r\n" +
                             "    <p>Data:" + DateTime.Now.ToString() + "</p>\r\n" +
                             "    <p>Condom�nio:" + Cred.rowCli.CODCON +"</p>"+
                             "    <p>Bloco:"      + Cred.rowCli.bloco +"</p>"+
                             "    <p>Apartamento:"+ Cred.rowCli.Apartamento +"</p>"+
                             "    <p>Tipo:<B>"+
                             titulo +
                             "    </b></p>\r\n" +
                             quadro +
                             "  </body>\r\n" +
                             "</html>";

                //Envia o e-mail
                SmtpClient1.Send(email);
            };

            //Enviou
            Confirmado.Visible = true;
        }

        protected void BSolicitarTalao_Click(object sender, EventArgs e)
        {
            conteudo.Text = "";
            enviarEmail(tipoEnvio.talao);
        }

        protected void BSolicitarCheque_Click(object sender, EventArgs e)
        {
            enviarEmail(tipoEnvio.cheque);
        }
    }
}
