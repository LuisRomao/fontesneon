using System;
using System.Data;
using LoginMenu.NeonOnLine;

namespace RelSindicos.NeonOnLine
{
    public partial class Acordos : System.Web.UI.Page
    {
        private string blocoanterior;

        private dAcordos _dAco;

        private dAcordos dAco
        {
            get
            {
                if (_dAco == null)
                    _dAco = new dAcordos(Cred.EMP);
                return _dAco;
            }
        }

        private dAcordos.AcordosRow Acrow;

        private Credenciais Cred
        {
            get
            {
                return Credenciais.CredST(this, true);
            }
        }



        protected void Page_Load(object sender, EventArgs e)
        {
            //Inicia campos
            blocoanterior = "";
            //Seleciona acordos
            dAco.AcordosTableAdapter.Fill(dAco.Acordos, Cred.EMP, Cred.CON);            
            //Preenche grid com todos
            Repeater1.DataSource = dAco.Acordos;
            Repeater1.DataBind();
        }

        public string propinq()
        {
            return (Acrow.ACOProprietario) ? "Propriet&aacute;rio" : "Inquilino";
        }

        public bool Pular(object oDRV)
        {
            bool retorno = false;
            if (oDRV != null)
            {
                DataRowView DRV = (DataRowView)oDRV;
                Acrow = (dAcordos.AcordosRow)DRV.Row;
            };
            if (blocoanterior != Acrow.BLOCodigo)
            {
                retorno = (blocoanterior != "");
                blocoanterior = Acrow.BLOCodigo;
            }
            return retorno;
        }

        public string ConteudoD()
        {
            //Boletos
            string retorno = "";
            dAco.ACOxBOLTableAdapter.FillByOriginal(dAco.ACOxBOL, Cred.EMP, Cred.CON, Acrow.ACO_APT);
            foreach (dAcordos.ACOxBOLRow rowOrig in dAco.ACOxBOL)
                //retorno += string.Format("<tr><td>{0:dd/MM/yyyy}</td><td>{1}</td><td>{2:00}/{3}</td><td>{4:n2}</td><td>{5:n2}</td></tr>",
                retorno += string.Format("<tr><td>{0:dd/MM/yyyy}</td><td>{1}</td><td align=\"center\">{2:00}/{3}</td><td align=\"right\">{4:n2}</td></tr>",
                                          rowOrig.BOLVencto,
                                          rowOrig.BOL,
                                          rowOrig.BOLCompetenciaMes,
                                          rowOrig.BOLCompetenciaAno,
                                          //rowOrig.BOLValorPrevisto,
                                          rowOrig.BOLValorPrevisto);
            return retorno;
        }

        public string ConteudoN()
        {
            //Parcelas
            string retorno = "";
            int N = 1;
            dAco.ACOxBOLTableAdapter.FillByNovo(dAco.ACOxBOL, Cred.EMP, Cred.CON, Acrow.ACO_APT);
            foreach (dAcordos.ACOxBOLRow rowNovo in dAco.ACOxBOL)
            {
                bool Aberto = rowNovo.IsBOLPagamentoNull();
                retorno += string.Format("<tr><td>{0}</td><td align=\"right\">{1:n2}</td><td>{2:dd/MM/yyyy}</td><td align=\"right\">{3}</td><td>{4}</td><td>{5}</td><td>{6}</td></tr>",
                                          N++,
                                          rowNovo.BOLValorPrevisto,
                                          rowNovo.BOLVencto,
                                          Aberto ? "" : rowNovo.BOLValorPago.ToString("n2"),
                                          Aberto ? "" : rowNovo.BOLPagamento.ToString("dd/MM/yyyy"),
                                          rowNovo.BOL,
                                          Aberto ? "N&atilde;o" : "Sim");
            }
            return retorno;
        }
    }
}
