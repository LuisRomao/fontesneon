﻿namespace RelSindicos.NeonOnLine
{
    public partial class dAcordos
    {
        public int EMP;

        public dAcordos(int EMP) : this()
        {
            this.EMP = EMP;
        }

        private dAcordosTableAdapters.AcordosTableAdapter acordosTableAdapter;
        /// <summary>
        /// TableAdapter padrão para tabela: Acordos
        /// </summary>
        public dAcordosTableAdapters.AcordosTableAdapter AcordosTableAdapter
        {
            get
            {
                if (acordosTableAdapter == null)
                {
                    acordosTableAdapter = new dAcordosTableAdapters.AcordosTableAdapter();
                    if (EMP == 3)
                        acordosTableAdapter.Connection.ConnectionString = acordosTableAdapter.Connection.ConnectionString.Replace("Initial Catalog=NEON", "Initial Catalog=NEONSA");
                };
                return acordosTableAdapter;
            }
        }

        private dAcordosTableAdapters.ACOxBOLTableAdapter aCOxBOLTableAdapter;
        /// <summary>
        /// TableAdapter padrão para tabela: ACOxBOL
        /// </summary>
        public dAcordosTableAdapters.ACOxBOLTableAdapter ACOxBOLTableAdapter
        {
            get
            {
                if (aCOxBOLTableAdapter == null)
                {
                    aCOxBOLTableAdapter = new dAcordosTableAdapters.ACOxBOLTableAdapter();
                    if (EMP == 3)
                        aCOxBOLTableAdapter.Connection.ConnectionString = aCOxBOLTableAdapter.Connection.ConnectionString.Replace("Initial Catalog=NEON", "Initial Catalog=NEONSA");
                };
                return aCOxBOLTableAdapter;
            }
        }
    }
}
