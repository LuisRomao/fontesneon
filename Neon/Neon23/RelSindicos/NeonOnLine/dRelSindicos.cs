﻿namespace RelSindicos.NeonOnLine
{
    public partial class dRelSindicos
    {
        public int EMP;

        private dRelSindicosTableAdapters.ClientesTableAdapter taClientesTableAdapter;
        /// <summary>
        /// TableAdapter padrão para tabela: Clientes
        /// </summary>
        public dRelSindicosTableAdapters.ClientesTableAdapter ClientesTableAdapter
        {
            get
            {
                if (taClientesTableAdapter == null)
                {
                    taClientesTableAdapter = new dRelSindicosTableAdapters.ClientesTableAdapter();
                    if (EMP == 3) taClientesTableAdapter.Connection.ConnectionString = taClientesTableAdapter.Connection.ConnectionString.Replace("Initial Catalog=NEON", "Initial Catalog=NEONSA");
                };
                return taClientesTableAdapter;
            }
        }
    }
}
