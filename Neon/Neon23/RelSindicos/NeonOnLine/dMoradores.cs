﻿namespace RelSindicos.NeonOnLine
{


    partial class dMoradores
    {
        public int EMP;

        public dMoradores(int EMP) : this()
        {
            this.EMP = EMP;
        }

        /*
        private dMoradoresTableAdapters.clientesTableAdapter clientesTableAdapter;
        /// <summary>
        /// TableAdapter padrão para tabela: Clientes
        /// </summary>
        public dMoradoresTableAdapters.clientesTableAdapter ClientesTableAdapter
        {
            get
            {
                if (clientesTableAdapter == null)
                {
                    clientesTableAdapter = new dMoradoresTableAdapters.clientesTableAdapter();
                    if (EMP != 1)
                        clientesTableAdapter.Connection.ConnectionString = clientesTableAdapter.Connection.ConnectionString.Replace("NEON1", "NEON" + EMP);
                };
                return clientesTableAdapter;
            }
        }*/

        private dMoradoresTableAdapters.CONDOMINIOSTableAdapter cONDOMINIOSTableAdapter;
        /// <summary>
        /// TableAdapter padrão para tabela: CONDOMINIOS
        /// </summary>
        public dMoradoresTableAdapters.CONDOMINIOSTableAdapter CONDOMINIOSTableAdapter
        {
            get
            {
                if (cONDOMINIOSTableAdapter == null)
                {
                    cONDOMINIOSTableAdapter = new dMoradoresTableAdapters.CONDOMINIOSTableAdapter();
                    if (EMP == 3)
                        cONDOMINIOSTableAdapter.Connection.ConnectionString = cONDOMINIOSTableAdapter.Connection.ConnectionString.Replace("Initial Catalog=NEON", "Initial Catalog=NEONSA");
                };
                return cONDOMINIOSTableAdapter;
            }
        }

        private dMoradoresTableAdapters.BLOCOSTableAdapter bLOCOSTableAdapter;
        /// <summary>
        /// TableAdapter padrão para tabela: BLOCOS
        /// </summary>
        public dMoradoresTableAdapters.BLOCOSTableAdapter BLOCOSTableAdapter
        {
            get
            {
                if (bLOCOSTableAdapter == null)
                {
                    bLOCOSTableAdapter = new dMoradoresTableAdapters.BLOCOSTableAdapter();
                    if (EMP == 3)
                        bLOCOSTableAdapter.Connection.ConnectionString = bLOCOSTableAdapter.Connection.ConnectionString.Replace("Initial Catalog=NEON", "Initial Catalog=NEONSA");
                };
                return bLOCOSTableAdapter;
            }
        }

        private dMoradoresTableAdapters.APARTAMENTOSTableAdapter aPARTAMENTOSTableAdapter;
        /// <summary>
        /// TableAdapter padrão para tabela: APARTAMENTOS
        /// </summary>
        public dMoradoresTableAdapters.APARTAMENTOSTableAdapter APARTAMENTOSTableAdapter
        {
            get
            {
                if (aPARTAMENTOSTableAdapter == null)
                {
                    aPARTAMENTOSTableAdapter = new dMoradoresTableAdapters.APARTAMENTOSTableAdapter();
                    if (EMP == 3)
                        aPARTAMENTOSTableAdapter.Connection.ConnectionString = aPARTAMENTOSTableAdapter.Connection.ConnectionString.Replace("Initial Catalog=NEON", "Initial Catalog=NEONSA");
                };
                return aPARTAMENTOSTableAdapter;
            }
        }
    }
}
