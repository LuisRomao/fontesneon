using System;
using System.Data;
using System.Web.UI;
using System.Web.UI.WebControls;
using LoginMenu.NeonOnLine;
using LoginMenu.NeonOnLine.dNeon2TableAdapters;

namespace RelSindicos.NeonOnLine
{
    public partial class Moradores : Page
    {
        private Credenciais Cred
        {
            get
            {
                return Credenciais.CredST(this, true);
            }
        }

        private dRelSindicos dRel = new dRelSindicos();

        private string ultimobloco = "";

        protected void Page_Load(object sender, EventArgs e)
        {
            //Seta EMP
            dRel.EMP = Cred.EMP;

            if (!IsPostBack)
            {
                //Seleciona Clientes
                dRel.ClientesTableAdapter.Fill(dRel.Clientes, Cred.CON);

                //Preenche grid com todos
                Repeater1.DataSource = dRel.Clientes;
                Repeater1.DataBind();
            }
        }

        public static string TipoUsuario(object oDRV)
        {
            DataRowView DRV = (DataRowView)oDRV;
            dRelSindicos.ClientesRow rowCli = (dRelSindicos.ClientesRow)DRV.Row;
            switch (rowCli.TipoUsuario)
            {
                case 1: return "Síndico";
                case 2: return "Sub-Síndico";
                case 3: return "Proprietário";
                case 4: return "Inquilino";
                default: return "";
            };
        }

        public bool Pulo(object oDRV)
        {
            bool retorno;
            DataRowView DRV = (DataRowView)oDRV;
            dRelSindicos.ClientesRow rowCli = (dRelSindicos.ClientesRow)DRV.Row;
            retorno = ultimobloco != rowCli.Bloco ? (ultimobloco != "") : false;
            ultimobloco = rowCli.Bloco;
            return retorno;
        }

        public static string Linhasemquebra(object oDRV, string Campo)
        {
            DataRowView DRV = (DataRowView)oDRV;
            dRelSindicos.ClientesRow rowCli = (dRelSindicos.ClientesRow)DRV.Row;
            return rowCli[Campo].ToString().Replace("-", "&nbsp;").Replace(" ", "&nbsp;");
        }

        protected void Repeater1_ItemCommand(object source, RepeaterCommandEventArgs e)
        {
            string IDCred = e.CommandArgument.ToString();
            string PropInq = e.CommandName.ToString();

            //Pega as credenciais referente ao morador selecionado
            LoginMenu.NeonOnLine.dNeon2 dNeon = new LoginMenu.NeonOnLine.dNeon2();
            LoginMenu.NeonOnLine.dNeon2TableAdapters.AutenticaTableAdapter TAAut = new LoginMenu.NeonOnLine.dNeon2TableAdapters.AutenticaTableAdapter();
            if (Cred.EMP == 3) TAAut.Connection.ConnectionString = TAAut.Connection.ConnectionString.Replace("Initial Catalog=NEON", "Initial Catalog=NEONSA");
            if (TAAut.FillByAPTPropInq(dNeon.Autentica, int.Parse(IDCred), PropInq) > 0)
            {
                LoginMenu.NeonOnLine.dNeon2.AutenticaRow rowAutentica = dNeon.Autentica[0];
                LoginMenu.NeonOnLine.dNeon2.clientesRow rowcli = dNeon.clientes.NewclientesRow();
                rowcli.APT = rowAutentica.APT;
                rowcli.Nome = rowAutentica.Nome;
                rowcli.CODCON = rowAutentica.CONCodigo;
                rowcli.CONNome = rowAutentica.CONNome;
                rowcli.bloco = rowAutentica.BLOCodigo;
                rowcli.Apartamento = rowAutentica.APTNumero;
                rowcli.Email = rowAutentica.Email;
                rowcli.tipousuario = rowAutentica.TipoUsuario;
                rowcli.Proprietario = Convert.ToBoolean(rowAutentica.Proprietario);
                Credenciais CredID = new Credenciais(TipoAut.Indireta, rowcli, Cred.EMP, Cred.CON);
                Session["Cred" + IDCred] = CredID;
                Response.Redirect("Boletos.aspx?IDCred=" + IDCred);
            };
        }
    }
}
