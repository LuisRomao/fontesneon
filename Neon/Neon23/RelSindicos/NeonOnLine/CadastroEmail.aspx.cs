﻿using System;
using System.Data;
using System.Web.UI;
using System.Web.UI.WebControls;
using LoginMenu.NeonOnLine;

namespace RelSindicos.NeonOnLine
{
    public partial class CadastroEmail : System.Web.UI.Page
    {
        private Credenciais Cred
        {
            get
            {
                return Credenciais.CredST(this, true);
            }
        }

        private dCondominios dCondominios1 = new dCondominios();
        private dCondominios.CONDOMINIOSRow CONDOMINIOSRow1;
        
        protected void Page_Load(object sender, EventArgs e)
        {
            //Sessao para grids
            Session["EMP"] = Cred.EMP;
            Session["CON"] = Cred.CON;

            if (!IsPostBack)
            {
                //Pega os dados do condominio
                LoginMenu.NeonOnLine.dCondominiosTableAdapters.CONDOMINIOSTableAdapter Ta = new LoginMenu.NeonOnLine.dCondominiosTableAdapters.CONDOMINIOSTableAdapter();
                Ta.Fill(dCondominios1.CONDOMINIOS, Cred.EMP);
                CONDOMINIOSRow1 = dCondominios1.CONDOMINIOS.FindByCON_EMPCON(Cred.EMP, Cred.CON); 

                //Preenche campos
                chkFaleCom.Checked = CONDOMINIOSRow1.CONFaleCom;
                chkFaleComCD.Checked = CONDOMINIOSRow1.CONFaleComCD;
                panControleMudanca.Visible= CONDOMINIOSRow1.CONControleMudanca;
                panAutorizacaoFornecedores.Visible = CONDOMINIOSRow1.CONAutorizacaoFornecedores;
                panSolicitacaoReparos.Visible = CONDOMINIOSRow1.CONSolicitacaoReparos;
                panSugestoes.Visible = CONDOMINIOSRow1.CONSugestoes;
                panReserva.Visible = CONDOMINIOSRow1.CONReserva;
            }
        }

        protected void cmdSalvarFaleCom_Click(object sender, EventArgs e)
        {
            //Pega os dados do condominio
            LoginMenu.NeonOnLine.dCondominiosTableAdapters.CONDOMINIOSTableAdapter Ta = new LoginMenu.NeonOnLine.dCondominiosTableAdapters.CONDOMINIOSTableAdapter();
            Ta.Fill(dCondominios1.CONDOMINIOS, Cred.EMP);
            CONDOMINIOSRow1 = dCondominios1.CONDOMINIOS.FindByCON_EMPCON(Cred.EMP, Cred.CON);
            
            //Salva novos dados
            CONDOMINIOSRow1.CONFaleCom = chkFaleCom.Checked;
            CONDOMINIOSRow1.CONFaleComCD = chkFaleComCD.Checked;
            Ta.Update(dCondominios1.CONDOMINIOS);
            dCondominios1.CONDOMINIOS.AcceptChanges();
            System.Web.HttpContext.Current.Response.Write("<script language='JavaScript'>alert('Dados salvos com sucesso!')</script>");
        }

        protected void cmdFaleConoscoIncluirContato_Click(object sender, EventArgs e)
        {
            TextBox xTextBoxNome = (TextBox)GridView2.FooterRow.FindControl("TextBoxNome");
            TextBox xTextBoxCargo = (TextBox)GridView2.FooterRow.FindControl("TextBoxCargo");
            TextBox xTextBoxEmail = (TextBox)GridView2.FooterRow.FindControl("TextBoxEmail");
            if ((xTextBoxNome.Text != "") && (xTextBoxCargo.Text != "") && (xTextBoxEmail.Text != ""))
            {
                SqlDataSource2.InsertParameters["FCCNome"].DefaultValue = xTextBoxNome.Text;
                SqlDataSource2.InsertParameters["FCCEmail"].DefaultValue = xTextBoxEmail.Text;
                SqlDataSource2.InsertParameters["FCCCargo"].DefaultValue = xTextBoxCargo.Text;
                SqlDataSource2.Insert();
            }
        }

        protected void cmdControleMudancaIncluirContato_Click(object sender, EventArgs e)
        {
           TextBox xTextBoxNome = (TextBox)GridView3.FooterRow.FindControl("TextBoxNome");
           TextBox xTextBoxCargo = (TextBox)GridView3.FooterRow.FindControl("TextBoxCargo");
           TextBox xTextBoxEmail = (TextBox)GridView3.FooterRow.FindControl("TextBoxEmail");
           if ((xTextBoxNome.Text != "") && (xTextBoxCargo.Text != "") && (xTextBoxEmail.Text != ""))
           {
              dsControleMudanca.InsertParameters["FCCNome"].DefaultValue = xTextBoxNome.Text;
              dsControleMudanca.InsertParameters["FCCEmail"].DefaultValue = xTextBoxEmail.Text;
              dsControleMudanca.InsertParameters["FCCCargo"].DefaultValue = xTextBoxCargo.Text;
              dsControleMudanca.Insert();
           }
        }

        protected void cmdAutorizacaoFornecedoresIncluirContato_Click(object sender, EventArgs e)
        {
           TextBox xTextBoxNome = (TextBox)GridView4.FooterRow.FindControl("TextBoxNome");
           TextBox xTextBoxCargo = (TextBox)GridView4.FooterRow.FindControl("TextBoxCargo");
           TextBox xTextBoxEmail = (TextBox)GridView4.FooterRow.FindControl("TextBoxEmail");
           if ((xTextBoxNome.Text != "") && (xTextBoxCargo.Text != "") && (xTextBoxEmail.Text != ""))
           {
              dsAutorizacaoFornecedores.InsertParameters["FCCNome"].DefaultValue = xTextBoxNome.Text;
              dsAutorizacaoFornecedores.InsertParameters["FCCEmail"].DefaultValue = xTextBoxEmail.Text;
              dsAutorizacaoFornecedores.InsertParameters["FCCCargo"].DefaultValue = xTextBoxCargo.Text;
              dsAutorizacaoFornecedores.Insert();
           }
        }

        protected void cmdSolicitacaoReparosIncluirContato_Click(object sender, EventArgs e)
        {
           TextBox xTextBoxNome = (TextBox)GridView5.FooterRow.FindControl("TextBoxNome");
           TextBox xTextBoxCargo = (TextBox)GridView5.FooterRow.FindControl("TextBoxCargo");
           TextBox xTextBoxEmail = (TextBox)GridView5.FooterRow.FindControl("TextBoxEmail");
           if ((xTextBoxNome.Text != "") && (xTextBoxCargo.Text != "") && (xTextBoxEmail.Text != ""))
           {
              dsSolicitacaoReparos.InsertParameters["FCCNome"].DefaultValue = xTextBoxNome.Text;
              dsSolicitacaoReparos.InsertParameters["FCCEmail"].DefaultValue = xTextBoxEmail.Text;
              dsSolicitacaoReparos.InsertParameters["FCCCargo"].DefaultValue = xTextBoxCargo.Text;
              dsSolicitacaoReparos.Insert();
           }
        }

        protected void cmdSugestoesIncluirContato_Click(object sender, EventArgs e)
        {
           TextBox xTextBoxNome = (TextBox)GridView6.FooterRow.FindControl("TextBoxNome");
           TextBox xTextBoxCargo = (TextBox)GridView6.FooterRow.FindControl("TextBoxCargo");
           TextBox xTextBoxEmail = (TextBox)GridView6.FooterRow.FindControl("TextBoxEmail");
           if ((xTextBoxNome.Text != "") && (xTextBoxCargo.Text != "") && (xTextBoxEmail.Text != ""))
           {
              dsSugestoes.InsertParameters["FCCNome"].DefaultValue = xTextBoxNome.Text;
              dsSugestoes.InsertParameters["FCCEmail"].DefaultValue = xTextBoxEmail.Text;
              dsSugestoes.InsertParameters["FCCCargo"].DefaultValue = xTextBoxCargo.Text;
              dsSugestoes.Insert();
           }
        }

        protected void cmdReservaIncluirContato_Click(object sender, EventArgs e)
        {
           TextBox xTextBoxNome = (TextBox)GridView7.FooterRow.FindControl("TextBoxNome");
           TextBox xTextBoxCargo = (TextBox)GridView7.FooterRow.FindControl("TextBoxCargo");
           TextBox xTextBoxEmail = (TextBox)GridView7.FooterRow.FindControl("TextBoxEmail");
           if ((xTextBoxNome.Text != "") && (xTextBoxCargo.Text != "") && (xTextBoxEmail.Text != ""))
           {
              dsReserva.InsertParameters["FCCNome"].DefaultValue = xTextBoxNome.Text;
              dsReserva.InsertParameters["FCCEmail"].DefaultValue = xTextBoxEmail.Text;
              dsReserva.InsertParameters["FCCCargo"].DefaultValue = xTextBoxCargo.Text;
              dsReserva.Insert();
           }
        }

        protected void cmdListaConvidadosIncluirContato_Click(object sender, EventArgs e)
        {
            TextBox xTextBoxNome = (TextBox)GridView9.FooterRow.FindControl("TextBoxNome");
            TextBox xTextBoxCargo = (TextBox)GridView9.FooterRow.FindControl("TextBoxCargo");
            TextBox xTextBoxEmail = (TextBox)GridView9.FooterRow.FindControl("TextBoxEmail");
            if ((xTextBoxNome.Text != "") && (xTextBoxCargo.Text != "") && (xTextBoxEmail.Text != ""))
            {
                dsListaConvidados.InsertParameters["FCCNome"].DefaultValue = xTextBoxNome.Text;
                dsListaConvidados.InsertParameters["FCCEmail"].DefaultValue = xTextBoxEmail.Text;
                dsListaConvidados.InsertParameters["FCCCargo"].DefaultValue = xTextBoxCargo.Text;
                dsListaConvidados.Insert();
            }
        }
    }
}