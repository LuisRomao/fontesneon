using System;
using System.Data;
using LoginMenu.NeonOnLine;
using Boletos.objetosNeon;

namespace RelSindicos.NeonOnLine
{
    public partial class Inadimp : System.Web.UI.Page
    {
        public decimal subvencido;
        public decimal subvencidocor;
        public decimal submulta;
        public decimal subjuros;
        public decimal subvalorfinal;
        public decimal subhonor;
        public decimal subvalorfinalg;
        public decimal totalvencido;
        public decimal totalvencidocor;
        public decimal totalmulta;
        public decimal totaljuros;
        public decimal totalvalorfinal;
        public decimal totalhonor;
        public decimal totalvalorfinalg;

        private string blocoanterior;
        private string apanterior;
        private bool linhaunica;

        public VirBoleto CurBoleto;

        private dBoletos dBol = new dBoletos();
        private dBoletos.BoletosRow rowBOL;

        private Credenciais Cred
        {
            get
            {
                return Credenciais.CredST(this, true);
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            //Seta EMP
            dBol.EMP = Cred.EMP;                

            //Pega o condominio
            if (dBol.CONDOMINIOSTableAdapter.Fill(dBol.CONDOMINIOS, Cred.CON) == 0)
                Response.Redirect("WVazia.aspx");

            //Inicia campos
            DataCalculo.Text = DateTime.Today.ToString("dd/MM/yyyy");
            DataCorte.Text = DateTime.Today.AddDays(-4).ToString("dd/MM/yyyy");
            apanterior = blocoanterior = "";
            linhaunica = true;

            //Seleciona boletos
            dBol.BoletosTableAdapter.FillByInadimplencia(dBol.Boletos, Cred.CON, DateTime.Today.AddDays(-3));
            CurBoleto = new VirBoleto(dBol, true);
            if ((Cred.CON == 732) && (Cred.EMP == 1))
                CurBoleto.jurosCompostos = false;

            //Preenche grid com todos
            Repeater3.DataSource = dBol.Boletos;
            Repeater3.DataBind();
        }

        public string Bloco()
        {
            try
            {
                return rowBOL.BLOCodigo;
            }
            catch
            {
                return "";
            }
        }

        public string Apartamento()
        {
            return rowBOL.APTNumero;
        }

        public static string saidastr(decimal valor, string zero)
        {
            return (valor == 0) ? zero : string.Format("{0:n2}", valor);
        }

        public string propinq()
        {
            return (rowBOL.BOLProprietario) ? "Propriet&aacute;rio" : "Inquilino";
        }

        public bool Pular()
        {
            bool retorno = false;
            if (blocoanterior != Bloco())
            {
                retorno = (blocoanterior != "");
                blocoanterior = Bloco();                
            }
            return retorno;            
        }

        public bool MostrarTotal(bool ultimo, object oDRV)
        {
            if (oDRV != null)
            {
                DataRowView DRV = (DataRowView)oDRV;
                rowBOL = (dBoletos.BoletosRow)DRV.Row;
            }
            if ((!linhaunica) && (ultimo || ((apanterior != "") && (apanterior != Apartamento() + (rowBOL.BOLProprietario ? "P":"I")))))
                return true;
            else
                return false;            
        }

        public bool entreapartamentos()
        {
            if (apanterior != (Apartamento() + (rowBOL.BOLProprietario ? "P" : "I")))
                return true;
            else
                return false; 
        }

        public string calculos()
        {
            CurBoleto.CarregaDados(rowBOL);
            CurBoleto.Calcula(DateTime.Today, false);
            totalvencido += CurBoleto.valororiginal;
            totalvencidocor += CurBoleto.ValorCorrigido;
            totalmulta += CurBoleto.multa;
            totaljuros += CurBoleto.juros;
            totalvalorfinal += CurBoleto.valorfinal;
            totalhonor += CurBoleto.honor;
            totalvalorfinalg += CurBoleto.totcomhonor;

            return "";
        }

        public string calculasub()
        {
            if (apanterior != Apartamento() + (rowBOL.BOLProprietario ? "P" : "I"))
            {
                subvencido = 0;
                subvencidocor = 0;
                submulta = 0;
                subjuros = 0;
                subvalorfinal = 0;
                subhonor = 0;
                subvalorfinalg = 0;
                apanterior = Apartamento() + (rowBOL.BOLProprietario ? "P" : "I");
                linhaunica = true;
            }
            else
                linhaunica = false;
            subvencido += CurBoleto.valororiginal;
            subvencidocor += CurBoleto.ValorCorrigido;
            submulta += CurBoleto.multa;
            subjuros += CurBoleto.juros;
            subvalorfinal += CurBoleto.valorfinal;
            subhonor += CurBoleto.honor;
            subvalorfinalg += CurBoleto.totcomhonor;
            return "";
        }
    }    
}


