using System;
using System.Data;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Drawing;
using RelSindicos.NeonOnLine;
using LoginMenu.NeonOnLine;
using LoginMenu.NeonOnLine.dNeon2TableAdapters;

namespace RelSindicos.NeonOnLine
{
    public partial class CadastroUsuario : Page
    {
        private dNeon2 dUser = new dNeon2();
        private dMoradores dMor = new dMoradores();

        private Credenciais Cred
        {
            get
            {
                return Credenciais.CredST(this, true);
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            //Seta EMP
            dMor.EMP = Cred.EMP;

            if (!IsPostBack)
            {
                //Carrega tela
                Carregar();
            }
        }

        private void Carregar()
        {
            //Carrega campos
            CTipoUsuario.Items.Clear();
            if (Cred.TUsuario == TipoUsuario.Administradora)
            {
                //Seta campo de tipos de usuario
                CTipoUsuario.Items.Add(new ListItem("", "0"));
                CTipoUsuario.Items.Add(new ListItem("Administradora", "1"));
                CTipoUsuario.Items.Add(new ListItem("Advogado Administradora", "2"));
                CTipoUsuario.Items.Add(new ListItem("Advogado", "3"));
                CTipoUsuario.Items.Add(new ListItem("S�ndico", "4"));
                CTipoUsuario.Items.Add(new ListItem("Subs�ndico", "5"));
                CTipoUsuario.Items.Add(new ListItem("Gerente Predial", "6"));
                CTipoUsuario.Items.Add(new ListItem("Zelador", "7"));
                CTipoUsuario.Items.Add(new ListItem("Portaria", "8"));

                //Seta visibilidade de campos adicionais
                CSite.SelectedIndex = -1;
                panSite.Visible = true;
                panCondominio.Visible = false;
                CCondominio.Enabled = true;
            }
            else
            {
                //Seta campo de tipos de usuario
                CTipoUsuario.Items.Add(new ListItem("", "0"));
                CTipoUsuario.Items.Add(new ListItem("Advogado", "3"));
                CTipoUsuario.Items.Add(new ListItem("S�ndico", "4"));
                CTipoUsuario.Items.Add(new ListItem("Subs�ndico", "5"));
                CTipoUsuario.Items.Add(new ListItem("Gerente Predial", "6"));
                CTipoUsuario.Items.Add(new ListItem("Zelador", "7"));
                CTipoUsuario.Items.Add(new ListItem("Portaria", "8"));

                //Seta visibilidade de campos adicionais
                CSite.SelectedValue = Cred.EMP.ToString();
                panSite.Visible = false;
                panCondominio.Visible = true;
                CCondominio.Enabled = false;
            }

            //Seta EMP
            dMor.EMP = Convert.ToInt32(CSite.SelectedValue);

            //Carrega o campo de condominios
            dMor.EnforceConstraints = false;
            dMor.CONDOMINIOSTableAdapter.Fill(dMor.CONDOMINIOS, Convert.ToInt32(CSite.SelectedValue));
            CCondominio.DataSource = dMor.CONDOMINIOS;
            CCondominio.DataBind();
            if (Cred.TUsuario != TipoUsuario.Administradora)
                CCondominio.SelectedValue = Cred.CON.ToString();

            //Carrega o campo de blocos
            dMor.EnforceConstraints = false;
            dMor.BLOCOSTableAdapter.Fill(dMor.BLOCOS, Convert.ToInt32(CCondominio.SelectedValue));
            CBloco.DataSource = dMor.BLOCOS;
            CBloco.DataBind();

            //Carrega o campo de apartamentos
            dMor.EnforceConstraints = false;
            dMor.APARTAMENTOSTableAdapter.Fill(dMor.APARTAMENTOS, Convert.ToInt32(CBloco.SelectedValue));
            CApartamento.DataSource = dMor.APARTAMENTOS;
            CApartamento.DataBind();

            //Carrega tela com os usuarios
            if (Cred.TUsuario == TipoUsuario.Administradora)
                dUser.UsuariosTableAdapter.Fill(dUser.USUARIOS);
            else
                dUser.UsuariosTableAdapter.FillByEMP_CON(dUser.USUARIOS, Convert.ToInt32(CSite.SelectedValue), Convert.ToInt32(CCondominio.SelectedValue));
            Repeater1.DataSource = dUser.USUARIOS;
            Repeater1.DataBind();

            //Posiciona no primeiro campo
            CTipoUsuario.Focus();
        }

        protected void VerificaExibicaoCampos()
        {
            //Verifica quais campos exibe dependendo do tipo de usuario
            if ((CTipoUsuario.SelectedValue == "0" && Cred.TUsuario == TipoUsuario.Administradora) || (CTipoUsuario.SelectedValue == "1"))
            {
                panFRNADV.Visible = false;
                panCondominio.Visible = false;
            }
            else if (CTipoUsuario.SelectedValue == "2")
            {
                panFRNADV.Visible = true;
                panCondominio.Visible = false;
            }
            else
            {
                panFRNADV.Visible = false;
                panCondominio.Visible = true;
            }
        }

        protected void CTipoUsuario_SelectedIndexChanged(object sender, EventArgs e)
        {
            //Verifica quais campos exibe dependendo do tipo de usuario
            VerificaExibicaoCampos();

            //Posiciona no mesmo campo
            CTipoUsuario.Focus();
        }

        protected void CFRNADV_SelectedIndexChanged(object sender, EventArgs e)
        {
            //Posiciona no mesmo campo
            CFRNADV.Focus();
        }

        protected void CSite_SelectedIndexChanged(object sender, EventArgs e)
        {
            //Seta EMP
            dMor.EMP = Convert.ToInt32(CSite.SelectedValue);

            //Carrega o campo de condominios
            dMor.EnforceConstraints = false;
            dMor.CONDOMINIOSTableAdapter.Fill(dMor.CONDOMINIOS, Convert.ToInt32(CSite.SelectedValue));
            CCondominio.DataSource = dMor.CONDOMINIOS;
            CCondominio.DataBind();

            //Carrega o campo de blocos
            dMor.EnforceConstraints = false;
            dMor.BLOCOSTableAdapter.Fill(dMor.BLOCOS, Convert.ToInt32(CCondominio.SelectedValue));
            CBloco.DataSource = dMor.BLOCOS;
            CBloco.DataBind();

            //Carrega o campo de apartamentos
            dMor.EnforceConstraints = false;
            dMor.APARTAMENTOSTableAdapter.Fill(dMor.APARTAMENTOS, Convert.ToInt32(CBloco.SelectedValue));
            CApartamento.DataSource = dMor.APARTAMENTOS;
            CApartamento.DataBind();

            //Posiciona no mesmo campo
            CSite.Focus();
        }

        protected void CCondominio_SelectedIndexChanged(object sender, EventArgs e)
        {
            //Seta EMP
            dMor.EMP = Convert.ToInt32(CSite.SelectedValue);

            //Carrega o campo de blocos
            dMor.EnforceConstraints = false;
            dMor.BLOCOSTableAdapter.Fill(dMor.BLOCOS, Convert.ToInt32(CCondominio.SelectedValue));
            CBloco.DataSource = dMor.BLOCOS;
            CBloco.DataBind();

            //Carrega o campo de apartamentos
            dMor.EnforceConstraints = false;
            dMor.APARTAMENTOSTableAdapter.Fill(dMor.APARTAMENTOS, Convert.ToInt32(CBloco.SelectedValue));
            CApartamento.DataSource = dMor.APARTAMENTOS;
            CApartamento.DataBind();

            //Posiciona no mesmo campo
            CCondominio.Focus();
        }

        protected void CBloco_SelectedIndexChanged(object sender, EventArgs e)
        {
            //Seta EMP
            dMor.EMP = Convert.ToInt32(CSite.SelectedValue);

            //Carrega o campo de apartamentos
            dMor.EnforceConstraints = false;
            dMor.APARTAMENTOSTableAdapter.Fill(dMor.APARTAMENTOS, Convert.ToInt32(CBloco.SelectedValue));
            CApartamento.DataSource = dMor.APARTAMENTOS;
            CApartamento.DataBind();

            //Posiciona no mesmo campo
            CBloco.Focus();
        }

        protected void Repeater1_OnItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            //Verifica se e item
            if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
            {
                //Seta o tipo de usuario
                Label lblTipoUsuarioCodigo = (Label)e.Item.FindControl("lblTipoUsuarioCodigo");
                Label lblTipoUsuario = (Label)e.Item.FindControl("lblTipoUsuario");

                if (lblTipoUsuarioCodigo.Text == "0")
                    lblTipoUsuario.Text = "Administradora";
                else if (lblTipoUsuarioCodigo.Text == "16")
                    lblTipoUsuario.Text = "Advogado Administradora";
                else if (lblTipoUsuarioCodigo.Text == "14" || lblTipoUsuarioCodigo.Text == "15")
                    lblTipoUsuario.Text = "Advogado";
                else if (lblTipoUsuarioCodigo.Text == "12" || lblTipoUsuarioCodigo.Text == "13")
                    lblTipoUsuario.Text = "S�ndico";
                else if (lblTipoUsuarioCodigo.Text == "24" || lblTipoUsuarioCodigo.Text == "25")
                    lblTipoUsuario.Text = "Subs�ndico";
                else if (lblTipoUsuarioCodigo.Text == "20" || lblTipoUsuarioCodigo.Text == "21")
                    lblTipoUsuario.Text = "Gerente Predial";
                else if (lblTipoUsuarioCodigo.Text == "22" || lblTipoUsuarioCodigo.Text == "23")
                    lblTipoUsuario.Text = "Zelador";
                else if (lblTipoUsuarioCodigo.Text == "26" || lblTipoUsuarioCodigo.Text == "27")
                    lblTipoUsuario.Text = "Portaria";
            }
        }

        protected void cmdAlterar_Click(object sender, EventArgs e)
        {
            //Limpa mensagem de erro
            LMsg.Text = "";
            LMsg.Visible = false;

            //Limpa os campos da tela
            LimpaCampos();

            //Altera o cadastro do usuario
            Button cmdAlterar = (Button)sender;
            int ID = int.Parse(cmdAlterar.CommandArgument);

            //Procura pelo registro selecionado para alterar
            dUser.UsuariosTableAdapter.Fill(dUser.USUARIOS);
            dNeon2.USUARIOSRow rowUser = dUser.USUARIOS.FindByID(ID);

            //Preenche tela
            LID.Text = rowUser.ID.ToString();
            
            if (rowUser.TIPOUSUARIO == 0)
                CTipoUsuario.SelectedValue = "1";
            if (rowUser.TIPOUSUARIO == 16)
                CTipoUsuario.SelectedValue = "2";
            else if (rowUser.TIPOUSUARIO == 14 || rowUser.TIPOUSUARIO == 15)
                CTipoUsuario.SelectedValue = "3";
            else if (rowUser.TIPOUSUARIO == 12 || rowUser.TIPOUSUARIO == 13)
                CTipoUsuario.SelectedValue = "4";
            else if (rowUser.TIPOUSUARIO == 24 || rowUser.TIPOUSUARIO == 25)
                CTipoUsuario.SelectedValue = "5";
            else if (rowUser.TIPOUSUARIO == 20 || rowUser.TIPOUSUARIO == 21)
                CTipoUsuario.SelectedValue = "6";
            else if (rowUser.TIPOUSUARIO == 22 || rowUser.TIPOUSUARIO == 23)
                CTipoUsuario.SelectedValue = "7";
            else if (rowUser.TIPOUSUARIO == 26 || rowUser.TIPOUSUARIO == 27)
                CTipoUsuario.SelectedValue = "8";

            if (CTipoUsuario.SelectedValue == "2")
                CFRNADV.SelectedValue = rowUser.CODCON.ToString().Trim();
            
            if (Cred.TUsuario == TipoUsuario.Administradora)
            {
                CSite.SelectedValue = rowUser.EMP.ToString();

                dMor.EnforceConstraints = false;
                dMor.CONDOMINIOSTableAdapter.Fill(dMor.CONDOMINIOS, Convert.ToInt32(CSite.SelectedValue));
                CCondominio.DataSource = dMor.CONDOMINIOS;
                CCondominio.DataBind();
                CCondominio.SelectedValue = rowUser.CON.ToString();
            }

            //Seta EMP
            dMor.EMP = Convert.ToInt32(CSite.SelectedValue);

            dMor.EnforceConstraints = false;
            dMor.BLOCOSTableAdapter.Fill(dMor.BLOCOS, Convert.ToInt32(CCondominio.SelectedValue));
            CBloco.DataSource = dMor.BLOCOS;
            CBloco.DataBind();
            if (rowUser.BLOCO.ToUpper() == "XX")
                CBloco.SelectedValue = "0";
            else
                CBloco.SelectedValue = CBloco.Items.FindByText(rowUser.BLOCO.Trim()).Value;
            
            dMor.EnforceConstraints = false;
            dMor.APARTAMENTOSTableAdapter.Fill(dMor.APARTAMENTOS, Convert.ToInt32(CBloco.SelectedValue));
            CApartamento.DataSource = dMor.APARTAMENTOS;
            CApartamento.DataBind();
            if (rowUser.APARTAMENTO.ToUpper() == "XXXX" && rowUser.BLOCO.ToUpper() == "XX")
                CApartamento.SelectedIndex = -1;
            else if (rowUser.APARTAMENTO.ToUpper() == "XXXX" && rowUser.BLOCO.ToUpper() != "XX")
                CApartamento.SelectedValue = "";
            else
                CApartamento.SelectedValue = rowUser.APARTAMENTO.Trim();
            
            TNome.Text = rowUser.NOME;
            TUsuario.Text = rowUser.USERNAME;
            //TSenha.Text = rowUser.SENHA;
            TSenha.Attributes["value"] = rowUser.SENHA;
            if (!rowUser.IsEMAILNull()) TEmail.Text = rowUser.EMAIL;

            //Verifica quais campos exibe dependendo do tipo de usuario
            VerificaExibicaoCampos();

            //Seta botao como alterar
            cmdInserirAlterar.Text = "Alterar";

            //Posiciona no primeiro campo
            CTipoUsuario.Focus();
        }

        protected void cmdRemover_Click(object sender, EventArgs e)
        {
            //Limpa mensagem de erro
            LMsg.Text = "";
            LMsg.Visible = false;

            //Remove o cadastro do usuario da tabela
            Button cmdRemover = (Button)sender;
            int ID = int.Parse(cmdRemover.CommandArgument);

            //Apaga o registro selecionado para excluir
            dUser.UsuariosTableAdapter.Delete(ID);

            //Recarrega a tela
            Carregar();

            //Limpa os campos da tela
            LimpaCampos();

            //Seta botao como inserir
            cmdInserirAlterar.Text = "Inserir";
        }

        private void LimpaCampos()
        {
            //Limpa os campos
            LID.Text = "";
            CTipoUsuario.SelectedIndex = 0;
            CFRNADV.SelectedIndex = 0;
            if (Cred.TUsuario == TipoUsuario.Administradora)
            {
                CSite.SelectedIndex = 0;
                CCondominio.SelectedIndex = 0;
            }
            CBloco.SelectedIndex = 0;
            CApartamento.SelectedIndex = 0;
            TNome.Text = "";
            TUsuario.Text = "";
            //TSenha.Text = "";
            TSenha.Attributes["value"] = "";
            TEmail.Text = "";

            //Exibe ou nao os campos de condominio
            VerificaExibicaoCampos();

            //Posiciona no primeiro campo
            CTipoUsuario.Focus();
        }

        protected void cmdCancelar_Click(object sender, EventArgs e)
        {
            //Limpa mensagem de erro
            LMsg.Text = "";
            LMsg.Visible = false;

            //Limpa os campos da tela
            LimpaCampos();

            //Seta botao como inserir
            cmdInserirAlterar.Text = "Inserir";
        }

        protected void cmdInserirAlterar_Click(object sender, EventArgs e)
        {
            //Limpa mensagem de erro
            LMsg.Text = "";
            LMsg.Visible = false;

            //Faz a consist�ncia dos campos
            if (CTipoUsuario.SelectedValue == "0")
            {
                LMsg.Text = "Informe o tipo de usu�rio";
                LMsg.Visible = true;
                return;
            }
            if (panSite.Visible && CSite.SelectedIndex == 0)
            {
                LMsg.Text = "Informe o site";
                LMsg.Visible = true;
                return;
            }           
            if (panFRNADV.Visible && CFRNADV.SelectedIndex == 0)
            {
                LMsg.Text = "Informe o fornecedor (advogado)";
                LMsg.Visible = true;
                return;
            }           
            if (CTipoUsuario.SelectedValue != "1" && CTipoUsuario.SelectedValue != "2" && CCondominio.SelectedIndex == 0)
            {
                LMsg.Text = "Informe o condom�nio";
                LMsg.Visible = true;
                return;
            }           
            if (TNome.Text == "")
            {
                LMsg.Text = "Preencha o campo nome";
                LMsg.Visible = true;
                return;
            }
            if (TUsuario.Text == "")
            {
                LMsg.Text = "Preencha o campo usu�rio";
                LMsg.Visible = true;
                return;
            }
            if (TSenha.Text == "")
            {
                LMsg.Text = "Preencha o campo senha";
                LMsg.Visible = true;
                TSenha.Attributes["value"] = "";
                return;
            }

            //Verifica usuario e senha repetido
            if (dUser.UsuariosTableAdapter.VerificaUsuarioSenhaRepetido(cmdInserirAlterar.Text, TUsuario.Text, TSenha.Text, LID.Text == "" ? 0 : Convert.ToInt32(LID.Text)) > 0)
            {
                LMsg.Text = "Usu�rio e senha j� utilizados pelo sistema.";
                LMsg.Visible = true;
                return;
            }

            //Seta campos finais
            string strCONCOD = "XXXXXX";
            if (CTipoUsuario.SelectedValue == "2")
                strCONCOD = CFRNADV.SelectedValue;
            else if (CTipoUsuario.SelectedValue != "1" && CTipoUsuario.SelectedValue != "2" && CCondominio.SelectedIndex != 0)
                strCONCOD = CCondominio.SelectedItem.Text.Substring(0, 6).Trim();
            string strAPARTAMENTO = "XXXX";
            if (CTipoUsuario.SelectedValue != "1" && CTipoUsuario.SelectedValue != "2" && CApartamento.SelectedIndex != 0)
                strAPARTAMENTO = CApartamento.SelectedItem.Text.Trim();
            string strBLOCO = "XX";
            if (CTipoUsuario.SelectedValue != "1" && CTipoUsuario.SelectedValue != "2" && CBloco.SelectedIndex != 0)
                strBLOCO = CBloco.SelectedItem.Text.Trim();
            Int16 intTipoUsuario = Convert.ToInt16(CTipoUsuario.SelectedValue);
            Int16 intEMP = Convert.ToInt16(CSite.SelectedValue);
            if (intTipoUsuario == 1)
                intTipoUsuario = 0;
            if (intTipoUsuario ==2)
                intTipoUsuario = 16;
            else if (intTipoUsuario == 3 && intEMP == 1)
                intTipoUsuario = 14;
            else if (intTipoUsuario == 3 && intEMP == 3)
                intTipoUsuario = 15;
            else if (intTipoUsuario == 4 && intEMP == 1)
                intTipoUsuario = 12;
            else if (intTipoUsuario == 4 && intEMP == 3)
                intTipoUsuario = 13;
            else if (intTipoUsuario == 5 && intEMP == 1)
                intTipoUsuario = 24;
            else if (intTipoUsuario == 5 && intEMP == 3)
                intTipoUsuario = 25;
            else if (intTipoUsuario == 6 && intEMP == 1)
                intTipoUsuario = 20;
            else if (intTipoUsuario == 6 && intEMP == 3)
                intTipoUsuario = 21;
            else if (intTipoUsuario == 7 && intEMP == 1)
                intTipoUsuario = 22;
            else if (intTipoUsuario == 7 && intEMP == 3)
                intTipoUsuario = 23;
            else if (intTipoUsuario == 8 && intEMP == 1)
                intTipoUsuario = 26;
            else if (intTipoUsuario == 8 && intEMP == 3)
                intTipoUsuario = 27;

            try
            {
                //Abre uma conexao
                dUser.UsuariosTableAdapter.Connection.Open();
                if (cmdInserirAlterar.Text == "Inserir")
                {
                    //Insere o novo registro
                    dUser.UsuariosTableAdapter.Insert(
                        strCONCOD.ToUpper(),
                        strAPARTAMENTO.ToUpper(),
                        strBLOCO.ToUpper(),
                        TNome.Text.ToUpper(),
                        TUsuario.Text.ToUpper(),
                        TSenha.Text.ToUpper(),
                        TEmail.Text.ToLower(),
                        intTipoUsuario,
                        null, null, null, null, null, null, null, null, null, null, null, null, false, true, int.Parse(CSite.SelectedValue), int.Parse(CCondominio.SelectedValue));

                    //Sucesso
                    LMsg.Text = "Usu�rio inserido com sucesso";
                    LMsg.Visible = true;
                }
                else
                {
                    //Altera o registro
                    dUser.UsuariosTableAdapter.Update(
                        strCONCOD.ToUpper(),
                        strAPARTAMENTO.ToUpper(),
                        strBLOCO.ToUpper(),
                        TNome.Text.ToUpper(),
                        TUsuario.Text.ToUpper(),
                        TSenha.Text.ToUpper(),
                        TEmail.Text.ToLower(),
                        intTipoUsuario,
                        null, null, null, null, null, null, null, null, null, null, null, null, false, true, int.Parse(CSite.SelectedValue), int.Parse(CCondominio.SelectedValue), int.Parse(LID.Text));

                    //Sucesso
                    LMsg.Text = "Usu�rio alterado com sucesso";
                    LMsg.Visible = true;
                }
            }
            catch (Exception)
            {
                //Erro
                LMsg.Text = "Ocorreu um erro no cadastro do usu�rio, tente novamente";
                LMsg.Visible = true;
                return;
            }
            finally
            {
                //Fecha a conexao
                dUser.UsuariosTableAdapter.Connection.Close();
            }

            //Recarrega a tela
            Carregar();

            //Limpa os campos da tela
            LimpaCampos();

            //Seta botao como inserir
            cmdInserirAlterar.Text = "Inserir";
        }
    }
}
