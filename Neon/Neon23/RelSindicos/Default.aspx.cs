using System;
using LoginMenu.NeonOnLine;
using System.Data;

namespace RelSindicos
{
    public partial class _Default : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
        }

        private bool SimulaLogin(string Usuario, string Senha)
        {
            VirMSSQL.TableAdapter TA = new VirMSSQL.TableAdapter(VirDB.Bancovirtual.TiposDeBanco.SQL);
            TA.Cone.ConnectionString = System.Configuration.ConfigurationManager.ConnectionStrings["NeonConnectionString"].ConnectionString;
            for (int EMP = 1; EMP != 0;)
            {
                bool proprietario = true;
                int? APT = TA.BuscaEscalar_int("select APT from APARTAMENTOS where APTUsuarioInternetProprietario = @P1 and APTSenhaInternetProprietario = @P2", Usuario, Senha);
                if (!APT.HasValue)
                {
                    proprietario = false;
                    APT = TA.BuscaEscalar_int("select APT from APARTAMENTOS where APTUsuarioInternetInquilino = @P1 and APTSenhaInternetInquilino = @P2", Usuario, Senha);
                }
                if (APT.HasValue)
                {
                    DataRow DR = TA.BuscaSQLRow("SELECT CON,CONCodigo,CONNome,BLOCodigo,APTNumero FROM CONDOMINIOS INNER JOIN BLOCOS ON CON = BLO_CON INNER JOIN APARTAMENTOS ON BLO = APT_BLO WHERE APT = @P1", APT.Value);
                    dNeon2 dNeon = new dNeon2();
                    dNeon2.clientesRow clirow = dNeon.clientes.NewclientesRow();
                    clirow.Nome = "";
                    clirow.tipousuario = 1;
                    clirow.CODCON = (string)DR["CONCodigo"];
                    clirow.CONNome = (string)DR["CONNome"];
                    clirow.bloco = (string)DR["BLOCodigo"];
                    clirow.Apartamento = (string)DR["APTNumero"];
                    clirow.Proprietario = proprietario;
                    clirow.juridico = false;
                    clirow.Judicial = false;
                    clirow.APT = APT.Value;
                    Credenciais Cred = new Credenciais(TipoAut.MSSQL1, clirow, EMP, (int)DR["CON"]);
                    Session["Cred"] = Cred;
                    //Session["APT"] = APT.Value;
                    return true;
                };
                if (EMP == 1)
                {
                    EMP = 3;
                    TA.Cone.ConnectionString = TA.Cone.ConnectionString.Replace("Catalog=NEON", "Catalog=NEONSA");
                }
                else
                {
                    EMP = 0;
                }
            }
            return false;
        }

        protected void Button1_Click(object sender, EventArgs e)
        {
            LoginMenu.NeonOnLine.dNeon2 dNeon = new LoginMenu.NeonOnLine.dNeon2();
            LoginMenu.NeonOnLine.dNeon2TableAdapters.AutenticaTableAdapter TAAut = new LoginMenu.NeonOnLine.dNeon2TableAdapters.AutenticaTableAdapter();

            //if (TAAut.FillProp(dNeon.Autentica, "1MARLI50", "3631", 1) > 0) //000999
            if (TAAut.FillProp(dNeon.Autentica, "1ANDRE39", "5542", 1) > 0) //000429
            {
                LoginMenu.NeonOnLine.dNeon2.AutenticaRow rowAutentica = dNeon.Autentica[0];
                LoginMenu.NeonOnLine.dNeon2.clientesRow rowcli = dNeon.clientes.NewclientesRow();
                rowcli.APT = rowAutentica.APT;
                rowcli.Nome = rowAutentica.Nome;
                rowcli.CODCON = rowAutentica.CONCodigo;
                rowcli.CONNome = rowAutentica.CONNome;
                rowcli.bloco = rowAutentica.BLOCodigo;
                rowcli.Apartamento = rowAutentica.APTNumero;
                rowcli.Email = rowAutentica.Email;
                rowcli.tipousuario = rowAutentica.TipoUsuario;
                rowcli.Proprietario = Convert.ToBoolean(rowAutentica.Proprietario);
                LoginMenu.NeonOnLine.Credenciais Cred = new LoginMenu.NeonOnLine.Credenciais(LoginMenu.NeonOnLine.TipoAut.Neon1, rowcli, rowAutentica.EMP, rowAutentica.CON);
                Session["Cred"] = Cred;
                Response.Redirect("NeonOnLine/Moradores.aspx");
            };                                                
        }

        protected void Button2_Click(object sender, EventArgs e)
        {
            LoginMenu.NeonOnLine.dNeon2 dNeon = new LoginMenu.NeonOnLine.dNeon2();
            LoginMenu.NeonOnLine.dNeon2TableAdapters.AutenticaTableAdapter TAAut = new LoginMenu.NeonOnLine.dNeon2TableAdapters.AutenticaTableAdapter();
            TAAut.Connection.ConnectionString = TAAut.Connection.ConnectionString.Replace("Initial Catalog=NEON", "Initial Catalog=NEONSA");

            if (TAAut.FillProp(dNeon.Autentica, "2AUGUST1", "4481", 3) > 0) //MANACA
            {
                LoginMenu.NeonOnLine.dNeon2.AutenticaRow rowAutentica = dNeon.Autentica[0];
                LoginMenu.NeonOnLine.dNeon2.clientesRow rowcli = dNeon.clientes.NewclientesRow();
                rowcli.APT = rowAutentica.APT;
                rowcli.Nome = rowAutentica.Nome;
                rowcli.CODCON = rowAutentica.CONCodigo;
                rowcli.CONNome = rowAutentica.CONNome;
                rowcli.bloco = rowAutentica.BLOCodigo;
                rowcli.Apartamento = rowAutentica.APTNumero;
                rowcli.Email = rowAutentica.Email;
                rowcli.tipousuario = rowAutentica.TipoUsuario;
                rowcli.Proprietario = Convert.ToBoolean(rowAutentica.Proprietario);
                LoginMenu.NeonOnLine.Credenciais Cred = new LoginMenu.NeonOnLine.Credenciais(LoginMenu.NeonOnLine.TipoAut.Neon2, rowcli, rowAutentica.EMP, rowAutentica.CON);
                Session["Cred"] = Cred;
                Response.Redirect("NeonOnLine/Moradores.aspx");
            };
        }

        protected void Button3_Click(object sender, EventArgs e)
        {
            LoginMenu.NeonOnLine.dNeon2 dNeon = new LoginMenu.NeonOnLine.dNeon2();
            LoginMenu.NeonOnLine.dNeon2TableAdapters.AutenticaTableAdapter TAAut = new LoginMenu.NeonOnLine.dNeon2TableAdapters.AutenticaTableAdapter();
            //TAAut.Connection.ConnectionString = TAAut.Connection.ConnectionString.Replace("Initial Catalog=NEON", "Initial Catalog=NEONSA");

            //if (TAAut.FillProp(dNeon.Autentica, "1MARLI50", "3631", 1) > 0) //000999
            if (TAAut.FillProp(dNeon.Autentica, "1ANDRE39", "5542", 1) > 0) //000429
            //if (TAAut.FillProp(dNeon.Autentica, "3SERGIO77", "1550", 3) > 0) //ALGRAN
            {
                LoginMenu.NeonOnLine.dNeon2.AutenticaRow rowAutentica = dNeon.Autentica[0];
                LoginMenu.NeonOnLine.dNeon2.clientesRow rowcli = dNeon.clientes.NewclientesRow();
                rowcli.APT = rowAutentica.APT;
                rowcli.Nome = rowAutentica.Nome;
                rowcli.CODCON = rowAutentica.CONCodigo;
                rowcli.CONNome = rowAutentica.CONNome;
                rowcli.bloco = rowAutentica.BLOCodigo;
                rowcli.Apartamento = rowAutentica.APTNumero;
                rowcli.Email = rowAutentica.Email;
                rowcli.tipousuario = rowAutentica.TipoUsuario;
                rowcli.Proprietario = Convert.ToBoolean(rowAutentica.Proprietario);
                LoginMenu.NeonOnLine.Credenciais Cred = new LoginMenu.NeonOnLine.Credenciais(LoginMenu.NeonOnLine.TipoAut.Neon1, rowcli, rowAutentica.EMP, rowAutentica.CON);
                Session["Cred"] = Cred;
                Response.Redirect("NeonOnLine/Inadimp.aspx");
            };
        }

        protected void Button4_Click(object sender, EventArgs e)
        {
            SimulaLogin("3GRAISO1", "2823");
            Response.Redirect("NeonOnLine/Acordos.aspx");            
        }

        protected void Button5_Click(object sender, EventArgs e)
        {
            LoginMenu.NeonOnLine.dNeon2 dNeon = new LoginMenu.NeonOnLine.dNeon2();
            LoginMenu.NeonOnLine.dNeon2TableAdapters.AutenticaTableAdapter TAAut = new LoginMenu.NeonOnLine.dNeon2TableAdapters.AutenticaTableAdapter();
            //TAAut.Connection.ConnectionString = TAAut.Connection.ConnectionString.Replace("Initial Catalog=NEON", "Initial Catalog=NEONSA");

            if (TAAut.FillProp(dNeon.Autentica, "1MARLI50", "3631", 1) > 0) //000999
            //if (TAAut.FillProp(dNeon.Autentica, "2AUGUST1", "4481", 3) > 0) //MANACA
            {
                LoginMenu.NeonOnLine.dNeon2.AutenticaRow rowAutentica = dNeon.Autentica[0];
                LoginMenu.NeonOnLine.dNeon2.clientesRow rowcli = dNeon.clientes.NewclientesRow();
                rowcli.APT = rowAutentica.APT;
                rowcli.Nome = rowAutentica.Nome;
                rowcli.CODCON = rowAutentica.CONCodigo;
                rowcli.CONNome = rowAutentica.CONNome;
                rowcli.bloco = rowAutentica.BLOCodigo;
                rowcli.Apartamento = rowAutentica.APTNumero;
                rowcli.Email = rowAutentica.Email;
                rowcli.tipousuario = rowAutentica.TipoUsuario;
                rowcli.Proprietario = Convert.ToBoolean(rowAutentica.Proprietario);
                LoginMenu.NeonOnLine.Credenciais Cred = new LoginMenu.NeonOnLine.Credenciais(LoginMenu.NeonOnLine.TipoAut.Neon1, rowcli, rowAutentica.EMP, rowAutentica.CON);
                Session["Cred"] = Cred;
                Response.Redirect("NeonOnLine/Cheques.aspx");
            };
        }

        protected void Button6_Click(object sender, EventArgs e)
        {
            LoginMenu.NeonOnLine.dNeon2 dNeon = new LoginMenu.NeonOnLine.dNeon2();
            LoginMenu.NeonOnLine.dNeon2TableAdapters.AutenticaTableAdapter TAAut = new LoginMenu.NeonOnLine.dNeon2TableAdapters.AutenticaTableAdapter();
            //TAAut.Connection.ConnectionString = TAAut.Connection.ConnectionString.Replace("Initial Catalog=NEON", "Initial Catalog=NEONSA");

            //if (TAAut.FillProp(dNeon.Autentica, "1MARLI50", "3631", 1) > 0) //000999
            if (TAAut.FillProp(dNeon.Autentica, "2AUGUST1", "4481", 3) > 0) //MANACA
            {
                LoginMenu.NeonOnLine.dNeon2.AutenticaRow rowAutentica = dNeon.Autentica[0];
                LoginMenu.NeonOnLine.dNeon2.clientesRow rowcli = dNeon.clientes.NewclientesRow();
                rowcli.APT = rowAutentica.APT;
                rowcli.Nome = rowAutentica.Nome;
                rowcli.CODCON = rowAutentica.CONCodigo;
                rowcli.CONNome = rowAutentica.CONNome;
                rowcli.bloco = rowAutentica.BLOCodigo;
                rowcli.Apartamento = rowAutentica.APTNumero;
                rowcli.Email = rowAutentica.Email;
                rowcli.tipousuario = rowAutentica.TipoUsuario;
                rowcli.Proprietario = Convert.ToBoolean(rowAutentica.Proprietario);
                LoginMenu.NeonOnLine.Credenciais Cred = new LoginMenu.NeonOnLine.Credenciais(LoginMenu.NeonOnLine.TipoAut.Neon1, rowcli, rowAutentica.EMP, rowAutentica.CON);
                Session["Cred"] = Cred;
                Response.Redirect("NeonOnLine/FaleCom.aspx");
            };   
        }

        protected void Button7_Click(object sender, EventArgs e)
        {
            LoginMenu.NeonOnLine.dNeon2 dNeon = new LoginMenu.NeonOnLine.dNeon2();
            LoginMenu.NeonOnLine.dNeon2TableAdapters.AutenticaTableAdapter TAAut = new LoginMenu.NeonOnLine.dNeon2TableAdapters.AutenticaTableAdapter();
            //TAAut.Connection.ConnectionString = TAAut.Connection.ConnectionString.Replace("Initial Catalog=NEON", "Initial Catalog=NEONSA");

            if (TAAut.FillProp(dNeon.Autentica, "1MARCEL603", "6122", 1) > 0) //000648
            //if (TAAut.FillProp(dNeon.Autentica, "1MARLI50", "3631", 1) > 0) //000999
            //if (TAAut.FillProp(dNeon.Autentica, "1ANDRE39", "5542", 1) > 0) //000429
            //if (TAAut.FillProp(dNeon.Autentica, "2AUGUST1", "4481", 3) > 0) //MANACA
            {
                LoginMenu.NeonOnLine.dNeon2.AutenticaRow rowAutentica = dNeon.Autentica[0];
                LoginMenu.NeonOnLine.dNeon2.clientesRow rowcli = dNeon.clientes.NewclientesRow();
                rowcli.APT = rowAutentica.APT;
                rowcli.Nome = rowAutentica.Nome;
                rowcli.CODCON = rowAutentica.CONCodigo;
                rowcli.CONNome = rowAutentica.CONNome;
                rowcli.bloco = rowAutentica.BLOCodigo;
                rowcli.Apartamento = rowAutentica.APTNumero;
                rowcli.Email = rowAutentica.Email;
                rowcli.tipousuario = rowAutentica.TipoUsuario;
                rowcli.Proprietario = Convert.ToBoolean(rowAutentica.Proprietario);
                LoginMenu.NeonOnLine.Credenciais Cred = new LoginMenu.NeonOnLine.Credenciais(LoginMenu.NeonOnLine.TipoAut.Neon1, rowcli, rowAutentica.EMP, rowAutentica.CON);
                Session["Cred"] = Cred;
                Response.Redirect("NeonOnLine/Circulares.aspx");
            };
        }

        protected void Button8_Click(object sender, EventArgs e)
        {
            LoginMenu.NeonOnLine.dNeon2 dNeon = new LoginMenu.NeonOnLine.dNeon2();
            LoginMenu.NeonOnLine.dNeon2TableAdapters.AutenticaTableAdapter TAAut = new LoginMenu.NeonOnLine.dNeon2TableAdapters.AutenticaTableAdapter();
            //TAAut.Connection.ConnectionString = TAAut.Connection.ConnectionString.Replace("Initial Catalog=NEON", "Initial Catalog=NEONSA");

            if (TAAut.FillProp(dNeon.Autentica, "1MARLI50", "3631", 1) > 0) //000999
            //if (TAAut.FillProp(dNeon.Autentica, "1ANDRE39", "5542", 1) > 0) //000429
            //if (TAAut.FillProp(dNeon.Autentica, "2AUGUST1", "4481", 3) > 0) //MANACA
            {
                LoginMenu.NeonOnLine.dNeon2.AutenticaRow rowAutentica = dNeon.Autentica[0];
                LoginMenu.NeonOnLine.dNeon2.clientesRow rowcli = dNeon.clientes.NewclientesRow();
                rowcli.APT = rowAutentica.APT;
                rowcli.Nome = rowAutentica.Nome;
                rowcli.CODCON = rowAutentica.CONCodigo;
                rowcli.CONNome = rowAutentica.CONNome;
                rowcli.bloco = rowAutentica.BLOCodigo;
                rowcli.Apartamento = rowAutentica.APTNumero;
                rowcli.Email = rowAutentica.Email;
                rowcli.tipousuario = rowAutentica.TipoUsuario;
                rowcli.Proprietario = Convert.ToBoolean(rowAutentica.Proprietario);
                LoginMenu.NeonOnLine.Credenciais Cred = new LoginMenu.NeonOnLine.Credenciais(LoginMenu.NeonOnLine.TipoAut.Neon1, rowcli, rowAutentica.EMP, rowAutentica.CON);
                Session["Cred"] = Cred;
                Response.Redirect("NeonOnLine/AvisoCorrespondencia.aspx");
            };
        }

        protected void Button9_Click(object sender, EventArgs e)
        {
            LoginMenu.NeonOnLine.dNeon2 dNeon = new LoginMenu.NeonOnLine.dNeon2();
            LoginMenu.NeonOnLine.dNeon2TableAdapters.AutenticaTableAdapter TAAut = new LoginMenu.NeonOnLine.dNeon2TableAdapters.AutenticaTableAdapter();
            //TAAut.Connection.ConnectionString = TAAut.Connection.ConnectionString.Replace("Initial Catalog=NEON", "Initial Catalog=NEONSA");

            if (TAAut.FillProp(dNeon.Autentica, "1MARLI50", "3631", 1) > 0) //000999
            //if (TAAut.FillProp(dNeon.Autentica, "2AUGUST1", "4481", 3) > 0) //MANACA
            {
                LoginMenu.NeonOnLine.dNeon2.AutenticaRow rowAutentica = dNeon.Autentica[0];
                LoginMenu.NeonOnLine.dNeon2.clientesRow rowcli = dNeon.clientes.NewclientesRow();
                rowcli.APT = rowAutentica.APT;
                rowcli.Nome = rowAutentica.Nome;
                rowcli.CODCON = rowAutentica.CONCodigo;
                rowcli.CONNome = rowAutentica.CONNome;
                rowcli.bloco = rowAutentica.BLOCodigo;
                rowcli.Apartamento = rowAutentica.APTNumero;
                rowcli.Email = rowAutentica.Email;
                rowcli.tipousuario = rowAutentica.TipoUsuario;
                rowcli.Proprietario = Convert.ToBoolean(rowAutentica.Proprietario);
                LoginMenu.NeonOnLine.Credenciais Cred = new LoginMenu.NeonOnLine.Credenciais(LoginMenu.NeonOnLine.TipoAut.Neon1, rowcli, rowAutentica.EMP, rowAutentica.CON);
                Session["Cred"] = Cred;
                Response.Redirect("NeonOnLine/CadastroEmail.aspx");
            };
        }

        protected void Button10_Click(object sender, EventArgs e)
        {
            LoginMenu.NeonOnLine.dNeon2 dNeon = new LoginMenu.NeonOnLine.dNeon2();
            LoginMenu.NeonOnLine.dNeon2TableAdapters.CLIENTESN1TableAdapter TACli1 = new LoginMenu.NeonOnLine.dNeon2TableAdapters.CLIENTESN1TableAdapter();

            if (TACli1.Fill(dNeon.CLIENTESN1, "rodino", "062129") > 0)
            {
                LoginMenu.NeonOnLine.dNeon2.CLIENTESN1Row clirow = dNeon.CLIENTESN1[0];
                LoginMenu.NeonOnLine.dNeon2.clientesRow rowcli = dNeon.clientes.NewclientesRow();
                rowcli = dNeon.clientes.NewclientesRow();
                rowcli.Nome = clirow.NOME;
                rowcli.tipousuario = clirow.TIPOUSUARIO;
                rowcli.CODCON = clirow.CODCON;
                rowcli.bloco = clirow.BLOCO;
                rowcli.Apartamento = clirow.APARTAMENTO;
                rowcli.APT = 0;
                LoginMenu.NeonOnLine.Credenciais Cred = new LoginMenu.NeonOnLine.Credenciais(LoginMenu.NeonOnLine.TipoAut.MSSQL1, LoginMenu.NeonOnLine.TipoUsuario.Administradora, rowcli);
                Cred.EMP = 1;
                Session["Cred"] = Cred;
                Response.Redirect("NeonOnLine/CadastroUsuario.aspx");
            };
        }

        protected void Button11_Click(object sender, EventArgs e)
        {
            LoginMenu.NeonOnLine.dNeon2 dNeon = new LoginMenu.NeonOnLine.dNeon2();
            LoginMenu.NeonOnLine.dNeon2TableAdapters.AutenticaTableAdapter TAAut = new LoginMenu.NeonOnLine.dNeon2TableAdapters.AutenticaTableAdapter();

            if (TAAut.FillProp(dNeon.Autentica, "1MARLI50", "3631", 1) > 0) //000999
            {
                LoginMenu.NeonOnLine.dNeon2.AutenticaRow rowAutentica = dNeon.Autentica[0];
                LoginMenu.NeonOnLine.dNeon2.clientesRow rowcli = dNeon.clientes.NewclientesRow();
                rowcli.APT = rowAutentica.APT;
                rowcli.Nome = rowAutentica.Nome;
                rowcli.CODCON = rowAutentica.CONCodigo;
                rowcli.CONNome = rowAutentica.CONNome;
                rowcli.bloco = rowAutentica.BLOCodigo;
                rowcli.Apartamento = rowAutentica.APTNumero;
                rowcli.Email = rowAutentica.Email;
                rowcli.tipousuario = rowAutentica.TipoUsuario;
                rowcli.Proprietario = Convert.ToBoolean(rowAutentica.Proprietario);
                LoginMenu.NeonOnLine.Credenciais Cred = new LoginMenu.NeonOnLine.Credenciais(LoginMenu.NeonOnLine.TipoAut.Neon1, rowcli, rowAutentica.EMP, rowAutentica.CON);
                Session["Cred"] = Cred;
                Response.Redirect("NeonOnLine/CadastroUsuario.aspx");
            };
        }

        protected void Button12_Click(object sender, EventArgs e)
        {
            LoginMenu.NeonOnLine.dNeon2 dNeon = new LoginMenu.NeonOnLine.dNeon2();
            LoginMenu.NeonOnLine.dNeon2TableAdapters.AutenticaTableAdapter TAAut = new LoginMenu.NeonOnLine.dNeon2TableAdapters.AutenticaTableAdapter();
            TAAut.Connection.ConnectionString = TAAut.Connection.ConnectionString.Replace("Initial Catalog=NEON", "Initial Catalog=NEONSA");

            if (TAAut.FillProp(dNeon.Autentica, "2AUGUST1", "4481", 3) > 0) //MANACA
            {
                LoginMenu.NeonOnLine.dNeon2.AutenticaRow rowAutentica = dNeon.Autentica[0];
                LoginMenu.NeonOnLine.dNeon2.clientesRow rowcli = dNeon.clientes.NewclientesRow();
                rowcli.APT = rowAutentica.APT;
                rowcli.Nome = rowAutentica.Nome;
                rowcli.CODCON = rowAutentica.CONCodigo;
                rowcli.CONNome = rowAutentica.CONNome;
                rowcli.bloco = rowAutentica.BLOCodigo;
                rowcli.Apartamento = rowAutentica.APTNumero;
                rowcli.Email = rowAutentica.Email;
                rowcli.tipousuario = rowAutentica.TipoUsuario;
                rowcli.Proprietario = Convert.ToBoolean(rowAutentica.Proprietario);
                LoginMenu.NeonOnLine.Credenciais Cred = new LoginMenu.NeonOnLine.Credenciais(LoginMenu.NeonOnLine.TipoAut.Neon2, rowcli, rowAutentica.EMP, rowAutentica.CON);
                Session["Cred"] = Cred;
                Response.Redirect("NeonOnLine/CadastroUsuario.aspx");
            };
        }
    }
}
