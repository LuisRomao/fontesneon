﻿using System;
using System.Collections.Generic;

using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using LoginMenu.NeonOnLine;

namespace Extratos.NeonOnLine
{
    /// <summary>
    /// 
    /// </summary>
    public partial class Francesinha : System.Web.UI.Page
    {

        private Credenciais Cred
        {
            get
            {
                return Credenciais.CredST(this, true);
            }
        }

        private int CON
        {
            get
            {
                if (Cred != null)
                    return Cred.CON;
                else
                    return 0;
            }
        }

        private int EMP
        {
            get
            {
                int _EMP = 0;
                if (Cred != null)
                    return (Cred.EMP == 2) ? 3 : Cred.EMP;                
                //Session["EMP"] = _EMP;
                return _EMP;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                dExtrato dExtrato1;
                dExtrato.SaldoContaCorrenteRow SCCrow = null;
                if ((Request.Params["SCC"] != null) && (Session["dExtrato1"] != null))
                {
                    int SCC = int.Parse(Request.Params["SCC"]);
                    dExtrato1 = (dExtrato)Session["dExtrato1"];
                    
                    SCCrow = dExtrato1.SaldoContaCorrente.FindBySCC(SCC);
                }
                else
                {
                    dExtrato1 = new dExtrato();                                        
                }
                dExtratoTableAdapters.ContaCorrenTeTableAdapter TACCT = new Extratos.dExtratoTableAdapters.ContaCorrenTeTableAdapter();
                if (EMP != 1)
                    TACCT.Connection.ConnectionString = TACCT.Connection.ConnectionString.Replace("Catalog=NEON", "Catalog=NEONSA");
                int nContas = TACCT.FillByFran(dExtrato1.ContaCorrenTe, CON,DateTime.Now.AddDays(-90));

                int SelCCT = -1;
                int i=0;
                foreach (dExtrato.ContaCorrenTeRow CCTrow in dExtrato1.ContaCorrenTe)
                {
                    if ((CCTrow.IsCCTTituloNull()) || (CCTrow.CCTTitulo == ""))
                        CCTrow.CCTTitulo = "Conta Corrente";
                    if((SCCrow != null) && (SCCrow.SCC_CCT == CCTrow.CCT))
                        SelCCT = i;
                    i++;
                }
                if (nContas > 0)
                {
                    RadioButtonList1.DataSourceID = null;
                    RadioButtonList1.DataSource = dExtrato1.ContaCorrenTe;
                    RadioButtonList1.DataBind();
                }
                if (nContas == 1)
                {
                    RadioButtonList1.SelectedIndex = 0;
                    PopulaDatas();
                }
                else
                    if(SelCCT != -1)
                    {
                        RadioButtonList1.SelectedIndex = SelCCT;
                        PopulaDatas();
                    }
                if (SCCrow != null)
                {
                    //int CCT = int.Parse(RadioButtonList1.SelectedValue);
                    //DateTime DF = DateTime.Parse(DropDownList1.SelectedValue);
                    //DateTime DI = DateTime.Parse(DropDownList2.SelectedValue);
                    for(int k=0;k<DropDownList1.Items.Count;k++)
                        if (DropDownList1.Items[k].Value == SCCrow.SCCData.ToString())
                        {
                            DropDownList1.SelectedIndex = k;
                            DropDownList2.SelectedIndex = k;
                            break;
                        }
                    Panel2.Visible = true;
                    Literal1.Text = TabelasExtratos(SCCrow.SCC_CCT, SCCrow.SCCData, SCCrow.SCCData);
                }
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void RadioButtonList1_SelectedIndexChanged(object sender, EventArgs e)
        {
            Label1.Text += "\r\nSelectedIndexChanged";
            Session["EMP"] = EMP;
            PopulaDatas();
        }

        private void PopulaDatas()
        {
            Session["EMP"] = EMP;
            //int xEMP = EMP;
            Panel1.Visible = true;
            Panel2.Visible = false;
            Label1.Text += string.Format(" CCT = {0} ", RadioButtonList1.SelectedValue);            
            SqlDataSourceDatas.SelectParameters["SCC_CCT"].DefaultValue = RadioButtonList1.SelectedValue;
            SqlDataSourceDatas.SelectParameters["DataCorte"].DefaultValue = DateTime.Now.AddDays(-60).ToString("yyyy-MM-dd");
            if (EMP != 1)
                SqlDataSourceDatas.ConnectionString = SqlDataSourceDatas.ConnectionString.Replace("Catalog=NEON", "Catalog=NEONSA");
            DropDownList2.Items.Clear();
            DropDownList2.DataBind();
            DropDownList1.Items.Clear();
            DropDownList1.DataBind();
            if (DropDownList1.Items.Count > 0)
            {
                DropDownList1.SelectedIndex = DropDownList1.Items.Count - 1;
                DropDownList2.SelectedIndex = (DropDownList2.Items.Count > 5) ? DropDownList2.Items.Count - 5 : 0;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void DropDownList2_SelectedIndexChanged(object sender, EventArgs e)
        {
            Panel2.Visible = false; 
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void Button1_Click(object sender, EventArgs e)
        {
            try
            {
                if (DropDownList2.SelectedValue == null)
                    return;
                Panel2.Visible = true;
                int CCT = int.Parse(RadioButtonList1.SelectedValue);
                DateTime DF = DateTime.Parse(DropDownList1.SelectedValue);
                DateTime DI = DateTime.Parse(DropDownList2.SelectedValue);                
                Literal1.Text = TabelasExtratos(CCT,DI,DF);
            }
            catch
            {
            }
        }

        private string TabelasExtratos(int CCT,DateTime DI,DateTime DF)
        {
            dExtrato dExtrato1 = new dExtrato();
            dExtratoTableAdapters.FrancesinhaTableAdapter TA = new dExtratoTableAdapters.FrancesinhaTableAdapter();
            if (EMP != 1)
                TA.Connection.ConnectionString = TA.Connection.ConnectionString.Replace("Catalog=NEON", "Catalog=NEONSA");
            TA.Fill(dExtrato1.Francesinha,CCT, DI, DF);
            bool ocultarBloco = true;
            foreach (dExtrato.FrancesinhaRow rowTestabloco in dExtrato1.Francesinha)
                if (rowTestabloco.BLOCodigo.ToUpper() != "SB")
                {
                    ocultarBloco = false;
                    break;
                }            
            string AbreTabela = "<table class=\"TabelasNeonOnLineSB\" align=\"left\"><tbody>";
            string LinhaRodape = 
"<tr class=\"Titulo\" align=\"center\">"+
" <td align=\"left\" colspan=\""+ 
(ocultarBloco ? "6" : "7")+
"\">Crédito {0:dd/MM/yyyy}: {1:n2}</td>"+
"</tr>"+
"<tr>"+
" <td>&nbsp;</td>"+
"</tr>";
            string FechaTabela = "</tbody></table>";            
            string LinhaCabecalho =
" <tr class=\"Titulo\" align=\"middle\">" +
"  <td>Data de crédito</td>" +
"  <td>Valor Creditado</td>" +
"  <td>Boleto original</td>"+            
"  <td>Valor</td>" +
(ocultarBloco ? "": "  <td>Bloco</td>")+
"  <td>Apartamento</td>"+
"  <td>Vencimento</td>"+
" </tr>";
            string LinhaLancamentoAbreDia =
"<tr class=\"Celulas\" align=\"center\">" +                
" <td rowspan=\"{0}\">{1:dd/MM/yyyy}</td>" +
" <td align=\"right\" rowspan=\"{2}\">{3:n2}</td>" +
" <td>{4}</td>" +
" <td align=\"right\">{5:n2}"+
(ocultarBloco ? "": " <td>{6}</td>") +
" <td>{7}</td>" +
" <td align=\"right\">{8:dd/MM/yyyy}</td>"+            
"</tr>";
            string LinhaLancamentoAbreCCD =
"<tr class=\"Celulas\" align=\"center\">" +
" <td align=\"right\" rowspan=\"{0}\">{1:n2}</td>" +
" <td>{2}</td>" +
" <td align=\"right\">{3:n2}" +
(ocultarBloco ? "": " <td>{4}</td>" )+
" <td>{5}</td>" +
" <td align=\"right\">{6:dd/MM/yyyy}</td>" +
"</tr>";
            string LinhaLancamento =
"<tr class=\"Celulas\" align=\"center\">" +
" <td>{0}</td>" +
" <td align=\"right\">{1:n2}" +
(ocultarBloco ? "": " <td>{2}</td>" )+
" <td>{3}</td>" +
" <td align=\"right\">{4:dd/MM/yyyy}</td>" +
"</tr>";

            string retorno = "&nbsp;";
            
            
            retorno += AbreTabela;
            //DateTime DataAnterior = DateTime.MinValue;
            decimal ValorDia = 0;
            //foreach (dExtrato.FrancesinhaRow frarow in dExtrato1.Francesinha)
            int linhasDia = 1;
            int linhasLancamentoA = 1;
            int linhasLancamentoB = 1;
            decimal TotalLancamentoA = 0;
            decimal TotalLancamentoB = 0;
            decimal DifA = 0;
            decimal DifB = 0;
            int iB = 0;
            for(int i = 0;i<dExtrato1.Francesinha.Count;i++)
            {
                if ((i == 0) || (dExtrato1.Francesinha[i].SCCData != dExtrato1.Francesinha[i - 1].SCCData))
                {
                    retorno += LinhaCabecalho;
                    ValorDia = 0;
                    linhasDia = 1;
                    linhasLancamentoA = 1;
                    linhasLancamentoB = 0;
                    TotalLancamentoA = dExtrato1.Francesinha[i].BOLValorPago;
                    TotalLancamentoB = 0;
                    DifA = 0;
                    DifB = 0;
                    iB = 0;
                    for (int j = i + 1; j < dExtrato1.Francesinha.Count; j++)
                        if (dExtrato1.Francesinha[i].SCCData == dExtrato1.Francesinha[j].SCCData)
                        {
                            linhasDia++;
                            if (dExtrato1.Francesinha[i].CCD == dExtrato1.Francesinha[j].CCD)
                            {
                                linhasLancamentoA++;
                                TotalLancamentoA += dExtrato1.Francesinha[j].BOLValorPago;
                            }
                            else
                            {
                                if (iB == 0)
                                    iB = j;
                                linhasLancamentoB++;
                                TotalLancamentoB += dExtrato1.Francesinha[j].BOLValorPago;
                            }
                        }
                        else
                            break;
                    if (dExtrato1.Francesinha[i].CCDValor != TotalLancamentoA)
                    {
                        linhasDia++;
                        linhasLancamentoA++;
                        DifA = dExtrato1.Francesinha[i].CCDValor - TotalLancamentoA;
                    }
                    if ((iB != 0) && (dExtrato1.Francesinha[iB].CCDValor != TotalLancamentoB))
                    {
                        linhasDia++;
                        linhasLancamentoB++;
                        DifB = dExtrato1.Francesinha[iB].CCDValor - TotalLancamentoB;
                    }
                    retorno += string.Format(LinhaLancamentoAbreDia,
                        linhasDia,
                        dExtrato1.Francesinha[i].SCCData,
                        linhasLancamentoA,
                        dExtrato1.Francesinha[i].CCDValor,
                        dExtrato1.Francesinha[i].BOL,
                        dExtrato1.Francesinha[i].BOLValorPago,
                        dExtrato1.Francesinha[i].BLOCodigo,
                        dExtrato1.Francesinha[i].APTNumero,
                        dExtrato1.Francesinha[i].BOLVencto);
                }
                else
                    if (dExtrato1.Francesinha[i].CCD == dExtrato1.Francesinha[i - 1].CCD)
                        retorno += string.Format(LinhaLancamento,
                            //dExtrato1.Francesinha[i].CCDValor,
                                dExtrato1.Francesinha[i].BOL,
                                dExtrato1.Francesinha[i].BOLValorPago,
                                dExtrato1.Francesinha[i].BLOCodigo,
                                dExtrato1.Francesinha[i].APTNumero,
                                dExtrato1.Francesinha[i].BOLVencto);
                    else
                    {
                        if (DifA != 0)
                        {
                            retorno += string.Format(LinhaLancamento,                                
                                                           "não identificado",
                                                           DifA,
                                                           "",
                                                           "",
                                                           "");
                            ValorDia += DifA;
                        }
                        
                        //int linhasLancamento2 = 1;
                        //for (int j = i + 1; j < dExtrato1.Francesinha.Count; j++)
                        //    if (dExtrato1.Francesinha[i].CCD == dExtrato1.Francesinha[j].CCD)                            
                        //        linhasLancamento2++;                                                            
                        //    else
                        //        break;
                        retorno += string.Format(LinhaLancamentoAbreCCD,
                            linhasLancamentoB,
                            dExtrato1.Francesinha[i].CCDValor,
                            dExtrato1.Francesinha[i].BOL,
                            dExtrato1.Francesinha[i].BOLValorPago,
                            dExtrato1.Francesinha[i].BLOCodigo,
                            dExtrato1.Francesinha[i].APTNumero,
                            dExtrato1.Francesinha[i].BOLVencto);
                    }
                ValorDia += dExtrato1.Francesinha[i].BOLValorPago;

                if ((i == dExtrato1.Francesinha.Count - 1) || (dExtrato1.Francesinha[i].SCCData != dExtrato1.Francesinha[i + 1].SCCData))
                {
                    if (iB == 0)
                    {
                        if (DifA != 0)
                        {
                            retorno += string.Format(LinhaLancamento,
                                                           "não identificado",
                                                           DifA,
                                                           "",
                                                           "",
                                                           "");
                            ValorDia += DifA;
                        }
                    }
                    else
                        if (DifB != 0)
                        {
                            retorno += string.Format(LinhaLancamento,
                                                           "não identificado",
                                                           DifB,
                                                           "",
                                                           "",
                                                           "");
                            ValorDia += DifB;
                        }
                    retorno += string.Format(LinhaRodape, dExtrato1.Francesinha[i].SCCData, ValorDia);
                }
            }
            
            
            
            retorno += FechaTabela;
            return retorno;
        }
    }
}