using System;
using LoginMenu.NeonOnLine;

namespace Extratos
{
    /// <summary>
    /// 
    /// </summary>
    public partial class Extratos2 : System.Web.UI.Page
    {

        private Credenciais Cred
        {
            get
            {
                return Credenciais.CredST(this, true);
            }
        }

        private int CON 
        {
            get 
            {
                return Cred.CON;                
            }
        }

        private int EMP
        {
            get
            {
                int _EMP;
                if (Cred != null)
                    return (Cred.EMP==2) ? 3 : Cred.EMP;
                else
                {                    
                    int.TryParse(Request.QueryString["F"], out _EMP);
                    return _EMP;
                }
                
            }
        }

        private System.Collections.Generic.SortedList<int,int> ListaPlus
        {
            get 
            {
                System.Collections.Generic.SortedList<int, int> Lista = (System.Collections.Generic.SortedList<int,int>)Session["ListaPlus"];
                if (Lista == null)
                {
                    Lista = new System.Collections.Generic.SortedList<int,int>();
                    Session["ListaPlus"] = Lista;
                }
                return Lista;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void Page_Load(object sender, EventArgs e)
        {                        
            if (!IsPostBack)
            {
                //if ((Cred.TUsuario != TipoUsuario.Administradora) && (Cred.TUsuario != TipoUsuario.Desenvolvimento))
                //    CheckBoxBOL.Visible = false;
                dExtrato dExtrato1 = new dExtrato();                
                dExtratoTableAdapters.ContaCorrenTeTableAdapter TACCT = new Extratos.dExtratoTableAdapters.ContaCorrenTeTableAdapter();
                if (EMP != 1)
                    TACCT.Connection.ConnectionString = TACCT.Connection.ConnectionString.Replace("Catalog=NEON", "Catalog=NEONSA");
                int nContas = TACCT.FillByAtivas(dExtrato1.ContaCorrenTe, CON);
                ListaPlus.Clear();
                foreach (dExtrato.ContaCorrenTeRow CCTrow in dExtrato1.ContaCorrenTe)
                {
                    if ((CCTrow.IsCCTTituloNull()) || (CCTrow.CCTTitulo == ""))
                        CCTrow.CCTTitulo = "Conta Corrente";
                    if (!CCTrow.IsCCTTipoNull() && (CCTrow.CCTTipo == 3))
                        ListaPlus.Add(CCTrow.CCT,CCTrow.CCT_BCO);
                }
                if (nContas > 0)
                {
                    RadioButtonList1.DataSourceID = null;
                    RadioButtonList1.DataSource = dExtrato1.ContaCorrenTe;
                    RadioButtonList1.DataBind();                    
                }                
                if (nContas == 1)
                {
                    RadioButtonList1.SelectedIndex = 0;
                    Session["EMP"] = EMP;
                    PopulaDatas();
                }

            }
            
            
            
            //DataBind();
        }

        private enum TipoModo { extrato,cheque,francesinha,mixto}
        
        private string TabelasExtratos(int CCT,int SCCI,int SCCF)
        {
            TipoModo Modo = TipoModo.extrato;
            bool SomarPlus = CheckBoxPLUS.Checked;

            if (CheckBoxCheque.Checked)
                if (CheckBoxBOL.Checked)
                    Modo = TipoModo.mixto;
                else
                    Modo = TipoModo.cheque;
            else
                if (CheckBoxBOL.Checked)
                    Modo = TipoModo.francesinha;

            string AbreTabela = "<table class=\"TabelasNeonOnLineSB\" align=\"left\"><tbody>";            
            string LinhaRodape = "<tr class=\"Titulo\" align=\"center\"><td align=\"left\" colspan=\"3\">SALDO - {0:dd/MM/yyyy}</td><td align=\"right\">{1:n2}</TD></tr><tr><td>&nbsp;</td></tr>";
            string FechaTabela = "</tbody></table>";
            string LinhaCabecalho="";

            string LinhaCabecalhoSimples =
" <tr class=\"Titulo\" align=\"middle\">" +
"  <td>Data</td>" +
"  <td>Documento</td>" +
"  <td>Lanšamento</td>" +
"  <td>Valor</td></tr>";

            switch (Modo)
            {
                case TipoModo.extrato:
                    LinhaCabecalho = LinhaCabecalhoSimples;
                    break;
                case TipoModo.cheque:
                    LinhaCabecalho =
" <tr class=\"Titulo\" align=\"middle\">" +
"  <td>Data</td>" +
"  <td>Documento</td>" +
"  <td>Lanšamento</td>" +
"  <td>Valor</td>" +
"  <td>Nominal</td>" +
"  <td>Descritivo</td>" +
"  <td>Valor</td></tr>";
                    break;
                case TipoModo.mixto:
                    LinhaCabecalho =
        " <tr class=\"Titulo\" align=\"middle\">" +
        "  <td>Data</td>" +
        "  <td>Documento</td>" +
        "  <td>Lanšamento</td>" +
        "  <td>Valor</td>"+
        "  <td colspan = \"3\">Detalhes</td></tr>";
                    break;
                case TipoModo.francesinha:
                    LinhaCabecalho =
        " <tr class=\"Titulo\" align=\"middle\">" +
        "  <td>Data</td>" +
        "  <td>Documento</td>" +
        "  <td>Lanšamento</td>" +
        "  <td>Valor</td>" +
        "  <td>Unidades</td></tr>";
                    break;
            }

            string LinhaLancamentoCCDUnico =
"<tr class=\"Celulas\" align=\"center\">" +
"<td>{1:dd/MM/yyyy}</td>" +
"<td>{2}</td>" +
"<td>{3}</td>" +
"<td align=\"right\">{4:n2}"+            
"<td>{5}</td>" +
"<td align=\"left\">{6}</td>" +
"<td align=\"right\">{7:n2}</td></tr>";

            string LinhaLancamentoCCDCab =
"<tr class=\"Celulas\" align=\"center\">" +
"<td rowspan=\"{0}\">{1:dd/MM/yyyy}</td>" +
"<td rowspan=\"{0}\">{2}</td>" +
"<td rowspan=\"{0}\">{3}</td>" +
"<td rowspan=\"{0}\" align=\"right\">{4:n2}" +
"<td>{5}</td>" +
"<td align=\"left\">{6}</td>" +
"<td align=\"right\">{7:n2}</td></tr>";

            string LinhaLancamentoCCDDup =
"<tr class=\"Celulas\" align=\"center\">" +
"<td>{5}</td>" +
"<td align=\"left\">{6}</td>" + 
"<td align=\"right\">{7:n2}</td></tr>";

            string LinhaLancamentoFran =
"<tr class=\"Celulas\" align=\"center\">" +
"<td>{0:dd/MM/yyyy}</td>" +
"<td>{1}</td>" +
"<td>{2}</td>" +
"<td align=\"right\">{3:n2}" +
"<td class=\"CelulasBR\">{4}</td>" +
"<td class=\"CelulasBR\">{5}</td>" +
"</tr>";

            string LinhaLancamentoPuro =
"<tr class=\"Celulas\" align=\"center\">" +
"<td>{1:dd/MM/yyyy}</td>" +
"<td>{2}</td>" +
"<td>{3}</td>" +
"<td align=\"right\">{4:n2}" +
"</tr>";


            string LinhaLancamentoChmixto1Cab =
"<tr class=\"Celulas\" align=\"center\">" +
"<td rowspan=\"{0}\">{1:dd/MM/yyyy}</td>" +
"<td rowspan=\"{0}\">{2}</td>" +
"<td rowspan=\"{0}\">{3}</td>" +
"<td rowspan=\"{0}\"align=\"right\">{4:n2}" +
"<td align=\"center\" class=\"subTitulo\">Nominal</td>" +
"<td align=\"center\" class=\"subTitulo\">Descritivo</td>" +
"<td align=\"center\" class=\"subTitulo\">Valor</td>" +
"</tr>";

            string LinhaLancamentoChmixto2 =
"<tr class=\"Celulas\" align=\"center\">" +
"<td>{0}</td>" +
"<td align=\"left\">{1}</td>" +
"<td align=\"right\">{2:n2}" +
"</tr>";

            string LinhaLancamentoFranmixto1 =
"<tr class=\"Celulas\" align=\"center\">" +
"<td rowspan=\"2\">{0:dd/MM/yyyy}</td>" +
"<td rowspan=\"2\">{1}</td>" +
"<td rowspan=\"2\">{2}</td>" +
"<td rowspan=\"2\" align=\"right\">{3:n2}" +
//"<td rowspan=\"2\">{4}</td>" +
"<td  colspan=\"2\" align=\"center\" class=\"subTitulo\">Unidades</td>" +
"</tr>";

            string LinhaLancamentoFranmixto2 =
"<tr class=\"Celulas\" align=\"center\">" +
"<td colspan=\"2\" class=\"CelulasBR\">{0}</td>" +
"<td class=\"CelulasBR\">{1}</td>" +
"</tr>";

            string retorno = "&nbsp;";
            dExtrato dExtrato1 = new dExtrato();
            Session["dExtrato1"] = dExtrato1;
            dExtratoTableAdapters.SaldoContaCorrenteTableAdapter TA = new Extratos.dExtratoTableAdapters.SaldoContaCorrenteTableAdapter();
            dExtratoTableAdapters.ContaCorrenteDetalheTableAdapter TAdet = new Extratos.dExtratoTableAdapters.ContaCorrenteDetalheTableAdapter();
            if (EMP != 1)
            {
                TA.Connection.ConnectionString = TA.Connection.ConnectionString.Replace("Catalog=NEON", "Catalog=NEONSA");
                TAdet.Connection.ConnectionString = TAdet.Connection.ConnectionString.Replace("Catalog=NEON", "Catalog=NEONSA");
            }
            TA.Fill(dExtrato1.SaldoContaCorrente, SCCI, SCCF,CCT);                                        
            TAdet.Fill(dExtrato1.ContaCorrenteDetalhe, CCT, SCCI, SCCF);

            dExtrato.ContaCorrenteDetalheRow rowDETBase = null;
            foreach (dExtrato.ContaCorrenteDetalheRow rowDET in dExtrato1.ContaCorrenteDetalhe)
            {                
                if ((rowDETBase == null) || (rowDETBase.CCD != rowDET.CCD))
                {
                    rowDETBase = rowDET;
                    rowDET.calc_spanCCD = 1;
                }
                else
                {
                    rowDETBase.calc_spanCCD += 1;
                    rowDET.calc_spanCCD = 0;
                }
            }
            if (CheckBoxBOL.Checked)
            {
                dExtratoTableAdapters.FrancesinhaTableAdapter TAF = new dExtratoTableAdapters.FrancesinhaTableAdapter();
                if (EMP != 1)
                    TAF.Connection.ConnectionString = TAF.Connection.ConnectionString.Replace("Catalog=NEON", "Catalog=NEONSA");
                TAF.FillBySCC(dExtrato1.Francesinha, CCT,SCCI, SCCF);
            }
            retorno += AbreTabela;
            foreach (dExtrato.SaldoContaCorrenteRow SCCrow in dExtrato1.SaldoContaCorrente)
            {               
                dExtrato.ContaCorrenteDetalheRow[] CCDrows = SCCrow.GetContaCorrenteDetalheRows();
                bool TemCheque = false;
                bool TemBoletos = false;
                bool Pular = SomarPlus;
                foreach (dExtrato.ContaCorrenteDetalheRow CCDrow in CCDrows)
                {
                    if (!CCDrow.IsPAGValorNull())
                        TemCheque = true;
                    if (CCDrow.GetFrancesinhaRows().Length > 0)
                        TemBoletos = true;
                    if (CCDrow.IsCCDTipoLancamentoNull() || ((CCDrow.CCDTipoLancamento != 15) && (CCDrow.CCDTipoLancamento != 16)))
                        Pular = false;
                }
                if (Pular)
                    continue;
                switch (Modo)
                {
                    case TipoModo.cheque:
                        retorno += TemCheque ? LinhaCabecalho : LinhaCabecalhoSimples;
                        break;
                    case TipoModo.francesinha:
                        retorno += TemBoletos ? LinhaCabecalho : LinhaCabecalhoSimples;
                        break;
                    case TipoModo.extrato:
                    case TipoModo.mixto:
                        retorno += (TemBoletos || TemCheque) ? LinhaCabecalho : LinhaCabecalhoSimples;
                        break;
                }
                
                foreach (dExtrato.ContaCorrenteDetalheRow CCDrow in CCDrows)
                {
                    if (SomarPlus && !CCDrow.IsCCDTipoLancamentoNull() && ((CCDrow.CCDTipoLancamento == 15) || (CCDrow.CCDTipoLancamento == 16)))
                        continue;
                    switch (Modo)
                    {
                        case TipoModo.extrato:
                            if (CCDrow.calc_spanCCD != 0)
                                retorno += string.Format(LinhaLancamentoPuro,
                                                       0,
                                                       SCCrow.SCCData,
                                                       CCDrow.CCDDocumento,
                                                       CCDrow.CCDDescricao,
                                                       CCDrow.CCDValor);
                            break;
                        case TipoModo.francesinha:
                            if (CCDrow.calc_spanCCD != 0)
                                retorno += string.Format(LinhaLancamentoFran,
                                                       SCCrow.SCCData,
                                                       CCDrow.CCDDocumento,
                                                       CCDrow.CCDDescricao,
                                                       CCDrow.CCDValor,
                                                       ListaPartamentos(CCDrow),
                                                       GeraLink(CCDrow));
                            break;
                        case TipoModo.cheque:
                            //string Descritivo = "&nbsp;";
                            //if (!CCDrow.IsNOAServicoNull())
                            //{
                            //    string manobra = "";
                            //    if (CCDrow.NOA_PLA != "213000")
                            //        manobra = CCDrow.PLADescricao + " - ";
                            //    Descritivo = string.Format("{0}{1}<br />\r\n", manobra, CCDrow.NOAServico);                                 
                            //}
                            string LinhaLancamento = LinhaLancamentoCCDUnico;
                            if (CCDrow.calc_spanCCD > 1)
                                LinhaLancamento = LinhaLancamentoCCDCab;
                            else if (CCDrow.calc_spanCCD == 0)
                                LinhaLancamento = LinhaLancamentoCCDDup;
                            else if (CCDrow.IsPAGValorNull())
                                LinhaLancamento = LinhaLancamentoPuro;
                            retorno += string.Format(LinhaLancamento,
                            CCDrow.calc_spanCCD,
                            SCCrow.SCCData,
                            CCDrow.CCDDocumento,
                            CCDrow.CCDDescricao,
                            CCDrow.CCDValor,
                            CCDrow.IsCHEFavorecidoNull() ? "&nbsp;" : CCDrow.CHEFavorecido,
                            //CCDrow.IsCHEdescritivoNull() ? "&nbsp;" : CCDrow.CHEdescritivo);
                            Descricao(CCDrow),
                            CCDrow.IsPAGValorNull() ? 0 : CCDrow.PAGValor);
                            break;
                        case TipoModo.mixto:
                            //string Descritivo1 = "&nbsp;";
                            if (CCDrow.IsCHEFavorecidoNull() && CCDrow.IsNOAServicoNull())
                            {
                                if (CCDrow.GetFrancesinhaRows().Length == 0)
                                {
                                    retorno += string.Format(LinhaLancamentoPuro,
                                                               0,
                                                               SCCrow.SCCData,
                                                               CCDrow.CCDDocumento,
                                                               CCDrow.CCDDescricao,
                                                               CCDrow.CCDValor);
                                }
                                else
                                {
                                    retorno += string.Format(LinhaLancamentoFranmixto1,
                                                               SCCrow.SCCData,
                                                               CCDrow.CCDDocumento,
                                                               CCDrow.CCDDescricao,
                                                               CCDrow.CCDValor);
                                    retorno += string.Format(LinhaLancamentoFranmixto2, 
                                                               ListaPartamentos(CCDrow),
                                                               GeraLink(CCDrow));
                                }
                            }
                            else
                            {                                
                                if (CCDrow.calc_spanCCD != 0)
                                    retorno += string.Format(LinhaLancamentoChmixto1Cab,
                                        CCDrow.calc_spanCCD + 1,
                                        SCCrow.SCCData,
                                        CCDrow.CCDDocumento,
                                        CCDrow.CCDDescricao,
                                        CCDrow.CCDValor);                                
                                retorno += string.Format(LinhaLancamentoChmixto2,
                                                         CCDrow.IsCHEFavorecidoNull() ? "&nbsp;" : CCDrow.CHEFavorecido,                                    
                                                         Descricao(CCDrow),
                                                         CCDrow.PAGValor);
                            }
                            break;
                    }                    
                }
                retorno += string.Format(LinhaRodape, SCCrow.SCCData, SomarPlus ? SCCrow.SCCValorF + SCCrow.SCCValorApF : SCCrow.SCCValorF);                
            }
            retorno += FechaTabela;
            return retorno;
        }

        private string Descricao(dExtrato.ContaCorrenteDetalheRow CCDrow)
        {
            if (CCDrow.IsNOA_PLANull())
                return "&nbsp;";
            if ((CCDrow.NOA_PLA == "213000") || (CCDrow.NOAServico.ToUpper().Contains(CCDrow.PLADescricao.ToUpper())))
                return CCDrow.NOAServico;
            else
                return string.Format("{0} - {1}", CCDrow.PLADescricao ,CCDrow.NOAServico);
        }

        private string ListaPartamentos(dExtrato.ContaCorrenteDetalheRow CCDrow)
        {
            string retorno = "<table class=\"TabelasNeonOnLineSBFra\"><tr>";
            int i = 0;
            int colunas = 8;
            bool linhaUnica = true;
            foreach (dExtrato.FrancesinhaRow rowFra in CCDrow.GetFrancesinhaRows())
            {
                i++;
                
                string Bloco = (rowFra.BLOCodigo.ToUpper() == "SB") ? "" : string.Format("{0}&nbsp;", rowFra.BLOCodigo);
                retorno += string.Format("<td class=\"CelulasFra\">&nbsp;{0}{1}&nbsp;</td>", Bloco, rowFra.APTNumero);
                if (i == colunas)
                {
                    i = 0;
                    retorno += "</tr><tr>";
                    linhaUnica = false;
                }
            }
            if(linhaUnica)
                for(;i<colunas;i++)
                    retorno += string.Format("<td class=\"CelulasFraBR\">&nbsp;</td>");
            retorno += "</tr></table>";
            return retorno;
        }

        private string GeraLink(dExtrato.ContaCorrenteDetalheRow CCDrow)
        {
            if (CCDrow.GetFrancesinhaRows().Length == 0)
                return "";
            else
                return string.Format("<a class=\"Botao\" href=\"Francesinha.aspx?SCC={0}\">francesinha&gt;&gt;</a>", CCDrow.CCD_SCC);
        }

        private void PopulaDatas()
        {
            int xEMP = EMP;
            Panel1.Visible = true;
            Panel2.Visible = false;
            int CCT = int.Parse(RadioButtonList1.SelectedValue);
            CheckBoxPLUS.Visible = false;
            CheckBoxPLUS.Checked = ListaPlus.ContainsKey(CCT);
            if (ListaPlus.ContainsKey(CCT) && ListaPlus[CCT] == 237)
                CheckBoxPLUS.Visible = false;                                   
            Label1.Text += string.Format(" CCT = {0} ", RadioButtonList1.SelectedValue);
            SqlDataSourceDatas.SelectParameters["SCC_CCT"].DefaultValue = RadioButtonList1.SelectedValue;
            SqlDataSourceDatas.SelectParameters["DataCorte"].DefaultValue = DateTime.Now.AddDays(-90).ToString("yyyy-MM-dd");
            if (EMP != 1)
                SqlDataSourceDatas.ConnectionString = SqlDataSourceDatas.ConnectionString.Replace("Catalog=NEON", "Catalog=NEONSA");
            DropDownList2.Items.Clear();
            DropDownList2.DataBind();
            DropDownList1.Items.Clear();
            DropDownList1.DataBind();
            if (DropDownList1.Items.Count > 0)
            {
                DropDownList1.SelectedIndex = DropDownList1.Items.Count - 1;
                DropDownList2.SelectedIndex = (DropDownList2.Items.Count > 5) ? DropDownList2.Items.Count - 5 : 0;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void RadioButtonList1_SelectedIndexChanged(object sender, EventArgs e)
        {
            Label1.Text += "\r\nSelectedIndexChanged";
            Session["EMP"] = EMP;
            PopulaDatas();
            //SEMINF.Visible = true;
            //Button1.Visible = true;
            //CheckBoxCheque.Visible = true;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void Button1_Click(object sender, EventArgs e)
        {
            try
            {
                if ((DropDownList2.SelectedValue == null) || (DropDownList2.SelectedValue == ""))
                    return;
                Panel2.Visible = true;
                //Literal1.Text = string.Format("{0}", TabelaGerada2(DropDownList2.SelectedValue, DropDownList1.SelectedValue));
                int CCT = int.Parse(RadioButtonList1.SelectedValue);
                int SCCI = int.Parse(DropDownList2.SelectedValue);
                int SCCF = int.Parse(DropDownList1.SelectedValue);
                Literal1.Text = TabelasExtratos(CCT, SCCI, SCCF);
            }
            catch (Exception ex)
            {
                Label1.Text = ex.Message;
                Label3.Text = ex.Message;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void DropDownList2_SelectedIndexChanged(object sender, EventArgs e)
        {
            Panel2.Visible = false;            
        }
                
    }
}
