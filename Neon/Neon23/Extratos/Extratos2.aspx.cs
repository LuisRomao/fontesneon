using System;
using LoginMenu.NeonOnLine;

namespace Extratos
{
    public partial class Extratos2 : System.Web.UI.Page
    {

        private Credenciais Cred
        {
            get
            {
                Credenciais ret = (Credenciais)Session["Cred"];
                if (ret == null)
                    Response.Redirect("Login.aspx?Ex=1");
                return ret;
            }
        }

        private int CON 
        {
            get 
            {
                if (Cred != null)
                    return Cred.CON;
                else
                {                                        
                    dExtratoTableAdapters.QueriesTableAdapter QTA = new Extratos.dExtratoTableAdapters.QueriesTableAdapter();
                    object oCON = QTA.ScalarQuery(EMP, Request.QueryString["CODCON"]);
                    return (int)(oCON ?? 0);                    
                }
            }
        }

        private int EMP
        {
            get
            {
                int _EMP;
                if (Cred != null)
                    return (Cred.EMP==2) ? 3 : Cred.EMP;
                else
                {                    
                    int.TryParse(Request.QueryString["F"], out _EMP);
                    return _EMP;
                }
                Session["EMP"] = _EMP;
                return _EMP;
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            
            
            if (!IsPostBack)
            {                
                dExtrato dExtrato1 = new dExtrato();                
                dExtratoTableAdapters.ContaCorrenTeTableAdapter TACCT = new Extratos.dExtratoTableAdapters.ContaCorrenTeTableAdapter();
                int nContas = TACCT.FillByAtivas(dExtrato1.ContaCorrenTe, CON, EMP);
                foreach (dExtrato.ContaCorrenTeRow CCTrow in dExtrato1.ContaCorrenTe)
                    if ((CCTrow.IsCCTTituloNull()) || (CCTrow.CCTTitulo == ""))
                        CCTrow.CCTTitulo = "Conta Corrente";
                if (nContas > 0)
                {
                    RadioButtonList1.DataSourceID = null;
                    RadioButtonList1.DataSource = dExtrato1.ContaCorrenTe;
                    RadioButtonList1.DataBind();                    
                }                
            }
            
            
            
            //DataBind();
        }

        
        /*
        private string TabelasExtratos(int CCT, int SCCI, int SCCF)
        {
            string AbreTabela = "<table cellpadding=\"2\" align=\"left\"><tbody>";
            //string AbreTabela = "<table cellpadding=\"2\" width=\"\" >";
            string LinhaRodape = "<TR class=\"tabelas\" align=\"center\"><td align=\"left\" colspan=\"3\">SALDO - {0:dd/MM/yyyy}</td><td align=\"right\">{1:n2}</TD></TR><TR><TD>&nbsp;</TD></TR>";
            string FechaTabela = "</tbody></table>";
            //string FechaTabela = "</table>";
            string LinhaCabecalho =
" <TR class=\"tabelas\" align=\"middle\">" +
                //" <TR class=\"tabelas\">" +
                //" <TR class=\"tabelas\" align=\"center\">" +
                //" <TR align=\"center\">" +
"  <td>Data</td>" +
"  <td>Documento</td>" +
"  <td>Lanšamento</td>" +
"  <td>Valor</td>";
            if (CheckBoxCheque.Checked)
                LinhaCabecalho +=
"  <td>Nominal</td>" +
"  <td>Descritivo</td>";
            LinhaCabecalho +=
" </TR>";
            string LinhaLancamento =
"<TR class=\"tabelacelula12\" align=\"center\">" +
                //"<TR align=\"center\">" +
"<td>{0:dd/MM/yyyy}</td>" +
"<td>{1}</td>" +
"<td>{2}</td>" +
"<td align=\"right\">{3:n2}";
            if (CheckBoxCheque.Checked)
                LinhaLancamento +=
"<td>{4}</td>" +
"<td align=\"left\">{5}</td>";
            LinhaLancamento +=
"</TR>";
            string retorno = "&nbsp;";
            dExtrato dExtrato1 = new dExtrato();
            dExtratoTableAdapters.SaldoContaCorrenteTableAdapter TA = new Extratos.dExtratoTableAdapters.SaldoContaCorrenteTableAdapter();
            TA.Fill(dExtrato1.SaldoContaCorrente, EMP, SCCI, SCCF, CCT);
            dExtratoTableAdapters.ContaCorrenteDetalheTableAdapter TAdet = new Extratos.dExtratoTableAdapters.ContaCorrenteDetalheTableAdapter();
            TAdet.Fill(dExtrato1.ContaCorrenteDetalhe, EMP, CCT, SCCI, SCCF);
            retorno += AbreTabela;
            foreach (dExtrato.SaldoContaCorrenteRow SCCrow in dExtrato1.SaldoContaCorrente)
            {
                retorno += LinhaCabecalho;
                foreach (dExtrato.ContaCorrenteDetalheRow CCDrow in SCCrow.GetContaCorrenteDetalheRows())
                    retorno += string.Format(LinhaLancamento,
                        SCCrow.SCCData,
                        CCDrow.CCDDocumento,
                        CCDrow.CCDDescricao,
                        CCDrow.CCDValor,
                        CCDrow.IsCHEFavorecidoNull() ? "&nbsp;" : CCDrow.CHEFavorecido,
                        CCDrow.IsCHEdescritivoNull() ? "&nbsp;" : CCDrow.CHEdescritivo);
                retorno += string.Format(LinhaRodape, SCCrow.SCCData, SCCrow.SCCValorF);
            }
            retorno += FechaTabela;
            return retorno;
        }

        
        */
        
        
        private string TabelasExtratos(int CCT,int SCCI,int SCCF)
        {
            string AbreTabela = "<table class=\"TabelasNeonOnLine\" align=\"left\"><tbody>";
            //string AbreTabela = "<table cellpadding=\"2\" width=\"\" >";
            string LinhaRodape = "<TR class=\"Titulo\" align=\"center\"><td align=\"left\" colspan=\"3\">SALDO - {0:dd/MM/yyyy}</td><td align=\"right\">{1:n2}</TD></TR><TR><TD>&nbsp;</TD></TR>";
            string FechaTabela = "</tbody></table>";
            //string FechaTabela = "</table>";
            string LinhaCabecalho =
" <TR class=\"Titulo\" align=\"middle\">" +
"  <td>Data</td>" +
"  <td>Documento</td>" +
"  <td>Lanšamento</td>" +
"  <td>Valor</td>";
            if (CheckBoxCheque.Checked)
                LinhaCabecalho +=
"  <td>Nominal</td>" +
"  <td>Descritivo</td>";
            LinhaCabecalho +=
" </TR>";
            string LinhaLancamento =
"<TR class=\"Celulas\" align=\"center\">" +
//"<TR align=\"center\">" +
"<td>{0:dd/MM/yyyy}</td>" +
"<td>{1}</td>" +
"<td>{2}</td>" +
"<td align=\"right\">{3:n2}";
            if (CheckBoxCheque.Checked)
                LinhaLancamento +=
"<td>{4}</td>" +
"<td align=\"left\">{5}</td>";
            LinhaLancamento +=
"</TR>";
            string retorno = "&nbsp;";
            dExtrato dExtrato1 = new dExtrato();
            dExtratoTableAdapters.SaldoContaCorrenteTableAdapter TA = new Extratos.dExtratoTableAdapters.SaldoContaCorrenteTableAdapter();
            TA.Fill(dExtrato1.SaldoContaCorrente, EMP, SCCI, SCCF,CCT);
            dExtratoTableAdapters.ContaCorrenteDetalheTableAdapter TAdet = new Extratos.dExtratoTableAdapters.ContaCorrenteDetalheTableAdapter();
            TAdet.Fill(dExtrato1.ContaCorrenteDetalhe, EMP, CCT, SCCI, SCCF);
            retorno += AbreTabela;
            foreach (dExtrato.SaldoContaCorrenteRow SCCrow in dExtrato1.SaldoContaCorrente)
            {                
                retorno += LinhaCabecalho;             
                foreach (dExtrato.ContaCorrenteDetalheRow CCDrow in SCCrow.GetContaCorrenteDetalheRows())
                    retorno += string.Format(LinhaLancamento, 
                        SCCrow.SCCData, 
                        CCDrow.CCDDocumento, 
                        CCDrow.CCDDescricao, 
                        CCDrow.CCDValor, 
                        CCDrow.IsCHEFavorecidoNull() ? "&nbsp;" : CCDrow.CHEFavorecido,
                        CCDrow.IsCHEdescritivoNull() ? "&nbsp;" : CCDrow.CHEdescritivo);
                retorno += string.Format(LinhaRodape, SCCrow.SCCData, SCCrow.SCCValorF);                
            }
            retorno += FechaTabela;
            return retorno;
        }

        


        private void PopulaDatas()
        {
            int xEMP = EMP;
            Panel1.Visible = true;
            Panel2.Visible = false;
            Label1.Text += string.Format(" CCT = {0} ", RadioButtonList1.SelectedValue);
            SqlDataSourceDatas.SelectParameters["SCC_CCT"].DefaultValue = RadioButtonList1.SelectedValue;
            DropDownList2.Items.Clear();
            DropDownList2.DataBind();
            DropDownList1.Items.Clear();
            DropDownList1.DataBind();
            if (DropDownList1.Items.Count > 0)
            {
                DropDownList1.SelectedIndex = DropDownList1.Items.Count - 1;
                DropDownList2.SelectedIndex = (DropDownList2.Items.Count > 5) ? DropDownList2.Items.Count - 5 : 0;
            }
        }

        protected void RadioButtonList1_SelectedIndexChanged(object sender, EventArgs e)
        {
            Label1.Text += "\r\nSelectedIndexChanged";
            Session["EMP"] = EMP;
            PopulaDatas();
            //SEMINF.Visible = true;
            //Button1.Visible = true;
            //CheckBoxCheque.Visible = true;
        }

        protected void Button1_Click(object sender, EventArgs e)
        {
            try
            {
                if (DropDownList2.SelectedValue == null)
                    return;
                Panel2.Visible = true;
                //Literal1.Text = string.Format("{0}", TabelaGerada2(DropDownList2.SelectedValue, DropDownList1.SelectedValue));
                int CCT = int.Parse(RadioButtonList1.SelectedValue);
                int SCCI = int.Parse(DropDownList2.SelectedValue);
                int SCCF = int.Parse(DropDownList1.SelectedValue);
                Literal1.Text = TabelasExtratos(CCT, SCCI, SCCF);
            }
            catch 
            { 
            }
        }

        protected void DropDownList2_SelectedIndexChanged(object sender, EventArgs e)
        {
            Panel2.Visible = false;            
        }
        
    }
}
