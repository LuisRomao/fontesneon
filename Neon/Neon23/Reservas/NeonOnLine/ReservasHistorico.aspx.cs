using System;
using System.Data;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Drawing;
using RelSindicos.NeonOnLine;
using LoginMenu.NeonOnLine;

namespace Reservas.NeonOnLine
{
    public partial class ReservasHistorico : Page
    {
        private dMoradores dMor = new dMoradores();
        private dReservas dRes = new dReservas();

        private Credenciais Cred
        {
            get
            {
                return Credenciais.CredST(this, true);
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            //Seta EMP
            dMor.EMP = Cred.EMP;

            if (!IsPostBack)
            {
                //Mascaras de campos
                TPeriodoDataInicial.Attributes.Add("onkeyup", "javascript:formataData(this);");
                TPeriodoDataFinal.Attributes.Add("onkeyup", "javascript:formataData(this);");

                //Carrega tela 
                Carregar();
            }
        }

        private void Carregar()
        {
            //Seta visibilidade de campos adicionais
            if (Cred.TUsuario == TipoUsuario.Administradora)
            {
                CSite.SelectedIndex = -1;
                CSite.Enabled = CCondominio.Enabled = true;
            }
            else
            {
                CSite.SelectedValue = Cred.EMP.ToString();
                CSite.Enabled = CCondominio.Enabled = false;
            }

            //Seta EMP
            dMor.EMP = Convert.ToInt32(CSite.SelectedValue);

            //Carrega o campo de condominios
            dMor.EnforceConstraints = false;
            dMor.CONDOMINIOSTableAdapter.Fill(dMor.CONDOMINIOS, Convert.ToInt32(CSite.SelectedValue));
            CCondominio.DataSource = dMor.CONDOMINIOS;
            CCondominio.DataBind();
            if (Cred.TUsuario != TipoUsuario.Administradora)
                CCondominio.SelectedValue = Cred.CON.ToString();

            //Carrega o campo de blocos
            dMor.EnforceConstraints = false;
            dMor.BLOCOSTableAdapter.Fill(dMor.BLOCOS, Convert.ToInt32(CCondominio.SelectedValue));
            CBloco.DataSource = dMor.BLOCOS;
            CBloco.DataBind();

            //Carrega o campo de apartamentos
            dMor.EnforceConstraints = false;
            dMor.APARTAMENTOSTableAdapter.Fill(dMor.APARTAMENTOS, Convert.ToInt32(CBloco.SelectedValue));
            CApartamento.DataSource = dMor.APARTAMENTOS;
            CApartamento.DataBind();

            //Carrega o campo de equipamentos / espacos
            dRes.EquipamentosTableAdapter.FillCombo(dRes.EQuiPamentos, Convert.ToInt32(CSite.SelectedValue), Convert.ToInt32(CCondominio.SelectedValue));
            CEquipamento.DataSource = dRes.EQuiPamentos;
            CEquipamento.DataBind();

            //Posiciona no primeiro campo
            if (Cred.TUsuario == TipoUsuario.Administradora)
                CSite.Focus();
            else
                CBloco.Focus();
        }

        protected void CSite_SelectedIndexChanged(object sender, EventArgs e)
        {
            //Seta EMP
            dMor.EMP = Convert.ToInt32(CSite.SelectedValue);

            //Carrega o campo de condominios
            dMor.EnforceConstraints = false;
            dMor.CONDOMINIOSTableAdapter.Fill(dMor.CONDOMINIOS, Convert.ToInt32(CSite.SelectedValue));
            CCondominio.DataSource = dMor.CONDOMINIOS;
            CCondominio.DataBind();

            //Carrega o campo de blocos
            dMor.EnforceConstraints = false;
            dMor.BLOCOSTableAdapter.Fill(dMor.BLOCOS, Convert.ToInt32(CCondominio.SelectedValue));
            CBloco.DataSource = dMor.BLOCOS;
            CBloco.DataBind();

            //Carrega o campo de apartamentos
            dMor.EnforceConstraints = false;
            dMor.APARTAMENTOSTableAdapter.Fill(dMor.APARTAMENTOS, Convert.ToInt32(CBloco.SelectedValue));
            CApartamento.DataSource = dMor.APARTAMENTOS;
            CApartamento.DataBind();

            //Carrega o campo de equipamentos / espacos
            dRes.EquipamentosTableAdapter.FillCombo(dRes.EQuiPamentos, Convert.ToInt32(CSite.SelectedValue), Convert.ToInt32(CCondominio.SelectedValue));
            CEquipamento.DataSource = dRes.EQuiPamentos;
            CEquipamento.DataBind();

            //Posiciona no mesmo campo
            CSite.Focus();
        }

        protected void CCondominio_SelectedIndexChanged(object sender, EventArgs e)
        {
            //Seta EMP
            dMor.EMP = Convert.ToInt32(CSite.SelectedValue);

            //Carrega o campo de blocos
            dMor.EnforceConstraints = false;
            dMor.BLOCOSTableAdapter.Fill(dMor.BLOCOS, Convert.ToInt32(CCondominio.SelectedValue));
            CBloco.DataSource = dMor.BLOCOS;
            CBloco.DataBind();

            //Carrega o campo de apartamentos
            dMor.EnforceConstraints = false;
            dMor.APARTAMENTOSTableAdapter.Fill(dMor.APARTAMENTOS, Convert.ToInt32(CBloco.SelectedValue));
            CApartamento.DataSource = dMor.APARTAMENTOS;
            CApartamento.DataBind();

            //Carrega o campo de equipamentos / espacos
            dRes.EquipamentosTableAdapter.FillCombo(dRes.EQuiPamentos, Convert.ToInt32(CSite.SelectedValue), Convert.ToInt32(CCondominio.SelectedValue));
            CEquipamento.DataSource = dRes.EQuiPamentos;
            CEquipamento.DataBind();

            //Posiciona no mesmo campo
            CCondominio.Focus();
        }

        protected void CBloco_SelectedIndexChanged(object sender, EventArgs e)
        {
            //Seta EMP
            dMor.EMP = Convert.ToInt32(CSite.SelectedValue);

            //Carrega o campo de apartamentos
            dMor.EnforceConstraints = false;
            dMor.APARTAMENTOSTableAdapter.Fill(dMor.APARTAMENTOS, Convert.ToInt32(CBloco.SelectedValue));
            CApartamento.DataSource = dMor.APARTAMENTOS;
            CApartamento.DataBind();

            //Posiciona no mesmo campo
            CBloco.Focus();
        }

        protected void cmdBuscar_Click(object sender, EventArgs e)
        {
            //Verifica se setou o site e condominio
            if ((CSite.Text == "0") || (CCondominio.Text == "0"))
                return;

            //Pega os dados
            int intBLO = ((CBloco.SelectedIndex == 0) ? 0 : Convert.ToInt32(CBloco.SelectedValue));
            int intAPT = ((CApartamento.SelectedIndex == 0) ? 0 : Convert.ToInt32(CApartamento.SelectedValue));
            int intEQP = ((CEquipamento.SelectedIndex == 0) ? 0 : Convert.ToInt32(CEquipamento.SelectedValue));

            //Carrega tela com os historicos
            dReservas dRes = new dReservas();
            dRes.ReservaEquipamentoHistoricoTableAdapter.FillBusca(dRes.ReservaEquipamentoHistorico, Convert.ToInt32(CSite.SelectedValue), Convert.ToInt32(CCondominio.SelectedValue), Convert.ToInt32(intBLO), Convert.ToInt32(intAPT), Convert.ToInt32(intEQP), TPeriodoDataInicial.Text == "" ? Convert.ToDateTime("01/01/1800") : Convert.ToDateTime(TPeriodoDataInicial.Text), TPeriodoDataFinal.Text == "" ? Convert.ToDateTime("01/01/9000") : Convert.ToDateTime(TPeriodoDataFinal.Text));
            Repeater1.DataSource = dRes.ReservaEquipamentoHistorico;
            Repeater1.DataBind();
        }

        private void LimpaCampos()
        {
            //Limpa os campos
            CSite.SelectedIndex = 0;
            CCondominio.SelectedIndex = 0;
            CBloco.SelectedIndex = 0;
            CApartamento.SelectedIndex = 0;

            //Posiciona no primeiro campo
            CCondominio.Focus();
        }

        protected void cmdCancelar_Click(object sender, EventArgs e)
        {
            //Limpa os campos da tela
            LimpaCampos();
        }

        protected void TPeriodoDataInicial_TextChanged(object sender, EventArgs e)
        {
            //Verifica data 
            DateTime parsedDateTime;
            if (TPeriodoDataInicial.Text != "")
                if (!DateTime.TryParse(TPeriodoDataInicial.Text, out parsedDateTime))
                {
                    TPeriodoDataInicial.Text = "";
                    TPeriodoDataInicial.Focus();
                }
                else
                    TPeriodoDataInicial.Text = parsedDateTime.ToString("dd/MM/yyyy");

            //Ajusta selecao calendario
            CPeriodoDataInicial.SelectedDates.Clear();
            if (TPeriodoDataInicial.Text == "")
            {
                panPeriodoDataInicial.Visible = false;
                cmdPeriodoDataInicial.Text = ">>";
            }
            else
            {
                CPeriodoDataInicial.SelectedDate = Convert.ToDateTime(TPeriodoDataInicial.Text);
                CPeriodoDataInicial.VisibleDate = CPeriodoDataInicial.SelectedDate;      
            }
        }

        protected void cmdPeriodoDataInicial_Click(object sender, EventArgs e)
        {
            //Abre calendario na data atual ou na data selecionada
            panPeriodoDataInicial.Visible = !panPeriodoDataInicial.Visible;
            cmdPeriodoDataInicial.Text = panPeriodoDataInicial.Visible ? "<<" : ">>";

            //Ajusta selecao calendario
            if (TPeriodoDataInicial.Text == "")
                CPeriodoDataInicial.VisibleDate = System.DateTime.Today;
            else
                CPeriodoDataInicial.VisibleDate = CPeriodoDataInicial.SelectedDate = Convert.ToDateTime(TPeriodoDataInicial.Text);            
        }

        protected void CPeriodoDataInicial_SelectionChanged(object sender, EventArgs e)
        {
            //Seta a data selecionada
            TPeriodoDataInicial.Text = CPeriodoDataInicial.SelectedDate.ToString("dd/MM/yyyy");
            CPeriodoDataInicial.VisibleDate = CPeriodoDataInicial.SelectedDate;
        }

        protected void TPeriodoDataFinal_TextChanged(object sender, EventArgs e)
        {
            //Verifica data 
            DateTime parsedDateTime;
            if (TPeriodoDataFinal.Text != "")
                if (!DateTime.TryParse(TPeriodoDataFinal.Text, out parsedDateTime))
                {
                    TPeriodoDataFinal.Text = "";
                    TPeriodoDataFinal.Focus();
                }                  
                else
                    TPeriodoDataFinal.Text = parsedDateTime.ToString("dd/MM/yyyy");

            //Ajusta selecao calendario
            CPeriodoDataFinal.SelectedDates.Clear();
            if (TPeriodoDataFinal.Text == "")
            {
                panPeriodoDataFinal.Visible = false;
                cmdPeriodoDataFinal.Text = ">>";
            }
            else
            {
                CPeriodoDataFinal.SelectedDate = Convert.ToDateTime(TPeriodoDataFinal.Text);
                CPeriodoDataFinal.VisibleDate = CPeriodoDataFinal.SelectedDate;
            }
        }

        protected void cmdPeriodoDataFinal_Click(object sender, EventArgs e)
        {
            //Abre calendario na data atual ou na data selecionada
            panPeriodoDataFinal.Visible = !panPeriodoDataFinal.Visible;
            cmdPeriodoDataFinal.Text = panPeriodoDataFinal.Visible ? "<<" : ">>";

            //Ajusta selecao calendario
            if (TPeriodoDataFinal.Text == "")
                CPeriodoDataFinal.VisibleDate = System.DateTime.Today;
            else
                CPeriodoDataFinal.VisibleDate = CPeriodoDataFinal.SelectedDate = Convert.ToDateTime(TPeriodoDataFinal.Text);
        }

        protected void CPeriodoDataFinal_SelectionChanged(object sender, EventArgs e)
        {
            //Seta a data selecionada
            TPeriodoDataFinal.Text = CPeriodoDataFinal.SelectedDate.ToString("dd/MM/yyyy");
            CPeriodoDataFinal.VisibleDate = CPeriodoDataFinal.SelectedDate;
        }
    }
}
