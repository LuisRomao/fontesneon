using System;
using System.Data;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Drawing;
using RelSindicos.NeonOnLine;
using LoginMenu.NeonOnLine;

namespace Reservas.NeonOnLine
{
    public partial class ReservasBloqueioUnidade : Page
    {
        private dMoradores dMor = new dMoradores();
        private dReservas dRes = new dReservas();

        private Credenciais Cred
        {
            get
            {
                return Credenciais.CredST(this, true);
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            //Seta EMP
            dMor.EMP = Cred.EMP;

            if (!IsPostBack)
            {
                //Mascaras de campos
                TDataInicial.Attributes.Add("onkeyup", "javascript:formataData(this);");
                TDataFinal.Attributes.Add("onkeyup", "javascript:formataData(this);");

                //Carrega tela
                Carregar();
            }
        }

        private void Carregar()
        {
            //Carrega o campo de equipamentos / espacos
            dRes.EquipamentosTableAdapter.FillCombo(dRes.EQuiPamentos, Cred.EMP, Cred.CON);
            CEquipamento.DataSource = dRes.EQuiPamentos;
            CEquipamento.DataBind();

            //Carrega o campo de blocos
            dMor.EnforceConstraints = false;
            dMor.BLOCOSTableAdapter.Fill(dMor.BLOCOS, Cred.CON);
            CBloco.DataSource = dMor.BLOCOS;
            CBloco.DataBind();

            //Carrega o campo de apartamentos
            dMor.EnforceConstraints = false;
            dMor.APARTAMENTOSTableAdapter.Fill(dMor.APARTAMENTOS, Convert.ToInt32(CBloco.SelectedValue));
            CApartamento.DataSource = dMor.APARTAMENTOS;
            CApartamento.DataBind();

            //Carrega tela com os bloqueios
            dRes.ReservaBloqueioUnidadeTableAdapter.FillByEMP_CON(dRes.ReservaBloqueioUnidade, Cred.EMP, Cred.CON);
            Repeater1.DataSource = dRes.ReservaBloqueioUnidade;
            Repeater1.DataBind();

            //Posiciona no primeiro campo
            CEquipamento.Focus();
        }

        protected void CEquipamento_SelectedIndexChanged(object sender, EventArgs e)
        {
            //Limpa mensagem de erro
            LMsg.Text = "";
            LMsg.Visible = false;

            //Posiciona no mesmo campo
            CEquipamento.Focus();
        }

        protected void CBloco_SelectedIndexChanged(object sender, EventArgs e)
        {
            //Limpa mensagem de erro
            LMsg.Text = "";
            LMsg.Visible = false;

            //Carrega o campo de apartamentos
            dMor.EnforceConstraints = false;
            dMor.APARTAMENTOSTableAdapter.Fill(dMor.APARTAMENTOS, Convert.ToInt32(CBloco.SelectedValue));
            CApartamento.DataSource = dMor.APARTAMENTOS;
            CApartamento.DataBind();

            //Posiciona no mesmo campo
            CBloco.Focus();
        }

        protected void cmdAlterar_Click(object sender, EventArgs e)
        {
            //Limpa mensagem de erro
            LMsg.Text = "";
            LMsg.Visible = false;

            //Limpa os campos da tela
            LimpaCampos();

            //Altera o cadastro do bloqueio
            Button cmdAlterar = (Button)sender;
            int ID = int.Parse(cmdAlterar.CommandArgument);

            //Procura pelo registro selecionado para alterar
            dRes.ReservaBloqueioUnidadeTableAdapter.Fill(dRes.ReservaBloqueioUnidade);
            dReservas.ReservaBloqueioUnidadeRow rowBloqueio = dRes.ReservaBloqueioUnidade.FindByRBU(ID);

            //Preenche tela
            LID.Text = rowBloqueio.RBU.ToString();

            dRes.EquipamentosTableAdapter.FillCombo(dRes.EQuiPamentos, Cred.EMP, Cred.CON);
            CEquipamento.DataSource = dRes.EQuiPamentos;
            CEquipamento.DataBind();
            CEquipamento.SelectedValue = rowBloqueio.RBU_EQP.ToString();

            dMor.EnforceConstraints = false;
            dMor.BLOCOSTableAdapter.Fill(dMor.BLOCOS, Cred.CON);
            CBloco.DataSource = dMor.BLOCOS;
            CBloco.DataBind();
            CBloco.SelectedValue = rowBloqueio.RBU_BLO.ToString();

            dMor.EnforceConstraints = false;
            dMor.APARTAMENTOSTableAdapter.Fill(dMor.APARTAMENTOS, Convert.ToInt32(CBloco.SelectedValue));
            CApartamento.DataSource = dMor.APARTAMENTOS;
            CApartamento.DataBind();
            if (!rowBloqueio.IsRBU_APTNull()) CApartamento.SelectedValue = rowBloqueio.RBU_APT.ToString();

            if (!rowBloqueio.IsRBU_DataInicialNull()) TDataInicial.Text = rowBloqueio.RBU_DataInicial.ToShortDateString();
            if (!rowBloqueio.IsRBU_DataFinalNull()) TDataFinal.Text = rowBloqueio.RBU_DataFinal.ToShortDateString();
            if (!rowBloqueio.IsRBU_ObservacaoNull()) TObservacao.Text = rowBloqueio.RBU_Observacao;

            //Esconde calendarios
            panDataInicial.Visible = panDataFinal.Visible = false;
            cmdDataInicial.Text = cmdDataInicial.Text = ">>";

            //Seta botao como alterar
            cmdInserirAlterar.Text = "Alterar";

            //Posiciona no primeiro campo
            CEquipamento.Focus();
        }

        protected void cmdRemover_Click(object sender, EventArgs e)
        {
            //Limpa mensagem de erro
            LMsg.Text = "";
            LMsg.Visible = false;

            //Remove o cadastro do usuario da tabela
            Button cmdRemover = (Button)sender;
            int ID = int.Parse(cmdRemover.CommandArgument);

            //Apaga o registro selecionado para excluir
            dRes.ReservaBloqueioUnidadeTableAdapter.Delete(ID);

            //Recarrega a tela
            Carregar();

            //Limpa os campos da tela
            LimpaCampos();

            //Seta botao como inserir
            cmdInserirAlterar.Text = "Inserir";
        }

        private void LimpaCampos()
        {
            //Limpa os campos
            CEquipamento.SelectedIndex = 0;
            CBloco.SelectedIndex = 0;
            CApartamento.SelectedIndex = 0;
            TDataInicial.Text = "";
            TDataFinal.Text = "";
            TObservacao.Text = "";

            //Posiciona no primeiro campo
            CEquipamento.Focus();
        }

        protected void cmdCancelar_Click(object sender, EventArgs e)
        {
            //Limpa mensagem de erro
            LMsg.Text = "";
            LMsg.Visible = false;

            //Limpa os campos da tela
            LimpaCampos();

            //Seta botao como inserir
            cmdInserirAlterar.Text = "Inserir";
        }

        protected void cmdInserirAlterar_Click(object sender, EventArgs e)
        {
            //Limpa mensagem de erro
            LMsg.Text = "";
            LMsg.Visible = false;

            //Faz a consist�ncia dos campos
            if (CEquipamento.SelectedValue == "0")
            {
                LMsg.Text = "Informe o equipamento.";
                LMsg.Visible = true;
                return;
            } 
            if (CBloco.SelectedValue == "0")
            {
                LMsg.Text = "Informe o bloco.";
                LMsg.Visible = true;
                return;
            }
            if ((TDataInicial.Text == "" && TDataFinal.Text != "") || (TDataInicial.Text != "" && TDataFinal.Text == ""))
            {
                LMsg.Text = "Per�odo inv�lido. Indique a data inicial e a data final do bloqueio para um per�odo espec�fico ou deixe as duas datas em branco para bloqueio com prazo indefinido.";
                LMsg.Visible = true;
                return;
            }
            if (TDataInicial.Text != "" && TDataFinal.Text != "")
                if (Convert.ToDateTime(TDataInicial.Text) > Convert.ToDateTime(TDataFinal.Text))
                {
                    LMsg.Text = "Per�odo inv�lido. A data final deve ser posterior a data inicial.";
                    LMsg.Visible = true;
                    return;
                }

            //Verifica se existe reserva para a unidade do equipamento selecionado dentro do periodo indicado (apenas avisa apos cadastro do bloqueio)
            string strMsgAdicional = "";
            dReservasTableAdapters.ReservaEQuipamentoTableAdapter dReserva = new dReservasTableAdapters.ReservaEQuipamentoTableAdapter();
            if ((bool)dReserva.VerificaReservaUnidade(Cred.EMP, int.Parse(CEquipamento.SelectedValue), int.Parse(CBloco.SelectedValue), int.Parse(CApartamento.SelectedValue) <= 0 ? -1 : int.Parse(CApartamento.SelectedValue), TDataInicial.Text, TDataFinal.Text))
                strMsgAdicional = "<br/><br/>ATEN��O: J� existe reserva efetuada dentro do bloqueio cadastrado e a reserva ser� mantida.<br/>Novas tentativas de reserva dentro do bloqueio cadastrado n�o ser�o permitidas a partir deste momento.";

            //Insere ou Altera
            try
            {
                //Abre uma conexao
                dRes.ReservaBloqueioUnidadeTableAdapter.Connection.Open();
                if (cmdInserirAlterar.Text == "Inserir")
                {
                    //Insere o novo registro
                    dRes.ReservaBloqueioUnidadeTableAdapter.Insert(
                        Cred.EMP,
                        int.Parse(CEquipamento.SelectedValue),
                        int.Parse(CBloco.SelectedValue),
                        int.Parse(CApartamento.SelectedValue) <= 0 ? (int?)null : int.Parse(CApartamento.SelectedValue),
                        TDataInicial.Text == "" ? (DateTime?)null : Convert.ToDateTime(TDataInicial.Text),
                        TDataFinal.Text == "" ? (DateTime?)null : Convert.ToDateTime(TDataFinal.Text),
                        TObservacao.Text);

                    //Sucesso
                    LMsg.Text = "Bloqueio inserido com sucesso !" + strMsgAdicional;
                    LMsg.Visible = true;
                }
                else
                {
                    //Altera o registro
                    dRes.ReservaBloqueioUnidadeTableAdapter.Update(
                        Cred.EMP,
                        int.Parse(CEquipamento.SelectedValue),
                        int.Parse(CBloco.SelectedValue),
                        int.Parse(CApartamento.SelectedValue) <= 0 ? (int?)null : int.Parse(CApartamento.SelectedValue),
                        TDataInicial.Text == "" ? (DateTime?)null : Convert.ToDateTime(TDataInicial.Text),
                        TDataFinal.Text == "" ? (DateTime?)null : Convert.ToDateTime(TDataFinal.Text),
                        TObservacao.Text,
                        int.Parse(LID.Text));

                    //Sucesso
                    LMsg.Text = "Bloqueio alterado com sucesso !" + strMsgAdicional;
                    LMsg.Visible = true;
                }

                //Esconde calendarios
                panDataInicial.Visible = panDataFinal.Visible = false;
                cmdDataInicial.Text = cmdDataInicial.Text = ">>";
            }
            catch (Exception)
            {
                //Erro
                LMsg.Text = "Ocorreu um erro no cadastro do bloqueio, tente novamente.";
                LMsg.Visible = true;
                return;
            }
            finally
            {
                //Fecha a conexao
                dRes.ReservaBloqueioUnidadeTableAdapter.Connection.Close();
            }

            //Recarrega a tela
            Carregar();

            //Limpa os campos da tela
            LimpaCampos();

            //Seta botao como inserir
            cmdInserirAlterar.Text = "Inserir";
        }

        protected void TDataInicial_TextChanged(object sender, EventArgs e)
        {
            //Verifica data 
            DateTime parsedDateTime;
            if (TDataInicial.Text != "")
                if (!DateTime.TryParse(TDataInicial.Text, out parsedDateTime))
                {
                    TDataInicial.Text = "";
                    TDataInicial.Focus();
                }
                else
                    TDataInicial.Text = parsedDateTime.ToString("dd/MM/yyyy");

            //Ajusta selecao calendario
            CDataInicial.SelectedDates.Clear();
            if (TDataInicial.Text == "")
            {
                panDataInicial.Visible = false;
                cmdDataInicial.Text = ">>";
            }
            else
            {
                CDataInicial.SelectedDate = Convert.ToDateTime(TDataInicial.Text);
                CDataInicial.VisibleDate = CDataInicial.SelectedDate;
            }
        }

        protected void cmdDataInicial_Click(object sender, EventArgs e)
        {
            //Abre calendario na data atual ou na data selecionada
            panDataInicial.Visible = !panDataInicial.Visible;
            cmdDataInicial.Text = panDataInicial.Visible ? "<<" : ">>";

            //Ajusta selecao calendario
            if (TDataInicial.Text == "")
                CDataInicial.VisibleDate = System.DateTime.Today;
            else
                CDataInicial.VisibleDate = CDataInicial.SelectedDate = Convert.ToDateTime(TDataInicial.Text);
        }

        protected void CDataInicial_SelectionChanged(object sender, EventArgs e)
        {
            //Seta a data selecionada
            TDataInicial.Text = CDataInicial.SelectedDate.ToString("dd/MM/yyyy");
            CDataInicial.VisibleDate = CDataInicial.SelectedDate;
        }

        protected void TDataFinal_TextChanged(object sender, EventArgs e)
        {
            //Verifica data 
            DateTime parsedDateTime;
            if (TDataFinal.Text != "")
                if (!DateTime.TryParse(TDataFinal.Text, out parsedDateTime))
                {
                    TDataFinal.Text = "";
                    TDataFinal.Focus();
                }
                else
                    TDataFinal.Text = parsedDateTime.ToString("dd/MM/yyyy");

            //Ajusta selecao calendario
            CDataFinal.SelectedDates.Clear();
            if (TDataFinal.Text == "")
            {
                panDataFinal.Visible = false;
                cmdDataFinal.Text = ">>";
            }
            else
            {
                CDataFinal.SelectedDate = Convert.ToDateTime(TDataFinal.Text);
                CDataFinal.VisibleDate = CDataFinal.SelectedDate;
            }
        }

        protected void cmdDataFinal_Click(object sender, EventArgs e)
        {
            //Abre calendario na data atual ou na data selecionada
            panDataFinal.Visible = !panDataFinal.Visible;
            cmdDataFinal.Text = panDataFinal.Visible ? "<<" : ">>";

            //Ajusta selecao calendario
            if (TDataFinal.Text == "")
                CDataFinal.VisibleDate = System.DateTime.Today;
            else
                CDataFinal.VisibleDate = CDataFinal.SelectedDate = Convert.ToDateTime(TDataFinal.Text);
        }

        protected void CDataFinal_SelectionChanged(object sender, EventArgs e)
        {
            //Seta a data selecionada
            TDataFinal.Text = CDataFinal.SelectedDate.ToString("dd/MM/yyyy");
            CDataFinal.VisibleDate = CDataFinal.SelectedDate;
        }
    }
}
