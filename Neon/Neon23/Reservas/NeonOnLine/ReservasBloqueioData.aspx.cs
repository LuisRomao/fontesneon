using System;
using System.Data;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Drawing;
using RelSindicos.NeonOnLine;
using LoginMenu.NeonOnLine;

namespace Reservas.NeonOnLine
{
    public partial class ReservasBloqueioData : Page
    {
        private dReservas dRes = new dReservas();

        private Credenciais Cred
        {
            get
            {
                return Credenciais.CredST(this, true);
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                //Carrega tela
                Carregar();
            }
        }

        private void Carregar()
        {
            //Carrega o campo de equipamentos / espacos
            dRes.EquipamentosTableAdapter.FillCombo(dRes.EQuiPamentos, Cred.EMP, Cred.CON);
            CEquipamento.DataSource = dRes.EQuiPamentos;
            CEquipamento.DataBind();

            //Carrega tela com os bloqueios
            dRes.ReservaBloqueioDataTableAdapter.FillByEMP_CON(dRes.ReservaBloqueioData, Cred.EMP, Cred.CON);
            Repeater1.DataSource = dRes.ReservaBloqueioData;
            Repeater1.DataBind();

            //Posiciona no primeiro campo
            CEquipamento.Focus();
        }

        protected void CEquipamento_SelectedIndexChanged(object sender, EventArgs e)
        {
            //Limpa mensagem de erro
            LMsg.Text = "";
            LMsg.Visible = false;

            //Posiciona no mesmo campo
            CEquipamento.Focus();
        }

        protected void cmdAlterar_Click(object sender, EventArgs e)
        {
            //Limpa mensagem de erro
            LMsg.Text = "";
            LMsg.Visible = false;

            //Limpa os campos da tela
            LimpaCampos();

            //Altera o cadastro do bloqueio
            Button cmdAlterar = (Button)sender;
            int ID = int.Parse(cmdAlterar.CommandArgument);

            //Procura pelo registro selecionado para alterar
            dRes.ReservaBloqueioDataTableAdapter.Fill(dRes.ReservaBloqueioData);
            dReservas.ReservaBloqueioDataRow rowBloqueio = dRes.ReservaBloqueioData.FindByRBD(ID);

            //Preenche tela
            LID.Text = rowBloqueio.RBD.ToString();

            dRes.EquipamentosTableAdapter.FillCombo(dRes.EQuiPamentos, Cred.EMP, Cred.CON);
            CEquipamento.DataSource = dRes.EQuiPamentos;
            CEquipamento.DataBind();
            CEquipamento.SelectedValue = rowBloqueio.RBD_EQP.ToString();

            TDia.Text = rowBloqueio.RBD_Dia;
            TMes.Text = rowBloqueio.RBD_Mes;
            if (!rowBloqueio.IsRBD_AnoNull()) TAno.Text = rowBloqueio.RBD_Ano;
            if (!rowBloqueio.IsRBD_ObservacaoNull()) TObservacao.Text = rowBloqueio.RBD_Observacao;

            //Seta botao como alterar
            cmdInserirAlterar.Text = "Alterar";

            //Posiciona no primeiro campo
            CEquipamento.Focus();
        }

        protected void cmdRemover_Click(object sender, EventArgs e)
        {
            //Limpa mensagem de erro
            LMsg.Text = "";
            LMsg.Visible = false;

            //Remove o cadastro do usuario da tabela
            Button cmdRemover = (Button)sender;
            int ID = int.Parse(cmdRemover.CommandArgument);

            //Apaga o registro selecionado para excluir
            dRes.ReservaBloqueioDataTableAdapter.Delete(ID);

            //Recarrega a tela
            Carregar();

            //Limpa os campos da tela
            LimpaCampos();

            //Seta botao como inserir
            cmdInserirAlterar.Text = "Inserir";
        }

        private void LimpaCampos()
        {
            //Limpa os campos
            CEquipamento.SelectedIndex = 0;
            TDia.Text = "";
            TMes.Text = "";
            TAno.Text = "";
            TObservacao.Text = "";

            //Posiciona no primeiro campo
            CEquipamento.Focus();
        }

        protected void cmdCancelar_Click(object sender, EventArgs e)
        {
            //Limpa mensagem de erro
            LMsg.Text = "";
            LMsg.Visible = false;

            //Limpa os campos da tela
            LimpaCampos();

            //Seta botao como inserir
            cmdInserirAlterar.Text = "Inserir";
        }

        protected void cmdInserirAlterar_Click(object sender, EventArgs e)
        {
            //Limpa mensagem de erro
            LMsg.Text = "";
            LMsg.Visible = false;

            //Faz a consist�ncia dos campos
            if (CEquipamento.SelectedValue == "0")
            {
                LMsg.Text = "Informe o equipamento.";
                LMsg.Visible = true;
                return;
            } 
            if (TDia.Text == "" || (TMes.Text == ""))
            {
                LMsg.Text = "Data inv�lida. Indique pelo menos os campos referentes ao dia e m�s da data de bloqueio.";
                LMsg.Visible = true;
                return;
            }
            DateTime datDataBloqueio;
            string strDataBloqueio = TDia.Text.PadLeft(2, '0') + "/" + TMes.Text.PadLeft(2, '0') + "/" + (TAno.Text == "" ? "1996" : TAno.Text.PadLeft(4, '0'));
            if (!DateTime.TryParse(strDataBloqueio, out datDataBloqueio))
            {
                LMsg.Text = "Data inv�lida. Verifique se os campos indicados formam uma data v�lida.";
                LMsg.Visible = true;
                return;
            }

            //Verifica se existe reserva na data para o equipamento selecionado (n�o permite cadastrar bloqueio)
            dReservasTableAdapters.ReservaEQuipamentoTableAdapter dReserva = new dReservasTableAdapters.ReservaEQuipamentoTableAdapter();
            if ((bool)dReserva.VerificaReservaData(Cred.EMP, int.Parse(CEquipamento.SelectedValue), int.Parse(TDia.Text), int.Parse(TMes.Text), TAno.Text == "" ? 0 : int.Parse(TAno.Text)))
            {
                LMsg.Text = "ATEN��O: Este bloqueio n�o pode ser confirmado porque o espa�o j� possui reserva na data escolhida.";
                LMsg.Visible = true;
                return;
            }

            //Insere ou Altera
            try
            {
                //Abre uma conexao
                dRes.ReservaBloqueioDataTableAdapter.Connection.Open();
                if (cmdInserirAlterar.Text == "Inserir")
                {
                    //Insere o novo registro
                    dRes.ReservaBloqueioDataTableAdapter.Insert(
                        Cred.EMP,
                        int.Parse(CEquipamento.SelectedValue),
                        TDia.Text.PadLeft(2,'0'),
                        TMes.Text.PadLeft(2,'0'),
                        TAno.Text == "" ? null : TAno.Text.PadLeft(4,'0'),
                        TObservacao.Text);

                    //Sucesso
                    LMsg.Text = "Bloqueio inserido com sucesso !";
                    LMsg.Visible = true;
                }
                else
                {
                    //Altera o registro
                    dRes.ReservaBloqueioDataTableAdapter.Update(
                        Cred.EMP,
                        int.Parse(CEquipamento.SelectedValue),
                        TDia.Text.PadLeft(2,'0'),
                        TMes.Text.PadLeft(2,'0'),
                        TAno.Text == "" ? null : TAno.Text.PadLeft(4,'0'),
                        TObservacao.Text,
                        int.Parse(LID.Text));

                    //Sucesso
                    LMsg.Text = "Bloqueio alterado com sucesso !";
                    LMsg.Visible = true;
                }
            }
            catch (Exception)
            {
                //Erro
                LMsg.Text = "Ocorreu um erro no cadastro do bloqueio, tente novamente.";
                LMsg.Visible = true;
                return;
            }
            finally
            {
                //Fecha a conexao
                dRes.ReservaBloqueioDataTableAdapter.Connection.Close();
            }

            //Recarrega a tela
            Carregar();

            //Limpa os campos da tela
            LimpaCampos();

            //Seta botao como inserir
            cmdInserirAlterar.Text = "Inserir";
        }
    }
}
