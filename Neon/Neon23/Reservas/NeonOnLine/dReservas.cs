﻿namespace Reservas.NeonOnLine
{
    public partial class dReservas
    {
        public int EMP;

        private dReservasTableAdapters.EQuiPamentos1TableAdapter taEQuiPamentos1TableAdapter;
        /// <summary>
        /// TableAdapter padrão para tabela: EQuiPamentos1
        /// </summary>
        public dReservasTableAdapters.EQuiPamentos1TableAdapter EQuiPamentos1TableAdapter
        {
            get
            {
                if (taEQuiPamentos1TableAdapter == null)
                {
                    taEQuiPamentos1TableAdapter = new dReservasTableAdapters.EQuiPamentos1TableAdapter();
                    if (EMP == 3) taEQuiPamentos1TableAdapter.Connection.ConnectionString = taEQuiPamentos1TableAdapter.Connection.ConnectionString.Replace("Initial Catalog=NEON", "Initial Catalog=NEONSA");
                };
                return taEQuiPamentos1TableAdapter;
            }
        }

        private dReservasTableAdapters.EQuiPamentosTableAdapter EQuiPamentosTableAdapter;
        /// <summary>
        /// TableAdapter padrão para tabela: EQuiPamentos
        /// </summary>
        public dReservasTableAdapters.EQuiPamentosTableAdapter EquipamentosTableAdapter
        {
            get
            {
                if (EQuiPamentosTableAdapter == null)
                {
                    EQuiPamentosTableAdapter = new dReservasTableAdapters.EQuiPamentosTableAdapter();
                };
                return EQuiPamentosTableAdapter;
            }
        }

        private dReservasTableAdapters.ReservaEQuipamentoTableAdapter ReservaEQuipamentoTableAdapter;
        /// <summary>
        /// TableAdapter padrão para tabela: ReservaEQuiPamentos
        /// </summary>
        public dReservasTableAdapters.ReservaEQuipamentoTableAdapter ReservaEquipamentosTableAdapter
        {
            get
            {
                if (ReservaEQuipamentoTableAdapter == null)
                {
                    ReservaEQuipamentoTableAdapter = new dReservasTableAdapters.ReservaEQuipamentoTableAdapter();
                };
                return ReservaEQuipamentoTableAdapter;
            }
        }

        private dReservasTableAdapters.APARTAMENTOSReservaTableAdapter aPARTAMENTOSReservaTableAdapter;
        /// <summary>
        /// TableAdapter padrão para tabela: APARTAMENTOSReserva
        /// </summary>
        public dReservasTableAdapters.APARTAMENTOSReservaTableAdapter APARTAMENTOSReservaTableAdapter
        {
            get
            {
                if (aPARTAMENTOSReservaTableAdapter == null)
                {
                    aPARTAMENTOSReservaTableAdapter = new dReservasTableAdapters.APARTAMENTOSReservaTableAdapter();
                };
                return aPARTAMENTOSReservaTableAdapter;
            }
        }

        private dReservasTableAdapters.ReservaEquipamentoHistoricoTableAdapter reservaEquipamentoHistoricoTableAdapter;
        /// <summary>
        /// TableAdapter padrão para tabela: ReservaEquipamentoHistoricoTableAdapter
        /// </summary>
        public dReservasTableAdapters.ReservaEquipamentoHistoricoTableAdapter ReservaEquipamentoHistoricoTableAdapter
        {
            get
            {
                if (reservaEquipamentoHistoricoTableAdapter == null)
                {
                    reservaEquipamentoHistoricoTableAdapter = new dReservasTableAdapters.ReservaEquipamentoHistoricoTableAdapter();
                };
                return reservaEquipamentoHistoricoTableAdapter;
            }
        }

        private dReservasTableAdapters.ReservaBloqueioUnidadeTableAdapter reservaBloqueioUnidadeTableAdapter;
        /// <summary>
        /// TableAdapter padrão para tabela: ReservaBloqueioUnidadeTableAdapter
        /// </summary>
        public dReservasTableAdapters.ReservaBloqueioUnidadeTableAdapter ReservaBloqueioUnidadeTableAdapter
        {
            get
            {
                if (reservaBloqueioUnidadeTableAdapter == null)
                {
                    reservaBloqueioUnidadeTableAdapter = new dReservasTableAdapters.ReservaBloqueioUnidadeTableAdapter();
                };
                return reservaBloqueioUnidadeTableAdapter;
            }
        }


        private dReservasTableAdapters.ReservaBloqueioDataTableAdapter reservaBloqueioDataTableAdapter;
        /// <summary>
        /// TableAdapter padrão para tabela: ReservaBloqueioDataTableAdapter
        /// </summary>
        public dReservasTableAdapters.ReservaBloqueioDataTableAdapter ReservaBloqueioDataTableAdapter
        {
            get
            {
                if (reservaBloqueioDataTableAdapter == null)
                {
                    reservaBloqueioDataTableAdapter = new dReservasTableAdapters.ReservaBloqueioDataTableAdapter();
                };
                return reservaBloqueioDataTableAdapter;
            }
        }
    }
}
