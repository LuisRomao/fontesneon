using System;
using System.IO;
using System.Data;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Collections;
using System.Configuration;
using LoginMenu.NeonOnLine;
using LoginMenu.NeonOnLine.dNeon2TableAdapters;

namespace Reservas.NeonOnLine
{
    public partial class Reservas : Page
    {
        private Credenciais Cred
        {
           get
           {
              return Credenciais.CredST(this, true);
           }
        }

        private dReservas dRes = new dReservas();

        private string[] arrMeses = new string[13] { "", "JANEIRO", "FEVEREIRO", "MAR�O", "ABRIL", "MAIO", "JUNHO", "JULHO", "AGOSTO", "SETEMBRO", "OUTUBRO", "NOVEMBRO", "DEZEMBRO" };
        private ArrayList UsuariosTipoSindico
        {
            get { return new ArrayList(new string[] { "1", "12", "13", "20", "21", "22", "23" }); }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            //Seta EMP
            dRes.EMP = Cred.EMP;

            if (!IsPostBack)
            {
                //Inicia dados
                IniciaDados();

                //Carrega tela reservas 
                CarregaTelaReservas();

                //Posiciona no combo de escolha de equipamentos / espacos
                this.smnReservas.SetFocus(cmbEquipamentos);
            }
        }

        private void IniciaDados()
        {
            //Carrega o campo de equipamentos / espacos
            dRes.EquipamentosTableAdapter.FillCombo(dRes.EQuiPamentos, Cred.EMP, Cred.CON);
            cmbEquipamentos.DataSource = dRes.EQuiPamentos;
            cmbEquipamentos.DataBind();

            //Carrega o campo de blocos
            dRes.EnforceConstraints = false;
            dRes.APARTAMENTOSReservaTableAdapter.FillComboBloco(dRes.APARTAMENTOSReserva, Cred.EMP, Cred.CON);
            cmbBloco.DataSource = dRes.APARTAMENTOSReserva;
            cmbBloco.DataBind();

            //Seta valores iniciais para validacao
            rfvBloco.InitialValue = "0";
            rfvApartamento.InitialValue = "0";

            //Inicia Mes e Ano
            lblMes.Text = DateTime.Now.Month.ToString();
            lblAno.Text = DateTime.Now.Year.ToString();

            //Desabilita botao anterior e habilita proximo
            cmdAnterior.Enabled = false;
            cmdProximo.Enabled = true;

            //Inicia pans
            panTelaReservas.Enabled = true;
            panHorarios.Visible = true;
            panCampos.Visible = false;
            panConfirmacao.Visible = false;
            panReserva.Visible = false;
            panBloqueioObservacao.Visible = false;
            if (UsuariosTipoSindico.Contains(Cred.rowCli.tipousuario.ToString()))
            { 
               panComandos.Visible = false;
               panComandosSindico.Visible  = panReservasImpressao.Visible = true; 
            }
            else
            { 
               panComandos.Visible = true;
               panComandosSindico.Visible  = panReservasImpressao.Visible = false; 
            }

            //Verifica dados de impressao
            CarregaDadosImpressao();
            
            //Limpa o equipamento
            cmbEquipamentos.SelectedValue = "0";
        }

        private void CarregaTelaReservas()
        {
           //Carrega calendario 
           CarregaCalendario();

           //Carrega reservas do apartamento 
           CarregaReservasApartamento();

           //Posiciona no combo de escolha de equipamentos / espacos
           this.smnReservas.SetFocus(cmbEquipamentos);
        }

        private void CarregaCalendario()
        {
           //Inicia parametros do calendario
           dsCalendarioReservas.SelectParameters.Clear();
           dsCalendarioReservas.SelectParameters.Add("EMP", Cred.EMP.ToString());
           dsCalendarioReservas.SelectParameters.Add("EQP", cmbEquipamentos.SelectedValue);
           dsCalendarioReservas.SelectParameters.Add("Mes", lblMes.Text);
           dsCalendarioReservas.SelectParameters.Add("Ano", lblAno.Text);
           if (UsuariosTipoSindico.Contains(Cred.rowCli.tipousuario.ToString()))
           { dsCalendarioReservas.SelectParameters.Add("TIPO", "1"); }
           else
           { dsCalendarioReservas.SelectParameters.Add("TIPO", Cred.rowCli.tipousuario.ToString()); }
           dsCalendarioReservas.DataBind();
        }

        private void CarregaListaConvidados(String strREQ, String strAPT, String strEquipamento, String strData)
        {
           //Pega dados do APT
           string strUnidade = "N�o identificada";
           dRes.APARTAMENTOSReservaTableAdapter.Fill(dRes.APARTAMENTOSReserva, Convert.ToInt32(strAPT), Cred.EMP);
           dReservas.APARTAMENTOSReservaRow rowAPT = dRes.APARTAMENTOSReserva.FindByAPT_EMPAPT(Cred.EMP, Convert.ToInt32(strAPT));
           if (rowAPT != null)
               strUnidade = rowAPT.BLONome + " - " + rowAPT.APTNumero;

           //Pega dados do EQP
           dReservasTableAdapters.ReservaEQuipamentoTableAdapter dReserva = new dReservasTableAdapters.ReservaEQuipamentoTableAdapter();
           int intMaxConvidados = Convert.ToInt32(dReserva.VerificaMaxConvidados(Convert.ToInt32(strREQ), Cred.EMP));

           //Indica os dados da reserva da lista de convidados
           lblListaConvidadosREQ.Text = strREQ;
           lblListaConvidadosAPT.Text = strAPT;
           lblListaConvidadosUnidade.Text = strUnidade;
           lblListaConvidadosEquipamento.Text = strEquipamento;
           lblListaConvidadosData.Text = strData;
           lblListaConvidadosNumMax.Text = string.Format("{0} Convidados", intMaxConvidados.ToString());

           //Inicia parametros da lista de convidados
           dsListaConvidados.SelectParameters.Clear();
           dsListaConvidados.SelectParameters.Add("EMP", Cred.EMP.ToString());
           dsListaConvidados.SelectParameters.Add("REQ", strREQ);
           dsListaConvidados.SelectParameters.Add("APT", strAPT);
           dsListaConvidados.DataBind();

           //Limpa campos
           txtNomeConvidado.Text = "";

           //Seta no primeiro campo (limpo)
           this.smnReservas.SetFocus(txtNomeConvidado);
        }

        private void CarregaReservasApartamento()
        {
           //Verifica o APT
           if (Cred.rowCli.APT == 0)
           {
               dtReservasApartamento.Visible = false;
               return;
           }
           dtReservasApartamento.Visible = true;

           //Carrega as reservas do apartamento
           dsReservasApartamento.SelectParameters.Clear();
           dsReservasApartamento.SelectParameters.Add("EMP", Cred.EMP.ToString());
           dsReservasApartamento.SelectParameters.Add("APT", Cred.rowCli.APT.ToString());
           dsReservasApartamento.DataBind();
        }

        private void CarregaHorarios()
        {
           //Inicia parametros da grade de horarios
           dsCalendarioReservasHorarios.SelectParameters.Clear();
           dsCalendarioReservasHorarios.SelectParameters.Add("EMP", Cred.EMP.ToString());
           dsCalendarioReservasHorarios.SelectParameters.Add("EQP", cmbEquipamentos.SelectedValue);
           dsCalendarioReservasHorarios.SelectParameters.Add("Data", lblHorariosData.Text);
           dsCalendarioReservasHorarios.DataBind();

           //Verifica se trata ou nao bloqueio (se for sindico)
           if (UsuariosTipoSindico.Contains(Cred.rowCli.tipousuario.ToString()))
           {
              //Verifica se tem bloqueio para a data
              lblBloqueio.Text = "";
              dReservasTableAdapters.ReservaEQuipamentoTableAdapter dReserva = new dReservasTableAdapters.ReservaEQuipamentoTableAdapter();
              if ((bool)dReserva.VerificaBloqueio(Convert.ToDateTime(lblHorariosData.Text), Convert.ToInt32(cmbEquipamentos.SelectedValue), Cred.EMP))
                  lblBloqueio.Text = (string)dReserva.VerificaBloqueioTipo(Convert.ToDateTime(lblHorariosData.Text), Convert.ToInt32(cmbEquipamentos.SelectedValue), Cred.EMP);  
           }
        }

        private void CarregaRegras()
        {
           //Limpa campo de Regra selecionada
           lblRegraSelecionada.Text = "";
           lblEquipamentosSelecionados.Text = "";

           //Seta APT final (caso selecao por combo)
           if (cmbBloco.Visible)
              lblAPT.Text = cmbApartamento.SelectedValue;

           //Verifica se mostra lista de regras
           dReservasTableAdapters.ReservaEQuipamentoTableAdapter dReserva = new dReservasTableAdapters.ReservaEQuipamentoTableAdapter();
           panRegras.Visible = ((lblAPT.Text != "0") && ((bool)dReserva.VerificaAplicaRegras(Convert.ToInt32(lblEQP.Text), Cred.EMP)));

           //Inicia parametros da lista de regras
           dsRegras.SelectParameters.Clear();
           dsRegras.SelectParameters.Add("EMP", Cred.EMP.ToString());
           dsRegras.SelectParameters.Add("EQP", lblEQP.Text);
           dsRegras.SelectParameters.Add("Apt", lblAPT.Text);
           dsRegras.SelectParameters.Add("Data", lblData.Text.Substring(0, 10));
           dsRegras.SelectParameters.Add("Tipo", cmdConfirmar.Text != "Cancelar Reserva"?"R":"C");
           dsRegras.DataBind();
        }

        private void CarregaDadosImpressao()
        {
           //Carrega dados for sindico (botao visivel)
           if (panReservasImpressao.Visible)
           {
              //Verifica se imprime todos os equipamentos ou apenas o selecionado
              string strEQP = "";
              if (cmbEquipamentos.SelectedIndex == 0)
              {
                 foreach (ListItem item in cmbEquipamentos.Items)
                 {
                    if (strEQP != "") { strEQP = strEQP + ","; }
                    strEQP = strEQP + "[" + item.Value.ToString() + "]";
                 }
              }
              else
              { strEQP = "[" + cmbEquipamentos.SelectedValue.ToString() + "]"; }
              strEQP = strEQP.Replace("[0],", "");

              //Seta dados para impressao
              cmdReservasImpressao.Enabled = true;
              cmdReservasImpressao.OnClientClick = "javascript:window.open('ReservasImpressao.aspx?EMP=" + Cred.EMP + "&E=" + strEQP + "&M=" + lblMes.Text + "&A=" + lblAno.Text + "','','width=875,height=500,scrollbars=1,toolbar=0,location=0,top=100');";
           }
        }

        protected void dtCalendarioReservas_ItemDataBound(object sender, DataListItemEventArgs e)
        {
           //Verifica se e header ou item
           if (e.Item.ItemType == ListItemType.Header)
           {
              //Preenche o mes e ano
              Label lblMesAno = (Label)e.Item.FindControl("lblMesAno");
              lblMesAno.Text = arrMeses[Convert.ToInt16(lblMes.Text)] + " / " + lblAno.Text;
           }
        }

        protected void cmbEquipamentos_SelectedIndexChanged(object sender, EventArgs e)
        {
           //Carrega tela reservas 
           CarregaTelaReservas();

           //Verifica botoes anterior e proximo
           if (Convert.ToInt16(lblMes.Text) == DateTime.Now.Month) { cmdAnterior.Enabled = false; }
           else { cmdAnterior.Enabled = true; }
           cmdProximo.Enabled = true;

           //Verifica dados de impressao
           CarregaDadosImpressao();

           //Volta o foco no campo
           this.smnReservas.SetFocus(cmbEquipamentos);
        }

        protected void lblDiaSemana_Click(object sender, EventArgs e)
        {
           //Limpa campo de erro
           lblErroGravar.Text = "";
           panErroGravar.Visible = false;

           //Pega o dia clicado e o id associado a ele
           Button lblDiaSemana = (Button)sender;

           //Preenche a data
           lblHorariosData.Text = lblDiaSemana.Text.PadLeft(2, '0') + "/" + lblMes.Text.PadLeft(2, '0') + "/" + lblAno.Text;

           //Preenche os horarios
           CarregaHorarios();

           //Desabilida tela de reservas
           panTelaReservas.Enabled = false;

           //Mostra o popup da reserva exibindo os horarios
           panReserva.Visible = true;
           panHorarios.Visible = true;
           panCampos.Visible = false;
           panConfirmacao.Visible = false;
           panBloqueioObservacao.Visible = false;
        }

        protected void lblVisualizarReserva_Click(object sender, EventArgs e)
        {
           //Limpa campo de erro
           lblErroGravar.Text = "";
           panErroGravar.Visible = false;

           //Pega a reserva clicada e o id associado a ela
           Button lblReserva = (Button)sender;

           //Inicia bloco e apartamento
           txtBloco.Text = "";
           txtApartamento.Text = "";

           //Pega dados do APT
           dRes.EnforceConstraints = false;
           dRes.APARTAMENTOSReservaTableAdapter.Fill(dRes.APARTAMENTOSReserva, Convert.ToInt32(lblReserva.CommandArgument), Cred.EMP);
           dReservas.APARTAMENTOSReservaRow rowAPT = dRes.APARTAMENTOSReserva.FindByAPT_EMPAPT(Cred.EMP, Convert.ToInt32(lblReserva.CommandArgument));
           if (rowAPT != null)
           {
              txtApartamento.Text = rowAPT.APTNumero;
              txtBloco.Text = rowAPT.BLONome;
           }

           //Preenche os detalhes da reserva
           PreencheTelaReserva(lblReserva.ToolTip.Substring(0, 10), lblReserva.ToolTip.Substring(13, 18), lblReserva.ToolTip.Substring(34), lblReserva.CommandName, lblReserva.CommandArgument, txtBloco.Text, txtApartamento.Text);

           //Desabilida tela de reservas
           panTelaReservas.Enabled = false;

           //Mostra o popup da reserva exibindo os campos
           panReserva.Visible = true;
           panHorarios.Visible = false;
           panCampos.Visible = true;
           panConfirmacao.Visible = false;
           panBloqueioObservacao.Visible = false;
        }

        protected void lblVisualizarLista_Click(object sender, EventArgs e)
        {
            //Limpa campo de erro
            lblErroIncluir.Text = "";
            panErroIncluir.Visible = false;

            //Pega a reserva clicada e o id associado a ela
            Button lblReserva = (Button)sender;

            //Carrega a lista de convidados
            CarregaListaConvidados(lblReserva.CommandName, lblReserva.CommandArgument, lblReserva.ToolTip.Substring(34), lblReserva.ToolTip.Substring(0, 10));

            //Deixa tela de reservas invisivel 
            panTelaReservas.Visible = false;

            //Mostra tela com lista de convidados
            panListaConvidados.Visible = true;
        }

        protected void lblIncluirConvidado_Click(object sender, EventArgs e)
        {
            //Limpa campo de erro
            lblErroIncluir.Text = "";
            panErroIncluir.Visible = false;

            //Verifica se digitou nome convidado
            if (txtNomeConvidado.Text == "")
            {
                lblErroIncluir.Text = "Informe o nome do convidado a ser inclu�do na lista.";
                panErroIncluir.Visible = true;
                this.smnReservas.SetFocus(txtNomeConvidado);
                return;
            }

            //Verifica lista de convidados esgotada
            dReservasTableAdapters.ReservaEQuipamentoTableAdapter dReserva = new dReservasTableAdapters.ReservaEQuipamentoTableAdapter();
            if ((bool)dReserva.VerificaListaConvidadosEsgotada(Cred.EMP, Convert.ToInt32(lblListaConvidadosREQ.Text), Convert.ToInt32(lblListaConvidadosAPT.Text)))
            {
                lblErroIncluir.Text = "Voc� j� incluiu nesta lista de convidados a quantidade m�xima permitida para este espa�o.";
                panErroIncluir.Visible = true;
                return;
            }

            //Grava
            try
            {
                //Insere convidado na lista
                dReserva.ListaConvidadosIncluir(Convert.ToInt32(lblListaConvidadosREQ.Text), Convert.ToInt32(lblListaConvidadosAPT.Text), txtNomeConvidado.Text, Cred.EMP);

                //Atualiza lista
                CarregaListaConvidados(lblListaConvidadosREQ.Text, lblListaConvidadosAPT.Text, lblListaConvidadosEquipamento.Text, lblListaConvidadosData.Text);
            }
            catch (Exception)
            {
                //Erro
                lblErroIncluir.Text = "Erro ao incluir o convidado na lista. Por favor, tente novamente.";
                panErroIncluir.Visible = true;
            }
        }

        protected void lblRemoverConvidado_Click(object sender, EventArgs e)
        {
            //Limpa campo de erro
            lblErroIncluir.Text = "";
            panErroIncluir.Visible = false;

            //Pega o convidado clicado e o id associado a ele
            Button lblConvidado = (Button)sender;
            int intRLC = Convert.ToInt32(lblConvidado.CommandName);
            
            //Remove
            try
            {
                //Remove convidado na lista
                dReservasTableAdapters.ReservaEQuipamentoTableAdapter dReserva = new dReservasTableAdapters.ReservaEQuipamentoTableAdapter();
                dReserva.ListaConvidadosRemover(intRLC);

                //Atualiza lista
                CarregaListaConvidados(lblListaConvidadosREQ.Text, lblListaConvidadosAPT.Text, lblListaConvidadosEquipamento.Text, lblListaConvidadosData.Text);
            }
            catch (Exception)
            {
                //Erro
                lblErroIncluir.Text = "Erro ao remover o convidado da lista. Por favor, tente novamente.";
                panErroIncluir.Visible = true;
            }
        }

        protected void cmdEnviarLista_Click(object sender, EventArgs e)
        {
            //Limpa campo de erro
            lblErroIncluir.Text = "";
            panErroIncluir.Visible = false; 

            try
            {
                //Cria e-mail
                System.Net.Mail.SmtpClient SmtpClient1 = new System.Net.Mail.SmtpClient();

                //Verifica SMTP de dominio proprio
                VirMSSQL.TableAdapter TA = new VirMSSQL.TableAdapter();
                TA.Cone.ConnectionString = System.Configuration.ConfigurationManager.ConnectionStrings["ConnectionStringN2"].ConnectionString;
                System.Data.DataTable SMTP = TA.BuscaSQLTabela("SELECT CONSMTPHost, CONSMTPUser, CONSMTPPass, CONSMTPPort FROM CONDOMINIOS WHERE (CON = @P1) AND (CON_EMP = @P2) AND CONSMTPHost IS NOT NULL AND CONSMTPHost <> ''", Cred.CON, Cred.EMP);
                foreach (System.Data.DataRow DR in SMTP.Rows)
                {
                    SmtpClient1.Host = DR[0].ToString();
                    SmtpClient1.Credentials = new System.Net.NetworkCredential(DR[1].ToString(), DR[2].ToString());
                    SmtpClient1.Port = Convert.ToInt32(DR[3]);
                }

                using (System.Net.Mail.MailMessage email = new System.Net.Mail.MailMessage())
                {
                    //Bcc
                    string Bcc = "";
                    if (System.Configuration.ConfigurationManager.AppSettings["CopiarEmail" + Cred.EMP.ToString()] != null)
                        Bcc = System.Configuration.ConfigurationManager.AppSettings["CopiarEmail" + Cred.EMP.ToString()];
                    if (Bcc != "")
                        email.Bcc.Add(Bcc);

                    //From
                    string strFrom = "";
                    TA.BuscaEscalar("SELECT CONRemetentePadrao FROM CONDOMINIOS WHERE (CON = @P1) AND (CON_EMP = @P2) AND CONSMTPHost IS NOT NULL AND CONSMTPHost <> ''", out strFrom, Cred.CON, Cred.EMP);
                    if (strFrom != "")
                        email.From = new System.Net.Mail.MailAddress(strFrom);
                    else
                        if (Cred.EMP == 1)
                            email.From = new System.Net.Mail.MailAddress("neon@neonimoveis.com.br");
                        else
                            email.From = new System.Net.Mail.MailAddress("neonsa@neonimoveis.com.br");

                    //To
                    System.Data.DataTable DT = TA.BuscaSQLTabela("SELECT FCCEmail FROM FaleComCondominio WHERE (FCC_CON = @P1) AND (FCC_EMP = @P2) AND (FCCTipo = 6)", Cred.CON, Cred.EMP);
                    foreach (System.Data.DataRow DR in DT.Rows)
                        email.To.Add(DR[0].ToString());

                    //Verifica se nao tem destino e nao envia
                    if (email.To.Count == 0) 
                    {
                        //Erro
                        lblErroIncluir.Text = "Nenhum e-mail de destino para envio da lista foi encontrado. Por favor, entre em contato com o s�ndico do condom�nio e informe o ocorrido.";
                        panErroIncluir.Visible = true;
                        return;
                    }

                    //Inicia corpo do email
                    email.Subject = "Lista de Convidados (" + lblListaConvidadosUnidade.Text + " - " + lblListaConvidadosEquipamento.Text + " - " + lblListaConvidadosData.Text + ")";
                    email.IsBodyHtml = true;
                    email.Body = string.Format("<html>\r\n" +
                                               "   <head>\r\n" +
                                               "   </head>\r\n" +
                                               "   <body>\r\n" +
                                               "      <p><b>LISTA DE CONVIDADOS</b></p>\r\n" +
                                               "      <p><b>Unidade: </b>{0}</p>\r\n" +
                                               "      <p><b>Espa�o: </b>{1}<br>\r\n" +
                                               "      <b>Data: </b>{2}</p>\r\n" +
                                               "      <p><b>NOMES</b></p>\r\n" +
                                               "      <p>",
                                               lblListaConvidadosUnidade.Text, lblListaConvidadosEquipamento.Text, lblListaConvidadosData.Text);

                    //Inclui nomes no corpo do email
                    foreach (DataListItem convidado in dtListaConvidados.Items)
                        email.Body += ((Label)convidado.FindControl("lblNome")).Text + "<br>\r\n";
                    
                    //Fecha corpo do email
                    email.Body += string.Format("   </p>\r\n" +
                                                "   </body>\r\n" +
                                                "</html>");

                    //Cria a lista em excel e anexa se criou com sucesso
                    try
                    {
                        //Cria o excel
                        StringWriter writer = new StringWriter();
                        HtmlTextWriter html = new HtmlTextWriter(writer);
                        html.WriteLine("LISTA DE CONVIDADOS");
                        html.WriteLine("");
                        html.WriteLine("UNIDADE: " + lblListaConvidadosUnidade.Text);
                        html.WriteLine("");
                        html.WriteLine("ESPA�O: " + lblListaConvidadosEquipamento.Text);
                        html.WriteLine("DATA: " + lblListaConvidadosData.Text);
                        html.WriteLine("");
                        html.WriteLine("NOMES");
                        html.WriteLine("");
                        foreach (DataListItem convidado in dtListaConvidados.Items)
                            html.WriteLine(((Label)convidado.FindControl("lblNome")).Text);
                        System.Text.Encoding Enc = System.Text.Encoding.Default;
                        byte[] mBArray = Enc.GetBytes(writer.ToString());
                        System.IO.MemoryStream stream = new System.IO.MemoryStream(mBArray, false);

                        //Anexa o excel
                        System.Net.Mail.Attachment attach = new System.Net.Mail.Attachment(stream, "ListaConvidados_" + lblListaConvidadosUnidade.Text + "_" + lblListaConvidadosEquipamento.Text + "_" + lblListaConvidadosData.Text.Replace("/", "-") + ".xls", "application/vnd.ms-excel");
                        email.Attachments.Add(attach);
                    }
                    catch (Exception)
                    { }

                    //Envia email
                    SmtpClient1.Send(email);

                    //Enviou
                    ScriptManager.RegisterStartupScript(this, GetType(),"ServerControlScript", "alert(\"Lista de Convidados enviada com sucesso!!\");", true);
                }
            }
            catch (Exception)
            {
                ScriptManager.RegisterStartupScript(this, GetType(), "ServerControlScript", "alert(\"Ocorreu um erro no envio da Lista de Convidados. Por favor, tente novamente.\");", true);
            }
        }

        protected void cmdFecharLista_Click(object sender, EventArgs e)
        {
            //Volta pans
            panTelaReservas.Visible = true;
            panListaConvidados.Visible = false;
        }

        protected void cmdVoltar_Click(object sender, EventArgs e)
        {
           //Volta pans
           panTelaReservas.Enabled = true;
           panReserva.Visible = false;
           panBloqueioObservacao.Visible = false;
           this.smnReservas.SetFocus(cmbEquipamentos);
        }

        protected void lblHorario_Click(object sender, EventArgs e)
        {
           //Limpa campo de erro
           lblErroGravar.Text = "";
           panErroGravar.Visible = false;

           //Pega o horario clicado e o id associado a ele
           Button lblHorario = (Button)sender;

           //Inicia bloco e apartamento
           txtBloco.Text = "";
           txtApartamento.Text = "";

           //Pega dados do APT
           if (lblHorario.CommandArgument != "0")
           {
              dRes.EnforceConstraints = false;
              dRes.APARTAMENTOSReservaTableAdapter.Fill(dRes.APARTAMENTOSReserva, Convert.ToInt32(lblHorario.CommandArgument), Cred.EMP);
              dReservas.APARTAMENTOSReservaRow rowAPT = dRes.APARTAMENTOSReserva.FindByAPT_EMPAPT(Cred.EMP, Convert.ToInt32(lblHorario.CommandArgument));
              if (rowAPT != null)
              {
                 txtApartamento.Text = rowAPT.APTNumero;
                 txtBloco.Text = rowAPT.BLONome;
              }
           }

           //Preenche os detalhes da reserva
           PreencheTelaReserva(lblHorariosData.Text, lblHorario.ToolTip, cmbEquipamentos.SelectedItem.Text, lblHorario.CommandName, lblHorario.CommandArgument, txtBloco.Text, txtApartamento.Text);

           //Esconde horarios e mostra os campos
           panHorarios.Visible = false;
           panCampos.Visible = true;
           panConfirmacao.Visible = false;
        }

        private void SetaStatusUnidade()
        {
            //Verifica se nao setou a unidade
            string strBloco = txtBloco.Text != "" ? txtBloco.Text : (cmbBloco.SelectedItem.Text != "" ? cmbBloco.SelectedItem.Text : "");
            string strApartamento = txtApartamento.Text != "" ? txtApartamento.Text : (cmbApartamento.SelectedItem.Text != "" ? cmbApartamento.SelectedItem.Text : "");
            if (strApartamento == "")
                return;

            //Exibe o campo apenas para o sindico
            if (Cred.rowCli.tipousuario == 1)
                panStatus.Visible = true;

            //Seta adimplente
            lblStatus.Text = "Adimplente";
            lblStatus.ForeColor = System.Drawing.Color.FromName("#08225a");

            //Verifica Inadimplencia
            dReservasTableAdapters.BOLetosTableAdapter dBol = new dReservasTableAdapters.BOLetosTableAdapter();
            if (Cred.EMP == 3) dBol.Connection.ConnectionString = dBol.Connection.ConnectionString.Replace("Initial Catalog=NEON", "Initial Catalog=NEONSA");
            if ((int)dBol.VerificaInadimplencia(Convert.ToInt32(lblAPT.Text)) > 0)
            {
                lblStatus.Text = "Inadimplente";
                lblStatus.ForeColor = System.Drawing.Color.Red;
                return;
            }

            //Verifica Acordo
            dReservasTableAdapters.ACOrdosTableAdapter dAco = new dReservasTableAdapters.ACOrdosTableAdapter();
            if (Cred.EMP == 3) dAco.Connection.ConnectionString = dAco.Connection.ConnectionString.Replace("Initial Catalog=NEON", "Initial Catalog=NEONSA");
            if ((int)dAco.VerificaAcordo(Convert.ToInt32(lblAPT.Text)) > 0)
            {
                lblStatus.Text = "Inadimplente (Acordo)";
                lblStatus.ForeColor = System.Drawing.Color.Red;
                return;
            }
        }

        protected void PreencheTelaReserva(String strData, String strHorario, String strEquipamento, String strREQ, String strAPT, String strAPTBloco, String strAPTNumero)
        {
           //Seta o id da reserva a ser alterada e id do apartamento
           lblREQ.Text = strREQ;
           lblAPT.Text = strAPT;

           //Seta dados da reserva
           dReservasTableAdapters.ReservaEQuipamentoTableAdapter dReserva = new dReservasTableAdapters.ReservaEQuipamentoTableAdapter();
           DateTime datDataReserva = Convert.ToDateTime(dReserva.VerificaDataReserva(Convert.ToInt32(lblREQ.Text), Cred.EMP));
           lblDataHoraReservaEfetuada.Text = dReserva.VerificaDataHoraReservaEfetuada(Convert.ToInt32(lblREQ.Text), Cred.EMP).ToString();
           if (lblDataHoraReservaEfetuada.Text == "") lblDataHoraReservaEfetuada.Text = DateTime.Now.ToString();
           lblEQP.Text = ((int)dReserva.VerificaEQP(Convert.ToInt32(lblREQ.Text), Cred.EMP)).ToString();
           lblEquipamento.Text = strEquipamento;
           lblData.Text = strData + " - " + strHorario;
           chkTermos.Checked = false;

           //Preenche combo de blocos e apartamentos
           cmbBloco.SelectedValue = cmbBloco.Items.FindByText(strAPTBloco).Value;
           dRes.EnforceConstraints = false;
           dRes.APARTAMENTOSReservaTableAdapter.FillComboApartamento(dRes.APARTAMENTOSReserva, Convert.ToInt32(cmbBloco.SelectedValue), Cred.EMP);
           cmbApartamento.DataSource = dRes.APARTAMENTOSReserva;
           cmbApartamento.DataBind();
           cmbApartamento.SelectedValue = cmbApartamento.Items.FindByText(strAPTNumero).Value;

           //Preenche valor
           lblValor.Text = Convert.ToString(dReserva.VerificaValor(Convert.ToInt32(strREQ), Cred.EMP)).Replace(".", ",").Replace(",00%", "%");

           //Preenche informacoes da lista de convidados
           panListaConvidadosMsg.Visible = false;
           int intMaxConvidados = Convert.ToInt32(dReserva.VerificaMaxConvidados(Convert.ToInt32(strREQ), Cred.EMP));
           if (intMaxConvidados > 0)
           {
               panListaConvidadosMsg.Visible = true;
               lblListaConvidadosMax.Text = string.Format("M�ximo de {0} Convidados", intMaxConvidados.ToString());
           }

           //Preenche prazo minimo da reserva
           panPrazoMinimoMsg.Visible = false;
           DateTime datPrazoMinimo = Convert.ToDateTime(dRes.EQuiPamentos1TableAdapter.VerificaPrazoMinimo(datDataReserva, Convert.ToInt32(lblEQP.Text)));
           if (datPrazoMinimo != datDataReserva)
           {
               panPrazoMinimoMsg.Visible = true;
               lblPrazoMinimo.Text = "Reserva permitida at� " + datPrazoMinimo.ToString().Replace(" ", " �s ").Replace(":00.000", "");
               if (datPrazoMinimo < DateTime.Now) { lblPrazoMinimo.ForeColor = System.Drawing.Color.Red; }
               else { lblPrazoMinimo.ForeColor = System.Drawing.Color.FromName("#08225a"); }
            }

           //Preenche prazo de cancelamento
           DateTime datPrazoCancelamento = Convert.ToDateTime(dRes.EQuiPamentos1TableAdapter.VerificaPrazoCancelamento(Convert.ToInt32(lblEQP.Text), datDataReserva.ToString(), lblDataHoraReservaEfetuada.Text));
           if (datPrazoCancelamento > datDataReserva)
           {
               datPrazoCancelamento = Convert.ToDateTime(lblDataHoraReservaEfetuada.Text).AddSeconds(-10);
               lblPrazoCancelamento.Text = "O cancelamento desta reserva n�o ser� permitido (reserva fora do prazo de cancelamento)";
           }
           else 
               lblPrazoCancelamento.Text = "Cancelamento permitido at� " + datPrazoCancelamento.ToString().Replace(" ", " �s ").Replace(":00.000", "");
           if (datPrazoCancelamento > datDataReserva) { lblPrazoCancelamento.ForeColor = System.Drawing.Color.Red; }
           else if (datPrazoCancelamento < DateTime.Now) { lblPrazoCancelamento.ForeColor = System.Drawing.Color.Red; }
           else { lblPrazoCancelamento.ForeColor = System.Drawing.Color.FromName("#08225a"); }
               
           //Seta o arquivo de regras
           string strArquivo = string.Format(@"RegrasReserva_EQP_{0}_{1}", lblEQP.Text, Cred.EMP);
           cmdTermos.OnClientClick = "javascript:window.open('pdfdoc.aspx?ARQUIVO=" + strArquivo + "','_blank');";

           //Verifica a condicao da reserva
           panUnidade.Visible = true;
           panTermos.Visible = true;
           txtBloco.Visible = txtApartamento.Visible = true;
           cmbBloco.Visible = cmbApartamento.Visible = false;
           rfvBloco.Enabled = rfvApartamento.Enabled = false;
           cmdConfirmar.Enabled = false;
           this.smnReservas.SetFocus(cmdConfirmar);
           if (UsuariosTipoSindico.Contains(Cred.rowCli.tipousuario.ToString()))
           {
              //Sindico
              if (strAPT == "0")
              {
                 lblReservaMsg.Text = "A reserva ser� feita para a unidade indicada abaixo.";
                 cmdConfirmar.Text = "Reservar";
                 txtBloco.Visible = txtApartamento.Visible = false;
                 cmbBloco.Visible = cmbApartamento.Visible = true;
                 rfvBloco.Enabled = rfvApartamento.Enabled = true;
                 this.smnReservas.SetFocus(cmbBloco);
              }
              else
              {
                 lblReservaMsg.Text = "A reserva est� efetuada para a unidade indicada abaixo.";
                 panTermos.Visible = false;
                 cmdConfirmar.Text = "Cancelar Reserva";
                 cmdConfirmar.Enabled = true;
              }
           }
           else
           {
              //Condomino
              if (strAPT == "0")
              {
                 lblReservaMsg.Text = "A reserva ser� feita para a sua unidade.";
                 txtBloco.Text = dRes.APARTAMENTOSReservaTableAdapter.VerificaBLONome(Cred.EMP, Cred.CON, Cred.rowCli.bloco, Cred.rowCli.Apartamento).ToString();
                 txtApartamento.Text = Cred.rowCli.Apartamento;
                 lblAPT.Text = Cred.rowCli.APT.ToString();
                 cmdConfirmar.Text = "Reservar";
              }
              else if (dRes.APARTAMENTOSReservaTableAdapter.VerificaBLOCodigo(Cred.EMP, Cred.CON, strAPTBloco, strAPTNumero).ToString() == Cred.rowCli.bloco && strAPTNumero == Cred.rowCli.Apartamento)
              {
                 lblReservaMsg.Text = "A reserva est� efetuada para a sua unidade.";
                 txtBloco.Text = dRes.APARTAMENTOSReservaTableAdapter.VerificaBLONome(Cred.EMP, Cred.CON, Cred.rowCli.bloco, Cred.rowCli.Apartamento).ToString();
                 txtApartamento.Text = Cred.rowCli.Apartamento;
                 panTermos.Visible = false;
                 cmdConfirmar.Text = "Cancelar Reserva";
                 if (lblPrazoCancelamento.ForeColor == System.Drawing.Color.Red) { cmdConfirmar.Enabled = false; }
                 else { cmdConfirmar.Enabled = true; }
              }
              else
              {
                 lblReservaMsg.Text = "�rea comum j� reservada na data escolhida.";
                 panUnidade.Visible = false;
                 panTermos.Visible = false;
                 cmdConfirmar.Text = "Reservar";
                 cmdConfirmar.Enabled = false;
                 this.smnReservas.SetFocus(cmdCancelar);
              }
           }

           //Verifica se reserva ou cancela
           lblRegras.Text = cmdConfirmar.Text != "Cancelar Reserva" ? "RESERVAS PERMITIDAS" : "CANCELAMENTOS PERMITIDOS";

           //Carrega as regras (para reserva ou cancelamento)
           CarregaRegras();

           //Verifica status unidade (apenas sindico)
           panStatus.Visible = false;
           SetaStatusUnidade();
        }

        protected void cmbBloco_SelectedIndexChanged(object sender, EventArgs e)
        {
           //Limpa campo de erro
           lblErroGravar.Text = "";
           panErroGravar.Visible = false;

           //Carrega o campo de apartamentos
           dRes.EnforceConstraints = false;
           dRes.APARTAMENTOSReservaTableAdapter.FillComboApartamento(dRes.APARTAMENTOSReserva, Convert.ToInt32(cmbBloco.SelectedValue), Cred.EMP);
           cmbApartamento.DataSource = dRes.APARTAMENTOSReserva;
           cmbApartamento.DataBind();
           cmbApartamento.SelectedIndex = 0;

           //Limpa as regras (para reserva ou cancelamento)
           CarregaRegras();

           //Verifica status unidade (apenas sindico)
           panStatus.Visible = false;
           SetaStatusUnidade();

           //Volta o foco no campo
           this.smnReservas.SetFocus(cmbBloco);            
        }

        protected void cmbApartamento_SelectedIndexChanged(object sender, EventArgs e)
        {
           //Limpa campo de erro
           lblErroGravar.Text = "";
           panErroGravar.Visible = false;

           //Carrega as regras (para reserva ou cancelamento)
           CarregaRegras();

           //Verifica status unidade (apenas sindico)
           panStatus.Visible = false;
           SetaStatusUnidade();

           //Volta o foco no campo
           this.smnReservas.SetFocus(cmbApartamento);
        }

        protected void rdbRegra_CheckedChanged(object sender, EventArgs e)
        {
           //Limpa campo de erro
           lblErroGravar.Text = "";
           panErroGravar.Visible = false;

           //Pega a regra clicada
           RadioButton rdbRegra = (RadioButton)sender;

           //Procura pelo item da opcao selecionada para selecionar apenas ele mesmo
           foreach (DataListItem Item in dtRegras.Items)
           {
              //Pega a regra da busca e verifica se e a selecionada
              RadioButton rdbRegraBusca = (RadioButton)Item.FindControl("rdbRegra");
              if (rdbRegraBusca.Equals(rdbRegra))
              {
                 //Pega os Equipamentos da Regra selecionada
                 Label lblEquipamentosRegra = (Label)Item.FindControl("lblEquipamentosRegra");
                 lblRegraSelecionada.Text = lblEquipamentosRegra.Text;
                 lblEquipamentosSelecionados.Text = rdbRegraBusca.Text;
              }
              else
              {
                 //Seta o campo como nao selecionado
                 rdbRegraBusca.Checked = false;
              }
           }

           //Volta o foco no campo
           this.smnReservas.SetFocus(rdbRegra);
        }

        protected void chkTermos_CheckedChanged(object sender, EventArgs e)
        {
           //Limpa campo de erro
           lblErroGravar.Text = "";
           panErroGravar.Visible = false;

           //Habilita ou nao o botao de enviar
           cmdConfirmar.Enabled = chkTermos.Checked;
           if (cmdConfirmar.Enabled) { this.smnReservas.SetFocus(cmdConfirmar); }
        }

        protected void cmdCancelar_Click(object sender, EventArgs e)
        {
           //Volta pans
           panTelaReservas.Enabled = true;
           panReserva.Visible = false;
           panBloqueioObservacao.Visible = false;
        }

        protected void cmdConfirmar_Click(object sender, EventArgs e)
        {
           //Limpa campo de erro
           lblErroGravar.Text = "";
           panErroGravar.Visible = false;

           dReservasTableAdapters.ReservaEQuipamentoTableAdapter dReserva = new dReservasTableAdapters.ReservaEQuipamentoTableAdapter();

           //Seta bloco, apartamento e APT final
           if (cmbBloco.Visible)
           {
              txtBloco.Text = cmbBloco.SelectedItem.Text;
              txtApartamento.Text = cmbApartamento.SelectedItem.Text;
              lblAPT.Text = cmbApartamento.SelectedValue;
           }
           
           //Verifica se esta liberando
           if (cmdConfirmar.Text == "Cancelar Reserva")
           { 
              //Verifica se deve selecionar regra para cancelamento
              if (panRegras.Visible && lblRegraSelecionada.Text == "")
              {
                 lblErroGravar.Text = "Selecione o(s) espa�o(s) que deseja cancelar.";
                 panErroGravar.Visible = true;
                 return;
              }

              //Verifica se segue regras para cancelamento em conjunto
              if (!((bool)dReserva.VerificaRegrasCancelamento(Cred.EMP, Cred.CON, Convert.ToInt32(lblAPT.Text), lblData.Text.Substring(0, 10), Convert.ToInt32(lblEQP.Text), lblRegraSelecionada.Text)))
              {
                 lblErroGravar.Text = "Este cancelamento n�o pode ser confirmado porque as �reas comuns ainda reservadas ap�s o cancelamento n�o obedecem as regras determinadas para reserva em conjunto.";
                 panErroGravar.Visible = true;
                 return;
              }
           }
           else
           {
              //Verifica regra de espaco cumulativo (OBS: se um espa�o aplica regra ent�o obrigatoriamente deve ser espa�o cumulativo - EQPCumultaivo = true)
              if ((int)dReserva.VerificaCumulativo(Convert.ToInt32(lblAPT.Text), Cred.EMP, lblData.Text.Substring(0, 10), Convert.ToInt32(lblEQP.Text)) > 1)
              {
                 lblErroGravar.Text = "Esta reserva n�o pode ser confirmada porque outro espa�o comum j� foi reservado para a mesma unidade na data escolhida (espa�o n�o cumulativo).";
                 panErroGravar.Visible = true;
                 return;
              }

              //Verifica se deve selecionar regra para reserva
              if (panRegras.Visible && lblRegraSelecionada.Text == "")
              {
                 lblErroGravar.Text = "Selecione o(s) espa�o(s) que deseja reservar.";
                 panErroGravar.Visible = true;
                 return;
              }

              //Verifica se segue regras para reserva em conjunto
              if (!((bool)dReserva.VerificaRegras(Cred.EMP, Convert.ToInt32(lblAPT.Text), lblData.Text.Substring(0, 10), Convert.ToInt32(lblEQP.Text), lblRegraSelecionada.Text)))
              {
                  lblErroGravar.Text = "Esta reserva n�o pode ser confirmada porque existem outras �reas comuns j� reservadas que junto com esta reserva n�o obedecem as regras determinadas para reserva em conjunto (regras de reserva em conjunto).";
                  panErroGravar.Visible = true;
                  return;
              }

              //Verifica inadimplencia ou acordo (apenas se o status da unidade nao estiver visivel, porque nesse caso o usuario e o sindico e ele pode reservar mesmo inadimplentes e acordos)
              if (!panStatus.Visible) 
              {
                  if (!((bool)dReserva.VerificaReservaInad(Convert.ToInt32(lblEQP.Text), Cred.EMP)))
                  {
                      if (lblStatus.ForeColor == System.Drawing.Color.Red)
                      {
                           lblErroGravar.Text = "Por favor, checar se n�o h� nenhuma pend�ncia financeira para a sua unidade. Se estiver em dia e o problema persistir, entre em contato com a Administradora.";
                           panErroGravar.Visible = true;
                           return;
                      }
                  }
              }
           }

           //Verifica regras e equipamentos selecionado para as reservas ou cancelamentos a serem feitos (um espaco sem regra ou espacos de regra selecionada)
           string strRegraSelecionada;
           string strEquipamentosSelecionados;
           if (!panRegras.Visible)
           {
               strRegraSelecionada = "REQ[" + lblREQ.Text + "]";
               strEquipamentosSelecionados = lblEquipamento.Text;
           }
           else
           {
               strRegraSelecionada = "REQ[" + lblREQ.Text + "]" + "EQP" + lblRegraSelecionada.Text.Replace("[" + lblEQP.Text + "]", "");
               strEquipamentosSelecionados = lblEquipamentosSelecionados.Text;
           }

           //Verifica se esta reservando          
           if (cmdConfirmar.Text == "Cancelar Reserva")
           {
                //Verifica prazo de cancelamento para os equipamentos selecionados (exceto usuarios tipo sindico)
                if (!UsuariosTipoSindico.Contains(Cred.rowCli.tipousuario.ToString()))
                    if (!((bool)dReserva.VerificaReservaCancelamentoValida(Cred.EMP, strRegraSelecionada, lblData.Text.Substring(0, 10))))
                    {
                        if (!panRegras.Visible)
                            lblErroGravar.Text = "Este cancelamento n�o pode ser efetivado porque o espa�o est� fora do prazo de cancelamento permitido pelo condom�nio.";
                        else
                            lblErroGravar.Text = "Este cancelamento n�o pode ser efetivado porque um dos espa�os selecionados est� fora do prazo de cancelamento permitido pelo condom�nio.";
                        panErroGravar.Visible = true;
                        return;
                    }
           }
           else
           {
               //Verifica prazo m�nino de reserva para os equipamentos selecionados
               if (!((bool)dReserva.VerificaReservaPrazoMinimoValida(Cred.EMP, strRegraSelecionada, lblData.Text.Substring(0, 10))))
               {
                    if (!panRegras.Visible)
                        lblErroGravar.Text = "Esta reserva n�o pode ser confirmada porque o espa�o est� fora do prazo m�nimo de reserva permitido pelo condom�nio.";
                    else
                        lblErroGravar.Text = "Esta reserva n�o pode ser confirmada porque um dos espa�os selecionados est� fora do prazo m�nimo de reserva permitido pelo condom�nio.";
                    panErroGravar.Visible = true;
                    return;
               }

               //Verifica reserva unitaria individual para os equipamentos selecionados
               if (!((bool)dReserva.VerificaReservaUnitariaIndividualValida(Cred.EMP, Convert.ToInt32(lblAPT.Text), strRegraSelecionada, lblData.Text.Substring(0, 10))))
               {
                    if (!panRegras.Visible)
                        lblErroGravar.Text = "Esta reserva n�o pode ser confirmada porque o espa�o j� est� reservado para a mesma unidade em uma outra data, n�o permitindo que seja reservado novamente enquanto a outra reserva n�o for finalizada ou cancelada (reserva unit�ria individual).";
                    else
                        lblErroGravar.Text = "Esta reserva n�o pode ser confirmada porque um dos espa�os selecionados j� est� reservado para a mesma unidade em uma outra data, n�o permitindo que seja reservado novamente enquanto a outra reserva n�o for finalizada ou cancelada (reserva unit�ria individual).";
                    panErroGravar.Visible = true;
                    return;
               }

               //Verifica reserva unitaria para os equipamentos selecionados
               if (!((bool)dReserva.VerificaReservaUnitariaValida(Cred.EMP, Convert.ToInt32(lblAPT.Text), strRegraSelecionada, lblData.Text.Substring(0, 10))))
               {
                    if (!panRegras.Visible)
                        lblErroGravar.Text = "Esta reserva n�o pode ser confirmada porque o espa�o j� est� reservado para a mesma unidade em uma outra data, n�o permitindo que este espa�o seja reservado enquanto a outra reserva n�o for finalizada ou cancelada (reserva unit�ria).";
                    else
                        lblErroGravar.Text = "Esta reserva n�o pode ser confirmada porque um dos espa�os selecionados j� est� reservado para a mesma unidade em uma outra data, n�o permitindo que este espa�o seja reservado enquanto a outra reserva n�o for finalizada ou cancelada (reserva unit�ria).";
                    panErroGravar.Visible = true;
                    return;
               }

               //Verifica se existe bloqueio de unidade (ou bloco) para os equipamentos selecionados
               if ((bool)dReserva.VerificaBloqueioUnidade(Cred.EMP, Convert.ToInt32(lblAPT.Text), strRegraSelecionada, lblData.Text.Substring(0, 10)))
               {
                    if (!panRegras.Visible)
                        lblErroGravar.Text = "Esta reserva n�o pode ser confirmada porque o espa�o est� bloqueado para esta unidade e/ou bloco, na data escolhida.";
                    else
                        lblErroGravar.Text = "Esta reserva n�o pode ser confirmada porque um dos espa�os selecionados est� bloqueado para esta unidade e/ou bloco, na data escolhida.";
                    panErroGravar.Visible = true;
                    return;
               }
           }

           //Grava
           try
           {
              //Registra a reserva
              if (!((bool)dReserva.SetaReserva(Cred.EMP, Convert.ToInt32(lblAPT.Text), cmdConfirmar.Text != "Cancelar Reserva" ? "R" : "C", strRegraSelecionada, lblData.Text.Substring(0, 10), string.Format("Usu�rio {0}/{1} (Nome: {2} - Apto: {3} - Bloco: {4}", UsuariosTipoSindico.Contains(Cred.rowCli.tipousuario.ToString()) ? "S" : "P", Cred.rowCli.tipousuario.ToString(), Cred.rowCli.Nome, Cred.rowCli.Apartamento, Cred.rowCli.bloco), cmdConfirmar.Text != "Cancelar Reserva" ? lblDataHoraReservaEfetuada.Text : "")))
              {
                  if (!panRegras.Visible)
                     lblErroGravar.Text = "Esta reserva n�o pode ser confirmada porque o espa�o pode ter sido reservado por outra unidade neste instante.";
                  else
                     lblErroGravar.Text = "Esta reserva n�o pode ser confirmada porque alguns dos espa�os pode ter sido reservado por outra unidade neste instante. As reservas dispon�veis foram atualizadas na lista acima.";
                  panErroGravar.Visible = true;
                  CarregaRegras();
                  return;
              }

              //Monta detalhes do e-mail
              string strDetalhes = string.Format("{0} - Espa�o: {1} - Data: {2}", cmdConfirmar.Text == "Reservar" ? "Reserva" : "Cancelamento de Reserva", lblEquipamento.Text, lblData.Text);
              string strDetalhes2 = "";
              string strDetalhes3 = ""; 
              if (panRegras.Visible)
                 strDetalhes2 = string.Format("{0} em conjunto permitida - Selecionada: {1}", cmdConfirmar.Text == "Reservar" ? "Reserva" : "Cancelamento de Reserva", lblEquipamentosSelecionados.Text);
              if (cmdConfirmar.Text == "Reservar")
                  if (Convert.ToInt32(dReserva.VerificaMaxConvidados(Convert.ToInt32(lblREQ.Text), Cred.EMP)) > 0)
                      strDetalhes3 = "* Preencha a lista de convidados em 'Minhas Reservas'";
                  else if ((panRegras.Visible) && (strRegraSelecionada != "REQ[" + lblREQ.Text + "]EQP"))
                      strDetalhes3 = "* Caso algum dos espa�os reservados possua lista de convidados, preencha cada lista em 'Minhas Reservas'";

              //Verifica destino do e-mail
              string strEmail = "";
              if (!Cred.rowCli.IsEmailNull()) { strEmail = Cred.rowCli.Email; }
              if (UsuariosTipoSindico.Contains(Cred.rowCli.tipousuario.ToString()))
              {
                  dReservasTableAdapters.APARTAMENTOSTableAdapter dApto = new dReservasTableAdapters.APARTAMENTOSTableAdapter();
                  if (Cred.EMP == 3) dApto.Connection.ConnectionString = dApto.Connection.ConnectionString.Replace("Initial Catalog=NEON", "Initial Catalog=NEONSA");
                  object strEmailApto = dApto.VerificaEmail(Convert.ToInt32(lblAPT.Text));
                  if (strEmailApto != null) strEmail = strEmailApto.ToString();
              }

              //Manda e-mail              
              EnviaEmail(Cred.rowCli.CONNome, strEmail, txtBloco.Text + " - " + txtApartamento.Text, strDetalhes, strDetalhes2, strDetalhes3);

              //Mostra mensagem de confirmacao reserva processando
              panConfirmacao.Visible = true;
              panCampos.Visible = false;
           }
           catch (Exception)
           {
              //Erro
              lblErroGravar.Text = "Erro ao confirmar a reserva. Por favor, tente novamente.";
              panErroGravar.Visible = true;
           }
        }

        protected void cmdOK_Click(object sender, EventArgs e)
        {
            //Volta pans
            panTelaReservas.Enabled = true;
            panReserva.Visible = false;
            panBloqueioObservacao.Visible = false;

            //Carrega tela reservas 
            CarregaTelaReservas();
        }

        private void EnviaEmail(string Condominio, string Destino, string Unidade, string Detalhes, string Detalhes2, string Detalhes3)
        {
           try
           {
              //Cria e-mail
              System.Net.Mail.SmtpClient SmtpClient1 = new System.Net.Mail.SmtpClient();

              //Verifica SMTP de dominio proprio
              VirMSSQL.TableAdapter TA = new VirMSSQL.TableAdapter();
              TA.Cone.ConnectionString = System.Configuration.ConfigurationManager.ConnectionStrings["ConnectionStringN2"].ConnectionString;
              System.Data.DataTable SMTP = TA.BuscaSQLTabela("SELECT CONSMTPHost, CONSMTPUser, CONSMTPPass, CONSMTPPort FROM CONDOMINIOS WHERE (CON = @P1) AND (CON_EMP = @P2) AND CONSMTPHost IS NOT NULL AND CONSMTPHost <> ''", Cred.CON, Cred.EMP);
              foreach (System.Data.DataRow DR in SMTP.Rows)
              {
                  SmtpClient1.Host = DR[0].ToString();
                  SmtpClient1.Credentials = new System.Net.NetworkCredential(DR[1].ToString(), DR[2].ToString());
                  SmtpClient1.Port = Convert.ToInt32(DR[3]);
              }

              using (System.Net.Mail.MailMessage email = new System.Net.Mail.MailMessage())
              {
                 //Bcc
                 string Bcc = "";
                 if (System.Configuration.ConfigurationManager.AppSettings["CopiarEmail" + Cred.EMP.ToString()] != null)
                     Bcc = System.Configuration.ConfigurationManager.AppSettings["CopiarEmail" + Cred.EMP.ToString()];
                 if (Bcc != "")
                     email.Bcc.Add(Bcc);
                 System.Data.DataTable DT = TA.BuscaSQLTabela("SELECT FCCEmail FROM FaleComCondominio WHERE (FCC_CON = @P1) AND (FCC_EMP = @P2) AND (FCCTipo = 5)", Cred.CON, Cred.EMP);
                 foreach (System.Data.DataRow DR in DT.Rows)
                     email.Bcc.Add(DR[0].ToString());
                 email.Bcc.Add("neon@vcommerce.com.br");

                 //From
                 string strFrom = "";
                 TA.BuscaEscalar("SELECT CONRemetentePadrao FROM CONDOMINIOS WHERE (CON = @P1) AND (CON_EMP = @P2) AND CONSMTPHost IS NOT NULL AND CONSMTPHost <> ''", out strFrom, Cred.CON, Cred.EMP);
                 if (strFrom != "")
                     email.From = new System.Net.Mail.MailAddress(strFrom);
                 else
                     if (Cred.EMP == 1)
                         email.From = new System.Net.Mail.MailAddress("neon@neonimoveis.com.br");
                     else
                         email.From = new System.Net.Mail.MailAddress("neonsa@neonimoveis.com.br");

                 //To
                 if (Destino != "")
                    email.To.Add(Destino);
                 else
                 {
                    foreach (System.Data.DataRow DR in DT.Rows)
                       email.To.Add(DR[0].ToString());
                    if (email.To.Count == 0 && Bcc != "")
                       email.To.Add(Bcc);
                 }

                 //Verifica se nao tem destino e nao envia
                 if (email.To.Count == 0) return;

                 //Corpo do email
                 email.Subject = "Reserva de Espa�o Comum";
                 email.IsBodyHtml = true;
                 email.Body = string.Format("<html>\r\n" + 
                                            "   <head>\r\n" + 
                                            "   </head>\r\n" +
                                            "   <body>\r\n" +
                                            "      <p>Data: {0}</p>\r\n" +
                                            "      <p>{1}: {2}</p>\r\n" +
                                            "      <p>Unidade: {3}\r\n</p>\r\n" +
                                            "      <p>Tipo: <b>{4}</b></p>\r\n" +
                                            "      <p>Detalhes:</p>\r\n" +
                                            "      <p>{5}</p>\r\n" +
                                            "      <p>{6}</p>\r\n" +
                                            "      <p>{7}</p>\r\n" + 
                                            "   </body>\r\n" +
                                            "</html>",
                                            DateTime.Now, Server.HtmlEncode("Condom�nio"), Server.HtmlEncode(Condominio), Server.HtmlEncode(Unidade), Server.HtmlEncode("Reserva de Espa�o Comum"), Server.HtmlEncode(Detalhes), Server.HtmlEncode(Detalhes2), Server.HtmlEncode(Detalhes3));
                 
                 //Envia email
                 SmtpClient1.Send(email);
              }
           }
           catch (Exception)
           {
           }
        }

        protected void cmdAnterior_Click(object sender, EventArgs e)
        {
            //Seta mes anterior
            if (lblMes.Text == "1") 
            {
               lblMes.Text = "12";
               lblAno.Text = Convert.ToString(Convert.ToInt16(lblAno.Text) - 1);
            }
            else
            { lblMes.Text = Convert.ToString(Convert.ToInt16(lblMes.Text) - 1);}

            //Verifica se desabilita anterior
            if (Convert.ToInt16(lblMes.Text) == DateTime.Now.Month) { cmdAnterior.Enabled = false; }

            //Verifica dados de impressao
            CarregaDadosImpressao();

            //Carrega calendario 
            CarregaCalendario();
        }

        protected void cmdProximo_Click(object sender, EventArgs e)
        {
            //Seta mes posterior
            if (lblMes.Text == "12")
            {
               lblMes.Text = "1";
               lblAno.Text = Convert.ToString(Convert.ToInt16(lblAno.Text) + 1);
            }
            else
            { lblMes.Text = Convert.ToString(Convert.ToInt16(lblMes.Text) + 1);}

            //Verifica se desabilita anterior
            if (Convert.ToInt16(lblMes.Text) > DateTime.Now.Month) { cmdAnterior.Enabled = true; }

            //Verifica dados de impressao
            CarregaDadosImpressao();

            //Carrega calendario 
            CarregaCalendario();
        }
    }
}
