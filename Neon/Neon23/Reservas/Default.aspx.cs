﻿using System;
using LoginMenu.NeonOnLine;

namespace Reservas
{
    public partial class _Default : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void Button1_Click(object sender, EventArgs e)
        {
            LoginMenu.NeonOnLine.dNeon2 dNeon = new LoginMenu.NeonOnLine.dNeon2();
            LoginMenu.NeonOnLine.dNeon2TableAdapters.AutenticaTableAdapter TAAut = new LoginMenu.NeonOnLine.dNeon2TableAdapters.AutenticaTableAdapter();

            if (TAAut.FillProp(dNeon.Autentica, "1MARLI50", "3631", 1) > 0) //000999
            //if (TAAut.FillProp(dNeon.Autentica, "1claude45", "5153145", 1) > 0) //SOUL
            {
                LoginMenu.NeonOnLine.dNeon2.AutenticaRow rowAutentica = dNeon.Autentica[0];
                LoginMenu.NeonOnLine.dNeon2.clientesRow rowcli = dNeon.clientes.NewclientesRow();
                rowcli.APT = rowAutentica.APT;
                rowcli.Nome = rowAutentica.Nome;
                rowcli.CODCON = rowAutentica.CONCodigo;
                rowcli.CONNome = rowAutentica.CONNome;
                rowcli.bloco = rowAutentica.BLOCodigo;
                rowcli.Apartamento = rowAutentica.APTNumero;
                rowcli.Email = rowAutentica.Email;
                rowcli.tipousuario = rowAutentica.TipoUsuario;
                rowcli.Proprietario = Convert.ToBoolean(rowAutentica.Proprietario);
                LoginMenu.NeonOnLine.Credenciais Cred = new LoginMenu.NeonOnLine.Credenciais(LoginMenu.NeonOnLine.TipoAut.Neon1, rowcli, rowAutentica.EMP, rowAutentica.CON);
                Session["Cred"] = Cred;
                Response.Redirect("NeonOnLine/Reservas.aspx");
            };
        }

        protected void Button2_Click(object sender, EventArgs e)
        {
            LoginMenu.NeonOnLine.dNeon2 dNeon = new LoginMenu.NeonOnLine.dNeon2();
            LoginMenu.NeonOnLine.dNeon2TableAdapters.AutenticaTableAdapter TAAut = new LoginMenu.NeonOnLine.dNeon2TableAdapters.AutenticaTableAdapter();
            TAAut.Connection.ConnectionString = TAAut.Connection.ConnectionString.Replace("Initial Catalog=NEON", "Initial Catalog=NEONSA");

            if (TAAut.FillProp(dNeon.Autentica, "ANDERSON", "AQUI09LA", 3) > 0) //MANACA
            //if (TAAut.FillProp(dNeon.Autentica, "3FABIO76", "6072", 3) > 0) //TERRAC
            {
                LoginMenu.NeonOnLine.dNeon2.AutenticaRow rowAutentica = dNeon.Autentica[0];
                LoginMenu.NeonOnLine.dNeon2.clientesRow rowcli = dNeon.clientes.NewclientesRow();
                rowcli.APT = rowAutentica.APT;
                rowcli.Nome = rowAutentica.Nome;
                rowcli.CODCON = rowAutentica.CONCodigo;
                rowcli.CONNome = rowAutentica.CONNome;
                rowcli.bloco = rowAutentica.BLOCodigo;
                rowcli.Apartamento = rowAutentica.APTNumero;
                rowcli.Email = rowAutentica.Email;
                rowcli.tipousuario = rowAutentica.TipoUsuario;
                rowcli.Proprietario = Convert.ToBoolean(rowAutentica.Proprietario);
                LoginMenu.NeonOnLine.Credenciais Cred = new LoginMenu.NeonOnLine.Credenciais(LoginMenu.NeonOnLine.TipoAut.Neon2, rowcli, rowAutentica.EMP, rowAutentica.CON);
                Session["Cred"] = Cred;
                Response.Redirect("NeonOnLine/Reservas.aspx");
            };
        }

        protected void Button3_Click(object sender, EventArgs e)
        {
            LoginMenu.NeonOnLine.dNeon2 dNeon = new LoginMenu.NeonOnLine.dNeon2();
            LoginMenu.NeonOnLine.dNeon2TableAdapters.AutenticaTableAdapter TAAut = new LoginMenu.NeonOnLine.dNeon2TableAdapters.AutenticaTableAdapter();

            if (TAAut.FillProp(dNeon.Autentica, "ROD", "062129", 1) > 0) //000999
            {
                LoginMenu.NeonOnLine.dNeon2.AutenticaRow rowAutentica = dNeon.Autentica[0];
                LoginMenu.NeonOnLine.dNeon2.clientesRow rowcli = dNeon.clientes.NewclientesRow();
                rowcli.APT = rowAutentica.APT;
                rowcli.Nome = rowAutentica.Nome;
                rowcli.CODCON = rowAutentica.CONCodigo;
                rowcli.CONNome = rowAutentica.CONNome;
                rowcli.bloco = rowAutentica.BLOCodigo;
                rowcli.Apartamento = rowAutentica.APTNumero;
                rowcli.Email = rowAutentica.Email;
                rowcli.tipousuario = rowAutentica.TipoUsuario;
                rowcli.Proprietario = Convert.ToBoolean(rowAutentica.Proprietario);
                LoginMenu.NeonOnLine.Credenciais Cred = new LoginMenu.NeonOnLine.Credenciais(LoginMenu.NeonOnLine.TipoAut.Neon1, rowcli, rowAutentica.EMP, rowAutentica.CON);
                Session["Cred"] = Cred;
                Response.Redirect("NeonOnLine/Reservas.aspx");
            };
        }

        protected void Button4_Click(object sender, EventArgs e)
        {
            LoginMenu.NeonOnLine.dNeon2 dNeon = new LoginMenu.NeonOnLine.dNeon2();
            LoginMenu.NeonOnLine.dNeon2TableAdapters.AutenticaTableAdapter TAAut = new LoginMenu.NeonOnLine.dNeon2TableAdapters.AutenticaTableAdapter();
            TAAut.Connection.ConnectionString = TAAut.Connection.ConnectionString.Replace("Initial Catalog=NEON", "Initial Catalog=NEONSA");

            if (TAAut.FillProp(dNeon.Autentica, "2AUGUST1", "4481", 3) > 0) //MANACA
            {
                LoginMenu.NeonOnLine.dNeon2.AutenticaRow rowAutentica = dNeon.Autentica[0];
                LoginMenu.NeonOnLine.dNeon2.clientesRow rowcli = dNeon.clientes.NewclientesRow();
                rowcli.APT = rowAutentica.APT;
                rowcli.Nome = rowAutentica.Nome;
                rowcli.CODCON = rowAutentica.CONCodigo;
                rowcli.CONNome = rowAutentica.CONNome;
                rowcli.bloco = rowAutentica.BLOCodigo;
                rowcli.Apartamento = rowAutentica.APTNumero;
                rowcli.Email = rowAutentica.Email;
                rowcli.tipousuario = rowAutentica.TipoUsuario;
                rowcli.Proprietario = Convert.ToBoolean(rowAutentica.Proprietario);
                LoginMenu.NeonOnLine.Credenciais Cred = new LoginMenu.NeonOnLine.Credenciais(LoginMenu.NeonOnLine.TipoAut.Neon2, rowcli, rowAutentica.EMP, rowAutentica.CON);
                Session["Cred"] = Cred;
                Response.Redirect("NeonOnLine/Reservas.aspx");
            };
        }

        protected void Button5_Click(object sender, EventArgs e)
        {
            LoginMenu.NeonOnLine.dNeon2 dNeon = new LoginMenu.NeonOnLine.dNeon2();
            LoginMenu.NeonOnLine.dNeon2TableAdapters.CLIENTESN1TableAdapter TACli1 = new LoginMenu.NeonOnLine.dNeon2TableAdapters.CLIENTESN1TableAdapter();

            if (TACli1.Fill(dNeon.CLIENTESN1, "GP000999", "GP") > 0) //000999
            {
                LoginMenu.NeonOnLine.dNeon2.CLIENTESN1Row clirow = dNeon.CLIENTESN1[0]; 
                LoginMenu.NeonOnLine.dNeon2.clientesRow rowcli = dNeon.clientes.NewclientesRow();
                rowcli = dNeon.clientes.NewclientesRow();
                rowcli.Nome = clirow.NOME;
                rowcli.tipousuario = clirow.TIPOUSUARIO;
                rowcli.CODCON = clirow.CODCON;
                rowcli.bloco = clirow.BLOCO;
                rowcli.Apartamento = clirow.APARTAMENTO;
                rowcli.APT = 0;
                LoginMenu.NeonOnLine.Credenciais Cred = new LoginMenu.NeonOnLine.Credenciais(TipoAut.MSSQL1, TipoUsuario.UsuarioSite, rowcli);
                Cred.EMP = clirow.TIPOUSUARIO == 20 ? 1 : 3;
                if (Cred.EMP == 3)
                    dNeon.AutenticaTableAdapter.Connection.ConnectionString = dNeon.AutenticaTableAdapter.Connection.ConnectionString.Replace("Initial Catalog=NEON", "Initial Catalog=NEONSA");
                Cred.CON = (int)dNeon.AutenticaTableAdapter.BuscaCON(Cred.EMP, rowcli.CODCON);
                rowcli.CONNome = (string)dNeon.AutenticaTableAdapter.BuscaCONNome(Cred.EMP, rowcli.CODCON);
                Session["Cred"] = Cred;
                Response.Redirect("NeonOnLine/Reservas.aspx");
            };
        }

        protected void Button6_Click(object sender, EventArgs e)
        {
            LoginMenu.NeonOnLine.dNeon2 dNeon = new LoginMenu.NeonOnLine.dNeon2();
            LoginMenu.NeonOnLine.dNeon2TableAdapters.AutenticaTableAdapter TAAut = new LoginMenu.NeonOnLine.dNeon2TableAdapters.AutenticaTableAdapter();
            //TAAut.Connection.ConnectionString = TAAut.Connection.ConnectionString.Replace("Initial Catalog=NEON", "Initial Catalog=NEONSA");

            if (TAAut.FillProp(dNeon.Autentica, "1MARLI50", "3631", 1) > 0) //000999
            //if (TAAut.FillProp(dNeon.Autentica, "2AUGUST1", "4481", 3) > 0) //MANACA
            {
                LoginMenu.NeonOnLine.dNeon2.AutenticaRow rowAutentica = dNeon.Autentica[0];
                LoginMenu.NeonOnLine.dNeon2.clientesRow rowcli = dNeon.clientes.NewclientesRow();
                rowcli.APT = rowAutentica.APT;
                rowcli.Nome = rowAutentica.Nome;
                rowcli.CODCON = rowAutentica.CONCodigo;
                rowcli.CONNome = rowAutentica.CONNome;
                rowcli.bloco = rowAutentica.BLOCodigo;
                rowcli.Apartamento = rowAutentica.APTNumero;
                rowcli.Email = rowAutentica.Email;
                rowcli.tipousuario = rowAutentica.TipoUsuario;
                rowcli.Proprietario = Convert.ToBoolean(rowAutentica.Proprietario);
                LoginMenu.NeonOnLine.Credenciais Cred = new LoginMenu.NeonOnLine.Credenciais(LoginMenu.NeonOnLine.TipoAut.Neon1, rowcli, rowAutentica.EMP, rowAutentica.CON);
                Session["Cred"] = Cred;
                Response.Redirect("NeonOnLine/ReservasHistorico.aspx");
            };
        }

        protected void Button7_Click(object sender, EventArgs e)
        {
            LoginMenu.NeonOnLine.dNeon2 dNeon = new LoginMenu.NeonOnLine.dNeon2();
            LoginMenu.NeonOnLine.dNeon2TableAdapters.AutenticaTableAdapter TAAut = new LoginMenu.NeonOnLine.dNeon2TableAdapters.AutenticaTableAdapter();
            //TAAut.Connection.ConnectionString = TAAut.Connection.ConnectionString.Replace("Initial Catalog=NEON", "Initial Catalog=NEONSA");

            if (TAAut.FillProp(dNeon.Autentica, "1MARLI50", "3631", 1) > 0) //000999
            //if (TAAut.FillProp(dNeon.Autentica, "2AUGUST1", "4481", 3) > 0) //MANACA
            {
                LoginMenu.NeonOnLine.dNeon2.AutenticaRow rowAutentica = dNeon.Autentica[0];
                LoginMenu.NeonOnLine.dNeon2.clientesRow rowcli = dNeon.clientes.NewclientesRow();
                rowcli.APT = rowAutentica.APT;
                rowcli.Nome = rowAutentica.Nome;
                rowcli.CODCON = rowAutentica.CONCodigo;
                rowcli.CONNome = rowAutentica.CONNome;
                rowcli.bloco = rowAutentica.BLOCodigo;
                rowcli.Apartamento = rowAutentica.APTNumero;
                rowcli.Email = rowAutentica.Email;
                rowcli.tipousuario = rowAutentica.TipoUsuario;
                rowcli.Proprietario = Convert.ToBoolean(rowAutentica.Proprietario);
                LoginMenu.NeonOnLine.Credenciais Cred = new LoginMenu.NeonOnLine.Credenciais(LoginMenu.NeonOnLine.TipoAut.Neon1, rowcli, rowAutentica.EMP, rowAutentica.CON);
                Session["Cred"] = Cred;
                Response.Redirect("NeonOnLine/ReservasBloqueioUnidade.aspx");
            };
        }

        protected void Button8_Click(object sender, EventArgs e)
        {
            LoginMenu.NeonOnLine.dNeon2 dNeon = new LoginMenu.NeonOnLine.dNeon2();
            LoginMenu.NeonOnLine.dNeon2TableAdapters.AutenticaTableAdapter TAAut = new LoginMenu.NeonOnLine.dNeon2TableAdapters.AutenticaTableAdapter();
            //TAAut.Connection.ConnectionString = TAAut.Connection.ConnectionString.Replace("Initial Catalog=NEON", "Initial Catalog=NEONSA");

            if (TAAut.FillProp(dNeon.Autentica, "1MARLI50", "3631", 1) > 0) //000999
            //if (TAAut.FillProp(dNeon.Autentica, "2AUGUST1", "4481", 3) > 0) //MANACA
            {
                LoginMenu.NeonOnLine.dNeon2.AutenticaRow rowAutentica = dNeon.Autentica[0];
                LoginMenu.NeonOnLine.dNeon2.clientesRow rowcli = dNeon.clientes.NewclientesRow();
                rowcli.APT = rowAutentica.APT;
                rowcli.Nome = rowAutentica.Nome;
                rowcli.CODCON = rowAutentica.CONCodigo;
                rowcli.CONNome = rowAutentica.CONNome;
                rowcli.bloco = rowAutentica.BLOCodigo;
                rowcli.Apartamento = rowAutentica.APTNumero;
                rowcli.Email = rowAutentica.Email;
                rowcli.tipousuario = rowAutentica.TipoUsuario;
                rowcli.Proprietario = Convert.ToBoolean(rowAutentica.Proprietario);
                LoginMenu.NeonOnLine.Credenciais Cred = new LoginMenu.NeonOnLine.Credenciais(LoginMenu.NeonOnLine.TipoAut.Neon1, rowcli, rowAutentica.EMP, rowAutentica.CON);
                Session["Cred"] = Cred;
                Response.Redirect("NeonOnLine/ReservasBloqueioData.aspx");
            };
        }
    }
}