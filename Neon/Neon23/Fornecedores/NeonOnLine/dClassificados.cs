﻿namespace Fornecedores.NeonOnLine
{
    public partial class dClassificados
    {
        public int EMP;

        private dClassificadosTableAdapters.ClientesTableAdapter taClientesTableAdapter;
        /// <summary>
        /// TableAdapter padrão para tabela: Clientes
        /// </summary>
        public dClassificadosTableAdapters.ClientesTableAdapter ClientesTableAdapter
        {
            get
            {
                if (taClientesTableAdapter == null)
                {
                    taClientesTableAdapter = new dClassificadosTableAdapters.ClientesTableAdapter();
                    if (EMP == 3) taClientesTableAdapter.Connection.ConnectionString = taClientesTableAdapter.Connection.ConnectionString.Replace("Initial Catalog=NEON", "Initial Catalog=NEONSA");
                };
                return taClientesTableAdapter;
            }
        }

        private dClassificadosTableAdapters.PESxRAVTableAdapter taPESxRAVTableAdapter;
        /// <summary>
        /// TableAdapter padrão para tabela: PESxRAV
        /// </summary>
        public dClassificadosTableAdapters.PESxRAVTableAdapter PESxRAVTableAdapter
        {
            get
            {
                if (taPESxRAVTableAdapter == null)
                {
                    taPESxRAVTableAdapter = new dClassificadosTableAdapters.PESxRAVTableAdapter();
                    if (EMP == 3) taPESxRAVTableAdapter.Connection.ConnectionString = taPESxRAVTableAdapter.Connection.ConnectionString.Replace("Initial Catalog=NEON", "Initial Catalog=NEONSA");
                };
                return taPESxRAVTableAdapter;
            }
        }

        private dClassificadosTableAdapters.RamoAtiVidadeTableAdapter taRamoAtiVidadeTableAdapter;
        /// <summary>
        /// TableAdapter padrão para tabela: RamoAtiVidade
        /// </summary>
        public dClassificadosTableAdapters.RamoAtiVidadeTableAdapter RamoAtiVidadeTableAdapter
        {
            get
            {
                if (taRamoAtiVidadeTableAdapter == null)
                {
                    taRamoAtiVidadeTableAdapter = new dClassificadosTableAdapters.RamoAtiVidadeTableAdapter();
                    if (EMP == 3) taRamoAtiVidadeTableAdapter.Connection.ConnectionString = taRamoAtiVidadeTableAdapter.Connection.ConnectionString.Replace("Initial Catalog=NEON", "Initial Catalog=NEONSA");
                };
                return taRamoAtiVidadeTableAdapter;
            }
        }
    }
}
