using System;
using System.Data;
using System.Web.UI;
using System.Web.UI.WebControls;
using LoginMenu.NeonOnLine;
using System.Drawing;

namespace Fornecedores.NeonOnLine
{
    public partial class ClassificadosInclusaoCondomino : Page
    {
        private Credenciais Cred
        {
            get
            {
                return Credenciais.CredST(this, true);
            }
        }

        private dClassificados dCla = new dClassificados();

        protected void Page_Load(object sender, EventArgs e)
        {
            //Seta EMP
            dCla.EMP = Cred.EMP;

            if (!IsPostBack)
            {
                //Carrega tela com todos os clientes do condominio (cond�minos) com ramo de atividade
                dCla.EnforceConstraints = false;
                dCla.ClientesTableAdapter.FillComRamoAtividade(dCla.Clientes, Cred.CON);

                //Preenche grid com todos
                Repeater1.DataSource = dCla.Clientes;
                Repeater1.DataBind();
            }
        }

        public static string TipoUsuario(object oDRV)
        {
            DataRowView DRV = (DataRowView)oDRV;
            dClassificados.ClientesRow rowCli = (dClassificados.ClientesRow)DRV.Row;
            switch (rowCli.TipoUsuario)
            {
                case 1: return "S�ndico";
                case 2: return "Sub-S�ndico";
                case 3: return "Propriet�rio";
                case 4: return "Inquilino";
                default: return "";
            };
        }

        protected void Repeater1_OnItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            //Verifica se e item
            if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
            {
                //Inicia botao de classificados do cliente
                Button cmdClassificados = (Button)e.Item.FindControl("cmdClassificados");
                cmdClassificados.CommandName = ((Label)e.Item.FindControl("lblPES")).Text;
                cmdClassificados.CommandArgument = ((Label)e.Item.FindControl("lblAPT")).Text;

                //Nomeia botao de classificados para exclusao caso ja esteja ativo
                if (Convert.ToInt32(((Label)e.Item.FindControl("lblClassificadosAtivo")).Text) > 0)
                {
                    cmdClassificados.Text = "Excluir";
                    cmdClassificados.ForeColor = System.Drawing.ColorTranslator.FromHtml("#00cc00");
                }
            }
        }

        protected void cmdClassificados_Click(object sender, EventArgs e)
        {
            //Ativa ou desativa classificados para o cliente
            Button cmdClassificados = (Button)sender;
            int PES = int.Parse(cmdClassificados.CommandName);
            int APT = int.Parse(cmdClassificados.CommandArgument);

            if (cmdClassificados.Text == "Incluir")
            {
                //Habilita classificados, incluindo o registro na tabela 
                dCla.ClientesTableAdapter.ClassificadosIncluir(PES, APT);
                cmdClassificados.Text = "Excluir";
                cmdClassificados.ForeColor = System.Drawing.ColorTranslator.FromHtml("#00cc00");
            }
            else
            {
                //Desabilita classificados, excluindo o registro selecionado na tabela 
                dCla.ClientesTableAdapter.ClassificadosExcluir(PES, APT);
                cmdClassificados.Text = "Incluir";
                cmdClassificados.ForeColor = Color.Red;
            }

            //Posiciona no botao clicado
            smnScriptManager.SetFocus(cmdClassificados);
        }
    }
}
