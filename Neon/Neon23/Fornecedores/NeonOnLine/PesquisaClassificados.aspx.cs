using System;
using System.Data;
using System.Web.UI;
using System.Web.UI.WebControls;
using LoginMenu.NeonOnLine;

namespace Fornecedores.NeonOnLine
{
    public partial class PesquisaClassificados : Page
    {
        private Credenciais Cred
        {
            get
            {
                return Credenciais.CredST(this, true);
            }
        }

        private dClassificados dCla = new dClassificados();
        private DataView dtClassificados;

        protected void Page_Load(object sender, EventArgs e)
        {
            //Seta EMP
            dCla.EMP = Cred.EMP;

            if (!IsPostBack)
            {
                //Carrega o campo de ramos de atividade do formulario
                dCla.RamoAtiVidadeTableAdapter.FillCombo(dCla.RamoAtiVidade);
                cmbRamoAtividade.DataSource = dCla.RamoAtiVidade;
                cmbRamoAtividade.DataBind();

                //Carrega tela com todos os clientes do condominio (cond�minos) com classificados ativo
                dCla.EnforceConstraints = false;
                dCla.ClientesTableAdapter.FillComClassificadoAtivo(dCla.Clientes, Cred.CON);

                //Para cada registro de cliente, seta ramos de atividade
                foreach (DataRow row in dCla.Clientes.Rows)
                {
                    //Pega todos os ramos de atividade do cliente
                    dCla.PESxRAVTableAdapter.Fill(dCla.PESxRAV, Convert.ToInt32(row["PES"]));
                    
                    //Para cada ramo de atividade, inclui a descricao na coluna
                    string rav = "";
                    foreach (DataRow rowRav in dCla.PESxRAV.Rows)
                    {
                        if (rav == "")
                        { rav = Convert.ToString(rowRav["RAVDescricao"]); }
                        else
                        { rav = Convert.ToString(rowRav["RAVDescricao"]) +"<br>" + rav; }
                    }
                    row["RamosAtividade"] = rav;

                    //Efetiva atualizacao dos dados
                    row.AcceptChanges();
                }

                //Guarda classificados na sessao
                dtClassificados = dCla.Clientes.DefaultView;
                Session["dtClassificados"] = dtClassificados;

                //Carrega tela com todos os classificados - campos de busca vazios
                Carregar();

                //Posciona no primeiro campo de busca
                TNome.Focus();
            }
        }

        private void Carregar()
        {
            //Pega classificados na sessao
            dtClassificados = (DataView)Session["dtClassificados"];

            //Carrega tela com todos os classificados - todos ou de acordo com os campos de busca            
            DataView dClaBusca = dtClassificados;
            dClaBusca.RowFilter = "(ClassificadosAtivo > 0) AND (Nome LIKE '%" + TNome.Text + "%') AND (RamosAtividade LIKE '%" + cmbRamoAtividade.SelectedItem.Text.Trim() + "%')";
            dClaBusca.Sort = "Nome";
            Repeater1.DataSource = dClaBusca;
            Repeater1.DataBind();
        }

        protected void cmdBuscar_Click(object sender, EventArgs e)
        {
            //Carrega tela com todos os classificados - de acordo com os campos de busca
            Carregar();
        }
    }
}
