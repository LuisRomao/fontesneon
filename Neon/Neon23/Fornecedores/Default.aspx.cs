﻿using System;
using LoginMenu.NeonOnLine;

namespace Fornecedores
{
    public partial class _Default : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void Button1_Click(object sender, EventArgs e)
        {
            LoginMenu.NeonOnLine.dNeon2 dNeon = new LoginMenu.NeonOnLine.dNeon2();
            LoginMenu.NeonOnLine.dNeon2TableAdapters.AutenticaTableAdapter TAAut = new LoginMenu.NeonOnLine.dNeon2TableAdapters.AutenticaTableAdapter();

            //if (TAAut.FillProp(dNeon.Autentica, "1MARLI50", "3631", 1) > 0) //000999
            //if (TAAut.FillProp(dNeon.Autentica, "1claude45", "5153145", 1) > 0) //SOUL
            //if (TAAut.FillProp(dNeon.Autentica, "1MONISE1", "9191", 1) > 0) //ANNIMA
            if (TAAut.FillProp(dNeon.Autentica, "1RICARD404", "0550", 1) > 0)
            {
                LoginMenu.NeonOnLine.dNeon2.AutenticaRow rowAutentica = dNeon.Autentica[0];
                LoginMenu.NeonOnLine.dNeon2.clientesRow rowcli = dNeon.clientes.NewclientesRow();
                rowcli.APT = rowAutentica.APT;
                rowcli.Nome = rowAutentica.Nome;
                rowcli.CODCON = rowAutentica.CONCodigo;
                rowcli.CONNome = rowAutentica.CONNome;
                rowcli.bloco = rowAutentica.BLOCodigo;
                rowcli.Apartamento = rowAutentica.APTNumero;
                rowcli.Email = rowAutentica.Email;
                rowcli.tipousuario = rowAutentica.TipoUsuario;
                rowcli.Proprietario = Convert.ToBoolean(rowAutentica.Proprietario);
                LoginMenu.NeonOnLine.Credenciais Cred = new LoginMenu.NeonOnLine.Credenciais(LoginMenu.NeonOnLine.TipoAut.Neon1, rowcli, rowAutentica.EMP, rowAutentica.CON);
                Session["Cred"] = Cred;
                Response.Redirect("NeonOnLine/ClassificadosInclusaoCondomino.aspx");
            };
        }

        protected void Button2_Click(object sender, EventArgs e)
        {
            LoginMenu.NeonOnLine.dNeon2 dNeon = new LoginMenu.NeonOnLine.dNeon2();
            LoginMenu.NeonOnLine.dNeon2TableAdapters.AutenticaTableAdapter TAAut = new LoginMenu.NeonOnLine.dNeon2TableAdapters.AutenticaTableAdapter();

            //if (TAAut.FillProp(dNeon.Autentica, "1MARLI50", "3631", 1) > 0) //000999
            //if (TAAut.FillProp(dNeon.Autentica, "1claude45", "5153145", 1) > 0) //SOUL
            //if (TAAut.FillProp(dNeon.Autentica, "1MONISE1", "9191", 1) > 0) //ANNIMA
            if (TAAut.FillProp(dNeon.Autentica, "1RICARD404", "0550", 1) > 0)
            {
                LoginMenu.NeonOnLine.dNeon2.AutenticaRow rowAutentica = dNeon.Autentica[0];
                LoginMenu.NeonOnLine.dNeon2.clientesRow rowcli = dNeon.clientes.NewclientesRow();
                rowcli.APT = rowAutentica.APT;
                rowcli.Nome = rowAutentica.Nome;
                rowcli.CODCON = rowAutentica.CONCodigo;
                rowcli.CONNome = rowAutentica.CONNome;
                rowcli.bloco = rowAutentica.BLOCodigo;
                rowcli.Apartamento = rowAutentica.APTNumero;
                rowcli.Email = rowAutentica.Email;
                rowcli.tipousuario = rowAutentica.TipoUsuario;
                rowcli.Proprietario = Convert.ToBoolean(rowAutentica.Proprietario);
                LoginMenu.NeonOnLine.Credenciais Cred = new LoginMenu.NeonOnLine.Credenciais(LoginMenu.NeonOnLine.TipoAut.Neon1, rowcli, rowAutentica.EMP, rowAutentica.CON);
                Session["Cred"] = Cred;
                Response.Redirect("NeonOnLine/PesquisaClassificados.aspx");
            };
        }

        protected void Button3_Click(object sender, EventArgs e)
        {
            LoginMenu.NeonOnLine.dNeon2 dNeon = new LoginMenu.NeonOnLine.dNeon2();
            LoginMenu.NeonOnLine.dNeon2TableAdapters.AutenticaTableAdapter TAAut = new LoginMenu.NeonOnLine.dNeon2TableAdapters.AutenticaTableAdapter();

            //if (TAAut.FillProp(dNeon.Autentica, "1MARLI50", "3631", 1) > 0) //000999
            //if (TAAut.FillProp(dNeon.Autentica, "1claude45", "5153145", 1) > 0) //SOUL
            //if (TAAut.FillProp(dNeon.Autentica, "1MONISE1", "9191", 1) > 0) //ANNIMA
            if (TAAut.FillProp(dNeon.Autentica, "1RICARD404", "0550", 1) > 0)
            {
                LoginMenu.NeonOnLine.dNeon2.AutenticaRow rowAutentica = dNeon.Autentica[0];
                LoginMenu.NeonOnLine.dNeon2.clientesRow rowcli = dNeon.clientes.NewclientesRow();
                rowcli.APT = rowAutentica.APT;
                rowcli.Nome = rowAutentica.Nome;
                rowcli.CODCON = rowAutentica.CONCodigo;
                rowcli.CONNome = rowAutentica.CONNome;
                rowcli.bloco = rowAutentica.BLOCodigo;
                rowcli.Apartamento = rowAutentica.APTNumero;
                rowcli.Email = rowAutentica.Email;
                rowcli.tipousuario = rowAutentica.TipoUsuario;
                rowcli.Proprietario = Convert.ToBoolean(rowAutentica.Proprietario);
                LoginMenu.NeonOnLine.Credenciais Cred = new LoginMenu.NeonOnLine.Credenciais(LoginMenu.NeonOnLine.TipoAut.Neon1, rowcli, rowAutentica.EMP, rowAutentica.CON);
                Session["Cred"] = Cred;
                Response.Redirect("NeonOnLine/PesquisaCondomino.aspx");
            };
        }

        protected void Button4_Click(object sender, EventArgs e)
        {
            LoginMenu.NeonOnLine.dNeon2 dNeon = new LoginMenu.NeonOnLine.dNeon2();
            LoginMenu.NeonOnLine.dNeon2TableAdapters.AutenticaTableAdapter TAAut = new LoginMenu.NeonOnLine.dNeon2TableAdapters.AutenticaTableAdapter();
            TAAut.Connection.ConnectionString = TAAut.Connection.ConnectionString.Replace("Initial Catalog=NEON", "Initial Catalog=NEONSA");

            //if (TAAut.FillProp(dNeon.Autentica, "ANDERSON", "AQUI09LA", 3) > 0) //MANACA
            if (TAAut.FillProp(dNeon.Autentica, "daianarib", "sophia12", 3) > 0) //ACACI.
            //if (TAAut.FillProp(dNeon.Autentica, "3FABIO76", "6072", 3) > 0) //TERRAC
            {
                LoginMenu.NeonOnLine.dNeon2.AutenticaRow rowAutentica = dNeon.Autentica[0];
                LoginMenu.NeonOnLine.dNeon2.clientesRow rowcli = dNeon.clientes.NewclientesRow();
                rowcli.APT = rowAutentica.APT;
                rowcli.Nome = rowAutentica.Nome;
                rowcli.CODCON = rowAutentica.CONCodigo;
                rowcli.CONNome = rowAutentica.CONNome;
                rowcli.bloco = rowAutentica.BLOCodigo;
                rowcli.Apartamento = rowAutentica.APTNumero;
                rowcli.Email = rowAutentica.Email;
                rowcli.tipousuario = rowAutentica.TipoUsuario;
                rowcli.Proprietario = Convert.ToBoolean(rowAutentica.Proprietario);
                LoginMenu.NeonOnLine.Credenciais Cred = new LoginMenu.NeonOnLine.Credenciais(LoginMenu.NeonOnLine.TipoAut.Neon2, rowcli, rowAutentica.EMP, rowAutentica.CON);
                Session["Cred"] = Cred;
                Response.Redirect("NeonOnLine/ClassificadosInclusaoCondomino.aspx");
            };
        }

        protected void Button5_Click(object sender, EventArgs e)
        {
            LoginMenu.NeonOnLine.dNeon2 dNeon = new LoginMenu.NeonOnLine.dNeon2();
            LoginMenu.NeonOnLine.dNeon2TableAdapters.AutenticaTableAdapter TAAut = new LoginMenu.NeonOnLine.dNeon2TableAdapters.AutenticaTableAdapter();
            TAAut.Connection.ConnectionString = TAAut.Connection.ConnectionString.Replace("Initial Catalog=NEON", "Initial Catalog=NEONSA");

            //if (TAAut.FillProp(dNeon.Autentica, "ANDERSON", "AQUI09LA", 3) > 0) //MANACA
            if (TAAut.FillProp(dNeon.Autentica, "daianarib", "sophia12", 3) > 0) //ACACI.
            //if (TAAut.FillProp(dNeon.Autentica, "3FABIO76", "6072", 3) > 0) //TERRAC
            {
                LoginMenu.NeonOnLine.dNeon2.AutenticaRow rowAutentica = dNeon.Autentica[0];
                LoginMenu.NeonOnLine.dNeon2.clientesRow rowcli = dNeon.clientes.NewclientesRow();
                rowcli.APT = rowAutentica.APT;
                rowcli.Nome = rowAutentica.Nome;
                rowcli.CODCON = rowAutentica.CONCodigo;
                rowcli.CONNome = rowAutentica.CONNome;
                rowcli.bloco = rowAutentica.BLOCodigo;
                rowcli.Apartamento = rowAutentica.APTNumero;
                rowcli.Email = rowAutentica.Email;
                rowcli.tipousuario = rowAutentica.TipoUsuario;
                rowcli.Proprietario = Convert.ToBoolean(rowAutentica.Proprietario);
                LoginMenu.NeonOnLine.Credenciais Cred = new LoginMenu.NeonOnLine.Credenciais(LoginMenu.NeonOnLine.TipoAut.Neon2, rowcli, rowAutentica.EMP, rowAutentica.CON);
                Session["Cred"] = Cred;
                Response.Redirect("NeonOnLine/PesquisaClassificados.aspx");
            };
        }

        protected void Button6_Click(object sender, EventArgs e)
        {
            LoginMenu.NeonOnLine.dNeon2 dNeon = new LoginMenu.NeonOnLine.dNeon2();
            LoginMenu.NeonOnLine.dNeon2TableAdapters.AutenticaTableAdapter TAAut = new LoginMenu.NeonOnLine.dNeon2TableAdapters.AutenticaTableAdapter();
            TAAut.Connection.ConnectionString = TAAut.Connection.ConnectionString.Replace("Initial Catalog=NEON", "Initial Catalog=NEONSA");

            //if (TAAut.FillProp(dNeon.Autentica, "ANDERSON", "AQUI09LA", 3) > 0) //MANACA
            if (TAAut.FillProp(dNeon.Autentica, "daianarib", "sophia12", 3) > 0) //ACACI.
            //if (TAAut.FillProp(dNeon.Autentica, "3FABIO76", "6072", 3) > 0) //TERRAC
            {
                LoginMenu.NeonOnLine.dNeon2.AutenticaRow rowAutentica = dNeon.Autentica[0];
                LoginMenu.NeonOnLine.dNeon2.clientesRow rowcli = dNeon.clientes.NewclientesRow();
                rowcli.APT = rowAutentica.APT;
                rowcli.Nome = rowAutentica.Nome;
                rowcli.CODCON = rowAutentica.CONCodigo;
                rowcli.CONNome = rowAutentica.CONNome;
                rowcli.bloco = rowAutentica.BLOCodigo;
                rowcli.Apartamento = rowAutentica.APTNumero;
                rowcli.Email = rowAutentica.Email;
                rowcli.tipousuario = rowAutentica.TipoUsuario;
                rowcli.Proprietario = Convert.ToBoolean(rowAutentica.Proprietario);
                LoginMenu.NeonOnLine.Credenciais Cred = new LoginMenu.NeonOnLine.Credenciais(LoginMenu.NeonOnLine.TipoAut.Neon2, rowcli, rowAutentica.EMP, rowAutentica.CON);
                Session["Cred"] = Cred;
                Response.Redirect("NeonOnLine/PesquisaCondomino.aspx");
            };
        }
    }
}