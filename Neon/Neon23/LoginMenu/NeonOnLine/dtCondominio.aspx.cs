using System;

namespace LoginMenu.NeonOnLine
{
    public partial class dtCondominio : System.Web.UI.Page
    {
        private Credenciais Cred
        {
            get
            {
                return Credenciais.CredST(this,true);
            }
        }

        public string JavaCarregar()
        {
            return (Request.QueryString["Recarregar"] != null) ? "onload=\"ReCarregaframes()\"" : "";
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            ddtCondominioTableAdapters.CONDOMINIOSTableAdapter dCon = new ddtCondominioTableAdapters.CONDOMINIOSTableAdapter();
            if (Cred.EMP == 3) dCon.Connection.ConnectionString = dCon.Connection.ConnectionString.Replace("Initial Catalog=NEON", "Initial Catalog=NEONSA");
            ddtCondominio dsCon = new ddtCondominio();
            if (dCon.Fill(dsCon.CONDOMINIOS, Cred.CON) > 0)
            {
                ddtCondominio.CONDOMINIOSRow rowCon = dsCon.CONDOMINIOS[0];
                LCondominio.Text = rowCon.CONNome;
                LEndereco.Text = rowCon.CONEndereco;
                LBairro.Text = rowCon.CONBairro;
                LCidade.Text = (rowCon.IsCIDNomeNull()) ? "" : rowCon.CIDNome;
                LEstado.Text = (rowCon.IsCIDUfNull()) ? "" : rowCon.CIDUf;
                LCEP.Text = (rowCon.IsCONCepNull()) ? "" : rowCon.CONCep;
                LCNPJ.Text = (rowCon.IsCONCnpjNull()) ? "" : rowCon.CONCnpj;
                LSindico.Text = (rowCon.IsPESNomeSindicoNull()) ? "" : rowCon.PESNomeSindico;
                LMandato.Text = (rowCon.IsCONDataMandatoNull()) ? "" : rowCon.CONDataMandato.ToString("dd/MM/yyyy");
                if (!rowCon.IsCONObservacaoInternetNull() && rowCon.CONObservacaoInternet != "")
                    Literal1.Text = string.Format("<tr><td class=\"Titulo\">{0}</td><td class=\"Celulas\">{1}</td></tr>",
                        Server.HtmlDecode("Observações:"),
                        rowCon.CONObservacaoInternet);
                else
                    Literal1.Text = "";
                if (Session["FRNADV"] != null)
                {
                    if (!rowCon.IsCONObservacaoNull() && rowCon.CONObservacao != "")
                        Literal1.Text += string.Format("<tr><td class=\"Titulo\">{0}</td><td class=\"Celulas\">{1}</td></tr>",
                            Server.HtmlDecode("Observações:"),
                            rowCon.CONObservacao);
                    if (!rowCon.IsCON_BCONull())
                    {
                        Literal1.Text += string.Format("<tr><td class=\"Titulo\">{0}</td><td class=\"Celulas\">{1}</td></tr>",
                            Server.HtmlDecode("Banco:"),
                            rowCon.CON_BCO);
                    }
                    if (!rowCon.IsCONAgenciaNull() && !rowCon.IsCONDigitoAgenciaNull())
                    {
                        Literal1.Text += string.Format(
                            "<tr><td class=\"Titulo\">{0}</td><td class=\"Celulas\">{1}-{2}</td></tr>",
                            Server.HtmlDecode("Agência:"),
                            rowCon.CONAgencia,
                            rowCon.CONDigitoAgencia);
                    }
                    if (!rowCon.IsCONContaNull() && !rowCon.IsCONDigitoContaNull())
                    {
                        Literal1.Text += string.Format(
                            "<tr><td class=\"Titulo\">{0}</td><td class=\"Celulas\">{1}-{2}</td></tr>",
                            Server.HtmlDecode("Conta:"),
                            rowCon.CONConta,
                            rowCon.CONDigitoConta);
                    }
                }
            }
        }
    }
}
