﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Configuration;

namespace LoginMenu.NeonOnLine
{
    public partial class MenuSitePreto : System.Web.UI.Page
    {

        private Credenciais Cred
        {
            get
            {
                Credenciais ret = (Credenciais)Session["Cred"];
                if (ret == null)
                    Response.Redirect("Login.aspx?Ex=1");
                return ret;
            }
        }

        private dCondominios dCondominios1
        {
            get
            {                
                return (dCondominios)Session["dCondominios"];
            }
        }

        private dCondominios.CONDOMINIOSRow CONDOMINIOSRow1
        {
            get { return dCondominios1.CONDOMINIOS.FindByCON_EMPCON(Cred.EMP, Cred.CON); }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            //Sessao para grids
            Session["CON"] = Cred.CON;
            Session["EMP"] = Cred.EMP;

            if (!IsPostBack)
            {
                //Pasta
                string pasta = string.Format("{0}_{1}", Cred.EMP, Cred.rowCli.CODCON.Replace(".","_"));
                LabelPasta.Text = string.Format("web/neon23/neononline/Condominios/{0}", pasta);
                pasta = string.Format("{0}{1}_{2}", ConfigurationManager.AppSettings["SiteCondominios"], Cred.EMP, Cred.rowCli.CODCON.Replace(".", "_"));               
                if (!System.IO.Directory.Exists(pasta))
                    LabelPasta.Text += " - CADASTRAR";
                //Link
                int CONCRI = Cred.CON;
                CONCRI = 100 * CONCRI + (CONCRI + 3) % 17;
                LabelLink.Text = string.Format("http://www.neonimoveis.com.br/neon23/NeonOnline/WBase.aspx?EMP={0}&CON={1}", Cred.EMP, CONCRI);
                //Dominio
                if (!CONDOMINIOSRow1.IsCONSiteDomNull())
                    TextBoxDom.Text = CONDOMINIOSRow1.CONSiteDom;
                //SMTP
                if (!CONDOMINIOSRow1.IsCONSMTPHostNull())
                    TextBoxSMTPHost.Text = CONDOMINIOSRow1.CONSMTPHost;
                if (!CONDOMINIOSRow1.IsCONSMTPUserNull())
                    TextBoxSMTPUsuario.Text = CONDOMINIOSRow1.CONSMTPUser;
                if (!CONDOMINIOSRow1.IsCONSMTPPassNull())
                    TextBoxSMTPSenha.Text = CONDOMINIOSRow1.CONSMTPPass;
                if (!CONDOMINIOSRow1.IsCONSMTPPortNull())
                    TextBoxSMTPPorta.Text = CONDOMINIOSRow1.CONSMTPPort.ToString();
                //Remetente Padrão
                if (!CONDOMINIOSRow1.IsCONRemetentePadraoNull())
                    TextBoxRemetentePadrao.Text = CONDOMINIOSRow1.CONRemetentePadrao;
                //Aviso Correspondencia
                chkAvisoCorrespondencia.Checked = CONDOMINIOSRow1.CONAvisoCorrespondencia;
                //Cadastro E-mail
                chkCadastroEmail.Checked = CONDOMINIOSRow1.CONCadastroEmail;
                //Cadastro Usuário
                chkCadastroUsuario.Checked = CONDOMINIOSRow1.CONCadastroUsuario;
                //Circulares
                chkCirculares.Checked = CONDOMINIOSRow1.CONCirculares;
                //Classificados
                chkClassificados.Checked = CONDOMINIOSRow1.CONClassificados1;
                //Fale Com (Corpo Diretivo e/ou E-mails Cadastrados)
                chkFaleCom.Checked = CONDOMINIOSRow1.CONFaleCom;
                chkFaleComCD.Checked = CONDOMINIOSRow1.CONFaleComCD;
                //Controle de Mudanca (1)
                chkControleMudanca.Checked = CONDOMINIOSRow1.CONControleMudanca;
                rfvRemControleMudanca.Enabled = chkControleMudanca.Checked;
                if (!CONDOMINIOSRow1.IsCOMRemetenteNull())
                   TextBoxRemetente.Text = CONDOMINIOSRow1.COMRemetente;
                //Autorizacao Fornecedores (2)
                chkAutorizacaoFornecedores.Checked = CONDOMINIOSRow1.CONAutorizacaoFornecedores;
                rfvRemAutorizacaoFornecedores.Enabled = chkAutorizacaoFornecedores.Checked;
                if (!CONDOMINIOSRow1.IsCONRemetente2Null())
                   TextBoxRemetente2.Text = CONDOMINIOSRow1.CONRemetente2; 
                //Solicitacao Reparos (3)
                chkSolicitacaoReparos.Checked = CONDOMINIOSRow1.CONSolicitacaoReparos;
                rfvRemSolicitacaoReparos.Enabled = chkSolicitacaoReparos.Checked;
                if (!CONDOMINIOSRow1.IsCONRemetente3Null())
                   TextBoxRemetente3.Text = CONDOMINIOSRow1.CONRemetente3; 
                //Sugestoes (4)
                chkSugestoes.Checked = CONDOMINIOSRow1.CONSugestoes;
                rfvRemSugestoes.Enabled = chkSugestoes.Checked;
                if (!CONDOMINIOSRow1.IsCONRemetente4Null())
                   TextBoxRemetente4.Text = CONDOMINIOSRow1.CONRemetente4;
                //Reserva (5)
                chkReserva.Checked = CONDOMINIOSRow1.CONReserva;
            }
        }

        protected void Button1_Click(object sender, EventArgs e)
        {
           SqlDataSource1.InsertParameters["MEMTitulo"].DefaultValue = "Inicial";
           SqlDataSource1.InsertParameters["MEMURL"].DefaultValue = "inicial.aspx";
           SqlDataSource1.InsertParameters["MEMCondomino"].DefaultValue = "false";
           SqlDataSource1.InsertParameters["MEMSindico"].DefaultValue = "false";
           SqlDataSource1.InsertParameters["MEMNeon"].DefaultValue = "false";
           SqlDataSource1.Insert();
        }

        protected void LinkButton3_Click(object sender, EventArgs e)
        {
            TextBox TextBoxTitulo = (TextBox)GridView1.FooterRow.FindControl("TextBoxNovoTitulo");
            TextBox TextBoxURL = (TextBox)GridView1.FooterRow.FindControl("TextBoxNovaURL");            
            if ((TextBoxTitulo.Text != "") && (TextBoxURL.Text != ""))
            {
                SqlDataSource1.InsertParameters["MEMTitulo"].DefaultValue = TextBoxTitulo.Text;
                SqlDataSource1.InsertParameters["MEMURL"].DefaultValue = TextBoxURL.Text;
                SqlDataSource1.InsertParameters["MEMCondomino"].DefaultValue = "false";
                SqlDataSource1.InsertParameters["MEMSindico"].DefaultValue = "false";
                SqlDataSource1.InsertParameters["MEMNeon"].DefaultValue = "false";
                SqlDataSource1.Insert();
            }
        }

        protected void LinkButton4_Click(object sender, EventArgs e)
        {
            if (TextBoxDom.Text != "")
            {
                CONDOMINIOSRow1.CONSiteDom = TextBoxDom.Text.ToUpper();
                using (dCondominiosTableAdapters.CONDOMINIOSTableAdapter Ta = new LoginMenu.NeonOnLine.dCondominiosTableAdapters.CONDOMINIOSTableAdapter())
                {
                    Ta.Update(dCondominios1.CONDOMINIOS);
                    dCondominios1.CONDOMINIOS.AcceptChanges();
                }
            } 
        }

        protected void cmdRemetentePadraoSalvar_Click(object sender, EventArgs e)
        {
            CONDOMINIOSRow1.CONSMTPHost = TextBoxSMTPHost.Text.ToLower();
            CONDOMINIOSRow1.CONSMTPUser = TextBoxSMTPUsuario.Text;
            CONDOMINIOSRow1.CONSMTPPass = TextBoxSMTPSenha.Text;
            CONDOMINIOSRow1.CONSMTPPort = Convert.ToInt32(TextBoxSMTPPorta.Text);
            CONDOMINIOSRow1.CONRemetentePadrao = TextBoxRemetentePadrao.Text.ToLower();
            using (dCondominiosTableAdapters.CONDOMINIOSTableAdapter Ta = new LoginMenu.NeonOnLine.dCondominiosTableAdapters.CONDOMINIOSTableAdapter())
            {
                Ta.Update(dCondominios1.CONDOMINIOS);
                dCondominios1.CONDOMINIOS.AcceptChanges();
            }
        }

        protected void cmdSalvarFuncoes_Click(object sender, EventArgs e)
        {
            CONDOMINIOSRow1.CONAvisoCorrespondencia = chkAvisoCorrespondencia.Checked;
            CONDOMINIOSRow1.CONCadastroEmail= chkCadastroEmail.Checked;
            CONDOMINIOSRow1.CONCadastroUsuario = chkCadastroUsuario.Checked;
            CONDOMINIOSRow1.CONCirculares = chkCirculares.Checked;
            CONDOMINIOSRow1.CONClassificados1 = chkClassificados.Checked;
            using (dCondominiosTableAdapters.CONDOMINIOSTableAdapter Ta = new LoginMenu.NeonOnLine.dCondominiosTableAdapters.CONDOMINIOSTableAdapter())
            {
                Ta.Update(dCondominios1.CONDOMINIOS);
                dCondominios1.CONDOMINIOS.AcceptChanges();
            }
        }

        protected void cmdSalvarFaleCom_Click(object sender, EventArgs e)
        {
            CONDOMINIOSRow1.CONFaleCom = chkFaleCom.Checked;
            CONDOMINIOSRow1.CONFaleComCD = chkFaleComCD.Checked;
            using (dCondominiosTableAdapters.CONDOMINIOSTableAdapter Ta = new LoginMenu.NeonOnLine.dCondominiosTableAdapters.CONDOMINIOSTableAdapter())
            {
                Ta.Update(dCondominios1.CONDOMINIOS);
                dCondominios1.CONDOMINIOS.AcceptChanges();
            }
        }

        protected void Button1_Click1(object sender, EventArgs e)
        {
            SqlDataSource2.InsertParameters["FCCNome"].DefaultValue = "Nome";
            SqlDataSource2.InsertParameters["FCCEmail"].DefaultValue = "xxx@xxx.com.br";
            SqlDataSource2.InsertParameters["FCCCargo"].DefaultValue = "Cargo";            
            SqlDataSource2.Insert();            
        }

        protected void LinkButton5_Click(object sender, EventArgs e)
        {
            TextBox xTextBoxNome = (TextBox)GridView2.FooterRow.FindControl("TextBoxNome");
            TextBox xTextBoxCargo = (TextBox)GridView2.FooterRow.FindControl("TextBoxCargo");
            TextBox xTextBoxEmail = (TextBox)GridView2.FooterRow.FindControl("TextBoxEmail");
            if ((xTextBoxNome.Text != "") && (xTextBoxCargo.Text != "") && (xTextBoxEmail.Text != ""))
            {
                SqlDataSource2.InsertParameters["FCCNome"].DefaultValue = xTextBoxNome.Text;
                SqlDataSource2.InsertParameters["FCCEmail"].DefaultValue = xTextBoxEmail.Text;
                SqlDataSource2.InsertParameters["FCCCargo"].DefaultValue = xTextBoxCargo.Text;
                SqlDataSource2.Insert();
            }
        }

        protected void chkControleMudanca_CheckedChanged(object sender, EventArgs e)
        {
            rfvRemControleMudanca.Enabled = chkControleMudanca.Checked;
        }

        protected void cmdControleMudancaSalvar_Click(object sender, EventArgs e)
        {   
            CONDOMINIOSRow1.CONControleMudanca = chkControleMudanca.Checked;
            CONDOMINIOSRow1.COMRemetente = TextBoxRemetente.Text.ToLower();
            using (dCondominiosTableAdapters.CONDOMINIOSTableAdapter Ta = new LoginMenu.NeonOnLine.dCondominiosTableAdapters.CONDOMINIOSTableAdapter())
            {
                Ta.Update(dCondominios1.CONDOMINIOS);
                dCondominios1.CONDOMINIOS.AcceptChanges();
            }
        }

        protected void cmdControleMudancaPrimeiroContato_Click(object sender, EventArgs e)
        {
           dsControleMudanca.InsertParameters["FCCNome"].DefaultValue = "Nome";
           dsControleMudanca.InsertParameters["FCCEmail"].DefaultValue = "xxx@xxx.com.br";
           dsControleMudanca.InsertParameters["FCCCargo"].DefaultValue = "Cargo";
           dsControleMudanca.Insert();
        }

        protected void cmdControleMudancaIncluirContato_Click(object sender, EventArgs e)
        {
           TextBox xTextBoxNome = (TextBox)GridView3.FooterRow.FindControl("TextBoxNome");
           TextBox xTextBoxCargo = (TextBox)GridView3.FooterRow.FindControl("TextBoxCargo");
           TextBox xTextBoxEmail = (TextBox)GridView3.FooterRow.FindControl("TextBoxEmail");
           if ((xTextBoxNome.Text != "") && (xTextBoxCargo.Text != "") && (xTextBoxEmail.Text != ""))
           {
              dsControleMudanca.InsertParameters["FCCNome"].DefaultValue = xTextBoxNome.Text;
              dsControleMudanca.InsertParameters["FCCEmail"].DefaultValue = xTextBoxEmail.Text;
              dsControleMudanca.InsertParameters["FCCCargo"].DefaultValue = xTextBoxCargo.Text;
              dsControleMudanca.Insert();
           }
        }

        protected void chkAutorizacaoFornecedores_CheckedChanged(object sender, EventArgs e)
        {
           rfvRemAutorizacaoFornecedores.Enabled = chkAutorizacaoFornecedores.Checked;
        }

        protected void cmdAutorizacaoFornecedoresSalvar_Click(object sender, EventArgs e)
        {
           CONDOMINIOSRow1.CONAutorizacaoFornecedores = chkAutorizacaoFornecedores.Checked;
           CONDOMINIOSRow1.CONRemetente2 = TextBoxRemetente2.Text.ToLower();
           using (dCondominiosTableAdapters.CONDOMINIOSTableAdapter Ta = new LoginMenu.NeonOnLine.dCondominiosTableAdapters.CONDOMINIOSTableAdapter())
           {
              Ta.Update(dCondominios1.CONDOMINIOS);
              dCondominios1.CONDOMINIOS.AcceptChanges();
           }
        }

        protected void cmdAutorizacaoFornecedoresPrimeiroContato_Click(object sender, EventArgs e)
        {
           dsAutorizacaoFornecedores.InsertParameters["FCCNome"].DefaultValue = "Nome";
           dsAutorizacaoFornecedores.InsertParameters["FCCEmail"].DefaultValue = "xxx@xxx.com.br";
           dsAutorizacaoFornecedores.InsertParameters["FCCCargo"].DefaultValue = "Cargo";
           dsAutorizacaoFornecedores.Insert();
        }

        protected void cmdAutorizacaoFornecedoresIncluirContato_Click(object sender, EventArgs e)
        {
           TextBox xTextBoxNome = (TextBox)GridView4.FooterRow.FindControl("TextBoxNome");
           TextBox xTextBoxCargo = (TextBox)GridView4.FooterRow.FindControl("TextBoxCargo");
           TextBox xTextBoxEmail = (TextBox)GridView4.FooterRow.FindControl("TextBoxEmail");
           if ((xTextBoxNome.Text != "") && (xTextBoxCargo.Text != "") && (xTextBoxEmail.Text != ""))
           {
              dsAutorizacaoFornecedores.InsertParameters["FCCNome"].DefaultValue = xTextBoxNome.Text;
              dsAutorizacaoFornecedores.InsertParameters["FCCEmail"].DefaultValue = xTextBoxEmail.Text;
              dsAutorizacaoFornecedores.InsertParameters["FCCCargo"].DefaultValue = xTextBoxCargo.Text;
              dsAutorizacaoFornecedores.Insert();
           }
        }

        protected void chkSolicitacaoReparos_CheckedChanged(object sender, EventArgs e)
        {
           rfvRemSolicitacaoReparos.Enabled = chkSolicitacaoReparos.Checked;
        }

        protected void cmdSolicitacaoReparosSalvar_Click(object sender, EventArgs e)
        {
           CONDOMINIOSRow1.CONSolicitacaoReparos = chkSolicitacaoReparos.Checked;
           CONDOMINIOSRow1.CONRemetente3 = TextBoxRemetente3.Text.ToLower();
           using (dCondominiosTableAdapters.CONDOMINIOSTableAdapter Ta = new LoginMenu.NeonOnLine.dCondominiosTableAdapters.CONDOMINIOSTableAdapter())
           {
              Ta.Update(dCondominios1.CONDOMINIOS);
              dCondominios1.CONDOMINIOS.AcceptChanges();
           }
        }

        protected void cmdSolicitacaoReparosPrimeiroContato_Click(object sender, EventArgs e)
        {
           dsSolicitacaoReparos.InsertParameters["FCCNome"].DefaultValue = "Nome";
           dsSolicitacaoReparos.InsertParameters["FCCEmail"].DefaultValue = "xxx@xxx.com.br";
           dsSolicitacaoReparos.InsertParameters["FCCCargo"].DefaultValue = "Cargo";
           dsSolicitacaoReparos.Insert();
        }

        protected void cmdSolicitacaoReparosIncluirContato_Click(object sender, EventArgs e)
        {
           TextBox xTextBoxNome = (TextBox)GridView5.FooterRow.FindControl("TextBoxNome");
           TextBox xTextBoxCargo = (TextBox)GridView5.FooterRow.FindControl("TextBoxCargo");
           TextBox xTextBoxEmail = (TextBox)GridView5.FooterRow.FindControl("TextBoxEmail");
           if ((xTextBoxNome.Text != "") && (xTextBoxCargo.Text != "") && (xTextBoxEmail.Text != ""))
           {
              dsSolicitacaoReparos.InsertParameters["FCCNome"].DefaultValue = xTextBoxNome.Text;
              dsSolicitacaoReparos.InsertParameters["FCCEmail"].DefaultValue = xTextBoxEmail.Text;
              dsSolicitacaoReparos.InsertParameters["FCCCargo"].DefaultValue = xTextBoxCargo.Text;
              dsSolicitacaoReparos.Insert();
           }
        }

        protected void chkSugestoes_CheckedChanged(object sender, EventArgs e)
        {
           rfvRemSugestoes.Enabled = chkSugestoes.Checked;
        }

        protected void cmdSugestoesSalvar_Click(object sender, EventArgs e)
        {
           CONDOMINIOSRow1.CONSugestoes = chkSugestoes.Checked;
           CONDOMINIOSRow1.CONRemetente4 = TextBoxRemetente4.Text.ToLower();
           using (dCondominiosTableAdapters.CONDOMINIOSTableAdapter Ta = new LoginMenu.NeonOnLine.dCondominiosTableAdapters.CONDOMINIOSTableAdapter())
           {
              Ta.Update(dCondominios1.CONDOMINIOS);
              dCondominios1.CONDOMINIOS.AcceptChanges();
           }
        }

        protected void cmdSugestoesPrimeiroContato_Click(object sender, EventArgs e)
        {
           dsSugestoes.InsertParameters["FCCNome"].DefaultValue = "Nome";
           dsSugestoes.InsertParameters["FCCEmail"].DefaultValue = "xxx@xxx.com.br";
           dsSugestoes.InsertParameters["FCCCargo"].DefaultValue = "Cargo";
           dsSugestoes.Insert();
        }

        protected void cmdSugestoesIncluirContato_Click(object sender, EventArgs e)
        {
           TextBox xTextBoxNome = (TextBox)GridView6.FooterRow.FindControl("TextBoxNome");
           TextBox xTextBoxCargo = (TextBox)GridView6.FooterRow.FindControl("TextBoxCargo");
           TextBox xTextBoxEmail = (TextBox)GridView6.FooterRow.FindControl("TextBoxEmail");
           if ((xTextBoxNome.Text != "") && (xTextBoxCargo.Text != "") && (xTextBoxEmail.Text != ""))
           {
              dsSugestoes.InsertParameters["FCCNome"].DefaultValue = xTextBoxNome.Text;
              dsSugestoes.InsertParameters["FCCEmail"].DefaultValue = xTextBoxEmail.Text;
              dsSugestoes.InsertParameters["FCCCargo"].DefaultValue = xTextBoxCargo.Text;
              dsSugestoes.Insert();
           }
        }

        protected void cmdReservaPrimeiroContato_Click(object sender, EventArgs e)
        {
           dsReserva.InsertParameters["FCCNome"].DefaultValue = "Nome";
           dsReserva.InsertParameters["FCCEmail"].DefaultValue = "xxx@xxx.com.br";
           dsReserva.InsertParameters["FCCCargo"].DefaultValue = "Cargo";
           dsReserva.Insert();
        }

        protected void cmdReservaIncluirContato_Click(object sender, EventArgs e)
        {
           TextBox xTextBoxNome = (TextBox)GridView7.FooterRow.FindControl("TextBoxNome");
           TextBox xTextBoxCargo = (TextBox)GridView7.FooterRow.FindControl("TextBoxCargo");
           TextBox xTextBoxEmail = (TextBox)GridView7.FooterRow.FindControl("TextBoxEmail");
           if ((xTextBoxNome.Text != "") && (xTextBoxCargo.Text != "") && (xTextBoxEmail.Text != ""))
           {
              dsReserva.InsertParameters["FCCNome"].DefaultValue = xTextBoxNome.Text;
              dsReserva.InsertParameters["FCCEmail"].DefaultValue = xTextBoxEmail.Text;
              dsReserva.InsertParameters["FCCCargo"].DefaultValue = xTextBoxCargo.Text;
              dsReserva.Insert();
           }
        }

        protected void cmdListaConvidadosPrimeiroContato_Click(object sender, EventArgs e)
        {
            dsListaConvidados.InsertParameters["FCCNome"].DefaultValue = "Nome";
            dsListaConvidados.InsertParameters["FCCEmail"].DefaultValue = "xxx@xxx.com.br";
            dsListaConvidados.InsertParameters["FCCCargo"].DefaultValue = "Cargo";
            dsListaConvidados.Insert();
        }

        protected void cmdListaConvidadosIncluirContato_Click(object sender, EventArgs e)
        {
            TextBox xTextBoxNome = (TextBox)GridView9.FooterRow.FindControl("TextBoxNome");
            TextBox xTextBoxCargo = (TextBox)GridView9.FooterRow.FindControl("TextBoxCargo");
            TextBox xTextBoxEmail = (TextBox)GridView9.FooterRow.FindControl("TextBoxEmail");
            if ((xTextBoxNome.Text != "") && (xTextBoxCargo.Text != "") && (xTextBoxEmail.Text != ""))
            {
                dsListaConvidados.InsertParameters["FCCNome"].DefaultValue = xTextBoxNome.Text;
                dsListaConvidados.InsertParameters["FCCEmail"].DefaultValue = xTextBoxEmail.Text;
                dsListaConvidados.InsertParameters["FCCCargo"].DefaultValue = xTextBoxCargo.Text;
                dsListaConvidados.Insert();
            }
        }
    }
}