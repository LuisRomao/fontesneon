using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

namespace LoginMenu.NeonOnLine
{
    public partial class WBase : System.Web.UI.Page
    {
        public string Menu;
        public string Central;
        public string Topo;

        private Credenciais Cred
        {
            get
            {
                Credenciais ret = (Credenciais)Session["Cred"];                
                return ret;
            }
        }

        private void AjustaSiteCond(dCondominios dCondominios1,int EMP,int CON)
        {
            if (dCondominios1.MENUS.Count > 0)
            {
                string pasta = string.Format("{0}_{1}", dCondominios1.CONDOMINIOS[0].CON_EMP, dCondominios1.CONDOMINIOS[0].CONCodigo.Replace(".", "_"));
                if (Request["Ex"] == "1")
                    Central = string.Format("Login.aspx?Ex=Segunda");
                else
                    Central = string.Format("condominios/{0}/{1}", pasta, dCondominios1.MENUS[0].MEMURL);
                if (Cred != null)
                {
                    foreach(dCondominios.MENUSRow rowCandidato in dCondominios1.MENUS)
                        if (rowCandidato.MEMURL.ToUpper().Contains("INICIAL1"))
                        {
                            Central = string.Format("condominios/{0}/{1}", pasta, rowCandidato.MEMURL);
                            break;
                        }
                }
                Topo = string.Format("condominios/{0}/TopoCond.aspx?EMP={1}&CON={2}", pasta, EMP, CON);
                Menu = string.Format("Menu.aspx?EMP={0}&CON={1}", EMP, CON);
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            
            Menu = "Menu.aspx";
            Central = "WVazia.aspx";
            Topo = "WTopo.aspx";
            
            if (!IsPostBack)
            {
                int EMP = 0;
                int CON = 0;
                if (Request["EMP"] != null)
                    int.TryParse(Request["EMP"], out EMP);
                if (Request["CON"] != null)
                    int.TryParse(Request["CON"], out CON);
                if (dCondominios.SubSite(this, EMP, CON))                                                                    
                    AjustaSiteCond((dCondominios)Session["dCondominios"], EMP, CON);                                                        
            }
        }
    }
}
