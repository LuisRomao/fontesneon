﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Collections;
using DocBacarios;

namespace LoginMenu.NeonOnLine
{
    public partial class CpfCnpj : System.Web.UI.Page
    {
        private Credenciais Cred
        {
            get
            {
                Credenciais ret = (Credenciais)Session["Cred"];
                return ret;
            }
        }

        private dCadastro dCad = new dCadastro();

        private ArrayList lstCPFInvalido
        {
            get { return new ArrayList(new string[] { "00000000000", "11111111111", "22222222222", "33333333333", "44444444444", "55555555555", "66666666666", "77777777777", "88888888888", "99999999999" }); }
        }

        private ArrayList lstCNPJInvalido
        {
            get { return new ArrayList(new string[] { "00000000000000", "11111111111111", "22222222222222", "33333333333333", "44444444444444", "55555555555555", "66666666666666", "77777777777777", "88888888888888", "99999999999999" }); }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            //Seta EMP
            dCad.EMP = Cred.EMP;

            if (!IsPostBack)
            {
                //Limpa tela
                txtCpfCnpj.Text = "";
                lblInvalido.Visible = false;

                //Seta propriedades dos campos
                txtCpfCnpj.Attributes.Add("onkeyup", "javascript:somenteMascara(this,'0123456789./-');");
            }
        }

        protected void cmdCadastrar_Click(object sender, EventArgs e)
        {
            //Verifica CPF ou CNPJ valido
            DocBacarios.CPFCNPJ CpfCnpj = new DocBacarios.CPFCNPJ(txtCpfCnpj.Text);
            if (CpfCnpj.Tipo != TipoCpfCnpj.INVALIDO 
                && !lstCPFInvalido.Contains(CpfCnpj.Valor.ToString())
                && !lstCNPJInvalido.Contains(CpfCnpj.Valor.ToString()))
            {
                //Cadastra na base de dados
                dCad.PESSOASTableAdapter.CadastraCpfCnpj((CpfCnpj.Tipo == TipoCpfCnpj.CNPJ) ? "J" : "F", CpfCnpj.ToString(), Cred.rowCli.PES);

                //Libera o acesso
                if (Request["EMP"] == "")
                    Response.Redirect("Wbase.aspx");
                else
                    Response.Redirect(string.Format("Wbase.aspx?EMP={0}&CON={1}", Request["EMP"], Request["CON"]));
            }
            else
                lblInvalido.Visible = true;
        }
    }
}