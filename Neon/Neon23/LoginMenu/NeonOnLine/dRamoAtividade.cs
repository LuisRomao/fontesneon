﻿namespace LoginMenu.NeonOnLine
{
    partial class dRamoAtividade
    {
        public int EMP;

        private dRamoAtividadeTableAdapters.RamoAtiVidadeTableAdapter RamoAtiVidadeTableAdapter;
        /// <summary>
        /// TableAdapter padrão para tabela: RamoATividade
        /// </summary>
        public dRamoAtividadeTableAdapters.RamoAtiVidadeTableAdapter RamoAtividadeTableAdapter
        {
            get
            {
                if (RamoAtiVidadeTableAdapter == null)
                {
                    RamoAtiVidadeTableAdapter = new dRamoAtividadeTableAdapters.RamoAtiVidadeTableAdapter();
                    if (EMP == 3) RamoAtiVidadeTableAdapter.Connection.ConnectionString = RamoAtiVidadeTableAdapter.Connection.ConnectionString.Replace("Initial Catalog=NEON", "Initial Catalog=NEONSA");
                };
                return RamoAtiVidadeTableAdapter;
            }
        }
    }
}
