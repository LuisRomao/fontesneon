using System;
using System.Web;

namespace LoginMenu.NeonOnLine
{
    public partial class Login : System.Web.UI.Page
    {
        private Credenciais Autentica()
        {
            dNeon2 dNeon = new dNeon2();
            dNeon2.AutenticaRow rowAutentica = null;
            TipoAut TipoAut;
                        
            //Autenticacao usu�rio cond�minos
            dNeon2TableAdapters.AutenticaTableAdapter TAAut = new LoginMenu.NeonOnLine.dNeon2TableAdapters.AutenticaTableAdapter();
            try
            {
                //Tenta autenticar pelo SQL - Neon
                TipoAut = TipoAut.Neon1;
                if (TAAut.FillProp(dNeon.Autentica, TxUsuario.Text, TxSenha.Text, 1) > 0)
                    rowAutentica = dNeon.Autentica[0];
                else if (TAAut.FillInq(dNeon.Autentica, TxUsuario.Text, TxSenha.Text, 1) > 0)
                    rowAutentica = dNeon.Autentica[0];
                else
                {
                    //Tenta autenticar pelo SQL - Neon SA
                    TAAut.Connection.ConnectionString = TAAut.Connection.ConnectionString.Replace("Initial Catalog=NEON", "Initial Catalog=NEONSA");
                    TipoAut = TipoAut.Neon2;
                    if (TAAut.FillProp(dNeon.Autentica, TxUsuario.Text, TxSenha.Text, 3) > 0)
                        rowAutentica = dNeon.Autentica[0];
                    else if (TAAut.FillInq(dNeon.Autentica, TxUsuario.Text, TxSenha.Text, 3) > 0)
                        rowAutentica = dNeon.Autentica[0];
                }

                //Verifica se autenticou
                if (rowAutentica != null)
                {
                    dNeon2.clientesRow rowcli = dNeon.clientes.NewclientesRow();
                    rowcli.APT = rowAutentica.APT;
                    rowcli.PES = rowAutentica.PES;
                    rowcli.Nome = rowAutentica.Nome;
                    rowcli.CODCON = rowAutentica.CONCodigo;
                    rowcli.CONNome = rowAutentica.CONNome;
                    rowcli.bloco = rowAutentica.BLOCodigo;
                    rowcli.Apartamento = rowAutentica.APTNumero;
                    rowcli.Email = rowAutentica.Email;
                    rowcli.CpfCnpj = rowAutentica.CpfCnpj;
                    rowcli.tipousuario = rowAutentica.TipoUsuario;
                    rowcli.Proprietario = Convert.ToBoolean(rowAutentica.Proprietario);
                    return new Credenciais(TipoAut, rowcli, rowAutentica.EMP, rowAutentica.CON);
                }
            }
            catch 
            { 
            };

            //Autentica FRN
            int FRN;
            if (int.TryParse(TxUsuario.Text, out FRN))
            {
                dNeon2TableAdapters.FORNECEDORESTableAdapter TFRN = new dNeon2TableAdapters.FORNECEDORESTableAdapter();
                try
                {

                    if (TFRN.Fill(dNeon.FORNECEDORES, FRN, TxSenha.Text) == 1)
                    {
                        Session["EMP"] = 1;
                        Session["FRN"] = FRN;
                        return new Credenciais(TipoAut.Neon1, TipoUsuario.FRN, null);
                    }
                    else
                    {
                        TFRN.Connection.ConnectionString = TFRN.Connection.ConnectionString.Replace("Initial Catalog=NEON", "Initial Catalog=NEONSA");
                        if (TFRN.Fill(dNeon.FORNECEDORES, FRN, TxSenha.Text) == 1)
                        {
                            Session["EMP"] = 3;
                            Session["FRN"] = FRN;
                            return new Credenciais(TipoAut.Neon2, TipoUsuario.FRN, null);
                        }
                    }
                }
                catch
                {
                }
            }

            //Autenticacao usu�rio site
            dNeon2TableAdapters.CLIENTESN1TableAdapter TACli1 = new LoginMenu.NeonOnLine.dNeon2TableAdapters.CLIENTESN1TableAdapter();
            try
            {
                if (TACli1.Fill(dNeon.CLIENTESN1, TxUsuario.Text, TxSenha.Text) > 0)
                {
                    dNeon2.CLIENTESN1Row clirow = dNeon.CLIENTESN1[0];
                    dNeon2.clientesRow rowcli;
                    TipoUsuario Tipo;
                    int EMP = 0;
                    switch (clirow.TIPOUSUARIO)
                    {
                        case 0: 
                            //Administradora
                            Tipo = TipoUsuario.Administradora;
                            rowcli = dNeon.clientes.NewclientesRow();
                            rowcli.Nome = clirow.NOME;
                            rowcli.CODCON = clirow.CODCON;                           
                            rowcli.CONNome = "Administradora";                           
                            rowcli.bloco = clirow.BLOCO;
                            rowcli.Apartamento = clirow.APARTAMENTO;
                            rowcli.Email = clirow.EMAIL;
                            rowcli.tipousuario = 1;                            
                            break;
                        case 12:
                        case 13:
                            //Sindico
                            Tipo = TipoUsuario.sindico;
                            rowcli = dNeon.clientes.NewclientesRow();
                            rowcli.Nome = clirow.NOME;
                            rowcli.CODCON = clirow.CODCON;                           
                            rowcli.bloco = clirow.BLOCO;
                            rowcli.Apartamento = clirow.APARTAMENTO;
                            rowcli.Email = clirow.EMAIL;
                            rowcli.tipousuario = clirow.TIPOUSUARIO;
                            EMP = clirow.TIPOUSUARIO == 12 ? 1 : 3;
                            break;
                        case 14:
                        case 15:
                            //Advogado
                            Tipo = TipoUsuario.advogado;
                            rowcli = dNeon.clientes.NewclientesRow();
                            rowcli.Nome = clirow.NOME;
                            rowcli.CODCON = clirow.CODCON;
                            rowcli.bloco = clirow.BLOCO;
                            rowcli.Apartamento = clirow.APARTAMENTO;
                            rowcli.Email = clirow.EMAIL;
                            rowcli.tipousuario = clirow.TIPOUSUARIO;
                            EMP = clirow.TIPOUSUARIO == 14 ? 1 : 3;
                            break;
                        case 16:
                            //Advogado Administradora
                            Tipo = TipoUsuario.advogado;
                            rowcli = dNeon.clientes.NewclientesRow();
                            rowcli.Nome = clirow.NOME;
                            rowcli.CODCON = "XXXXXX";
                            rowcli.CONNome = "Advogado(a)";
                            rowcli.bloco = clirow.BLOCO;
                            rowcli.Apartamento = clirow.APARTAMENTO;
                            rowcli.Email = clirow.EMAIL;
                            rowcli.tipousuario = clirow.TIPOUSUARIO;
                            Session["FRNADV"] = int.Parse(clirow.CODCON);
                            break;
                        case 20:
                        case 21:
                            //Gerente Predial
                            Tipo = TipoUsuario.UsuarioSite;
                            rowcli = dNeon.clientes.NewclientesRow();
                            rowcli.Nome = clirow.NOME;
                            rowcli.CODCON = clirow.CODCON;
                            rowcli.bloco = clirow.BLOCO;
                            rowcli.Apartamento = clirow.APARTAMENTO;
                            rowcli.Email = clirow.EMAIL;
                            rowcli.tipousuario = clirow.TIPOUSUARIO;
                            EMP = clirow.TIPOUSUARIO == 20 ? 1 : 3;
                            break;
                        case 22:
                        case 23:
                            //Zelador
                            Tipo = TipoUsuario.UsuarioSite;
                            rowcli = dNeon.clientes.NewclientesRow();
                            rowcli.Nome = clirow.NOME;
                            rowcli.CODCON = clirow.CODCON;
                            rowcli.bloco = clirow.BLOCO;
                            rowcli.Apartamento = clirow.APARTAMENTO;
                            rowcli.Email = clirow.EMAIL;
                            rowcli.tipousuario = clirow.TIPOUSUARIO;
                            EMP = clirow.TIPOUSUARIO == 22 ? 1 : 3;
                            break;
                        case 24:
                        case 25:
                            //Subsindico
                            Tipo = TipoUsuario.sindico;
                            rowcli = dNeon.clientes.NewclientesRow();
                            rowcli.Nome = clirow.NOME;
                            rowcli.CODCON = clirow.CODCON;
                            rowcli.bloco = clirow.BLOCO;
                            rowcli.Apartamento = clirow.APARTAMENTO;
                            rowcli.Email = clirow.EMAIL;
                            rowcli.tipousuario = clirow.TIPOUSUARIO;
                            EMP = clirow.TIPOUSUARIO == 24 ? 1 : 3;
                            break;
                        case 26:
                        case 27:
                            //Portaria
                            Tipo = TipoUsuario.UsuarioSite;
                            rowcli = dNeon.clientes.NewclientesRow();
                            rowcli.Nome = clirow.NOME;
                            rowcli.CODCON = clirow.CODCON;
                            rowcli.bloco = clirow.BLOCO;
                            rowcli.Apartamento = clirow.APARTAMENTO;
                            rowcli.Email = clirow.EMAIL;
                            rowcli.tipousuario = clirow.TIPOUSUARIO;
                            EMP = clirow.TIPOUSUARIO == 26 ? 1 : 3;
                            break;
                        default:
                            return null;                                                        
                    }
                    rowcli.APT = 0;
                    rowcli.PES = 0;
                    rowcli.CpfCnpj = "";
                    rowcli.Proprietario = false;
                    Credenciais Cred = new Credenciais(TipoAut.MSSQL1, Tipo, rowcli);
                    Cred.EMP = EMP;
                    if (EMP != 0)
                    {
                        if (EMP == 3) 
                            dNeon.AutenticaTableAdapter.Connection.ConnectionString = dNeon.AutenticaTableAdapter.Connection.ConnectionString.Replace("Initial Catalog=NEON", "Initial Catalog=NEONSA");
                        Cred.CON = (int)dNeon.AutenticaTableAdapter.BuscaCON(EMP, rowcli.CODCON);
                        rowcli.CONNome = (string)dNeon.AutenticaTableAdapter.BuscaCONNome(EMP, rowcli.CODCON);
                    }
                    return Cred;
                }
            }
            catch
            { 
            }
            return null;
        }

        public string Carregar()
        {
            if ((Request.QueryString["Ex"] != null) && (Request.QueryString["Ex"] != "Segunda"))
                return "onload = top.location.replace('Login.aspx?Ex=Segunda')";
            else
                return "";
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            LabelExp.Visible = (Request.QueryString["Ex"] != null);
            if (!IsPostBack)
            {
                if ((Request.QueryString["User"] != null) && (Request.QueryString["Pass"] != null))
                {
                    TxUsuario.Text = Request.QueryString["User"];
                    TxSenha.Text = Request.QueryString["Pass"];
                }
                else
                    if ((Session["User"] != null) && (Session["Pass"] != null))
                    {
                        TxUsuario.Text = Session["User"].ToString();
                        TxSenha.Text = Session["Pass"].ToString();
                    }
                    else
                        if (Request.Cookies["neon4"] != null)
                        {
                            HttpCookie cookie = (HttpCookie)Request.Cookies["neon4"];
                            if ((cookie.Values["id"] != "") && (cookie.Values["nome"] != ""))
                            {
                                TxUsuario.Text = cookie.Values["nome"].ToString();
                                TxSenha.Text = cookie.Values["id"].ToString();
                            };
                        }
                TxUsuario.Focus();
            }
            if ((TxUsuario.Text != "") && (TxSenha.Text != ""))
            {
                Credenciais Cred = Autentica();
                if (Cred != null)
                {
                    if (Cred.TUsuario == TipoUsuario.FRN)
                        Response.Redirect("Locacao.aspx");
                    else
                    {
                        Session["Cred"] = Cred;
                        Session["Username"] = TxUsuario.Text;
                        Session["Senha"] = TxSenha.Text;
                        if (Request.QueryString["Tipo"] != null)
                            Session["TipoAbertura"] = Request.QueryString["Tipo"];
                        if ((Cred.TAut == TipoAut.Neon1 || Cred.TAut == TipoAut.Neon2) && Cred.rowCli.CpfCnpj == "")
                            Response.Redirect("CpfCnpj.aspx");
                        else
                            Response.Redirect("Wbase.aspx");
                    }
                }
                else
                {
                    LabelNaoCad.Visible = true;
                }
            }
        }
    }
}
