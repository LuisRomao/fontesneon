﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace LoginMenu.NeonOnLine
{
    public partial class Solicitacoes : System.Web.UI.Page
    {
        private Credenciais Cred
        {
            get
            {
                return Credenciais.CredST(this, true);
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                //Preenche campos
                LCondominio.Text = Cred.rowCli.CONNome;
                if (Cred.rowCli.bloco.ToUpper() != "SB" && Cred.rowCli.bloco.ToUpper() != "XX")
                    Literal1.Text = string.Format("<tr><td class=\"Titulo\" >Bloco:</td>" +
                                                  "<td class=\"Celulas\">{0}</td></tr>",
                                                  Cred.rowCli.bloco);
                LApartamento.Text = Cred.rowCli.Apartamento.ToUpper() == "XXXX" ? "" : Cred.rowCli.Apartamento;
                LProprietario.Text = Cred.rowCli.Apartamento.ToUpper() == "XXXX" ? "" : (Cred.rowCli.Proprietario ? "Proprietário" : "Inquilino");
                LNome.Text = Cred.rowCli.Nome;
                TEmail.Text = Cred.rowCli.Email;
                TNomeCondomino.Text = Cred.rowCli.Nome;

                //Seta propriedades dos campos
                TData.Attributes.Add("onkeyup", "javascript:formataData(this);");
                THorario.Attributes.Add("onkeyup", "javascript:formataHora(this);");
                Ttelefone1.Attributes.Add("onkeyup", "javascript:somenteMascara(this,'0123456789- ');");
                Ttelefone2.Attributes.Add("onkeyup", "javascript:somenteMascara(this,'0123456789- ');");
                Ttelefone3.Attributes.Add("onkeyup", "javascript:somenteMascara(this,'0123456789- ');");
                string strPastaCondominio = string.Format("{0}_{1}", Cred.EMP, Cred.rowCli.CODCON.Replace(".", "_"));
                cmdTermos.Attributes.Add("onclick", "javascript:window.open('http://www.neonimoveis.com.br/neon23/neononline/condominios/" + strPastaCondominio + "/RegrasMudanca.pdf', '_blank', 'fullscreen=false')");
            }
            
        }

        private void geraemail(string Condominio, string Bloco, string Apartamento, string detalhes)
        {
            //Cria e-mail
            System.Net.Mail.SmtpClient SmtpClient1 = new System.Net.Mail.SmtpClient();

            //Verifica SMTP de dominio proprio
            VirMSSQL.TableAdapter TA = new VirMSSQL.TableAdapter();
            TA.Cone.ConnectionString = System.Configuration.ConfigurationManager.ConnectionStrings["ConnectionStringN2"].ConnectionString;
            System.Data.DataTable SMTP = TA.BuscaSQLTabela("SELECT CONSMTPHost, CONSMTPUser, CONSMTPPass, CONSMTPPort FROM CONDOMINIOS WHERE (CON = @P1) AND (CON_EMP = @P2) AND CONSMTPHost IS NOT NULL AND CONSMTPHost <> ''", Cred.CON, Cred.EMP);
            foreach (System.Data.DataRow DR in SMTP.Rows)
            {
                SmtpClient1.Host = DR[0].ToString();
                SmtpClient1.Credentials = new System.Net.NetworkCredential(DR[1].ToString(), DR[2].ToString());
                SmtpClient1.Port = Convert.ToInt32(DR[3]);
            }

            using (System.Net.Mail.MailMessage email = new System.Net.Mail.MailMessage())
            {
                //Bcc
                string Bcc = "";
                if (System.Configuration.ConfigurationManager.AppSettings["CopiarEmail" + Cred.EMP.ToString()] != null)
                    Bcc = System.Configuration.ConfigurationManager.AppSettings["CopiarEmail" + Cred.EMP.ToString()];
                if (Bcc != "")
                    email.Bcc.Add(Bcc);

                //From
                string strFrom = "";
                TA.BuscaEscalar("SELECT COMRemetente FROM CONDOMINIOS WHERE (CON = @P1) AND (CON_EMP = @P2) AND CONSMTPHost IS NOT NULL AND CONSMTPHost <> ''", out strFrom, Cred.CON, Cred.EMP);
                if (strFrom != "")
                    email.From = new System.Net.Mail.MailAddress(strFrom);
                else
                    if (Cred.EMP == 1)
                        email.From = new System.Net.Mail.MailAddress("neon@neonimoveis.com.br");
                    else
                        email.From = new System.Net.Mail.MailAddress("neonsa@neonimoveis.com.br");
                
                //To
                System.Data.DataTable DT = TA.BuscaSQLTabela("SELECT FCCEmail FROM FaleComCondominio WHERE (FCC_CON = @P1) AND (FCC_EMP = @P2) AND (FCCTipo = 1)", Cred.CON, Cred.EMP);
                foreach(System.Data.DataRow DR in DT.Rows)
                    email.To.Add(DR[0].ToString());

                //Corpo do email
                email.Bcc.Add("luis@virweb.com.br");                
                email.Subject = "Comunicação de Mudança";
                email.IsBodyHtml = true;
                email.Body = string.Format("<html>\r\n" + 
                                           "  <head>\r\n" + 
                                           "  </head>\r\n" + 
                                           "  <body>\r\n" +                                           
                                           "    <p>Data de envio: {0}</p>\r\n" + 
                                           "    <p>{1}: {2}</p>\r\n" + ((Bloco.ToUpper() == "SB") ? "" : "    <p>Bloco: {3}</p>\r\n") + 
                                           "    <p>Apartamento: {4}\r\n</p>\r\n" + 
                                           "    <p>Tipo:<B> {5}</b></p>\r\n" + 
                                           "    <p>\r\n" + 
                                           "    <table bordercolor=\"#75aadb\" cellspacing=\"2\" cellpadding=\"2\"\r\n" + 
                                           "        border=\"4\" frame=\"border\">                                                       \r\n" + "        <tbody>                                                                          \r\n" + "{6}" + "		</tbody>                                                                              \r\n" + "	  </table>                                                                              \r\n" + "	</p>                                                                                    \r\n" + "  </body>                                                                                \r\n" + "</html>                                                                                  ", DateTime.Now, Server.HtmlEncode("Condomínio"), Server.HtmlEncode(Condominio), Server.HtmlEncode(Bloco), Server.HtmlEncode(Apartamento), Server.HtmlEncode("Comunicação de Mudança"), detalhes);
                
                //Envia email
                SmtpClient1.Send(email);
            }
        }

        private string Detalhes()
        {
            string Retorno = "<table>";
            Retorno += string.Format("<tr><td>{0}</td><td>{1}</td></tr>", "Nome do Condômino:", TNomeCondomino.Text);
            Retorno += string.Format("<tr><td>{0}</td><td>{1}</td></tr>", "Responsável pela mudança:", TResponsavel.Text);
            Retorno += string.Format("<tr><td>{0}</td><td>{1}</td></tr>", "E-mail:", TEmail.Text);
            Retorno += string.Format("<tr><td>{0}</td><td>{1}</td></tr>", "Telefone 1:", Ttelefone1.Text);
            Retorno += string.Format("<tr><td>{0}</td><td>{1}</td></tr>", "Telefone 2:", Ttelefone2.Text);
            Retorno += string.Format("<tr><td>{0}</td><td>{1}</td></tr>", "Celular:", Ttelefone3.Text);
            Retorno += string.Format("<tr><td>{0}</td><td>{1}</td></tr>", "Data da mudança:", TData.Text);
            Retorno += string.Format("<tr><td>{0}</td><td>{1}</td></tr>", "Horário:", THorario.Text);
            Retorno += string.Format("<tr><td>{0}</td><td>{1}</td></tr>", "Placa do caminhão:", TPlaca.Text);
            Retorno += string.Format("<tr><td>{0}</td><td>{1}</td></tr>", "Empresa contratada:", TEmpresa.Text);
            Retorno += string.Format("<tr><td>{0}</td><td>{1}</td></tr>", "Uso de elevador:", CBElevador.Checked ? "SIM" : "NÃO");
            Retorno += string.Format("<tr><td>{0}</td><td>{1}</td></tr>", "Uso de escada:", CBEscada.Checked ? "SIM" : "NÃO");
            Retorno += string.Format("<tr><td>{0}</td><td>{1}</td></tr>", "Içamento:", CBIcamento.Checked ? "SIM" : "NÃO");
            Retorno += "</table>";
            return Retorno;
        }

        protected void chkTermos_CheckedChanged(object sender, EventArgs e)
        {
            //Habilita ou nao o botao de enviar
            cmdEnviar.Enabled = chkTermos.Checked;
        }

        protected void cmdEnviar_Click(object sender, EventArgs e)
        {
            //Limpa mensagem de erro
            LMsg.Text = "";
            LMsg.Visible = false;

            //Faz a consistência dos campos
            if (TEmail.Text == "")
            {
                LMsg.Text = "Informe um e-mail.";
                LMsg.Visible = true;
                return;
            }
            if (TData.Text == "")
            {
                LMsg.Text = "Informe a data da mudança.";
                LMsg.Visible = true;
                return;
            }
            if (Ttelefone1.Text == "")
            {
                LMsg.Text = "Informe o telefone (1) para contato.";
                LMsg.Visible = true;
                return;
            }

            //Envia email
            geraemail(Cred.rowCli.CONNome, Cred.rowCli.bloco, Cred.rowCli.Apartamento, Detalhes());

            //Enviou
            Session["txtConfirmar"] = "E-mails enviados";
            Response.Redirect("Confirmacao.aspx");
        }

        protected void TData_TextChanged(object sender, EventArgs e)
        {
            //Verifica data 
            DateTime parsedDateTime;
            if (TData.Text != "")
                if (!DateTime.TryParse(TData.Text, out parsedDateTime))
                {
                    TData.Text = "";
                    TData.Focus();
                }
                else
                    TData.Text = parsedDateTime.ToString("dd/MM/yyyy");

            //Ajusta selecao calendario
            CData.SelectedDates.Clear();
            if (TData.Text == "")
            {
                panData.Visible = false;
                cmdData.Text = ">>";
            }
            else
            {
                CData.SelectedDate = Convert.ToDateTime(TData.Text);
                CData.VisibleDate = CData.SelectedDate;
            }
        }

        protected void cmdData_Click(object sender, EventArgs e)
        {
            //Abre calendario na data atual ou na data selecionada
            panData.Visible = !panData.Visible;
            cmdData.Text = panData.Visible ? "<<" : ">>";

            //Ajusta selecao calendario
            if (TData.Text == "")
                CData.VisibleDate = System.DateTime.Today;
            else
                CData.VisibleDate = CData.SelectedDate = Convert.ToDateTime(TData.Text);
        }

        protected void CData_SelectionChanged(object sender, EventArgs e)
        {
            //Seta a data selecionada
            TData.Text = CData.SelectedDate.ToString("dd/MM/yyyy");
            CData.VisibleDate = CData.SelectedDate;
        }
    }
}