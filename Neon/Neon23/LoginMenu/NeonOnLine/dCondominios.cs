﻿using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace LoginMenu.NeonOnLine
{


    public partial class dCondominios
    {

        private static bool ProcuraDados(System.Web.UI.Page Chamador, int EMP, int CON)
        {
            dCondominios dCondominios1 = new dCondominios();
            using (dCondominiosTableAdapters.CONDOMINIOSTableAdapter Ta = new LoginMenu.NeonOnLine.dCondominiosTableAdapters.CONDOMINIOSTableAdapter())
            {
                if ((EMP != 0) && (CON != 0))
                    Ta.FillByCODCON(dCondominios1.CONDOMINIOS, EMP, CON);
                else
                    Ta.FillBy(dCondominios1.CONDOMINIOS, Chamador.Request.Url.Host.ToUpper().Trim());

                if (dCondominios1.CONDOMINIOS.Count > 0)
                {
                    using (dCondominiosTableAdapters.MENUSTableAdapter TaM = new LoginMenu.NeonOnLine.dCondominiosTableAdapters.MENUSTableAdapter())
                    {
                        TaM.Fill(dCondominios1.MENUS, dCondominios1.CONDOMINIOS[0].CON, dCondominios1.CONDOMINIOS[0].CON_EMP);
                        Chamador.Session["dCondominios"] = dCondominios1;
                    }
                }
                else
                    return false;
            }
            return true;
        }

        public static bool TemFaleComCondominio(int CON, int EMP)
        {

            dCondominiosTableAdapters.CONDOMINIOSTableAdapter Ta = new LoginMenu.NeonOnLine.dCondominiosTableAdapters.CONDOMINIOSTableAdapter();
            return ((int)Ta.ContatosFCC(CON, EMP) > 0);

        }

        public static bool SubSite(System.Web.UI.Page Chamador, int EMP, int CON)
        {
            if (Chamador.Session["dCondominios"] != null)
                return true;


            if ((EMP != 0) && (CON != 0))
            {
                int CONreal = CON / 100;
                if (CON != (100 * CONreal + (CONreal + 3) % 17))
                    return false;
                return ProcuraDados(Chamador, EMP, CONreal);
            }
            else
                if (!Chamador.Request.Url.Host.ToUpper().Contains("WWW.NEONIMOVEIS"))
                return ProcuraDados(Chamador, 0, 0);
            return false;
        }
    }
}
