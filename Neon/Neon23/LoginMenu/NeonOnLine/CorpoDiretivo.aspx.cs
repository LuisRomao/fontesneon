﻿using System;
using System.Collections.Generic;

using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace LoginMenu.NeonOnLine
{
    public partial class CorpoDiretivo : System.Web.UI.Page
    {
        private Credenciais Cred
        {
            get
            {
                return Credenciais.CredST(this, true);
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            //Sessao para grids
            Session["CON"] = Cred.CON;
            Session["EMP"] = Cred.EMP;
        }
    }
}