using System;
using System.Web.UI.WebControls;

namespace LoginMenu.NeonOnLine
{
    public partial class Atas : System.Web.UI.Page
    {
        private Credenciais Cred
        {
            get
            {
                return Credenciais.CredST(this, true);
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {                                    
            dAtas dataset = new dAtas();
            dAtasTableAdapters.assembleias1TableAdapter TA = new LoginMenu.NeonOnLine.dAtasTableAdapters.assembleias1TableAdapter();
            dAtasTableAdapters.DOCumentacaoTableAdapter TD = new dAtasTableAdapters.DOCumentacaoTableAdapter();
            short Tipo = short.Parse(Request.QueryString["Tipo"]);
            ViewState["Tipo"] = Tipo;
            switch (Tipo)
            {
                case 0:
                    Label1.Text = "Atas";
                    break;
                case 1:
                    Label1.Text = "Convocações";
                    break;
                case 2:
                    Label1.Text = "Regulamento interno";
                    GridView1.Columns[0].Visible = false;
                    GridView1.Columns[1].Visible = false;
                    break;
                case 3:
                    Label1.Text = "Convenção";
                    GridView1.Columns[0].Visible = false;
                    GridView1.Columns[1].Visible = false;                    
                    break;
                case 4:
                    Label1.Text = "Outros";
                    GridView1.Columns[0].Visible = false;
                    GridView1.Columns[1].Visible = false;
                    break;
                case 5:
                    Label1.Text = "Contratos Ativos";
                    GridView1.Columns[0].Visible = false;
                    GridView1.Columns[1].Visible = false;
                    break;
                case 6:
                    Label1.Text = "Contratos Histórico";
                    GridView1.Columns[0].Visible = false;
                    GridView1.Columns[1].Visible = false;
                    break;
            }
            if (Tipo == 5)
            {
                TD.Fill(dataset.DOCumentacao, Cred.CON, Cred.EMP);
                GridView1.DataSource = dataset.DOCumentacao;
            }
            else if (Tipo == 6)
            {
                TD.FillBy4(dataset.DOCumentacao, Cred.CON, Cred.EMP);
                GridView1.DataSource = dataset.DOCumentacao;
            }
            else
            {
                TA.Fill(dataset.assembleias1, Cred.rowCli.CODCON, Tipo, Cred.EMP.ToString());
                GridView1.DataSource = dataset.assembleias1;
            }
            GridView1.DataBind();
            Session["dAtas"] = dataset;
        }

        protected void GridView1_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            if (e.CommandName.ToUpper() == "ABRIR")
            {
                short Tipo = (short)(ViewState["Tipo"]);
                //Label1.Text = string.Format("{0} - {1} - {2}",e.CommandName,e.CommandArgument,e.CommandSource);
                int indice = int.Parse(e.CommandArgument.ToString());
                int Id = (int)GridView1.DataKeys[indice][0];
                //IDSel:=&Object(int32(DataGrid1.DataKeys[e.Item.ItemIndex])).ToString;
                dAtas dataset = (dAtas)Session["dAtas"];
                string Ext = ".html";
                if ((Tipo != 5) && (Tipo != 6))
                {
                    dAtas.assembleias1Row rowAss = dataset.assembleias1.FindByID(Id);
                    if (!rowAss.IsEXTNull())
                        Ext = rowAss.EXT;
                    Response.Redirect(string.Format("/publicacoes/{0}{1}", Id, Ext));
                }
                else
                {
                    dAtas.DOCumentacaoRow rowDOC = dataset.DOCumentacao.FindByID(Id);
                    Ext = rowDOC.EXT;
                    Response.Redirect(string.Format("/publicacoes/PF/{0}_{1}.{2}", Cred.EMP, Id, Ext));
                }
            }
        }
    }
}
