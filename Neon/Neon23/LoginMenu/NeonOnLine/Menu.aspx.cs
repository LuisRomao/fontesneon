using System.Collections;
using System;

namespace LoginMenu.NeonOnLine
{
    public partial class Menu : System.Web.UI.Page
    {
        private string strRow(string Nome, string target, string URL,int Nivel)
        {
            string classe = "menu-transparente";
            if (!Nome.StartsWith("|"))
                Nome = Server.HtmlEncode(Nome);
            else
                Nome = Nome.Substring(1);
            for (int i = 0; i < Nivel; i++)
                Nome = "&nbsp;&nbsp;&nbsp;&nbsp;" + Nome;
            string Tabela1 = 
            "            <tr>\r\n" +
            "                <td class=\"#classe#\">\r\n" +
            "                <a href=\"#URL#\" target=\"#target#\">#Nome#</a>\r\n" +
            "                </td>\r\n" +
            "            </tr>\r\n" +
            "            <tr>\r\n" +
            "                <td class=\"linha-branca\" >\r\n" +
            "                </td>\r\n" +
            "            </tr>\r\n";
            return Tabela1.Replace("#classe#", classe).Replace("#URL#", URL).Replace("#target#", target).Replace("#Nome#", Nome);
        }

        enum Grupos { condominio, unidade, sindico, adminstrativo, geral}

        private ArrayList usuAdministradora
        {
            get { return new ArrayList(new string[] { "0" }); }
        }
        private ArrayList usuAdministradoraAdvogado
        {
            get { return new ArrayList(new string[] { "16" }); }
        }
        private ArrayList usuAdvogado
        {
            get { return new ArrayList(new string[] { "14", "15" }); }
        }
        private ArrayList usuSindico
        {
            get { return new ArrayList(new string[] { "12", "13" }); }
        }
        private ArrayList usuSubSindico
        {
            get { return new ArrayList(new string[] { "24", "25" }); }
        }
        private ArrayList usuGerentePredial
        {
            get { return new ArrayList(new string[] { "20", "21" }); }
        }
        private ArrayList usuZelador
        {
            get { return new ArrayList(new string[] { "22", "23" }); }
        }
        private ArrayList usuPortaria
        {
            get { return new ArrayList(new string[] { "26", "27" }); }
        }

        private string finallink = "";

        private string Linkcomfinal(string Link)
        {
            if (finallink == "")
                return Link;
            if (Link.Contains("?"))
                return string.Format("{0}&{1}",Link,finallink);
            else
                return string.Format("{0}?{1}", Link, finallink);
        }

        private string URLMenuPreto(string cadastrado)
        {
            if (cadastrado.Contains("http:"))
                return cadastrado;
            else
                return string.Format("condominios/{0}/{1}", pasta, cadastrado);
        }

        private string strTabelas(Grupos Grupo)
        {
            dCondominios dCondominiosLogado = (dCondominios)Session["dCondominios"];
            dCondominios.CONDOMINIOSRow rowCON = null;
            
            if (Request["EMP"] != null)
                finallink = string.Format("EMP={0}&CON={1}", Request["EMP"], Request["CON"]);
            string Retorno =
            "        <table style=\"width: 90%; \" cellpadding=\"0\" cellspacing=\"0\">\r\n";

            //Monta os menus
            switch (Grupo)
            {
                case Grupos.unidade:
                    //Seta o condominio
                    if (dCondominiosLogado == null)
                    {
                        dCondominios dCondominiosX = new dCondominios();
                        using (dCondominiosTableAdapters.CONDOMINIOSTableAdapter Ta = new LoginMenu.NeonOnLine.dCondominiosTableAdapters.CONDOMINIOSTableAdapter())
                        {
                            if (Ta.FillByCODCON(dCondominiosX.CONDOMINIOS, (Cred.EMP == 2 ? 3 : Cred.EMP), Cred.CON) > 0)
                                rowCON = dCondominiosX.CONDOMINIOS[0];
                        }
                    }
                    else
                        rowCON = dCondominiosLogado.CONDOMINIOS.FindByCON_EMPCON((Cred.EMP == 2 ? 3 : Cred.EMP), Cred.CON);

                    //Monta o menu azul
                    if (usuPortaria.Contains(Cred.rowCli.tipousuario.ToString()))
                        break;

                    if (Cred.TUsuario != TipoUsuario.advogado)
                    {
                        Retorno += strRow("Cadastro", "", Linkcomfinal("Menu.aspx?mod=Cadastro"), 0);
                        if ((Request.QueryString["mod"] != null) && (Request.QueryString["mod"].ToString() == "Cadastro"))
                        {
                            Retorno += strRow("Condom�nio", "mainFrame", Linkcomfinal("dtCondominio.aspx"), 1);
                            if (!Cred.rowCli.IsApartamentoNull())
                                if (Cred.rowCli.Apartamento.ToUpper() != "XXXX")
                                    Retorno += strRow("Unidade", "mainFrame", Linkcomfinal("Cadastro.aspx"), 1);
                        };

                        Retorno += strRow("Corpo Diretivo", "mainFrame", Linkcomfinal("CorpoDiretivo.aspx"), 0);

                        if (!Cred.rowCli.IsApartamentoNull())
                            if (Cred.rowCli.Apartamento.ToUpper() != "XXXX")
                                Retorno += strRow("|Boletos<br/>Segunda Via<br/>Acordos", "mainFrame", Linkcomfinal("boletos.aspx"), 0);

                        if (rowCON != null)
                        {
                            if ((Cred != null) && (rowCON.CONFaleComCD || (rowCON.CONFaleCom && dCondominios.TemFaleComCondominio(Cred.CON, (Cred.EMP == 2 ? 3 : Cred.EMP)))))
                                Retorno += strRow("Fale Conosco", "mainFrame", Linkcomfinal("FaleCom.aspx"), 0);
                        }

                        if (Cred.TUsuario != TipoUsuario.UsuarioSite)
                            Retorno += strRow("Balancetes", "mainFrame", Linkcomfinal("Balancetes.aspx"), 0);
                    }

                    Retorno += strRow("Documenta��o", "", Linkcomfinal("Menu.aspx?mod=Documentacao"), 0);
                    if ((Request.QueryString["mod"] != null) && (Request.QueryString["mod"].ToString() == "Documentacao"))
                    {
                        Retorno += strRow("Convoca��o", "mainFrame", Linkcomfinal("Atas.aspx?Tipo=1"), 1);
                        Retorno += strRow("Atas", "mainFrame", Linkcomfinal("Atas.aspx?Tipo=0"), 1);
                        Retorno += strRow("Conven��o", "mainFrame", Linkcomfinal("Atas.aspx?Tipo=3"), 1);
                        Retorno += strRow("Regulamento", "mainFrame", Linkcomfinal("Atas.aspx?Tipo=2"), 1);
                        Retorno += strRow("Outros", "mainFrame", Linkcomfinal("Atas.aspx?Tipo=4"), 1);
                    };

                    if (Cred.TUsuario != TipoUsuario.advogado)
                    {
                        if (rowCON != null)
                        {
                            if (rowCON.CONControleMudanca)
                            { Retorno += strRow("Controle de Mudan�a", "mainFrame", Linkcomfinal("Solicitacoes.aspx"), 0); }

                            if (rowCON.CONAutorizacaoFornecedores)
                            { Retorno += strRow("Autoriza��o Entrada Prestadores de Servi�o", "mainFrame", Linkcomfinal("AutorizacaoFornecedores.aspx"), 0); }

                            if (rowCON.CONSolicitacaoReparos)
                            { Retorno += strRow("Solicita��o de Reparo na �rea Comum", "mainFrame", Linkcomfinal("SolicitacaoReparos.aspx"), 0); }

                            if (rowCON.CONSugestoes)
                            { Retorno += strRow("Caixa de Sugest�es", "mainFrame", Linkcomfinal("Sugestoes.aspx"), 0); }

                            if (rowCON.CONClassificados1)
                            { Retorno += strRow("Classificados Internos", "mainFrame", Linkcomfinal("PesquisaClassificados.aspx"), 0); }

                            if (rowCON.CONReserva)
                            { Retorno += strRow("Reserva �rea Comum", "mainFrame", Linkcomfinal("Reservas.aspx"), 0); }
                        }
                    }

                    if (ComMenuPreto)
                        foreach (dCondominios.MENUSRow rowMEN in dCondominios1.MENUS)
                            if (rowMEN.MEMCondomino && !rowMEN.MEMSindico)
                                Retorno += strRow(rowMEN.MEMTitulo, "mainFrame", URLMenuPreto(rowMEN.MEMURL), 0); //string.Format("condominios/{0}/{1}", pasta, rowMEN.MEMURL)

                    break;

                case Grupos.sindico:
                    //Seta o condominio
                    if (dCondominiosLogado == null)
                    {
                        dCondominios dCondominiosX = new dCondominios();
                        using (dCondominiosTableAdapters.CONDOMINIOSTableAdapter Ta = new LoginMenu.NeonOnLine.dCondominiosTableAdapters.CONDOMINIOSTableAdapter())
                        {
                            if (Ta.FillByCODCON(dCondominiosX.CONDOMINIOS, (Cred.EMP == 2 ? 3 : Cred.EMP), Cred.CON) > 0)
                                rowCON = dCondominiosX.CONDOMINIOS[0];
                        }
                    }
                    else
                        rowCON = dCondominiosLogado.CONDOMINIOS.FindByCON_EMPCON((Cred.EMP == 2 ? 3 : Cred.EMP), Cred.CON);

                    //Monta o menu laranja
                    if ((Cred.TUsuario != TipoUsuario.advogado && Cred.TUsuario != TipoUsuario.UsuarioSite) || (Cred.TUsuario == TipoUsuario.advogado && Session["FRNADV"] != null))
                    {
                        Retorno += strRow("Extratos Bancario", "mainFrame", Linkcomfinal("Extratos2.aspx"), 0);
                        Retorno += strRow("Francesinha", "mainFrame", Linkcomfinal("Francesinha.aspx"), 0);
                    }

                    Retorno += strRow("Cond�minos", "mainFrame", Linkcomfinal("Moradores.aspx"), 0);

                    if (!usuZelador.Contains(Cred.rowCli.tipousuario.ToString()) && !usuPortaria.Contains(Cred.rowCli.tipousuario.ToString()))
                    {
                        Retorno += strRow("Acordos", "mainFrame", Linkcomfinal("Acordos.aspx"), 0);
                        Retorno += strRow("Inadimplentes", "mainFrame", Linkcomfinal("Inadimp.aspx"), 0);
                    }

                    if (Cred.TUsuario != TipoUsuario.advogado && Cred.TUsuario != TipoUsuario.UsuarioSite)
                        Retorno += strRow("Cheques", "mainFrame", Linkcomfinal("Cheques.aspx"), 0);

                    if (Cred.TUsuario != TipoUsuario.advogado && !usuPortaria.Contains(Cred.rowCli.tipousuario.ToString()))
                        if (rowCON != null)
                        {
                            if (rowCON.CONClassificados1)
                            {
                                Retorno += strRow("Classificados", "", Linkcomfinal("Menu.aspx?mod=Classificados"), 0);
                                if ((Request.QueryString["mod"] != null) && (Request.QueryString["mod"].ToString() == "Classificados"))
                                {
                                    Retorno += strRow("Incluir Cond�mino", "mainFrame", Linkcomfinal("ClassificadosInclusaoCondomino.aspx"), 1);
                                    Retorno += strRow("Busca Cond�mino", "mainFrame", Linkcomfinal("PesquisaCondomino.aspx"), 1);
                                }
                            }
                        }

                    if (Cred.TUsuario != TipoUsuario.UsuarioSite)
                        Retorno += strRow("Contratos Ativos", "mainFrame", Linkcomfinal("Atas.aspx?Tipo=5"), 0);

                    if (rowCON != null)
                    {
                        if (Cred.TUsuario != TipoUsuario.advogado)
                            if (rowCON.CONAvisoCorrespondencia)
                            { Retorno += strRow("Aviso de Correspond�ncia", "mainFrame", Linkcomfinal("AvisoCorrespondencia.aspx"), 0); }

                        if (Cred.TUsuario != TipoUsuario.advogado && Cred.TUsuario != TipoUsuario.UsuarioSite)
                        {
                            if (rowCON.CONCadastroEmail)
                            { Retorno += strRow("Cadastro de E-mails", "mainFrame", Linkcomfinal("CadastroEmail.aspx"), 0); }
                            if (rowCON.CONCadastroUsuario)
                            { Retorno += strRow("Cadastro de Usu�rios", "mainFrame", Linkcomfinal("CadastroUsuario.aspx"), 0); }
                        }

                        if (Cred.TUsuario != TipoUsuario.advogado && !usuZelador.Contains(Cred.rowCli.tipousuario.ToString()) && !usuPortaria.Contains(Cred.rowCli.tipousuario.ToString()))
                            if (rowCON.CONCirculares)
                            { Retorno += strRow("Circulares", "mainFrame", Linkcomfinal("Circulares.aspx"), 0); }

                        if (!usuPortaria.Contains(Cred.rowCli.tipousuario.ToString()))
                            if (rowCON.CONReserva)
                            {
                                if (Cred.TiposSindico.Contains(Cred.TUsuario) && Cred.TUsuario != TipoUsuario.advogado)
                                {
                                    Retorno += strRow("Reserva �rea Comum", "", Linkcomfinal("Menu.aspx?mod=Reservas"), 0);
                                    if ((Request.QueryString["mod"] != null) && (Request.QueryString["mod"].ToString() == "Reservas"))
                                    {
                                        Retorno += strRow("Reservas", "mainFrame", Linkcomfinal("Reservas.aspx"), 1);
                                        Retorno += strRow("Bloqueio Data", "mainFrame", Linkcomfinal("ReservasBloqueioData.aspx"), 1);
                                        Retorno += strRow("Bloqueio Unidade", "mainFrame", Linkcomfinal("ReservasBloqueioUnidade.aspx"), 1);                                
                                        Retorno += strRow("Hist�rico Reservas", "mainFrame", Linkcomfinal("ReservasHistorico.aspx"), 1);
                                    }
                                }
                            }
                    }

                    if (ComMenuPreto)
                        foreach (dCondominios.MENUSRow rowMEN in dCondominios1.MENUS)
                            if (rowMEN.MEMSindico)
                                Retorno += strRow(rowMEN.MEMTitulo, "mainFrame", URLMenuPreto(rowMEN.MEMURL), 0);
                    break;

                case Grupos.adminstrativo:
                    //Monta o menu verde
                    Retorno += strRow("Condom�nios", "mainFrame", Linkcomfinal("condominios.aspx"), 0);

                    if(Cred.rowCli.tipousuario != 16)
                    {
                        Retorno += strRow("Configurar o site", "mainFrame", "../adminsite", 0);
                        Retorno += strRow("Cadastro de Usu�rios", "mainFrame", Linkcomfinal("CadastroUsuario.aspx"), 0);
                        Retorno += strRow("Hist�rico Reservas", "mainFrame", Linkcomfinal("ReservasHistorico.aspx"), 0);
                    }

                    Session["ADM"] = "LIBERADO";

                    break;

                case Grupos.condominio:
                    //Monta o menu preto
                    if (ComMenuPreto)
                        foreach (dCondominios.MENUSRow rowMEN in dCondominios1.MENUS)
                            if (!rowMEN.MEMCondomino && !rowMEN.MEMSindico)
                                Retorno += strRow(rowMEN.MEMTitulo, "mainFrame", URLMenuPreto(rowMEN.MEMURL), 0);

                    break;

                case Grupos.geral:
                    Retorno +=
               "            <tr>\r\n" +
               "                <td class=\"menu-azul\">\r\n" +
               "                <a style=\"cursor: hand\" onclick=\"imprime()\">Imprimir</a>\r\n" +
               "                </td>\r\n" +
               "            </tr>\r\n" +
               "            <tr>\r\n" +
               "                <td class=\"linha-branca\" >\r\n" +
               "                </td>\r\n" +
               "            </tr>\r\n";
                    break;
            };
            Retorno +=
            "        </table>";
            return Retorno; 
        }


        private dCondominios dCondominios1
        {
            get 
            {
                int EMP = 0;
                int CON = 0;
                if (Request["EMP"] != null)
                    int.TryParse(Request["EMP"], out EMP);
                if (Request["CON"] != null)
                    int.TryParse(Request["CON"], out CON);
                if(dCondominios.SubSite(this,EMP,CON))
                    return (dCondominios)Session["dCondominios"];
                else
                    return null;                                
            }
        }

        private Credenciais Cred
        {
            get
            {
                Credenciais ret = Credenciais.CredST(this,false);
                
                //Credenciais ret = (Credenciais)Session["Cred"];
                if ((ret == null) && (Session["dCondominios"] == null))
                {
                    //if(HEMP.Value != "-1")
                    //    Response.Redirect(string.Format("WBase.aspx?EMP={0}&CON={1}&Ex=1",HEMP.Value,HCON.Value));
                    //else
                        Response.Redirect("Login.aspx?Ex=1");
                }
                return ret;
                
            }
        }

        string pasta;

        private bool ComMenuPreto;

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                ComMenuPreto = ((dCondominios1 != null) && (dCondominios1.MENUS.Count > 0));
                if (ComMenuPreto)
                {
                    pasta = string.Format("{0}_{1}", dCondominios1.CONDOMINIOS[0].CON_EMP, dCondominios1.CONDOMINIOS[0].CONCodigo.Replace(".", "_"));
                    Table1.Rows[0].Cells[0].Text = strTabelas(Grupos.condominio);
                    Table1.Rows[0].Visible = true;
                    Table1.Rows[1].Visible = true;
                }
                else
                {
                    Table1.Rows[0].Visible = false;
                    Table1.Rows[1].Visible = false;
                    pasta = "";
                }

                if ((Cred != null) && (Cred.TiposUnidade.Contains(Cred.TUsuario)) && (!Cred.rowCli.IsCODCONNull()))
                {
                    Table1.Rows[2].Cells[0].Text = strTabelas(Grupos.unidade);
                    Table1.Rows[2].Visible = true;
                    Table1.Rows[3].Visible = true;
                }
                else
                {
                    Table1.Rows[2].Visible = false;
                    Table1.Rows[3].Visible = false;
                }
                if ((Cred != null) && (Cred.TiposSindico.Contains(Cred.TUsuario))
                     &&
                     (!Cred.rowCli.IsCODCONNull())
                     &&
                     (Cred.rowCli.CODCON != "")
                   )
                {
                    Table1.Rows[4].Cells[0].Text = strTabelas(Grupos.sindico);
                    Table1.Rows[4].Visible = true;
                    Table1.Rows[5].Visible = true;

                }
                else
                {
                    Table1.Rows[4].Visible = false;
                    Table1.Rows[5].Visible = false;
                }

                if ((Cred != null) 
                    &&
                    (
                      (Cred.TiposADM.Contains(Cred.TUsuario))
                      ||
                      (Cred.rowCli.tipousuario == 16)
                    )
                   )
                {                    
                    Table1.Rows[6].Cells[0].Text = strTabelas(Grupos.adminstrativo);
                    Table1.Rows[6].Visible = true;
                    Table1.Rows[7].Visible = true;
                }
                else
                {
                    Table1.Rows[6].Visible = false;
                    Table1.Rows[7].Visible = false;
                };
                Table1.Rows[8].Cells[0].Text = strTabelas(Grupos.geral);
                HEMP.Value = Request["EMP"] ?? "-1";
                HCON.Value = Request["CON"] ?? "-1";
            }
            
        }
    }
}
