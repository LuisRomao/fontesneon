﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.IO;

namespace LoginMenu.NeonOnLine
{
   public partial class Sugestoes : System.Web.UI.Page
   {
      private Credenciais Cred
      {
         get
         {
            return Credenciais.CredST(this, true);
         }
      }

      protected void Page_Load(object sender, EventArgs e)
      {
         //Sessao para grids
         Session["CON"] = Cred.CON;
         Session["EMP"] = Cred.EMP;

         if (!IsPostBack)
         {
            if (!Cred.rowCli.IsEmailNull())
               TEmail.Text = Cred.rowCli.Email;
            dAnexos.Anexos.Clear();
            conteudo.Text = "";
            GridView2.DataBind();
         }
      }

      private void enviarEmail()
      {
         //Dados do email
         string titulo = Server.HtmlEncode("Caixa de Sugestões");
         string quadro = "    <p>\r\n" +
                         "      <table bordercolor=\"#75aadb\" cellspacing=\"2\" cellpadding=\"2\"\r\n" +
                         "        border=\"4\" frame=\"border\">\r\n" +
                         "        <tbody>\r\n" +
                         "		  <tr>\r\n" +
                         "			<td>\r\n" +
                         Server.HtmlEncode(conteudo.Text).Replace("\r\n", "<br />") + "\r\n" +
                         "			</td>\r\n" +
                         "		  </tr>\r\n" +
                         "		</tbody>\r\n" +
                         "	  </table>\r\n" +
                         "	</p>\r\n";

         //Cria e-mail
         System.Net.Mail.SmtpClient SmtpClient1 = new System.Net.Mail.SmtpClient();

         //Verifica SMTP de dominio proprio
         VirMSSQL.TableAdapter TA = new VirMSSQL.TableAdapter();
         TA.Cone.ConnectionString = System.Configuration.ConfigurationManager.ConnectionStrings["ConnectionStringN2"].ConnectionString;
         System.Data.DataTable SMTP = TA.BuscaSQLTabela("SELECT CONSMTPHost, CONSMTPUser, CONSMTPPass, CONSMTPPort FROM CONDOMINIOS WHERE (CON = @P1) AND (CON_EMP = @P2) AND CONSMTPHost IS NOT NULL AND CONSMTPHost <> ''", Cred.CON, Cred.EMP);
         foreach (System.Data.DataRow DR in SMTP.Rows)
         {
             SmtpClient1.Host = DR[0].ToString();
             SmtpClient1.Credentials = new System.Net.NetworkCredential(DR[1].ToString(), DR[2].ToString());
             SmtpClient1.Port = Convert.ToInt32(DR[3]);
         }

         using (System.Net.Mail.MailMessage email = new System.Net.Mail.MailMessage())
         {
            //Bcc
            string Bcc = "";
            if (System.Configuration.ConfigurationManager.AppSettings["CopiarEmail" + Cred.EMP.ToString()] != null)
                Bcc = System.Configuration.ConfigurationManager.AppSettings["CopiarEmail" + Cred.EMP.ToString()];
            if (Bcc != "")
                email.Bcc.Add(Bcc);

            //From
            string strFrom = "";
            TA.BuscaEscalar("SELECT CONRemetente4 FROM CONDOMINIOS WHERE (CON = @P1) AND (CON_EMP = @P2) AND CONSMTPHost IS NOT NULL AND CONSMTPHost <> ''", out strFrom, Cred.CON, Cred.EMP);
            if (strFrom != "")
               email.From = new System.Net.Mail.MailAddress(strFrom);
            else
                if (Cred.EMP == 1)
                  email.From = new System.Net.Mail.MailAddress("neon@neonimoveis.com.br");
               else
                  email.From = new System.Net.Mail.MailAddress("neonsa@neonimoveis.com.br");

            //Replay
            email.ReplyToList.Add(new System.Net.Mail.MailAddress(TEmail.Text));

            //To
            System.Data.DataTable DT = TA.BuscaSQLTabela("SELECT FCCEmail FROM FaleComCondominio WHERE (FCC_CON = @P1) AND (FCC_EMP = @P2) AND (FCCTipo = 4)", Cred.CON, Cred.EMP);
            foreach (System.Data.DataRow DR in DT.Rows)
               email.To.Add(DR[0].ToString());

            //Corpo do email
            string strMsgResponsabilidade = "    <p>Esta é uma mensagem enviada através do site da Neon, o texto abaixo é de responsabilidade exclusiva do autor</p>";
            string strDominio = "";
            TA.BuscaEscalar("SELECT CONSiteDom FROM CONDOMINIOS WHERE (CON = @P1) AND (CON_EMP = @P2)", out strDominio, Cred.CON, Cred.EMP);
            if (!(strDominio == ""))
               strMsgResponsabilidade = "    <p></p>";
            email.Subject = "Caixa de Sugestões";
            email.IsBodyHtml = true;
            email.Body = "<html>\r\n" +
                         "  <head>\r\n" +
                         "  </head>\r\n" +
                         "  <body>\r\n" +
                         "    <p>Data: " + DateTime.Now.ToString() + "</p>\r\n" +
                         "    <p>Condomínio: " + Cred.rowCli.CODCON + "</p>" +
                         "    <p>Bloco: " + Cred.rowCli.bloco + "</p>" +
                         "    <p>Apartamento: " + Cred.rowCli.Apartamento + "</p>" +
                         "    <p>Nome: " + Cred.rowCli.Nome + "</p>" +
                         "    <p>E-mail: " + TEmail.Text + "</p>" +
                         "    <p>Tipo: <b>" + titulo + "</b></p>\r\n" +
                         strMsgResponsabilidade +
                         quadro +
                         "  </body>\r\n" +
                         "</html>";
            
            //Inclui anexos
            foreach (objetos.dAnexosFormularios.AnexosRow rowANE in dAnexos.Anexos)
            {
               MemoryStream ms = (MemoryStream)Session[NomeSessinAnexo(rowANE.ID)];
               email.Attachments.Add(new System.Net.Mail.Attachment(ms, rowANE.NomeAnexo));
            }

            //Envia email
            SmtpClient1.Send(email);

            //Limpa dados
            dAnexos.Anexos.Clear();
            conteudo.Text = "";
            GridView2.DataBind();
         };

         //Enviou
         Confirmado.Visible = true;
         Session["txtConfirmar"] = "E-mails enviados";
         Response.Redirect("Confirmacao.aspx");
      }

      protected void Button1_Click(object sender, EventArgs e)
      {
         enviarEmail();
      }

      protected void Button2_Click(object sender, EventArgs e)
      {
          byte[] Dados = FileUpload1.FileBytes;
          string NomeArquivo = FileUpload1.FileName;
          dAnexos.Anexos.AddAnexosRow(++nAnexos, NomeArquivo);
          Session[NomeSessinAnexo(nAnexos)] = new MemoryStream(Dados);
          GridView2.DataBind();
      }

      private int nAnexos;

      private objetos.dAnexosFormularios dAnexos
      {
         get
         {
            objetos.dAnexosFormularios _dAnexos = (objetos.dAnexosFormularios)Session["AnexosSug"];
            if (_dAnexos == null)
               Session["AnexosSug"] = _dAnexos = new objetos.dAnexosFormularios();
            nAnexos = _dAnexos.Anexos.Count == 0 ? 0 : _dAnexos.Anexos[_dAnexos.Anexos.Count - 1].ID;
            return _dAnexos;
         }
      }

      private string NomeSessinAnexo(int n)
      {
         return string.Format("An_{0}", n);
      }

      public objetos.dAnexosFormularios.AnexosDataTable AnexosCadastrados()
      {
         return dAnexos.Anexos;
      }

      public void ApagaAnexo(int ID)
      {
         objetos.dAnexosFormularios.AnexosRow rowdel = dAnexos.Anexos.FindByID(ID);
         if (rowdel != null)
            rowdel.Delete();
      }
   }
}