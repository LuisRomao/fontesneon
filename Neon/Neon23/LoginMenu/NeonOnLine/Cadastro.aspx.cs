using System;
using System.Data;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Collections;

namespace LoginMenu.NeonOnLine
{
    public partial class Cadastro : System.Web.UI.Page
    {
        private Credenciais Cred
        {
            get
            {
                return Credenciais.CredST(this, true);
            }
        }

        private dCadastro dCad = new dCadastro();
        private dRamoAtividade dRav = new dRamoAtividade();

        private SortedList Alteracoes = new SortedList();

        protected void Page_Load(object sender, EventArgs e)
        {
            //Seta EMP
            dCad.EMP = Cred.EMP;
            dRav.EMP = Cred.EMP;

            if (!IsPostBack)
            {
                //Preenche campos
                LCondominio.Text = Cred.rowCli.CONNome;
                if (Cred.rowCli.bloco.ToUpper() != "SB")
                    Literal1.Text = string.Format("<tr><td class=\"Titulo\" >Bloco:</td>" +
                                                  "<td class=\"Celulas\">{0}</td></tr>",
                                                  Cred.rowCli.bloco);
                LApartamento.Text = Cred.rowCli.Apartamento;
                LProprietario.Text = Cred.rowCli.Proprietario ? "Propriet�rio" : "Inquilino";
                LNome.Text = Cred.rowCli.Nome;

                //Seleciona Cliente
                if (dCad.ClienteTableAdapter.Fill(dCad.Cliente, Cred.rowCli.APT, Cred.rowCli.PES) > 0)
                {
                    //Pega dados do Cliente
                    dCadastro.ClienteRow rowCli = dCad.Cliente[0];
                    TEmail.Text = rowCli.Email;
                    TEndereco.Text = rowCli.Endereco;
                    TBairro.Text = rowCli.Bairro;
                    TCEP.Text = rowCli.CEP;
                    TCidade.Text = rowCli.Cidade;
                    TEstado.Text = rowCli.Estado;
                    TTelefone1.Text = rowCli.Telefone1;
                    TTelefone2.Text = rowCli.Telefone2;
                    TTelefone3.Text = rowCli.Telefone3;

                    //Preenche correio e email
                    int intAPTFormaPagto = rowCli.IsAPTFormaPagtoNull() ? 4 : rowCli.APTFormaPagto;
                    switch (intAPTFormaPagto)
                    {
                        //Com Correio / Sem E-mail
                        case 2:
                            CCorreio.Checked = true;
                            rdbEmailBoleto.SelectedValue = "0";
                            break;
                        //Sem Correio / Sem E-mail
                        case 4:
                            CCorreio.Checked = false;
                            rdbEmailBoleto.SelectedValue = "0";
                            break;
                        //Com Correio / Com Aviso
                        case 12:
                            CCorreio.Checked = true;
                            rdbEmailBoleto.SelectedValue = "1";
                            break;
                        //Sem Correio / Com Aviso
                        case 14:
                            CCorreio.Checked = false;
                            rdbEmailBoleto.SelectedValue = "1";
                            break;
                        //Com Correio / E-mail
                        case 22:
                            CCorreio.Checked = true;
                            rdbEmailBoleto.SelectedValue = "2";
                            break;
                        //Sem Correio / E-mail
                        case 24:
                            CCorreio.Checked = false;
                            rdbEmailBoleto.SelectedValue = "2";
                            break;
                        //Default - Sem Correio / Sem E-mail
                        default:
                            CCorreio.Checked = false;
                            rdbEmailBoleto.SelectedValue = "0";
                            break;
                    }

                    //Preenche seguro conteudo
                    PanelSeguro.Visible = false;
                    if (!rowCli.IsCON_PSCNull())
                    {
                        PanelSeguro.Visible = true;
                        CAPTSeguro.Checked = rowCli.APTSeguro;
                    }

                    //Preenche usuario
                    TUsuario.Text = rowCli.Usuario;

                    //Inicia condominio logado
                    dCondominios dCondominiosLogado = (dCondominios)Session["dCondominios"];
                    dCondominios.CONDOMINIOSRow rowCON = null;
                    if (dCondominiosLogado == null)
                    {
                        dCondominios dCondominiosX = new dCondominios();
                        dCondominiosTableAdapters.CONDOMINIOSTableAdapter Ta = new LoginMenu.NeonOnLine.dCondominiosTableAdapters.CONDOMINIOSTableAdapter();
                        if (Ta.FillByCODCON(dCondominiosX.CONDOMINIOS, Cred.EMP, Cred.CON) > 0)
                            rowCON = dCondominiosX.CONDOMINIOS[0];
                    }
                    else
                        rowCON = dCondominiosLogado.CONDOMINIOS.FindByCON_EMPCON(Cred.EMP, Cred.CON);

                    //Verifica se habilita campos de classificados (ativo ou nao para o condominio)
                    PanelClassificados.Visible = rowCON.CONClassificados1;
                    if (PanelClassificados.Visible)
                    {
                        //Carrega o campo de ramos de atividade do formulario
                        dRav.RamoAtividadeTableAdapter.Fill(dRav.RamoAtiVidade);
                        dtgRamoAtividade.DataSource = dRav.RamoAtiVidade;
                        dtgRamoAtividade.DataBind();

                        //Preenche dados classificados
                        TSite.Text = rowCli.Site;
                        TVideo.Text = rowCli.Video;
                        TDescricao.Text = rowCli.Descricao;

                        //Para cada ramo de atividade do cliente, seleciona a descricao na lista
                        dCad.PESxRAVTableAdapter.Fill(dCad.PESxRAV, Cred.rowCli.PES);
                        foreach (DataRow row in dCad.PESxRAV.Rows)
                        {
                            for (int i = 0; i < (dtgRamoAtividade.Items.Count); i++)
                            {
                                if (row["RAV"].ToString() == dtgRamoAtividade.Items[i].Cells[1].Text)
                                {
                                    lblRavOriginal.Text = lblRavOriginal.Text + "[" + row["RAV"].ToString() + "]";
                                    CheckBox chkSelecao = (CheckBox)(dtgRamoAtividade.Items[i].Cells[0].FindControl("chkSelecao") as CheckBox);
                                    chkSelecao.Checked = true;
                                }
                            }
                        }
                    }
                }

                //Guarda cadastro na sessao
                Session["dCad"] = dCad;
            }
        }

        protected void rdbEmailBoleto_SelectedIndexChanged(object sender, EventArgs e)
        {
            //Verifica se clicou na opcao de envio de boleto por e-mail e seta codigo
            lblCodigo.Text = "";
            if (rdbEmailBoleto.SelectedValue == "2") 
            {
                Random randNum = new Random();
                lblCodigo.Text = randNum.Next(999999).ToString("000000");
            }
        }

        protected void EnviaEmailConfirmacao(string Destino, string strCodigo)
        {
            //Cria e-mail
            System.Net.Mail.SmtpClient SmtpClient1 = new System.Net.Mail.SmtpClient();

            using (System.Net.Mail.MailMessage email = new System.Net.Mail.MailMessage())
            {
                //Bcc
                string Bcc = "";
                if (System.Configuration.ConfigurationManager.AppSettings["CopiarEmail" + Cred.EMP.ToString()] != null)
                    Bcc = System.Configuration.ConfigurationManager.AppSettings["CopiarEmail" + Cred.EMP.ToString()];
                if (Bcc != "")
                    email.Bcc.Add(Bcc);

                //From
                if (Cred.EMP == 1)
                    email.From = new System.Net.Mail.MailAddress("neon@neonimoveis.com.br");
                else
                    email.From = new System.Net.Mail.MailAddress("neonsa@neonimoveis.com.br");

                //To
                if (Destino != "")
                    email.To.Add(Destino);

                //Corpo do email
                email.Subject = "Confirma��o de E-mail";
                email.IsBodyHtml = true;
                email.Body = string.Format(
                            "<html>\r\n" + 
                            "   <head>\r\n" + 
                            "   </head>\r\n" + 
                            "   <body>\r\n" + 
                            "       <p><b>Data:</b> {0}</p>\r\n" +
                            "       <p><b>C�digo de Confirma��o:</b> {1}\r\n</p>\r\n" +
                            "       <p><i>Caro Cond�mino,</i></p>\r\n" + 
                            "       <p><i>Com muita satisfa��o, vimos por meio desta informar que recebemos sua solicita��o para o envio dos Boletos Mensais de Condom�nio <b>APENAS POR E-MAIL, com o acesso ao site <u>www.neonimoveis.com.br</u></b>, portanto, a partir da confirma��o deste e-mail o senhor(a) passar� a receb�-los exclusivamente de forma eletr�nica, sendo que fazendo-o, ajudar� a preservar o meio ambiente, e ainda, estar� contando com os principais benef�cios deste servi�o, quais sejam, agilidade, praticidade, conforto e economia.</i></p>\r\n" +
                            "       <p><i>Informamos que os boletos ficar�o como efetivamente j� ocorre, mensalmente dispon�veis no site da Neon Im�veis para consulta ou pagamento, resguardando-se os prazos administrativos e banc�rios, al�m do envio via e-mail mediante a presente solicita��o.</i></p>\r\n" +
                            "       <p><i>Resta por este interm�dio caracterizado como de inteira responsabilidade do solicitante, o acompanhamento das datas de vencimento, e principalmente de manuten��o do e-mail devidamente atualizado e configurado para recep��o do mesmo evitando qualquer tipo de transtorno, portanto, solicitamos que a caixa de e-mail seja assim configurada implicando no n�o envio � lixeira, ou mesmo ao chamado �spam�.</i></p>\r\n" +
                            "       <p><i>Ao confirmar a presente solicita��o para que haja a ativa��o do servi�o retro, Vossa Senhoria est� concordando e assumindo a responsabilidade legal para com o controle do presente envio e consequente pagamento conforme estipulado na legisla��o Civil, Lei n.� 10.406/2002, artigos 1336 Inciso I e  1347 e 1348 VII.</i></p>\r\n" +
                            "       <p><i>Agradecemos portanto a confian�a em n�s depositada!</i></p>\r\n" + 
                            "   </body>\r\n" + 
                            "</html>", 
                            DateTime.Now, strCodigo);

                //Envia email
                SmtpClient1.Send(email);
            }
        }

        protected void cmdReenviar_Click(object sender, EventArgs e)
        {
            //Limpa campo de erro
            lblErroGravar.Text = "";
            panErroGravar.Visible = false;

            //Envia novamente o e-mail de confirmacao
            EnviaEmailConfirmacao(TEmail.Text, lblCodigo.Text);

            //Indica e-mail reenviado
            lblErroGravar.Text = "E-mail reenviado.";
            panErroGravar.Visible = true;
        }

        protected void cmdConfirmar_Click(object sender, EventArgs e)
        {
            //Limpa campo de erro
            lblErroGravar.Text = "";
            panErroGravar.Visible = false;

            //Verifica se codigo nao confirma
            if (txtCodigo.Text != lblCodigo.Text)
            {
                lblErroGravar.Text = "C�digo de confirma��o de e-mail n�o confere. Verifique o c�digo digitado. Caso n�o tenha recebido o e-mail com o c�digo de confirma��o, clique em 'Reenviar' e verifique sua caixa de 'spam' ou sua lixeira.";
                panErroGravar.Visible = true;
                return;                
            }

            //Grava
            Gravar();

            //Volta pans
            panConfirmacaoEmail.Visible = false;
            panFormulario.Enabled = true;
        }

        protected void cmdCancelar_Click(object sender, EventArgs e)
        {
            //Seta a opcao default
            rdbEmailBoleto.SelectedValue = "1";
            txtCodigo.Text = lblCodigo.Text = "";

            //Grava
            Gravar();

            //Volta pans
            panConfirmacaoEmail.Visible = false;
            panFormulario.Enabled = true;
        }

        protected void cmdGravar_Click(object sender, EventArgs e)
        {
            try
            {               
                //Limpa campo de erro
                lblErroGravar.Text = "";
                panErroGravar.Visible = false;                

                //Esconde mensagens
                LAlteracoes.Visible = LErroAlteracoes.Visible = LJaCadastrado.Visible = LBoletoEmail.Visible = false;

                //Verifica confirmacao de email
                if (lblCodigo.Text != "")
                {                    
                    //Verifica se informou o e-mail
                    if (TEmail.Text == "")
                    {
                        LBoletoEmail.Visible = true;
                        return;
                    }

                    //Envia o e-mail de confirmacao
                    EnviaEmailConfirmacao(TEmail.Text, lblCodigo.Text);

                    //Abre tela de confirmacao
                    panConfirmacaoEmail.Visible = true;
                    panFormulario.Enabled = false;
                }
                else                                   
                    Gravar();                                    
            }
            catch (Exception ex) 
            {
                LErroAlteracoes.Text += ex.Message + "<br/>";
                LErroAlteracoes.Visible = true;
            }
        }

        protected void Gravar()
        {
            //Pega cadastro na sessao
            dCad = (dCadastro)Session["dCad"];
            dCadastro.ClienteRow rowCli = dCad.Cliente[0];

            //Verifica usuario e senha alterados
            if ((TNovaSenha.Text.Trim() != "" && TNovaSenha.Text != rowCli.Senha) || (TUsuario.Text.Trim() != "" && TUsuario.Text != rowCli.Usuario))
            {
                LJaCadastrado.Visible = false;
                object oID = null;
                //Base apartamentos
                dCadastroTableAdapters.ClienteTableAdapter tCli = new dCadastroTableAdapters.ClienteTableAdapter();
                oID = tCli.VerificaUsuarioSenha(TUsuario.Text, TNovaSenha.Text.Trim() != "" ? TNovaSenha.Text : rowCli.Senha);
                if ((oID != null) && ((int)oID != rowCli.PES))
                {
                    LJaCadastrado.Visible = true;
                    return;
                }
                //Base usuarios
                dCadastroTableAdapters.USUARIOSTableAdapter tUsu = new dCadastroTableAdapters.USUARIOSTableAdapter();
                oID = tUsu.VerificaUsuarioSenha(TUsuario.Text, TNovaSenha.Text.Trim() != "" ? TNovaSenha.Text : rowCli.Senha);
                if (oID != null)
                {
                    LJaCadastrado.Visible = true;
                    return;
                }
            };

            //Salva dados do Cliente
            dCad.PESSOASTableAdapter.Fill(dCad.PESSOAS, rowCli.PES);
            dCadastro.PESSOASRow rowPes = dCad.PESSOAS[0];
            if (TEmail.Text != rowCli.Email) { rowPes.PESEmail = TEmail.Text; Alteracoes.Add((Alteracoes.Count + 1).ToString("00") + " - E-mail", TEmail.Text); }
            if (TEndereco.Text != rowCli.Endereco) { rowPes.PESEndereco = TEndereco.Text; Alteracoes.Add((Alteracoes.Count + 1).ToString("00") + " - Endere�o", TEndereco.Text); }
            if (TBairro.Text != rowCli.Bairro) { rowPes.PESBairro = TBairro.Text; Alteracoes.Add((Alteracoes.Count + 1).ToString("00") + " - Bairro", TBairro.Text); }
            if (TCEP.Text != rowCli.CEP) { rowPes.PESCep = TCEP.Text; Alteracoes.Add((Alteracoes.Count + 1).ToString("00") + " - CEP", TCEP.Text); }
            if (TCidade.Text != rowCli.Cidade) 
            {
                if (TCidade.Text != rowCli.Cidade) Alteracoes.Add((Alteracoes.Count + 1).ToString("00") + " - Cidade/Estado", TCidade.Text + "/" + TEstado.Text);
                object objCID = dCad.CIDADESTableAdapter.BuscaCidade(TCidade.Text);
                int CID;
                if (objCID == null)
                {
                    //dCad.CIDADES.Clear();
                    dCadastro.CIDADESRow rowCid = dCad.CIDADES.NewCIDADESRow();
                    rowCid.CIDDataInclusao = DateTime.Now;
                    if (TCidade.Text.Trim() != "")
                        rowCid.CIDNome = TCidade.Text.Trim();
                    if (TEstado.Text.Trim() != "")
                        rowCid.CIDUf = TEstado.Text.Trim();
                    dCad.CIDADES.AddCIDADESRow(rowCid);
                    dCad.CIDADESTableAdapter.Update(rowCid);
                    CID = rowCid.CID;
                }
                else
                    CID = (int)objCID;
                rowPes.PES_CID = CID;
            }
            if (TTelefone1.Text != rowCli.Telefone1) { rowPes.PESFone1 = TTelefone1.Text; Alteracoes.Add((Alteracoes.Count + 1).ToString("00") + " - Telefone 1", TTelefone1.Text); }
            if (TTelefone2.Text != rowCli.Telefone2) { rowPes.PESFone2 = TTelefone2.Text; Alteracoes.Add((Alteracoes.Count + 1).ToString("00") + " - Telefone 2", TTelefone2.Text); }
            if (TTelefone3.Text != rowCli.Telefone3) { rowPes.PESFone3 = TTelefone3.Text; Alteracoes.Add((Alteracoes.Count + 1).ToString("00") + " - Telefone 3", TTelefone3.Text); }
            rowPes.PESSolicitante = "INTERNET";
            rowPes.PESDataAtualizacao = DateTime.Now;
            dCad.PESSOASTableAdapter.Update(rowPes);

            //Salva correio, email, seguro conteudo (se necessario), usuario e senha
            dCad.APARTAMENTOSTableAdapter.Fill(dCad.APARTAMENTOS, rowCli.APT);
            dCadastro.APARTAMENTOSRow rowApt = dCad.APARTAMENTOS[0];
            int intAPTFormaPagtoNova = 4;
            if (CCorreio.Checked && rdbEmailBoleto.SelectedValue == "0") intAPTFormaPagtoNova = 2;
            else if (!CCorreio.Checked && rdbEmailBoleto.SelectedValue == "0") intAPTFormaPagtoNova = 4;
            else if (CCorreio.Checked && rdbEmailBoleto.SelectedValue == "1") intAPTFormaPagtoNova = 12;
            else if (!CCorreio.Checked && rdbEmailBoleto.SelectedValue == "1") intAPTFormaPagtoNova = 14;
            else if (CCorreio.Checked && rdbEmailBoleto.SelectedValue == "2") intAPTFormaPagtoNova = 22;
            else if (!CCorreio.Checked && rdbEmailBoleto.SelectedValue == "2") intAPTFormaPagtoNova = 24;
            if (intAPTFormaPagtoNova != rowCli.APTFormaPagto)
            {
                if (rowCli.Proprietario == 1)
                    rowApt.APTFormaPagtoProprietario_FPG = intAPTFormaPagtoNova;
                else
                    rowApt.APTFormaPagtoInquilino_FPG = intAPTFormaPagtoNova;
                Alteracoes.Add((Alteracoes.Count + 1).ToString("00") + " - Correio", CCorreio.Checked ? "QUERO receber correspond�ncia por correio" : "N�O quero receber correspond�ncia por correio");
                Alteracoes.Add((Alteracoes.Count + 1).ToString("00") + " - E-mail Boletos", rdbEmailBoleto.SelectedItem.Text);
            }
            if (PanelSeguro.Visible && (CAPTSeguro.Checked != rowCli.APTSeguro))
            {
                rowApt.APTSeguro = CAPTSeguro.Checked;
                Alteracoes.Add((Alteracoes.Count + 1).ToString("00") + " - Seguro", CAPTSeguro.Checked ? "Ades�o ao programa seguro conte�do" : "exclus�o do programa seguro conte�do");
            }
            if (TUsuario.Text.Trim() != "" && TUsuario.Text != rowCli.Usuario) 
            { 
                if (rowCli.Proprietario == 1)
                    rowApt.APTUsuarioInternetProprietario = TUsuario.Text;
                else
                    rowApt.APTUsuarioInternetInquilino = TUsuario.Text;
                Alteracoes.Add((Alteracoes.Count + 1).ToString("00") + " - Usu�rio", TUsuario.Text); 
            }
            if (TNovaSenha.Text.Trim() != "" && TNovaSenha.Text != rowCli.Senha) 
            { 
                if (rowCli.Proprietario == 1)
                    rowApt.APTSenhaInternetProprietario = TNovaSenha.Text;
                else
                    rowApt.APTSenhaInternetInquilino = TNovaSenha.Text;
                Alteracoes.Add((Alteracoes.Count + 1).ToString("00") + " - Senha", TNovaSenha.Text); 
            }
            dCad.APARTAMENTOSTableAdapter.Update(rowApt);

            //Salva classificados
            if (PanelClassificados.Visible)
            {
                //Dados classificados
                if (TSite.Text != rowCli.Site || TVideo.Text != rowCli.Video || TDescricao.Text != rowCli.Descricao)
                {
                    dCad.ST_CLAssificadosTableAdapter.Fill(dCad.ST_CLAssificados, rowCli.PES);
                    dCadastro.ST_CLAssificadosRow rowCla;
                    if (dCad.ST_CLAssificados.Count > 0)
                        rowCla = dCad.ST_CLAssificados[0];
                    else
                    {
                        rowCla = dCad.ST_CLAssificados.NewST_CLAssificadosRow();
                        rowCla.CLA_PES = rowCli.PES;
                        dCad.ST_CLAssificados.AddST_CLAssificadosRow(rowCla);
                    }
                    if (TSite.Text != rowCli.Site) { rowCla.CLASite = TSite.Text; Alteracoes.Add((Alteracoes.Count + 1).ToString("00") + " - Site", TSite.Text); }
                    if (TVideo.Text != rowCli.Video) { rowCla.CLAVideo = TVideo.Text; Alteracoes.Add((Alteracoes.Count + 1).ToString("00") + " - V�deo", TVideo.Text); }
                    if (TDescricao.Text != rowCli.Descricao) { rowCla.CLADescricao = TDescricao.Text; Alteracoes.Add((Alteracoes.Count + 1).ToString("00") + " - Descri��o", TDescricao.Text); }
                    dCad.ST_CLAssificadosTableAdapter.Update(rowCla);
                }

                //Lista ramos de atividade
                string strRavAlterado = "";
                string strRavAlteradoDesc = "";
                for (int i = 0; i < (dtgRamoAtividade.Items.Count); i++)
                {
                    CheckBox chkSelecao = (CheckBox)(dtgRamoAtividade.Items[i].Cells[0].FindControl("chkSelecao") as CheckBox);
                    if (chkSelecao.Checked)
                    { 
                        strRavAlterado = strRavAlterado + "[" + dtgRamoAtividade.Items[i].Cells[1].Text + "]";
                        strRavAlteradoDesc = strRavAlteradoDesc + dtgRamoAtividade.Items[i].Cells[2].Text + ", ";
                    }
                }

                //Compara com a selecao oirginal
                if (lblRavOriginal.Text != strRavAlterado)
                {
                    //Apaga originais
                    dCad.PESxRAVTableAdapter.DeletePESxRAV(rowCli.PES);

                    //Insere novos
                    for (int i = 0; i < (dtgRamoAtividade.Items.Count); i++)
                    {
                        CheckBox chkSelecao = (CheckBox)(dtgRamoAtividade.Items[i].Cells[0].FindControl("chkSelecao") as CheckBox);
                        if (chkSelecao.Checked)
                            dCad.PESxRAVTableAdapter.InsertPESxRAV(rowCli.PES, int.Parse(dtgRamoAtividade.Items[i].Cells[1].Text));
                    }
                    if (strRavAlteradoDesc != "") { strRavAlteradoDesc = strRavAlteradoDesc.Substring(0, (strRavAlteradoDesc.Length - 2)); }
                    Alteracoes.Add((Alteracoes.Count + 1).ToString("00") + " - Ramos de Atividade", strRavAlteradoDesc);

                    //Atualiza ramos de atividade original
                    lblRavOriginal.Text = strRavAlterado;
                }
            }

            //Gera o email de atualizacao
            geraemail(rowCli.Email, Cred.rowCli.CONNome, Cred.rowCli.bloco, Cred.rowCli.Apartamento, geraCodigoEmail());

            //Atualiza dados
            if (dCad.ClienteTableAdapter.Fill(dCad.Cliente, Cred.rowCli.APT, Cred.rowCli.PES) > 0)
            {
                rowCli = dCad.Cliente[0];
                Session["dCad"] = dCad;
            }

            //Sucesso
            LAlteracoes.Visible = true;
        }

        private string geraCodigoEmail()
        {
            string retorno = "";
            if (Alteracoes == null)
                retorno = "Nenhum campo foi alterado";
            else
                foreach (DictionaryEntry DE in Alteracoes)
                {
                    string Titulo = DE.Key.ToString();
                    string Conteudo = DE.Value.ToString();
                    retorno += string.Format("<tr><td>{0}</td><td>{1}</td></tr>",
                        Server.HtmlEncode(Titulo), Server.HtmlEncode(Conteudo));
                }
            return retorno;
        }

        private void geraemail(string Destino,string Condominio,string Bloco,string Apartamento,string detalhes)
        {
            //Cria e-mail
            System.Net.Mail.SmtpClient SmtpClient1 = new System.Net.Mail.SmtpClient();
            
            using (System.Net.Mail.MailMessage email = new System.Net.Mail.MailMessage())
            {
                //Bcc
                string Bcc = "";
                if (System.Configuration.ConfigurationManager.AppSettings["CopiarEmail" + Cred.EMP.ToString()] != null)
                    Bcc = System.Configuration.ConfigurationManager.AppSettings["CopiarEmail" + Cred.EMP.ToString()];
                if (Bcc != "")
                    email.Bcc.Add(Bcc);

                //From
                if (Cred.EMP == 1)
                    email.From = new System.Net.Mail.MailAddress("neon@neonimoveis.com.br");
                else
                    email.From = new System.Net.Mail.MailAddress("neonsa@neonimoveis.com.br");

                //To
                if (Destino != "")
                    email.To.Add(Destino);
                else
                    if (Bcc == "")
                        return;
                    else
                        email.To.Add(Bcc);

                //Corpo do email
                email.Subject = "Altera��o de cadastro";
                email.IsBodyHtml = true;
                email.Body = string.Format("<html>\r\n" + 
                            "  <head>\r\n" + 
                            "  </head>\r\n" + 
                            "  <body>\r\n" + 
                            "    <p>Data:{0}</p>\r\n" + 
                            "    <p>{1}:{2}</p>\r\n" + 
                            ((Bloco.ToUpper() == "SB") ? "" : "    <p>Bloco:{3}</p>\r\n") + 
                            "    <p>Apartamento:{4}\r\n</p>\r\n" + 
                            "    <p>Tipo:<B>{5}</b></p>\r\n" + 
                            "    <p>\r\n" + 
                            "     <table bordercolor=\"#75aadb\" cellspacing=\"2\" cellpadding=\"2\" \r\n" + 
                            "        border=\"4\" frame=\"border\">\r\n" + 
                            "       <tbody>\r\n" + 
                            "{6}" + 
                            "		</tbody>\r\n" + 
                            "	  </table>\r\n" + 
                            "	</p>\r\n" + 
                            "  </body>\r\n" + 
                            "</html>", DateTime.Now, Server.HtmlEncode("Condom�nio"), Server.HtmlEncode(Condominio), Server.HtmlEncode(Bloco), Server.HtmlEncode(Apartamento), Server.HtmlEncode("Altera��o de cadastro"), detalhes);

                //Envia email
                SmtpClient1.Send(email);
            }            
        }
    }
}
