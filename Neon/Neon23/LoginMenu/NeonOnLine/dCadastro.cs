﻿namespace LoginMenu.NeonOnLine
{
    partial class dCadastro
    {
        public int EMP;

        private dCadastroTableAdapters.ClienteTableAdapter taClienteTableAdapter;
        /// <summary>
        /// TableAdapter padrão para tabela: Cliente
        /// </summary>
        public dCadastroTableAdapters.ClienteTableAdapter ClienteTableAdapter
        {
            get
            {
                if (taClienteTableAdapter == null)
                {
                    taClienteTableAdapter = new dCadastroTableAdapters.ClienteTableAdapter();
                    if (EMP == 3) taClienteTableAdapter.Connection.ConnectionString = taClienteTableAdapter.Connection.ConnectionString.Replace("Initial Catalog=NEON", "Initial Catalog=NEONSA");
                };
                return taClienteTableAdapter;
            }
        }

        private dCadastroTableAdapters.PESSOASTableAdapter taPESSOASTableAdapter;
        /// <summary>
        /// TableAdapter padrão para tabela: PESSOAS
        /// </summary>
        public dCadastroTableAdapters.PESSOASTableAdapter PESSOASTableAdapter
        {
            get
            {
                if (taPESSOASTableAdapter == null)
                {
                    taPESSOASTableAdapter = new dCadastroTableAdapters.PESSOASTableAdapter();
                    if (EMP == 3) taPESSOASTableAdapter.Connection.ConnectionString = taPESSOASTableAdapter.Connection.ConnectionString.Replace("Initial Catalog=NEON", "Initial Catalog=NEONSA");
                };
                return taPESSOASTableAdapter;
            }
        }

        private dCadastroTableAdapters.PESxRAVTableAdapter taPESxRAVTableAdapter;
        /// <summary>
        /// TableAdapter padrão para tabela: PESxRAV
        /// </summary>
        public dCadastroTableAdapters.PESxRAVTableAdapter PESxRAVTableAdapter
        {
            get
            {
                if (taPESxRAVTableAdapter == null)
                {
                    taPESxRAVTableAdapter = new dCadastroTableAdapters.PESxRAVTableAdapter();
                    if (EMP == 3) taPESxRAVTableAdapter.Connection.ConnectionString = taPESxRAVTableAdapter.Connection.ConnectionString.Replace("Initial Catalog=NEON", "Initial Catalog=NEONSA");
                };
                return taPESxRAVTableAdapter;
            }
        }

        private dCadastroTableAdapters.ST_CLAssificadosTableAdapter taST_CLAssificadosTableAdapter;
        /// <summary>
        /// TableAdapter padrão para tabela: ST_CLAssificadosTableAdapter
        /// </summary>
        public dCadastroTableAdapters.ST_CLAssificadosTableAdapter ST_CLAssificadosTableAdapter
        {
            get
            {
                if (taST_CLAssificadosTableAdapter == null)
                {
                    taST_CLAssificadosTableAdapter = new dCadastroTableAdapters.ST_CLAssificadosTableAdapter();
                    if (EMP == 3) taST_CLAssificadosTableAdapter.Connection.ConnectionString = taST_CLAssificadosTableAdapter.Connection.ConnectionString.Replace("Initial Catalog=NEON", "Initial Catalog=NEONSA");
                };
                return taST_CLAssificadosTableAdapter;
            }
        }

        private dCadastroTableAdapters.CIDADESTableAdapter taCIDADESTableAdapter;
        /// <summary>
        /// TableAdapter padrão para tabela: CIDADES
        /// </summary>
        public dCadastroTableAdapters.CIDADESTableAdapter CIDADESTableAdapter
        {
            get
            {
                if (taCIDADESTableAdapter == null)
                {
                    taCIDADESTableAdapter = new dCadastroTableAdapters.CIDADESTableAdapter();
                    if (EMP == 3) taCIDADESTableAdapter.Connection.ConnectionString = taCIDADESTableAdapter.Connection.ConnectionString.Replace("Initial Catalog=NEON", "Initial Catalog=NEONSA");
                };
                return taCIDADESTableAdapter;
            }
        }

        private dCadastroTableAdapters.APARTAMENTOSTableAdapter taAPARTAMENTOSTableAdapter;
        /// <summary>
        /// TableAdapter padrão para tabela: APARTAMENTOSTableAdapter
        /// </summary>
        public dCadastroTableAdapters.APARTAMENTOSTableAdapter APARTAMENTOSTableAdapter
        {
            get
            {
                if (taAPARTAMENTOSTableAdapter == null)
                {
                    taAPARTAMENTOSTableAdapter = new dCadastroTableAdapters.APARTAMENTOSTableAdapter();
                    if (EMP == 3) taAPARTAMENTOSTableAdapter.Connection.ConnectionString = taAPARTAMENTOSTableAdapter.Connection.ConnectionString.Replace("Initial Catalog=NEON", "Initial Catalog=NEONSA");
                };
                return taAPARTAMENTOSTableAdapter;
            }
        }






        private dCadastroTableAdapters.clientesTableAdapter clientesTableAdapter;
        /// <summary>
        /// TableAdapter padrão para tabela: Clientes
        /// </summary>
        public dCadastroTableAdapters.clientesTableAdapter ClientesTableAdapter
        {
            get
            {
                if (clientesTableAdapter == null)
                {
                    clientesTableAdapter = new dCadastroTableAdapters.clientesTableAdapter();
                };
                return clientesTableAdapter;
            }
        }

        private dCadastroTableAdapters.RetClienteTableAdapter retClienteTableAdapter;
        /// <summary>
        /// TableAdapter padrão para tabela: RetCliente
        /// </summary>
        public dCadastroTableAdapters.RetClienteTableAdapter RetClienteTableAdapter
        {
            get
            {
                if (retClienteTableAdapter == null)
                {
                    retClienteTableAdapter = new dCadastroTableAdapters.RetClienteTableAdapter();
                };
                return retClienteTableAdapter;
            }
        }

        private dCadastroTableAdapters.APARTAMENTOSN1TableAdapter aPARTAMENTOSN1TableAdapter;
        /// <summary>
        /// TableAdapter padrão para tabela: APARTAMENTOSN1
        /// </summary>
        public dCadastroTableAdapters.APARTAMENTOSN1TableAdapter APARTAMENTOSN1TableAdapter
        {
            get
            {
                if (aPARTAMENTOSN1TableAdapter == null)
                {
                    aPARTAMENTOSN1TableAdapter = new dCadastroTableAdapters.APARTAMENTOSN1TableAdapter();
                };
                return aPARTAMENTOSN1TableAdapter;
            }
        }

        private dCadastroTableAdapters.CLIxRAVTableAdapter CLIxRAVTableAdapter;
        /// <summary>
        /// TableAdapter padrão para tabela: CLIxRAV
        /// </summary>
        public dCadastroTableAdapters.CLIxRAVTableAdapter ClixRavTableAdapter
        {
            get
            {
                if (CLIxRAVTableAdapter == null)
                {
                    CLIxRAVTableAdapter = new dCadastroTableAdapters.CLIxRAVTableAdapter();
                };
                return CLIxRAVTableAdapter;
            }
        }

        private dCadastroTableAdapters.RetClienteRAVTableAdapter RetClienteRAVTableAdapter;
        /// <summary>
        /// TableAdapter padrão para tabela: RetClienteRAV
        /// </summary>
        public dCadastroTableAdapters.RetClienteRAVTableAdapter RetClienteRavTableAdapter
        {
            get
            {
                if (RetClienteRAVTableAdapter == null)
                {
                    RetClienteRAVTableAdapter = new dCadastroTableAdapters.RetClienteRAVTableAdapter();
                };
                return RetClienteRAVTableAdapter;
            }
        }
    }
}
