﻿namespace LoginMenu.NeonOnLine
{

    partial class dNeon2
    {
        private dNeon2TableAdapters.AutenticaTableAdapter autenticaTableAdapter;
        /// <summary>
        /// TableAdapter padrão para tabela: Autentica
        /// </summary>
        public dNeon2TableAdapters.AutenticaTableAdapter AutenticaTableAdapter
        {
            get
            {
                if (autenticaTableAdapter == null)
                {
                    autenticaTableAdapter = new dNeon2TableAdapters.AutenticaTableAdapter();
                };
                return autenticaTableAdapter;
            }
        }

        private dNeon2TableAdapters.USUARIOSTableAdapter uSUARIOSTableAdapter;
        /// <summary>
        /// TableAdapter padrão para tabela: USUARIOS
        /// </summary>
        public dNeon2TableAdapters.USUARIOSTableAdapter UsuariosTableAdapter
        {
            get
            {
                if (uSUARIOSTableAdapter == null)
                {
                    uSUARIOSTableAdapter = new dNeon2TableAdapters.USUARIOSTableAdapter();
                };
                return uSUARIOSTableAdapter;
            }
        }

        private dNeon2TableAdapters.FORNECEDORESTableAdapter fORNECEDORESTableAdapter;
        /// <summary>
        /// TableAdapter padrão para tabela: FORNECEDORES
        /// </summary>
        public dNeon2TableAdapters.FORNECEDORESTableAdapter FORNECEDORESTableAdapter
        {
            get
            {
                if (fORNECEDORESTableAdapter == null)
                {
                    fORNECEDORESTableAdapter = new dNeon2TableAdapters.FORNECEDORESTableAdapter();
                };
                return fORNECEDORESTableAdapter;
            }
        }
    }
}
