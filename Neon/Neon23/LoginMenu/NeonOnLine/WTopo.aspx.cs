using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

namespace LoginMenu.NeonOnLine
{
    public partial class WTopo : System.Web.UI.Page
    {
        private Credenciais Cred
        {
            get
            {
                Credenciais ret = (Credenciais)Session["Cred"];
                return ret;
            }
        }

        private void AjustaBotao(bool TemCookie)
        {
            if (!TemCookie)
            {
                LCookie.Text = "Para n�o ser necess�ria a digita��o do usu�rio e senha NESTE computador clique ";
                Button1.CommandName = "gravarC";
            }
            else
            {
                LCookie.Text = "Para VOLTAR a ser necess�ria a digita��o do usu�rio e senha NESTE computador clique ";
                Button1.CommandName = "apagarC";
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                AjustaBotao(Request.Cookies["neon4"] != null);                           
            }
            if (Session["Cred"] != null)
            {
                if (Cred.TUsuario == TipoUsuario.Desenvolvimento)
                {
                    LNome.Text = "Desenvolvedor";
                    if(Cred.rowCli.IsCONNomeNull())
                        Lcondominio.Text = "XCondominioX";
                    else
                        Lcondominio.Text = Cred.rowCli.CONNome;
                    LBloco.Text = "| Bloco: XX";
                    Lapartamento.Text = "NNNN";
                    Lclasse.Text = "TI";
                }
                if (Cred.TUsuario == TipoUsuario.Administradora)
                {                    
                    LNome.Text = Cred.rowCli.Nome;
                    if (Cred.rowCli.IsCONNomeNull())
                        Lcondominio.Text = "Administradora";
                    else
                        Lcondominio.Text = Cred.rowCli.CONNome;
                    LBloco.Text = "";
                    Lapartamento.Text = "";
                    Lclasse.Text = "ADM";
                }
                else if ((Cred.TAut == TipoAut.Neon1) || (Cred.TAut == TipoAut.Neon2) || (Cred.TAut == TipoAut.Neon0) || (Cred.TAut == TipoAut.MSSQL1))
                {
                    LNome.Text = Cred.rowCli.Nome;
                    Lcondominio.Text = Cred.rowCli.CONNome;
                    LBloco.Text = (Cred.rowCli.bloco.ToUpper() == "SB") ? "" : "| Bloco: " + Cred.rowCli.bloco;
                    Lapartamento.Text = Cred.rowCli.Apartamento;
                    if (Cred.TUsuario == TipoUsuario.advogado)
                        Lclasse.Text = "Advogado";
                    else if (Cred.TUsuario == TipoUsuario.UsuarioSite && (Cred.rowCli.tipousuario == 20 || Cred.rowCli.tipousuario == 21))
                       Lclasse.Text = "Gerente Predial";
                    else if (Cred.TUsuario == TipoUsuario.UsuarioSite && (Cred.rowCli.tipousuario == 22 || Cred.rowCli.tipousuario == 23))
                       Lclasse.Text = "Zelador";
                    else if (Cred.TUsuario == TipoUsuario.sindico && (Cred.rowCli.tipousuario == 24 || Cred.rowCli.tipousuario == 25))
                        Lclasse.Text = "Subs�ndico";
                    else if (Cred.TUsuario == TipoUsuario.sindico && (Cred.rowCli.tipousuario == 12 || Cred.rowCli.tipousuario == 13))
                       Lclasse.Text = "Subs�ndico";
                    else if (Cred.TiposSindico.Contains(Cred.TUsuario))
                       Lclasse.Text = "S�ndico";
                    else
                       Lclasse.Text = Cred.rowCli.Proprietario ? "Propriet�rio" : "Inquilino";
                }
            }
        }

        protected void Button1_Command(object sender, CommandEventArgs e)
        {
            if (e.CommandName == "apagarC")
            {
                HttpCookie cookie = new HttpCookie("neon4");
                cookie.Expires = new DateTime(1200,1,1);
                Response.Cookies.Add(cookie);
                AjustaBotao(false);
            }
            else if (e.CommandName == "gravarC")
            {
                HttpCookie cookie = new HttpCookie("neon4");
                cookie.Expires = DateTime.MaxValue;
                cookie.Values.Add("id"  ,Session["senha"].ToString());
                cookie.Values.Add("nome",Session["Username"].ToString());
                Response.Cookies.Add(cookie);
                AjustaBotao(true);
            } 
        }
    }
}
