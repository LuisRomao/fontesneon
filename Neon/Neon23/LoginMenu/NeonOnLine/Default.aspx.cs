﻿using System;
using System.Collections.Generic;

using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace LoginMenu.NeonOnLine
{
    public partial class Default : System.Web.UI.Page
    {
        /*
        private void CondominioEspecifico()
        {
            dCondominios dCondominios1 = new dCondominios();
            using (dCondominiosTableAdapters.CONDOMINIOSTableAdapter Ta = new LoginMenu.NeonOnLine.dCondominiosTableAdapters.CONDOMINIOSTableAdapter())
            {
                if ((Request["EMP"] != null) && (Request["CON"] != null))
                {
                    Ta.FillByCODCON(dCondominios1.CONDOMINIOS, int.Parse(Request["EMP"]), int.Parse(Request["CON"]));
                }
                else
                {
                    string SiteDom = Request.Url.Host.ToUpper();
                    Ta.FillBy(dCondominios1.CONDOMINIOS, SiteDom);
                }

                if (dCondominios1.CONDOMINIOS.Count > 0)
                {
                    using (dCondominiosTableAdapters.MENUSTableAdapter TaM = new LoginMenu.NeonOnLine.dCondominiosTableAdapters.MENUSTableAdapter())
                    {
                        TaM.Fill(dCondominios1.MENUS, dCondominios1.CONDOMINIOS[0].CON, dCondominios1.CONDOMINIOS[0].CON_EMP);
                        Session["dCondominios"] = dCondominios1;
                        Response.Redirect("WBase.aspx");
                    }
                    
                }
                else
                    //Label1.Text = string.Format("nenhum registro EMP=|{0}| CON=|{1}|", Request["EMP"], Request["CON"]);
                    Response.Redirect("..\\default.aspx");
            }           
        }*/

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {                                
                int EMP = 0;
                int CON = 0;
                if (Request["EMP"] != null)
                    int.TryParse(Request["EMP"], out EMP);
                if (Request["CON"] != null)
                    int.TryParse(Request["CON"], out CON);
                if(dCondominios.SubSite(this, EMP,CON))
                    Response.Redirect(string.Format("WBase.aspx?EMP={0}&CON={1}", EMP, CON));
                else
                    Response.Redirect("..\\default.aspx");
                //Label1.Text = string.Format("EMP=|{0}| CON=|{1}|", Request["EMP"], Request["CON"]);
                //CondominioEspecifico();
            }
        }
    }
}