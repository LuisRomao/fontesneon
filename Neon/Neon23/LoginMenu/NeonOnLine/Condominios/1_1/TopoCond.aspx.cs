﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace LoginMenu.NeonOnLine
{
    public partial class TopoCond : System.Web.UI.Page
    {
        private Credenciais Autentica()
        {
            dNeon2 dNeon = new dNeon2();
            dNeon2.AutenticaRow rowAutentica = null;
            TipoAut TipoAut;

            //Autenticacao usuário condôminos
            dNeon2TableAdapters.AutenticaTableAdapter TAAut = new LoginMenu.NeonOnLine.dNeon2TableAdapters.AutenticaTableAdapter();
            try
            {
                //Tenta autenticar pelo SQL - Neon
                TipoAut = TipoAut.Neon1;
                if (TAAut.FillProp(dNeon.Autentica, TxUsuario.Text, TxSenha.Text, 1) > 0)
                    rowAutentica = dNeon.Autentica[0];
                else if (TAAut.FillInq(dNeon.Autentica, TxUsuario.Text, TxSenha.Text, 1) > 0)
                    rowAutentica = dNeon.Autentica[0];
                else
                {
                    //Tenta autenticar pelo SQL - Neon SA
                    TAAut.Connection.ConnectionString = TAAut.Connection.ConnectionString.Replace("Initial Catalog=NEON", "Initial Catalog=NEONSA");
                    TipoAut = TipoAut.Neon2;
                    if (TAAut.FillProp(dNeon.Autentica, TxUsuario.Text, TxSenha.Text, 3) > 0)
                        rowAutentica = dNeon.Autentica[0];
                    else if (TAAut.FillInq(dNeon.Autentica, TxUsuario.Text, TxSenha.Text, 3) > 0)
                        rowAutentica = dNeon.Autentica[0];
                }

                //Verifica se autenticou
                if (rowAutentica != null)
                {
                    dNeon2.clientesRow rowcli = dNeon.clientes.NewclientesRow();
                    rowcli.APT = rowAutentica.APT;
                    rowcli.PES = rowAutentica.PES;
                    rowcli.Nome = rowAutentica.Nome;
                    rowcli.CODCON = rowAutentica.CONCodigo;
                    rowcli.CONNome = rowAutentica.CONNome;
                    rowcli.bloco = rowAutentica.BLOCodigo;
                    rowcli.Apartamento = rowAutentica.APTNumero;
                    rowcli.Email = rowAutentica.Email;
                    rowcli.CpfCnpj = rowAutentica.CpfCnpj;
                    rowcli.tipousuario = rowAutentica.TipoUsuario;
                    rowcli.Proprietario = Convert.ToBoolean(rowAutentica.Proprietario);
                    return new Credenciais(TipoAut, rowcli, rowAutentica.EMP, rowAutentica.CON);
                }
            }
            catch
            {
            };

            //Autenticacao usuário site
            dNeon2TableAdapters.CLIENTESN1TableAdapter TACli1 = new LoginMenu.NeonOnLine.dNeon2TableAdapters.CLIENTESN1TableAdapter();
            try
            {
                if (TACli1.Fill(dNeon.CLIENTESN1, TxUsuario.Text, TxSenha.Text) > 0)
                {
                    dNeon2.CLIENTESN1Row clirow = dNeon.CLIENTESN1[0];
                    dNeon2.clientesRow rowcli;
                    TipoUsuario Tipo;
                    int EMP = 0;
                    switch (clirow.TIPOUSUARIO)
                    {
                        case 0:
                            //Administradora
                            Tipo = TipoUsuario.Administradora;
                            rowcli = dNeon.clientes.NewclientesRow();
                            rowcli.Nome = clirow.NOME;
                            rowcli.CODCON = clirow.CODCON;
                            rowcli.CONNome = "Administradora";
                            rowcli.bloco = clirow.BLOCO;
                            rowcli.Apartamento = clirow.APARTAMENTO;
                            rowcli.Email = clirow.EMAIL;
                            rowcli.tipousuario = 1;
                            break;
                        case 12:
                        case 13:
                            //Sindico
                            Tipo = TipoUsuario.sindico;
                            rowcli = dNeon.clientes.NewclientesRow();
                            rowcli.Nome = clirow.NOME;
                            rowcli.CODCON = clirow.CODCON;
                            rowcli.bloco = clirow.BLOCO;
                            rowcli.Apartamento = clirow.APARTAMENTO;
                            rowcli.Email = clirow.EMAIL;
                            rowcli.tipousuario = clirow.TIPOUSUARIO;
                            EMP = clirow.TIPOUSUARIO == 12 ? 1 : 3;
                            break;
                        case 14:
                        case 15:
                            //Advogado
                            Tipo = TipoUsuario.advogado;
                            rowcli = dNeon.clientes.NewclientesRow();
                            rowcli.Nome = clirow.NOME;
                            rowcli.CODCON = clirow.CODCON;
                            rowcli.bloco = clirow.BLOCO;
                            rowcli.Apartamento = clirow.APARTAMENTO;
                            rowcli.Email = clirow.EMAIL;
                            rowcli.tipousuario = clirow.TIPOUSUARIO;
                            EMP = clirow.TIPOUSUARIO == 14 ? 1 : 3;
                            break;
                        case 16:
                            //Advogado Administradora
                            Tipo = TipoUsuario.advogado;
                            rowcli = dNeon.clientes.NewclientesRow();
                            rowcli.Nome = clirow.NOME;
                            rowcli.CODCON = "XXXXXX";
                            rowcli.CONNome = "Advogado(a)";
                            rowcli.bloco = clirow.BLOCO;
                            rowcli.Apartamento = clirow.APARTAMENTO;
                            rowcli.Email = clirow.EMAIL;
                            rowcli.tipousuario = clirow.TIPOUSUARIO;
                            Session["FRNADV"] = int.Parse(clirow.CODCON);
                            break;
                        case 20:
                        case 21:
                            //Gerente Predial
                            Tipo = TipoUsuario.UsuarioSite;
                            rowcli = dNeon.clientes.NewclientesRow();
                            rowcli.Nome = clirow.NOME;
                            rowcli.CODCON = clirow.CODCON;
                            rowcli.bloco = clirow.BLOCO;
                            rowcli.Apartamento = clirow.APARTAMENTO;
                            rowcli.Email = clirow.EMAIL;
                            rowcli.tipousuario = clirow.TIPOUSUARIO;
                            EMP = clirow.TIPOUSUARIO == 20 ? 1 : 3;
                            break;
                        case 22:
                        case 23:
                            //Zelador
                            Tipo = TipoUsuario.UsuarioSite;
                            rowcli = dNeon.clientes.NewclientesRow();
                            rowcli.Nome = clirow.NOME;
                            rowcli.CODCON = clirow.CODCON;
                            rowcli.bloco = clirow.BLOCO;
                            rowcli.Apartamento = clirow.APARTAMENTO;
                            rowcli.Email = clirow.EMAIL;
                            rowcli.tipousuario = clirow.TIPOUSUARIO;
                            EMP = clirow.TIPOUSUARIO == 22 ? 1 : 3;
                            break;
                        case 24:
                        case 25:
                            //Subsindico
                            Tipo = TipoUsuario.sindico;
                            rowcli = dNeon.clientes.NewclientesRow();
                            rowcli.Nome = clirow.NOME;
                            rowcli.CODCON = clirow.CODCON;
                            rowcli.bloco = clirow.BLOCO;
                            rowcli.Apartamento = clirow.APARTAMENTO;
                            rowcli.Email = clirow.EMAIL;
                            rowcli.tipousuario = clirow.TIPOUSUARIO;
                            EMP = clirow.TIPOUSUARIO == 24 ? 1 : 3;
                            break;
                        case 26:
                        case 27:
                            //Portaria
                            Tipo = TipoUsuario.UsuarioSite;
                            rowcli = dNeon.clientes.NewclientesRow();
                            rowcli.Nome = clirow.NOME;
                            rowcli.CODCON = clirow.CODCON;
                            rowcli.bloco = clirow.BLOCO;
                            rowcli.Apartamento = clirow.APARTAMENTO;
                            rowcli.Email = clirow.EMAIL;
                            rowcli.tipousuario = clirow.TIPOUSUARIO;
                            EMP = clirow.TIPOUSUARIO == 26 ? 1 : 3;
                            break;
                        default:
                            return null;
                    }
                    rowcli.APT = 0;
                    rowcli.PES = 0;
                    rowcli.CpfCnpj = "";
                    rowcli.Proprietario = false;
                    Credenciais Cred = new Credenciais(TipoAut.MSSQL1, Tipo, rowcli);
                    Cred.EMP = EMP;
                    if (EMP != 0)
                    {
                        if (EMP == 3)
                            dNeon.AutenticaTableAdapter.Connection.ConnectionString = dNeon.AutenticaTableAdapter.Connection.ConnectionString.Replace("Initial Catalog=NEON", "Initial Catalog=NEONSA");
                        Cred.CON = (int)dNeon.AutenticaTableAdapter.BuscaCON(EMP, rowcli.CODCON);
                        rowcli.CONNome = (string)dNeon.AutenticaTableAdapter.BuscaCONNome(EMP, rowcli.CODCON);
                    }
                    return Cred;
                }
            }
            catch
            {
            }
            return null;
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            Credenciais Cred = null;
            if (Session["Cred"] != null)
            {
                Panel1.Visible = false;
                Panel2.Visible = true;
                Cred = (Credenciais)Session["Cred"];
                if (Cred.TUsuario == TipoUsuario.Desenvolvimento)
                {
                    LNome.Text = "Desenvolvedor";
                    if (Cred.rowCli.IsCONNomeNull())
                        Lcondominio.Text = "XCondominioX";
                    else
                        Lcondominio.Text = Cred.rowCli.CONNome;
                    LBloco.Text = "| Bloco: XX";
                    Lapartamento.Text = "NNNN";
                    Lclasse.Text = "TI";
                }
                if (Cred.TUsuario == TipoUsuario.Administradora)
                {
                    LNome.Text = Cred.rowCli.Nome;
                    if (Cred.rowCli.IsCONNomeNull())
                        Lcondominio.Text = "Administradora";
                    else
                        Lcondominio.Text = Cred.rowCli.CONNome;
                    LBloco.Text = "";
                    Lapartamento.Text = "";
                    Lclasse.Text = "ADM";
                }
                else if ((Cred.TAut == TipoAut.Neon1) || (Cred.TAut == TipoAut.Neon2) || (Cred.TAut == TipoAut.Neon0) || (Cred.TAut == TipoAut.MSSQL1))
                {
                    LNome.Text = Cred.rowCli.Nome;
                    Lcondominio.Text = Cred.rowCli.CONNome;
                    LBloco.Text = (Cred.rowCli.bloco.ToUpper() == "SB") ? "" : "| Bloco: " + Cred.rowCli.bloco;
                    Lapartamento.Text = Cred.rowCli.Apartamento;
                    if (Cred.TUsuario == TipoUsuario.advogado)
                        Lclasse.Text = "Advogado";
                    else if (Cred.TUsuario == TipoUsuario.UsuarioSite && (Cred.rowCli.tipousuario == 20 || Cred.rowCli.tipousuario == 21))
                       Lclasse.Text = "Gerente Predial";
                    else if (Cred.TUsuario == TipoUsuario.UsuarioSite && (Cred.rowCli.tipousuario == 22 || Cred.rowCli.tipousuario == 23))
                       Lclasse.Text = "Zelador";
                    else if (Cred.TUsuario == TipoUsuario.sindico && (Cred.rowCli.tipousuario == 24 || Cred.rowCli.tipousuario == 25))
                       Lclasse.Text = "Subsíndico";
                    else if (Cred.TUsuario == TipoUsuario.sindico && (Cred.rowCli.tipousuario == 12 || Cred.rowCli.tipousuario == 13))
                        Lclasse.Text = "Subsíndico";
                    else if (Cred.TiposSindico.Contains(Cred.TUsuario))
                       Lclasse.Text = "Síndico";
                    else
                       Lclasse.Text = Cred.rowCli.Proprietario ? "Proprietário" : "Inquilino";
                }
            }
            else
            {
                if ((TxUsuario.Text != "") && (TxSenha.Text != ""))
                {
                    Cred = Autentica();
                    if (Cred != null)
                    {
                        Session["Cred"] = Cred;
                        Session["Username"] = TxUsuario.Text;
                        Session["Senha"] = TxSenha.Text;
                        if (Request.QueryString["Tipo"] != null)
                            Session["TipoAbertura"] = Request.QueryString["Tipo"];
                        if ((Cred.TAut == TipoAut.Neon1 || Cred.TAut == TipoAut.Neon2) && Cred.rowCli.CpfCnpj == "")
                            Response.Redirect(string.Format("..\\..\\CpfCnpj.aspx?EMP={0}&CON={1}", Request["EMP"], Request["CON"]));
                        else
                            Response.Redirect(string.Format("..\\..\\Wbase.aspx?EMP={0}&CON={1}", Request["EMP"], Request["CON"]));
                    }
                    else
                    {
                        //LabelNaoCad.Visible = true;
                    }
                }
            }
        }

        protected void Sair(object sender, EventArgs e)
        {
            Session["Cred"] = null;
            Session["Username"] = null;
            Session["Senha"] = null;
            Response.Redirect(string.Format("..\\..\\Wbase.aspx?EMP={0}&CON={1}", Request["EMP"], Request["CON"]));
        }
    }
}