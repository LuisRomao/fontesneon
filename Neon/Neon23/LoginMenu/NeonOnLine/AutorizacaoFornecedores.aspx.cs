﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace LoginMenu.NeonOnLine
{
   public partial class AutorizacaoFornecedores : System.Web.UI.Page
   {  
      private Credenciais Cred
      {
         get
         {
            return Credenciais.CredST(this, true);
         }
      }

      protected void Page_Load(object sender, EventArgs e)
      {
         if (!IsPostBack)
         {
            //Preenche campos
            LCondominio.Text = Cred.rowCli.CONNome;
            if (Cred.rowCli.bloco.ToUpper() != "SB" && Cred.rowCli.bloco.ToUpper() != "XX")
                Literal1.Text = string.Format("<tr><td class=\"Titulo\" >Bloco:</td>" +
                                              "<td class=\"Celulas\">{0}</td></tr>",
                                              Cred.rowCli.bloco);
            LApartamento.Text = Cred.rowCli.Apartamento.ToUpper() == "XXXX" ? "" : Cred.rowCli.Apartamento;
            LProprietario.Text = Cred.rowCli.Apartamento.ToUpper() == "XXXX" ? "" : (Cred.rowCli.Proprietario ? "Proprietário" : "Inquilino");
            LNome.Text = Cred.rowCli.Nome;
            TEmail.Text = Cred.rowCli.Email;
            TNomeCondomino.Text = Cred.rowCli.Nome;

            //Seta propriedades dos campos
            Ttelefone.Attributes.Add("onkeyup", "javascript:somenteMascara(this,'0123456789- ');");
            TDataInicio.Attributes.Add("onkeyup", "javascript:formataData(this);");
            TDataTermino.Attributes.Add("onkeyup", "javascript:formataData(this);");
            THorarioEntrada.Attributes.Add("onkeyup", "javascript:formataHora(this);");
            THorarioSaida.Attributes.Add("onkeyup", "javascript:formataHora(this);");
            TRG1.Attributes.Add("onkeyup", "javascript:somenteMascara(this,'0123456789-.');");
            TRG2.Attributes.Add("onkeyup", "javascript:somenteMascara(this,'0123456789-.');");
            TRG3.Attributes.Add("onkeyup", "javascript:somenteMascara(this,'0123456789-.');");
            TRG4.Attributes.Add("onkeyup", "javascript:somenteMascara(this,'0123456789-.');");
            TRG5.Attributes.Add("onkeyup", "javascript:somenteMascara(this,'0123456789-.');");
         }
      }

      private void geraemail(string Condominio, string Bloco, string Apartamento, string detalhes)
      {
         //Cria e-mail
         System.Net.Mail.SmtpClient SmtpClient1 = new System.Net.Mail.SmtpClient();

         //Verifica SMTP de dominio proprio
         VirMSSQL.TableAdapter TA = new VirMSSQL.TableAdapter();
         TA.Cone.ConnectionString = System.Configuration.ConfigurationManager.ConnectionStrings["ConnectionStringN2"].ConnectionString;
         System.Data.DataTable SMTP = TA.BuscaSQLTabela("SELECT CONSMTPHost, CONSMTPUser, CONSMTPPass, CONSMTPPort FROM CONDOMINIOS WHERE (CON = @P1) AND (CON_EMP = @P2) AND CONSMTPHost IS NOT NULL AND CONSMTPHost <> ''", Cred.CON, Cred.EMP);
         foreach (System.Data.DataRow DR in SMTP.Rows)
         {
             SmtpClient1.Host = DR[0].ToString();
             SmtpClient1.Credentials = new System.Net.NetworkCredential(DR[1].ToString(), DR[2].ToString());
             SmtpClient1.Port = Convert.ToInt32(DR[3]);
         }

         using (System.Net.Mail.MailMessage email = new System.Net.Mail.MailMessage())
         {
             //Bcc
             string Bcc = "";
             if (System.Configuration.ConfigurationManager.AppSettings["CopiarEmail" + Cred.EMP.ToString()] != null)
                 Bcc = System.Configuration.ConfigurationManager.AppSettings["CopiarEmail" + Cred.EMP.ToString()];
             if (Bcc != "")
                 email.Bcc.Add(Bcc);

            //From
            string strFrom = "";
            TA.BuscaEscalar("SELECT CONRemetente2 FROM CONDOMINIOS WHERE (CON = @P1) AND (CON_EMP = @P2) AND CONSMTPHost IS NOT NULL AND CONSMTPHost <> ''", out strFrom, Cred.CON, Cred.EMP);
            if (strFrom != "")
               email.From = new System.Net.Mail.MailAddress(strFrom);
            else
               if (Cred.EMP == 1)
                  email.From = new System.Net.Mail.MailAddress("neon@neonimoveis.com.br");
               else
                  email.From = new System.Net.Mail.MailAddress("neonsa@neonimoveis.com.br");

            //To
            System.Data.DataTable DT = TA.BuscaSQLTabela("SELECT FCCEmail FROM FaleComCondominio WHERE (FCC_CON = @P1) AND (FCC_EMP = @P2) AND (FCCTipo = 2)", Cred.CON, Cred.EMP);
            foreach (System.Data.DataRow DR in DT.Rows)
               email.To.Add(DR[0].ToString());

            //Corpo do email
            email.Subject = "Solicitação de Autorização para Entrada de Prestadores de Serviço";
            email.IsBodyHtml = true;
            email.Body = string.Format("<html>\r\n" +
                                       "  <head>\r\n" +
                                       "  </head>\r\n" +
                                       "  <body>\r\n" +
                                       "    <p>Data de envio: {0}</p>\r\n" +
                                       "    <p>{1}: {2}</p>\r\n" + ((Bloco.ToUpper() == "SB") ? "" : "    <p>Bloco: {3}</p>\r\n") +
                                       "    <p>Apartamento: {4}\r\n</p>\r\n" +
                                       "    <p>Tipo: <B>{5}</b></p>\r\n" +
                                       "    <p>\r\n" +
                                       "        <table bordercolor=\"#75aadb\" cellspacing=\"2\" cellpadding=\"2\"\r\n border=\"4\" frame=\"border\">\r\n" + 
                                       "            <tbody>\r\n" + "{6}" + 
                                       "		       </tbody>\r\n" + 
                                       "	      </table>\r\n" + 
                                       "	  </p>\r\n" + 
                                       "  </body>\r\n" + 
                                       "</html>",
                                       DateTime.Now, Server.HtmlEncode("Condomínio"), Server.HtmlEncode(Condominio), Server.HtmlEncode(Bloco), Server.HtmlEncode(Apartamento), Server.HtmlEncode("Solicitação de Autorização para Entrada de Prestadores de Serviço"), detalhes);
            
            //Envia email
            SmtpClient1.Send(email);
         }
      }

      private string Detalhes()
      {
         string Retorno = "<table>";
         Retorno += string.Format("<tr><td>{0}</td><td>{1}</td></tr>", "Nome do condômino:", TNomeCondomino.Text);
         Retorno += string.Format("<tr><td>{0}</td><td>{1}</td></tr>", "E-mail:", TEmail.Text);
         Retorno += string.Format("<tr><td>{0}</td><td>{1}</td></tr>", "Telefone para contato:", Ttelefone.Text);
         Retorno += string.Format("<tr><td>{0}</td><td>{1}</td></tr>", "Serviço prestado:", TServico.Text);
         Retorno += string.Format("<tr><td>{0}</td><td>{1}</td></tr>", "Período (início):", TDataInicio.Text);
         Retorno += string.Format("<tr><td>{0}</td><td>{1}</td></tr>", "Período (término):", TDataTermino.Text);
         if (THorarioSaida.Text != "")
            Retorno += string.Format("<tr><td>{0}</td><td>{1}</td></tr>", "Horário:", THorarioEntrada.Text + " até " + THorarioSaida.Text);
         else
            Retorno += string.Format("<tr><td>{0}</td><td>{1}</td></tr>", "Horário:", THorarioEntrada.Text);
         Retorno += string.Format("<tr><td colspan=2>{0}</td></tr>", "----------------------------------------------");
         Retorno += string.Format("<tr><td colspan=2>{0}</td></tr>", "Prestadores de Serviço:");
         Retorno += string.Format("<tr><td colspan=2>Nome: {0} - RG: {1} - Empresa: {2}</td></tr>", TNome1.Text, TRG1.Text, TEmpresa1.Text);
         if (TNome2.Text != "") { Retorno += string.Format("<tr><td colspan=2>Nome: {0} - RG: {1} - Empresa: {2}</td></tr>", TNome2.Text, TRG2.Text, TEmpresa2.Text); }
         if (TNome3.Text != "") { Retorno += string.Format("<tr><td colspan=2>Nome: {0} - RG: {1} - Empresa: {2}</td></tr>", TNome3.Text, TRG3.Text, TEmpresa3.Text); }
         if (TNome4.Text != "") { Retorno += string.Format("<tr><td colspan=2>Nome: {0} - RG: {1} - Empresa: {2}</td></tr>", TNome4.Text, TRG4.Text, TEmpresa4.Text); }
         if (TNome5.Text != "") { Retorno += string.Format("<tr><td colspan=2>Nome: {0} - RG: {1} - Empresa: {2}</td></tr>", TNome5.Text, TRG5.Text, TEmpresa5.Text); }
         return Retorno;
      }

      protected void cmdEnviar_Click(object sender, EventArgs e)
      {
         //Limpa mensagem de erro
         LMsg.Text = "";
         LMsg.Visible = false;

         //Faz a consistência dos campos
         if (TEmail.Text == "")
         {
             LMsg.Text = "Informe um e-mail.";
             LMsg.Visible = true;
             return;
         }
         if (Ttelefone.Text == "")
         {
             LMsg.Text = "Informe um telefone para contato.";
             LMsg.Visible = true;
             return;
         }
         if (TDataInicio.Text == "")
         {
             LMsg.Text = "Informe a data de início da autorização de entrada.";
             LMsg.Visible = true;
             return;
         }
         if (TDataInicio.Text != "" && TDataTermino.Text != "")
            if (Convert.ToDateTime(TDataInicio.Text) > Convert.ToDateTime(TDataTermino.Text))
            {
               LMsg.Text = "Período inválido. A data de término deve ser posterior a data início.";
               LMsg.Visible = true;
               return;
            }
         if (TNome1.Text == "" || TRG1.Text == "")
         {
             LMsg.Text = "Informe pelo menos o nome completo e o RG de um prestador de serviço (primeira linha).";
             LMsg.Visible = true;
             return;
         }

         //Envia email
         geraemail(Cred.rowCli.CONNome, Cred.rowCli.bloco, Cred.rowCli.Apartamento, Detalhes());

         //Enviou
         Session["txtConfirmar"] = "E-mails enviados";
         Response.Redirect("Confirmacao.aspx");
      }

      protected void TDataInicio_TextChanged(object sender, EventArgs e)
      {
          //Verifica data 
          DateTime parsedDateTime;
          if (TDataInicio.Text != "")
              if (!DateTime.TryParse(TDataInicio.Text, out parsedDateTime))
              {
                  TDataInicio.Text = "";
                  TDataInicio.Focus();
              }
              else
                  TDataInicio.Text = parsedDateTime.ToString("dd/MM/yyyy");

          //Ajusta selecao calendario
          CDataInicio.SelectedDates.Clear();
          if (TDataInicio.Text == "")
          {
              panDataInicio.Visible = false;
              cmdDataInicio.Text = ">>";
          }
          else
          {
              CDataInicio.SelectedDate = Convert.ToDateTime(TDataInicio.Text);
              CDataInicio.VisibleDate = CDataInicio.SelectedDate;
          }
      }

      protected void cmdDataInicio_Click(object sender, EventArgs e)
      {
          //Abre calendario na data atual ou na data selecionada
          panDataInicio.Visible = !panDataInicio.Visible;
          cmdDataInicio.Text = panDataInicio.Visible ? "<<" : ">>";

          //Ajusta selecao calendario
          if (TDataInicio.Text == "")
              CDataInicio.VisibleDate = System.DateTime.Today;
          else
              CDataInicio.VisibleDate = CDataInicio.SelectedDate = Convert.ToDateTime(TDataInicio.Text);
      }

      protected void CDataInicio_SelectionChanged(object sender, EventArgs e)
      {
          //Seta a data selecionada
          TDataInicio.Text = CDataInicio.SelectedDate.ToString("dd/MM/yyyy");
          CDataInicio.VisibleDate = CDataInicio.SelectedDate;
      }

      protected void TDataTermino_TextChanged(object sender, EventArgs e)
      {
          //Verifica data 
          DateTime parsedDateTime;
          if (TDataTermino.Text != "")
              if (!DateTime.TryParse(TDataTermino.Text, out parsedDateTime))
              {
                  TDataTermino.Text = "";
                  TDataTermino.Focus();
              }
              else
                  TDataTermino.Text = parsedDateTime.ToString("dd/MM/yyyy");

          //Ajusta selecao calendario
          CDataTermino.SelectedDates.Clear();
          if (TDataTermino.Text == "")
          {
              panDataTermino.Visible = false;
              cmdDataTermino.Text = ">>";
          }
          else
          {
              CDataTermino.SelectedDate = Convert.ToDateTime(TDataTermino.Text);
              CDataTermino.VisibleDate = CDataTermino.SelectedDate;
          }
      }

      protected void cmdDataTermino_Click(object sender, EventArgs e)
      {
          //Abre calendario na data atual ou na data selecionada
          panDataTermino.Visible = !panDataTermino.Visible;
          cmdDataTermino.Text = panDataTermino.Visible ? "<<" : ">>";

          //Ajusta selecao calendario
          if (TDataTermino.Text == "")
              CDataTermino.VisibleDate = System.DateTime.Today;
          else
              CDataTermino.VisibleDate = CDataTermino.SelectedDate = Convert.ToDateTime(TDataTermino.Text);
      }

      protected void CDataTermino_SelectionChanged(object sender, EventArgs e)
      {
          //Seta a data selecionada
          TDataTermino.Text = CDataTermino.SelectedDate.ToString("dd/MM/yyyy");
          CDataTermino.VisibleDate = CDataTermino.SelectedDate;
      }
   }
}