using System;
using System.Web.UI.WebControls;
using LoginMenu.NeonOnLine;

namespace LoginMenu.NeonOnLine
{
    

    public partial class Condominios : System.Web.UI.Page
    {
        private Credenciais Cred
        {
            get
            {
                return Credenciais.CredST(this, true);
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {

        }

        int EMP 
        {
            get 
            {
                try
                {
                    return int.Parse(RadioButtonList1.SelectedValue);
                }
                catch
                {
                    return -1;
                }
            }
        }

        protected void RadioButtonList1_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (RadioButtonList1.SelectedIndex != -1)
            {
                dCondominios dCondominios1 = new dCondominios();
                using (dCondominiosTableAdapters.CONDOMINIOSTableAdapter Ta = new LoginMenu.NeonOnLine.dCondominiosTableAdapters.CONDOMINIOSTableAdapter())
                {
                    Ta.Fill(dCondominios1.CONDOMINIOS, EMP);
                }
                GridView1.DataSource = dCondominios1.CONDOMINIOS;
                Session["dCondominios"] = dCondominios1;
                GridView1.DataBind();
            }
        }

        protected void GridView1_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            //Seta tipo de autenticacao
            Cred.TAut = (EMP == 1) ? TipoAut.Neon1 : TipoAut.Neon2;

            //Seta dados do condominio escolhido nas credenciais
            dNeon2 dNeon = new dNeon2();
            dCondominios dCondominios1 = (dCondominios)Session["dCondominios"];
            int indice = int.Parse(e.CommandArgument.ToString());
            int CON = (int)GridView1.DataKeys[indice][0];
            dCondominios.CONDOMINIOSRow rowCON = dCondominios1.CONDOMINIOS.FindByCON_EMPCON(EMP, CON);
            Cred.EMP = EMP;
            Cred.CON = CON;
            Cred.rowCli.CODCON = rowCON.CONCodigo;
            Cred.rowCli.CONNome = rowCON.CONNome;

            //Verifica acao do grid
            if (e.CommandName == "Abrir")
                Response.Redirect("dtCondominio.aspx?Recarregar=1");
            else
                Response.Redirect("MenuSitePreto.aspx");
        }
    }
}
