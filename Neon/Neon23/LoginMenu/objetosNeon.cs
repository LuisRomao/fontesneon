using System.Collections;
using System.Data;
using System.Web.UI.WebControls;

namespace LoginMenu.NeonOnLine
{
    public enum TipoAut
    {
        Desenvolvimento,
        MSSQL1, //neonimoveis_1
        MSSQL2, //neonimoveis_2
        Neon1, //Neon
        Neon2, //NeonSA
        Neon0,
        Indireta,
    }

    public enum TipoUsuario
    {
        proprietario,
        inquilino,
        sindico,
        proprietariosindico,
        inquilinosindico,
        imobiliaria,
        advogado,
        Desenvolvimento,
        Administradora,
        UsuarioSite,
        FRN,
    }

    public class Credenciais
    {
        public static Credenciais CredST(System.Web.UI.Page Chamador,bool obrigatorio)
        {
            string HEMP = Chamador.Request["EMP"] ?? "-1";
            string HCON = Chamador.Request["CON"] ?? "-1";
            Credenciais ret = (Credenciais)Chamador.Session["Cred"];
            if ((ret == null) && obrigatorio)
            {
                if (HEMP != "-1")
                    Chamador.Response.Redirect(string.Format("WBase.aspx?EMP={0}&CON={1}&Ex=1", HEMP, HCON));
                else
                    Chamador.Response.Redirect("Login.aspx?Ex=1");
            }
            return ret;
        }

        public void TiraDBNull(DataRow row)
        {
            foreach (DataColumn DC in row.Table.Columns)
                if (row[DC] == System.DBNull.Value)
                {
                    if (DC.DataType == typeof(string))
                        row[DC] = "";
                    else if (DC.DataType == typeof(int))
                        row[DC] = 0;
                }
        }

        public TipoAut TAut;
        public TipoUsuario TUsuario;        
        public dNeon2.clientesRow rowCli;
        public int EMP;
        public int CON;

        public ArrayList TiposSindico
        {
            get { return new ArrayList(new TipoUsuario[] { TipoUsuario.sindico, 
                                                           TipoUsuario.proprietariosindico, 
                                                           TipoUsuario.inquilinosindico,
                                                           TipoUsuario.Administradora,
                                                           TipoUsuario.Desenvolvimento,
                                                           TipoUsuario.UsuarioSite,
                                                           TipoUsuario.advogado }); }
        }

        public ArrayList TiposUnidade
        {
            get { return new ArrayList(new TipoUsuario[] { TipoUsuario.sindico,
                                                           TipoUsuario.proprietariosindico, 
                                                           TipoUsuario.inquilinosindico, 
                                                           TipoUsuario.inquilino, 
                                                           TipoUsuario.proprietario, 
                                                           TipoUsuario.Administradora, 
                                                           TipoUsuario.Desenvolvimento,
                                                           TipoUsuario.UsuarioSite,
                                                           TipoUsuario.advogado}); }
        }

        public ArrayList TiposADM
        {
            get { return new ArrayList(new TipoUsuario[] { TipoUsuario.Administradora, 
                                                           TipoUsuario.Desenvolvimento }); }
        }

        public Credenciais(TipoAut _Aut, TipoUsuario _TUsuario, dNeon2.clientesRow _rowCli)
        {
            TAut = _Aut;
            TUsuario = _TUsuario;
            rowCli = _rowCli;
        }

        public Credenciais(TipoAut _Aut, dNeon2.clientesRow _rowCli,int _Emp,int _CON)
        {
            TAut = _Aut;
            rowCli = _rowCli;
            TiraDBNull(rowCli);
            EMP = _Emp;
            CON = _CON;
            if (rowCli.tipousuario == 1)
                TUsuario = rowCli.Proprietario ? TipoUsuario.proprietariosindico : TipoUsuario.inquilinosindico;
            else
                TUsuario = rowCli.Proprietario ? TipoUsuario.proprietario : TipoUsuario.inquilino;            
        }
    }

    public class VirBind
    {
        public string FormatadorPadraodecimal;
        private SortedList CamposXObj;

        public VirBind(params WebControl[] Controles) 
        {
            CamposXObj = new SortedList();
            foreach (WebControl Controle in Controles)
            {
                CamposXObj.Add(Controle.ID.Substring(1).ToUpper(), Controle);
            }
        }

        public void Popula(DataRow row)
        {
            foreach (DataColumn DC in row.Table.Columns)
                if (CamposXObj.ContainsKey(DC.ColumnName.ToUpper()))
                {
                    WebControl Controle = (WebControl)CamposXObj[DC.ColumnName.ToUpper()];
                    if (Controle is Label)
                    {
                        Label CLable = (Label)Controle;
                        if((FormatadorPadraodecimal != null) && (row[DC]) is decimal)
                            CLable.Text = ((decimal)row[DC]).ToString(FormatadorPadraodecimal);                        
                        else
                            CLable.Text = row[DC].ToString();                        
                    }
                    if (Controle is TextBox)
                    {
                        TextBox CTextBox = (TextBox)Controle;
                        if ((FormatadorPadraodecimal != null) && (row[DC]) is decimal)
                            CTextBox.Text = ((decimal)row[DC]).ToString(FormatadorPadraodecimal);
                        else
                            CTextBox.Text = row[DC].ToString();
                        CTextBox.MaxLength = DC.MaxLength;
                    }
                    if (Controle is CheckBox)
                    {
                        CheckBox CCheckBox = (CheckBox)Controle;
                        CCheckBox.Checked = (bool)row[DC];
                    }
                }
        }

        public void Recupera(DataRow row,out SortedList Alteracoes)
        {
            Alteracoes = new SortedList();
            foreach (DataColumn DC in row.Table.Columns)
                if (CamposXObj.ContainsKey(DC.ColumnName.ToUpper()))
                {
                    WebControl Controle = (WebControl)CamposXObj[DC.ColumnName.ToUpper()];
                    if (Controle is Label)
                    {
                        Label CLable = (Label)Controle;
                        if (row[DC].ToString() != CLable.Text)
                        {
                            row[DC] = CLable.Text;
                            Alteracoes.Add(DC.ColumnName.ToUpper(), CLable.Text);
                        }
                    }
                    if (Controle is TextBox)
                    {
                        TextBox CTextBox = (TextBox)Controle;
                        if (CTextBox.Text != row[DC].ToString())
                        {
                            row[DC] = CTextBox.Text;
                            Alteracoes.Add(DC.ColumnName.ToUpper(), CTextBox.Text);
                        }
                        
                    }
                    if (Controle is CheckBox)
                    {
                        CheckBox CCheckBox = (CheckBox)Controle;
                        if ((row[DC] == System.DBNull.Value) || (CCheckBox.Checked != (bool)row[DC]))
                        {
                            row[DC] = CCheckBox.Checked;
                            Alteracoes.Add(DC.ColumnName.ToUpper(), CCheckBox.Checked);
                        }
                    }
                }
        }
    }
}