﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace LoginMenu
{
    public partial class TestaMenu : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void Button1_Click(object sender, EventArgs e)
        {
            LoginMenu.NeonOnLine.dNeon2 dNeon = new LoginMenu.NeonOnLine.dNeon2();
            LoginMenu.NeonOnLine.dNeon2TableAdapters.AutenticaTableAdapter TAAut = new LoginMenu.NeonOnLine.dNeon2TableAdapters.AutenticaTableAdapter();

            if (TAAut.FillProp(dNeon.Autentica, "1MARLI50", "3631", 1) > 0) //000999
            //if (TAAut.FillProp(dNeon.Autentica, "1ANDRE39", "5542", 1) > 0) //000429
            //if (TAAut.FillProp(dNeon.Autentica, "1ROBESP2", "8023", 1) > 0) //000589
            {
                LoginMenu.NeonOnLine.dNeon2.AutenticaRow rowAutentica = dNeon.Autentica[0];
                LoginMenu.NeonOnLine.dNeon2.clientesRow rowcli = dNeon.clientes.NewclientesRow();
                rowcli.APT = rowAutentica.APT;
                rowcli.PES = rowAutentica.PES;
                rowcli.Nome = rowAutentica.Nome;
                rowcli.CODCON = rowAutentica.CONCodigo;
                rowcli.CONNome = rowAutentica.CONNome;
                rowcli.bloco = rowAutentica.BLOCodigo;
                rowcli.Apartamento = rowAutentica.APTNumero;
                rowcli.Email = rowAutentica.Email;
                rowcli.tipousuario = rowAutentica.TipoUsuario;
                rowcli.Proprietario = Convert.ToBoolean(rowAutentica.Proprietario);
                LoginMenu.NeonOnLine.Credenciais Cred = new LoginMenu.NeonOnLine.Credenciais(LoginMenu.NeonOnLine.TipoAut.Neon1, rowcli, rowAutentica.EMP, rowAutentica.CON);
                Session["Cred"] = Cred;
                Response.Redirect("NeonOnLine/WBase.aspx");
            };
        }

        protected void Button2_Click(object sender, EventArgs e)
        {
            LoginMenu.NeonOnLine.dNeon2 dNeon = new LoginMenu.NeonOnLine.dNeon2();
            LoginMenu.NeonOnLine.dNeon2TableAdapters.AutenticaTableAdapter TAAut = new LoginMenu.NeonOnLine.dNeon2TableAdapters.AutenticaTableAdapter();

            //if (TAAut.FillProp(dNeon.Autentica, "ROD", "062129", 1) > 0) //000999
            //if (TAAut.FillProp(dNeon.Autentica, "1MARCOS123", "6008", 1) > 0) //000999
            //if (TAAut.FillProp(dNeon.Autentica, "1LUCIAN2", "2570", 1) > 0) //NAVONA
            //if (TAAut.FillProp(dNeon.Autentica, "1LUCIAN215", "2886", 1) > 0) //000566 - Com Seguro Conteudo
            //if (TAAut.FillProp(dNeon.Autentica, "1VITOR58", "2996", 1) > 0) //000566 - Sem Seguro Conteudo
            if (TAAut.FillProp(dNeon.Autentica, "1NAIR20", "2580", 1) > 0) //Box
            {
                LoginMenu.NeonOnLine.dNeon2.AutenticaRow rowAutentica = dNeon.Autentica[0];
                LoginMenu.NeonOnLine.dNeon2.clientesRow rowcli = dNeon.clientes.NewclientesRow();
                rowcli.APT = rowAutentica.APT;
                rowcli.PES = rowAutentica.PES;
                rowcli.Nome = rowAutentica.Nome;
                rowcli.CODCON = rowAutentica.CONCodigo;
                rowcli.CONNome = rowAutentica.CONNome;
                rowcli.bloco = rowAutentica.BLOCodigo;
                rowcli.Apartamento = rowAutentica.APTNumero;
                rowcli.Email = rowAutentica.Email;
                rowcli.tipousuario = rowAutentica.TipoUsuario;
                rowcli.Proprietario = Convert.ToBoolean(rowAutentica.Proprietario);
                LoginMenu.NeonOnLine.Credenciais Cred = new LoginMenu.NeonOnLine.Credenciais(LoginMenu.NeonOnLine.TipoAut.Neon1, rowcli, rowAutentica.EMP, rowAutentica.CON);
                Session["Cred"] = Cred;
                Response.Redirect("NeonOnLine/WBase.aspx");
            };
        }

        protected void Button3_Click(object sender, EventArgs e)
        {
            LoginMenu.NeonOnLine.dNeon2 dNeon = new LoginMenu.NeonOnLine.dNeon2();
            LoginMenu.NeonOnLine.dNeon2TableAdapters.AutenticaTableAdapter TAAut = new LoginMenu.NeonOnLine.dNeon2TableAdapters.AutenticaTableAdapter();
            TAAut.Connection.ConnectionString = TAAut.Connection.ConnectionString.Replace("Initial Catalog=NEON", "Initial Catalog=NEONSA");

            if (TAAut.FillProp(dNeon.Autentica, "ANDERSON", "AQUI09LA", 3) > 0) //MANACA
            //if (TAAut.FillProp(dNeon.Autentica, "3SERGIO77", "1550", 3) > 0) //ALGRAN
            {
                LoginMenu.NeonOnLine.dNeon2.AutenticaRow rowAutentica = dNeon.Autentica[0];
                LoginMenu.NeonOnLine.dNeon2.clientesRow rowcli = dNeon.clientes.NewclientesRow();
                rowcli.APT = rowAutentica.APT;
                rowcli.PES = rowAutentica.PES;
                rowcli.Nome = rowAutentica.Nome;
                rowcli.CODCON = rowAutentica.CONCodigo;
                rowcli.CONNome = rowAutentica.CONNome;
                rowcli.bloco = rowAutentica.BLOCodigo;
                rowcli.Apartamento = rowAutentica.APTNumero;
                rowcli.Email = rowAutentica.Email;
                rowcli.tipousuario = rowAutentica.TipoUsuario;
                rowcli.Proprietario = Convert.ToBoolean(rowAutentica.Proprietario);
                LoginMenu.NeonOnLine.Credenciais Cred = new LoginMenu.NeonOnLine.Credenciais(LoginMenu.NeonOnLine.TipoAut.Neon2, rowcli, rowAutentica.EMP, rowAutentica.CON);
                Session["Cred"] = Cred;
                Response.Redirect("NeonOnLine/WBase.aspx");
            };
        }

        protected void Button4_Click(object sender, EventArgs e)
        {
            LoginMenu.NeonOnLine.dNeon2 dNeon = new LoginMenu.NeonOnLine.dNeon2();
            LoginMenu.NeonOnLine.dNeon2TableAdapters.AutenticaTableAdapter TAAut = new LoginMenu.NeonOnLine.dNeon2TableAdapters.AutenticaTableAdapter();
            TAAut.Connection.ConnectionString = TAAut.Connection.ConnectionString.Replace("Initial Catalog=NEON", "Initial Catalog=NEONSA");

            //if (TAAut.FillProp(dNeon.Autentica, "2AUGUST1", "4481", 3) > 0) //MANACA
            //if (TAAut.FillProp(dNeon.Autentica, "3ROGÉRI9", "8616", 3) > 0) //ALGRAN - Proprietario 03-0012
            //if (TAAut.FillInq(dNeon.Autentica, "3LUIS12", "9394", 3) > 0) //ALGRAN - Inquilino 03-0012
            if (TAAut.FillProp(dNeon.Autentica, "2MARCOS57", "2967", 3) > 0) //ACACI.
            {
                LoginMenu.NeonOnLine.dNeon2.AutenticaRow rowAutentica = dNeon.Autentica[0];
                LoginMenu.NeonOnLine.dNeon2.clientesRow rowcli = dNeon.clientes.NewclientesRow();
                rowcli.APT = rowAutentica.APT;
                rowcli.PES = rowAutentica.PES;
                rowcli.Nome = rowAutentica.Nome;
                rowcli.CODCON = rowAutentica.CONCodigo;
                rowcli.CONNome = rowAutentica.CONNome;
                rowcli.bloco = rowAutentica.BLOCodigo;
                rowcli.Apartamento = rowAutentica.APTNumero;
                rowcli.Email = rowAutentica.Email;
                rowcli.tipousuario = rowAutentica.TipoUsuario;
                rowcli.Proprietario = Convert.ToBoolean(rowAutentica.Proprietario);
                LoginMenu.NeonOnLine.Credenciais Cred = new LoginMenu.NeonOnLine.Credenciais(LoginMenu.NeonOnLine.TipoAut.Neon2, rowcli, rowAutentica.EMP, rowAutentica.CON);
                Session["Cred"] = Cred;
                Response.Redirect("NeonOnLine/WBase.aspx");
            };
        }

        protected void Button5_Click(object sender, EventArgs e)
        {
            LoginMenu.NeonOnLine.dNeon2 dNeon = new LoginMenu.NeonOnLine.dNeon2();
            LoginMenu.NeonOnLine.dNeon2TableAdapters.CLIENTESN1TableAdapter TACli1 = new LoginMenu.NeonOnLine.dNeon2TableAdapters.CLIENTESN1TableAdapter();

            if (TACli1.Fill(dNeon.CLIENTESN1, "GP000999", "GP") > 0) //000999
            {
                LoginMenu.NeonOnLine.dNeon2.CLIENTESN1Row clirow = dNeon.CLIENTESN1[0];
                LoginMenu.NeonOnLine.dNeon2.clientesRow rowcli = dNeon.clientes.NewclientesRow();
                rowcli = dNeon.clientes.NewclientesRow();
                rowcli.Nome = clirow.NOME;
                rowcli.tipousuario = clirow.TIPOUSUARIO;
                rowcli.CODCON = clirow.CODCON;
                rowcli.bloco = clirow.BLOCO;
                rowcli.Apartamento = clirow.APARTAMENTO;
                rowcli.APT = 0;
                LoginMenu.NeonOnLine.Credenciais Cred = new LoginMenu.NeonOnLine.Credenciais(LoginMenu.NeonOnLine.TipoAut.MSSQL1, LoginMenu.NeonOnLine.TipoUsuario.UsuarioSite, rowcli);
                Cred.EMP = clirow.TIPOUSUARIO == 20 ? 1 : 3;
                if (Cred.EMP == 3)
                    dNeon.AutenticaTableAdapter.Connection.ConnectionString = dNeon.AutenticaTableAdapter.Connection.ConnectionString.Replace("Initial Catalog=NEON", "Initial Catalog=NEONSA");
                Cred.CON = (int)dNeon.AutenticaTableAdapter.BuscaCON(Cred.EMP, rowcli.CODCON);
                rowcli.CONNome = (string)dNeon.AutenticaTableAdapter.BuscaCONNome(Cred.EMP, rowcli.CODCON);
                Session["Cred"] = Cred;
                Response.Redirect("NeonOnLine/WBase.aspx");
            };
        }

        protected void Button6_Click(object sender, EventArgs e)
        {
            LoginMenu.NeonOnLine.dNeon2 dNeon = new LoginMenu.NeonOnLine.dNeon2();
            LoginMenu.NeonOnLine.dNeon2TableAdapters.CLIENTESN1TableAdapter TACli1 = new LoginMenu.NeonOnLine.dNeon2TableAdapters.CLIENTESN1TableAdapter();

            if (TACli1.Fill(dNeon.CLIENTESN1, "rodino", "062129") > 0)
            {
                LoginMenu.NeonOnLine.dNeon2.CLIENTESN1Row clirow = dNeon.CLIENTESN1[0];
                LoginMenu.NeonOnLine.dNeon2.clientesRow rowcli = dNeon.clientes.NewclientesRow();
                rowcli = dNeon.clientes.NewclientesRow();
                rowcli.Nome = clirow.NOME;
                rowcli.tipousuario = clirow.TIPOUSUARIO;
                rowcli.CODCON = clirow.CODCON;
                rowcli.bloco = clirow.BLOCO;
                rowcli.Apartamento = clirow.APARTAMENTO;
                rowcli.APT = 0;
                LoginMenu.NeonOnLine.Credenciais Cred = new LoginMenu.NeonOnLine.Credenciais(LoginMenu.NeonOnLine.TipoAut.MSSQL1, LoginMenu.NeonOnLine.TipoUsuario.Administradora, rowcli);
                Cred.EMP = 1;
                Session["Cred"] = Cred;
                Response.Redirect("NeonOnLine/WBase.aspx");
            };
        }
    }
}