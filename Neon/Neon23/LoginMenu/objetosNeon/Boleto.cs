using System;
using System.Data;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

namespace LoginMenu.objetosNeon
{
    public class VirBoleto
    {
        public decimal valororiginal;
        public decimal ValorCorrigido;
        public decimal multa;
        public decimal juros;
        public decimal valorfinal;
        public decimal TxMulta;
        public string TipoMulta;
        public string ValorCorrigidostr;
        public DateTime Vencto;
        public DateTime data;
        public Statusparcela stparcela;
        public decimal honor;
        public decimal totcomhonor;
        public int numero;
        public string competencia;
        public string NumBanco;
        public string Agencia;
        public string DigitoAg;
        public int NossoNumero;
        public string CC;
        public string CNR;
        public string[] Campos;
        public string CodigoBarras;
        public string NossoNumerostr;
        public string Erro;

        private dBoletos dBoletos1;
        private dBoletos.indicesRow rowIndicesFinal = null;
        private dBoletos.indicesRow rowIndicesInicial = null;

        public VirBoleto(dBoletos _dBoletos1)
        {
            dBoletos1 = _dBoletos1;
        }

        private void Buscaindices()
        {
            if (rowIndicesFinal == null)
                rowIndicesFinal = dBoletos1.IndicesTableAdapter.GetData(new DateTime(data.Year, data.Month, 1))[0];

            DateTime manobradata = new DateTime(Vencto.Year, Vencto.Month, 1);
            if ((rowIndicesInicial == null) || (rowIndicesInicial.data != manobradata))
                rowIndicesInicial = dBoletos1.IndicesTableAdapter.GetData(manobradata)[0];
        }


        public void Calcula(DateTime novadata)
        {
            /*

    multamanobra         :double;
    jurosmanobra         :double;
    

                                    function power(base,expoente:double):double;
                                     begin
                                      result:=exp(expoente*ln(base));
                                     end;
             */

            data = novadata;
            int diascorridos = ((TimeSpan)(data - Vencto)).Days;
            decimal valorcorrigidomanobra;
            if (stparcela != Statusparcela.Stparatrasadofora)
                valorcorrigidomanobra = valororiginal;
            else
            {
                int diasinicio = Vencto.Day;
                int diasfinal = 30 - data.Day;
                Buscaindices();
                valorcorrigidomanobra = valororiginal * (decimal)((rowIndicesFinal.Indice / rowIndicesInicial.Indice) * rowIndicesFinal.INPC);
                //   Corre��o dos dias iniciais
                valorcorrigidomanobra = valorcorrigidomanobra / (decimal)Math.Pow(rowIndicesInicial.INPC, (diasinicio / 30));
                //   Corre��o dos dias finais
                valorcorrigidomanobra = valorcorrigidomanobra / (decimal)Math.Pow(rowIndicesFinal.INPC, (diasfinal / 30));
            };
            decimal multamanobra = 0;
            decimal jurosmanobra = 0;
            if ((stparcela != Statusparcela.Stparaberto) && (stparcela != Statusparcela.Stparok))
            {
                if (TipoMulta == "M")
                    multamanobra = valorcorrigidomanobra * TxMulta;
                else
                    if (diascorridos > 30)
                        multamanobra = valorcorrigidomanobra * 30 * TxMulta;
                    else
                        multamanobra = valorcorrigidomanobra * diascorridos * TxMulta;

                if (diascorridos > 30)
                    jurosmanobra = (valorcorrigidomanobra + multamanobra) * (decimal)(Math.Pow(1.01, (diascorridos / 30)) - 1);
                else
                    jurosmanobra = 0;
            }


            multa = Math.Round(multamanobra, 2, MidpointRounding.AwayFromZero);
            juros = Math.Round(jurosmanobra, 2, MidpointRounding.AwayFromZero);


            valorfinal = Math.Round(valorcorrigidomanobra, 2, MidpointRounding.AwayFromZero) + multa + juros;
            //valorcorrigido = valorfinal - multa - juros;
            //if ((stparcela == Statusparcela.Stparatrasadodentro) || (stparcela == Statusparcela.Stparatrasadofora))
            // valorcorrigidostr=valorcorrigido.to
            //else
            // valorcorrigidostr:='';
            if (diascorridos > 30)
                honor = Math.Round((100 * valorfinal * 0.2M) / 100, 2, MidpointRounding.AwayFromZero);
            else
                honor = 0;
            totcomhonor = valorfinal + honor;

        }
    }

    public enum Statusparcela 
    {
        Stparok, 
        Stparaberto, 
        Stparabertotolerancia, 
        Stparatrasadodentro, 
        Stparatrasadofora
    }
}


/*
 
 unit Boleto;

interface

uses
System.Data.OleDb,math,System.Text;

type
  
  VirBoleto = class
  private
    comando        :OLEDBCommand;
    parametro      :OleDbParameter;
    tabela         :OleDbDataReader;
    inpcmesfinal   :Double;
    inpcmesinicial :Double;
    indicefinal    :Double;
    indiceinicial  :Double;
    primeirachamada:boolean;
    binario :string;
    procedure buscaindices;
    procedure CalculaBarras(numero:String);
  public (ja copiada) 
    
    //Falha            :boolean;

    constructor Create;
    procedure Calcula(novadata:datetime);
    function saidastr(valor:currency):string;
    Function LinhaDigitavel:String;
  end;

implementation

Function VirBoleto.LinhaDigitavel:String;
var Fatorvenc:Int32;
    DigitoNossoNumero:String;
    numero:String;
    i:Int32;
                Function Modulo11_2_9(entrada:String;DigitoNN:boolean):String;
                 var Soma:Int32;
                     y,x :Int32;
                     resto :Int32;
                 begin
                   Soma := 0;
                   Y := 2;
                   For x := entrada.Length downTo 1 do
                    begin
                     soma := soma + Int32.Parse(Copy(entrada, x, 1)) * Y;
                     inc(y);
                     if y=10 then
                      y:=2;
                    end;
                   resto := 11-(soma Mod 11);
                   if(DigitoNN) then
                    begin
                     If (resto = 0) or (resto > 9) Then
                      Result := '0'
                     Else
                      Result := resto.ToString;
                    end
                   else
                    begin
                     If (resto = 0) or (resto = 1) or (resto > 9) Then
                      Result := '1'
                     Else
                      Result := resto.ToString;
                    end
                 end;



                Function DigitoBradesco(entrada:String):String;
                 var Soma:Int32;
                     y,x :Int32;
                     resto :Int32;
                 begin
                   Soma := 0;
                   Y := 2;
                   For x := entrada.Length downTo 1 do
                    begin
                     soma := soma + Int32.Parse(Copy(entrada, x, 1)) * Y;
                     inc(y);
                     if y=8 then
                      y:=2;
                    end;
                   resto := soma Mod 11;
                   If resto = 1 Then
                    Result := 'P'
                   Else
                    if resto = 0 then
                     Result := '0'
                    else
                     Result:=Int16(11-resto).ToString;
                 end;
                Function Modulo10(entrada:String):String;
                 var Soma:Int32;
                     y,x :Int32;
                     NUM1  :Int32;
                 begin
                  Y    := 2;
                  soma := 0;
                  For x := entrada.Length downTo 1 do
                   begin
                    NUM1 := Int32.Parse(copy(entrada, x, 1)) * Y;
                    If NUM1 >= 10 Then
                     soma := soma + (NUM1 div 10) + (NUM1 - (NUM1 div 10) * 10)
                    Else
                     soma := soma + NUM1;
                    dec(y);
                    If Y = 0 Then
                     Y := 2;
                   end;
                  If Int(soma / 10) = (soma / 10) Then
                   Result:='0'
                  Else
                   Result := Int32((((soma div 10) * 10) + 10) - soma).ToString;
                 end;
                Procedure CalculaNossoNumero;
                 begin
                  if Numbanco = '237' then
                   begin
                     NossoNumeroStr   := System.String.Format('600{0:000000000}',[NossoNumero]);
                     DigitoNossoNumero:=DigitoBradesco(NossoNumeroStr);
                     NossoNumerostr   := System.String.Format('{0}-{1}&nbsp;&nbsp;&nbsp;&nbsp;',[NossoNumerostr.Insert(1,'/'),DigitoBradesco(NossoNumerostr)]);
                   end
                  else if Numbanco = '341' then
                   begin
                    NossoNumeroStr   := System.String.Format('{0}{1}175{2:00000000}',[Agencia,copy(CC,1,5),NossoNumero]);
                    DigitoNossoNumero:=Modulo10(NossoNumerostr);
                    NossoNumeroStr   := System.String.Format('175-{0:00000000}-{1}&nbsp;&nbsp;&nbsp;&nbsp;',[NossoNumero,DigitoNossoNumero]);
                   end
                  else if Numbanco = '409' then
                   begin
                    Erro := 'C1.1';
//                    NossoNumeroStr   := System.String.Format('001{0:0000000000}2',[NossoNumero]);
                    NossoNumeroStr   := System.String.Format('0000{0:0000000000}',[NossoNumero]);
                    Erro := 'C1.2';
                    DigitoNossoNumero:=Modulo11_2_9(NossoNumerostr,true);
                    Erro := 'C1.3';
                    NossoNumeroStr   := System.String.Format('0000{0:0000000000}/{1:0}&nbsp;&nbsp;',[NossoNumero,DigitoNossoNumero]);
                    Erro := 'C1.4';
                   end;
                 end;

 begin

    Fatorvenc := timespan(data-DateTime.Create(1997,10,7)).days;
    Erro := 'C1';
    CalculaNossoNumero;
    Erro := 'C2';
    if NumBanco = '237' then
     begin
      numero:= System.String.Format('2379{0}{1:0000000000}{2}06{3}0{4}0',
                                    [Fatorvenc,100*valorfinal,Agencia,Copy(NossoNumeroStr,3,11),Copy(CC,1,6)]);
      numero:=numero.Insert(4,Modulo11_2_9(numero,false));
      CAMPOs[0] := '2379'+ Agencia + '0';
      CAMPOs[1] := copy(numero, 25, 10);
      CAMPOs[2] := copy(numero, 35, 10);
      CAMPOs[3] := copy(numero,  5,  1);
      CAMPOs[4] := copy(numero,  6, 14);

     end
	  else if NumBanco = '341' then
     begin
	    numero := System.String.Format('3419{0}{1:0000000000}175{2:00000000}{3}{4}{5}000',
                                     [Fatorvenc,100*valorfinal,NossoNumero,DigitoNossoNumero,Agencia,Copy(CC,1,6)]);



      numero:=numero.Insert(4,Modulo11_2_9(numero,false));
      CAMPOs[0] := '3419175' + Copy(numero, 23, 2);
      CAMPOs[1] := Copy(numero, 25, 6) + DigitoNossoNumero + Copy(Agencia, 1, 3);
      CAMPOs[2] := Copy(Agencia, 4, 1) + copy(CC, 1, 6) + '000';
      CAMPOs[3] := Copy(numero, 5, 1);
      CAMPOs[4] := Copy(numero, 6, 14);
     end

    else if NumBanco = '409' then
     begin
      Erro := 'C3';
      //CC:='2199173';
      //CC:='2345706';
      CNR := CNR.PadLeft(7,'0');
	    numero := System.String.Format('4099{0:0000}{1:0000000000}5{2:0000000}000000{3:0000000000}{4:0}',
                                     [Fatorvenc,100*valorfinal,CNR,NossoNumero,DigitoNossoNumero]);
//                                     [0,0,CC,NossoNumero,DigitoNossoNumero]);

      Erro := 'C4';

      numero:=numero.Insert(4,Modulo11_2_9(numero,false));
      CAMPOs[0] := '40995'+Copy(CNR, 1, 4);
      CAMPOs[1] := Copy(CNR, 5, 3)+'00'+Copy(Numero, 30, 5);
      CAMPOs[2] := Copy(numero, 35, 10);
      CAMPOs[3] := Copy(numero, 5, 1);
      CAMPOs[4] := Copy(numero, 6, 14);
//      CAMPOs[4] := copy(numero,  6, 3);
      Erro := 'C5';
     end;


    for i:=0 to 2 do
     Campos[i]:=Campos[i]+Modulo10(Campos[i]);

    Erro := 'C6 BANCO:'+NumBanco+' ';
    for i:=0 to 4 do
    begin
    if(Campos[i] = nil) then
       Erro := Erro + 'C'+i.ToString()+' - '
    else
       Erro := Erro + 'C'+i.ToString()+' '+Campos[i].ToString()+' ';
    end;

    result := System.String.Format('{0}  {1}  {2}  {3}  {4}',
                                   [Campos[0].Insert(5,'.'),
                                    Campos[1].Insert(5,'.'),
                                    Campos[2].Insert(5,'.'),
                                    Copy(Campos[3],1,1),
                                    Campos[4]]);

//    result:='nosso numero:'+ NossoNumero.ToString;
    Erro := 'C7';
    CalculaBarras(numero);
    Erro := 'C8';

//    result := result + '<br>'+binario;
 end;


procedure VirBoleto.CalculaBarras(numero:string);
 var Manobra  :String;
     Num1,Num2:String;
     Saida    :String;
     Retorno  :StringBuilder;
     b,c,m    :Int16;

 const a:array['0'..'9'] of String = ('00110',
                                      '10001',
                                      '01001',
                                      '11000',
                                      '00101',
                                      '10100',
                                      '01100',
                                      '00011',
                                      '10010',
                                      '01010');
begin
    Retorno:=StringBuilder.Create;
    Manobra:=(numero.Trim);
    if (Manobra.Length mod 2) <> 0 then
//     Manobra.Insert(0,'0');
          Manobra:='0'+Manobra;

    Saida:='0000';

    For c := 1 To Manobra.Length div 2 do
     begin
        NUM1 := a[manobra[2 * (c - 1) + 1]];
        NUM2 := a[manobra[2 * (c - 1) + 2]];
        For b := 1 To 5 do
            Saida:=Saida+NUM1[b]+NUM2[b];
     end;
    Saida:=Saida+'100';
    binario:=saida;
		For M := 1 To Saida.Length do
       begin
         if (M mod 2) <> 0 then //impar
           If Saida[M] = '1' Then
              Retorno.Append('<img src="imagens/p.gif" width=3 height=47>')
           Else
              Retorno.Append('<img src="imagens/p.gif" width=1 height=47>')
           //par
         else
           If Saida[M] = '1' Then
              Retorno.Append('<img src="imagens/b.gif" width=3 height=47>')
           else
              Retorno.Append('<img src="imagens/b.gif" width=1 height=47>');
         end;

    CodigoBarras:=Retorno.ToString;


end;





constructor VirBoleto.Create;
begin
  inherited Create;
  primeirachamada:=true;
  Conexao:=nil;
  trans:=nil;
end;

function VirBoleto.saidastr(valor: currency): string;
begin
 if valor=0 then
  result:=''
 else
  result:=system.String.Format('{0:#,##0.00}',valor);
end;

end.
 
 */