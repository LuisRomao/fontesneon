﻿namespace LoginMenu.objetosNeon {


    partial class dBoletos
    {
        public int EMP;

        
        private dBoletosTableAdapters.CONDOMINIOSTableAdapter cONDOMINIOSTableAdapter;
        /// <summary>
        /// TableAdapter padrão para tabela: CONDOMINIOS
        /// </summary>
        public dBoletosTableAdapters.CONDOMINIOSTableAdapter CONDOMINIOSTableAdapter
        {
            get
            {
                if (cONDOMINIOSTableAdapter == null)
                {
                    cONDOMINIOSTableAdapter = new dBoletosTableAdapters.CONDOMINIOSTableAdapter();                    
                };
                return cONDOMINIOSTableAdapter;
            }
        }

        private dBoletosTableAdapters.DividasTableAdapter dividasTableAdapter;
        /// <summary>
        /// TableAdapter padrão para tabela: Dividas
        /// </summary>
        public dBoletosTableAdapters.DividasTableAdapter DividasTableAdapter
        {
            get
            {
                if (dividasTableAdapter == null)
                {
                    dividasTableAdapter = new dBoletosTableAdapters.DividasTableAdapter();
                    dividasTableAdapter.Connection.ConnectionString = dividasTableAdapter.Connection.ConnectionString.Replace("NEON1","NEON"+EMP.ToString());
                };
                return dividasTableAdapter;
            }
        }

        private dBoletosTableAdapters.novasTableAdapter novasTableAdapter;
        /// <summary>
        /// TableAdapter padrão para tabela: Novas
        /// </summary>
        public dBoletosTableAdapters.novasTableAdapter NovasTableAdapter
        {
            get
            {
                if (novasTableAdapter == null)
                {
                    novasTableAdapter = new dBoletosTableAdapters.novasTableAdapter();
                    novasTableAdapter.Connection.ConnectionString = novasTableAdapter.Connection.ConnectionString.Replace("NEON1", "NEON" + EMP.ToString());
                };
                return novasTableAdapter;
            }
        }

        private dBoletosTableAdapters.BoletosTableAdapter boletosTableAdapter;
        /// <summary>
        /// TableAdapter padrão para tabela: Boletos
        /// </summary>
        public dBoletosTableAdapters.BoletosTableAdapter BoletosTableAdapter
        {
            get
            {
                if (boletosTableAdapter == null)
                {
                    boletosTableAdapter = new dBoletosTableAdapters.BoletosTableAdapter();
                    boletosTableAdapter.Connection.ConnectionString = boletosTableAdapter.Connection.ConnectionString.Replace("NEON1", "NEON" + EMP.ToString());
                };
                return boletosTableAdapter;
            }
        }

        private dBoletosTableAdapters.QueriesTableAdapter queriesTableAdapter;
        /// <summary>
        /// TableAdapter padrão para tabela: Queries
        /// </summary>
        public dBoletosTableAdapters.QueriesTableAdapter QueriesTableAdapter
        {
            get
            {
                if (queriesTableAdapter == null)
                {
                    queriesTableAdapter = new dBoletosTableAdapters.QueriesTableAdapter();                    
                };
                return queriesTableAdapter;
            }
        }

        private dBoletosTableAdapters.indicesTableAdapter indicesTableAdapter;
        /// <summary>
        /// TableAdapter padrão para tabela: Indices
        /// </summary>
        public dBoletosTableAdapters.indicesTableAdapter IndicesTableAdapter
        {
            get
            {
                if (indicesTableAdapter == null)
                {
                    indicesTableAdapter = new dBoletosTableAdapters.indicesTableAdapter();
                    indicesTableAdapter.Connection.ConnectionString = indicesTableAdapter.Connection.ConnectionString.Replace("NEON1", "NEON" + EMP.ToString());
                };
                return indicesTableAdapter;
            }
        }
    }
}
