﻿/*
MR - 10/12/2014 12:00 -           - Inclusao dos campos PES e PESEmail em todos os Fill da DadosDestinatariosTableAdapter para ser utilizado no envio de e-mails
                                    Criado o FillByPES (no DadosDestinatariosTableAdapter) para ser utilizado individualmente no envio de e-mails 
*/

namespace maladireta {


    partial class dDestinatarios
    {
        private dDestinatariosTableAdapters.DadosDestinatariosTableAdapter dadosDestinatariosTableAdapter;

        /// <summary>
        /// 
        /// </summary>
        public dDestinatariosTableAdapters.DadosDestinatariosTableAdapter DadosDestinatariosTableAdapter {
            get {
                if (dadosDestinatariosTableAdapter == null) {
                    dadosDestinatariosTableAdapter = new maladireta.dDestinatariosTableAdapters.DadosDestinatariosTableAdapter();
                    dadosDestinatariosTableAdapter.TrocarStringDeConexao();
                };
                return dadosDestinatariosTableAdapter;
            }
        }
    }
}

