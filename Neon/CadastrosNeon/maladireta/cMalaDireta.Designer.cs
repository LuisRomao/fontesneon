namespace maladireta
{
    partial class cMalaDireta
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.groupControl1 = new DevExpress.XtraEditors.GroupControl();
            this.rtfEditor1 = new RtfEditor.RtfEditor();
            this.groupControl2 = new DevExpress.XtraEditors.GroupControl();
            this.cb_OcultarCodCon = new DevExpress.XtraEditors.CheckEdit();
            this.chIdent = new DevExpress.XtraEditors.CheckEdit();
            this.chDistinct = new DevExpress.XtraEditors.CheckEdit();
            this.chMalote = new DevExpress.XtraEditors.CheckEdit();
            this.chCorreio = new DevExpress.XtraEditors.CheckEdit();
            this.chProp = new DevExpress.XtraEditors.CheckEdit();
            this.checkLogo = new DevExpress.XtraEditors.CheckEdit();
            this.chInq = new DevExpress.XtraEditors.CheckEdit();
            this.lookupBlocosAptos_F1 = new Framework.Lookup.LookupBlocosAptos_F();
            this.groupControl3 = new DevExpress.XtraEditors.GroupControl();
            this.rtfEditor2 = new RtfEditor.RtfEditor();
            this.panelBot = new DevExpress.XtraEditors.PanelControl();
            this.panelEtiquetas = new DevExpress.XtraEditors.PanelControl();
            this.cBotaoImpBol1 = new dllImpresso.Botoes.cBotaoImpBol();
            this.Res = new DevExpress.XtraEditors.SimpleButton();
            this.ResL = new DevExpress.XtraEditors.SimpleButton();
            this.ResImp = new dllImpresso.Botoes.cBotaoImpBol();
            this.panelMala = new DevExpress.XtraEditors.PanelControl();
            this.simpleButton1 = new DevExpress.XtraEditors.SimpleButton();
            this.cBotaoImpBol3 = new dllImpresso.Botoes.cBotaoImpBol();
            this.cBotaoImpBol2 = new dllImpresso.Botoes.cBotaoImpBol();
            this.groupControl4 = new DevExpress.XtraEditors.GroupControl();
            this.memoEdit1 = new DevExpress.XtraEditors.MemoEdit();
            this.panelControl1 = new DevExpress.XtraEditors.PanelControl();
            this.lookUpTPI = new DevExpress.XtraEditors.LookUpEdit();
            this.iMPressoesBindingSource = new System.Windows.Forms.BindingSource();
            this.dDestinatarios1 = new maladireta.dDestinatarios();
            ((System.ComponentModel.ISupportInitialize)(this.StyleController_F)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl1)).BeginInit();
            this.groupControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl2)).BeginInit();
            this.groupControl2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.cb_OcultarCodCon.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chIdent.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chDistinct.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chMalote.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chCorreio.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chProp.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkLogo.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chInq.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl3)).BeginInit();
            this.groupControl3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.panelBot)).BeginInit();
            this.panelBot.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.panelEtiquetas)).BeginInit();
            this.panelEtiquetas.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.panelMala)).BeginInit();
            this.panelMala.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl4)).BeginInit();
            this.groupControl4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.memoEdit1.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl1)).BeginInit();
            this.panelControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.lookUpTPI.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.iMPressoesBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dDestinatarios1)).BeginInit();
            this.SuspendLayout();
            // 
            // ToolTipController_F
            // 
            this.ToolTipController_F.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.ToolTipController_F.Appearance.ForeColor = System.Drawing.Color.Black;
            this.ToolTipController_F.Appearance.Options.UseBackColor = true;
            this.ToolTipController_F.Appearance.Options.UseFont = true;
            this.ToolTipController_F.Appearance.Options.UseForeColor = true;
            this.ToolTipController_F.Appearance.Options.UseTextOptions = true;
            this.ToolTipController_F.Appearance.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            // 
            // StyleController_F
            // 
            this.StyleController_F.AppearanceDisabled.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this.StyleController_F.AppearanceDisabled.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this.StyleController_F.AppearanceDisabled.Options.UseBackColor = true;
            this.StyleController_F.AppearanceDisabled.Options.UseForeColor = true;
            this.StyleController_F.AppearanceFocused.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.StyleController_F.AppearanceFocused.Options.UseBackColor = true;
            // 
            // groupControl1
            // 
            this.groupControl1.Controls.Add(this.rtfEditor1);
            this.groupControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.groupControl1.Location = new System.Drawing.Point(0, 104);
            this.groupControl1.Name = "groupControl1";
            this.groupControl1.Size = new System.Drawing.Size(0, 0);
            this.groupControl1.TabIndex = 0;
            this.groupControl1.Text = "Conte�do";
            // 
            // rtfEditor1
            // 
            this.rtfEditor1.CaixaAltaGeral = true;
            this.rtfEditor1.Doc = System.Windows.Forms.DockStyle.Fill;
            this.rtfEditor1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.rtfEditor1.Estado = CompontesBasicos.EstadosDosComponentes.Registrado;
            this.rtfEditor1.Location = new System.Drawing.Point(0, 19);
            this.rtfEditor1.LookAndFeel.UseDefaultLookAndFeel = false;
            this.rtfEditor1.Name = "rtfEditor1";
            this.rtfEditor1.Size = new System.Drawing.Size(0, 0);
            this.rtfEditor1.somenteleitura = false;
            this.rtfEditor1.TabIndex = 0;
            this.rtfEditor1.Titulo = null;
            // 
            // groupControl2
            // 
            this.groupControl2.Controls.Add(this.cb_OcultarCodCon);
            this.groupControl2.Controls.Add(this.chIdent);
            this.groupControl2.Controls.Add(this.chDistinct);
            this.groupControl2.Controls.Add(this.chMalote);
            this.groupControl2.Controls.Add(this.chCorreio);
            this.groupControl2.Controls.Add(this.chProp);
            this.groupControl2.Controls.Add(this.checkLogo);
            this.groupControl2.Controls.Add(this.chInq);
            this.groupControl2.Controls.Add(this.lookupBlocosAptos_F1);
            this.groupControl2.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.groupControl2.Location = new System.Drawing.Point(0, -75);
            this.groupControl2.Name = "groupControl2";
            this.groupControl2.Size = new System.Drawing.Size(0, 75);
            this.groupControl2.TabIndex = 1;
            this.groupControl2.Text = "Destinat�rios";
            // 
            // cb_OcultarCodCon
            // 
            this.cb_OcultarCodCon.Location = new System.Drawing.Point(831, 48);
            this.cb_OcultarCodCon.Name = "cb_OcultarCodCon";
            this.cb_OcultarCodCon.Properties.Caption = "Ocultar CODCON";
            this.cb_OcultarCodCon.Size = new System.Drawing.Size(170, 19);
            this.cb_OcultarCodCon.TabIndex = 10;
            // 
            // chIdent
            // 
            this.chIdent.Location = new System.Drawing.Point(607, 48);
            this.chIdent.Name = "chIdent";
            this.chIdent.Properties.Caption = "Colocar endere�o mesmo no via malote";
            this.chIdent.Size = new System.Drawing.Size(218, 19);
            this.chIdent.TabIndex = 9;
            // 
            // chDistinct
            // 
            this.chDistinct.Location = new System.Drawing.Point(831, 23);
            this.chDistinct.Name = "chDistinct";
            this.chDistinct.Properties.Caption = "Uma c�pia por pessoa";
            this.chDistinct.Size = new System.Drawing.Size(170, 19);
            this.chDistinct.TabIndex = 8;
            this.chDistinct.Visible = false;
            // 
            // chMalote
            // 
            this.chMalote.EditValue = true;
            this.chMalote.Location = new System.Drawing.Point(543, 48);
            this.chMalote.Name = "chMalote";
            this.chMalote.Properties.Caption = "Malote";
            this.chMalote.Size = new System.Drawing.Size(75, 19);
            this.chMalote.TabIndex = 7;
            // 
            // chCorreio
            // 
            this.chCorreio.EditValue = true;
            this.chCorreio.Location = new System.Drawing.Point(543, 23);
            this.chCorreio.Name = "chCorreio";
            this.chCorreio.Properties.Caption = "Correio";
            this.chCorreio.Size = new System.Drawing.Size(58, 19);
            this.chCorreio.TabIndex = 6;
            // 
            // chProp
            // 
            this.chProp.EditValue = true;
            this.chProp.Location = new System.Drawing.Point(437, 48);
            this.chProp.Name = "chProp";
            this.chProp.Properties.Caption = "Propriet�rios";
            this.chProp.Size = new System.Drawing.Size(88, 19);
            this.chProp.TabIndex = 5;
            // 
            // checkLogo
            // 
            this.checkLogo.EditValue = true;
            this.checkLogo.Location = new System.Drawing.Point(607, 23);
            this.checkLogo.Name = "checkLogo";
            this.checkLogo.Properties.Caption = "Logo";
            this.checkLogo.Size = new System.Drawing.Size(75, 19);
            this.checkLogo.TabIndex = 4;
            // 
            // chInq
            // 
            this.chInq.Location = new System.Drawing.Point(437, 23);
            this.chInq.Name = "chInq";
            this.chInq.Properties.Caption = "Inquilinos";
            this.chInq.Size = new System.Drawing.Size(75, 19);
            this.chInq.TabIndex = 3;
            // 
            // lookupBlocosAptos_F1
            // 
            this.lookupBlocosAptos_F1.APT_Sel = -1;
            this.lookupBlocosAptos_F1.Autofill = true;
            this.lookupBlocosAptos_F1.BLO_Sel = -1;
            this.lookupBlocosAptos_F1.CaixaAltaGeral = true;
            this.lookupBlocosAptos_F1.CON_sel = -1;
            this.lookupBlocosAptos_F1.CON_selrow = null;
            this.lookupBlocosAptos_F1.Cursor = System.Windows.Forms.Cursors.Default;
            this.lookupBlocosAptos_F1.Doc = System.Windows.Forms.DockStyle.None;
            this.lookupBlocosAptos_F1.Estado = CompontesBasicos.EstadosDosComponentes.Registrado;
            this.lookupBlocosAptos_F1.LinhaMae_F = null;
            this.lookupBlocosAptos_F1.Location = new System.Drawing.Point(6, 23);
            this.lookupBlocosAptos_F1.LookAndFeel.UseDefaultLookAndFeel = false;
            this.lookupBlocosAptos_F1.Margin = new System.Windows.Forms.Padding(3, 5, 3, 5);
            this.lookupBlocosAptos_F1.Name = "lookupBlocosAptos_F1";
            this.lookupBlocosAptos_F1.NivelCONOculto = 2;
            this.lookupBlocosAptos_F1.Size = new System.Drawing.Size(424, 46);
            this.lookupBlocosAptos_F1.somenteleitura = false;
            this.lookupBlocosAptos_F1.TabIndex = 2;
            this.lookupBlocosAptos_F1.TableAdapterPrincipal = null;
            this.lookupBlocosAptos_F1.Titulo = null;
            // 
            // groupControl3
            // 
            this.groupControl3.Controls.Add(this.rtfEditor2);
            this.groupControl3.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.groupControl3.Location = new System.Drawing.Point(0, -182);
            this.groupControl3.Name = "groupControl3";
            this.groupControl3.Size = new System.Drawing.Size(0, 107);
            this.groupControl3.TabIndex = 2;
            this.groupControl3.Text = "Segunda P�gina";
            // 
            // rtfEditor2
            // 
            this.rtfEditor2.CaixaAltaGeral = true;
            this.rtfEditor2.Doc = System.Windows.Forms.DockStyle.Fill;
            this.rtfEditor2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.rtfEditor2.Estado = CompontesBasicos.EstadosDosComponentes.Registrado;
            this.rtfEditor2.Location = new System.Drawing.Point(1, 20);
            this.rtfEditor2.LookAndFeel.UseDefaultLookAndFeel = false;
            this.rtfEditor2.Name = "rtfEditor2";
            this.rtfEditor2.Size = new System.Drawing.Size(0, 85);
            this.rtfEditor2.somenteleitura = false;
            this.rtfEditor2.TabIndex = 0;
            this.rtfEditor2.Titulo = null;
            // 
            // panelBot
            // 
            this.panelBot.Controls.Add(this.panelEtiquetas);
            this.panelBot.Controls.Add(this.panelMala);
            this.panelBot.Dock = System.Windows.Forms.DockStyle.Top;
            this.panelBot.Location = new System.Drawing.Point(0, 0);
            this.panelBot.Name = "panelBot";
            this.panelBot.Size = new System.Drawing.Size(0, 42);
            this.panelBot.TabIndex = 3;
            // 
            // panelEtiquetas
            // 
            this.panelEtiquetas.Controls.Add(this.cBotaoImpBol1);
            this.panelEtiquetas.Controls.Add(this.Res);
            this.panelEtiquetas.Controls.Add(this.ResL);
            this.panelEtiquetas.Controls.Add(this.ResImp);
            this.panelEtiquetas.Dock = System.Windows.Forms.DockStyle.Right;
            this.panelEtiquetas.Enabled = false;
            this.panelEtiquetas.Location = new System.Drawing.Point(-505, 2);
            this.panelEtiquetas.Name = "panelEtiquetas";
            this.panelEtiquetas.Size = new System.Drawing.Size(504, 38);
            this.panelEtiquetas.TabIndex = 11;
            // 
            // cBotaoImpBol1
            // 
            this.cBotaoImpBol1.ItemComprovante = false;
            this.cBotaoImpBol1.Location = new System.Drawing.Point(5, 5);
            this.cBotaoImpBol1.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.cBotaoImpBol1.Name = "cBotaoImpBol1";
            this.cBotaoImpBol1.Size = new System.Drawing.Size(112, 29);
            this.cBotaoImpBol1.TabIndex = 4;
            this.cBotaoImpBol1.Titulo = "Etiquetas";
            this.cBotaoImpBol1.clicado += new System.EventHandler(this.cBotaoImpBol1_clicado);
            // 
            // Res
            // 
            this.Res.Location = new System.Drawing.Point(123, 5);
            this.Res.Name = "Res";
            this.Res.Size = new System.Drawing.Size(107, 27);
            this.Res.TabIndex = 7;
            this.Res.Text = "Reservar";
            this.Res.Click += new System.EventHandler(this.Res_Click);
            // 
            // ResL
            // 
            this.ResL.Enabled = false;
            this.ResL.Location = new System.Drawing.Point(391, 5);
            this.ResL.Name = "ResL";
            this.ResL.Size = new System.Drawing.Size(107, 27);
            this.ResL.TabIndex = 9;
            this.ResL.Text = "Limpar";
            this.ResL.Click += new System.EventHandler(this.ResL_Click);
            // 
            // ResImp
            // 
            this.ResImp.Enabled = false;
            this.ResImp.ItemComprovante = false;
            this.ResImp.Location = new System.Drawing.Point(273, 5);
            this.ResImp.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.ResImp.Name = "ResImp";
            this.ResImp.Size = new System.Drawing.Size(112, 29);
            this.ResImp.TabIndex = 8;
            this.ResImp.Titulo = "Reserva";
            this.ResImp.clicado += new System.EventHandler(this.ResImp_clicado);
            // 
            // panelMala
            // 
            this.panelMala.Controls.Add(this.simpleButton1);
            this.panelMala.Controls.Add(this.cBotaoImpBol3);
            this.panelMala.Controls.Add(this.cBotaoImpBol2);
            this.panelMala.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelMala.Enabled = false;
            this.panelMala.Location = new System.Drawing.Point(1, 2);
            this.panelMala.Name = "panelMala";
            this.panelMala.Size = new System.Drawing.Size(0, 38);
            this.panelMala.TabIndex = 10;
            // 
            // simpleButton1
            // 
            this.simpleButton1.Location = new System.Drawing.Point(320, 4);
            this.simpleButton1.Name = "simpleButton1";
            this.simpleButton1.Size = new System.Drawing.Size(117, 29);
            this.simpleButton1.TabIndex = 7;
            this.simpleButton1.Text = "Margem etiquetas";
            this.simpleButton1.Click += new System.EventHandler(this.simpleButton1_Click);
            // 
            // cBotaoImpBol3
            // 
            this.cBotaoImpBol3.ItemComprovante = false;
            this.cBotaoImpBol3.Location = new System.Drawing.Point(5, 5);
            this.cBotaoImpBol3.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.cBotaoImpBol3.Name = "cBotaoImpBol3";
            this.cBotaoImpBol3.Size = new System.Drawing.Size(126, 29);
            this.cBotaoImpBol3.TabIndex = 6;
            this.cBotaoImpBol3.Titulo = "Mala Direta";
            this.cBotaoImpBol3.clicado += new System.EventHandler(this.cBotaoImpBol3_clicado);
            // 
            // cBotaoImpBol2
            // 
            this.cBotaoImpBol2.ItemComprovante = false;
            this.cBotaoImpBol2.Location = new System.Drawing.Point(137, 5);
            this.cBotaoImpBol2.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.cBotaoImpBol2.Name = "cBotaoImpBol2";
            this.cBotaoImpBol2.Size = new System.Drawing.Size(110, 29);
            this.cBotaoImpBol2.TabIndex = 5;
            this.cBotaoImpBol2.Titulo = "Protocolo";
            this.cBotaoImpBol2.clicado += new System.EventHandler(this.cBotaoImpBol2_clicado);
            // 
            // groupControl4
            // 
            this.groupControl4.Controls.Add(this.memoEdit1);
            this.groupControl4.Controls.Add(this.panelControl1);
            this.groupControl4.Dock = System.Windows.Forms.DockStyle.Top;
            this.groupControl4.Location = new System.Drawing.Point(0, 42);
            this.groupControl4.Name = "groupControl4";
            this.groupControl4.Size = new System.Drawing.Size(0, 62);
            this.groupControl4.TabIndex = 4;
            this.groupControl4.Text = "T�tulo do protocolo";
            // 
            // memoEdit1
            // 
            this.memoEdit1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.memoEdit1.Location = new System.Drawing.Point(1, 20);
            this.memoEdit1.Name = "memoEdit1";
            this.memoEdit1.Size = new System.Drawing.Size(0, 40);
            this.memoEdit1.TabIndex = 0;
            this.memoEdit1.EditValueChanged += new System.EventHandler(this.memoEdit1_EditValueChanged);
            // 
            // panelControl1
            // 
            this.panelControl1.Controls.Add(this.lookUpTPI);
            this.panelControl1.Dock = System.Windows.Forms.DockStyle.Right;
            this.panelControl1.Location = new System.Drawing.Point(-212, 20);
            this.panelControl1.Name = "panelControl1";
            this.panelControl1.Size = new System.Drawing.Size(211, 40);
            this.panelControl1.TabIndex = 1;
            // 
            // lookUpTPI
            // 
            this.lookUpTPI.Location = new System.Drawing.Point(5, 10);
            this.lookUpTPI.Name = "lookUpTPI";
            this.lookUpTPI.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.lookUpTPI.Properties.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("TPIDescricao", "TPIDescricao", 68, DevExpress.Utils.FormatType.None, "", true, DevExpress.Utils.HorzAlignment.Near, DevExpress.Data.ColumnSortOrder.Ascending)});
            this.lookUpTPI.Properties.DataSource = this.iMPressoesBindingSource;
            this.lookUpTPI.Properties.DisplayMember = "TPIDescricao";
            this.lookUpTPI.Properties.NullText = "Tipo n�o informado";
            this.lookUpTPI.Properties.ShowHeader = false;
            this.lookUpTPI.Properties.ValueMember = "TPI";
            this.lookUpTPI.Size = new System.Drawing.Size(201, 20);
            this.lookUpTPI.TabIndex = 6;
            this.lookUpTPI.EditValueChanged += new System.EventHandler(this.lookUpTPI_EditValueChanged);
            // 
            // iMPressoesBindingSource
            // 
            this.iMPressoesBindingSource.DataMember = "TiPoImpressao";
            this.iMPressoesBindingSource.DataSource = typeof(Framework.DSCentral);
            // 
            // dDestinatarios1
            // 
            this.dDestinatarios1.DataSetName = "dDestinatarios";
            this.dDestinatarios1.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // cMalaDireta
            // 
            this.CaixaAltaGeral = false;
            this.Controls.Add(this.groupControl1);
            this.Controls.Add(this.groupControl3);
            this.Controls.Add(this.groupControl2);
            this.Controls.Add(this.groupControl4);
            this.Controls.Add(this.panelBot);
            this.Doc = System.Windows.Forms.DockStyle.Fill;
            this.LookAndFeel.UseDefaultLookAndFeel = false;
            this.Name = "cMalaDireta";
            this.Size = new System.Drawing.Size(0, 0);
            ((System.ComponentModel.ISupportInitialize)(this.StyleController_F)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl1)).EndInit();
            this.groupControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.groupControl2)).EndInit();
            this.groupControl2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.cb_OcultarCodCon.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chIdent.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chDistinct.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chMalote.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chCorreio.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chProp.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkLogo.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chInq.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl3)).EndInit();
            this.groupControl3.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.panelBot)).EndInit();
            this.panelBot.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.panelEtiquetas)).EndInit();
            this.panelEtiquetas.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.panelMala)).EndInit();
            this.panelMala.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.groupControl4)).EndInit();
            this.groupControl4.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.memoEdit1.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl1)).EndInit();
            this.panelControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.lookUpTPI.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.iMPressoesBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dDestinatarios1)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraEditors.GroupControl groupControl1;
        private DevExpress.XtraEditors.GroupControl groupControl2;
        private DevExpress.XtraEditors.GroupControl groupControl3;
        private DevExpress.XtraEditors.PanelControl panelBot;
        private dDestinatarios dDestinatarios1;
        private RtfEditor.RtfEditor rtfEditor1;
        private RtfEditor.RtfEditor rtfEditor2;
        private Framework.Lookup.LookupBlocosAptos_F lookupBlocosAptos_F1;
        private DevExpress.XtraEditors.CheckEdit chInq;
        private DevExpress.XtraEditors.CheckEdit checkLogo;
        private DevExpress.XtraEditors.GroupControl groupControl4;
        private DevExpress.XtraEditors.MemoEdit memoEdit1;
        private DevExpress.XtraEditors.CheckEdit chProp;
        private DevExpress.XtraEditors.PanelControl panelControl1;
        private DevExpress.XtraEditors.LookUpEdit lookUpTPI;
        private System.Windows.Forms.BindingSource iMPressoesBindingSource;
        private DevExpress.XtraEditors.CheckEdit chMalote;
        private DevExpress.XtraEditors.CheckEdit chCorreio;
        private DevExpress.XtraEditors.CheckEdit chDistinct;
        private dllImpresso.Botoes.cBotaoImpBol cBotaoImpBol1;
        private dllImpresso.Botoes.cBotaoImpBol cBotaoImpBol2;
        private dllImpresso.Botoes.cBotaoImpBol cBotaoImpBol3;
        private DevExpress.XtraEditors.SimpleButton ResL;
        private dllImpresso.Botoes.cBotaoImpBol ResImp;
        private DevExpress.XtraEditors.SimpleButton Res;
        private DevExpress.XtraEditors.CheckEdit chIdent;
        private DevExpress.XtraEditors.PanelControl panelMala;
        private DevExpress.XtraEditors.PanelControl panelEtiquetas;
        private DevExpress.XtraEditors.SimpleButton simpleButton1;
        private DevExpress.XtraEditors.CheckEdit cb_OcultarCodCon;
    }
}
