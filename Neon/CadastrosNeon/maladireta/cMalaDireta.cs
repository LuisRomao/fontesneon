/*
MR - 10/12/2014 12:00 -           - Implementado o envio de e-mail para todos os destinat�rios ao clicar em "Mala Direta / Email" (Altera��es indicadas por *** MRC - INICIO (10/12/2014 12:00) ***)
MR - 05/01/2015 10:00 -           - Ajustes no envio de e-mail da Mala Direta para envio mais r�pido, com uso de um novo impresso �nico para todos os destinat�rios, sem remetente, e com tela de aguarde (Altera��es indicadas por *** MRC - INICIO (05/01/2015 10:00) ***)
*/

using System;
using System.Windows.Forms;
using dllImpresso;
using DevExpress.XtraReports.UI;
using CompontesBasicosProc;
using CompontesBasicos;
using System.Net.Mail;
using System.Collections.Generic;
using VirEnumeracoesNeon;

namespace maladireta
{
    /// <summary>
    /// Mala direta
    /// </summary>
    public partial class cMalaDireta : ComponenteBase
    {
        /// <summary>
        /// Construtor padrao
        /// </summary>
        public cMalaDireta()
        {
            
            InitializeComponent();
            
            if (VirDB.Bancovirtual.BancoVirtual.Configurado(VirDB.Bancovirtual.TiposDeBanco.SQL))
            {

                iMPressoesBindingSource.DataSource = Framework.DSCentral.DCentral;
                Framework.DSCentral.DCentral.fill_TPI();
            };

        }

        private bool ForcarPapel = true;

        private int AjustaDestinatario() {
            //string Endereco;
            string CEP = "";
            int CONdoCEP = 0;
            //string Bairro;
            //int FPG;
            
            System.Collections.ArrayList Remover = new System.Collections.ArrayList();
            //bool Proprietario;
            foreach (dDestinatarios.DadosDestinatariosRow row in dDestinatarios1.DadosDestinatarios)
            {
                AbstratosNeon.ABS_Apartamento Apartamento = BuscaAPT(row.APT); //AbstratosNeon.ABS_Apartamento.GetApartamento(row.APT);

                //if(row.Proprietario)                    
                //    FPG = (row.IsAPTFormaPagtoProprietario_FPGNull() ? 3 : row.APTFormaPagtoProprietario_FPG);                
                //else                    
                //    FPG = (row.IsAPTFormaPagtoInquilino_FPGNull() ? 3 : row.APTFormaPagtoInquilino_FPG);

                FormaPagamento FormaPagamento1 = Apartamento.FormaPagamentoEfetivo(null, row.Proprietario, ForcarPapel);

                row.BOLEndereco = Apartamento.PESEndereco(null, row.Proprietario, ForcarPapel);

                if (FormaPagamento1.EstaNoGrupo(FormaPagamento.Correio, FormaPagamento.CorreioEemail))
                {
                    if (chCorreio.Checked)
                    {
                        row.BOLEndereco = Apartamento.PESEndereco(null, row.Proprietario, ForcarPapel);
                        //if (row.IsPESEnderecoNull())
                        //    Endereco = "-";
                        //else
                        //    Endereco = row.PESEndereco.Trim();

                        //if (row.IsPESCepNull())
                        //    CEP = "-";
                        //else
                        //    CEP = row.PESCep;

                        //if (row.IsPESBairroNull())
                        //    Bairro = "-";
                        //else
                        //    Bairro = row.PESBairro;

                        //if (row.IsPESEnderecoNull() || row.IsPESCepNull() || row.IsPESBairroNull() || row.IsCIDNomeNull())
                        //{
                        //    MessageBox.Show("ATEN��O: edere�o incompleto para " + row.BLOCodigo + "-" + row.APTNumero + " " + (row.Proprietario ? "P" : "I"));
                        //    row.BOLEndereco = "";
                        //}
                        //else
                        //    row.BOLEndereco = Endereco + "\r\nCEP: " + CEP + " " + Bairro + "\r\n" + row.CIDNome.Trim() + " - " + row.CIDUf;
                        row.Correio = 0;
                    }
                    else
                        Remover.Add(row);
                }
                else
                {
                    if (chMalote.Checked)
                    {
                        if (chIdent.Checked)
                        {                            
                            if (CONdoCEP != row.CON)
                            {
                                if (VirMSSQL.TableAdapter.STTableAdapter.BuscaEscalar("select concep from condominios where con = @P1",out CEP, row.CON))
                                    CONdoCEP = row.CON;
                                else
                                    CEP = "-";

                            }
                            row.BOLEndereco = string.Format("{0}\r\n{1}{2} {3} {4}\r\nCEP:{5}", row.CONEndereco,
                                                                                            row.BLOCodigo == "SB" ? "" : row.CONNomeBloco,                                                                                            
                                                                                            row.BLOCodigo == "SB"?"":" "+row.BLOCodigo,
                                                                                            row.CONNomeApto,
                                                                                            row.APTNumero,                                                                                           
                                                                                            CEP) ;
                            row.Correio = 1;
                        }
                        else
                        {
                            //row.BOLEndereco = "Entrega Pessoal";
                            row.Correio = 1;
                        }
                    }
                    else
                        Remover.Add(row);
                }
            }
            foreach (dDestinatarios.DadosDestinatariosRow row in Remover)
                dDestinatarios1.DadosDestinatarios.Rows.Remove(row);
            return Remover.Count;
        }

        private bool CarregaDados() {
            int Gerados = 0;
            dDestinatarios1.DadosDestinatarios.Clear();
            dDestinatarios1.DadosDestinatariosTableAdapter.ClearBeforeFill = false;
            if (chDistinct.Checked)
            {
                /*
                if (lookupBlocosAptos_F1.APT_Sel != -1)
                {
                    if (chInq.Checked)
                        Gerados += dDestinatarios1.DadosDestinatariosTableAdapter.FillByAPTInqDistinct(dDestinatarios1.DadosDestinatarios, lookupBlocosAptos_F1.APT_Sel);
                    if (chProp.Checked)
                        Gerados += dDestinatarios1.DadosDestinatariosTableAdapter.FillByAPTPropDistinct(dDestinatarios1.DadosDestinatarios, lookupBlocosAptos_F1.APT_Sel);
                }
                else
                    if (lookupBlocosAptos_F1.BLO_Sel != -1)
                    {
                        if (chInq.Checked)
                            Gerados += dDestinatarios1.DadosDestinatariosTableAdapter.FillByBLOInqDistinct(dDestinatarios1.DadosDestinatarios, lookupBlocosAptos_F1.BLO_Sel);
                        if (chProp.Checked)
                            Gerados += dDestinatarios1.DadosDestinatariosTableAdapter.FillByBLOPropDistinct(dDestinatarios1.DadosDestinatarios, lookupBlocosAptos_F1.BLO_Sel);
                    }
                    else
                        if (lookupBlocosAptos_F1.CON_sel != -1)
                        {
                            if (chInq.Checked)
                                Gerados += dDestinatarios1.DadosDestinatariosTableAdapter.FillInqDistinct(dDestinatarios1.DadosDestinatarios, lookupBlocosAptos_F1.CON_sel);
                            if (chProp.Checked)
                                Gerados += dDestinatarios1.DadosDestinatariosTableAdapter.FillPropDistinct(dDestinatarios1.DadosDestinatarios, lookupBlocosAptos_F1.CON_sel);
                        };
                Gerados -= AjustaDestinatario();
                return (Gerados > 0);
                */
                return false;
            }
            else
            {
                if (lookupBlocosAptos_F1.APT_Sel != -1)
                {
                    if (chInq.Checked)
                        Gerados += dDestinatarios1.DadosDestinatariosTableAdapter.FillByAPTInq(dDestinatarios1.DadosDestinatarios, lookupBlocosAptos_F1.APT_Sel);
                    if (chProp.Checked)
                        Gerados += dDestinatarios1.DadosDestinatariosTableAdapter.FillByAPTProp(dDestinatarios1.DadosDestinatarios, lookupBlocosAptos_F1.APT_Sel);
                }
                else
                    if (lookupBlocosAptos_F1.BLO_Sel != -1)
                    {
                        if (chInq.Checked)
                            Gerados += dDestinatarios1.DadosDestinatariosTableAdapter.FillByBLOInq(dDestinatarios1.DadosDestinatarios, lookupBlocosAptos_F1.BLO_Sel);
                        if (chProp.Checked)
                            Gerados += dDestinatarios1.DadosDestinatariosTableAdapter.FillByBLOProp(dDestinatarios1.DadosDestinatarios, lookupBlocosAptos_F1.BLO_Sel);
                    }
                    else
                        if (lookupBlocosAptos_F1.CON_sel != -1)
                        {
                            if (chInq.Checked)
                                Gerados += dDestinatarios1.DadosDestinatariosTableAdapter.FillInq(dDestinatarios1.DadosDestinatarios, lookupBlocosAptos_F1.CON_sel);
                            if (chProp.Checked)
                                Gerados += dDestinatarios1.DadosDestinatariosTableAdapter.FillProp(dDestinatarios1.DadosDestinatarios, lookupBlocosAptos_F1.CON_sel);
                        };
                Gerados -= AjustaDestinatario();
                return (Gerados > 0);
            }
        }

        private void Habilita() {
            panelMala.Enabled = ((memoEdit1.Text.Trim() != "") && (lookUpTPI.EditValue != null));
            panelEtiquetas.Enabled = (memoEdit1.Text.Trim() != "");
        }

        private void memoEdit1_EditValueChanged(object sender, EventArgs e)
        {
            Habilita();            
        }

        private void lookUpTPI_EditValueChanged(object sender, EventArgs e)
        {
            Habilita();
        }

        private ImpEtiqueta _ImpEtiquetaSt;
        private ImpEtiqueta ImpEtiquetaSt {
            get {
                if (_ImpEtiquetaSt == null)
                    _ImpEtiquetaSt = new ImpEtiqueta();
                return _ImpEtiquetaSt;
            }
        }

        private void impressaoEtiquetas(dllImpresso.Botoes.Botao Botao) {
            ImpEtiquetaSt.CreateDocument();
            switch (Botao)
            {
                case dllImpresso.Botoes.Botao.botao:
                case dllImpresso.Botoes.Botao.imprimir:
                case dllImpresso.Botoes.Botao.imprimir_frente:                                        
                    ImpEtiquetaSt.Print();
                    break;
                case dllImpresso.Botoes.Botao.tela:                    
                    ImpEtiquetaSt.ShowPreviewDialog();
                    break;
                case dllImpresso.Botoes.Botao.email:
                    VirEmailNeon.EmailDiretoNeon.EmalST.Enviar("", "Etiquetas", ImpEtiquetaSt, "Etiquetas", "Etiquetas");
                    break;
                case dllImpresso.Botoes.Botao.pdf:
                case dllImpresso.Botoes.Botao.PDF_frente:
                    System.Windows.Forms.SaveFileDialog saveFileDialog1 = new SaveFileDialog();
                    saveFileDialog1.DefaultExt = "pdf";
                    if (saveFileDialog1.ShowDialog() == DialogResult.OK)                                            
                        ImpEtiquetaSt.ExportToPdf(saveFileDialog1.FileName);                    
                    break;
            }
        }

        private SortedList<int, AbstratosNeon.ABS_Apartamento> Apartamentos;

        private AbstratosNeon.ABS_Apartamento BuscaAPT(int APT)
        {
            if (Apartamentos != null)
                if (Apartamentos.ContainsKey(APT))
                    return Apartamentos[APT];
            return AbstratosNeon.ABS_Apartamento.GetApartamento(APT);
        }

        private void cBotaoImpBol1_clicado(object sender, EventArgs e)
        {
            if(lookupBlocosAptos_F1.CON_sel >= 0)
                Apartamentos = AbstratosNeon.ABS_Apartamento.ABSGetApartamentos(lookupBlocosAptos_F1.CON_sel);
            if (CarregaDados())
            {
                ImpEtiquetaSt.dImpProtocolo1.DadosProt.Clear();
                ImpEtiquetaSt.xrCondominio.Text = string.Format("{0}{1}{2} - {3}", 
                                                                 cb_OcultarCodCon.Checked ? "" : lookupBlocosAptos_F1.CON_selrow.CONCodigo,
                                                                 cb_OcultarCodCon.Checked ? "" : " - ",
                                                                 lookupBlocosAptos_F1.CON_selrow.CONNome, 
                                                                 memoEdit1.Text);                                    
                foreach (dDestinatarios.DadosDestinatariosRow row in dDestinatarios1.DadosDestinatarios)
                {
                    string Bloco = row.BLOCodigo;
                    if (row.BLONome != row.BLOCodigo)
                        Bloco += " " + row.BLONome;
                    //AbstratosNeon.ABS_Apartamento Apartamento = AbstratosNeon.ABS_Apartamento.GetApartamento(row.APT);
                    AbstratosNeon.ABS_Apartamento Apartamento = BuscaAPT(row.APT);
                    ImpEtiquetaSt.dImpProtocolo1.DadosProt.AddDadosProtRow(row.APTNumero, 
                                                                           row.BOLNome, 
                                                                           row.BOLEndereco, 
                                                                           Bloco, 
                                                                           row.Proprietario ? "P" : "I",
                                                                           //chIdent.Checked ? lookupBlocosAptos_F1.CON_selrow.CONCodigo + " - " + Bloco + " - " + row.APTNumero : "",
                                                                           cb_OcultarCodCon.Checked ? "" : string.Format("{0} - {1} - {2}", lookupBlocosAptos_F1.CON_selrow.CONCodigo, Bloco, row.APTNumero),
                                                                           row.CONNomeBloco,
                                                                           (int)Apartamento.FormaPagamentoEfetivo(null,row.Proprietario,ForcarPapel),
                                                                           0);
                };
                ImpEtiquetaSt.bindingSource1.Sort = "BLOCO,APTNumero";
                impressaoEtiquetas(cBotaoImpBol1.BotaoClicado);
                
            }
        }

        private void cBotaoImpBol2_clicado(object sender, EventArgs e)
        {
            if (CarregaDados()) {
                ImpProtocolo.ImpProtocoloSt.Apontamento(lookupBlocosAptos_F1.CON_sel, -1);
                ImpProtocolo.ImpProtocoloSt.SetNome(lookupBlocosAptos_F1.CON_selrow.CONNome);
                ImpProtocolo.ImpProtocoloSt.dImpProtocolo.DadosProt.Clear();
                foreach (dDestinatarios.DadosDestinatariosRow row in dDestinatarios1.DadosDestinatarios)
                {
                    string Bloco = row.BLOCodigo;
                    if (row.BLONome != row.BLOCodigo)
                        Bloco += " " + row.BLONome;
                    AbstratosNeon.ABS_Apartamento Apartamento = BuscaAPT(row.APT);
                    ImpProtocolo.ImpProtocoloSt.dImpProtocolo.DadosProt.AddDadosProtRow(row.APTNumero, 
                                                                                        row.BOLNome, 
                                                                                        row.BOLEndereco, 
                                                                                        Bloco, 
                                                                                        row.Proprietario ? "P" : "I",
                                                                                        "",
                                                                                        row.CONNomeBloco,
                                                                                        (int)Apartamento.FormaPagamentoEfetivo(null,row.Proprietario,true),
                                                                                        0);
                }
                ImpProtocolo.ImpProtocoloSt.bindingSource1.Sort = "BLOCO,APTNumero";
                ImpProtocolo.ImpProtocoloSt.SetDocumento(memoEdit1.Text);

                switch (cBotaoImpBol2.BotaoClicado)
                {
                    case dllImpresso.Botoes.Botao.botao:
                    case dllImpresso.Botoes.Botao.imprimir:
                    case dllImpresso.Botoes.Botao.imprimir_frente:
                        ImpProtocolo.ImpProtocoloSt.Apontamento(lookupBlocosAptos_F1.CON_sel, (int)lookUpTPI.EditValue);
                        ImpProtocolo.ImpProtocoloSt.CreateDocument();
                        ImpProtocolo.ImpProtocoloSt.Print();
                        break;
                    case dllImpresso.Botoes.Botao.tela:
                        ImpProtocolo.ImpProtocoloSt.CreateDocument();
                        ImpProtocolo.ImpProtocoloSt.ShowPreviewDialog();
                        break;
                    case dllImpresso.Botoes.Botao.email:
                        VirEmailNeon.EmailDiretoNeon.EmalST.Enviar("", "Etiquetas", ImpProtocolo.ImpProtocoloSt, "Etiquetas", "Etiquetas");
                        break;
                    case dllImpresso.Botoes.Botao.pdf:
                    case dllImpresso.Botoes.Botao.PDF_frente:
                        System.Windows.Forms.SaveFileDialog saveFileDialog1 = new SaveFileDialog();
                        saveFileDialog1.DefaultExt = "pdf";
                        if (saveFileDialog1.ShowDialog() == DialogResult.OK)
                        {
                            ImpProtocolo.ImpProtocoloSt.CreateDocument();
                            ImpProtocolo.ImpProtocoloSt.ExportToPdf(saveFileDialog1.FileName);
                        };
                        break;
                }
            }
        }

        private void cBotaoImpBol3_clicado(object sender, EventArgs e)
        {
            if (CarregaDados())
            {
                //*** MRC - INICIO (05/01/2015 10:00) ***
                ImpRTF Impresso = null;

                ImpRTFMalaDiretaEmail ImpressoEmail = null;
                string Path = System.Windows.Forms.Application.StartupPath + @"\TMP\";
                string ImpressoUnico = String.Format("{0}Mala Direta.pdf", Path);

                if (cBotaoImpBol3.BotaoClicado != dllImpresso.Botoes.Botao.email)
                {
                    //Impresso baseado nos destinatarios (com remetente)
                    if (checkLogo.Checked)
                        Impresso = ImpRTFLogo.ImpRTFLogoSt;
                    else
                        Impresso = ImpRTF.ImpRTFSt;

                    Impresso.Apontamento(lookupBlocosAptos_F1.CON_sel, -1);
                    Impresso.bindingSourcePrincipal.DataMember = "";
                    Impresso.bindingSourcePrincipal.DataSource = dDestinatarios1.DadosDestinatarios;
                    Impresso.bindingSourcePrincipal.Sort = "Correio,BLOCodigo,APTNumero";
                    Impresso.QuadroRTF.Rtf = rtfEditor1.Rtf;
                    Impresso.QuadroRTFSegundaPagina.Rtf = rtfEditor2.Rtf;
                }
                else
                {
                    //Impresso �nico (sem remetente)
                    ImpressoEmail = new ImpRTFMalaDiretaEmail();

                    if (checkLogo.Checked)
                    {
                        ImpressoEmail.Logo.Visible = true;
                        ImpressoEmail.QuadroRTF.TopF = 165;
                    }
                    else
                    {
                        ImpressoEmail.Logo.Visible = false;
                        ImpressoEmail.QuadroRTF.TopF = 0;
                    }
                    ImpressoEmail.QuadroRTF.Rtf = rtfEditor1.Rtf;
                }
                //*** MRC - TERMINO (05/01/2015 10:00) ***
               
                switch (cBotaoImpBol3.BotaoClicado)
                {
                    case dllImpresso.Botoes.Botao.botao:
                    case dllImpresso.Botoes.Botao.imprimir:
                    case dllImpresso.Botoes.Botao.imprimir_frente:
                        Impresso.Apontamento(lookupBlocosAptos_F1.CON_sel, (int)lookUpTPI.EditValue);
                        Impresso.CreateDocument();
                        Impresso.Print();
                        break;
                    case dllImpresso.Botoes.Botao.tela:
                        Impresso.CreateDocument();
                        Impresso.ShowPreviewDialog();
                        break;
                    case dllImpresso.Botoes.Botao.email:
                        //*** MRC - INICIO (05/01/2015 10:00) ***
                        if (!System.IO.Directory.Exists(Path))
                            System.IO.Directory.CreateDirectory(Path);
                        if (System.IO.File.Exists(ImpressoUnico))
                            try
                            { System.IO.File.Delete(ImpressoUnico); }
                            catch
                            { ImpressoUnico = String.Format("{0}Mala Direta_{1:ddMMyyhhmmss}.pdf", Path, DateTime.Now); };
                        System.Threading.Thread.Sleep(3000);
                        ImpressoEmail.CreateDocument();
                        ImpressoEmail.ExportToPdf(ImpressoUnico);
                        //*** MRC - TERMINO (05/01/2015 10:00) ***

                        //*** MRC - INICIO (10/12/2014 12:00) ***
                        bool booFaltaEmail = false;
                        string ConteudoEmail = "Segue documento em anexo.\r\n" +
                                                "\r\n" +
                                                "Atenciosamente,\r\n" +
                                                "Neon Im�veis\r\n" +
                                                "\r\n" +
                                                "\"Este e-mail foi enviado automaticamente. Favor n�o responder.\"";
                        //*** MRC - TERMINO (10/12/2014 12:00) ***

                        //*** MRC - INICIO (05/01/2015 10:00) ***
                        using (CompontesBasicos.Espera.cEspera Esp = new CompontesBasicos.Espera.cEspera(this))
                        {
                            Esp.Espere("Enviando e-mails. Aguarde ...");
                            Esp.AtivaGauge(dDestinatarios1.DadosDestinatarios.Count);
                            Application.DoEvents();
                            int i = 0;
                            DateTime prox = DateTime.Now.AddSeconds(3);
                            foreach (dDestinatarios.DadosDestinatariosRow row in dDestinatarios1.DadosDestinatarios)
                            {
                                i++;
                                if (DateTime.Now > prox)
                                {
                                    prox = DateTime.Now.AddSeconds(3);
                                    Esp.Gauge(i);
                                }
                                //*** MRC - INICIO (10/12/2014 12:00) ***
                                if (!row.IsPESEmailNull())
                                    if (row.PESEmail != "")
                                        VirEmailNeon.EmailDiretoNeon.EmalST.Enviar(VirEmailNeon.EmailDiretoNeon.FormaEnvio.ViaServidor, row.PESEmail, ConteudoEmail, memoEdit1.Text, new Attachment(ImpressoUnico));
                                    else booFaltaEmail = true;
                                else booFaltaEmail = true;
                            }
                            Esp.Gauge(dDestinatarios1.DadosDestinatarios.Count);
                            System.Threading.Thread.Sleep(3000);
                        }

                        if (booFaltaEmail) MessageBox.Show("ATEN��O: Envio de e-mail finalizado. Existem unidades que n�o possuem e-mail cadastrado, providenciar o envio manualmente.");
                        else MessageBox.Show("Envio de e-mail finalizado.");
                        //*** MRC - TERMINO (10/12/2014 12:00) ***
                        //*** MRC - TERMINO (05/01/2015 10:00) ***
                        break;
                    case dllImpresso.Botoes.Botao.pdf:
                    case dllImpresso.Botoes.Botao.PDF_frente:
                        System.Windows.Forms.SaveFileDialog saveFileDialog1 = new SaveFileDialog();
                        saveFileDialog1.DefaultExt = "pdf";
                        if (saveFileDialog1.ShowDialog() == DialogResult.OK)
                        {
                            Impresso.CreateDocument();
                            Impresso.ExportToPdf(saveFileDialog1.FileName);
                        };
                        break;
                }                            
            }
        }

        

        private dImpProtocolo _dReserva;

        private dImpProtocolo dReserva {
            get {
                if (_dReserva == null)
                    _dReserva = new dImpProtocolo();
                return _dReserva;
            }
        }

        private void Res_Click(object sender, EventArgs e)
        {
            if (CarregaDados())
            {                
                foreach (dDestinatarios.DadosDestinatariosRow row in dDestinatarios1.DadosDestinatarios)
                {
                    string Bloco = row.BLOCodigo;
                    if (row.BLONome != row.BLOCodigo)
                        Bloco += " " + row.BLONome;
                    AbstratosNeon.ABS_Apartamento Apartamento = BuscaAPT(row.APT);
                    dReserva.DadosProt.AddDadosProtRow(row.APTNumero, 
                                                       row.BOLNome, 
                                                       row.BOLEndereco, 
                                                       Bloco, 
                                                       row.Proprietario ? "P" : "I", 
                                                       //chIdent.Checked ? lookupBlocosAptos_F1.CON_selrow.CONCodigo + " - " + Bloco + " - " + row.APTNumero : "",
                                                       lookupBlocosAptos_F1.CON_selrow.CONCodigo + " - " + Bloco + " - " + row.APTNumero,
                                                       row.CONNomeBloco,
                                                       (int)Apartamento.FormaPagamentoEfetivo(null, row.Proprietario, ForcarPapel),
                                                       0);
                };
                ResL.Enabled = true;
                ResImp.Enabled = true;
            }
        }

        private void ResL_Click(object sender, EventArgs e)
        {
            dReserva.DadosProt.Clear();
            ResL.Enabled = false;
            ResImp.Enabled = false;
        }

        private void ResImp_clicado(object sender, EventArgs e)
        {
            ImpEtiquetaSt.dImpProtocolo1.DadosProt.Clear();
            ImpEtiquetaSt.xrCondominio.Text = "RESERVA";
            ImpEtiquetaSt.dImpProtocolo1.DadosProt.Merge(dReserva.DadosProt);            
            impressaoEtiquetas(ResImp.BotaoClicado);                        
        }

        private void simpleButton1_Click(object sender, EventArgs e)
        {
            int margem = dEstacao.dEstacaoSt.GetValor("Mala direta margem", 84);
            decimal dmargem = margem / 10;
            if (VirInput.Input.Execute("Margem superior (mm)", ref dmargem, 1))
            {
                margem = (int)(dmargem * 10);
                dEstacao.dEstacaoSt.SetValor("Mala direta margem", margem.ToString());
            }

        }


    }
}

