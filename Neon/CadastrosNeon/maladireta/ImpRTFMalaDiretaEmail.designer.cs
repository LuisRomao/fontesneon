﻿namespace dllImpresso
{
    partial class ImpRTFMalaDiretaEmail
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(ImpRTFMalaDiretaEmail));
            this.Detail = new DevExpress.XtraReports.UI.DetailBand();
            this.QuadroRTF = new DevExpress.XtraReports.UI.XRRichText();
            this.Logo = new DevExpress.XtraReports.UI.XRPictureBox();
            this.TopMargin = new DevExpress.XtraReports.UI.TopMarginBand();
            this.BottomMargin = new DevExpress.XtraReports.UI.BottomMarginBand();
            ((System.ComponentModel.ISupportInitialize)(this.QuadroRTF)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            // 
            // Detail
            // 
            this.Detail.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.QuadroRTF,
            this.Logo});
            this.Detail.Dpi = 254F;
            this.Detail.HeightF = 3030F;
            this.Detail.Name = "Detail";
            this.Detail.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            this.Detail.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // QuadroRTF
            // 
            this.QuadroRTF.CanGrow = false;
            this.QuadroRTF.CanShrink = true;
            this.QuadroRTF.Dpi = 254F;
            this.QuadroRTF.KeepTogether = true;
            this.QuadroRTF.LocationFloat = new DevExpress.Utils.PointFloat(0F, 165F);
            this.QuadroRTF.Name = "QuadroRTF";
            this.QuadroRTF.SerializableRtfString = resources.GetString("QuadroRTF.SerializableRtfString");
            this.QuadroRTF.SizeF = new System.Drawing.SizeF(1820F, 2865F);
            // 
            // Logo
            // 
            this.Logo.Dpi = 254F;
            this.Logo.Image = ((System.Drawing.Image)(resources.GetObject("Logo.Image")));
            this.Logo.LocationFloat = new DevExpress.Utils.PointFloat(0F, 0F);
            this.Logo.Name = "Logo";
            this.Logo.SizeF = new System.Drawing.SizeF(339F, 160F);
            this.Logo.Sizing = DevExpress.XtraPrinting.ImageSizeMode.ZoomImage;
            // 
            // TopMargin
            // 
            this.TopMargin.Dpi = 254F;
            this.TopMargin.HeightF = 51F;
            this.TopMargin.Name = "TopMargin";
            this.TopMargin.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            this.TopMargin.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // BottomMargin
            // 
            this.BottomMargin.Dpi = 254F;
            this.BottomMargin.HeightF = 51F;
            this.BottomMargin.Name = "BottomMargin";
            this.BottomMargin.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            this.BottomMargin.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // ImpRTFMalaDireta
            // 
            this.Bands.AddRange(new DevExpress.XtraReports.UI.Band[] {
            this.Detail,
            this.TopMargin,
            this.BottomMargin});
            this.Dpi = 254F;
            this.ExportOptions.Pdf.ImageQuality = DevExpress.XtraPrinting.PdfJpegImageQuality.Lowest;
            this.Margins = new System.Drawing.Printing.Margins(150, 130, 51, 51);
            this.PageHeight = 2970;
            this.PageWidth = 2100;
            this.PaperKind = System.Drawing.Printing.PaperKind.A4;
            this.ReportUnit = DevExpress.XtraReports.UI.ReportUnit.TenthsOfAMillimeter;
            this.Version = "14.1";
            ((System.ComponentModel.ISupportInitialize)(this.QuadroRTF)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();

        }

        #endregion

        private DevExpress.XtraReports.UI.DetailBand Detail;
        private DevExpress.XtraReports.UI.TopMarginBand TopMargin;
        private DevExpress.XtraReports.UI.BottomMarginBand BottomMargin;
        /// <summary>
        /// Conteúdo da Mala Direta
        /// </summary>
        public DevExpress.XtraReports.UI.XRRichText QuadroRTF;
        /// <summary>
        /// Logo da Mala Direta
        /// </summary>
        public DevExpress.XtraReports.UI.XRPictureBox Logo;
    }
}
