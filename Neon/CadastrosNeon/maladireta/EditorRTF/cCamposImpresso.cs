using System;
using System.Data;

namespace maladireta.EditorRTF
{
    public partial class cCamposImpresso : CompontesBasicos.ComponenteCamposBase
    {
        private dImpressos _dImpressos;

        private dImpressos DImpressos
        {
            get => _dImpressos ?? (_dImpressos = dImpressos.dImpressosSt);
            set => _dImpressos = value;
        }

        /// <summary>
        /// Construtor
        /// </summary>
        public cCamposImpresso()
        {
            InitializeComponent();
            //BindingSource_F.DataSource = dImpressos.dImpressosSt;
            //TableAdapterPrincipal = dImpressos.dImpressosSt.IMpreSsosTableAdapter;
        }

        /// <summary>
        /// Construtor que cadastra um novo impresso conforme o modelo
        /// </summary>
        /// <param name="IMSNome"></param>
        /// <param name="IMSNomeModelo"></param>
        /// <param name="Descricao"></param>
        public cCamposImpresso(string IMSNome, string IMSNomeModelo = null, string Descricao = null) :this()
        {
            DImpressos = new dImpressos();
            if (DImpressos.IMpreSsosTableAdapter.FillByNome(DImpressos.IMpreSsos, IMSNome) == 0)
            {
                if (IMSNomeModelo != null)
                {
                    DImpressos.IMpreSsosTableAdapter.FillByNome(DImpressos.IMpreSsos, IMSNomeModelo);
                    dImpressos.IMpreSsosRow ModeloIMS = DImpressos.IMpreSsos[0];
                    DImpressos.IMpressosCamposTableAdapter.FillByIMS(DImpressos.IMpressosCampos, ModeloIMS.IMS);
                    dImpressos.IMpreSsosRow NovaIMS = DImpressos.IMpreSsos.NewIMpreSsosRow();
                    NovaIMS.IMSI_USU = Framework.DSCentral.USU;
                    NovaIMS.IMSDATAI = DateTime.Now;
                    NovaIMS.IMSDescritivo = Descricao;
                    NovaIMS.IMSNome = IMSNome;
                    NovaIMS.IMSRTF = ModeloIMS.IMSRTF;
                    DImpressos.IMpreSsos.AddIMpreSsosRow(NovaIMS);
                    DImpressos.IMpreSsosTableAdapter.Update(NovaIMS);
                    foreach (dImpressos.IMpressosCamposRow ModeloIMC in ModeloIMS.GetIMpressosCamposRows())
                    {
                        dImpressos.IMpressosCamposRow NovaIMC = DImpressos.IMpressosCampos.NewIMpressosCamposRow();
                        NovaIMC.ISCDescritivo = ModeloIMC.ISCDescritivo;
                        if (!ModeloIMS.IsIMSDescritivoNull())
                            NovaIMC.ISCExemplo = ModeloIMC.ISCExemplo;
                        NovaIMC.ISC_IMS = NovaIMS.IMS;
                        DImpressos.IMpressosCampos.AddIMpressosCamposRow(NovaIMC);
                        DImpressos.IMpressosCamposTableAdapter.Update(NovaIMC);
                    }
                    LinhaMae = NovaIMS;
                }
                else
                {                                                            
                    dImpressos.IMpreSsosRow NovaIMS = DImpressos.IMpreSsos.NewIMpreSsosRow();
                    NovaIMS.IMSI_USU = Framework.DSCentral.USU;
                    NovaIMS.IMSDATAI = DateTime.Now;
                    NovaIMS.IMSDescritivo = Descricao;
                    NovaIMS.IMSNome = IMSNome;                    
                    DImpressos.IMpreSsos.AddIMpreSsosRow(NovaIMS);
                    DImpressos.IMpreSsosTableAdapter.Update(NovaIMS);                    
                    LinhaMae = NovaIMS;
                }
            }
            else
            {
                LinhaMae = DImpressos.IMpreSsos[0];
                DImpressos.IMpressosCamposTableAdapter.FillByIMS(DImpressos.IMpressosCampos, LinhaMae.IMS);
            }
            BindingSource_F.DataSource = DImpressos;
            TableAdapterPrincipal = DImpressos.IMpreSsosTableAdapter;
            PopulaDetalhe();
        }

        dImpressos.IMpreSsosRow LinhaMae
        {
            get => (dImpressos.IMpreSsosRow)LinhaMae_F; 
            set => LinhaMae_F = value;
        }

        private void PopulaDetalhe()
        {
            Campos = new DataTable();
            foreach (dImpressos.IMpressosCamposRow row in LinhaMae.GetIMpressosCamposRows())
                Campos.Columns.Add(row.ISCDescritivo);
            DataRow DR = Campos.NewRow();
            foreach (dImpressos.IMpressosCamposRow row in LinhaMae.GetIMpressosCamposRows())
                DR[row.ISCDescritivo] = (row.IsISCExemploNull() ? row.ISCDescritivo : row.ISCExemplo);
            Campos.Rows.Add(DR);
            richEditControl1.Options.MailMerge.DataSource = Campos;
        }

        private DataTable Campos;

        private void cCamposImpresso_cargaFinal(object sender, EventArgs e)
        {
            PopulaDetalhe();            
        }

        private void cCamposImpresso_cargaInicial(object sender, EventArgs e)
        {
            BindingSource_F.DataSource = DImpressos;
            TableAdapterPrincipal = DImpressos.IMpreSsosTableAdapter;
        }
    }
}
