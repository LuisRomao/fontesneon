using System;

namespace maladireta.EditorRTF
{
    /// <summary>
    /// 
    /// </summary>
    public partial class cGradeImpressos : CompontesBasicos.ComponenteGradeNavegadorP
    {
        /// <summary>
        /// 
        /// </summary>
        public cGradeImpressos()
        {
            InitializeComponent();
            iMpreSsosBindingSource.DataSource = dImpressos.dImpressosSt;
            TableAdapterPrincipal = dImpressos.dImpressosSt.IMpreSsosTableAdapter;
            USUbindingSource.DataSource = FrameworkProc.datasets.dUSUarios.dUSUariosSt;
        }

        private void cGradeImpressos_cargaInicial(object sender, EventArgs e)
        {
            dImpressos.dImpressosSt.IMpressosCampos.Clear();
        }

        private void cGradeImpressos_cargaFinal(object sender, EventArgs e)
        {
            dImpressos.dImpressosSt.IMpressosCamposTableAdapter.Fill(dImpressos.dImpressosSt.IMpressosCampos);
        }

        /// <summary>
        /// 
        /// </summary>
        public override void RefreshDados()
        {
            base.RefreshDados();
        }
    }
}
