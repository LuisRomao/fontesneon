﻿using System.Windows.Forms;

namespace maladireta.EditorRTF
{
    /// <summary>
    /// 
    /// </summary>
    public partial class cGeraRTF : CompontesBasicos.ComponenteBaseDialog
    {
        /// <summary>
        /// construtor padrão para designer time
        /// </summary>
        public cGeraRTF()
        {
            InitializeComponent();
        }

        /// <summary>
        /// Construtor
        /// </summary>
        /// <param name="_retornodechamada">Callback</param>
        /// <param name="Texto">Texto plano</param>
        public cGeraRTF(del_retornodechamada _retornodechamada,string Texto)
        {
            InitializeComponent();
            retornodechamada = _retornodechamada;
            richEditControl1.Text = Texto;
        }

        /// <summary>
        /// Construtor
        /// </summary>
        /// <param name="TextoRTF">Atenção é RTF e nao texto plano</param>
        public cGeraRTF(string TextoRTF)
        {
            InitializeComponent();            
            richEditControl1.RtfText = TextoRTF;
        }
        

        /// <summary>
        /// Retorna a chamada
        /// </summary>
        /// <param name="Modo"></param>
        /// <param name="dados"></param>
        protected override void RetornaChamada(DialogResult Modo, params object[] dados)
        {
            base.RetornaChamada(Modo, richEditControl1.RtfText);
        }

        
    }
}
