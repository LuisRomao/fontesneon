namespace Calendario.Zoom
{
    partial class fChamada
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.cChamada1 = new Calendario.Zoom.cChamada();
            this.SuspendLayout();
            // 
            // cChamada1
            // 
            this.cChamada1.Doc = System.Windows.Forms.DockStyle.Fill;
            this.cChamada1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.cChamada1.Estado = CompontesBasicos.EstadosDosComponentes.Registrado;
            
            this.cChamada1.Location = new System.Drawing.Point(0, 0);
            this.cChamada1.LookAndFeel.UseDefaultLookAndFeel = false;
            this.cChamada1.Name = "cChamada1";
            this.cChamada1.Size = new System.Drawing.Size(613, 465);
            this.cChamada1.TabIndex = 0;
            this.cChamada1.Titulo = null;
            // 
            // fChamada
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.CaixaAltaGeral = false;
            this.ClientSize = new System.Drawing.Size(613, 465);
            this.Controls.Add(this.cChamada1);
            this.Name = "fChamada";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.ResumeLayout(false);

        }

        #endregion

        /// <summary>
        /// 
        /// </summary>
        public cChamada cChamada1;

    }
}
