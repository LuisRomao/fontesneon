using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace Calendario.Zoom
{
    /// <summary>
    /// Classe para edi��o de uma chamada
    /// </summary>
    public partial class cChamada : CompontesBasicos.ComponenteBase
    {
        /// <summary>
        /// Construtor padr�o
        /// </summary>
        public cChamada()
        {
            InitializeComponent();
        }

        private dAgenda.HISTORICOSRow HistRow;

        /// <summary>
        /// 
        /// </summary>
        /// <param name="_HistRow"></param>
        public void Carregar(dAgenda.HISTORICOSRow _HistRow)
        {
            HistRow = _HistRow;
            memoAntigo.Text = HistRow.HSTDescricao;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="Tipo"></param>
        public void apenda(string Tipo)
        {
            if (HistRow.HSTDescricao != "")
                HistRow.HSTDescricao += "\r\n\r\n";
            HistRow.HSTDescricao += "(" + DateTime.Now.ToString() + ")" + CompontesBasicos.Bancovirtual.UsuarioLogado.UsuarioLogadoSt.USUNome + " " + Tipo + "\r\n";
            if (memoEdit2.Text != "")
                HistRow.HSTDescricao += "--------------\r\n" + memoEdit2.Text;
            HistRow.HSTADATA = DateTime.Now;
        }

        private void simpleButton1_Click(object sender, EventArgs e)
        {
            apenda("Post");
            if(Parent is Form)
               ((Form)Parent).DialogResult = DialogResult.OK;
        }

        private void simpleButton2_Click(object sender, EventArgs e)
        {
            if (Parent is Form)
                ((Form)Parent).DialogResult = DialogResult.Cancel;
        }
    }
}

