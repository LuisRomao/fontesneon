using System;
using DevExpress.XtraScheduler;
using DevExpress.XtraScheduler.UI;


namespace Calendario.Agenda
{
    /// <summary>
    /// 
    /// </summary>
    public class ControladorCampos : AppointmentFormController
    {
        //public string[] MappingNames { get { return new string[] { "USU", "AGBRec2" }; } }

        /*
        public bool AreCustomFieldsEqual(string customFieldName, Appointment apt1, Appointment apt2)
        {
            if (apt1.CustomFields[customFieldName] == null || apt2.CustomFields[customFieldName] == null)
                return false;
            return apt1.CustomFields[customFieldName].Equals(apt2.CustomFields[customFieldName]);
        }

        public new void AssignCustomField(string customFieldName, Appointment dest, Appointment source)
        {
            dest.CustomFields[customFieldName] = source.CustomFields[customFieldName];
        }


        /*
        protected override void ApplyChangesCore()
        {
            ExceptionHelper exceptionHelper = new ExceptionHelper(this);
            exceptionHelper.AssignRecurrenceProperties(EditedPattern, SourceAppointment);

            AppointmentFormAppointmentCopyHelper copyHelper = new AppointmentFormAppointmentCopyHelper(this);
            copyHelper.GetType().GetMethod("AssignSimpleProperties", System.Reflection.BindingFlags.NonPublic | System.Reflection.BindingFlags.Instance).Invoke(copyHelper, new object[] { EditedAppointmentCopy, SourceAppointment });
            copyHelper.GetType().GetMethod("AssignCollectionProperties", System.Reflection.BindingFlags.NonPublic | System.Reflection.BindingFlags.Instance).Invoke(copyHelper, new object[] { EditedAppointmentCopy, SourceAppointment });
            ApplyCustomFieldsValues();

        }
        */

        /// <summary>
        /// 
        /// </summary>
        public int USU { 
            get {
                if (Control.Storage.Resources.Mappings.Id == "USU")
                    return (int)ResourceId;
                else
                    return (int)(EditedAppointmentCopy.CustomFields["AGBRec2"] ?? 0);

            } 
            set {
                if (Control.Storage.Resources.Mappings.Id == "USU")
                    ResourceId = value;
                else
                    EditedAppointmentCopy.CustomFields["AGBRec2"] = value;
            } 
        }

        //EditedAppointmentCopy.CustomFields["AGBDataLimite"] = this.dtStart.DateTime.Date.AddDays(-1);

        /// <summary>
        /// 
        /// </summary>
        public DateTime AGBDataLimite 
        {
            get 
            {
                return (DateTime)(EditedAppointmentCopy.CustomFields["AGBDataLimite"] ?? DateTime.MinValue);
            }
            set 
            {
                EditedAppointmentCopy.CustomFields["AGBDataLimite"] = value;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        public int TAG {
            get
            {
                if (Control.Storage.Resources.Mappings.Id == "TAG")
                    return (int)ResourceId;
                else
                    return (int)(EditedAppointmentCopy.CustomFields["AGBRec2"] ?? 0);

            }
            set
            {
                if (Control.Storage.Resources.Mappings.Id == "TAG")
                    ResourceId = value;
                else
                    EditedAppointmentCopy.CustomFields["AGBRec2"] = value;
            } 
        }

        private int SourceUSU { 
            get {
                if(Control.Storage.Resources.Mappings.Id == "USU")
                    return (int)SourceAppointment.ResourceId; 
                else
                    return (int)SourceAppointment.CustomFields["AGBRec2"]; 
            } 
            set {
                if (Control.Storage.Resources.Mappings.Id == "USU")
                    SourceAppointment.ResourceId = value;
                else
                    SourceAppointment.CustomFields["AGBRec2"] = value;
            } 
        }

        private int SourceTAG {
            get
            {
                if (Control.Storage.Resources.Mappings.Id == "TAG")
                    return (int)SourceAppointment.ResourceId;
                else
                    return (int)SourceAppointment.CustomFields["AGBRec2"];
            }
            set
            {
                if (Control.Storage.Resources.Mappings.Id == "TAG")
                    SourceAppointment.ResourceId = value;
                else
                    SourceAppointment.CustomFields["AGBRec2"] = value;
            } 
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="control"></param>
        /// <param name="apt"></param>
        public ControladorCampos(SchedulerControl control, Appointment apt)
            : base(control, apt)
        {
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public override bool IsAppointmentChanged()
        {
            if (base.IsAppointmentChanged())
                return true;
            return (SourceUSU != USU) || (SourceTAG != TAG);
        }

        /*
        protected override void ApplyCustomFieldsValues()
        {
            if (Control.Storage.Resources.Mappings.Id == "USU")
                SourceTAG = TAG;
            else
                SourceUSU = USU;
            
        }
        */
    
    }

    /*

    public class ExceptionHelper
    {
        ControladorCampos controller;

        public ExceptionHelper(ControladorCampos controller)
        {
            if (controller == null)
                Exceptions.ThrowArgumentNullException("controller");
            this.controller = controller;
        }

        public ControladorCampos Controller { get { return controller; } }

        //TODO: test it!
        public void AssignRecurrenceProperties(Appointment src, Appointment target)
        {
            if (target.Type == AppointmentType.Pattern && src != null)
            {
                if (!((target.Start.Equals(src.Start) && (target.Duration.Equals(src.Duration))) && AreRecurrenceInfosEqual(target.RecurrenceInfo, src.RecurrenceInfo)))
                    target.DeleteExceptions();

                ExceptionPropertiesManager manager = new ExceptionPropertiesManager(target, src, Controller);
                AppointmentBaseCollection exceptions = target.GetExceptions();
                foreach (Appointment exception in exceptions)
                {
                    manager.Apply(exception);
                }
                AssignRecurrenceInfo(target.RecurrenceInfo, src.RecurrenceInfo);
            }
        }
        void AssignRecurrenceInfo(RecurrenceInfo dst, RecurrenceInfo src)
        {
            dst.BeginUpdate();
            try
            {
                dst.DayNumber = src.DayNumber;
                dst.WeekOfMonth = src.WeekOfMonth;
                dst.Periodicity = src.Periodicity;
                dst.Month = src.Month;
                dst.OccurrenceCount = src.OccurrenceCount;
                dst.Range = src.Range;
                dst.Type = src.Type;
                dst.WeekDays = src.WeekDays;
                dst.AllDay = src.AllDay;
                dst.Start = src.Start;
                dst.Duration = src.Duration;
            }
            finally
            {
                dst.EndUpdate();
            }
        }
        bool AreRecurrenceInfosEqual(RecurrenceInfo info1, RecurrenceInfo info2)
        {
            if (info1.Type != info2.Type)
                return false;
            if (info1.Start != info2.Start)
                return false;
            if (info1.Range != info2.Range)
                return false;
            if (info1.Range == RecurrenceRange.OccurrenceCount && info1.OccurrenceCount != info2.OccurrenceCount)
                return false;
            if (info1.Range == RecurrenceRange.EndByDate && info1.End != info2.End)
                return false;
            if (info1.Type == RecurrenceType.Daily || info1.Type == RecurrenceType.Weekly)
                if (info1.WeekDays != info2.WeekDays || info1.Periodicity != info2.Periodicity)
                    return false;
            if (info1.Type == RecurrenceType.Monthly || info1.Type == RecurrenceType.Yearly)
            {
                if (info1.WeekOfMonth == WeekOfMonth.None)
                {
                    if (info1.DayNumber != info2.DayNumber)
                        return false;
                }
                else
                    if (info1.WeekDays != info2.WeekDays)
                        return false;
            }
            if (info1.Type == RecurrenceType.Monthly)
            {
                if (info1.WeekOfMonth != info2.WeekOfMonth || info1.Periodicity != info2.Periodicity)
                    return false;
            }
            if (info1.Type == RecurrenceType.Yearly)
                if (info1.WeekOfMonth != info2.WeekOfMonth || info1.Month != info2.Month)
                    return false;
            return true;
        }
    }

    public class ExceptionPropertiesManager
    {
        #region Fields
        Appointment oldPattern;
        Appointment newPattern;
        ControladorCampos controller;
        #endregion

        public ExceptionPropertiesManager(Appointment oldPattern, Appointment newPattern, ControladorCampos controller)
        {
            if (oldPattern == null)
                Exceptions.ThrowArgumentNullException("oldPattern");
            if (newPattern == null)
                Exceptions.ThrowArgumentNullException("newPattern");
            if (controller == null)
                Exceptions.ThrowArgumentNullException("controller");
            this.oldPattern = oldPattern;
            this.newPattern = newPattern;
            this.controller = controller;
        }

        #region Properties
        public Appointment OldPattern { get { return oldPattern; } }
        public Appointment NewPattern { get { return newPattern; } }
        public ControladorCampos Controller { get { return controller; } }
        #endregion

        public void Apply(Appointment exception)
        {
            AssignSimpleProperties(OldPattern, NewPattern, exception);
            AssignCollectionProperties(OldPattern, NewPattern, exception);
        }

        protected internal void AssignSimpleProperties(Appointment oldPattern, Appointment newPattern, Appointment exception)
        {
            if (exception.Subject == oldPattern.Subject)
                exception.Subject = newPattern.Subject;
            if (exception.Location == oldPattern.Location)
                exception.Location = newPattern.Location;
            if (exception.Description == oldPattern.Description)
                exception.Description = newPattern.Description;
            if (exception.StatusId == oldPattern.StatusId)
                exception.StatusId = newPattern.StatusId;
            if (exception.LabelId == oldPattern.LabelId)
                exception.LabelId = newPattern.LabelId;
        }
        public void AssignCollectionProperties(Appointment oldPattern, Appointment newPattern, Appointment exception)
        {
            AssignCustomFields(oldPattern, NewPattern, exception);
            if (!(exception.Reminders is ReminderReadOnlyCollection))
                if (AreReminderCollectionsEqual(oldPattern.Reminders, exception.Reminders))
                {
                    exception.Reminders.Clear();
                    exception.Reminders.AddRange(newPattern.Reminders);
                }
        }
        protected internal virtual bool AreReminderCollectionsEqual(ReminderCollection reminders1, ReminderCollection reminders2)
        {
            int count = reminders1.Count;
            if (reminders2.Count != count)
                return false;
            ReminderCollection copyReminders1 = new ReminderCollection();
            copyReminders1.AddRange(reminders1);
            ReminderCollection copyReminders2 = new ReminderCollection();
            copyReminders2.AddRange(reminders2);

            TimeBeforeStartReminderComparer comparer = new TimeBeforeStartReminderComparer();
            copyReminders1.Sort(comparer);
            copyReminders2.Sort(comparer);

            for (int i = 0; i < count; i++)
                if (copyReminders1[i].TimeBeforeStart != copyReminders2[i].TimeBeforeStart)
                    return false;
            return true;
        }
        protected internal virtual void AssignCustomFields(Appointment oldPattern, Appointment newPattern, Appointment exception)
        {
            string[] mappingNames = Controller.MappingNames;
            int count = mappingNames.Length;
            for (int i = 0; i < count; i++)
            {
                if (Controller.AreCustomFieldsEqual(mappingNames[i], oldPattern, exception))
                    Controller.AssignCustomField(mappingNames[i], exception, newPattern);
            }
        }
        #region TimeBeforeStartReminderComparer
        class TimeBeforeStartReminderComparer : IComparer<Reminder>
        {
            #region IComparer<Reminder> Members
            public int Compare(Reminder x, Reminder y)
            {
                if (x.TimeBeforeStart < y.TimeBeforeStart)
                    return -1;
                if (x.TimeBeforeStart == y.TimeBeforeStart)
                    return 0;
                return 1;
            }
            #endregion
        }
        #endregion
    }


    */
}
