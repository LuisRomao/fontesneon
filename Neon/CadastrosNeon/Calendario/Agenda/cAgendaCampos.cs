using System;
using System.Data;
using System.Windows.Forms;
using Framework;
using FrameworkProc.datasets;
using VirEnumeracoesNeon;

namespace Calendario.Agenda
{
    /// <summary>
    /// 
    /// </summary>
    public partial class cAgendaCampos : CompontesBasicos.ComponenteCamposBase
    {
        /// <summary>
        /// 
        /// </summary>
        public cAgendaCampos()
        {
            InitializeComponent();
            //BindingSource_F.DataSource = dAGendaBase.dAGendaBaseSt;
            cONDOMINIOSBindingSource.DataSource = FrameworkProc.datasets.dCondominiosAtivos.dCondominiosAtivosST;
            tipoAGendamentoBindingSource.DataSource = dAGendaBase.dAGendaBaseSt;
            uSUariosBindingSource.DataSource = dUSUarios.dUSUariosSt;
            //TableAdapterPrincipal = dAGendaBase.dAGendaBaseSt.AGendaBaseTableAdapter;
            TableAdapterPrincipal = dAGendaBase.AGendaBaseTableAdapter;
        }

        private dAGendaBase.AGendaBaseRow LinhaMae 
        {
            get 
            {
                return (dAGendaBase.AGendaBaseRow)LinhaMae_F;
            }
        }

        private cAgendaGrade Chamador
        {
            get
            {
                if (GradeChamadora != null)
                    return (cAgendaGrade)GradeChamadora;
                else
                    return null;
            }
        }

        private System.Collections.ArrayList NaoCancelavel = new System.Collections.ArrayList(new TiposTAG[] { 
            TiposTAG.BOLETOS_CONTRATOS,
            TiposTAG.Contas,
            TiposTAG.Contratos,
            TiposTAG.Debito_Automatico,
            TiposTAG.MANDATO_DO_SINDICO}); 

        private void cAgendaCampos_cargaFinal(object sender, EventArgs e)
        {
            if (pk == 0)
            {
                LinhaMae.AGB_CON = Chamador.CON;
                LinhaMae.AGBDiaTodo = true;

                LinhaMae.AGBDataTermino = DateTime.MinValue;
                lookUpEdit2.Properties.ReadOnly = false;
                LinhaMae.AGBMemo = string.Format("{0:dd/MM/yyyy HH:mm} inclu�do por {1}", DateTime.Now, Framework.usuarioLogado.UsuarioLogadoNeon.UsuarioLogadoStNEON.USUNome);
                simpleButton1.Visible = false;
            }
            else
                if (NaoCancelavel.Contains((TiposTAG)LinhaMae.AGB_TAG))
                    simpleButton1.Visible = false;
            lookUpEdit1.Properties.ReadOnly = true;
            LinhaMae.AGBStatus = (int)AGBStatus.Agendado;
        }

        private string AlteracaoCampo(string Campo, string Titulo, bool memo)
        {
            string velho = LinhaMae[Campo, DataRowVersion.Original].ToString();
            string novo = LinhaMae[Campo, DataRowVersion.Current].ToString();
            if (novo != velho)
                if (memo)
                    return string.Format("\r\n{0}: Original:\r\n{1}\r\nNovo:\r\n{2}", Titulo, LinhaMae[Campo, DataRowVersion.Original], LinhaMae[Campo, DataRowVersion.Current]);
                else
                    return string.Format("\r\n{0}: {1} -> {2}", Titulo, LinhaMae[Campo, DataRowVersion.Original], LinhaMae[Campo, DataRowVersion.Current]);
            else
                return "";
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public override bool Update_F()
        {
            Validate(true);
            dUSUarios.USUariosRow rowUSUNovo = dUSUarios.dUSUariosSt.USUarios.FindByUSU(LinhaMae.AGB_USU);
            if (rowUSUNovo == null)
            {
                MessageBox.Show("Respons�vel n�o definido");
                return false;
            }            
            if (!CamposObrigatoriosOk(BindingSourcePrincipal))
                return false;
            
            BindingSourcePrincipal.EndEdit();
            
            
            if (LinhaMae.AGBDataTermino < LinhaMae.AGBDataInicio)
                LinhaMae.AGBDataTermino = LinhaMae.AGBDataInicio;
            dAGendaBase.dAGendaBaseSt.AjustaStatus(DateTime.Today, LinhaMae);
            if (pk != 0)
            {
                string Justificativa = "";
                if(!VirInput.Input.Execute("Justificativa:",ref Justificativa,true))
                    return false;
                LinhaMae.AGBMemo += string.Format("\r\n---------------\r\n{0:dd/MM/yyyy HH:mm} Alterado por {1}", DateTime.Now, Framework.usuarioLogado.UsuarioLogadoNeon.UsuarioLogadoStNEON.USUNome);
                LinhaMae.AGBMemo += string.Format("\r\nJustificativa :\r\n{0}\r\nAltera��es:", Justificativa);
                LinhaMae.AGBMemo += string.Format(AlteracaoCampo("AGBDescricao","Descri��o",false));
                LinhaMae.AGBMemo += string.Format(AlteracaoCampo("AGBDataInicio","Data",false));                
                int USU = (int)LinhaMae["AGB_USU", DataRowVersion.Original];                
                FrameworkProc.datasets.dUSUarios.USUariosRow rowUSU = FrameworkProc.datasets.dUSUarios.dUSUariosStTodos.USUarios.FindByUSU(USU);
                string Velho = rowUSU.USUNome;
                USU = (int)LinhaMae["AGB_USU", DataRowVersion.Current];
                //rowUSU = Framework.datasets.dUSUarios.dUSUariosSt.USUarios.FindByUSU(USU);
                string Novo = rowUSUNovo.USUNome;
                if(Novo != Velho)
                    LinhaMae.AGBMemo += string.Format("\r\n{0}: {1} -> {2}", "Respons�vel", Velho, Novo);
            }
            return base.Update_F();
        }

        private void lookUpEdit2_EditValueChanged(object sender, EventArgs e)
        {
            if ((lookUpEdit2.EditValue != null) && (lookUpEdit2.EditValue != DBNull.Value) && (lookUpEdit3.EditValue == DBNull.Value))
            {
                int TAG = (int)lookUpEdit2.EditValue;
                dAGendaBase.TipoAGendamentoRow TAGrow = dAGendaBase.dAGendaBaseSt.TipoAGendamento.FindByTAG(TAG);
                lookUpEdit3.EditValue = TAGrow.TAGResp_USU;
                LinhaMae.AGB_USU = TAGrow.TAGResp_USU;
                //lookUpEdit3.DoValidate();
            }
        }

        private void simpleButton1_Click(object sender, EventArgs e)
        {
            if (NaoCancelavel.Contains((TiposTAG)LinhaMae.AGB_TAG))
                return;
            string Justificativa = "";
            if(MessageBox.Show("Confirma que esta tarefa n�o necessita ser renovada","confirma��o",MessageBoxButtons.YesNo,MessageBoxIcon.Warning,MessageBoxDefaultButton.Button2) == DialogResult.Yes)
                if (VirInput.Input.Execute("Justificativa", ref Justificativa,true))
                {
                    LinhaMae.AGBStatus = (int)AGBStatus.Ok;
                    LinhaMae.AGBMemo += string.Format("\r\n{0:dd/MM/yyyy HH:mm} tarefa conclu�da por {1}\r\nJustificativa:\r\n{2}", DateTime.Now, Framework.usuarioLogado.UsuarioLogadoNeon.UsuarioLogadoStNEON.USUNome,Justificativa);
                    if (base.Update_F())
                        FechaTela(DialogResult.OK);
                }

        }

        private void simpleButton2_Click(object sender, EventArgs e)
        {
            string Justificativa = "";
            
                if (VirInput.Input.Execute("OBS.:", ref Justificativa, true))
                {                    
                    LinhaMae.AGBMemo += string.Format("\r\n{0:dd/MM/yyyy HH:mm} {1}\r\nObs:\r\n{2}", DateTime.Now, Framework.usuarioLogado.UsuarioLogadoNeon.UsuarioLogadoStNEON.USUNome, Justificativa);
                    if (base.Update_F())
                        FechaTela(DialogResult.OK);
                }

        }

        private void cAgendaCampos_cargaInicial(object sender, EventArgs e)
        {
            if (pk != 0)
            {
                dAGendaBase.AGendaBaseTableAdapter.FillByAGB(dAGendaBase.AGendaBase, pk);
                LinhaMae_F = dAGendaBase.AGendaBase[0];
            }
        }
    }
}
