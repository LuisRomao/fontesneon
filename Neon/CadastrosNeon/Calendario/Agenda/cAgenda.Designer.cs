namespace Calendario.Agenda
{
    partial class cAgenda
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            DevExpress.XtraScheduler.TimeRuler timeRuler1 = new DevExpress.XtraScheduler.TimeRuler();
            DevExpress.XtraScheduler.TimeRuler timeRuler2 = new DevExpress.XtraScheduler.TimeRuler();
            this.splitContainerControl1 = new DevExpress.XtraEditors.SplitContainerControl();
            this.splitContainerControl2 = new DevExpress.XtraEditors.SplitContainerControl();
            this.schedulerControl1 = new DevExpress.XtraScheduler.SchedulerControl();
            this.schedulerStorage1 = new DevExpress.XtraScheduler.SchedulerStorage(this.components);
            this.AGendaBasebindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.UsuariosbindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.dateNavigator1 = new DevExpress.XtraScheduler.DateNavigator();
            this.resourcesComboBoxControl1 = new DevExpress.XtraScheduler.UI.ResourcesComboBoxControl();
            this.cbGrouping = new DevExpress.XtraEditors.ImageComboBoxEdit();
            this.lblGroup = new System.Windows.Forms.Label();
            this.cbView = new DevExpress.XtraEditors.ImageComboBoxEdit();
            this.lblView = new System.Windows.Forms.Label();
            this.simpleButton1 = new DevExpress.XtraEditors.SimpleButton();
            this.TipoAGendamentobindingSource = new System.Windows.Forms.BindingSource(this.components);
            ((System.ComponentModel.ISupportInitialize)(this.StyleController_F)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainerControl1)).BeginInit();
            this.splitContainerControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainerControl2)).BeginInit();
            this.splitContainerControl2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.schedulerControl1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.schedulerStorage1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.AGendaBasebindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.UsuariosbindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateNavigator1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateNavigator1.CalendarTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.resourcesComboBoxControl1.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbGrouping.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbView.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TipoAGendamentobindingSource)).BeginInit();
            this.SuspendLayout();
            // 
            // ToolTipController_F
            // 
            this.ToolTipController_F.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.ToolTipController_F.Appearance.ForeColor = System.Drawing.Color.Black;
            this.ToolTipController_F.Appearance.Options.UseBackColor = true;
            this.ToolTipController_F.Appearance.Options.UseFont = true;
            this.ToolTipController_F.Appearance.Options.UseForeColor = true;
            this.ToolTipController_F.Appearance.Options.UseTextOptions = true;
            this.ToolTipController_F.Appearance.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            // 
            // StyleController_F
            // 
            this.StyleController_F.AppearanceDisabled.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this.StyleController_F.AppearanceDisabled.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this.StyleController_F.AppearanceDisabled.Options.UseBackColor = true;
            this.StyleController_F.AppearanceDisabled.Options.UseForeColor = true;
            this.StyleController_F.AppearanceFocused.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.StyleController_F.AppearanceFocused.Options.UseBackColor = true;
            // 
            // splitContainerControl1
            // 
            this.splitContainerControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainerControl1.FixedPanel = DevExpress.XtraEditors.SplitFixedPanel.Panel2;
            this.splitContainerControl1.Horizontal = false;
            this.splitContainerControl1.Location = new System.Drawing.Point(0, 0);
            this.splitContainerControl1.Name = "splitContainerControl1";
            this.splitContainerControl1.Panel1.Controls.Add(this.splitContainerControl2);
            this.splitContainerControl1.Panel1.Text = "Panel1";
            this.splitContainerControl1.Panel2.Controls.Add(this.resourcesComboBoxControl1);
            this.splitContainerControl1.Panel2.Controls.Add(this.cbGrouping);
            this.splitContainerControl1.Panel2.Controls.Add(this.lblGroup);
            this.splitContainerControl1.Panel2.Controls.Add(this.cbView);
            this.splitContainerControl1.Panel2.Controls.Add(this.lblView);
            this.splitContainerControl1.Panel2.Controls.Add(this.simpleButton1);
            this.splitContainerControl1.Panel2.Text = "Panel2";
            this.splitContainerControl1.Size = new System.Drawing.Size(1545, 799);
            this.splitContainerControl1.SplitterPosition = 61;
            this.splitContainerControl1.TabIndex = 0;
            this.splitContainerControl1.Text = "splitContainerControl1";
            // 
            // splitContainerControl2
            // 
            this.splitContainerControl2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainerControl2.FixedPanel = DevExpress.XtraEditors.SplitFixedPanel.Panel2;
            this.splitContainerControl2.Location = new System.Drawing.Point(0, 0);
            this.splitContainerControl2.Name = "splitContainerControl2";
            this.splitContainerControl2.Panel1.Controls.Add(this.schedulerControl1);
            this.splitContainerControl2.Panel1.Text = "Panel1";
            this.splitContainerControl2.Panel2.Controls.Add(this.dateNavigator1);
            this.splitContainerControl2.Panel2.Text = "Panel2";
            this.splitContainerControl2.Size = new System.Drawing.Size(1545, 733);
            this.splitContainerControl2.SplitterPosition = 158;
            this.splitContainerControl2.TabIndex = 0;
            this.splitContainerControl2.Text = "splitContainerControl2";
            // 
            // schedulerControl1
            // 
            this.schedulerControl1.ActiveViewType = DevExpress.XtraScheduler.SchedulerViewType.Timeline;
            this.schedulerControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.schedulerControl1.GroupType = DevExpress.XtraScheduler.SchedulerGroupType.Resource;
            this.schedulerControl1.Location = new System.Drawing.Point(0, 0);
            this.schedulerControl1.Name = "schedulerControl1";
            this.schedulerControl1.OptionsView.ResourceHeaders.ImageSize = new System.Drawing.Size(40, 40);
            this.schedulerControl1.OptionsView.ResourceHeaders.ImageSizeMode = DevExpress.XtraScheduler.HeaderImageSizeMode.StretchImage;
            this.schedulerControl1.Size = new System.Drawing.Size(1382, 733);
            this.schedulerControl1.Start = new System.DateTime(2008, 7, 13, 0, 0, 0, 0);
            this.schedulerControl1.Storage = this.schedulerStorage1;
            this.schedulerControl1.TabIndex = 0;
            this.schedulerControl1.Text = "schedulerControl1";
            this.schedulerControl1.ToolTipController = this.ToolTipController_F;
            this.schedulerControl1.Views.DayView.ResourcesPerPage = 4;
            this.schedulerControl1.Views.DayView.TimeRulers.Add(timeRuler1);
            this.schedulerControl1.Views.MonthView.ResourcesPerPage = 4;
            this.schedulerControl1.Views.TimelineView.ResourcesPerPage = 4;
            this.schedulerControl1.Views.WeekView.ResourcesPerPage = 4;
            this.schedulerControl1.Views.WorkWeekView.ResourcesPerPage = 4;
            this.schedulerControl1.Views.WorkWeekView.TimeRulers.Add(timeRuler2);
            this.schedulerControl1.ActiveViewChanged += new System.EventHandler(this.schedulerControl1_ActiveViewChanged);
            this.schedulerControl1.InitNewAppointment += new DevExpress.XtraScheduler.AppointmentEventHandler(this.schedulerControl1_InitNewAppointment);
            this.schedulerControl1.EditAppointmentFormShowing += new DevExpress.XtraScheduler.AppointmentFormEventHandler(this.schedulerControl1_EditAppointmentFormShowing);
            // 
            // schedulerStorage1
            // 
            this.schedulerStorage1.Appointments.CustomFieldMappings.Add(new DevExpress.XtraScheduler.AppointmentCustomFieldMapping("AGBRec2", "AGB_TAG", DevExpress.XtraScheduler.FieldValueType.Integer));
            this.schedulerStorage1.Appointments.CustomFieldMappings.Add(new DevExpress.XtraScheduler.AppointmentCustomFieldMapping("AGBDataLimite", "AGBDataLimite"));
            this.schedulerStorage1.Appointments.DataSource = this.AGendaBasebindingSource;
            this.schedulerStorage1.Appointments.Labels.Add(System.Drawing.Color.Gray, "Vencido", "&Vencido");
            this.schedulerStorage1.Appointments.Labels.Add(System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(194)))), ((int)(((byte)(190))))), "Limite", "&Limite");
            this.schedulerStorage1.Appointments.Labels.Add(System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(128))))), "Alerta", "&Alerta");
            this.schedulerStorage1.Appointments.Labels.Add(System.Drawing.Color.FromArgb(((int)(((byte)(193)))), ((int)(((byte)(244)))), ((int)(((byte)(156))))), "Agendado", "A&gendado");
            this.schedulerStorage1.Appointments.Mappings.AllDay = "AGBDiaTodo";
            this.schedulerStorage1.Appointments.Mappings.Description = "AGBMemo";
            this.schedulerStorage1.Appointments.Mappings.End = "AGBDataTermino";
            this.schedulerStorage1.Appointments.Mappings.Label = "calAGBStatus";
            this.schedulerStorage1.Appointments.Mappings.Location = "AGBSubDescricao";
            this.schedulerStorage1.Appointments.Mappings.RecurrenceInfo = "AGBRecorrencia";
            this.schedulerStorage1.Appointments.Mappings.ReminderInfo = "AGBLembrete";
            this.schedulerStorage1.Appointments.Mappings.ResourceId = "AGB_USU";
            this.schedulerStorage1.Appointments.Mappings.Start = "AGBDataInicio";
            this.schedulerStorage1.Appointments.Mappings.Subject = "AGBDescricao";
            this.schedulerStorage1.Appointments.Mappings.Type = "AGBTipo";
            this.schedulerStorage1.Appointments.Statuses.Add(DevExpress.XtraScheduler.AppointmentStatusType.Free, "Vencido", "&Vencido", System.Drawing.Color.Black);
            this.schedulerStorage1.Appointments.Statuses.Add(DevExpress.XtraScheduler.AppointmentStatusType.Tentative, "Limite", "&Limite", System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(128))))));
            this.schedulerStorage1.Appointments.Statuses.Add(DevExpress.XtraScheduler.AppointmentStatusType.Busy, "Alerta", "&Alerta", System.Drawing.Color.Yellow);
            this.schedulerStorage1.Appointments.Statuses.Add(DevExpress.XtraScheduler.AppointmentStatusType.OutOfOffice, "Agendado", "A&gendado", System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(255)))), ((int)(((byte)(128))))));
            this.schedulerStorage1.Resources.DataSource = this.UsuariosbindingSource;
            this.schedulerStorage1.Resources.Mappings.Caption = "USUNome";
            this.schedulerStorage1.Resources.Mappings.Id = "USU";
            this.schedulerStorage1.Resources.Mappings.Image = "USUFoto";
            this.schedulerStorage1.AppointmentsInserted += new DevExpress.XtraScheduler.PersistentObjectsEventHandler(this.schedulerStorage1_AppointmentsInserted);
            this.schedulerStorage1.AppointmentsChanged += new DevExpress.XtraScheduler.PersistentObjectsEventHandler(this.schedulerStorage1_AppointmentsChanged);
            this.schedulerStorage1.AppointmentsDeleted += new DevExpress.XtraScheduler.PersistentObjectsEventHandler(this.schedulerStorage1_AppointmentsDeleted);
            // 
            // AGendaBasebindingSource
            // 
            this.AGendaBasebindingSource.DataMember = "AGendaBase";
            this.AGendaBasebindingSource.DataSource = typeof(Calendario.Agenda.dAGendaBase);
            // 
            // UsuariosbindingSource
            // 
            this.UsuariosbindingSource.DataMember = "USUarios";
            this.UsuariosbindingSource.DataSource = typeof(Framework.datasets.dUSUarios);
            // 
            // dateNavigator1
            // 
            this.dateNavigator1.CalendarTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.dateNavigator1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dateNavigator1.FirstDayOfWeek = System.DayOfWeek.Sunday;
            this.dateNavigator1.Location = new System.Drawing.Point(0, 0);
            this.dateNavigator1.Name = "dateNavigator1";
            this.dateNavigator1.SchedulerControl = this.schedulerControl1;
            this.dateNavigator1.Size = new System.Drawing.Size(158, 733);
            this.dateNavigator1.TabIndex = 0;
            // 
            // resourcesComboBoxControl1
            // 
            this.resourcesComboBoxControl1.Location = new System.Drawing.Point(188, 31);
            this.resourcesComboBoxControl1.Name = "resourcesComboBoxControl1";
            this.resourcesComboBoxControl1.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.resourcesComboBoxControl1.SchedulerControl = this.schedulerControl1;
            this.resourcesComboBoxControl1.Size = new System.Drawing.Size(169, 20);
            this.resourcesComboBoxControl1.TabIndex = 40;
            // 
            // cbGrouping
            // 
            this.cbGrouping.EditValue = "Usuario";
            this.cbGrouping.Location = new System.Drawing.Point(64, 31);
            this.cbGrouping.Name = "cbGrouping";
            this.cbGrouping.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.cbGrouping.Properties.Items.AddRange(new DevExpress.XtraEditors.Controls.ImageComboBoxItem[] {
            new DevExpress.XtraEditors.Controls.ImageComboBoxItem("Nada", DevExpress.XtraScheduler.SchedulerGroupType.None, -1),
            new DevExpress.XtraEditors.Controls.ImageComboBoxItem("Data / Usu�rio", "DataUsuario", -1),
            new DevExpress.XtraEditors.Controls.ImageComboBoxItem("Data / Tipo", "DataTipo", -1),
            new DevExpress.XtraEditors.Controls.ImageComboBoxItem("Usuarios", "Usuario", -1),
            new DevExpress.XtraEditors.Controls.ImageComboBoxItem("Tipos", "Tipo", -1)});
            this.cbGrouping.Size = new System.Drawing.Size(118, 20);
            this.cbGrouping.TabIndex = 39;
            this.cbGrouping.SelectedIndexChanged += new System.EventHandler(this.cbGrouping_SelectedIndexChanged);
            // 
            // lblGroup
            // 
            this.lblGroup.Location = new System.Drawing.Point(4, 37);
            this.lblGroup.Name = "lblGroup";
            this.lblGroup.Size = new System.Drawing.Size(57, 16);
            this.lblGroup.TabIndex = 38;
            this.lblGroup.Text = "Agrupar:";
            // 
            // cbView
            // 
            this.cbView.EditValue = DevExpress.XtraScheduler.SchedulerViewType.Timeline;
            this.cbView.Location = new System.Drawing.Point(64, 10);
            this.cbView.Name = "cbView";
            this.cbView.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.cbView.Properties.Items.AddRange(new DevExpress.XtraEditors.Controls.ImageComboBoxItem[] {
            new DevExpress.XtraEditors.Controls.ImageComboBoxItem("Dia", DevExpress.XtraScheduler.SchedulerViewType.Day, -1),
            new DevExpress.XtraEditors.Controls.ImageComboBoxItem("Semana �til", DevExpress.XtraScheduler.SchedulerViewType.WorkWeek, -1),
            new DevExpress.XtraEditors.Controls.ImageComboBoxItem("Semana", DevExpress.XtraScheduler.SchedulerViewType.Week, -1),
            new DevExpress.XtraEditors.Controls.ImageComboBoxItem("M�s", DevExpress.XtraScheduler.SchedulerViewType.Month, -1),
            new DevExpress.XtraEditors.Controls.ImageComboBoxItem("Linha do tempo", DevExpress.XtraScheduler.SchedulerViewType.Timeline, -1)});
            this.cbView.Size = new System.Drawing.Size(118, 20);
            this.cbView.TabIndex = 37;
            this.cbView.SelectedIndexChanged += new System.EventHandler(this.cbView_SelectedIndexChanged);
            // 
            // lblView
            // 
            this.lblView.Location = new System.Drawing.Point(4, 14);
            this.lblView.Name = "lblView";
            this.lblView.Size = new System.Drawing.Size(40, 16);
            this.lblView.TabIndex = 36;
            this.lblView.Text = "Mostrar:";
            // 
            // simpleButton1
            // 
            this.simpleButton1.Location = new System.Drawing.Point(374, 14);
            this.simpleButton1.Name = "simpleButton1";
            this.simpleButton1.Size = new System.Drawing.Size(75, 23);
            this.simpleButton1.TabIndex = 0;
            this.simpleButton1.Text = "Atualizar";
            this.simpleButton1.Click += new System.EventHandler(this.simpleButton1_Click);
            // 
            // TipoAGendamentobindingSource
            // 
            this.TipoAGendamentobindingSource.DataMember = "TipoAGendamento";
            this.TipoAGendamentobindingSource.DataSource = typeof(Calendario.Agenda.dAGendaBase);
            // 
            // cAgenda
            // 
            this.Controls.Add(this.splitContainerControl1);
            this.Doc = System.Windows.Forms.DockStyle.Fill;
            this.LookAndFeel.UseDefaultLookAndFeel = false;
            this.Name = "cAgenda";
            this.Size = new System.Drawing.Size(1545, 799);
            ((System.ComponentModel.ISupportInitialize)(this.StyleController_F)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainerControl1)).EndInit();
            this.splitContainerControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splitContainerControl2)).EndInit();
            this.splitContainerControl2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.schedulerControl1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.schedulerStorage1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.AGendaBasebindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.UsuariosbindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateNavigator1.CalendarTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateNavigator1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.resourcesComboBoxControl1.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbGrouping.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbView.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TipoAGendamentobindingSource)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraEditors.SplitContainerControl splitContainerControl2;
        private DevExpress.XtraScheduler.SchedulerControl schedulerControl1;
        private DevExpress.XtraScheduler.SchedulerStorage schedulerStorage1;
        private DevExpress.XtraScheduler.DateNavigator dateNavigator1;
        private System.Windows.Forms.BindingSource AGendaBasebindingSource;
        private System.Windows.Forms.BindingSource TipoAGendamentobindingSource;
        private DevExpress.XtraEditors.SimpleButton simpleButton1;
        /// <summary>
        /// 
        /// </summary>
        protected DevExpress.XtraEditors.SplitContainerControl splitContainerControl1;
        private DevExpress.XtraEditors.ImageComboBoxEdit cbGrouping;
        private System.Windows.Forms.Label lblGroup;
        private DevExpress.XtraEditors.ImageComboBoxEdit cbView;
        private System.Windows.Forms.Label lblView;
        private System.Windows.Forms.BindingSource UsuariosbindingSource;
        private DevExpress.XtraScheduler.UI.ResourcesComboBoxControl resourcesComboBoxControl1;
    }
}
