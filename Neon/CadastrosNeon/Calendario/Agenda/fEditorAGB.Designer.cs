namespace Calendario.Agenda
{
    partial class fEditorAGB
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.btnCancel = new DevExpress.XtraEditors.SimpleButton();
            this.btnOK = new DevExpress.XtraEditors.SimpleButton();
            this.uSUARIOSBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.tipoAGendamentoBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.cEditorAGB1 = new Calendario.Agenda.cEditorAGB();
            this.simpleButton1 = new DevExpress.XtraEditors.SimpleButton();
            ((System.ComponentModel.ISupportInitialize)(this.uSUARIOSBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tipoAGendamentoBindingSource)).BeginInit();
            this.SuspendLayout();
            // 
            // btnCancel
            // 
            this.btnCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.btnCancel.Location = new System.Drawing.Point(93, 371);
            this.btnCancel.Name = "btnCancel";
            this.btnCancel.Size = new System.Drawing.Size(75, 27);
            this.btnCancel.TabIndex = 13;
            this.btnCancel.Text = "Cancel";
            // 
            // btnOK
            // 
            this.btnOK.DialogResult = System.Windows.Forms.DialogResult.OK;
            this.btnOK.Location = new System.Drawing.Point(12, 371);
            this.btnOK.Name = "btnOK";
            this.btnOK.Size = new System.Drawing.Size(75, 27);
            this.btnOK.TabIndex = 12;
            this.btnOK.Text = "OK";
            this.btnOK.Click += new System.EventHandler(this.btnOK_Click);
            // 
            // uSUARIOSBindingSource
            // 
            this.uSUARIOSBindingSource.DataMember = "USUarios";
            this.uSUARIOSBindingSource.DataSource = typeof(Framework.datasets.dUSUarios);
            // 
            // tipoAGendamentoBindingSource
            // 
            this.tipoAGendamentoBindingSource.DataMember = "TipoAGendamento";
            this.tipoAGendamentoBindingSource.DataSource = typeof(Calendario.Agenda.dAGendaBase);
            // 
            // cEditorAGB1
            // 
            this.cEditorAGB1.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.cEditorAGB1.Appearance.Options.UseBackColor = true;
            this.cEditorAGB1.CaixaAltaGeral = true;
            this.cEditorAGB1.Doc = System.Windows.Forms.DockStyle.Top;
            this.cEditorAGB1.Dock = System.Windows.Forms.DockStyle.Top;
            this.cEditorAGB1.Estado = CompontesBasicos.EstadosDosComponentes.Registrado;
            
            this.cEditorAGB1.Location = new System.Drawing.Point(0, 0);
            this.cEditorAGB1.LookAndFeel.UseDefaultLookAndFeel = false;
            this.cEditorAGB1.Name = "cEditorAGB1";
            this.cEditorAGB1.Size = new System.Drawing.Size(535, 365);
            this.cEditorAGB1.somenteleitura = false;
            this.cEditorAGB1.TabIndex = 66;
            this.cEditorAGB1.Titulo = null;
            // 
            // simpleButton1
            // 
            this.simpleButton1.Location = new System.Drawing.Point(379, 371);
            this.simpleButton1.Name = "simpleButton1";
            this.simpleButton1.Size = new System.Drawing.Size(144, 27);
            this.simpleButton1.TabIndex = 67;
            this.simpleButton1.Text = "S�rie";
            this.simpleButton1.Click += new System.EventHandler(this.simpleButton1_Click);
            // 
            // fEditorAGB
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.ClientSize = new System.Drawing.Size(535, 405);
            this.Controls.Add(this.simpleButton1);
            this.Controls.Add(this.cEditorAGB1);
            this.Controls.Add(this.btnCancel);
            this.Controls.Add(this.btnOK);
            this.Name = "fEditorAGB";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            ((System.ComponentModel.ISupportInitialize)(this.uSUARIOSBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tipoAGendamentoBindingSource)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraEditors.SimpleButton btnCancel;
        private DevExpress.XtraEditors.SimpleButton btnOK;
        private System.Windows.Forms.BindingSource uSUARIOSBindingSource;
        private System.Windows.Forms.BindingSource tipoAGendamentoBindingSource;
        private cEditorAGB cEditorAGB1;
        private DevExpress.XtraEditors.SimpleButton simpleButton1;
    }
}
