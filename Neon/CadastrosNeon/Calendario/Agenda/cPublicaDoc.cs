
namespace Calendario.Agenda
{
    /// <summary>
    /// 
    /// </summary>
    public partial class cPublicaDoc : CompontesBasicos.ComponenteBaseDialog
    {
        /// <summary>
        /// 
        /// </summary>
        public cPublicaDoc()
        {
            InitializeComponent();
        }

        private void XArquivo_ButtonClick(object sender, DevExpress.XtraEditors.Controls.ButtonPressedEventArgs e)
        {
            if (openFileDialog1.ShowDialog() == System.Windows.Forms.DialogResult.OK)
                XArquivo.Text = openFileDialog1.FileName;
        }
    }
}
