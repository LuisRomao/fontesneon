using System;
using System.Data;
using System.Windows.Forms;
using DevExpress.XtraScheduler;

namespace Calendario.Agenda
{
    /// <summary>
    /// 
    /// </summary>
    public partial class cAgenda : CompontesBasicos.ComponenteBase
    {
        /// <summary>
        /// 
        /// </summary>
        public cAgenda()
        {
            InitializeComponent();
            if (!DesignMode)
            {
                AGendaBasebindingSource.DataSource = Agenda.dAGendaBase.dAGendaBaseSt;
                TipoAGendamentobindingSource.DataSource = Agenda.dAGendaBase.dAGendaBaseSt;
                Agenda.dAGendaBase.dAGendaBaseSt.FillAGendaBase(-1);
                //UsuariosbindingSource.DataSource = Framework.datasets.dUSUarios.dUSUariosSt;
            };
        }

        private void simpleButton1_Click(object sender, EventArgs e)
        {
            Atualizar();
        }

        /// <summary>
        /// M�todo virtual para re-carregar os dados
        /// </summary>
        protected virtual void Atualizar() {
            Agenda.dAGendaBase.dAGendaBaseSt.FillAGendaBase(-1);
        }

        private void MapearRecurso(bool Usuario) {
            if (Usuario)
            {
                schedulerStorage1.Resources.DataSource = UsuariosbindingSource;
                schedulerStorage1.Resources.Mappings.Id = "USU";
                schedulerStorage1.Resources.Mappings.Caption = "USUNome";
                schedulerStorage1.Resources.Mappings.Image = "USUFoto";
                schedulerStorage1.Appointments.Mappings.ResourceId = "AGB_USU";
                schedulerStorage1.Appointments.CustomFieldMappings["AGBRec2"].Member = "AGB_TAG";
                schedulerControl1.GroupType = SchedulerGroupType.Resource;
            }
            else 
            {
                schedulerStorage1.Resources.DataSource = TipoAGendamentobindingSource;
                schedulerStorage1.Resources.Mappings.Id = "TAG";
                schedulerStorage1.Resources.Mappings.Caption = "TAGTitulo";
                schedulerStorage1.Resources.Mappings.Image = "";
                schedulerStorage1.Appointments.Mappings.ResourceId = "AGB_TAG";
                schedulerStorage1.Appointments.CustomFieldMappings["AGBRec2"].Member = "AGB_USU";
                schedulerControl1.GroupType = SchedulerGroupType.Resource;
            }
        }

        private TiposAgrupamento Tipar(object Entrada)
        {
            if (Entrada == null)
                return TiposAgrupamento.Nada;
            try
            {
                object Retorno = Enum.Parse(typeof(TiposAgrupamento), Entrada.ToString());
                return (TiposAgrupamento)Retorno;
            }
            catch
            {
                return TiposAgrupamento.Nada;
            };
            
        }

        enum TiposAgrupamento {            
            Nada,DataUsuario,DataTipo,Usuario,Tipo 
        }

        private void cbGrouping_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (Block)
                return;
            Block = true;
            schedulerControl1.BeginUpdate();
            try
            {
                switch (Tipar(cbGrouping.EditValue))
                {
                    case TiposAgrupamento.Nada:
                        schedulerControl1.GroupType = SchedulerGroupType.None;
                        break;
                    case TiposAgrupamento.DataUsuario:
                        MapearRecurso(true);
                        schedulerControl1.GroupType = SchedulerGroupType.Date;
                        break;
                    case TiposAgrupamento.DataTipo:
                        MapearRecurso(false);
                        schedulerControl1.GroupType = SchedulerGroupType.Date;
                        break;
                    case TiposAgrupamento.Usuario:
                        MapearRecurso(true);
                        schedulerControl1.GroupType = SchedulerGroupType.Resource;
                        break;
                    case TiposAgrupamento.Tipo:
                        MapearRecurso(false);
                        schedulerControl1.GroupType = SchedulerGroupType.Resource;
                        break;
                    default:
                        break;
                };                
            }
            finally
            {
                schedulerControl1.EndUpdate();
                Block = false;
            }
        }

        private void cbView_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (Block)
                return;
            Block = true;
            schedulerControl1.BeginUpdate();
            try
            {
                schedulerControl1.ActiveViewType = (SchedulerViewType)cbView.EditValue;               
            }
            finally
            {
                schedulerControl1.EndUpdate();
                Block = false;
            }          
        }

        private bool Block = false;

        private void schedulerControl1_ActiveViewChanged(object sender, EventArgs e)
        {
            if (Block)
                return;
            Block = true;
            try
            {
                switch (schedulerControl1.ActiveViewType)
                {
                    case SchedulerViewType.Day:
                        cbView.SelectedIndex = 0;
                        break;
                    case SchedulerViewType.Month:
                        cbView.SelectedIndex = 3;
                        break;
                    case SchedulerViewType.Timeline:
                        cbView.SelectedIndex = 4;
                        break;
                    case SchedulerViewType.Week:
                        cbView.SelectedIndex = 2;
                        break;
                    case SchedulerViewType.WorkWeek:
                        cbView.SelectedIndex = 1;
                        break;
                    default:
                        break;
                }
            }
            finally
            {
                Block = false;
            };
        }

        private void schedulerStorage1_AppointmentsChanged(object sender, PersistentObjectsEventArgs e)
        {
            GravarDeVoltaNoBanco();
            foreach (Appointment apt in e.Objects)
            {
                DataRowView DRV = (DataRowView)apt.GetRow(schedulerStorage1);
                if ((DRV != null) && (DRV.Row != null))
                {
                    dAGendaBase.AGendaBaseRow AGBrow = (dAGendaBase.AGendaBaseRow)DRV.Row;
                    apt.LabelId = AGBrow.calAGBStatus;
                    /*
                    if (apt.ResourceId != null)
                        if (schedulerStorage1.Resources.Mappings.Id == "USU")
                            AGBrow.AGB_USU = (int)apt.ResourceId;
                        else
                            AGBrow.AGB_TAG = (int)apt.ResourceId;
                    */ 
                }
            }
            
            
        }

        private void schedulerStorage1_AppointmentsDeleted(object sender, PersistentObjectsEventArgs e)
        {
            GravarDeVoltaNoBanco();
        }

        private void schedulerStorage1_AppointmentsInserted(object sender, PersistentObjectsEventArgs e)
        {
            GravarDeVoltaNoBanco();
        }

        /// <summary>
        /// 
        /// </summary>
        protected virtual void GravarDeVoltaNoBanco() {
            try
            {
                Agenda.dAGendaBase.dAGendaBaseSt.AGendaBaseTableAdapter.Update(Agenda.dAGendaBase.dAGendaBaseSt.AGendaBase);
                Agenda.dAGendaBase.dAGendaBaseSt.AGendaBase.AcceptChanges();
                schedulerControl1.Refresh();
            }
            catch {
                foreach (Agenda.dAGendaBase.AGendaBaseRow rowMOD in Agenda.dAGendaBase.dAGendaBaseSt.AGendaBase.GetChanges().Rows) {
                    try
                    {
                        Agenda.dAGendaBase.dAGendaBaseSt.AGendaBaseTableAdapter.Update(rowMOD);
                        rowMOD.AcceptChanges();
                    }
                    catch (Exception e){
                        string Conteudo = e.Message + "\r\n\r\n";
                        foreach (DataColumn DC in Agenda.dAGendaBase.dAGendaBaseSt.AGendaBase.Columns)
                        {
                            Conteudo += DC.Caption.PadRight(30) + " = " + rowMOD[DC.Caption].ToString() + "\r\n";
                            try
                            {
                                Conteudo += DC.Caption.PadRight(30) + " (orig) = " + rowMOD[DC.Caption, DataRowVersion.Original].ToString() + "\r\n";
                            }
                            catch 
                            {
                                Conteudo += DC.Caption.PadRight(30) + " (orig) = NULO\r\n";
                            }
                        }
                        MessageBox.Show(Conteudo);
                    }
                }
            }
        }

        

        private void schedulerControl1_EditAppointmentFormShowing(object sender, AppointmentFormEventArgs e)
        {
            SchedulerControl schedulerControl = (SchedulerControl)sender;
            Appointment apt = e.Appointment;
            bool openRecurrenceForm = apt.IsRecurring && schedulerControl.Storage.Appointments.IsNewAppointment(apt);

            //cEditorAGB Editor = new cEditorAGB();
            //Editor.Iniciar(schedulerControl, apt, openRecurrenceForm);
            //Editor.VirShowModulo(CompontesBasicos.EstadosDosComponentes.PopUp);

            fEditorAGB f = new fEditorAGB(schedulerControl, apt, openRecurrenceForm);            
            //f.LookAndFeel.ParentLookAndFeel = this.LookAndFeel.ParentLookAndFeel;
            e.DialogResult = f.ShowDialog();
            e.Handled = true;

            if (apt.Type == AppointmentType.Pattern && schedulerControl.SelectedAppointments.Contains(apt))
                schedulerControl.SelectedAppointments.Remove(apt);

            DataRowView DRV = (DataRowView)apt.GetRow(schedulerStorage1);
            if ((DRV != null) && (DRV.Row != null))
            {
                dAGendaBase.AGendaBaseRow AGBrow = (dAGendaBase.AGendaBaseRow)DRV.Row;
                //if (schedulerStorage1.Resources.Mappings.Id == "USU")
                //    apt.ResourceId = AGBrow.AGB_USU;
                //else
                //    apt.ResourceId = AGBrow.AGB_TAG;
            }

            schedulerControl.Refresh();
        }

        private void schedulerControl1_InitNewAppointment(object sender, AppointmentEventArgs e)
        {
            //e.Appointment.Description = "novo";
        }

        
    }
}

