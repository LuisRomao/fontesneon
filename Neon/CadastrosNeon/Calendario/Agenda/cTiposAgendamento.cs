
namespace Calendario.Agenda
{
    /// <summary>
    /// 
    /// </summary>
    public partial class cTiposAgendamento : CompontesBasicos.ComponenteGradeNavegador
    {
        /// <summary>
        /// 
        /// </summary>
        public cTiposAgendamento()
        {
            InitializeComponent();
            TipoAgendamentobindingSource.DataSource = dAGendaBase.dAGendaBaseSt;
            uSUariosBindingSource.DataSource = FrameworkProc.datasets.dUSUarios.dUSUariosSt;
            TableAdapterPrincipal = dAGendaBase.dAGendaBaseSt.TipoAGendamentoTableAdapter;
        }
    }
}
