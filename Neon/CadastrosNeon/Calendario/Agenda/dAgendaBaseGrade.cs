﻿using VirEnumeracoesNeon;

namespace Calendario.Agenda
{


    partial class dAgendaBaseGrade
    {
        private dAgendaBaseGradeTableAdapters.TipoAGendamentoTableAdapter tipoAGendamentoTableAdapter;
        /// <summary>
        /// TableAdapter padrão para tabela: TipoAGendamento
        /// </summary>
        public dAgendaBaseGradeTableAdapters.TipoAGendamentoTableAdapter TipoAGendamentoTableAdapter
        {
            get
            {
                if (tipoAGendamentoTableAdapter == null)
                {
                    tipoAGendamentoTableAdapter = new dAgendaBaseGradeTableAdapters.TipoAGendamentoTableAdapter();
                    tipoAGendamentoTableAdapter.TrocarStringDeConexao();
                };
                return tipoAGendamentoTableAdapter;
            }
        }

        private dAgendaBaseGradeTableAdapters.AGendaBaseTableAdapter aGendaBaseTableAdapter;
        /// <summary>
        /// TableAdapter padrão para tabela: AGendaBase
        /// </summary>
        public dAgendaBaseGradeTableAdapters.AGendaBaseTableAdapter AGendaBaseTableAdapter
        {
            get
            {
                if (aGendaBaseTableAdapter == null)
                {
                    aGendaBaseTableAdapter = new dAgendaBaseGradeTableAdapters.AGendaBaseTableAdapter();
                    aGendaBaseTableAdapter.TrocarStringDeConexao();
                };
                return aGendaBaseTableAdapter;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        public void AjustatodosStatus()
        {
            //int pkchecar = 0;
            //VirInput.Input.Execute("ok", out pkchecar);
            foreach (AGendaBaseRow row in AGendaBase)
            {
                //  if (row.AGB == pkchecar)
                //     pkchecar = 0;
                AjustaStatus(System.DateTime.Today, row);
            }
            AGendaBaseTableAdapter.Update(AGendaBase);
            //AGendaBase.AcceptChanges();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="DataRef"></param>
        /// <param name="row"></param>
        /// <returns></returns>
        public bool AjustaStatus(System.DateTime DataRef, AGendaBaseRow row)
        {

            TipoAGendamentoRow tiporow = TipoAGendamento.FindByTAG(row.AGB_TAG);
            System.DateTime DataAviso = row.AGBDataInicio.AddDays(-tiporow.TAGDiasUteis);
            if ((row["AGBStatus"] == System.DBNull.Value) || ((AGBStatus)row.AGBStatus != AGBStatus.Ok))
            {
                AGBStatus NovoStatus = AGBStatus.Ok;
                if (DataRef < DataAviso)
                    NovoStatus = AGBStatus.Agendado;

                else if (DataRef < row.AGBDataInicio)
                    NovoStatus = AGBStatus.Aviso;



                else NovoStatus = AGBStatus.Atraso;
                if ((int)NovoStatus != row.AGBStatus)
                {
                    row.AGBStatus = (int)NovoStatus;
                    return true;
                }
                else
                    return false;
            }
            else
                return false;
        }
    }
}
