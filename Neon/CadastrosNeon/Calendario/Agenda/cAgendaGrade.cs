using System;
using System.Collections.Generic;
using System.Drawing;
using System.Windows.Forms;
using Framework;
using FrameworkProc.datasets;
using VirEnumeracoesNeon;


namespace Calendario.Agenda
{
    /// <summary>
    /// 
    /// </summary>
    public partial class cAgendaGrade : Framework.ComponentesNeon.NavegadorCondominio
    {
        /// <summary>
        /// 
        /// </summary>
        public cAgendaGrade()
        {
            InitializeComponent();
            dAgendaBaseGrade.TipoAGendamentoTableAdapter.Fill(dAgendaBaseGrade.TipoAGendamento);
            //aGendaBaseBindingSource.DataSource = dAGendaBase.dAGendaBaseSt;
            uSUARIOSBindingSource.DataSource = dUSUarios.dUSUariosSt;
            //tipoAGendamentoBindingSource.DataSource = dAGendaBase.dAGendaBaseSt;
            cONDOMINIOSBindingSource.DataSource = FrameworkProc.datasets.dCondominiosAtivos.dCondominiosAtivosST;            
            Enumeracoes.VirEnumAGBStatus.CarregaEditorDaGrid(colAGBStatus);
            if (CompontesBasicos.FormPrincipalBase.FormPrincipal != null)
            {
                int Pendencias = (int)dUSUarios.dUSUariosSt.USUariosTableAdapter.ScalarQueryPendentes(Framework.usuarioLogado.UsuarioLogadoNeon.UsuarioLogadoSt.USU);
                if (Pendencias > 0)
                    MessageBox.Show(String.Format("ATEN��O: Existem {0} agendamentos pendentes em seu nome.", Pendencias));                      
            }                        
        }

        private void cAgendaGrade_CondominioAlterado(object sender, EventArgs e)
        {
            ultimoTAG = 0;
            ultimoGerente = -1;
            dAGendaBase.dAGendaBaseSt.AGendaBase.Clear();
            //RefreshDados();                        
        }

        /// <summary>
        /// 
        /// </summary>
        protected override void Alterar()
        {
            base.Alterar();
            if (ultimoTAG == 0)
                RefreshDados();
            else
                FiltroContas(ultimoTAG,ultimoGerente);
        }

        /// <summary>
        /// 
        /// </summary>
        public override void RefreshDados()
        {
            if (CON == -1)
                dAgendaBaseGrade.AGendaBaseTableAdapter.Fill(dAgendaBaseGrade.AGendaBase);
                //dAGendaBase.dAGendaBaseSt.AGendaBaseTableAdapter.Fill(dAGendaBase.dAGendaBaseSt.AGendaBase);
            else
                dAgendaBaseGrade.AGendaBaseTableAdapter.FillByCON(dAgendaBaseGrade.AGendaBase,CON);
            //dAGendaBase.dAGendaBaseSt.AGendaBaseTableAdapter.FillByCON(dAGendaBase.dAGendaBaseSt.AGendaBase, CON);
            dAgendaBaseGrade.AjustatodosStatus();
        }

        private void GridView_F_RowCellStyle(object sender, DevExpress.XtraGrid.Views.Grid.RowCellStyleEventArgs e)
        {
            if (e.CellValue == null)
                return;
            if (e.Column == colAGBStatus)
            {
                e.Appearance.BackColor = Enumeracoes.VirEnumAGBStatus.GetCor(e.CellValue);
                if (e.Appearance.BackColor == Color.Black)
                    e.Appearance.ForeColor = Color.White;
                else
                    e.Appearance.ForeColor = Color.Black;
                
            }
            else if (e.Column == colAGB_TAG)
            {
                dAGendaBase.TipoAGendamentoRow rowTag = dAGendaBase.dAGendaBaseSt.TipoAGendamento.FindByTAG((int)e.CellValue);
                e.Appearance.BackColor = Color.FromArgb(rowTag.TAGCor);
                e.Appearance.ForeColor = Color.Black;
            }
        }

        private void simpleButton1_Click(object sender, EventArgs e)
        {
            RefreshDados();
        }

       

        

        private void simpleButton2_Click(object sender, EventArgs e)
        {
            dAgendaBaseGrade.AGendaBaseRow row = (dAgendaBaseGrade.AGendaBaseRow)GridView_F.GetFocusedDataRow();
            if (row != null)
            {
                cPublicaAGDOC Publicador = new cPublicaAGDOC();
                Publicador.Carregar(row.AGB);
                FrameworkProc.datasets.dCondominiosAtivos.CONDOMINIOSRow rowCON = FrameworkProc.datasets.dCondominiosAtivos.dCondominiosAtivosST.CONDOMINIOS.FindByCON(row.AGB_CON);
                dAGendaBase.TipoAGendamentoRow rowTAG = dAGendaBase.dAGendaBaseSt.TipoAGendamento.FindByTAG(row.AGB_TAG);
                Publicador.Titulo = string.Format("{0} - {1}",rowCON.CONCodigo,rowTAG.TAGTitulo);
                Publicador.VirShowModulo(CompontesBasicos.EstadosDosComponentes.JanelasAtivas);
            }
        }

        private int ultimoTAG = 0;

        private int ultimoGerente = -1;

        /// <summary>
        /// Filtro para ser chamado do mapa geral
        /// </summary>
        /// <param name="TAG"></param>
        /// <param name="Gerente"></param>
        public void FiltroContas(int TAG,int Gerente)
        {
            CON = -1;
            ultimoTAG = TAG;
            ultimoGerente = Gerente;
            dAGendaBase.dAGendaBaseSt.AGendaBaseTableAdapter.FillByTAG(dAGendaBase.dAGendaBaseSt.AGendaBase,TAG);
            if (Gerente == -1)
                GridView_F.ActiveFilterString = string.Format("[AGBStatus] = {0}", (int)AGBStatus.Atraso);
            else
                GridView_F.ActiveFilterString = string.Format("[AGBStatus] = {0} and [Gerente] = {1}", (int)AGBStatus.Atraso, Gerente);
        }

        private void Linques_ButtonClick(object sender, DevExpress.XtraEditors.Controls.ButtonPressedEventArgs e)
        {
            dAGendaBase.AGendaBaseRow row = (dAGendaBase.AGendaBaseRow)GridView_F.GetFocusedDataRow();
            if (row != null)
            {
                int? PGFnull;
                if ((TiposTAG)row.AGB_TAG == TiposTAG.Contratos)
                    PGFnull = dAGendaBase.dAGendaBaseSt.AGendaBaseTableAdapter.PegaPGFContrato(row.AGB);
                else                    
                    PGFnull = (int?)dAGendaBase.dAGendaBaseSt.AGendaBaseTableAdapter.PegaPGF(row.AGB);
                if (PGFnull.HasValue)
                    Framework.Integrador.Integrador.AbreComponente(Framework.Integrador.TiposIntegra.Periodicos, PGFnull.Value, false, false);
            }
        }

        

        private void GridView_F_CustomRowCellEdit(object sender, DevExpress.XtraGrid.Views.Grid.CustomRowCellEditEventArgs e)
        {
            if (e.Column.Caption == "Botoes")
            {
                dAgendaBaseGrade.AGendaBaseRow row = (dAgendaBaseGrade.AGendaBaseRow)GridView_F.GetDataRow(e.RowHandle);
                if (row == null)
                    return;
                TiposTAG TipoTAG = (TiposTAG)row.AGB_TAG;
                List<TiposTAG> ControladosNoPeriodico = new List<TiposTAG>();
                ControladosNoPeriodico.Add(TiposTAG.BOLETOS_CONTRATOS);
                ControladosNoPeriodico.Add(TiposTAG.Debito_Automatico);
                ControladosNoPeriodico.Add(TiposTAG.Contratos);
                if (ControladosNoPeriodico.Contains(TipoTAG))                    
                    e.RepositoryItem = Linques;
                else
                    e.RepositoryItem = null;
            }            
        }

        private void cAgendaGrade_Load(object sender, EventArgs e)
        {
            GridView_F.OptionsBehavior.ReadOnly = true;
            GridView_F.OptionsBehavior.Editable = true;
            colGerente.OptionsColumn.AllowEdit = false;
            colAGBStatus.OptionsColumn.AllowEdit = false;
            colAGB_CON.OptionsColumn.AllowEdit = false;
            colAGB_TAG.OptionsColumn.AllowEdit = false;
            colAGB_USU.OptionsColumn.AllowEdit = false;
        }

        private void repositoryItemButtonEdit1_ButtonClick(object sender, DevExpress.XtraEditors.Controls.ButtonPressedEventArgs e)
        {
            Alterar();
        }

        /// <summary>
        /// 
        /// </summary>
        public int teste;

        private void simpleButton3_Click(object sender, EventArgs e)
        {
            foreach (dAgendaBaseGrade.AGendaBaseRow row in dAgendaBaseGrade.AGendaBase)
            {
                AGBStatus Status = (AGBStatus)row.AGBStatus;
                switch (row.AGB_TAG)
                {
                    case 5:
                        if (Status == AGBStatus.Atraso)
                        {
                            int? PGFnull = (int?)dAGendaBase.dAGendaBaseSt.AGendaBaseTableAdapter.PegaPGF(row.AGB);                           
                            if (PGFnull.HasValue)
                            {
                                //if (PGFnull.Value == 2110)
                                //    teste = 1;
                                AbstratosNeon.ABS_PagamentoPeriodico Periodico = AbstratosNeon.ABS_PagamentoPeriodico.GetPagamentoPeriodico(PGFnull.Value);
                                Periodico.CadastrarProximo();
                            }
                        }
                        break;
                }
            }
            RefreshDados();
        }

        

        //private bool liberado = false;

        


    }
}
