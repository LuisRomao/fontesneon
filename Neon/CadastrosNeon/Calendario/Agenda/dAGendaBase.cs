﻿using VirEnumeracoesNeon;

namespace Calendario.Agenda
{

    /*
    /// <summary>
    /// 
    /// </summary>
    [Obsolete("Transferido para enumerações", true)]
    public enum AGBStatus
    {
        /// <summary>
        /// 
        /// </summary>        
        Agendado = 0,
        /// <summary>
        /// 
        /// </summary>
        Aviso = 1,
        /// <summary>
        /// 
        /// </summary>
        Atraso = 2,
        /// <summary>
        /// 
        /// </summary>
        Vencido = 3,
        /// <summary>
        /// 
        /// </summary>
        Ok = 4
    }
    */

    partial class dAGendaBase
    {
        /*
        [Obsolete("Transferido para enumerações", true)]
        private static dllVirEnum.VirEnum virEnumAGBStatus;

        /// <summary>
        /// VirEnum para AGBStatus
        /// </summary>
        [Obsolete("Transferido para enumerações", true)]
        public static dllVirEnum.VirEnum VirEnumAGBStatus
        {
            get
            {
                if (virEnumAGBStatus == null)
                {
                    virEnumAGBStatus = new dllVirEnum.VirEnum(typeof(AGBStatus));
                    virEnumAGBStatus.GravaNomes(AGBStatus.Agendado, "Agendado");
                    virEnumAGBStatus.GravaNomes(AGBStatus.Aviso, "Aviso");
                    virEnumAGBStatus.GravaNomes(AGBStatus.Vencido, "Vencido");
                    virEnumAGBStatus.GravaNomes(AGBStatus.Atraso, "Atraso");
                    virEnumAGBStatus.GravaNomes(AGBStatus.Ok, "Ok");
                    virEnumAGBStatus.GravaCor(AGBStatus.Agendado, System.Drawing.Color.White);
                    virEnumAGBStatus.GravaCor(AGBStatus.Aviso, System.Drawing.Color.Yellow);
                    virEnumAGBStatus.GravaCor(AGBStatus.Atraso, System.Drawing.Color.Red);
                    virEnumAGBStatus.GravaCor(AGBStatus.Vencido, System.Drawing.Color.Black);
                    virEnumAGBStatus.GravaCor(AGBStatus.Ok, System.Drawing.Color.Silver);
                }
                return virEnumAGBStatus;
            }
        }
        */
        /// <summary>
        /// 
        /// </summary>
        public void AjustatodosStatus()
        {
            int pkchecar = 0;
            VirInput.Input.Execute("ok", out pkchecar);
            foreach (AGendaBaseRow row in AGendaBase)
            {
                if (row.AGB == pkchecar)
                    pkchecar = 0;
                AjustaStatus(System.DateTime.Today, row);
            }
            AGendaBaseTableAdapter.Update(AGendaBase);
            AGendaBase.AcceptChanges();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="DataRef"></param>
        /// <param name="row"></param>
        /// <returns></returns>
        public bool AjustaStatus(System.DateTime DataRef, dAGendaBase.AGendaBaseRow row)
        {

            TipoAGendamentoRow tiporow = TipoAGendamento.FindByTAG(row.AGB_TAG);
            System.DateTime DataAviso = row.AGBDataInicio.AddDays(-tiporow.TAGDiasUteis);
            if ((row["AGBStatus"] == System.DBNull.Value) || ((AGBStatus)row.AGBStatus != AGBStatus.Ok))
            {
                AGBStatus NovoStatus = AGBStatus.Ok;
                if (DataRef < DataAviso)
                    NovoStatus = AGBStatus.Agendado;

                else if (DataRef < row.AGBDataInicio)
                    NovoStatus = AGBStatus.Aviso;



                else NovoStatus = AGBStatus.Atraso;
                if ((int)NovoStatus != row.AGBStatus)
                {
                    row.AGBStatus = (int)NovoStatus;
                    return true;
                }
                else
                    return false;
            }
            else
                return false;
        }

        private void CalculaCampos(object sender, AGendaBaseRowChangeEvent e)
        {
            //System.IO.File.AppendAllText("c:\\log.txt", e.Row.AGBDescricao + " - " + e.Action.ToString() + "\r\n");
            switch (e.Action)
            {
                case System.Data.DataRowAction.Add:
                case System.Data.DataRowAction.Change:
                    break;
                default:
                    return;
            }

            //if (Trava)
            //    return;            
            //System.TimeSpan TSDelta = e.Row.AGBDataLimite - e.Row.AGBDataInicio;
            //int Delta = TSDelta.Days;
            //int NovoStatus;
            //if (Delta < 5)
            //    NovoStatus = 0;
            //else if (Delta < 6)
            //    NovoStatus = 1;
            //else if (Delta < 15)
            //    NovoStatus = 2;
            //else
            //    NovoStatus = 3;
            //if ((e.Row.IscalAGBStatusNull()) || (e.Row.calAGBStatus != NovoStatus))
            // {
            //   e.Row.calAGBStatus = NovoStatus;
            //System.IO.File.AppendAllText("c:\\log.txt", e.Row.AGBDescricao + " - GRAVEI: "+NovoStatus.ToString()+"\r\n");
            //}

        }





        private static dAGendaBase _dAGendaBaseSt;

        /// <summary>
        /// 
        /// </summary>
        public string UltimoErro;

        /// <summary>
        /// dataset estático:dAGendaBase
        /// </summary>
        public static dAGendaBase dAGendaBaseSt
        {
            get
            {
                if (_dAGendaBaseSt == null)
                {
                    _dAGendaBaseSt = new dAGendaBase();

                    if (!_dAGendaBaseSt.DesignMode)
                    {
                        //try
                        //{
                        _dAGendaBaseSt.AGendaBase.AGendaBaseRowChanged += new AGendaBaseRowChangeEventHandler(_dAGendaBaseSt.CalculaCampos);
                        _dAGendaBaseSt.TipoAGendamentoTableAdapter.Fill(_dAGendaBaseSt.TipoAGendamento);
                        //}
                        //catch (System.Exception e) {
                        //    _dAGendaBaseSt.UltimoErro = e.Message;                            
                        //}
                    }
                }
                return _dAGendaBaseSt;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="TAG"></param>
        public void FillAGendaBase(int TAG)
        {
            if (!_dAGendaBaseSt.DesignMode)
            {
                if (TAG == -1)
                    _dAGendaBaseSt.AGendaBaseTableAdapter.Fill(dAGendaBaseSt.AGendaBase);
                else
                    _dAGendaBaseSt.AGendaBaseTableAdapter.FillByTAG(dAGendaBaseSt.AGendaBase, TAG);
            }
        }

        private dAGendaBaseTableAdapters.AGendaBaseTableAdapter aGendaBaseTableAdapter;

        /// <summary>
        /// TableAdapter padrão para tabela: AGendaBase
        /// </summary>
        public dAGendaBaseTableAdapters.AGendaBaseTableAdapter AGendaBaseTableAdapter
        {
            get
            {
                if (aGendaBaseTableAdapter == null)
                {
                    aGendaBaseTableAdapter = new dAGendaBaseTableAdapters.AGendaBaseTableAdapter();
                    aGendaBaseTableAdapter.TrocarStringDeConexao();
                };
                return aGendaBaseTableAdapter;
            }
        }

        private dAGendaBaseTableAdapters.TipoAGendamentoTableAdapter tipoAGendamentoTableAdapter;

        /// <summary>
        /// TableAdapter padrão para tabela: TipoAGendamento
        /// </summary>
        public dAGendaBaseTableAdapters.TipoAGendamentoTableAdapter TipoAGendamentoTableAdapter
        {
            get
            {
                if (tipoAGendamentoTableAdapter == null)
                {
                    tipoAGendamentoTableAdapter = new dAGendaBaseTableAdapters.TipoAGendamentoTableAdapter();
                    tipoAGendamentoTableAdapter.TrocarStringDeConexao();
                };
                return tipoAGendamentoTableAdapter;
            }
        }
    }
}
