using System;
using System.Windows.Forms;
using DevExpress.XtraScheduler;
using DevExpress.XtraScheduler.UI;

namespace Calendario.Agenda
{
    /// <summary>
    /// Componente para edi��o de apontamentos
    /// </summary>
    public partial class cEditorAGB : CompontesBasicos.ComponenteBase
    {
        private SchedulerControl control;
        private Appointment apt;
        private bool openRecurrenceForm = false;
        private int suspendUpdateCount;
        private ControladorCampos controller;

        /// <summary>
        /// Construtor
        /// </summary>
        public cEditorAGB()
        {
            SuspendUpdate();
            InitializeComponent();
            if (!DesignMode)
            {
                try
                {
                    tipoAGendamentoBindingSource.DataSource = Agenda.dAGendaBase.dAGendaBaseSt;
                    uSUARIOSBindingSource.DataSource = Framework.datasets.dUSUarios.dUSUariosSt;
                }
                catch { };
            };
            ResumeUpdate();            
        }

        /// <summary>
        /// Inicia os campos
        /// </summary>
        /// <param name="control">SchedulerControl</param>
        /// <param name="apt">Apontamento</param>
        /// <param name="openRecurrenceForm">Recursividade</param>
        public void Iniciar(SchedulerControl control, Appointment apt, bool openRecurrenceForm)
        {
            this.openRecurrenceForm = openRecurrenceForm;
            this.controller = new ControladorCampos(control, apt);
            this.apt = apt;
            this.control = control;
            UpdateForm();
        }

        private AppointmentStorage Appointments
        {
            get { return control.Storage.Appointments; }
        }

        private bool IsUpdateSuspended { get { return suspendUpdateCount > 0; } }

        private void SuspendUpdate()
        {
            suspendUpdateCount++;
        }
        private void ResumeUpdate()
        {
            if (suspendUpdateCount > 0)
                suspendUpdateCount--;
        }

        

        
        /// <summary>
        /// 
        /// </summary>
        public enum TipoRec 
        { 
            /// <summary>
            /// 
            /// </summary>
            Apontamento, 
            /// <summary>
            /// 
            /// </summary>
            Serie 
        };

        /// <summary>
        /// Passa os valores alterados para o controlador
        /// </summary>
        public void RecuperarValores(TipoRec TipoR)
        {
            // Required to check appointment's conflicts.
            if (!controller.IsConflictResolved())
                return;
            
            controller.Subject = txSubject.Text;
            controller.Location = txLocal.Text;
            if (USUlookUpEdit.EditValue != null)
                controller.USU = (int)USUlookUpEdit.EditValue;
            if (TAGlookUpEdit.EditValue != null)
                controller.TAG = (int)TAGlookUpEdit.EditValue;
            //controller.SetStatus(edStatus.Status);
            controller.SetLabel(edLabel.Label);
            controller.AllDay = this.checkAllDay.Checked;
            controller.Start = this.dtStart.DateTime.Date + this.timeStart.Time.TimeOfDay;
            controller.End = this.dtEnd.DateTime.Date + this.timeEnd.Time.TimeOfDay;
            controller.Description = memoEdit1.Text;
            //melhorar
            controller.AGBDataLimite = dtLimite.DateTime.Date;
            //controller.CustomName = txCustomName.Text;
            //controller.CustomStatus = txCustomStatus.Text;
            if ((TipoR == TipoRec.Apontamento) && (controller.EditedPattern != null))
            {
                control.Storage.Appointments.Add(controller.EditedAppointmentCopy.Copy());
                controller.DeleteAppointment();
            }
            else
                controller.ApplyChanges();
            // Save all changes made to the appointment edited in a form.
            
        }

        void UpdateForm()
        {
            SuspendUpdate();
            try
            {
                txSubject.Text = controller.Subject;
                txLocal.Text = controller.Location;
                USUlookUpEdit.EditValue = controller.USU;
                TAGlookUpEdit.EditValue = controller.TAG;
                //edStatus.Status = Appointments.Statuses[controller.StatusId];

                edLabel.Label = Appointments.Labels[controller.LabelId];

                dtStart.DateTime = controller.Start.Date;
                dtEnd.DateTime = controller.End.Date;

                timeStart.Time = DateTime.MinValue.AddTicks(controller.Start.TimeOfDay.Ticks);
                timeEnd.Time = DateTime.MinValue.AddTicks(controller.End.TimeOfDay.Ticks);
                checkAllDay.Checked = controller.AllDay;
                memoEdit1.Text = controller.Description;
                //edStatus.Storage = control.Storage;

                edLabel.Storage = control.Storage;
                dtLimite.DateTime = controller.AGBDataLimite;
                if (dtLimite.DateTime == DateTime.MinValue)
                    dtLimite.DateTime = dtEnd.DateTime;
                //txCustomName.Text = controller.CustomName;
                //txCustomStatus.Text = controller.CustomStatus;
            }
            finally
            {
                ResumeUpdate();
            }
            UpdateIntervalControls();
        }

        private void MyAppointmentEditForm_Activated(object sender, System.EventArgs e)
        {
            // Required to show the recurrence form.
            if (openRecurrenceForm)
            {
                openRecurrenceForm = false;
                //OnRecurrenceButton();
            }
        }

        private void dtStart_EditValueChanged(object sender, System.EventArgs e)
        {
            if (!IsUpdateSuspended)
            {
                controller.Start = dtStart.DateTime.Date + timeStart.Time.TimeOfDay;
            }
            UpdateIntervalControls();
        }
        /// <summary>
        /// 
        /// </summary>
        protected virtual void UpdateIntervalControls()
        {
            if (IsUpdateSuspended)
                return;

            SuspendUpdate();
            try
            {
                dtStart.EditValue = controller.Start.Date;
                dtEnd.EditValue = controller.End.Date;
                timeStart.EditValue = controller.Start.TimeOfDay;
                timeEnd.EditValue = controller.End.TimeOfDay;
                timeStart.Enabled = !controller.AllDay;
                timeEnd.Enabled = !controller.AllDay;

            }
            finally
            {
                ResumeUpdate();
            }
        }
        private void timeStart_EditValueChanged(object sender, System.EventArgs e)
        {
            if (!IsUpdateSuspended)
            {
                controller.Start = dtStart.DateTime.Date + timeStart.Time.TimeOfDay;
            }
            UpdateIntervalControls();
        }
        private void timeEnd_EditValueChanged(object sender, System.EventArgs e)
        {
            if (IsUpdateSuspended) return;
            if (IsIntervalValid())
            {
                //controller.End = dtEnd.DateTime + timeEnd.Time.TimeOfDay;
            }
            else
            {
                //timeEnd.EditValue = controller.End.TimeOfDay;
            }
        }
        private void dtEnd_EditValueChanged(object sender, System.EventArgs e)
        {
            if (IsUpdateSuspended) return;
            if (IsIntervalValid())
            {
                //controller.End = dtEnd.DateTime + timeEnd.Time.TimeOfDay;
            }
            else
            {
                //dtEnd.EditValue = controller.End.Date;
            }
        }
        bool IsIntervalValid()
        {
            DateTime start = dtStart.DateTime + timeStart.Time.TimeOfDay;
            DateTime end = dtEnd.DateTime + timeEnd.Time.TimeOfDay;
            return end >= start;

        }

        private void checkAllDay_CheckedChanged(object sender, System.EventArgs e)
        {
            controller.AllDay = this.checkAllDay.Checked;
            if (!IsUpdateSuspended)
                UpdateAppointmentStatus();

            UpdateIntervalControls();
        }
        void UpdateAppointmentStatus()
        {
            //AppointmentStatus currentStatus = edStatus.Status;
            //AppointmentStatus newStatus = controller.UpdateAppointmentStatus(currentStatus);
            //if (newStatus != currentStatus)
            //    edStatus.Status = newStatus;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public DialogResult Serie()
        {
            if (!control.SupportsRecurrence)
                return DialogResult.Cancel;

            Appointment editedAptCopy = controller.EditedAppointmentCopy;
            Appointment editedPattern = controller.EditedPattern;
            Appointment patternCopy = controller.PrepareToRecurrenceEdit();
            AppointmentRecurrenceForm dlg = new AppointmentRecurrenceForm(patternCopy, control.OptionsView.FirstDayOfWeek, controller);
            dlg.LookAndFeel.ParentLookAndFeel = this.LookAndFeel.ParentLookAndFeel;

            DialogResult result = dlg.ShowDialog(this);
            dlg.Dispose();
            if (result == DialogResult.Abort)
            {
                controller.RemoveRecurrence();
                return DialogResult.No;
            }
            else
                if (result == DialogResult.OK)
                {
                    controller.ApplyRecurrence(patternCopy);
                    if (controller.EditedAppointmentCopy != editedAptCopy)
                        UpdateForm();
                    UpdateIntervalControls();
                    RecuperarValores(TipoRec.Serie);
                    return DialogResult.OK;
                }
                else
                    return DialogResult.Cancel;
            
        }

        private void button1_Click(object sender, EventArgs e)
        {
            //VirMSSQL.TableAdapter.STTableAdapter.BuscaEscalar
        }

        

    }
}

