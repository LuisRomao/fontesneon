using System;
using System.IO;
using System.Windows.Forms;

namespace Calendario.Agenda
{
    /// <summary>
    /// 
    /// </summary>
    public partial class cPublicaAGDOC : CompontesBasicos.ComponenteCamposBase
    {
        /// <summary>
        /// 
        /// </summary>
        public cPublicaAGDOC()
        {
            InitializeComponent();
            uSUariosBindingSource.DataSource = FrameworkProc.datasets.dUSUarios.dUSUariosSt;
            cONDOMINIOSBindingSource.DataSource = FrameworkProc.datasets.dCondominiosAtivos.dCondominiosAtivosST;
        }

        private void simpleButton1_Click(object sender, EventArgs e)
        {
            cPublicaDoc Editor = new cPublicaDoc();

            if (Editor.VirShowModulo(CompontesBasicos.EstadosDosComponentes.PopUp) == DialogResult.OK)
            {
                if ((Editor.XArquivo.Text == "") || (Editor.XDescricao.Text == ""))
                    return;
                Publica(Editor.XArquivo.Text, Editor.XDescricao.Text);
                CarregaLista();
            }
        }

        private dAGendaBase dAGBLocal;

        private dAGendaBase.AGendaBaseRow LinhaMae;

        private void CarregaLista()
        {
            try
            {
                aGendaDocBindingSource.DataSource = WS.ListaAGB(Criptografa(), LinhaMae.AGB); ;

            }
            catch (Exception e)
            {
                aGendaDocBindingSource.DataSource = null;
                MessageBox.Show(String.Format("{0} - {1}", e.GetType(), e.Message));
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="AGB"></param>
        public void Carregar(int AGB)
        {
            dAGBLocal = new dAGendaBase();
            dAGBLocal.AGendaBaseTableAdapter.FillByAGB(dAGBLocal.AGendaBase,AGB);
            LinhaMae = dAGBLocal.AGendaBase[0];
            CarregaLista();
        }

        private WSPublica3.WSPublica3 _WS;

        /// <summary>
        /// 
        /// </summary>
        public WSPublica3.WSPublica3 WS
        {
            get
            {
                if (_WS == null)
                {
                    _WS = new WSPublica3.WSPublica3();
                    if (CompontesBasicosProc.FuncoesBasicasProc.EstaEmProducao)
                    {
                        WS.Proxy = Framework.usuarioLogado.UsuarioLogadoNeon.UsuarioLogadoSt.Proxy;
                        WS.Url = @"https://ssl493.websiteseguro.com/neonimoveis/neon23/Neononline/WSPublica3.asmx";
                    }
                };
                return _WS;
            }
        }

        private static int staticR;

        private byte[] Criptografa()
        {
            string Aberto = string.Format("{0:yyyyMMddHHmmss}{1:000}{2:00000000}<-*W*->", DateTime.Now, Framework.DSCentral.EMP, staticR++);
            return VirCrip.VirCripWS.Crip(Aberto);
        }

        private void Publica(string Arquivo, string Descricao)
        {
            byte[] bdata;
            using (FileStream fStream = new FileStream(Arquivo, FileMode.Open, FileAccess.Read))
            {
                using (BinaryReader br = new BinaryReader(fStream))
                {
                    bdata = br.ReadBytes((int)(new FileInfo(Arquivo).Length));
                    br.Close();
                    fStream.Close();
                }
            };
            WSPublica3.dAPartamentoDoc dNovo = new Calendario.WSPublica3.dAPartamentoDoc();
            WSPublica3.dAPartamentoDoc.AGendaDocRow novarow = dNovo.AGendaDoc.NewAGendaDocRow();
            novarow.AGD_AGB = LinhaMae.AGB;
            novarow.AGD_CON = LinhaMae.AGB_CON;
            novarow.AGD_EMP = Framework.DSCentral.EMP;
            novarow.AGD_TAG = LinhaMae.AGB_TAG;
            novarow.AGD_USU = Framework.usuarioLogado.UsuarioLogadoNeon.UsuarioLogadoSt.USU;
            novarow.AGDData = DateTime.Now;
            novarow.AGDDescritivo = Descricao;
            novarow.AGDEXT = Path.GetExtension(Arquivo).Substring(1);
            novarow.AGDPublico = false;            
            dNovo.AGendaDoc.AddAGendaDocRow(novarow);
            WS.PublicaDOCAGD(Criptografa(), dNovo, bdata);
        }

        private void gridControl1_DoubleClick(object sender, EventArgs e)
        {
            WSPublica3.dAPartamentoDoc.AGendaDocRow row = (WSPublica3.dAPartamentoDoc.AGendaDocRow)gridView1.GetFocusedDataRow();
            if (row != null)
            {
                componenteWEB1.Navigate(string.Format(@"http://www.neonimoveis.com.br/neon23/neononline/pdfdoc.aspx?ARQUIVO=APTDOC{0}", row.AGD));
            }
        }
    }
}
