using System;
using DevExpress.XtraScheduler;

namespace Calendario.Agenda
{
    /// <summary>
    /// 
    /// </summary>
    public partial class fEditorAGB : CompontesBasicos.FormBase
    {                                
        /// <summary>
        /// 
        /// </summary>
        /// <param name="control"></param>
        /// <param name="apt"></param>
        /// <param name="openRecurrenceForm"></param>
        public fEditorAGB(SchedulerControl control, Appointment apt, bool openRecurrenceForm)
        {            
            InitializeComponent();            
            cEditorAGB1.Iniciar(control,apt,openRecurrenceForm);            
        }
                
        private void btnOK_Click(object sender, System.EventArgs e)
        {
            cEditorAGB1.RecuperarValores(cEditorAGB.TipoRec.Apontamento);
            
        }

        private void simpleButton1_Click(object sender, EventArgs e)
        {

            DialogResult = cEditorAGB1.Serie();
        }                
    }

    
}

