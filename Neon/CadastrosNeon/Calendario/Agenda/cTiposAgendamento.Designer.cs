namespace Calendario.Agenda
{
    partial class cTiposAgendamento
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.TipoAgendamentobindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.colTAGTitulo = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colTAGResp_USU = new DevExpress.XtraGrid.Columns.GridColumn();
            this.LookUpUSU = new DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit();
            this.uSUariosBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.dUSUariosBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.colTAGDiasUteis = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colTAGCor = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemColorEdit1 = new DevExpress.XtraEditors.Repository.RepositoryItemColorEdit();
            ((System.ComponentModel.ISupportInitialize)(this.PopImprimir_F)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.BarManager_F)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.PopExportar_F)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.GridControl_F)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.GridView_F)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.BindingSource_F)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.StyleController_F)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TipoAgendamentobindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LookUpUSU)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uSUariosBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dUSUariosBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemColorEdit1)).BeginInit();
            this.SuspendLayout();
            // 
            // barNavegacao_F
            // 
            this.barNavegacao_F.OptionsBar.DisableClose = true;
            this.barNavegacao_F.OptionsBar.DrawDragBorder = false;
            this.barNavegacao_F.OptionsBar.DrawSizeGrip = true;
            this.barNavegacao_F.OptionsBar.RotateWhenVertical = false;
            this.barNavegacao_F.OptionsBar.UseWholeRow = true;
            // 
            // BtnAlterar_F
            // 
            this.BtnAlterar_F.Enabled = false;
            // 
            // BtnExcluir_F
            // 
            this.BtnExcluir_F.Enabled = false;
            // 
            // barDataBase_F
            // 
            this.barDataBase_F.OptionsBar.AllowQuickCustomization = false;
            this.barDataBase_F.OptionsBar.DisableClose = true;
            this.barDataBase_F.OptionsBar.DisableCustomization = true;
            this.barDataBase_F.OptionsBar.DrawDragBorder = false;
            this.barDataBase_F.OptionsBar.UseWholeRow = true;
            // 
            // GridControl_F
            // 
            this.GridControl_F.DataSource = this.TipoAgendamentobindingSource;
            this.GridControl_F.EmbeddedNavigator.Buttons.Append.Visible = false;
            this.GridControl_F.EmbeddedNavigator.Buttons.CancelEdit.Visible = false;
            this.GridControl_F.EmbeddedNavigator.Buttons.Edit.Visible = false;
            this.GridControl_F.EmbeddedNavigator.Buttons.EndEdit.Visible = false;
            this.GridControl_F.EmbeddedNavigator.Buttons.Remove.Visible = false;
            this.GridControl_F.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.LookUpUSU,
            this.repositoryItemColorEdit1});
            this.GridControl_F.ShowOnlyPredefinedDetails = true;
            this.GridControl_F.Size = new System.Drawing.Size(1545, 739);
            // 
            // GridView_F
            // 
            this.GridView_F.Appearance.ColumnFilterButton.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(244)))), ((int)(((byte)(247)))), ((int)(((byte)(222)))));
            this.GridView_F.Appearance.ColumnFilterButton.BackColor2 = System.Drawing.Color.FromArgb(((int)(((byte)(184)))), ((int)(((byte)(199)))), ((int)(((byte)(146)))));
            this.GridView_F.Appearance.ColumnFilterButton.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(244)))), ((int)(((byte)(247)))), ((int)(((byte)(222)))));
            this.GridView_F.Appearance.ColumnFilterButton.ForeColor = System.Drawing.Color.Black;
            this.GridView_F.Appearance.ColumnFilterButton.GradientMode = System.Drawing.Drawing2D.LinearGradientMode.Vertical;
            this.GridView_F.Appearance.ColumnFilterButton.Options.UseBackColor = true;
            this.GridView_F.Appearance.ColumnFilterButton.Options.UseBorderColor = true;
            this.GridView_F.Appearance.ColumnFilterButton.Options.UseForeColor = true;
            this.GridView_F.Appearance.ColumnFilterButtonActive.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(254)))), ((int)(((byte)(255)))), ((int)(((byte)(244)))));
            this.GridView_F.Appearance.ColumnFilterButtonActive.BackColor2 = System.Drawing.Color.FromArgb(((int)(((byte)(213)))), ((int)(((byte)(222)))), ((int)(((byte)(183)))));
            this.GridView_F.Appearance.ColumnFilterButtonActive.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(254)))), ((int)(((byte)(255)))), ((int)(((byte)(244)))));
            this.GridView_F.Appearance.ColumnFilterButtonActive.ForeColor = System.Drawing.Color.Black;
            this.GridView_F.Appearance.ColumnFilterButtonActive.GradientMode = System.Drawing.Drawing2D.LinearGradientMode.Vertical;
            this.GridView_F.Appearance.ColumnFilterButtonActive.Options.UseBackColor = true;
            this.GridView_F.Appearance.ColumnFilterButtonActive.Options.UseBorderColor = true;
            this.GridView_F.Appearance.ColumnFilterButtonActive.Options.UseForeColor = true;
            this.GridView_F.Appearance.Empty.BackColor = System.Drawing.Color.White;
            this.GridView_F.Appearance.Empty.Options.UseBackColor = true;
            this.GridView_F.Appearance.EvenRow.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(244)))), ((int)(((byte)(247)))), ((int)(((byte)(222)))));
            this.GridView_F.Appearance.EvenRow.ForeColor = System.Drawing.Color.Black;
            this.GridView_F.Appearance.EvenRow.Options.UseBackColor = true;
            this.GridView_F.Appearance.EvenRow.Options.UseForeColor = true;
            this.GridView_F.Appearance.FilterCloseButton.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(244)))), ((int)(((byte)(247)))), ((int)(((byte)(222)))));
            this.GridView_F.Appearance.FilterCloseButton.BackColor2 = System.Drawing.Color.FromArgb(((int)(((byte)(184)))), ((int)(((byte)(199)))), ((int)(((byte)(146)))));
            this.GridView_F.Appearance.FilterCloseButton.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(244)))), ((int)(((byte)(247)))), ((int)(((byte)(222)))));
            this.GridView_F.Appearance.FilterCloseButton.ForeColor = System.Drawing.Color.Black;
            this.GridView_F.Appearance.FilterCloseButton.GradientMode = System.Drawing.Drawing2D.LinearGradientMode.Vertical;
            this.GridView_F.Appearance.FilterCloseButton.Options.UseBackColor = true;
            this.GridView_F.Appearance.FilterCloseButton.Options.UseBorderColor = true;
            this.GridView_F.Appearance.FilterCloseButton.Options.UseForeColor = true;
            this.GridView_F.Appearance.FilterPanel.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(139)))), ((int)(((byte)(158)))), ((int)(((byte)(126)))));
            this.GridView_F.Appearance.FilterPanel.ForeColor = System.Drawing.Color.White;
            this.GridView_F.Appearance.FilterPanel.Options.UseBackColor = true;
            this.GridView_F.Appearance.FilterPanel.Options.UseForeColor = true;
            this.GridView_F.Appearance.FixedLine.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(96)))), ((int)(((byte)(128)))), ((int)(((byte)(88)))));
            this.GridView_F.Appearance.FixedLine.Options.UseBackColor = true;
            this.GridView_F.Appearance.FocusedCell.BackColor = System.Drawing.Color.White;
            this.GridView_F.Appearance.FocusedCell.ForeColor = System.Drawing.Color.Black;
            this.GridView_F.Appearance.FocusedCell.Options.UseBackColor = true;
            this.GridView_F.Appearance.FocusedCell.Options.UseForeColor = true;
            this.GridView_F.Appearance.FocusedRow.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(147)))), ((int)(((byte)(160)))), ((int)(((byte)(112)))));
            this.GridView_F.Appearance.FocusedRow.ForeColor = System.Drawing.Color.White;
            this.GridView_F.Appearance.FocusedRow.Options.UseBackColor = true;
            this.GridView_F.Appearance.FocusedRow.Options.UseForeColor = true;
            this.GridView_F.Appearance.FooterPanel.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(244)))), ((int)(((byte)(247)))), ((int)(((byte)(222)))));
            this.GridView_F.Appearance.FooterPanel.BackColor2 = System.Drawing.Color.FromArgb(((int)(((byte)(184)))), ((int)(((byte)(199)))), ((int)(((byte)(146)))));
            this.GridView_F.Appearance.FooterPanel.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(244)))), ((int)(((byte)(247)))), ((int)(((byte)(222)))));
            this.GridView_F.Appearance.FooterPanel.ForeColor = System.Drawing.Color.Black;
            this.GridView_F.Appearance.FooterPanel.GradientMode = System.Drawing.Drawing2D.LinearGradientMode.Vertical;
            this.GridView_F.Appearance.FooterPanel.Options.UseBackColor = true;
            this.GridView_F.Appearance.FooterPanel.Options.UseBorderColor = true;
            this.GridView_F.Appearance.FooterPanel.Options.UseForeColor = true;
            this.GridView_F.Appearance.GroupButton.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(223)))), ((int)(((byte)(230)))), ((int)(((byte)(195)))));
            this.GridView_F.Appearance.GroupButton.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(223)))), ((int)(((byte)(230)))), ((int)(((byte)(195)))));
            this.GridView_F.Appearance.GroupButton.ForeColor = System.Drawing.Color.Black;
            this.GridView_F.Appearance.GroupButton.Options.UseBackColor = true;
            this.GridView_F.Appearance.GroupButton.Options.UseBorderColor = true;
            this.GridView_F.Appearance.GroupButton.Options.UseForeColor = true;
            this.GridView_F.Appearance.GroupFooter.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(223)))), ((int)(((byte)(230)))), ((int)(((byte)(195)))));
            this.GridView_F.Appearance.GroupFooter.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(223)))), ((int)(((byte)(230)))), ((int)(((byte)(195)))));
            this.GridView_F.Appearance.GroupFooter.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Bold);
            this.GridView_F.Appearance.GroupFooter.ForeColor = System.Drawing.Color.Black;
            this.GridView_F.Appearance.GroupFooter.Options.UseBackColor = true;
            this.GridView_F.Appearance.GroupFooter.Options.UseBorderColor = true;
            this.GridView_F.Appearance.GroupFooter.Options.UseFont = true;
            this.GridView_F.Appearance.GroupFooter.Options.UseForeColor = true;
            this.GridView_F.Appearance.GroupPanel.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(139)))), ((int)(((byte)(158)))), ((int)(((byte)(126)))));
            this.GridView_F.Appearance.GroupPanel.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(244)))), ((int)(((byte)(247)))), ((int)(((byte)(222)))));
            this.GridView_F.Appearance.GroupPanel.Options.UseBackColor = true;
            this.GridView_F.Appearance.GroupPanel.Options.UseForeColor = true;
            this.GridView_F.Appearance.GroupRow.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(223)))), ((int)(((byte)(230)))), ((int)(((byte)(195)))));
            this.GridView_F.Appearance.GroupRow.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(223)))), ((int)(((byte)(230)))), ((int)(((byte)(195)))));
            this.GridView_F.Appearance.GroupRow.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Bold);
            this.GridView_F.Appearance.GroupRow.ForeColor = System.Drawing.Color.Black;
            this.GridView_F.Appearance.GroupRow.Options.UseBackColor = true;
            this.GridView_F.Appearance.GroupRow.Options.UseBorderColor = true;
            this.GridView_F.Appearance.GroupRow.Options.UseFont = true;
            this.GridView_F.Appearance.GroupRow.Options.UseForeColor = true;
            this.GridView_F.Appearance.HeaderPanel.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(244)))), ((int)(((byte)(247)))), ((int)(((byte)(222)))));
            this.GridView_F.Appearance.HeaderPanel.BackColor2 = System.Drawing.Color.FromArgb(((int)(((byte)(184)))), ((int)(((byte)(199)))), ((int)(((byte)(146)))));
            this.GridView_F.Appearance.HeaderPanel.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(244)))), ((int)(((byte)(247)))), ((int)(((byte)(222)))));
            this.GridView_F.Appearance.HeaderPanel.ForeColor = System.Drawing.Color.Black;
            this.GridView_F.Appearance.HeaderPanel.GradientMode = System.Drawing.Drawing2D.LinearGradientMode.Vertical;
            this.GridView_F.Appearance.HeaderPanel.Options.UseBackColor = true;
            this.GridView_F.Appearance.HeaderPanel.Options.UseBorderColor = true;
            this.GridView_F.Appearance.HeaderPanel.Options.UseForeColor = true;
            this.GridView_F.Appearance.HideSelectionRow.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(197)))), ((int)(((byte)(207)))), ((int)(((byte)(170)))));
            this.GridView_F.Appearance.HideSelectionRow.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(147)))), ((int)(((byte)(160)))), ((int)(((byte)(112)))));
            this.GridView_F.Appearance.HideSelectionRow.Options.UseBackColor = true;
            this.GridView_F.Appearance.HideSelectionRow.Options.UseForeColor = true;
            this.GridView_F.Appearance.HorzLine.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(136)))), ((int)(((byte)(168)))), ((int)(((byte)(128)))));
            this.GridView_F.Appearance.HorzLine.Options.UseBackColor = true;
            this.GridView_F.Appearance.Preview.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(252)))), ((int)(((byte)(253)))), ((int)(((byte)(247)))));
            this.GridView_F.Appearance.Preview.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(147)))), ((int)(((byte)(160)))), ((int)(((byte)(112)))));
            this.GridView_F.Appearance.Preview.Options.UseBackColor = true;
            this.GridView_F.Appearance.Preview.Options.UseForeColor = true;
            this.GridView_F.Appearance.Row.BackColor = System.Drawing.Color.White;
            this.GridView_F.Appearance.Row.ForeColor = System.Drawing.Color.Black;
            this.GridView_F.Appearance.Row.Options.UseBackColor = true;
            this.GridView_F.Appearance.Row.Options.UseForeColor = true;
            this.GridView_F.Appearance.RowSeparator.BackColor = System.Drawing.Color.White;
            this.GridView_F.Appearance.RowSeparator.Options.UseBackColor = true;
            this.GridView_F.Appearance.SelectedRow.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(166)))), ((int)(((byte)(178)))), ((int)(((byte)(133)))));
            this.GridView_F.Appearance.SelectedRow.ForeColor = System.Drawing.Color.White;
            this.GridView_F.Appearance.SelectedRow.Options.UseBackColor = true;
            this.GridView_F.Appearance.SelectedRow.Options.UseForeColor = true;
            this.GridView_F.Appearance.TopNewRow.BackColor = System.Drawing.Color.White;
            this.GridView_F.Appearance.TopNewRow.Options.UseBackColor = true;
            this.GridView_F.Appearance.VertLine.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(136)))), ((int)(((byte)(160)))), ((int)(((byte)(188)))));
            this.GridView_F.Appearance.VertLine.Options.UseBackColor = true;
            this.GridView_F.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colTAGTitulo,
            this.colTAGResp_USU,
            this.colTAGDiasUteis,
            this.colTAGCor});
            this.GridView_F.OptionsBehavior.AllowIncrementalSearch = true;
            this.GridView_F.OptionsBehavior.Editable = false;
            this.GridView_F.OptionsLayout.Columns.StoreAllOptions = true;
            this.GridView_F.OptionsLayout.Columns.StoreAppearance = true;
            this.GridView_F.OptionsLayout.StoreAllOptions = true;
            this.GridView_F.OptionsLayout.StoreAppearance = true;
            this.GridView_F.OptionsNavigation.EnterMoveNextColumn = true;
            this.GridView_F.OptionsPrint.EnableAppearanceEvenRow = true;
            this.GridView_F.OptionsPrint.EnableAppearanceOddRow = true;
            this.GridView_F.OptionsPrint.ExpandAllDetails = true;
            this.GridView_F.OptionsView.ColumnAutoWidth = false;
            this.GridView_F.OptionsView.EnableAppearanceEvenRow = true;
            this.GridView_F.OptionsView.GroupFooterShowMode = DevExpress.XtraGrid.Views.Grid.GroupFooterShowMode.VisibleAlways;
            this.GridView_F.OptionsView.NewItemRowPosition = DevExpress.XtraGrid.Views.Grid.NewItemRowPosition.Bottom;
            this.GridView_F.OptionsView.ShowAutoFilterRow = true;
            this.GridView_F.OptionsView.ShowFooter = true;
            this.GridView_F.OptionsView.ShowGroupPanel = false;
            // 
            // ToolTipController_F
            // 
            this.ToolTipController_F.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(222)))), ((int)(((byte)(237)))), ((int)(((byte)(250)))));
            this.ToolTipController_F.Appearance.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(20)))), ((int)(((byte)(69)))), ((int)(((byte)(141)))));
            this.ToolTipController_F.Appearance.Options.UseBackColor = true;
            this.ToolTipController_F.Appearance.Options.UseFont = true;
            this.ToolTipController_F.Appearance.Options.UseForeColor = true;
            this.ToolTipController_F.Appearance.Options.UseTextOptions = true;
            this.ToolTipController_F.Appearance.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            // 
            // StyleController_F
            // 
            this.StyleController_F.AppearanceDisabled.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this.StyleController_F.AppearanceDisabled.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this.StyleController_F.AppearanceDisabled.Options.UseBackColor = true;
            this.StyleController_F.AppearanceDisabled.Options.UseForeColor = true;
            this.StyleController_F.AppearanceFocused.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.StyleController_F.AppearanceFocused.Options.UseBackColor = true;
            // 
            // TipoAgendamentobindingSource
            // 
            this.TipoAgendamentobindingSource.DataMember = "TipoAGendamento";
            this.TipoAgendamentobindingSource.DataSource = typeof(Calendario.Agenda.dAGendaBase);
            // 
            // colTAGTitulo
            // 
            this.colTAGTitulo.Caption = "Título";
            this.colTAGTitulo.FieldName = "TAGTitulo";
            this.colTAGTitulo.Name = "colTAGTitulo";
            this.colTAGTitulo.Visible = true;
            this.colTAGTitulo.VisibleIndex = 0;
            this.colTAGTitulo.Width = 227;
            // 
            // colTAGResp_USU
            // 
            this.colTAGResp_USU.Caption = "Responsável";
            this.colTAGResp_USU.ColumnEdit = this.LookUpUSU;
            this.colTAGResp_USU.FieldName = "TAGResp_USU";
            this.colTAGResp_USU.Name = "colTAGResp_USU";
            this.colTAGResp_USU.Visible = true;
            this.colTAGResp_USU.VisibleIndex = 1;
            this.colTAGResp_USU.Width = 131;
            // 
            // LookUpUSU
            // 
            this.LookUpUSU.AutoHeight = false;
            this.LookUpUSU.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LookUpUSU.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("USUNome", "USU Nome", 60, DevExpress.Utils.FormatType.None, "", true, DevExpress.Utils.HorzAlignment.Near)});
            this.LookUpUSU.DataSource = this.uSUariosBindingSource;
            this.LookUpUSU.DisplayMember = "USUNome";
            this.LookUpUSU.Name = "LookUpUSU";
            this.LookUpUSU.ShowHeader = false;
            this.LookUpUSU.ValueMember = "USU";
            // 
            // uSUariosBindingSource
            // 
            this.uSUariosBindingSource.DataMember = "USUarios";
            this.uSUariosBindingSource.DataSource = this.dUSUariosBindingSource;
            // 
            // dUSUariosBindingSource
            // 
            this.dUSUariosBindingSource.DataSource = typeof(FrameworkProc.datasets.dUSUarios);
            this.dUSUariosBindingSource.Position = 0;
            // 
            // colTAGDiasUteis
            // 
            this.colTAGDiasUteis.Caption = "Dias de aviso";
            this.colTAGDiasUteis.FieldName = "TAGDiasUteis";
            this.colTAGDiasUteis.Name = "colTAGDiasUteis";
            this.colTAGDiasUteis.Visible = true;
            this.colTAGDiasUteis.VisibleIndex = 2;
            this.colTAGDiasUteis.Width = 95;
            // 
            // colTAGCor
            // 
            this.colTAGCor.Caption = "Cor";
            this.colTAGCor.ColumnEdit = this.repositoryItemColorEdit1;
            this.colTAGCor.FieldName = "TAGCor";
            this.colTAGCor.Name = "colTAGCor";
            this.colTAGCor.Visible = true;
            this.colTAGCor.VisibleIndex = 3;
            this.colTAGCor.Width = 32;
            // 
            // repositoryItemColorEdit1
            // 
            this.repositoryItemColorEdit1.AutoHeight = false;
            this.repositoryItemColorEdit1.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemColorEdit1.Name = "repositoryItemColorEdit1";
            this.repositoryItemColorEdit1.StoreColorAsInteger = true;
            // 
            // cTiposAgendamento
            // 
            this.Autofill = false;
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BindingSourcePrincipal = this.TipoAgendamentobindingSource;
            this.LookAndFeel.UseDefaultLookAndFeel = false;
            this.Name = "cTiposAgendamento";
            this.Size = new System.Drawing.Size(1545, 799);
            this.Titulo = "CompontesBasicos.ComponenteGradeNavegador - CompontesBasicos.ComponenteGradeNaveg" +
    "ador - CompontesBasicos.ComponenteGradeNavegador - Tipos de Agendamento";
            ((System.ComponentModel.ISupportInitialize)(this.PopImprimir_F)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.BarManager_F)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.PopExportar_F)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.GridControl_F)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.GridView_F)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.BindingSource_F)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.StyleController_F)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TipoAgendamentobindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LookUpUSU)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uSUariosBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dUSUariosBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemColorEdit1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.BindingSource TipoAgendamentobindingSource;
        private DevExpress.XtraGrid.Columns.GridColumn colTAGTitulo;
        private DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit LookUpUSU;
        private System.Windows.Forms.BindingSource uSUariosBindingSource;
        private System.Windows.Forms.BindingSource dUSUariosBindingSource;
        private DevExpress.XtraGrid.Columns.GridColumn colTAGResp_USU;
        private DevExpress.XtraGrid.Columns.GridColumn colTAGDiasUteis;
        private DevExpress.XtraEditors.Repository.RepositoryItemColorEdit repositoryItemColorEdit1;
        private DevExpress.XtraGrid.Columns.GridColumn colTAGCor;
    }
}
