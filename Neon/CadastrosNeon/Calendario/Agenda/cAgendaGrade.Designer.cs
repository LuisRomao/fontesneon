namespace Calendario.Agenda
{
    partial class cAgendaGrade
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(cAgendaGrade));
            DevExpress.XtraEditors.Controls.EditorButtonImageOptions editorButtonImageOptions1 = new DevExpress.XtraEditors.Controls.EditorButtonImageOptions();
            this.aGendaBaseBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.dAgendaBaseGrade = new Calendario.Agenda.dAgendaBaseGrade();
            this.colAGB_CON = new DevExpress.XtraGrid.Columns.GridColumn();
            this.LookUpCond = new DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit();
            this.cONDOMINIOSBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.colAGBDataInicio = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colAGBDataTermino = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colAGB_TAG = new DevExpress.XtraGrid.Columns.GridColumn();
            this.LookUpTAG = new DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit();
            this.tipoAGendamentoBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.colAGBDescricao = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colAGBSubDescricao = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colAGB_USU = new DevExpress.XtraGrid.Columns.GridColumn();
            this.LookUpUSU = new DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit();
            this.uSUARIOSBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.colAGBDataLimite = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colAGBDATAA = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colAGBDATAI = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemImageComboBox1 = new DevExpress.XtraEditors.Repository.RepositoryItemImageComboBox();
            this.colAGBStatus = new DevExpress.XtraGrid.Columns.GridColumn();
            this.simpleButton1 = new DevExpress.XtraEditors.SimpleButton();
            this.simpleButton2 = new DevExpress.XtraEditors.SimpleButton();
            this.colGerente = new DevExpress.XtraGrid.Columns.GridColumn();
            this.cChamada1 = new Calendario.Zoom.cChamada();
            this.colAGB = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumnLinques = new DevExpress.XtraGrid.Columns.GridColumn();
            this.Linques = new DevExpress.XtraEditors.Repository.RepositoryItemButtonEdit();
            this.gridColumn1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemButtonEdit1 = new DevExpress.XtraEditors.Repository.RepositoryItemButtonEdit();
            this.simpleButton3 = new DevExpress.XtraEditors.SimpleButton();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl1)).BeginInit();
            this.panelControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.PopImprimir_F)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.BarManager_F)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.PopExportar_F)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.GridControl_F)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.GridView_F)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.BindingSource_F)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.StyleController_F)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.aGendaBaseBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dAgendaBaseGrade)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LookUpCond)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cONDOMINIOSBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LookUpTAG)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tipoAGendamentoBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LookUpUSU)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uSUARIOSBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemImageComboBox1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Linques)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemButtonEdit1)).BeginInit();
            this.SuspendLayout();
            // 
            // panelControl1
            // 
            this.panelControl1.Controls.Add(this.simpleButton3);
            this.panelControl1.Controls.Add(this.simpleButton2);
            this.panelControl1.Controls.Add(this.simpleButton1);
            this.panelControl1.Size = new System.Drawing.Size(1396, 50);
            this.panelControl1.Controls.SetChildIndex(this.simpleButton1, 0);
            this.panelControl1.Controls.SetChildIndex(this.simpleButton2, 0);
            this.panelControl1.Controls.SetChildIndex(this.simpleButton3, 0);
            // 
            // barNavegacao_F
            // 
            this.barNavegacao_F.OptionsBar.DisableClose = true;
            this.barNavegacao_F.OptionsBar.DrawDragBorder = false;
            this.barNavegacao_F.OptionsBar.DrawSizeGrip = true;
            this.barNavegacao_F.OptionsBar.RotateWhenVertical = false;
            this.barNavegacao_F.OptionsBar.UseWholeRow = true;
            // 
            // BtnIncluir_F
            // 
            this.BtnIncluir_F.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("BtnIncluir_F.ImageOptions.Image")));
            // 
            // BtnAlterar_F
            // 
            this.BtnAlterar_F.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("BtnAlterar_F.ImageOptions.Image")));
            this.BtnAlterar_F.ImageOptions.ImageIndex = 0;
            // 
            // BtnExcluir_F
            // 
            this.BtnExcluir_F.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("BtnExcluir_F.ImageOptions.Image")));
            // 
            // BtnImprimir_F
            // 
            this.BtnImprimir_F.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("BtnImprimir_F.ImageOptions.Image")));
            // 
            // BtnImprimirGrade_F
            // 
            this.BtnImprimirGrade_F.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("BtnImprimirGrade_F.ImageOptions.Image")));
            // 
            // BtnExportar_F
            // 
            this.BtnExportar_F.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("BtnExportar_F.ImageOptions.Image")));
            // 
            // btnConfirmar_F
            // 
            this.btnConfirmar_F.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("btnConfirmar_F.ImageOptions.Image")));
            // 
            // btnCancelar_F
            // 
            this.btnCancelar_F.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("btnCancelar_F.ImageOptions.Image")));
            // 
            // barDataBase_F
            // 
            this.barDataBase_F.OptionsBar.AllowQuickCustomization = false;
            this.barDataBase_F.OptionsBar.DisableClose = true;
            this.barDataBase_F.OptionsBar.DisableCustomization = true;
            this.barDataBase_F.OptionsBar.DrawDragBorder = false;
            this.barDataBase_F.OptionsBar.UseWholeRow = true;
            // 
            // GridControl_F
            // 
            this.GridControl_F.DataSource = this.aGendaBaseBindingSource;
            this.GridControl_F.EmbeddedNavigator.Buttons.Append.Visible = false;
            this.GridControl_F.EmbeddedNavigator.Buttons.CancelEdit.Visible = false;
            this.GridControl_F.EmbeddedNavigator.Buttons.Edit.Visible = false;
            this.GridControl_F.EmbeddedNavigator.Buttons.EndEdit.Visible = false;
            this.GridControl_F.EmbeddedNavigator.Buttons.Remove.Visible = false;
            this.GridControl_F.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.LookUpUSU,
            this.LookUpTAG,
            this.LookUpCond,
            this.repositoryItemImageComboBox1,
            this.Linques,
            this.repositoryItemButtonEdit1});
            this.GridControl_F.ShowOnlyPredefinedDetails = true;
            this.GridControl_F.Size = new System.Drawing.Size(1396, 697);
            // 
            // GridView_F
            // 
            this.GridView_F.Appearance.ColumnFilterButton.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(104)))), ((int)(((byte)(184)))), ((int)(((byte)(251)))));
            this.GridView_F.Appearance.ColumnFilterButton.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(104)))), ((int)(((byte)(184)))), ((int)(((byte)(251)))));
            this.GridView_F.Appearance.ColumnFilterButton.ForeColor = System.Drawing.Color.White;
            this.GridView_F.Appearance.ColumnFilterButton.Options.UseBackColor = true;
            this.GridView_F.Appearance.ColumnFilterButton.Options.UseBorderColor = true;
            this.GridView_F.Appearance.ColumnFilterButton.Options.UseForeColor = true;
            this.GridView_F.Appearance.ColumnFilterButtonActive.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(170)))), ((int)(((byte)(216)))), ((int)(((byte)(254)))));
            this.GridView_F.Appearance.ColumnFilterButtonActive.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(170)))), ((int)(((byte)(216)))), ((int)(((byte)(254)))));
            this.GridView_F.Appearance.ColumnFilterButtonActive.ForeColor = System.Drawing.Color.Black;
            this.GridView_F.Appearance.ColumnFilterButtonActive.Options.UseBackColor = true;
            this.GridView_F.Appearance.ColumnFilterButtonActive.Options.UseBorderColor = true;
            this.GridView_F.Appearance.ColumnFilterButtonActive.Options.UseForeColor = true;
            this.GridView_F.Appearance.Empty.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(236)))), ((int)(((byte)(246)))), ((int)(((byte)(255)))));
            this.GridView_F.Appearance.Empty.BackColor2 = System.Drawing.Color.White;
            this.GridView_F.Appearance.Empty.Options.UseBackColor = true;
            this.GridView_F.Appearance.EvenRow.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(247)))), ((int)(((byte)(251)))), ((int)(((byte)(255)))));
            this.GridView_F.Appearance.EvenRow.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(247)))), ((int)(((byte)(251)))), ((int)(((byte)(255)))));
            this.GridView_F.Appearance.EvenRow.ForeColor = System.Drawing.Color.Black;
            this.GridView_F.Appearance.EvenRow.Options.UseBackColor = true;
            this.GridView_F.Appearance.EvenRow.Options.UseBorderColor = true;
            this.GridView_F.Appearance.EvenRow.Options.UseForeColor = true;
            this.GridView_F.Appearance.FilterCloseButton.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(104)))), ((int)(((byte)(184)))), ((int)(((byte)(251)))));
            this.GridView_F.Appearance.FilterCloseButton.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(104)))), ((int)(((byte)(184)))), ((int)(((byte)(251)))));
            this.GridView_F.Appearance.FilterCloseButton.ForeColor = System.Drawing.Color.White;
            this.GridView_F.Appearance.FilterCloseButton.Options.UseBackColor = true;
            this.GridView_F.Appearance.FilterCloseButton.Options.UseBorderColor = true;
            this.GridView_F.Appearance.FilterCloseButton.Options.UseForeColor = true;
            this.GridView_F.Appearance.FilterPanel.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(236)))), ((int)(((byte)(246)))), ((int)(((byte)(255)))));
            this.GridView_F.Appearance.FilterPanel.BackColor2 = System.Drawing.Color.White;
            this.GridView_F.Appearance.FilterPanel.ForeColor = System.Drawing.Color.Black;
            this.GridView_F.Appearance.FilterPanel.Options.UseBackColor = true;
            this.GridView_F.Appearance.FilterPanel.Options.UseForeColor = true;
            this.GridView_F.Appearance.FixedLine.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(59)))), ((int)(((byte)(133)))), ((int)(((byte)(195)))));
            this.GridView_F.Appearance.FixedLine.Options.UseBackColor = true;
            this.GridView_F.Appearance.FocusedCell.BackColor = System.Drawing.Color.White;
            this.GridView_F.Appearance.FocusedCell.ForeColor = System.Drawing.Color.Black;
            this.GridView_F.Appearance.FocusedCell.Options.UseBackColor = true;
            this.GridView_F.Appearance.FocusedCell.Options.UseForeColor = true;
            this.GridView_F.Appearance.FocusedRow.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(38)))), ((int)(((byte)(109)))), ((int)(((byte)(189)))));
            this.GridView_F.Appearance.FocusedRow.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(59)))), ((int)(((byte)(139)))), ((int)(((byte)(206)))));
            this.GridView_F.Appearance.FocusedRow.ForeColor = System.Drawing.Color.White;
            this.GridView_F.Appearance.FocusedRow.Options.UseBackColor = true;
            this.GridView_F.Appearance.FocusedRow.Options.UseBorderColor = true;
            this.GridView_F.Appearance.FocusedRow.Options.UseForeColor = true;
            this.GridView_F.Appearance.FooterPanel.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(104)))), ((int)(((byte)(184)))), ((int)(((byte)(251)))));
            this.GridView_F.Appearance.FooterPanel.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(104)))), ((int)(((byte)(184)))), ((int)(((byte)(251)))));
            this.GridView_F.Appearance.FooterPanel.ForeColor = System.Drawing.Color.Black;
            this.GridView_F.Appearance.FooterPanel.Options.UseBackColor = true;
            this.GridView_F.Appearance.FooterPanel.Options.UseBorderColor = true;
            this.GridView_F.Appearance.FooterPanel.Options.UseForeColor = true;
            this.GridView_F.Appearance.GroupButton.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(104)))), ((int)(((byte)(184)))), ((int)(((byte)(251)))));
            this.GridView_F.Appearance.GroupButton.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(104)))), ((int)(((byte)(184)))), ((int)(((byte)(251)))));
            this.GridView_F.Appearance.GroupButton.Options.UseBackColor = true;
            this.GridView_F.Appearance.GroupButton.Options.UseBorderColor = true;
            this.GridView_F.Appearance.GroupFooter.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(170)))), ((int)(((byte)(216)))), ((int)(((byte)(254)))));
            this.GridView_F.Appearance.GroupFooter.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(170)))), ((int)(((byte)(216)))), ((int)(((byte)(254)))));
            this.GridView_F.Appearance.GroupFooter.ForeColor = System.Drawing.Color.Black;
            this.GridView_F.Appearance.GroupFooter.Options.UseBackColor = true;
            this.GridView_F.Appearance.GroupFooter.Options.UseBorderColor = true;
            this.GridView_F.Appearance.GroupFooter.Options.UseForeColor = true;
            this.GridView_F.Appearance.GroupPanel.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(236)))), ((int)(((byte)(246)))), ((int)(((byte)(255)))));
            this.GridView_F.Appearance.GroupPanel.BackColor2 = System.Drawing.Color.White;
            this.GridView_F.Appearance.GroupPanel.ForeColor = System.Drawing.Color.Black;
            this.GridView_F.Appearance.GroupPanel.Options.UseBackColor = true;
            this.GridView_F.Appearance.GroupPanel.Options.UseForeColor = true;
            this.GridView_F.Appearance.GroupRow.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(170)))), ((int)(((byte)(216)))), ((int)(((byte)(254)))));
            this.GridView_F.Appearance.GroupRow.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(170)))), ((int)(((byte)(216)))), ((int)(((byte)(254)))));
            this.GridView_F.Appearance.GroupRow.ForeColor = System.Drawing.Color.Black;
            this.GridView_F.Appearance.GroupRow.Options.UseBackColor = true;
            this.GridView_F.Appearance.GroupRow.Options.UseBorderColor = true;
            this.GridView_F.Appearance.GroupRow.Options.UseForeColor = true;
            this.GridView_F.Appearance.HeaderPanel.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(139)))), ((int)(((byte)(201)))), ((int)(((byte)(254)))));
            this.GridView_F.Appearance.HeaderPanel.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(139)))), ((int)(((byte)(201)))), ((int)(((byte)(254)))));
            this.GridView_F.Appearance.HeaderPanel.ForeColor = System.Drawing.Color.Black;
            this.GridView_F.Appearance.HeaderPanel.Options.UseBackColor = true;
            this.GridView_F.Appearance.HeaderPanel.Options.UseBorderColor = true;
            this.GridView_F.Appearance.HeaderPanel.Options.UseForeColor = true;
            this.GridView_F.Appearance.HideSelectionRow.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(105)))), ((int)(((byte)(170)))), ((int)(((byte)(225)))));
            this.GridView_F.Appearance.HideSelectionRow.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(83)))), ((int)(((byte)(155)))), ((int)(((byte)(215)))));
            this.GridView_F.Appearance.HideSelectionRow.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(236)))), ((int)(((byte)(246)))), ((int)(((byte)(255)))));
            this.GridView_F.Appearance.HideSelectionRow.Options.UseBackColor = true;
            this.GridView_F.Appearance.HideSelectionRow.Options.UseBorderColor = true;
            this.GridView_F.Appearance.HideSelectionRow.Options.UseForeColor = true;
            this.GridView_F.Appearance.HorzLine.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(104)))), ((int)(((byte)(184)))), ((int)(((byte)(251)))));
            this.GridView_F.Appearance.HorzLine.Options.UseBackColor = true;
            this.GridView_F.Appearance.OddRow.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(236)))), ((int)(((byte)(246)))), ((int)(((byte)(255)))));
            this.GridView_F.Appearance.OddRow.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(236)))), ((int)(((byte)(246)))), ((int)(((byte)(255)))));
            this.GridView_F.Appearance.OddRow.ForeColor = System.Drawing.Color.Black;
            this.GridView_F.Appearance.OddRow.Options.UseBackColor = true;
            this.GridView_F.Appearance.OddRow.Options.UseBorderColor = true;
            this.GridView_F.Appearance.OddRow.Options.UseForeColor = true;
            this.GridView_F.Appearance.Preview.Font = new System.Drawing.Font("Verdana", 7.5F);
            this.GridView_F.Appearance.Preview.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(83)))), ((int)(((byte)(155)))), ((int)(((byte)(215)))));
            this.GridView_F.Appearance.Preview.Options.UseFont = true;
            this.GridView_F.Appearance.Preview.Options.UseForeColor = true;
            this.GridView_F.Appearance.Row.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(247)))), ((int)(((byte)(251)))), ((int)(((byte)(255)))));
            this.GridView_F.Appearance.Row.ForeColor = System.Drawing.Color.Black;
            this.GridView_F.Appearance.Row.Options.UseBackColor = true;
            this.GridView_F.Appearance.Row.Options.UseForeColor = true;
            this.GridView_F.Appearance.RowSeparator.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(236)))), ((int)(((byte)(246)))), ((int)(((byte)(255)))));
            this.GridView_F.Appearance.RowSeparator.BackColor2 = System.Drawing.Color.White;
            this.GridView_F.Appearance.RowSeparator.Options.UseBackColor = true;
            this.GridView_F.Appearance.SelectedRow.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(83)))), ((int)(((byte)(155)))), ((int)(((byte)(215)))));
            this.GridView_F.Appearance.SelectedRow.ForeColor = System.Drawing.Color.White;
            this.GridView_F.Appearance.SelectedRow.Options.UseBackColor = true;
            this.GridView_F.Appearance.SelectedRow.Options.UseForeColor = true;
            this.GridView_F.Appearance.TopNewRow.BackColor = System.Drawing.Color.White;
            this.GridView_F.Appearance.TopNewRow.Options.UseBackColor = true;
            this.GridView_F.Appearance.VertLine.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(104)))), ((int)(((byte)(184)))), ((int)(((byte)(251)))));
            this.GridView_F.Appearance.VertLine.Options.UseBackColor = true;
            this.GridView_F.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colAGB_CON,
            this.colAGBDataInicio,
            this.colAGBDataTermino,
            this.colAGB_TAG,
            this.colAGBDescricao,
            this.colAGBSubDescricao,
            this.colAGB_USU,
            this.colAGBDataLimite,
            this.colAGBDATAA,
            this.colAGBDATAI,
            this.colAGBStatus,
            this.colGerente,
            this.colAGB,
            this.gridColumnLinques,
            this.gridColumn1});
            this.GridView_F.GroupSummary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridGroupSummaryItem(DevExpress.Data.SummaryItemType.Count, "AGB_CON", null, "")});
            this.GridView_F.OptionsBehavior.AllowIncrementalSearch = true;
            this.GridView_F.OptionsBehavior.Editable = false;
            this.GridView_F.OptionsLayout.Columns.StoreAllOptions = true;
            this.GridView_F.OptionsLayout.Columns.StoreAppearance = true;
            this.GridView_F.OptionsLayout.StoreAllOptions = true;
            this.GridView_F.OptionsLayout.StoreAppearance = true;
            this.GridView_F.OptionsNavigation.EnterMoveNextColumn = true;
            this.GridView_F.OptionsPrint.EnableAppearanceEvenRow = true;
            this.GridView_F.OptionsPrint.EnableAppearanceOddRow = true;
            this.GridView_F.OptionsPrint.ExpandAllDetails = true;
            this.GridView_F.OptionsView.EnableAppearanceEvenRow = true;
            this.GridView_F.OptionsView.EnableAppearanceOddRow = true;
            this.GridView_F.OptionsView.GroupFooterShowMode = DevExpress.XtraGrid.Views.Grid.GroupFooterShowMode.VisibleAlways;
            this.GridView_F.OptionsView.RowAutoHeight = true;
            this.GridView_F.OptionsView.ShowAutoFilterRow = true;
            this.GridView_F.OptionsView.ShowFooter = true;
            this.GridView_F.OptionsView.ShowGroupedColumns = true;
            this.GridView_F.OptionsView.ShowGroupPanel = false;
            this.GridView_F.SortInfo.AddRange(new DevExpress.XtraGrid.Columns.GridColumnSortInfo[] {
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colAGBStatus, DevExpress.Data.ColumnSortOrder.Ascending),
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colAGBDataInicio, DevExpress.Data.ColumnSortOrder.Ascending)});
            this.GridView_F.RowCellStyle += new DevExpress.XtraGrid.Views.Grid.RowCellStyleEventHandler(this.GridView_F_RowCellStyle);
            this.GridView_F.CustomRowCellEdit += new DevExpress.XtraGrid.Views.Grid.CustomRowCellEditEventHandler(this.GridView_F_CustomRowCellEdit);
            // 
            // ToolTipController_F
            // 
            this.ToolTipController_F.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(222)))), ((int)(((byte)(237)))), ((int)(((byte)(250)))));
            this.ToolTipController_F.Appearance.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(20)))), ((int)(((byte)(69)))), ((int)(((byte)(141)))));
            this.ToolTipController_F.Appearance.Options.UseBackColor = true;
            this.ToolTipController_F.Appearance.Options.UseFont = true;
            this.ToolTipController_F.Appearance.Options.UseForeColor = true;
            this.ToolTipController_F.Appearance.Options.UseTextOptions = true;
            this.ToolTipController_F.Appearance.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            // 
            // StyleController_F
            // 
            this.StyleController_F.AppearanceDisabled.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this.StyleController_F.AppearanceDisabled.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this.StyleController_F.AppearanceDisabled.Options.UseBackColor = true;
            this.StyleController_F.AppearanceDisabled.Options.UseForeColor = true;
            this.StyleController_F.AppearanceFocused.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.StyleController_F.AppearanceFocused.Options.UseBackColor = true;
            // 
            // aGendaBaseBindingSource
            // 
            this.aGendaBaseBindingSource.DataMember = "AGendaBase";
            this.aGendaBaseBindingSource.DataSource = this.dAgendaBaseGrade;
            // 
            // dAgendaBaseGrade
            // 
            this.dAgendaBaseGrade.DataSetName = "dAgendaBaseGrade";
            this.dAgendaBaseGrade.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // colAGB_CON
            // 
            this.colAGB_CON.Caption = "Condom�nio";
            this.colAGB_CON.ColumnEdit = this.LookUpCond;
            this.colAGB_CON.FieldName = "AGB_CON";
            this.colAGB_CON.Name = "colAGB_CON";
            this.colAGB_CON.OptionsColumn.AllowEdit = false;
            this.colAGB_CON.OptionsColumn.ReadOnly = true;
            this.colAGB_CON.Visible = true;
            this.colAGB_CON.VisibleIndex = 2;
            this.colAGB_CON.Width = 145;
            // 
            // LookUpCond
            // 
            this.LookUpCond.AutoHeight = false;
            this.LookUpCond.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LookUpCond.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("CONCodigo", "CON Codigo", 68, DevExpress.Utils.FormatType.None, "", true, DevExpress.Utils.HorzAlignment.Near),
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("CONNome", "CON Nome", 62, DevExpress.Utils.FormatType.None, "", true, DevExpress.Utils.HorzAlignment.Near)});
            this.LookUpCond.DataSource = this.cONDOMINIOSBindingSource;
            this.LookUpCond.DisplayMember = "CONCodigo";
            this.LookUpCond.Name = "LookUpCond";
            this.LookUpCond.ShowHeader = false;
            this.LookUpCond.ValueMember = "CON";
            // 
            // cONDOMINIOSBindingSource
            // 
            this.cONDOMINIOSBindingSource.DataMember = "CONDOMINIOS";
            this.cONDOMINIOSBindingSource.DataSource = typeof(FrameworkProc.datasets.dCondominiosAtivos);
            // 
            // colAGBDataInicio
            // 
            this.colAGBDataInicio.Caption = "Data";
            this.colAGBDataInicio.FieldName = "AGBDataInicio";
            this.colAGBDataInicio.Name = "colAGBDataInicio";
            this.colAGBDataInicio.OptionsColumn.AllowEdit = false;
            this.colAGBDataInicio.OptionsColumn.ReadOnly = true;
            this.colAGBDataInicio.Visible = true;
            this.colAGBDataInicio.VisibleIndex = 3;
            this.colAGBDataInicio.Width = 164;
            // 
            // colAGBDataTermino
            // 
            this.colAGBDataTermino.Caption = "T�rmino";
            this.colAGBDataTermino.FieldName = "AGBDataTermino";
            this.colAGBDataTermino.Name = "colAGBDataTermino";
            this.colAGBDataTermino.OptionsColumn.AllowEdit = false;
            this.colAGBDataTermino.OptionsColumn.ReadOnly = true;
            // 
            // colAGB_TAG
            // 
            this.colAGB_TAG.Caption = "Tipo";
            this.colAGB_TAG.ColumnEdit = this.LookUpTAG;
            this.colAGB_TAG.FieldName = "AGB_TAG";
            this.colAGB_TAG.Name = "colAGB_TAG";
            this.colAGB_TAG.OptionsColumn.AllowEdit = false;
            this.colAGB_TAG.OptionsColumn.ReadOnly = true;
            this.colAGB_TAG.Visible = true;
            this.colAGB_TAG.VisibleIndex = 4;
            this.colAGB_TAG.Width = 145;
            // 
            // LookUpTAG
            // 
            this.LookUpTAG.AutoHeight = false;
            this.LookUpTAG.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LookUpTAG.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("TAGTitulo", "TAG Titulo", 72, DevExpress.Utils.FormatType.None, "", true, DevExpress.Utils.HorzAlignment.Near)});
            this.LookUpTAG.DataSource = this.tipoAGendamentoBindingSource;
            this.LookUpTAG.DisplayMember = "TAGTitulo";
            this.LookUpTAG.Name = "LookUpTAG";
            this.LookUpTAG.ShowHeader = false;
            this.LookUpTAG.ValueMember = "TAG";
            // 
            // tipoAGendamentoBindingSource
            // 
            this.tipoAGendamentoBindingSource.DataMember = "TipoAGendamento";
            this.tipoAGendamentoBindingSource.DataSource = this.dAgendaBaseGrade;
            // 
            // colAGBDescricao
            // 
            this.colAGBDescricao.Caption = "Descri��o";
            this.colAGBDescricao.FieldName = "AGBDescricao";
            this.colAGBDescricao.Name = "colAGBDescricao";
            this.colAGBDescricao.OptionsColumn.AllowEdit = false;
            this.colAGBDescricao.OptionsColumn.ReadOnly = true;
            this.colAGBDescricao.Visible = true;
            this.colAGBDescricao.VisibleIndex = 5;
            this.colAGBDescricao.Width = 164;
            // 
            // colAGBSubDescricao
            // 
            this.colAGBSubDescricao.Caption = "Detalhe";
            this.colAGBSubDescricao.FieldName = "AGBSubDescricao";
            this.colAGBSubDescricao.Name = "colAGBSubDescricao";
            this.colAGBSubDescricao.OptionsColumn.AllowEdit = false;
            this.colAGBSubDescricao.OptionsColumn.ReadOnly = true;
            // 
            // colAGB_USU
            // 
            this.colAGB_USU.Caption = "Respons�vel";
            this.colAGB_USU.ColumnEdit = this.LookUpUSU;
            this.colAGB_USU.FieldName = "AGB_USU";
            this.colAGB_USU.Name = "colAGB_USU";
            this.colAGB_USU.OptionsColumn.AllowEdit = false;
            this.colAGB_USU.OptionsColumn.ReadOnly = true;
            this.colAGB_USU.Visible = true;
            this.colAGB_USU.VisibleIndex = 6;
            this.colAGB_USU.Width = 190;
            // 
            // LookUpUSU
            // 
            this.LookUpUSU.AutoHeight = false;
            this.LookUpUSU.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LookUpUSU.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("USUNome", "USU Nome", 60, DevExpress.Utils.FormatType.None, "", true, DevExpress.Utils.HorzAlignment.Near)});
            this.LookUpUSU.DataSource = this.uSUARIOSBindingSource;
            this.LookUpUSU.DisplayMember = "USUNome";
            this.LookUpUSU.Name = "LookUpUSU";
            this.LookUpUSU.ShowHeader = false;
            this.LookUpUSU.ValueMember = "USU";
            // 
            // uSUARIOSBindingSource
            // 
            this.uSUARIOSBindingSource.DataMember = "USUarios";
            this.uSUARIOSBindingSource.DataSource = typeof(FrameworkProc.datasets.dUSUarios);
            // 
            // colAGBDataLimite
            // 
            this.colAGBDataLimite.Caption = "Data Limite";
            this.colAGBDataLimite.FieldName = "AGBDataLimite";
            this.colAGBDataLimite.Name = "colAGBDataLimite";
            this.colAGBDataLimite.OptionsColumn.AllowEdit = false;
            this.colAGBDataLimite.OptionsColumn.ReadOnly = true;
            // 
            // colAGBDATAA
            // 
            this.colAGBDATAA.Caption = "Altera��o";
            this.colAGBDATAA.DisplayFormat.FormatString = "dd/mm/yyyy HH:MM";
            this.colAGBDATAA.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.colAGBDATAA.FieldName = "AGBDATAA";
            this.colAGBDATAA.Name = "colAGBDATAA";
            this.colAGBDATAA.OptionsColumn.AllowEdit = false;
            this.colAGBDATAA.OptionsColumn.ReadOnly = true;
            // 
            // colAGBDATAI
            // 
            this.colAGBDATAI.Caption = "Inclus�o";
            this.colAGBDATAI.DisplayFormat.FormatString = "dd/mm/yyyy HH:MM";
            this.colAGBDATAI.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.colAGBDATAI.FieldName = "AGBDATAI";
            this.colAGBDATAI.Name = "colAGBDATAI";
            this.colAGBDATAI.OptionsColumn.AllowEdit = false;
            this.colAGBDATAI.OptionsColumn.ReadOnly = true;
            // 
            // repositoryItemImageComboBox1
            // 
            this.repositoryItemImageComboBox1.AutoHeight = false;
            this.repositoryItemImageComboBox1.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemImageComboBox1.Name = "repositoryItemImageComboBox1";
            // 
            // colAGBStatus
            // 
            this.colAGBStatus.Caption = "Status";
            this.colAGBStatus.ColumnEdit = this.repositoryItemImageComboBox1;
            this.colAGBStatus.FieldName = "AGBStatus";
            this.colAGBStatus.Name = "colAGBStatus";
            this.colAGBStatus.OptionsColumn.AllowEdit = false;
            this.colAGBStatus.OptionsColumn.ReadOnly = true;
            this.colAGBStatus.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Count, "AGBStatus", "{0:n0}")});
            this.colAGBStatus.Visible = true;
            this.colAGBStatus.VisibleIndex = 0;
            this.colAGBStatus.Width = 87;
            // 
            // simpleButton1
            // 
            this.simpleButton1.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.simpleButton1.Appearance.Options.UseFont = true;
            this.simpleButton1.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("simpleButton1.ImageOptions.Image")));
            this.simpleButton1.Location = new System.Drawing.Point(339, 5);
            this.simpleButton1.Name = "simpleButton1";
            this.simpleButton1.Size = new System.Drawing.Size(114, 40);
            this.simpleButton1.TabIndex = 1;
            this.simpleButton1.Text = "Calcular";
            this.simpleButton1.Click += new System.EventHandler(this.simpleButton1_Click);
            // 
            // simpleButton2
            // 
            this.simpleButton2.Appearance.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.simpleButton2.Appearance.Options.UseFont = true;
            this.simpleButton2.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("simpleButton2.ImageOptions.Image")));
            this.simpleButton2.Location = new System.Drawing.Point(487, 5);
            this.simpleButton2.Name = "simpleButton2";
            this.simpleButton2.Size = new System.Drawing.Size(161, 40);
            this.simpleButton2.TabIndex = 2;
            this.simpleButton2.Text = "DOCUMENTA��O";
            this.simpleButton2.Click += new System.EventHandler(this.simpleButton2_Click);
            // 
            // colGerente
            // 
            this.colGerente.ColumnEdit = this.LookUpUSU;
            this.colGerente.FieldName = "Gerente";
            this.colGerente.Name = "colGerente";
            this.colGerente.OptionsColumn.AllowEdit = false;
            this.colGerente.OptionsColumn.ReadOnly = true;
            this.colGerente.Visible = true;
            this.colGerente.VisibleIndex = 1;
            this.colGerente.Width = 89;
            // 
            // cChamada1
            // 
            this.cChamada1.CaixaAltaGeral = true;
            this.cChamada1.Doc = System.Windows.Forms.DockStyle.None;
            this.cChamada1.Estado = CompontesBasicos.EstadosDosComponentes.Registrado;
            this.cChamada1.Location = new System.Drawing.Point(764, 542);
            this.cChamada1.LookAndFeel.UseDefaultLookAndFeel = false;
            this.cChamada1.Name = "cChamada1";
            this.cChamada1.Size = new System.Drawing.Size(636, 356);
            this.cChamada1.somenteleitura = false;
            this.cChamada1.TabIndex = 6;
            this.cChamada1.Titulo = "Calendario.Zoom.cChamada - Calendario.Zoom.cChamada - ";
            // 
            // colAGB
            // 
            this.colAGB.Caption = "AGB";
            this.colAGB.FieldName = "AGB";
            this.colAGB.Name = "colAGB";
            this.colAGB.OptionsColumn.AllowEdit = false;
            this.colAGB.OptionsColumn.ReadOnly = true;
            // 
            // gridColumnLinques
            // 
            this.gridColumnLinques.Caption = "Botoes";
            this.gridColumnLinques.Fixed = DevExpress.XtraGrid.Columns.FixedStyle.Right;
            this.gridColumnLinques.Name = "gridColumnLinques";
            this.gridColumnLinques.OptionsColumn.FixedWidth = true;
            this.gridColumnLinques.OptionsColumn.ShowCaption = false;
            this.gridColumnLinques.Visible = true;
            this.gridColumnLinques.VisibleIndex = 7;
            this.gridColumnLinques.Width = 36;
            // 
            // Linques
            // 
            editorButtonImageOptions1.Image = global::Calendario.Properties.Resources.perBr1;
            this.Linques.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(editorButtonImageOptions1, DevExpress.XtraEditors.Controls.ButtonPredefines.Glyph, null)});
            this.Linques.Name = "Linques";
            this.Linques.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.HideTextEditor;
            this.Linques.ButtonClick += new DevExpress.XtraEditors.Controls.ButtonPressedEventHandler(this.Linques_ButtonClick);
            // 
            // gridColumn1
            // 
            this.gridColumn1.Caption = "gridColumn1";
            this.gridColumn1.ColumnEdit = this.repositoryItemButtonEdit1;
            this.gridColumn1.Fixed = DevExpress.XtraGrid.Columns.FixedStyle.Right;
            this.gridColumn1.Name = "gridColumn1";
            this.gridColumn1.OptionsColumn.FixedWidth = true;
            this.gridColumn1.OptionsColumn.ShowCaption = false;
            this.gridColumn1.Visible = true;
            this.gridColumn1.VisibleIndex = 8;
            this.gridColumn1.Width = 26;
            // 
            // repositoryItemButtonEdit1
            // 
            this.repositoryItemButtonEdit1.AutoHeight = false;
            this.repositoryItemButtonEdit1.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.repositoryItemButtonEdit1.Name = "repositoryItemButtonEdit1";
            this.repositoryItemButtonEdit1.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.HideTextEditor;
            this.repositoryItemButtonEdit1.ButtonClick += new DevExpress.XtraEditors.Controls.ButtonPressedEventHandler(this.repositoryItemButtonEdit1_ButtonClick);
            // 
            // simpleButton3
            // 
            this.simpleButton3.Location = new System.Drawing.Point(682, 6);
            this.simpleButton3.Name = "simpleButton3";
            this.simpleButton3.Size = new System.Drawing.Size(105, 39);
            this.simpleButton3.TabIndex = 3;
            this.simpleButton3.Text = "Revisar";
            this.simpleButton3.Click += new System.EventHandler(this.simpleButton3_Click);
            // 
            // cAgendaGrade
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BindingSourcePrincipal = this.aGendaBaseBindingSource;
            this.ComponenteCampos = typeof(Calendario.Agenda.cAgendaCampos);
            this.Controls.Add(this.cChamada1);
            this.EstadoComponenteCampos = CompontesBasicos.EstadosDosComponentes.PopUp;
            this.LookAndFeel.UseDefaultLookAndFeel = false;
            this.Name = "cAgendaGrade";
            this.Size = new System.Drawing.Size(1396, 807);
            this.Titulo = "Framework.ComponentesNeon.NavegadorCondominio - Framework.ComponentesNeon.Navegad" +
    "orCondominio - Agendamentos";
            this.CondominioAlterado += new System.EventHandler(this.cAgendaGrade_CondominioAlterado);
            this.Load += new System.EventHandler(this.cAgendaGrade_Load);
            this.Controls.SetChildIndex(this.cChamada1, 0);
            this.Controls.SetChildIndex(this.panelControl1, 0);
            this.Controls.SetChildIndex(this.GridControl_F, 0);
            ((System.ComponentModel.ISupportInitialize)(this.panelControl1)).EndInit();
            this.panelControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.PopImprimir_F)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.BarManager_F)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.PopExportar_F)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.GridControl_F)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.GridView_F)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.BindingSource_F)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.StyleController_F)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.aGendaBaseBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dAgendaBaseGrade)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LookUpCond)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cONDOMINIOSBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LookUpTAG)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tipoAGendamentoBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LookUpUSU)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uSUARIOSBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemImageComboBox1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Linques)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemButtonEdit1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.BindingSource aGendaBaseBindingSource;
        private DevExpress.XtraGrid.Columns.GridColumn colAGB_CON;
        private DevExpress.XtraGrid.Columns.GridColumn colAGBDataInicio;
        private DevExpress.XtraGrid.Columns.GridColumn colAGBDataTermino;
        private DevExpress.XtraGrid.Columns.GridColumn colAGB_TAG;
        private DevExpress.XtraGrid.Columns.GridColumn colAGBDescricao;
        private DevExpress.XtraGrid.Columns.GridColumn colAGBSubDescricao;
        private DevExpress.XtraGrid.Columns.GridColumn colAGB_USU;
        private DevExpress.XtraGrid.Columns.GridColumn colAGBDataLimite;
        private DevExpress.XtraGrid.Columns.GridColumn colAGBDATAA;
        private DevExpress.XtraGrid.Columns.GridColumn colAGBDATAI;
        private System.Windows.Forms.BindingSource uSUARIOSBindingSource;
        private DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit LookUpUSU;
        private DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit LookUpTAG;
        private System.Windows.Forms.BindingSource tipoAGendamentoBindingSource;
        private DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit LookUpCond;
        private System.Windows.Forms.BindingSource cONDOMINIOSBindingSource;
        private DevExpress.XtraEditors.Repository.RepositoryItemImageComboBox repositoryItemImageComboBox1;
        private DevExpress.XtraGrid.Columns.GridColumn colAGBStatus;
        private DevExpress.XtraEditors.SimpleButton simpleButton1;
        private DevExpress.XtraEditors.SimpleButton simpleButton2;
        private DevExpress.XtraGrid.Columns.GridColumn colGerente;
        private Zoom.cChamada cChamada1;
        private DevExpress.XtraGrid.Columns.GridColumn colAGB;
        private DevExpress.XtraEditors.Repository.RepositoryItemButtonEdit Linques;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumnLinques;
        private DevExpress.XtraEditors.Repository.RepositoryItemButtonEdit repositoryItemButtonEdit1;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn1;
        private DevExpress.XtraEditors.SimpleButton simpleButton3;
        private dAgendaBaseGrade dAgendaBaseGrade;
    }
}
