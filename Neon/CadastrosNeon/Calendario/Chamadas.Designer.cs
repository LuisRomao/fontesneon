namespace Calendario
{
    partial class Chamadas
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject1 = new DevExpress.Utils.SerializableAppearanceObject();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Chamadas));
            this.panelControl2 = new DevExpress.XtraEditors.PanelControl();
            this.lookupCondominio_F1 = new Framework.Lookup.LookupCondominio_F();
            this.chMeus = new DevExpress.XtraEditors.CheckEdit();
            this.chAbertos = new DevExpress.XtraEditors.CheckEdit();
            this.gridControl1 = new DevExpress.XtraGrid.GridControl();
            this.dAgendaBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.advBandedGridView1 = new DevExpress.XtraGrid.Views.BandedGrid.AdvBandedGridView();
            this.gridBand4 = new DevExpress.XtraGrid.Views.BandedGrid.GridBand();
            this.colHSTAssunto = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.buscaAssunto = new DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit();
            this.aSSuntoBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.colHST_CON = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.BuscaCON = new DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit();
            this.cONDOMINIOSBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.colHSTCritico = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.chCritico = new DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit();
            this.gridBand1 = new DevExpress.XtraGrid.Views.BandedGrid.GridBand();
            this.colHSTDescricao = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.repositoryItemMemoEdit1 = new DevExpress.XtraEditors.Repository.RepositoryItemMemoEdit();
            this.bandedGridColumn2 = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.BotaoPostar = new DevExpress.XtraEditors.Repository.RepositoryItemButtonEdit();
            this.gridBand2 = new DevExpress.XtraGrid.Views.BandedGrid.GridBand();
            this.colHSTResponsavel_USU = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.BuscaUSU = new DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit();
            this.uSUARIOSBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.colHSTData = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.colHSTConcluido = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.chConcluido = new DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit();
            this.gridBand3 = new DevExpress.XtraGrid.Views.BandedGrid.GridBand();
            this.colHSTA_USU = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.colHSTADATA = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.Agendadata = new DevExpress.XtraEditors.Repository.RepositoryItemDateEdit();
            this.colHSTAceito = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.chAceito = new DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit();
            this.timer1 = new System.Windows.Forms.Timer(this.components);
            this.notifyIcon1 = new System.Windows.Forms.NotifyIcon(this.components);
            this.notifyIcon2 = new System.Windows.Forms.NotifyIcon(this.components);
            ((System.ComponentModel.ISupportInitialize)(this.StyleController_F)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl2)).BeginInit();
            this.panelControl2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.chMeus.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chAbertos.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dAgendaBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.advBandedGridView1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.buscaAssunto)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.aSSuntoBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.BuscaCON)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cONDOMINIOSBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chCritico)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoEdit1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.BotaoPostar)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.BuscaUSU)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uSUARIOSBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chConcluido)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Agendadata)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Agendadata.CalendarTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chAceito)).BeginInit();
            this.SuspendLayout();
            // 
            // ToolTipController_F
            // 
            this.ToolTipController_F.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.ToolTipController_F.Appearance.ForeColor = System.Drawing.Color.Black;
            this.ToolTipController_F.Appearance.Options.UseBackColor = true;
            this.ToolTipController_F.Appearance.Options.UseFont = true;
            this.ToolTipController_F.Appearance.Options.UseForeColor = true;
            this.ToolTipController_F.Appearance.Options.UseTextOptions = true;
            this.ToolTipController_F.Appearance.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            // 
            // StyleController_F
            // 
            this.StyleController_F.AppearanceDisabled.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this.StyleController_F.AppearanceDisabled.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this.StyleController_F.AppearanceDisabled.Options.UseBackColor = true;
            this.StyleController_F.AppearanceDisabled.Options.UseForeColor = true;
            this.StyleController_F.AppearanceFocused.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.StyleController_F.AppearanceFocused.Options.UseBackColor = true;
            // 
            // panelControl2
            // 
            this.panelControl2.Controls.Add(this.lookupCondominio_F1);
            this.panelControl2.Controls.Add(this.chMeus);
            this.panelControl2.Controls.Add(this.chAbertos);
            this.panelControl2.Dock = System.Windows.Forms.DockStyle.Top;
            this.panelControl2.Location = new System.Drawing.Point(0, 0);
            this.panelControl2.Name = "panelControl2";
            this.panelControl2.Size = new System.Drawing.Size(1545, 31);
            this.panelControl2.TabIndex = 8;
            // 
            // lookupCondominio_F1
            // 
            this.lookupCondominio_F1.Autofill = true;
            this.lookupCondominio_F1.CaixaAltaGeral = true;
            this.lookupCondominio_F1.CON_sel = -1;
            this.lookupCondominio_F1.CON_selrow = null;
            this.lookupCondominio_F1.Cursor = System.Windows.Forms.Cursors.Default;
            this.lookupCondominio_F1.Doc = System.Windows.Forms.DockStyle.None;
            this.lookupCondominio_F1.Estado = CompontesBasicos.EstadosDosComponentes.Registrado;
            this.lookupCondominio_F1.LinhaMae_F = null;
            this.lookupCondominio_F1.Location = new System.Drawing.Point(293, 8);
            this.lookupCondominio_F1.LookAndFeel.UseDefaultLookAndFeel = false;
            this.lookupCondominio_F1.Name = "lookupCondominio_F1";
            this.lookupCondominio_F1.NivelCONOculto = 2;
            this.lookupCondominio_F1.Size = new System.Drawing.Size(308, 20);
            this.lookupCondominio_F1.somenteleitura = false;
            this.lookupCondominio_F1.TabIndex = 2;
            this.lookupCondominio_F1.TableAdapterPrincipal = null;
            this.lookupCondominio_F1.Titulo = null;
            this.lookupCondominio_F1.alterado += new System.EventHandler(this.lookupCondominio_F1_alterado);
            // 
            // chMeus
            // 
            this.chMeus.EditValue = true;
            this.chMeus.Location = new System.Drawing.Point(166, 6);
            this.chMeus.Name = "chMeus";
            this.chMeus.Properties.Caption = "Somente os meus";
            this.chMeus.Size = new System.Drawing.Size(121, 19);
            this.chMeus.TabIndex = 1;
            this.chMeus.CheckedChanged += new System.EventHandler(this.chAbertos_CheckedChanged);
            // 
            // chAbertos
            // 
            this.chAbertos.EditValue = true;
            this.chAbertos.Location = new System.Drawing.Point(6, 6);
            this.chAbertos.Name = "chAbertos";
            this.chAbertos.Properties.Caption = "Somene Abertos";
            this.chAbertos.Size = new System.Drawing.Size(110, 19);
            this.chAbertos.TabIndex = 0;
            this.chAbertos.CheckedChanged += new System.EventHandler(this.chAbertos_CheckedChanged);
            // 
            // gridControl1
            // 
            this.gridControl1.DataMember = "HISTORICOS";
            this.gridControl1.DataSource = this.dAgendaBindingSource;
            this.gridControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridControl1.Location = new System.Drawing.Point(0, 31);
            this.gridControl1.MainView = this.advBandedGridView1;
            this.gridControl1.Name = "gridControl1";
            this.gridControl1.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemMemoEdit1,
            this.BuscaUSU,
            this.BotaoPostar,
            this.chCritico,
            this.chConcluido,
            this.chAceito,
            this.Agendadata,
            this.buscaAssunto,
            this.BuscaCON});
            this.gridControl1.Size = new System.Drawing.Size(1545, 566);
            this.gridControl1.TabIndex = 9;
            this.gridControl1.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.advBandedGridView1});
            this.gridControl1.DoubleClick += new System.EventHandler(this.gridControl1_DoubleClick);
            // 
            // dAgendaBindingSource
            // 
            this.dAgendaBindingSource.DataSource = typeof(Calendario.dAgenda);
            this.dAgendaBindingSource.Position = 0;
            // 
            // advBandedGridView1
            // 
            this.advBandedGridView1.Appearance.BandPanel.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(240)))), ((int)(((byte)(177)))), ((int)(((byte)(94)))));
            this.advBandedGridView1.Appearance.BandPanel.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(240)))), ((int)(((byte)(177)))), ((int)(((byte)(94)))));
            this.advBandedGridView1.Appearance.BandPanel.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Bold);
            this.advBandedGridView1.Appearance.BandPanel.ForeColor = System.Drawing.Color.White;
            this.advBandedGridView1.Appearance.BandPanel.Options.UseBackColor = true;
            this.advBandedGridView1.Appearance.BandPanel.Options.UseBorderColor = true;
            this.advBandedGridView1.Appearance.BandPanel.Options.UseFont = true;
            this.advBandedGridView1.Appearance.BandPanel.Options.UseForeColor = true;
            this.advBandedGridView1.Appearance.BandPanelBackground.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(251)))), ((int)(((byte)(244)))), ((int)(((byte)(232)))));
            this.advBandedGridView1.Appearance.BandPanelBackground.BackColor2 = System.Drawing.Color.White;
            this.advBandedGridView1.Appearance.BandPanelBackground.Options.UseBackColor = true;
            this.advBandedGridView1.Appearance.ColumnFilterButton.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(240)))), ((int)(((byte)(177)))), ((int)(((byte)(94)))));
            this.advBandedGridView1.Appearance.ColumnFilterButton.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(240)))), ((int)(((byte)(177)))), ((int)(((byte)(94)))));
            this.advBandedGridView1.Appearance.ColumnFilterButton.ForeColor = System.Drawing.Color.Black;
            this.advBandedGridView1.Appearance.ColumnFilterButton.Options.UseBackColor = true;
            this.advBandedGridView1.Appearance.ColumnFilterButton.Options.UseBorderColor = true;
            this.advBandedGridView1.Appearance.ColumnFilterButton.Options.UseForeColor = true;
            this.advBandedGridView1.Appearance.ColumnFilterButtonActive.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(248)))), ((int)(((byte)(204)))), ((int)(((byte)(124)))));
            this.advBandedGridView1.Appearance.ColumnFilterButtonActive.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(248)))), ((int)(((byte)(204)))), ((int)(((byte)(124)))));
            this.advBandedGridView1.Appearance.ColumnFilterButtonActive.ForeColor = System.Drawing.Color.Black;
            this.advBandedGridView1.Appearance.ColumnFilterButtonActive.Options.UseBackColor = true;
            this.advBandedGridView1.Appearance.ColumnFilterButtonActive.Options.UseBorderColor = true;
            this.advBandedGridView1.Appearance.ColumnFilterButtonActive.Options.UseForeColor = true;
            this.advBandedGridView1.Appearance.Empty.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(251)))), ((int)(((byte)(244)))), ((int)(((byte)(232)))));
            this.advBandedGridView1.Appearance.Empty.BackColor2 = System.Drawing.Color.White;
            this.advBandedGridView1.Appearance.Empty.Options.UseBackColor = true;
            this.advBandedGridView1.Appearance.EvenRow.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(253)))), ((int)(((byte)(248)))), ((int)(((byte)(236)))));
            this.advBandedGridView1.Appearance.EvenRow.ForeColor = System.Drawing.Color.Black;
            this.advBandedGridView1.Appearance.EvenRow.Options.UseBackColor = true;
            this.advBandedGridView1.Appearance.EvenRow.Options.UseForeColor = true;
            this.advBandedGridView1.Appearance.FilterCloseButton.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(240)))), ((int)(((byte)(177)))), ((int)(((byte)(94)))));
            this.advBandedGridView1.Appearance.FilterCloseButton.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(240)))), ((int)(((byte)(177)))), ((int)(((byte)(94)))));
            this.advBandedGridView1.Appearance.FilterCloseButton.ForeColor = System.Drawing.Color.Black;
            this.advBandedGridView1.Appearance.FilterCloseButton.Options.UseBackColor = true;
            this.advBandedGridView1.Appearance.FilterCloseButton.Options.UseBorderColor = true;
            this.advBandedGridView1.Appearance.FilterCloseButton.Options.UseForeColor = true;
            this.advBandedGridView1.Appearance.FilterPanel.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(251)))), ((int)(((byte)(244)))), ((int)(((byte)(232)))));
            this.advBandedGridView1.Appearance.FilterPanel.BackColor2 = System.Drawing.Color.White;
            this.advBandedGridView1.Appearance.FilterPanel.ForeColor = System.Drawing.Color.Black;
            this.advBandedGridView1.Appearance.FilterPanel.Options.UseBackColor = true;
            this.advBandedGridView1.Appearance.FilterPanel.Options.UseForeColor = true;
            this.advBandedGridView1.Appearance.FixedLine.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(218)))), ((int)(((byte)(153)))), ((int)(((byte)(73)))));
            this.advBandedGridView1.Appearance.FixedLine.Options.UseBackColor = true;
            this.advBandedGridView1.Appearance.FocusedCell.BackColor = System.Drawing.Color.White;
            this.advBandedGridView1.Appearance.FocusedCell.ForeColor = System.Drawing.Color.Black;
            this.advBandedGridView1.Appearance.FocusedCell.Options.UseBackColor = true;
            this.advBandedGridView1.Appearance.FocusedCell.Options.UseForeColor = true;
            this.advBandedGridView1.Appearance.FocusedRow.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(217)))), ((int)(((byte)(154)))), ((int)(((byte)(91)))));
            this.advBandedGridView1.Appearance.FocusedRow.ForeColor = System.Drawing.Color.White;
            this.advBandedGridView1.Appearance.FocusedRow.Options.UseBackColor = true;
            this.advBandedGridView1.Appearance.FocusedRow.Options.UseForeColor = true;
            this.advBandedGridView1.Appearance.FooterPanel.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(240)))), ((int)(((byte)(177)))), ((int)(((byte)(94)))));
            this.advBandedGridView1.Appearance.FooterPanel.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(240)))), ((int)(((byte)(177)))), ((int)(((byte)(94)))));
            this.advBandedGridView1.Appearance.FooterPanel.ForeColor = System.Drawing.Color.Black;
            this.advBandedGridView1.Appearance.FooterPanel.Options.UseBackColor = true;
            this.advBandedGridView1.Appearance.FooterPanel.Options.UseBorderColor = true;
            this.advBandedGridView1.Appearance.FooterPanel.Options.UseForeColor = true;
            this.advBandedGridView1.Appearance.GroupButton.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(248)))), ((int)(((byte)(204)))), ((int)(((byte)(124)))));
            this.advBandedGridView1.Appearance.GroupButton.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(248)))), ((int)(((byte)(204)))), ((int)(((byte)(124)))));
            this.advBandedGridView1.Appearance.GroupButton.Options.UseBackColor = true;
            this.advBandedGridView1.Appearance.GroupButton.Options.UseBorderColor = true;
            this.advBandedGridView1.Appearance.GroupFooter.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(248)))), ((int)(((byte)(204)))), ((int)(((byte)(124)))));
            this.advBandedGridView1.Appearance.GroupFooter.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(248)))), ((int)(((byte)(204)))), ((int)(((byte)(124)))));
            this.advBandedGridView1.Appearance.GroupFooter.ForeColor = System.Drawing.Color.Black;
            this.advBandedGridView1.Appearance.GroupFooter.Options.UseBackColor = true;
            this.advBandedGridView1.Appearance.GroupFooter.Options.UseBorderColor = true;
            this.advBandedGridView1.Appearance.GroupFooter.Options.UseForeColor = true;
            this.advBandedGridView1.Appearance.GroupPanel.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(251)))), ((int)(((byte)(244)))), ((int)(((byte)(232)))));
            this.advBandedGridView1.Appearance.GroupPanel.BackColor2 = System.Drawing.Color.White;
            this.advBandedGridView1.Appearance.GroupPanel.ForeColor = System.Drawing.Color.Black;
            this.advBandedGridView1.Appearance.GroupPanel.Options.UseBackColor = true;
            this.advBandedGridView1.Appearance.GroupPanel.Options.UseForeColor = true;
            this.advBandedGridView1.Appearance.GroupRow.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(248)))), ((int)(((byte)(204)))), ((int)(((byte)(124)))));
            this.advBandedGridView1.Appearance.GroupRow.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(248)))), ((int)(((byte)(204)))), ((int)(((byte)(124)))));
            this.advBandedGridView1.Appearance.GroupRow.ForeColor = System.Drawing.Color.Black;
            this.advBandedGridView1.Appearance.GroupRow.Options.UseBackColor = true;
            this.advBandedGridView1.Appearance.GroupRow.Options.UseBorderColor = true;
            this.advBandedGridView1.Appearance.GroupRow.Options.UseForeColor = true;
            this.advBandedGridView1.Appearance.HeaderPanel.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(248)))), ((int)(((byte)(204)))), ((int)(((byte)(124)))));
            this.advBandedGridView1.Appearance.HeaderPanel.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(248)))), ((int)(((byte)(204)))), ((int)(((byte)(124)))));
            this.advBandedGridView1.Appearance.HeaderPanel.ForeColor = System.Drawing.Color.Black;
            this.advBandedGridView1.Appearance.HeaderPanel.Options.UseBackColor = true;
            this.advBandedGridView1.Appearance.HeaderPanel.Options.UseBorderColor = true;
            this.advBandedGridView1.Appearance.HeaderPanel.Options.UseForeColor = true;
            this.advBandedGridView1.Appearance.HeaderPanelBackground.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(251)))), ((int)(((byte)(244)))), ((int)(((byte)(232)))));
            this.advBandedGridView1.Appearance.HeaderPanelBackground.BackColor2 = System.Drawing.Color.White;
            this.advBandedGridView1.Appearance.HeaderPanelBackground.Options.UseBackColor = true;
            this.advBandedGridView1.Appearance.HideSelectionRow.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(225)))), ((int)(((byte)(183)))), ((int)(((byte)(125)))));
            this.advBandedGridView1.Appearance.HideSelectionRow.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(251)))), ((int)(((byte)(244)))), ((int)(((byte)(232)))));
            this.advBandedGridView1.Appearance.HideSelectionRow.Options.UseBackColor = true;
            this.advBandedGridView1.Appearance.HideSelectionRow.Options.UseForeColor = true;
            this.advBandedGridView1.Appearance.HorzLine.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(240)))), ((int)(((byte)(177)))), ((int)(((byte)(94)))));
            this.advBandedGridView1.Appearance.HorzLine.Options.UseBackColor = true;
            this.advBandedGridView1.Appearance.OddRow.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(251)))), ((int)(((byte)(236)))), ((int)(((byte)(208)))));
            this.advBandedGridView1.Appearance.OddRow.ForeColor = System.Drawing.Color.Black;
            this.advBandedGridView1.Appearance.OddRow.Options.UseBackColor = true;
            this.advBandedGridView1.Appearance.OddRow.Options.UseForeColor = true;
            this.advBandedGridView1.Appearance.Preview.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(254)))), ((int)(((byte)(249)))));
            this.advBandedGridView1.Appearance.Preview.Font = new System.Drawing.Font("Verdana", 7.5F);
            this.advBandedGridView1.Appearance.Preview.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(186)))), ((int)(((byte)(146)))), ((int)(((byte)(78)))));
            this.advBandedGridView1.Appearance.Preview.Options.UseBackColor = true;
            this.advBandedGridView1.Appearance.Preview.Options.UseFont = true;
            this.advBandedGridView1.Appearance.Preview.Options.UseForeColor = true;
            this.advBandedGridView1.Appearance.Row.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(253)))), ((int)(((byte)(248)))), ((int)(((byte)(236)))));
            this.advBandedGridView1.Appearance.Row.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(253)))), ((int)(((byte)(248)))), ((int)(((byte)(236)))));
            this.advBandedGridView1.Appearance.Row.ForeColor = System.Drawing.Color.Black;
            this.advBandedGridView1.Appearance.Row.Options.UseBackColor = true;
            this.advBandedGridView1.Appearance.Row.Options.UseBorderColor = true;
            this.advBandedGridView1.Appearance.Row.Options.UseForeColor = true;
            this.advBandedGridView1.Appearance.RowSeparator.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(251)))), ((int)(((byte)(244)))), ((int)(((byte)(232)))));
            this.advBandedGridView1.Appearance.RowSeparator.BackColor2 = System.Drawing.Color.White;
            this.advBandedGridView1.Appearance.RowSeparator.Options.UseBackColor = true;
            this.advBandedGridView1.Appearance.SelectedRow.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(231)))), ((int)(((byte)(167)))), ((int)(((byte)(103)))));
            this.advBandedGridView1.Appearance.SelectedRow.Options.UseBackColor = true;
            this.advBandedGridView1.Appearance.TopNewRow.BackColor = System.Drawing.Color.White;
            this.advBandedGridView1.Appearance.TopNewRow.Options.UseBackColor = true;
            this.advBandedGridView1.Appearance.VertLine.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(240)))), ((int)(((byte)(177)))), ((int)(((byte)(94)))));
            this.advBandedGridView1.Appearance.VertLine.Options.UseBackColor = true;
            this.advBandedGridView1.BandPanelRowHeight = 0;
            this.advBandedGridView1.Bands.AddRange(new DevExpress.XtraGrid.Views.BandedGrid.GridBand[] {
            this.gridBand4,
            this.gridBand1,
            this.gridBand2,
            this.gridBand3});
            this.advBandedGridView1.Columns.AddRange(new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn[] {
            this.colHSTAssunto,
            this.colHSTDescricao,
            this.colHSTCritico,
            this.colHSTData,
            this.colHSTResponsavel_USU,
            this.colHSTA_USU,
            this.colHSTADATA,
            this.colHSTAceito,
            this.colHSTConcluido,
            this.bandedGridColumn2,
            this.colHST_CON});
            this.advBandedGridView1.GridControl = this.gridControl1;
            this.advBandedGridView1.Name = "advBandedGridView1";
            this.advBandedGridView1.NewItemRowText = "Incluir nova linha";
            this.advBandedGridView1.OptionsCustomization.AllowChangeColumnParent = true;
            this.advBandedGridView1.OptionsCustomization.AllowRowSizing = true;
            this.advBandedGridView1.OptionsCustomization.ShowBandsInCustomizationForm = false;
            this.advBandedGridView1.OptionsMenu.EnableColumnMenu = false;
            this.advBandedGridView1.OptionsSelection.EnableAppearanceFocusedRow = false;
            this.advBandedGridView1.OptionsView.ColumnAutoWidth = true;
            this.advBandedGridView1.OptionsView.EnableAppearanceEvenRow = true;
            this.advBandedGridView1.OptionsView.EnableAppearanceOddRow = true;
            this.advBandedGridView1.OptionsView.NewItemRowPosition = DevExpress.XtraGrid.Views.Grid.NewItemRowPosition.Bottom;
            this.advBandedGridView1.OptionsView.ShowGroupPanel = false;
            this.advBandedGridView1.RowHeight = 30;
            this.advBandedGridView1.RowSeparatorHeight = 10;
            this.advBandedGridView1.RowCellStyle += new DevExpress.XtraGrid.Views.Grid.RowCellStyleEventHandler(this.advBandedGridView1_RowCellStyle);
            this.advBandedGridView1.InitNewRow += new DevExpress.XtraGrid.Views.Grid.InitNewRowEventHandler(this.gridView1_InitNewRow);
            this.advBandedGridView1.FocusedRowChanged += new DevExpress.XtraGrid.Views.Base.FocusedRowChangedEventHandler(this.advBandedGridView1_FocusedRowChanged);
            this.advBandedGridView1.ValidateRow += new DevExpress.XtraGrid.Views.Base.ValidateRowEventHandler(this.gridView1_ValidateRow);
            this.advBandedGridView1.RowUpdated += new DevExpress.XtraGrid.Views.Base.RowObjectEventHandler(this.advBandedGridView1_RowUpdated);
            // 
            // gridBand4
            // 
            this.gridBand4.Caption = "gridBand4";
            this.gridBand4.Columns.Add(this.colHSTAssunto);
            this.gridBand4.Columns.Add(this.colHST_CON);
            this.gridBand4.Columns.Add(this.colHSTCritico);
            this.gridBand4.MinWidth = 20;
            this.gridBand4.Name = "gridBand4";
            this.gridBand4.OptionsBand.AllowMove = false;
            this.gridBand4.OptionsBand.AllowSize = false;
            this.gridBand4.OptionsBand.FixedWidth = true;
            this.gridBand4.OptionsBand.ShowCaption = false;
            this.gridBand4.VisibleIndex = 0;
            this.gridBand4.Width = 137;
            // 
            // colHSTAssunto
            // 
            this.colHSTAssunto.AutoFillDown = true;
            this.colHSTAssunto.Caption = "Assunto";
            this.colHSTAssunto.ColumnEdit = this.buscaAssunto;
            this.colHSTAssunto.FieldName = "HST_ASS";
            this.colHSTAssunto.Name = "colHSTAssunto";
            this.colHSTAssunto.OptionsColumn.FixedWidth = true;
            this.colHSTAssunto.Visible = true;
            // 
            // buscaAssunto
            // 
            this.buscaAssunto.AutoHeight = false;
            this.buscaAssunto.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo),
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Plus)});
            this.buscaAssunto.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("ASSDescricao", "ASSDescricao", 71, DevExpress.Utils.FormatType.None, "", true, DevExpress.Utils.HorzAlignment.Near)});
            this.buscaAssunto.DataSource = this.aSSuntoBindingSource;
            this.buscaAssunto.DisplayMember = "ASSDescricao";
            this.buscaAssunto.Name = "buscaAssunto";
            this.buscaAssunto.NullText = "Indefinido";
            this.buscaAssunto.ShowHeader = false;
            this.buscaAssunto.ValueMember = "ASS";
            this.buscaAssunto.ButtonClick += new DevExpress.XtraEditors.Controls.ButtonPressedEventHandler(this.buscaAssunto_ButtonClick);
            this.buscaAssunto.PropertiesChanged += new System.EventHandler(this.buscaAssunto_PropertiesChanged);
            // 
            // aSSuntoBindingSource
            // 
            this.aSSuntoBindingSource.DataMember = "ASSunto";
            this.aSSuntoBindingSource.DataSource = this.dAgendaBindingSource;
            // 
            // colHST_CON
            // 
            this.colHST_CON.AppearanceCell.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.colHST_CON.AppearanceCell.Options.UseBackColor = true;
            this.colHST_CON.Caption = "CODCON";
            this.colHST_CON.ColumnEdit = this.BuscaCON;
            this.colHST_CON.FieldName = "HST_CON";
            this.colHST_CON.Name = "colHST_CON";
            this.colHST_CON.RowIndex = 1;
            this.colHST_CON.Visible = true;
            // 
            // BuscaCON
            // 
            this.BuscaCON.AutoHeight = false;
            this.BuscaCON.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.BuscaCON.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("CONCodigo", "CODCON", 20, DevExpress.Utils.FormatType.None, "", true, DevExpress.Utils.HorzAlignment.Near),
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("CONNome", "Nome", 100, DevExpress.Utils.FormatType.None, "", true, DevExpress.Utils.HorzAlignment.Near)});
            this.BuscaCON.DataSource = this.cONDOMINIOSBindingSource;
            this.BuscaCON.DisplayMember = "CONCodigo";
            this.BuscaCON.Name = "BuscaCON";
            this.BuscaCON.PopupWidth = 300;
            this.BuscaCON.ValueMember = "CON";
            // 
            // cONDOMINIOSBindingSource
            // 
            this.cONDOMINIOSBindingSource.DataMember = "CONDOMINIOS";
            this.cONDOMINIOSBindingSource.DataSource = typeof(Framework.Lookup.dComponente_F);
            // 
            // colHSTCritico
            // 
            this.colHSTCritico.AutoFillDown = true;
            this.colHSTCritico.Caption = "Cr�tico";
            this.colHSTCritico.ColumnEdit = this.chCritico;
            this.colHSTCritico.FieldName = "HSTCritico";
            this.colHSTCritico.Name = "colHSTCritico";
            this.colHSTCritico.OptionsColumn.FixedWidth = true;
            this.colHSTCritico.RowIndex = 1;
            this.colHSTCritico.Visible = true;
            this.colHSTCritico.Width = 62;
            // 
            // chCritico
            // 
            this.chCritico.AutoHeight = false;
            this.chCritico.Caption = "Cr�tico";
            this.chCritico.GlyphAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.chCritico.Name = "chCritico";
            // 
            // gridBand1
            // 
            this.gridBand1.Caption = "Hist�rico";
            this.gridBand1.Columns.Add(this.colHSTDescricao);
            this.gridBand1.Columns.Add(this.bandedGridColumn2);
            this.gridBand1.MinWidth = 20;
            this.gridBand1.Name = "gridBand1";
            this.gridBand1.OptionsBand.AllowMove = false;
            this.gridBand1.OptionsBand.AllowSize = false;
            this.gridBand1.OptionsBand.ShowCaption = false;
            this.gridBand1.VisibleIndex = 1;
            this.gridBand1.Width = 592;
            // 
            // colHSTDescricao
            // 
            this.colHSTDescricao.AutoFillDown = true;
            this.colHSTDescricao.Caption = "Descri��o";
            this.colHSTDescricao.ColumnEdit = this.repositoryItemMemoEdit1;
            this.colHSTDescricao.FieldName = "HSTDescricao";
            this.colHSTDescricao.Name = "colHSTDescricao";
            this.colHSTDescricao.Visible = true;
            this.colHSTDescricao.Width = 534;
            // 
            // repositoryItemMemoEdit1
            // 
            this.repositoryItemMemoEdit1.Name = "repositoryItemMemoEdit1";
            this.repositoryItemMemoEdit1.ReadOnly = true;
            this.repositoryItemMemoEdit1.ScrollBars = System.Windows.Forms.ScrollBars.Both;
            // 
            // bandedGridColumn2
            // 
            this.bandedGridColumn2.AutoFillDown = true;
            this.bandedGridColumn2.Caption = "Postar";
            this.bandedGridColumn2.ColumnEdit = this.BotaoPostar;
            this.bandedGridColumn2.Name = "bandedGridColumn2";
            this.bandedGridColumn2.OptionsColumn.FixedWidth = true;
            this.bandedGridColumn2.RowCount = 2;
            this.bandedGridColumn2.ShowButtonMode = DevExpress.XtraGrid.Views.Base.ShowButtonModeEnum.ShowAlways;
            this.bandedGridColumn2.Visible = true;
            this.bandedGridColumn2.Width = 58;
            // 
            // BotaoPostar
            // 
            this.BotaoPostar.AutoHeight = false;
            this.BotaoPostar.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Plus, "Postar", -1, true, true, true, DevExpress.XtraEditors.ImageLocation.MiddleRight, null, new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), serializableAppearanceObject1, "", null, null, false)});
            this.BotaoPostar.Name = "BotaoPostar";
            this.BotaoPostar.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.HideTextEditor;
            this.BotaoPostar.ButtonClick += new DevExpress.XtraEditors.Controls.ButtonPressedEventHandler(this.BotaoPostar_ButtonClick);
            this.BotaoPostar.PropertiesChanged += new System.EventHandler(this.BotaoPostar_PropertiesChanged);
            // 
            // gridBand2
            // 
            this.gridBand2.Caption = "Autor";
            this.gridBand2.Columns.Add(this.colHSTResponsavel_USU);
            this.gridBand2.Columns.Add(this.colHSTData);
            this.gridBand2.Columns.Add(this.colHSTConcluido);
            this.gridBand2.MinWidth = 20;
            this.gridBand2.Name = "gridBand2";
            this.gridBand2.OptionsBand.AllowMove = false;
            this.gridBand2.OptionsBand.AllowSize = false;
            this.gridBand2.OptionsBand.FixedWidth = true;
            this.gridBand2.OptionsBand.ShowCaption = false;
            this.gridBand2.VisibleIndex = 2;
            this.gridBand2.Width = 180;
            // 
            // colHSTResponsavel_USU
            // 
            this.colHSTResponsavel_USU.Caption = "Autor";
            this.colHSTResponsavel_USU.ColumnEdit = this.BuscaUSU;
            this.colHSTResponsavel_USU.FieldName = "HSTI_USU";
            this.colHSTResponsavel_USU.Name = "colHSTResponsavel_USU";
            this.colHSTResponsavel_USU.OptionsColumn.AllowEdit = false;
            this.colHSTResponsavel_USU.OptionsColumn.FixedWidth = true;
            this.colHSTResponsavel_USU.Visible = true;
            this.colHSTResponsavel_USU.Width = 156;
            // 
            // BuscaUSU
            // 
            this.BuscaUSU.AllowNullInput = DevExpress.Utils.DefaultBoolean.False;
            this.BuscaUSU.AutoHeight = false;
            this.BuscaUSU.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.BuscaUSU.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("USUNome", "USUNome", 53, DevExpress.Utils.FormatType.None, "", true, DevExpress.Utils.HorzAlignment.Near, DevExpress.Data.ColumnSortOrder.Ascending)});
            this.BuscaUSU.DataSource = this.uSUARIOSBindingSource;
            this.BuscaUSU.DisplayMember = "USUNome";
            this.BuscaUSU.HeaderClickMode = DevExpress.XtraEditors.Controls.HeaderClickMode.AutoSearch;
            this.BuscaUSU.Name = "BuscaUSU";
            this.BuscaUSU.NullText = "-";
            this.BuscaUSU.ShowHeader = false;
            this.BuscaUSU.ValueMember = "USU";
            this.BuscaUSU.EditValueChanged += new System.EventHandler(this.BuscaUSU_EditValueChanged);
            this.BuscaUSU.Enter += new System.EventHandler(this.BuscaUSU_Enter);
            this.BuscaUSU.Leave += new System.EventHandler(this.BuscaUSU_Leave);
            // 
            // uSUARIOSBindingSource
            // 
            this.uSUARIOSBindingSource.DataMember = "USUARIOS";
            this.uSUARIOSBindingSource.DataSource = typeof(FrameworkProc.datasets.dUSUarios);
            // 
            // colHSTData
            // 
            this.colHSTData.Caption = "Inclus�o";
            this.colHSTData.DisplayFormat.FormatString = "dd/MM/yy hh:mm:ss";
            this.colHSTData.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.colHSTData.FieldName = "HSTDATAI";
            this.colHSTData.Name = "colHSTData";
            this.colHSTData.OptionsColumn.AllowEdit = false;
            this.colHSTData.OptionsColumn.FixedWidth = true;
            this.colHSTData.RowIndex = 1;
            this.colHSTData.Visible = true;
            this.colHSTData.Width = 102;
            // 
            // colHSTConcluido
            // 
            this.colHSTConcluido.Caption = "Conclu�do";
            this.colHSTConcluido.ColumnEdit = this.chConcluido;
            this.colHSTConcluido.FieldName = "HSTConcluido";
            this.colHSTConcluido.Name = "colHSTConcluido";
            this.colHSTConcluido.RowIndex = 1;
            this.colHSTConcluido.Visible = true;
            this.colHSTConcluido.Width = 78;
            // 
            // chConcluido
            // 
            this.chConcluido.AutoHeight = false;
            this.chConcluido.Caption = "Conclu�do";
            this.chConcluido.GlyphAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.chConcluido.Name = "chConcluido";
            this.chConcluido.CheckedChanged += new System.EventHandler(this.chConcluido_EditValueChanged);
            // 
            // gridBand3
            // 
            this.gridBand3.Caption = "Responsavel";
            this.gridBand3.Columns.Add(this.colHSTA_USU);
            this.gridBand3.Columns.Add(this.colHSTADATA);
            this.gridBand3.Columns.Add(this.colHSTAceito);
            this.gridBand3.MinWidth = 20;
            this.gridBand3.Name = "gridBand3";
            this.gridBand3.OptionsBand.AllowMove = false;
            this.gridBand3.OptionsBand.AllowSize = false;
            this.gridBand3.OptionsBand.FixedWidth = true;
            this.gridBand3.OptionsBand.ShowCaption = false;
            this.gridBand3.VisibleIndex = 3;
            this.gridBand3.Width = 180;
            // 
            // colHSTA_USU
            // 
            this.colHSTA_USU.Caption = "Respons�vel";
            this.colHSTA_USU.ColumnEdit = this.BuscaUSU;
            this.colHSTA_USU.FieldName = "HSTA_USU";
            this.colHSTA_USU.Name = "colHSTA_USU";
            this.colHSTA_USU.Visible = true;
            this.colHSTA_USU.Width = 180;
            // 
            // colHSTADATA
            // 
            this.colHSTADATA.Caption = "Data Prometida";
            this.colHSTADATA.ColumnEdit = this.Agendadata;
            this.colHSTADATA.FieldName = "HSTDATAPrometida";
            this.colHSTADATA.Name = "colHSTADATA";
            this.colHSTADATA.RowIndex = 1;
            this.colHSTADATA.Visible = true;
            this.colHSTADATA.Width = 113;
            // 
            // Agendadata
            // 
            this.Agendadata.AllowNullInput = DevExpress.Utils.DefaultBoolean.False;
            this.Agendadata.AutoHeight = false;
            this.Agendadata.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.Agendadata.CalendarTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.Agendadata.Name = "Agendadata";
            this.Agendadata.EditValueChanged += new System.EventHandler(this.eddataprometida_EditValueChanged);
            // 
            // colHSTAceito
            // 
            this.colHSTAceito.Caption = "Aceito";
            this.colHSTAceito.ColumnEdit = this.chAceito;
            this.colHSTAceito.FieldName = "HSTAceito";
            this.colHSTAceito.Name = "colHSTAceito";
            this.colHSTAceito.RowIndex = 1;
            this.colHSTAceito.Visible = true;
            this.colHSTAceito.Width = 67;
            // 
            // chAceito
            // 
            this.chAceito.AutoHeight = false;
            this.chAceito.Caption = "Aceito";
            this.chAceito.GlyphAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.chAceito.Name = "chAceito";
            // 
            // timer1
            // 
            this.timer1.Enabled = true;
            this.timer1.Interval = 10000;
            this.timer1.Tick += new System.EventHandler(this.timer1_Tick);
            // 
            // notifyIcon1
            // 
            this.notifyIcon1.BalloonTipIcon = System.Windows.Forms.ToolTipIcon.Warning;
            this.notifyIcon1.BalloonTipText = "Nova chamada encaminhada para voc�";
            this.notifyIcon1.BalloonTipTitle = "Sistema Neon -Chamada";
            this.notifyIcon1.Icon = ((System.Drawing.Icon)(resources.GetObject("notifyIcon1.Icon")));
            this.notifyIcon1.Text = "notifyIcon1";
            this.notifyIcon1.Click += new System.EventHandler(this.notifyIcon1_Click);
            // 
            // notifyIcon2
            // 
            this.notifyIcon2.BalloonTipIcon = System.Windows.Forms.ToolTipIcon.Info;
            this.notifyIcon2.BalloonTipText = "Altera��o em chamada";
            this.notifyIcon2.BalloonTipTitle = "Sistema Neon -Chamada";
            this.notifyIcon2.Icon = ((System.Drawing.Icon)(resources.GetObject("notifyIcon2.Icon")));
            this.notifyIcon2.Text = "notifyIcon1";
            this.notifyIcon2.Click += new System.EventHandler(this.notifyIcon1_Click);
            // 
            // Chamadas
            // 
            this.Controls.Add(this.gridControl1);
            this.Controls.Add(this.panelControl2);
            this.Doc = System.Windows.Forms.DockStyle.Fill;
            this.LookAndFeel.UseDefaultLookAndFeel = false;
            this.Name = "Chamadas";
            this.Size = new System.Drawing.Size(1545, 597);
            this.Enter += new System.EventHandler(this.Chamadas_Enter);
            this.Leave += new System.EventHandler(this.Chamadas_Leave);
            ((System.ComponentModel.ISupportInitialize)(this.StyleController_F)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl2)).EndInit();
            this.panelControl2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.chMeus.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chAbertos.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dAgendaBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.advBandedGridView1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.buscaAssunto)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.aSSuntoBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.BuscaCON)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cONDOMINIOSBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chCritico)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoEdit1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.BotaoPostar)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.BuscaUSU)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uSUARIOSBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chConcluido)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Agendadata.CalendarTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Agendadata)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chAceito)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraEditors.PanelControl panelControl2;
        private DevExpress.XtraEditors.CheckEdit chMeus;
        private DevExpress.XtraEditors.CheckEdit chAbertos;
        private DevExpress.XtraGrid.GridControl gridControl1;
        private DevExpress.XtraGrid.Views.BandedGrid.AdvBandedGridView advBandedGridView1;
        private DevExpress.XtraGrid.Views.BandedGrid.GridBand gridBand4;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn colHSTAssunto;
        private DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit buscaAssunto;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn colHSTCritico;
        private DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit chCritico;
        private DevExpress.XtraGrid.Views.BandedGrid.GridBand gridBand1;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn colHSTDescricao;
        private DevExpress.XtraEditors.Repository.RepositoryItemMemoEdit repositoryItemMemoEdit1;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn bandedGridColumn2;
        private DevExpress.XtraEditors.Repository.RepositoryItemButtonEdit BotaoPostar;
        private DevExpress.XtraGrid.Views.BandedGrid.GridBand gridBand2;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn colHSTResponsavel_USU;
        private DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit BuscaUSU;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn colHSTData;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn colHSTConcluido;
        private DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit chConcluido;
        private DevExpress.XtraGrid.Views.BandedGrid.GridBand gridBand3;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn colHSTA_USU;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn colHSTAceito;
        private DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit chAceito;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn colHSTADATA;
        private DevExpress.XtraEditors.Repository.RepositoryItemDateEdit Agendadata;
        private System.Windows.Forms.BindingSource dAgendaBindingSource;
        private System.Windows.Forms.BindingSource aSSuntoBindingSource;
        private System.Windows.Forms.BindingSource uSUARIOSBindingSource;
        private Framework.Lookup.LookupCondominio_F lookupCondominio_F1;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn colHST_CON;
        private DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit BuscaCON;
        private System.Windows.Forms.BindingSource cONDOMINIOSBindingSource;
        private System.Windows.Forms.Timer timer1;
        private System.Windows.Forms.NotifyIcon notifyIcon1;
        private System.Windows.Forms.NotifyIcon notifyIcon2;
    }
}
