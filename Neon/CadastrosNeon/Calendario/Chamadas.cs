using System;
using System.Data;
using System.Drawing;
using System.Windows.Forms;


namespace Calendario
{
    /// <summary>
    /// 
    /// </summary>
    public partial class Chamadas : CompontesBasicos.ComponenteBase
    {
        private dAgenda.HISTORICOSRow HistRow;

        private bool PegaLinha()
        {
            return PegaLinha(advBandedGridView1.FocusedRowHandle);
        }

        private bool PegaLinha(int RowHandle)
        {
            return PegaLinha(RowHandle, out HistRow);            
        }

        private bool PegaLinha(int RowHandle, out dAgenda.HISTORICOSRow rowHST)
        {
            DataRowView DRV = (DataRowView)advBandedGridView1.GetRow(RowHandle);
            if ((DRV != null) && (DRV.Row != null))
                rowHST = (dAgenda.HISTORICOSRow)DRV.Row;
            else
                rowHST = null;
            return (rowHST != null);
        }

        /// <summary>
        /// 
        /// </summary>
        public Chamadas()
        {
            InitializeComponent();
            if (!DesignMode)
            {
                aSSuntoBindingSource.DataSource = dAgendaBindingSource.DataSource = dAgenda.DAgenda;
                uSUARIOSBindingSource.DataSource = FrameworkProc.datasets.dUSUarios.dUSUariosSt;// CompontesBasicos.Bancovirtual.UsuarioLogado.dUsuarios;
                cONDOMINIOSBindingSource.DataSource = Framework.Lookup.dComponente_F.dComponente_FSt;// EdComponente_F;
                dAgenda.DAgenda.ASSuntoTableAdapter.Fill(dAgenda.DAgenda.ASSunto);
                CargaH();
                Agendadata.MinValue = DateTime.Today;
                Agendadata.MaxValue = DateTime.Today.AddYears(1);
            }
        }
        
        private void gridView1_InitNewRow(object sender, DevExpress.XtraGrid.Views.Grid.InitNewRowEventArgs e)
        {
            PegaLinha(e.RowHandle);
            if (HistRow != null)
            {
                string Texto = "";
                if (lookupCondominio_F1.CON_sel != -1)
                    HistRow.HST_CON = lookupCondominio_F1.CON_sel;
                else
                    HistRow.HST_CON = 232;
                HistRow.HSTCritico = false;
                HistRow.HSTDATAI = DateTime.Now;
                HistRow.HSTI_USU = CompontesBasicos.Bancovirtual.UsuarioLogado.UsuarioLogadoSt.USU;
                HistRow.HST_ASS = 0;
                HistRow.HSTDescricao = "";
                HistRow.HSTAceito = true;
                HistRow.HSTA_USU = CompontesBasicos.Bancovirtual.UsuarioLogado.UsuarioLogadoSt.USU;
                HistRow.HSTConcluido = false;
                HistRow.HSTADATA = DateTime.Now;
                VirInput.Input.Execute("Descri��o inicial", ref Texto, true);

                apenda("Cadastro", Texto);
                //    advBandedGridView1.CloseEditor();
                //advBandedGridView1.UpdateCurrentRow();


            };
        }

        private void Chamadas_Enter(object sender, EventArgs e)
        {
            notifyIcon2.Visible = false;
            AvisoAmarelo = false;
            if(CompontesBasicos.FormPrincipalBase.FormPrincipal != null)
                 CompontesBasicos.FormPrincipalBase.FormPrincipal.CaixaAltaGeral = false;
        }

        private void Chamadas_Leave(object sender, EventArgs e)
        {
            if (CompontesBasicos.FormPrincipalBase.FormPrincipal != null)
                 CompontesBasicos.FormPrincipalBase.FormPrincipal.CaixaAltaGeral = true;
             Salvar();
        }

        private void gridView1_ValidateRow(object sender, DevExpress.XtraGrid.Views.Base.ValidateRowEventArgs e)
        {
            e.ErrorText = "";
            if (PegaLinha(e.RowHandle))
            {
                if (HistRow.HST_CON == -1)
                {
                    e.Valid = false;
                    e.ErrorText = "Condom�nio n�o informado";
                };
                
                if (HistRow.HST_ASS == 0)
                {
                    e.Valid = false;
                    e.ErrorText = "Assunto n�o informado";
                }
                
                    
            }
            else
                e.Valid = false;
        }

        private void apenda(string Tipo, string Texto)
        {
            if (HistRow.HSTDescricao != "")
                HistRow.HSTDescricao += "\r\n\r\n";
            HistRow.HSTDescricao += "(" + DateTime.Now.ToString() + ")" + CompontesBasicos.Bancovirtual.UsuarioLogado.UsuarioLogadoSt.USUNome + " " + Tipo + "\r\n";
            if (Texto != "")
                HistRow.HSTDescricao += "--------------\r\n" + Texto;
            HistRow.HSTADATA = DateTime.Now;
        }

        private void BotaoPostar_ButtonClick(object sender, DevExpress.XtraEditors.Controls.ButtonPressedEventArgs e)
        {
            Zoom.fChamada FChamada = new Calendario.Zoom.fChamada();
            FChamada.cChamada1.Carregar(HistRow);
            FChamada.Text = "Chamada";
            if (FChamada.ShowDialog() == DialogResult.OK)
            {
                advBandedGridView1.UpdateCurrentRow();
                Salvar();
            };
            return;                        
        }

        private void BuscaUSU_EditValueChanged(object sender, EventArgs e)
        {
            return;            
        }

        private void BuscaUSU_Leave(object sender, EventArgs e)
        {
            DevExpress.XtraEditors.LookUpEdit Editor = (DevExpress.XtraEditors.LookUpEdit)sender;
            if (Editor.EditValue == null)
                return;
            if (HSTA_USUEntrada != (int)Editor.EditValue)
            {
                //if (PegaLinha(advBandedGridView1.FocusedRowHandle))
                //{

                HistRow.HSTA_USU = (int)Editor.EditValue;
                HistRow.HSTAceito = ((int)HistRow.HSTA_USU == CompontesBasicos.Bancovirtual.UsuarioLogado.UsuarioLogadoSt.USU);
                chAceito.ReadOnly = true;
                apenda("Encaminhado para " + Editor.Text, "");
                
                advBandedGridView1.CloseEditor();
                advBandedGridView1.UpdateCurrentRow();
                Salvar();
                //}
            }
        }

        private int HSTA_USUEntrada;

        private void BuscaUSU_Enter(object sender, EventArgs e)
        {
            HSTA_USUEntrada = HistRow.HSTA_USU;
        }

        private void eddataprometida_EditValueChanged(object sender, EventArgs e)
        {
            if (PegaLinha(advBandedGridView1.FocusedRowHandle))
            {
                DevExpress.XtraEditors.DateEdit Editor = (DevExpress.XtraEditors.DateEdit)sender;


                apenda("Agendado para  " + Editor.Text, "");
                advBandedGridView1.CloseEditor();
            }
        }

        private void chConcluido_EditValueChanged(object sender, EventArgs e)
        {
            advBandedGridView1.CloseEditor();
            
            if (PegaLinha())
            {
                DevExpress.XtraEditors.CheckEdit Editor = (DevExpress.XtraEditors.CheckEdit)sender;

                string comentario = "";
                VirInput.Input.Execute("Coment�rio", ref comentario, false);
                apenda(Editor.Checked ? "Conclu�do" : "Reaberto", comentario);
                HistRow.EndEdit();
                advBandedGridView1.UpdateCurrentRow();
                Salvar();
            }
        }

        private void buscaAssunto_ButtonClick(object sender, DevExpress.XtraEditors.Controls.ButtonPressedEventArgs e)
        {
            DevExpress.XtraEditors.LookUpEdit Editor = (DevExpress.XtraEditors.LookUpEdit)sender;
            if (e.Button.Kind == DevExpress.XtraEditors.Controls.ButtonPredefines.Plus)
            {
                string novo = "";
                DataRow[] RowASSs;
                dAgenda.ASSuntoRow RowASS;
                if (VirInput.Input.Execute("Novo Assunto", ref novo, false, 30))
                {
                    novo = novo.Trim().ToUpper();
                    if (novo == "")
                        return;
                    RowASSs = dAgenda.DAgenda.ASSunto.Select("ASSDescricao = '" + novo + "'");
                    if (RowASSs.Length == 0)
                    {
                        RowASS = dAgenda.DAgenda.ASSunto.AddASSuntoRow(novo);
                        dAgenda.DAgenda.ASSuntoTableAdapter.Update(RowASS);
                        RowASS.AcceptChanges();
                    }
                    else
                        RowASS = (dAgenda.ASSuntoRow)RowASSs[0];
                    Editor.EditValue = RowASS.ASS;
                };
            }
        }

        private void Salvar() {
            if (dAgenda.DAgenda.HasChanges())
            {
                try
                {
                    dAgenda.DAgenda.HISTORICOSTableAdapter.Update(dAgenda.DAgenda.HISTORICOS);
                    dAgenda.DAgenda.HISTORICOS.AcceptChanges();
                }
                catch (System.Data.DBConcurrencyException) {
                    MessageBox.Show("Registro alterado por outro usu�rio");
                    dAgenda.DAgenda.HISTORICOS.RejectChanges();
                }
            }
        }

        private void AjustaReadOnly(bool NovaLinha) {
            if (!NovaLinha)
            {
                chAceito.ReadOnly = (HistRow.HSTAceito || HistRow.HSTConcluido || (CompontesBasicos.Bancovirtual.UsuarioLogado.UsuarioLogadoSt.USU != HistRow.HSTA_USU));
                BuscaCON.ReadOnly = buscaAssunto.ReadOnly = chConcluido.ReadOnly = chCritico.ReadOnly = (CompontesBasicos.Bancovirtual.UsuarioLogado.UsuarioLogadoSt.USU != HistRow.HSTI_USU);
                Agendadata.ReadOnly = BuscaUSU.ReadOnly = BotaoPostar.ReadOnly = HistRow.HSTConcluido;

            }
            else
            {
                chAceito.ReadOnly = true;
                BuscaCON.ReadOnly = Agendadata.ReadOnly = BuscaUSU.ReadOnly = BotaoPostar.ReadOnly = buscaAssunto.ReadOnly = chConcluido.ReadOnly = chCritico.ReadOnly = false;

            }
        }
        
        private void advBandedGridView1_FocusedRowChanged(object sender, DevExpress.XtraGrid.Views.Base.FocusedRowChangedEventArgs e)
        {
            Salvar();
            
            AjustaReadOnly(!PegaLinha(e.FocusedRowHandle));                            

        }

        private void buscaAssunto_PropertiesChanged(object sender, EventArgs e)
        {
            DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit Editor = (DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit)sender;
            foreach (DevExpress.XtraEditors.Controls.EditorButton Botao in Editor.Buttons)
            {
                Botao.Enabled = !Editor.ReadOnly;
            }

        }

        private void BotaoPostar_PropertiesChanged(object sender, EventArgs e)
        {
            DevExpress.XtraEditors.Repository.RepositoryItemButtonEdit Editor = (DevExpress.XtraEditors.Repository.RepositoryItemButtonEdit)sender;
            foreach (DevExpress.XtraEditors.Controls.EditorButton Botao in Editor.Buttons)
            {
                Botao.Enabled = !Editor.ReadOnly;
            }
        }
       
        private void chAbertos_CheckedChanged(object sender, EventArgs e)
        {
            Salvar();
            CargaH();
        }

        private DateTime UltimaDataCorte = DateTime.MinValue;

        private void CargaH()
        {
            UltimaDataCorte = DateTime.Now;
            if (chAbertos.Checked) {
                if (chMeus.Checked)
                    dAgenda.DAgenda.HISTORICOSTableAdapter.MeusAbertos(dAgenda.DAgenda.HISTORICOS, CompontesBasicos.Bancovirtual.UsuarioLogado.UsuarioLogadoSt.USU);
                else
                    dAgenda.DAgenda.HISTORICOSTableAdapter.Abertos(dAgenda.DAgenda.HISTORICOS);
            }
            else {
                if (chMeus.Checked)
                    dAgenda.DAgenda.HISTORICOSTableAdapter.Meus(dAgenda.DAgenda.HISTORICOS, CompontesBasicos.Bancovirtual.UsuarioLogado.UsuarioLogadoSt.USU);
                else
                    dAgenda.DAgenda.HISTORICOSTableAdapter.Fill(dAgenda.DAgenda.HISTORICOS);
            };
            
        }

        private DateTime dataRef = DateTime.MinValue;

        private DateTime DataRef {
            get {
                if (dataRef == DateTime.MinValue) {
                    switch (DateTime.Today.DayOfWeek) {
                        case DayOfWeek.Wednesday: 
                        case DayOfWeek.Thursday: 
                        case DayOfWeek.Friday: 
                            dataRef = DateTime.Today.AddDays(+5);
                            break;
                        default: dataRef = DateTime.Today.AddDays(+3);
                            break;
                    };
                    
                }
                return dataRef;
            }
            set{
                dataRef = value;
            }
        }

        private void advBandedGridView1_RowCellStyle(object sender, DevExpress.XtraGrid.Views.Grid.RowCellStyleEventArgs e)
        {
            dAgenda.HISTORICOSRow rowHST;
            if (!PegaLinha(e.RowHandle, out rowHST))
                return;
            if (rowHST.RowState == DataRowState.Detached)
                return;
            e.Appearance.ForeColor = Color.Black;
            if (e.Column == colHSTA_USU)

                if (rowHST.HSTA_USU == CompontesBasicos.Bancovirtual.UsuarioLogado.UsuarioLogadoSt.USU)
                    if (rowHST.HSTAceito)
                        e.Appearance.BackColor = Color.Aqua;
                    else
                        e.Appearance.BackColor = Color.Red;


            if (e.Column == colHSTResponsavel_USU)
                if (rowHST.HSTI_USU == CompontesBasicos.Bancovirtual.UsuarioLogado.UsuarioLogadoSt.USU)
                    e.Appearance.BackColor = Color.Aqua;
            if (e.Column == colHSTADATA){
                if (rowHST.IsHSTDATAPrometidaNull())
                    if (rowHST.HSTA_USU == CompontesBasicos.Bancovirtual.UsuarioLogado.UsuarioLogadoSt.USU)
                        e.Appearance.BackColor = Color.Red;
                    else
                        e.Appearance.BackColor = Color.Yellow;
                else
                    if (rowHST.HSTDATAPrometida < DateTime.Today)
                            e.Appearance.BackColor = Color.Red;
                    else
                        if (rowHST.HSTDATAPrometida <= DataRef)
                           e.Appearance.BackColor = Color.Yellow;
                    
                        
            }
            
        }

        private void lookupCondominio_F1_alterado(object sender, EventArgs e)
        {
            
            
           
            if (lookupCondominio_F1.CON_sel == -1)
                colHST_CON.ClearFilter();
            else
                colHST_CON.FilterInfo = new DevExpress.XtraGrid.Columns.ColumnFilterInfo(colHST_CON, lookupCondominio_F1.CON_sel);
        }

        private dAgenda.HISTORICOSRow RowAntiga;

        private void aviso() {
            //notifyIcon2.Visible = true;  notifyIcon2.ShowBalloonTip(1000, this.ContainsFocus ? "SIM":"nao", "teste", ToolTipIcon.None);
            if (this.ContainsFocus || notifyIcon1.Visible)
            {
                notifyIcon2.Visible = false;
                AvisoAmarelo = false;
            }

            DataRow[] meus = dAgenda.DAgenda.HISTORICOS.Select("(HSTA_USU = " + CompontesBasicos.Bancovirtual.UsuarioLogado.UsuarioLogadoSt.USU + ") and (HSTConcluido = false) and ((HSTAceito = false) or (HSTDataPrometida is null))");
            if (meus.Length > 0)
            {
                notifyIcon1.Visible = true;
                notifyIcon1.ShowBalloonTip(5000);
            }
            else
                notifyIcon1.Visible = false;
            if (!notifyIcon1.Visible && !this.ContainsFocus && !notifyIcon2.Visible && AvisoAmarelo)
            {
                notifyIcon2.Visible = true;
                notifyIcon2.ShowBalloonTip(3000);
            };
            AvisoAmarelo = false;
        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            //notifyIcon2.Visible = true;
            
            dAgenda.HISTORICOSDataTable DadosAlt = dAgenda.DAgenda.HISTORICOSTableAdapter.Alteracoes(UltimaDataCorte);
            UltimaDataCorte = DateTime.Now.AddSeconds(-10);
            if (DadosAlt.Rows.Count > 0)
            {
                foreach (dAgenda.HISTORICOSRow novaRow in DadosAlt)
                {
                    if (!chMeus.Checked || (novaRow.HSTA_USU == CompontesBasicos.Bancovirtual.UsuarioLogado.UsuarioLogadoSt.USU) || (novaRow.HSTI_USU == CompontesBasicos.Bancovirtual.UsuarioLogado.UsuarioLogadoSt.USU))
                    {
                        RowAntiga = dAgenda.DAgenda.HISTORICOS.FindByHST(novaRow.HST);
                        if (RowAntiga == null)
                        {
                            dAgenda.DAgenda.HISTORICOSTableAdapter.ClearBeforeFill = false;
                            dAgenda.DAgenda.HISTORICOSTableAdapter.FillByHST(dAgenda.DAgenda.HISTORICOS,novaRow.HST);
                            
                            dAgenda.DAgenda.HISTORICOSTableAdapter.ClearBeforeFill = true;
                        }
                        else
                            if ((RowAntiga.RowState == DataRowState.Unchanged) || (RowAntiga.RowState == DataRowState.Detached))
                            {
                                foreach (DataColumn Coluna in dAgenda.DAgenda.HISTORICOS.Columns)
                                    if (!Coluna.ReadOnly)
                                    {
                                        if (!RowAntiga[Coluna.ColumnName].Equals(novaRow[Coluna.ColumnName]))
                                            RowAntiga[Coluna.ColumnName] = novaRow[Coluna.ColumnName];
                                    }
                                    else
                                        if ((Coluna.AutoIncrement) && (RowAntiga.RowState == DataRowState.Detached))
                                            RowAntiga[Coluna.ColumnName] = novaRow[Coluna.ColumnName];
                                // if (RowAntiga.RowState == DataRowState.Detached)
                                //    dAgenda.DAgenda.HISTORICOS.AddHISTORICOSRow(RowAntiga);
                                if (RowAntiga.RowState != DataRowState.Unchanged)
                                {
                                    RowAntiga.AcceptChanges();
                                    AvisoAmarelo = true;
                                }
                                if ((HistRow != null) && (HistRow.HST == RowAntiga.HST))
                                    AjustaReadOnly(false);
                            };
                    };
                };
                
                aviso();
            };
            
        }

        private void advBandedGridView1_RowUpdated(object sender, DevExpress.XtraGrid.Views.Base.RowObjectEventArgs e)
        {
            if(PegaLinha(e.RowHandle))
                HistRow.HSTADATA = DateTime.Now;
        }
        
        private bool AvisoAmarelo = false;

        private void notifyIcon1_Click(object sender, EventArgs e)
        {
            if (CompontesBasicos.FormPrincipalBase.FormPrincipal != null)
                CompontesBasicos.FormPrincipalBase.FormPrincipal.ShowModulo(typeof(Chamadas));
        }

        private void gridControl1_DoubleClick(object sender, EventArgs e)
        {
            Zoom.fChamada FChamada = new Calendario.Zoom.fChamada();
            FChamada.cChamada1.Carregar(HistRow);
            FChamada.Text = "Chamada";
            if (FChamada.ShowDialog() == DialogResult.OK)
            {
                advBandedGridView1.UpdateCurrentRow();
                Salvar();
            };
        }
                
    }
}

