namespace Calendario.StatusGeral
{
    partial class cStatusIndividual
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.pictureBoxN = new System.Windows.Forms.PictureBox();
            this.pictureBoxP = new System.Windows.Forms.PictureBox();
            this.TituloTela = new DevExpress.XtraEditors.LabelControl();
            this.simpleButton1 = new DevExpress.XtraEditors.SimpleButton();
            this.spinEdit1 = new DevExpress.XtraEditors.SpinEdit();
            this.BotObs = new DevExpress.XtraEditors.SimpleButton();
            ((System.ComponentModel.ISupportInitialize)(this.StyleController_F)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxN)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxP)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.spinEdit1.Properties)).BeginInit();
            this.SuspendLayout();
            // 
            // ToolTipController_F
            // 
            this.ToolTipController_F.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.ToolTipController_F.Appearance.ForeColor = System.Drawing.Color.Black;
            this.ToolTipController_F.Appearance.Options.UseBackColor = true;
            this.ToolTipController_F.Appearance.Options.UseFont = true;
            this.ToolTipController_F.Appearance.Options.UseForeColor = true;
            this.ToolTipController_F.Appearance.Options.UseTextOptions = true;
            this.ToolTipController_F.Appearance.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            // 
            // StyleController_F
            // 
            this.StyleController_F.AppearanceDisabled.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this.StyleController_F.AppearanceDisabled.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this.StyleController_F.AppearanceDisabled.Options.UseBackColor = true;
            this.StyleController_F.AppearanceDisabled.Options.UseForeColor = true;
            this.StyleController_F.AppearanceFocused.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.StyleController_F.AppearanceFocused.Options.UseBackColor = true;
            // 
            // pictureBoxN
            // 
            this.pictureBoxN.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.pictureBoxN.BackColor = System.Drawing.Color.Transparent;
            this.pictureBoxN.Image = global::Calendario.Properties.Resources.negativoT;
            this.pictureBoxN.Location = new System.Drawing.Point(3, 37);
            this.pictureBoxN.Name = "pictureBoxN";
            this.pictureBoxN.Size = new System.Drawing.Size(55, 45);
            this.pictureBoxN.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize;
            this.pictureBoxN.TabIndex = 0;
            this.pictureBoxN.TabStop = false;
            this.pictureBoxN.Visible = false;
            // 
            // pictureBoxP
            // 
            this.pictureBoxP.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.pictureBoxP.Image = global::Calendario.Properties.Resources.positivoT;
            this.pictureBoxP.Location = new System.Drawing.Point(3, 3);
            this.pictureBoxP.Name = "pictureBoxP";
            this.pictureBoxP.Size = new System.Drawing.Size(53, 41);
            this.pictureBoxP.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize;
            this.pictureBoxP.TabIndex = 1;
            this.pictureBoxP.TabStop = false;
            this.pictureBoxP.Visible = false;
            // 
            // TituloTela
            // 
            this.TituloTela.Appearance.Font = new System.Drawing.Font("Tahoma", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TituloTela.Appearance.ForeColor = System.Drawing.Color.Blue;
            this.TituloTela.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.TituloTela.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.TituloTela.Dock = System.Windows.Forms.DockStyle.Top;
            this.TituloTela.ImageAlignToText = DevExpress.XtraEditors.ImageAlignToText.TopCenter;
            this.TituloTela.Location = new System.Drawing.Point(0, 0);
            this.TituloTela.Name = "TituloTela";
            this.TituloTela.Size = new System.Drawing.Size(263, 23);
            this.TituloTela.TabIndex = 2;
            this.TituloTela.Text = "TituloTela";
            // 
            // simpleButton1
            // 
            this.simpleButton1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.simpleButton1.Image = global::Calendario.Properties.Resources.Help;
            this.simpleButton1.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.simpleButton1.Location = new System.Drawing.Point(182, 22);
            this.simpleButton1.Name = "simpleButton1";
            this.simpleButton1.Size = new System.Drawing.Size(75, 57);
            this.simpleButton1.TabIndex = 3;
            this.simpleButton1.Text = "simpleButton1";
            this.simpleButton1.Click += new System.EventHandler(this.simpleButton1_Click);
            // 
            // spinEdit1
            // 
            this.spinEdit1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.spinEdit1.EditValue = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.spinEdit1.Location = new System.Drawing.Point(64, 31);
            this.spinEdit1.Name = "spinEdit1";
            this.spinEdit1.Properties.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.spinEdit1.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 27.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.spinEdit1.Properties.Appearance.ForeColor = System.Drawing.Color.Blue;
            this.spinEdit1.Properties.Appearance.Options.UseBackColor = true;
            this.spinEdit1.Properties.Appearance.Options.UseFont = true;
            this.spinEdit1.Properties.Appearance.Options.UseForeColor = true;
            this.spinEdit1.Properties.DisplayFormat.FormatString = "n0";
            this.spinEdit1.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.spinEdit1.Properties.ReadOnly = true;
            this.spinEdit1.Size = new System.Drawing.Size(112, 52);
            this.spinEdit1.TabIndex = 4;
            // 
            // BotObs
            // 
            this.BotObs.Image = global::Calendario.Properties.Resources.AlertaP;
            this.BotObs.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.BotObs.Location = new System.Drawing.Point(204, 3);
            this.BotObs.Name = "BotObs";
            this.BotObs.Size = new System.Drawing.Size(24, 31);
            this.BotObs.TabIndex = 5;
            this.BotObs.Text = "simpleButton2";
            this.BotObs.Visible = false;
            this.BotObs.Click += new System.EventHandler(this.BotObs_Click);
            // 
            // cStatusIndividual
            // 
            this.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.Appearance.Options.UseBackColor = true;
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.Controls.Add(this.BotObs);
            this.Controls.Add(this.spinEdit1);
            this.Controls.Add(this.simpleButton1);
            this.Controls.Add(this.TituloTela);
            this.Controls.Add(this.pictureBoxP);
            this.Controls.Add(this.pictureBoxN);
            this.LookAndFeel.UseDefaultLookAndFeel = false;
            this.Name = "cStatusIndividual";
            this.Size = new System.Drawing.Size(263, 85);
            ((System.ComponentModel.ISupportInitialize)(this.StyleController_F)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxN)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxP)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.spinEdit1.Properties)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.PictureBox pictureBoxN;
        private System.Windows.Forms.PictureBox pictureBoxP;
        private DevExpress.XtraEditors.LabelControl TituloTela;
        private DevExpress.XtraEditors.SimpleButton simpleButton1;
        private DevExpress.XtraEditors.SpinEdit spinEdit1;
        private DevExpress.XtraEditors.SimpleButton BotObs;
    }
}
