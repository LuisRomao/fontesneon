using Calendario.Agenda;
using CompontesBasicosProc;
using CompontesBasicos;
using Framework;
using FrameworkProc;
using Framework.objetosNeon;
using System;
using System.Collections.Generic;
using System.Data;
using VirEnumeracoesNeon;

namespace Calendario.StatusGeral
{
    /// <summary>
    /// 
    /// </summary>
    public partial class cStatusGeral : CompontesBasicos.ComponenteBase
    {

        private int AtrasadosEmitir;
        private int AtrasadosRetorno;
        private int Borda = 1;

        private readonly string Comando =
"SELECT     CHEques.CHEVencimento, CONDOMINIOS.CONMalote, CONDOMINIOS.CONProcuracao, CHEques.CHEStatus, CHEques.CHETipo,CONAuditor1_USU\r\n" +
"FROM         CHEques INNER JOIN\r\n" +
"                      CONDOMINIOS ON CHEques.CHE_CON = CONDOMINIOS.CON\r\n" +
"WHERE     (CHEques.CHEVencimento <= @P1) AND \r\n" +
"(CHEques.CHEStatus in (1,2,3,8))  AND (CONDOMINIOS.CONStatus = 1);";

        private readonly string comandobase =
"SELECT     COUNT(AGB) AS Atraso\r\n" +
"FROM         AGendaBase INNER JOIN\r\n" +
"                      CONDOMINIOS ON AGendaBase.AGB_CON = CONDOMINIOS.CON\r\n" +
"WHERE     (AGB_TAG = @TAG) AND (AGBStatus in (2,3))  and (CONStatus = 1);";

        private readonly string comandobaseG =
"SELECT     COUNT(AGendaBase.AGB) AS Atraso\r\n" +
"FROM         AGendaBase INNER JOIN\r\n" +
"                      CONDOMINIOS ON AGendaBase.AGB_CON = CONDOMINIOS.CON\r\n" +
"WHERE     \r\n" +
"(AGendaBase.AGB_TAG = @TAG) AND (AGendaBase.AGBStatus IN (2, 3)) \r\n" +
"AND \r\n" +
"(CONDOMINIOS.CONAuditor1_USU = @USUg) and (CONStatus = 1);";

        private string comandoSQL = String.Empty;
        private List<cStatusIndividual> ControlesInternos;
        private List<cStatusIndividual> ControlesInternosTAG;

        private cStatusIndividual cStatusInativo;

        private int FundoWidthAnterior = 0;

        private int reservaElevador = 30;
        private int SemCopia;

        private int WQuadroOriginal;

        /// <summary>
        /// Construtor
        /// </summary>
        public cStatusGeral()
        {
            InitializeComponent();

            if (FrameworkProc.datasets.dUSUarios.dUSUariosStGerentes.USUarios.FindByUSU(DSCentral.USU) != null)
            {
                lookupGerente1.USUSel = DSCentral.USU;
                lookupGerente1.Tipo = Framework.Lookup.LookupGerente.TipoUsuario.gerente;
            }
            else if (FrameworkProc.datasets.dUSUarios.dUSUariosStAssistentes.USUarios.FindByUSU(DSCentral.USU) != null)
            {
                lookupGerente1.USUSel = DSCentral.USU;
                lookupGerente1.Tipo = Framework.Lookup.LookupGerente.TipoUsuario.assistente;
            }
            else
                lookupGerente1.USUSel = -1;
            //PreparaControles();
            timer1.Enabled = true;
        }

        private ComponenteBase AbreTelaCheques()
        {
            FormPrincipalBase.FormPrincipal.ModuleInfoCollection.ShowModule("dllCheques.cChequesEmitir", FormPrincipalBase.FormPrincipal);
            return FormPrincipalBase.FormPrincipal.GetModuloRegistrado("dllCheques.cChequesEmitir");
            //return (cChequesEmitir)CompontesBasicos.FormPrincipalBase.FormPrincipal.GetModuloRegistrado(typeof(cChequesEmitir));
        }

        /*
        private const int WM_SETREDRAW = 0x000B;

        public static void Suspend(Control control)
        {
            Message msgSuspendUpdate = Message.Create(control.Handle, WM_SETREDRAW, IntPtr.Zero,
                IntPtr.Zero);

            NativeWindow window = NativeWindow.FromHandle(control.Handle);
            window.DefWndProc(ref msgSuspendUpdate);
        }

        public static void Resume(Control control,bool Filhos)
        {
            // Create a C "true" boolean as an IntPtr
            IntPtr wparam = new IntPtr(1);
            Message msgResumeUpdate = Message.Create(control.Handle, WM_SETREDRAW, wparam,
                IntPtr.Zero);

            NativeWindow window = NativeWindow.FromHandle(control.Handle);
            window.DefWndProc(ref msgResumeUpdate);

            control.Invalidate(Filhos);
        }*/

        private void AjustaPos()
        {

            if (FundoWidthAnterior == Fundo.Width)
                return;

            if (ControlesInternos == null)
                return;

            FundoWidthAnterior = Fundo.Width;

            try
            {
                if ((ControlesInternos.Count == 0) || (Fundo.Width == 0))
                    return;
                if (WQuadroOriginal == 0)
                    WQuadroOriginal = (ControlesInternos[0]).Width;
                if (WQuadroOriginal == 0)
                    return;
                int HQuadro = (ControlesInternos[0]).Height;
                int coluna = 1;
                int linha = 1;
                int QuadrosPorLinha = (Fundo.Width - reservaElevador) / (WQuadroOriginal + 2 * Borda);
                if (QuadrosPorLinha == 0)
                    return;
                int WQuadro = (Fundo.Width - reservaElevador) / QuadrosPorLinha;
                //Fundo.SuspendLayout();

                foreach (cStatusIndividual c in ControlesInternos)
                {
                    c.Top = (linha - 1) * (HQuadro + (2 * Borda));
                    c.Left = (coluna - 1) * WQuadro;
                    c.Width = WQuadro - (2 * Borda);
                    c.Height = HQuadro;
                    c.Visible = true;
                    coluna = coluna + 1;
                    if (coluna > QuadrosPorLinha)
                    {
                        linha++;
                        coluna = 1;
                    }
                }
            }
            catch
            {
            }

        }



        private static void CompletaAtivos()
        {
            string comando =
"INSERT INTO AGendaBase(AGB_CON,AGB_TAG,AGBDataInicio,AGBDataTermino,AGBMemo,AGBDescricao,AGB_USU,AGBDiaTodo,AGBSTATUS)\r\n" +
"SELECT     \r\n" +
"teste.CON as AGB_CON, \r\n" +
"teste.TAG as AGB_TAG,\r\n" +
"getdate() as AGBDataInicio,\r\n" +
"getdate() as AGBDataTermino,\r\n" +
"'cadastro autom�tico' as AGBMemo,\r\n" +
"teste.TAGTitulo as AGBDescricao,\r\n" +
"teste.TAGResp_USU as AGB_USU,\r\n" +
"1 as AGBDiaTodo,\r\n" +
"2 as AGBSTATUS\r\n" +
"FROM         (\r\n" +
"SELECT     CONDOMINIOS.CON, TipoAGendamento.TAG, TipoAGendamento.TAGTitulo, TipoAGendamento.TAGResp_USU\r\n" +
"FROM         CONDOMINIOS CROSS JOIN\r\n" +
"                      TipoAGendamento\r\n" +
"WHERE     (NOT (TipoAGendamento.TAG IN (5,6,10,11))) AND (CONDOMINIOS.CONStatus = 1) AND (CONDOMINIOS.CON_EMP = @P1)) AS teste \r\n" +
Environment.NewLine +
"LEFT OUTER JOIN\r\n" +
"                      AGendaBase AS AGendaBase_1 ON teste.CON = AGendaBase_1.AGB_CON AND teste.TAG = AGendaBase_1.AGB_TAG\r\n" +
"WHERE     (AGendaBase_1.AGB IS NULL);";
            VirMSSQL.TableAdapter.STTableAdapter.ExecutarSQLNonQuery(comando, Framework.DSCentral.EMP);
        }

        private void cStatusAlarmes_OnVerificaDadosManual(object sender, ref StatusStatus Status, ref int Atrasados)
        {
            int Carencia = 3;
            string ComandoBuscaAlarmes =
"SELECT COUNT(dbo.AVIsos.AVI) AS Total\r\n" +
"FROM dbo.AVIsos INNER JOIN\r\n" +
"dbo.CONDOMINIOS ON dbo.AVIsos.AVI_CON = dbo.CONDOMINIOS.CON\r\n" +
"WHERE (dbo.AVIsos.AVI_TAV in (14)) AND (dbo.CONDOMINIOS.CONStatus = 1) AND (NOT (dbo.AVIsos.AVIChave IS NULL)) AND (dbo.AVIsos.AVIDataI <= @P1)";
            string ComandoBuscaAlarmes1 =
"SELECT        COUNT(dbo.AVIsos.AVI) AS Total\r\n" +
"FROM            dbo.AVIsos INNER JOIN\r\n" +
"                         dbo.CONDOMINIOS ON dbo.AVIsos.AVI_CON = dbo.CONDOMINIOS.CON INNER JOIN\r\n" +
"                         dbo.CHEques ON dbo.CONDOMINIOS.CON = dbo.CHEques.CHE_CON AND dbo.AVIsos.AVIChave = dbo.CHEques.CHE\r\n" +
"WHERE        (dbo.AVIsos.AVI_TAV IN (9)) AND (dbo.CONDOMINIOS.CONStatus = 1) AND (dbo.AVIsos.AVIDataI <= @P1);";

            if (lookupGerente1.USUSel != -1)
                if (lookupGerente1.Tipo == Framework.Lookup.LookupGerente.TipoUsuario.gerente)
                    ComandoBuscaAlarmes =
"SELECT COUNT(dbo.AVIsos.AVI) AS Total\r\n" +
"FROM dbo.AVIsos INNER JOIN\r\n" +
"dbo.CONDOMINIOS ON dbo.AVIsos.AVI_CON = dbo.CONDOMINIOS.CON\r\n" +
"WHERE (dbo.AVIsos.AVI_TAV in (14)) AND (dbo.CONDOMINIOS.CONStatus = 1) AND (NOT (dbo.AVIsos.AVIChave IS NULL)) AND (dbo.AVIsos.AVIDataI <= @P1) and (CONAuditor1_USU = @P2)";
                else
                    ComandoBuscaAlarmes =
"SELECT COUNT(dbo.AVIsos.AVI) AS Total\r\n" +
"FROM dbo.AVIsos INNER JOIN\r\n" +
"dbo.CONDOMINIOS ON dbo.AVIsos.AVI_CON = dbo.CONDOMINIOS.CON\r\n" +
"WHERE (dbo.AVIsos.AVI_TAV in (14)) AND (dbo.CONDOMINIOS.CONStatus = 1) AND (NOT (dbo.AVIsos.AVIChave IS NULL)) AND (dbo.AVIsos.AVIDataI <= @P1) and (CONAuditor2_USU = @P2)";
            DateTime DataCorte = DateTime.Today.SomaDiasUteis(Carencia, Sentido.Traz);
            Atrasados = VirMSSQL.TableAdapter.STTableAdapter.BuscaEscalar_int(ComandoBuscaAlarmes, DataCorte, lookupGerente1.USUSel).GetValueOrDefault(0);
            Atrasados += VirMSSQL.TableAdapter.STTableAdapter.BuscaEscalar_int(ComandoBuscaAlarmes1, DataCorte, lookupGerente1.USUSel).GetValueOrDefault(0);
            Status = Atrasados == 0 ? StatusStatus.ok : StatusStatus.naook;
        }

        private void cStatusBalancete_OnVerificaDadosManual(object sender, ref StatusStatus Status, ref int Atrasados)
        {
            Atrasados = 0;
            string Comando =
"SELECT     BALancetes.BALdataFim, CONDOMINIOS.CONLimiteBal\r\n" +
"FROM         CONDOMINIOS INNER JOIN\r\n" +
"                      BALancetes ON CONDOMINIOS.CON = BALancetes.BAL_CON\r\n" +
"WHERE     (CONDOMINIOS.CONStatus = 1) AND (CONDOMINIOS.CON_EMP = @P1) AND (BALancetes.BALSTATUS IN (0, 1,2,3,4,5));";
            if (lookupGerente1.USUSel != -1)
                Comando =
"SELECT     BALancetes.BALdataFim, CONDOMINIOS.CONLimiteBal\r\n" +
"FROM         CONDOMINIOS INNER JOIN\r\n" +
"                      BALancetes ON CONDOMINIOS.CON = BALancetes.BAL_CON\r\n" +
"WHERE     (CONDOMINIOS.CONStatus = 1) AND (CONDOMINIOS.CON_EMP = @P1) AND (CONAuditor1_USU = @P2) AND (BALancetes.BALSTATUS IN (0, 1,2,3,4,5));";

            //System.Text.StringBuilder SB = new System.Text.StringBuilder();
            using (DataTable DT = VirMSSQL.TableAdapter.STTableAdapter.BuscaSQLTabela(Comando, Framework.DSCentral.EMP, lookupGerente1.USUSel))
                foreach (DataRow DR in DT.Rows)
                {
                    if ((DR["BALdataFim"] == DBNull.Value) || (DR["CONLimiteBal"] == DBNull.Value))
                        continue;
                    DateTime BALdataFim = (DateTime)DR["BALdataFim"];
                    int CONLimiteBal = (int)DR["CONLimiteBal"];
                    if (CONLimiteBal <= 0)
                        continue;
                    DateTime datacorte;
                    try
                    {
                        datacorte = new DateTime(BALdataFim.Year, BALdataFim.Month, CONLimiteBal);
                    }
                    catch
                    {
                        datacorte = new DateTime(BALdataFim.Year, BALdataFim.Month, 1).AddMonths(1).AddDays(-1);
                    }
                    if (datacorte <= BALdataFim)
                        datacorte = datacorte.AddMonths(1);
                    if (datacorte < DateTime.Today)
                    {
                        Atrasados++;
                        //SB.AppendFormat("BAL={0} dataFIM = {3:dd/MM/yyyy} dataCorte = {1:dd/MM/yyyy} Limite = {2}\r\n", DR["BAL"],datacorte, DR["CONLimiteBal"], DR["BALdataFim"]);
                    }

                }
            //if (lookupGerente1.USUSel == -1)
            //{
            //    System.Windows.Forms.Clipboard.SetText(SB.ToString());
            //    System.Windows.Forms.MessageBox.Show(SB.ToString());
            //}
            Status = (Atrasados == 0) ? StatusStatus.ok : StatusStatus.naook;
        }

        private void cStatusGeral_Load(object sender, EventArgs e)
        {
            RefreshDados();
        }

        private void cStatusIndividual1_OnCliqueBotao(object sender, EventArgs e)
        {
            ComponenteBase cChequesEmitirPadrao = AbreTelaCheques();
            cChequesEmitirPadrao.ChamadaGenerica(1, lookupGerente1.USUSel);
            //cChequesEmitirPadrao.selAtrasados(cChequesEmitir.Radio.Emitir,lookupGerente1.USUSel);
        }


        private void cStatusIndividual1_OnVerificaDadosManual(object sender, ref StatusStatus Status, ref int Atrasados)
        {
            Atrasados = AtrasadosEmitir;
            Status = (Atrasados == 0) ? StatusStatus.ok : StatusStatus.naook;
        }

        private void cStatusIndividual2_OnCliqueBotao(object sender, EventArgs e)
        {
            ComponenteBase cChequesEmitirPadrao = AbreTelaCheques();
            cChequesEmitirPadrao.ChamadaGenerica(2, lookupGerente1.USUSel);
            //cChequesEmitirPadrao.selAtrasados(cChequesEmitir.Radio.AguardaRetorno, lookupGerente1.USUSel);
        }

        private void cStatusIndividual2_OnVerificaDadosManual(object sender, ref StatusStatus Status, ref int Atrasados)
        {
            Atrasados = AtrasadosRetorno;
            Status = (Atrasados == 0) ? StatusStatus.ok : StatusStatus.naook;
        }

        private void cStatusIndividual3_OnVerificaDadosManual(object sender, ref StatusStatus Status, ref int Atrasados)
        {
            Atrasados = SemCopia;
            Status = (Atrasados == 0) ? StatusStatus.ok : StatusStatus.naook;
            Atrasados = 0;
            Status = StatusStatus.indefinido;
        }

        private void cStatusIndividual4_OnVerificaDadosManual(object sender, ref StatusStatus Status, ref int Atrasados)
        {
            if (lookupGerente1.USUSel == -1)
                dStatusGeral.dStatusGeralSt.CONDOMINIOSBOLETOSTableAdapter.Fill(dStatusGeral.dStatusGeralSt.CONDOMINIOSBOLETOS, Framework.DSCentral.EMP);
            else
                dStatusGeral.dStatusGeralSt.CONDOMINIOSBOLETOSTableAdapter.FillByGerente(dStatusGeral.dStatusGeralSt.CONDOMINIOSBOLETOS, Framework.DSCentral.EMP, lookupGerente1.USUSel);
            int ContaAt = 0;
            foreach (dStatusGeral.CONDOMINIOSBOLETOSRow rowTestar in dStatusGeral.dStatusGeralSt.CONDOMINIOSBOLETOS)
                if (rowTestar.IsultimaDataNull())
                    ContaAt++;
                    //Detalhes += string.Format("{0}: Nenhuma emiss�o\r\n",rowTestar.CONCodigo);
                else
                    if (rowTestar.ultimaData.AddMonths(1).AddDays(-5) <= DateTime.Today)
                    ContaAt++;
                    //Detalhes += string.Format("{0}: �ltima emiss�o:{1:dd/MM/yyyy}\r\n", rowTestar.CONCodigo,rowTestar.ultimaData);
            ;
            Atrasados = ContaAt;
            Status = (Atrasados == 0) ? StatusStatus.ok : StatusStatus.naook;
            cStatusIndividual Chamador = (cStatusIndividual)sender;
            //Chamador.Obs = Detalhes;
        }

        private void cStatusSaldos_OnVerificaDadosManual(object sender, ref StatusStatus Status, ref int Atrasados)
        {
            string ComandoSQL;
            if (lookupGerente1.USUSel == -1)
                ComandoSQL =
"SELECT     COUNT(SaldoContaCorrente.SCC) AS Expr1\r\n" +
"FROM         SaldoContaCorrente INNER JOIN\r\n" +
"                          (SELECT     MAX(SaldoContaCorrente_1.SCCData) AS Data, ContaCorrenTe.CCT AS chave\r\n" +
"                            FROM          SaldoContaCorrente AS SaldoContaCorrente_1 INNER JOIN\r\n" +
"                                                   ContaCorrenTe ON SaldoContaCorrente_1.SCC_CCT = ContaCorrenTe.CCT INNER JOIN\r\n" +
"                                                   CONDOMINIOS ON ContaCorrenTe.CCT_CON = CONDOMINIOS.CON\r\n" +
"                            WHERE      (CONDOMINIOS.CON_EMP = 1) AND (CONDOMINIOS.CONStatus = 1) and (CONDOMINIOS.CONOculto = 0) and (CCTAtiva = 1) and (CCTTipo <> 4)\r\n" +
"                            GROUP BY CONDOMINIOS.CONCodigo, ContaCorrenTe.CCT) AS t1 ON SaldoContaCorrente.SCCData = t1.Data AND SaldoContaCorrente.SCC_CCT = t1.chave\r\n" +
"WHERE     (SaldoContaCorrente.SCCValorF < 0)";
            else
                ComandoSQL = string.Format(
"SELECT     COUNT(SaldoContaCorrente.SCC) AS Expr1\r\n" +
"FROM         SaldoContaCorrente INNER JOIN\r\n" +
"                          (SELECT     MAX(SaldoContaCorrente_1.SCCData) AS Data, ContaCorrenTe.CCT AS chave\r\n" +
"                            FROM          SaldoContaCorrente AS SaldoContaCorrente_1 INNER JOIN\r\n" +
"                                                   ContaCorrenTe ON SaldoContaCorrente_1.SCC_CCT = ContaCorrenTe.CCT INNER JOIN\r\n" +
"                                                   CONDOMINIOS ON ContaCorrenTe.CCT_CON = CONDOMINIOS.CON\r\n" +
"                            WHERE      (CON_EMP = 1) AND (CONStatus = 1) and (CONOculto = 0) and (CONAuditor1_USU = {0})  and (CCTAtiva = 1) and (CCTTipo <> 4)\r\n" +
"                            GROUP BY CONDOMINIOS.CONCodigo, ContaCorrenTe.CCT) AS t1 ON SaldoContaCorrente.SCCData = t1.Data AND SaldoContaCorrente.SCC_CCT = t1.chave\r\n" +
"WHERE     (SaldoContaCorrente.SCCValorF < 0)", lookupGerente1.USUSel);




            VirMSSQL.TableAdapter.STTableAdapter.BuscaEscalar(ComandoSQL, out Atrasados);
            if (Atrasados > 0)
                Status = StatusStatus.naook;
            else
                Status = StatusStatus.ok;

        }

        private ComponenteBase cTelaCheques()
        {
            return FormPrincipalBase.FormPrincipal.GetModuloRegistrado("dllCheques.cChequesEmitir");
        }



        private void desligar()
        {
            if (ControlesInternos != null)
                foreach (cStatusIndividual c in ControlesInternos)
                    c.Status = StatusStatus.indefinido;
        }

        private void Fundo_Resize(object sender, EventArgs e)
        {
            FuncoesBasicasProc.TravaPaint(AjustaPos, Fundo, true);
        }

        private void lookupGerente1_alterado(object sender, EventArgs e)
        {
            desligar();
        }

        private void PreparaControles()
        {
            ControlesInternos = new List<cStatusIndividual>();
            ControlesInternosTAG = new List<cStatusIndividual>();


            /*
            foreach (Control c in Fundo.Controls)
            {
                if (c is cStatusIndividual)
                     ControlesInternos.Add((cStatusIndividual)c);                   
            }
            */
            ControlesInternos.Add(new cStatusIndividual(Fundo, "Alarmes Vencidos", "Framework.Alarmes.cMeusAlarmes", new cStatusIndividual.VerificaDadosManualEventHandler(cStatusAlarmes_OnVerificaDadosManual)));
            //ControlesInternos.Add(new cStatusIndividual(Fundo, "Aguarda C�pia de Cheque", "dllCheques.cChequesEmitir", new cStatusIndividual.VerificaDadosManualEventHandler(cStatusIndividual3_OnVerificaDadosManual)));
            ControlesInternos.Add(new cStatusIndividual(Fundo, "Emiss�o de Cheques", "dllCheques.cChequesEmitir",
                                                               new cStatusIndividual.VerificaDadosManualEventHandler(cStatusIndividual1_OnVerificaDadosManual),
                                                               new EventHandler(cStatusIndividual1_OnCliqueBotao)));
            ControlesInternos.Add(new cStatusIndividual(Fundo, "Fluxo de caixa", "dllCaixa.cLancamentosFuturos"));
            ControlesInternos.Add(new cStatusIndividual(Fundo, "Retorno de Cheques", "dllCheques.cChequesEmitir",
                                                               new cStatusIndividual.VerificaDadosManualEventHandler(cStatusIndividual2_OnVerificaDadosManual),
                                                               new EventHandler(cStatusIndividual2_OnCliqueBotao)));
            ControlesInternos.Add(new cStatusIndividual(Fundo, "Saldos", "dllCaixa.cSaldosGeral", new cStatusIndividual.VerificaDadosManualEventHandler(cStatusSaldos_OnVerificaDadosManual)));
            ControlesInternos.Add(new cStatusIndividual(Fundo, "Balancetes", "Balancete.cGradeBalancetes", new cStatusIndividual.VerificaDadosManualEventHandler(cStatusBalancete_OnVerificaDadosManual)));
            cStatusInativo = new cStatusIndividual(Fundo, "Condominios Inativos", "Cadastros.Condominios.cCondominiosGrade");
            ControlesInternos.Add(cStatusInativo);
            ControlesInternos.Add(new cStatusIndividual(Fundo, "Emiss�o de boletos", "Boletos.geracao.cGrade", new cStatusIndividual.VerificaDadosManualEventHandler(cStatusIndividual4_OnVerificaDadosManual)));
            ControlesInternos.Add(new cStatusIndividual(Fundo, "Cobran�a"));

            foreach (dAGendaBase.TipoAGendamentoRow rowTAG in dAGendaBase.dAGendaBaseSt.TipoAGendamento)
            {
                cStatusIndividual NovoControle = new cStatusIndividual(Fundo, rowTAG.TAGTitulo);
                ControlesInternos.Add(NovoControle);
                ControlesInternosTAG.Add(NovoControle);
                //delta++;
                //NovoControle.comandoSQL = comandobase.Replace("@TAG", rowTAG.TAG.ToString());
                NovoControle.strComponente = "Calendario.Agenda.cAgendaGrade";
                NovoControle.TAG = rowTAG.TAG;
            }
            FuncoesBasicasProc.TravaPaint(AjustaPos, Fundo, true);
        }

        private void Recalcular()
        {
            //return;
            if (ControlesInternos == null)
                return;
            CompletaAtivos();
            VerificaCheques();
            desligar();
            if (lookupGerente1.USUSel == -1)
                cStatusInativo.comandoSQL = string.Format("SELECT COUNT(CON) FROM CONDOMINIOS WHERE (CONStatus = 2) AND (CON_EMP = {0})", Framework.DSCentral.EMP);
            else
                cStatusInativo.comandoSQL = string.Format("SELECT COUNT(CON) FROM CONDOMINIOS WHERE (CONStatus = 2) AND (CON_EMP = {0}) AND (CONAuditor1_USU = {1})", Framework.DSCentral.EMP, lookupGerente1.USUSel);
            foreach (cStatusIndividual c in ControlesInternosTAG)
                if (lookupGerente1.USUSel == -1)
                    c.comandoSQL = comandobase.Replace("@TAG", c.TAG.ToString());
                else
                    c.comandoSQL = comandobaseG.Replace("@TAG", c.TAG.ToString()).Replace("@USUg", lookupGerente1.USUSel.ToString());

            foreach (cStatusIndividual c in ControlesInternos)
            {
                c.Gerente = lookupGerente1.USUSel;
                c.VerificaDados();
            }
        }

        /*
        private void cStatusIndividual3_OnCliqueBotao(object sender, EventArgs e)
        {
            ComponenteBase cChequesEmitirPadrao = AbreTelaCheques();
            cChequesEmitirPadrao.ChamadaGenerica(3, lookupGerente1.USUSel);
            //cChequesEmitirPadrao.selAtrasados(cChequesEmitir.Radio.AguardaCopia, lookupGerente1.USUSel);
        }
        */

        private void simpleButton1_Click(object sender, EventArgs e)
        {
            Recalcular();
        }



        private void timer1_Tick(object sender, EventArgs e)
        {
            timer1.Enabled = false;
            PreparaControles();
            Recalcular();
        }

        private void VerificaCheques()
        {
            AtrasadosEmitir = AtrasadosRetorno = SemCopia = 0;

            using (DataTable DT = VirMSSQL.TableAdapter.STTableAdapter.BuscaSQLTabela(Comando, DateTime.Today.AddMonths(1)))
                foreach (DataRow DR in DT.Rows)
                {
                    if ((DR["CONMalote"] == DBNull.Value) || (DR["CONProcuracao"] == DBNull.Value) || (DR["CHETipo"] == DBNull.Value) || (DR["CHEVencimento"] == DBNull.Value) || (DR["CHEStatus"] == DBNull.Value))
                        continue;
                    if (lookupGerente1.USUSel != -1)
                    {
                        if (DR["CONAuditor1_USU"] == DBNull.Value)
                            continue;
                        if ((int)DR["CONAuditor1_USU"] != lookupGerente1.USUSel)
                            continue;
                    }
                    int CONMalote = (int)DR["CONMalote"];
                    int cHEStatus = (int)DR["CHEStatus"];
                    bool CONProcuracao = (bool)DR["CONProcuracao"];
                    int CHETipo = (int)DR["CHETipo"];
                    DateTime CHEVencimento = (DateTime)DR["CHEVencimento"];
                    DateTime CHEIdaData;
                    DateTime CHERetornoData;
                    //malote M1 = null;
                    //malote M2 = null;
                    //Caso particular onde o cheque retorna no malote da manh� no dia do vencimento
                    //bool RetornaNoVencimento;
                    if ((CHEStatus)cHEStatus == CHEStatus.CompensadoAguardaCopia)
                        SemCopia++;
                    else
                    {
                        DateTime[] datas = (DateTime[])(cTelaCheques().ChamadaGenerica(4, CHETipo, CONProcuracao, (malote.TiposMalote)CONMalote, CHEVencimento, DateTime.Now)[0]);
                        //DateTime[] datas = Cheque.CalculaDatas((dCheque.PAGTipo)CHETipo, CONProcuracao, (malote.TiposMalote)CONMalote, CHEVencimento, DateTime.Now, ref M1, ref M2, out RetornaNoVencimento);                      
                        if ((CHEStatus)cHEStatus == CHEStatus.Cadastrado)
                        {
                            CHEIdaData = datas[0];
                            if (CHEIdaData < DateTime.Now)
                                AtrasadosEmitir++;
                        }
                        else
                            if ((CHEStatus)cHEStatus == CHEStatus.AguardaRetorno)
                        {
                            System.Collections.ArrayList ParaRetornar = new System.Collections.ArrayList(new CHEStatus[] { CHEStatus.Cadastrado, CHEStatus.Aguardacopia, CHEStatus.AguardaRetorno, CHEStatus.CompensadoAguardaCopia });
                            if (ParaRetornar.Contains((CHEStatus)cHEStatus))
                            {
                                CHERetornoData = datas[1];
                                if (CHERetornoData < DateTime.Now)
                                    AtrasadosRetorno++;
                            }
                        }
                    }
                }
;
        }

        /// <summary>
        /// 
        /// </summary>
        public override void RefreshDados()
        {
            Recalcular();
        }




    }
}
