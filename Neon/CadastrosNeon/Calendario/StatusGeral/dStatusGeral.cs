﻿namespace Calendario.StatusGeral {


    partial class dStatusGeral
    {
        private static dStatusGeral _dStatusGeralSt;

        /// <summary>
        /// dataset estático:dStatusGeral
        /// </summary>
        public static dStatusGeral dStatusGeralSt
        {
            get
            {
                if (_dStatusGeralSt == null)
                    _dStatusGeralSt = new dStatusGeral();
                return _dStatusGeralSt;
            }
        }

        private dStatusGeralTableAdapters.CONDOMINIOSBOLETOSTableAdapter cONDOMINIOSBOLETOSTableAdapter;
        /// <summary>
        /// TableAdapter padrão para tabela: CONDOMINIOSBOLETOS
        /// </summary>
        public dStatusGeralTableAdapters.CONDOMINIOSBOLETOSTableAdapter CONDOMINIOSBOLETOSTableAdapter
        {
            get
            {
                if (cONDOMINIOSBOLETOSTableAdapter == null)
                {
                    cONDOMINIOSBOLETOSTableAdapter = new dStatusGeralTableAdapters.CONDOMINIOSBOLETOSTableAdapter();
                    cONDOMINIOSBOLETOSTableAdapter.TrocarStringDeConexao();
                };
                return cONDOMINIOSBOLETOSTableAdapter;
            }
        }
    }
}
