using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Windows.Forms;

namespace Calendario.StatusGeral
{
    /// <summary>
    /// 
    /// </summary>
    public partial class cStatusIndividual : CompontesBasicos.ComponenteBase
    {
        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="Status"></param>
        /// <param name="Atrasados"></param>
        public delegate void VerificaDadosManualEventHandler(object sender,ref StatusStatus Status,ref int Atrasados);

        private string _Obs;

        /// <summary>
        /// 
        /// </summary>
        public string Obs
        {
            get { return _Obs; }
            set { 
                _Obs = value;
                BotObs.Visible = ((_Obs != "") && (_Obs != null));
            }
        }


        /// <summary>
        /// 
        /// </summary>
        public cStatusIndividual()
        {
            InitializeComponent();            
        }

        /// <summary>
        /// Construtor
        /// </summary>
        /// <param name="_Parent">Parent</param>
        /// <param name="_Titulo">T�tulo</param>
        /// <param name="_strComponente">Nome do componente</param>
        /// <param name="H">delegate do evento OnVerificaDadosManual</param>
        /// <param name="H1">delegate do evento OnCliqueBotao</param>
        public cStatusIndividual(Control _Parent, string _Titulo,string _strComponente = "",cStatusIndividual.VerificaDadosManualEventHandler H = null,EventHandler H1 = null):this()
        {            
            Titulo = _Titulo;            
            if (_strComponente != "")
                strComponente = _strComponente;
            if(H != null)
                OnVerificaDadosManual += H;
            if (H1 != null)
                OnCliqueBotao += H1;
            Parent = _Parent;
        }

        /// <summary>
        /// 
        /// </summary>
        public string _comandoSQL;

        /// <summary>
        /// 
        /// </summary>
        [Category("Virtual Software"), Description("Comando")]
        [DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
        public string comandoSQL
        {
            get { return _comandoSQL; }
            set { _comandoSQL = value; }
        }

        
        /// <summary>
        /// Evento para verifica��o dos dados
        /// </summary>
        /// <remarks>� chamado pelo VerificaDados</remarks>
        [Category("Virtual Software")]
        [Description("Verifica��o Manual")]
        public event VerificaDadosManualEventHandler OnVerificaDadosManual;

        /// <summary>
        /// Evento no clique do botao
        /// </summary>
        /// <remarks></remarks>
        [Category("Virtual Software")]
        [Description("Botao")]
        public event EventHandler OnCliqueBotao;
        
        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public bool VerificaDados()
        {
            int atrasados = 0;
            if (OnVerificaDadosManual != null)
            {
                StatusStatus NovoStatus = Status;
                OnVerificaDadosManual(this,ref NovoStatus,ref atrasados);
                Status = NovoStatus;
            }
            else
            {
                if ((comandoSQL == null) || (comandoSQL == ""))
                    Status = StatusStatus.indefinido;
                else
                {

                    VirMSSQL.TableAdapter.STTableAdapter.BuscaEscalar(comandoSQL, out atrasados);
                    if (atrasados > 0)
                        Status = StatusStatus.naook;
                    else
                        Status = StatusStatus.ok;                    
                }
            }
            spinEdit1.Value = atrasados;
            Application.DoEvents();
            return (Status == StatusStatus.ok);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="e"></param>
        protected override void OnLoad(EventArgs e)
        {
            base.OnLoad(e);
            pictureBoxP.Location = pictureBoxN.Location;
            TituloTela.Text = Titulo;
            if (_ImagemBotao != null)
                simpleButton1.Image = _ImagemBotao;
            else if (strComponente != null)
            {
                if (CompontesBasicos.FormPrincipalBase.FormPrincipal != null)
                {
                    CompontesBasicos.ModuleInfo Mi = CompontesBasicos.FormPrincipalBase.FormPrincipal.ModuleInfoCollection[strComponente];
                    if (Mi != null)
                    {
                        if (Mi.ImagemP != null)
                            simpleButton1.Image = Mi.ImagemP;
                        else if (Mi.ImagemG != null)
                            simpleButton1.Image = Mi.ImagemG;
                    }
                }
            }
            //VerificaDados();
            //MessageBox.Show("x");
        }

        private StatusStatus _Status = StatusStatus.indefinido;

        /// <summary>
        /// 
        /// </summary>
        [Category("Virtual Software"), Description("Status")]
        public StatusStatus Status
        {
            get { return _Status; }
            set
            {
                _Status = value;
                BackColor = virEnumStatusStatus.virEnumStatusStatusSt.GetCor(value);
                pictureBoxN.Visible = (value == StatusStatus.naook);
                pictureBoxP.Visible = (value == StatusStatus.ok);
                if (_Status == StatusStatus.indefinido)
                    spinEdit1.Value = 0;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        public Image _ImagemBotao;

        /// <summary>
        /// imagem do botao
        /// </summary>
        [Category("Virtual Software"), Description("Imagem do bot�o")]
        [DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
        public Image ImagemBotao
        {
            get { return _ImagemBotao; }
            set { _ImagemBotao = value; }
        }

        string _strComponente;

        /// <summary>
        /// 
        /// </summary>
        [Category("Virtual Software"), Description("Componente")]
        [DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
        public string strComponente
        {
            get { return _strComponente; }
            set { _strComponente = value; }
        }

        private int _TAG;

        /// <summary>
        /// 
        /// </summary>
        [Category("Virtual Software"), Description("TAG")]
        [DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
        public int TAG
        {
            get { return _TAG; }
            set { _TAG = value; }
        }

        private int _Gerente = -1;

        /// <summary>
        /// 
        /// </summary>
        [Category("Virtual Software"), Description("Gerente")]
        [DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
        public int Gerente
        {
            get { return _Gerente; }
            set { _Gerente = value; }
        }

        private void simpleButton1_Click(object sender, EventArgs e)
        {
            if (Status == StatusStatus.indefinido)
                return;
            if (OnCliqueBotao != null)
            {
                OnCliqueBotao(sender, e);
            }
            else
            {
                if (_strComponente != null)
                {
                    CompontesBasicos.FormPrincipalBase.FormPrincipal.ModuleInfoCollection.ShowModule(strComponente, CompontesBasicos.FormPrincipalBase.FormPrincipal);
                    if (strComponente == "Calendario.Agenda.cAgendaGrade")
                    {
                        Calendario.Agenda.cAgendaGrade ModuloAtivo = (Calendario.Agenda.cAgendaGrade)CompontesBasicos.FormPrincipalBase.FormPrincipal.GetModuloRegistrado(strComponente);
                        ModuloAtivo.FiltroContas(TAG,Gerente);
                    }
                    else
                    {                        
                        CompontesBasicos.ComponenteBase CompontesBasico = CompontesBasicos.FormPrincipalBase.FormPrincipal.GetModuloRegistrado(strComponente);
                        CompontesBasico.ChamadaGenerica(Gerente);
                    }
                }                
            }
        }

        private void BotObs_Click(object sender, EventArgs e)
        {
            MessageBox.Show(Obs);
        }

      /*
        /// <summary>
        /// Titulo
        /// </summary>
        [Category("Virtual Software"), Description("T�tulo")]
        public bool somenteleitura
        {
            get
            {
                return _somenteleitura;
            }
            set
            {
                _somenteleitura = value;
            }
        }
        */
    }

    /// <summary>
    /// Enumera��o para campo 
    /// </summary>
    public enum StatusStatus
    {
        /// <summary>
        /// 
        /// </summary>
        ok,
        /// <summary>
        /// 
        /// </summary>
        naook,
        /// <summary>
        /// 
        /// </summary>
        indefinido
    }

    /// <summary>
    /// virEnum para campo 
    /// </summary>
    public class virEnumStatusStatus : dllVirEnum.VirEnum
    {
        /// <summary>
        /// Construtor padrao
        /// </summary>
        /// <param name="Tipo">Tipo</param>
        public virEnumStatusStatus(Type Tipo)
            :
            base(Tipo)
        {
        }

        private static virEnumStatusStatus _virEnumStatusStatusSt;

        /// <summary>
        /// VirEnum est�tico para campo StatusStatus
        /// </summary>
        public static virEnumStatusStatus virEnumStatusStatusSt
        {
            get
            {
                if (_virEnumStatusStatusSt == null)
                {
                    _virEnumStatusStatusSt = new virEnumStatusStatus(typeof(StatusStatus));
                    _virEnumStatusStatusSt.GravaNomes(StatusStatus.ok, "OK");
                    _virEnumStatusStatusSt.GravaNomes(StatusStatus.naook, "ATEN��O");
                    _virEnumStatusStatusSt.GravaNomes(StatusStatus.indefinido, "???");
                    _virEnumStatusStatusSt.GravaCor(StatusStatus.ok, Color.FromArgb(192, 255, 192));
                    _virEnumStatusStatusSt.GravaCor(StatusStatus.naook, Color.FromArgb(255, 128, 128));
                    _virEnumStatusStatusSt.GravaCor(StatusStatus.indefinido, Color.FromArgb(255,255,192));
                    
                }
                return _virEnumStatusStatusSt;
            }
        }
    }
}
