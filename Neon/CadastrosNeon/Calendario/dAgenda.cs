﻿namespace Calendario {

    

    partial class dAgenda
    {
        private static dAgenda _dAgenda;

        /// <summary>
        /// 
        /// </summary>
        public static dAgenda DAgenda
        {
            get {
                if (_dAgenda == null) {
                    _dAgenda = new dAgenda();
                }
                return _dAgenda; 
            }            
        }

        
        
        private dAgendaTableAdapters.HISTORICOSTableAdapter hISTORICOSTableAdapter;
        private dAgendaTableAdapters.ASSuntoTableAdapter aSSuntoTableAdapter;

        /// <summary>
        /// 
        /// </summary>
        public dAgendaTableAdapters.ASSuntoTableAdapter ASSuntoTableAdapter
        {
            get
            {
                if (aSSuntoTableAdapter == null)
                {
                    aSSuntoTableAdapter = new dAgendaTableAdapters.ASSuntoTableAdapter();
                    aSSuntoTableAdapter.TrocarStringDeConexao(VirDB.Bancovirtual.TiposDeBanco.SQL);
                };
                return aSSuntoTableAdapter;
            }

        }

        /// <summary>
        /// 
        /// </summary>
        public dAgendaTableAdapters.HISTORICOSTableAdapter HISTORICOSTableAdapter
        {
            get
            {
                if (hISTORICOSTableAdapter == null)
                {
                    hISTORICOSTableAdapter = new dAgendaTableAdapters.HISTORICOSTableAdapter();
                    hISTORICOSTableAdapter.TrocarStringDeConexao(VirDB.Bancovirtual.TiposDeBanco.SQL);
                };
                return hISTORICOSTableAdapter;
            }

        }
    }
}
