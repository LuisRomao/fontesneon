using System;
using System.Drawing;
using System.Drawing.Drawing2D;
using DevExpress.XtraPrinting;
using DevExpress.XtraReports.UI;

namespace dllImpresso
{
    /// <summary>
    /// Classe est�tica para gerar efeitos nos impressos
    /// </summary>
    static public class ImpGeradorDeEfeitos
    {

        private static Color C1;
        private static Color C2;
        private static Brush brushPreto;
        //public static Brush brushTrans;
        private static Brush brushC1;

        static ImpGeradorDeEfeitos()
        {
            C1 = Color.FromArgb(200, 200, 200);
            C2 = Color.FromArgb(255, 255, 255);
            brushPreto = new SolidBrush(Color.Black);
            //brushTrans = new SolidBrush(Color.Transparent);
            brushC1 = new SolidBrush(Color.FromArgb(150, 150, 150));
        }

        /// <summary>
        /// Funcao para ser chamada a partir do DRAW. Desenha os cantos redondos
        /// </summary>
        /// <param name="sender">Chamados, repassar o do DRAW</param>
        /// <param name="e">Repassar o do DRAW</param>
        /// <param name="Raio">Raio do canto</param>
        public static void CantosRedondos(object sender, DrawEventArgs e, Int32 Raio)
        {

            Rectangle rect = Rectangle.Truncate(e.Bounds);
            rect.Width++;
            Rectangle rectD;
            Rectangle rectM2;
            Rectangle rectM;
            Rectangle rectE = rectD = rectM2 = rectM = rect;
            rectE.Width = rectD.Width = 2 * Raio;
            rectD.X = rect.Right - 2 * Raio;
            rectD.Height = rectE.Height = 2 * Raio;
            rectM.X = rect.X + Raio;
            rectM.Width = rect.Width - (2 * Raio);
            rectM.Height = Raio;
            rectM2.Y = rectM2.Y + Raio;
            rectM2.Height = rectM2.Height - Raio;            
            //XRControl control = e.Data.Control;
            Brush brush = new SolidBrush(e.Brick.BackColor);
            rect.Width++;            


            ////////////
            e.UniGraphics.FillRectangle(Brushes.White, rect);
            e.UniGraphics.FillEllipse(brush, rectE.X, rectE.Y, 2 * Raio, 2 * Raio);
            e.UniGraphics.FillEllipse(brush, rectD.X, rectD.Y, 2 * Raio, 2 * Raio);
            e.UniGraphics.FillRectangle(brush, rectM);
            e.UniGraphics.FillRectangle(brush, rectM2);
            if ((e.Brick != null) && (e.Brick.Text != null))
            {
                brush = new SolidBrush(e.Brick.Style.ForeColor);
                e.UniGraphics.DrawString(e.Brick.Text.ToString(), e.Brick.Style.Font, brush, rect, e.Brick.Style.StringFormat.Value);
            };
            ////////////

            /*
            if (e.UniGraphics is GdiGraphics)
            {

                GdiGraphics graph = (GdiGraphics)e.UniGraphics;
                graph.Graphics.FillRectangle(Brushes.White, rect);
                graph.Graphics.FillPie(brush, rectE, 180, 90);
                graph.Graphics.FillPie(brush, rectD, -90, 90);
                graph.Graphics.FillRectangle(brush, rectM);
                graph.Graphics.FillRectangle(brush, rectM2);
                if ((e.Brick != null) && (e.Brick.Text != null))
                {                    
                    brush = new SolidBrush(e.Brick.Style.ForeColor);                    
                    graph.DrawString(e.Brick.Text.ToString(), e.Brick.Style.Font, brush, rect,e.Brick.Style.StringFormat.Value);
                };

            }
            else
            {                
                //teste
                brush = new SolidBrush(Color.Red);

                //DevExpress.XtraPrinting.Export.Pdf.PdfGraphics graph = (DevExpress.XtraPrinting.Export.Pdf.PdfGraphics)e.UniGraphics;
                //DevExpress.Printing.Core.NativePdfExport.PdfGraphics graph = (DevExpress.Printing.Core.NativePdfExport.PdfGraphics)e.UniGraphics;
                e.UniGraphics.FillRectangle(Brushes.White, rect);
                e.UniGraphics.FillEllipse(brush, rectE.X, rectE.Y, 2 * Raio, 2 * Raio);
                e.UniGraphics.FillEllipse(brush, rectD.X, rectD.Y, 2 * Raio, 2 * Raio);
                e.UniGraphics.FillRectangle(brush, rectM);
                e.UniGraphics.FillRectangle(brush, rectM2);
                if ((e.Brick != null) && (e.Brick.Text != null))
                {                    
                    brush = new SolidBrush(e.Brick.Style.ForeColor);                    
                    e.UniGraphics.DrawString(e.Brick.Text.ToString(), e.Brick.Style.Font, brush, rect, e.Brick.Style.StringFormat.Value);
                };
            }*/

        }

        /// <summary>
        /// Tipo de arredondamento
        /// </summary>
        public enum TipoDraw
        {
            /// <summary>
            /// 
            /// </summary>
            Trunca,
            /// <summary>
            /// 
            /// </summary>
            Arredonda,
            /// <summary>
            /// 
            /// </summary>
            Infla
        }

        /// <summary>
        /// Funcao para ser chamada a partir do DRAW. Desenha o fundo em degrade
        /// </summary>
        /// <param name="sender">Repassar o do DRAW</param>
        /// <param name="e">Repassar o do DRAW</param>
        /// <param name="Tipo">Tipo de arredondamento</param>
        public static void Degrade(object sender, DrawEventArgs e, TipoDraw Tipo = TipoDraw.Trunca )
        {
            Rectangle rect;
            switch (Tipo)
            {
                case TipoDraw.Trunca:
                    rect = Rectangle.Truncate(e.Bounds);
                    break;
                case TipoDraw.Arredonda:
                    rect = Rectangle.Round(e.Bounds);
                    break;
                default:
                    rect = Rectangle.Ceiling(e.Bounds);
                    break;
            }
            
            if ((rect.Width == 0) || (rect.Height == 0))
                return;
            LinearGradientBrush Grad = new LinearGradientBrush(rect, C1, C2, 0.0);
                  
         
            
            ///////////////////////
            e.UniGraphics.FillRectangle(Grad, rect);
            if ((e.Brick != null) && (e.Brick.Text != null))                        
                e.UniGraphics.DrawString(e.Brick.Text, e.Brick.Style.Font, brushPreto, rect, e.Brick.Style.StringFormat.Value);            
            ///////////////////////

            /*
            if (e.UniGraphics is GdiGraphics)
            {

                GdiGraphics graph = (GdiGraphics)e.UniGraphics;
                graph.Graphics.FillRectangle(Grad, rect);
                if ((e.Brick != null) && (e.Brick.Text != null))                                    
                    graph.DrawString(e.Brick.Text, e.Brick.Style.Font, brushPreto, rect, e.Brick.Style.StringFormat.Value);                
            }
            else
            {   
                //este � o normal 
                //e.UniGraphics.FillRectangle(brushC1, rect);
                //este � teste
                e.UniGraphics.FillRectangle(Grad, rect);

                //O gerador de pdf n�o suporta o gradiente
                //graph.FillRectangle(Grad, rect);
                
                if ((e.Brick != null) && (e.Brick.Text != null))
                {
                    //brush = new SolidBrush(e.Brick.Style.ForeColor);
                    e.UniGraphics.DrawString(e.Brick.Text, e.Brick.Style.Font, brushPreto, rect, e.Brick.Style.StringFormat.Value);                    
                }
            }*/            
        }

        #region Controles din�mincos
        /// <summary>
        /// Cria um clone de um controle
        /// </summary>
        /// <param name="Origem"></param>
        /// <param name="Destino"></param>
        /// <param name="SufNome"></param>
        public static void ClonaXRControl(XRControl Origem, XRControl Destino, string SufNome)
        {
            Destino.Name = string.Format("{0}_{1}", Origem.Name, SufNome);
            Destino.BackColor = Origem.BackColor;
            Destino.BorderColor = Origem.BorderColor;
            Destino.BorderDashStyle = Origem.BorderDashStyle;
            Destino.Borders = Origem.Borders;
            Destino.BorderWidth = Origem.BorderWidth;
            Destino.CanGrow = Origem.CanGrow;
            Destino.Dpi = Origem.Dpi;
            Destino.Font = Origem.Font;
            Destino.ForeColor = Origem.ForeColor;
            Destino.Padding = Origem.Padding;
            Destino.SizeF = Origem.SizeF;
            Destino.StylePriority.UseBackColor = Origem.StylePriority.UseBackColor;
            Destino.StylePriority.UseBorderColor = Origem.StylePriority.UseBorderColor;
            Destino.StylePriority.UseBorderDashStyle = Origem.StylePriority.UseBorderDashStyle;
            Destino.StylePriority.UseBorders = Origem.StylePriority.UseBorders;
            Destino.StylePriority.UseBorderWidth = Origem.StylePriority.UseBorderWidth;
            Destino.StylePriority.UseFont = Origem.StylePriority.UseFont;
            Destino.StylePriority.UseForeColor = Origem.StylePriority.UseForeColor;
            Destino.StylePriority.UseTextAlignment = Origem.StylePriority.UseTextAlignment;
            Destino.TextAlignment = Origem.TextAlignment;
            Destino.Location = Origem.Location;
            Destino.Text = Origem.Text;
        }

        /// <summary>
        /// Cria um clone de um controle
        /// </summary>
        /// <param name="Origem"></param>
        /// <param name="SufNome"></param>
        /// <returns></returns>
        public static XRLabel ClonaXRLabel(XRLabel Origem, string SufNome)
        {
            XRLabel Clone = new XRLabel();
            ClonaXRControl(Origem, Clone, SufNome);
            Clone.Multiline = Origem.Multiline;
            Clone.WordWrap = Origem.WordWrap;
            return Clone;
        }

        /// <summary>
        /// Cria um clone de um controle
        /// </summary>
        /// <param name="Origem"></param>
        /// <param name="SufNome"></param>
        /// <returns></returns>
        public static XRLine ClonaXRLine(XRLine Origem, string SufNome)
        {
            XRLine Clone = new XRLine();
            ClonaXRControl(Origem, Clone, SufNome);
            Clone.LineWidth = Origem.LineWidth;
            return Clone;
        }

        /// <summary>
        /// Cria um clone de um controle
        /// </summary>
        /// <param name="Origem"></param>
        /// <param name="SufNome"></param>
        /// <returns></returns>
        public static XRPanel ClonaXRPanel(XRPanel Origem, string SufNome)
        {
            XRPanel Clone = new XRPanel();
            ClonaXRControl(Origem, Clone, SufNome);
            
            return Clone;
        }

        #endregion
    }
}
