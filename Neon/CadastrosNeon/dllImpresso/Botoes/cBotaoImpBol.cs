using System;
using System.ComponentModel;
using System.Windows.Forms;

namespace dllImpresso.Botoes
{
    /// <summary>
    /// Bot�o para impress�o
    /// </summary>
    public partial class cBotaoImpBol : DevExpress.XtraEditors.XtraUserControl
    {

        private System.Windows.Forms.SaveFileDialog _saveFileDialog1;

        /// <summary>
        /// Indica qual o bot�o do menu foi clicado
        /// </summary>
        public Botao BotaoClicado;

        /// <summary>
        /// Construtor padr�o
        /// </summary>
        public cBotaoImpBol()
        {
            InitializeComponent();
        }

        /// <summary>
        /// Evento que ocorre quando algum dos bot�es � clicado
        /// </summary>
        [Category("Virtual Software")]
        [Description("Evento que ocorre quando algum dos bot�es � clicado")]
        public event EventHandler clicado;

        private void barButtonItem1_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            BotaoClicado = Botao.imprimir;
            OncargaInicial(new EventArgs());
        }

        private void barButtonItem10_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            BotaoClicado = Botao.email_comprovante;
            OncargaInicial(new EventArgs());
        }

        private void barButtonItem2_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            BotaoClicado = Botao.tela;
            OncargaInicial(new EventArgs());
        }

        private void barButtonItem3_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            BotaoClicado = Botao.pdf;
            OncargaInicial(new EventArgs());
        }

        private void barButtonItem4_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            BotaoClicado = Botao.email;
            OncargaInicial(new EventArgs());
        }

        private void barButtonItem6_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            BotaoClicado = Botao.imprimir_frente;
            OncargaInicial(new EventArgs());
        }

        private void barButtonItem7_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            BotaoClicado = Botao.PDF_frente;
            OncargaInicial(new EventArgs());
        }

        private void barButtonItem8_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            BotaoClicado = Botao.imprimir_comprovante;
            OncargaInicial(new EventArgs());
        }

        private void barButtonItem9_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            BotaoClicado = Botao.pdf_comprovante;
            OncargaInicial(new EventArgs());
        }

        private void dropDownButton1_Click(object sender, EventArgs e)
        {
            BotaoClicado = Botao.botao;
            OncargaInicial(new EventArgs());
        }

        /// <summary>
        /// Evento que ocorre quando algum dos bot�es � clicado
        /// </summary>        
        private void OncargaInicial(EventArgs e)
        {
            if (clicado != null)
                clicado(this, e);
        }        

        /// <summary>
        /// 
        /// </summary>
        /// <param name="Imp"></param>
        public void GerarPDF(DevExpress.XtraReports.UI.XtraReport Imp)
        {
            if (saveFileDialog1.ShowDialog() == DialogResult.OK)
            {
                Imp.ExportOptions.Pdf.Compressed = true;
                Imp.ExportOptions.Pdf.ImageQuality = DevExpress.XtraPrinting.PdfJpegImageQuality.Lowest;
                Imp.ExportToPdf(saveFileDialog1.FileName);
            }
        }

        /// <summary>
        /// Mostrar ou nao o sub-menu comprovante de endere�o
        /// </summary>
        [Category("Virtual Software")]
        [Description("Comprovante")]
        public bool ItemComprovante
        {
            get
            {
                return barSubItem2.Visibility == DevExpress.XtraBars.BarItemVisibility.Always;
            }
            set
            {
                barSubItem2.Visibility = value ? DevExpress.XtraBars.BarItemVisibility.Always : DevExpress.XtraBars.BarItemVisibility.OnlyInCustomizing;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        public System.Windows.Forms.SaveFileDialog saveFileDialog1
        {
            get
            {
                if (_saveFileDialog1 == null)
                {
                    _saveFileDialog1 = new SaveFileDialog();
                    _saveFileDialog1.DefaultExt = "pdf";
                };
                return _saveFileDialog1;
            }
        }

        /// <summary>
        /// T�tulo do bot�o
        /// </summary>
        [Category("Virtual Software")]
        [Description("Titulo")]
        public string Titulo
        {
            get
            {
                return dropDownButton1.Text;
            }
            set
            {
                dropDownButton1.Text = value;
            }
        }


    }

    /// <summary>
    /// Tipos de chamado do botao
    /// </summary>
    public enum Botao
    {
        /// <summary>
        /// Bot�o
        /// </summary>
        botao,
        /// <summary>
        /// Imprimir
        /// </summary>
        imprimir,
        /// <summary>
        /// Mostrar na tela
        /// </summary>
        tela,
        /// <summary>
        /// Enviar por e.mail
        /// </summary>
        email,
        /// <summary>
        /// Gerar pdf
        /// </summary>
        pdf,
        /// <summary>
        /// Imprimir somente a parte da frente
        /// </summary>
        imprimir_frente,
        /// <summary>
        /// pdf da parte da frente
        /// </summary>
        PDF_frente,
        /// <summary>
        /// n�o faz nada, S� � chamado por processo
        /// </summary>
        nulo,
        /// <summary>
        /// 
        /// </summary>
        imprimir_comprovante,
        /// <summary>
        /// 
        /// </summary>
        pdf_comprovante,
        /// <summary>
        /// 
        /// </summary>
        email_comprovante
    }
}
