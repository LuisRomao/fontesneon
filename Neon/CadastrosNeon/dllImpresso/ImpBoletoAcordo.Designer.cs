namespace dllImpresso
{
    partial class ImpBoletoAcordo
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.QuadroBoletosOriginais = new DevExpress.XtraReports.UI.XRPanel();
            this.xrPanel37 = new DevExpress.XtraReports.UI.XRPanel();
            this.xrLabel40 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel39 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel38 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel37 = new DevExpress.XtraReports.UI.XRLabel();
            this.labOrigValor = new DevExpress.XtraReports.UI.XRLabel();
            this.labOrigTipo = new DevExpress.XtraReports.UI.XRLabel();
            this.labOrigComp = new DevExpress.XtraReports.UI.XRLabel();
            this.labOrigVencimento = new DevExpress.XtraReports.UI.XRLabel();
            this.labOrigNumero = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel76 = new DevExpress.XtraReports.UI.XRLabel();
            this.QuadroNovosBoletos = new DevExpress.XtraReports.UI.XRPanel();
            this.xrPanel4 = new DevExpress.XtraReports.UI.XRPanel();
            this.xrLabel12 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabelAcordoVenc = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel13 = new DevExpress.XtraReports.UI.XRLabel();
            this.labNovValor = new DevExpress.XtraReports.UI.XRLabel();
            this.labNovParcela = new DevExpress.XtraReports.UI.XRLabel();
            this.labNovVencimento = new DevExpress.XtraReports.UI.XRLabel();
            this.labNovNumero = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel19 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel2Duvida = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTitulo = new DevExpress.XtraReports.UI.XRLabel();
            ((System.ComponentModel.ISupportInitialize)(this.dImpBoletoBase1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dImpDestRem1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.bindingSourcePrincipal)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            // 
            // xrLabelUsoBanco
            // 
            this.xrLabelUsoBanco.StylePriority.UseBorders = false;
            // 
            // xrLabelCarteira
            // 
            this.xrLabelCarteira.StylePriority.UseBorders = false;
            // 
            // xrLabelEspecie
            // 
            this.xrLabelEspecie.StylePriority.UseBorders = false;
            // 
            // BANCOLocal
            // 
            this.BANCOLocal.StylePriority.UseBorders = false;
            // 
            // BANCONumero
            // 
            this.BANCONumero.StylePriority.UseFont = false;
            // 
            // QuadroUtil
            // 
            this.QuadroUtil.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrTitulo,
            this.QuadroNovosBoletos,
            this.QuadroBoletosOriginais});
            // 
            // xrLabelcompet1
            // 
            this.xrLabelcompet1.Text = "PARCELA";
            // 
            // rLabelcompet
            // 
            this.rLabelcompet.StylePriority.UseBorders = false;
            this.rLabelcompet.Text = "Parcela";
            // 
            // xrLabel3
            // 
            this.xrLabel3.StylePriority.UseBorders = false;            
            // 
            // QuadroSegundaPagina
            // 
            this.QuadroSegundaPagina.StylePriority.UseBackColor = false;
            // 
            // Detail
            // 
            this.Detail.StylePriority.UseTextAlignment = false;
            // 
            // xrSigla1
            // 
            this.xrSigla1.StylePriority.UseFont = false;
            this.xrSigla1.StylePriority.UseTextAlignment = false;
            // 
            // QuadroBoletosOriginais
            // 
            this.QuadroBoletosOriginais.AnchorVertical = DevExpress.XtraReports.UI.VerticalAnchorStyles.Top;
            this.QuadroBoletosOriginais.BorderColor = System.Drawing.Color.Silver;
            this.QuadroBoletosOriginais.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.QuadroBoletosOriginais.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrPanel37,
            this.labOrigValor,
            this.labOrigTipo,
            this.labOrigComp,
            this.labOrigVencimento,
            this.labOrigNumero,
            this.xrLabel76});
            this.QuadroBoletosOriginais.Dpi = 254F;
            this.QuadroBoletosOriginais.LocationFloat = new DevExpress.Utils.PointFloat(169F, 360F);
            this.QuadroBoletosOriginais.Name = "QuadroBoletosOriginais";
            this.QuadroBoletosOriginais.SizeF = new System.Drawing.SizeF(620F, 112F);
            // 
            // xrPanel37
            // 
            this.xrPanel37.BackColor = System.Drawing.Color.DarkGray;
            this.xrPanel37.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrPanel37.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrLabel40,
            this.xrLabel39,
            this.xrLabel38,
            this.xrLabel37});
            this.xrPanel37.Dpi = 254F;
            this.xrPanel37.LocationFloat = new DevExpress.Utils.PointFloat(0F, 47F);
            this.xrPanel37.Name = "xrPanel37";
            this.xrPanel37.SizeF = new System.Drawing.SizeF(614F, 36F);
            this.xrPanel37.StylePriority.UseBorders = false;
            // 
            // xrLabel40
            // 
            this.xrLabel40.Dpi = 254F;
            this.xrLabel40.Font = new System.Drawing.Font("Times New Roman", 6.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel40.ForeColor = System.Drawing.Color.Black;
            this.xrLabel40.LocationFloat = new DevExpress.Utils.PointFloat(506F, 0F);
            this.xrLabel40.Name = "xrLabel40";
            this.xrLabel40.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrLabel40.SizeF = new System.Drawing.SizeF(77F, 26F);
            this.xrLabel40.Text = "Valor";
            // 
            // xrLabel39
            // 
            this.xrLabel39.Dpi = 254F;
            this.xrLabel39.Font = new System.Drawing.Font("Times New Roman", 6F, System.Drawing.FontStyle.Bold);
            this.xrLabel39.ForeColor = System.Drawing.Color.Black;
            this.xrLabel39.LocationFloat = new DevExpress.Utils.PointFloat(339F, 0F);
            this.xrLabel39.Name = "xrLabel39";
            this.xrLabel39.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrLabel39.SizeF = new System.Drawing.SizeF(123F, 25F);
            this.xrLabel39.Text = "Vencimento";
            // 
            // xrLabel38
            // 
            this.xrLabel38.Dpi = 254F;
            this.xrLabel38.Font = new System.Drawing.Font("Times New Roman", 6F, System.Drawing.FontStyle.Bold);
            this.xrLabel38.ForeColor = System.Drawing.Color.Black;
            this.xrLabel38.LocationFloat = new DevExpress.Utils.PointFloat(189F, 0F);
            this.xrLabel38.Name = "xrLabel38";
            this.xrLabel38.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrLabel38.SizeF = new System.Drawing.SizeF(135F, 25F);
            this.xrLabel38.Text = "Compet�ncia";
            // 
            // xrLabel37
            // 
            this.xrLabel37.Dpi = 254F;
            this.xrLabel37.Font = new System.Drawing.Font("Times New Roman", 6F, System.Drawing.FontStyle.Bold);
            this.xrLabel37.ForeColor = System.Drawing.Color.Black;
            this.xrLabel37.LocationFloat = new DevExpress.Utils.PointFloat(21F, 0F);
            this.xrLabel37.Name = "xrLabel37";
            this.xrLabel37.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrLabel37.SizeF = new System.Drawing.SizeF(95F, 25F);
            this.xrLabel37.Text = "N�mero";
            // 
            // labOrigValor
            // 
            this.labOrigValor.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.labOrigValor.CanShrink = true;
            this.labOrigValor.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "A5")});
            this.labOrigValor.Dpi = 254F;
            this.labOrigValor.Font = new System.Drawing.Font("Tahoma", 7F);
            this.labOrigValor.LocationFloat = new DevExpress.Utils.PointFloat(466F, 83F);
            this.labOrigValor.Multiline = true;
            this.labOrigValor.Name = "labOrigValor";
            this.labOrigValor.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.labOrigValor.SizeF = new System.Drawing.SizeF(148F, 26F);
            this.labOrigValor.StylePriority.UseBorders = false;
            this.labOrigValor.Text = "R$ 999.999.99";
            this.labOrigValor.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.labOrigValor.Draw += new DevExpress.XtraReports.UI.DrawEventHandler(this.labOrigValor_Draw);
            // 
            // labOrigTipo
            // 
            this.labOrigTipo.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.labOrigTipo.CanShrink = true;
            this.labOrigTipo.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "A2")});
            this.labOrigTipo.Dpi = 254F;
            this.labOrigTipo.Font = new System.Drawing.Font("Tahoma", 7F);
            this.labOrigTipo.LocationFloat = new DevExpress.Utils.PointFloat(148F, 83F);
            this.labOrigTipo.Multiline = true;
            this.labOrigTipo.Name = "labOrigTipo";
            this.labOrigTipo.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.labOrigTipo.SizeF = new System.Drawing.SizeF(34F, 26F);
            this.labOrigTipo.StylePriority.UseBorders = false;
            this.labOrigTipo.Text = "CONDOM�NIO";
            // 
            // labOrigComp
            // 
            this.labOrigComp.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.labOrigComp.CanShrink = true;
            this.labOrigComp.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "A3")});
            this.labOrigComp.Dpi = 254F;
            this.labOrigComp.Font = new System.Drawing.Font("Tahoma", 7F);
            this.labOrigComp.LocationFloat = new DevExpress.Utils.PointFloat(189F, 83F);
            this.labOrigComp.Multiline = true;
            this.labOrigComp.Name = "labOrigComp";
            this.labOrigComp.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.labOrigComp.SizeF = new System.Drawing.SizeF(120F, 26F);
            this.labOrigComp.StylePriority.UseBorders = false;
            this.labOrigComp.Text = "10/10/2000";
            this.labOrigComp.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter;
            // 
            // labOrigVencimento
            // 
            this.labOrigVencimento.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.labOrigVencimento.CanShrink = true;
            this.labOrigVencimento.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "A4")});
            this.labOrigVencimento.Dpi = 254F;
            this.labOrigVencimento.Font = new System.Drawing.Font("Tahoma", 7F);
            this.labOrigVencimento.LocationFloat = new DevExpress.Utils.PointFloat(318F, 83F);
            this.labOrigVencimento.Multiline = true;
            this.labOrigVencimento.Name = "labOrigVencimento";
            this.labOrigVencimento.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.labOrigVencimento.SizeF = new System.Drawing.SizeF(138F, 26F);
            this.labOrigVencimento.StylePriority.UseBorders = false;
            this.labOrigVencimento.Text = "R$ 999.999.99";
            this.labOrigVencimento.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            // 
            // labOrigNumero
            // 
            this.labOrigNumero.Borders = DevExpress.XtraPrinting.BorderSide.Left;
            this.labOrigNumero.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "A1")});
            this.labOrigNumero.Dpi = 254F;
            this.labOrigNumero.Font = new System.Drawing.Font("Tahoma", 7F);
            this.labOrigNumero.LocationFloat = new DevExpress.Utils.PointFloat(0F, 83F);
            this.labOrigNumero.Multiline = true;
            this.labOrigNumero.Name = "labOrigNumero";
            this.labOrigNumero.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.labOrigNumero.SizeF = new System.Drawing.SizeF(139F, 26F);
            this.labOrigNumero.Text = "labOrigNumero";
            // 
            // xrLabel76
            // 
            this.xrLabel76.BackColor = System.Drawing.Color.DarkGray;
            this.xrLabel76.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel76.Dpi = 254F;
            this.xrLabel76.Font = new System.Drawing.Font("Verdana", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel76.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.xrLabel76.LocationFloat = new DevExpress.Utils.PointFloat(0F, 0F);
            this.xrLabel76.Name = "xrLabel76";
            this.xrLabel76.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrLabel76.SizeF = new System.Drawing.SizeF(614F, 47F);
            this.xrLabel76.StylePriority.UseBorders = false;
            this.xrLabel76.Text = "Boletos Originais";
            this.xrLabel76.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter;
            this.xrLabel76.Draw += new DevExpress.XtraReports.UI.DrawEventHandler(this.xrLabel76_Draw);
            // 
            // QuadroNovosBoletos
            // 
            this.QuadroNovosBoletos.BorderColor = System.Drawing.Color.Silver;
            this.QuadroNovosBoletos.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.QuadroNovosBoletos.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrPanel4,
            this.labNovValor,
            this.labNovParcela,
            this.labNovVencimento,
            this.labNovNumero,
            this.xrLabel19});
            this.QuadroNovosBoletos.Dpi = 254F;
            this.QuadroNovosBoletos.LocationFloat = new DevExpress.Utils.PointFloat(910F, 360F);
            this.QuadroNovosBoletos.Name = "QuadroNovosBoletos";
            this.QuadroNovosBoletos.SizeF = new System.Drawing.SizeF(620F, 112F);
            // 
            // xrPanel4
            // 
            this.xrPanel4.BackColor = System.Drawing.Color.DarkGray;
            this.xrPanel4.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrPanel4.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrLabel12,
            this.xrLabelAcordoVenc,
            this.xrLabel2Duvida,
            this.xrLabel13});
            this.xrPanel4.Dpi = 254F;
            this.xrPanel4.LocationFloat = new DevExpress.Utils.PointFloat(0F, 47F);
            this.xrPanel4.Name = "xrPanel4";
            this.xrPanel4.SizeF = new System.Drawing.SizeF(614F, 36F);
            this.xrPanel4.StylePriority.UseBorders = false;
            this.xrPanel4.Controls.SetChildIndex(this.xrLabel13, 0);
            this.xrPanel4.Controls.SetChildIndex(this.xrLabelAcordoVenc, 0);
            this.xrPanel4.Controls.SetChildIndex(this.xrLabel12, 0);
            // 
            // xrLabel12
            // 
            this.xrLabel12.Dpi = 254F;
            this.xrLabel12.Font = new System.Drawing.Font("Times New Roman", 6F, System.Drawing.FontStyle.Bold);
            this.xrLabel12.ForeColor = System.Drawing.Color.Black;
            this.xrLabel12.LocationFloat = new DevExpress.Utils.PointFloat(21F, 0F);
            this.xrLabel12.Name = "xrLabel12";
            this.xrLabel12.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrLabel12.SizeF = new System.Drawing.SizeF(95F, 25F);
            this.xrLabel12.Text = "Parcela";
            // 
            // xrLabelAcordoVenc
            // 
            this.xrLabelAcordoVenc.Dpi = 254F;
            this.xrLabelAcordoVenc.Font = new System.Drawing.Font("Times New Roman", 6F, System.Drawing.FontStyle.Bold);
            this.xrLabelAcordoVenc.ForeColor = System.Drawing.Color.Black;
            this.xrLabelAcordoVenc.LocationFloat = new DevExpress.Utils.PointFloat(339F, 0F);
            this.xrLabelAcordoVenc.Name = "xrLabelAcordoVenc";
            this.xrLabelAcordoVenc.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrLabelAcordoVenc.SizeF = new System.Drawing.SizeF(127F, 25F);
            this.xrLabelAcordoVenc.Text = "Vencimento";
            // 
            // xrLabel13
            // 
            this.xrLabel13.Dpi = 254F;
            this.xrLabel13.Font = new System.Drawing.Font("Times New Roman", 6F, System.Drawing.FontStyle.Bold);
            this.xrLabel13.ForeColor = System.Drawing.Color.Black;
            this.xrLabel13.LocationFloat = new DevExpress.Utils.PointFloat(148F, 0F);
            this.xrLabel13.Name = "xrLabel13";
            this.xrLabel13.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrLabel13.SizeF = new System.Drawing.SizeF(95F, 25F);
            this.xrLabel13.Text = "N�mero";
            // 
            // labNovValor
            // 
            this.labNovValor.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.labNovValor.CanShrink = true;
            this.labNovValor.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "A9")});
            this.labNovValor.Dpi = 254F;
            this.labNovValor.Font = new System.Drawing.Font("Tahoma", 7F);
            this.labNovValor.LocationFloat = new DevExpress.Utils.PointFloat(466F, 83F);
            this.labNovValor.Multiline = true;
            this.labNovValor.Name = "labNovValor";
            this.labNovValor.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.labNovValor.SizeF = new System.Drawing.SizeF(148F, 26F);
            this.labNovValor.StylePriority.UseBorders = false;
            this.labNovValor.Text = "R$ 999.999.99";
            this.labNovValor.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.labNovValor.Draw += new DevExpress.XtraReports.UI.DrawEventHandler(this.labNovValor_Draw);
            // 
            // labNovParcela
            // 
            this.labNovParcela.Borders = DevExpress.XtraPrinting.BorderSide.Left;
            this.labNovParcela.CanShrink = true;
            this.labNovParcela.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "A6")});
            this.labNovParcela.Dpi = 254F;
            this.labNovParcela.Font = new System.Drawing.Font("Tahoma", 7F);
            this.labNovParcela.LocationFloat = new DevExpress.Utils.PointFloat(0F, 83F);
            this.labNovParcela.Multiline = true;
            this.labNovParcela.Name = "labNovParcela";
            this.labNovParcela.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.labNovParcela.SizeF = new System.Drawing.SizeF(94F, 26F);
            this.labNovParcela.Text = "CONDOM�NIO";
            this.labNovParcela.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight;
            // 
            // labNovVencimento
            // 
            this.labNovVencimento.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.labNovVencimento.CanShrink = true;
            this.labNovVencimento.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "A8")});
            this.labNovVencimento.Dpi = 254F;
            this.labNovVencimento.Font = new System.Drawing.Font("Tahoma", 7F);
            this.labNovVencimento.LocationFloat = new DevExpress.Utils.PointFloat(318F, 83F);
            this.labNovVencimento.Multiline = true;
            this.labNovVencimento.Name = "labNovVencimento";
            this.labNovVencimento.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.labNovVencimento.SizeF = new System.Drawing.SizeF(138F, 26F);
            this.labNovVencimento.StylePriority.UseBorders = false;
            this.labNovVencimento.Text = "R$ 999.999.99";
            this.labNovVencimento.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            // 
            // labNovNumero
            // 
            this.labNovNumero.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.labNovNumero.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "A7")});
            this.labNovNumero.Dpi = 254F;
            this.labNovNumero.Font = new System.Drawing.Font("Tahoma", 7F);
            this.labNovNumero.LocationFloat = new DevExpress.Utils.PointFloat(127F, 83F);
            this.labNovNumero.Multiline = true;
            this.labNovNumero.Name = "labNovNumero";
            this.labNovNumero.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.labNovNumero.SizeF = new System.Drawing.SizeF(139F, 26F);
            this.labNovNumero.StylePriority.UseBorders = false;
            this.labNovNumero.Text = "PROPRIET�RIO";
            // 
            // xrLabel19
            // 
            this.xrLabel19.BackColor = System.Drawing.Color.DarkGray;
            this.xrLabel19.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel19.Dpi = 254F;
            this.xrLabel19.Font = new System.Drawing.Font("Verdana", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel19.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.xrLabel19.LocationFloat = new DevExpress.Utils.PointFloat(0F, 0F);
            this.xrLabel19.Name = "xrLabel19";
            this.xrLabel19.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrLabel19.SizeF = new System.Drawing.SizeF(614F, 47F);
            this.xrLabel19.StylePriority.UseBorders = false;
            this.xrLabel19.Text = "Novos Boletos";
            this.xrLabel19.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter;
            this.xrLabel19.Draw += new DevExpress.XtraReports.UI.DrawEventHandler(this.xrLabel19_Draw);
            // 
            // xrLabel2Duvida
            // 
            this.xrLabel2Duvida.Dpi = 254F;
            this.xrLabel2Duvida.Font = new System.Drawing.Font("Times New Roman", 6.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel2Duvida.ForeColor = System.Drawing.Color.Black;
            this.xrLabel2Duvida.LocationFloat = new DevExpress.Utils.PointFloat(506F, 0F);
            this.xrLabel2Duvida.Name = "xrLabel2";
            this.xrLabel2Duvida.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrLabel2Duvida.SizeF = new System.Drawing.SizeF(77F, 26F);
            this.xrLabel2Duvida.Text = "Valor";
            // 
            // xrTitulo
            // 
            this.xrTitulo.Dpi = 254F;
            this.xrTitulo.Font = new System.Drawing.Font("Times New Roman", 20.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrTitulo.ForeColor = System.Drawing.Color.DarkGray;
            this.xrTitulo.LocationFloat = new DevExpress.Utils.PointFloat(21F, 85F);
            this.xrTitulo.Name = "xrTitulo";
            this.xrTitulo.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTitulo.SizeF = new System.Drawing.SizeF(1762F, 103F);
            this.xrTitulo.Text = "ACORDO";
            this.xrTitulo.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter;
            // 
            // ImpBoletoAcordo
            // 
            this.Bands.AddRange(new DevExpress.XtraReports.UI.Band[] {
            this.Detail,
            this.bottomMarginBand1});
            this.Version = "12.2";
            ((System.ComponentModel.ISupportInitialize)(this.dImpBoletoBase1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dImpDestRem1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.bindingSourcePrincipal)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();

        }

        #endregion

        private DevExpress.XtraReports.UI.XRPanel QuadroBoletosOriginais;
        private DevExpress.XtraReports.UI.XRLabel xrLabel76;
        private DevExpress.XtraReports.UI.XRPanel xrPanel37;
        private DevExpress.XtraReports.UI.XRLabel xrLabel40;
        private DevExpress.XtraReports.UI.XRLabel xrLabel39;
        private DevExpress.XtraReports.UI.XRLabel xrLabel38;
        private DevExpress.XtraReports.UI.XRLabel xrLabel37;
        /// <summary>
        /// 
        /// </summary>
        public DevExpress.XtraReports.UI.XRLabel labOrigTipo;
        private DevExpress.XtraReports.UI.XRPanel QuadroNovosBoletos;
        private DevExpress.XtraReports.UI.XRPanel xrPanel4;
        private DevExpress.XtraReports.UI.XRLabel xrLabel2Duvida;
        private DevExpress.XtraReports.UI.XRLabel xrLabelAcordoVenc;
        private DevExpress.XtraReports.UI.XRLabel xrLabel13;
        private DevExpress.XtraReports.UI.XRLabel xrLabel19;
        /// <summary>
        /// 
        /// </summary>
        public DevExpress.XtraReports.UI.XRLabel xrTitulo;
        private DevExpress.XtraReports.UI.XRLabel labOrigValor;
        private DevExpress.XtraReports.UI.XRLabel labOrigNumero;
        private DevExpress.XtraReports.UI.XRLabel labOrigVencimento;
        private DevExpress.XtraReports.UI.XRLabel labOrigComp;
        private DevExpress.XtraReports.UI.XRLabel labNovValor;
        private DevExpress.XtraReports.UI.XRLabel labNovParcela;
        private DevExpress.XtraReports.UI.XRLabel labNovVencimento;
        private DevExpress.XtraReports.UI.XRLabel labNovNumero;
        private DevExpress.XtraReports.UI.XRLabel xrLabel12;
    }
}