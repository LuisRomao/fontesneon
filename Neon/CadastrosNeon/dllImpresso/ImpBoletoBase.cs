/*
MR - 25/02/2016 11:00 -           - Ajustes de nomenclaturas e textos padr�es para boleto Ita� (Altera��es indicadas por *** MRC - INICIO - REMESSA (25/02/2016 11:00) ***)
*/

using DevExpress.XtraPrinting;
using DevExpress.XtraReports.UI;
using System;
using System.Drawing;
using System.Text;
using CompontesBasicosProc;
using VirEnumeracoes;

namespace dllImpresso
{
    /// <summary>
    /// 
    /// </summary>
    public partial class ImpBoletoBase : dllImpresso.ImpDestRem
    {

        private int _Banco = 0;

        /// <summary>
        /// Construtor
        /// </summary>
        public ImpBoletoBase()
        {
            InitializeComponent();

            Label_SEMCPF.Text = string.Format(
"ATEN��O\r\n" +
"\r\n" +
"Conforme informado nos meses anteriores, a partir de janeiro de 2017 a nova regra da Febraban determina que os boletos s� poder�o ser emitidos com o CPF cadastrado.\r\n" +
"Portanto, para emiss�o do seu boleto, entre em contato pelo n�mero {0} ou informe o CPF pelo site www.neonimoveis.com.br",
Framework.DSCentral.EMP == 1 ? "3376.7900" : "4990-1394");
        }

        private void CantosRedondos_Draw(object sender, DrawEventArgs e)
        {
            CantosRedondos(sender, e, 30);
        }

        private void CodigoDeBarras_Draw(object sender, DrawEventArgs e)
        {
            DrawBarras(e);
        }
        private void DrawBarras(DrawEventArgs e)
        {
            int largura = 3;
            String Manobra;
            String NUM1, NUM2;
            String Saida;
            Rectangle rect = Rectangle.Truncate(e.Bounds);
            //String numero = e.Data.Text;
            String numero = e.Brick.Text;
            foreach (char c in numero)
                if (!char.IsDigit(c))
                    return;
            StringBuilder Retorno = new StringBuilder();
            string[] a = new string[10] { "00110", "10001", "01001", "11000", "00101", "10100", "01100", "00011", "10010", "01010" };


            IGraphics graph = e.UniGraphics;
            graph.FillRectangle(Brushes.White, rect);





            Manobra = numero.Trim();
            if ((Manobra.Length % 2) != 0)
                Manobra = "0" + Manobra;

            Saida = "0000";

            Int32 indice1;
            Int32 indice2;
            char base0 = '0';// Convert.ToChar("0");
            for (int c = 1; c <= (Manobra.Length / 2); c++)
            {
                indice1 = Manobra[2 * (c - 1) + 1 - 1] - base0;
                indice2 = Manobra[2 * (c - 1) + 2 - 1] - base0;
                NUM1 = a[indice1];
                NUM2 = a[indice2];
                for (int b = 0; b < 5; b++)
                    Saida += (NUM1.Substring(b, 1) + NUM2.Substring(b, 1));
            };
            Saida += "100";

            for (int m = 1; m <= Saida.Length; m++)
                if ((m % 2) != 0)
                { //impar
                    if (Saida[m - 1] == Convert.ToChar("1"))
                        rect.Width = 3 * largura;
                    else
                        rect.Width = 1 * largura;
                    graph.FillRectangle(Brushes.Black, rect);
                    rect.X += rect.Width;
                }
                //par
                else
                    if (Saida[m - 1] == Convert.ToChar("1"))
                    rect.X += 3 * largura;
                else
                    rect.X += 1 * largura;
            ;


        }

        private void MostraCodBar(string TextoSub)
        {
            if (TextoSub == "")
            {
                NamePago.Visible = false;
                CodigoDeBarras.Visible = true;
                LinhaDigitavel.Visible = true;
            }
            else
            {
                NamePago.Top = CodigoDeBarras.Top;
                NamePago.Left = CodigoDeBarras.Left;
                NamePago.Width = CodigoDeBarras.Width;
                NamePago.Height = CodigoDeBarras.Height;
                NamePago.Visible = true;
                NamePago.Text = TextoSub;
                CodigoDeBarras.Visible = false;
                LinhaDigitavel.Visible = false;                
            }
        }

        private void ImpBoletoBase_DataSourceRowChanged(object sender, DataSourceRowEventArgs e)
        {
            System.Data.DataRowView DRV = (System.Data.DataRowView)bindingSourcePrincipal[e.CurrentRow];
            dImpBoletoBase.BOLETOSIMPRow row = (dImpBoletoBase.BOLETOSIMPRow)DRV.Row;            
            if (!row.IsBOLPagamentoNull())            
                MostraCodBar("PAGO");                            
            else
            {
                if (row.BOLStatusRegistro.EstaNoGrupo((int)StatusRegistroBoleto.BoletoAguardandoRetornoDA,
                                                      (int)StatusRegistroBoleto.BoletoRegistradoDA,
                                                      (int)StatusRegistroBoleto.BoletoRegistrandoDA))                
                    MostraCodBar("D�bito Autom�tico");                
                else                
                    MostraCodBar("");                                    
            }

            if (row.IsCPFNull())
            
            {
                xrPanel5.Visible = false;
                Panel_SEMCPF.Visible = true;
            }
            else
            {
                xrPanel5.Visible = true;
                Panel_SEMCPF.Visible = false;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="Banco"></param>
        /// <param name="Carteira"></param>
        public void DadosBanco(int Banco, int Carteira)
        {
            if (Banco != _Banco)
            {
                _Banco = Banco;
                switch (Banco)
                {
                    case 104:
                        xrLabel6.DataBindings[0].FormatString = "{0:dd/MM/yyyy}";
                        //Font FontRed = new Font(xrLabel6.Font.FontFamily, xrLabel6.Font.Size - 1, xrLabel6.Font.Style);
                        //xrLabel6.Font = FontRed;
                        //xrLabel6.HeightF = 103;
                        xrLabelEspecie.Text = "RC";
                        xrLabelCarteira.Text = Carteira == 9 ? "RG" : "SR";
                        xrLabelUsoBanco.Text = string.Empty;
                        xrLogoBanco.WidthF = 382;
                        xrLogoBanco.HeightF = 80;
                        xrLogoBanco.Visible = true;
                        //xrLabelrecsacado.Parent = xrLabel7.Parent;
                        //xrLabelrecsacado.TopF = xrLabel7.TopF;
                        //xrLabelrecsacado.LeftF = xrLabel7.LeftF;
                        //xrLabelrecsacado.WidthF = xrLabel7.WidthF;
                        //xrLabelrecsacado.Visible = true;
                        BANCONome.Visible = false;
                        BANCONumero.Text = "104-0";
                        BANCOLocal.Text = "PREFERENCIALMENTE NAS CASAS LOT�RICAS AT� O VALOR LIMITE";
                        //xrCompetencia.HeightF -= 27;
                        //xrLabel9.HeightF -= 27;
                        //xrLabel6.HeightF -= 27;
                        //xrCompetencia.TopF += 27;
                        //xrLabel9.TopF += 27;
                        //xrLabel6.TopF += 27;
                        //xrLabelcompet1.TopF += 27;
                        //xrLabel8.TopF += 27;
                        //xrLabel7.TopF += 27;
                        xrLabel3.Visible = true;
                        break;
                    case 237:
                        xrLabelEspecie.Text = "REC";
                        xrLabelCarteira.Text = Carteira.ToString();
                        xrLabelUsoBanco.Text = string.Empty;
                        BANCONome.Text = "BRADESCO S/A";
                        BANCONumero.Text = "237-2";
                        BANCOLocal.Text = "At� o vencimento, preferencialmente na Rede Bradesco ou Bradesco Expresso, ap�s o vencimento, somente no BRADESCO";
                        break;
                    case 33:
                        xrLogoBanco33.WidthF = 382;
                        xrLogoBanco33.HeightF = 80;
                        xrLogoBanco33.LeftF = xrLogoBanco.LeftF;
                        xrLogoBanco33.Visible = true;
                        BANCONome.Visible = false;
                        xrLabelEspecie.Text = "DM";
                        xrLabelCarteira.Text = "RCR";
                        xrLabelUsoBanco.Text = string.Empty;
                        BANCONumero.Text = "033-7";
                        BANCOLocal.Text = "At� o vencimento,preferencialmente no Santander. Ap�s o vencimento, somente no Santander";
                        break;
                    case 341:
                        xrLabelEspecie.Text = string.Empty;
                        xrLabelCarteira.Text = Carteira.ToString();
                        xrLabelUsoBanco.Text = string.Empty;
                        BANCONome.Text = "Banco Ita� S.A.";
                        BANCONumero.Text = "341-7";
                        //*** MRC - INICIO - REMESSA (25/02/2016 11:00) ***
                        BANCOLocal.Text = "At� o vencimento pague preferencialmente no Ita�, ap�s o vencimento pague somente no Ita�";
                        xrLabel78.Text = "Benefici�rio";
                        //xrLabel109.Text = "Pagador";
                        //xrLabel59.Text = "Ag�ncia/C�digo do Benefici�rio";
                        xrLabel84.Text = "Instru��es (Instru��es de responsabilidade do Benefici�rio. Qualquer d�vida sobre este boleto, contate o Benefici�rio)";
                        //*** MRC - TERMINO - REMESSA (25/02/2016 11:00) ***
                        break;
                    case 409:
                        xrLabelEspecie.Text = "REC";
                        xrLabelCarteira.Text = "ESPECIAL";
                        xrLabelUsoBanco.Text = "CVT 7744-5";
                        BANCONome.Text = "UNIBANCO";
                        BANCONumero.Text = "409-0";
                        BANCOLocal.Text = "At� o vencimento, pag�vel em qualquer banco. Ap�s o vencimento, em qualquer ag�ncia do Unibanco mediante consulta no sistema VC";
                        break;
                    case 399:
                        xrLabelEspecie.Text = string.Empty;
                        xrEspecie.Text = "09 - Real";
                        xrLabelCarteira.Text = "CNR";// "CNR";
                        xrLabelUsoBanco.Text = string.Empty;
                        BANCONome.Text = "HSBC";
                        BANCONumero.Text = "399-9";
                        BANCOLocal.Text = "At� o vencimento,preferencialmente no HSBC. Ap�s o vencimento, somente no HSBC";
                        break;
                    default:
                        throw new NotImplementedException(string.Format("Impresso n�o configurado para banco = {0}", Banco));
                }
            }
        }

        //private List<int> SemCPF;
        /*
        private void xrLabelCPFPagador_PrintOnPage(object sender, PrintOnPageEventArgs e)
        {
            /*
            XRLabel XRLabelTeste = (XRLabel)sender;
            Console.WriteLine(string.Format("Teste:{0}", XRLabelTeste.Text));
            if (!XRLabelTeste.Text.Contains("CPF:") && !XRLabelTeste.Text.Contains("CNPJ:"))
            {
                SemCPF.Add(e.PageIndex);                
                Console.WriteLine(string.Format("p�gina inclu�da: {0} Total {1}", e.PageIndex, SemCPF.Count));
            }
        }



        private void ImpBoletoBase_AfterPrint(object sender, EventArgs e)
        {
            /*
            //Console.WriteLine(string.Format("After Total {0}", SemCPF.Count));
            XRWatermark w = CriaW();
            for (int i = 0; i < PrintingSystem.PageCount; i++)
            {
                //Console.WriteLine(string.Format("CPF i: {0}", i));
                if (SemCPF.Contains(i))
                {
                    //Console.WriteLine("Presente");
                    PrintingSystem.Pages[i].AssignWatermark(w);
                }
                //else
                //{
                    //Console.WriteLine("ausente Total: {0}",SemCPF.Count);
                //}
            }
            
        }*/

        /*
        private XRWatermark CriaW()
        {
            XRWatermark w = new XRWatermark();
            w.Font = new Font("Verdana", 20F, FontStyle.Bold); //66
            w.ForeColor = Color.Gray;
            w.Text = string.Format(
                     "ATEN��O:\r\n"+
                     "O boleto n�o pode ser emitido por falta do CPF no cadastro.\r\n" +
                     "Favor entrar em contato urgente para regularizar o cadastro\r\n" +
                     "atrav�s do tel {0} ou acessando o site - www.neonimoveis.com.br\r\n" +
                     "N�o � poss�vel o pagamento deste boleto sem esta informa��o\r\n"+                     
                     "passando a unidade a ser considerada inadimplente ap�s o vencimento\r\n" +
                     "NEON IM�VEIS",
                     Framework.DSCentral.EMP == 1 ? "3376.7900" : "4990-1394"
                     );
            w.TextTransparency = 55;
            w.ShowBehind = false;            
            w.TextDirection = DevExpress.XtraPrinting.Drawing.DirectionMode.BackwardDiagonal;
            return w;
        }

        private void ImpBoletoBase_BeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e)
        {
            //SemCPF = new List<int>();
        }

        private void xrLabelNomePagador_PrintOnPage(object sender, PrintOnPageEventArgs e)
        {   
            /*
            XRLabel XRLabelTeste = (XRLabel)sender;
            //Console.WriteLine(string.Format("Teste:{0}", XRLabelTeste.Text));
            if (!XRLabelTeste.Text.Contains("CPF:") && !XRLabelTeste.Text.Contains("CNPJ:"))
            {
                SemCPF.Add(e.PageIndex);
                //Console.WriteLine(string.Format("p�gina inclu�da: {0} Total {1}", e.PageIndex, SemCPF.Count));
            }
        }*/
    }
}

