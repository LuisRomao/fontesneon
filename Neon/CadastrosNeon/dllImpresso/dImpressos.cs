﻿namespace dllImpresso
{


    partial class dImpressos
    {
        private dImpressosTableAdapters.IMpreSsosTableAdapter iMpreSsosTableAdapter;
        /// <summary>
        /// TableAdapter padrão para tabela: IMpreSsos
        /// </summary>
        public dImpressosTableAdapters.IMpreSsosTableAdapter IMpreSsosTableAdapter
        {
            get
            {
                if (iMpreSsosTableAdapter == null)
                {
                    iMpreSsosTableAdapter = new dImpressosTableAdapters.IMpreSsosTableAdapter();
                    iMpreSsosTableAdapter.TrocarStringDeConexao();
                };
                return iMpreSsosTableAdapter;
            }
        }

        private dImpressosTableAdapters.IMpressosCamposTableAdapter iMpressosCamposTableAdapter;
        /// <summary>
        /// TableAdapter padrão para tabela: IMpressosCampos
        /// </summary>
        public dImpressosTableAdapters.IMpressosCamposTableAdapter IMpressosCamposTableAdapter
        {
            get
            {
                if (iMpressosCamposTableAdapter == null)
                {
                    iMpressosCamposTableAdapter = new dImpressosTableAdapters.IMpressosCamposTableAdapter();
                    iMpressosCamposTableAdapter.TrocarStringDeConexao();
                };
                return iMpressosCamposTableAdapter;
            }
        }

    }
}
