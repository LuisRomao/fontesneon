namespace dllImpresso
{
    partial class ImpRTFLogoSimples
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(ImpRTFLogoSimples));
            this.QuadroRTF = new DevExpress.XtraReports.UI.XRRichText();
            ((System.ComponentModel.ISupportInitialize)(this.QuadroRTF)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            // 
            // Detail
            // 
            this.Detail.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.QuadroRTF});
            this.Detail.HeightF = 2600F;
            // 
            // QuadroRTF
            // 
            this.QuadroRTF.Dpi = 254F;
            this.QuadroRTF.LocationFloat = new DevExpress.Utils.PointFloat(0F, 0F);
            this.QuadroRTF.Name = "QuadroRTF";
            this.QuadroRTF.SerializableRtfString = resources.GetString("QuadroRTF.SerializableRtfString");
            this.QuadroRTF.SizeF = new System.Drawing.SizeF(1799F, 2600F);
            // 
            // ImpRTFLogoSimples
            // 
            this.Bands.AddRange(new DevExpress.XtraReports.UI.Band[] {
            this.Detail,
            this.PageHeader});
            this.Version = "10.1";
            ((System.ComponentModel.ISupportInitialize)(this.QuadroRTF)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();

        }

        #endregion

        /// <summary>
        /// 
        /// </summary>
        public DevExpress.XtraReports.UI.XRRichText QuadroRTF;

    }
}
