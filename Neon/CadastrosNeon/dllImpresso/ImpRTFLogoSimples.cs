using System.Collections;
using System.Data;
using System.IO;
using DevExpress.XtraReports.UI;

namespace dllImpresso
{
    /// <summary>
    /// 
    /// </summary>
    public partial class ImpRTFLogoSimples : dllImpresso.ImpLogoBase
    {
        /// <summary>
        /// 
        /// </summary>
        /// <param name="Titulo"></param>
        /// <param name="CON"></param>
        /// <param name="Tipo"></param>
        public ImpRTFLogoSimples(string Titulo,int CON,int Tipo)
            :base()
        {
            InitializeComponent();
            xrTitulo.Text = Titulo;
            Apontamento(CON, Tipo);
            QuadroRTFBase = QuadroRTF;
        }

        
        /*
        private bool CarregarImpresso(int IMS, object Dados)
        {
            using (dImpressos dImp = new dImpressos())
            {
                if (dImp.IMpreSsosTableAdapter.Fill(dImp.IMpreSsos, IMS) == 0)
                    return false;
                dImpressos.IMpreSsosRow LinhaMae = dImp.IMpreSsos[0];
                if (LinhaMae.IsIMSRTFNull())
                    return false;
                using (DevExpress.XtraRichEdit.RichEditControl richEditControl1 = new DevExpress.XtraRichEdit.RichEditControl())
                {
                    richEditControl1.RtfText = LinhaMae.IMSRTF;
                    richEditControl1.Options.MailMerge.DataSource = Dados;
                    richEditControl1.BindingContext = new System.Windows.Forms.BindingContext();                    
                    using (MemoryStream Ms = new MemoryStream())
                    {
                        richEditControl1.Document.MailMerge(Ms, DevExpress.XtraRichEdit.DocumentFormat.Rtf);
                        Ms.Position = 0;
                        QuadroRTF.LoadFile(Ms, XRRichTextStreamType.RtfText);
                    }
                }
            }            
            return true;
        }*/

    }
}
