﻿using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using DevExpress.XtraReports.UI;

namespace dllImpresso
{
    /// <summary>
    /// Componente para impressão de mala direta via e-mail (sem remetente)
    /// </summary>
    public partial class ImpRTFMalaDiretaEmail : DevExpress.XtraReports.UI.XtraReport
    {
        /// <summary>
        /// Construtor
        /// </summary>
        public ImpRTFMalaDiretaEmail()
        {
            InitializeComponent();
        }

    }
}
