namespace dllImpresso
{
    partial class ImpBoletoReciboAc
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            ((System.ComponentModel.ISupportInitialize)(this.dImpBoletoBase1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dImpDestRem1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.bindingSourcePrincipal)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            // 
            // xrTitulo
            // 
            this.xrTitulo.StylePriority.UseFont = false;
            // 
            // xrLabelUsoBanco
            // 
            this.xrLabelUsoBanco.StylePriority.UseBorders = false;
            // 
            // xrLabelCarteira
            // 
            this.xrLabelCarteira.StylePriority.UseBorders = false;
            // 
            // xrLabelEspecie
            // 
            this.xrLabelEspecie.StylePriority.UseBorders = false;
            // 
            // BANCOLocal
            // 
            this.BANCOLocal.StylePriority.UseBorders = false;
            // 
            // xrLabelcompet1
            // 
            this.xrLabelcompet1.Text = "PARCELA";
            // 
            // rLabelcompet
            // 
            this.rLabelcompet.StylePriority.UseBorders = false;
            this.rLabelcompet.Text = "Parcela";
            // 
            // QuadroSegundaPagina
            // 
            this.QuadroSegundaPagina.StylePriority.UseBackColor = false;
            // 
            // Detail
            // 
            this.Detail.StylePriority.UseTextAlignment = false;
            // 
            // ImpBoletoReciboAc
            // 
            this.Version = "8.1";
            ((System.ComponentModel.ISupportInitialize)(this.dImpBoletoBase1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dImpDestRem1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.bindingSourcePrincipal)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();

        }

        #endregion
    }
}