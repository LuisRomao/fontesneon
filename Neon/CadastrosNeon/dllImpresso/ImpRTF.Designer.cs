namespace dllImpresso
{
    partial class ImpRTF
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(ImpRTF));
            this.QuadroRTF = new DevExpress.XtraReports.UI.XRRichText();
            this.QuadroRTFSegundaPagina = new DevExpress.XtraReports.UI.XRRichText();
            ((System.ComponentModel.ISupportInitialize)(this.dImpDestRem1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.QuadroRTF)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.QuadroRTFSegundaPagina)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            // 
            // QuadroPrincipal
            // 
            this.QuadroPrincipal.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.QuadroRTF});
            this.QuadroPrincipal.ParentStyleUsing.UseBackColor = false;
            // 
            // QuadroSegundaPagina
            // 
            this.QuadroSegundaPagina.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.QuadroRTFSegundaPagina});
            // 
            // QuadroRTF
            // 
            this.QuadroRTF.CanGrow = false;
            this.QuadroRTF.CanShrink = true;
            this.QuadroRTF.Dpi = 254F;
            this.QuadroRTF.KeepTogether = true;
            this.QuadroRTF.Location = new System.Drawing.Point(0, 0);
            this.QuadroRTF.Name = "QuadroRTF";
            this.QuadroRTF.SerializableRtfString = resources.GetString("QuadroRTF.SerializableRtfString");
            this.QuadroRTF.Size = new System.Drawing.Size(1799, 2865);
            // 
            // QuadroRTFSegundaPagina
            // 
            this.QuadroRTFSegundaPagina.CanGrow = false;
            this.QuadroRTFSegundaPagina.CanShrink = true;
            this.QuadroRTFSegundaPagina.Dpi = 254F;
            this.QuadroRTFSegundaPagina.KeepTogether = true;
            this.QuadroRTFSegundaPagina.Location = new System.Drawing.Point(0, 0);
            this.QuadroRTFSegundaPagina.Name = "QuadroRTFSegundaPagina";
            this.QuadroRTFSegundaPagina.SerializableRtfString = resources.GetString("QuadroRTFSegundaPagina.SerializableRtfString");
            this.QuadroRTFSegundaPagina.Size = new System.Drawing.Size(1799, 720);
            ((System.ComponentModel.ISupportInitialize)(this.dImpDestRem1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.QuadroRTF)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.QuadroRTFSegundaPagina)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();

        }

        #endregion

        /// <summary>
        /// 
        /// </summary>
        public DevExpress.XtraReports.UI.XRRichText QuadroRTF;

        /// <summary>
        /// 
        /// </summary>
        public DevExpress.XtraReports.UI.XRRichText QuadroRTFSegundaPagina;

    }
}