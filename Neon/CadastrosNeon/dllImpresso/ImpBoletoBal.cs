/*
LH - 13/05/2014       - 13.2.8.48 - Reativa��o do XML
MR - 15/03/2016 10:00 -           - Tratamento do tamanho de fonte para Quadro Inadimplente (Altera��es indicadas por *** MRC - INICIO (15/03/2016 10:00) ***)
MR - 01/04/2016 19:00 -           - Troca do campo TextoUnidadesInadimplentes para o tipo RichText (Altera��es indicadas por *** MRC - INICIO (01/04/2016 19:00) ***)
*/


using System;
using System.Drawing;
using System.Collections.Generic;
using DevExpress.XtraReports.UI;
using DevExpress.XtraPrinting;
using CompontesBasicosProc;
using Framework.objetosNeon;
using AbstratosNeon;
using VirEnumeracoesNeon;

namespace dllImpresso
{
    /// <summary>
    /// Impresso com balancete
    /// </summary>
    public partial class ImpBoletoBal : dllImpresso.ImpBoletoBase
    {
        /// <summary>
        /// Construtor
        /// </summary>
        public ImpBoletoBal()
        {
            InitializeComponent();
            DescDesp = new XRLabel[] { DespesaNome, DespesaNome1, DespesaNome2, DespesaNomeADM, DespesaNomeBANC, DespesaNomeADV };
            ValorDesp = new XRLabel[] { DespesaValor, DespesaValor1, DespesaValor2, DespesaValorADM, DespesaValorBANC, DespesaValorADV };
            PainelDesp = new XRPanel[] { PDespG, PdespMO, PDespADMA, PAdministradora, PDespB, PDespH };
            Receitas  = new XRLabel[] { ReceitaA,ReceitaMes,RecetaP,ReceitaT,ReceitaPrev };
            ReceitaTs = new XRLabel[] { ReceitaAT, ReceitaMesT, RecetaPT, ReceitaTT, ReceitaPrevT };

        }

        //*** MRC - INICIO (01/04/2016 19:00) ***
        string nome_fonte = null;
        float tamanho_fonte = 0;

        private void ImpBoletoBal_BeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e)
        {
            DevExpress.XtraRichEdit.RichEditControl richEditControl = new DevExpress.XtraRichEdit.RichEditControl();
            richEditControl.RtfText = TextoUnidadesInadimplentes.Rtf;
            DevExpress.XtraRichEdit.API.Native.Document doc = richEditControl.Document;
            DevExpress.XtraRichEdit.API.Native.DocumentRange range = richEditControl.Document.Range;
            DevExpress.XtraRichEdit.API.Native.CharacterProperties cp = doc.BeginUpdateCharacters(range);
            cp.FontName = nome_fonte;
            cp.FontSize = tamanho_fonte;
            doc.EndUpdateCharacters(cp);
            TextoUnidadesInadimplentes.Rtf = richEditControl.RtfText;
            richEditControl.Dispose();
        }        
        //*** MRC - TERMINO (01/04/2016 19:00) ***

        private XRLabel[] DescDesp;
        private XRLabel[] ValorDesp;
        private XRPanel[] PainelDesp;
        private XRLabel[] Receitas;
        private XRLabel[] ReceitaTs;        

        private void CantosRedondosL(object sender, DrawEventArgs e)
        {
            CantosRedondos(sender, e, 30);
        }

        private void DegradeL(object sender, DrawEventArgs e)
        {
            Degrade(sender, e);
        }
        
        private SortedList<string, decimal[]> LinhasReceitas;

        private void Registralinha(string nome, double[] colunas, string PLA)
        {
            decimal[] Troca = new decimal[colunas.Length];
            for (int i = 0; i < colunas.Length; i++)
                Troca[i] = (decimal)colunas[i];
            Registralinha(nome, Troca, PLA);
        }

        private decimal SubTotalHonorarios;

        private void Registralinha(string nome, decimal[] colunas,string PLA)
        {
            if (nome != "")
            {
                if (PadraoPLA.PLAPadraoCodigo(PLAPadrao.honorarios) == PLA)
                {
                    LabelHono.Visible = SubTotHono.Visible = true;
                    SubTotalHonorarios += colunas[3];
                    //SubTotHono.Text = colunas[3].ToString("n2");
                    SubTotHono.Text = SubTotalHonorarios.ToString("n2");
                }
                else
                {
                    bool casuismo = false;
                    if (balancete != null)
                    {
                        if ((CON == 809) && (Framework.DSCentral.EMP == 3))
                            casuismo = true;
                    }
                    if (!casuismo)
                    {
                        if (colunas[4] != 0)
                        {
                            //if ((PLA != "112000") && (PLA != "113000") && (PLA != "114000"))
                            if (!PLA.EstaNoGrupo("112000", "113000", "114000", "123001"))
                                nome = Framework.datasets.dPLAnocontas.dPLAnocontasSt.GetDescricao(PLA);
                        }
                        else
                            if (colunas[1] == 0)
                                nome = Framework.datasets.dPLAnocontas.dPLAnocontasSt.GetDescricao(PLA);
                    }
                    decimal[] Valores;
                    if (LinhasReceitas.ContainsKey(nome))
                    {
                        Valores = (decimal[])LinhasReceitas[nome];
                    }
                    else
                    {
                        Valores = new decimal[5];
                        LinhasReceitas.Add(nome, Valores);
                    }
                    for (int i = 0; i < 5; i++)
                        Valores[i] += colunas[i];
                }
                                
            }
        }

        private void PublicaLinhaReceitas()
        {
            decimal[] Totais = new decimal[5];
            decimal RecNaoPrevito = 0;
            foreach (string nome in LinhasReceitas.Keys)
            {
                decimal[] Valores = LinhasReceitas[nome];
                ReceitaNome.Text += string.Format("\r\n  {0}", nome);
                for (int i = 0; i < 5; i++)
                {
                    Receitas[i].Text += string.Format("\r\n{0}", ((Valores[i] == 0) ? " " : Valores[i].ToString("n2")));
                    Totais[i] += Valores[i];
                    if ((i == (int)ColunasReceita.recMes) && (nome.ToUpper() == "MULTAS"))
                        RecNaoPrevito += (decimal)Valores[i];
                    if ((i == (int)ColunasReceita.recMes) && (nome.ToUpper() == "RENDIMENTOS FINANCEIROS"))
                        RecNaoPrevito += (decimal)Valores[i];
                };                
            };

            
            for (int i = 0; i < 5; i++)
                ReceitaTs[i].Text = string.Format("{0}", ((Totais[i] == 0) ? " " : Totais[i].ToString("n2")));
            
            if (balancete != null)
            {
                
                //decimal ValInad = (decimal)(Totais[(int)ColunasReceita.recPrevisto] - Totais[(int)ColunasReceita.recMes]) - ValorAntecipado.GetValueOrDefault(0);
                
                //if ((decimal)Totais[(int)ColunasReceita.recPrevisto] != 0)
                if (balancete.totPrevXRel.Previsto == 0)
                {
                    OcultaPrevRea.Value = true;
                }
                else
                {
                    xrPrevXEf1.Text =
                    string.Format("Prev. para o per�odo ({0:dd/MM/yyyy} a {1:dd/MM/yyyy}):\r\n", balancete.DataI, balancete.DataF) +
                    string.Format("Recebido antes de {0:dd/MM/yyyy}:\r\n", balancete.DataI) +
                                  "Recebido do previsto no per�odo:\r\n" +
                                  "Inadimplencia no per�odo:";
                    xrPrevXEf2.Text = string.Format("{0:n2}\r\n{1:n2}\r\n{2:n2}\r\n({4:n1}%) {3:n2}",
                                                       balancete.totPrevXRel.Previsto,
                                                       balancete.totPrevXRel.RecAnterior,
                                                       balancete.totPrevXRel.RecPrevisto,
                                                       balancete.totPrevXRel.Inad,
                                                       balancete.totPrevXRel.perInad
                                                   );
                }
                //else
                //    xrPrevXEf2.Text = string.Format("{0:n2}\r\n{1:n2}\r\n{2:n2}\r\n --",
                  //                                     Totais[(int)ColunasReceita.recPrevisto],
                    //                                   ValorAntecipado.HasValue ? ValorAntecipado : 0,
                      //                                 Totais[(int)ColunasReceita.recMes]
                        //                           );
                //ValorAntecipado = dImpBoletoBase1.PrevistoTableAdapter.ValorAntecipado(balancete.DataI, balancete.DataF, CON);                
            }
            else
            {
                /*
                decimal? ValorAntecipado;
                xrPrevXEf1.Text =
                    string.Format("Prev. para o per�odo ({0:dd/MM/yyyy} a {1:dd/MM/yyyy}):\r\n", SaldosAccessRow.Data_Inicial, SaldosAccessRow.Data_Final) +
                    string.Format("Recebido antes de {0:dd/MM/yyyy}:\r\n", SaldosAccessRow.Data_Inicial) +
                                  "Recebido do previsto no per�odo:\r\n" +
                                  "Inadimplencia no per�odo:";
                ValorAntecipado = (decimal?)dImpBoletoBase1.PrevistoTableAdapter.ValorAntecipado(SaldosAccessRow.Data_Inicial, SaldosAccessRow.Data_Final, CON);
                decimal ValInad = (decimal)(Totais[(int)ColunasReceita.recPrevisto] - Totais[(int)ColunasReceita.recMes]) - ValorAntecipado.GetValueOrDefault(0);
                if ((decimal)Totais[(int)ColunasReceita.recPrevisto] != 0)
                {
                    xrPrevXEf2.Text = string.Format("{0:n2}\r\n{1:n2}\r\n{2:n2}\r\n({4:n1}%) {3:n2}",
                                                       Totais[(int)ColunasReceita.recPrevisto],
                                                       ValorAntecipado.HasValue ? ValorAntecipado : 0,
                                                       Totais[(int)ColunasReceita.recMes] - RecNaoPrevito,
                                                       ValInad,
                                                       100.00M * ValInad / (decimal)Totais[(int)ColunasReceita.recPrevisto]
                                                   );
                }
                else
                    xrPrevXEf2.Text = string.Format("{0:n2}\r\n{1:n2}\r\n{2:n2}\r\n --",
                                                       Totais[(int)ColunasReceita.recPrevisto],
                                                       ValorAntecipado.HasValue ? ValorAntecipado : 0,
                                                       Totais[(int)ColunasReceita.recMes]
                                                   );
                */
            }                                        
            

            //ReceitaPrev.Text = ReceitaA.Text;
            //ReceitaPrevT.Text = ReceitaAT.Text;
            
        }

        //private bool? CONSeguro;

       


        //private static TiposSeguroObrigatorio StatusSeguroTESTE = TiposSeguroObrigatorio.Nao_disponivel;

        //private TiposSeguroObrigatorio StatusSeguro;

        private enum TiposSeguroObrigatorio
        { 
            Nao_disponivel,
            Primeiro_mes,
            Aderido,
            Nao_aderido
        }

        private bool QuadrosDinamicos = false;

        private void OcultaQuadrosDesp()
        {
            if (QuadrosDinamicos)
            {
                PDespG.Visible = PdespMO.Visible = PDespADMA.Visible = PAdministradora.Visible = PDespB.Visible = PDespH.Visible = false;                               
            }
            else
            {
                PDespG.Visible = (DespesaNome.Text != "");
                if (!PDespG.Visible)
                {
                    QuadroTotais.Top = QuadroContinua.Top = PDespH.Top;
                    PDespH.Top = PDespB.Top;
                    PDespB.Top = PAdministradora.Top;
                    PAdministradora.Top = PDespADMA.Top;
                    PDespADMA.Top = PdespMO.Top;
                    PdespMO.Top = PDespG.Top;
                };

                PdespMO.Visible = (DespesaNome1.Text != "");
                if (!PdespMO.Visible)
                {
                    QuadroTotais.Top = QuadroContinua.Top = PDespH.Top;
                    PDespH.Top = PDespB.Top;
                    PDespB.Top = PAdministradora.Top;
                    PAdministradora.Top = PDespADMA.Top;
                    PDespADMA.Top = PdespMO.Top;
                };

                PDespADMA.Visible = (DespesaNome.Text != "");
                if (!PDespADMA.Visible)
                {
                    QuadroTotais.Top = QuadroContinua.Top = PDespH.Top;
                    PDespH.Top = PDespB.Top;
                    PDespB.Top = PAdministradora.Top;
                    PAdministradora.Top = PDespADMA.Top;
                };

                PAdministradora.Visible = (DespesaNomeADM.Text != "");
                if (!PAdministradora.Visible)
                {
                    QuadroTotais.Top = QuadroContinua.Top = PDespH.Top;
                    PDespH.Top = PDespB.Top;
                    PDespB.Top = PAdministradora.Top;
                };

                PDespB.Visible = (DespesaNomeBANC.Text != "");
                if (!PDespB.Visible)
                {
                    QuadroTotais.Top = QuadroContinua.Top = PDespH.Top;
                    PDespH.Top = PDespB.Top;
                };

                PDespH.Visible = (DespesaNomeADV.Text != "");
                if (!PDespH.Visible)
                {
                    QuadroTotais.Top = QuadroContinua.Top = PDespH.Top;
                };
            }
        }

        private void Despesas()
        {
            if (balancete == null)
            {
                /*
                QuadroDesp.Text = "DESPESAS " + SaldosAccessRow.Data_Inicial.ToString("dd/MM/yy") + " � " + SaldosAccessRow.Data_Final.ToString("dd/MM/yy");
                double TotalLinha = 0;
                double[] SubTotais = new double[6];
                int indsub;
                for (indsub = 0; indsub < SubTotais.GetLength(0); indsub++)
                    SubTotais[indsub] = 0;

                double TotalDesp = 0;
                string nome = "";


                DespesaNome.Text = DespesaValor.Text = DespesaNome1.Text = DespesaValor1.Text = "";
                DespesaNome2.Text = DespesaValor2.Text = DespesaNomeADM.Text = DespesaValorADM.Text = "";
                DespesaNomeADV.Text = DespesaValorADV.Text = DespesaNomeBANC.Text = DespesaValorBANC.Text = "";


                ////////////////////
                //////GERAIS////////
                ////////////////////
                int maioritem = indsub = 0;
                double maiorvalor;
                dImpBoletoBase1.HDespesasAccessTableAdapter.Fill(dImpBoletoBase1.HDespesasAccess, CONCodigo, SaldosAccessRow.Data_Inicial, SaldosAccessRow.Data_Final);

                bool testePularReal = false;
                if (EmTeste)
                {
                    int LinhasQ1;
                    VirInput.Input.Execute("Linhas Q1", out LinhasQ1);
                    if (LinhasQ1 > 0)
                    {
                        for (int i = 1; LinhasQ1 > 0; LinhasQ1--, i++)
                            RegistralinhaDesp(DespesaNome, DespesaValor, string.Format("Teste teste teste {0}", i), 1000);
                        testePularReal = true;
                    }
                }
                if (!testePularReal)
                {

                    foreach (dImpBoletoBase.HDespesasAccessRow DR in dImpBoletoBase1.HDespesasAccess)
                        if ((Int32.Parse((string)DR["TipoPag"]) < 219999)
                            ||
                            (Int32.Parse((string)DR["TipoPag"]) >= 250000)
                            ||
                            (Int32.Parse((string)DR["TipoPag"]) == 230009)
                            )
                        {
                            if (nome != DR["descricao"].ToString())
                            {
                                RegistralinhaDesp(DespesaNome, DespesaValor, nome, TotalLinha);
                                nome = DR["descricao"].ToString();
                                TotalLinha = 0;
                            };
                            TotalLinha += (double)DR["valorpago"];
                            SubTotais[indsub] += (double)DR["valorpago"];
                            TotalDesp += (double)DR["valorpago"];
                        };
                }

                maiorvalor = indsub = 0;
                RegistralinhaDesp(DespesaNome, DespesaValor, nome, TotalLinha);
                DespesaValor.Text += "\r\n" + SubTotais[indsub].ToString("#,##0.00") + "       ";

                /////////////////////////
                //////M�O DE OBRA////////
                /////////////////////////
                nome = "";
                DespesaNome1.Text += "";
                DespesaValor1.Text += "";
                TotalLinha = 0;
                indsub++;


                if (EmTeste)
                {
                    int LinhasQ1;
                    VirInput.Input.Execute("Linhas Q2", out LinhasQ1);
                    if (LinhasQ1 > 0)
                    {
                        for (int i = 1; LinhasQ1 > 0; LinhasQ1--, i++)
                            RegistralinhaDesp(DespesaNome1, DespesaValor1, string.Format("Teste teste teste {0}", i), 1000);
                        testePularReal = true;
                    }
                }
                if (!testePularReal)
                {
                    foreach (dImpBoletoBase.HDespesasAccessRow DR in dImpBoletoBase1.HDespesasAccess)
                        if ((Int32.Parse((string)DR["TipoPag"]) >= 220000) && (Int32.Parse((string)DR["TipoPag"]) < 230000))
                        {
                            if (nome != DR["descricao"].ToString())
                            {
                                RegistralinhaDesp(DespesaNome1, DespesaValor1, nome, TotalLinha);
                                nome = DR["descricao"].ToString();
                                TotalLinha = 0;
                            };
                            TotalLinha += (double)DR["valorpago"];
                            SubTotais[indsub] += (double)DR["valorpago"];
                            TotalDesp += (double)DR["valorpago"];
                        };
                }

                if (SubTotais[indsub] > maiorvalor)
                {
                    maiorvalor = SubTotais[indsub];
                    maioritem = indsub;
                };

                RegistralinhaDesp(DespesaNome1, DespesaValor1, nome, TotalLinha);
                DespesaValor1.Text += "\r\n" + SubTotais[indsub].ToString("#,##0.00") + "       ";


                /////////////////////////////
                //////ADMINISTRATIVAS////////
                /////////////////////////////
                nome = "";
                DespesaNome2.Text += "";
                DespesaValor2.Text += "";
                TotalLinha = 0;
                indsub++;

                if (EmTeste)
                {
                    int LinhasQ1;
                    VirInput.Input.Execute("Linhas Q3", out LinhasQ1);
                    if (LinhasQ1 > 0)
                    {
                        for (int i = 1; LinhasQ1 > 0; LinhasQ1--, i++)
                            RegistralinhaDesp(DespesaNome2, DespesaValor2, string.Format("Teste teste teste {0}", i), 1000);
                        testePularReal = true;
                    }
                }
                if (!testePularReal)
                {
                    foreach (dImpBoletoBase.HDespesasAccessRow DR in dImpBoletoBase1.HDespesasAccess)
                    {
                        Int32 Conta = Int32.Parse((string)DR["TipoPag"]);
                        if ((Conta > 230002)
                            && (Conta <= 250000)
                            //  && (Conta != 240001)
                            //  && (Conta != 240004)
                            && (Conta != 240005)
                            && (Conta != 240006)
                            && (Conta != 230009)
                            //  && (Conta != 240007)
                            )
                        {
                            if (nome != DR["descricao"].ToString())
                            {
                                RegistralinhaDesp(DespesaNome2, DespesaValor2, nome, TotalLinha);
                                nome = DR["descricao"].ToString();
                                TotalLinha = 0;
                            };
                            TotalLinha += (double)DR["valorpago"];
                            SubTotais[indsub] += (double)DR["valorpago"];
                            TotalDesp += (double)DR["valorpago"];
                        };
                    };
                }
                if (SubTotais[indsub] > maiorvalor)
                {
                    maiorvalor = SubTotais[indsub];
                    maioritem = indsub;
                };

                RegistralinhaDesp(DespesaNome2, DespesaValor2, nome, TotalLinha);
                DespesaValor2.Text += "\r\n" + SubTotais[indsub].ToString("#,##0.00") + "       ";

                xrTotalDesp.Text = TotalDesp.ToString("#,##0.00");


                /////////////////////////////
                //////ADMINISTRADORA/////////
                /////////////////////////////
                nome = "";
                DespesaNomeADM.Text += "";
                DespesaValorADM.Text += "";
                TotalLinha = 0;
                indsub++;
                if (EmTeste)
                {
                    int LinhasQ1;
                    VirInput.Input.Execute("Linhas Q4", out LinhasQ1);
                    if (LinhasQ1 > 0)
                    {
                        for (int i = 1; LinhasQ1 > 0; LinhasQ1--, i++)
                            RegistralinhaDesp(DespesaNomeADM, DespesaValorADM, string.Format("Teste teste teste {0}", i), 1000);
                        testePularReal = true;
                    }
                }
                if (!testePularReal)
                {
                    foreach (dImpBoletoBase.HDespesasAccessRow DR in dImpBoletoBase1.HDespesasAccess)
                        if (
                            ((Int32.Parse((string)DR["TipoPag"]) > 230000) && (Int32.Parse((string)DR["TipoPag"]) < 230003))
                            //        ||
                            //      (Int32.Parse((string)DR["TipoPag"]) == 240001)
                            //    ||
                            //  (Int32.Parse((string)DR["TipoPag"]) == 240004)
                            //   ||
                            // (Int32.Parse((string)DR["TipoPag"]) == 240007)
                            )
                        {
                            if (nome != DR["descricao"].ToString())
                            {
                                RegistralinhaDesp(DespesaNomeADM, DespesaValorADM, nome, TotalLinha);
                                nome = DR["descricao"].ToString();
                                TotalLinha = 0;
                            };
                            TotalLinha += (double)DR["valorpago"];
                            SubTotais[indsub] += (double)DR["valorpago"];
                            TotalDesp += (double)DR["valorpago"];
                        };
                }
                if (SubTotais[indsub] > maiorvalor)
                {
                    maiorvalor = SubTotais[indsub];
                    maioritem = indsub;
                };
                RegistralinhaDesp(DespesaNomeADM, DespesaValorADM, nome, TotalLinha);
                DespesaValorADM.Text += "\r\n" + SubTotais[indsub].ToString("#,##0.00") + "       ";

                xrTotalDesp.Text = TotalDesp.ToString("#,##0.00");




                /////////////////////////////
                ////////////Bancarias////////
                /////////////////////////////
                nome = "";
                DespesaNomeBANC.Text += "";
                DespesaValorBANC.Text += "";
                TotalLinha = 0;
                indsub++;
                if (EmTeste)
                {
                    int LinhasQ1;
                    VirInput.Input.Execute("Linhas Q5", out LinhasQ1);
                    if (LinhasQ1 > 0)
                    {
                        for (int i = 1; LinhasQ1 > 0; LinhasQ1--, i++)
                            RegistralinhaDesp(DespesaNomeBANC, DespesaValorBANC, string.Format("Teste teste teste {0}", i), 1000);
                        testePularReal = true;
                    }
                }
                if (!testePularReal)
                {
                    foreach (dImpBoletoBase.HDespesasAccessRow DR in dImpBoletoBase1.HDespesasAccess)
                        if (
                            (Int32.Parse((string)DR["TipoPag"]) == 240005)
                            ||
                            (Int32.Parse((string)DR["TipoPag"]) == 240006)
                            )
                        {
                            if (nome != DR["descricao"].ToString())
                            {
                                RegistralinhaDesp(DespesaNomeBANC, DespesaValorBANC, nome, TotalLinha);
                                nome = DR["descricao"].ToString();
                                TotalLinha = 0;
                            };
                            TotalLinha += (double)DR["valorpago"];
                            SubTotais[indsub] += (double)DR["valorpago"];
                            TotalDesp += (double)DR["valorpago"];
                        };
                }
                if (SubTotais[indsub] > maiorvalor)
                {
                    maiorvalor = SubTotais[indsub];
                    maioritem = indsub;
                };

                RegistralinhaDesp(DespesaNomeBANC, DespesaValorBANC, nome, TotalLinha);
                DespesaValorBANC.Text += "\r\n" + SubTotais[indsub].ToString("#,##0.00") + "       ";

                xrTotalDesp.Text = TotalDesp.ToString("#,##0.00");


                /////////////////////////////
                ////////ADVOCATICEAS/////////
                /////////////////////////////
                nome = "";
                DespesaNomeADV.Text += "";
                DespesaValorADV.Text += "";
                TotalLinha = 0;
                indsub++;
                if (EmTeste)
                {
                    int LinhasQ1;
                    VirInput.Input.Execute("Linhas Q6", out LinhasQ1);
                    if (LinhasQ1 > 0)
                    {
                        for (int i = 1; LinhasQ1 > 0; LinhasQ1--, i++)
                            RegistralinhaDesp(DespesaNomeADV, DespesaValorADV, string.Format("Teste teste teste {0}", i), 1000);
                        testePularReal = true;
                    }
                }
                if (!testePularReal)
                {
                    foreach (dImpBoletoBase.HDespesasAccessRow DR in dImpBoletoBase1.HDespesasAccess)
                        if (Int32.Parse((string)DR["TipoPag"]) == 230000)
                        {
                            TotalLinha += (double)DR["valorpago"];
                            SubTotais[indsub] += (double)DR["valorpago"];
                            TotalDesp += (double)DR["valorpago"];
                        };

                    if (SubTotais[indsub] > maiorvalor)
                    {
                        maiorvalor = SubTotais[indsub];
                        maioritem = indsub;
                    };




                    if (SubTotais[indsub] != 0)
                    {
                        RegistralinhaDesp(DespesaNomeADV, DespesaValorADV, "Honor�rios", TotalLinha);
                        DespesaValorADV.Text += "\r\n" + SubTotais[indsub].ToString("#,##0.00") + "       ";
                    };
                }




                xrTotalDesp.Text = TotalDesp.ToString("#,##0.00");

                XRLabel[] pors = new XRLabel[] { DGpor, MOpor, ADMpor, ADMApor, Bancpor, HORpor };
                XRLine[] Lines = new XRLine[] { DGLine, MOLine, ADMLine, ADMALine, BancLine, HORLine };
                XRLine[] Lines1 = new XRLine[] { DGLine1, MOLine1, ADMLine1, ADMALine1, BancLine1, HORLine1 };

                int arr = 0;
                for (indsub = 0; indsub < SubTotais.GetLength(0); indsub++)
                {
                    if (indsub != maioritem)
                    {
                        if (TotalDesp != 0)
                            arr += Convert.ToInt32((1000.0 * SubTotais[indsub] / TotalDesp));
                    }

                };

                for (indsub = 0; indsub < SubTotais.GetLength(0); indsub++)
                {
                    if (SubTotais[indsub] != 0)
                    {
                        if (indsub != maioritem)
                            pors[indsub].Text = ((100.0 * SubTotais[indsub] / TotalDesp)).ToString("0.0") + "%";
                        else
                            pors[indsub].Text = ((1000.0 - arr) / 10).ToString("0.0") + "%";


                        Lines[indsub].Width = (Int16)(370 * SubTotais[indsub] / TotalDesp);

                        Lines1[indsub].Left = Lines[indsub].Width;
                        Lines1[indsub].Width = 370 - Lines1[indsub].Left;
                    };
                };
                */
            }
            else
            {                
                decimal TotalDesp = 0;
                QuadroDesp.Text = string.Format("DESPESAS {0:dd/MM/yy} � {1:dd/MM/yy}", balancete.DataI, balancete.DataF);
                DespesaNome.Text = DespesaValor.Text = DespesaNome1.Text = DespesaValor1.Text = "";
                DespesaNome2.Text = DespesaValor2.Text = DespesaNomeADM.Text = DespesaValorADM.Text = "";
                DespesaNomeADV.Text = DespesaValorADV.Text = DespesaNomeBANC.Text = DespesaValorBANC.Text = "";

                if (balancete.TotDebitosGrupo != null)
                {
                    QuadrosDinamicos = true;                    
                    int iMaioItem = 0;
                    decimal MaiorValor = 0;
                    decimal TotaArr = 0;
                    TotalDesp = 0;
                    DeslocamentoY = 37;
                    nGrupoDebito = 0;
                    foreach (string SuperGrupo in balancete.TotDebitosGrupo.Keys)
                        foreach (string Grupo in balancete.TotDebitosGrupo[SuperGrupo].Keys)
                        {
                            TotalDesp += balancete.TotDebitosGrupo[SuperGrupo][Grupo].Total;
                            if (balancete.TotDebitosGrupo[SuperGrupo][Grupo].Total > MaiorValor)
                            {
                                MaiorValor = balancete.TotDebitosGrupo[SuperGrupo][Grupo].Total;
                                iMaioItem = nGrupoDebito;
                            };
                            nGrupoDebito++;
                        }
                    nGrupoDebito = 0;
                    PorcentagensArr = new SortedList<int, decimal>();
                    foreach (string SuperGrupo in balancete.TotDebitosGrupo.Keys)
                        foreach (string Grupo in balancete.TotDebitosGrupo[SuperGrupo].Keys)
                        {
                            if (nGrupoDebito != iMaioItem)
                            {
                                decimal PorcentagenArr = Math.Round(balancete.TotDebitosGrupo[SuperGrupo][Grupo].Total / TotalDesp, 3, MidpointRounding.AwayFromZero);
                                PorcentagensArr.Add(nGrupoDebito, PorcentagenArr);
                                TotaArr += PorcentagenArr;
                            }
                            nGrupoDebito++;
                        }
                    PorcentagensArr.Add(iMaioItem, 1 - TotaArr);
                    nGrupoDebito = 0;
                    Tranferido = false;
                    foreach (string SuperGrupo in balancete.TotDebitosGrupo.Keys)
                    {
                        XRLabel SuperCabecTransferir = null;                        
                        if (balancete.TotDebitosGrupo.Count > 1)
                            SuperCabecTransferir = CriaSuperGrupoDebito(SuperGrupo);
                        decimal TotalSGrupo = 0;
                        int contador = 0;
                        foreach (string Grupo in balancete.TotDebitosGrupo[SuperGrupo].Keys)
                        {
                            contador++;
                            if (Tranferido)
                                SuperCabecTransferir = null;
                            float Rodape = 0;
                            if ((balancete.TotDebitosGrupo.Count > 1) && (contador == balancete.TotDebitosGrupo[SuperGrupo].Count))
                                Rodape = 10 + Modelo_Label_Sub_TotalValG.HeightF;
                            CriaGrupoDebito(Grupo, balancete.TotDebitosGrupo[SuperGrupo][Grupo], SuperCabecTransferir,Rodape);
                            SuperCabecTransferir = null;
                            TotalSGrupo += balancete.TotDebitosGrupo[SuperGrupo][Grupo].Total;
                        }
                        if (balancete.TotDebitosGrupo.Count > 1)
                            CriaSuperGrupoDebitoRodape(SuperGrupo,TotalSGrupo);
                        QuadroTotais.Top = DeslocamentoY;                        
                        xrTotalDesp.Text = string.Format("{0:n2}", TotalDesp);                     
                    }
                    if (Tranferido && (QuadroTotais.BottomF > QuadroContinuacao.HeightF))
                    {
                        TituloContinuacao.Text = "* " + TituloContinuacao.Text;
                        Cabe = false;
                    }
                }
                /*
                else
                {
                    if (balancete.TotDebitos.ContainsKey(ABS_balancete.GrupoDebito.gerais))
                    {
                        DespesaNome.Text = balancete.TotDebitos[ABS_balancete.GrupoDebito.gerais].Descricao;
                        DespesaValor.Text = balancete.TotDebitos[ABS_balancete.GrupoDebito.gerais].strValores;
                        TotalDesp += balancete.TotDebitos[ABS_balancete.GrupoDebito.gerais].Total;
                    };
                    if (balancete.TotDebitos.ContainsKey(ABS_balancete.GrupoDebito.MaoObra))
                    {
                        DespesaNome1.Text = balancete.TotDebitos[ABS_balancete.GrupoDebito.MaoObra].Descricao;
                        DespesaValor1.Text = balancete.TotDebitos[ABS_balancete.GrupoDebito.MaoObra].strValores;
                        TotalDesp += balancete.TotDebitos[ABS_balancete.GrupoDebito.MaoObra].Total;
                    };
                    if (balancete.TotDebitos.ContainsKey(ABS_balancete.GrupoDebito.Administrativas))
                    {
                        DespesaNome2.Text = balancete.TotDebitos[ABS_balancete.GrupoDebito.Administrativas].Descricao;
                        DespesaValor2.Text = balancete.TotDebitos[ABS_balancete.GrupoDebito.Administrativas].strValores;
                        TotalDesp += balancete.TotDebitos[ABS_balancete.GrupoDebito.Administrativas].Total;
                    };
                    if (balancete.TotDebitos.ContainsKey(ABS_balancete.GrupoDebito.Administradora))
                    {
                        DespesaNomeADM.Text = balancete.TotDebitos[ABS_balancete.GrupoDebito.Administradora].Descricao;
                        DespesaValorADM.Text = balancete.TotDebitos[ABS_balancete.GrupoDebito.Administradora].strValores;
                        TotalDesp += balancete.TotDebitos[ABS_balancete.GrupoDebito.Administradora].Total;
                    }
                    if (balancete.TotDebitos.ContainsKey(ABS_balancete.GrupoDebito.Bancarias))
                    {
                        DespesaNomeBANC.Text = balancete.TotDebitos[ABS_balancete.GrupoDebito.Bancarias].Descricao;
                        DespesaValorBANC.Text = balancete.TotDebitos[ABS_balancete.GrupoDebito.Bancarias].strValores;
                        TotalDesp += balancete.TotDebitos[ABS_balancete.GrupoDebito.Bancarias].Total;
                    };
                    if (balancete.TotDebitos.ContainsKey(ABS_balancete.GrupoDebito.Honorarios))
                    {
                        DespesaNomeADV.Text = balancete.TotDebitos[ABS_balancete.GrupoDebito.Honorarios].Descricao;
                        DespesaValorADV.Text = balancete.TotDebitos[ABS_balancete.GrupoDebito.Honorarios].strValores;
                        TotalDesp += balancete.TotDebitos[ABS_balancete.GrupoDebito.Honorarios].Total;
                    };
                    xrTotalDesp.Text = string.Format("{0:n2}", TotalDesp);

                    XRLabel[] pors = new XRLabel[] { DGpor, MOpor, ADMpor, ADMApor, Bancpor, HORpor };
                    XRLine[] Lines = new XRLine[] { DGLine, MOLine, ADMLine, ADMALine, BancLine, HORLine };
                    XRLine[] Lines1 = new XRLine[] { DGLine1, MOLine1, ADMLine1, ADMALine1, BancLine1, HORLine1 };

                    if (TotalDesp != 0)
                    {
                        ABS_balancete.GrupoDebito MaioIten = ABS_balancete.GrupoDebito.gerais;
                        decimal MaiorValor = 0;
                        decimal TotaArr = 0;
                        SortedList<ABS_balancete.GrupoDebito, decimal> Arredondados = new SortedList<ABS_balancete.GrupoDebito, decimal>();
                        foreach (ABS_balancete.GrupoDebito GrX in balancete.TotDebitos.Keys)
                        {
                            decimal Arredondado = Math.Round(balancete.TotDebitos[GrX].Total / TotalDesp, 3, MidpointRounding.AwayFromZero);
                            Arredondados.Add(GrX, Arredondado);
                            TotaArr += Arredondado;
                            if (balancete.TotDebitos[GrX].Total > MaiorValor)
                            {
                                MaiorValor = balancete.TotDebitos[GrX].Total;
                                MaioIten = GrX;
                            }
                        };
                        Arredondados[MaioIten] += (1 - TotaArr);
                        foreach (ABS_balancete.GrupoDebito GrX in balancete.TotDebitos.Keys)
                        {
                            pors[(int)GrX].Text = string.Format("{0:p1}", Arredondados[GrX]);
                            Lines[(int)GrX].Width = (int)(370 * Arredondados[GrX]);

                            Lines1[(int)GrX].Left = Lines[(int)GrX].Width;
                            Lines1[(int)GrX].Width = 370 - Lines1[(int)GrX].Left;
                        }
                    }
                }*/                
            }
        }


        #region Quadros din�micos de despesa        

        private void CriaSuperGrupoDebitoRodape(string Titulo, decimal Valor)
        {
            XRLabel Label_Sub_TotalG = ImpGeradorDeEfeitos.ClonaXRLabel(Modelo_Label_Sub_TotalG, string.Format("{0}", nSGrupoDebito - 1));
            XRLabel Label_Sub_TotalValG = ImpGeradorDeEfeitos.ClonaXRLabel(Modelo_Label_Sub_TotalValG, string.Format("{0}", nSGrupoDebito - 1));
            
            Label_Sub_TotalG.LocationFloat = new DevExpress.Utils.PointFloat(Modelo_Label_Sub_TotalG.LocationF.X, DeslocamentoY);
            Label_Sub_TotalG.Text = string.Format("Sub Total {0}", Titulo);

            Label_Sub_TotalValG.LocationFloat = new DevExpress.Utils.PointFloat(Modelo_Label_Sub_TotalValG.LocationF.X, DeslocamentoY);
            Label_Sub_TotalValG.Text = string.Format("{0:n2}",Valor);

            DevExpress.XtraReports.UI.XRControl[] Novos = new DevExpress.XtraReports.UI.XRControl[] { Label_Sub_TotalG, Label_Sub_TotalValG };

            if (Tranferido)
                QuadroContinuacao.Controls.AddRange(Novos);
            else
                QuadroDespesas.Controls.AddRange(Novos);   

            DeslocamentoY = Label_Sub_TotalG.Bottom + 10;            
        }

        private XRLabel CriaSuperGrupoDebito(string Titulo)
        {
            //if (nSGrupoDebito != 0)            
            DeslocamentoY += 10;            
            XRLabel Label_SGrupo = ImpGeradorDeEfeitos.ClonaXRLabel(ModeloSGrupo, string.Format("{0}", nSGrupoDebito++));
            if (Tranferido)
                QuadroContinuacao.Controls.Add(Label_SGrupo);
            else
                QuadroDespesas.Controls.Add(Label_SGrupo);            
            Label_SGrupo.LocationFloat = new DevExpress.Utils.PointFloat(0F, DeslocamentoY);            
            Label_SGrupo.Text = Titulo;
            DeslocamentoY = Label_SGrupo.Bottom + 10;
            return Label_SGrupo;
        }

        private int nGrupoDebito;
        int nSGrupoDebito = 0;
        private SortedList<int, decimal> PorcentagensArr;
        private int DeslocamentoY;

        private bool Tranferido = false;

        private void TranfereVerso()
        {
            Tranferido = true;
            QuadroContinuacao.Visible = true;
            QuadroContinua.Visible = true;
            QuadroContinua.Top = DeslocamentoY;

            DeslocamentoY = 37;
            QuadroTotais.Parent = QuadroContinuacao;
        }

        private void CriaGrupoDebito(string Titulo, ABS_balancete.TotDebito TotDebito, XRLabel Super,float ReservarRodape = 0)
        {
            XRLabel Label_Sub_Total;
            XRLabel Label_Sub_TotalVal;
            XRLine LinePor1;
            XRLine LinePor2;
            XRPanel PanelPorcent;
            XRLabel LabelValPor;
            XRLabel LabelTituloGrupo;
            XRLabel LabelValorGrupo;
            XRLabel LabelDescrisGrupo;
            XRPanel PanelGrupo;

            int LinhaPicotar = -1;

            //Verifica se cabe

            //O QuadroDespesas.HeightF tem o tamanho efetivo de 1638 (n�o sei o motivo)
            float QuadroDespesasHeightF = 1638;

            //float EspacoDisponivel = QuadroDespesas.HeightF - DeslocamentoY - 10F - QuadroTotais.HeightF - ReservarRodape;
            float EspacoDisponivel = QuadroDespesasHeightF - DeslocamentoY - 10F - QuadroTotais.HeightF - ReservarRodape;

            float PainelHeightSimulado = Modelo_PanelGrupo.HeightF;
            PainelHeightSimulado += Modelo_LabelDescrisGrupo.HeightF * ((TotDebito.Desc_Valor.Count) - 1);
            if (TotDebito.Desc_Valor.Count == 1)
                PainelHeightSimulado -= (Modelo_Label_Sub_TotalVal.HeightF + 8F);

            //Verifica se cabe inteiro
            if (PainelHeightSimulado > EspacoDisponivel)
            {
                EspacoDisponivel += (Modelo_Label_Sub_TotalVal.HeightF + 10F); // n�o vai ter o sub total
                EspacoDisponivel += ReservarRodape; // O rodap� ser� tranferido
                float LinhasFicaF = (EspacoDisponivel - (Modelo_PanelGrupo.HeightF - Modelo_LabelDescrisGrupo.HeightF)) / Modelo_LabelDescrisGrupo.HeightF;
                int LinhasFica = (int)Math.Truncate((decimal)LinhasFicaF);

                if ((TotDebito.Desc_Valor.Count < 4) || (LinhasFica < 2))
                    TranfereVerso();
                else
                {
                    LinhaPicotar = LinhasFica - 1; //zero base
                    if ((TotDebito.Desc_Valor.Count - LinhasFica) < 2)
                        LinhaPicotar = TotDebito.Desc_Valor.Count - 3;                    
                }
            }

            LinePor1 = ImpGeradorDeEfeitos.ClonaXRLine(Modelo_LinePor1, string.Format("{0}", nGrupoDebito));
            LinePor2 = ImpGeradorDeEfeitos.ClonaXRLine(Modelo_LinePor2, string.Format("{0}", nGrupoDebito));

            PanelPorcent = ImpGeradorDeEfeitos.ClonaXRPanel(Modelo_PanelPorcent, string.Format("{0}", nGrupoDebito));            
            PanelPorcent.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] 
                       {
                                                          LinePor1,
                                                          LinePor2         
                       });            
            //PanelPorcent.LocationFloat = new DevExpress.Utils.PointFloat(310F, 8F);
            //PanelPorcent.Name = string.Format("PanelPorcent_{0}", nGrupoDebito);
            //PanelPorcent.SizeF = Modelo_PanelPorcent.SizeF;
            //PanelPorcent.StylePriority.UseBorders = Modelo_PanelPorcent.StylePriority.UseBorders;

            LabelValPor = ImpGeradorDeEfeitos.ClonaXRLabel(Modelo_LabelValPor, string.Format("{0}", nGrupoDebito));

            LabelTituloGrupo = ImpGeradorDeEfeitos.ClonaXRLabel(Modelo_LabelTituloGrupo, string.Format("{0}", nGrupoDebito));            
            LabelTituloGrupo.Text = Titulo;

            Label_Sub_Total = ImpGeradorDeEfeitos.ClonaXRLabel(Modelo_Label_Sub_Total, string.Format("{0}", nGrupoDebito));            
            Label_Sub_Total.Text = Modelo_Label_Sub_Total.Text;

            Label_Sub_TotalVal = ImpGeradorDeEfeitos.ClonaXRLabel(Modelo_Label_Sub_TotalVal, string.Format("{0}", nGrupoDebito));            
            Label_Sub_TotalVal.Text = string.Format("{0:n2}", TotDebito.Total);
            
            //PanelGrupo = new XRPanel();
            PanelGrupo = ImpGeradorDeEfeitos.ClonaXRPanel(Modelo_PanelGrupo, string.Format("{0}", nGrupoDebito));
            if (Tranferido)
            {
                if (Super != null)
                {
                    QuadroContinua.TopF = (Super.TopF - 10F);
                    Super.Parent = QuadroContinuacao;
                    DeslocamentoY += 10;
                    Super.TopF = DeslocamentoY;
                    DeslocamentoY += (int)(Super.HeightF + 10F);
                }
                QuadroContinuacao.Controls.Add(PanelGrupo);
            }
            else
                QuadroDespesas.Controls.Add(PanelGrupo);
            //PanelGrupo.BorderColor = Modelo_PanelGrupo.BorderColor;
            //PanelGrupo.CanGrow = Modelo_PanelGrupo.CanGrow;
            //PanelGrupo.Borders = Modelo_PanelGrupo.Borders;
            PanelGrupo.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] 
                     {
                                                          Label_Sub_Total,
                                                          Label_Sub_TotalVal,
                                                          PanelPorcent,
                                                          LabelValPor,
                                                          LabelTituloGrupo                                                          
                     });
            //PanelGrupo.Dpi = Modelo_PanelGrupo.Dpi;
            //PanelGrupo.Name = string.Format("PanelGrupo_{0}", nGrupoDebito);

            float PainelHeight = Modelo_PanelGrupo.HeightF;

            int n = 0;
            foreach (string DescX in TotDebito.Desc_Valor.Keys)
            {
                LabelDescrisGrupo = ImpGeradorDeEfeitos.ClonaXRLabel(Modelo_LabelDescrisGrupo, string.Format("{0}_{1}", nGrupoDebito, n));                
                LabelDescrisGrupo.LocationFloat = new DevExpress.Utils.PointFloat(
                    Modelo_LabelDescrisGrupo.LocationF.X,
                    Modelo_LabelDescrisGrupo.LocationF.Y + n * Modelo_LabelDescrisGrupo.SizeF.Height);                
                LabelDescrisGrupo.Text = DescX;
                PanelGrupo.Controls.Add(LabelDescrisGrupo);

                if (n > 0)
                    PainelHeight += LabelDescrisGrupo.HeightF;

                if (TotDebito.Desc_Valor.Count > 1)
                {
                    LabelValorGrupo = ImpGeradorDeEfeitos.ClonaXRLabel(Modelo_LabelValorGrupo, string.Format("{0}_{1}", nGrupoDebito, n));                    
                    LabelValorGrupo.LocationFloat = new DevExpress.Utils.PointFloat(
                                                          Modelo_LabelValorGrupo.LocationF.X,
                                                          Modelo_LabelValorGrupo.LocationF.Y + n * Modelo_LabelDescrisGrupo.SizeF.Height);                    
                    LabelValorGrupo.Draw += new DevExpress.XtraReports.UI.DrawEventHandler(this.DegradeL);
                    LabelValorGrupo.Text = string.Format("{0:n2}", TotDebito.Desc_Valor[DescX]);
                    PanelGrupo.Controls.Add(LabelValorGrupo);
                }
                if (n == LinhaPicotar)
                {
                    PanelGrupo.Borders = ((DevExpress.XtraPrinting.BorderSide)(DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top));
                    PanelGrupo.LocationFloat = new DevExpress.Utils.PointFloat(0F, (float)DeslocamentoY);
                    PainelHeight -= (Modelo_Label_Sub_Total.HeightF + 10F);
                    PanelGrupo.SizeF = new System.Drawing.SizeF(Modelo_PanelGrupo.WidthF, PainelHeight);
                    DeslocamentoY = PanelGrupo.Bottom + 10;
                    TranfereVerso();
                    LabelTituloGrupo = ImpGeradorDeEfeitos.ClonaXRLabel(Modelo_LabelTituloGrupo, string.Format("{0}_Cont", nGrupoDebito));                    
                    LabelTituloGrupo.SizeF = new System.Drawing.SizeF(Modelo_LabelTituloGrupo.WidthF * 2, Modelo_LabelTituloGrupo.HeightF);                    
                    LabelTituloGrupo.Text = string.Format("{0} (Continua��o)", Titulo);

                    PanelGrupo = ImpGeradorDeEfeitos.ClonaXRPanel(Modelo_PanelGrupo, string.Format("{0}_T", nGrupoDebito));
                    QuadroContinuacao.Controls.Add(PanelGrupo);
                    //PanelGrupo.BorderColor = Modelo_PanelGrupo.BorderColor;
                    //PanelGrupo.CanGrow = Modelo_PanelGrupo.CanGrow;
                    //PanelGrupo.Borders = ((DevExpress.XtraPrinting.BorderSide)(DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Bottom));
                    PanelGrupo.Controls.Add(LabelTituloGrupo);
                    Label_Sub_Total.Parent = PanelGrupo;
                    Label_Sub_TotalVal.Parent = PanelGrupo;
                    //PanelGrupo.Dpi = Modelo_PanelGrupo.Dpi;
                    //PanelGrupo.Name = string.Format("PanelGrupo_{0}_T", nGrupoDebito);

                    PainelHeight = Modelo_PanelGrupo.HeightF;// -Modelo_LabelTituloGrupo.HeightF;
                    LinhaPicotar = -1;
                    n = 0;
                }
                else
                    n++;
            }
            if (TotDebito.Desc_Valor.Count > 1)
            {
                Label_Sub_Total.LocationFloat = new DevExpress.Utils.PointFloat(
                                                          Modelo_Label_Sub_Total.LocationF.X,
                                                          Modelo_Label_Sub_Total.LocationF.Y + (n - 1) * Modelo_LabelDescrisGrupo.SizeF.Height);
                Label_Sub_TotalVal.LocationF = new DevExpress.Utils.PointFloat(
                                                          Modelo_Label_Sub_TotalVal.LocationF.X,
                                                          Modelo_Label_Sub_TotalVal.LocationF.Y + (n - 1) * Modelo_LabelDescrisGrupo.SizeF.Height);
            }
            else
            {
                Label_Sub_Total.LocationFloat = new DevExpress.Utils.PointFloat(
                                                          Modelo_Label_Sub_Total.LocationF.X,
                                                          Modelo_LabelValorGrupo.LocationF.Y);
                Label_Sub_TotalVal.LocationF = new DevExpress.Utils.PointFloat(
                                                          Modelo_Label_Sub_TotalVal.LocationF.X,
                                                          Modelo_LabelValorGrupo.LocationF.Y);
                PainelHeight -= (Modelo_Label_Sub_TotalVal.HeightF + 8F);
            }

            PanelGrupo.LocationFloat = new DevExpress.Utils.PointFloat(0F, (float)DeslocamentoY);            

            LabelValPor.Text = string.Format("{0:p1}", PorcentagensArr[nGrupoDebito]);
            LinePor1.Width = (int)(370 * PorcentagensArr[nGrupoDebito]);
            LinePor2.Left = LinePor1.Width;
            LinePor2.Width = 370 - LinePor1.Left;

            PanelGrupo.SizeF = new System.Drawing.SizeF(Modelo_PanelGrupo.WidthF, PainelHeight);

            DeslocamentoY = PanelGrupo.Bottom + 10;

            nGrupoDebito++;
        }

        #endregion



        private void RegistralinhaDesp(XRLabel Quadro, XRLabel Quadroval, string nome, double valor)
        {
            if (nome != "")
            {
                if (Quadro.Text != "")
                {
                    Quadro.Text += "\r\n";
                    Quadroval.Text += "\r\n";
                    //deltaposicao += aumentoporlinha;
                };
                Quadro.Text += "    " + nome;
                Quadroval.Text += valor.ToString("#,##0.00");
            }
        }

        //private double RentabilidadeTotal = 0;

        enum ColunasReceita
        {
            recAtrasado = 0,
            recMes = 1,
            recAtecipado = 2,
            recTotalMes = 3,
            recPrevisto = 4
        }

        private string RemoveParenteses(string entrada)
        {
            string limpo = entrada.Trim();
            for (int i = 0; i < limpo.Length; i++)
                if (limpo[i] == '(')
                {
                    int inicio = i;
                    i++;
                    if (i >= limpo.Length)
                        break;
                    while (char.IsWhiteSpace(limpo[i]) || char.IsDigit(limpo[i]))
                        i++;
                    if (limpo[i] == '/')
                    {
                        i++;
                        if (i >= limpo.Length)
                            break;
                        while (char.IsWhiteSpace(limpo[i]) || char.IsDigit(limpo[i]))
                        {
                            i++;
                            if (i >= limpo.Length)
                                break;
                        }
                        if (i >= limpo.Length)
                            break;
                        if (limpo[i] == ')')
                        {
                            i++;
                            if (i >= limpo.Length)
                                break;
                            limpo = limpo.Remove(inicio, i - inicio).Trim();
                            break;
                        }
                    }
                };
            return limpo;
        }

        

        private void QuadoReceitas()
        {
            double[] colunas = new double[5];
            //string nome = "";
            //string PLA = "";
            //int colunasel;
            ReceitaNome.Text = "";
            ReceitaA.Text = "";
            ReceitaMes.Text = "";
            RecetaP.Text = "";
            ReceitaT.Text = "";
            ReceitaPrev.Text = "";
            double TotalRec = 0;

            LinhasReceitas = new SortedList<string, decimal[]>();
            SubTotalHonorarios = 0;
            //if ((balancete == null) || (CompontesBasicos.FormPrincipalBase.strEmProducao))
            if (balancete == null)
            {
                /*
                xrLabel12.Text = string.Format("RECEITAS {0:dd/MM/yy} � {1:dd/MM/yy}", SaldosAccessRow.Data_Inicial, SaldosAccessRow.Data_Final);

                foreach (dImpBoletoBase.ReceitasAccessRow DR in dImpBoletoBase1.ReceitasAccess)
                {
                    string limpo = RemoveParenteses(DR.descricao);                    
                    if (limpo != nome)
                    {
                        //registra o anterior e prepara o novo
                        Registralinha(nome, colunas, PLA);
                        nome = limpo;
                        PLA = DR.PLA;
                        for (int i = 0; i < 5; i++)
                            colunas[i] = 0;
                    };
                    if ((!DR.IsCodigoCatNull()) || DR.IsDocumentoNull() || (DR.Documento == 0))
                    {
                        if (DR.IsVers�oNull())
                            colunasel = 1;
                        else
                            colunasel = DR.Vers�o;

                    }
                    else
                    {
                        DateTime Vencto;


                        if (VirMSSQL.TableAdapter.STTableAdapter.BuscaEscalar("SELECT BOLVencto FROM BOLetos WHERE (BOL = @P1)", out Vencto, DR.Documento))
                        {
                            if (Vencto < SaldosAccessRow.Data_Inicial)
                                colunasel = 0;
                            else if (Vencto > SaldosAccessRow.Data_Final)
                                colunasel = 2;
                            else
                                colunasel = 1;
                        }
                        else
                            colunasel = 0;

                    };
                    colunas[colunasel] += DR.valorpago;
                    colunas[(int)ColunasReceita.recTotalMes] += DR.valorpago;

                    TotalRec += DR.valorpago;
                };
                Registralinha(nome, colunas, PLA);

                LevantamentoPrevisto();
                */
                
                
            }
            else
            {
                if (balancete.TotCreditos.Count > 15)
                    AjustaFontesRec(tamfonte.T475);
                else if (balancete.TotCreditos.Count > 10)
                    AjustaFontesRec(tamfonte.T500);
                xrLabel12.Text = string.Format("RECEITAS {0:dd/MM/yy} � {1:dd/MM/yy}", balancete.DataI, balancete.DataF);

                foreach (ABS_balancete.TotCredito TCred in balancete.TotCreditos.Values)
                {
                    Registralinha(TCred.Descricao, new decimal[] { TCred.Atrasado, 
                                                                   TCred.Mes, 
                                                                   TCred.Antecipado, 
                                                                   TCred.TotRec, 
                                                                   TCred.Previsto},
                                                                 TCred.PLA);
                    TotalRec += (double)TCred.TotRec;
                };
            };
            TotR.Text = TotalRec.ToString("n2");
            PublicaLinhaReceitas();
        }

        //private bool gerarLog;

        private void LevantamentoPrevisto()
        {
            // = 0;
            double[] colunas = new double[5];
            foreach (dImpBoletoBase.PrevistoRow rowPREV in dImpBoletoBase1.Previsto)
            {
                if (rowPREV.BOD_PLA != "120001") //multa
                {
                    string limpo = RemoveParenteses(rowPREV.BODMensagem);
                    colunas[(int)ColunasReceita.recPrevisto] = (double)rowPREV.ValorTot;
                    //    TotPrev += rowPREV.ValorTot;;
                    Registralinha(limpo, colunas, rowPREV.BOD_PLA);
                }
            };
            

            
        }



        private void QuadroSaldos()
        {
            if (balancete == null)
            {
                /*
                xrSaldoInicial.Visible = false;
                xrDataSaldoInicial.Visible = false;
                xrDataSaldoFinal.Visible = false;
                double[] SaldosA = new double[8];
                xrContasLogicas.Text = 
" Saldo Anterior de Conta Corrente\r\n"+
" Saldo Atual de Conta Corrente\r\n"+
" Fundo de Reserva\r\n"+
" Poupan�a 13�\r\n"+
" Aplica��o de Curto Prazo\r\n"+
" Poupan�a Obras\r\n"+
" Pupan�a S.Festas/Churras.\r\n"+
" Aplica��o Financeira\r\n";
                SaldosA[0] = SaldosAccessRow.Saldo_Anterior;// ((double)(dtSaldosH.Rows[0]["SALDO anterior"]));
                SaldosA[1] = SaldosAccessRow.Saldo_Fechamento;// ((double)(dtSaldosH.Rows[0]["saldo fechamento"]));
                SaldosA[2] = SaldosAccessRow.Saldo_Poupan�a_FR_Fechamento;// ((double)(dtSaldosH.Rows[0]["SALDO Poupan�a FR fechamento"]));
                SaldosA[3] = SaldosAccessRow.Saldo_Poupan�a_13_Fechamento;// ((double)(dtSaldosH.Rows[0]["SALDO Poupan�a 13 fechamento"]));
                SaldosA[4] = SaldosAccessRow.Saldo_Aplica��o_Autom�tica_Fechamento;// ((double)(dtSaldosH.Rows[0]["saldo aplica��o autom�tica fechamento"]));
                SaldosA[5] = SaldosAccessRow.Saldo_Poupan�a_Obras_Fechamento;// ((double)(dtSaldosH.Rows[0]["saldo Poupan�a obras fechamento"]));
                SaldosA[6] = SaldosAccessRow.Saldo_Poupan�a_Sal�o_Fechamento;// ((double)(dtSaldosH.Rows[0]["saldo Poupan�a sal�o fechamento"]));
                SaldosA[7] = SaldosAccessRow.Saldo_Aplica��o_Fechamento;// ((double)(dtSaldosH.Rows[0]["saldo aplica��o fechamento"]));


                xrSaldoFinal.Text =
                    SaldosA[0].ToString("#,##0.00") +
                    "\r\n" + SaldosA[1].ToString("#,##0.00") +
                    "\r\n" + SaldosA[2].ToString("#,##0.00") +
                    "\r\n" + SaldosA[3].ToString("#,##0.00") +
                    "\r\n" + SaldosA[4].ToString("#,##0.00") +
                    "\r\n" + SaldosA[5].ToString("#,##0.00") +
                    "\r\n" + SaldosA[6].ToString("#,##0.00") +
                    "\r\n" + SaldosA[7].ToString("#,##0.00");
                */
            }
            else
            {
                string Titulos= "";
                string ValoresI ="";
                string ValoresF = "";
                string ValoresC = "";
                string ValoresD = "";
                string ValoresT = "";
                string ValoresR = "";
                
                xrDataSaldoInicial.Text = string.Format("{0:dd/MM/yyyy}",balancete.DataI);
                xrDataSaldoFinal.Text = string.Format("{0:dd/MM/yyyy}",balancete.DataF);
                xrDebitos.Visible = xrDebitosT.Visible = xrCreditos.Visible = xrCreditosT.Visible = true;
                xrTransferencias.Visible = xrTransferenciaT.Visible = true;
                xrRentabilidade.Visible = xrRentabilidadeT.Visible = true;
                xrSaldoFinal.Visible = xrDataSaldoFinal.Visible = true;
                xrDataSaldoInicial.Visible = true;
                bool primeiro = true;
                decimal TotalI = 0;
                decimal TotalF = 0;
                decimal TotalC = 0;
                decimal TotalD = 0;
                decimal TotalT = 0;
                decimal TotalR = 0;
                foreach (ABS_balancete.ContaLogica ContaL in balancete.ContasLogicas.Values)
                {
                    Titulos += string.Format("{0}{1:n2}",primeiro?"":"\r\n",ContaL.CTLNome == "" ? " " : ContaL.CTLNome);
                    ValoresI += string.Format("{0}{1:n2}", primeiro ? "" : "\r\n", ContaL.SaldoI);                    
                    ValoresF += string.Format("{0}{1:n2}", primeiro ? "" : "\r\n", ContaL.SaldoF);
                    ValoresC += string.Format("{0}{1:n2}", primeiro ? "" : "\r\n", ContaL.Creditos);
                    ValoresD += string.Format("{0}{1:n2}", primeiro ? "" : "\r\n", ContaL.Debitos);
                    ValoresT += string.Format("{0}{1:n2}", primeiro ? "" : "\r\n", ContaL.Transferencias);
                    ValoresR += string.Format("{0}{1:n2}", primeiro ? "" : "\r\n", ContaL.Rentabilidade);
                    TotalI += ContaL.SaldoI;
                    TotalF += ContaL.SaldoF;
                    TotalC += ContaL.Creditos;
                    TotalD += ContaL.Debitos;
                    TotalT += ContaL.Transferencias;
                    TotalR += ContaL.Rentabilidade;
                    primeiro = false;
                };
                ValoresI += string.Format("{0}{1:n2}", "\r\n", TotalI);
                ValoresF += string.Format("{0}{1:n2}", "\r\n", TotalF);
                ValoresC += string.Format("{0}{1:n2}", "\r\n", TotalC);
                ValoresD += string.Format("{0}{1:n2}", "\r\n", TotalD);
                ValoresT += string.Format("{0}{1:n2}", "\r\n", TotalT);
                ValoresR += string.Format("{0}{1:n2}", "\r\n", TotalR);
                xrSaldoInicial.Text = ValoresI;
                xrSaldoFinal.Text = ValoresF;
                xrContasLogicas.Text = Titulos;
                xrCreditos.Text = ValoresC;
                xrDebitos.Text = ValoresD;
                xrTransferencias.Text = ValoresT;
                xrRentabilidade.Text = ValoresR;
            }

        }

        private string CONCodigo = "";

        

        //private bool EmTeste = false;//true;

        private void VerificaTransfereQuadroDesp(int iQuadro)
        {
            if (PainelDesp[iQuadro].Visible)            
            {
                if ((Acumulado + DescDesp[iQuadro].Lines.Length + LinhasCab) > MaximoReferencia)
                {
                    if (quadrostransferidos == 0)
                        QuadroContinua.Top = PainelDesp[iQuadro].Top;
                    PainelDesp[iQuadro].Parent = QuadroContinuacao;
                    PainelDesp[iQuadro].Top = 37 + DistanciaQuadros * quadrostransferidos;
                    quadrostransferidos++;
                }
                Acumulado += DescDesp[iQuadro].Lines.Length + LinhasCab;
            };
        }

        int LinhasCab = 4;
        int MaximoReferencia = 74;
        decimal TamanhoFonte = 5.25M;
        int Acumulado = 0;
        int DistanciaQuadros = 79;
        int quadrostransferidos = 0;

        enum tamfonte
        {
            T525 = 0,
            T500 = 1,
            T475 = 2,
            T450 = 3,
            T425 = 4,
            T400 = 5
        }

        private void AjustaFontes(tamfonte TF)
        {
            MaximoReferencia = LimitesImpresso[(int)TF];
            LinhasCab = LimitesImpressoCab[(int)TF];
            TamanhoFonte = TamanhosFonte[(int)TF];
            
            Font FontRed = new System.Drawing.Font(DespesaNome.Font.FontFamily, (float)TamanhoFonte);
            for (int i = 0; i < 6; i++)
            {
                DescDesp[i].Font = FontRed;
                ValorDesp[i].Font = FontRed;
            }
        }

        private void AjustaFontesRec(tamfonte TF)
        {            
            float TamanhoFonteRec = (float)TamanhosFonte[(int)TF];

            Font FontRed = new System.Drawing.Font(DespesaNome.Font.FontFamily, TamanhoFonteRec);
            ReceitaNome.Font = FontRed;
            ReceitaPrev.Font = FontRed; 
            ReceitaA.Font = FontRed;
            ReceitaMes.Font = FontRed;
            RecetaP.Font = FontRed;
            ReceitaT.Font = FontRed;
        }

        private int[] LimitesImpresso = new int[] { 74, 77, 81, 84, 89, 96 };
        private int[] LimitesImpressoCab = new int[] { 4, 4, 4, 4, 4, 5 };
        private decimal[] TamanhosFonte = new decimal[] { 5.25M, 5.00M, 4.75M, 4.50M, 4.25M, 4.00M };

        private void ParaQuadroP2() {            



            /*
            if (EmTeste)
            {
                VirInput.Input.Execute("Linhas corte", out MaximoReferencia, 72);
                VirInput.Input.Execute("Linhas cab", out LinhasCab,3);
                VirInput.Input.Execute("tamanho", ref TamanhoFonte);
                                
            }*/

            //Redu��o de fontes   

            tamfonte TF = tamfonte.T525;

            for (int i = 0; i < LimitesImpresso.Length - 1;)
            {
                if ((DespesaNome.Lines.Length + DespesaNome1.Lines.Length) < (LimitesImpresso[i] - 2 * LimitesImpressoCab[i]))
                    break;
                i++;
                TF = (tamfonte)(i);
            }       


            AjustaFontes(TF);         
            

            //passa o quadro mao-de-obra para a outra p�gina se o numero de linhas acumulado > 66

            Acumulado = 0;
            DistanciaQuadros = 79;
            quadrostransferidos = 0;
            if (PDespG.Visible)                
                Acumulado += DespesaNome.Lines.Length+LinhasCab;

            for (int i = 1; i < 6; i++)
                VerificaTransfereQuadroDesp(i);

            /*
            if (PdespMO.Visible)
            //    Acumulado -= 3;
            //else
            {
                if ((Acumulado + DespesaNome1.Lines.Length + LinhasCab) > LinhasDespesaNome1)
                {
                    if (quadrostransferidos == 0)
                        QuadroContinua.Top = PdespMO.Top;
                    PdespMO.Parent = QuadroContinuacao;
                    PdespMO.Top = 37 + DistanciaQuadros * quadrostransferidos;
                    quadrostransferidos++;
                }
                Acumulado += DespesaNome1.Lines.Length + LinhasCab;
            };




            //passa o quadro administrativa para a outra p�gina se o numero de linhas acumulado > 63
            if (PDespADMA.Visible)
            //    Acumulado -= 3;
            //else
            {
                if ((Acumulado + DespesaNome2.Lines.Length + LinhasCab) > LinhasDespesaNome1)
                {
                    if (quadrostransferidos == 0)
                        QuadroContinua.Top = PDespADMA.Top;
                    PDespADMA.Parent = QuadroContinuacao;
                    PDespADMA.Top = 37 + DistanciaQuadros * quadrostransferidos;
                    quadrostransferidos++;
                };
                Acumulado += DespesaNome2.Lines.Length + LinhasCab;
            };

            //passa o quadro administradora para a outra p�gina se o numero de linhas acumulado

            if (PAdministradora.Visible)
                //Acumulado -= 3;
            //else
            {

                if ((Acumulado + DespesaNomeADM.Lines.Length + LinhasCab) > LinhasDespesaNome1)
                {
                    if (quadrostransferidos == 0)
                        QuadroContinua.Top = PAdministradora.Top;
                    PAdministradora.Parent = QuadroContinuacao;
                    PAdministradora.Top = 37 + DistanciaQuadros * quadrostransferidos;
                    quadrostransferidos++;
                };
                Acumulado += DespesaNomeADM.Lines.Length + LinhasCab;
            }

            if (PDespB.Visible)
                //Acumulado -= 3;
            //else
            {




                if ((Acumulado + DespesaNomeBANC.Lines.Length + LinhasCab) > LinhasDespesaNome1)
                {
                    if (quadrostransferidos == 0)
                        QuadroContinua.Top = PDespB.Top;
                    PDespB.Parent = QuadroContinuacao;
                    PDespB.Top = 37 + DistanciaQuadros * quadrostransferidos;
                    quadrostransferidos++;
                };
                Acumulado += DespesaNomeBANC.Lines.Length + LinhasCab;
            };


            if (PDespH.Visible)
            {

                if ((Acumulado + DespesaNomeADV.Lines.Length + LinhasCab) > LinhasDespesaNome1)
                {
                    if (quadrostransferidos == 0)
                        QuadroContinua.Top = PDespH.Top;
                    PDespH.Parent = QuadroContinuacao;
                    PDespH.Top = 37 + DistanciaQuadros * quadrostransferidos;
                    quadrostransferidos++;
                };

            }*/



            if (quadrostransferidos > 0)
            {
                QuadroContinuacao.Visible = true;
                QuadroContinua.Visible = true;
                QuadroTotais.Parent = QuadroContinuacao;
                QuadroTotais.Top = 37 + DistanciaQuadros * quadrostransferidos - 10;

            }
            else
            {
                if (!Tranferido)
                {
                    QuadroUnidadeInadimplentes.Width += 903;
                    TituloUnidadesInad.Width = TextoUnidadesInadimplentes.Width = 1778;
                }
            }
        }

        private int? QuadroPendenciasUnidade_Top_Original = null;
        private int? QuadroConsumo_Top_Original = null;

        private void PosicaoQuadros(dImpBoletoBase.BOLETOSIMPRow row)
        {
            if (QuadroPendenciasUnidade_Top_Original.HasValue)
            {
                QuadroPendenciasUnidade.Top = QuadroPendenciasUnidade_Top_Original.Value;
                QuadroConsumo.Top = QuadroConsumo_Top_Original.Value;
            }
            else
            {
                QuadroPendenciasUnidade_Top_Original = QuadroPendenciasUnidade.Top;
                QuadroConsumo_Top_Original = QuadroConsumo.Top;
            }

            int DeltaP = 0;
            int DeltaH = 0;
                      
            //Verificando se existe mensagem no boleto se n�o esconde o quadro.

            if ((row.IsBOLMensagemImpNull()) || (row.BOLMensagemImp == ""))
            {
                QuadroMensagem.Visible = false;
                DeltaP = DeltaH = (QuadroPendenciasUnidade.Top - QuadroMensagem.Top);
            }
            else            
                QuadroMensagem.Visible = true;

            if (QuadroPendenciasUnidade.Visible == false)            
                DeltaH += (QuadroConsumo.Top - QuadroPendenciasUnidade.Top);                    
            
            QuadroPendenciasUnidade.Top -= DeltaP;
            QuadroConsumo.Top -= DeltaH;                                                              

        }

        /// <summary>
        /// Calula o conte�do da quadro unidades inadimplentes conforme o modelo no cadastro do condom�nio
        /// </summary>
        public void SetaquadroInad(string Titulo, string Conteudo) {
            if (Conteudo == "")
                QuadroUnidadeInadimplentes.Visible = false;
            else {
                QuadroUnidadeInadimplentes.Visible = true;
                //*** MRC - INICIO (01/04/2016 19:00) ***
                //TextoUnidadesInadimplentes.Text = Conteudo;
                nome_fonte = TextoUnidadesInadimplentes.Font.Name;
                tamanho_fonte = TextoUnidadesInadimplentes.Font.Size;
                TextoUnidadesInadimplentes.Rtf = Conteudo;
                //*** MRC - TERMINO (01/04/2016 19:00) ***
                TituloUnidadesInad.Text = Titulo;
            }
        }

        private bool inibirInadimplencia = false;

        private int CON;      

        /// <summary>
        /// 
        /// </summary>
        public ABS_balancete balancete = null;

        private Competencia compBalancete;
      
        /// <summary>
        /// Informa se o balancete cabe
        /// </summary>
        public bool Cabe = true;

        /// <summary>
        /// For�a o boleto a ser sem o balancete
        /// </summary>
        public bool OcultarBalancete = false;

        private dImpBoletoBase.CONDOMINIOSRow rowCON;

        //*** MRC - INICIO (15/03/2016 10:00) ***
        /// <summary>
        /// Carrega os dados do historico (balancete etc)
        /// </summary>
        /// <param name="comp"></param>
        /// <param name="_CON"></param>
        /// <param name="ForcarBalancete">For�a a impress�o de um balancete</param>
        /// <param name="TamanhoFonte">Seta um tamanho de fonte diferente para o quadro de inadimplentes</param>
        /// <returns>Largura que sobrou no quadro da inadimpl�ncia</returns>   
        //public int PreparaH(int _CON, Competencia comp, bool ForcarBalancete)
        public int PreparaH(int _CON, Competencia comp, bool ForcarBalancete, float? TamanhoFonte = null)        
        //*** MRC - TERMINO (15/03/2016 10:00) ***
        {
            int MaxCar = 0;
            if (CON != _CON)
            {
                CON = _CON;
                bool ComBalancete;
                ComBalancete = inibirInadimplencia = false;
                if (dImpBoletoBase1.CONDOMINIOSTableAdapter.Fill(dImpBoletoBase1.CONDOMINIOS, CON) != 0)
                {
                    rowCON = dImpBoletoBase1.CONDOMINIOS[0];
                    
                    if (!rowCON.IsCONImprimeBalanceteBoletoNull())
                        ComBalancete = rowCON.CONImprimeBalanceteBoleto;
                    
                    if (!rowCON.IsCONInibirPendeciasNull())
                        inibirInadimplencia = rowCON.CONInibirPendecias;
                    if (OcultarBalancete)
                        ComBalancete = false;
                    
                    OcultaPrevRea.Value = rowCON.CONOcultaPxR;
                    CONCodigo = rowCON.CONCodigo;
                    if (ComBalancete && !rowCON.IsCONBalBoletoNull() && !rowCON.IsCONCompFecNull())
                    {
                    
                        if (balancete == null)
                        {
                            compBalancete = comp.CloneCompet(rowCON.CONBalBoleto);
                    
                        }
                        else
                            compBalancete = (Competencia)balancete.ABS_comp;
                    

                        if (!ForcarBalancete && (compBalancete > new Competencia(rowCON.CONCompFec)))
                            ComBalancete = false;
                        else
                        {
                    
                            if (balancete == null)
                            {
                    
                                balancete = ABS_balancete.GetBalancete(CON, compBalancete,TipoBalancete.Boleto);
                                if (balancete != null)
                                {
                                    balancete.CarregarDados(true);
                                    int? MBA = VirMSSQL.TableAdapter.STTableAdapter.BuscaEscalar_int("select CONBoleto_MBA from CONDOMINIOS where CON = @P1", CON);
                                    if (MBA.HasValue)
                                    {
                                        Framework.objetosNeon.AgrupadorBalancete.solicitaAgrupamento AgrupamentoUsar = new Framework.objetosNeon.AgrupadorBalancete.solicitaAgrupamento(Framework.objetosNeon.AgrupadorBalancete.TipoClassificacaoDesp.CadastroMBA, MBA.Value);
                                        balancete.CarregarDadosBalancete(AgrupamentoUsar, false);
                                    }
                                    else
                                        balancete.CarregarDadosBalancete();
                                }
                            }
                        }
                    }
                }
                if (ComBalancete)
                {
                    if (balancete == null)
                    {
                        /*
                        //antigo - eliminar
                        if (dImpBoletoBase1.SaldosAccessTableAdapter.Fill(dImpBoletoBase1.SaldosAccess, CONCodigo) == 1)
                        {
                            SaldosAccessRow = dImpBoletoBase1.SaldosAccess[0];
                            dImpBoletoBase1.ReceitasAccessTableAdapter.Fill(dImpBoletoBase1.ReceitasAccess, CONCodigo, SaldosAccessRow.Data_Inicial, SaldosAccessRow.Data_Final);
                            dImpBoletoBase1.PrevistoTableAdapter.Fill(dImpBoletoBase1.Previsto, SaldosAccessRow.Data_Inicial, SaldosAccessRow.Data_Final, CON);
                            QuadoReceitas();
                            QuadroSaldos();
                            Despesas();
                            OcultaQuadrosDesp();
                            //StatusSeguro = StatusSeguroTESTE++;                                
                            //AjustaQuadroSeguro();
                            ParaQuadroP2();
                        }
                        */
                    }
                    else
                    {
                        //dImpBoletoBase1.PrevistoTableAdapter.Fill(dImpBoletoBase1.Previsto, SaldosAccessRow.Data_Inicial, SaldosAccessRow.Data_Final, CON);
                        QuadoReceitas();
                        QuadroSaldos();
                        Despesas();
                        OcultaQuadrosDesp();
                        ParaQuadroP2();
                    }
                }
                else
                {
                    //QuadroReceitas. = QuadroSaldo.Visible = QuadroDespesas.Visible = false;
                    QuadroReceitas.Visible = QuadroSaldo.Visible = QuadroDespesas.Visible = false;
                    int delta = QuadroMensagem.Top - QuadroReceitas.Top;
                    QuadroEsquerda.LeftF += QuadroDespesas.WidthF / 2;
                    QuadroMensagem.Top -= delta;
                    QuadroPendenciasUnidade.Top -= delta;
                    QuadroConsumo.Top -= delta;
                    //QuadroConsumo.Parent = QuadroUtil;
                    //QuadroConsumo.Top = QuadroDespesas.Top;
                    //QuadroConsumo.Left = QuadroDespesas.Left;
                };

                //*** MRC - INICIO (15/03/2016 10:00) ***
                if (TamanhoFonte != null)
                    TextoUnidadesInadimplentes.Font = new System.Drawing.Font(TextoUnidadesInadimplentes.Font.Name, (float)TamanhoFonte, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));

                if (TextoUnidadesInadimplentes.Font.Size == 8)
                    MaxCar = (int)Math.Truncate(TextoUnidadesInadimplentes.Width / 17.0);
                else
                //*** MRC - TERMINO (15/03/2016 10:00) ***
                    if (TextoUnidadesInadimplentes.Font.Size == 7)
                        MaxCar = (int)Math.Truncate(TextoUnidadesInadimplentes.Width / 14.6);
                    else
                        MaxCar = (int)Math.Truncate(TextoUnidadesInadimplentes.Width / 13.0);

            }
            return MaxCar;
        }

        

        decimal m3gas;
        decimal Valgas;
        decimal leituragas;
        decimal m3agua;
        decimal Valagua;
        decimal leituraagua;
        decimal m3Luz;
        decimal ValLuz;
        //private decimal maxGas;
        //private decimal maxAgua;
        //private decimal maxLuz;
        decimal PrecoGas = 0;

        private bool TemAgua = false;
        private bool TemGas = false;
        private bool TemLuz = false;
        private List<decimal> porAguaC;
        private List<decimal> porGasC;
        private List<decimal> porLuzC;
        private List<decimal> porAguaV;
        private List<decimal> porGasV;
        private List<decimal> porLuzV;

        private void GravaGasAgua(Framework.objetosNeon.Competencia Comp, bool CompDoBoleto)
        {                                    
            if (ConsMes.Text != "")
            {
                ConsMes.Text += "\r\n";
                ConsLeitGas.Text += "\r\n";
                Consm3Gas.Text += "\r\n";
                ConsKgGas.Text += "\r\n";
                ConsValGas.Text += "\r\n";
                ConsLeitAgua.Text += "\r\n";
                Consm3Agua.Text += "\r\n";
                ConsValAgua.Text += "\r\n";
                Consm3Luz.Text += "\r\n";
                ConsValLuz.Text += "\r\n";
            }
            else
            {
                porAguaC = new List<decimal>();
                porGasC = new List<decimal>();
                porLuzC = new List<decimal>();
                porAguaV = new List<decimal>();
                porGasV = new List<decimal>();
                porLuzV = new List<decimal>();
            }
            if (CompDoBoleto)
                ConsMes.Text += "*";
            ConsMes.Text += Comp.ToString();

            if ((leituragas != 0) || (Valgas != 0))
            {
                TemGas = true;
                ConsLeitGas.Text += leituragas.ToString("n2");
                Consm3Gas.Text += m3gas.ToString("n2");
                ConsKgGas.Text += (m3gas * 2.3M).ToString("n1");
                ConsValGas.Text += Valgas.ToString("n2");
                porGasC.Add(m3gas);
                porGasV.Add(Valgas);
            }
            else
            {
                ConsLeitGas.Text += " - ";
                Consm3Gas.Text += " - ";
                ConsKgGas.Text += " - ";
                ConsValGas.Text += " - ";
                porGasC.Add(0);
                porGasV.Add(0);
            }

            if ((leituraagua != 0) || (Valagua != 0))
            {
                TemAgua = true;
                ConsLeitAgua.Text += leituraagua.ToString("n3");
                Consm3Agua.Text += m3agua.ToString("n2");
                ConsValAgua.Text += Valagua.ToString("n2");
                porAguaC.Add(m3agua);
                porAguaV.Add(Valagua);
            }
            else
            {
                ConsLeitAgua.Text += " - ";
                Consm3Agua.Text += " - ";
                ConsValAgua.Text += " - ";
                porAguaC.Add(0);
                porAguaV.Add(0);
            };

            if ((m3Luz != 0) || (ValLuz != 0))
            {
                TemLuz = true;                
                Consm3Luz.Text += m3Luz.ToString("n0");
                ConsValLuz.Text += ValLuz.ToString("n2");
                porLuzC.Add(m3Luz);
                porLuzV.Add(ValLuz);
            }
            else
            {                
                Consm3Luz.Text += " - ";
                ConsValLuz.Text += " - ";
                porLuzC.Add(0);
                porLuzV.Add(0);
            };

            m3gas = Valgas = leituragas = m3agua = Valagua = leituraagua = m3Luz = ValLuz = 0;
            Comp = null;
        }

        private enum TipoConsumo 
        {
            Gas = 1,
            Agua = 2,
            Luz = 3
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="CON"></param>
        /// <param name="CompBoleto"></param>
        public void PrecargaConsumo(int CON, Competencia CompBoleto)
        {
            if ((CompMaximaCarregada == null) || (CON != CONCarregado) || (CompMaximaCarregada != CompBoleto))
            {
                CONCarregado = CON;                
                CompMaximaCarregada = CompBoleto.CloneCompet();
                Competencia CompMinima = CompBoleto.CloneCompet(-3);
                dImpBoletoBase1.ConsumoTableAdapter.FillByCON(dImpBoletoBase1.Consumo, CompMinima.CompetenciaBind, CompMaximaCarregada.CompetenciaBind, CON);
            }
        }

        private int CONCarregado = 0;        
        private Competencia CompMaximaCarregada = null;

        private void AjustaPorq(XRLabel L, List<decimal> C, List<decimal> V)
        {
            decimal maximo = 0;
            L.Text = "";
            foreach (decimal Valor in C)
                maximo = Math.Max(maximo, Valor);
            if (maximo != 0)
                foreach (decimal Valor in C)
                    L.Text = string.Format("{0}{1}{2}", L.Text,L.Text == "" ? "":Environment.NewLine, (int)(100 * Valor / maximo));
            else
            {
                foreach (decimal Valor in V)
                    maximo = Math.Max(maximo, Valor);
                if (maximo != 0)
                    foreach (decimal Valor in V)
                        L.Text = string.Format("{0}{1}{2}", L.Text, L.Text == "" ? "" : Environment.NewLine, (int)(100 * Valor / maximo));
                else
                    foreach (decimal Valor in V)
                        L.Text = string.Format("{0}{1}0", L.Text, L.Text == "" ? "" : Environment.NewLine);
            }
        }

        private void QuadroConsumoCalc(dImpBoletoBase.BOLETOSIMPRow row)
        {            
            Competencia CompBoleto = new Competencia(row.CompMes, row.CompAno);
            Competencia CompMinima = CompBoleto.CloneCompet(-3);
            if ((CompMaximaCarregada != null) && (row.CON == CONCarregado) && (CompMaximaCarregada == CompBoleto))            
                dImpBoletoBase1.Consumo.DefaultView.RowFilter = string.Format("GAS_APT = {0}",row.APT);            
            else
            {
                CompMaximaCarregada = null;
                dImpBoletoBase1.ConsumoTableAdapter.Fill(dImpBoletoBase1.Consumo, row.APT, CompMinima.CompetenciaBind, CompBoleto.CompetenciaBind);
                dImpBoletoBase1.Consumo.DefaultView.RowFilter = "";
            }
            ConsMes.Text = ConsLeitGas.Text = Consm3Gas.Text = ConsKgGas.Text = ConsValGas.Text = ConsValLuz.Text = Consm3Luz.Text = "";
            ConsLeitAgua.Text = Consm3Agua.Text = ConsValAgua.Text = "";            
            Competencia CompAnterior = null;            
            m3gas = Valgas = leituragas = m3agua = Valagua = leituraagua = m3Luz = ValLuz = 0;
            TemAgua = TemGas = TemLuz = false;
            if (dImpBoletoBase1.Consumo.DefaultView.Count > 0)
            {
                RestauraPos();
                foreach (System.Data.DataRowView DRV in dImpBoletoBase1.Consumo.DefaultView)
                {
                    dImpBoletoBase.ConsumoRow rowGAS = (dImpBoletoBase.ConsumoRow)DRV.Row;
                    switch ((TipoConsumo)rowGAS.GASTipo)
                    {
                        case TipoConsumo.Gas:
                            if ((rowCON != null) && (rowCON.CONBoletoOcGas))
                                continue;
                            break;
                        case TipoConsumo.Agua:
                            if ((rowCON != null) && (rowCON.CONBoletoOcAgua))
                                continue;
                            break;
                        case TipoConsumo.Luz:
                            if ((rowCON != null) && (rowCON.CONBoletoOcLuz))
                                continue;
                            break;
                        default:
                            break;
                    }
                    Competencia NovaComp = new Competencia(rowGAS.GASCompetencia);
                    if ((CompAnterior != null) && (NovaComp != CompAnterior))
                        GravaGasAgua(CompAnterior, CompAnterior == CompBoleto);
                    CompAnterior = NovaComp;
                    switch ((TipoConsumo)rowGAS.GASTipo)
                    {
                        case TipoConsumo.Gas:
                            m3gas = rowGAS.GASM3;
                            Valgas = (rowGAS.IsBODValorNull() ? 0 : rowGAS.BODValor);
                            leituragas = rowGAS.GASLeitura;
                            //maxGas = Math.Max(m3gas, maxGas);
                            break;
                        case TipoConsumo.Agua:
                            m3agua = rowGAS.GASM3;
                            Valagua = (rowGAS.IsBODValorNull() ? 0 : rowGAS.BODValor);
                            leituraagua = rowGAS.GASLeitura;
                            //maxAgua = Math.Max(m3agua, maxAgua);
                            break;
                        case TipoConsumo.Luz:
                            m3Luz = rowGAS.GASM3;
                            ValLuz = (rowGAS.IsBODValorNull() ? 0 : rowGAS.BODValor);
                            break;
                        default:
                            break;
                    }
                };
                if ((TemAgua || TemGas || TemLuz))
                {
                    if (CompAnterior != null)
                        GravaGasAgua(CompAnterior, CompAnterior == CompBoleto);
                    AjustaPorq(ConsPorcGas, porGasC, porGasV);
                    AjustaPorq(ConsPorcAgua, porAguaC, porAguaV);
                    AjustaPorq(ConsPorcLuz, porLuzC, porLuzV);
                }
            }
            if ((ConsMes.Text != "") && (TemAgua || TemGas || TemLuz))
            {
                QuadroConsumo.Visible = true;
                if (TemGas && (PrecoGas == 0))                
                {
                    if (VirMSSQL.TableAdapter.STTableAdapter.BuscaEscalar("SELECT CONValorGas FROM CONDOMINIOS WHERE CON = @P1", out PrecoGas, row.CON) && (PrecoGas != 0))
                            TituloGas.Text = string.Format("G�s ({0:n2} R$/Kg)", PrecoGas);
                };
                float delta = 0;
                foreach (XRControl X in GrupoGas)
                    X.Visible = TemGas;
                if (!TemGas)
                    delta = TituloGas.WidthF;
                if (delta > 0 && TemAgua)
                    foreach (XRControl X in GrupoAgua)
                        RecuaXRControl(X,delta);
                if (!TemAgua)
                    delta += ConsAguaT.WidthF;
                foreach (XRControl X in GrupoAgua)
                    X.Visible = TemAgua;
                if (delta > 0 && TemLuz)
                    foreach (XRControl X in GrupoLuz)
                        RecuaXRControl(X, delta);
                foreach (XRControl X in GrupoLuz)
                    X.Visible = TemLuz;
            }
            else
                QuadroConsumo.Visible = false;

        }

        private SortedList<string, float> PosOriginal;

        private void RecuaXRControl(XRControl Controle,float delta)
        {
            if (PosOriginal == null)
                PosOriginal = new SortedList<string, float>();
            if (!PosOriginal.ContainsKey(Controle.Name))
                PosOriginal.Add(Controle.Name, Controle.LeftF);
            Controle.LeftF = PosOriginal[Controle.Name] - delta;
        }

        private void RestauraPos()
        {
            if (PosOriginal == null)
                return;
            foreach (XRControl X in GrupoLuz)
                if (PosOriginal.ContainsKey(X.Name))
                    X.LeftF = PosOriginal[X.Name];
            foreach (XRControl Y in GrupoAgua)
                if (PosOriginal.ContainsKey(Y.Name))
                    Y.LeftF = PosOriginal[Y.Name];
        }

        #region Grupos
        private List<XRControl> _GrupoGas;

        private List<XRControl> GrupoGas
        {
            get
            {
                if (_GrupoGas == null)
                    _GrupoGas = new List<XRControl>(new XRControl[] { ConsLeitGas, Consm3Gas, ConsKgGas, ConsValGas, ConsPorcGas, TituloGas, ConsLeitGasT, Consm3GasT, ConsKgGasT, ConsValGasT });
                return _GrupoGas;
            }
        }

        private List<XRControl> _GrupoAgua;

        private List<XRControl> GrupoAgua
        {
            get
            {
                if (_GrupoAgua == null)
                    _GrupoAgua = new List<XRControl>(new XRControl[] { ConsLeitAgua, Consm3Agua, ConsValAgua, ConsPorcAgua, ConsAguaT, ConsLeitAguaT, Consm3AguaT, ConsValAguaT });
                return _GrupoAgua;
            }
        }

        private List<XRControl> _GrupoLuz;

        private List<XRControl> GrupoLuz
        {
            get
            {
                if (_GrupoLuz == null)
                    _GrupoLuz = new List<XRControl>(new XRControl[] { Consm3Luz, ConsValLuz, ConsPorcLuz, ConsLuzT, Consm3LuzT, ConsValLuzT });
                return _GrupoLuz;
            }
        } 
        #endregion
         
        private void QuadroPendenciasUnidadeCalc(dImpBoletoBase.BOLETOSIMPRow row)
        {
           
            if (inibirInadimplencia)
            {
                
                QuadroPendenciasUnidade.Visible = false;
                return;
            }

            if (row.APTStatusCobranca == (int)APTStatusCobranca.juridico)
            {
                labRespBA.Text = "******";
                labValorBA.Text = "Juridico ";
                labVenctoBA.Text = "******";
                labCategoriaBA.Text = "******";
                QuadroPendenciasUnidade.Visible = true;
            }
            else
            {
                //int Lidos = dImpBoletoBase1.BoletosAbertoAccessTableAdapter.Fill(dImpBoletoBase1.BoletosAbertoAccess, CONCodigo, row.BLOCodigo, row.APTNumero, DateTime.Today);
                int Lidos = dImpBoletoBase1.BOLetosAbertosTableAdapter.Fill(dImpBoletoBase1.BOLetosAbertos, row.APT, DateTime.Today.AddDays(-3));

                labRespBA.Text = "";
                labValorBA.Text = "";
                labVenctoBA.Text = "";
                labCategoriaBA.Text = "";

                bool mostrar = false;

                //foreach (dImpBoletoBase.BoletosAbertoAccessRow BoletosAbertoAccessRow in dImpBoletoBase1.BoletosAbertoAccess)
                foreach (dImpBoletoBase.BOLetosAbertosRow BOLetosAbertosRow in dImpBoletoBase1.BOLetosAbertos)
                {
                    /*
                    DateTime Vencimento = BoletosAbertoAccessRow.Data_Vencimento;
                    if ((Vencimento < DateTime.Today.AddDays(-3))
                        &&
                        ((row.Calc_PI == "P") || (BoletosAbertoAccessRow.Destinat�rio.ToUpper() == "I"))
                        )
                        mostrar = true;*/
                    DateTime Vencimento = BOLetosAbertosRow.BOLVencto;
                    if 
                        ((row.Calc_PI == "P") || (!BOLetosAbertosRow.BOLProprietario))
                        
                    {
                        mostrar = true;
                        break;
                    }

                }




                if (mostrar && (Lidos > 10))
                {
                    mostrar = false;
                };

                if (mostrar)
                {


                    QuadroPendenciasUnidade.Visible = true;

                    foreach (dImpBoletoBase.BOLetosAbertosRow BOLetosAbertosRow in dImpBoletoBase1.BOLetosAbertos)
                    //foreach (dImpBoletoBase.BoletosAbertoAccessRow _DTR in dImpBoletoBase1.BoletosAbertoAccess)
                    {
                        if ((row.Calc_PI == "I")
                            &&
                            BOLetosAbertosRow.BOLProprietario)
                            continue;

                        if (BOLetosAbertosRow.BOLProprietario)
                            labRespBA.Text += "PROPRIET�RIO\r\n";
                        else
                            labRespBA.Text += "INQUILINO\r\n";

                        labValorBA.Text += string.Format("{0:n2}\r\n", BOLetosAbertosRow.BOLValorPrevisto); //Convert.ToDecimal(_DTR["Valor Previsto"]).ToString("n2") + "\r\n";

                        labVenctoBA.Text += string.Format("{0:dd/MM/yyyy}\r\n", BOLetosAbertosRow.BOLVencto);// Convert.ToDateTime(_DTR["Data Vencimento"]).ToString("dd/MM/yyyy") + "\r\n";

                        switch (BOLetosAbertosRow.BOLTipoCRAI)
                        {
                            case "C":
                                labCategoriaBA.Text += "CONDOM�NIO" + "\r\n";
                                break;
                            case "R":
                                labCategoriaBA.Text += "RATEIO" + "\r\n";
                                break;                            
                            case "A":
                                labCategoriaBA.Text += "ACORDO" + "\r\n";
                                break;
                            case "I":
                            default:
                                labCategoriaBA.Text += "   " + "\r\n";
                                break;                            
                        };

                        /*
                        if (_DTR["Condominio"].ToString().ToUpper() == "C")
                            labCategoriaBA.Text += "CONDOM�NIO" + "\r\n";
                        else
                            if (_DTR["Condominio"].ToString().ToUpper() == "R")
                                labCategoriaBA.Text += "RATEIO" + "\r\n";
                            else
                                if (_DTR["Condominio"].ToString().ToUpper() == "I")
                                    labCategoriaBA.Text += "   " + "\r\n";
                                else
                                    if (_DTR["Condominio"].ToString().ToUpper() == "A")
                                        labCategoriaBA.Text += "ACORDO" + "\r\n";
                                    else
                                        labCategoriaBA.Text += _DTR["Condominio"].ToString().ToUpper() + "\r\n";*/
                    }
                }
                else
                {
                    QuadroPendenciasUnidade.Visible = false;
                }
            };

        }

        private void ImpBoletoBal_DataSourceRowChanged(object sender, DataSourceRowEventArgs e)
        {
            System.Data.DataRowView DRV = (System.Data.DataRowView)bindingSourcePrincipal[e.CurrentRow];
            dImpBoletoBase.BOLETOSIMPRow row = (dImpBoletoBase.BOLETOSIMPRow)DRV.Row;            
            if (!row.IsAPTNull())
            {
                QuadroPendenciasUnidadeCalc(row);
                QuadroConsumoCalc(row);
                //AjustaQuadroSeguro(row);
            }            
            PosicaoQuadros(row);
        }

        private void ConsPorcAgua_Draw(object sender, DrawEventArgs e)
        {
            DrawGrafBarra(e);
        }

        private void ConsPorcGas_Draw(object sender, DrawEventArgs e)
        {
            DrawGrafBarra(e);
        }

        private void ConsPorcLuz_Draw(object sender, DrawEventArgs e)
        {
            DrawGrafBarra(e);
        }

        private char[] separadorQuebra = new char[2] { '\r', '\n' };

        private void DrawGrafBarra(DrawEventArgs e)
        {            
            Rectangle rect = Rectangle.Truncate(e.Bounds);
            if (e.UniGraphics is GdiGraphics)
            {

                GdiGraphics graph = (GdiGraphics)e.UniGraphics;
                graph.Graphics.FillRectangle(Brushes.White, rect);
                if ((e.Brick != null) && (e.Brick.Text != null))
                {
                    string[] linhas = e.Brick.Text.Split(separadorQuebra, StringSplitOptions.RemoveEmptyEntries);
                    if (linhas.Length == 0)
                        return;
                    int larg = (int)(rect.Height / linhas.Length);
                    for (int i = 0; i < linhas.Length; i++)
                    {
                        decimal Valor;
                        decimal.TryParse(linhas[i], out Valor);
                        //int w = (i + 1) * (int)(Valor * rect.Width / Maximo);
                        int w = (int)(Valor * rect.Width / 100);
                        Rectangle recti = new Rectangle(rect.X, rect.Y + i * larg + 4, w, larg - 13);
                        graph.Graphics.FillRectangle(Brushes.Silver, recti);
                    }
                }
            }
            else
            {
                //DevExpress.XtraPrinting.Export.Pdf.PdfGraphics graph = (DevExpress.XtraPrinting.Export.Pdf.PdfGraphics)e.UniGraphics;
                e.UniGraphics.FillRectangle(Brushes.White, rect);

                if ((e.Brick != null) && (e.Brick.Text != null))
                {
                    string[] linhas = e.Brick.Text.Split(separadorQuebra, StringSplitOptions.RemoveEmptyEntries);
                    if (linhas.Length == 0)
                        return;
                    int larg = (int)(rect.Height / linhas.Length);
                    for (int i = 0; i < linhas.Length; i++)
                    {
                        decimal Valor;
                        decimal.TryParse(linhas[i], out Valor);
                        //int w = (i + 1) * (int)(Valor * rect.Width / Maximo);
                        int w = (int)(Valor * rect.Width / 100);
                        Rectangle recti = new Rectangle(rect.X, rect.Y + i * larg + 4, w, larg - 13);
                        e.UniGraphics.FillRectangle(Brushes.Silver, recti);
                    }
                }
            }
        }
    }
}

