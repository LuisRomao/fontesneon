﻿using DevExpress.XtraReports.UI;
using DevExpress.XtraRichEdit;
using DevExpress.XtraRichEdit.API.Native;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Drawing;
using System.IO;

namespace dllImpresso
{
    /// <summary>
    /// Impresso base geral
    /// </summary>
    public partial class ImpBase : XtraReport
    {

        private DataTable Dados;
        private dImpressos dImp;
        private int IMS;
        private dImpressos.IMpreSsosRow LinhaMae;
        private RichEditControl richEditControl1;

        /// <summary>
        /// Rtf para importação de relatórios pré montados
        /// </summary>
        protected XRRichText QuadroRTFBase;

        /// <summary>
        /// Tabelas criadas na pré-carga
        /// </summary>
        public SortedList<string, Table> TabelasCriadas;

        /// <summary>
        /// Construtor
        /// </summary>
        public ImpBase()
        {
            InitializeComponent();
        }

        private bool CarregaDados(int IMS)
        {
            if (dImp == null)
            {
                dImp = new dImpressos();
                if (dImp.IMpreSsosTableAdapter.Fill(dImp.IMpreSsos, IMS) == 0)
                    return false;
                LinhaMae = dImp.IMpreSsos[0];
                dImp.IMpressosCamposTableAdapter.Fill(dImp.IMpressosCampos, IMS);
            }
            return true;
        }

        private int CarregaDados(string IMSNome)
        {
            if (dImp == null)
            {
                dImp = new dImpressos();
                if (dImp.IMpreSsosTableAdapter.FillBy(dImp.IMpreSsos, IMSNome) == 0)
                {
                    LinhaMae = dImp.IMpreSsos.NewIMpreSsosRow();
                    LinhaMae.IMSNome = IMSNome;
                    dImp.IMpreSsos.AddIMpreSsosRow(LinhaMae);
                    dImp.IMpreSsosTableAdapter.Update(LinhaMae);
                }
                else
                    LinhaMae = dImp.IMpreSsos[0];
                dImp.IMpressosCamposTableAdapter.Fill(dImp.IMpressosCampos, LinhaMae.IMS);
            }
            return LinhaMae.IMS;
        }
        
        private Table CarregaTabela(RichEditControl richEditControl1, string NomeTabela, DataTable Tabela, bool FormatacaoPadrao = true)
        {
            Document doc = richEditControl1.Document;
            doc.Unit = DevExpress.Office.DocumentUnit.Millimeter;
            Table NovaTabela = doc.Tables.Create(PosicaoTabela(doc, NomeTabela), Tabela.Rows.Count, Tabela.Columns.Count);
            NovaTabela.BeginUpdate();
            if (FormatacaoPadrao)
            {
                SetNegrito(NovaTabela,true,new List<int> { 0 });
                NovaTabela.TableAlignment = TableRowAlignment.Center;
                NovaTabela.SetPreferredWidth(4500, WidthType.FiftiethsOfPercent);
                SetCorFundo(NovaTabela, Color.WhiteSmoke, Color.Gainsboro, Color.Gainsboro);
                SetAlinhamentoCel(NovaTabela, ParagraphAlignment.Center);
            }

            NovaTabela.ForEachCell((cell, rowIndex, cellIndex) =>
            {
                doc.InsertText(cell.ContentRange.Start, (string)Tabela.Rows[rowIndex][cellIndex]);
            });
            NovaTabela.EndUpdate();
            return NovaTabela;
        }

        #region Ferramentas internas
        private void DataRow_para_Dados(DataRow DadosRow)
        {
            Dados = new DataTable();
            foreach (DataColumn DC in DadosRow.Table.Columns)
                Dados.Columns.Add(new DataColumn(DC.ColumnName));
            DataRow DR = Dados.NewRow();
            foreach (DataColumn DC in DadosRow.Table.Columns)
                DR[DC.ColumnName] = DadosRow[DC];
            Dados.Rows.Add(DR);
        }

        private DocumentPosition PosicaoTabela(Document doc, string NomeTabela)
        {
            for (int i = 0; i < doc.Fields.Count; i++)
            {
                string NomeCampo = doc.GetText(doc.Fields[i].Range);

                if (NomeCampo == string.Format("<<{0}>>", NomeTabela))
                    return doc.Fields[i].Range.Start;
            }
            return doc.CreatePosition(0);
        }

        private void SortedList_para_Dados(SortedList DadosList)
        {
            Dados = new DataTable();
            foreach (DictionaryEntry DE in DadosList)
                Dados.Columns.Add((string)DE.Key);
            DataRow DR = Dados.NewRow();
            foreach (DictionaryEntry DE in DadosList)
                DR[(string)DE.Key] = DE.Value;
            Dados.Rows.Add(DR);
        }

        private void VerificaCadastraCampo(string NomeCampo)
        {
            foreach (dImpressos.IMpressosCamposRow rowIMC in LinhaMae.GetIMpressosCamposRows())
                if (rowIMC.ISCDescritivo == NomeCampo)
                    return;
            dImp.IMpressosCampos.AddIMpressosCamposRow(LinhaMae, NomeCampo);
            dImp.IMpressosCamposTableAdapter.Update(dImp.IMpressosCampos);
        } 
        #endregion

        private Document doc { get => richEditControl1.Document; }

        #region Formatação de tabelas

        private void SetAlinhamentoCel(Table Tabela, ParagraphAlignment AlinhamentoTabela, ParagraphAlignment? AlinhamentoCabecalho = null, ParagraphAlignment? AlinhamentoRodape = null, List<int> Linhas = null, List<int> Colunas = null)
        {
            for (int i = 0; i < Tabela.Rows.Count; i++)
            {
                if ((Linhas != null) && (!Linhas.Contains(i)))
                    continue;
                ParagraphAlignment Alinhamento = AlinhamentoTabela;
                if (i == 0)
                    Alinhamento = AlinhamentoCabecalho.GetValueOrDefault(AlinhamentoTabela);
                else if (i == Tabela.Rows.Count - 1)
                    Alinhamento = AlinhamentoRodape.GetValueOrDefault(AlinhamentoTabela);                
                foreach (TableCell Celula in Tabela.Rows[i].Cells)
                {
                    if ((Colunas != null) && (!Colunas.Contains(Celula.Index)))
                        continue;
                    ParagraphProperties props = doc.BeginUpdateParagraphs(Celula.Range);
                    props.Alignment = Alinhamento;
                }
            }
        }

        private void SetFonteCel(Table Tabela, Font FonteTabela, Font FonteCabecalho = null, Font FonteRodape = null, List<int> Linhas = null, List<int> Colunas = null)
        {
            for (int i = 0; i < Tabela.Rows.Count; i++)
            {
                if ((Linhas != null) && (!Linhas.Contains(i)))
                    continue;
                Font Fonte = FonteTabela;
                if (i == 0)
                    Fonte = FonteCabecalho ?? FonteTabela;
                else if (i == Tabela.Rows.Count - 1)
                    Fonte = FonteRodape ?? FonteTabela;
                foreach (TableCell Celula in Tabela.Rows[i].Cells)
                {
                    if ((Colunas != null) && (!Colunas.Contains(Celula.Index)))
                        continue;
                    CharacterProperties cpCell = doc.BeginUpdateCharacters(Celula.Range);
                    cpCell.FontName = Fonte.Name;
                    cpCell.FontSize = Font.Size;                    
                    doc.EndUpdateCharacters(cpCell);
                }
            }
        }        

        private void SetLargurasColunas_mm(Table Tabela, params int[] Larguras)
        {
            Tabela.SetPreferredWidth(1000, WidthType.Auto);
            int indice = 0;
            foreach (int Largura in Larguras)
            {
                Tabela[0, indice].PreferredWidthType = WidthType.Fixed;
                Tabela[0, indice].PreferredWidth = Largura;
                indice++;
            }
        }

        private void SetCorFundo(Table Tabela, Color CorTabela, Color? CorCabecalho = null, Color? CorRodape = null, List<int> Linhas = null, List<int> Colunas = null)
        {
            for (int i = 0; i < Tabela.Rows.Count; i++)
            {
                if ((Linhas != null) && (!Linhas.Contains(i)))
                    continue;
                Color Cor = CorTabela;
                if (i == 0)
                    Cor = CorCabecalho.GetValueOrDefault(CorTabela);
                else if (i == Tabela.Rows.Count - 1)
                    Cor = CorRodape.GetValueOrDefault(CorTabela);
                foreach (TableCell Celula in Tabela.Rows[i].Cells)
                {
                    if ((Colunas != null) && (!Colunas.Contains(Celula.Index)))
                        continue;
                    Celula.BackgroundColor = Cor;
                }
            }
        }

        private void SetNegrito(Table Tabela, bool Negrito = true, List<int> Linhas = null, List<int> Colunas = null)
        {
            for (int i = 0; i < Tabela.Rows.Count; i++)
            {
                if ((Linhas != null) && (!Linhas.Contains(i)))
                    continue;                
                foreach (TableCell Celula in Tabela.Rows[i].Cells)
                {
                    if ((Colunas != null) && (!Colunas.Contains(Celula.Index)))
                        continue;
                    CharacterProperties cp = doc.BeginUpdateCharacters(Celula.Range);
                    cp.Bold = Negrito;
                    doc.EndUpdateCharacters(cp);
                }
            }

            //CharacterProperties cp = doc.BeginUpdateCharacters(NovaTabela.Rows[0].Range);
            //cp.Bold = true;
            //doc.EndUpdateCharacters(cp);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="NomeTabela"></param>
        /// <param name="Linha"></param>
        /// <param name="Coluna"></param>
        /// <param name="Negrito"></param>
        public void VirSetNegrito(string NomeTabela, int Linha, int Coluna, bool Negrito = true)
        {
            VirSetNegrito(NomeTabela, Negrito, new List<int> { Linha }, new List<int> { Coluna });
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="NomeTabela"></param>
        /// <param name="Negrito"></param>
        /// <param name="Linhas"></param>
        /// <param name="Colunas"></param>
        public void VirSetNegrito(string NomeTabela, bool Negrito = true, List<int> Linhas = null, List<int> Colunas = null)
        {
            Table Tabela = TabelasCriadas[NomeTabela];
            SetNegrito(Tabela, Negrito, Linhas, Colunas);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="NomeTabela"></param>
        /// <param name="Cor"></param>
        /// <param name="Linha"></param>
        /// <param name="Coluna"></param>
        public void VirSetCorFundo(string NomeTabela, Color Cor, int Linha, int Coluna)
        {
            VirSetCorFundo(NomeTabela, Cor,null,null, new List<int> { Linha }, new List<int> { Coluna });
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="NomeTabela"></param>
        /// <param name="CorTabela"></param>
        /// <param name="CorCabecalho"></param>
        /// <param name="CorRodape"></param>
        /// <param name="Linhas"></param>
        /// <param name="Colunas"></param>
        public void VirSetCorFundo(string NomeTabela, Color CorTabela, Color? CorCabecalho = null, Color? CorRodape = null, List<int> Linhas = null, List<int> Colunas = null)
        {
            Table Tabela = TabelasCriadas[NomeTabela];
            Tabela.BeginUpdate();
            SetCorFundo(Tabela, CorTabela, CorCabecalho, CorRodape, Linhas, Colunas);
            Tabela.EndUpdate();
        }        

        /// <summary>
        /// 
        /// </summary>
        /// <param name="NomeTabela"></param>
        /// <param name="Larguras"></param>
        public void VirSetLarguras_mm(string NomeTabela, params int[] Larguras)
        {
            Table Tabela = TabelasCriadas[NomeTabela];
            Tabela.BeginUpdate();
            SetLargurasColunas_mm(Tabela, Larguras);
            Tabela.EndUpdate();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="NomeTabela"></param>
        /// <param name="AlinhamentoTabela"></param>
        /// <param name="Linha"></param>
        /// <param name="Coluna"></param>
        public void VirSetAlinhamentoCel(string NomeTabela, ParagraphAlignment AlinhamentoTabela, int Linha, int Coluna)
        {
            VirSetAlinhamentoCel(NomeTabela, AlinhamentoTabela, null, null, new List<int> { Linha }, new List<int> { Coluna });
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="NomeTabela"></param>
        /// <param name="AlinhamentoTabela"></param>
        /// <param name="AlinhamentoCabecalho"></param>
        /// <param name="AlinhamentoRodape"></param>
        /// <param name="Linhas"></param>
        /// <param name="Colunas"></param>
        public void VirSetAlinhamentoCel(string NomeTabela, ParagraphAlignment AlinhamentoTabela, ParagraphAlignment? AlinhamentoCabecalho = null, ParagraphAlignment? AlinhamentoRodape = null,List<int> Linhas = null, List<int> Colunas = null)
        {
            Table Tabela = TabelasCriadas[NomeTabela];
            Tabela.BeginUpdate();
            SetAlinhamentoCel(Tabela, AlinhamentoTabela, AlinhamentoCabecalho, AlinhamentoRodape,Linhas,Colunas);
            Tabela.EndUpdate();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="NomeTabela"></param>
        /// <param name="FonteTabela"></param>
        /// <param name="Linha"></param>
        /// <param name="Coluna"></param>
        public void VirSetFonteCel(string NomeTabela, Font FonteTabela, int Linha, int Coluna)
        {
            VirSetFonteCel(NomeTabela, FonteTabela, null, null, new List<int> { Linha }, new List<int> { Coluna });
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="NomeTabela"></param>
        /// <param name="FonteTabela"></param>
        /// <param name="FonteCabecalho"></param>
        /// <param name="FonteRodape"></param>
        /// <param name="Linhas"></param>
        /// <param name="Colunas"></param>
        public void VirSetFonteCel(string NomeTabela, Font FonteTabela, Font FonteCabecalho = null, Font FonteRodape = null, List<int> Linhas = null, List<int> Colunas = null)
        {
            Table Tabela = TabelasCriadas[NomeTabela];
            SetFonteCel(Tabela, FonteTabela, FonteCabecalho, FonteRodape, Linhas, Colunas);
        }

        #endregion        

        #region Carrega Impresso

        /// <summary>
        /// 
        /// </summary>
        /// <param name="IMSNome"></param>
        /// <returns></returns>
        public bool CarregarImpresso(string IMSNome)
        {
            CarregaDados(IMSNome);
            return CarregarImpresso(LinhaMae.IMS, new SortedList(), null);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="IMSNome"></param>
        /// <param name="Dados"></param>
        /// <param name="Tabelas">Coleção de tabelas</param>
        /// <returns></returns>
        public bool CarregarImpresso(string IMSNome, SortedList Dados, SortedList<string, DataTable> Tabelas = null)
        {
            CarregaDados(IMSNome);
            return CarregarImpresso(LinhaMae.IMS, Dados, Tabelas);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="IMSNome"></param>
        /// <param name="Dados"></param>
        /// <param name="Tabelas">Coleção de tabelas</param>
        /// <returns></returns>
        public bool CarregarImpresso(string IMSNome, DataTable Dados, SortedList<string, DataTable> Tabelas = null)
        {
            CarregaDados(IMSNome);
            return CarregarImpresso(LinhaMae.IMS, Dados, Tabelas);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="IMSNome"></param>
        /// <param name="Dados"></param>
        /// <param name="Tabelas">Coleção de tabelas</param>
        /// <returns></returns>
        public bool CarregarImpresso(string IMSNome, DataRow Dados, SortedList<string, DataTable> Tabelas = null)
        {
            CarregaDados(IMSNome);
            return CarregarImpresso(LinhaMae.IMS, Dados, Tabelas);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="_IMS"></param>
        /// <returns></returns>
        public bool CarregarImpresso(int _IMS)
        {             
            return CarregarImpresso(_IMS, new SortedList(), null);
        }

        /// <summary>
        /// Carrega os dados
        /// </summary>
        /// <param name="_IMS"></param>
        /// <param name="DadosList"></param>
        /// <param name="Tabelas">Coleção de tabelas</param>
        /// <returns></returns>
        public bool CarregarImpresso(int _IMS, SortedList DadosList, SortedList<string, DataTable> Tabelas = null)
        {
            IMS = _IMS;            
            SortedList_para_Dados(DadosList);
            return CarregarImpressoEfetivo(Tabelas);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="_IMS"></param>
        /// <param name="DadosRow"></param>
        /// <param name="Tabelas"></param>
        /// <returns></returns>
        public bool CarregarImpresso(int _IMS, DataRow DadosRow, SortedList<string, DataTable> Tabelas = null)
        {
            IMS = _IMS;
            DataRow_para_Dados(DadosRow);
            return CarregarImpressoEfetivo(Tabelas);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="_IMS"></param>
        /// <param name="_Dados"></param>
        /// <param name="Tabelas"></param>
        /// <returns></returns>
        public bool CarregarImpresso(int _IMS, DataTable _Dados, SortedList<string, DataTable> Tabelas = null)
        {
            IMS = _IMS;
            Dados = _Dados;
            return CarregarImpressoEfetivo(Tabelas);
        }

        /// <summary>
        /// Carrega o impresso pré-montado
        /// </summary>        
        /// <param name="Tabelas"></param>
        /// <returns></returns>
        private bool CarregarImpressoEfetivo(SortedList<string, DataTable> Tabelas = null)
        {
            PreCargaEfetivo(Tabelas, true);
            FimCarrega();
            return true;
        }

        #endregion

        #region Carrega Impressso parcial
        /// <summary>
        /// Final do processo Pré-carga Formatação Final
        /// </summary>
        /// <returns></returns>
        public bool FimCarrega()
        {
            using (MemoryStream Ms = new MemoryStream())
            {
                richEditControl1.Document.MailMerge(Ms, DocumentFormat.Rtf);
                Ms.Position = 0;
                QuadroRTFBase.LoadFile(Ms, XRRichTextStreamType.RtfText);
            }
            return true;
        }

        /// <summary>
        /// início do processo do processo Pré-carga Formatação Final
        /// </summary>
        /// <param name="IMSNome"></param>
        /// <param name="Dados"></param>
        /// <param name="Tabelas"></param>
        /// <param name="FormatacaoPadrao"></param>
        /// <returns></returns>
        public bool PreCarga(string IMSNome, SortedList Dados, SortedList<string, DataTable> Tabelas = null, bool FormatacaoPadrao = false)
        {
            CarregaDados(IMSNome);
            return PreCarga(LinhaMae.IMS, Dados, Tabelas, FormatacaoPadrao);
        }

        /// <summary>
        /// início do processo do processo Pré-carga Formatação Final
        /// </summary>
        /// <param name="IMSNome"></param>
        /// <param name="Dados"></param>
        /// <param name="Tabelas"></param>
        /// <param name="FormatacaoPadrao"></param>
        /// <returns></returns>
        public bool PreCarga(string IMSNome, DataRow Dados, SortedList<string, DataTable> Tabelas = null, bool FormatacaoPadrao = false)
        {
            CarregaDados(IMSNome);
            return PreCarga(LinhaMae.IMS, Dados, Tabelas, FormatacaoPadrao);
        }

        /// <summary>
        /// início do processo do processo Pré-carga Formatação Final
        /// </summary>
        /// <param name="IMSNome"></param>
        /// <param name="Dados"></param>
        /// <param name="Tabelas"></param>
        /// <param name="FormatacaoPadrao"></param>
        /// <returns></returns>
        public bool PreCarga(string IMSNome, DataTable Dados, SortedList<string, DataTable> Tabelas = null, bool FormatacaoPadrao = false)
        {
            CarregaDados(IMSNome);
            return PreCarga(LinhaMae.IMS, Dados, Tabelas, FormatacaoPadrao);
        }

        /// <summary>
        /// início do processo do processo Pré-carga Formatação Final
        /// </summary>
        /// <param name="_IMS"></param>
        /// <param name="DadosList"></param>
        /// <param name="Tabelas"></param>
        /// <param name="FormatacaoPadrao"></param>
        /// <returns></returns>
        public bool PreCarga(int _IMS, SortedList DadosList, SortedList<string, DataTable> Tabelas = null, bool FormatacaoPadrao = false)
        {
            IMS = _IMS;
            SortedList_para_Dados(DadosList);
            return PreCargaEfetivo(Tabelas, FormatacaoPadrao);
        }

        /// <summary>
        /// início do processo do processo Pré-carga Formatação Final
        /// </summary>
        /// <param name="_IMS"></param>
        /// <param name="DadosRow"></param>
        /// <param name="Tabelas"></param>
        /// <param name="FormatacaoPadrao"></param>
        /// <returns></returns>
        public bool PreCarga(int _IMS, DataRow DadosRow, SortedList<string, DataTable> Tabelas = null, bool FormatacaoPadrao = false)
        {
            IMS = _IMS;
            DataRow_para_Dados(DadosRow);
            return PreCargaEfetivo(Tabelas, FormatacaoPadrao);
        }

        /// <summary>
        /// início do processo do processo Pré-carga Formatação Final
        /// </summary>
        /// <param name="_IMS"></param>
        /// <param name="_Dados"></param>
        /// <param name="Tabelas"></param>
        /// <param name="FormatacaoPadrao"></param>
        /// <returns></returns>
        public bool PreCarga(int _IMS, DataTable _Dados, SortedList<string, DataTable> Tabelas = null, bool FormatacaoPadrao = false)
        {
            IMS = _IMS;
            Dados = _Dados;
            return PreCargaEfetivo(Tabelas);
        }

        /// <summary>
        /// início do processo do processo Pré-carga Formatação Final
        /// </summary>
        /// <param name="Tabelas"></param>
        /// <param name="FormatacaoPadrao"></param>
        /// <returns></returns>
        private bool PreCargaEfetivo(SortedList<string, DataTable> Tabelas = null, bool FormatacaoPadrao = true)
        {
            CarregaDados(IMS);

            if (Tabelas != null)
                foreach (string NomeTabela in Tabelas.Keys)
                {
                    Dados.Columns.Add(NomeTabela);
                    Dados.Rows[0][NomeTabela] = string.Empty;
                }

            richEditControl1 = new RichEditControl();
            if (!LinhaMae.IsIMSRTFNull())
                richEditControl1.RtfText = LinhaMae.IMSRTF;
            
            foreach (DataColumn DC in Dados.Columns)
                VerificaCadastraCampo(DC.ColumnName);
            richEditControl1.Options.MailMerge.DataSource = Dados;               
            
            richEditControl1.BindingContext = new System.Windows.Forms.BindingContext();

            if (Tabelas != null)
            {
                TabelasCriadas = new SortedList<string, Table>();
                foreach (string NomeTabela in Tabelas.Keys)
                    TabelasCriadas.Add(NomeTabela, CarregaTabela(richEditControl1, NomeTabela, Tabelas[NomeTabela], FormatacaoPadrao));
            }

            return true;
        }

        #endregion


    }
}
