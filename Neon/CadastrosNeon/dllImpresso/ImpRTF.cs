using System;
using System.Drawing;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using DevExpress.XtraReports.UI;

namespace dllImpresso
{
    /// <summary>
    /// Compotente para impress�o de mala direta
    /// </summary>
    public partial class ImpRTF : dllImpresso.ImpDestRem
    {
        /// <summary>
        /// Construtor
        /// </summary>
        public ImpRTF()
        {
            InitializeComponent();
            QuadroRTFBase = QuadroRTF;
        }

        private static ImpRTF _ImpRTFSt;

        /// <summary>
        /// 
        /// </summary>
        public static ImpRTF ImpRTFSt {
            get {
                if (_ImpRTFSt == null)
                    _ImpRTFSt = new ImpRTF();
                return _ImpRTFSt;
            }
        }
 
    }
}

