﻿namespace dllImpresso
{


    partial class dImpDestRem
    {
        private dImpBoletoBaseTableAdapters.BOLETOSIMPTableAdapter bOLETOSIMPTableAdapter;
        /// <summary>
        /// TableAdapter padrão para tabela: BOLETOSIMP
        /// </summary>
        public dImpBoletoBaseTableAdapters.BOLETOSIMPTableAdapter BOLETOSIMPTableAdapter
        {
            get
            {
                if (bOLETOSIMPTableAdapter == null)
                {
                    bOLETOSIMPTableAdapter = new dImpBoletoBaseTableAdapters.BOLETOSIMPTableAdapter();
                    bOLETOSIMPTableAdapter.TrocarStringDeConexao();
                };
                return bOLETOSIMPTableAdapter;
            }
        }
    }
}
