namespace dllImpresso
{
    partial class ImpDestRem
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(ImpDestRem));            
            this.xrSigla1 = new DevExpress.XtraReports.UI.XRLabel();
            this.QuadroSegundaPagina = new DevExpress.XtraReports.UI.XRPanel();
            this.QuadroPrincipal = new DevExpress.XtraReports.UI.XRPanel();
            this.QuadroEndereco = new DevExpress.XtraReports.UI.XRPanel();
            this.xrLabel2 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel1 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrPanel20 = new DevExpress.XtraReports.UI.XRPanel();
            this.xrLabel95 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel94 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel93 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel92 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel91 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel90 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel89 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel88 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel75 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel74 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrPictureBox7 = new DevExpress.XtraReports.UI.XRPictureBox();
            this.QuadroRemetente = new DevExpress.XtraReports.UI.XRPanel();
            this.xrSigla = new DevExpress.XtraReports.UI.XRLabel();
            this.xrPanel26 = new DevExpress.XtraReports.UI.XRPanel();
            this.xrLabel11 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel97 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrPanel35 = new DevExpress.XtraReports.UI.XRPanel();
            this.xrPanel34 = new DevExpress.XtraReports.UI.XRPanel();
            this.xrPanel33 = new DevExpress.XtraReports.UI.XRPanel();
            this.xrPanel32 = new DevExpress.XtraReports.UI.XRPanel();
            this.xrPanel31 = new DevExpress.XtraReports.UI.XRPanel();
            this.xrPanel30 = new DevExpress.XtraReports.UI.XRPanel();
            this.xrPanel29 = new DevExpress.XtraReports.UI.XRPanel();
            this.xrPanel28 = new DevExpress.XtraReports.UI.XRPanel();
            this.xrPanel27 = new DevExpress.XtraReports.UI.XRPanel();
            this.xrPanel21 = new DevExpress.XtraReports.UI.XRPanel();
            this.xrLabel96 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel87 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel86 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrPanel1 = new DevExpress.XtraReports.UI.XRPanel();
            this.xrLine10 = new DevExpress.XtraReports.UI.XRLine();
            this.xrLabel68 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel46 = new DevExpress.XtraReports.UI.XRLabel();
            this.bindingSource1 = new System.Windows.Forms.BindingSource(this.components);
            this.xrLabel42 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel55 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel122 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel53 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel57 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel115 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel117 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel25 = new DevExpress.XtraReports.UI.XRLabel();            
            this.bindingSourcePrincipal = new System.Windows.Forms.BindingSource(this.components);
            this.dImpDestRem1 = new dllImpresso.dImpDestRem();
            ((System.ComponentModel.ISupportInitialize)(this.bindingSource1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.bindingSourcePrincipal)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dImpDestRem1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            // 
            // Detail
            // 
            this.Detail.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrSigla1,
            this.QuadroSegundaPagina,
            this.QuadroPrincipal,
            this.QuadroEndereco,
            this.QuadroRemetente});
            this.Detail.Dpi = 254F;
            this.Detail.HeightF = 5620F;
            this.Detail.Name = "Detail";
            this.Detail.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            this.Detail.PageBreak = DevExpress.XtraReports.UI.PageBreak.AfterBand;
            this.Detail.StylePriority.UseTextAlignment = false;
            // 
            // xrSigla1
            // 
            this.xrSigla1.Angle = 90F;
            this.xrSigla1.Dpi = 254F;
            this.xrSigla1.Font = new System.Drawing.Font("Courier New", 5.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrSigla1.LocationFloat = new DevExpress.Utils.PointFloat(1799F, 1016F);
            this.xrSigla1.Name = "xrSigla1";
            this.xrSigla1.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrSigla1.SizeF = new System.Drawing.SizeF(21F, 720F);
            this.xrSigla1.StylePriority.UseFont = false;
            this.xrSigla1.StylePriority.UseTextAlignment = false;
            this.xrSigla1.Text = "xrSigla";
            this.xrSigla1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomLeft;
            // 
            // QuadroSegundaPagina
            // 
            this.QuadroSegundaPagina.Dpi = 254F;
            this.QuadroSegundaPagina.LocationFloat = new DevExpress.Utils.PointFloat(0F, 4895F);
            this.QuadroSegundaPagina.Name = "QuadroSegundaPagina";
            this.QuadroSegundaPagina.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            this.QuadroSegundaPagina.SizeF = new System.Drawing.SizeF(1799F, 725F);
            this.QuadroSegundaPagina.StylePriority.UseBackColor = false;
            // 
            // QuadroPrincipal
            // 
            this.QuadroPrincipal.BackColor = System.Drawing.Color.Transparent;
            this.QuadroPrincipal.CanGrow = false;
            this.QuadroPrincipal.Dpi = 254F;
            this.QuadroPrincipal.LocationFloat = new DevExpress.Utils.PointFloat(0F, 0F);
            this.QuadroPrincipal.Name = "QuadroPrincipal";
            this.QuadroPrincipal.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            this.QuadroPrincipal.SizeF = new System.Drawing.SizeF(1799F, 2865F);
            // 
            // QuadroEndereco
            // 
            this.QuadroEndereco.CanGrow = false;
            this.QuadroEndereco.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrLabel2,
            this.xrLabel1,
            this.xrPanel20,
            this.xrPictureBox7});
            this.QuadroEndereco.Dpi = 254F;
            this.QuadroEndereco.LocationFloat = new DevExpress.Utils.PointFloat(0F, 3789F);
            this.QuadroEndereco.Name = "QuadroEndereco";
            this.QuadroEndereco.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            this.QuadroEndereco.SizeF = new System.Drawing.SizeF(1593F, 1106F);
            // 
            // xrLabel2
            // 
            this.xrLabel2.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "CONCodigoFolha1")});
            this.xrLabel2.Dpi = 254F;
            this.xrLabel2.Font = new System.Drawing.Font("Verdana", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel2.LocationFloat = new DevExpress.Utils.PointFloat(21.00001F, 817.1875F);
            this.xrLabel2.Name = "xrLabel2";
            this.xrLabel2.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrLabel2.SizeF = new System.Drawing.SizeF(155.7291F, 57.99951F);
            this.xrLabel2.StylePriority.UseBorders = false;
            this.xrLabel2.StylePriority.UseFont = false;
            this.xrLabel2.Text = "xrLabel2";
            this.xrLabel2.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrLabel1
            // 
            this.xrLabel1.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel1.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "CONCodigo")});
            this.xrLabel1.Dpi = 254F;
            this.xrLabel1.Font = new System.Drawing.Font("Verdana", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel1.LocationFloat = new DevExpress.Utils.PointFloat(187.9375F, 817.1877F);
            this.xrLabel1.Name = "xrLabel1";
            this.xrLabel1.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrLabel1.SizeF = new System.Drawing.SizeF(332.9999F, 58F);
            this.xrLabel1.StylePriority.UseFont = false;
            this.xrLabel1.Text = "xrLabel1";
            this.xrLabel1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrPanel20
            // 
            this.xrPanel20.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrPanel20.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrLabel95,
            this.xrLabel94,
            this.xrLabel93,
            this.xrLabel92,
            this.xrLabel91,
            this.xrLabel90,
            this.xrLabel89,
            this.xrLabel88,
            this.xrLabel75,
            this.xrLabel74});
            this.xrPanel20.Dpi = 254F;
            this.xrPanel20.LocationFloat = new DevExpress.Utils.PointFloat(26F, 418F);
            this.xrPanel20.Name = "xrPanel20";
            this.xrPanel20.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            this.xrPanel20.SizeF = new System.Drawing.SizeF(1376F, 381F);
            // 
            // xrLabel95
            // 
            this.xrLabel95.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel95.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "BOLEndereco")});
            this.xrLabel95.Dpi = 254F;
            this.xrLabel95.Font = new System.Drawing.Font("Verdana", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel95.LocationFloat = new DevExpress.Utils.PointFloat(339F, 265F);
            this.xrLabel95.Multiline = true;
            this.xrLabel95.Name = "xrLabel95";
            this.xrLabel95.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrLabel95.SizeF = new System.Drawing.SizeF(1026F, 58F);
            this.xrLabel95.Text = "xrLabel95";
            this.xrLabel95.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrLabel94
            // 
            this.xrLabel94.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel94.Dpi = 254F;
            this.xrLabel94.Font = new System.Drawing.Font("Verdana", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel94.LocationFloat = new DevExpress.Utils.PointFloat(32F, 265F);
            this.xrLabel94.Name = "xrLabel94";
            this.xrLabel94.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrLabel94.SizeF = new System.Drawing.SizeF(296F, 58F);
            this.xrLabel94.Text = "Endere�o:";
            this.xrLabel94.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrLabel93
            // 
            this.xrLabel93.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel93.Dpi = 254F;
            this.xrLabel93.Font = new System.Drawing.Font("Verdana", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel93.LocationFloat = new DevExpress.Utils.PointFloat(32F, 180F);
            this.xrLabel93.Name = "xrLabel93";
            this.xrLabel93.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrLabel93.SizeF = new System.Drawing.SizeF(296F, 58F);
            this.xrLabel93.Text = "Nome:";
            this.xrLabel93.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrLabel92
            // 
            this.xrLabel92.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel92.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "BOLNome")});
            this.xrLabel92.Dpi = 254F;
            this.xrLabel92.Font = new System.Drawing.Font("Verdana", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel92.LocationFloat = new DevExpress.Utils.PointFloat(339F, 180F);
            this.xrLabel92.Name = "xrLabel92";
            this.xrLabel92.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrLabel92.SizeF = new System.Drawing.SizeF(1026F, 58F);
            this.xrLabel92.Text = "xrLabel92";
            this.xrLabel92.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrLabel91
            // 
            this.xrLabel91.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel91.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "APTNumero")});
            this.xrLabel91.Dpi = 254F;
            this.xrLabel91.Font = new System.Drawing.Font("Verdana", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel91.LocationFloat = new DevExpress.Utils.PointFloat(773F, 111F);
            this.xrLabel91.Name = "xrLabel91";
            this.xrLabel91.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrLabel91.SizeF = new System.Drawing.SizeF(233F, 58F);
            this.xrLabel91.Text = "xrLabel91";
            this.xrLabel91.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrLabel90
            // 
            this.xrLabel90.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel90.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "BLOCodigo")});
            this.xrLabel90.Dpi = 254F;
            this.xrLabel90.Font = new System.Drawing.Font("Verdana", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel90.LocationFloat = new DevExpress.Utils.PointFloat(339F, 106F);
            this.xrLabel90.Name = "xrLabel90";
            this.xrLabel90.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrLabel90.SizeF = new System.Drawing.SizeF(233F, 58F);
            this.xrLabel90.Text = "xrLabel90";
            this.xrLabel90.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrLabel89
            // 
            this.xrLabel89.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel89.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "CONNomeAptoTit")});
            this.xrLabel89.Dpi = 254F;
            this.xrLabel89.Font = new System.Drawing.Font("Verdana", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel89.LocationFloat = new DevExpress.Utils.PointFloat(603F, 111F);
            this.xrLabel89.Name = "xrLabel89";
            this.xrLabel89.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrLabel89.SizeF = new System.Drawing.SizeF(154F, 58F);
            this.xrLabel89.Text = "xrLabel89";
            this.xrLabel89.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrLabel88
            // 
            this.xrLabel88.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel88.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "CONNomeBlocoTit")});
            this.xrLabel88.Dpi = 254F;
            this.xrLabel88.Font = new System.Drawing.Font("Verdana", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel88.LocationFloat = new DevExpress.Utils.PointFloat(32.00001F, 106F);
            this.xrLabel88.Name = "xrLabel88";
            this.xrLabel88.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrLabel88.SizeF = new System.Drawing.SizeF(301.0001F, 58F);
            this.xrLabel88.Text = "xrLabel88";
            this.xrLabel88.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrLabel75
            // 
            this.xrLabel75.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel75.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "CONNome")});
            this.xrLabel75.Dpi = 254F;
            this.xrLabel75.Font = new System.Drawing.Font("Verdana", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel75.LocationFloat = new DevExpress.Utils.PointFloat(339F, 32F);
            this.xrLabel75.Name = "xrLabel75";
            this.xrLabel75.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrLabel75.SizeF = new System.Drawing.SizeF(1026F, 58F);
            this.xrLabel75.Text = "xrLabel75";
            this.xrLabel75.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrLabel74
            // 
            this.xrLabel74.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel74.Dpi = 254F;
            this.xrLabel74.Font = new System.Drawing.Font("Verdana", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel74.LocationFloat = new DevExpress.Utils.PointFloat(32F, 32F);
            this.xrLabel74.Name = "xrLabel74";
            this.xrLabel74.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrLabel74.SizeF = new System.Drawing.SizeF(296F, 58F);
            this.xrLabel74.Text = "Condom�nio:";
            this.xrLabel74.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrPictureBox7
            // 
            this.xrPictureBox7.Dpi = 254F;
            this.xrPictureBox7.Image = ((System.Drawing.Image)(resources.GetObject("xrPictureBox7.Image")));
            this.xrPictureBox7.LocationFloat = new DevExpress.Utils.PointFloat(16F, 32F);
            this.xrPictureBox7.Name = "xrPictureBox7";
            this.xrPictureBox7.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            this.xrPictureBox7.SizeF = new System.Drawing.SizeF(592F, 329F);
            this.xrPictureBox7.Sizing = DevExpress.XtraPrinting.ImageSizeMode.ZoomImage;
            // 
            // QuadroRemetente
            // 
            this.QuadroRemetente.CanGrow = false;
            this.QuadroRemetente.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrSigla,
            this.xrPanel26,
            this.xrPanel1});
            this.QuadroRemetente.Dpi = 254F;
            this.QuadroRemetente.LocationFloat = new DevExpress.Utils.PointFloat(0F, 2953F);
            this.QuadroRemetente.Name = "QuadroRemetente";
            this.QuadroRemetente.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            this.QuadroRemetente.SizeF = new System.Drawing.SizeF(1794F, 825F);
            // 
            // xrSigla
            // 
            this.xrSigla.Angle = 180F;
            this.xrSigla.Dpi = 254F;
            this.xrSigla.Font = new System.Drawing.Font("Courier New", 5.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrSigla.LocationFloat = new DevExpress.Utils.PointFloat(1376F, 455F);
            this.xrSigla.Name = "xrSigla";
            this.xrSigla.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrSigla.SizeF = new System.Drawing.SizeF(360F, 32F);
            this.xrSigla.StylePriority.UseFont = false;
            this.xrSigla.StylePriority.UseTextAlignment = false;
            this.xrSigla.Text = "xrSigla";
            this.xrSigla.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight;
            // 
            // xrPanel26
            // 
            this.xrPanel26.BorderColor = System.Drawing.Color.DarkGray;
            this.xrPanel26.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrPanel26.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrLabel11,
            this.xrLabel97,
            this.xrPanel35,
            this.xrPanel34,
            this.xrPanel33,
            this.xrPanel32,
            this.xrPanel31,
            this.xrPanel30,
            this.xrPanel29,
            this.xrPanel28,
            this.xrPanel27,
            this.xrPanel21,
            this.xrLabel96,
            this.xrLabel87,
            this.xrLabel86});
            this.xrPanel26.Dpi = 254F;
            this.xrPanel26.LocationFloat = new DevExpress.Utils.PointFloat(1074F, 32F);
            this.xrPanel26.Name = "xrPanel26";
            this.xrPanel26.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            this.xrPanel26.SizeF = new System.Drawing.SizeF(667F, 418F);
            // 
            // xrLabel11
            // 
            this.xrLabel11.Angle = 180F;
            this.xrLabel11.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel11.Dpi = 254F;
            this.xrLabel11.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel11.ForeColor = System.Drawing.Color.DarkGray;
            this.xrLabel11.LocationFloat = new DevExpress.Utils.PointFloat(16F, 101F);
            this.xrLabel11.Multiline = true;
            this.xrLabel11.Name = "xrLabel11";
            this.xrLabel11.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrLabel11.SizeF = new System.Drawing.SizeF(566F, 108F);
            this.xrLabel11.Text = "N�o existe o N� Indicado\r\n\r\nInforma��o escrita pelo porteiro ou zelador";
            this.xrLabel11.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrLabel97
            // 
            this.xrLabel97.Angle = 180F;
            this.xrLabel97.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel97.Dpi = 254F;
            this.xrLabel97.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel97.ForeColor = System.Drawing.Color.DarkGray;
            this.xrLabel97.LocationFloat = new DevExpress.Utils.PointFloat(32F, 243F);
            this.xrLabel97.Multiline = true;
            this.xrLabel97.Name = "xrLabel97";
            this.xrLabel97.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrLabel97.SizeF = new System.Drawing.SizeF(204F, 108F);
            this.xrLabel97.Text = "Falecido\r\nAusente\r\nN�o Procurado";
            this.xrLabel97.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrPanel35
            // 
            this.xrPanel35.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrPanel35.Dpi = 254F;
            this.xrPanel35.LocationFloat = new DevExpress.Utils.PointFloat(243F, 249F);
            this.xrPanel35.Name = "xrPanel35";
            this.xrPanel35.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            this.xrPanel35.SizeF = new System.Drawing.SizeF(42F, 29F);
            // 
            // xrPanel34
            // 
            this.xrPanel34.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrPanel34.Dpi = 254F;
            this.xrPanel34.LocationFloat = new DevExpress.Utils.PointFloat(243F, 286F);
            this.xrPanel34.Name = "xrPanel34";
            this.xrPanel34.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            this.xrPanel34.SizeF = new System.Drawing.SizeF(42F, 29F);
            // 
            // xrPanel33
            // 
            this.xrPanel33.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrPanel33.Dpi = 254F;
            this.xrPanel33.LocationFloat = new DevExpress.Utils.PointFloat(243F, 318F);
            this.xrPanel33.Name = "xrPanel33";
            this.xrPanel33.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            this.xrPanel33.SizeF = new System.Drawing.SizeF(42F, 29F);
            // 
            // xrPanel32
            // 
            this.xrPanel32.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrPanel32.Dpi = 254F;
            this.xrPanel32.LocationFloat = new DevExpress.Utils.PointFloat(587F, 106F);
            this.xrPanel32.Name = "xrPanel32";
            this.xrPanel32.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            this.xrPanel32.SizeF = new System.Drawing.SizeF(42F, 29F);
            // 
            // xrPanel31
            // 
            this.xrPanel31.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrPanel31.Dpi = 254F;
            this.xrPanel31.LocationFloat = new DevExpress.Utils.PointFloat(587F, 138F);
            this.xrPanel31.Name = "xrPanel31";
            this.xrPanel31.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            this.xrPanel31.SizeF = new System.Drawing.SizeF(42F, 29F);
            // 
            // xrPanel30
            // 
            this.xrPanel30.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrPanel30.Dpi = 254F;
            this.xrPanel30.LocationFloat = new DevExpress.Utils.PointFloat(587F, 175F);
            this.xrPanel30.Name = "xrPanel30";
            this.xrPanel30.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            this.xrPanel30.SizeF = new System.Drawing.SizeF(42F, 29F);
            // 
            // xrPanel29
            // 
            this.xrPanel29.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrPanel29.Dpi = 254F;
            this.xrPanel29.LocationFloat = new DevExpress.Utils.PointFloat(587F, 212F);
            this.xrPanel29.Name = "xrPanel29";
            this.xrPanel29.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            this.xrPanel29.SizeF = new System.Drawing.SizeF(42F, 29F);
            // 
            // xrPanel28
            // 
            this.xrPanel28.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrPanel28.Dpi = 254F;
            this.xrPanel28.LocationFloat = new DevExpress.Utils.PointFloat(587F, 249F);
            this.xrPanel28.Name = "xrPanel28";
            this.xrPanel28.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            this.xrPanel28.SizeF = new System.Drawing.SizeF(42F, 29F);
            // 
            // xrPanel27
            // 
            this.xrPanel27.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrPanel27.Dpi = 254F;
            this.xrPanel27.LocationFloat = new DevExpress.Utils.PointFloat(587F, 286F);
            this.xrPanel27.Name = "xrPanel27";
            this.xrPanel27.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            this.xrPanel27.SizeF = new System.Drawing.SizeF(42F, 29F);
            // 
            // xrPanel21
            // 
            this.xrPanel21.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrPanel21.Dpi = 254F;
            this.xrPanel21.LocationFloat = new DevExpress.Utils.PointFloat(587F, 318F);
            this.xrPanel21.Name = "xrPanel21";
            this.xrPanel21.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            this.xrPanel21.SizeF = new System.Drawing.SizeF(42F, 29F);
            // 
            // xrLabel96
            // 
            this.xrLabel96.Angle = 180F;
            this.xrLabel96.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel96.Dpi = 254F;
            this.xrLabel96.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel96.ForeColor = System.Drawing.Color.DarkGray;
            this.xrLabel96.LocationFloat = new DevExpress.Utils.PointFloat(349F, 212F);
            this.xrLabel96.Multiline = true;
            this.xrLabel96.Name = "xrLabel96";
            this.xrLabel96.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrLabel96.SizeF = new System.Drawing.SizeF(233F, 148F);
            this.xrLabel96.Text = "Mudou-se\r\nDesconhecido\r\nRecusado\r\nEnd. Insuficiente";
            this.xrLabel96.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrLabel87
            // 
            this.xrLabel87.Angle = 180F;
            this.xrLabel87.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrLabel87.Dpi = 254F;
            this.xrLabel87.Font = new System.Drawing.Font("Tahoma", 6.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel87.ForeColor = System.Drawing.Color.DarkGray;
            this.xrLabel87.LocationFloat = new DevExpress.Utils.PointFloat(164F, 5F);
            this.xrLabel87.Multiline = true;
            this.xrLabel87.Name = "xrLabel87";
            this.xrLabel87.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrLabel87.SizeF = new System.Drawing.SizeF(497F, 90F);
            this.xrLabel87.Text = "Reintegrado ao Servi�o Postal em __/__/__\r\nEm __/__/__   ________________________" +
    "\r\n                     Assinatura e n� do entregador";
            this.xrLabel87.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrLabel86
            // 
            this.xrLabel86.Angle = 180F;
            this.xrLabel86.Dpi = 254F;
            this.xrLabel86.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel86.ForeColor = System.Drawing.Color.DarkGray;
            this.xrLabel86.LocationFloat = new DevExpress.Utils.PointFloat(10F, 365F);
            this.xrLabel86.Name = "xrLabel86";
            this.xrLabel86.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrLabel86.SizeF = new System.Drawing.SizeF(650F, 50F);
            this.xrLabel86.Text = "PARA USO DOS CORREIOS";
            this.xrLabel86.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrPanel1
            // 
            this.xrPanel1.BorderColor = System.Drawing.SystemColors.ControlText;
            this.xrPanel1.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrPanel1.BorderWidth = 1;
            this.xrPanel1.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrLine10,
            this.xrLabel68,
            this.xrLabel46,
            this.xrLabel42,
            this.xrLabel55,
            this.xrLabel122,
            this.xrLabel53,
            this.xrLabel57,
            this.xrLabel115,
            this.xrLabel117,
            this.xrLabel25});
            this.xrPanel1.Dpi = 254F;
            this.xrPanel1.LocationFloat = new DevExpress.Utils.PointFloat(21F, 21F);
            this.xrPanel1.Name = "xrPanel1";
            this.xrPanel1.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            this.xrPanel1.SizeF = new System.Drawing.SizeF(1032F, 312F);
            // 
            // xrLine10
            // 
            this.xrLine10.BorderColor = System.Drawing.SystemColors.ControlText;
            this.xrLine10.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLine10.BorderWidth = 1;
            this.xrLine10.Dpi = 254F;                                    
            this.xrLine10.ForeColor = System.Drawing.SystemColors.ControlText;
            this.xrLine10.LocationFloat = new DevExpress.Utils.PointFloat(10F, 243F);
            this.xrLine10.Name = "xrLine10";
            this.xrLine10.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            this.xrLine10.SizeF = new System.Drawing.SizeF(1016F, 11F);
            // 
            // xrLabel68
            // 
            this.xrLabel68.Angle = 180F;
            this.xrLabel68.BorderColor = System.Drawing.SystemColors.ControlText;
            this.xrLabel68.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel68.BorderWidth = 1;
            this.xrLabel68.Dpi = 254F;
            this.xrLabel68.Font = new System.Drawing.Font("Verdana", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel68.LocationFloat = new DevExpress.Utils.PointFloat(381F, 265F);
            this.xrLabel68.Name = "xrLabel68";
            this.xrLabel68.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrLabel68.SizeF = new System.Drawing.SizeF(254F, 37F);
            this.xrLabel68.Text = "Remetente";
            this.xrLabel68.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrLabel46
            // 
            this.xrLabel46.Angle = 180F;
            this.xrLabel46.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel46.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", this.bindingSource1, "EnderecoBairro")});
            this.xrLabel46.Dpi = 254F;
            this.xrLabel46.Font = new System.Drawing.Font("Verdana", 9.75F);
            this.xrLabel46.LocationFloat = new DevExpress.Utils.PointFloat(21F, 132F);
            this.xrLabel46.Name = "xrLabel46";
            this.xrLabel46.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrLabel46.SizeF = new System.Drawing.SizeF(990F, 48F);
            this.xrLabel46.Text = "xrLabel46";
            this.xrLabel46.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight;
            // 
            // bindingSource1
            // 
            this.bindingSource1.DataSource = typeof(Framework.DSCentral.EMPRESASDataTable);
            // 
            // xrLabel42
            // 
            this.xrLabel42.Angle = 180F;
            this.xrLabel42.BorderColor = System.Drawing.SystemColors.ControlText;
            this.xrLabel42.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel42.BorderWidth = 1;
            this.xrLabel42.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", this.bindingSource1, "EMPCidade")});
            this.xrLabel42.Dpi = 254F;
            this.xrLabel42.Font = new System.Drawing.Font("Verdana", 10F);
            this.xrLabel42.ForeColor = System.Drawing.SystemColors.ControlText;
            this.xrLabel42.LocationFloat = new DevExpress.Utils.PointFloat(413F, 74F);
            this.xrLabel42.Name = "xrLabel42";
            this.xrLabel42.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrLabel42.SizeF = new System.Drawing.SizeF(598F, 42F);
            this.xrLabel42.Text = "xrLabel42";
            this.xrLabel42.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            // 
            // xrLabel55
            // 
            this.xrLabel55.Angle = 180F;
            this.xrLabel55.BorderColor = System.Drawing.SystemColors.ControlText;
            this.xrLabel55.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel55.BorderWidth = 1;
            this.xrLabel55.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", this.bindingSource1, "EMPUF")});
            this.xrLabel55.Dpi = 254F;
            this.xrLabel55.Font = new System.Drawing.Font("Verdana", 10F);
            this.xrLabel55.ForeColor = System.Drawing.SystemColors.ControlText;
            this.xrLabel55.LocationFloat = new DevExpress.Utils.PointFloat(296F, 74F);
            this.xrLabel55.Name = "xrLabel55";
            this.xrLabel55.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrLabel55.SizeF = new System.Drawing.SizeF(101F, 42F);
            this.xrLabel55.Text = "xrLabel55";
            this.xrLabel55.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            // 
            // xrLabel122
            // 
            this.xrLabel122.Angle = 180F;
            this.xrLabel122.BorderColor = System.Drawing.SystemColors.ControlText;
            this.xrLabel122.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel122.BorderWidth = 1;
            this.xrLabel122.CanGrow = false;
            this.xrLabel122.Dpi = 254F;
            this.xrLabel122.Font = new System.Drawing.Font("Tahoma", 10F);
            this.xrLabel122.ForeColor = System.Drawing.SystemColors.ControlText;
            this.xrLabel122.LocationFloat = new DevExpress.Utils.PointFloat(376F, 16F);
            this.xrLabel122.Name = "xrLabel122";
            this.xrLabel122.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrLabel122.SizeF = new System.Drawing.SizeF(106F, 42F);
            this.xrLabel122.Text = "Fax: ";
            this.xrLabel122.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            // 
            // xrLabel53
            // 
            this.xrLabel53.Angle = 180F;
            this.xrLabel53.BorderColor = System.Drawing.SystemColors.ControlText;
            this.xrLabel53.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel53.BorderWidth = 1;
            this.xrLabel53.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", this.bindingSource1, "EMPCEP")});
            this.xrLabel53.Dpi = 254F;
            this.xrLabel53.Font = new System.Drawing.Font("Verdana", 10F);
            this.xrLabel53.ForeColor = System.Drawing.SystemColors.ControlText;
            this.xrLabel53.LocationFloat = new DevExpress.Utils.PointFloat(21F, 74F);
            this.xrLabel53.Name = "xrLabel53";
            this.xrLabel53.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrLabel53.SizeF = new System.Drawing.SizeF(259F, 43F);
            this.xrLabel53.Text = "xrLabel53";
            this.xrLabel53.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // xrLabel57
            // 
            this.xrLabel57.Angle = 180F;
            this.xrLabel57.BorderColor = System.Drawing.SystemColors.ControlText;
            this.xrLabel57.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel57.BorderWidth = 1;
            this.xrLabel57.CanGrow = false;
            this.xrLabel57.Dpi = 254F;
            this.xrLabel57.Font = new System.Drawing.Font("Tahoma", 10F);
            this.xrLabel57.ForeColor = System.Drawing.SystemColors.ControlText;
            this.xrLabel57.LocationFloat = new DevExpress.Utils.PointFloat(889F, 16F);
            this.xrLabel57.Name = "xrLabel57";
            this.xrLabel57.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrLabel57.SizeF = new System.Drawing.SizeF(121F, 42F);
            this.xrLabel57.Text = "Fone: ";
            this.xrLabel57.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            // 
            // xrLabel115
            // 
            this.xrLabel115.Angle = 180F;
            this.xrLabel115.BorderColor = System.Drawing.SystemColors.ControlText;
            this.xrLabel115.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel115.BorderWidth = 1;
            this.xrLabel115.CanGrow = false;
            this.xrLabel115.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", this.bindingSource1, "EMPFone")});
            this.xrLabel115.Dpi = 254F;
            this.xrLabel115.Font = new System.Drawing.Font("Verdana", 10F);
            this.xrLabel115.ForeColor = System.Drawing.SystemColors.ControlText;
            this.xrLabel115.LocationFloat = new DevExpress.Utils.PointFloat(540F, 16F);
            this.xrLabel115.Name = "xrLabel115";
            this.xrLabel115.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrLabel115.SizeF = new System.Drawing.SizeF(339F, 47F);
            this.xrLabel115.Text = "xrLabel115";
            this.xrLabel115.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            // 
            // xrLabel117
            // 
            this.xrLabel117.Angle = 180F;
            this.xrLabel117.BorderColor = System.Drawing.SystemColors.ControlText;
            this.xrLabel117.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel117.BorderWidth = 1;
            this.xrLabel117.CanGrow = false;
            this.xrLabel117.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", this.bindingSource1, "EMPFax")});
            this.xrLabel117.Dpi = 254F;
            this.xrLabel117.Font = new System.Drawing.Font("Verdana", 10F);
            this.xrLabel117.ForeColor = System.Drawing.SystemColors.ControlText;
            this.xrLabel117.LocationFloat = new DevExpress.Utils.PointFloat(26F, 16F);
            this.xrLabel117.Name = "xrLabel117";
            this.xrLabel117.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrLabel117.SizeF = new System.Drawing.SizeF(339F, 47F);
            this.xrLabel117.Text = "xrLabel117";
            this.xrLabel117.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            // 
            // xrLabel25
            // 
            this.xrLabel25.Angle = 180F;
            this.xrLabel25.BorderColor = System.Drawing.SystemColors.ControlText;
            this.xrLabel25.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel25.BorderWidth = 1;
            this.xrLabel25.CanGrow = false;
            this.xrLabel25.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", this.bindingSource1, "EMPRazao")});
            this.xrLabel25.Dpi = 254F;
            this.xrLabel25.Font = new System.Drawing.Font("Verdana", 10F);
            this.xrLabel25.ForeColor = System.Drawing.SystemColors.ControlText;
            this.xrLabel25.LocationFloat = new DevExpress.Utils.PointFloat(21F, 196F);
            this.xrLabel25.Name = "xrLabel25";
            this.xrLabel25.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrLabel25.SizeF = new System.Drawing.SizeF(990F, 42F);
            this.xrLabel25.Text = "xrLabel25";
            this.xrLabel25.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            
            
            // 
            // bindingSourcePrincipal
            // 
            this.bindingSourcePrincipal.DataMember = "CONDOMINIOS";
            this.bindingSourcePrincipal.DataSource = this.dImpDestRem1;
            // 
            // dImpDestRem1
            // 
            this.dImpDestRem1.DataSetName = "dImpDestRem";
            this.dImpDestRem1.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // ImpDestRem
            //             
            this.DataSource = this.bindingSourcePrincipal;
            this.Dpi = 254F;
            this.Margins = new System.Drawing.Printing.Margins(150, 130, 51, 51);
            this.PageHeight = 2970;
            this.PageWidth = 2100;
            this.PaperKind = System.Drawing.Printing.PaperKind.A4;
            this.ReportUnit = DevExpress.XtraReports.UI.ReportUnit.TenthsOfAMillimeter;
            this.Version = "12.2";
            this.BeforePrint += new System.Drawing.Printing.PrintEventHandler(this.ImpDestRem_BeforePrint);
            this.AfterPrint += new System.EventHandler(this.ImpDestRem_AfterPrint);
            ((System.ComponentModel.ISupportInitialize)(this.bindingSource1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.bindingSourcePrincipal)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dImpDestRem1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();

        }

        #endregion

        private DevExpress.XtraReports.UI.XRPanel xrPanel20;
        private DevExpress.XtraReports.UI.XRLabel xrLabel95;
        private DevExpress.XtraReports.UI.XRLabel xrLabel94;
        private DevExpress.XtraReports.UI.XRLabel xrLabel93;
        private DevExpress.XtraReports.UI.XRLabel xrLabel92;
        private DevExpress.XtraReports.UI.XRLabel xrLabel91;
        private DevExpress.XtraReports.UI.XRLabel xrLabel90;
        private DevExpress.XtraReports.UI.XRLabel xrLabel89;
        private DevExpress.XtraReports.UI.XRLabel xrLabel88;
        private DevExpress.XtraReports.UI.XRLabel xrLabel74;
        private DevExpress.XtraReports.UI.XRPictureBox xrPictureBox7;
        private DevExpress.XtraReports.UI.XRPanel QuadroRemetente;
        private DevExpress.XtraReports.UI.XRPanel xrPanel26;
        private DevExpress.XtraReports.UI.XRLabel xrLabel11;
        private DevExpress.XtraReports.UI.XRLabel xrLabel97;
        private DevExpress.XtraReports.UI.XRPanel xrPanel35;
        private DevExpress.XtraReports.UI.XRPanel xrPanel34;
        private DevExpress.XtraReports.UI.XRPanel xrPanel33;
        private DevExpress.XtraReports.UI.XRPanel xrPanel32;
        private DevExpress.XtraReports.UI.XRPanel xrPanel31;
        private DevExpress.XtraReports.UI.XRPanel xrPanel30;
        private DevExpress.XtraReports.UI.XRPanel xrPanel29;
        private DevExpress.XtraReports.UI.XRPanel xrPanel28;
        private DevExpress.XtraReports.UI.XRPanel xrPanel27;
        private DevExpress.XtraReports.UI.XRPanel xrPanel21;
        private DevExpress.XtraReports.UI.XRLabel xrLabel96;
        private DevExpress.XtraReports.UI.XRLabel xrLabel87;
        private DevExpress.XtraReports.UI.XRLabel xrLabel86;
        private DevExpress.XtraReports.UI.XRPanel xrPanel1;
        private DevExpress.XtraReports.UI.XRLabel xrLabel46;
        private DevExpress.XtraReports.UI.XRLabel xrLabel42;
        private DevExpress.XtraReports.UI.XRLabel xrLabel55;
        private DevExpress.XtraReports.UI.XRLabel xrLabel68;
        private DevExpress.XtraReports.UI.XRLine xrLine10;
        private DevExpress.XtraReports.UI.XRLabel xrLabel122;
        private DevExpress.XtraReports.UI.XRLabel xrLabel53;
        private DevExpress.XtraReports.UI.XRLabel xrLabel57;
        private DevExpress.XtraReports.UI.XRLabel xrLabel115;
        private DevExpress.XtraReports.UI.XRLabel xrLabel117;
        private DevExpress.XtraReports.UI.XRLabel xrLabel25;
        /// <summary>
        /// DataSet interno
        /// </summary>
        public dImpDestRem dImpDestRem1;
        /// <summary>
        /// Quadro principal
        /// </summary>
        protected DevExpress.XtraReports.UI.XRPanel QuadroPrincipal;
        /// <summary>
        /// Quadro da segunda p�gina
        /// </summary>
        protected DevExpress.XtraReports.UI.XRPanel QuadroSegundaPagina;
        /// <summary>
        /// bindingSourcePrincipal
        /// </summary>
        public System.Windows.Forms.BindingSource bindingSourcePrincipal;        
        /// <summary>
        /// Quadro endere�os
        /// </summary>
        protected DevExpress.XtraReports.UI.XRPanel QuadroEndereco;
        private DevExpress.XtraReports.UI.XRLabel xrSigla;
        /// <summary>
        /// Sigla
        /// </summary>
        protected DevExpress.XtraReports.UI.XRLabel xrSigla1;                
        private DevExpress.XtraReports.UI.XRLabel xrLabel75;
        private DevExpress.XtraReports.UI.XRLabel xrLabel1;
        private DevExpress.XtraReports.UI.XRLabel xrLabel2;
        /// <summary>
        /// 
        /// </summary>
        public System.Windows.Forms.BindingSource bindingSource1;
    }
}
