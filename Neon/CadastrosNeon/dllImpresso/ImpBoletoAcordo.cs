using System.Drawing;
using DevExpress.XtraReports.UI;

namespace dllImpresso
{
    /// <summary>
    /// 
    /// </summary>
    public partial class ImpBoletoAcordo : dllImpresso.ImpBoletoBase
    {
        /// <summary>
        /// 
        /// </summary>
        public ImpBoletoAcordo()
        {
            InitializeComponent();
        }

        /// <summary>
        /// 
        /// </summary>
        public void AjustarTamanho(){
            xrTitulo.Width = 800;
            QuadroNovosBoletos.Top = 5;
        }

        /// <summary>
        /// 
        /// </summary>
        public void AjustarFonte()
        {
            System.Drawing.Font novoFonte = new Font(labNovVencimento.Font.FontFamily, 5);
            labNovVencimento.Font = labNovValor.Font = labNovParcela.Font = labNovNumero.Font = novoFonte;
        }

        private void xrLabel76_Draw(object sender, DrawEventArgs e)
        {
            CantosRedondos(sender, e, 30);
        }

        private void labOrigValor_Draw(object sender, DrawEventArgs e)
        {
            Degrade(sender, e);
        }

        private void xrLabel19_Draw(object sender, DrawEventArgs e)
        {
            CantosRedondos(sender, e, 30);
        }

        private void labNovValor_Draw(object sender, DrawEventArgs e)
        {
            Degrade(sender, e);
        }
    }
}

