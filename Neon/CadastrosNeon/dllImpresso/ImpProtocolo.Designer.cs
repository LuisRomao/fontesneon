namespace dllImpresso
{
    partial class ImpProtocolo
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(ImpProtocolo));
            this.xrBLOCO = new DevExpress.XtraReports.UI.XRLabel();
            this.GroupHeader1 = new DevExpress.XtraReports.UI.GroupHeaderBand();
            this.xrBLOCO1 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel5 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel6 = new DevExpress.XtraReports.UI.XRLabel();
            this.Endereco = new DevExpress.XtraReports.UI.XRLabel();
            this.Linha = new DevExpress.XtraReports.UI.XRLine();
            this.GroupFooter1 = new DevExpress.XtraReports.UI.GroupFooterBand();
            this.xrPictureBox2 = new DevExpress.XtraReports.UI.XRPictureBox();
            this.xrLabel2 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrDocumento = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel3 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrPictureBoxEmail = new DevExpress.XtraReports.UI.XRPictureBox();
            this.bindingSource1 = new System.Windows.Forms.BindingSource(this.components);
            this.dImpProtocolo = new dllImpresso.dImpProtocolo();
            this.xrPictureBoxBradesco = new DevExpress.XtraReports.UI.XRPictureBox();
            ((System.ComponentModel.ISupportInitialize)(this.bindingSource1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dImpProtocolo)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            // 
            // xrTitulo
            // 
            this.xrTitulo.Text = "Protocolo de entrega";
            // 
            // PageHeader
            // 
            this.PageHeader.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrDocumento,
            this.xrLabel2});
            this.PageHeader.HeightF = 365F;
            // 
            // Detail
            // 
            this.Detail.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrPictureBoxBradesco,
            this.xrPictureBoxEmail,
            this.xrLabel3,
            this.Linha,
            this.xrPictureBox2,
            this.Endereco,
            this.xrLabel6,
            this.xrLabel5});
            this.Detail.HeightF = 124.8541F;
            this.Detail.KeepTogether = true;
            // 
            // xrBLOCO
            // 
            this.xrBLOCO.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrBLOCO.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "CONNomeBlocoTit")});
            this.xrBLOCO.Dpi = 254F;
            this.xrBLOCO.Font = new System.Drawing.Font("Times New Roman", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrBLOCO.LocationFloat = new DevExpress.Utils.PointFloat(0F, 0F);
            this.xrBLOCO.Name = "xrBLOCO";
            this.xrBLOCO.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrBLOCO.SizeF = new System.Drawing.SizeF(137.2419F, 42F);
            this.xrBLOCO.StylePriority.UseBorders = false;
            this.xrBLOCO.Text = "xrBLOCO";
            // 
            // GroupHeader1
            // 
            this.GroupHeader1.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrBLOCO1,
            this.xrBLOCO});
            this.GroupHeader1.Dpi = 254F;
            this.GroupHeader1.GroupFields.AddRange(new DevExpress.XtraReports.UI.GroupField[] {
            new DevExpress.XtraReports.UI.GroupField("BLOCO", DevExpress.XtraReports.UI.XRColumnSortOrder.Ascending)});
            this.GroupHeader1.HeightF = 58F;
            this.GroupHeader1.Name = "GroupHeader1";
            this.GroupHeader1.RepeatEveryPage = true;
            // 
            // xrBLOCO1
            // 
            this.xrBLOCO1.BackColor = System.Drawing.Color.LightGray;
            this.xrBLOCO1.CanGrow = false;
            this.xrBLOCO1.CanShrink = true;
            this.xrBLOCO1.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "BLOCO")});
            this.xrBLOCO1.Dpi = 254F;
            this.xrBLOCO1.Font = new System.Drawing.Font("Times New Roman", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrBLOCO1.LocationFloat = new DevExpress.Utils.PointFloat(148F, 0F);
            this.xrBLOCO1.Name = "xrBLOCO1";
            this.xrBLOCO1.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrBLOCO1.SizeF = new System.Drawing.SizeF(1630F, 42F);
            this.xrBLOCO1.Text = "xrBLOCO1";
            // 
            // xrLabel5
            // 
            this.xrLabel5.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "APTNumero")});
            this.xrLabel5.Dpi = 254F;
            this.xrLabel5.Font = new System.Drawing.Font("Times New Roman", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel5.LocationFloat = new DevExpress.Utils.PointFloat(0F, 0F);
            this.xrLabel5.Name = "xrLabel5";
            this.xrLabel5.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrLabel5.SizeF = new System.Drawing.SizeF(127F, 64F);
            this.xrLabel5.Text = "xrLabel5";
            this.xrLabel5.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomLeft;
            // 
            // xrLabel6
            // 
            this.xrLabel6.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "PESNome")});
            this.xrLabel6.Dpi = 254F;
            this.xrLabel6.LocationFloat = new DevExpress.Utils.PointFloat(169F, 21F);
            this.xrLabel6.Name = "xrLabel6";
            this.xrLabel6.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrLabel6.SizeF = new System.Drawing.SizeF(783F, 42F);
            this.xrLabel6.Text = "xrLabel6";
            this.xrLabel6.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomLeft;
            // 
            // Endereco
            // 
            this.Endereco.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "PESEndereco")});
            this.Endereco.Dpi = 254F;
            this.Endereco.Font = new System.Drawing.Font("Times New Roman", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Endereco.LocationFloat = new DevExpress.Utils.PointFloat(1079F, 21.00002F);
            this.Endereco.Multiline = true;
            this.Endereco.Name = "Endereco";
            this.Endereco.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.Endereco.SizeF = new System.Drawing.SizeF(719.0005F, 42F);
            this.Endereco.Text = "Endereco";
            // 
            // Linha
            // 
            this.Linha.Dpi = 254F;
            this.Linha.LineWidth = 3;
            this.Linha.LocationFloat = new DevExpress.Utils.PointFloat(762F, 85F);
            this.Linha.Name = "Linha";
            this.Linha.SizeF = new System.Drawing.SizeF(1037F, 5F);
            // 
            // GroupFooter1
            // 
            this.GroupFooter1.Dpi = 254F;
            this.GroupFooter1.HeightF = 40F;
            this.GroupFooter1.Name = "GroupFooter1";
            this.GroupFooter1.PageBreak = DevExpress.XtraReports.UI.PageBreak.AfterBand;
            // 
            // xrPictureBox2
            // 
            this.xrPictureBox2.Dpi = 254F;
            this.xrPictureBox2.Image = ((System.Drawing.Image)(resources.GetObject("xrPictureBox2.Image")));
            this.xrPictureBox2.LocationFloat = new DevExpress.Utils.PointFloat(952F, 20.66667F);
            this.xrPictureBox2.Name = "xrPictureBox2";
            this.xrPictureBox2.SizeF = new System.Drawing.SizeF(42.33333F, 42.33333F);
            this.xrPictureBox2.Sizing = DevExpress.XtraPrinting.ImageSizeMode.AutoSize;
            // 
            // xrLabel2
            // 
            this.xrLabel2.Dpi = 254F;
            this.xrLabel2.Font = new System.Drawing.Font("Times New Roman", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel2.LocationFloat = new DevExpress.Utils.PointFloat(0F, 296F);
            this.xrLabel2.Name = "xrLabel2";
            this.xrLabel2.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrLabel2.SizeF = new System.Drawing.SizeF(318F, 64F);
            this.xrLabel2.Text = "Documento:";
            // 
            // xrDocumento
            // 
            this.xrDocumento.Dpi = 254F;
            this.xrDocumento.Font = new System.Drawing.Font("Times New Roman", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrDocumento.LocationFloat = new DevExpress.Utils.PointFloat(339F, 296F);
            this.xrDocumento.Multiline = true;
            this.xrDocumento.Name = "xrDocumento";
            this.xrDocumento.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrDocumento.SizeF = new System.Drawing.SizeF(1439F, 64F);
            this.xrDocumento.Text = "xrDocumento";
            // 
            // xrLabel3
            // 
            this.xrLabel3.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "Proprietario")});
            this.xrLabel3.Dpi = 254F;
            this.xrLabel3.Font = new System.Drawing.Font("Times New Roman", 6F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel3.LocationFloat = new DevExpress.Utils.PointFloat(127F, 21F);
            this.xrLabel3.Name = "xrLabel3";
            this.xrLabel3.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrLabel3.SizeF = new System.Drawing.SizeF(21F, 21F);
            this.xrLabel3.Text = "xrLabel3";
            this.xrLabel3.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomLeft;
            // 
            // xrPictureBoxEmail
            // 
            this.xrPictureBoxEmail.Dpi = 254F;
            this.xrPictureBoxEmail.Image = ((System.Drawing.Image)(resources.GetObject("xrPictureBoxEmail.Image")));
            this.xrPictureBoxEmail.LocationFloat = new DevExpress.Utils.PointFloat(994.3333F, 21.66665F);
            this.xrPictureBoxEmail.Name = "xrPictureBoxEmail";
            this.xrPictureBoxEmail.SizeF = new System.Drawing.SizeF(42.33331F, 41.33335F);
            this.xrPictureBoxEmail.Sizing = DevExpress.XtraPrinting.ImageSizeMode.ZoomImage;
            // 
            // bindingSource1
            // 
            this.bindingSource1.DataMember = "DadosProt";
            this.bindingSource1.DataSource = this.dImpProtocolo;
            // 
            // dImpProtocolo
            // 
            this.dImpProtocolo.DataSetName = "dImpProtocolo";
            this.dImpProtocolo.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // xrPictureBoxBradesco
            // 
            this.xrPictureBoxBradesco.Dpi = 254F;
            this.xrPictureBoxBradesco.Image = ((System.Drawing.Image)(resources.GetObject("xrPictureBoxBradesco.Image")));
            this.xrPictureBoxBradesco.LocationFloat = new DevExpress.Utils.PointFloat(1036.667F, 21.66664F);
            this.xrPictureBoxBradesco.Name = "xrPictureBoxBradesco";
            this.xrPictureBoxBradesco.SizeF = new System.Drawing.SizeF(42.33331F, 41.33335F);
            this.xrPictureBoxBradesco.Sizing = DevExpress.XtraPrinting.ImageSizeMode.ZoomImage;
            // 
            // ImpProtocolo
            // 
            this.Bands.AddRange(new DevExpress.XtraReports.UI.Band[] {
            this.Detail,
            this.PageHeader,
            this.GroupHeader1,
            this.GroupFooter1});
            this.DataSource = this.bindingSource1;
            this.Version = "16.2";
            this.DataSourceRowChanged += new DevExpress.XtraReports.UI.DataSourceRowEventHandler(this.ImpProtocolo_DataSourceRowChanged);
            this.Controls.SetChildIndex(this.GroupFooter1, 0);
            this.Controls.SetChildIndex(this.GroupHeader1, 0);
            this.Controls.SetChildIndex(this.PageHeader, 0);
            this.Controls.SetChildIndex(this.Detail, 0);
            ((System.ComponentModel.ISupportInitialize)(this.bindingSource1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dImpProtocolo)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();

        }

        #endregion

        private DevExpress.XtraReports.UI.XRLabel xrBLOCO;
        private DevExpress.XtraReports.UI.GroupHeaderBand GroupHeader1;
        private DevExpress.XtraReports.UI.XRLabel xrBLOCO1;
        private DevExpress.XtraReports.UI.XRLine Linha;
        private DevExpress.XtraReports.UI.XRLabel Endereco;
        private DevExpress.XtraReports.UI.XRLabel xrLabel6;
        private DevExpress.XtraReports.UI.XRLabel xrLabel5;
        /// <summary>
        /// 
        /// </summary>
        public System.Windows.Forms.BindingSource bindingSource1;
        /// <summary>
        /// 
        /// </summary>
        public dImpProtocolo dImpProtocolo;
        private DevExpress.XtraReports.UI.GroupFooterBand GroupFooter1;
        private DevExpress.XtraReports.UI.XRPictureBox xrPictureBox2;
        private DevExpress.XtraReports.UI.XRLabel xrLabel2;
        private DevExpress.XtraReports.UI.XRLabel xrDocumento;
        private DevExpress.XtraReports.UI.XRLabel xrLabel3;
        private DevExpress.XtraReports.UI.XRPictureBox xrPictureBoxEmail;
        private DevExpress.XtraReports.UI.XRPictureBox xrPictureBoxBradesco;
    }
}