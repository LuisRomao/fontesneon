using System;
using System.Drawing;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using DevExpress.XtraReports.UI;

namespace dllImpresso
{
    /// <summary>
    /// 
    /// </summary>
    public partial class ImpSegundaVia : dllImpresso.ImpBoletoBase
    {
        /// <summary>
        /// 
        /// </summary>
        public ImpSegundaVia()
        {
            InitializeComponent();
        }

        private void xrLabel120_Draw(object sender, DrawEventArgs e)
        {
            CantosRedondos(sender, e, 30);
        }

        private void Lista12_Draw(object sender, DrawEventArgs e)
        {
            Degrade(sender, e);
        }

        private void ImpSegundaVia_DataSourceRowChanged(object sender, DataSourceRowEventArgs e)
        {
            System.Data.DataRowView DRV = (System.Data.DataRowView)bindingSourcePrincipal[e.CurrentRow];
            dImpBoletoBase.BOLETOSIMPRow row = (dImpBoletoBase.BOLETOSIMPRow)DRV.Row;
            if((row.IsBOLMensagemImpNull()) || (row.BOLMensagemImp == ""))            
                QuadroMensagem.Visible = false;
            else
                QuadroMensagem.Visible = true;
        }

        private void xrMensagem_Draw(object sender, DrawEventArgs e)
        {
            Degrade(sender, e);
        }

        private void xrLabel118_Draw(object sender, DrawEventArgs e)
        {
            CantosRedondos(sender, e, 30);
        }
    }
}

