﻿namespace dllImpresso
{
    partial class ImpBoletoSeguro
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(ImpBoletoSeguro));
            this.QuadroRTF = new DevExpress.XtraReports.UI.XRRichText();
            ((System.ComponentModel.ISupportInitialize)(this.dImpBoletoBase1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dImpDestRem1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.bindingSourcePrincipal)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.bindingSource1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.QuadroRTF)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            // 
            // xrLabelUsoBanco
            // 
            this.xrLabelUsoBanco.StylePriority.UseBorders = false;
            // 
            // xrLabelCarteira
            // 
            this.xrLabelCarteira.StylePriority.UseBorders = false;
            // 
            // xrLabelEspecie
            // 
            this.xrLabelEspecie.StylePriority.UseBorders = false;
            // 
            // BANCOLocal
            // 
            this.BANCOLocal.StylePriority.UseBorders = false;
            this.BANCOLocal.StylePriority.UseFont = false;
            // 
            // BANCONumero
            // 
            this.BANCONumero.StylePriority.UseFont = false;
            // 
            // QuadroUtil
            // 
            this.QuadroUtil.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.QuadroRTF});
            // 
            // xrLabel6
            // 
            this.xrLabel6.StylePriority.UseFont = false;
            this.xrLabel6.StylePriority.UseTextAlignment = false;
            // 
            // rLabelcompet
            // 
            this.rLabelcompet.StylePriority.UseBorders = false;
            // 
            // xrLabel3
            // 
            this.xrLabel3.StylePriority.UseBorders = false;
            // 
            // QuadroDadosTopo
            // 
            this.QuadroDadosTopo.StylePriority.UseBackColor = false;
            // 
            // RuleOcultaUsu
            // 
            this.RuleOcultaUsu.Formatting.Visible = DevExpress.Utils.DefaultBoolean.False;
            // 
            // QuadroSegundaPagina
            // 
            this.QuadroSegundaPagina.StylePriority.UseBackColor = false;
            // 
            // xrSigla1
            // 
            this.xrSigla1.StylePriority.UseFont = false;
            this.xrSigla1.StylePriority.UseTextAlignment = false;
            // 
            // Detail
            // 
            this.Detail.StylePriority.UseTextAlignment = false;
            // 
            // QuadroRTF
            // 
            this.QuadroRTF.Dpi = 254F;
            this.QuadroRTF.Font = new System.Drawing.Font("Times New Roman", 9.75F);
            this.QuadroRTF.LocationFloat = new DevExpress.Utils.PointFloat(0F, 0F);
            this.QuadroRTF.Name = "QuadroRTF";
            this.QuadroRTF.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.QuadroRTF.SerializableRtfString = resources.GetString("QuadroRTF.SerializableRtfString");
            this.QuadroRTF.SizeF = new System.Drawing.SizeF(1799F, 1714F);
            // 
            // ImpBoletoSeguro
            // 
            this.Bands.AddRange(new DevExpress.XtraReports.UI.Band[] {
            this.Detail,
            this.bottomMarginBand1});
            this.FormattingRuleSheet.AddRange(new DevExpress.XtraReports.UI.FormattingRule[] {
            this.RuleOcultaUsu});
            this.Version = "17.2";
            ((System.ComponentModel.ISupportInitialize)(this.dImpBoletoBase1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dImpDestRem1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.bindingSourcePrincipal)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.bindingSource1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.QuadroRTF)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();

        }

        #endregion

        private DevExpress.XtraReports.UI.XRRichText QuadroRTF;
    }
}
