﻿using VirEnumeracoesNeon;

namespace dllImpresso.ExtratoBancario {


    partial class dImpExtratoB
    {
        
    
        private dImpExtratoBTableAdapters.DescricaoDoChequeTableAdapter descricaoDoChequeTableAdapter;
        /// <summary>
        /// TableAdapter padrão para tabela: DescricaoDoCheque
        /// </summary>
        public dImpExtratoBTableAdapters.DescricaoDoChequeTableAdapter DescricaoDoChequeTableAdapter
        {
            get
            {
                if (descricaoDoChequeTableAdapter == null)
                {
                    descricaoDoChequeTableAdapter = new dImpExtratoBTableAdapters.DescricaoDoChequeTableAdapter();
                    descricaoDoChequeTableAdapter.TrocarStringDeConexao();
                };
                return descricaoDoChequeTableAdapter;
            }
        }

        private dllImpresso.ExtratoBancario.dImpExtratoBTableAdapters.DadosExtratoBTableAdapter dadosExtratoBTableAdapter;

        /// <summary>
        /// 
        /// </summary>
        public dllImpresso.ExtratoBancario.dImpExtratoBTableAdapters.DadosExtratoBTableAdapter DadosExtratoBTableAdapter {
            get {
                if (dadosExtratoBTableAdapter == null) {
                    dadosExtratoBTableAdapter = new dllImpresso.ExtratoBancario.dImpExtratoBTableAdapters.DadosExtratoBTableAdapter();
                    dadosExtratoBTableAdapter.TrocarStringDeConexao();
                };
                return dadosExtratoBTableAdapter;
            }        
        }

        private bool TransferenciaInterna(string Descricao)
        {
            
            if ((Descricao.ToUpper().Trim() == "APL APLIC AUT MAIS") || (Descricao.ToUpper().Trim() == "RES APLIC AUT MAIS"))
                    return true;
            return false;
        }

        internal CCTTipo CCTTipo;
        internal int BCO;
         
        /// <summary>
        /// 
        /// </summary>
        /// <param name="ComComentarios"></param>
        /// <param name="SomarAp"></param>
        public void CalculosDadosExtratoB(bool ComComentarios,bool SomarAp){

            System.Collections.Generic.List<dImpExtratoB.DadosExtratoBRow> Eliminar = new System.Collections.Generic.List<DadosExtratoBRow>();

            foreach (dImpExtratoB.DadosExtratoBRow row in DadosExtratoB) {

                if ((BCO == 341) && TransferenciaInterna(row.CCDDescricao))
                {
                    Eliminar.Add(row);
                    continue;
                }
                row.cDescricao2linhas = row.CCDDescricao.Trim();

                if ((BCO == 237) && (!row.IsCCDDescComplementoNull()))
                {
                    string manobra = row.CCDDescComplemento.Trim();
                    while (manobra.Contains("  "))
                        manobra = manobra.Replace("  ", " ");
                    if (manobra != "")
                        row.cDescricao2linhas += " - " + manobra;
                }

                if(!row.IsCCDTipoLancamentoNull())
                    if ((row.CCDTipoLancamento == 1) && ComComentarios) //cheque
                        if (DescricaoDoChequeTableAdapter.Fill(DescricaoDoCheque, row.CCD) > 0)
                        {
                            foreach (DescricaoDoChequeRow rowch in DescricaoDoCheque)
                            {
                                string manobra = "";
                                if (rowch.NOA_PLA != "213000")
                                    manobra = rowch.PLADescricao + " - ";                                
                                row.cDescricao2linhas += " - " + manobra + rowch.NOAServico;
                            }
                        }

                
                if (row.CCDValor < 0)
                {
                    row.cValor = -row.CCDValor;
                    row.cSinal = "-";
                }
                else
                {
                    row.cValor = row.CCDValor;
                    row.cSinal = "";
                }

                decimal SCCValorTotI = row.SCCValorI;
                if (SomarAp)
                    SCCValorTotI += row.SCCValorApI;
                if (SCCValorTotI < 0)
                {
                    row.cValorI = -SCCValorTotI;
                    row.cSinalI = "-";
                }
                else
                {
                    row.cValorI = SCCValorTotI;
                    row.cSinalI = "";
                };

                decimal SCCValorTotF = row.SCCValorF;                
                if (SomarAp)
                    SCCValorTotF += row.SCCValorApF;                
                row.cValorF = System.Math.Abs(SCCValorTotF);
                row.cSinalF = (SCCValorTotF < 0) ? "-" : "";

                row.cValorFAp = System.Math.Abs(row.SCCValorApF);
                row.cSinalFAp = (row.SCCValorApF < 0) ? "-" : "";

                row.cValorFcc = System.Math.Abs(row.SCCValorF);
                row.cSinalFcc = (row.SCCValorF < 0) ? "-" : "";

                row.cInterna = (!row.IsCCDTipoLancamentoNull() && (row.CCDTipoLancamento == (int)TipoLancamentoNeon.TransferenciaInterna));
            }
            foreach (dImpExtratoB.DadosExtratoBRow row in Eliminar)
                row.Delete();
            DadosExtratoB.AcceptChanges();
        }
        /*
        private void separaSinal(decimal Valor,System.Data.DataRow row,string CampoValor,string CampoSinal)
        {
            row[CampoValor] = System.Math.Abs(Valor);

            if (SCCValorTotF < 0)
                {
                    row.cValorF = -SCCValorTotF;
                    row.cSinalF = "-";
                }
                else
                {
                    row.cValorF = SCCValorTotF;
                    row.cSinalF = "";
                }
        }*/
    }
}
