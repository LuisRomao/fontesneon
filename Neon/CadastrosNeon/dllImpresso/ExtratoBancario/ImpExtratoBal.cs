﻿using System;
using System.Data;
using VirEnumeracoesNeon;
using FrameworkProc.datasets;

namespace dllImpresso.ExtratoBancario
{
    /// <summary>
    /// 
    /// </summary>
    public partial class ImpExtratoBal : dllImpresso.ImpLogoBase
    {
        enum TipoImpresso
        {
            CC,                //mostra os sados da CC e todos os lançamentos da mesma forma até os internos
            CCPlus,            //Mostra o saldo somado coloca os lançamentos internos fora da coluna de soma
            CCPlusTransparente //Mostra o saldo somado e oculta os lançamenos internos
        }
        
        /// <summary>
        /// Extrato contem dados
        /// </summary>
        public bool ContemDados;

        /// <summary>
        /// Construtor base não usar diretamente
        /// </summary>
        public ImpExtratoBal()
        {
            InitializeComponent();
            RemoverLogo();
        }

        private DateTime DataI;
        private DateTime DataF;
        private TipoImpresso Modelo;
        //private bool ComAplicacao;
        //private bool AplicacaiTransparente;
        

        /// <summary>
        /// Construtor
        /// </summary>
        /// <param name="Titulo"></param>
        /// <param name="_DataI"></param>
        /// <param name="_DataF"></param>
        /// <param name="CCT"></param>
        public ImpExtratoBal(string Titulo,DateTime _DataI, DateTime _DataF, int CCT)
            : this()
        {
            xrDetValorLeftFOriginal = xrDetValor.LeftF;
            xrDetSinalLeftFOriginal = xrDetSinal.LeftF;
            xrDetDescricaoLeftFOriginal = xrDetDescricao.LeftF;
            xrDetValorLeftFPLUS = xrValorDiaAPL.LeftF;
            xrDetSinalLeftFPLUS = xrValorDiaAPLSinal.LeftF;
            xrDetDescricaoLeftFPLUS = xrSaldoCC.LeftF;
            xrSaldoTotalTopFOriginal = xrSaldoTotal.TopF;
            xrValorTotalTopFOriginal = xrValorTotal.TopF;
            xrValorTotalSinalTopFOriginal = xrValorTotalSinal.TopF;
            DataI = _DataI;
            DataF = _DataF;
            dContasLogicas.ContaCorrenTeRow rowCCT = dContasLogicas.dContasLogicasSt.ContaCorrenTe.FindByCCT(CCT);
            xrCCTAgencia.Text = string.Format("{0:d4}",rowCCT.CCTAgencia);
            xrCCTContaDigito.Text = string.Format("{0:d6}-{1}", rowCCT.CCTConta,rowCCT.CCTContaDg);
            CCTTipo CCTTipo = (CCTTipo)rowCCT.CCTTipo;
            dImpExtratoB1.BCO = rowCCT.CCT_BCO;
            dImpExtratoB1.CCTTipo = CCTTipo;
            Modelo = TipoImpresso.CC;
            if (CCTTipo == CCTTipo.ContaCorrete_com_Oculta)
            {
                if (rowCCT.CCT_BCO != 237)
                    Modelo = TipoImpresso.CCPlusTransparente;
                else
                    Modelo = TipoImpresso.CCPlus;
            }
            /*
            if (rowCCT.IsCCT_CCTPlusNull())
            {
                if (CCTTipo == Framework.CCTTipo.ContaCorrete_com_Oculta)
                {
                    ComAplicacao = true;
                    AplicacaiTransparente = ComAplicacao && (rowCCT.CCT_BCO != 237);
                }
                else
                {
                    ComAplicacao = false;
                    AplicacaiTransparente = false; 
                }
            }
            else
            {
                ComAplicacao = false;
                AplicacaiTransparente = false;
            }*/

            comPLUS.Value = Modelo == TipoImpresso.CCPlus;// ComAplicacao && !AplicacaiTransparente;
            ContemDados = dImpExtratoB1.DadosExtratoBTableAdapter.Fill(dImpExtratoB1.DadosExtratoB, DataI, DataF, CCT) > 0;
            if (ContemDados)
            {
                Nomecondominio.Text = Titulo;
                DataInicial.Text = DataI.ToString("dd/MM/yyyy");
                DataFinal.Text = DataF.ToString("dd/MM/yyyy");
                if (rowCCT.IsCCTTituloNull())
                {
                    if (rowCCT.CCTAplicacao)
                        xrTitulo.Text = "EXTRATO MENSAL DE APLICAÇÃO";
                    else
                        xrTitulo.Text = "EXTRATO MENSAL DE CONTA CORRENTE";
                }
                else
                    xrTitulo.Text = rowCCT.CCTTitulo;
                
                //dImpExtratoB1.CalculosDadosExtratoB(true,ComAplicacao);
                dImpExtratoB1.CalculosDadosExtratoB(true, Modelo != TipoImpresso.CC);
                //if ((!ComAplicacao) || AplicacaiTransparente)
                if(Modelo == TipoImpresso.CCPlus)
                {
                    xrSaldoTotal.Text = "Saldo Total do Dia";
                }
                else
                {
                    GroupFooter1.HeightF = 64;
                    xrSaldoTotal.TopF = 0;
                    xrValorTotal.TopF = 0;
                    xrValorTotalSinal.TopF = 0;                    
                }
                CreateDocument();
            }
        }

        private float xrDetValorLeftFOriginal;
        private float xrDetSinalLeftFOriginal;
        private float xrDetDescricaoLeftFOriginal;
        private float xrDetValorLeftFPLUS;
        private float xrDetSinalLeftFPLUS;
        private float xrDetDescricaoLeftFPLUS;
        private float xrSaldoTotalTopFOriginal;
        private float xrValorTotalTopFOriginal;
        private float xrValorTotalSinalTopFOriginal;

        private dImpExtratoB.DadosExtratoBRow rowCorrente
        {
            get 
            {
                if (GetCurrentRow() == null)
                    return null;
                DataRowView DRV = (DataRowView)GetCurrentRow();
                if (DRV.Row == null)
                    return null;
                return (dImpExtratoB.DadosExtratoBRow)DRV.Row;                
            }
        }

        private void Detail_BeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e)
        {
            if (Modelo == TipoImpresso.CCPlus)
            {
                if (!rowCorrente.IsCCDTipoLancamentoNull() && (rowCorrente.CCDTipoLancamento == (int)TipoLancamentoNeon.TransferenciaInterna))
                {
                    xrDetValor.LeftF = xrDetValorLeftFPLUS;
                    xrDetSinal.LeftF = xrDetSinalLeftFPLUS;
                    xrDetCodigo.Visible = false;
                    xrDetDescricao.LeftF = xrDetDescricaoLeftFPLUS;
                    xrDetDoc.Visible = false;
                }
                else
                {
                    xrDetValor.LeftF = xrDetValorLeftFOriginal;
                    xrDetSinal.LeftF = xrDetSinalLeftFOriginal;
                    xrDetDescricao.LeftF = xrDetDescricaoLeftFOriginal;
                    xrDetCodigo.Visible = true;
                    xrDetDoc.Visible = true;
                }
            }
        }

        private void GroupFooter1_BeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e)
        {
            //if (ComAplicacao && !AplicacaiTransparente && (rowCorrente.SCCValorApF != 0))
            if ((Modelo == TipoImpresso.CCPlus) && (rowCorrente.SCCValorApF != 0))
            {
                xrSaldoTotal.TopF = xrSaldoTotalTopFOriginal;
                xrValorTotal.TopF = xrValorTotalTopFOriginal;
                xrValorTotalSinal.TopF = xrValorTotalSinalTopFOriginal;
            }
            else
            {                
                xrSaldoTotal.TopF = 0;
                xrValorTotal.TopF = 0;
                xrValorTotalSinal.TopF = 0;
            }
        }

    }
}
