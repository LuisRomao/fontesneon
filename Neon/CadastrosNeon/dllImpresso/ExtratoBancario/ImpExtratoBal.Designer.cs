﻿namespace dllImpresso.ExtratoBancario
{
    partial class ImpExtratoBal
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.dImpExtratoB1 = new dllImpresso.ExtratoBancario.dImpExtratoB();
            this.xrLabel5 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrCCTAgencia = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel7 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrCCTContaDigito = new DevExpress.XtraReports.UI.XRLabel();
            this.Nomecondominio = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel10 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel11 = new DevExpress.XtraReports.UI.XRLabel();
            this.DataInicial = new DevExpress.XtraReports.UI.XRLabel();
            this.DataFinal = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel1 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel14 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel15 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel29 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel26 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel16 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel17 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel13 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel28 = new DevExpress.XtraReports.UI.XRLabel();
            this.GroupHeader1 = new DevExpress.XtraReports.UI.GroupHeaderBand();
            this.xrDetData = new DevExpress.XtraReports.UI.XRLabel();
            this.xrDetCodigo = new DevExpress.XtraReports.UI.XRLabel();
            this.xrDetDescricao = new DevExpress.XtraReports.UI.XRLabel();
            this.xrDetDoc = new DevExpress.XtraReports.UI.XRLabel();
            this.xrDetValor = new DevExpress.XtraReports.UI.XRLabel();
            this.xrDetSinal = new DevExpress.XtraReports.UI.XRLabel();
            this.GroupFooter1 = new DevExpress.XtraReports.UI.GroupFooterBand();
            this.xrLabel23 = new DevExpress.XtraReports.UI.XRLabel();
            this.NaoPlusSome = new DevExpress.XtraReports.UI.FormattingRule();
            this.xrValorDiaAPLSinal = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel12 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrValorDiaAPL = new DevExpress.XtraReports.UI.XRLabel();
            this.xrSaldoPlus = new DevExpress.XtraReports.UI.XRLabel();
            this.xrSaldoCC = new DevExpress.XtraReports.UI.XRLabel();
            this.xrValorTotalSinal = new DevExpress.XtraReports.UI.XRLabel();
            this.xrValorTotal = new DevExpress.XtraReports.UI.XRLabel();
            this.xrSaldoTotal = new DevExpress.XtraReports.UI.XRLabel();
            this.comPLUS = new DevExpress.XtraReports.Parameters.Parameter();
            ((System.ComponentModel.ISupportInitialize)(this.dImpExtratoB1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            // 
            // PageHeader
            // 
            this.PageHeader.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrLabel16,
            this.xrLabel26,
            this.xrLabel29,
            this.xrLabel28,
            this.xrLabel13,
            this.xrLabel17,
            this.xrLabel15,
            this.DataInicial,
            this.xrLabel11,
            this.xrLabel10,
            this.xrLabel14,
            this.xrLabel1,
            this.DataFinal,
            this.Nomecondominio,
            this.xrCCTContaDigito,
            this.xrLabel7,
            this.xrCCTAgencia,
            this.xrLabel5});
            this.PageHeader.HeightF = 581.4167F;
            this.PageHeader.Controls.SetChildIndex(this.xrTitulo, 0);
            this.PageHeader.Controls.SetChildIndex(this.xrLabel5, 0);
            this.PageHeader.Controls.SetChildIndex(this.xrCCTAgencia, 0);
            this.PageHeader.Controls.SetChildIndex(this.xrLabel7, 0);
            this.PageHeader.Controls.SetChildIndex(this.xrCCTContaDigito, 0);
            this.PageHeader.Controls.SetChildIndex(this.Nomecondominio, 0);
            this.PageHeader.Controls.SetChildIndex(this.DataFinal, 0);
            this.PageHeader.Controls.SetChildIndex(this.xrLabel1, 0);
            this.PageHeader.Controls.SetChildIndex(this.xrLabel14, 0);
            this.PageHeader.Controls.SetChildIndex(this.xrLabel10, 0);
            this.PageHeader.Controls.SetChildIndex(this.xrLabel11, 0);
            this.PageHeader.Controls.SetChildIndex(this.DataInicial, 0);
            this.PageHeader.Controls.SetChildIndex(this.xrLabel15, 0);
            this.PageHeader.Controls.SetChildIndex(this.xrLabel17, 0);
            this.PageHeader.Controls.SetChildIndex(this.xrLabel13, 0);
            this.PageHeader.Controls.SetChildIndex(this.xrLabel28, 0);
            this.PageHeader.Controls.SetChildIndex(this.xrLabel29, 0);
            this.PageHeader.Controls.SetChildIndex(this.xrLabel26, 0);
            this.PageHeader.Controls.SetChildIndex(this.xrLabel16, 0);
            // 
            // Detail
            // 
            this.Detail.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrDetDoc,
            this.xrDetValor,
            this.xrDetSinal,
            this.xrDetData,
            this.xrDetCodigo,
            this.xrDetDescricao});
            this.Detail.HeightF = 55.62777F;
            this.Detail.KeepTogether = true;
            this.Detail.SortFields.AddRange(new DevExpress.XtraReports.UI.GroupField[] {
            new DevExpress.XtraReports.UI.GroupField("cInterna", DevExpress.XtraReports.UI.XRColumnSortOrder.Ascending)});
            this.Detail.BeforePrint += new System.Drawing.Printing.PrintEventHandler(this.Detail_BeforePrint);
            // 
            // dImpExtratoB1
            // 
            this.dImpExtratoB1.DataSetName = "dImpExtratoB";
            this.dImpExtratoB1.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // xrLabel5
            // 
            this.xrLabel5.Dpi = 254F;
            this.xrLabel5.Font = new System.Drawing.Font("Courier New", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel5.LocationFloat = new DevExpress.Utils.PointFloat(36.08339F, 208.8542F);
            this.xrLabel5.Name = "xrLabel5";
            this.xrLabel5.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrLabel5.SizeF = new System.Drawing.SizeF(233F, 50F);
            this.xrLabel5.StylePriority.UseFont = false;
            this.xrLabel5.Text = "Agencia:";
            // 
            // xrCCTAgencia
            // 
            this.xrCCTAgencia.Dpi = 254F;
            this.xrCCTAgencia.Font = new System.Drawing.Font("Courier New", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrCCTAgencia.LocationFloat = new DevExpress.Utils.PointFloat(270.229F, 208.8542F);
            this.xrCCTAgencia.Name = "xrCCTAgencia";
            this.xrCCTAgencia.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrCCTAgencia.SizeF = new System.Drawing.SizeF(127F, 50F);
            this.xrCCTAgencia.StylePriority.UseFont = false;
            this.xrCCTAgencia.Text = "0000";
            // 
            // xrLabel7
            // 
            this.xrLabel7.Dpi = 254F;
            this.xrLabel7.Font = new System.Drawing.Font("Courier New", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel7.LocationFloat = new DevExpress.Utils.PointFloat(418.2292F, 208.8542F);
            this.xrLabel7.Name = "xrLabel7";
            this.xrLabel7.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrLabel7.SizeF = new System.Drawing.SizeF(169F, 50F);
            this.xrLabel7.StylePriority.UseFont = false;
            this.xrLabel7.Text = "Conta:";
            // 
            // xrCCTContaDigito
            // 
            this.xrCCTContaDigito.Dpi = 254F;
            this.xrCCTContaDigito.Font = new System.Drawing.Font("Courier New", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrCCTContaDigito.LocationFloat = new DevExpress.Utils.PointFloat(609.2293F, 208.8542F);
            this.xrCCTContaDigito.Name = "xrCCTContaDigito";
            this.xrCCTContaDigito.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrCCTContaDigito.SizeF = new System.Drawing.SizeF(566F, 49F);
            this.xrCCTContaDigito.StylePriority.UseFont = false;
            this.xrCCTContaDigito.StylePriority.UseTextAlignment = false;
            this.xrCCTContaDigito.Text = "xrCCTContaDigito";
            this.xrCCTContaDigito.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // Nomecondominio
            // 
            this.Nomecondominio.Dpi = 254F;
            this.Nomecondominio.Font = new System.Drawing.Font("Courier New", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Nomecondominio.LocationFloat = new DevExpress.Utils.PointFloat(0F, 280.0209F);
            this.Nomecondominio.Name = "Nomecondominio";
            this.Nomecondominio.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.Nomecondominio.SizeF = new System.Drawing.SizeF(1778F, 50F);
            this.Nomecondominio.StylePriority.UseFont = false;
            this.Nomecondominio.Text = "NOME DO CONDOMINIO";
            // 
            // xrLabel10
            // 
            this.xrLabel10.Dpi = 254F;
            this.xrLabel10.Font = new System.Drawing.Font("Courier New", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel10.LocationFloat = new DevExpress.Utils.PointFloat(0F, 349.3333F);
            this.xrLabel10.Name = "xrLabel10";
            this.xrLabel10.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrLabel10.SizeF = new System.Drawing.SizeF(360F, 50F);
            this.xrLabel10.StylePriority.UseFont = false;
            this.xrLabel10.Text = "Data Inicial:";
            // 
            // xrLabel11
            // 
            this.xrLabel11.Dpi = 254F;
            this.xrLabel11.Font = new System.Drawing.Font("Courier New", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel11.LocationFloat = new DevExpress.Utils.PointFloat(0F, 399.3333F);
            this.xrLabel11.Name = "xrLabel11";
            this.xrLabel11.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrLabel11.SizeF = new System.Drawing.SizeF(296F, 50F);
            this.xrLabel11.StylePriority.UseFont = false;
            this.xrLabel11.Text = "Data Final:";
            // 
            // DataInicial
            // 
            this.DataInicial.Dpi = 254F;
            this.DataInicial.Font = new System.Drawing.Font("Courier New", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DataInicial.LocationFloat = new DevExpress.Utils.PointFloat(378.8541F, 349.3333F);
            this.DataInicial.Name = "DataInicial";
            this.DataInicial.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.DataInicial.SizeF = new System.Drawing.SizeF(318F, 50F);
            this.DataInicial.StylePriority.UseFont = false;
            this.DataInicial.Text = "00/00/0000";
            // 
            // DataFinal
            // 
            this.DataFinal.Dpi = 254F;
            this.DataFinal.Font = new System.Drawing.Font("Courier New", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DataFinal.LocationFloat = new DevExpress.Utils.PointFloat(378.8541F, 399.3333F);
            this.DataFinal.Name = "DataFinal";
            this.DataFinal.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.DataFinal.SizeF = new System.Drawing.SizeF(318F, 50F);
            this.DataFinal.StylePriority.UseFont = false;
            this.DataFinal.Text = "00/00/0000";
            // 
            // xrLabel1
            // 
            this.xrLabel1.Dpi = 254F;
            this.xrLabel1.Font = new System.Drawing.Font("Courier New", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel1.LocationFloat = new DevExpress.Utils.PointFloat(0F, 455.3334F);
            this.xrLabel1.Name = "xrLabel1";
            this.xrLabel1.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrLabel1.SizeF = new System.Drawing.SizeF(233F, 64F);
            this.xrLabel1.StylePriority.UseFont = false;
            this.xrLabel1.StylePriority.UseTextAlignment = false;
            this.xrLabel1.Text = "Data Lanc.";
            this.xrLabel1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomLeft;
            // 
            // xrLabel14
            // 
            this.xrLabel14.Dpi = 254F;
            this.xrLabel14.Font = new System.Drawing.Font("Courier New", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel14.LocationFloat = new DevExpress.Utils.PointFloat(233F, 455.3334F);
            this.xrLabel14.Name = "xrLabel14";
            this.xrLabel14.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrLabel14.SizeF = new System.Drawing.SizeF(106F, 64F);
            this.xrLabel14.StylePriority.UseFont = false;
            this.xrLabel14.StylePriority.UseTextAlignment = false;
            this.xrLabel14.Text = "Cod";
            this.xrLabel14.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomRight;
            // 
            // xrLabel15
            // 
            this.xrLabel15.Dpi = 254F;
            this.xrLabel15.Font = new System.Drawing.Font("Courier New", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel15.LocationFloat = new DevExpress.Utils.PointFloat(339F, 455.3333F);
            this.xrLabel15.Name = "xrLabel15";
            this.xrLabel15.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrLabel15.SizeF = new System.Drawing.SizeF(275F, 64F);
            this.xrLabel15.StylePriority.UseFont = false;
            this.xrLabel15.StylePriority.UseTextAlignment = false;
            this.xrLabel15.Text = "Lancamento";
            this.xrLabel15.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomLeft;
            // 
            // xrLabel29
            // 
            this.xrLabel29.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "DadosExtratoB.SCCDataA", "{0:dd/MM/yyyy}")});
            this.xrLabel29.Dpi = 254F;
            this.xrLabel29.Font = new System.Drawing.Font("Courier New", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel29.LocationFloat = new DevExpress.Utils.PointFloat(24.99999F, 539.4167F);
            this.xrLabel29.Name = "xrLabel29";
            this.xrLabel29.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrLabel29.SizeF = new System.Drawing.SizeF(233F, 42F);
            this.xrLabel29.StylePriority.UseFont = false;
            this.xrLabel29.StylePriority.UseTextAlignment = false;
            this.xrLabel29.Text = "xrLabel3";
            this.xrLabel29.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomLeft;
            // 
            // xrLabel26
            // 
            this.xrLabel26.Dpi = 254F;
            this.xrLabel26.Font = new System.Drawing.Font("Courier New", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel26.LocationFloat = new DevExpress.Utils.PointFloat(505.8541F, 539.3333F);
            this.xrLabel26.Name = "xrLabel26";
            this.xrLabel26.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrLabel26.SizeF = new System.Drawing.SizeF(471F, 42F);
            this.xrLabel26.StylePriority.UseFont = false;
            this.xrLabel26.StylePriority.UseTextAlignment = false;
            this.xrLabel26.Text = "Saldo Anterior";
            this.xrLabel26.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomLeft;
            // 
            // xrLabel16
            // 
            this.xrLabel16.Dpi = 254F;
            this.xrLabel16.Font = new System.Drawing.Font("Courier New", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel16.LocationFloat = new DevExpress.Utils.PointFloat(1330.854F, 455.3333F);
            this.xrLabel16.Name = "xrLabel16";
            this.xrLabel16.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrLabel16.SizeF = new System.Drawing.SizeF(190F, 64F);
            this.xrLabel16.StylePriority.UseFont = false;
            this.xrLabel16.StylePriority.UseTextAlignment = false;
            this.xrLabel16.Text = "Num. Doc";
            this.xrLabel16.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomLeft;
            // 
            // xrLabel17
            // 
            this.xrLabel17.Dpi = 254F;
            this.xrLabel17.Font = new System.Drawing.Font("Courier New", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel17.LocationFloat = new DevExpress.Utils.PointFloat(1542.854F, 455.3333F);
            this.xrLabel17.Name = "xrLabel17";
            this.xrLabel17.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrLabel17.SizeF = new System.Drawing.SizeF(254F, 64F);
            this.xrLabel17.StylePriority.UseFont = false;
            this.xrLabel17.StylePriority.UseTextAlignment = false;
            this.xrLabel17.Text = "Valor Lanc.";
            this.xrLabel17.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomLeft;
            // 
            // xrLabel13
            // 
            this.xrLabel13.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "DadosExtratoB.cValorI", "{0:n2}")});
            this.xrLabel13.Dpi = 254F;
            this.xrLabel13.Font = new System.Drawing.Font("Courier New", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel13.LocationFloat = new DevExpress.Utils.PointFloat(1500.854F, 539.3333F);
            this.xrLabel13.Name = "xrLabel13";
            this.xrLabel13.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrLabel13.SizeF = new System.Drawing.SizeF(254F, 42F);
            this.xrLabel13.StylePriority.UseFont = false;
            this.xrLabel13.StylePriority.UseTextAlignment = false;
            this.xrLabel13.Text = "Valor Lanc.";
            this.xrLabel13.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomRight;
            // 
            // xrLabel28
            // 
            this.xrLabel28.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "DadosExtratoB.cSinalI")});
            this.xrLabel28.Dpi = 254F;
            this.xrLabel28.Font = new System.Drawing.Font("Courier New", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel28.LocationFloat = new DevExpress.Utils.PointFloat(1754.854F, 539.3333F);
            this.xrLabel28.Name = "xrLabel28";
            this.xrLabel28.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrLabel28.SizeF = new System.Drawing.SizeF(42F, 42F);
            this.xrLabel28.StylePriority.UseFont = false;
            this.xrLabel28.StylePriority.UseTextAlignment = false;
            this.xrLabel28.Text = "-";
            this.xrLabel28.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomLeft;
            // 
            // GroupHeader1
            // 
            this.GroupHeader1.Dpi = 254F;
            this.GroupHeader1.GroupFields.AddRange(new DevExpress.XtraReports.UI.GroupField[] {
            new DevExpress.XtraReports.UI.GroupField("SCCData", DevExpress.XtraReports.UI.XRColumnSortOrder.Ascending)});
            this.GroupHeader1.GroupUnion = DevExpress.XtraReports.UI.GroupUnion.WholePage;
            this.GroupHeader1.HeightF = 0F;
            this.GroupHeader1.KeepTogether = true;
            this.GroupHeader1.Name = "GroupHeader1";
            // 
            // xrDetData
            // 
            this.xrDetData.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "DadosExtratoB.SCCData", "{0:dd/MM/yyyy}")});
            this.xrDetData.Dpi = 254F;
            this.xrDetData.Font = new System.Drawing.Font("Courier New", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrDetData.LocationFloat = new DevExpress.Utils.PointFloat(0F, 0F);
            this.xrDetData.Name = "xrDetData";
            this.xrDetData.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrDetData.SizeF = new System.Drawing.SizeF(219.665F, 42F);
            this.xrDetData.StylePriority.UseFont = false;
            this.xrDetData.StylePriority.UseTextAlignment = false;
            this.xrDetData.Text = "xrDetData";
            this.xrDetData.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomLeft;
            // 
            // xrDetCodigo
            // 
            this.xrDetCodigo.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "DadosExtratoB.CCDCodHistorico", "{0:00000}")});
            this.xrDetCodigo.Dpi = 254F;
            this.xrDetCodigo.Font = new System.Drawing.Font("Courier New", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrDetCodigo.LocationFloat = new DevExpress.Utils.PointFloat(219.665F, 0F);
            this.xrDetCodigo.Name = "xrDetCodigo";
            this.xrDetCodigo.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrDetCodigo.SizeF = new System.Drawing.SizeF(106F, 42F);
            this.xrDetCodigo.StylePriority.UseFont = false;
            this.xrDetCodigo.StylePriority.UseTextAlignment = false;
            this.xrDetCodigo.Text = "xrDetCodigo";
            this.xrDetCodigo.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomCenter;
            // 
            // xrDetDescricao
            // 
            this.xrDetDescricao.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "DadosExtratoB.cDescricao2linhas")});
            this.xrDetDescricao.Dpi = 254F;
            this.xrDetDescricao.Font = new System.Drawing.Font("Courier New", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrDetDescricao.LocationFloat = new DevExpress.Utils.PointFloat(325.665F, 0F);
            this.xrDetDescricao.Multiline = true;
            this.xrDetDescricao.Name = "xrDetDescricao";
            this.xrDetDescricao.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrDetDescricao.SizeF = new System.Drawing.SizeF(1005.189F, 42F);
            this.xrDetDescricao.StylePriority.UseFont = false;
            this.xrDetDescricao.StylePriority.UseTextAlignment = false;
            this.xrDetDescricao.Text = "xrDetDescricao";
            this.xrDetDescricao.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomLeft;
            // 
            // xrDetDoc
            // 
            this.xrDetDoc.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "DadosExtratoB.CCDDocumento", "{0:0000000}")});
            this.xrDetDoc.Dpi = 254F;
            this.xrDetDoc.Font = new System.Drawing.Font("Courier New", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrDetDoc.LocationFloat = new DevExpress.Utils.PointFloat(1330.854F, 0F);
            this.xrDetDoc.Name = "xrDetDoc";
            this.xrDetDoc.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrDetDoc.SizeF = new System.Drawing.SizeF(169F, 42F);
            this.xrDetDoc.StylePriority.UseFont = false;
            this.xrDetDoc.StylePriority.UseTextAlignment = false;
            this.xrDetDoc.Text = "Lancamento";
            this.xrDetDoc.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomRight;
            // 
            // xrDetValor
            // 
            this.xrDetValor.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "DadosExtratoB.cValor", "{0:n2}")});
            this.xrDetValor.Dpi = 254F;
            this.xrDetValor.Font = new System.Drawing.Font("Courier New", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrDetValor.LocationFloat = new DevExpress.Utils.PointFloat(1500.854F, 0F);
            this.xrDetValor.Name = "xrDetValor";
            this.xrDetValor.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrDetValor.SizeF = new System.Drawing.SizeF(254F, 42F);
            this.xrDetValor.StylePriority.UseFont = false;
            this.xrDetValor.StylePriority.UseTextAlignment = false;
            this.xrDetValor.Text = "Valor Lanc.";
            this.xrDetValor.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomRight;
            // 
            // xrDetSinal
            // 
            this.xrDetSinal.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "DadosExtratoB.cSinal")});
            this.xrDetSinal.Dpi = 254F;
            this.xrDetSinal.Font = new System.Drawing.Font("Courier New", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrDetSinal.LocationFloat = new DevExpress.Utils.PointFloat(1754.854F, 0F);
            this.xrDetSinal.Name = "xrDetSinal";
            this.xrDetSinal.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrDetSinal.SizeF = new System.Drawing.SizeF(42F, 42F);
            this.xrDetSinal.StylePriority.UseFont = false;
            this.xrDetSinal.StylePriority.UseTextAlignment = false;
            this.xrDetSinal.Text = "-";
            this.xrDetSinal.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomLeft;
            // 
            // GroupFooter1
            // 
            this.GroupFooter1.BackColor = System.Drawing.Color.Transparent;
            this.GroupFooter1.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrLabel23,
            this.xrValorDiaAPLSinal,
            this.xrLabel12,
            this.xrValorDiaAPL,
            this.xrSaldoPlus,
            this.xrSaldoCC,
            this.xrValorTotalSinal,
            this.xrValorTotal,
            this.xrSaldoTotal});
            this.GroupFooter1.Dpi = 254F;
            this.GroupFooter1.GroupUnion = DevExpress.XtraReports.UI.GroupFooterUnion.WithLastDetail;
            this.GroupFooter1.HeightF = 134.3551F;
            this.GroupFooter1.KeepTogether = true;
            this.GroupFooter1.Name = "GroupFooter1";
            this.GroupFooter1.StylePriority.UseBackColor = false;
            this.GroupFooter1.BeforePrint += new System.Drawing.Printing.PrintEventHandler(this.GroupFooter1_BeforePrint);
            // 
            // xrLabel23
            // 
            this.xrLabel23.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "DadosExtratoB.cSinalFAp")});
            this.xrLabel23.Dpi = 254F;
            this.xrLabel23.Font = new System.Drawing.Font("Courier New", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel23.FormattingRules.Add(this.NaoPlusSome);
            this.xrLabel23.LocationFloat = new DevExpress.Utils.PointFloat(1440F, 42.00006F);
            this.xrLabel23.Name = "xrLabel23";
            this.xrLabel23.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrLabel23.SizeF = new System.Drawing.SizeF(42F, 42F);
            this.xrLabel23.StylePriority.UseFont = false;
            this.xrLabel23.StylePriority.UseTextAlignment = false;
            this.xrLabel23.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomLeft;
            // 
            // NaoPlusSome
            // 
            this.NaoPlusSome.Condition = "([Parameters.comPLUS]  == False)    Or  ([SCCValorApF]  == 0)";
            // 
            // 
            // 
            this.NaoPlusSome.Formatting.Visible = DevExpress.Utils.DefaultBoolean.False;
            this.NaoPlusSome.Name = "NaoPlusSome";
            // 
            // xrValorDiaAPLSinal
            // 
            this.xrValorDiaAPLSinal.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "DadosExtratoB.cSinalFcc")});
            this.xrValorDiaAPLSinal.Dpi = 254F;
            this.xrValorDiaAPLSinal.Font = new System.Drawing.Font("Courier New", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrValorDiaAPLSinal.FormattingRules.Add(this.NaoPlusSome);
            this.xrValorDiaAPLSinal.LocationFloat = new DevExpress.Utils.PointFloat(1440F, 0F);
            this.xrValorDiaAPLSinal.Name = "xrValorDiaAPLSinal";
            this.xrValorDiaAPLSinal.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrValorDiaAPLSinal.SizeF = new System.Drawing.SizeF(42F, 42F);
            this.xrValorDiaAPLSinal.StylePriority.UseFont = false;
            this.xrValorDiaAPLSinal.StylePriority.UseTextAlignment = false;
            this.xrValorDiaAPLSinal.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomLeft;
            // 
            // xrLabel12
            // 
            this.xrLabel12.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "DadosExtratoB.cValorFAp", "{0:n2}")});
            this.xrLabel12.Dpi = 254F;
            this.xrLabel12.Font = new System.Drawing.Font("Courier New", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel12.FormattingRules.Add(this.NaoPlusSome);
            this.xrLabel12.LocationFloat = new DevExpress.Utils.PointFloat(1186F, 42.00006F);
            this.xrLabel12.Name = "xrLabel12";
            this.xrLabel12.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrLabel12.SizeF = new System.Drawing.SizeF(254F, 42F);
            this.xrLabel12.StylePriority.UseFont = false;
            this.xrLabel12.StylePriority.UseTextAlignment = false;
            this.xrLabel12.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomRight;
            // 
            // xrValorDiaAPL
            // 
            this.xrValorDiaAPL.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "DadosExtratoB.cValorFcc", "{0:n2}")});
            this.xrValorDiaAPL.Dpi = 254F;
            this.xrValorDiaAPL.Font = new System.Drawing.Font("Courier New", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrValorDiaAPL.FormattingRules.Add(this.NaoPlusSome);
            this.xrValorDiaAPL.LocationFloat = new DevExpress.Utils.PointFloat(1186F, 0F);
            this.xrValorDiaAPL.Name = "xrValorDiaAPL";
            this.xrValorDiaAPL.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrValorDiaAPL.SizeF = new System.Drawing.SizeF(254F, 42F);
            this.xrValorDiaAPL.StylePriority.UseFont = false;
            this.xrValorDiaAPL.StylePriority.UseTextAlignment = false;
            this.xrValorDiaAPL.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomRight;
            // 
            // xrSaldoPlus
            // 
            this.xrSaldoPlus.Dpi = 254F;
            this.xrSaldoPlus.Font = new System.Drawing.Font("Courier New", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrSaldoPlus.FormattingRules.Add(this.NaoPlusSome);
            this.xrSaldoPlus.LocationFloat = new DevExpress.Utils.PointFloat(504.9999F, 42.00002F);
            this.xrSaldoPlus.Name = "xrSaldoPlus";
            this.xrSaldoPlus.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrSaldoPlus.SizeF = new System.Drawing.SizeF(645.75F, 42F);
            this.xrSaldoPlus.StylePriority.UseFont = false;
            this.xrSaldoPlus.StylePriority.UseTextAlignment = false;
            this.xrSaldoPlus.Text = "Saldo Invest Plus";
            this.xrSaldoPlus.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomLeft;
            // 
            // xrSaldoCC
            // 
            this.xrSaldoCC.Dpi = 254F;
            this.xrSaldoCC.Font = new System.Drawing.Font("Courier New", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrSaldoCC.FormattingRules.Add(this.NaoPlusSome);
            this.xrSaldoCC.LocationFloat = new DevExpress.Utils.PointFloat(505F, 0F);
            this.xrSaldoCC.Name = "xrSaldoCC";
            this.xrSaldoCC.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrSaldoCC.SizeF = new System.Drawing.SizeF(645.75F, 42F);
            this.xrSaldoCC.StylePriority.UseFont = false;
            this.xrSaldoCC.StylePriority.UseTextAlignment = false;
            this.xrSaldoCC.Text = "Saldo Conta Corrente";
            this.xrSaldoCC.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomLeft;
            // 
            // xrValorTotalSinal
            // 
            this.xrValorTotalSinal.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "DadosExtratoB.cSinalF")});
            this.xrValorTotalSinal.Dpi = 254F;
            this.xrValorTotalSinal.Font = new System.Drawing.Font("Courier New", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrValorTotalSinal.LocationFloat = new DevExpress.Utils.PointFloat(1754.854F, 84.00004F);
            this.xrValorTotalSinal.Name = "xrValorTotalSinal";
            this.xrValorTotalSinal.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrValorTotalSinal.SizeF = new System.Drawing.SizeF(42F, 42F);
            this.xrValorTotalSinal.StylePriority.UseFont = false;
            this.xrValorTotalSinal.StylePriority.UseTextAlignment = false;
            this.xrValorTotalSinal.Text = "-";
            this.xrValorTotalSinal.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomLeft;
            // 
            // xrValorTotal
            // 
            this.xrValorTotal.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "DadosExtratoB.cValorF", "{0:n2}")});
            this.xrValorTotal.Dpi = 254F;
            this.xrValorTotal.Font = new System.Drawing.Font("Courier New", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrValorTotal.LocationFloat = new DevExpress.Utils.PointFloat(1500.854F, 84.00004F);
            this.xrValorTotal.Name = "xrValorTotal";
            this.xrValorTotal.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrValorTotal.SizeF = new System.Drawing.SizeF(254F, 42F);
            this.xrValorTotal.StylePriority.UseFont = false;
            this.xrValorTotal.StylePriority.UseTextAlignment = false;
            this.xrValorTotal.Text = "Valor Lanc.";
            this.xrValorTotal.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomRight;
            // 
            // xrSaldoTotal
            // 
            this.xrSaldoTotal.Dpi = 254F;
            this.xrSaldoTotal.Font = new System.Drawing.Font("Courier New", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrSaldoTotal.LocationFloat = new DevExpress.Utils.PointFloat(504.9999F, 84.00004F);
            this.xrSaldoTotal.Name = "xrSaldoTotal";
            this.xrSaldoTotal.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrSaldoTotal.SizeF = new System.Drawing.SizeF(645.75F, 42F);
            this.xrSaldoTotal.StylePriority.UseFont = false;
            this.xrSaldoTotal.StylePriority.UseTextAlignment = false;
            this.xrSaldoTotal.Text = "Saldo do Dia";
            this.xrSaldoTotal.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomLeft;
            // 
            // comPLUS
            // 
            this.comPLUS.Description = "Somar a conta plus";
            this.comPLUS.Name = "comPLUS";
            this.comPLUS.Type = typeof(bool);
            this.comPLUS.ValueInfo = "True";
            // 
            // ImpExtratoBal
            // 
            this.Bands.AddRange(new DevExpress.XtraReports.UI.Band[] {
            this.Detail,
            this.PageHeader,
            this.GroupHeader1,
            this.GroupFooter1});
            this.DataMember = "DadosExtratoB";
            this.DataSource = this.dImpExtratoB1;
            this.FormattingRuleSheet.AddRange(new DevExpress.XtraReports.UI.FormattingRule[] {
            this.NaoPlusSome});
            this.Parameters.AddRange(new DevExpress.XtraReports.Parameters.Parameter[] {
            this.comPLUS});
            this.Version = "14.2";
            this.Controls.SetChildIndex(this.GroupFooter1, 0);
            this.Controls.SetChildIndex(this.GroupHeader1, 0);
            this.Controls.SetChildIndex(this.PageHeader, 0);
            this.Controls.SetChildIndex(this.Detail, 0);
            ((System.ComponentModel.ISupportInitialize)(this.dImpExtratoB1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();

        }

        #endregion

        private DevExpress.XtraReports.UI.XRLabel xrLabel5;
        private DevExpress.XtraReports.UI.XRLabel xrCCTContaDigito;
        private DevExpress.XtraReports.UI.XRLabel xrLabel7;
        private DevExpress.XtraReports.UI.XRLabel xrCCTAgencia;
        /// <summary>
        /// 
        /// </summary>
        public DevExpress.XtraReports.UI.XRLabel Nomecondominio;
        private DevExpress.XtraReports.UI.XRLabel xrLabel16;
        private DevExpress.XtraReports.UI.XRLabel xrLabel26;
        private DevExpress.XtraReports.UI.XRLabel xrLabel29;
        private DevExpress.XtraReports.UI.XRLabel xrLabel28;
        private DevExpress.XtraReports.UI.XRLabel xrLabel13;
        private DevExpress.XtraReports.UI.XRLabel xrLabel17;
        private DevExpress.XtraReports.UI.XRLabel xrLabel15;
        /// <summary>
        /// 
        /// </summary>
        public DevExpress.XtraReports.UI.XRLabel DataInicial;
        private DevExpress.XtraReports.UI.XRLabel xrLabel11;
        private DevExpress.XtraReports.UI.XRLabel xrLabel10;
        private DevExpress.XtraReports.UI.XRLabel xrLabel14;
        private DevExpress.XtraReports.UI.XRLabel xrLabel1;
        /// <summary>
        /// 
        /// </summary>
        public DevExpress.XtraReports.UI.XRLabel DataFinal;
        private DevExpress.XtraReports.UI.GroupHeaderBand GroupHeader1;
        private DevExpress.XtraReports.UI.XRLabel xrDetDoc;
        private DevExpress.XtraReports.UI.XRLabel xrDetValor;
        private DevExpress.XtraReports.UI.XRLabel xrDetSinal;
        private DevExpress.XtraReports.UI.XRLabel xrDetData;
        private DevExpress.XtraReports.UI.XRLabel xrDetCodigo;
        private DevExpress.XtraReports.UI.XRLabel xrDetDescricao;
        private DevExpress.XtraReports.UI.GroupFooterBand GroupFooter1;
        private DevExpress.XtraReports.UI.XRLabel xrValorTotalSinal;
        private DevExpress.XtraReports.UI.XRLabel xrValorTotal;
        private DevExpress.XtraReports.UI.XRLabel xrSaldoTotal;
        /// <summary>
        /// 
        /// </summary>
        public dImpExtratoB dImpExtratoB1;
        private DevExpress.XtraReports.Parameters.Parameter comPLUS;
        private DevExpress.XtraReports.UI.FormattingRule NaoPlusSome;
        private DevExpress.XtraReports.UI.XRLabel xrSaldoPlus;
        private DevExpress.XtraReports.UI.XRLabel xrSaldoCC;
        private DevExpress.XtraReports.UI.XRLabel xrLabel23;
        private DevExpress.XtraReports.UI.XRLabel xrValorDiaAPLSinal;
        private DevExpress.XtraReports.UI.XRLabel xrLabel12;
        private DevExpress.XtraReports.UI.XRLabel xrValorDiaAPL;
    }
}
