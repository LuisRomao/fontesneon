using System;

namespace dllImpresso.ExtratoBancario
{
    /// <summary>
    /// 
    /// </summary>
    public partial class ImpExtratoB : DevExpress.XtraReports.UI.XtraReport
    {
        /// <summary>
        /// 
        /// </summary>
        public ImpExtratoB()
        {
            InitializeComponent();
        }

        private int condominio = 0;
        private int tipo = 0;
        private bool avisar = false;

        /// <summary>
        /// 
        /// </summary>
        public int Apontadas = 0;

        /// <summary>
        /// Define os dados para o apontamento de impress�o
        /// </summary>
        /// <param name="CON">Condom�nio. Se for -1 n�o aponta</param>
        /// <param name="Tipo"></param>
        /// <param name="Avisar"></param>
        public void Apontamento(int CON, int Tipo,bool Avisar)
        {
            condominio = CON;
            tipo = Tipo;
            avisar = Avisar;
        }

        private void ImpExtratoB_AfterPrint(object sender, EventArgs e)
        {
            Apontadas = 0;
            if (tipo > 0)
                if (condominio > 0)
                    Apontadas = Framework.DSCentral.IMPressoesTableAdapter.Incluir(condominio, tipo, Pages.Count, DateTime.Now, CompontesBasicos.Bancovirtual.UsuarioLogado.UsuarioLogadoSt.USU, "Autom�tico", false);
            if ((Apontadas > 0) && (avisar))
                System.Windows.Forms.MessageBox.Show("Apontadas: " + Pages.Count.ToString());
        }


    }
}
