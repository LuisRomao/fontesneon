using System;
using System.Drawing;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using DevExpress.XtraReports.UI;

namespace dllImpresso
{
    public partial class ImpBoletoReciboSV : dllImpresso.ImpSegundaVia
    {
        /// <summary>
        /// Ajusta data do pagamento
        /// </summary>
        /// <param name="DataPag">Data do pagamento do recido</param>
        public void SetaDataPag(DateTime DataPag){
                xrDataPag.Text = DataPag.ToString("dd/MM/yy");
                //xrDataPag.Font = xrLabel6.Font;
                //xrDataPag.Top = xrLabel6.Top;
                //xrDataPag.Left = xrLabel6.Left;
                //xrDataPag.Width = xrLabel7.Width;
                //xrDataPag.Height = xrLabel6.Height;
        }


        /// <summary>
        /// construtor padr�o
        /// </summary>
        public ImpBoletoReciboSV()
        {
            InitializeComponent();
            //if (!this.DesignMode)
            //{
                
                //DataSubstituta.Font = xrLabel6.Font;
                //DataSubstituta.Top = xrLabel6.Top;
                //DataSubstituta.Left = xrLabel6.Left;
                //DataSubstituta.Width = xrLabel7.Width;
                //DataSubstituta.Height = xrLabel6.Height;               
            //};
        }

        private void CantoRedondo(object sender, DrawEventArgs e)
        {
            CantosRedondos(sender, e, 30);
        }

        private void DrawDegrade(object sender, DrawEventArgs e)
        {
            Degrade(sender, e);
        }
    }
}

