using System.Data;
using DevExpress.XtraReports.UI;
using VirEnumeracoesNeon;

namespace dllImpresso
{
    /// <summary>
    /// 
    /// </summary>
    public partial class ImpProtocolo : dllImpresso.ImpLogoCond
    {
        private static ImpProtocolo _ImpProtocoloSt;

        /// <summary>
        /// 
        /// </summary>
        public static ImpProtocolo ImpProtocoloSt {
            get {
                if (_ImpProtocoloSt == null)
                    _ImpProtocoloSt = new ImpProtocolo();
                return _ImpProtocoloSt;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        public ImpProtocolo()
        {
            InitializeComponent();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="Documento"></param>
        public void SetDocumento(string Documento) {
            xrDocumento.Text = Documento;
        }

        private void ImpProtocolo_DataSourceRowChanged(object sender, DataSourceRowEventArgs e)
        {
            DataRowView DRV = (DataRowView)bindingSource1[e.CurrentRow];
            dImpProtocolo.DadosProtRow row = (dImpProtocolo.DadosProtRow)((System.Data.DataRow)DRV.Row);
            switch(row.DA_BCO)
            {                
                case 237:
                    xrPictureBoxBradesco.Visible = true;
                    break;                
                default:
                    xrPictureBoxBradesco.Visible = false;
                    break;
            }
            switch ((FormaPagamento)row.Tipo)
            {
                case FormaPagamento.EntreigaPessoal:
                case FormaPagamento.EntreigaPessoalEemail:
                    xrPictureBoxEmail.Visible = Endereco.Visible = xrPictureBox2.Visible = false;
                    Linha.Visible = true;
                    break;
                case FormaPagamento.Correio:
                case FormaPagamento.CorreioEemail:
                    Endereco.Visible = xrPictureBox2.Visible = true;
                    xrPictureBoxEmail.Visible = false;
                    Linha.Visible = false;
                    break;
                case FormaPagamento.email_Correio:
                case FormaPagamento.email_EntreigaPessoal:
                    Endereco.Visible = true;
                    xrPictureBox2.Visible = false;
                    xrPictureBoxEmail.Visible = true;
                    xrPictureBoxEmail.LeftF = xrPictureBox2.LeftF;
                    Linha.Visible = false;
                    break;
                case FormaPagamento.Dentro_DeSuper:                
                    Endereco.Visible = true;
                    xrPictureBox2.Visible = false;
                    xrPictureBoxEmail.Visible = false;
                    xrPictureBoxEmail.Visible = false;
                    Linha.Visible = false;
                    break;
                default:
                    break;
            }
            //bool visivel = (row.PESEndereco.ToString().Trim() != "Entrega Pessoal");
            //Endereco.Visible = xrPictureBox2.Visible = visivel;
            //xrPictureBoxEmail.Visible = false;
            //Linha.Visible = !visivel;
            bool visivel = (row.BLOCO.ToString() != "SB") && (row.BLOCO.ToString() != "");
            xrBLOCO.Visible = xrBLOCO1.Visible = visivel;
        }
    }
}

