﻿namespace Boletos.Boleto.Impresso {


    partial class dImpBoletoBal
    {
        partial class DadosBoletoDataTable
        {
        }

        private dImpBoletoBalTableAdapters.InadimplentesTableAdapter inadimplentesTableAdapter;
        private dImpBoletoBalTableAdapters.ConsumoTableAdapter consumoTableAdapter;

        public dImpBoletoBalTableAdapters.ConsumoTableAdapter ConsumoTableAdapter {
            get {
                if (consumoTableAdapter == null) {
                    consumoTableAdapter = new Boletos.Boleto.Impresso.dImpBoletoBalTableAdapters.ConsumoTableAdapter();
                    consumoTableAdapter.TrocarStringDeConexao();
                };
                return consumoTableAdapter;
            }
        }

        public dImpBoletoBalTableAdapters.InadimplentesTableAdapter InadimplentesTableAdapter
        {
            get {
                if (inadimplentesTableAdapter == null) { 
                    inadimplentesTableAdapter = new Boletos.Boleto.Impresso.dImpBoletoBalTableAdapters.InadimplentesTableAdapter();
                    inadimplentesTableAdapter.TrocarStringDeConexao(CompontesBasicos.Bancovirtual.TiposDeBanco.Access);
                };
                return inadimplentesTableAdapter; 
            }            
        }

        //private Impresso.dImpBoletoBalTableAdapters.EMPRESASTableAdapter eMPRESASTableAdapter;
        public string MensagemRateio(int BOL) {
            dImpBoletoBalTableAdapters.RATeiosMenBolTableAdapter RATeiosMenBolTableAdapter = new Boletos.Boleto.Impresso.dImpBoletoBalTableAdapters.RATeiosMenBolTableAdapter();
            RATeiosMenBolTableAdapter.TrocarStringDeConexao();
            RATeiosMenBolTableAdapter.Fill(RATeiosMenBol, BOL);
            string Retorno = "";
            foreach (RATeiosMenBolRow RATeiosMenBolRow in RATeiosMenBol)
                if(!RATeiosMenBolRow.IsRATHistoricoNull())
                    Retorno += ("\r\n" + RATeiosMenBolRow.RATHistorico + "\r\n");
            return Retorno;
        }
    }
}
