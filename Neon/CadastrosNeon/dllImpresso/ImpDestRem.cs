using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using DevExpress.XtraReports.UI;
using System.Drawing.Printing;
using DevExpress.XtraPrinting;
using System.Drawing.Drawing2D;

namespace dllImpresso
{
    /// <summary>
    /// Componente base para impress�o de carta dobrada
    /// </summary>
    public partial class ImpDestRem : ImpBase
    {
        private bool _ComRemetente = true;

        

        /// <summary>
        /// Se � frente e verso
        /// </summary>
        [Category("Virtual Software"), Description("Se � frente e verso")]
        [DefaultValue(typeof(bool), "True")]
        [DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
        public bool ComRemetente {
            get {
                return _ComRemetente;
            }
            set {
                _ComRemetente = value;
            }
        }

        

        /// <summary>
        /// Construtor
        /// </summary>
        public ImpDestRem()
        {
            if (!this.DesignMode)
            {
                try
                {

                    if (CompontesBasicos.Bancovirtual.UsuarioLogado.ImpressoraBoletos != null)
                        foreach (String strPrinter in PrinterSettings.InstalledPrinters)
                            if (strPrinter.ToUpper() == CompontesBasicos.Bancovirtual.UsuarioLogado.ImpressoraBoletos)
                                PrinterName = strPrinter;
                }
                catch { }
            };
            InitializeComponent();
            Apontador = new ApontadorImp();
            if (!this.DesignMode)
            {
                try
                {
                    this.PrintingSystem.StartPrint += new DevExpress.XtraPrinting.PrintDocumentEventHandler(PrintingSystem_StartPrint);
                    bindingSource1.DataSource = Framework.DSCentral.DCentral.EMPRESASST;
                    string Sigla = CompontesBasicos.Bancovirtual.UsuarioLogado.UsuarioLogadoSt.USU.ToString();
                    char Anterior = ' ';
                    Array.ForEach(CompontesBasicos.Bancovirtual.UsuarioLogado.UsuarioLogadoSt.USUNome.ToCharArray(), delegate(char c) 
                    {
                        if (char.IsWhiteSpace(Anterior))
                            Sigla += c.ToString().ToUpper();
                        Anterior = c;
                    });

                    xrSigla1.Text = xrSigla.Text = Sigla; 
                }
                catch { 
                }
            }            
        }

        /// <summary>
        /// Fun��o para omitir o apartamento na capa do impresso
        /// </summary>
        public void OmitirApartamento() {
            xrLabel88.Visible = xrLabel89.Visible = xrLabel90.Visible = xrLabel91.Visible = false;
        }

        private void PrintingSystem_StartPrint(object sender, DevExpress.XtraPrinting.PrintDocumentEventArgs e)
        {
            if (_ComRemetente)
                e.PrintDocument.PrinterSettings.Duplex = System.Drawing.Printing.Duplex.Horizontal;
            else
                Detail.Height = QuadroPrincipal.Height;
        }

        #region Fun�oes DRAW

        /// <summary>
        /// Funcao para ser chamada a partir do DRAW. Desenha os cantos redondos
        /// </summary>
        /// <param name="sender">Chamados, repassar o do DRAW</param>
        /// <param name="e">Repassar o do DRAW</param>
        /// <param name="Raio">Raio do canto</param>
        protected void CantosRedondos(object sender, DrawEventArgs e, Int32 Raio)
        {
            ImpGeradorDeEfeitos.CantosRedondos(sender, e, Raio);
            return;            
        }

        /// <summary>
        /// Funcao para ser chamada a partir do DRAW. Desenha o fundo em degrade
        /// </summary>
        /// <param name="sender">Repassar o do DRAW</param>
        /// <param name="e">Repassar o do DRAW</param>
        protected void Degrade(object sender, DrawEventArgs e)
        {
            ImpGeradorDeEfeitos.Degrade(sender, e,ImpGeradorDeEfeitos.TipoDraw.Infla);
            return;            
        } 
        #endregion

        private ApontadorImp Apontador;

        /// <summary>
        /// Define os dados para o apontamento de impress�o
        /// </summary>
        /// <param name="CON">Condom�nio. Se for -1 n�o aponta</param>
        /// <param name="Tipo"></param>
        public void Apontamento(int CON, int Tipo) {
            Apontador.condominio = CON;
            Apontador.tipo = Tipo;
        }

        private void ImpDestRem_AfterPrint(object sender, EventArgs e)
        {
            Apontador.Apontar(Pages.Count);                
        }

        private void ImpDestRem_BeforePrint(object sender, PrintEventArgs e)
        {
            if ((!_ComRemetente) && (QuadroSegundaPagina != null))
            {
                QuadroSegundaPagina.Dispose();
                QuadroEndereco.Dispose();
                QuadroRemetente.Dispose();
                QuadroSegundaPagina = QuadroEndereco = QuadroRemetente = null;
                Detail.Height = QuadroPrincipal.Height;
            }
        }

        /// <summary>
        /// Ajusta um destinat�rio
        /// </summary>
        /// <param name="apartamento"></param>
        public void AjustaDestinatario(AbstratosNeon.ABS_Apartamento apartamento)
        {
            dImpDestRem.CONDOMINIOSRow row = dImpDestRem1.CONDOMINIOS.Count == 0 ? dImpDestRem1.CONDOMINIOS.NewCONDOMINIOSRow() : dImpDestRem1.CONDOMINIOS[0];
            row.CONNome = apartamento.Condominio.Nome;
            row.CONEndereco = "CON ENDERECO";
            row.BLOCodigo = apartamento.BLOCodigo;
            row.APTNumero = apartamento.APTNumero;
            row.BOLEndereco = apartamento.PESEndereco(null, true, true);
            row.BOLNome = apartamento.PESNome(true);
            row.CONCodigo = apartamento.CONCodigo;
            row.CONCodigoFolha1 = apartamento.Condominio.CONCodigoFolha1.GetValueOrDefault(0);
            row.CONNomeBloco = apartamento.Condominio.CONNomeBloco;
            row.CONNomeApto = apartamento.Condominio.CONNomeApto;
            row.CONOcultaSenha = true;
            if (row.RowState == System.Data.DataRowState.Detached)
                dImpDestRem1.CONDOMINIOS.AddCONDOMINIOSRow(row);
            row.AcceptChanges();
        }
        
    }
}
