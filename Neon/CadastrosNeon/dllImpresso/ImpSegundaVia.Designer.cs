namespace dllImpresso
{
    /// <summary>
    /// Impresso de boleto com segunda via
    /// </summary>
    partial class ImpSegundaVia
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(ImpSegundaVia));
            this.QuadroComposicaoBoleto = new DevExpress.XtraReports.UI.XRPanel();
            this.Lista11 = new DevExpress.XtraReports.UI.XRLabel();
            this.Lista12 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel120 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel116 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTitulo = new DevExpress.XtraReports.UI.XRLabel();
            this.QuadroMensagem = new DevExpress.XtraReports.UI.XRRichText();
            ((System.ComponentModel.ISupportInitialize)(this.dImpBoletoBase1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dImpDestRem1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.bindingSourcePrincipal)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.bindingSource1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.QuadroMensagem)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            // 
            // xrLabelUsoBanco
            // 
            this.xrLabelUsoBanco.StylePriority.UseBorders = false;
            // 
            // xrLabelCarteira
            // 
            this.xrLabelCarteira.StylePriority.UseBorders = false;
            // 
            // xrLabelEspecie
            // 
            this.xrLabelEspecie.StylePriority.UseBorders = false;
            // 
            // BANCOLocal
            // 
            this.BANCOLocal.StylePriority.UseBorders = false;
            // 
            // BANCONumero
            // 
            this.BANCONumero.StylePriority.UseFont = false;
            // 
            // QuadroUtil
            // 
            this.QuadroUtil.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.QuadroMensagem,
            this.xrTitulo,
            this.QuadroComposicaoBoleto});
            // 
            // rLabelcompet
            // 
            this.rLabelcompet.StylePriority.UseBorders = false;
            // 
            // xrLabel3
            // 
            this.xrLabel3.StylePriority.UseBorders = false;                        
            // 
            // QuadroSegundaPagina
            // 
            this.QuadroSegundaPagina.StylePriority.UseBackColor = false;
            // 
            // Detail
            // 
            this.Detail.StylePriority.UseTextAlignment = false;
            // 
            // xrSigla1
            // 
            this.xrSigla1.StylePriority.UseFont = false;
            this.xrSigla1.StylePriority.UseTextAlignment = false;
            // 
            // QuadroComposicaoBoleto
            // 
            this.QuadroComposicaoBoleto.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.Lista11,
            this.Lista12,
            this.xrLabel120,
            this.xrLabel116});
            this.QuadroComposicaoBoleto.Dpi = 254F;
            this.QuadroComposicaoBoleto.LocationFloat = new DevExpress.Utils.PointFloat(487F, 275F);
            this.QuadroComposicaoBoleto.Name = "QuadroComposicaoBoleto";
            this.QuadroComposicaoBoleto.SizeF = new System.Drawing.SizeF(863F, 132F);
            // 
            // Lista11
            // 
            this.Lista11.BorderColor = System.Drawing.Color.Silver;
            this.Lista11.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.Lista11.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "Composicao")});
            this.Lista11.Dpi = 254F;
            this.Lista11.Font = new System.Drawing.Font("Tahoma", 6.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Lista11.LocationFloat = new DevExpress.Utils.PointFloat(0F, 37F);
            this.Lista11.Multiline = true;
            this.Lista11.Name = "Lista11";
            this.Lista11.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.Lista11.SizeF = new System.Drawing.SizeF(730F, 59F);
            this.Lista11.Text = "Lista11";
            // 
            // Lista12
            // 
            this.Lista12.BackColor = System.Drawing.Color.WhiteSmoke;
            this.Lista12.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "ComposicaoTotal")});
            this.Lista12.Dpi = 254F;
            this.Lista12.Font = new System.Drawing.Font("Tahoma", 6.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Lista12.LocationFloat = new DevExpress.Utils.PointFloat(730F, 37F);
            this.Lista12.Multiline = true;
            this.Lista12.Name = "Lista12";
            this.Lista12.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.Lista12.SizeF = new System.Drawing.SizeF(126F, 91F);
            this.Lista12.Text = "Lista12";
            this.Lista12.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight;
            this.Lista12.WordWrap = false;
            this.Lista12.Draw += new DevExpress.XtraReports.UI.DrawEventHandler(this.Lista12_Draw);
            // 
            // xrLabel120
            // 
            this.xrLabel120.BackColor = System.Drawing.Color.DarkGray;
            this.xrLabel120.Dpi = 254F;
            this.xrLabel120.Font = new System.Drawing.Font("Verdana", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel120.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.xrLabel120.LocationFloat = new DevExpress.Utils.PointFloat(0F, 0F);
            this.xrLabel120.Name = "xrLabel120";
            this.xrLabel120.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrLabel120.SizeF = new System.Drawing.SizeF(857F, 37F);
            this.xrLabel120.Text = "COMPOSIÇÃO DO BOLETO";
            this.xrLabel120.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter;
            this.xrLabel120.Draw += new DevExpress.XtraReports.UI.DrawEventHandler(this.xrLabel120_Draw);
            // 
            // xrLabel116
            // 
            this.xrLabel116.Dpi = 254F;
            this.xrLabel116.Font = new System.Drawing.Font("Tahoma", 6.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel116.LocationFloat = new DevExpress.Utils.PointFloat(587F, 101F);
            this.xrLabel116.Name = "xrLabel116";
            this.xrLabel116.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrLabel116.SizeF = new System.Drawing.SizeF(143F, 26F);
            this.xrLabel116.Text = "TOTAL";
            this.xrLabel116.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight;
            // 
            // xrTitulo
            // 
            this.xrTitulo.Dpi = 254F;
            this.xrTitulo.Font = new System.Drawing.Font("Times New Roman", 20.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrTitulo.ForeColor = System.Drawing.Color.DarkGray;
            this.xrTitulo.LocationFloat = new DevExpress.Utils.PointFloat(21F, 85F);
            this.xrTitulo.Name = "xrTitulo";
            this.xrTitulo.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTitulo.SizeF = new System.Drawing.SizeF(1762F, 103F);
            this.xrTitulo.Text = "Segunda Via";
            this.xrTitulo.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter;
            // 
            // QuadroMensagem
            // 
            this.QuadroMensagem.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Rtf", null, "BOLMensagemImp")});
            this.QuadroMensagem.Dpi = 254F;
            this.QuadroMensagem.Font = new System.Drawing.Font("Times New Roman", 9.75F);
            this.QuadroMensagem.LocationFloat = new DevExpress.Utils.PointFloat(20.99999F, 417.4164F);
            this.QuadroMensagem.Name = "QuadroMensagem";
            this.QuadroMensagem.SerializableRtfString = resources.GetString("QuadroMensagem.SerializableRtfString");
            this.QuadroMensagem.SizeF = new System.Drawing.SizeF(1771F, 58.42004F);
            // 
            // ImpSegundaVia
            // 
            this.Bands.AddRange(new DevExpress.XtraReports.UI.Band[] {
            this.Detail,
            this.bottomMarginBand1});
            this.FormattingRuleSheet.AddRange(new DevExpress.XtraReports.UI.FormattingRule[] {
            this.RuleOcultaUsu});
            this.Version = "13.2";
            this.DataSourceRowChanged += new DevExpress.XtraReports.UI.DataSourceRowEventHandler(this.ImpSegundaVia_DataSourceRowChanged);
            ((System.ComponentModel.ISupportInitialize)(this.dImpBoletoBase1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dImpDestRem1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.bindingSourcePrincipal)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.bindingSource1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.QuadroMensagem)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();

        }

        #endregion

        private DevExpress.XtraReports.UI.XRLabel Lista11;
        private DevExpress.XtraReports.UI.XRLabel Lista12;
        private DevExpress.XtraReports.UI.XRLabel xrLabel120;
        private DevExpress.XtraReports.UI.XRLabel xrLabel116;
        /// <summary>
        /// 
        /// </summary>
        public DevExpress.XtraReports.UI.XRLabel xrTitulo;
        /// <summary>
        /// 
        /// </summary>
        protected DevExpress.XtraReports.UI.XRPanel QuadroComposicaoBoleto;
        /// <summary>
        /// Quadro de mensagens
        /// </summary>
        public DevExpress.XtraReports.UI.XRRichText QuadroMensagem;

    }
}