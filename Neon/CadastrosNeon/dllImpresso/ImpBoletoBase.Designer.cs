namespace dllImpresso
{
    /// <summary>
    /// Impresso base para os boletos
    /// </summary>
    partial class ImpBoletoBase
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(ImpBoletoBase));
            this.QuadroTopo = new DevExpress.XtraReports.UI.XRPanel();
            this.QuadroDadosTopo = new DevExpress.XtraReports.UI.XRPanel();
            this.xrLEndBenefComp = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLEndBenef = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabelCab = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabelDOC2 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabelDOC1 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrCONNome = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel48 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel47 = new DevExpress.XtraReports.UI.XRLabel();
            this.NNSTR = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel45 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabelBeneficiario = new DevExpress.XtraReports.UI.XRLabel();
            this.LabApt3 = new DevExpress.XtraReports.UI.XRLabel();
            this.LabBlo3 = new DevExpress.XtraReports.UI.XRLabel();
            this.LabApt4 = new DevExpress.XtraReports.UI.XRLabel();
            this.LabBlo4 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrCompetencia = new DevExpress.XtraReports.UI.XRLabel();
            this.RegraCompetencia = new DevExpress.XtraReports.UI.FormattingRule();
            this.xrLabelrecsacado = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel8 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabelcompet1 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel9 = new DevExpress.XtraReports.UI.XRLabel();
            this.Valor_maior_99_mil = new DevExpress.XtraReports.UI.FormattingRule();
            this.xrLabel7 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel6 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrPictureBox1 = new DevExpress.XtraReports.UI.XRPictureBox();
            this.xrLabel1x = new DevExpress.XtraReports.UI.XRLabel();
            this.QuadoBoleto = new DevExpress.XtraReports.UI.XRPanel();
            this.Panel_SEMCPF = new DevExpress.XtraReports.UI.XRPanel();
            this.Label_SEMCPF = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLogoBanco33 = new DevExpress.XtraReports.UI.XRPictureBox();
            this.xrLogoBanco = new DevExpress.XtraReports.UI.XRPictureBox();
            this.NamePago = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel33 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel32 = new DevExpress.XtraReports.UI.XRLabel();
            this.CodigoDeBarras = new DevExpress.XtraReports.UI.XRLabel();
            this.xrPanel7 = new DevExpress.XtraReports.UI.XRPanel();
            this.xrPanel36 = new DevExpress.XtraReports.UI.XRPanel();
            this.xrLabel85 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel84 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrPanel24 = new DevExpress.XtraReports.UI.XRPanel();
            this.xrLabel103 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabelUsoBanco = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabelCarteira = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLine9 = new DevExpress.XtraReports.UI.XRLine();
            this.xrLine8 = new DevExpress.XtraReports.UI.XRLine();
            this.xrLine7 = new DevExpress.XtraReports.UI.XRLine();
            this.xrLine6 = new DevExpress.XtraReports.UI.XRLine();
            this.xrLabel108 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel107 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel106 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrEspecie = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel104 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel82 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrPanel23 = new DevExpress.XtraReports.UI.XRPanel();
            this.xrLabel3 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabelEspecie = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel81 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLine5 = new DevExpress.XtraReports.UI.XRLine();
            this.xrLine4 = new DevExpress.XtraReports.UI.XRLine();
            this.xrLine3 = new DevExpress.XtraReports.UI.XRLine();
            this.xrLine2 = new DevExpress.XtraReports.UI.XRLine();
            this.xrLabel102 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel101 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel100 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel99 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel98 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel79 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel80 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrPanel19 = new DevExpress.XtraReports.UI.XRPanel();
            this.NNSTR2 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel73 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrPanel18 = new DevExpress.XtraReports.UI.XRPanel();
            this.xrLabel71 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel70 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrPanel17 = new DevExpress.XtraReports.UI.XRPanel();
            this.xrLabel69 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrPanel16 = new DevExpress.XtraReports.UI.XRPanel();
            this.xrLabel67 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrPanel15 = new DevExpress.XtraReports.UI.XRPanel();
            this.xrLabel65 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrPanel14 = new DevExpress.XtraReports.UI.XRPanel();
            this.xrLabel63 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrPanel13 = new DevExpress.XtraReports.UI.XRPanel();
            this.xrLabel61 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrPanel12 = new DevExpress.XtraReports.UI.XRPanel();
            this.xrAgencia = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel59 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrPanel11 = new DevExpress.XtraReports.UI.XRPanel();
            this.xrLabelAvalista = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel131 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrProprietario = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel124 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel123 = new DevExpress.XtraReports.UI.XRLabel();
            this.LabBlo2 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel119 = new DevExpress.XtraReports.UI.XRLabel();
            this.LabBlo1 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrCompetencia1 = new DevExpress.XtraReports.UI.XRLabel();
            this.rLabelcompet = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel112 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel111 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabelCPFPagador = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel109 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrPanel10 = new DevExpress.XtraReports.UI.XRPanel();
            this.xrLabel36 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel35 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrPanel22 = new DevExpress.XtraReports.UI.XRPanel();
            this.xrLabel77 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel78 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrPanel8 = new DevExpress.XtraReports.UI.XRPanel();
            this.xrLabel34 = new DevExpress.XtraReports.UI.XRLabel();
            this.BANCOLocal = new DevExpress.XtraReports.UI.XRLabel();
            this.LinhaDigitavel = new DevExpress.XtraReports.UI.XRLabel();
            this.BANCONumero = new DevExpress.XtraReports.UI.XRLabel();
            this.BANCONome = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLine1 = new DevExpress.XtraReports.UI.XRLine();
            this.xrLabel2x = new DevExpress.XtraReports.UI.XRLabel();
            this.QuadroAutenticacao = new DevExpress.XtraReports.UI.XRPanel();
            this.xrLabel10 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabelPagEnder = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabelNomePagador = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabelPagador = new DevExpress.XtraReports.UI.XRLabel();
            this.xrPanel5 = new DevExpress.XtraReports.UI.XRPanel();
            this.xrLabel5 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel4 = new DevExpress.XtraReports.UI.XRLabel();
            this.RuleOcultaUsu = new DevExpress.XtraReports.UI.FormattingRule();
            this.QuadroUtil = new DevExpress.XtraReports.UI.XRPanel();
            this.dImpBoletoBase1 = new dllImpresso.dImpBoletoBase();
            ((System.ComponentModel.ISupportInitialize)(this.dImpDestRem1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.bindingSourcePrincipal)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.bindingSource1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dImpBoletoBase1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            // 
            // QuadroPrincipal
            // 
            this.QuadroPrincipal.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.QuadroUtil,
            this.QuadroAutenticacao,
            this.QuadoBoleto,
            this.QuadroTopo});
            // 
            // QuadroSegundaPagina
            // 
            this.QuadroSegundaPagina.StylePriority.UseBackColor = false;
            // 
            // bindingSourcePrincipal
            // 
            this.bindingSourcePrincipal.DataMember = "BOLETOSIMP";
            this.bindingSourcePrincipal.DataSource = this.dImpBoletoBase1;
            // 
            // Detail
            // 
            this.Detail.StylePriority.UseTextAlignment = false;
            // 
            // xrSigla1
            // 
            this.xrSigla1.StylePriority.UseFont = false;
            this.xrSigla1.StylePriority.UseTextAlignment = false;
            // 
            // QuadroTopo
            // 
            this.QuadroTopo.CanGrow = false;
            this.QuadroTopo.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.QuadroDadosTopo,
            this.xrPictureBox1});
            this.QuadroTopo.Dpi = 254F;
            this.QuadroTopo.LocationFloat = new DevExpress.Utils.PointFloat(0F, 0F);
            this.QuadroTopo.Name = "QuadroTopo";
            this.QuadroTopo.SizeF = new System.Drawing.SizeF(1799F, 148F);
            this.QuadroTopo.StylePriority.UseBackColor = false;
            // 
            // QuadroDadosTopo
            // 
            this.QuadroDadosTopo.BorderColor = System.Drawing.Color.Silver;
            this.QuadroDadosTopo.CanGrow = false;
            this.QuadroDadosTopo.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrLEndBenefComp,
            this.xrLEndBenef,
            this.xrLabelCab,
            this.xrLabelDOC2,
            this.xrLabelDOC1,
            this.xrCONNome,
            this.xrLabel48,
            this.xrLabel47,
            this.NNSTR,
            this.xrLabel45,
            this.xrLabelBeneficiario,
            this.LabApt3,
            this.LabBlo3,
            this.LabApt4,
            this.LabBlo4,
            this.xrCompetencia,
            this.xrLabelrecsacado,
            this.xrLabel8,
            this.xrLabelcompet1,
            this.xrLabel9,
            this.xrLabel7,
            this.xrLabel6});
            this.QuadroDadosTopo.Dpi = 254F;
            this.QuadroDadosTopo.LocationFloat = new DevExpress.Utils.PointFloat(313F, 0F);
            this.QuadroDadosTopo.Name = "QuadroDadosTopo";
            this.QuadroDadosTopo.SizeF = new System.Drawing.SizeF(1486F, 148F);
            this.QuadroDadosTopo.StylePriority.UseBackColor = false;
            // 
            // xrLEndBenefComp
            // 
            this.xrLEndBenefComp.CanGrow = false;
            this.xrLEndBenefComp.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "CONEnderecoComple")});
            this.xrLEndBenefComp.Dpi = 254F;
            this.xrLEndBenefComp.Font = new System.Drawing.Font("Tahoma", 5.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLEndBenefComp.LocationFloat = new DevExpress.Utils.PointFloat(0F, 51.5F);
            this.xrLEndBenefComp.Name = "xrLEndBenefComp";
            this.xrLEndBenefComp.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrLEndBenefComp.SizeF = new System.Drawing.SizeF(567.9363F, 18.5F);
            this.xrLEndBenefComp.StylePriority.UseFont = false;
            // 
            // xrLEndBenef
            // 
            this.xrLEndBenef.CanGrow = false;
            this.xrLEndBenef.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "CONEndereco")});
            this.xrLEndBenef.Dpi = 254F;
            this.xrLEndBenef.Font = new System.Drawing.Font("Tahoma", 5.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLEndBenef.LocationFloat = new DevExpress.Utils.PointFloat(0.1311109F, 33F);
            this.xrLEndBenef.Name = "xrLEndBenef";
            this.xrLEndBenef.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrLEndBenef.SizeF = new System.Drawing.SizeF(567.9363F, 18.5F);
            this.xrLEndBenef.StylePriority.UseFont = false;
            // 
            // xrLabelCab
            // 
            this.xrLabelCab.CanGrow = false;
            this.xrLabelCab.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "CONCNPJ")});
            this.xrLabelCab.Dpi = 254F;
            this.xrLabelCab.Font = new System.Drawing.Font("Tahoma", 5.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabelCab.LocationFloat = new DevExpress.Utils.PointFloat(280.1311F, 73F);
            this.xrLabelCab.Name = "xrLabelCab";
            this.xrLabelCab.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrLabelCab.SizeF = new System.Drawing.SizeF(288.1014F, 25F);
            this.xrLabelCab.StylePriority.UseFont = false;
            // 
            // xrLabelDOC2
            // 
            this.xrLabelDOC2.CanGrow = false;
            this.xrLabelDOC2.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "BOL")});
            this.xrLabelDOC2.Dpi = 254F;
            this.xrLabelDOC2.Font = new System.Drawing.Font("Tahoma", 6.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabelDOC2.LocationFloat = new DevExpress.Utils.PointFloat(389.3299F, 123F);
            this.xrLabelDOC2.Name = "xrLabelDOC2";
            this.xrLabelDOC2.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrLabelDOC2.SizeF = new System.Drawing.SizeF(178.6064F, 25F);
            // 
            // xrLabelDOC1
            // 
            this.xrLabelDOC1.CanGrow = false;
            this.xrLabelDOC1.Dpi = 254F;
            this.xrLabelDOC1.Font = new System.Drawing.Font("Tahoma", 5.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabelDOC1.LocationFloat = new DevExpress.Utils.PointFloat(342F, 123F);
            this.xrLabelDOC1.Multiline = true;
            this.xrLabelDOC1.Name = "xrLabelDOC1";
            this.xrLabelDOC1.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrLabelDOC1.SizeF = new System.Drawing.SizeF(47F, 25F);
            this.xrLabelDOC1.StylePriority.UseBackColor = false;
            this.xrLabelDOC1.StylePriority.UseFont = false;
            this.xrLabelDOC1.StylePriority.UseTextAlignment = false;
            this.xrLabelDOC1.Text = "Doc";
            this.xrLabelDOC1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomLeft;
            // 
            // xrCONNome
            // 
            this.xrCONNome.CanGrow = false;
            this.xrCONNome.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "CONNome")});
            this.xrCONNome.Dpi = 254F;
            this.xrCONNome.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrCONNome.LocationFloat = new DevExpress.Utils.PointFloat(121.1311F, 0F);
            this.xrCONNome.Name = "xrCONNome";
            this.xrCONNome.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrCONNome.SizeF = new System.Drawing.SizeF(1138.886F, 33F);
            this.xrCONNome.StylePriority.UseFont = false;
            this.xrCONNome.Text = "xrCONNome";
            this.xrCONNome.WordWrap = false;
            // 
            // xrLabel48
            // 
            this.xrLabel48.CanGrow = false;
            this.xrLabel48.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "Calc_AgCedente")});
            this.xrLabel48.Dpi = 254F;
            this.xrLabel48.Font = new System.Drawing.Font("Tahoma", 6.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel48.LocationFloat = new DevExpress.Utils.PointFloat(111F, 123F);
            this.xrLabel48.Name = "xrLabel48";
            this.xrLabel48.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrLabel48.SizeF = new System.Drawing.SizeF(231F, 25F);
            this.xrLabel48.Text = "xrLabel48";
            // 
            // xrLabel47
            // 
            this.xrLabel47.CanGrow = false;
            this.xrLabel47.Dpi = 254F;
            this.xrLabel47.Font = new System.Drawing.Font("Tahoma", 5.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel47.LocationFloat = new DevExpress.Utils.PointFloat(3.477033E-05F, 123F);
            this.xrLabel47.Multiline = true;
            this.xrLabel47.Name = "xrLabel47";
            this.xrLabel47.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrLabel47.SizeF = new System.Drawing.SizeF(111F, 25F);
            this.xrLabel47.StylePriority.UseFont = false;
            this.xrLabel47.StylePriority.UseTextAlignment = false;
            this.xrLabel47.Text = "Ag/Benefi.";
            this.xrLabel47.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomLeft;
            // 
            // NNSTR
            // 
            this.NNSTR.CanGrow = false;
            this.NNSTR.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "NNStr2")});
            this.NNSTR.Dpi = 254F;
            this.NNSTR.Font = new System.Drawing.Font("Tahoma", 6.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.NNSTR.LocationFloat = new DevExpress.Utils.PointFloat(165.7576F, 98F);
            this.NNSTR.Name = "NNSTR";
            this.NNSTR.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.NNSTR.SizeF = new System.Drawing.SizeF(402.3098F, 25F);
            this.NNSTR.Text = "NNSTR";
            // 
            // xrLabel45
            // 
            this.xrLabel45.CanGrow = false;
            this.xrLabel45.Dpi = 254F;
            this.xrLabel45.Font = new System.Drawing.Font("Tahoma", 5.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel45.LocationFloat = new DevExpress.Utils.PointFloat(0F, 98F);
            this.xrLabel45.Name = "xrLabel45";
            this.xrLabel45.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrLabel45.SizeF = new System.Drawing.SizeF(158.4611F, 25F);
            this.xrLabel45.StylePriority.UseTextAlignment = false;
            this.xrLabel45.Text = "Nosso Numero";
            this.xrLabel45.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomLeft;
            // 
            // xrLabelBeneficiario
            // 
            this.xrLabelBeneficiario.CanGrow = false;
            this.xrLabelBeneficiario.Dpi = 254F;
            this.xrLabelBeneficiario.Font = new System.Drawing.Font("Tahoma", 5.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabelBeneficiario.LocationFloat = new DevExpress.Utils.PointFloat(0.1311109F, 13F);
            this.xrLabelBeneficiario.Name = "xrLabelBeneficiario";
            this.xrLabelBeneficiario.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrLabelBeneficiario.SizeF = new System.Drawing.SizeF(121F, 20F);
            this.xrLabelBeneficiario.Text = "Benefici�rio";
            this.xrLabelBeneficiario.WordWrap = false;
            // 
            // LabApt3
            // 
            this.LabApt3.CanGrow = false;
            this.LabApt3.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "CONNomeAptoTit")});
            this.LabApt3.Dpi = 254F;
            this.LabApt3.Font = new System.Drawing.Font("Tahoma", 5.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LabApt3.LocationFloat = new DevExpress.Utils.PointFloat(113F, 73F);
            this.LabApt3.Name = "LabApt3";
            this.LabApt3.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.LabApt3.SizeF = new System.Drawing.SizeF(85F, 25F);
            this.LabApt3.StylePriority.UseTextAlignment = false;
            this.LabApt3.Text = "LabApt3";
            this.LabApt3.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomLeft;
            // 
            // LabBlo3
            // 
            this.LabBlo3.CanGrow = false;
            this.LabBlo3.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "CONNomeBlocoTit")});
            this.LabBlo3.Dpi = 254F;
            this.LabBlo3.Font = new System.Drawing.Font("Tahoma", 5.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LabBlo3.LocationFloat = new DevExpress.Utils.PointFloat(0F, 73F);
            this.LabBlo3.Name = "LabBlo3";
            this.LabBlo3.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.LabBlo3.SizeF = new System.Drawing.SizeF(70F, 25F);
            this.LabBlo3.StylePriority.UseTextAlignment = false;
            this.LabBlo3.Text = "Bloco";
            this.LabBlo3.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomLeft;
            // 
            // LabApt4
            // 
            this.LabApt4.CanGrow = false;
            this.LabApt4.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "APTNumero")});
            this.LabApt4.Dpi = 254F;
            this.LabApt4.Font = new System.Drawing.Font("Tahoma", 6.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LabApt4.LocationFloat = new DevExpress.Utils.PointFloat(200F, 73F);
            this.LabApt4.Name = "LabApt4";
            this.LabApt4.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.LabApt4.SizeF = new System.Drawing.SizeF(80F, 25F);
            this.LabApt4.Text = "LabApt4";
            // 
            // LabBlo4
            // 
            this.LabBlo4.CanGrow = false;
            this.LabBlo4.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "BLOCodigo")});
            this.LabBlo4.Dpi = 254F;
            this.LabBlo4.Font = new System.Drawing.Font("Tahoma", 6.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LabBlo4.LocationFloat = new DevExpress.Utils.PointFloat(73F, 73F);
            this.LabBlo4.Name = "LabBlo4";
            this.LabBlo4.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.LabBlo4.SizeF = new System.Drawing.SizeF(40F, 25F);
            this.LabBlo4.Text = "LabBlo4";
            // 
            // xrCompetencia
            // 
            this.xrCompetencia.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(219)))), ((int)(((byte)(219)))), ((int)(((byte)(219)))));
            this.xrCompetencia.CanGrow = false;
            this.xrCompetencia.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "Competencia")});
            this.xrCompetencia.Dpi = 254F;
            this.xrCompetencia.Font = new System.Drawing.Font("Times New Roman", 17F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrCompetencia.FormattingRules.Add(this.RegraCompetencia);
            this.xrCompetencia.LocationFloat = new DevExpress.Utils.PointFloat(574F, 83F);
            this.xrCompetencia.Name = "xrCompetencia";
            this.xrCompetencia.SizeF = new System.Drawing.SizeF(307F, 65F);
            this.xrCompetencia.StylePriority.UseFont = false;
            this.xrCompetencia.StylePriority.UsePadding = false;
            this.xrCompetencia.StylePriority.UseTextAlignment = false;
            this.xrCompetencia.Text = "xrCompetencia";
            this.xrCompetencia.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter;
            // 
            // RegraCompetencia
            // 
            this.RegraCompetencia.Condition = "[CompAno] == 1";
            // 
            // 
            // 
            this.RegraCompetencia.Formatting.Visible = DevExpress.Utils.DefaultBoolean.False;
            this.RegraCompetencia.Name = "RegraCompetencia";
            // 
            // xrLabelrecsacado
            // 
            this.xrLabelrecsacado.CanGrow = false;
            this.xrLabelrecsacado.Dpi = 254F;
            this.xrLabelrecsacado.Font = new System.Drawing.Font("Tahoma", 5.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabelrecsacado.LocationFloat = new DevExpress.Utils.PointFloat(1285.762F, 0F);
            this.xrLabelrecsacado.Name = "xrLabelrecsacado";
            this.xrLabelrecsacado.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrLabelrecsacado.SizeF = new System.Drawing.SizeF(192.2547F, 27F);
            this.xrLabelrecsacado.StylePriority.UseTextAlignment = false;
            this.xrLabelrecsacado.Text = "Recibo do Pagador";
            this.xrLabelrecsacado.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight;
            this.xrLabelrecsacado.WordWrap = false;
            // 
            // xrLabel8
            // 
            this.xrLabel8.BackColor = System.Drawing.Color.DarkGray;
            this.xrLabel8.CanGrow = false;
            this.xrLabel8.Dpi = 254F;
            this.xrLabel8.Font = new System.Drawing.Font("Verdana", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel8.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.xrLabel8.LocationFloat = new DevExpress.Utils.PointFloat(892F, 33F);
            this.xrLabel8.Name = "xrLabel8";
            this.xrLabel8.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrLabel8.SizeF = new System.Drawing.SizeF(289F, 50F);
            this.xrLabel8.Text = "VALOR";
            this.xrLabel8.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomCenter;
            this.xrLabel8.Draw += new DevExpress.XtraReports.UI.DrawEventHandler(this.CantosRedondos_Draw);
            // 
            // xrLabelcompet1
            // 
            this.xrLabelcompet1.BackColor = System.Drawing.Color.DarkGray;
            this.xrLabelcompet1.CanGrow = false;
            this.xrLabelcompet1.Dpi = 254F;
            this.xrLabelcompet1.Font = new System.Drawing.Font("Verdana", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabelcompet1.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.xrLabelcompet1.FormattingRules.Add(this.RegraCompetencia);
            this.xrLabelcompet1.LocationFloat = new DevExpress.Utils.PointFloat(574F, 33F);
            this.xrLabelcompet1.Name = "xrLabelcompet1";
            this.xrLabelcompet1.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrLabelcompet1.SizeF = new System.Drawing.SizeF(307F, 50F);
            this.xrLabelcompet1.Text = "COMPET�NCIA";
            this.xrLabelcompet1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomCenter;
            this.xrLabelcompet1.Draw += new DevExpress.XtraReports.UI.DrawEventHandler(this.CantosRedondos_Draw);
            // 
            // xrLabel9
            // 
            this.xrLabel9.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(219)))), ((int)(((byte)(219)))), ((int)(((byte)(219)))));
            this.xrLabel9.CanGrow = false;
            this.xrLabel9.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "BOLValorPrevisto", "{0:#,##0.00}")});
            this.xrLabel9.Dpi = 254F;
            this.xrLabel9.Font = new System.Drawing.Font("Times New Roman", 17F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel9.FormattingRules.Add(this.Valor_maior_99_mil);
            this.xrLabel9.LocationFloat = new DevExpress.Utils.PointFloat(892F, 83F);
            this.xrLabel9.Name = "xrLabel9";
            this.xrLabel9.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrLabel9.SizeF = new System.Drawing.SizeF(289F, 65F);
            this.xrLabel9.StylePriority.UseFont = false;
            this.xrLabel9.StylePriority.UseTextAlignment = false;
            this.xrLabel9.Text = "xrLabel9";
            this.xrLabel9.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter;
            // 
            // Valor_maior_99_mil
            // 
            this.Valor_maior_99_mil.Condition = "[BOLValorPrevisto] > 99999.99";
            // 
            // 
            // 
            this.Valor_maior_99_mil.Formatting.Font = new System.Drawing.Font("Times New Roman", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Valor_maior_99_mil.Name = "Valor_maior_99_mil";
            // 
            // xrLabel7
            // 
            this.xrLabel7.BackColor = System.Drawing.Color.DarkGray;
            this.xrLabel7.CanGrow = false;
            this.xrLabel7.Dpi = 254F;
            this.xrLabel7.Font = new System.Drawing.Font("Verdana", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel7.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.xrLabel7.LocationFloat = new DevExpress.Utils.PointFloat(1192F, 33F);
            this.xrLabel7.Name = "xrLabel7";
            this.xrLabel7.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrLabel7.SizeF = new System.Drawing.SizeF(294F, 50F);
            this.xrLabel7.Text = "VENCIMENTO";
            this.xrLabel7.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomCenter;
            this.xrLabel7.Draw += new DevExpress.XtraReports.UI.DrawEventHandler(this.CantosRedondos_Draw);
            // 
            // xrLabel6
            // 
            this.xrLabel6.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(219)))), ((int)(((byte)(219)))), ((int)(((byte)(219)))));
            this.xrLabel6.CanGrow = false;
            this.xrLabel6.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "BOLVencto", "{0:dd/MM/yy}")});
            this.xrLabel6.Dpi = 254F;
            this.xrLabel6.Font = new System.Drawing.Font("Times New Roman", 17F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel6.LocationFloat = new DevExpress.Utils.PointFloat(1192F, 83F);
            this.xrLabel6.Name = "xrLabel6";
            this.xrLabel6.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrLabel6.SizeF = new System.Drawing.SizeF(294F, 65F);
            this.xrLabel6.StylePriority.UseFont = false;
            this.xrLabel6.StylePriority.UseTextAlignment = false;
            this.xrLabel6.Text = "xrLabel6";
            this.xrLabel6.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter;
            // 
            // xrPictureBox1
            // 
            this.xrPictureBox1.Dpi = 254F;
            this.xrPictureBox1.Image = ((System.Drawing.Image)(resources.GetObject("xrPictureBox1.Image")));
            this.xrPictureBox1.LocationFloat = new DevExpress.Utils.PointFloat(0F, 0F);
            this.xrPictureBox1.Name = "xrPictureBox1";
            this.xrPictureBox1.SizeF = new System.Drawing.SizeF(313F, 148F);
            this.xrPictureBox1.Sizing = DevExpress.XtraPrinting.ImageSizeMode.ZoomImage;
            // 
            // xrLabel1x
            // 
            this.xrLabel1x.CanGrow = false;
            this.xrLabel1x.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "CONNome")});
            this.xrLabel1x.Dpi = 254F;
            this.xrLabel1x.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel1x.LocationFloat = new DevExpress.Utils.PointFloat(0F, 0F);
            this.xrLabel1x.Name = "xrLabel1x";
            this.xrLabel1x.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrLabel1x.SizeF = new System.Drawing.SizeF(569F, 36F);
            this.xrLabel1x.StylePriority.UseTextAlignment = false;
            this.xrLabel1x.Text = "xrLabel1x";
            this.xrLabel1x.WordWrap = false;
            // 
            // QuadoBoleto
            // 
            this.QuadoBoleto.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.Panel_SEMCPF,
            this.xrLogoBanco33,
            this.xrLogoBanco,
            this.NamePago,
            this.xrLabel33,
            this.xrLabel32,
            this.CodigoDeBarras,
            this.xrPanel7,
            this.LinhaDigitavel,
            this.BANCONumero,
            this.BANCONome,
            this.xrLine1});
            this.QuadoBoleto.Dpi = 254F;
            this.QuadoBoleto.LocationFloat = new DevExpress.Utils.PointFloat(0F, 1958F);
            this.QuadoBoleto.Name = "QuadoBoleto";
            this.QuadoBoleto.SizeF = new System.Drawing.SizeF(1799F, 896F);
            // 
            // Panel_SEMCPF
            // 
            this.Panel_SEMCPF.BackColor = System.Drawing.Color.Gainsboro;
            this.Panel_SEMCPF.CanGrow = false;
            this.Panel_SEMCPF.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.Label_SEMCPF});
            this.Panel_SEMCPF.Dpi = 254F;
            this.Panel_SEMCPF.LocationFloat = new DevExpress.Utils.PointFloat(20.94866F, 24.99991F);
            this.Panel_SEMCPF.Name = "Panel_SEMCPF";
            this.Panel_SEMCPF.SizeF = new System.Drawing.SizeF(1753F, 871.0001F);
            this.Panel_SEMCPF.StylePriority.UseBackColor = false;
            // 
            // Label_SEMCPF
            // 
            this.Label_SEMCPF.CanGrow = false;
            this.Label_SEMCPF.Dpi = 254F;
            this.Label_SEMCPF.Font = new System.Drawing.Font("Times New Roman", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Label_SEMCPF.LocationFloat = new DevExpress.Utils.PointFloat(20.99998F, 41.57997F);
            this.Label_SEMCPF.Multiline = true;
            this.Label_SEMCPF.Name = "Label_SEMCPF";
            this.Label_SEMCPF.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.Label_SEMCPF.SizeF = new System.Drawing.SizeF(1697F, 804.42F);
            this.Label_SEMCPF.StylePriority.UseFont = false;
            this.Label_SEMCPF.StylePriority.UseTextAlignment = false;
            this.Label_SEMCPF.Text = resources.GetString("Label_SEMCPF.Text");
            this.Label_SEMCPF.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrLogoBanco33
            // 
            this.xrLogoBanco33.Dpi = 254F;
            this.xrLogoBanco33.Image = ((System.Drawing.Image)(resources.GetObject("xrLogoBanco33.Image")));
            this.xrLogoBanco33.LocationFloat = new DevExpress.Utils.PointFloat(112.7961F, 15.00001F);
            this.xrLogoBanco33.Name = "xrLogoBanco33";
            this.xrLogoBanco33.SizeF = new System.Drawing.SizeF(90.95831F, 17F);
            this.xrLogoBanco33.Sizing = DevExpress.XtraPrinting.ImageSizeMode.ZoomImage;
            this.xrLogoBanco33.Visible = false;
            // 
            // xrLogoBanco
            // 
            this.xrLogoBanco.Dpi = 254F;
            this.xrLogoBanco.Image = ((System.Drawing.Image)(resources.GetObject("xrLogoBanco.Image")));
            this.xrLogoBanco.LocationFloat = new DevExpress.Utils.PointFloat(5.000018F, 15F);
            this.xrLogoBanco.Name = "xrLogoBanco";
            this.xrLogoBanco.SizeF = new System.Drawing.SizeF(90.95831F, 17F);
            this.xrLogoBanco.Sizing = DevExpress.XtraPrinting.ImageSizeMode.ZoomImage;
            this.xrLogoBanco.Visible = false;
            // 
            // NamePago
            // 
            this.NamePago.Dpi = 254F;
            this.NamePago.Font = new System.Drawing.Font("Verdana", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.NamePago.ForeColor = System.Drawing.Color.Gainsboro;
            this.NamePago.LocationFloat = new DevExpress.Utils.PointFloat(1101F, 815F);
            this.NamePago.Name = "NamePago";
            this.NamePago.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.NamePago.SizeF = new System.Drawing.SizeF(687F, 75F);
            this.NamePago.Text = "PAGO";
            this.NamePago.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.NamePago.Visible = false;
            // 
            // xrLabel33
            // 
            this.xrLabel33.Dpi = 254F;
            this.xrLabel33.Font = new System.Drawing.Font("Tahoma", 6.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel33.LocationFloat = new DevExpress.Utils.PointFloat(1482F, 765F);
            this.xrLabel33.Name = "xrLabel33";
            this.xrLabel33.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrLabel33.SizeF = new System.Drawing.SizeF(304F, 35F);
            this.xrLabel33.Text = "Ficha de Compensa��o";
            // 
            // xrLabel32
            // 
            this.xrLabel32.Dpi = 254F;
            this.xrLabel32.Font = new System.Drawing.Font("Tahoma", 6F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel32.LocationFloat = new DevExpress.Utils.PointFloat(1101F, 765F);
            this.xrLabel32.Name = "xrLabel32";
            this.xrLabel32.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrLabel32.SizeF = new System.Drawing.SizeF(275F, 35F);
            this.xrLabel32.Text = "Autentica��o Mec�nica";
            // 
            // CodigoDeBarras
            // 
            this.CodigoDeBarras.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "BOLCodBar")});
            this.CodigoDeBarras.Dpi = 254F;
            this.CodigoDeBarras.LocationFloat = new DevExpress.Utils.PointFloat(42.00002F, 761F);
            this.CodigoDeBarras.Name = "CodigoDeBarras";
            this.CodigoDeBarras.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.CodigoDeBarras.SizeF = new System.Drawing.SizeF(1048F, 135F);
            this.CodigoDeBarras.Text = "CodigoDeBarras";
            this.CodigoDeBarras.Draw += new DevExpress.XtraReports.UI.DrawEventHandler(this.CodigoDeBarras_Draw);
            // 
            // xrPanel7
            // 
            this.xrPanel7.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrPanel7.CanGrow = false;
            this.xrPanel7.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrPanel36,
            this.xrPanel24,
            this.xrPanel23,
            this.xrPanel19,
            this.xrPanel18,
            this.xrPanel17,
            this.xrPanel16,
            this.xrPanel15,
            this.xrPanel14,
            this.xrPanel13,
            this.xrPanel12,
            this.xrPanel11,
            this.xrPanel10,
            this.xrPanel22,
            this.xrPanel8});
            this.xrPanel7.Dpi = 254F;
            this.xrPanel7.LocationFloat = new DevExpress.Utils.PointFloat(0F, 100F);
            this.xrPanel7.Name = "xrPanel7";
            this.xrPanel7.SizeF = new System.Drawing.SizeF(1799F, 661F);
            // 
            // xrPanel36
            // 
            this.xrPanel36.Borders = DevExpress.XtraPrinting.BorderSide.Left;
            this.xrPanel36.CanGrow = false;
            this.xrPanel36.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrLabel85,
            this.xrLabel84});
            this.xrPanel36.Dpi = 254F;
            this.xrPanel36.LocationFloat = new DevExpress.Utils.PointFloat(2.999996F, 250F);
            this.xrPanel36.Name = "xrPanel36";
            this.xrPanel36.SizeF = new System.Drawing.SizeF(1193.105F, 250F);
            // 
            // xrLabel85
            // 
            this.xrLabel85.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel85.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "BOLMensagem")});
            this.xrLabel85.Dpi = 254F;
            this.xrLabel85.Font = new System.Drawing.Font("Tahoma", 5.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel85.LocationFloat = new DevExpress.Utils.PointFloat(2.999996F, 21.00006F);
            this.xrLabel85.Multiline = true;
            this.xrLabel85.Name = "xrLabel85";
            this.xrLabel85.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrLabel85.SizeF = new System.Drawing.SizeF(1185.105F, 229F);
            this.xrLabel85.StylePriority.UseBorders = false;
            this.xrLabel85.Text = "xrLabel85";
            // 
            // xrLabel84
            // 
            this.xrLabel84.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel84.Dpi = 254F;
            this.xrLabel84.Font = new System.Drawing.Font("Tahoma", 5.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel84.LocationFloat = new DevExpress.Utils.PointFloat(3F, 0F);
            this.xrLabel84.Name = "xrLabel84";
            this.xrLabel84.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrLabel84.SizeF = new System.Drawing.SizeF(693F, 21F);
            this.xrLabel84.StylePriority.UseBorders = false;
            this.xrLabel84.Text = "Instru��es (Texto de responsabilidade do Benefici�rio)";
            // 
            // xrPanel24
            // 
            this.xrPanel24.AnchorVertical = DevExpress.XtraReports.UI.VerticalAnchorStyles.Top;
            this.xrPanel24.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Top | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrPanel24.CanGrow = false;
            this.xrPanel24.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrLabel103,
            this.xrLabelUsoBanco,
            this.xrLabelCarteira,
            this.xrLine9,
            this.xrLine8,
            this.xrLine7,
            this.xrLine6,
            this.xrLabel108,
            this.xrLabel107,
            this.xrLabel106,
            this.xrEspecie,
            this.xrLabel104,
            this.xrLabel82});
            this.xrPanel24.Dpi = 254F;
            this.xrPanel24.LocationFloat = new DevExpress.Utils.PointFloat(3F, 200F);
            this.xrPanel24.Name = "xrPanel24";
            this.xrPanel24.SizeF = new System.Drawing.SizeF(1197F, 50F);
            // 
            // xrLabel103
            // 
            this.xrLabel103.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel103.CanGrow = false;
            this.xrLabel103.Dpi = 254F;
            this.xrLabel103.Font = new System.Drawing.Font("Tahoma", 5.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel103.LocationFloat = new DevExpress.Utils.PointFloat(254F, 3F);
            this.xrLabel103.Name = "xrLabel103";
            this.xrLabel103.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrLabel103.SizeF = new System.Drawing.SizeF(85F, 21F);
            this.xrLabel103.StylePriority.UseBorders = false;
            this.xrLabel103.Text = "Carteira";
            // 
            // xrLabelUsoBanco
            // 
            this.xrLabelUsoBanco.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabelUsoBanco.CanGrow = false;
            this.xrLabelUsoBanco.Dpi = 254F;
            this.xrLabelUsoBanco.Font = new System.Drawing.Font("Tahoma", 6.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabelUsoBanco.LocationFloat = new DevExpress.Utils.PointFloat(18F, 25F);
            this.xrLabelUsoBanco.Name = "xrLabelUsoBanco";
            this.xrLabelUsoBanco.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrLabelUsoBanco.SizeF = new System.Drawing.SizeF(202F, 21F);
            this.xrLabelUsoBanco.StylePriority.UseBorders = false;
            this.xrLabelUsoBanco.Text = "CVT 7744-5";
            this.xrLabelUsoBanco.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrLabelCarteira
            // 
            this.xrLabelCarteira.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabelCarteira.CanGrow = false;
            this.xrLabelCarteira.Dpi = 254F;
            this.xrLabelCarteira.Font = new System.Drawing.Font("Tahoma", 6.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabelCarteira.LocationFloat = new DevExpress.Utils.PointFloat(257F, 25F);
            this.xrLabelCarteira.Name = "xrLabelCarteira";
            this.xrLabelCarteira.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrLabelCarteira.SizeF = new System.Drawing.SizeF(183F, 21F);
            this.xrLabelCarteira.StylePriority.UseBorders = false;
            this.xrLabelCarteira.Text = "6";
            this.xrLabelCarteira.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrLine9
            // 
            this.xrLine9.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLine9.Dpi = 254F;
            this.xrLine9.LineDirection = DevExpress.XtraReports.UI.LineDirection.Vertical;
            this.xrLine9.LineWidth = 3;
            this.xrLine9.LocationFloat = new DevExpress.Utils.PointFloat(897F, 3F);
            this.xrLine9.Name = "xrLine9";
            this.xrLine9.SizeF = new System.Drawing.SizeF(11F, 16F);
            this.xrLine9.StylePriority.UseBorders = false;
            // 
            // xrLine8
            // 
            this.xrLine8.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLine8.Dpi = 254F;
            this.xrLine8.LineDirection = DevExpress.XtraReports.UI.LineDirection.Vertical;
            this.xrLine8.LineWidth = 3;
            this.xrLine8.LocationFloat = new DevExpress.Utils.PointFloat(675F, 3F);
            this.xrLine8.Name = "xrLine8";
            this.xrLine8.SizeF = new System.Drawing.SizeF(11F, 44F);
            this.xrLine8.StylePriority.UseBorders = false;
            // 
            // xrLine7
            // 
            this.xrLine7.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLine7.Dpi = 254F;
            this.xrLine7.LineDirection = DevExpress.XtraReports.UI.LineDirection.Vertical;
            this.xrLine7.LineWidth = 3;
            this.xrLine7.LocationFloat = new DevExpress.Utils.PointFloat(444F, 3F);
            this.xrLine7.Name = "xrLine7";
            this.xrLine7.SizeF = new System.Drawing.SizeF(11F, 44F);
            this.xrLine7.StylePriority.UseBorders = false;
            // 
            // xrLine6
            // 
            this.xrLine6.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLine6.Dpi = 254F;
            this.xrLine6.LineDirection = DevExpress.XtraReports.UI.LineDirection.Vertical;
            this.xrLine6.LineWidth = 3;
            this.xrLine6.LocationFloat = new DevExpress.Utils.PointFloat(241F, 3F);
            this.xrLine6.Name = "xrLine6";
            this.xrLine6.SizeF = new System.Drawing.SizeF(11F, 44F);
            this.xrLine6.StylePriority.UseBorders = false;
            // 
            // xrLabel108
            // 
            this.xrLabel108.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel108.CanGrow = false;
            this.xrLabel108.Dpi = 254F;
            this.xrLabel108.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel108.LocationFloat = new DevExpress.Utils.PointFloat(888F, 19F);
            this.xrLabel108.Name = "xrLabel108";
            this.xrLabel108.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrLabel108.SizeF = new System.Drawing.SizeF(35F, 28F);
            this.xrLabel108.StylePriority.UseBorders = false;
            this.xrLabel108.Text = "x";
            this.xrLabel108.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomJustify;
            // 
            // xrLabel107
            // 
            this.xrLabel107.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel107.Dpi = 254F;
            this.xrLabel107.Font = new System.Drawing.Font("Tahoma", 5.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel107.LocationFloat = new DevExpress.Utils.PointFloat(955F, 3F);
            this.xrLabel107.Name = "xrLabel107";
            this.xrLabel107.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrLabel107.SizeF = new System.Drawing.SizeF(74F, 26F);
            this.xrLabel107.StylePriority.UseBorders = false;
            this.xrLabel107.Text = "Valor";
            // 
            // xrLabel106
            // 
            this.xrLabel106.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel106.CanGrow = false;
            this.xrLabel106.Dpi = 254F;
            this.xrLabel106.Font = new System.Drawing.Font("Tahoma", 5.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel106.LocationFloat = new DevExpress.Utils.PointFloat(688F, 3F);
            this.xrLabel106.Name = "xrLabel106";
            this.xrLabel106.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrLabel106.SizeF = new System.Drawing.SizeF(127F, 26F);
            this.xrLabel106.StylePriority.UseBorders = false;
            this.xrLabel106.Text = "Quantidade";
            // 
            // xrEspecie
            // 
            this.xrEspecie.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrEspecie.CanGrow = false;
            this.xrEspecie.Dpi = 254F;
            this.xrEspecie.Font = new System.Drawing.Font("Tahoma", 6.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrEspecie.LocationFloat = new DevExpress.Utils.PointFloat(550F, 25F);
            this.xrEspecie.Name = "xrEspecie";
            this.xrEspecie.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrEspecie.SizeF = new System.Drawing.SizeF(116F, 21F);
            this.xrEspecie.StylePriority.UseBorders = false;
            this.xrEspecie.Text = "R$";
            this.xrEspecie.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // xrLabel104
            // 
            this.xrLabel104.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel104.CanGrow = false;
            this.xrLabel104.Dpi = 254F;
            this.xrLabel104.Font = new System.Drawing.Font("Tahoma", 5.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel104.LocationFloat = new DevExpress.Utils.PointFloat(460F, 3F);
            this.xrLabel104.Name = "xrLabel104";
            this.xrLabel104.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrLabel104.SizeF = new System.Drawing.SizeF(80F, 21F);
            this.xrLabel104.StylePriority.UseBorders = false;
            this.xrLabel104.Text = "Esp�cie";
            // 
            // xrLabel82
            // 
            this.xrLabel82.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel82.CanGrow = false;
            this.xrLabel82.Dpi = 254F;
            this.xrLabel82.Font = new System.Drawing.Font("Tahoma", 5.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel82.LocationFloat = new DevExpress.Utils.PointFloat(0F, 3F);
            this.xrLabel82.Name = "xrLabel82";
            this.xrLabel82.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrLabel82.SizeF = new System.Drawing.SizeF(138F, 18F);
            this.xrLabel82.StylePriority.UseBorders = false;
            this.xrLabel82.Text = "Uso do Banco";
            // 
            // xrPanel23
            // 
            this.xrPanel23.Borders = DevExpress.XtraPrinting.BorderSide.Top;
            this.xrPanel23.CanGrow = false;
            this.xrPanel23.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrLabel3,
            this.xrLabelEspecie,
            this.xrLabel81,
            this.xrLine5,
            this.xrLine4,
            this.xrLine3,
            this.xrLine2,
            this.xrLabel102,
            this.xrLabel101,
            this.xrLabel100,
            this.xrLabel99,
            this.xrLabel98,
            this.xrLabel79,
            this.xrLabel80});
            this.xrPanel23.Dpi = 254F;
            this.xrPanel23.LocationFloat = new DevExpress.Utils.PointFloat(3F, 150F);
            this.xrPanel23.Name = "xrPanel23";
            this.xrPanel23.SizeF = new System.Drawing.SizeF(1197F, 50F);
            // 
            // xrLabel3
            // 
            this.xrLabel3.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel3.CanGrow = false;
            this.xrLabel3.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "BOL")});
            this.xrLabel3.Dpi = 254F;
            this.xrLabel3.Font = new System.Drawing.Font("Tahoma", 6.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel3.LocationFloat = new DevExpress.Utils.PointFloat(269.6597F, 27F);
            this.xrLabel3.Name = "xrLabel3";
            this.xrLabel3.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrLabel3.SizeF = new System.Drawing.SizeF(199.3402F, 21F);
            this.xrLabel3.StylePriority.UseBorders = false;
            this.xrLabel3.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.xrLabel3.Visible = false;
            // 
            // xrLabelEspecie
            // 
            this.xrLabelEspecie.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabelEspecie.CanGrow = false;
            this.xrLabelEspecie.Dpi = 254F;
            this.xrLabelEspecie.Font = new System.Drawing.Font("Tahoma", 6.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabelEspecie.LocationFloat = new DevExpress.Utils.PointFloat(566F, 27F);
            this.xrLabelEspecie.Name = "xrLabelEspecie";
            this.xrLabelEspecie.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrLabelEspecie.SizeF = new System.Drawing.SizeF(143F, 21F);
            this.xrLabelEspecie.StylePriority.UseBorders = false;
            this.xrLabelEspecie.Text = "N";
            this.xrLabelEspecie.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrLabel81
            // 
            this.xrLabel81.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel81.CanGrow = false;
            this.xrLabel81.Dpi = 254F;
            this.xrLabel81.Font = new System.Drawing.Font("Tahoma", 6.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel81.LocationFloat = new DevExpress.Utils.PointFloat(820F, 27F);
            this.xrLabel81.Name = "xrLabel81";
            this.xrLabel81.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrLabel81.SizeF = new System.Drawing.SizeF(58F, 20F);
            this.xrLabel81.StylePriority.UseBorders = false;
            this.xrLabel81.Text = "N";
            this.xrLabel81.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrLine5
            // 
            this.xrLine5.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLine5.Dpi = 254F;
            this.xrLine5.LineDirection = DevExpress.XtraReports.UI.LineDirection.Vertical;
            this.xrLine5.LineWidth = 3;
            this.xrLine5.LocationFloat = new DevExpress.Utils.PointFloat(233F, 3F);
            this.xrLine5.Name = "xrLine5";
            this.xrLine5.SizeF = new System.Drawing.SizeF(11F, 47F);
            this.xrLine5.StylePriority.UseBorders = false;
            // 
            // xrLine4
            // 
            this.xrLine4.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLine4.Dpi = 254F;
            this.xrLine4.LineDirection = DevExpress.XtraReports.UI.LineDirection.Vertical;
            this.xrLine4.LineWidth = 3;
            this.xrLine4.LocationFloat = new DevExpress.Utils.PointFloat(508F, 3F);
            this.xrLine4.Name = "xrLine4";
            this.xrLine4.SizeF = new System.Drawing.SizeF(11F, 47F);
            this.xrLine4.StylePriority.UseBorders = false;
            // 
            // xrLine3
            // 
            this.xrLine3.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLine3.Dpi = 254F;
            this.xrLine3.LineDirection = DevExpress.XtraReports.UI.LineDirection.Vertical;
            this.xrLine3.LineWidth = 3;
            this.xrLine3.LocationFloat = new DevExpress.Utils.PointFloat(765F, 3F);
            this.xrLine3.Name = "xrLine3";
            this.xrLine3.SizeF = new System.Drawing.SizeF(11F, 47F);
            this.xrLine3.StylePriority.UseBorders = false;
            // 
            // xrLine2
            // 
            this.xrLine2.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLine2.Dpi = 254F;
            this.xrLine2.LineDirection = DevExpress.XtraReports.UI.LineDirection.Vertical;
            this.xrLine2.LineWidth = 3;
            this.xrLine2.LocationFloat = new DevExpress.Utils.PointFloat(921F, 3F);
            this.xrLine2.Name = "xrLine2";
            this.xrLine2.SizeF = new System.Drawing.SizeF(11F, 47F);
            this.xrLine2.StylePriority.UseBorders = false;
            // 
            // xrLabel102
            // 
            this.xrLabel102.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel102.CanGrow = false;
            this.xrLabel102.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "BOLEmissao", "{0:dd/MM/yyyy}")});
            this.xrLabel102.Dpi = 254F;
            this.xrLabel102.Font = new System.Drawing.Font("Tahoma", 6.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel102.LocationFloat = new DevExpress.Utils.PointFloat(963F, 27F);
            this.xrLabel102.Name = "xrLabel102";
            this.xrLabel102.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrLabel102.SizeF = new System.Drawing.SizeF(212F, 21F);
            this.xrLabel102.StylePriority.UseBorders = false;
            this.xrLabel102.Text = "xrLabel102";
            this.xrLabel102.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // xrLabel101
            // 
            this.xrLabel101.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel101.CanGrow = false;
            this.xrLabel101.Dpi = 254F;
            this.xrLabel101.Font = new System.Drawing.Font("Tahoma", 5.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel101.LocationFloat = new DevExpress.Utils.PointFloat(939F, 3F);
            this.xrLabel101.Name = "xrLabel101";
            this.xrLabel101.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrLabel101.SizeF = new System.Drawing.SizeF(249F, 24F);
            this.xrLabel101.StylePriority.UseBorders = false;
            this.xrLabel101.Text = "Data do Processamento";
            // 
            // xrLabel100
            // 
            this.xrLabel100.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel100.CanGrow = false;
            this.xrLabel100.Dpi = 254F;
            this.xrLabel100.Font = new System.Drawing.Font("Tahoma", 5.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel100.LocationFloat = new DevExpress.Utils.PointFloat(778F, 3F);
            this.xrLabel100.Name = "xrLabel100";
            this.xrLabel100.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrLabel100.SizeF = new System.Drawing.SizeF(69F, 24F);
            this.xrLabel100.StylePriority.UseBorders = false;
            this.xrLabel100.Text = "Aceite";
            // 
            // xrLabel99
            // 
            this.xrLabel99.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel99.CanGrow = false;
            this.xrLabel99.Dpi = 254F;
            this.xrLabel99.Font = new System.Drawing.Font("Tahoma", 5.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel99.LocationFloat = new DevExpress.Utils.PointFloat(524F, 3F);
            this.xrLabel99.Name = "xrLabel99";
            this.xrLabel99.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrLabel99.SizeF = new System.Drawing.SizeF(222F, 24F);
            this.xrLabel99.StylePriority.UseBorders = false;
            this.xrLabel99.Text = "Especie do Documento";
            // 
            // xrLabel98
            // 
            this.xrLabel98.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel98.CanGrow = false;
            this.xrLabel98.Dpi = 254F;
            this.xrLabel98.Font = new System.Drawing.Font("Tahoma", 5.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel98.LocationFloat = new DevExpress.Utils.PointFloat(249F, 3F);
            this.xrLabel98.Name = "xrLabel98";
            this.xrLabel98.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrLabel98.SizeF = new System.Drawing.SizeF(227F, 24F);
            this.xrLabel98.StylePriority.UseBorders = false;
            this.xrLabel98.Text = "N�mero do Documento";
            // 
            // xrLabel79
            // 
            this.xrLabel79.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel79.CanGrow = false;
            this.xrLabel79.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "BOLEmissao", "{0:dd/MM/yyyy}")});
            this.xrLabel79.Dpi = 254F;
            this.xrLabel79.Font = new System.Drawing.Font("Tahoma", 6.75F);
            this.xrLabel79.LocationFloat = new DevExpress.Utils.PointFloat(13F, 27F);
            this.xrLabel79.Name = "xrLabel79";
            this.xrLabel79.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrLabel79.SizeF = new System.Drawing.SizeF(212F, 21F);
            this.xrLabel79.StylePriority.UseBorders = false;
            this.xrLabel79.Text = "xrLabel79";
            this.xrLabel79.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // xrLabel80
            // 
            this.xrLabel80.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel80.CanGrow = false;
            this.xrLabel80.Dpi = 254F;
            this.xrLabel80.Font = new System.Drawing.Font("Tahoma", 5.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel80.LocationFloat = new DevExpress.Utils.PointFloat(0F, 3F);
            this.xrLabel80.Name = "xrLabel80";
            this.xrLabel80.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrLabel80.SizeF = new System.Drawing.SizeF(201F, 24F);
            this.xrLabel80.StylePriority.UseBorders = false;
            this.xrLabel80.Text = "Data do Documento";
            // 
            // xrPanel19
            // 
            this.xrPanel19.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrPanel19.CanGrow = false;
            this.xrPanel19.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.NNSTR2,
            this.xrLabel73});
            this.xrPanel19.Dpi = 254F;
            this.xrPanel19.LocationFloat = new DevExpress.Utils.PointFloat(1200F, 150F);
            this.xrPanel19.Name = "xrPanel19";
            this.xrPanel19.SizeF = new System.Drawing.SizeF(595.9999F, 50F);
            // 
            // NNSTR2
            // 
            this.NNSTR2.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.NNSTR2.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "NNStr2")});
            this.NNSTR2.Dpi = 254F;
            this.NNSTR2.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.NNSTR2.LocationFloat = new DevExpress.Utils.PointFloat(202F, 3F);
            this.NNSTR2.Name = "NNSTR2";
            this.NNSTR2.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.NNSTR2.SizeF = new System.Drawing.SizeF(375F, 42F);
            this.NNSTR2.StylePriority.UseBorders = false;
            this.NNSTR2.Text = "NNSTR2";
            this.NNSTR2.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            // 
            // xrLabel73
            // 
            this.xrLabel73.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel73.Dpi = 254F;
            this.xrLabel73.Font = new System.Drawing.Font("Tahoma", 5.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel73.LocationFloat = new DevExpress.Utils.PointFloat(3F, 3F);
            this.xrLabel73.Name = "xrLabel73";
            this.xrLabel73.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrLabel73.SizeF = new System.Drawing.SizeF(170F, 32F);
            this.xrLabel73.StylePriority.UseBorders = false;
            this.xrLabel73.Text = "Nosso N�mero";
            // 
            // xrPanel18
            // 
            this.xrPanel18.BackColor = System.Drawing.Color.LightGray;
            this.xrPanel18.Borders = DevExpress.XtraPrinting.BorderSide.Left;
            this.xrPanel18.CanGrow = false;
            this.xrPanel18.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrLabel71,
            this.xrLabel70});
            this.xrPanel18.Dpi = 254F;
            this.xrPanel18.LocationFloat = new DevExpress.Utils.PointFloat(1200F, 200F);
            this.xrPanel18.Name = "xrPanel18";
            this.xrPanel18.SizeF = new System.Drawing.SizeF(596.0002F, 50F);
            // 
            // xrLabel71
            // 
            this.xrLabel71.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel71.Dpi = 254F;
            this.xrLabel71.Font = new System.Drawing.Font("Tahoma", 5.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel71.LocationFloat = new DevExpress.Utils.PointFloat(3F, 3F);
            this.xrLabel71.Name = "xrLabel71";
            this.xrLabel71.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrLabel71.SizeF = new System.Drawing.SizeF(243F, 26F);
            this.xrLabel71.StylePriority.UseBorders = false;
            this.xrLabel71.Text = "(=) Valor do Documento";
            // 
            // xrLabel70
            // 
            this.xrLabel70.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel70.CanGrow = false;
            this.xrLabel70.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "BOLValorPrevisto", "{0:#,##0.00}")});
            this.xrLabel70.Dpi = 254F;
            this.xrLabel70.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel70.LocationFloat = new DevExpress.Utils.PointFloat(282F, 0F);
            this.xrLabel70.Name = "xrLabel70";
            this.xrLabel70.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrLabel70.SizeF = new System.Drawing.SizeF(300F, 49F);
            this.xrLabel70.StylePriority.UseBorders = false;
            this.xrLabel70.Text = "xrLabel70";
            this.xrLabel70.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            // 
            // xrPanel17
            // 
            this.xrPanel17.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrPanel17.CanGrow = false;
            this.xrPanel17.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrLabel69});
            this.xrPanel17.Dpi = 254F;
            this.xrPanel17.LocationFloat = new DevExpress.Utils.PointFloat(1200F, 250F);
            this.xrPanel17.Name = "xrPanel17";
            this.xrPanel17.SizeF = new System.Drawing.SizeF(596.0004F, 50F);
            // 
            // xrLabel69
            // 
            this.xrLabel69.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel69.Dpi = 254F;
            this.xrLabel69.Font = new System.Drawing.Font("Tahoma", 5.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel69.LocationFloat = new DevExpress.Utils.PointFloat(3F, 3F);
            this.xrLabel69.Name = "xrLabel69";
            this.xrLabel69.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrLabel69.SizeF = new System.Drawing.SizeF(291F, 32F);
            this.xrLabel69.StylePriority.UseBorders = false;
            this.xrLabel69.Text = "(-) Desconto/Abatimento";
            // 
            // xrPanel16
            // 
            this.xrPanel16.Borders = DevExpress.XtraPrinting.BorderSide.Left;
            this.xrPanel16.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrLabel67});
            this.xrPanel16.Dpi = 254F;
            this.xrPanel16.LocationFloat = new DevExpress.Utils.PointFloat(1200F, 400F);
            this.xrPanel16.Name = "xrPanel16";
            this.xrPanel16.SizeF = new System.Drawing.SizeF(595.9999F, 50F);
            // 
            // xrLabel67
            // 
            this.xrLabel67.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel67.Dpi = 254F;
            this.xrLabel67.Font = new System.Drawing.Font("Tahoma", 5.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel67.LocationFloat = new DevExpress.Utils.PointFloat(3F, 3F);
            this.xrLabel67.Name = "xrLabel67";
            this.xrLabel67.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrLabel67.SizeF = new System.Drawing.SizeF(275F, 32F);
            this.xrLabel67.StylePriority.UseBorders = false;
            this.xrLabel67.Text = "(+) Outros Acr�sciomos";
            // 
            // xrPanel15
            // 
            this.xrPanel15.BackColor = System.Drawing.Color.LightGray;
            this.xrPanel15.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top)));
            this.xrPanel15.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrLabel65});
            this.xrPanel15.Dpi = 254F;
            this.xrPanel15.LocationFloat = new DevExpress.Utils.PointFloat(1200F, 450F);
            this.xrPanel15.Name = "xrPanel15";
            this.xrPanel15.SizeF = new System.Drawing.SizeF(595.9999F, 50F);
            // 
            // xrLabel65
            // 
            this.xrLabel65.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel65.Dpi = 254F;
            this.xrLabel65.Font = new System.Drawing.Font("Tahoma", 5.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel65.LocationFloat = new DevExpress.Utils.PointFloat(3F, 3F);
            this.xrLabel65.Name = "xrLabel65";
            this.xrLabel65.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrLabel65.SizeF = new System.Drawing.SizeF(254F, 32F);
            this.xrLabel65.StylePriority.UseBorders = false;
            this.xrLabel65.Text = "(=) Valor Cobrado";
            // 
            // xrPanel14
            // 
            this.xrPanel14.Borders = DevExpress.XtraPrinting.BorderSide.Left;
            this.xrPanel14.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrLabel63});
            this.xrPanel14.Dpi = 254F;
            this.xrPanel14.LocationFloat = new DevExpress.Utils.PointFloat(1200F, 300F);
            this.xrPanel14.Name = "xrPanel14";
            this.xrPanel14.SizeF = new System.Drawing.SizeF(596.0006F, 50F);
            // 
            // xrLabel63
            // 
            this.xrLabel63.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel63.Dpi = 254F;
            this.xrLabel63.Font = new System.Drawing.Font("Tahoma", 5.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel63.LocationFloat = new DevExpress.Utils.PointFloat(3F, 3F);
            this.xrLabel63.Name = "xrLabel63";
            this.xrLabel63.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrLabel63.SizeF = new System.Drawing.SizeF(254F, 32F);
            this.xrLabel63.StylePriority.UseBorders = false;
            this.xrLabel63.Text = "(-) Outras Dedu��es";
            // 
            // xrPanel13
            // 
            this.xrPanel13.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrPanel13.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrLabel61});
            this.xrPanel13.Dpi = 254F;
            this.xrPanel13.LocationFloat = new DevExpress.Utils.PointFloat(1200F, 350F);
            this.xrPanel13.Name = "xrPanel13";
            this.xrPanel13.SizeF = new System.Drawing.SizeF(596.0007F, 50F);
            // 
            // xrLabel61
            // 
            this.xrLabel61.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel61.Dpi = 254F;
            this.xrLabel61.Font = new System.Drawing.Font("Tahoma", 5.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel61.LocationFloat = new DevExpress.Utils.PointFloat(3F, 3F);
            this.xrLabel61.Name = "xrLabel61";
            this.xrLabel61.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrLabel61.SizeF = new System.Drawing.SizeF(254F, 32F);
            this.xrLabel61.StylePriority.UseBorders = false;
            this.xrLabel61.Text = "(+) Mora/Multa";
            // 
            // xrPanel12
            // 
            this.xrPanel12.Borders = DevExpress.XtraPrinting.BorderSide.Left;
            this.xrPanel12.CanGrow = false;
            this.xrPanel12.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrAgencia,
            this.xrLabel59});
            this.xrPanel12.Dpi = 254F;
            this.xrPanel12.LocationFloat = new DevExpress.Utils.PointFloat(1200F, 81F);
            this.xrPanel12.Name = "xrPanel12";
            this.xrPanel12.SizeF = new System.Drawing.SizeF(596.0001F, 69F);
            // 
            // xrAgencia
            // 
            this.xrAgencia.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrAgencia.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "Calc_AgCedente")});
            this.xrAgencia.Dpi = 254F;
            this.xrAgencia.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrAgencia.LocationFloat = new DevExpress.Utils.PointFloat(282F, 0F);
            this.xrAgencia.Name = "xrAgencia";
            this.xrAgencia.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrAgencia.SizeF = new System.Drawing.SizeF(300F, 49F);
            this.xrAgencia.StylePriority.UseBorders = false;
            this.xrAgencia.Text = "xrAgencia";
            this.xrAgencia.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            // 
            // xrLabel59
            // 
            this.xrLabel59.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel59.Dpi = 254F;
            this.xrLabel59.Font = new System.Drawing.Font("Tahoma", 5.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel59.LocationFloat = new DevExpress.Utils.PointFloat(3F, 3F);
            this.xrLabel59.Name = "xrLabel59";
            this.xrLabel59.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrLabel59.SizeF = new System.Drawing.SizeF(276F, 26F);
            this.xrLabel59.StylePriority.UseBorders = false;
            this.xrLabel59.Text = "Ag�ncia/C�digo do Benefici�rio";
            // 
            // xrPanel11
            // 
            this.xrPanel11.Borders = DevExpress.XtraPrinting.BorderSide.Top;
            this.xrPanel11.CanGrow = false;
            this.xrPanel11.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrLabelAvalista,
            this.xrLabel131,
            this.xrProprietario,
            this.xrLabel124,
            this.xrLabel123,
            this.LabBlo2,
            this.xrLabel119,
            this.LabBlo1,
            this.xrCompetencia1,
            this.rLabelcompet,
            this.xrLabel112,
            this.xrLabel111,
            this.xrLabelCPFPagador,
            this.xrLabel109});
            this.xrPanel11.Dpi = 254F;
            this.xrPanel11.LocationFloat = new DevExpress.Utils.PointFloat(2.999978F, 500F);
            this.xrPanel11.Name = "xrPanel11";
            this.xrPanel11.SizeF = new System.Drawing.SizeF(1793.001F, 158F);
            // 
            // xrLabelAvalista
            // 
            this.xrLabelAvalista.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabelAvalista.CanGrow = false;
            this.xrLabelAvalista.Dpi = 254F;
            this.xrLabelAvalista.Font = new System.Drawing.Font("Tahoma", 5.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabelAvalista.LocationFloat = new DevExpress.Utils.PointFloat(0.2964241F, 135F);
            this.xrLabelAvalista.Name = "xrLabelAvalista";
            this.xrLabelAvalista.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrLabelAvalista.SizeF = new System.Drawing.SizeF(275F, 20.61926F);
            this.xrLabelAvalista.StylePriority.UseBackColor = false;
            this.xrLabelAvalista.StylePriority.UseBorders = false;
            this.xrLabelAvalista.Text = "Sacador/Avalista:";
            // 
            // xrLabel131
            // 
            this.xrLabel131.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel131.Dpi = 254F;
            this.xrLabel131.Font = new System.Drawing.Font("Tahoma", 5.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel131.LocationFloat = new DevExpress.Utils.PointFloat(1492F, 13F);
            this.xrLabel131.Name = "xrLabel131";
            this.xrLabel131.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrLabel131.SizeF = new System.Drawing.SizeF(90F, 27.00024F);
            this.xrLabel131.StylePriority.UseBorders = false;
            this.xrLabel131.Text = "N� Neon";
            // 
            // xrProprietario
            // 
            this.xrProprietario.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrProprietario.CanGrow = false;
            this.xrProprietario.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "Calc_PI")});
            this.xrProprietario.Dpi = 254F;
            this.xrProprietario.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrProprietario.LocationFloat = new DevExpress.Utils.PointFloat(1749F, 3F);
            this.xrProprietario.Name = "xrProprietario";
            this.xrProprietario.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrProprietario.SizeF = new System.Drawing.SizeF(37F, 45F);
            this.xrProprietario.StylePriority.UseBorders = false;
            this.xrProprietario.Text = "xrProprietario";
            this.xrProprietario.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // xrLabel124
            // 
            this.xrLabel124.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel124.CanGrow = false;
            this.xrLabel124.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "BOL")});
            this.xrLabel124.Dpi = 254F;
            this.xrLabel124.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel124.LocationFloat = new DevExpress.Utils.PointFloat(1582F, 3F);
            this.xrLabel124.Name = "xrLabel124";
            this.xrLabel124.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrLabel124.SizeF = new System.Drawing.SizeF(154F, 45F);
            this.xrLabel124.StylePriority.UseBorders = false;
            this.xrLabel124.Text = "xrLabel124";
            this.xrLabel124.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // xrLabel123
            // 
            this.xrLabel123.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel123.CanGrow = false;
            this.xrLabel123.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "APTNumero")});
            this.xrLabel123.Dpi = 254F;
            this.xrLabel123.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel123.LocationFloat = new DevExpress.Utils.PointFloat(1454F, 42.99996F);
            this.xrLabel123.Name = "xrLabel123";
            this.xrLabel123.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrLabel123.SizeF = new System.Drawing.SizeF(116F, 40F);
            this.xrLabel123.StylePriority.UseBorders = false;
            this.xrLabel123.Text = "xrLabel123";
            this.xrLabel123.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // LabBlo2
            // 
            this.LabBlo2.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.LabBlo2.CanGrow = false;
            this.LabBlo2.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "BLOCodigo")});
            this.LabBlo2.Dpi = 254F;
            this.LabBlo2.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LabBlo2.LocationFloat = new DevExpress.Utils.PointFloat(1281F, 43F);
            this.LabBlo2.Name = "LabBlo2";
            this.LabBlo2.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.LabBlo2.SizeF = new System.Drawing.SizeF(106F, 40F);
            this.LabBlo2.StylePriority.UseBorders = false;
            this.LabBlo2.Text = "LabBlo2";
            this.LabBlo2.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // xrLabel119
            // 
            this.xrLabel119.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel119.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "CONNomeApto")});
            this.xrLabel119.Dpi = 254F;
            this.xrLabel119.Font = new System.Drawing.Font("Tahoma", 5.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel119.LocationFloat = new DevExpress.Utils.PointFloat(1399F, 56F);
            this.xrLabel119.Name = "xrLabel119";
            this.xrLabel119.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrLabel119.SizeF = new System.Drawing.SizeF(55F, 24.00049F);
            this.xrLabel119.StylePriority.UseBorders = false;
            this.xrLabel119.Text = "xrLabel119";
            // 
            // LabBlo1
            // 
            this.LabBlo1.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.LabBlo1.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "CONNomeBloco")});
            this.LabBlo1.Dpi = 254F;
            this.LabBlo1.Font = new System.Drawing.Font("Tahoma", 5.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LabBlo1.LocationFloat = new DevExpress.Utils.PointFloat(1193F, 56F);
            this.LabBlo1.Name = "LabBlo1";
            this.LabBlo1.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.LabBlo1.SizeF = new System.Drawing.SizeF(82F, 24.00024F);
            this.LabBlo1.StylePriority.UseBorders = false;
            this.LabBlo1.Text = "LabBlo1";
            // 
            // xrCompetencia1
            // 
            this.xrCompetencia1.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrCompetencia1.CanGrow = false;
            this.xrCompetencia1.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "Competencia")});
            this.xrCompetencia1.Dpi = 254F;
            this.xrCompetencia1.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrCompetencia1.LocationFloat = new DevExpress.Utils.PointFloat(1331F, 3F);
            this.xrCompetencia1.Name = "xrCompetencia1";
            this.xrCompetencia1.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrCompetencia1.SizeF = new System.Drawing.SizeF(156F, 40F);
            this.xrCompetencia1.StylePriority.UseBorders = false;
            this.xrCompetencia1.Text = "xrCompetencia1";
            this.xrCompetencia1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // rLabelcompet
            // 
            this.rLabelcompet.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.rLabelcompet.Dpi = 254F;
            this.rLabelcompet.Font = new System.Drawing.Font("Tahoma", 5.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.rLabelcompet.LocationFloat = new DevExpress.Utils.PointFloat(1193F, 13F);
            this.rLabelcompet.Name = "rLabelcompet";
            this.rLabelcompet.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.rLabelcompet.SizeF = new System.Drawing.SizeF(132F, 27.00024F);
            this.rLabelcompet.StylePriority.UseBorders = false;
            this.rLabelcompet.Text = "Compet�ncia";
            // 
            // xrLabel112
            // 
            this.xrLabel112.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel112.CanGrow = false;
            this.xrLabel112.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "calcCONEnderCompleto")});
            this.xrLabel112.Dpi = 254F;
            this.xrLabel112.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel112.LocationFloat = new DevExpress.Utils.PointFloat(109.4014F, 82.99978F);
            this.xrLabel112.Name = "xrLabel112";
            this.xrLabel112.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrLabel112.SizeF = new System.Drawing.SizeF(1678.58F, 36.99987F);
            this.xrLabel112.StylePriority.UseBackColor = false;
            this.xrLabel112.StylePriority.UseBorders = false;
            this.xrLabel112.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // xrLabel111
            // 
            this.xrLabel111.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel111.CanGrow = false;
            this.xrLabel111.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "CONNome")});
            this.xrLabel111.Dpi = 254F;
            this.xrLabel111.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel111.LocationFloat = new DevExpress.Utils.PointFloat(109.0066F, 43.00005F);
            this.xrLabel111.Name = "xrLabel111";
            this.xrLabel111.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrLabel111.SizeF = new System.Drawing.SizeF(1067.993F, 36.9998F);
            this.xrLabel111.StylePriority.UseBorders = false;
            this.xrLabel111.Text = "[CONNome]";
            this.xrLabel111.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // xrLabelCPFPagador
            // 
            this.xrLabelCPFPagador.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabelCPFPagador.CanGrow = false;
            this.xrLabelCPFPagador.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "NomeCPFPagador")});
            this.xrLabelCPFPagador.Dpi = 254F;
            this.xrLabelCPFPagador.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabelCPFPagador.LocationFloat = new DevExpress.Utils.PointFloat(109.4014F, 2.999984F);
            this.xrLabelCPFPagador.Name = "xrLabelCPFPagador";
            this.xrLabelCPFPagador.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrLabelCPFPagador.SizeF = new System.Drawing.SizeF(1067.599F, 37.00008F);
            this.xrLabelCPFPagador.StylePriority.UseBorders = false;
            this.xrLabelCPFPagador.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // xrLabel109
            // 
            this.xrLabel109.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel109.Dpi = 254F;
            this.xrLabel109.Font = new System.Drawing.Font("Tahoma", 5.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel109.LocationFloat = new DevExpress.Utils.PointFloat(0F, 2.999984F);
            this.xrLabel109.Name = "xrLabel109";
            this.xrLabel109.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrLabel109.SizeF = new System.Drawing.SizeF(92.56361F, 25.99986F);
            this.xrLabel109.StylePriority.UseBorders = false;
            this.xrLabel109.Text = "Pagador";
            // 
            // xrPanel10
            // 
            this.xrPanel10.BackColor = System.Drawing.Color.LightGray;
            this.xrPanel10.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrPanel10.CanGrow = false;
            this.xrPanel10.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrLabel36,
            this.xrLabel35});
            this.xrPanel10.Dpi = 254F;
            this.xrPanel10.LocationFloat = new DevExpress.Utils.PointFloat(1200F, 3F);
            this.xrPanel10.Name = "xrPanel10";
            this.xrPanel10.SizeF = new System.Drawing.SizeF(596.0001F, 78F);
            // 
            // xrLabel36
            // 
            this.xrLabel36.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel36.CanGrow = false;
            this.xrLabel36.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "BOLVencto", "{0:dd/MM/yyyy}")});
            this.xrLabel36.Dpi = 254F;
            this.xrLabel36.Font = new System.Drawing.Font("Tahoma", 15F, System.Drawing.FontStyle.Bold);
            this.xrLabel36.LocationFloat = new DevExpress.Utils.PointFloat(134F, 0F);
            this.xrLabel36.Name = "xrLabel36";
            this.xrLabel36.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrLabel36.SizeF = new System.Drawing.SizeF(460F, 72F);
            this.xrLabel36.StylePriority.UseBorders = false;
            this.xrLabel36.StylePriority.UseFont = false;
            this.xrLabel36.StylePriority.UseTextAlignment = false;
            this.xrLabel36.Text = "xrLabel36";
            this.xrLabel36.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight;
            // 
            // xrLabel35
            // 
            this.xrLabel35.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel35.Dpi = 254F;
            this.xrLabel35.Font = new System.Drawing.Font("Tahoma", 5.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel35.LocationFloat = new DevExpress.Utils.PointFloat(3F, 0F);
            this.xrLabel35.Name = "xrLabel35";
            this.xrLabel35.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrLabel35.SizeF = new System.Drawing.SizeF(127F, 21F);
            this.xrLabel35.StylePriority.UseBorders = false;
            this.xrLabel35.Text = "Vencimento";
            // 
            // xrPanel22
            // 
            this.xrPanel22.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrPanel22.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrLabel77,
            this.xrLabel78});
            this.xrPanel22.Dpi = 254F;
            this.xrPanel22.LocationFloat = new DevExpress.Utils.PointFloat(2.999999F, 81F);
            this.xrPanel22.Name = "xrPanel22";
            this.xrPanel22.SizeF = new System.Drawing.SizeF(1197F, 69F);
            this.xrPanel22.StylePriority.UseBorders = false;
            // 
            // xrLabel77
            // 
            this.xrLabel77.CanGrow = false;
            this.xrLabel77.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "Cedente")});
            this.xrLabel77.Dpi = 254F;
            this.xrLabel77.Font = new System.Drawing.Font("Tahoma", 6.75F);
            this.xrLabel77.LocationFloat = new DevExpress.Utils.PointFloat(129F, 2.000116F);
            this.xrLabel77.Multiline = true;
            this.xrLabel77.Name = "xrLabel77";
            this.xrLabel77.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrLabel77.SizeF = new System.Drawing.SizeF(1059F, 66.99987F);
            this.xrLabel77.StylePriority.UseFont = false;
            this.xrLabel77.Text = "xrLabel77";
            this.xrLabel77.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // xrLabel78
            // 
            this.xrLabel78.Dpi = 254F;
            this.xrLabel78.Font = new System.Drawing.Font("Tahoma", 5.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel78.LocationFloat = new DevExpress.Utils.PointFloat(0F, 3F);
            this.xrLabel78.Name = "xrLabel78";
            this.xrLabel78.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrLabel78.SizeF = new System.Drawing.SizeF(127F, 26F);
            this.xrLabel78.Text = "Benefici�rio";
            // 
            // xrPanel8
            // 
            this.xrPanel8.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrPanel8.CanGrow = false;
            this.xrPanel8.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrLabel34,
            this.BANCOLocal});
            this.xrPanel8.Dpi = 254F;
            this.xrPanel8.LocationFloat = new DevExpress.Utils.PointFloat(2.999978F, 3F);
            this.xrPanel8.Name = "xrPanel8";
            this.xrPanel8.SizeF = new System.Drawing.SizeF(1197F, 78F);
            // 
            // xrLabel34
            // 
            this.xrLabel34.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel34.CanGrow = false;
            this.xrLabel34.Dpi = 254F;
            this.xrLabel34.Font = new System.Drawing.Font("Tahoma", 5.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel34.LocationFloat = new DevExpress.Utils.PointFloat(0F, 0F);
            this.xrLabel34.Name = "xrLabel34";
            this.xrLabel34.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrLabel34.SizeF = new System.Drawing.SizeF(201F, 21F);
            this.xrLabel34.StylePriority.UseBorders = false;
            this.xrLabel34.Text = "Local de Pagamento";
            // 
            // BANCOLocal
            // 
            this.BANCOLocal.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.BANCOLocal.Dpi = 254F;
            this.BANCOLocal.Font = new System.Drawing.Font("Tahoma", 7F, System.Drawing.FontStyle.Bold);
            this.BANCOLocal.LocationFloat = new DevExpress.Utils.PointFloat(201F, 0F);
            this.BANCOLocal.Multiline = true;
            this.BANCOLocal.Name = "BANCOLocal";
            this.BANCOLocal.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.BANCOLocal.SizeF = new System.Drawing.SizeF(987F, 72.00024F);
            this.BANCOLocal.StylePriority.UseBorders = false;
            this.BANCOLocal.StylePriority.UseFont = false;
            this.BANCOLocal.Text = "At� o vencimento, preferencialmente na Rede Bradesco ou Bradesco Expresso, ap�s o" +
    " vencimento, somente no BRADESCO";
            this.BANCOLocal.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // LinhaDigitavel
            // 
            this.LinhaDigitavel.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "BOLCodbarstr")});
            this.LinhaDigitavel.Dpi = 254F;
            this.LinhaDigitavel.Font = new System.Drawing.Font("Verdana", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LinhaDigitavel.LocationFloat = new DevExpress.Utils.PointFloat(582F, 37F);
            this.LinhaDigitavel.Name = "LinhaDigitavel";
            this.LinhaDigitavel.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.LinhaDigitavel.SizeF = new System.Drawing.SizeF(1212F, 58F);
            this.LinhaDigitavel.Text = "LinhaDigitavel";
            // 
            // BANCONumero
            // 
            this.BANCONumero.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right)));
            this.BANCONumero.Dpi = 254F;
            this.BANCONumero.Font = new System.Drawing.Font("Verdana", 12.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BANCONumero.LocationFloat = new DevExpress.Utils.PointFloat(394F, 16F);
            this.BANCONumero.Name = "BANCONumero";
            this.BANCONumero.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.BANCONumero.SizeF = new System.Drawing.SizeF(188F, 82F);
            this.BANCONumero.StylePriority.UseFont = false;
            this.BANCONumero.Text = "237-2";
            this.BANCONumero.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // BANCONome
            // 
            this.BANCONome.Dpi = 254F;
            this.BANCONome.Font = new System.Drawing.Font("Verdana", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BANCONome.LocationFloat = new DevExpress.Utils.PointFloat(5F, 32F);
            this.BANCONome.Name = "BANCONome";
            this.BANCONome.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.BANCONome.SizeF = new System.Drawing.SizeF(381F, 55F);
            this.BANCONome.Text = "BRADESCO S/A";
            this.BANCONome.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter;
            // 
            // xrLine1
            // 
            this.xrLine1.Dpi = 254F;
            this.xrLine1.LineStyle = System.Drawing.Drawing2D.DashStyle.Dash;
            this.xrLine1.LineWidth = 5;
            this.xrLine1.LocationFloat = new DevExpress.Utils.PointFloat(0F, 0F);
            this.xrLine1.Name = "xrLine1";
            this.xrLine1.SizeF = new System.Drawing.SizeF(1791F, 13F);
            // 
            // xrLabel2x
            // 
            this.xrLabel2x.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel2x.Dpi = 254F;
            this.xrLabel2x.Font = new System.Drawing.Font("Tahoma", 5.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel2x.LocationFloat = new DevExpress.Utils.PointFloat(2.000039F, 120F);
            this.xrLabel2x.Name = "xrLabel2x";
            this.xrLabel2x.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrLabel2x.SizeF = new System.Drawing.SizeF(176.1927F, 26F);
            this.xrLabel2x.StylePriority.UseBorders = false;
            this.xrLabel2x.Text = "Sacador/Avalista:";
            // 
            // QuadroAutenticacao
            // 
            this.QuadroAutenticacao.CanGrow = false;
            this.QuadroAutenticacao.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrLabel10,
            this.xrLabelPagEnder,
            this.xrLabelNomePagador,
            this.xrLabelPagador,
            this.xrPanel5});
            this.QuadroAutenticacao.Dpi = 254F;
            this.QuadroAutenticacao.LocationFloat = new DevExpress.Utils.PointFloat(0F, 1863F);
            this.QuadroAutenticacao.Name = "QuadroAutenticacao";
            this.QuadroAutenticacao.SizeF = new System.Drawing.SizeF(1799F, 93F);
            // 
            // xrLabel10
            // 
            this.xrLabel10.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel10.CanGrow = false;
            this.xrLabel10.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "CONEnderecoComple")});
            this.xrLabel10.Dpi = 254F;
            this.xrLabel10.Font = new System.Drawing.Font("Tahoma", 6.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel10.LocationFloat = new DevExpress.Utils.PointFloat(0F, 63F);
            this.xrLabel10.Name = "xrLabel10";
            this.xrLabel10.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrLabel10.SizeF = new System.Drawing.SizeF(865.7983F, 28F);
            this.xrLabel10.StylePriority.UseBackColor = false;
            this.xrLabel10.StylePriority.UseBorders = false;
            this.xrLabel10.StylePriority.UseFont = false;
            this.xrLabel10.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // xrLabelPagEnder
            // 
            this.xrLabelPagEnder.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabelPagEnder.CanGrow = false;
            this.xrLabelPagEnder.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "CONEndereco")});
            this.xrLabelPagEnder.Dpi = 254F;
            this.xrLabelPagEnder.Font = new System.Drawing.Font("Tahoma", 6.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabelPagEnder.LocationFloat = new DevExpress.Utils.PointFloat(0F, 32.00005F);
            this.xrLabelPagEnder.Name = "xrLabelPagEnder";
            this.xrLabelPagEnder.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrLabelPagEnder.SizeF = new System.Drawing.SizeF(865.7983F, 28F);
            this.xrLabelPagEnder.StylePriority.UseBackColor = false;
            this.xrLabelPagEnder.StylePriority.UseBorders = false;
            this.xrLabelPagEnder.StylePriority.UseFont = false;
            this.xrLabelPagEnder.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // xrLabelNomePagador
            // 
            this.xrLabelNomePagador.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabelNomePagador.CanGrow = false;
            this.xrLabelNomePagador.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "NomeCPFPagador")});
            this.xrLabelNomePagador.Dpi = 254F;
            this.xrLabelNomePagador.Font = new System.Drawing.Font("Tahoma", 6.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabelNomePagador.LocationFloat = new DevExpress.Utils.PointFloat(97.79807F, 0F);
            this.xrLabelNomePagador.Name = "xrLabelNomePagador";
            this.xrLabelNomePagador.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrLabelNomePagador.SizeF = new System.Drawing.SizeF(768.0001F, 32.00005F);
            this.xrLabelNomePagador.StylePriority.UseBorders = false;
            this.xrLabelNomePagador.StylePriority.UseFont = false;
            this.xrLabelNomePagador.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // xrLabelPagador
            // 
            this.xrLabelPagador.Dpi = 254F;
            this.xrLabelPagador.Font = new System.Drawing.Font("Tahoma", 5.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabelPagador.LocationFloat = new DevExpress.Utils.PointFloat(0F, 0F);
            this.xrLabelPagador.Name = "xrLabelPagador";
            this.xrLabelPagador.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrLabelPagador.SizeF = new System.Drawing.SizeF(95.36167F, 32.00005F);
            this.xrLabelPagador.StylePriority.UseTextAlignment = false;
            this.xrLabelPagador.Text = "Pagador";
            this.xrLabelPagador.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomLeft;
            // 
            // xrPanel5
            // 
            this.xrPanel5.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right)));
            this.xrPanel5.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrLabel5,
            this.xrLabel4});
            this.xrPanel5.Dpi = 254F;
            this.xrPanel5.LocationFloat = new DevExpress.Utils.PointFloat(868F, 0F);
            this.xrPanel5.Name = "xrPanel5";
            this.xrPanel5.SizeF = new System.Drawing.SizeF(931F, 74F);
            // 
            // xrLabel5
            // 
            this.xrLabel5.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel5.Dpi = 254F;
            this.xrLabel5.Font = new System.Drawing.Font("Tahoma", 6.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel5.LocationFloat = new DevExpress.Utils.PointFloat(651F, 5F);
            this.xrLabel5.Name = "xrLabel5";
            this.xrLabel5.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrLabel5.SizeF = new System.Drawing.SizeF(238F, 42F);
            this.xrLabel5.StylePriority.UseBorders = false;
            this.xrLabel5.Text = "Recibo do Pagador";
            // 
            // xrLabel4
            // 
            this.xrLabel4.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel4.Dpi = 254F;
            this.xrLabel4.Font = new System.Drawing.Font("Tahoma", 6F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel4.LocationFloat = new DevExpress.Utils.PointFloat(19F, 5F);
            this.xrLabel4.Name = "xrLabel4";
            this.xrLabel4.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrLabel4.SizeF = new System.Drawing.SizeF(267F, 27F);
            this.xrLabel4.StylePriority.UseBorders = false;
            this.xrLabel4.Text = "Autentica��o Mec�nica";
            // 
            // RuleOcultaUsu
            // 
            this.RuleOcultaUsu.Condition = "[CONOcultaSenha] == True";
            // 
            // 
            // 
            this.RuleOcultaUsu.Formatting.Visible = DevExpress.Utils.DefaultBoolean.False;
            this.RuleOcultaUsu.Name = "RuleOcultaUsu";
            // 
            // QuadroUtil
            // 
            this.QuadroUtil.CanGrow = false;
            this.QuadroUtil.Dpi = 254F;
            this.QuadroUtil.LocationFloat = new DevExpress.Utils.PointFloat(0F, 148F);
            this.QuadroUtil.Name = "QuadroUtil";
            this.QuadroUtil.SizeF = new System.Drawing.SizeF(1799F, 1714F);
            // 
            // dImpBoletoBase1
            // 
            this.dImpBoletoBase1.DataSetName = "dImpBoletoBase";
            this.dImpBoletoBase1.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // ImpBoletoBase
            // 
            this.Bands.AddRange(new DevExpress.XtraReports.UI.Band[] {
            this.Detail,
            this.bottomMarginBand1});
            this.FormattingRuleSheet.AddRange(new DevExpress.XtraReports.UI.FormattingRule[] {
            this.RegraCompetencia,
            this.Valor_maior_99_mil,
            this.RuleOcultaUsu});
            this.Version = "16.2";
            this.DataSourceRowChanged += new DevExpress.XtraReports.UI.DataSourceRowEventHandler(this.ImpBoletoBase_DataSourceRowChanged);
            ((System.ComponentModel.ISupportInitialize)(this.dImpDestRem1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.bindingSourcePrincipal)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.bindingSource1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dImpBoletoBase1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();

        }

        #endregion

        private DevExpress.XtraReports.UI.XRPanel QuadroTopo;
        private DevExpress.XtraReports.UI.XRPictureBox xrPictureBox1;
        private DevExpress.XtraReports.UI.XRLabel xrLabel48;
        private DevExpress.XtraReports.UI.XRLabel xrLabel47;
        private DevExpress.XtraReports.UI.XRLabel NNSTR;
        private DevExpress.XtraReports.UI.XRLabel xrLabel45;
        private DevExpress.XtraReports.UI.XRLabel xrLabelBeneficiario;
        private DevExpress.XtraReports.UI.XRLabel LabApt3;
        private DevExpress.XtraReports.UI.XRLabel LabBlo3;
        private DevExpress.XtraReports.UI.XRLabel LabApt4;
        private DevExpress.XtraReports.UI.XRLabel LabBlo4;
        private DevExpress.XtraReports.UI.XRLabel xrLabel1x;
        private DevExpress.XtraReports.UI.XRLabel xrLabel9;
        private DevExpress.XtraReports.UI.XRLabel xrLabel8;
        private DevExpress.XtraReports.UI.XRLabel xrCompetencia;
        private DevExpress.XtraReports.UI.XRPanel QuadoBoleto;
        /// <summary>
        /// 
        /// </summary>
        public DevExpress.XtraReports.UI.XRLabel BANCONome;
        private DevExpress.XtraReports.UI.XRLine xrLine1;
        private DevExpress.XtraReports.UI.XRPanel xrPanel7;
        private DevExpress.XtraReports.UI.XRPanel xrPanel36;
        private DevExpress.XtraReports.UI.XRLabel xrLabel85;
        private DevExpress.XtraReports.UI.XRLabel xrLabel84;
        private DevExpress.XtraReports.UI.XRPanel xrPanel24;
        private DevExpress.XtraReports.UI.XRLabel xrLabel103;
        /// <summary>
        /// 
        /// </summary>
        public DevExpress.XtraReports.UI.XRLabel xrLabelUsoBanco;
        /// <summary>
        /// 
        /// </summary>
        public DevExpress.XtraReports.UI.XRLabel xrLabelCarteira;
        private DevExpress.XtraReports.UI.XRLine xrLine9;
        private DevExpress.XtraReports.UI.XRLine xrLine8;
        private DevExpress.XtraReports.UI.XRLine xrLine7;
        private DevExpress.XtraReports.UI.XRLine xrLine6;
        private DevExpress.XtraReports.UI.XRLabel xrLabel108;
        private DevExpress.XtraReports.UI.XRLabel xrLabel107;
        private DevExpress.XtraReports.UI.XRLabel xrLabel106;
        private DevExpress.XtraReports.UI.XRLabel xrEspecie;
        private DevExpress.XtraReports.UI.XRLabel xrLabel104;
        private DevExpress.XtraReports.UI.XRLabel xrLabel82;
        private DevExpress.XtraReports.UI.XRPanel xrPanel23;
        /// <summary>
        /// 
        /// </summary>
        public DevExpress.XtraReports.UI.XRLabel xrLabelEspecie;
        private DevExpress.XtraReports.UI.XRLabel xrLabel81;
        private DevExpress.XtraReports.UI.XRLine xrLine5;
        private DevExpress.XtraReports.UI.XRLine xrLine4;
        private DevExpress.XtraReports.UI.XRLine xrLine3;
        private DevExpress.XtraReports.UI.XRLine xrLine2;
        private DevExpress.XtraReports.UI.XRLabel xrLabel102;
        private DevExpress.XtraReports.UI.XRLabel xrLabel101;
        private DevExpress.XtraReports.UI.XRLabel xrLabel100;
        private DevExpress.XtraReports.UI.XRLabel xrLabel99;
        private DevExpress.XtraReports.UI.XRLabel xrLabel98;
        private DevExpress.XtraReports.UI.XRLabel xrLabel79;
        private DevExpress.XtraReports.UI.XRLabel xrLabel80;
        private DevExpress.XtraReports.UI.XRPanel xrPanel19;
        private DevExpress.XtraReports.UI.XRLabel NNSTR2;
        private DevExpress.XtraReports.UI.XRLabel xrLabel73;
        private DevExpress.XtraReports.UI.XRPanel xrPanel18;
        private DevExpress.XtraReports.UI.XRLabel xrLabel71;
        private DevExpress.XtraReports.UI.XRLabel xrLabel70;
        private DevExpress.XtraReports.UI.XRPanel xrPanel17;
        private DevExpress.XtraReports.UI.XRLabel xrLabel69;
        private DevExpress.XtraReports.UI.XRPanel xrPanel16;
        private DevExpress.XtraReports.UI.XRLabel xrLabel67;
        private DevExpress.XtraReports.UI.XRPanel xrPanel15;
        private DevExpress.XtraReports.UI.XRLabel xrLabel65;
        private DevExpress.XtraReports.UI.XRPanel xrPanel14;
        private DevExpress.XtraReports.UI.XRLabel xrLabel63;
        private DevExpress.XtraReports.UI.XRPanel xrPanel13;
        private DevExpress.XtraReports.UI.XRLabel xrLabel61;
        private DevExpress.XtraReports.UI.XRPanel xrPanel12;
        private DevExpress.XtraReports.UI.XRLabel xrAgencia;
        private DevExpress.XtraReports.UI.XRLabel xrLabel59;
        private DevExpress.XtraReports.UI.XRPanel xrPanel11;
        private DevExpress.XtraReports.UI.XRLabel xrLabel131;
        private DevExpress.XtraReports.UI.XRLabel xrProprietario;
        private DevExpress.XtraReports.UI.XRLabel xrLabel124;
        private DevExpress.XtraReports.UI.XRLabel xrLabel123;
        private DevExpress.XtraReports.UI.XRLabel LabBlo2;
        private DevExpress.XtraReports.UI.XRLabel xrLabel119;
        private DevExpress.XtraReports.UI.XRLabel LabBlo1;
        private DevExpress.XtraReports.UI.XRLabel xrCompetencia1;
        private DevExpress.XtraReports.UI.XRLabel xrLabel112;
        private DevExpress.XtraReports.UI.XRLabel xrLabelCPFPagador;
        private DevExpress.XtraReports.UI.XRLabel xrLabel109;
        private DevExpress.XtraReports.UI.XRPanel xrPanel10;
        private DevExpress.XtraReports.UI.XRLabel xrLabel36;
        private DevExpress.XtraReports.UI.XRLabel xrLabel35;
        private DevExpress.XtraReports.UI.XRPanel xrPanel22;
        private DevExpress.XtraReports.UI.XRLabel xrLabel77;
        private DevExpress.XtraReports.UI.XRLabel xrLabel78;
        private DevExpress.XtraReports.UI.XRPanel xrPanel8;
        private DevExpress.XtraReports.UI.XRLabel xrLabel34;
        /// <summary>
        /// 
        /// </summary>
        public DevExpress.XtraReports.UI.XRLabel BANCOLocal;
        private DevExpress.XtraReports.UI.XRLabel LinhaDigitavel;
        /// <summary>
        /// 
        /// </summary>
        public DevExpress.XtraReports.UI.XRLabel BANCONumero;
        private DevExpress.XtraReports.UI.XRLabel xrLabel33;
        private DevExpress.XtraReports.UI.XRLabel xrLabel32;
        private DevExpress.XtraReports.UI.XRLabel CodigoDeBarras;
        private DevExpress.XtraReports.UI.XRLabel NamePago;
        private DevExpress.XtraReports.UI.XRPanel QuadroAutenticacao;
        private DevExpress.XtraReports.UI.XRPanel xrPanel5;
        private DevExpress.XtraReports.UI.XRLabel xrLabel5;
        private DevExpress.XtraReports.UI.XRLabel xrLabel4;
        /// <summary>
        /// 
        /// </summary>
        protected DevExpress.XtraReports.UI.XRPanel QuadroUtil;
        /// <summary>
        /// 
        /// </summary>
        public dImpBoletoBase dImpBoletoBase1;
        /// <summary>
        /// 
        /// </summary>
        protected DevExpress.XtraReports.UI.XRLabel xrLabelcompet1;
        /// <summary>
        /// 
        /// </summary>
        protected DevExpress.XtraReports.UI.XRLabel xrLabel7;
        /// <summary>
        /// 
        /// </summary>
        protected DevExpress.XtraReports.UI.XRLabel xrLabel6;
        /// <summary>
        /// 
        /// </summary>
        protected DevExpress.XtraReports.UI.XRLabel rLabelcompet;
        private DevExpress.XtraReports.UI.XRLabel xrLabel2x;
        private DevExpress.XtraReports.UI.XRPictureBox xrLogoBanco;
        private DevExpress.XtraReports.UI.XRLabel xrLabelrecsacado;
        /// <summary>
        /// 
        /// </summary>
        public DevExpress.XtraReports.UI.XRLabel xrLabel3;
        private DevExpress.XtraReports.UI.FormattingRule RegraCompetencia;
        private DevExpress.XtraReports.UI.FormattingRule Valor_maior_99_mil;
        private DevExpress.XtraReports.UI.XRLabel xrLabel111;
        private DevExpress.XtraReports.UI.XRLabel xrCONNome;
        private DevExpress.XtraReports.UI.XRPictureBox xrLogoBanco33;
        private DevExpress.XtraReports.UI.XRLabel xrLabelAvalista;
        private DevExpress.XtraReports.UI.XRLabel xrLabelPagEnder;
        private DevExpress.XtraReports.UI.XRLabel xrLabelNomePagador;
        private DevExpress.XtraReports.UI.XRLabel xrLabelPagador;
        private DevExpress.XtraReports.UI.XRLabel xrLabelDOC2;
        private DevExpress.XtraReports.UI.XRLabel xrLabelDOC1;
        private DevExpress.XtraReports.UI.XRLabel xrLabelCab;
        private DevExpress.XtraReports.UI.XRLabel xrLEndBenef;
        /// <summary>
        /// 
        /// </summary>
        protected DevExpress.XtraReports.UI.XRPanel QuadroDadosTopo;
        private DevExpress.XtraReports.UI.XRLabel xrLEndBenefComp;
        private DevExpress.XtraReports.UI.XRLabel xrLabel10;
        private DevExpress.XtraReports.UI.XRPanel Panel_SEMCPF;
        private DevExpress.XtraReports.UI.XRLabel Label_SEMCPF;
        /// <summary>
        /// 
        /// </summary>
        public DevExpress.XtraReports.UI.FormattingRule RuleOcultaUsu;
    }
}