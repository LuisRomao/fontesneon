namespace dllImpresso
{
    partial class ImpBoletoBal
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(ImpBoletoBal));
            this.QuadroDespesas = new DevExpress.XtraReports.UI.XRPanel();
            this.Modelo_Label_Sub_TotalValG = new DevExpress.XtraReports.UI.XRLabel();
            this.Modelo_Label_Sub_TotalG = new DevExpress.XtraReports.UI.XRLabel();
            this.Modelo_PanelGrupo = new DevExpress.XtraReports.UI.XRPanel();
            this.Modelo_Label_Sub_TotalVal = new DevExpress.XtraReports.UI.XRLabel();
            this.Modelo_Label_Sub_Total = new DevExpress.XtraReports.UI.XRLabel();
            this.Modelo_PanelPorcent = new DevExpress.XtraReports.UI.XRPanel();
            this.Modelo_LinePor1 = new DevExpress.XtraReports.UI.XRLine();
            this.Modelo_LinePor2 = new DevExpress.XtraReports.UI.XRLine();
            this.Modelo_LabelValPor = new DevExpress.XtraReports.UI.XRLabel();
            this.Modelo_LabelTituloGrupo = new DevExpress.XtraReports.UI.XRLabel();
            this.Modelo_LabelValorGrupo = new DevExpress.XtraReports.UI.XRLabel();
            this.Modelo_LabelDescrisGrupo = new DevExpress.XtraReports.UI.XRLabel();
            this.ModeloSGrupo = new DevExpress.XtraReports.UI.XRLabel();
            this.QuadroContinua = new DevExpress.XtraReports.UI.XRPanel();
            this.xrLabel31 = new DevExpress.XtraReports.UI.XRLabel();
            this.QuadroTotais = new DevExpress.XtraReports.UI.XRPanel();
            this.xrLabel26 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTotalDesp = new DevExpress.XtraReports.UI.XRLabel();
            this.PDespADMA = new DevExpress.XtraReports.UI.XRPanel();
            this.DespesaValor2 = new DevExpress.XtraReports.UI.XRLabel();
            this.DespesaNome2 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel15 = new DevExpress.XtraReports.UI.XRLabel();
            this.ADMpor = new DevExpress.XtraReports.UI.XRLabel();
            this.xrPanel39 = new DevExpress.XtraReports.UI.XRPanel();
            this.ADMLine = new DevExpress.XtraReports.UI.XRLine();
            this.ADMLine1 = new DevExpress.XtraReports.UI.XRLine();
            this.xrLabel16 = new DevExpress.XtraReports.UI.XRLabel();
            this.PAdministradora = new DevExpress.XtraReports.UI.XRPanel();
            this.DespesaValorADM = new DevExpress.XtraReports.UI.XRLabel();
            this.DespesaNomeADM = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel20 = new DevExpress.XtraReports.UI.XRLabel();
            this.ADMApor = new DevExpress.XtraReports.UI.XRLabel();
            this.xrPanel40 = new DevExpress.XtraReports.UI.XRPanel();
            this.ADMALine = new DevExpress.XtraReports.UI.XRLine();
            this.ADMALine1 = new DevExpress.XtraReports.UI.XRLine();
            this.xrLabel17 = new DevExpress.XtraReports.UI.XRLabel();
            this.QuadroDesp = new DevExpress.XtraReports.UI.XRLabel();
            this.PDespG = new DevExpress.XtraReports.UI.XRPanel();
            this.xrLabel10 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrPanel6 = new DevExpress.XtraReports.UI.XRPanel();
            this.DGLine = new DevExpress.XtraReports.UI.XRLine();
            this.DGLine1 = new DevExpress.XtraReports.UI.XRLine();
            this.DGpor = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel13 = new DevExpress.XtraReports.UI.XRLabel();
            this.DespesaValor = new DevExpress.XtraReports.UI.XRLabel();
            this.DespesaNome = new DevExpress.XtraReports.UI.XRLabel();
            this.PdespMO = new DevExpress.XtraReports.UI.XRPanel();
            this.xrLabel3XX = new DevExpress.XtraReports.UI.XRLabel();
            this.xrPanel9 = new DevExpress.XtraReports.UI.XRPanel();
            this.MOLine = new DevExpress.XtraReports.UI.XRLine();
            this.MOLine1 = new DevExpress.XtraReports.UI.XRLine();
            this.MOpor = new DevExpress.XtraReports.UI.XRLabel();
            this.DespesaValor1 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel14 = new DevExpress.XtraReports.UI.XRLabel();
            this.DespesaNome1 = new DevExpress.XtraReports.UI.XRLabel();
            this.PDespB = new DevExpress.XtraReports.UI.XRPanel();
            this.xrLabel21 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrPanel41 = new DevExpress.XtraReports.UI.XRPanel();
            this.BancLine = new DevExpress.XtraReports.UI.XRLine();
            this.BancLine1 = new DevExpress.XtraReports.UI.XRLine();
            this.Bancpor = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel52 = new DevExpress.XtraReports.UI.XRLabel();
            this.DespesaNomeBANC = new DevExpress.XtraReports.UI.XRLabel();
            this.DespesaValorBANC = new DevExpress.XtraReports.UI.XRLabel();
            this.PDespH = new DevExpress.XtraReports.UI.XRPanel();
            this.DespesaNomeADV = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel23 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrPanel42 = new DevExpress.XtraReports.UI.XRPanel();
            this.HORLine = new DevExpress.XtraReports.UI.XRLine();
            this.HORLine1 = new DevExpress.XtraReports.UI.XRLine();
            this.HORpor = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel56 = new DevExpress.XtraReports.UI.XRLabel();
            this.DespesaValorADV = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel200 = new DevExpress.XtraReports.UI.XRLabel();
            this.QuadroEsquerda = new DevExpress.XtraReports.UI.XRPanel();
            this.QuadroConsumo = new DevExpress.XtraReports.UI.XRPanel();
            this.Consm3Luz = new DevExpress.XtraReports.UI.XRLabel();
            this.ConsPorcLuz = new DevExpress.XtraReports.UI.XRLabel();
            this.ConsValLuz = new DevExpress.XtraReports.UI.XRLabel();
            this.ConsPorcAgua = new DevExpress.XtraReports.UI.XRLabel();
            this.ConsValAgua = new DevExpress.XtraReports.UI.XRLabel();
            this.Consm3Agua = new DevExpress.XtraReports.UI.XRLabel();
            this.ConsLeitAgua = new DevExpress.XtraReports.UI.XRLabel();
            this.ConsPorcGas = new DevExpress.XtraReports.UI.XRLabel();
            this.ConsValGas = new DevExpress.XtraReports.UI.XRLabel();
            this.ConsKgGas = new DevExpress.XtraReports.UI.XRLabel();
            this.Consm3Gas = new DevExpress.XtraReports.UI.XRLabel();
            this.ConsLeitGas = new DevExpress.XtraReports.UI.XRLabel();
            this.ConsMes = new DevExpress.XtraReports.UI.XRLabel();
            this.SubQuadroGas = new DevExpress.XtraReports.UI.XRPanel();
            this.ConsValLuzT = new DevExpress.XtraReports.UI.XRLabel();
            this.Consm3LuzT = new DevExpress.XtraReports.UI.XRLabel();
            this.TituloGas = new DevExpress.XtraReports.UI.XRLabel();
            this.ConsValGasT = new DevExpress.XtraReports.UI.XRLabel();
            this.ConsKgGasT = new DevExpress.XtraReports.UI.XRLabel();
            this.Consm3GasT = new DevExpress.XtraReports.UI.XRLabel();
            this.ConsLeitGasT = new DevExpress.XtraReports.UI.XRLabel();
            this.ConsAguaT = new DevExpress.XtraReports.UI.XRLabel();
            this.ConsLeitAguaT = new DevExpress.XtraReports.UI.XRLabel();
            this.Consm3AguaT = new DevExpress.XtraReports.UI.XRLabel();
            this.ConsValAguaT = new DevExpress.XtraReports.UI.XRLabel();
            this.ConsLuzT = new DevExpress.XtraReports.UI.XRLabel();
            this.xrPanel45 = new DevExpress.XtraReports.UI.XRPanel();
            this.xrLabel139 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel58 = new DevExpress.XtraReports.UI.XRLabel();
            this.QuadroPendenciasUnidade = new DevExpress.XtraReports.UI.XRPanel();
            this.xrLabel76 = new DevExpress.XtraReports.UI.XRLabel();
            this.labRespBA = new DevExpress.XtraReports.UI.XRLabel();
            this.labValorBA = new DevExpress.XtraReports.UI.XRLabel();
            this.labVenctoBA = new DevExpress.XtraReports.UI.XRLabel();
            this.labCategoriaBA = new DevExpress.XtraReports.UI.XRLabel();
            this.QuadroMensagem = new DevExpress.XtraReports.UI.XRPanel();
            this.xrLabel118 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrRichText1 = new DevExpress.XtraReports.UI.XRRichText();
            this.QuadroSaldo = new DevExpress.XtraReports.UI.XRPanel();
            this.xrRentabilidade = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTransferencias = new DevExpress.XtraReports.UI.XRLabel();
            this.xrDebitos = new DevExpress.XtraReports.UI.XRLabel();
            this.xrCreditos = new DevExpress.XtraReports.UI.XRLabel();
            this.xrSaldoInicial = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel18 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrPanel38 = new DevExpress.XtraReports.UI.XRPanel();
            this.xrRentabilidadeT = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTransferenciaT = new DevExpress.XtraReports.UI.XRLabel();
            this.xrDebitosT = new DevExpress.XtraReports.UI.XRLabel();
            this.xrCreditosT = new DevExpress.XtraReports.UI.XRLabel();
            this.xrDataSaldoFinal = new DevExpress.XtraReports.UI.XRLabel();
            this.xrDataSaldoInicial = new DevExpress.XtraReports.UI.XRLabel();
            this.xrSaldoFinal = new DevExpress.XtraReports.UI.XRLabel();
            this.xrContasLogicas = new DevExpress.XtraReports.UI.XRLabel();
            this.QuadroReceitas = new DevExpress.XtraReports.UI.XRPanel();
            this.xrPrevXEf2 = new DevExpress.XtraReports.UI.XRLabel();
            this.OcultaPre = new DevExpress.XtraReports.UI.FormattingRule();
            this.xrPrevXEf1 = new DevExpress.XtraReports.UI.XRLabel();
            this.LabelHono = new DevExpress.XtraReports.UI.XRLabel();
            this.SubTotHono = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel54 = new DevExpress.XtraReports.UI.XRLabel();
            this.TotR = new DevExpress.XtraReports.UI.XRLabel();
            this.xrPanel2 = new DevExpress.XtraReports.UI.XRPanel();
            this.ReceitaTT = new DevExpress.XtraReports.UI.XRLabel();
            this.RecetaPT = new DevExpress.XtraReports.UI.XRLabel();
            this.ReceitaMesT = new DevExpress.XtraReports.UI.XRLabel();
            this.ReceitaAT = new DevExpress.XtraReports.UI.XRLabel();
            this.ReceitaPrevT = new DevExpress.XtraReports.UI.XRLabel();
            this.ReceitaPrev = new DevExpress.XtraReports.UI.XRLabel();
            this.xrPanel37 = new DevExpress.XtraReports.UI.XRPanel();
            this.xrLine11 = new DevExpress.XtraReports.UI.XRLine();
            this.xrLabel29 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel22 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel40 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel39 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel38 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel37 = new DevExpress.XtraReports.UI.XRLabel();
            this.RecetaP = new DevExpress.XtraReports.UI.XRLabel();
            this.ReceitaMes = new DevExpress.XtraReports.UI.XRLabel();
            this.ReceitaA = new DevExpress.XtraReports.UI.XRLabel();
            this.ReceitaT = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel12 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel28 = new DevExpress.XtraReports.UI.XRLabel();
            this.ReceitaNome = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabelPxR = new DevExpress.XtraReports.UI.XRLabel();
            this.QuadroComposicaoBoleto = new DevExpress.XtraReports.UI.XRPanel();
            this.Lista11 = new DevExpress.XtraReports.UI.XRLabel();
            this.Lista12 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel120 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel116 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrPanel4 = new DevExpress.XtraReports.UI.XRPanel();
            this.xrLabel113 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel105 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel83 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel30 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel27 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel72 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrMensagem = new DevExpress.XtraReports.UI.XRLabel();
            this.QuadroUnidadeInadimplentes = new DevExpress.XtraReports.UI.XRPanel();
            this.TituloUnidadesInad = new DevExpress.XtraReports.UI.XRLabel();
            this.TextoUnidadesInadimplentes = new DevExpress.XtraReports.UI.XRRichText();
            this.QuadroContinuacao = new DevExpress.XtraReports.UI.XRPanel();
            this.TituloContinuacao = new DevExpress.XtraReports.UI.XRLabel();
            this.OcultaPrevRea = new DevExpress.XtraReports.Parameters.Parameter();
            this.xrLabel19 = new DevExpress.XtraReports.UI.XRLabel();
            this.SaldoFinal = new DevExpress.XtraReports.UI.XRLabel();
            ((System.ComponentModel.ISupportInitialize)(this.dImpBoletoBase1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dImpDestRem1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.bindingSourcePrincipal)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.bindingSource1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrRichText1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TextoUnidadesInadimplentes)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            // 
            // xrLabelUsoBanco
            // 
            this.xrLabelUsoBanco.StylePriority.UseBorders = false;
            // 
            // xrLabelCarteira
            // 
            this.xrLabelCarteira.StylePriority.UseBorders = false;
            // 
            // xrLabelEspecie
            // 
            this.xrLabelEspecie.StylePriority.UseBorders = false;
            // 
            // BANCOLocal
            // 
            this.BANCOLocal.StylePriority.UseBorders = false;
            this.BANCOLocal.StylePriority.UseFont = false;
            // 
            // BANCONumero
            // 
            this.BANCONumero.StylePriority.UseFont = false;
            // 
            // QuadroUtil
            // 
            this.QuadroUtil.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.QuadroEsquerda,
            this.QuadroDespesas});
            // 
            // rLabelcompet
            // 
            this.rLabelcompet.StylePriority.UseBorders = false;
            // 
            // xrLabel3
            // 
            this.xrLabel3.StylePriority.UseBorders = false;                        
            // 
            // QuadroSegundaPagina
            // 
            this.QuadroSegundaPagina.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.QuadroContinuacao,
            this.QuadroUnidadeInadimplentes});
            this.QuadroSegundaPagina.StylePriority.UseBackColor = false;
            // 
            // Detail
            // 
            this.Detail.StylePriority.UseTextAlignment = false;
            // 
            // xrSigla1
            // 
            this.xrSigla1.StylePriority.UseFont = false;
            this.xrSigla1.StylePriority.UseTextAlignment = false;
            // 
            // QuadroDespesas
            // 
            this.QuadroDespesas.CanGrow = false;
            this.QuadroDespesas.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.Modelo_Label_Sub_TotalValG,
            this.Modelo_Label_Sub_TotalG,
            this.Modelo_PanelGrupo,
            this.ModeloSGrupo,
            this.QuadroContinua,
            this.QuadroTotais,
            this.PDespADMA,
            this.PAdministradora,
            this.QuadroDesp,
            this.PDespG,
            this.PdespMO,
            this.PDespB,
            this.PDespH});
            this.QuadroDespesas.Dpi = 254F;
            this.QuadroDespesas.LocationFloat = new DevExpress.Utils.PointFloat(1094F, 5F);
            this.QuadroDespesas.Name = "QuadroDespesas";
            this.QuadroDespesas.SizeF = new System.Drawing.SizeF(703F, 1704F);
            this.QuadroDespesas.StylePriority.UseBorderColor = false;
            this.QuadroDespesas.StylePriority.UseBorders = false;
            // 
            // Modelo_Label_Sub_TotalValG
            // 
            this.Modelo_Label_Sub_TotalValG.BackColor = System.Drawing.Color.DarkGray;
            this.Modelo_Label_Sub_TotalValG.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.Modelo_Label_Sub_TotalValG.CanGrow = false;
            this.Modelo_Label_Sub_TotalValG.Dpi = 254F;
            this.Modelo_Label_Sub_TotalValG.Font = new System.Drawing.Font("Tahoma", 6F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Modelo_Label_Sub_TotalValG.ForeColor = System.Drawing.Color.White;
            this.Modelo_Label_Sub_TotalValG.LocationFloat = new DevExpress.Utils.PointFloat(561.9998F, 662F);
            this.Modelo_Label_Sub_TotalValG.Name = "Modelo_Label_Sub_TotalValG";
            this.Modelo_Label_Sub_TotalValG.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.Modelo_Label_Sub_TotalValG.SizeF = new System.Drawing.SizeF(138F, 19.00012F);
            this.Modelo_Label_Sub_TotalValG.StylePriority.UseBackColor = false;
            this.Modelo_Label_Sub_TotalValG.StylePriority.UseBorders = false;
            this.Modelo_Label_Sub_TotalValG.StylePriority.UseFont = false;
            this.Modelo_Label_Sub_TotalValG.StylePriority.UseForeColor = false;
            this.Modelo_Label_Sub_TotalValG.StylePriority.UseTextAlignment = false;
            this.Modelo_Label_Sub_TotalValG.Text = "00.00";
            this.Modelo_Label_Sub_TotalValG.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.Modelo_Label_Sub_TotalValG.Visible = false;
            this.Modelo_Label_Sub_TotalValG.WordWrap = false;
            // 
            // Modelo_Label_Sub_TotalG
            // 
            this.Modelo_Label_Sub_TotalG.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.Modelo_Label_Sub_TotalG.CanGrow = false;
            this.Modelo_Label_Sub_TotalG.Dpi = 254F;
            this.Modelo_Label_Sub_TotalG.Font = new System.Drawing.Font("Tahoma", 3.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Modelo_Label_Sub_TotalG.LocationFloat = new DevExpress.Utils.PointFloat(47.98446F, 663.0001F);
            this.Modelo_Label_Sub_TotalG.Name = "Modelo_Label_Sub_TotalG";
            this.Modelo_Label_Sub_TotalG.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.Modelo_Label_Sub_TotalG.SizeF = new System.Drawing.SizeF(501.0155F, 18.00006F);
            this.Modelo_Label_Sub_TotalG.StylePriority.UseBorders = false;
            this.Modelo_Label_Sub_TotalG.StylePriority.UseTextAlignment = false;
            this.Modelo_Label_Sub_TotalG.Text = "Sub Total";
            this.Modelo_Label_Sub_TotalG.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.Modelo_Label_Sub_TotalG.Visible = false;
            // 
            // Modelo_PanelGrupo
            // 
            this.Modelo_PanelGrupo.BorderColor = System.Drawing.Color.Silver;
            this.Modelo_PanelGrupo.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.Modelo_PanelGrupo.CanGrow = false;
            this.Modelo_PanelGrupo.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.Modelo_Label_Sub_TotalVal,
            this.Modelo_Label_Sub_Total,
            this.Modelo_PanelPorcent,
            this.Modelo_LabelValPor,
            this.Modelo_LabelTituloGrupo,
            this.Modelo_LabelValorGrupo,
            this.Modelo_LabelDescrisGrupo});
            this.Modelo_PanelGrupo.Dpi = 254F;
            this.Modelo_PanelGrupo.LocationFloat = new DevExpress.Utils.PointFloat(0F, 579.9999F);
            this.Modelo_PanelGrupo.Name = "Modelo_PanelGrupo";
            this.Modelo_PanelGrupo.SizeF = new System.Drawing.SizeF(699.9998F, 80F);
            this.Modelo_PanelGrupo.Visible = false;
            // 
            // Modelo_Label_Sub_TotalVal
            // 
            this.Modelo_Label_Sub_TotalVal.BackColor = System.Drawing.Color.Gainsboro;
            this.Modelo_Label_Sub_TotalVal.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.Modelo_Label_Sub_TotalVal.CanGrow = false;
            this.Modelo_Label_Sub_TotalVal.Dpi = 254F;
            this.Modelo_Label_Sub_TotalVal.Font = new System.Drawing.Font("Tahoma", 5.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Modelo_Label_Sub_TotalVal.LocationFloat = new DevExpress.Utils.PointFloat(561.9999F, 58F);
            this.Modelo_Label_Sub_TotalVal.Name = "Modelo_Label_Sub_TotalVal";
            this.Modelo_Label_Sub_TotalVal.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.Modelo_Label_Sub_TotalVal.SizeF = new System.Drawing.SizeF(138F, 19.00006F);
            this.Modelo_Label_Sub_TotalVal.StylePriority.UseBackColor = false;
            this.Modelo_Label_Sub_TotalVal.StylePriority.UseBorders = false;
            this.Modelo_Label_Sub_TotalVal.StylePriority.UseFont = false;
            this.Modelo_Label_Sub_TotalVal.StylePriority.UseTextAlignment = false;
            this.Modelo_Label_Sub_TotalVal.Text = "DespesaValor";
            this.Modelo_Label_Sub_TotalVal.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomRight;
            this.Modelo_Label_Sub_TotalVal.WordWrap = false;
            // 
            // Modelo_Label_Sub_Total
            // 
            this.Modelo_Label_Sub_Total.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.Modelo_Label_Sub_Total.CanGrow = false;
            this.Modelo_Label_Sub_Total.Dpi = 254F;
            this.Modelo_Label_Sub_Total.Font = new System.Drawing.Font("Tahoma", 3.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Modelo_Label_Sub_Total.LocationFloat = new DevExpress.Utils.PointFloat(456.0001F, 57F);
            this.Modelo_Label_Sub_Total.Name = "Modelo_Label_Sub_Total";
            this.Modelo_Label_Sub_Total.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.Modelo_Label_Sub_Total.SizeF = new System.Drawing.SizeF(93F, 18.00006F);
            this.Modelo_Label_Sub_Total.StylePriority.UseBorders = false;
            this.Modelo_Label_Sub_Total.StylePriority.UseTextAlignment = false;
            this.Modelo_Label_Sub_Total.Text = "Sub Total";
            this.Modelo_Label_Sub_Total.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomRight;
            // 
            // Modelo_PanelPorcent
            // 
            this.Modelo_PanelPorcent.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.Modelo_PanelPorcent.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.Modelo_LinePor1,
            this.Modelo_LinePor2});
            this.Modelo_PanelPorcent.Dpi = 254F;
            this.Modelo_PanelPorcent.LocationFloat = new DevExpress.Utils.PointFloat(320F, 11F);
            this.Modelo_PanelPorcent.Name = "Modelo_PanelPorcent";
            this.Modelo_PanelPorcent.SizeF = new System.Drawing.SizeF(375F, 11F);
            this.Modelo_PanelPorcent.StylePriority.UseBorders = false;
            // 
            // Modelo_LinePor1
            // 
            this.Modelo_LinePor1.BorderColor = System.Drawing.Color.Silver;
            this.Modelo_LinePor1.Dpi = 254F;
            this.Modelo_LinePor1.ForeColor = System.Drawing.Color.Black;
            this.Modelo_LinePor1.LineWidth = 10;
            this.Modelo_LinePor1.LocationFloat = new DevExpress.Utils.PointFloat(0F, 0F);
            this.Modelo_LinePor1.Name = "Modelo_LinePor1";
            this.Modelo_LinePor1.SizeF = new System.Drawing.SizeF(100F, 10F);
            this.Modelo_LinePor1.StylePriority.UseBorderColor = false;
            this.Modelo_LinePor1.StylePriority.UseForeColor = false;
            // 
            // Modelo_LinePor2
            // 
            this.Modelo_LinePor2.Dpi = 254F;
            this.Modelo_LinePor2.ForeColor = System.Drawing.Color.Black;
            this.Modelo_LinePor2.LineWidth = 3;
            this.Modelo_LinePor2.LocationFloat = new DevExpress.Utils.PointFloat(185F, 3F);
            this.Modelo_LinePor2.Name = "Modelo_LinePor2";
            this.Modelo_LinePor2.SizeF = new System.Drawing.SizeF(185F, 5F);
            this.Modelo_LinePor2.StylePriority.UseForeColor = false;
            // 
            // Modelo_LabelValPor
            // 
            this.Modelo_LabelValPor.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.Modelo_LabelValPor.CanGrow = false;
            this.Modelo_LabelValPor.Dpi = 254F;
            this.Modelo_LabelValPor.Font = new System.Drawing.Font("Tahoma", 5.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Modelo_LabelValPor.LocationFloat = new DevExpress.Utils.PointFloat(240F, 2.999939F);
            this.Modelo_LabelValPor.Name = "Modelo_LabelValPor";
            this.Modelo_LabelValPor.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.Modelo_LabelValPor.SizeF = new System.Drawing.SizeF(75F, 27F);
            this.Modelo_LabelValPor.StylePriority.UseBorders = false;
            this.Modelo_LabelValPor.Text = "10.0%";
            this.Modelo_LabelValPor.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight;
            this.Modelo_LabelValPor.WordWrap = false;
            // 
            // Modelo_LabelTituloGrupo
            // 
            this.Modelo_LabelTituloGrupo.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.Modelo_LabelTituloGrupo.CanGrow = false;
            this.Modelo_LabelTituloGrupo.Dpi = 254F;
            this.Modelo_LabelTituloGrupo.Font = new System.Drawing.Font("Tahoma", 6.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Modelo_LabelTituloGrupo.LocationFloat = new DevExpress.Utils.PointFloat(3.000122F, 3F);
            this.Modelo_LabelTituloGrupo.Name = "Modelo_LabelTituloGrupo";
            this.Modelo_LabelTituloGrupo.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.Modelo_LabelTituloGrupo.SizeF = new System.Drawing.SizeF(236.9998F, 27F);
            this.Modelo_LabelTituloGrupo.StylePriority.UseBorders = false;
            this.Modelo_LabelTituloGrupo.Text = "Gerais";
            this.Modelo_LabelTituloGrupo.WordWrap = false;
            // 
            // Modelo_LabelValorGrupo
            // 
            this.Modelo_LabelValorGrupo.BackColor = System.Drawing.Color.WhiteSmoke;
            this.Modelo_LabelValorGrupo.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.Modelo_LabelValorGrupo.CanGrow = false;
            this.Modelo_LabelValorGrupo.Dpi = 254F;
            this.Modelo_LabelValorGrupo.Font = new System.Drawing.Font("Tahoma", 5.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Modelo_LabelValorGrupo.LocationFloat = new DevExpress.Utils.PointFloat(597.9999F, 30F);
            this.Modelo_LabelValorGrupo.Name = "Modelo_LabelValorGrupo";
            this.Modelo_LabelValorGrupo.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.Modelo_LabelValorGrupo.SizeF = new System.Drawing.SizeF(102.0001F, 23F);
            this.Modelo_LabelValorGrupo.StylePriority.UseBorders = false;
            this.Modelo_LabelValorGrupo.Text = "DespesaValor";
            this.Modelo_LabelValorGrupo.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight;
            this.Modelo_LabelValorGrupo.WordWrap = false;
            this.Modelo_LabelValorGrupo.Draw += new DevExpress.XtraReports.UI.DrawEventHandler(this.DegradeL);
            // 
            // Modelo_LabelDescrisGrupo
            // 
            this.Modelo_LabelDescrisGrupo.BorderColor = System.Drawing.Color.Silver;
            this.Modelo_LabelDescrisGrupo.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.Modelo_LabelDescrisGrupo.CanGrow = false;
            this.Modelo_LabelDescrisGrupo.Dpi = 254F;
            this.Modelo_LabelDescrisGrupo.Font = new System.Drawing.Font("Tahoma", 5.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Modelo_LabelDescrisGrupo.LocationFloat = new DevExpress.Utils.PointFloat(18F, 30F);
            this.Modelo_LabelDescrisGrupo.Name = "Modelo_LabelDescrisGrupo";
            this.Modelo_LabelDescrisGrupo.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.Modelo_LabelDescrisGrupo.SizeF = new System.Drawing.SizeF(580F, 22F);
            this.Modelo_LabelDescrisGrupo.StylePriority.UseBorders = false;
            this.Modelo_LabelDescrisGrupo.Text = "DespesaNome";
            this.Modelo_LabelDescrisGrupo.WordWrap = false;
            // 
            // ModeloSGrupo
            // 
            this.ModeloSGrupo.BackColor = System.Drawing.Color.DarkGray;
            this.ModeloSGrupo.BorderColor = System.Drawing.Color.Black;
            this.ModeloSGrupo.BorderDashStyle = DevExpress.XtraPrinting.BorderDashStyle.Double;
            this.ModeloSGrupo.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.ModeloSGrupo.BorderWidth = 2F;
            this.ModeloSGrupo.CanGrow = false;
            this.ModeloSGrupo.Dpi = 254F;
            this.ModeloSGrupo.Font = new System.Drawing.Font("Tahoma", 6.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ModeloSGrupo.ForeColor = System.Drawing.Color.White;
            this.ModeloSGrupo.LocationFloat = new DevExpress.Utils.PointFloat(0F, 525.6124F);
            this.ModeloSGrupo.Name = "ModeloSGrupo";
            this.ModeloSGrupo.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.ModeloSGrupo.SizeF = new System.Drawing.SizeF(700.0001F, 48.31366F);
            this.ModeloSGrupo.StylePriority.UseBackColor = false;
            this.ModeloSGrupo.StylePriority.UseBorderColor = false;
            this.ModeloSGrupo.StylePriority.UseBorderDashStyle = false;
            this.ModeloSGrupo.StylePriority.UseBorders = false;
            this.ModeloSGrupo.StylePriority.UseBorderWidth = false;
            this.ModeloSGrupo.StylePriority.UseForeColor = false;
            this.ModeloSGrupo.StylePriority.UseTextAlignment = false;
            this.ModeloSGrupo.Text = "Gerais";
            this.ModeloSGrupo.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.ModeloSGrupo.Visible = false;
            // 
            // QuadroContinua
            // 
            this.QuadroContinua.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrLabel31});
            this.QuadroContinua.Dpi = 254F;
            this.QuadroContinua.LocationFloat = new DevExpress.Utils.PointFloat(5F, 444F);
            this.QuadroContinua.Name = "QuadroContinua";
            this.QuadroContinua.SizeF = new System.Drawing.SizeF(254F, 32F);
            this.QuadroContinua.Visible = false;
            // 
            // xrLabel31
            // 
            this.xrLabel31.Dpi = 254F;
            this.xrLabel31.Font = new System.Drawing.Font("Tahoma", 6.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel31.LocationFloat = new DevExpress.Utils.PointFloat(0F, 0F);
            this.xrLabel31.Name = "xrLabel31";
            this.xrLabel31.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrLabel31.SizeF = new System.Drawing.SizeF(233F, 29F);
            this.xrLabel31.Text = "Continua no verso";
            // 
            // QuadroTotais
            // 
            this.QuadroTotais.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrLabel26,
            this.xrTotalDesp});
            this.QuadroTotais.Dpi = 254F;
            this.QuadroTotais.LocationFloat = new DevExpress.Utils.PointFloat(265F, 444F);
            this.QuadroTotais.Name = "QuadroTotais";
            this.QuadroTotais.SizeF = new System.Drawing.SizeF(435F, 29F);
            // 
            // xrLabel26
            // 
            this.xrLabel26.Dpi = 254F;
            this.xrLabel26.Font = new System.Drawing.Font("Tahoma", 6.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel26.LocationFloat = new DevExpress.Utils.PointFloat(56F, 0F);
            this.xrLabel26.Name = "xrLabel26";
            this.xrLabel26.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrLabel26.SizeF = new System.Drawing.SizeF(185F, 29F);
            this.xrLabel26.Text = "TOTAL GERAL";
            // 
            // xrTotalDesp
            // 
            this.xrTotalDesp.Dpi = 254F;
            this.xrTotalDesp.Font = new System.Drawing.Font("Tahoma", 6.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrTotalDesp.LocationFloat = new DevExpress.Utils.PointFloat(247F, 0F);
            this.xrTotalDesp.Name = "xrTotalDesp";
            this.xrTotalDesp.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTotalDesp.SizeF = new System.Drawing.SizeF(187F, 29F);
            this.xrTotalDesp.Text = "TOTAL";
            this.xrTotalDesp.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight;
            // 
            // PDespADMA
            // 
            this.PDespADMA.BorderColor = System.Drawing.Color.Silver;
            this.PDespADMA.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.PDespADMA.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.DespesaValor2,
            this.DespesaNome2,
            this.xrLabel15,
            this.ADMpor,
            this.xrPanel39,
            this.xrLabel16});
            this.PDespADMA.Dpi = 254F;
            this.PDespADMA.LocationFloat = new DevExpress.Utils.PointFloat(0F, 169F);
            this.PDespADMA.Name = "PDespADMA";
            this.PDespADMA.SizeF = new System.Drawing.SizeF(700F, 64F);
            this.PDespADMA.Visible = false;
            // 
            // DespesaValor2
            // 
            this.DespesaValor2.BackColor = System.Drawing.Color.WhiteSmoke;
            this.DespesaValor2.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.DespesaValor2.Dpi = 254F;
            this.DespesaValor2.Font = new System.Drawing.Font("Tahoma", 5.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DespesaValor2.LocationFloat = new DevExpress.Utils.PointFloat(562F, 26F);
            this.DespesaValor2.Multiline = true;
            this.DespesaValor2.Name = "DespesaValor2";
            this.DespesaValor2.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.DespesaValor2.SizeF = new System.Drawing.SizeF(138F, 30F);
            this.DespesaValor2.StylePriority.UseBorders = false;
            this.DespesaValor2.Text = "DespesaValor2";
            this.DespesaValor2.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight;
            this.DespesaValor2.Draw += new DevExpress.XtraReports.UI.DrawEventHandler(this.DegradeL);
            // 
            // DespesaNome2
            // 
            this.DespesaNome2.BorderColor = System.Drawing.Color.Silver;
            this.DespesaNome2.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.DespesaNome2.Dpi = 254F;
            this.DespesaNome2.Font = new System.Drawing.Font("Tahoma", 5.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DespesaNome2.LocationFloat = new DevExpress.Utils.PointFloat(3F, 26F);
            this.DespesaNome2.Multiline = true;
            this.DespesaNome2.Name = "DespesaNome2";
            this.DespesaNome2.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.DespesaNome2.SizeF = new System.Drawing.SizeF(558F, 16F);
            this.DespesaNome2.StylePriority.UseBorders = false;
            this.DespesaNome2.Text = "DespesaNome2";
            this.DespesaNome2.WordWrap = false;
            // 
            // xrLabel15
            // 
            this.xrLabel15.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel15.Dpi = 254F;
            this.xrLabel15.Font = new System.Drawing.Font("Tahoma", 6.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel15.LocationFloat = new DevExpress.Utils.PointFloat(3F, 3F);
            this.xrLabel15.Name = "xrLabel15";
            this.xrLabel15.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrLabel15.SizeF = new System.Drawing.SizeF(217F, 23F);
            this.xrLabel15.StylePriority.UseBorders = false;
            this.xrLabel15.Text = "Administrativas";
            // 
            // ADMpor
            // 
            this.ADMpor.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.ADMpor.Dpi = 254F;
            this.ADMpor.Font = new System.Drawing.Font("Tahoma", 5.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ADMpor.LocationFloat = new DevExpress.Utils.PointFloat(225F, 3F);
            this.ADMpor.Name = "ADMpor";
            this.ADMpor.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.ADMpor.SizeF = new System.Drawing.SizeF(75F, 21F);
            this.ADMpor.StylePriority.UseBorders = false;
            this.ADMpor.Text = "100%";
            this.ADMpor.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight;
            // 
            // xrPanel39
            // 
            this.xrPanel39.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrPanel39.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.ADMLine,
            this.ADMLine1});
            this.xrPanel39.Dpi = 254F;
            this.xrPanel39.LocationFloat = new DevExpress.Utils.PointFloat(310F, 8F);
            this.xrPanel39.Name = "xrPanel39";
            this.xrPanel39.SizeF = new System.Drawing.SizeF(375F, 11F);
            this.xrPanel39.StylePriority.UseBorders = false;
            // 
            // ADMLine
            // 
            this.ADMLine.Dpi = 254F;
            this.ADMLine.ForeColor = System.Drawing.Color.Silver;
            this.ADMLine.LineWidth = 10;
            this.ADMLine.LocationFloat = new DevExpress.Utils.PointFloat(0F, 0F);
            this.ADMLine.Name = "ADMLine";
            this.ADMLine.SizeF = new System.Drawing.SizeF(100F, 10F);
            // 
            // ADMLine1
            // 
            this.ADMLine1.Dpi = 254F;
            this.ADMLine1.ForeColor = System.Drawing.Color.LightGray;
            this.ADMLine1.LineWidth = 3;
            this.ADMLine1.LocationFloat = new DevExpress.Utils.PointFloat(132F, 3F);
            this.ADMLine1.Name = "ADMLine1";
            this.ADMLine1.SizeF = new System.Drawing.SizeF(238F, 5F);
            // 
            // xrLabel16
            // 
            this.xrLabel16.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel16.Dpi = 254F;
            this.xrLabel16.Font = new System.Drawing.Font("Tahoma", 3.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel16.LocationFloat = new DevExpress.Utils.PointFloat(456F, 45F);
            this.xrLabel16.Name = "xrLabel16";
            this.xrLabel16.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrLabel16.SizeF = new System.Drawing.SizeF(93F, 16F);
            this.xrLabel16.StylePriority.UseBorders = false;
            this.xrLabel16.Text = "Sub Total";
            this.xrLabel16.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight;
            // 
            // PAdministradora
            // 
            this.PAdministradora.BorderColor = System.Drawing.Color.Silver;
            this.PAdministradora.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.PAdministradora.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.DespesaValorADM,
            this.DespesaNomeADM,
            this.xrLabel20,
            this.ADMApor,
            this.xrPanel40,
            this.xrLabel17});
            this.PAdministradora.Dpi = 254F;
            this.PAdministradora.LocationFloat = new DevExpress.Utils.PointFloat(0F, 238F);
            this.PAdministradora.Name = "PAdministradora";
            this.PAdministradora.SizeF = new System.Drawing.SizeF(700F, 69F);
            this.PAdministradora.Visible = false;
            // 
            // DespesaValorADM
            // 
            this.DespesaValorADM.BackColor = System.Drawing.Color.WhiteSmoke;
            this.DespesaValorADM.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.DespesaValorADM.Dpi = 254F;
            this.DespesaValorADM.Font = new System.Drawing.Font("Tahoma", 5.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DespesaValorADM.LocationFloat = new DevExpress.Utils.PointFloat(562F, 26F);
            this.DespesaValorADM.Multiline = true;
            this.DespesaValorADM.Name = "DespesaValorADM";
            this.DespesaValorADM.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.DespesaValorADM.SizeF = new System.Drawing.SizeF(138F, 37F);
            this.DespesaValorADM.StylePriority.UseBorders = false;
            this.DespesaValorADM.Text = "DespesaValorADM";
            this.DespesaValorADM.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight;
            this.DespesaValorADM.Draw += new DevExpress.XtraReports.UI.DrawEventHandler(this.DegradeL);
            // 
            // DespesaNomeADM
            // 
            this.DespesaNomeADM.BorderColor = System.Drawing.Color.Silver;
            this.DespesaNomeADM.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.DespesaNomeADM.Dpi = 254F;
            this.DespesaNomeADM.Font = new System.Drawing.Font("Tahoma", 5.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DespesaNomeADM.LocationFloat = new DevExpress.Utils.PointFloat(3F, 26F);
            this.DespesaNomeADM.Multiline = true;
            this.DespesaNomeADM.Name = "DespesaNomeADM";
            this.DespesaNomeADM.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.DespesaNomeADM.SizeF = new System.Drawing.SizeF(558F, 20F);
            this.DespesaNomeADM.StylePriority.UseBorders = false;
            this.DespesaNomeADM.Text = "DespesaNomeADM";
            this.DespesaNomeADM.WordWrap = false;
            // 
            // xrLabel20
            // 
            this.xrLabel20.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel20.Dpi = 254F;
            this.xrLabel20.Font = new System.Drawing.Font("Tahoma", 6.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel20.LocationFloat = new DevExpress.Utils.PointFloat(3F, 3F);
            this.xrLabel20.Name = "xrLabel20";
            this.xrLabel20.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrLabel20.SizeF = new System.Drawing.SizeF(210F, 23F);
            this.xrLabel20.StylePriority.UseBorders = false;
            this.xrLabel20.Text = "Administradora";
            // 
            // ADMApor
            // 
            this.ADMApor.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.ADMApor.Dpi = 254F;
            this.ADMApor.Font = new System.Drawing.Font("Tahoma", 5.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ADMApor.LocationFloat = new DevExpress.Utils.PointFloat(225F, 3F);
            this.ADMApor.Name = "ADMApor";
            this.ADMApor.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.ADMApor.SizeF = new System.Drawing.SizeF(75F, 21F);
            this.ADMApor.StylePriority.UseBorders = false;
            this.ADMApor.Text = "100%";
            this.ADMApor.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight;
            // 
            // xrPanel40
            // 
            this.xrPanel40.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrPanel40.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.ADMALine,
            this.ADMALine1});
            this.xrPanel40.Dpi = 254F;
            this.xrPanel40.LocationFloat = new DevExpress.Utils.PointFloat(310F, 8F);
            this.xrPanel40.Name = "xrPanel40";
            this.xrPanel40.SizeF = new System.Drawing.SizeF(375F, 11F);
            this.xrPanel40.StylePriority.UseBorders = false;
            // 
            // ADMALine
            // 
            this.ADMALine.Dpi = 254F;
            this.ADMALine.ForeColor = System.Drawing.Color.Silver;
            this.ADMALine.LineWidth = 10;
            this.ADMALine.LocationFloat = new DevExpress.Utils.PointFloat(0F, 0F);
            this.ADMALine.Name = "ADMALine";
            this.ADMALine.SizeF = new System.Drawing.SizeF(100F, 10F);
            // 
            // ADMALine1
            // 
            this.ADMALine1.Dpi = 254F;
            this.ADMALine1.ForeColor = System.Drawing.Color.LightGray;
            this.ADMALine1.LineWidth = 3;
            this.ADMALine1.LocationFloat = new DevExpress.Utils.PointFloat(132F, 3F);
            this.ADMALine1.Name = "ADMALine1";
            this.ADMALine1.SizeF = new System.Drawing.SizeF(222F, 6F);
            // 
            // xrLabel17
            // 
            this.xrLabel17.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel17.Dpi = 254F;
            this.xrLabel17.Font = new System.Drawing.Font("Tahoma", 3.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel17.LocationFloat = new DevExpress.Utils.PointFloat(456F, 48F);
            this.xrLabel17.Name = "xrLabel17";
            this.xrLabel17.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrLabel17.SizeF = new System.Drawing.SizeF(93F, 16F);
            this.xrLabel17.StylePriority.UseBorders = false;
            this.xrLabel17.Text = "Sub Total";
            this.xrLabel17.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight;
            // 
            // QuadroDesp
            // 
            this.QuadroDesp.BackColor = System.Drawing.Color.DarkGray;
            this.QuadroDesp.Dpi = 254F;
            this.QuadroDesp.Font = new System.Drawing.Font("Verdana", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.QuadroDesp.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.QuadroDesp.LocationFloat = new DevExpress.Utils.PointFloat(0F, 0F);
            this.QuadroDesp.Name = "QuadroDesp";
            this.QuadroDesp.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.QuadroDesp.SizeF = new System.Drawing.SizeF(700F, 37F);
            this.QuadroDesp.Text = "DESPESAS";
            this.QuadroDesp.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter;
            this.QuadroDesp.Draw += new DevExpress.XtraReports.UI.DrawEventHandler(this.CantosRedondosL);
            // 
            // PDespG
            // 
            this.PDespG.BorderColor = System.Drawing.Color.Silver;
            this.PDespG.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.PDespG.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrLabel10,
            this.xrPanel6,
            this.DGpor,
            this.xrLabel13,
            this.DespesaValor,
            this.DespesaNome});
            this.PDespG.Dpi = 254F;
            this.PDespG.LocationFloat = new DevExpress.Utils.PointFloat(0F, 37F);
            this.PDespG.Name = "PDespG";
            this.PDespG.SizeF = new System.Drawing.SizeF(700F, 59F);
            // 
            // xrLabel10
            // 
            this.xrLabel10.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel10.Dpi = 254F;
            this.xrLabel10.Font = new System.Drawing.Font("Tahoma", 3.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel10.LocationFloat = new DevExpress.Utils.PointFloat(456.0001F, 42F);
            this.xrLabel10.Name = "xrLabel10";
            this.xrLabel10.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrLabel10.SizeF = new System.Drawing.SizeF(93F, 14F);
            this.xrLabel10.StylePriority.UseBorders = false;
            this.xrLabel10.Text = "Sub Total";
            this.xrLabel10.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight;
            // 
            // xrPanel6
            // 
            this.xrPanel6.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrPanel6.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.DGLine,
            this.DGLine1});
            this.xrPanel6.Dpi = 254F;
            this.xrPanel6.LocationFloat = new DevExpress.Utils.PointFloat(310F, 8F);
            this.xrPanel6.Name = "xrPanel6";
            this.xrPanel6.SizeF = new System.Drawing.SizeF(375F, 11F);
            this.xrPanel6.StylePriority.UseBorders = false;
            // 
            // DGLine
            // 
            this.DGLine.Dpi = 254F;
            this.DGLine.ForeColor = System.Drawing.Color.Silver;
            this.DGLine.LineWidth = 10;
            this.DGLine.LocationFloat = new DevExpress.Utils.PointFloat(0F, 0F);
            this.DGLine.Name = "DGLine";
            this.DGLine.SizeF = new System.Drawing.SizeF(100F, 10F);
            // 
            // DGLine1
            // 
            this.DGLine1.Dpi = 254F;
            this.DGLine1.ForeColor = System.Drawing.Color.LightGray;
            this.DGLine1.LineWidth = 3;
            this.DGLine1.LocationFloat = new DevExpress.Utils.PointFloat(185F, 3F);
            this.DGLine1.Name = "DGLine1";
            this.DGLine1.SizeF = new System.Drawing.SizeF(185F, 5F);
            // 
            // DGpor
            // 
            this.DGpor.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.DGpor.Dpi = 254F;
            this.DGpor.Font = new System.Drawing.Font("Tahoma", 5.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DGpor.LocationFloat = new DevExpress.Utils.PointFloat(225F, 5F);
            this.DGpor.Name = "DGpor";
            this.DGpor.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.DGpor.SizeF = new System.Drawing.SizeF(75F, 21F);
            this.DGpor.StylePriority.UseBorders = false;
            this.DGpor.Text = "10.0%";
            this.DGpor.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight;
            // 
            // xrLabel13
            // 
            this.xrLabel13.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel13.Dpi = 254F;
            this.xrLabel13.Font = new System.Drawing.Font("Tahoma", 6.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel13.LocationFloat = new DevExpress.Utils.PointFloat(3F, 3F);
            this.xrLabel13.Name = "xrLabel13";
            this.xrLabel13.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrLabel13.SizeF = new System.Drawing.SizeF(221.9999F, 23F);
            this.xrLabel13.StylePriority.UseBorders = false;
            this.xrLabel13.Text = "Gerais";
            // 
            // DespesaValor
            // 
            this.DespesaValor.BackColor = System.Drawing.Color.WhiteSmoke;
            this.DespesaValor.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.DespesaValor.Dpi = 254F;
            this.DespesaValor.Font = new System.Drawing.Font("Tahoma", 5.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DespesaValor.LocationFloat = new DevExpress.Utils.PointFloat(562F, 26F);
            this.DespesaValor.Multiline = true;
            this.DespesaValor.Name = "DespesaValor";
            this.DespesaValor.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.DespesaValor.SizeF = new System.Drawing.SizeF(138F, 29F);
            this.DespesaValor.StylePriority.UseBorders = false;
            this.DespesaValor.Text = "DespesaValor";
            this.DespesaValor.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight;
            this.DespesaValor.Draw += new DevExpress.XtraReports.UI.DrawEventHandler(this.DegradeL);
            // 
            // DespesaNome
            // 
            this.DespesaNome.BorderColor = System.Drawing.Color.Silver;
            this.DespesaNome.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.DespesaNome.Dpi = 254F;
            this.DespesaNome.Font = new System.Drawing.Font("Tahoma", 5.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DespesaNome.LocationFloat = new DevExpress.Utils.PointFloat(3F, 26F);
            this.DespesaNome.Multiline = true;
            this.DespesaNome.Name = "DespesaNome";
            this.DespesaNome.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.DespesaNome.SizeF = new System.Drawing.SizeF(558F, 16F);
            this.DespesaNome.StylePriority.UseBorders = false;
            this.DespesaNome.Text = "DespesaNome";
            this.DespesaNome.WordWrap = false;
            // 
            // PdespMO
            // 
            this.PdespMO.BorderColor = System.Drawing.Color.Silver;
            this.PdespMO.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.PdespMO.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrLabel3XX,
            this.xrPanel9,
            this.MOpor,
            this.DespesaValor1,
            this.xrLabel14,
            this.DespesaNome1});
            this.PdespMO.Dpi = 254F;
            this.PdespMO.LocationFloat = new DevExpress.Utils.PointFloat(0F, 101F);
            this.PdespMO.Name = "PdespMO";
            this.PdespMO.SizeF = new System.Drawing.SizeF(700F, 61F);
            this.PdespMO.Visible = false;
            // 
            // xrLabel3XX
            // 
            this.xrLabel3XX.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel3XX.Dpi = 254F;
            this.xrLabel3XX.Font = new System.Drawing.Font("Tahoma", 3.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel3XX.LocationFloat = new DevExpress.Utils.PointFloat(456F, 42F);
            this.xrLabel3XX.Name = "xrLabel3XX";
            this.xrLabel3XX.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrLabel3XX.SizeF = new System.Drawing.SizeF(93F, 16F);
            this.xrLabel3XX.StylePriority.UseBorders = false;
            this.xrLabel3XX.Text = "Sub Total";
            this.xrLabel3XX.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight;
            // 
            // xrPanel9
            // 
            this.xrPanel9.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrPanel9.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.MOLine,
            this.MOLine1});
            this.xrPanel9.Dpi = 254F;
            this.xrPanel9.LocationFloat = new DevExpress.Utils.PointFloat(310F, 8F);
            this.xrPanel9.Name = "xrPanel9";
            this.xrPanel9.SizeF = new System.Drawing.SizeF(375F, 11F);
            this.xrPanel9.StylePriority.UseBorders = false;
            // 
            // MOLine
            // 
            this.MOLine.Dpi = 254F;
            this.MOLine.ForeColor = System.Drawing.Color.Silver;
            this.MOLine.LineWidth = 10;
            this.MOLine.LocationFloat = new DevExpress.Utils.PointFloat(0F, 0F);
            this.MOLine.Name = "MOLine";
            this.MOLine.SizeF = new System.Drawing.SizeF(100F, 10F);
            // 
            // MOLine1
            // 
            this.MOLine1.Dpi = 254F;
            this.MOLine1.ForeColor = System.Drawing.Color.LightGray;
            this.MOLine1.LineWidth = 3;
            this.MOLine1.LocationFloat = new DevExpress.Utils.PointFloat(143F, 3F);
            this.MOLine1.Name = "MOLine1";
            this.MOLine1.SizeF = new System.Drawing.SizeF(217F, 6F);
            // 
            // MOpor
            // 
            this.MOpor.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.MOpor.Dpi = 254F;
            this.MOpor.Font = new System.Drawing.Font("Tahoma", 5.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MOpor.LocationFloat = new DevExpress.Utils.PointFloat(225F, 3F);
            this.MOpor.Name = "MOpor";
            this.MOpor.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.MOpor.SizeF = new System.Drawing.SizeF(75F, 21F);
            this.MOpor.StylePriority.UseBorders = false;
            this.MOpor.Text = "100%";
            this.MOpor.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight;
            // 
            // DespesaValor1
            // 
            this.DespesaValor1.BackColor = System.Drawing.Color.WhiteSmoke;
            this.DespesaValor1.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.DespesaValor1.Dpi = 254F;
            this.DespesaValor1.Font = new System.Drawing.Font("Tahoma", 5.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DespesaValor1.LocationFloat = new DevExpress.Utils.PointFloat(562F, 26F);
            this.DespesaValor1.Multiline = true;
            this.DespesaValor1.Name = "DespesaValor1";
            this.DespesaValor1.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.DespesaValor1.SizeF = new System.Drawing.SizeF(138F, 30F);
            this.DespesaValor1.StylePriority.UseBorders = false;
            this.DespesaValor1.Text = "DespesaValor1";
            this.DespesaValor1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight;
            this.DespesaValor1.Draw += new DevExpress.XtraReports.UI.DrawEventHandler(this.DegradeL);
            // 
            // xrLabel14
            // 
            this.xrLabel14.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel14.Dpi = 254F;
            this.xrLabel14.Font = new System.Drawing.Font("Tahoma", 6.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel14.LocationFloat = new DevExpress.Utils.PointFloat(3F, 3F);
            this.xrLabel14.Name = "xrLabel14";
            this.xrLabel14.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrLabel14.SizeF = new System.Drawing.SizeF(212F, 23F);
            this.xrLabel14.StylePriority.UseBorders = false;
            this.xrLabel14.Text = "M�o de Obra";
            // 
            // DespesaNome1
            // 
            this.DespesaNome1.BorderColor = System.Drawing.SystemColors.ControlDark;
            this.DespesaNome1.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.DespesaNome1.Dpi = 254F;
            this.DespesaNome1.Font = new System.Drawing.Font("Tahoma", 5.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DespesaNome1.LocationFloat = new DevExpress.Utils.PointFloat(3F, 26F);
            this.DespesaNome1.Multiline = true;
            this.DespesaNome1.Name = "DespesaNome1";
            this.DespesaNome1.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.DespesaNome1.SizeF = new System.Drawing.SizeF(558F, 16F);
            this.DespesaNome1.StylePriority.UseBorders = false;
            this.DespesaNome1.Text = "DespesaNome1";
            this.DespesaNome1.WordWrap = false;
            // 
            // PDespB
            // 
            this.PDespB.BorderColor = System.Drawing.Color.Silver;
            this.PDespB.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.PDespB.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrLabel21,
            this.xrPanel41,
            this.Bancpor,
            this.xrLabel52,
            this.DespesaNomeBANC,
            this.DespesaValorBANC});
            this.PDespB.Dpi = 254F;
            this.PDespB.LocationFloat = new DevExpress.Utils.PointFloat(0F, 312F);
            this.PDespB.Name = "PDespB";
            this.PDespB.SizeF = new System.Drawing.SizeF(700F, 58F);
            this.PDespB.Visible = false;
            // 
            // xrLabel21
            // 
            this.xrLabel21.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel21.Dpi = 254F;
            this.xrLabel21.Font = new System.Drawing.Font("Tahoma", 3.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel21.LocationFloat = new DevExpress.Utils.PointFloat(456F, 38F);
            this.xrLabel21.Name = "xrLabel21";
            this.xrLabel21.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrLabel21.SizeF = new System.Drawing.SizeF(93F, 16F);
            this.xrLabel21.StylePriority.UseBorders = false;
            this.xrLabel21.Text = "Sub Total";
            this.xrLabel21.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight;
            // 
            // xrPanel41
            // 
            this.xrPanel41.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrPanel41.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.BancLine,
            this.BancLine1});
            this.xrPanel41.Dpi = 254F;
            this.xrPanel41.LocationFloat = new DevExpress.Utils.PointFloat(310F, 8F);
            this.xrPanel41.Name = "xrPanel41";
            this.xrPanel41.SizeF = new System.Drawing.SizeF(375F, 11F);
            this.xrPanel41.StylePriority.UseBorders = false;
            // 
            // BancLine
            // 
            this.BancLine.Dpi = 254F;
            this.BancLine.ForeColor = System.Drawing.Color.Silver;
            this.BancLine.LineWidth = 10;
            this.BancLine.LocationFloat = new DevExpress.Utils.PointFloat(0F, 0F);
            this.BancLine.Name = "BancLine";
            this.BancLine.SizeF = new System.Drawing.SizeF(100F, 10F);
            // 
            // BancLine1
            // 
            this.BancLine1.Dpi = 254F;
            this.BancLine1.ForeColor = System.Drawing.Color.LightGray;
            this.BancLine1.LineWidth = 3;
            this.BancLine1.LocationFloat = new DevExpress.Utils.PointFloat(138F, 3F);
            this.BancLine1.Name = "BancLine1";
            this.BancLine1.SizeF = new System.Drawing.SizeF(185F, 6F);
            // 
            // Bancpor
            // 
            this.Bancpor.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.Bancpor.Dpi = 254F;
            this.Bancpor.Font = new System.Drawing.Font("Tahoma", 5.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Bancpor.LocationFloat = new DevExpress.Utils.PointFloat(225F, 3F);
            this.Bancpor.Name = "Bancpor";
            this.Bancpor.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.Bancpor.SizeF = new System.Drawing.SizeF(75F, 21F);
            this.Bancpor.StylePriority.UseBorders = false;
            this.Bancpor.Text = "100%";
            this.Bancpor.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight;
            // 
            // xrLabel52
            // 
            this.xrLabel52.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel52.Dpi = 254F;
            this.xrLabel52.Font = new System.Drawing.Font("Tahoma", 6.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel52.LocationFloat = new DevExpress.Utils.PointFloat(3F, 3F);
            this.xrLabel52.Name = "xrLabel52";
            this.xrLabel52.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrLabel52.SizeF = new System.Drawing.SizeF(159F, 23F);
            this.xrLabel52.StylePriority.UseBorders = false;
            this.xrLabel52.Text = "Banc�rias";
            // 
            // DespesaNomeBANC
            // 
            this.DespesaNomeBANC.BorderColor = System.Drawing.Color.Silver;
            this.DespesaNomeBANC.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.DespesaNomeBANC.Dpi = 254F;
            this.DespesaNomeBANC.Font = new System.Drawing.Font("Tahoma", 5.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DespesaNomeBANC.LocationFloat = new DevExpress.Utils.PointFloat(3F, 26F);
            this.DespesaNomeBANC.Multiline = true;
            this.DespesaNomeBANC.Name = "DespesaNomeBANC";
            this.DespesaNomeBANC.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.DespesaNomeBANC.SizeF = new System.Drawing.SizeF(558F, 11F);
            this.DespesaNomeBANC.StylePriority.UseBorders = false;
            this.DespesaNomeBANC.Text = "DespesaNomeBANC";
            this.DespesaNomeBANC.WordWrap = false;
            // 
            // DespesaValorBANC
            // 
            this.DespesaValorBANC.BackColor = System.Drawing.Color.WhiteSmoke;
            this.DespesaValorBANC.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.DespesaValorBANC.Dpi = 254F;
            this.DespesaValorBANC.Font = new System.Drawing.Font("Tahoma", 5.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DespesaValorBANC.LocationFloat = new DevExpress.Utils.PointFloat(562F, 26F);
            this.DespesaValorBANC.Multiline = true;
            this.DespesaValorBANC.Name = "DespesaValorBANC";
            this.DespesaValorBANC.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.DespesaValorBANC.SizeF = new System.Drawing.SizeF(138F, 27F);
            this.DespesaValorBANC.StylePriority.UseBorders = false;
            this.DespesaValorBANC.Text = "DespesaValorBANC";
            this.DespesaValorBANC.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight;
            this.DespesaValorBANC.Draw += new DevExpress.XtraReports.UI.DrawEventHandler(this.DegradeL);
            // 
            // PDespH
            // 
            this.PDespH.BorderColor = System.Drawing.Color.Silver;
            this.PDespH.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.PDespH.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.DespesaNomeADV,
            this.xrLabel23,
            this.xrPanel42,
            this.HORpor,
            this.xrLabel56,
            this.DespesaValorADV});
            this.PDespH.Dpi = 254F;
            this.PDespH.LocationFloat = new DevExpress.Utils.PointFloat(0F, 376F);
            this.PDespH.Name = "PDespH";
            this.PDespH.SizeF = new System.Drawing.SizeF(700F, 61F);
            this.PDespH.Visible = false;
            // 
            // DespesaNomeADV
            // 
            this.DespesaNomeADV.BorderColor = System.Drawing.Color.Silver;
            this.DespesaNomeADV.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.DespesaNomeADV.Dpi = 254F;
            this.DespesaNomeADV.Font = new System.Drawing.Font("Tahoma", 5.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DespesaNomeADV.LocationFloat = new DevExpress.Utils.PointFloat(3F, 26F);
            this.DespesaNomeADV.Multiline = true;
            this.DespesaNomeADV.Name = "DespesaNomeADV";
            this.DespesaNomeADV.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.DespesaNomeADV.SizeF = new System.Drawing.SizeF(558F, 16F);
            this.DespesaNomeADV.StylePriority.UseBorders = false;
            this.DespesaNomeADV.Text = "DespesaNomeADV";
            this.DespesaNomeADV.WordWrap = false;
            // 
            // xrLabel23
            // 
            this.xrLabel23.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel23.Dpi = 254F;
            this.xrLabel23.Font = new System.Drawing.Font("Tahoma", 3.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel23.LocationFloat = new DevExpress.Utils.PointFloat(456F, 42F);
            this.xrLabel23.Name = "xrLabel23";
            this.xrLabel23.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrLabel23.SizeF = new System.Drawing.SizeF(93F, 16F);
            this.xrLabel23.StylePriority.UseBorders = false;
            this.xrLabel23.Text = "Sub Total";
            this.xrLabel23.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight;
            // 
            // xrPanel42
            // 
            this.xrPanel42.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrPanel42.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.HORLine,
            this.HORLine1});
            this.xrPanel42.Dpi = 254F;
            this.xrPanel42.LocationFloat = new DevExpress.Utils.PointFloat(310F, 8F);
            this.xrPanel42.Name = "xrPanel42";
            this.xrPanel42.SizeF = new System.Drawing.SizeF(386F, 11F);
            this.xrPanel42.StylePriority.UseBorders = false;
            // 
            // HORLine
            // 
            this.HORLine.Dpi = 254F;
            this.HORLine.ForeColor = System.Drawing.Color.Silver;
            this.HORLine.LineWidth = 10;
            this.HORLine.LocationFloat = new DevExpress.Utils.PointFloat(0F, 0F);
            this.HORLine.Name = "HORLine";
            this.HORLine.SizeF = new System.Drawing.SizeF(100F, 10F);
            // 
            // HORLine1
            // 
            this.HORLine1.Dpi = 254F;
            this.HORLine1.ForeColor = System.Drawing.Color.LightGray;
            this.HORLine1.LineWidth = 3;
            this.HORLine1.LocationFloat = new DevExpress.Utils.PointFloat(106F, 3F);
            this.HORLine1.Name = "HORLine1";
            this.HORLine1.SizeF = new System.Drawing.SizeF(185F, 5F);
            // 
            // HORpor
            // 
            this.HORpor.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.HORpor.Dpi = 254F;
            this.HORpor.Font = new System.Drawing.Font("Tahoma", 5.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.HORpor.LocationFloat = new DevExpress.Utils.PointFloat(225F, 3F);
            this.HORpor.Name = "HORpor";
            this.HORpor.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.HORpor.SizeF = new System.Drawing.SizeF(75F, 21F);
            this.HORpor.StylePriority.UseBorders = false;
            this.HORpor.Text = "100%";
            this.HORpor.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight;
            // 
            // xrLabel56
            // 
            this.xrLabel56.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel56.Dpi = 254F;
            this.xrLabel56.Font = new System.Drawing.Font("Tahoma", 6.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel56.LocationFloat = new DevExpress.Utils.PointFloat(3F, 3F);
            this.xrLabel56.Name = "xrLabel56";
            this.xrLabel56.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrLabel56.SizeF = new System.Drawing.SizeF(169F, 23F);
            this.xrLabel56.StylePriority.UseBorders = false;
            this.xrLabel56.Text = "Honor�rios";
            // 
            // DespesaValorADV
            // 
            this.DespesaValorADV.BackColor = System.Drawing.Color.WhiteSmoke;
            this.DespesaValorADV.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.DespesaValorADV.Dpi = 254F;
            this.DespesaValorADV.Font = new System.Drawing.Font("Tahoma", 5.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DespesaValorADV.LocationFloat = new DevExpress.Utils.PointFloat(562F, 26F);
            this.DespesaValorADV.Multiline = true;
            this.DespesaValorADV.Name = "DespesaValorADV";
            this.DespesaValorADV.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.DespesaValorADV.SizeF = new System.Drawing.SizeF(138F, 32F);
            this.DespesaValorADV.StylePriority.UseBorders = false;
            this.DespesaValorADV.Text = "DespesaValorADV";
            this.DespesaValorADV.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight;
            this.DespesaValorADV.Draw += new DevExpress.XtraReports.UI.DrawEventHandler(this.DegradeL);
            // 
            // xrLabel200
            // 
            this.xrLabel200.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel200.Dpi = 254F;
            this.xrLabel200.Font = new System.Drawing.Font("Tahoma", 3.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel200.LocationFloat = new DevExpress.Utils.PointFloat(656F, 42F);
            this.xrLabel200.Name = "xrLabel200";
            this.xrLabel200.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrLabel200.SizeF = new System.Drawing.SizeF(93F, 16F);
            this.xrLabel200.StylePriority.UseBorders = false;
            this.xrLabel200.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight;
            // 
            // QuadroEsquerda
            // 
            this.QuadroEsquerda.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.QuadroConsumo,
            this.QuadroPendenciasUnidade,
            this.QuadroMensagem,
            this.QuadroSaldo,
            this.QuadroReceitas,
            this.QuadroComposicaoBoleto,
            this.xrPanel4,
            this.xrMensagem});
            this.QuadroEsquerda.Dpi = 254F;
            this.QuadroEsquerda.LocationFloat = new DevExpress.Utils.PointFloat(0F, 5F);
            this.QuadroEsquerda.Name = "QuadroEsquerda";
            this.QuadroEsquerda.SizeF = new System.Drawing.SizeF(1073F, 1704F);
            // 
            // QuadroConsumo
            // 
            this.QuadroConsumo.BorderColor = System.Drawing.Color.Silver;
            this.QuadroConsumo.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.QuadroConsumo.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.Consm3Luz,
            this.ConsPorcLuz,
            this.ConsValLuz,
            this.ConsPorcAgua,
            this.ConsValAgua,
            this.Consm3Agua,
            this.ConsLeitAgua,
            this.ConsPorcGas,
            this.ConsValGas,
            this.ConsKgGas,
            this.Consm3Gas,
            this.ConsLeitGas,
            this.ConsMes,
            this.SubQuadroGas,
            this.xrPanel45,
            this.xrLabel58});
            this.QuadroConsumo.Dpi = 254F;
            this.QuadroConsumo.LocationFloat = new DevExpress.Utils.PointFloat(0F, 779.9999F);
            this.QuadroConsumo.Name = "QuadroConsumo";
            this.QuadroConsumo.SizeF = new System.Drawing.SizeF(1063F, 113F);
            this.QuadroConsumo.Visible = false;
            // 
            // Consm3Luz
            // 
            this.Consm3Luz.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.Consm3Luz.CanShrink = true;
            this.Consm3Luz.Dpi = 254F;
            this.Consm3Luz.Font = new System.Drawing.Font("Tahoma", 5.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Consm3Luz.LocationFloat = new DevExpress.Utils.PointFloat(810.6703F, 85.00003F);
            this.Consm3Luz.Multiline = true;
            this.Consm3Luz.Name = "Consm3Luz";
            this.Consm3Luz.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.Consm3Luz.SizeF = new System.Drawing.SizeF(81F, 24.99994F);
            this.Consm3Luz.StylePriority.UseBorders = false;
            this.Consm3Luz.Text = "100.0000";
            this.Consm3Luz.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight;
            // 
            // ConsPorcLuz
            // 
            this.ConsPorcLuz.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.ConsPorcLuz.CanShrink = true;
            this.ConsPorcLuz.Dpi = 254F;
            this.ConsPorcLuz.Font = new System.Drawing.Font("Tahoma", 5.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ConsPorcLuz.LocationFloat = new DevExpress.Utils.PointFloat(988.6701F, 84.99998F);
            this.ConsPorcLuz.Multiline = true;
            this.ConsPorcLuz.Name = "ConsPorcLuz";
            this.ConsPorcLuz.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.ConsPorcLuz.SizeF = new System.Drawing.SizeF(65F, 25F);
            this.ConsPorcLuz.StylePriority.UseBorders = false;
            this.ConsPorcLuz.Text = "xxxx";
            this.ConsPorcLuz.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight;
            this.ConsPorcLuz.Draw += new DevExpress.XtraReports.UI.DrawEventHandler(this.ConsPorcLuz_Draw);
            // 
            // ConsValLuz
            // 
            this.ConsValLuz.BackColor = System.Drawing.Color.WhiteSmoke;
            this.ConsValLuz.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.ConsValLuz.CanShrink = true;
            this.ConsValLuz.Dpi = 254F;
            this.ConsValLuz.Font = new System.Drawing.Font("Tahoma", 5.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ConsValLuz.LocationFloat = new DevExpress.Utils.PointFloat(891.6703F, 84.99974F);
            this.ConsValLuz.Multiline = true;
            this.ConsValLuz.Name = "ConsValLuz";
            this.ConsValLuz.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.ConsValLuz.SizeF = new System.Drawing.SizeF(96.99982F, 24.99988F);
            this.ConsValLuz.StylePriority.UseBorders = false;
            this.ConsValLuz.Text = "100.00";
            this.ConsValLuz.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight;
            this.ConsValLuz.Draw += new DevExpress.XtraReports.UI.DrawEventHandler(this.DegradeL);
            // 
            // ConsPorcAgua
            // 
            this.ConsPorcAgua.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.ConsPorcAgua.CanShrink = true;
            this.ConsPorcAgua.Dpi = 254F;
            this.ConsPorcAgua.Font = new System.Drawing.Font("Tahoma", 5.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ConsPorcAgua.LocationFloat = new DevExpress.Utils.PointFloat(740.3301F, 84.99995F);
            this.ConsPorcAgua.Multiline = true;
            this.ConsPorcAgua.Name = "ConsPorcAgua";
            this.ConsPorcAgua.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.ConsPorcAgua.SizeF = new System.Drawing.SizeF(65F, 25F);
            this.ConsPorcAgua.StylePriority.UseBorders = false;
            this.ConsPorcAgua.Text = "xxxx";
            this.ConsPorcAgua.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight;
            this.ConsPorcAgua.Draw += new DevExpress.XtraReports.UI.DrawEventHandler(this.ConsPorcAgua_Draw);
            // 
            // ConsValAgua
            // 
            this.ConsValAgua.BackColor = System.Drawing.Color.WhiteSmoke;
            this.ConsValAgua.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.ConsValAgua.CanShrink = true;
            this.ConsValAgua.Dpi = 254F;
            this.ConsValAgua.Font = new System.Drawing.Font("Tahoma", 5.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ConsValAgua.LocationFloat = new DevExpress.Utils.PointFloat(656.9319F, 84.99998F);
            this.ConsValAgua.Multiline = true;
            this.ConsValAgua.Name = "ConsValAgua";
            this.ConsValAgua.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.ConsValAgua.SizeF = new System.Drawing.SizeF(79F, 25F);
            this.ConsValAgua.StylePriority.UseBorders = false;
            this.ConsValAgua.Text = "100.00";
            this.ConsValAgua.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight;
            this.ConsValAgua.Draw += new DevExpress.XtraReports.UI.DrawEventHandler(this.DegradeL);
            // 
            // Consm3Agua
            // 
            this.Consm3Agua.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.Consm3Agua.CanShrink = true;
            this.Consm3Agua.Dpi = 254F;
            this.Consm3Agua.Font = new System.Drawing.Font("Tahoma", 5.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Consm3Agua.LocationFloat = new DevExpress.Utils.PointFloat(587.9319F, 84.99998F);
            this.Consm3Agua.Multiline = true;
            this.Consm3Agua.Name = "Consm3Agua";
            this.Consm3Agua.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.Consm3Agua.SizeF = new System.Drawing.SizeF(69F, 25F);
            this.Consm3Agua.StylePriority.UseBorders = false;
            this.Consm3Agua.Text = "100.0000";
            this.Consm3Agua.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight;
            // 
            // ConsLeitAgua
            // 
            this.ConsLeitAgua.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.ConsLeitAgua.CanShrink = true;
            this.ConsLeitAgua.Dpi = 254F;
            this.ConsLeitAgua.Font = new System.Drawing.Font("Tahoma", 5.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ConsLeitAgua.LocationFloat = new DevExpress.Utils.PointFloat(478.3302F, 85F);
            this.ConsLeitAgua.Multiline = true;
            this.ConsLeitAgua.Name = "ConsLeitAgua";
            this.ConsLeitAgua.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.ConsLeitAgua.SizeF = new System.Drawing.SizeF(109.6017F, 25F);
            this.ConsLeitAgua.StylePriority.UseBorders = false;
            this.ConsLeitAgua.Text = "100.0000";
            this.ConsLeitAgua.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight;
            // 
            // ConsPorcGas
            // 
            this.ConsPorcGas.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.ConsPorcGas.CanShrink = true;
            this.ConsPorcGas.Dpi = 254F;
            this.ConsPorcGas.Font = new System.Drawing.Font("Tahoma", 5.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ConsPorcGas.LocationFloat = new DevExpress.Utils.PointFloat(395.9373F, 84.99999F);
            this.ConsPorcGas.Multiline = true;
            this.ConsPorcGas.Name = "ConsPorcGas";
            this.ConsPorcGas.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.ConsPorcGas.SizeF = new System.Drawing.SizeF(76.06253F, 25.00006F);
            this.ConsPorcGas.StylePriority.UseBorders = false;
            this.ConsPorcGas.Text = "xxxx";
            this.ConsPorcGas.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight;
            this.ConsPorcGas.Draw += new DevExpress.XtraReports.UI.DrawEventHandler(this.ConsPorcGas_Draw);
            // 
            // ConsValGas
            // 
            this.ConsValGas.BackColor = System.Drawing.Color.WhiteSmoke;
            this.ConsValGas.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.ConsValGas.CanShrink = true;
            this.ConsValGas.Dpi = 254F;
            this.ConsValGas.Font = new System.Drawing.Font("Tahoma", 5.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ConsValGas.LocationFloat = new DevExpress.Utils.PointFloat(312.0625F, 84.99977F);
            this.ConsValGas.Multiline = true;
            this.ConsValGas.Name = "ConsValGas";
            this.ConsValGas.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.ConsValGas.SizeF = new System.Drawing.SizeF(79F, 25F);
            this.ConsValGas.StylePriority.UseBorders = false;
            this.ConsValGas.Text = "1000,00\r\n1000,00\r\n300,00";
            this.ConsValGas.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight;
            this.ConsValGas.Draw += new DevExpress.XtraReports.UI.DrawEventHandler(this.DegradeL);
            // 
            // ConsKgGas
            // 
            this.ConsKgGas.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.ConsKgGas.CanShrink = true;
            this.ConsKgGas.Dpi = 254F;
            this.ConsKgGas.Font = new System.Drawing.Font("Tahoma", 5.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ConsKgGas.LocationFloat = new DevExpress.Utils.PointFloat(255F, 84.99999F);
            this.ConsKgGas.Multiline = true;
            this.ConsKgGas.Name = "ConsKgGas";
            this.ConsKgGas.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.ConsKgGas.SizeF = new System.Drawing.SizeF(57.0625F, 25.00006F);
            this.ConsKgGas.StylePriority.UseBorders = false;
            this.ConsKgGas.Text = "100.00";
            this.ConsKgGas.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight;
            // 
            // Consm3Gas
            // 
            this.Consm3Gas.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.Consm3Gas.CanShrink = true;
            this.Consm3Gas.Dpi = 254F;
            this.Consm3Gas.Font = new System.Drawing.Font("Tahoma", 5.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Consm3Gas.LocationFloat = new DevExpress.Utils.PointFloat(187.9375F, 84.99986F);
            this.Consm3Gas.Multiline = true;
            this.Consm3Gas.Name = "Consm3Gas";
            this.Consm3Gas.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.Consm3Gas.SizeF = new System.Drawing.SizeF(67.0625F, 25F);
            this.Consm3Gas.StylePriority.UseBorders = false;
            this.Consm3Gas.Text = "100.0000";
            this.Consm3Gas.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight;
            // 
            // ConsLeitGas
            // 
            this.ConsLeitGas.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.ConsLeitGas.CanShrink = true;
            this.ConsLeitGas.Dpi = 254F;
            this.ConsLeitGas.Font = new System.Drawing.Font("Tahoma", 5.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ConsLeitGas.LocationFloat = new DevExpress.Utils.PointFloat(93.99999F, 84.99991F);
            this.ConsLeitGas.Multiline = true;
            this.ConsLeitGas.Name = "ConsLeitGas";
            this.ConsLeitGas.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.ConsLeitGas.SizeF = new System.Drawing.SizeF(93.93751F, 25.00012F);
            this.ConsLeitGas.StylePriority.UseBorders = false;
            this.ConsLeitGas.Text = "100.000";
            this.ConsLeitGas.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight;
            // 
            // ConsMes
            // 
            this.ConsMes.BorderColor = System.Drawing.Color.Silver;
            this.ConsMes.Borders = DevExpress.XtraPrinting.BorderSide.Left;
            this.ConsMes.Dpi = 254F;
            this.ConsMes.Font = new System.Drawing.Font("Tahoma", 5.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ConsMes.LocationFloat = new DevExpress.Utils.PointFloat(0F, 85F);
            this.ConsMes.Multiline = true;
            this.ConsMes.Name = "ConsMes";
            this.ConsMes.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.ConsMes.SizeF = new System.Drawing.SizeF(94F, 25F);
            this.ConsMes.Text = "*01/2000";
            this.ConsMes.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight;
            // 
            // SubQuadroGas
            // 
            this.SubQuadroGas.BackColor = System.Drawing.Color.DarkGray;
            this.SubQuadroGas.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.SubQuadroGas.CanGrow = false;
            this.SubQuadroGas.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.ConsValLuzT,
            this.Consm3LuzT,
            this.TituloGas,
            this.ConsValGasT,
            this.ConsKgGasT,
            this.Consm3GasT,
            this.ConsLeitGasT,
            this.ConsAguaT,
            this.ConsLeitAguaT,
            this.Consm3AguaT,
            this.ConsValAguaT,
            this.ConsLuzT});
            this.SubQuadroGas.Dpi = 254F;
            this.SubQuadroGas.LocationFloat = new DevExpress.Utils.PointFloat(93.99999F, 36.99994F);
            this.SubQuadroGas.Name = "SubQuadroGas";
            this.SubQuadroGas.SizeF = new System.Drawing.SizeF(963.9994F, 46F);
            this.SubQuadroGas.StylePriority.UseBorders = false;
            // 
            // ConsValLuzT
            // 
            this.ConsValLuzT.CanGrow = false;
            this.ConsValLuzT.Dpi = 254F;
            this.ConsValLuzT.Font = new System.Drawing.Font("Times New Roman", 5.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ConsValLuzT.ForeColor = System.Drawing.Color.Black;
            this.ConsValLuzT.LocationFloat = new DevExpress.Utils.PointFloat(841F, 24F);
            this.ConsValLuzT.Name = "ConsValLuzT";
            this.ConsValLuzT.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.ConsValLuzT.SizeF = new System.Drawing.SizeF(37F, 21F);
            this.ConsValLuzT.Text = "R$";
            // 
            // Consm3LuzT
            // 
            this.Consm3LuzT.CanGrow = false;
            this.Consm3LuzT.Dpi = 254F;
            this.Consm3LuzT.Font = new System.Drawing.Font("Times New Roman", 5.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Consm3LuzT.ForeColor = System.Drawing.Color.Black;
            this.Consm3LuzT.LocationFloat = new DevExpress.Utils.PointFloat(716.6703F, 24.00001F);
            this.Consm3LuzT.Name = "Consm3LuzT";
            this.Consm3LuzT.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.Consm3LuzT.SizeF = new System.Drawing.SizeF(85F, 21F);
            this.Consm3LuzT.StylePriority.UseTextAlignment = false;
            this.Consm3LuzT.Text = "kWh";
            this.Consm3LuzT.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter;
            // 
            // TituloGas
            // 
            this.TituloGas.BorderColor = System.Drawing.Color.Black;
            this.TituloGas.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.TituloGas.CanGrow = false;
            this.TituloGas.Dpi = 254F;
            this.TituloGas.Font = new System.Drawing.Font("Times New Roman", 5.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TituloGas.ForeColor = System.Drawing.Color.Black;
            this.TituloGas.LocationFloat = new DevExpress.Utils.PointFloat(0F, 0F);
            this.TituloGas.Name = "TituloGas";
            this.TituloGas.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.TituloGas.SizeF = new System.Drawing.SizeF(377.9999F, 23F);
            this.TituloGas.Text = "G�s";
            this.TituloGas.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter;
            // 
            // ConsValGasT
            // 
            this.ConsValGasT.CanGrow = false;
            this.ConsValGasT.Dpi = 254F;
            this.ConsValGasT.Font = new System.Drawing.Font("Times New Roman", 5.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ConsValGasT.ForeColor = System.Drawing.Color.Black;
            this.ConsValGasT.LocationFloat = new DevExpress.Utils.PointFloat(238.5559F, 24.00004F);
            this.ConsValGasT.Name = "ConsValGasT";
            this.ConsValGasT.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.ConsValGasT.SizeF = new System.Drawing.SizeF(37F, 21F);
            this.ConsValGasT.Text = "R$";
            // 
            // ConsKgGasT
            // 
            this.ConsKgGasT.CanGrow = false;
            this.ConsKgGasT.Dpi = 254F;
            this.ConsKgGasT.Font = new System.Drawing.Font("Times New Roman", 5.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ConsKgGasT.ForeColor = System.Drawing.Color.Black;
            this.ConsKgGasT.LocationFloat = new DevExpress.Utils.PointFloat(170F, 24.00003F);
            this.ConsKgGasT.Name = "ConsKgGasT";
            this.ConsKgGasT.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.ConsKgGasT.SizeF = new System.Drawing.SizeF(37F, 21F);
            this.ConsKgGasT.Text = "Kg";
            // 
            // Consm3GasT
            // 
            this.Consm3GasT.CanGrow = false;
            this.Consm3GasT.Dpi = 254F;
            this.Consm3GasT.Font = new System.Drawing.Font("Times New Roman", 5.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Consm3GasT.ForeColor = System.Drawing.Color.Black;
            this.Consm3GasT.LocationFloat = new DevExpress.Utils.PointFloat(102F, 24.00004F);
            this.Consm3GasT.Name = "Consm3GasT";
            this.Consm3GasT.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.Consm3GasT.SizeF = new System.Drawing.SizeF(32F, 21F);
            this.Consm3GasT.Text = "m�";
            // 
            // ConsLeitGasT
            // 
            this.ConsLeitGasT.CanGrow = false;
            this.ConsLeitGasT.Dpi = 254F;
            this.ConsLeitGasT.Font = new System.Drawing.Font("Times New Roman", 5.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ConsLeitGasT.ForeColor = System.Drawing.Color.Black;
            this.ConsLeitGasT.LocationFloat = new DevExpress.Utils.PointFloat(8.729107F, 24.00003F);
            this.ConsLeitGasT.Name = "ConsLeitGasT";
            this.ConsLeitGasT.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.ConsLeitGasT.SizeF = new System.Drawing.SizeF(74F, 18F);
            this.ConsLeitGasT.Text = "Leitura";
            this.ConsLeitGasT.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopJustify;
            // 
            // ConsAguaT
            // 
            this.ConsAguaT.BorderColor = System.Drawing.Color.Black;
            this.ConsAguaT.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.ConsAguaT.CanGrow = false;
            this.ConsAguaT.Dpi = 254F;
            this.ConsAguaT.Font = new System.Drawing.Font("Times New Roman", 5.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ConsAguaT.ForeColor = System.Drawing.Color.Black;
            this.ConsAguaT.LocationFloat = new DevExpress.Utils.PointFloat(384.3302F, 0F);
            this.ConsAguaT.Name = "ConsAguaT";
            this.ConsAguaT.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.ConsAguaT.SizeF = new System.Drawing.SizeF(327F, 23.00006F);
            this.ConsAguaT.Text = "�gua";
            this.ConsAguaT.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter;
            // 
            // ConsLeitAguaT
            // 
            this.ConsLeitAguaT.CanGrow = false;
            this.ConsLeitAguaT.Dpi = 254F;
            this.ConsLeitAguaT.Font = new System.Drawing.Font("Times New Roman", 5.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ConsLeitAguaT.ForeColor = System.Drawing.Color.Black;
            this.ConsLeitAguaT.LocationFloat = new DevExpress.Utils.PointFloat(397F, 24.00003F);
            this.ConsLeitAguaT.Name = "ConsLeitAguaT";
            this.ConsLeitAguaT.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.ConsLeitAguaT.SizeF = new System.Drawing.SizeF(85F, 21F);
            this.ConsLeitAguaT.Text = "Leitura";
            // 
            // Consm3AguaT
            // 
            this.Consm3AguaT.CanGrow = false;
            this.Consm3AguaT.Dpi = 254F;
            this.Consm3AguaT.Font = new System.Drawing.Font("Times New Roman", 5.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Consm3AguaT.ForeColor = System.Drawing.Color.Black;
            this.Consm3AguaT.LocationFloat = new DevExpress.Utils.PointFloat(518.9999F, 23.99998F);
            this.Consm3AguaT.Name = "Consm3AguaT";
            this.Consm3AguaT.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.Consm3AguaT.SizeF = new System.Drawing.SizeF(42F, 21F);
            this.Consm3AguaT.Text = "m�";
            // 
            // ConsValAguaT
            // 
            this.ConsValAguaT.CanGrow = false;
            this.ConsValAguaT.Dpi = 254F;
            this.ConsValAguaT.Font = new System.Drawing.Font("Times New Roman", 5.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ConsValAguaT.ForeColor = System.Drawing.Color.Black;
            this.ConsValAguaT.LocationFloat = new DevExpress.Utils.PointFloat(583.0001F, 24.00001F);
            this.ConsValAguaT.Name = "ConsValAguaT";
            this.ConsValAguaT.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.ConsValAguaT.SizeF = new System.Drawing.SizeF(37F, 21F);
            this.ConsValAguaT.Text = "R$";
            // 
            // ConsLuzT
            // 
            this.ConsLuzT.BorderColor = System.Drawing.Color.Black;
            this.ConsLuzT.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.ConsLuzT.CanGrow = false;
            this.ConsLuzT.Dpi = 254F;
            this.ConsLuzT.Font = new System.Drawing.Font("Times New Roman", 5.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ConsLuzT.ForeColor = System.Drawing.Color.Black;
            this.ConsLuzT.LocationFloat = new DevExpress.Utils.PointFloat(716.6702F, 0F);
            this.ConsLuzT.Name = "ConsLuzT";
            this.ConsLuzT.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.ConsLuzT.SizeF = new System.Drawing.SizeF(245.3298F, 23.00037F);
            this.ConsLuzT.Text = "Luz";
            this.ConsLuzT.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter;
            // 
            // xrPanel45
            // 
            this.xrPanel45.BackColor = System.Drawing.Color.DarkGray;
            this.xrPanel45.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrPanel45.CanGrow = false;
            this.xrPanel45.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrLabel139});
            this.xrPanel45.Dpi = 254F;
            this.xrPanel45.LocationFloat = new DevExpress.Utils.PointFloat(1.240102E-05F, 36.99996F);
            this.xrPanel45.Name = "xrPanel45";
            this.xrPanel45.SizeF = new System.Drawing.SizeF(93.99998F, 46.00006F);
            this.xrPanel45.StylePriority.UseBorders = false;
            // 
            // xrLabel139
            // 
            this.xrLabel139.Dpi = 254F;
            this.xrLabel139.Font = new System.Drawing.Font("Times New Roman", 5.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel139.ForeColor = System.Drawing.Color.Black;
            this.xrLabel139.LocationFloat = new DevExpress.Utils.PointFloat(5F, 11F);
            this.xrLabel139.Name = "xrLabel139";
            this.xrLabel139.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrLabel139.SizeF = new System.Drawing.SizeF(81F, 21F);
            this.xrLabel139.Text = "M�s/Ano";
            // 
            // xrLabel58
            // 
            this.xrLabel58.BackColor = System.Drawing.Color.DarkGray;
            this.xrLabel58.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel58.CanGrow = false;
            this.xrLabel58.Dpi = 254F;
            this.xrLabel58.Font = new System.Drawing.Font("Verdana", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel58.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.xrLabel58.LocationFloat = new DevExpress.Utils.PointFloat(0F, 0F);
            this.xrLabel58.Name = "xrLabel58";
            this.xrLabel58.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrLabel58.SizeF = new System.Drawing.SizeF(1058F, 36.99994F);
            this.xrLabel58.StylePriority.UseBorders = false;
            this.xrLabel58.Text = "HIST�RICO DE CONSUMO";
            this.xrLabel58.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter;
            this.xrLabel58.Draw += new DevExpress.XtraReports.UI.DrawEventHandler(this.CantosRedondosL);
            // 
            // QuadroPendenciasUnidade
            // 
            this.QuadroPendenciasUnidade.BorderColor = System.Drawing.Color.Silver;
            this.QuadroPendenciasUnidade.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.QuadroPendenciasUnidade.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrLabel76,
            this.labRespBA,
            this.labValorBA,
            this.labVenctoBA,
            this.labCategoriaBA});
            this.QuadroPendenciasUnidade.Dpi = 254F;
            this.QuadroPendenciasUnidade.LocationFloat = new DevExpress.Utils.PointFloat(0F, 690F);
            this.QuadroPendenciasUnidade.Name = "QuadroPendenciasUnidade";
            this.QuadroPendenciasUnidade.SizeF = new System.Drawing.SizeF(1063F, 68F);
            this.QuadroPendenciasUnidade.Visible = false;
            // 
            // xrLabel76
            // 
            this.xrLabel76.BackColor = System.Drawing.Color.DarkGray;
            this.xrLabel76.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel76.Dpi = 254F;
            this.xrLabel76.Font = new System.Drawing.Font("Verdana", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel76.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.xrLabel76.LocationFloat = new DevExpress.Utils.PointFloat(0F, 0F);
            this.xrLabel76.Name = "xrLabel76";
            this.xrLabel76.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrLabel76.SizeF = new System.Drawing.SizeF(1057.999F, 37F);
            this.xrLabel76.StylePriority.UseBorders = false;
            this.xrLabel76.Text = "PEND�NCIAS DA UNIDADE";
            this.xrLabel76.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter;
            this.xrLabel76.Draw += new DevExpress.XtraReports.UI.DrawEventHandler(this.CantosRedondosL);
            // 
            // labRespBA
            // 
            this.labRespBA.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.labRespBA.Dpi = 254F;
            this.labRespBA.Font = new System.Drawing.Font("Tahoma", 7F);
            this.labRespBA.LocationFloat = new DevExpress.Utils.PointFloat(0F, 37F);
            this.labRespBA.Multiline = true;
            this.labRespBA.Name = "labRespBA";
            this.labRespBA.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.labRespBA.SizeF = new System.Drawing.SizeF(201F, 26F);
            this.labRespBA.StylePriority.UseBorders = false;
            this.labRespBA.Text = "PROPRIET�RIO";
            // 
            // labValorBA
            // 
            this.labValorBA.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.labValorBA.CanShrink = true;
            this.labValorBA.Dpi = 254F;
            this.labValorBA.Font = new System.Drawing.Font("Tahoma", 7F);
            this.labValorBA.LocationFloat = new DevExpress.Utils.PointFloat(661F, 37F);
            this.labValorBA.Multiline = true;
            this.labValorBA.Name = "labValorBA";
            this.labValorBA.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.labValorBA.SizeF = new System.Drawing.SizeF(185F, 26F);
            this.labValorBA.StylePriority.UseBorders = false;
            this.labValorBA.Text = "R$ 999.999.99";
            this.labValorBA.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.labValorBA.Draw += new DevExpress.XtraReports.UI.DrawEventHandler(this.DegradeL);
            // 
            // labVenctoBA
            // 
            this.labVenctoBA.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.labVenctoBA.CanShrink = true;
            this.labVenctoBA.Dpi = 254F;
            this.labVenctoBA.Font = new System.Drawing.Font("Tahoma", 7F);
            this.labVenctoBA.LocationFloat = new DevExpress.Utils.PointFloat(429F, 37F);
            this.labVenctoBA.Multiline = true;
            this.labVenctoBA.Name = "labVenctoBA";
            this.labVenctoBA.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.labVenctoBA.SizeF = new System.Drawing.SizeF(185F, 26F);
            this.labVenctoBA.StylePriority.UseBorders = false;
            this.labVenctoBA.Text = "10/10/2000";
            this.labVenctoBA.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter;
            // 
            // labCategoriaBA
            // 
            this.labCategoriaBA.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.labCategoriaBA.CanShrink = true;
            this.labCategoriaBA.Dpi = 254F;
            this.labCategoriaBA.Font = new System.Drawing.Font("Tahoma", 7F);
            this.labCategoriaBA.LocationFloat = new DevExpress.Utils.PointFloat(217F, 37F);
            this.labCategoriaBA.Multiline = true;
            this.labCategoriaBA.Name = "labCategoriaBA";
            this.labCategoriaBA.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.labCategoriaBA.SizeF = new System.Drawing.SizeF(211F, 27F);
            this.labCategoriaBA.StylePriority.UseBorders = false;
            this.labCategoriaBA.Text = "CONDOM�NIO";
            // 
            // QuadroMensagem
            // 
            this.QuadroMensagem.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrLabel118,
            this.xrRichText1});
            this.QuadroMensagem.Dpi = 254F;
            this.QuadroMensagem.LocationFloat = new DevExpress.Utils.PointFloat(0F, 573F);
            this.QuadroMensagem.Name = "QuadroMensagem";
            this.QuadroMensagem.SizeF = new System.Drawing.SizeF(1068F, 106F);
            this.QuadroMensagem.Visible = false;
            // 
            // xrLabel118
            // 
            this.xrLabel118.BackColor = System.Drawing.Color.DarkGray;
            this.xrLabel118.CanShrink = true;
            this.xrLabel118.Dpi = 254F;
            this.xrLabel118.Font = new System.Drawing.Font("Verdana", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel118.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.xrLabel118.LocationFloat = new DevExpress.Utils.PointFloat(5.000005F, 0F);
            this.xrLabel118.Name = "xrLabel118";
            this.xrLabel118.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrLabel118.SizeF = new System.Drawing.SizeF(1053F, 53F);
            this.xrLabel118.Text = "MENSAGEM IMPORTANTE";
            this.xrLabel118.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter;
            this.xrLabel118.Draw += new DevExpress.XtraReports.UI.DrawEventHandler(this.CantosRedondosL);
            // 
            // xrRichText1
            // 
            this.xrRichText1.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Rtf", null, "BOLMensagemImp")});
            this.xrRichText1.Dpi = 254F;
            this.xrRichText1.Font = new System.Drawing.Font("Tahoma", 6.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrRichText1.LocationFloat = new DevExpress.Utils.PointFloat(7.999825F, 53F);
            this.xrRichText1.Name = "xrRichText1";
            this.xrRichText1.SerializableRtfString = resources.GetString("xrRichText1.SerializableRtfString");
            this.xrRichText1.SizeF = new System.Drawing.SizeF(1050F, 53F);
            this.xrRichText1.StylePriority.UseFont = false;
            // 
            // QuadroSaldo
            // 
            this.QuadroSaldo.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrRentabilidade,
            this.xrTransferencias,
            this.xrDebitos,
            this.xrCreditos,
            this.xrSaldoInicial,
            this.xrLabel18,
            this.xrPanel38,
            this.xrSaldoFinal,
            this.xrContasLogicas});
            this.QuadroSaldo.Dpi = 254F;
            this.QuadroSaldo.LocationFloat = new DevExpress.Utils.PointFloat(0F, 450F);
            this.QuadroSaldo.Name = "QuadroSaldo";
            this.QuadroSaldo.SizeF = new System.Drawing.SizeF(1068F, 110.9227F);
            // 
            // xrRentabilidade
            // 
            this.xrRentabilidade.BackColor = System.Drawing.Color.WhiteSmoke;
            this.xrRentabilidade.Dpi = 254F;
            this.xrRentabilidade.Font = new System.Drawing.Font("Tahoma", 5.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrRentabilidade.LocationFloat = new DevExpress.Utils.PointFloat(728F, 57.99995F);
            this.xrRentabilidade.Multiline = true;
            this.xrRentabilidade.Name = "xrRentabilidade";
            this.xrRentabilidade.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrRentabilidade.SizeF = new System.Drawing.SizeF(100F, 40.61243F);
            this.xrRentabilidade.StylePriority.UseFont = false;
            this.xrRentabilidade.Text = "SaldoFinal";
            this.xrRentabilidade.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight;
            this.xrRentabilidade.Visible = false;
            this.xrRentabilidade.WordWrap = false;
            this.xrRentabilidade.Draw += new DevExpress.XtraReports.UI.DrawEventHandler(this.DegradeL);
            // 
            // xrTransferencias
            // 
            this.xrTransferencias.BackColor = System.Drawing.Color.WhiteSmoke;
            this.xrTransferencias.Dpi = 254F;
            this.xrTransferencias.Font = new System.Drawing.Font("Tahoma", 5.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrTransferencias.LocationFloat = new DevExpress.Utils.PointFloat(828F, 57.99999F);
            this.xrTransferencias.Multiline = true;
            this.xrTransferencias.Name = "xrTransferencias";
            this.xrTransferencias.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTransferencias.SizeF = new System.Drawing.SizeF(115F, 40.61243F);
            this.xrTransferencias.StylePriority.UseFont = false;
            this.xrTransferencias.Text = "SaldoFinal";
            this.xrTransferencias.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight;
            this.xrTransferencias.Visible = false;
            this.xrTransferencias.WordWrap = false;
            this.xrTransferencias.Draw += new DevExpress.XtraReports.UI.DrawEventHandler(this.DegradeL);
            // 
            // xrDebitos
            // 
            this.xrDebitos.BackColor = System.Drawing.Color.WhiteSmoke;
            this.xrDebitos.Dpi = 254F;
            this.xrDebitos.Font = new System.Drawing.Font("Tahoma", 5.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrDebitos.LocationFloat = new DevExpress.Utils.PointFloat(613F, 57.99997F);
            this.xrDebitos.Multiline = true;
            this.xrDebitos.Name = "xrDebitos";
            this.xrDebitos.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrDebitos.SizeF = new System.Drawing.SizeF(115F, 40.61243F);
            this.xrDebitos.StylePriority.UseFont = false;
            this.xrDebitos.Text = "SaldoFinal";
            this.xrDebitos.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight;
            this.xrDebitos.Visible = false;
            this.xrDebitos.WordWrap = false;
            this.xrDebitos.Draw += new DevExpress.XtraReports.UI.DrawEventHandler(this.DegradeL);
            // 
            // xrCreditos
            // 
            this.xrCreditos.BackColor = System.Drawing.Color.WhiteSmoke;
            this.xrCreditos.Dpi = 254F;
            this.xrCreditos.Font = new System.Drawing.Font("Tahoma", 5.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrCreditos.LocationFloat = new DevExpress.Utils.PointFloat(498F, 57.99999F);
            this.xrCreditos.Multiline = true;
            this.xrCreditos.Name = "xrCreditos";
            this.xrCreditos.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrCreditos.SizeF = new System.Drawing.SizeF(115F, 40.61243F);
            this.xrCreditos.StylePriority.UseFont = false;
            this.xrCreditos.Text = "SaldoFinal";
            this.xrCreditos.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight;
            this.xrCreditos.Visible = false;
            this.xrCreditos.WordWrap = false;
            this.xrCreditos.Draw += new DevExpress.XtraReports.UI.DrawEventHandler(this.DegradeL);
            // 
            // xrSaldoInicial
            // 
            this.xrSaldoInicial.BackColor = System.Drawing.Color.WhiteSmoke;
            this.xrSaldoInicial.Dpi = 254F;
            this.xrSaldoInicial.Font = new System.Drawing.Font("Tahoma", 5.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrSaldoInicial.LocationFloat = new DevExpress.Utils.PointFloat(383F, 57.99999F);
            this.xrSaldoInicial.Multiline = true;
            this.xrSaldoInicial.Name = "xrSaldoInicial";
            this.xrSaldoInicial.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrSaldoInicial.SizeF = new System.Drawing.SizeF(115F, 40.61243F);
            this.xrSaldoInicial.StylePriority.UseFont = false;
            this.xrSaldoInicial.Text = "SaldoFinal";
            this.xrSaldoInicial.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight;
            this.xrSaldoInicial.WordWrap = false;
            this.xrSaldoInicial.Draw += new DevExpress.XtraReports.UI.DrawEventHandler(this.DegradeL);
            // 
            // xrLabel18
            // 
            this.xrLabel18.BackColor = System.Drawing.Color.DarkGray;
            this.xrLabel18.Dpi = 254F;
            this.xrLabel18.Font = new System.Drawing.Font("Verdana", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel18.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.xrLabel18.LocationFloat = new DevExpress.Utils.PointFloat(4.99994F, 0F);
            this.xrLabel18.Name = "xrLabel18";
            this.xrLabel18.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrLabel18.SizeF = new System.Drawing.SizeF(1053F, 34F);
            this.xrLabel18.Text = "SALDOS";
            this.xrLabel18.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter;
            this.xrLabel18.Draw += new DevExpress.XtraReports.UI.DrawEventHandler(this.CantosRedondosL);
            // 
            // xrPanel38
            // 
            this.xrPanel38.BackColor = System.Drawing.Color.DarkGray;
            this.xrPanel38.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrRentabilidadeT,
            this.xrTransferenciaT,
            this.xrDebitosT,
            this.xrCreditosT,
            this.xrDataSaldoFinal,
            this.xrDataSaldoInicial});
            this.xrPanel38.Dpi = 254F;
            this.xrPanel38.LocationFloat = new DevExpress.Utils.PointFloat(4.99994F, 34F);
            this.xrPanel38.Name = "xrPanel38";
            this.xrPanel38.SizeF = new System.Drawing.SizeF(1053F, 24F);
            // 
            // xrRentabilidadeT
            // 
            this.xrRentabilidadeT.Dpi = 254F;
            this.xrRentabilidadeT.Font = new System.Drawing.Font("Times New Roman", 5.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrRentabilidadeT.ForeColor = System.Drawing.Color.Black;
            this.xrRentabilidadeT.LocationFloat = new DevExpress.Utils.PointFloat(723F, 3.999939F);
            this.xrRentabilidadeT.Name = "xrRentabilidadeT";
            this.xrRentabilidadeT.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrRentabilidadeT.SizeF = new System.Drawing.SizeF(100F, 20F);
            this.xrRentabilidadeT.StylePriority.UseFont = false;
            this.xrRentabilidadeT.StylePriority.UseForeColor = false;
            this.xrRentabilidadeT.StylePriority.UseTextAlignment = false;
            this.xrRentabilidadeT.Text = "Rentab.";
            this.xrRentabilidadeT.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter;
            this.xrRentabilidadeT.Visible = false;
            // 
            // xrTransferenciaT
            // 
            this.xrTransferenciaT.Dpi = 254F;
            this.xrTransferenciaT.Font = new System.Drawing.Font("Times New Roman", 5.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrTransferenciaT.ForeColor = System.Drawing.Color.Black;
            this.xrTransferenciaT.LocationFloat = new DevExpress.Utils.PointFloat(842.3076F, 4.000305F);
            this.xrTransferenciaT.Name = "xrTransferenciaT";
            this.xrTransferenciaT.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTransferenciaT.SizeF = new System.Drawing.SizeF(95.6925F, 20F);
            this.xrTransferenciaT.StylePriority.UseFont = false;
            this.xrTransferenciaT.StylePriority.UseForeColor = false;
            this.xrTransferenciaT.StylePriority.UseTextAlignment = false;
            this.xrTransferenciaT.Text = "Transf.";
            this.xrTransferenciaT.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter;
            this.xrTransferenciaT.Visible = false;
            // 
            // xrDebitosT
            // 
            this.xrDebitosT.Dpi = 254F;
            this.xrDebitosT.Font = new System.Drawing.Font("Times New Roman", 5.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrDebitosT.ForeColor = System.Drawing.Color.Black;
            this.xrDebitosT.LocationFloat = new DevExpress.Utils.PointFloat(608.0001F, 4.000244F);
            this.xrDebitosT.Name = "xrDebitosT";
            this.xrDebitosT.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrDebitosT.SizeF = new System.Drawing.SizeF(115F, 20F);
            this.xrDebitosT.StylePriority.UseFont = false;
            this.xrDebitosT.StylePriority.UseForeColor = false;
            this.xrDebitosT.StylePriority.UseTextAlignment = false;
            this.xrDebitosT.Text = "D�bitos";
            this.xrDebitosT.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter;
            this.xrDebitosT.Visible = false;
            // 
            // xrCreditosT
            // 
            this.xrCreditosT.Dpi = 254F;
            this.xrCreditosT.Font = new System.Drawing.Font("Times New Roman", 5.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrCreditosT.ForeColor = System.Drawing.Color.Black;
            this.xrCreditosT.LocationFloat = new DevExpress.Utils.PointFloat(493F, 4.000244F);
            this.xrCreditosT.Name = "xrCreditosT";
            this.xrCreditosT.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrCreditosT.SizeF = new System.Drawing.SizeF(115F, 20F);
            this.xrCreditosT.StylePriority.UseFont = false;
            this.xrCreditosT.StylePriority.UseForeColor = false;
            this.xrCreditosT.StylePriority.UseTextAlignment = false;
            this.xrCreditosT.Text = "Cr�ditos";
            this.xrCreditosT.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter;
            this.xrCreditosT.Visible = false;
            // 
            // xrDataSaldoFinal
            // 
            this.xrDataSaldoFinal.Dpi = 254F;
            this.xrDataSaldoFinal.Font = new System.Drawing.Font("Times New Roman", 5.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrDataSaldoFinal.ForeColor = System.Drawing.Color.Black;
            this.xrDataSaldoFinal.LocationFloat = new DevExpress.Utils.PointFloat(948F, 4.000122F);
            this.xrDataSaldoFinal.Name = "xrDataSaldoFinal";
            this.xrDataSaldoFinal.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrDataSaldoFinal.SizeF = new System.Drawing.SizeF(104.9995F, 20.00006F);
            this.xrDataSaldoFinal.StylePriority.UseFont = false;
            this.xrDataSaldoFinal.StylePriority.UseForeColor = false;
            this.xrDataSaldoFinal.StylePriority.UseTextAlignment = false;
            this.xrDataSaldoFinal.Text = "Final";
            this.xrDataSaldoFinal.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter;
            // 
            // xrDataSaldoInicial
            // 
            this.xrDataSaldoInicial.Dpi = 254F;
            this.xrDataSaldoInicial.Font = new System.Drawing.Font("Times New Roman", 5.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrDataSaldoInicial.ForeColor = System.Drawing.Color.Black;
            this.xrDataSaldoInicial.LocationFloat = new DevExpress.Utils.PointFloat(378.0001F, 4.000061F);
            this.xrDataSaldoInicial.Name = "xrDataSaldoInicial";
            this.xrDataSaldoInicial.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrDataSaldoInicial.SizeF = new System.Drawing.SizeF(115F, 20F);
            this.xrDataSaldoInicial.StylePriority.UseFont = false;
            this.xrDataSaldoInicial.StylePriority.UseForeColor = false;
            this.xrDataSaldoInicial.StylePriority.UseTextAlignment = false;
            this.xrDataSaldoInicial.Text = "Inicial";
            this.xrDataSaldoInicial.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter;
            this.xrDataSaldoInicial.Visible = false;
            // 
            // xrSaldoFinal
            // 
            this.xrSaldoFinal.BackColor = System.Drawing.Color.WhiteSmoke;
            this.xrSaldoFinal.Dpi = 254F;
            this.xrSaldoFinal.Font = new System.Drawing.Font("Tahoma", 5.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrSaldoFinal.LocationFloat = new DevExpress.Utils.PointFloat(943F, 57.99996F);
            this.xrSaldoFinal.Multiline = true;
            this.xrSaldoFinal.Name = "xrSaldoFinal";
            this.xrSaldoFinal.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrSaldoFinal.SizeF = new System.Drawing.SizeF(115F, 40.61243F);
            this.xrSaldoFinal.StylePriority.UseFont = false;
            this.xrSaldoFinal.Text = "SaldoFinal";
            this.xrSaldoFinal.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight;
            this.xrSaldoFinal.WordWrap = false;
            this.xrSaldoFinal.Draw += new DevExpress.XtraReports.UI.DrawEventHandler(this.DegradeL);
            // 
            // xrContasLogicas
            // 
            this.xrContasLogicas.BorderColor = System.Drawing.Color.Silver;
            this.xrContasLogicas.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrContasLogicas.Dpi = 254F;
            this.xrContasLogicas.Font = new System.Drawing.Font("Tahoma", 5.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrContasLogicas.LocationFloat = new DevExpress.Utils.PointFloat(5.000049F, 58F);
            this.xrContasLogicas.Multiline = true;
            this.xrContasLogicas.Name = "xrContasLogicas";
            this.xrContasLogicas.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrContasLogicas.SizeF = new System.Drawing.SizeF(374.9999F, 40.61243F);
            this.xrContasLogicas.StylePriority.UseFont = false;
            this.xrContasLogicas.Text = "x";
            // 
            // QuadroReceitas
            // 
            this.QuadroReceitas.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrPrevXEf2,
            this.xrPrevXEf1,
            this.LabelHono,
            this.SubTotHono,
            this.xrLabel54,
            this.TotR,
            this.xrPanel2,
            this.xrLabelPxR});
            this.QuadroReceitas.Dpi = 254F;
            this.QuadroReceitas.LocationFloat = new DevExpress.Utils.PointFloat(0F, 143F);
            this.QuadroReceitas.Name = "QuadroReceitas";
            this.QuadroReceitas.SizeF = new System.Drawing.SizeF(1068F, 294F);
            // 
            // xrPrevXEf2
            // 
            this.xrPrevXEf2.BackColor = System.Drawing.Color.Gainsboro;
            this.xrPrevXEf2.Dpi = 254F;
            this.xrPrevXEf2.Font = new System.Drawing.Font("Tahoma", 5.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrPrevXEf2.FormattingRules.Add(this.OcultaPre);
            this.xrPrevXEf2.LocationFloat = new DevExpress.Utils.PointFloat(450.0001F, 191F);
            this.xrPrevXEf2.Multiline = true;
            this.xrPrevXEf2.Name = "xrPrevXEf2";
            this.xrPrevXEf2.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrPrevXEf2.SizeF = new System.Drawing.SizeF(164F, 90F);
            this.xrPrevXEf2.StylePriority.UseBackColor = false;
            this.xrPrevXEf2.StylePriority.UseFont = false;
            this.xrPrevXEf2.Text = "      Previsto para o per�do:";
            this.xrPrevXEf2.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight;
            this.xrPrevXEf2.WordWrap = false;
            // 
            // OcultaPre
            // 
            this.OcultaPre.Condition = "[Parameters.OcultaPrevRea] == True";
            // 
            // 
            // 
            this.OcultaPre.Formatting.Visible = DevExpress.Utils.DefaultBoolean.False;
            this.OcultaPre.Name = "OcultaPre";
            // 
            // xrPrevXEf1
            // 
            this.xrPrevXEf1.BackColor = System.Drawing.Color.Gainsboro;
            this.xrPrevXEf1.Dpi = 254F;
            this.xrPrevXEf1.Font = new System.Drawing.Font("Tahoma", 5.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrPrevXEf1.FormattingRules.Add(this.OcultaPre);
            this.xrPrevXEf1.LocationFloat = new DevExpress.Utils.PointFloat(5.000024F, 191F);
            this.xrPrevXEf1.Multiline = true;
            this.xrPrevXEf1.Name = "xrPrevXEf1";
            this.xrPrevXEf1.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrPrevXEf1.SizeF = new System.Drawing.SizeF(445F, 89.99997F);
            this.xrPrevXEf1.StylePriority.UseBackColor = false;
            this.xrPrevXEf1.StylePriority.UseFont = false;
            this.xrPrevXEf1.Text = "      Previsto para o per�do:";
            this.xrPrevXEf1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight;
            this.xrPrevXEf1.WordWrap = false;
            // 
            // LabelHono
            // 
            this.LabelHono.Dpi = 254F;
            this.LabelHono.Font = new System.Drawing.Font("Tahoma", 6.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LabelHono.LocationFloat = new DevExpress.Utils.PointFloat(642F, 197F);
            this.LabelHono.Name = "LabelHono";
            this.LabelHono.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.LabelHono.SizeF = new System.Drawing.SizeF(234F, 34F);
            this.LabelHono.Text = "Honor�rios";
            this.LabelHono.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight;
            this.LabelHono.Visible = false;
            // 
            // SubTotHono
            // 
            this.SubTotHono.BackColor = System.Drawing.Color.WhiteSmoke;
            this.SubTotHono.Dpi = 254F;
            this.SubTotHono.Font = new System.Drawing.Font("Tahoma", 5.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.SubTotHono.LocationFloat = new DevExpress.Utils.PointFloat(936F, 197F);
            this.SubTotHono.Multiline = true;
            this.SubTotHono.Name = "SubTotHono";
            this.SubTotHono.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.SubTotHono.SizeF = new System.Drawing.SizeF(120F, 34F);
            this.SubTotHono.StylePriority.UseFont = false;
            this.SubTotHono.Text = "1";
            this.SubTotHono.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight;
            this.SubTotHono.Visible = false;
            this.SubTotHono.Draw += new DevExpress.XtraReports.UI.DrawEventHandler(this.DegradeL);
            // 
            // xrLabel54
            // 
            this.xrLabel54.Dpi = 254F;
            this.xrLabel54.Font = new System.Drawing.Font("Tahoma", 6.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel54.LocationFloat = new DevExpress.Utils.PointFloat(687F, 237F);
            this.xrLabel54.Name = "xrLabel54";
            this.xrLabel54.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrLabel54.SizeF = new System.Drawing.SizeF(190F, 42F);
            this.xrLabel54.Text = "TOTAL GERAL";
            this.xrLabel54.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight;
            // 
            // TotR
            // 
            this.TotR.Dpi = 254F;
            this.TotR.Font = new System.Drawing.Font("Tahoma", 6.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TotR.LocationFloat = new DevExpress.Utils.PointFloat(877F, 237F);
            this.TotR.Name = "TotR";
            this.TotR.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.TotR.SizeF = new System.Drawing.SizeF(179F, 42.00006F);
            this.TotR.Text = "TOTAL";
            this.TotR.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight;
            // 
            // xrPanel2
            // 
            this.xrPanel2.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.ReceitaTT,
            this.RecetaPT,
            this.ReceitaMesT,
            this.ReceitaAT,
            this.ReceitaPrevT,
            this.ReceitaPrev,
            this.xrPanel37,
            this.RecetaP,
            this.ReceitaMes,
            this.ReceitaA,
            this.ReceitaT,
            this.xrLabel12,
            this.xrLabel28,
            this.ReceitaNome});
            this.xrPanel2.Dpi = 254F;
            this.xrPanel2.LocationFloat = new DevExpress.Utils.PointFloat(0F, 0F);
            this.xrPanel2.Name = "xrPanel2";
            this.xrPanel2.SizeF = new System.Drawing.SizeF(1063F, 164F);
            // 
            // ReceitaTT
            // 
            this.ReceitaTT.BackColor = System.Drawing.Color.WhiteSmoke;
            this.ReceitaTT.BorderColor = System.Drawing.Color.Silver;
            this.ReceitaTT.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.ReceitaTT.Dpi = 254F;
            this.ReceitaTT.Font = new System.Drawing.Font("Tahoma", 5.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ReceitaTT.LocationFloat = new DevExpress.Utils.PointFloat(935F, 134.9999F);
            this.ReceitaTT.Multiline = true;
            this.ReceitaTT.Name = "ReceitaTT";
            this.ReceitaTT.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.ReceitaTT.SizeF = new System.Drawing.SizeF(119.9999F, 26.00003F);
            this.ReceitaTT.StylePriority.UseBorderColor = false;
            this.ReceitaTT.StylePriority.UseBorders = false;
            this.ReceitaTT.StylePriority.UseFont = false;
            this.ReceitaTT.Text = "ReceitaA";
            this.ReceitaTT.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight;
            this.ReceitaTT.WordWrap = false;
            this.ReceitaTT.Draw += new DevExpress.XtraReports.UI.DrawEventHandler(this.DegradeL);
            // 
            // RecetaPT
            // 
            this.RecetaPT.BackColor = System.Drawing.Color.WhiteSmoke;
            this.RecetaPT.BorderColor = System.Drawing.Color.Silver;
            this.RecetaPT.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.RecetaPT.Dpi = 254F;
            this.RecetaPT.Font = new System.Drawing.Font("Tahoma", 5.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.RecetaPT.LocationFloat = new DevExpress.Utils.PointFloat(810.6701F, 135F);
            this.RecetaPT.Multiline = true;
            this.RecetaPT.Name = "RecetaPT";
            this.RecetaPT.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.RecetaPT.SizeF = new System.Drawing.SizeF(119.9999F, 26.00003F);
            this.RecetaPT.StylePriority.UseBorderColor = false;
            this.RecetaPT.StylePriority.UseBorders = false;
            this.RecetaPT.StylePriority.UseFont = false;
            this.RecetaPT.Text = "ReceitaA";
            this.RecetaPT.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight;
            this.RecetaPT.WordWrap = false;
            this.RecetaPT.Draw += new DevExpress.XtraReports.UI.DrawEventHandler(this.DegradeL);
            // 
            // ReceitaMesT
            // 
            this.ReceitaMesT.BackColor = System.Drawing.Color.WhiteSmoke;
            this.ReceitaMesT.BorderColor = System.Drawing.Color.Silver;
            this.ReceitaMesT.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.ReceitaMesT.Dpi = 254F;
            this.ReceitaMesT.Font = new System.Drawing.Font("Tahoma", 5.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ReceitaMesT.LocationFloat = new DevExpress.Utils.PointFloat(685.3303F, 135F);
            this.ReceitaMesT.Multiline = true;
            this.ReceitaMesT.Name = "ReceitaMesT";
            this.ReceitaMesT.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.ReceitaMesT.SizeF = new System.Drawing.SizeF(119.9999F, 26.00003F);
            this.ReceitaMesT.StylePriority.UseBorderColor = false;
            this.ReceitaMesT.StylePriority.UseBorders = false;
            this.ReceitaMesT.StylePriority.UseFont = false;
            this.ReceitaMesT.Text = "ReceitaA";
            this.ReceitaMesT.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight;
            this.ReceitaMesT.WordWrap = false;
            this.ReceitaMesT.Draw += new DevExpress.XtraReports.UI.DrawEventHandler(this.DegradeL);
            // 
            // ReceitaAT
            // 
            this.ReceitaAT.BackColor = System.Drawing.Color.WhiteSmoke;
            this.ReceitaAT.BorderColor = System.Drawing.Color.Silver;
            this.ReceitaAT.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.ReceitaAT.Dpi = 254F;
            this.ReceitaAT.Font = new System.Drawing.Font("Tahoma", 5.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ReceitaAT.LocationFloat = new DevExpress.Utils.PointFloat(560F, 135F);
            this.ReceitaAT.Multiline = true;
            this.ReceitaAT.Name = "ReceitaAT";
            this.ReceitaAT.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.ReceitaAT.SizeF = new System.Drawing.SizeF(119.9999F, 26.00003F);
            this.ReceitaAT.StylePriority.UseBorderColor = false;
            this.ReceitaAT.StylePriority.UseBorders = false;
            this.ReceitaAT.StylePriority.UseFont = false;
            this.ReceitaAT.Text = "ReceitaA";
            this.ReceitaAT.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight;
            this.ReceitaAT.WordWrap = false;
            this.ReceitaAT.Draw += new DevExpress.XtraReports.UI.DrawEventHandler(this.DegradeL);
            // 
            // ReceitaPrevT
            // 
            this.ReceitaPrevT.BackColor = System.Drawing.Color.WhiteSmoke;
            this.ReceitaPrevT.BorderColor = System.Drawing.Color.Silver;
            this.ReceitaPrevT.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.ReceitaPrevT.Dpi = 254F;
            this.ReceitaPrevT.Font = new System.Drawing.Font("Tahoma", 5.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ReceitaPrevT.FormattingRules.Add(this.OcultaPre);
            this.ReceitaPrevT.LocationFloat = new DevExpress.Utils.PointFloat(365.0001F, 135F);
            this.ReceitaPrevT.Multiline = true;
            this.ReceitaPrevT.Name = "ReceitaPrevT";
            this.ReceitaPrevT.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.ReceitaPrevT.SizeF = new System.Drawing.SizeF(119.9999F, 26.00003F);
            this.ReceitaPrevT.StylePriority.UseBorderColor = false;
            this.ReceitaPrevT.StylePriority.UseBorders = false;
            this.ReceitaPrevT.StylePriority.UseFont = false;
            this.ReceitaPrevT.Text = "ReceitaA";
            this.ReceitaPrevT.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight;
            this.ReceitaPrevT.WordWrap = false;
            this.ReceitaPrevT.Draw += new DevExpress.XtraReports.UI.DrawEventHandler(this.DegradeL);
            // 
            // ReceitaPrev
            // 
            this.ReceitaPrev.BackColor = System.Drawing.Color.WhiteSmoke;
            this.ReceitaPrev.BorderColor = System.Drawing.Color.Silver;
            this.ReceitaPrev.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.ReceitaPrev.Dpi = 254F;
            this.ReceitaPrev.Font = new System.Drawing.Font("Tahoma", 5.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ReceitaPrev.FormattingRules.Add(this.OcultaPre);
            this.ReceitaPrev.LocationFloat = new DevExpress.Utils.PointFloat(365F, 89F);
            this.ReceitaPrev.Multiline = true;
            this.ReceitaPrev.Name = "ReceitaPrev";
            this.ReceitaPrev.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.ReceitaPrev.SizeF = new System.Drawing.SizeF(120F, 43.99997F);
            this.ReceitaPrev.StylePriority.UseBorderColor = false;
            this.ReceitaPrev.StylePriority.UseBorders = false;
            this.ReceitaPrev.StylePriority.UseFont = false;
            this.ReceitaPrev.Text = "ReceitaA";
            this.ReceitaPrev.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight;
            this.ReceitaPrev.WordWrap = false;
            this.ReceitaPrev.Draw += new DevExpress.XtraReports.UI.DrawEventHandler(this.DegradeL);
            // 
            // xrPanel37
            // 
            this.xrPanel37.BackColor = System.Drawing.Color.DarkGray;
            this.xrPanel37.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrLine11,
            this.xrLabel29,
            this.xrLabel22,
            this.xrLabel40,
            this.xrLabel39,
            this.xrLabel38,
            this.xrLabel37});
            this.xrPanel37.Dpi = 254F;
            this.xrPanel37.LocationFloat = new DevExpress.Utils.PointFloat(0F, 37.00003F);
            this.xrPanel37.Name = "xrPanel37";
            this.xrPanel37.SizeF = new System.Drawing.SizeF(1058F, 52F);
            // 
            // xrLine11
            // 
            this.xrLine11.Dpi = 254F;
            this.xrLine11.ForeColor = System.Drawing.Color.Black;
            this.xrLine11.LineWidth = 3;
            this.xrLine11.LocationFloat = new DevExpress.Utils.PointFloat(560F, 25F);
            this.xrLine11.Name = "xrLine11";
            this.xrLine11.SizeF = new System.Drawing.SizeF(487.9999F, 5F);
            this.xrLine11.StylePriority.UseForeColor = false;
            // 
            // xrLabel29
            // 
            this.xrLabel29.Dpi = 254F;
            this.xrLabel29.Font = new System.Drawing.Font("Times New Roman", 6F, System.Drawing.FontStyle.Bold);
            this.xrLabel29.ForeColor = System.Drawing.Color.Black;
            this.xrLabel29.LocationFloat = new DevExpress.Utils.PointFloat(755.0001F, 0F);
            this.xrLabel29.Name = "xrLabel29";
            this.xrLabel29.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrLabel29.SizeF = new System.Drawing.SizeF(112F, 24.99997F);
            this.xrLabel29.StylePriority.UseTextAlignment = false;
            this.xrLabel29.Text = "Realizado";
            this.xrLabel29.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter;
            // 
            // xrLabel22
            // 
            this.xrLabel22.Dpi = 254F;
            this.xrLabel22.Font = new System.Drawing.Font("Times New Roman", 6F, System.Drawing.FontStyle.Bold);
            this.xrLabel22.ForeColor = System.Drawing.Color.Black;
            this.xrLabel22.FormattingRules.Add(this.OcultaPre);
            this.xrLabel22.LocationFloat = new DevExpress.Utils.PointFloat(380F, 0F);
            this.xrLabel22.Name = "xrLabel22";
            this.xrLabel22.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrLabel22.SizeF = new System.Drawing.SizeF(112F, 24.99997F);
            this.xrLabel22.StylePriority.UseTextAlignment = false;
            this.xrLabel22.Text = "Previsto";
            this.xrLabel22.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter;
            // 
            // xrLabel40
            // 
            this.xrLabel40.Dpi = 254F;
            this.xrLabel40.Font = new System.Drawing.Font("Times New Roman", 5.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel40.ForeColor = System.Drawing.Color.Black;
            this.xrLabel40.LocationFloat = new DevExpress.Utils.PointFloat(952.9999F, 30.00002F);
            this.xrLabel40.Name = "xrLabel40";
            this.xrLabel40.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrLabel40.SizeF = new System.Drawing.SizeF(95F, 20F);
            this.xrLabel40.StylePriority.UseFont = false;
            this.xrLabel40.Text = "TOTAL";
            // 
            // xrLabel39
            // 
            this.xrLabel39.Dpi = 254F;
            this.xrLabel39.Font = new System.Drawing.Font("Times New Roman", 5.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel39.ForeColor = System.Drawing.Color.Black;
            this.xrLabel39.LocationFloat = new DevExpress.Utils.PointFloat(819F, 30F);
            this.xrLabel39.Name = "xrLabel39";
            this.xrLabel39.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrLabel39.SizeF = new System.Drawing.SizeF(120.0001F, 20F);
            this.xrLabel39.StylePriority.UseFont = false;
            this.xrLabel39.StylePriority.UseForeColor = false;
            this.xrLabel39.StylePriority.UseTextAlignment = false;
            this.xrLabel39.Text = "Antecipado";
            this.xrLabel39.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter;
            // 
            // xrLabel38
            // 
            this.xrLabel38.Dpi = 254F;
            this.xrLabel38.Font = new System.Drawing.Font("Times New Roman", 5.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel38.ForeColor = System.Drawing.Color.Black;
            this.xrLabel38.LocationFloat = new DevExpress.Utils.PointFloat(672F, 30F);
            this.xrLabel38.Name = "xrLabel38";
            this.xrLabel38.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrLabel38.SizeF = new System.Drawing.SizeF(147F, 20F);
            this.xrLabel38.StylePriority.UseFont = false;
            this.xrLabel38.StylePriority.UseForeColor = false;
            this.xrLabel38.StylePriority.UseTextAlignment = false;
            this.xrLabel38.Text = "No vencimento";
            this.xrLabel38.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter;
            // 
            // xrLabel37
            // 
            this.xrLabel37.Dpi = 254F;
            this.xrLabel37.Font = new System.Drawing.Font("Times New Roman", 5.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel37.ForeColor = System.Drawing.Color.Black;
            this.xrLabel37.LocationFloat = new DevExpress.Utils.PointFloat(560F, 30F);
            this.xrLabel37.Name = "xrLabel37";
            this.xrLabel37.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrLabel37.SizeF = new System.Drawing.SizeF(112F, 20F);
            this.xrLabel37.StylePriority.UseFont = false;
            this.xrLabel37.StylePriority.UseForeColor = false;
            this.xrLabel37.StylePriority.UseTextAlignment = false;
            this.xrLabel37.Text = "Atrasado";
            this.xrLabel37.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter;
            // 
            // RecetaP
            // 
            this.RecetaP.BackColor = System.Drawing.Color.WhiteSmoke;
            this.RecetaP.BorderColor = System.Drawing.Color.Silver;
            this.RecetaP.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.RecetaP.Dpi = 254F;
            this.RecetaP.Font = new System.Drawing.Font("Tahoma", 5.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.RecetaP.LocationFloat = new DevExpress.Utils.PointFloat(810.6701F, 89F);
            this.RecetaP.Multiline = true;
            this.RecetaP.Name = "RecetaP";
            this.RecetaP.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.RecetaP.SizeF = new System.Drawing.SizeF(120F, 43.99994F);
            this.RecetaP.StylePriority.UseBorderColor = false;
            this.RecetaP.StylePriority.UseBorders = false;
            this.RecetaP.StylePriority.UseFont = false;
            this.RecetaP.Text = "RecetaP";
            this.RecetaP.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight;
            this.RecetaP.WordWrap = false;
            this.RecetaP.Draw += new DevExpress.XtraReports.UI.DrawEventHandler(this.DegradeL);
            // 
            // ReceitaMes
            // 
            this.ReceitaMes.BackColor = System.Drawing.Color.WhiteSmoke;
            this.ReceitaMes.BorderColor = System.Drawing.Color.Silver;
            this.ReceitaMes.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.ReceitaMes.Dpi = 254F;
            this.ReceitaMes.Font = new System.Drawing.Font("Tahoma", 5.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ReceitaMes.LocationFloat = new DevExpress.Utils.PointFloat(685.33F, 89F);
            this.ReceitaMes.Multiline = true;
            this.ReceitaMes.Name = "ReceitaMes";
            this.ReceitaMes.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.ReceitaMes.SizeF = new System.Drawing.SizeF(120.0001F, 43.99994F);
            this.ReceitaMes.StylePriority.UseBorderColor = false;
            this.ReceitaMes.StylePriority.UseBorders = false;
            this.ReceitaMes.StylePriority.UseFont = false;
            this.ReceitaMes.Text = "ReceitaMes";
            this.ReceitaMes.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight;
            this.ReceitaMes.WordWrap = false;
            this.ReceitaMes.Draw += new DevExpress.XtraReports.UI.DrawEventHandler(this.DegradeL);
            // 
            // ReceitaA
            // 
            this.ReceitaA.BackColor = System.Drawing.Color.WhiteSmoke;
            this.ReceitaA.BorderColor = System.Drawing.Color.Silver;
            this.ReceitaA.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.ReceitaA.Dpi = 254F;
            this.ReceitaA.Font = new System.Drawing.Font("Tahoma", 5.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ReceitaA.LocationFloat = new DevExpress.Utils.PointFloat(560F, 89F);
            this.ReceitaA.Multiline = true;
            this.ReceitaA.Name = "ReceitaA";
            this.ReceitaA.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.ReceitaA.SizeF = new System.Drawing.SizeF(120F, 43.99997F);
            this.ReceitaA.StylePriority.UseBorderColor = false;
            this.ReceitaA.StylePriority.UseBorders = false;
            this.ReceitaA.StylePriority.UseFont = false;
            this.ReceitaA.Text = "ReceitaA";
            this.ReceitaA.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight;
            this.ReceitaA.WordWrap = false;
            this.ReceitaA.Draw += new DevExpress.XtraReports.UI.DrawEventHandler(this.DegradeL);
            // 
            // ReceitaT
            // 
            this.ReceitaT.BackColor = System.Drawing.Color.WhiteSmoke;
            this.ReceitaT.BorderColor = System.Drawing.Color.Silver;
            this.ReceitaT.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.ReceitaT.Dpi = 254F;
            this.ReceitaT.Font = new System.Drawing.Font("Tahoma", 5.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ReceitaT.LocationFloat = new DevExpress.Utils.PointFloat(936.0001F, 89F);
            this.ReceitaT.Multiline = true;
            this.ReceitaT.Name = "ReceitaT";
            this.ReceitaT.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.ReceitaT.SizeF = new System.Drawing.SizeF(120.0001F, 43.99994F);
            this.ReceitaT.StylePriority.UseBorderColor = false;
            this.ReceitaT.StylePriority.UseBorders = false;
            this.ReceitaT.StylePriority.UseFont = false;
            this.ReceitaT.Text = "1";
            this.ReceitaT.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight;
            this.ReceitaT.WordWrap = false;
            this.ReceitaT.Draw += new DevExpress.XtraReports.UI.DrawEventHandler(this.DegradeL);
            // 
            // xrLabel12
            // 
            this.xrLabel12.BackColor = System.Drawing.Color.DarkGray;
            this.xrLabel12.Dpi = 254F;
            this.xrLabel12.Font = new System.Drawing.Font("Verdana", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel12.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.xrLabel12.LocationFloat = new DevExpress.Utils.PointFloat(0F, 0F);
            this.xrLabel12.Name = "xrLabel12";
            this.xrLabel12.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrLabel12.SizeF = new System.Drawing.SizeF(1058F, 37F);
            this.xrLabel12.Text = "RECEITAS";
            this.xrLabel12.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter;
            this.xrLabel12.Draw += new DevExpress.XtraReports.UI.DrawEventHandler(this.CantosRedondosL);
            // 
            // xrLabel28
            // 
            this.xrLabel28.Dpi = 254F;
            this.xrLabel28.Font = new System.Drawing.Font("Tahoma", 3.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel28.LocationFloat = new DevExpress.Utils.PointFloat(246F, 140F);
            this.xrLabel28.Name = "xrLabel28";
            this.xrLabel28.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrLabel28.SizeF = new System.Drawing.SizeF(108F, 15F);
            this.xrLabel28.StylePriority.UseFont = false;
            this.xrLabel28.Text = "Sub-Total";
            this.xrLabel28.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight;
            // 
            // ReceitaNome
            // 
            this.ReceitaNome.BorderColor = System.Drawing.Color.Silver;
            this.ReceitaNome.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.ReceitaNome.Dpi = 254F;
            this.ReceitaNome.Font = new System.Drawing.Font("Tahoma", 5.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ReceitaNome.LocationFloat = new DevExpress.Utils.PointFloat(0F, 89F);
            this.ReceitaNome.Multiline = true;
            this.ReceitaNome.Name = "ReceitaNome";
            this.ReceitaNome.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.ReceitaNome.SizeF = new System.Drawing.SizeF(359F, 44F);
            this.ReceitaNome.StylePriority.UseFont = false;
            this.ReceitaNome.Text = "ReceitaNome";
            this.ReceitaNome.WordWrap = false;
            // 
            // xrLabelPxR
            // 
            this.xrLabelPxR.Dpi = 254F;
            this.xrLabelPxR.Font = new System.Drawing.Font("Tahoma", 6F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabelPxR.FormattingRules.Add(this.OcultaPre);
            this.xrLabelPxR.LocationFloat = new DevExpress.Utils.PointFloat(4.999914F, 164F);
            this.xrLabelPxR.Name = "xrLabelPxR";
            this.xrLabelPxR.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrLabelPxR.SizeF = new System.Drawing.SizeF(250.0001F, 27F);
            this.xrLabelPxR.StylePriority.UseFont = false;
            this.xrLabelPxR.StylePriority.UseTextAlignment = false;
            this.xrLabelPxR.Text = "Previsto X Realizado";
            this.xrLabelPxR.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // QuadroComposicaoBoleto
            // 
            this.QuadroComposicaoBoleto.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.Lista11,
            this.Lista12,
            this.xrLabel120,
            this.xrLabel116});
            this.QuadroComposicaoBoleto.Dpi = 254F;
            this.QuadroComposicaoBoleto.LocationFloat = new DevExpress.Utils.PointFloat(410F, 0F);
            this.QuadroComposicaoBoleto.Name = "QuadroComposicaoBoleto";
            this.QuadroComposicaoBoleto.SizeF = new System.Drawing.SizeF(658F, 132F);
            // 
            // Lista11
            // 
            this.Lista11.BorderColor = System.Drawing.Color.Silver;
            this.Lista11.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.Lista11.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "Composicao")});
            this.Lista11.Dpi = 254F;
            this.Lista11.Font = new System.Drawing.Font("Tahoma", 6.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Lista11.LocationFloat = new DevExpress.Utils.PointFloat(0F, 37F);
            this.Lista11.Multiline = true;
            this.Lista11.Name = "Lista11";
            this.Lista11.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.Lista11.SizeF = new System.Drawing.SizeF(525F, 59F);
            this.Lista11.Text = "Lista11";
            this.Lista11.WordWrap = false;
            // 
            // Lista12
            // 
            this.Lista12.BackColor = System.Drawing.Color.WhiteSmoke;
            this.Lista12.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "ComposicaoTotal")});
            this.Lista12.Dpi = 254F;
            this.Lista12.Font = new System.Drawing.Font("Tahoma", 6.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Lista12.LocationFloat = new DevExpress.Utils.PointFloat(525F, 37F);
            this.Lista12.Multiline = true;
            this.Lista12.Name = "Lista12";
            this.Lista12.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.Lista12.SizeF = new System.Drawing.SizeF(126F, 91F);
            this.Lista12.Text = "Lista12";
            this.Lista12.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight;
            this.Lista12.WordWrap = false;
            this.Lista12.Draw += new DevExpress.XtraReports.UI.DrawEventHandler(this.DegradeL);
            // 
            // xrLabel120
            // 
            this.xrLabel120.BackColor = System.Drawing.Color.DarkGray;
            this.xrLabel120.Dpi = 254F;
            this.xrLabel120.Font = new System.Drawing.Font("Verdana", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel120.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.xrLabel120.LocationFloat = new DevExpress.Utils.PointFloat(0F, 0F);
            this.xrLabel120.Name = "xrLabel120";
            this.xrLabel120.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrLabel120.SizeF = new System.Drawing.SizeF(652F, 37F);
            this.xrLabel120.Text = "COMPOSI��O DO BOLETO";
            this.xrLabel120.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter;
            this.xrLabel120.Draw += new DevExpress.XtraReports.UI.DrawEventHandler(this.CantosRedondosL);
            // 
            // xrLabel116
            // 
            this.xrLabel116.Dpi = 254F;
            this.xrLabel116.Font = new System.Drawing.Font("Tahoma", 6.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel116.LocationFloat = new DevExpress.Utils.PointFloat(382F, 101F);
            this.xrLabel116.Name = "xrLabel116";
            this.xrLabel116.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrLabel116.SizeF = new System.Drawing.SizeF(143F, 26F);
            this.xrLabel116.Text = "TOTAL";
            this.xrLabel116.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight;
            // 
            // xrPanel4
            // 
            this.xrPanel4.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(219)))), ((int)(((byte)(219)))), ((int)(((byte)(219)))));
            this.xrPanel4.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrLabel113,
            this.xrLabel105,
            this.xrLabel83,
            this.xrLabel30,
            this.xrLabel27,
            this.xrLabel72});
            this.xrPanel4.Dpi = 254F;
            this.xrPanel4.LocationFloat = new DevExpress.Utils.PointFloat(0F, 0F);
            this.xrPanel4.Name = "xrPanel4";
            this.xrPanel4.SizeF = new System.Drawing.SizeF(404F, 132F);
            this.xrPanel4.StylePriority.UseBackColor = false;
            // 
            // xrLabel113
            // 
            this.xrLabel113.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel113.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "IntSenha")});
            this.xrLabel113.Dpi = 254F;
            this.xrLabel113.Font = new System.Drawing.Font("Tahoma", 6.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel113.FormattingRules.Add(this.RuleOcultaUsu);
            this.xrLabel113.LocationFloat = new DevExpress.Utils.PointFloat(121.9999F, 103F);
            this.xrLabel113.Name = "xrLabel113";
            this.xrLabel113.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrLabel113.SizeF = new System.Drawing.SizeF(277.0002F, 25.00002F);
            this.xrLabel113.StylePriority.UseBorders = false;
            this.xrLabel113.Text = "xrLabel113";
            // 
            // xrLabel105
            // 
            this.xrLabel105.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel105.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "IntUsu")});
            this.xrLabel105.Dpi = 254F;
            this.xrLabel105.Font = new System.Drawing.Font("Tahoma", 6.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel105.FormattingRules.Add(this.RuleOcultaUsu);
            this.xrLabel105.LocationFloat = new DevExpress.Utils.PointFloat(121.9999F, 71F);
            this.xrLabel105.Name = "xrLabel105";
            this.xrLabel105.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrLabel105.SizeF = new System.Drawing.SizeF(277.0002F, 25F);
            this.xrLabel105.StylePriority.UseBorders = false;
            this.xrLabel105.Text = "xrLabel105";
            // 
            // xrLabel83
            // 
            this.xrLabel83.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel83.Dpi = 254F;
            this.xrLabel83.Font = new System.Drawing.Font("Tahoma", 6.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel83.LocationFloat = new DevExpress.Utils.PointFloat(4.999939F, 42F);
            this.xrLabel83.Name = "xrLabel83";
            this.xrLabel83.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrLabel83.SizeF = new System.Drawing.SizeF(394.0002F, 28F);
            this.xrLabel83.StylePriority.UseBorders = false;
            this.xrLabel83.Text = "Site: www.neonimoveis.com.br";
            // 
            // xrLabel30
            // 
            this.xrLabel30.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel30.Dpi = 254F;
            this.xrLabel30.Font = new System.Drawing.Font("Tahoma", 6F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel30.FormattingRules.Add(this.RuleOcultaUsu);
            this.xrLabel30.LocationFloat = new DevExpress.Utils.PointFloat(4.999939F, 101F);
            this.xrLabel30.Name = "xrLabel30";
            this.xrLabel30.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrLabel30.SizeF = new System.Drawing.SizeF(114.0001F, 25F);
            this.xrLabel30.StylePriority.UseBorders = false;
            this.xrLabel30.StylePriority.UseFont = false;
            this.xrLabel30.Text = "Senha:";
            // 
            // xrLabel27
            // 
            this.xrLabel27.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel27.Dpi = 254F;
            this.xrLabel27.Font = new System.Drawing.Font("Tahoma", 6F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel27.FormattingRules.Add(this.RuleOcultaUsu);
            this.xrLabel27.LocationFloat = new DevExpress.Utils.PointFloat(4.999904F, 70F);
            this.xrLabel27.Name = "xrLabel27";
            this.xrLabel27.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrLabel27.SizeF = new System.Drawing.SizeF(114.0001F, 26.00002F);
            this.xrLabel27.StylePriority.UseBorders = false;
            this.xrLabel27.StylePriority.UseFont = false;
            this.xrLabel27.Text = "Usu�rio:";
            // 
            // xrLabel72
            // 
            this.xrLabel72.BackColor = System.Drawing.Color.DarkGray;
            this.xrLabel72.Dpi = 254F;
            this.xrLabel72.Font = new System.Drawing.Font("Verdana", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel72.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.xrLabel72.LocationFloat = new DevExpress.Utils.PointFloat(0F, 0F);
            this.xrLabel72.Name = "xrLabel72";
            this.xrLabel72.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrLabel72.SizeF = new System.Drawing.SizeF(404.0001F, 37F);
            this.xrLabel72.Text = "INTERNET";
            this.xrLabel72.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter;
            this.xrLabel72.Draw += new DevExpress.XtraReports.UI.DrawEventHandler(this.CantosRedondosL);
            // 
            // xrMensagem
            // 
            this.xrMensagem.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "BOLMensagemImp")});
            this.xrMensagem.Dpi = 254F;
            this.xrMensagem.Font = new System.Drawing.Font("Tahoma", 6.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrMensagem.LocationFloat = new DevExpress.Utils.PointFloat(7.999825F, 1097.775F);
            this.xrMensagem.Multiline = true;
            this.xrMensagem.Name = "xrMensagem";
            this.xrMensagem.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrMensagem.SizeF = new System.Drawing.SizeF(1047F, 50F);
            this.xrMensagem.Visible = false;
            this.xrMensagem.Draw += new DevExpress.XtraReports.UI.DrawEventHandler(this.DegradeL);
            // 
            // QuadroUnidadeInadimplentes
            // 
            this.QuadroUnidadeInadimplentes.CanGrow = false;
            this.QuadroUnidadeInadimplentes.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.TituloUnidadesInad,
            this.TextoUnidadesInadimplentes});
            this.QuadroUnidadeInadimplentes.Dpi = 254F;
            this.QuadroUnidadeInadimplentes.LocationFloat = new DevExpress.Utils.PointFloat(5F, 0F);
            this.QuadroUnidadeInadimplentes.Name = "QuadroUnidadeInadimplentes";
            this.QuadroUnidadeInadimplentes.SizeF = new System.Drawing.SizeF(889F, 720F);
            // 
            // TituloUnidadesInad
            // 
            this.TituloUnidadesInad.BackColor = System.Drawing.Color.DarkGray;
            this.TituloUnidadesInad.CanShrink = true;
            this.TituloUnidadesInad.Dpi = 254F;
            this.TituloUnidadesInad.Font = new System.Drawing.Font("Verdana", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TituloUnidadesInad.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.TituloUnidadesInad.LocationFloat = new DevExpress.Utils.PointFloat(6F, 0F);
            this.TituloUnidadesInad.Name = "TituloUnidadesInad";
            this.TituloUnidadesInad.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.TituloUnidadesInad.SizeF = new System.Drawing.SizeF(878F, 37F);
            this.TituloUnidadesInad.Text = "UNIDADES INADIMPLENTES";
            this.TituloUnidadesInad.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter;
            this.TituloUnidadesInad.Draw += new DevExpress.XtraReports.UI.DrawEventHandler(this.CantosRedondosL);
            // 
            // TextoUnidadesInadimplentes
            // 
            this.TextoUnidadesInadimplentes.BorderColor = System.Drawing.Color.Silver;
            this.TextoUnidadesInadimplentes.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.TextoUnidadesInadimplentes.Dpi = 254F;
            this.TextoUnidadesInadimplentes.Font = new System.Drawing.Font("Courier New", 6F, System.Drawing.FontStyle.Bold);
            this.TextoUnidadesInadimplentes.LocationFloat = new DevExpress.Utils.PointFloat(6F, 37F);
            this.TextoUnidadesInadimplentes.Name = "TextoUnidadesInadimplentes";
            this.TextoUnidadesInadimplentes.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.TextoUnidadesInadimplentes.SerializableRtfString = resources.GetString("TextoUnidadesInadimplentes.SerializableRtfString");
            this.TextoUnidadesInadimplentes.SizeF = new System.Drawing.SizeF(878F, 50F);
            this.TextoUnidadesInadimplentes.StylePriority.UseBorderColor = false;
            this.TextoUnidadesInadimplentes.StylePriority.UseBorders = false;
            this.TextoUnidadesInadimplentes.StylePriority.UseFont = false;
            this.TextoUnidadesInadimplentes.StylePriority.UsePadding = false;
            // 
            // QuadroContinuacao
            // 
            this.QuadroContinuacao.CanGrow = false;
            this.QuadroContinuacao.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.TituloContinuacao});
            this.QuadroContinuacao.Dpi = 254F;
            this.QuadroContinuacao.LocationFloat = new DevExpress.Utils.PointFloat(1094F, 0F);
            this.QuadroContinuacao.Name = "QuadroContinuacao";
            this.QuadroContinuacao.SizeF = new System.Drawing.SizeF(705F, 720F);
            this.QuadroContinuacao.StylePriority.UseBorders = false;
            this.QuadroContinuacao.Visible = false;
            // 
            // TituloContinuacao
            // 
            this.TituloContinuacao.BackColor = System.Drawing.Color.DarkGray;
            this.TituloContinuacao.Dpi = 254F;
            this.TituloContinuacao.Font = new System.Drawing.Font("Verdana", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TituloContinuacao.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.TituloContinuacao.LocationFloat = new DevExpress.Utils.PointFloat(0F, 0F);
            this.TituloContinuacao.Name = "TituloContinuacao";
            this.TituloContinuacao.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.TituloContinuacao.SizeF = new System.Drawing.SizeF(700F, 37F);
            this.TituloContinuacao.Text = "DESPESAS (Continua��o)";
            this.TituloContinuacao.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter;
            this.TituloContinuacao.Draw += new DevExpress.XtraReports.UI.DrawEventHandler(this.CantosRedondosL);
            // 
            // OcultaPrevRea
            // 
            this.OcultaPrevRea.Description = "Oculta Previsto x Realizado";
            this.OcultaPrevRea.Name = "OcultaPrevRea";
            this.OcultaPrevRea.Type = typeof(bool);
            this.OcultaPrevRea.ValueInfo = "False";
            this.OcultaPrevRea.Visible = false;
            // 
            // xrLabel19
            // 
            this.xrLabel19.BorderColor = System.Drawing.Color.Silver;
            this.xrLabel19.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrLabel19.Dpi = 254F;
            this.xrLabel19.Font = new System.Drawing.Font("Tahoma", 6.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel19.LocationFloat = new DevExpress.Utils.PointFloat(5F, 58F);
            this.xrLabel19.Multiline = true;
            this.xrLabel19.Name = "xrLabel19";
            this.xrLabel19.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrLabel19.SizeF = new System.Drawing.SizeF(451F, 233F);
            // 
            // SaldoFinal
            // 
            this.SaldoFinal.BackColor = System.Drawing.Color.WhiteSmoke;
            this.SaldoFinal.Dpi = 254F;
            this.SaldoFinal.Font = new System.Drawing.Font("Tahoma", 6.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.SaldoFinal.LocationFloat = new DevExpress.Utils.PointFloat(593.0001F, 58F);
            this.SaldoFinal.Multiline = true;
            this.SaldoFinal.Name = "SaldoFinal";
            this.SaldoFinal.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.SaldoFinal.SizeF = new System.Drawing.SizeF(130F, 233F);
            this.SaldoFinal.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight;
            this.SaldoFinal.WordWrap = false;
            this.SaldoFinal.Draw += new DevExpress.XtraReports.UI.DrawEventHandler(this.DegradeL);
            // 
            // ImpBoletoBal
            // 
            this.Bands.AddRange(new DevExpress.XtraReports.UI.Band[] {
            this.Detail,
            this.bottomMarginBand1});
            this.FormattingRuleSheet.AddRange(new DevExpress.XtraReports.UI.FormattingRule[] {
            this.RuleOcultaUsu,
            this.OcultaPre});
            this.Parameters.AddRange(new DevExpress.XtraReports.Parameters.Parameter[] {
            this.OcultaPrevRea});
            this.Version = "15.2";
            this.DataSourceRowChanged += new DevExpress.XtraReports.UI.DataSourceRowEventHandler(this.ImpBoletoBal_DataSourceRowChanged);
            this.BeforePrint += new System.Drawing.Printing.PrintEventHandler(this.ImpBoletoBal_BeforePrint);
            ((System.ComponentModel.ISupportInitialize)(this.dImpBoletoBase1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dImpDestRem1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.bindingSourcePrincipal)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.bindingSource1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrRichText1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TextoUnidadesInadimplentes)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();

        }

        #endregion

        private DevExpress.XtraReports.UI.XRPanel QuadroDespesas;
        private DevExpress.XtraReports.UI.XRPanel QuadroContinua;
        private DevExpress.XtraReports.UI.XRLabel xrLabel31;
        private DevExpress.XtraReports.UI.XRPanel QuadroTotais;
        private DevExpress.XtraReports.UI.XRLabel xrLabel26;
        private DevExpress.XtraReports.UI.XRLabel xrTotalDesp;
        private DevExpress.XtraReports.UI.XRPanel PDespADMA;
        private DevExpress.XtraReports.UI.XRLabel DespesaValor2;
        private DevExpress.XtraReports.UI.XRLabel DespesaNome2;
        private DevExpress.XtraReports.UI.XRLabel xrLabel15;
        private DevExpress.XtraReports.UI.XRLabel ADMpor;
        private DevExpress.XtraReports.UI.XRPanel xrPanel39;
        private DevExpress.XtraReports.UI.XRLine ADMLine;
        private DevExpress.XtraReports.UI.XRLine ADMLine1;
        private DevExpress.XtraReports.UI.XRLabel xrLabel16;
        private DevExpress.XtraReports.UI.XRPanel PAdministradora;
        private DevExpress.XtraReports.UI.XRLabel DespesaValorADM;
        private DevExpress.XtraReports.UI.XRLabel DespesaNomeADM;
        private DevExpress.XtraReports.UI.XRLabel xrLabel20;
        private DevExpress.XtraReports.UI.XRLabel ADMApor;
        private DevExpress.XtraReports.UI.XRPanel xrPanel40;
        private DevExpress.XtraReports.UI.XRLine ADMALine;
        private DevExpress.XtraReports.UI.XRLine ADMALine1;
        private DevExpress.XtraReports.UI.XRLabel xrLabel17;
        private DevExpress.XtraReports.UI.XRLabel QuadroDesp;
        private DevExpress.XtraReports.UI.XRPanel PDespG;
        private DevExpress.XtraReports.UI.XRLabel xrLabel200;
        private DevExpress.XtraReports.UI.XRPanel xrPanel6;
        private DevExpress.XtraReports.UI.XRLine DGLine;
        private DevExpress.XtraReports.UI.XRLine DGLine1;
        private DevExpress.XtraReports.UI.XRLabel DGpor;
        private DevExpress.XtraReports.UI.XRLabel xrLabel13;
        private DevExpress.XtraReports.UI.XRLabel DespesaValor;
        private DevExpress.XtraReports.UI.XRLabel DespesaNome;
        private DevExpress.XtraReports.UI.XRPanel PdespMO;
        private DevExpress.XtraReports.UI.XRLabel xrLabel3XX;
        private DevExpress.XtraReports.UI.XRPanel xrPanel9;
        private DevExpress.XtraReports.UI.XRLine MOLine;
        private DevExpress.XtraReports.UI.XRLine MOLine1;
        private DevExpress.XtraReports.UI.XRLabel MOpor;
        private DevExpress.XtraReports.UI.XRLabel DespesaValor1;
        private DevExpress.XtraReports.UI.XRLabel xrLabel14;
        private DevExpress.XtraReports.UI.XRLabel DespesaNome1;
        private DevExpress.XtraReports.UI.XRPanel PDespB;
        private DevExpress.XtraReports.UI.XRLabel xrLabel21;
        private DevExpress.XtraReports.UI.XRPanel xrPanel41;
        private DevExpress.XtraReports.UI.XRLine BancLine;
        private DevExpress.XtraReports.UI.XRLine BancLine1;
        private DevExpress.XtraReports.UI.XRLabel Bancpor;
        private DevExpress.XtraReports.UI.XRLabel xrLabel52;
        private DevExpress.XtraReports.UI.XRLabel DespesaNomeBANC;
        private DevExpress.XtraReports.UI.XRLabel DespesaValorBANC;
        private DevExpress.XtraReports.UI.XRPanel PDespH;
        private DevExpress.XtraReports.UI.XRLabel DespesaNomeADV;
        private DevExpress.XtraReports.UI.XRLabel xrLabel23;
        private DevExpress.XtraReports.UI.XRPanel xrPanel42;
        private DevExpress.XtraReports.UI.XRLine HORLine;
        private DevExpress.XtraReports.UI.XRLine HORLine1;
        private DevExpress.XtraReports.UI.XRLabel HORpor;
        private DevExpress.XtraReports.UI.XRLabel xrLabel56;
        private DevExpress.XtraReports.UI.XRLabel DespesaValorADV;
        private DevExpress.XtraReports.UI.XRPanel QuadroEsquerda;
        private DevExpress.XtraReports.UI.XRPanel QuadroComposicaoBoleto;
        private DevExpress.XtraReports.UI.XRLabel Lista11;
        private DevExpress.XtraReports.UI.XRLabel Lista12;
        private DevExpress.XtraReports.UI.XRLabel xrLabel120;
        private DevExpress.XtraReports.UI.XRLabel xrLabel116;
        private DevExpress.XtraReports.UI.XRPanel QuadroReceitas;
        private DevExpress.XtraReports.UI.XRPanel xrPanel2;
        private DevExpress.XtraReports.UI.XRPanel xrPanel37;
        private DevExpress.XtraReports.UI.XRLabel xrLabel40;
        private DevExpress.XtraReports.UI.XRLabel xrLabel39;
        private DevExpress.XtraReports.UI.XRLabel xrLabel38;
        private DevExpress.XtraReports.UI.XRLabel xrLabel37;
        private DevExpress.XtraReports.UI.XRLabel RecetaP;
        private DevExpress.XtraReports.UI.XRLabel ReceitaMes;
        private DevExpress.XtraReports.UI.XRLabel ReceitaA;
        private DevExpress.XtraReports.UI.XRLabel ReceitaT;
        private DevExpress.XtraReports.UI.XRLabel xrLabel12;
        private DevExpress.XtraReports.UI.XRLabel xrLabel28;
        private DevExpress.XtraReports.UI.XRLabel ReceitaNome;
        private DevExpress.XtraReports.UI.XRLabel LabelHono;
        private DevExpress.XtraReports.UI.XRLabel SubTotHono;
        private DevExpress.XtraReports.UI.XRLabel xrLabel54;
        private DevExpress.XtraReports.UI.XRLabel TotR;
        private DevExpress.XtraReports.UI.XRPanel QuadroSaldo;
        private DevExpress.XtraReports.UI.XRPanel xrPanel38;
        private DevExpress.XtraReports.UI.XRLabel xrSaldoFinal;
        private DevExpress.XtraReports.UI.XRLabel xrContasLogicas;
        private DevExpress.XtraReports.UI.XRLabel xrLabel18;
        private DevExpress.XtraReports.UI.XRPanel QuadroMensagem;
        private DevExpress.XtraReports.UI.XRLabel xrMensagem;
        private DevExpress.XtraReports.UI.XRLabel xrLabel118;
        private DevExpress.XtraReports.UI.XRPanel QuadroPendenciasUnidade;
        private DevExpress.XtraReports.UI.XRLabel xrLabel76;
        private DevExpress.XtraReports.UI.XRLabel labRespBA;
        private DevExpress.XtraReports.UI.XRLabel labValorBA;
        private DevExpress.XtraReports.UI.XRLabel labVenctoBA;
        private DevExpress.XtraReports.UI.XRLabel labCategoriaBA;
        private DevExpress.XtraReports.UI.XRPanel QuadroConsumo;
        private DevExpress.XtraReports.UI.XRLabel xrLabel58;
        private DevExpress.XtraReports.UI.XRPanel SubQuadroGas;
        private DevExpress.XtraReports.UI.XRLabel TituloGas;
        private DevExpress.XtraReports.UI.XRLabel ConsValGasT;
        private DevExpress.XtraReports.UI.XRLabel ConsKgGasT;
        private DevExpress.XtraReports.UI.XRLabel Consm3GasT;
        private DevExpress.XtraReports.UI.XRLabel ConsLeitGasT;
        private DevExpress.XtraReports.UI.XRPanel xrPanel45;
        private DevExpress.XtraReports.UI.XRLabel xrLabel139;
        private DevExpress.XtraReports.UI.XRLabel ConsAguaT;
        private DevExpress.XtraReports.UI.XRLabel ConsValAguaT;
        private DevExpress.XtraReports.UI.XRLabel Consm3AguaT;
        private DevExpress.XtraReports.UI.XRLabel ConsLeitAguaT;
        private DevExpress.XtraReports.UI.XRLabel ConsMes;
        private DevExpress.XtraReports.UI.XRLabel ConsLeitGas;
        private DevExpress.XtraReports.UI.XRLabel ConsKgGas;
        private DevExpress.XtraReports.UI.XRLabel Consm3Gas;
        private DevExpress.XtraReports.UI.XRLabel ConsPorcGas;
        private DevExpress.XtraReports.UI.XRLabel ConsValGas;
        private DevExpress.XtraReports.UI.XRLabel ConsValAgua;
        private DevExpress.XtraReports.UI.XRLabel Consm3Agua;
        private DevExpress.XtraReports.UI.XRLabel ConsLeitAgua;
        private DevExpress.XtraReports.UI.XRLabel ConsPorcAgua;
        private DevExpress.XtraReports.UI.XRPanel QuadroUnidadeInadimplentes;
        private DevExpress.XtraReports.UI.XRLabel TituloUnidadesInad;
        private DevExpress.XtraReports.UI.XRPanel QuadroContinuacao;
        private DevExpress.XtraReports.UI.XRLabel TituloContinuacao;
        private DevExpress.XtraReports.UI.XRLabel xrLabel10;
        private DevExpress.XtraReports.UI.XRLabel xrLabel22;
        private DevExpress.XtraReports.UI.XRPanel xrPanel4;
        private DevExpress.XtraReports.UI.XRLabel xrLabel72;
        private DevExpress.XtraReports.UI.XRLabel xrLabel113;
        private DevExpress.XtraReports.UI.XRLabel xrLabel105;
        private DevExpress.XtraReports.UI.XRLabel xrLabel83;
        private DevExpress.XtraReports.UI.XRLabel xrLabel30;
        private DevExpress.XtraReports.UI.XRLabel xrLabel27;
        private DevExpress.XtraReports.UI.XRLabel xrPrevXEf2;
        private DevExpress.XtraReports.UI.XRLabel xrPrevXEf1;
        private DevExpress.XtraReports.UI.XRLabel ReceitaPrev;
        private DevExpress.XtraReports.UI.XRLabel xrLabelPxR;
        private DevExpress.XtraReports.UI.XRLabel ReceitaTT;
        private DevExpress.XtraReports.UI.XRLabel RecetaPT;
        private DevExpress.XtraReports.UI.XRLabel ReceitaMesT;
        private DevExpress.XtraReports.UI.XRLabel ReceitaAT;
        private DevExpress.XtraReports.UI.XRLabel ReceitaPrevT;
        private DevExpress.XtraReports.UI.XRLine xrLine11;
        private DevExpress.XtraReports.UI.XRLabel xrLabel29;
        private DevExpress.XtraReports.UI.FormattingRule OcultaPre;        
        private DevExpress.XtraReports.Parameters.Parameter OcultaPrevRea;
        private DevExpress.XtraReports.UI.XRLabel xrSaldoInicial;
        private DevExpress.XtraReports.UI.XRLabel xrDataSaldoFinal;
        private DevExpress.XtraReports.UI.XRLabel xrDataSaldoInicial;
        private DevExpress.XtraReports.UI.XRLabel xrLabel19;
        private DevExpress.XtraReports.UI.XRLabel SaldoFinal;
        private DevExpress.XtraReports.UI.XRLabel ModeloSGrupo;
        private DevExpress.XtraReports.UI.XRPanel Modelo_PanelGrupo;
        private DevExpress.XtraReports.UI.XRLabel Modelo_Label_Sub_Total;
        private DevExpress.XtraReports.UI.XRPanel Modelo_PanelPorcent;
        private DevExpress.XtraReports.UI.XRLine Modelo_LinePor1;
        private DevExpress.XtraReports.UI.XRLine Modelo_LinePor2;
        private DevExpress.XtraReports.UI.XRLabel Modelo_LabelValPor;
        private DevExpress.XtraReports.UI.XRLabel Modelo_LabelTituloGrupo;
        private DevExpress.XtraReports.UI.XRLabel Modelo_LabelValorGrupo;
        private DevExpress.XtraReports.UI.XRLabel Modelo_LabelDescrisGrupo;
        private DevExpress.XtraReports.UI.XRLabel Modelo_Label_Sub_TotalVal;
        private DevExpress.XtraReports.UI.XRRichText xrRichText1;
        private DevExpress.XtraReports.UI.XRLabel Modelo_Label_Sub_TotalValG;
        private DevExpress.XtraReports.UI.XRLabel Modelo_Label_Sub_TotalG;
        private DevExpress.XtraReports.UI.XRLabel xrTransferencias;
        private DevExpress.XtraReports.UI.XRLabel xrDebitos;
        private DevExpress.XtraReports.UI.XRLabel xrCreditos;
        private DevExpress.XtraReports.UI.XRLabel xrTransferenciaT;
        private DevExpress.XtraReports.UI.XRLabel xrDebitosT;
        private DevExpress.XtraReports.UI.XRLabel xrCreditosT;
        private DevExpress.XtraReports.UI.XRLabel xrRentabilidade;
        private DevExpress.XtraReports.UI.XRLabel xrRentabilidadeT;
        private DevExpress.XtraReports.UI.XRLabel Consm3Luz;
        private DevExpress.XtraReports.UI.XRLabel ConsPorcLuz;
        private DevExpress.XtraReports.UI.XRLabel ConsValLuz;
        private DevExpress.XtraReports.UI.XRLabel ConsValLuzT;
        private DevExpress.XtraReports.UI.XRLabel Consm3LuzT;
        private DevExpress.XtraReports.UI.XRLabel ConsLuzT;
        private DevExpress.XtraReports.UI.XRRichText TextoUnidadesInadimplentes;
        
        
        
    }
}