﻿namespace dllImpresso
{


    partial class dImpBoletoBase
    {
        //partial class BOLETOSIMPDataTable
        //{
        //}

        private dImpBoletoBaseTableAdapters.CONDOMINIOSTableAdapter cONDOMINIOSTableAdapter;
        /// <summary>
        /// TableAdapter padrão para tabela: CONDOMINIOS
        /// </summary>
        public dImpBoletoBaseTableAdapters.CONDOMINIOSTableAdapter CONDOMINIOSTableAdapter
        {
            get
            {
                if (cONDOMINIOSTableAdapter == null)
                {
                    cONDOMINIOSTableAdapter = new dImpBoletoBaseTableAdapters.CONDOMINIOSTableAdapter();
                    cONDOMINIOSTableAdapter.TrocarStringDeConexao();
                };
                return cONDOMINIOSTableAdapter;
            }
        }

        private dImpBoletoBaseTableAdapters.PrevistoTableAdapter previstoTableAdapter;
        /// <summary>
        /// TableAdapter padrão para tabela: Previsto
        /// </summary>
        public dImpBoletoBaseTableAdapters.PrevistoTableAdapter PrevistoTableAdapter
        {
            get
            {
                if (previstoTableAdapter == null)
                {
                    previstoTableAdapter = new dImpBoletoBaseTableAdapters.PrevistoTableAdapter();
                    previstoTableAdapter.TrocarStringDeConexao();
                };
                return previstoTableAdapter;
            }
        }


        

        private dImpBoletoBaseTableAdapters.BOLetosAbertosTableAdapter bOLetosAbertosTableAdapter;
        /// <summary>
        /// TableAdapter padrão para tabela: BOLetosAbertos
        /// </summary>
        public dImpBoletoBaseTableAdapters.BOLetosAbertosTableAdapter BOLetosAbertosTableAdapter
        {
            get
            {
                if (bOLetosAbertosTableAdapter == null)
                {
                    bOLetosAbertosTableAdapter = new dImpBoletoBaseTableAdapters.BOLetosAbertosTableAdapter();
                    bOLetosAbertosTableAdapter.TrocarStringDeConexao();
                };
                return bOLetosAbertosTableAdapter;
            }
        }

        

        private dImpBoletoBaseTableAdapters.ConsumoTableAdapter consumoTableAdapter;

        /// <summary>
        /// 
        /// </summary>
        public dImpBoletoBaseTableAdapters.ConsumoTableAdapter ConsumoTableAdapter
        {
            get
            {
                if (consumoTableAdapter == null)
                {
                    consumoTableAdapter = new dllImpresso.dImpBoletoBaseTableAdapters.ConsumoTableAdapter();
                    consumoTableAdapter.TrocarStringDeConexao();
                };
                return consumoTableAdapter;
            }
        }




    }
}
