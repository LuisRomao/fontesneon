namespace dllImpresso
{
    /// <summary>
    /// Impresso para Boleto com recibo
    /// </summary>
    partial class ImpBoletoReciboSV
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(ImpBoletoReciboSV));
            this.QuadroAutent = new DevExpress.XtraReports.UI.XRPanel();
            this.QuadroCheques2 = new DevExpress.XtraReports.UI.XRPanel();
            this.xrLabel66 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel72 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel76 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel83 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel105 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel114 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel121 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel125 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel126 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel133 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLine11 = new DevExpress.XtraReports.UI.XRLine();
            this.xrPanel9 = new DevExpress.XtraReports.UI.XRPanel();
            this.xrLabel38 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel39 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrPanel2 = new DevExpress.XtraReports.UI.XRPanel();
            this.xrLabel31 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel29 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel30 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel37 = new DevExpress.XtraReports.UI.XRLabel();
            this.QuadroTopoAut = new DevExpress.XtraReports.UI.XRPanel();
            this.xrPanel6 = new DevExpress.XtraReports.UI.XRPanel();
            this.xrLabel16 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel17 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel18 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel19 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel20 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel21 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel22 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel23 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel26 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel27 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel28 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrPictureBox2 = new DevExpress.XtraReports.UI.XRPictureBox();
            this.xrPanel4 = new DevExpress.XtraReports.UI.XRPanel();
            this.xrLabel2Duvida1 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel3Duvida1 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel12 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel13 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel14 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel15 = new DevExpress.XtraReports.UI.XRLabel();
            this.QuadroCheques = new DevExpress.XtraReports.UI.XRPanel();
            this.LabelCheques = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel60 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel58 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel56 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel54 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel52 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel51 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel50 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel49 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel41 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel40 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrDataPag = new DevExpress.XtraReports.UI.XRLabel();
            ((System.ComponentModel.ISupportInitialize)(this.QuadroMensagem)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dImpBoletoBase1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dImpDestRem1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.bindingSourcePrincipal)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.bindingSource1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            // 
            // xrTitulo
            // 
            this.xrTitulo.Font = new System.Drawing.Font("Times New Roman", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrTitulo.ForeColor = System.Drawing.Color.Black;
            this.xrTitulo.LocationFloat = new DevExpress.Utils.PointFloat(21F, 339F);
            this.xrTitulo.SizeF = new System.Drawing.SizeF(1762F, 79F);
            this.xrTitulo.StylePriority.UseFont = false;
            this.xrTitulo.StylePriority.UseForeColor = false;
            this.xrTitulo.Text = "Comprovante de pagamento";
            // 
            // QuadroComposicaoBoleto
            // 
            this.QuadroComposicaoBoleto.LocationFloat = new DevExpress.Utils.PointFloat(0F, 21F);
            // 
            // QuadroMensagem
            // 
            this.QuadroMensagem.LocationFloat = new DevExpress.Utils.PointFloat(881F, 21F);
            this.QuadroMensagem.SizeF = new System.Drawing.SizeF(915F, 122F);
            // 
            // xrLabelUsoBanco
            // 
            this.xrLabelUsoBanco.StylePriority.UseBorders = false;
            // 
            // xrLabelCarteira
            // 
            this.xrLabelCarteira.StylePriority.UseBorders = false;
            // 
            // xrLabelEspecie
            // 
            this.xrLabelEspecie.StylePriority.UseBorders = false;
            // 
            // BANCOLocal
            // 
            this.BANCOLocal.StylePriority.UseBorders = false;
            this.BANCOLocal.StylePriority.UseFont = false;
            // 
            // BANCONumero
            // 
            this.BANCONumero.StylePriority.UseFont = false;
            // 
            // QuadroUtil
            // 
            this.QuadroUtil.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.QuadroCheques,
            this.QuadroAutent});
            this.QuadroUtil.Controls.SetChildIndex(this.QuadroAutent, 0);
            this.QuadroUtil.Controls.SetChildIndex(this.QuadroCheques, 0);
            this.QuadroUtil.Controls.SetChildIndex(this.QuadroComposicaoBoleto, 0);
            this.QuadroUtil.Controls.SetChildIndex(this.xrTitulo, 0);
            this.QuadroUtil.Controls.SetChildIndex(this.QuadroMensagem, 0);
            // 
            // xrLabel7
            // 
            this.xrLabel7.Text = "PAGAMENTO";
            // 
            // xrLabel6
            // 
            this.xrLabel6.LocationFloat = new DevExpress.Utils.PointFloat(1182.017F, 83.00001F);
            this.xrLabel6.SizeF = new System.Drawing.SizeF(8.701538F, 39.99998F);
            this.xrLabel6.StylePriority.UseFont = false;
            this.xrLabel6.StylePriority.UseTextAlignment = false;
            this.xrLabel6.Visible = false;
            // 
            // rLabelcompet
            // 
            this.rLabelcompet.StylePriority.UseBorders = false;
            // 
            // xrLabel3
            // 
            this.xrLabel3.StylePriority.UseBorders = false;
            // 
            // QuadroDadosTopo
            // 
            this.QuadroDadosTopo.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrDataPag});
            this.QuadroDadosTopo.StylePriority.UseBackColor = false;
            this.QuadroDadosTopo.Controls.SetChildIndex(this.xrDataPag, 0);
            this.QuadroDadosTopo.Controls.SetChildIndex(this.xrLabel6, 0);
            this.QuadroDadosTopo.Controls.SetChildIndex(this.xrLabel7, 0);
            this.QuadroDadosTopo.Controls.SetChildIndex(this.xrLabelcompet1, 0);
            // 
            // RuleOcultaUsu
            // 
            // 
            // 
            // 
            this.RuleOcultaUsu.Formatting.Visible = DevExpress.Utils.DefaultBoolean.False;
            // 
            // QuadroSegundaPagina
            // 
            this.QuadroSegundaPagina.StylePriority.UseBackColor = false;
            // 
            // Detail
            // 
            this.Detail.StylePriority.UseTextAlignment = false;
            // 
            // xrSigla1
            // 
            this.xrSigla1.StylePriority.UseFont = false;
            this.xrSigla1.StylePriority.UseTextAlignment = false;
            // 
            // QuadroAutent
            // 
            this.QuadroAutent.AnchorVertical = DevExpress.XtraReports.UI.VerticalAnchorStyles.Top;
            this.QuadroAutent.CanGrow = false;
            this.QuadroAutent.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.QuadroCheques2,
            this.xrLine11,
            this.xrPanel9,
            this.xrPanel2,
            this.QuadroTopoAut});
            this.QuadroAutent.Dpi = 254F;
            this.QuadroAutent.LocationFloat = new DevExpress.Utils.PointFloat(0F, 902F);
            this.QuadroAutent.Name = "QuadroAutent";
            this.QuadroAutent.SizeF = new System.Drawing.SizeF(1799F, 802F);
            // 
            // QuadroCheques2
            // 
            this.QuadroCheques2.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrLabel66,
            this.xrLabel72,
            this.xrLabel76,
            this.xrLabel83,
            this.xrLabel105,
            this.xrLabel114,
            this.xrLabel121,
            this.xrLabel125,
            this.xrLabel126,
            this.xrLabel133});
            this.QuadroCheques2.Dpi = 254F;
            this.QuadroCheques2.LocationFloat = new DevExpress.Utils.PointFloat(801F, 312F);
            this.QuadroCheques2.Name = "QuadroCheques2";
            this.QuadroCheques2.SizeF = new System.Drawing.SizeF(995F, 153F);
            // 
            // xrLabel66
            // 
            this.xrLabel66.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(219)))), ((int)(((byte)(219)))), ((int)(((byte)(219)))));
            this.xrLabel66.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "A1", "{0:dd/MM/yy}")});
            this.xrLabel66.Dpi = 254F;
            this.xrLabel66.Font = new System.Drawing.Font("Times New Roman", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel66.LocationFloat = new DevExpress.Utils.PointFloat(0F, 79F);
            this.xrLabel66.Multiline = true;
            this.xrLabel66.Name = "xrLabel66";
            this.xrLabel66.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrLabel66.SizeF = new System.Drawing.SizeF(138F, 58F);
            this.xrLabel66.StylePriority.UseFont = false;
            this.xrLabel66.Text = "xrLabel6";
            this.xrLabel66.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomCenter;
            // 
            // xrLabel72
            // 
            this.xrLabel72.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(219)))), ((int)(((byte)(219)))), ((int)(((byte)(219)))));
            this.xrLabel72.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "A2", "{0:dd/MM/yy}")});
            this.xrLabel72.Dpi = 254F;
            this.xrLabel72.Font = new System.Drawing.Font("Times New Roman", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel72.LocationFloat = new DevExpress.Utils.PointFloat(144F, 79F);
            this.xrLabel72.Multiline = true;
            this.xrLabel72.Name = "xrLabel72";
            this.xrLabel72.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrLabel72.SizeF = new System.Drawing.SizeF(178F, 58F);
            this.xrLabel72.StylePriority.UseFont = false;
            this.xrLabel72.Text = "xrLabel6";
            this.xrLabel72.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomCenter;
            // 
            // xrLabel76
            // 
            this.xrLabel76.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(219)))), ((int)(((byte)(219)))), ((int)(((byte)(219)))));
            this.xrLabel76.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "A3", "{0:dd/MM/yy}")});
            this.xrLabel76.Dpi = 254F;
            this.xrLabel76.Font = new System.Drawing.Font("Times New Roman", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel76.LocationFloat = new DevExpress.Utils.PointFloat(330F, 79F);
            this.xrLabel76.Multiline = true;
            this.xrLabel76.Name = "xrLabel76";
            this.xrLabel76.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrLabel76.SizeF = new System.Drawing.SizeF(226F, 58F);
            this.xrLabel76.StylePriority.UseFont = false;
            this.xrLabel76.Text = "xrLabel6";
            this.xrLabel76.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomCenter;
            // 
            // xrLabel83
            // 
            this.xrLabel83.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(219)))), ((int)(((byte)(219)))), ((int)(((byte)(219)))));
            this.xrLabel83.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "A4", "{0:dd/MM/yy}")});
            this.xrLabel83.Dpi = 254F;
            this.xrLabel83.Font = new System.Drawing.Font("Times New Roman", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel83.LocationFloat = new DevExpress.Utils.PointFloat(563F, 79F);
            this.xrLabel83.Multiline = true;
            this.xrLabel83.Name = "xrLabel83";
            this.xrLabel83.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrLabel83.SizeF = new System.Drawing.SizeF(220F, 58F);
            this.xrLabel83.StylePriority.UseFont = false;
            this.xrLabel83.Text = "xrLabel6";
            this.xrLabel83.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomCenter;
            // 
            // xrLabel105
            // 
            this.xrLabel105.BackColor = System.Drawing.Color.DarkGray;
            this.xrLabel105.Dpi = 254F;
            this.xrLabel105.Font = new System.Drawing.Font("Verdana", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel105.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.xrLabel105.LocationFloat = new DevExpress.Utils.PointFloat(0F, 21F);
            this.xrLabel105.Name = "xrLabel105";
            this.xrLabel105.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrLabel105.SizeF = new System.Drawing.SizeF(138F, 53F);
            this.xrLabel105.StylePriority.UseFont = false;
            this.xrLabel105.Text = "BANCO";
            this.xrLabel105.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomCenter;
            this.xrLabel105.Draw += new DevExpress.XtraReports.UI.DrawEventHandler(this.CantoRedondo);
            // 
            // xrLabel114
            // 
            this.xrLabel114.BackColor = System.Drawing.Color.DarkGray;
            this.xrLabel114.Dpi = 254F;
            this.xrLabel114.Font = new System.Drawing.Font("Verdana", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel114.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.xrLabel114.LocationFloat = new DevExpress.Utils.PointFloat(144F, 21F);
            this.xrLabel114.Name = "xrLabel114";
            this.xrLabel114.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrLabel114.SizeF = new System.Drawing.SizeF(178F, 53F);
            this.xrLabel114.StylePriority.UseFont = false;
            this.xrLabel114.Text = "AG�NCIA";
            this.xrLabel114.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomCenter;
            this.xrLabel114.Draw += new DevExpress.XtraReports.UI.DrawEventHandler(this.CantoRedondo);
            // 
            // xrLabel121
            // 
            this.xrLabel121.BackColor = System.Drawing.Color.DarkGray;
            this.xrLabel121.Dpi = 254F;
            this.xrLabel121.Font = new System.Drawing.Font("Verdana", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel121.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.xrLabel121.LocationFloat = new DevExpress.Utils.PointFloat(330F, 21F);
            this.xrLabel121.Name = "xrLabel121";
            this.xrLabel121.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrLabel121.SizeF = new System.Drawing.SizeF(226F, 53F);
            this.xrLabel121.StylePriority.UseFont = false;
            this.xrLabel121.Text = "CONTA";
            this.xrLabel121.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomCenter;
            this.xrLabel121.Draw += new DevExpress.XtraReports.UI.DrawEventHandler(this.CantoRedondo);
            // 
            // xrLabel125
            // 
            this.xrLabel125.BackColor = System.Drawing.Color.DarkGray;
            this.xrLabel125.Dpi = 254F;
            this.xrLabel125.Font = new System.Drawing.Font("Verdana", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel125.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.xrLabel125.LocationFloat = new DevExpress.Utils.PointFloat(563F, 21F);
            this.xrLabel125.Name = "xrLabel125";
            this.xrLabel125.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrLabel125.SizeF = new System.Drawing.SizeF(220F, 53F);
            this.xrLabel125.StylePriority.UseFont = false;
            this.xrLabel125.Text = "N�MERO";
            this.xrLabel125.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomCenter;
            this.xrLabel125.Draw += new DevExpress.XtraReports.UI.DrawEventHandler(this.CantoRedondo);
            // 
            // xrLabel126
            // 
            this.xrLabel126.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(219)))), ((int)(((byte)(219)))), ((int)(((byte)(219)))));
            this.xrLabel126.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "A5", "{0:dd/MM/yy}")});
            this.xrLabel126.Dpi = 254F;
            this.xrLabel126.Font = new System.Drawing.Font("Times New Roman", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel126.LocationFloat = new DevExpress.Utils.PointFloat(790F, 80F);
            this.xrLabel126.Multiline = true;
            this.xrLabel126.Name = "xrLabel126";
            this.xrLabel126.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrLabel126.SizeF = new System.Drawing.SizeF(199F, 58F);
            this.xrLabel126.StylePriority.UseFont = false;
            this.xrLabel126.Text = "xrLabel6";
            this.xrLabel126.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomCenter;
            // 
            // xrLabel133
            // 
            this.xrLabel133.BackColor = System.Drawing.Color.DarkGray;
            this.xrLabel133.Dpi = 254F;
            this.xrLabel133.Font = new System.Drawing.Font("Verdana", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel133.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.xrLabel133.LocationFloat = new DevExpress.Utils.PointFloat(790F, 22F);
            this.xrLabel133.Name = "xrLabel133";
            this.xrLabel133.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrLabel133.SizeF = new System.Drawing.SizeF(199F, 53F);
            this.xrLabel133.StylePriority.UseFont = false;
            this.xrLabel133.Text = "VALOR";
            this.xrLabel133.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomCenter;
            this.xrLabel133.Draw += new DevExpress.XtraReports.UI.DrawEventHandler(this.CantoRedondo);
            // 
            // xrLine11
            // 
            this.xrLine11.Dpi = 254F;
            this.xrLine11.LineStyle = System.Drawing.Drawing2D.DashStyle.Dash;
            this.xrLine11.LineWidth = 5;
            this.xrLine11.LocationFloat = new DevExpress.Utils.PointFloat(5F, 0F);
            this.xrLine11.Name = "xrLine11";
            this.xrLine11.SizeF = new System.Drawing.SizeF(1791F, 13F);
            // 
            // xrPanel9
            // 
            this.xrPanel9.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrLabel38,
            this.xrLabel39});
            this.xrPanel9.Dpi = 254F;
            this.xrPanel9.LocationFloat = new DevExpress.Utils.PointFloat(889F, 190F);
            this.xrPanel9.Name = "xrPanel9";
            this.xrPanel9.SizeF = new System.Drawing.SizeF(863F, 106F);
            // 
            // xrLabel38
            // 
            this.xrLabel38.BackColor = System.Drawing.Color.DarkGray;
            this.xrLabel38.CanShrink = true;
            this.xrLabel38.Dpi = 254F;
            this.xrLabel38.Font = new System.Drawing.Font("Verdana", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel38.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.xrLabel38.LocationFloat = new DevExpress.Utils.PointFloat(5F, 0F);
            this.xrLabel38.Name = "xrLabel38";
            this.xrLabel38.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrLabel38.SizeF = new System.Drawing.SizeF(847F, 53F);
            this.xrLabel38.Text = "MENSAGEM IMPORTANTE";
            this.xrLabel38.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter;
            this.xrLabel38.Draw += new DevExpress.XtraReports.UI.DrawEventHandler(this.CantoRedondo);
            // 
            // xrLabel39
            // 
            this.xrLabel39.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "BOLMensagemImp")});
            this.xrLabel39.Dpi = 254F;
            this.xrLabel39.Font = new System.Drawing.Font("Tahoma", 6.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel39.LocationFloat = new DevExpress.Utils.PointFloat(5F, 56F);
            this.xrLabel39.Multiline = true;
            this.xrLabel39.Name = "xrLabel39";
            this.xrLabel39.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrLabel39.SizeF = new System.Drawing.SizeF(847F, 50F);
            this.xrLabel39.Text = "xrMensagem";
            this.xrLabel39.Draw += new DevExpress.XtraReports.UI.DrawEventHandler(this.DrawDegrade);
            // 
            // xrPanel2
            // 
            this.xrPanel2.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrLabel31,
            this.xrLabel29,
            this.xrLabel30,
            this.xrLabel37});
            this.xrPanel2.Dpi = 254F;
            this.xrPanel2.LocationFloat = new DevExpress.Utils.PointFloat(0F, 190F);
            this.xrPanel2.Name = "xrPanel2";
            this.xrPanel2.SizeF = new System.Drawing.SizeF(791F, 132F);
            // 
            // xrLabel31
            // 
            this.xrLabel31.BackColor = System.Drawing.Color.DarkGray;
            this.xrLabel31.Dpi = 254F;
            this.xrLabel31.Font = new System.Drawing.Font("Verdana", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel31.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.xrLabel31.LocationFloat = new DevExpress.Utils.PointFloat(0F, 0F);
            this.xrLabel31.Name = "xrLabel31";
            this.xrLabel31.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrLabel31.SizeF = new System.Drawing.SizeF(784F, 37F);
            this.xrLabel31.Text = "COMPOSI��O DO BOLETO";
            this.xrLabel31.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter;
            this.xrLabel31.Draw += new DevExpress.XtraReports.UI.DrawEventHandler(this.CantoRedondo);
            // 
            // xrLabel29
            // 
            this.xrLabel29.BorderColor = System.Drawing.Color.Silver;
            this.xrLabel29.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrLabel29.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "Composicao")});
            this.xrLabel29.Dpi = 254F;
            this.xrLabel29.Font = new System.Drawing.Font("Tahoma", 6.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel29.LocationFloat = new DevExpress.Utils.PointFloat(0F, 37F);
            this.xrLabel29.Multiline = true;
            this.xrLabel29.Name = "xrLabel29";
            this.xrLabel29.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrLabel29.SizeF = new System.Drawing.SizeF(646F, 59F);
            this.xrLabel29.Text = "Lista11";
            // 
            // xrLabel30
            // 
            this.xrLabel30.BackColor = System.Drawing.Color.WhiteSmoke;
            this.xrLabel30.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "ComposicaoTotal")});
            this.xrLabel30.Dpi = 254F;
            this.xrLabel30.Font = new System.Drawing.Font("Tahoma", 6.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel30.LocationFloat = new DevExpress.Utils.PointFloat(656F, 38F);
            this.xrLabel30.Multiline = true;
            this.xrLabel30.Name = "xrLabel30";
            this.xrLabel30.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrLabel30.SizeF = new System.Drawing.SizeF(126F, 91F);
            this.xrLabel30.Text = "Lista12";
            this.xrLabel30.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight;
            this.xrLabel30.WordWrap = false;
            // 
            // xrLabel37
            // 
            this.xrLabel37.Dpi = 254F;
            this.xrLabel37.Font = new System.Drawing.Font("Tahoma", 6.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel37.LocationFloat = new DevExpress.Utils.PointFloat(508F, 101F);
            this.xrLabel37.Name = "xrLabel37";
            this.xrLabel37.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrLabel37.SizeF = new System.Drawing.SizeF(138F, 26F);
            this.xrLabel37.Text = "TOTAL";
            this.xrLabel37.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight;
            // 
            // QuadroTopoAut
            // 
            this.QuadroTopoAut.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrPanel6,
            this.xrPictureBox2,
            this.xrPanel4});
            this.QuadroTopoAut.Dpi = 254F;
            this.QuadroTopoAut.LocationFloat = new DevExpress.Utils.PointFloat(0F, 14F);
            this.QuadroTopoAut.Name = "QuadroTopoAut";
            this.QuadroTopoAut.SizeF = new System.Drawing.SizeF(1799F, 148F);
            // 
            // xrPanel6
            // 
            this.xrPanel6.BorderColor = System.Drawing.Color.Silver;
            this.xrPanel6.CanGrow = false;
            this.xrPanel6.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrLabel16,
            this.xrLabel17,
            this.xrLabel18,
            this.xrLabel19,
            this.xrLabel20,
            this.xrLabel21,
            this.xrLabel22,
            this.xrLabel23,
            this.xrLabel26,
            this.xrLabel27,
            this.xrLabel28});
            this.xrPanel6.Dpi = 254F;
            this.xrPanel6.LocationFloat = new DevExpress.Utils.PointFloat(313F, 0F);
            this.xrPanel6.Name = "xrPanel6";
            this.xrPanel6.SizeF = new System.Drawing.SizeF(569F, 148F);
            // 
            // xrLabel16
            // 
            this.xrLabel16.CanGrow = false;
            this.xrLabel16.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "Calc_AgCedente")});
            this.xrLabel16.Dpi = 254F;
            this.xrLabel16.Font = new System.Drawing.Font("Tahoma", 6.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel16.LocationFloat = new DevExpress.Utils.PointFloat(143F, 120F);
            this.xrLabel16.Name = "xrLabel16";
            this.xrLabel16.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrLabel16.SizeF = new System.Drawing.SizeF(390F, 28F);
            this.xrLabel16.Text = "xrLabel48";
            // 
            // xrLabel17
            // 
            this.xrLabel17.CanGrow = false;
            this.xrLabel17.Dpi = 254F;
            this.xrLabel17.Font = new System.Drawing.Font("Tahoma", 5.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel17.LocationFloat = new DevExpress.Utils.PointFloat(0F, 121F);
            this.xrLabel17.Multiline = true;
            this.xrLabel17.Name = "xrLabel17";
            this.xrLabel17.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrLabel17.SizeF = new System.Drawing.SizeF(132F, 27F);
            this.xrLabel17.Text = "Ag/Cedente:";
            // 
            // xrLabel18
            // 
            this.xrLabel18.CanGrow = false;
            this.xrLabel18.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "NNStr2")});
            this.xrLabel18.Dpi = 254F;
            this.xrLabel18.Font = new System.Drawing.Font("Tahoma", 6.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel18.LocationFloat = new DevExpress.Utils.PointFloat(197F, 92F);
            this.xrLabel18.Name = "xrLabel18";
            this.xrLabel18.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrLabel18.SizeF = new System.Drawing.SizeF(336F, 28F);
            this.xrLabel18.Text = "NNSTR";
            // 
            // xrLabel19
            // 
            this.xrLabel19.CanGrow = false;
            this.xrLabel19.Dpi = 254F;
            this.xrLabel19.Font = new System.Drawing.Font("Tahoma", 5.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel19.LocationFloat = new DevExpress.Utils.PointFloat(0F, 93F);
            this.xrLabel19.Name = "xrLabel19";
            this.xrLabel19.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrLabel19.SizeF = new System.Drawing.SizeF(159F, 26F);
            this.xrLabel19.Text = "Nosso N�mero:";
            // 
            // xrLabel20
            // 
            this.xrLabel20.CanGrow = false;
            this.xrLabel20.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "BOLNome")});
            this.xrLabel20.Dpi = 254F;
            this.xrLabel20.Font = new System.Drawing.Font("Tahoma", 6.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel20.LocationFloat = new DevExpress.Utils.PointFloat(74F, 64F);
            this.xrLabel20.Name = "xrLabel20";
            this.xrLabel20.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrLabel20.SizeF = new System.Drawing.SizeF(495F, 28F);
            this.xrLabel20.Text = "xrLabel44";
            // 
            // xrLabel21
            // 
            this.xrLabel21.CanGrow = false;
            this.xrLabel21.Dpi = 254F;
            this.xrLabel21.Font = new System.Drawing.Font("Tahoma", 5.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel21.LocationFloat = new DevExpress.Utils.PointFloat(0F, 65F);
            this.xrLabel21.Name = "xrLabel21";
            this.xrLabel21.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrLabel21.SizeF = new System.Drawing.SizeF(72F, 22F);
            this.xrLabel21.Text = "Nome:";
            this.xrLabel21.WordWrap = false;
            // 
            // xrLabel22
            // 
            this.xrLabel22.CanGrow = false;
            this.xrLabel22.Dpi = 254F;
            this.xrLabel22.Font = new System.Drawing.Font("Tahoma", 5.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel22.LocationFloat = new DevExpress.Utils.PointFloat(188F, 37F);
            this.xrLabel22.Name = "xrLabel22";
            this.xrLabel22.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrLabel22.SizeF = new System.Drawing.SizeF(79F, 26F);
            this.xrLabel22.Text = "Apto:";
            // 
            // xrLabel23
            // 
            this.xrLabel23.CanGrow = false;
            this.xrLabel23.Dpi = 254F;
            this.xrLabel23.Font = new System.Drawing.Font("Tahoma", 5.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel23.LocationFloat = new DevExpress.Utils.PointFloat(0F, 37F);
            this.xrLabel23.Name = "xrLabel23";
            this.xrLabel23.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrLabel23.SizeF = new System.Drawing.SizeF(74F, 27F);
            this.xrLabel23.Text = "Bloco:";
            // 
            // xrLabel26
            // 
            this.xrLabel26.CanGrow = false;
            this.xrLabel26.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "APTNumero")});
            this.xrLabel26.Dpi = 254F;
            this.xrLabel26.Font = new System.Drawing.Font("Tahoma", 6.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel26.LocationFloat = new DevExpress.Utils.PointFloat(275F, 36F);
            this.xrLabel26.Name = "xrLabel26";
            this.xrLabel26.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrLabel26.SizeF = new System.Drawing.SizeF(135F, 28F);
            this.xrLabel26.Text = "LabApt4";
            // 
            // xrLabel27
            // 
            this.xrLabel27.CanGrow = false;
            this.xrLabel27.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "BLOCodigo")});
            this.xrLabel27.Dpi = 254F;
            this.xrLabel27.Font = new System.Drawing.Font("Tahoma", 6.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel27.LocationFloat = new DevExpress.Utils.PointFloat(85F, 36F);
            this.xrLabel27.Name = "xrLabel27";
            this.xrLabel27.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrLabel27.SizeF = new System.Drawing.SizeF(82F, 28F);
            this.xrLabel27.Text = "LabBlo4";
            // 
            // xrLabel28
            // 
            this.xrLabel28.CanGrow = false;
            this.xrLabel28.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "CONNome")});
            this.xrLabel28.Dpi = 254F;
            this.xrLabel28.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel28.LocationFloat = new DevExpress.Utils.PointFloat(0F, 0F);
            this.xrLabel28.Name = "xrLabel28";
            this.xrLabel28.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrLabel28.SizeF = new System.Drawing.SizeF(569F, 36F);
            this.xrLabel28.StylePriority.UseTextAlignment = false;
            this.xrLabel28.Text = "xrLabel1";
            this.xrLabel28.WordWrap = false;
            // 
            // xrPictureBox2
            // 
            this.xrPictureBox2.Dpi = 254F;
            this.xrPictureBox2.Image = ((System.Drawing.Image)(resources.GetObject("xrPictureBox2.Image")));
            this.xrPictureBox2.LocationFloat = new DevExpress.Utils.PointFloat(0F, 0F);
            this.xrPictureBox2.Name = "xrPictureBox2";
            this.xrPictureBox2.SizeF = new System.Drawing.SizeF(313F, 148F);
            this.xrPictureBox2.Sizing = DevExpress.XtraPrinting.ImageSizeMode.ZoomImage;
            // 
            // xrPanel4
            // 
            this.xrPanel4.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrLabel2Duvida1,
            this.xrLabel3Duvida1,
            this.xrLabel12,
            this.xrLabel13,
            this.xrLabel14,
            this.xrLabel15});
            this.xrPanel4.Dpi = 254F;
            this.xrPanel4.LocationFloat = new DevExpress.Utils.PointFloat(882F, 0F);
            this.xrPanel4.Name = "xrPanel4";
            this.xrPanel4.SizeF = new System.Drawing.SizeF(917F, 148F);
            // 
            // xrLabel2Duvida1
            // 
            this.xrLabel2Duvida1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(219)))), ((int)(((byte)(219)))), ((int)(((byte)(219)))));
            this.xrLabel2Duvida1.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "BOLVencto", "{0:dd/MM/yy}")});
            this.xrLabel2Duvida1.Dpi = 254F;
            this.xrLabel2Duvida1.Font = new System.Drawing.Font("Times New Roman", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel2Duvida1.LocationFloat = new DevExpress.Utils.PointFloat(616F, 57F);
            this.xrLabel2Duvida1.Name = "xrLabel2Duvida1";
            this.xrLabel2Duvida1.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrLabel2Duvida1.SizeF = new System.Drawing.SizeF(294F, 90F);
            this.xrLabel2Duvida1.Text = "xrLabel6";
            this.xrLabel2Duvida1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomCenter;
            // 
            // xrLabel3Duvida1
            // 
            this.xrLabel3Duvida1.BackColor = System.Drawing.Color.DarkGray;
            this.xrLabel3Duvida1.Dpi = 254F;
            this.xrLabel3Duvida1.Font = new System.Drawing.Font("Verdana", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel3Duvida1.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.xrLabel3Duvida1.LocationFloat = new DevExpress.Utils.PointFloat(616F, 0F);
            this.xrLabel3Duvida1.Name = "xrLabel3Duvida1";
            this.xrLabel3Duvida1.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrLabel3Duvida1.SizeF = new System.Drawing.SizeF(294F, 53F);
            this.xrLabel3Duvida1.Text = "VENCIMENTO";
            this.xrLabel3Duvida1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomCenter;
            this.xrLabel3Duvida1.Draw += new DevExpress.XtraReports.UI.DrawEventHandler(this.CantoRedondo);
            // 
            // xrLabel12
            // 
            this.xrLabel12.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(219)))), ((int)(((byte)(219)))), ((int)(((byte)(219)))));
            this.xrLabel12.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "BOLValorPrevisto", "{0:#,##0.00}")});
            this.xrLabel12.Dpi = 254F;
            this.xrLabel12.Font = new System.Drawing.Font("Times New Roman", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel12.LocationFloat = new DevExpress.Utils.PointFloat(322F, 57F);
            this.xrLabel12.Name = "xrLabel12";
            this.xrLabel12.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrLabel12.SizeF = new System.Drawing.SizeF(289F, 90F);
            this.xrLabel12.Text = "xrLabel9";
            this.xrLabel12.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomCenter;
            // 
            // xrLabel13
            // 
            this.xrLabel13.BackColor = System.Drawing.Color.DarkGray;
            this.xrLabel13.Dpi = 254F;
            this.xrLabel13.Font = new System.Drawing.Font("Verdana", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel13.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.xrLabel13.LocationFloat = new DevExpress.Utils.PointFloat(322F, 0F);
            this.xrLabel13.Name = "xrLabel13";
            this.xrLabel13.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrLabel13.SizeF = new System.Drawing.SizeF(289F, 53F);
            this.xrLabel13.Text = "VALOR";
            this.xrLabel13.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomCenter;
            this.xrLabel13.Draw += new DevExpress.XtraReports.UI.DrawEventHandler(this.CantoRedondo);
            // 
            // xrLabel14
            // 
            this.xrLabel14.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(219)))), ((int)(((byte)(219)))), ((int)(((byte)(219)))));
            this.xrLabel14.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "Competencia")});
            this.xrLabel14.Dpi = 254F;
            this.xrLabel14.Font = new System.Drawing.Font("Times New Roman", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel14.LocationFloat = new DevExpress.Utils.PointFloat(7F, 57F);
            this.xrLabel14.Name = "xrLabel14";
            this.xrLabel14.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrLabel14.SizeF = new System.Drawing.SizeF(307F, 90F);
            this.xrLabel14.Text = "xrCompetencia";
            this.xrLabel14.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomCenter;
            // 
            // xrLabel15
            // 
            this.xrLabel15.BackColor = System.Drawing.Color.DarkGray;
            this.xrLabel15.Dpi = 254F;
            this.xrLabel15.Font = new System.Drawing.Font("Verdana", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel15.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.xrLabel15.LocationFloat = new DevExpress.Utils.PointFloat(7F, 0F);
            this.xrLabel15.Name = "xrLabel15";
            this.xrLabel15.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrLabel15.SizeF = new System.Drawing.SizeF(307F, 53F);
            this.xrLabel15.Text = "COMPET�NCIA";
            this.xrLabel15.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomCenter;
            this.xrLabel15.Draw += new DevExpress.XtraReports.UI.DrawEventHandler(this.CantoRedondo);
            // 
            // QuadroCheques
            // 
            this.QuadroCheques.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.LabelCheques,
            this.xrLabel60,
            this.xrLabel58,
            this.xrLabel56,
            this.xrLabel54,
            this.xrLabel52,
            this.xrLabel51,
            this.xrLabel50,
            this.xrLabel49,
            this.xrLabel41,
            this.xrLabel40});
            this.QuadroCheques.Dpi = 254F;
            this.QuadroCheques.LocationFloat = new DevExpress.Utils.PointFloat(0F, 170F);
            this.QuadroCheques.Name = "QuadroCheques";
            this.QuadroCheques.SizeF = new System.Drawing.SizeF(1799F, 161F);
            // 
            // LabelCheques
            // 
            this.LabelCheques.Dpi = 254F;
            this.LabelCheques.Font = new System.Drawing.Font("Times New Roman", 20.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LabelCheques.ForeColor = System.Drawing.Color.DarkGray;
            this.LabelCheques.LocationFloat = new DevExpress.Utils.PointFloat(0F, 48F);
            this.LabelCheques.Name = "LabelCheques";
            this.LabelCheques.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.LabelCheques.SizeF = new System.Drawing.SizeF(312F, 103F);
            this.LabelCheques.Text = "Cheques:";
            this.LabelCheques.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter;
            // 
            // xrLabel60
            // 
            this.xrLabel60.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(219)))), ((int)(((byte)(219)))), ((int)(((byte)(219)))));
            this.xrLabel60.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "A1", "{0:dd/MM/yy}")});
            this.xrLabel60.Dpi = 254F;
            this.xrLabel60.Font = new System.Drawing.Font("Times New Roman", 15F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel60.LocationFloat = new DevExpress.Utils.PointFloat(318F, 78F);
            this.xrLabel60.Multiline = true;
            this.xrLabel60.Name = "xrLabel60";
            this.xrLabel60.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrLabel60.SizeF = new System.Drawing.SizeF(169F, 75F);
            this.xrLabel60.StylePriority.UseFont = false;
            this.xrLabel60.Text = "xrLabel6";
            this.xrLabel60.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomCenter;
            // 
            // xrLabel58
            // 
            this.xrLabel58.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(219)))), ((int)(((byte)(219)))), ((int)(((byte)(219)))));
            this.xrLabel58.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "A2", "{0:dd/MM/yy}")});
            this.xrLabel58.Dpi = 254F;
            this.xrLabel58.Font = new System.Drawing.Font("Times New Roman", 15F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel58.LocationFloat = new DevExpress.Utils.PointFloat(508F, 78F);
            this.xrLabel58.Multiline = true;
            this.xrLabel58.Name = "xrLabel58";
            this.xrLabel58.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrLabel58.SizeF = new System.Drawing.SizeF(254F, 75F);
            this.xrLabel58.StylePriority.UseFont = false;
            this.xrLabel58.Text = "xrLabel6";
            this.xrLabel58.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomCenter;
            // 
            // xrLabel56
            // 
            this.xrLabel56.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(219)))), ((int)(((byte)(219)))), ((int)(((byte)(219)))));
            this.xrLabel56.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "A3", "{0:dd/MM/yy}")});
            this.xrLabel56.Dpi = 254F;
            this.xrLabel56.Font = new System.Drawing.Font("Times New Roman", 15F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel56.LocationFloat = new DevExpress.Utils.PointFloat(783F, 78F);
            this.xrLabel56.Multiline = true;
            this.xrLabel56.Name = "xrLabel56";
            this.xrLabel56.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrLabel56.SizeF = new System.Drawing.SizeF(339F, 75F);
            this.xrLabel56.StylePriority.UseFont = false;
            this.xrLabel56.Text = "xrLabel6";
            this.xrLabel56.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomCenter;
            // 
            // xrLabel54
            // 
            this.xrLabel54.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(219)))), ((int)(((byte)(219)))), ((int)(((byte)(219)))));
            this.xrLabel54.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "A4", "{0:dd/MM/yy}")});
            this.xrLabel54.Dpi = 254F;
            this.xrLabel54.Font = new System.Drawing.Font("Times New Roman", 15F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel54.LocationFloat = new DevExpress.Utils.PointFloat(1143F, 78F);
            this.xrLabel54.Multiline = true;
            this.xrLabel54.Name = "xrLabel54";
            this.xrLabel54.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrLabel54.SizeF = new System.Drawing.SizeF(339F, 75F);
            this.xrLabel54.StylePriority.UseFont = false;
            this.xrLabel54.Text = "xrLabel6";
            this.xrLabel54.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomCenter;
            // 
            // xrLabel52
            // 
            this.xrLabel52.BackColor = System.Drawing.Color.DarkGray;
            this.xrLabel52.Dpi = 254F;
            this.xrLabel52.Font = new System.Drawing.Font("Verdana", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel52.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.xrLabel52.LocationFloat = new DevExpress.Utils.PointFloat(318F, 21F);
            this.xrLabel52.Name = "xrLabel52";
            this.xrLabel52.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrLabel52.SizeF = new System.Drawing.SizeF(169F, 53F);
            this.xrLabel52.Text = "BANCO";
            this.xrLabel52.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomCenter;
            this.xrLabel52.Draw += new DevExpress.XtraReports.UI.DrawEventHandler(this.CantoRedondo);
            // 
            // xrLabel51
            // 
            this.xrLabel51.BackColor = System.Drawing.Color.DarkGray;
            this.xrLabel51.Dpi = 254F;
            this.xrLabel51.Font = new System.Drawing.Font("Verdana", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel51.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.xrLabel51.LocationFloat = new DevExpress.Utils.PointFloat(508F, 21F);
            this.xrLabel51.Name = "xrLabel51";
            this.xrLabel51.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrLabel51.SizeF = new System.Drawing.SizeF(254F, 53F);
            this.xrLabel51.Text = "AG�NCIA";
            this.xrLabel51.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomCenter;
            this.xrLabel51.Draw += new DevExpress.XtraReports.UI.DrawEventHandler(this.CantoRedondo);
            // 
            // xrLabel50
            // 
            this.xrLabel50.BackColor = System.Drawing.Color.DarkGray;
            this.xrLabel50.Dpi = 254F;
            this.xrLabel50.Font = new System.Drawing.Font("Verdana", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel50.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.xrLabel50.LocationFloat = new DevExpress.Utils.PointFloat(783F, 21F);
            this.xrLabel50.Name = "xrLabel50";
            this.xrLabel50.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrLabel50.SizeF = new System.Drawing.SizeF(339F, 53F);
            this.xrLabel50.Text = "CONTA";
            this.xrLabel50.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomCenter;
            this.xrLabel50.Draw += new DevExpress.XtraReports.UI.DrawEventHandler(this.CantoRedondo);
            // 
            // xrLabel49
            // 
            this.xrLabel49.BackColor = System.Drawing.Color.DarkGray;
            this.xrLabel49.Dpi = 254F;
            this.xrLabel49.Font = new System.Drawing.Font("Verdana", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel49.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.xrLabel49.LocationFloat = new DevExpress.Utils.PointFloat(1143F, 21F);
            this.xrLabel49.Name = "xrLabel49";
            this.xrLabel49.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrLabel49.SizeF = new System.Drawing.SizeF(339F, 53F);
            this.xrLabel49.Text = "N�MERO";
            this.xrLabel49.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomCenter;
            this.xrLabel49.Draw += new DevExpress.XtraReports.UI.DrawEventHandler(this.CantoRedondo);
            // 
            // xrLabel41
            // 
            this.xrLabel41.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(219)))), ((int)(((byte)(219)))), ((int)(((byte)(219)))));
            this.xrLabel41.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "A5", "{0:dd/MM/yy}")});
            this.xrLabel41.Dpi = 254F;
            this.xrLabel41.Font = new System.Drawing.Font("Times New Roman", 15F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel41.LocationFloat = new DevExpress.Utils.PointFloat(1503F, 78F);
            this.xrLabel41.Multiline = true;
            this.xrLabel41.Name = "xrLabel41";
            this.xrLabel41.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrLabel41.SizeF = new System.Drawing.SizeF(286F, 75F);
            this.xrLabel41.StylePriority.UseFont = false;
            this.xrLabel41.Text = "xrLabel6";
            this.xrLabel41.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomCenter;
            // 
            // xrLabel40
            // 
            this.xrLabel40.BackColor = System.Drawing.Color.DarkGray;
            this.xrLabel40.Dpi = 254F;
            this.xrLabel40.Font = new System.Drawing.Font("Verdana", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel40.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.xrLabel40.LocationFloat = new DevExpress.Utils.PointFloat(1503F, 21F);
            this.xrLabel40.Name = "xrLabel40";
            this.xrLabel40.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrLabel40.SizeF = new System.Drawing.SizeF(286F, 53F);
            this.xrLabel40.Text = "VALOR";
            this.xrLabel40.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomCenter;
            this.xrLabel40.Draw += new DevExpress.XtraReports.UI.DrawEventHandler(this.CantoRedondo);
            // 
            // xrDataPag
            // 
            this.xrDataPag.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(219)))), ((int)(((byte)(219)))), ((int)(((byte)(219)))));
            this.xrDataPag.CanGrow = false;
            this.xrDataPag.Dpi = 254F;
            this.xrDataPag.Font = new System.Drawing.Font("Times New Roman", 17F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrDataPag.LocationFloat = new DevExpress.Utils.PointFloat(1192F, 83F);
            this.xrDataPag.Multiline = true;
            this.xrDataPag.Name = "xrDataPag";
            this.xrDataPag.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrDataPag.SizeF = new System.Drawing.SizeF(294F, 65F);
            this.xrDataPag.StylePriority.UseFont = false;
            this.xrDataPag.StylePriority.UseTextAlignment = false;
            this.xrDataPag.Text = "00/00/00";
            this.xrDataPag.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter;
            // 
            // ImpBoletoReciboSV
            // 
            this.Bands.AddRange(new DevExpress.XtraReports.UI.Band[] {
            this.Detail,
            this.bottomMarginBand1});
            this.ComRemetente = false;
            this.FormattingRuleSheet.AddRange(new DevExpress.XtraReports.UI.FormattingRule[] {
            this.RuleOcultaUsu});
            this.Version = "16.1";
            ((System.ComponentModel.ISupportInitialize)(this.QuadroMensagem)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dImpBoletoBase1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dImpDestRem1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.bindingSourcePrincipal)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.bindingSource1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();

        }

        #endregion

        private DevExpress.XtraReports.UI.XRPanel QuadroAutent;
        private DevExpress.XtraReports.UI.XRPanel QuadroTopoAut;
        private DevExpress.XtraReports.UI.XRPanel xrPanel4;
        private DevExpress.XtraReports.UI.XRLabel xrLabel2Duvida1;
        private DevExpress.XtraReports.UI.XRLabel xrLabel3Duvida1;
        private DevExpress.XtraReports.UI.XRLabel xrLabel12;
        private DevExpress.XtraReports.UI.XRLabel xrLabel13;
        private DevExpress.XtraReports.UI.XRLabel xrLabel14;
        /// <summary>
        /// 
        /// </summary>
        protected DevExpress.XtraReports.UI.XRLabel xrLabel15;
        /// <summary>
        /// 
        /// </summary>
        protected DevExpress.XtraReports.UI.XRPanel xrPanel2;
        private DevExpress.XtraReports.UI.XRLabel xrLabel31;
        private DevExpress.XtraReports.UI.XRLabel xrLabel29;
        private DevExpress.XtraReports.UI.XRLabel xrLabel30;
        private DevExpress.XtraReports.UI.XRLabel xrLabel37;
        /// <summary>
        /// 
        /// </summary>
        protected DevExpress.XtraReports.UI.XRPanel xrPanel9;
        private DevExpress.XtraReports.UI.XRLabel xrLabel38;
        private DevExpress.XtraReports.UI.XRLabel xrLabel39;
        private DevExpress.XtraReports.UI.XRLabel xrLabel52;
        private DevExpress.XtraReports.UI.XRLabel xrLabel51;
        private DevExpress.XtraReports.UI.XRLabel xrLabel50;
        private DevExpress.XtraReports.UI.XRLabel xrLabel49;
        private DevExpress.XtraReports.UI.XRLabel xrLabel41;
        private DevExpress.XtraReports.UI.XRLabel xrLabel40;
        private DevExpress.XtraReports.UI.XRLabel xrLabel60;
        private DevExpress.XtraReports.UI.XRLabel xrLabel58;
        private DevExpress.XtraReports.UI.XRLabel xrLabel56;
        private DevExpress.XtraReports.UI.XRLabel xrLabel54;
        private DevExpress.XtraReports.UI.XRLine xrLine11;
        /// <summary>
        /// 
        /// </summary>
        public DevExpress.XtraReports.UI.XRLabel LabelCheques;
        /// <summary>
        /// 
        /// </summary>
        public DevExpress.XtraReports.UI.XRPanel QuadroCheques;
        private DevExpress.XtraReports.UI.XRPictureBox xrPictureBox2;
        private DevExpress.XtraReports.UI.XRPanel xrPanel6;
        private DevExpress.XtraReports.UI.XRLabel xrLabel16;
        private DevExpress.XtraReports.UI.XRLabel xrLabel17;
        private DevExpress.XtraReports.UI.XRLabel xrLabel18;
        private DevExpress.XtraReports.UI.XRLabel xrLabel19;
        private DevExpress.XtraReports.UI.XRLabel xrLabel20;
        private DevExpress.XtraReports.UI.XRLabel xrLabel21;
        private DevExpress.XtraReports.UI.XRLabel xrLabel22;
        private DevExpress.XtraReports.UI.XRLabel xrLabel23;
        private DevExpress.XtraReports.UI.XRLabel xrLabel26;
        private DevExpress.XtraReports.UI.XRLabel xrLabel27;
        private DevExpress.XtraReports.UI.XRLabel xrLabel28;
        /// <summary>
        /// 
        /// </summary>
        public DevExpress.XtraReports.UI.XRPanel QuadroCheques2;
        private DevExpress.XtraReports.UI.XRLabel xrLabel66;
        private DevExpress.XtraReports.UI.XRLabel xrLabel72;
        private DevExpress.XtraReports.UI.XRLabel xrLabel76;
        private DevExpress.XtraReports.UI.XRLabel xrLabel83;
        private DevExpress.XtraReports.UI.XRLabel xrLabel105;
        private DevExpress.XtraReports.UI.XRLabel xrLabel114;
        private DevExpress.XtraReports.UI.XRLabel xrLabel121;
        private DevExpress.XtraReports.UI.XRLabel xrLabel125;
        private DevExpress.XtraReports.UI.XRLabel xrLabel126;
        private DevExpress.XtraReports.UI.XRLabel xrLabel133;
        private DevExpress.XtraReports.UI.XRLabel xrDataPag;
    }
}