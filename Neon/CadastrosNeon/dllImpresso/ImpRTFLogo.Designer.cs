namespace dllImpresso
{
    partial class ImpRTFLogo
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(ImpRTFLogo));
            this.xrPictureBox1 = new DevExpress.XtraReports.UI.XRPictureBox();
            ((System.ComponentModel.ISupportInitialize)(this.QuadroRTF)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.QuadroRTFSegundaPagina)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dImpDestRem1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.bindingSourcePrincipal)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            // 
            // QuadroRTF
            // 
            this.QuadroRTF.LocationFloat = new DevExpress.Utils.PointFloat(0F, 165F);
            this.QuadroRTF.SizeF = new System.Drawing.SizeF(1799F, 2700F);
            // 
            // QuadroPrincipal
            // 
            this.QuadroPrincipal.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrPictureBox1});
            this.QuadroPrincipal.Controls.SetChildIndex(this.xrPictureBox1, 0);
            this.QuadroPrincipal.Controls.SetChildIndex(this.QuadroRTF, 0);
            // 
            // QuadroSegundaPagina
            // 
            this.QuadroSegundaPagina.StylePriority.UseBackColor = false;
            // 
            // Detail
            // 
            this.Detail.StylePriority.UseTextAlignment = false;
            // 
            // xrSigla1
            // 
            this.xrSigla1.StylePriority.UseFont = false;
            this.xrSigla1.StylePriority.UseTextAlignment = false;
            // 
            // xrPictureBox1
            // 
            this.xrPictureBox1.Dpi = 254F;
            this.xrPictureBox1.Image = ((System.Drawing.Image)(resources.GetObject("xrPictureBox1.Image")));
            this.xrPictureBox1.LocationFloat = new DevExpress.Utils.PointFloat(0F, 0F);
            this.xrPictureBox1.Name = "xrPictureBox1";
            this.xrPictureBox1.SizeF = new System.Drawing.SizeF(339F, 160F);
            this.xrPictureBox1.Sizing = DevExpress.XtraPrinting.ImageSizeMode.ZoomImage;
            // 
            // ImpRTFLogo
            // 
            this.Bands.AddRange(new DevExpress.XtraReports.UI.Band[] {
            this.Detail,
            this.bottomMarginBand1});
            this.Version = "12.2";
            ((System.ComponentModel.ISupportInitialize)(this.QuadroRTF)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.QuadroRTFSegundaPagina)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dImpDestRem1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.bindingSourcePrincipal)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();

        }

        #endregion

        private DevExpress.XtraReports.UI.XRPictureBox xrPictureBox1;
    }
}