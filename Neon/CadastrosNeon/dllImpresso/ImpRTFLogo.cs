using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using DevExpress.XtraReports.UI;

namespace dllImpresso
{
    /// <summary>
    /// 
    /// </summary>
    public partial class ImpRTFLogo : dllImpresso.ImpRTF
    {
        /// <summary>
        /// 
        /// </summary>
        public ImpRTFLogo()
        {
            InitializeComponent();
        }

        private static ImpRTFLogo _ImpRTFLogoSt;

        /// <summary>
        /// 
        /// </summary>
        public static ImpRTFLogo ImpRTFLogoSt
        {
            get
            {
                if (_ImpRTFLogoSt == null)
                    _ImpRTFLogoSt = new ImpRTFLogo();
                return _ImpRTFLogoSt;
            }
        }
    }
}

