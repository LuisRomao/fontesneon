using System;
using System.Drawing;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using DevExpress.XtraReports.UI;

namespace dllImpresso
{
    /// <summary>
    /// 
    /// </summary>
    public partial class ImpLogoCond : ImpLogoBase
    {
        /// <summary>
        /// 
        /// </summary>
        public ImpLogoCond()
        {
            InitializeComponent();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="Nome"></param>
        public void SetNome(string Nome)
        {         
            xrNomeCond.Text = Nome;
        }

        /// <summary>
        /// 
        /// </summary>
        public override void RemoverLogoeTitulo()
        {
            base.RemoverLogoeTitulo();
            xrLabel1.Visible = xrNomeCond.Visible = false;
        }
    }

 
}

