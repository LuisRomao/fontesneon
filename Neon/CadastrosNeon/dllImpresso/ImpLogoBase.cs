using DevExpress.XtraReports.UI;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;

namespace dllImpresso
{
    /// <summary>
    /// 
    /// </summary>
    public partial class ImpLogoBase : ImpBase
    {

        private ApontadorImp Apontador;

        /// <summary>
        /// Construtor
        /// </summary>
        public ImpLogoBase()
        {
            InitializeComponent();
            Apontador = new ApontadorImp();
            if (!this.DesignMode)
                try
                {
                    string Sigla = CompontesBasicos.Bancovirtual.UsuarioLogado.UsuarioLogadoSt.USU.ToString();
                    char Anterior = ' ';
                    Array.ForEach(CompontesBasicos.Bancovirtual.UsuarioLogado.UsuarioLogadoSt.USUNome.ToCharArray(), delegate (char c)
                    {
                        if (char.IsWhiteSpace(Anterior))
                            Sigla += c.ToString().ToUpper();
                        Anterior = c;
                    });

                    xrSigla2.Text = Sigla;
                }
                catch { }
        }

        #region Colunas din�micas
        private const int W30 = 17;
        private int larguraColunas;
        private Font fonte;
        private Font fonteTotal;


        #region MyRegion Propriedades
        private bool linhasVerticais = false;
        private bool usaDegrade = true;
        private XRLabel modeloBind;
        private XRLabel modeloDin;
        private XRLabel modeloDinTitulo;
        private XRLabel modeloDinTotal;
        private XRLabel modeloDinSubTotal;
        private XRControl[] moveDireita;
        private int[] nCorte;

        private List<XRLabel> NovosComponentes;

        
        /// <summary>
        /// Controles para mover a direita
        /// </summary>
        [Category("Virtual Software"), Description("Controles para mover a direita"), DisplayName("D MoverDireita")]        
        public XRControl[] MoveDireita { get => moveDireita ?? (moveDireita = new XRControl[0]); set => moveDireita = value; }

        /// <summary>
        /// Add Controles para mover a direita
        /// </summary>
        [Category("Virtual Software"), Description("Add Controle para mover a direita"), DisplayName("D MoverDireita ADD")]
        public XRControl MoveDireitaAdd
        {
            get => null;
            set
            {
                XRControl[] moveDireitaNovo = new XRControl[MoveDireita.Length + 1];
                for (int i = 0; i < MoveDireita.Length; i++)
                    if (MoveDireita[i] == value)
                        return;
                    else
                        moveDireitaNovo[i] = MoveDireita[i];
                moveDireitaNovo[moveDireitaNovo.Length - 1] = value;
                MoveDireita = moveDireitaNovo;
            }
        }

        /// <summary>
        /// Remove Controles para mover a direita
        /// </summary>
        [Category("Virtual Software"), Description("Remove Controle para mover a direita"), DisplayName("D MoverDireita DEL")]
        public XRControl MoveDireitaDel
        {
            get => null;
            set
            {
                XRControl[] moveDireitaNovo = new XRControl[MoveDireita.Length - 1];
                bool Feito = false;
                int k = 0;
                for (int i = 0; i < MoveDireita.Length; i++)
                    if (MoveDireita[i] == value)
                        Feito = true;
                    else
                        moveDireitaNovo[k++] = MoveDireita[i];
                if (Feito)
                    MoveDireita = moveDireitaNovo;
            }
        }

        /// <summary>
        /// D Modelo Bind
        /// </summary>
        [Category("Virtual Software"), Description("Modelo de campo bind"), DisplayName("D Modelo Bind")]
        public XRLabel ModeloBind { get => modeloBind; set => modeloBind = value; }

        /// <summary>
        /// Modelo a repetir
        /// </summary>
        [Category("Virtual Software"), Description("Modelo a repetir"), DisplayName("D Modelo 1 XRLabel")]        
        public XRLabel ModeloDin { get => modeloDin; set => modeloDin = value; }

        /// <summary>
        /// Modelo do t�tulo a repetir
        /// </summary>
        [Category("Virtual Software"), Description("Modelo do t�tulo a repetir"), DisplayName("D Modelo 2 T�tulo")]
        public XRLabel ModeloDinTitulo { get => modeloDinTitulo; set => modeloDinTitulo = value; }

        /// <summary>
        /// Modelo subtotal repetir
        /// </summary>
        [Category("Virtual Software"), Description("Modelo subtotal repetir"), DisplayName("D Modelo 3 Sub Total")]
        public XRLabel ModeloDinSubTotal { get => modeloDinSubTotal; set => modeloDinSubTotal = value; }

        /// <summary>
        /// Modelo total repetir
        /// </summary>
        [Category("Virtual Software"), Description("Modelo total repetir"), DisplayName("D Modelo 4 Total")]
        public XRLabel ModeloDinTotal { get => modeloDinTotal; set => modeloDinTotal = value; }

        /// <summary>
        /// Usar ou n�o linhas entre as colunas din�micas
        /// </summary>
        [Category("Virtual Software"), Description("Usar ou n�o linhas entre as colunas din�micas"), DisplayName("D Linhas entre colunas")]
        public bool LinhasVerticais { get => linhasVerticais; set => linhasVerticais = value; }

        /// <summary>
        /// Usar degrade nas colunas din�micas
        /// </summary>
        [Category("Virtual Software"), Description("Usar degrade nas colunas din�micas"), DisplayName("D Degrade colunas")]
        public bool UsaDegrade { get => usaDegrade; set => usaDegrade = value; }

        /// <summary>
        /// N�meros de colunas corte
        /// </summary>
        [Category("Virtual Software"), Description("N�meros de colunas corte"), DisplayName("D Faixas corte")]
        public int[] NCorte { get => nCorte ?? (nCorte = new int[] {6,12,13,15,16,19}); set => nCorte = value; }
        #endregion

        private void LimpaColunasDinamicas()
        {
            if (NovosComponentes == null)
                NovosComponentes = new List<XRLabel>();
            else
            {
                foreach (XRLabel Apagar in NovosComponentes)
                    Apagar.Parent.Controls.Remove(Apagar);
                NovosComponentes.Clear();
            }
        }

        private enum NivelRed
        {
            natural,
            N0,
            N1,
            N2,
            N3,
            N4,
            N5
        }

        private void RedimImpresso(int ColunasInc)
        {
            if (ColunasInc > nCorte[5])
                RedimImpresso(NivelRed.N5);
            else if (ColunasInc > nCorte[4])
                RedimImpresso(NivelRed.N4);
            else if (ColunasInc > nCorte[3])
                RedimImpresso(NivelRed.N3);
            else if (ColunasInc > nCorte[2])
                RedimImpresso(NivelRed.N2);
            else if (ColunasInc > nCorte[1])
                RedimImpresso(NivelRed.N1);
            else if (ColunasInc > nCorte[0])
                RedimImpresso(NivelRed.N0);
        }

        private void RedimImpresso(NivelRed Nivel)
        {
            if (Nivel != NivelRed.natural)
            {
                this.Landscape = true;
                if (Nivel != NivelRed.N0)
                    if (ModeloDinTitulo != null)
                    {
                        ModeloDinTitulo.Angle = 30;
                        ModeloDinTitulo.Size = new Size(ModeloDinTitulo.Width, ModeloDinTitulo.Height + W30);
                        ModeloDinTitulo.Band.Height += W30;
                        Font f = ModeloDinTitulo.Font;
                        ModeloDinTitulo.Font = new Font(f.FontFamily, f.Size - 2, FontStyle.Bold);
                    }
            }
            switch (Nivel)
            {
                case NivelRed.N1:
                    larguraColunas = larguraColunas - 15;
                    fonte = new Font(fonte.FontFamily, fonte.Size - 2, FontStyle.Bold);
                    fonteTotal = new Font(fonteTotal.FontFamily, fonteTotal.Size - 2, FontStyle.Bold);
                    break;
                case NivelRed.N2:
                    larguraColunas = larguraColunas - 30;
                    fonte = new Font(fonte.FontFamily, fonte.Size - 3, FontStyle.Bold);
                    fonteTotal = new Font(fonteTotal.FontFamily, fonteTotal.Size - 3, FontStyle.Bold);
                    break;
                case NivelRed.N3:
                    larguraColunas = larguraColunas - 40;
                    fonte = new Font(fonte.FontFamily, fonte.Size - 3, FontStyle.Bold);
                    fonteTotal = new Font(fonteTotal.FontFamily, fonteTotal.Size - 3, FontStyle.Bold);
                    break;
                case NivelRed.N4:
                    larguraColunas = larguraColunas - 55;
                    fonte = new Font(fonte.FontFamily, fonte.Size - 3, FontStyle.Bold);
                    fonteTotal = new Font(fonteTotal.FontFamily, fonteTotal.Size - 3, FontStyle.Bold);
                    break;
                case NivelRed.N5:
                    larguraColunas = larguraColunas - 70;
                    fonte = new Font(fonte.FontFamily, fonte.Size - 3, FontStyle.Bold);
                    fonteTotal = new Font(fonteTotal.FontFamily, fonteTotal.Size - 3, FontStyle.Bold);
                    break;
            }            
        }

        private void AjustaBind(XRLabel XR,string Campo,string Mascara = "", string prop = "Text")
        {
            if (ModeloBind != null)
            {
                string TabelaMod = ModeloBind.DataBindings[prop].DataMember ?? "";
                if (TabelaMod.Contains("."))
                    TabelaMod = TabelaMod.Substring(0, TabelaMod.LastIndexOf('.') + 1);
                else
                    TabelaMod = "";
                XR.DataBindings.Add(prop, ModeloBind.DataBindings[prop].DataSource, TabelaMod + Campo, Mascara);
            }            
        }

        /// <summary>
        /// Crias as colunas din�micas
        /// </summary>
        /// <param name="ColunasDinamicas"></param>        
        public void CriaColunasDinamicas(SortedList<string,string> ColunasDinamicas)
        {            
            LimpaColunasDinamicas();            
            XRLabel clone = null;
            XRLabel cloneTitulo = null;
            XRLabel cloneTot = null;
            XRLabel cloneSubTot = null;            
            fonte = ModeloDin != null ? ModeloDin.Font : (ModeloDinTitulo != null ? ModeloDinTitulo.Font : null);
            fonteTotal = ModeloDinTotal != null ? ModeloDinTotal.Font : (ModeloDinSubTotal != null ? ModeloDinSubTotal.Font : null);
            larguraColunas = ModeloDin != null ? ModeloDin.Width : (ModeloDinTitulo != null ? ModeloDinTitulo.Width : 0);
            RedimImpresso(ColunasDinamicas.Count);
            int deltaAcumulado = 0;
            int MoverDireita = 0;
           
            foreach (string NovaColuna in ColunasDinamicas.Keys)
            {            
                string TituloColuna = NovaColuna;
                string TituloImpresso = ColunasDinamicas[NovaColuna];
                if (TituloImpresso == "")
                    TituloImpresso = Framework.datasets.dPLAnocontas.dPLAnocontasSt.PLAnocontas.FindByPLA(TituloColuna).PLADescricaoRes;
                Type TipoDado = typeof(decimal);
                string Mascara = "{0:n2}";
                if (TipoDado == typeof(DateTime))
                    Mascara = "{0:dd/MM}";
                if (deltaAcumulado != 0)
                {
                    if (ModeloDinTitulo != null)
                    {
                        cloneTitulo = new XRLabel();
                        NovosComponentes.Add(cloneTitulo);
                        ModeloDinTitulo.Band.Controls.Add(cloneTitulo);
                        cloneTitulo.TextAlignment = ModeloDinTitulo.TextAlignment;
                        cloneTitulo.Angle = ModeloDinTitulo.Angle;
                        cloneTitulo.Multiline = true;
                        cloneTitulo.WordWrap = true;
                        cloneTitulo.Font = ModeloDinTitulo.Font;
                    }
                    if (ModeloDin != null)
                    {
                        clone = new XRLabel();
                        NovosComponentes.Add(clone);
                        clone.TextAlignment = ModeloDin.TextAlignment;
                        clone.Size = new Size(0, 0);
                        clone.CanGrow = ModeloDin.CanGrow;
                        clone.WordWrap = ModeloDin.WordWrap;
                        Bands[BandKind.Detail].Controls.Add(clone);
                    }
                    if ((ModeloDinTotal != null) && (ModeloDinTotal.Band is GroupFooterBand))
                    {
                        GroupFooterBand RodapeTotal = (GroupFooterBand)ModeloDinTotal.Band;
                        cloneTot = new XRLabel();
                        NovosComponentes.Add(cloneTot);
                        RodapeTotal.Controls.Add(cloneTot);
                        cloneTot.TextAlignment = ModeloDinTotal.TextAlignment;

                        cloneTot.Summary.FormatString = ModeloDinTotal.Summary.FormatString;
                        cloneTot.Summary.Func = ModeloDinTotal.Summary.Func;
                        cloneTot.Summary.Running = ModeloDinTotal.Summary.Running;
                    }
                    if ((ModeloDinSubTotal != null) && (ModeloDinSubTotal.Band is GroupFooterBand))
                    {
                        GroupFooterBand RodapeSubTotal = (GroupFooterBand)ModeloDinSubTotal.Band;
                        cloneSubTot = new XRLabel();
                        NovosComponentes.Add(cloneSubTot);
                        RodapeSubTotal.Controls.Add(cloneSubTot);
                        cloneSubTot.TextAlignment = ModeloDinSubTotal.TextAlignment;
                        cloneSubTot.Summary.FormatString = ModeloDinSubTotal.Summary.FormatString;
                        cloneSubTot.Summary.Func = ModeloDinSubTotal.Summary.Func;
                        cloneSubTot.Summary.Running = ModeloDinSubTotal.Summary.Running;
                    }
                }
                else
                {
                    clone = ModeloDin;
                    cloneTitulo = ModeloDinTitulo;
                    cloneSubTot = ModeloDinSubTotal;
                    cloneTot = ModeloDinTotal;
                }
                if (clone != null)
                {
                    if (UsaDegrade)
                        clone.Draw += Degrade;
                    clone.Size = new Size(larguraColunas, ModeloDin.Height);
                    clone.Location = new Point(ModeloDin.Location.X + deltaAcumulado, ModeloDin.Location.Y);
                    clone.Font = fonte;
                    AjustaBind(clone, TituloColuna,Mascara);                    
                }
                if (cloneTitulo != null)
                {
                    cloneTitulo.Location = new Point(ModeloDinTitulo.Location.X + deltaAcumulado, ModeloDinTitulo.Location.Y);
                    cloneTitulo.Size = new Size(larguraColunas, ModeloDinTitulo.Height);
                    cloneTitulo.Text = TituloImpresso;
                }
                if (cloneSubTot != null)
                {
                    cloneSubTot.Location = new Point(ModeloDinSubTotal.Location.X + deltaAcumulado, ModeloDinSubTotal.Location.Y);
                    cloneSubTot.Size = new Size(larguraColunas, ModeloDinSubTotal.Height);
                    cloneSubTot.Font = fonteTotal;
                    if (TipoDado == typeof(decimal))                    
                        AjustaBind(cloneSubTot, TituloColuna, Mascara);                    
                    else
                        cloneSubTot.Visible = false;
                }
                if (cloneTot != null)
                {
                    cloneTot.Location = new Point(ModeloDinTotal.Location.X + deltaAcumulado, ModeloDinTotal.Location.Y);
                    cloneTot.Size = new Size(larguraColunas, ModeloDinTotal.Height);
                    cloneTot.Font = fonteTotal;
                    if (TipoDado == typeof(decimal))                    
                        AjustaBind(cloneTot, TituloColuna, Mascara);                    
                    else
                        cloneTot.Visible = false;
                }
                MoverDireita = deltaAcumulado;
                deltaAcumulado += larguraColunas;
            };
            if(moveDireita != null)
                foreach(XRControl Mover in moveDireita)
                    Mover.Location = new Point(Mover.Location.X + MoverDireita, Mover.Location.Y);

        }
        /*
         private void CriaColunaNoImpressoEfetivo(bool ComLinhas)
        {
            
            

            int deltaAcumulado = 0;
            int larguraColunas = xrX0.Width;
            bool NumeroBol = (bool)NumeroBoleto.Value;
            int ColunasInc = 0;
                       
            int testacolunas = (int)Partestacolunas.Value;

            int CorrigePosicao = 0;

            if (!NumeroBol && !BoletoRemovido)
                CorrigePosicao = -larguraColunas;
            else if (NumeroBol && BoletoRemovido)
                CorrigePosicao = larguraColunas;
            BoletoRemovido = !NumeroBol;

            if (!NumeroBol)
            {
                xrNumeroBol.Visible = xrNumeroBolTit.Visible = false;                
                ColunasInc--;                
            }
            else
                xrNumeroBol.Visible = xrNumeroBolTit.Visible = true;

            if (CorrigePosicao != 0)
            {
                xrResp.Location = new Point(xrResp.Location.X + CorrigePosicao, xrResp.Location.Y);
                xrRespTit.Location = new Point(xrRespTit.Location.X + CorrigePosicao, xrRespTit.Location.Y);
                xrVenc.Location = new Point(xrVenc.Location.X + CorrigePosicao, xrVenc.Location.Y);
                xrVencTit.Location = new Point(xrVencTit.Location.X + CorrigePosicao, xrVencTit.Location.Y);
                xrSubTotalTit.Location = new Point(xrSubTotalTit.Location.X + CorrigePosicao, xrSubTotalTit.Location.Y);
                xrSubTotalSum.Location = new Point(xrSubTotalSum.Location.X + CorrigePosicao, xrSubTotalSum.Location.Y);
                xrX0.Location = new Point(xrX0.Location.X + CorrigePosicao, xrX0.Location.Y);
                xrX0Tit.Location = new Point(xrX0Tit.Location.X + CorrigePosicao, xrX0Tit.Location.Y);
                xrX0SutTot.Location = new Point(xrX0SutTot.Location.X + CorrigePosicao, xrX0SutTot.Location.Y);
                xrX0Tot.Location = new Point(xrX0Tot.Location.X + CorrigePosicao, xrX0Tot.Location.Y);
                xrPag.Location = new Point(xrPag.Location.X + CorrigePosicao, xrPag.Location.Y);
                xrPagT.Location = new Point(xrPagT.Location.X + CorrigePosicao, xrPagT.Location.Y);
            }

            
            if(fonte0 == null)
                fonte0 = xrX0.Font;
            Font fonte = fonte0;
            Font fonteT;
            XRLabel ModeloDetalhe = xrX0;
            XRLabel ModeloDetalheTot = xrX0Tot;
            XRLabel ModeloTitulo = xrX0Tit;
            XRLabel ModeloSubTot = xrX0SutTot;
            clone = ModeloDetalhe;
            cloneT = ModeloTitulo;
            cloneTot = ModeloDetalheTot;
            cloneSubTot = ModeloSubTot;
            if (testacolunas != 0)
            {
                while (balancete.ColunasDinamicas.Count > testacolunas)
                    balancete.ColunasDinamicas.RemoveAt(0);
                
                while (balancete.ColunasDinamicas.Count < testacolunas)
                    balancete.ColunasDinamicas.Add(string.Format("Teste{0}", nColunaTeste), string.Format("Coluna Teste: {0}", nColunaTeste++));
            }

            ColunasInc += balancete.ColunasDinamicas.Count;

            int W30 = 17;

            

            //else if (ColunasInc < 10)
            //{
            //    larguraColunas = larguraColunas + 15;                        
            //}
            fonteT = new Font(fonte.FontFamily, fonte.Size - 1, FontStyle.Bold);
            
            xrtotal.Location = new Point(xrX0.Location.X + deltaAcumulado, ModeloDetalhe.Location.Y);
            if ((bool)Marcadores.Value)
            {
                xrTotalG.Location = new Point(xrX0.Location.X + deltaAcumulado, xrTotalG.Location.Y);
                xrmarcador.Location = new Point(xrTotalG.Location.X + xrTotalG.Width, xrmarcador.Location.Y);
            }
            else
                xrTotalG.Location = new Point(xrX0.Location.X + deltaAcumulado, xrTotalG.Location.Y);
            xrSubTotalG.Location = new Point(xrX0.Location.X + deltaAcumulado, xrSubTotalG.Location.Y);
            xrTotalTitulo.Location = new Point(xrX0.Location.X + deltaAcumulado, xrTotalTitulo.Location.Y);

            if (ComLinhas)
            {
                XRLine Novalinha = new XRLine();
                Novalinha.Size = new Size(10, 5);
                Novalinha.Width = 1;
                Bands[BandKind.Detail].Controls.Add(Novalinha);
                Novalinha.Location = new Point(0, 42);
                Bands[BandKind.Detail].Height = 42;
                Novalinha.Width = xrtotal.Location.X + xrtotal.Width;
            };
            xrPanel1.Width = xrtotal.Location.X + xrtotal.Width;
            xrAgrupa.Width = xrPanel1.Width;
            //if (Tipo == TipoRelatorio.Emissao)
            //{
            //    GroupFooter1.Visible = GroupHeader1.Visible = false;
            //}
        }
        */
        #endregion

        private void ImpDestRem_AfterPrint(object sender, EventArgs e)
        {
            Apontador.Apontar(Pages.Count);
        }

        /// <summary>
        /// Funcao para ser chamada a partir do DRAW. Desenha o fundo em degrade
        /// </summary>
        /// <param name="sender">Repassar o do DRAW</param>
        /// <param name="e">Repassar o do DRAW</param>
        protected void Degrade(object sender, DrawEventArgs e)
        {
            ImpGeradorDeEfeitos.Degrade(sender, e);
            return;

        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="CON"></param>
        /// <param name="Tipo"></param>
        public void Apontamento(int CON, int Tipo)
        {
            Apontador.condominio = CON;
            Apontador.tipo = Tipo;
        }
        /// <summary>
        /// Retira o logo
        /// </summary>
        public void RemoverLogo()
        {
            xrPictureBox1.Visible = false;
            xrTitulo.Width += xrTitulo.Left;
            xrTitulo.Left = 0;
        }

        /// <summary>
        /// Remove o logo e o titulo
        /// </summary>
        public virtual void RemoverLogoeTitulo()
        {
            xrPictureBox1.Visible = false;
            xrTitulo.Visible = false;
        }
    }
}
