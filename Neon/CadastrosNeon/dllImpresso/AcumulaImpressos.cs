﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DevExpress.XtraReports.UI;

namespace dllImpresso
{
    /// <summary>
    /// Objeto para acumular impressos em um só
    /// </summary>
    public class AcumulaImpressos
    {
        private List<ImpBase> ImpressosAcumulados;

        /// <summary>
        /// Construtor
        /// </summary>
        /// <param name="Impressos"></param>
        public AcumulaImpressos(params ImpBase[] Impressos)
        {
            ImpressosAcumulados = new List<ImpBase>();
            foreach (ImpBase Novo in Impressos)
                Acumula(Novo);
        }

        /// <summary>
        /// Acumula
        /// </summary>
        /// <param name="Novo"></param>
        public void Acumula(ImpBase Novo)
        {
            if (Novo != null)
            {
                foreach (ImpBase Acumulado in ImpressosAcumulados)
                {
                    Type TipoNovo = Novo.GetType();
                    if (TipoNovo == Acumulado.GetType())
                    {
                        Acumulado.Pages.AddRange(Novo.Pages);
                        return;
                    }
                };
                ImpressosAcumulados.Add(Novo);
            }
        }

        /// <summary>
        /// Acumula
        /// </summary>
        /// <param name="Novos"></param>
        public void Acumula(AcumulaImpressos Novos)
        {
            if (Novos != null)
                foreach (ImpBase novo in Novos.ImpressosAcumulados)
                    Acumula(novo);                          
        }

        /// <summary>
        /// imprimir
        /// </summary>
        public void Imprimir()
        {
            foreach (ImpBase Acumulado in ImpressosAcumulados)
                Acumulado.Print();
        }

    }
}
