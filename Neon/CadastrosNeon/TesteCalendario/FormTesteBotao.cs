﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace TesteCalendario
{
    public partial class FormTesteBotao : Form
    {
        public FormTesteBotao()
        {
            InitializeComponent();
        }

        private void simpleButton1_Click(object sender, EventArgs e)
        {
            cBotaoImpBol3.Enabled = true;
        }

        private void simpleButton2_Click(object sender, EventArgs e)
        {
            cBotaoImpBol3.Enabled = false;
        }

        private void simpleButton3_Click(object sender, EventArgs e)
        {
            Cadastros.Apartamentos.Apartamento Ap = (Cadastros.Apartamentos.Apartamento)AbstratosNeon.ABS_Apartamento.GetApartamento(58410);
            AbstratosNeon.ABS_Condominio Con = Ap.Condominio;
            CadastrosProc.PessoaProc Pes = (CadastrosProc.PessoaProc)Con.Sindico;
            textEdit1.Text = Pes.Nome;
        }
    }
}
