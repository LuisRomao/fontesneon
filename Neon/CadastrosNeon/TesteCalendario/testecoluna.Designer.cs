﻿namespace TesteCalendario
{
    partial class testecoluna
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            DevExpress.XtraReports.UI.XRSummary xrSummary1 = new DevExpress.XtraReports.UI.XRSummary();
            this.xrModelo = new DevExpress.XtraReports.UI.XRLabel();
            this.ModeloTx = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLine1 = new DevExpress.XtraReports.UI.XRLine();
            this.xrLabel1 = new DevExpress.XtraReports.UI.XRLabel();
            this.ModeloTotal = new DevExpress.XtraReports.UI.XRLabel();
            this.GroupFooter1 = new DevExpress.XtraReports.UI.GroupFooterBand();
            this.xrLine2 = new DevExpress.XtraReports.UI.XRLine();
            this.objectDataSource1 = new DevExpress.DataAccess.ObjectBinding.ObjectDataSource(this.components);
            this.dtesteColunas1 = new TesteCalendario.dtesteColunas();
            this.Direita1 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel2 = new DevExpress.XtraReports.UI.XRLabel();
            this.LinhaMover = new DevExpress.XtraReports.UI.XRLine();
            this.Direita2 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel3 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLine3 = new DevExpress.XtraReports.UI.XRLine();
            ((System.ComponentModel.ISupportInitialize)(this.objectDataSource1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dtesteColunas1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            // 
            // PageHeader
            // 
            this.PageHeader.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrLine3,
            this.xrLabel3,
            this.Direita2,
            this.LinhaMover,
            this.Direita1,
            this.ModeloTx});
            this.PageHeader.HeightF = 333.1043F;
            this.PageHeader.Controls.SetChildIndex(this.xrTitulo, 0);
            this.PageHeader.Controls.SetChildIndex(this.ModeloTx, 0);
            this.PageHeader.Controls.SetChildIndex(this.Direita1, 0);
            this.PageHeader.Controls.SetChildIndex(this.LinhaMover, 0);
            this.PageHeader.Controls.SetChildIndex(this.Direita2, 0);
            this.PageHeader.Controls.SetChildIndex(this.xrLabel3, 0);
            this.PageHeader.Controls.SetChildIndex(this.xrLine3, 0);
            // 
            // Detail
            // 
            this.Detail.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrLabel2,
            this.xrLabel1,
            this.xrLine1,
            this.xrModelo});
            this.Detail.HeightF = 66.1459F;
            // 
            // xrModelo
            // 
            this.xrModelo.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrModelo.Dpi = 254F;
            this.xrModelo.LocationFloat = new DevExpress.Utils.PointFloat(349.5248F, 7.725897F);
            this.xrModelo.Name = "xrModelo";
            this.xrModelo.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrModelo.SizeF = new System.Drawing.SizeF(185.5884F, 58.42001F);
            this.xrModelo.StylePriority.UseBorders = false;
            this.xrModelo.StylePriority.UseTextAlignment = false;
            this.xrModelo.Text = "Modelo";
            this.xrModelo.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            // 
            // ModeloTx
            // 
            this.ModeloTx.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.ModeloTx.Dpi = 254F;
            this.ModeloTx.LocationFloat = new DevExpress.Utils.PointFloat(349.5248F, 274.6842F);
            this.ModeloTx.Name = "ModeloTx";
            this.ModeloTx.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.ModeloTx.SizeF = new System.Drawing.SizeF(185.5884F, 58.42001F);
            this.ModeloTx.StylePriority.UseBorders = false;
            this.ModeloTx.Text = "xrModelo";
            // 
            // xrLine1
            // 
            this.xrLine1.Dpi = 254F;
            this.xrLine1.LineDirection = DevExpress.XtraReports.UI.LineDirection.Vertical;
            this.xrLine1.LineWidth = 3;
            this.xrLine1.LocationFloat = new DevExpress.Utils.PointFloat(339F, 25F);
            this.xrLine1.Name = "xrLine1";
            this.xrLine1.SizeF = new System.Drawing.SizeF(10.52486F, 34.2058F);
            // 
            // xrLabel1
            // 
            this.xrLabel1.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "ID")});
            this.xrLabel1.Dpi = 254F;
            this.xrLabel1.LocationFloat = new DevExpress.Utils.PointFloat(11.82624F, 7.725897F);
            this.xrLabel1.Name = "xrLabel1";
            this.xrLabel1.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrLabel1.SizeF = new System.Drawing.SizeF(69.42466F, 58.42F);
            // 
            // ModeloTotal
            // 
            this.ModeloTotal.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.ModeloTotal.Dpi = 254F;
            this.ModeloTotal.LocationFloat = new DevExpress.Utils.PointFloat(349.5248F, 13.47781F);
            this.ModeloTotal.Name = "ModeloTotal";
            this.ModeloTotal.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.ModeloTotal.SizeF = new System.Drawing.SizeF(185.5884F, 58.42001F);
            this.ModeloTotal.StylePriority.UseBorders = false;
            this.ModeloTotal.StylePriority.UseTextAlignment = false;
            xrSummary1.FormatString = "{0:n2}";
            xrSummary1.Running = DevExpress.XtraReports.UI.SummaryRunning.Group;
            this.ModeloTotal.Summary = xrSummary1;
            this.ModeloTotal.Text = "xrModelo";
            this.ModeloTotal.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            // 
            // GroupFooter1
            // 
            this.GroupFooter1.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrLine2,
            this.ModeloTotal});
            this.GroupFooter1.Dpi = 254F;
            this.GroupFooter1.HeightF = 109.3021F;
            this.GroupFooter1.Name = "GroupFooter1";
            // 
            // xrLine2
            // 
            this.xrLine2.Dpi = 254F;
            this.xrLine2.LineWidth = 3;
            this.xrLine2.LocationFloat = new DevExpress.Utils.PointFloat(325.0605F, 92.18657F);
            this.xrLine2.Name = "xrLine2";
            this.xrLine2.SizeF = new System.Drawing.SizeF(234.9776F, 17.11549F);
            // 
            // objectDataSource1
            // 
            this.objectDataSource1.DataMember = "TabelaTeste";
            this.objectDataSource1.DataSource = typeof(TesteCalendario.dtesteColunas);
            this.objectDataSource1.Name = "objectDataSource1";
            // 
            // dtesteColunas1
            // 
            this.dtesteColunas1.DataSetName = "dtesteColunas";
            this.dtesteColunas1.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // Direita1
            // 
            this.Direita1.BackColor = System.Drawing.Color.Gold;
            this.Direita1.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.Direita1.Dpi = 254F;
            this.Direita1.LocationFloat = new DevExpress.Utils.PointFloat(545.6381F, 274.6842F);
            this.Direita1.Name = "Direita1";
            this.Direita1.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.Direita1.SizeF = new System.Drawing.SizeF(185.5884F, 58.42001F);
            this.Direita1.StylePriority.UseBackColor = false;
            this.Direita1.StylePriority.UseBorders = false;
            this.Direita1.Text = "DIREITA";
            // 
            // xrLabel2
            // 
            this.xrLabel2.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "Nome")});
            this.xrLabel2.Dpi = 254F;
            this.xrLabel2.LocationFloat = new DevExpress.Utils.PointFloat(84.99998F, 7.725897F);
            this.xrLabel2.Name = "xrLabel2";
            this.xrLabel2.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrLabel2.SizeF = new System.Drawing.SizeF(254F, 58.42F);
            // 
            // LinhaMover
            // 
            this.LinhaMover.BackColor = System.Drawing.Color.Maroon;
            this.LinhaMover.Dpi = 254F;
            this.LinhaMover.LineDirection = DevExpress.XtraReports.UI.LineDirection.Vertical;
            this.LinhaMover.LineWidth = 3;
            this.LinhaMover.LocationFloat = new DevExpress.Utils.PointFloat(535.1132F, 274.6842F);
            this.LinhaMover.Name = "LinhaMover";
            this.LinhaMover.SizeF = new System.Drawing.SizeF(10.52486F, 34.2058F);
            this.LinhaMover.StylePriority.UseBackColor = false;
            // 
            // Direita2
            // 
            this.Direita2.BackColor = System.Drawing.Color.Gold;
            this.Direita2.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.Direita2.Dpi = 254F;
            this.Direita2.LocationFloat = new DevExpress.Utils.PointFloat(731.2265F, 274.6843F);
            this.Direita2.Name = "Direita2";
            this.Direita2.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.Direita2.SizeF = new System.Drawing.SizeF(185.5884F, 58.42001F);
            this.Direita2.StylePriority.UseBackColor = false;
            this.Direita2.StylePriority.UseBorders = false;
            this.Direita2.Text = "DIREITA";
            // 
            // xrLabel3
            // 
            this.xrLabel3.BackColor = System.Drawing.Color.Gold;
            this.xrLabel3.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrLabel3.Dpi = 254F;
            this.xrLabel3.LocationFloat = new DevExpress.Utils.PointFloat(1061.206F, 229.9463F);
            this.xrLabel3.Name = "xrLabel3";
            this.xrLabel3.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrLabel3.SizeF = new System.Drawing.SizeF(185.5884F, 58.42001F);
            this.xrLabel3.StylePriority.UseBackColor = false;
            this.xrLabel3.StylePriority.UseBorders = false;
            this.xrLabel3.Text = "DIREITA";
            // 
            // xrLine3
            // 
            this.xrLine3.BackColor = System.Drawing.Color.DarkTurquoise;
            this.xrLine3.Dpi = 254F;
            this.xrLine3.LineDirection = DevExpress.XtraReports.UI.LineDirection.Vertical;
            this.xrLine3.LineWidth = 3;
            this.xrLine3.LocationFloat = new DevExpress.Utils.PointFloat(535.1132F, 229.9463F);
            this.xrLine3.Name = "xrLine3";
            this.xrLine3.SizeF = new System.Drawing.SizeF(10.52486F, 34.2058F);
            this.xrLine3.StylePriority.UseBackColor = false;
            // 
            // testecoluna
            // 
            this.Bands.AddRange(new DevExpress.XtraReports.UI.Band[] {
            this.Detail,
            this.PageHeader,
            this.GroupFooter1});
            this.ComponentStorage.AddRange(new System.ComponentModel.IComponent[] {
            this.objectDataSource1});
            this.DataSource = this.objectDataSource1;
            this.ModeloBind = this.xrLabel1;
            this.ModeloDin = this.xrModelo;
            this.ModeloDinTitulo = this.ModeloTx;
            this.ModeloDinTotal = this.ModeloTotal;
            this.MoveDireita = new DevExpress.XtraReports.UI.XRControl[] {
        ((DevExpress.XtraReports.UI.XRControl)(this.Direita1)),
        ((DevExpress.XtraReports.UI.XRControl)(this.Direita2)),
        ((DevExpress.XtraReports.UI.XRControl)(this.xrLine3))};
            this.Version = "16.2";
            this.Controls.SetChildIndex(this.GroupFooter1, 0);
            this.Controls.SetChildIndex(this.PageHeader, 0);
            this.Controls.SetChildIndex(this.Detail, 0);
            ((System.ComponentModel.ISupportInitialize)(this.objectDataSource1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dtesteColunas1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();

        }


        #endregion

        private DevExpress.XtraReports.UI.XRLabel xrModelo;
        private DevExpress.XtraReports.UI.XRLabel ModeloTx;
        private DevExpress.XtraReports.UI.XRLine xrLine1;
        private DevExpress.XtraReports.UI.XRLabel xrLabel1;
        private DevExpress.XtraReports.UI.XRLabel ModeloTotal;
        private DevExpress.XtraReports.UI.GroupFooterBand GroupFooter1;
        private DevExpress.XtraReports.UI.XRLine xrLine2;
        public DevExpress.DataAccess.ObjectBinding.ObjectDataSource objectDataSource1;
        public dtesteColunas dtesteColunas1;
        private DevExpress.XtraReports.UI.XRLabel Direita1;
        private DevExpress.XtraReports.UI.XRLabel xrLabel2;
        private DevExpress.XtraReports.UI.XRLabel Direita2;
        private DevExpress.XtraReports.UI.XRLine LinhaMover;
        private DevExpress.XtraReports.UI.XRLine xrLine3;
        private DevExpress.XtraReports.UI.XRLabel xrLabel3;
    }
}
