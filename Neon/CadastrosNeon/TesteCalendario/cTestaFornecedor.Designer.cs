﻿namespace TesteCalendario
{
    partial class cTestaFornecedor
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.simpleButton1 = new DevExpress.XtraEditors.SimpleButton();
            this.textEdit1 = new DevExpress.XtraEditors.TextEdit();
            this.simpleButton2 = new DevExpress.XtraEditors.SimpleButton();
            this.simpleButton3 = new DevExpress.XtraEditors.SimpleButton();
            this.gridControl1 = new DevExpress.XtraGrid.GridControl();
            this.dMigraBindingSource = new System.Windows.Forms.BindingSource();
            this.dMigra = new TesteCalendario.dMigra();
            this.gridView1 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colAPD = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colAPD_EMP = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colAPD_CON = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colAPD_APT = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colAPDTipo = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colAPDDescritivo = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colAPDEXT = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colAPDPublico = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colAPDData = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colAPD_USU = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridControl2 = new DevExpress.XtraGrid.GridControl();
            this.gridView2 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colAPD1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colAPD_APT1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colAPDTipo1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colAPDDescritivo1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colAPDEXT1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colAPDPublico1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colAPDData1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colAPD_USU1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridControl3 = new DevExpress.XtraGrid.GridControl();
            this.gridView3 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colAPD2 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colAPD_APT2 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colAPDTipo2 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colAPDDescritivo2 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colAPDEXT2 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colAPDPublico2 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colAPDData2 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colAPD_USU2 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.simpleButton4 = new DevExpress.XtraEditors.SimpleButton();
            this.checkEditSBC = new DevExpress.XtraEditors.CheckEdit();
            this.labelControl1 = new DevExpress.XtraEditors.LabelControl();
            this.xtraTabControl1 = new DevExpress.XtraTab.XtraTabControl();
            this.xtraTabPage1 = new DevExpress.XtraTab.XtraTabPage();
            this.xtraTabPage2 = new DevExpress.XtraTab.XtraTabPage();
            this.labelControl2 = new DevExpress.XtraEditors.LabelControl();
            this.simpleButtonMigra = new DevExpress.XtraEditors.SimpleButton();
            this.gridControl5 = new DevExpress.XtraGrid.GridControl();
            this.gridView5 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colPUB = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colPUB_CODCON = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colPUBData = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colPUBTipo = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colPUBLocal = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colPUBDescritivo = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colPUBArquivo = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colPUBExtensao = new DevExpress.XtraGrid.Columns.GridColumn();
            this.simpleButton5 = new DevExpress.XtraEditors.SimpleButton();
            this.gridControl4 = new DevExpress.XtraGrid.GridControl();
            this.gridView4 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colPUB1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colPUB_CODCON1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colPUBData1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colPUBTipo1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colPUBLocal1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colPUBDescritivo1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colPUBExtensao1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colPUBArquivo1 = new DevExpress.XtraGrid.Columns.GridColumn();
            ((System.ComponentModel.ISupportInitialize)(this.StyleController_F)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit1.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dMigraBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dMigra)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEditSBC.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xtraTabControl1)).BeginInit();
            this.xtraTabControl1.SuspendLayout();
            this.xtraTabPage1.SuspendLayout();
            this.xtraTabPage2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView4)).BeginInit();
            this.SuspendLayout();
            // 
            // ToolTipController_F
            // 
            this.ToolTipController_F.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.ToolTipController_F.Appearance.ForeColor = System.Drawing.Color.Black;
            this.ToolTipController_F.Appearance.Options.UseBackColor = true;
            this.ToolTipController_F.Appearance.Options.UseFont = true;
            this.ToolTipController_F.Appearance.Options.UseForeColor = true;
            this.ToolTipController_F.Appearance.Options.UseTextOptions = true;
            this.ToolTipController_F.Appearance.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            // 
            // StyleController_F
            // 
            this.StyleController_F.AppearanceDisabled.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this.StyleController_F.AppearanceDisabled.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this.StyleController_F.AppearanceDisabled.Options.UseBackColor = true;
            this.StyleController_F.AppearanceDisabled.Options.UseForeColor = true;
            this.StyleController_F.AppearanceFocused.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.StyleController_F.AppearanceFocused.Options.UseBackColor = true;
            // 
            // simpleButton1
            // 
            this.simpleButton1.Location = new System.Drawing.Point(13, 3);
            this.simpleButton1.Name = "simpleButton1";
            this.simpleButton1.Size = new System.Drawing.Size(159, 23);
            this.simpleButton1.TabIndex = 0;
            this.simpleButton1.Text = "TESTE Fornecedor";
            this.simpleButton1.Click += new System.EventHandler(this.simpleButton1_Click);
            // 
            // textEdit1
            // 
            this.textEdit1.Location = new System.Drawing.Point(13, 32);
            this.textEdit1.Name = "textEdit1";
            this.textEdit1.Size = new System.Drawing.Size(391, 20);
            this.textEdit1.TabIndex = 1;
            // 
            // simpleButton2
            // 
            this.simpleButton2.Location = new System.Drawing.Point(13, 58);
            this.simpleButton2.Name = "simpleButton2";
            this.simpleButton2.Size = new System.Drawing.Size(159, 23);
            this.simpleButton2.TabIndex = 2;
            this.simpleButton2.Text = "TESTE Condominio";
            this.simpleButton2.Click += new System.EventHandler(this.simpleButton2_Click);
            // 
            // simpleButton3
            // 
            this.simpleButton3.Location = new System.Drawing.Point(37, 20);
            this.simpleButton3.Name = "simpleButton3";
            this.simpleButton3.Size = new System.Drawing.Size(159, 23);
            this.simpleButton3.TabIndex = 3;
            this.simpleButton3.Text = "Migra Abre";
            this.simpleButton3.Click += new System.EventHandler(this.simpleButton3_Click);
            // 
            // gridControl1
            // 
            this.gridControl1.DataMember = "APartamentoDoc";
            this.gridControl1.DataSource = this.dMigraBindingSource;
            this.gridControl1.Location = new System.Drawing.Point(20, 49);
            this.gridControl1.MainView = this.gridView1;
            this.gridControl1.Name = "gridControl1";
            this.gridControl1.Size = new System.Drawing.Size(1292, 200);
            this.gridControl1.TabIndex = 4;
            this.gridControl1.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridView1});
            // 
            // dMigraBindingSource
            // 
            this.dMigraBindingSource.DataSource = this.dMigra;
            this.dMigraBindingSource.Position = 0;
            // 
            // dMigra
            // 
            this.dMigra.DataSetName = "dMigra";
            this.dMigra.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // gridView1
            // 
            this.gridView1.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colAPD,
            this.colAPD_EMP,
            this.colAPD_CON,
            this.colAPD_APT,
            this.colAPDTipo,
            this.colAPDDescritivo,
            this.colAPDEXT,
            this.colAPDPublico,
            this.colAPDData,
            this.colAPD_USU});
            this.gridView1.GridControl = this.gridControl1;
            this.gridView1.Name = "gridView1";
            this.gridView1.OptionsView.ShowFooter = true;
            // 
            // colAPD
            // 
            this.colAPD.FieldName = "APD";
            this.colAPD.Name = "colAPD";
            this.colAPD.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Count, "APD", "{0}")});
            this.colAPD.Visible = true;
            this.colAPD.VisibleIndex = 0;
            // 
            // colAPD_EMP
            // 
            this.colAPD_EMP.FieldName = "APD_EMP";
            this.colAPD_EMP.Name = "colAPD_EMP";
            this.colAPD_EMP.Visible = true;
            this.colAPD_EMP.VisibleIndex = 1;
            // 
            // colAPD_CON
            // 
            this.colAPD_CON.FieldName = "APD_CON";
            this.colAPD_CON.Name = "colAPD_CON";
            this.colAPD_CON.Visible = true;
            this.colAPD_CON.VisibleIndex = 2;
            // 
            // colAPD_APT
            // 
            this.colAPD_APT.FieldName = "APD_APT";
            this.colAPD_APT.Name = "colAPD_APT";
            this.colAPD_APT.Visible = true;
            this.colAPD_APT.VisibleIndex = 3;
            // 
            // colAPDTipo
            // 
            this.colAPDTipo.FieldName = "APDTipo";
            this.colAPDTipo.Name = "colAPDTipo";
            this.colAPDTipo.Visible = true;
            this.colAPDTipo.VisibleIndex = 4;
            // 
            // colAPDDescritivo
            // 
            this.colAPDDescritivo.FieldName = "APDDescritivo";
            this.colAPDDescritivo.Name = "colAPDDescritivo";
            this.colAPDDescritivo.Visible = true;
            this.colAPDDescritivo.VisibleIndex = 5;
            // 
            // colAPDEXT
            // 
            this.colAPDEXT.FieldName = "APDEXT";
            this.colAPDEXT.Name = "colAPDEXT";
            this.colAPDEXT.Visible = true;
            this.colAPDEXT.VisibleIndex = 6;
            // 
            // colAPDPublico
            // 
            this.colAPDPublico.FieldName = "APDPublico";
            this.colAPDPublico.Name = "colAPDPublico";
            this.colAPDPublico.Visible = true;
            this.colAPDPublico.VisibleIndex = 7;
            // 
            // colAPDData
            // 
            this.colAPDData.FieldName = "APDData";
            this.colAPDData.Name = "colAPDData";
            this.colAPDData.Visible = true;
            this.colAPDData.VisibleIndex = 8;
            // 
            // colAPD_USU
            // 
            this.colAPD_USU.FieldName = "APD_USU";
            this.colAPD_USU.Name = "colAPD_USU";
            this.colAPD_USU.Visible = true;
            this.colAPD_USU.VisibleIndex = 9;
            // 
            // gridControl2
            // 
            this.gridControl2.DataMember = "APartamentoDocSBC";
            this.gridControl2.DataSource = this.dMigra;
            this.gridControl2.Location = new System.Drawing.Point(20, 255);
            this.gridControl2.MainView = this.gridView2;
            this.gridControl2.Name = "gridControl2";
            this.gridControl2.Size = new System.Drawing.Size(1292, 166);
            this.gridControl2.TabIndex = 5;
            this.gridControl2.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridView2});
            // 
            // gridView2
            // 
            this.gridView2.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colAPD1,
            this.colAPD_APT1,
            this.colAPDTipo1,
            this.colAPDDescritivo1,
            this.colAPDEXT1,
            this.colAPDPublico1,
            this.colAPDData1,
            this.colAPD_USU1});
            this.gridView2.GridControl = this.gridControl2;
            this.gridView2.Name = "gridView2";
            this.gridView2.OptionsView.ShowFooter = true;
            // 
            // colAPD1
            // 
            this.colAPD1.FieldName = "APD";
            this.colAPD1.Name = "colAPD1";
            this.colAPD1.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Count, "APD", "{0}")});
            this.colAPD1.Visible = true;
            this.colAPD1.VisibleIndex = 0;
            // 
            // colAPD_APT1
            // 
            this.colAPD_APT1.FieldName = "APD_APT";
            this.colAPD_APT1.Name = "colAPD_APT1";
            this.colAPD_APT1.Visible = true;
            this.colAPD_APT1.VisibleIndex = 1;
            // 
            // colAPDTipo1
            // 
            this.colAPDTipo1.FieldName = "APDTipo";
            this.colAPDTipo1.Name = "colAPDTipo1";
            this.colAPDTipo1.Visible = true;
            this.colAPDTipo1.VisibleIndex = 2;
            // 
            // colAPDDescritivo1
            // 
            this.colAPDDescritivo1.FieldName = "APDDescritivo";
            this.colAPDDescritivo1.Name = "colAPDDescritivo1";
            this.colAPDDescritivo1.Visible = true;
            this.colAPDDescritivo1.VisibleIndex = 3;
            // 
            // colAPDEXT1
            // 
            this.colAPDEXT1.FieldName = "APDEXT";
            this.colAPDEXT1.Name = "colAPDEXT1";
            this.colAPDEXT1.Visible = true;
            this.colAPDEXT1.VisibleIndex = 4;
            // 
            // colAPDPublico1
            // 
            this.colAPDPublico1.FieldName = "APDPublico";
            this.colAPDPublico1.Name = "colAPDPublico1";
            this.colAPDPublico1.Visible = true;
            this.colAPDPublico1.VisibleIndex = 5;
            // 
            // colAPDData1
            // 
            this.colAPDData1.FieldName = "APDData";
            this.colAPDData1.Name = "colAPDData1";
            this.colAPDData1.Visible = true;
            this.colAPDData1.VisibleIndex = 6;
            // 
            // colAPD_USU1
            // 
            this.colAPD_USU1.FieldName = "APD_USU";
            this.colAPD_USU1.Name = "colAPD_USU1";
            this.colAPD_USU1.Visible = true;
            this.colAPD_USU1.VisibleIndex = 7;
            // 
            // gridControl3
            // 
            this.gridControl3.DataMember = "APartamentoDocSA";
            this.gridControl3.DataSource = this.dMigra;
            this.gridControl3.Location = new System.Drawing.Point(20, 427);
            this.gridControl3.MainView = this.gridView3;
            this.gridControl3.Name = "gridControl3";
            this.gridControl3.Size = new System.Drawing.Size(1292, 229);
            this.gridControl3.TabIndex = 6;
            this.gridControl3.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridView3});
            // 
            // gridView3
            // 
            this.gridView3.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colAPD2,
            this.colAPD_APT2,
            this.colAPDTipo2,
            this.colAPDDescritivo2,
            this.colAPDEXT2,
            this.colAPDPublico2,
            this.colAPDData2,
            this.colAPD_USU2});
            this.gridView3.GridControl = this.gridControl3;
            this.gridView3.Name = "gridView3";
            this.gridView3.OptionsView.ShowFooter = true;
            // 
            // colAPD2
            // 
            this.colAPD2.FieldName = "APD";
            this.colAPD2.Name = "colAPD2";
            this.colAPD2.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Count, "APD", "{0}")});
            this.colAPD2.Visible = true;
            this.colAPD2.VisibleIndex = 0;
            // 
            // colAPD_APT2
            // 
            this.colAPD_APT2.FieldName = "APD_APT";
            this.colAPD_APT2.Name = "colAPD_APT2";
            this.colAPD_APT2.Visible = true;
            this.colAPD_APT2.VisibleIndex = 1;
            // 
            // colAPDTipo2
            // 
            this.colAPDTipo2.FieldName = "APDTipo";
            this.colAPDTipo2.Name = "colAPDTipo2";
            this.colAPDTipo2.Visible = true;
            this.colAPDTipo2.VisibleIndex = 2;
            // 
            // colAPDDescritivo2
            // 
            this.colAPDDescritivo2.FieldName = "APDDescritivo";
            this.colAPDDescritivo2.Name = "colAPDDescritivo2";
            this.colAPDDescritivo2.Visible = true;
            this.colAPDDescritivo2.VisibleIndex = 3;
            // 
            // colAPDEXT2
            // 
            this.colAPDEXT2.FieldName = "APDEXT";
            this.colAPDEXT2.Name = "colAPDEXT2";
            this.colAPDEXT2.Visible = true;
            this.colAPDEXT2.VisibleIndex = 4;
            // 
            // colAPDPublico2
            // 
            this.colAPDPublico2.FieldName = "APDPublico";
            this.colAPDPublico2.Name = "colAPDPublico2";
            this.colAPDPublico2.Visible = true;
            this.colAPDPublico2.VisibleIndex = 5;
            // 
            // colAPDData2
            // 
            this.colAPDData2.FieldName = "APDData";
            this.colAPDData2.Name = "colAPDData2";
            this.colAPDData2.Visible = true;
            this.colAPDData2.VisibleIndex = 6;
            // 
            // colAPD_USU2
            // 
            this.colAPD_USU2.FieldName = "APD_USU";
            this.colAPD_USU2.Name = "colAPD_USU2";
            this.colAPD_USU2.Visible = true;
            this.colAPD_USU2.VisibleIndex = 7;
            // 
            // simpleButton4
            // 
            this.simpleButton4.Location = new System.Drawing.Point(202, 20);
            this.simpleButton4.Name = "simpleButton4";
            this.simpleButton4.Size = new System.Drawing.Size(159, 23);
            this.simpleButton4.TabIndex = 7;
            this.simpleButton4.Text = "Migra ApartamentoDoc";
            this.simpleButton4.Click += new System.EventHandler(this.simpleButton4_Click);
            // 
            // checkEditSBC
            // 
            this.checkEditSBC.Location = new System.Drawing.Point(380, 20);
            this.checkEditSBC.Name = "checkEditSBC";
            this.checkEditSBC.Properties.Caption = "SBC";
            this.checkEditSBC.Size = new System.Drawing.Size(75, 19);
            this.checkEditSBC.TabIndex = 8;
            // 
            // labelControl1
            // 
            this.labelControl1.Location = new System.Drawing.Point(447, 26);
            this.labelControl1.Name = "labelControl1";
            this.labelControl1.Size = new System.Drawing.Size(34, 13);
            this.labelControl1.TabIndex = 9;
            this.labelControl1.Text = "Parado";
            // 
            // xtraTabControl1
            // 
            this.xtraTabControl1.Location = new System.Drawing.Point(13, 87);
            this.xtraTabControl1.Name = "xtraTabControl1";
            this.xtraTabControl1.SelectedTabPage = this.xtraTabPage1;
            this.xtraTabControl1.Size = new System.Drawing.Size(1337, 687);
            this.xtraTabControl1.TabIndex = 10;
            this.xtraTabControl1.TabPages.AddRange(new DevExpress.XtraTab.XtraTabPage[] {
            this.xtraTabPage1,
            this.xtraTabPage2});
            // 
            // xtraTabPage1
            // 
            this.xtraTabPage1.Controls.Add(this.labelControl1);
            this.xtraTabPage1.Controls.Add(this.gridControl3);
            this.xtraTabPage1.Controls.Add(this.simpleButton3);
            this.xtraTabPage1.Controls.Add(this.gridControl2);
            this.xtraTabPage1.Controls.Add(this.checkEditSBC);
            this.xtraTabPage1.Controls.Add(this.gridControl1);
            this.xtraTabPage1.Controls.Add(this.simpleButton4);
            this.xtraTabPage1.Name = "xtraTabPage1";
            this.xtraTabPage1.Size = new System.Drawing.Size(1331, 659);
            this.xtraTabPage1.Text = "PubApartamentos";
            // 
            // xtraTabPage2
            // 
            this.xtraTabPage2.Controls.Add(this.labelControl2);
            this.xtraTabPage2.Controls.Add(this.simpleButtonMigra);
            this.xtraTabPage2.Controls.Add(this.gridControl5);
            this.xtraTabPage2.Controls.Add(this.simpleButton5);
            this.xtraTabPage2.Controls.Add(this.gridControl4);
            this.xtraTabPage2.Name = "xtraTabPage2";
            this.xtraTabPage2.Size = new System.Drawing.Size(1331, 659);
            this.xtraTabPage2.Text = "PubCondomínios";
            // 
            // labelControl2
            // 
            this.labelControl2.Location = new System.Drawing.Point(428, 21);
            this.labelControl2.Name = "labelControl2";
            this.labelControl2.Size = new System.Drawing.Size(34, 13);
            this.labelControl2.TabIndex = 11;
            this.labelControl2.Text = "Parado";
            // 
            // simpleButtonMigra
            // 
            this.simpleButtonMigra.Location = new System.Drawing.Point(246, 16);
            this.simpleButtonMigra.Name = "simpleButtonMigra";
            this.simpleButtonMigra.Size = new System.Drawing.Size(159, 23);
            this.simpleButtonMigra.TabIndex = 10;
            this.simpleButtonMigra.Text = "Migra";
            this.simpleButtonMigra.Visible = false;
            this.simpleButtonMigra.Click += new System.EventHandler(this.simpleButtonMigra_Click);
            // 
            // gridControl5
            // 
            this.gridControl5.DataMember = "PUBlicacao";
            this.gridControl5.DataSource = this.dMigraBindingSource;
            this.gridControl5.Location = new System.Drawing.Point(25, 331);
            this.gridControl5.MainView = this.gridView5;
            this.gridControl5.Name = "gridControl5";
            this.gridControl5.Size = new System.Drawing.Size(1280, 312);
            this.gridControl5.TabIndex = 5;
            this.gridControl5.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridView5});
            // 
            // gridView5
            // 
            this.gridView5.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colPUB,
            this.colPUB_CODCON,
            this.colPUBData,
            this.colPUBTipo,
            this.colPUBLocal,
            this.colPUBDescritivo,
            this.colPUBArquivo,
            this.colPUBExtensao});
            this.gridView5.GridControl = this.gridControl5;
            this.gridView5.Name = "gridView5";
            // 
            // colPUB
            // 
            this.colPUB.FieldName = "PUB";
            this.colPUB.Name = "colPUB";
            this.colPUB.Visible = true;
            this.colPUB.VisibleIndex = 0;
            // 
            // colPUB_CODCON
            // 
            this.colPUB_CODCON.FieldName = "PUB_CODCON";
            this.colPUB_CODCON.Name = "colPUB_CODCON";
            this.colPUB_CODCON.Visible = true;
            this.colPUB_CODCON.VisibleIndex = 1;
            // 
            // colPUBData
            // 
            this.colPUBData.FieldName = "PUBData";
            this.colPUBData.Name = "colPUBData";
            this.colPUBData.Visible = true;
            this.colPUBData.VisibleIndex = 2;
            // 
            // colPUBTipo
            // 
            this.colPUBTipo.FieldName = "PUBTipo";
            this.colPUBTipo.Name = "colPUBTipo";
            this.colPUBTipo.Visible = true;
            this.colPUBTipo.VisibleIndex = 3;
            // 
            // colPUBLocal
            // 
            this.colPUBLocal.FieldName = "PUBLocal";
            this.colPUBLocal.Name = "colPUBLocal";
            this.colPUBLocal.Visible = true;
            this.colPUBLocal.VisibleIndex = 4;
            // 
            // colPUBDescritivo
            // 
            this.colPUBDescritivo.FieldName = "PUBDescritivo";
            this.colPUBDescritivo.Name = "colPUBDescritivo";
            this.colPUBDescritivo.Visible = true;
            this.colPUBDescritivo.VisibleIndex = 5;
            // 
            // colPUBArquivo
            // 
            this.colPUBArquivo.FieldName = "PUBArquivo";
            this.colPUBArquivo.Name = "colPUBArquivo";
            this.colPUBArquivo.Visible = true;
            this.colPUBArquivo.VisibleIndex = 6;
            // 
            // colPUBExtensao
            // 
            this.colPUBExtensao.FieldName = "PUBExtensao";
            this.colPUBExtensao.Name = "colPUBExtensao";
            this.colPUBExtensao.Visible = true;
            this.colPUBExtensao.VisibleIndex = 7;
            // 
            // simpleButton5
            // 
            this.simpleButton5.Location = new System.Drawing.Point(25, 15);
            this.simpleButton5.Name = "simpleButton5";
            this.simpleButton5.Size = new System.Drawing.Size(159, 23);
            this.simpleButton5.TabIndex = 4;
            this.simpleButton5.Text = "Migra Abre";
            this.simpleButton5.Click += new System.EventHandler(this.simpleButton5_Click);
            // 
            // gridControl4
            // 
            this.gridControl4.DataMember = "assembleias";
            this.gridControl4.DataSource = this.dMigraBindingSource;
            this.gridControl4.Location = new System.Drawing.Point(25, 56);
            this.gridControl4.MainView = this.gridView4;
            this.gridControl4.Name = "gridControl4";
            this.gridControl4.Size = new System.Drawing.Size(1280, 238);
            this.gridControl4.TabIndex = 0;
            this.gridControl4.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridView4});
            // 
            // gridView4
            // 
            this.gridView4.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colPUB1,
            this.colPUB_CODCON1,
            this.colPUBData1,
            this.colPUBTipo1,
            this.colPUBLocal1,
            this.colPUBDescritivo1,
            this.colPUBExtensao1,
            this.colPUBArquivo1});
            this.gridView4.GridControl = this.gridControl4;
            this.gridView4.Name = "gridView4";
            // 
            // colPUB1
            // 
            this.colPUB1.FieldName = "PUB";
            this.colPUB1.Name = "colPUB1";
            this.colPUB1.Visible = true;
            this.colPUB1.VisibleIndex = 0;
            // 
            // colPUB_CODCON1
            // 
            this.colPUB_CODCON1.FieldName = "PUB_CODCON";
            this.colPUB_CODCON1.Name = "colPUB_CODCON1";
            this.colPUB_CODCON1.Visible = true;
            this.colPUB_CODCON1.VisibleIndex = 1;
            // 
            // colPUBData1
            // 
            this.colPUBData1.FieldName = "PUBData";
            this.colPUBData1.Name = "colPUBData1";
            this.colPUBData1.Visible = true;
            this.colPUBData1.VisibleIndex = 2;
            // 
            // colPUBTipo1
            // 
            this.colPUBTipo1.FieldName = "PUBTipo";
            this.colPUBTipo1.Name = "colPUBTipo1";
            this.colPUBTipo1.Visible = true;
            this.colPUBTipo1.VisibleIndex = 3;
            // 
            // colPUBLocal1
            // 
            this.colPUBLocal1.FieldName = "PUBLocal";
            this.colPUBLocal1.Name = "colPUBLocal1";
            this.colPUBLocal1.Visible = true;
            this.colPUBLocal1.VisibleIndex = 4;
            // 
            // colPUBDescritivo1
            // 
            this.colPUBDescritivo1.FieldName = "PUBDescritivo";
            this.colPUBDescritivo1.Name = "colPUBDescritivo1";
            this.colPUBDescritivo1.Visible = true;
            this.colPUBDescritivo1.VisibleIndex = 5;
            // 
            // colPUBExtensao1
            // 
            this.colPUBExtensao1.FieldName = "PUBExtensao";
            this.colPUBExtensao1.Name = "colPUBExtensao1";
            this.colPUBExtensao1.Visible = true;
            this.colPUBExtensao1.VisibleIndex = 6;
            // 
            // colPUBArquivo1
            // 
            this.colPUBArquivo1.FieldName = "PUBArquivo";
            this.colPUBArquivo1.Name = "colPUBArquivo1";
            this.colPUBArquivo1.Visible = true;
            this.colPUBArquivo1.VisibleIndex = 7;
            // 
            // cTestaFornecedor
            // 
            this.Controls.Add(this.xtraTabControl1);
            this.Controls.Add(this.simpleButton2);
            this.Controls.Add(this.textEdit1);
            this.Controls.Add(this.simpleButton1);
            this.LookAndFeel.UseDefaultLookAndFeel = false;
            this.Name = "cTestaFornecedor";
            this.Size = new System.Drawing.Size(1374, 786);
            ((System.ComponentModel.ISupportInitialize)(this.StyleController_F)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit1.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dMigraBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dMigra)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEditSBC.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xtraTabControl1)).EndInit();
            this.xtraTabControl1.ResumeLayout(false);
            this.xtraTabPage1.ResumeLayout(false);
            this.xtraTabPage1.PerformLayout();
            this.xtraTabPage2.ResumeLayout(false);
            this.xtraTabPage2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView4)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraEditors.SimpleButton simpleButton1;
        private DevExpress.XtraEditors.TextEdit textEdit1;
        private DevExpress.XtraEditors.SimpleButton simpleButton2;
        private DevExpress.XtraEditors.SimpleButton simpleButton3;
        private DevExpress.XtraGrid.GridControl gridControl1;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView1;
        private DevExpress.XtraGrid.GridControl gridControl2;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView2;
        private DevExpress.XtraGrid.GridControl gridControl3;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView3;
        private System.Windows.Forms.BindingSource dMigraBindingSource;
        private dMigra dMigra;
        private DevExpress.XtraGrid.Columns.GridColumn colAPD;
        private DevExpress.XtraGrid.Columns.GridColumn colAPD_EMP;
        private DevExpress.XtraGrid.Columns.GridColumn colAPD_CON;
        private DevExpress.XtraGrid.Columns.GridColumn colAPD_APT;
        private DevExpress.XtraGrid.Columns.GridColumn colAPDTipo;
        private DevExpress.XtraGrid.Columns.GridColumn colAPDDescritivo;
        private DevExpress.XtraGrid.Columns.GridColumn colAPDEXT;
        private DevExpress.XtraGrid.Columns.GridColumn colAPDPublico;
        private DevExpress.XtraGrid.Columns.GridColumn colAPDData;
        private DevExpress.XtraGrid.Columns.GridColumn colAPD_USU;
        private DevExpress.XtraGrid.Columns.GridColumn colAPD1;
        private DevExpress.XtraGrid.Columns.GridColumn colAPD_APT1;
        private DevExpress.XtraGrid.Columns.GridColumn colAPDTipo1;
        private DevExpress.XtraGrid.Columns.GridColumn colAPDDescritivo1;
        private DevExpress.XtraGrid.Columns.GridColumn colAPDEXT1;
        private DevExpress.XtraGrid.Columns.GridColumn colAPDPublico1;
        private DevExpress.XtraGrid.Columns.GridColumn colAPDData1;
        private DevExpress.XtraGrid.Columns.GridColumn colAPD_USU1;
        private DevExpress.XtraGrid.Columns.GridColumn colAPD2;
        private DevExpress.XtraGrid.Columns.GridColumn colAPD_APT2;
        private DevExpress.XtraGrid.Columns.GridColumn colAPDTipo2;
        private DevExpress.XtraGrid.Columns.GridColumn colAPDDescritivo2;
        private DevExpress.XtraGrid.Columns.GridColumn colAPDEXT2;
        private DevExpress.XtraGrid.Columns.GridColumn colAPDPublico2;
        private DevExpress.XtraGrid.Columns.GridColumn colAPDData2;
        private DevExpress.XtraGrid.Columns.GridColumn colAPD_USU2;
        private DevExpress.XtraEditors.SimpleButton simpleButton4;
        private DevExpress.XtraEditors.CheckEdit checkEditSBC;
        private DevExpress.XtraEditors.LabelControl labelControl1;
        private DevExpress.XtraTab.XtraTabControl xtraTabControl1;
        private DevExpress.XtraTab.XtraTabPage xtraTabPage1;
        private DevExpress.XtraTab.XtraTabPage xtraTabPage2;
        private DevExpress.XtraGrid.GridControl gridControl5;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView5;
        private DevExpress.XtraGrid.Columns.GridColumn colPUB;
        private DevExpress.XtraGrid.Columns.GridColumn colPUB_CODCON;
        private DevExpress.XtraGrid.Columns.GridColumn colPUBData;
        private DevExpress.XtraGrid.Columns.GridColumn colPUBTipo;
        private DevExpress.XtraGrid.Columns.GridColumn colPUBLocal;
        private DevExpress.XtraGrid.Columns.GridColumn colPUBDescritivo;
        private DevExpress.XtraGrid.Columns.GridColumn colPUBArquivo;
        private DevExpress.XtraGrid.Columns.GridColumn colPUBExtensao;
        private DevExpress.XtraEditors.SimpleButton simpleButton5;
        private DevExpress.XtraGrid.GridControl gridControl4;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView4;
        private DevExpress.XtraEditors.SimpleButton simpleButtonMigra;
        private DevExpress.XtraEditors.LabelControl labelControl2;
        private DevExpress.XtraGrid.Columns.GridColumn colPUB1;
        private DevExpress.XtraGrid.Columns.GridColumn colPUB_CODCON1;
        private DevExpress.XtraGrid.Columns.GridColumn colPUBData1;
        private DevExpress.XtraGrid.Columns.GridColumn colPUBTipo1;
        private DevExpress.XtraGrid.Columns.GridColumn colPUBLocal1;
        private DevExpress.XtraGrid.Columns.GridColumn colPUBDescritivo1;
        private DevExpress.XtraGrid.Columns.GridColumn colPUBExtensao1;
        private DevExpress.XtraGrid.Columns.GridColumn colPUBArquivo1;
    }
}
