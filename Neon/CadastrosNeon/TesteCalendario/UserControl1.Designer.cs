﻿namespace TesteCalendario
{
    partial class UserControl1
    {
        /// <summary>
        /// Variável de designer necessária.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Limpar os recursos que estão sendo usados.
        /// </summary>
        /// <param name="disposing">true se for necessário descartar os recursos gerenciados; caso contrário, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Código gerado pelo Windows Form Designer

        /// <summary>
        /// Método necessário para suporte ao Designer - não modifique 
        /// o conteúdo deste método com o editor de código.
        /// </summary>
        private void InitializeComponent()
        {
            this.cBotaoImpBol1 = new dllImpresso.Botoes.cBotaoImpBol();
            this.cBotaoImpBol2 = new dllImpresso.Botoes.cBotaoImpBol();
            this.cBotaoImpBol3 = new dllImpresso.Botoes.cBotaoImpBol();
            this.simpleButton1 = new DevExpress.XtraEditors.SimpleButton();
            this.simpleButton2 = new DevExpress.XtraEditors.SimpleButton();
            this.simpleButton3 = new DevExpress.XtraEditors.SimpleButton();
            this.simpleButton4 = new DevExpress.XtraEditors.SimpleButton();
            this.memoEdit1 = new DevExpress.XtraEditors.MemoEdit();
            ((System.ComponentModel.ISupportInitialize)(this.StyleController_F)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.memoEdit1.Properties)).BeginInit();
            this.SuspendLayout();
            // 
            // ToolTipController_F
            // 
            this.ToolTipController_F.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.ToolTipController_F.Appearance.ForeColor = System.Drawing.Color.Black;
            this.ToolTipController_F.Appearance.Options.UseBackColor = true;
            this.ToolTipController_F.Appearance.Options.UseFont = true;
            this.ToolTipController_F.Appearance.Options.UseForeColor = true;
            this.ToolTipController_F.Appearance.Options.UseTextOptions = true;
            this.ToolTipController_F.Appearance.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            // 
            // StyleController_F
            // 
            this.StyleController_F.AppearanceDisabled.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this.StyleController_F.AppearanceDisabled.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this.StyleController_F.AppearanceDisabled.Options.UseBackColor = true;
            this.StyleController_F.AppearanceDisabled.Options.UseForeColor = true;
            this.StyleController_F.AppearanceFocused.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.StyleController_F.AppearanceFocused.Options.UseBackColor = true;
            // 
            // cBotaoImpBol1
            // 
            this.cBotaoImpBol1.ItemComprovante = false;
            this.cBotaoImpBol1.Location = new System.Drawing.Point(199, 47);
            this.cBotaoImpBol1.Name = "cBotaoImpBol1";
            this.cBotaoImpBol1.Size = new System.Drawing.Size(120, 26);
            this.cBotaoImpBol1.TabIndex = 0;
            this.cBotaoImpBol1.Titulo = "Imprimir";
            // 
            // cBotaoImpBol2
            // 
            this.cBotaoImpBol2.Enabled = false;
            this.cBotaoImpBol2.ItemComprovante = false;
            this.cBotaoImpBol2.Location = new System.Drawing.Point(199, 113);
            this.cBotaoImpBol2.Name = "cBotaoImpBol2";
            this.cBotaoImpBol2.Size = new System.Drawing.Size(120, 26);
            this.cBotaoImpBol2.TabIndex = 1;
            this.cBotaoImpBol2.Titulo = "Imprimir";
            // 
            // cBotaoImpBol3
            // 
            this.cBotaoImpBol3.ItemComprovante = false;
            this.cBotaoImpBol3.Location = new System.Drawing.Point(199, 171);
            this.cBotaoImpBol3.LookAndFeel.UseDefaultLookAndFeel = false;
            this.cBotaoImpBol3.Name = "cBotaoImpBol3";
            this.cBotaoImpBol3.Size = new System.Drawing.Size(120, 26);
            this.cBotaoImpBol3.TabIndex = 2;
            this.cBotaoImpBol3.Titulo = "Imprimir";
            // 
            // simpleButton1
            // 
            this.simpleButton1.Location = new System.Drawing.Point(395, 132);
            this.simpleButton1.Name = "simpleButton1";
            this.simpleButton1.Size = new System.Drawing.Size(75, 23);
            this.simpleButton1.TabIndex = 4;
            this.simpleButton1.Text = "Liga";
            this.simpleButton1.Click += new System.EventHandler(this.simpleButton1_Click);
            // 
            // simpleButton2
            // 
            this.simpleButton2.Location = new System.Drawing.Point(395, 171);
            this.simpleButton2.Name = "simpleButton2";
            this.simpleButton2.Size = new System.Drawing.Size(75, 26);
            this.simpleButton2.TabIndex = 5;
            this.simpleButton2.Text = "Desliga";
            this.simpleButton2.Click += new System.EventHandler(this.simpleButton2_Click);
            // 
            // simpleButton3
            // 
            this.simpleButton3.Location = new System.Drawing.Point(26, 245);
            this.simpleButton3.Name = "simpleButton3";
            this.simpleButton3.Size = new System.Drawing.Size(368, 23);
            this.simpleButton3.TabIndex = 6;
            this.simpleButton3.Text = "Testa Apartamento";
            this.simpleButton3.Click += new System.EventHandler(this.simpleButton3_Click);
            // 
            // simpleButton4
            // 
            this.simpleButton4.Location = new System.Drawing.Point(26, 274);
            this.simpleButton4.Name = "simpleButton4";
            this.simpleButton4.Size = new System.Drawing.Size(368, 23);
            this.simpleButton4.TabIndex = 7;
            this.simpleButton4.Text = "Testa Apartamento Proc";
            this.simpleButton4.Click += new System.EventHandler(this.simpleButton4_Click);
            // 
            // memoEdit1
            // 
            this.memoEdit1.Location = new System.Drawing.Point(410, 245);
            this.memoEdit1.Name = "memoEdit1";
            this.memoEdit1.Size = new System.Drawing.Size(470, 368);
            this.memoEdit1.TabIndex = 8;
            // 
            // UserControl1
            // 
            this.Controls.Add(this.memoEdit1);
            this.Controls.Add(this.simpleButton4);
            this.Controls.Add(this.simpleButton3);
            this.Controls.Add(this.simpleButton2);
            this.Controls.Add(this.simpleButton1);
            this.Controls.Add(this.cBotaoImpBol3);
            this.Controls.Add(this.cBotaoImpBol2);
            this.Controls.Add(this.cBotaoImpBol1);
            this.LookAndFeel.UseDefaultLookAndFeel = false;
            this.Name = "UserControl1";
            this.Size = new System.Drawing.Size(913, 636);
            ((System.ComponentModel.ISupportInitialize)(this.StyleController_F)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.memoEdit1.Properties)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private dllImpresso.Botoes.cBotaoImpBol cBotaoImpBol1;
        private dllImpresso.Botoes.cBotaoImpBol cBotaoImpBol2;
        private dllImpresso.Botoes.cBotaoImpBol cBotaoImpBol3;
        private DevExpress.XtraEditors.SimpleButton simpleButton1;
        private DevExpress.XtraEditors.SimpleButton simpleButton2;
        private DevExpress.XtraEditors.SimpleButton simpleButton3;
        private DevExpress.XtraEditors.SimpleButton simpleButton4;
        private DevExpress.XtraEditors.MemoEdit memoEdit1;
    }
}
