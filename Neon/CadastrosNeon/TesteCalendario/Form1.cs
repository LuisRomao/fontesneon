﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using CompontesBasicos;

namespace TesteCalendario
{
    public partial class Form1 : FormPrincipalBase
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void navBarItem1_LinkClicked(object sender, DevExpress.XtraNavBar.NavBarLinkEventArgs e)
        {
            FormTesteBotao F = new FormTesteBotao();
            F.ShowDialog();
        }
    }
}
