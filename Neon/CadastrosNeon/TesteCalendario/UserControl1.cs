﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using AbstratosNeon;

namespace TesteCalendario
{
    public partial class UserControl1 : CompontesBasicos.ComponenteBase
    {
        public UserControl1()
        {
            InitializeComponent();
        }

        private void simpleButton1_Click(object sender, EventArgs e)
        {
            cBotaoImpBol3.Enabled = true;
            SortedList<int, ABS_Condominio> Condominios1 = ABS_Condominio.ABSGetCondominios(true);
            SortedList<int, ABS_Condominio> Condominios2 = ABS_Condominio.ABSGetCondominios(true);
            SortedList<int, ABS_Condominio> Condominios3 = ABS_Condominio.ABSGetCondominios(true);
            SortedList<int, ABS_Condominio> Condominios4 = ABS_Condominio.ABSGetCondominios(true);
            Condominios1 = null;
            Condominios2 = null;
            Condominios3 = null;
            Condominios4 = null;
            GC.SuppressFinalize(this);
            Condominios1 = null;
            Condominios2 = null;
            Condominios3 = null;
            Condominios4 = null;
        }

        private void simpleButton2_Click(object sender, EventArgs e)
        {
            cBotaoImpBol3.Enabled = false;
            SortedList<int, Cadastros.Condominios.Condominio> Condominios1 = new SortedList<int, Cadastros.Condominios.Condominio>();
            for (int i = 0; i < 1000; i++)
                Condominios1.Add(i, new Cadastros.Condominios.Condominio(233));
            //for (int i = 0; i < 1000; i++)
            //    Condominios1[i].Dispose();
            Condominios1 = null;
            Condominios1 = new SortedList<int, Cadastros.Condominios.Condominio>();
            for (int i = 0; i < 1000; i++)
                Condominios1.Add(i, new Cadastros.Condominios.Condominio(233));
            //for (int i = 0; i < 1000; i++)
            //    Condominios1[i].Dispose();
            Condominios1 = null;
        }

        private void simpleButton3_Click(object sender, EventArgs e)
        {
            ABS_Apartamento ABS = ABS_Apartamento.GetApartamento(16554);
            Cadastros.Apartamentos.Apartamento Ap = (Cadastros.Apartamentos.Apartamento)ABS;
            memoEdit1.Text += string.Format("Apartamento: {0}\r\nTipo:{1}\r\n", Ap.ToString(), Ap.GetType());
        }

        private void simpleButton4_Click(object sender, EventArgs e)
        {           
            ABS_Apartamento ABS = ABS_Apartamento.GetApartamentoProc(16554);
            CadastrosProc.ApartamentoProc Ap = (CadastrosProc.ApartamentoProc)ABS;
            memoEdit1.Text += string.Format("Apartamento: {0}\r\nTipo:{1}\r\n", Ap.ToString(),Ap.GetType());
        }
    }
}
