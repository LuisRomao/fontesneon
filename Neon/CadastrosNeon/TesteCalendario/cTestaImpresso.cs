﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using DevExpress.XtraReports.UI;
using DevExpress.XtraRichEdit.API.Native;

namespace TesteCalendario
{
    public partial class cTestaImpresso : CompontesBasicos.ComponenteBase
    {
        public cTestaImpresso()
        {
            InitializeComponent();
        }

        private void simpleButton1_Click(object sender, EventArgs e)
        {
            dllImpresso.ImpRTF Impresso = new dllImpresso.ImpRTF();
            System.Collections.SortedList Dados = new System.Collections.SortedList();
            Dados.Add(textEditCampo.Text, "TESTE");
            SortedList<string, DataTable> Tabelas = new SortedList<string, DataTable>();
            DataTable DT = new DataTable();
            Tabelas.Add("Boletos Abertos", DT);
            DT.Columns.Add("C1");
            DT.Columns.Add("C2");
            DT.Columns.Add("C3");
            DT.Columns.Add("C4");
            DT.Columns.Add("C5");
            DT.Rows.Add("L 1 1", "L 1 2", "L 1 3", "L 1 4", "L 1 5");
            DT.Rows.Add("L 2 1", "L 2 2", "L 2 3", "L 2 4", "L 2 5");
            DT.Rows.Add("L 3 1", "L 3 2", "L 3 3", "L 3 4", "L 3 5");
            DT.Rows.Add("L 4 1", "L 4 2", "L 4 3", "L 4 4", "L 4 5");
            DT.Rows.Add("L 5 1", "L 5 2", "L 5 3", "L 5 4", "L 5 5 x xxxxxx xx");
            Impresso.CarregarImpresso(textEditImpresso.Text, Dados,Tabelas);
            Impresso.ShowPreview();
        }

        private void simpleButton2_Click(object sender, EventArgs e)
        {
            dllImpresso.ImpRTF Impresso = new dllImpresso.ImpRTF();
            System.Collections.SortedList Dados = new System.Collections.SortedList();
            Dados.Add(textEditCampo.Text, "TESTE");
            SortedList<string, DataTable> Tabelas = new SortedList<string, DataTable>();
            DataTable DT = new DataTable();
            Tabelas.Add("Boletos Abertos", DT);
            DT.Columns.Add("C1");
            DT.Columns.Add("C2");
            DT.Columns.Add("C3");
            DT.Columns.Add("C4");
            DT.Columns.Add("C5");
            DT.Rows.Add("L 1 1", "L 1 2", "L 1 3", "L 1 4", "L 1 5");
            DT.Rows.Add("L 2 1", "L 2 2", "L 2 3", "L 2 4", "L 2 5");
            DT.Rows.Add("L 3 1", "L 3 2", "L 3 3", "L 3 4", "L 3 5");
            DT.Rows.Add("L 4 1", "L 4 2", "L 4 3", "L 4 4", "L 4 5");
            DT.Rows.Add("L 5 1", "L 5 2", "L 5 3", "L 5 4", "L 5 5 x xxxxxx xx");
            Impresso.PreCarga(textEditImpresso.Text, Dados, Tabelas);
            Impresso.TabelasCriadas["Boletos Abertos"].BeginUpdate();
            Impresso.VirSetLarguras_mm("Boletos Abertos",10, 20, 30, 40, 50);
            Impresso.VirSetCorFundo("Boletos Abertos", Color.Cornsilk, Color.LightSkyBlue, Color.Orchid);
            Impresso.VirSetAlinhamentoCel("Boletos Abertos", ParagraphAlignment.Left, ParagraphAlignment.Center, ParagraphAlignment.Right);
            Impresso.TabelasCriadas["Boletos Abertos"].EndUpdate();
            Impresso.FimCarrega();
            Impresso.ShowPreview();
        }

        private void simpleButton3_Click(object sender, EventArgs e)
        {
            dllImpresso.ImpRTF Impresso = new dllImpresso.ImpRTF();
            System.Collections.SortedList Dados = new System.Collections.SortedList();
            Dados.Add(textEditCampo.Text, "TESTE");
            SortedList<string, DataTable> Tabelas = new SortedList<string, DataTable>();
            DataTable DT = new DataTable();
            Tabelas.Add("Boletos Abertos", DT);
            DT.Columns.Add("C1");
            DT.Columns.Add("C2");
            DT.Columns.Add("C3");
            DT.Columns.Add("C4");
            DT.Columns.Add("C5");
            DT.Rows.Add("L 1 1", "L 1 2", "L 1 3", "L 1 4", "L 1 5");
            DT.Rows.Add("L 2 1", "L 2 2", "L 2 3", "L 2 4", "L 2 5");
            DT.Rows.Add("L 3 1", "L 3 2", "L 3 3", "L 3 4", "L 3 5");
            DT.Rows.Add("L 4 1", "L 4 2", "L 4 3", "L 4 4", "L 4 5");
            DT.Rows.Add("L 5 1", "L 5 2", "L 5 3", "L 5 4", "L 5 5 x xxxxxx xx");
            Impresso.PreCarga(textEditImpresso.Text, Dados, Tabelas,true);
            //Impresso.TabelasCriadas["Boletos Abertos"].BeginUpdate();
            Impresso.VirSetLarguras_mm("Boletos Abertos", 10, 20, 30, 40, 50);
            //Impresso.VirSetCorFundo("Boletos Abertos", Color.Cornsilk, Color.LightSkyBlue, Color.Orchid);            
            //Impresso.TabelasCriadas["Boletos Abertos"].EndUpdate();
            //Impresso.VirSetAlinhamentoCel("Boletos Abertos", ParagraphAlignment.Left, null,null,new List<int>() { 2, 3 }, new List<int>() { 2, 4 });
            //Impresso.VirSetNegrito("Boletos Abertos",true,new List<int>() { 2},new List<int>() { 2 });
            //Impresso.VirSetNegrito("Boletos Abertos", false, new List<int>() { 0 }, new List<int>() { 2 });
            //Impresso.VirSetFonteCel("Boletos Abertos", new Font(FontFamily.GenericMonospace, 10), null, null, new List<int> { 3 }, new List<int> { 3 });
            //Impresso.VirSetFonteCel("Boletos Abertos", new Font(FontFamily.GenericMonospace, 10), null, null, 4, 4);
            Impresso.FimCarrega();
            Impresso.ShowPreview();
        }
    }
}
