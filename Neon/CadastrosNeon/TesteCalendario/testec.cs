﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using DevExpress.XtraReports.UI;

namespace TesteCalendario
{
    public partial class testec : CompontesBasicos.ComponenteBase
    {
        public testec()
        {
            InitializeComponent();
        }

        private void simpleButton1_Click(object sender, EventArgs e)
        {
            Teste(true);
        }

        private void Teste(bool orinal)
        {
            dtesteColunas d1;            
            for (int c = 21; c > 0; c--)
            {
                testecoluna T1 = new testecoluna();
                d1 = new dtesteColunas();
                //d1 = T1.dtesteColunas1;
                d1.TabelaTeste.AddTabelaTesteRow(1, "Linha 1");
                d1.TabelaTeste.AddTabelaTesteRow(2, "Linha 1");
                d1.TabelaTeste.AddTabelaTesteRow(3, "Linha 1");                
                T1.objectDataSource1.DataSource = d1;
                if (orinal)
                {
                    S0.Value = T1.NCorte[0];
                    S1.Value = T1.NCorte[1];
                    S2.Value = T1.NCorte[2];
                    S3.Value = T1.NCorte[3];
                    S4.Value = T1.NCorte[4];
                    S5.Value = T1.NCorte[5];
                }
                else
                {
                    T1.NCorte[0] = (int)S0.Value;
                    T1.NCorte[1] = (int)S1.Value;
                    T1.NCorte[2] = (int)S2.Value;
                    T1.NCorte[3] = (int)S3.Value;
                    T1.NCorte[4] = (int)S4.Value;
                    T1.NCorte[5] = (int)S5.Value;
                }
                T1.xrTitulo.Text = string.Format("Colunas: {0}", c);
                SortedList<string, string> ColunasTeste = new SortedList<string, string>();                
                for (int i = 1; i <= c; i++)
                {                    
                    d1.TabelaTeste.Columns.Add(string.Format("C{0:0000}", i), typeof(decimal), string.Format("ID * {0}", i));
                    ColunasTeste.Add(string.Format("C{0:0000}", i), string.Format("Título {0}", i));
                }
                T1.CriaColunasDinamicas(ColunasTeste);
                T1.CreateDocument();
                T1.ShowPreview();
            }
        }

        private void simpleButton2_Click(object sender, EventArgs e)
        {
            Teste(false);
        }

        private void simpleButton3_Click(object sender, EventArgs e)
        {
            TesteI(true);
        }

        private void simpleButton4_Click(object sender, EventArgs e)
        {
            TesteI(false);
        }

        private void TesteI(bool orinal)
        {   
            /*
            Balancete.Relatorios.Inadimplencia.dInadimplencia d1;
            for (int c = (int)NTestes.Value; c >= 0; c--)
            {
                Balancete.Relatorios.Inadimplencia.ImpInadDet T1 = new Balancete.Relatorios.Inadimplencia.ImpInadDet();
                d1 = new Balancete.Relatorios.Inadimplencia.dInadimplencia();
                d1.Carregar(829, new DateTime(2016, 12, 1), new DateTime(2016, 12, 31));
                T1.objectDataSource1.DataSource = d1;
                if (orinal)
                {
                    S0.Value = T1.NCorte[0];
                    S1.Value = T1.NCorte[1];
                    S2.Value = T1.NCorte[2];
                    S3.Value = T1.NCorte[3];
                    S4.Value = T1.NCorte[4];
                    S5.Value = T1.NCorte[5];
                }
                else
                {
                    T1.NCorte[0] = (int)S0.Value;
                    T1.NCorte[1] = (int)S1.Value;
                    T1.NCorte[2] = (int)S2.Value;
                    T1.NCorte[3] = (int)S3.Value;
                    T1.NCorte[4] = (int)S4.Value;
                    T1.NCorte[5] = (int)S5.Value;
                }
                T1.xrTitulo.Text = string.Format("Colunas: {0}", c);
                SortedList<string, string> ColunasTeste = new SortedList<string, string>();
                for (int i = 0; i < c; i++)
                {
                    if (i >= d1.ColunasDinamicasInad.Count)
                        break;
                    ColunasTeste.Add(d1.ColunasDinamicasInad.Keys[i], d1.ColunasDinamicasInad.Values[i]);
                }
                T1.CriaColunasDinamicas(ColunasTeste);
                T1.CreateDocument();
                T1.ShowPreview();
                
            }
            */
        }
    }
}
