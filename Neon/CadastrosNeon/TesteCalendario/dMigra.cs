﻿namespace TesteCalendario
{


    partial class dMigra
    {

        private dMigraTableAdapters.assembleiasTableAdapter assembleiasTableAdapter;
        /// <summary>
        /// TableAdapter padrão para tabela: Assembleias
        /// </summary>
        public dMigraTableAdapters.assembleiasTableAdapter AssembleiasTableAdapter
        {
            get
            {
                if (assembleiasTableAdapter == null)
                {
                    assembleiasTableAdapter = new dMigraTableAdapters.assembleiasTableAdapter();
                };
                return assembleiasTableAdapter;
            }
        }

        private dMigraTableAdapters.PUBlicacaoTableAdapter pUBlicacaoTableAdapter;
        /// <summary>
        /// TableAdapter padrão para tabela: PUBlicacao
        /// </summary>
        public dMigraTableAdapters.PUBlicacaoTableAdapter PUBlicacaoTableAdapter
        {
            get
            {
                if (pUBlicacaoTableAdapter == null)
                {
                    pUBlicacaoTableAdapter = new dMigraTableAdapters.PUBlicacaoTableAdapter();
                    pUBlicacaoTableAdapter.TrocarStringDeConexao();
                };
                return pUBlicacaoTableAdapter;
            }
        }

        private dMigraTableAdapters.APartamentoDocTableAdapter aPartamentoDocTableAdapter;
        /// <summary>
        /// TableAdapter padrão para tabela: APartamentoDoc
        /// </summary>
        public dMigraTableAdapters.APartamentoDocTableAdapter APartamentoDocTableAdapter
        {
            get
            {
                if (aPartamentoDocTableAdapter == null)
                {
                    aPartamentoDocTableAdapter = new dMigraTableAdapters.APartamentoDocTableAdapter();
                    aPartamentoDocTableAdapter.TrocarStringDeConexao(VirDB.Bancovirtual.TiposDeBanco.Internet2);
                };
                return aPartamentoDocTableAdapter;
            }
        }

        private dMigraTableAdapters.APartamentoDocSBCTableAdapter aPartamentoDocSBCTableAdapter;
        /// <summary>
        /// TableAdapter padrão para tabela: APartamentoDocSBC
        /// </summary>
        public dMigraTableAdapters.APartamentoDocSBCTableAdapter APartamentoDocSBCTableAdapter
        {
            get
            {
                if (aPartamentoDocSBCTableAdapter == null)
                {
                    aPartamentoDocSBCTableAdapter = new dMigraTableAdapters.APartamentoDocSBCTableAdapter();
                    aPartamentoDocSBCTableAdapter.TrocarStringDeConexao();
                };
                return aPartamentoDocSBCTableAdapter;
            }
        }
        private dMigraTableAdapters.APartamentoDocSATableAdapter aPartamentoDocSATableAdapter;
        /// <summary>
        /// TableAdapter padrão para tabela: APartamentoDocSA
        /// </summary>
        public dMigraTableAdapters.APartamentoDocSATableAdapter APartamentoDocSATableAdapter
        {
            get
            {
                if (aPartamentoDocSATableAdapter == null)
                {
                    aPartamentoDocSATableAdapter = new dMigraTableAdapters.APartamentoDocSATableAdapter();
                    aPartamentoDocSATableAdapter.TrocarStringDeConexao(VirDB.Bancovirtual.TiposDeBanco.Filial);
                };
                return aPartamentoDocSATableAdapter;
            }
        }
    }
}
