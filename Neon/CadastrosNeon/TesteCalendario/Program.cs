﻿using System;
using System.Globalization;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Forms;
using VirDB.Bancovirtual;
using Login;
using System.Threading;

namespace TesteCalendario
{
    static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main()
        {
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            BancoVirtual.Popular("QAS SBC", @"Data Source=177.190.193.218\NEON,49259;Initial Catalog=NeonH  ;Persist Security Info=True;User ID=NEONT;Connect Timeout=120;Password=TPC4P2DT");            
            BancoVirtual.Popular("QAS SA", @"Data Source=177.190.193.218\NEON,49259;Initial Catalog=NeonSAH;Persist Security Info=True;User ID=NEONT;Connect Timeout=120;Password=TPC4P2DT");
            BancoVirtual.PopularFilial("QAS SBC", @"Data Source=177.190.193.218\NEON,49259;Initial Catalog=NeonH  ;Persist Security Info=True;User ID=NEONT;Connect Timeout=120;Password=TPC4P2DT");
            BancoVirtual.PopularFilial("QAS SA" , @"Data Source=177.190.193.218\NEON,49259;Initial Catalog=NeonSAH  ;Persist Security Info=True;User ID=NEONT;Connect Timeout=120;Password=TPC4P2DT");
            BancoVirtual.PopularFilial("NEON SBC (cuidado)", @"Data Source=177.190.193.218\NEON;Initial Catalog=NEON;User ID=NEONP;Password=TPC4P2DP;Connect Timeout=30");
            BancoVirtual.PopularFilial("NEON SA  (cuidado)", @"Data Source=177.190.193.218\NEON;Initial Catalog=NEONSA;User ID=NEONP;Password=TPC4P2DP;Connect Timeout=30");
            BancoVirtual.PopularAcces("SBC LOCAL", @"Provider=Microsoft.Jet.OLEDB.4.0;Data Source=C:\clientes\neon\sbc\dados.mdb;Persist Security Info=True;Password=96333018;User ID=NEON;Jet OLEDB:System database=C:\clientes\neon\sbc\neon.mda;Jet OLEDB:Database Password=96333018");
            BancoVirtual.PopularAcces("SA LOCAL", @"Provider=Microsoft.Jet.OLEDB.4.0;Data Source=c:\Clientes\neon\SA\dados.MDB;Persist Security Info=True;User ID=CPD;Password=7778;Jet OLEDB:System database=c:\Clientes\neon\SA\neon.MDA;Jet OLEDB:Database Password=7778");

            BancoVirtual.Popular("MRC - NEON SBC Local", @"Data Source=MARCIA-NOTE\SQL2012;Initial Catalog=Neon;Persist Security Info=True;User ID=sa;Connect Timeout=30;Password=130306");
            BancoVirtual.PopularFilial("MRC - NEON SA Local", @"Data Source=MARCIA-NOTE\SQL2012;Initial Catalog=NeonSA;Persist Security Info=True;User ID=sa;Connect Timeout=30;Password=130306");
            BancoVirtual.Popular("NEON SBC (cuidado)", @"Data Source=177.190.193.218\NEON;Initial Catalog=NEON;User ID=NEONP;Password=TPC4P2DP;Connect Timeout=30");
            BancoVirtual.Popular("NEON SA  (cuidado)", @"Data Source=177.190.193.218\NEON;Initial Catalog=NEONSA;User ID=NEONP;Password=TPC4P2DP;Connect Timeout=30");
            BancoVirtual.PopularInternet("INTERNET REAL2", @"Data Source=177.190.193.218\NEON,49259;Initial Catalog=neonimoveis_2;Persist Security Info=True;User ID=NEONP;Password=TPC4P2DP", 2);

            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            Thread.CurrentThread.CurrentUICulture = new CultureInfo("pt-BR");
            fLogin FLogin = new fLogin();
            FLogin.ShowDialog();
            FLogin.Dispose();
            FLogin = null;
            Application.Run(new Form1());
            //Application.Run(new FormTesteBotao());
        }
    }
}
