﻿namespace TesteCalendario
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            CompontesBasicos.ModuleInfo moduleInfo1 = new CompontesBasicos.ModuleInfo();
            CompontesBasicos.ModuleInfo moduleInfo2 = new CompontesBasicos.ModuleInfo();
            CompontesBasicos.ModuleInfo moduleInfo3 = new CompontesBasicos.ModuleInfo();
            CompontesBasicos.ModuleInfo moduleInfo4 = new CompontesBasicos.ModuleInfo();
            CompontesBasicos.ModuleInfo moduleInfo5 = new CompontesBasicos.ModuleInfo();
            CompontesBasicos.ModuleInfo moduleInfo6 = new CompontesBasicos.ModuleInfo();
            CompontesBasicos.ModuleInfo moduleInfo7 = new CompontesBasicos.ModuleInfo();
            CompontesBasicos.ModuleInfo moduleInfo8 = new CompontesBasicos.ModuleInfo();
            CompontesBasicos.ModuleInfo moduleInfo9 = new CompontesBasicos.ModuleInfo();
            CompontesBasicos.ModuleInfo moduleInfo10 = new CompontesBasicos.ModuleInfo();
            CompontesBasicos.ModuleInfo moduleInfo11 = new CompontesBasicos.ModuleInfo();
            CompontesBasicos.ModuleInfo moduleInfo12 = new CompontesBasicos.ModuleInfo();
            CompontesBasicos.ModuleInfo moduleInfo13 = new CompontesBasicos.ModuleInfo();
            CompontesBasicos.ModuleInfo moduleInfo14 = new CompontesBasicos.ModuleInfo();
            CompontesBasicos.ModuleInfo moduleInfo15 = new CompontesBasicos.ModuleInfo();
            this.navBarGroup1 = new DevExpress.XtraNavBar.NavBarGroup();
            this.navBarGroup2 = new DevExpress.XtraNavBar.NavBarGroup();
            this.navBarGroup3 = new DevExpress.XtraNavBar.NavBarGroup();
            this.navBarGroup4 = new DevExpress.XtraNavBar.NavBarGroup();
            this.navBarGroup5 = new DevExpress.XtraNavBar.NavBarGroup();
            this.navBarGroup6 = new DevExpress.XtraNavBar.NavBarGroup();
            this.navBarItem1 = new DevExpress.XtraNavBar.NavBarItem();
            this.navBarGroup7 = new DevExpress.XtraNavBar.NavBarGroup();
            this.navBarGroup8 = new DevExpress.XtraNavBar.NavBarGroup();
            ((System.ComponentModel.ISupportInitialize)(this.PictureEdit_F.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.NavBarControl_F)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.BarAndDockingController_F)).BeginInit();
            this.DockPanel_FContainer.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.TabControl_F)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.BarManager_F)).BeginInit();
            this.DockPanel_F.SuspendLayout();
            this.SuspendLayout();
            // 
            // PictureEdit_F
            // 
            this.PictureEdit_F.Cursor = System.Windows.Forms.Cursors.Default;
            // 
            // NavBarControl_F
            // 
            this.NavBarControl_F.ActiveGroup = this.navBarGroup1;
            this.NavBarControl_F.Groups.AddRange(new DevExpress.XtraNavBar.NavBarGroup[] {
            this.navBarGroup1,
            this.navBarGroup2,
            this.navBarGroup3,
            this.navBarGroup4,
            this.navBarGroup5,
            this.navBarGroup6,
            this.navBarGroup7,
            this.navBarGroup8});
            this.NavBarControl_F.Items.AddRange(new DevExpress.XtraNavBar.NavBarItem[] {
            this.navBarItem1});
            this.NavBarControl_F.OptionsNavPane.ExpandedWidth = 194;
            this.NavBarControl_F.Size = new System.Drawing.Size(194, 430);
            // 
            // BarAndDockingController_F
            // 
            this.BarAndDockingController_F.PropertiesBar.AllowLinkLighting = false;
            this.BarAndDockingController_F.PropertiesBar.DefaultGlyphSize = new System.Drawing.Size(16, 16);
            this.BarAndDockingController_F.PropertiesBar.DefaultLargeGlyphSize = new System.Drawing.Size(32, 32);
            // 
            // DefaultLookAndFeel_F
            // 
            this.DefaultLookAndFeel_F.LookAndFeel.SkinName = "The Asphalt World";
            this.DefaultLookAndFeel_F.LookAndFeel.Style = DevExpress.LookAndFeel.LookAndFeelStyle.Flat;
            this.DefaultLookAndFeel_F.LookAndFeel.UseWindowsXPTheme = true;
            // 
            // DefaultToolTipController_F
            // 
            // 
            // 
            // 
            this.DefaultToolTipController_F.DefaultController.AutoPopDelay = 10000;
            this.DefaultToolTipController_F.DefaultController.Rounded = true;
            // 
            // barStatus_F
            // 
            this.barStatus_F.OptionsBar.AllowQuickCustomization = false;
            this.barStatus_F.OptionsBar.DisableClose = true;
            this.barStatus_F.OptionsBar.DisableCustomization = true;
            this.barStatus_F.OptionsBar.DrawDragBorder = false;
            this.barStatus_F.OptionsBar.DrawSizeGrip = true;
            this.barStatus_F.OptionsBar.UseWholeRow = true;
            // 
            // DockPanel_FContainer
            // 
            this.DefaultToolTipController_F.SetAllowHtmlText(this.DockPanel_FContainer, DevExpress.Utils.DefaultBoolean.Default);
            this.DockPanel_FContainer.Location = new System.Drawing.Point(3, 20);
            this.DockPanel_FContainer.Size = new System.Drawing.Size(194, 511);
            // 
            // DockPanel_F
            // 
            this.DockPanel_F.Appearance.BackColor = System.Drawing.Color.White;
            this.DockPanel_F.Appearance.Options.UseBackColor = true;
            this.DockPanel_F.Options.AllowDockFill = false;
            this.DockPanel_F.Options.ShowCloseButton = false;
            this.DockPanel_F.Options.ShowMaximizeButton = false;
            // 
            // navBarGroup1
            // 
            this.navBarGroup1.Caption = "navBarGroup1";
            this.navBarGroup1.Name = "navBarGroup1";
            // 
            // navBarGroup2
            // 
            this.navBarGroup2.Caption = "Condomínios";
            this.navBarGroup2.Name = "navBarGroup2";
            // 
            // navBarGroup3
            // 
            this.navBarGroup3.Caption = "Mala Direta";
            this.navBarGroup3.Name = "navBarGroup3";
            // 
            // navBarGroup4
            // 
            this.navBarGroup4.Caption = "Pessoas";
            this.navBarGroup4.Name = "navBarGroup4";
            // 
            // navBarGroup5
            // 
            this.navBarGroup5.Caption = "Objetos";
            this.navBarGroup5.Name = "navBarGroup5";
            // 
            // navBarGroup6
            // 
            this.navBarGroup6.Caption = "Botão";
            this.navBarGroup6.ItemLinks.AddRange(new DevExpress.XtraNavBar.NavBarItemLink[] {
            new DevExpress.XtraNavBar.NavBarItemLink(this.navBarItem1)});
            this.navBarGroup6.Name = "navBarGroup6";
            // 
            // navBarItem1
            // 
            this.navBarItem1.Caption = "Tela de teste";
            this.navBarItem1.Name = "navBarItem1";
            this.navBarItem1.LinkClicked += new DevExpress.XtraNavBar.NavBarLinkEventHandler(this.navBarItem1_LinkClicked);
            // 
            // navBarGroup7
            // 
            this.navBarGroup7.Caption = "Impressos";
            this.navBarGroup7.Name = "navBarGroup7";
            // 
            // navBarGroup8
            // 
            this.navBarGroup8.Caption = "Fornecedores";
            this.navBarGroup8.Name = "navBarGroup8";
            // 
            // Form1
            // 
            this.DefaultToolTipController_F.SetAllowHtmlText(this, DevExpress.Utils.DefaultBoolean.Default);
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(782, 556);
            moduleInfo1.Grupo = 0;
            moduleInfo1.ImagemG = null;
            moduleInfo1.ImagemP = null;
            moduleInfo1.ModuleType = null;
            moduleInfo1.ModuleTypestr = "Calendario.StatusGeral.cStatusGeral";
            moduleInfo1.Name = "Mapa Geral";
            moduleInfo2.Grupo = 0;
            moduleInfo2.ImagemG = null;
            moduleInfo2.ImagemP = null;
            moduleInfo2.ModuleType = null;
            moduleInfo2.ModuleTypestr = "dllCheques.cChequesEmitir";
            moduleInfo2.Name = null;
            moduleInfo3.Grupo = 1;
            moduleInfo3.ImagemG = null;
            moduleInfo3.ImagemP = null;
            moduleInfo3.ModuleType = null;
            moduleInfo3.ModuleTypestr = "Cadastros.Condominios.cCondominiosGrade";
            moduleInfo3.Name = "Condomínios";
            moduleInfo4.Grupo = 2;
            moduleInfo4.ImagemG = null;
            moduleInfo4.ImagemP = null;
            moduleInfo4.ModuleType = null;
            moduleInfo4.ModuleTypestr = "maladireta.cMalaDireta";
            moduleInfo4.Name = "Masla direta";
            moduleInfo5.Grupo = 3;
            moduleInfo5.ImagemG = null;
            moduleInfo5.ImagemP = null;
            moduleInfo5.ModuleType = null;
            moduleInfo5.ModuleTypestr = "Cadastros.Pessoas.PesquisaPessoas";
            moduleInfo5.Name = "Prop/Inq";
            moduleInfo6.Grupo = 4;
            moduleInfo6.ImagemG = null;
            moduleInfo6.ImagemP = null;
            moduleInfo6.ModuleType = null;
            moduleInfo6.ModuleTypestr = "TesteCalendario.cTestaFornecedor";
            moduleInfo6.Name = "Fornecedor";
            moduleInfo7.Grupo = 4;
            moduleInfo7.ImagemG = null;
            moduleInfo7.ImagemP = null;
            moduleInfo7.ModuleType = null;
            moduleInfo7.ModuleTypestr = "TesteCalendario.testec";
            moduleInfo7.Name = "Teste Colunas";
            moduleInfo8.Grupo = 0;
            moduleInfo8.ImagemG = null;
            moduleInfo8.ImagemP = null;
            moduleInfo8.ModuleType = null;
            moduleInfo8.ModuleTypestr = "TesteCalendario.UserControl1";
            moduleInfo8.Name = "Teste botão";
            moduleInfo9.Grupo = 0;
            moduleInfo9.ImagemG = null;
            moduleInfo9.ImagemP = null;
            moduleInfo9.ModuleType = null;
            moduleInfo9.ModuleTypestr = "Calendario.Agenda.cAgendaGrade";
            moduleInfo9.Name = "Agenda";
            moduleInfo10.Grupo = 6;
            moduleInfo10.ImagemG = null;
            moduleInfo10.ImagemP = null;
            moduleInfo10.ModuleType = null;
            moduleInfo10.ModuleTypestr = "TesteCalendario.cTestaImpresso";
            moduleInfo10.Name = "Teste Impresso";
            moduleInfo11.Grupo = 7;
            moduleInfo11.ImagemG = null;
            moduleInfo11.ImagemP = null;
            moduleInfo11.ModuleType = null;
            moduleInfo11.ModuleTypestr = "Cadastros.Fornecedores.cFornecedoresGradeBase";
            moduleInfo11.Name = "Fornecedores";
            moduleInfo12.Grupo = 0;
            moduleInfo12.ImagemG = null;
            moduleInfo12.ImagemP = null;
            moduleInfo12.ModuleType = null;
            moduleInfo12.ModuleTypestr = "Cadastros.cConfiguraEstacao";
            moduleInfo12.Name = "Configura estação";
            moduleInfo13.Grupo = 6;
            moduleInfo13.ImagemG = null;
            moduleInfo13.ImagemP = null;
            moduleInfo13.ModuleType = null;
            moduleInfo13.ModuleTypestr = "maladireta.EditorRTF.cGradeImpressos";
            moduleInfo13.Name = "Edita impressos";
            moduleInfo14.Grupo = 0;
            moduleInfo14.ImagemG = null;
            moduleInfo14.ImagemP = null;
            moduleInfo14.ModuleType = null;
            moduleInfo14.ModuleTypestr = "Cadastros.seguroconteudo.cPlanosSeguroConteudo";
            moduleInfo14.Name = "Seguro Conteúdo";
            moduleInfo15.Grupo = 0;
            moduleInfo15.ImagemG = null;
            moduleInfo15.ImagemP = null;
            moduleInfo15.ModuleType = typeof(Cadastros.Faturamento.cInsumos);
            moduleInfo15.ModuleTypestr = "Cadastros.Faturamento.cInsumos";
            moduleInfo15.Name = "Insumos";
            this.ModuleInfoCollection.AddRange(new CompontesBasicos.ModuleInfo[] {
            moduleInfo1,
            moduleInfo2,
            moduleInfo3,
            moduleInfo4,
            moduleInfo5,
            moduleInfo6,
            moduleInfo7,
            moduleInfo8,
            moduleInfo9,
            moduleInfo10,
            moduleInfo11,
            moduleInfo12,
            moduleInfo13,
            moduleInfo14,
            moduleInfo15});
            this.Name = "Form1";
            this.Text = "Form1";
            ((System.ComponentModel.ISupportInitialize)(this.PictureEdit_F.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.NavBarControl_F)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.BarAndDockingController_F)).EndInit();
            this.DockPanel_FContainer.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.TabControl_F)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.BarManager_F)).EndInit();
            this.DockPanel_F.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private DevExpress.XtraNavBar.NavBarGroup navBarGroup1;
        private DevExpress.XtraNavBar.NavBarGroup navBarGroup2;
        private DevExpress.XtraNavBar.NavBarGroup navBarGroup3;
        private DevExpress.XtraNavBar.NavBarGroup navBarGroup4;
        private DevExpress.XtraNavBar.NavBarGroup navBarGroup5;
        private DevExpress.XtraNavBar.NavBarGroup navBarGroup6;
        private DevExpress.XtraNavBar.NavBarItem navBarItem1;
        private DevExpress.XtraNavBar.NavBarGroup navBarGroup7;
        private DevExpress.XtraNavBar.NavBarGroup navBarGroup8;
    }
}

