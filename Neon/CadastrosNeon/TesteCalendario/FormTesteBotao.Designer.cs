﻿namespace TesteCalendario
{
    partial class FormTesteBotao
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.cBotaoImpBol1 = new dllImpresso.Botoes.cBotaoImpBol();
            this.cBotaoImpBol2 = new dllImpresso.Botoes.cBotaoImpBol();
            this.cBotaoImpBol3 = new dllImpresso.Botoes.cBotaoImpBol();
            this.simpleButton1 = new DevExpress.XtraEditors.SimpleButton();
            this.simpleButton2 = new DevExpress.XtraEditors.SimpleButton();
            this.simpleButton3 = new DevExpress.XtraEditors.SimpleButton();
            this.textEdit1 = new DevExpress.XtraEditors.TextEdit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit1.Properties)).BeginInit();
            this.SuspendLayout();
            // 
            // cBotaoImpBol1
            // 
            this.cBotaoImpBol1.ItemComprovante = true;
            this.cBotaoImpBol1.Location = new System.Drawing.Point(113, 77);
            this.cBotaoImpBol1.Name = "cBotaoImpBol1";
            this.cBotaoImpBol1.Size = new System.Drawing.Size(120, 26);
            this.cBotaoImpBol1.TabIndex = 0;
            this.cBotaoImpBol1.Titulo = "teste 1";
            // 
            // cBotaoImpBol2
            // 
            this.cBotaoImpBol2.Enabled = false;
            this.cBotaoImpBol2.ItemComprovante = true;
            this.cBotaoImpBol2.Location = new System.Drawing.Point(113, 134);
            this.cBotaoImpBol2.Name = "cBotaoImpBol2";
            this.cBotaoImpBol2.Size = new System.Drawing.Size(120, 26);
            this.cBotaoImpBol2.TabIndex = 1;
            this.cBotaoImpBol2.Titulo = "Teste 2";
            // 
            // cBotaoImpBol3
            // 
            this.cBotaoImpBol3.ItemComprovante = true;
            this.cBotaoImpBol3.Location = new System.Drawing.Point(113, 275);
            this.cBotaoImpBol3.Name = "cBotaoImpBol3";
            this.cBotaoImpBol3.Size = new System.Drawing.Size(120, 26);
            this.cBotaoImpBol3.TabIndex = 2;
            this.cBotaoImpBol3.Titulo = "Teste 3";
            // 
            // simpleButton1
            // 
            this.simpleButton1.Location = new System.Drawing.Point(311, 252);
            this.simpleButton1.Name = "simpleButton1";
            this.simpleButton1.Size = new System.Drawing.Size(75, 23);
            this.simpleButton1.TabIndex = 3;
            this.simpleButton1.Text = "Liga";
            this.simpleButton1.Click += new System.EventHandler(this.simpleButton1_Click);
            // 
            // simpleButton2
            // 
            this.simpleButton2.Location = new System.Drawing.Point(311, 311);
            this.simpleButton2.Name = "simpleButton2";
            this.simpleButton2.Size = new System.Drawing.Size(75, 23);
            this.simpleButton2.TabIndex = 4;
            this.simpleButton2.Text = "Desliga";
            this.simpleButton2.Click += new System.EventHandler(this.simpleButton2_Click);
            // 
            // simpleButton3
            // 
            this.simpleButton3.Location = new System.Drawing.Point(167, 486);
            this.simpleButton3.Name = "simpleButton3";
            this.simpleButton3.Size = new System.Drawing.Size(256, 23);
            this.simpleButton3.TabIndex = 5;
            this.simpleButton3.Text = "Busca Sindico";
            this.simpleButton3.Click += new System.EventHandler(this.simpleButton3_Click);
            // 
            // textEdit1
            // 
            this.textEdit1.Location = new System.Drawing.Point(495, 489);
            this.textEdit1.Name = "textEdit1";
            this.textEdit1.Size = new System.Drawing.Size(264, 20);
            this.textEdit1.TabIndex = 6;
            // 
            // FormTesteBotao
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(802, 626);
            this.Controls.Add(this.textEdit1);
            this.Controls.Add(this.simpleButton3);
            this.Controls.Add(this.simpleButton2);
            this.Controls.Add(this.simpleButton1);
            this.Controls.Add(this.cBotaoImpBol3);
            this.Controls.Add(this.cBotaoImpBol2);
            this.Controls.Add(this.cBotaoImpBol1);
            this.Name = "FormTesteBotao";
            this.Text = "FormTesteBotao";
            ((System.ComponentModel.ISupportInitialize)(this.textEdit1.Properties)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private dllImpresso.Botoes.cBotaoImpBol cBotaoImpBol1;
        private dllImpresso.Botoes.cBotaoImpBol cBotaoImpBol2;
        private dllImpresso.Botoes.cBotaoImpBol cBotaoImpBol3;
        private DevExpress.XtraEditors.SimpleButton simpleButton1;
        private DevExpress.XtraEditors.SimpleButton simpleButton2;
        private DevExpress.XtraEditors.SimpleButton simpleButton3;
        private DevExpress.XtraEditors.TextEdit textEdit1;
    }
}