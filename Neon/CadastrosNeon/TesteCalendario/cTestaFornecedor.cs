﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using AbstratosNeon;

namespace TesteCalendario
{
    public partial class cTestaFornecedor : CompontesBasicos.ComponenteBase
    {
        

        public cTestaFornecedor()
        {
            InitializeComponent();
        }

        private void simpleButton1_Click(object sender, EventArgs e)
        {
            ABS_Fornecedor Fornecedor;
            DocBacarios.CPFCNPJ cnpj = new DocBacarios.CPFCNPJ(textEdit1.Text);
            if (cnpj.Tipo != DocBacarios.TipoCpfCnpj.INVALIDO)
            {
                Fornecedor = ABS_Fornecedor.ABSGetFornecedor(cnpj, true);
                if (Fornecedor != null)
                {
                    MessageBox.Show(string.Format("Achei:{0}",Fornecedor.FRN));
                }
                else
                {
                    MessageBox.Show("Não Achei");
                }
            }
        }

        private void simpleButton2_Click(object sender, EventArgs e)
        {
            Cadastros.Condominios.Condominio C = new Cadastros.Condominios.Condominio(824);
            textEdit1.Text = C.Conta.CaixaPostal;
        }

        

        private void simpleButton3_Click(object sender, EventArgs e)
        {
            dMigra.APartamentoDocTableAdapter.FillGeral(dMigra.APartamentoDoc);
            dMigra.APartamentoDocSBCTableAdapter.Fill(dMigra.APartamentoDocSBC);
            dMigra.APartamentoDocSATableAdapter.Fill(dMigra.APartamentoDocSA);
        }

        private void simpleButton4_Click(object sender, EventArgs e)
        {
            int Pulados = 0;
            int Gravados = 0;
            int Erro = 0;
            int Total = dMigra.APartamentoDocTableAdapter.Fill(dMigra.APartamentoDoc, checkEditSBC.Checked ? 1 : 3);
            dMigra.APartamentoDocSBCTableAdapter.Fill(dMigra.APartamentoDocSBC);
            dMigra.APartamentoDocSATableAdapter.Fill(dMigra.APartamentoDocSA);
            int contador = 0;
            foreach (dMigra.APartamentoDocRow rowOrigem in dMigra.APartamentoDoc)
            {
                contador++;
                if (contador % 100 == 0)
                {
                    labelControl1.Text = string.Format("Pulados {0:n0} Gravados {1:n0} Erro {2:n0} {3:n0}/{4:n0}", Pulados, Gravados,Erro, contador, Total);
                    Application.DoEvents();
                }
                if (checkEditSBC.Checked)
                {
                    try
                    {
                        if (dMigra.APartamentoDocSBC.FindByAPD(rowOrigem.APD) == null)
                        {
                            Gravados++;
                            dMigra.APartamentoDocSBCTableAdapter.Insert("APartamentoDoc", rowOrigem, true, false);
                        }
                        else
                            Pulados++;
                    }
                    catch (Exception)
                    {
                        Erro++;
                    }
                }
                else
                {
                    try
                    {
                        if (dMigra.APartamentoDocSA.FindByAPD(rowOrigem.APD) == null)
                        {
                            Gravados++;
                            dMigra.APartamentoDocSATableAdapter.Insert("APartamentoDoc", rowOrigem, true, false);
                        }
                        else
                            Pulados++;
                    }
                    catch (Exception)
                    {
                        Erro++;
                    }
                }
            }
            labelControl1.Text = string.Format("parado. Pulados {0:n0} Gravados {1:n0} Erro {2:n0} {3:n0}/{4:n0}", Pulados, Gravados,Erro, contador, Total); 
        }

        private void simpleButton5_Click(object sender, EventArgs e)
        {
            dMigra.AssembleiasTableAdapter.FillByEMP(dMigra.assembleias, Framework.DSCentral.EMP == 1 ? 1 : 2);
            dMigra.PUBlicacaoTableAdapter.Fill(dMigra.PUBlicacao);            
            simpleButtonMigra.Visible = true;
        }

        private void simpleButtonMigra_Click(object sender, EventArgs e)
        {
            int Pulados = 0;
            int Gravados = 0;
            int Erro = 0;
            int Total = dMigra.assembleias.Count;
            int contador = 0;
            foreach (dMigra.assembleiasRow rowOrigem in dMigra.assembleias)
            {
                contador++;
                if (contador % 100 == 0)
                {
                    labelControl2.Text = string.Format("Pulados {0:n0} Gravados {1:n0} Erro {2:n0} {3:n0}/{4:n0}", Pulados, Gravados, Erro, contador, Total);
                    Application.DoEvents();
                }
                try
                {
                    if (dMigra.PUBlicacao.FindByPUB(rowOrigem.PUB) == null)
                    {
                        int? CON = VirMSSQL.TableAdapter.STTableAdapter.BuscaEscalar_int("select CON from condominios where concodigo = @P1", rowOrigem.PUB_CODCON);
                        if (!CON.HasValue)
                            Erro++;
                        else
                        {
                            Gravados++;
                            if (rowOrigem.IsPUBExtensaoNull())
                                rowOrigem.PUBExtensao = ".html";
                            if (rowOrigem.IsPUBDescritivoNull())
                                rowOrigem.PUBDescritivo = "";
                            rowOrigem.PUBAtiva = true;
                            rowOrigem.PUBDATAI = DateTime.Now;
                            rowOrigem.PUBI_USU = 30;
                            rowOrigem.PUB_CON = CON.Value;
                            dMigra.APartamentoDocSBCTableAdapter.Insert("Publicacao", rowOrigem, true, false);
                        }
                    }
                    else
                        Pulados++;
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message);
                    Erro++;
                }
            }
            labelControl2.Text = string.Format("parado. Pulados {0:n0} Gravados {1:n0} Erro {2:n0} {3:n0}/{4:n0}", Pulados, Gravados, Erro, contador, Total);
        }
    }
}
