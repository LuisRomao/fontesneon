﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using CompontesBasicos;

namespace Cadastros.Indices
{
    /// <summary>
    /// Cadastro de INPC
    /// </summary>
    public partial class cINPC : ComponenteBase
    {
        /// <summary>
        /// Construtor
        /// </summary>
        public cINPC()
        {
            InitializeComponent();
            dINPC.INPCTableAdapter.Fill(dINPC.INPC);
        }

        
    }
}
