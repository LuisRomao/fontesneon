﻿namespace Cadastros.Indices {
    
    /// <summary>
    /// dINPC
    /// </summary>
    public partial class dINPC 
    {
        private dINPCTableAdapters.SAlarioMinimoTableAdapter sAlarioMinimoTableAdapter;
        /// <summary>
        /// TableAdapter padrão para tabela: SAlarioMinimo
        /// </summary>
        public dINPCTableAdapters.SAlarioMinimoTableAdapter SAlarioMinimoTableAdapter
        {
            get
            {
                if (sAlarioMinimoTableAdapter == null)
                {
                    sAlarioMinimoTableAdapter = new dINPCTableAdapters.SAlarioMinimoTableAdapter();
                    sAlarioMinimoTableAdapter.TrocarStringDeConexao();
                };
                return sAlarioMinimoTableAdapter;
            }
        }

        private dINPCTableAdapters.INPCTableAdapter iNPCTableAdapter;
        /// <summary>
        /// TableAdapter padrão para tabela: INPC
        /// </summary>
        public dINPCTableAdapters.INPCTableAdapter INPCTableAdapter
        {
            get
            {
                if (iNPCTableAdapter == null)
                {
                    iNPCTableAdapter = new dINPCTableAdapters.INPCTableAdapter();
                    iNPCTableAdapter.TrocarStringDeConexao();
                };
                return iNPCTableAdapter;
            }
        }
    }
}
