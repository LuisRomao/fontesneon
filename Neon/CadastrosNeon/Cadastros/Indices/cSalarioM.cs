﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using CompontesBasicos;

namespace Cadastros.Indices
{
    /// <summary>
    /// salário mínimo
    /// </summary>
    public partial class cSalarioM : ComponenteBase
    {
        /// <summary>
        /// Construtor
        /// </summary>
        public cSalarioM()
        {
            InitializeComponent();
            dINPC.SAlarioMinimoTableAdapter.Fill(dINPC.SAlarioMinimo);
            if (CompontesBasicos.Bancovirtual.UsuarioLogado.UsuarioLogadoSt.VerificaFuncionalidade("CADASTRO") > 3)
                colSAMValor.OptionsColumn.ReadOnly = false;
        }

        private void gridView1_CustomColumnDisplayText(object sender, DevExpress.XtraGrid.Views.Base.CustomColumnDisplayTextEventArgs e)
        {
            try
            {
                if (e.Column.Equals(colSAMCompetencia))
                {
                    Framework.objetosNeon.Competencia Comp = new Framework.objetosNeon.Competencia((int)e.Value);
                    e.DisplayText = Comp.ToString();
                }
            }
            catch { }
        }

        private void gridView1_RowUpdated(object sender, DevExpress.XtraGrid.Views.Base.RowObjectEventArgs e)
        {
            DataRowView DRV = (DataRowView)e.Row;
            if ((DRV == null) || (DRV.Row == null))
                return;
            dINPC.SAlarioMinimoRow row = (dINPC.SAlarioMinimoRow)DRV.Row;
            if (row.SAMCompetencia >= new Framework.objetosNeon.Competencia().CompetenciaBind)
            {
                dINPC.SAlarioMinimoTableAdapter.Update(row);
                row.AcceptChanges();
            }
            else
                row.RejectChanges();
        }
    }
}
