using System;

namespace Cadastros.Pessoas
{
    /// <summary>
    /// 
    /// </summary>
    public partial class PesquisaPessoas : CompontesBasicos.ComponenteBase
    {
        /// <summary>
        /// 
        /// </summary>
        public PesquisaPessoas()
        {
            InitializeComponent();
            //this.Dock = DockStyle.Fill;                               
        }

        private void lookupBlocosAptos_F1_alterado(object sender, EventArgs e)
        {
            if (lookupBlocosAptos_F1.CON_sel > 0)
            {
                cPessoasGrade1.complementoWhere = " CONDOMINIOS.CON = " + lookupBlocosAptos_F1.CON_sel.ToString();

                if (lookupBlocosAptos_F1.BLO_Sel > 0)
                {
                    cPessoasGrade1.complementoWhere += " AND BLOCOS.BLO = " + lookupBlocosAptos_F1.BLO_Sel.ToString();

                    if (lookupBlocosAptos_F1.APT_Sel > 0)
                    {
                        cPessoasGrade1.complementoWhere += " AND APARTAMENTOS.APT = " + lookupBlocosAptos_F1.APT_Sel.ToString();
                    }
                }

            }
            else
                cPessoasGrade1.complementoWhere = "";
            cPessoasGrade1.Pesquisar();
        }
       
        private void PesquisaPessoas_Load(object sender, EventArgs e)
        {
            if (!this.DesignMode)
            {
             //   cPessoasGrade1.Pesquisar();
                cPessoasGrade1.Ativado = true;
            };
        }

        /// <summary>
        /// 
        /// </summary>
        public override void RefreshDados()
        {
            cPessoasGrade1.RefreshDados();
        }
    }
}

