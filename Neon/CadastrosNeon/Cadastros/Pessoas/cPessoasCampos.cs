using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Windows.Forms;
using Cadastros.Bancos;
using Cadastros.Cidades;
using Cadastros.Fornecedores;
using DocBacarios;

namespace Cadastros.Pessoas
{
    /// <summary>
    /// 
    /// </summary>
    public partial class cPessoasCampos : CompontesBasicos.ComponenteCamposBase
    {
        /// <summary>
        /// 
        /// </summary>
        public cPessoasCampos()
        {
            InitializeComponent();
        }

  //      protected override void ConfirmarRegistro_F()
    //    {
      //      pESSOASTableAdapter.Update(dPessoasCampos.PESSOAS);
        //}

        private void PopulaTipoDePessoas()
        {
            Object _NewObject = tIPOSBindingSource.AddNew();

            ((DataRowView)(_NewObject)).BeginEdit();
            ((DataRowView)(_NewObject))["TPODescricao"] = "F�SICA";
            ((DataRowView)(_NewObject))["TPO"] = "F";
            ((DataRowView)(_NewObject)).EndEdit();

            _NewObject = tIPOSBindingSource.AddNew();

            ((DataRowView)(_NewObject)).BeginEdit();
            ((DataRowView)(_NewObject))["TPODescricao"] = "JUR�DICA";
            ((DataRowView)(_NewObject))["TPO"] = "J";
            ((DataRowView)(_NewObject)).EndEdit();
        }

        private void cPessoasCampos_Load(object sender, EventArgs e)
        {
            if (!DesignMode)
            {
                PopulaTipoDePessoas();

                //cIDADESTableAdapter.Fill(dPessoasCampos.CIDADES);
                cIDADESBindingSource.DataSource = Framework.datasets.dCIDADES.dCIDADESSt;
                fORNECEDORESTableAdapter.Fill(dPessoasCampos.FORNECEDORES);
                bANCOSTableAdapter.Fill(dPessoasCampos.BANCOS);

                tcgPessoas.SelectedTabPage = lcgBasico;
                EdNome.Select(EdNome.Text.Length, 0);
                dPessoasCampos.Carrega(pk);
                CPFCNPJ cpf = new CPFCNPJ(txtCnpjCpf.Text);
                if ((cpf.Tipo != TipoCpfCnpj.INVALIDO) && (cpf.Valor != 0))
                {
                    if ((LinhaMae != null) && (LinhaMae.PESCpfCnpjViaSite))
                    {
                        txtCnpjCpf.BackColor = LinhaMae.PESCpfErrado ? System.Drawing.Color.Pink : System.Drawing.Color.Yellow;
                        txtCnpjCpf.Properties.Buttons[0].Visible = !somenteleitura;
                        txtCnpjCpf.Properties.Buttons[1].Visible = !somenteleitura;
                        txtCnpjCpf.Properties.ReadOnly = somenteleitura;
                    }
                    else
                        txtCnpjCpf.Properties.ReadOnly = true;
                }
                if (!somenteleitura)
                {
                    if (dPessoasCampos.nCorreios > 0)
                    {
                        MessageBox.Show("Existem outras unidades que enviam boletos para este endere�o.", "A T E N � � O");
                        tcgPessoas.SelectedTabPage = lcgUnidades;
                    }
                }
                else
                {
                    //colocar readonly em cada componente
                }
            };
        }

        private void txtCnpjCpf_ButtonClick(object sender, DevExpress.XtraEditors.Controls.ButtonPressedEventArgs e)
        {
            if (e.Button.Kind == DevExpress.XtraEditors.Controls.ButtonPredefines.OK)
            {
                if (MessageBox.Show(string.Format("Confirma que o CPF/CNPJ foi checado e pertence a {0} ?", LinhaMae.PESNome), "CONFIRMA��O", MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button2) == DialogResult.Yes)
                {
                    LinhaMae.PESCpfCnpjViaSite = LinhaMae.PESCpfErrado = false;
                    LinhaMae.PESHistorico = string.Format("{0}{1} CPF Verificado por {2}\r\n",
                                                          LinhaMae.IsPESHistoricoNull() ? "" : LinhaMae.PESHistorico + Environment.NewLine,
                                                          DateTime.Now,
                                                          Framework.DSCentral.USUNome);
                    txtCnpjCpf.BackColor = System.Drawing.Color.Lime;
                    txtCnpjCpf.Properties.Buttons[0].Visible = false;
                    txtCnpjCpf.Properties.Buttons[1].Visible = false;
                }
            }
            else
            {
                LinhaMae.PESCpfCnpjViaSite = LinhaMae.PESCpfErrado = true;
                LinhaMae.PESHistorico = string.Format("{0}{1} CPF marcado com inv�lido por {2}\r\n",
                                                      LinhaMae.IsPESHistoricoNull() ? "" : LinhaMae.PESHistorico + Environment.NewLine,
                                                      DateTime.Now,
                                                      Framework.DSCentral.USUNome);
                txtCnpjCpf.BackColor = System.Drawing.Color.Pink;                
            }
        }

        private void lkpCidades_ButtonClick(object sender, DevExpress.XtraEditors.Controls.ButtonPressedEventArgs e)
        {
            if (e.Button.Index == 1)//Incluir
            {
                Int32 _PK = ShowModuloAdd(typeof(cCidadesGrade));

                if (_PK > 0)
                {
                    //cIDADESTableAdapter.Fill(dPessoasCampos.CIDADES);
                    Framework.datasets.dCIDADES.dCIDADESSt.RefreshDados();
                    lkpCidades.EditValue = _PK;
                }
            }
        }

        private void lkpCartorios_ButtonClick(object sender, DevExpress.XtraEditors.Controls.ButtonPressedEventArgs e)
        {
            if (e.Button.Index == 1) //Adicionar
            {
                Int32 pk = ShowModuloAdd(typeof(cFornecedoresCampos), "FRNTipo", "CAR");

                if (pk > 0)
                {
                    fORNECEDORESTableAdapter.Fill(dPessoasCampos.FORNECEDORES);
                    lkpCartorios.EditValue = pk;
                }
            }
            else
                if ((e.Button.Index == 2) && (lkpCartorios.EditValue != null))//Visualizar
                    ShowModulo(typeof(Fornecedores.cFornecedoresGradeBase), true);
        }

        private void lkpBancos_ButtonClick(object sender, DevExpress.XtraEditors.Controls.ButtonPressedEventArgs e)
        {
            if (e.Button.Index == 1) //Adicionar
            {
                Int32 pk = ShowModuloAdd(typeof(cBancosGrade));

                if (pk > 0)
                {
                    bANCOSTableAdapter.Fill(dPessoasCampos.BANCOS);
                    lkpBancos.EditValue = pk;
                }
            }
            else
                if ((e.Button.Index == 2) && (lkpBancos.EditValue != null))//Visualizar
                    ShowModulo(typeof(cBancosGrade), true);
        }

        private void lkpTipoPessoa_EditValueChanged(object sender, EventArgs e)
        {
            if (lkpTipoPessoa.EditValue.ToString().ToUpper() == "J")
            {
                lciCnpjCpf.Text = "CNPJ";
                txtCnpjCpf.Properties.Mask.EditMask = "99.999.999/9999-99";

                lciRgIe.Text = "IE";
                txtRgIe.Properties.Mask.EditMask = "";

                dteNascimento.Enabled = false;
            }
            else
            {
                lciCnpjCpf.Text = "Cpf";
                txtCnpjCpf.Properties.Mask.EditMask = "999.999.999-99";

                lciRgIe.Text = "RG";
                txtRgIe.Properties.Mask.EditMask = "99.999.999-a";

                dteNascimento.Enabled = true;
            }
        }

        private void lkpCidades_EditValueChanged(object sender, EventArgs e)
        {

            lkpUF.EditValue = lkpCidades.EditValue;

         /*
            if (lkpCidades.Text.Trim() == "")
                lkpUF.Text = "";
            else
                lkpUF.Text = lkpCidades.GetColumnValue("CIDUf").ToString();
         */
        }

        private dPessoasCampos.PESSOASRow LinhaMae
        {
            get
            {
                return (dPessoasCampos.PESSOASRow)LinhaMae_F;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected override void  btnConfirmar_F_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {                        
            BindingSourcePrincipal.EndEdit();            
            if (LinhaMae != null)
            {
                if (!LinhaMae.IsPESCpfCnpjNull())
                {
                    CPFCNPJ cnpj = new CPFCNPJ(LinhaMae.PESCpfCnpj);
                    if (cnpj.Tipo == TipoCpfCnpj.INVALIDO)
                    {
                        MessageBox.Show("CPF/CNPJ inv�lido");
                        txtCnpjCpf.Focus();
                        return;
                    }
                    else
                    {
                        if(LinhaMae.PESCpfCnpj != cnpj.ToString(true))
                            LinhaMae.PESCpfCnpj = cnpj.ToString(true);                        
                        string strTipo = (cnpj.Tipo == TipoCpfCnpj.CNPJ) ? "J" : "F";
                        if (LinhaMae.IsPESTipoNull() || (LinhaMae.PESTipo != strTipo))
                            LinhaMae.PESTipo = strTipo;                        
                    }
                }

                SortedList CamposAlterados = VirDB.VirtualTableAdapter.CamposEfetivamenteAlterados(LinhaMae);
                if (CamposAlterados.Count > 0)
                {
                    string Just = String.Format("\r\n\r\n{0} - Cadastro alterado por {1}\r\nAltera��es:", DateTime.Now, CompontesBasicos.Bancovirtual.UsuarioLogado.UsuarioLogadoSt.USUNome);
                    if (LinhaMae.HasVersion(DataRowVersion.Original))
                    {
                        foreach (DictionaryEntry Alterado in CamposAlterados)
                            Just += String.Format("\r\n{0}: {1} - > {2}", Alterado.Key, LinhaMae[Alterado.Key.ToString(), DataRowVersion.Original], LinhaMae[Alterado.Key.ToString()]);
                        if (LinhaMae.IsPESHistoricoNull())
                            LinhaMae.PESHistorico = Just;
                        else
                            LinhaMae.PESHistorico += Just;
                    };
                }
            }
 	        base.btnConfirmar_F_ItemClick(sender, e);

            if (Estatica.EdEstatico.PESSOAS.Rows.Count > 0) {
                dEstatico.PESSOASRow rowEstatica = Estatica.EdEstatico.PESSOAS.FindByPES(pk);
                if (rowEstatica != null)
                    rowEstatica.Delete();
                Estatica.EAdapPessoas.ClearBeforeFill = false;
                Estatica.EAdapPessoas.FillByPES(Estatica.EdEstatico.PESSOAS,pk);
                Estatica.EAdapPessoas.ClearBeforeFill = true;
                Estatica.EdEstatico.PESSOAS.AcceptChanges();
            };

            /*
            using (fExportToMsAccess _Export = new fExportToMsAccess())
            {
                //unidadesDaPessoaTableAdapter.Fill(dPessoasCampos.UnidadesDaPessoa, pk);
                dPessoasCampos.Carrega(pk);
                foreach (dPessoasCampos.UnidadesDaPessoaRow row in dPessoasCampos.UnidadesDaPessoa)
                {
                    if (row["APTProprietario_PES"].ToString() != "")
                        _Export.ExportarInquilinoProprietarioParaMsAccess(true, row.APTProprietario_PES, row.CONCodigo, row.BLOCodigo, row.APTNumero, row.IsAPTFormaPagtoProprietario_FPGNull() ? 4 : row.APTFormaPagtoProprietario_FPG);
                    if (row["APTInquilino_PES"].ToString() != "")
                        _Export.ExportarInquilinoProprietarioParaMsAccess(false, row.APTInquilino_PES, row.CONCodigo, row.BLOCodigo, row.APTNumero,row.IsAPTFormaPagtoInquilino_FPGNull() ? 4 :  row.APTFormaPagtoInquilino_FPG);
                }
            }*/
        }
        
        private static void Truncar(DevExpress.XtraEditors.TextEdit Controle, string Valor)
        {
            Controle.Text = (Controle.Properties.MaxLength >= Valor.Length) ? Valor : Valor.Substring(0, Controle.Properties.MaxLength);
        }

        private void simpleButton1_Click(object sender, EventArgs e)
        {
            if (VirMSSQL.CEP.STCEP.BuscaPorCEP(textEdit1.Text))
            {
                Truncar(textEdit1, VirMSSQL.CEP.STCEP.CEPFormatado);
                Truncar(textEdit5, VirMSSQL.CEP.STCEP.Rua);
                Truncar(textEdit9, VirMSSQL.CEP.STCEP.Bairro);
                if (VirMSSQL.CEP.STCEP.NovaCidade)
                    Framework.datasets.dCIDADES.dCIDADESSt.RefreshDados();
                    //cIDADESTableAdapter.Fill(dPessoasCampos.CIDADES);
                lkpCidades.EditValue = VirMSSQL.CEP.STCEP.nCidadeNeon;
                textEdit5.Focus();
            }
        }

        private void txtCnpjCpf_Validating(object sender, CancelEventArgs e)
        {
            CPFCNPJ cnpj = new CPFCNPJ(txtCnpjCpf.Text);
            if (cnpj.Tipo == TipoCpfCnpj.INVALIDO)
                txtCnpjCpf.ErrorText = "CNPJ inv�lido";
            else
            {
                txtCnpjCpf.Text = cnpj.ToString(true);
                //if (lkpTipoPessoa.EditValue == null)
                //    MessageBox.Show("antingo:nulo");
                //else
                //    MessageBox.Show("antingo:" + lkpTipoPessoa.EditValue.ToString());
                if (cnpj.Tipo == TipoCpfCnpj.CNPJ)
                    lkpTipoPessoa.EditValue = "J";
                else
                    lkpTipoPessoa.EditValue = "F";
                //if (lkpTipoPessoa.EditValue == null)
                //    MessageBox.Show("antingo:nulo");
                //else
                //    MessageBox.Show("antingo:" + lkpTipoPessoa.EditValue.ToString());
            }
        }

        private void textEdit14_Validating(object sender, CancelEventArgs e)
        {
            if ((textEdit14.EditValue == null) || (textEdit14.EditValue.ToString() == ""))
                return;
            DocBacarios.PIS PIS = new PIS(textEdit14.EditValue.ToString());
            if (PIS.Valor == 0)
                e.Cancel = true;
            else
                textEdit14.EditValue = PIS.ToString();
        }

        
    }
}

