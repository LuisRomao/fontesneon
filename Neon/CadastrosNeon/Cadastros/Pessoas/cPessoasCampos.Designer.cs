namespace Cadastros.Pessoas
{
    partial class cPessoasCampos
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(cPessoasCampos));
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject1 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject2 = new DevExpress.Utils.SerializableAppearanceObject();
            this.LayoutControl = new DevExpress.XtraLayout.LayoutControl();
            this.memoEdit1 = new DevExpress.XtraEditors.MemoEdit();
            this.simpleButton1 = new DevExpress.XtraEditors.SimpleButton();
            this.textEdit2 = new DevExpress.XtraEditors.TextEdit();
            this.gridControl1 = new DevExpress.XtraGrid.GridControl();
            this.unidadesDaPessoaBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.dPessoasCampos = new Cadastros.Pessoas.dPessoasCampos();
            this.gridView1 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colCONCodigo = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colCONNome = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colBLONome = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colAPTNumero = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colAPTFormaPagtoX_FPG = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemImageComboBox1 = new DevExpress.XtraEditors.Repository.RepositoryItemImageComboBox();
            this.imageCollection1 = new DevExpress.Utils.ImageCollection(this.components);
            this.colProprietario = new DevExpress.XtraGrid.Columns.GridColumn();
            this.ComboBoxPI = new DevExpress.XtraEditors.Repository.RepositoryItemImageComboBox();
            this.imagensPI1 = new Framework.objetosNeon.ImagensPI();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.textEdit18 = new DevExpress.XtraEditors.TextEdit();
            this.textEdit17 = new DevExpress.XtraEditors.TextEdit();
            this.dteNascimento = new DevExpress.XtraEditors.DateEdit();
            this.textEdit16 = new DevExpress.XtraEditors.TextEdit();
            this.textEdit15 = new DevExpress.XtraEditors.TextEdit();
            this.lkpCartorios = new DevExpress.XtraEditors.LookUpEdit();
            this.fORNECEDORESBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.textEdit14 = new DevExpress.XtraEditors.TextEdit();
            this.txtRgIe = new DevExpress.XtraEditors.TextEdit();
            this.lkpBancos = new DevExpress.XtraEditors.LookUpEdit();
            this.bANCOSBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.textEdit13 = new DevExpress.XtraEditors.TextEdit();
            this.textEdit12 = new DevExpress.XtraEditors.TextEdit();
            this.textEdit11 = new DevExpress.XtraEditors.TextEdit();
            this.textEdit10 = new DevExpress.XtraEditors.TextEdit();
            this.textEdit9 = new DevExpress.XtraEditors.TextEdit();
            this.lkpUF = new DevExpress.XtraEditors.LookUpEdit();
            this.cIDADESBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.lkpCidades = new DevExpress.XtraEditors.LookUpEdit();
            this.textEdit5 = new DevExpress.XtraEditors.TextEdit();
            this.textEdit4 = new DevExpress.XtraEditors.TextEdit();
            this.lkpTipoPessoa = new DevExpress.XtraEditors.LookUpEdit();
            this.tIPOSBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.textEdit1 = new DevExpress.XtraEditors.TextEdit();
            this.EdNome = new DevExpress.XtraEditors.TextEdit();
            this.txtCnpjCpf = new DevExpress.XtraEditors.ButtonEdit();
            this.LayoutControlGroup = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlItem2 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem1 = new DevExpress.XtraLayout.LayoutControlItem();
            this.tcgPessoas = new DevExpress.XtraLayout.TabbedControlGroup();
            this.lcgBasico = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlGroup3 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlItem3 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem10 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem11 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem12 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem19 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem13 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlGroup4 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlItem9 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem14 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem15 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem16 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem17 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlGroup6 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.lciCnpjCpf = new DevExpress.XtraLayout.LayoutControlItem();
            this.lciRgIe = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem7 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem18 = new DevExpress.XtraLayout.LayoutControlItem();
            this.lciNascimento = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlGroup5 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlGroup7 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlItem4 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem20 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem21 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem22 = new DevExpress.XtraLayout.LayoutControlItem();
            this.emptySpaceItem2 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.layoutControlItem23 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem24 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem25 = new DevExpress.XtraLayout.LayoutControlItem();
            this.lcgUnidades = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlItem6 = new DevExpress.XtraLayout.LayoutControlItem();
            this.lcgHistorico = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlItem5 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem8 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlGroup2 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.pESSOASTableAdapter = new Cadastros.Pessoas.dPessoasCamposTableAdapters.PESSOASTableAdapter();
            this.fORNECEDORESTableAdapter = new Cadastros.Pessoas.dPessoasCamposTableAdapters.FORNECEDORESTableAdapter();
            this.bANCOSTableAdapter = new Cadastros.Pessoas.dPessoasCamposTableAdapters.BANCOSTableAdapter();
            ((System.ComponentModel.ISupportInitialize)(this.BarManager_F)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.BarAndDockingController_F)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.BindingSource_F)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.StyleController_F)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LayoutControl)).BeginInit();
            this.LayoutControl.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.memoEdit1.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit2.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.unidadesDaPessoaBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dPessoasCampos)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemImageComboBox1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.imageCollection1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ComboBoxPI)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.imagensPI1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit18.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit17.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dteNascimento.Properties.CalendarTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dteNascimento.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit16.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit15.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lkpCartorios.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.fORNECEDORESBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit14.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtRgIe.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lkpBancos.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.bANCOSBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit13.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit12.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit11.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit10.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit9.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lkpUF.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cIDADESBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lkpCidades.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit5.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit4.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lkpTipoPessoa.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tIPOSBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit1.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.EdNome.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCnpjCpf.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LayoutControlGroup)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tcgPessoas)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lcgBasico)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem10)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem11)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem12)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem19)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem13)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem9)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem14)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem15)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem16)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem17)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lciCnpjCpf)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lciRgIe)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem18)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lciNascimento)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem20)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem21)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem22)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem23)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem24)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem25)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lcgUnidades)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lcgHistorico)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem8)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup2)).BeginInit();
            this.SuspendLayout();
            // 
            // barStatus_F
            // 
            this.barStatus_F.OptionsBar.AllowQuickCustomization = false;
            this.barStatus_F.OptionsBar.DisableClose = true;
            this.barStatus_F.OptionsBar.DisableCustomization = true;
            this.barStatus_F.OptionsBar.DrawDragBorder = false;
            this.barStatus_F.OptionsBar.DrawSizeGrip = true;
            this.barStatus_F.OptionsBar.UseWholeRow = true;
            // 
            // barComponente_F
            // 
            this.barComponente_F.OptionsBar.AllowQuickCustomization = false;
            this.barComponente_F.OptionsBar.DisableClose = true;
            this.barComponente_F.OptionsBar.DisableCustomization = true;
            this.barComponente_F.OptionsBar.DrawDragBorder = false;
            this.barComponente_F.OptionsBar.UseWholeRow = true;
            // 
            // btnFechar_F
            // 
            this.btnFechar_F.Visibility = DevExpress.XtraBars.BarItemVisibility.Never;
            // 
            // BarAndDockingController_F
            // 
            this.BarAndDockingController_F.PropertiesBar.AllowLinkLighting = false;
            this.BarAndDockingController_F.PropertiesBar.DefaultGlyphSize = new System.Drawing.Size(16, 16);
            this.BarAndDockingController_F.PropertiesBar.DefaultLargeGlyphSize = new System.Drawing.Size(32, 32);
            // 
            // BindingSource_F
            // 
            this.BindingSource_F.DataMember = "PESSOAS";
            this.BindingSource_F.DataSource = this.dPessoasCampos;
            // 
            // ToolTipController_F
            // 
            this.ToolTipController_F.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(222)))), ((int)(((byte)(237)))), ((int)(((byte)(250)))));
            this.ToolTipController_F.Appearance.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(20)))), ((int)(((byte)(69)))), ((int)(((byte)(141)))));
            this.ToolTipController_F.Appearance.Options.UseBackColor = true;
            this.ToolTipController_F.Appearance.Options.UseFont = true;
            this.ToolTipController_F.Appearance.Options.UseForeColor = true;
            this.ToolTipController_F.Appearance.Options.UseTextOptions = true;
            this.ToolTipController_F.Appearance.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            // 
            // StyleController_F
            // 
            this.StyleController_F.AppearanceDisabled.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.StyleController_F.AppearanceDisabled.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.StyleController_F.AppearanceDisabled.Options.UseBackColor = true;
            this.StyleController_F.AppearanceDisabled.Options.UseForeColor = true;
            this.StyleController_F.AppearanceFocused.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(253)))), ((int)(((byte)(253)))), ((int)(((byte)(181)))));
            this.StyleController_F.AppearanceFocused.Options.UseBackColor = true;
            // 
            // LayoutControl
            // 
            this.LayoutControl.AllowCustomization = false;
            this.LayoutControl.Controls.Add(this.memoEdit1);
            this.LayoutControl.Controls.Add(this.simpleButton1);
            this.LayoutControl.Controls.Add(this.textEdit2);
            this.LayoutControl.Controls.Add(this.gridControl1);
            this.LayoutControl.Controls.Add(this.label2);
            this.LayoutControl.Controls.Add(this.label1);
            this.LayoutControl.Controls.Add(this.textEdit18);
            this.LayoutControl.Controls.Add(this.textEdit17);
            this.LayoutControl.Controls.Add(this.dteNascimento);
            this.LayoutControl.Controls.Add(this.textEdit16);
            this.LayoutControl.Controls.Add(this.textEdit15);
            this.LayoutControl.Controls.Add(this.lkpCartorios);
            this.LayoutControl.Controls.Add(this.textEdit14);
            this.LayoutControl.Controls.Add(this.txtRgIe);
            this.LayoutControl.Controls.Add(this.lkpBancos);
            this.LayoutControl.Controls.Add(this.textEdit13);
            this.LayoutControl.Controls.Add(this.textEdit12);
            this.LayoutControl.Controls.Add(this.textEdit11);
            this.LayoutControl.Controls.Add(this.textEdit10);
            this.LayoutControl.Controls.Add(this.textEdit9);
            this.LayoutControl.Controls.Add(this.lkpUF);
            this.LayoutControl.Controls.Add(this.lkpCidades);
            this.LayoutControl.Controls.Add(this.textEdit5);
            this.LayoutControl.Controls.Add(this.textEdit4);
            this.LayoutControl.Controls.Add(this.lkpTipoPessoa);
            this.LayoutControl.Controls.Add(this.textEdit1);
            this.LayoutControl.Controls.Add(this.EdNome);
            this.LayoutControl.Controls.Add(this.txtCnpjCpf);
            this.LayoutControl.Dock = System.Windows.Forms.DockStyle.Fill;
            this.LayoutControl.Location = new System.Drawing.Point(0, 35);
            this.LayoutControl.Margin = new System.Windows.Forms.Padding(0);
            this.LayoutControl.Name = "LayoutControl";
            this.LayoutControl.OptionsFocus.AllowFocusTabbedGroups = false;
            this.LayoutControl.Root = this.LayoutControlGroup;
            this.LayoutControl.Size = new System.Drawing.Size(748, 461);
            this.LayoutControl.TabIndex = 0;
            // 
            // memoEdit1
            // 
            this.memoEdit1.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.BindingSource_F, "PESHistorico", true));
            this.memoEdit1.Location = new System.Drawing.Point(23, 76);
            this.memoEdit1.MenuManager = this.BarManager_F;
            this.memoEdit1.Name = "memoEdit1";
            this.memoEdit1.Properties.ReadOnly = true;
            this.memoEdit1.Size = new System.Drawing.Size(698, 331);
            this.memoEdit1.StyleController = this.LayoutControl;
            this.memoEdit1.TabIndex = 1;
            // 
            // simpleButton1
            // 
            this.simpleButton1.Location = new System.Drawing.Point(244, 208);
            this.simpleButton1.Name = "simpleButton1";
            this.simpleButton1.Size = new System.Drawing.Size(175, 22);
            this.simpleButton1.StyleController = this.LayoutControl;
            this.simpleButton1.TabIndex = 9;
            this.simpleButton1.Text = "Buscar Endere�o";
            this.simpleButton1.Click += new System.EventHandler(this.simpleButton1_Click);
            // 
            // textEdit2
            // 
            this.textEdit2.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.BindingSource_F, "PESSolicitante", true));
            this.textEdit2.Location = new System.Drawing.Point(70, 426);
            this.textEdit2.Name = "textEdit2";
            this.textEdit2.Size = new System.Drawing.Size(666, 20);
            this.textEdit2.StyleController = this.LayoutControl;
            this.textEdit2.TabIndex = 19;
            // 
            // gridControl1
            // 
            this.gridControl1.DataSource = this.unidadesDaPessoaBindingSource;
            this.gridControl1.Location = new System.Drawing.Point(23, 76);
            this.gridControl1.MainView = this.gridView1;
            this.gridControl1.Name = "gridControl1";
            this.gridControl1.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemImageComboBox1,
            this.ComboBoxPI});
            this.gridControl1.Size = new System.Drawing.Size(698, 331);
            this.gridControl1.TabIndex = 1;
            this.gridControl1.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridView1});
            // 
            // unidadesDaPessoaBindingSource
            // 
            this.unidadesDaPessoaBindingSource.DataMember = "UnidadesDaPessoa";
            this.unidadesDaPessoaBindingSource.DataSource = this.dPessoasCampos;
            // 
            // dPessoasCampos
            // 
            this.dPessoasCampos.DataSetName = "dPessoasCampos";
            this.dPessoasCampos.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // gridView1
            // 
            this.gridView1.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colCONCodigo,
            this.colCONNome,
            this.colBLONome,
            this.colAPTNumero,
            this.colAPTFormaPagtoX_FPG,
            this.colProprietario});
            this.gridView1.GridControl = this.gridControl1;
            this.gridView1.Name = "gridView1";
            this.gridView1.OptionsView.ShowFooter = true;
            this.gridView1.OptionsView.ShowGroupPanel = false;
            // 
            // colCONCodigo
            // 
            this.colCONCodigo.Caption = "CODCON";
            this.colCONCodigo.FieldName = "CONCodigo";
            this.colCONCodigo.Name = "colCONCodigo";
            this.colCONCodigo.OptionsColumn.FixedWidth = true;
            this.colCONCodigo.OptionsColumn.ReadOnly = true;
            this.colCONCodigo.Visible = true;
            this.colCONCodigo.VisibleIndex = 0;
            // 
            // colCONNome
            // 
            this.colCONNome.Caption = "Nome";
            this.colCONNome.FieldName = "CONNome";
            this.colCONNome.Name = "colCONNome";
            this.colCONNome.OptionsColumn.ReadOnly = true;
            this.colCONNome.Visible = true;
            this.colCONNome.VisibleIndex = 1;
            this.colCONNome.Width = 366;
            // 
            // colBLONome
            // 
            this.colBLONome.Caption = "Bloco";
            this.colBLONome.FieldName = "BLOCodigo";
            this.colBLONome.Name = "colBLONome";
            this.colBLONome.OptionsColumn.FixedWidth = true;
            this.colBLONome.OptionsColumn.ReadOnly = true;
            this.colBLONome.Visible = true;
            this.colBLONome.VisibleIndex = 2;
            // 
            // colAPTNumero
            // 
            this.colAPTNumero.Caption = "N�";
            this.colAPTNumero.FieldName = "APTNumero";
            this.colAPTNumero.Name = "colAPTNumero";
            this.colAPTNumero.OptionsColumn.FixedWidth = true;
            this.colAPTNumero.OptionsColumn.ReadOnly = true;
            this.colAPTNumero.Visible = true;
            this.colAPTNumero.VisibleIndex = 3;
            // 
            // colAPTFormaPagtoX_FPG
            // 
            this.colAPTFormaPagtoX_FPG.Caption = "Boleto";
            this.colAPTFormaPagtoX_FPG.ColumnEdit = this.repositoryItemImageComboBox1;
            this.colAPTFormaPagtoX_FPG.FieldName = "APTFormaPagtoX_FPG";
            this.colAPTFormaPagtoX_FPG.Name = "colAPTFormaPagtoX_FPG";
            this.colAPTFormaPagtoX_FPG.OptionsColumn.FixedWidth = true;
            this.colAPTFormaPagtoX_FPG.OptionsColumn.ReadOnly = true;
            this.colAPTFormaPagtoX_FPG.OptionsColumn.ShowCaption = false;
            this.colAPTFormaPagtoX_FPG.Visible = true;
            this.colAPTFormaPagtoX_FPG.VisibleIndex = 4;
            this.colAPTFormaPagtoX_FPG.Width = 30;
            // 
            // repositoryItemImageComboBox1
            // 
            this.repositoryItemImageComboBox1.AutoHeight = false;
            this.repositoryItemImageComboBox1.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemImageComboBox1.GlyphAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.repositoryItemImageComboBox1.Items.AddRange(new DevExpress.XtraEditors.Controls.ImageComboBoxItem[] {
            new DevExpress.XtraEditors.Controls.ImageComboBoxItem("Correio", 2, 0),
            new DevExpress.XtraEditors.Controls.ImageComboBoxItem("Condom�nio", 4, 1)});
            this.repositoryItemImageComboBox1.LargeImages = this.imageCollection1;
            this.repositoryItemImageComboBox1.Name = "repositoryItemImageComboBox1";
            this.repositoryItemImageComboBox1.SmallImages = this.imageCollection1;
            // 
            // imageCollection1
            // 
            this.imageCollection1.ImageStream = ((DevExpress.Utils.ImageCollectionStreamer)(resources.GetObject("imageCollection1.ImageStream")));
            // 
            // colProprietario
            // 
            this.colProprietario.ColumnEdit = this.ComboBoxPI;
            this.colProprietario.FieldName = "Proprietario";
            this.colProprietario.Name = "colProprietario";
            this.colProprietario.OptionsColumn.FixedWidth = true;
            this.colProprietario.OptionsColumn.ReadOnly = true;
            this.colProprietario.OptionsColumn.ShowCaption = false;
            this.colProprietario.Visible = true;
            this.colProprietario.VisibleIndex = 5;
            this.colProprietario.Width = 30;
            // 
            // ComboBoxPI
            // 
            this.ComboBoxPI.AutoHeight = false;
            this.ComboBoxPI.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.ComboBoxPI.GlyphAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.ComboBoxPI.Items.AddRange(new DevExpress.XtraEditors.Controls.ImageComboBoxItem[] {
            new DevExpress.XtraEditors.Controls.ImageComboBoxItem("P", true, 0),
            new DevExpress.XtraEditors.Controls.ImageComboBoxItem("I", false, 1)});
            this.ComboBoxPI.Name = "ComboBoxPI";
            this.ComboBoxPI.SmallImages = this.imagensPI1;
            // 
            // imagensPI1
            // 
            this.imagensPI1.ImageStream = ((DevExpress.Utils.ImageCollectionStreamer)(resources.GetObject("imagensPI1.ImageStream")));
            // 
            // label2
            // 
            this.label2.Location = new System.Drawing.Point(621, 131);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(7, 266);
            this.label2.TabIndex = 1;
            this.label2.Text = "-";
            this.label2.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label1
            // 
            this.label1.Location = new System.Drawing.Point(276, 131);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(7, 266);
            this.label1.TabIndex = 1;
            this.label1.Text = "-";
            this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // textEdit18
            // 
            this.textEdit18.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.BindingSource_F, "PESDigitoConta", true));
            this.textEdit18.Location = new System.Drawing.Point(630, 133);
            this.textEdit18.Name = "textEdit18";
            this.textEdit18.Size = new System.Drawing.Size(79, 20);
            this.textEdit18.StyleController = this.LayoutControl;
            this.textEdit18.TabIndex = 1;
            // 
            // textEdit17
            // 
            this.textEdit17.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.BindingSource_F, "PESDigitoAgencia", true));
            this.textEdit17.Location = new System.Drawing.Point(285, 133);
            this.textEdit17.Name = "textEdit17";
            this.textEdit17.Size = new System.Drawing.Size(79, 20);
            this.textEdit17.StyleController = this.LayoutControl;
            this.textEdit17.TabIndex = 1;
            // 
            // dteNascimento
            // 
            this.dteNascimento.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.BindingSource_F, "PESDataNascimento", true));
            this.dteNascimento.EditValue = null;
            this.dteNascimento.Location = new System.Drawing.Point(519, 136);
            this.dteNascimento.Name = "dteNascimento";
            this.dteNascimento.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.dteNascimento.Properties.CalendarTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.dteNascimento.Size = new System.Drawing.Size(190, 20);
            this.dteNascimento.StyleController = this.LayoutControl;
            this.dteNascimento.TabIndex = 7;
            // 
            // textEdit16
            // 
            this.textEdit16.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.BindingSource_F, "PESConta", true));
            this.textEdit16.Location = new System.Drawing.Point(439, 133);
            this.textEdit16.Name = "textEdit16";
            this.textEdit16.Size = new System.Drawing.Size(180, 20);
            this.textEdit16.StyleController = this.LayoutControl;
            this.textEdit16.TabIndex = 1;
            // 
            // textEdit15
            // 
            this.textEdit15.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.BindingSource_F, "PESAgencia", true));
            this.textEdit15.Location = new System.Drawing.Point(93, 133);
            this.textEdit15.Name = "textEdit15";
            this.textEdit15.Size = new System.Drawing.Size(181, 20);
            this.textEdit15.StyleController = this.LayoutControl;
            this.textEdit15.TabIndex = 1;
            // 
            // lkpCartorios
            // 
            this.lkpCartorios.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.BindingSource_F, "PESCartorio_FRN", true));
            this.lkpCartorios.Location = new System.Drawing.Point(93, 136);
            this.lkpCartorios.Name = "lkpCartorios";
            this.lkpCartorios.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo),
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Plus),
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.lkpCartorios.Properties.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("FRNNome", "Nome", 53, DevExpress.Utils.FormatType.None, "", true, DevExpress.Utils.HorzAlignment.Near),
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("FRNCnpj", "Cnpj", 48, DevExpress.Utils.FormatType.None, "", true, DevExpress.Utils.HorzAlignment.Near)});
            this.lkpCartorios.Properties.DataSource = this.fORNECEDORESBindingSource;
            this.lkpCartorios.Properties.DisplayMember = "FRNNome";
            this.lkpCartorios.Properties.ValueMember = "FRN";
            this.lkpCartorios.Size = new System.Drawing.Size(364, 20);
            this.lkpCartorios.StyleController = this.LayoutControl;
            this.lkpCartorios.TabIndex = 6;
            this.lkpCartorios.ButtonClick += new DevExpress.XtraEditors.Controls.ButtonPressedEventHandler(this.lkpCartorios_ButtonClick);
            // 
            // fORNECEDORESBindingSource
            // 
            this.fORNECEDORESBindingSource.DataMember = "FORNECEDORES";
            this.fORNECEDORESBindingSource.DataSource = this.dPessoasCampos;
            // 
            // textEdit14
            // 
            this.textEdit14.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.BindingSource_F, "PESPis", true));
            this.textEdit14.Location = new System.Drawing.Point(541, 106);
            this.textEdit14.Name = "textEdit14";
            this.textEdit14.Size = new System.Drawing.Size(168, 20);
            this.textEdit14.StyleController = this.LayoutControl;
            this.textEdit14.TabIndex = 5;
            this.textEdit14.Validating += new System.ComponentModel.CancelEventHandler(this.textEdit14_Validating);
            // 
            // txtRgIe
            // 
            this.txtRgIe.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.BindingSource_F, "PESRgIe", true));
            this.txtRgIe.Location = new System.Drawing.Point(350, 106);
            this.txtRgIe.Name = "txtRgIe";
            this.txtRgIe.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Simple;
            this.txtRgIe.Properties.Mask.PlaceHolder = ' ';
            this.txtRgIe.Size = new System.Drawing.Size(162, 20);
            this.txtRgIe.StyleController = this.LayoutControl;
            this.txtRgIe.TabIndex = 4;
            // 
            // lkpBancos
            // 
            this.lkpBancos.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.BindingSource_F, "PESBanco_BCO", true));
            this.lkpBancos.Location = new System.Drawing.Point(93, 109);
            this.lkpBancos.Name = "lkpBancos";
            this.lkpBancos.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo),
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Plus)});
            this.lkpBancos.Properties.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("BCONome", "Nome", 54, DevExpress.Utils.FormatType.None, "", true, DevExpress.Utils.HorzAlignment.Near)});
            this.lkpBancos.Properties.DataSource = this.bANCOSBindingSource;
            this.lkpBancos.Properties.DisplayMember = "BCONome";
            this.lkpBancos.Properties.ValueMember = "BCO";
            this.lkpBancos.Size = new System.Drawing.Size(616, 20);
            this.lkpBancos.StyleController = this.LayoutControl;
            this.lkpBancos.TabIndex = 1;
            this.lkpBancos.ButtonClick += new DevExpress.XtraEditors.Controls.ButtonPressedEventHandler(this.lkpBancos_ButtonClick);
            // 
            // bANCOSBindingSource
            // 
            this.bANCOSBindingSource.DataMember = "BANCOS";
            this.bANCOSBindingSource.DataSource = this.dPessoasCampos;
            // 
            // textEdit13
            // 
            this.textEdit13.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.BindingSource_F, "PESEmail", true));
            this.textEdit13.Location = new System.Drawing.Point(93, 375);
            this.textEdit13.Name = "textEdit13";
            this.textEdit13.Size = new System.Drawing.Size(616, 20);
            this.textEdit13.StyleController = this.LayoutControl;
            this.textEdit13.TabIndex = 18;
            // 
            // textEdit12
            // 
            this.textEdit12.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.BindingSource_F, "PESFone4", true));
            this.textEdit12.Location = new System.Drawing.Point(430, 351);
            this.textEdit12.Name = "textEdit12";
            this.textEdit12.Size = new System.Drawing.Size(279, 20);
            this.textEdit12.StyleController = this.LayoutControl;
            this.textEdit12.TabIndex = 17;
            // 
            // textEdit11
            // 
            this.textEdit11.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.BindingSource_F, "PESFone1", true));
            this.textEdit11.Location = new System.Drawing.Point(93, 327);
            this.textEdit11.Name = "textEdit11";
            this.textEdit11.Size = new System.Drawing.Size(275, 20);
            this.textEdit11.StyleController = this.LayoutControl;
            this.textEdit11.TabIndex = 14;
            // 
            // textEdit10
            // 
            this.textEdit10.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.BindingSource_F, "PESFone2", true));
            this.textEdit10.Location = new System.Drawing.Point(430, 327);
            this.textEdit10.Name = "textEdit10";
            this.textEdit10.Size = new System.Drawing.Size(279, 20);
            this.textEdit10.StyleController = this.LayoutControl;
            this.textEdit10.TabIndex = 15;
            // 
            // textEdit9
            // 
            this.textEdit9.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.BindingSource_F, "PESBairro", true));
            this.textEdit9.Location = new System.Drawing.Point(481, 208);
            this.textEdit9.Name = "textEdit9";
            this.textEdit9.Size = new System.Drawing.Size(228, 20);
            this.textEdit9.StyleController = this.LayoutControl;
            this.textEdit9.TabIndex = 10;
            // 
            // lkpUF
            // 
            this.lkpUF.Enabled = false;
            this.lkpUF.Location = new System.Drawing.Point(554, 258);
            this.lkpUF.Name = "lkpUF";
            this.lkpUF.Properties.DataSource = this.cIDADESBindingSource;
            this.lkpUF.Properties.DisplayMember = "CIDUf";
            this.lkpUF.Properties.ValueMember = "CID";
            this.lkpUF.Size = new System.Drawing.Size(155, 20);
            this.lkpUF.StyleController = this.LayoutControl;
            this.lkpUF.TabIndex = 13;
            // 
            // cIDADESBindingSource
            // 
            this.cIDADESBindingSource.DataMember = "CIDADES";
            this.cIDADESBindingSource.DataSource = typeof(Framework.datasets.dCIDADES);
            // 
            // lkpCidades
            // 
            this.lkpCidades.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.BindingSource_F, "PES_CID", true));
            this.lkpCidades.Location = new System.Drawing.Point(93, 258);
            this.lkpCidades.Name = "lkpCidades";
            this.lkpCidades.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo),
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Plus)});
            this.lkpCidades.Properties.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("CIDNome", "Nome", 64, DevExpress.Utils.FormatType.None, "", true, DevExpress.Utils.HorzAlignment.Near),
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("CIDUf", "UF")});
            this.lkpCidades.Properties.DataSource = this.cIDADESBindingSource;
            this.lkpCidades.Properties.DisplayMember = "CIDNome";
            this.lkpCidades.Properties.ValueMember = "CID";
            this.lkpCidades.Size = new System.Drawing.Size(432, 20);
            this.lkpCidades.StyleController = this.LayoutControl;
            this.lkpCidades.TabIndex = 12;
            this.lkpCidades.ButtonClick += new DevExpress.XtraEditors.Controls.ButtonPressedEventHandler(this.lkpCidades_ButtonClick);
            this.lkpCidades.EditValueChanged += new System.EventHandler(this.lkpCidades_EditValueChanged);
            // 
            // textEdit5
            // 
            this.textEdit5.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.BindingSource_F, "PESEndereco", true));
            this.textEdit5.Location = new System.Drawing.Point(93, 234);
            this.textEdit5.Name = "textEdit5";
            this.textEdit5.Size = new System.Drawing.Size(616, 20);
            this.textEdit5.StyleController = this.LayoutControl;
            this.textEdit5.TabIndex = 11;
            // 
            // textEdit4
            // 
            this.textEdit4.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.BindingSource_F, "PESFone3", true));
            this.textEdit4.Location = new System.Drawing.Point(93, 351);
            this.textEdit4.Name = "textEdit4";
            this.textEdit4.Size = new System.Drawing.Size(275, 20);
            this.textEdit4.StyleController = this.LayoutControl;
            this.textEdit4.TabIndex = 16;
            // 
            // lkpTipoPessoa
            // 
            this.lkpTipoPessoa.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.BindingSource_F, "PESTipo", true));
            this.lkpTipoPessoa.Location = new System.Drawing.Point(549, 15);
            this.lkpTipoPessoa.Name = "lkpTipoPessoa";
            this.lkpTipoPessoa.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.lkpTipoPessoa.Properties.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("TPODescricao", "Descri��o", 85, DevExpress.Utils.FormatType.None, "", true, DevExpress.Utils.HorzAlignment.Near)});
            this.lkpTipoPessoa.Properties.DataSource = this.tIPOSBindingSource;
            this.lkpTipoPessoa.Properties.DisplayMember = "TPODescricao";
            this.lkpTipoPessoa.Properties.ValueMember = "TPO";
            this.lkpTipoPessoa.Size = new System.Drawing.Size(187, 20);
            this.lkpTipoPessoa.StyleController = this.LayoutControl;
            this.lkpTipoPessoa.TabIndex = 2;
            this.lkpTipoPessoa.EditValueChanged += new System.EventHandler(this.lkpTipoPessoa_EditValueChanged);
            // 
            // tIPOSBindingSource
            // 
            this.tIPOSBindingSource.DataMember = "TIPOS";
            this.tIPOSBindingSource.DataSource = this.dPessoasCampos;
            // 
            // textEdit1
            // 
            this.textEdit1.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.BindingSource_F, "PESCep", true));
            this.textEdit1.Location = new System.Drawing.Point(93, 208);
            this.textEdit1.Name = "textEdit1";
            this.textEdit1.Properties.Mask.EditMask = "99999-999";
            this.textEdit1.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Simple;
            this.textEdit1.Properties.Mask.PlaceHolder = ' ';
            this.textEdit1.Size = new System.Drawing.Size(147, 20);
            this.textEdit1.StyleController = this.LayoutControl;
            this.textEdit1.TabIndex = 8;
            // 
            // EdNome
            // 
            this.EdNome.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.BindingSource_F, "PESNome", true));
            this.EdNome.Location = new System.Drawing.Point(52, 15);
            this.EdNome.Name = "EdNome";
            this.EdNome.Size = new System.Drawing.Size(458, 20);
            this.EdNome.StyleController = this.LayoutControl;
            this.EdNome.TabIndex = 0;
            // 
            // txtCnpjCpf
            // 
            this.txtCnpjCpf.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.BindingSource_F, "PESCpfCnpj", true));
            this.txtCnpjCpf.Location = new System.Drawing.Point(91, 106);
            this.txtCnpjCpf.Name = "txtCnpjCpf";
            this.txtCnpjCpf.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.OK, "Verificado", -1, true, false, false, DevExpress.XtraEditors.ImageLocation.MiddleCenter, null, new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), serializableAppearanceObject1, "", null, null, true),
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Delete, "ERRADO", -1, true, false, false, DevExpress.XtraEditors.ImageLocation.MiddleCenter, null, new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), serializableAppearanceObject2, "", null, null, true)});
            this.txtCnpjCpf.Properties.Mask.PlaceHolder = ' ';
            this.txtCnpjCpf.Size = new System.Drawing.Size(230, 20);
            this.txtCnpjCpf.StyleController = this.LayoutControl;
            this.txtCnpjCpf.TabIndex = 3;
            this.txtCnpjCpf.ButtonClick += new DevExpress.XtraEditors.Controls.ButtonPressedEventHandler(this.txtCnpjCpf_ButtonClick);
            this.txtCnpjCpf.Validating += new System.ComponentModel.CancelEventHandler(this.txtCnpjCpf_Validating);
            // 
            // LayoutControlGroup
            // 
            this.LayoutControlGroup.CustomizationFormText = "LayoutControlGroup";
            this.LayoutControlGroup.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItem2,
            this.layoutControlItem1,
            this.tcgPessoas,
            this.layoutControlItem8});
            this.LayoutControlGroup.Location = new System.Drawing.Point(0, 0);
            this.LayoutControlGroup.Name = "LayoutControlGroup";
            this.LayoutControlGroup.Size = new System.Drawing.Size(748, 461);
            this.LayoutControlGroup.Spacing = new DevExpress.XtraLayout.Utils.Padding(0, 0, 3, 3);
            this.LayoutControlGroup.TextVisible = false;
            // 
            // layoutControlItem2
            // 
            this.layoutControlItem2.AllowHotTrack = false;
            this.layoutControlItem2.Control = this.EdNome;
            this.layoutControlItem2.CustomizationFormText = "Nome";
            this.layoutControlItem2.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItem2.Name = "layoutControlItem2";
            this.layoutControlItem2.Size = new System.Drawing.Size(502, 24);
            this.layoutControlItem2.Text = "Nome";
            this.layoutControlItem2.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem2.TextSize = new System.Drawing.Size(35, 20);
            this.layoutControlItem2.TextToControlDistance = 5;
            // 
            // layoutControlItem1
            // 
            this.layoutControlItem1.AllowHotTrack = false;
            this.layoutControlItem1.Control = this.lkpTipoPessoa;
            this.layoutControlItem1.CustomizationFormText = "Tipo";
            this.layoutControlItem1.Location = new System.Drawing.Point(502, 0);
            this.layoutControlItem1.Name = "layoutControlItem1";
            this.layoutControlItem1.Size = new System.Drawing.Size(226, 24);
            this.layoutControlItem1.Text = "Tipo";
            this.layoutControlItem1.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem1.TextSize = new System.Drawing.Size(30, 20);
            this.layoutControlItem1.TextToControlDistance = 5;
            // 
            // tcgPessoas
            // 
            this.tcgPessoas.CustomizationFormText = "tcgPessoas";
            this.tcgPessoas.Location = new System.Drawing.Point(0, 24);
            this.tcgPessoas.Name = "tcgPessoas";
            this.tcgPessoas.SelectedTabPage = this.lcgBasico;
            this.tcgPessoas.SelectedTabPageIndex = 0;
            this.tcgPessoas.Size = new System.Drawing.Size(728, 387);
            this.tcgPessoas.Spacing = new DevExpress.XtraLayout.Utils.Padding(1, 1, 5, 1);
            this.tcgPessoas.TabPages.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.lcgBasico,
            this.layoutControlGroup5,
            this.lcgUnidades,
            this.lcgHistorico});
            // 
            // lcgBasico
            // 
            this.lcgBasico.CustomizationFormText = "&1-B�sico";
            this.lcgBasico.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlGroup3,
            this.layoutControlGroup4,
            this.layoutControlGroup6});
            this.lcgBasico.Location = new System.Drawing.Point(0, 0);
            this.lcgBasico.Name = "lcgBasico";
            this.lcgBasico.Size = new System.Drawing.Size(702, 335);
            this.lcgBasico.Text = "&1-B�sico";
            // 
            // layoutControlGroup3
            // 
            this.layoutControlGroup3.CustomizationFormText = ":: Endere�o ::";
            this.layoutControlGroup3.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItem3,
            this.layoutControlItem10,
            this.layoutControlItem11,
            this.layoutControlItem12,
            this.layoutControlItem19,
            this.layoutControlItem13});
            this.layoutControlGroup3.Location = new System.Drawing.Point(0, 102);
            this.layoutControlGroup3.Name = "layoutControlGroup3";
            this.layoutControlGroup3.Size = new System.Drawing.Size(702, 116);
            this.layoutControlGroup3.Text = ":: Endere�o ::";
            // 
            // layoutControlItem3
            // 
            this.layoutControlItem3.AllowHotTrack = false;
            this.layoutControlItem3.Control = this.textEdit1;
            this.layoutControlItem3.CustomizationFormText = "CEP";
            this.layoutControlItem3.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItem3.Name = "layoutControlItem3";
            this.layoutControlItem3.Size = new System.Drawing.Size(209, 26);
            this.layoutControlItem3.Text = "CEP";
            this.layoutControlItem3.TextSize = new System.Drawing.Size(55, 13);
            // 
            // layoutControlItem10
            // 
            this.layoutControlItem10.AllowHotTrack = false;
            this.layoutControlItem10.Control = this.textEdit5;
            this.layoutControlItem10.CustomizationFormText = "Endere�o";
            this.layoutControlItem10.Location = new System.Drawing.Point(0, 26);
            this.layoutControlItem10.Name = "layoutControlItem10";
            this.layoutControlItem10.Size = new System.Drawing.Size(678, 24);
            this.layoutControlItem10.Text = "Endere�o";
            this.layoutControlItem10.TextSize = new System.Drawing.Size(55, 13);
            // 
            // layoutControlItem11
            // 
            this.layoutControlItem11.AllowHotTrack = false;
            this.layoutControlItem11.Control = this.lkpCidades;
            this.layoutControlItem11.CustomizationFormText = "Cidade";
            this.layoutControlItem11.Location = new System.Drawing.Point(0, 50);
            this.layoutControlItem11.Name = "layoutControlItem11";
            this.layoutControlItem11.Size = new System.Drawing.Size(494, 24);
            this.layoutControlItem11.Text = "Cidade";
            this.layoutControlItem11.TextSize = new System.Drawing.Size(55, 13);
            // 
            // layoutControlItem12
            // 
            this.layoutControlItem12.AllowHotTrack = false;
            this.layoutControlItem12.Control = this.lkpUF;
            this.layoutControlItem12.CustomizationFormText = "UF";
            this.layoutControlItem12.Location = new System.Drawing.Point(494, 50);
            this.layoutControlItem12.Name = "layoutControlItem12";
            this.layoutControlItem12.Size = new System.Drawing.Size(184, 24);
            this.layoutControlItem12.Text = "UF";
            this.layoutControlItem12.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem12.TextSize = new System.Drawing.Size(20, 20);
            this.layoutControlItem12.TextToControlDistance = 5;
            // 
            // layoutControlItem19
            // 
            this.layoutControlItem19.Control = this.simpleButton1;
            this.layoutControlItem19.CustomizationFormText = "layoutControlItem19";
            this.layoutControlItem19.Location = new System.Drawing.Point(209, 0);
            this.layoutControlItem19.Name = "layoutControlItem19";
            this.layoutControlItem19.Size = new System.Drawing.Size(179, 26);
            this.layoutControlItem19.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem19.TextVisible = false;
            // 
            // layoutControlItem13
            // 
            this.layoutControlItem13.AllowHotTrack = false;
            this.layoutControlItem13.Control = this.textEdit9;
            this.layoutControlItem13.CustomizationFormText = "Bairro";
            this.layoutControlItem13.Location = new System.Drawing.Point(388, 0);
            this.layoutControlItem13.Name = "layoutControlItem13";
            this.layoutControlItem13.Size = new System.Drawing.Size(290, 26);
            this.layoutControlItem13.Text = "Bairro";
            this.layoutControlItem13.TextSize = new System.Drawing.Size(55, 13);
            // 
            // layoutControlGroup4
            // 
            this.layoutControlGroup4.CustomizationFormText = ":: Contato ::";
            this.layoutControlGroup4.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItem9,
            this.layoutControlItem14,
            this.layoutControlItem15,
            this.layoutControlItem16,
            this.layoutControlItem17});
            this.layoutControlGroup4.Location = new System.Drawing.Point(0, 218);
            this.layoutControlGroup4.Name = "layoutControlGroup4";
            this.layoutControlGroup4.Size = new System.Drawing.Size(702, 117);
            this.layoutControlGroup4.Spacing = new DevExpress.XtraLayout.Utils.Padding(2, 2, 5, 2);
            this.layoutControlGroup4.Text = ":: Contato ::";
            // 
            // layoutControlItem9
            // 
            this.layoutControlItem9.AllowHotTrack = false;
            this.layoutControlItem9.Control = this.textEdit4;
            this.layoutControlItem9.CustomizationFormText = "Telefone 3";
            this.layoutControlItem9.Location = new System.Drawing.Point(0, 24);
            this.layoutControlItem9.Name = "layoutControlItem5";
            this.layoutControlItem9.Size = new System.Drawing.Size(337, 24);
            this.layoutControlItem9.Text = "Telefone 3";
            this.layoutControlItem9.TextSize = new System.Drawing.Size(55, 13);
            // 
            // layoutControlItem14
            // 
            this.layoutControlItem14.AllowHotTrack = false;
            this.layoutControlItem14.Control = this.textEdit10;
            this.layoutControlItem14.CustomizationFormText = "Telefone 2";
            this.layoutControlItem14.Location = new System.Drawing.Point(337, 0);
            this.layoutControlItem14.Name = "layoutControlItem14";
            this.layoutControlItem14.Size = new System.Drawing.Size(341, 24);
            this.layoutControlItem14.Text = "Telefone 2";
            this.layoutControlItem14.TextSize = new System.Drawing.Size(55, 13);
            // 
            // layoutControlItem15
            // 
            this.layoutControlItem15.AllowHotTrack = false;
            this.layoutControlItem15.Control = this.textEdit11;
            this.layoutControlItem15.CustomizationFormText = "Telefone 1";
            this.layoutControlItem15.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItem15.Name = "layoutControlItem15";
            this.layoutControlItem15.Size = new System.Drawing.Size(337, 24);
            this.layoutControlItem15.Text = "Telefone 1";
            this.layoutControlItem15.TextSize = new System.Drawing.Size(55, 13);
            // 
            // layoutControlItem16
            // 
            this.layoutControlItem16.AllowHotTrack = false;
            this.layoutControlItem16.Control = this.textEdit12;
            this.layoutControlItem16.CustomizationFormText = "Telefone 4";
            this.layoutControlItem16.Location = new System.Drawing.Point(337, 24);
            this.layoutControlItem16.Name = "layoutControlItem16";
            this.layoutControlItem16.Size = new System.Drawing.Size(341, 24);
            this.layoutControlItem16.Text = "Telefone 4";
            this.layoutControlItem16.TextSize = new System.Drawing.Size(55, 13);
            // 
            // layoutControlItem17
            // 
            this.layoutControlItem17.AllowHotTrack = false;
            this.layoutControlItem17.Control = this.textEdit13;
            this.layoutControlItem17.CustomizationFormText = "Email";
            this.layoutControlItem17.Location = new System.Drawing.Point(0, 48);
            this.layoutControlItem17.Name = "layoutControlItem17";
            this.layoutControlItem17.Size = new System.Drawing.Size(678, 24);
            this.layoutControlItem17.Text = "Email";
            this.layoutControlItem17.TextSize = new System.Drawing.Size(55, 13);
            // 
            // layoutControlGroup6
            // 
            this.layoutControlGroup6.CustomizationFormText = ":: Pessoal ::";
            this.layoutControlGroup6.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.lciCnpjCpf,
            this.lciRgIe,
            this.layoutControlItem7,
            this.layoutControlItem18,
            this.lciNascimento});
            this.layoutControlGroup6.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroup6.Name = "layoutControlGroup6";
            this.layoutControlGroup6.Size = new System.Drawing.Size(702, 102);
            this.layoutControlGroup6.Text = ":: Pessoal ::";
            // 
            // lciCnpjCpf
            // 
            this.lciCnpjCpf.AllowHotTrack = false;
            this.lciCnpjCpf.Control = this.txtCnpjCpf;
            this.lciCnpjCpf.CustomizationFormText = "CNPJ";
            this.lciCnpjCpf.Location = new System.Drawing.Point(0, 0);
            this.lciCnpjCpf.MinSize = new System.Drawing.Size(50, 25);
            this.lciCnpjCpf.Name = "lciCnpjCpf";
            this.lciCnpjCpf.Size = new System.Drawing.Size(290, 30);
            this.lciCnpjCpf.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.lciCnpjCpf.Text = "CNPJ";
            this.lciCnpjCpf.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.lciCnpjCpf.TextSize = new System.Drawing.Size(51, 20);
            this.lciCnpjCpf.TextToControlDistance = 5;
            // 
            // lciRgIe
            // 
            this.lciRgIe.AllowHotTrack = false;
            this.lciRgIe.Control = this.txtRgIe;
            this.lciRgIe.CustomizationFormText = "IE";
            this.lciRgIe.Location = new System.Drawing.Point(290, 0);
            this.lciRgIe.MaxSize = new System.Drawing.Size(0, 30);
            this.lciRgIe.MinSize = new System.Drawing.Size(76, 30);
            this.lciRgIe.Name = "lciRgIe";
            this.lciRgIe.Size = new System.Drawing.Size(191, 30);
            this.lciRgIe.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.lciRgIe.Text = "IE";
            this.lciRgIe.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.lciRgIe.TextSize = new System.Drawing.Size(20, 20);
            this.lciRgIe.TextToControlDistance = 5;
            // 
            // layoutControlItem7
            // 
            this.layoutControlItem7.AllowHotTrack = false;
            this.layoutControlItem7.Control = this.textEdit14;
            this.layoutControlItem7.CustomizationFormText = "PIS";
            this.layoutControlItem7.Location = new System.Drawing.Point(481, 0);
            this.layoutControlItem7.MaxSize = new System.Drawing.Size(0, 30);
            this.layoutControlItem7.MinSize = new System.Drawing.Size(76, 30);
            this.layoutControlItem7.Name = "layoutControlItem7";
            this.layoutControlItem7.Size = new System.Drawing.Size(197, 30);
            this.layoutControlItem7.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem7.Text = "PIS";
            this.layoutControlItem7.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem7.TextSize = new System.Drawing.Size(20, 20);
            this.layoutControlItem7.TextToControlDistance = 5;
            // 
            // layoutControlItem18
            // 
            this.layoutControlItem18.AllowHotTrack = false;
            this.layoutControlItem18.Control = this.lkpCartorios;
            this.layoutControlItem18.CustomizationFormText = "Cart�rio";
            this.layoutControlItem18.Location = new System.Drawing.Point(0, 30);
            this.layoutControlItem18.MaxSize = new System.Drawing.Size(0, 30);
            this.layoutControlItem18.MinSize = new System.Drawing.Size(76, 30);
            this.layoutControlItem18.Name = "layoutControlItem18";
            this.layoutControlItem18.Size = new System.Drawing.Size(426, 30);
            this.layoutControlItem18.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem18.Text = "Cart�rio";
            this.layoutControlItem18.TextSize = new System.Drawing.Size(55, 13);
            // 
            // lciNascimento
            // 
            this.lciNascimento.AllowHotTrack = false;
            this.lciNascimento.Control = this.dteNascimento;
            this.lciNascimento.CustomizationFormText = "Nascimento";
            this.lciNascimento.Location = new System.Drawing.Point(426, 30);
            this.lciNascimento.MinSize = new System.Drawing.Size(80, 30);
            this.lciNascimento.Name = "lciNascimento";
            this.lciNascimento.Size = new System.Drawing.Size(252, 30);
            this.lciNascimento.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.lciNascimento.Text = "Nascimento";
            this.lciNascimento.TextSize = new System.Drawing.Size(55, 13);
            // 
            // layoutControlGroup5
            // 
            this.layoutControlGroup5.CustomizationFormText = "&2-Outros";
            this.layoutControlGroup5.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlGroup7});
            this.layoutControlGroup5.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroup5.Name = "layoutControlGroup5";
            this.layoutControlGroup5.Size = new System.Drawing.Size(702, 335);
            this.layoutControlGroup5.Text = "&2-Outros";
            // 
            // layoutControlGroup7
            // 
            this.layoutControlGroup7.CustomizationFormText = ":: Financeiro ::";
            this.layoutControlGroup7.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItem4,
            this.layoutControlItem20,
            this.layoutControlItem21,
            this.layoutControlItem22,
            this.emptySpaceItem2,
            this.layoutControlItem23,
            this.layoutControlItem24,
            this.layoutControlItem25});
            this.layoutControlGroup7.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroup7.Name = "layoutControlGroup7";
            this.layoutControlGroup7.Size = new System.Drawing.Size(702, 335);
            this.layoutControlGroup7.Spacing = new DevExpress.XtraLayout.Utils.Padding(2, 2, 5, 2);
            this.layoutControlGroup7.Text = ":: Financeiro ::";
            // 
            // layoutControlItem4
            // 
            this.layoutControlItem4.AllowHotTrack = false;
            this.layoutControlItem4.Control = this.lkpBancos;
            this.layoutControlItem4.CustomizationFormText = "Banco";
            this.layoutControlItem4.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItem4.Name = "layoutControlItem4";
            this.layoutControlItem4.Size = new System.Drawing.Size(678, 24);
            this.layoutControlItem4.Text = "Banco";
            this.layoutControlItem4.TextSize = new System.Drawing.Size(55, 13);
            // 
            // layoutControlItem20
            // 
            this.layoutControlItem20.AllowHotTrack = false;
            this.layoutControlItem20.Control = this.textEdit15;
            this.layoutControlItem20.CustomizationFormText = "Ag�ncia";
            this.layoutControlItem20.Location = new System.Drawing.Point(0, 24);
            this.layoutControlItem20.Name = "layoutControlItem20";
            this.layoutControlItem20.Size = new System.Drawing.Size(243, 266);
            this.layoutControlItem20.Text = "Ag�ncia";
            this.layoutControlItem20.TextSize = new System.Drawing.Size(55, 13);
            // 
            // layoutControlItem21
            // 
            this.layoutControlItem21.AllowHotTrack = false;
            this.layoutControlItem21.Control = this.textEdit16;
            this.layoutControlItem21.CustomizationFormText = "Conta";
            this.layoutControlItem21.Location = new System.Drawing.Point(346, 24);
            this.layoutControlItem21.Name = "layoutControlItem21";
            this.layoutControlItem21.Size = new System.Drawing.Size(242, 266);
            this.layoutControlItem21.Text = "Conta";
            this.layoutControlItem21.TextSize = new System.Drawing.Size(55, 13);
            // 
            // layoutControlItem22
            // 
            this.layoutControlItem22.AllowHotTrack = false;
            this.layoutControlItem22.Control = this.textEdit17;
            this.layoutControlItem22.CustomizationFormText = "layoutControlItem22";
            this.layoutControlItem22.Location = new System.Drawing.Point(250, 24);
            this.layoutControlItem22.Name = "layoutControlItem22";
            this.layoutControlItem22.Size = new System.Drawing.Size(83, 266);
            this.layoutControlItem22.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem22.TextVisible = false;
            // 
            // emptySpaceItem2
            // 
            this.emptySpaceItem2.AllowHotTrack = false;
            this.emptySpaceItem2.CustomizationFormText = "emptySpaceItem2";
            this.emptySpaceItem2.Location = new System.Drawing.Point(333, 24);
            this.emptySpaceItem2.Name = "emptySpaceItem2";
            this.emptySpaceItem2.Size = new System.Drawing.Size(13, 266);
            this.emptySpaceItem2.TextSize = new System.Drawing.Size(0, 0);
            // 
            // layoutControlItem23
            // 
            this.layoutControlItem23.AllowHotTrack = false;
            this.layoutControlItem23.Control = this.textEdit18;
            this.layoutControlItem23.CustomizationFormText = "layoutControlItem23";
            this.layoutControlItem23.Location = new System.Drawing.Point(595, 24);
            this.layoutControlItem23.Name = "layoutControlItem23";
            this.layoutControlItem23.Size = new System.Drawing.Size(83, 266);
            this.layoutControlItem23.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem23.TextVisible = false;
            // 
            // layoutControlItem24
            // 
            this.layoutControlItem24.AllowHotTrack = false;
            this.layoutControlItem24.Control = this.label1;
            this.layoutControlItem24.CustomizationFormText = "layoutControlItem24";
            this.layoutControlItem24.Location = new System.Drawing.Point(243, 24);
            this.layoutControlItem24.MinSize = new System.Drawing.Size(5, 20);
            this.layoutControlItem24.Name = "layoutControlItem24";
            this.layoutControlItem24.Padding = new DevExpress.XtraLayout.Utils.Padding(0, 0, 0, 0);
            this.layoutControlItem24.Size = new System.Drawing.Size(7, 266);
            this.layoutControlItem24.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem24.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem24.TextVisible = false;
            // 
            // layoutControlItem25
            // 
            this.layoutControlItem25.AllowHotTrack = false;
            this.layoutControlItem25.Control = this.label2;
            this.layoutControlItem25.CustomizationFormText = "layoutControlItem25";
            this.layoutControlItem25.Location = new System.Drawing.Point(588, 24);
            this.layoutControlItem25.MinSize = new System.Drawing.Size(5, 20);
            this.layoutControlItem25.Name = "layoutControlItem25";
            this.layoutControlItem25.Padding = new DevExpress.XtraLayout.Utils.Padding(0, 0, 0, 0);
            this.layoutControlItem25.Size = new System.Drawing.Size(7, 266);
            this.layoutControlItem25.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem25.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem25.TextVisible = false;
            // 
            // lcgUnidades
            // 
            this.lcgUnidades.CustomizationFormText = "3-Unidades";
            this.lcgUnidades.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItem6});
            this.lcgUnidades.Location = new System.Drawing.Point(0, 0);
            this.lcgUnidades.Name = "lcgUnidades";
            this.lcgUnidades.Size = new System.Drawing.Size(702, 335);
            this.lcgUnidades.Text = "3-Unidades";
            // 
            // layoutControlItem6
            // 
            this.layoutControlItem6.AllowHotTrack = false;
            this.layoutControlItem6.Control = this.gridControl1;
            this.layoutControlItem6.CustomizationFormText = "layoutControlItem6";
            this.layoutControlItem6.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItem6.Name = "layoutControlItem6";
            this.layoutControlItem6.Size = new System.Drawing.Size(702, 335);
            this.layoutControlItem6.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem6.TextVisible = false;
            // 
            // lcgHistorico
            // 
            this.lcgHistorico.CustomizationFormText = "Hist�rico";
            this.lcgHistorico.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItem5});
            this.lcgHistorico.Location = new System.Drawing.Point(0, 0);
            this.lcgHistorico.Name = "lcgHistorico";
            this.lcgHistorico.Size = new System.Drawing.Size(702, 335);
            this.lcgHistorico.Text = "Hist�rico";
            // 
            // layoutControlItem5
            // 
            this.layoutControlItem5.Control = this.memoEdit1;
            this.layoutControlItem5.CustomizationFormText = "layoutControlItem5";
            this.layoutControlItem5.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItem5.Name = "layoutControlItem5";
            this.layoutControlItem5.Size = new System.Drawing.Size(702, 335);
            this.layoutControlItem5.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem5.TextVisible = false;
            // 
            // layoutControlItem8
            // 
            this.layoutControlItem8.AllowHotTrack = false;
            this.layoutControlItem8.Control = this.textEdit2;
            this.layoutControlItem8.CustomizationFormText = "Altera��o";
            this.layoutControlItem8.Location = new System.Drawing.Point(0, 411);
            this.layoutControlItem8.Name = "layoutControlItem8";
            this.layoutControlItem8.Size = new System.Drawing.Size(728, 24);
            this.layoutControlItem8.Text = "Altera��o";
            this.layoutControlItem8.TextSize = new System.Drawing.Size(55, 13);
            // 
            // layoutControlGroup2
            // 
            this.layoutControlGroup2.CustomizationFormText = ":: Outros Dados ::";
            this.layoutControlGroup2.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroup2.Name = "layoutControlGroup2";
            this.layoutControlGroup2.OptionsItemText.TextToControlDistance = 5;
            this.layoutControlGroup2.Size = new System.Drawing.Size(547, 84);
            this.layoutControlGroup2.Text = ":: Outros Dados ::";
            // 
            // pESSOASTableAdapter
            // 
            this.pESSOASTableAdapter.ClearBeforeFill = true;
            this.pESSOASTableAdapter.GetNovosDados = ((System.Collections.ArrayList)(resources.GetObject("pESSOASTableAdapter.GetNovosDados")));
            this.pESSOASTableAdapter.TabelaDataTable = null;
            // 
            // fORNECEDORESTableAdapter
            // 
            this.fORNECEDORESTableAdapter.ClearBeforeFill = true;
            this.fORNECEDORESTableAdapter.GetNovosDados = ((System.Collections.ArrayList)(resources.GetObject("fORNECEDORESTableAdapter.GetNovosDados")));
            this.fORNECEDORESTableAdapter.TabelaDataTable = null;
            // 
            // bANCOSTableAdapter
            // 
            this.bANCOSTableAdapter.ClearBeforeFill = true;
            this.bANCOSTableAdapter.GetNovosDados = ((System.Collections.ArrayList)(resources.GetObject("bANCOSTableAdapter.GetNovosDados")));
            this.bANCOSTableAdapter.TabelaDataTable = null;
            // 
            // cPessoasCampos
            // 
            this.Autofill = false;
            this.Controls.Add(this.LayoutControl);
            this.Cursor = System.Windows.Forms.Cursors.Default;
            this.LookAndFeel.UseDefaultLookAndFeel = false;
            this.Name = "cPessoasCampos";
            this.Size = new System.Drawing.Size(748, 521);
            this.Load += new System.EventHandler(this.cPessoasCampos_Load);
            this.Controls.SetChildIndex(this.LayoutControl, 0);
            ((System.ComponentModel.ISupportInitialize)(this.BarManager_F)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.BarAndDockingController_F)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.BindingSource_F)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.StyleController_F)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LayoutControl)).EndInit();
            this.LayoutControl.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.memoEdit1.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit2.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.unidadesDaPessoaBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dPessoasCampos)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemImageComboBox1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.imageCollection1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ComboBoxPI)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.imagensPI1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit18.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit17.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dteNascimento.Properties.CalendarTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dteNascimento.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit16.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit15.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lkpCartorios.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.fORNECEDORESBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit14.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtRgIe.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lkpBancos.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.bANCOSBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit13.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit12.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit11.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit10.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit9.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lkpUF.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cIDADESBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lkpCidades.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit5.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit4.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lkpTipoPessoa.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tIPOSBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit1.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.EdNome.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCnpjCpf.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LayoutControlGroup)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tcgPessoas)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lcgBasico)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem10)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem11)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem12)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem19)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem13)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem9)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem14)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem15)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem16)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem17)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lciCnpjCpf)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lciRgIe)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem18)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lciNascimento)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem20)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem21)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem22)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem23)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem24)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem25)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lcgUnidades)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lcgHistorico)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem8)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup2)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private DevExpress.XtraLayout.LayoutControl LayoutControl;
        private DevExpress.XtraLayout.LayoutControlGroup LayoutControlGroup;
        private DevExpress.XtraEditors.TextEdit EdNome;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem2;
        private DevExpress.XtraEditors.LookUpEdit lkpTipoPessoa;
        private System.Windows.Forms.BindingSource tIPOSBindingSource;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem1;
        private DevExpress.XtraEditors.TextEdit textEdit1;
        private DevExpress.XtraEditors.TextEdit textEdit4;
        private DevExpress.XtraEditors.TextEdit textEdit9;
        private DevExpress.XtraEditors.LookUpEdit lkpCidades;
        private DevExpress.XtraEditors.TextEdit textEdit5;
        private DevExpress.XtraEditors.TextEdit textEdit12;
        private DevExpress.XtraEditors.TextEdit textEdit11;
        private DevExpress.XtraEditors.TextEdit textEdit10;
        private DevExpress.XtraEditors.TextEdit textEdit13;
        private DevExpress.XtraLayout.TabbedControlGroup tcgPessoas;
        private DevExpress.XtraLayout.LayoutControlGroup lcgBasico;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup3;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem10;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem11;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem12;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem13;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup4;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem9;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem14;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem15;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem16;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem17;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup5;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup2;
        private DevExpress.XtraEditors.LookUpEdit lkpBancos;
        private DevExpress.XtraLayout.LayoutControlItem lciCnpjCpf;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem4;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup6;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup7;
        private DevExpress.XtraEditors.LookUpEdit lkpCartorios;
        private DevExpress.XtraEditors.TextEdit textEdit14;
        private DevExpress.XtraEditors.TextEdit txtRgIe;
        private DevExpress.XtraLayout.LayoutControlItem lciRgIe;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem7;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem18;
        private DevExpress.XtraEditors.TextEdit textEdit16;
        private DevExpress.XtraEditors.TextEdit textEdit15;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem20;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem21;
        private DevExpress.XtraEditors.DateEdit dteNascimento;
        private DevExpress.XtraLayout.LayoutControlItem lciNascimento;
        private DevExpress.XtraEditors.TextEdit textEdit17;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem22;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem2;
        private DevExpress.XtraEditors.TextEdit textEdit18;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem23;
        private System.Windows.Forms.Label label1;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem24;
        private System.Windows.Forms.Label label2;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem25;
        private Cadastros.Pessoas.dPessoasCamposTableAdapters.PESSOASTableAdapter pESSOASTableAdapter;
        private System.Windows.Forms.BindingSource cIDADESBindingSource;
        private System.Windows.Forms.BindingSource fORNECEDORESBindingSource;
        private Cadastros.Pessoas.dPessoasCamposTableAdapters.FORNECEDORESTableAdapter fORNECEDORESTableAdapter;
        private System.Windows.Forms.BindingSource bANCOSBindingSource;
        private Cadastros.Pessoas.dPessoasCamposTableAdapters.BANCOSTableAdapter bANCOSTableAdapter;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem3;
        private DevExpress.XtraEditors.LookUpEdit lkpUF;
        private DevExpress.XtraLayout.LayoutControlGroup lcgUnidades;
        private DevExpress.XtraGrid.GridControl gridControl1;
        private System.Windows.Forms.BindingSource unidadesDaPessoaBindingSource;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView1;
        private DevExpress.XtraGrid.Columns.GridColumn colCONCodigo;
        private DevExpress.XtraGrid.Columns.GridColumn colCONNome;
        private DevExpress.XtraGrid.Columns.GridColumn colBLONome;
        private DevExpress.XtraGrid.Columns.GridColumn colAPTNumero;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem6;
        private DevExpress.XtraEditors.TextEdit textEdit2;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem8;
        /// <summary>
        /// 
        /// </summary>
        public dPessoasCampos dPessoasCampos;
        private DevExpress.XtraEditors.SimpleButton simpleButton1;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem19;
        private DevExpress.XtraGrid.Columns.GridColumn colAPTFormaPagtoX_FPG;
        private DevExpress.XtraGrid.Columns.GridColumn colProprietario;
        private DevExpress.XtraEditors.Repository.RepositoryItemImageComboBox repositoryItemImageComboBox1;
        private DevExpress.Utils.ImageCollection imageCollection1;
        private Framework.objetosNeon.ImagensPI imagensPI1;
        private DevExpress.XtraEditors.Repository.RepositoryItemImageComboBox ComboBoxPI;
        private DevExpress.XtraLayout.LayoutControlGroup lcgHistorico;
        private DevExpress.XtraEditors.MemoEdit memoEdit1;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem5;
        private DevExpress.XtraEditors.ButtonEdit txtCnpjCpf;
    }
}
