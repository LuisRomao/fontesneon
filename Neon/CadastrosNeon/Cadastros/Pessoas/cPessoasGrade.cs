/*
MR - 10/02/2015 12:00 -           - Implementado um novo bot�o "Notifica��o / Doc" que exibe o relat�rio com todas as pessoas com notifica��es e/ou documentos cadastrados (Altera��es indicadas por *** MRC - INICIO (10/02/2015 12:00) ***)
*/

using System;
using System.Data;

namespace Cadastros.Pessoas
{
    /// <summary>
    /// 
    /// </summary>
    public partial class cPessoasGrade : CompontesBasicos.ComponenteGradeNavegadorPesquisa
    {
        /// <summary>
        /// 
        /// </summary>
        public cPessoasGrade()
        {
            InitializeComponent();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected override void BtnExcluir_F_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            //Desabilitado excluir no modulo de pessoas
        }

        /// <summary>
        /// 
        /// </summary>
        protected override void Alterar()
        {
            if (BindingSourcePrincipal.Current == null)
                return;
            DataRowView DRV = (DataRowView)BindingSourcePrincipal.Current;
            dPessoasGrade.PESSOASRow rowPessoas = (dPessoasGrade.PESSOASRow)DRV.Row;
            /*
            if (Framework.DSCentral.ReceberDoAccess)
            {
                CompontesBasicos.FEspera.Espere("Atualizando dados Access");
                Utilitarios.MSAccess.fImportFromMsAccess.ImportarApartamentoDoMsAccess(rowPessoas.CONCodigo, rowPessoas.BLOCodigo, rowPessoas.APTNumero);
                CompontesBasicos.FEspera.Espere();
            };
            */ 
            AbreCampos(false, "Altera", "APT");
        }
        
        private dPessoasGrade.PESSOASRow LinhaMae {
            get {
                return (LinhaMae_F != null) ? (dPessoasGrade.PESSOASRow)LinhaMae_F : null;
            }
        }

        private void barButtonItem1_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {            
            //Boletos.Acordo.cAcordo Novo = new Boletos.Acordo.cAcordo(LinhaMae.APT);
            //CompontesBasicos.FormPrincipalBase.FormPrincipal.ShowMoludointerno(Novo, "Acordos "+LinhaMae.CONCodigo+" - "+LinhaMae.BLOCodigo+" - "+LinhaMae.APTNumero, CompontesBasicos.EstadosDosComponentes.JanelasAtivas, false);
        }

        private void cPessoasGrade_cargaInicial(object sender, EventArgs e)
        {
            
        }

        /// <summary>
        /// 
        /// </summary>
        public override void RefreshDados()
        {
            base.RefreshDados();
            dPessoasGrade.CargosTableAdapter.Fill(dPessoasGrade.Cargos);
        }

        private void cPessoasGrade_Load(object sender, EventArgs e)
        {
            dPessoasGrade.CargosTableAdapter.Fill(dPessoasGrade.Cargos);
        }

        //*** MRC - INICIO (10/02/2015 12:00) ***
        private void cmdNotificacaoDoc_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {

        }
        //*** MRC - TERMINO (10/02/2015 12:00) ***
    }
}

