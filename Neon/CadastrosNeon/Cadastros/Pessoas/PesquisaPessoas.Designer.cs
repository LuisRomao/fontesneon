namespace Cadastros.Pessoas
{
    partial class PesquisaPessoas
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.groupControl1 = new DevExpress.XtraEditors.GroupControl();
            this.lookupBlocosAptos_F1 = new Framework.Lookup.LookupBlocosAptos_F();
            this.cPessoasGrade1 = new Cadastros.Pessoas.cPessoasGrade();
            ((System.ComponentModel.ISupportInitialize)(this.StyleController_F)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl1)).BeginInit();
            this.groupControl1.SuspendLayout();
            this.SuspendLayout();
            // 
            // ToolTipController_F
            // 
            this.ToolTipController_F.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.ToolTipController_F.Appearance.ForeColor = System.Drawing.Color.Black;
            this.ToolTipController_F.Appearance.Options.UseBackColor = true;
            this.ToolTipController_F.Appearance.Options.UseFont = true;
            this.ToolTipController_F.Appearance.Options.UseForeColor = true;
            this.ToolTipController_F.Appearance.Options.UseTextOptions = true;
            this.ToolTipController_F.Appearance.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            // 
            // StyleController_F
            // 
            this.StyleController_F.AppearanceDisabled.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this.StyleController_F.AppearanceDisabled.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this.StyleController_F.AppearanceDisabled.Options.UseBackColor = true;
            this.StyleController_F.AppearanceDisabled.Options.UseForeColor = true;
            this.StyleController_F.AppearanceFocused.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.StyleController_F.AppearanceFocused.Options.UseBackColor = true;
            // 
            // groupControl1
            // 
            this.groupControl1.Controls.Add(this.lookupBlocosAptos_F1);
            this.groupControl1.Dock = System.Windows.Forms.DockStyle.Top;
            this.groupControl1.Location = new System.Drawing.Point(0, 0);
            this.groupControl1.Name = "groupControl1";
            this.groupControl1.Size = new System.Drawing.Size(1560, 77);
            this.groupControl1.TabIndex = 0;
            this.groupControl1.Text = "Identificação ::";
            // 
            // lookupBlocosAptos_F1
            // 
            this.lookupBlocosAptos_F1.APT_Sel = -1;
            this.lookupBlocosAptos_F1.Autofill = true;
            this.lookupBlocosAptos_F1.BLO_Sel = -1;
            this.lookupBlocosAptos_F1.CaixaAltaGeral = true;
            this.lookupBlocosAptos_F1.CON_sel = -1;
            this.lookupBlocosAptos_F1.CON_selrow = null;
            this.lookupBlocosAptos_F1.Cursor = System.Windows.Forms.Cursors.Default;
            this.lookupBlocosAptos_F1.Doc = System.Windows.Forms.DockStyle.Fill;
            this.lookupBlocosAptos_F1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lookupBlocosAptos_F1.Estado = CompontesBasicos.EstadosDosComponentes.Registrado;
            this.lookupBlocosAptos_F1.LinhaMae_F = null;
            this.lookupBlocosAptos_F1.Location = new System.Drawing.Point(2, 20);
            this.lookupBlocosAptos_F1.LookAndFeel.UseDefaultLookAndFeel = false;
            this.lookupBlocosAptos_F1.Name = "lookupBlocosAptos_F1";
            this.lookupBlocosAptos_F1.NivelCONOculto = 2;
            this.lookupBlocosAptos_F1.Size = new System.Drawing.Size(1556, 55);
            this.lookupBlocosAptos_F1.somenteleitura = false;
            this.lookupBlocosAptos_F1.TabIndex = 0;
            this.lookupBlocosAptos_F1.TableAdapterPrincipal = null;
            this.lookupBlocosAptos_F1.Titulo = null;
            this.lookupBlocosAptos_F1.alterado += new System.EventHandler(this.lookupBlocosAptos_F1_alterado);
            // 
            // cPessoasGrade1
            // 
            this.cPessoasGrade1.Autofill = true;
            this.cPessoasGrade1.BotaoAlterar = true;
            this.cPessoasGrade1.BotaoExcluir = true;
            this.cPessoasGrade1.BotaoIncluir = true;
            this.cPessoasGrade1.CaixaAltaGeral = true;
            this.cPessoasGrade1.CampoTab = "Apartamento";
            this.cPessoasGrade1.ComponenteCampos = typeof(Cadastros.Apartamentos.cApartamentosCampos);
            this.cPessoasGrade1.Cursor = System.Windows.Forms.Cursors.Default;
            this.cPessoasGrade1.dataLayout = new System.DateTime(((long)(0)));
            this.cPessoasGrade1.Doc = System.Windows.Forms.DockStyle.Fill;
            this.cPessoasGrade1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.cPessoasGrade1.EmEdicao = false;
            this.cPessoasGrade1.Estado = CompontesBasicos.EstadosDosComponentes.Registrado;
            this.cPessoasGrade1.EstadoComponenteCampos = CompontesBasicos.EstadosDosComponentes.JanelasAtivas;
            this.cPessoasGrade1.Funcionalidade = "CADASTRO";
            this.cPessoasGrade1.LinhaMae_F = null;
            this.cPessoasGrade1.Location = new System.Drawing.Point(0, 77);
            this.cPessoasGrade1.LookAndFeel.UseDefaultLookAndFeel = false;
            this.cPessoasGrade1.Name = "cPessoasGrade1";
            this.cPessoasGrade1.NivelLeitura = 2;
            this.cPessoasGrade1.PrefixoTab = "APT:";
            this.cPessoasGrade1.RefreshDadosLiberado = CompontesBasicos.ComponenteGradeNavegador.CriterioRefresh.sempre;
            this.cPessoasGrade1.Size = new System.Drawing.Size(1560, 744);
            this.cPessoasGrade1.somenteleitura = false;
            this.cPessoasGrade1.TabIndex = 1;
            this.cPessoasGrade1.TableAdapter = null;
            this.cPessoasGrade1.TableAdapterPrincipal = null;
            this.cPessoasGrade1.Titulo = null;
            // 
            // PesquisaPessoas
            // 
            this.Controls.Add(this.cPessoasGrade1);
            this.Controls.Add(this.groupControl1);
            this.Doc = System.Windows.Forms.DockStyle.Fill;
            this.LookAndFeel.UseDefaultLookAndFeel = false;
            this.Name = "PesquisaPessoas";
            this.Size = new System.Drawing.Size(1560, 821);
            this.Load += new System.EventHandler(this.PesquisaPessoas_Load);
            ((System.ComponentModel.ISupportInitialize)(this.StyleController_F)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl1)).EndInit();
            this.groupControl1.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraEditors.GroupControl groupControl1;
        private Framework.Lookup.LookupBlocosAptos_F lookupBlocosAptos_F1;
        private cPessoasGrade cPessoasGrade1;
    }
}
