namespace Cadastros.Pessoas
{
    partial class cPessoasGrade
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(cPessoasGrade));
            this.dPessoasGrade = new Cadastros.Pessoas.dPessoasGrade();
            this.pESSOASTableAdapter = new Cadastros.Pessoas.dPessoasGradeTableAdapters.PESSOASTableAdapter();
            this.colPESNome = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colPESTipo = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colPESCpfCnpj = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colPESRgIe = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colPESPis = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colPESEndereco = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colPESBairro = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colCIDNome = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colCIDUf = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colPESCep = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colPESDataNascimento = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colPESFone1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colPESFone2 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colPESFone3 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colPESFone4 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colPESEmail = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colFRNFantasia = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colAgencia = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colConta = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colBCONome = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colCONCODIGO = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colAPTNUMERO = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colBLOCO = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colUsuarioInternet = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSenhaInternet = new DevExpress.XtraGrid.Columns.GridColumn();
            this.barButtonItem1 = new DevExpress.XtraBars.BarButtonItem();
            this.colProprietario = new DevExpress.XtraGrid.Columns.GridColumn();
            this.ComboBoxPI = new DevExpress.XtraEditors.Repository.RepositoryItemImageComboBox();
            this.imagensPI1 = new Framework.objetosNeon.ImagensPI();
            this.colAPTRegistro = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colAPT = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colPES = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colcopiaAPT = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemLookUpEdit1 = new DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit();
            this.bindingSourceCargos = new System.Windows.Forms.BindingSource(this.components);
            this.cmdNotificacaoDoc = new DevExpress.XtraBars.BarButtonItem();
            this.colPESCpfCnpjViaSite = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colPESCpfErrado = new DevExpress.XtraGrid.Columns.GridColumn();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl1)).BeginInit();
            this.groupControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.LayoutPesquisa_F)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.PopImprimir_F)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.BarManager_F)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.PopExportar_F)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.GridControl_F)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.GridView_F)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.BindingSource_F)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.StyleController_F)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dPessoasGrade)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ComboBoxPI)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.imagensPI1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemLookUpEdit1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.bindingSourceCargos)).BeginInit();
            this.SuspendLayout();
            // 
            // groupControl1
            // 
            this.groupControl1.Size = new System.Drawing.Size(1560, 84);
            // 
            // LayoutPesquisa_F
            // 
            this.LayoutPesquisa_F.Size = new System.Drawing.Size(1556, 84);
            // 
            // barNavegacao_F
            // 
            this.barNavegacao_F.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(this.barButtonItem1),
            new DevExpress.XtraBars.LinkPersistInfo(this.cmdNotificacaoDoc)});
            this.barNavegacao_F.OptionsBar.AllowQuickCustomization = false;
            this.barNavegacao_F.OptionsBar.DisableClose = true;
            this.barNavegacao_F.OptionsBar.DisableCustomization = true;
            this.barNavegacao_F.OptionsBar.DrawDragBorder = false;
            this.barNavegacao_F.OptionsBar.DrawSizeGrip = true;
            this.barNavegacao_F.OptionsBar.RotateWhenVertical = false;
            this.barNavegacao_F.OptionsBar.UseWholeRow = true;
            // 
            // BarManager_F
            // 
            this.BarManager_F.Items.AddRange(new DevExpress.XtraBars.BarItem[] {
            this.barButtonItem1,
            this.cmdNotificacaoDoc});
            this.BarManager_F.MaxItemId = 25;
            // 
            // BtnAlterar_F
            // 
            this.BtnAlterar_F.Enabled = false;
            // 
            // BtnExcluir_F
            // 
            this.BtnExcluir_F.Enabled = false;
            // 
            // barDataBase_F
            // 
            this.barDataBase_F.OptionsBar.AllowQuickCustomization = false;
            this.barDataBase_F.OptionsBar.DisableClose = true;
            this.barDataBase_F.OptionsBar.DisableCustomization = true;
            this.barDataBase_F.OptionsBar.DrawDragBorder = false;
            this.barDataBase_F.OptionsBar.UseWholeRow = true;
            // 
            // GridControl_F
            // 
            this.GridControl_F.EmbeddedNavigator.Buttons.Append.Visible = false;
            this.GridControl_F.EmbeddedNavigator.Buttons.CancelEdit.Visible = false;
            this.GridControl_F.EmbeddedNavigator.Buttons.Edit.Visible = false;
            this.GridControl_F.EmbeddedNavigator.Buttons.EndEdit.Visible = false;
            this.GridControl_F.EmbeddedNavigator.Buttons.Remove.Visible = false;
            this.GridControl_F.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.ComboBoxPI,
            this.repositoryItemLookUpEdit1});
            this.GridControl_F.Size = new System.Drawing.Size(1560, 677);
            // 
            // GridView_F
            // 
            this.GridView_F.Appearance.ColumnFilterButton.BackColor = System.Drawing.Color.Orange;
            this.GridView_F.Appearance.ColumnFilterButton.BorderColor = System.Drawing.Color.Orange;
            this.GridView_F.Appearance.ColumnFilterButton.ForeColor = System.Drawing.Color.DarkOrange;
            this.GridView_F.Appearance.ColumnFilterButton.Options.UseBackColor = true;
            this.GridView_F.Appearance.ColumnFilterButton.Options.UseBorderColor = true;
            this.GridView_F.Appearance.ColumnFilterButton.Options.UseForeColor = true;
            this.GridView_F.Appearance.ColumnFilterButtonActive.BackColor = System.Drawing.Color.DarkOrange;
            this.GridView_F.Appearance.ColumnFilterButtonActive.BorderColor = System.Drawing.Color.DarkOrange;
            this.GridView_F.Appearance.ColumnFilterButtonActive.Options.UseBackColor = true;
            this.GridView_F.Appearance.ColumnFilterButtonActive.Options.UseBorderColor = true;
            this.GridView_F.Appearance.Empty.BackColor = System.Drawing.Color.LightSkyBlue;
            this.GridView_F.Appearance.Empty.BackColor2 = System.Drawing.Color.SkyBlue;
            this.GridView_F.Appearance.Empty.GradientMode = System.Drawing.Drawing2D.LinearGradientMode.BackwardDiagonal;
            this.GridView_F.Appearance.Empty.Options.UseBackColor = true;
            this.GridView_F.Appearance.EvenRow.BackColor = System.Drawing.Color.Linen;
            this.GridView_F.Appearance.EvenRow.BackColor2 = System.Drawing.Color.AntiqueWhite;
            this.GridView_F.Appearance.EvenRow.GradientMode = System.Drawing.Drawing2D.LinearGradientMode.ForwardDiagonal;
            this.GridView_F.Appearance.EvenRow.Options.UseBackColor = true;
            this.GridView_F.Appearance.FilterCloseButton.BackColor = System.Drawing.Color.Orange;
            this.GridView_F.Appearance.FilterCloseButton.BorderColor = System.Drawing.Color.Orange;
            this.GridView_F.Appearance.FilterCloseButton.Options.UseBackColor = true;
            this.GridView_F.Appearance.FilterCloseButton.Options.UseBorderColor = true;
            this.GridView_F.Appearance.FilterPanel.BackColor = System.Drawing.Color.DarkOrange;
            this.GridView_F.Appearance.FilterPanel.BackColor2 = System.Drawing.Color.Orange;
            this.GridView_F.Appearance.FilterPanel.ForeColor = System.Drawing.Color.Black;
            this.GridView_F.Appearance.FilterPanel.GradientMode = System.Drawing.Drawing2D.LinearGradientMode.BackwardDiagonal;
            this.GridView_F.Appearance.FilterPanel.Options.UseBackColor = true;
            this.GridView_F.Appearance.FilterPanel.Options.UseForeColor = true;
            this.GridView_F.Appearance.FocusedCell.BackColor = System.Drawing.Color.Yellow;
            this.GridView_F.Appearance.FocusedCell.Options.UseBackColor = true;
            this.GridView_F.Appearance.FocusedRow.BackColor = System.Drawing.Color.Orange;
            this.GridView_F.Appearance.FocusedRow.BackColor2 = System.Drawing.Color.White;
            this.GridView_F.Appearance.FocusedRow.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
            this.GridView_F.Appearance.FocusedRow.ForeColor = System.Drawing.Color.Black;
            this.GridView_F.Appearance.FocusedRow.GradientMode = System.Drawing.Drawing2D.LinearGradientMode.Vertical;
            this.GridView_F.Appearance.FocusedRow.Options.UseBackColor = true;
            this.GridView_F.Appearance.FocusedRow.Options.UseFont = true;
            this.GridView_F.Appearance.FocusedRow.Options.UseForeColor = true;
            this.GridView_F.Appearance.FocusedRow.Options.UseTextOptions = true;
            this.GridView_F.Appearance.FocusedRow.TextOptions.HotkeyPrefix = DevExpress.Utils.HKeyPrefix.Show;
            this.GridView_F.Appearance.FocusedRow.TextOptions.Trimming = DevExpress.Utils.Trimming.EllipsisCharacter;
            this.GridView_F.Appearance.FooterPanel.BackColor = System.Drawing.Color.Orange;
            this.GridView_F.Appearance.FooterPanel.BorderColor = System.Drawing.Color.Orange;
            this.GridView_F.Appearance.FooterPanel.Options.UseBackColor = true;
            this.GridView_F.Appearance.FooterPanel.Options.UseBorderColor = true;
            this.GridView_F.Appearance.GroupButton.BackColor = System.Drawing.Color.Wheat;
            this.GridView_F.Appearance.GroupButton.BorderColor = System.Drawing.Color.Wheat;
            this.GridView_F.Appearance.GroupButton.Options.UseBackColor = true;
            this.GridView_F.Appearance.GroupButton.Options.UseBorderColor = true;
            this.GridView_F.Appearance.GroupFooter.BackColor = System.Drawing.Color.Wheat;
            this.GridView_F.Appearance.GroupFooter.BorderColor = System.Drawing.Color.Wheat;
            this.GridView_F.Appearance.GroupFooter.Options.UseBackColor = true;
            this.GridView_F.Appearance.GroupFooter.Options.UseBorderColor = true;
            this.GridView_F.Appearance.GroupPanel.BackColor = System.Drawing.Color.RoyalBlue;
            this.GridView_F.Appearance.GroupPanel.ForeColor = System.Drawing.Color.White;
            this.GridView_F.Appearance.GroupPanel.Options.UseBackColor = true;
            this.GridView_F.Appearance.GroupPanel.Options.UseForeColor = true;
            this.GridView_F.Appearance.GroupRow.BackColor = System.Drawing.Color.Wheat;
            this.GridView_F.Appearance.GroupRow.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Bold);
            this.GridView_F.Appearance.GroupRow.Options.UseBackColor = true;
            this.GridView_F.Appearance.GroupRow.Options.UseFont = true;
            this.GridView_F.Appearance.HeaderPanel.BackColor = System.Drawing.Color.Orange;
            this.GridView_F.Appearance.HeaderPanel.BorderColor = System.Drawing.Color.Orange;
            this.GridView_F.Appearance.HeaderPanel.Options.UseBackColor = true;
            this.GridView_F.Appearance.HeaderPanel.Options.UseBorderColor = true;
            this.GridView_F.Appearance.HideSelectionRow.BackColor = System.Drawing.Color.LightSlateGray;
            this.GridView_F.Appearance.HideSelectionRow.Options.UseBackColor = true;
            this.GridView_F.Appearance.HorzLine.BackColor = System.Drawing.Color.Tan;
            this.GridView_F.Appearance.HorzLine.Options.UseBackColor = true;
            this.GridView_F.Appearance.OddRow.BackColor = System.Drawing.Color.White;
            this.GridView_F.Appearance.OddRow.Options.UseBackColor = true;
            this.GridView_F.Appearance.Preview.BackColor = System.Drawing.Color.Khaki;
            this.GridView_F.Appearance.Preview.BackColor2 = System.Drawing.Color.Cornsilk;
            this.GridView_F.Appearance.Preview.Font = new System.Drawing.Font("Tahoma", 7.5F);
            this.GridView_F.Appearance.Preview.GradientMode = System.Drawing.Drawing2D.LinearGradientMode.BackwardDiagonal;
            this.GridView_F.Appearance.Preview.Options.UseBackColor = true;
            this.GridView_F.Appearance.Preview.Options.UseFont = true;
            this.GridView_F.Appearance.RowSeparator.BackColor = System.Drawing.Color.LightSkyBlue;
            this.GridView_F.Appearance.RowSeparator.Options.UseBackColor = true;
            this.GridView_F.Appearance.SelectedRow.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(255)))), ((int)(((byte)(128)))));
            this.GridView_F.Appearance.SelectedRow.Options.UseBackColor = true;
            this.GridView_F.Appearance.TopNewRow.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
            this.GridView_F.Appearance.TopNewRow.Options.UseFont = true;
            this.GridView_F.Appearance.VertLine.BackColor = System.Drawing.Color.Tan;
            this.GridView_F.Appearance.VertLine.Options.UseBackColor = true;
            this.GridView_F.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colPESNome,
            this.colPESTipo,
            this.colPESCpfCnpj,
            this.colPESRgIe,
            this.colPESPis,
            this.colPESEndereco,
            this.colPESBairro,
            this.colCIDNome,
            this.colCIDUf,
            this.colPESCep,
            this.colPESDataNascimento,
            this.colPESFone1,
            this.colPESFone2,
            this.colPESFone3,
            this.colPESFone4,
            this.colPESEmail,
            this.colFRNFantasia,
            this.colAgencia,
            this.colConta,
            this.colBCONome,
            this.colCONCODIGO,
            this.colAPTNUMERO,
            this.colBLOCO,
            this.colUsuarioInternet,
            this.colSenhaInternet,
            this.colProprietario,
            this.colAPTRegistro,
            this.colAPT,
            this.colPES,
            this.colcopiaAPT,
            this.colPESCpfCnpjViaSite,
            this.colPESCpfErrado});
            this.GridView_F.FocusRectStyle = DevExpress.XtraGrid.Views.Grid.DrawFocusRectStyle.RowFocus;
            this.GridView_F.OptionsBehavior.AllowIncrementalSearch = true;
            this.GridView_F.OptionsBehavior.Editable = false;
            this.GridView_F.OptionsLayout.Columns.StoreAllOptions = true;
            this.GridView_F.OptionsLayout.Columns.StoreAppearance = true;
            this.GridView_F.OptionsLayout.StoreAllOptions = true;
            this.GridView_F.OptionsLayout.StoreAppearance = true;
            this.GridView_F.OptionsNavigation.AutoFocusNewRow = true;
            this.GridView_F.OptionsNavigation.EnterMoveNextColumn = true;
            this.GridView_F.OptionsPrint.EnableAppearanceEvenRow = true;
            this.GridView_F.OptionsPrint.EnableAppearanceOddRow = true;
            this.GridView_F.OptionsPrint.ExpandAllDetails = true;
            this.GridView_F.OptionsSelection.InvertSelection = true;
            this.GridView_F.OptionsView.ColumnAutoWidth = false;
            this.GridView_F.OptionsView.EnableAppearanceEvenRow = true;
            this.GridView_F.OptionsView.EnableAppearanceOddRow = true;
            this.GridView_F.OptionsView.GroupFooterShowMode = DevExpress.XtraGrid.Views.Grid.GroupFooterShowMode.VisibleAlways;
            this.GridView_F.OptionsView.ShowAutoFilterRow = true;
            this.GridView_F.OptionsView.ShowButtonMode = DevExpress.XtraGrid.Views.Base.ShowButtonModeEnum.ShowForFocusedCell;
            this.GridView_F.OptionsView.ShowFooter = true;
            this.GridView_F.OptionsView.ShowGroupPanel = false;
            this.GridView_F.SortInfo.AddRange(new DevExpress.XtraGrid.Columns.GridColumnSortInfo[] {
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colCONCODIGO, DevExpress.Data.ColumnSortOrder.Ascending),
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colBLOCO, DevExpress.Data.ColumnSortOrder.Ascending),
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colAPTNUMERO, DevExpress.Data.ColumnSortOrder.Ascending)});
            // 
            // BindingSource_F
            // 
            this.BindingSource_F.DataMember = "PESSOAS";
            this.BindingSource_F.DataSource = this.dPessoasGrade;
            // 
            // ToolTipController_F
            // 
            this.ToolTipController_F.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(222)))), ((int)(((byte)(237)))), ((int)(((byte)(250)))));
            this.ToolTipController_F.Appearance.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(20)))), ((int)(((byte)(69)))), ((int)(((byte)(141)))));
            this.ToolTipController_F.Appearance.Options.UseBackColor = true;
            this.ToolTipController_F.Appearance.Options.UseFont = true;
            this.ToolTipController_F.Appearance.Options.UseForeColor = true;
            this.ToolTipController_F.Appearance.Options.UseTextOptions = true;
            this.ToolTipController_F.Appearance.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            // 
            // StyleController_F
            // 
            this.StyleController_F.AppearanceDisabled.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this.StyleController_F.AppearanceDisabled.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this.StyleController_F.AppearanceDisabled.Options.UseBackColor = true;
            this.StyleController_F.AppearanceDisabled.Options.UseForeColor = true;
            this.StyleController_F.AppearanceFocused.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.StyleController_F.AppearanceFocused.Options.UseBackColor = true;
            this.StyleController_F.LookAndFeel.SkinName = "Lilian";
            this.StyleController_F.LookAndFeel.UseDefaultLookAndFeel = false;
            // 
            // dPessoasGrade
            // 
            this.dPessoasGrade.DataSetName = "dPessoasGrade";
            this.dPessoasGrade.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // pESSOASTableAdapter
            // 
            this.pESSOASTableAdapter.ClearBeforeFill = true;
            this.pESSOASTableAdapter.GetNovosDados = ((System.Collections.ArrayList)(resources.GetObject("pESSOASTableAdapter.GetNovosDados")));
            this.pESSOASTableAdapter.TabelaDataTable = null;
            // 
            // colPESNome
            // 
            this.colPESNome.Caption = "Nome";
            this.colPESNome.FieldName = "PESNome";
            this.colPESNome.Name = "colPESNome";
            this.colPESNome.Visible = true;
            this.colPESNome.VisibleIndex = 4;
            this.colPESNome.Width = 190;
            // 
            // colPESTipo
            // 
            this.colPESTipo.Caption = "F�sica/Jur�dica";
            this.colPESTipo.FieldName = "PESTipo";
            this.colPESTipo.Name = "colPESTipo";
            this.colPESTipo.Width = 40;
            // 
            // colPESCpfCnpj
            // 
            this.colPESCpfCnpj.Caption = "CPF/CNPJ";
            this.colPESCpfCnpj.FieldName = "PESCpfCnpj";
            this.colPESCpfCnpj.Name = "colPESCpfCnpj";
            // 
            // colPESRgIe
            // 
            this.colPESRgIe.Caption = "RG/IE";
            this.colPESRgIe.FieldName = "PESRgIe";
            this.colPESRgIe.Name = "colPESRgIe";
            // 
            // colPESPis
            // 
            this.colPESPis.Caption = "PIS";
            this.colPESPis.FieldName = "PESPis";
            this.colPESPis.Name = "colPESPis";
            // 
            // colPESEndereco
            // 
            this.colPESEndereco.Caption = "Endereco";
            this.colPESEndereco.FieldName = "PESEndereco";
            this.colPESEndereco.Name = "colPESEndereco";
            this.colPESEndereco.Visible = true;
            this.colPESEndereco.VisibleIndex = 5;
            this.colPESEndereco.Width = 91;
            // 
            // colPESBairro
            // 
            this.colPESBairro.Caption = "Bairro";
            this.colPESBairro.FieldName = "PESBairro";
            this.colPESBairro.Name = "colPESBairro";
            this.colPESBairro.Visible = true;
            this.colPESBairro.VisibleIndex = 6;
            this.colPESBairro.Width = 58;
            // 
            // colCIDNome
            // 
            this.colCIDNome.Caption = "Cidade";
            this.colCIDNome.FieldName = "CIDNome";
            this.colCIDNome.Name = "colCIDNome";
            this.colCIDNome.Visible = true;
            this.colCIDNome.VisibleIndex = 7;
            this.colCIDNome.Width = 77;
            // 
            // colCIDUf
            // 
            this.colCIDUf.Caption = "UF";
            this.colCIDUf.FieldName = "CIDUf";
            this.colCIDUf.Name = "colCIDUf";
            this.colCIDUf.Width = 30;
            // 
            // colPESCep
            // 
            this.colPESCep.Caption = "CEP";
            this.colPESCep.FieldName = "PESCep";
            this.colPESCep.Name = "colPESCep";
            // 
            // colPESDataNascimento
            // 
            this.colPESDataNascimento.Caption = "Dt Nascimento";
            this.colPESDataNascimento.FieldName = "PESDataNascimento";
            this.colPESDataNascimento.Name = "colPESDataNascimento";
            // 
            // colPESFone1
            // 
            this.colPESFone1.Caption = "Telefone 1";
            this.colPESFone1.FieldName = "PESFone1";
            this.colPESFone1.Name = "colPESFone1";
            this.colPESFone1.Visible = true;
            this.colPESFone1.VisibleIndex = 8;
            this.colPESFone1.Width = 113;
            // 
            // colPESFone2
            // 
            this.colPESFone2.Caption = "Telefone 2";
            this.colPESFone2.FieldName = "PESFone2";
            this.colPESFone2.Name = "colPESFone2";
            // 
            // colPESFone3
            // 
            this.colPESFone3.Caption = "Telefone 3";
            this.colPESFone3.FieldName = "PESFone3";
            this.colPESFone3.Name = "colPESFone3";
            // 
            // colPESFone4
            // 
            this.colPESFone4.Caption = "Telefone 4";
            this.colPESFone4.FieldName = "PESFone4";
            this.colPESFone4.Name = "colPESFone4";
            // 
            // colPESEmail
            // 
            this.colPESEmail.Caption = "Email";
            this.colPESEmail.FieldName = "PESEmail";
            this.colPESEmail.Name = "colPESEmail";
            this.colPESEmail.Width = 102;
            // 
            // colFRNFantasia
            // 
            this.colFRNFantasia.Caption = "Cart�rio";
            this.colFRNFantasia.FieldName = "FRNFantasia";
            this.colFRNFantasia.Name = "colFRNFantasia";
            // 
            // colAgencia
            // 
            this.colAgencia.Caption = "Ag�ncia";
            this.colAgencia.FieldName = "Agencia";
            this.colAgencia.Name = "colAgencia";
            this.colAgencia.OptionsColumn.ReadOnly = true;
            // 
            // colConta
            // 
            this.colConta.Caption = "Conta";
            this.colConta.FieldName = "Conta";
            this.colConta.Name = "colConta";
            this.colConta.OptionsColumn.ReadOnly = true;
            // 
            // colBCONome
            // 
            this.colBCONome.Caption = "Banco";
            this.colBCONome.FieldName = "BCONome";
            this.colBCONome.Name = "colBCONome";
            // 
            // colCONCODIGO
            // 
            this.colCONCODIGO.Caption = "Codcon";
            this.colCONCODIGO.FieldName = "CONCodigo";
            this.colCONCODIGO.Fixed = DevExpress.XtraGrid.Columns.FixedStyle.Left;
            this.colCONCODIGO.Name = "colCONCODIGO";
            this.colCONCODIGO.Visible = true;
            this.colCONCODIGO.VisibleIndex = 0;
            // 
            // colAPTNUMERO
            // 
            this.colAPTNUMERO.Caption = "Apto";
            this.colAPTNUMERO.FieldName = "APTNumero";
            this.colAPTNUMERO.Fixed = DevExpress.XtraGrid.Columns.FixedStyle.Left;
            this.colAPTNUMERO.Name = "colAPTNUMERO";
            this.colAPTNUMERO.Visible = true;
            this.colAPTNUMERO.VisibleIndex = 2;
            this.colAPTNUMERO.Width = 57;
            // 
            // colBLOCO
            // 
            this.colBLOCO.Caption = "Bloco";
            this.colBLOCO.FieldName = "BLOCO";
            this.colBLOCO.Fixed = DevExpress.XtraGrid.Columns.FixedStyle.Left;
            this.colBLOCO.Name = "colBLOCO";
            this.colBLOCO.OptionsColumn.ReadOnly = true;
            this.colBLOCO.Visible = true;
            this.colBLOCO.VisibleIndex = 1;
            this.colBLOCO.Width = 64;
            // 
            // colUsuarioInternet
            // 
            this.colUsuarioInternet.Caption = "Usuario";
            this.colUsuarioInternet.FieldName = "UsuarioInternet";
            this.colUsuarioInternet.Name = "colUsuarioInternet";
            this.colUsuarioInternet.Visible = true;
            this.colUsuarioInternet.VisibleIndex = 9;
            this.colUsuarioInternet.Width = 79;
            // 
            // colSenhaInternet
            // 
            this.colSenhaInternet.Caption = "Senha";
            this.colSenhaInternet.FieldName = "SenhaInternet";
            this.colSenhaInternet.Name = "colSenhaInternet";
            this.colSenhaInternet.Visible = true;
            this.colSenhaInternet.VisibleIndex = 10;
            // 
            // barButtonItem1
            // 
            this.barButtonItem1.Caption = "Acordo";
            this.barButtonItem1.Id = 23;
            this.barButtonItem1.Name = "barButtonItem1";
            this.barButtonItem1.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.barButtonItem1_ItemClick);
            // 
            // colProprietario
            // 
            this.colProprietario.Caption = "Proprietario";
            this.colProprietario.ColumnEdit = this.ComboBoxPI;
            this.colProprietario.FieldName = "Proprietario";
            this.colProprietario.Fixed = DevExpress.XtraGrid.Columns.FixedStyle.Left;
            this.colProprietario.Name = "colProprietario";
            this.colProprietario.OptionsColumn.ShowCaption = false;
            this.colProprietario.Visible = true;
            this.colProprietario.VisibleIndex = 3;
            this.colProprietario.Width = 24;
            // 
            // ComboBoxPI
            // 
            this.ComboBoxPI.AutoHeight = false;
            this.ComboBoxPI.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.ComboBoxPI.GlyphAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.ComboBoxPI.Items.AddRange(new DevExpress.XtraEditors.Controls.ImageComboBoxItem[] {
            new DevExpress.XtraEditors.Controls.ImageComboBoxItem("Proprietario", 1, 0),
            new DevExpress.XtraEditors.Controls.ImageComboBoxItem("Inquilino", 0, 1)});
            this.ComboBoxPI.Name = "ComboBoxPI";
            this.ComboBoxPI.SmallImages = this.imagensPI1;
            // 
            // imagensPI1
            // 
            this.imagensPI1.ImageStream = ((DevExpress.Utils.ImageCollectionStreamer)(resources.GetObject("imagensPI1.ImageStream")));
            // 
            // colAPTRegistro
            // 
            this.colAPTRegistro.Caption = "Registro APT";
            this.colAPTRegistro.FieldName = "APTRegistro";
            this.colAPTRegistro.Name = "colAPTRegistro";
            // 
            // colAPT
            // 
            this.colAPT.FieldName = "APT";
            this.colAPT.Name = "colAPT";
            // 
            // colPES
            // 
            this.colPES.FieldName = "PES";
            this.colPES.Name = "colPES";
            // 
            // colcopiaAPT
            // 
            this.colcopiaAPT.Caption = "Cargo";
            this.colcopiaAPT.ColumnEdit = this.repositoryItemLookUpEdit1;
            this.colcopiaAPT.FieldName = "copiaAPT";
            this.colcopiaAPT.FilterMode = DevExpress.XtraGrid.ColumnFilterMode.DisplayText;
            this.colcopiaAPT.Name = "colcopiaAPT";
            this.colcopiaAPT.SortMode = DevExpress.XtraGrid.ColumnSortMode.DisplayText;
            this.colcopiaAPT.Visible = true;
            this.colcopiaAPT.VisibleIndex = 11;
            // 
            // repositoryItemLookUpEdit1
            // 
            this.repositoryItemLookUpEdit1.AutoHeight = false;
            this.repositoryItemLookUpEdit1.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemLookUpEdit1.DataSource = this.bindingSourceCargos;
            this.repositoryItemLookUpEdit1.DisplayMember = "CGONome";
            this.repositoryItemLookUpEdit1.Name = "repositoryItemLookUpEdit1";
            this.repositoryItemLookUpEdit1.NullText = "--";
            this.repositoryItemLookUpEdit1.ValueMember = "CDR_APT";
            // 
            // bindingSourceCargos
            // 
            this.bindingSourceCargos.DataMember = "Cargos";
            this.bindingSourceCargos.DataSource = this.dPessoasGrade;
            // 
            // cmdNotificacaoDoc
            // 
            this.cmdNotificacaoDoc.Caption = "Notifica��o / Doc";
            this.cmdNotificacaoDoc.Glyph = ((System.Drawing.Image)(resources.GetObject("cmdNotificacaoDoc.Glyph")));
            this.cmdNotificacaoDoc.Id = 24;
            this.cmdNotificacaoDoc.LargeGlyph = ((System.Drawing.Image)(resources.GetObject("cmdNotificacaoDoc.LargeGlyph")));
            this.cmdNotificacaoDoc.LargeGlyphDisabled = ((System.Drawing.Image)(resources.GetObject("cmdNotificacaoDoc.LargeGlyphDisabled")));
            this.cmdNotificacaoDoc.Name = "cmdNotificacaoDoc";
            this.cmdNotificacaoDoc.PaintStyle = DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph;
            this.cmdNotificacaoDoc.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.cmdNotificacaoDoc_ItemClick);
            // 
            // colPESCpfCnpjViaSite
            // 
            this.colPESCpfCnpjViaSite.Caption = "Verificar CPF";
            this.colPESCpfCnpjViaSite.FieldName = "PESCpfCnpjViaSite";
            this.colPESCpfCnpjViaSite.Name = "colPESCpfCnpjViaSite";
            this.colPESCpfCnpjViaSite.Visible = true;
            this.colPESCpfCnpjViaSite.VisibleIndex = 12;
            // 
            // colPESCpfErrado
            // 
            this.colPESCpfErrado.Caption = "CPF errado";
            this.colPESCpfErrado.FieldName = "PESCpfErrado";
            this.colPESCpfErrado.Name = "colPESCpfErrado";
            this.colPESCpfErrado.Visible = true;
            this.colPESCpfErrado.VisibleIndex = 13;
            // 
            // cPessoasGrade
            // 
            this.Autofill = false;
            this.CampoTab = "Apartamento";
            this.ComponenteCampos = typeof(Cadastros.Apartamentos.cApartamentosCampos);
            this.Funcionalidade = "CADASTRO";
            this.LookAndFeel.UseDefaultLookAndFeel = false;
            this.Name = "cPessoasGrade";
            this.NivelLeitura = 2;
            this.PrefixoTab = "APT:";
            this.Size = new System.Drawing.Size(1560, 821);
            this.cargaInicial += new System.EventHandler(this.cPessoasGrade_cargaInicial);
            this.Load += new System.EventHandler(this.cPessoasGrade_Load);
            ((System.ComponentModel.ISupportInitialize)(this.groupControl1)).EndInit();
            this.groupControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.LayoutPesquisa_F)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.PopImprimir_F)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.BarManager_F)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.PopExportar_F)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.GridControl_F)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.GridView_F)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.BindingSource_F)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.StyleController_F)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dPessoasGrade)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ComboBoxPI)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.imagensPI1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemLookUpEdit1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.bindingSourceCargos)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private DevExpress.XtraGrid.Columns.GridColumn colPESNome;
        private DevExpress.XtraGrid.Columns.GridColumn colPESTipo;
        private DevExpress.XtraGrid.Columns.GridColumn colPESCpfCnpj;
        private DevExpress.XtraGrid.Columns.GridColumn colPESRgIe;
        private DevExpress.XtraGrid.Columns.GridColumn colPESPis;
        private DevExpress.XtraGrid.Columns.GridColumn colPESEndereco;
        private DevExpress.XtraGrid.Columns.GridColumn colPESBairro;
        private DevExpress.XtraGrid.Columns.GridColumn colCIDNome;
        private DevExpress.XtraGrid.Columns.GridColumn colCIDUf;
        private DevExpress.XtraGrid.Columns.GridColumn colPESCep;
        private DevExpress.XtraGrid.Columns.GridColumn colPESDataNascimento;
        private DevExpress.XtraGrid.Columns.GridColumn colPESFone1;
        private DevExpress.XtraGrid.Columns.GridColumn colPESFone2;
        private DevExpress.XtraGrid.Columns.GridColumn colPESFone3;
        private DevExpress.XtraGrid.Columns.GridColumn colPESFone4;
        private DevExpress.XtraGrid.Columns.GridColumn colPESEmail;
        private DevExpress.XtraGrid.Columns.GridColumn colFRNFantasia;
        private DevExpress.XtraGrid.Columns.GridColumn colAgencia;
        private DevExpress.XtraGrid.Columns.GridColumn colConta;
        private DevExpress.XtraGrid.Columns.GridColumn colBCONome;
        private DevExpress.XtraGrid.Columns.GridColumn colCONCODIGO;
        private DevExpress.XtraGrid.Columns.GridColumn colAPTNUMERO;
        private DevExpress.XtraGrid.Columns.GridColumn colBLOCO;
        private DevExpress.XtraGrid.Columns.GridColumn colUsuarioInternet;
        private DevExpress.XtraGrid.Columns.GridColumn colSenhaInternet;
        /// <summary>
        /// 
        /// </summary>
        public dPessoasGrade dPessoasGrade;
        /// <summary>
        /// 
        /// </summary>
        public Cadastros.Pessoas.dPessoasGradeTableAdapters.PESSOASTableAdapter pESSOASTableAdapter;
        private DevExpress.XtraBars.BarButtonItem barButtonItem1;
        private DevExpress.XtraGrid.Columns.GridColumn colProprietario;
        private DevExpress.XtraEditors.Repository.RepositoryItemImageComboBox ComboBoxPI;
        private Framework.objetosNeon.ImagensPI imagensPI1;
        private DevExpress.XtraGrid.Columns.GridColumn colAPTRegistro;
        private DevExpress.XtraGrid.Columns.GridColumn colAPT;
        private DevExpress.XtraGrid.Columns.GridColumn colPES;
        private DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit repositoryItemLookUpEdit1;
        private System.Windows.Forms.BindingSource bindingSourceCargos;
        private DevExpress.XtraGrid.Columns.GridColumn colcopiaAPT;
        private DevExpress.XtraBars.BarButtonItem cmdNotificacaoDoc;
        private DevExpress.XtraGrid.Columns.GridColumn colPESCpfCnpjViaSite;
        private DevExpress.XtraGrid.Columns.GridColumn colPESCpfErrado;
    }
}
