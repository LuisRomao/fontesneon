﻿namespace Cadastros.Pessoas
{


    public partial class dPessoasCampos
    {
        private static dPessoasCamposTableAdapters.PESSOASTableAdapter pESSOASTableAdapter;
        /// <summary>
        /// TableAdapter padrão para tabela: PESSOAS
        /// </summary>
        public static dPessoasCamposTableAdapters.PESSOASTableAdapter PESSOASTableAdapter
        {
            get
            {
                if (pESSOASTableAdapter == null)
                {
                    pESSOASTableAdapter = new dPessoasCamposTableAdapters.PESSOASTableAdapter();
                    pESSOASTableAdapter.TrocarStringDeConexao();
                };
                return pESSOASTableAdapter;
            }
        }

        private static dPessoasCamposTableAdapters.UnidadesDaPessoaTableAdapter unidadesDaPessoaTableAdapter;
        /// <summary>
        /// TableAdapter padrão para tabela: UnidadesDaPessoa
        /// </summary>
        public static dPessoasCamposTableAdapters.UnidadesDaPessoaTableAdapter UnidadesDaPessoaTableAdapter
        {
            get
            {
                if (unidadesDaPessoaTableAdapter == null)
                {
                    unidadesDaPessoaTableAdapter = new dPessoasCamposTableAdapters.UnidadesDaPessoaTableAdapter();
                    unidadesDaPessoaTableAdapter.TrocarStringDeConexao();
                };
                return unidadesDaPessoaTableAdapter;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        public int nCorreios = 0;

        /// <summary>
        /// 
        /// </summary>
        public int APTChamador = 0;

        /// <summary>
        /// 
        /// </summary>
        /// <param name="PES"></param>
        /// <returns></returns>
        public int Carrega(int PES)
        {
            nCorreios = 0;
            int retorno = UnidadesDaPessoaTableAdapter.Fill(UnidadesDaPessoa, PES);
            foreach (UnidadesDaPessoaRow row in UnidadesDaPessoa)
            {
                if (row.IsAPTProprietario_PESNull())
                    continue;
                if (row.APTProprietario_PES == PES)
                {
                    row.Proprietario = true;
                    if (!row.IsAPTFormaPagtoProprietario_FPGNull())
                        row.APTFormaPagtoX_FPG = row.APTFormaPagtoProprietario_FPG;
                    else
                        row.APTFormaPagtoX_FPG = 4;
                }
                else
                {
                    row.Proprietario = false;
                    if (!row.IsAPTFormaPagtoInquilino_FPGNull())
                        row.APTFormaPagtoX_FPG = row.APTFormaPagtoInquilino_FPG;
                    else
                        row.APTFormaPagtoX_FPG = 4;
                }
                if ((row.APTFormaPagtoX_FPG == 2) && (row.APT != APTChamador))
                    nCorreios++;
            };
            UnidadesDaPessoa.AcceptChanges();
            return retorno;
        }

    }
}
