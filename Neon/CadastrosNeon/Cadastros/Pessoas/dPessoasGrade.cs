﻿namespace Cadastros.Pessoas
{


    public partial class dPessoasGrade
    {
        partial class PESSOASDataTable
        {

        }

        private dPessoasGradeTableAdapters.CargosTableAdapter cargosTableAdapter;
        /// <summary>
        /// TableAdapter padrão para tabela: Cargos
        /// </summary>
        public dPessoasGradeTableAdapters.CargosTableAdapter CargosTableAdapter
        {
            get
            {
                if (cargosTableAdapter == null)
                {
                    cargosTableAdapter = new dPessoasGradeTableAdapters.CargosTableAdapter();
                    cargosTableAdapter.TrocarStringDeConexao();
                };
                return cargosTableAdapter;
            }
        }

        private dPessoasGradeTableAdapters.PESSOASTableAdapter pESSOASTableAdapter;
        /// <summary>
        /// TableAdapter padrão para tabela: PESSOAS
        /// </summary>
        public dPessoasGradeTableAdapters.PESSOASTableAdapter PESSOASTableAdapter
        {
            get
            {
                if (pESSOASTableAdapter == null)
                {
                    pESSOASTableAdapter = new dPessoasGradeTableAdapters.PESSOASTableAdapter();
                    pESSOASTableAdapter.TrocarStringDeConexao();
                };
                return pESSOASTableAdapter;
            }
        }
    }
}
