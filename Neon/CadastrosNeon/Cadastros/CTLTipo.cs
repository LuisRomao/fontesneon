﻿using System;
using System.Drawing;


/// <summary>
/// Enumeração para campo 
/// </summary>
public enum CTLTipo
{
    Caixa = 0,
    Fundo = 1,
    Rateio = 2,
    Terceiro = 3,
    Provisao = 4
}

/// <summary>
/// virEnum para campo 
/// </summary>
public class virEnumCTLTipo : dllVirEnum.VirEnum
{
    /// <summary>
    /// Construtor padrao
    /// </summary>
    /// <param name="Tipo">Tipo</param>
    public virEnumCTLTipo(Type Tipo) :
        base(Tipo)
    {
    }

    private static virEnumCTLTipo _virEnumCTLTipoSt;

    /// <summary>
    /// VirEnum estático para campo CTLTipo
    /// </summary>
    public static virEnumCTLTipo virEnumCTLTipoSt
    {
        get
        {
            if (_virEnumCTLTipoSt == null)
            {
                _virEnumCTLTipoSt = new virEnumCTLTipo(typeof(CTLTipo));                
                _virEnumCTLTipoSt.GravaNomes(CTLTipo.Caixa, "Caixa",Color.White);
                _virEnumCTLTipoSt.GravaNomes(CTLTipo.Fundo, "Fundo de reserva", Color.Yellow);
                _virEnumCTLTipoSt.GravaNomes(CTLTipo.Rateio, "Rateio", Color.Lime);
                _virEnumCTLTipoSt.GravaNomes(CTLTipo.Terceiro, "Terceiros", Color.Pink);
                _virEnumCTLTipoSt.GravaNomes(CTLTipo.Provisao, "Provisão", Color.Aqua);
            }
            return _virEnumCTLTipoSt;
        }
    }
}