using System;
using DevExpress.XtraReports.UI;
using Cadastros.Cetidao;

namespace Cadastros.Cetidao
{ 
    /// <summary>
    /// 
    /// </summary>
public partial class cCertidao : CompontesBasicos.ComponenteBase
    {
        private ImpCetidao Impresso;

        /// <summary>
        /// 
        /// </summary>
        public cCertidao()
        {            
            InitializeComponent();
            Impresso = new ImpCetidao();
            dadosCertidaoDataTableBindingSource.DataSource = Impresso.dCertidao.DadosCertidao;
        }

        private void simpleButton1_Click(object sender, EventArgs e)
        {
            Impresso.CreateDocument();
            Impresso.ShowPreviewDialog();
        }

        private void simpleButton2_Click(object sender, EventArgs e)
        {
            string endereco;
            string Bloco;
            if (lookupBlocosAptos_F1.CON_sel > 0)
            {
                System.Collections.ArrayList Res = VirMSSQL.TableAdapter.STTableAdapter.BuscaSQLLinha("select CONEndereco,CONBairro from condominios where CON = @P1", lookupBlocosAptos_F1.CON_sel);
                if(Res == null)
                    return;
                endereco = Res[0].ToString();
                if (Res[1].ToString() != "")
                    endereco += " - "+Res[1].ToString();
                if (lookupBlocosAptos_F1.APT_Sel > 0)
                {
                    if (lookupBlocosAptos_F1.BLO_Selrow.BLOCodigo != "SB")
                        Bloco = lookupBlocosAptos_F1.BLO_Selrow.BLONome + " - ";
                    else
                        Bloco = "";
                    Impresso.dCertidao.DadosCertidao.AddDadosCertidaoRow(DateTime.Today, lookupBlocosAptos_F1.CON_selrow.CONNome, endereco, Bloco + lookupBlocosAptos_F1.APT_Selrow.APTNumero, 28.43m);
                }
                else
                    Impresso.dCertidao.DadosCertidao.AddDadosCertidaoRow(DateTime.Today, lookupBlocosAptos_F1.CON_selrow.CONNome, endereco, "", 28.43m);
            }
        }
    }
}

