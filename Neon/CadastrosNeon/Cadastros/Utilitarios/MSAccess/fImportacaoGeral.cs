using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using System.Data.SqlClient;
using System.Data.OleDb;

namespace Cadastros.Utilitarios.MSAccess
{
    public partial class fImportacaoGeral : Form
    {


        public fImportacaoGeral()
        {
            InitializeComponent();
        }

        private void btnImportar_Click(object sender, EventArgs e)
        {
            /*
            btnImportar.Enabled = false;
            btnFechar.Enabled = false;

            SqlConnection _connSQL = new SqlConnection(VirDB.Bancovirtual.BancoVirtual.StringConeFinal(VirDB.Bancovirtual.TiposDeBanco.SQL));
            OleDbConnection _connAccess = new OleDbConnection(VirDB.Bancovirtual.BancoVirtual.StringConeFinal(VirDB.Bancovirtual.TiposDeBanco.Access));

            progressBarOperacao.Position = 0;

            progressBarItem.Position = 0;
            
            OleDbCommand _oleCountCommand = new OleDbCommand("Select Count(CODCON) From Condominios", _connAccess);

            _connAccess.Open();
            Object _Count = _oleCountCommand.ExecuteScalar();
            _connAccess.Close();

            if (_Count!=null)
            {
                progressBarOperacao.Properties.Maximum = Convert.ToInt32(_Count);

                OleDbCommand _oleSelectCommand = new OleDbCommand("Select CODCON From Condominios Order by CODCON", _connAccess);

                _connAccess.Open();
                OleDbDataReader _oleDR = _oleSelectCommand.ExecuteReader();

                if (_oleDR.HasRows)
                {
                    while (_oleDR.Read())
                    {
                        chkCondominio.CheckState = CheckState.Unchecked;
                        chkCondominio.Text = "";
                        chkBlocos.CheckState = CheckState.Unchecked;
                        chkBlocos.Text = "";
                        chkApartamentos.CheckState = CheckState.Unchecked;
                        chkApartamentos.Text = "";

                        progressBarItem.Position = 0;

                        Application.DoEvents();

                        String _CODCON = _oleDR["CODCON"].ToString();

                        MSAccess.fImportFromMsAccess _Import = new MSAccess.fImportFromMsAccess();

                        //Importando Condominios
                        chkCondominio.Text = "Importando condomínio " + _CODCON + "...";
                        chkCondominio.Update();
                        _Import.ImportarCondominioDoMsAccess(_CODCON);
                        chkCondominio.Text += " ok!";
                        chkCondominio.CheckState = CheckState.Checked;
                        chkCondominio.Update();
                        progressBarItem.Position = 5;
                        progressBarItem.Update();

                        //Importando Blocos
                        chkBlocos.Text = "Importando blocos...";
                        chkBlocos.Update();
                        _Import.ImportarBlocoDoMsAccess(_CODCON);
                        chkBlocos.Text += " ok!";
                        chkBlocos.CheckState = CheckState.Checked;
                        chkBlocos.Update();                        
                        progressBarItem.Position = 10;
                        progressBarItem.Update();

                        //Importando Apartamentos/Pessoas/Imobiliarias
                        chkApartamentos.Text = "Importando apartamentos + proprietários + inquilinos...";
                        chkApartamentos.Update();
                        fImportFromMsAccess.ImportarApartamentoDoMsAccess(_CODCON);
                        chkApartamentos.Text += " ok!";
                        chkApartamentos.CheckState = CheckState.Checked;
                        chkApartamentos.Update();
                        progressBarItem.Position = 15;
                        progressBarItem.Update();

                        progressBarOperacao.Position++;
                    }

                    _oleDR.Close();
                    _connAccess.Close();

                    _connSQL.Close();

                    MessageBox.Show("Importação dos dados concluída com sucesso.", "Importação", MessageBoxButtons.OK, MessageBoxIcon.Information);

                }
                else
                {
                    _oleDR.Close();
                    _connAccess.Close();
                }
            }

            btnImportar.Enabled = true;
            btnFechar.Enabled = true;
            */
        }

        private void btnFechar_Click(object sender, EventArgs e)
        {
            Close();
        }
    }
}