using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using System.Data.OleDb;
using System.Data.SqlClient;
using VirOleDb;
using Framework;

namespace Cadastros.Utilitarios.MSAccess
{
    public partial class fImportFromMsAccessXXX : Form
    {            
        public fImportFromMsAccessXXX()
        {
            InitializeComponent();
            
        }


        private static bool ImportaBoleto(int NossoNumero, string CC,string CODCON,out int BOL) { 
            
            if(VirOleDb.TableAdapter.STTableAdapter.BuscaEscalar("Select [N�mero Boleto] from boleto where [Nosso N�mero] = @P1 and [codcon]= @P2",out BOL,NossoNumero,CODCON))
                return ImportaBoleto(BOL.ToString());
            else
                return false;
        }

        private static bool ImportaBoleto(String NumeroNeon)
        {
            OleDbConnection _connACCESS = new OleDbConnection(VirDB.Bancovirtual.BancoVirtual.StringConeFinal(VirDB.Bancovirtual.TiposDeBanco.Access));
            SqlConnection _connSQL = new SqlConnection(VirDB.Bancovirtual.BancoVirtual.StringConeFinal(VirDB.Bancovirtual.TiposDeBanco.SQL));

            OleDbCommand _oleSelectCommand = new OleDbCommand("SELECT * " +
                                                              "  FROM boleto " +
                                                              " WHERE [N�mero Boleto] = @NumeroBoleto "
                                                              , _connACCESS);

            _oleSelectCommand.Parameters.AddWithValue("@NumeroBoleto", NumeroNeon);
            _connACCESS.Open();
            OleDbDataReader _oleDR = _oleSelectCommand.ExecuteReader();

            if (_oleDR.Read())
            {
                _connSQL.Open();
                
                SqlCommand ChecaExistencia = new SqlCommand("SELECT BOL FROM Boletos WHERE(BOL = " + _oleDR["N�mero Boleto"].ToString() + ");", _connSQL);
                String _strInsertSQL;
                bool Novo;
                SqlCommand _sqlIncluir = new SqlCommand("", _connSQL);
                SqlCommand _sqlVerificaBOD = new SqlCommand("SELECT BOD FROM BOletoDetalhe WHERE (BOD_BOL = @BOL) AND (BOD_PLA = @PLA) AND (BODValor = @Valor)", _connSQL);
                double BOLNovo = (double)_oleDR["N�mero Boleto"];
                Int32 APT = fImportFromMsAccessXXX.GetSQLApartamentoPK(_oleDR["CODCON"].ToString(), _oleDR["BLOCO"].ToString(), _oleDR["Apartamento"].ToString());
                if (APT == 0)
                    return false;
                SqlCommand PegaDig = new SqlCommand("SELECT CONDigitoAgencia FROM CONDOMINIOS WHERE(CONCodigo = '" + _oleDR["CODCON"].ToString() + "')", _connSQL);
                string digito = PegaDig.ExecuteScalar().ToString();
                DateTime DataVencto = (DateTime)_oleDR["Data Vencimento"];
                string Mes;
                string Ano;
                if (_oleDR["M�s Compet�ncia"].ToString() == "0101")
                {
                    Mes = "01";
                    Ano = "01";
                }
                else
                {
                    Mes = _oleDR["M�s Compet�ncia"].ToString().Substring(0, 2);
                    Ano = _oleDR["M�s Compet�ncia"].ToString().Substring(2, 4);
                }
                object retorno = ChecaExistencia.ExecuteScalar();
                if (retorno == null)
                {
                    _strInsertSQL =
                          "SET IDENTITY_INSERT boletos ON;" +
                          "INSERT INTO BOLetos " +
                          "(BOL,BOLNNAntigo,BOL_APT, BOLProprietario, BOLTipoCRAI, BOLEmissao, BOLVencto, BOLPagamento, BOLMulta, BOLValorPrevisto,  " +
                          "BOLValorPago, BOLCompetenciaAno, BOLCompetenciaMes, BOLEndereco, BOLNome, BOLMensagem, BOLImpressao, BOLTipoMulta,   " +
                          "BOL_FPG, BOLDATAI, BOLDATAA) " +
                          "VALUES " +
                          "(@BOL,@BOLNNAntigo,@BOL_APT,@BOLProprietario,@BOLTipoCRAI,@BOLEmissao,@BOLVencto,@BOLPagamento,@BOLMulta,@BOLValorPrevisto," +
                          "@BOLValorPago,@BOLCompetenciaAno,@BOLCompetenciaMes,@BOLEndereco,@BOLNome,@BOLMensagem,@BOLImpressao," +
                          "@BOLTipoMulta,@BOL_FPG,@BOLDATAI,@BOLDATAA);" +
                          "SET IDENTITY_INSERT boletos OFF;";
                    Novo = true;





                    _sqlIncluir.Parameters.AddWithValue("@BOL", BOLNovo);
                    _sqlIncluir.Parameters.AddWithValue("@BOLNNAntigo", (double)_oleDR["Nosso N�mero"]);

                    _sqlIncluir.Parameters.AddWithValue("@BOL_APT", APT);
                    _sqlIncluir.Parameters.AddWithValue("@BOLProprietario", (bool)(_oleDR["Destinat�rio"].ToString() == "P"));
                    _sqlIncluir.Parameters.AddWithValue("@BOLTipoCRAI", _oleDR["Condominio"]);
                    _sqlIncluir.Parameters.AddWithValue("@BOLEmissao", (DateTime)_oleDR["Data emiss�o"]);
                    _sqlIncluir.Parameters.AddWithValue("@BOLVencto", DataVencto);
                    if ((_oleDR["Data Pagamento"] != null) && (_oleDR["Data Pagamento"] != DBNull.Value))
                        _sqlIncluir.Parameters.AddWithValue("@BOLPagamento", (DateTime)_oleDR["Data Pagamento"]);
                    else
                    {
                        System.Data.SqlClient.SqlParameter Par1 = new SqlParameter("@BOLPagamento", SqlDbType.DateTime);
                        _sqlIncluir.Parameters.Add(Par1);
                        Par1.Value = DBNull.Value;
                    };

                    _sqlIncluir.Parameters.AddWithValue("@BOLMulta", (double)_oleDR["Multa"]);
                    _sqlIncluir.Parameters.AddWithValue("@BOLValorPrevisto", Math.Round((float)_oleDR["Valor Previsto"],2));
                    _sqlIncluir.Parameters.AddWithValue("@BOLValorPago", Math.Round((float)_oleDR["Valor Pago"],2));
                    _sqlIncluir.Parameters.AddWithValue("@BOLCompetenciaAno", Ano);
                    _sqlIncluir.Parameters.AddWithValue("@BOLCompetenciaMes", Mes);
                    //string strend = _oleDR["Endere�o"].ToString();
                    _sqlIncluir.Parameters.AddWithValue("@BOLEndereco", _oleDR["Endere�o"].ToString());//(strend == "") ? "" : strend);
                    _sqlIncluir.Parameters.AddWithValue("@BOLNome", _oleDR["Nome"].ToString());
                    _sqlIncluir.Parameters.AddWithValue("@BOLMensagem", _oleDR["Mensagem"]);
                    _sqlIncluir.Parameters.AddWithValue("@BOLImpressao", (bool)_oleDR["Impresso"]);
                    _sqlIncluir.Parameters.AddWithValue("@BOLTipoMulta", _oleDR["Tipo de Multa"]);                                        
                    _sqlIncluir.Parameters.AddWithValue("@BOL_FPG", ((bool)_oleDR["Correio"]) ? 2 : 4);
                    _sqlIncluir.Parameters.AddWithValue("@BOLDATAI", DateTime.Now);
                    _sqlIncluir.Parameters.AddWithValue("@BOLDATAA", DateTime.Now);


                }
                else
                {
                    _strInsertSQL =
                      "UPDATE BOLetos " +
                      "SET " +
                      "BOLPagamento = @BOLPagamento, " +
                      "BOLValorPago = @BOLValorPago, " +
                      "BOLVencto = @BOLVencto, " +
                      "BOLValorPrevisto = @BOLValorPrevisto "+
                      "WHERE     (BOL = @BOL)";
                    Novo = false;
                    _sqlIncluir.Parameters.AddWithValue("@BOL", _oleDR["N�mero Boleto"].ToString());
                    if ((_oleDR["Data Pagamento"] != null) && (_oleDR["Data Pagamento"] != DBNull.Value))
                        _sqlIncluir.Parameters.AddWithValue("@BOLPagamento", (DateTime)_oleDR["Data Pagamento"]);
                    else
                    {
                        System.Data.SqlClient.SqlParameter Par1 = new SqlParameter("@BOLPagamento", SqlDbType.DateTime);
                        _sqlIncluir.Parameters.Add(Par1);
                        Par1.Value = DBNull.Value;
                    };
                    _sqlIncluir.Parameters.AddWithValue("@BOLValorPago", Math.Round((float)_oleDR["Valor Pago"],2));
                    _sqlIncluir.Parameters.AddWithValue("@BOLVencto", DataVencto);
                    _sqlIncluir.Parameters.AddWithValue("@BOLValorPrevisto",Math.Round((float)_oleDR["Valor Previsto"],2));
                };
                
                
                _sqlIncluir.CommandText = _strInsertSQL;

                try
                {
                    //MessageBox.Show(CODCON + " - " + BLOCO + " - " + APARTAMENTO + " - " + _oleDR["NOME"].ToString());


                    
                    _sqlIncluir.ExecuteNonQuery();

                    _oleDR.Close();
                    ///// ITENS DOS BOLETOS
                    // if (Novo)
                    //{
                        _oleSelectCommand.CommandText = "SELECT [Tipo Pagamento], Valor, Destinat�rio, Mensagem " +
                                                        "FROM [Boleto - Pagamentos] " +
                                                        "WHERE ([N�mero Boleto] = @NumeroBoleto)";

                        _oleSelectCommand.Parameters.AddWithValue("@NumeroBoleto", NumeroNeon);

                        _oleDR = _oleSelectCommand.ExecuteReader();

///////////////////////////////////////
                        
                        _sqlIncluir.CommandText = "INSERT INTO BOletoDetalhe " +
                                                  "(BOD_BOL, BOD_PLA, BODValor, BODMensagem, BOD_APT, BODProprietario,BODFracaoEfetiva,BODData," +
                                                  "BODCompetenciaMes,BODCompetenciaAno,BODComCondominio)" +
                                                  "VALUES " +
                                                  "(@BOD_BOL,@BOD_PLA,@BODValor,@BODMensagem,@BOD_APT,@BODProprietario,1,@BODData," +
                                                  "@BODCompetenciaMes,@BODCompetenciaAno,0);";
                        SqlCommand MenPLA = new SqlCommand("", _connSQL);
                        while (_oleDR.Read())
                        {
                            ///////////////////////

                            _sqlVerificaBOD.Parameters.Clear();
                            _sqlVerificaBOD.Parameters.AddWithValue("@BOL", BOLNovo);
                            _sqlVerificaBOD.Parameters.AddWithValue("@PLA", _oleDR["Tipo Pagamento"]);
                            _sqlVerificaBOD.Parameters.AddWithValue("@Valor", _oleDR["Valor"]);
                            object oBOD = _sqlVerificaBOD.ExecuteScalar();
                            if (oBOD != null)
                                continue;

                            string PLA = _oleDR["Tipo Pagamento"].ToString();
                            DSCentral.PLAnocontasRow rowPLA = DSCentral.DCentral.PLAnocontas.FindByPLA(PLA);
                            if (rowPLA == null)
                                rowPLA = DSCentral.DCentral.ImportaPLA(PLA);

                            string MenBase = "";
                            if(rowPLA != null)
                                MenBase = rowPLA.PLADescricaoRes;                      
                            
                            _sqlIncluir.Parameters.Clear();


                            _sqlIncluir.Parameters.AddWithValue("@BOD_BOL", BOLNovo);
                            _sqlIncluir.Parameters.AddWithValue("@BOD_PLA", _oleDR["Tipo Pagamento"]);
                            _sqlIncluir.Parameters.AddWithValue("@BODValor", Math.Round((float)_oleDR["Valor"],2));
                            if (_oleDR["Mensagem"] != DBNull.Value)
                                MenBase = (string)_oleDR["Mensagem"];

                            _sqlIncluir.Parameters.AddWithValue("@BODMensagem", MenBase);
                            _sqlIncluir.Parameters.AddWithValue("@BOD_APT", APT);
                            _sqlIncluir.Parameters.AddWithValue("@BODProprietario", (bool)(_oleDR["Destinat�rio"].ToString() == "P"));
                            _sqlIncluir.Parameters.AddWithValue("@BODData", DataVencto);
                            _sqlIncluir.Parameters.AddWithValue("@BODCompetenciaAno", Ano);
                            _sqlIncluir.Parameters.AddWithValue("@BODCompetenciaMes", Mes);
                            _sqlIncluir.ExecuteNonQuery();


                        };

                    //};
                    /////
                    _connSQL.Close();

                    
                    _connACCESS.Close();
                    return true;

                }
                catch (Exception Erro)
                {
                    String _Str = "";
                    foreach (SqlParameter _Parametro in _sqlIncluir.Parameters)
                        if(_Parametro.Value == null)
                            _Str += _Parametro.ToString() + " - NULO \r\n\r\n";
                        else

                            _Str += _Parametro.ToString() + " - " + _Parametro.Value.ToString() + " - " + _Parametro.Value.ToString().Length.ToString() + "\r\n\r\n";

                    MessageBox.Show("Ocorreu um erro ao tentar importar o boleto do MS Access.\r\n\r\n Erro: " + Erro.Message + "\r\n\r\n" + _Str,
                                    "Importar Propriet�rio do MS Access", MessageBoxButtons.OK, MessageBoxIcon.Warning);

                    _connSQL.Close();

                    _oleDR.Close();
                    _connACCESS.Close();

                    return false;
                }





            }
            else
            {
                _oleDR.Close();
                _connACCESS.Close();
                return false;
            }
        }

        private static string GetCODCON(int CON)
        {
            SqlConnection _connSQL = new SqlConnection(VirDB.Bancovirtual.BancoVirtual.StringConeFinal(VirDB.Bancovirtual.TiposDeBanco.SQL));

            SqlCommand _sqlSelectCommand = new SqlCommand("Select CONCodigo from Condominios Where CON = @CON", _connSQL);

            _sqlSelectCommand.Parameters.AddWithValue("@CON", CON);

            _connSQL.Open();
            object _CODCON = _sqlSelectCommand.ExecuteScalar();
            _connSQL.Close();

            if (_CODCON != null)
                return _CODCON.ToString();
            else
            {
                return "";
            }
        }

        private bool CondominioExisteNoAcces(String CODCON)
        {
            OleDbConnection _connACCESS = new OleDbConnection(VirDB.Bancovirtual.BancoVirtual.StringConeFinal(VirDB.Bancovirtual.TiposDeBanco.Access));

            OleDbCommand _SelectCommand = new OleDbCommand("Select CODCON from Condominios Where CODCON = @CODCON", _connACCESS);
            _SelectCommand.Parameters.AddWithValue("@CODCON", CODCON);

            _connACCESS.Open();
            object _PK = _SelectCommand.ExecuteScalar();
            _connACCESS.Close();

            if (_PK != null)
                return true;
            else
                return false;
            
        }

        private static Int32 GetSQLCondominioPK(String CODCON)
        {
            SqlConnection _connSQL = new SqlConnection(VirDB.Bancovirtual.BancoVirtual.StringConeFinal(VirDB.Bancovirtual.TiposDeBanco.SQL));
        
            SqlCommand _sqlSelectCommand = new SqlCommand("Select CON from Condominios Where CONCodigo = @CODCON", _connSQL);
            _sqlSelectCommand.Parameters.AddWithValue("@CODCON", CODCON);
            
            _connSQL.Open();
            object _PK = _sqlSelectCommand.ExecuteScalar();
            _connSQL.Close();

            if (_PK != null)
                return Convert.ToInt32(_PK);
            else
            {
                MessageBox.Show("N�o foi poss�vel encontrar o valor da chave no SQL Server para " + CODCON + ".",
                                "Chave Prim�ria", MessageBoxButtons.OK, MessageBoxIcon.Error);
                _connSQL.Close();
                return 0;
            }
        }

        private static Int32 GetSQLBlocoPK(String CODCON, String Bloco)
        {
            SqlConnection _connSQL = new SqlConnection(VirDB.Bancovirtual.BancoVirtual.StringConeFinal(VirDB.Bancovirtual.TiposDeBanco.SQL));

            SqlCommand _sqlSelectCommand = new SqlCommand("Select BLO from Blocos Where BLO_CON = @CON and BLOCodigo = @BLOCodigo", _connSQL);
            _sqlSelectCommand.Parameters.AddWithValue("@CON", GetSQLCondominioPK(CODCON));
            _sqlSelectCommand.Parameters.AddWithValue("@BLOCodigo", Bloco);

            _connSQL.Open();
            object _PK = _sqlSelectCommand.ExecuteScalar();
            _connSQL.Close();

            if (_PK != null)
                return Convert.ToInt32(_PK);
            else
            {
                MessageBox.Show("N�o foi poss�vel encontrar o valor da chave no SQL Server para o " + CODCON + " e o BLOCO " + Bloco + ".",
                                "Chave Prim�ria", MessageBoxButtons.OK, MessageBoxIcon.Error);
                _connSQL.Close();
                return 0;
            }
        }

        private static Int32 GetSQLApartamentoPK(String CODCON, String Bloco, String Apartamento)
        {
            SqlConnection _connSQL = new SqlConnection(VirDB.Bancovirtual.BancoVirtual.StringConeFinal(VirDB.Bancovirtual.TiposDeBanco.SQL));

            SqlCommand _sqlSelectCommand = new SqlCommand("Select APT from Apartamentos Where APT_BLO = @BLO and APTNumero = @APARTAMENTO", _connSQL);
            _sqlSelectCommand.Parameters.AddWithValue("@BLO", GetSQLBlocoPK(CODCON,Bloco));
            _sqlSelectCommand.Parameters.AddWithValue("@Apartamento", Apartamento);

            _connSQL.Open();
            object _PK = _sqlSelectCommand.ExecuteScalar();
            _connSQL.Close();

            if (_PK != null)
                return Convert.ToInt32(_PK);
            else
            {
                MessageBox.Show("N�o foi poss�vel encontrar o valor da chave no SQL Server para o " + CODCON + " e o BLOCO " + Bloco + ".",
                                "Chave Prim�ria", MessageBoxButtons.OK, MessageBoxIcon.Error);
                _connSQL.Close();
                return 0;
            }
        }

        private static String GetSQLNomePessoaPK(Int32 PES)
        {
            SqlConnection _connSQL = new SqlConnection(VirDB.Bancovirtual.BancoVirtual.StringConeFinal(VirDB.Bancovirtual.TiposDeBanco.SQL));

            SqlCommand _sqlSelectCommand = new SqlCommand("Select PESNome from Pessoas Where PES = @PES", _connSQL);

            _sqlSelectCommand.Parameters.AddWithValue("@PES", PES);

            _connSQL.Open();
            object _NOME = _sqlSelectCommand.ExecuteScalar();
            _connSQL.Close();

            if (_NOME != null)
                return _NOME.ToString();
            else
            {
                return "";
            }
        }

        private static Int32 GetSQLProprietarioPK(Int32 ApartamentoPK)
        {
            SqlConnection _connSQL = new SqlConnection(VirDB.Bancovirtual.BancoVirtual.StringConeFinal(VirDB.Bancovirtual.TiposDeBanco.SQL));

            SqlCommand _sqlSelectCommand = new SqlCommand("Select APTProprietario_PES from Apartamentos Where APT = @APT", _connSQL);
            
            _sqlSelectCommand.Parameters.AddWithValue("@APT", ApartamentoPK);

            _connSQL.Open();
            object _PK = _sqlSelectCommand.ExecuteScalar();
            _connSQL.Close();

            if (_PK != null)
                return Convert.ToInt32(_PK);
            else
            {                
                return 0;
            }
        }

        private static Int32 GetSQLInquilinoPK(Int32 ApartamentoPK)
        {
            SqlConnection _connSQL = new SqlConnection(VirDB.Bancovirtual.BancoVirtual.StringConeFinal(VirDB.Bancovirtual.TiposDeBanco.SQL));

            SqlCommand _sqlSelectCommand = new SqlCommand("Select APTInquilino_PES from Apartamentos Where APT = @APT", _connSQL);

            _sqlSelectCommand.Parameters.AddWithValue("@APT", ApartamentoPK);

            _connSQL.Open();
            object _PK = _sqlSelectCommand.ExecuteScalar();
            _connSQL.Close();

            if (_PK != null)
                return Convert.ToInt32(_PK);
            else
            {
                return 0;
            }
        }

        private String GetSQLPlanoContasMen(String TipoPagamento)
        {
            SqlConnection _connSQL = new SqlConnection(VirDB.Bancovirtual.BancoVirtual.StringConeFinal(VirDB.Bancovirtual.TiposDeBanco.SQL));

            SqlCommand _sqlSelectCommand = new SqlCommand("Select PLADescricaoRes from Planocontas Where PLA = @PLA", _connSQL);
            _sqlSelectCommand.Parameters.AddWithValue("@PLA", TipoPagamento);

            _connSQL.Open();
            object Descri = _sqlSelectCommand.ExecuteScalar();
            _connSQL.Close();

            if (Descri != null)
            {
                _connSQL.Close();
                return Descri.ToString();
            }
            else
            {
                //     MessageBox.Show("N�o foi poss�vel encontrar o valor da chave no SQL Server para " + CODCON + ".",
                //                   "Chave Prim�ria", MessageBoxButtons.OK, MessageBoxIcon.Error);
                _connSQL.Close();
                return "";
            }
        }

        private static Object ImportarCidadeDoMsAccess(String Cidade, String UF)
        {
            Cidade = Cidade.ToUpper();
            UF = (UF.Trim() == "" ? "SP" : UF.ToUpper());

            SqlConnection _connSQL = new SqlConnection(VirDB.Bancovirtual.BancoVirtual.StringConeFinal(VirDB.Bancovirtual.TiposDeBanco.SQL));

            SqlCommand _sqlVerificaCidade = new SqlCommand("Select CID, CIDUF from Cidades Where CIDNome = @Cidade", _connSQL);
            _sqlVerificaCidade.Parameters.AddWithValue("@Cidade", Cidade);            
            _connSQL.Open();
            //Object _PK = _sqlVerificaCidade.ExecuteScalar();
            SqlDataReader _cidades = _sqlVerificaCidade.ExecuteReader();

            Object _PK = null;
            Object _UF = null;

            if (_cidades.Read()){
                _PK = _cidades["CID"];
                _UF = _cidades["CIDUF"];
            };


            _connSQL.Close();

            if (_PK != null)
            {
                //if (UF.ToString() != _UF.ToString())
                  //  MessageBox.Show("ATEN��O:\r\nConflito de cadastros de UF para a cidade "+Cidade+"\r\n1) "+_UF+"\r\n2) "+UF);
                return _PK;
            }
            else
            {
                SqlCommand _sqlDevolvePK = new SqlCommand("Select @@Identity from Cidades", _connSQL);

                SqlCommand _sqlInsertCidade = new SqlCommand("Insert Into Cidades (CIDNOME, CIDUF) Values (@Cidade, @UF)", _connSQL);
                _sqlInsertCidade.Parameters.AddWithValue("@Cidade", Cidade);
                _sqlInsertCidade.Parameters.AddWithValue("@UF", UF);

                try
                {
                    _connSQL.Open();
                    _sqlInsertCidade.ExecuteNonQuery();
                    _PK = _sqlDevolvePK.ExecuteScalar();
                    _connSQL.Close();

                    return _PK;
                }
                catch (Exception Erro)
                {
                    MessageBox.Show("Nao foi possivel incluir a cidade " + Cidade + ". Verifique os dados do cadastro. \r\n\r\n" +
                                    "Erro: " + Erro.Message, "CIDADE", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    _connSQL.Close();
                    return DBNull.Value;
                }
            }
        }

        private static Object ImportarBancoDoMsAccess(Int32 CodBanco)
        {
            SqlConnection _connSQL = new SqlConnection(VirDB.Bancovirtual.BancoVirtual.StringConeFinal(VirDB.Bancovirtual.TiposDeBanco.SQL));

            SqlCommand _sqlVerificaBanco = new SqlCommand("Select BCO from Bancos Where BCO = @BCO", _connSQL);
            _sqlVerificaBanco.Parameters.AddWithValue("@BCO", CodBanco);

            _connSQL.Open();
            Object _PK = _sqlVerificaBanco.ExecuteScalar();
            _connSQL.Close();

            if (_PK != null)
                return CodBanco;
            else
            {
                SqlCommand _sqlInsertBanco = new SqlCommand("Insert Into Bancos (BCO, BCONOME) Values (@BCO, @BCONOME)", _connSQL);
                _sqlInsertBanco.Parameters.AddWithValue("@BCO", CodBanco);
                _sqlInsertBanco.Parameters.AddWithValue("@BCONOME", CodBanco.ToString() + " - NOME");

                try
                {
                    _connSQL.Open();
                    _sqlInsertBanco.ExecuteNonQuery();
                    _connSQL.Close();

                    return CodBanco;
                }
                catch (Exception Erro)
                {
                    MessageBox.Show("N�o foi poss�vel incluir o banco " + CodBanco + ". Verifique os dados do cadastro. \r\n\r\n" +
                                    "Erro: " + Erro.Message, "BANCO", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    _connSQL.Close();
                    return DBNull.Value;
                }
            }
        }

        private static Object ImportarUsuarioDoMsAccess(String Nome)
        {
            Nome = Nome.ToUpper();

            SqlConnection _connSQL = new SqlConnection(VirDB.Bancovirtual.BancoVirtual.StringConeFinal(VirDB.Bancovirtual.TiposDeBanco.SQL));
         
            SqlCommand _sqlVerificaUsuario = new SqlCommand("Select USU from Usuarios Where USUNome = @USUNome", _connSQL);
            _sqlVerificaUsuario.Parameters.AddWithValue("@USUNome", Nome);

            _connSQL.Open();
            Object _PK = _sqlVerificaUsuario.ExecuteScalar();
            _connSQL.Close();

            if (_PK != null)
                return _PK;
            else
            {
                SqlCommand _sqlDevolvePK = new SqlCommand("Select @@Identity from Cidades", _connSQL); 
               
                SqlCommand _sqlInsertUsuario = new SqlCommand("Insert Into Usuarios (USUNOME) Values (@USUNOME)", _connSQL);
                _sqlInsertUsuario.Parameters.AddWithValue("@USUNOME", Nome);

                try
                {
                    _connSQL.Open();
                    _sqlInsertUsuario.ExecuteNonQuery();
                    _PK = _sqlDevolvePK.ExecuteScalar();
                    _connSQL.Close();

                    return _PK;
                }
                catch (Exception Erro)
                {
                    MessageBox.Show("N�o foi poss�vel incluir o usu�rio " + Nome + ". Verifique os dados do cadastro.\r\n\r\n" +
                                    "Erro: " + Erro.Message, "USU�RIO", MessageBoxButtons.OK, MessageBoxIcon.Error);

                    _connSQL.Close();
                    return DBNull.Value;
                }
            }
        }

        private void ImportarEspecificos()
        {
            Cadastros.Utilitarios.MSAccess.dAccessTableAdapters.PaGamentoEspecificoTableAdapter PaGamentoEspecificoTableAdapter = new Cadastros.Utilitarios.MSAccess.dAccessTableAdapters.PaGamentoEspecificoTableAdapter();
            PaGamentoEspecificoTableAdapter.TrocarStringDeConexao(VirDB.Bancovirtual.TiposDeBanco.SQL);
            PaGamentoEspecificoTableAdapter.Fill(dAccess.PaGamentoEspecifico);
            Cadastros.Utilitarios.MSAccess.dAccessTableAdapters.APTxPGETableAdapter APTxPGETableAdapter = new Cadastros.Utilitarios.MSAccess.dAccessTableAdapters.APTxPGETableAdapter();
            APTxPGETableAdapter.TrocarStringDeConexao(VirDB.Bancovirtual.TiposDeBanco.SQL);
            APTxPGETableAdapter.Fill(dAccess.APTxPGE);
            OleDbConnection _connACCESS = new OleDbConnection(VirDB.Bancovirtual.BancoVirtual.StringConeFinal(VirDB.Bancovirtual.TiposDeBanco.Access));
            

            string Codcon = "-";
            string TipoPagamento = "-";
            decimal Valor = 0;
            string NCodcon;
            string NTipoPagamento;
            decimal NValor;
            String BLOCO;
            String APARTAMENTO;
            OleDbCommand _oleSelectCommand = new OleDbCommand("SELECT "+
                                                              "Codcon, TipoPagamento, Valor, Bloco, Apartamento, Destinat�rio "+
                                                              "FROM [Apartamento pagamentos espec�ficos] "+
                                                              "ORDER BY Codcon, TipoPagamento, Valor", _connACCESS);            
            _connACCESS.Open();
            OleDbDataReader _oleDR = _oleSelectCommand.ExecuteReader();
            Int32 CON=0;
            Int32 APT=0;
            Int32 PGE=0;
            bool Prop;
            while (_oleDR.Read())
            {
                NCodcon=_oleDR["Codcon"].ToString();
                NTipoPagamento=_oleDR["TipoPagamento"].ToString();
                NValor=(decimal)_oleDR["Valor"];
                BLOCO=_oleDR["BLOCO"].ToString();
                APARTAMENTO = _oleDR["Apartamento"].ToString();
                Prop = (_oleDR["Destinat�rio"].ToString().ToUpper() == "P");
                if((NCodcon!=Codcon)||(NTipoPagamento!=TipoPagamento)||(NValor!=Valor)){
                    Codcon=NCodcon;
                    TipoPagamento=NTipoPagamento;
                    Valor = NValor;
                    CON = GetSQLCondominioPK(Codcon);
                    if (CON != 0)
                    {

                        object obPGE = PaGamentoEspecificoTableAdapter.GetPGE(CON, TipoPagamento, Valor);
                        
                        if (obPGE == null)
                        {
                            memoEdit1.Text += NCodcon + " - " + NTipoPagamento + " - " + NValor.ToString() + "\r\n";
                            
                            String Mensagem = GetSQLPlanoContasMen(TipoPagamento);
                            dAccess.PaGamentoEspecificoRow rowPGE = dAccess.PaGamentoEspecifico.AddPaGamentoEspecificoRow(CON, TipoPagamento, Valor, Mensagem);
                            PaGamentoEspecificoTableAdapter.Update(dAccess.PaGamentoEspecifico);
                            dAccess.AcceptChanges();
                            PGE = rowPGE.PGE;
                            
                        }
                        else
                        {
                            PGE = (Int32)obPGE;                            
                        }
                    };
                };
                
                if (CON != 0) {
                    APT = GetSQLApartamentoPK(Codcon, BLOCO, APARTAMENTO);
                    if (APT != 0) {
                        object obPaga = APTxPGETableAdapter.VerificaPaga(APT, PGE, Prop);

                        if (obPaga == null)
                        {
                            try
                            {
                                dAccess.APTxPGE.AddAPTxPGERow(APT, PGE, true, Prop);
                                memoEdit1.Text += "        " + NCodcon + " - " + NTipoPagamento + " - " + NValor.ToString() + " - " + APARTAMENTO + "\r\n";
                            }
                            catch {
                                memoEdit1.Text += "D        " + NCodcon + " - " + NTipoPagamento + " - " + NValor.ToString() + " - " + APARTAMENTO + "\r\n";
                            }
                        }
                        else
                            if ((bool)obPaga != true)
                            {
                                APTxPGETableAdapter.SetaPaga(APT, PGE, Prop);
                                memoEdit1.Text += "        *  " + NCodcon + " - " + NTipoPagamento + " - " + NValor.ToString() + " - " + APARTAMENTO + "\r\n";
                            }
                    };
                    APTxPGETableAdapter.Update(dAccess.APTxPGE);
                    dAccess.APTxPGE.AcceptChanges();
                }
            }
            _oleDR.Close();
            _connACCESS.Close();
            
        }

        private static Object ImportarPessoaDoMsAccess(Boolean Proprietario, String CODCON, String BLOCO, String APARTAMENTO)
        {
            CODCON = CODCON.ToUpper();
            BLOCO = BLOCO.ToUpper();
            APARTAMENTO = APARTAMENTO.ToUpper();

            OleDbConnection _connACCESS = new OleDbConnection(VirDB.Bancovirtual.BancoVirtual.StringConeFinal(VirDB.Bancovirtual.TiposDeBanco.Access));
            SqlConnection _connSQL = new SqlConnection(VirDB.Bancovirtual.BancoVirtual.StringConeFinal(VirDB.Bancovirtual.TiposDeBanco.SQL));

            String _strTabela = "";

            if (Proprietario)
                _strTabela = "Propriet�rio";
            else
                _strTabela = "Inquilino";

            OleDbCommand _oleSelectCommand = new OleDbCommand("SELECT * " +
                                                              "  FROM  " + _strTabela +
                                                              " WHERE Codcon = @CODCON " +
                                                              "   AND Bloco = @Bloco " +
                                                              "   AND Apartamento = @Apartamento" +
                                                              " ORDER BY CODCON, BLOCO, APARTAMENTO", _connACCESS);
            
            _oleSelectCommand.Parameters.AddWithValue("@CODCON", CODCON);
            _oleSelectCommand.Parameters.AddWithValue("@BLOCO", BLOCO);
            _oleSelectCommand.Parameters.AddWithValue("@APARTAMENTO", APARTAMENTO);

            _connACCESS.Open();
            OleDbDataReader _oleDR = _oleSelectCommand.ExecuteReader();
            //_connACCESS.Close();

            if (_oleDR.Read())
            {
                String _strCondicaoSQL = "";
                SqlCommand _sqlVerificaPessoa = new SqlCommand("", _connSQL);

                String _strInsertSQL = "";
                String _strValuesSQL = "";

                String _strUpdateSQL = "";

                SqlCommand _sqlIncluirPessoa = new SqlCommand("", _connSQL);

                //if (_oleDR["NOME"] != DBNull.Value) 
                //{
                String _Nome = (_oleDR["NOME"] == DBNull.Value ? "SEM NOME" : _oleDR["NOME"].ToString().ToUpper());

                _strCondicaoSQL += " AND PESNOME = @NOME ";
                _sqlVerificaPessoa.Parameters.AddWithValue("@NOME", _Nome);

                _strInsertSQL += " PESNOME, ";
                _strValuesSQL += " @NOME, ";
                _strUpdateSQL += " PESNome = @Nome, ";
                _sqlIncluirPessoa.Parameters.AddWithValue("@NOME", _Nome);
                //}

                
                if (_oleDR["CPF"] != DBNull.Value)
                {
                    _strCondicaoSQL += " AND PESCPFCNPJ = @CPFCNPJ ";
                    _sqlVerificaPessoa.Parameters.AddWithValue("@CPFCNPJ", _oleDR["CPF"].ToString());

                    _strInsertSQL += " PESCPFCNPJ, ";
                    _strValuesSQL += " @CPFCNPJ, ";
                    _strUpdateSQL += " PESCPFCNPJ = @CPFCNPJ, ";
                    DocBacarios.CPFCNPJ cpf = new DocBacarios.CPFCNPJ(_oleDR["CPF"].ToString());

                    _sqlIncluirPessoa.Parameters.AddWithValue("@CPFCNPJ", cpf.ToString());

                    // Tipo de pessoa (f�sica ou jur�dica)
                    _strInsertSQL += " PESTipo, ";
                    _strValuesSQL += " @PESTipo, ";
                    _strUpdateSQL += " PESTipo = @PESTipo, ";

                    if (cpf.Tipo == DocBacarios.TipoCpfCnpj.CNPJ) 
                        _sqlIncluirPessoa.Parameters.AddWithValue("@PESTipo", "J");
                    else
                        _sqlIncluirPessoa.Parameters.AddWithValue("@PESTipo", "F");
                    
                }

                if (_oleDR["RG"] != DBNull.Value)
                {
                    _strCondicaoSQL += " AND PESRGIE = @RGIE ";
                    _sqlVerificaPessoa.Parameters.AddWithValue("@RGIE", _oleDR["RG"].ToString());

                    _strInsertSQL += " PESRGIE, ";
                    _strValuesSQL += " @RGIE, ";
                    _strUpdateSQL += " PESRGIE = @RGIE, ";
                    _sqlIncluirPessoa.Parameters.AddWithValue("@RGIE", _oleDR["RG"].ToString());
                }

                if (_oleDR["PIS"] != DBNull.Value)
                {
                    _strCondicaoSQL += " AND PESPIS = @PIS ";
                    _sqlVerificaPessoa.Parameters.AddWithValue("@PIS", _oleDR["PIS"].ToString());

                    _strInsertSQL += " PESPIS, ";
                    _strValuesSQL += " @PIS, ";
                    _strUpdateSQL += " PESPIS = @PIS, ";
                    _sqlIncluirPessoa.Parameters.AddWithValue("@PIS", _oleDR["PIS"].ToString());
                }

                if (_oleDR["END"] != DBNull.Value)
                {
                    _strCondicaoSQL += " AND PESENDERECO = @ENDERECO ";
                    _sqlVerificaPessoa.Parameters.AddWithValue("@ENDERECO", _oleDR["END"].ToString().ToUpper());

                    _strInsertSQL += " PESENDERECO, ";
                    _strValuesSQL += " @ENDERECO, ";
                    _strUpdateSQL += " PESENDERECO = @ENDERECO, ";
                    _sqlIncluirPessoa.Parameters.AddWithValue("@ENDERECO", _oleDR["END"].ToString().ToUpper());
                }

                if (_oleDR["BAIRRO"] != DBNull.Value)
                {
                    _strCondicaoSQL += " AND PESBAIRRO = @BAIRRO ";
                    _sqlVerificaPessoa.Parameters.AddWithValue("@BAIRRO", _oleDR["BAIRRO"].ToString().ToUpper());

                    _strInsertSQL += " PESBAIRRO, ";
                    _strValuesSQL += " @BAIRRO, ";
                    _strUpdateSQL += " PESBAIRRO = @BAIRRO, ";
                    _sqlIncluirPessoa.Parameters.AddWithValue("@BAIRRO", _oleDR["BAIRRO"].ToString().ToUpper());
                }

                if (_oleDR["CIDADE"] != DBNull.Value)
                {
                    _strCondicaoSQL += " AND PES_CID = @CIDADE ";
                    _sqlVerificaPessoa.Parameters.AddWithValue("@CIDADE", ImportarCidadeDoMsAccess(_oleDR["CIDADE"].ToString(), _oleDR["ESTADO"].ToString().ToUpper()));

                    _strInsertSQL += " PES_CID, ";
                    _strValuesSQL += " @CIDADE, ";
                    _strUpdateSQL += " PES_CID = @CIDADE, ";
                    _sqlIncluirPessoa.Parameters.AddWithValue("@CIDADE", ImportarCidadeDoMsAccess(_oleDR["CIDADE"].ToString(), _oleDR["ESTADO"].ToString().ToUpper()));
                }
                
                
                if (_oleDR["CEP"] != DBNull.Value)
                {
                    _strCondicaoSQL += " AND PESCEP = @CEP ";
                    _sqlVerificaPessoa.Parameters.AddWithValue("@CEP", _oleDR["CEP"].ToString());

                    _strInsertSQL += " PESCEP, ";
                    _strValuesSQL += " @CEP, ";
                    _strUpdateSQL += " PESCEP = @CEP, ";
                    _sqlIncluirPessoa.Parameters.AddWithValue("@CEP", _oleDR["CEP"].ToString());
                }

                if (_oleDR["TELEFONE1"] != DBNull.Value)
                {
                    _strCondicaoSQL += " AND PESFONE1 = @TELEFONE1 ";
                    _sqlVerificaPessoa.Parameters.AddWithValue("@TELEFONE1", _oleDR["TELEFONE1"].ToString());

                    _strInsertSQL += " PESFONE1, ";
                    _strValuesSQL += " @TELEFONE1, ";
                    _strUpdateSQL += " PESFone1 = @TELEFONE1, ";
                    _sqlIncluirPessoa.Parameters.AddWithValue("@TELEFONE1", _oleDR["TELEFONE1"].ToString());
                }

                if (_oleDR["TELEFONE2"] != DBNull.Value)
                {
                    _strCondicaoSQL += " AND PESFONE2 = @TELEFONE2 ";
                    _sqlVerificaPessoa.Parameters.AddWithValue("@TELEFONE2", _oleDR["TELEFONE2"].ToString());

                    _strInsertSQL += " PESFONE2, ";
                    _strValuesSQL += " @TELEFONE2, ";
                    _strUpdateSQL += " PESFone2 = @TELEFONE2, ";
                    _sqlIncluirPessoa.Parameters.AddWithValue("@TELEFONE2", _oleDR["TELEFONE2"].ToString());
                }

                if (_oleDR["TELEFONE3"] != DBNull.Value)
                {
                    _strCondicaoSQL += " AND PESFONE3 = @TELEFONE3 ";
                    _sqlVerificaPessoa.Parameters.AddWithValue("@TELEFONE3", _oleDR["TELEFONE3"].ToString());

                    _strInsertSQL += " PESFONE3, ";
                    _strValuesSQL += " @TELEFONE3, ";
                    _strUpdateSQL += " PESFone3 = @TELEFONE3, ";
                    _sqlIncluirPessoa.Parameters.AddWithValue("@TELEFONE3", _oleDR["TELEFONE3"].ToString());
                }

                if (_oleDR["EMAIL"] != DBNull.Value)
                {
                    _strCondicaoSQL += " AND PESEMAIL = @EMAIL";
                    _sqlVerificaPessoa.Parameters.AddWithValue("@EMAIL", _oleDR["EMAIL"].ToString().ToUpper());

                    _strInsertSQL += " PESEMAIL, ";
                    _strValuesSQL += " @EMAIL, ";
                    _strUpdateSQL += " PESEmail = @EMAIL, ";
                    _sqlIncluirPessoa.Parameters.AddWithValue("@EMAIL", _oleDR["EMAIL"].ToString().ToUpper());
                }

                

                if (_oleDR["NASC"] != DBNull.Value)
                {
                    _strCondicaoSQL += " AND PESDATANASCIMENTO = @NASC";
                    _sqlVerificaPessoa.Parameters.AddWithValue("@NASC", Convert.ToDateTime(_oleDR["NASC"]));

                    _strInsertSQL += " PESDATANASCIMENTO, ";
                    _strValuesSQL += " @NASC, ";
                    _strUpdateSQL += " PESDATANASCIMENTO = @NASC, ";
                    _sqlIncluirPessoa.Parameters.AddWithValue("@NASC", Convert.ToDateTime(_oleDR["NASC"]));
                }

                if (_oleDR["USUARIO INTERNET"] != DBNull.Value)
                {
                    _strCondicaoSQL += " AND PESUSUARIOINTERNET = @USUINT";
                    _sqlVerificaPessoa.Parameters.AddWithValue("@USUINT", _oleDR["USUARIO INTERNET"].ToString().ToUpper());

                    _strInsertSQL += " PESUSUARIOINTERNET, ";
                    _strValuesSQL += " @USUINT, ";
                    _strUpdateSQL += " PESUsuarioInternet = @USUINT, ";
                    _sqlIncluirPessoa.Parameters.AddWithValue("@USUINT", _oleDR["USUARIO INTERNET"].ToString().ToUpper());
                }

                if (_oleDR["SENHA INTERNET"] != DBNull.Value)
                {
                    _strCondicaoSQL += " AND PESSENHAINTERNET = @SENINT";
                    _sqlVerificaPessoa.Parameters.AddWithValue("@SENINT", _oleDR["SENHA INTERNET"].ToString());

                    _strInsertSQL += " PESSENHAINTERNET, ";
                    _strValuesSQL += " @SENINT, ";
                    _strUpdateSQL += " PESSenhaInternet = @SENINT, ";
                    _sqlIncluirPessoa.Parameters.AddWithValue("@SENINT", _oleDR["SENHA INTERNET"].ToString());
                }

                if (_strCondicaoSQL.Trim() != "")
                {
                    _strCondicaoSQL = _strCondicaoSQL.Substring(4, (_strCondicaoSQL.Length - 4));
                    _sqlVerificaPessoa.CommandText = "Select PES From Pessoas Where " + _strCondicaoSQL;

                    _connSQL.Open();
                    object _PES = _sqlVerificaPessoa.ExecuteScalar();
                    _connSQL.Close();

                    
                    if (_PES == null) //Pessoa nao existe...incluir
                    {
                        SqlCommand _sqlDevolvePK = new SqlCommand("Select @@Identity From Pessoas", _connSQL);

                        _strInsertSQL = "Insert Into Pessoas ( " + _strInsertSQL.Substring(0, (_strInsertSQL.Length - 2)) + " ) ";
                        _strValuesSQL = "             Values ( " + _strValuesSQL.Substring(0, (_strValuesSQL.Length - 2)) + " ) ";

                        _sqlIncluirPessoa.CommandText = _strInsertSQL + _strValuesSQL;

                        try
                        {
                            //MessageBox.Show(CODCON + " - " + BLOCO + " - " + APARTAMENTO + " - " + _oleDR["NOME"].ToString());


                            _connSQL.Open();
                            _sqlIncluirPessoa.ExecuteNonQuery();
                            Object _PK = _sqlDevolvePK.ExecuteScalar();
                            _connSQL.Close();

                            _oleDR.Close();
                            _connACCESS.Close();

                            return _PK;
                        }
                        catch (Exception Erro)
                        {
                            String _Str = "";
                            foreach (SqlParameter _Parametro in _sqlIncluirPessoa.Parameters)
                                _Str += _Parametro.ToString() + " - " + _Parametro.Value.ToString() + " - " + _Parametro.Value.ToString().Length.ToString() + "\r\n\r\n";

                            MessageBox.Show("Ocorreu um erro ao tentar importar o propriet�rio do condom�nio " + CODCON + ", bloco " + BLOCO + " e apartamento " + APARTAMENTO + " do MS Access.\r\n\r\n Erro: " + Erro.Message + "\r\n\r\n" + _Str,
                                            "Importar Propriet�rio do MS Access", MessageBoxButtons.OK, MessageBoxIcon.Warning);

                            _connSQL.Close();

                            _oleDR.Close();
                            _connACCESS.Close();

                            return DBNull.Value;
                        }
                    }
                    else
                    {

                        _strUpdateSQL = "update Pessoas set " + _strUpdateSQL.Substring(0, (_strUpdateSQL.Length - 2)) + " where PES = @PES";

                        _sqlIncluirPessoa.Parameters.AddWithValue("@PES", _PES);

                        _sqlIncluirPessoa.CommandText = _strUpdateSQL;

                        try
                        {
                            _connSQL.Open();
                            _sqlIncluirPessoa.ExecuteNonQuery();
                            
                            

                            _oleDR.Close();
                            _connACCESS.Close();

                            return _PES;
                        }
                        catch (Exception Erro)
                        {
                            String _Str = "";
                            foreach (SqlParameter _Parametro in _sqlIncluirPessoa.Parameters)
                                _Str += _Parametro.ToString() + " - " + _Parametro.Value.ToString() + " - " + _Parametro.Value.ToString().Length.ToString() + "\r\n\r\n";

                            MessageBox.Show("Ocorreu um erro ao tentar importar (atualizar) o propriet�rio do condom�nio " + CODCON + ", bloco " + BLOCO + " e apartamento " + APARTAMENTO + " do MS Access.\r\n\r\n Erro: " + Erro.Message + "\r\n\r\n" + _Str,
                                            "Importar Propriet�rio do MS Access", MessageBoxButtons.OK, MessageBoxIcon.Warning);

                            _connSQL.Close();

                            _oleDR.Close();
                            _connACCESS.Close();

                            return DBNull.Value;
                        }
                    }
                }
                else
                {
                    _oleDR.Close();
                    _connACCESS.Close();
                    return DBNull.Value;
                }
            }
            else
            {
                _oleDR.Close();
                _connACCESS.Close();
                return DBNull.Value;
            }
        }

        private static Object ImportarImobiliariaDoMsAccess(String CODCON, String BLOCO, String APARTAMENTO)
        {
            CODCON = CODCON.ToUpper();
            BLOCO = BLOCO.ToUpper();
            APARTAMENTO = APARTAMENTO.ToUpper();

            OleDbConnection _connACCESS = new OleDbConnection(VirDB.Bancovirtual.BancoVirtual.StringConeFinal(VirDB.Bancovirtual.TiposDeBanco.Access));
            SqlConnection _connSQL = new SqlConnection(VirDB.Bancovirtual.BancoVirtual.StringConeFinal(VirDB.Bancovirtual.TiposDeBanco.SQL));

            OleDbCommand _oleGetCODIMO = new OleDbCommand("Select CODIMO from Inquilino Where CODCON = @CODCON AND BLOCO = @BLOCO AND APARTAMENTO = @APARTAMENTO", _connACCESS);
            _oleGetCODIMO.Parameters.AddWithValue("@CODCON", CODCON);
            _oleGetCODIMO.Parameters.AddWithValue("@BLOCO", BLOCO);
            _oleGetCODIMO.Parameters.AddWithValue("@APARTAMENTO", APARTAMENTO);

            _connACCESS.Open();
            Object _CODIMO = _oleGetCODIMO.ExecuteScalar();
            _connACCESS.Close();

            if ((_CODIMO != null) && (_CODIMO != DBNull.Value))
            {
                SqlCommand _sqlVerificaImobiliara = new SqlCommand("Select FRN from Fornecedores Where FRNFantasia = @Fantasia", _connSQL);
                _sqlVerificaImobiliara.Parameters.AddWithValue("@Fantasia", _CODIMO.ToString());

                _connSQL.Open();
                Object _IMO = _sqlVerificaImobiliara.ExecuteScalar();
                _connSQL.Close();

                OleDbCommand _oleSelectCommand = new OleDbCommand("Select * from [Imobili�ria] Where CODIMO = @CODIMO", _connACCESS);
                _oleSelectCommand.Parameters.AddWithValue("@CODIMO", _CODIMO.ToString());

                OleDbDataReader _oleDR = null;

                _connACCESS.Open();
                _oleDR = _oleSelectCommand.ExecuteReader();

                if (_oleDR.Read())
                {
                    String _strInsertSQL = "";
                    String _strValuesSQL = "";

                    String _strUpdateSQL = "";

                    SqlCommand _sqlCommand = new SqlCommand("", _connSQL);

                    if (_oleDR["CODIMO"] != DBNull.Value)
                    {
                        _strInsertSQL += " FRNFANTASIA, ";
                        _strValuesSQL += " @FANTASIA, ";

                        _strUpdateSQL += " FRNFANTASIA = @FANTASIA, ";

                        _sqlCommand.Parameters.AddWithValue("@FANTASIA", _oleDR["CODIMO"].ToString().ToUpper());
                    }

                    if (_oleDR["NOME"] != DBNull.Value)
                    {
                        _strInsertSQL += " FRNNOME, ";
                        _strValuesSQL += " @NOME, ";

                        _strUpdateSQL += " FRNNOME = @NOME, ";

                        _sqlCommand.Parameters.AddWithValue("@NOME", _oleDR["NOME"].ToString().ToUpper());
                    }

                    if (_oleDR["END"] != DBNull.Value)
                    {
                        _strInsertSQL += " FRNENDERECO, ";
                        _strValuesSQL += " @ENDERECO, ";

                        _strUpdateSQL += " FRNENDERECO = @ENDERECO, ";

                        _sqlCommand.Parameters.AddWithValue("@ENDERECO", _oleDR["END"].ToString().ToUpper());
                    }

                    if (_oleDR["BAIRRO"] != DBNull.Value)
                    {
                        _strInsertSQL += " FRNBAIRRO, ";
                        _strValuesSQL += " @BAIRRO, ";

                        _strUpdateSQL += " FRNBAIRRO = @BAIRRO, ";

                        _sqlCommand.Parameters.AddWithValue("@BAIRRO", _oleDR["BAIRRO"].ToString().ToUpper());
                    }

                    if (_oleDR["CEP"] != DBNull.Value)
                    {
                        _strInsertSQL += " FRNCEP, ";
                        _strValuesSQL += " @CEP, ";

                        _strUpdateSQL += " FRNCEP = @CEP, ";

                        _sqlCommand.Parameters.AddWithValue("@CEP", _oleDR["CEP"].ToString().ToUpper());
                    }

                    if (_oleDR["CIDADE"] != DBNull.Value)
                    {
                        _strInsertSQL += " FRN_CID, ";
                        _strValuesSQL += " @CIDADE, ";

                        _strUpdateSQL += " FRN_CID = @CIDADE, ";

                        _sqlCommand.Parameters.AddWithValue("@CIDADE", ImportarCidadeDoMsAccess(_oleDR["CIDADE"].ToString(), _oleDR["ESTADO"].ToString().ToUpper()));
                    }

                    /*
                    if (_oleDR["ESTADO"] != DBNull.Value)
                    {
                        _strInsertSQL += " FRNUF, ";
                        _strValuesSQL += " @ESTADO, ";

                        _strUpdateSQL += " FRNUF = @ESTADO, ";

                        _sqlCommand.Parameters.AddWithValue("@ESTADO", _oleDR["ESTADO"].ToString().ToUpper());
                    }
                    */

                    if (_oleDR["TELEFONE1"] != DBNull.Value)
                    {
                        _strInsertSQL += " FRNFONE1, ";
                        _strValuesSQL += " @FONE1, ";

                        _strUpdateSQL += " FRNFONE1 = @FONE1, ";

                        _sqlCommand.Parameters.AddWithValue("@FONE1", _oleDR["TELEFONE1"].ToString());
                    }

                    if (_oleDR["TELEFONE2"] != DBNull.Value)
                    {
                        _strInsertSQL += " FRNFONE2, ";
                        _strValuesSQL += " @FONE2, ";

                        _strUpdateSQL += " FRNFONE2 = @FONE2, ";

                        _sqlCommand.Parameters.AddWithValue("@FONE2", _oleDR["TELEFONE2"].ToString());
                    }

                    if (_oleDR["TELEFONE3"] != DBNull.Value)
                    {
                        _strInsertSQL += " FRNFONE3, ";
                        _strValuesSQL += " @FONE3, ";

                        _strUpdateSQL += " FRNFONE3 = @FONE3, ";

                        _sqlCommand.Parameters.AddWithValue("@FONE3", _oleDR["TELEFONE3"].ToString());
                    }

                    if (_IMO == null)
                    {
                        _strInsertSQL = "Insert into Fornecedores ( " + _strInsertSQL + " FRNTIPO )";
                        _strValuesSQL = "                  Values ( " + _strValuesSQL + " @TIPO )";
                        _sqlCommand.Parameters.AddWithValue("@TIPO", "IMO");

                        SqlCommand _sqlGetPKValue = new SqlCommand("Select @@Identity From Fornecedores ", _connSQL);

                        _sqlCommand.CommandText = _strInsertSQL + _strValuesSQL;

                        _connSQL.Open();
                        _sqlCommand.ExecuteNonQuery();
                        Object _PK = _sqlGetPKValue.ExecuteScalar();
                        _connSQL.Close();

                        _oleDR.Close();
                        _connACCESS.Close();

                        return Convert.ToInt32(_PK);
                    }
                    else
                    {
                        _sqlCommand.CommandText = "Update Fornecedores Set " + _strUpdateSQL.Substring(0, (_strUpdateSQL.Length - 2)) + " Where FRN = @FRN";
                        _sqlCommand.Parameters.AddWithValue("@FRN", Convert.ToInt32(_IMO));

                        _connSQL.Open();
                        _sqlCommand.ExecuteNonQuery();
                        _connSQL.Close();

                        _oleDR.Close();
                        _connACCESS.Close();

                        return Convert.ToInt32(_IMO);
                    }
                }
                else
                {
                    _oleDR.Close();
                    _connACCESS.Close();
                    _connSQL.Close();

                    return DBNull.Value;
                }
            }
            else
            {
                return DBNull.Value;
            }
        }

        private static void ImportaCorpodiretivo(String CODCON,Int32 Cargo) {
            OleDbConnection _connACCESS = new OleDbConnection(VirDB.Bancovirtual.BancoVirtual.StringConeFinal(VirDB.Bancovirtual.TiposDeBanco.Access));
            OleDbCommand _oleSelectCommand;
            dAccess Dataset = new dAccess();

            String NomeSindico = "";

            
            try
            {
                _connACCESS.Open();
                if (Cargo == 1)
                {
                    _oleSelectCommand = new OleDbCommand("SELECT  [Nome do S�ndico] AS NomeSindico FROM Condominios " +
                                                                      "Where Codcon = @CODCON", _connACCESS);
                    _oleSelectCommand.Parameters.AddWithValue("@CODCON", CODCON.ToUpper());
                    NomeSindico = _oleSelectCommand.ExecuteScalar().ToString();
                    _oleSelectCommand = new OleDbCommand("SELECT Apartamento, Bloco, Nome,'P' as Tipo FROM Propriet�rio " +
                                                                      "Where Codcon = @CODCON and S�ndico = 1 " +
                                                                      "Union " +
                                                                      "SELECT Apartamento, Bloco, Nome,'I' FROM Inquilino " +
                                                                      "Where Codcon = @CODCONI and S�ndico = 1", _connACCESS);
                }
                else
                    _oleSelectCommand = new OleDbCommand("SELECT Apartamento, Bloco, Nome,'P' as Tipo FROM Propriet�rio " +
                                                                      "Where Codcon = @CODCON and [Sub-S�ndico] = 1 " +
                                                                      "Union " +
                                                                      "SELECT Apartamento, Bloco, Nome,'I' FROM Inquilino " +
                                                                      "Where Codcon = @CODCONI and [Sub-S�ndico] = 1", _connACCESS);
                _oleSelectCommand.Parameters.AddWithValue("@CODCON", CODCON.ToUpper());
                _oleSelectCommand.Parameters.AddWithValue("@CODCONI", CODCON.ToUpper());
                OleDbDataReader _oleDR = _oleSelectCommand.ExecuteReader();
                if (!_oleDR.HasRows){
                    if (Cargo == 1)
                       MessageBox.Show("Sindico n�o cadastrado!");
                    _oleDR.Close();
                    _connACCESS.Close();
                    return;
                }
                else
                {
                    while (_oleDR.Read())
                    {                        
                        String BLOCO = _oleDR["BLOCO"].ToString();
                        String APARTAMENTO = _oleDR["APARTAMENTO"].ToString();
                        String Tipo = _oleDR["TIPO"].ToString();
                        String NomeS = _oleDR["Nome"].ToString();

                        if (Cargo == 1) 
                        {
                            if (_oleDR.Read())
                            {
                                MessageBox.Show("ERRO DOIS Sindicos cadastrados!");
                                _oleDR.Close();
                                _connACCESS.Close();
                                return;
                            };
                        }
                        dAccessTableAdapters.CORPODIRETIVOTableAdapter Ta = new dAccessTableAdapters.CORPODIRETIVOTableAdapter();
                        Ta.Tipo = VirDB.Bancovirtual.TiposDeBanco.SQL;
                        Ta.TrocarStringDeConexao();
                        Int32 CONPK = GetSQLCondominioPK(CODCON);
                        Int32 APTPK = GetSQLApartamentoPK(CODCON, BLOCO, APARTAMENTO);
                        Ta.Fill(Dataset.CORPODIRETIVO, Cargo, CONPK);
                        dAccess.CORPODIRETIVORow SindRow;
                        if (Cargo == 1)
                        {
                            if (Dataset.CORPODIRETIVO.Rows.Count == 0)
                                SindRow = (dAccess.CORPODIRETIVORow)Dataset.CORPODIRETIVO.NewRow();
                            else
                                SindRow = (dAccess.CORPODIRETIVORow)Dataset.CORPODIRETIVO.Rows[0];
                        }
                        else {
                            DataRow[] sel = Dataset.CORPODIRETIVO.Select("CDR_APT = " + APTPK.ToString());
                            if (sel.GetLength(0) == 0)
                                SindRow = (dAccess.CORPODIRETIVORow)Dataset.CORPODIRETIVO.NewRow();
                            else
                                SindRow = (dAccess.CORPODIRETIVORow)sel[0];
                        };
                        SindRow.CDR_APT = APTPK;
                        SindRow.CDR_CGO = Cargo;
                        SindRow.CDR_CON = CONPK;
                        Int32 PES;
                        if (Tipo == "P")
                        {
                            PES = GetSQLProprietarioPK(APTPK);
                            SindRow.CDRProprietario = true;
                        }
                        else
                        {
                            PES = GetSQLInquilinoPK(APTPK);
                            SindRow.CDRProprietario = false;
                        }
                         
                        //ATENCAO:
                        //NomeSindico e o nome cadastrado no Access em um campo separado
                        //NomeS e o nome do pro/Inq do apartamento do sindico
                        //NomePI e o nome do sindico no MSSQL
                        if ((NomeSindico != "") && (NomeS.ToUpper() != NomeSindico.ToUpper()))
                        {   //Neste caso o sidico deve ter um cadastro a parte do cadastro do prop/inq
                            String NomePI = "";
                            if(SindRow["CDR_PES"] != DBNull.Value)
                                 NomePI = GetSQLNomePessoaPK(SindRow.CDR_PES);
                            if (NomePI.ToUpper() != NomeSindico.ToUpper())
                            {
                                object retorno = ImportarPessoaDoMsAccessEspecificoparaSindico(NomeS, CODCON, BLOCO, APARTAMENTO, Tipo);
                                SindRow.CDR_PES = Convert.ToInt32(retorno);
                            }
                        }
                        else
                           SindRow.CDR_PES = PES;
                        if (SindRow.RowState == DataRowState.Detached)
                            Dataset.CORPODIRETIVO.AddCORPODIRETIVORow(SindRow);
                        Ta.Update(Dataset.CORPODIRETIVO);
                        Dataset.CORPODIRETIVO.AcceptChanges();
                    };
                };
                _oleDR.Close();
                _connACCESS.Close();
            }
            catch (Exception Erro)
            {

                MessageBox.Show("Ocorreu um erro ao importar o " + CODCON + " do MS Access.\r\n\r\nErro: " + Erro.Message, "Condom�nio", MessageBoxButtons.OK, MessageBoxIcon.Warning);

            };
        }

        private static Object ImportarPessoaDoMsAccessEspecificoparaSindico(String NovoNome, String _CODCON,String _BLOCO,String _APARTAMENTO,String _Proprietario)
        {
            String CODCON = _CODCON;
            String BLOCO = _BLOCO;
            String APARTAMENTO = _APARTAMENTO;

            OleDbConnection _connACCESS = new OleDbConnection(VirDB.Bancovirtual.BancoVirtual.StringConeFinal(VirDB.Bancovirtual.TiposDeBanco.Access));
            SqlConnection _connSQL = new SqlConnection(VirDB.Bancovirtual.BancoVirtual.StringConeFinal(VirDB.Bancovirtual.TiposDeBanco.SQL));

            String _strTabela = "";

            if (_Proprietario == "P")
                _strTabela = "Propriet�rio";
            else
                _strTabela = "Inquilino";

            OleDbCommand _oleSelectCommand = new OleDbCommand("SELECT * " +
                                                              "  FROM  " + _strTabela +
                                                              " WHERE Codcon = @CODCON " +
                                                              "   AND Bloco = @Bloco " +
                                                              "   AND Apartamento = @Apartamento" +
                                                              " ORDER BY CODCON, BLOCO, APARTAMENTO", _connACCESS);

            _oleSelectCommand.Parameters.AddWithValue("@CODCON", CODCON);
            _oleSelectCommand.Parameters.AddWithValue("@BLOCO", BLOCO);
            _oleSelectCommand.Parameters.AddWithValue("@APARTAMENTO", APARTAMENTO);

            _connACCESS.Open();
            OleDbDataReader _oleDR = _oleSelectCommand.ExecuteReader();
            //_connACCESS.Close();

            if (_oleDR.Read())
            {
                
                String _strInsertSQL = "";
                String _strValuesSQL = "";

                
                SqlCommand _sqlIncluirPessoa = new SqlCommand("", _connSQL);

                
                String _Nome = NovoNome;

                
                _strInsertSQL += " PESNOME, ";
                _strValuesSQL += " @NOME, ";
                
                _sqlIncluirPessoa.Parameters.AddWithValue("@NOME", _Nome);
                


                if (_oleDR["CPF"] != DBNull.Value)
                {
                    
                    _strInsertSQL += " PESCPFCNPJ, ";
                    _strValuesSQL += " @CPFCNPJ, ";
                    
                    DocBacarios.CPFCNPJ cpf = new DocBacarios.CPFCNPJ(_oleDR["CPF"].ToString());

                    _sqlIncluirPessoa.Parameters.AddWithValue("@CPFCNPJ", cpf.ToString());

                    // Tipo de pessoa (f�sica ou jur�dica)
                    _strInsertSQL += " PESTipo, ";
                    _strValuesSQL += " @PESTipo, ";
                    //_strUpdateSQL += " PESTipo = @PESTipo, ";

                    if (cpf.Tipo == DocBacarios.TipoCpfCnpj.CNPJ)
                        _sqlIncluirPessoa.Parameters.AddWithValue("@PESTipo", "J");
                    else
                        _sqlIncluirPessoa.Parameters.AddWithValue("@PESTipo", "F");

                }

                if (_oleDR["RG"] != DBNull.Value)
                {
                    //_strCondicaoSQL += " AND PESRGIE = @RGIE ";
                    //_sqlVerificaPessoa.Parameters.AddWithValue("@RGIE", _oleDR["RG"].ToString());

                    _strInsertSQL += " PESRGIE, ";
                    _strValuesSQL += " @RGIE, ";
                    //_strUpdateSQL += " PESRGIE = @RGIE, ";
                    _sqlIncluirPessoa.Parameters.AddWithValue("@RGIE", _oleDR["RG"].ToString());
                }

                if (_oleDR["PIS"] != DBNull.Value)
                {
                    //_strCondicaoSQL += " AND PESPIS = @PIS ";
//                    _sqlVerificaPessoa.Parameters.AddWithValue("@PIS", _oleDR["PIS"].ToString());

                    _strInsertSQL += " PESPIS, ";
                    _strValuesSQL += " @PIS, ";
  //                  _strUpdateSQL += " PESPIS = @PIS, ";
                    _sqlIncluirPessoa.Parameters.AddWithValue("@PIS", _oleDR["PIS"].ToString());
                }

                if (_oleDR["END"] != DBNull.Value)
                {
    //                _strCondicaoSQL += " AND PESENDERECO = @ENDERECO ";
      //              _sqlVerificaPessoa.Parameters.AddWithValue("@ENDERECO", _oleDR["END"].ToString().ToUpper());

                    _strInsertSQL += " PESENDERECO, ";
                    _strValuesSQL += " @ENDERECO, ";
        //            _strUpdateSQL += " PESENDERECO = @ENDERECO, ";
                    _sqlIncluirPessoa.Parameters.AddWithValue("@ENDERECO", _oleDR["END"].ToString().ToUpper());
                }

                if (_oleDR["BAIRRO"] != DBNull.Value)
                {
          //          _strCondicaoSQL += " AND PESBAIRRO = @BAIRRO ";
            //        _sqlVerificaPessoa.Parameters.AddWithValue("@BAIRRO", _oleDR["BAIRRO"].ToString().ToUpper());

                    _strInsertSQL += " PESBAIRRO, ";
                    _strValuesSQL += " @BAIRRO, ";
              //      _strUpdateSQL += " PESBAIRRO = @BAIRRO, ";
                    _sqlIncluirPessoa.Parameters.AddWithValue("@BAIRRO", _oleDR["BAIRRO"].ToString().ToUpper());
                }

                if (_oleDR["CIDADE"] != DBNull.Value)
                {
                //    _strCondicaoSQL += " AND PES_CID = @CIDADE ";
                  //  _sqlVerificaPessoa.Parameters.AddWithValue("@CIDADE", ImportarCidadeDoMsAccess(_oleDR["CIDADE"].ToString(), _oleDR["ESTADO"].ToString().ToUpper()));

                    _strInsertSQL += " PES_CID, ";
                    _strValuesSQL += " @CIDADE, ";
                    //_strUpdateSQL += " PES_CID = @CIDADE, ";
                    _sqlIncluirPessoa.Parameters.AddWithValue("@CIDADE", ImportarCidadeDoMsAccess(_oleDR["CIDADE"].ToString(), _oleDR["ESTADO"].ToString().ToUpper()));
                }


                if (_oleDR["CEP"] != DBNull.Value)
                {
//                    _strCondicaoSQL += " AND PESCEP = @CEP ";
  //                  _sqlVerificaPessoa.Parameters.AddWithValue("@CEP", _oleDR["CEP"].ToString());

                    _strInsertSQL += " PESCEP, ";
                    _strValuesSQL += " @CEP, ";
    //                _strUpdateSQL += " PESCEP = @CEP, ";
                    _sqlIncluirPessoa.Parameters.AddWithValue("@CEP", _oleDR["CEP"].ToString());
                }

                if (_oleDR["TELEFONE1"] != DBNull.Value)
                {
      //              _strCondicaoSQL += " AND PESFONE1 = @TELEFONE1 ";
        //            _sqlVerificaPessoa.Parameters.AddWithValue("@TELEFONE1", _oleDR["TELEFONE1"].ToString());

                    _strInsertSQL += " PESFONE1, ";
                    _strValuesSQL += " @TELEFONE1, ";
          //          _strUpdateSQL += " PESFone1 = @TELEFONE1, ";
                    _sqlIncluirPessoa.Parameters.AddWithValue("@TELEFONE1", _oleDR["TELEFONE1"].ToString());
                }

                if (_oleDR["TELEFONE2"] != DBNull.Value)
                {
            //        _strCondicaoSQL += " AND PESFONE2 = @TELEFONE2 ";
              //      _sqlVerificaPessoa.Parameters.AddWithValue("@TELEFONE2", _oleDR["TELEFONE2"].ToString());

                    _strInsertSQL += " PESFONE2, ";
                    _strValuesSQL += " @TELEFONE2, ";
                //    _strUpdateSQL += " PESFone2 = @TELEFONE2, ";
                    _sqlIncluirPessoa.Parameters.AddWithValue("@TELEFONE2", _oleDR["TELEFONE2"].ToString());
                }

                if (_oleDR["TELEFONE3"] != DBNull.Value)
                {
                  //  _strCondicaoSQL += " AND PESFONE3 = @TELEFONE3 ";
                    //_sqlVerificaPessoa.Parameters.AddWithValue("@TELEFONE3", _oleDR["TELEFONE3"].ToString());

                    _strInsertSQL += " PESFONE3, ";
                    _strValuesSQL += " @TELEFONE3, ";
                    //_strUpdateSQL += " PESFone3 = @TELEFONE3, ";
                    _sqlIncluirPessoa.Parameters.AddWithValue("@TELEFONE3", _oleDR["TELEFONE3"].ToString());
                }

                if (_oleDR["EMAIL"] != DBNull.Value)
                {
//                    _strCondicaoSQL += " AND PESEMAIL = @EMAIL";
  //                  _sqlVerificaPessoa.Parameters.AddWithValue("@EMAIL", _oleDR["EMAIL"].ToString().ToUpper());

                    _strInsertSQL += " PESEMAIL, ";
                    _strValuesSQL += " @EMAIL, ";
    //                _strUpdateSQL += " PESEmail = @EMAIL, ";
                    _sqlIncluirPessoa.Parameters.AddWithValue("@EMAIL", _oleDR["EMAIL"].ToString().ToUpper());
                }

                

                if (_oleDR["NASC"] != DBNull.Value)
                {
      //              _strCondicaoSQL += " AND PESDATANASCIMENTO = @NASC";
        //            _sqlVerificaPessoa.Parameters.AddWithValue("@NASC", Convert.ToDateTime(_oleDR["NASC"]));

                    _strInsertSQL += " PESDATANASCIMENTO, ";
                    _strValuesSQL += " @NASC, ";
          //          _strUpdateSQL += " PESDATANASCIMENTO = @NASC, ";
                    _sqlIncluirPessoa.Parameters.AddWithValue("@NASC", Convert.ToDateTime(_oleDR["NASC"]));
                }

                SqlCommand _sqlDevolvePK = new SqlCommand("Select @@Identity From Pessoas", _connSQL);

                _strInsertSQL = "Insert Into Pessoas ( " + _strInsertSQL.Substring(0, (_strInsertSQL.Length - 2)) + " ) ";
                _strValuesSQL = "             Values ( " + _strValuesSQL.Substring(0, (_strValuesSQL.Length - 2)) + " ) ";

                _sqlIncluirPessoa.CommandText = _strInsertSQL + _strValuesSQL;

                try
                {
                    //MessageBox.Show(CODCON + " - " + BLOCO + " - " + APARTAMENTO + " - " + _oleDR["NOME"].ToString());


                    _connSQL.Open();
                    _sqlIncluirPessoa.ExecuteNonQuery();
                    Object _PK = _sqlDevolvePK.ExecuteScalar();
                    _connSQL.Close();

                    _oleDR.Close();
                    _connACCESS.Close();

                    return _PK;
                }
                catch (Exception Erro)
                {
                    String _Str = "";
                    foreach (SqlParameter _Parametro in _sqlIncluirPessoa.Parameters)
                        _Str += _Parametro.ToString() + " - " + _Parametro.Value.ToString() + " - " + _Parametro.Value.ToString().Length.ToString() + "\r\n\r\n";

                    MessageBox.Show("Ocorreu um erro ao tentar importar o propriet�rio do condom�nio " + CODCON + ", bloco " + BLOCO + " e apartamento " + APARTAMENTO + " do MS Access.\r\n\r\n Erro: " + Erro.Message + "\r\n\r\n" + _Str,
                                    "Importar Propriet�rio do MS Access", MessageBoxButtons.OK, MessageBoxIcon.Warning);

                    _connSQL.Close();

                    _oleDR.Close();
                    _connACCESS.Close();

                    return DBNull.Value;
                }

                

                
                    
            }
            else
            {
                _oleDR.Close();
                _connACCESS.Close();
                return DBNull.Value;
            }
        }



        private static void ImportaCorpodiretivo(String CODCON)
        {
            ImportaCorpodiretivo(CODCON, 1);
            ImportaCorpodiretivo(CODCON, 2);            
        }

        private void ImportarCondominioDoMsAccess(String CODCON)
        {

            /*************************************************************
             * CAMPOS NAO MIGRADOS  
             *************************************************************
            CONTIPOCONSTRUCAO - NAO EXISTE NO ACCESS
            CONTIPOFINALIDADE - NAO EXISTE NO ACCESS
            CONTOTALBLOCO - MIGRADO NA ROTINA DE IMPORTAR BLOCOS
            CONTOTALAPARTAMENTO - MIGRADO NA ROTINA DE IMPORTAR BLOCOS
            CONVALORJUROS - NAO EXISTE NO ACCESS
            CONDIACONTABILIDADE - NAO EXISTE NO ACCESS
            CONQTDEBALANCETEM1 - NAO EXISTE NO ACCESS
            CONQTDEBALANCETEM2 - NAO EXISTE NO ACCESS
            CONQTDEBALANCETEM3 - NAO EXISTE NO ACCESS
            CONQTDEBALANCETEM4 - NAO EXISTE NO ACCESS
            CONQTDEBALANCETEM5 - NAO EXISTE NO ACCESS 
            CONREPASSACORREIO - NAO EXISTE NO ACCESS
            CONVALORAR - NAO EXISTE NO ACCESS
            CONVALORCARTASIMPLES - NAO EXISTE NO ACCESS
            CONCORREIOASSEMBLEIA - NAO EXISTE NO ACCESS
            CONCORREIOCOBRANCA - NAO EXISTE NO ACCESS
            CONCORREIOBOLETO - NAO EXISTE NO ACCESS
            CONSEGURADORA_FRN - NAO EXISTE NO ACCESS
            CONCORRETORASEGUROS_FRN - NAO EXISTE NO ACCESS
            CONVALIDADEAVCB - NAO EXISTE NO ACCESS
            CONDATAULTIMAASSEMBLEIA - NAO EXISTE NO ACCESS
            CONCONVOCACAOASSEMBLEIA - NAO EXISTE NO ACCESS
            CONAUDITOR1_USU - NAO EXISTE NO ACCESS
            CONAUDITOR2_USU - NAO EXISTE NO ACCESS
            CONPERMITEACORDOINTERNET - NAO EXISTE NO ACCESS
            CONMODELOBALANCETEINTERNET - NAO EXISTE NO ACCESS
            ***************************************************************/
            CODCON = CODCON.ToUpper();

            OleDbConnection _connACCESS = new OleDbConnection(VirDB.Bancovirtual.BancoVirtual.StringConeFinal(VirDB.Bancovirtual.TiposDeBanco.Access));
            SqlConnection _connSQL = new SqlConnection(VirDB.Bancovirtual.BancoVirtual.StringConeFinal(VirDB.Bancovirtual.TiposDeBanco.SQL));

            OleDbCommand _oleSelectCommand = new OleDbCommand("Select * from Condominios Where Codcon = @CODCON", _connACCESS);
            _oleSelectCommand.Parameters.AddWithValue("@CODCON", CODCON.ToUpper());

            OleDbDataReader _oleDR = null;

            try
            {
                _connACCESS.Open();
                _oleDR = _oleSelectCommand.ExecuteReader();
                //_connACCESS.Close();

                if (_oleDR.Read())
                {
                    SqlCommand _sqlVerificaCondominio = new SqlCommand("Select CON from Condominios Where CONCodigo = @CODCON", _connSQL);
                    _sqlVerificaCondominio.Parameters.AddWithValue("@CODCON", CODCON);

                    Boolean _IsUpdate = false;

                    _connSQL.Open();
                    _IsUpdate = (_sqlVerificaCondominio.ExecuteScalar() != null);
                    _connSQL.Close();

                    String _strInsertCommand = "";
                    String _strValuesCommand = "";

                    String _strUpdateCommand = "";

                    SqlCommand _sqlCommand = new SqlCommand("", _connSQL);

                    if (_oleDR["CODCON"] != DBNull.Value)
                    {
                        _strInsertCommand += " CONCODIGO, ";
                        _strValuesCommand += " @CODCON, ";

                        _strUpdateCommand += " CONCODIGO = @CODCON, ";

                        _sqlCommand.Parameters.AddWithValue("@CODCON", _oleDR["CODCON"].ToString().ToUpper());
                    }

                    if (_oleDR["NOME"] != DBNull.Value)
                    {
                        _strInsertCommand += " CONNOME, ";
                        _strValuesCommand += " @NOME, ";

                        _strUpdateCommand += " CONNOME = @NOME, ";

                        _sqlCommand.Parameters.AddWithValue("@NOME", _oleDR["NOME"].ToString().ToUpper());
                    }

                    if (_oleDR["END"] != DBNull.Value)
                    {
                        _strInsertCommand += " CONENDERECO, ";
                        _strValuesCommand += " @END, ";

                        _strUpdateCommand += " CONENDERECO = @END, ";

                        _sqlCommand.Parameters.AddWithValue("@END", _oleDR["END"].ToString().ToUpper());
                    }

                    if (_oleDR["BAIRRO"] != DBNull.Value)
                    {
                        _strInsertCommand += " CONBAIRRO, ";
                        _strValuesCommand += " @BAIRRO, ";

                        _strUpdateCommand += " CONBAIRRO = @BAIRRO, ";

                        _sqlCommand.Parameters.AddWithValue("@BAIRRO", _oleDR["BAIRRO"].ToString().ToUpper());
                    }


                    if (_oleDR["CIDADE"] != DBNull.Value)
                    {
                        _strInsertCommand += " CON_CID, ";
                        _strValuesCommand += " @CIDADE, ";

                        _strUpdateCommand += " CON_CID = @CIDADE, ";

                        _sqlCommand.Parameters.AddWithValue("@CIDADE", ImportarCidadeDoMsAccess(_oleDR["CIDADE"].ToString(), _oleDR["ESTADO"].ToString().ToUpper()));
                    }

                    
                    
                    if (_oleDR["CEP"] != DBNull.Value)
                    {
                        _strInsertCommand += " CONCEP, ";
                        _strValuesCommand += " @CEP, ";

                        _strUpdateCommand += " CONCEP = @CEP, ";

                        _sqlCommand.Parameters.AddWithValue("@CEP", _oleDR["CEP"].ToString());
                    }

                    if (_oleDR["OBSERVA��ES"] != DBNull.Value)
                    {
                        _strInsertCommand += " CONOBSERVACAO, ";
                        _strValuesCommand += " @OBS, ";

                        _strUpdateCommand += " CONOBSERVACAO = @OBS, ";

                        _sqlCommand.Parameters.AddWithValue("@OBS ", _oleDR["OBSERVA��ES"].ToString().ToUpper());
                    }

                    if (_oleDR["CNPJ"] != DBNull.Value)
                    {
                        _strInsertCommand += " CONCNPJ, ";
                        _strValuesCommand += " @CNPJ, ";

                        _strUpdateCommand += " CONCNPJ = @CNPJ, ";

                        _sqlCommand.Parameters.AddWithValue("@CNPJ", _oleDR["CNPJ"].ToString());
                    }

                    if (_oleDR["PROCURA��O"] != DBNull.Value)
                    {
                        _strInsertCommand += " CONPROCURACAO, ";
                        _strValuesCommand += " @PROCURACAO, ";

                        _strUpdateCommand += " CONPROCURACAO = @PROCURACAO, ";

                        _sqlCommand.Parameters.AddWithValue("@PROCURACAO", Convert.ToBoolean(_oleDR["PROCURA��O"]));
                    }

                    if (_oleDR["UTILIZA FRA��O"] != DBNull.Value)
                    {
                        _strInsertCommand += " CONFRACAOIDEAL, ";
                        _strValuesCommand += " @FRACAOIDEAL, ";

                        _strUpdateCommand += " CONFRACAOIDEAL = @FRACAOIDEAL, ";

                        _sqlCommand.Parameters.AddWithValue("@FRACAOIDEAL", Convert.ToBoolean(_oleDR["UTILIZA FRA��O"]));
                    }

                    if (_oleDR["S�NDICO RATEIO"] != DBNull.Value)
                    {
                        _strInsertCommand += " CONRATEIOSINDICO, ";
                        _strValuesCommand += " @RATEIOSINDICO, ";

                        _strUpdateCommand += " CONRATEIOSINDICO = @RATEIOSINDICO, ";

                        _sqlCommand.Parameters.AddWithValue("@RATEIOSINDICO", Convert.ToDecimal(_oleDR["S�NDICO RATEIO"]));
                    }

                    if (_oleDR["SUB-S�NDICO RATEIO"] != DBNull.Value)
                    {
                        _strInsertCommand += " CONRATEIOSUBSINDICO, ";
                        _strValuesCommand += " @RATEIOSUBSINDICO, ";

                        _strUpdateCommand += " CONRATEIOSUBSINDICO = @RATEIOSUBSINDICO, ";

                        _sqlCommand.Parameters.AddWithValue("@RATEIOSUBSINDICO", Convert.ToDecimal(_oleDR["SUB-S�NDICO RATEIO"]));
                    }

                    if (_oleDR["S�NDICO CONDOMINIO"] != DBNull.Value)
                    {
                        _strInsertCommand += " CONCONDOMINIOSINDICO, ";
                        _strValuesCommand += " @CONDOMINIOSINDICO, ";

                        _strUpdateCommand += " CONCONDOMINIOSINDICO = @CONDOMINIOSINDICO, ";

                        _sqlCommand.Parameters.AddWithValue("@CONDOMINIOSINDICO", Convert.ToDecimal(_oleDR["S�NDICO CONDOMINIO"]));
                    }

                    if (_oleDR["SUB-S�NDICO CONDOMINIO"] != DBNull.Value)
                    {
                        _strInsertCommand += " CONCONDOMINIOSUBSINDICO, ";
                        _strValuesCommand += " @CONDOMINIOSUBSINDICO, ";

                        _strUpdateCommand += " CONCONDOMINIOSUBSINDICO = @CONDOMINIOSUBSINDICO, ";

                        _sqlCommand.Parameters.AddWithValue("@CONDOMINIOSUBSINDICO", Convert.ToDecimal(_oleDR["SUB-S�NDICO CONDOMINIO"]));
                    }

                    if (_oleDR["S�NDICO PAGA FR"] != DBNull.Value)
                    {
                        _strInsertCommand += " CONSINDICOPAGAFR, ";
                        _strValuesCommand += " @SINDICOPAGAFR, ";

                        _strUpdateCommand += " CONSINDICOPAGAFR = @SINDICOPAGAFR, ";

                        _sqlCommand.Parameters.AddWithValue("@SINDICOPAGAFR", Convert.ToBoolean(_oleDR["S�NDICO PAGA FR"]));
                    }

                    if (_oleDR["INQUILINO PAGA FR"] != DBNull.Value)
                    {
                        _strInsertCommand += " CONINQUILINOPAGAFR, ";
                        _strValuesCommand += " @INQUILINOPAGAFR, ";

                        _strUpdateCommand += " CONINQUILINOPAGAFR = @INQUILINOPAGAFR, ";

                        _sqlCommand.Parameters.AddWithValue("@INQUILINOPAGAFR", Convert.ToBoolean(_oleDR["INQUILINO PAGA FR"]));
                    }

                    if (_oleDR["NUMBANCO"] != DBNull.Value)
                    {
                        _strInsertCommand += " CON_BCO, ";
                        _strValuesCommand += " @NUMBANCO, ";

                        _strUpdateCommand += " CON_BCO = @NUMBANCO, ";

                        _sqlCommand.Parameters.AddWithValue("@NUMBANCO", ImportarBancoDoMsAccess(Convert.ToInt32(_oleDR["NUMBANCO"])));
                    }

                    if (_oleDR["AG�NCIA"] != DBNull.Value)
                    {
                        _strInsertCommand += " CONAGENCIA, ";
                        _strValuesCommand += " @AGENCIA, ";

                        _strUpdateCommand += " CONAGENCIA = @AGENCIA, ";

                        _sqlCommand.Parameters.AddWithValue("@AGENCIA", _oleDR["AG�NCIA"].ToString());
                    }

                    if (_oleDR["DIGITO AG�NCIA"] != DBNull.Value)
                    {
                        _strInsertCommand += " CONDIGITOAGENCIA, ";
                        _strValuesCommand += " @DIGITOAGENCIA, ";

                        _strUpdateCommand += " CONDIGITOAGENCIA = @DIGITOAGENCIA, ";

                        _sqlCommand.Parameters.AddWithValue("@DIGITOAGENCIA", _oleDR["DIGITO AG�NCIA"].ToString());
                    }

                    if (_oleDR["CC"] != DBNull.Value)
                    {
                        _strInsertCommand += " CONCONTA, ";
                        _strValuesCommand += " @CONTA, ";

                        _strUpdateCommand += " CONCONTA = @CONTA, ";

                        _sqlCommand.Parameters.AddWithValue("@CONTA", _oleDR["CC"].ToString().Substring(0, (_oleDR["CC"].ToString().Length - 1)));

                        _strInsertCommand += " CONDIGITOCONTA, ";
                        _strValuesCommand += " @DIGITOCONTA, ";

                        _strUpdateCommand += " CONDIGITOCONTA = @DIGITOCONTA, ";

                        _sqlCommand.Parameters.AddWithValue("@DIGITOCONTA", _oleDR["CC"].ToString().Substring((_oleDR["CC"].ToString().Length - 1), 1));
                    }

                    if (_oleDR["D0"] != DBNull.Value)
                    {
                        _strInsertCommand += " COND0, ";
                        _strValuesCommand += " @D0, ";

                        _strUpdateCommand += " COND0 = @D0, ";

                        _sqlCommand.Parameters.AddWithValue("@D0", Convert.ToBoolean(_oleDR["D0"]));
                    }

                    if (_oleDR["UTILIZA VALOR FR"] != DBNull.Value)
                    {
                        _strInsertCommand += " CONTIPOFR, CONVALORFR, ";
                        _strValuesCommand += " @TIPOFR, @VALORFR, ";

                        _strUpdateCommand += " CONTIPOFR = @TIPOFR, CONVALORFR = @VALORFR, ";

                        if (Convert.ToBoolean(_oleDR["UTILIZA VALOR FR"]) == true)
                        {
                            _sqlCommand.Parameters.AddWithValue("@TIPOFR", "F");
                            _sqlCommand.Parameters.AddWithValue("@VALORFR", (_oleDR["VALOR FUNDO RESERVA"] != DBNull.Value ? Convert.ToDecimal(_oleDR["VALOR FUNDO RESERVA"]) : 0));

                        }
                        else
                        {
                            _sqlCommand.Parameters.AddWithValue("@TIPOFR", "P");
                            _sqlCommand.Parameters.AddWithValue("@VALORFR", (_oleDR["FUNDO DE RESERVA"] != DBNull.Value ? Convert.ToDecimal(_oleDR["FUNDO DE RESERVA"]) : 0));
                        }
                    }

                    if (_oleDR["TIPO DE MULTA"] != DBNull.Value)
                    {
                        _strInsertCommand += " CONTIPOMULTA, ";
                        _strValuesCommand += " @TIPOMULTA, ";

                        _strUpdateCommand += " CONTIPOMULTA = @TIPOMULTA, ";

                        _sqlCommand.Parameters.AddWithValue("@TIPOMULTA", _oleDR["TIPO DE MULTA"].ToString());
                    }

                    if (_oleDR["MULTA"] != DBNull.Value)
                    {
                        _strInsertCommand += " CONVALORMULTA, ";
                        _strValuesCommand += " @MULTA, ";

                        _strUpdateCommand += " CONVALORMULTA = @MULTA, ";

                        _sqlCommand.Parameters.AddWithValue("@MULTA", Convert.ToDecimal(_oleDR["MULTA"]));
                    }

                    if (_oleDR["VALOR DE CONDOMINIO"] != DBNull.Value)
                    {
                        _strInsertCommand += " CONVALORCONDOMINIO, ";
                        _strValuesCommand += " @VALORCONDOMINIO, ";

                        _strUpdateCommand += " CONVALORCONDOMINIO = @VALORCONDOMINIO, ";

                        _sqlCommand.Parameters.AddWithValue("@VALORCONDOMINIO", Convert.ToDecimal(_oleDR["VALOR DE CONDOMINIO"]));
                    }

                    if (_oleDR["VALOR G�S"] != DBNull.Value)
                    {
                        _strInsertCommand += " CONVALORGAS, ";
                        _strValuesCommand += " @VALORGAS, ";

                        _strUpdateCommand += " CONVALORGAS = @VALORGAS, ";

                        _sqlCommand.Parameters.AddWithValue("@VALORGAS", Convert.ToDecimal(_oleDR["VALOR G�S"]));
                    }

                    if (_oleDR["DIA CONDOM�NIO"] != DBNull.Value)
                    {
                        _strInsertCommand += " CONDIAVENCIMENTO, ";
                        _strValuesCommand += " @DIAVENCIMENTO, ";

                        _strUpdateCommand += " CONDIAVENCIMENTO = @DIAVENCIMENTO, ";

                        _sqlCommand.Parameters.AddWithValue("@DIAVENCIMENTO", Convert.ToInt32(_oleDR["DIA CONDOM�NIO"]));

                        _strInsertCommand += " CONTIPOVENCIMENTO, ";
                        _strValuesCommand += " @TIPOVENCIMENTO, ";

                        _strUpdateCommand += " CONTIPOVENCIMENTO = @TIPOVENCIMENTO, ";

                        _sqlCommand.Parameters.AddWithValue("@TIPOVENCIMENTO", "C");

                        _strInsertCommand += " CONREFERENCIAVENCIMENTO, ";
                        _strValuesCommand += " @REFERENCIAVENCIMENTO, ";

                        _strUpdateCommand += " CONREFERENCIAVENCIMENTO = @REFERENCIAVENCIMENTO, ";

                        _sqlCommand.Parameters.AddWithValue("@REFERENCIAVENCIMENTO", "I");

                        _strInsertCommand += " CONPOSTERGAFERIADO, ";
                        _strValuesCommand += " @POSTERGAFERIADO, ";

                        _strUpdateCommand += " CONPOSTERGAFERIADO = @POSTERGAFERIADO, ";

                        _sqlCommand.Parameters.AddWithValue("@POSTERGAFERIADO", false);

                    }

                    if (_oleDR["PERIODO"] != DBNull.Value)
                    {
                        _strInsertCommand += " CONCARTACOBRANCA, ";
                        _strValuesCommand += " @PERIODO, ";

                        _strUpdateCommand += " CONCARTACOBRANCA = @PERIODO, ";

                        _sqlCommand.Parameters.AddWithValue("@PERIODO", Convert.ToInt32(_oleDR["PERIODO"]));
                    }

                    if (_oleDR["ADVOGADO1"] != DBNull.Value)
                    {
                        _strInsertCommand += " CONADVOGADO1_USU, ";
                        _strValuesCommand += " @ADVOGADO1, ";

                        _strUpdateCommand += " CONADVOGADO1_USU = @ADVOGADO1, ";

                        _sqlCommand.Parameters.AddWithValue("@ADVOGADO1", ImportarUsuarioDoMsAccess(_oleDR["ADVOGADO1"].ToString()));
                    }

                    if (_oleDR["ADVOGADO2"] != DBNull.Value)
                    {
                        _strInsertCommand += " CONADVOGADO2_USU, ";
                        _strValuesCommand += " @ADVOGADO2, ";

                        _strUpdateCommand += " CONADVOGADO2_USU = @ADVOGADO2, ";

                        _sqlCommand.Parameters.AddWithValue("@ADVOGADO2", ImportarUsuarioDoMsAccess(_oleDR["ADVOGADO2"].ToString()));
                    }

                    if (_oleDR["ELEVADOR"] != DBNull.Value)
                    {
                        _strInsertCommand += " CONVALIDADEELEVADOR, ";
                        _strValuesCommand += " @ELEVADOR, ";

                        _strUpdateCommand += " CONVALIDADEELEVADOR = @ELEVADOR, ";

                        _sqlCommand.Parameters.AddWithValue("@ELEVADOR", Convert.ToDateTime(_oleDR["ELEVADOR"]));
                    }

                    if (_oleDR["EXTINTOR"] != DBNull.Value)
                    {
                        _strInsertCommand += " CONVALIDADEEXTINTOR, ";
                        _strValuesCommand += " @EXTINTOR, ";

                        _strUpdateCommand += " CONVALIDADEEXTINTOR = @EXTINTOR, ";

                        _sqlCommand.Parameters.AddWithValue("@EXTINTOR", Convert.ToDateTime(_oleDR["EXTINTOR"]));
                    }

                    if (_oleDR["UTILIZA PREVIS�O"] != DBNull.Value)
                    {
                        _strInsertCommand += " CONUTILIZAPREVISAO, ";
                        _strValuesCommand += " @UTILIZAPREVISAO, ";

                        _strUpdateCommand += " CONUTILIZAPREVISAO = @UTILIZAPREVISAO, ";

                        _sqlCommand.Parameters.AddWithValue("@UTILIZAPREVISAO", Convert.ToBoolean(_oleDR["UTILIZA PREVIS�O"]));
                    }

                    if (_oleDR["RODA PREVIS�O"] != DBNull.Value)
                    {
                        _strInsertCommand += " CONIMPRIMEPREVISAO, ";
                        _strValuesCommand += " @IMPRIMEPREVISAO, ";

                        _strUpdateCommand += " CONIMPRIMEPREVISAO = @IMPRIMEPREVISAO, ";

                        _sqlCommand.Parameters.AddWithValue("@IMPRIMEPREVISAO", Convert.ToBoolean(_oleDR["RODA PREVIS�O"]));
                    }

                    if (_oleDR["COMPET�NCIA NO M�S"] != DBNull.Value)
                    {
                        _strInsertCommand += " CONCOMPETENCIAVENCIDA, ";
                        _strValuesCommand += " @COMPETENCIAVENCIDA, ";

                        _strUpdateCommand += " CONCOMPETENCIAVENCIDA = @COMPETENCIAVENCIDA, ";

                        if (Convert.ToBoolean(_oleDR["COMPET�NCIA NO M�S"]) == true)
                            _sqlCommand.Parameters.AddWithValue("@COMPETENCIAVENCIDA", false);
                        else
                            _sqlCommand.Parameters.AddWithValue("@COMPETENCIAVENCIDA", true);
                    }

                    if (_oleDR["BALANCETE BOLETO"] != DBNull.Value)
                    {
                        _strInsertCommand += " CONIMPRIMEBALANCETEBOLETO, ";
                        _strValuesCommand += " @IMPRIMEBALANCETEBOLETO, ";

                        _strUpdateCommand += " CONIMPRIMEBALANCETEBOLETO = @IMPRIMEBALANCETEBOLETO, ";

                        _sqlCommand.Parameters.AddWithValue("@IMPRIMEBALANCETEBOLETO", Convert.ToBoolean(_oleDR["BALANCETE BOLETO"]));
                    }

                    if (_oleDR["INTERNET"] != DBNull.Value)
                    {
                        _strInsertCommand += " CONEXPORTAINTERNET, ";
                        _strValuesCommand += " @INTERNET, ";

                        _strUpdateCommand += " CONEXPORTAINTERNET = @INTERNET, ";

                        _sqlCommand.Parameters.AddWithValue("@INTERNET", Convert.ToBoolean(_oleDR["INTERNET"]));
                    }

                    if (_oleDR["LIBERA BALANCETE"] != DBNull.Value)
                    {
                        _strInsertCommand += " CONPUBLICABALANCETEINTERNET, ";
                        _strValuesCommand += " @PUBLICABALANCETEINTERNET, ";

                        _strUpdateCommand += " CONPUBLICABALANCETEINTERNET = @PUBLICABALANCETEINTERNET, ";

                        _sqlCommand.Parameters.AddWithValue("@PUBLICABALANCETEINTERNET", Convert.ToBoolean(_oleDR["LIBERA BALANCETE"]));
                    }

                    if (_oleDR["OBS INTERNET"] != DBNull.Value)
                    {
                        _strInsertCommand += " CONOBSERVACAOINTERNET, ";
                        _strValuesCommand += " @OBSERVACAOINTERNET, ";

                        _strUpdateCommand += " CONOBSERVACAOINTERNET = @OBSERVACAOINTERNET, ";

                        _sqlCommand.Parameters.AddWithValue("@OBSERVACAOINTERNET", _oleDR["OBS INTERNET"].ToString());
                    }

                    if (_oleDR["DATA CLIENTE"] != DBNull.Value)
                    {
                        _strInsertCommand += " CONDATAINCLUSAO, ";
                        _strValuesCommand += " @DATAINCLUSAO, ";

                        _strUpdateCommand += " CONDATAINCLUSAO = @DATAINCLUSAO, ";

                        _sqlCommand.Parameters.AddWithValue("@DATAINCLUSAO", Convert.ToDateTime(_oleDR["DATA CLIENTE"]));
                    }

                    if (_oleDR["Data Mandato S�ndico"] != DBNull.Value)
                    {
                        _strInsertCommand += " CONDataMandato, ";
                        _strValuesCommand += " @DataMandato, ";

                        _strUpdateCommand += " CONDataMandato = @DataMandato, ";

                        _sqlCommand.Parameters.AddWithValue("@DataMandato", Convert.ToDateTime(_oleDR["Data Mandato S�ndico"]));
                    }

                    if (_IsUpdate)
                    {
                        _sqlCommand.CommandText = "Update Condominios Set " + _strUpdateCommand.Substring(0, (_strUpdateCommand.Length - 2)) + " Where CONCodigo = @CONCodigo ";
                        _sqlCommand.Parameters.AddWithValue("@CONCodigo", CODCON);
                    }
                    else
                    {
                        _strInsertCommand += " CON_EMP, CONStatus, ";
                        _strValuesCommand += "@EMP, @Status, ";
                        _sqlCommand.Parameters.AddWithValue("@EMP", 1);
                        _sqlCommand.Parameters.AddWithValue("@Status", 1);
                        _strInsertCommand = "Insert Into Condominios (" + _strInsertCommand.Substring(0, (_strInsertCommand.Length - 2)) + " ) ";
                        _strValuesCommand = "                 Values (" + _strValuesCommand.Substring(0, (_strValuesCommand.Length - 2)) + " ) ";

                        _sqlCommand.CommandText = _strInsertCommand + _strValuesCommand;
                    }

                    _connSQL.Open();
                    _sqlCommand.ExecuteNonQuery();
                    _connSQL.Close();

                    _oleDR.Close();
                    _connACCESS.Close();
                }
                else
                {
                    MessageBox.Show("N�o foi encontrado o condom�nio " + CODCON + " no MS Access.", "Condom�nio", MessageBoxButtons.OK, MessageBoxIcon.Warning);

                    _oleDR.Close();
                    _connACCESS.Close();
                    _connSQL.Close();
                }
            }
            catch (Exception Erro)
            {

                MessageBox.Show("Ocorreu um erro ao importar o " + CODCON + " do MS Access.\r\n\r\nErro: " + Erro.Message, "Condom�nio", MessageBoxButtons.OK, MessageBoxIcon.Warning);

                _oleDR.Close();
                _connACCESS.Close();
                _connSQL.Close();

            }
        }

        // passar os m�todos para estaticos
        private void ImportarBlocoDoMsAccess(String CODCON)
        {
            OleDbConnection _connACCESS = new OleDbConnection(VirDB.Bancovirtual.BancoVirtual.StringConeFinal(VirDB.Bancovirtual.TiposDeBanco.Access));
            SqlConnection _connSQL = new SqlConnection(VirDB.Bancovirtual.BancoVirtual.StringConeFinal(VirDB.Bancovirtual.TiposDeBanco.SQL));

            OleDbCommand _oleSelectCommand = new OleDbCommand("SELECT Codcon, Bloco, count(apartamento) as quantidade FROM Apartamento WHERE Codcon = @CODCON group by Codcon, Bloco", _connACCESS);
            _oleSelectCommand.Parameters.AddWithValue("@CODCON", CODCON);

            OleDbDataReader _oleDR = null;

            Int32 _BLO_CON = 0;

            Int32 _TotalBloco = 0;

            try
            {
                _connACCESS.Open();
                _oleDR = _oleSelectCommand.ExecuteReader();

                if (_oleDR.HasRows)
                {

                    _BLO_CON = GetSQLCondominioPK(CODCON);

                    while (_oleDR.Read())
                    {
                        _TotalBloco++;

                        SqlCommand _sqlVerificaBloco = new SqlCommand("Select Blocos.BLO From Blocos Inner Join Condominios On Blocos.BLO_CON = Condominios.CON Where Condominios.CONCodigo = @CODCON and Blocos.BLOCodigo = @BLOCO", _connSQL);
                        _sqlVerificaBloco.Parameters.AddWithValue("@CODCON", CODCON);
                        _sqlVerificaBloco.Parameters.AddWithValue("@BLOCO", _oleDR["BLOCO"].ToString());

                        _connSQL.Open();
                        object _BLO = _sqlVerificaBloco.ExecuteScalar();
                        _connSQL.Close();

                        if (_BLO_CON > 0)
                        {
                            SqlCommand _sqlCommand = new SqlCommand("", _connSQL);

                            if (_BLO == null) //Bloco nao existe... precisa ser incluido
                            {
                                _sqlCommand.CommandText = "Insert Into Blocos (BLO_CON, BLOCODIGO, BLONome, BLOTOTALAPARTAMENTO) Values (@BLO_CON, @BLOCodigo, @BLONome, @BLOTotalApartamento)";
                            }
                            else
                            {
                                _sqlCommand.CommandText = "Update Blocos set BLO_CON = @BLO_CON, BLOCODIGO = @BLOCODIGO, BLOTOTALAPARTAMENTO = @BLOTOTALAPARTAMENTO WHERE BLO = @BLO";
                                _sqlCommand.Parameters.AddWithValue("@BLO", Convert.ToInt32(_BLO));
                            }

                            //Parametros padrao entre insert/update
                            _sqlCommand.Parameters.AddWithValue("@BLO_CON", _BLO_CON);
                            _sqlCommand.Parameters.AddWithValue("@BLOCODIGO", _oleDR["BLOCO"].ToString());
                            if (_BLO == null)
                                _sqlCommand.Parameters.AddWithValue("@BLONOME", _oleDR["BLOCO"].ToString());
                            _sqlCommand.Parameters.AddWithValue("@BLOTOTALAPARTAMENTO", Convert.ToInt32(_oleDR["QUANTIDADE"]));

                            _connSQL.Open();
                            _sqlCommand.ExecuteNonQuery();
                            _connSQL.Close();
                        }
                    }
                }
            }
            catch (Exception Erro)
            {
                MessageBox.Show("Ocorreu um erro na importa��o de BLOCO do MS Access.\r\n\r\nErro: " + Erro.Message, "BLOCO");

                _oleDR.Close();
                _connACCESS.Close();
                _connSQL.Close();

                return;
            }

            //Atualizando Totais do Condominio               
            OleDbCommand _oleTotalApto = new OleDbCommand("SELECT count(Apartamento) as bloco FROM Apartamento WHERE Codcon = @CODCON", _connACCESS);
            _oleTotalApto.Parameters.AddWithValue("@CODCON", CODCON);

            Int32 _Apto = 0;

            try
            {
                _Apto = Convert.ToInt32(_oleTotalApto.ExecuteScalar());
            }
            catch (Exception Erro)
            {
                MessageBox.Show("N�o foi poss�vel calcular o total de apartamentos do condom�nio " + CODCON + ".\r\n\r\nErro: " + Erro.Message,
                                "Calcular Total Apartamento", MessageBoxButtons.OK, MessageBoxIcon.Error);
                _Apto = 0;
            }


            SqlCommand _sqlTotalBlocoApto = new SqlCommand("Update Condominios Set CONTotalBloco = @TotalBloco, CONTotalApartamento = @TotalApartamento Where CON = @CON", _connSQL);
            _sqlTotalBlocoApto.Parameters.AddWithValue("@TotalBloco", _TotalBloco);
            _sqlTotalBlocoApto.Parameters.AddWithValue("@TotalApartamento", _Apto);
            _sqlTotalBlocoApto.Parameters.AddWithValue("@CON", _BLO_CON);

            _connSQL.Open();
            _sqlTotalBlocoApto.ExecuteNonQuery();
            _connSQL.Close();
        }

        private static void ImportarApartamentoDoMsAccess(String CODCON, String Bloco, String Apto)
        { 
            OleDbConnection _connACCESS = new OleDbConnection(VirDB.Bancovirtual.BancoVirtual.StringConeFinal(VirDB.Bancovirtual.TiposDeBanco.Access));
            //SqlConnection _connSQL = new SqlConnection(VirDB.Bancovirtual.BancoVirtual.StringConeFinal());

            OleDbCommand _oleSelectCommand = new OleDbCommand("SELECT     Apartamento.Codcon, Apartamento.Bloco, Apartamento.Apartamento, Apartamento.[Fra��o Ideal] as FR, Apartamento.Alugado, Apartamento.[Dia condom�nio] as dia, " +
                                                              "Apartamento.[Dia Diferente] as Diferente, Inquilino.Correio AS CorreioI, Inquilino.[usuario internet] AS UsuarioInternetI, Inquilino.[senha internet] AS SenhaInternetI, " +
                                                              "Propriet�rio.[usuario internet] AS UsuarioInternetP, Propriet�rio.[senha internet] AS SenhaInternetP, Propriet�rio.Correio AS CorreioP " +
                                                              "FROM         ((Apartamento LEFT OUTER JOIN " +
                                                              "Propriet�rio ON Apartamento.Codcon = Propriet�rio.Codcon AND Apartamento.Bloco = Propriet�rio.Bloco AND  " +
                                                              "Apartamento.Apartamento = Propriet�rio.Apartamento) LEFT OUTER JOIN " +
                                                              "Inquilino ON Apartamento.Codcon = Inquilino.Codcon AND Apartamento.Apartamento = Inquilino.Apartamento AND Apartamento.Bloco = Inquilino.Bloco) " +
                                                              " WHERE Apartamento.Codcon = @CODCON AND Apartamento.Bloco = @BLOCO and Apartamento.Apartamento = @Apartamento", _connACCESS);
            _oleSelectCommand.Parameters.AddWithValue("@CODCON", CODCON);
            _oleSelectCommand.Parameters.AddWithValue("@BLOCO", Bloco);
            _oleSelectCommand.Parameters.AddWithValue("@APARTAMENTO", Apto);

            OleDbDataReader _oleDR = null;

            try
            {
                _connACCESS.Open();
                _oleDR = _oleSelectCommand.ExecuteReader();

                if (_oleDR.HasRows)
                {
                    _oleDR.Read();
                    fImportFromMsAccessXXX.ImportarApartamentoDoMsAccess(_oleDR);
                    _oleDR.Close();
                }
            }
            catch{};
        }

        private static void ImportarApartamentoDoMsAccess(OleDbDataReader _oleDR)
        {
            int BLO = GetSQLBlocoPK(_oleDR["CODCON"].ToString(), _oleDR["BLOCO"].ToString());
            SqlConnection _connSQL = new SqlConnection(VirDB.Bancovirtual.BancoVirtual.StringConeFinal(VirDB.Bancovirtual.TiposDeBanco.SQL));
            SqlCommand _sqlVerificaApto = new SqlCommand("SELECT APARTAMENTOS.APT " +
                                                                     "  FROM APARTAMENTOS " +
                                                                     " INNER JOIN BLOCOS ON APARTAMENTOS.APT_BLO = BLOCOS.BLO " +
                                                                     " INNER JOIN CONDOMINIOS ON BLOCOS.BLO_CON = CONDOMINIOS.CON " +
                                                                     " WHERE CONDOMINIOS.CON = @CON " +
                                                                     "   AND BLOCOS.BLO = @BLO " +
                                                                     "   AND APARTAMENTOS.APTNUMERO = @APTNUMERO", _connSQL);
            _sqlVerificaApto.Parameters.AddWithValue("@CON", GetSQLCondominioPK(_oleDR["CODCON"].ToString()));
            _sqlVerificaApto.Parameters.AddWithValue("@BLO", BLO);
            _sqlVerificaApto.Parameters.AddWithValue("@APTNUMERO", _oleDR["APARTAMENTO"].ToString());

            _connSQL.Open();
            object _APT = _sqlVerificaApto.ExecuteScalar();
            _connSQL.Close();

            SqlCommand _sqlCommand = new SqlCommand("", _connSQL);

            if (_APT == null) //Apto nao existe... incluir
            {
                //_sqlCommand.CommandText = "Insert Into Apartamentos (APT_BLO, APTNumero, APTFracaoIdeal, APTProprietario_PES, APTInquilino_PES, APTImobiliaria_FRN, APTDiaCondominio, APTDiaDiferente) Values (@APT_BLO, @APTNumero, @APTFracaoIdeal, @Proprietario, @Inquilino, @Imobiliaria, @Dia,@DiaDiferente)";
                _sqlCommand.CommandText = "Insert Into Apartamentos (APT_BLO, APTNumero, APTFracaoIdeal, APTProprietario_PES, APTInquilino_PES, APTImobiliaria_FRN, APTDiaCondominio, APTDiaDiferente, APTFormaPagtoProprietario_FPG, APTFormaPagtoInquilino_FPG, APTUsuarioInternetProprietario,APTSenhaInternetProprietario,APTUsuarioInternetInquilino,APTSenhaInternetInquilino) " +
                                          "Values (@APT_BLO, @APTNumero, @APTFracaoIdeal, @Proprietario, @Inquilino, @Imobiliaria, @Dia,@DiaDiferente, @APTFormaPagtoProprietario, @APTFormaPagtoInquilino, @APTUsuarioInternetProprietario,@APTSenhaInternetProprietario,@APTUsuarioInternetInquilino,@APTSenhaInternetInquilino)";

            }
            else //Apto existe... atualizar
            {
                _sqlCommand.CommandText = "Update Apartamentos Set APT_BLO = @APT_BLO, APTNumero = @APTNumero, APTFracaoIdeal = @APTFracaoIdeal, APTProprietario_PES = @Proprietario, APTInquilino_PES = @Inquilino, APTImobiliaria_FRN = @Imobiliaria, APTDiaCondominio = @Dia,APTDiaDiferente=@DiaDiferente"+
                ",APTFormaPagtoProprietario_FPG=@APTFormaPagtoProprietario, APTFormaPagtoInquilino_FPG=@APTFormaPagtoInquilino"+
                ",APTUsuarioInternetProprietario=@APTUsuarioInternetProprietario,APTSenhaInternetProprietario=@APTSenhaInternetProprietario"+
                ",APTUsuarioInternetInquilino   =@APTUsuarioInternetInquilino   ,APTSenhaInternetInquilino   =@APTSenhaInternetInquilino "+
                " Where APT = @APT";
                _sqlCommand.Parameters.AddWithValue("@APT", Convert.ToInt32(_APT));
            }

            //Parametros padrao entre insert/update
            _sqlCommand.Parameters.AddWithValue("@APT_BLO", BLO);
            _sqlCommand.Parameters.AddWithValue("@APTNumero", _oleDR["Apartamento"].ToString());
            _sqlCommand.Parameters.AddWithValue("@APTFracaoIdeal", Convert.ToDecimal(_oleDR["FR"]));
            _sqlCommand.Parameters.AddWithValue("@Proprietario", ImportarPessoaDoMsAccess(true, _oleDR["CODCON"].ToString(), _oleDR["BLOCO"].ToString(), _oleDR["APARTAMENTO"].ToString()));
            _sqlCommand.Parameters.AddWithValue("@Inquilino", ImportarPessoaDoMsAccess(false, _oleDR["CODCON"].ToString(), _oleDR["BLOCO"].ToString(), _oleDR["APARTAMENTO"].ToString()));
            _sqlCommand.Parameters.AddWithValue("@Imobiliaria", ImportarImobiliariaDoMsAccess(_oleDR["CODCON"].ToString(), _oleDR["BLOCO"].ToString(), _oleDR["APARTAMENTO"].ToString()));
            _sqlCommand.Parameters.AddWithValue("@Dia", Convert.ToInt32(_oleDR["Dia"]));
            _sqlCommand.Parameters.AddWithValue("@DiaDiferente", (bool)(_oleDR["Diferente"]));
            if (_oleDR["CorreioP"] != DBNull.Value)
                if ((bool)(_oleDR["CorreioP"]))
                     _sqlCommand.Parameters.AddWithValue("@APTFormaPagtoProprietario", 2);
                else
                     _sqlCommand.Parameters.AddWithValue("@APTFormaPagtoProprietario", 4);
            else
                _sqlCommand.Parameters.AddWithValue("@APTFormaPagtoProprietario", DBNull.Value);
            if (_oleDR["CorreioI"] != DBNull.Value)
                if ((bool)(_oleDR["CorreioI"]))
                     _sqlCommand.Parameters.AddWithValue("@APTFormaPagtoInquilino", 2);
                else
                     _sqlCommand.Parameters.AddWithValue("@APTFormaPagtoInquilino", 4);            
            else
                _sqlCommand.Parameters.AddWithValue("@APTFormaPagtoInquilino", DBNull.Value);         
            _sqlCommand.Parameters.AddWithValue("@APTUsuarioInternetProprietario", _oleDR["UsuarioInternetP"].ToString());
            _sqlCommand.Parameters.AddWithValue("@APTSenhaInternetProprietario", _oleDR["SenhaInternetP"].ToString());
            _sqlCommand.Parameters.AddWithValue("@APTUsuarioInternetInquilino", _oleDR["UsuarioInternetI"].ToString());
            _sqlCommand.Parameters.AddWithValue("@APTSenhaInternetInquilino", _oleDR["SenhaInternetI"].ToString());
            

            _connSQL.Open();
            _sqlCommand.ExecuteNonQuery();
            _connSQL.Close();
        }

        private static OleDbConnection _connACCESS = null;
        private static SqlConnection _connSQL = null;

        private static void ImportarApartamentoDoMsAccess(String CODCON)
        {
            if(_connACCESS == null)
                  _connACCESS = new OleDbConnection(VirDB.Bancovirtual.BancoVirtual.StringConeFinal(VirDB.Bancovirtual.TiposDeBanco.Access));
            
            if(_connSQL == null)
                  _connSQL = new SqlConnection(VirDB.Bancovirtual.BancoVirtual.StringConeFinal(VirDB.Bancovirtual.TiposDeBanco.SQL));

            //OleDbCommand _oleSelectCommand = new OleDbCommand("SELECT Apartamento.Codcon, Apartamento.Bloco, Apartamento.Apartamento, Apartamento.[Fra��o Ideal] AS FR, Apartamento.[Dia Condom�nio] AS DIA, Apartamento.[Dia Diferente] as Diferente " +
            //                                                  "  FROM Apartamento INNER JOIN Condominios ON Apartamento.Codcon = Condominios.CODCON " +
            //                                                  " WHERE Apartamento.Codcon = @CODCON " + 
            //                                                  " ORDER By Apartamento.Codcon, Apartamento.Bloco, Apartamento.Apartamento", _connACCESS);

            OleDbCommand _oleSelectCommand = new OleDbCommand("SELECT     Apartamento.Codcon, Apartamento.Bloco, Apartamento.Apartamento, Apartamento.[Fra��o Ideal] as FR, Apartamento.Alugado, Apartamento.[Dia condom�nio] as dia, " +
                                                              "Apartamento.[Dia Diferente] as Diferente, Inquilino.Correio AS CorreioI, Inquilino.[usuario internet] AS UsuarioInternetI, Inquilino.[senha internet] AS SenhaInternetI, " +
                                                              "Propriet�rio.[usuario internet] AS UsuarioInternetP, Propriet�rio.[senha internet] AS SenhaInternetP, Propriet�rio.Correio AS CorreioP " +
                                                              "FROM         ((Apartamento LEFT OUTER JOIN " +
                                                              "Propriet�rio ON Apartamento.Codcon = Propriet�rio.Codcon AND Apartamento.Bloco = Propriet�rio.Bloco AND  " +
                                                              "Apartamento.Apartamento = Propriet�rio.Apartamento) LEFT OUTER JOIN " +
                                                              "Inquilino ON Apartamento.Codcon = Inquilino.Codcon AND Apartamento.Apartamento = Inquilino.Apartamento AND Apartamento.Bloco = Inquilino.Bloco) " +
                                                              "WHERE     (Apartamento.Codcon = @CODCON) ", _connACCESS);

            _oleSelectCommand.Parameters.AddWithValue("@CODCON", CODCON);

            OleDbDataReader _oleDR = null;

            try
            {
                
                _connACCESS.Open();
                _oleDR = _oleSelectCommand.ExecuteReader();

                if (_oleDR.HasRows)
                {
                    while (_oleDR.Read())
                    {
                        ImportarApartamentoDoMsAccess(_oleDR);                        
                    }



                };
                if (_connACCESS.State == ConnectionState.Open)
                {
                    _oleDR.Close();
                    _connACCESS.Close();
                }
            }
            catch (Exception Erro)
            {
                MessageBox.Show("Ocorreu um erro na importa��o de APARTAMENTO do MS Access.\r\n\r\nErro: " + Erro.Message, "APARTAMENTO");

                _oleDR.Close();
                _connACCESS.Close();
                _connSQL.Close();
            }
        }

        private static void ImportarApartamentoDoMsAccess(int CON)
        {
            ImportarApartamentoDoMsAccess(GetCODCON(CON));
        }

        private void fImportFromMsAccess_Load(object sender, EventArgs e)
        {
        }

        private void toolStripButton1_Click(object sender, EventArgs e)
        {
        }

        

        private void toolStripButton4_Click(object sender, EventArgs e)
        {
            ImportarEspecificos();
        }

        private void toolStripButton3_Click(object sender, EventArgs e)
        {
            Parar = false;
            CompontesBasicos.FEspera.Espere("Atualizando dados do Access");
            Framework.DSCentral.DCentral.fill_CON();
            foreach (Framework.DSCentral.CONDOMINIOSRow CONDOMINIOSRow in Framework.DSCentral.DCentral.CONDOMINIOS)
            {
                CompontesBasicos.FEspera.Espere(CONDOMINIOSRow.CONCodigo);
                Cadastros.Utilitarios.MSAccess.fImportFromMsAccessXXX.ImportarApartamentoDoMsAccess(CONDOMINIOSRow.CON);
                Application.DoEvents();
                if(Parar)
                    break;
            };
            
            CompontesBasicos.FEspera.Espere();
        }

        bool Parar = false;

        private void toolStripButton5_Click(object sender, EventArgs e)
        {
            Parar = true;
        }

        private void toolStripButton2_Click(object sender, EventArgs e)
        {

        }
    }
}

