namespace Cadastros.Utilitarios.MSAccess
{
    partial class fImportacaoGeral
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(fImportacaoGeral));
            this.chkCondominio = new DevExpress.XtraEditors.CheckEdit();
            this.chkBlocos = new DevExpress.XtraEditors.CheckEdit();
            this.chkApartamentos = new DevExpress.XtraEditors.CheckEdit();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.panel1 = new System.Windows.Forms.Panel();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.panel2 = new System.Windows.Forms.Panel();
            this.btnFechar = new DevExpress.XtraEditors.SimpleButton();
            this.btnImportar = new DevExpress.XtraEditors.SimpleButton();
            this.panel3 = new System.Windows.Forms.Panel();
            this.progressBarOperacao = new DevExpress.XtraEditors.ProgressBarControl();
            this.progressBarItem = new DevExpress.XtraEditors.ProgressBarControl();
            ((System.ComponentModel.ISupportInitialize)(this.chkCondominio.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkBlocos.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkApartamentos.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.panel1.SuspendLayout();
            this.panel2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.progressBarOperacao.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.progressBarItem.Properties)).BeginInit();
            this.SuspendLayout();
            // 
            // chkCondominio
            // 
            this.chkCondominio.Location = new System.Drawing.Point(126, 79);
            this.chkCondominio.Name = "chkCondominio";
            this.chkCondominio.Properties.Appearance.ForeColor = System.Drawing.Color.Black;
            this.chkCondominio.Properties.Appearance.Options.UseForeColor = true;
            this.chkCondominio.Properties.Caption = "";
            this.chkCondominio.Size = new System.Drawing.Size(365, 19);
            this.chkCondominio.TabIndex = 0;
            // 
            // chkBlocos
            // 
            this.chkBlocos.Location = new System.Drawing.Point(126, 104);
            this.chkBlocos.Name = "chkBlocos";
            this.chkBlocos.Properties.Appearance.ForeColor = System.Drawing.Color.Black;
            this.chkBlocos.Properties.Appearance.Options.UseForeColor = true;
            this.chkBlocos.Properties.Caption = "";
            this.chkBlocos.Size = new System.Drawing.Size(365, 19);
            this.chkBlocos.TabIndex = 1;
            // 
            // chkApartamentos
            // 
            this.chkApartamentos.Location = new System.Drawing.Point(126, 129);
            this.chkApartamentos.Name = "chkApartamentos";
            this.chkApartamentos.Properties.Appearance.ForeColor = System.Drawing.Color.Black;
            this.chkApartamentos.Properties.Appearance.Options.UseForeColor = true;
            this.chkApartamentos.Properties.Caption = "";
            this.chkApartamentos.Size = new System.Drawing.Size(365, 19);
            this.chkApartamentos.TabIndex = 2;
            // 
            // pictureBox1
            // 
            this.pictureBox1.Dock = System.Windows.Forms.DockStyle.Left;
            this.pictureBox1.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox1.Image")));
            this.pictureBox1.Location = new System.Drawing.Point(0, 0);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(118, 318);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize;
            this.pictureBox1.TabIndex = 4;
            this.pictureBox1.TabStop = false;
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.Color.White;
            this.panel1.Controls.Add(this.label2);
            this.panel1.Controls.Add(this.label1);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel1.Location = new System.Drawing.Point(118, 0);
            this.panel1.Name = "panel1";
            this.panel1.Padding = new System.Windows.Forms.Padding(5);
            this.panel1.Size = new System.Drawing.Size(382, 42);
            this.panel1.TabIndex = 5;
            // 
            // label2
            // 
            this.label2.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.label2.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.ForeColor = System.Drawing.SystemColors.Desktop;
            this.label2.Location = new System.Drawing.Point(5, 24);
            this.label2.Name = "label2";
            this.label2.Padding = new System.Windows.Forms.Padding(10, 0, 0, 0);
            this.label2.Size = new System.Drawing.Size(372, 13);
            this.label2.TabIndex = 1;
            this.label2.Text = "Clique em \"Importar\" para com inciar a importação dos dados";
            this.label2.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label1
            // 
            this.label1.Dock = System.Windows.Forms.DockStyle.Top;
            this.label1.Font = new System.Drawing.Font("Verdana", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.SystemColors.Desktop;
            this.label1.Location = new System.Drawing.Point(5, 5);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(372, 17);
            this.label1.TabIndex = 0;
            this.label1.Text = "Bem vindo ao importador de dados do Microsoft Access!";
            // 
            // panel2
            // 
            this.panel2.Controls.Add(this.btnFechar);
            this.panel2.Controls.Add(this.btnImportar);
            this.panel2.Controls.Add(this.panel3);
            this.panel2.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panel2.Location = new System.Drawing.Point(118, 290);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(382, 28);
            this.panel2.TabIndex = 6;
            // 
            // btnFechar
            // 
            this.btnFechar.Location = new System.Drawing.Point(305, 3);
            this.btnFechar.Name = "btnFechar";
            this.btnFechar.Size = new System.Drawing.Size(74, 23);
            this.btnFechar.TabIndex = 2;
            this.btnFechar.Text = "Fechar";
            this.btnFechar.Click += new System.EventHandler(this.btnFechar_Click);
            // 
            // btnImportar
            // 
            this.btnImportar.Location = new System.Drawing.Point(224, 3);
            this.btnImportar.Name = "btnImportar";
            this.btnImportar.Size = new System.Drawing.Size(75, 23);
            this.btnImportar.TabIndex = 1;
            this.btnImportar.Text = "Importar";
            this.btnImportar.Click += new System.EventHandler(this.btnImportar_Click);
            // 
            // panel3
            // 
            this.panel3.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.panel3.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel3.Location = new System.Drawing.Point(3, 0);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(382, 1);
            this.panel3.TabIndex = 0;
            // 
            // progressBarOperacao
            // 
            this.progressBarOperacao.Location = new System.Drawing.Point(126, 264);
            this.progressBarOperacao.Name = "progressBarOperacao";
            this.progressBarOperacao.Properties.LookAndFeel.SkinName = "Money Twins";
            this.progressBarOperacao.Properties.LookAndFeel.UseDefaultLookAndFeel = false;
            this.progressBarOperacao.Properties.Step = 1;
            this.progressBarOperacao.Size = new System.Drawing.Size(365, 20);
            this.progressBarOperacao.TabIndex = 7;
            // 
            // progressBarItem
            // 
            this.progressBarItem.Location = new System.Drawing.Point(126, 243);
            this.progressBarItem.Name = "progressBarItem";
            this.progressBarItem.Properties.LookAndFeel.SkinName = "iMaginary";
            this.progressBarItem.Properties.LookAndFeel.UseDefaultLookAndFeel = false;
            this.progressBarItem.Properties.Maximum = 15;
            this.progressBarItem.Properties.Step = 5;
            this.progressBarItem.Size = new System.Drawing.Size(365, 15);
            this.progressBarItem.TabIndex = 8;
            // 
            // fImportacaoGeral
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.WhiteSmoke;
            this.ClientSize = new System.Drawing.Size(500, 318);
            this.Controls.Add(this.progressBarItem);
            this.Controls.Add(this.progressBarOperacao);
            this.Controls.Add(this.panel2);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.pictureBox1);
            this.Controls.Add(this.chkApartamentos);
            this.Controls.Add(this.chkBlocos);
            this.Controls.Add(this.chkCondominio);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "fImportacaoGeral";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Importação de Condomínios";
            ((System.ComponentModel.ISupportInitialize)(this.chkCondominio.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkBlocos.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkApartamentos.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.panel1.ResumeLayout(false);
            this.panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.progressBarOperacao.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.progressBarItem.Properties)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private DevExpress.XtraEditors.CheckEdit chkCondominio;
        private DevExpress.XtraEditors.CheckEdit chkBlocos;
        private DevExpress.XtraEditors.CheckEdit chkApartamentos;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Panel panel3;
        private DevExpress.XtraEditors.SimpleButton btnFechar;
        private DevExpress.XtraEditors.SimpleButton btnImportar;
        private DevExpress.XtraEditors.ProgressBarControl progressBarOperacao;
        private DevExpress.XtraEditors.ProgressBarControl progressBarItem;
    }
}