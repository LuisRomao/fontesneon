using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using System.Data.OleDb;
using DocBacarios;
using CadastrosProc;

namespace Cadastros.Utilitarios.MSAccess
{
    /// <summary>
    /// 
    /// </summary>
    public partial class fExportToMsAccess : Form
    {

        
        /// <summary>
        /// 
        /// </summary>
        public fExportToMsAccess()
        {
            InitializeComponent();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="CODCON"></param>
        /// <returns></returns>
        protected Int32 GetSQLCondominioPK(String CODCON)
        {
            SqlConnection _connSQL = new SqlConnection(VirDB.Bancovirtual.BancoVirtual.StringConeFinal(VirDB.Bancovirtual.TiposDeBanco.SQL));

            SqlCommand _sqlCommand = new SqlCommand("Select CON From Condominios Where CONCodigo = @CODCON", _connSQL);
            _sqlCommand.Parameters.AddWithValue("@CODCON", CODCON);

            Int32 _PK = 0;
            
            try
            {
                _connSQL.Open();
                _PK = Convert.ToInt32(_sqlCommand.ExecuteScalar());
                _connSQL.Close();

                return _PK;
            }
            catch
            {
                _connSQL.Close();
                return 0;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="CIDPK"></param>
        /// <returns></returns>
        protected String[] ExportarCidadeParaMsAccess(Int32 CIDPK)
        {
            SqlConnection _connSQL = new SqlConnection(VirDB.Bancovirtual.BancoVirtual.StringConeFinal(VirDB.Bancovirtual.TiposDeBanco.SQL));

            SqlCommand _sqlCommand = new SqlCommand("Select CIDNOME, CIDUF From CIDADES Where CID = @CID", _connSQL);
            _sqlCommand.Parameters.AddWithValue("@CID", CIDPK);

            Object _Nome = DBNull.Value;

            String[] _Return = new String[2];

            _Return[0] = "";
            _Return[1] = "";

            try
            {
                _connSQL.Open();
                SqlDataReader _DR = _sqlCommand.ExecuteReader();

                if (_DR.Read())
                {
                    _Return[0] = _DR["CIDNome"].ToString();
                    _Return[1] = _DR["CIDUF"].ToString();
                }
            
                _DR.Close();
                _connSQL.Close();

                return _Return;
            }
            catch
            {
                _connSQL.Close();
                return _Return;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="USUPK"></param>
        /// <returns></returns>
        protected Object ExportarUsuarioParaMsAccess(Int32 USUPK)
        {
            SqlConnection _connSQL = new SqlConnection(VirDB.Bancovirtual.BancoVirtual.StringConeFinal(VirDB.Bancovirtual.TiposDeBanco.SQL));

            SqlCommand _sqlCommand = new SqlCommand("Select USUNOME From USUARIOS Where USU = @USU", _connSQL);
            _sqlCommand.Parameters.AddWithValue("@USU", USUPK);

            Object _Nome = DBNull.Value;

            try
            {
                _connSQL.Open();
                _Nome = _sqlCommand.ExecuteScalar().ToString();
                _connSQL.Close();

                return _Nome;
            }
            catch
            {
                _connSQL.Close();
                return DBNull.Value;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="IMOPK"></param>
        /// <param name="CODCON"></param>
        /// <param name="BLOCO"></param>
        /// <param name="APARTAMENTO"></param>
        protected void ExportarImobiliariaParaMsAccess(Int32 IMOPK, String CODCON, String BLOCO, String APARTAMENTO)
        {
            SqlConnection _connSQL = new SqlConnection(VirDB.Bancovirtual.BancoVirtual.StringConeFinal(VirDB.Bancovirtual.TiposDeBanco.SQL));
            OleDbConnection _connACCESS = new OleDbConnection(VirDB.Bancovirtual.BancoVirtual.StringConeFinal(VirDB.Bancovirtual.TiposDeBanco.Access));

            SqlDataReader _sqlDR = null;
            OleDbCommand _oleCommand = new OleDbCommand("", _connACCESS);

            try
            {
                SqlCommand _sqlSelectCommand = new SqlCommand("Select * From Fornecedores Where FRN = @FRN", _connSQL);
                _sqlSelectCommand.Parameters.AddWithValue("@FRN", IMOPK);

                _connSQL.Open();
                _sqlDR = _sqlSelectCommand.ExecuteReader();

                if (_sqlDR.Read())
                {
                    OleDbCommand _oleVerificaImobiliaria = new OleDbCommand("Select CODIMO From Imobili�ria Where CODIMO = @CODIMO", _connACCESS);
                    _oleVerificaImobiliaria.Parameters.AddWithValue("@CODIMO", _sqlDR["FRNFantasia"].ToString());

                    _connACCESS.Open();
                    Boolean _IsUpdate = (_oleVerificaImobiliaria.ExecuteScalar() != null);
                    _connACCESS.Close();

                    String _strUpdateCommand = "";
                    String _strInsertCommand = "";
                    String _strValuesCommand = "";

                    if (_sqlDR["FRNFantasia"].ToString() != "")
                    {
                        _strUpdateCommand += " CODIMO = @CODIMO, ";

                        _strInsertCommand += " CODIMO, ";
                        _strValuesCommand += " @CODIMO, ";

                        _oleCommand.Parameters.AddWithValue("@CODIMO", _sqlDR["FRNFantasia"].ToString());
                    }

                    if (_sqlDR["FRNNome"].ToString() != "")
                    {
                        _strUpdateCommand += " Nome = @Nome, ";

                        _strInsertCommand += " Nome, ";
                        _strValuesCommand += " @Nome, ";

                        _oleCommand.Parameters.AddWithValue("@NOME", _sqlDR["FRNNome"].ToString());
                    }

                    if (_sqlDR["FRNEndereco"].ToString() != "")
                    {
                        _strUpdateCommand += " [End] = @End, ";

                        _strInsertCommand += " [End], ";
                        _strValuesCommand += " @End, ";

                        _oleCommand.Parameters.AddWithValue("@End", _sqlDR["FRNEndereco"].ToString());
                    }

                    if (_sqlDR["FRNBairro"].ToString() != "")
                    {
                        _strUpdateCommand += " Bairro = @Bairro, ";

                        _strInsertCommand += " Bairro, ";
                        _strValuesCommand += " @Bairro, ";

                        _oleCommand.Parameters.AddWithValue("@Bairro", _sqlDR["FRNBairro"].ToString());
                    }

                    if (_sqlDR["FRNCEP"].ToString() != "")
                    {
                        _strUpdateCommand += " CEP = @CEP, ";

                        _strInsertCommand += " CEP, ";
                        _strValuesCommand += " @CEP, ";

                        _oleCommand.Parameters.AddWithValue("@CEP", _sqlDR["FRNCEP"].ToString());
                    }

                    if (_sqlDR["FRN_CID"].ToString() != "")
                    {

                        String[] _Cidade = ExportarCidadeParaMsAccess(Convert.ToInt32(_sqlDR["FRN_CID"]));

                        _strUpdateCommand += " CIDADE = @CIDADE, ";

                        _strInsertCommand += " CIDADE, ";
                        _strValuesCommand += " @CIDADE, ";

                        if (_Cidade[0] != "")
                            _oleCommand.Parameters.AddWithValue("@CIDADE", _Cidade[0]);
                        else
                            _oleCommand.Parameters.AddWithValue("@CIDADE", "  ");

                        //_oleCommand.Parameters.AddWithValue("@CIDADE", _Cidade[0]);


                        _strUpdateCommand += " ESTADO = @ESTADO, ";

                        _strInsertCommand += " ESTADO, ";
                        _strValuesCommand += " @ESTADO, ";

                        _oleCommand.Parameters.AddWithValue("@ESTADO", _Cidade[1]);

                    }

                    /*
                    if (_sqlDR["FRNUF"].ToString() != "")
                    {
                        _strUpdateCommand += " ESTADO = @ESTADO, ";

                        _strInsertCommand += " ESTADO, ";
                        _strValuesCommand += " @ESTADO, ";

                        _oleCommand.Parameters.AddWithValue("@ESTADO", _sqlDR["FRNUF"].ToString());
                    }
                    */

                    if (_sqlDR["FRNFONE1"].ToString() != "")
                    {
                        _strUpdateCommand += " TELEFONE1 = @FONE1, ";

                        _strInsertCommand += " TELEFONE1, ";
                        _strValuesCommand += " @FONE1, ";

                        _oleCommand.Parameters.AddWithValue("@FONE1", _sqlDR["FRNFONE1"].ToString());
                    }

                    if (_sqlDR["FRNFONE2"].ToString() != "")
                    {
                        _strUpdateCommand += " TELEFONE2 = @FONE2, ";

                        _strInsertCommand += " TELEFONE2, ";
                        _strValuesCommand += " @FONE2, ";

                        _oleCommand.Parameters.AddWithValue("@FONE2", _sqlDR["FRNFONE2"].ToString());
                    }

                    if (_sqlDR["FRNFONE3"].ToString() != "")
                    {
                        _strUpdateCommand += " TELEFONE3 = @FONE3, ";

                        _strInsertCommand += " TELEFONE3, ";
                        _strValuesCommand += " @FONE3, ";

                        _oleCommand.Parameters.AddWithValue("@FONE3", _sqlDR["FRNFONE3"].ToString());
                    }

                    if (_IsUpdate)
                        _oleCommand.CommandText = "Update [Imobili�ria] Set " + _strUpdateCommand.Substring(0, (_strUpdateCommand.Length - 2)) + " Where CODIMO = @CODIMO";
                    else
                        _oleCommand.CommandText = "Insert Into [Imobili�ria] ( " + _strInsertCommand.Substring(0, (_strInsertCommand.Length - 2)) + " ) Values ( " + _strValuesCommand.Substring(0, (_strValuesCommand.Length - 2)) + " ) ";

                    _connACCESS.Open();
                    _oleCommand.ExecuteNonQuery();
                    _connACCESS.Close();

                    //Acertando CODIMO do Inquilinio
                    OleDbCommand _oleUpdateCommand = new OleDbCommand("Update Inquilino Set CODIMO = @CODIMO WHERE CODCON = @CODCON AND BLOCO = @BLOCO AND APARTAMENTO = @APARTAMENTO", _connACCESS);
                    _oleUpdateCommand.Parameters.AddWithValue("@CODIMO", _sqlDR["FRNFantasia"].ToString());
                    _oleUpdateCommand.Parameters.AddWithValue("@CODCON", CODCON );
                    _oleUpdateCommand.Parameters.AddWithValue("@BLOCO", BLOCO);
                    _oleUpdateCommand.Parameters.AddWithValue("@APARTAMENTO", APARTAMENTO);

                    _connACCESS.Open();
                    _oleUpdateCommand.ExecuteNonQuery();
                    _connACCESS.Close();

                    _sqlDR.Close();
                    _connSQL.Close();
                }
            }
            catch(Exception Erro)
            {

                MessageBox.Show("Ocorreu um erro ao tentar incluir a imobiliaria.\r\n\r\nErro: " + Erro.Message, "Incluir", MessageBoxButtons.OK, MessageBoxIcon.Error);
                
                _sqlDR.Close();
                _connSQL.Close();
                _connACCESS.Close();
            }

        }

    
        /// <summary>
        /// 
        /// </summary>
        /// <param name="Condominiorow"></param>
        public void ExportarCondominioParaMsAccessRow(Condominios.dCondominiosCampos.CONDOMINIOSRow Condominiorow)
        {

            OleDbConnection _connACCESS = new OleDbConnection(VirDB.Bancovirtual.BancoVirtual.StringConeFinal(VirDB.Bancovirtual.TiposDeBanco.Access));
            OleDbCommand _oleCommand = new OleDbCommand("", _connACCESS);
            
            try
            {                                
                    OleDbCommand _oleVerificaCondominio = new OleDbCommand("Select CODCON From Condominios Where CODCON = @CODCON", _connACCESS);
                    _oleVerificaCondominio.Parameters.AddWithValue("CODCON", Condominiorow.CONCodigo);

                    _connACCESS.Open();
                    Boolean _IsUpdate = (_oleVerificaCondominio.ExecuteScalar() != null);
                    _connACCESS.Close();

                    String _strInsertCommand = "";
                    String _strValuesCommand = "";

                    String _strUpdateCommand = "";

                    //OleDbCommand _oleCommand = new OleDbCommand("", _connACCESS);

                    if (Condominiorow["CONTotalApartamento"].ToString() != "")
                    {
                        _strInsertCommand += " NUM_UNID, ";
                        _strValuesCommand += " @NUM_UNID, ";

                        _strUpdateCommand += " NUM_UNID = @NUM_UNID, ";

                        _oleCommand.Parameters.AddWithValue("@NUM_UNID", Convert.ToDouble(Condominiorow.CONTotalApartamento));
                    }
                    else {
                        // Requerido pelo MS Access - Sera atualizado na rotina de exportacao de blocos
                        _strInsertCommand += " NUM_UNID, ";
                        _strValuesCommand += " @NUM_UNID, ";
                        _strUpdateCommand += " NUM_UNID = @NUM_UNID, ";
                        _oleCommand.Parameters.AddWithValue("@NUM_UNID", 0);
                    }
                
                    if (Condominiorow["CONNOME"].ToString() != "")
                    {
                        _strInsertCommand += " NOME, ";
                        _strValuesCommand += " @NOME, ";

                        _strUpdateCommand += " NOME = @NOME, ";

                        _oleCommand.Parameters.AddWithValue("@NOME", Condominiorow["CONNOME"].ToString().ToUpper());
                    }

                    if (Condominiorow["CONENDERECO"].ToString() != "")
                    {
                        _strInsertCommand += " [END], ";
                        _strValuesCommand += " @END, ";

                        _strUpdateCommand += " [END] = @END, ";

                        _oleCommand.Parameters.AddWithValue("@END", Condominiorow["CONENDERECO"].ToString().ToUpper());
                    }

                    if (Condominiorow["CON_CID"].ToString() != "")
                    {

                        String[] _Cidade = ExportarCidadeParaMsAccess(Convert.ToInt32(Condominiorow["CON_CID"]));

                        _strInsertCommand += " CIDADE, ";
                        _strValuesCommand += " @CIDADE, ";

                        _strUpdateCommand += " CIDADE = @CIDADE, ";

                        if (_Cidade[0] != "")
                            _oleCommand.Parameters.AddWithValue("@CIDADE", _Cidade[0]);
                        else
                            _oleCommand.Parameters.AddWithValue("@CIDADE", "  ");

                        //_oleCommand.Parameters.AddWithValue("@CIDADE", _Cidade[0]);

                        _strInsertCommand += " ESTADO, ";
                        _strValuesCommand += " @ESTADO, ";

                        _strUpdateCommand += " ESTADO = @ESTADO, ";

                        _oleCommand.Parameters.AddWithValue("@ESTADO", _Cidade[1]);
                    }

                    if (Condominiorow["CONBAIRRO"].ToString() != "")
                    {
                        _strInsertCommand += " BAIRRO, ";
                        _strValuesCommand += " @BAIRRO, ";

                        _strUpdateCommand += " BAIRRO = @BAIRRO, ";

                        _oleCommand.Parameters.AddWithValue("@BAIRRO", Condominiorow["CONBAIRRO"].ToString().ToUpper());
                    }

                    

                    if (Condominiorow["CONCEP"].ToString() != "")
                    {
                        _strInsertCommand += " CEP, ";
                        _strValuesCommand += " @CEP, ";

                        _strUpdateCommand += " CEP = @CEP, ";

                        _oleCommand.Parameters.AddWithValue("@CEP", Condominiorow["CONCEP"].ToString().ToUpper());
                    }

                    if (Condominiorow["CONOBSERVACAO"].ToString() != "")
                    {
                        _strInsertCommand += " [OBSERVA��ES], ";
                        _strValuesCommand += " @OBS, ";

                        _strUpdateCommand += " [OBSERVA��ES] = @OBS, ";

                        _oleCommand.Parameters.AddWithValue("@OBS", Condominiorow["CONOBSERVACAO"].ToString().ToUpper());
                    }

                    if (Condominiorow["CONCNPJ"].ToString() != "")
                    {
                        _strInsertCommand += " CNPJ, ";
                        _strValuesCommand += " @CNPJ, ";

                        _strUpdateCommand += " CNPJ = @CNPJ, ";

                        _oleCommand.Parameters.AddWithValue("@CNPJ", Condominiorow["CONCNPJ"].ToString());
                    }

                    if (Condominiorow["CONPROCURACAO"].ToString() != "")
                    {
                        _strInsertCommand += " [PROCURA��O], ";
                        _strValuesCommand += " @PROCURACAO, ";

                        _strUpdateCommand += " [PROCURA��O] = @PROCURACAO, ";

                        _oleCommand.Parameters.AddWithValue("@PROCURACAO", Convert.ToBoolean(Condominiorow["CONPROCURACAO"]));
                    }

                    if (Condominiorow["CONFRACAOIDEAL"].ToString() != "")
                    {
                        _strInsertCommand += " [UTILIZA FRA��O], ";
                        _strValuesCommand += " @FRACAOIDEAL, ";

                        _strUpdateCommand += " [UTILIZA FRA��O] = @FRACAOIDEAL, ";

                        _oleCommand.Parameters.AddWithValue("@FRACAOIDEAL", Convert.ToBoolean(Condominiorow["CONFRACAOIDEAL"]));
                    }

                /*
                    if (Condominiorow["CONRATEIOSINDICO"].ToString() != "")
                    {
                        _strInsertCommand += " [S�NDICO RATEIO], ";
                        _strValuesCommand += " @SINDICORATEIO, ";

                        _strUpdateCommand += " [S�NDICO RATEIO] = @SINDICORATEIO, ";

                        _oleCommand.Parameters.AddWithValue("@SINDICORATEIO", Convert.ToDecimal(Condominiorow["CONRATEIOSINDICO"]));
                    }

                    if (Condominiorow["CONRATEIOSUBSINDICO"].ToString() != "")
                    {
                        _strInsertCommand += " [SUB-S�NDICO RATEIO], ";
                        _strValuesCommand += " @SUBSINDICORATEIO, ";

                        _strUpdateCommand += " [SUB-S�NDICO RATEIO] = @SUBSINDICORATEIO, ";

                        _oleCommand.Parameters.AddWithValue("@SUBSINDICORATEIO", Convert.ToDecimal(Condominiorow["CONRATEIOSUBSINDICO"]));
                    }

                    if (Condominiorow["CONCONDOMINIOSINDICO"].ToString() != "")
                    {
                        _strInsertCommand += " [S�NDICO CONDOMINIO], ";
                        _strValuesCommand += " @SINDICOCONDOMINIO, ";

                        _strUpdateCommand += " [S�NDICO CONDOMINIO] = @SINDICOCONDOMINIO, ";

                        _oleCommand.Parameters.AddWithValue("@SINDICOCONDOMINIO", Convert.ToDecimal(Condominiorow["CONCONDOMINIOSINDICO"]));
                    }

                    if (Condominiorow["CONCONDOMINIOSUBSINDICO"].ToString() != "")
                    {
                        _strInsertCommand += " [SUB-S�NDICO CONDOMINIO], ";
                        _strValuesCommand += " @SUBSINDICOCONDOMINIO, ";

                        _strUpdateCommand += " [SUB-S�NDICO CONDOMINIO] = @SUBSINDICOCONDOMINIO, ";

                        _oleCommand.Parameters.AddWithValue("@SUBSINDICOCONDOMINIO", Convert.ToDecimal(Condominiorow["CONCONDOMINIOSUBSINDICO"]));
                    }

                    if (Condominiorow["CONSINDICOPAGAFR"].ToString() != "")
                    {
                        _strInsertCommand += " [S�NDICO PAGA FR], ";
                        _strValuesCommand += " @SINDICOFR, ";

                        _strUpdateCommand += " [S�NDICO PAGA FR] = @SINDICOFR, ";

                        _oleCommand.Parameters.AddWithValue("@SINDICOFR", Convert.ToBoolean(Condominiorow["CONSINDICOPAGAFR"]));
                    }
                

                    if (Condominiorow["CONINQUILINOPAGAFR"].ToString() != "")
                    {
                        _strInsertCommand += " [INQUILINO PAGA FR], ";
                        _strValuesCommand += " @INQUILINOFR, ";

                        _strUpdateCommand += " [INQUILINO PAGA FR] = @INQUILINOFR, ";

                        _oleCommand.Parameters.AddWithValue("@INQUILINOFR", Convert.ToBoolean(Condominiorow["CONINQUILINOPAGAFR"]));
                    }*/

                    if (Condominiorow["CON_BCO"].ToString() != "")
                    {
                        _strInsertCommand += " NUMBANCO, ";
                        _strValuesCommand += " @NUMBANCO, ";

                        _strUpdateCommand += " NUMBANCO = @NUMBANCO, ";

                        _oleCommand.Parameters.AddWithValue("@NUMBANCO", Convert.ToInt32(Condominiorow["CON_BCO"]));
                    }

                    if (Condominiorow["CONAGENCIA"].ToString() != "")
                    {
                        _strInsertCommand += " [AG�NCIA], ";
                        _strValuesCommand += " @AGENCIA, ";

                        _strUpdateCommand += " [AG�NCIA] = @AGENCIA, ";

                        _oleCommand.Parameters.AddWithValue("@AGENCIA", Condominiorow["CONAGENCIA"].ToString());
                    }

                    if (Condominiorow["CONDIGITOAGENCIA"].ToString() != "")
                    {
                        _strInsertCommand += " [DIGITO AG�NCIA], ";
                        _strValuesCommand += " @DIGITOAGENCIA, ";

                        _strUpdateCommand += " [DIGITO AG�NCIA] = @DIGITOAGENCIA, ";

                        _oleCommand.Parameters.AddWithValue("@DIGITOAGENCIA", Condominiorow["CONDIGITOAGENCIA"].ToString());
                    }

                    if (Condominiorow["CONCONTA"].ToString() != "")
                    {
                        String _CC = Condominiorow["CONCONTA"].ToString();

                        if (Condominiorow["CONDIGITOCONTA"].ToString() != "")
                            _CC += Condominiorow["CONDIGITOCONTA"].ToString();

                        _strInsertCommand += " CC, ";
                        _strValuesCommand += " @CC, ";

                        _strUpdateCommand += " CC = @CC, ";

                        _oleCommand.Parameters.AddWithValue("@CC", _CC);
                    }

                    if (Condominiorow["COND0"].ToString() != "")
                    {
                        _strInsertCommand += " D0, ";
                        _strValuesCommand += " @D0, ";

                        _strUpdateCommand += " D0 = @D0, ";

                        _oleCommand.Parameters.AddWithValue("@D0", Convert.ToBoolean(Condominiorow["COND0"]));
                    }

                    if (Condominiorow["CONTIPOFR"].ToString() != "")
                    {
                        Double _ValorFR = (Condominiorow["CONVALORFR"] == DBNull.Value ? 0 : Convert.ToDouble(Condominiorow["CONVALORFR"]));

                        if (Condominiorow["CONTIPOFR"].ToString() == "F")
                        {
                            _strInsertCommand += " [UTILIZA VALOR FR], ";
                            _strValuesCommand += " @UTILIZAFR, ";

                            _strUpdateCommand += " [UTILIZA VALOR FR] = @UTILIZAFR, ";

                            _oleCommand.Parameters.AddWithValue("@UTILIZAFR", true);

                            _strInsertCommand += " [VALOR FUNDO RESERVA], ";
                            _strValuesCommand += " @VALORFR, ";

                            _strUpdateCommand += " [VALOR FUNDO RESERVA] = @VALORFR, ";

                            _oleCommand.Parameters.AddWithValue("@VALORFR", _ValorFR);
                        }
                        else
                        {
                            _strInsertCommand += " [UTILIZA VALOR FR], ";
                            _strValuesCommand += " @UTILIZAFR, ";

                            _strUpdateCommand += " [UTILIZA VALOR FR] = @UTILIZAFR, ";

                            _oleCommand.Parameters.AddWithValue("@UTILIZAFR", false);

                            _strInsertCommand += " [FUNDO DE RESERVA], ";
                            _strValuesCommand += " @VALORFR, ";

                            _strUpdateCommand += "  [FUNDO DE RESERVA] = @VALORFR, ";

                            _oleCommand.Parameters.AddWithValue("@VALORFR", _ValorFR);
                        }
                    }

                    if (Condominiorow["CONTIPOMULTA"].ToString() != "")
                    {
                        _strInsertCommand += " [TIPO DE MULTA], ";
                        _strValuesCommand += " @TIPOMULTA, ";

                        _strUpdateCommand += " [TIPO DE MULTA] = @TIPOMULTA, ";

                        _oleCommand.Parameters.AddWithValue("@TIPOMULTA", Condominiorow["CONTIPOMULTA"].ToString());
                    }

                    if (Condominiorow["CONVALORMULTA"].ToString() != "")
                    {
                        _strInsertCommand += " MULTA, ";
                        _strValuesCommand += " @VRMULTA, ";

                        _strUpdateCommand += " MULTA = @VRMULTA, ";

                        _oleCommand.Parameters.AddWithValue("@VRMULTA", Convert.ToDouble(Condominiorow["CONVALORMULTA"]));
                    }

                    if (Condominiorow["CONVALORCONDOMINIO"].ToString() != "")
                    {
                        _strInsertCommand += " [VALOR DE CONDOMINIO], ";
                        _strValuesCommand += " @VALORCON, ";

                        _strUpdateCommand += " [VALOR DE CONDOMINIO] = @VALORCON, ";

                        _oleCommand.Parameters.AddWithValue("@VALORCON", Convert.ToDouble(Condominiorow["CONVALORCONDOMINIO"]));
                    }

                    if (Condominiorow["CONVALORGAS"].ToString() != "")
                    {
                        _strInsertCommand += " [VALOR G�S], ";
                        _strValuesCommand += " @VALORGAS, ";

                        _strUpdateCommand += " [VALOR G�S] = @VALORGAS, ";

                        _oleCommand.Parameters.AddWithValue("@VALORGAS", Convert.ToDouble(Condominiorow["CONVALORGAS"]));
                    }

                    if (Condominiorow["CONDIAVENCIMENTO"].ToString() != "")
                    {
                        _strInsertCommand += " [DIA CONDOM�NIO], ";
                        _strValuesCommand += " @DIACONDOMINIO, ";

                        _strUpdateCommand += " [DIA CONDOM�NIO] = @DIACONDOMINIO, ";

                        _oleCommand.Parameters.AddWithValue("@DIACONDOMINIO", Convert.ToInt32(Condominiorow["CONDIAVENCIMENTO"]));
                    }

                    if (Condominiorow["CONCARTACOBRANCA"].ToString() != "")
                    {
                        _strInsertCommand += " PERIODO, ";
                        _strValuesCommand += " @PERIODO, ";

                        _strUpdateCommand += " PERIODO = @PERIODO, ";

                        _oleCommand.Parameters.AddWithValue("@PERIODO", Convert.ToInt32(Condominiorow["CONCARTACOBRANCA"]));
                    }

                    if (Condominiorow["CONADVOGADO1_USU"].ToString() != "")
                    {
                        _strInsertCommand += " ADVOGADO1, ";
                        _strValuesCommand += " @ADVOGADO1, ";

                        _strUpdateCommand += " ADVOGADO1 = @ADVOGADO1, ";

                        _oleCommand.Parameters.AddWithValue("@ADVOGADO1", ExportarUsuarioParaMsAccess(Convert.ToInt32(Condominiorow["CONADVOGADO1_USU"])));
                    }

                    if (Condominiorow["CONADVOGADO2_USU"].ToString() != "")
                    {
                        _strInsertCommand += " ADVOGADO2, ";
                        _strValuesCommand += " @ADVOGADO2, ";

                        _strUpdateCommand += " ADVOGADO2 = @ADVOGADO2, ";

                        _oleCommand.Parameters.AddWithValue("@ADVOGADO2", ExportarUsuarioParaMsAccess(Convert.ToInt32(Condominiorow["CONADVOGADO2_USU"])));
                    }

                    

                    
                
                    if (Condominiorow["CONUTILIZAPREVISAO"].ToString() != "")
                    {
                        _strInsertCommand += " [UTILIZA PREVIS�O], ";
                        _strValuesCommand += " @UTILIZAPREVISAO, ";

                        _strUpdateCommand += " [UTILIZA PREVIS�O] = @UTILIZAPREVISAO, ";

                        _oleCommand.Parameters.AddWithValue("@UTILIZAPREVISAO", Convert.ToBoolean(Condominiorow["CONUTILIZAPREVISAO"]));
                    }

                    if (Condominiorow["CONIMPRIMEPREVISAO"].ToString() != "")
                    {
                        _strInsertCommand += " [RODA PREVIS�O], ";
                        _strValuesCommand += " @RODAPREVISAO, ";

                        _strUpdateCommand += " [RODA PREVIS�O] = @RODAPREVISAO, ";

                        _oleCommand.Parameters.AddWithValue("@RODAPREVISAO", Convert.ToBoolean(Condominiorow["CONIMPRIMEPREVISAO"]));
                    }

                    if (Condominiorow["CONCOMPETENCIAVENCIDA"].ToString() != "")
                    {
                        _strInsertCommand += " [COMPET�NCIA NO M�S], ";
                        _strValuesCommand += " @COMPVENCIDA, ";

                        _strUpdateCommand += " [COMPET�NCIA NO M�S] = @COMPVENCIDA, ";

                        if (Convert.ToBoolean(Condominiorow["CONCOMPETENCIAVENCIDA"]) == true)
                            _oleCommand.Parameters.AddWithValue("@COMPVENCIDA", false);
                        else
                            _oleCommand.Parameters.AddWithValue("@COMPVENCIDA", true);
                    }

                    if (Condominiorow["CONIMPRIMEBALANCETEBOLETO"].ToString() != "")
                    {
                        _strInsertCommand += " [BALANCETE BOLETO], ";
                        _strValuesCommand += " @BALBOLETO, ";

                        _strUpdateCommand += " [BALANCETE BOLETO] = @BALBOLETO, ";

                        _oleCommand.Parameters.AddWithValue("@BALBOLETO", Convert.ToBoolean(Condominiorow["CONIMPRIMEBALANCETEBOLETO"]));
                    }

                    if (Condominiorow["CONEXPORTAINTERNET"].ToString() != "")
                    {
                        _strInsertCommand += " INTERNET , ";
                        _strValuesCommand += " @INTERNET, ";

                        _strUpdateCommand += " INTERNET = @INTERNET, ";

                        _oleCommand.Parameters.AddWithValue("@INTERNET", Convert.ToBoolean(Condominiorow["CONEXPORTAINTERNET"]));
                    }

                    if (Condominiorow["CONPUBLICABALANCETEINTERNET"].ToString() != "")
                    {
                        _strInsertCommand += " [LIBERA BALANCETE], ";
                        _strValuesCommand += " @BALANCETEINTERNET, ";

                        _strUpdateCommand += " [LIBERA BALANCETE] = @BALANCETEINTERNET, ";

                        _oleCommand.Parameters.AddWithValue("@BALANCETEINTERNET", Convert.ToBoolean(Condominiorow["CONPUBLICABALANCETEINTERNET"]));
                    }
                
                    if (Condominiorow["CONOBSERVACAOINTERNET"].ToString() != "")
                    {
                        _strInsertCommand += " [OBS INTERNET], ";
                        _strValuesCommand += " @OBSINTERNET, ";

                        _strUpdateCommand += " [OBS INTERNET] = @OBSINTERNET, ";

                        _oleCommand.Parameters.AddWithValue("@OBSINTERNET", Condominiorow["CONOBSERVACAOINTERNET"].ToString());
                    }
                
                    if (Condominiorow["CONDATAINCLUSAO"].ToString() != "")
                    {
                        _strInsertCommand += " [DATA CLIENTE], ";
                        _strValuesCommand += " @DATACLIENTE, ";

                        _strUpdateCommand += " [DATA CLIENTE] = @DATACLIENTE, ";

                        _oleCommand.Parameters.AddWithValue("@DATACLIENTE", Convert.ToDateTime(Condominiorow["CONDATAINCLUSAO"]).Date);
                    }

                    
                 
                    if (_IsUpdate)
                    {
                        _oleCommand.CommandText = "Update Condominios Set " + _strUpdateCommand.Substring(0, (_strUpdateCommand.Length - 2)) + " Where CODCON = @CODCON_OLD";
                        _oleCommand.Parameters.AddWithValue("@CODCON_OLD", Condominiorow.CONCodigo);
                    }
                    else
                    {
                        
                        _strInsertCommand += " CODCON, ";
                        _strValuesCommand += " @CODCON, ";
                        _oleCommand.Parameters.AddWithValue("@CODCON", Condominiorow.CONCodigo);

                        // Requerido pelo MS Access - NAO EXISTE NO SQL
                        _strInsertCommand += " BALANCETE, ";
                        _strValuesCommand += " @BALANCETE, ";
                        _oleCommand.Parameters.AddWithValue("@BALANCETE", 1);

                        // Requerido pelo MS Access - NAO EXISTE NO SQL
                        _strInsertCommand += " [BALANCETE CONSELHO], ";
                        _strValuesCommand += " @BALANCETECONSELHO, ";
                        _oleCommand.Parameters.AddWithValue("@BALANCETECONSELHO", 1);

                        _strInsertCommand = "Insert Into Condominios (" + _strInsertCommand.Substring(0, (_strInsertCommand.Length - 2)) + " ) ";
                        _strValuesCommand = "                 Values (" + _strValuesCommand.Substring(0, (_strValuesCommand.Length - 2)) + " ) ";

                        _oleCommand.CommandText = _strInsertCommand + _strValuesCommand;
                    }

                    _connACCESS.Open();
                    _oleCommand.ExecuteNonQuery();
                    _connACCESS.Close();

                    
                    
                
            }
            catch (Exception Erro)
            {                                
                _connACCESS.Close();
                MessageBox.Show("Ocorreu um erro ao tentar EXPORTAR o condom�nio " + Condominiorow.CONCodigo + ".\r\n\r\nErro: " + Erro.Message, "Exportar Condom�nio", MessageBoxButtons.OK, MessageBoxIcon.Error);
                String Paramentros = "";
                foreach (OleDbParameter Par in _oleCommand.Parameters)
                    Paramentros += Par.ParameterName.ToString() + " = " + Par.Value.ToString() + "\r\n";
                MessageBox.Show(Paramentros);
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="CODCON"></param>
        public void ExportarApartamentosParaMsAccess(String CODCON)
        {
            SqlConnection _connSQL = new SqlConnection(VirDB.Bancovirtual.BancoVirtual.StringConeFinal(VirDB.Bancovirtual.TiposDeBanco.SQL));
            OleDbConnection _connACCESS = new OleDbConnection(VirDB.Bancovirtual.BancoVirtual.StringConeFinal(VirDB.Bancovirtual.TiposDeBanco.Access));

            SqlDataReader _sqlDR = null;

            OleDbCommand _oleCommand = new OleDbCommand("", _connACCESS);

            try
            {
                SqlCommand _sqlCommand = new SqlCommand("Select Apartamentos.*, Blocos.BLOCodigo, Condominios.CONCodigo From Apartamentos Inner Join Blocos On Apartamentos.APT_BLO = Blocos.BLO Inner Join Condominios On Blocos.BLO_CON = Condominios.CON Where Condominios.CONCodigo = @CODCON", _connSQL);
                _sqlCommand.Parameters.AddWithValue("@CODCON", CODCON);

                _connSQL.Open();
                _sqlDR = _sqlCommand.ExecuteReader();

                if (_sqlDR.HasRows)
                {
                    Int32 _TotalUnid = 0;

                    while (_sqlDR.Read())
                    {
                        _TotalUnid++;

                        OleDbCommand _oleVerificaApto = new OleDbCommand("Select Apartamento From Apartamento Where CODCON = @CODCON and BLOCO = @BLOCO and APARTAMENTO = @APARTAMENTO", _connACCESS);
                        _oleVerificaApto.Parameters.AddWithValue("@CODCON", _sqlDR["CONCODIGO"].ToString());
                        _oleVerificaApto.Parameters.AddWithValue("@BLOCO", _sqlDR["BLOCODIGO"].ToString());
                        _oleVerificaApto.Parameters.AddWithValue("@APARTAMENTO", _sqlDR["APTNumero"].ToString());

                        _connACCESS.Open();
                        Boolean _IsUpdate = (_oleVerificaApto.ExecuteScalar() != null);
                        _connACCESS.Close();

                        _oleCommand.Parameters.Clear();
                        
                        if (_IsUpdate) //Apto existe... Atualizar
                        {
                            _oleCommand.CommandText = "Update Apartamento Set [Fra��o Ideal] = @FracaoIdeal, [Dia Condom�nio] = @DiaCondominio, [Dia Diferente] = @DiaDiferente Where CODCON = @CODCON and BLOCO = @BLOCO and APARTAMENTO = @APARTAMENTO";

                            _oleCommand.Parameters.AddWithValue("@FracaoIdeal", (_sqlDR["APTFracaoIdeal"].ToString() != "" ? Convert.ToDouble(_sqlDR["APTFracaoIdeal"]) : 0));
                            _oleCommand.Parameters.AddWithValue("@DiaCondominio", (_sqlDR["APTDIACondominio"].ToString() != "" ? Convert.ToInt32(_sqlDR["APTDIACondominio"]) : 1));
                            _oleCommand.Parameters.AddWithValue("@CODCON", _sqlDR["CONCODIGO"].ToString());
                            _oleCommand.Parameters.AddWithValue("@BLOCO", _sqlDR["BLOCODIGO"].ToString());
                            _oleCommand.Parameters.AddWithValue("@APARTAMENTO", _sqlDR["APTNumero"].ToString());
                            _oleCommand.Parameters.AddWithValue("@DiaDiferente", (_sqlDR["APTDIADIFERENTE"].ToString() != "" ? Convert.ToBoolean(_sqlDR["APTDIADIFERENTE"]) : false));
                        }
                        else
                        {
                            _oleCommand.CommandText = "Insert Into Apartamento (Codcon, Bloco, Apartamento, [Fra��o Ideal], [Dia Condom�nio], [Dia Diferente]) Values (@Codcon, @Bloco, @Apartamento, @FracaoIdeal, @DiaCondominio, @DiaDiferente)";

                            _oleCommand.Parameters.AddWithValue("@CODCON", _sqlDR["CONCODIGO"].ToString());
                            _oleCommand.Parameters.AddWithValue("@BLOCO", _sqlDR["BLOCODIGO"].ToString());
                            _oleCommand.Parameters.AddWithValue("@APARTAMENTO", _sqlDR["APTNumero"].ToString());
                            _oleCommand.Parameters.AddWithValue("@FracaoIdeal", (_sqlDR["APTFracaoIdeal"].ToString() != "" ? Convert.ToDouble(_sqlDR["APTFracaoIdeal"]) : 0));
                            _oleCommand.Parameters.AddWithValue("@DiaCondominio", (_sqlDR["APTDIACondominio"].ToString() != "" ? Convert.ToInt32(_sqlDR["APTDIACondominio"]) : 1));
                            _oleCommand.Parameters.AddWithValue("@DiaDiferente", (_sqlDR["APTDIADIFERENTE"].ToString() != "" ? Convert.ToBoolean(_sqlDR["APTDIADIFERENTE"]) : false));
                        }

                        _connACCESS.Open();
                        _oleCommand.ExecuteNonQuery();
                        _connACCESS.Close();

                    }

                    _sqlDR.Close();
                    _connSQL.Close();

                    OleDbCommand _oleTotalUnidade = new OleDbCommand("Update Condominios Set NUM_UNID = @NUM_UNID Where CODCON = @CODCON", _connACCESS);
                    _oleTotalUnidade.Parameters.AddWithValue("@NUM_UNID", _TotalUnid);
                    _oleTotalUnidade.Parameters.AddWithValue("@CODCON", CODCON);

                    _connACCESS.Open();
                    _oleTotalUnidade.ExecuteNonQuery();
                    _connACCESS.Close();
                }
            }
            catch(Exception Erro)
            {
                String _Str = "";

                foreach (OleDbParameter _Parametro in _oleCommand.Parameters)
                    _Str += _Parametro.ToString() + " - " + _Parametro.Value.ToString() + " - " + _Parametro.Value.ToString().Length.ToString() + "\r\n";

                MessageBox.Show("Ocorreu um erro ao exportar BLOCO/APARTAMENTO do condom�nio " + CODCON + ".\r\n\r\nErro: " + Erro.Message + "\r\n\r\n" + _Str, "Exportar BLOCO/APARTAMENTO");
                
                _sqlDR.Close();
                _connSQL.Close();
                _connACCESS.Close();
            }
        }

        
        /// <summary>
        /// 
        /// </summary>
        /// <param name="CODCON"></param>
        /// <param name="BLOCO"></param>
        /// <param name="Apartamentorow"></param>
        public void ExportarApartamentosParaMsAccessRow(String CODCON,string BLOCO,Condominios.dCondominiosCampos.APARTAMENTOSRow Apartamentorow)
        {
            OleDbConnection _connACCESS = new OleDbConnection(VirDB.Bancovirtual.BancoVirtual.StringConeFinal(VirDB.Bancovirtual.TiposDeBanco.Access));           
            OleDbCommand _oleCommand = new OleDbCommand("", _connACCESS);
            try
            {                                                                            
                        OleDbCommand _oleVerificaApto = new OleDbCommand("Select Apartamento From Apartamento Where CODCON = @CODCON and BLOCO = @BLOCO and APARTAMENTO = @APARTAMENTO", _connACCESS);
                        _oleVerificaApto.Parameters.AddWithValue("@CODCON", CODCON);
                        _oleVerificaApto.Parameters.AddWithValue("@BLOCO", BLOCO);
                        _oleVerificaApto.Parameters.AddWithValue("@APARTAMENTO", Apartamentorow["APTNumero"].ToString());

                        _connACCESS.Open();
                        Boolean _IsUpdate = (_oleVerificaApto.ExecuteScalar() != null);
                        _connACCESS.Close();
                        _oleCommand.Parameters.Clear();

                        if (_IsUpdate) //Apto existe... Atualizar
                        {
                            _oleCommand.CommandText = "Update Apartamento Set [Fra��o Ideal] = @FracaoIdeal, [Dia Condom�nio] = @DiaCondominio, [Dia Diferente] = @DiaDiferente Where CODCON = @CODCON and BLOCO = @BLOCO and APARTAMENTO = @APARTAMENTO";

                            _oleCommand.Parameters.AddWithValue("@FracaoIdeal", (Apartamentorow["APTFracaoIdeal"].ToString() != "" ? Convert.ToDouble(Apartamentorow["APTFracaoIdeal"]) : 0));
                            _oleCommand.Parameters.AddWithValue("@DiaCondominio", (Apartamentorow["APTDIACondominio"].ToString() != "" ? Convert.ToInt32(Apartamentorow["APTDIACondominio"]) : 1));
                            _oleCommand.Parameters.AddWithValue("@DiaDiferente", (Apartamentorow["APTDIADIFERENTE"].ToString() != "" ? Convert.ToBoolean(Apartamentorow["APTDIADIFERENTE"]) : false));
                            _oleCommand.Parameters.AddWithValue("@CODCON", CODCON);
                            _oleCommand.Parameters.AddWithValue("@BLOCO", BLOCO);
                            _oleCommand.Parameters.AddWithValue("@APARTAMENTO", Apartamentorow["APTNumero"].ToString());
                            
                        }
                        else
                        {
                            _oleCommand.CommandText = "Insert Into Apartamento (Codcon, Bloco, Apartamento, [Fra��o Ideal], [Dia Condom�nio], [Dia Diferente]) Values (@Codcon, @Bloco, @Apartamento, @FracaoIdeal, @DiaCondominio, @DiaDiferente)";

                            _oleCommand.Parameters.AddWithValue("@CODCON", CODCON);
                            _oleCommand.Parameters.AddWithValue("@BLOCO", BLOCO);
                            _oleCommand.Parameters.AddWithValue("@APARTAMENTO", Apartamentorow["APTNumero"].ToString());
                            _oleCommand.Parameters.AddWithValue("@FracaoIdeal", (Apartamentorow["APTFracaoIdeal"].ToString() != "" ? Convert.ToDouble(Apartamentorow["APTFracaoIdeal"]) : 0));
                            _oleCommand.Parameters.AddWithValue("@DiaCondominio", (Apartamentorow["APTDIACondominio"].ToString() != "" ? Convert.ToInt32(Apartamentorow["APTDIACondominio"]) : 1));
                            _oleCommand.Parameters.AddWithValue("@DiaDiferente", (Apartamentorow["APTDIADIFERENTE"].ToString() != "" ? Convert.ToBoolean(Apartamentorow["APTDIADIFERENTE"]) : false));
                        }

                        _connACCESS.Open();
                        int afetados = _oleCommand.ExecuteNonQuery();
                        if (afetados != 1)
                            MessageBox.Show("Apartamento n�o atualizado no access");
                        _connACCESS.Close();

                    

                    

                
            }
            catch (Exception Erro)
            {
                String _Str = "";
                foreach (OleDbParameter _Parametro in _oleCommand.Parameters)
                    _Str += _Parametro.ToString() + " - " + _Parametro.Value.ToString() + " - " + _Parametro.Value.ToString().Length.ToString() + "\r\n";
                MessageBox.Show("Ocorreu um erro ao exportar BLOCO/APARTAMENTO do condom�nio " + CODCON + ".\r\n\r\nErro: " + Erro.Message + "\r\n\r\n" + _Str, "Exportar BLOCO/APARTAMENTO");                
                _connACCESS.Close();
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="CODCON"></param>
        /// <param name="BLOCO"></param>
        /// <param name="Apartamentorow"></param>
        public static void ExportarApartamentosParaMsAccessRowAP(String CODCON, string BLOCO, dApartamentos.APARTAMENTOSRow Apartamentorow)
        {

            OleDbConnection _connACCESS = new OleDbConnection(VirDB.Bancovirtual.BancoVirtual.StringConeFinal(VirDB.Bancovirtual.TiposDeBanco.Access));



            OleDbCommand _oleCommand = new OleDbCommand("", _connACCESS);

            try
            {






                OleDbCommand _oleVerificaApto = new OleDbCommand("Select Apartamento From Apartamento Where CODCON = @CODCON and BLOCO = @BLOCO and APARTAMENTO = @APARTAMENTO", _connACCESS);
                _oleVerificaApto.Parameters.AddWithValue("@CODCON", CODCON);
                _oleVerificaApto.Parameters.AddWithValue("@BLOCO", BLOCO);
                _oleVerificaApto.Parameters.AddWithValue("@APARTAMENTO", Apartamentorow["APTNumero"].ToString());

                _connACCESS.Open();
                Boolean _IsUpdate = (_oleVerificaApto.ExecuteScalar() != null);
                _connACCESS.Close();

                _oleCommand.Parameters.Clear();

                if (_IsUpdate) //Apto existe... Atualizar
                {
                    _oleCommand.CommandText = "Update Apartamento Set [Fra��o Ideal] = @FracaoIdeal, [Dia Condom�nio] = @DiaCondominio, [Dia Diferente] = @DiaDiferente Where CODCON = @CODCON and BLOCO = @BLOCO and APARTAMENTO = @APARTAMENTO";

                    _oleCommand.Parameters.AddWithValue("@FracaoIdeal", (Apartamentorow["APTFracaoIdeal"].ToString() != "" ? Convert.ToDouble(Apartamentorow["APTFracaoIdeal"]) : 0));
                    _oleCommand.Parameters.AddWithValue("@DiaCondominio", (Apartamentorow["APTDIACondominio"].ToString() != "" ? Convert.ToInt32(Apartamentorow["APTDIACondominio"]) : 1));
                    _oleCommand.Parameters.AddWithValue("@DiaDiferente", (Apartamentorow["APTDIADIFERENTE"].ToString() != "" ? Convert.ToBoolean(Apartamentorow["APTDIADIFERENTE"]) : false));
                    _oleCommand.Parameters.AddWithValue("@CODCON", CODCON);
                    _oleCommand.Parameters.AddWithValue("@BLOCO", BLOCO);
                    _oleCommand.Parameters.AddWithValue("@APARTAMENTO", Apartamentorow["APTNumero"].ToString());

                }
                else
                {
                    _oleCommand.CommandText = "Insert Into Apartamento (Codcon, Bloco, Apartamento, [Fra��o Ideal], [Dia Condom�nio], [Dia Diferente]) Values (@Codcon, @Bloco, @Apartamento, @FracaoIdeal, @DiaCondominio, @DiaDiferente)";

                    _oleCommand.Parameters.AddWithValue("@CODCON", CODCON);
                    _oleCommand.Parameters.AddWithValue("@BLOCO", BLOCO);
                    _oleCommand.Parameters.AddWithValue("@APARTAMENTO", Apartamentorow["APTNumero"].ToString());
                    _oleCommand.Parameters.AddWithValue("@FracaoIdeal", (Apartamentorow["APTFracaoIdeal"].ToString() != "" ? Convert.ToDouble(Apartamentorow["APTFracaoIdeal"]) : 0));
                    _oleCommand.Parameters.AddWithValue("@DiaCondominio", (Apartamentorow["APTDIACondominio"].ToString() != "" ? Convert.ToInt32(Apartamentorow["APTDIACondominio"]) : 1));
                    _oleCommand.Parameters.AddWithValue("@DiaDiferente", (Apartamentorow["APTDIADIFERENTE"].ToString() != "" ? Convert.ToBoolean(Apartamentorow["APTDIADIFERENTE"]) : false));
                }

                _connACCESS.Open();
                int afetados = _oleCommand.ExecuteNonQuery();
                if (afetados != 1)
                    MessageBox.Show("Apartamento n�o atualizado no access");
                _connACCESS.Close();





                //                    OleDbCommand _oleTotalUnidade = new OleDbCommand("Update Condominios Set NUM_UNID = @NUM_UNID Where CODCON = @CODCON", _connACCESS);
                //                  _oleTotalUnidade.Parameters.AddWithValue("@NUM_UNID", _TotalUnid);
                //                _oleTotalUnidade.Parameters.AddWithValue("@CODCON", CODCON);

                //              _connACCESS.Open();
                //            _oleTotalUnidade.ExecuteNonQuery();
                //          _connACCESS.Close();

            }
            catch (Exception Erro)
            {
                String _Str = "";

                foreach (OleDbParameter _Parametro in _oleCommand.Parameters)
                    _Str += _Parametro.ToString() + " - " + _Parametro.Value.ToString() + " - " + _Parametro.Value.ToString().Length.ToString() + "\r\n";

                MessageBox.Show("Ocorreu um erro ao exportar BLOCO/APARTAMENTO do condom�nio " + CODCON + ".\r\n\r\nErro: " + Erro.Message + "\r\n\r\n" + _Str, "Exportar BLOCO/APARTAMENTO");


                _connACCESS.Close();
            }
        }

        private static String PrimeirasLetrasMaiusculas(String Entrada) {
            bool Primeira = true;
            String Saida = "";
            for (int i = 0; i < Entrada.Length; i++) {
                if (Primeira)
                    Saida += Entrada[i].ToString().ToUpper();
                else
                    Saida += Entrada[i].ToString().ToLower();
                Primeira = Char.IsWhiteSpace(Entrada,i);
            };
            return Saida;
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="CONPK"></param>
        public static void ExportaSindico(Int32 CONPK)
        {
            dAccess Dataset = new dAccess();
            dAccessTableAdapters.DadosSindicoTableAdapter Ta = new dAccessTableAdapters.DadosSindicoTableAdapter();
            Ta.TrocarStringDeConexao(VirDB.Bancovirtual.BancoVirtual.StringConeFinal(VirDB.Bancovirtual.TiposDeBanco.SQL));
            Ta.Fill(Dataset.DadosSindico, CONPK ,1);
            dAccess.DadosSindicoRow SindRow;
            String nometabela;
            if (Dataset.DadosSindico.Rows.Count == 1)
            {
                OleDbTransaction trans=null;
                OleDbConnection _connACCESS = null;
                try
                {
                    SindRow = (dAccess.DadosSindicoRow)Dataset.DadosSindico.Rows[0];
                    _connACCESS = new OleDbConnection(VirDB.Bancovirtual.BancoVirtual.StringConeFinal(VirDB.Bancovirtual.TiposDeBanco.Access));
                    _connACCESS.Open();
                    trans = _connACCESS.BeginTransaction();
                    OleDbCommand _sqlCommand = new OleDbCommand("Update propriet�rio set s�ndico = 0,[Sub-S�ndico]=0 Where codcon = @CODCON", _connACCESS, trans);
                    _sqlCommand.Parameters.AddWithValue("@CODCON", SindRow.CONCodigo);
                    _sqlCommand.ExecuteNonQuery();
                    _sqlCommand = new OleDbCommand("Update inquilino set s�ndico = 0,[Sub-S�ndico]=0 Where codcon = @CODCON", _connACCESS, trans);
                    _sqlCommand.Parameters.AddWithValue("@CODCON", SindRow.CONCodigo);
                    _sqlCommand.ExecuteNonQuery();

                    if (SindRow.CDRProprietario)
                        nometabela = "Propriet�rio";
                    else
                        nometabela = "Inquilino";
                    _sqlCommand = new OleDbCommand("Update " + nometabela + " set s�ndico = 1 Where codcon = @CODCON and bloco=@bloco and Apartamento = @Apartamento", _connACCESS, trans);
                    _sqlCommand.Parameters.AddWithValue("@CODCON", SindRow.CONCodigo);
                    _sqlCommand.Parameters.AddWithValue("@bloco", SindRow.BLOCodigo.ToString());
                    _sqlCommand.Parameters.AddWithValue("@apartamento", SindRow.APTNumero.ToString());
                    _sqlCommand.ExecuteNonQuery();
                    _sqlCommand = new OleDbCommand("Update Condominios set [Nome do s�ndico] = @NomeSindico Where codcon = @CODCON", _connACCESS, trans);
                    _sqlCommand.Parameters.AddWithValue("@NomeSindico", PrimeirasLetrasMaiusculas(SindRow.PESNome));
                    _sqlCommand.Parameters.AddWithValue("@CODCON", SindRow.CONCodigo);
                    _sqlCommand.ExecuteNonQuery();

                    Ta.Fill(Dataset.DadosSindico, CONPK, 2);
                    foreach (dAccess.DadosSindicoRow SubSindRow in Dataset.DadosSindico.Rows)
                    {
                        if (SubSindRow.CDRProprietario)
                            nometabela = "Propriet�rio";
                        else
                            nometabela = "Inquilino";
                        _sqlCommand = new OleDbCommand("Update " + nometabela + " set [Sub-S�ndico] = 1 Where codcon = @CODCON and bloco=@bloco and Apartamento = @Apartamento", _connACCESS, trans);
                        _sqlCommand.Parameters.AddWithValue("@CODCON", SubSindRow.CONCodigo);
                        _sqlCommand.Parameters.AddWithValue("@bloco", SubSindRow.BLOCodigo.ToString());
                        _sqlCommand.Parameters.AddWithValue("@apartamento", SubSindRow.APTNumero.ToString());
                        _sqlCommand.ExecuteNonQuery();
                    };
                    trans.Commit();
                }
                catch (Exception Erro)
                {
                    MessageBox.Show("Ocorreu um erro ao exportar o corpo diretivo condom�nio.\r\n\r\nErro: " + Erro.Message + "\r\n\r\n");
                    if (trans != null)
                        trans.Rollback();
                };
                _connACCESS.Close();
            }
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="CODCON"></param>
        public void ExportarPessoasParaMsAccess(String CODCON)
        {
            SqlConnection _connSQL = new SqlConnection(VirDB.Bancovirtual.BancoVirtual.StringConeFinal(VirDB.Bancovirtual.TiposDeBanco.SQL));
            OleDbConnection _connACCESS = new OleDbConnection(VirDB.Bancovirtual.BancoVirtual.StringConeFinal(VirDB.Bancovirtual.TiposDeBanco.Access));

            SqlDataReader _sqlDR = null;

            try
            {
                SqlCommand _sqlCommand = new SqlCommand("Select APTImobiliaria_FRN, APTProprietario_PES, APTInquilino_PES, Apartamentos.APTNumero, Blocos.BLOCodigo, Condominios.CONCodigo From Apartamentos Inner Join Blocos On Apartamentos.APT_BLO = Blocos.BLO Inner Join Condominios On Blocos.BLO_CON = Condominios.CON Where Condominios.CONCodigo = @CODCON Order by Condominios.CONCodigo, Blocos.BLOCodigo, Apartamentos.APTNumero,Apartamentos.APTFormaPagtoInquilino_FPG,Apartamentos.APTFormaPagtoProprietario_FPG", _connSQL);
                _sqlCommand.Parameters.AddWithValue("@CODCON", CODCON);

                _connSQL.Open();
                _sqlDR = _sqlCommand.ExecuteReader();

                if (_sqlDR.HasRows)
                {
                    while (_sqlDR.Read())
                    {
                        if (_sqlDR["APTProprietario_PES"].ToString() != "")
                        {
                            ExportarInquilinoProprietarioParaMsAccess(true, Convert.ToInt32(_sqlDR["APTProprietario_PES"]), _sqlDR["CONCodigo"].ToString(), _sqlDR["BLOCodigo"].ToString(), _sqlDR["APTNumero"].ToString(),  (Int32)_sqlDR["APTFormaPagtoProprietario_FPG"]);
                        }

                        if (_sqlDR["APTInquilino_PES"].ToString() != "")
                        {
                            ExportarInquilinoProprietarioParaMsAccess(false, Convert.ToInt32(_sqlDR["APTInquilino_PES"]), _sqlDR["CONCodigo"].ToString(), _sqlDR["BLOCodigo"].ToString(), _sqlDR["APTNumero"].ToString(), (Int32)_sqlDR["APTFormaPagtoInquilino_FPG"]);

                            if (_sqlDR["APTImobiliaria_FRN"].ToString() != "")
                                ExportarImobiliariaParaMsAccess(Convert.ToInt32(_sqlDR["APTImobiliaria_FRN"]), _sqlDR["CONCodigo"].ToString(), _sqlDR["BLOCodigo"].ToString(), _sqlDR["APTNumero"].ToString());
                        }
                        else {
                            ApagaInquilinoDoAccess(CODCON, _sqlDR["Blocos.BLOCodigo"].ToString(), _sqlDR["Apartamentos.APTNumero"].ToString());
                        }
                    }
                }
            }
            catch (Exception Erro)
            {
                MessageBox.Show("Ocorreu um erro ao tentar exportar as pessoas para o MS Access.\r\n\r\nErro: " + Erro.Message,
                                "Exportar Pessoas", MessageBoxButtons.OK, MessageBoxIcon.Error);

                _sqlDR.Close();
                _connSQL.Close();
                _connACCESS.Close();
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="CODCON"></param>
        /// <param name="BLOCO"></param>
        /// <param name="APARTAMENTO"></param>
        public static void ApagaInquilinoDoAccess(String CODCON, String BLOCO, String APARTAMENTO)
        {
            //SqlConnection _connSQL = new SqlConnection(VirDB.Bancovirtual.BancoVirtual.StringConeFinal());
            OleDbConnection _connACCESS = new OleDbConnection(VirDB.Bancovirtual.BancoVirtual.StringConeFinal(VirDB.Bancovirtual.TiposDeBanco.Access));

            //SqlDataReader _sqlDR = null;
            try{
                OleDbCommand _oleCommand = new OleDbCommand("Delete from inquilino where  CODCON = @CODCON And BLOCO = @BLOCO And APARTAMENTO = @APARTAMENTO ", _connACCESS);

                _oleCommand.Parameters.AddWithValue("@CODCON", CODCON);
                _oleCommand.Parameters.AddWithValue("@BLOCO", BLOCO);
                _oleCommand.Parameters.AddWithValue("@APARTAMENTO", APARTAMENTO);
                _connACCESS.Open();
                _oleCommand.ExecuteNonQuery();


                //Setando alugado = false na tab de apto
                OleDbCommand _oleAptoAlugado = new OleDbCommand("Update Apartamento set Alugado = @Alugado Where Codcon = @Codcon and Bloco = @Bloco and Apartamento = @Apartamento", _connACCESS);
                _oleAptoAlugado.Parameters.AddWithValue("@Alugado", false);
                _oleAptoAlugado.Parameters.AddWithValue("@Codcon", CODCON);
                _oleAptoAlugado.Parameters.AddWithValue("@Bloco", BLOCO);
                _oleAptoAlugado.Parameters.AddWithValue("@Apartamento", APARTAMENTO);

                _oleAptoAlugado.ExecuteNonQuery();
                _connACCESS.Close();
                                                                            
            }
            catch 
            {                                
                _connACCESS.Close();
            }
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="Proprietario"></param>
        /// <param name="PESPK"></param>
        /// <param name="CODCON"></param>
        /// <param name="BLOCO"></param>
        /// <param name="APARTAMENTO"></param>
        /// <param name="FormaDePag"></param>
        /// <returns></returns>
        public bool ExportarInquilinoProprietarioParaMsAccess(Boolean Proprietario, Int32 PESPK, String CODCON, String BLOCO, String APARTAMENTO,Int32 FormaDePag)
        {
            Boolean Correio = (FormaDePag <= 2);
            SqlConnection _connSQL = new SqlConnection(VirDB.Bancovirtual.BancoVirtual.StringConeFinal(VirDB.Bancovirtual.TiposDeBanco.SQL));
            OleDbConnection _connACCESS = new OleDbConnection(VirDB.Bancovirtual.BancoVirtual.StringConeFinal(VirDB.Bancovirtual.TiposDeBanco.Access));

            SqlDataReader _sqlDR = null;
            
            OleDbCommand _oleCommand = new OleDbCommand("", _connACCESS);

            String _Tabela = "";

            try
            {
                SqlCommand _sqlCommand = new SqlCommand("Select * From Pessoas Where PES = @PES", _connSQL);
                _sqlCommand.Parameters.AddWithValue("@PES", PESPK);

                _connSQL.Open();
                _sqlDR = _sqlCommand.ExecuteReader();

                if (_sqlDR.Read())
                {
                    if (Proprietario)
                        _Tabela = "[Propriet�rio]";
                    else
                        _Tabela = "Inquilino";

                    OleDbCommand _oleVerificaPessoa = new OleDbCommand("Select Nome From " + _Tabela + " Where CODCON = @CODCON And BLOCO = @BLOCO And APARTAMENTO = @APARTAMENTO ", _connACCESS);
                    _oleVerificaPessoa.Parameters.AddWithValue("@CODCON", CODCON);
                    _oleVerificaPessoa.Parameters.AddWithValue("@BLOCO", BLOCO);
                    _oleVerificaPessoa.Parameters.AddWithValue("@APARTAMENTO", APARTAMENTO);

                    _connACCESS.Open();
                    Boolean _IsUpdate = (_oleVerificaPessoa.ExecuteScalar() != null);
                    _connACCESS.Close();

                    String _strUpdateCommand = "";
                    String _strInsertCommand = "";
                    String _strValuesCommand = "";

                    if (_sqlDR["PESCPFCNPJ"].ToString() != "")
                    {
                        _strUpdateCommand += " CPF = @CPF, ";

                        _strInsertCommand += " CPF, ";
                        _strValuesCommand += " @CPF, ";

                        CPFCNPJ cnpj = new CPFCNPJ(_sqlDR["PESCPFCNPJ"].ToString());

                        switch (cnpj.Tipo)
                        {
                            case TipoCpfCnpj.CPF:
                                _oleCommand.Parameters.AddWithValue("@CPF", cnpj.ToString(false));
                                break;
                            case TipoCpfCnpj.CNPJ:
                                _oleCommand.Parameters.AddWithValue("@CPF", cnpj.ToString(false).Substring(0, 11));
                                break;
                            case TipoCpfCnpj.INVALIDO:
                            default:
                                string cpferrado = _sqlDR["PESCPFCNPJ"].ToString();
                                if (cpferrado.Length > 11)
                                    cpferrado = cpferrado.Substring(0,11);
                                _oleCommand.Parameters.AddWithValue("@CPF", cpferrado);
                                break;
                        }
                    }

                    if (_sqlDR["PESRGIE"].ToString() != "")
                    {
                        _strUpdateCommand += " RG = @RG, ";

                        _strInsertCommand += " RG, ";
                        _strValuesCommand += " @RG, ";

                        _oleCommand.Parameters.AddWithValue("@RG", _sqlDR["PESRGIE"].ToString());
                    }

                    if (_sqlDR["PESPIS"].ToString() != "")
                    {
                        _strUpdateCommand += " PIS = @PIS, ";

                        _strInsertCommand += " PIS, ";
                        _strValuesCommand += " @PIS, ";

                        _oleCommand.Parameters.AddWithValue("@PIS", _sqlDR["PESPIS"].ToString());
                    }

                    if (_sqlDR["PESNOME"].ToString() != "")
                    {
                        _strUpdateCommand += " NOME = @NOME, ";

                        _strInsertCommand += " NOME, ";
                        _strValuesCommand += " @NOME, ";

                        _oleCommand.Parameters.AddWithValue("@NOME", _sqlDR["PESNOME"].ToString());
                    }

                    if (_sqlDR["PESENDERECO"].ToString() != "")
                    {
                        _strUpdateCommand += " [END] = @ENDERECO, ";

                        _strInsertCommand += " [END], ";
                        _strValuesCommand += " @ENDERECO, ";

                        _oleCommand.Parameters.AddWithValue("@ENDERECO", _sqlDR["PESENDERECO"].ToString());
                    }

                    if (_sqlDR["PESBAIRRO"].ToString() != "")
                    {
                        _strUpdateCommand += " BAIRRO = @BAIRRO, ";

                        _strInsertCommand += " BAIRRO, ";
                        _strValuesCommand += " @BAIRRO, ";

                        _oleCommand.Parameters.AddWithValue("@BAIRRO", _sqlDR["PESBAIRRO"].ToString());
                    }

                    if (_sqlDR["PES_CID"].ToString() != "")
                    {
                        String[] _Cidade = ExportarCidadeParaMsAccess(Convert.ToInt32(_sqlDR["PES_CID"]));

                        _strUpdateCommand += " CIDADE = @CIDADE, ";

                        _strInsertCommand += " CIDADE, ";
                        _strValuesCommand += " @CIDADE, ";

                        if (_Cidade[0] != "")
                            _oleCommand.Parameters.AddWithValue("@CIDADE", _Cidade[0]);
                        else
                            _oleCommand.Parameters.AddWithValue("@CIDADE", "  ");


                        _strUpdateCommand += " ESTADO = @ESTADO, ";

                        _strInsertCommand += " ESTADO, ";
                        _strValuesCommand += " @ESTADO, ";

                        _oleCommand.Parameters.AddWithValue("@ESTADO", _Cidade[1]);
                    }

                    /*
                    if (_sqlDR["PESUF"].ToString() != "")
                    {
                        _strUpdateCommand += " ESTADO = @ESTADO, ";

                        _strInsertCommand += " ESTADO, ";
                        _strValuesCommand += " @ESTADO, ";

                        _oleCommand.Parameters.AddWithValue("@ESTADO", _sqlDR["PESUF"]);
                    }
                    */

                    if (_sqlDR["PESCEP"].ToString() != "")
                    {
                        _strUpdateCommand += " CEP = @CEP, ";

                        _strInsertCommand += " CEP, ";
                        _strValuesCommand += " @CEP, ";

                        _oleCommand.Parameters.AddWithValue("@CEP", _sqlDR["PESCEP"].ToString());
                    }

                    if (_sqlDR["PESFONE1"].ToString() != "")
                    {
                        _strUpdateCommand += " TELEFONE1 = @FONE1, ";

                        _strInsertCommand += " TELEFONE1, ";
                        _strValuesCommand += " @FONE1, ";

                        String _Fone1 = _sqlDR["PESFONE1"].ToString();

                        if (_Tabela == "Inquilino")
                        {
                            if (_Fone1.Length > 14)
                                _Fone1 = _Fone1.Substring(0, 13);
                        }

                        _oleCommand.Parameters.AddWithValue("@FONE1", _Fone1);
                    }

                    if (_sqlDR["PESFONE2"].ToString() != "")
                    {
                        _strUpdateCommand += " TELEFONE2 = @FONE2, ";

                        _strInsertCommand += " TELEFONE2, ";
                        _strValuesCommand += " @FONE2, ";

                        String _Fone2 = _sqlDR["PESFONE2"].ToString();

                        if (_Tabela == "Inquilino")
                        {
                            if (_Fone2.Length > 14)
                                _Fone2 = _Fone2.Substring(0, 13);
                        }

                        _oleCommand.Parameters.AddWithValue("@FONE2", _Fone2);
                    }

                    if (_sqlDR["PESFONE3"].ToString() != "")
                    {
                        _strUpdateCommand += " TELEFONE3 = @FONE3, ";

                        _strInsertCommand += " TELEFONE3, ";
                        _strValuesCommand += " @FONE3, ";

                        String _Fone3 = _sqlDR["PESFONE3"].ToString();

                        if (_Tabela == "Inquilino")
                        {
                            if (_Fone3.Length > 14)
                                _Fone3 = _Fone3.Substring(0, 13);
                        }

                        _oleCommand.Parameters.AddWithValue("@FONE3", _Fone3);
                    }

                    if (_sqlDR["PESDATANASCIMENTO"].ToString() != "")
                    {
                        _strUpdateCommand += " NASC = @NASC, ";

                        _strInsertCommand += " NASC, ";
                        _strValuesCommand += " @NASC, ";

                        _oleCommand.Parameters.AddWithValue("@NASC", Convert.ToDateTime(_sqlDR["PESDATANASCIMENTO"]));
                    }
                    
                    if (_sqlDR["PESEMAIL"].ToString() != "")
                    {
                        _strUpdateCommand += " EMAIL = @EMAIL, ";

                        _strInsertCommand += " EMAIL, ";
                        _strValuesCommand += " @EMAIL, ";

                        _oleCommand.Parameters.AddWithValue("@EMAIL", _sqlDR["PESEMAIL"]);
                    }
                    
                    _strUpdateCommand += " Correio = @Correio, ";
                    _strInsertCommand += " Correio, ";
                    _strValuesCommand += " @Correio, ";
                    _oleCommand.Parameters.AddWithValue("@Correio", Correio);
                    
                    /*
                    if (_sqlDR["PESUSUARIOINTERNET"].ToString() != "")
                    if (Usuario != "")
                    {
                        _strUpdateCommand += " [USUARIO INTERNET] = @USUINT, ";

                        _strInsertCommand += " [USUARIO INTERNET], ";
                        _strValuesCommand += " @USUINT, ";

                        _oleCommand.Parameters.AddWithValue("@USUINT", Usuario);
                    }

                    //if (_sqlDR["PESSENHAINTERNET"].ToString() != "")
                    if (Senha != "")
                    {
                        _strUpdateCommand += " [SENHA INTERNET] = @SENINT, ";

                        _strInsertCommand += " [SENHA INTERNET], ";
                        _strValuesCommand += " @SENINT, ";

                        _oleCommand.Parameters.AddWithValue("@SENINT", Senha);
                    }
                    */

                    if (_IsUpdate)
                        _oleCommand.CommandText = "Update " + _Tabela + " Set " + _strUpdateCommand.Substring(0, _strUpdateCommand.Length - 2) + " Where CODCON = @CODCON AND BLOCO = @BLOCO AND APARTAMENTO = @APARTAMENTO";
                    else
                        _oleCommand.CommandText = "Insert Into " + _Tabela + " ( " + _strInsertCommand.Substring(0, _strInsertCommand.Length - 2) + ", CODCON, BLOCO, APARTAMENTO ) Values ( " + _strValuesCommand.Substring(0, _strValuesCommand.Length - 2) + ", @CODCON, @BLOCO, @APARTAMENTO ) ";

                    _oleCommand.Parameters.AddWithValue("@CODCON", CODCON);
                    _oleCommand.Parameters.AddWithValue("@BLOCO", BLOCO);
                    _oleCommand.Parameters.AddWithValue("@APARTAMENTO", APARTAMENTO);

                    //MessageBox.Show(CODCON + " - " + BLOCO + " - " + APARTAMENTO + " - " + _sqlDR["PESNOME"].ToString());

                    _connACCESS.Open();
                    _oleCommand.ExecuteNonQuery();
                    _connACCESS.Close();

                    if (!Proprietario)
                    {
                        //Setando alugado = true na tab de apto
                        OleDbCommand _oleAptoAlugado = new OleDbCommand("Update Apartamento set Alugado = @Alugado Where Codcon = @Codcon and Bloco = @Bloco and Apartamento = @Apartamento", _connACCESS);
                        _oleAptoAlugado.Parameters.AddWithValue("@Alugado", true);
                        _oleAptoAlugado.Parameters.AddWithValue("@Codcon", CODCON);
                        _oleAptoAlugado.Parameters.AddWithValue("@Bloco", BLOCO);
                        _oleAptoAlugado.Parameters.AddWithValue("@Apartamento", APARTAMENTO);

                        _connACCESS.Open();
                        _oleAptoAlugado.ExecuteNonQuery();
                        _connACCESS.Close();
                    }

                    _sqlDR.Close();
                    _connSQL.Close();
                }
                return true;
            }
            catch (Exception Erro)
            {
                String _Str = "";

                foreach (OleDbParameter _Parametro in _oleCommand.Parameters)
                    _Str += _Parametro.ToString() + " - " + _Parametro.Value.ToString() + " - " + _Parametro.Value.ToString().Length.ToString() + "\r\n";

                erroReportado = "Ocorreu um erro ao tentar exportar as pessoas para o MS Access.\r\n\r\nErro: " + Erro.Message + "\r\n\r\n" + _oleCommand.CommandText + "\r\n\r\n" + _Str + "\r\n\r\n" + CODCON + " - " + BLOCO + " - " + APARTAMENTO + " - " + _sqlDR["PESNOME"].ToString() + " - " + _Tabela;

                if (Assistido)
                    MessageBox.Show(erroReportado, "Exportar Pessoas", MessageBoxButtons.OK, MessageBoxIcon.Error);
                else
                    erroReportado = "Erro em Exportar Pessoas:\r\n" + erroReportado;

                _sqlDR.Close();
                _connSQL.Close();
                _connACCESS.Close();
                return false;
            }
        }
        /// <summary>
        /// 
        /// </summary>
        public bool Assistido = true;
        /// <summary>
        /// 
        /// </summary>
        public string erroReportado = "";

        private void fExportToMsAccess_Load(object sender, EventArgs e)
        {
        }

        private void toolStripButton1_Click(object sender, EventArgs e)
        {
        }

        private void toolStripButton2_Click(object sender, EventArgs e)
        {
        }

        private void toolStripButton3_Click(object sender, EventArgs e)
        {
        }
    }
}