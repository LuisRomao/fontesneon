namespace Cadastros.Utilitarios.Cidades
{
    partial class fCorretorCidades
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(fCorretorCidades));
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.checkedListBox = new DevExpress.XtraEditors.CheckedListBoxControl();
            this.cIDADESBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.dCidades = new Cadastros.Utilitarios.Cidades.dCidades();
            this.label1 = new System.Windows.Forms.Label();
            this.lkpCidade = new DevExpress.XtraEditors.LookUpEdit();
            this.btnAgrupar = new DevExpress.XtraEditors.SimpleButton();
            this.progressBar = new DevExpress.XtraEditors.ProgressBarControl();
            this.cIDADESTableAdapter = new Cadastros.Utilitarios.Cidades.dCidadesTableAdapters.CIDADESTableAdapter();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkedListBox)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cIDADESBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dCidades)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lkpCidade.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.progressBar.Properties)).BeginInit();
            this.SuspendLayout();
            // 
            // pictureBox1
            // 
            this.pictureBox1.Dock = System.Windows.Forms.DockStyle.Left;
            this.pictureBox1.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox1.Image")));
            this.pictureBox1.Location = new System.Drawing.Point(0, 0);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(118, 318);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize;
            this.pictureBox1.TabIndex = 5;
            this.pictureBox1.TabStop = false;
            // 
            // checkedListBox
            // 
            this.checkedListBox.CheckOnClick = true;
            this.checkedListBox.DataSource = this.cIDADESBindingSource;
            this.checkedListBox.DisplayMember = "CIDNome";
            this.checkedListBox.Location = new System.Drawing.Point(127, 23);
            this.checkedListBox.MultiColumn = true;
            this.checkedListBox.Name = "checkedListBox";
            this.checkedListBox.Size = new System.Drawing.Size(355, 238);
            this.checkedListBox.TabIndex = 6;
            this.checkedListBox.ValueMember = "CID";
            // 
            // cIDADESBindingSource
            // 
            this.cIDADESBindingSource.DataMember = "CIDADES";
            this.cIDADESBindingSource.DataSource = this.dCidades;
            // 
            // dCidades
            // 
            this.dCidades.DataSetName = "dCidades";
            this.dCidades.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // label1
            // 
            this.label1.Location = new System.Drawing.Point(125, 6);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(358, 16);
            this.label1.TabIndex = 7;
            this.label1.Text = "Marque aqui as cidades que dever�o ser corrigidas.";
            // 
            // lkpCidade
            // 
            this.lkpCidade.Location = new System.Drawing.Point(127, 267);
            this.lkpCidade.Name = "lkpCidade";
            this.lkpCidade.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.lkpCidade.Properties.DataSource = this.cIDADESBindingSource;
            this.lkpCidade.Properties.DisplayMember = "CIDNome";
            this.lkpCidade.Properties.NullText = "[ Selecione uma cidade para o agrupamento ]";
            this.lkpCidade.Properties.ValueMember = "CID";
            this.lkpCidade.Size = new System.Drawing.Size(274, 20);
            this.lkpCidade.TabIndex = 8;
            // 
            // btnAgrupar
            // 
            this.btnAgrupar.Location = new System.Drawing.Point(408, 264);
            this.btnAgrupar.Name = "btnAgrupar";
            this.btnAgrupar.Size = new System.Drawing.Size(75, 26);
            this.btnAgrupar.TabIndex = 9;
            this.btnAgrupar.Text = "Agrupar";
            this.btnAgrupar.Click += new System.EventHandler(this.btnAgrupar_Click);
            // 
            // progressBar
            // 
            this.progressBar.Location = new System.Drawing.Point(127, 293);
            this.progressBar.Name = "progressBar";
            this.progressBar.Properties.PercentView = false;
            this.progressBar.Properties.ShowTitle = true;
            this.progressBar.Properties.Step = 1;
            this.progressBar.Size = new System.Drawing.Size(356, 19);
            this.progressBar.TabIndex = 10;
            // 
            // cIDADESTableAdapter
            // 
            this.cIDADESTableAdapter.ClearBeforeFill = true;
            // 
            // fCorretorCidades
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.WhiteSmoke;
            this.ClientSize = new System.Drawing.Size(494, 318);
            this.Controls.Add(this.progressBar);
            this.Controls.Add(this.btnAgrupar);
            this.Controls.Add(this.lkpCidade);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.checkedListBox);
            this.Controls.Add(this.pictureBox1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "fCorretorCidades";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Organizador de Cidades";
            this.Load += new System.EventHandler(this.fCorretorCidades_Load);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkedListBox)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cIDADESBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dCidades)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lkpCidade.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.progressBar.Properties)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.PictureBox pictureBox1;
        private DevExpress.XtraEditors.CheckedListBoxControl checkedListBox;
        private dCidades dCidades;
        private Cadastros.Utilitarios.Cidades.dCidadesTableAdapters.CIDADESTableAdapter cIDADESTableAdapter;
        private System.Windows.Forms.BindingSource cIDADESBindingSource;
        private System.Windows.Forms.Label label1;
        private DevExpress.XtraEditors.LookUpEdit lkpCidade;
        private DevExpress.XtraEditors.SimpleButton btnAgrupar;
        private DevExpress.XtraEditors.ProgressBarControl progressBar;
    }
}