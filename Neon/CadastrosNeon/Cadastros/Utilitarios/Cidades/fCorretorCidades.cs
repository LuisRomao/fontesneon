using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using System.Data.OleDb;
using System.Data.SqlClient;

namespace Cadastros.Utilitarios.Cidades
{
    /// <summary>
    /// 
    /// </summary>
    public partial class fCorretorCidades : Form
    {
        /// <summary>
        /// 
        /// </summary>
        public fCorretorCidades()
        {
            InitializeComponent();
        }

        private void fCorretorCidades_Load(object sender, EventArgs e)
        {
            // TODO: This line of code loads data into the 'dCidades.CIDADES' table. You can move, or remove it, as needed.
            VirDB.VirtualTableAdapter Vtableadapter = (VirDB.VirtualTableAdapter)this.cIDADESTableAdapter;
            Vtableadapter.Tipo = VirDB.Bancovirtual.TiposDeBanco.SQL;
            Vtableadapter.TrocarStringDeConexao();
            this.cIDADESTableAdapter.Fill(this.dCidades.CIDADES);
        }

        private void btnAgrupar_Click(object sender, EventArgs e)
        {
            this.Validate();

            try
            {
                if (lkpCidade.EditValue == null)
                {
                    MessageBox.Show("Selecione uma cidade destino para as cidades selecionadas.", "Cidade Destino", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    return;
                }

                btnAgrupar.Enabled = false;

                progressBar.Position = 0;
                progressBar.Properties.Maximum = checkedListBox.CheckedIndices.Count;

                OleDbConnection _connAccess = new OleDbConnection(VirDB.Bancovirtual.BancoVirtual.StringConeFinal(VirDB.Bancovirtual.TiposDeBanco .Access));
                OleDbCommand _oleCommand = new OleDbCommand("", _connAccess);

                SqlConnection _connSQL = new SqlConnection(VirDB.Bancovirtual.BancoVirtual.StringConeFinal(VirDB.Bancovirtual.TiposDeBanco.SQL));
                SqlCommand _sqlCommand = new SqlCommand("", _connSQL);

                Int32 _Index = 0;
                String _Caption = "";
                Int32 _Value = 0;

                for (_Index = 0; _Index < checkedListBox.CheckedIndices.Count; _Index++)
                {

                    progressBar.Position++;
                    progressBar.Update();
                    
                    _Caption = checkedListBox.GetItemText(checkedListBox.CheckedIndices[_Index]);
                    _Value = Convert.ToInt32(checkedListBox.GetItemValue(checkedListBox.CheckedIndices[_Index]));

                    if (_Value == Convert.ToInt32(lkpCidade.EditValue))
                        continue;


                    /*** MS Access ***/
                    //Condominio 
                    _oleCommand.CommandText = "Update Condominios Set CIDADE = @CIDADE_NEW Where CIDADE = @CIDADE_OLD";
                    _oleCommand.Parameters.Clear();
                    _oleCommand.Parameters.AddWithValue("@CIDADE_NEW", lkpCidade.Text);
                    _oleCommand.Parameters.AddWithValue("@CIDADE_OLD", _Caption);
                    _connAccess.Open();
                    _oleCommand.ExecuteNonQuery();
                    _connAccess.Close();

                    //Proprietarios
                    _oleCommand.CommandText = "Update [Proprietário] Set CIDADE = @CIDADE_NEW Where CIDADE = @CIDADE_OLD";
                    _oleCommand.Parameters.Clear();
                    _oleCommand.Parameters.AddWithValue("@CIDADE_NEW", lkpCidade.Text);
                    _oleCommand.Parameters.AddWithValue("@CIDADE_OLD", _Caption);
                    _connAccess.Open();
                    _oleCommand.ExecuteNonQuery();
                    _connAccess.Close();

                    //Inquilinos
                    _oleCommand.CommandText = "Update Inquilino Set CIDADE = @CIDADE_NEW Where CIDADE = @CIDADE_OLD";
                    _oleCommand.Parameters.Clear();
                    _oleCommand.Parameters.AddWithValue("@CIDADE_NEW", lkpCidade.Text);
                    _oleCommand.Parameters.AddWithValue("@CIDADE_OLD", _Caption);
                    _connAccess.Open();
                    _oleCommand.ExecuteNonQuery();
                    _connAccess.Close();

                    //Imobiliarias
                    _oleCommand.CommandText = "Update [Imobiliária] Set CIDADE = @CIDADE_NEW Where CIDADE = @CIDADE_OLD";
                    _oleCommand.Parameters.Clear();
                    _oleCommand.Parameters.AddWithValue("@CIDADE_NEW", lkpCidade.Text);
                    _oleCommand.Parameters.AddWithValue("@CIDADE_OLD", _Caption);
                    _connAccess.Open();
                    _oleCommand.ExecuteNonQuery();
                    _connAccess.Close();


                    /*** SQL ***/
                    //Condominio
                    _sqlCommand.CommandText = "Update Condominios Set CON_CID = @CIDADE_NEW Where CON_CID = @CIDADE_OLD";
                    _sqlCommand.Parameters.Clear();
                    _sqlCommand.Parameters.AddWithValue("@CIDADE_NEW", lkpCidade.EditValue);
                    _sqlCommand.Parameters.AddWithValue("@CIDADE_OLD", _Value);
                    _connSQL.Open();
                    _sqlCommand.ExecuteNonQuery();
                    _connSQL.Close();

                    //Pessoas
                    _sqlCommand.CommandText = "Update Pessoas Set PES_CID = @CIDADE_NEW Where PES_CID = @CIDADE_OLD";
                    _sqlCommand.Parameters.Clear();
                    _sqlCommand.Parameters.AddWithValue("@CIDADE_NEW", lkpCidade.EditValue);
                    _sqlCommand.Parameters.AddWithValue("@CIDADE_OLD", _Value);
                    _connSQL.Open();
                    _sqlCommand.ExecuteNonQuery();
                    _connSQL.Close();

                    //Fornecedores
                    _sqlCommand.CommandText = "Update Fornecedores Set FRN_CID = @CIDADE_NEW Where FRN_CID = @CIDADE_OLD";
                    _sqlCommand.Parameters.Clear();
                    _sqlCommand.Parameters.AddWithValue("@CIDADE_NEW", lkpCidade.EditValue);
                    _sqlCommand.Parameters.AddWithValue("@CIDADE_OLD", _Value);
                    _connSQL.Open();
                    _sqlCommand.ExecuteNonQuery();
                    _connSQL.Close();


                    //Excluindo Cidade da Lista
                    _sqlCommand.CommandText = "Delete From Cidades Where CID = @CIDADE_OLD";
                    _sqlCommand.Parameters.Clear();
                    _sqlCommand.Parameters.AddWithValue("@CIDADE_OLD", _Value);
                    _connSQL.Open();
                    _sqlCommand.ExecuteNonQuery();
                    _connSQL.Close();

                    Application.DoEvents();
                }

                //Refresh 
                dCidades.CIDADES.Clear();
                this.cIDADESTableAdapter.Fill(this.dCidades.CIDADES);
                checkedListBox.Refresh();

                btnAgrupar.Enabled = true;

                MessageBox.Show("Organizador de cidades foi finalizado com sucesso.", "Organizador de Cidades", MessageBoxButtons.OK, MessageBoxIcon.Information);

                progressBar.Position = 0;
                progressBar.Update();

            }

            catch(Exception Erro)
            {
                MessageBox.Show("Ocorreu um erro ao tentar organizar as cidades.\r\n\r\nErro: " + Erro.Message,
                                "Organizador de Cidades", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }
        }
    }
}