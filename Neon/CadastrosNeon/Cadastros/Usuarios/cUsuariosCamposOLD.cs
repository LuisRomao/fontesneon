using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace Cadastros.Usuarios
{
    public partial class cUsuariosCamposOLD : CompontesBasicos.ComponenteCamposGroupTabControl
    {
        private void InitData()
        {
            cIDADESTableAdapter.Fill(dUsuariosCampos.CIDADES);
        }
        
        public cUsuariosCamposOLD()
        {
            InitializeComponent();

            InitData();
        }

        protected override void ConfirmarRegistro_F()
        {
            uSUARIOSTableAdapter.Update(dUsuariosCampos);
        }
    }
}

