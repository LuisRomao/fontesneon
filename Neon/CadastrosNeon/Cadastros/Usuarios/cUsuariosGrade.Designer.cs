namespace Cadastros.Usuarios
{
    partial class cUsuariosGrade
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.colUSUNome = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colUSUEndereco = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colUSUBairro = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colCIDNome = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colUSUUf = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colUSUCep = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colUSUFone1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colUSUFone2 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colUSURamal = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colUSUEmail = new DevExpress.XtraGrid.Columns.GridColumn();
            this.dUsuariosGrade = new Cadastros.Usuarios.dUsuariosGrade();
            this.uSUARIOSTableAdapter = new Cadastros.Usuarios.dUsuariosGradeTableAdapters.USUARIOSTableAdapter();
            this.colPERNome = new DevExpress.XtraGrid.Columns.GridColumn();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl1)).BeginInit();
            this.groupControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.LayoutPesquisa_F)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.PopImprimir_F)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.BarManager_F)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.PopExportar_F)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.GridControl_F)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.GridView_F)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.BindingSource_F)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.StyleController_F)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dUsuariosGrade)).BeginInit();
            this.SuspendLayout();
            // 
            // groupControl1
            // 
            this.groupControl1.Size = new System.Drawing.Size(944, 84);
            this.ToolTipController_F.SetSuperTip(this.groupControl1, null);
            // 
            // LayoutPesquisa_F
            // 
            this.LayoutPesquisa_F.Size = new System.Drawing.Size(940, 84);
            // 
            // barNavegacao_F
            // 
            this.barNavegacao_F.OptionsBar.AllowQuickCustomization = false;
            this.barNavegacao_F.OptionsBar.DisableClose = true;
            this.barNavegacao_F.OptionsBar.DisableCustomization = true;
            this.barNavegacao_F.OptionsBar.DrawDragBorder = false;
            this.barNavegacao_F.OptionsBar.DrawSizeGrip = true;
            this.barNavegacao_F.OptionsBar.RotateWhenVertical = false;
            this.barNavegacao_F.OptionsBar.UseWholeRow = true;
            // 
            // BtnAlterar_F
            // 
            this.BtnAlterar_F.Enabled = false;
            // 
            // BtnExcluir_F
            // 
            this.BtnExcluir_F.Enabled = false;
            // 
            // barDataBase_F
            // 
            this.barDataBase_F.OptionsBar.AllowQuickCustomization = false;
            this.barDataBase_F.OptionsBar.DisableClose = true;
            this.barDataBase_F.OptionsBar.DisableCustomization = true;
            this.barDataBase_F.OptionsBar.DrawDragBorder = false;
            this.barDataBase_F.OptionsBar.UseWholeRow = true;
            // 
            // GridControl_F
            // 
            this.GridControl_F.EmbeddedNavigator.Buttons.Append.Visible = false;
            this.GridControl_F.EmbeddedNavigator.Buttons.CancelEdit.Visible = false;
            this.GridControl_F.EmbeddedNavigator.Buttons.Edit.Visible = false;
            this.GridControl_F.EmbeddedNavigator.Buttons.EndEdit.Visible = false;
            this.GridControl_F.EmbeddedNavigator.Buttons.Remove.Visible = false;
            this.GridControl_F.LookAndFeel.SkinName = "The Asphalt World";
            this.GridControl_F.Size = new System.Drawing.Size(944, 499);
            // 
            // GridView_F
            // 
            this.GridView_F.Appearance.ColumnFilterButton.BackColor = System.Drawing.Color.Orange;
            this.GridView_F.Appearance.ColumnFilterButton.BorderColor = System.Drawing.Color.Orange;
            this.GridView_F.Appearance.ColumnFilterButton.ForeColor = System.Drawing.Color.DarkOrange;
            this.GridView_F.Appearance.ColumnFilterButton.Options.UseBackColor = true;
            this.GridView_F.Appearance.ColumnFilterButton.Options.UseBorderColor = true;
            this.GridView_F.Appearance.ColumnFilterButton.Options.UseForeColor = true;
            this.GridView_F.Appearance.ColumnFilterButtonActive.BackColor = System.Drawing.Color.DarkOrange;
            this.GridView_F.Appearance.ColumnFilterButtonActive.BorderColor = System.Drawing.Color.DarkOrange;
            this.GridView_F.Appearance.ColumnFilterButtonActive.ForeColor = System.Drawing.Color.White;
            this.GridView_F.Appearance.ColumnFilterButtonActive.Options.UseBackColor = true;
            this.GridView_F.Appearance.ColumnFilterButtonActive.Options.UseBorderColor = true;
            this.GridView_F.Appearance.ColumnFilterButtonActive.Options.UseForeColor = true;
            this.GridView_F.Appearance.Empty.BackColor = System.Drawing.Color.LightSkyBlue;
            this.GridView_F.Appearance.Empty.BackColor2 = System.Drawing.Color.SkyBlue;
            this.GridView_F.Appearance.Empty.GradientMode = System.Drawing.Drawing2D.LinearGradientMode.BackwardDiagonal;
            this.GridView_F.Appearance.Empty.Options.UseBackColor = true;
            this.GridView_F.Appearance.EvenRow.BackColor = System.Drawing.Color.Linen;
            this.GridView_F.Appearance.EvenRow.BackColor2 = System.Drawing.Color.AntiqueWhite;
            this.GridView_F.Appearance.EvenRow.GradientMode = System.Drawing.Drawing2D.LinearGradientMode.ForwardDiagonal;
            this.GridView_F.Appearance.EvenRow.Options.UseBackColor = true;
            this.GridView_F.Appearance.FilterCloseButton.BackColor = System.Drawing.Color.Orange;
            this.GridView_F.Appearance.FilterCloseButton.BorderColor = System.Drawing.Color.Orange;
            this.GridView_F.Appearance.FilterCloseButton.Options.UseBackColor = true;
            this.GridView_F.Appearance.FilterCloseButton.Options.UseBorderColor = true;
            this.GridView_F.Appearance.FilterPanel.BackColor = System.Drawing.Color.DarkOrange;
            this.GridView_F.Appearance.FilterPanel.BackColor2 = System.Drawing.Color.Orange;
            this.GridView_F.Appearance.FilterPanel.ForeColor = System.Drawing.Color.Black;
            this.GridView_F.Appearance.FilterPanel.GradientMode = System.Drawing.Drawing2D.LinearGradientMode.BackwardDiagonal;
            this.GridView_F.Appearance.FilterPanel.Options.UseBackColor = true;
            this.GridView_F.Appearance.FilterPanel.Options.UseForeColor = true;
            this.GridView_F.Appearance.FocusedRow.BackColor = System.Drawing.Color.RoyalBlue;
            this.GridView_F.Appearance.FocusedRow.BackColor2 = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this.GridView_F.Appearance.FocusedRow.ForeColor = System.Drawing.Color.White;
            this.GridView_F.Appearance.FocusedRow.GradientMode = System.Drawing.Drawing2D.LinearGradientMode.Vertical;
            this.GridView_F.Appearance.FocusedRow.Options.UseBackColor = true;
            this.GridView_F.Appearance.FocusedRow.Options.UseForeColor = true;
            this.GridView_F.Appearance.FooterPanel.BackColor = System.Drawing.Color.Orange;
            this.GridView_F.Appearance.FooterPanel.BorderColor = System.Drawing.Color.Orange;
            this.GridView_F.Appearance.FooterPanel.Options.UseBackColor = true;
            this.GridView_F.Appearance.FooterPanel.Options.UseBorderColor = true;
            this.GridView_F.Appearance.GroupButton.BackColor = System.Drawing.Color.Wheat;
            this.GridView_F.Appearance.GroupButton.BorderColor = System.Drawing.Color.Wheat;
            this.GridView_F.Appearance.GroupButton.Options.UseBackColor = true;
            this.GridView_F.Appearance.GroupButton.Options.UseBorderColor = true;
            this.GridView_F.Appearance.GroupFooter.BackColor = System.Drawing.Color.Wheat;
            this.GridView_F.Appearance.GroupFooter.BorderColor = System.Drawing.Color.Wheat;
            this.GridView_F.Appearance.GroupFooter.Options.UseBackColor = true;
            this.GridView_F.Appearance.GroupFooter.Options.UseBorderColor = true;
            this.GridView_F.Appearance.GroupPanel.BackColor = System.Drawing.Color.RoyalBlue;
            this.GridView_F.Appearance.GroupPanel.ForeColor = System.Drawing.Color.White;
            this.GridView_F.Appearance.GroupPanel.Options.UseBackColor = true;
            this.GridView_F.Appearance.GroupPanel.Options.UseForeColor = true;
            this.GridView_F.Appearance.GroupRow.BackColor = System.Drawing.Color.Wheat;
            this.GridView_F.Appearance.GroupRow.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Bold);
            this.GridView_F.Appearance.GroupRow.Options.UseBackColor = true;
            this.GridView_F.Appearance.GroupRow.Options.UseFont = true;
            this.GridView_F.Appearance.HeaderPanel.BackColor = System.Drawing.Color.Orange;
            this.GridView_F.Appearance.HeaderPanel.BorderColor = System.Drawing.Color.Orange;
            this.GridView_F.Appearance.HeaderPanel.Options.UseBackColor = true;
            this.GridView_F.Appearance.HeaderPanel.Options.UseBorderColor = true;
            this.GridView_F.Appearance.HideSelectionRow.BackColor = System.Drawing.Color.LightSlateGray;
            this.GridView_F.Appearance.HideSelectionRow.Options.UseBackColor = true;
            this.GridView_F.Appearance.HorzLine.BackColor = System.Drawing.Color.Tan;
            this.GridView_F.Appearance.HorzLine.Options.UseBackColor = true;
            this.GridView_F.Appearance.OddRow.BackColor = System.Drawing.Color.White;
            this.GridView_F.Appearance.OddRow.Options.UseBackColor = true;
            this.GridView_F.Appearance.Preview.BackColor = System.Drawing.Color.Khaki;
            this.GridView_F.Appearance.Preview.BackColor2 = System.Drawing.Color.Cornsilk;
            this.GridView_F.Appearance.Preview.Font = new System.Drawing.Font("Tahoma", 7.5F);
            this.GridView_F.Appearance.Preview.GradientMode = System.Drawing.Drawing2D.LinearGradientMode.BackwardDiagonal;
            this.GridView_F.Appearance.Preview.Options.UseBackColor = true;
            this.GridView_F.Appearance.Preview.Options.UseFont = true;
            this.GridView_F.Appearance.Row.BackColor = System.Drawing.Color.White;
            this.GridView_F.Appearance.Row.Options.UseBackColor = true;
            this.GridView_F.Appearance.RowSeparator.BackColor = System.Drawing.Color.LightSkyBlue;
            this.GridView_F.Appearance.RowSeparator.Options.UseBackColor = true;
            this.GridView_F.Appearance.VertLine.BackColor = System.Drawing.Color.Tan;
            this.GridView_F.Appearance.VertLine.Options.UseBackColor = true;
            this.GridView_F.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colUSUNome,
            this.colUSUEndereco,
            this.colUSUBairro,
            this.colCIDNome,
            this.colUSUUf,
            this.colUSUCep,
            this.colUSUFone1,
            this.colUSUFone2,
            this.colUSURamal,
            this.colUSUEmail,
            this.colPERNome});
            this.GridView_F.OptionsBehavior.AllowIncrementalSearch = true;
            this.GridView_F.OptionsBehavior.Editable = false;
            this.GridView_F.OptionsLayout.Columns.StoreAllOptions = true;
            this.GridView_F.OptionsLayout.Columns.StoreAppearance = true;
            this.GridView_F.OptionsLayout.StoreAllOptions = true;
            this.GridView_F.OptionsLayout.StoreAppearance = true;
            this.GridView_F.OptionsNavigation.AutoFocusNewRow = true;
            this.GridView_F.OptionsNavigation.EnterMoveNextColumn = true;
            this.GridView_F.OptionsPrint.EnableAppearanceEvenRow = true;
            this.GridView_F.OptionsPrint.EnableAppearanceOddRow = true;
            this.GridView_F.OptionsPrint.ExpandAllDetails = true;
            this.GridView_F.OptionsSelection.InvertSelection = true;
            this.GridView_F.OptionsView.EnableAppearanceEvenRow = true;
            this.GridView_F.OptionsView.EnableAppearanceOddRow = true;
            this.GridView_F.OptionsView.ShowAutoFilterRow = true;
            this.GridView_F.OptionsView.ShowGroupPanel = false;
            this.GridView_F.SortInfo.AddRange(new DevExpress.XtraGrid.Columns.GridColumnSortInfo[] {
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colUSUNome, DevExpress.Data.ColumnSortOrder.Ascending)});
            // 
            // BindingSource_F
            // 
            this.BindingSource_F.DataMember = "USUARIOS";
            this.BindingSource_F.DataSource = this.dUsuariosGrade;
            // 
            // ToolTipController_F
            // 
            this.ToolTipController_F.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(222)))), ((int)(((byte)(237)))), ((int)(((byte)(250)))));
            this.ToolTipController_F.Appearance.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(20)))), ((int)(((byte)(69)))), ((int)(((byte)(141)))));
            this.ToolTipController_F.Appearance.Options.UseBackColor = true;
            this.ToolTipController_F.Appearance.Options.UseFont = true;
            this.ToolTipController_F.Appearance.Options.UseForeColor = true;
            this.ToolTipController_F.Appearance.Options.UseTextOptions = true;
            this.ToolTipController_F.Appearance.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            // 
            // StyleController_F
            // 
            this.StyleController_F.AppearanceDisabled.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this.StyleController_F.AppearanceDisabled.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this.StyleController_F.AppearanceDisabled.Options.UseBackColor = true;
            this.StyleController_F.AppearanceDisabled.Options.UseForeColor = true;
            this.StyleController_F.AppearanceFocused.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.StyleController_F.AppearanceFocused.Options.UseBackColor = true;
            this.StyleController_F.LookAndFeel.SkinName = "Lilian";
            this.StyleController_F.LookAndFeel.UseDefaultLookAndFeel = false;
         
            // 
            // colUSUNome
            // 
            this.colUSUNome.Caption = "Nome";
            this.colUSUNome.FieldName = "USUNome";
            this.colUSUNome.Name = "colUSUNome";
            this.colUSUNome.Visible = true;
            this.colUSUNome.VisibleIndex = 0;
            // 
            // colUSUEndereco
            // 
            this.colUSUEndereco.Caption = "Endere�o";
            this.colUSUEndereco.FieldName = "USUEndereco";
            this.colUSUEndereco.Name = "colUSUEndereco";
            // 
            // colUSUBairro
            // 
            this.colUSUBairro.Caption = "Bairro";
            this.colUSUBairro.FieldName = "USUBairro";
            this.colUSUBairro.Name = "colUSUBairro";
            // 
            // colCIDNome
            // 
            this.colCIDNome.Caption = "Cidade";
            this.colCIDNome.FieldName = "CIDNome";
            this.colCIDNome.Name = "colCIDNome";
            // 
            // colUSUUf
            // 
            this.colUSUUf.Caption = "UF";
            this.colUSUUf.FieldName = "CIDUF";
            this.colUSUUf.Name = "colUSUUf";
            // 
            // colUSUCep
            // 
            this.colUSUCep.Caption = "CEP";
            this.colUSUCep.FieldName = "USUCep";
            this.colUSUCep.Name = "colUSUCep";
            // 
            // colUSUFone1
            // 
            this.colUSUFone1.Caption = "TeleFone 1";
            this.colUSUFone1.FieldName = "USUFone1";
            this.colUSUFone1.Name = "colUSUFone1";
            this.colUSUFone1.Visible = true;
            this.colUSUFone1.VisibleIndex = 1;
            // 
            // colUSUFone2
            // 
            this.colUSUFone2.Caption = "Telefone 2";
            this.colUSUFone2.FieldName = "USUFone2";
            this.colUSUFone2.Name = "colUSUFone2";
            // 
            // colUSURamal
            // 
            this.colUSURamal.Caption = "Ramal";
            this.colUSURamal.FieldName = "USURamal";
            this.colUSURamal.Name = "colUSURamal";
            this.colUSURamal.Visible = true;
            this.colUSURamal.VisibleIndex = 2;
            // 
            // colUSUEmail
            // 
            this.colUSUEmail.Caption = "Email";
            this.colUSUEmail.FieldName = "USUEmail";
            this.colUSUEmail.Name = "colUSUEmail";
            this.colUSUEmail.Visible = true;
            this.colUSUEmail.VisibleIndex = 3;
            // 
            // dUsuariosGrade
            // 
            this.dUsuariosGrade.DataSetName = "dUsuariosGrade";
            this.dUsuariosGrade.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // uSUARIOSTableAdapter
            // 
            this.uSUARIOSTableAdapter.ClearBeforeFill = true;
            // 
            // colPERNome
            // 
            this.colPERNome.Caption = "Tipo de acesso";
            this.colPERNome.FieldName = "PERNome";
            this.colPERNome.Name = "colPERNome";
            this.colPERNome.Visible = true;
            this.colPERNome.VisibleIndex = 4;
            // 
            // cUsuariosGrade
            // 
            this.ComponenteCampos = typeof(Cadastros.Usuarios.cUsuariosCampos);
            this.Cursor = System.Windows.Forms.Cursors.WaitCursor;
            this.LookAndFeel.UseDefaultLookAndFeel = false;
            this.Name = "cUsuariosGrade";
            this.Size = new System.Drawing.Size(944, 638);
            this.ToolTipController_F.SetSuperTip(this, null);
            ((System.ComponentModel.ISupportInitialize)(this.groupControl1)).EndInit();
            this.groupControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.LayoutPesquisa_F)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.PopImprimir_F)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.BarManager_F)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.PopExportar_F)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.GridControl_F)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.GridView_F)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.BindingSource_F)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.StyleController_F)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dUsuariosGrade)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraGrid.Columns.GridColumn colUSUNome;
        private DevExpress.XtraGrid.Columns.GridColumn colUSUEndereco;
        private DevExpress.XtraGrid.Columns.GridColumn colUSUBairro;
        private DevExpress.XtraGrid.Columns.GridColumn colCIDNome;
        private DevExpress.XtraGrid.Columns.GridColumn colUSUUf;
        private DevExpress.XtraGrid.Columns.GridColumn colUSUCep;
        private DevExpress.XtraGrid.Columns.GridColumn colUSUFone1;
        private DevExpress.XtraGrid.Columns.GridColumn colUSUFone2;
        private DevExpress.XtraGrid.Columns.GridColumn colUSURamal;
        private DevExpress.XtraGrid.Columns.GridColumn colUSUEmail;
        private dUsuariosGrade dUsuariosGrade;
        private Cadastros.Usuarios.dUsuariosGradeTableAdapters.USUARIOSTableAdapter uSUARIOSTableAdapter;
        private DevExpress.XtraGrid.Columns.GridColumn colPERNome;
    }
}
