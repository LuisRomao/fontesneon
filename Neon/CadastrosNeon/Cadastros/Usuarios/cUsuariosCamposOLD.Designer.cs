namespace Cadastros.Usuarios
{
    partial class cUsuariosCamposOLD
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.label2 = new System.Windows.Forms.Label();
            this.TXTNome = new DevExpress.XtraEditors.TextEdit();
            this.groupControl2 = new DevExpress.XtraEditors.GroupControl();
            this.txtRamal = new DevExpress.XtraEditors.TextEdit();
            this.label11 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.txtEmail = new DevExpress.XtraEditors.TextEdit();
            this.label8 = new System.Windows.Forms.Label();
            this.txtTelefone2 = new DevExpress.XtraEditors.TextEdit();
            this.label12 = new System.Windows.Forms.Label();
            this.txtTelefone1 = new DevExpress.XtraEditors.TextEdit();
            this.groupControl3 = new DevExpress.XtraEditors.GroupControl();
            this.label7 = new System.Windows.Forms.Label();
            this.txtUF = new DevExpress.XtraEditors.TextEdit();
            this.label6 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.txtBairro = new DevExpress.XtraEditors.TextEdit();
            this.label4 = new System.Windows.Forms.Label();
            this.txtEndereco = new DevExpress.XtraEditors.TextEdit();
            this.label3 = new System.Windows.Forms.Label();
            this.TXTCep = new DevExpress.XtraEditors.TextEdit();
            this.lkpCidade = new DevExpress.XtraEditors.LookUpEdit();
            this.cIDADESBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.dUsuariosCampos = new Cadastros.Usuarios.dUsuariosCampos();
            this.uSUARIOSTableAdapter = new Cadastros.Usuarios.dUsuariosCamposTableAdapters.USUARIOSTableAdapter();
            this.cIDADESTableAdapter = new Cadastros.Usuarios.dUsuariosCamposTableAdapters.CIDADESTableAdapter();
            ((System.ComponentModel.ISupportInitialize)(this.TabControl_F)).BeginInit();
            this.TabControl_F.SuspendLayout();
            this.TabPage_F.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.GroupControl_F)).BeginInit();
            this.GroupControl_F.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.BarManager_F)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.BarAndDockingController_F)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.BindingSource_F)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.StyleController_F)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TXTNome.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl2)).BeginInit();
            this.groupControl2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtRamal.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtEmail.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTelefone2.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTelefone1.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl3)).BeginInit();
            this.groupControl3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtUF.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtBairro.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtEndereco.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TXTCep.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lkpCidade.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cIDADESBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dUsuariosCampos)).BeginInit();
            this.SuspendLayout();
            // 
            // TabControl_F
            // 
            this.TabControl_F.Location = new System.Drawing.Point(0, 74);
            this.TabControl_F.LookAndFeel.SkinName = "The Asphalt World";
            this.TabControl_F.LookAndFeel.UseDefaultLookAndFeel = false;
            this.TabControl_F.Size = new System.Drawing.Size(470, 270);
            // 
            // TabPage_F
            // 
            this.TabPage_F.Controls.Add(this.groupControl2);
            this.TabPage_F.Controls.Add(this.groupControl3);
            this.TabPage_F.Size = new System.Drawing.Size(461, 240);
            // 
            // GroupControl_F
            // 
            this.GroupControl_F.Controls.Add(this.label2);
            this.GroupControl_F.Controls.Add(this.TXTNome);
            this.GroupControl_F.LookAndFeel.SkinName = "Lilian";
            this.GroupControl_F.LookAndFeel.UseDefaultLookAndFeel = false;
            this.GroupControl_F.Size = new System.Drawing.Size(470, 32);
            // 
            // barStatus_F
            // 
            this.barStatus_F.OptionsBar.AllowQuickCustomization = false;
            this.barStatus_F.OptionsBar.DisableClose = true;
            this.barStatus_F.OptionsBar.DisableCustomization = true;
            this.barStatus_F.OptionsBar.DrawDragBorder = false;
            this.barStatus_F.OptionsBar.DrawSizeGrip = true;
            this.barStatus_F.OptionsBar.UseWholeRow = true;
            // 
            // barComponente_F
            // 
            this.barComponente_F.OptionsBar.AllowQuickCustomization = false;
            this.barComponente_F.OptionsBar.DisableClose = true;
            this.barComponente_F.OptionsBar.DisableCustomization = true;
            this.barComponente_F.OptionsBar.DrawDragBorder = false;
            this.barComponente_F.OptionsBar.UseWholeRow = true;
            // 
            // BarAndDockingController_F
            // 
            this.BarAndDockingController_F.LookAndFeel.SkinName = "The Asphalt World";
            this.BarAndDockingController_F.PropertiesBar.AllowLinkLighting = false;
            // 
            // BindingSource_F
            // 
            this.BindingSource_F.DataMember = "USUARIOS";
            this.BindingSource_F.DataSource = this.dUsuariosCampos;
            // 
            // ToolTipController_F
            // 
            this.ToolTipController_F.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(222)))), ((int)(((byte)(237)))), ((int)(((byte)(250)))));
            this.ToolTipController_F.Appearance.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(20)))), ((int)(((byte)(69)))), ((int)(((byte)(141)))));
            this.ToolTipController_F.Appearance.Options.UseBackColor = true;
            this.ToolTipController_F.Appearance.Options.UseFont = true;
            this.ToolTipController_F.Appearance.Options.UseForeColor = true;
            this.ToolTipController_F.Appearance.Options.UseTextOptions = true;
            this.ToolTipController_F.Appearance.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            // 
            // StyleController_F
            // 
            this.StyleController_F.AppearanceDisabled.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.StyleController_F.AppearanceDisabled.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.StyleController_F.AppearanceDisabled.Options.UseBackColor = true;
            this.StyleController_F.AppearanceDisabled.Options.UseForeColor = true;
            this.StyleController_F.AppearanceFocused.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(253)))), ((int)(((byte)(253)))), ((int)(((byte)(181)))));
            this.StyleController_F.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
            this.StyleController_F.AppearanceFocused.Options.UseBackColor = true;
            this.StyleController_F.AppearanceFocused.Options.UseFont = true;
            // 
            // DefaultLookAndFeel_F
            // 
            this.DefaultLookAndFeel_F.LookAndFeel.SkinName = "The Asphalt World";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.ForeColor = System.Drawing.Color.Navy;
            this.label2.Location = new System.Drawing.Point(5, 12);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(39, 13);
            this.label2.TabIndex = 2;
            this.label2.Text = "Nome";
            // 
            // TXTNome
            // 
            this.TXTNome.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.BindingSource_F, "USUNome", true));
            this.TXTNome.Location = new System.Drawing.Point(51, 6);
            this.TXTNome.Name = "TXTNome";
            this.TXTNome.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TXTNome.Properties.Appearance.Options.UseBackColor = true;
            this.TXTNome.Size = new System.Drawing.Size(414, 20);
            this.TXTNome.StyleController = this.StyleController_F;
            this.TXTNome.TabIndex = 3;
            // 
            // groupControl2
            // 
            this.groupControl2.Controls.Add(this.txtRamal);
            this.groupControl2.Controls.Add(this.label11);
            this.groupControl2.Controls.Add(this.label10);
            this.groupControl2.Controls.Add(this.txtEmail);
            this.groupControl2.Controls.Add(this.label8);
            this.groupControl2.Controls.Add(this.txtTelefone2);
            this.groupControl2.Controls.Add(this.label12);
            this.groupControl2.Controls.Add(this.txtTelefone1);
            this.groupControl2.Location = new System.Drawing.Point(3, 136);
            this.groupControl2.LookAndFeel.SkinName = "Lilian";
            this.groupControl2.LookAndFeel.UseDefaultLookAndFeel = false;
            this.groupControl2.Name = "groupControl2";
            this.groupControl2.Size = new System.Drawing.Size(455, 101);
            this.groupControl2.TabIndex = 3;
            this.groupControl2.Text = ":: Contato ::";
            // 
            // txtRamal
            // 
            this.txtRamal.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.BindingSource_F, "USURamal", true));
            this.txtRamal.Location = new System.Drawing.Point(78, 23);
            this.txtRamal.Name = "txtRamal";
            this.txtRamal.Size = new System.Drawing.Size(130, 20);
            this.txtRamal.StyleController = this.StyleController_F;
            this.txtRamal.TabIndex = 9;
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label11.ForeColor = System.Drawing.Color.Navy;
            this.label11.Location = new System.Drawing.Point(29, 30);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(43, 13);
            this.label11.TabIndex = 8;
            this.label11.Text = "Ramal";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label10.ForeColor = System.Drawing.Color.Navy;
            this.label10.Location = new System.Drawing.Point(35, 82);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(37, 13);
            this.label10.TabIndex = 6;
            this.label10.Text = "Email";
            // 
            // txtEmail
            // 
            this.txtEmail.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.BindingSource_F, "USUEmail", true));
            this.txtEmail.Location = new System.Drawing.Point(78, 75);
            this.txtEmail.Name = "txtEmail";
            this.txtEmail.Size = new System.Drawing.Size(372, 20);
            this.txtEmail.StyleController = this.StyleController_F;
            this.txtEmail.TabIndex = 7;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.ForeColor = System.Drawing.Color.Navy;
            this.label8.Location = new System.Drawing.Point(247, 56);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(67, 13);
            this.label8.TabIndex = 2;
            this.label8.Text = "Tefefone 2";
            // 
            // txtTelefone2
            // 
            this.txtTelefone2.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.BindingSource_F, "USUFone2", true));
            this.txtTelefone2.Location = new System.Drawing.Point(320, 49);
            this.txtTelefone2.Name = "txtTelefone2";
            this.txtTelefone2.Size = new System.Drawing.Size(130, 20);
            this.txtTelefone2.StyleController = this.StyleController_F;
            this.txtTelefone2.TabIndex = 3;
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label12.ForeColor = System.Drawing.Color.Navy;
            this.label12.Location = new System.Drawing.Point(5, 56);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(67, 13);
            this.label12.TabIndex = 0;
            this.label12.Text = "Tefefone 1";
            // 
            // txtTelefone1
            // 
            this.txtTelefone1.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.BindingSource_F, "USUFone1", true));
            this.txtTelefone1.Location = new System.Drawing.Point(78, 49);
            this.txtTelefone1.Name = "txtTelefone1";
            this.txtTelefone1.Size = new System.Drawing.Size(130, 20);
            this.txtTelefone1.StyleController = this.StyleController_F;
            this.txtTelefone1.TabIndex = 1;
            // 
            // groupControl3
            // 
            this.groupControl3.Controls.Add(this.label7);
            this.groupControl3.Controls.Add(this.txtUF);
            this.groupControl3.Controls.Add(this.label6);
            this.groupControl3.Controls.Add(this.label5);
            this.groupControl3.Controls.Add(this.txtBairro);
            this.groupControl3.Controls.Add(this.label4);
            this.groupControl3.Controls.Add(this.txtEndereco);
            this.groupControl3.Controls.Add(this.label3);
            this.groupControl3.Controls.Add(this.TXTCep);
            this.groupControl3.Controls.Add(this.lkpCidade);
            this.groupControl3.Location = new System.Drawing.Point(3, 3);
            this.groupControl3.LookAndFeel.SkinName = "Lilian";
            this.groupControl3.LookAndFeel.UseDefaultLookAndFeel = false;
            this.groupControl3.Name = "groupControl3";
            this.groupControl3.Size = new System.Drawing.Size(455, 127);
            this.groupControl3.TabIndex = 2;
            this.groupControl3.Text = ":: Endere�o ::";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.ForeColor = System.Drawing.Color.Navy;
            this.label7.Location = new System.Drawing.Point(393, 108);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(21, 13);
            this.label7.TabIndex = 8;
            this.label7.Text = "UF";
            // 
            // txtUF
            // 
            this.txtUF.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.BindingSource_F, "USUUf", true));
            this.txtUF.Location = new System.Drawing.Point(420, 101);
            this.txtUF.Name = "txtUF";
            this.txtUF.Size = new System.Drawing.Size(30, 20);
            this.txtUF.StyleController = this.StyleController_F;
            this.txtUF.TabIndex = 9;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.ForeColor = System.Drawing.Color.Navy;
            this.label6.Location = new System.Drawing.Point(27, 108);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(45, 13);
            this.label6.TabIndex = 6;
            this.label6.Text = "Cidade";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.ForeColor = System.Drawing.Color.Navy;
            this.label5.Location = new System.Drawing.Point(31, 82);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(41, 13);
            this.label5.TabIndex = 4;
            this.label5.Text = "Bairro";
            // 
            // txtBairro
            // 
            this.txtBairro.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.BindingSource_F, "USUBairro", true));
            this.txtBairro.Location = new System.Drawing.Point(78, 75);
            this.txtBairro.Name = "txtBairro";
            this.txtBairro.Size = new System.Drawing.Size(372, 20);
            this.txtBairro.StyleController = this.StyleController_F;
            this.txtBairro.TabIndex = 5;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.ForeColor = System.Drawing.Color.Navy;
            this.label4.Location = new System.Drawing.Point(13, 56);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(59, 13);
            this.label4.TabIndex = 2;
            this.label4.Text = "Endere�o";
            // 
            // txtEndereco
            // 
            this.txtEndereco.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.BindingSource_F, "USUEndereco", true));
            this.txtEndereco.Location = new System.Drawing.Point(78, 49);
            this.txtEndereco.Name = "txtEndereco";
            this.txtEndereco.Size = new System.Drawing.Size(372, 20);
            this.txtEndereco.StyleController = this.StyleController_F;
            this.txtEndereco.TabIndex = 3;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.ForeColor = System.Drawing.Color.Navy;
            this.label3.Location = new System.Drawing.Point(45, 30);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(27, 13);
            this.label3.TabIndex = 0;
            this.label3.Text = "CEP";
            // 
            // TXTCep
            // 
            this.TXTCep.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.BindingSource_F, "USUCep", true));
            this.TXTCep.Location = new System.Drawing.Point(78, 23);
            this.TXTCep.Name = "TXTCep";
            this.TXTCep.Size = new System.Drawing.Size(90, 20);
            this.TXTCep.StyleController = this.StyleController_F;
            this.TXTCep.TabIndex = 1;
            // 
            // lkpCidade
            // 
            this.lkpCidade.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.BindingSource_F, "USU_CID", true));
            this.lkpCidade.Location = new System.Drawing.Point(78, 101);
            this.lkpCidade.Name = "lkpCidade";
            this.lkpCidade.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo),
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Plus)});
            this.lkpCidade.Properties.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("CIDNome", "Nome", 51, DevExpress.Utils.FormatType.None, "", true, DevExpress.Utils.HorzAlignment.Near)});
            this.lkpCidade.Properties.DataSource = this.cIDADESBindingSource;
            this.lkpCidade.Properties.DisplayMember = "CIDNome";
            this.lkpCidade.Properties.NullText = "";
            this.lkpCidade.Properties.ValueMember = "CID";
            this.lkpCidade.Size = new System.Drawing.Size(309, 20);
            this.lkpCidade.StyleController = this.StyleController_F;
            this.lkpCidade.TabIndex = 7;
            // 
            // cIDADESBindingSource
            // 
            this.cIDADESBindingSource.DataMember = "CIDADES";
            this.cIDADESBindingSource.DataSource = this.dUsuariosCampos;
            // 
            // dUsuariosCampos
            // 
            this.dUsuariosCampos.DataSetName = "dUsuariosCampos";
            this.dUsuariosCampos.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // uSUARIOSTableAdapter
            // 
            this.uSUARIOSTableAdapter.ClearBeforeFill = true;
            // 
            // cIDADESTableAdapter
            // 
            this.cIDADESTableAdapter.ClearBeforeFill = true;
            // 
            // cUsuariosCampos
            // 
            this.LookAndFeel.UseDefaultLookAndFeel = false;
            this.Name = "cUsuariosCampos";
            this.Size = new System.Drawing.Size(470, 369);
            ((System.ComponentModel.ISupportInitialize)(this.TabControl_F)).EndInit();
            this.TabControl_F.ResumeLayout(false);
            this.TabPage_F.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.GroupControl_F)).EndInit();
            this.GroupControl_F.ResumeLayout(false);
            this.GroupControl_F.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.BarManager_F)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.BarAndDockingController_F)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.BindingSource_F)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.StyleController_F)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TXTNome.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl2)).EndInit();
            this.groupControl2.ResumeLayout(false);
            this.groupControl2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtRamal.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtEmail.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTelefone2.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTelefone1.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl3)).EndInit();
            this.groupControl3.ResumeLayout(false);
            this.groupControl3.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtUF.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtBairro.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtEndereco.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TXTCep.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lkpCidade.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cIDADESBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dUsuariosCampos)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Label label2;
        private DevExpress.XtraEditors.TextEdit TXTNome;
        private DevExpress.XtraEditors.GroupControl groupControl2;
        private System.Windows.Forms.Label label10;
        private DevExpress.XtraEditors.TextEdit txtEmail;
        private System.Windows.Forms.Label label8;
        private DevExpress.XtraEditors.TextEdit txtTelefone2;
        private System.Windows.Forms.Label label12;
        private DevExpress.XtraEditors.TextEdit txtTelefone1;
        private DevExpress.XtraEditors.GroupControl groupControl3;
        private System.Windows.Forms.Label label7;
        private DevExpress.XtraEditors.TextEdit txtUF;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label5;
        private DevExpress.XtraEditors.TextEdit txtBairro;
        private System.Windows.Forms.Label label4;
        private DevExpress.XtraEditors.TextEdit txtEndereco;
        private System.Windows.Forms.Label label3;
        private DevExpress.XtraEditors.TextEdit TXTCep;
        private dUsuariosCampos dUsuariosCampos;
        private Cadastros.Usuarios.dUsuariosCamposTableAdapters.USUARIOSTableAdapter uSUARIOSTableAdapter;
        private DevExpress.XtraEditors.TextEdit txtRamal;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.BindingSource cIDADESBindingSource;
        private Cadastros.Usuarios.dUsuariosCamposTableAdapters.CIDADESTableAdapter cIDADESTableAdapter;
        private DevExpress.XtraEditors.LookUpEdit lkpCidade;
    }
}
