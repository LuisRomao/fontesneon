using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using CompontesBasicos;
using System.Data.SqlClient;

namespace Cadastros.Usuarios
{
    /// <summary>
    /// 
    /// </summary>
    public partial class cUsuariosGrade : CompontesBasicos.ComponenteGradeNavegadorPesquisa
    {
        /// <summary>
        /// 
        /// </summary>
        public cUsuariosGrade()
        {
            InitializeComponent();
        }

        /// <summary>
        /// 
        /// </summary>
        protected override void ExcluirRegistro_F()
        {
            int pk = Convert.ToInt32(((DataRowView)(BindingSource_F.Current))["USU"]);

            uSUARIOSTableAdapter.DeleteQuery(pk);
        }
    }
}

