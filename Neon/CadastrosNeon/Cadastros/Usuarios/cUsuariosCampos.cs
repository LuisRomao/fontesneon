/*
27/01/2015 LH 14.2.4.8 - Ajusta tamanho da imagem
*/

using System;
using System.Data;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Drawing.Printing;
using System.IO;
using System.Windows.Forms;
using Cadastros.Cidades;
//using Digital;


namespace Cadastros.Usuarios
{
    /// <summary>
    /// 
    /// </summary>
    public partial class cUsuariosCampos : CompontesBasicos.ComponenteCamposBase
    {
        /// <summary>
        /// 
        /// </summary>
        public cUsuariosCampos()
        {
            InitializeComponent();
        }

        /// <summary>
        /// 
        /// </summary>
        public void SetaUsuarioUnico() {
            layoutControlGroup6.Enabled = false;
            
        }

        private void lkpCidades_EditValueChanged(object sender, EventArgs e)
        {
            lkpUF.EditValue = lkpCidades.EditValue;
        }

        private void lkpCidades_ButtonClick(object sender, DevExpress.XtraEditors.Controls.ButtonPressedEventArgs e)
        {
            if (e.Button.Index == 1) //Adicionar
            {
                Int32 _PK = ShowModuloAdd(typeof(cCidadesGrade));

                if (_PK > 0)
                {
                    cIDADESTableAdapter.Fill(dUsuariosCampos.CIDADES);
                    lkpCidades.EditValue = _PK;
                }
            }
        }

        private dUsuariosCampos.USUARIOSRow LinhaMae
        {
            get
            {
                return (dUsuariosCampos.USUARIOSRow)LinhaMae_F;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected override void btnConfirmar_F_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            //Salvando Imagem
            if (picFoto.Image != null)
            {
                MemoryStream _picStream = new MemoryStream();
                //picFoto.Image.Save(_picStream, picFoto.Image.RawFormat);               
                picFoto.Image.Save(_picStream, System.Drawing.Imaging.ImageFormat.Jpeg);               
                Byte[] _arrImage = _picStream.ToArray();
                _picStream.Close();
                ((DataRowView)(BindingSource_F.Current))["USUFoto"] = _arrImage;
            }

            if(Update_F())
                FechaTela(DialogResult.OK);            
        }

        private void cUsuariosCampos_Load(object sender, EventArgs e)
        {
            cIDADESTableAdapter.Fill(dUsuariosCampos.CIDADES);
            foreach (String strPrinter in PrinterSettings.InstalledPrinters)
                comboBoxEdit1.Properties.Items.Add(strPrinter.ToUpper());

                
            if (((DataRowView)(BindingSource_F.Current))["USUFoto"] != DBNull.Value)
            {
                Byte[] _arrImage = (Byte[])((DataRowView)(BindingSource_F.Current))["USUFoto"];
                MemoryStream _picStream = new MemoryStream(_arrImage);
                picFoto.Image = Image.FromStream(_picStream);
            }
        }

        private void simpleButton1_Click(object sender, EventArgs e)
        {
            //serro = erro.ToString();
            OpenFileDialog _ImageDialog = new OpenFileDialog();

            _ImageDialog.Filter = "Arquivo Bitmap|*.bmp|Arquivo JPEG|*.jpg|Arquivo PNG|*.png|Arquivo WMF|*.wmf";
            _ImageDialog.Title = "Selecione um Arquivo";

            if (_ImageDialog.ShowDialog() == DialogResult.OK)
            {
                picFoto.Image = null;
                Image Original = Image.FromFile(_ImageDialog.FileName);
                Image Ajustada = ResizeImage(Original, 100 ,120,false);
                picFoto.Image = Ajustada;
                //picFoto.Image = Original;
            }

            _ImageDialog.Dispose();
        }

        private static System.Drawing.Image ResizeImage(System.Drawing.Image image, int? W, int? H, bool ampliar = true, bool Diminuir = true, bool preserveAspectRatio = true)
        {
            bool redim = true;
            int newWidth;
            int newHeight;
            if (preserveAspectRatio)
            {
                int originalWidth = image.Width;
                int originalHeight = image.Height;
                float percentWidth = (float)W.GetValueOrDefault(1000000) / (float)originalWidth;
                float percentHeight = (float)H.GetValueOrDefault(1000000) / (float)originalHeight;
                float percent = percentHeight < percentWidth ? percentHeight : percentWidth;
                newWidth = (int)(originalWidth * percent);
                newHeight = (int)(originalHeight * percent);
                if ((newWidth < originalWidth) && !Diminuir)
                    redim = false;
                    //return image;
                if ((newWidth > originalWidth) && !ampliar)
                    redim = false;
                    //return image;
            }
            else
            {
                newWidth = W.Value;
                newHeight = H.Value;
            }
            if (!redim)
            {
                newWidth = image.Width;
                newHeight = image.Height;
            }
            System.Drawing.Image newImage = new Bitmap(newWidth, newHeight);
            using (Graphics graphicsHandle = Graphics.FromImage(newImage))
            {
                //graphicsHandle.InterpolationMode = InterpolationMode.HighQualityBicubic;
                graphicsHandle.InterpolationMode = InterpolationMode.Low;
                if (redim)
                    graphicsHandle.DrawImage(image, 0, 0, newWidth, newHeight);
                else
                    graphicsHandle.DrawImage(image, 0, 0);
            }
            return newImage;
        }

        private void txtLogin_Validated(object sender, EventArgs e)
        {
            txtLogin.ErrorText = "";

            Object _USUNome = uSUARIOSTableAdapter.VerificaLogin(txtLogin.Text, Convert.ToInt32(((DataRowView)(BindingSource_F.Current))["USU"]));

            if (_USUNome != null)
                txtLogin.ErrorText = "O login informado j� pertence ao usu�rio " + _USUNome.ToString();
        }

        private void cUsuariosCampos_cargaInicial(object sender, EventArgs e)
        {
            pERfilTableAdapter.Fill(dUsuariosCampos.PERfil);
        }
        
        private void simpleButton2_Click(object sender, EventArgs e)
        {
            //dPonto.dPontoSt.DIGitaisTableAdapter.Fill(dPonto.dPontoSt.DIGitais);
            //FFinger.FFingerSt.SetBancoInterno(dPonto.dPontoSt.DIGitais);
            //FFinger.FFingerSt.DigitalLida += new System.EventHandler(LeituraFeita);
            //int USU = Digital.FFinger.FFingerSt.Busca();
            //if (USU == pk)
            //    MessageBox.Show("OK");
        }

        /*
        private void simpleButton3_Click(object sender, EventArgs e)
        {
            
            FFinger.FFingerSt.Ligar(false);
            Array Lido = FFinger.FFingerSt.Captura();
            if (Lido != null)
            {
                Ponto.dPonto.DIGitaisRow novaRow = Ponto.dPonto.dPontoSt.DIGitais.NewDIGitaisRow();
                novaRow.DIG_USU = pk;
                novaRow.DIGVetor = (byte[])Lido;
                Ponto.dPonto.dPontoSt.DIGitais.AddDIGitaisRow(novaRow);
                Ponto.dPonto.dPontoSt.DIGitaisTableAdapter.Update(novaRow);
                novaRow.AcceptChanges();
                //if (VirMSSQL.TableAdapter.STTableAdapter.ExecutarSQLNonQuery("insert into DIGitais (DIG_USU,DIGVetor) values (@P1,@P2)", LinhaMae.USU, Lido) == 1)
                MessageBox.Show("Cadastrado");
            }
            
        }*/

        //public object erro;
        //public string serro;

            /*
        private void Apagar(object sender, EventArgs e)
        {
            
            System.Collections.ArrayList Apagar = new System.Collections.ArrayList();
            foreach (Ponto.dPonto.DIGitaisRow novaRow in Ponto.dPonto.dPontoSt.DIGitais)
                if (novaRow.DIG_USU == pk)
                    Apagar.Add(novaRow);
            foreach (Ponto.dPonto.DIGitaisRow novaRow in Apagar)
                novaRow.Delete();
            Ponto.dPonto.dPontoSt.DIGitaisTableAdapter.Update(Ponto.dPonto.dPontoSt.DIGitais);
            Ponto.dPonto.dPontoSt.DIGitais.AcceptChanges();
            
        }*/

        
        
    }
}

