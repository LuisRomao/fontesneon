﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Text;
using System.Windows.Forms;

namespace Cadastros.Feriados
{
    /// <summary>
    /// 
    /// </summary>
    public partial class cFeriados : CompontesBasicos.ComponenteGradeNavegador
    {
        /// <summary>
        /// 
        /// </summary>
        public cFeriados()
        {
            InitializeComponent();
            if (!DesignMode)
            {
                dFERiadosBindingSource.DataSource = Framework.datasets.dFERiados.dFERiadosSt;
                TableAdapterPrincipal = Framework.datasets.dFERiados.dFERiadosSt.FERiadosTableAdapter;
            }
        }
    }
}
