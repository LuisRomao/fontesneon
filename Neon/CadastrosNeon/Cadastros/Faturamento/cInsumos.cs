﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using CompontesBasicos;
using CadastrosProc.Faturamento;

namespace Cadastros.Faturamento
{
    public partial class cInsumos : ComponenteGradeNavegador
    {
        private dInsumos dInsumos1;

        public cInsumos()
        {
            InitializeComponent();
            dInsumos1 = new dInsumos();
            dInsumos1.PLAnocontasTableAdapter.Fill(dInsumos1.PLAnocontas);
            dInsumos1.INSumosTableAdapter.Fill(dInsumos1.INSumos);
            BindingSource_F.DataSource = dInsumos1;
            pLAnocontasBindingSource.DataSource = dInsumos1;
        }

        private void GridView_F_RowUpdated(object sender, DevExpress.XtraGrid.Views.Base.RowObjectEventArgs e)
        {
            DataRowView DRV = (DataRowView)e.Row;
            dInsumos.INSumosRow row = (dInsumos.INSumosRow)DRV.Row;  
            if (row != null)
                dInsumos1.INSumosTableAdapter.Update(row);           
        }

        private void GridView_F_InitNewRow(object sender, DevExpress.XtraGrid.Views.Grid.InitNewRowEventArgs e)
        {
            dInsumos.INSumosRow row = (dInsumos.INSumosRow)GridView_F.GetDataRow(e.RowHandle);
            if (row != null)
            {
                row.INSDescricao = "";
                row.INSAtivo = true;
                row.INSUnidade = "pç";
                row.INSValor = 0;
            }
        }
    }
}
