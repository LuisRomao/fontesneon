﻿namespace Cadastros.Faturamento
{
    partial class cInsumos
    {
        /// <summary> 
        /// Variável de designer necessária.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Limpar os recursos que estão sendo usados.
        /// </summary>
        /// <param name="disposing">true se for necessário descartar os recursos gerenciados; caso contrário, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Código gerado pelo Designer de Componentes

        /// <summary> 
        /// Método necessário para suporte ao Designer - não modifique 
        /// o conteúdo deste método com o editor de código.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(cInsumos));
            this.colINS = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colINSDescricao = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colINSValor = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemCalcEdit1 = new DevExpress.XtraEditors.Repository.RepositoryItemCalcEdit();
            this.colINSUnidade = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colINSAtivo = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colINS_PLA = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemLookUpEditPLA = new DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit();
            this.repositoryItemLookUpEditDesc = new DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit();
            this.pLAnocontasBindingSource = new System.Windows.Forms.BindingSource(this.components);
            ((System.ComponentModel.ISupportInitialize)(this.PopImprimir_F)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.BarManager_F)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.PopExportar_F)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.GridControl_F)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.GridView_F)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.BindingSource_F)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.StyleController_F)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCalcEdit1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemLookUpEditPLA)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemLookUpEditDesc)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pLAnocontasBindingSource)).BeginInit();
            this.SuspendLayout();
            // 
            // barNavegacao_F
            // 
            this.barNavegacao_F.OptionsBar.DisableClose = true;
            this.barNavegacao_F.OptionsBar.DrawDragBorder = false;
            this.barNavegacao_F.OptionsBar.DrawSizeGrip = true;
            this.barNavegacao_F.OptionsBar.RotateWhenVertical = false;
            this.barNavegacao_F.OptionsBar.UseWholeRow = true;
            // 
            // BtnIncluir_F
            // 
            this.BtnIncluir_F.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("BtnIncluir_F.ImageOptions.Image")));
            // 
            // BtnAlterar_F
            // 
            this.BtnAlterar_F.Enabled = false;
            this.BtnAlterar_F.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("BtnAlterar_F.ImageOptions.Image")));
            this.BtnAlterar_F.ImageOptions.ImageIndex = 0;
            // 
            // BtnExcluir_F
            // 
            this.BtnExcluir_F.Enabled = false;
            this.BtnExcluir_F.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("BtnExcluir_F.ImageOptions.Image")));
            this.BtnExcluir_F.Visibility = DevExpress.XtraBars.BarItemVisibility.Never;
            // 
            // BtnImprimir_F
            // 
            this.BtnImprimir_F.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("BtnImprimir_F.ImageOptions.Image")));
            // 
            // BtnImprimirGrade_F
            // 
            this.BtnImprimirGrade_F.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("BtnImprimirGrade_F.ImageOptions.Image")));
            // 
            // BtnExportar_F
            // 
            this.BtnExportar_F.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("BtnExportar_F.ImageOptions.Image")));
            // 
            // btnConfirmar_F
            // 
            this.btnConfirmar_F.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("btnConfirmar_F.ImageOptions.Image")));
            // 
            // btnCancelar_F
            // 
            this.btnCancelar_F.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("btnCancelar_F.ImageOptions.Image")));
            // 
            // barDataBase_F
            // 
            this.barDataBase_F.OptionsBar.AllowQuickCustomization = false;
            this.barDataBase_F.OptionsBar.DisableClose = true;
            this.barDataBase_F.OptionsBar.DisableCustomization = true;
            this.barDataBase_F.OptionsBar.DrawDragBorder = false;
            this.barDataBase_F.OptionsBar.UseWholeRow = true;
            // 
            // GridControl_F
            // 
            this.GridControl_F.EmbeddedNavigator.Buttons.Append.Visible = false;
            this.GridControl_F.EmbeddedNavigator.Buttons.CancelEdit.Visible = false;
            this.GridControl_F.EmbeddedNavigator.Buttons.Edit.Visible = false;
            this.GridControl_F.EmbeddedNavigator.Buttons.EndEdit.Visible = false;
            this.GridControl_F.EmbeddedNavigator.Buttons.Remove.Visible = false;
            this.GridControl_F.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemCalcEdit1,
            this.repositoryItemLookUpEditPLA,
            this.repositoryItemLookUpEditDesc});
            this.GridControl_F.Size = new System.Drawing.Size(1511, 747);
            // 
            // GridView_F
            // 
            this.GridView_F.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colINS,
            this.colINSDescricao,
            this.colINSValor,
            this.colINSUnidade,
            this.colINSAtivo,
            this.colINS_PLA,
            this.gridColumn1});
            this.GridView_F.OptionsBehavior.AllowIncrementalSearch = true;
            this.GridView_F.OptionsBehavior.Editable = false;
            this.GridView_F.OptionsLayout.Columns.StoreAllOptions = true;
            this.GridView_F.OptionsLayout.Columns.StoreAppearance = true;
            this.GridView_F.OptionsLayout.StoreAllOptions = true;
            this.GridView_F.OptionsLayout.StoreAppearance = true;
            this.GridView_F.OptionsNavigation.EnterMoveNextColumn = true;
            this.GridView_F.OptionsPrint.EnableAppearanceEvenRow = true;
            this.GridView_F.OptionsPrint.EnableAppearanceOddRow = true;
            this.GridView_F.OptionsPrint.ExpandAllDetails = true;
            this.GridView_F.OptionsView.ColumnAutoWidth = false;
            this.GridView_F.OptionsView.GroupFooterShowMode = DevExpress.XtraGrid.Views.Grid.GroupFooterShowMode.VisibleAlways;
            this.GridView_F.OptionsView.NewItemRowPosition = DevExpress.XtraGrid.Views.Grid.NewItemRowPosition.Bottom;
            this.GridView_F.OptionsView.ShowAutoFilterRow = true;
            this.GridView_F.OptionsView.ShowFooter = true;
            this.GridView_F.OptionsView.ShowGroupPanel = false;
            this.GridView_F.SortInfo.AddRange(new DevExpress.XtraGrid.Columns.GridColumnSortInfo[] {
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colINSDescricao, DevExpress.Data.ColumnSortOrder.Ascending)});
            this.GridView_F.InitNewRow += new DevExpress.XtraGrid.Views.Grid.InitNewRowEventHandler(this.GridView_F_InitNewRow);
            this.GridView_F.RowUpdated += new DevExpress.XtraGrid.Views.Base.RowObjectEventHandler(this.GridView_F_RowUpdated);
            // 
            // BindingSource_F
            // 
            this.BindingSource_F.DataMember = "INSumos";
            this.BindingSource_F.DataSource = typeof(CadastrosProc.Faturamento.dInsumos);
            // 
            // ToolTipController_F
            // 
            this.ToolTipController_F.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.ToolTipController_F.Appearance.ForeColor = System.Drawing.Color.Black;
            this.ToolTipController_F.Appearance.Options.UseBackColor = true;
            this.ToolTipController_F.Appearance.Options.UseFont = true;
            this.ToolTipController_F.Appearance.Options.UseForeColor = true;
            this.ToolTipController_F.Appearance.Options.UseTextOptions = true;
            this.ToolTipController_F.Appearance.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            // 
            // StyleController_F
            // 
            this.StyleController_F.AppearanceDisabled.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this.StyleController_F.AppearanceDisabled.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this.StyleController_F.AppearanceDisabled.Options.UseBackColor = true;
            this.StyleController_F.AppearanceDisabled.Options.UseForeColor = true;
            this.StyleController_F.AppearanceFocused.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.StyleController_F.AppearanceFocused.Options.UseBackColor = true;
            // 
            // colINS
            // 
            this.colINS.FieldName = "INS";
            this.colINS.Name = "colINS";
            // 
            // colINSDescricao
            // 
            this.colINSDescricao.Caption = "Descrição";
            this.colINSDescricao.FieldName = "INSDescricao";
            this.colINSDescricao.Name = "colINSDescricao";
            this.colINSDescricao.Visible = true;
            this.colINSDescricao.VisibleIndex = 0;
            this.colINSDescricao.Width = 407;
            // 
            // colINSValor
            // 
            this.colINSValor.Caption = "Valor";
            this.colINSValor.ColumnEdit = this.repositoryItemCalcEdit1;
            this.colINSValor.FieldName = "INSValor";
            this.colINSValor.Name = "colINSValor";
            this.colINSValor.Visible = true;
            this.colINSValor.VisibleIndex = 1;
            this.colINSValor.Width = 132;
            // 
            // repositoryItemCalcEdit1
            // 
            this.repositoryItemCalcEdit1.AutoHeight = false;
            this.repositoryItemCalcEdit1.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemCalcEdit1.Mask.EditMask = "n2";
            this.repositoryItemCalcEdit1.Mask.UseMaskAsDisplayFormat = true;
            this.repositoryItemCalcEdit1.Name = "repositoryItemCalcEdit1";
            // 
            // colINSUnidade
            // 
            this.colINSUnidade.Caption = "Unidade";
            this.colINSUnidade.FieldName = "INSUnidade";
            this.colINSUnidade.Name = "colINSUnidade";
            this.colINSUnidade.Visible = true;
            this.colINSUnidade.VisibleIndex = 2;
            this.colINSUnidade.Width = 97;
            // 
            // colINSAtivo
            // 
            this.colINSAtivo.Caption = "Ativo";
            this.colINSAtivo.FieldName = "INSAtivo";
            this.colINSAtivo.Name = "colINSAtivo";
            this.colINSAtivo.Visible = true;
            this.colINSAtivo.VisibleIndex = 5;
            // 
            // colINS_PLA
            // 
            this.colINS_PLA.ColumnEdit = this.repositoryItemLookUpEditPLA;
            this.colINS_PLA.FieldName = "INS_PLA";
            this.colINS_PLA.Name = "colINS_PLA";
            this.colINS_PLA.Visible = true;
            this.colINS_PLA.VisibleIndex = 3;
            this.colINS_PLA.Width = 104;
            // 
            // gridColumn1
            // 
            this.gridColumn1.Caption = "gridColumn1";
            this.gridColumn1.ColumnEdit = this.repositoryItemLookUpEditDesc;
            this.gridColumn1.FieldName = "INS_PLA";
            this.gridColumn1.Name = "gridColumn1";
            this.gridColumn1.Visible = true;
            this.gridColumn1.VisibleIndex = 4;
            this.gridColumn1.Width = 256;
            // 
            // repositoryItemLookUpEditPLA
            // 
            this.repositoryItemLookUpEditPLA.AutoHeight = false;
            this.repositoryItemLookUpEditPLA.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemLookUpEditPLA.DataSource = this.pLAnocontasBindingSource;
            this.repositoryItemLookUpEditPLA.DisplayMember = "PLA";
            this.repositoryItemLookUpEditPLA.Name = "repositoryItemLookUpEditPLA";
            this.repositoryItemLookUpEditPLA.ValueMember = "PLA";
            // 
            // repositoryItemLookUpEditDesc
            // 
            this.repositoryItemLookUpEditDesc.AutoHeight = false;
            this.repositoryItemLookUpEditDesc.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemLookUpEditDesc.DataSource = this.pLAnocontasBindingSource;
            this.repositoryItemLookUpEditDesc.DisplayMember = "PLADescricao";
            this.repositoryItemLookUpEditDesc.Name = "repositoryItemLookUpEditDesc";
            this.repositoryItemLookUpEditDesc.ValueMember = "PLA";
            // 
            // pLAnocontasBindingSource
            // 
            this.pLAnocontasBindingSource.DataMember = "PLAnocontas";
            this.pLAnocontasBindingSource.DataSource = typeof(CadastrosProc.Faturamento.dInsumos);
            // 
            // cInsumos
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BotaoExcluir = false;
            this.CaixaAltaGeral = false;
            this.LookAndFeel.UseDefaultLookAndFeel = false;
            this.Name = "cInsumos";
            ((System.ComponentModel.ISupportInitialize)(this.PopImprimir_F)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.BarManager_F)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.PopExportar_F)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.GridControl_F)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.GridView_F)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.BindingSource_F)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.StyleController_F)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCalcEdit1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemLookUpEditPLA)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemLookUpEditDesc)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pLAnocontasBindingSource)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private DevExpress.XtraEditors.Repository.RepositoryItemCalcEdit repositoryItemCalcEdit1;
        private DevExpress.XtraGrid.Columns.GridColumn colINS;
        private DevExpress.XtraGrid.Columns.GridColumn colINSDescricao;
        private DevExpress.XtraGrid.Columns.GridColumn colINSValor;
        private DevExpress.XtraGrid.Columns.GridColumn colINSUnidade;
        private DevExpress.XtraGrid.Columns.GridColumn colINSAtivo;
        private DevExpress.XtraGrid.Columns.GridColumn colINS_PLA;
        private DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit repositoryItemLookUpEditPLA;
        private System.Windows.Forms.BindingSource pLAnocontasBindingSource;
        private DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit repositoryItemLookUpEditDesc;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn1;
    }
}
