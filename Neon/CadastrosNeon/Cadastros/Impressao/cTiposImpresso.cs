using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace Cadastros.Impressao
{
    /// <summary>
    /// 
    /// </summary>
    public partial class cTiposImpresso : CompontesBasicos.ComponenteGradeNavegador
    {
        /// <summary>
        /// 
        /// </summary>
        public cTiposImpresso()
        {
            InitializeComponent();
            tableAdapter = Framework.DSCentral.TiPoImpressaoTableAdapter;
            tabela = Framework.DSCentral.DCentral.TiPoImpressao;
            Type[] Tipos = new Type[1];

            Tipos[0] = typeof(Framework.DSCentral.TiPoImpressaoDataTable);
            metodoupdate = tableAdapter.GetType().GetMethod("Update",Tipos);
        }

        private void GridControl_F_Load(object sender, EventArgs e)
        {
            BindingSource_F.DataSource = Framework.DSCentral.DCentral;
            Framework.DSCentral.DCentral.fill_TPI();
        }
    }
}

