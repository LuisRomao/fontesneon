﻿namespace Cadastros.Sindicos {


    partial class dSindicos        
    {
        private static dSindicos _dSindicosSt;

        /// <summary>
        /// dataset estático:dSindicos
        /// </summary>
        public static dSindicos dSindicosSt
        {
            get
            {
                if (_dSindicosSt == null)
                {
                    _dSindicosSt = new dSindicos();
                    _dSindicosSt.SindicosTableAdapter.Fill(_dSindicosSt.Sindicos,Framework.DSCentral.EMP);
                }
                return _dSindicosSt;
            }
        }


        private dSindicosTableAdapters.SindicosTableAdapter sindicosTableAdapter;
        /// <summary>
        /// TableAdapter padrão para tabela: Sindicos
        /// </summary>
        public dSindicosTableAdapters.SindicosTableAdapter SindicosTableAdapter
        {
            get
            {
                if (sindicosTableAdapter == null)
                {
                    sindicosTableAdapter = new dSindicosTableAdapters.SindicosTableAdapter();
                    sindicosTableAdapter.TrocarStringDeConexao();
                }; 
                return sindicosTableAdapter;
            }
        }
    }
}
