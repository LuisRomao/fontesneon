using System;
using System.Windows.Forms;

namespace Cadastros.Sindicos
{
    /// <summary>
    /// 
    /// </summary>
    public partial class cSindico : CompontesBasicos.ComponenteBase
    {
        /// <summary>
        /// 
        /// </summary>
        public cSindico()
        {
            InitializeComponent();
            USUbindingSource.DataSource = FrameworkProc.datasets.dUSUarios.dUSUariosSt;
            dSindicos.SindicosTableAdapter.Fill(dSindicos.Sindicos, Framework.DSCentral.EMP);
        }

        /// <summary>
        /// 
        /// </summary>
        public override void RefreshDados()
        {
            dSindicos.SindicosRow linhaMae = (dSindicos.SindicosRow)gridView1.GetDataRow(gridView1.FocusedRowHandle);
            int CON = linhaMae == null ? -1 : linhaMae.CON;
            dSindicos.SindicosTableAdapter.Fill(dSindicos.Sindicos, Framework.DSCentral.EMP);
            if (CON > 0)
                sindicosBindingSource.Position = sindicosBindingSource.Find("CON", CON);
        }

        private void repositoryItemButtonEdit1_Click(object sender, EventArgs e)
        {
            dSindicos.SindicosRow linhaMae = (dSindicos.SindicosRow)gridView1.GetDataRow(gridView1.FocusedRowHandle);
            if((linhaMae != null) && (!linhaMae.IsPESNull())){
                Pessoas.cPessoasCampos cadSindico = new Cadastros.Pessoas.cPessoasCampos();
                cadSindico.Fill(CompontesBasicos.TipoDeCarga.pk, linhaMae.PES, false);
                if (cadSindico.VirShowModulo(CompontesBasicos.EstadosDosComponentes.PopUp) == DialogResult.OK)
                    RefreshDados();
                //ShowModuloSTA(CompontesBasicos.EstadosDosComponentes.PopUp,

                /*
                Int64 CPF;
                if(VirInput.Input.Execute("CPF:",out CPF)){
                    DocBacarios.CPFCNPJ cpf = new DocBacarios.CPFCNPJ(CPF);
                    if (cpf.Tipo == DocBacarios.TipoCpfCnpj.CPF)
                    {
                        VirMSSQL.TableAdapter.STTableAdapter.ExecutarSQLNonQuery("update pessoas set PEScpfcnpj = @P1 where PES = @P2", cpf.ToString(), linhaMae.PES);
                        linhaMae.PESCpfCnpj = cpf.ToString();
                    }
                }
                */
            }
        }

        private void simpleButton1_Click(object sender, EventArgs e)
        {
            SaveFileDialog FD = new SaveFileDialog();
            FD.Filter = "*.xls|*.xls";
            if (FD.ShowDialog() == DialogResult.OK)
            {
                DevExpress.XtraPrinting.XlsExportOptions Ops = new DevExpress.XtraPrinting.XlsExportOptions();
                Ops.SheetName = "Sindicos";
                Ops.TextExportMode = DevExpress.XtraPrinting.TextExportMode.Value;
                gridView1.ExportToXls(FD.FileName, Ops);
            }
        }
    }
}

