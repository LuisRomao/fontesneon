using System;
using System.Data;
using System.Windows.Forms;
using CompontesBasicos;

namespace Cadastros.Cidades
{
    /// <summary>
    /// 
    /// </summary>
    public partial class cCidadesGrade : CompontesBasicos.ComponenteGradeNavegador
    {
        /// <summary>
        /// 
        /// </summary>
        public cCidadesGrade()
        {
            InitializeComponent();
        }
        
        private void cCidadesGrade_Load(object sender, EventArgs e)
        {
            //cIDADESTableAdapter.Fill(dCidadesGrade.CIDADES);
        }
        
        /*
        protected override bool ConfirmarRegistro_F()
        {
            cIDADESTableAdapter.Update(dCidadesGrade.CIDADES);
        }
        */

        /// <summary>
        /// 
        /// </summary>
        protected override void ExcluirRegistro_F()
        {
            int pk = Convert.ToInt32(((DataRowView)(BindingSource_F).Current)["CID"]);

            cIDADESTableAdapter.DeleteQuery(pk);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected override void GridView_F_ValidateRow(object sender, DevExpress.XtraGrid.Views.Base.ValidateRowEventArgs e)
        {
            if (GridView_F.GetFocusedRowCellDisplayText(colCIDNome).ToString().Trim() == "")
                GridView_F.SetColumnError(colCIDNome, "Campo obrigat�rio sem um valor informado.");
            
            if (GridView_F.GetFocusedRowCellDisplayText(colCIDUf).ToString().Trim() == "")
                GridView_F.SetColumnError(colCIDUf, "Campo obrigat�rio sem um valor informado.");

            DataRow[] _FoundRows = dCidadesGrade.CIDADES.Select("CIDNOME = '" + GridView_F.GetFocusedRowCellDisplayText(colCIDNome).ToString() + "' AND CIDUF = '" + GridView_F.GetFocusedRowCellDisplayText(colCIDUf).ToString() + "'");

            if (_FoundRows.Length > 0)
            {
                //MessageBox.Show("Foi encontrado um outro registro com os dados informados.", "Registro Duplicado", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                GridView_F.SetColumnError(colCIDNome, "Este campo n�o permite valor duplicado.");
                GridView_F.SetColumnError(colCIDUf, "Este campo n�o permite valor duplicado.");
            }

            e.Valid = (!GridView_F.HasColumnErrors);

        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected override void btnConfirmar_F_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            try
            {
                this.Validate();

                if (GridView_F.HasColumnErrors)
                    MessageBox.Show("O registro atual possui alertas que impedem que seja gravado.\r\nVerifique os alertas e tente gravar novamente.", "Gravar Registro", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                else
                {
                    if (DialogResult.Yes == MessageBox.Show("Confirma a grava��o?", "Confirmar", MessageBoxButtons.YesNo, MessageBoxIcon.Question))
                    {
                        this.Validate();
                        BindingSource_F.EndEdit();

                        cIDADESTableAdapter.Update(dCidadesGrade.CIDADES);

                        if (dCidadesGrade.HasChanges())
                            dCidadesGrade.AcceptChanges();

                        ShowToolBarByRowState(false);

                        if ((estado == EstadosDosComponentes.PopUp) && (tipoDeCarga == TipoDeCarga.novo))
                        {
                            Form formulario = (Form)this.Parent;
                            formulario.DialogResult = DialogResult.OK;
                        }

                    }
                }
            }
            catch (Exception Erro)
            {
                MessageBox.Show("Ocorreu um erro ao tentar gravar o registro.\r\n\r\nErro: " + Erro.Message);
            }
        }
    }
}

