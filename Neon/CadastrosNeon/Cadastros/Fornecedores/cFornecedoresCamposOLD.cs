using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace Cadastros.Fornecedores
{
    public partial class cFornecedoresCamposOLD : CompontesBasicos.ComponenteCamposGroupTabControl
    {
        public cFornecedoresCamposOLD()
        {
            InitializeComponent();
        }

        protected override void ConfirmarRegistro_F()
        {
            fORNECEDORESTableAdapter.Update(dFornecedoresCampos.FORNECEDORES);
        }

        private void cFornecedoresCampos_Load(object sender, EventArgs e)
        {
            cIDADESTableAdapter.Fill(dFornecedoresCampos.CIDADES);
        }
    }
}

