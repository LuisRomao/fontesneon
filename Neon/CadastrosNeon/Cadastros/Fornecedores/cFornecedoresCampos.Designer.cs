namespace Cadastros.Fornecedores
{
    partial class cFornecedoresCampos
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(cFornecedoresCampos));
            this.LayoutControl = new DevExpress.XtraLayout.LayoutControl();
            this.gridControl1 = new DevExpress.XtraGrid.GridControl();
            this.fKFRNxRAVFORNECEDORESBindingSource = new System.Windows.Forms.BindingSource();
            this.gridView1 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colRAV = new DevExpress.XtraGrid.Columns.GridColumn();
            this.LookUpEditRAV = new DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit();
            this.dRamoAtividadeGradeBindingSource = new System.Windows.Forms.BindingSource();
            this.dRamoAtividadeGrade = new Cadastros.RamoAtividade.dRamoAtividadeGrade();
            this.colDEL = new DevExpress.XtraGrid.Columns.GridColumn();
            this.ItemButtonDEL = new DevExpress.XtraEditors.Repository.RepositoryItemButtonEdit();
            this.fRNRetemINSSPrestadorCheckEdit = new DevExpress.XtraEditors.CheckEdit();
            this.fRNRetemINSSTomadorCheckEdit = new DevExpress.XtraEditors.CheckEdit();
            this.fRNRetemISSCheckEdit = new DevExpress.XtraEditors.CheckEdit();
            this.ComboContaTipoCredito = new DevExpress.XtraEditors.ImageComboBoxEdit();
            this.txtUsuario = new DevExpress.XtraEditors.TextEdit();
            this.textEdit20 = new DevExpress.XtraEditors.TextEdit();
            this.textEdit19 = new DevExpress.XtraEditors.TextEdit();
            this.textEdit18 = new DevExpress.XtraEditors.TextEdit();
            this.textEdit17 = new DevExpress.XtraEditors.TextEdit();
            this.textEdit16 = new DevExpress.XtraEditors.TextEdit();
            this.checkEdit2 = new DevExpress.XtraEditors.CheckEdit();
            this.textEdit15 = new DevExpress.XtraEditors.TextEdit();
            this.textEdit14 = new DevExpress.XtraEditors.TextEdit();
            this.calcEdit1 = new DevExpress.XtraEditors.CalcEdit();
            this.textEdit5 = new DevExpress.XtraEditors.TextEdit();
            this.checkEdit1 = new DevExpress.XtraEditors.CheckEdit();
            this.simpleButton1 = new DevExpress.XtraEditors.SimpleButton();
            this.textEdit13 = new DevExpress.XtraEditors.TextEdit();
            this.PIS_IM = new DevExpress.XtraEditors.TextEdit();
            this.txtFantasia = new DevExpress.XtraEditors.TextEdit();
            this.textEditCPFCNPJ = new DevExpress.XtraEditors.TextEdit();
            this.textEdit11 = new DevExpress.XtraEditors.TextEdit();
            this.textEdit10 = new DevExpress.XtraEditors.TextEdit();
            this.textEdit9 = new DevExpress.XtraEditors.TextEdit();
            this.textEdit8 = new DevExpress.XtraEditors.TextEdit();
            this.textEdit7 = new DevExpress.XtraEditors.TextEdit();
            this.textEdit6 = new DevExpress.XtraEditors.TextEdit();
            this.lkpCidades = new DevExpress.XtraEditors.LookUpEdit();
            this.cIDADESBindingSource = new System.Windows.Forms.BindingSource();
            this.lkpUF = new DevExpress.XtraEditors.LookUpEdit();
            this.textEdit4 = new DevExpress.XtraEditors.TextEdit();
            this.textEdit3 = new DevExpress.XtraEditors.TextEdit();
            this.textEdit2 = new DevExpress.XtraEditors.TextEdit();
            this.textEdit1 = new DevExpress.XtraEditors.TextEdit();
            this.SBProcuracao = new DevExpress.XtraEditors.SimpleButton();
            this.LayoutControlGroup = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlItem1 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem13 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem14 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem18 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem21 = new DevExpress.XtraLayout.LayoutControlItem();
            this.tabbedControlGroup1 = new DevExpress.XtraLayout.TabbedControlGroup();
            this.layoutControlGroup4 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.LCProcuracao = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem36 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlGroup1 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlGroup2 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlItem2 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem4 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem6 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem5 = new DevExpress.XtraLayout.LayoutControlItem();
            this.emptySpaceItem1 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.layoutControlItem3 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem17 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem20 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlGroup3 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlItem7 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem8 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem10 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem11 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem12 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem9 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem15 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem16 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem22 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlGroup5 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlGroup6 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlItem29 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem30 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem32 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem33 = new DevExpress.XtraLayout.LayoutControlItem();
            this.emptySpaceItem2 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.layoutControlItem28 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem35 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlGroup9 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlItem37 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem38 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem39 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem24 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem23 = new DevExpress.XtraLayout.LayoutControlItem();
            this.cntUsuario = new DevExpress.XtraLayout.LayoutControlItem();
            this.dFornecedoresCampos = new CadastrosProc.Fornecedores.dFornecedoresCampos();
            this.layoutControlItem19 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem31 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem25 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem34 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlGroup7 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlGroup8 = new DevExpress.XtraLayout.LayoutControlGroup();
            ((System.ComponentModel.ISupportInitialize)(this.BarManager_F)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.BarAndDockingController_F)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.BindingSource_F)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.StyleController_F)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LayoutControl)).BeginInit();
            this.LayoutControl.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.fKFRNxRAVFORNECEDORESBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LookUpEditRAV)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dRamoAtividadeGradeBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dRamoAtividadeGrade)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemButtonDEL)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.fRNRetemINSSPrestadorCheckEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.fRNRetemINSSTomadorCheckEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.fRNRetemISSCheckEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ComboContaTipoCredito.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtUsuario.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit20.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit19.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit18.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit17.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit16.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit2.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit15.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit14.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.calcEdit1.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit5.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit1.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit13.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.PIS_IM.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtFantasia.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEditCPFCNPJ.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit11.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit10.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit9.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit8.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit7.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit6.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lkpCidades.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cIDADESBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lkpUF.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit4.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit3.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit2.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit1.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LayoutControlGroup)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem13)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem14)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem18)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem21)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tabbedControlGroup1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LCProcuracao)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem36)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem17)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem20)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem8)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem10)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem11)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem12)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem9)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem15)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem16)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem22)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem29)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem30)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem32)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem33)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem28)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem35)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup9)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem37)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem38)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem39)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem24)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem23)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cntUsuario)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dFornecedoresCampos)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem19)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem31)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem25)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem34)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup8)).BeginInit();
            this.SuspendLayout();
            // 
            // barStatus_F
            // 
            this.barStatus_F.OptionsBar.AllowQuickCustomization = false;
            this.barStatus_F.OptionsBar.DisableClose = true;
            this.barStatus_F.OptionsBar.DisableCustomization = true;
            this.barStatus_F.OptionsBar.DrawDragBorder = false;
            this.barStatus_F.OptionsBar.DrawSizeGrip = true;
            this.barStatus_F.OptionsBar.UseWholeRow = true;
            // 
            // barComponente_F
            // 
            this.barComponente_F.OptionsBar.AllowQuickCustomization = false;
            this.barComponente_F.OptionsBar.DisableClose = true;
            this.barComponente_F.OptionsBar.DisableCustomization = true;
            this.barComponente_F.OptionsBar.DrawDragBorder = false;
            this.barComponente_F.OptionsBar.UseWholeRow = true;
            // 
            // btnConfirmar_F
            // 
            this.btnConfirmar_F.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("btnConfirmar_F.ImageOptions.Image")));
            // 
            // btnCancelar_F
            // 
            this.btnCancelar_F.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("btnCancelar_F.ImageOptions.Image")));
            // 
            // btnFechar_F
            // 
            this.btnFechar_F.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("btnFechar_F.ImageOptions.Image")));
            this.btnFechar_F.Visibility = DevExpress.XtraBars.BarItemVisibility.Never;
            // 
            // BarAndDockingController_F
            // 
            this.BarAndDockingController_F.PropertiesBar.AllowLinkLighting = false;
            this.BarAndDockingController_F.PropertiesBar.DefaultGlyphSize = new System.Drawing.Size(16, 16);
            this.BarAndDockingController_F.PropertiesBar.DefaultLargeGlyphSize = new System.Drawing.Size(32, 32);
            // 
            // BindingSource_F
            // 
            this.BindingSource_F.DataMember = "FORNECEDORES";
            this.BindingSource_F.DataSource = this.dFornecedoresCampos;
            // 
            // ToolTipController_F
            // 
            this.ToolTipController_F.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(222)))), ((int)(((byte)(237)))), ((int)(((byte)(250)))));
            this.ToolTipController_F.Appearance.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(20)))), ((int)(((byte)(69)))), ((int)(((byte)(141)))));
            this.ToolTipController_F.Appearance.Options.UseBackColor = true;
            this.ToolTipController_F.Appearance.Options.UseFont = true;
            this.ToolTipController_F.Appearance.Options.UseForeColor = true;
            this.ToolTipController_F.Appearance.Options.UseTextOptions = true;
            this.ToolTipController_F.Appearance.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            // 
            // StyleController_F
            // 
            this.StyleController_F.AppearanceDisabled.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.StyleController_F.AppearanceDisabled.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.StyleController_F.AppearanceDisabled.Options.UseBackColor = true;
            this.StyleController_F.AppearanceDisabled.Options.UseForeColor = true;
            this.StyleController_F.AppearanceFocused.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(253)))), ((int)(((byte)(253)))), ((int)(((byte)(181)))));
            this.StyleController_F.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
            this.StyleController_F.AppearanceFocused.Options.UseBackColor = true;
            this.StyleController_F.AppearanceFocused.Options.UseFont = true;
            // 
            // LayoutControl
            // 
            this.LayoutControl.AllowCustomization = false;
            this.LayoutControl.Controls.Add(this.gridControl1);
            this.LayoutControl.Controls.Add(this.fRNRetemINSSPrestadorCheckEdit);
            this.LayoutControl.Controls.Add(this.fRNRetemINSSTomadorCheckEdit);
            this.LayoutControl.Controls.Add(this.fRNRetemISSCheckEdit);
            this.LayoutControl.Controls.Add(this.ComboContaTipoCredito);
            this.LayoutControl.Controls.Add(this.txtUsuario);
            this.LayoutControl.Controls.Add(this.textEdit20);
            this.LayoutControl.Controls.Add(this.textEdit19);
            this.LayoutControl.Controls.Add(this.textEdit18);
            this.LayoutControl.Controls.Add(this.textEdit17);
            this.LayoutControl.Controls.Add(this.textEdit16);
            this.LayoutControl.Controls.Add(this.checkEdit2);
            this.LayoutControl.Controls.Add(this.textEdit15);
            this.LayoutControl.Controls.Add(this.textEdit14);
            this.LayoutControl.Controls.Add(this.calcEdit1);
            this.LayoutControl.Controls.Add(this.textEdit5);
            this.LayoutControl.Controls.Add(this.checkEdit1);
            this.LayoutControl.Controls.Add(this.simpleButton1);
            this.LayoutControl.Controls.Add(this.textEdit13);
            this.LayoutControl.Controls.Add(this.PIS_IM);
            this.LayoutControl.Controls.Add(this.txtFantasia);
            this.LayoutControl.Controls.Add(this.textEditCPFCNPJ);
            this.LayoutControl.Controls.Add(this.textEdit11);
            this.LayoutControl.Controls.Add(this.textEdit10);
            this.LayoutControl.Controls.Add(this.textEdit9);
            this.LayoutControl.Controls.Add(this.textEdit8);
            this.LayoutControl.Controls.Add(this.textEdit7);
            this.LayoutControl.Controls.Add(this.textEdit6);
            this.LayoutControl.Controls.Add(this.lkpCidades);
            this.LayoutControl.Controls.Add(this.lkpUF);
            this.LayoutControl.Controls.Add(this.textEdit4);
            this.LayoutControl.Controls.Add(this.textEdit3);
            this.LayoutControl.Controls.Add(this.textEdit2);
            this.LayoutControl.Controls.Add(this.textEdit1);
            this.LayoutControl.Controls.Add(this.SBProcuracao);
            this.LayoutControl.Cursor = System.Windows.Forms.Cursors.Arrow;
            this.LayoutControl.Dock = System.Windows.Forms.DockStyle.Fill;
            this.LayoutControl.Location = new System.Drawing.Point(0, 35);
            this.LayoutControl.Name = "LayoutControl";
            this.LayoutControl.OptionsCustomizationForm.DesignTimeCustomizationFormPositionAndSize = new System.Drawing.Rectangle(643, 373, 598, 350);
            this.LayoutControl.Root = this.LayoutControlGroup;
            this.LayoutControl.Size = new System.Drawing.Size(607, 620);
            this.LayoutControl.TabIndex = 0;
            // 
            // gridControl1
            // 
            this.gridControl1.DataMember = "FK_FRNxRAV_FORNECEDORES";
            this.gridControl1.DataSource = this.fKFRNxRAVFORNECEDORESBindingSource;
            this.gridControl1.Location = new System.Drawing.Point(24, 145);
            this.gridControl1.MainView = this.gridView1;
            this.gridControl1.MenuManager = this.BarManager_F;
            this.gridControl1.Name = "gridControl1";
            this.gridControl1.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.LookUpEditRAV,
            this.ItemButtonDEL});
            this.gridControl1.ShowOnlyPredefinedDetails = true;
            this.gridControl1.Size = new System.Drawing.Size(559, 422);
            this.gridControl1.TabIndex = 41;
            this.gridControl1.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridView1});
            // 
            // fKFRNxRAVFORNECEDORESBindingSource
            // 
            this.fKFRNxRAVFORNECEDORESBindingSource.DataSource = this.BindingSource_F;
            // 
            // gridView1
            // 
            this.gridView1.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colRAV,
            this.colDEL});
            this.gridView1.GridControl = this.gridControl1;
            this.gridView1.Name = "gridView1";
            this.gridView1.NewItemRowText = "INCLUIR RAMO DE ATIVIDADE";
            this.gridView1.OptionsView.NewItemRowPosition = DevExpress.XtraGrid.Views.Grid.NewItemRowPosition.Top;
            this.gridView1.OptionsView.ShowColumnHeaders = false;
            this.gridView1.OptionsView.ShowGroupPanel = false;
            this.gridView1.CustomRowCellEdit += new DevExpress.XtraGrid.Views.Grid.CustomRowCellEditEventHandler(this.gridView1_CustomRowCellEdit);
            this.gridView1.FocusedRowChanged += new DevExpress.XtraGrid.Views.Base.FocusedRowChangedEventHandler(this.gridView1_FocusedRowChanged);
            this.gridView1.ValidateRow += new DevExpress.XtraGrid.Views.Base.ValidateRowEventHandler(this.gridView1_ValidateRow);
            this.gridView1.RowUpdated += new DevExpress.XtraGrid.Views.Base.RowObjectEventHandler(this.gridView1_RowUpdated);
            // 
            // colRAV
            // 
            this.colRAV.ColumnEdit = this.LookUpEditRAV;
            this.colRAV.FieldName = "RAV";
            this.colRAV.Name = "colRAV";
            this.colRAV.OptionsColumn.ShowCaption = false;
            this.colRAV.Visible = true;
            this.colRAV.VisibleIndex = 0;
            this.colRAV.Width = 464;
            // 
            // LookUpEditRAV
            // 
            this.LookUpEditRAV.AutoHeight = false;
            this.LookUpEditRAV.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LookUpEditRAV.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("RAVDescricao", "RAV Descricao", 79, DevExpress.Utils.FormatType.None, "", true, DevExpress.Utils.HorzAlignment.Near, DevExpress.Data.ColumnSortOrder.None, DevExpress.Utils.DefaultBoolean.Default)});
            this.LookUpEditRAV.DataSource = this.dRamoAtividadeGradeBindingSource;
            this.LookUpEditRAV.DisplayMember = "RAVDescricao";
            this.LookUpEditRAV.Name = "LookUpEditRAV";
            this.LookUpEditRAV.NullText = " -- ";
            this.LookUpEditRAV.PopupFilterMode = DevExpress.XtraEditors.PopupFilterMode.Contains;
            this.LookUpEditRAV.ShowHeader = false;
            this.LookUpEditRAV.ValueMember = "RAV";
            // 
            // dRamoAtividadeGradeBindingSource
            // 
            this.dRamoAtividadeGradeBindingSource.DataMember = "RamoAtiVidade";
            this.dRamoAtividadeGradeBindingSource.DataSource = this.dRamoAtividadeGrade;
            // 
            // dRamoAtividadeGrade
            // 
            this.dRamoAtividadeGrade.DataSetName = "dRamoAtividadeGrade";
            this.dRamoAtividadeGrade.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // colDEL
            // 
            this.colDEL.Caption = "gridColumn1";
            this.colDEL.Fixed = DevExpress.XtraGrid.Columns.FixedStyle.Right;
            this.colDEL.Name = "colDEL";
            this.colDEL.Visible = true;
            this.colDEL.VisibleIndex = 1;
            this.colDEL.Width = 30;
            // 
            // ItemButtonDEL
            // 
            this.ItemButtonDEL.AutoHeight = false;
            this.ItemButtonDEL.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Delete)});
            this.ItemButtonDEL.Name = "ItemButtonDEL";
            this.ItemButtonDEL.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.HideTextEditor;
            this.ItemButtonDEL.ButtonClick += new DevExpress.XtraEditors.Controls.ButtonPressedEventHandler(this.ItemButtonDEL_ButtonClick);
            // 
            // fRNRetemINSSPrestadorCheckEdit
            // 
            this.fRNRetemINSSPrestadorCheckEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.BindingSource_F, "FRNRetemINSSPrestador", true));
            this.fRNRetemINSSPrestadorCheckEdit.Location = new System.Drawing.Point(24, 191);
            this.fRNRetemINSSPrestadorCheckEdit.MenuManager = this.BarManager_F;
            this.fRNRetemINSSPrestadorCheckEdit.Name = "fRNRetemINSSPrestadorCheckEdit";
            this.fRNRetemINSSPrestadorCheckEdit.Properties.Caption = "Reter INSS Prestador";
            this.fRNRetemINSSPrestadorCheckEdit.Size = new System.Drawing.Size(559, 19);
            this.fRNRetemINSSPrestadorCheckEdit.StyleController = this.LayoutControl;
            this.fRNRetemINSSPrestadorCheckEdit.TabIndex = 39;
            // 
            // fRNRetemINSSTomadorCheckEdit
            // 
            this.fRNRetemINSSTomadorCheckEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.BindingSource_F, "FRNRetemINSSTomador", true));
            this.fRNRetemINSSTomadorCheckEdit.Location = new System.Drawing.Point(24, 168);
            this.fRNRetemINSSTomadorCheckEdit.MenuManager = this.BarManager_F;
            this.fRNRetemINSSTomadorCheckEdit.Name = "fRNRetemINSSTomadorCheckEdit";
            this.fRNRetemINSSTomadorCheckEdit.Properties.Caption = "Reter INSS Tomador";
            this.fRNRetemINSSTomadorCheckEdit.Size = new System.Drawing.Size(559, 19);
            this.fRNRetemINSSTomadorCheckEdit.StyleController = this.LayoutControl;
            this.fRNRetemINSSTomadorCheckEdit.TabIndex = 38;
            // 
            // fRNRetemISSCheckEdit
            // 
            this.fRNRetemISSCheckEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.BindingSource_F, "FRNRetemISS", true));
            this.fRNRetemISSCheckEdit.Location = new System.Drawing.Point(24, 145);
            this.fRNRetemISSCheckEdit.MenuManager = this.BarManager_F;
            this.fRNRetemISSCheckEdit.Name = "fRNRetemISSCheckEdit";
            this.fRNRetemISSCheckEdit.Properties.Caption = "Reter ISS";
            this.fRNRetemISSCheckEdit.Size = new System.Drawing.Size(559, 19);
            this.fRNRetemISSCheckEdit.StyleController = this.LayoutControl;
            this.fRNRetemISSCheckEdit.TabIndex = 37;
            // 
            // ComboContaTipoCredito
            // 
            this.ComboContaTipoCredito.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.BindingSource_F, "FRNContaTipoCredito", true));
            this.ComboContaTipoCredito.Location = new System.Drawing.Point(141, 175);
            this.ComboContaTipoCredito.MenuManager = this.BarManager_F;
            this.ComboContaTipoCredito.Name = "ComboContaTipoCredito";
            this.ComboContaTipoCredito.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.ComboContaTipoCredito.Size = new System.Drawing.Size(430, 20);
            this.ComboContaTipoCredito.StyleController = this.LayoutControl;
            this.ComboContaTipoCredito.TabIndex = 24;
            // 
            // txtUsuario
            // 
            this.txtUsuario.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.BindingSource_F, "FRN", true));
            this.txtUsuario.Enabled = false;
            this.txtUsuario.Location = new System.Drawing.Point(451, 87);
            this.txtUsuario.MenuManager = this.BarManager_F;
            this.txtUsuario.Name = "txtUsuario";
            this.txtUsuario.Properties.MaxLength = 6;
            this.txtUsuario.Size = new System.Drawing.Size(144, 20);
            this.txtUsuario.StyleController = this.LayoutControl;
            this.txtUsuario.TabIndex = 6;
            this.txtUsuario.TabStop = false;
            // 
            // textEdit20
            // 
            this.textEdit20.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.BindingSource_F, "FRNContaDgCredito", true));
            this.textEdit20.Location = new System.Drawing.Point(280, 247);
            this.textEdit20.MaximumSize = new System.Drawing.Size(50, 0);
            this.textEdit20.MenuManager = this.BarManager_F;
            this.textEdit20.Name = "textEdit20";
            this.textEdit20.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.True;
            this.textEdit20.Properties.MaxLength = 1;
            this.textEdit20.Size = new System.Drawing.Size(50, 20);
            this.textEdit20.StyleController = this.LayoutControl;
            this.textEdit20.TabIndex = 29;
            // 
            // textEdit19
            // 
            this.textEdit19.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.BindingSource_F, "FRNContaCredito", true));
            this.textEdit19.Location = new System.Drawing.Point(141, 247);
            this.textEdit19.MaximumSize = new System.Drawing.Size(100, 0);
            this.textEdit19.MenuManager = this.BarManager_F;
            this.textEdit19.Name = "textEdit19";
            this.textEdit19.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.True;
            this.textEdit19.Properties.MaxLength = 8;
            this.textEdit19.Size = new System.Drawing.Size(100, 20);
            this.textEdit19.StyleController = this.LayoutControl;
            this.textEdit19.TabIndex = 28;
            // 
            // textEdit18
            // 
            this.textEdit18.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.BindingSource_F, "FRNAgenciaDgCredito", true));
            this.textEdit18.Location = new System.Drawing.Point(280, 223);
            this.textEdit18.MaximumSize = new System.Drawing.Size(50, 0);
            this.textEdit18.MenuManager = this.BarManager_F;
            this.textEdit18.Name = "textEdit18";
            this.textEdit18.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.True;
            this.textEdit18.Properties.MaxLength = 1;
            this.textEdit18.Size = new System.Drawing.Size(50, 20);
            this.textEdit18.StyleController = this.LayoutControl;
            this.textEdit18.TabIndex = 27;
            // 
            // textEdit17
            // 
            this.textEdit17.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.BindingSource_F, "FRNAgenciaCredito", true));
            this.textEdit17.Location = new System.Drawing.Point(141, 223);
            this.textEdit17.MaximumSize = new System.Drawing.Size(100, 0);
            this.textEdit17.MenuManager = this.BarManager_F;
            this.textEdit17.Name = "textEdit17";
            this.textEdit17.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.True;
            this.textEdit17.Properties.MaxLength = 5;
            this.textEdit17.Size = new System.Drawing.Size(100, 20);
            this.textEdit17.StyleController = this.LayoutControl;
            this.textEdit17.TabIndex = 26;
            // 
            // textEdit16
            // 
            this.textEdit16.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.BindingSource_F, "FRNBancoCredito", true));
            this.textEdit16.Location = new System.Drawing.Point(141, 199);
            this.textEdit16.MaximumSize = new System.Drawing.Size(100, 0);
            this.textEdit16.MenuManager = this.BarManager_F;
            this.textEdit16.Name = "textEdit16";
            this.textEdit16.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.True;
            this.textEdit16.Properties.MaxLength = 3;
            this.textEdit16.Size = new System.Drawing.Size(100, 20);
            this.textEdit16.StyleController = this.LayoutControl;
            this.textEdit16.TabIndex = 25;
            // 
            // checkEdit2
            // 
            this.checkEdit2.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.BindingSource_F, "FRNAtivoSite", true));
            this.checkEdit2.Location = new System.Drawing.Point(12, 87);
            this.checkEdit2.Name = "checkEdit2";
            this.checkEdit2.Properties.Caption = "Consta no Cadastro de Fornecedores do Site";
            this.checkEdit2.Size = new System.Drawing.Size(380, 19);
            this.checkEdit2.StyleController = this.LayoutControl;
            this.checkEdit2.TabIndex = 5;
            this.checkEdit2.TabStop = false;
            // 
            // textEdit15
            // 
            this.textEdit15.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.BindingSource_F, "FRNSenha", true));
            this.textEdit15.Location = new System.Drawing.Point(451, 63);
            this.textEdit15.MenuManager = this.BarManager_F;
            this.textEdit15.Name = "textEdit15";
            this.textEdit15.Properties.MaxLength = 6;
            this.textEdit15.Size = new System.Drawing.Size(144, 20);
            this.textEdit15.StyleController = this.LayoutControl;
            this.textEdit15.TabIndex = 6;
            // 
            // textEdit14
            // 
            this.textEdit14.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.BindingSource_F, "FRNIE", true));
            this.textEdit14.Location = new System.Drawing.Point(92, 435);
            this.textEdit14.MenuManager = this.BarManager_F;
            this.textEdit14.Name = "textEdit14";
            this.textEdit14.Size = new System.Drawing.Size(212, 20);
            this.textEdit14.StyleController = this.LayoutControl;
            this.textEdit14.TabIndex = 21;
            // 
            // calcEdit1
            // 
            this.calcEdit1.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.BindingSource_F, "FRNSimplesAliquota", true));
            this.calcEdit1.Location = new System.Drawing.Point(184, 63);
            this.calcEdit1.MenuManager = this.BarManager_F;
            this.calcEdit1.Name = "calcEdit1";
            this.calcEdit1.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.calcEdit1.Size = new System.Drawing.Size(208, 20);
            this.calcEdit1.StyleController = this.LayoutControl;
            this.calcEdit1.TabIndex = 4;
            // 
            // textEdit5
            // 
            this.textEdit5.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.BindingSource_F, "FRNNumero", true));
            this.textEdit5.Location = new System.Drawing.Point(364, 225);
            this.textEdit5.MenuManager = this.BarManager_F;
            this.textEdit5.Name = "textEdit5";
            this.textEdit5.Size = new System.Drawing.Size(207, 20);
            this.textEdit5.StyleController = this.LayoutControl;
            this.textEdit5.TabIndex = 11;
            // 
            // checkEdit1
            // 
            this.checkEdit1.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.BindingSource_F, "FRNSimples", true));
            this.checkEdit1.Location = new System.Drawing.Point(12, 63);
            this.checkEdit1.Name = "checkEdit1";
            this.checkEdit1.Properties.Caption = "Simples";
            this.checkEdit1.Size = new System.Drawing.Size(112, 19);
            this.checkEdit1.StyleController = this.LayoutControl;
            this.checkEdit1.TabIndex = 3;
            this.checkEdit1.TabStop = false;
            // 
            // simpleButton1
            // 
            this.simpleButton1.Location = new System.Drawing.Point(303, 175);
            this.simpleButton1.Name = "simpleButton1";
            this.simpleButton1.Size = new System.Drawing.Size(108, 22);
            this.simpleButton1.StyleController = this.LayoutControl;
            this.simpleButton1.TabIndex = 8;
            this.simpleButton1.Text = "Buscar Endere�o";
            this.simpleButton1.Click += new System.EventHandler(this.simpleButton1_Click);
            // 
            // textEdit13
            // 
            this.textEdit13.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.BindingSource_F, "FRNRegistroUF", true));
            this.textEdit13.Location = new System.Drawing.Point(364, 411);
            this.textEdit13.Name = "textEdit13";
            this.textEdit13.Size = new System.Drawing.Size(207, 20);
            this.textEdit13.StyleController = this.LayoutControl;
            this.textEdit13.TabIndex = 22;
            // 
            // PIS_IM
            // 
            this.PIS_IM.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.BindingSource_F, "FRNRegistro", true));
            this.PIS_IM.Location = new System.Drawing.Point(92, 411);
            this.PIS_IM.Name = "PIS_IM";
            this.PIS_IM.Properties.Mask.EditMask = "000\\.00000\\.00-0";
            this.PIS_IM.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.PIS_IM.Size = new System.Drawing.Size(212, 20);
            this.PIS_IM.StyleController = this.LayoutControl;
            this.PIS_IM.TabIndex = 20;
            this.PIS_IM.Validating += new System.ComponentModel.CancelEventHandler(this.textEdit5_Validating);
            // 
            // txtFantasia
            // 
            this.txtFantasia.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.BindingSource_F, "FRNFantasia", true));
            this.txtFantasia.Location = new System.Drawing.Point(68, 39);
            this.txtFantasia.Name = "txtFantasia";
            this.txtFantasia.Size = new System.Drawing.Size(324, 20);
            this.txtFantasia.StyleController = this.LayoutControl;
            this.txtFantasia.TabIndex = 1;
            // 
            // textEditCPFCNPJ
            // 
            this.textEditCPFCNPJ.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.BindingSource_F, "FRNCnpj", true));
            this.textEditCPFCNPJ.Location = new System.Drawing.Point(431, 39);
            this.textEditCPFCNPJ.Name = "textEditCPFCNPJ";
            this.textEditCPFCNPJ.Properties.Mask.PlaceHolder = ' ';
            this.textEditCPFCNPJ.Properties.ReadOnly = true;
            this.textEditCPFCNPJ.Size = new System.Drawing.Size(164, 20);
            this.textEditCPFCNPJ.StyleController = this.LayoutControl;
            this.textEditCPFCNPJ.TabIndex = 2;
            this.textEditCPFCNPJ.Validating += new System.ComponentModel.CancelEventHandler(this.textEdit12_Validating);
            // 
            // textEdit11
            // 
            this.textEdit11.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.BindingSource_F, "FRNFone4", true));
            this.textEdit11.Location = new System.Drawing.Point(364, 339);
            this.textEdit11.Name = "textEdit11";
            this.textEdit11.Size = new System.Drawing.Size(207, 20);
            this.textEdit11.StyleController = this.LayoutControl;
            this.textEdit11.TabIndex = 17;
            // 
            // textEdit10
            // 
            this.textEdit10.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.BindingSource_F, "FRNFone3", true));
            this.textEdit10.Location = new System.Drawing.Point(92, 339);
            this.textEdit10.Name = "textEdit10";
            this.textEdit10.Size = new System.Drawing.Size(212, 20);
            this.textEdit10.StyleController = this.LayoutControl;
            this.textEdit10.TabIndex = 16;
            // 
            // textEdit9
            // 
            this.textEdit9.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.BindingSource_F, "FRNEmail", true));
            this.textEdit9.Location = new System.Drawing.Point(92, 363);
            this.textEdit9.Name = "textEdit9";
            this.textEdit9.Size = new System.Drawing.Size(479, 20);
            this.textEdit9.StyleController = this.LayoutControl;
            this.textEdit9.TabIndex = 18;
            // 
            // textEdit8
            // 
            this.textEdit8.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.BindingSource_F, "FRNSite", true));
            this.textEdit8.Location = new System.Drawing.Point(92, 387);
            this.textEdit8.Name = "textEdit8";
            this.textEdit8.Size = new System.Drawing.Size(479, 20);
            this.textEdit8.StyleController = this.LayoutControl;
            this.textEdit8.TabIndex = 19;
            // 
            // textEdit7
            // 
            this.textEdit7.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.BindingSource_F, "FRNFone2", true));
            this.textEdit7.Location = new System.Drawing.Point(364, 315);
            this.textEdit7.Name = "textEdit7";
            this.textEdit7.Size = new System.Drawing.Size(207, 20);
            this.textEdit7.StyleController = this.LayoutControl;
            this.textEdit7.TabIndex = 15;
            // 
            // textEdit6
            // 
            this.textEdit6.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.BindingSource_F, "FRNFone1", true));
            this.textEdit6.Location = new System.Drawing.Point(92, 315);
            this.textEdit6.Name = "textEdit6";
            this.textEdit6.Size = new System.Drawing.Size(212, 20);
            this.textEdit6.StyleController = this.LayoutControl;
            this.textEdit6.TabIndex = 14;
            // 
            // lkpCidades
            // 
            this.lkpCidades.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.BindingSource_F, "FRN_CID", true));
            this.lkpCidades.Location = new System.Drawing.Point(92, 249);
            this.lkpCidades.Name = "lkpCidades";
            this.lkpCidades.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo),
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Plus)});
            this.lkpCidades.Properties.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("CIDNome", "Nome", 64, DevExpress.Utils.FormatType.None, "", true, DevExpress.Utils.HorzAlignment.Near, DevExpress.Data.ColumnSortOrder.None, DevExpress.Utils.DefaultBoolean.Default),
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("CIDUf", "UF")});
            this.lkpCidades.Properties.DataSource = this.cIDADESBindingSource;
            this.lkpCidades.Properties.DisplayMember = "CIDNome";
            this.lkpCidades.Properties.ValueMember = "CID";
            this.lkpCidades.Size = new System.Drawing.Size(335, 20);
            this.lkpCidades.StyleController = this.LayoutControl;
            this.lkpCidades.TabIndex = 12;
            this.lkpCidades.TabStop = false;
            this.lkpCidades.ButtonClick += new DevExpress.XtraEditors.Controls.ButtonPressedEventHandler(this.lkpCidades_ButtonClick);
            this.lkpCidades.EditValueChanged += new System.EventHandler(this.lkpCidades_EditValueChanged);
            // 
            // cIDADESBindingSource
            // 
            this.cIDADESBindingSource.DataMember = "CIDADES";
            this.cIDADESBindingSource.DataSource = typeof(Framework.datasets.dCIDADES);
            // 
            // lkpUF
            // 
            this.lkpUF.Enabled = false;
            this.lkpUF.Location = new System.Drawing.Point(452, 249);
            this.lkpUF.Name = "lkpUF";
            this.lkpUF.Properties.DataSource = this.cIDADESBindingSource;
            this.lkpUF.Properties.DisplayMember = "CIDUf";
            this.lkpUF.Properties.ValueMember = "CID";
            this.lkpUF.Size = new System.Drawing.Size(119, 20);
            this.lkpUF.StyleController = this.LayoutControl;
            this.lkpUF.TabIndex = 13;
            this.lkpUF.TabStop = false;
            // 
            // textEdit4
            // 
            this.textEdit4.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.BindingSource_F, "FRNBairro", true));
            this.textEdit4.EditValue = "";
            this.textEdit4.Location = new System.Drawing.Point(92, 225);
            this.textEdit4.Name = "textEdit4";
            this.textEdit4.Size = new System.Drawing.Size(212, 20);
            this.textEdit4.StyleController = this.LayoutControl;
            this.textEdit4.TabIndex = 10;
            this.textEdit4.TabStop = false;
            // 
            // textEdit3
            // 
            this.textEdit3.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.BindingSource_F, "FRNEndereco", true));
            this.textEdit3.Location = new System.Drawing.Point(92, 201);
            this.textEdit3.Name = "textEdit3";
            this.textEdit3.Size = new System.Drawing.Size(479, 20);
            this.textEdit3.StyleController = this.LayoutControl;
            this.textEdit3.TabIndex = 9;
            // 
            // textEdit2
            // 
            this.textEdit2.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.BindingSource_F, "FRNCep", true));
            this.textEdit2.Location = new System.Drawing.Point(92, 175);
            this.textEdit2.Name = "textEdit2";
            this.textEdit2.Properties.Mask.PlaceHolder = ' ';
            this.textEdit2.Size = new System.Drawing.Size(207, 20);
            this.textEdit2.StyleController = this.LayoutControl;
            this.textEdit2.TabIndex = 7;
            // 
            // textEdit1
            // 
            this.textEdit1.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.BindingSource_F, "FRNNome", true));
            this.textEdit1.Location = new System.Drawing.Point(68, 15);
            this.textEdit1.Name = "textEdit1";
            this.textEdit1.Size = new System.Drawing.Size(527, 20);
            this.textEdit1.StyleController = this.LayoutControl;
            this.textEdit1.TabIndex = 0;
            // 
            // SBProcuracao
            // 
            this.SBProcuracao.Location = new System.Drawing.Point(24, 571);
            this.SBProcuracao.Name = "SBProcuracao";
            this.SBProcuracao.Size = new System.Drawing.Size(559, 22);
            this.SBProcuracao.StyleController = this.LayoutControl;
            this.SBProcuracao.TabIndex = 40;
            this.SBProcuracao.Text = "Procura��o";
            this.SBProcuracao.Visible = false;
            this.SBProcuracao.Click += new System.EventHandler(this.simpleButton2_Click);
            // 
            // LayoutControlGroup
            // 
            this.LayoutControlGroup.CustomizationFormText = "Root";
            this.LayoutControlGroup.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItem1,
            this.layoutControlItem13,
            this.layoutControlItem14,
            this.layoutControlItem18,
            this.layoutControlItem21,
            this.tabbedControlGroup1,
            this.layoutControlItem24,
            this.layoutControlItem23,
            this.cntUsuario});
            this.LayoutControlGroup.Name = "Root";
            this.LayoutControlGroup.OptionsItemText.TextToControlDistance = 5;
            this.LayoutControlGroup.Size = new System.Drawing.Size(607, 620);
            this.LayoutControlGroup.Spacing = new DevExpress.XtraLayout.Utils.Padding(0, 0, 3, 3);
            this.LayoutControlGroup.TextVisible = false;
            // 
            // layoutControlItem1
            // 
            this.layoutControlItem1.Control = this.textEdit1;
            this.layoutControlItem1.CustomizationFormText = "Nome";
            this.layoutControlItem1.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItem1.Name = "layoutControlItem1";
            this.layoutControlItem1.Size = new System.Drawing.Size(587, 24);
            this.layoutControlItem1.Text = "Nome";
            this.layoutControlItem1.TextSize = new System.Drawing.Size(51, 13);
            // 
            // layoutControlItem13
            // 
            this.layoutControlItem13.Control = this.textEditCPFCNPJ;
            this.layoutControlItem13.CustomizationFormText = "CNPJ";
            this.layoutControlItem13.Location = new System.Drawing.Point(384, 24);
            this.layoutControlItem13.Name = "layoutControlItem13";
            this.layoutControlItem13.Size = new System.Drawing.Size(203, 24);
            this.layoutControlItem13.Text = "CNPJ";
            this.layoutControlItem13.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem13.TextSize = new System.Drawing.Size(30, 20);
            this.layoutControlItem13.TextToControlDistance = 5;
            // 
            // layoutControlItem14
            // 
            this.layoutControlItem14.Control = this.txtFantasia;
            this.layoutControlItem14.CustomizationFormText = "Fantasia";
            this.layoutControlItem14.Location = new System.Drawing.Point(0, 24);
            this.layoutControlItem14.Name = "layoutControlItem14";
            this.layoutControlItem14.Size = new System.Drawing.Size(384, 24);
            this.layoutControlItem14.Text = "Fantasia";
            this.layoutControlItem14.TextSize = new System.Drawing.Size(51, 13);
            // 
            // layoutControlItem18
            // 
            this.layoutControlItem18.Control = this.checkEdit1;
            this.layoutControlItem18.CustomizationFormText = "layoutControlItem18";
            this.layoutControlItem18.Location = new System.Drawing.Point(0, 48);
            this.layoutControlItem18.Name = "layoutControlItem18";
            this.layoutControlItem18.Size = new System.Drawing.Size(116, 24);
            this.layoutControlItem18.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem18.TextVisible = false;
            // 
            // layoutControlItem21
            // 
            this.layoutControlItem21.Control = this.calcEdit1;
            this.layoutControlItem21.CustomizationFormText = "Aliquota";
            this.layoutControlItem21.Location = new System.Drawing.Point(116, 48);
            this.layoutControlItem21.Name = "layoutControlItem21";
            this.layoutControlItem21.Size = new System.Drawing.Size(268, 24);
            this.layoutControlItem21.Text = "Aliquota";
            this.layoutControlItem21.TextSize = new System.Drawing.Size(51, 13);
            // 
            // tabbedControlGroup1
            // 
            this.tabbedControlGroup1.CustomizationFormText = "tabbedControlGroup1";
            this.tabbedControlGroup1.Location = new System.Drawing.Point(0, 96);
            this.tabbedControlGroup1.Name = "tabbedControlGroup1";
            this.tabbedControlGroup1.SelectedTabPage = this.layoutControlGroup1;
            this.tabbedControlGroup1.SelectedTabPageIndex = 0;
            this.tabbedControlGroup1.Size = new System.Drawing.Size(587, 498);
            this.tabbedControlGroup1.TabPages.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlGroup1,
            this.layoutControlGroup4,
            this.layoutControlGroup5,
            this.layoutControlGroup9});
            // 
            // layoutControlGroup4
            // 
            this.layoutControlGroup4.CustomizationFormText = "&2-Ramos de Atividade";
            this.layoutControlGroup4.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.LCProcuracao,
            this.layoutControlItem36});
            this.layoutControlGroup4.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroup4.Name = "layoutControlGroup4";
            this.layoutControlGroup4.Size = new System.Drawing.Size(563, 452);
            this.layoutControlGroup4.Text = "&2-Ramos de Atividade";
            // 
            // LCProcuracao
            // 
            this.LCProcuracao.Control = this.SBProcuracao;
            this.LCProcuracao.Location = new System.Drawing.Point(0, 426);
            this.LCProcuracao.Name = "LCProcuracao";
            this.LCProcuracao.Size = new System.Drawing.Size(563, 26);
            this.LCProcuracao.TextSize = new System.Drawing.Size(0, 0);
            this.LCProcuracao.TextVisible = false;
            this.LCProcuracao.Visibility = DevExpress.XtraLayout.Utils.LayoutVisibility.Never;
            // 
            // layoutControlItem36
            // 
            this.layoutControlItem36.Control = this.gridControl1;
            this.layoutControlItem36.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItem36.Name = "layoutControlItem36";
            this.layoutControlItem36.Size = new System.Drawing.Size(563, 426);
            this.layoutControlItem36.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem36.TextVisible = false;
            // 
            // layoutControlGroup1
            // 
            this.layoutControlGroup1.CustomizationFormText = "&1-B�sico";
            this.layoutControlGroup1.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlGroup2,
            this.layoutControlGroup3});
            this.layoutControlGroup1.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroup1.Name = "layoutControlGroup1";
            this.layoutControlGroup1.Size = new System.Drawing.Size(563, 452);
            this.layoutControlGroup1.Text = "&1-B�sico";
            // 
            // layoutControlGroup2
            // 
            this.layoutControlGroup2.CustomizationFormText = ":: Endere�o ::";
            this.layoutControlGroup2.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItem2,
            this.layoutControlItem4,
            this.layoutControlItem6,
            this.layoutControlItem5,
            this.emptySpaceItem1,
            this.layoutControlItem3,
            this.layoutControlItem17,
            this.layoutControlItem20});
            this.layoutControlGroup2.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroup2.Name = "layoutControlGroup2";
            this.layoutControlGroup2.OptionsItemText.TextToControlDistance = 5;
            this.layoutControlGroup2.Size = new System.Drawing.Size(563, 140);
            this.layoutControlGroup2.Text = ":: Endere�o ::";
            // 
            // layoutControlItem2
            // 
            this.layoutControlItem2.Control = this.textEdit2;
            this.layoutControlItem2.CustomizationFormText = "CEP";
            this.layoutControlItem2.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItem2.Name = "layoutControlItem2";
            this.layoutControlItem2.Size = new System.Drawing.Size(267, 26);
            this.layoutControlItem2.Text = "CEP";
            this.layoutControlItem2.TextSize = new System.Drawing.Size(51, 13);
            // 
            // layoutControlItem4
            // 
            this.layoutControlItem4.Control = this.textEdit4;
            this.layoutControlItem4.CustomizationFormText = "Bairro";
            this.layoutControlItem4.Location = new System.Drawing.Point(0, 50);
            this.layoutControlItem4.Name = "layoutControlItem4";
            this.layoutControlItem4.Size = new System.Drawing.Size(272, 24);
            this.layoutControlItem4.Text = "Bairro";
            this.layoutControlItem4.TextSize = new System.Drawing.Size(51, 13);
            // 
            // layoutControlItem6
            // 
            this.layoutControlItem6.Control = this.lkpCidades;
            this.layoutControlItem6.CustomizationFormText = "Cidade";
            this.layoutControlItem6.Location = new System.Drawing.Point(0, 74);
            this.layoutControlItem6.Name = "layoutControlItem6";
            this.layoutControlItem6.Size = new System.Drawing.Size(395, 24);
            this.layoutControlItem6.Text = "Cidade";
            this.layoutControlItem6.TextSize = new System.Drawing.Size(51, 13);
            // 
            // layoutControlItem5
            // 
            this.layoutControlItem5.Control = this.lkpUF;
            this.layoutControlItem5.CustomizationFormText = "UF";
            this.layoutControlItem5.Location = new System.Drawing.Point(395, 74);
            this.layoutControlItem5.Name = "layoutControlItem5";
            this.layoutControlItem5.Size = new System.Drawing.Size(144, 24);
            this.layoutControlItem5.Text = "UF";
            this.layoutControlItem5.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem5.TextSize = new System.Drawing.Size(16, 20);
            this.layoutControlItem5.TextToControlDistance = 5;
            // 
            // emptySpaceItem1
            // 
            this.emptySpaceItem1.AllowHotTrack = false;
            this.emptySpaceItem1.CustomizationFormText = "emptySpaceItem1";
            this.emptySpaceItem1.Location = new System.Drawing.Point(379, 0);
            this.emptySpaceItem1.Name = "emptySpaceItem1";
            this.emptySpaceItem1.Size = new System.Drawing.Size(160, 26);
            this.emptySpaceItem1.TextSize = new System.Drawing.Size(0, 0);
            // 
            // layoutControlItem3
            // 
            this.layoutControlItem3.Control = this.textEdit3;
            this.layoutControlItem3.CustomizationFormText = "Endere�o";
            this.layoutControlItem3.Location = new System.Drawing.Point(0, 26);
            this.layoutControlItem3.Name = "layoutControlItem3";
            this.layoutControlItem3.Size = new System.Drawing.Size(539, 24);
            this.layoutControlItem3.Text = "Endere�o";
            this.layoutControlItem3.TextSize = new System.Drawing.Size(51, 13);
            // 
            // layoutControlItem17
            // 
            this.layoutControlItem17.Control = this.simpleButton1;
            this.layoutControlItem17.CustomizationFormText = "layoutControlItem17";
            this.layoutControlItem17.Location = new System.Drawing.Point(267, 0);
            this.layoutControlItem17.Name = "layoutControlItem17";
            this.layoutControlItem17.Size = new System.Drawing.Size(112, 26);
            this.layoutControlItem17.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem17.TextVisible = false;
            // 
            // layoutControlItem20
            // 
            this.layoutControlItem20.Control = this.textEdit5;
            this.layoutControlItem20.CustomizationFormText = "N�";
            this.layoutControlItem20.Location = new System.Drawing.Point(272, 50);
            this.layoutControlItem20.Name = "layoutControlItem20";
            this.layoutControlItem20.Size = new System.Drawing.Size(267, 24);
            this.layoutControlItem20.Text = "N�";
            this.layoutControlItem20.TextSize = new System.Drawing.Size(51, 13);
            // 
            // layoutControlGroup3
            // 
            this.layoutControlGroup3.CustomizationFormText = ":: Contato ::";
            this.layoutControlGroup3.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItem7,
            this.layoutControlItem8,
            this.layoutControlItem10,
            this.layoutControlItem11,
            this.layoutControlItem12,
            this.layoutControlItem9,
            this.layoutControlItem15,
            this.layoutControlItem16,
            this.layoutControlItem22});
            this.layoutControlGroup3.Location = new System.Drawing.Point(0, 140);
            this.layoutControlGroup3.Name = "layoutControlGroup3";
            this.layoutControlGroup3.OptionsItemText.TextToControlDistance = 5;
            this.layoutControlGroup3.Size = new System.Drawing.Size(563, 312);
            this.layoutControlGroup3.Text = ":: Contato ::";
            // 
            // layoutControlItem7
            // 
            this.layoutControlItem7.Control = this.textEdit6;
            this.layoutControlItem7.CustomizationFormText = "Telefone 1";
            this.layoutControlItem7.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItem7.Name = "layoutControlItem7";
            this.layoutControlItem7.Size = new System.Drawing.Size(272, 24);
            this.layoutControlItem7.Text = "Telefone 1";
            this.layoutControlItem7.TextSize = new System.Drawing.Size(51, 13);
            // 
            // layoutControlItem8
            // 
            this.layoutControlItem8.Control = this.textEdit7;
            this.layoutControlItem8.CustomizationFormText = "Telefone 2";
            this.layoutControlItem8.Location = new System.Drawing.Point(272, 0);
            this.layoutControlItem8.Name = "layoutControlItem8";
            this.layoutControlItem8.Size = new System.Drawing.Size(267, 24);
            this.layoutControlItem8.Text = "Telefone 2";
            this.layoutControlItem8.TextSize = new System.Drawing.Size(51, 13);
            // 
            // layoutControlItem10
            // 
            this.layoutControlItem10.Control = this.textEdit9;
            this.layoutControlItem10.CustomizationFormText = "Email";
            this.layoutControlItem10.Location = new System.Drawing.Point(0, 48);
            this.layoutControlItem10.Name = "layoutControlItem10";
            this.layoutControlItem10.Size = new System.Drawing.Size(539, 24);
            this.layoutControlItem10.Text = "Email";
            this.layoutControlItem10.TextSize = new System.Drawing.Size(51, 13);
            // 
            // layoutControlItem11
            // 
            this.layoutControlItem11.Control = this.textEdit10;
            this.layoutControlItem11.CustomizationFormText = "Telefone 3";
            this.layoutControlItem11.Location = new System.Drawing.Point(0, 24);
            this.layoutControlItem11.Name = "layoutControlItem11";
            this.layoutControlItem11.Size = new System.Drawing.Size(272, 24);
            this.layoutControlItem11.Text = "Telefone 3";
            this.layoutControlItem11.TextSize = new System.Drawing.Size(51, 13);
            // 
            // layoutControlItem12
            // 
            this.layoutControlItem12.Control = this.textEdit11;
            this.layoutControlItem12.CustomizationFormText = "Telefone 4";
            this.layoutControlItem12.Location = new System.Drawing.Point(272, 24);
            this.layoutControlItem12.Name = "layoutControlItem12";
            this.layoutControlItem12.Size = new System.Drawing.Size(267, 24);
            this.layoutControlItem12.Text = "Telefone 4";
            this.layoutControlItem12.TextSize = new System.Drawing.Size(51, 13);
            // 
            // layoutControlItem9
            // 
            this.layoutControlItem9.Control = this.textEdit8;
            this.layoutControlItem9.CustomizationFormText = "Site";
            this.layoutControlItem9.Location = new System.Drawing.Point(0, 72);
            this.layoutControlItem9.Name = "layoutControlItem9";
            this.layoutControlItem9.Size = new System.Drawing.Size(539, 24);
            this.layoutControlItem9.Text = "Site";
            this.layoutControlItem9.TextSize = new System.Drawing.Size(51, 13);
            // 
            // layoutControlItem15
            // 
            this.layoutControlItem15.Control = this.PIS_IM;
            this.layoutControlItem15.CustomizationFormText = "NIT / PIS";
            this.layoutControlItem15.Location = new System.Drawing.Point(0, 96);
            this.layoutControlItem15.Name = "layoutControlItem15";
            this.layoutControlItem15.Size = new System.Drawing.Size(272, 24);
            this.layoutControlItem15.Text = "NIT / PIS";
            this.layoutControlItem15.TextSize = new System.Drawing.Size(51, 13);
            // 
            // layoutControlItem16
            // 
            this.layoutControlItem16.Control = this.textEdit13;
            this.layoutControlItem16.CustomizationFormText = "Estado";
            this.layoutControlItem16.Location = new System.Drawing.Point(272, 96);
            this.layoutControlItem16.Name = "layoutControlItem16";
            this.layoutControlItem16.Size = new System.Drawing.Size(267, 174);
            this.layoutControlItem16.Text = "Estado";
            this.layoutControlItem16.TextSize = new System.Drawing.Size(51, 13);
            // 
            // layoutControlItem22
            // 
            this.layoutControlItem22.Control = this.textEdit14;
            this.layoutControlItem22.CustomizationFormText = "I.E.";
            this.layoutControlItem22.Location = new System.Drawing.Point(0, 120);
            this.layoutControlItem22.Name = "layoutControlItem22";
            this.layoutControlItem22.Size = new System.Drawing.Size(272, 150);
            this.layoutControlItem22.Text = "I.E.";
            this.layoutControlItem22.TextSize = new System.Drawing.Size(51, 13);
            // 
            // layoutControlGroup5
            // 
            this.layoutControlGroup5.CustomizationFormText = "&3-Pagamento Eletr�nico";
            this.layoutControlGroup5.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlGroup6});
            this.layoutControlGroup5.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroup5.Name = "layoutControlGroup5";
            this.layoutControlGroup5.Size = new System.Drawing.Size(563, 452);
            this.layoutControlGroup5.Text = "&3-Pagamento Eletr�nico";
            // 
            // layoutControlGroup6
            // 
            this.layoutControlGroup6.CustomizationFormText = ":: Conta Cr�dito ::";
            this.layoutControlGroup6.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItem29,
            this.layoutControlItem30,
            this.layoutControlItem32,
            this.layoutControlItem33,
            this.emptySpaceItem2,
            this.layoutControlItem28,
            this.layoutControlItem35});
            this.layoutControlGroup6.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroup6.Name = "layoutControlGroup6";
            this.layoutControlGroup6.Size = new System.Drawing.Size(563, 452);
            this.layoutControlGroup6.Text = ":: Conta Cr�dito ::";
            // 
            // layoutControlItem29
            // 
            this.layoutControlItem29.Control = this.textEdit17;
            this.layoutControlItem29.CustomizationFormText = "Ag�ncia";
            this.layoutControlItem29.Location = new System.Drawing.Point(0, 48);
            this.layoutControlItem29.Name = "layoutControlItem29";
            this.layoutControlItem29.Size = new System.Drawing.Size(209, 24);
            this.layoutControlItem29.Text = "Ag�ncia";
            this.layoutControlItem29.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem29.TextSize = new System.Drawing.Size(100, 13);
            this.layoutControlItem29.TextToControlDistance = 5;
            // 
            // layoutControlItem30
            // 
            this.layoutControlItem30.Control = this.textEdit19;
            this.layoutControlItem30.CustomizationFormText = "Conta Corrente";
            this.layoutControlItem30.Location = new System.Drawing.Point(0, 72);
            this.layoutControlItem30.Name = "layoutControlItem30";
            this.layoutControlItem30.Size = new System.Drawing.Size(209, 24);
            this.layoutControlItem30.Text = "Conta Corrente";
            this.layoutControlItem30.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem30.TextSize = new System.Drawing.Size(100, 13);
            this.layoutControlItem30.TextToControlDistance = 5;
            // 
            // layoutControlItem32
            // 
            this.layoutControlItem32.Control = this.textEdit18;
            this.layoutControlItem32.CustomizationFormText = "D�gito";
            this.layoutControlItem32.Location = new System.Drawing.Point(209, 48);
            this.layoutControlItem32.Name = "layoutControlItem32";
            this.layoutControlItem32.Size = new System.Drawing.Size(330, 24);
            this.layoutControlItem32.Text = "D�gito";
            this.layoutControlItem32.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem32.TextSize = new System.Drawing.Size(30, 13);
            this.layoutControlItem32.TextToControlDistance = 5;
            // 
            // layoutControlItem33
            // 
            this.layoutControlItem33.Control = this.textEdit20;
            this.layoutControlItem33.CustomizationFormText = "Digito";
            this.layoutControlItem33.Location = new System.Drawing.Point(209, 72);
            this.layoutControlItem33.Name = "layoutControlItem33";
            this.layoutControlItem33.Size = new System.Drawing.Size(330, 24);
            this.layoutControlItem33.Text = "Digito";
            this.layoutControlItem33.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem33.TextSize = new System.Drawing.Size(30, 13);
            this.layoutControlItem33.TextToControlDistance = 5;
            // 
            // emptySpaceItem2
            // 
            this.emptySpaceItem2.AllowHotTrack = false;
            this.emptySpaceItem2.Location = new System.Drawing.Point(0, 96);
            this.emptySpaceItem2.Name = "emptySpaceItem2";
            this.emptySpaceItem2.Size = new System.Drawing.Size(539, 314);
            this.emptySpaceItem2.TextSize = new System.Drawing.Size(0, 0);
            // 
            // layoutControlItem28
            // 
            this.layoutControlItem28.Control = this.textEdit16;
            this.layoutControlItem28.CustomizationFormText = "Banco";
            this.layoutControlItem28.Location = new System.Drawing.Point(0, 24);
            this.layoutControlItem28.Name = "layoutControlItem28";
            this.layoutControlItem28.Size = new System.Drawing.Size(539, 24);
            this.layoutControlItem28.Text = "Banco";
            this.layoutControlItem28.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem28.TextSize = new System.Drawing.Size(100, 13);
            this.layoutControlItem28.TextToControlDistance = 5;
            // 
            // layoutControlItem35
            // 
            this.layoutControlItem35.Control = this.ComboContaTipoCredito;
            this.layoutControlItem35.CustomizationFormText = "Tipo de Conta";
            this.layoutControlItem35.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItem35.Name = "layoutControlItem35";
            this.layoutControlItem35.Size = new System.Drawing.Size(539, 24);
            this.layoutControlItem35.Text = "Tipo de Conta";
            this.layoutControlItem35.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem35.TextSize = new System.Drawing.Size(100, 20);
            this.layoutControlItem35.TextToControlDistance = 5;
            // 
            // layoutControlGroup9
            // 
            this.layoutControlGroup9.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItem37,
            this.layoutControlItem38,
            this.layoutControlItem39});
            this.layoutControlGroup9.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroup9.Name = "layoutControlGroup9";
            this.layoutControlGroup9.Size = new System.Drawing.Size(563, 452);
            this.layoutControlGroup9.Text = "&4-Reten��es";
            // 
            // layoutControlItem37
            // 
            this.layoutControlItem37.Control = this.fRNRetemISSCheckEdit;
            this.layoutControlItem37.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItem37.Name = "layoutControlItem37";
            this.layoutControlItem37.Size = new System.Drawing.Size(563, 23);
            this.layoutControlItem37.Text = "FRNRetem ISS:";
            this.layoutControlItem37.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem37.TextVisible = false;
            // 
            // layoutControlItem38
            // 
            this.layoutControlItem38.Control = this.fRNRetemINSSTomadorCheckEdit;
            this.layoutControlItem38.Location = new System.Drawing.Point(0, 23);
            this.layoutControlItem38.Name = "layoutControlItem38";
            this.layoutControlItem38.Size = new System.Drawing.Size(563, 23);
            this.layoutControlItem38.Text = "FRNRetem INSSTomador:";
            this.layoutControlItem38.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem38.TextVisible = false;
            // 
            // layoutControlItem39
            // 
            this.layoutControlItem39.Control = this.fRNRetemINSSPrestadorCheckEdit;
            this.layoutControlItem39.Location = new System.Drawing.Point(0, 46);
            this.layoutControlItem39.Name = "layoutControlItem39";
            this.layoutControlItem39.Size = new System.Drawing.Size(563, 406);
            this.layoutControlItem39.Text = "FRNRetem INSSPrestador:";
            this.layoutControlItem39.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem39.TextVisible = false;
            // 
            // layoutControlItem24
            // 
            this.layoutControlItem24.Control = this.checkEdit2;
            this.layoutControlItem24.CustomizationFormText = "layoutControlItem24";
            this.layoutControlItem24.Location = new System.Drawing.Point(0, 72);
            this.layoutControlItem24.Name = "layoutControlItem24";
            this.layoutControlItem24.Size = new System.Drawing.Size(384, 24);
            this.layoutControlItem24.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem24.TextVisible = false;
            // 
            // layoutControlItem23
            // 
            this.layoutControlItem23.Control = this.textEdit15;
            this.layoutControlItem23.CustomizationFormText = "Senha";
            this.layoutControlItem23.Location = new System.Drawing.Point(384, 48);
            this.layoutControlItem23.Name = "layoutControlItem23";
            this.layoutControlItem23.Size = new System.Drawing.Size(203, 24);
            this.layoutControlItem23.Text = "Senha";
            this.layoutControlItem23.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem23.TextSize = new System.Drawing.Size(50, 13);
            this.layoutControlItem23.TextToControlDistance = 5;
            // 
            // cntUsuario
            // 
            this.cntUsuario.Control = this.txtUsuario;
            this.cntUsuario.CustomizationFormText = "Usu�rio";
            this.cntUsuario.Location = new System.Drawing.Point(384, 72);
            this.cntUsuario.MaxSize = new System.Drawing.Size(203, 24);
            this.cntUsuario.MinSize = new System.Drawing.Size(203, 24);
            this.cntUsuario.Name = "cntUsuario";
            this.cntUsuario.Size = new System.Drawing.Size(203, 24);
            this.cntUsuario.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.cntUsuario.Text = "Usu�rio";
            this.cntUsuario.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.cntUsuario.TextSize = new System.Drawing.Size(50, 13);
            this.cntUsuario.TextToControlDistance = 5;
            // 
            // dFornecedoresCampos
            // 
            this.dFornecedoresCampos.DataSetName = "dFornecedoresCampos";
            this.dFornecedoresCampos.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // layoutControlItem19
            // 
            this.layoutControlItem19.Control = this.textEdit4;
            this.layoutControlItem19.CustomizationFormText = "Bairro";
            this.layoutControlItem19.Location = new System.Drawing.Point(0, 50);
            this.layoutControlItem19.Name = "layoutControlItem4";
            this.layoutControlItem19.Size = new System.Drawing.Size(426, 24);
            this.layoutControlItem19.Text = "Bairro";
            this.layoutControlItem19.TextSize = new System.Drawing.Size(51, 13);
            // 
            // layoutControlItem31
            // 
            this.layoutControlItem31.CustomizationFormText = "Ramo de Atividade";
            this.layoutControlItem31.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItem31.Name = "layoutControlItem31";
            this.layoutControlItem31.Size = new System.Drawing.Size(503, 42);
            this.layoutControlItem31.Text = "Ramo de Atividade";
            this.layoutControlItem31.TextLocation = DevExpress.Utils.Locations.Top;
            this.layoutControlItem31.TextSize = new System.Drawing.Size(90, 13);
            // 
            // layoutControlItem25
            // 
            this.layoutControlItem25.CustomizationFormText = "Ramo de Atividade";
            this.layoutControlItem25.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItem25.Name = "layoutControlItem31";
            this.layoutControlItem25.Size = new System.Drawing.Size(503, 42);
            this.layoutControlItem25.Text = "Ramo de Atividade";
            this.layoutControlItem25.TextLocation = DevExpress.Utils.Locations.Top;
            this.layoutControlItem25.TextSize = new System.Drawing.Size(90, 13);
            // 
            // layoutControlItem34
            // 
            this.layoutControlItem34.Control = this.txtUsuario;
            this.layoutControlItem34.CustomizationFormText = "Senha";
            this.layoutControlItem34.Location = new System.Drawing.Point(324, 48);
            this.layoutControlItem34.Name = "layoutControlItem23";
            this.layoutControlItem34.Size = new System.Drawing.Size(203, 24);
            this.layoutControlItem34.Text = "Senha";
            this.layoutControlItem34.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem34.TextSize = new System.Drawing.Size(30, 13);
            this.layoutControlItem34.TextToControlDistance = 5;
            // 
            // layoutControlGroup7
            // 
            this.layoutControlGroup7.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlGroup8});
            this.layoutControlGroup7.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroup7.Name = "layoutControlGroup7";
            this.layoutControlGroup7.Size = new System.Drawing.Size(479, 109);
            // 
            // layoutControlGroup8
            // 
            this.layoutControlGroup8.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroup8.Name = "layoutControlGroup8";
            this.layoutControlGroup8.Size = new System.Drawing.Size(455, 67);
            // 
            // cFornecedoresCampos
            // 
            this.BindingSourcePrincipal = this.BindingSource_F;
            this.Controls.Add(this.LayoutControl);
            this.Cursor = System.Windows.Forms.Cursors.Default;
            this.LookAndFeel.UseDefaultLookAndFeel = false;
            this.Name = "cFornecedoresCampos";
            this.Size = new System.Drawing.Size(607, 680);
            this.cargaFinal += new System.EventHandler(this.cFornecedoresCampos_cargaFinal);
            this.Load += new System.EventHandler(this.cFornecedoresCampos_Load);
            this.Controls.SetChildIndex(this.LayoutControl, 0);
            ((System.ComponentModel.ISupportInitialize)(this.BarManager_F)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.BarAndDockingController_F)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.BindingSource_F)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.StyleController_F)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LayoutControl)).EndInit();
            this.LayoutControl.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gridControl1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.fKFRNxRAVFORNECEDORESBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LookUpEditRAV)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dRamoAtividadeGradeBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dRamoAtividadeGrade)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemButtonDEL)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.fRNRetemINSSPrestadorCheckEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.fRNRetemINSSTomadorCheckEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.fRNRetemISSCheckEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ComboContaTipoCredito.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtUsuario.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit20.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit19.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit18.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit17.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit16.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit2.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit15.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit14.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.calcEdit1.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit5.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit1.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit13.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.PIS_IM.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtFantasia.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEditCPFCNPJ.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit11.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit10.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit9.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit8.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit7.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit6.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lkpCidades.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cIDADESBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lkpUF.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit4.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit3.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit2.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit1.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LayoutControlGroup)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem13)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem14)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem18)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem21)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tabbedControlGroup1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LCProcuracao)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem36)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem17)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem20)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem8)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem10)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem11)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem12)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem9)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem15)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem16)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem22)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem29)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem30)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem32)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem33)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem28)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem35)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup9)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem37)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem38)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem39)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem24)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem23)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cntUsuario)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dFornecedoresCampos)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem19)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem31)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem25)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem34)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup8)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private DevExpress.XtraLayout.LayoutControl LayoutControl;
        private DevExpress.XtraLayout.LayoutControlGroup LayoutControlGroup;
        private DevExpress.XtraEditors.TextEdit textEdit2;
        private DevExpress.XtraEditors.TextEdit textEdit1;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem1;
        private DevExpress.XtraEditors.LookUpEdit lkpCidades;
        //private DevExpress.XtraEditors.TextEdit txtUF;
        private DevExpress.XtraEditors.TextEdit textEdit4;
        private DevExpress.XtraEditors.TextEdit textEdit3;
        private DevExpress.XtraEditors.TextEdit textEdit6;
        private DevExpress.XtraEditors.TextEdit textEdit9;
        private DevExpress.XtraEditors.TextEdit textEdit8;
        private DevExpress.XtraEditors.TextEdit textEdit7;
        private DevExpress.XtraEditors.TextEdit textEdit11;
        private DevExpress.XtraEditors.TextEdit textEdit10;
        private CadastrosProc.Fornecedores.dFornecedoresCampos dFornecedoresCampos;
        private System.Windows.Forms.BindingSource cIDADESBindingSource;
        private DevExpress.XtraEditors.TextEdit textEditCPFCNPJ;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem13;
        private DevExpress.XtraEditors.TextEdit txtFantasia;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem14;
        private DevExpress.XtraEditors.LookUpEdit lkpUF;
        private DevExpress.XtraEditors.TextEdit PIS_IM;
        private DevExpress.XtraEditors.TextEdit textEdit13;
        private DevExpress.XtraEditors.SimpleButton simpleButton1;
        private DevExpress.XtraEditors.CheckEdit checkEdit1;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem18;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem19;
        private DevExpress.XtraEditors.TextEdit textEdit14;
        private DevExpress.XtraEditors.CalcEdit calcEdit1;
        private DevExpress.XtraEditors.TextEdit textEdit5;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem21;
        private DevExpress.XtraEditors.TextEdit textEdit15;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem23;
        private DevExpress.XtraLayout.TabbedControlGroup tabbedControlGroup1;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup1;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup2;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem2;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem4;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem6;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem5;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem1;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem3;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem17;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem20;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup3;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem7;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem8;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem10;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem11;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem12;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem9;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem15;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem16;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem22;
        private DevExpress.XtraEditors.CheckEdit checkEdit2;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup4;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem24;
        private System.Windows.Forms.BindingSource dRamoAtividadeGradeBindingSource;
        private RamoAtividade.dRamoAtividadeGrade dRamoAtividadeGrade;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem31;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem25;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup5;
        private DevExpress.XtraEditors.TextEdit textEdit20;
        private DevExpress.XtraEditors.TextEdit textEdit19;
        private DevExpress.XtraEditors.TextEdit textEdit18;
        private DevExpress.XtraEditors.TextEdit textEdit17;
        private DevExpress.XtraEditors.TextEdit textEdit16;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup6;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem28;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem29;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem30;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem32;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem33;
        private DevExpress.XtraEditors.TextEdit txtUsuario;
        private DevExpress.XtraLayout.LayoutControlItem cntUsuario;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem34;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem2;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup7;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup8;
        private DevExpress.XtraEditors.ImageComboBoxEdit ComboContaTipoCredito;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem35;
        private DevExpress.XtraEditors.CheckEdit fRNRetemINSSPrestadorCheckEdit;
        private DevExpress.XtraEditors.CheckEdit fRNRetemINSSTomadorCheckEdit;
        private DevExpress.XtraEditors.CheckEdit fRNRetemISSCheckEdit;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup9;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem37;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem38;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem39;
        private DevExpress.XtraEditors.SimpleButton SBProcuracao;
        private DevExpress.XtraLayout.LayoutControlItem LCProcuracao;
        private DevExpress.XtraGrid.GridControl gridControl1;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView1;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem36;
        private DevExpress.XtraGrid.Columns.GridColumn colRAV;
        private System.Windows.Forms.BindingSource fKFRNxRAVFORNECEDORESBindingSource;
        private DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit LookUpEditRAV;
        private DevExpress.XtraGrid.Columns.GridColumn colDEL;
        private DevExpress.XtraEditors.Repository.RepositoryItemButtonEdit ItemButtonDEL;
    }
}
