/*
MR - 05/01/2016 13:30            - Tipo de conta cr�dito para fornecedor (Altera��es indicadas por *** MRC - INICIO (05/01/2016 13:30) ***)
*/

using System;
using System.ComponentModel;
using System.Data;
using System.Windows.Forms;
using Cadastros.Cidades;
using DocBacarios;
using CadastrosProc.Fornecedores;
using VirEnumeracoesNeon;
using System.Collections.Generic;

namespace Cadastros.Fornecedores
{
    /// <summary>
    /// 
    /// </summary>
    public partial class cFornecedoresCampos : CompontesBasicos.ComponenteCamposBase
    {

        private Fornecedor Fornecedor1;

        /// <summary>
        /// 
        /// </summary>
        public cFornecedoresCampos()
        {
            InitializeComponent();
            TravadoParaEvitarBug = false;
            //metodoFillBy = dFornecedoresCampos.FORNECEDORESTableAdapter.GetType().GetMethod("Fill");
        }

        private void AjustaTelaPelosRAV()
        {
            if (Fornecedor1 != null)
                LCProcuracao.Visibility = Fornecedor1.ContemRAV(RAVPadrao.EscritorioAdvogado) ? DevExpress.XtraLayout.Utils.LayoutVisibility.Always : DevExpress.XtraLayout.Utils.LayoutVisibility.Never;
        }

        private void cFornecedoresCampos_Load(object sender, EventArgs e)
        {
            cIDADESBindingSource.DataSource = Framework.datasets.dCIDADES.dCIDADESSt;
            Framework.virEnumFRNContaTipoCredito.virEnumFRNContaTipoCreditoSt.CarregaEditorDaGrid(ComboContaTipoCredito);
            ValidaCPFCNPJ();
            dRamoAtividadeGradeBindingSource.DataSource = dRamoAtividadeGrade;
            dRamoAtividadeGrade.RamoAtiVidadeTableAdapter.Fill(dRamoAtividadeGrade.RamoAtiVidade);
            //dFornecedoresCampos.FRNxRAVDataTable datForRav = dFornecedoresCampos.FRNxRAVTableAdapter.GetByFRN(LinhaMae.FRN);
            //foreach (dFornecedoresCampos.FRNxRAVRow row in datForRav.Rows)
            // {
            //  //lstRamosAtividade.Items.Add(row.RAV.ToString(), row.RAVDescricao, 0);
            // if (row.RAV == (int)RAVPadrao.EscritorioAdvogado)
            //   LCProcuracao.Visibility = DevExpress.XtraLayout.Utils.LayoutVisibility.Always;
            //   }            
            if (pk <= 0)
            {
                LinhaMae.FRNRetemISS = true;
                LinhaMae.FRNRetemINSSPrestador = true;
                LinhaMae.FRNRetemINSSTomador = true;
                LinhaMae.FRNContaTipoCredito = (int)Framework.FRNContaTipoCredito.ContaCorrete;
                LCProcuracao.Visibility = DevExpress.XtraLayout.Utils.LayoutVisibility.Never;
                /*
                 * *************************************************************************
                 *                  ATEN��O: A linha de baixo est� desligado para evitar 
                 *                  erro na inclus�o de procura��o sem o n�mero definitivo do FRN
                 *                  Seria s� fazer o update no bot�o da procura��o mas precisa testar!!!
                 *         ==>>  Fornecedor1 = new Fornecedor(LinhaMae);  <<==
                 * *******************************************************************
                */
            }
            else
            {
                Fornecedor1 = new Fornecedor(LinhaMae);                
                if (LinhaMae.IsFRNCnpjNull())
                    textEditCPFCNPJ.Properties.ReadOnly = false;
                else
                {
                    CPFCNPJ cpfcnpj = new CPFCNPJ(LinhaMae.FRNCnpj);
                    if(cpfcnpj.Tipo == TipoCpfCnpj.INVALIDO)
                        textEditCPFCNPJ.Properties.ReadOnly = false;
                }
                AjustaTelaPelosRAV();
            }
            tabbedControlGroup1.SelectedTabPage = layoutControlGroup1;
        }

        private void lkpCidades_ButtonClick(object sender, DevExpress.XtraEditors.Controls.ButtonPressedEventArgs e)
        {
            if (e.Button.Index == 1)//Incluir
            {
                Int32 _PK = ShowModuloAdd(typeof(cCidadesGrade));

                if (_PK > 0)
                {
                    //cIDADESTableAdapter.Fill(dFornecedoresCampos.CIDADES);
                    Framework.datasets.dCIDADES.dCIDADESSt.RefreshDados();
                    lkpCidades.EditValue = _PK;
                }
            }
        }        

        private void lkpCidades_EditValueChanged(object sender, EventArgs e)
        {
            lkpUF.EditValue = lkpCidades.EditValue;
        }

        private CPFCNPJ cnpj;

        private bool ValidaCPFCNPJ() 
        {
            cnpj = new CPFCNPJ(textEditCPFCNPJ.Text);
            if (cnpj.Tipo == TipoCpfCnpj.INVALIDO)
                textEditCPFCNPJ.ErrorText = "CNPJ inv�lido";
            else
                textEditCPFCNPJ.Text = cnpj.ToString(true);

            //if (LinhaMae.FRNTipo == "FSR")
            //{
                if (cnpj.Tipo == TipoCpfCnpj.CPF)
                {
                    PIS_IM.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Simple;
                    PIS_IM.Properties.Mask.EditMask = @"000\.00000\.00-0";
                    PIS_IM.Properties.Mask.PlaceHolder = '_';
                    PIS_IM.Properties.Mask.SaveLiteral = false;
                    PIS_IM.Properties.Mask.BeepOnError = true;
                    PIS_IM.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
                    PIS_IM.Properties.DisplayFormat.FormatString = @"000\.00000\.00-0";
                    layoutControlItem15.Text = "NIT";
                };
                if (cnpj.Tipo == TipoCpfCnpj.CNPJ)
                {
                    PIS_IM.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.None;                    
                    PIS_IM.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.None;
                    PIS_IM.Properties.DisplayFormat.FormatString = "";
                    layoutControlItem15.Text = "I. M.";
                }
            //}
            return cnpj.Tipo != TipoCpfCnpj.INVALIDO;
        }

        private void textEdit12_Validating(object sender, CancelEventArgs e)
        {
            ValidaCPFCNPJ();
        }
        
        private void Truncar(DevExpress.XtraEditors.TextEdit Controle, string Valor) {
            Controle.Text = (Controle.Properties.MaxLength >= Valor.Length) ? Valor : Valor.Substring(0, Controle.Properties.MaxLength);        
        }

        private void simpleButton1_Click(object sender, EventArgs e)
        {
            if (VirMSSQL.CEP.STCEP.BuscaPorCEP(textEdit2.Text))
            {
                Truncar(textEdit2, VirMSSQL.CEP.STCEP.CEPFormatado);
                Truncar(textEdit3, VirMSSQL.CEP.STCEP.Rua);
                Truncar(textEdit4, VirMSSQL.CEP.STCEP.Bairro);                
                if(VirMSSQL.CEP.STCEP.NovaCidade)
                    Framework.datasets.dCIDADES.dCIDADESSt.RefreshDados();
                    //cIDADESTableAdapter.Fill(dFornecedoresCampos.CIDADES);
                lkpCidades.EditValue = VirMSSQL.CEP.STCEP.nCidadeNeon;
                BindingSource_F.EndEdit();
                textEdit2.Focus();
            }
        }        

        private dFornecedoresCampos.FORNECEDORESRow LinhaMae{
            get { return (dFornecedoresCampos.FORNECEDORESRow)LinhaMae_F; }
        }

        private void textEdit5_Validating(object sender, CancelEventArgs e)
        {
            if ((LinhaMae.FRNTipo == "FSR") && (cnpj.Tipo == TipoCpfCnpj.CPF))
                if (!PIS.IsValid(PIS_IM.Text))
                {
                    PIS_IM.ErrorText = "PIS/PASEP/NIT inv�lido";
                    e.Cancel = true;
                }
        }

        /*
        private void lookUpEdit1_ButtonClick(object sender, DevExpress.XtraEditors.Controls.ButtonPressedEventArgs e)
        {
            if (lookUpEdit1.Text != "")
            {
                if (!lstRamosAtividade.Items.ContainsKey(lookUpEdit1.EditValue.ToString()))
                {
                    lstRamosAtividade.Items.Add(lookUpEdit1.EditValue.ToString(), lookUpEdit1.Text, 0);
                    
                    if ((int)lookUpEdit1.EditValue == (int)RAVPadrao.EscritorioAdvogado)
                        LCProcuracao.Visibility = DevExpress.XtraLayout.Utils.LayoutVisibility.Always;
                }
            }
        }

        private void lstRamosAtividade_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyValue == 46 && lstRamosAtividade.Items.Count > 0)
            {
                if (lstRamosAtividade.SelectedIndices.Count > 0)
                {
                    int remover = lstRamosAtividade.SelectedIndices[0];
                    int RAVremover = int.Parse(lstRamosAtividade.Items[remover].Name);
                    RAVExcluir.Add(RAVremover);
                    if (RAVIncluir.ContainsKey(RAVremover))
                        RAVIncluir.Remove(RAVremover);
                    if (RAVremover == (int)RAVPadrao.EscritorioAdvogado)
                        LCProcuracao.Visibility = DevExpress.XtraLayout.Utils.LayoutVisibility.Never ;
                    lstRamosAtividade.Items.RemoveAt(remover);
                }
            }
        }*/


        //private List<int> _RAVExcluir;
        //private SortedList<int,string> _RAVIncluir;

        //private List<int> RAVExcluir { get => _RAVExcluir ?? (_RAVExcluir = new List<int>()); }
        //private SortedList<int, string> RAVIncluir { get => _RAVIncluir ?? (_RAVIncluir = new SortedList<int, string>()); }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public override bool Update_F()
        {                        
            if (base.Update_F())
            {

                //foreach (int RAV in RAVExcluir)
                //    dFornecedoresCampos.FRNxRAV.FindByFRNRAV(LinhaMae.FRN, RAV).Delete();
                //foreach (int RAV in RAVIncluir.Keys)
                //    dFornecedoresCampos.FRNxRAV.AddFRNxRAVRow(LinhaMae, RAV, RAVIncluir[RAV]);

                dFornecedoresCampos.FRNxRAVTableAdapter.Update(dFornecedoresCampos.FRNxRAV);
                return true;
            }
            else
               return false;
        }

        private void simpleButton2_Click(object sender, EventArgs e)
        {
            if (System.Windows.Forms.MessageBox.Show("A procura��o que ser� editada vale para TODOS os condom�nio deste escrit�rio", "* A T E N � � O *", System.Windows.Forms.MessageBoxButtons.OKCancel, System.Windows.Forms.MessageBoxIcon.Warning, System.Windows.Forms.MessageBoxDefaultButton.Button2) == System.Windows.Forms.DialogResult.OK)
            {                
                string DescProcuracao = string.Format("Procura��o - {0}", LinhaMae.FRNNome);
                maladireta.EditorRTF.cCamposImpresso Editor = new maladireta.EditorRTF.cCamposImpresso(Fornecedor1.ModeloProcuracao, "Cob Proc", DescProcuracao);                
                Editor.ShowEmPopUp();
            }
        }

        private void cFornecedoresCampos_cargaFinal(object sender, EventArgs e)
        {
            dFornecedoresCampos.FRNxRAVTableAdapter.Fill(dFornecedoresCampos.FRNxRAV, pk);
        }

        private void ItemButtonDEL_ButtonClick(object sender, DevExpress.XtraEditors.Controls.ButtonPressedEventArgs e)
        {
            dFornecedoresCampos.FRNxRAVRow rowApagar = (dFornecedoresCampos.FRNxRAVRow)gridView1.GetFocusedDataRow();
            rowApagar.Delete();
            AjustaTelaPelosRAV();
        }

        private void gridView1_CustomRowCellEdit(object sender, DevExpress.XtraGrid.Views.Grid.CustomRowCellEditEventArgs e)
        {
            if (e.Column == colDEL)                
                    e.RepositoryItem = (!somenteleitura) ? ItemButtonDEL:null;
            //if (e.Column == colRAV)
            //    LookUpEditRAV.ReadOnly = (e.RowHandle >= 0);
        }

        private void gridView1_FocusedRowChanged(object sender, DevExpress.XtraGrid.Views.Base.FocusedRowChangedEventArgs e)
        {
            LookUpEditRAV.ReadOnly = (e.FocusedRowHandle >= 0);
        }

        private void gridView1_RowUpdated(object sender, DevExpress.XtraGrid.Views.Base.RowObjectEventArgs e)
        {
            AjustaTelaPelosRAV();   
        }

        private void gridView1_ValidateRow(object sender, DevExpress.XtraGrid.Views.Base.ValidateRowEventArgs e)
        {            
            dFornecedoresCampos.FRNxRAVRow rowNova = (dFornecedoresCampos.FRNxRAVRow)((DataRowView)e.Row).Row;
            if (dFornecedoresCampos.FRNxRAV.FindByFRNRAV(rowNova.FRN, rowNova.RAV) != null)
            {
                e.ErrorText = "Ramos de atividade j� presente.\r\nSelecionar outro?";
                e.Valid = false;
            }

        }

        /*
        private void cFornecedoresCampos_cargaInicial(object sender, EventArgs e)
        {
            dFornecedoresCampos.FORNECEDORESTableAdapter.Fill(dFornecedoresCampos.FORNECEDORES, pk);
            LinhaMae_F = dFornecedoresCampos.FORNECEDORES[0];
        }

        private void cFornecedoresCampos_cargaPrincipal(object sender, EventArgs e)
        {
            dFornecedoresCampos.FORNECEDORESTableAdapter.Fill(dFornecedoresCampos.FORNECEDORES, pk);
            LinhaMae_F = dFornecedoresCampos.FORNECEDORES[0];
        }
        */
    }
}

