
using System;
using CadastrosProc.Fornecedores;

namespace Cadastros.Fornecedores
{
    /// <summary>
    /// 
    /// </summary>
    public partial class cFornecedoresGradeBase : CompontesBasicos.ComponenteGradeNavegadorP
    {
        /*
        /// <summary>
        /// Sigla que identifica o tipo de fornecedor na tabela fornecedores
        /// </summary>
        private string sigla = "";

        /*
        [Obsolete("N�o usar mais pool")]
        /// <summary>
        /// Pega do pool
        /// </summary>
        protected dFornecedoresGrade _dFornecedoresGrade
        {
            get { return dFornecedoresGrade.GetdFornecedoresGradeStd(sigla); }
        }*/

        /*
        /// <summary>
        /// Sigla que identifica o tipo de fornecedor na tabela fornecedores
        /// </summary>
        [Category("Virtual Software"), Description("Tipo de Fornecedor")]
        [DefaultValue(typeof(string), "")]
        [DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
        public string Sigla
        {
            get { return sigla; }
            set { sigla = value; }
        }
         */ 
         /*
        /// <summary>
        /// Metodo para ajuste do ds. Cada classe herdada ajusta para o ds espec�fico
        /// </summary>
        private void PegaDsEstatico() {
            dFornecedoresGradeBindingSource.DataSource = _dFornecedoresGrade;
            //dFornecedoresRamoAtividadeComboBindingSource.DataSource = _dFornecedoresGrade;            
        }*/

            /*
        [Obsolete("Cadastro de fornecedores unificado")]
        /// <summary>
        /// Construtor principal
        /// </summary>
        public cFornecedoresGradeBase(string sigla)
        {
            InitializeComponent();
            this.sigla = sigla;
            //InitializeComponent();
            if(!this.DesignMode)
                PegaDsEstatico();          
        }*/

        private dFornecedoresGrade dFornecedoresGradeLocal;
        

        /// <summary>
        /// Construtor Padr�o n�o usar
        /// </summary>
        public cFornecedoresGradeBase()
        {
            InitializeComponent();
            dFornecedoresGradeLocal = new dFornecedoresGrade();
            dFornecedoresGradeBindingSource.DataSource = dFornecedoresGradeLocal;
            dFornecedoresRamoAtividadeComboBindingSource.DataSource = dFornecedoresGradeLocal;
            dFornecedoresGradeLocal.FornecedoresRamoAtividadeTableAdapter.FillCompleto(dFornecedoresGradeLocal.FornecedoresRamoAtividade);
            //dFornecedoresGradeLocal.FornecedoresTableAdapter.FillTodos(dFornecedoresGradeLocal.Fornecedores);
            GridView_F.OptionsBehavior.Editable = true;
            GridView_F.OptionsBehavior.ReadOnly = true;

        }
        
        private dFornecedoresGrade.FornecedoresRow LinhaMae {
            get {
                if (LinhaMae_F != null)
                    return (dFornecedoresGrade.FornecedoresRow)LinhaMae_F;
                else
                    return null;
            }
        }

        /// <summary>
        /// Exclui o registro
        /// </summary>
        protected override void ExcluirRegistro_F()
        {
            if (LinhaMae != null)
            {
                //int pk = Convert.ToInt32(((DataRowView)(BindingSource_F.Current))["FRN"]);
                int pk = LinhaMae.FRN;
                //dFornecedoresCampos.FRNxRAVTableAdapter.DeleteFRNRAV(pk);
                dFornecedoresGrade.FFornecedoresTableAdapter.DeleteQuery(pk);
                //dFornecedoresGrade.FFornecedoresTableAdapter.Fill(_dFornecedoresGrade.Fornecedores, sigla);
                Calcular();
            }
        }

        /// <summary>
        /// Inclui o registro
        /// </summary>
        /// <param name="sender">Chamador</param>
        /// <param name="e">Dados do evento</param>
        protected override void BtnIncluir_F_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            if (lookUpEdit1.EditValue == null)
                Fornecedor.NovoFornecedor();
            else
                Fornecedor.NovoFornecedor((int)lookUpEdit1.EditValue);
            //ShowModuloAdd(typeof(cFornecedoresCampos), "FRNTipo", _dFornecedoresGrade.Sigla);
            Calcular();            
        }

        private void cSeguradorasGrade_cargaPrincipal(object sender, EventArgs e)
        {
           Calcular();
        }

        private void lookUpEdit1_EditValueChanged(object sender, EventArgs e)
        {
            dFornecedoresGradeLocal.Fornecedores.Clear();
        }

        private void lookUpEdit1_ButtonClick(object sender, DevExpress.XtraEditors.Controls.ButtonPressedEventArgs e)
        {
           lookUpEdit1.EditValue = null;
        }

        

        private void repositoryEditar_Click(object sender, EventArgs e)
        {
            if (somenteleitura)
                Visualizar();
            else            
                Alterar();            
        }
        

        private void simpleButton1_Click(object sender, EventArgs e)
        {
            Calcular();
        }

        private void Calcular()
        {
            if (lookUpEdit1.EditValue == null)            
                dFornecedoresGradeLocal.FornecedoresTableAdapter.FillTodos(dFornecedoresGradeLocal.Fornecedores);            
            else
                dFornecedoresGradeLocal.FornecedoresTableAdapter.FillRAV(dFornecedoresGradeLocal.Fornecedores, (int)lookUpEdit1.EditValue);
            dFornecedoresGradeLocal.FRNxRAVTableAdapter.Fill(dFornecedoresGradeLocal.FRNxRAV);
        }
    }
}

