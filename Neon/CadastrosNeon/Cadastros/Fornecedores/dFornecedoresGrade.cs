﻿/*
MR - 05/01/2016 13:30            - FillComboCompleto com todos os ramos de atividade (Alterações indicadas por *** MRC - INICIO (05/01/2016 13:30) ***)
*/

using System;

namespace Cadastros.Fornecedores
{

    /// <summary>
    /// 
    /// </summary>
    partial class dFornecedoresGrade
    {
        #region Table Adapters

        private dFornecedoresGradeTableAdapters.FRNxRAVTableAdapter fRNxRAVTableAdapter;
        /// <summary>
        /// TableAdapter padrão para tabela: FRNxRAV
        /// </summary>
        public dFornecedoresGradeTableAdapters.FRNxRAVTableAdapter FRNxRAVTableAdapter
        {
            get
            {
                if (fRNxRAVTableAdapter == null)
                {
                    fRNxRAVTableAdapter = new dFornecedoresGradeTableAdapters.FRNxRAVTableAdapter();
                    fRNxRAVTableAdapter.TrocarStringDeConexao();
                };
                return fRNxRAVTableAdapter;
            }
        }

        private dFornecedoresGradeTableAdapters.FornecedoresTableAdapter fornecedoresTableAdapter;
        /// <summary>
        /// TableAdapter padrão para tabela: Fornecedores
        /// </summary>
        public dFornecedoresGradeTableAdapters.FornecedoresTableAdapter FornecedoresTableAdapter
        {
            get
            {
                if (fornecedoresTableAdapter == null)
                {
                    fornecedoresTableAdapter = new dFornecedoresGradeTableAdapters.FornecedoresTableAdapter();
                    fornecedoresTableAdapter.TrocarStringDeConexao();
                };
                return fornecedoresTableAdapter;
            }
        }

        private dFornecedoresGradeTableAdapters.FornecedoresRamoAtividadeTableAdapter fornecedoresRamoAtividadeTableAdapter;
        /// <summary>
        /// TableAdapter padrão para tabela: FornecedoresRamoAtividade
        /// </summary>
        public dFornecedoresGradeTableAdapters.FornecedoresRamoAtividadeTableAdapter FornecedoresRamoAtividadeTableAdapter
        {
            get
            {
                if (fornecedoresRamoAtividadeTableAdapter == null)
                {
                    fornecedoresRamoAtividadeTableAdapter = new dFornecedoresGradeTableAdapters.FornecedoresRamoAtividadeTableAdapter();
                    fornecedoresRamoAtividadeTableAdapter.TrocarStringDeConexao();
                };
                return fornecedoresRamoAtividadeTableAdapter;
            }
        }

        private static Fornecedores.dFornecedoresGradeTableAdapters.FornecedoresTableAdapter fFornecedoresTableAdapter;

        /// <summary>
        /// TableAdapter para Fornecedores
        /// </summary>
        public static Fornecedores.dFornecedoresGradeTableAdapters.FornecedoresTableAdapter FFornecedoresTableAdapter
        {
            get
            {
                if (fFornecedoresTableAdapter == null)
                {
                    fFornecedoresTableAdapter = new Cadastros.Fornecedores.dFornecedoresGradeTableAdapters.FornecedoresTableAdapter();
                    fFornecedoresTableAdapter.TrocarStringDeConexao();
                }
                return fFornecedoresTableAdapter;
            }
        }

        private static Fornecedores.dFornecedoresGradeTableAdapters.FornecedoresRamoAtividadeTableAdapter fFornecedoresRamoAtividadeTableAdapter;

        /// <summary>
        /// TableAdapter para FornecedoresRamoAtividade
        /// </summary>
        public static Fornecedores.dFornecedoresGradeTableAdapters.FornecedoresRamoAtividadeTableAdapter FFornecedoresRamoAtividadeTableAdapter
        {
            get
            {
                if (fFornecedoresRamoAtividadeTableAdapter == null)
                {
                    fFornecedoresRamoAtividadeTableAdapter = new Cadastros.Fornecedores.dFornecedoresGradeTableAdapters.FornecedoresRamoAtividadeTableAdapter();
                    fFornecedoresRamoAtividadeTableAdapter.TrocarStringDeConexao();
                }
                return fFornecedoresRamoAtividadeTableAdapter;
            }
        }



        #endregion

        /// <summary>
        /// Tipo de Fornecedor
        /// </summary>
        public string Sigla;
        /// <summary>
        /// 
        /// </summary>
        public string Sigla2;
        /// <summary>
        /// 
        /// </summary>
        public int RAV;

        /// <summary>
        /// Carrega os fornecedores do tipo especificado em Sigla
        /// </summary>
        /// <returns></returns>
        public int Fill()
        {
            int Retorno;
            if (Sigla2 == null)
                Retorno = FFornecedoresTableAdapter.Fill(Fornecedores, Sigla);
            else
                Retorno = FFornecedoresTableAdapter.FillByTipos(Fornecedores, Sigla, Sigla2);

            /*
            //Retorno = FFornecedoresTableAdapter.Fill(Fornecedores, Sigla);
            if(Sigla2 != null)
                try
                {
                    FFornecedoresTableAdapter.ClearBeforeFill = false;
                    Retorno += FFornecedoresTableAdapter.Fill(Fornecedores, Sigla2);
                }
                finally
                {
                    FFornecedoresTableAdapter.ClearBeforeFill = true;
                }*/
            return Retorno;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="RAV"></param>
        /// <returns></returns>
        public int FillByRAV(int RAV)
        {
            int Retorno = FFornecedoresTableAdapter.FillByRAV(Fornecedores, Sigla, RAV);
            if (Sigla2 != null)
                try
                {
                    FFornecedoresTableAdapter.ClearBeforeFill = false;
                    Retorno += FFornecedoresTableAdapter.FillByRAV(Fornecedores, Sigla2, RAV);
                }
                finally
                {
                    FFornecedoresTableAdapter.ClearBeforeFill = true;
                }
            return Retorno;
        }

        /// <summary>
        /// Carrega os ramos de atividades dos fornecedores do tipo especificado em Sigla
        /// </summary>
        /// <returns></returns>
        public int FillCombo()
        {
            int Retorno = FFornecedoresRamoAtividadeTableAdapter.Fill(FornecedoresRamoAtividade, Sigla);
            return Retorno;
        }

        //*** MRC - INICIO (05/01/2016 13:30) ***
        /// <summary>
        /// Carrega todos os ramos de atividades 
        /// </summary>
        /// <returns></returns>
        public int FillComboCompleto()
        {
            int Retorno = FFornecedoresRamoAtividadeTableAdapter.FillCompleto(FornecedoresRamoAtividade);
            return Retorno;
        }
        //*** MRC - TERMINO (05/01/2016 13:30) ***

        //pool de datasets estáticos
        //static private System.Collections.SortedList dFornecedoresGradeTIPOStd;


        /*
        [Obsolete("Não usar mais pool")]
        /// <summary>
        /// Pega o dataset do pool
        /// </summary>
        /// <param name="Sigla"></param>
        /// <returns></returns>        
        static public dFornecedoresGrade GetdFornecedoresGradeStd(string Sigla)
        {
            dFornecedoresGrade retorno;
            if (dFornecedoresGradeTIPOStd == null)
                dFornecedoresGradeTIPOStd = new System.Collections.SortedList();
            if (dFornecedoresGradeTIPOStd.ContainsKey(Sigla))
                retorno = (dFornecedoresGrade)dFornecedoresGradeTIPOStd[Sigla];
            else
            {
                retorno = new dFornecedoresGrade();
                retorno.Sigla = Sigla;
                if (Sigla == "FSR")
                    retorno.Sigla2 = "ADV";
                retorno.Fill();
                retorno.FillCombo();
                dFornecedoresGradeTIPOStd.Add(Sigla, retorno);
            }
            return retorno;
        }

        /*
        /// <summary>
        /// 
        /// </summary>
        /// <param name="cnpj"></param>
        /// <param name="Sigla"></param>
        /// <returns></returns>
        static public FornecedoresRow BuscaCadastraFornecedorSt(DocBacarios.CPFCNPJ cnpj, string Sigla)
        {
            dFornecedoresGrade ds = GetdFornecedoresGradeStd(Sigla);
            return ds.BuscaCadastraFornecedor(cnpj);
        }*/

        /// <summary>
        /// Busca um fornecedor pelo CNPJ e cadastra se necessário
        /// </summary>
        /// <param name="cnpj"></param>
        /// <returns></returns>
        public FornecedoresRow BuscaCadastraFornecedor(DocBacarios.CPFCNPJ cnpj)
        {
            string strcnpj = cnpj.ToString();
            foreach (FornecedoresRow candidato in Fornecedores)
                if (!candidato.IsFRNCnpjNull() && (candidato.FRNCnpj == strcnpj))
                    return candidato;
            int FRN = 0;
            if (VirMSSQL.TableAdapter.STTableAdapter.BuscaEscalar("select FRN from fornecedores where FRNCnpj = @P1 and FRNTipo = @P2", out FRN, strcnpj, Sigla))
            {
                Fill();
                return Fornecedores.FindByFRN(FRN);
            }

            System.Collections.SortedList CamposNovos = new System.Collections.SortedList();
            CamposNovos.Add("FRNCnpj", strcnpj);
            CamposNovos.Add("FRNTipo", Sigla);
            FRN = CompontesBasicos.ComponenteBase.ShowModuloAdd(typeof(Cadastros.Fornecedores.cFornecedoresCampos), CamposNovos);
            if (FRN > 0)
            {
                Fill();
                return Fornecedores.FindByFRN(FRN);
            }
            return null;
        }
    }
}
