namespace Cadastros.Fornecedores
{
    partial class cFornecedoresCamposOLD
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.label2 = new System.Windows.Forms.Label();
            this.TXTNome = new DevExpress.XtraEditors.TextEdit();
            this.label1 = new System.Windows.Forms.Label();
            this.TXTCNPJ = new DevExpress.XtraEditors.TextEdit();
            this.groupControl2 = new DevExpress.XtraEditors.GroupControl();
            this.label7 = new System.Windows.Forms.Label();
            this.txtUF = new DevExpress.XtraEditors.TextEdit();
            this.label6 = new System.Windows.Forms.Label();
            this.lkpCidade = new DevExpress.XtraEditors.LookUpEdit();
            this.cIDADESBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.dFornecedoresCampos = new Cadastros.Fornecedores.dFornecedoresCampos();
            this.label5 = new System.Windows.Forms.Label();
            this.txtBairro = new DevExpress.XtraEditors.TextEdit();
            this.label4 = new System.Windows.Forms.Label();
            this.txtEndereco = new DevExpress.XtraEditors.TextEdit();
            this.label3 = new System.Windows.Forms.Label();
            this.TXTCep = new DevExpress.XtraEditors.TextEdit();
            this.groupControl3 = new DevExpress.XtraEditors.GroupControl();
            this.label13 = new System.Windows.Forms.Label();
            this.textEdit1 = new DevExpress.XtraEditors.TextEdit();
            this.label9 = new System.Windows.Forms.Label();
            this.txtFone3 = new DevExpress.XtraEditors.TextEdit();
            this.label10 = new System.Windows.Forms.Label();
            this.textEdit10 = new DevExpress.XtraEditors.TextEdit();
            this.label11 = new System.Windows.Forms.Label();
            this.txtEmail = new DevExpress.XtraEditors.TextEdit();
            this.label8 = new System.Windows.Forms.Label();
            this.txtFone2 = new DevExpress.XtraEditors.TextEdit();
            this.label12 = new System.Windows.Forms.Label();
            this.txtFone1 = new DevExpress.XtraEditors.TextEdit();
            this.fORNECEDORESTableAdapter = new Cadastros.Fornecedores.dFornecedoresCamposTableAdapters.FORNECEDORESTableAdapter();
            this.cIDADESTableAdapter = new Cadastros.Fornecedores.dFornecedoresCamposTableAdapters.CIDADESTableAdapter();
            ((System.ComponentModel.ISupportInitialize)(this.GroupControl_F)).BeginInit();
            this.GroupControl_F.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.BarManager_F)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TabControl_F)).BeginInit();
            this.TabControl_F.SuspendLayout();
            this.TabPage_F.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.BarAndDockingController_F)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.BindingSource_F)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.StyleController_F)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TXTNome.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TXTCNPJ.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl2)).BeginInit();
            this.groupControl2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtUF.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lkpCidade.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cIDADESBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dFornecedoresCampos)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtBairro.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtEndereco.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TXTCep.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl3)).BeginInit();
            this.groupControl3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit1.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtFone3.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit10.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtEmail.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtFone2.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtFone1.Properties)).BeginInit();
            this.SuspendLayout();
            // 
            // barStatus_F
            // 
            this.barStatus_F.OptionsBar.AllowQuickCustomization = false;
            this.barStatus_F.OptionsBar.DisableClose = true;
            this.barStatus_F.OptionsBar.DisableCustomization = true;
            this.barStatus_F.OptionsBar.DrawDragBorder = false;
            this.barStatus_F.OptionsBar.DrawSizeGrip = true;
            this.barStatus_F.OptionsBar.UseWholeRow = true;
            // 
            // barComponente_F
            // 
            this.barComponente_F.OptionsBar.AllowQuickCustomization = false;
            this.barComponente_F.OptionsBar.DisableClose = true;
            this.barComponente_F.OptionsBar.DisableCustomization = true;
            this.barComponente_F.OptionsBar.DrawDragBorder = false;
            this.barComponente_F.OptionsBar.UseWholeRow = true;
            // 
            // GroupControl_F
            // 
            this.GroupControl_F.Controls.Add(this.label2);
            this.GroupControl_F.Controls.Add(this.TXTNome);
            this.GroupControl_F.Controls.Add(this.label1);
            this.GroupControl_F.Controls.Add(this.TXTCNPJ);
            this.GroupControl_F.LookAndFeel.SkinName = "The Asphalt World";
            this.GroupControl_F.LookAndFeel.UseDefaultLookAndFeel = false;
            this.GroupControl_F.Size = new System.Drawing.Size(470, 30);
            this.GroupControl_F.TabIndex = 0;
            // 
            // TabControl_F
            // 
            this.TabControl_F.Location = new System.Drawing.Point(0, 72);
            this.TabControl_F.LookAndFeel.SkinName = "The Asphalt World";
            this.TabControl_F.LookAndFeel.UseDefaultLookAndFeel = false;
            this.TabControl_F.Size = new System.Drawing.Size(470, 293);
            this.TabControl_F.TabIndex = 1;
            // 
            // TabPage_F
            // 
            this.TabPage_F.Controls.Add(this.groupControl3);
            this.TabPage_F.Controls.Add(this.groupControl2);
            this.TabPage_F.Size = new System.Drawing.Size(461, 263);
            // 
            // BarAndDockingController_F
            // 
            this.BarAndDockingController_F.LookAndFeel.SkinName = "The Asphalt World";
            this.BarAndDockingController_F.PropertiesBar.AllowLinkLighting = false;
            // 
            // BindingSource_F
            // 
            this.BindingSource_F.DataMember = "FORNECEDORES";
            this.BindingSource_F.DataSource = this.dFornecedoresCampos;
            // 
            // ToolTipController_F
            // 
            this.ToolTipController_F.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(222)))), ((int)(((byte)(237)))), ((int)(((byte)(250)))));
            this.ToolTipController_F.Appearance.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(20)))), ((int)(((byte)(69)))), ((int)(((byte)(141)))));
            this.ToolTipController_F.Appearance.Options.UseBackColor = true;
            this.ToolTipController_F.Appearance.Options.UseFont = true;
            this.ToolTipController_F.Appearance.Options.UseForeColor = true;
            this.ToolTipController_F.Appearance.Options.UseTextOptions = true;
            this.ToolTipController_F.Appearance.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            // 
            // StyleController_F
            // 
            this.StyleController_F.AppearanceDisabled.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.StyleController_F.AppearanceDisabled.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.StyleController_F.AppearanceDisabled.Options.UseBackColor = true;
            this.StyleController_F.AppearanceDisabled.Options.UseForeColor = true;
            this.StyleController_F.AppearanceFocused.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(253)))), ((int)(((byte)(253)))), ((int)(((byte)(181)))));
            this.StyleController_F.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
            this.StyleController_F.AppearanceFocused.Options.UseBackColor = true;
            this.StyleController_F.AppearanceFocused.Options.UseFont = true;
            // 
            // DefaultLookAndFeel_F
            // 
            this.DefaultLookAndFeel_F.LookAndFeel.SkinName = "The Asphalt World";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.ForeColor = System.Drawing.Color.Navy;
            this.label2.Location = new System.Drawing.Point(6, 13);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(39, 13);
            this.label2.TabIndex = 0;
            this.label2.Text = "Nome";
            // 
            // TXTNome
            // 
            this.TXTNome.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.BindingSource_F, "FRNNome", true));
            this.TXTNome.Location = new System.Drawing.Point(50, 5);
            this.TXTNome.Name = "TXTNome";
            this.TXTNome.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TXTNome.Properties.Appearance.Options.UseBackColor = true;
            this.TXTNome.Size = new System.Drawing.Size(243, 20);
            this.TXTNome.StyleController = this.StyleController_F;
            this.TXTNome.TabIndex = 1;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.Color.Navy;
            this.label1.Location = new System.Drawing.Point(304, 12);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(34, 13);
            this.label1.TabIndex = 2;
            this.label1.Text = "CNPJ";
            // 
            // TXTCNPJ
            // 
            this.TXTCNPJ.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.BindingSource_F, "FRNCnpj", true));
            this.TXTCNPJ.Location = new System.Drawing.Point(344, 5);
            this.TXTCNPJ.Name = "TXTCNPJ";
            this.TXTCNPJ.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Simple;
            this.TXTCNPJ.Properties.Mask.PlaceHolder = ' ';
            this.TXTCNPJ.Properties.Mask.SaveLiteral = false;
            this.TXTCNPJ.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.TXTCNPJ.Size = new System.Drawing.Size(121, 20);
            this.TXTCNPJ.StyleController = this.StyleController_F;
            this.TXTCNPJ.TabIndex = 3;
            // 
            // groupControl2
            // 
            this.groupControl2.Controls.Add(this.label7);
            this.groupControl2.Controls.Add(this.txtUF);
            this.groupControl2.Controls.Add(this.label6);
            this.groupControl2.Controls.Add(this.lkpCidade);
            this.groupControl2.Controls.Add(this.label5);
            this.groupControl2.Controls.Add(this.txtBairro);
            this.groupControl2.Controls.Add(this.label4);
            this.groupControl2.Controls.Add(this.txtEndereco);
            this.groupControl2.Controls.Add(this.label3);
            this.groupControl2.Controls.Add(this.TXTCep);
            this.groupControl2.Location = new System.Drawing.Point(3, 3);
            this.groupControl2.LookAndFeel.SkinName = "Lilian";
            this.groupControl2.Name = "groupControl2";
            this.groupControl2.Size = new System.Drawing.Size(455, 126);
            this.groupControl2.TabIndex = 0;
            this.groupControl2.Text = ":: Endere�o ::";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.ForeColor = System.Drawing.Color.Navy;
            this.label7.Location = new System.Drawing.Point(397, 108);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(21, 13);
            this.label7.TabIndex = 8;
            this.label7.Text = "UF";
            // 
            // txtUF
            // 
            this.txtUF.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.BindingSource_F, "FRNUf", true));
            this.txtUF.Location = new System.Drawing.Point(424, 101);
            this.txtUF.Name = "txtUF";
            this.txtUF.Size = new System.Drawing.Size(26, 20);
            this.txtUF.StyleController = this.StyleController_F;
            this.txtUF.TabIndex = 9;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.ForeColor = System.Drawing.Color.Navy;
            this.label6.Location = new System.Drawing.Point(32, 108);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(45, 13);
            this.label6.TabIndex = 6;
            this.label6.Text = "Cidade";
            // 
            // lkpCidade
            // 
            this.lkpCidade.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.BindingSource_F, "FRN_CID", true));
            this.lkpCidade.Location = new System.Drawing.Point(83, 101);
            this.lkpCidade.Name = "lkpCidade";
            this.lkpCidade.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo),
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Plus)});
            this.lkpCidade.Properties.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("CIDNome", "Nome", 64, DevExpress.Utils.FormatType.None, "", true, DevExpress.Utils.HorzAlignment.Near)});
            this.lkpCidade.Properties.DataSource = this.cIDADESBindingSource;
            this.lkpCidade.Properties.DisplayMember = "CIDNome";
            this.lkpCidade.Properties.NullText = "";
            this.lkpCidade.Properties.ValueMember = "CID";
            this.lkpCidade.Size = new System.Drawing.Size(308, 20);
            this.lkpCidade.StyleController = this.StyleController_F;
            this.lkpCidade.TabIndex = 7;
            // 
            // cIDADESBindingSource
            // 
            this.cIDADESBindingSource.DataMember = "CIDADES";
            this.cIDADESBindingSource.DataSource = this.dFornecedoresCampos;
            // 
            // dFornecedoresCampos
            // 
            this.dFornecedoresCampos.DataSetName = "dFornecedoresCampos";
            this.dFornecedoresCampos.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.ForeColor = System.Drawing.Color.Navy;
            this.label5.Location = new System.Drawing.Point(36, 82);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(41, 13);
            this.label5.TabIndex = 4;
            this.label5.Text = "Bairro";
            // 
            // txtBairro
            // 
            this.txtBairro.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.BindingSource_F, "FRNBairro", true));
            this.txtBairro.Location = new System.Drawing.Point(83, 75);
            this.txtBairro.Name = "txtBairro";
            this.txtBairro.Size = new System.Drawing.Size(367, 20);
            this.txtBairro.StyleController = this.StyleController_F;
            this.txtBairro.TabIndex = 5;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.ForeColor = System.Drawing.Color.Navy;
            this.label4.Location = new System.Drawing.Point(18, 56);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(59, 13);
            this.label4.TabIndex = 2;
            this.label4.Text = "Endere�o";
            // 
            // txtEndereco
            // 
            this.txtEndereco.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.BindingSource_F, "FRNEndereco", true));
            this.txtEndereco.Location = new System.Drawing.Point(83, 49);
            this.txtEndereco.Name = "txtEndereco";
            this.txtEndereco.Size = new System.Drawing.Size(367, 20);
            this.txtEndereco.StyleController = this.StyleController_F;
            this.txtEndereco.TabIndex = 3;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.ForeColor = System.Drawing.Color.Navy;
            this.label3.Location = new System.Drawing.Point(50, 30);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(27, 13);
            this.label3.TabIndex = 0;
            this.label3.Text = "CEP";
            // 
            // TXTCep
            // 
            this.TXTCep.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.BindingSource_F, "FRNCep", true));
            this.TXTCep.Location = new System.Drawing.Point(83, 23);
            this.TXTCep.Name = "TXTCep";
            this.TXTCep.Size = new System.Drawing.Size(97, 20);
            this.TXTCep.StyleController = this.StyleController_F;
            this.TXTCep.TabIndex = 1;
            // 
            // groupControl3
            // 
            this.groupControl3.Controls.Add(this.label13);
            this.groupControl3.Controls.Add(this.textEdit1);
            this.groupControl3.Controls.Add(this.label9);
            this.groupControl3.Controls.Add(this.txtFone3);
            this.groupControl3.Controls.Add(this.label10);
            this.groupControl3.Controls.Add(this.textEdit10);
            this.groupControl3.Controls.Add(this.label11);
            this.groupControl3.Controls.Add(this.txtEmail);
            this.groupControl3.Controls.Add(this.label8);
            this.groupControl3.Controls.Add(this.txtFone2);
            this.groupControl3.Controls.Add(this.label12);
            this.groupControl3.Controls.Add(this.txtFone1);
            this.groupControl3.Location = new System.Drawing.Point(3, 135);
            this.groupControl3.LookAndFeel.SkinName = "Lilian";
            this.groupControl3.Name = "groupControl3";
            this.groupControl3.Size = new System.Drawing.Size(455, 126);
            this.groupControl3.TabIndex = 1;
            this.groupControl3.Text = ":: Contato ::";
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label13.ForeColor = System.Drawing.Color.Navy;
            this.label13.Location = new System.Drawing.Point(239, 56);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(67, 13);
            this.label13.TabIndex = 6;
            this.label13.Text = "Tefefone 4";
            // 
            // textEdit1
            // 
            this.textEdit1.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.BindingSource_F, "FRNFone4", true));
            this.textEdit1.Location = new System.Drawing.Point(312, 49);
            this.textEdit1.Name = "textEdit1";
            this.textEdit1.Size = new System.Drawing.Size(138, 20);
            this.textEdit1.StyleController = this.StyleController_F;
            this.textEdit1.TabIndex = 7;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label9.ForeColor = System.Drawing.Color.Navy;
            this.label9.Location = new System.Drawing.Point(10, 56);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(67, 13);
            this.label9.TabIndex = 4;
            this.label9.Text = "Tefefone 3";
            // 
            // txtFone3
            // 
            this.txtFone3.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.BindingSource_F, "FRNFone3", true));
            this.txtFone3.Location = new System.Drawing.Point(83, 49);
            this.txtFone3.Name = "txtFone3";
            this.txtFone3.Size = new System.Drawing.Size(138, 20);
            this.txtFone3.StyleController = this.StyleController_F;
            this.txtFone3.TabIndex = 5;
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label10.ForeColor = System.Drawing.Color.Navy;
            this.label10.Location = new System.Drawing.Point(48, 108);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(29, 13);
            this.label10.TabIndex = 10;
            this.label10.Text = "Site";
            // 
            // textEdit10
            // 
            this.textEdit10.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.BindingSource_F, "FRNSite", true));
            this.textEdit10.Location = new System.Drawing.Point(83, 101);
            this.textEdit10.Name = "textEdit10";
            this.textEdit10.Size = new System.Drawing.Size(367, 20);
            this.textEdit10.StyleController = this.StyleController_F;
            this.textEdit10.TabIndex = 11;
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label11.ForeColor = System.Drawing.Color.Navy;
            this.label11.Location = new System.Drawing.Point(40, 82);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(37, 13);
            this.label11.TabIndex = 8;
            this.label11.Text = "Email";
            // 
            // txtEmail
            // 
            this.txtEmail.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.BindingSource_F, "FRNEmail", true));
            this.txtEmail.Location = new System.Drawing.Point(83, 75);
            this.txtEmail.Name = "txtEmail";
            this.txtEmail.Size = new System.Drawing.Size(367, 20);
            this.txtEmail.StyleController = this.StyleController_F;
            this.txtEmail.TabIndex = 9;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.ForeColor = System.Drawing.Color.Navy;
            this.label8.Location = new System.Drawing.Point(239, 30);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(67, 13);
            this.label8.TabIndex = 2;
            this.label8.Text = "Tefefone 2";
            // 
            // txtFone2
            // 
            this.txtFone2.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.BindingSource_F, "FRNFone2", true));
            this.txtFone2.Location = new System.Drawing.Point(312, 23);
            this.txtFone2.Name = "txtFone2";
            this.txtFone2.Size = new System.Drawing.Size(138, 20);
            this.txtFone2.StyleController = this.StyleController_F;
            this.txtFone2.TabIndex = 3;
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label12.ForeColor = System.Drawing.Color.Navy;
            this.label12.Location = new System.Drawing.Point(10, 30);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(67, 13);
            this.label12.TabIndex = 0;
            this.label12.Text = "Tefefone 1";
            // 
            // txtFone1
            // 
            this.txtFone1.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.BindingSource_F, "FRNFone1", true));
            this.txtFone1.Location = new System.Drawing.Point(83, 23);
            this.txtFone1.Name = "txtFone1";
            this.txtFone1.Size = new System.Drawing.Size(138, 20);
            this.txtFone1.StyleController = this.StyleController_F;
            this.txtFone1.TabIndex = 1;
            // 
            // fORNECEDORESTableAdapter
            // 
            this.fORNECEDORESTableAdapter.ClearBeforeFill = true;
            // 
            // cIDADESTableAdapter
            // 
            this.cIDADESTableAdapter.ClearBeforeFill = true;
            // 
            // cFornecedoresCampos
            // 
            this.LookAndFeel.UseDefaultLookAndFeel = false;
            this.Name = "cFornecedoresCampos";
            this.Size = new System.Drawing.Size(470, 390);
            this.Load += new System.EventHandler(this.cFornecedoresCampos_Load);
            ((System.ComponentModel.ISupportInitialize)(this.GroupControl_F)).EndInit();
            this.GroupControl_F.ResumeLayout(false);
            this.GroupControl_F.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.BarManager_F)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TabControl_F)).EndInit();
            this.TabControl_F.ResumeLayout(false);
            this.TabPage_F.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.BarAndDockingController_F)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.BindingSource_F)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.StyleController_F)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TXTNome.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TXTCNPJ.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl2)).EndInit();
            this.groupControl2.ResumeLayout(false);
            this.groupControl2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtUF.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lkpCidade.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cIDADESBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dFornecedoresCampos)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtBairro.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtEndereco.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TXTCep.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl3)).EndInit();
            this.groupControl3.ResumeLayout(false);
            this.groupControl3.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit1.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtFone3.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit10.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtEmail.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtFone2.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtFone1.Properties)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Label label2;
        private DevExpress.XtraEditors.TextEdit TXTNome;
        private System.Windows.Forms.Label label1;
        private DevExpress.XtraEditors.TextEdit TXTCNPJ;
        private DevExpress.XtraEditors.GroupControl groupControl2;
        private System.Windows.Forms.Label label7;
        private DevExpress.XtraEditors.TextEdit txtUF;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label5;
        private DevExpress.XtraEditors.TextEdit txtBairro;
        private System.Windows.Forms.Label label4;
        private DevExpress.XtraEditors.TextEdit txtEndereco;
        private System.Windows.Forms.Label label3;
        private DevExpress.XtraEditors.TextEdit TXTCep;
        private DevExpress.XtraEditors.GroupControl groupControl3;
        private System.Windows.Forms.Label label9;
        private DevExpress.XtraEditors.TextEdit txtFone3;
        private System.Windows.Forms.Label label10;
        private DevExpress.XtraEditors.TextEdit textEdit10;
        private System.Windows.Forms.Label label11;
        private DevExpress.XtraEditors.TextEdit txtEmail;
        private System.Windows.Forms.Label label8;
        private DevExpress.XtraEditors.TextEdit txtFone2;
        private System.Windows.Forms.Label label12;
        private DevExpress.XtraEditors.TextEdit txtFone1;
        private dFornecedoresCampos dFornecedoresCampos;
        private Cadastros.Fornecedores.dFornecedoresCamposTableAdapters.FORNECEDORESTableAdapter fORNECEDORESTableAdapter;
        private System.Windows.Forms.BindingSource cIDADESBindingSource;
        private Cadastros.Fornecedores.dFornecedoresCamposTableAdapters.CIDADESTableAdapter cIDADESTableAdapter;
        private DevExpress.XtraEditors.LookUpEdit lkpCidade;
        private System.Windows.Forms.Label label13;
        private DevExpress.XtraEditors.TextEdit textEdit1;
    }
}
