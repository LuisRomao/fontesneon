﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using AbstratosNeon;
using DocBacarios;
using CadastrosProc.Fornecedores;

namespace Cadastros.Fornecedores
{

    /// <summary>
    /// Fornecedor
    /// </summary>
    public class Fornecedor : FornecedorProc
    {        

        /// <summary>
        /// Construtor
        /// </summary>
        /// <param name="_FRN"></param>
        public Fornecedor(int _FRN):base(_FRN)
        {                                    
        }

        /// <summary>
        /// Construtor
        /// </summary>
        /// <param name="_FRNrow"></param>
        public Fornecedor(dFornecedoresCampos.FORNECEDORESRow _FRNrow):base(_FRNrow)
        {            
        }

        /// <summary>
        /// Cria um objeto
        /// </summary>
        /// <param name="CNPJ"></param>        
        /// <returns></returns>
        public static Fornecedor GetFornecedor(CPFCNPJ CNPJ)
        {
            dFornecedoresCampos dFornecedoresCampos = new dFornecedoresCampos();
            if (dFornecedoresCampos.FORNECEDORESTableAdapter.FillByCNPJ(dFornecedoresCampos.FORNECEDORES, CNPJ.ToString()) == 0)
                return null;
            else
                return new Fornecedor(dFornecedoresCampos.FORNECEDORES[0]);
        }

        /// <summary>
        /// Cria um objeto
        /// </summary>
        /// <param name="CNPJ"></param>
        /// <param name="PodeInserir"></param>
        /// <returns></returns>
        public static Fornecedor GetFornecedor(CPFCNPJ CNPJ, bool PodeInserir)
        {
            Fornecedor Retorno = Fornecedor.GetFornecedor(CNPJ);
            if ((Retorno == null) && PodeInserir)
            {
                System.Collections.SortedList CamposNovos = new System.Collections.SortedList();
                CamposNovos.Add("FRNCnpj", CNPJ.ToString());
                CamposNovos.Add("FRNTipo", "FRN");
                int FRN = CompontesBasicos.ComponenteBase.ShowModuloAdd(typeof(cFornecedoresCampos), CamposNovos);
                if (FRN > 0)
                    return new Fornecedor(FRN);
                else
                    return null;
            }
            else
                return Retorno;                       
        }

        /// <summary>
        /// inclui novo Fornecedor
        /// </summary>
        /// <returns></returns>
        public static Fornecedor NovoFornecedor(params int[] RAVs)
        {
            Fornecedor NovoFornecedor = null;
            string strCNPJ = "";
            if (VirInput.Input.Execute("CNPJ/CPF", ref strCNPJ))
            {
                CPFCNPJ novoCPFCNPJ = new CPFCNPJ(strCNPJ);
                if (novoCPFCNPJ.Tipo == TipoCpfCnpj.INVALIDO)
                {
                    System.Windows.Forms.MessageBox.Show("CPF/CNPJ inválido");
                    return null;
                }
                NovoFornecedor = (Fornecedor)GetFornecedor(novoCPFCNPJ, true);
                if (NovoFornecedor != null)
                    foreach (int RAV in RAVs)
                        NovoFornecedor.incluiRAV(RAV);
            }
            return NovoFornecedor;
        }

        /// <summary>
        /// Editar cadastro do fornecedor
        /// </summary>
        /// <param name="EmPopup">Em diálogo (síncrono) ou em janela (assíncrono)</param>
        /// <returns></returns>
        public bool Editar(bool EmPopup)
        {
            cFornecedoresCampos Editor = new cFornecedoresCampos();
            try
            {
                Editor.Titulo = string.Format("{0} - {1}", CNPJ, FRNNome);
            }
            catch
            {
                Editor.Titulo = string.Format("CNPJ = ? - {0}", FRNNome);
            }
            Editor.Fill(CompontesBasicos.TipoDeCarga.pk, FRN, false);
            if (EmPopup)
                return Editor.VirShowModulo(CompontesBasicos.EstadosDosComponentes.PopUp) == System.Windows.Forms.DialogResult.OK;
            else
            {
                Editor.VirShowModulo(CompontesBasicos.EstadosDosComponentes.JanelasAtivas);
                return true;
            }

        }

    }
}
