﻿using System;
using System.Collections.Generic;
using Framework.datasets;
using CompontesBasicos;

namespace Cadastros.AgendaGeral
{
    /// <summary>
    /// 
    /// </summary>
    public partial class cAGendaGeral : ComponenteBase
    {
        /// <summary>
        /// 
        /// </summary>
        public cAGendaGeral()
        {
            InitializeComponent();
            aGendaGeralBindingSource.DataSource = dAGendaGeral.dAGendaGeralStFill;
            virEnumAGGStatus.virEnumAGGStatusSt.CarregaEditorDaGrid(colAGGSTatus);
            virEnumAGGTipo.virEnumAGGTipoSt.CarregaEditorDaGrid(colAGG);
        }

        private void gridView1_CustomColumnDisplayText(object sender, DevExpress.XtraGrid.Views.Base.CustomColumnDisplayTextEventArgs e)
        {
            if(e.Column == colAGGCompetencia)
            {
                if(e.Value is int)
                {
                    Framework.objetosNeon.Competencia comp = new Framework.objetosNeon.Competencia((int)e.Value);
                    e.DisplayText = comp.ToString();
                }
            }
        }
    }
}
