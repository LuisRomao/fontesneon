namespace Cadastros.Perfil_funcionalidade
{
    partial class cFuncionalidadesGrade
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(cFuncionalidadesGrade));
            this.dPerfilFuncionalidade = new Cadastros.Perfil_funcionalidade.dPerfilFuncionalidade();
            this.fUNcionalidadesBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.fUNcionalidadesTableAdapter = new Cadastros.Perfil_funcionalidade.dPerfilFuncionalidadeTableAdapters.FUNcionalidadesTableAdapter();
            this.colFUNNome = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colFUNDescricao = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colFUNNiveis = new DevExpress.XtraGrid.Columns.GridColumn();
            ((System.ComponentModel.ISupportInitialize)(this.PopImprimir_F)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.BarManager_F)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.PopExportar_F)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.GridControl_F)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.GridView_F)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.BindingSource_F)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.StyleController_F)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dPerfilFuncionalidade)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.fUNcionalidadesBindingSource)).BeginInit();
            this.SuspendLayout();
            // 
            // barNavegacao_F
            // 
            this.barNavegacao_F.OptionsBar.DisableClose = true;
            this.barNavegacao_F.OptionsBar.DrawDragBorder = false;
            this.barNavegacao_F.OptionsBar.DrawSizeGrip = true;
            this.barNavegacao_F.OptionsBar.RotateWhenVertical = false;
            this.barNavegacao_F.OptionsBar.UseWholeRow = true;
            // 
            // BtnIncluir_F
            // 
            this.BtnIncluir_F.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("BtnIncluir_F.ImageOptions.Image")));
            this.BtnIncluir_F.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.BtnIncluir_F_ItemClick_1);
            // 
            // BtnAlterar_F
            // 
            this.BtnAlterar_F.Enabled = false;
            this.BtnAlterar_F.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("BtnAlterar_F.ImageOptions.Image")));
            this.BtnAlterar_F.ImageOptions.ImageIndex = 0;
            // 
            // BtnExcluir_F
            // 
            this.BtnExcluir_F.Enabled = false;
            this.BtnExcluir_F.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("BtnExcluir_F.ImageOptions.Image")));
            // 
            // BtnImprimir_F
            // 
            this.BtnImprimir_F.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("BtnImprimir_F.ImageOptions.Image")));
            // 
            // BtnImprimirGrade_F
            // 
            this.BtnImprimirGrade_F.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("BtnImprimirGrade_F.ImageOptions.Image")));
            // 
            // BtnExportar_F
            // 
            this.BtnExportar_F.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("BtnExportar_F.ImageOptions.Image")));
            // 
            // btnConfirmar_F
            // 
            this.btnConfirmar_F.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("btnConfirmar_F.ImageOptions.Image")));
            // 
            // btnCancelar_F
            // 
            this.btnCancelar_F.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("btnCancelar_F.ImageOptions.Image")));
            // 
            // barDataBase_F
            // 
            this.barDataBase_F.OptionsBar.AllowQuickCustomization = false;
            this.barDataBase_F.OptionsBar.DisableClose = true;
            this.barDataBase_F.OptionsBar.DisableCustomization = true;
            this.barDataBase_F.OptionsBar.DrawDragBorder = false;
            this.barDataBase_F.OptionsBar.UseWholeRow = true;
            // 
            // GridControl_F
            // 
            this.GridControl_F.DataSource = this.fUNcionalidadesBindingSource;
            this.GridControl_F.EmbeddedNavigator.Buttons.Append.Visible = false;
            this.GridControl_F.EmbeddedNavigator.Buttons.CancelEdit.Visible = false;
            this.GridControl_F.EmbeddedNavigator.Buttons.Edit.Visible = false;
            this.GridControl_F.EmbeddedNavigator.Buttons.EndEdit.Visible = false;
            this.GridControl_F.EmbeddedNavigator.Buttons.Remove.Visible = false;
            this.GridControl_F.Size = new System.Drawing.Size(1511, 747);
            // 
            // GridView_F
            // 
            this.GridView_F.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colFUNNome,
            this.colFUNDescricao,
            this.colFUNNiveis});
            this.GridView_F.OptionsBehavior.AllowIncrementalSearch = true;
            this.GridView_F.OptionsBehavior.Editable = false;
            this.GridView_F.OptionsDetail.EnableMasterViewMode = false;
            this.GridView_F.OptionsLayout.Columns.StoreAllOptions = true;
            this.GridView_F.OptionsLayout.Columns.StoreAppearance = true;
            this.GridView_F.OptionsLayout.StoreAllOptions = true;
            this.GridView_F.OptionsLayout.StoreAppearance = true;
            this.GridView_F.OptionsNavigation.EnterMoveNextColumn = true;
            this.GridView_F.OptionsPrint.EnableAppearanceEvenRow = true;
            this.GridView_F.OptionsPrint.EnableAppearanceOddRow = true;
            this.GridView_F.OptionsPrint.ExpandAllDetails = true;
            this.GridView_F.OptionsView.GroupFooterShowMode = DevExpress.XtraGrid.Views.Grid.GroupFooterShowMode.VisibleAlways;
            this.GridView_F.OptionsView.NewItemRowPosition = DevExpress.XtraGrid.Views.Grid.NewItemRowPosition.Top;
            this.GridView_F.OptionsView.ShowFooter = true;
            // 
            // ToolTipController_F
            // 
            this.ToolTipController_F.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(222)))), ((int)(((byte)(237)))), ((int)(((byte)(250)))));
            this.ToolTipController_F.Appearance.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(20)))), ((int)(((byte)(69)))), ((int)(((byte)(141)))));
            this.ToolTipController_F.Appearance.Options.UseBackColor = true;
            this.ToolTipController_F.Appearance.Options.UseFont = true;
            this.ToolTipController_F.Appearance.Options.UseForeColor = true;
            this.ToolTipController_F.Appearance.Options.UseTextOptions = true;
            this.ToolTipController_F.Appearance.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            // 
            // StyleController_F
            // 
            this.StyleController_F.AppearanceDisabled.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this.StyleController_F.AppearanceDisabled.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this.StyleController_F.AppearanceDisabled.Options.UseBackColor = true;
            this.StyleController_F.AppearanceDisabled.Options.UseForeColor = true;
            this.StyleController_F.AppearanceFocused.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.StyleController_F.AppearanceFocused.Options.UseBackColor = true;
            // 
            // dPerfilFuncionalidade
            // 
            this.dPerfilFuncionalidade.DataSetName = "dPerfilFuncionalidade";
            this.dPerfilFuncionalidade.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // fUNcionalidadesBindingSource
            // 
            this.fUNcionalidadesBindingSource.DataMember = "FUNcionalidades";
            this.fUNcionalidadesBindingSource.DataSource = this.dPerfilFuncionalidade;
            // 
            // fUNcionalidadesTableAdapter
            // 
            this.fUNcionalidadesTableAdapter.ClearBeforeFill = true;
            this.fUNcionalidadesTableAdapter.GetNovosDados = ((System.Collections.ArrayList)(resources.GetObject("fUNcionalidadesTableAdapter.GetNovosDados")));
            this.fUNcionalidadesTableAdapter.TabelaDataTable = null;
            // 
            // colFUNNome
            // 
            this.colFUNNome.Caption = "Nome";
            this.colFUNNome.FieldName = "FUNNome";
            this.colFUNNome.Name = "colFUNNome";
            this.colFUNNome.Visible = true;
            this.colFUNNome.VisibleIndex = 0;
            // 
            // colFUNDescricao
            // 
            this.colFUNDescricao.Caption = "Descri��o";
            this.colFUNDescricao.FieldName = "FUNDescricao";
            this.colFUNDescricao.Name = "colFUNDescricao";
            this.colFUNDescricao.Visible = true;
            this.colFUNDescricao.VisibleIndex = 1;
            // 
            // colFUNNiveis
            // 
            this.colFUNNiveis.Caption = "N�veis";
            this.colFUNNiveis.FieldName = "FUNNiveis";
            this.colFUNNiveis.Name = "colFUNNiveis";
            this.colFUNNiveis.Visible = true;
            this.colFUNNiveis.VisibleIndex = 2;
            // 
            // cFuncionalidadesGrade
            // 
            this.BindingSourcePrincipal = this.fUNcionalidadesBindingSource;
            this.LookAndFeel.UseDefaultLookAndFeel = false;
            this.Name = "cFuncionalidadesGrade";
            ((System.ComponentModel.ISupportInitialize)(this.PopImprimir_F)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.BarManager_F)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.PopExportar_F)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.GridControl_F)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.GridView_F)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.BindingSource_F)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.StyleController_F)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dPerfilFuncionalidade)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.fUNcionalidadesBindingSource)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.BindingSource fUNcionalidadesBindingSource;
        private dPerfilFuncionalidade dPerfilFuncionalidade;
        private DevExpress.XtraGrid.Columns.GridColumn colFUNNome;
        private DevExpress.XtraGrid.Columns.GridColumn colFUNDescricao;
        private DevExpress.XtraGrid.Columns.GridColumn colFUNNiveis;
        private Cadastros.Perfil_funcionalidade.dPerfilFuncionalidadeTableAdapters.FUNcionalidadesTableAdapter fUNcionalidadesTableAdapter;
    }
}
