using System;

namespace Cadastros.Perfil_funcionalidade
{
    /// <summary>
    /// 
    /// </summary>
    public partial class cPerfisGrade : CompontesBasicos.ComponenteGradeNavegador
    {
        /// <summary>
        /// 
        /// </summary>
        public cPerfisGrade()
        {
            InitializeComponent();
        }

        private void cPerfisGrade_cargaInicial(object sender, EventArgs e)
        {
            fUNcionalidadesTableAdapter.Fill(dPerfilFuncionalidade.FUNcionalidades);
            fuNxPERTableAdapter1.Fill(dPerfilFuncionalidade.FUNxPER);
        }

        private void BtnIncluir_F_ItemClick_1(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            GridView_F.AddNewRow();
        }

        private void btnConfirmar_F_ItemClick_1(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            if (dPerfilFuncionalidade.HasChanges()) {
                fuNxPERTableAdapter1.Update(dPerfilFuncionalidade.FUNxPER);
                dPerfilFuncionalidade.FUNxPER.AcceptChanges();
                gridView1.OptionsBehavior.Editable = false;
            }
        }

        private void BtnAlterar_F_ItemClick_1(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            gridView1.OptionsBehavior.Editable = true;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected override void btnCancelar_F_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            gridView1.OptionsBehavior.Editable = false;
        }
        //private void btnCancelar_F_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        //{
        //    gridView1.OptionsBehavior.Editable = false;
        //}
    }
}

