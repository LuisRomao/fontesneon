namespace Cadastros.Perfil_funcionalidade
{
    partial class cPerfisGrade
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            DevExpress.XtraGrid.GridLevelNode gridLevelNode1 = new DevExpress.XtraGrid.GridLevelNode();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(cPerfisGrade));
            this.gridView1 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colFUN = new DevExpress.XtraGrid.Columns.GridColumn();
            this.LookUpEditFuncionalidades = new DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit();
            this.fUNcionalidadesBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.dPerfilFuncionalidade = new Cadastros.Perfil_funcionalidade.dPerfilFuncionalidade();
            this.colFUNPERNivel = new DevExpress.XtraGrid.Columns.GridColumn();
            this.pERfilBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.pERfilTableAdapter = new Cadastros.Perfil_funcionalidade.dPerfilFuncionalidadeTableAdapters.PERfilTableAdapter();
            this.colPERNome = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colPERDescricao = new DevExpress.XtraGrid.Columns.GridColumn();
            this.fuNxPERTableAdapter1 = new Cadastros.Perfil_funcionalidade.dPerfilFuncionalidadeTableAdapters.FUNxPERTableAdapter();
            this.fUNcionalidadesTableAdapter = new Cadastros.Perfil_funcionalidade.dPerfilFuncionalidadeTableAdapters.FUNcionalidadesTableAdapter();
            ((System.ComponentModel.ISupportInitialize)(this.PopImprimir_F)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.BarManager_F)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.PopExportar_F)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.GridControl_F)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.GridView_F)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.BindingSource_F)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.StyleController_F)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LookUpEditFuncionalidades)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.fUNcionalidadesBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dPerfilFuncionalidade)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pERfilBindingSource)).BeginInit();
            this.SuspendLayout();
            // 
            // barNavegacao_F
            // 
            this.barNavegacao_F.OptionsBar.DisableClose = true;
            this.barNavegacao_F.OptionsBar.DrawDragBorder = false;
            this.barNavegacao_F.OptionsBar.DrawSizeGrip = true;
            this.barNavegacao_F.OptionsBar.RotateWhenVertical = false;
            this.barNavegacao_F.OptionsBar.UseWholeRow = true;
            // 
            // BarManager_F
            // 
            this.BarManager_F.MaxItemId = 22;
            // 
            // BtnIncluir_F
            // 
            this.BtnIncluir_F.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.BtnIncluir_F_ItemClick_1);
            // 
            // BtnAlterar_F
            // 
            this.BtnAlterar_F.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.BtnAlterar_F_ItemClick_1);
            // 
            // btnConfirmar_F
            // 
            this.btnConfirmar_F.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.btnConfirmar_F_ItemClick_1);
            // 
            // btnCancelar_F
            // 
            this.btnCancelar_F.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.btnCancelar_F_ItemClick);
            // 
            // barDataBase_F
            // 
            this.barDataBase_F.OptionsBar.AllowQuickCustomization = false;
            this.barDataBase_F.OptionsBar.DisableClose = true;
            this.barDataBase_F.OptionsBar.DisableCustomization = true;
            this.barDataBase_F.OptionsBar.DrawDragBorder = false;
            this.barDataBase_F.OptionsBar.UseWholeRow = true;
            // 
            // GridControl_F
            // 
            this.GridControl_F.DataSource = this.pERfilBindingSource;
            this.GridControl_F.EmbeddedNavigator.Buttons.Append.Visible = false;
            this.GridControl_F.EmbeddedNavigator.Buttons.CancelEdit.Visible = false;
            this.GridControl_F.EmbeddedNavigator.Buttons.Edit.Visible = false;
            this.GridControl_F.EmbeddedNavigator.Buttons.EndEdit.Visible = false;
            this.GridControl_F.EmbeddedNavigator.Buttons.Remove.Visible = false;
            this.GridControl_F.EmbeddedNavigator.Name = "";
            gridLevelNode1.LevelTemplate = this.gridView1;
            gridLevelNode1.RelationName = "FK_FUNxPER_PERfil";
            this.GridControl_F.LevelTree.Nodes.AddRange(new DevExpress.XtraGrid.GridLevelNode[] {
            gridLevelNode1});
            this.GridControl_F.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.LookUpEditFuncionalidades});
            this.GridControl_F.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridView1});
            // 
            // GridView_F
            // 
            this.GridView_F.Appearance.ColumnFilterButton.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(104)))), ((int)(((byte)(184)))), ((int)(((byte)(251)))));
            this.GridView_F.Appearance.ColumnFilterButton.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(104)))), ((int)(((byte)(184)))), ((int)(((byte)(251)))));
            this.GridView_F.Appearance.ColumnFilterButton.ForeColor = System.Drawing.Color.White;
            this.GridView_F.Appearance.ColumnFilterButton.Options.UseBackColor = true;
            this.GridView_F.Appearance.ColumnFilterButton.Options.UseBorderColor = true;
            this.GridView_F.Appearance.ColumnFilterButton.Options.UseForeColor = true;
            this.GridView_F.Appearance.ColumnFilterButtonActive.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(170)))), ((int)(((byte)(216)))), ((int)(((byte)(254)))));
            this.GridView_F.Appearance.ColumnFilterButtonActive.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(170)))), ((int)(((byte)(216)))), ((int)(((byte)(254)))));
            this.GridView_F.Appearance.ColumnFilterButtonActive.ForeColor = System.Drawing.Color.Black;
            this.GridView_F.Appearance.ColumnFilterButtonActive.Options.UseBackColor = true;
            this.GridView_F.Appearance.ColumnFilterButtonActive.Options.UseBorderColor = true;
            this.GridView_F.Appearance.ColumnFilterButtonActive.Options.UseForeColor = true;
            this.GridView_F.Appearance.Empty.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(236)))), ((int)(((byte)(246)))), ((int)(((byte)(255)))));
            this.GridView_F.Appearance.Empty.BackColor2 = System.Drawing.Color.White;
            this.GridView_F.Appearance.Empty.Options.UseBackColor = true;
            this.GridView_F.Appearance.EvenRow.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(247)))), ((int)(((byte)(251)))), ((int)(((byte)(255)))));
            this.GridView_F.Appearance.EvenRow.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(247)))), ((int)(((byte)(251)))), ((int)(((byte)(255)))));
            this.GridView_F.Appearance.EvenRow.ForeColor = System.Drawing.Color.Black;
            this.GridView_F.Appearance.EvenRow.Options.UseBackColor = true;
            this.GridView_F.Appearance.EvenRow.Options.UseBorderColor = true;
            this.GridView_F.Appearance.EvenRow.Options.UseForeColor = true;
            this.GridView_F.Appearance.FilterCloseButton.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(104)))), ((int)(((byte)(184)))), ((int)(((byte)(251)))));
            this.GridView_F.Appearance.FilterCloseButton.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(104)))), ((int)(((byte)(184)))), ((int)(((byte)(251)))));
            this.GridView_F.Appearance.FilterCloseButton.ForeColor = System.Drawing.Color.White;
            this.GridView_F.Appearance.FilterCloseButton.Options.UseBackColor = true;
            this.GridView_F.Appearance.FilterCloseButton.Options.UseBorderColor = true;
            this.GridView_F.Appearance.FilterCloseButton.Options.UseForeColor = true;
            this.GridView_F.Appearance.FilterPanel.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(236)))), ((int)(((byte)(246)))), ((int)(((byte)(255)))));
            this.GridView_F.Appearance.FilterPanel.BackColor2 = System.Drawing.Color.White;
            this.GridView_F.Appearance.FilterPanel.ForeColor = System.Drawing.Color.Black;
            this.GridView_F.Appearance.FilterPanel.Options.UseBackColor = true;
            this.GridView_F.Appearance.FilterPanel.Options.UseForeColor = true;
            this.GridView_F.Appearance.FixedLine.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(59)))), ((int)(((byte)(133)))), ((int)(((byte)(195)))));
            this.GridView_F.Appearance.FixedLine.Options.UseBackColor = true;
            this.GridView_F.Appearance.FocusedCell.BackColor = System.Drawing.Color.White;
            this.GridView_F.Appearance.FocusedCell.ForeColor = System.Drawing.Color.Black;
            this.GridView_F.Appearance.FocusedCell.Options.UseBackColor = true;
            this.GridView_F.Appearance.FocusedCell.Options.UseForeColor = true;
            this.GridView_F.Appearance.FocusedRow.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(38)))), ((int)(((byte)(109)))), ((int)(((byte)(189)))));
            this.GridView_F.Appearance.FocusedRow.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(59)))), ((int)(((byte)(139)))), ((int)(((byte)(206)))));
            this.GridView_F.Appearance.FocusedRow.ForeColor = System.Drawing.Color.White;
            this.GridView_F.Appearance.FocusedRow.Options.UseBackColor = true;
            this.GridView_F.Appearance.FocusedRow.Options.UseBorderColor = true;
            this.GridView_F.Appearance.FocusedRow.Options.UseForeColor = true;
            this.GridView_F.Appearance.FooterPanel.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(104)))), ((int)(((byte)(184)))), ((int)(((byte)(251)))));
            this.GridView_F.Appearance.FooterPanel.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(104)))), ((int)(((byte)(184)))), ((int)(((byte)(251)))));
            this.GridView_F.Appearance.FooterPanel.ForeColor = System.Drawing.Color.Black;
            this.GridView_F.Appearance.FooterPanel.Options.UseBackColor = true;
            this.GridView_F.Appearance.FooterPanel.Options.UseBorderColor = true;
            this.GridView_F.Appearance.FooterPanel.Options.UseForeColor = true;
            this.GridView_F.Appearance.GroupButton.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(104)))), ((int)(((byte)(184)))), ((int)(((byte)(251)))));
            this.GridView_F.Appearance.GroupButton.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(104)))), ((int)(((byte)(184)))), ((int)(((byte)(251)))));
            this.GridView_F.Appearance.GroupButton.Options.UseBackColor = true;
            this.GridView_F.Appearance.GroupButton.Options.UseBorderColor = true;
            this.GridView_F.Appearance.GroupFooter.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(170)))), ((int)(((byte)(216)))), ((int)(((byte)(254)))));
            this.GridView_F.Appearance.GroupFooter.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(170)))), ((int)(((byte)(216)))), ((int)(((byte)(254)))));
            this.GridView_F.Appearance.GroupFooter.ForeColor = System.Drawing.Color.Black;
            this.GridView_F.Appearance.GroupFooter.Options.UseBackColor = true;
            this.GridView_F.Appearance.GroupFooter.Options.UseBorderColor = true;
            this.GridView_F.Appearance.GroupFooter.Options.UseForeColor = true;
            this.GridView_F.Appearance.GroupPanel.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(236)))), ((int)(((byte)(246)))), ((int)(((byte)(255)))));
            this.GridView_F.Appearance.GroupPanel.BackColor2 = System.Drawing.Color.White;
            this.GridView_F.Appearance.GroupPanel.ForeColor = System.Drawing.Color.Black;
            this.GridView_F.Appearance.GroupPanel.Options.UseBackColor = true;
            this.GridView_F.Appearance.GroupPanel.Options.UseForeColor = true;
            this.GridView_F.Appearance.GroupRow.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(170)))), ((int)(((byte)(216)))), ((int)(((byte)(254)))));
            this.GridView_F.Appearance.GroupRow.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(170)))), ((int)(((byte)(216)))), ((int)(((byte)(254)))));
            this.GridView_F.Appearance.GroupRow.ForeColor = System.Drawing.Color.Black;
            this.GridView_F.Appearance.GroupRow.Options.UseBackColor = true;
            this.GridView_F.Appearance.GroupRow.Options.UseBorderColor = true;
            this.GridView_F.Appearance.GroupRow.Options.UseForeColor = true;
            this.GridView_F.Appearance.HeaderPanel.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(139)))), ((int)(((byte)(201)))), ((int)(((byte)(254)))));
            this.GridView_F.Appearance.HeaderPanel.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(139)))), ((int)(((byte)(201)))), ((int)(((byte)(254)))));
            this.GridView_F.Appearance.HeaderPanel.ForeColor = System.Drawing.Color.Black;
            this.GridView_F.Appearance.HeaderPanel.Options.UseBackColor = true;
            this.GridView_F.Appearance.HeaderPanel.Options.UseBorderColor = true;
            this.GridView_F.Appearance.HeaderPanel.Options.UseForeColor = true;
            this.GridView_F.Appearance.HideSelectionRow.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(105)))), ((int)(((byte)(170)))), ((int)(((byte)(225)))));
            this.GridView_F.Appearance.HideSelectionRow.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(83)))), ((int)(((byte)(155)))), ((int)(((byte)(215)))));
            this.GridView_F.Appearance.HideSelectionRow.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(236)))), ((int)(((byte)(246)))), ((int)(((byte)(255)))));
            this.GridView_F.Appearance.HideSelectionRow.Options.UseBackColor = true;
            this.GridView_F.Appearance.HideSelectionRow.Options.UseBorderColor = true;
            this.GridView_F.Appearance.HideSelectionRow.Options.UseForeColor = true;
            this.GridView_F.Appearance.HorzLine.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(104)))), ((int)(((byte)(184)))), ((int)(((byte)(251)))));
            this.GridView_F.Appearance.HorzLine.Options.UseBackColor = true;
            this.GridView_F.Appearance.OddRow.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(236)))), ((int)(((byte)(246)))), ((int)(((byte)(255)))));
            this.GridView_F.Appearance.OddRow.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(236)))), ((int)(((byte)(246)))), ((int)(((byte)(255)))));
            this.GridView_F.Appearance.OddRow.ForeColor = System.Drawing.Color.Black;
            this.GridView_F.Appearance.OddRow.Options.UseBackColor = true;
            this.GridView_F.Appearance.OddRow.Options.UseBorderColor = true;
            this.GridView_F.Appearance.OddRow.Options.UseForeColor = true;
            this.GridView_F.Appearance.Preview.Font = new System.Drawing.Font("Verdana", 7.5F);
            this.GridView_F.Appearance.Preview.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(83)))), ((int)(((byte)(155)))), ((int)(((byte)(215)))));
            this.GridView_F.Appearance.Preview.Options.UseFont = true;
            this.GridView_F.Appearance.Preview.Options.UseForeColor = true;
            this.GridView_F.Appearance.Row.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(247)))), ((int)(((byte)(251)))), ((int)(((byte)(255)))));
            this.GridView_F.Appearance.Row.ForeColor = System.Drawing.Color.Black;
            this.GridView_F.Appearance.Row.Options.UseBackColor = true;
            this.GridView_F.Appearance.Row.Options.UseForeColor = true;
            this.GridView_F.Appearance.RowSeparator.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(236)))), ((int)(((byte)(246)))), ((int)(((byte)(255)))));
            this.GridView_F.Appearance.RowSeparator.BackColor2 = System.Drawing.Color.White;
            this.GridView_F.Appearance.RowSeparator.Options.UseBackColor = true;
            this.GridView_F.Appearance.SelectedRow.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(83)))), ((int)(((byte)(155)))), ((int)(((byte)(215)))));
            this.GridView_F.Appearance.SelectedRow.ForeColor = System.Drawing.Color.White;
            this.GridView_F.Appearance.SelectedRow.Options.UseBackColor = true;
            this.GridView_F.Appearance.SelectedRow.Options.UseForeColor = true;
            this.GridView_F.Appearance.TopNewRow.BackColor = System.Drawing.Color.White;
            this.GridView_F.Appearance.TopNewRow.Options.UseBackColor = true;
            this.GridView_F.Appearance.VertLine.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(104)))), ((int)(((byte)(184)))), ((int)(((byte)(251)))));
            this.GridView_F.Appearance.VertLine.Options.UseBackColor = true;
            this.GridView_F.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colPERNome,
            this.colPERDescricao});
            this.GridView_F.OptionsBehavior.AllowIncrementalSearch = true;
            this.GridView_F.OptionsBehavior.Editable = false;
            this.GridView_F.OptionsDetail.AllowExpandEmptyDetails = true;
            this.GridView_F.OptionsDetail.ShowDetailTabs = false;
            this.GridView_F.OptionsLayout.Columns.StoreAllOptions = true;
            this.GridView_F.OptionsLayout.Columns.StoreAppearance = true;
            this.GridView_F.OptionsLayout.StoreAllOptions = true;
            this.GridView_F.OptionsLayout.StoreAppearance = true;
            this.GridView_F.OptionsNavigation.EnterMoveNextColumn = true;
            this.GridView_F.OptionsPrint.EnableAppearanceEvenRow = true;
            this.GridView_F.OptionsPrint.EnableAppearanceOddRow = true;
            this.GridView_F.OptionsPrint.ExpandAllDetails = true;
            this.GridView_F.OptionsView.EnableAppearanceEvenRow = true;
            this.GridView_F.OptionsView.EnableAppearanceOddRow = true;
            this.GridView_F.OptionsView.ShowGroupPanel = false;
            // 
            // ToolTipController_F
            // 
            this.ToolTipController_F.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(222)))), ((int)(((byte)(237)))), ((int)(((byte)(250)))));
            this.ToolTipController_F.Appearance.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(20)))), ((int)(((byte)(69)))), ((int)(((byte)(141)))));
            this.ToolTipController_F.Appearance.Options.UseBackColor = true;
            this.ToolTipController_F.Appearance.Options.UseFont = true;
            this.ToolTipController_F.Appearance.Options.UseForeColor = true;
            this.ToolTipController_F.Appearance.Options.UseTextOptions = true;
            this.ToolTipController_F.Appearance.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            // 
            // StyleController_F
            // 
            this.StyleController_F.AppearanceFocused.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.StyleController_F.AppearanceFocused.Options.UseBackColor = true;
            
            // 
            // gridView1
            // 
            this.gridView1.Appearance.ColumnFilterButton.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(195)))), ((int)(((byte)(143)))), ((int)(((byte)(62)))));
            this.gridView1.Appearance.ColumnFilterButton.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(195)))), ((int)(((byte)(143)))), ((int)(((byte)(62)))));
            this.gridView1.Appearance.ColumnFilterButton.ForeColor = System.Drawing.Color.White;
            this.gridView1.Appearance.ColumnFilterButton.Options.UseBackColor = true;
            this.gridView1.Appearance.ColumnFilterButton.Options.UseBorderColor = true;
            this.gridView1.Appearance.ColumnFilterButton.Options.UseForeColor = true;
            this.gridView1.Appearance.ColumnFilterButtonActive.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(230)))), ((int)(((byte)(189)))), ((int)(((byte)(125)))));
            this.gridView1.Appearance.ColumnFilterButtonActive.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(230)))), ((int)(((byte)(189)))), ((int)(((byte)(125)))));
            this.gridView1.Appearance.ColumnFilterButtonActive.ForeColor = System.Drawing.Color.White;
            this.gridView1.Appearance.ColumnFilterButtonActive.Options.UseBackColor = true;
            this.gridView1.Appearance.ColumnFilterButtonActive.Options.UseBorderColor = true;
            this.gridView1.Appearance.ColumnFilterButtonActive.Options.UseForeColor = true;
            this.gridView1.Appearance.Empty.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(247)))), ((int)(((byte)(219)))));
            this.gridView1.Appearance.Empty.BackColor2 = System.Drawing.Color.White;
            this.gridView1.Appearance.Empty.Options.UseBackColor = true;
            this.gridView1.Appearance.EvenRow.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(247)))), ((int)(((byte)(219)))));
            this.gridView1.Appearance.EvenRow.ForeColor = System.Drawing.Color.Black;
            this.gridView1.Appearance.EvenRow.Options.UseBackColor = true;
            this.gridView1.Appearance.EvenRow.Options.UseForeColor = true;
            this.gridView1.Appearance.FilterCloseButton.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(195)))), ((int)(((byte)(143)))), ((int)(((byte)(62)))));
            this.gridView1.Appearance.FilterCloseButton.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(195)))), ((int)(((byte)(143)))), ((int)(((byte)(62)))));
            this.gridView1.Appearance.FilterCloseButton.ForeColor = System.Drawing.Color.White;
            this.gridView1.Appearance.FilterCloseButton.Options.UseBackColor = true;
            this.gridView1.Appearance.FilterCloseButton.Options.UseBorderColor = true;
            this.gridView1.Appearance.FilterCloseButton.Options.UseForeColor = true;
            this.gridView1.Appearance.FilterPanel.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(212)))), ((int)(((byte)(166)))), ((int)(((byte)(93)))));
            this.gridView1.Appearance.FilterPanel.BackColor2 = System.Drawing.Color.White;
            this.gridView1.Appearance.FilterPanel.ForeColor = System.Drawing.Color.White;
            this.gridView1.Appearance.FilterPanel.Options.UseBackColor = true;
            this.gridView1.Appearance.FilterPanel.Options.UseForeColor = true;
            this.gridView1.Appearance.FixedLine.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(129)))), ((int)(((byte)(120)))), ((int)(((byte)(88)))));
            this.gridView1.Appearance.FixedLine.Options.UseBackColor = true;
            this.gridView1.Appearance.FocusedCell.BackColor = System.Drawing.Color.White;
            this.gridView1.Appearance.FocusedCell.ForeColor = System.Drawing.Color.Black;
            this.gridView1.Appearance.FocusedCell.Options.UseBackColor = true;
            this.gridView1.Appearance.FocusedCell.Options.UseForeColor = true;
            this.gridView1.Appearance.FocusedRow.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(164)))), ((int)(((byte)(136)))), ((int)(((byte)(91)))));
            this.gridView1.Appearance.FocusedRow.ForeColor = System.Drawing.Color.White;
            this.gridView1.Appearance.FocusedRow.Options.UseBackColor = true;
            this.gridView1.Appearance.FocusedRow.Options.UseForeColor = true;
            this.gridView1.Appearance.FooterPanel.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(195)))), ((int)(((byte)(143)))), ((int)(((byte)(62)))));
            this.gridView1.Appearance.FooterPanel.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(195)))), ((int)(((byte)(143)))), ((int)(((byte)(62)))));
            this.gridView1.Appearance.FooterPanel.ForeColor = System.Drawing.Color.White;
            this.gridView1.Appearance.FooterPanel.Options.UseBackColor = true;
            this.gridView1.Appearance.FooterPanel.Options.UseBorderColor = true;
            this.gridView1.Appearance.FooterPanel.Options.UseForeColor = true;
            this.gridView1.Appearance.GroupButton.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(195)))), ((int)(((byte)(143)))), ((int)(((byte)(62)))));
            this.gridView1.Appearance.GroupButton.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(195)))), ((int)(((byte)(143)))), ((int)(((byte)(62)))));
            this.gridView1.Appearance.GroupButton.Options.UseBackColor = true;
            this.gridView1.Appearance.GroupButton.Options.UseBorderColor = true;
            this.gridView1.Appearance.GroupFooter.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(230)))), ((int)(((byte)(189)))), ((int)(((byte)(125)))));
            this.gridView1.Appearance.GroupFooter.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(230)))), ((int)(((byte)(189)))), ((int)(((byte)(125)))));
            this.gridView1.Appearance.GroupFooter.ForeColor = System.Drawing.Color.Black;
            this.gridView1.Appearance.GroupFooter.Options.UseBackColor = true;
            this.gridView1.Appearance.GroupFooter.Options.UseBorderColor = true;
            this.gridView1.Appearance.GroupFooter.Options.UseForeColor = true;
            this.gridView1.Appearance.GroupPanel.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(212)))), ((int)(((byte)(166)))), ((int)(((byte)(93)))));
            this.gridView1.Appearance.GroupPanel.BackColor2 = System.Drawing.Color.White;
            this.gridView1.Appearance.GroupPanel.ForeColor = System.Drawing.Color.White;
            this.gridView1.Appearance.GroupPanel.Options.UseBackColor = true;
            this.gridView1.Appearance.GroupPanel.Options.UseForeColor = true;
            this.gridView1.Appearance.GroupRow.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(230)))), ((int)(((byte)(189)))), ((int)(((byte)(125)))));
            this.gridView1.Appearance.GroupRow.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(230)))), ((int)(((byte)(189)))), ((int)(((byte)(125)))));
            this.gridView1.Appearance.GroupRow.ForeColor = System.Drawing.Color.Black;
            this.gridView1.Appearance.GroupRow.Options.UseBackColor = true;
            this.gridView1.Appearance.GroupRow.Options.UseBorderColor = true;
            this.gridView1.Appearance.GroupRow.Options.UseForeColor = true;
            this.gridView1.Appearance.HeaderPanel.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(212)))), ((int)(((byte)(166)))), ((int)(((byte)(93)))));
            this.gridView1.Appearance.HeaderPanel.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(212)))), ((int)(((byte)(166)))), ((int)(((byte)(93)))));
            this.gridView1.Appearance.HeaderPanel.ForeColor = System.Drawing.Color.Black;
            this.gridView1.Appearance.HeaderPanel.Options.UseBackColor = true;
            this.gridView1.Appearance.HeaderPanel.Options.UseBorderColor = true;
            this.gridView1.Appearance.HeaderPanel.Options.UseForeColor = true;
            this.gridView1.Appearance.HideSelectionRow.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(196)))), ((int)(((byte)(172)))), ((int)(((byte)(134)))));
            this.gridView1.Appearance.HideSelectionRow.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(245)))), ((int)(((byte)(234)))), ((int)(((byte)(216)))));
            this.gridView1.Appearance.HideSelectionRow.Options.UseBackColor = true;
            this.gridView1.Appearance.HideSelectionRow.Options.UseForeColor = true;
            this.gridView1.Appearance.HorzLine.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(209)))), ((int)(((byte)(169)))), ((int)(((byte)(107)))));
            this.gridView1.Appearance.HorzLine.Options.UseBackColor = true;
            this.gridView1.Appearance.OddRow.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(234)))), ((int)(((byte)(224)))), ((int)(((byte)(190)))));
            this.gridView1.Appearance.OddRow.ForeColor = System.Drawing.Color.Black;
            this.gridView1.Appearance.OddRow.Options.UseBackColor = true;
            this.gridView1.Appearance.OddRow.Options.UseForeColor = true;
            this.gridView1.Appearance.Preview.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(252)))), ((int)(((byte)(237)))));
            this.gridView1.Appearance.Preview.Font = new System.Drawing.Font("Verdana", 7.5F);
            this.gridView1.Appearance.Preview.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(129)))), ((int)(((byte)(120)))), ((int)(((byte)(88)))));
            this.gridView1.Appearance.Preview.Options.UseBackColor = true;
            this.gridView1.Appearance.Preview.Options.UseFont = true;
            this.gridView1.Appearance.Preview.Options.UseForeColor = true;
            this.gridView1.Appearance.Row.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(247)))), ((int)(((byte)(219)))));
            this.gridView1.Appearance.Row.ForeColor = System.Drawing.Color.Black;
            this.gridView1.Appearance.Row.Options.UseBackColor = true;
            this.gridView1.Appearance.Row.Options.UseForeColor = true;
            this.gridView1.Appearance.RowSeparator.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(247)))), ((int)(((byte)(219)))));
            this.gridView1.Appearance.RowSeparator.BackColor2 = System.Drawing.Color.White;
            this.gridView1.Appearance.RowSeparator.Options.UseBackColor = true;
            this.gridView1.Appearance.SelectedRow.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(187)))), ((int)(((byte)(159)))), ((int)(((byte)(114)))));
            this.gridView1.Appearance.SelectedRow.ForeColor = System.Drawing.Color.White;
            this.gridView1.Appearance.SelectedRow.Options.UseBackColor = true;
            this.gridView1.Appearance.SelectedRow.Options.UseForeColor = true;
            this.gridView1.Appearance.TopNewRow.BackColor = System.Drawing.Color.White;
            this.gridView1.Appearance.TopNewRow.Options.UseBackColor = true;
            this.gridView1.Appearance.VertLine.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(209)))), ((int)(((byte)(169)))), ((int)(((byte)(107)))));
            this.gridView1.Appearance.VertLine.Options.UseBackColor = true;
            this.gridView1.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colFUN,
            this.colFUNPERNivel});
            this.gridView1.GridControl = this.GridControl_F;
            this.gridView1.Name = "gridView1";
            this.gridView1.NewItemRowText = "Nova Funcionalidade";
            this.gridView1.OptionsBehavior.Editable = false;
            this.gridView1.OptionsDetail.ShowDetailTabs = false;
            this.gridView1.OptionsView.EnableAppearanceEvenRow = true;
            this.gridView1.OptionsView.EnableAppearanceOddRow = true;
            this.gridView1.OptionsView.NewItemRowPosition = DevExpress.XtraGrid.Views.Grid.NewItemRowPosition.Top;
            this.gridView1.OptionsView.ShowGroupPanel = false;
            // 
            // colFUN
            // 
            this.colFUN.Caption = "Funcionalidade";
            this.colFUN.ColumnEdit = this.LookUpEditFuncionalidades;
            this.colFUN.FieldName = "FUN";
            this.colFUN.Name = "colFUN";
            this.colFUN.Visible = true;
            this.colFUN.VisibleIndex = 0;
            this.colFUN.Width = 766;
            // 
            // LookUpEditFuncionalidades
            // 
            this.LookUpEditFuncionalidades.AutoHeight = false;
            this.LookUpEditFuncionalidades.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LookUpEditFuncionalidades.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("FUNNome", "Nome", 53, DevExpress.Utils.FormatType.None, "", true, DevExpress.Utils.HorzAlignment.Near),
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("FUNDescricao", "Descri��o", 72, DevExpress.Utils.FormatType.None, "", true, DevExpress.Utils.HorzAlignment.Near)});
            this.LookUpEditFuncionalidades.DataSource = this.fUNcionalidadesBindingSource;
            this.LookUpEditFuncionalidades.DisplayMember = "FUNNome";
            this.LookUpEditFuncionalidades.Name = "LookUpEditFuncionalidades";
            this.LookUpEditFuncionalidades.NullText = "Selecione uma funcionalidade";
            this.LookUpEditFuncionalidades.ValueMember = "FUN";
            // 
            // fUNcionalidadesBindingSource
            // 
            this.fUNcionalidadesBindingSource.DataMember = "FUNcionalidades";
            this.fUNcionalidadesBindingSource.DataSource = this.dPerfilFuncionalidade;
            // 
            // dPerfilFuncionalidade
            // 
            this.dPerfilFuncionalidade.DataSetName = "dPerfilFuncionalidade";
            this.dPerfilFuncionalidade.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // colFUNPERNivel
            // 
            this.colFUNPERNivel.Caption = "N�vel";
            this.colFUNPERNivel.FieldName = "FUNPERNivel";
            this.colFUNPERNivel.Name = "colFUNPERNivel";
            this.colFUNPERNivel.OptionsColumn.FixedWidth = true;
            this.colFUNPERNivel.Visible = true;
            this.colFUNPERNivel.VisibleIndex = 1;
            this.colFUNPERNivel.Width = 66;
            // 
            // pERfilBindingSource
            // 
            this.pERfilBindingSource.DataMember = "PERfil";
            this.pERfilBindingSource.DataSource = this.dPerfilFuncionalidade;
            // 
            // pERfilTableAdapter
            // 
            this.pERfilTableAdapter.ClearBeforeFill = true;
            // 
            // colPERNome
            // 
            this.colPERNome.Caption = "Nome";
            this.colPERNome.FieldName = "PERNome";
            this.colPERNome.Name = "colPERNome";
            this.colPERNome.Visible = true;
            this.colPERNome.VisibleIndex = 0;
            // 
            // colPERDescricao
            // 
            this.colPERDescricao.Caption = "Descri��o";
            this.colPERDescricao.FieldName = "PERDescricao";
            this.colPERDescricao.Name = "colPERDescricao";
            this.colPERDescricao.Visible = true;
            this.colPERDescricao.VisibleIndex = 1;
            // 
            // fuNxPERTableAdapter1
            // 
            this.fuNxPERTableAdapter1.ClearBeforeFill = true;
            // 
            // fUNcionalidadesTableAdapter
            // 
            this.fUNcionalidadesTableAdapter.ClearBeforeFill = true;
            // 
            // cPerfisGrade
            // 
            this.BindingSourcePrincipal = this.pERfilBindingSource;
     
            this.LookAndFeel.UseDefaultLookAndFeel = false;
            this.Name = "cPerfisGrade";
            this.ToolTipController_F.SetSuperTip(this, null);
            this.cargaInicial += new System.EventHandler(this.cPerfisGrade_cargaInicial);
            ((System.ComponentModel.ISupportInitialize)(this.PopImprimir_F)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.BarManager_F)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.PopExportar_F)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.GridControl_F)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.GridView_F)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.BindingSource_F)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.StyleController_F)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LookUpEditFuncionalidades)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.fUNcionalidadesBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dPerfilFuncionalidade)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pERfilBindingSource)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.BindingSource pERfilBindingSource;
        private dPerfilFuncionalidade dPerfilFuncionalidade;
        private DevExpress.XtraGrid.Columns.GridColumn colPERNome;
        private DevExpress.XtraGrid.Columns.GridColumn colPERDescricao;
        private Cadastros.Perfil_funcionalidade.dPerfilFuncionalidadeTableAdapters.PERfilTableAdapter pERfilTableAdapter;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView1;
        private Cadastros.Perfil_funcionalidade.dPerfilFuncionalidadeTableAdapters.FUNxPERTableAdapter fuNxPERTableAdapter1;
        private DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit LookUpEditFuncionalidades;
        private System.Windows.Forms.BindingSource fUNcionalidadesBindingSource;
        private Cadastros.Perfil_funcionalidade.dPerfilFuncionalidadeTableAdapters.FUNcionalidadesTableAdapter fUNcionalidadesTableAdapter;
        private DevExpress.XtraGrid.Columns.GridColumn colFUN;
        private DevExpress.XtraGrid.Columns.GridColumn colFUNPERNivel;
    }
}
