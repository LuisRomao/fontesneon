using System;
using System.Collections.Generic;
using System.Text;


namespace Cadastros
{
    /// <summary>
    /// Classe estatica para as tabelas p�blicas
    /// </summary>
    class Estatica
    {
        private static dEstatico edEstatico;

        public static dEstatico EdEstatico
        {
            get {
                if (edEstatico == null)
                    edEstatico = new dEstatico();
                return Estatica.edEstatico; 
            }
            set { }
        }

        private static dEstaticoTableAdapters.PESSOASTableAdapter eAdapPessoas;

        public static dEstaticoTableAdapters.PESSOASTableAdapter EAdapPessoas
        {
            get
            {
                if (eAdapPessoas == null)
                {
                    eAdapPessoas = new dEstaticoTableAdapters.PESSOASTableAdapter();
                    eAdapPessoas.TrocarStringDeConexao(VirDB.Bancovirtual.TiposDeBanco.SQL);
                }
                return Estatica.eAdapPessoas;
            }
            set { }
        }

    }
}
