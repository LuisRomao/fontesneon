﻿using System;
using System.Drawing;
using Framework.datasets;

namespace Cadastros.Responsaveis
{
    /// <summary>
    /// 
    /// </summary>
    public partial class cAvisosTipos : CompontesBasicos.ComponenteGradeNavegador
    {
        private Framework.datasets.dAvisos dAvisos1;

        /// <summary>
        /// 
        /// </summary>
        public cAvisosTipos()
        {
            InitializeComponent();
            uSUariosBindingSource.DataSource = FrameworkProc.datasets.dUSUarios.dUSUariosSt;
            dAvisos1 = new Framework.datasets.dAvisos();
            dAvisosBindingSource.DataSource = dAvisos1;
            TableAdapterPrincipal = dAvisos1.TipoAVisoTableAdapter;
            virEnumATVTipo.virEnumATVTipoSt.CarregaEditorDaGrid(colTAV);
            virEnumATVClasse.virEnumATVClasseSt.CarregaEditorDaGrid(colTAVClasse);
            //tableAdapterSelect = dAvisos1.AVIsosTableAdapter;
        }

        private void cAvisosTipos_cargaFinal(object sender, EventArgs e)
        {
            System.Collections.ArrayList Alertas = new System.Collections.ArrayList(new ATVTipo[] { ATVTipo.ChequeDuplicidade, ATVTipo.ChequeNaoEncontrado, ATVTipo.ChequeValor, ATVTipo.CondominioDesativado, ATVTipo.ISSRetificar, ATVTipo.SEFIPRetificar,ATVTipo.DebitoAutomaticoNaoOcorreu,ATVTipo.DebitoAutomaticoNaoPrevisto });
            foreach (ATVTipo ATVx in Enum.GetValues(typeof(ATVTipo)))
            {
                if (dAvisos1.TipoAViso.FindByTAV((int)ATVx) == null)
                {
                    dAvisos.TipoAVisoRow novaRow = dAvisos1.TipoAViso.NewTipoAVisoRow();
                    novaRow.TAV = (int)ATVx;
                    novaRow.TAV_USU = 30;
                    novaRow.TAVdiasaviso = 3;
                    novaRow.TAVClasse = (int)(Alertas.Contains((ATVTipo)ATVx) ? ATVClasse.Alerta : ATVClasse.Aviso);
                    dAvisos1.TipoAViso.AddTipoAVisoRow(novaRow);
                }
            }
            dAvisos1.TipoAVisoTableAdapter.Update(dAvisos1.TipoAViso);
            dAvisos1.TipoAViso.AcceptChanges();

        }

        private void GridView_F_RowCellStyle(object sender, DevExpress.XtraGrid.Views.Grid.RowCellStyleEventArgs e)
        {            
            if ((e.RowHandle >= 0) && (e.Column == colTAVClasse))
            {
                e.Appearance.BackColor = virEnumATVClasse.virEnumATVClasseSt.GetCor((ATVClasse)e.CellValue);
                e.Appearance.ForeColor = Color.Black;
            }
        }
    }
}
