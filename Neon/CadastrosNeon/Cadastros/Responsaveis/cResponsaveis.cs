﻿using System;
using System.Collections.Generic;
using CompontesBasicos;
using System.Windows.Forms;

namespace Cadastros.Responsaveis
{
    /// <summary>
    /// 
    /// </summary>
    public partial class cResponsaveis : ComponenteBase
    {
        
        /// <summary>
        /// 
        /// </summary>
        public cResponsaveis()
        {
            InitializeComponent();
            
        }

        private void lookupUSUAtivos1_alterado(object sender, EventArgs e)
        {
            Habilitar();
        }

        private void Habilitar()
        {
            if ((!chBalancete.Checked) && (!chGerente.Checked) && (!chCobranca.Checked))
                simpleButton1.Enabled = false;
            else
                if ((lookupUSUAtivos1.USUSel == -1) || (lookupUSUAtivos2.USUSel == -1))
                    simpleButton1.Enabled = false;
                else
                    if (CompontesBasicos.Bancovirtual.UsuarioLogado.UsuarioLogadoSt.VerificaFuncionalidade("GERÊNCIA") < 2)
                        simpleButton1.Enabled = false;
                    else
                        simpleButton1.Enabled = true;
        }

        private void lookupUSUAtivos2_alterado(object sender, EventArgs e)
        {
            Habilitar();
        }

        private void chGerente_CheckedChanged(object sender, EventArgs e)
        {
            Habilitar();
        }

        private void AlteraUm(string Campo,string CampoTXT)
        {           
            string CONHistoricoAdd = string.Format("\r\n{0:dd/MM/yyyy HH:mm:ss} - Cadastro alterado em lote por {1}\r\n{2}: {3} -> {4}\r\n", 
                                                    DateTime.Now,
                                                    Framework.usuarioLogado.UsuarioLogadoNeon.UsuarioLogadoStNEON.USUNome,
                                                    CampoTXT,
                                                    lookupUSUAtivos1.USUNomeSel,
                                                    lookupUSUAtivos2.USUNomeSel);
            string Comando = string.Format("update CONDOMINIOS set {0} = @P1,CONHistorico = CONHistorico + @P2 where {0} = @P3",Campo);
            VirMSSQL.TableAdapter.STTableAdapter.ExecutarSQLNonQuery(Comando, lookupUSUAtivos2.USUSel, CONHistoricoAdd, lookupUSUAtivos1.USUSel);
        }

        private void simpleButton1_Click(object sender, EventArgs e)
        {
            DevExpress.XtraEditors.CheckEdit[] Chs = new DevExpress.XtraEditors.CheckEdit[] {chGerente,chCobranca,chBalancete };
            string[] Campo = new string[] { "CONAuditor1_USU", "CONCobranca_USU", "CONBalancete_USU" };
            string[] CampoTXT = new string[] { "Gerente", "Cobrança", "Balancete" };
            
            if (VirInput.Input.Senha("ATENÇÃO esta ação é irreversível. Senha:") == "CUIDADO")
            {
                for (int i = 0; i < 3; i++)
                    AlteraUm(Campo[i], CampoTXT[i]);
                MessageBox.Show("Ok.");
            }
            else
                MessageBox.Show("Senha incorreta.");
        }
    }
}
