﻿namespace Cadastros.Responsaveis
{
    partial class cResponsaveis
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(cResponsaveis));
            this.tipoAVisoBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.groupControl2 = new DevExpress.XtraEditors.GroupControl();
            this.simpleButton1 = new DevExpress.XtraEditors.SimpleButton();
            this.chBalancete = new DevExpress.XtraEditors.CheckEdit();
            this.chCobranca = new DevExpress.XtraEditors.CheckEdit();
            this.chGerente = new DevExpress.XtraEditors.CheckEdit();
            this.lookupUSUAtivos2 = new Framework.Lookup.LookupUSUAtivos();
            this.lookupUSUAtivos1 = new Framework.Lookup.LookupUSUAtivos();
            this.labelControl3 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl2 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl1 = new DevExpress.XtraEditors.LabelControl();
            ((System.ComponentModel.ISupportInitialize)(this.StyleController_F)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tipoAVisoBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl2)).BeginInit();
            this.groupControl2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.chBalancete.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chCobranca.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chGerente.Properties)).BeginInit();
            this.SuspendLayout();
            // 
            // ToolTipController_F
            // 
            this.ToolTipController_F.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.ToolTipController_F.Appearance.ForeColor = System.Drawing.Color.Black;
            this.ToolTipController_F.Appearance.Options.UseBackColor = true;
            this.ToolTipController_F.Appearance.Options.UseFont = true;
            this.ToolTipController_F.Appearance.Options.UseForeColor = true;
            this.ToolTipController_F.Appearance.Options.UseTextOptions = true;
            this.ToolTipController_F.Appearance.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            // 
            // StyleController_F
            // 
            this.StyleController_F.AppearanceDisabled.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this.StyleController_F.AppearanceDisabled.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this.StyleController_F.AppearanceDisabled.Options.UseBackColor = true;
            this.StyleController_F.AppearanceDisabled.Options.UseForeColor = true;
            this.StyleController_F.AppearanceFocused.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.StyleController_F.AppearanceFocused.Options.UseBackColor = true;
        
            // 
            // tipoAVisoBindingSource
            // 
            this.tipoAVisoBindingSource.DataMember = "TipoAViso";
            this.tipoAVisoBindingSource.DataSource = typeof(Framework.datasets.dAvisos);
            // 
            // groupControl2
            // 
            this.groupControl2.Controls.Add(this.simpleButton1);
            this.groupControl2.Controls.Add(this.chBalancete);
            this.groupControl2.Controls.Add(this.chCobranca);
            this.groupControl2.Controls.Add(this.chGerente);
            this.groupControl2.Controls.Add(this.lookupUSUAtivos2);
            this.groupControl2.Controls.Add(this.lookupUSUAtivos1);
            this.groupControl2.Controls.Add(this.labelControl3);
            this.groupControl2.Controls.Add(this.labelControl2);
            this.groupControl2.Controls.Add(this.labelControl1);
            this.groupControl2.Dock = System.Windows.Forms.DockStyle.Top;
            this.groupControl2.Location = new System.Drawing.Point(0, 0);
            this.groupControl2.Name = "groupControl2";
            this.groupControl2.Size = new System.Drawing.Size(0, 148);
            this.groupControl2.TabIndex = 2;
            this.groupControl2.Text = "Substituição de Responsáveis em massa";
            // 
            // simpleButton1
            // 
            this.simpleButton1.Appearance.Font = new System.Drawing.Font("Tahoma", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.simpleButton1.Appearance.Options.UseFont = true;
            this.simpleButton1.Enabled = false;
            this.simpleButton1.Image = ((System.Drawing.Image)(resources.GetObject("simpleButton1.Image")));
            this.simpleButton1.Location = new System.Drawing.Point(430, 30);
            this.simpleButton1.Name = "simpleButton1";
            this.simpleButton1.Size = new System.Drawing.Size(213, 101);
            this.simpleButton1.TabIndex = 8;
            this.simpleButton1.Text = "Substituir todos";
            this.simpleButton1.Click += new System.EventHandler(this.simpleButton1_Click);
            // 
            // chBalancete
            // 
            this.chBalancete.Location = new System.Drawing.Point(162, 112);
            this.chBalancete.Name = "chBalancete";
            this.chBalancete.Properties.Caption = "Balancete";
            this.chBalancete.Size = new System.Drawing.Size(75, 19);
            this.chBalancete.TabIndex = 7;
            this.chBalancete.CheckedChanged += new System.EventHandler(this.chGerente_CheckedChanged);
            // 
            // chCobranca
            // 
            this.chCobranca.Location = new System.Drawing.Point(293, 87);
            this.chCobranca.Name = "chCobranca";
            this.chCobranca.Properties.Caption = "Cobrança";
            this.chCobranca.Size = new System.Drawing.Size(75, 19);
            this.chCobranca.TabIndex = 6;
            this.chCobranca.CheckedChanged += new System.EventHandler(this.chGerente_CheckedChanged);
            // 
            // chGerente
            // 
            this.chGerente.Location = new System.Drawing.Point(162, 87);
            this.chGerente.Name = "chGerente";
            this.chGerente.Properties.Caption = "Gerente";
            this.chGerente.Size = new System.Drawing.Size(75, 19);
            this.chGerente.TabIndex = 5;
            this.chGerente.CheckedChanged += new System.EventHandler(this.chGerente_CheckedChanged);
            // 
            // lookupUSUAtivos2
            // 
            this.lookupUSUAtivos2.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.lookupUSUAtivos2.Appearance.Options.UseBackColor = true;
            this.lookupUSUAtivos2.CaixaAltaGeral = true;
            this.lookupUSUAtivos2.Doc = System.Windows.Forms.DockStyle.None;
            this.lookupUSUAtivos2.Estado = CompontesBasicos.EstadosDosComponentes.Registrado;
            this.lookupUSUAtivos2.Location = new System.Drawing.Point(113, 61);
            this.lookupUSUAtivos2.LookAndFeel.UseDefaultLookAndFeel = false;
            this.lookupUSUAtivos2.Name = "lookupUSUAtivos2";
            this.lookupUSUAtivos2.Size = new System.Drawing.Size(290, 20);
            this.lookupUSUAtivos2.somenteleitura = false;
            this.lookupUSUAtivos2.SoUsuariosAtivos = true;
            this.lookupUSUAtivos2.TabIndex = 4;
            this.lookupUSUAtivos2.Titulo = resources.GetString("lookupUSUAtivos2.Titulo");            
            this.lookupUSUAtivos2.USUSel = -1;
            this.lookupUSUAtivos2.alterado += new System.EventHandler(this.lookupUSUAtivos2_alterado);
            // 
            // lookupUSUAtivos1
            // 
            this.lookupUSUAtivos1.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.lookupUSUAtivos1.Appearance.Options.UseBackColor = true;
            this.lookupUSUAtivos1.CaixaAltaGeral = true;
            this.lookupUSUAtivos1.Doc = System.Windows.Forms.DockStyle.None;
            this.lookupUSUAtivos1.Estado = CompontesBasicos.EstadosDosComponentes.Registrado;
            this.lookupUSUAtivos1.Location = new System.Drawing.Point(113, 34);
            this.lookupUSUAtivos1.LookAndFeel.UseDefaultLookAndFeel = false;
            this.lookupUSUAtivos1.Name = "lookupUSUAtivos1";
            this.lookupUSUAtivos1.Size = new System.Drawing.Size(290, 20);
            this.lookupUSUAtivos1.somenteleitura = false;
            this.lookupUSUAtivos1.SoUsuariosAtivos = false;
            this.lookupUSUAtivos1.TabIndex = 3;
            this.lookupUSUAtivos1.Titulo = resources.GetString("lookupUSUAtivos1.Titulo");            
            this.lookupUSUAtivos1.USUSel = -1;
            this.lookupUSUAtivos1.alterado += new System.EventHandler(this.lookupUSUAtivos1_alterado);
            // 
            // labelControl3
            // 
            this.labelControl3.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl3.Location = new System.Drawing.Point(13, 87);
            this.labelControl3.Name = "labelControl3";
            this.labelControl3.Size = new System.Drawing.Size(123, 19);
            this.labelControl3.TabIndex = 2;
            this.labelControl3.Text = "nas atividades:";
            // 
            // labelControl2
            // 
            this.labelControl2.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl2.Location = new System.Drawing.Point(13, 61);
            this.labelControl2.Name = "labelControl2";
            this.labelControl2.Size = new System.Drawing.Size(27, 19);
            this.labelControl2.TabIndex = 1;
            this.labelControl2.Text = "por";
            // 
            // labelControl1
            // 
            this.labelControl1.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl1.Location = new System.Drawing.Point(13, 34);
            this.labelControl1.Name = "labelControl1";
            this.labelControl1.Size = new System.Drawing.Size(79, 19);
            this.labelControl1.TabIndex = 0;
            this.labelControl1.Text = "Substituir";
            // 
            // cResponsaveis
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.groupControl2);
            this.Doc = System.Windows.Forms.DockStyle.Fill;
            this.LookAndFeel.UseDefaultLookAndFeel = false;
            this.Name = "cResponsaveis";
            this.Size = new System.Drawing.Size(0, 0);
            this.Titulo = "CompontesBasicos.ComponenteBase - CompontesBasicos.ComponenteBase - Responsáveis";
            ((System.ComponentModel.ISupportInitialize)(this.StyleController_F)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tipoAVisoBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl2)).EndInit();
            this.groupControl2.ResumeLayout(false);
            this.groupControl2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.chBalancete.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chCobranca.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chGerente.Properties)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraEditors.GroupControl groupControl2;
        private DevExpress.XtraEditors.LabelControl labelControl3;
        private DevExpress.XtraEditors.LabelControl labelControl2;
        private DevExpress.XtraEditors.LabelControl labelControl1;
        private DevExpress.XtraEditors.CheckEdit chBalancete;
        private DevExpress.XtraEditors.CheckEdit chCobranca;
        private DevExpress.XtraEditors.CheckEdit chGerente;
        private Framework.Lookup.LookupUSUAtivos lookupUSUAtivos2;
        private Framework.Lookup.LookupUSUAtivos lookupUSUAtivos1;
        private DevExpress.XtraEditors.SimpleButton simpleButton1;
        private System.Windows.Forms.BindingSource tipoAVisoBindingSource;
    }
}
