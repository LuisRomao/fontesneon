﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace Cadastros
{
    /// <summary>
    /// Configuração da estação
    /// </summary>
    public partial class cConfiguraEstacao : CompontesBasicos.ComponenteBase
    {
        /// <summary>
        /// Construtor
        /// </summary>
        public cConfiguraEstacao()
        {
            InitializeComponent();
            Estado = CompontesBasicos.EstadosDosComponentes.JanelasAtivas;
            if ((Framework.usuarioLogado.UsuarioLogadoNeon.UsuarioLogadoSt != null) && (Framework.DSCentral.USU == 30))
                simpleButton2.Visible = true;
        }

        private void cConfiguraEstacao_Load(object sender, EventArgs e)
        {            
            radioFilial.EditValue = CompontesBasicos.dEstacao.dEstacaoSt.GetValor("EMP", 1);
            radioQAS.EditValue = CompontesBasicos.dEstacao.dEstacaoSt.GetValor("QAS", false);
            Estado = CompontesBasicos.EstadosDosComponentes.JanelasAtivas;
        }
        
        private void simpleButton1_Click(object sender, EventArgs e)
        {
            CompontesBasicos.dEstacao.dEstacaoSt.SetValor("EMP", radioFilial.EditValue.ToString());
            CompontesBasicos.dEstacao.dEstacaoSt.SetValor("QAS", radioQAS.EditValue);
            MessageBox.Show("Válido após a reinicialização");
            CompontesBasicos.FormPrincipalBase.FormPrincipal.Close();
        }

        private void simpleButton2_Click(object sender, EventArgs e)
        {
            bool Cadastrar = false;
            DataTable DT = new DataTable("ValorePadrão");
            DT.Columns.Add("RAV", typeof(int));
            DT.Columns.Add("RAVDescricao", typeof(string));
            foreach (VirEnumeracoesNeon.RAVPadrao RAV in Enum.GetValues(typeof(VirEnumeracoesNeon.RAVPadrao)))
            {
                if (!VirMSSQL.TableAdapter.STTableAdapter.EstaCadastrado("select RAV from RamoAtiVidade where RAV = @P1",RAV))
                {
                    Cadastrar = true;
                    DataRow DR = DT.NewRow();
                    DR["RAV"] = RAV;                     
                    DR["RAVDescricao"] = Framework.Enumeracoes.virEnumRAVPadrao.virEnumRAVPadraoSt.Nomes.ContainsKey((int)RAV) ? Framework.Enumeracoes.virEnumRAVPadrao.virEnumRAVPadraoSt.Nomes[(int)RAV] : RAV.ToString();
                    DT.Rows.Add(DR);
                }
            }
            if (Cadastrar)
                VirMSSQL.TableAdapter.STTableAdapter.Insert("RamoAtiVidade", DT, true, false);
        }
    }
}
