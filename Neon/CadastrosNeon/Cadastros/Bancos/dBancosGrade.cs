﻿namespace Cadastros.Bancos {
    
    
    public partial class dBancosGrade 
    {
        /// <summary>
        /// função pra correção dos daos nao cadastrado
        /// </summary>
        public void Correcao()
        {
            dBancosGradeTableAdapters.ContaCorrenTeTableAdapter TACCT = new dBancosGradeTableAdapters.ContaCorrenTeTableAdapter();
            TACCT.TrocarStringDeConexao();
            if (TACCT.Fill(ContaCorrenTe) > 0)
            {
                dBancosGrade.AGEnciaRow rowAGE;
                System.Data.DataView DV = new System.Data.DataView(AGEncia, "", "AGE_BCO,AGENumero", System.Data.DataViewRowState.CurrentRows);
                foreach (dBancosGrade.ContaCorrenTeRow rowCCT in ContaCorrenTe)
                {
                    int ind = DV.Find(new object[] { rowCCT.CCT_BCO, rowCCT.CCTAgencia });
                    if (ind >= 0)
                        rowAGE = (dBancosGrade.AGEnciaRow)DV[ind].Row;
                    else
                    {
                        rowAGE = AGEncia.NewAGEnciaRow();
                        rowAGE.AGE_BCO = rowCCT.CCT_BCO;
                        if (!rowCCT.IsCCTAgenciaDgNull())
                            rowAGE.AGEDg = rowCCT.CCTAgenciaDg;
                        rowAGE.AGENumero = rowCCT.CCTAgencia;
                        AGEncia.AddAGEnciaRow(rowAGE);                        
                    }
                    AGEnciaTableAdapter.Update(rowAGE);
                    rowCCT.CCT_AGE = rowAGE.AGE;                        
                }
                TACCT.Update(ContaCorrenTe);
                ContaCorrenTe.AcceptChanges();
            }
        }

        

        private dBancosGradeTableAdapters.BANCOSTableAdapter bANCOSTableAdapter;
        /// <summary>
        /// TableAdapter padrão para tabela: BANCOS
        /// </summary>
        public dBancosGradeTableAdapters.BANCOSTableAdapter BANCOSTableAdapter
        {
            get
            {
                if (bANCOSTableAdapter == null)
                {
                    bANCOSTableAdapter = new dBancosGradeTableAdapters.BANCOSTableAdapter();
                    bANCOSTableAdapter.TrocarStringDeConexao();
                };
                return bANCOSTableAdapter;
            }
        }

        private dBancosGradeTableAdapters.AGEnciaTableAdapter aGEnciaTableAdapter;
        /// <summary>
        /// TableAdapter padrão para tabela: AGEncia
        /// </summary>
        public dBancosGradeTableAdapters.AGEnciaTableAdapter AGEnciaTableAdapter
        {
            get
            {
                if (aGEnciaTableAdapter == null)
                {
                    aGEnciaTableAdapter = new dBancosGradeTableAdapters.AGEnciaTableAdapter();
                    aGEnciaTableAdapter.TrocarStringDeConexao();
                };
                return aGEnciaTableAdapter;
            }
        }
    }
}
