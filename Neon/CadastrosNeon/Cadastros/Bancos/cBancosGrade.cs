using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace Cadastros.Bancos
{
    /// <summary>
    /// 
    /// </summary>
    public partial class cBancosGrade : CompontesBasicos.ComponenteGradeNavegador
    {
        /// <summary>
        /// 
        /// </summary>
        public cBancosGrade()
        {
            InitializeComponent();
            TableAdapterPrincipal = dBancosGrade.BANCOSTableAdapter;
            tableAdapterSelect = dBancosGrade.BANCOSTableAdapter;
        }                        
        

        private void cBancosGrade_cargaFinal(object sender, EventArgs e)
        {
            dBancosGrade.AGEnciaTableAdapter.Fill(dBancosGrade.AGEncia);
            dBancosGrade.Correcao();
        }

        private void gridView1_RowUpdated(object sender, DevExpress.XtraGrid.Views.Base.RowObjectEventArgs e)
        {
            if (e.Row == null)
                return;
            DataRowView DRV = (DataRowView)e.Row;
            dBancosGrade.AGEnciaRow rowAGE = (dBancosGrade.AGEnciaRow)DRV.Row;
            dBancosGrade.AGEnciaTableAdapter.Update(rowAGE);
            rowAGE.AcceptChanges();
        }
    }
}

