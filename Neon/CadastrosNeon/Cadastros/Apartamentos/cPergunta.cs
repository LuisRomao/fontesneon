using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Text;
using System.Windows.Forms;

namespace Cadastros.Apartamentos
{
    /// <summary>
    /// 
    /// </summary>
    public partial class cPergunta : CompontesBasicos.ComponenteBaseDialog
    {
        /// <summary>
        /// 
        /// </summary>
        /// <param name="inquilino"></param>
        public cPergunta(bool inquilino)
        {
            InitializeComponent();
            if (inquilino)
            {
                radioGroup1.Properties.Items[0].Description = "A unidade foi alugada para outra pessoa.";
                radioGroup1.Properties.Items[1].Description = "Quero corrigir o cadastro do inquilino.";
            }
        }

       
    }
}
