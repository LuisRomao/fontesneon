using Cadastros.Condominios;
using System;
using System.Collections;
using System.Data;
using System.IO;
using System.Windows.Forms;
using FrameworkProc.objetosNeon;
using DevExpress.XtraEditors;
using CompontesBasicos;
using VirEnumeracoes;
using VirEnumeracoesNeon;
using CadastrosProc;

namespace Cadastros.Apartamentos
{
    enum Tarefas
    {
        Carrega,
        endedit,
        CanClose
    }

    /// <summary>
    /// 
    /// </summary>
    public partial class cApartamentosCampos : CompontesBasicos.ComponenteCamposGroupTabControl
    {        

        private static int staticR;

        private Apartamento _Apartamento;

        private int? _CON_PSC;

        //private WSPublica3.WSPublica3 _WS;

        /// <summary>
        /// 
        /// </summary>
        public dApartamentos.APARTAMENTOSRow LinhaMae;
        /// <summary>
        /// 
        /// </summary>
        public dApartamentos.PessoasRow LinhaPESI;
        /// <summary>
        /// 
        /// </summary>
        public dApartamentos.PessoasRow LinhaPESP;

        /// <summary>
        /// 
        /// </summary>
        public cApartamentosCampos()
        {
            InitializeComponent();
        }

        
        private ContaCorrenteNeon contaCorrenteNeon;

        internal ContaCorrenteNeon ContaCorrenteNeon
        {
            get
            {                
                return contaCorrenteNeon ?? (contaCorrenteNeon = new ContaCorrenteNeon(LinhaMae.CON, TipoChaveContaCorrenteNeon.CON));
            }
        }

        private void cApartamentosCampos_cargaInicial(object sender, EventArgs e)
        {
            if (pk > 0)
                if (dApartamentos.APARTAMENTOSTableAdapter.FillCompleto(dApartamentos.APARTAMENTOS, pk) == 1)
                {
                    dApartamentos.Pessoas.Clear();
                    LinhaMae_F = dApartamentos.APARTAMENTOS[0];
                    LinhaMae = dApartamentos.APARTAMENTOS[0];
                    CarregaPES(false);
                    dApartamentos.APTPGETableAdapter.Fill(dApartamentos.APTPGE, LinhaMae.APT, LinhaMae.CON);
                }
        }

        private byte[] Criptografa()
        {
            string Aberto = string.Format("{0:yyyyMMddHHmmss}{1:000}{2:00000000}<-*W*->", DateTime.Now, Framework.DSCentral.EMP, staticR++);
            return VirCrip.VirCripWS.Crip(Aberto);
        }

        private Apartamento Apartamento
        {
            get
            {
                if (_Apartamento == null)
                    _Apartamento = new Apartamento(LinhaMae);
                return _Apartamento;
            }
        }

        private TipoSeguro TipoS
        {
            get
            {
                if (CON_PSC.HasValue)
                    if (LinhaMae.APTSeguro)
                        return TipoSeguro.Dentro;
                    else
                        if (LinhaMae.IsAPT_PSCNull())
                        return TipoSeguro.Sem;
                    else
                        return TipoSeguro.Fora;
                else
                    if (LinhaMae.IsAPT_PSCNull())
                    return TipoSeguro.Sem;
                else
                    return TipoSeguro.Fora;
            }
        }

        internal string NovoUsuario(string Nome)
        {
            int Tentativa = 1;
            string strBase = (Nome.Length >= 6) ? Nome.Substring(0, 6).Trim().ToUpper() : Nome.Trim().ToUpper();
            if (strBase.Contains(" "))
                strBase = strBase.Substring(0, strBase.IndexOf(" "));
            string Usuario;
            do
            {
                Usuario = string.Format("{0}{1}{2}", Framework.DSCentral.EMP, strBase, Tentativa);
                Tentativa++;
            }
            while (VirMSSQL.TableAdapter.STTableAdapter.EstaCadastrado("select APT from AParTamentos where APTUsuarioInternetProprietario = @P1 or APTUsuarioInternetInquilino  = @P1", Usuario));
            return Usuario;
        }

        internal int? CON_PSC
        {
            set
            {
                _CON_PSC = value;
            }
            get
            {
                if (!_CON_PSC.HasValue)
                    _CON_PSC = VirMSSQL.TableAdapter.STTableAdapter.BuscaEscalar_int("select CON_PSC from Condominios where CON = @P1", LinhaMae.CON);
                return _CON_PSC;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public override bool CanClose()
        {
            Validate();
            LinhaMae.EndEdit();
            return ((TarefaParaTodos(Tarefas.CanClose)) && (LinhaMae_F.RowState == DataRowState.Unchanged));
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="recarga"></param>
        public void CarregaPES(bool recarga)
        {

            if (!LinhaMae.IsAPTProprietario_PESNull())
            {
                dApartamentos.Pessoas.Clear();
                int PESInquilino = LinhaMae.IsAPTInquilino_PESNull() ? -1 : LinhaMae.APTInquilino_PES;
                dApartamentos.PessoasTableAdapter.Fill(dApartamentos.Pessoas, LinhaMae.APTProprietario_PES, PESInquilino);
                LinhaPESP = dApartamentos.Pessoas.FindByPES(LinhaMae.APTProprietario_PES);
                LinhaPESI = dApartamentos.Pessoas.FindByPES(PESInquilino);
                if (recarga)
                    TarefaParaTodos(Tarefas.Carrega);
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="Recarregar"></param>
        public void Grava(bool Recarregar)
        {

            AjustaInclusaiAlteracao();



            SortedList CamposAlterados = VirDB.VirtualTableAdapter.CamposEfetivamenteAlterados(LinhaMae);
            if (CamposAlterados.Count > 0)
            {
                string Just = String.Format("\r\n\r\n{0} - Cadastro alterado por {1}\r\nAltera��es:", DateTime.Now, CompontesBasicos.Bancovirtual.UsuarioLogado.UsuarioLogadoSt.USUNome);
                if (LinhaMae.HasVersion(DataRowVersion.Original))
                {
                    foreach (DictionaryEntry Alterado in CamposAlterados)
                    {
                        string Chave = (string)Alterado.Key;
                        if (Chave == "APTDataAtualizacao")
                            continue;
                        if ((Chave == "APT_PSC") || (Chave == "APTSeguro"))
                            Just += String.Format("\r\nSeguro Conte�do: - > {0}", TipoS);
                        else
                            Just += String.Format("\r\n{0}: {1} - > {2}", Alterado.Key, LinhaMae[Alterado.Key.ToString(), DataRowVersion.Original], LinhaMae[Alterado.Key.ToString()]);
                    }
                    if (LinhaMae.IsAPTHistoricoNull())
                        LinhaMae.APTHistorico = Just;
                    else
                        LinhaMae.APTHistorico += Just;
                };
            }
        }

        /*
        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public WSPublica3.dAPartamentoDoc PegaListaDocs()
        {
            return WS.ListaAPT(Criptografa(), LinhaMae.APT);
        }*/

        private ClientWCFFtp.ClientFTP _clientFTP;

        private ClientWCFFtp.ClientFTP clientFTP
        {
            get
            {
                if (_clientFTP == null)
                    _clientFTP = new ClientWCFFtp.ClientFTP(CompontesBasicosProc.FuncoesBasicasProc.EstaEmProducao, Framework.DSCentral.USU, Framework.DSCentral.EMP);
                return _clientFTP;
            }
        }

        internal void AjustaCHDebitoAutomatico(bool Proprietario, ref CheckEdit CHDeb, ref ContaCorrenteNeon ContaDebito)
        {
            if (!CHDeb.Checked)
            {
                if (MessageBox.Show("Confirma o cancelamento do d�bito autom�tico?", "Confirma��o", MessageBoxButtons.YesNo, MessageBoxIcon.Warning, MessageBoxDefaultButton.Button2) == DialogResult.Yes)
                {
                    if (Proprietario)
                        LinhaMae.SetAPTDebitoP_CCGNull();
                    else
                        LinhaMae.SetAPTDebitoI_CCGNull();
                    ContaDebito = null;
                }
            }
            else
            {
                dApartamentos.PessoasRow LinhaPES;
                if (Proprietario)
                {
                    if (LinhaPESP == null)
                    {
                        MessageBox.Show("Propriet�rio n�o informado");
                        return;
                    }
                    LinhaPES = LinhaPESP;                    
                }
                else
                {
                    if (LinhaPESI == null)
                    {
                        MessageBox.Show("Inquilino n�o informado");
                        return;
                    }
                    LinhaPES = LinhaPESI;                    
                }
                if (LinhaPES.IsPESCpfCnpjNull())
                {
                    MessageBox.Show("CPF/CNPJ n�o informado");
                    return;
                }
                try
                {                    
                    DocBacarios.CPFCNPJ CPF = new DocBacarios.CPFCNPJ(LinhaPES.PESCpfCnpj);
                    cContaCorrenteNeon cContaCorrenteNeon1 = new cContaCorrenteNeon();
                    cContaCorrenteNeon1.TravaBanco(237);
                    cContaCorrenteNeon1.SetDefaults(LinhaPES.PESNome, CPF);
                    if (cContaCorrenteNeon1.VirShowModulo(CompontesBasicos.EstadosDosComponentes.PopUp) == DialogResult.OK)
                    {
                        cPublicaDoc Editor = new cPublicaDoc();
                        Editor.XTipo.EditValue = (int)TiposAPTDOC.debito_automatico;
                        Editor.XTipo.Properties.ReadOnly = true;
                        Editor.XDescricao.Text = string.Format("Autoriza��o para d�bito autom�tico - {0:dd/MM/yyyy}", DateTime.Today);
                        Editor.XDescricao.Properties.ReadOnly = true;
                        if ((Editor.VirShowModulo(EstadosDosComponentes.PopUp) == DialogResult.OK) && (Editor.XArquivo.Text != string.Empty))
                        {

                            Publica(Editor.XArquivo.Text, Editor.XDescricao.Text, (TiposAPTDOC)Editor.XTipo.EditValue);
                            if (Proprietario)
                                LinhaMae.APTDebitoP_CCG = cContaCorrenteNeon1.contaCorrenteNeon1.CCG.Value;
                            else
                                LinhaMae.APTDebitoI_CCG = cContaCorrenteNeon1.contaCorrenteNeon1.CCG.Value;
                            ContaDebito = cContaCorrenteNeon1.ContaCorrenteNeon1;
                        }
                    }
                }
                catch (Exception ex)
                {
                    CHDeb.Checked = false;
                    ContaDebito = null;
                    throw new Exception(string.Format("Erro na publica��o: {0}",ex.Message), ex);
                }
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="Arquivo"></param>
        /// <param name="Descricao"></param>
        /// <param name="Tipo"></param>
        public bool Publica(string Arquivo, string Descricao, TiposAPTDOC Tipo)
        {
            dApartamentos.APartamentoDocRow novarow = dApartamentos.APartamentoDoc.NewAPartamentoDocRow();            
            try
            {
                VirMSSQL.TableAdapter.AbreTrasacaoSQL("Cadastros.apartamentos.capartamentoscampos 251",dApartamentos.APartamentoDocTableAdapter);
                novarow.APD_APT = LinhaMae.APT;
                novarow.APD_USU = Framework.usuarioLogado.UsuarioLogadoNeon.UsuarioLogadoSt.USU;
                novarow.APDData = DateTime.Now;
                novarow.APDDescritivo = Descricao;
                novarow.APDEXT = Path.GetExtension(Arquivo).Substring(1);
                if (Tipo == TiposAPTDOC.advertencia)
                    novarow.APDPublico = true;
                else
                    novarow.APDPublico = false;
                novarow.APDTipo = (int)Tipo;
                dApartamentos.APartamentoDoc.AddAPartamentoDocRow(novarow);
                dApartamentos.APartamentoDocTableAdapter.Update(novarow);
                string ArquivoRemoto = string.Format(@"APTDOC{0}.{1}", novarow.APD, novarow.APDEXT);
                if (!clientFTP.Put(Arquivo, ArquivoRemoto))
                    throw new Exception(string.Format("Erro na publica��o: {0}",clientFTP.UltimoErro));                
                VirMSSQL.TableAdapter.CommitSQL();
                return true;
            }
            catch (Exception ex)
            {
                VirMSSQL.TableAdapter.VircatchSQL(ex);                
                throw new Exception(string.Format("Erro na publica��o: {0}", ex.Message),ex);
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public override bool Update_F()
        {
            if (Validate() && CamposObrigatoriosOk(BindingSource_F))
            {
                TarefaParaTodos(Tarefas.endedit);
                LinhaMae.EndEdit();
                if (LinhaMae.RowState == DataRowState.Unchanged)
                    return true;
                if (!LinhaMae.IsAPTInquilino_PESNull())
                {
                    if (LinhaMae.IsAPTFormaPagtoInquilino_FPGNull())
                        LinhaMae.APTFormaPagtoInquilino_FPG = (int)FormaPagamento.EntreigaPessoal;
                    if (LinhaMae.IsAPTInquilinoPagaCondominioNull())
                        LinhaMae.APTInquilinoPagaCondominio = true;
                    if (LinhaMae.IsAPTInquilinoPagaFRNull())
                        LinhaMae.APTInquilinoPagaFR = false;
                }
                Grava(false);
                dApartamentos.APARTAMENTOSTableAdapter.Update(LinhaMae);
                LinhaMae.AcceptChanges();
                //aPTPGEBindingSource.EndEdit();


                //Utilitarios.MSAccess.fExportToMsAccess.ExportarApartamentosParaMsAccessRowAP(LinhaMae.CONCodigo, LinhaMae.BLOCodigo, LinhaMae);
                //Utilitarios.MSAccess.fExportToMsAccess exp = new Utilitarios.MSAccess.fExportToMsAccess();
                //if (!LinhaMae.IsAPTProprietario_PESNull())
                //    exp.ExportarInquilinoProprietarioParaMsAccess(true, LinhaMae.APTProprietario_PES, LinhaMae.CONCodigo, LinhaMae.BLOCodigo, LinhaMae.APTNumero, LinhaMae.APTFormaPagtoProprietario_FPG);
                //if (!LinhaMae.IsAPTInquilino_PESNull())
                //    exp.ExportarInquilinoProprietarioParaMsAccess(false, LinhaMae.APTInquilino_PES, LinhaMae.CONCodigo, LinhaMae.BLOCodigo, LinhaMae.APTNumero, LinhaMae.APTFormaPagtoInquilino_FPG);
                //else
                //    Utilitarios.MSAccess.fExportToMsAccess.ApagaInquilinoDoAccess(LinhaMae.CONCodigo, LinhaMae.BLOCodigo, LinhaMae.APTNumero);



                return true;
            }
            else
            {
                MessageBox.Show("Existe um campo obrigat�rio sem um valor informado.",
                                "Campo Obrigat�rio", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return false;
            }
        }

        /*
        /// <summary>
        /// 
        /// </summary>
        public WSPublica3.WSPublica3 WS
        {
            get
            {
                if (_WS == null)
                {
                    _WS = new Cadastros.WSPublica3.WSPublica3();
                    if (CompontesBasicos.FormPrincipalBase.strEmProducao)
                    {
                        _WS.Proxy = Framework.usuarioLogado.UsuarioLogadoNeon.UsuarioLogadoSt.Proxy;
                        //WS.Url = @"https://ssl493.websiteseguro.com/neonimoveis/neon23/Neononline/WSPublica3.asmx";
                        _WS.Url = @"http://www.neonimoveis.com.br/neon23/Neononline/WSPublica3.asmx";
                    }
                };
                return _WS;
            }
        }*/

        private enum TipoSeguro
        {
            Sem,
            Dentro,
            Fora
        }
    }
}
