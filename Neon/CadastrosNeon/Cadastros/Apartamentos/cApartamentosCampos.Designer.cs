namespace Cadastros.Apartamentos
{
    partial class cApartamentosCampos
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(cApartamentosCampos));
            CompontesBasicos.Classes_de_interface.PaginaTabInfo paginaTabInfo13 = new CompontesBasicos.Classes_de_interface.PaginaTabInfo();
            CompontesBasicos.Classes_de_interface.PaginaTabInfo paginaTabInfo14 = new CompontesBasicos.Classes_de_interface.PaginaTabInfo();
            CompontesBasicos.Classes_de_interface.PaginaTabInfo paginaTabInfo15 = new CompontesBasicos.Classes_de_interface.PaginaTabInfo();
            CompontesBasicos.Classes_de_interface.PaginaTabInfo paginaTabInfo16 = new CompontesBasicos.Classes_de_interface.PaginaTabInfo();
            CompontesBasicos.Classes_de_interface.PaginaTabInfo paginaTabInfo17 = new CompontesBasicos.Classes_de_interface.PaginaTabInfo();
            CompontesBasicos.Classes_de_interface.PaginaTabInfo paginaTabInfo18 = new CompontesBasicos.Classes_de_interface.PaginaTabInfo();
            this.labelControl1 = new DevExpress.XtraEditors.LabelControl();
            this.dApartamentos = new CadastrosProc.dApartamentos();
            this.labelControl2 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl3 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl4 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl5 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl6 = new DevExpress.XtraEditors.LabelControl();
            ((System.ComponentModel.ISupportInitialize)(this.GroupControl_F)).BeginInit();
            this.GroupControl_F.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.TabControl_F)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.BarManager_F)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.BarAndDockingController_F)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.BindingSource_F)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.StyleController_F)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dApartamentos)).BeginInit();
            this.SuspendLayout();
            // 
            // GroupControl_F
            // 
            this.GroupControl_F.Controls.Add(this.labelControl6);
            this.GroupControl_F.Controls.Add(this.labelControl5);
            this.GroupControl_F.Controls.Add(this.labelControl4);
            this.GroupControl_F.Controls.Add(this.labelControl3);
            this.GroupControl_F.Controls.Add(this.labelControl2);
            this.GroupControl_F.Controls.Add(this.labelControl1);
            this.GroupControl_F.Location = new System.Drawing.Point(0, 35);
            this.GroupControl_F.Size = new System.Drawing.Size(1568, 64);
            // 
            // TabControl_F
            // 
            this.TabControl_F.Location = new System.Drawing.Point(0, 99);
            this.TabControl_F.Size = new System.Drawing.Size(1568, 477);
            // 
            // barStatus_F
            // 
            this.barStatus_F.OptionsBar.AllowQuickCustomization = false;
            this.barStatus_F.OptionsBar.DisableClose = true;
            this.barStatus_F.OptionsBar.DisableCustomization = true;
            this.barStatus_F.OptionsBar.DrawDragBorder = false;
            this.barStatus_F.OptionsBar.DrawSizeGrip = true;
            this.barStatus_F.OptionsBar.UseWholeRow = true;
            // 
            // barComponente_F
            // 
            this.barComponente_F.OptionsBar.AllowQuickCustomization = false;
            this.barComponente_F.OptionsBar.DisableClose = true;
            this.barComponente_F.OptionsBar.DisableCustomization = true;
            this.barComponente_F.OptionsBar.DrawDragBorder = false;
            this.barComponente_F.OptionsBar.UseWholeRow = true;
            // 
            // btnConfirmar_F
            // 
            this.btnConfirmar_F.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("btnConfirmar_F.ImageOptions.Image")));
            // 
            // btnCancelar_F
            // 
            this.btnCancelar_F.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("btnCancelar_F.ImageOptions.Image")));
            // 
            // btnFechar_F
            // 
            this.btnFechar_F.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("btnFechar_F.ImageOptions.Image")));
            // 
            // BarAndDockingController_F
            // 
            this.BarAndDockingController_F.PropertiesBar.AllowLinkLighting = false;
            this.BarAndDockingController_F.PropertiesBar.DefaultGlyphSize = new System.Drawing.Size(16, 16);
            this.BarAndDockingController_F.PropertiesBar.DefaultLargeGlyphSize = new System.Drawing.Size(32, 32);
            // 
            // BindingSource_F
            // 
            this.BindingSource_F.DataMember = "APARTAMENTOS";
            this.BindingSource_F.DataSource = this.dApartamentos;
            // 
            // ToolTipController_F
            // 
            this.ToolTipController_F.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(222)))), ((int)(((byte)(237)))), ((int)(((byte)(250)))));
            this.ToolTipController_F.Appearance.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(20)))), ((int)(((byte)(69)))), ((int)(((byte)(141)))));
            this.ToolTipController_F.Appearance.Options.UseBackColor = true;
            this.ToolTipController_F.Appearance.Options.UseFont = true;
            this.ToolTipController_F.Appearance.Options.UseForeColor = true;
            this.ToolTipController_F.Appearance.Options.UseTextOptions = true;
            this.ToolTipController_F.Appearance.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            // 
            // StyleController_F
            // 
            this.StyleController_F.AppearanceDisabled.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.StyleController_F.AppearanceDisabled.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.StyleController_F.AppearanceDisabled.Options.UseBackColor = true;
            this.StyleController_F.AppearanceDisabled.Options.UseForeColor = true;
            this.StyleController_F.AppearanceFocused.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(253)))), ((int)(((byte)(253)))), ((int)(((byte)(181)))));
            this.StyleController_F.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
            this.StyleController_F.AppearanceFocused.Options.UseBackColor = true;
            this.StyleController_F.AppearanceFocused.Options.UseFont = true;
            // 
            // labelControl1
            // 
            this.labelControl1.Appearance.Font = new System.Drawing.Font("Tahoma", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl1.Appearance.Options.UseFont = true;
            this.labelControl1.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.BindingSource_F, "CONCodigo", true));
            this.labelControl1.Location = new System.Drawing.Point(142, 7);
            this.labelControl1.Name = "labelControl1";
            this.labelControl1.Size = new System.Drawing.Size(129, 23);
            this.labelControl1.TabIndex = 0;
            this.labelControl1.Text = "labelControl1";
            // 
            // dApartamentos
            // 
            this.dApartamentos.DataSetName = "dApartamentosCampos";
            this.dApartamentos.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // labelControl2
            // 
            this.labelControl2.Appearance.Font = new System.Drawing.Font("Tahoma", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl2.Appearance.Options.UseFont = true;
            this.labelControl2.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.BindingSource_F, "CONNome", true));
            this.labelControl2.Location = new System.Drawing.Point(289, 7);
            this.labelControl2.Name = "labelControl2";
            this.labelControl2.Size = new System.Drawing.Size(129, 23);
            this.labelControl2.TabIndex = 1;
            this.labelControl2.Text = "labelControl2";
            // 
            // labelControl3
            // 
            this.labelControl3.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl3.Appearance.Options.UseFont = true;
            this.labelControl3.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.BindingSource_F, "BLOCodigo", true));
            this.labelControl3.Location = new System.Drawing.Point(7, 36);
            this.labelControl3.Name = "labelControl3";
            this.labelControl3.Size = new System.Drawing.Size(110, 19);
            this.labelControl3.TabIndex = 2;
            this.labelControl3.Text = "labelControl3";
            // 
            // labelControl4
            // 
            this.labelControl4.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl4.Appearance.Options.UseFont = true;
            this.labelControl4.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.BindingSource_F, "BLONome", true));
            this.labelControl4.Location = new System.Drawing.Point(142, 36);
            this.labelControl4.Name = "labelControl4";
            this.labelControl4.Size = new System.Drawing.Size(110, 19);
            this.labelControl4.TabIndex = 3;
            this.labelControl4.Text = "labelControl4";
            // 
            // labelControl5
            // 
            this.labelControl5.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl5.Appearance.Options.UseFont = true;
            this.labelControl5.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.BindingSource_F, "APTNumero", true));
            this.labelControl5.Location = new System.Drawing.Point(289, 36);
            this.labelControl5.Name = "labelControl5";
            this.labelControl5.Size = new System.Drawing.Size(110, 19);
            this.labelControl5.TabIndex = 4;
            this.labelControl5.Text = "labelControl5";
            // 
            // labelControl6
            // 
            this.labelControl6.Appearance.Font = new System.Drawing.Font("Tahoma", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl6.Appearance.Options.UseFont = true;
            this.labelControl6.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.BindingSource_F, "CONCodigoFolha1", true));
            this.labelControl6.Location = new System.Drawing.Point(7, 7);
            this.labelControl6.Name = "labelControl6";
            this.labelControl6.Size = new System.Drawing.Size(129, 23);
            this.labelControl6.TabIndex = 5;
            this.labelControl6.Text = "labelControl6";
            // 
            // cApartamentosCampos
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Doc = System.Windows.Forms.DockStyle.Fill;
            this.LookAndFeel.UseDefaultLookAndFeel = false;
            this.Name = "cApartamentosCampos";
            paginaTabInfo13.Name = "Unidade";
            paginaTabInfo13.PaginaTabType = typeof(Cadastros.Apartamentos.PaginasApartamento.TabBasicos);
            paginaTabInfo14.Name = "Pagamentos";
            paginaTabInfo14.PaginaTabType = typeof(Cadastros.Apartamentos.PaginasApartamento.TabPagamentos);
            paginaTabInfo15.Name = "Proprietário";
            paginaTabInfo15.PaginaTabType = typeof(Cadastros.Apartamentos.PaginasApartamento.TapProprietario);
            paginaTabInfo16.Name = "Inquilino";
            paginaTabInfo16.PaginaTabType = typeof(Cadastros.Apartamentos.PaginasApartamento.TabInquilino);
            paginaTabInfo17.Name = "Documentos";
            paginaTabInfo17.PaginaTabType = typeof(Cadastros.Apartamentos.PaginasApartamento.TabDocumentos);
            paginaTabInfo18.Name = "Histórico";
            paginaTabInfo18.PaginaTabType = typeof(Cadastros.Apartamentos.PaginasApartamento.TabHistorico);
            this.PaginaTabInfoCollection.AddRange(new CompontesBasicos.Classes_de_interface.PaginaTabInfo[] {
            paginaTabInfo13,
            paginaTabInfo14,
            paginaTabInfo15,
            paginaTabInfo16,
            paginaTabInfo17,
            paginaTabInfo18});
            this.Size = new System.Drawing.Size(1568, 601);
            this.TipoBuscaPk = CompontesBasicos.ETipoBuscaPk.Nenhum;
            this.cargaInicial += new System.EventHandler(this.cApartamentosCampos_cargaInicial);
            ((System.ComponentModel.ISupportInitialize)(this.GroupControl_F)).EndInit();
            this.GroupControl_F.ResumeLayout(false);
            this.GroupControl_F.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.TabControl_F)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.BarManager_F)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.BarAndDockingController_F)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.BindingSource_F)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.StyleController_F)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dApartamentos)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private DevExpress.XtraEditors.LabelControl labelControl1;
        private DevExpress.XtraEditors.LabelControl labelControl4;
        private DevExpress.XtraEditors.LabelControl labelControl3;
        private DevExpress.XtraEditors.LabelControl labelControl2;
        /// <summary>
        /// 
        /// </summary>
        public CadastrosProc.dApartamentos dApartamentos;
        private DevExpress.XtraEditors.LabelControl labelControl5;
        private DevExpress.XtraEditors.LabelControl labelControl6;
    }
}
