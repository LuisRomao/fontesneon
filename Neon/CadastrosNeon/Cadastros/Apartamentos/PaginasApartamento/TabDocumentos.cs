using System;
using System.Windows.Forms;
using CadastrosProc;
using VirEnumeracoesNeon;


namespace Cadastros.Apartamentos.PaginasApartamento
{
    /// <summary>
    /// 
    /// </summary>
    public partial class TabDocumentos : CompontesBasicos.ComponenteTabCampos
    {
        /// <summary>
        /// 
        /// </summary>
        public TabDocumentos(CompontesBasicos.ComponenteCamposGroupTabControl _TabMae) :
            base(_TabMae)
        {
            InitializeComponent();
            aPartamentoDocBindingSource.DataSource = cApartamentosCampos1.dApartamentos;
            Framework.Enumeracoes.VirEnumTiposAPTDOC.CarregaEditorDaGrid(colAPDTipo);
        }
        
        private cApartamentosCampos cApartamentosCampos1
        {
            get
            {
                return (cApartamentosCampos)TabMae;
            }
        }

        private dApartamentos.APARTAMENTOSRow LinhaMae
        {
            get 
            {
                return cApartamentosCampos1.LinhaMae;
            }
        }
        
        private void CarregaLista()
        {            
                
                cApartamentosCampos1.dApartamentos.APartamentoDocTableAdapter.FillByAPT(cApartamentosCampos1.dApartamentos.APartamentoDoc,LinhaMae.APT);
                //aPartamentoDocBindingSource.DataSource = cApartamentosCampos1.PegaListaDocs();            
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="Entrando"></param>
        public override void OnTrocaPagina(bool Entrando)
        {
            if (Entrando)
            {
                CarregaLista();                
            }
        }
        
        private void simpleButton1_Click(object sender, EventArgs e)
        {
            cPublicaDoc Editor = new cPublicaDoc();
            
            if(Editor.VirShowModulo(CompontesBasicos.EstadosDosComponentes.PopUp) == DialogResult.OK)
            {
                if ((Editor.XArquivo.Text == "") || (Editor.XTipo.EditValue == null) || (Editor.XDescricao.Text == ""))
                    return;
                try
                {
                    cApartamentosCampos1.Publica(Editor.XArquivo.Text, Editor.XDescricao.Text, (TiposAPTDOC)Editor.XTipo.EditValue);
                }
                finally
                {
                    CarregaLista();
                }
            }
        }

        private void gridView1_DoubleClick(object sender, EventArgs e)
        {

            dApartamentos.APartamentoDocRow row = (dApartamentos.APartamentoDocRow)gridView1.GetFocusedDataRow();
            if (row != null)
            {                
                componenteWEB1.Navigate(string.Format(@"http://www.neonimoveis.com.br/neon23/neononline/pdfdoc.aspx?ARQUIVO=APTDOC{0}", row.APD));                
            }
        }

        private void simpleButton2_Click(object sender, EventArgs e)
        {
            
        }

        private void simpleButton3_Click(object sender, EventArgs e)
        {

        }
    }
}
