
using Cadastros.Pessoas;
using CompontesBasicosProc;
using System;
using System.Windows.Forms;
using VirEnumeracoesNeon;
using VirEnumeracoes;
using CadastrosProc;

namespace Cadastros.Apartamentos.PaginasApartamento
{
    /// <summary>
    /// 
    /// </summary>
    public partial class TapProprietario : CompontesBasicos.ComponenteTabCampos
    {

        private bool bloc = false;

        private ContaCorrenteNeon ContaDebitoP;

        /// <summary>
        /// 
        /// </summary>
        /// <param name="_TabMae"></param>
        public TapProprietario(CompontesBasicos.ComponenteCamposGroupTabControl _TabMae) :
            base(_TabMae)
        {
            InitializeComponent();
            if (cApartamentosCampos1.LinhaPESP != null)
                pessoasRowBindingSource.DataSource = cApartamentosCampos1.LinhaPESP;
            if ((!cApartamentosCampos1.LinhaMae.IsAPTSenhaInternetProprietarioNull()) && (cApartamentosCampos1.LinhaMae.APTSenhaInternetProprietario == "****"))
                AjustaTelaAcesso(true);
            else
                AjustaTelaAcesso(false);
            if (cApartamentosCampos1.ContaCorrenteNeon.DebitoAutomatico)
            {
                if (!cApartamentosCampos1.LinhaMae.IsAPTDebitoP_CCGNull())
                    ContaDebitoP = new ContaCorrenteNeon(cApartamentosCampos1.LinhaMae.APTDebitoP_CCG, TipoChaveContaCorrenteNeon.CCG);
            }
            MarcaDebitoAutomatico();
        }

        private void AjustaTelaAcesso(bool semAcesso)
        {
            labelControl2.Visible = semAcesso;
            textEdit11.Visible = !labelControl2.Visible;
            bloc = true;
            chTravaAcesso.Checked = labelControl2.Visible;
            bloc = false;
        }

        private void checkEdit1_CheckedChanged(object sender, EventArgs e)
        {
            if (bloc)
                return;            
            try
            {
                bloc = true;
                TabMaeC.AjustaCHDebitoAutomatico(true, ref CHDebP, ref ContaDebitoP);
            }
            finally
            {                
                MarcaDebitoAutomatico();
                bloc = false;
            }            
        }

        private void chTravaAcesso_CheckedChanged(object sender, EventArgs e)
        {
            if (bloc)
                return;
            if (chTravaAcesso.Checked)
            {
                cApartamentosCampos1.LinhaMae.APTSenhaInternetProprietario = "****";
                AjustaTelaAcesso(true);
            }
            else
            {
                cApartamentosCampos1.LinhaMae.APTSenhaInternetProprietario = string.Format("{0:0000}", DateTime.Now.Ticks % 10000);
                AjustaTelaAcesso(false);
            }
        }

        private void imageComboBoxEdit1_EditValueChanged(object sender, EventArgs e)
        {
            if ((imageComboBoxEdit1.EditValue != null) && (imageComboBoxEdit1.EditValue != DBNull.Value))
                Aviso.Visible = ((FormaPagamento)imageComboBoxEdit1.EditValue).EstaNoGrupo(FormaPagamento.EntreigaPessoal,
                                                                                                         FormaPagamento.EntreigaPessoalEemail,
                                                                                                         FormaPagamento.email_EntreigaPessoal);
        }

        private void imageComboBoxEdit1_EditValueChanging(object sender, DevExpress.XtraEditors.Controls.ChangingEventArgs e)
        {
            FormaPagamento NewFP = (FormaPagamento)e.NewValue;
            FormaPagamento OldFP = FormaPagamento.EntreigaPessoal;
            try
            {
                OldFP = (FormaPagamento)e.OldValue;
            }
            catch { }
            if (
                NewFP.EstaNoGrupo(FormaPagamento.email_Correio, FormaPagamento.email_EntreigaPessoal)
              &&
                !OldFP.EstaNoGrupo(FormaPagamento.email_Correio, FormaPagamento.email_EntreigaPessoal)
              )
            {
                MessageBox.Show("Esta forma de pagamento s� pode ser cadastrada por e-mail");
                e.Cancel = true;
            }
        }

        private void MarcaDebitoAutomatico()
        {
            try
            {
                bloc = true;
                CHDebP.Enabled = cApartamentosCampos1.ContaCorrenteNeon.DebitoAutomatico;
                if (ContaDebitoP != null)
                {
                    CHDebP.Checked = true;
                    CHDebP.Text = string.Format("D�bito autom�tico: {0}", ContaDebitoP);
                }
                else
                {
                    CHDebP.Checked = false;
                    CHDebP.Text = "D�bito autom�tico";
                }
            }
            finally
            {
                bloc = false;
            }
        }

        private void simpleButton1_Click(object sender, EventArgs e)
        {
            Pessoas.cPessoasCampos EditorPes;
            cPergunta cPergunta1 = new cPergunta(false);
            if ((cApartamentosCampos1.LinhaPESP == null) || (cPergunta1.VirShowModulo(CompontesBasicos.EstadosDosComponentes.PopUp) == DialogResult.OK))
            {
                if ((cApartamentosCampos1.LinhaPESP != null) && (cPergunta1.radioGroup1.EditValue == null))
                    return;
                if ((cApartamentosCampos1.LinhaPESP == null) || (cPergunta1.radioGroup1.EditValue.ToString() == "NOVO"))                   
                {
                    string cpf = string.Empty;
                    if (VirInput.Input.Execute("CPF/CNPJ", ref cpf, false))
                    {
                        DocBacarios.CPFCNPJ novoCPF = new DocBacarios.CPFCNPJ(cpf);
                        if ((novoCPF.Tipo == DocBacarios.TipoCpfCnpj.INVALIDO) || (novoCPF.Valor == 0))
                        {
                            MessageBox.Show("CPF/CNPJ inv�lido");
                            return;
                        }
                        else
                        {
                            //Documento
                            cPublicaDoc Editor = new cPublicaDoc();
                            Editor.XTipo.EditValue = (int)TiposAPTDOC.escritura;
                            Editor.XTipo.Properties.ReadOnly = true;
                            Editor.XDescricao.Text = string.Format("Doc Propriet�rio - {0:dd/MM/yyyy}", DateTime.Today);
                            Editor.XDescricao.Properties.ReadOnly = true;
                            if (Editor.VirShowModulo(CompontesBasicos.EstadosDosComponentes.PopUp) == DialogResult.OK)
                            {
                                if (Editor.XArquivo.Text == string.Empty)
                                    return;
                                cApartamentosCampos1.Publica(Editor.XArquivo.Text, Editor.XDescricao.Text, (TiposAPTDOC)Editor.XTipo.EditValue);
                            }
                            else
                                return;
                            //Documento/
                            EditorPes = new Pessoas.cPessoasCampos();
                            if (Cadastros.Pessoas.dPessoasCampos.PESSOASTableAdapter.FillByCPFCNPJ(EditorPes.dPessoasCampos.PESSOAS, novoCPF.ToString()) == 0)
                            {
                                dPessoasCampos.PESSOASRow novarow = EditorPes.dPessoasCampos.PESSOAS.NewPESSOASRow();
                                if (cApartamentosCampos1.dApartamentos.CodConBloco.Count == 0)
                                    cApartamentosCampos1.dApartamentos.CodConBlocoTableAdapter.Fill(cApartamentosCampos1.dApartamentos.CodConBloco, cApartamentosCampos1.LinhaMae.APT_BLO);
                                dApartamentos.CodConBlocoRow rowCodCon = cApartamentosCampos1.dApartamentos.CodConBloco[0];
                                novarow.PESEndereco = rowCodCon.CONEndereco;
                                novarow.PES_CID = rowCodCon.CON_CID;
                                if (!rowCodCon.IsCONCepNull())
                                    novarow.PESCep = rowCodCon.CONCep;
                                novarow.PESBairro = rowCodCon.CONBairro;
                                novarow.PESNome = string.Empty;
                                novarow.PESEmail = novarow.PESFone1 = novarow.PESFone2 = novarow.PESFone3 = novarow.PESFone4 = string.Empty;
                                novarow.PESCpfCnpj = novoCPF.ToString();
                                novarow.PESTipo = (novoCPF.Tipo == DocBacarios.TipoCpfCnpj.CPF ? "F" : "J");
                                novarow.PESCpfCnpjViaSite = false;
                                novarow.PESCpfErrado = false;
                                EditorPes.dPessoasCampos.PESSOAS.AddPESSOASRow(novarow);
                                EditorPes.tipoDeCarga = CompontesBasicos.TipoDeCarga.navegacao;
                                if (EditorPes.VirShowModulo(CompontesBasicos.EstadosDosComponentes.PopUp) == DialogResult.OK)
                                {
                                    cApartamentosCampos1.LinhaMae.APTProprietario_PES = novarow.PES;
                                    cApartamentosCampos1.CarregaPES(true);
                                    //pessoasRowBindingSource.DataSource = cApartamentosCampos1.LinhaPESP;
                                }
                            }
                            else
                            {
                                cApartamentosCampos1.LinhaMae.APTProprietario_PES = EditorPes.dPessoasCampos.PESSOAS[0].PES;
                                cApartamentosCampos1.CarregaPES(true);
                                //pessoasRowBindingSource.DataSource = cApartamentosCampos1.LinhaPESP;
                            };
                            cApartamentosCampos1.LinhaMae.APTUsuarioInternetProprietario = cApartamentosCampos1.NovoUsuario(cApartamentosCampos1.LinhaPESP.PESNome);
                            int isenha = cApartamentosCampos1.LinhaPESP.PES + cApartamentosCampos1.LinhaMae.APT;
                            cApartamentosCampos1.LinhaMae.APTSenhaInternetProprietario = string.Format("{0:0000}", (777.76743M * isenha) % 10000); ;
                            cApartamentosCampos1.LinhaMae.SetAPTDebitoP_CCGNull();
                            ContaDebitoP = null;
                            MarcaDebitoAutomatico();
                        }
                    }
                }
                else
                {
                    EditorPes = new Pessoas.cPessoasCampos();
                    EditorPes.dPessoasCampos.APTChamador = cApartamentosCampos1.LinhaMae.APT;
                    EditorPes.Fill(CompontesBasicos.TipoDeCarga.pk, cApartamentosCampos1.LinhaPESP.PES, false);
                    if (EditorPes.VirShowModulo(CompontesBasicos.EstadosDosComponentes.PopUp) == DialogResult.OK)
                        cApartamentosCampos1.CarregaPES(true);
                    //pessoasRowBindingSource.DataSource = cApartamentosCampos1.LinhaPESP;
                }
            }
        }

        private void simpleButton2_Click(object sender, EventArgs e)
        {
            if (cApartamentosCampos1.LinhaPESP != null)
            {
                Pessoas.cPessoasCampos EditorPes = new Pessoas.cPessoasCampos();
                EditorPes.Fill(CompontesBasicos.TipoDeCarga.pk, cApartamentosCampos1.LinhaPESP.PES, true);
                EditorPes.VirShowModulo(CompontesBasicos.EstadosDosComponentes.PopUp);
            }
        }

        private void TapProprietario_Load(object sender, EventArgs e)
        {
            textEdit11.Properties.MaxLength = 10;
        }

        private cApartamentosCampos cApartamentosCampos1
        {
            get
            {
                return (cApartamentosCampos)TabMae;
            }
        }

        private cApartamentosCampos TabMaeC
        {
            get
            {
                return (cApartamentosCampos)TabMae;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="oTarefa"></param>
        /// <returns></returns>
        public override bool TarefaParaTodos(object oTarefa)
        {
            Tarefas Tarefa = (Tarefas)oTarefa;
            if (Tarefa == Tarefas.Carrega)
                pessoasRowBindingSource.DataSource = cApartamentosCampos1.LinhaPESP;
            return true;
        }
    }
}
