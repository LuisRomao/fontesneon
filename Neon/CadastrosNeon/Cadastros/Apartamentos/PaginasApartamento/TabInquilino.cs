using Cadastros.Condominios;
using Cadastros.Pessoas;
using CompontesBasicosProc;
using System;
using System.Windows.Forms;
using VirEnumeracoes;
using VirEnumeracoesNeon;
using CadastrosProc;

namespace Cadastros.Apartamentos.PaginasApartamento
{
    /// <summary>
    /// 
    /// </summary>
    public partial class TabInquilino : CompontesBasicos.ComponenteTabCampos
    {

        private bool bloc = false;

        private ContaCorrenteNeon ContaDebitoI;

        /// <summary>
        /// Construtor
        /// </summary>
        /// <param name="_TabMae"></param>
        public TabInquilino(CompontesBasicos.ComponenteCamposGroupTabControl _TabMae) :
            base(_TabMae)
        {
            InitializeComponent();
            if (cApartamentosCampos1.LinhaPESI != null)
                pessoasRowBindingSource.DataSource = cApartamentosCampos1.LinhaPESI;
            bindingSourceFRN.DataSource = Framework.datasets.dFornecedores.GetdFornecedoresST("IMO");
            if ((!cApartamentosCampos1.LinhaMae.IsAPTSenhaInternetInquilinoNull()) && (cApartamentosCampos1.LinhaMae.APTSenhaInternetInquilino == "****"))
                AjustaTelaAcesso(true);
            else
                AjustaTelaAcesso(false);
            if (cApartamentosCampos1.ContaCorrenteNeon.DebitoAutomatico)
                if (!cApartamentosCampos1.LinhaMae.IsAPTDebitoI_CCGNull())
                    ContaDebitoI = new ContaCorrenteNeon(cApartamentosCampos1.LinhaMae.APTDebitoI_CCG, TipoChaveContaCorrenteNeon.CCG);
            MarcaDebitoAutomatico();
        }

        private void AjustaTelaAcesso(bool semAcesso)
        {
            labelControl2.Visible = semAcesso;
            textEdit11.Visible = !labelControl2.Visible;
            bloc = true;
            chTravaAcesso.Checked = labelControl2.Visible;
            bloc = false;
        }

        private void CHDebI_CheckedChanged(object sender, EventArgs e)
        {            
            if (bloc)
                return;
            try
            {
                bloc = true;
                TabMaeC.AjustaCHDebitoAutomatico(false, ref CHDebI, ref ContaDebitoI);                                
            }
            finally
            {
                MarcaDebitoAutomatico();
                bloc = false;
            }
        }

        private void chTravaAcesso_CheckedChanged(object sender, EventArgs e)
        {
            if (bloc)
                return;
            if (chTravaAcesso.Checked)
            {
                cApartamentosCampos1.LinhaMae.APTSenhaInternetInquilino = "****";
                AjustaTelaAcesso(true);
            }
            else
            {
                cApartamentosCampos1.LinhaMae.APTSenhaInternetInquilino = string.Format("{0:0000}", DateTime.Now.Ticks % 10000);
                AjustaTelaAcesso(false);
            }
        }

        private void imageComboBoxEdit1_EditValueChanged(object sender, EventArgs e)
        {
            if ((ICB_Boleto.EditValue != null) && (ICB_Boleto.EditValue != DBNull.Value))
                Aviso.Visible = ((FormaPagamento)ICB_Boleto.EditValue).EstaNoGrupo(FormaPagamento.EntreigaPessoal,
                                                                                                         FormaPagamento.EntreigaPessoalEemail,
                                                                                                         FormaPagamento.email_EntreigaPessoal);
        }

        private void imageComboBoxEdit1_EditValueChanging(object sender, DevExpress.XtraEditors.Controls.ChangingEventArgs e)
        {
            FormaPagamento NewFP = (FormaPagamento)e.NewValue;
            FormaPagamento OldFP = FormaPagamento.EntreigaPessoal;
            try
            {
                OldFP = (FormaPagamento)e.OldValue;
            }
            catch { }
            if (
                NewFP.EstaNoGrupo(FormaPagamento.email_Correio, FormaPagamento.email_EntreigaPessoal)
              &&
                !OldFP.EstaNoGrupo(FormaPagamento.email_Correio, FormaPagamento.email_EntreigaPessoal)
              )
            {
                MessageBox.Show("Esta forma de pagamento s� pode ser cadastrada por e-mail");
                e.Cancel = true;
            }
        }

        private void lookUpEdit1_ButtonClick(object sender, DevExpress.XtraEditors.Controls.ButtonPressedEventArgs e)
        {
            if (e.Button.Kind == DevExpress.XtraEditors.Controls.ButtonPredefines.Delete)
                lookUpEdit1.EditValue = null;
        }

        private void MarcaDebitoAutomatico()
        {
            try
            {
                bloc = true;
                CHDebI.Enabled = cApartamentosCampos1.ContaCorrenteNeon.DebitoAutomatico;
                if (ContaDebitoI != null)
                {
                    CHDebI.Checked = true;
                    CHDebI.Text = string.Format("D�bito autom�tico: {0}", ContaDebitoI);
                }
                else
                {
                    CHDebI.Checked = false;
                    CHDebI.Text = "D�bito autom�tico";
                }
            }
            finally
            {
                bloc = false;
            }
        }

        private void simpleButton1_Click(object sender, EventArgs e)
        {
            Pessoas.cPessoasCampos EditorPes;
            cPergunta cPergunta1 = new cPergunta(true);
            if ((cApartamentosCampos1.LinhaPESI == null) || (cPergunta1.VirShowModulo(CompontesBasicos.EstadosDosComponentes.PopUp) == DialogResult.OK))
                if ((cApartamentosCampos1.LinhaPESI != null) && (cPergunta1.radioGroup1.EditValue == null))
                    return;
            if ((cApartamentosCampos1.LinhaPESI == null) || (cPergunta1.radioGroup1.EditValue.ToString() == "NOVO"))                      
                {
                    string cpf = string.Empty;
                    if (VirInput.Input.Execute("CPF/CNPJ", ref cpf, false))
                    {
                        DocBacarios.CPFCNPJ novoCPF = new DocBacarios.CPFCNPJ(cpf);
                        if ((novoCPF.Tipo == DocBacarios.TipoCpfCnpj.INVALIDO) || (novoCPF.Valor == 0))
                        {
                            MessageBox.Show("CPF/CNPJ inv�lido");
                            return;
                        }
                        else
                        {
                            //Documento
                            cPublicaDoc Editor = new cPublicaDoc();
                            Editor.XTipo.EditValue = (int)TiposAPTDOC.escritura;
                            Editor.XTipo.Properties.ReadOnly = true;
                            Editor.XDescricao.Text = string.Format("Doc Inquilino - {0:dd/MM/yyyy}", DateTime.Today);
                            Editor.XDescricao.Properties.ReadOnly = true;
                            if (Editor.VirShowModulo(CompontesBasicos.EstadosDosComponentes.PopUp) == DialogResult.OK)
                            {
                                if (Editor.XArquivo.Text == string.Empty)
                                    return;
                                cApartamentosCampos1.Publica(Editor.XArquivo.Text, Editor.XDescricao.Text, (TiposAPTDOC)Editor.XTipo.EditValue);
                            }
                            else
                                return;
                            //Documento/
                            EditorPes = new Pessoas.cPessoasCampos();
                            if (Cadastros.Pessoas.dPessoasCampos.PESSOASTableAdapter.FillByCPFCNPJ(EditorPes.dPessoasCampos.PESSOAS, novoCPF.ToString()) == 0)
                            {
                                dPessoasCampos.PESSOASRow novarow = EditorPes.dPessoasCampos.PESSOAS.NewPESSOASRow();
                                if (cApartamentosCampos1.dApartamentos.CodConBloco.Count == 0)
                                    cApartamentosCampos1.dApartamentos.CodConBlocoTableAdapter.Fill(cApartamentosCampos1.dApartamentos.CodConBloco, cApartamentosCampos1.LinhaMae.APT_BLO);
                                dApartamentos.CodConBlocoRow rowCodCon = cApartamentosCampos1.dApartamentos.CodConBloco[0];
                                novarow.PESEndereco = rowCodCon.CONEndereco;
                                novarow.PES_CID = rowCodCon.CON_CID;
                                if (!rowCodCon.IsCONCepNull())
                                    novarow.PESCep = rowCodCon.CONCep;
                                if (!rowCodCon.IsCONCepNull())
                                    novarow.PESBairro = rowCodCon.CONBairro;
                                novarow.PESNome = string.Empty;
                                novarow.PESEmail = novarow.PESFone1 = novarow.PESFone2 = novarow.PESFone3 = novarow.PESFone4 = string.Empty;
                                novarow.PESCpfCnpj = novoCPF.ToString();
                                novarow.PESTipo = (novoCPF.Tipo == DocBacarios.TipoCpfCnpj.CPF ? "F" : "J");
                                novarow.PESCpfCnpjViaSite = false;
                                novarow.PESCpfErrado = false;
                                EditorPes.dPessoasCampos.PESSOAS.AddPESSOASRow(novarow);
                                EditorPes.tipoDeCarga = CompontesBasicos.TipoDeCarga.navegacao;
                                if (EditorPes.VirShowModulo(CompontesBasicos.EstadosDosComponentes.PopUp) == DialogResult.OK)                                
                                    cApartamentosCampos1.LinhaMae.APTInquilino_PES = novarow.PES;                                                                                                        
                            }
                            else                            
                                cApartamentosCampos1.LinhaMae.APTInquilino_PES = EditorPes.dPessoasCampos.PESSOAS[0].PES;                            
                            cApartamentosCampos1.CarregaPES(true);
                            cApartamentosCampos1.LinhaMae.APTFormaPagtoInquilino_FPG = (int)FormaPagamento.EntreigaPessoal;
                            ICB_Boleto.EditValue = (int)FormaPagamento.EntreigaPessoal;
                            Aviso.Visible = true;
                            cApartamentosCampos1.LinhaMae.APTInquilinoPagaCondominio = true;
                            chCondominio.Checked = true;
                            cApartamentosCampos1.LinhaMae.APTInquilinoPagaFR = false;
                            chFR.Checked = false;
                            cApartamentosCampos1.LinhaMae.SetAPTInquilinoPagaRateioNull();
                            ICB_Rateios.EditValue = DBNull.Value;
                            cApartamentosCampos1.LinhaMae.APTPagaConsumo = true;
                            chAgua.Checked = true;
                            cApartamentosCampos1.LinhaMae.APTUsuarioInternetInquilino = cApartamentosCampos1.NovoUsuario(cApartamentosCampos1.LinhaPESI.PESNome);
                            int isenha = cApartamentosCampos1.LinhaPESI.PES + cApartamentosCampos1.LinhaMae.APT;
                            cApartamentosCampos1.LinhaMae.APTSenhaInternetInquilino = string.Format("{0:0000}", (777.76743M * isenha) % 10000);
                            cApartamentosCampos1.CarregaPES(true);
                            cApartamentosCampos1.LinhaMae.SetAPTDebitoI_CCGNull();
                            ContaDebitoI = null;
                            MarcaDebitoAutomatico();
                        }
                    }

                }
                else
                {
                    EditorPes = new Pessoas.cPessoasCampos();
                    EditorPes.dPessoasCampos.APTChamador = cApartamentosCampos1.LinhaMae.APT;
                    EditorPes.Fill(CompontesBasicos.TipoDeCarga.pk, cApartamentosCampos1.LinhaPESI.PES, false);
                    if (EditorPes.VirShowModulo(CompontesBasicos.EstadosDosComponentes.PopUp) == DialogResult.OK)
                        cApartamentosCampos1.CarregaPES(true);
                    //pessoasRowBindingSource.DataSource = cApartamentosCampos1.LinhaPESI;
                }
        }

        private void simpleButton2_Click(object sender, EventArgs e)
        {
            if ((!cApartamentosCampos1.LinhaMae.IsAPTInquilino_PESNull()) && (MessageBox.Show("Confirma que a unidade n�o est� mais alugada?", "Confirma��o", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes))
            {
                cApartamentosCampos1.LinhaMae.SetAPTInquilino_PESNull();
                cApartamentosCampos1.LinhaMae.SetAPTFormaPagtoInquilino_FPGNull();
                cApartamentosCampos1.LinhaMae.SetAPTInquilinoPagaCondominioNull();
                cApartamentosCampos1.LinhaMae.SetAPTInquilinoPagaFRNull();
                cApartamentosCampos1.LinhaMae.SetAPTInquilinoPagaRateioNull();
                cApartamentosCampos1.LinhaMae.APTPagaConsumo = true;
                cApartamentosCampos1.CarregaPES(true);
                cApartamentosCampos1.LinhaMae.APTUsuarioInternetInquilino = string.Empty;
                cApartamentosCampos1.LinhaMae.APTSenhaInternetInquilino = string.Empty;
                cApartamentosCampos1.LinhaMae.SetAPTDebitoI_CCGNull();
                ContaDebitoI = null;
                MarcaDebitoAutomatico();
            }
        }

        private void simpleButton3_Click(object sender, EventArgs e)
        {
            if (cApartamentosCampos1.LinhaPESI != null)
            {
                Pessoas.cPessoasCampos EditorPes = new Pessoas.cPessoasCampos();
                EditorPes.Fill(CompontesBasicos.TipoDeCarga.pk, cApartamentosCampos1.LinhaPESI.PES, true);
                EditorPes.VirShowModulo(CompontesBasicos.EstadosDosComponentes.PopUp);
            }
        }

        private void TapProprietario_Load(object sender, EventArgs e)
        {
            textEdit11.Properties.MaxLength = 10;
        }

        private cApartamentosCampos cApartamentosCampos1
        {
            get
            {
                return (cApartamentosCampos)TabMae;
            }
        }

        private cApartamentosCampos TabMaeC
        {
            get
            {
                return (cApartamentosCampos)TabMae;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="Entrando"></param>
        public override void OnTrocaPagina(bool Entrando)
        {
            base.OnTrocaPagina(Entrando);
            AvisoPGE.Visible = false;
            foreach (dApartamentos.APTPGERow rowPGE in TabMaeC.dApartamentos.APTPGE)
                if (!rowPGE.IsAPTPGEPagaNull() && (rowPGE.APTPGEPaga))
                    if (rowPGE.IsAPTPGEProprietarioNull() || !rowPGE.APTPGEProprietario)
                    {
                        AvisoPGE.Visible = true;
                        break;
                    }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="oTarefa"></param>
        /// <returns></returns>
        public override bool TarefaParaTodos(object oTarefa)
        {
            Tarefas Tarefa = (Tarefas)oTarefa;
            if (Tarefa == Tarefas.Carrega)
                if (cApartamentosCampos1.LinhaPESI != null)
                    pessoasRowBindingSource.DataSource = cApartamentosCampos1.LinhaPESI;
                else
                    pessoasRowBindingSource.DataSource = typeof(dApartamentos.PessoasRow);
            return true;
        }
    }
}
