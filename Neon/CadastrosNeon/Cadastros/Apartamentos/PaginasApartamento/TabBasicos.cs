using System;

namespace Cadastros.Apartamentos.PaginasApartamento
{
    /// <summary>
    /// 
    /// </summary>
    public partial class TabBasicos : CompontesBasicos.ComponenteTabCampos
    {
        /// <summary>
        /// Construtor
        /// </summary>
        public TabBasicos()
        {
            InitializeComponent();
            bindingSourceFRN.DataSource = Framework.datasets.dFornecedores.GetdFornecedoresST("ADV");
            dPlanosSeguroConteudoBindingSource.DataSource = Framework.datasets.dPlanosSeguroConteudo.dPlanosSeguroConteudoSt;              
        }

        private cApartamentosCampos cApartamentosCampos1 
        {
            get 
            {
                return (cApartamentosCampos)TabMae;
            }
        }

        private void ajustaTela()
        {
            spinEdit1.Visible = spinEdit2.Visible = label2.Visible = checkEdit1.Checked;            
        }        

        private bool Carregado = false;

        private void checkEdit1_CheckedChanged(object sender, EventArgs e)
        {
            ajustaTela();
            if (Carregado)
            {                
                if (checkEdit1.Checked)                     
                    spinEdit1.Value = cApartamentosCampos1.LinhaMae.APTDiaCondominio = cApartamentosCampos1.LinhaMae.CONDiaVencimento;                
            }
        }

        private bool BlocAjustaTelaSeguro = false;        

        private void AjustaTelaSeguro()
        {
            if (BlocAjustaTelaSeguro)
                return;
            try
            {
                BlocAjustaTelaSeguro = true;
                lookUpEdit2.Visible = false;
                if (cApartamentosCampos1.CON_PSC.HasValue)
                {
                    radioGroup1.Properties.Items[1].Enabled = true;
                    if (cApartamentosCampos1.LinhaMae.APTSeguro)
                        radioGroup1.EditValue = "C";
                    else
                    {
                        if (cApartamentosCampos1.LinhaMae.IsAPT_PSCNull())
                            radioGroup1.EditValue = "N";
                        else
                        {
                            radioGroup1.EditValue = "S";
                            lookUpEdit2.Visible = true;
                        }
                    }
                }
                else
                {
                    radioGroup1.Properties.Items[1].Enabled = false;
                    if (cApartamentosCampos1.LinhaMae.IsAPT_PSCNull())
                        radioGroup1.EditValue = "N";
                    else
                    {
                        radioGroup1.EditValue = "S";
                        lookUpEdit2.Visible = true;
                    }
                }
            }
            finally 
            {
                BlocAjustaTelaSeguro = false;
            }
        }

        

        private void TabBasicos_Load(object sender, EventArgs e)
        {
            Carregado = true;            
            AjustaTelaSeguro();
        }

        private void radioGroup1_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (BlocAjustaTelaSeguro)
                return;
            switch ((string)radioGroup1.EditValue)
            {
                case "N":
                    cApartamentosCampos1.LinhaMae.SetAPT_PSCNull();
                    cApartamentosCampos1.LinhaMae.APTSeguro = false;
                    break;
                case "C":
                    cApartamentosCampos1.LinhaMae.SetAPT_PSCNull();
                    cApartamentosCampos1.LinhaMae.APTSeguro = true;
                    break;
                case "S":
                    if (cApartamentosCampos1.LinhaMae.IsAPT_PSCNull())
                        cApartamentosCampos1.LinhaMae.APT_PSC = cApartamentosCampos1.CON_PSC.GetValueOrDefault(Framework.datasets.dPlanosSeguroConteudo.dPlanosSeguroConteudoSt.PlanosSeguroConteudo[0].PSC);
                    cApartamentosCampos1.LinhaMae.APTSeguro = false;
                    break;
            }
            AjustaTelaSeguro();
        }  

        private void lookUpEdit1_ButtonClick(object sender, DevExpress.XtraEditors.Controls.ButtonPressedEventArgs e)
        {
            if (e.Button.Kind == DevExpress.XtraEditors.Controls.ButtonPredefines.Delete)
                lookUpEdit1.EditValue = null;
        }

        private void lookUpEdit2_ButtonClick(object sender, DevExpress.XtraEditors.Controls.ButtonPressedEventArgs e)
        {
            if (e.Button.Kind == DevExpress.XtraEditors.Controls.ButtonPredefines.Delete)
                lookUpEdit2.EditValue = null;
        }                     
       
    }
}
