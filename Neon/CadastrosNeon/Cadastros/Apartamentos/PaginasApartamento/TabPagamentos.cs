using System;
using System.Data;
using Cadastros.Condominios;
using CadastrosProc;

namespace Cadastros.Apartamentos.PaginasApartamento
{
    /// <summary>
    /// 
    /// </summary>
    public partial class TabPagamentos : CompontesBasicos.ComponenteTabCampos
    {
        /// <summary>
        /// 
        /// </summary>
        public TabPagamentos()
        {
            InitializeComponent();
        }

        private cApartamentosCampos cApartamentosCampos1
        {
            get
            {
                return (cApartamentosCampos)TabMae;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="oTarefa"></param>
        /// <returns></returns>
        public override bool TarefaParaTodos(object oTarefa)
        {
            Tarefas Tarefa = (Tarefas)oTarefa;
            if (Tarefa == Tarefas.CanClose)
            {
                foreach (dApartamentos.APTPGERow linha in cApartamentosCampos1.dApartamentos.APTPGE)
                {
                    if (!linha.IsAPTPGEPagaNull())

                        if (linha.RowState != DataRowState.Unchanged)
                            return false;
                }
                return true;
            }
            else if (Tarefa == Tarefas.endedit)
            {
                dApartamentosCamposBindingSource.EndEdit();
                foreach (dApartamentos.APTPGERow linha in cApartamentosCampos1.dApartamentos.APTPGE)
                {
                    if (!linha.IsAPTPGEPagaNull())
                    {
                        if (linha.RowState != DataRowState.Unchanged)
                        {
                            if (linha.IsAPTPGEProprietarioNull())
                                linha.APTPGEProprietario = true;
                            if (linha.IsAPTPGEDATAINull())
                                cApartamentosCampos1.dApartamentos.APTPGETableAdapter.InsertQuery(cApartamentosCampos1.pk, linha.PGE, linha.APTPGEPaga, linha.APTPGEProprietario, CompontesBasicos.Bancovirtual.UsuarioLogado.UsuarioLogadoSt.USU, DateTime.Now);
                            else
                                cApartamentosCampos1.dApartamentos.APTPGETableAdapter.UpdateQuery(linha.APTPGEPaga, DateTime.Now, CompontesBasicos.Bancovirtual.UsuarioLogado.UsuarioLogadoSt.USU, linha.APTPGEProprietario, cApartamentosCampos1.pk, linha.PGE);
                            linha.AcceptChanges();
                        }
                    }
                }
                return true;
            }
            else
                return true;
        }
    }
}
