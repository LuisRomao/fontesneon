namespace Cadastros.Apartamentos.PaginasApartamento
{
    partial class TabInquilino
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.Windows.Forms.Label label10;
            System.Windows.Forms.Label label9;
            System.Windows.Forms.Label label8;
            System.Windows.Forms.Label label7;
            System.Windows.Forms.Label label6;
            System.Windows.Forms.Label label5;
            System.Windows.Forms.Label label4;
            System.Windows.Forms.Label label3;
            System.Windows.Forms.Label label2;
            System.Windows.Forms.Label label1;
            System.Windows.Forms.Label aPTNumeroLabel;
            System.Windows.Forms.Label label11;
            System.Windows.Forms.Label label12;
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(TabInquilino));
            this.pessoasRowBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.imageCollection1 = new DevExpress.Utils.ImageCollection(this.components);
            this.Aviso = new DevExpress.XtraEditors.LabelControl();
            this.ICB_Boleto = new DevExpress.XtraEditors.ImageComboBoxEdit();
            this.aPARTAMENTOSBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.textEdit11 = new DevExpress.XtraEditors.TextEdit();
            this.textEdit10 = new DevExpress.XtraEditors.TextEdit();
            this.textEdit9 = new DevExpress.XtraEditors.TextEdit();
            this.textEdit8 = new DevExpress.XtraEditors.TextEdit();
            this.textEdit7 = new DevExpress.XtraEditors.TextEdit();
            this.textEdit6 = new DevExpress.XtraEditors.TextEdit();
            this.textEdit5 = new DevExpress.XtraEditors.TextEdit();
            this.textEdit4 = new DevExpress.XtraEditors.TextEdit();
            this.textEdit3 = new DevExpress.XtraEditors.TextEdit();
            this.textEdit2 = new DevExpress.XtraEditors.TextEdit();
            this.textEdit1 = new DevExpress.XtraEditors.TextEdit();
            this.bindingSourceFRN = new System.Windows.Forms.BindingSource(this.components);
            this.lookUpEdit1 = new DevExpress.XtraEditors.LookUpEdit();
            this.ICB_Rateios = new DevExpress.XtraEditors.ImageComboBoxEdit();
            this.chFR = new DevExpress.XtraEditors.CheckEdit();
            this.chCondominio = new DevExpress.XtraEditors.CheckEdit();
            this.simpleButton1 = new DevExpress.XtraEditors.SimpleButton();
            this.simpleButton2 = new DevExpress.XtraEditors.SimpleButton();
            this.simpleButton3 = new DevExpress.XtraEditors.SimpleButton();
            this.chTravaAcesso = new DevExpress.XtraEditors.CheckEdit();
            this.labelControl2 = new DevExpress.XtraEditors.LabelControl();
            this.chAgua = new DevExpress.XtraEditors.CheckEdit();
            this.AvisoPGE = new DevExpress.XtraEditors.LabelControl();
            this.CHDebI = new DevExpress.XtraEditors.CheckEdit();
            label10 = new System.Windows.Forms.Label();
            label9 = new System.Windows.Forms.Label();
            label8 = new System.Windows.Forms.Label();
            label7 = new System.Windows.Forms.Label();
            label6 = new System.Windows.Forms.Label();
            label5 = new System.Windows.Forms.Label();
            label4 = new System.Windows.Forms.Label();
            label3 = new System.Windows.Forms.Label();
            label2 = new System.Windows.Forms.Label();
            label1 = new System.Windows.Forms.Label();
            aPTNumeroLabel = new System.Windows.Forms.Label();
            label11 = new System.Windows.Forms.Label();
            label12 = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.StyleController_F)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pessoasRowBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.imageCollection1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ICB_Boleto.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.aPARTAMENTOSBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit11.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit10.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit9.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit8.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit7.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit6.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit5.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit4.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit3.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit2.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit1.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.bindingSourceFRN)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lookUpEdit1.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ICB_Rateios.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chFR.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chCondominio.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chTravaAcesso.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chAgua.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.CHDebI.Properties)).BeginInit();
            this.SuspendLayout();
            // 
            // ToolTipController_F
            // 
            this.ToolTipController_F.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.ToolTipController_F.Appearance.ForeColor = System.Drawing.Color.Black;
            this.ToolTipController_F.Appearance.Options.UseBackColor = true;
            this.ToolTipController_F.Appearance.Options.UseFont = true;
            this.ToolTipController_F.Appearance.Options.UseForeColor = true;
            this.ToolTipController_F.Appearance.Options.UseTextOptions = true;
            this.ToolTipController_F.Appearance.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            // 
            // StyleController_F
            // 
            this.StyleController_F.AppearanceDisabled.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this.StyleController_F.AppearanceDisabled.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this.StyleController_F.AppearanceDisabled.Options.UseBackColor = true;
            this.StyleController_F.AppearanceDisabled.Options.UseForeColor = true;
            this.StyleController_F.AppearanceFocused.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.StyleController_F.AppearanceFocused.Options.UseBackColor = true;
            // 
            // label10
            // 
            label10.AutoSize = true;
            label10.Font = new System.Drawing.Font("Tahoma", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            label10.Location = new System.Drawing.Point(25, 456);
            label10.Name = "label10";
            label10.Size = new System.Drawing.Size(78, 18);
            label10.TabIndex = 52;
            label10.Text = "Telefone:";
            // 
            // label9
            // 
            label9.AutoSize = true;
            label9.Font = new System.Drawing.Font("Tahoma", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            label9.Location = new System.Drawing.Point(25, 420);
            label9.Name = "label9";
            label9.Size = new System.Drawing.Size(59, 18);
            label9.TabIndex = 51;
            label9.Text = "E.mail:";
            // 
            // label8
            // 
            label8.AutoSize = true;
            label8.Font = new System.Drawing.Font("Tahoma", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            label8.Location = new System.Drawing.Point(25, 267);
            label8.Name = "label8";
            label8.Size = new System.Drawing.Size(42, 18);
            label8.TabIndex = 50;
            label8.Text = "CEP:";
            // 
            // label7
            // 
            label7.AutoSize = true;
            label7.Font = new System.Drawing.Font("Tahoma", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            label7.Location = new System.Drawing.Point(25, 235);
            label7.Name = "label7";
            label7.Size = new System.Drawing.Size(64, 18);
            label7.TabIndex = 49;
            label7.Text = "Cidade:";
            // 
            // label6
            // 
            label6.AutoSize = true;
            label6.Font = new System.Drawing.Font("Tahoma", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            label6.Location = new System.Drawing.Point(25, 203);
            label6.Name = "label6";
            label6.Size = new System.Drawing.Size(60, 18);
            label6.TabIndex = 48;
            label6.Text = "Bairro:";
            // 
            // label5
            // 
            label5.AutoSize = true;
            label5.Font = new System.Drawing.Font("Tahoma", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            label5.Location = new System.Drawing.Point(25, 360);
            label5.Name = "label5";
            label5.Size = new System.Drawing.Size(61, 18);
            label5.TabIndex = 46;
            label5.Text = "Boleto:";
            // 
            // label4
            // 
            label4.AutoSize = true;
            label4.Font = new System.Drawing.Font("Tahoma", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            label4.Location = new System.Drawing.Point(25, 170);
            label4.Name = "label4";
            label4.Size = new System.Drawing.Size(82, 18);
            label4.TabIndex = 45;
            label4.Text = "Endere�o:";
            // 
            // label3
            // 
            label3.AutoSize = true;
            label3.Font = new System.Drawing.Font("Tahoma", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            label3.Location = new System.Drawing.Point(25, 328);
            label3.Name = "label3";
            label3.Size = new System.Drawing.Size(125, 18);
            label3.TabIndex = 44;
            label3.Text = "Senha Internet:";
            // 
            // label2
            // 
            label2.AutoSize = true;
            label2.Font = new System.Drawing.Font("Tahoma", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            label2.Location = new System.Drawing.Point(25, 297);
            label2.Name = "label2";
            label2.Size = new System.Drawing.Size(137, 18);
            label2.TabIndex = 43;
            label2.Text = "Usu�rio Internet:";
            // 
            // label1
            // 
            label1.AutoSize = true;
            label1.Font = new System.Drawing.Font("Tahoma", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            label1.Location = new System.Drawing.Point(25, 118);
            label1.Name = "label1";
            label1.Size = new System.Drawing.Size(56, 18);
            label1.TabIndex = 42;
            label1.Text = "Nome:";
            // 
            // aPTNumeroLabel
            // 
            aPTNumeroLabel.AutoSize = true;
            aPTNumeroLabel.Font = new System.Drawing.Font("Tahoma", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            aPTNumeroLabel.Location = new System.Drawing.Point(25, 85);
            aPTNumeroLabel.Name = "aPTNumeroLabel";
            aPTNumeroLabel.Size = new System.Drawing.Size(91, 18);
            aPTNumeroLabel.TabIndex = 41;
            aPTNumeroLabel.Text = "CPF/CNPJ:";
            // 
            // label11
            // 
            label11.AutoSize = true;
            label11.Font = new System.Drawing.Font("Tahoma", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            label11.Location = new System.Drawing.Point(25, 6);
            label11.Name = "label11";
            label11.Size = new System.Drawing.Size(96, 18);
            label11.TabIndex = 67;
            label11.Text = "Imobili�ria:";
            // 
            // label12
            // 
            label12.AutoSize = true;
            label12.Font = new System.Drawing.Font("Tahoma", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            label12.Location = new System.Drawing.Point(353, 36);
            label12.Name = "label12";
            label12.Size = new System.Drawing.Size(70, 18);
            label12.TabIndex = 71;
            label12.Text = "Rateios:";
            // 
            // pessoasRowBindingSource
            // 
            this.pessoasRowBindingSource.DataSource = typeof(CadastrosProc.dApartamentos.PessoasRow);
            // 
            // imageCollection1
            // 
            this.imageCollection1.ImageSize = new System.Drawing.Size(32, 16);
            this.imageCollection1.ImageStream = ((DevExpress.Utils.ImageCollectionStreamer)(resources.GetObject("imageCollection1.ImageStream")));
            this.imageCollection1.Images.SetKeyName(0, "FP4.bmp");
            this.imageCollection1.Images.SetKeyName(1, "FP2.bmp");
            this.imageCollection1.Images.SetKeyName(2, "FP14.bmp");
            this.imageCollection1.Images.SetKeyName(3, "FP12.bmp");
            this.imageCollection1.Images.SetKeyName(4, "FP24.bmp");
            this.imageCollection1.Images.SetKeyName(5, "FP22.bmp");
            // 
            // Aviso
            // 
            this.Aviso.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Aviso.Appearance.ForeColor = System.Drawing.Color.Red;
            this.Aviso.Appearance.Options.UseFont = true;
            this.Aviso.Appearance.Options.UseForeColor = true;
            this.Aviso.Location = new System.Drawing.Point(28, 146);
            this.Aviso.Name = "Aviso";
            this.Aviso.Size = new System.Drawing.Size(551, 14);
            this.Aviso.TabIndex = 65;
            this.Aviso.Text = "ATEN��O: Os boletos s�o enviados para o condom�nio e N�O para o endere�o do inqui" +
    "lino";
            // 
            // ICB_Boleto
            // 
            this.ICB_Boleto.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.aPARTAMENTOSBindingSource, "APTFormaPagtoInquilino_FPG", true));
            this.ICB_Boleto.Location = new System.Drawing.Point(178, 356);
            this.ICB_Boleto.Name = "ICB_Boleto";
            this.ICB_Boleto.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ICB_Boleto.Properties.Appearance.Options.UseFont = true;
            this.ICB_Boleto.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.ICB_Boleto.Properties.Items.AddRange(new DevExpress.XtraEditors.Controls.ImageComboBoxItem[] {
            new DevExpress.XtraEditors.Controls.ImageComboBoxItem("Condom�nio", 4, 0),
            new DevExpress.XtraEditors.Controls.ImageComboBoxItem("Correio", 2, 1),
            new DevExpress.XtraEditors.Controls.ImageComboBoxItem("Condom�nio + e-mail", 14, 2),
            new DevExpress.XtraEditors.Controls.ImageComboBoxItem("Correio + e-mail", 12, 3),
            new DevExpress.XtraEditors.Controls.ImageComboBoxItem("e-mail (documentos no condom�nio)", 24, 4),
            new DevExpress.XtraEditors.Controls.ImageComboBoxItem("e-mail (documentos pelo correio)", 22, 5)});
            this.ICB_Boleto.Properties.SmallImages = this.imageCollection1;
            this.ICB_Boleto.Size = new System.Drawing.Size(442, 26);
            this.ICB_Boleto.TabIndex = 64;
            this.ICB_Boleto.EditValueChanged += new System.EventHandler(this.imageComboBoxEdit1_EditValueChanged);
            this.ICB_Boleto.EditValueChanging += new DevExpress.XtraEditors.Controls.ChangingEventHandler(this.imageComboBoxEdit1_EditValueChanging);
            // 
            // aPARTAMENTOSBindingSource
            // 
            this.aPARTAMENTOSBindingSource.DataMember = "APARTAMENTOS";
            this.aPARTAMENTOSBindingSource.DataSource = typeof(CadastrosProc.dApartamentos);
            // 
            // textEdit11
            // 
            this.textEdit11.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.aPARTAMENTOSBindingSource, "APTSenhaInternetInquilino", true));
            this.textEdit11.Location = new System.Drawing.Point(178, 324);
            this.textEdit11.Name = "textEdit11";
            this.textEdit11.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textEdit11.Properties.Appearance.Options.UseFont = true;
            this.textEdit11.Properties.MaxLength = 10;
            this.textEdit11.Size = new System.Drawing.Size(130, 26);
            this.textEdit11.TabIndex = 63;
            // 
            // textEdit10
            // 
            this.textEdit10.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.aPARTAMENTOSBindingSource, "APTUsuarioInternetInquilino", true));
            this.textEdit10.Location = new System.Drawing.Point(178, 293);
            this.textEdit10.Name = "textEdit10";
            this.textEdit10.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textEdit10.Properties.Appearance.Options.UseFont = true;
            this.textEdit10.Size = new System.Drawing.Size(130, 26);
            this.textEdit10.TabIndex = 62;
            // 
            // textEdit9
            // 
            this.textEdit9.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.pessoasRowBindingSource, "PESEmail", true));
            this.textEdit9.Location = new System.Drawing.Point(176, 416);
            this.textEdit9.Name = "textEdit9";
            this.textEdit9.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textEdit9.Properties.Appearance.Options.UseFont = true;
            this.textEdit9.Properties.ReadOnly = true;
            this.textEdit9.Size = new System.Drawing.Size(442, 26);
            this.textEdit9.TabIndex = 61;
            // 
            // textEdit8
            // 
            this.textEdit8.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.pessoasRowBindingSource, "PESCep", true));
            this.textEdit8.Location = new System.Drawing.Point(178, 263);
            this.textEdit8.Name = "textEdit8";
            this.textEdit8.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textEdit8.Properties.Appearance.Options.UseFont = true;
            this.textEdit8.Properties.ReadOnly = true;
            this.textEdit8.Size = new System.Drawing.Size(219, 26);
            this.textEdit8.TabIndex = 60;
            // 
            // textEdit7
            // 
            this.textEdit7.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.pessoasRowBindingSource, "CIDUf", true));
            this.textEdit7.Location = new System.Drawing.Point(564, 231);
            this.textEdit7.Name = "textEdit7";
            this.textEdit7.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textEdit7.Properties.Appearance.Options.UseFont = true;
            this.textEdit7.Properties.ReadOnly = true;
            this.textEdit7.Size = new System.Drawing.Size(54, 26);
            this.textEdit7.TabIndex = 59;
            // 
            // textEdit6
            // 
            this.textEdit6.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.pessoasRowBindingSource, "CIDNome", true));
            this.textEdit6.Location = new System.Drawing.Point(178, 231);
            this.textEdit6.Name = "textEdit6";
            this.textEdit6.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textEdit6.Properties.Appearance.Options.UseFont = true;
            this.textEdit6.Properties.ReadOnly = true;
            this.textEdit6.Size = new System.Drawing.Size(382, 26);
            this.textEdit6.TabIndex = 58;
            // 
            // textEdit5
            // 
            this.textEdit5.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.pessoasRowBindingSource, "PESBairro", true));
            this.textEdit5.Location = new System.Drawing.Point(178, 199);
            this.textEdit5.Name = "textEdit5";
            this.textEdit5.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textEdit5.Properties.Appearance.Options.UseFont = true;
            this.textEdit5.Properties.ReadOnly = true;
            this.textEdit5.Size = new System.Drawing.Size(442, 26);
            this.textEdit5.TabIndex = 57;
            // 
            // textEdit4
            // 
            this.textEdit4.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.pessoasRowBindingSource, "PESEndereco", true));
            this.textEdit4.Location = new System.Drawing.Point(178, 166);
            this.textEdit4.Name = "textEdit4";
            this.textEdit4.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textEdit4.Properties.Appearance.Options.UseFont = true;
            this.textEdit4.Properties.ReadOnly = true;
            this.textEdit4.Size = new System.Drawing.Size(442, 26);
            this.textEdit4.TabIndex = 56;
            // 
            // textEdit3
            // 
            this.textEdit3.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.pessoasRowBindingSource, "PESNome", true));
            this.textEdit3.Location = new System.Drawing.Point(178, 114);
            this.textEdit3.Name = "textEdit3";
            this.textEdit3.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textEdit3.Properties.Appearance.Options.UseFont = true;
            this.textEdit3.Properties.ReadOnly = true;
            this.textEdit3.Size = new System.Drawing.Size(442, 26);
            this.textEdit3.TabIndex = 55;
            // 
            // textEdit2
            // 
            this.textEdit2.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.pessoasRowBindingSource, "PESCpfCnpj", true));
            this.textEdit2.Location = new System.Drawing.Point(178, 81);
            this.textEdit2.Name = "textEdit2";
            this.textEdit2.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textEdit2.Properties.Appearance.Options.UseFont = true;
            this.textEdit2.Properties.ReadOnly = true;
            this.textEdit2.Size = new System.Drawing.Size(245, 26);
            this.textEdit2.TabIndex = 54;
            // 
            // textEdit1
            // 
            this.textEdit1.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.pessoasRowBindingSource, "PESFone1", true));
            this.textEdit1.Location = new System.Drawing.Point(176, 448);
            this.textEdit1.Name = "textEdit1";
            this.textEdit1.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textEdit1.Properties.Appearance.Options.UseFont = true;
            this.textEdit1.Properties.ReadOnly = true;
            this.textEdit1.Size = new System.Drawing.Size(442, 26);
            this.textEdit1.TabIndex = 53;
            // 
            // bindingSourceFRN
            // 
            this.bindingSourceFRN.DataMember = "FORNECEDORES";
            this.bindingSourceFRN.DataSource = typeof(CadastrosProc.Fornecedores.dFornecedoresCampos);
            // 
            // lookUpEdit1
            // 
            this.lookUpEdit1.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.aPARTAMENTOSBindingSource, "APTImobiliaria_FRN", true));
            this.lookUpEdit1.Location = new System.Drawing.Point(176, 3);
            this.lookUpEdit1.Name = "lookUpEdit1";
            this.lookUpEdit1.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.True;
            this.lookUpEdit1.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lookUpEdit1.Properties.Appearance.Options.UseFont = true;
            this.lookUpEdit1.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo),
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Delete)});
            this.lookUpEdit1.Properties.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("FRNNome", "FRN Nome", 95, DevExpress.Utils.FormatType.None, "", true, DevExpress.Utils.HorzAlignment.Near)});
            this.lookUpEdit1.Properties.DataSource = this.bindingSourceFRN;
            this.lookUpEdit1.Properties.DisplayMember = "FRNNome";
            this.lookUpEdit1.Properties.NullText = "Nenhum";
            this.lookUpEdit1.Properties.ShowHeader = false;
            this.lookUpEdit1.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.Standard;
            this.lookUpEdit1.Properties.ValueMember = "FRN";
            this.lookUpEdit1.Size = new System.Drawing.Size(442, 24);
            this.lookUpEdit1.TabIndex = 66;
            this.lookUpEdit1.ButtonClick += new DevExpress.XtraEditors.Controls.ButtonPressedEventHandler(this.lookUpEdit1_ButtonClick);
            // 
            // ICB_Rateios
            // 
            this.ICB_Rateios.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.aPARTAMENTOSBindingSource, "APTInquilinoPagaRateio", true));
            this.ICB_Rateios.Location = new System.Drawing.Point(429, 33);
            this.ICB_Rateios.Name = "ICB_Rateios";
            this.ICB_Rateios.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.True;
            this.ICB_Rateios.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ICB_Rateios.Properties.Appearance.Options.UseFont = true;
            this.ICB_Rateios.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.ICB_Rateios.Properties.Items.AddRange(new DevExpress.XtraEditors.Controls.ImageComboBoxItem[] {
            new DevExpress.XtraEditors.Controls.ImageComboBoxItem("Paga Todos", true, -1),
            new DevExpress.XtraEditors.Controls.ImageComboBoxItem("N�o Paga nenhum", false, -1),
            new DevExpress.XtraEditors.Controls.ImageComboBoxItem("Segue o rateio", null, -1)});
            this.ICB_Rateios.Properties.NullText = "Segue o rateio";
            this.ICB_Rateios.Size = new System.Drawing.Size(191, 26);
            this.ICB_Rateios.TabIndex = 69;
            // 
            // chFR
            // 
            this.chFR.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.aPARTAMENTOSBindingSource, "APTInquilinoPagaFR", true));
            this.chFR.Location = new System.Drawing.Point(176, 33);
            this.chFR.Name = "chFR";
            this.chFR.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.chFR.Properties.Appearance.Options.UseFont = true;
            this.chFR.Properties.Caption = "Fundo de Reserva";
            this.chFR.Size = new System.Drawing.Size(167, 23);
            this.chFR.TabIndex = 68;
            // 
            // chCondominio
            // 
            this.chCondominio.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.aPARTAMENTOSBindingSource, "APTInquilinoPagaCondominio", true));
            this.chCondominio.Location = new System.Drawing.Point(28, 33);
            this.chCondominio.Name = "chCondominio";
            this.chCondominio.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.chCondominio.Properties.Appearance.Options.UseFont = true;
            this.chCondominio.Properties.Caption = "Condom�nio";
            this.chCondominio.Size = new System.Drawing.Size(124, 23);
            this.chCondominio.TabIndex = 70;
            // 
            // simpleButton1
            // 
            this.simpleButton1.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.simpleButton1.Appearance.Options.UseFont = true;
            this.simpleButton1.Location = new System.Drawing.Point(427, 72);
            this.simpleButton1.Name = "simpleButton1";
            this.simpleButton1.Size = new System.Drawing.Size(154, 36);
            this.simpleButton1.TabIndex = 72;
            this.simpleButton1.Text = "Alterar Inquilino";
            this.simpleButton1.Click += new System.EventHandler(this.simpleButton1_Click);
            // 
            // simpleButton2
            // 
            this.simpleButton2.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.simpleButton2.Appearance.Options.UseFont = true;
            this.simpleButton2.Location = new System.Drawing.Point(586, 72);
            this.simpleButton2.Name = "simpleButton2";
            this.simpleButton2.Size = new System.Drawing.Size(34, 36);
            this.simpleButton2.TabIndex = 73;
            this.simpleButton2.Text = "X";
            this.simpleButton2.Click += new System.EventHandler(this.simpleButton2_Click);
            // 
            // simpleButton3
            // 
            this.simpleButton3.Appearance.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.simpleButton3.Appearance.Options.UseFont = true;
            this.simpleButton3.Location = new System.Drawing.Point(28, 480);
            this.simpleButton3.Name = "simpleButton3";
            this.simpleButton3.Size = new System.Drawing.Size(193, 36);
            this.simpleButton3.TabIndex = 74;
            this.simpleButton3.Text = "Cadastro Inquilino";
            this.simpleButton3.Click += new System.EventHandler(this.simpleButton3_Click);
            // 
            // chTravaAcesso
            // 
            this.chTravaAcesso.Location = new System.Drawing.Point(315, 295);
            this.chTravaAcesso.Name = "chTravaAcesso";
            this.chTravaAcesso.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.chTravaAcesso.Properties.Appearance.Options.UseFont = true;
            this.chTravaAcesso.Properties.Caption = "Usu�rio sem acesso ao site";
            this.chTravaAcesso.Size = new System.Drawing.Size(303, 22);
            this.chTravaAcesso.TabIndex = 75;
            this.chTravaAcesso.CheckedChanged += new System.EventHandler(this.chTravaAcesso_CheckedChanged);
            // 
            // labelControl2
            // 
            this.labelControl2.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl2.Appearance.ForeColor = System.Drawing.Color.Red;
            this.labelControl2.Appearance.Options.UseFont = true;
            this.labelControl2.Appearance.Options.UseForeColor = true;
            this.labelControl2.Location = new System.Drawing.Point(178, 331);
            this.labelControl2.Name = "labelControl2";
            this.labelControl2.Size = new System.Drawing.Size(412, 14);
            this.labelControl2.TabIndex = 76;
            this.labelControl2.Text = "ATEN��O: Usu�rio sem acesso ao site. N�O alterar sem autoriza��o.";
            // 
            // chAgua
            // 
            this.chAgua.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.aPARTAMENTOSBindingSource, "APTPagaConsumo", true));
            this.chAgua.Location = new System.Drawing.Point(28, 58);
            this.chAgua.Name = "chAgua";
            this.chAgua.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.chAgua.Properties.Appearance.Options.UseFont = true;
            this.chAgua.Properties.Caption = "�gua";
            this.chAgua.Size = new System.Drawing.Size(124, 23);
            this.chAgua.TabIndex = 77;
            // 
            // AvisoPGE
            // 
            this.AvisoPGE.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.AvisoPGE.Appearance.ForeColor = System.Drawing.Color.Red;
            this.AvisoPGE.Appearance.Options.UseFont = true;
            this.AvisoPGE.Appearance.Options.UseForeColor = true;
            this.AvisoPGE.Location = new System.Drawing.Point(95, 63);
            this.AvisoPGE.Name = "AvisoPGE";
            this.AvisoPGE.Size = new System.Drawing.Size(328, 14);
            this.AvisoPGE.TabIndex = 78;
            this.AvisoPGE.Text = "ATEN��O: Existem pagamentos em nome do inquilino";
            this.AvisoPGE.Visible = false;
            // 
            // CHDebI
            // 
            this.CHDebI.Location = new System.Drawing.Point(28, 388);
            this.CHDebI.Name = "CHDebI";
            this.CHDebI.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.CHDebI.Properties.Appearance.Options.UseFont = true;
            this.CHDebI.Properties.Caption = "D�bito Autom�tico:";
            this.CHDebI.Size = new System.Drawing.Size(590, 22);
            this.CHDebI.TabIndex = 79;
            this.CHDebI.CheckedChanged += new System.EventHandler(this.CHDebI_CheckedChanged);
            // 
            // TabInquilino
            // 
            this.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(224)))), ((int)(((byte)(192)))));
            this.Appearance.Options.UseBackColor = true;
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.CHDebI);
            this.Controls.Add(this.AvisoPGE);
            this.Controls.Add(this.chAgua);
            this.Controls.Add(this.labelControl2);
            this.Controls.Add(this.chTravaAcesso);
            this.Controls.Add(this.simpleButton3);
            this.Controls.Add(this.simpleButton2);
            this.Controls.Add(this.simpleButton1);
            this.Controls.Add(label12);
            this.Controls.Add(this.ICB_Rateios);
            this.Controls.Add(this.chFR);
            this.Controls.Add(this.chCondominio);
            this.Controls.Add(label11);
            this.Controls.Add(this.lookUpEdit1);
            this.Controls.Add(this.Aviso);
            this.Controls.Add(this.ICB_Boleto);
            this.Controls.Add(this.textEdit11);
            this.Controls.Add(this.textEdit10);
            this.Controls.Add(this.textEdit9);
            this.Controls.Add(this.textEdit8);
            this.Controls.Add(this.textEdit7);
            this.Controls.Add(this.textEdit6);
            this.Controls.Add(this.textEdit5);
            this.Controls.Add(this.textEdit4);
            this.Controls.Add(this.textEdit3);
            this.Controls.Add(this.textEdit2);
            this.Controls.Add(this.textEdit1);
            this.Controls.Add(label10);
            this.Controls.Add(label9);
            this.Controls.Add(label8);
            this.Controls.Add(label7);
            this.Controls.Add(label6);
            this.Controls.Add(label5);
            this.Controls.Add(label4);
            this.Controls.Add(label3);
            this.Controls.Add(label2);
            this.Controls.Add(label1);
            this.Controls.Add(aPTNumeroLabel);
            this.LookAndFeel.UseDefaultLookAndFeel = false;
            this.Name = "TabInquilino";
            this.Size = new System.Drawing.Size(632, 522);
            this.Load += new System.EventHandler(this.TapProprietario_Load);
            ((System.ComponentModel.ISupportInitialize)(this.StyleController_F)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pessoasRowBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.imageCollection1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ICB_Boleto.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.aPARTAMENTOSBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit11.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit10.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit9.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit8.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit7.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit6.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit5.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit4.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit3.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit2.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit1.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.bindingSourceFRN)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lookUpEdit1.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ICB_Rateios.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chFR.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chCondominio.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chTravaAcesso.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chAgua.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.CHDebI.Properties)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.BindingSource aPARTAMENTOSBindingSource;
        private System.Windows.Forms.BindingSource pessoasRowBindingSource;
        private DevExpress.Utils.ImageCollection imageCollection1;
        private DevExpress.XtraEditors.LabelControl Aviso;
        private DevExpress.XtraEditors.ImageComboBoxEdit ICB_Boleto;
        private DevExpress.XtraEditors.TextEdit textEdit11;
        private DevExpress.XtraEditors.TextEdit textEdit10;
        private DevExpress.XtraEditors.TextEdit textEdit9;
        private DevExpress.XtraEditors.TextEdit textEdit8;
        private DevExpress.XtraEditors.TextEdit textEdit7;
        private DevExpress.XtraEditors.TextEdit textEdit6;
        private DevExpress.XtraEditors.TextEdit textEdit5;
        private DevExpress.XtraEditors.TextEdit textEdit4;
        private DevExpress.XtraEditors.TextEdit textEdit3;
        private DevExpress.XtraEditors.TextEdit textEdit2;
        private DevExpress.XtraEditors.TextEdit textEdit1;
        private System.Windows.Forms.BindingSource bindingSourceFRN;
        private DevExpress.XtraEditors.LookUpEdit lookUpEdit1;
        private DevExpress.XtraEditors.ImageComboBoxEdit ICB_Rateios;
        private DevExpress.XtraEditors.CheckEdit chFR;
        private DevExpress.XtraEditors.CheckEdit chCondominio;
        private DevExpress.XtraEditors.SimpleButton simpleButton1;
        private DevExpress.XtraEditors.SimpleButton simpleButton2;
        private DevExpress.XtraEditors.SimpleButton simpleButton3;
        private DevExpress.XtraEditors.CheckEdit chTravaAcesso;
        private DevExpress.XtraEditors.LabelControl labelControl2;
        private DevExpress.XtraEditors.CheckEdit chAgua;
        private DevExpress.XtraEditors.LabelControl AvisoPGE;
        private DevExpress.XtraEditors.CheckEdit CHDebI;
    }
}
