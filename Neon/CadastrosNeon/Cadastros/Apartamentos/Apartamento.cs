﻿using Abstratos;
using AbstratosNeon;
using Cadastros.Condominios;
using CompontesBasicosProc;
using CompontesBasicos;
using Framework.objetosNeon;
using FrameworkProc.objetosNeon;
using System;
using System.Collections.Generic;
using VirEnumeracoes;
using VirEnumeracoesNeon;
using CadastrosProc;

namespace Cadastros.Apartamentos
{
    /// <summary>
    /// Objeto Apartamento
    /// </summary>
    public class Apartamento : ApartamentoProc
    {

        private DadosPI _DadosI;
        private DadosPI _DadosP;        
        private ContaCorrenteNeon ContaCorrenteNeonI;
        private ContaCorrenteNeon ContaCorrenteNeonP;        

        #region Construtores
        /// <summary>
        /// Construtor
        /// </summary>
        /// <param name="APT"></param>
        public Apartamento(int APT) : base(APT)
        {
        }

        /// <summary>
        /// Construtor
        /// </summary>
        /// <param name="_LinhaMae"></param>
        public Apartamento(dApartamentos.APARTAMENTOSRow _LinhaMae) : base(_LinhaMae)
        {
        } 
        #endregion

        private DadosPI Dados(bool Proprietario)
        {
            if (!Proprietario && !Alugado)
                Proprietario = false;
            return Proprietario ? DadosP : DadosI;
        }

        /// <summary>
        /// Forma de pagamento
        /// </summary>
        /// <param name="Proprietario"></param>
        /// <returns></returns>
        private FormaPagamento FormaPagamentoCadastrada(bool Proprietario)
        {
            if (Proprietario)
                if (LinhaMae.IsAPTFormaPagtoProprietario_FPGNull())
                    return FormaPagamento.EntreigaPessoal;
                else
                    return (FormaPagamento)LinhaMae.APTFormaPagtoProprietario_FPG;
            else
                if (LinhaMae.IsAPTFormaPagtoInquilino_FPGNull())
                return FormaPagamento.EntreigaPessoal;
            else
                return (FormaPagamento)LinhaMae.APTFormaPagtoInquilino_FPG;
        }

        private void LeDadosPI()
        {
            if (_DadosP != null)
                return;
            if (DApartamentos.Pessoas.Count == 0)
                DApartamentos.PessoasTableAdapter.Fill(DApartamentos.Pessoas, APTProprietario_PES.GetValueOrDefault(0), APTInquilino_PES.GetValueOrDefault(0));
            if (APTProprietario_PES.HasValue)
            {
                if (DApartamentos.Pessoas.FindByPES(APTProprietario_PES.Value) == null)
                {
                    DApartamentos.PessoasTableAdapter.ClearBeforeFill = false;
                    DApartamentos.PessoasTableAdapter.Fill(DApartamentos.Pessoas, APTProprietario_PES.GetValueOrDefault(0), 0);
                    DApartamentos.PessoasTableAdapter.ClearBeforeFill = true;
                }
                _DadosP = new DadosPI(DApartamentos.Pessoas.FindByPES(APTProprietario_PES.Value));
            }
            else
                _DadosP = new DadosPI(null);
            if (Alugado && APTInquilino_PES.HasValue)
            {
                if (DApartamentos.Pessoas.FindByPES(APTInquilino_PES.Value) == null)
                {
                    DApartamentos.PessoasTableAdapter.ClearBeforeFill = false;
                    DApartamentos.PessoasTableAdapter.Fill(DApartamentos.Pessoas, APTInquilino_PES.GetValueOrDefault(0), 0);
                    DApartamentos.PessoasTableAdapter.ClearBeforeFill = true;
                }
                _DadosI = new DadosPI(DApartamentos.Pessoas.FindByPES(APTInquilino_PES.Value));
            }
            else
                _DadosI = new DadosPI(null);
        }

        private int? APTInquilino_PES
        {
            get { return LinhaMae.IsAPTInquilino_PESNull() ? (int?)null : LinhaMae.APTInquilino_PES; }
        }

        private int? APTProprietario_PES
        {
            get { return LinhaMae.IsAPTProprietario_PESNull() ? (int?)null : LinhaMae.APTProprietario_PES; }
        }
        private DadosPI DadosI
        {
            get
            {
                if (_DadosP == null)
                    LeDadosPI();
                return _DadosI;
            }
        }

        private DadosPI DadosP
        {
            get
            {
                if (_DadosP == null)
                    LeDadosPI();
                return _DadosP;
            }
        }

        /// <summary>
        /// Abre a cApartamentoCampos
        /// </summary>
        /// <param name="Proprietario"></param>
        public override void AbrecApartamento(bool? Proprietario)
        {
            cApartamentosCampos cApartamentosCampos = new cApartamentosCampos();
            cApartamentosCampos.Fill(TipoDeCarga.pk, APT, false);
            cApartamentosCampos.Titulo = ToString();
            cApartamentosCampos.VirShowModulo(EstadosDosComponentes.JanelasAtivas);
            cApartamentosCampos.TabControl_F.SelectedTabPageIndex = Proprietario.GetValueOrDefault(true) ? 2 : 3;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="Proprietario"></param>
        /// <returns></returns>
        public override string APTSenhaInternet(bool Proprietario)
        {
            if (Proprietario)
                return LinhaMae.IsAPTSenhaInternetProprietarioNull() ? string.Empty : LinhaMae.APTSenhaInternetProprietario;
            else
                return LinhaMae.IsAPTSenhaInternetInquilinoNull() ? string.Empty : LinhaMae.APTSenhaInternetInquilino;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="Proprietario"></param>
        /// <returns></returns>
        public override string APTUsuarioInternet(bool Proprietario)
        {
            if (Proprietario)
                return LinhaMae.IsAPTUsuarioInternetProprietarioNull() ? null : LinhaMae.APTUsuarioInternetProprietario;
            else
                return LinhaMae.IsAPTUsuarioInternetInquilinoNull() ? null : LinhaMae.APTUsuarioInternetInquilino;
        }

        /// <summary>
        /// Conta debito automático
        /// </summary>
        /// <param name="Proprietario"></param>
        /// <returns></returns>
        public override ABS_ContaCorrente ContaDebito(bool Proprietario)
        {
            if (Proprietario)
            {
                if ((ContaCorrenteNeonP == null) && (!LinhaMae.IsAPTDebitoP_CCGNull()))
                    ContaCorrenteNeonP = new ContaCorrenteNeon(LinhaMae.APTDebitoP_CCG, TipoChaveContaCorrenteNeon.CCG);
                return ContaCorrenteNeonP;
            }
            else
            {
                if ((ContaCorrenteNeonI == null) && (!LinhaMae.IsAPTDebitoI_CCGNull()))
                    ContaCorrenteNeonI = new ContaCorrenteNeon(LinhaMae.APTDebitoI_CCG, TipoChaveContaCorrenteNeon.CCG);
                return ContaCorrenteNeonI;
            }
        }

        /// <summary>
        /// Vencimento do boleto de condominio
        /// </summary>
        /// <param name="competencia"></param>
        /// <returns></returns>
        public override DateTime DataVencimento(Abstratos.ABS_Competencia competencia)
        {
            CompontesBasicos.Performance.Performance.PerformanceST.Registra("DataVencimento do objeto Apartamento", true);
            DateTime retorno;
            Competencia comp = (Framework.objetosNeon.Competencia)competencia;
            if (comp.CON == 0)
                comp.CON = CON;
            if (!APTDiaDiferente)
                retorno = comp.VencimentoPadrao;
            else
            {
                int APTDiaDiferenteREF = LinhaMae.IsAPTDiaDiferenteREFNull() ? 0 : LinhaMae.APTDiaDiferenteREF;
                Competencia CompRef = comp.CloneCompet(APTDiaDiferenteREF);
                retorno = CompRef.DataNaCompetencia(LinhaMae.APTDiaCondominio);
            }
            CompontesBasicos.Performance.Performance.PerformanceST.StackPop();
            return retorno;
        }

        /// <summary>
        /// Fomas de paagamento que contem e-mail
        /// </summary>
        /// <param name="Forma"></param>
        /// <returns></returns>
        public static bool FormaContemplaEmail(FormaPagamento Forma)
        {
            return Forma.EstaNoGrupo(FormaPagamento.CorreioEemail,
                                    FormaPagamento.email_Correio,
                                    FormaPagamento.email_EntreigaPessoal,
                                    FormaPagamento.EntreigaPessoalEemail);
        }

        /// <summary>
        /// Forma de pagametento efetiva
        /// </summary>
        /// <param name="FormaSolicitada">Forma solicitada, na ausencia usa a cadastrada</param>
        /// <param name="Proprietario"></param>
        /// <param name="ForcaPapel"></param>
        /// <returns></returns>
        public override FormaPagamento FormaPagamentoEfetivo(FormaPagamento? FormaSolicitada, bool Proprietario, bool ForcaPapel)
        {
            if (!Proprietario && !Alugado)
                Proprietario = false;
            if (!FormaSolicitada.HasValue)
                FormaSolicitada = FormaPagamentoCadastrada(Proprietario);
            if (FormaContemplaEmail(FormaSolicitada.Value))
            {
                if (PESEmail(Proprietario) == string.Empty)
                    switch (FormaSolicitada.Value)
                    {
                        case FormaPagamento.Correio:
                        case FormaPagamento.CorreioEemail:
                        case FormaPagamento.email_Correio:
                            FormaSolicitada = FormaPagamento.Correio;
                            break;
                        case FormaPagamento.EntreigaPessoal:
                        case FormaPagamento.EntreigaPessoalEemail:
                        case FormaPagamento.email_EntreigaPessoal:
                            FormaSolicitada = FormaPagamento.EntreigaPessoal;
                            break;
                        default:
                            break;
                    }
;
                if (ForcaPapel)
                    switch (FormaSolicitada.Value)
                    {
                        case FormaPagamento.CorreioEemail:
                        case FormaPagamento.email_Correio:
                            FormaSolicitada = FormaPagamento.CorreioEemail;
                            break;
                        case FormaPagamento.EntreigaPessoalEemail:
                        case FormaPagamento.email_EntreigaPessoal:
                            FormaSolicitada = FormaPagamento.EntreigaPessoalEemail;
                            break;
                        case FormaPagamento.Correio:
                        case FormaPagamento.EntreigaPessoal:
                        default:
                            break;
                    }
            }
            return FormaSolicitada.Value;
        }

        /// <summary>
        /// Retorna todos os apartamentos de um condomínio
        /// </summary>
        /// <param name="CON"></param>
        /// <returns></returns>
        public static SortedList<int, ABS_Apartamento> GetApartamentos(int CON)
        {
            Condominio Condominio = new Condominio(CON);
            SortedList<int, ABS_Apartamento> Retorno = new SortedList<int, ABS_Apartamento>();
            dApartamentos dApartamentos1 = new dApartamentos();
            dApartamentos1.APARTAMENTOSTableAdapter.FillByCON(dApartamentos1.APARTAMENTOS, CON);
            dApartamentos1.PessoasTableAdapter.FillByCON(dApartamentos1.Pessoas, CON);
            foreach (dApartamentos.APARTAMENTOSRow row in dApartamentos1.APARTAMENTOS)
            {
                Apartamento Novo = new Apartamento(row);
                Novo.Condominio = Condominio;
                Retorno.Add(row.APT, Novo);
            }
            return Retorno;
        }

        /// <summary>
        /// Todos os apartamentos ativos
        /// </summary>
        /// <returns></returns>
        public static SortedList<int, ABS_Apartamento> GetApartamentosAtivos()
        {
            SortedList<int, ABS_Condominio> Condominios = ABS_Condominio.ABSGetCondominios(true);
            SortedList<int, ABS_Apartamento> Retorno = new SortedList<int, ABS_Apartamento>();
            dApartamentos dApartamentos1 = new dApartamentos();
            dApartamentos1.APARTAMENTOSTableAdapter.FillAtivos(dApartamentos1.APARTAMENTOS);
            //dApartamentos1.PessoasTableAdapter.FillAtivos(dApartamentos1.Pessoas);
            foreach (dApartamentos.APARTAMENTOSRow row in dApartamentos1.APARTAMENTOS)
            {
                Apartamento Novo = new Apartamento(row);
                Novo.Condominio = Condominios[Novo.CON];
                Retorno.Add(row.APT, Novo);
            }
            return Retorno;
        }

        /// <summary>
        /// Todos os apartamentos ativos
        /// </summary>
        /// <returns></returns>
        public static SortedList<int, ABS_Apartamento> GetApartamentosSeguro(bool? interno)
        {
            SortedList<int, ABS_Condominio> Condominios = ABS_Condominio.ABSGetCondominios(true);
            SortedList<int, ABS_Apartamento> Retorno = new SortedList<int, ABS_Apartamento>();
            dApartamentos dApartamentos1 = new dApartamentos();
            if (!interno.HasValue)
            {
                dApartamentos1.APARTAMENTOSTableAdapter.FillBySeguroTodos(dApartamentos1.APARTAMENTOS);
                dApartamentos1.PessoasTableAdapter.FillBySeguroTodos(dApartamentos1.Pessoas);
            }
            else
            {
                if (interno.Value)
                {
                    dApartamentos1.APARTAMENTOSTableAdapter.FillBySeguroInterno(dApartamentos1.APARTAMENTOS);
                    dApartamentos1.PessoasTableAdapter.FillBySeguroInterno(dApartamentos1.Pessoas);
                }
                else
                {
                    dApartamentos1.APARTAMENTOSTableAdapter.FillBySeguroExterno(dApartamentos1.APARTAMENTOS);
                    dApartamentos1.PessoasTableAdapter.FillBySeguroExterno(dApartamentos1.Pessoas);
                }
            }


            foreach (dApartamentos.APARTAMENTOSRow row in dApartamentos1.APARTAMENTOS)
            {
                Apartamento Novo = new Apartamento(row);
                Novo.Condominio = Condominios[Novo.CON];
                Retorno.Add(row.APT, Novo);
            }
            return Retorno;
        }

        /// <summary>
        /// CEP
        /// </summary>
        /// <param name="FormaSolicitada"></param>
        /// <param name="Proprietario"></param>
        /// <returns></returns>
        public override string PESCEP(FormaPagamento? FormaSolicitada, bool Proprietario)
        {
            switch (FormaPagamentoEfetivo(FormaSolicitada, Proprietario, false))
            {
                case FormaPagamento.Correio:
                case FormaPagamento.CorreioEemail:
                case FormaPagamento.email_Correio:
                    return Dados(Proprietario).PESCEP == string.Empty ? "0" : Dados(Proprietario).PESCEP;
                case FormaPagamento.EntreigaPessoal:
                case FormaPagamento.EntreigaPessoalEemail:
                case FormaPagamento.email_EntreigaPessoal:
                    return "0";
                default:
                    throw new NotImplementedException();
            }
        }

        /// <summary>
        /// PESCpfCnpj
        /// </summary>
        /// <param name="Proprietario"></param>
        /// <returns></returns>
        public override DocBacarios.CPFCNPJ PESCpfCnpj(bool Proprietario)
        {
            return Dados(Proprietario).PEScpf;
        }

        /// <summary>
        /// Email
        /// </summary>
        /// <param name="Proprietario"></param>
        /// <returns></returns>
        public override string PESEmail(bool Proprietario)
        {
            if (!CompontesBasicosProc.FuncoesBasicasProc.EstaEmProducao)
                return "virtual@virweb.com.br";
            else
                return Dados(Proprietario).PESEmail;
        }

        /// <summary>
        /// PESEndereco
        /// </summary>
        /// <param name="FormaSolicitada">Forma solicitada, na ausencia usa a cadastrada</param>
        /// <param name="Proprietario"></param>
        /// <param name="ForcaPapel"></param>
        /// <returns></returns>
        public override string PESEndereco(FormaPagamento? FormaSolicitada, bool Proprietario, bool ForcaPapel)
        {
            switch (FormaPagamentoEfetivo(FormaSolicitada, Proprietario, ForcaPapel))
            {
                case FormaPagamento.Correio:
                case FormaPagamento.CorreioEemail:
                    return Dados(Proprietario).Endereco;
                case FormaPagamento.EntreigaPessoal:
                case FormaPagamento.EntreigaPessoalEemail:
                    return "Entrega Pessoal";
                case FormaPagamento.email_Correio:
                case FormaPagamento.email_EntreigaPessoal:
                    return Dados(Proprietario).PESEmail;
                default:
                    return "Entrega Pessoal";
                    //throw new NotImplementedException(string.Format("Não implementado: {0}", FormaPagamentoEfetivo(FormaSolicitada, Proprietario, ForcaPapel)));
            }

        }

        /// <summary>
        /// PESNome
        /// </summary>
        /// <param name="Proprietario"></param>
        /// <returns></returns>
        public override string PESNome(bool Proprietario)
        {
            LeDadosPI();
            return Dados(Proprietario).PESNome;
        }

        /// <summary>
        /// Retorna se o proprietário ou inquilino quer e-mail
        /// </summary>
        /// <param name="FormaSolicitada">Forma solicitada, na ausencia usa a cadastrada</param>
        /// <param name="Proprietario"></param>
        /// <returns></returns>
        public override bool QuerEmail(FormaPagamento? FormaSolicitada, bool Proprietario)
        {
            return FormaContemplaEmail(FormaPagamentoEfetivo(FormaSolicitada, Proprietario, false));
        }

        /// <summary>
        /// Retorna se o proprietário ou inquilino quer impresso em papel
        /// </summary>
        /// <param name="FormaSolicitada">Forma solicitada, na ausencia usa a cadastrada</param>
        /// <param name="Proprietario"></param>
        /// <returns></returns>
        public override bool QuerImpresso(FormaPagamento? FormaSolicitada, bool Proprietario)
        {
            return !FormaPagamentoEfetivo(FormaSolicitada, Proprietario, false).EstaNoGrupo(FormaPagamento.email_Correio, FormaPagamento.email_EntreigaPessoal);
        }
        /// <summary>
        /// Identifica o apartamento
        /// </summary>
        /// <returns></returns>
        public override string ToString()
        {
            if (encontrado)
                return string.Format("{0} {1} {2}", CONCodigo, BLOCodigo, APTNumero);
            else
                return "???";
        }

        /// <summary>
        /// Aligado
        /// </summary>
        public override bool Alugado { get { return !LinhaMae.IsAPTInquilino_PESNull(); } }

        /// <summary>
        /// APT
        /// </summary>
        public override int APT { get { return LinhaMae.APT; } }

        /// <summary>
        /// Dia diferente do condominio
        /// </summary>
        public override bool APTDiaDiferente
        {
            get
            {
                if (LinhaMae.IsAPTDiaDiferenteNull() || LinhaMae.IsAPTDiaCondominioNull())
                    return false;
                return LinhaMae.APTDiaDiferente;
            }
        }

        /// <summary>
        /// APTNumero
        /// </summary>
        public override string APTNumero { get { return LinhaMae.APTNumero; } }

        /// <summary>
        /// APTSeguro
        /// </summary>
        public override bool APTSeguro { get { return LinhaMae.APTSeguro; } }

        /// <summary>
        /// BLO
        /// </summary>
        public override int BLO { get { return LinhaMae.APT_BLO; } }

        /// <summary>
        /// BLOCodigo
        /// </summary>
        public override string BLOCodigo { get { return LinhaMae.BLOCodigo; } }

        /// <summary>
        /// BLONome
        /// </summary>
        public override string BLONome { get { return LinhaMae.BLONome; } }

        /// <summary>
        /// CON
        /// </summary>
        public override int CON { get { return LinhaMae.CON; } }

        /// <summary>
        /// CONCodigo
        /// </summary>
        public override string CONCodigo { get { return LinhaMae.CONCodigo; } }

        /// <summary>
        /// 
        /// </summary>
        public override ABS_Condominio Condominio
        {
            get
            {
                if (_Condominio == null)
                {
                    CompontesBasicosProc.Performance.Performance.PerformanceST.Registra("Busca CON", true);
                    _Condominio = new Condominio(CON);
                    CompontesBasicosProc.Performance.Performance.PerformanceST.StackPop();
                }
                return _Condominio;
            }
        }

        /// <summary>
        /// Email
        /// </summary>
        public override string EmailResponsavel
        {
            get
            {
                if (!CompontesBasicosProc.FuncoesBasicasProc.EstaEmProducao)
                    return "virtual@virweb.com.br";
                return Dados(false).PESEmail;
            }
        }

        /// <summary>
        /// Se foi encontrado
        /// </summary>
        public override bool encontrado
        {
            get
            {
                return (LinhaMae != null);
            }
        }

        /// <summary>
        /// Iquilino que paga o rateio
        /// </summary>
        public override TipoPagIP InquilinoPagaRateio
        {
            get
            {
                if (!Alugado)
                    return TipoPagIP.NaoPaga;
                if (LinhaMae.IsAPTInquilinoPagaRateioNull())
                    return TipoPagIP.SeguePLA;
                else
                    return LinhaMae.APTInquilinoPagaRateio ? TipoPagIP.Paga : TipoPagIP.NaoPaga;
            }
        }

        /// <summary>
        /// PES para INSS do corpo diretivo
        /// </summary>
        public override int PES_INSS
        {
            get
            {
                if (DApartamentos.CORPODIRETIVO.Count == 0)
                    DApartamentos.CORPODIRETIVOTableAdapter.FillbyAPT(DApartamentos.CORPODIRETIVO, APT);
                dApartamentos.CORPODIRETIVORow[] rowCDRs = LinhaMae.GetCORPODIRETIVORows();
                if (rowCDRs.Length > 1)
                    throw new Exception(string.Format("Erro: Apartamento como mais de 1 cargo no corpo diretivo\r\nUnidade: {0} {1} {2}", CONCodigo, BLOCodigo, APTNumero));
                else if (rowCDRs.Length == 1)
                    return rowCDRs[0].CDR_PES;
                else
                    return LinhaMae.APTProprietario_PES;
            }
        }                  

        /// <summary>
        /// Proprietário
        /// </summary>
        public override ABS_Pessoa Proprietario
        {
            get
            {
                if((proprietario == null) && (!LinhaMae.IsAPTProprietario_PESNull()))
                    proprietario = ABS_Pessoa.GetPessoa(LinhaMae.APTProprietario_PES);
                return proprietario;
            }
        }

        /// <summary>
        /// Inquilino
        /// </summary>
        public override ABS_Pessoa Inquilino
        {
            get
            {
                if ((proprietario == null) && (!LinhaMae.IsAPTProprietario_PESNull()))
                    proprietario = ABS_Pessoa.GetPessoa(LinhaMae.APTProprietario_PES);
                return proprietario;
            }
        }

        private class DadosPI
        {
            internal string CIDNome;
            internal string CIDUf;
            internal string PESBairro;
            internal string PESCEP;
            internal DocBacarios.CPFCNPJ PEScpf;
            internal string PESEmail;
            internal string PESEndereco;
            internal string PESNome;

            internal DadosPI(dApartamentos.PessoasRow rowPES)
            {
                PEScpf = null;
                PESEmail = PESNome = PESEndereco = PESCEP = PESBairro = CIDNome = CIDUf = string.Empty;
                if (rowPES != null)
                {
                    if (!rowPES.IsPESEmailNull())
                        PESEmail = rowPES.PESEmail;
                    if (!rowPES.IsPESCpfCnpjNull())
                        PEScpf = new DocBacarios.CPFCNPJ(rowPES.PESCpfCnpj);
                    PESNome = rowPES.PESNome;
                    if (!rowPES.IsPESEnderecoNull())
                        PESEndereco = rowPES.PESEndereco;
                    if (!rowPES.IsPESCepNull())
                        PESCEP = rowPES.PESCep;
                    if (!rowPES.IsPESBairroNull())
                        PESBairro = rowPES.PESBairro;
                    if (!rowPES.IsCIDNomeNull())
                        CIDNome = rowPES.CIDNome;
                    if (!rowPES.IsCIDUfNull())
                        CIDUf = rowPES.CIDUf;
                }
            }

            internal string Endereco
            {
                get
                {
                    //return String.Format("{0} CEP: {1} {2} {3} - {4}", PESEndereco, PESCEP, PESBairro, CIDNome.Trim(), CIDUf); 
                    return String.Format("{0} {2}\r\n{3} - {4}\r\nCEP: {1}", PESEndereco, PESCEP, PESBairro, CIDNome.Trim(), CIDUf);
                }
            }
        }
    }
}
