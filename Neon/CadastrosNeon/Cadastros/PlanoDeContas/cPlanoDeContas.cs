using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Text;
using System.Windows.Forms;

namespace Cadastros.PlanoDeContas
{
    /// <summary>
    /// Planos de conta
    /// </summary>
    public partial class cPlanoDeContas : CompontesBasicos.ComponenteGradeNavegador
    {
        /// <summary>
        /// Construtor
        /// </summary>
        public cPlanoDeContas()
        {
            InitializeComponent();
            BindingSource_F.DataSource = Framework.datasets.dPLAnocontas.dPLAnocontasSt;
            TableAdapterPrincipal = Framework.datasets.dPLAnocontas.dPLAnocontasSt.PLAnocontasTableAdapter;
        }

        private Framework.datasets.dPLAnocontas.PLAnocontasRow novaRow = null;

        /// <summary>
        /// Inclui registro
        /// </summary>
        /// <returns></returns>
        protected override DataRow Incluir_F()
        {
            int NovoCodigo = 0;
            if (VirInput.Input.Execute("Novo C�digo", out NovoCodigo))
            {
                if (NovoCodigo < 100000)
                {
                    MessageBox.Show("C�digo inv�lido");
                    return null;
                }
                if(Framework.datasets.dPLAnocontas.dPLAnocontasSt.PLAnocontas.FindByPLA(NovoCodigo.ToString("000000")) != null)
                {
                    MessageBox.Show("C�digo j� cadastrado");
                    return null;
                }
                novaRow = (Framework.datasets.dPLAnocontas.PLAnocontasRow)base.Incluir_F("PLA", NovoCodigo.ToString("000000"));          
                novaRow.PLAEVE = true;
                novaRow.PLAProprietario = true;
                novaRow.PLADescricaoLivre = false;
                return novaRow;
            }
            return null;
        }

        /// <summary>
        /// Grava as altera��es
        /// </summary>
        /// <returns></returns>
        public override bool Update_F()
        {
            bool retorno = base.Update_F();
            
            if(retorno && (novaRow != null))
            {                
                novaRow = null;
            }
            return retorno;
        }

        /// <summary>
        /// Alerar
        /// </summary>
        protected override void Alterar()
        {
            base.Alterar();
        }
    }
}
