
namespace Cadastros.PlanoDeContas
{
    /// <summary>
    /// 
    /// </summary>
    public partial class cSubPlano : CompontesBasicos.ComponenteGradeNavegador
    {
        /// <summary>
        /// 
        /// </summary>
        public cSubPlano()
        {
            InitializeComponent();
            BindingSourcePrincipal = BindingSource_F;
            BindingSource_F.DataSource = Framework.datasets.dPLAnocontas.dPLAnocontasSt;
            TableAdapterPrincipal = Framework.datasets.dPLAnocontas.dPLAnocontasSt.SubPLanoTableAdapter;
            pLAnocontasBindingSource.DataSource = Framework.datasets.dPLAnocontas.dPLAnocontasSt25;
        }
    }
}
