using System;
using System.Data;

namespace Cadastros.RamoAtividade
{
    /// <summary>
    /// 
    /// </summary>
    public partial class cRamoAtividadeGrade : CompontesBasicos.ComponenteGradeNavegador
    {
        /// <summary>
        /// Construtor
        /// </summary>
        public cRamoAtividadeGrade()
        {
            InitializeComponent();
            TableAdapterPrincipal = dRamoAtividadeGrade.RamoAtiVidadeTableAdapter;
        }

        private void cRamoAtividadeGrade_Load(object sender, EventArgs e)
        {
           //RamoAtividadeTableAdapter.Fill(dRamoAtividadeGrade.RamoAtiVidade);
        }

        /// <summary>
        /// 
        /// </summary>
        protected override void ExcluirRegistro_F()
        {
           int pk = Convert.ToInt32(((DataRowView)(BindingSource_F.Current))["RAV"]);
           dRamoAtividadeGrade.RamoAtiVidadeTableAdapter.DeleteQuery(pk);
           Refresh();
        }

        private void btnConfirmar_F_ItemClick_1(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
           Refresh();
        }

        /// <summary>
        /// 
        /// </summary>
        public override void RefreshDados()
        {
           Refresh();
        }

        /// <summary>
        /// 
        /// </summary>
        public override void Refresh()
        {
            dRamoAtividadeGrade.RamoAtiVidadeTableAdapter.Fill(dRamoAtividadeGrade.RamoAtiVidade);
        }
    }
}

