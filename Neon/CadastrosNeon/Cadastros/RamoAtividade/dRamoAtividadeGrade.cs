﻿namespace Cadastros.RamoAtividade
{


   public partial class dRamoAtividadeGrade
   {
       private dRamoAtividadeGradeTableAdapters.RamoAtiVidadeTableAdapter ramoAtiVidadeTableAdapter;
       /// <summary>
       /// TableAdapter padrão para tabela: RamoAtiVidade
       /// </summary>
       public dRamoAtividadeGradeTableAdapters.RamoAtiVidadeTableAdapter RamoAtiVidadeTableAdapter
       {
           get
           {
               if (ramoAtiVidadeTableAdapter == null)
               {
                   ramoAtiVidadeTableAdapter = new dRamoAtividadeGradeTableAdapters.RamoAtiVidadeTableAdapter();
                   ramoAtiVidadeTableAdapter.TrocarStringDeConexao();
               };
               return ramoAtiVidadeTableAdapter;
           }
       }
    }
}
