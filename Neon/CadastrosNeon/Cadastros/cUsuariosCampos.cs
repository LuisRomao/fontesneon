using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace Cadastros.Usuarios
{
    public partial class cUsuariosCampos : CompontesBasicos.ComponenteCamposBase
    {
        private void InitData()
        {
            cIDADESTableAdapter.Fill(dUsuariosCampos.CIDADES);
        }
        
        public cUsuariosCampos()
        {
            InitializeComponent();

            InitData();
        }

        protected override void ConfirmarRegistro_F()
        {
            uSUARIOSTableAdapter.Update(dUsuariosCampos);
        }
    }
}

