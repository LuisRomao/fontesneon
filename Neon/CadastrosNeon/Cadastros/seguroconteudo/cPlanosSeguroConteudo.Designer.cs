﻿namespace Cadastros.seguroconteudo
{
    partial class cPlanosSeguroConteudo
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(cPlanosSeguroConteudo));
            DevExpress.XtraEditors.Controls.EditorButtonImageOptions editorButtonImageOptions1 = new DevExpress.XtraEditors.Controls.EditorButtonImageOptions();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject1 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject2 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject3 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject4 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.XtraEditors.Controls.EditorButtonImageOptions editorButtonImageOptions2 = new DevExpress.XtraEditors.Controls.EditorButtonImageOptions();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject5 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject6 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject7 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject8 = new DevExpress.Utils.SerializableAppearanceObject();
            this.planosSeguroConteudoBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.colPSC = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colPSCDescricao = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colPSCValor = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemCalcEdit1 = new DevExpress.XtraEditors.Repository.RepositoryItemCalcEdit();
            this.colPSCStatus = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemImageComboBox1 = new DevExpress.XtraEditors.Repository.RepositoryItemImageComboBox();
            this.colPSCI_USU = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colPSCA_USU = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colPSCDataI = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colPSCDataA = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemButtonEdit1 = new DevExpress.XtraEditors.Repository.RepositoryItemButtonEdit();
            ((System.ComponentModel.ISupportInitialize)(this.PopImprimir_F)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.BarManager_F)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.PopExportar_F)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.GridControl_F)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.GridView_F)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.BindingSource_F)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.StyleController_F)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.planosSeguroConteudoBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCalcEdit1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemImageComboBox1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemButtonEdit1)).BeginInit();
            this.SuspendLayout();
            // 
            // barNavegacao_F
            // 
            this.barNavegacao_F.OptionsBar.DisableClose = true;
            this.barNavegacao_F.OptionsBar.DrawDragBorder = false;
            this.barNavegacao_F.OptionsBar.DrawSizeGrip = true;
            this.barNavegacao_F.OptionsBar.RotateWhenVertical = false;
            this.barNavegacao_F.OptionsBar.UseWholeRow = true;
            // 
            // BtnIncluir_F
            // 
            this.BtnIncluir_F.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("BtnIncluir_F.ImageOptions.Image")));
            // 
            // BtnAlterar_F
            // 
            this.BtnAlterar_F.Enabled = false;
            this.BtnAlterar_F.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("BtnAlterar_F.ImageOptions.Image")));
            this.BtnAlterar_F.ImageOptions.ImageIndex = 0;
            // 
            // BtnExcluir_F
            // 
            this.BtnExcluir_F.Enabled = false;
            this.BtnExcluir_F.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("BtnExcluir_F.ImageOptions.Image")));
            this.BtnExcluir_F.Visibility = DevExpress.XtraBars.BarItemVisibility.Never;
            // 
            // BtnImprimir_F
            // 
            this.BtnImprimir_F.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("BtnImprimir_F.ImageOptions.Image")));
            // 
            // BtnImprimirGrade_F
            // 
            this.BtnImprimirGrade_F.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("BtnImprimirGrade_F.ImageOptions.Image")));
            // 
            // BtnExportar_F
            // 
            this.BtnExportar_F.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("BtnExportar_F.ImageOptions.Image")));
            // 
            // btnConfirmar_F
            // 
            this.btnConfirmar_F.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("btnConfirmar_F.ImageOptions.Image")));
            // 
            // btnCancelar_F
            // 
            this.btnCancelar_F.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("btnCancelar_F.ImageOptions.Image")));
            // 
            // barDataBase_F
            // 
            this.barDataBase_F.OptionsBar.AllowQuickCustomization = false;
            this.barDataBase_F.OptionsBar.DisableClose = true;
            this.barDataBase_F.OptionsBar.DisableCustomization = true;
            this.barDataBase_F.OptionsBar.DrawDragBorder = false;
            this.barDataBase_F.OptionsBar.UseWholeRow = true;
            // 
            // GridControl_F
            // 
            this.GridControl_F.DataSource = this.planosSeguroConteudoBindingSource;
            this.GridControl_F.EmbeddedNavigator.Buttons.Append.Visible = false;
            this.GridControl_F.EmbeddedNavigator.Buttons.CancelEdit.Visible = false;
            this.GridControl_F.EmbeddedNavigator.Buttons.Edit.Visible = false;
            this.GridControl_F.EmbeddedNavigator.Buttons.EndEdit.Visible = false;
            this.GridControl_F.EmbeddedNavigator.Buttons.Remove.Visible = false;
            this.GridControl_F.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemImageComboBox1,
            this.repositoryItemCalcEdit1,
            this.repositoryItemButtonEdit1});
            this.GridControl_F.Size = new System.Drawing.Size(1614, 730);
            // 
            // GridView_F
            // 
            this.GridView_F.Appearance.ColumnFilterButton.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(243)))), ((int)(((byte)(174)))), ((int)(((byte)(66)))));
            this.GridView_F.Appearance.ColumnFilterButton.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(243)))), ((int)(((byte)(174)))), ((int)(((byte)(66)))));
            this.GridView_F.Appearance.ColumnFilterButton.ForeColor = System.Drawing.Color.Black;
            this.GridView_F.Appearance.ColumnFilterButton.Options.UseBackColor = true;
            this.GridView_F.Appearance.ColumnFilterButton.Options.UseBorderColor = true;
            this.GridView_F.Appearance.ColumnFilterButton.Options.UseForeColor = true;
            this.GridView_F.Appearance.ColumnFilterButtonActive.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(193)))), ((int)(((byte)(95)))));
            this.GridView_F.Appearance.ColumnFilterButtonActive.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(193)))), ((int)(((byte)(95)))));
            this.GridView_F.Appearance.ColumnFilterButtonActive.ForeColor = System.Drawing.Color.Black;
            this.GridView_F.Appearance.ColumnFilterButtonActive.Options.UseBackColor = true;
            this.GridView_F.Appearance.ColumnFilterButtonActive.Options.UseBorderColor = true;
            this.GridView_F.Appearance.ColumnFilterButtonActive.Options.UseForeColor = true;
            this.GridView_F.Appearance.Empty.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(238)))), ((int)(((byte)(238)))), ((int)(((byte)(238)))));
            this.GridView_F.Appearance.Empty.BackColor2 = System.Drawing.Color.White;
            this.GridView_F.Appearance.Empty.Options.UseBackColor = true;
            this.GridView_F.Appearance.EvenRow.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(248)))), ((int)(((byte)(248)))), ((int)(((byte)(248)))));
            this.GridView_F.Appearance.EvenRow.ForeColor = System.Drawing.Color.Black;
            this.GridView_F.Appearance.EvenRow.Options.UseBackColor = true;
            this.GridView_F.Appearance.EvenRow.Options.UseForeColor = true;
            this.GridView_F.Appearance.FilterCloseButton.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(243)))), ((int)(((byte)(174)))), ((int)(((byte)(66)))));
            this.GridView_F.Appearance.FilterCloseButton.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(243)))), ((int)(((byte)(174)))), ((int)(((byte)(66)))));
            this.GridView_F.Appearance.FilterCloseButton.ForeColor = System.Drawing.Color.Black;
            this.GridView_F.Appearance.FilterCloseButton.Options.UseBackColor = true;
            this.GridView_F.Appearance.FilterCloseButton.Options.UseBorderColor = true;
            this.GridView_F.Appearance.FilterCloseButton.Options.UseForeColor = true;
            this.GridView_F.Appearance.FilterPanel.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(238)))), ((int)(((byte)(238)))), ((int)(((byte)(238)))));
            this.GridView_F.Appearance.FilterPanel.BackColor2 = System.Drawing.Color.White;
            this.GridView_F.Appearance.FilterPanel.ForeColor = System.Drawing.Color.Black;
            this.GridView_F.Appearance.FilterPanel.Options.UseBackColor = true;
            this.GridView_F.Appearance.FilterPanel.Options.UseForeColor = true;
            this.GridView_F.Appearance.FixedLine.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(100)))), ((int)(((byte)(100)))), ((int)(((byte)(100)))));
            this.GridView_F.Appearance.FixedLine.Options.UseBackColor = true;
            this.GridView_F.Appearance.FocusedCell.BackColor = System.Drawing.Color.White;
            this.GridView_F.Appearance.FocusedCell.ForeColor = System.Drawing.Color.Black;
            this.GridView_F.Appearance.FocusedCell.Options.UseBackColor = true;
            this.GridView_F.Appearance.FocusedCell.Options.UseForeColor = true;
            this.GridView_F.Appearance.FocusedRow.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(194)))), ((int)(((byte)(194)))), ((int)(((byte)(194)))));
            this.GridView_F.Appearance.FocusedRow.ForeColor = System.Drawing.Color.Black;
            this.GridView_F.Appearance.FocusedRow.Options.UseBackColor = true;
            this.GridView_F.Appearance.FocusedRow.Options.UseForeColor = true;
            this.GridView_F.Appearance.FooterPanel.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(243)))), ((int)(((byte)(174)))), ((int)(((byte)(66)))));
            this.GridView_F.Appearance.FooterPanel.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(243)))), ((int)(((byte)(174)))), ((int)(((byte)(66)))));
            this.GridView_F.Appearance.FooterPanel.ForeColor = System.Drawing.Color.Black;
            this.GridView_F.Appearance.FooterPanel.Options.UseBackColor = true;
            this.GridView_F.Appearance.FooterPanel.Options.UseBorderColor = true;
            this.GridView_F.Appearance.FooterPanel.Options.UseForeColor = true;
            this.GridView_F.Appearance.GroupButton.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(243)))), ((int)(((byte)(174)))), ((int)(((byte)(66)))));
            this.GridView_F.Appearance.GroupButton.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(243)))), ((int)(((byte)(174)))), ((int)(((byte)(66)))));
            this.GridView_F.Appearance.GroupButton.Options.UseBackColor = true;
            this.GridView_F.Appearance.GroupButton.Options.UseBorderColor = true;
            this.GridView_F.Appearance.GroupFooter.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(193)))), ((int)(((byte)(95)))));
            this.GridView_F.Appearance.GroupFooter.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(193)))), ((int)(((byte)(95)))));
            this.GridView_F.Appearance.GroupFooter.ForeColor = System.Drawing.Color.Black;
            this.GridView_F.Appearance.GroupFooter.Options.UseBackColor = true;
            this.GridView_F.Appearance.GroupFooter.Options.UseBorderColor = true;
            this.GridView_F.Appearance.GroupFooter.Options.UseForeColor = true;
            this.GridView_F.Appearance.GroupPanel.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(238)))), ((int)(((byte)(238)))), ((int)(((byte)(238)))));
            this.GridView_F.Appearance.GroupPanel.BackColor2 = System.Drawing.Color.White;
            this.GridView_F.Appearance.GroupPanel.ForeColor = System.Drawing.Color.Black;
            this.GridView_F.Appearance.GroupPanel.Options.UseBackColor = true;
            this.GridView_F.Appearance.GroupPanel.Options.UseForeColor = true;
            this.GridView_F.Appearance.GroupRow.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(193)))), ((int)(((byte)(95)))));
            this.GridView_F.Appearance.GroupRow.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(193)))), ((int)(((byte)(95)))));
            this.GridView_F.Appearance.GroupRow.ForeColor = System.Drawing.Color.Black;
            this.GridView_F.Appearance.GroupRow.Options.UseBackColor = true;
            this.GridView_F.Appearance.GroupRow.Options.UseBorderColor = true;
            this.GridView_F.Appearance.GroupRow.Options.UseForeColor = true;
            this.GridView_F.Appearance.HeaderPanel.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(193)))), ((int)(((byte)(95)))));
            this.GridView_F.Appearance.HeaderPanel.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(193)))), ((int)(((byte)(95)))));
            this.GridView_F.Appearance.HeaderPanel.ForeColor = System.Drawing.Color.Black;
            this.GridView_F.Appearance.HeaderPanel.Options.UseBackColor = true;
            this.GridView_F.Appearance.HeaderPanel.Options.UseBorderColor = true;
            this.GridView_F.Appearance.HeaderPanel.Options.UseForeColor = true;
            this.GridView_F.Appearance.HideSelectionRow.BackColor = System.Drawing.Color.Gainsboro;
            this.GridView_F.Appearance.HideSelectionRow.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(100)))), ((int)(((byte)(100)))), ((int)(((byte)(100)))));
            this.GridView_F.Appearance.HideSelectionRow.Options.UseBackColor = true;
            this.GridView_F.Appearance.HideSelectionRow.Options.UseForeColor = true;
            this.GridView_F.Appearance.HorzLine.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(175)))), ((int)(((byte)(175)))), ((int)(((byte)(175)))));
            this.GridView_F.Appearance.HorzLine.Options.UseBackColor = true;
            this.GridView_F.Appearance.OddRow.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(238)))), ((int)(((byte)(238)))), ((int)(((byte)(238)))));
            this.GridView_F.Appearance.OddRow.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(238)))), ((int)(((byte)(238)))), ((int)(((byte)(238)))));
            this.GridView_F.Appearance.OddRow.ForeColor = System.Drawing.Color.Black;
            this.GridView_F.Appearance.OddRow.Options.UseBackColor = true;
            this.GridView_F.Appearance.OddRow.Options.UseBorderColor = true;
            this.GridView_F.Appearance.OddRow.Options.UseForeColor = true;
            this.GridView_F.Appearance.Preview.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(250)))), ((int)(((byte)(250)))), ((int)(((byte)(250)))));
            this.GridView_F.Appearance.Preview.Font = new System.Drawing.Font("Verdana", 7.5F);
            this.GridView_F.Appearance.Preview.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(100)))), ((int)(((byte)(100)))), ((int)(((byte)(100)))));
            this.GridView_F.Appearance.Preview.Options.UseBackColor = true;
            this.GridView_F.Appearance.Preview.Options.UseFont = true;
            this.GridView_F.Appearance.Preview.Options.UseForeColor = true;
            this.GridView_F.Appearance.Row.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(238)))), ((int)(((byte)(238)))), ((int)(((byte)(238)))));
            this.GridView_F.Appearance.Row.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(238)))), ((int)(((byte)(238)))), ((int)(((byte)(238)))));
            this.GridView_F.Appearance.Row.ForeColor = System.Drawing.Color.Black;
            this.GridView_F.Appearance.Row.Options.UseBackColor = true;
            this.GridView_F.Appearance.Row.Options.UseBorderColor = true;
            this.GridView_F.Appearance.Row.Options.UseForeColor = true;
            this.GridView_F.Appearance.RowSeparator.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(238)))), ((int)(((byte)(238)))), ((int)(((byte)(238)))));
            this.GridView_F.Appearance.RowSeparator.BackColor2 = System.Drawing.Color.White;
            this.GridView_F.Appearance.RowSeparator.Options.UseBackColor = true;
            this.GridView_F.Appearance.SelectedRow.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(215)))), ((int)(((byte)(215)))), ((int)(((byte)(215)))));
            this.GridView_F.Appearance.SelectedRow.ForeColor = System.Drawing.Color.Black;
            this.GridView_F.Appearance.SelectedRow.Options.UseBackColor = true;
            this.GridView_F.Appearance.SelectedRow.Options.UseForeColor = true;
            this.GridView_F.Appearance.TopNewRow.BackColor = System.Drawing.Color.White;
            this.GridView_F.Appearance.TopNewRow.Options.UseBackColor = true;
            this.GridView_F.Appearance.VertLine.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(175)))), ((int)(((byte)(175)))), ((int)(((byte)(175)))));
            this.GridView_F.Appearance.VertLine.Options.UseBackColor = true;
            this.GridView_F.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colPSC,
            this.colPSCDescricao,
            this.colPSCValor,
            this.colPSCStatus,
            this.colPSCI_USU,
            this.colPSCA_USU,
            this.colPSCDataI,
            this.colPSCDataA,
            this.gridColumn1});
            this.GridView_F.OptionsBehavior.AllowIncrementalSearch = true;
            this.GridView_F.OptionsBehavior.Editable = false;
            this.GridView_F.OptionsLayout.Columns.StoreAllOptions = true;
            this.GridView_F.OptionsLayout.Columns.StoreAppearance = true;
            this.GridView_F.OptionsLayout.StoreAllOptions = true;
            this.GridView_F.OptionsLayout.StoreAppearance = true;
            this.GridView_F.OptionsNavigation.EnterMoveNextColumn = true;
            this.GridView_F.OptionsPrint.EnableAppearanceEvenRow = true;
            this.GridView_F.OptionsPrint.EnableAppearanceOddRow = true;
            this.GridView_F.OptionsPrint.ExpandAllDetails = true;
            this.GridView_F.OptionsView.ColumnAutoWidth = false;
            this.GridView_F.OptionsView.EnableAppearanceEvenRow = true;
            this.GridView_F.OptionsView.EnableAppearanceOddRow = true;
            this.GridView_F.OptionsView.GroupFooterShowMode = DevExpress.XtraGrid.Views.Grid.GroupFooterShowMode.VisibleAlways;
            this.GridView_F.OptionsView.ShowFooter = true;
            this.GridView_F.OptionsView.ShowGroupPanel = false;
            this.GridView_F.SortInfo.AddRange(new DevExpress.XtraGrid.Columns.GridColumnSortInfo[] {
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colPSCDescricao, DevExpress.Data.ColumnSortOrder.Ascending)});
            // 
            // ToolTipController_F
            // 
            this.ToolTipController_F.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(222)))), ((int)(((byte)(237)))), ((int)(((byte)(250)))));
            this.ToolTipController_F.Appearance.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(20)))), ((int)(((byte)(69)))), ((int)(((byte)(141)))));
            this.ToolTipController_F.Appearance.Options.UseBackColor = true;
            this.ToolTipController_F.Appearance.Options.UseFont = true;
            this.ToolTipController_F.Appearance.Options.UseForeColor = true;
            this.ToolTipController_F.Appearance.Options.UseTextOptions = true;
            this.ToolTipController_F.Appearance.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            // 
            // StyleController_F
            // 
            this.StyleController_F.AppearanceDisabled.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this.StyleController_F.AppearanceDisabled.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this.StyleController_F.AppearanceDisabled.Options.UseBackColor = true;
            this.StyleController_F.AppearanceDisabled.Options.UseForeColor = true;
            this.StyleController_F.AppearanceFocused.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.StyleController_F.AppearanceFocused.Options.UseBackColor = true;
            // 
            // planosSeguroConteudoBindingSource
            // 
            this.planosSeguroConteudoBindingSource.DataMember = "PlanosSeguroConteudo";
            this.planosSeguroConteudoBindingSource.DataSource = typeof(Framework.datasets.dPlanosSeguroConteudo);
            // 
            // colPSC
            // 
            this.colPSC.FieldName = "PSC";
            this.colPSC.Name = "colPSC";
            this.colPSC.OptionsColumn.ReadOnly = true;
            // 
            // colPSCDescricao
            // 
            this.colPSCDescricao.Caption = "Descrição";
            this.colPSCDescricao.FieldName = "PSCDescricao";
            this.colPSCDescricao.Name = "colPSCDescricao";
            this.colPSCDescricao.Visible = true;
            this.colPSCDescricao.VisibleIndex = 0;
            this.colPSCDescricao.Width = 301;
            // 
            // colPSCValor
            // 
            this.colPSCValor.Caption = "Valor";
            this.colPSCValor.ColumnEdit = this.repositoryItemCalcEdit1;
            this.colPSCValor.FieldName = "PSCValor";
            this.colPSCValor.Name = "colPSCValor";
            this.colPSCValor.Visible = true;
            this.colPSCValor.VisibleIndex = 1;
            this.colPSCValor.Width = 128;
            // 
            // repositoryItemCalcEdit1
            // 
            this.repositoryItemCalcEdit1.AutoHeight = false;
            this.repositoryItemCalcEdit1.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemCalcEdit1.Mask.EditMask = "n2";
            this.repositoryItemCalcEdit1.Mask.UseMaskAsDisplayFormat = true;
            this.repositoryItemCalcEdit1.Name = "repositoryItemCalcEdit1";
            // 
            // colPSCStatus
            // 
            this.colPSCStatus.Caption = "Status";
            this.colPSCStatus.ColumnEdit = this.repositoryItemImageComboBox1;
            this.colPSCStatus.FieldName = "PSCStatus";
            this.colPSCStatus.Name = "colPSCStatus";
            this.colPSCStatus.Visible = true;
            this.colPSCStatus.VisibleIndex = 2;
            this.colPSCStatus.Width = 89;
            // 
            // repositoryItemImageComboBox1
            // 
            this.repositoryItemImageComboBox1.AutoHeight = false;
            this.repositoryItemImageComboBox1.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemImageComboBox1.Name = "repositoryItemImageComboBox1";
            // 
            // colPSCI_USU
            // 
            this.colPSCI_USU.FieldName = "PSCI_USU";
            this.colPSCI_USU.Name = "colPSCI_USU";
            // 
            // colPSCA_USU
            // 
            this.colPSCA_USU.FieldName = "PSCA_USU";
            this.colPSCA_USU.Name = "colPSCA_USU";
            // 
            // colPSCDataI
            // 
            this.colPSCDataI.FieldName = "PSCDataI";
            this.colPSCDataI.Name = "colPSCDataI";
            // 
            // colPSCDataA
            // 
            this.colPSCDataA.FieldName = "PSCDataA";
            this.colPSCDataA.Name = "colPSCDataA";
            // 
            // gridColumn1
            // 
            this.gridColumn1.Caption = "Impresso Boleto";
            this.gridColumn1.ColumnEdit = this.repositoryItemButtonEdit1;
            this.gridColumn1.Name = "gridColumn1";
            this.gridColumn1.Visible = true;
            this.gridColumn1.VisibleIndex = 3;
            // 
            // repositoryItemButtonEdit1
            // 
            this.repositoryItemButtonEdit1.AutoHeight = false;
            editorButtonImageOptions2.Image = ((System.Drawing.Image)(resources.GetObject("editorButtonImageOptions2.Image")));
            this.repositoryItemButtonEdit1.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Ellipsis, "Editar", -1, true, true, false, editorButtonImageOptions1, new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), serializableAppearanceObject1, serializableAppearanceObject2, serializableAppearanceObject3, serializableAppearanceObject4, "", null, null, DevExpress.Utils.ToolTipAnchor.Default),
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Glyph, "Testar", -1, true, true, false, editorButtonImageOptions2, new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), serializableAppearanceObject5, serializableAppearanceObject6, serializableAppearanceObject7, serializableAppearanceObject8, "", null, null, DevExpress.Utils.ToolTipAnchor.Default)});
            this.repositoryItemButtonEdit1.Name = "repositoryItemButtonEdit1";
            this.repositoryItemButtonEdit1.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.HideTextEditor;
            this.repositoryItemButtonEdit1.ButtonClick += new DevExpress.XtraEditors.Controls.ButtonPressedEventHandler(this.repositoryItemButtonEdit1_ButtonClick);
            // 
            // cPlanosSeguroConteudo
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BindingSourcePrincipal = this.planosSeguroConteudoBindingSource;
            this.BotaoExcluir = false;
            this.LookAndFeel.UseDefaultLookAndFeel = false;
            this.Name = "cPlanosSeguroConteudo";
            this.Size = new System.Drawing.Size(1614, 790);
            this.Titulo = "Planos de seguro conteúdo";
            ((System.ComponentModel.ISupportInitialize)(this.PopImprimir_F)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.BarManager_F)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.PopExportar_F)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.GridControl_F)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.GridView_F)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.BindingSource_F)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.StyleController_F)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.planosSeguroConteudoBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCalcEdit1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemImageComboBox1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemButtonEdit1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.BindingSource planosSeguroConteudoBindingSource;
        private DevExpress.XtraEditors.Repository.RepositoryItemImageComboBox repositoryItemImageComboBox1;
        private DevExpress.XtraGrid.Columns.GridColumn colPSC;
        private DevExpress.XtraGrid.Columns.GridColumn colPSCDescricao;
        private DevExpress.XtraGrid.Columns.GridColumn colPSCValor;
        private DevExpress.XtraGrid.Columns.GridColumn colPSCStatus;
        private DevExpress.XtraGrid.Columns.GridColumn colPSCI_USU;
        private DevExpress.XtraGrid.Columns.GridColumn colPSCA_USU;
        private DevExpress.XtraGrid.Columns.GridColumn colPSCDataI;
        private DevExpress.XtraGrid.Columns.GridColumn colPSCDataA;
        private DevExpress.XtraEditors.Repository.RepositoryItemCalcEdit repositoryItemCalcEdit1;
        private DevExpress.XtraEditors.Repository.RepositoryItemButtonEdit repositoryItemButtonEdit1;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn1;
    }
}
