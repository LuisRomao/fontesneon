﻿using Framework.datasets;
using DevExpress.XtraReports.UI;

namespace Cadastros.seguroconteudo
{
    /// <summary>
    /// Planos de seguro
    /// </summary>
    public partial class cPlanosSeguroConteudo : CompontesBasicos.ComponenteGradeNavegador
    {
        /// <summary>
        /// Construtor
        /// </summary>
        public cPlanosSeguroConteudo()
        {
            InitializeComponent();
            Framework.virEnumPSCStatus.virEnumPSCStatusSt.CarregaEditorDaGrid(colPSCStatus);
            tabela = Framework.datasets.dPlanosSeguroConteudo.dPlanosSeguroConteudoSt.PlanosSeguroConteudo;
            TableAdapterPrincipal = Framework.datasets.dPlanosSeguroConteudo.dPlanosSeguroConteudoSt.PlanosSeguroConteudoTableAdapter;            
            planosSeguroConteudoBindingSource.DataSource = Framework.datasets.dPlanosSeguroConteudo.dPlanosSeguroConteudoSt;            
        }

        private void repositoryItemButtonEdit1_ButtonClick(object sender, DevExpress.XtraEditors.Controls.ButtonPressedEventArgs e)
        {
            dPlanosSeguroConteudo.PlanosSeguroConteudoRow row = (dPlanosSeguroConteudo.PlanosSeguroConteudoRow)GridView_F.GetFocusedDataRow();
            if (row == null)
                return;
            if (e.Button.Kind == DevExpress.XtraEditors.Controls.ButtonPredefines.Ellipsis)
            {
                string DescProcuracao = string.Format("Seguro Conteúdo - {0}", row.PSC);
                maladireta.EditorRTF.cCamposImpresso Editor = new maladireta.EditorRTF.cCamposImpresso(DescProcuracao);
                Editor.ShowEmPopUp();
            }
            else
            {
                dllImpresso.ImpBoletoSeguro Teste = new dllImpresso.ImpBoletoSeguro();
                Teste.CarregarImpresso(string.Format("Seguro Conteúdo - {0}", row.PSC));
                Teste.CreateDocument();
                Teste.ShowPreviewDialog();
            }
        }
    }
}
