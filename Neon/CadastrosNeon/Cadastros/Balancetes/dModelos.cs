﻿namespace Cadastros.Balancetes {
    
    
    public partial class dModelos 
    {
        private dModelosTableAdapters.ModeloBalanceteGrupoTableAdapter modeloBalanceteGrupoTableAdapter;
        /// <summary>
        /// TableAdapter padrão para tabela: ModeloBalanceteGrupo
        /// </summary>
        public dModelosTableAdapters.ModeloBalanceteGrupoTableAdapter ModeloBalanceteGrupoTableAdapter
        {
            get
            {
                if (modeloBalanceteGrupoTableAdapter == null)
                {
                    modeloBalanceteGrupoTableAdapter = new dModelosTableAdapters.ModeloBalanceteGrupoTableAdapter();
                    modeloBalanceteGrupoTableAdapter.TrocarStringDeConexao();
                };
                return modeloBalanceteGrupoTableAdapter;
            }
        }

        private dModelosTableAdapters.MBDxPLATableAdapter mBDxPLATableAdapter;
        /// <summary>
        /// TableAdapter padrão para tabela: MBDxPLA
        /// </summary>
        public dModelosTableAdapters.MBDxPLATableAdapter MBDxPLATableAdapter
        {
            get
            {
                if (mBDxPLATableAdapter == null)
                {
                    mBDxPLATableAdapter = new dModelosTableAdapters.MBDxPLATableAdapter();
                    mBDxPLATableAdapter.TrocarStringDeConexao();
                };
                return mBDxPLATableAdapter;
            }
        }

        private dModelosTableAdapters.ModeloBAlanceteTableAdapter modeloBAlanceteTableAdapter;
        /// <summary>
        /// TableAdapter padrão para tabela: ModeloBAlancete
        /// </summary>
        public dModelosTableAdapters.ModeloBAlanceteTableAdapter ModeloBAlanceteTableAdapter
        {
            get
            {
                if (modeloBAlanceteTableAdapter == null)
                {
                    modeloBAlanceteTableAdapter = new dModelosTableAdapters.ModeloBAlanceteTableAdapter();
                    modeloBAlanceteTableAdapter.TrocarStringDeConexao();
                };
                return modeloBAlanceteTableAdapter;
            }
        }

        private dModelosTableAdapters.ModeloBalanceteDetalheTableAdapter modeloBalanceteDetalheTableAdapter;
        /// <summary>
        /// TableAdapter padrão para tabela: ModeloBalanceteDetalhe
        /// </summary>
        public dModelosTableAdapters.ModeloBalanceteDetalheTableAdapter ModeloBalanceteDetalheTableAdapter
        {
            get
            {
                if (modeloBalanceteDetalheTableAdapter == null)
                {
                    modeloBalanceteDetalheTableAdapter = new dModelosTableAdapters.ModeloBalanceteDetalheTableAdapter();
                    modeloBalanceteDetalheTableAdapter.TrocarStringDeConexao();
                };
                return modeloBalanceteDetalheTableAdapter;
            }
        }
    }
}
