﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Drawing;
using System.Windows.Forms;
using CompontesBasicos;
using DevExpress.XtraGrid;
using DevExpress.XtraGrid.Views.Grid;
using DevExpress.XtraGrid.Views.Grid.ViewInfo;
using Framework.datasets;
using Framework.objetosNeon;
using VirEnumeracoesNeon;

namespace Cadastros.Balancetes
{
    /// <summary>
    /// Modelos de balancete
    /// </summary>
    public partial class cModelos : ComponenteBase
    {
        private dModelos dModelos1;

        private void ajustaQuadrinho(object oGrid)
        {
            DevExpress.XtraGrid.Columns.GridColumn column;
            System.Reflection.FieldInfo myPropertyInfo1 = oGrid.GetType().GetField("checkboxSelectorColumn", System.Reflection.BindingFlags.NonPublic | System.Reflection.BindingFlags.Instance);
            if (myPropertyInfo1 != null)
            {
                column = myPropertyInfo1.GetValue(oGrid) as DevExpress.XtraGrid.Columns.GridColumn;
                if (column != null)
                    column.Width = 50;// 30 é pequento    300 é grande 50?
            }
        }

        /// <summary>
        /// Construtor
        /// </summary>
        public cModelos()
        {
            InitializeComponent();
            dModelos1 = new dModelos();
            CarregaDados();            
            modeloBAlanceteBindingSource.DataSource = dModelos1;
            pLAnaoconfiguradoBindingSource.DataSource = dModelos1;
            dPLAnocontasBindingSource.DataSource = Framework.datasets.dPLAnocontas.dPLAnocontasSt;
            AgrupadorBalancete.virEnumMBDDetalhamento.virEnumMBDDetalhamentoSt.CarregaEditorDaGrid(colMBDDetalhamento);
            if (dModelos1.ModeloBAlancete.Count > 0)
                lookUpEdit1.EditValue = dModelos1.ModeloBAlancete[0].MBA;            
            ajustaQuadrinho(gridView2);
            ajustaQuadrinho(gridView3);
        }

        private void CarregaDados(bool recarga= false)
        {
            if (recarga)
            {
                dModelos1.MBDxPLA.Clear();
                dModelos1.ModeloBalanceteDetalhe.Clear();
                dModelos1.ModeloBalanceteGrupo.Clear();
            }
            dModelos1.ModeloBAlanceteTableAdapter.Fill(dModelos1.ModeloBAlancete);
            dModelos1.ModeloBalanceteGrupoTableAdapter.Fill(dModelos1.ModeloBalanceteGrupo);            
            dModelos1.ModeloBalanceteDetalheTableAdapter.Fill(dModelos1.ModeloBalanceteDetalhe);
            dModelos1.MBDxPLATableAdapter.Fill(dModelos1.MBDxPLA);
            if ((recarga) && (dModelos1.ModeloBAlancete.Count > 0))
                lookUpEdit1.EditValue = dModelos1.ModeloBAlancete[0].MBA;
            _MaiorOrdemMBG = null;            
        }

        private void simpleButton1_Click(object sender, EventArgs e)
        {
            CriarNovo();
        }        

        private bool CriarNovo()
        {
            
            cNovoModelo cNovo = new cNovoModelo(dModelos1);
            if (cNovo.ShowEmPopUp() == DialogResult.OK)
            {
                Framework.objetosNeon.dAgrupadorBalancete.ResetSt();
                string NovoNome = cNovo.retNome.Trim();
                foreach (dModelos.ModeloBAlanceteRow rowTeste in dModelos1.ModeloBAlancete)
                    if (rowTeste.MBANome == NovoNome)
                    {
                        MessageBox.Show("Modelo já existe");
                        return false;
                    }
                try
                {
                    VirMSSQL.TableAdapter.AbreTrasacaoSQL("Cadastros cModelos CriarNovo - 57",
                                                            dModelos1.ModeloBAlanceteTableAdapter,
                                                            dModelos1.ModeloBalanceteGrupoTableAdapter,
                                                            dModelos1.ModeloBalanceteDetalheTableAdapter,
                                                            dModelos1.MBDxPLATableAdapter);
                    dModelos.ModeloBAlanceteRow Novarow = dModelos1.ModeloBAlancete.NewModeloBAlanceteRow();
                    Novarow.MBANome = NovoNome;
                    dModelos1.ModeloBAlancete.AddModeloBAlanceteRow(Novarow);
                    dModelos1.ModeloBAlanceteTableAdapter.Update(Novarow);
                    Novarow.AcceptChanges();
                    dModelos.ModeloBalanceteDetalheRow rowMBD;
                    switch (cNovo.retTipo)
                    {
                        case AgrupadorBalancete.TipoClassificacaoDesp.CadastroMBA:
                            int? MDBescolhido = cNovo.retModeloBaseEscolhido;
                            dModelos.ModeloBAlanceteRow MBAmodelo = dModelos1.ModeloBAlancete.FindByMBA(MDBescolhido.Value);
                            foreach (dModelos.ModeloBalanceteGrupoRow MBGmodelo in MBAmodelo.GetModeloBalanceteGrupoRows())
                            {
                                dModelos.ModeloBalanceteGrupoRow rowMBG = dModelos1.ModeloBalanceteGrupo.NewModeloBalanceteGrupoRow();
                                rowMBG.MBG_MBA = Novarow.MBA;
                                rowMBG.MBGOrdem = MBGmodelo.MBGOrdem;
                                rowMBG.MBGTitulo = MBGmodelo.MBGTitulo;
                                dModelos1.ModeloBalanceteGrupo.AddModeloBalanceteGrupoRow(rowMBG);
                                dModelos1.ModeloBalanceteGrupoTableAdapter.Update(rowMBG);
                                rowMBG.AcceptChanges();
                                foreach (dModelos.ModeloBalanceteDetalheRow MBDmodelo in MBGmodelo.GetModeloBalanceteDetalheRows())
                                {
                                    rowMBD = dModelos1.ModeloBalanceteDetalhe.NewModeloBalanceteDetalheRow();
                                    rowMBD.MBD_MBG = rowMBG.MBG;
                                    rowMBD.MBDCred = MBDmodelo.MBDCred;
                                    rowMBD.MBDDetalhamento = MBDmodelo.MBDDetalhamento;
                                    rowMBD.MBDMostraZerado = MBDmodelo.MBDMostraZerado;
                                    rowMBD.MBDOrdem = MBDmodelo.MBDOrdem;
                                    rowMBD.MBDTitulo = MBDmodelo.MBDTitulo;
                                    dModelos1.ModeloBalanceteDetalhe.AddModeloBalanceteDetalheRow(rowMBD);
                                    dModelos1.ModeloBalanceteDetalheTableAdapter.Update(rowMBD);
                                    rowMBD.AcceptChanges();
                                    foreach (dModelos.MBDxPLARow MBDxPLAModelo in MBDmodelo.GetMBDxPLARows())
                                    {
                                        dModelos.MBDxPLARow NovoPLA = dModelos1.MBDxPLA.NewMBDxPLARow();
                                        NovoPLA.MBD = rowMBD.MBD;
                                        NovoPLA.PLA = MBDxPLAModelo.PLA;
                                        NovoPLA.MBDxPLADescricaoLivre = MBDxPLAModelo.MBDxPLADescricaoLivre;
                                        NovoPLA.MBDxPLAOrdem = MBDxPLAModelo.MBDxPLAOrdem;
                                        dModelos1.MBDxPLA.AddMBDxPLARow(NovoPLA);
                                        dModelos1.MBDxPLATableAdapter.Update(NovoPLA);
                                        NovoPLA.AcceptChanges();
                                    }
                                }
                            }
                            break;
                        case AgrupadorBalancete.TipoClassificacaoDesp.Grupos:
                        case AgrupadorBalancete.TipoClassificacaoDesp.PlanoContas:
                            dModelos.ModeloBalanceteGrupoRow rowGUnico = dModelos1.ModeloBalanceteGrupo.NewModeloBalanceteGrupoRow();
                            rowGUnico.MBG_MBA = Novarow.MBA;
                            rowGUnico.MBGOrdem = 1;
                            rowGUnico.MBGTitulo = "Único";
                            dModelos1.ModeloBalanceteGrupo.AddModeloBalanceteGrupoRow(rowGUnico);
                            dModelos1.ModeloBalanceteGrupoTableAdapter.Update(rowGUnico);
                            rowGUnico.AcceptChanges();
                            SortedList<string, dModelos.ModeloBalanceteDetalheRow> Adcionados = new SortedList<string, dModelos.ModeloBalanceteDetalheRow>();
                            int Ordem = 0;
                            int OrdemCreditos = 1;
                            foreach (dPLAnocontas.PLAnocontasRow rowPLA in dPLAnocontas.dPLAnocontasSt.PLAnocontas)
                            {
                                string TituloGrupo;
                                string Chave;
                                int ordemGrupo;
                                if (rowPLA.PLA.Substring(0, 1) == "1")
                                {
                                    TituloGrupo = rowPLA.PLADescricaoRes;
                                    ordemGrupo = OrdemCreditos++;
                                    Chave = "1" + TituloGrupo;
                                }
                                else if (rowPLA.PLA.Substring(0, 1) == "2")
                                {
                                    AgrupadorBalancete.solicitaAgrupamento Solicitacao = new AgrupadorBalancete.solicitaAgrupamento(cNovo.retTipo);
                                    AgrupadorBalancete.retornoClassificacao Classificacao = AgrupadorBalancete.ClassificaDespesa(Solicitacao, rowPLA.PLA,rowPLA.PLADescricaoRes);
                                    TituloGrupo = Classificacao.Titulo;
                                    ordemGrupo = Classificacao.Ordem;
                                    Chave = "2" + TituloGrupo;
                                }
                                else
                                    continue;
                                if (Adcionados.ContainsKey(Chave))
                                    rowMBD = Adcionados[Chave];
                                else
                                {
                                    rowMBD = dModelos1.ModeloBalanceteDetalhe.NewModeloBalanceteDetalheRow();
                                    rowMBD.MBD_MBG = rowGUnico.MBG;
                                    rowMBD.MBDCred = rowPLA.PLA.Substring(0, 1) == "1";                                    
                                    if (!rowMBD.MBDCred)
                                        rowMBD.MBDDetalhamento = (int)AgrupadorBalancete.MBDDetalhamento.Descricao;
                                    else
                                    {
                                        if (rowPLA.PLADescricaoLivre)
                                            rowMBD.MBDDetalhamento = (int)AgrupadorBalancete.MBDDetalhamento.Descricao;
                                        else
                                            rowMBD.MBDDetalhamento = (int)AgrupadorBalancete.MBDDetalhamento.PlanoDeContas;
                                    }
                                    rowMBD.MBDMostraZerado = false;
                                    rowMBD.MBDOrdem = ordemGrupo;
                                    rowMBD.MBDTitulo = TituloGrupo;
                                    dModelos1.ModeloBalanceteDetalhe.AddModeloBalanceteDetalheRow(rowMBD);
                                    dModelos1.ModeloBalanceteDetalheTableAdapter.Update(rowMBD);
                                    rowMBD.AcceptChanges();
                                    Adcionados.Add(Chave, rowMBD);
                                }
                                dModelos.MBDxPLARow NovoPLA = dModelos1.MBDxPLA.NewMBDxPLARow();
                                NovoPLA.MBD = rowMBD.MBD;
                                NovoPLA.PLA = rowPLA.PLA;
                                if (rowMBD.MBDCred)
                                    NovoPLA.MBDxPLADescricaoLivre = rowPLA.PLADescricaoLivre;
                                else
                                    NovoPLA.MBDxPLADescricaoLivre = true;
                                NovoPLA.MBDxPLAOrdem = Ordem++;
                                dModelos1.MBDxPLA.AddMBDxPLARow(NovoPLA);
                                dModelos1.MBDxPLATableAdapter.Update(NovoPLA);
                                NovoPLA.AcceptChanges();
                            }
                            break;
                        case AgrupadorBalancete.TipoClassificacaoDesp.ContasLogicas:
                        case AgrupadorBalancete.TipoClassificacaoDesp.EmBanco:
                        default:
                            break;
                    }

                    VirMSSQL.TableAdapter.CommitSQL();
                    lookUpEdit1.EditValue = Novarow.MBA;
                }
                catch (Exception ex)
                {
                    VirMSSQL.TableAdapter.VircatchSQL(ex);
                    MessageBox.Show("Erro na gravação!");
                    CarregaDados(true);
                    return false;
                }
                return true;
            }
            else
                return false;
        }

        private void lookUpEdit1_Properties_ButtonClick(object sender, DevExpress.XtraEditors.Controls.ButtonPressedEventArgs e)
        {
            if (e.Button.Kind == DevExpress.XtraEditors.Controls.ButtonPredefines.Plus)
                CriarNovo();
        }

        private dModelos.ModeloBAlanceteRow LinhaMae
        {
            get 
            {
                if (lookUpEdit1.EditValue == null)
                    return null;
                else
                    return dModelos1.ModeloBAlancete.FindByMBA((int)lookUpEdit1.EditValue);
            }
        }

        private void simpleButton2_Click(object sender, EventArgs e)
        {
            string Nova = "";
            if (!VirInput.Input.Execute("Obs", ref Nova, true) || Nova == "")
                return;            
            dModelos.ModeloBAlanceteRow roweditar = LinhaMae;
            if (roweditar != null)
            {
                string antiga = roweditar.IsMBAOBSNull() ? "" : roweditar.MBAOBS + "\r\n";
                roweditar.MBAOBS = antiga + Nova;
                dModelos1.ModeloBAlanceteTableAdapter.Update(roweditar);
                roweditar.AcceptChanges();
            }
        }

        private void expandirGrupos(bool exp)
        {
            if (exp)
                for (int hand1 = 0; hand1 < gridView4.RowCount; hand1++)
                    gridView4.ExpandMasterRow(hand1);
            else
                gridView4.CollapseAllDetails();            
        }

        private void lookUpEdit1_EditValueChanged(object sender, EventArgs e)
        {               
            modeloBAlanceteBindingSource.Position = modeloBAlanceteBindingSource.Find("MBA", lookUpEdit1.EditValue);
            dModelos1.PLAnaoconfigurado.Clear();
            List<string> PLAusados = new List<string>();
            foreach (dModelos.ModeloBalanceteGrupoRow rowMBG in LinhaMae.GetModeloBalanceteGrupoRows())
                foreach (dModelos.ModeloBalanceteDetalheRow rowMBD in rowMBG.GetModeloBalanceteDetalheRows())
                    foreach (dModelos.MBDxPLARow rowMBDxPLA in rowMBD.GetMBDxPLARows())
                        PLAusados.Add(rowMBDxPLA.PLA);
            foreach(Framework.datasets.dPLAnocontas.PLAnocontasRow rowPLA in Framework.datasets.dPLAnocontas.dPLAnocontasSt.PLAnocontas)
            {
                string TipoPLA = rowPLA.PLA.Substring(0,1);
                if ((TipoPLA == "1") || (TipoPLA == "2") || (TipoPLA == "9"))
                    if (!PLAusados.Contains(rowPLA.PLA))
                        dModelos1.PLAnaoconfigurado.AddPLAnaoconfiguradoRow(rowPLA.PLA, rowPLA.PLADescricaoRes);
            };
            expandirGrupos(checkEdit1.Checked);             
        }

        private void gridView1_RowUpdated(object sender, DevExpress.XtraGrid.Views.Base.RowObjectEventArgs e)
        {
            DataRowView DRV = (DataRowView)e.Row;
            if((DRV == null) || (DRV.Row == null))
                return;
            dModelos.ModeloBalanceteDetalheRow row = (dModelos.ModeloBalanceteDetalheRow)DRV.Row;
            if ((row.RowState == DataRowState.Modified) || (row.RowState == DataRowState.Added))
            {
                Framework.objetosNeon.dAgrupadorBalancete.ResetSt();
                string Manobra = CompontesBasicos.ObjetosEstaticos.StringEdit.Limpa(row.MBDTitulo, CompontesBasicos.ObjetosEstaticos.StringEdit.TiposLimpesa.PrimeiroCaracMaiusculo);
                if (row.MBDTitulo != Manobra)
                    row.MBDTitulo = Manobra;
                if (row.MBDOrdem == -1)
                {
                    dModelos.ModeloBalanceteGrupoRow rowPai = dModelos1.ModeloBalanceteGrupo.FindByMBG(row.MBD_MBG); 
                    int MaiorOrdem = 0;
                    foreach (dModelos.ModeloBalanceteDetalheRow rowi in rowPai.GetModeloBalanceteDetalheRows())
                        if (rowi.MBDOrdem > MaiorOrdem)
                            MaiorOrdem = rowi.MBDOrdem;
                    row.MBDOrdem = MaiorOrdem + 1;                
                }
                dModelos1.ModeloBalanceteDetalheTableAdapter.Update(row);
                row.AcceptChanges();
            }
        }

        private void gridView1_InitNewRow(object sender, DevExpress.XtraGrid.Views.Grid.InitNewRowEventArgs e)
        {
            GridView view = (GridView)sender;            
            dModelos.ModeloBalanceteDetalheRow row = (dModelos.ModeloBalanceteDetalheRow)view.GetDataRow(e.RowHandle);
            //GridView viewPai = (GridView)view.ParentView;
            //dModelos.ModeloBalanceteGrupoRow rowPai = (dModelos.ModeloBalanceteGrupoRow)viewPai.GetFocusedDataRow(); 
            if (row != null)
            {                
                row.MBDCred = false;
                row.MBDDetalhamento = (int)AgrupadorBalancete.MBDDetalhamento.UmaLinha;
                row.MBDMostraZerado = false;
                row.MBDOrdem = -1;
                row.MBDTitulo = "";                                                                
            }
        }

        private enum sentido { sobe, desce }
        
        private void repositoryItemButtonEdit1_ButtonClick(object sender, DevExpress.XtraEditors.Controls.ButtonPressedEventArgs e)
        {
            GridView view = (GridView)gridControl1.FocusedView;
            if (view == null)
                return;
            dModelos.ModeloBalanceteDetalheRow rowMBD = (dModelos.ModeloBalanceteDetalheRow)view.GetFocusedDataRow();
            if(rowMBD == null)
                return;
            Framework.objetosNeon.dAgrupadorBalancete.ResetSt();
            switch (e.Button.Kind)
            {
                //case DevExpress.XtraEditors.Controls.ButtonPredefines.Up:
                //    move(gridView1.FocusedRowHandle, sentido.sobe);
                //    break;
                //case DevExpress.XtraEditors.Controls.ButtonPredefines.Down:
                //    move(gridView1.FocusedRowHandle, sentido.desce);
                //    break;
                case DevExpress.XtraEditors.Controls.ButtonPredefines.Delete:
                    dModelos.MBDxPLARow[] Abandonados = rowMBD.GetMBDxPLARows();
                    foreach (dModelos.MBDxPLARow rowDel in Abandonados)
                    {
                        dPLAnocontas.PLAnocontasRow rowPLA = dPLAnocontas.dPLAnocontasSt.PLAnocontas.FindByPLA(rowDel.PLA);
                        dModelos1.PLAnaoconfigurado.AddPLAnaoconfiguradoRow(rowDel.PLA, rowPLA.PLADescricaoRes);
                        rowDel.Delete();
                    }
                    try
                    {
                        VirMSSQL.TableAdapter.AbreTrasacaoSQL("Cadastros cModelos repositoryItemButtonEdit1_ButtonClick - 217", dModelos1.ModeloBalanceteDetalheTableAdapter, dModelos1.MBDxPLATableAdapter);
                        dModelos1.MBDxPLATableAdapter.Update(dModelos1.MBDxPLA);
                        dModelos1.MBDxPLA.AcceptChanges();
                        rowMBD.Delete();
                        dModelos1.ModeloBalanceteDetalheTableAdapter.Update(dModelos1.ModeloBalanceteDetalhe);
                        dModelos1.ModeloBalanceteDetalhe.AcceptChanges();                        
                        VirMSSQL.TableAdapter.CommitSQL();
                    }
                    catch (Exception ex)
                    {
                        VirMSSQL.TableAdapter.VircatchSQL(ex);
                        MessageBox.Show("Erro na gravação");
                        CarregaDados(true);
                    }
                    break;
                default:
                    break;
            }
        }       

        #region DragDrop

        /// <summary>
        /// Dados da posição de inicio no quadro direito
        /// </summary>
        private GridHitInfo downHitInfo = null;

        private GridHitInfo downHitInfoEsquerda = null;

        private GridHitInfo downHitInfoGrupo = null;

        private GridHitInfo downHitInfoDetalhe = null;

        private int OrigemGruporowhandle;

        private int OrigemDetrowhandle;        

        private GridView OrigemDetGrid;

        private string[] GetDragPLAs()
        {
            int[] selection = gridView2.GetSelectedRows();
            if (selection == null) 
                return null;
            int count = selection.Length;
            string[] result = new string[count];
            for (int i = 0; i < count; i++)
            {
                dModelos.PLAnaoconfiguradoRow rowPLAnao = (dModelos.PLAnaoconfiguradoRow)gridView2.GetDataRow(selection[i]);
                result[i] = rowPLAnao.PLA;
            }
            return result;
        }
        
        /// <summary>
        /// inicia drag a partir do grid direito - preparação
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void gridView2_MouseDown(object sender, MouseEventArgs e)
        {            
            GridView view = sender as GridView;
            downHitInfo = null;
            GridHitInfo hitInfo = view.CalcHitInfo(new Point(e.X, e.Y));
            if (Control.ModifierKeys != Keys.None)
                return;
            if (e.Button == MouseButtons.Left && hitInfo.InRow && hitInfo.RowHandle != GridControl.NewItemRowHandle)            
                downHitInfo = hitInfo;            
        }

        /// <summary>
        /// inicio efetifo do drag do quadrodireito
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void gridView2_MouseMove(object sender, MouseEventArgs e)
        {
            GridView view = sender as GridView;
            if ((e.Button == MouseButtons.Left) 
                && 
                (downHitInfo != null)
                &&
                (view.GetSelectedRows().Length > 0))
            {
                Size dragSize = SystemInformation.DragSize;
                Rectangle dragRect = new Rectangle(new Point(downHitInfo.HitPoint.X - dragSize.Width / 2,
                    downHitInfo.HitPoint.Y - dragSize.Height / 2), dragSize);

                if (!dragRect.Contains(new Point(e.X, e.Y)))
                {
                    view.GridControl.DoDragDrop(GetDragPLAs(), DragDropEffects.All);
                    downHitInfo = null;
                }
            }
        }

        /// <summary>
        /// Verifica se esta sobre um grid detalhe
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void gridControl1_DragOver(object sender, DragEventArgs e)
        {
            if (    (!e.Data.GetDataPresent(typeof(string[]))) 
                       && 
                    (!e.Data.GetDataPresent(typeof(dModelos.MBDxPLARow[]))) 
                       && 
                    (!e.Data.GetDataPresent(typeof(dModelos.ModeloBalanceteGrupoRow)))
                       &&
                    (!e.Data.GetDataPresent(typeof(dModelos.ModeloBalanceteDetalheRow)))
               )
            {
                e.Effect = DragDropEffects.None;
                return;
            }
            GridControl grid = sender as GridControl;
            Point pt = grid.PointToClient(new Point(e.X, e.Y));
            GridView view = (GridView)grid.GetViewAt(pt);
            if (view == null)
            {
                e.Effect = DragDropEffects.None;
                return;
            }
            GridHitInfo hitInfo = view.CalcHitInfo(grid.PointToClient(new Point(e.X, e.Y)));
            if ((e.Data.GetDataPresent(typeof(string[]))) || (e.Data.GetDataPresent(typeof(dModelos.MBDxPLARow[]))))
            {
                bool Novos = e.Data.GetDataPresent(typeof(string[]));
                if (view.Name == "gridView4")
                    e.Effect = DragDropEffects.None;                                                                                    
                else if (view.Name == "gridView3")
                    {
                        viewTarget = view;
                        if (hitInfo.HitTest == GridHitTest.EmptyRow)
                            DropTargetRowHandle = view.DataRowCount;
                        else
                            DropTargetRowHandle = hitInfo.RowHandle;
                        if (DropTargetRowHandle >= 0)
                            e.Effect = Novos ? DragDropEffects.Copy : DragDropEffects.Move;
                        else
                            e.Effect = DragDropEffects.None;
                    }
                    else
                    {
                        DropTargetRowHandle = -1;
                        if (hitInfo.InRow && hitInfo.RowHandle != GridControl.NewItemRowHandle && (hitInfo.RowHandle >= 0))
                            e.Effect = Novos ? DragDropEffects.Copy : DragDropEffects.Move;
                            //e.Effect = DragDropEffects.Move;
                        else
                            e.Effect = DragDropEffects.None;
                    }                
            }
            else if (e.Data.GetDataPresent(typeof(dModelos.ModeloBalanceteGrupoRow)))
            {
                viewTarget = view;
                if (view.Name != "gridView4")
                {
                    e.Effect = DragDropEffects.None;
                    DropTargetRowHandle = -1;
                }
                else
                {
                    if (hitInfo.HitTest == GridHitTest.EmptyRow)
                        DropTargetRowHandle = view.DataRowCount;
                    else
                        DropTargetRowHandle = hitInfo.RowHandle;

                    if ((OrigemGruporowhandle != hitInfo.RowHandle) // a propria linha
                            &&
                            (hitInfo.InRow || (hitInfo.HitTest == GridHitTest.EmptyRow)) // tem que estar dentro de uma linha
                            &&
                            (hitInfo.RowHandle != GridControl.NewItemRowHandle)  // linha de inclusão 
                        //&& 
                        //(hitInfo.RowHandle >= 0)  //linha viva
                        )
                        e.Effect = DragDropEffects.Move;
                    else
                    {
                        e.Effect = DragDropEffects.None;
                        DropTargetRowHandle = -1;
                    }
                }
            }
            else if (e.Data.GetDataPresent(typeof(dModelos.ModeloBalanceteDetalheRow)))
            {
                viewTarget = view;
                if (view.Name == "gridView4")
                {
                    DropTargetRowHandle = -1; // sem linha vermelha

                    if (    (OrigemGruporowhandle != hitInfo.RowHandle) // a propria linha
                            &&
                            (hitInfo.InRow) // tem que estar dentro de uma linha
                            &&
                            (hitInfo.RowHandle != GridControl.NewItemRowHandle)  // linha de inclusão 
                        //&& 
                        //(hitInfo.RowHandle >= 0)  //linha viva
                        )
                        e.Effect = DragDropEffects.Move;
                    else
                    {
                        e.Effect = DragDropEffects.None;
                        DropTargetRowHandle = -1;
                    }
                }
                else if (view.Name == "gridView1")
                {
                    e.Effect = DragDropEffects.None;

                    dModelos.ModeloBalanceteDetalheRow rowTarget = (dModelos.ModeloBalanceteDetalheRow)view.GetDataRow(hitInfo.RowHandle);
                    dModelos.ModeloBalanceteDetalheRow rowMovida = (dModelos.ModeloBalanceteDetalheRow)OrigemDetGrid.GetDataRow(OrigemDetrowhandle);

                    if (    ((OrigemDetrowhandle != hitInfo.RowHandle) || (view != OrigemDetGrid))// a propria linha
                            &&
                            (hitInfo.InRow) // tem que estar dentro de uma linha
                            &&
                            (hitInfo.RowHandle != GridControl.NewItemRowHandle)  // linha de inclusão 
                            && 
                            (rowTarget.MBDCred == rowMovida.MBDCred)  //linha viva
                        )
                    {
                        DropTargetRowHandle = hitInfo.RowHandle;
                        e.Effect = DragDropEffects.Move;
                    }
                    else
                    {
                        e.Effect = DragDropEffects.None;
                        DropTargetRowHandle = -1;
                    }
                    
                }
                else
                {
                    e.Effect = DragDropEffects.None;
                    DropTargetRowHandle = -1;
                }
            }
        }

        private void gridControl1_DragLeave(object sender, EventArgs e)
        {
            DropTargetRowHandle = -1;
        }

        private void MoveGrupo(int OrdemDestino, dModelos.ModeloBalanceteGrupoRow Movido)
        {
            if (Movido.MBGOrdem == OrdemDestino)
                return;
            else if (Movido.MBGOrdem > OrdemDestino)
            {
                foreach (dModelos.ModeloBalanceteGrupoRow rowRenumera in LinhaMae.GetModeloBalanceteGrupoRows())
                    if ((rowRenumera.MBGOrdem >= OrdemDestino) && (rowRenumera.MBGOrdem < Movido.MBGOrdem))
                        rowRenumera.MBGOrdem++;
                Movido.MBGOrdem = OrdemDestino;
            }
            else
            {
                foreach (dModelos.ModeloBalanceteGrupoRow rowRenumera in LinhaMae.GetModeloBalanceteGrupoRows())
                    if ((rowRenumera.MBGOrdem < OrdemDestino) && (rowRenumera.MBGOrdem > Movido.MBGOrdem))
                        rowRenumera.MBGOrdem--;
                Movido.MBGOrdem = OrdemDestino-1;
            }
        }

        private void gridControl1_DragDrop(object sender, DragEventArgs e)
        {
            Framework.objetosNeon.dAgrupadorBalancete.ResetSt();
            GridControl grid = sender as GridControl;
            Point pt = grid.PointToClient(new Point(e.X, e.Y));
            GridView view = (GridView)grid.GetViewAt(pt);
            GridHitInfo hitInfo = view.CalcHitInfo(grid.PointToClient(new Point(e.X, e.Y)));
            int TargetRowHandle = hitInfo.RowHandle;
            bool isMaster = true;
            if (view == null)
                return;
            if (e.Data.GetDataPresent(typeof(dModelos.ModeloBalanceteDetalheRow))) //Arrastando detalhe
            {
                dModelos.ModeloBalanceteDetalheRow rowMovida = (dModelos.ModeloBalanceteDetalheRow)e.Data.GetData(typeof(dModelos.ModeloBalanceteDetalheRow));
                if (view.Name == "gridView4")
                {
                    dModelos.ModeloBalanceteGrupoRow rowTargetGrupo = (dModelos.ModeloBalanceteGrupoRow)view.GetDataRow(TargetRowHandle);
                    int MaximaOrdem = -1;
                    foreach (dModelos.ModeloBalanceteDetalheRow rowi in rowTargetGrupo.GetModeloBalanceteDetalheRows())
                        if (rowi.MBDOrdem > MaximaOrdem)
                            MaximaOrdem = rowi.MBDOrdem;
                    rowMovida.MBDOrdem = MaximaOrdem + 1;
                    rowMovida.MBD_MBG = rowTargetGrupo.MBG;
                    dModelos1.ModeloBalanceteDetalheTableAdapter.Update(rowMovida);
                    rowMovida.AcceptChanges();
                    view.ExpandMasterRow(TargetRowHandle);
                }
                else if (view.Name == "gridView1")
                {
                    dModelos.ModeloBalanceteDetalheRow rowTargetDet = (dModelos.ModeloBalanceteDetalheRow)view.GetDataRow(TargetRowHandle);
                    int TargetOrdem = rowTargetDet.MBDOrdem;
                    DataRowView DRVMaster = (DataRowView)view.SourceRow;
                    foreach (dModelos.ModeloBalanceteDetalheRow rowi in ((dModelos.ModeloBalanceteGrupoRow)DRVMaster.Row).GetModeloBalanceteDetalheRows())
                        if (rowi.MBDOrdem >= TargetOrdem)
                            rowi.MBDOrdem = rowi.MBDOrdem+1;
                    rowMovida.MBDOrdem = TargetOrdem;
                    rowMovida.MBD_MBG = rowTargetDet.MBD_MBG;
                    dModelos1.ModeloBalanceteDetalheTableAdapter.Update(dModelos1.ModeloBalanceteDetalhe);
                    dModelos1.ModeloBalanceteDetalhe.AcceptChanges();
                }
            }
            else if (e.Data.GetDataPresent(typeof(dModelos.ModeloBalanceteGrupoRow))) //Arrastando grupo
            {
                dModelos.ModeloBalanceteGrupoRow rowMovida = (dModelos.ModeloBalanceteGrupoRow)e.Data.GetData(typeof(dModelos.ModeloBalanceteGrupoRow));
                if (TargetRowHandle >= 0)
                {
                    dModelos.ModeloBalanceteGrupoRow rowTargetGrupo = (dModelos.ModeloBalanceteGrupoRow)view.GetDataRow(TargetRowHandle);
                    MoveGrupo(rowTargetGrupo.MBGOrdem, rowMovida);
                    dModelos1.ModeloBalanceteGrupoTableAdapter.Update(dModelos1.ModeloBalanceteGrupo);
                    dModelos1.ModeloBalanceteGrupo.AcceptChanges();
                }
                else
                {
                    rowMovida.MBGOrdem = ProxOrdMBG();
                    dModelos1.ModeloBalanceteGrupoTableAdapter.Update(rowMovida);
                    dModelos1.ModeloBalanceteGrupo.AcceptChanges();
                }        
            }
            else // arrastando PLA
            {
                dModelos.ModeloBalanceteDetalheRow rowTarget;
                int NovasOrdens;
                if (e.Data.GetDataPresent(typeof(string[])))   // Nova
                {
                    string[] PLAs = e.Data.GetData(typeof(string[])) as string[];

                    if (view.Name == "gridView1")
                    {
                        rowTarget = (dModelos.ModeloBalanceteDetalheRow)view.GetDataRow(TargetRowHandle);
                        NovasOrdens = MairoOrdem(rowTarget) + 1;
                        isMaster = true;
                    }
                    else if (view.Name == "gridView3")
                    {
                        dModelos.MBDxPLARow rowTargetdet = (dModelos.MBDxPLARow)view.GetDataRow(TargetRowHandle);
                        rowTarget = rowTargetdet.ModeloBalanceteDetalheRow;
                        NovasOrdens = rowTargetdet.MBDxPLAOrdem;
                        SomaOrdem(rowTarget, NovasOrdens, PLAs.Length);
                        isMaster = false;
                    }
                    else
                        return;
                    foreach (string PLA in PLAs)
                    {
                        Framework.datasets.dPLAnocontas.PLAnocontasRow PLArow = Framework.datasets.dPLAnocontas.dPLAnocontasSt.PLAnocontas.FindByPLA(PLA);
                        dModelos.MBDxPLARow novaMDBxPLA = dModelos1.MBDxPLA.NewMBDxPLARow();
                        novaMDBxPLA.MBD = rowTarget.MBD;
                        novaMDBxPLA.MBDxPLADescricaoLivre = PLArow.PLADescricaoLivre;
                        novaMDBxPLA.MBDxPLAOrdem = NovasOrdens++;
                        novaMDBxPLA.PLA = PLA;
                        dModelos1.MBDxPLA.AddMBDxPLARow(novaMDBxPLA);
                        dModelos1.MBDxPLATableAdapter.Update(novaMDBxPLA);
                        novaMDBxPLA.AcceptChanges();
                        dModelos1.PLAnaoconfigurado.FindByPLA(PLA).Delete();
                    }

                }
                else if (e.Data.GetDataPresent(typeof(dModelos.MBDxPLARow[])))
                {
                    dModelos.MBDxPLARow[] rowsmover = e.Data.GetData(typeof(dModelos.MBDxPLARow[])) as dModelos.MBDxPLARow[];


                    if (view.Name == "gridView3")
                    {
                        isMaster = false;
                        dModelos.MBDxPLARow rowTargetdet = (dModelos.MBDxPLARow)view.GetDataRow(TargetRowHandle);
                        rowTarget = rowTargetdet.ModeloBalanceteDetalheRow;
                        NovasOrdens = rowTargetdet.MBDxPLAOrdem;
                        SomaOrdem(rowTarget, NovasOrdens, rowsmover.Length);
                    }
                    else
                    {
                        isMaster = true;
                        rowTarget = (dModelos.ModeloBalanceteDetalheRow)view.GetDataRow(TargetRowHandle);
                        NovasOrdens = MairoOrdem(rowTarget) + 1;                        
                    }

                    foreach (dModelos.MBDxPLARow rowmover in rowsmover)
                    {
                        rowmover.MBD = rowTarget.MBD;
                        rowmover.MBDxPLAOrdem = NovasOrdens++;
                        dModelos1.MBDxPLATableAdapter.Update(rowmover);
                        rowmover.AcceptChanges();
                    }
                }
                if (isMaster)
                    view.ExpandMasterRow(TargetRowHandle);
            }
            DropTargetRowHandle = -1;
        }

        private void gridControl2_DragDrop(object sender, DragEventArgs e)
        {
            Framework.objetosNeon.dAgrupadorBalancete.ResetSt();
            if (e.Data.GetDataPresent(typeof(dModelos.MBDxPLARow[])))
            {
                dModelos.MBDxPLARow[] rowsMover = e.Data.GetData(typeof(dModelos.MBDxPLARow[])) as dModelos.MBDxPLARow[];
                foreach (dModelos.MBDxPLARow rowMover in rowsMover)
                {
                    Framework.datasets.dPLAnocontas.PLAnocontasRow PLArow = Framework.datasets.dPLAnocontas.dPLAnocontasSt.PLAnocontas.FindByPLA(rowMover.PLA);
                    dModelos1.PLAnaoconfigurado.AddPLAnaoconfiguradoRow(rowMover.PLA, PLArow.PLADescricaoRes);
                    rowMover.Delete();
                }
                dModelos1.MBDxPLATableAdapter.Update(dModelos1.MBDxPLA);
                dModelos1.MBDxPLA.AcceptChanges();
            }
        }

        private void gridView3_MouseDown(object sender, MouseEventArgs e)
        {
            GridView view = sender as GridView;
            downHitInfoEsquerda = null;
            GridHitInfo hitInfo = view.CalcHitInfo(new Point(e.X, e.Y));
            if (Control.ModifierKeys != Keys.None)
                return;
            if (e.Button == MouseButtons.Left && hitInfo.InRow && hitInfo.RowHandle != GridControl.NewItemRowHandle)
                downHitInfoEsquerda = hitInfo;
        }

        private dModelos.MBDxPLARow[] GetDragrows(GridView view)
        {
            int[] selection = view.GetSelectedRows();
            if (selection == null)
                return null;
            int count = selection.Length;
            dModelos.MBDxPLARow[] result = new dModelos.MBDxPLARow[count];
            for (int i = 0; i < count; i++)
                result[i] = (dModelos.MBDxPLARow)view.GetDataRow(selection[i]);
            return result;
        }

        private void gridView3_MouseMove(object sender, MouseEventArgs e)
        {
            GridView view = sender as GridView;
            if ((e.Button == MouseButtons.Left)
                &&
                (downHitInfoEsquerda != null)
                &&
                (view.GetSelectedRows().Length > 0))
            {
                Size dragSize = SystemInformation.DragSize;
                Rectangle dragRect = new Rectangle(new Point(downHitInfoEsquerda.HitPoint.X - dragSize.Width / 2,
                    downHitInfoEsquerda.HitPoint.Y - dragSize.Height / 2), dragSize);

                if (!dragRect.Contains(new Point(e.X, e.Y)))
                {
                    view.GridControl.DoDragDrop(GetDragrows(view), DragDropEffects.All);
                    downHitInfoEsquerda = null;
                }
            }
        }

        private void gridControl2_DragOver(object sender, DragEventArgs e)
        {
            if (e.Data.GetDataPresent(typeof(dModelos.MBDxPLARow[])))
            {
                e.Effect = DragDropEffects.Move;
            }
        }

        private void gridView4_MouseMove(object sender, MouseEventArgs e)
        {
            GridView view = sender as GridView;
            dModelos.ModeloBalanceteGrupoRow rowMover = (dModelos.ModeloBalanceteGrupoRow)view.GetFocusedDataRow();
            if ((e.Button == MouseButtons.Left)
                &&
                (downHitInfoGrupo != null)
                &&
                (rowMover != null))
            {
                Size dragSize = SystemInformation.DragSize;
                Rectangle dragRect = new Rectangle(new Point(downHitInfoGrupo.HitPoint.X - dragSize.Width / 2,
                    downHitInfoGrupo.HitPoint.Y - dragSize.Height / 2), dragSize);

                if (!dragRect.Contains(new Point(e.X, e.Y)))
                {
                    OrigemGruporowhandle = view.FocusedRowHandle;
                    view.GridControl.DoDragDrop(rowMover, DragDropEffects.All);
                    downHitInfoGrupo = null;
                }
            }
        }

        private void gridView4_MouseDown(object sender, MouseEventArgs e)
        {
            GridView view = sender as GridView;
            downHitInfoGrupo = null;
            GridHitInfo hitInfo = view.CalcHitInfo(new Point(e.X, e.Y));
            if (Control.ModifierKeys != Keys.None)
                return;
            if (e.Button == MouseButtons.Left && hitInfo.InRow && hitInfo.RowHandle != GridControl.NewItemRowHandle)
                downHitInfoGrupo = hitInfo;
        }

        private void gridView1_MouseDown(object sender, MouseEventArgs e)
        {
            GridView view = sender as GridView;
            downHitInfoDetalhe = null;
            GridHitInfo hitInfo = view.CalcHitInfo(new Point(e.X, e.Y));
            if (Control.ModifierKeys != Keys.None)
                return;
            if (e.Button == MouseButtons.Left && hitInfo.InRow && hitInfo.RowHandle != GridControl.NewItemRowHandle)
                downHitInfoDetalhe = hitInfo;
        }

        private void gridView1_MouseMove(object sender, MouseEventArgs e)
        {
            GridView view = sender as GridView;
            dModelos.ModeloBalanceteDetalheRow rowMover = (dModelos.ModeloBalanceteDetalheRow)view.GetFocusedDataRow();
            if ((e.Button == MouseButtons.Left)
                &&
                (downHitInfoDetalhe != null)
                &&
                (rowMover != null))
            {
                Size dragSize = SystemInformation.DragSize;
                Rectangle dragRect = new Rectangle(new Point(downHitInfoDetalhe.HitPoint.X - dragSize.Width / 2,
                    downHitInfoDetalhe.HitPoint.Y - dragSize.Height / 2), dragSize);

                if (!dragRect.Contains(new Point(e.X, e.Y)))
                {
                    OrigemDetGrid = view;
                    OrigemDetrowhandle = view.FocusedRowHandle;
                    OrigemGruporowhandle = view.SourceRowHandle;
                    view.GridControl.DoDragDrop(rowMover, DragDropEffects.All);
                    downHitInfoDetalhe = null;
                }
            }
        }       

        #endregion

        #region Linha vermelha
        private int dropTargetRowHandle = -1;
        private GridView viewTarget;

        private int DropTargetRowHandle
        {
            get { return dropTargetRowHandle; }
            set
            {
                dropTargetRowHandle = value;
                if (value == -1)
                    viewTarget = null;
                gridControl1.Invalidate();
            }
        }

        private void gridControl1_Paint(object sender, PaintEventArgs e)
        {
            if (DropTargetRowHandle < 0)
                return;
            GridControl grid = (GridControl)sender;

            bool isBottomLine = (DropTargetRowHandle == viewTarget.DataRowCount);

            GridViewInfo viewInfo = viewTarget.GetViewInfo() as GridViewInfo;
            GridRowInfo rowInfo = viewInfo.GetGridRowInfo(isBottomLine ? DropTargetRowHandle - 1 : DropTargetRowHandle);

            if (rowInfo == null) return;

            Point p1, p2;
            if (isBottomLine)
            {
                //p1 = new Point(rowInfo.Bounds.Left, rowInfo.Bounds.Bottom - 1);
                //p2 = new Point(rowInfo.Bounds.Right, rowInfo.Bounds.Bottom - 1);
                p1 = new Point(rowInfo.Bounds.Left, rowInfo.TotalBounds.Bottom - 1);
                p2 = new Point(rowInfo.Bounds.Right, rowInfo.TotalBounds.Bottom - 1);
            }
            else
            {
                p1 = new Point(rowInfo.Bounds.Left, rowInfo.Bounds.Top - 1);
                p2 = new Point(rowInfo.Bounds.Right, rowInfo.Bounds.Top - 1);
            }
            e.Graphics.DrawLine(Pens.Red, p1, p2);
        } 

        #endregion

        #region Ordem
        private int MairoOrdem(dModelos.ModeloBalanceteDetalheRow MBDrow)
        {
            int Maior = 0;
            foreach (dModelos.MBDxPLARow MBDxPLARow in MBDrow.GetMBDxPLARows())
                if (MBDxPLARow.MBDxPLAOrdem > Maior)
                    Maior = MBDxPLARow.MBDxPLAOrdem;
            return Maior;
        }

        private void SomaOrdem(dModelos.ModeloBalanceteDetalheRow MBDrow, int piso, int intervalo)
        {
            Framework.objetosNeon.dAgrupadorBalancete.ResetSt();
            foreach (dModelos.MBDxPLARow MBDxPLARow in MBDrow.GetMBDxPLARows())
                if (MBDxPLARow.MBDxPLAOrdem >= piso)
                {
                    MBDxPLARow.MBDxPLAOrdem = MBDxPLARow.MBDxPLAOrdem + intervalo;
                    dModelos1.MBDxPLATableAdapter.Update(MBDxPLARow);
                    MBDxPLARow.AcceptChanges();
                }
            //foreach (dModelos.MBDxPLARow MBDxPLARow in Superiores)

        }

        /*
        private void move(int indiceAtual, sentido s)
        {
            int indiceNovo = s == sentido.sobe ? indiceAtual - 1 : indiceAtual + 1;
            //GridView view = gridView1;
            //view.GridControl.Focus();
            //int index = view.FocusedRowHandle;
            //if (index <= 0)
            //    return;

            //ClearSorting(view);
            dModelos.ModeloBalanceteDetalheRow row1 = (dModelos.ModeloBalanceteDetalheRow)gridView1.GetDataRow(indiceAtual);
            dModelos.ModeloBalanceteDetalheRow row2 = (dModelos.ModeloBalanceteDetalheRow)gridView1.GetDataRow(indiceNovo);
            if ((row1 == null) || (row2 == null))
                return;
            int Ordem1 = row1.MBDOrdem;
            int Ordem2 = row2.MBDOrdem;
            if (Ordem1 == Ordem2)
            {
                if (s == sentido.sobe)
                    Ordem1--;
                else
                    Ordem1++;
            }
            row1.MBDOrdem = Ordem2;
            row2.MBDOrdem = Ordem1;
            dModelos1.ModeloBalanceteDetalheTableAdapter.Update(dModelos1.ModeloBalanceteDetalhe);
            dModelos1.ModeloBalanceteDetalhe.AcceptChanges();
            gridView1.FocusedRowHandle = indiceNovo;
        }
        */ 
        #endregion

        private void gridView3_RowUpdated(object sender, DevExpress.XtraGrid.Views.Base.RowObjectEventArgs e)
        {
            DataRowView DRV = (DataRowView)e.Row;
            if ((DRV == null) || (DRV.Row == null))
                return;
            Framework.objetosNeon.dAgrupadorBalancete.ResetSt();
            dModelos.MBDxPLARow row = (dModelos.MBDxPLARow)DRV.Row;
            if ((row.RowState == DataRowState.Modified) || (row.RowState == DataRowState.Added))
            {
                if (!row.IsMBDxPLADescricaoNull())
                {
                    string Manobra = CompontesBasicos.ObjetosEstaticos.StringEdit.Limpa(row.MBDxPLADescricao, CompontesBasicos.ObjetosEstaticos.StringEdit.TiposLimpesa.PrimeiroCaracMaiusculo);
                    if (row.MBDxPLADescricao != Manobra)
                        row.MBDxPLADescricao = Manobra;
                }
                dModelos1.MBDxPLATableAdapter.Update(row);
                row.AcceptChanges();
            }
        }        

        private int? _MaiorOrdemMBG;

        private int MaiorOrdemMBG
        {
            get 
            {
                if (!_MaiorOrdemMBG.HasValue)
                {
                    _MaiorOrdemMBG = 0;
                    foreach (dModelos.ModeloBalanceteGrupoRow rowi in LinhaMae.GetModeloBalanceteGrupoRows())
                        if (rowi.MBGOrdem > _MaiorOrdemMBG)
                            _MaiorOrdemMBG = rowi.MBGOrdem;
                };
                return _MaiorOrdemMBG.Value;
            }
            set { _MaiorOrdemMBG = value; }
        }

        private int ProxOrdMBG()
        {
            MaiorOrdemMBG = MaiorOrdemMBG +1;
            return MaiorOrdemMBG;
        }

        private void gridView4_InitNewRow(object sender, InitNewRowEventArgs e)
        {
            if(LinhaMae == null)
                throw new Exception("Nenhum modelo cadastrado");
            GridView view = (GridView)sender;
            dModelos.ModeloBalanceteGrupoRow row = (dModelos.ModeloBalanceteGrupoRow)view.GetDataRow(e.RowHandle);
            if (row != null)
            {
                row.MBG_MBA = LinhaMae.MBA;                                                
                row.MBGOrdem = ProxOrdMBG();
                row.MBGTitulo = "";                                
            }
        }
        
        private void gridView4_RowUpdated(object sender, DevExpress.XtraGrid.Views.Base.RowObjectEventArgs e)
        {
            DataRowView DRV = (DataRowView)e.Row;
            if ((DRV == null) || (DRV.Row == null))
                return;
            Framework.objetosNeon.dAgrupadorBalancete.ResetSt();
            dModelos.ModeloBalanceteGrupoRow row = (dModelos.ModeloBalanceteGrupoRow)DRV.Row;                         
            if ((row.RowState == DataRowState.Modified) || (row.RowState == DataRowState.Added))
            {
                string Manobra = CompontesBasicos.ObjetosEstaticos.StringEdit.Limpa(row.MBGTitulo, CompontesBasicos.ObjetosEstaticos.StringEdit.TiposLimpesa.PrimeiroCaracMaiusculo);
                if (row.MBGTitulo != Manobra)
                    row.MBGTitulo = Manobra;
                dModelos1.ModeloBalanceteGrupoTableAdapter.Update(row);
                row.AcceptChanges();
            }
        }

        private void repositoryDelGrupo_ButtonClick(object sender, DevExpress.XtraEditors.Controls.ButtonPressedEventArgs e)
        {
            dModelos.ModeloBalanceteGrupoRow row = (dModelos.ModeloBalanceteGrupoRow)gridView4.GetFocusedDataRow();
            if (row == null)
                return;
            Framework.objetosNeon.dAgrupadorBalancete.ResetSt();
            if (row.GetModeloBalanceteDetalheRows().Length > 0)
            {
                MessageBox.Show("Para esta operação o grupo deve estar vazio");
                return;
            }
            row.Delete();
            dModelos1.ModeloBalanceteGrupoTableAdapter.Update(row);
            dModelos1.ModeloBalanceteGrupo.AcceptChanges();

        }
        
        private void checkEdit1_CheckedChanged(object sender, EventArgs e)
        {
            expandirGrupos(checkEdit1.Checked);
        }

        private void repDELPLA_ButtonClick(object sender, DevExpress.XtraEditors.Controls.ButtonPressedEventArgs e)
        {
            Framework.objetosNeon.dAgrupadorBalancete.ResetSt();
            GridView view = (GridView)gridControl1.FocusedView;
            dModelos.MBDxPLARow rowDEL = (dModelos.MBDxPLARow)view.GetFocusedDataRow();
            Framework.datasets.dPLAnocontas.PLAnocontasRow PLArow = Framework.datasets.dPLAnocontas.dPLAnocontasSt.PLAnocontas.FindByPLA(rowDEL.PLA);
            dModelos1.PLAnaoconfigurado.AddPLAnaoconfiguradoRow(rowDEL.PLA, PLArow.PLADescricaoRes);
            rowDEL.Delete();                
            dModelos1.MBDxPLATableAdapter.Update(dModelos1.MBDxPLA);
            dModelos1.MBDxPLA.AcceptChanges();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            //Competencia compBalancete = new Competencia();
            AbstratosNeon.ABS_balancete balancete = AbstratosNeon.ABS_balancete.GetBalancete(lookupCondominio_F1.CON_sel, cCompet1.Comp, TipoBalancete.Simulador);
            
            balancete.CarregarDados(true);
            balancete.CarregarDadosBalancete(new AgrupadorBalancete.solicitaAgrupamento(AgrupadorBalancete.TipoClassificacaoDesp.CadastroMBA, LinhaMae.MBA));

            if (!balancete.simulaBoleto(null,true,false))
                MessageBox.Show(string.Format("Balancete não disponível ({0})", cCompet1.Comp));
            //AbstratosNeon.ABS_solicitaAgrupamento Agrup = new AbstratosNeon.ABS_solicitaAgrupamento(
            //balancete.CarregarDadosBalancete(AbstratosNeon.ABS_balancete.TipoBalancete.Boleto, false);
            //balancete1.simulaBoleto(null, true, false);



            /*
            //if (!this.Validate(true))
            //    return;
            //previsaoBOletosBindingSource.EndEdit();
            if (linhamae.RowState != DataRowState.Unchanged)
            {
                dPrevBolCampos.PrevisaoBOletosTableAdapter.Update(linhamae);
                linhamae.AcceptChanges();
            };
            int? CONBalBoleto = VirMSSQL.TableAdapter.STTableAdapter.BuscaEscalar_int("select CONBalBoleto from condominios where con = @P1", linhamae.PBO_CON);
            if (CONBalBoleto.HasValue)
            {
                Competencia compBol = new Competencia(linhamae.PBOCompetencia);
                Competencia compBalancete = compBol.CloneCompet(CONBalBoleto.Value);
                AbstratosNeon.ABS_balancete balancete = AbstratosNeon.ABS_balancete.GetBalancete(linhamae.PBO_CON, compBalancete, AbstratosNeon.ABS_balancete.TipoBalancete.Boleto);
                if (!balancete.simulaBoleto(linhamae.PBO))
                    MessageBox.Show(string.Format("Balancete não disponível ({0})", compBalancete));
            }
            */
        }

        private void lookupCondominio_F1_alterado(object sender, EventArgs e)
        {
            AtivarEmulador();
        }

        private void AtivarEmulador()
        {
            if (lookupCondominio_F1.CON_sel > 0)
                Bemulador.Enabled = true;
        }

        
                     
    }
}
