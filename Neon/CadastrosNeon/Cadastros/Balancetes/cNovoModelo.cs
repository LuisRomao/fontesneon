﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Framework.objetosNeon;

namespace Cadastros.Balancetes
{
    /// <summary>
    /// Criar novo modelo
    /// </summary>
    public partial class cNovoModelo : CompontesBasicos.ComponenteBaseDialog
    {
        /// <summary>
        /// Tipo de agrupamento
        /// </summary>
        public AgrupadorBalancete.TipoClassificacaoDesp retTipo;
        /// <summary>
        /// Retorno. Modelo escolhido. -1 = padrão -2 = boleto e positivo o MBA do modelo
        /// </summary>
        public int? retModeloBaseEscolhido;
        /// <summary>
        /// Retorno. Nome
        /// </summary>
        public string retNome;

        /// <summary>
        /// Construtor padrão - não usar.
        /// </summary>
        public cNovoModelo()
        {
            InitializeComponent();            
        }

        private AgrupadorBalancete.TipoClassificacaoDesp TipoSelecionado
        {
            get 
            {
                return (AgrupadorBalancete.TipoClassificacaoDesp)radioGroup1.EditValue;
            }
        }

        /// <summary>
        /// Construtor
        /// </summary>
        public cNovoModelo(dModelos _dModelos)
        {
            InitializeComponent();
            dModelosBindingSource.DataSource = _dModelos;
            radioGroup1.Properties.Items.Clear();
            radioGroup1.Properties.Items.Add(new DevExpress.XtraEditors.Controls.RadioGroupItem(AgrupadorBalancete.TipoClassificacaoDesp.PlanoContas,"Balancete Padrão"));
            radioGroup1.Properties.Items.Add(new DevExpress.XtraEditors.Controls.RadioGroupItem(AgrupadorBalancete.TipoClassificacaoDesp.Grupos, "Boleto Padrão"));
            radioGroup1.Properties.Items.Add(new DevExpress.XtraEditors.Controls.RadioGroupItem(AgrupadorBalancete.TipoClassificacaoDesp.CadastroMBA, "Outro Modelo"));
            radioGroup1.Properties.Items.Add(new DevExpress.XtraEditors.Controls.RadioGroupItem(AgrupadorBalancete.TipoClassificacaoDesp.EmBanco, "Em Branco"));
            radioGroup1.EditValue = AgrupadorBalancete.TipoClassificacaoDesp.PlanoContas;
        }

        private void radioGroup1_SelectedIndexChanged(object sender, EventArgs e)
        {
            LoockOutroModelo.Visible = (TipoSelecionado == AgrupadorBalancete.TipoClassificacaoDesp.CadastroMBA);
        }

        /// <summary>
        /// Verifica se pode fechar e ajusta o retonrno: ModeloBaseEscolhido 
        /// </summary>
        /// <returns></returns>
        public override bool CanClose()
        {
            if (textEditNome.Text == "")
                return false;
            else
            {
                retNome = textEditNome.Text;
                if (TipoSelecionado == AgrupadorBalancete.TipoClassificacaoDesp.CadastroMBA)
                {
                    if (LoockOutroModelo.EditValue == null)
                        return false;
                    retTipo = AgrupadorBalancete.TipoClassificacaoDesp.CadastroMBA;
                    retModeloBaseEscolhido = (int)LoockOutroModelo.EditValue;
                }
                else
                    retTipo = TipoSelecionado;
                return base.CanClose();
            }
        }
    }
}
