using System;
using System.Data;
using System.Windows.Forms;

namespace Cadastros.Cargos
{
    /// <summary>
    /// 
    /// </summary>
    public partial class cCargosGrade : CompontesBasicos.ComponenteGradeNavegador
    {
        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        protected override bool ConfirmarRegistro_F()
        {
            try
            {
                this.Validate();
                BindingSource_F.EndEdit();
                GridView_F.UpdateCurrentRow();
                cARGOSTableAdapter.Update(dCargosGrade.CARGOS);
                dCargosGrade.AcceptChanges();

                return true;
            }
            catch (Exception Erro)
            {
                if (Erro is NoNullAllowedException)
                    MessageBox.Show("Existe campo obrig�torio sem um valor informado.", "Campo Obrigat�rio", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                else
                    if ((Erro is ConstraintException) && (Erro.Message.ToUpper().IndexOf("UNIQUE") >= 0))
                        MessageBox.Show("Existe campo de valor �nico com valor duplicado.", "Campo de Valor �nico", MessageBoxButtons.OK, MessageBoxIcon.Warning);

                    else
                        MessageBox.Show("Ocorreu uma excess�o desconhecida ao tentar efetuar a opera��o:\r\n\r\n" + Erro.Message.ToUpper() + "\r\n\r\n" +
                                        "Entre em contato com o suporte da Virtual Software e informe sobre o problema.", "Excess�o Desconhecida", MessageBoxButtons.OK, MessageBoxIcon.Error);

                return false;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        protected override void ExcluirRegistro_F()
        {
            Int32 _PK = Convert.ToInt32(((DataRowView)(BindingSource_F.Current))["CGO"]);
            cARGOSTableAdapter.DeleteQuery(_PK);
        }

        private void TrocaPosicao(bool MoverParaCima)
        {
            try
            {
                //GridView_F.BeginDataUpdate();

                DataRowView _FocusedRow = ((DataRowView)(GridView_F.GetRow(GridView_F.FocusedRowHandle)));
                DataRowView _ChangedRow = null;
                Int32 _FocusedHandle = 0;

                if (MoverParaCima)
                {
                    if (!GridView_F.IsFirstRow)
                    {
                        GridView_F.FocusedRowHandle--;
                        _ChangedRow = ((DataRowView)(GridView_F.GetRow(GridView_F.FocusedRowHandle)));
                        
                        _FocusedHandle = GridView_F.FocusedRowHandle;
                    }
                }
                else
                {
                    if (!GridView_F.IsLastRow)
                    {
                        GridView_F.FocusedRowHandle++;
                        _ChangedRow = ((DataRowView)(GridView_F.GetRow(GridView_F.FocusedRowHandle)));
                        _FocusedHandle = GridView_F.FocusedRowHandle;
                    }
                }

                //Trocando Posicao
                if (_ChangedRow != null)
                {
                    Object _Obj = _FocusedRow["CGOPosicao"];

                    _FocusedRow["CGOPosicao"] = _ChangedRow["CGOPosicao"];
                    _FocusedRow.EndEdit();

                    _ChangedRow["CGOPosicao"] = _Obj;
                    _ChangedRow.EndEdit();
                }

                GridView_F.FocusedRowHandle = _FocusedHandle;

                cARGOSTableAdapter.Update(dCargosGrade.CARGOS);

                dCargosGrade.AcceptChanges();

                //GridView_F.EndDataUpdate();
            }
            catch (Exception Erro)
            {
                MessageBox.Show("Ocorreu um erro ao tentar trocar as posi��es."
                                + "\r\n\r\n" + Erro.Message.ToUpper()
                                + "\r\n\r\n" + "A troca de posi��o n�o foi efetuada.",
                                "Trocar de Posi��o",
                                MessageBoxButtons.OK, MessageBoxIcon.Error);

                GridView_F.CancelUpdateCurrentRow();

                GridView_F.HideEditor();
            }
        }
        
        /// <summary>
        /// 
        /// </summary>
        public cCargosGrade()
        {
            InitializeComponent();
        }

        private void btnMoveUp_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            TrocaPosicao(true);
        }

        private void btnMoveDown_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            TrocaPosicao(false);
        }

        private void cCargosGrade_Load(object sender, EventArgs e)
        {
            //cARGOSTableAdapter.Fill(dCargosGrade.CARGOS);
        }

        private void GridControl_F_Click(object sender, EventArgs e)
        {

        }
    }
}

