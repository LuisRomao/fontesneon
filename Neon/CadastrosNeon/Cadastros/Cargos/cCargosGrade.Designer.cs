namespace Cadastros.Cargos
{
    partial class cCargosGrade
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(cCargosGrade));
            this.dCargosGrade = new Cadastros.Cargos.dCargosGrade();
            this.colCGONome = new DevExpress.XtraGrid.Columns.GridColumn();
            this.cARGOSTableAdapter = new Cadastros.Cargos.dCargosGradeTableAdapters.CARGOSTableAdapter();
            this.btnMoveUp = new DevExpress.XtraBars.BarButtonItem();
            this.btnMoveDown = new DevExpress.XtraBars.BarButtonItem();
            this.colCGOPosicao = new DevExpress.XtraGrid.Columns.GridColumn();
            ((System.ComponentModel.ISupportInitialize)(this.PopImprimir_F)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.BarManager_F)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.PopExportar_F)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.GridControl_F)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.GridView_F)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.BindingSource_F)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.StyleController_F)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dCargosGrade)).BeginInit();
            this.SuspendLayout();
            // 
            // barNavegacao_F
            // 
            this.barNavegacao_F.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(this.btnMoveUp, true),
            new DevExpress.XtraBars.LinkPersistInfo(this.btnMoveDown)});
            this.barNavegacao_F.OptionsBar.AllowQuickCustomization = false;
            this.barNavegacao_F.OptionsBar.DisableClose = true;
            this.barNavegacao_F.OptionsBar.DisableCustomization = true;
            this.barNavegacao_F.OptionsBar.DrawDragBorder = false;
            this.barNavegacao_F.OptionsBar.DrawSizeGrip = true;
            this.barNavegacao_F.OptionsBar.RotateWhenVertical = false;
            this.barNavegacao_F.OptionsBar.UseWholeRow = true;
            // 
            // BarManager_F
            // 
            this.BarManager_F.Items.AddRange(new DevExpress.XtraBars.BarItem[] {
            this.btnMoveUp,
            this.btnMoveDown});
            this.BarManager_F.MaxItemId = 20;
            // 
            // BtnAlterar_F
            // 
            this.BtnAlterar_F.Enabled = false;
            // 
            // BtnExcluir_F
            // 
            this.BtnExcluir_F.Enabled = false;
            // 
            // BtnImprimir_F
            // 
            this.BtnImprimir_F.Enabled = false;
            // 
            // BtnExportar_F
            // 
            this.BtnExportar_F.Enabled = false;
            // 
            // barDataBase_F
            // 
            this.barDataBase_F.OptionsBar.AllowQuickCustomization = false;
            this.barDataBase_F.OptionsBar.DisableClose = true;
            this.barDataBase_F.OptionsBar.DisableCustomization = true;
            this.barDataBase_F.OptionsBar.DrawDragBorder = false;
            this.barDataBase_F.OptionsBar.UseWholeRow = true;
            // 
            // GridControl_F
            // 
            this.GridControl_F.EmbeddedNavigator.Buttons.Append.Visible = false;
            this.GridControl_F.EmbeddedNavigator.Buttons.CancelEdit.Visible = false;
            this.GridControl_F.EmbeddedNavigator.Buttons.Edit.Visible = false;
            this.GridControl_F.EmbeddedNavigator.Buttons.EndEdit.Visible = false;
            this.GridControl_F.EmbeddedNavigator.Buttons.Remove.Visible = false;
            this.GridControl_F.EmbeddedNavigator.Name = "";
            this.GridControl_F.Size = new System.Drawing.Size(1004, 486);
            this.GridControl_F.Click += new System.EventHandler(this.GridControl_F_Click);
            // 
            // GridView_F
            // 
            this.GridView_F.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colCGONome,
            this.colCGOPosicao});
            this.GridView_F.OptionsBehavior.AllowIncrementalSearch = true;
            this.GridView_F.OptionsBehavior.Editable = false;
            this.GridView_F.OptionsCustomization.AllowFilter = false;
            this.GridView_F.OptionsCustomization.AllowGroup = false;
            this.GridView_F.OptionsCustomization.AllowSort = false;
            this.GridView_F.OptionsLayout.Columns.StoreAllOptions = true;
            this.GridView_F.OptionsLayout.Columns.StoreAppearance = true;
            this.GridView_F.OptionsLayout.StoreAllOptions = true;
            this.GridView_F.OptionsLayout.StoreAppearance = true;
            this.GridView_F.OptionsMenu.EnableGroupPanelMenu = false;
            this.GridView_F.OptionsNavigation.EnterMoveNextColumn = true;
            this.GridView_F.OptionsPrint.EnableAppearanceEvenRow = true;
            this.GridView_F.OptionsPrint.EnableAppearanceOddRow = true;
            this.GridView_F.OptionsPrint.ExpandAllDetails = true;
            this.GridView_F.OptionsSelection.InvertSelection = true;
            this.GridView_F.OptionsView.ShowGroupPanel = false;
            this.GridView_F.SortInfo.AddRange(new DevExpress.XtraGrid.Columns.GridColumnSortInfo[] {
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colCGOPosicao, DevExpress.Data.ColumnSortOrder.Ascending)});
            // 
            // BindingSource_F
            // 
            this.BindingSource_F.DataMember = "CARGOS";
            this.BindingSource_F.DataSource = this.dCargosGrade;
            // 
            // ToolTipController_F
            // 
            this.ToolTipController_F.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(222)))), ((int)(((byte)(237)))), ((int)(((byte)(250)))));
            this.ToolTipController_F.Appearance.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(20)))), ((int)(((byte)(69)))), ((int)(((byte)(141)))));
            this.ToolTipController_F.Appearance.Options.UseBackColor = true;
            this.ToolTipController_F.Appearance.Options.UseFont = true;
            this.ToolTipController_F.Appearance.Options.UseForeColor = true;
            this.ToolTipController_F.Appearance.Options.UseTextOptions = true;
            this.ToolTipController_F.Appearance.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            // 
            // StyleController_F
            // 
            this.StyleController_F.AppearanceDisabled.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this.StyleController_F.AppearanceDisabled.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this.StyleController_F.AppearanceDisabled.Options.UseBackColor = true;
            this.StyleController_F.AppearanceDisabled.Options.UseForeColor = true;
            this.StyleController_F.AppearanceFocused.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.StyleController_F.AppearanceFocused.Options.UseBackColor = true;
            
            // 
            // dCargosGrade
            // 
            this.dCargosGrade.DataSetName = "dCargosGrade";
            this.dCargosGrade.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // colCGONome
            // 
            this.colCGONome.Caption = "Nome";
            this.colCGONome.FieldName = "CGONome";
            this.colCGONome.Name = "colCGONome";
            this.colCGONome.Visible = true;
            this.colCGONome.VisibleIndex = 0;
            this.colCGONome.Width = 661;
            // 
            // cARGOSTableAdapter
            // 
            this.cARGOSTableAdapter.ClearBeforeFill = true;
            // 
            // btnMoveUp
            // 
            this.btnMoveUp.Caption = "Mover para Cima";
            this.btnMoveUp.CategoryGuid = new System.Guid("70b162a1-0e6a-4dea-87b4-fc5b71c53e05");
            this.btnMoveUp.Glyph = ((System.Drawing.Image)(resources.GetObject("btnMoveUp.Glyph")));
            this.btnMoveUp.Hint = "Mover para cima.";
            this.btnMoveUp.Id = 18;
            this.btnMoveUp.Name = "btnMoveUp";
            this.btnMoveUp.PaintStyle = DevExpress.XtraBars.BarItemPaintStyle.CaptionInMenu;
            this.btnMoveUp.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.btnMoveUp_ItemClick);
            // 
            // btnMoveDown
            // 
            this.btnMoveDown.Caption = "Mover para Baixo";
            this.btnMoveDown.CategoryGuid = new System.Guid("70b162a1-0e6a-4dea-87b4-fc5b71c53e05");
            this.btnMoveDown.Glyph = ((System.Drawing.Image)(resources.GetObject("btnMoveDown.Glyph")));
            this.btnMoveDown.Hint = "Mover para baixo.";
            this.btnMoveDown.Id = 19;
            this.btnMoveDown.Name = "btnMoveDown";
            this.btnMoveDown.PaintStyle = DevExpress.XtraBars.BarItemPaintStyle.CaptionInMenu;
            this.btnMoveDown.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.btnMoveDown_ItemClick);
            // 
            // colCGOPosicao
            // 
            this.colCGOPosicao.Caption = "colCGOPosicao";
            this.colCGOPosicao.FieldName = "CGOPosicao";
            this.colCGOPosicao.Name = "colCGOPosicao";
            this.colCGOPosicao.OptionsColumn.ShowInCustomizationForm = false;
            // 
            // cCargosGrade
            // 
            this.LookAndFeel.UseDefaultLookAndFeel = false;
            this.Name = "cCargosGrade";
            this.Size = new System.Drawing.Size(1004, 541);
            this.ToolTipController_F.SetSuperTip(this, null);
            this.Load += new System.EventHandler(this.cCargosGrade_Load);
            ((System.ComponentModel.ISupportInitialize)(this.PopImprimir_F)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.BarManager_F)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.PopExportar_F)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.GridControl_F)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.GridView_F)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.BindingSource_F)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.StyleController_F)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dCargosGrade)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private dCargosGrade dCargosGrade;
        private DevExpress.XtraGrid.Columns.GridColumn colCGONome;
        private Cadastros.Cargos.dCargosGradeTableAdapters.CARGOSTableAdapter cARGOSTableAdapter;
        private DevExpress.XtraBars.BarButtonItem btnMoveUp;
        private DevExpress.XtraBars.BarButtonItem btnMoveDown;
        private DevExpress.XtraGrid.Columns.GridColumn colCGOPosicao;
    }
}
