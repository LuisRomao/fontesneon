namespace Cadastros.Condominios
{
    partial class PesquisaCondominios
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(PesquisaCondominios));
            this.groupControl1 = new DevExpress.XtraEditors.GroupControl();
            this.lookupCondominio_F1 = new Framework.Lookup.LookupCondominio_F();
            this.cCondominiosGrade1 = new Cadastros.Condominios.cCondominiosGrade();
            ((System.ComponentModel.ISupportInitialize)(this.StyleController_F)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl1)).BeginInit();
            this.groupControl1.SuspendLayout();
            this.SuspendLayout();
            // 
            // ToolTipController_F
            // 
            this.ToolTipController_F.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.ToolTipController_F.Appearance.ForeColor = System.Drawing.Color.Black;
            this.ToolTipController_F.Appearance.Options.UseBackColor = true;
            this.ToolTipController_F.Appearance.Options.UseFont = true;
            this.ToolTipController_F.Appearance.Options.UseForeColor = true;
            this.ToolTipController_F.Appearance.Options.UseTextOptions = true;
            this.ToolTipController_F.Appearance.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            // 
            // StyleController_F
            // 
            this.StyleController_F.AppearanceFocused.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.StyleController_F.AppearanceFocused.Options.UseBackColor = true;
            
            // 
            // groupControl1
            // 
            this.groupControl1.Controls.Add(this.lookupCondominio_F1);
            this.groupControl1.Dock = System.Windows.Forms.DockStyle.Top;
            this.groupControl1.Location = new System.Drawing.Point(0, 0);
            this.groupControl1.Name = "groupControl1";
            this.groupControl1.Size = new System.Drawing.Size(732, 43);
            this.groupControl1.TabIndex = 0;
            this.groupControl1.Text = "Identificação ::";
            // 
            // lookupCondominio_F1
            // 
            this.lookupCondominio_F1.Autofill = true;
            this.lookupCondominio_F1.Cursor = System.Windows.Forms.Cursors.Default;
            this.lookupCondominio_F1.Dock = System.Windows.Forms.DockStyle.Top;
            this.lookupCondominio_F1.Estado = CompontesBasicos.EstadosDosComponentes.Registrado;
           
            this.lookupCondominio_F1.Location = new System.Drawing.Point(2, 20);
            this.lookupCondominio_F1.LookAndFeel.UseDefaultLookAndFeel = false;
            this.lookupCondominio_F1.Name = "lookupCondominio_F1";
            this.lookupCondominio_F1.Size = new System.Drawing.Size(728, 20);
            this.lookupCondominio_F1.TabIndex = 0;
            this.lookupCondominio_F1.Titulo = null;
            this.lookupCondominio_F1.alterado += new System.EventHandler(this.lookupCondominio_F1_alterado);
            // 
            // cCondominiosGrade1
            // 
            this.cCondominiosGrade1.Autofill = false;
            this.cCondominiosGrade1.BindingSourcePrincipal = null;
            this.cCondominiosGrade1.ComponenteCampos = typeof(Cadastros.Condominios.cCondominiosCampos2);
            this.cCondominiosGrade1.Cursor = System.Windows.Forms.Cursors.Default;
            this.cCondominiosGrade1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.cCondominiosGrade1.Estado = CompontesBasicos.EstadosDosComponentes.Registrado;
            
            this.cCondominiosGrade1.Location = new System.Drawing.Point(0, 43);
            this.cCondominiosGrade1.LookAndFeel.UseDefaultLookAndFeel = false;
            this.cCondominiosGrade1.Name = "cCondominiosGrade1";
            this.cCondominiosGrade1.Size = new System.Drawing.Size(732, 329);
            this.cCondominiosGrade1.TabIndex = 1;
            this.cCondominiosGrade1.TableAdapter = null;
            this.cCondominiosGrade1.Titulo = null;
            // 
            // PesquisaCondominios
            // 
            this.Controls.Add(this.cCondominiosGrade1);
            this.Controls.Add(this.groupControl1);
          
            this.LookAndFeel.UseDefaultLookAndFeel = false;
            this.Name = "PesquisaCondominios";
            this.Size = new System.Drawing.Size(732, 372);
            ((System.ComponentModel.ISupportInitialize)(this.StyleController_F)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl1)).EndInit();
            this.groupControl1.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraEditors.GroupControl groupControl1;
        private Framework.Lookup.LookupCondominio_F lookupCondominio_F1;
        private cCondominiosGrade cCondominiosGrade1;
    }
}
