using System;
using System.Data;
using System.Windows.Forms;

namespace Cadastros.Condominios
{
    /// <summary>
    /// 
    /// </summary>
    public partial class cVencimento : CompontesBasicos.ComponenteCamposBase
    {
        /// <summary>
        /// 
        /// </summary>
        public cVencimento()
        {
            InitializeComponent();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected override void btnConfirmar_F_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            this.Validate(true);

            if (!CamposObrigatoriosOk(BindingSource_F))
            {
                MessageBox.Show("Campos Obrigat�rios n�o preenchidos!!");
                return;
            };

            FechaTela(DialogResult.OK); 
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected override void btnCancelar_F_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            FechaTela(DialogResult.Cancel); 
        }

        private void RegraVenctoExtenso()
        {
            this.Validate();

            Int32 _Dia = Convert.ToInt32(((DataRowView)(BindingSource_F.Current))["CONDiaVencimento"]);

            String _TipoDia = ((DataRowView)(BindingSource_F.Current))["CONTipoVencimento"].ToString();

            String _OrdemContagem = ((DataRowView)(BindingSource_F.Current))["CONReferenciaVencimento"].ToString();

            txtRegra.Text = cCondominiosCampos2.RegraVencimento(_Dia, _TipoDia, _OrdemContagem);
        }

        private void speDia_TextChanged(object sender, EventArgs e)
        {
            RegraVenctoExtenso();
        }

        private void rdgTipo_SelectedIndexChanged(object sender, EventArgs e)
        {
            RegraVenctoExtenso();
        }

        private void rdgOrdem_SelectedIndexChanged(object sender, EventArgs e)
        {
            RegraVenctoExtenso();
        }

        private void chkFeriado_CheckedChanged(object sender, EventArgs e)
        {
            RegraVenctoExtenso();
        }
    }
}

