using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using Cadastros.Pessoas;
using Cadastros.Fornecedores;

namespace Cadastros.Condominios
{
    public partial class cApartamentos : CompontesBasicos.ComponenteCamposBase
    {
        private bool Salvar = false;
        public cApartamentos()
        {
            InitializeComponent();

            
        }

        public bool EdicaoSiplificada = true;
        
        private void cApartamentos_cargaInicial(object sender, EventArgs e)
        {
            //melhorar pois esta carregando mesmo quando n�o � necessario
            // pESSOASTableAdapter.Fill(dCondominiosCampos.PESSOAS);
            fORMAPAGTOTableAdapter.Fill(dCondominiosCampos.FORMAPAGTO);
            
            bindingSourceFRN.DataSource = Framework.datasets.dFornecedores.GetdFornecedoresST("ADV");
            Salvar = false;
        }

        private cPessoasCampos EditorPes;

        private void lkpPESSOAS_ButtonClick(object sender, DevExpress.XtraEditors.Controls.ButtonPressedEventArgs e)
        {
            DevExpress.XtraEditors.LookUpEdit Loo = (DevExpress.XtraEditors.LookUpEdit)sender;
            string CampoFPG = (Loo == lkpProprietario) ? "APTFormaPagtoProprietario_FPG" : "APTFormaPagtoInquilino_FPG";
            string CampoPES = (Loo == lkpProprietario) ? "APTProprietario_PES" : "APTInquilino_PES";                    
            switch (e.Button.Kind) {
                case DevExpress.XtraEditors.Controls.ButtonPredefines.Minus:
                    Loo.EditValue = null;                    
                    
                    LinhaMae_F[CampoFPG] = DBNull.Value;
                    LinhaMae_F[CampoPES] = DBNull.Value;
                    LinhaMae_F.EndEdit();
                    break;
                case DevExpress.XtraEditors.Controls.ButtonPredefines.Plus:
                    //int novo = ShowModuloAdd(typeof(Pessoas.cPessoasCampos), "PESNome", Loo.AutoSearchText);                    
                    EditorPes = new cPessoasCampos();
                    dPessoasCampos.PESSOASRow novarow = EditorPes.dPessoasCampos.PESSOAS.NewPESSOASRow();
                    novarow.PESNome = Loo.AutoSearchText;// Loo.Text;
                    rowCodCon = (dApartamentosCampos.CodConBlocoRow)this.dApartamentosCampos.CodConBloco.Rows[0];
                    novarow.PESEndereco = rowCodCon.CONEndereco;
                    novarow.PES_CID = rowCodCon.CON_CID;
                    if(!rowCodCon.IsCONCepNull())
                      novarow.PESCep = rowCodCon.CONCep;
                    if (!rowCodCon.IsCONCepNull())
                      novarow.PESBairro = rowCodCon.CONBairro;                    
                    EditorPes.dPessoasCampos.PESSOAS.AddPESSOASRow(novarow);
                    EditorPes.tipoDeCarga = CompontesBasicos.TipoDeCarga.navegacao;
                    //EditorPes.tipoDeCarga = CompontesBasicos.TipoDeCarga.novo;
                      //EditorPes.Fill(CompontesBasicos.TipoDeCarga.pk, (int)Loo.EditValue, false);
                    if (ShowModulo(EditorPes) == DialogResult.OK)
                    {
                        //Estatica.EAdapPessoas.Fill(Estatica.EdEstatico.PESSOAS);
                        LinhaMae_F[CampoPES] = Loo.EditValue = novarow.PES;                        
                        LinhaMae_F.EndEdit();
                        dEstatico.PESSOASRow PessoaEditadaRow = Estatica.EdEstatico.PESSOAS.FindByPES(novarow.PES);
                        if (rowCodCon.CONEndereco != PessoaEditadaRow["PESEndereco"].ToString())
                        {
                            if (PessoaEditadaRow["PESEndereco"].ToString() == "")
                            {
                                PessoaEditadaRow.PESEndereco = rowCodCon.CONEndereco;
                                PessoaEditadaRow.PESBairro = rowCodCon.CONBairro;
                                if (!rowCodCon.IsCONCepNull())
                                    PessoaEditadaRow.PESCep = rowCodCon.CONCep;
                                PessoaEditadaRow.PES_CID = rowCodCon.CON_CID;
                                PessoaEditadaRow.EndEdit();
                                Estatica.EAdapPessoas.Update(PessoaEditadaRow);
                                PessoaEditadaRow.AcceptChanges();
                                LinhaMae_F[CampoFPG] = 4;
                                //GV.CloseEditor();
                                //GV.RefreshData();
                            }
                            else
                                if (LinhaMae_F[CampoFPG] == DBNull.Value)
                                    LinhaMae_F[CampoFPG] = 2;
                        }
                        else
                        {
                            LinhaMae_F[CampoFPG] = 4;
                            LinhaMae_F.EndEdit();
                        }
                        //LinhaMaeAPT[CampoPES] = novarow.PES;
                    }
                    
                    //int pk = ShowModuloAdd(typeof(cPessoasCampos));
                    //if (pk > 0)
                    //{
                      //  Estatica.EAdapPessoas.Fill(Estatica.EdEstatico.PESSOAS);
                      //  Loo.EditValue = pk;
                    //};
                    break;
                case DevExpress.XtraEditors.Controls.ButtonPredefines.Ellipsis:
                    if ((Loo.EditValue != null) && (Loo.EditValue != DBNull.Value))
                    {
                        int PES = (int)Loo.EditValue;
                        //if(EditorPes == null)
                        EditorPes = new Pessoas.cPessoasCampos();
                        EditorPes.Fill(CompontesBasicos.TipoDeCarga.pk, PES, false);
                        EditorPes.VirShowModulo(CompontesBasicos.EstadosDosComponentes.PopUp);                             
                    };                         
                    break;

            };
            /*if (e.Button.Index == 1) //Incluir
            {
                int pk = ShowModuloAdd(typeof(cPessoasCampos));

                if (pk > 0)
                {
                    pESSOASTableAdapter.Fill(dCondominiosCampos.PESSOAS);

                    if (sender is DevExpress.XtraEditors.LookUpEdit)
                    {
                        (sender as DevExpress.XtraEditors.LookUpEdit).EditValue = pk;
                    }
                }
            }
            else
                if ((e.Button.Index == 2) && ((sender as DevExpress.XtraEditors.LookUpEdit).Text.Trim() != "")) //Visualizar
                {
                    ShowModulo(typeof(cPessoasCampos), pESSOASBindingSource, true);
                }*/
        }

        private void lkpImobiliaria_ButtonClick(object sender, DevExpress.XtraEditors.Controls.ButtonPressedEventArgs e)
        {
            if (e.Button.Index == 1) //Incluir
            {
                int pk = ShowModuloAdd(typeof(cFornecedoresCampos), "FRNTipo", "IMO");

                if (pk > 0)
                {
                    //iMOBILIARIASTableAdapter.Fill(dCondominiosCampos.IMOBILIARIAS);
                    lkpImobiliaria.EditValue = pk;
                }
            }
            else
                if ((e.Button.Index == 2) && (lkpImobiliaria.Text.Trim() != ""))//Visualizar
                {
                    ShowModulo(typeof(cFornecedoresCampos), iMOBILIARIASBindingSource, true);
                }
        }

       

        private dApartamentosCampos.APARTAMENTOSRow LinhaMaeAPT {
            get {
                return (LinhaMae_F == null) ? null : (dApartamentosCampos.APARTAMENTOSRow)LinhaMae_F;
            }
        }

        protected override void btnConfirmar_F_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            try
            {
                
                this.Validate();

                if (!CamposObrigatoriosOk(BindingSource_F))
                {
                    MessageBox.Show("Existe campo obrigat�rio sem um valor informado.", 
                                    "Campo Obrigat�rio", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    return;
                };

                if (!EdicaoSiplificada)
                {
                    //Validando campo Fracao Ideal
                    clcFracaoIdeal.ErrorText = "";

                    ((DataRowView)(BindingSource_F.Current)).Row.SetColumnError("APTFracaoIdeal", "");

                    DataRow _RowCON = ((DataRowView)(BindingSource_F.Current)).Row.GetParentRow("FK_APARTAMENTOS_BLOCOS").GetParentRow("FK_BLOCOS_CONDOMINIOS");

                    if (_RowCON["CONFracaoIdeal"] != DBNull.Value)
                    {

                        Boolean _UtilizaFracaoIdeal = Convert.ToBoolean(_RowCON["CONFracaoIdeal"]);

                        Decimal _FracaoIdeal = Convert.ToInt32(((DataRowView)(BindingSource_F.Current))["APTFracaoIdeal"]);

                        if ((_FracaoIdeal <= 0) && (_UtilizaFracaoIdeal))
                        {
                            String _Error = "Fra��o ideal n�o pode ser um n�mero menor ou igual a 0.";

                            clcFracaoIdeal.ErrorText = _Error;

                            ((DataRowView)(BindingSource_F.Current)).Row.SetColumnError("APTFracaoIdeal", _Error);
                        }
                    }

                    //Validando campo Identificacao
                    txtIdentificacao.ErrorText = "";

                    ((DataRowView)(BindingSource_F.Current)).Row.SetColumnError("APTNumero", "");

                    if (((DataRowView)(BindingSource_F.Current))["APTNumero"].ToString().Trim() == "")
                    {
                        String _Error = "Campo obrigat�rio n�o preenchido.";

                        txtIdentificacao.ErrorText = _Error;

                        ((DataRowView)(BindingSource_F.Current)).Row.SetColumnError("APTNumero", _Error);
                    }

                };
                //Se for encontrado campos invalidos nao grava
                if (((DataRowView)(BindingSource_F.Current)).Row.HasErrors)
                {
                    MessageBox.Show("Ainda existe campos sinalizando alguma ocorr�ncia.",
                                    "Campo Sinalizado", MessageBoxButtons.OK, MessageBoxIcon.Warning);

                    return;
                }

                this.Validate();

                BindingSource_F.EndEdit();

                if (EdicaoSiplificada)
                {
                    if ((LinhaMae_F["APTProprietario_PES"] != DBNull.Value) && (LinhaMae_F["APTFormaPagtoProprietario_FPG"]==DBNull.Value))
                        LinhaMae_F["APTFormaPagtoProprietario_FPG"] = 4;

                   if ((LinhaMae_F["APTInquilino_PES"] != DBNull.Value) && (LinhaMae_F["APTFormaPagtoInquilino_FPG"]==DBNull.Value))
                       LinhaMae_F["APTFormaPagtoInquilino_FPG"] = 4; ;

                    if (Update_F())
                    {


                        if ((rowCodCon != null) || (Salvar))
                        {
                            Utilitarios.MSAccess.fExportToMsAccess.ExportarApartamentosParaMsAccessRowAP(rowCodCon.CONCodigo, rowCodCon.BLOCodigo, LinhaMaeAPT);
                            Utilitarios.MSAccess.fExportToMsAccess exp = new Utilitarios.MSAccess.fExportToMsAccess();
                            if (!LinhaMaeAPT.IsAPTProprietario_PESNull())                            
                                exp.ExportarInquilinoProprietarioParaMsAccess(true, LinhaMaeAPT.APTProprietario_PES, rowCodCon.CONCodigo, rowCodCon.BLOCodigo, LinhaMaeAPT.APTNumero, LinhaMaeAPT.APTFormaPagtoProprietario_FPG);                            
                            if (!LinhaMaeAPT.IsAPTInquilino_PESNull())                            
                                exp.ExportarInquilinoProprietarioParaMsAccess(false, LinhaMaeAPT.APTInquilino_PES, rowCodCon.CONCodigo, rowCodCon.BLOCodigo, LinhaMaeAPT.APTNumero, LinhaMaeAPT.APTFormaPagtoInquilino_FPG);                            
                            else
                                Utilitarios.MSAccess.fExportToMsAccess.ApagaInquilinoDoAccess(rowCodCon.CONCodigo, rowCodCon.BLOCodigo, LinhaMaeAPT.APTNumero);
                        };
                        FechaTela(DialogResult.OK);
                    }
                    //base.btnConfirmar_F_ItemClick(sender, e);
                }
                else
                    FechaTela(DialogResult.OK);
            }
            catch (Exception Erro)
            {
                MessageBox.Show("Ocorreu um erro ao tentar efetuar a opera��o.\r\n\r\n" +Erro.Message + "\r\n\r\n Entre em contato com a Virtual Software e avise sobre o ocorrido.",
                                "Confirmar", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            };
            aPTPGEBindingSource.EndEdit();
            foreach (dApartamentosCampos.APTPGERow linha in dApartamentosCampos.APTPGE) {
                if (!linha.IsAPTPGEPagaNull()) {
                    if (linha["APTPGEProprietario"] == DBNull.Value)
                        linha.APTPGEProprietario = true;
                    if (linha.IsAPTPGEDATAINull())
                        aPTPGETableAdapter.InsertQuery(pk, linha.PGE, linha.APTPGEPaga, linha.APTPGEProprietario, CompontesBasicos.Bancovirtual.UsuarioLogado.UsuarioLogadoSt.USU, DateTime.Now);                    
                    else
                        aPTPGETableAdapter.UpdateQuery(linha.APTPGEPaga, DateTime.Now, CompontesBasicos.Bancovirtual.UsuarioLogado.UsuarioLogadoSt.USU, linha.APTPGEProprietario, pk, linha.PGE);
                }
            }
        }

        protected override void btnCancelar_F_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            BindingSource_F.CancelEdit();
            FechaTela(DialogResult.Cancel); 
        }

        private dApartamentosCampos.CodConBlocoRow rowCodCon = null;
        private DataRowView DRV;

        private void cApartamentos_cargaFinal(object sender, EventArgs e)
        {
            
            if (BindingSource_F.Current != null)
            {
                
                dApartamentosCamposTableAdapters.CodConBlocoTableAdapter Adap = new Cadastros.Condominios.dApartamentosCamposTableAdapters.CodConBlocoTableAdapter();
                Adap.TrocarStringDeConexao(VirDB.Bancovirtual.TiposDeBanco.SQL);
                Adap.Fill(this.dApartamentosCampos.CodConBloco, (int)LinhaMae_F["APT_BLO"]);
                if (this.dApartamentosCampos.CodConBloco.Rows.Count == 1)
                {
                    rowCodCon = (dApartamentosCampos.CodConBlocoRow)this.dApartamentosCampos.CodConBloco.Rows[0];
                    aPTPGETableAdapter.Fill(dApartamentosCampos.APTPGE, pk, rowCodCon.BLO_CON);
                };
                if (LinhaMae_F["APTDiaCondominio"]!=DBNull.Value)
                    if (((Int16)LinhaMae_F["APTDiaCondominio"] != rowCodCon.CONDiaVencimento) && (!(bool)LinhaMae_F["APTDiaDiferente"]))
                        LinhaMae_F["APTDiaCondominio"] = rowCodCon.CONDiaVencimento;
                if (LinhaMaeAPT.IsAPTInquilinoPagaCondominioNull())
                    LinhaMaeAPT.APTInquilinoPagaCondominio = true;
                if (LinhaMaeAPT.IsAPTInquilinoPagaFRNull())
                    LinhaMaeAPT.APTInquilinoPagaFR = false;
                
            }
        }

        private void cApartamentos_Load(object sender, EventArgs e)
        {
            
            pESSOASBindingSource.DataSource = Estatica.EdEstatico;
            if ((lkpProprietario.EditValue != DBNull.Value) && (lkpProprietario.Text == ""))
                Estatica.EAdapPessoas.Fill(Estatica.EdEstatico.PESSOAS);
            if ((lkpInquilino.EditValue != DBNull.Value) && (lkpInquilino.Text == ""))
                Estatica.EAdapPessoas.Fill(Estatica.EdEstatico.PESSOAS);
            if (Estatica.EdEstatico.PESSOAS.Rows.Count == 0)
            {
                Estatica.EAdapPessoas.Fill(Estatica.EdEstatico.PESSOAS);
            };
            Salvar = false;
            if (EdicaoSiplificada) {
                txtIdentificacao.Enabled = false;
                clcFracaoIdeal.Enabled = false;
                
                //checkEdit1.Enabled = false;
                //checkEdit2.Enabled = false;
                //checkEdit3.Enabled = false;
            }
            else
                layoutControlItem18.Visibility = DevExpress.XtraLayout.Utils.LayoutVisibility.Never;
            
        }

        private void checkEdit4_Click(object sender, EventArgs e)
        {           
            if (checkEdit4.Checked)
                spinEdit1.Value = rowCodCon.CONDiaVencimento;                
        }

        private void spinEdit1_ValueChanged(object sender, EventArgs e)
        {            
            if(LinhaMae_F["APTDiaCondominio"] != DBNull.Value)
                checkEdit4.Checked = ((Int16)LinhaMae_F["APTDiaCondominio"] != rowCodCon.CONDiaVencimento);                
        }

        private void lkpPagtoProprietario_EditValueChanged(object sender, EventArgs e)
        {
            DevExpress.XtraEditors.LookUpEdit Chamador = (DevExpress.XtraEditors.LookUpEdit)sender;
            String CampoPes = ((Chamador == lkpPagtoProprietario) ? "APTProprietario_PES" : "APTInquilino_PES");
            if (Chamador.Focused)
            {                
                DRV = (DataRowView)(BindingSource_F.Current);
                DataRow rowAp = DRV.Row;
                dEstatico.PESSOASRow PessoaEditadaRow;
                PessoaEditadaRow = Estatica.EdEstatico.PESSOAS.FindByPES((Int32)rowAp[CampoPes]);
                dApartamentosCamposTableAdapters.CodConBlocoTableAdapter Adap = new Cadastros.Condominios.dApartamentosCamposTableAdapters.CodConBlocoTableAdapter();
                Adap.TrocarStringDeConexao(VirDB.Bancovirtual.TiposDeBanco.SQL);
                Adap.Fill(this.dApartamentosCampos.CodConBloco, (Int32)rowAp["APT_BLO"]);
                if (this.dApartamentosCampos.CodConBloco.Rows.Count == 1)
                {
                    rowCodCon = (dApartamentosCampos.CodConBlocoRow)this.dApartamentosCampos.CodConBloco.Rows[0];
                    switch ((Int32)Chamador.EditValue)
                    {
                        case 4:                            
                            if (PessoaEditadaRow["PESEndereco"].ToString() != rowCodCon.CONEndereco)
                                if (MessageBox.Show("Alterar o endere�o do propriet�rio para o ender�o do pr�dio?", "Confirma��o", MessageBoxButtons.YesNo) == DialogResult.Yes)
                                {
                                    PessoaEditadaRow.PESEndereco = rowCodCon.CONEndereco;
                                    PessoaEditadaRow.PESBairro = rowCodCon.CONBairro;
                                    if (!rowCodCon.IsCONCepNull())
                                        PessoaEditadaRow.PESCep = rowCodCon.CONCep;
                                    PessoaEditadaRow.PES_CID = rowCodCon.CON_CID;
                                    PessoaEditadaRow.EndEdit();
                                    Estatica.EAdapPessoas.Update(PessoaEditadaRow);
                                    Salvar = true;
                                }
                            break;
                        case 2:
                            if (PessoaEditadaRow["PESEndereco"].ToString() == rowCodCon.CONEndereco)
                            {
                                cPessoasCampos EditorPes = new cPessoasCampos();
                                EditorPes.Fill(CompontesBasicos.TipoDeCarga.pk, (Int32)rowAp[CampoPes], false);
                                ShowModulo(EditorPes);// == DialogResult.OK;
                                    //Estatica.EAdapPessoas.Fill(Estatica.EdEstatico.PESSOAS);
                            }
                            break;
                    };
                };
                
                
                
                

            }
        }

        

        

        private void repositoryItemCheckEdit1_CheckedChanged(object sender, EventArgs e)
        {
            DevExpress.XtraEditors.CheckEdit Editor = (DevExpress.XtraEditors.CheckEdit)sender;
            Cadastros.Condominios.dApartamentosCampos.APTPGERow APTPGERow = (Cadastros.Condominios.dApartamentosCampos.APTPGERow)gridView1.GetDataRow(gridView1.FocusedRowHandle);
            if (APTPGERow != null)
            {

                //if ((Editor.Checked) && (APTPGERow.IsAPTPGEProprietarioNull()))
                if (Editor.Checked)
                {
                    bool Destinatario;
                    string comando = "SELECT PLAProprietario FROM PLAnocontas WHERE (PLA = @P1)";
                    if (VirMSSQL.TableAdapter.STTableAdapter.BuscaEscalar(comando, out Destinatario, APTPGERow.PGE_PLA))
                    {
                        APTPGERow.APTPGEProprietario = Destinatario;
                        APTPGERow.EndEdit();
                    }
                }
            }
        }

        private void lookUpEdit1_ButtonClick(object sender, DevExpress.XtraEditors.Controls.ButtonPressedEventArgs e)
        {
            if (e.Button.Kind == DevExpress.XtraEditors.Controls.ButtonPredefines.Delete)
                lookUpEdit1.EditValue = null;
        }
    }
}

