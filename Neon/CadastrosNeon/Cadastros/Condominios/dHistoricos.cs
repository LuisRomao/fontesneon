﻿namespace Cadastros.Condominios {


    partial class dHistoricos
    {
        private static dHistoricos _dHistoricosSt;

        /// <summary>
        /// dataset estático:dHistoricos
        /// </summary>
        public static dHistoricos dHistoricosSt
        {
            get
            {
                if (_dHistoricosSt == null)
                    _dHistoricosSt = new dHistoricos();
                return _dHistoricosSt;
            }
        }

        private dHistoricosTableAdapters.HISTORICOSTableAdapter hISTORICOSTableAdapter;
        /// <summary>
        /// TableAdapter padrão para tabela: HISTORICOS
        /// </summary>
        public dHistoricosTableAdapters.HISTORICOSTableAdapter HISTORICOSTableAdapter
        {
            get
            {
                if (hISTORICOSTableAdapter == null)
                {
                    hISTORICOSTableAdapter = new dHistoricosTableAdapters.HISTORICOSTableAdapter();
                    hISTORICOSTableAdapter.TrocarStringDeConexao();
                };
                return hISTORICOSTableAdapter;
            }
        }
    }
}
