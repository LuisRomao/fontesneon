namespace Cadastros.Condominios
{
    partial class cCorpoDiretivo
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(cCorpoDiretivo));
            this.LayoutControl = new DevExpress.XtraLayout.LayoutControl();
            this.lkpApto = new DevExpress.XtraEditors.LookUpEdit();
            this.fKAPARTAMENTOSBLOCOSBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.fKBLOCOSCONDOMINIOSBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.cONDOMINIOSBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.dCondominiosCampos = new Cadastros.Condominios.dCondominiosCampos();
            this.checkEdit1 = new DevExpress.XtraEditors.CheckEdit();
            this.lkpPessoa = new DevExpress.XtraEditors.LookUpEdit();
            this.pESSOASBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.lkpBloNome = new DevExpress.XtraEditors.LookUpEdit();
            this.lkpConCodigo = new DevExpress.XtraEditors.LookUpEdit();
            this.lkpCargos = new DevExpress.XtraEditors.LookUpEdit();
            this.cARGOSBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.lkpBloCodigo = new DevExpress.XtraEditors.LookUpEdit();
            this.lkpConNome = new DevExpress.XtraEditors.LookUpEdit();
            this.LayoutControlGroup = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlGroup1 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlItem3 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem2 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem8 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem4 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem1 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlGroup3 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlItem5 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem6 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem7 = new DevExpress.XtraLayout.LayoutControlItem();
            
            this.bLOCOSTableAdapter = new Cadastros.Condominios.dCondominiosCamposTableAdapters.BLOCOSTableAdapter();
            this.aPARTAMENTOSTableAdapter = new Cadastros.Condominios.dCondominiosCamposTableAdapters.APARTAMENTOSTableAdapter();
            this.cONDOMINIOSTableAdapter = new Cadastros.Condominios.dCondominiosCamposTableAdapters.CONDOMINIOSTableAdapter();
            
            this.cARGOSTableAdapter = new Cadastros.Condominios.dCondominiosCamposTableAdapters.CARGOSTableAdapter();
            ((System.ComponentModel.ISupportInitialize)(this.BarManager_F)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.BarAndDockingController_F)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.BindingSource_F)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.StyleController_F)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LayoutControl)).BeginInit();
            this.LayoutControl.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.lkpApto.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.fKAPARTAMENTOSBLOCOSBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.fKBLOCOSCONDOMINIOSBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cONDOMINIOSBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dCondominiosCampos)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit1.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lkpPessoa.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pESSOASBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lkpBloNome.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lkpConCodigo.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lkpCargos.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cARGOSBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lkpBloCodigo.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lkpConNome.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LayoutControlGroup)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem8)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem7)).BeginInit();
            this.SuspendLayout();
            // 
            // barStatus_F
            // 
            this.barStatus_F.OptionsBar.AllowQuickCustomization = false;
            this.barStatus_F.OptionsBar.DisableClose = true;
            this.barStatus_F.OptionsBar.DisableCustomization = true;
            this.barStatus_F.OptionsBar.DrawDragBorder = false;
            this.barStatus_F.OptionsBar.DrawSizeGrip = true;
            this.barStatus_F.OptionsBar.UseWholeRow = true;
            this.barStatus_F.Visible = false;
            // 
            // barComponente_F
            // 
            this.barComponente_F.OptionsBar.AllowQuickCustomization = false;
            this.barComponente_F.OptionsBar.DisableClose = true;
            this.barComponente_F.OptionsBar.DisableCustomization = true;
            this.barComponente_F.OptionsBar.DrawDragBorder = false;
            this.barComponente_F.OptionsBar.UseWholeRow = true;
            // 
            // btnFechar_F
            // 
            this.btnFechar_F.Visibility = DevExpress.XtraBars.BarItemVisibility.Never;
            // 
            // BarAndDockingController_F
            // 
            this.BarAndDockingController_F.PropertiesBar.AllowLinkLighting = false;
            // 
            // BindingSource_F
            // 
            this.BindingSource_F.DataMember = "CORPODIRETIVO";
            this.BindingSource_F.DataSource = this.dCondominiosCampos;
            // 
            // ToolTipController_F
            // 
            this.ToolTipController_F.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(222)))), ((int)(((byte)(237)))), ((int)(((byte)(250)))));
            this.ToolTipController_F.Appearance.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(20)))), ((int)(((byte)(69)))), ((int)(((byte)(141)))));
            this.ToolTipController_F.Appearance.Options.UseBackColor = true;
            this.ToolTipController_F.Appearance.Options.UseFont = true;
            this.ToolTipController_F.Appearance.Options.UseForeColor = true;
            this.ToolTipController_F.Appearance.Options.UseTextOptions = true;
            this.ToolTipController_F.Appearance.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            // 
            // StyleController_F
            // 
            this.StyleController_F.AppearanceDisabled.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.StyleController_F.AppearanceDisabled.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.StyleController_F.AppearanceDisabled.Options.UseBackColor = true;
            this.StyleController_F.AppearanceDisabled.Options.UseForeColor = true;
            this.StyleController_F.AppearanceFocused.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(253)))), ((int)(((byte)(253)))), ((int)(((byte)(181)))));
            this.StyleController_F.AppearanceFocused.Options.UseBackColor = true;
            // 
            // spellChecker1
            // 
            this.spellChecker1.OptionsSpelling.CheckSelectedTextFirst = DevExpress.Utils.DefaultBoolean.True;
            this.spellChecker1.OptionsSpelling.IgnoreEmails = DevExpress.Utils.DefaultBoolean.True;
            this.spellChecker1.OptionsSpelling.IgnoreMixedCaseWords = DevExpress.Utils.DefaultBoolean.False;
            this.spellChecker1.OptionsSpelling.IgnoreRepeatedWords = DevExpress.Utils.DefaultBoolean.False;
            this.spellChecker1.OptionsSpelling.IgnoreUpperCaseWords = DevExpress.Utils.DefaultBoolean.False;
            this.spellChecker1.OptionsSpelling.IgnoreUrls = DevExpress.Utils.DefaultBoolean.True;
            this.spellChecker1.OptionsSpelling.IgnoreWordsWithNumbers = DevExpress.Utils.DefaultBoolean.True;
            // 
            // LayoutControl
            // 
            this.LayoutControl.Appearance.DisabledLayoutGroupCaption.ForeColor = System.Drawing.SystemColors.GrayText;
            this.LayoutControl.Appearance.DisabledLayoutGroupCaption.Options.UseForeColor = true;
            this.LayoutControl.Appearance.DisabledLayoutItem.ForeColor = System.Drawing.SystemColors.GrayText;
            this.LayoutControl.Appearance.DisabledLayoutItem.Options.UseForeColor = true;
            this.LayoutControl.Controls.Add(this.lkpApto);
            this.LayoutControl.Controls.Add(this.checkEdit1);
            this.LayoutControl.Controls.Add(this.lkpPessoa);
            this.LayoutControl.Controls.Add(this.lkpBloNome);
            this.LayoutControl.Controls.Add(this.lkpConCodigo);
            this.LayoutControl.Controls.Add(this.lkpCargos);
            this.LayoutControl.Controls.Add(this.lkpBloCodigo);
            this.LayoutControl.Controls.Add(this.lkpConNome);
            this.LayoutControl.Dock = System.Windows.Forms.DockStyle.Fill;
            this.LayoutControl.Location = new System.Drawing.Point(0, 29);
            this.LayoutControl.Name = "LayoutControl";
            this.LayoutControl.Root = this.LayoutControlGroup;
            this.LayoutControl.Size = new System.Drawing.Size(755, 275);
            this.LayoutControl.TabIndex = 4;
            // 
            // lkpApto
            // 
            this.lkpApto.Location = new System.Drawing.Point(587, 68);
            this.lkpApto.Name = "lkpApto";
            this.lkpApto.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.lkpApto.Properties.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("APTNumero", "APTNumero", 62, DevExpress.Utils.FormatType.None, "", true, DevExpress.Utils.HorzAlignment.Near)});
            this.lkpApto.Properties.DataSource = this.fKAPARTAMENTOSBLOCOSBindingSource;
            this.lkpApto.Properties.DisplayMember = "APTNumero";
            this.lkpApto.Properties.ValueMember = "APT";
            this.lkpApto.Size = new System.Drawing.Size(153, 20);
            this.lkpApto.StyleController = this.LayoutControl;
            this.lkpApto.TabIndex = 10;
            this.lkpApto.EditValueChanged += new System.EventHandler(this.lkpApto_EditValueChanged);
            // 
            // fKAPARTAMENTOSBLOCOSBindingSource
            // 
            this.fKAPARTAMENTOSBLOCOSBindingSource.DataMember = "FK_APARTAMENTOS_BLOCOS";
            this.fKAPARTAMENTOSBLOCOSBindingSource.DataSource = this.fKBLOCOSCONDOMINIOSBindingSource;
            // 
            // fKBLOCOSCONDOMINIOSBindingSource
            // 
            this.fKBLOCOSCONDOMINIOSBindingSource.DataMember = "FK_BLOCOS_CONDOMINIOS";
            this.fKBLOCOSCONDOMINIOSBindingSource.DataSource = this.cONDOMINIOSBindingSource;
            // 
            // cONDOMINIOSBindingSource
            // 
            this.cONDOMINIOSBindingSource.DataMember = "CONDOMINIOS";
            this.cONDOMINIOSBindingSource.DataSource = this.dCondominiosCampos;
            // 
            // dCondominiosCampos
            // 
            this.dCondominiosCampos.DataSetName = "dCondominiosCampos";
            this.dCondominiosCampos.EnforceConstraints = false;
            this.dCondominiosCampos.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // checkEdit1
            // 
            this.checkEdit1.Location = new System.Drawing.Point(462, 158);
            this.checkEdit1.Name = "checkEdit1";
            this.checkEdit1.Properties.Caption = "Autorizado assinar cheque";
            this.checkEdit1.Size = new System.Drawing.Size(278, 18);
            this.checkEdit1.StyleController = this.LayoutControl;
            this.checkEdit1.TabIndex = 9;
            // 
            // lkpPessoa
            // 
            this.lkpPessoa.Location = new System.Drawing.Point(83, 128);
            this.lkpPessoa.Name = "lkpPessoa";
            this.lkpPessoa.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo),
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Down),
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Plus),
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.lkpPessoa.Properties.DataSource = this.pESSOASBindingSource;
            this.lkpPessoa.Properties.DisplayMember = "PESNome";
            this.lkpPessoa.Properties.ValueMember = "PES";
            this.lkpPessoa.Size = new System.Drawing.Size(657, 20);
            this.lkpPessoa.StyleController = this.LayoutControl;
            this.lkpPessoa.TabIndex = 7;
            this.lkpPessoa.ButtonClick += new DevExpress.XtraEditors.Controls.ButtonPressedEventHandler(this.lkpPessoa_ButtonClick);
            this.lkpPessoa.Enter += new System.EventHandler(this.lkpPessoa_Enter);
            // 
            // pESSOASBindingSource
            // 
            this.pESSOASBindingSource.DataMember = "PESSOAS";
            this.pESSOASBindingSource.DataSource = this.dCondominiosCampos;
            // 
            // lkpBloNome
            // 
            this.lkpBloNome.Location = new System.Drawing.Point(246, 68);
            this.lkpBloNome.Name = "lkpBloNome";
            this.lkpBloNome.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.lkpBloNome.Properties.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("BLOCodigo", "BLOCodigo", 58, DevExpress.Utils.FormatType.None, "", true, DevExpress.Utils.HorzAlignment.Near),
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("BLONome", "BLONome", 52, DevExpress.Utils.FormatType.None, "", true, DevExpress.Utils.HorzAlignment.Near)});
            this.lkpBloNome.Properties.DataSource = this.fKBLOCOSCONDOMINIOSBindingSource;
            this.lkpBloNome.Properties.DisplayMember = "BLONome";
            this.lkpBloNome.Properties.ValueMember = "BLO";
            this.lkpBloNome.Size = new System.Drawing.Size(263, 20);
            this.lkpBloNome.StyleController = this.LayoutControl;
            this.lkpBloNome.TabIndex = 6;
            this.lkpBloNome.EditValueChanged += new System.EventHandler(this.lkpBloCodigo_EditValueChanged);
            // 
            // lkpConCodigo
            // 
            this.lkpConCodigo.Enabled = false;
            this.lkpConCodigo.Location = new System.Drawing.Point(83, 38);
            this.lkpConCodigo.Name = "lkpConCodigo";
            this.lkpConCodigo.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.lkpConCodigo.Properties.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("CONCodigo", "CONCodigo", 61, DevExpress.Utils.FormatType.None, "", true, DevExpress.Utils.HorzAlignment.Near),
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("CONNome", "CONNome", 55, DevExpress.Utils.FormatType.None, "", true, DevExpress.Utils.HorzAlignment.Near)});
            this.lkpConCodigo.Properties.DataSource = this.cONDOMINIOSBindingSource;
            this.lkpConCodigo.Properties.DisplayMember = "CONCodigo";
            this.lkpConCodigo.Properties.ValueMember = "CON";
            this.lkpConCodigo.Size = new System.Drawing.Size(203, 20);
            this.lkpConCodigo.StyleController = this.LayoutControl;
            this.lkpConCodigo.TabIndex = 5;
            this.lkpConCodigo.EditValueChanged += new System.EventHandler(this.lkpConCodigo_EditValueChanged);
            // 
            // lkpCargos
            // 
            this.lkpCargos.Location = new System.Drawing.Point(83, 158);
            this.lkpCargos.Name = "lkpCargos";
            this.lkpCargos.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo),
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Plus)});
            this.lkpCargos.Properties.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("CGO", "CGO", 41, DevExpress.Utils.FormatType.Numeric, "", false, DevExpress.Utils.HorzAlignment.Far, DevExpress.Data.ColumnSortOrder.None),
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("CGONome", "CARGO", 55, DevExpress.Utils.FormatType.None, "", true, DevExpress.Utils.HorzAlignment.Near)});
            this.lkpCargos.Properties.DataSource = this.cARGOSBindingSource;
            this.lkpCargos.Properties.DisplayMember = "CGONome";
            this.lkpCargos.Properties.ValueMember = "CGO";
            this.lkpCargos.Size = new System.Drawing.Size(369, 20);
            this.lkpCargos.StyleController = this.LayoutControl;
            this.lkpCargos.TabIndex = 8;
            this.lkpCargos.ButtonClick += new DevExpress.XtraEditors.Controls.ButtonPressedEventHandler(this.lkpCargos_ButtonClick);
            // 
            // cARGOSBindingSource
            // 
            this.cARGOSBindingSource.DataMember = "CARGOS";
            this.cARGOSBindingSource.DataSource = this.dCondominiosCampos;
            // 
            // lkpBloCodigo
            // 
            this.lkpBloCodigo.Location = new System.Drawing.Point(83, 68);
            this.lkpBloCodigo.Name = "lkpBloCodigo";
            this.lkpBloCodigo.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.lkpBloCodigo.Properties.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("BLOCodigo", "BLOCodigo", 58, DevExpress.Utils.FormatType.None, "", true, DevExpress.Utils.HorzAlignment.Near),
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("BLONome", "BLONome", 52, DevExpress.Utils.FormatType.None, "", true, DevExpress.Utils.HorzAlignment.Near)});
            this.lkpBloCodigo.Properties.DataSource = this.fKBLOCOSCONDOMINIOSBindingSource;
            this.lkpBloCodigo.Properties.DisplayMember = "BLOCodigo";
            this.lkpBloCodigo.Properties.ValueMember = "BLO";
            this.lkpBloCodigo.Size = new System.Drawing.Size(153, 20);
            this.lkpBloCodigo.StyleController = this.LayoutControl;
            this.lkpBloCodigo.TabIndex = 3;
            this.lkpBloCodigo.EditValueChanged += new System.EventHandler(this.lkpBloCodigo_EditValueChanged);
            // 
            // lkpConNome
            // 
            this.lkpConNome.Enabled = false;
            this.lkpConNome.Location = new System.Drawing.Point(296, 38);
            this.lkpConNome.Name = "lkpConNome";
            this.lkpConNome.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.lkpConNome.Properties.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("CONCodigo", "CONCodigo", 61, DevExpress.Utils.FormatType.None, "", true, DevExpress.Utils.HorzAlignment.Near),
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("CONNome", "CONNome", 55, DevExpress.Utils.FormatType.None, "", true, DevExpress.Utils.HorzAlignment.Near)});
            this.lkpConNome.Properties.DataSource = this.cONDOMINIOSBindingSource;
            this.lkpConNome.Properties.DisplayMember = "CONNome";
            this.lkpConNome.Properties.ValueMember = "CON";
            this.lkpConNome.Size = new System.Drawing.Size(444, 20);
            this.lkpConNome.StyleController = this.LayoutControl;
            this.lkpConNome.TabIndex = 4;
            this.lkpConNome.EditValueChanged += new System.EventHandler(this.lkpConCodigo_EditValueChanged);
            // 
            // LayoutControlGroup
            // 
            this.LayoutControlGroup.CustomizationFormText = "LayoutControlGroup";
            this.LayoutControlGroup.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlGroup1,
            this.layoutControlGroup3});
            this.LayoutControlGroup.Location = new System.Drawing.Point(0, 0);
            this.LayoutControlGroup.Name = "LayoutControlGroup";
            this.LayoutControlGroup.Size = new System.Drawing.Size(755, 275);
            this.LayoutControlGroup.Spacing = new DevExpress.XtraLayout.Utils.Padding(0, 0, 5, 5);
            this.LayoutControlGroup.Text = "LayoutControlGroup";
            this.LayoutControlGroup.TextVisible = false;
            // 
            // layoutControlGroup1
            // 
            this.layoutControlGroup1.CustomizationFormText = "::Identifica��o ::";
            this.layoutControlGroup1.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItem3,
            this.layoutControlItem2,
            this.layoutControlItem8,
            this.layoutControlItem4,
            this.layoutControlItem1});
            this.layoutControlGroup1.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroup1.Name = "layoutControlGroup1";
            this.layoutControlGroup1.Size = new System.Drawing.Size(747, 90);
            this.layoutControlGroup1.Text = "::Identifica��o ::";
            // 
            // layoutControlItem3
            // 
            this.layoutControlItem3.Control = this.lkpConCodigo;
            this.layoutControlItem3.CustomizationFormText = "CODCON";
            this.layoutControlItem3.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItem3.Name = "layoutControlItem3";
            this.layoutControlItem3.Size = new System.Drawing.Size(281, 30);
            this.layoutControlItem3.Text = "CODCON";
            this.layoutControlItem3.TextLocation = DevExpress.Utils.Locations.Left;
            this.layoutControlItem3.TextSize = new System.Drawing.Size(63, 20);
            // 
            // layoutControlItem2
            // 
            this.layoutControlItem2.Control = this.lkpConNome;
            this.layoutControlItem2.CustomizationFormText = "layoutControlItem2";
            this.layoutControlItem2.Location = new System.Drawing.Point(281, 0);
            this.layoutControlItem2.Name = "layoutControlItem2";
            this.layoutControlItem2.Size = new System.Drawing.Size(454, 30);
            this.layoutControlItem2.Text = "layoutControlItem2";
            this.layoutControlItem2.TextLocation = DevExpress.Utils.Locations.Left;
            this.layoutControlItem2.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem2.TextToControlDistance = 0;
            this.layoutControlItem2.TextVisible = false;
            // 
            // layoutControlItem8
            // 
            this.layoutControlItem8.Control = this.lkpApto;
            this.layoutControlItem8.CustomizationFormText = "Apartamento";
            this.layoutControlItem8.Location = new System.Drawing.Point(504, 30);
            this.layoutControlItem8.Name = "layoutControlItem8";
            this.layoutControlItem8.Size = new System.Drawing.Size(231, 30);
            this.layoutControlItem8.Text = "Apartamento";
            this.layoutControlItem8.TextLocation = DevExpress.Utils.Locations.Left;
            this.layoutControlItem8.TextSize = new System.Drawing.Size(63, 20);
            // 
            // layoutControlItem4
            // 
            this.layoutControlItem4.Control = this.lkpBloNome;
            this.layoutControlItem4.CustomizationFormText = "layoutControlItem4";
            this.layoutControlItem4.Location = new System.Drawing.Point(231, 30);
            this.layoutControlItem4.Name = "layoutControlItem4";
            this.layoutControlItem4.Size = new System.Drawing.Size(273, 30);
            this.layoutControlItem4.Text = "layoutControlItem4";
            this.layoutControlItem4.TextLocation = DevExpress.Utils.Locations.Left;
            this.layoutControlItem4.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem4.TextToControlDistance = 0;
            this.layoutControlItem4.TextVisible = false;
            // 
            // layoutControlItem1
            // 
            this.layoutControlItem1.Control = this.lkpBloCodigo;
            this.layoutControlItem1.CustomizationFormText = "Bloco";
            this.layoutControlItem1.Location = new System.Drawing.Point(0, 30);
            this.layoutControlItem1.Name = "layoutControlItem1";
            this.layoutControlItem1.Size = new System.Drawing.Size(231, 30);
            this.layoutControlItem1.Text = "Bloco";
            this.layoutControlItem1.TextLocation = DevExpress.Utils.Locations.Left;
            this.layoutControlItem1.TextSize = new System.Drawing.Size(63, 20);
            // 
            // layoutControlGroup3
            // 
            this.layoutControlGroup3.CustomizationFormText = ":: Cond�mino ::";
            this.layoutControlGroup3.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItem5,
            this.layoutControlItem6,
            this.layoutControlItem7});
            this.layoutControlGroup3.Location = new System.Drawing.Point(0, 90);
            this.layoutControlGroup3.Name = "layoutControlGroup3";
            this.layoutControlGroup3.Size = new System.Drawing.Size(747, 167);
            this.layoutControlGroup3.Text = ":: Cond�mino ::";
            // 
            // layoutControlItem5
            // 
            this.layoutControlItem5.Control = this.lkpPessoa;
            this.layoutControlItem5.CustomizationFormText = "Nome";
            this.layoutControlItem5.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItem5.Name = "layoutControlItem5";
            this.layoutControlItem5.Size = new System.Drawing.Size(735, 30);
            this.layoutControlItem5.Text = "Nome";
            this.layoutControlItem5.TextLocation = DevExpress.Utils.Locations.Left;
            this.layoutControlItem5.TextSize = new System.Drawing.Size(63, 20);
            // 
            // layoutControlItem6
            // 
            this.layoutControlItem6.Control = this.lkpCargos;
            this.layoutControlItem6.CustomizationFormText = "Cargo";
            this.layoutControlItem6.Location = new System.Drawing.Point(0, 30);
            this.layoutControlItem6.Name = "layoutControlItem6";
            this.layoutControlItem6.Size = new System.Drawing.Size(447, 107);
            this.layoutControlItem6.Text = "Cargo";
            this.layoutControlItem6.TextLocation = DevExpress.Utils.Locations.Left;
            this.layoutControlItem6.TextSize = new System.Drawing.Size(63, 20);
            // 
            // layoutControlItem7
            // 
            this.layoutControlItem7.Control = this.checkEdit1;
            this.layoutControlItem7.CustomizationFormText = "layoutControlItem7";
            this.layoutControlItem7.Location = new System.Drawing.Point(447, 30);
            this.layoutControlItem7.Name = "layoutControlItem7";
            this.layoutControlItem7.Size = new System.Drawing.Size(288, 107);
            this.layoutControlItem7.Text = "layoutControlItem7";
            this.layoutControlItem7.TextLocation = DevExpress.Utils.Locations.Left;
            this.layoutControlItem7.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem7.TextToControlDistance = 0;
            this.layoutControlItem7.TextVisible = false;
            
            // 
            // bLOCOSTableAdapter
            // 
            this.bLOCOSTableAdapter.ClearBeforeFill = true;
            this.bLOCOSTableAdapter.GetNovosDados = null;
            this.bLOCOSTableAdapter.TabelaDataTable = null;
            // 
            // aPARTAMENTOSTableAdapter
            // 
            this.aPARTAMENTOSTableAdapter.ClearBeforeFill = true;
            this.aPARTAMENTOSTableAdapter.GetNovosDados = null;
            this.aPARTAMENTOSTableAdapter.TabelaDataTable = null;
            // 
            // cONDOMINIOSTableAdapter
            // 
            this.cONDOMINIOSTableAdapter.ClearBeforeFill = true;
            this.cONDOMINIOSTableAdapter.GetNovosDados = null;
            this.cONDOMINIOSTableAdapter.TabelaDataTable = null;
            
            // 
            // cARGOSTableAdapter
            // 
            this.cARGOSTableAdapter.ClearBeforeFill = true;
            this.cARGOSTableAdapter.GetNovosDados = null;
            this.cARGOSTableAdapter.TabelaDataTable = null;
            // 
            // cCorpoDiretivo
            // 
            this.Autofill = false;
            this.Controls.Add(this.LayoutControl);
            this.Cursor = System.Windows.Forms.Cursors.Default;
            this.ImagemG = ((System.Drawing.Image)(resources.GetObject("$this.ImagemG")));
            this.ImagemP = ((System.Drawing.Image)(resources.GetObject("$this.ImagemP")));
            this.LookAndFeel.UseDefaultLookAndFeel = false;
            this.Name = "cCorpoDiretivo";
            this.Size = new System.Drawing.Size(755, 325);
            this.Load += new System.EventHandler(this.cCorpoDiretivo_Load);
            this.Controls.SetChildIndex(this.LayoutControl, 0);
            ((System.ComponentModel.ISupportInitialize)(this.BarManager_F)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.BarAndDockingController_F)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.BindingSource_F)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.StyleController_F)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LayoutControl)).EndInit();
            this.LayoutControl.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.lkpApto.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.fKAPARTAMENTOSBLOCOSBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.fKBLOCOSCONDOMINIOSBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cONDOMINIOSBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dCondominiosCampos)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit1.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lkpPessoa.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pESSOASBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lkpBloNome.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lkpConCodigo.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lkpCargos.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cARGOSBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lkpBloCodigo.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lkpConNome.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LayoutControlGroup)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem8)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem7)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraLayout.LayoutControl LayoutControl;
        private DevExpress.XtraLayout.LayoutControlGroup LayoutControlGroup;
        private DevExpress.XtraEditors.LookUpEdit lkpBloNome;
        private DevExpress.XtraEditors.LookUpEdit lkpConCodigo;
        private DevExpress.XtraEditors.LookUpEdit lkpConNome;
        private DevExpress.XtraEditors.LookUpEdit lkpBloCodigo;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem3;
        private DevExpress.XtraEditors.CheckEdit checkEdit1;
        private DevExpress.XtraEditors.LookUpEdit lkpCargos;
        private DevExpress.XtraEditors.LookUpEdit lkpPessoa;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup1;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem2;
        private DevExpress.XtraEditors.LookUpEdit lkpApto;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem8;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem4;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem1;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup3;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem5;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem6;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem7;
        private dCondominiosCampos dCondominiosCampos;
        private System.Windows.Forms.BindingSource fKAPARTAMENTOSBLOCOSBindingSource;
        private System.Windows.Forms.BindingSource fKBLOCOSCONDOMINIOSBindingSource;
        private System.Windows.Forms.BindingSource cONDOMINIOSBindingSource;
        
        private Cadastros.Condominios.dCondominiosCamposTableAdapters.BLOCOSTableAdapter bLOCOSTableAdapter;
        private Cadastros.Condominios.dCondominiosCamposTableAdapters.APARTAMENTOSTableAdapter aPARTAMENTOSTableAdapter;
        private Cadastros.Condominios.dCondominiosCamposTableAdapters.CONDOMINIOSTableAdapter cONDOMINIOSTableAdapter;
        private System.Windows.Forms.BindingSource pESSOASBindingSource;
        
        private System.Windows.Forms.BindingSource cARGOSBindingSource;
        private Cadastros.Condominios.dCondominiosCamposTableAdapters.CARGOSTableAdapter cARGOSTableAdapter;
    }
}
