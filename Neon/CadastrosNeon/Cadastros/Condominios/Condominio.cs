﻿using System;
using System.Collections.Generic;
using Abstratos;
using AbstratosNeon;
using DocBacarios;
using VirEnumeracoesNeon;
using CadastrosProc;
using CadastrosProc.Condominios;

namespace Cadastros.Condominios
{
    /// <summary>
    /// Componente representante dos condomínios
    /// </summary>
    public class Condominio : CondominioProc
    {
        /// <summary>
        /// Retorna todos os condomínios
        /// </summary>
        /// <param name="Ativos"></param>
        /// <returns></returns>
        public static SortedList<int, ABS_Condominio> GetCondominios(bool Ativos)
        {            
            SortedList<int, ABS_Condominio> Retorno = new SortedList<int, ABS_Condominio>();
            //dApartamentosCampos dApartamentosCampos1 = new dApartamentosCampos();
            dCondominiosCampos dCondominiosCampos1 = new dCondominiosCampos();
            if (Ativos)
                dCondominiosCampos1.CONDOMINIOSTableAdapter.FillByAtivos(dCondominiosCampos1.CONDOMINIOS,Framework.DSCentral.EMP);
            else
                dCondominiosCampos1.CONDOMINIOSTableAdapter.FillByTodos(dCondominiosCampos1.CONDOMINIOS);
            foreach (dCondominiosCampos.CONDOMINIOSRow row in dCondominiosCampos1.CONDOMINIOS)
            {
                Condominio Novo = new Condominio(row);                
                Retorno.Add(row.CON, Novo);
            }
            dContaCorrente dContaCorrente1 = new dContaCorrente();
            dContaCorrente1.ContaCorrenTeTableAdapter.FillAtivas(dContaCorrente1.ContaCorrenTe);
            //dContaCorrente.ContaCorrenTeDataTable TabelaCCT = dContaCorrente.dContaCorrenteSt.ContaCorrenTeTableAdapter.GetDataAtivas();
            foreach (dContaCorrente.ContaCorrenTeRow rowCCT in dContaCorrente1.ContaCorrenTe)
            {
                if (Retorno.ContainsKey(rowCCT.CCT_CON))
                    ((Condominio)Retorno[rowCCT.CCT_CON]).CarregaContaLote(rowCCT);
            }
            return Retorno;
        }

        
        

        //private dCondominiosCampos dCondominiosCampos;

        /// <summary>
        /// Construitor
        /// </summary>
        /// <param name="CON"></param>
        public Condominio(int CON): base(CON)
        {
            //dCondominiosCampos = new dCondominiosCampos();
            //dCondominiosCampos.CONDOMINIOSTableAdapter.Fill(dCondominiosCampos.CONDOMINIOS, CON);
            //CONrow = dCondominiosCampos.CONDOMINIOS[0];
        }

        /// <summary>
        /// Construtor
        /// </summary>
        /// <param name="_CONRow"></param>
        public Condominio(dCondominiosCampos.CONDOMINIOSRow _CONRow):base(_CONRow)
        {
            //CONrow = _CONRow;
            //dCondominiosCampos = (dCondominiosCampos)CONrow.Table.DataSet;
        }

        
        



        

        



        

        //private CadastrosProc.PessoaProc sindico;

        /// <summary>
        /// Sindico
        /// </summary>
        //public override ABS_Pessoa Sindico => sindico ?? (sindico = CadastrosProc.PessoaProc.GetSindico(CON));

        

        private Fornecedores.Fornecedor _escritorioAdvocacia;

        /// <summary>
        /// Escritório de advocacia
        /// </summary>
        public Fornecedores.Fornecedor EscritorioAdvocacia_Fornecedor
        {
            get
            {
                if (CONrow.IsCONEscrit_FRNNull())
                    return null;
                else
                    return _escritorioAdvocacia ?? (_escritorioAdvocacia = new Fornecedores.Fornecedor(CONrow.CONEscrit_FRN));
            }
        }

        /// <summary>
        /// Escritório de advocacia tipado para ABS
        /// </summary>
        public override ABS_Fornecedor EscritorioAdvocacia => EscritorioAdvocacia_Fornecedor;
        

    }
}
