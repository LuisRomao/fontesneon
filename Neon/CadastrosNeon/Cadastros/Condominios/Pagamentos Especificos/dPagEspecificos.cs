﻿namespace Cadastros.Condominios.Pagamentos_Especificos {


    partial class dPagEspecificos
    {
        static private dPagEspecificosTableAdapters.PaGamentoEspecificoTableAdapter paGamentoEspecificoTableAdapter;
        static private dPagEspecificosTableAdapters.APTxPGETableAdapter aPTxPGETableAdapter;
        static private dPagEspecificosTableAdapters.ApartamentosPGETableAdapter apartamentosPGETableAdapter;

        /// <summary>
        /// 
        /// </summary>
        public static dPagEspecificosTableAdapters.ApartamentosPGETableAdapter ApartamentosPGETableAdapter
        {
            get {
                if (apartamentosPGETableAdapter == null) {
                    apartamentosPGETableAdapter = new Cadastros.Condominios.Pagamentos_Especificos.dPagEspecificosTableAdapters.ApartamentosPGETableAdapter();
                    apartamentosPGETableAdapter.TrocarStringDeConexao(VirDB.Bancovirtual.TiposDeBanco.SQL);
                };
                return dPagEspecificos.apartamentosPGETableAdapter; 
            }
            
        }

        /// <summary>
        /// 
        /// </summary>
        public static dPagEspecificosTableAdapters.APTxPGETableAdapter APTxPGETableAdapter
        {
            get {
                if (aPTxPGETableAdapter == null) {
                    aPTxPGETableAdapter = new Cadastros.Condominios.Pagamentos_Especificos.dPagEspecificosTableAdapters.APTxPGETableAdapter();
                    aPTxPGETableAdapter.TrocarStringDeConexao(VirDB.Bancovirtual.TiposDeBanco.SQL);
                };
                return dPagEspecificos.aPTxPGETableAdapter; 
            }
            
        }

        /// <summary>
        /// 
        /// </summary>
        public static dPagEspecificosTableAdapters.PaGamentoEspecificoTableAdapter PaGamentoEspecificoTableAdapter
        {
            get {
                if (paGamentoEspecificoTableAdapter == null) {
                    paGamentoEspecificoTableAdapter = new Cadastros.Condominios.Pagamentos_Especificos.dPagEspecificosTableAdapters.PaGamentoEspecificoTableAdapter();
                    paGamentoEspecificoTableAdapter.TrocarStringDeConexao(VirDB.Bancovirtual.TiposDeBanco.SQL);
                };
                return dPagEspecificos.paGamentoEspecificoTableAdapter; 
            }
            
        }
    }
}
