using System;
using System.Collections;
using System.Data;
using System.Windows.Forms;
using DevExpress.XtraGrid.Columns;

namespace Cadastros.Condominios
{
    /// <summary>
    /// 
    /// </summary>
    public partial class cPagEspecificos : CompontesBasicos.ComponenteCamposBase
    {
        private class Coluna:Object {
            public Int32 PGE;
            public string Titulo;
            public GridColumn ColunaG;
            public Coluna(Int32 PGE,string Titulo) {
                this.PGE = PGE;
                this.Titulo = Titulo;
            }
            public override string ToString()
            {
                return PGE.ToString();
            }             
        }

        //private int PGE;
        private int CON;
        private int Posicao = 3;

        private GridColumn BuscaColuna (Int32 PGE){ 
            foreach (Coluna Col in ListasMem)
                if (Col.PGE == PGE)
                    return Col.ColunaG;
            return null;
        }

        private void CriaColuna(Coluna Col) {
            DataColumn novaColuna;
            DevExpress.XtraGrid.Columns.GridColumn DvnovaColuna = null;            
            novaColuna = dPagEspecificos.Tela.Columns.Add(Col.PGE.ToString(), typeof(string));
            novaColuna.Caption = Col.Titulo;
            DvnovaColuna = gridView1.Columns.Add();
            DvnovaColuna.FieldName = Col.PGE.ToString();
            DvnovaColuna.Visible = true;
            DvnovaColuna.Width = 120;
            DvnovaColuna.Caption = Col.Titulo;
            DvnovaColuna.ColumnEdit = EditorPI;
            DvnovaColuna.OptionsColumn.AllowEdit = true;
            DvnovaColuna.AbsoluteIndex = Posicao;
            Posicao++;
            //novaColuna = dPrevBol.CONDOMINIOS.Columns.Add("Gerado" + mes.ToString("00") + "/" + ano.ToString(), typeof(bool));
            //DvnovaColuna = gridView1.Columns.Add();
            //DvnovaColuna.FieldName = mes.ToString() + "/" + ano.ToString();
            //DvnovaColuna.Visible = true;
            Col.ColunaG = DvnovaColuna;
        }

        
        ArrayList ListasMem;

       
        /// <summary>
        /// 
        /// </summary>
        /// <param name="_CON"></param>
        public cPagEspecificos(int _CON)
        {
            
            InitializeComponent();
            //PGE = _PGE;
            CON = _CON;
            Pagamentos_Especificos.dPagEspecificos.ApartamentosPGETableAdapter.Fill(dPagEspecificos.ApartamentosPGE, CON);
            dPagEspecificos.Tela.Clear();            
            ListasMem = new System.Collections.ArrayList();
            Pagamentos_Especificos.dPagEspecificos.PaGamentoEspecificoDataTable linhasPGE = Pagamentos_Especificos.dPagEspecificos.PaGamentoEspecificoTableAdapter.GetData(_CON);
            foreach (Pagamentos_Especificos.dPagEspecificos.PaGamentoEspecificoRow linhas in linhasPGE) {
                
                if (BuscaColuna(linhas.PGE) == null)
                  {
                      Coluna Col = new Coluna(linhas.PGE, linhas.PGEMensagem+" - "+linhas.PGEValor.ToString("0.00"));                      
                      ListasMem.Add(Col);
                      CriaColuna(Col);                    
                  };                
            };
            
            foreach (Pagamentos_Especificos.dPagEspecificos.ApartamentosPGERow LinhaAPTPGE in dPagEspecificos.ApartamentosPGE) {
                Pagamentos_Especificos.dPagEspecificos.TelaRow LinhaTela = dPagEspecificos.Tela.FindByAPT(LinhaAPTPGE.APT);
               
                if (LinhaTela == null)
                {
                    LinhaTela = dPagEspecificos.Tela.NewTelaRow();
                    LinhaTela.Bloco = LinhaAPTPGE.BLOCodigo;
                    LinhaTela.Apartamento = LinhaAPTPGE.APTNumero;
                    LinhaTela.APT = LinhaAPTPGE.APT;
                    LinhaTela.oculto = true;
                    dPagEspecificos.Tela.Rows.Add(LinhaTela);
                };
            //    if((!LinhaAPTPGE.IsPGENull() && (!LinhaAPTPGE.IsAPTPGEPagaNull())))
            //        if(LinhaAPTPGE.APTPGEPaga)
            //            LinhaTela[LinhaAPTPGE.PGE.ToString()] = (LinhaAPTPGE.APTPGEProprietario ? "P":"I");
                
                    
            }
            Pagamentos_Especificos.dPagEspecificos.APTxPGETableAdapter.Fill(dPagEspecificos.APTxPGE, CON);
            foreach (Pagamentos_Especificos.dPagEspecificos.APTxPGERow APTxPGERow in dPagEspecificos.APTxPGE) {
                Pagamentos_Especificos.dPagEspecificos.TelaRow LinhaTela = dPagEspecificos.Tela.FindByAPT(APTxPGERow.APT);

                if (LinhaTela != null)
                    if (APTxPGERow.APTPGEPaga)
                    {
                        LinhaTela[APTxPGERow.PGE.ToString()] = (APTxPGERow.APTPGEProprietario ? "P" : "I");
                        LinhaTela.oculto = false;
                    }
            }
            dPagEspecificos.Tela.AcceptChanges();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected override void btnConfirmar_F_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            
            Validate();
            apartamentosPGEBindingSource.EndEdit();
            
            foreach (Pagamentos_Especificos.dPagEspecificos.TelaRow linhaTela in dPagEspecificos.Tela)
            {
                if (linhaTela.RowState != DataRowState.Unchanged)
                {
                    foreach (DataColumn ColunaX in dPagEspecificos.Tela.Columns)
                    {
                        if (ColunaX.Ordinal > 3)
                            if (linhaTela[ColunaX] != DBNull.Value)
                            {
                                int PGE = int.Parse(ColunaX.ColumnName);
                                Pagamentos_Especificos.dPagEspecificos.APTxPGERow APTxPGERow = dPagEspecificos.APTxPGE.FindByAPTPGE(linhaTela.APT, PGE);
                                if (APTxPGERow == null)
                                {
                                    APTxPGERow = dPagEspecificos.APTxPGE.NewAPTxPGERow();
                                    APTxPGERow.APT = linhaTela.APT;
                                    APTxPGERow.PGE = PGE;
                                    APTxPGERow.APTPGEDATAI = DateTime.Now;
                                    APTxPGERow.APTPGEI_USU = CompontesBasicos.Bancovirtual.UsuarioLogado.UsuarioLogadoSt.USU;
                                    APTxPGERow.APTPGEPaga = false;
                                    APTxPGERow.APTPGEProprietario = true;
                                    dPagEspecificos.APTxPGE.AddAPTxPGERow(APTxPGERow);
                                };

                                switch (linhaTela[ColunaX].ToString())
                                {
                                    case "P": APTxPGERow.APTPGEProprietario = true;
                                        APTxPGERow.APTPGEPaga = true;
                                        break;
                                    case "I": APTxPGERow.APTPGEProprietario = false;
                                        APTxPGERow.APTPGEPaga = true;
                                        break;
                                    case "": APTxPGERow.APTPGEPaga = false;
                                        break;
                                };
                                if (APTxPGERow.RowState != DataRowState.Unchanged)
                                {
                                    APTxPGERow.APTPGEDATAA = DateTime.Now;
                                    APTxPGERow.APTPGEA_USU = CompontesBasicos.Bancovirtual.UsuarioLogado.UsuarioLogadoSt.USU;
                                };
                            };
                    };

                    Pagamentos_Especificos.dPagEspecificos.APTxPGETableAdapter.Update(dPagEspecificos.APTxPGE);
                    dPagEspecificos.APTxPGE.AcceptChanges();
                };
            }
            FechaTela(DialogResult.OK);    
            
        } 

        
    }
}

