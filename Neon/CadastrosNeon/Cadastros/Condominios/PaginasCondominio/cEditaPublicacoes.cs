using System;
using System.ComponentModel;
using CompontesBasicos;

namespace Cadastros.Condominios.PaginasCondominio
{
    /// <summary>
    /// 
    /// </summary>
    public partial class cEditaPublicacoes : ComponenteBaseDialog
    {
        /// <summary>
        /// 
        /// </summary>
        public cEditaPublicacoes()
        {
            InitializeComponent();
            TabPublicacoes.VirEnumTipos.CarregaEditorDaGrid(XTipo);            
            FormPrincipalBase.FormPrincipal.CaixaAltaGeral = false;
            
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="container"></param>
        public cEditaPublicacoes(IContainer container)
        {
            container.Add(this);

            InitializeComponent();
            CaixaAltaGeral = false;
        }

        private void XArquivo_EditValueChanged(object sender, EventArgs e)
        {
            
        }

        private void XArquivo_ButtonClick(object sender, DevExpress.XtraEditors.Controls.ButtonPressedEventArgs e)
        {
            if (openFileDialog1.ShowDialog() == System.Windows.Forms.DialogResult.OK)
                XArquivo.Text = openFileDialog1.FileName;
            CompontesBasicos.FormPrincipalBase.FormPrincipal.CaixaAltaGeral = false;
            
            CaixaAltaGeral = false;
        }
    }
}
