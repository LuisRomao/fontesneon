﻿namespace Cadastros.Condominios.PaginasCondominio
{
    partial class TabFaturamento
    {
        /// <summary> 
        /// Variável de designer necessária.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Limpar os recursos que estão sendo usados.
        /// </summary>
        /// <param name="disposing">true se for necessário descartar os recursos gerenciados; caso contrário, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Código gerado pelo Designer de Componentes

        /// <summary> 
        /// Método necessário para suporte ao Designer - não modifique 
        /// o conteúdo deste método com o editor de código.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.gridControl1 = new DevExpress.XtraGrid.GridControl();
            this.rEPassesBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.gridView1 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colREP = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colREP_CON = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colREP_INS = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colREPQuantidade = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colINSDescricao = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colINSValor = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemCalcEdit1 = new DevExpress.XtraEditors.Repository.RepositoryItemCalcEdit();
            this.colINSUnidade = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colINS = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colREPValorDif = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemCalcEditcomDEL = new DevExpress.XtraEditors.Repository.RepositoryItemCalcEdit();
            this.colVALORefetivo = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colcalcValor = new DevExpress.XtraGrid.Columns.GridColumn();
            this.labelControl1 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl2 = new DevExpress.XtraEditors.LabelControl();
            this.calcEdit1 = new DevExpress.XtraEditors.CalcEdit();
            ((System.ComponentModel.ISupportInitialize)(this.StyleController_F)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.rEPassesBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCalcEdit1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCalcEditcomDEL)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.calcEdit1.Properties)).BeginInit();
            this.SuspendLayout();
            // 
            // ToolTipController_F
            // 
            this.ToolTipController_F.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.ToolTipController_F.Appearance.ForeColor = System.Drawing.Color.Black;
            this.ToolTipController_F.Appearance.Options.UseBackColor = true;
            this.ToolTipController_F.Appearance.Options.UseFont = true;
            this.ToolTipController_F.Appearance.Options.UseForeColor = true;
            this.ToolTipController_F.Appearance.Options.UseTextOptions = true;
            this.ToolTipController_F.Appearance.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            // 
            // StyleController_F
            // 
            this.StyleController_F.AppearanceDisabled.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this.StyleController_F.AppearanceDisabled.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this.StyleController_F.AppearanceDisabled.Options.UseBackColor = true;
            this.StyleController_F.AppearanceDisabled.Options.UseForeColor = true;
            this.StyleController_F.AppearanceFocused.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.StyleController_F.AppearanceFocused.Options.UseBackColor = true;
            // 
            // gridControl1
            // 
            this.gridControl1.DataSource = this.rEPassesBindingSource;
            this.gridControl1.Location = new System.Drawing.Point(135, 57);
            this.gridControl1.MainView = this.gridView1;
            this.gridControl1.Name = "gridControl1";
            this.gridControl1.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemCalcEdit1,
            this.repositoryItemCalcEditcomDEL});
            this.gridControl1.Size = new System.Drawing.Size(831, 426);
            this.gridControl1.TabIndex = 0;
            this.gridControl1.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridView1});
            // 
            // rEPassesBindingSource
            // 
            this.rEPassesBindingSource.DataMember = "INSumos";
            this.rEPassesBindingSource.DataSource = typeof(CadastrosProc.Faturamento.dInsumos);
            // 
            // gridView1
            // 
            this.gridView1.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colREP,
            this.colREP_CON,
            this.colREP_INS,
            this.colREPQuantidade,
            this.colINSDescricao,
            this.colINSValor,
            this.colINSUnidade,
            this.colINS,
            this.colREPValorDif,
            this.colVALORefetivo,
            this.colcalcValor});
            this.gridView1.GridControl = this.gridControl1;
            this.gridView1.Name = "gridView1";
            this.gridView1.OptionsView.ColumnAutoWidth = false;
            this.gridView1.OptionsView.ShowGroupPanel = false;
            this.gridView1.SortInfo.AddRange(new DevExpress.XtraGrid.Columns.GridColumnSortInfo[] {
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colINSDescricao, DevExpress.Data.ColumnSortOrder.Ascending)});
            this.gridView1.RowUpdated += new DevExpress.XtraGrid.Views.Base.RowObjectEventHandler(this.gridView1_RowUpdated);
            // 
            // colREP
            // 
            this.colREP.FieldName = "REP";
            this.colREP.Name = "colREP";
            this.colREP.OptionsColumn.ReadOnly = true;
            // 
            // colREP_CON
            // 
            this.colREP_CON.FieldName = "REP_CON";
            this.colREP_CON.Name = "colREP_CON";
            this.colREP_CON.OptionsColumn.ReadOnly = true;
            // 
            // colREP_INS
            // 
            this.colREP_INS.FieldName = "REP_INS";
            this.colREP_INS.Name = "colREP_INS";
            this.colREP_INS.OptionsColumn.ReadOnly = true;
            // 
            // colREPQuantidade
            // 
            this.colREPQuantidade.AppearanceCell.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.colREPQuantidade.AppearanceCell.BackColor2 = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.colREPQuantidade.AppearanceCell.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.colREPQuantidade.AppearanceCell.Options.UseBackColor = true;
            this.colREPQuantidade.AppearanceCell.Options.UseFont = true;
            this.colREPQuantidade.Caption = "Quantidade";
            this.colREPQuantidade.FieldName = "REPQuantidade";
            this.colREPQuantidade.Name = "colREPQuantidade";
            this.colREPQuantidade.Visible = true;
            this.colREPQuantidade.VisibleIndex = 1;
            this.colREPQuantidade.Width = 69;
            // 
            // colINSDescricao
            // 
            this.colINSDescricao.Caption = "Descrição";
            this.colINSDescricao.FieldName = "INSDescricao";
            this.colINSDescricao.Name = "colINSDescricao";
            this.colINSDescricao.OptionsColumn.ReadOnly = true;
            this.colINSDescricao.Visible = true;
            this.colINSDescricao.VisibleIndex = 0;
            this.colINSDescricao.Width = 325;
            // 
            // colINSValor
            // 
            this.colINSValor.Caption = "Valor Padrão";
            this.colINSValor.ColumnEdit = this.repositoryItemCalcEdit1;
            this.colINSValor.FieldName = "INSValor";
            this.colINSValor.Name = "colINSValor";
            this.colINSValor.OptionsColumn.ReadOnly = true;
            this.colINSValor.Visible = true;
            this.colINSValor.VisibleIndex = 3;
            this.colINSValor.Width = 87;
            // 
            // repositoryItemCalcEdit1
            // 
            this.repositoryItemCalcEdit1.AutoHeight = false;
            this.repositoryItemCalcEdit1.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemCalcEdit1.Mask.EditMask = "n2";
            this.repositoryItemCalcEdit1.Mask.UseMaskAsDisplayFormat = true;
            this.repositoryItemCalcEdit1.Name = "repositoryItemCalcEdit1";
            // 
            // colINSUnidade
            // 
            this.colINSUnidade.Caption = "Unidade";
            this.colINSUnidade.FieldName = "INSUnidade";
            this.colINSUnidade.Name = "colINSUnidade";
            this.colINSUnidade.OptionsColumn.ReadOnly = true;
            this.colINSUnidade.Visible = true;
            this.colINSUnidade.VisibleIndex = 2;
            this.colINSUnidade.Width = 54;
            // 
            // colINS
            // 
            this.colINS.FieldName = "INS";
            this.colINS.Name = "colINS";
            this.colINS.OptionsColumn.ReadOnly = true;
            // 
            // colREPValorDif
            // 
            this.colREPValorDif.AppearanceCell.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.colREPValorDif.AppearanceCell.BackColor2 = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.colREPValorDif.AppearanceCell.Options.UseBackColor = true;
            this.colREPValorDif.Caption = "Valor Diferenciado";
            this.colREPValorDif.ColumnEdit = this.repositoryItemCalcEditcomDEL;
            this.colREPValorDif.FieldName = "REPValorDif";
            this.colREPValorDif.Name = "colREPValorDif";
            this.colREPValorDif.Visible = true;
            this.colREPValorDif.VisibleIndex = 4;
            this.colREPValorDif.Width = 108;
            // 
            // repositoryItemCalcEditcomDEL
            // 
            this.repositoryItemCalcEditcomDEL.AutoHeight = false;
            this.repositoryItemCalcEditcomDEL.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo),
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Delete)});
            this.repositoryItemCalcEditcomDEL.Mask.EditMask = "n2";
            this.repositoryItemCalcEditcomDEL.Mask.UseMaskAsDisplayFormat = true;
            this.repositoryItemCalcEditcomDEL.Name = "repositoryItemCalcEditcomDEL";
            this.repositoryItemCalcEditcomDEL.ButtonClick += new DevExpress.XtraEditors.Controls.ButtonPressedEventHandler(this.repositoryItemCalcEditcomDEL_ButtonClick);
            // 
            // colVALORefetivo
            // 
            this.colVALORefetivo.Caption = "Valor Efetivo";
            this.colVALORefetivo.ColumnEdit = this.repositoryItemCalcEdit1;
            this.colVALORefetivo.FieldName = "VALORefetivo";
            this.colVALORefetivo.Name = "colVALORefetivo";
            this.colVALORefetivo.OptionsColumn.ReadOnly = true;
            // 
            // colcalcValor
            // 
            this.colcalcValor.Caption = "Total";
            this.colcalcValor.ColumnEdit = this.repositoryItemCalcEdit1;
            this.colcalcValor.FieldName = "calcValor";
            this.colcalcValor.Name = "colcalcValor";
            this.colcalcValor.OptionsColumn.ReadOnly = true;
            this.colcalcValor.Visible = true;
            this.colcalcValor.VisibleIndex = 5;
            this.colcalcValor.Width = 127;
            // 
            // labelControl1
            // 
            this.labelControl1.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl1.Appearance.Options.UseFont = true;
            this.labelControl1.Location = new System.Drawing.Point(25, 18);
            this.labelControl1.Name = "labelControl1";
            this.labelControl1.Size = new System.Drawing.Size(47, 19);
            this.labelControl1.TabIndex = 0;
            this.labelControl1.Text = "Taxa:";
            // 
            // labelControl2
            // 
            this.labelControl2.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl2.Appearance.Options.UseFont = true;
            this.labelControl2.Location = new System.Drawing.Point(25, 57);
            this.labelControl2.Name = "labelControl2";
            this.labelControl2.Size = new System.Drawing.Size(74, 19);
            this.labelControl2.TabIndex = 1;
            this.labelControl2.Text = "Repasse:";
            // 
            // calcEdit1
            // 
            this.calcEdit1.Location = new System.Drawing.Point(135, 15);
            this.calcEdit1.Name = "calcEdit1";
            this.calcEdit1.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.calcEdit1.Properties.Appearance.Options.UseFont = true;
            this.calcEdit1.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.calcEdit1.Size = new System.Drawing.Size(364, 26);
            this.calcEdit1.TabIndex = 2;
            // 
            // TabFaturamento
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.calcEdit1);
            this.Controls.Add(this.labelControl2);
            this.Controls.Add(this.labelControl1);
            this.Controls.Add(this.gridControl1);
            this.Doc = System.Windows.Forms.DockStyle.Fill;
            this.LookAndFeel.UseDefaultLookAndFeel = false;
            this.Name = "TabFaturamento";
            this.Size = new System.Drawing.Size(1531, 473);
            this.Load += new System.EventHandler(this.TabFaturamento_Load);
            ((System.ComponentModel.ISupportInitialize)(this.StyleController_F)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.rEPassesBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCalcEdit1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCalcEditcomDEL)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.calcEdit1.Properties)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private DevExpress.XtraGrid.GridControl gridControl1;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView1;
        private DevExpress.XtraEditors.LabelControl labelControl1;
        private DevExpress.XtraEditors.LabelControl labelControl2;
        private DevExpress.XtraEditors.CalcEdit calcEdit1;
        private System.Windows.Forms.BindingSource rEPassesBindingSource;
        private DevExpress.XtraGrid.Columns.GridColumn colREP;
        private DevExpress.XtraGrid.Columns.GridColumn colREP_CON;
        private DevExpress.XtraGrid.Columns.GridColumn colREP_INS;
        private DevExpress.XtraGrid.Columns.GridColumn colREPQuantidade;
        private DevExpress.XtraGrid.Columns.GridColumn colINSDescricao;
        private DevExpress.XtraGrid.Columns.GridColumn colINSValor;
        private DevExpress.XtraGrid.Columns.GridColumn colINSUnidade;
        private DevExpress.XtraGrid.Columns.GridColumn colINS;
        private DevExpress.XtraEditors.Repository.RepositoryItemCalcEdit repositoryItemCalcEdit1;
        private DevExpress.XtraGrid.Columns.GridColumn colREPValorDif;
        private DevExpress.XtraGrid.Columns.GridColumn colVALORefetivo;
        private DevExpress.XtraGrid.Columns.GridColumn colcalcValor;
        private DevExpress.XtraEditors.Repository.RepositoryItemCalcEdit repositoryItemCalcEditcomDEL;
    }
}
