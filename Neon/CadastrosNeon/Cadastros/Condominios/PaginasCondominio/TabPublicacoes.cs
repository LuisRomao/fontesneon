/*
MR - 05/01/2015 10:00 -           - Implementado o envio por email da publica��o selecionada para todos os condominos ao clicar em "Enviar por E-mail" como mala direta (Altera��es indicadas por *** MRC - INICIO (05/01/2015 10:00) ***)
*/

using System;
using System.IO;
using System.Windows.Forms;
using dllVirEnum;
using maladireta;
using System.Net.Mail;
using System.Net;
using CadastrosProc.Condominios;

namespace Cadastros.Condominios.PaginasCondominio
{
    /// <summary>
    /// 
    /// </summary>
    public partial class TabPublicacoes : CompontesBasicos.ComponenteTabCampos
    {
        /// <summary>
        /// 
        /// </summary>
        public TabPublicacoes()
        {
            InitializeComponent();            
            panelControl2.Visible = (Framework.usuarioLogado.UsuarioLogadoNeon.UsuarioLogadoSt.VerificaFuncionalidade("CADASTRO") >= 3);            
            TabPublicacoes.VirEnumTipos.CarregaEditorDaGrid(colTIPO);
        }

        

        private cCondominiosCampos2 TabMaeTipada
        {
            get 
            {
                return (cCondominiosCampos2)TabMae;
            }
        }

        /*
        private com.websiteseguro.ssl493.WsPublicacoes _WsP;

        /// <summary>
        /// 
        /// </summary>
        public com.websiteseguro.ssl493.WsPublicacoes WsP
        {
            get
            {
                if (_WsP == null)
                {
                    _WsP = new Cadastros.com.websiteseguro.ssl493.WsPublicacoes();

                    //_WsP.Url = @"http://www.neonimoveis.com.br/neononline2/Wspublicacoes.asmx";

                    _WsP.Proxy = Framework.usuarioLogado.UsuarioLogadoNeon.UsuarioLogadoSt.Proxy;

                    /*
                    if (Framework.DSCentral.EMP == 1)
                    {
                        _WsP.Proxy = new System.Net.WebProxy("10.135.1.20", 8080);
                        _WsP.Proxy.Credentials = new System.Net.NetworkCredential("publicacoes", "neon01");
                    }
                    else
                    {
                        _WsP.Proxy = new System.Net.WebProxy("10.135.1.170", 8080);
                        //_WsP.Proxy.Credentials = new System.Net.NetworkCredential("iris", "neon01");
                    }
                    *//* 
                }
                return _WsP;
            }            
        }
        */
        private int EMP 
        {
            get 
            {
                return (Framework.DSCentral.EMP == 1 ? 1 : 2);
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="Entrando"></param>
        public override void OnTrocaPagina(bool Entrando)
        {
            if (Entrando)
            {
                assembleiasBindingSource.DataSource = TabMaeTipada.dCondominiosCampos;
                //assembleiasBindingSource.DataSource = TabMaeTipada.dCondominiosCampos;
                //assembleiasBindingSource.DataMember = "assembleias";
                TabMaeTipada.dCondominiosCampos.PUBlicacaoTableAdapter.FillByCON(TabMaeTipada.dCondominiosCampos.PUBlicacao, TabMaeTipada.linhaMae.CON);
                simpleButton1.Enabled = true;
                cmdEnviarPorEmail.Enabled = true;
            }              
        }

        

        private void gridView1_DoubleClick(object sender, EventArgs e)
        {
            dCondominiosCampos.PUBlicacaoRow row = (dCondominiosCampos.PUBlicacaoRow)gridView1.GetFocusedDataRow();
            if (row != null)            
                componenteWEB1.Navigate(string.Format(@"http://www.neonimoveis.com.br/publicacoes/{0}{1}", row.PUB, row.PUBExtensao));            
        }

        enum Tipos
        {
            ATA = 0,
            Convocacao = 1,
            Regulamento = 2,
            Convencao = 3,
            Outros = 4
        }

        private ClientWCFFtp.ClientFTP _clientFTP;

        private ClientWCFFtp.ClientFTP clientFTP
        {
            get
            {
                if (_clientFTP == null)
                    _clientFTP = new ClientWCFFtp.ClientFTP(CompontesBasicosProc.FuncoesBasicasProc.EstaEmProducao, Framework.DSCentral.USU, Framework.DSCentral.EMP);
                return _clientFTP;
            }
        }

        private void simpleButton1_Click(object sender, EventArgs e)
        {
            cEditaPublicacoes Editor = new cEditaPublicacoes();
            Editor.XData.Time = DateTime.Now;
            Editor.XArquivo.Visible = true;
            if (Editor.VirShowModulo(CompontesBasicos.EstadosDosComponentes.PopUp) == DialogResult.OK) 
            {
                if ((Editor.XArquivo.Text != null) && (File.Exists(Editor.XArquivo.Text)) && (Editor.XTipo.EditValue != null) && (Editor.XDescricao.Text != null))
                {
                    FileInfo fInfo = new FileInfo(Editor.XArquivo.Text);
                    long numBytes = fInfo.Length;
                    //double dLen = Convert.ToDouble(fInfo.Length / 1000000);
                    if(numBytes > 4000000)
                    {
                        MessageBox.Show("Arquivo muito grande!!!");
                        return;
                    };                    
                    dCondominiosCampos.PUBlicacaoRow novaRow = TabMaeTipada.dCondominiosCampos.PUBlicacao.NewPUBlicacaoRow();
                    novaRow.PUBArquivo = "";
                    novaRow.PUBAtiva = true;
                    novaRow.PUBData = Editor.XData.Time;
                    novaRow.PUBDATAI = DateTime.Now;
                    novaRow.PUBDescritivo = Editor.XDescricao.Text;
                    novaRow.PUBExtensao = fInfo.Extension;
                    novaRow.PUBI_USU = Framework.DSCentral.USU;
                    novaRow.PUBLocal = Editor.XLocao.Text ?? "";
                    novaRow.PUBTipo = (int)Editor.XTipo.EditValue;
                    novaRow.PUB_CODCON = TabMaeTipada.linhaMae.CONCodigo;
                    novaRow.PUB_CON = TabMaeTipada.linhaMae.CON;
                    TabMaeTipada.dCondominiosCampos.PUBlicacao.AddPUBlicacaoRow(novaRow);

                    try
                    {
                        VirMSSQL.TableAdapter.AbreTrasacaoSQL("TabPuclicacoes.cs 161", TabMaeTipada.dCondominiosCampos.PUBlicacaoTableAdapter);
                        TabMaeTipada.dCondominiosCampos.PUBlicacaoTableAdapter.Update(novaRow);
                        novaRow.PUBArquivo = novaRow.PUB.ToString();
                        TabMaeTipada.dCondominiosCampos.PUBlicacaoTableAdapter.Update(novaRow);
                        string ArquivoRemoto = string.Format("{0}{1}{2}",
                                                              CompontesBasicosProc.FuncoesBasicasProc.EstaEmProducao ? @"C:\HostingSpaces\neonimov\neonimoveis.com\wwwroot\Publicacoes\" : @"C:\HostingSpaces\neonimov\neonimoveis.com\PublicacoesH\",
                                                              novaRow.PUB, 
                                                              fInfo.Extension);
                        if (!clientFTP.Put(Editor.XArquivo.Text, ArquivoRemoto,true))
                            throw new Exception(string.Format("Erro na publica��o: {0}", clientFTP.UltimoErro));
                        VirMSSQL.TableAdapter.CommitSQL();
                    }
                    catch (Exception ex)
                    {
                        VirMSSQL.TableAdapter.VircatchSQL(ex);
                    }

                    /*
                    byte[] bdata;
                    using (System.IO.FileStream fStream = new FileStream(Editor.XArquivo.Text, FileMode.Open, FileAccess.Read))
                    {
                        using (BinaryReader br = new BinaryReader(fStream))
                        {
                            bdata = br.ReadBytes((int)numBytes);
                            br.Close();
                            fStream.Close();
                        }
                    };
                    string ext = Path.GetExtension(Editor.XArquivo.Text);
                    short itipo = (short)(Editor.XTipo.EditValue);
                    string resposta = WsP.Nova(TabMaeTipada.linhaMae.CONCodigo,Editor.XData.Time,Editor.XDescricao.Text, EMP, ext,Editor.XLocao.Text,(int)itipo, bdata);
                    if (resposta == "ok")
                    {
                        assembleiasBindingSource.DataSource = WsP.GetdWsPublicacoes(TabMaeTipada.linhaMae.CONCodigo, EMP);
                    }
                    else
                    {
                        MessageBox.Show(resposta);
                    }*/
                }
            }
        }

        private static VirEnum virEnumTipos;

        /// <summary>
        /// VirEnum para Tipos
        /// </summary>
        public static VirEnum VirEnumTipos
        {
            get
            {
                if (virEnumTipos == null)
                {                    
                    virEnumTipos = new VirEnum(typeof(Tipos));
                    //virEnumTipos.UsarShort = true;
                    virEnumTipos.GravaNomes(Tipos.ATA, "ATA");
                    virEnumTipos.GravaNomes(Tipos.Convocacao, "Convoca��o");
                    virEnumTipos.GravaNomes(Tipos.Regulamento, "Regulamento");
                    virEnumTipos.GravaNomes(Tipos.Convencao, "Conven��o");
                    virEnumTipos.GravaNomes(Tipos.Outros, "Outros");
                }
                return virEnumTipos;
            }
        }

        private void simpleButton2_Click(object sender, EventArgs e)
        {
            dCondominiosCampos.PUBlicacaoRow row = (dCondominiosCampos.PUBlicacaoRow)gridView1.GetFocusedDataRow();
            if (row != null)
                if (MessageBox.Show("Confirma", "ATEN��O", MessageBoxButtons.YesNo, MessageBoxIcon.Warning) == DialogResult.Yes)
                {
                    row.PUBAtiva = false;
                    TabMaeTipada.dCondominiosCampos.PUBlicacaoTableAdapter.Update(row);
                }
        }

        private void simpleButton3_Click(object sender, EventArgs e)
        {
            dCondominiosCampos.PUBlicacaoRow row = (dCondominiosCampos.PUBlicacaoRow)gridView1.GetFocusedDataRow();
            if (row != null)
            {
                using (cEditaPublicacoes Editor = new cEditaPublicacoes())
                {
                    Editor.XData.Time = row.PUBData;
                    Editor.XDescricao.Text = row.PUBDescritivo;
                    Editor.XLocao.Text = row.PUBLocal;
                    Editor.XTipo.EditValue = row.PUBTipo;
                    Editor.XArquivo.Visible = false;
                    if (Editor.VirShowModulo(CompontesBasicos.EstadosDosComponentes.PopUp) == DialogResult.OK)
                    {
                        row.PUBAtiva = false;
                        TabMaeTipada.dCondominiosCampos.PUBlicacaoTableAdapter.Update(row);
                    }
                }
            }
        }

        //*** MRC - INICIO (05/01/2015 10:00) ***
        private void cmdEnviarPorEmail_Click(object sender, EventArgs e)
        {
            //Pega a publicacao
            dCondominiosCampos.PUBlicacaoRow rowPublicacao = (dCondominiosCampos.PUBlicacaoRow)gridView1.GetFocusedDataRow();
            if (rowPublicacao == null)
                return;
            else
                if (MessageBox.Show(string.Format("Confirma o envio por e-mail da publica��o '{0}'?", rowPublicacao.PUBDescritivo), "ATEN��O", MessageBoxButtons.YesNo, MessageBoxIcon.Warning) != DialogResult.Yes)
                    return;

            //Copia o arquivo localmente
            string Path = Application.StartupPath + @"\TMP\";
            string ImpressoUnico = string.Format("{0}Mala Direta.pdf", Path);
            if (!Directory.Exists(Path))
                Directory.CreateDirectory(Path);
            if (File.Exists(ImpressoUnico))
                try
                { File.Delete(ImpressoUnico); }
                catch
                { ImpressoUnico = String.Format("{0}Mala Direta_{1:ddMMyyhhmmss}.pdf", Path, DateTime.Now); };
            System.Threading.Thread.Sleep(3000);
            using (WebClient webClient = new WebClient())
                try
                {
                    webClient.DownloadFile(string.Format(@"http://www.neonimoveis.com.br/publicacoes/{0}{1}", rowPublicacao.PUB, rowPublicacao.PUBExtensao), ImpressoUnico);
                }
                catch (Exception)
                {
                    MessageBox.Show("N�o foi poss�vel copiar o arquivo de publica��o.");
                    return;
                }
                

            //Pega os proprietarios
            dDestinatarios dDestProp = new dDestinatarios();
            dDestProp.DadosDestinatariosTableAdapter.FillProp(dDestProp.DadosDestinatarios, TabMaeTipada.linhaMae.CON);

            //Pega os inquilinos
            dDestinatarios dDestInq = new dDestinatarios();
            dDestInq.DadosDestinatariosTableAdapter.FillInq(dDestInq.DadosDestinatarios, TabMaeTipada.linhaMae.CON);

            //Inicia dados do envio
            bool booFaltaEmail = false;
            string ConteudoEmail = "Segue documento em anexo.\r\n" +
                                    "\r\n" +
                                    "Atenciosamente,\r\n" +
                                    "Neon Im�veis\r\n" +
                                    "\r\n" +
                                    "\"Este e-mail foi enviado automaticamente. Favor n�o responder.\"";

            //Envia os emails
            using (CompontesBasicos.Espera.cEspera Esp = new CompontesBasicos.Espera.cEspera(this))
            {
                Esp.Espere("Enviando e-mails. Aguarde ...");
                Esp.AtivaGauge(dDestProp.DadosDestinatarios.Count + dDestInq.DadosDestinatarios.Count);
                Application.DoEvents();
                int i = 0;
                DateTime prox = DateTime.Now.AddSeconds(3);

                //Envia para proprietarios 
                foreach (dDestinatarios.DadosDestinatariosRow row in dDestProp.DadosDestinatarios)
                {
                    i++;
                    if (DateTime.Now > prox)
                    {
                        prox = DateTime.Now.AddSeconds(3);
                        Esp.Gauge(i);
                    }

                    if (!row.IsPESEmailNull())
                        if (row.PESEmail != "")
                            VirEmailNeon.EmailDiretoNeon.EmalST.Enviar(VirEmailNeon.EmailDiretoNeon.FormaEnvio.ViaServidor, row.PESEmail, ConteudoEmail, rowPublicacao.PUBDescritivo, new Attachment(ImpressoUnico));
                        else booFaltaEmail = true;
                    else booFaltaEmail = true;
                }

                //Envia para inquilinos
                foreach (dDestinatarios.DadosDestinatariosRow row in dDestInq.DadosDestinatarios)
                {
                    i++;
                    if (DateTime.Now > prox)
                    {
                        prox = DateTime.Now.AddSeconds(3);
                        Esp.Gauge(i);
                    }

                    if (!row.IsPESEmailNull())
                        if (row.PESEmail != "")
                            VirEmailNeon.EmailDiretoNeon.EmalST.Enviar(VirEmailNeon.EmailDiretoNeon.FormaEnvio.ViaServidor, row.PESEmail, ConteudoEmail, rowPublicacao.PUBDescritivo, new Attachment(ImpressoUnico));
                        else booFaltaEmail = true;
                    else booFaltaEmail = true;
                }

                Esp.Gauge(dDestProp.DadosDestinatarios.Count + dDestInq.DadosDestinatarios.Count);
                System.Threading.Thread.Sleep(3000);
            }

            if (booFaltaEmail) MessageBox.Show("ATEN��O: Envio de e-mail finalizado. Existem unidades que n�o possuem e-mail cadastrado, providenciar o envio manualmente.");
            else MessageBox.Show("Envio de e-mail finalizado.");
        }
        //*** MRC - TERMINO (05/01/2015 10:00) ***
    }

}

