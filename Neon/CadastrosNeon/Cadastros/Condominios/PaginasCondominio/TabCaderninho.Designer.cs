namespace Cadastros.Condominios.PaginasCondominio
{
    partial class TabCaderninho
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(TabCaderninho));
            this.dCaderninho = new Cadastros.Condominios.PaginasCondominio.dCaderninho();
            this.cADernoEletronicoBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.cADernoEletronicoGridControl = new DevExpress.XtraGrid.GridControl();
            this.layoutView1 = new DevExpress.XtraGrid.Views.Layout.LayoutView();
            this.colCADCompetenciaMes = new DevExpress.XtraGrid.Columns.LayoutViewColumn();
            this.colCADCompetenciaAno = new DevExpress.XtraGrid.Columns.LayoutViewColumn();
            this.colCADTexto = new DevExpress.XtraGrid.Columns.LayoutViewColumn();
            this.repositoryItemMemoEdit1 = new DevExpress.XtraEditors.Repository.RepositoryItemMemoEdit();
            this.colCADDATAI = new DevExpress.XtraGrid.Columns.LayoutViewColumn();
            this.colCADI_USU = new DevExpress.XtraGrid.Columns.LayoutViewColumn();
            this.LookUpEditUSU = new DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit();
            this.UsuariosBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.repositoryItemMemoExEdit1 = new DevExpress.XtraEditors.Repository.RepositoryItemMemoExEdit();
            this.gridView2 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.barManager1 = new DevExpress.XtraBars.BarManager(this.components);
            this.bar3 = new DevExpress.XtraBars.Bar();
            this.barButtonItem1 = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonItem2 = new DevExpress.XtraBars.BarButtonItem();
            this.barDockControlTop = new DevExpress.XtraBars.BarDockControl();
            this.barDockControlBottom = new DevExpress.XtraBars.BarDockControl();
            this.barDockControlLeft = new DevExpress.XtraBars.BarDockControl();
            this.barDockControlRight = new DevExpress.XtraBars.BarDockControl();
            this.layoutViewCard1 = new DevExpress.XtraGrid.Views.Layout.LayoutViewCard();
            this.Item1 = new DevExpress.XtraGrid.Views.Layout.LayoutViewField();
            this.Item2 = new DevExpress.XtraGrid.Views.Layout.LayoutViewField();
            this.Item3 = new DevExpress.XtraGrid.Views.Layout.LayoutViewField();
            this.Item4 = new DevExpress.XtraGrid.Views.Layout.LayoutViewField();
            this.Item5 = new DevExpress.XtraGrid.Views.Layout.LayoutViewField();
            ((System.ComponentModel.ISupportInitialize)(this.StyleController_F)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dCaderninho)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cADernoEletronicoBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cADernoEletronicoGridControl)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutView1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoEdit1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LookUpEditUSU)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.UsuariosBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoExEdit1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.barManager1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutViewCard1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Item1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Item2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Item3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Item4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Item5)).BeginInit();
            this.SuspendLayout();
            // 
            // ToolTipController_F
            // 
            this.ToolTipController_F.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.ToolTipController_F.Appearance.ForeColor = System.Drawing.Color.Black;
            this.ToolTipController_F.Appearance.Options.UseBackColor = true;
            this.ToolTipController_F.Appearance.Options.UseFont = true;
            this.ToolTipController_F.Appearance.Options.UseForeColor = true;
            this.ToolTipController_F.Appearance.Options.UseTextOptions = true;
            this.ToolTipController_F.Appearance.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            // 
            // StyleController_F
            // 
            this.StyleController_F.AppearanceDisabled.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this.StyleController_F.AppearanceDisabled.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this.StyleController_F.AppearanceDisabled.Options.UseBackColor = true;
            this.StyleController_F.AppearanceDisabled.Options.UseForeColor = true;
            this.StyleController_F.AppearanceFocused.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.StyleController_F.AppearanceFocused.Options.UseBackColor = true;
         
            // 
            // dCaderninho
            // 
            this.dCaderninho.DataSetName = "dCaderninho";
            this.dCaderninho.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // cADernoEletronicoBindingSource
            // 
            this.cADernoEletronicoBindingSource.DataMember = "CADernoEletronico";
            this.cADernoEletronicoBindingSource.DataSource = this.dCaderninho;
            // 
            // cADernoEletronicoGridControl
            // 
            this.cADernoEletronicoGridControl.DataSource = this.cADernoEletronicoBindingSource;
            this.cADernoEletronicoGridControl.Dock = System.Windows.Forms.DockStyle.Fill;
            this.cADernoEletronicoGridControl.EmbeddedNavigator.Buttons.Append.Visible = false;
            this.cADernoEletronicoGridControl.EmbeddedNavigator.Buttons.CancelEdit.Visible = false;
            this.cADernoEletronicoGridControl.EmbeddedNavigator.Buttons.Edit.Visible = false;
            this.cADernoEletronicoGridControl.EmbeddedNavigator.Buttons.EndEdit.Visible = false;
            this.cADernoEletronicoGridControl.EmbeddedNavigator.Buttons.Remove.Visible = false;
            this.cADernoEletronicoGridControl.Location = new System.Drawing.Point(0, 0);
            this.cADernoEletronicoGridControl.MainView = this.layoutView1;
            this.cADernoEletronicoGridControl.Name = "cADernoEletronicoGridControl";
            this.cADernoEletronicoGridControl.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemMemoEdit1,
            this.LookUpEditUSU,
            this.repositoryItemMemoExEdit1});
            this.cADernoEletronicoGridControl.Size = new System.Drawing.Size(1253, 592);
            this.cADernoEletronicoGridControl.TabIndex = 2;
            this.cADernoEletronicoGridControl.UseEmbeddedNavigator = true;
            this.cADernoEletronicoGridControl.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.layoutView1,
            this.gridView2});
            // 
            // layoutView1
            // 
            this.layoutView1.Appearance.CardCaption.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(193)))), ((int)(((byte)(95)))));
            this.layoutView1.Appearance.CardCaption.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(193)))), ((int)(((byte)(95)))));
            this.layoutView1.Appearance.CardCaption.ForeColor = System.Drawing.Color.Black;
            this.layoutView1.Appearance.CardCaption.Options.UseBackColor = true;
            this.layoutView1.Appearance.CardCaption.Options.UseBorderColor = true;
            this.layoutView1.Appearance.CardCaption.Options.UseForeColor = true;
            this.layoutView1.Appearance.FieldCaption.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(248)))), ((int)(((byte)(248)))), ((int)(((byte)(248)))));
            this.layoutView1.Appearance.FieldCaption.ForeColor = System.Drawing.Color.Black;
            this.layoutView1.Appearance.FieldCaption.Options.UseBackColor = true;
            this.layoutView1.Appearance.FieldCaption.Options.UseForeColor = true;
            this.layoutView1.Appearance.FieldValue.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(238)))), ((int)(((byte)(238)))), ((int)(((byte)(238)))));
            this.layoutView1.Appearance.FieldValue.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(238)))), ((int)(((byte)(238)))), ((int)(((byte)(238)))));
            this.layoutView1.Appearance.FieldValue.ForeColor = System.Drawing.Color.Black;
            this.layoutView1.Appearance.FieldValue.Options.UseBackColor = true;
            this.layoutView1.Appearance.FieldValue.Options.UseBorderColor = true;
            this.layoutView1.Appearance.FieldValue.Options.UseForeColor = true;
            this.layoutView1.Appearance.FilterCloseButton.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(243)))), ((int)(((byte)(174)))), ((int)(((byte)(66)))));
            this.layoutView1.Appearance.FilterCloseButton.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(243)))), ((int)(((byte)(174)))), ((int)(((byte)(66)))));
            this.layoutView1.Appearance.FilterCloseButton.ForeColor = System.Drawing.Color.Black;
            this.layoutView1.Appearance.FilterCloseButton.Options.UseBackColor = true;
            this.layoutView1.Appearance.FilterCloseButton.Options.UseBorderColor = true;
            this.layoutView1.Appearance.FilterCloseButton.Options.UseForeColor = true;
            this.layoutView1.Appearance.FilterPanel.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(238)))), ((int)(((byte)(238)))), ((int)(((byte)(238)))));
            this.layoutView1.Appearance.FilterPanel.BackColor2 = System.Drawing.Color.White;
            this.layoutView1.Appearance.FilterPanel.ForeColor = System.Drawing.Color.Black;
            this.layoutView1.Appearance.FilterPanel.Options.UseBackColor = true;
            this.layoutView1.Appearance.FilterPanel.Options.UseForeColor = true;
            this.layoutView1.Appearance.FocusedCardCaption.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(194)))), ((int)(((byte)(194)))), ((int)(((byte)(194)))));
            this.layoutView1.Appearance.FocusedCardCaption.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(194)))), ((int)(((byte)(194)))), ((int)(((byte)(194)))));
            this.layoutView1.Appearance.FocusedCardCaption.ForeColor = System.Drawing.Color.Black;
            this.layoutView1.Appearance.FocusedCardCaption.Options.UseBackColor = true;
            this.layoutView1.Appearance.FocusedCardCaption.Options.UseBorderColor = true;
            this.layoutView1.Appearance.FocusedCardCaption.Options.UseForeColor = true;
            this.layoutView1.Appearance.HideSelectionCardCaption.BackColor = System.Drawing.Color.Gainsboro;
            this.layoutView1.Appearance.HideSelectionCardCaption.BorderColor = System.Drawing.Color.Gainsboro;
            this.layoutView1.Appearance.HideSelectionCardCaption.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(100)))), ((int)(((byte)(100)))), ((int)(((byte)(100)))));
            this.layoutView1.Appearance.HideSelectionCardCaption.Options.UseBackColor = true;
            this.layoutView1.Appearance.HideSelectionCardCaption.Options.UseBorderColor = true;
            this.layoutView1.Appearance.HideSelectionCardCaption.Options.UseForeColor = true;
            this.layoutView1.Appearance.SelectedCardCaption.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(215)))), ((int)(((byte)(215)))), ((int)(((byte)(215)))));
            this.layoutView1.Appearance.SelectedCardCaption.ForeColor = System.Drawing.Color.Black;
            this.layoutView1.Appearance.SelectedCardCaption.Options.UseBackColor = true;
            this.layoutView1.Appearance.SelectedCardCaption.Options.UseForeColor = true;
            this.layoutView1.Appearance.SeparatorLine.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(175)))), ((int)(((byte)(175)))), ((int)(((byte)(175)))));
            this.layoutView1.Appearance.SeparatorLine.Options.UseBackColor = true;
            this.layoutView1.Appearance.ViewBackground.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(238)))), ((int)(((byte)(238)))), ((int)(((byte)(238)))));
            this.layoutView1.Appearance.ViewBackground.BackColor2 = System.Drawing.Color.White;
            this.layoutView1.Appearance.ViewBackground.Options.UseBackColor = true;
            this.layoutView1.CardCaptionFormat = "Record N {0}";
            this.layoutView1.CardMinSize = new System.Drawing.Size(530, 230);
            this.layoutView1.Columns.AddRange(new DevExpress.XtraGrid.Columns.LayoutViewColumn[] {
            this.colCADCompetenciaMes,
            this.colCADCompetenciaAno,
            this.colCADTexto,
            this.colCADDATAI,
            this.colCADI_USU});
            this.layoutView1.GridControl = this.cADernoEletronicoGridControl;
            this.layoutView1.Name = "layoutView1";
            this.layoutView1.OptionsBehavior.ScrollVisibility = DevExpress.XtraGrid.Views.Base.ScrollVisibility.Auto;
            this.layoutView1.OptionsItemText.AlignMode = DevExpress.XtraGrid.Views.Layout.FieldTextAlignMode.AutoSize;
            this.layoutView1.OptionsMultiRecordMode.MultiRowScrollBarOrientation = DevExpress.XtraGrid.Views.Layout.ScrollBarOrientation.Horizontal;
            this.layoutView1.OptionsMultiRecordMode.StretchCardToViewHeight = true;
            this.layoutView1.OptionsMultiRecordMode.StretchCardToViewWidth = true;
            this.layoutView1.OptionsView.CardArrangeRule = DevExpress.XtraGrid.Views.Layout.LayoutCardArrangeRule.AllowPartialCards;
            this.layoutView1.OptionsView.ShowCardCaption = false;
            this.layoutView1.OptionsView.ShowCardExpandButton = false;
            this.layoutView1.OptionsView.ShowHeaderPanel = false;
            this.layoutView1.OptionsView.ViewMode = DevExpress.XtraGrid.Views.Layout.LayoutViewMode.Column;
            this.layoutView1.SortInfo.AddRange(new DevExpress.XtraGrid.Columns.GridColumnSortInfo[] {
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colCADCompetenciaAno, DevExpress.Data.ColumnSortOrder.Ascending),
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colCADCompetenciaMes, DevExpress.Data.ColumnSortOrder.Ascending)});
            this.layoutView1.TemplateCard = this.layoutViewCard1;
            // 
            // colCADCompetenciaMes
            // 
            this.colCADCompetenciaMes.AppearanceCell.BackColor = System.Drawing.Color.Red;
            this.colCADCompetenciaMes.AppearanceCell.BackColor2 = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(0)))));
            this.colCADCompetenciaMes.AppearanceCell.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            this.colCADCompetenciaMes.AppearanceCell.ForeColor = System.Drawing.Color.Yellow;
            this.colCADCompetenciaMes.AppearanceCell.Options.UseBackColor = true;
            this.colCADCompetenciaMes.AppearanceCell.Options.UseFont = true;
            this.colCADCompetenciaMes.AppearanceCell.Options.UseForeColor = true;
            this.colCADCompetenciaMes.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 14.25F, System.Drawing.FontStyle.Bold);
            this.colCADCompetenciaMes.AppearanceHeader.Options.UseFont = true;
            this.colCADCompetenciaMes.Caption = "M�s";
            this.colCADCompetenciaMes.FieldName = "CADCompetenciaMes";
            this.colCADCompetenciaMes.LayoutViewField = this.Item3;
            this.colCADCompetenciaMes.Name = "colCADCompetenciaMes";
            this.colCADCompetenciaMes.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.False;
            this.colCADCompetenciaMes.OptionsColumn.FixedWidth = true;
            // 
            // colCADCompetenciaAno
            // 
            this.colCADCompetenciaAno.AppearanceCell.BackColor = System.Drawing.Color.Red;
            this.colCADCompetenciaAno.AppearanceCell.BackColor2 = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(0)))));
            this.colCADCompetenciaAno.AppearanceCell.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            this.colCADCompetenciaAno.AppearanceCell.ForeColor = System.Drawing.Color.Yellow;
            this.colCADCompetenciaAno.AppearanceCell.Options.UseBackColor = true;
            this.colCADCompetenciaAno.AppearanceCell.Options.UseFont = true;
            this.colCADCompetenciaAno.AppearanceCell.Options.UseForeColor = true;
            this.colCADCompetenciaAno.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 14.25F, System.Drawing.FontStyle.Bold);
            this.colCADCompetenciaAno.AppearanceHeader.Options.UseFont = true;
            this.colCADCompetenciaAno.Caption = "Ano";
            this.colCADCompetenciaAno.FieldName = "CADCompetenciaAno";
            this.colCADCompetenciaAno.LayoutViewField = this.Item1;
            this.colCADCompetenciaAno.Name = "colCADCompetenciaAno";
            this.colCADCompetenciaAno.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.False;
            this.colCADCompetenciaAno.OptionsColumn.FixedWidth = true;
            // 
            // colCADTexto
            // 
            this.colCADTexto.AppearanceCell.Font = new System.Drawing.Font("Courier New", 8.25F, System.Drawing.FontStyle.Bold);
            this.colCADTexto.AppearanceCell.Options.UseFont = true;
            this.colCADTexto.Caption = "Texto";
            this.colCADTexto.ColumnEdit = this.repositoryItemMemoEdit1;
            this.colCADTexto.FieldName = "CADTexto";
            this.colCADTexto.LayoutViewField = this.Item2;
            this.colCADTexto.Name = "colCADTexto";
            this.colCADTexto.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.False;
            this.colCADTexto.OptionsColumn.ReadOnly = true;
            this.colCADTexto.OptionsColumn.ShowCaption = false;
            this.colCADTexto.OptionsFilter.AllowFilter = false;
            // 
            // repositoryItemMemoEdit1
            // 
            this.repositoryItemMemoEdit1.Appearance.Font = new System.Drawing.Font("Courier New", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.repositoryItemMemoEdit1.Appearance.Options.UseFont = true;
            this.repositoryItemMemoEdit1.Name = "repositoryItemMemoEdit1";
            // 
            // colCADDATAI
            // 
            this.colCADDATAI.Caption = "Gerado";
            this.colCADDATAI.FieldName = "CADDATAI";
            this.colCADDATAI.LayoutViewField = this.Item4;
            this.colCADDATAI.Name = "colCADDATAI";
            this.colCADDATAI.OptionsColumn.FixedWidth = true;
            // 
            // colCADI_USU
            // 
            this.colCADI_USU.Caption = "Operador";
            this.colCADI_USU.ColumnEdit = this.LookUpEditUSU;
            this.colCADI_USU.FieldName = "CADI_USU";
            this.colCADI_USU.LayoutViewField = this.Item5;
            this.colCADI_USU.Name = "colCADI_USU";
            // 
            // LookUpEditUSU
            // 
            this.LookUpEditUSU.AutoHeight = false;
            this.LookUpEditUSU.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LookUpEditUSU.DataSource = this.UsuariosBindingSource;
            this.LookUpEditUSU.DisplayMember = "USUNome";
            this.LookUpEditUSU.Name = "LookUpEditUSU";
            this.LookUpEditUSU.NullText = " --";
            this.LookUpEditUSU.ValueMember = "USU";
            // 
            // UsuariosBindingSource
            // 
            this.UsuariosBindingSource.DataSource = typeof(FrameworkProc.datasets.dUSUarios.USUariosDataTable);
            // 
            // repositoryItemMemoExEdit1
            // 
            this.repositoryItemMemoExEdit1.AutoHeight = false;
            this.repositoryItemMemoExEdit1.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemMemoExEdit1.Name = "repositoryItemMemoExEdit1";
            this.repositoryItemMemoExEdit1.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.repositoryItemMemoExEdit1.ShowIcon = false;
            // 
            // gridView2
            // 
            this.gridView2.GridControl = this.cADernoEletronicoGridControl;
            this.gridView2.Name = "gridView2";
            // 
            // barManager1
            // 
            this.barManager1.Bars.AddRange(new DevExpress.XtraBars.Bar[] {
            this.bar3});
            this.barManager1.DockControls.Add(this.barDockControlTop);
            this.barManager1.DockControls.Add(this.barDockControlBottom);
            this.barManager1.DockControls.Add(this.barDockControlLeft);
            this.barManager1.DockControls.Add(this.barDockControlRight);
            this.barManager1.Form = this;
            this.barManager1.Items.AddRange(new DevExpress.XtraBars.BarItem[] {
            this.barButtonItem1,
            this.barButtonItem2});
            this.barManager1.MaxItemId = 2;
            this.barManager1.StatusBar = this.bar3;
            // 
            // bar3
            // 
            this.bar3.BarName = "Status bar";
            this.bar3.CanDockStyle = DevExpress.XtraBars.BarCanDockStyle.Bottom;
            this.bar3.DockCol = 0;
            this.bar3.DockRow = 0;
            this.bar3.DockStyle = DevExpress.XtraBars.BarDockStyle.Bottom;
            this.bar3.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(this.barButtonItem1),
            new DevExpress.XtraBars.LinkPersistInfo(this.barButtonItem2)});
            this.bar3.OptionsBar.AllowQuickCustomization = false;
            this.bar3.OptionsBar.DrawDragBorder = false;
            this.bar3.OptionsBar.UseWholeRow = true;
            this.bar3.Text = "Status bar";
            // 
            // barButtonItem1
            // 
            this.barButtonItem1.Border = DevExpress.XtraEditors.Controls.BorderStyles.Simple;
            this.barButtonItem1.Caption = "Incluir";
            this.barButtonItem1.Glyph = global::Cadastros.Properties.Resources.BtnIncluir_F_Glyph;
            this.barButtonItem1.Id = 0;
            this.barButtonItem1.Name = "barButtonItem1";
            this.barButtonItem1.PaintStyle = DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph;
            this.barButtonItem1.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.barButtonItem1_ItemClick);
            // 
            // barButtonItem2
            // 
            this.barButtonItem2.Border = DevExpress.XtraEditors.Controls.BorderStyles.Simple;
            this.barButtonItem2.Caption = "Pr�xima emiss�o";
            this.barButtonItem2.Glyph = global::Cadastros.Properties.Resources.BtnIncluirProx_Glyph;
            this.barButtonItem2.Id = 1;
            this.barButtonItem2.Name = "barButtonItem2";
            this.barButtonItem2.PaintStyle = DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph;
            this.barButtonItem2.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.barButtonItem2_ItemClick);
            // 
            // barDockControlTop
            // 
            this.barDockControlTop.CausesValidation = false;
            this.barDockControlTop.Dock = System.Windows.Forms.DockStyle.Top;
            this.barDockControlTop.Location = new System.Drawing.Point(0, 0);
            this.barDockControlTop.Size = new System.Drawing.Size(1253, 0);
            // 
            // barDockControlBottom
            // 
            this.barDockControlBottom.CausesValidation = false;
            this.barDockControlBottom.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.barDockControlBottom.Location = new System.Drawing.Point(0, 592);
            this.barDockControlBottom.Size = new System.Drawing.Size(1253, 29);
            // 
            // barDockControlLeft
            // 
            this.barDockControlLeft.CausesValidation = false;
            this.barDockControlLeft.Dock = System.Windows.Forms.DockStyle.Left;
            this.barDockControlLeft.Location = new System.Drawing.Point(0, 0);
            this.barDockControlLeft.Size = new System.Drawing.Size(0, 592);
            // 
            // barDockControlRight
            // 
            this.barDockControlRight.CausesValidation = false;
            this.barDockControlRight.Dock = System.Windows.Forms.DockStyle.Right;
            this.barDockControlRight.Location = new System.Drawing.Point(1253, 0);
            this.barDockControlRight.Size = new System.Drawing.Size(0, 592);
            // 
            // layoutViewCard1
            // 
            this.layoutViewCard1.CustomizationFormText = "layoutViewTemplateCard";
            this.layoutViewCard1.HeaderButtonsLocation = DevExpress.Utils.GroupElementLocation.AfterText;
            this.layoutViewCard1.GroupBordersVisible = false;
            this.layoutViewCard1.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.Item1,
            this.Item2,
            this.Item3,
            this.Item4,
            this.Item5});
            this.layoutViewCard1.Name = "layoutViewCard1";
            this.layoutViewCard1.OptionsItemText.TextToControlDistance = 5;
            this.layoutViewCard1.Padding = new DevExpress.XtraLayout.Utils.Padding(2, 2, 2, 2);
            this.layoutViewCard1.Text = "TemplateCard";
            // 
            // Item1
            // 
            this.Item1.EditorPreferredWidth = 39;
            this.Item1.Location = new System.Drawing.Point(0, 0);
            this.Item1.Name = "Item1";
            this.Item1.Size = new System.Drawing.Size(77, 26);
            this.Item1.Spacing = new DevExpress.XtraLayout.Utils.Padding(3, 3, 3, 3);
            this.Item1.TextSize = new System.Drawing.Size(23, 13);
            this.Item1.TextToControlDistance = 5;
            // 
            // Item2
            // 
            this.Item2.EditorPreferredWidth = 483;
            this.Item2.Location = new System.Drawing.Point(0, 26);
            this.Item2.MaxSize = new System.Drawing.Size(0, 1500);
            this.Item2.MinSize = new System.Drawing.Size(67, 28);
            this.Item2.Name = "Item2";
            this.Item2.Size = new System.Drawing.Size(530, 200);
            this.Item2.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.Item2.Spacing = new DevExpress.XtraLayout.Utils.Padding(3, 3, 3, 3);
            this.Item2.TextSize = new System.Drawing.Size(32, 13);
            this.Item2.TextToControlDistance = 5;
            // 
            // Item3
            // 
            this.Item3.EditorPreferredWidth = 27;
            this.Item3.Location = new System.Drawing.Point(77, 0);
            this.Item3.Name = "Item3";
            this.Item3.Size = new System.Drawing.Size(65, 26);
            this.Item3.Spacing = new DevExpress.XtraLayout.Utils.Padding(3, 3, 3, 3);
            this.Item3.TextSize = new System.Drawing.Size(23, 13);
            this.Item3.TextToControlDistance = 5;
            // 
            // Item4
            // 
            this.Item4.EditorPreferredWidth = 128;
            this.Item4.Location = new System.Drawing.Point(142, 0);
            this.Item4.Name = "Item4";
            this.Item4.Size = new System.Drawing.Size(182, 26);
            this.Item4.Spacing = new DevExpress.XtraLayout.Utils.Padding(3, 3, 3, 3);
            this.Item4.TextSize = new System.Drawing.Size(39, 13);
            this.Item4.TextToControlDistance = 5;
            // 
            // Item5
            // 
            this.Item5.EditorPreferredWidth = 141;
            this.Item5.Location = new System.Drawing.Point(324, 0);
            this.Item5.Name = "Item5";
            this.Item5.Size = new System.Drawing.Size(206, 26);
            this.Item5.Spacing = new DevExpress.XtraLayout.Utils.Padding(3, 3, 3, 3);
            this.Item5.TextSize = new System.Drawing.Size(50, 13);
            this.Item5.TextToControlDistance = 5;
            // 
            // TabCaderninho
            // 
            this.Controls.Add(this.cADernoEletronicoGridControl);
            this.Controls.Add(this.barDockControlLeft);
            this.Controls.Add(this.barDockControlRight);
            this.Controls.Add(this.barDockControlBottom);
            this.Controls.Add(this.barDockControlTop);
            this.Doc = System.Windows.Forms.DockStyle.Fill;

            this.LookAndFeel.UseDefaultLookAndFeel = false;
            this.Name = "TabCaderninho";
            this.Size = new System.Drawing.Size(1253, 621);
            this.Titulo = "Caderninho";
            ((System.ComponentModel.ISupportInitialize)(this.StyleController_F)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dCaderninho)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cADernoEletronicoBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cADernoEletronicoGridControl)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutView1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoEdit1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LookUpEditUSU)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.UsuariosBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoExEdit1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.barManager1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutViewCard1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Item1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Item2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Item3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Item4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Item5)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private dCaderninho dCaderninho;
        private System.Windows.Forms.BindingSource cADernoEletronicoBindingSource;
        private DevExpress.XtraGrid.GridControl cADernoEletronicoGridControl;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView2;
        private DevExpress.XtraEditors.Repository.RepositoryItemMemoEdit repositoryItemMemoEdit1;
        private DevExpress.XtraGrid.Views.Layout.LayoutView layoutView1;
        private DevExpress.XtraGrid.Columns.LayoutViewColumn colCADCompetenciaMes;
        private DevExpress.XtraGrid.Columns.LayoutViewColumn colCADCompetenciaAno;
        private DevExpress.XtraGrid.Columns.LayoutViewColumn colCADTexto;
        private DevExpress.XtraGrid.Columns.LayoutViewColumn colCADDATAI;
        private DevExpress.XtraGrid.Columns.LayoutViewColumn colCADI_USU;
        private DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit LookUpEditUSU;
        private System.Windows.Forms.BindingSource UsuariosBindingSource;
        private DevExpress.XtraBars.BarManager barManager1;
        private DevExpress.XtraBars.Bar bar3;
        private DevExpress.XtraBars.BarDockControl barDockControlTop;
        private DevExpress.XtraBars.BarDockControl barDockControlBottom;
        private DevExpress.XtraBars.BarDockControl barDockControlLeft;
        private DevExpress.XtraBars.BarDockControl barDockControlRight;
        private DevExpress.XtraBars.BarButtonItem barButtonItem1;
        private DevExpress.XtraBars.BarButtonItem barButtonItem2;
        private DevExpress.XtraEditors.Repository.RepositoryItemMemoExEdit repositoryItemMemoExEdit1;
        private DevExpress.XtraGrid.Views.Layout.LayoutViewField Item3;
        private DevExpress.XtraGrid.Views.Layout.LayoutViewField Item1;
        private DevExpress.XtraGrid.Views.Layout.LayoutViewField Item2;
        private DevExpress.XtraGrid.Views.Layout.LayoutViewField Item4;
        private DevExpress.XtraGrid.Views.Layout.LayoutViewField Item5;
        private DevExpress.XtraGrid.Views.Layout.LayoutViewCard layoutViewCard1;
    }
}
