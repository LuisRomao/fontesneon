using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace Cadastros.Condominios.PaginasCondominio
{
    /// <summary>
    /// 
    /// </summary>
    public partial class TabCaderninho : CompontesBasicos.ComponenteTabCampos
    {
        /// <summary>
        /// Construtor
        /// </summary>
        public TabCaderninho()
        {
            InitializeComponent();            
        }

        /// <summary>
        /// Troca de p�gina
        /// </summary>
        /// <param name="Entrando"></param>
        public override void OnTrocaPagina(bool Entrando)
        {
            if (Entrando)
            {
                UsuariosBindingSource.DataSource = FrameworkProc.datasets.dUSUarios.dUSUariosSt.USUarios;
                dCaderninho.CADernoEletronicoTableAdapter.Fill(dCaderninho.CADernoEletronico, TabMae.pk);
                layoutView1.MoveLast();
            }
        }

        dCaderninho.CADernoEletronicoRow LinhaMae {
            get {
                DataRowView DRV = (DataRowView)layoutView1.GetRow(layoutView1.FocusedRowHandle);
                if ((DRV == null) || (DRV.Row == null))
                    return null;
                return (dCaderninho.CADernoEletronicoRow)DRV.Row;
            }
        }

        private string Separador = 
"\r\n"+
"---------------------------------------"+
"**  INCLUS�ES FEITAS AP�S A EMISS�O  **"+
"---------------------------------------";


        private void barButtonItem1_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            if (LinhaMae == null)
                return;
            string inclusao = "";
            if (VirInput.Input.Execute("Texto:", ref inclusao, true) && (inclusao.Trim() != ""))
            {
                if (!LinhaMae.CADTexto.Contains(Separador))
                {
                    Framework.objetosNeon.Competencia CompMae = new Framework.objetosNeon.Competencia(LinhaMae.CADCompetenciaMes, LinhaMae.CADCompetenciaAno);
                    if(CompMae <= Framework.objetosNeon.Competencia.Ultima(TabMae.pk))
                           LinhaMae.CADTexto += Separador;
                };
                LinhaMae.CADTexto += TituloAutor + inclusao.Trim();
                LinhaMae.EndEdit();
                dCaderninho.CADernoEletronicoTableAdapter.Update(LinhaMae);
                LinhaMae.AcceptChanges();
                layoutView1.Refresh();
            }                
        }

        private string TituloAutor
        {
            get
            {
                return "\r\n\r\nInclu�do por " + Framework.usuarioLogado.UsuarioLogadoNeon.UsuarioLogadoStNEON.USUNome + " (" + DateTime.Now.ToString() + "):\r\n";
            }
        }

        private void barButtonItem2_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            string inclusao = "";
            Framework.objetosNeon.Competencia UltimaCompetencia = Framework.objetosNeon.Competencia.Ultima(TabMae.pk);
            UltimaCompetencia++;
            if (VirInput.Input.Execute(UltimaCompetencia.ToString(), ref inclusao, true) && (inclusao.Trim() != ""))
            {                                    
                    
                    dCaderninho.CADernoEletronicoRow NovaLinha = null;
                    foreach(dCaderninho.CADernoEletronicoRow RowBusca in dCaderninho.CADernoEletronico)
                        if((UltimaCompetencia.Ano == RowBusca.CADCompetenciaAno) && (UltimaCompetencia.Mes == RowBusca.CADCompetenciaMes)){
                            NovaLinha = RowBusca;
                            break;
                        }
                    string TextoOrig = "";
                    if(NovaLinha == null)
                    {
                        NovaLinha = dCaderninho.CADernoEletronico.NewCADernoEletronicoRow();
                        NovaLinha.CAD_CON = TabMae.pk;
                        NovaLinha.CADI_USU = Framework.usuarioLogado.UsuarioLogadoNeon.UsuarioLogadoStNEON.USU;
                        NovaLinha.CADCompetenciaAno = (Int16)UltimaCompetencia.Ano;
                        NovaLinha.CADCompetenciaMes = (Int16)UltimaCompetencia.Mes;
                        NovaLinha.CADDATAI = DateTime.Now;
                    }
                    else{
                        NovaLinha.CADA_USU = Framework.usuarioLogado.UsuarioLogadoNeon.UsuarioLogadoStNEON.USU;
                        NovaLinha.CADDATAA = DateTime.Now;
                        TextoOrig = NovaLinha.CADTexto+"\r\n";
                    };
                    NovaLinha.CADTexto = TextoOrig + TituloAutor + inclusao;
                    if(NovaLinha.RowState == DataRowState.Detached)
                        dCaderninho.CADernoEletronico.AddCADernoEletronicoRow(NovaLinha);
                    dCaderninho.CADernoEletronicoTableAdapter.Update(NovaLinha);
                    NovaLinha.AcceptChanges();
                    layoutView1.Refresh();
                    cADernoEletronicoBindingSource.Position = cADernoEletronicoBindingSource.Find("CAD", NovaLinha.CAD);
                
            }
        }

        
    }
}

