using System.Drawing;
using Calendario.Agenda;
using CadastrosProc.Condominios;

namespace Cadastros.Condominios.PaginasCondominio
{
    /// <summary>
    /// 
    /// </summary>
    public partial class TabAgenda : CompontesBasicos.ComponenteTabCampos
    {
        /// <summary>
        /// 
        /// </summary>
        public TabAgenda()
        {
            InitializeComponent();
        }

        private cCondominiosCampos2 TabMaeC
        {
            get
            {
                return (cCondominiosCampos2)TabMae;
            }
        }

        private bool Configurado = false;

        /// <summary>
        /// 
        /// </summary>
        /// <param name="Entrando"></param>
        public override void OnTrocaPagina(bool Entrando)
        {
            base.OnTrocaPagina(Entrando);
            if (Entrando)
            {
                if (!Configurado)
                {
                    Configurado = true;
                    uSUariosBindingSource.DataSource = FrameworkProc.datasets.dUSUarios.dUSUariosStTodos;
                    TipoAGendamentoBindingSource.DataSource = Calendario.Agenda.dAGendaBase.dAGendaBaseSt;
                    Framework.Enumeracoes.VirEnumAGBStatus.CarregaEditorDaGrid(colAGBStatus);
                    //pESSOASBindingSource.DataSource = Estatica.EdEstatico;
                    //Framework.Enumeracoes.VirEnumAssinaCheque.CarregaEditorDaGrid(colCDRAssinaCheque);
                    //Framework.Enumeracoes.VirEnumINSSSindico.CarregaEditorDaGrid(colCDRINSS);
                }
                TabMaeC.dCondominiosCampos.AGendaBaseTableAdapter.Fill(TabMaeC.dCondominiosCampos.AGendaBase, TabMaeC.pk);

            }
        }

        private void gridView1_RowCellStyle(object sender, DevExpress.XtraGrid.Views.Grid.RowCellStyleEventArgs e)
        {
            if (e.CellValue == null)
                return;
            if (e.Column == colAGBStatus)
            {
                e.Appearance.BackColor = Framework.Enumeracoes.VirEnumAGBStatus.GetCor(e.CellValue);
                if (e.Appearance.BackColor == Color.Black)
                    e.Appearance.ForeColor = Color.White;
                else
                    e.Appearance.ForeColor = Color.Black;

            }
            else if (e.Column == colAGB_TAG)
            {
                dAGendaBase.TipoAGendamentoRow rowTag = dAGendaBase.dAGendaBaseSt.TipoAGendamento.FindByTAG((int)e.CellValue);
                e.Appearance.BackColor = Color.FromArgb(rowTag.TAGCor);
                e.Appearance.ForeColor = Color.Black;
            }
        }

        private void repositoryItemButtonEdit1_ButtonClick(object sender, DevExpress.XtraEditors.Controls.ButtonPressedEventArgs e)
        {
            dCondominiosCampos.AGendaBaseRow row = (dCondominiosCampos.AGendaBaseRow)gridView1.GetFocusedDataRow();
            if (row == null)
                return;

            
            int pk = row.AGB;
            dAGendaBase.dAGendaBaseSt.AGendaBaseTableAdapter.FillByCON(dAGendaBase.dAGendaBaseSt.AGendaBase, TabMaeC.pk);
            CompontesBasicos.FormPrincipalBase.FormPrincipal.ShowMoludoCampos(typeof(cAgendaCampos), "teste", pk, false, null,CompontesBasicos.EstadosDosComponentes.PopUp);
            TabMaeC.dCondominiosCampos.AGendaBaseTableAdapter.Fill(TabMaeC.dCondominiosCampos.AGendaBase, TabMaeC.pk);
           
        }
    }
}
