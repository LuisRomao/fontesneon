/*
LH - 27/01/2015 16:00  14.2.4.9          - Novo malote - Ter�a de manh�
*/

using System;
using VirEnumeracoesNeon;
using CadastrosProc;

namespace Cadastros.Condominios.PaginasCondominio
{
    /// <summary>
    /// 
    /// </summary>
    public partial class TabOperacional : CompontesBasicos.ComponenteTabCampos
    {
        /// <summary>
        /// Construtor
        /// </summary>
        public TabOperacional()
        {
            InitializeComponent();
            //Cadastros.Fornecedores.dFornecedoresGrade ds = Cadastros.Fornecedores.Advogados.cAdvogadosGrade.DFornecedoresGradeSTD;
            //bindingSourceEscADVOGADOS.DataSource = TabMaeC.DFornecedoresLookup;
            //sEGURADORASBindingSource.DataSource = Fornecedores.dFornecedoresGrade.GetdFornecedoresGradeStd("SEG");
            //cORRETORASEGUROSBindingSource.DataSource = Fornecedores.dFornecedoresGrade.GetdFornecedoresGradeStd("COR");
            aUDITORESBindingSource.DataSource = FrameworkProc.datasets.dUSUarios.dUSUariosSt;
            dPlanosSeguroConteudoBindingSource.DataSource = Framework.datasets.dPlanosSeguroConteudo.dPlanosSeguroConteudoSt;
            Framework.Enumeracoes.VirEnumTiposMalote.CarregaEditorDaGrid(imageComboBoxEdit2);            
        }

        private cCondominiosCampos2 TabMaeC
        {
            get
            {
                return (cCondominiosCampos2)TabMae;
            }
        }

        private bool Configurado = false;

        /// <summary>
        /// 
        /// </summary>
        /// <param name="Entrando"></param>
        public override void OnTrocaPagina(bool Entrando)
        {
            base.OnTrocaPagina(Entrando);
            if (Entrando)
            {
                if (!Configurado)
                {
                    Configurado = true;
                    conTasLogicasBindingSource.DataSource = TabMaeC.dCondominiosCampos;
                    //System.Windows.Forms.MessageBox.Show("Carregando");
                    //Framework.objetosNeon.malote.VirEnumTiposMalote.CarregaEditorDaGrid(comboMalotes1);
                }
                //if (TabMaeC.dCondominiosCampos.ConTasLogicas.Count == 0)
                TabMaeC.dCondominiosCampos.ConTasLogicasTableAdapter.Fill(TabMaeC.dCondominiosCampos.ConTasLogicas, TabMaeC.pk);
            }
        }

        private void FRNLookUpEdit_ButtonClick(object sender, DevExpress.XtraEditors.Controls.ButtonPressedEventArgs e)
        {
            RAVPadrao Tipo;
            
            DevExpress.XtraEditors.LookUpEdit Loo = (DevExpress.XtraEditors.LookUpEdit)sender;
            if (Loo == cONSeguradora_FRNLookUpEdit)
                Tipo = RAVPadrao.Seguradora;
            else if (Loo == cONCorretoraSeguros_FRNLookUpEdit)
                Tipo = RAVPadrao.Corretora;
            else if (Loo == lookUpEditADV)
                Tipo = RAVPadrao.EscritorioAdvogado;
            else
                throw new NotImplementedException(string.Format("N�o implementado sender = {0}",Loo));

            switch (e.Button.Kind)
            {
                case DevExpress.XtraEditors.Controls.ButtonPredefines.Plus:
                    Fornecedores.Fornecedor NovoFornecedor = Fornecedores.Fornecedor.NovoFornecedor((int)Tipo);
                    if (NovoFornecedor != null)
                    {
                        TabMaeC.RecarregaFRN();
                        Loo.EditValue = NovoFornecedor.FRN;
                        //NovoFornecedor.incluiRAV(Tipo);
                    }
                    /*
                    string stCNPJ = "";
                    if (VirInput.Input.Execute("CNPJ/CPF", ref stCNPJ))
                    {
                        DocBacarios.CPFCNPJ Cnpj = new DocBacarios.CPFCNPJ(stCNPJ);
                        if (Cnpj.Tipo == DocBacarios.TipoCpfCnpj.INVALIDO)
                            System.Windows.Forms.MessageBox.Show("CNPJ/CPF inv�lido");
                        else
                        {                            
                            CadastrosProc.Fornecedores.FornecedorProc Fornecedor = (CadastrosProc.Fornecedores.FornecedorProc)AbstratosNeon.ABS_Fornecedor.ABSGetFornecedor(Cnpj,true);
                            if (Fornecedor != null)
                            {
                                TabMaeC.RecarregaFRN();
                                Loo.EditValue = Fornecedor.FRN;
                                Fornecedor.incluiRAV(Tipo);
                            };
                        }
                    }*/
                    break;
                case DevExpress.XtraEditors.Controls.ButtonPredefines.Ellipsis:
                    if ((Loo.EditValue != null) && (Loo.EditValue != DBNull.Value))
                    {
                        Fornecedores.Fornecedor Fornecedor = new Fornecedores.Fornecedor((int)Loo.EditValue);
                        if(Fornecedor.Editar(true))
                            TabMaeC.RecarregaFRN();

                        //Fornecedor.Editar(false);
                        /*
                        Fornecedores.cFornecedoresCampos Editor = new Fornecedores.cFornecedoresCampos();
                        Editor.Fill(CompontesBasicos.TipoDeCarga.pk, (int)Loo.EditValue, false);
                        if (Editor.VirShowModulo(CompontesBasicos.EstadosDosComponentes.PopUp) == System.Windows.Forms.DialogResult.OK)
                            TabMaeC.RecarregaFRN();*/
                    }
                         
                    break;
                case DevExpress.XtraEditors.Controls.ButtonPredefines.Minus:
                    Loo.EditValue = null;
                    break;
            }
        }

        private void lookUpEdit4_ButtonClick(object sender, DevExpress.XtraEditors.Controls.ButtonPressedEventArgs e)
        {
            lookUpEdit4.EditValue = null;
        }

        private void lookUpEdit5_Properties_ButtonClick(object sender, DevExpress.XtraEditors.Controls.ButtonPressedEventArgs e)
        {
            if (e.Button.Kind == DevExpress.XtraEditors.Controls.ButtonPredefines.Delete)
                lookUpEdit5.EditValue = null;
        }

        private void SBProcuracao_Click(object sender, EventArgs e)
        {
            if (System.Windows.Forms.MessageBox.Show("A procura��o que ser� editada vale para TODOS os condom�nio deste escrit�rio", "* A T E N � � O *", System.Windows.Forms.MessageBoxButtons.OKCancel, System.Windows.Forms.MessageBoxIcon.Warning, System.Windows.Forms.MessageBoxDefaultButton.Button2) == System.Windows.Forms.DialogResult.OK)
            {

            }
        }

        private void lookUpEdit1_Properties_PopupFilter(object sender, DevExpress.XtraEditors.Controls.PopupFilterEventArgs e)
        {
            e.Criteria = DevExpress.Data.Filtering.CriteriaOperator.Parse("RAV = ?", (int)RAVPadrao.EscritorioAdvogado);
        }

        private void TabOperacional_Load(object sender, EventArgs e)
        {
            bindingSourceFornecedores.DataSource = TabMaeC.DFornecedoresLookup;
        }

        private void cONSeguradora_FRNLookUpEdit_Properties_PopupFilter(object sender, DevExpress.XtraEditors.Controls.PopupFilterEventArgs e)
        {
            e.Criteria = DevExpress.Data.Filtering.CriteriaOperator.Parse("RAV = ?", (int)RAVPadrao.Seguradora);
        }

        private void cONCorretoraSeguros_FRNLookUpEdit_Properties_PopupFilter(object sender, DevExpress.XtraEditors.Controls.PopupFilterEventArgs e)
        {
            e.Criteria = DevExpress.Data.Filtering.CriteriaOperator.Parse("RAV = ?", (int)RAVPadrao.Corretora);
        }
    }
}

