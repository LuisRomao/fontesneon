using System;
using System.Data;
using System.Windows.Forms;
using CadastrosProc.Fornecedores;
using VirEnumeracoesNeon;
using CadastrosProc.Condominios;

namespace Cadastros.Condominios.PaginasCondominio
{
    /// <summary>
    /// 
    /// </summary>
    public partial class TabCorpoDiretivo : CompontesBasicos.ComponenteTabCampos
    {
        /// <summary>
        /// 
        /// </summary>
        public TabCorpoDiretivo()
        {
            InitializeComponent();            
        }

        private bool Configurado;

        private cCondominiosCampos2 TabMaeC
        {
            get
            {
                return (cCondominiosCampos2)TabMae;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="Entrando"></param>
        public override void OnTrocaPagina(bool Entrando)
        {
            base.OnTrocaPagina(Entrando);
            if (Entrando)
            {
                if (!Configurado)
                {
                    Configurado = true;
                    //pESSOASBindingSource.DataSource = Estatica.EdEstatico;
                    pESSOASBindingSource.DataSource = TabMaeC.dCondominiosCampos;
                    TabMaeC.dCondominiosCampos.PESloockTableAdapter.Fill(TabMaeC.dCondominiosCampos.PESloock, TabMaeC.pk);
                    Framework.Enumeracoes.VirEnumAssinaCheque.CarregaEditorDaGrid(colCDRAssinaCheque);
                    Framework.Enumeracoes.VirEnumINSSSindico.CarregaEditorDaGrid(colCDRINSS);
                }
                TabMaeC.dCondominiosCampos.CORPODIRETIVO1TableAdapter.Fill(TabMaeC.dCondominiosCampos.CORPODIRETIVO1, TabMaeC.pk);
                
            }
        }
        
        private void gridView1_ValidatingEditor(object sender, DevExpress.XtraEditors.Controls.BaseContainerValidateEditorEventArgs e)
        {
            
            if ((gridView1.FocusedColumn.FieldName == "CDR_CGO") && (e.Value.ToString() == "1")) {  //s�ndico
                int loc = gridView1.LocateByValue(0, gridView1.FocusedColumn, 1);
                if (loc >= 0)
                {
                    e.ErrorText = "N�o s�o permitidos dois s�ndicos";
                    e.Valid = false;
                }
            } 
        }

        private void repositoryItemLookUpApBloco_EditValueChanged(object sender, EventArgs e)
        {
            if (gridView1.FocusedColumn.FieldName == "CDR_APT")
            {
                DataRowView DRV = (DataRowView)gridView1.GetRow(gridView1.FocusedRowHandle);
                dCondominiosCampos.CORPODIRETIVO1Row Row = (dCondominiosCampos.CORPODIRETIVO1Row)DRV.Row;
                DevExpress.XtraEditors.LookUpEdit Editor = (DevExpress.XtraEditors.LookUpEdit)sender;

                if (Editor.GetColumnValue("APTProprietario_PES") == DBNull.Value)
                    MessageBox.Show("Apartamento sem proprie�rio!!!");
                else
                    Row.CDR_PES = (int)Editor.GetColumnValue("APTProprietario_PES");
                Row.CDRProprietario = true;
                
            }
        }
        
        private void gridView1_ValidateRow(object sender, DevExpress.XtraGrid.Views.Base.ValidateRowEventArgs e)
        {
            DataRowView DRV = (DataRowView)e.Row;
            if ((DRV != null) && (DRV.Row != null))
            {
                dCondominiosCampos.CORPODIRETIVO1Row CDRRow = (dCondominiosCampos.CORPODIRETIVO1Row)DRV.Row;
                foreach (dCondominiosCampos.CORPODIRETIVO1Row RowTeste in CDRRow.Table.Rows)
                {
                    if (RowTeste.RowState == DataRowState.Deleted)
                        continue;
                    if(!RowTeste.IsCDR_APTNull() && (!CDRRow.IsCDR_APTNull()))
                        if ((RowTeste.CDR != CDRRow.CDR) && (RowTeste.CDR_APT == CDRRow.CDR_APT))                        
                            RowTeste.SetCDR_APTNull();
                };
                if ((INSSSindico)CDRRow.CDRINSS != INSSSindico.NaoRecolhe)
                {
                    string ComandoVerificaPis =
"SELECT     PESCpfCnpj, PESPis, PESDataNascimento, PES\r\n" +
"FROM         PESSOAS\r\n" +
"WHERE     (PES = @P1)";
                    DataRow DrPis = VirMSSQL.TableAdapter.STTableAdapter.BuscaSQLRow(ComandoVerificaPis, CDRRow.CDR_PES);
                    if (DrPis == null)
                    {
                        e.ErrorText = "PIS n�o cadastrado";
                        e.Valid = false;
                        return;
                    }
                    if (DrPis["PESPis"] == DBNull.Value)
                    {
                        e.ErrorText = "PIS n�o cadastrado";
                        e.Valid = false;
                        return;
                    }
                    if (DrPis["PESCpfCnpj"] == DBNull.Value)
                    {
                        e.ErrorText = "CPF n�o cadastrado";
                        e.Valid = false;
                        return;
                    }
                    
                }
                if (((CDRRow.CDRDescontoCondominio != 0) || (CDRRow.CDRDescontoRateios != 0) || (CDRRow.CDRDescontoFR != 0))
                    &&
                    ((INSSSindico)CDRRow.CDRINSS != INSSSindico.ParteCondSindico))
                {
                    string JustificativaINSS = "";
                    if (!VirInput.Input.Execute("Justificativa para n�o recolhimento do INSS integral:", ref JustificativaINSS, true) || (JustificativaINSS == ""))
                    {
                        e.ErrorText = "Justificativa inv�lida";
                        e.Valid = false;
                        return;
                    }
                    else
                        TabMaeC.CorpoDiretivoJustificativas += string.Format("{0}: {1}\r\n",CDRRow.CARGOSRow.CGONome,JustificativaINSS);
                }
            }
            
            
        }        

        /*
        private Fornecedores.dFornecedoresGrade.FornecedoresDataTable Fornecedores
        {
            get
            {
                return null;
            }
        }*/

        private void repositoryItemLookUpPessoas_ButtonClick(object sender, DevExpress.XtraEditors.Controls.ButtonPressedEventArgs e)
        {
            DevExpress.XtraEditors.LookUpEdit Loo = (DevExpress.XtraEditors.LookUpEdit)sender;
            switch (e.Button.Kind)
            {
                case DevExpress.XtraEditors.Controls.ButtonPredefines.Plus:
                    string Ultimocpf = "";
                    if (VirInput.Input.Execute("CPF/CNPJ", ref Ultimocpf, false))
                    {
                        DocBacarios.CPFCNPJ novoCPF = new DocBacarios.CPFCNPJ(Ultimocpf);
                        if ((novoCPF.Tipo == DocBacarios.TipoCpfCnpj.INVALIDO) || (novoCPF.Valor == 0))
                        {
                            MessageBox.Show("CPF/CNPJ inv�lido");
                            return;
                        }
                        else
                        {
                            Pessoas.cPessoasCampos EditorPes = new Pessoas.cPessoasCampos();
                            if (Pessoas.dPessoasCampos.PESSOASTableAdapter.FillByCPFCNPJ(EditorPes.dPessoasCampos.PESSOAS, novoCPF.ToString()) == 0)
                            {
                                Pessoas.dPessoasCampos.PESSOASRow novarow = EditorPes.dPessoasCampos.PESSOAS.NewPESSOASRow();                                
                                novarow.PESNome = "";
                                novarow.PESEmail = novarow.PESFone1 = novarow.PESFone2 = novarow.PESFone3 = novarow.PESFone4 = "";
                                novarow.PESCpfCnpj = novoCPF.ToString();
                                novarow.PESTipo = (novoCPF.Tipo == DocBacarios.TipoCpfCnpj.CPF ? "F" : "J");
                                novarow.PESCpfCnpjViaSite = false;
                                novarow.PESCpfErrado = false;
                                EditorPes.dPessoasCampos.PESSOAS.AddPESSOASRow(novarow);
                                EditorPes.tipoDeCarga = CompontesBasicos.TipoDeCarga.navegacao;
                                if (EditorPes.VirShowModulo(CompontesBasicos.EstadosDosComponentes.PopUp) == DialogResult.OK)
                                {
                                    TabMaeC.IncluiPESloo(EditorPes.pk, novarow.PESNome);
                                    Loo.EditValue = EditorPes.pk;                                    
                                }
                            }
                            else
                            {
                                TabMaeC.IncluiPESloo(EditorPes.dPessoasCampos.PESSOAS[0].PES, EditorPes.dPessoasCampos.PESSOAS[0].PESNome);
                                Loo.EditValue = EditorPes.dPessoasCampos.PESSOAS[0].PES;                                
                            };
                        }
                    }
                    break;                    
                case DevExpress.XtraEditors.Controls.ButtonPredefines.Ellipsis:
                    if ((Loo.EditValue != null) && (Loo.EditValue != DBNull.Value))
                    {
                        int PES = (int)Loo.EditValue;
                        Pessoas.cPessoasCampos EditorPes = new Pessoas.cPessoasCampos();
                        EditorPes.Fill(CompontesBasicos.TipoDeCarga.pk, PES, false);

                        if (EditorPes.VirShowModulo(CompontesBasicos.EstadosDosComponentes.PopUp) == DialogResult.OK)
                            TabMaeC.IncluiPESloo(EditorPes.dPessoasCampos.PESSOAS[0].PES, EditorPes.dPessoasCampos.PESSOAS[0].PESNome);
                    }
                    //     
                    break;
                case DevExpress.XtraEditors.Controls.ButtonPredefines.Glyph:                    
                    DataRowView DRV = (DataRowView)gridView1.GetRow(gridView1.FocusedRowHandle);
                    dCondominiosCampos.CORPODIRETIVO1Row Row = (dCondominiosCampos.CORPODIRETIVO1Row)DRV.Row;
                    if (Row["CDR_APT"].ToString() != "")
                    {
                        dCondominiosCampos.APARTAMENTOSRow AP = TabMaeC.dCondominiosCampos.APARTAMENTOS.FindByAPT(Row.CDR_APT);
                        if (e.Button.Tag.ToString() == "P"){
                            if (AP["APTProprietario_PES"] != DBNull.Value)
                            {
                                Loo.EditValue = AP.APTProprietario_PES;
                                Row.CDRProprietario = true;
                            }
                        }
                        else {
                            if (AP["APTInquilino_PES"] != DBNull.Value)
                            {
                                Loo.EditValue = AP.APTInquilino_PES;
                                Row.CDRProprietario = false;
                            }
                        }
                    }
                    break;
                case DevExpress.XtraEditors.Controls.ButtonPredefines.Redo:
                    if((Loo.EditValue == null) ||(Loo.EditValue == DBNull.Value))
                        return;
                    Pessoas.dPessoasCampos DPes = new Cadastros.Pessoas.dPessoasCampos();
                    if (Pessoas.dPessoasCampos.PESSOASTableAdapter.Fill(DPes.PESSOAS, (int)Loo.EditValue) == 1)
                    {
                        Pessoas.dPessoasCampos.PESSOASRow rowPES = DPes.PESSOAS[0];
                        if (rowPES.IsPESCpfCnpjNull())
                            MessageBox.Show("CPF/CNPJ n�o cadastrado");
                        else
                        {
                            /*
                            foreach(Fornecedores.dFornecedoresGrade.FornecedoresRow rowFRN in FornecedoresServ)
                                if ((!rowFRN.IsFRNCnpjNull()) && (rowFRN.FRNCnpj == rowPES.PESCpfCnpj))
                                {
                                    MessageBox.Show("J� cadastrado");
                                    return;
                                };*/
                            dFornecedoresCampos dFornecedoresC = new dFornecedoresCampos();
                            dFornecedoresCampos.FORNECEDORESRow novorowFRN = dFornecedoresC.FORNECEDORES.NewFORNECEDORESRow();
                            foreach (DataColumn DC in novorowFRN.Table.Columns)
                            {
                                if (DC.ColumnName == "FRN")
                                    continue;
                                string campoPES = DC.ColumnName.Replace("FRN", "PES");
                                if (campoPES == "PESFantasia")
                                    campoPES = "PESNome";
                                if(rowPES.Table.Columns.Contains(campoPES))
                                    if (rowPES[campoPES] != DBNull.Value)
                                    {
                                        if (DC.DataType == typeof(string))
                                        {
                                            string manobra = (string)rowPES[campoPES];
                                            if (DC.MaxLength < manobra.Length)
                                                manobra = manobra.Substring(0, DC.MaxLength);
                                            novorowFRN[DC] = manobra;
                                        }
                                        else
                                            novorowFRN[DC] = rowPES[campoPES];
                                    }
                            };
                            
                            novorowFRN.FRNCnpj = rowPES.PESCpfCnpj;
                            if (!rowPES.IsPESPisNull())
                                novorowFRN.FRNRegistro = (new DocBacarios.PIS(rowPES.PESPis)).Valor;                            
                            novorowFRN.FRNTipo = "FSR";
                            novorowFRN.FRNContaTipoCredito = (int)Framework.FRNContaTipoCredito.ContaCorrete;
                            novorowFRN.FRNRetemISS = true;
                            novorowFRN.FRNRetemINSSPrestador = true;
                            novorowFRN.FRNRetemINSSTomador = true;
                            dFornecedoresC.FORNECEDORES.AddFORNECEDORESRow(novorowFRN);
                            dFornecedoresC.FORNECEDORESTableAdapter.Update(novorowFRN);
                            novorowFRN.AcceptChanges();
                            //Cadastros.Fornecedores.Servicos.cFornecedoresServicos.DFornecedoresGradeSTD.Fill();
                        }
                    }
                    break;
            }
        }
        
        private void repositoryItemButtonEdit1_ButtonClick(object sender, DevExpress.XtraEditors.Controls.ButtonPressedEventArgs e)
        {
            switch (e.Button.Kind)
            {
                case DevExpress.XtraEditors.Controls.ButtonPredefines.Delete:
                    if (DialogResult.Yes == MessageBox.Show("Confirma a exclus�o?", "Excluir Membro", MessageBoxButtons.YesNo, MessageBoxIcon.Question))
                    {
                        dCondominiosCampos.CORPODIRETIVO1Row row = (dCondominiosCampos.CORPODIRETIVO1Row)gridView1.GetFocusedDataRow();
                        if (row != null)
                        {
                            row.Delete();
                            TabMaeC.dCondominiosCampos.CORPODIRETIVO1TableAdapter.Update(TabMaeC.dCondominiosCampos.CORPODIRETIVO1);
                            TabMaeC.dCondominiosCampos.CORPODIRETIVO1.AcceptChanges();
                            gridView1.RefreshData();
                        }
                    }
                    //    gridView1.DeleteSelectedRows();
                    break;
            }
        }

        private void gridView1_InitNewRow(object sender, DevExpress.XtraGrid.Views.Grid.InitNewRowEventArgs e)
        {
            dCondominiosCampos.CORPODIRETIVO1Row Row = (dCondominiosCampos.CORPODIRETIVO1Row)gridView1.GetDataRow(e.RowHandle);
            if (Row != null)
            {
                Row.CDRDescontoCondominio = 0;
                Row.CDRDescontoCondominioValor = false;
                Row.CDRDescontoFR = 0;
                Row.CDRDescontoFRValor = false;
                Row.CDRDescontoRateios = 0;
                Row.CDRDescontoRateiosValor = false;
                Row.CDRTipoAssinaCheque = (int)AssinaCheque.NaoAssina;
                Row.CDRProprietario = true;
                Row.CDRINSS = (int)INSSSindico.NaoRecolhe;
            }
        }
        
        private void gridView1_RowUpdated(object sender, DevExpress.XtraGrid.Views.Base.RowObjectEventArgs e)
        {
            dCondominiosCampos.CORPODIRETIVO1Row row = (dCondominiosCampos.CORPODIRETIVO1Row)(((DataRowView)e.Row).Row);
            TabMaeC.dCondominiosCampos.CORPODIRETIVO1TableAdapter.Update(row);
            row.AcceptChanges();
            //if (row.CDR_CGO == 1)
            //    Cadastros.Utilitarios.MSAccess.fExportToMsAccess.ExportaSindico(row.CDR_CON);           
            TabMaeC.CorpoDiretivoAlterado = true;
        }        
    }
}

