namespace Cadastros.Condominios.PaginasCondominio
{
    partial class TabDescontos
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(TabDescontos));
            this.panelControl1 = new DevExpress.XtraEditors.PanelControl();
            this.calcEdit1 = new DevExpress.XtraEditors.CalcEdit();
            this.simpleButton1 = new DevExpress.XtraEditors.SimpleButton();
            this.spinNovaData = new DevExpress.XtraEditors.SpinEdit();
            this.simpleButton2 = new DevExpress.XtraEditors.SimpleButton();
            this.gridControl1 = new DevExpress.XtraGrid.GridControl();
            this.dTabDescontosBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.dTabDescontos = new Cadastros.Condominios.PaginasCondominio.dTabDescontos();
            this.gridView1 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colBloco = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colAPTNumero = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colAPTFracaoIdeal = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colcalDiaCondominio = new DevExpress.XtraGrid.Columns.GridColumn();
            this.EditorValor = new DevExpress.XtraEditors.Repository.RepositoryItemCalcEdit();
            ((System.ComponentModel.ISupportInitialize)(this.StyleController_F)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl1)).BeginInit();
            this.panelControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.calcEdit1.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.spinNovaData.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dTabDescontosBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dTabDescontos)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.EditorValor)).BeginInit();
            this.SuspendLayout();
            // 
            // ToolTipController_F
            // 
            this.ToolTipController_F.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.ToolTipController_F.Appearance.ForeColor = System.Drawing.Color.Black;
            this.ToolTipController_F.Appearance.Options.UseBackColor = true;
            this.ToolTipController_F.Appearance.Options.UseFont = true;
            this.ToolTipController_F.Appearance.Options.UseForeColor = true;
            this.ToolTipController_F.Appearance.Options.UseTextOptions = true;
            this.ToolTipController_F.Appearance.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            // 
            // StyleController_F
            // 
            this.StyleController_F.AppearanceDisabled.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this.StyleController_F.AppearanceDisabled.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this.StyleController_F.AppearanceDisabled.Options.UseBackColor = true;
            this.StyleController_F.AppearanceDisabled.Options.UseForeColor = true;
            this.StyleController_F.AppearanceFocused.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.StyleController_F.AppearanceFocused.Options.UseBackColor = true;
  
            // 
            // panelControl1
            // 
            this.panelControl1.Controls.Add(this.calcEdit1);
            this.panelControl1.Controls.Add(this.simpleButton1);
            this.panelControl1.Controls.Add(this.spinNovaData);
            this.panelControl1.Controls.Add(this.simpleButton2);
            this.panelControl1.Dock = System.Windows.Forms.DockStyle.Top;
            this.panelControl1.Location = new System.Drawing.Point(0, 0);
            this.panelControl1.Name = "panelControl1";
            this.panelControl1.Size = new System.Drawing.Size(936, 61);
            this.panelControl1.TabIndex = 0;
            // 
            // calcEdit1
            // 
            this.calcEdit1.Location = new System.Drawing.Point(312, 13);
            this.calcEdit1.Name = "calcEdit1";
            this.calcEdit1.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.calcEdit1.Properties.Appearance.Options.UseFont = true;
            this.calcEdit1.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.calcEdit1.Properties.Mask.EditMask = "n2";
            this.calcEdit1.Properties.Precision = 2;
            this.calcEdit1.Size = new System.Drawing.Size(100, 26);
            this.calcEdit1.TabIndex = 4;
            // 
            // simpleButton1
            // 
            this.simpleButton1.Location = new System.Drawing.Point(231, 16);
            this.simpleButton1.Name = "simpleButton1";
            this.simpleButton1.Size = new System.Drawing.Size(75, 23);
            this.simpleButton1.TabIndex = 3;
            this.simpleButton1.Text = "Novo Valor";
            this.simpleButton1.Click += new System.EventHandler(this.simpleButton1_Click);
            // 
            // spinNovaData
            // 
            this.spinNovaData.EditValue = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.spinNovaData.Location = new System.Drawing.Point(86, 13);
            this.spinNovaData.Name = "spinNovaData";
            this.spinNovaData.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.spinNovaData.Properties.Appearance.Options.UseFont = true;
            this.spinNovaData.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.spinNovaData.Properties.MaxValue = new decimal(new int[] {
            31,
            0,
            0,
            0});
            this.spinNovaData.Properties.MinValue = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.spinNovaData.Size = new System.Drawing.Size(100, 26);
            this.spinNovaData.TabIndex = 2;
            // 
            // simpleButton2
            // 
            this.simpleButton2.Location = new System.Drawing.Point(5, 16);
            this.simpleButton2.Name = "simpleButton2";
            this.simpleButton2.Size = new System.Drawing.Size(75, 23);
            this.simpleButton2.TabIndex = 1;
            this.simpleButton2.Text = "Nova Data";
            this.simpleButton2.Click += new System.EventHandler(this.simpleButton2_Click);
            // 
            // gridControl1
            // 
            this.gridControl1.DataMember = "APARTAMENTOS";
            this.gridControl1.DataSource = this.dTabDescontosBindingSource;
            this.gridControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridControl1.Location = new System.Drawing.Point(0, 61);
            this.gridControl1.MainView = this.gridView1;
            this.gridControl1.Name = "gridControl1";
            this.gridControl1.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.EditorValor});
            this.gridControl1.ShowOnlyPredefinedDetails = true;
            this.gridControl1.Size = new System.Drawing.Size(936, 548);
            this.gridControl1.TabIndex = 1;
            this.gridControl1.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridView1});
            // 
            // dTabDescontosBindingSource
            // 
            this.dTabDescontosBindingSource.DataSource = this.dTabDescontos;
            this.dTabDescontosBindingSource.Position = 0;
            // 
            // dTabDescontos
            // 
            this.dTabDescontos.DataSetName = "dTabDescontos";
            this.dTabDescontos.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // gridView1
            // 
            this.gridView1.Appearance.ColumnFilterButton.BackColor = System.Drawing.Color.Orange;
            this.gridView1.Appearance.ColumnFilterButton.BorderColor = System.Drawing.Color.Orange;
            this.gridView1.Appearance.ColumnFilterButton.ForeColor = System.Drawing.Color.DarkOrange;
            this.gridView1.Appearance.ColumnFilterButton.Options.UseBackColor = true;
            this.gridView1.Appearance.ColumnFilterButton.Options.UseBorderColor = true;
            this.gridView1.Appearance.ColumnFilterButton.Options.UseForeColor = true;
            this.gridView1.Appearance.ColumnFilterButtonActive.BackColor = System.Drawing.Color.DarkOrange;
            this.gridView1.Appearance.ColumnFilterButtonActive.BorderColor = System.Drawing.Color.DarkOrange;
            this.gridView1.Appearance.ColumnFilterButtonActive.ForeColor = System.Drawing.Color.White;
            this.gridView1.Appearance.ColumnFilterButtonActive.Options.UseBackColor = true;
            this.gridView1.Appearance.ColumnFilterButtonActive.Options.UseBorderColor = true;
            this.gridView1.Appearance.ColumnFilterButtonActive.Options.UseForeColor = true;
            this.gridView1.Appearance.Empty.BackColor = System.Drawing.Color.LightSkyBlue;
            this.gridView1.Appearance.Empty.BackColor2 = System.Drawing.Color.SkyBlue;
            this.gridView1.Appearance.Empty.GradientMode = System.Drawing.Drawing2D.LinearGradientMode.BackwardDiagonal;
            this.gridView1.Appearance.Empty.Options.UseBackColor = true;
            this.gridView1.Appearance.EvenRow.BackColor = System.Drawing.Color.Linen;
            this.gridView1.Appearance.EvenRow.BackColor2 = System.Drawing.Color.AntiqueWhite;
            this.gridView1.Appearance.EvenRow.GradientMode = System.Drawing.Drawing2D.LinearGradientMode.ForwardDiagonal;
            this.gridView1.Appearance.EvenRow.Options.UseBackColor = true;
            this.gridView1.Appearance.FilterCloseButton.BackColor = System.Drawing.Color.Orange;
            this.gridView1.Appearance.FilterCloseButton.BorderColor = System.Drawing.Color.Orange;
            this.gridView1.Appearance.FilterCloseButton.Options.UseBackColor = true;
            this.gridView1.Appearance.FilterCloseButton.Options.UseBorderColor = true;
            this.gridView1.Appearance.FilterPanel.BackColor = System.Drawing.Color.DarkOrange;
            this.gridView1.Appearance.FilterPanel.BackColor2 = System.Drawing.Color.Orange;
            this.gridView1.Appearance.FilterPanel.ForeColor = System.Drawing.Color.Black;
            this.gridView1.Appearance.FilterPanel.GradientMode = System.Drawing.Drawing2D.LinearGradientMode.BackwardDiagonal;
            this.gridView1.Appearance.FilterPanel.Options.UseBackColor = true;
            this.gridView1.Appearance.FilterPanel.Options.UseForeColor = true;
            this.gridView1.Appearance.FocusedRow.BackColor = System.Drawing.Color.RoyalBlue;
            this.gridView1.Appearance.FocusedRow.BackColor2 = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this.gridView1.Appearance.FocusedRow.ForeColor = System.Drawing.Color.White;
            this.gridView1.Appearance.FocusedRow.GradientMode = System.Drawing.Drawing2D.LinearGradientMode.Vertical;
            this.gridView1.Appearance.FocusedRow.Options.UseBackColor = true;
            this.gridView1.Appearance.FocusedRow.Options.UseForeColor = true;
            this.gridView1.Appearance.FooterPanel.BackColor = System.Drawing.Color.Orange;
            this.gridView1.Appearance.FooterPanel.BorderColor = System.Drawing.Color.Orange;
            this.gridView1.Appearance.FooterPanel.Options.UseBackColor = true;
            this.gridView1.Appearance.FooterPanel.Options.UseBorderColor = true;
            this.gridView1.Appearance.GroupButton.BackColor = System.Drawing.Color.Wheat;
            this.gridView1.Appearance.GroupButton.BorderColor = System.Drawing.Color.Wheat;
            this.gridView1.Appearance.GroupButton.Options.UseBackColor = true;
            this.gridView1.Appearance.GroupButton.Options.UseBorderColor = true;
            this.gridView1.Appearance.GroupFooter.BackColor = System.Drawing.Color.Wheat;
            this.gridView1.Appearance.GroupFooter.BorderColor = System.Drawing.Color.Wheat;
            this.gridView1.Appearance.GroupFooter.Options.UseBackColor = true;
            this.gridView1.Appearance.GroupFooter.Options.UseBorderColor = true;
            this.gridView1.Appearance.GroupPanel.BackColor = System.Drawing.Color.RoyalBlue;
            this.gridView1.Appearance.GroupPanel.ForeColor = System.Drawing.Color.White;
            this.gridView1.Appearance.GroupPanel.Options.UseBackColor = true;
            this.gridView1.Appearance.GroupPanel.Options.UseForeColor = true;
            this.gridView1.Appearance.GroupRow.BackColor = System.Drawing.Color.Wheat;
            this.gridView1.Appearance.GroupRow.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Bold);
            this.gridView1.Appearance.GroupRow.Options.UseBackColor = true;
            this.gridView1.Appearance.GroupRow.Options.UseFont = true;
            this.gridView1.Appearance.HeaderPanel.BackColor = System.Drawing.Color.Orange;
            this.gridView1.Appearance.HeaderPanel.BorderColor = System.Drawing.Color.Orange;
            this.gridView1.Appearance.HeaderPanel.Options.UseBackColor = true;
            this.gridView1.Appearance.HeaderPanel.Options.UseBorderColor = true;
            this.gridView1.Appearance.HideSelectionRow.BackColor = System.Drawing.Color.LightSlateGray;
            this.gridView1.Appearance.HideSelectionRow.Options.UseBackColor = true;
            this.gridView1.Appearance.HorzLine.BackColor = System.Drawing.Color.Tan;
            this.gridView1.Appearance.HorzLine.Options.UseBackColor = true;
            this.gridView1.Appearance.OddRow.BackColor = System.Drawing.Color.White;
            this.gridView1.Appearance.OddRow.Options.UseBackColor = true;
            this.gridView1.Appearance.Preview.BackColor = System.Drawing.Color.Khaki;
            this.gridView1.Appearance.Preview.BackColor2 = System.Drawing.Color.Cornsilk;
            this.gridView1.Appearance.Preview.Font = new System.Drawing.Font("Tahoma", 7.5F);
            this.gridView1.Appearance.Preview.GradientMode = System.Drawing.Drawing2D.LinearGradientMode.BackwardDiagonal;
            this.gridView1.Appearance.Preview.Options.UseBackColor = true;
            this.gridView1.Appearance.Preview.Options.UseFont = true;
            this.gridView1.Appearance.Row.BackColor = System.Drawing.Color.White;
            this.gridView1.Appearance.Row.Options.UseBackColor = true;
            this.gridView1.Appearance.RowSeparator.BackColor = System.Drawing.Color.LightSkyBlue;
            this.gridView1.Appearance.RowSeparator.Options.UseBackColor = true;
            this.gridView1.Appearance.VertLine.BackColor = System.Drawing.Color.Tan;
            this.gridView1.Appearance.VertLine.Options.UseBackColor = true;
            this.gridView1.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colBloco,
            this.colAPTNumero,
            this.colAPTFracaoIdeal,
            this.colcalDiaCondominio});
            this.gridView1.GridControl = this.gridControl1;
            this.gridView1.Name = "gridView1";
            this.gridView1.OptionsSelection.MultiSelect = true;
            this.gridView1.OptionsSelection.MultiSelectMode = DevExpress.XtraGrid.Views.Grid.GridMultiSelectMode.CellSelect;
            this.gridView1.OptionsView.ColumnAutoWidth = false;
            this.gridView1.OptionsView.EnableAppearanceEvenRow = true;
            this.gridView1.OptionsView.EnableAppearanceOddRow = true;
            this.gridView1.OptionsView.ShowAutoFilterRow = true;
            this.gridView1.OptionsView.ShowGroupedColumns = true;
            this.gridView1.OptionsView.ShowGroupPanel = false;
            this.gridView1.SortInfo.AddRange(new DevExpress.XtraGrid.Columns.GridColumnSortInfo[] {
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colBloco, DevExpress.Data.ColumnSortOrder.Ascending),
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colAPTNumero, DevExpress.Data.ColumnSortOrder.Ascending)});
            this.gridView1.RowCellStyle += new DevExpress.XtraGrid.Views.Grid.RowCellStyleEventHandler(this.gridView1_RowCellStyle);
            // 
            // colBloco
            // 
            this.colBloco.AppearanceCell.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.colBloco.AppearanceCell.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.colBloco.AppearanceCell.Options.UseBackColor = true;
            this.colBloco.AppearanceCell.Options.UseFont = true;
            this.colBloco.Caption = "Bloco";
            this.colBloco.FieldName = "Bloco";
            this.colBloco.Fixed = DevExpress.XtraGrid.Columns.FixedStyle.Left;
            this.colBloco.Name = "colBloco";
            this.colBloco.OptionsColumn.AllowFocus = false;
            this.colBloco.OptionsColumn.AllowGroup = DevExpress.Utils.DefaultBoolean.True;
            this.colBloco.OptionsColumn.AllowSize = false;
            this.colBloco.OptionsColumn.FixedWidth = true;
            this.colBloco.OptionsColumn.ReadOnly = true;
            this.colBloco.Visible = true;
            this.colBloco.VisibleIndex = 0;
            // 
            // colAPTNumero
            // 
            this.colAPTNumero.AppearanceCell.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.colAPTNumero.AppearanceCell.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.colAPTNumero.AppearanceCell.Options.UseBackColor = true;
            this.colAPTNumero.AppearanceCell.Options.UseFont = true;
            this.colAPTNumero.Caption = "Unidade";
            this.colAPTNumero.FieldName = "APTNumero";
            this.colAPTNumero.Fixed = DevExpress.XtraGrid.Columns.FixedStyle.Left;
            this.colAPTNumero.Name = "colAPTNumero";
            this.colAPTNumero.OptionsColumn.AllowFocus = false;
            this.colAPTNumero.OptionsColumn.AllowGroup = DevExpress.Utils.DefaultBoolean.True;
            this.colAPTNumero.OptionsColumn.AllowSize = false;
            this.colAPTNumero.OptionsColumn.FixedWidth = true;
            this.colAPTNumero.OptionsColumn.ReadOnly = true;
            this.colAPTNumero.Visible = true;
            this.colAPTNumero.VisibleIndex = 1;
            // 
            // colAPTFracaoIdeal
            // 
            this.colAPTFracaoIdeal.AppearanceCell.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.colAPTFracaoIdeal.AppearanceCell.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.colAPTFracaoIdeal.AppearanceCell.Options.UseBackColor = true;
            this.colAPTFracaoIdeal.AppearanceCell.Options.UseFont = true;
            this.colAPTFracaoIdeal.Caption = "Fra��o";
            this.colAPTFracaoIdeal.DisplayFormat.FormatString = "n4";
            this.colAPTFracaoIdeal.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.colAPTFracaoIdeal.FieldName = "APTFracaoIdeal";
            this.colAPTFracaoIdeal.Fixed = DevExpress.XtraGrid.Columns.FixedStyle.Left;
            this.colAPTFracaoIdeal.Name = "colAPTFracaoIdeal";
            this.colAPTFracaoIdeal.OptionsColumn.AllowFocus = false;
            this.colAPTFracaoIdeal.OptionsColumn.AllowGroup = DevExpress.Utils.DefaultBoolean.True;
            this.colAPTFracaoIdeal.OptionsColumn.AllowSize = false;
            this.colAPTFracaoIdeal.OptionsColumn.FixedWidth = true;
            this.colAPTFracaoIdeal.OptionsColumn.ReadOnly = true;
            this.colAPTFracaoIdeal.Visible = true;
            this.colAPTFracaoIdeal.VisibleIndex = 2;
            // 
            // colcalDiaCondominio
            // 
            this.colcalDiaCondominio.AppearanceCell.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.colcalDiaCondominio.AppearanceCell.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.colcalDiaCondominio.AppearanceCell.Options.UseBackColor = true;
            this.colcalDiaCondominio.AppearanceCell.Options.UseFont = true;
            this.colcalDiaCondominio.Caption = "Vencimento";
            this.colcalDiaCondominio.FieldName = "calDiaCondominio";
            this.colcalDiaCondominio.Fixed = DevExpress.XtraGrid.Columns.FixedStyle.Left;
            this.colcalDiaCondominio.Name = "colcalDiaCondominio";
            this.colcalDiaCondominio.OptionsColumn.AllowFocus = false;
            this.colcalDiaCondominio.OptionsColumn.AllowGroup = DevExpress.Utils.DefaultBoolean.True;
            this.colcalDiaCondominio.OptionsColumn.AllowSize = false;
            this.colcalDiaCondominio.OptionsColumn.FixedWidth = true;
            this.colcalDiaCondominio.OptionsColumn.ReadOnly = true;
            this.colcalDiaCondominio.Visible = true;
            this.colcalDiaCondominio.VisibleIndex = 3;
            // 
            // EditorValor
            // 
            this.EditorValor.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo),
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Delete)});
            this.EditorValor.Mask.EditMask = "n2";
            this.EditorValor.Name = "EditorValor";
            this.EditorValor.Precision = 2;
            this.EditorValor.Validating += new System.ComponentModel.CancelEventHandler(this.EditorValor_Validating);
            this.EditorValor.ButtonClick += new DevExpress.XtraEditors.Controls.ButtonPressedEventHandler(this.EditorValor_ButtonClick);
            // 
            // TabDescontos
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.gridControl1);
            this.Controls.Add(this.panelControl1);
            this.Doc = System.Windows.Forms.DockStyle.Fill;
         
            this.LookAndFeel.UseDefaultLookAndFeel = false;
            this.Name = "TabDescontos";
            this.Size = new System.Drawing.Size(936, 609);
            ((System.ComponentModel.ISupportInitialize)(this.StyleController_F)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl1)).EndInit();
            this.panelControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.calcEdit1.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.spinNovaData.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dTabDescontosBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dTabDescontos)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.EditorValor)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraEditors.PanelControl panelControl1;
        private DevExpress.XtraGrid.GridControl gridControl1;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView1;
        private DevExpress.XtraEditors.SimpleButton simpleButton2;
        private System.Windows.Forms.BindingSource dTabDescontosBindingSource;
        private dTabDescontos dTabDescontos;
        private DevExpress.XtraGrid.Columns.GridColumn colBloco;
        private DevExpress.XtraGrid.Columns.GridColumn colAPTNumero;
        private DevExpress.XtraGrid.Columns.GridColumn colAPTFracaoIdeal;
        private DevExpress.XtraEditors.SpinEdit spinNovaData;
        private DevExpress.XtraGrid.Columns.GridColumn colcalDiaCondominio;
        private DevExpress.XtraEditors.Repository.RepositoryItemCalcEdit EditorValor;
        private DevExpress.XtraEditors.CalcEdit calcEdit1;
        private DevExpress.XtraEditors.SimpleButton simpleButton1;
    }
}
