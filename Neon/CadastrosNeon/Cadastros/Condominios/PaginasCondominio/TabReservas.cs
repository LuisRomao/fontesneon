﻿/*
MR - 28/05/2014 12:30             - Inclusão de cadastro de regras para os Equipamento (Alterações indicadas por *** MRC - INICIO - RESERVA (28/05/2014 12:30) ***)
MR - 26/08/2014 15:00             - Inclusão do campo que indica se o equipamento deve tratar apenas reserva unitaria, ou seja, uma por vez (Alterações indicadas por *** MRC - INICIO - RESERVA (26/08/2014 15:00) ***)
MR - 02/09/2014 16:00             - Inclusão do campo que indica a quantidade máxima de convidados para o uso do espaço a ser usando na montagem da lista (Alterações indicadas por *** MRC - INICIO - RESERVA (02/09/2014 16:00) ***)
MR - 18/03/2015 10:00             - Tratamento adequado de EMP para nome de arquivo de regras publicado, visando habilitar reservas em Santo André (Alterações indicadas por *** MRC - INICIO - RESERVA (18/03/2015 10:00) ***)
                                    Ajuste na URL para publicação de arquivo de regras de equipamento, corrigindo o erro de pemissao ao salvar o arquivo no provedor
MR - 14/01/2016 12:30             - Inclusão do campo que indica se o equipamento deve tratar apenas reserva unitaria individual, ou seja, uma por vez apenas para o equipamento (Alterações indicadas por *** MRC - INICIO - RESERVA (14/01/2016 12:30) ***)
MR - 15/08/2016 14:00             - Inclusão do campo que indica prazo minimo de reserva (Alterações indicadas por *** MRC - INICIO (15/08/2016 14:00) ***)
                                    Inclusão do campo que indica tipo de grade, tipo de cancelamento e tipo de cobrança (Alterações indicadas por *** MRC - INICIO (15/08/2016 14:00) - Pacote 2 ***)
*/

using System;
using System.Data;
using System.IO;
using System.Windows.Forms;
using AbstratosNeon;
using CompontesBasicos;
using CadastrosProc.Condominios;


namespace Cadastros.Condominios.PaginasCondominio
{
    /// <summary>
    /// Tab para Reservas
    /// </summary>
    public partial class TabReservas : ComponenteTabCampos
    {
        /// <summary>
        /// Construtor
        /// </summary>
        public TabReservas()
        {
            InitializeComponent();           
            //repositoryItemImageComboBox1.Items.AddEnum(typeof(DayOfWeek));
            Framework.Enumeracoes.virEnumGradeReserva.virEnumGradeReservaSt.CarregaEditorDaGrid(colGEQDiaSemana);
            Framework.Enumeracoes.virEnumEQPFormaCobrar.virEnumEQPFormaCobrarSt.CarregaEditorDaGrid(colEQPFormaCobrar);
            //*** MRC - INICIO (15/08/2016 14:00) - Pacote 2 ***
            Framework.Enumeracoes.virEnumEQPTipoGrade.virEnumEQPTipoGradeSt.CarregaEditorDaGrid(colEQPTipoGrade);
            Framework.Enumeracoes.virEnumEQPTipoCancelamento.virEnumEQPTipoCancelamentoSt.CarregaEditorDaGrid(colEQPTipoCancelamento);
            Framework.Enumeracoes.virEnumEQPTipoCobranca.virEnumEQPTipoCobrancaSt.CarregaEditorDaGrid(colEQPTipoCobranca);
            //*** MRC - TERMINO (15/08/2016 14:00) - Pacote 2 ***
            pLAnocontasBindingSource.DataSource = Framework.datasets.dPLAnocontas.dPLAnocontasStReceitas;            
        }

        private cCondominiosCampos2 Chamador
        {
            get { return (cCondominiosCampos2)TabMae; }
        }

        private void gridView1_RowUpdated(object sender, DevExpress.XtraGrid.Views.Base.RowObjectEventArgs e)
        {
            DataRowView DRV = (DataRowView)e.Row;
            dCondominiosCampos.EQuiPamentosRow rowEQP = (dCondominiosCampos.EQuiPamentosRow)DRV.Row;
            if (rowEQP != null)
            {
                Chamador.dCondominiosCampos.EQuiPamentosTableAdapter.Update(rowEQP);
                rowEQP.AcceptChanges();
            }
        }

        /*
        private void gridView1_InitNewRow(object sender, DevExpress.XtraGrid.Views.Grid.InitNewRowEventArgs e)
        {
            DevExpress.XtraGrid.Views.Grid.GridView grid = (DevExpress.XtraGrid.Views.Grid.GridView)sender;
            dCondominiosCampos.EQuiPamentosRow rowEQP = (dCondominiosCampos.EQuiPamentosRow)grid.GetDataRow(e.RowHandle);
            rowEQP.EQPNome = "";
            rowEQP.EQPCumulativo = true;
            rowEQP.EQPDiasCancelar = 7;
            rowEQP.EQPFormaCobrar = (int)AbstratosNeon.EQPFormaCobrar.Salario;
            rowEQP.EQPValor = 0.5M;
            rowEQP.EQP_PLA = "104000";
            rowEQP.EQPJanela = 60;
            rowEQP.EQPReservaInad = false;
        }*/

        private void gridView2_RowUpdated(object sender, DevExpress.XtraGrid.Views.Base.RowObjectEventArgs e)
        {
            DataRowView DRV = (DataRowView)e.Row;
            dCondominiosCampos.GradeEQuipamentoRow rowGEQ = (dCondominiosCampos.GradeEQuipamentoRow)DRV.Row;
            if (rowGEQ != null)
            {
                Chamador.dCondominiosCampos.GradeEQuipamentoTableAdapter.Update(rowGEQ);
                rowGEQ.AcceptChanges();
                UltimoDia = (GradeReserva)rowGEQ.GEQDiaSemana;
                UltimaHora = (int)rowGEQ.GEQTermino.Day;
            }
        }

        private void gridView2_ValidateRow(object sender, DevExpress.XtraGrid.Views.Base.ValidateRowEventArgs e)
        {
            //pendente
        }

        private GradeReserva? UltimoDia;
        private int? UltimaHora;

        private void gridView2_InitNewRow(object sender, DevExpress.XtraGrid.Views.Grid.InitNewRowEventArgs e)
        {
            DevExpress.XtraGrid.Views.Grid.GridView grid = (DevExpress.XtraGrid.Views.Grid.GridView)sender;
            dCondominiosCampos.GradeEQuipamentoRow rowGEQ = (dCondominiosCampos.GradeEQuipamentoRow)grid.GetDataRow(e.RowHandle);
            if ((UltimoDia.HasValue) && (rowGEQ["GEQDiaSemana"] != DBNull.Value))
            {
                if (rowGEQ.GEQDiaSemana == (int)UltimoDia.Value)                
                    rowGEQ.GEQInicio = DateTime.Today.AddHours(UltimaHora.Value);                                    
            }
            else                            
                rowGEQ.GEQInicio = DateTime.Today.AddHours(20);                            
            rowGEQ.GEQTermino = DateTime.Today.AddHours(23);
        }

        private void repositoryItemButtonEdit1_ButtonClick(object sender, DevExpress.XtraEditors.Controls.ButtonPressedEventArgs e)
        {
            DevExpress.XtraGrid.Views.Grid.GridView grid = (DevExpress.XtraGrid.Views.Grid.GridView)gridControl1.FocusedView;
            if (e.Button.Kind == DevExpress.XtraEditors.Controls.ButtonPredefines.Delete)
            {
                dCondominiosCampos.GradeEQuipamentoRow rowGEQ = (dCondominiosCampos.GradeEQuipamentoRow)grid.GetFocusedDataRow();
                rowGEQ.Delete();
                Chamador.dCondominiosCampos.GradeEQuipamentoTableAdapter.Update(Chamador.dCondominiosCampos.GradeEQuipamento);
                Chamador.dCondominiosCampos.GradeEQuipamento.AcceptChanges();
            }
        }

        /*
        private void TabReservas_Load(object sender, EventArgs e)
        {
            if (Chamador != null)
            {
                conTasLogicasBindingSource.DataSource = Chamador.dCondominiosCampos;
                if (Chamador.dCondominiosCampos.ConTasLogicas.Count == 0)
                    Chamador.dCondominiosCampos.ConTasLogicasTableAdapter.Fill(Chamador.dCondominiosCampos.ConTasLogicas, Chamador.linhaMae.CON);
                //*** MRC - INICIO - RESERVA (28/05/2014 12:30) ***
                if (Chamador.dCondominiosCampos.ReGrasEquipamento.Count == 0)
                   Chamador.dCondominiosCampos.ReGrasEquipamentoTableAdapter.FillByCON(Chamador.dCondominiosCampos.ReGrasEquipamento, Chamador.linhaMae.CON);
                //*** MRC - TERMINO - RESERVA (28/05/2014 12:30) ***
                //checkEdit2.Properties.ReadOnly = false;
            }
        }
        */

        /// <summary>
        /// Troca de página
        /// </summary>
        /// <param name="Entrando"></param>
        public override void OnTrocaPagina(bool Entrando)
        {
            if (Entrando)
            {
                conTasLogicasBindingSource.DataSource = Chamador.dCondominiosCampos;
                if (Chamador.dCondominiosCampos.ConTasLogicas.Count == 0)
                    Chamador.dCondominiosCampos.ConTasLogicasTableAdapter.Fill(Chamador.dCondominiosCampos.ConTasLogicas, Chamador.linhaMae.CON);
                //*** MRC - INICIO - RESERVA (28/05/2014 12:30) ***
                if (Chamador.dCondominiosCampos.ReGrasEquipamento.Count == 0)
                    Chamador.dCondominiosCampos.ReGrasEquipamentoTableAdapter.FillByCON(Chamador.dCondominiosCampos.ReGrasEquipamento, Chamador.linhaMae.CON);
                //*** MRC - TERMINO - RESERVA (28/05/2014 12:30) ***
                //checkEdit2.Properties.ReadOnly = false;
            }
        }

        /*
        private WSPublica3.WSPublica3 _WS;

        /// <summary>
        /// 
        /// </summary>
        public WSPublica3.WSPublica3 WS
        {
            get
            {
                if (_WS == null)
                {
                    _WS = new Cadastros.WSPublica3.WSPublica3();
                    if (CompontesBasicos.FormPrincipalBase.strEmProducao)
                    {
                        WS.Proxy = Framework.usuarioLogado.UsuarioLogadoNeon.UsuarioLogadoSt.Proxy;
                        //*** MRC - INICIO - RESERVA (18/03/2015 10:00) ***
                        //WS.Url = @"https://ssl493.websiteseguro.com/neonimoveis/neon23/Neononline/WSPublica3.asmx";
                        WS.Url = @"http://www.neonimoveis.com.br/Neon23/neononline/WSPublica3.asmx";
                        //*** MRC - TERMINO - RESERVA (18/03/2015 10:00) ***
                    }
                };
                return _WS;
            }
        }*/

        private static int staticR;

        private byte[] Criptografa()
        {
            string Aberto = string.Format("{0:yyyyMMddHHmmss}{1:000}{2:00000000}<-*W*->", DateTime.Now, Framework.DSCentral.EMP, staticR++);
            return VirCrip.VirCripWS.Crip(Aberto);
        }

        private void repositoryItemButtonEdit2_ButtonClick(object sender, DevExpress.XtraEditors.Controls.ButtonPressedEventArgs e)
        {
            dCondominiosCampos.EQuiPamentosRow rowEQP = (dCondominiosCampos.EQuiPamentosRow)gridView1.GetFocusedDataRow();
            if (rowEQP == null)
                return;
            switch (e.Button.Kind)
            {
                case DevExpress.XtraEditors.Controls.ButtonPredefines.Delete:
                    if (Chamador.linhaMae.CONStatus == 1)
                        return;
                    if (MessageBox.Show("Confirma a exclusão deste equipamento?", "Confirmação", MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button2) != DialogResult.Yes)
                        return;
                    //*** MRC - INICIO - RESERVA (28/05/2014 12:30) ***
                    try
                    {
                        VirMSSQL.TableAdapter.AbreTrasacaoSQL("Cadastros TabReservas - 164", Chamador.dCondominiosCampos.EQuiPamentosTableAdapter, Chamador.dCondominiosCampos.GradeEQuipamentoTableAdapter, Chamador.dCondominiosCampos.ReGrasEquipamentoTableAdapter);
                        Chamador.dCondominiosCampos.GradeEQuipamentoTableAdapter.ExecutarSQLNonQuery("DELETE FROM ReservaEQuipamento WHERE (REQ_EQP = @P1)", rowEQP.EQP);
                        foreach (dCondominiosCampos.GradeEQuipamentoRow rowGEQ in rowEQP.GetGradeEQuipamentoRows())
                            rowGEQ.Delete();
                        foreach (dCondominiosCampos.ReGrasEquipamentoRow rowRGE in rowEQP.GetReGrasEquipamentoRows())
                           rowRGE.Delete();
                        rowEQP.Delete();

                        Chamador.dCondominiosCampos.GradeEQuipamentoTableAdapter.Update(Chamador.dCondominiosCampos.GradeEQuipamento);
                        Chamador.dCondominiosCampos.ReGrasEquipamentoTableAdapter.Update(Chamador.dCondominiosCampos.ReGrasEquipamento);
                        Chamador.dCondominiosCampos.EQuiPamentosTableAdapter.Update(Chamador.dCondominiosCampos.EQuiPamentos);
                        
                        Chamador.dCondominiosCampos.GradeEQuipamento.AcceptChanges();
                        Chamador.dCondominiosCampos.ReGrasEquipamento.AcceptChanges();
                        Chamador.dCondominiosCampos.EQuiPamentos.AcceptChanges();

                        VirMSSQL.TableAdapter.STTableAdapter.Commit();                                            
                    }
                    catch (Exception ex)
                    {
                       VirMSSQL.TableAdapter.STTableAdapter.Vircatch(ex);
                       if (ex.Message.Contains("The DELETE statement conflicted with the REFERENCE constraint \"FK_ReGrasEquipamento_EQuiPamentos"))
                          MessageBox.Show("Erro:\r\nNão é possível excluir este equipamento pois o mesmo faz parte das 'Regras para Reserva Cumulativa' de outros equipamentos. Ajuste as regras dos demais equipamentos e repita a operação.", "Erro", MessageBoxButtons.OK, MessageBoxIcon.Error); 
                       else
                          MessageBox.Show(string.Format("Erro: {0}\r\nNão foi excluído.", ex.Message), "Erro", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    }

                    //Atualiza dados
                    Chamador.dCondominiosCampos.EQuiPamentosTableAdapter.FillBy(Chamador.dCondominiosCampos.EQuiPamentos, Chamador.linhaMae.CON);
                    gridView1.RefreshData();
                    Chamador.dCondominiosCampos.GradeEQuipamentoTableAdapter.FillByCON(Chamador.dCondominiosCampos.GradeEQuipamento, Chamador.linhaMae.CON);
                    gridView2.RefreshData();
                    Chamador.dCondominiosCampos.ReGrasEquipamentoTableAdapter.FillByCON(Chamador.dCondominiosCampos.ReGrasEquipamento, Chamador.linhaMae.CON);
                    gridView3.RefreshData();

                    break;
                    //*** MRC - TERMINO - RESERVA (28/05/2014 12:30) ***
            }
        }

        private void simpleButton1_Click(object sender, EventArgs e)
        {
            if (openFileDialog1.ShowDialog() == DialogResult.OK)
            {
                dCondominiosCampos.EQuiPamentosRow rowEQP = Chamador.dCondominiosCampos.EQuiPamentos.NewEQuiPamentosRow();
                rowEQP.EQP_CON = Chamador.linhaMae.CON;
                rowEQP.EQPNome = "";
                rowEQP.EQPCumulativo = true;
                //*** MRC - INICIO (15/08/2016 14:00) ***
                rowEQP.EQPDiasReservar = 0;
                //*** MRC - TERMINO (15/08/2016 14:00) ***
                rowEQP.EQPDiasCancelar = 7;
                rowEQP.EQPFormaCobrar = (int)AbstratosNeon.EQPFormaCobrar.Salario;
                rowEQP.EQPValor = 0.5M;
                rowEQP.EQP_PLA = "104000";
                //*** MRC - INICIO (15/08/2016 14:00) - Pacote 2 ***
                rowEQP.EQPTipoGrade = (int)AbstratosNeon.EQPTipoGrade.DiasCorridos;
                rowEQP.EQPTipoCancelamento = (int)AbstratosNeon.EQPTipoCancelamento.AntesDataReservada;
                rowEQP.EQPTipoCobranca = (int)AbstratosNeon.EQPTipoCobranca.Total;
                //*** MRC - TERMINO (15/08/2016 14:00) - Pacote 2 ***
                rowEQP.EQPJanela = 60;
                rowEQP.EQPReservaInad = false;
                rowEQP.EQPAplicaRegras = false;
                //*** MRC - INICIO - RESERVA (26/08/2014 15:00) ***
                rowEQP.EQPReservaUnitaria = false;
                //*** MRC - TERMINO - RESERVA (26/08/2014 15:00) ***
                //*** MRC - INICIO - RESERVA (02/09/2014 16:00) ***
                rowEQP.EQPMaxConvidados = 0;
                //*** MRC - TERMINO - RESERVA (02/09/2014 16:00) ***
                //*** MRC - INICIO - RESERVA (14/01/2016 12:30) ***
                rowEQP.EQPReservaUnitariaIndividual = false;
                //*** MRC - TERMINO - RESERVA (14/01/2016 12:30) ***
                Chamador.dCondominiosCampos.EQuiPamentos.AddEQuiPamentosRow(rowEQP);
                try
                {
                    VirMSSQL.TableAdapter.AbreTrasacaoSQL("Cadastros TabReservas - 197",Chamador.dCondominiosCampos.EQuiPamentosTableAdapter);
                    Chamador.dCondominiosCampos.EQuiPamentosTableAdapter.Update(rowEQP);
                    rowEQP.AcceptChanges();
                    if(PublicaPDF(rowEQP.EQP))
                        VirMSSQL.TableAdapter.STTableAdapter.Commit();                                            
                    else
                        throw new Exception("erro na publicação");

                }
                catch (Exception ex)
                {
                    VirMSSQL.TableAdapter.STTableAdapter.Vircatch(ex);
                    MessageBox.Show(string.Format("Erro: {0}\r\nNão foi gravado.", ex.Message));
                }
            }
        }

        private void checkEdit2_CheckedChanged(object sender, EventArgs e)
        {
            panelPDF.Visible = checkEdit2.Checked;
            if (panelPDF.Visible)
            {
                dCondominiosCampos.EQuiPamentosRow rowEQP = (dCondominiosCampos.EQuiPamentosRow)gridView1.GetFocusedDataRow();
                if (rowEQP != null)
                    MostraPDF(rowEQP);
            }
        }

        private void gridView1_FocusedRowChanged(object sender, DevExpress.XtraGrid.Views.Base.FocusedRowChangedEventArgs e)
        {
            checkEdit2.Properties.ReadOnly = false;
            if(!panelPDF.Visible)
                return;
            dCondominiosCampos.EQuiPamentosRow rowEQP = (dCondominiosCampos.EQuiPamentosRow)gridView1.GetDataRow(e.FocusedRowHandle);
            if (rowEQP == null)
                return;
            else
                MostraPDF(rowEQP);
        }

        private void MostraPDF(dCondominiosCampos.EQuiPamentosRow rowEQP)
        {
            //*** MRC - INICIO - RESERVA (18/03/2015 10:00) ***
            componenteWEB1.Navigate(string.Format(@"http://www.neonimoveis.com.br/neon23/neononline/pdfdoc.aspx?ARQUIVO=RegrasReserva_EQP_{0}_{1}", rowEQP.EQP, Framework.DSCentral.EMP));
            //*** MRC - TERMINO - RESERVA (18/03/2015 10:00) ***
        }

        private bool PublicaPDF(int EQP)
        {
            /*
                    byte[] bdata;
                    using (FileStream fStream = new FileStream(openFileDialog1.FileName, FileMode.Open, FileAccess.Read))
                    {
                        using (BinaryReader br = new BinaryReader(fStream))
                        {
                            bdata = br.ReadBytes((int)(new FileInfo(openFileDialog1.FileName).Length));
                            br.Close();
                            fStream.Close();
                        }
                    };                    
                    string arquivo = string.Format("RegrasReserva_EQP_{0}_{1}.pdf ", EQP, Framework.DSCentral.EMP);                    
                    string retorno = WS.PublicaDOC(Criptografa(), arquivo, bdata);
                    if (retorno == "ok")
                        return true;
                    else
                        throw new Exception(retorno);*/

            using (ClientWCFFtp.ClientFTP clientFTP = new ClientWCFFtp.ClientFTP(CompontesBasicosProc.FuncoesBasicasProc.EstaEmProducao, Framework.DSCentral.USU, Framework.DSCentral.EMP))
            {
                string arquivo = string.Format("RegrasReserva_EQP_{0}_{1}.pdf ", EQP, Framework.DSCentral.EMP);
                if (clientFTP.Put(openFileDialog1.FileName, arquivo))
                    return true;
                else
                    throw new Exception(string.Format("Erro na publicação: {0}", clientFTP.UltimoErro));
            }

        }

        private void simpleButton2_Click(object sender, EventArgs e)
        {
            dCondominiosCampos.EQuiPamentosRow rowEQP = (dCondominiosCampos.EQuiPamentosRow)gridView1.GetFocusedDataRow();
            if (rowEQP == null)
                return;
            if (openFileDialog1.ShowDialog() == DialogResult.OK)
            {
                if (PublicaPDF(rowEQP.EQP))
                    MostraPDF(rowEQP);                                    
            }
        }

        //*** MRC - INICIO - RESERVA (28/05/2014 12:30) ***
        private void gridView3_RowUpdated(object sender, DevExpress.XtraGrid.Views.Base.RowObjectEventArgs e)
        {
           DataRowView DRV = (DataRowView)e.Row;
           dCondominiosCampos.ReGrasEquipamentoRow rowRGE = (dCondominiosCampos.ReGrasEquipamentoRow)DRV.Row;
           if (rowRGE != null)
           {
              Chamador.dCondominiosCampos.ReGrasEquipamentoTableAdapter.Update(rowRGE);
              rowRGE.AcceptChanges();
           }
        }

        private void repositoryItemButtonEdit3_ButtonClick(object sender, DevExpress.XtraEditors.Controls.ButtonPressedEventArgs e)
        {
           DevExpress.XtraGrid.Views.Grid.GridView grid = (DevExpress.XtraGrid.Views.Grid.GridView)gridControl1.FocusedView;
           if (e.Button.Kind == DevExpress.XtraEditors.Controls.ButtonPredefines.Delete)
           {
              dCondominiosCampos.ReGrasEquipamentoRow rowRGE = (dCondominiosCampos.ReGrasEquipamentoRow)grid.GetFocusedDataRow();
              rowRGE.Delete();
              Chamador.dCondominiosCampos.ReGrasEquipamentoTableAdapter.Update(Chamador.dCondominiosCampos.ReGrasEquipamento);
              Chamador.dCondominiosCampos.ReGrasEquipamento.AcceptChanges();
           }
        }
        //*** MRC - TERMINO - RESERVA (28/05/2014 12:30) ***
        
    }
}
