﻿namespace Cadastros.Condominios.PaginasCondominio {


    partial class dCaderninho
    {
        private dCaderninhoTableAdapters.CADernoEletronicoTableAdapter cADernoEletronicoTableAdapter;
        /// <summary>
        /// TableAdapter padrão para tabela: CADernoEletronico
        /// </summary>
        public dCaderninhoTableAdapters.CADernoEletronicoTableAdapter CADernoEletronicoTableAdapter
        {
            get
            {
                if (cADernoEletronicoTableAdapter == null)
                {
                    cADernoEletronicoTableAdapter = new dCaderninhoTableAdapters.CADernoEletronicoTableAdapter();
                    cADernoEletronicoTableAdapter.TrocarStringDeConexao();
                };
                return cADernoEletronicoTableAdapter;
            }
        }
    }
}
