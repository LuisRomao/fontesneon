namespace Cadastros.Condominios.PaginasCondominio
{
    partial class TabBoleto
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.checkEdit1 = new DevExpress.XtraEditors.CheckEdit();
            this.cONDOMINIOSBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.radDataRef = new DevExpress.XtraEditors.RadioGroup();
            this.spinEdit1 = new DevExpress.XtraEditors.SpinEdit();
            this.RadAgrupamento = new DevExpress.XtraEditors.RadioGroup();
            this.chVo = new DevExpress.XtraEditors.CheckEdit();
            this.chVc = new DevExpress.XtraEditors.CheckEdit();
            this.chQuantidade = new DevExpress.XtraEditors.CheckEdit();
            this.radInadMes = new DevExpress.XtraEditors.RadioGroup();
            this.groupControl1 = new DevExpress.XtraEditors.GroupControl();
            this.groupControl7 = new DevExpress.XtraEditors.GroupControl();
            this.label5 = new System.Windows.Forms.Label();
            this.ComboInaTamanhoFonteDefault = new DevExpress.XtraEditors.ImageComboBoxEdit();
            this.simpleButton1 = new DevExpress.XtraEditors.SimpleButton();
            this.groupControl6 = new DevExpress.XtraEditors.GroupControl();
            this.memoTitulo = new DevExpress.XtraEditors.MemoEdit();
            this.memoEdit1 = new DevExpress.XtraEditors.MemoEdit();
            this.groupControl5 = new DevExpress.XtraEditors.GroupControl();
            this.groupControl4 = new DevExpress.XtraEditors.GroupControl();
            this.groupControl3 = new DevExpress.XtraEditors.GroupControl();
            this.labelControl2 = new DevExpress.XtraEditors.LabelControl();
            this.groupControl2 = new DevExpress.XtraEditors.GroupControl();
            this.checkEdit2 = new DevExpress.XtraEditors.CheckEdit();
            this.checkEdit3 = new DevExpress.XtraEditors.CheckEdit();
            this.checkEdit4 = new DevExpress.XtraEditors.CheckEdit();
            this.spinEdit2 = new DevExpress.XtraEditors.SpinEdit();
            this.checkEdit5 = new DevExpress.XtraEditors.CheckEdit();
            this.checkEdit6 = new DevExpress.XtraEditors.CheckEdit();
            this.checkEdit7 = new DevExpress.XtraEditors.CheckEdit();
            this.checkEdit8 = new DevExpress.XtraEditors.CheckEdit();
            ((System.ComponentModel.ISupportInitialize)(this.StyleController_F)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit1.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cONDOMINIOSBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radDataRef.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.spinEdit1.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.RadAgrupamento.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chVo.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chVc.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chQuantidade.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radInadMes.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl1)).BeginInit();
            this.groupControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl7)).BeginInit();
            this.groupControl7.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ComboInaTamanhoFonteDefault.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl6)).BeginInit();
            this.groupControl6.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.memoTitulo.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.memoEdit1.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl5)).BeginInit();
            this.groupControl5.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl4)).BeginInit();
            this.groupControl4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl3)).BeginInit();
            this.groupControl3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl2)).BeginInit();
            this.groupControl2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit2.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit3.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit4.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.spinEdit2.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit5.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit6.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit7.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit8.Properties)).BeginInit();
            this.SuspendLayout();
            // 
            // ToolTipController_F
            // 
            this.ToolTipController_F.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.ToolTipController_F.Appearance.ForeColor = System.Drawing.Color.Black;
            this.ToolTipController_F.Appearance.Options.UseBackColor = true;
            this.ToolTipController_F.Appearance.Options.UseFont = true;
            this.ToolTipController_F.Appearance.Options.UseForeColor = true;
            this.ToolTipController_F.Appearance.Options.UseTextOptions = true;
            this.ToolTipController_F.Appearance.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            // 
            // StyleController_F
            // 
            this.StyleController_F.AppearanceDisabled.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this.StyleController_F.AppearanceDisabled.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this.StyleController_F.AppearanceDisabled.Options.UseBackColor = true;
            this.StyleController_F.AppearanceDisabled.Options.UseForeColor = true;
            this.StyleController_F.AppearanceFocused.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.StyleController_F.AppearanceFocused.Options.UseBackColor = true;
            // 
            // checkEdit1
            // 
            this.checkEdit1.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.cONDOMINIOSBindingSource, "CONImprimeBalanceteBoleto", true));
            this.checkEdit1.Location = new System.Drawing.Point(16, 13);
            this.checkEdit1.Name = "checkEdit1";
            this.checkEdit1.Properties.Caption = "Imprime Balancete";
            this.checkEdit1.Size = new System.Drawing.Size(119, 19);
            this.checkEdit1.TabIndex = 0;
            // 
            // cONDOMINIOSBindingSource
            // 
            this.cONDOMINIOSBindingSource.DataMember = "CONDOMINIOS";
            this.cONDOMINIOSBindingSource.DataSource = typeof(CadastrosProc.Condominios.dCondominiosCampos);
            // 
            // radDataRef
            // 
            this.radDataRef.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.cONDOMINIOSBindingSource, "CONInaDataRef", true));
            this.radDataRef.Location = new System.Drawing.Point(5, 23);
            this.radDataRef.Name = "radDataRef";
            this.radDataRef.Properties.Items.AddRange(new DevExpress.XtraEditors.Controls.RadioGroupItem[] {
            new DevExpress.XtraEditors.Controls.RadioGroupItem(0, "Emiss�o"),
            new DevExpress.XtraEditors.Controls.RadioGroupItem(1, "Fechamento do balancete"),
            new DevExpress.XtraEditors.Controls.RadioGroupItem(2, "Solicitar")});
            this.radDataRef.Size = new System.Drawing.Size(166, 67);
            this.radDataRef.TabIndex = 1;
            this.radDataRef.EditValueChanged += new System.EventHandler(this.RadAgrupamento_EditValueChanged);
            // 
            // spinEdit1
            // 
            this.spinEdit1.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.cONDOMINIOSBindingSource, "CONInaPeriodo", true));
            this.spinEdit1.EditValue = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.spinEdit1.Location = new System.Drawing.Point(57, 96);
            this.spinEdit1.Name = "spinEdit1";
            this.spinEdit1.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.spinEdit1.Size = new System.Drawing.Size(114, 20);
            this.spinEdit1.TabIndex = 2;
            this.spinEdit1.EditValueChanged += new System.EventHandler(this.RadAgrupamento_EditValueChanged);
            // 
            // RadAgrupamento
            // 
            this.RadAgrupamento.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.cONDOMINIOSBindingSource, "CONInaAgrupamento", true));
            this.RadAgrupamento.Location = new System.Drawing.Point(5, 23);
            this.RadAgrupamento.Name = "RadAgrupamento";
            this.RadAgrupamento.Properties.Items.AddRange(new DevExpress.XtraEditors.Controls.RadioGroupItem[] {
            new DevExpress.XtraEditors.Controls.RadioGroupItem(0, "N�o Mostrar"),
            new DevExpress.XtraEditors.Controls.RadioGroupItem(1, "Lista"),
            new DevExpress.XtraEditors.Controls.RadioGroupItem(2, "Agrupar por Bloco"),
            new DevExpress.XtraEditors.Controls.RadioGroupItem(3, "Total do condom�nio")});
            this.RadAgrupamento.Size = new System.Drawing.Size(166, 93);
            this.RadAgrupamento.TabIndex = 3;
            this.RadAgrupamento.EditValueChanged += new System.EventHandler(this.RadAgrupamento_EditValueChanged);
            // 
            // chVo
            // 
            this.chVo.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.cONDOMINIOSBindingSource, "CONInaValorOrig", true));
            this.chVo.Location = new System.Drawing.Point(5, 58);
            this.chVo.Name = "chVo";
            this.chVo.Properties.Caption = "Valor Original";
            this.chVo.Size = new System.Drawing.Size(102, 19);
            this.chVo.TabIndex = 4;
            this.chVo.CheckedChanged += new System.EventHandler(this.RadAgrupamento_EditValueChanged);
            // 
            // chVc
            // 
            this.chVc.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.cONDOMINIOSBindingSource, "CONInaValorCor", true));
            this.chVc.Location = new System.Drawing.Point(5, 95);
            this.chVc.Name = "chVc";
            this.chVc.Properties.Caption = "Valor Corrigido";
            this.chVc.Size = new System.Drawing.Size(102, 19);
            this.chVc.TabIndex = 5;
            this.chVc.CheckedChanged += new System.EventHandler(this.RadAgrupamento_EditValueChanged);
            // 
            // chQuantidade
            // 
            this.chQuantidade.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.cONDOMINIOSBindingSource, "CONInaNumero", true));
            this.chQuantidade.Location = new System.Drawing.Point(5, 23);
            this.chQuantidade.Name = "chQuantidade";
            this.chQuantidade.Properties.Caption = "Quantidade";
            this.chQuantidade.Size = new System.Drawing.Size(89, 19);
            this.chQuantidade.TabIndex = 6;
            this.chQuantidade.CheckedChanged += new System.EventHandler(this.RadAgrupamento_EditValueChanged);
            // 
            // radInadMes
            // 
            this.radInadMes.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.cONDOMINIOSBindingSource, "CONInaDestacaMes", true));
            this.radInadMes.Location = new System.Drawing.Point(5, 23);
            this.radInadMes.Name = "radInadMes";
            this.radInadMes.Properties.Items.AddRange(new DevExpress.XtraEditors.Controls.RadioGroupItem[] {
            new DevExpress.XtraEditors.Controls.RadioGroupItem(0, "N�o mostrar"),
            new DevExpress.XtraEditors.Controls.RadioGroupItem(1, "Valor"),
            new DevExpress.XtraEditors.Controls.RadioGroupItem(2, "Unidades")});
            this.radInadMes.Size = new System.Drawing.Size(139, 93);
            this.radInadMes.TabIndex = 7;
            this.radInadMes.EditValueChanged += new System.EventHandler(this.RadAgrupamento_EditValueChanged);
            // 
            // groupControl1
            // 
            this.groupControl1.Controls.Add(this.groupControl7);
            this.groupControl1.Controls.Add(this.simpleButton1);
            this.groupControl1.Controls.Add(this.groupControl6);
            this.groupControl1.Controls.Add(this.groupControl5);
            this.groupControl1.Controls.Add(this.groupControl4);
            this.groupControl1.Controls.Add(this.groupControl3);
            this.groupControl1.Controls.Add(this.groupControl2);
            this.groupControl1.Location = new System.Drawing.Point(16, 68);
            this.groupControl1.Name = "groupControl1";
            this.groupControl1.Size = new System.Drawing.Size(738, 446);
            this.groupControl1.TabIndex = 8;
            this.groupControl1.Text = "Quadro de inadimpl�ncia";
            // 
            // groupControl7
            // 
            this.groupControl7.Controls.Add(this.label5);
            this.groupControl7.Controls.Add(this.ComboInaTamanhoFonteDefault);
            this.groupControl7.Location = new System.Drawing.Point(7, 150);
            this.groupControl7.Name = "groupControl7";
            this.groupControl7.Size = new System.Drawing.Size(723, 49);
            this.groupControl7.TabIndex = 11;
            this.groupControl7.Text = "Modelo";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(7, 26);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(82, 13);
            this.label5.TabIndex = 13;
            this.label5.Text = "Tamanho Fonte";
            // 
            // ComboInaTamanhoFonteDefault
            // 
            this.ComboInaTamanhoFonteDefault.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.cONDOMINIOSBindingSource, "CONInaTamanhoFonte", true));
            this.ComboInaTamanhoFonteDefault.Location = new System.Drawing.Point(95, 23);
            this.ComboInaTamanhoFonteDefault.Name = "ComboInaTamanhoFonteDefault";
            this.ComboInaTamanhoFonteDefault.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.ComboInaTamanhoFonteDefault.Size = new System.Drawing.Size(82, 20);
            this.ComboInaTamanhoFonteDefault.TabIndex = 12;
            // 
            // simpleButton1
            // 
            this.simpleButton1.Location = new System.Drawing.Point(7, 415);
            this.simpleButton1.Name = "simpleButton1";
            this.simpleButton1.Size = new System.Drawing.Size(199, 23);
            this.simpleButton1.TabIndex = 14;
            this.simpleButton1.Text = "Testar com dados reais";
            this.simpleButton1.Click += new System.EventHandler(this.simpleButton1_Click_1);
            // 
            // groupControl6
            // 
            this.groupControl6.Controls.Add(this.memoTitulo);
            this.groupControl6.Controls.Add(this.memoEdit1);
            this.groupControl6.Location = new System.Drawing.Point(7, 205);
            this.groupControl6.Name = "groupControl6";
            this.groupControl6.Size = new System.Drawing.Size(723, 204);
            this.groupControl6.TabIndex = 13;
            this.groupControl6.Text = "Exemplo";
            // 
            // memoTitulo
            // 
            this.memoTitulo.EditValue = "UNIDADES INADIMPL�NTES";
            this.memoTitulo.Location = new System.Drawing.Point(5, 24);
            this.memoTitulo.Name = "memoTitulo";
            this.memoTitulo.Properties.Appearance.BackColor = System.Drawing.Color.Gray;
            this.memoTitulo.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.memoTitulo.Properties.Appearance.ForeColor = System.Drawing.Color.White;
            this.memoTitulo.Properties.Appearance.Options.UseBackColor = true;
            this.memoTitulo.Properties.Appearance.Options.UseFont = true;
            this.memoTitulo.Properties.Appearance.Options.UseForeColor = true;
            this.memoTitulo.Properties.Appearance.Options.UseTextOptions = true;
            this.memoTitulo.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.memoTitulo.Properties.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.memoTitulo.Size = new System.Drawing.Size(712, 41);
            this.memoTitulo.TabIndex = 1;
            // 
            // memoEdit1
            // 
            this.memoEdit1.EditValue = "";
            this.memoEdit1.Location = new System.Drawing.Point(5, 70);
            this.memoEdit1.Name = "memoEdit1";
            this.memoEdit1.Properties.AcceptsReturn = false;
            this.memoEdit1.Properties.Appearance.Font = new System.Drawing.Font("Courier New", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.memoEdit1.Properties.Appearance.Options.UseFont = true;
            this.memoEdit1.Properties.ReadOnly = true;
            this.memoEdit1.Properties.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.memoEdit1.Properties.WordWrap = false;
            this.memoEdit1.Size = new System.Drawing.Size(712, 129);
            this.memoEdit1.TabIndex = 0;
            // 
            // groupControl5
            // 
            this.groupControl5.Controls.Add(this.radInadMes);
            this.groupControl5.Location = new System.Drawing.Point(396, 23);
            this.groupControl5.Name = "groupControl5";
            this.groupControl5.Size = new System.Drawing.Size(151, 121);
            this.groupControl5.TabIndex = 12;
            this.groupControl5.Text = "Inadempl�ncia do m�s";
            // 
            // groupControl4
            // 
            this.groupControl4.Controls.Add(this.chQuantidade);
            this.groupControl4.Controls.Add(this.chVo);
            this.groupControl4.Controls.Add(this.chVc);
            this.groupControl4.Location = new System.Drawing.Point(190, 23);
            this.groupControl4.Name = "groupControl4";
            this.groupControl4.Size = new System.Drawing.Size(200, 121);
            this.groupControl4.TabIndex = 11;
            this.groupControl4.Text = "Dados";
            // 
            // groupControl3
            // 
            this.groupControl3.Controls.Add(this.radDataRef);
            this.groupControl3.Controls.Add(this.labelControl2);
            this.groupControl3.Controls.Add(this.spinEdit1);
            this.groupControl3.Location = new System.Drawing.Point(553, 23);
            this.groupControl3.Name = "groupControl3";
            this.groupControl3.Size = new System.Drawing.Size(177, 121);
            this.groupControl3.TabIndex = 10;
            this.groupControl3.Text = "Data";
            // 
            // labelControl2
            // 
            this.labelControl2.Location = new System.Drawing.Point(9, 99);
            this.labelControl2.Name = "labelControl2";
            this.labelControl2.Size = new System.Drawing.Size(42, 13);
            this.labelControl2.TabIndex = 2;
            this.labelControl2.Text = "Car�ncia";
            // 
            // groupControl2
            // 
            this.groupControl2.Controls.Add(this.RadAgrupamento);
            this.groupControl2.Location = new System.Drawing.Point(7, 23);
            this.groupControl2.Name = "groupControl2";
            this.groupControl2.Size = new System.Drawing.Size(177, 121);
            this.groupControl2.TabIndex = 9;
            this.groupControl2.Text = "Agrupamento";
            // 
            // checkEdit2
            // 
            this.checkEdit2.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.cONDOMINIOSBindingSource, "CONInibirPendecias", true));
            this.checkEdit2.Location = new System.Drawing.Point(185, 13);
            this.checkEdit2.Name = "checkEdit2";
            this.checkEdit2.Properties.Caption = "Inibir Pend�ncias da Unidade";
            this.checkEdit2.Size = new System.Drawing.Size(174, 19);
            this.checkEdit2.TabIndex = 9;
            // 
            // checkEdit3
            // 
            this.checkEdit3.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.cONDOMINIOSBindingSource, "CONEmiteBoletos", true));
            this.checkEdit3.Location = new System.Drawing.Point(514, 13);
            this.checkEdit3.Name = "checkEdit3";
            this.checkEdit3.Properties.Caption = "Emite Boletos";
            this.checkEdit3.Size = new System.Drawing.Size(87, 19);
            this.checkEdit3.TabIndex = 10;
            this.checkEdit3.CheckedChanged += new System.EventHandler(this.checkEdit3_CheckedChanged);
            // 
            // checkEdit4
            // 
            this.checkEdit4.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.cONDOMINIOSBindingSource, "CONOcultaPxR", true));
            this.checkEdit4.Location = new System.Drawing.Point(347, 13);
            this.checkEdit4.Name = "checkEdit4";
            this.checkEdit4.Properties.Caption = "Inibir Previsto x Realizado";
            this.checkEdit4.Size = new System.Drawing.Size(152, 19);
            this.checkEdit4.TabIndex = 11;
            // 
            // spinEdit2
            // 
            this.spinEdit2.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.cONDOMINIOSBindingSource, "CONBalBoleto", true));
            this.spinEdit2.EditValue = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.spinEdit2.Location = new System.Drawing.Point(141, 12);
            this.spinEdit2.Name = "spinEdit2";
            this.spinEdit2.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.spinEdit2.Size = new System.Drawing.Size(38, 20);
            this.spinEdit2.TabIndex = 12;
            // 
            // checkEdit5
            // 
            this.checkEdit5.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.cONDOMINIOSBindingSource, "CONOcultaSenha", true));
            this.checkEdit5.Location = new System.Drawing.Point(608, 13);
            this.checkEdit5.Name = "checkEdit5";
            this.checkEdit5.Properties.Caption = "Ocultar usu�rio e senha";
            this.checkEdit5.Size = new System.Drawing.Size(148, 19);
            this.checkEdit5.TabIndex = 13;
            // 
            // checkEdit6
            // 
            this.checkEdit6.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.cONDOMINIOSBindingSource, "CONBoletoOcAgua", true));
            this.checkEdit6.Location = new System.Drawing.Point(16, 38);
            this.checkEdit6.Name = "checkEdit6";
            this.checkEdit6.Properties.Caption = "Inibir Quadro �gua";
            this.checkEdit6.Size = new System.Drawing.Size(152, 19);
            this.checkEdit6.TabIndex = 14;
            // 
            // checkEdit7
            // 
            this.checkEdit7.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.cONDOMINIOSBindingSource, "CONBoletoOcGas", true));
            this.checkEdit7.Location = new System.Drawing.Point(185, 38);
            this.checkEdit7.Name = "checkEdit7";
            this.checkEdit7.Properties.Caption = "Inibir Quadro G�s";
            this.checkEdit7.Size = new System.Drawing.Size(152, 19);
            this.checkEdit7.TabIndex = 15;
            // 
            // checkEdit8
            // 
            this.checkEdit8.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.cONDOMINIOSBindingSource, "CONBoletoOcLuz", true));
            this.checkEdit8.Location = new System.Drawing.Point(347, 38);
            this.checkEdit8.Name = "checkEdit8";
            this.checkEdit8.Properties.Caption = "Inibir Quadro Luz";
            this.checkEdit8.Size = new System.Drawing.Size(152, 19);
            this.checkEdit8.TabIndex = 16;
            // 
            // TabBoleto
            // 
            this.Controls.Add(this.checkEdit8);
            this.Controls.Add(this.checkEdit7);
            this.Controls.Add(this.checkEdit6);
            this.Controls.Add(this.checkEdit5);
            this.Controls.Add(this.spinEdit2);
            this.Controls.Add(this.checkEdit4);
            this.Controls.Add(this.checkEdit3);
            this.Controls.Add(this.checkEdit2);
            this.Controls.Add(this.groupControl1);
            this.Controls.Add(this.checkEdit1);
            this.Doc = System.Windows.Forms.DockStyle.Fill;
            this.LookAndFeel.UseDefaultLookAndFeel = false;
            this.Name = "TabBoleto";
            this.Size = new System.Drawing.Size(1490, 807);
            this.Load += new System.EventHandler(this.TabBoleto_Load);
            ((System.ComponentModel.ISupportInitialize)(this.StyleController_F)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit1.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cONDOMINIOSBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radDataRef.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.spinEdit1.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.RadAgrupamento.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chVo.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chVc.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chQuantidade.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radInadMes.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl1)).EndInit();
            this.groupControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.groupControl7)).EndInit();
            this.groupControl7.ResumeLayout(false);
            this.groupControl7.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ComboInaTamanhoFonteDefault.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl6)).EndInit();
            this.groupControl6.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.memoTitulo.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.memoEdit1.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl5)).EndInit();
            this.groupControl5.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.groupControl4)).EndInit();
            this.groupControl4.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.groupControl3)).EndInit();
            this.groupControl3.ResumeLayout(false);
            this.groupControl3.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl2)).EndInit();
            this.groupControl2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit2.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit3.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit4.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.spinEdit2.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit5.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit6.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit7.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit8.Properties)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraEditors.CheckEdit checkEdit1;
        private System.Windows.Forms.BindingSource cONDOMINIOSBindingSource;
        private DevExpress.XtraEditors.RadioGroup radDataRef;
        private DevExpress.XtraEditors.SpinEdit spinEdit1;
        private DevExpress.XtraEditors.RadioGroup RadAgrupamento;
        private DevExpress.XtraEditors.CheckEdit chVo;
        private DevExpress.XtraEditors.CheckEdit chVc;
        private DevExpress.XtraEditors.CheckEdit chQuantidade;
        private DevExpress.XtraEditors.RadioGroup radInadMes;
        private DevExpress.XtraEditors.GroupControl groupControl1;
        private DevExpress.XtraEditors.LabelControl labelControl2;
        private DevExpress.XtraEditors.GroupControl groupControl4;
        private DevExpress.XtraEditors.GroupControl groupControl3;
        private DevExpress.XtraEditors.GroupControl groupControl2;
        private DevExpress.XtraEditors.GroupControl groupControl6;
        private DevExpress.XtraEditors.MemoEdit memoEdit1;
        private DevExpress.XtraEditors.GroupControl groupControl5;
        private DevExpress.XtraEditors.MemoEdit memoTitulo;
        private DevExpress.XtraEditors.SimpleButton simpleButton1;
        private DevExpress.XtraEditors.CheckEdit checkEdit2;
        private DevExpress.XtraEditors.CheckEdit checkEdit3;
        private DevExpress.XtraEditors.CheckEdit checkEdit4;
        private DevExpress.XtraEditors.SpinEdit spinEdit2;
        private DevExpress.XtraEditors.CheckEdit checkEdit5;
        private DevExpress.XtraEditors.GroupControl groupControl7;
        private DevExpress.XtraEditors.ImageComboBoxEdit ComboInaTamanhoFonteDefault;
        private System.Windows.Forms.Label label5;
        private DevExpress.XtraEditors.CheckEdit checkEdit6;
        private DevExpress.XtraEditors.CheckEdit checkEdit7;
        private DevExpress.XtraEditors.CheckEdit checkEdit8;
    }
}
