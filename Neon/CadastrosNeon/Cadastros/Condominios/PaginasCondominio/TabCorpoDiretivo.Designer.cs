namespace Cadastros.Condominios.PaginasCondominio
{
    partial class TabCorpoDiretivo
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            DevExpress.XtraEditors.Controls.EditorButtonImageOptions editorButtonImageOptions7 = new DevExpress.XtraEditors.Controls.EditorButtonImageOptions();
            DevExpress.XtraEditors.Controls.EditorButtonImageOptions editorButtonImageOptions8 = new DevExpress.XtraEditors.Controls.EditorButtonImageOptions();
            DevExpress.XtraEditors.Controls.EditorButtonImageOptions editorButtonImageOptions9 = new DevExpress.XtraEditors.Controls.EditorButtonImageOptions();
            DevExpress.XtraEditors.Controls.EditorButtonImageOptions editorButtonImageOptions10 = new DevExpress.XtraEditors.Controls.EditorButtonImageOptions();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(TabCorpoDiretivo));
            DevExpress.XtraEditors.Controls.EditorButtonImageOptions editorButtonImageOptions11 = new DevExpress.XtraEditors.Controls.EditorButtonImageOptions();
            DevExpress.XtraEditors.Controls.EditorButtonImageOptions editorButtonImageOptions12 = new DevExpress.XtraEditors.Controls.EditorButtonImageOptions();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject2 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SuperToolTip superToolTip2 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem2 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem2 = new DevExpress.Utils.ToolTipItem();
            this.cORPODIRETIVO1BindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.cONDOMINIOSBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.cORPODIRETIVO1GridControl = new DevExpress.XtraGrid.GridControl();
            this.gridView1 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colCDR_CGO = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemLookUpCargo = new DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit();
            this.cARGOSBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.colCDR_APT = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemLookUpApBloco = new DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit();
            this.apartamentosBlocoBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.colCDR_PES = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemLookUpPessoas = new DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit();
            this.pESSOASBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.colCDRAssinaCheque = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemImageComboBox1 = new DevExpress.XtraEditors.Repository.RepositoryItemImageComboBox();
            this.gridColumn1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemButtonEdit1 = new DevExpress.XtraEditors.Repository.RepositoryItemButtonEdit();
            this.colCDRProprietario = new DevExpress.XtraGrid.Columns.GridColumn();
            this.ComboBoxPI = new DevExpress.XtraEditors.Repository.RepositoryItemImageComboBox();
            this.imagensPI1 = new Framework.objetosNeon.ImagensPI();
            this.colCDRDescontoCondominio = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colCDRDescontoCondominioValor = new DevExpress.XtraGrid.Columns.GridColumn();
            this.ComboValorPor = new DevExpress.XtraEditors.Repository.RepositoryItemImageComboBox();
            this.colCDRDescontoFR = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colCDRDescontoFRValor = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colCDRDescontoRateios = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colCDRDescontoRateiosValor = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colCDRINSS = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemImageComboBox2 = new DevExpress.XtraEditors.Repository.RepositoryItemImageComboBox();
            this.aPARTAMENTOSBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.panelControl1 = new DevExpress.XtraEditors.PanelControl();
            this.dateEdit1 = new DevExpress.XtraEditors.DateEdit();
            this.labelControl1 = new DevExpress.XtraEditors.LabelControl();
            ((System.ComponentModel.ISupportInitialize)(this.StyleController_F)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cORPODIRETIVO1BindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cONDOMINIOSBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cORPODIRETIVO1GridControl)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemLookUpCargo)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cARGOSBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemLookUpApBloco)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.apartamentosBlocoBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemLookUpPessoas)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pESSOASBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemImageComboBox1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemButtonEdit1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ComboBoxPI)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.imagensPI1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ComboValorPor)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemImageComboBox2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.aPARTAMENTOSBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl1)).BeginInit();
            this.panelControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dateEdit1.Properties.CalendarTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEdit1.Properties)).BeginInit();
            this.SuspendLayout();
            // 
            // ToolTipController_F
            // 
            this.ToolTipController_F.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.ToolTipController_F.Appearance.ForeColor = System.Drawing.Color.Black;
            this.ToolTipController_F.Appearance.Options.UseBackColor = true;
            this.ToolTipController_F.Appearance.Options.UseFont = true;
            this.ToolTipController_F.Appearance.Options.UseForeColor = true;
            this.ToolTipController_F.Appearance.Options.UseTextOptions = true;
            this.ToolTipController_F.Appearance.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            // 
            // StyleController_F
            // 
            this.StyleController_F.AppearanceDisabled.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this.StyleController_F.AppearanceDisabled.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this.StyleController_F.AppearanceDisabled.Options.UseBackColor = true;
            this.StyleController_F.AppearanceDisabled.Options.UseForeColor = true;
            this.StyleController_F.AppearanceFocused.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.StyleController_F.AppearanceFocused.Options.UseBackColor = true;
            // 
            // cORPODIRETIVO1BindingSource
            // 
            this.cORPODIRETIVO1BindingSource.DataMember = "FK_CORPODIRETIVO_CONDOMINIOS";
            this.cORPODIRETIVO1BindingSource.DataSource = this.cONDOMINIOSBindingSource;
            // 
            // cONDOMINIOSBindingSource
            // 
            this.cONDOMINIOSBindingSource.DataMember = "CONDOMINIOS";
            this.cONDOMINIOSBindingSource.DataSource = typeof(CadastrosProc.Condominios.dCondominiosCampos);
            // 
            // cORPODIRETIVO1GridControl
            // 
            this.cORPODIRETIVO1GridControl.DataSource = this.cORPODIRETIVO1BindingSource;
            this.cORPODIRETIVO1GridControl.Dock = System.Windows.Forms.DockStyle.Fill;
            this.cORPODIRETIVO1GridControl.Location = new System.Drawing.Point(0, 32);
            this.cORPODIRETIVO1GridControl.MainView = this.gridView1;
            this.cORPODIRETIVO1GridControl.Name = "cORPODIRETIVO1GridControl";
            this.cORPODIRETIVO1GridControl.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemLookUpCargo,
            this.repositoryItemLookUpApBloco,
            this.repositoryItemLookUpPessoas,
            this.repositoryItemButtonEdit1,
            this.ComboValorPor,
            this.repositoryItemImageComboBox1,
            this.repositoryItemImageComboBox2,
            this.ComboBoxPI});
            this.cORPODIRETIVO1GridControl.Size = new System.Drawing.Size(1256, 775);
            this.cORPODIRETIVO1GridControl.TabIndex = 2;
            this.cORPODIRETIVO1GridControl.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridView1});
            // 
            // gridView1
            // 
            this.gridView1.Appearance.ColumnFilterButton.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(104)))), ((int)(((byte)(184)))), ((int)(((byte)(251)))));
            this.gridView1.Appearance.ColumnFilterButton.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(104)))), ((int)(((byte)(184)))), ((int)(((byte)(251)))));
            this.gridView1.Appearance.ColumnFilterButton.ForeColor = System.Drawing.Color.White;
            this.gridView1.Appearance.ColumnFilterButton.Options.UseBackColor = true;
            this.gridView1.Appearance.ColumnFilterButton.Options.UseBorderColor = true;
            this.gridView1.Appearance.ColumnFilterButton.Options.UseForeColor = true;
            this.gridView1.Appearance.ColumnFilterButtonActive.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(170)))), ((int)(((byte)(216)))), ((int)(((byte)(254)))));
            this.gridView1.Appearance.ColumnFilterButtonActive.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(170)))), ((int)(((byte)(216)))), ((int)(((byte)(254)))));
            this.gridView1.Appearance.ColumnFilterButtonActive.ForeColor = System.Drawing.Color.Black;
            this.gridView1.Appearance.ColumnFilterButtonActive.Options.UseBackColor = true;
            this.gridView1.Appearance.ColumnFilterButtonActive.Options.UseBorderColor = true;
            this.gridView1.Appearance.ColumnFilterButtonActive.Options.UseForeColor = true;
            this.gridView1.Appearance.Empty.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(236)))), ((int)(((byte)(246)))), ((int)(((byte)(255)))));
            this.gridView1.Appearance.Empty.BackColor2 = System.Drawing.Color.White;
            this.gridView1.Appearance.Empty.Options.UseBackColor = true;
            this.gridView1.Appearance.EvenRow.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(247)))), ((int)(((byte)(251)))), ((int)(((byte)(255)))));
            this.gridView1.Appearance.EvenRow.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(247)))), ((int)(((byte)(251)))), ((int)(((byte)(255)))));
            this.gridView1.Appearance.EvenRow.ForeColor = System.Drawing.Color.Black;
            this.gridView1.Appearance.EvenRow.Options.UseBackColor = true;
            this.gridView1.Appearance.EvenRow.Options.UseBorderColor = true;
            this.gridView1.Appearance.EvenRow.Options.UseForeColor = true;
            this.gridView1.Appearance.FilterCloseButton.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(104)))), ((int)(((byte)(184)))), ((int)(((byte)(251)))));
            this.gridView1.Appearance.FilterCloseButton.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(104)))), ((int)(((byte)(184)))), ((int)(((byte)(251)))));
            this.gridView1.Appearance.FilterCloseButton.ForeColor = System.Drawing.Color.White;
            this.gridView1.Appearance.FilterCloseButton.Options.UseBackColor = true;
            this.gridView1.Appearance.FilterCloseButton.Options.UseBorderColor = true;
            this.gridView1.Appearance.FilterCloseButton.Options.UseForeColor = true;
            this.gridView1.Appearance.FilterPanel.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(236)))), ((int)(((byte)(246)))), ((int)(((byte)(255)))));
            this.gridView1.Appearance.FilterPanel.BackColor2 = System.Drawing.Color.White;
            this.gridView1.Appearance.FilterPanel.ForeColor = System.Drawing.Color.Black;
            this.gridView1.Appearance.FilterPanel.Options.UseBackColor = true;
            this.gridView1.Appearance.FilterPanel.Options.UseForeColor = true;
            this.gridView1.Appearance.FixedLine.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(59)))), ((int)(((byte)(133)))), ((int)(((byte)(195)))));
            this.gridView1.Appearance.FixedLine.Options.UseBackColor = true;
            this.gridView1.Appearance.FocusedCell.BackColor = System.Drawing.Color.White;
            this.gridView1.Appearance.FocusedCell.ForeColor = System.Drawing.Color.Black;
            this.gridView1.Appearance.FocusedCell.Options.UseBackColor = true;
            this.gridView1.Appearance.FocusedCell.Options.UseForeColor = true;
            this.gridView1.Appearance.FocusedRow.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(38)))), ((int)(((byte)(109)))), ((int)(((byte)(189)))));
            this.gridView1.Appearance.FocusedRow.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(59)))), ((int)(((byte)(139)))), ((int)(((byte)(206)))));
            this.gridView1.Appearance.FocusedRow.ForeColor = System.Drawing.Color.White;
            this.gridView1.Appearance.FocusedRow.Options.UseBackColor = true;
            this.gridView1.Appearance.FocusedRow.Options.UseBorderColor = true;
            this.gridView1.Appearance.FocusedRow.Options.UseForeColor = true;
            this.gridView1.Appearance.FooterPanel.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(104)))), ((int)(((byte)(184)))), ((int)(((byte)(251)))));
            this.gridView1.Appearance.FooterPanel.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(104)))), ((int)(((byte)(184)))), ((int)(((byte)(251)))));
            this.gridView1.Appearance.FooterPanel.ForeColor = System.Drawing.Color.Black;
            this.gridView1.Appearance.FooterPanel.Options.UseBackColor = true;
            this.gridView1.Appearance.FooterPanel.Options.UseBorderColor = true;
            this.gridView1.Appearance.FooterPanel.Options.UseForeColor = true;
            this.gridView1.Appearance.GroupButton.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(104)))), ((int)(((byte)(184)))), ((int)(((byte)(251)))));
            this.gridView1.Appearance.GroupButton.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(104)))), ((int)(((byte)(184)))), ((int)(((byte)(251)))));
            this.gridView1.Appearance.GroupButton.Options.UseBackColor = true;
            this.gridView1.Appearance.GroupButton.Options.UseBorderColor = true;
            this.gridView1.Appearance.GroupFooter.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(170)))), ((int)(((byte)(216)))), ((int)(((byte)(254)))));
            this.gridView1.Appearance.GroupFooter.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(170)))), ((int)(((byte)(216)))), ((int)(((byte)(254)))));
            this.gridView1.Appearance.GroupFooter.ForeColor = System.Drawing.Color.Black;
            this.gridView1.Appearance.GroupFooter.Options.UseBackColor = true;
            this.gridView1.Appearance.GroupFooter.Options.UseBorderColor = true;
            this.gridView1.Appearance.GroupFooter.Options.UseForeColor = true;
            this.gridView1.Appearance.GroupPanel.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(236)))), ((int)(((byte)(246)))), ((int)(((byte)(255)))));
            this.gridView1.Appearance.GroupPanel.BackColor2 = System.Drawing.Color.White;
            this.gridView1.Appearance.GroupPanel.ForeColor = System.Drawing.Color.Black;
            this.gridView1.Appearance.GroupPanel.Options.UseBackColor = true;
            this.gridView1.Appearance.GroupPanel.Options.UseForeColor = true;
            this.gridView1.Appearance.GroupRow.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(170)))), ((int)(((byte)(216)))), ((int)(((byte)(254)))));
            this.gridView1.Appearance.GroupRow.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(170)))), ((int)(((byte)(216)))), ((int)(((byte)(254)))));
            this.gridView1.Appearance.GroupRow.ForeColor = System.Drawing.Color.Black;
            this.gridView1.Appearance.GroupRow.Options.UseBackColor = true;
            this.gridView1.Appearance.GroupRow.Options.UseBorderColor = true;
            this.gridView1.Appearance.GroupRow.Options.UseForeColor = true;
            this.gridView1.Appearance.HeaderPanel.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(139)))), ((int)(((byte)(201)))), ((int)(((byte)(254)))));
            this.gridView1.Appearance.HeaderPanel.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(139)))), ((int)(((byte)(201)))), ((int)(((byte)(254)))));
            this.gridView1.Appearance.HeaderPanel.ForeColor = System.Drawing.Color.Black;
            this.gridView1.Appearance.HeaderPanel.Options.UseBackColor = true;
            this.gridView1.Appearance.HeaderPanel.Options.UseBorderColor = true;
            this.gridView1.Appearance.HeaderPanel.Options.UseForeColor = true;
            this.gridView1.Appearance.HideSelectionRow.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(105)))), ((int)(((byte)(170)))), ((int)(((byte)(225)))));
            this.gridView1.Appearance.HideSelectionRow.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(83)))), ((int)(((byte)(155)))), ((int)(((byte)(215)))));
            this.gridView1.Appearance.HideSelectionRow.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(236)))), ((int)(((byte)(246)))), ((int)(((byte)(255)))));
            this.gridView1.Appearance.HideSelectionRow.Options.UseBackColor = true;
            this.gridView1.Appearance.HideSelectionRow.Options.UseBorderColor = true;
            this.gridView1.Appearance.HideSelectionRow.Options.UseForeColor = true;
            this.gridView1.Appearance.HorzLine.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(104)))), ((int)(((byte)(184)))), ((int)(((byte)(251)))));
            this.gridView1.Appearance.HorzLine.Options.UseBackColor = true;
            this.gridView1.Appearance.OddRow.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(236)))), ((int)(((byte)(246)))), ((int)(((byte)(255)))));
            this.gridView1.Appearance.OddRow.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(236)))), ((int)(((byte)(246)))), ((int)(((byte)(255)))));
            this.gridView1.Appearance.OddRow.ForeColor = System.Drawing.Color.Black;
            this.gridView1.Appearance.OddRow.Options.UseBackColor = true;
            this.gridView1.Appearance.OddRow.Options.UseBorderColor = true;
            this.gridView1.Appearance.OddRow.Options.UseForeColor = true;
            this.gridView1.Appearance.Preview.Font = new System.Drawing.Font("Verdana", 7.5F);
            this.gridView1.Appearance.Preview.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(83)))), ((int)(((byte)(155)))), ((int)(((byte)(215)))));
            this.gridView1.Appearance.Preview.Options.UseFont = true;
            this.gridView1.Appearance.Preview.Options.UseForeColor = true;
            this.gridView1.Appearance.Row.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(247)))), ((int)(((byte)(251)))), ((int)(((byte)(255)))));
            this.gridView1.Appearance.Row.ForeColor = System.Drawing.Color.Black;
            this.gridView1.Appearance.Row.Options.UseBackColor = true;
            this.gridView1.Appearance.Row.Options.UseForeColor = true;
            this.gridView1.Appearance.RowSeparator.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(236)))), ((int)(((byte)(246)))), ((int)(((byte)(255)))));
            this.gridView1.Appearance.RowSeparator.BackColor2 = System.Drawing.Color.White;
            this.gridView1.Appearance.RowSeparator.Options.UseBackColor = true;
            this.gridView1.Appearance.SelectedRow.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(83)))), ((int)(((byte)(155)))), ((int)(((byte)(215)))));
            this.gridView1.Appearance.SelectedRow.ForeColor = System.Drawing.Color.White;
            this.gridView1.Appearance.SelectedRow.Options.UseBackColor = true;
            this.gridView1.Appearance.SelectedRow.Options.UseForeColor = true;
            this.gridView1.Appearance.TopNewRow.BackColor = System.Drawing.Color.White;
            this.gridView1.Appearance.TopNewRow.Options.UseBackColor = true;
            this.gridView1.Appearance.VertLine.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(104)))), ((int)(((byte)(184)))), ((int)(((byte)(251)))));
            this.gridView1.Appearance.VertLine.Options.UseBackColor = true;
            this.gridView1.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colCDR_CGO,
            this.colCDR_APT,
            this.colCDR_PES,
            this.colCDRAssinaCheque,
            this.gridColumn1,
            this.colCDRProprietario,
            this.colCDRDescontoCondominio,
            this.colCDRDescontoCondominioValor,
            this.colCDRDescontoFR,
            this.colCDRDescontoFRValor,
            this.colCDRDescontoRateios,
            this.colCDRDescontoRateiosValor,
            this.colCDRINSS});
            this.gridView1.GridControl = this.cORPODIRETIVO1GridControl;
            this.gridView1.Name = "gridView1";
            this.gridView1.NewItemRowText = "Clique aqui para incluir novo membro";
            this.gridView1.OptionsBehavior.EditorShowMode = DevExpress.Utils.EditorShowMode.MouseUp;
            this.gridView1.OptionsView.ColumnAutoWidth = false;
            this.gridView1.OptionsView.EnableAppearanceEvenRow = true;
            this.gridView1.OptionsView.EnableAppearanceOddRow = true;
            this.gridView1.OptionsView.NewItemRowPosition = DevExpress.XtraGrid.Views.Grid.NewItemRowPosition.Top;
            this.gridView1.OptionsView.ShowGroupPanel = false;
            this.gridView1.SortInfo.AddRange(new DevExpress.XtraGrid.Columns.GridColumnSortInfo[] {
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colCDR_CGO, DevExpress.Data.ColumnSortOrder.Ascending)});
            this.gridView1.InitNewRow += new DevExpress.XtraGrid.Views.Grid.InitNewRowEventHandler(this.gridView1_InitNewRow);
            this.gridView1.ValidateRow += new DevExpress.XtraGrid.Views.Base.ValidateRowEventHandler(this.gridView1_ValidateRow);
            this.gridView1.RowUpdated += new DevExpress.XtraGrid.Views.Base.RowObjectEventHandler(this.gridView1_RowUpdated);
            this.gridView1.ValidatingEditor += new DevExpress.XtraEditors.Controls.BaseContainerValidateEditorEventHandler(this.gridView1_ValidatingEditor);
            // 
            // colCDR_CGO
            // 
            this.colCDR_CGO.Caption = "Cargo";
            this.colCDR_CGO.ColumnEdit = this.repositoryItemLookUpCargo;
            this.colCDR_CGO.FieldName = "CDR_CGO";
            this.colCDR_CGO.Name = "colCDR_CGO";
            this.colCDR_CGO.ShowButtonMode = DevExpress.XtraGrid.Views.Base.ShowButtonModeEnum.ShowOnlyInEditor;
            this.colCDR_CGO.Visible = true;
            this.colCDR_CGO.VisibleIndex = 0;
            this.colCDR_CGO.Width = 89;
            // 
            // repositoryItemLookUpCargo
            // 
            this.repositoryItemLookUpCargo.AutoHeight = false;
            this.repositoryItemLookUpCargo.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemLookUpCargo.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("CGONome", "Cargo", 55, DevExpress.Utils.FormatType.None, "", true, DevExpress.Utils.HorzAlignment.Near)});
            this.repositoryItemLookUpCargo.DataSource = this.cARGOSBindingSource;
            this.repositoryItemLookUpCargo.DisplayMember = "CGONome";
            this.repositoryItemLookUpCargo.Name = "repositoryItemLookUpCargo";
            this.repositoryItemLookUpCargo.NullText = "  --";
            this.repositoryItemLookUpCargo.ValueMember = "CGO";
            // 
            // cARGOSBindingSource
            // 
            this.cARGOSBindingSource.DataMember = "CARGOS";
            this.cARGOSBindingSource.DataSource = typeof(CadastrosProc.Condominios.dCondominiosCampos);
            // 
            // colCDR_APT
            // 
            this.colCDR_APT.Caption = "Apartamento";
            this.colCDR_APT.ColumnEdit = this.repositoryItemLookUpApBloco;
            this.colCDR_APT.FieldName = "CDR_APT";
            this.colCDR_APT.Name = "colCDR_APT";
            this.colCDR_APT.ShowButtonMode = DevExpress.XtraGrid.Views.Base.ShowButtonModeEnum.ShowOnlyInEditor;
            this.colCDR_APT.Visible = true;
            this.colCDR_APT.VisibleIndex = 1;
            this.colCDR_APT.Width = 84;
            // 
            // repositoryItemLookUpApBloco
            // 
            this.repositoryItemLookUpApBloco.AllowNullInput = DevExpress.Utils.DefaultBoolean.False;
            this.repositoryItemLookUpApBloco.AutoHeight = false;
            this.repositoryItemLookUpApBloco.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemLookUpApBloco.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("BLOCodigo", "Bloco", 70, DevExpress.Utils.FormatType.None, "", true, DevExpress.Utils.HorzAlignment.Near, DevExpress.Data.ColumnSortOrder.Ascending),
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("BLONome", "", 150, DevExpress.Utils.FormatType.None, "", true, DevExpress.Utils.HorzAlignment.Near),
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("APTNumero", "N�", 62, DevExpress.Utils.FormatType.None, "", true, DevExpress.Utils.HorzAlignment.Center, DevExpress.Data.ColumnSortOrder.Ascending),
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("PESNome", "Propriet�rio", 300, DevExpress.Utils.FormatType.None, "", true, DevExpress.Utils.HorzAlignment.Near),
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("Bloco_Ap", "", 20, DevExpress.Utils.FormatType.None, "", false, DevExpress.Utils.HorzAlignment.Default, DevExpress.Data.ColumnSortOrder.Ascending)});
            this.repositoryItemLookUpApBloco.DataSource = this.apartamentosBlocoBindingSource;
            this.repositoryItemLookUpApBloco.DisplayMember = "Bloco-AP";
            this.repositoryItemLookUpApBloco.Name = "repositoryItemLookUpApBloco";
            this.repositoryItemLookUpApBloco.NullText = "  --";
            this.repositoryItemLookUpApBloco.PopupWidth = 500;
            this.repositoryItemLookUpApBloco.ValueMember = "APT";
            this.repositoryItemLookUpApBloco.EditValueChanged += new System.EventHandler(this.repositoryItemLookUpApBloco_EditValueChanged);
            // 
            // apartamentosBlocoBindingSource
            // 
            this.apartamentosBlocoBindingSource.DataMember = "ApartamentosBloco";
            this.apartamentosBlocoBindingSource.DataSource = typeof(CadastrosProc.Condominios.dCondominiosCampos);
            // 
            // colCDR_PES
            // 
            this.colCDR_PES.Caption = "Nome";
            this.colCDR_PES.ColumnEdit = this.repositoryItemLookUpPessoas;
            this.colCDR_PES.FieldName = "CDR_PES";
            this.colCDR_PES.Name = "colCDR_PES";
            this.colCDR_PES.ShowButtonMode = DevExpress.XtraGrid.Views.Base.ShowButtonModeEnum.ShowOnlyInEditor;
            this.colCDR_PES.Visible = true;
            this.colCDR_PES.VisibleIndex = 3;
            this.colCDR_PES.Width = 188;
            // 
            // repositoryItemLookUpPessoas
            // 
            this.repositoryItemLookUpPessoas.AllowNullInput = DevExpress.Utils.DefaultBoolean.False;
            this.repositoryItemLookUpPessoas.AutoHeight = false;
            editorButtonImageOptions10.EnableTransparency = false;
            editorButtonImageOptions10.Image = ((System.Drawing.Image)(resources.GetObject("editorButtonImageOptions10.Image")));
            editorButtonImageOptions11.EnableTransparency = false;
            editorButtonImageOptions11.Image = ((System.Drawing.Image)(resources.GetObject("editorButtonImageOptions11.Image")));
            toolTipTitleItem2.Text = "Fornecedor";
            toolTipItem2.LeftIndent = 6;
            toolTipItem2.Text = "Cadastra como fornecedor";
            superToolTip2.Items.Add(toolTipTitleItem2);
            superToolTip2.Items.Add(toolTipItem2);
            this.repositoryItemLookUpPessoas.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo, "", -1, true, true, false, editorButtonImageOptions7, new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), "Selecionar"),
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Plus, "", -1, true, true, false, editorButtonImageOptions8, new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), "Novo"),
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Ellipsis, "", -1, true, true, false, editorButtonImageOptions9, new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), "Editar"),
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Glyph, "", -1, true, true, false, editorButtonImageOptions10, new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), "Propriet�rio", "P"),
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Glyph, "", -1, true, true, false, editorButtonImageOptions11, new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), "Inquilino", "I"),
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Redo, "Fornecedor", -1, true, true, false, editorButtonImageOptions12, new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), serializableAppearanceObject2, "Cadastra como fornecedor", null, superToolTip2)});
            this.repositoryItemLookUpPessoas.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("PESNome", "Nome", 51, DevExpress.Utils.FormatType.None, "", true, DevExpress.Utils.HorzAlignment.Near),
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("PESEndereco", "Endere�o", 69, DevExpress.Utils.FormatType.None, "", true, DevExpress.Utils.HorzAlignment.Near),
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("CIDNome", "Cidade", 51, DevExpress.Utils.FormatType.None, "", true, DevExpress.Utils.HorzAlignment.Near)});
            this.repositoryItemLookUpPessoas.DataSource = this.pESSOASBindingSource;
            this.repositoryItemLookUpPessoas.DisplayMember = "PESNome";
            this.repositoryItemLookUpPessoas.Name = "repositoryItemLookUpPessoas";
            this.repositoryItemLookUpPessoas.NullText = "  --";
            this.repositoryItemLookUpPessoas.ShowDropDown = DevExpress.XtraEditors.Controls.ShowDropDown.Never;
            this.repositoryItemLookUpPessoas.ValueMember = "PES";
            this.repositoryItemLookUpPessoas.ButtonClick += new DevExpress.XtraEditors.Controls.ButtonPressedEventHandler(this.repositoryItemLookUpPessoas_ButtonClick);
            // 
            // pESSOASBindingSource
            // 
            this.pESSOASBindingSource.DataMember = "PESloock";
            this.pESSOASBindingSource.DataSource = typeof(CadastrosProc.Condominios.dCondominiosCampos);
            // 
            // colCDRAssinaCheque
            // 
            this.colCDRAssinaCheque.AppearanceHeader.Options.UseTextOptions = true;
            this.colCDRAssinaCheque.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colCDRAssinaCheque.Caption = "Assina Cheque";
            this.colCDRAssinaCheque.ColumnEdit = this.repositoryItemImageComboBox1;
            this.colCDRAssinaCheque.FieldName = "CDRTipoAssinaCheque";
            this.colCDRAssinaCheque.Name = "colCDRAssinaCheque";
            this.colCDRAssinaCheque.Visible = true;
            this.colCDRAssinaCheque.VisibleIndex = 11;
            this.colCDRAssinaCheque.Width = 86;
            // 
            // repositoryItemImageComboBox1
            // 
            this.repositoryItemImageComboBox1.AutoHeight = false;
            this.repositoryItemImageComboBox1.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemImageComboBox1.Name = "repositoryItemImageComboBox1";
            // 
            // gridColumn1
            // 
            this.gridColumn1.ColumnEdit = this.repositoryItemButtonEdit1;
            this.gridColumn1.FieldName = "gridColumn1";
            this.gridColumn1.Fixed = DevExpress.XtraGrid.Columns.FixedStyle.Right;
            this.gridColumn1.Name = "gridColumn1";
            this.gridColumn1.OptionsColumn.AllowGroup = DevExpress.Utils.DefaultBoolean.False;
            this.gridColumn1.OptionsColumn.AllowMerge = DevExpress.Utils.DefaultBoolean.False;
            this.gridColumn1.OptionsColumn.AllowMove = false;
            this.gridColumn1.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.False;
            this.gridColumn1.OptionsColumn.ShowCaption = false;
            this.gridColumn1.OptionsFilter.AllowAutoFilter = false;
            this.gridColumn1.OptionsFilter.AllowFilter = false;
            this.gridColumn1.ShowButtonMode = DevExpress.XtraGrid.Views.Base.ShowButtonModeEnum.ShowAlways;
            this.gridColumn1.UnboundType = DevExpress.Data.UnboundColumnType.Object;
            this.gridColumn1.Visible = true;
            this.gridColumn1.VisibleIndex = 12;
            this.gridColumn1.Width = 25;
            // 
            // repositoryItemButtonEdit1
            // 
            this.repositoryItemButtonEdit1.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Delete)});
            this.repositoryItemButtonEdit1.Name = "repositoryItemButtonEdit1";
            this.repositoryItemButtonEdit1.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor;
            this.repositoryItemButtonEdit1.ButtonClick += new DevExpress.XtraEditors.Controls.ButtonPressedEventHandler(this.repositoryItemButtonEdit1_ButtonClick);
            // 
            // colCDRProprietario
            // 
            this.colCDRProprietario.Caption = "P/I";
            this.colCDRProprietario.ColumnEdit = this.ComboBoxPI;
            this.colCDRProprietario.FieldName = "CDRProprietario";
            this.colCDRProprietario.Name = "colCDRProprietario";
            this.colCDRProprietario.OptionsColumn.ShowCaption = false;
            this.colCDRProprietario.Visible = true;
            this.colCDRProprietario.VisibleIndex = 2;
            this.colCDRProprietario.Width = 34;
            // 
            // ComboBoxPI
            // 
            this.ComboBoxPI.AutoHeight = false;
            this.ComboBoxPI.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.ComboBoxPI.GlyphAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.ComboBoxPI.Items.AddRange(new DevExpress.XtraEditors.Controls.ImageComboBoxItem[] {
            new DevExpress.XtraEditors.Controls.ImageComboBoxItem("P", true, 0),
            new DevExpress.XtraEditors.Controls.ImageComboBoxItem("I", false, 1)});
            this.ComboBoxPI.LargeImages = this.imagensPI1;
            this.ComboBoxPI.Name = "ComboBoxPI";
            this.ComboBoxPI.SmallImages = this.imagensPI1;
            // 
            // imagensPI1
            // 
            this.imagensPI1.ImageStream = ((DevExpress.Utils.ImageCollectionStreamer)(resources.GetObject("imagensPI1.ImageStream")));
            // 
            // colCDRDescontoCondominio
            // 
            this.colCDRDescontoCondominio.Caption = "Desc Condom�nio";
            this.colCDRDescontoCondominio.DisplayFormat.FormatString = "n2";
            this.colCDRDescontoCondominio.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.colCDRDescontoCondominio.FieldName = "CDRDescontoCondominio";
            this.colCDRDescontoCondominio.Name = "colCDRDescontoCondominio";
            this.colCDRDescontoCondominio.Visible = true;
            this.colCDRDescontoCondominio.VisibleIndex = 4;
            this.colCDRDescontoCondominio.Width = 105;
            // 
            // colCDRDescontoCondominioValor
            // 
            this.colCDRDescontoCondominioValor.ColumnEdit = this.ComboValorPor;
            this.colCDRDescontoCondominioValor.FieldName = "CDRDescontoCondominioValor";
            this.colCDRDescontoCondominioValor.Name = "colCDRDescontoCondominioValor";
            this.colCDRDescontoCondominioValor.OptionsColumn.ShowCaption = false;
            this.colCDRDescontoCondominioValor.Visible = true;
            this.colCDRDescontoCondominioValor.VisibleIndex = 5;
            this.colCDRDescontoCondominioValor.Width = 23;
            // 
            // ComboValorPor
            // 
            this.ComboValorPor.AutoHeight = false;
            this.ComboValorPor.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.ComboValorPor.Items.AddRange(new DevExpress.XtraEditors.Controls.ImageComboBoxItem[] {
            new DevExpress.XtraEditors.Controls.ImageComboBoxItem("R$", true, -1),
            new DevExpress.XtraEditors.Controls.ImageComboBoxItem("%", false, -1)});
            this.ComboValorPor.Name = "ComboValorPor";
            // 
            // colCDRDescontoFR
            // 
            this.colCDRDescontoFR.Caption = "Desc F.R.";
            this.colCDRDescontoFR.DisplayFormat.FormatString = "n2";
            this.colCDRDescontoFR.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.colCDRDescontoFR.FieldName = "CDRDescontoFR";
            this.colCDRDescontoFR.Name = "colCDRDescontoFR";
            this.colCDRDescontoFR.Visible = true;
            this.colCDRDescontoFR.VisibleIndex = 6;
            this.colCDRDescontoFR.Width = 69;
            // 
            // colCDRDescontoFRValor
            // 
            this.colCDRDescontoFRValor.ColumnEdit = this.ComboValorPor;
            this.colCDRDescontoFRValor.FieldName = "CDRDescontoFRValor";
            this.colCDRDescontoFRValor.Name = "colCDRDescontoFRValor";
            this.colCDRDescontoFRValor.OptionsColumn.ShowCaption = false;
            this.colCDRDescontoFRValor.Visible = true;
            this.colCDRDescontoFRValor.VisibleIndex = 7;
            this.colCDRDescontoFRValor.Width = 25;
            // 
            // colCDRDescontoRateios
            // 
            this.colCDRDescontoRateios.Caption = "Desc Rateios";
            this.colCDRDescontoRateios.DisplayFormat.FormatString = "n2";
            this.colCDRDescontoRateios.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.colCDRDescontoRateios.FieldName = "CDRDescontoRateios";
            this.colCDRDescontoRateios.Name = "colCDRDescontoRateios";
            this.colCDRDescontoRateios.Visible = true;
            this.colCDRDescontoRateios.VisibleIndex = 8;
            this.colCDRDescontoRateios.Width = 86;
            // 
            // colCDRDescontoRateiosValor
            // 
            this.colCDRDescontoRateiosValor.ColumnEdit = this.ComboValorPor;
            this.colCDRDescontoRateiosValor.FieldName = "CDRDescontoRateiosValor";
            this.colCDRDescontoRateiosValor.Name = "colCDRDescontoRateiosValor";
            this.colCDRDescontoRateiosValor.OptionsColumn.ShowCaption = false;
            this.colCDRDescontoRateiosValor.Visible = true;
            this.colCDRDescontoRateiosValor.VisibleIndex = 9;
            this.colCDRDescontoRateiosValor.Width = 25;
            // 
            // colCDRINSS
            // 
            this.colCDRINSS.Caption = "INSS";
            this.colCDRINSS.ColumnEdit = this.repositoryItemImageComboBox2;
            this.colCDRINSS.FieldName = "CDRINSS";
            this.colCDRINSS.Name = "colCDRINSS";
            this.colCDRINSS.Visible = true;
            this.colCDRINSS.VisibleIndex = 10;
            this.colCDRINSS.Width = 61;
            // 
            // repositoryItemImageComboBox2
            // 
            this.repositoryItemImageComboBox2.AutoHeight = false;
            this.repositoryItemImageComboBox2.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemImageComboBox2.Name = "repositoryItemImageComboBox2";
            // 
            // aPARTAMENTOSBindingSource
            // 
            this.aPARTAMENTOSBindingSource.DataMember = "APARTAMENTOS";
            this.aPARTAMENTOSBindingSource.DataSource = typeof(CadastrosProc.Condominios.dCondominiosCampos);
            // 
            // panelControl1
            // 
            this.panelControl1.Controls.Add(this.dateEdit1);
            this.panelControl1.Controls.Add(this.labelControl1);
            this.panelControl1.Dock = System.Windows.Forms.DockStyle.Top;
            this.panelControl1.Location = new System.Drawing.Point(0, 0);
            this.panelControl1.Name = "panelControl1";
            this.panelControl1.Size = new System.Drawing.Size(1256, 32);
            this.panelControl1.TabIndex = 3;
            // 
            // dateEdit1
            // 
            this.dateEdit1.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.cONDOMINIOSBindingSource, "CONDataMandato", true));
            this.dateEdit1.EditValue = null;
            this.dateEdit1.Location = new System.Drawing.Point(124, 6);
            this.dateEdit1.Name = "dateEdit1";
            this.dateEdit1.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.dateEdit1.Properties.CalendarTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.dateEdit1.Size = new System.Drawing.Size(123, 20);
            this.dateEdit1.TabIndex = 1;
            this.dateEdit1.Visible = false;
            // 
            // labelControl1
            // 
            this.labelControl1.Location = new System.Drawing.Point(21, 9);
            this.labelControl1.Name = "labelControl1";
            this.labelControl1.Size = new System.Drawing.Size(97, 13);
            this.labelControl1.TabIndex = 0;
            this.labelControl1.Text = "Mandato do S�ndico:";
            // 
            // TabCorpoDiretivo
            // 
            this.Controls.Add(this.cORPODIRETIVO1GridControl);
            this.Controls.Add(this.panelControl1);
            this.Doc = System.Windows.Forms.DockStyle.Fill;
            this.LookAndFeel.UseDefaultLookAndFeel = false;
            this.Name = "TabCorpoDiretivo";
            this.Size = new System.Drawing.Size(1256, 807);
            ((System.ComponentModel.ISupportInitialize)(this.StyleController_F)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cORPODIRETIVO1BindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cONDOMINIOSBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cORPODIRETIVO1GridControl)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemLookUpCargo)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cARGOSBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemLookUpApBloco)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.apartamentosBlocoBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemLookUpPessoas)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pESSOASBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemImageComboBox1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemButtonEdit1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ComboBoxPI)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.imagensPI1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ComboValorPor)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemImageComboBox2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.aPARTAMENTOSBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl1)).EndInit();
            this.panelControl1.ResumeLayout(false);
            this.panelControl1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dateEdit1.Properties.CalendarTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEdit1.Properties)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.BindingSource cORPODIRETIVO1BindingSource;
        private DevExpress.XtraGrid.GridControl cORPODIRETIVO1GridControl;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView1;
        private DevExpress.XtraGrid.Columns.GridColumn colCDR_CGO;
        private DevExpress.XtraGrid.Columns.GridColumn colCDR_PES;
        private DevExpress.XtraGrid.Columns.GridColumn colCDR_APT;
        private DevExpress.XtraGrid.Columns.GridColumn colCDRAssinaCheque;
        private System.Windows.Forms.BindingSource cONDOMINIOSBindingSource;
        private DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit repositoryItemLookUpCargo;
        private System.Windows.Forms.BindingSource cARGOSBindingSource;
        private DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit repositoryItemLookUpApBloco;
        private System.Windows.Forms.BindingSource aPARTAMENTOSBindingSource;
        private DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit repositoryItemLookUpPessoas;
        private System.Windows.Forms.BindingSource pESSOASBindingSource;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn1;
        private DevExpress.XtraEditors.Repository.RepositoryItemButtonEdit repositoryItemButtonEdit1;
        private System.Windows.Forms.BindingSource apartamentosBlocoBindingSource;
        private DevExpress.XtraGrid.Columns.GridColumn colCDRProprietario;
        private DevExpress.XtraEditors.PanelControl panelControl1;
        private DevExpress.XtraEditors.DateEdit dateEdit1;
        private DevExpress.XtraEditors.LabelControl labelControl1;
        private DevExpress.XtraGrid.Columns.GridColumn colCDRDescontoCondominio;
        private DevExpress.XtraGrid.Columns.GridColumn colCDRDescontoCondominioValor;
        private DevExpress.XtraGrid.Columns.GridColumn colCDRDescontoFR;
        private DevExpress.XtraGrid.Columns.GridColumn colCDRDescontoFRValor;
        private DevExpress.XtraGrid.Columns.GridColumn colCDRDescontoRateios;
        private DevExpress.XtraGrid.Columns.GridColumn colCDRDescontoRateiosValor;
        private DevExpress.XtraEditors.Repository.RepositoryItemImageComboBox ComboValorPor;
        private DevExpress.XtraEditors.Repository.RepositoryItemImageComboBox repositoryItemImageComboBox1;
        private DevExpress.XtraGrid.Columns.GridColumn colCDRINSS;
        private DevExpress.XtraEditors.Repository.RepositoryItemImageComboBox repositoryItemImageComboBox2;
        private Framework.objetosNeon.ImagensPI imagensPI1;
        private DevExpress.XtraEditors.Repository.RepositoryItemImageComboBox ComboBoxPI;
    }
}
