namespace Cadastros.Condominios.PaginasCondominio
{
    partial class TabFinaceiro
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.Windows.Forms.Label label8;
            System.Windows.Forms.Label label7;
            System.Windows.Forms.Label label6;
            System.Windows.Forms.Label cONValorFRLabel;
            System.Windows.Forms.Label cONValorJurosLabel;
            System.Windows.Forms.Label cONValorMultaLabel;
            System.Windows.Forms.Label cONValorGasLabel;
            System.Windows.Forms.Label cONValorCondominioLabel;
            System.Windows.Forms.Label cONDiaVencimentoLabel;
            System.Windows.Forms.Label cONValorARLabel;
            System.Windows.Forms.Label cONValorCartaSimplesLabel;
            System.Windows.Forms.Label label1;
            System.Windows.Forms.Label label2;
            System.Windows.Forms.Label label3;
            System.Windows.Forms.Label label4;
            System.Windows.Forms.Label label9;
            DevExpress.Utils.SuperToolTip superToolTip1 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem1 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem1 = new DevExpress.Utils.ToolTipItem();
            this.label5 = new System.Windows.Forms.Label();
            this.groupControl5 = new DevExpress.XtraEditors.GroupControl();
            this.spinEdit1 = new DevExpress.XtraEditors.SpinEdit();
            this.cONDOMINIOSBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.lookUpTipoEnvio = new DevExpress.XtraEditors.LookUpEdit();
            this.tipoEnvioArquivosBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.ComboTipoContaPEDefault = new DevExpress.XtraEditors.ImageComboBoxEdit();
            this.txtCodComunicacao = new DevExpress.XtraEditors.TextEdit();
            this.txtCodigoCNR = new DevExpress.XtraEditors.TextEdit();
            this.textEdit8 = new DevExpress.XtraEditors.TextEdit();
            this.textEdit7 = new DevExpress.XtraEditors.TextEdit();
            this.textEdit6 = new DevExpress.XtraEditors.TextEdit();
            this.textEdit5 = new DevExpress.XtraEditors.TextEdit();
            this.lookUpEdit2 = new DevExpress.XtraEditors.LookUpEdit();
            this.bANCOSBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.groupControl1 = new DevExpress.XtraEditors.GroupControl();
            this.cONValorFRCalcEdit = new DevExpress.XtraEditors.CalcEdit();
            this.cONTipoFRRadioGroup = new DevExpress.XtraEditors.RadioGroup();
            this.groupControl4 = new DevExpress.XtraEditors.GroupControl();
            this.txtRegra = new DevExpress.XtraEditors.TextEdit();
            this.speVencimento = new DevExpress.XtraEditors.SpinEdit();
            this.groupControl6 = new DevExpress.XtraEditors.GroupControl();
            this.groupControl3 = new DevExpress.XtraEditors.GroupControl();
            this.cONValorGasCalcEdit = new DevExpress.XtraEditors.CalcEdit();
            this.cONValorCondominioCalcEdit = new DevExpress.XtraEditors.CalcEdit();
            this.groupControl2 = new DevExpress.XtraEditors.GroupControl();
            this.checkEdit3 = new DevExpress.XtraEditors.CheckEdit();
            this.imageComboBoxEdit2 = new DevExpress.XtraEditors.ImageComboBoxEdit();
            this.imageComboBoxEdit1 = new DevExpress.XtraEditors.ImageComboBoxEdit();
            this.cONValorMultaCalcEdit = new DevExpress.XtraEditors.CalcEdit();
            this.cONValorJurosCalcEdit = new DevExpress.XtraEditors.CalcEdit();
            this.groupControl7 = new DevExpress.XtraEditors.GroupControl();
            this.groupControl8 = new DevExpress.XtraEditors.GroupControl();
            this.checkEdit4 = new DevExpress.XtraEditors.CheckEdit();
            this.groupControl10 = new DevExpress.XtraEditors.GroupControl();
            this.groupControl14 = new DevExpress.XtraEditors.GroupControl();
            this.radioGroup2 = new DevExpress.XtraEditors.RadioGroup();
            this.groupControl13 = new DevExpress.XtraEditors.GroupControl();
            this.radioGroup1 = new DevExpress.XtraEditors.RadioGroup();
            this.groupControl12 = new DevExpress.XtraEditors.GroupControl();
            this.cONCorreioAssembleiaRadioGroup = new DevExpress.XtraEditors.RadioGroup();
            this.panelControl1 = new DevExpress.XtraEditors.PanelControl();
            this.checkEdit2 = new DevExpress.XtraEditors.CheckEdit();
            this.checkEdit1 = new DevExpress.XtraEditors.CheckEdit();
            this.cONRepassaCorreioCheckEdit = new DevExpress.XtraEditors.CheckEdit();
            this.groupControl11 = new DevExpress.XtraEditors.GroupControl();
            this.calcEdit2 = new DevExpress.XtraEditors.CalcEdit();
            this.calcEdit1 = new DevExpress.XtraEditors.CalcEdit();
            this.cONValorCartaSimplesCalcEdit = new DevExpress.XtraEditors.CalcEdit();
            this.cONValorARCalcEdit = new DevExpress.XtraEditors.CalcEdit();
            this.groupControl15 = new DevExpress.XtraEditors.GroupControl();
            this.gridControl1 = new DevExpress.XtraGrid.GridControl();
            this.fKPaGamentoEspecificoCONDOMINIOSBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.gridView1 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colPGE_PLA = new DevExpress.XtraGrid.Columns.GridColumn();
            this.LookUpPLA = new DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit();
            this.pLAnocontasBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.colPGEValor = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colPGEMensagem = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colPGEDATAA = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colPGEDATAI = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colPGEA_USU = new DevExpress.XtraGrid.Columns.GridColumn();
            this.LookUpUsuario = new DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit();
            this.uSUARIOSBindingSource1 = new System.Windows.Forms.BindingSource(this.components);
            this.colPGEI_USU = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemButtonEdit1 = new DevExpress.XtraEditors.Repository.RepositoryItemButtonEdit();
            this.colPGE_CTL = new DevExpress.XtraGrid.Columns.GridColumn();
            this.LookUpCTL = new DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit();
            this.conTasLogicasBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.dConta = new Cadastros.Condominios.PaginasCondominio.dConta();
            label8 = new System.Windows.Forms.Label();
            label7 = new System.Windows.Forms.Label();
            label6 = new System.Windows.Forms.Label();
            cONValorFRLabel = new System.Windows.Forms.Label();
            cONValorJurosLabel = new System.Windows.Forms.Label();
            cONValorMultaLabel = new System.Windows.Forms.Label();
            cONValorGasLabel = new System.Windows.Forms.Label();
            cONValorCondominioLabel = new System.Windows.Forms.Label();
            cONDiaVencimentoLabel = new System.Windows.Forms.Label();
            cONValorARLabel = new System.Windows.Forms.Label();
            cONValorCartaSimplesLabel = new System.Windows.Forms.Label();
            label1 = new System.Windows.Forms.Label();
            label2 = new System.Windows.Forms.Label();
            label3 = new System.Windows.Forms.Label();
            label4 = new System.Windows.Forms.Label();
            label9 = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.StyleController_F)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl5)).BeginInit();
            this.groupControl5.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.spinEdit1.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cONDOMINIOSBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lookUpTipoEnvio.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tipoEnvioArquivosBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ComboTipoContaPEDefault.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCodComunicacao.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCodigoCNR.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit8.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit7.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit6.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit5.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lookUpEdit2.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.bANCOSBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl1)).BeginInit();
            this.groupControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.cONValorFRCalcEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cONTipoFRRadioGroup.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl4)).BeginInit();
            this.groupControl4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtRegra.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.speVencimento.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl6)).BeginInit();
            this.groupControl6.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl3)).BeginInit();
            this.groupControl3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.cONValorGasCalcEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cONValorCondominioCalcEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl2)).BeginInit();
            this.groupControl2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit3.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.imageComboBoxEdit2.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.imageComboBoxEdit1.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cONValorMultaCalcEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cONValorJurosCalcEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl7)).BeginInit();
            this.groupControl7.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl8)).BeginInit();
            this.groupControl8.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit4.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl10)).BeginInit();
            this.groupControl10.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl14)).BeginInit();
            this.groupControl14.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.radioGroup2.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl13)).BeginInit();
            this.groupControl13.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.radioGroup1.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl12)).BeginInit();
            this.groupControl12.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.cONCorreioAssembleiaRadioGroup.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl1)).BeginInit();
            this.panelControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit2.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit1.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cONRepassaCorreioCheckEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl11)).BeginInit();
            this.groupControl11.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.calcEdit2.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.calcEdit1.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cONValorCartaSimplesCalcEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cONValorARCalcEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl15)).BeginInit();
            this.groupControl15.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.fKPaGamentoEspecificoCONDOMINIOSBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LookUpPLA)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pLAnocontasBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LookUpUsuario)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uSUARIOSBindingSource1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemButtonEdit1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LookUpCTL)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.conTasLogicasBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dConta)).BeginInit();
            this.SuspendLayout();
            // 
            // ToolTipController_F
            // 
            this.ToolTipController_F.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.ToolTipController_F.Appearance.ForeColor = System.Drawing.Color.Black;
            this.ToolTipController_F.Appearance.Options.UseBackColor = true;
            this.ToolTipController_F.Appearance.Options.UseFont = true;
            this.ToolTipController_F.Appearance.Options.UseForeColor = true;
            this.ToolTipController_F.Appearance.Options.UseTextOptions = true;
            this.ToolTipController_F.Appearance.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            // 
            // StyleController_F
            // 
            this.StyleController_F.AppearanceDisabled.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this.StyleController_F.AppearanceDisabled.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this.StyleController_F.AppearanceDisabled.Options.UseBackColor = true;
            this.StyleController_F.AppearanceDisabled.Options.UseForeColor = true;
            this.StyleController_F.AppearanceFocused.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.StyleController_F.AppearanceFocused.Options.UseBackColor = true;
            // 
            // label8
            // 
            label8.AutoSize = true;
            label8.Location = new System.Drawing.Point(293, 47);
            label8.Name = "label8";
            label8.Size = new System.Drawing.Size(36, 13);
            label8.TabIndex = 5;
            label8.Text = "Conta";
            // 
            // label7
            // 
            label7.AutoSize = true;
            label7.Location = new System.Drawing.Point(293, 24);
            label7.Name = "label7";
            label7.Size = new System.Drawing.Size(45, 13);
            label7.TabIndex = 2;
            label7.Text = "Ag�ncia";
            // 
            // label6
            // 
            label6.AutoSize = true;
            label6.Location = new System.Drawing.Point(11, 24);
            label6.Name = "label6";
            label6.Size = new System.Drawing.Size(36, 13);
            label6.TabIndex = 0;
            label6.Text = "Banco";
            // 
            // cONValorFRLabel
            // 
            cONValorFRLabel.AutoSize = true;
            cONValorFRLabel.Location = new System.Drawing.Point(121, 27);
            cONValorFRLabel.Name = "cONValorFRLabel";
            cONValorFRLabel.Size = new System.Drawing.Size(31, 13);
            cONValorFRLabel.TabIndex = 1;
            cONValorFRLabel.Text = "Valor";
            // 
            // cONValorJurosLabel
            // 
            cONValorJurosLabel.AutoSize = true;
            cONValorJurosLabel.Location = new System.Drawing.Point(15, 47);
            cONValorJurosLabel.Name = "cONValorJurosLabel";
            cONValorJurosLabel.Size = new System.Drawing.Size(33, 13);
            cONValorJurosLabel.TabIndex = 1;
            cONValorJurosLabel.Text = "Juros";
            // 
            // cONValorMultaLabel
            // 
            cONValorMultaLabel.AutoSize = true;
            cONValorMultaLabel.Location = new System.Drawing.Point(15, 24);
            cONValorMultaLabel.Name = "cONValorMultaLabel";
            cONValorMultaLabel.Size = new System.Drawing.Size(33, 13);
            cONValorMultaLabel.TabIndex = 7;
            cONValorMultaLabel.Text = "Multa";
            // 
            // cONValorGasLabel
            // 
            cONValorGasLabel.AutoSize = true;
            cONValorGasLabel.Location = new System.Drawing.Point(19, 47);
            cONValorGasLabel.Name = "cONValorGasLabel";
            cONValorGasLabel.Size = new System.Drawing.Size(48, 13);
            cONValorGasLabel.TabIndex = 2;
            cONValorGasLabel.Text = "G�s (Kg)";
            // 
            // cONValorCondominioLabel
            // 
            cONValorCondominioLabel.AutoSize = true;
            cONValorCondominioLabel.Location = new System.Drawing.Point(19, 24);
            cONValorCondominioLabel.Name = "cONValorCondominioLabel";
            cONValorCondominioLabel.Size = new System.Drawing.Size(62, 13);
            cONValorCondominioLabel.TabIndex = 0;
            cONValorCondominioLabel.Text = "Condominio";
            // 
            // cONDiaVencimentoLabel
            // 
            cONDiaVencimentoLabel.AutoSize = true;
            cONDiaVencimentoLabel.Location = new System.Drawing.Point(12, 23);
            cONDiaVencimentoLabel.Name = "cONDiaVencimentoLabel";
            cONDiaVencimentoLabel.Size = new System.Drawing.Size(22, 13);
            cONDiaVencimentoLabel.TabIndex = 0;
            cONDiaVencimentoLabel.Text = "Dia";
            // 
            // cONValorARLabel
            // 
            cONValorARLabel.AutoSize = true;
            cONValorARLabel.Location = new System.Drawing.Point(11, 25);
            cONValorARLabel.Name = "cONValorARLabel";
            cONValorARLabel.Size = new System.Drawing.Size(21, 13);
            cONValorARLabel.TabIndex = 0;
            cONValorARLabel.Text = "AR";
            // 
            // cONValorCartaSimplesLabel
            // 
            cONValorCartaSimplesLabel.AutoSize = true;
            cONValorCartaSimplesLabel.Location = new System.Drawing.Point(11, 51);
            cONValorCartaSimplesLabel.Name = "cONValorCartaSimplesLabel";
            cONValorCartaSimplesLabel.Size = new System.Drawing.Size(34, 13);
            cONValorCartaSimplesLabel.TabIndex = 2;
            cONValorCartaSimplesLabel.Text = "Carta";
            // 
            // label1
            // 
            label1.AutoSize = true;
            label1.Location = new System.Drawing.Point(11, 77);
            label1.Name = "label1";
            label1.Size = new System.Drawing.Size(37, 13);
            label1.TabIndex = 4;
            label1.Text = "Boleto";
            // 
            // label2
            // 
            label2.AutoSize = true;
            label2.Location = new System.Drawing.Point(464, 28);
            label2.Name = "label2";
            label2.Size = new System.Drawing.Size(54, 13);
            label2.TabIndex = 8;
            label2.Text = "C�d. CNR";
            // 
            // label3
            // 
            label3.AutoSize = true;
            label3.Location = new System.Drawing.Point(143, 25);
            label3.Name = "label3";
            label3.Size = new System.Drawing.Size(35, 13);
            label3.TabIndex = 6;
            label3.Text = "2� Via";
            // 
            // label4
            // 
            label4.AutoSize = true;
            label4.Location = new System.Drawing.Point(635, 24);
            label4.Name = "label4";
            label4.Size = new System.Drawing.Size(155, 13);
            label4.TabIndex = 10;
            label4.Text = "C�d. Comunica��o / Tipo Envio";
            // 
            // label9
            // 
            label9.AutoSize = true;
            label9.Location = new System.Drawing.Point(464, 52);
            label9.Name = "label9";
            label9.Size = new System.Drawing.Size(70, 13);
            label9.TabIndex = 12;
            label9.Text = "Dias Registro";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(635, 47);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(134, 13);
            this.label5.TabIndex = 11;
            this.label5.Text = "Tipo Conta Pag. Eletr�nico";
            // 
            // groupControl5
            // 
            this.groupControl5.Controls.Add(this.spinEdit1);
            this.groupControl5.Controls.Add(label9);
            this.groupControl5.Controls.Add(this.lookUpTipoEnvio);
            this.groupControl5.Controls.Add(this.ComboTipoContaPEDefault);
            this.groupControl5.Controls.Add(this.label5);
            this.groupControl5.Controls.Add(this.txtCodComunicacao);
            this.groupControl5.Controls.Add(label4);
            this.groupControl5.Controls.Add(this.txtCodigoCNR);
            this.groupControl5.Controls.Add(label2);
            this.groupControl5.Controls.Add(this.textEdit8);
            this.groupControl5.Controls.Add(this.textEdit7);
            this.groupControl5.Controls.Add(this.textEdit6);
            this.groupControl5.Controls.Add(this.textEdit5);
            this.groupControl5.Controls.Add(label8);
            this.groupControl5.Controls.Add(label7);
            this.groupControl5.Controls.Add(this.lookUpEdit2);
            this.groupControl5.Controls.Add(label6);
            this.groupControl5.Dock = System.Windows.Forms.DockStyle.Top;
            this.groupControl5.Location = new System.Drawing.Point(0, 0);
            this.groupControl5.Name = "groupControl5";
            this.groupControl5.Size = new System.Drawing.Size(1531, 77);
            this.groupControl5.TabIndex = 2;
            this.groupControl5.Text = ":: Dados Bancarios ::";
            // 
            // spinEdit1
            // 
            this.spinEdit1.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.cONDOMINIOSBindingSource, "CONDiasExpiraRegistro", true));
            this.spinEdit1.EditValue = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.spinEdit1.Location = new System.Drawing.Point(540, 49);
            this.spinEdit1.Name = "spinEdit1";
            this.spinEdit1.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.spinEdit1.Properties.MaxValue = new decimal(new int[] {
            90,
            0,
            0,
            0});
            this.spinEdit1.Size = new System.Drawing.Size(89, 20);
            this.spinEdit1.TabIndex = 13;
            // 
            // cONDOMINIOSBindingSource
            // 
            this.cONDOMINIOSBindingSource.DataMember = "CONDOMINIOS";
            this.cONDOMINIOSBindingSource.DataSource = typeof(CadastrosProc.Condominios.dCondominiosCampos);
            // 
            // lookUpTipoEnvio
            // 
            this.lookUpTipoEnvio.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.cONDOMINIOSBindingSource, "CONRemessa_TEA", true));
            this.lookUpTipoEnvio.Location = new System.Drawing.Point(887, 25);
            this.lookUpTipoEnvio.Name = "lookUpTipoEnvio";
            this.lookUpTipoEnvio.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.True;
            this.lookUpTipoEnvio.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.lookUpTipoEnvio.Properties.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("TEANome", "TEA Nome", 5, DevExpress.Utils.FormatType.None, "", true, DevExpress.Utils.HorzAlignment.Near, DevExpress.Data.ColumnSortOrder.None, DevExpress.Utils.DefaultBoolean.Default)});
            this.lookUpTipoEnvio.Properties.DataSource = this.tipoEnvioArquivosBindingSource;
            this.lookUpTipoEnvio.Properties.DisplayMember = "TEANome";
            this.lookUpTipoEnvio.Properties.NullText = "";
            this.lookUpTipoEnvio.Properties.ShowHeader = false;
            this.lookUpTipoEnvio.Properties.ValueMember = "TEA";
            this.lookUpTipoEnvio.Size = new System.Drawing.Size(143, 20);
            this.lookUpTipoEnvio.TabIndex = 10;
            // 
            // tipoEnvioArquivosBindingSource
            // 
            this.tipoEnvioArquivosBindingSource.DataMember = "TipoEnvioArquivos";
            this.tipoEnvioArquivosBindingSource.DataSource = typeof(Framework.datasets.dTipoEnvioArquivos);
            // 
            // ComboTipoContaPEDefault
            // 
            this.ComboTipoContaPEDefault.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.cONDOMINIOSBindingSource, "CONTipoContaPE", true));
            this.ComboTipoContaPEDefault.Location = new System.Drawing.Point(800, 49);
            this.ComboTipoContaPEDefault.Name = "ComboTipoContaPEDefault";
            this.ComboTipoContaPEDefault.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.ComboTipoContaPEDefault.Size = new System.Drawing.Size(230, 20);
            this.ComboTipoContaPEDefault.TabIndex = 11;
            // 
            // txtCodComunicacao
            // 
            this.txtCodComunicacao.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.cONDOMINIOSBindingSource, "CONCodigoComunicacao", true));
            this.txtCodComunicacao.Location = new System.Drawing.Point(800, 25);
            this.txtCodComunicacao.Name = "txtCodComunicacao";
            this.txtCodComunicacao.Size = new System.Drawing.Size(81, 20);
            this.txtCodComunicacao.TabIndex = 9;
            this.txtCodComunicacao.Validating += new System.ComponentModel.CancelEventHandler(this.txtCodComunicacao_Validating);
            // 
            // txtCodigoCNR
            // 
            this.txtCodigoCNR.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.cONDOMINIOSBindingSource, "CONCodigoCNR", true));
            this.txtCodigoCNR.Location = new System.Drawing.Point(540, 23);
            this.txtCodigoCNR.Name = "txtCodigoCNR";
            this.txtCodigoCNR.Size = new System.Drawing.Size(89, 20);
            this.txtCodigoCNR.TabIndex = 7;
            // 
            // textEdit8
            // 
            this.textEdit8.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.cONDOMINIOSBindingSource, "CONDigitoConta", true));
            this.textEdit8.Location = new System.Drawing.Point(414, 49);
            this.textEdit8.Name = "textEdit8";
            this.textEdit8.Properties.MaxLength = 1;
            this.textEdit8.Size = new System.Drawing.Size(33, 20);
            this.textEdit8.TabIndex = 5;
            // 
            // textEdit7
            // 
            this.textEdit7.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.cONDOMINIOSBindingSource, "CONConta", true));
            this.textEdit7.Location = new System.Drawing.Point(338, 49);
            this.textEdit7.Name = "textEdit7";
            this.textEdit7.Properties.MaxLength = 6;
            this.textEdit7.Size = new System.Drawing.Size(70, 20);
            this.textEdit7.TabIndex = 4;
            // 
            // textEdit6
            // 
            this.textEdit6.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.cONDOMINIOSBindingSource, "CONDigitoAgencia", true));
            this.textEdit6.Location = new System.Drawing.Point(414, 25);
            this.textEdit6.Name = "textEdit6";
            this.textEdit6.Properties.MaxLength = 1;
            this.textEdit6.Size = new System.Drawing.Size(33, 20);
            this.textEdit6.TabIndex = 3;
            // 
            // textEdit5
            // 
            this.textEdit5.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.cONDOMINIOSBindingSource, "CONAgencia", true));
            this.textEdit5.Location = new System.Drawing.Point(338, 25);
            this.textEdit5.Name = "textEdit5";
            this.textEdit5.Properties.MaxLength = 4;
            this.textEdit5.Size = new System.Drawing.Size(70, 20);
            this.textEdit5.TabIndex = 2;
            // 
            // lookUpEdit2
            // 
            this.lookUpEdit2.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.cONDOMINIOSBindingSource, "CON_BCO", true));
            this.lookUpEdit2.Location = new System.Drawing.Point(57, 25);
            this.lookUpEdit2.Name = "lookUpEdit2";
            this.lookUpEdit2.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.lookUpEdit2.Properties.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("BCONome", "Nome")});
            this.lookUpEdit2.Properties.DataSource = this.bANCOSBindingSource;
            this.lookUpEdit2.Properties.DisplayMember = "BCONome";
            this.lookUpEdit2.Properties.NullText = " --";
            this.lookUpEdit2.Properties.ValueMember = "BCO";
            this.lookUpEdit2.Size = new System.Drawing.Size(224, 20);
            this.lookUpEdit2.TabIndex = 1;
            // 
            // bANCOSBindingSource
            // 
            this.bANCOSBindingSource.DataMember = "BANCOS";
            this.bANCOSBindingSource.DataSource = typeof(Framework.datasets.dBANCOS);
            // 
            // groupControl1
            // 
            this.groupControl1.Controls.Add(cONValorFRLabel);
            this.groupControl1.Controls.Add(this.cONValorFRCalcEdit);
            this.groupControl1.Controls.Add(this.cONTipoFRRadioGroup);
            this.groupControl1.Dock = System.Windows.Forms.DockStyle.Left;
            this.groupControl1.Location = new System.Drawing.Point(2, 2);
            this.groupControl1.Name = "groupControl1";
            this.groupControl1.Size = new System.Drawing.Size(269, 78);
            this.groupControl1.TabIndex = 3;
            this.groupControl1.Text = ":: Fundo De Reserva ::";
            // 
            // cONValorFRCalcEdit
            // 
            this.cONValorFRCalcEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.cONDOMINIOSBindingSource, "CONValorFR", true));
            this.cONValorFRCalcEdit.Location = new System.Drawing.Point(158, 28);
            this.cONValorFRCalcEdit.Name = "cONValorFRCalcEdit";
            this.cONValorFRCalcEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.cONValorFRCalcEdit.Size = new System.Drawing.Size(100, 20);
            this.cONValorFRCalcEdit.TabIndex = 2;
            // 
            // cONTipoFRRadioGroup
            // 
            this.cONTipoFRRadioGroup.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.cONDOMINIOSBindingSource, "CONTipoFR", true));
            this.cONTipoFRRadioGroup.Dock = System.Windows.Forms.DockStyle.Left;
            this.cONTipoFRRadioGroup.Location = new System.Drawing.Point(2, 20);
            this.cONTipoFRRadioGroup.Name = "cONTipoFRRadioGroup";
            this.cONTipoFRRadioGroup.Properties.Items.AddRange(new DevExpress.XtraEditors.Controls.RadioGroupItem[] {
            new DevExpress.XtraEditors.Controls.RadioGroupItem("P", "% do Condominio"),
            new DevExpress.XtraEditors.Controls.RadioGroupItem("F", "Valor Fixo")});
            this.cONTipoFRRadioGroup.Size = new System.Drawing.Size(113, 56);
            this.cONTipoFRRadioGroup.TabIndex = 1;
            // 
            // groupControl4
            // 
            this.groupControl4.Controls.Add(this.txtRegra);
            this.groupControl4.Controls.Add(cONDiaVencimentoLabel);
            this.groupControl4.Controls.Add(this.speVencimento);
            this.groupControl4.Dock = System.Windows.Forms.DockStyle.Left;
            this.groupControl4.Location = new System.Drawing.Point(2, 2);
            this.groupControl4.Name = "groupControl4";
            this.groupControl4.Size = new System.Drawing.Size(561, 51);
            this.groupControl4.TabIndex = 6;
            this.groupControl4.Text = ":: Vencimento ::";
            // 
            // txtRegra
            // 
            this.txtRegra.Location = new System.Drawing.Point(140, 24);
            this.txtRegra.Name = "txtRegra";
            this.txtRegra.Size = new System.Drawing.Size(396, 20);
            this.txtRegra.TabIndex = 2;
            // 
            // speVencimento
            // 
            this.speVencimento.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.cONDOMINIOSBindingSource, "CONDiaVencimento", true));
            this.speVencimento.EditValue = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.speVencimento.Location = new System.Drawing.Point(34, 24);
            this.speVencimento.Name = "speVencimento";
            this.speVencimento.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(),
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.speVencimento.Properties.ButtonClick += new DevExpress.XtraEditors.Controls.ButtonPressedEventHandler(this.cONDiaVencimentoSpinEdit_Properties_ButtonClick);
            this.speVencimento.Size = new System.Drawing.Size(100, 20);
            this.speVencimento.TabIndex = 1;
            this.speVencimento.TextChanged += new System.EventHandler(this.speVencimento_TextChanged);
            // 
            // groupControl6
            // 
            this.groupControl6.Controls.Add(this.groupControl3);
            this.groupControl6.Controls.Add(this.groupControl2);
            this.groupControl6.Controls.Add(this.groupControl1);
            this.groupControl6.Dock = System.Windows.Forms.DockStyle.Top;
            this.groupControl6.Location = new System.Drawing.Point(0, 77);
            this.groupControl6.Name = "groupControl6";
            this.groupControl6.ShowCaption = false;
            this.groupControl6.Size = new System.Drawing.Size(1531, 82);
            this.groupControl6.TabIndex = 7;
            this.groupControl6.Text = "groupControl6";
            // 
            // groupControl3
            // 
            this.groupControl3.Controls.Add(cONValorGasLabel);
            this.groupControl3.Controls.Add(this.cONValorGasCalcEdit);
            this.groupControl3.Controls.Add(cONValorCondominioLabel);
            this.groupControl3.Controls.Add(this.cONValorCondominioCalcEdit);
            this.groupControl3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.groupControl3.Location = new System.Drawing.Point(703, 2);
            this.groupControl3.Name = "groupControl3";
            this.groupControl3.Size = new System.Drawing.Size(826, 78);
            this.groupControl3.TabIndex = 6;
            this.groupControl3.Text = ":: Outros Valores ::";
            // 
            // cONValorGasCalcEdit
            // 
            this.cONValorGasCalcEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.cONDOMINIOSBindingSource, "CONValorGas", true));
            this.cONValorGasCalcEdit.Location = new System.Drawing.Point(81, 48);
            this.cONValorGasCalcEdit.Name = "cONValorGasCalcEdit";
            this.cONValorGasCalcEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.cONValorGasCalcEdit.Properties.DisplayFormat.FormatString = "n4";
            this.cONValorGasCalcEdit.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.cONValorGasCalcEdit.Properties.EditFormat.FormatString = "n4";
            this.cONValorGasCalcEdit.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.cONValorGasCalcEdit.Properties.Mask.EditMask = "c4";
            this.cONValorGasCalcEdit.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.cONValorGasCalcEdit.Size = new System.Drawing.Size(100, 20);
            this.cONValorGasCalcEdit.TabIndex = 3;
            // 
            // cONValorCondominioCalcEdit
            // 
            this.cONValorCondominioCalcEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.cONDOMINIOSBindingSource, "CONValorCondominio", true));
            this.cONValorCondominioCalcEdit.Location = new System.Drawing.Point(81, 25);
            this.cONValorCondominioCalcEdit.Name = "cONValorCondominioCalcEdit";
            this.cONValorCondominioCalcEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.cONValorCondominioCalcEdit.Properties.Mask.EditMask = "c2";
            this.cONValorCondominioCalcEdit.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.cONValorCondominioCalcEdit.Size = new System.Drawing.Size(100, 20);
            this.cONValorCondominioCalcEdit.TabIndex = 1;
            // 
            // groupControl2
            // 
            this.groupControl2.Controls.Add(this.checkEdit3);
            this.groupControl2.Controls.Add(this.imageComboBoxEdit2);
            this.groupControl2.Controls.Add(this.imageComboBoxEdit1);
            this.groupControl2.Controls.Add(this.cONValorMultaCalcEdit);
            this.groupControl2.Controls.Add(cONValorMultaLabel);
            this.groupControl2.Controls.Add(cONValorJurosLabel);
            this.groupControl2.Controls.Add(this.cONValorJurosCalcEdit);
            this.groupControl2.Dock = System.Windows.Forms.DockStyle.Left;
            this.groupControl2.Location = new System.Drawing.Point(271, 2);
            this.groupControl2.Name = "groupControl2";
            this.groupControl2.Size = new System.Drawing.Size(432, 78);
            this.groupControl2.TabIndex = 5;
            this.groupControl2.Text = ":: Multa e Juros ::";
            // 
            // checkEdit3
            // 
            this.checkEdit3.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.cONDOMINIOSBindingSource, "CONJurosPrimeiroMes", true));
            this.checkEdit3.Location = new System.Drawing.Point(241, 49);
            this.checkEdit3.Name = "checkEdit3";
            this.checkEdit3.Properties.Caption = "Cobrar antes de um m�s";
            this.checkEdit3.Size = new System.Drawing.Size(216, 19);
            this.checkEdit3.TabIndex = 12;
            // 
            // imageComboBoxEdit2
            // 
            this.imageComboBoxEdit2.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.cONDOMINIOSBindingSource, "CONTipoMulta", true));
            this.imageComboBoxEdit2.Location = new System.Drawing.Point(131, 25);
            this.imageComboBoxEdit2.Name = "imageComboBoxEdit2";
            this.imageComboBoxEdit2.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.imageComboBoxEdit2.Properties.Items.AddRange(new DevExpress.XtraEditors.Controls.ImageComboBoxItem[] {
            new DevExpress.XtraEditors.Controls.ImageComboBoxItem("Di�ria", "D", -1),
            new DevExpress.XtraEditors.Controls.ImageComboBoxItem("Mensal", "M", -1)});
            this.imageComboBoxEdit2.Size = new System.Drawing.Size(104, 20);
            toolTipTitleItem1.Text = "Multa";
            toolTipItem1.LeftIndent = 6;
            toolTipItem1.Text = "Mesal: Aplicadicada integralmente ap�s o vencimento\r\nDi�ria: Aplicada ap�s o venc" +
    "imento e limitada a 30 dias";
            superToolTip1.Items.Add(toolTipTitleItem1);
            superToolTip1.Items.Add(toolTipItem1);
            this.imageComboBoxEdit2.SuperTip = superToolTip1;
            this.imageComboBoxEdit2.TabIndex = 11;
            this.imageComboBoxEdit2.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Asterisk;
            // 
            // imageComboBoxEdit1
            // 
            this.imageComboBoxEdit1.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.cONDOMINIOSBindingSource, "CONJurosCompostos", true));
            this.imageComboBoxEdit1.Location = new System.Drawing.Point(131, 48);
            this.imageComboBoxEdit1.Name = "imageComboBoxEdit1";
            this.imageComboBoxEdit1.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.imageComboBoxEdit1.Properties.Items.AddRange(new DevExpress.XtraEditors.Controls.ImageComboBoxItem[] {
            new DevExpress.XtraEditors.Controls.ImageComboBoxItem("Simpes", false, -1),
            new DevExpress.XtraEditors.Controls.ImageComboBoxItem("Compostos", true, -1)});
            this.imageComboBoxEdit1.Size = new System.Drawing.Size(104, 20);
            this.imageComboBoxEdit1.TabIndex = 10;
            // 
            // cONValorMultaCalcEdit
            // 
            this.cONValorMultaCalcEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.cONDOMINIOSBindingSource, "CONValorMulta", true));
            this.cONValorMultaCalcEdit.Location = new System.Drawing.Point(48, 25);
            this.cONValorMultaCalcEdit.Name = "cONValorMultaCalcEdit";
            this.cONValorMultaCalcEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.cONValorMultaCalcEdit.Properties.DisplayFormat.FormatString = "0.";
            this.cONValorMultaCalcEdit.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.cONValorMultaCalcEdit.Properties.Mask.EditMask = "P";
            this.cONValorMultaCalcEdit.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.cONValorMultaCalcEdit.Size = new System.Drawing.Size(81, 20);
            this.cONValorMultaCalcEdit.TabIndex = 8;
            // 
            // cONValorJurosCalcEdit
            // 
            this.cONValorJurosCalcEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.cONDOMINIOSBindingSource, "CONValorJuros", true));
            this.cONValorJurosCalcEdit.Location = new System.Drawing.Point(48, 48);
            this.cONValorJurosCalcEdit.Name = "cONValorJurosCalcEdit";
            this.cONValorJurosCalcEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.cONValorJurosCalcEdit.Properties.DisplayFormat.FormatString = "0.";
            this.cONValorJurosCalcEdit.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.cONValorJurosCalcEdit.Properties.Mask.EditMask = "P";
            this.cONValorJurosCalcEdit.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.cONValorJurosCalcEdit.Size = new System.Drawing.Size(81, 20);
            this.cONValorJurosCalcEdit.TabIndex = 2;
            // 
            // groupControl7
            // 
            this.groupControl7.Controls.Add(this.groupControl8);
            this.groupControl7.Controls.Add(this.groupControl4);
            this.groupControl7.Dock = System.Windows.Forms.DockStyle.Top;
            this.groupControl7.Location = new System.Drawing.Point(0, 159);
            this.groupControl7.Name = "groupControl7";
            this.groupControl7.ShowCaption = false;
            this.groupControl7.Size = new System.Drawing.Size(1531, 55);
            this.groupControl7.TabIndex = 8;
            this.groupControl7.Text = "groupControl7";
            // 
            // groupControl8
            // 
            this.groupControl8.Controls.Add(this.checkEdit4);
            this.groupControl8.Dock = System.Windows.Forms.DockStyle.Fill;
            this.groupControl8.Location = new System.Drawing.Point(563, 2);
            this.groupControl8.Name = "groupControl8";
            this.groupControl8.Size = new System.Drawing.Size(966, 51);
            this.groupControl8.TabIndex = 7;
            this.groupControl8.Text = ":: Cheques ::";
            // 
            // checkEdit4
            // 
            this.checkEdit4.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.cONDOMINIOSBindingSource, "CONDivideCheque", true));
            this.checkEdit4.Location = new System.Drawing.Point(14, 24);
            this.checkEdit4.Name = "checkEdit4";
            this.checkEdit4.Properties.Caption = "Dividir cheques de valor elevado";
            this.checkEdit4.Size = new System.Drawing.Size(216, 19);
            this.checkEdit4.TabIndex = 1;
            // 
            // groupControl10
            // 
            this.groupControl10.Controls.Add(this.groupControl14);
            this.groupControl10.Controls.Add(this.groupControl13);
            this.groupControl10.Controls.Add(this.groupControl12);
            this.groupControl10.Controls.Add(this.panelControl1);
            this.groupControl10.Controls.Add(this.groupControl11);
            this.groupControl10.Dock = System.Windows.Forms.DockStyle.Top;
            this.groupControl10.Location = new System.Drawing.Point(0, 214);
            this.groupControl10.Name = "groupControl10";
            this.groupControl10.Size = new System.Drawing.Size(1531, 126);
            this.groupControl10.TabIndex = 9;
            this.groupControl10.Text = ":: Correio / Despesa de boleto ::";
            // 
            // groupControl14
            // 
            this.groupControl14.Controls.Add(this.radioGroup2);
            this.groupControl14.Dock = System.Windows.Forms.DockStyle.Fill;
            this.groupControl14.Location = new System.Drawing.Point(563, 47);
            this.groupControl14.Name = "groupControl14";
            this.groupControl14.Size = new System.Drawing.Size(966, 77);
            this.groupControl14.TabIndex = 10;
            this.groupControl14.Text = ":: Boleto ::";
            // 
            // radioGroup2
            // 
            this.radioGroup2.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.cONDOMINIOSBindingSource, "CONCorreioBoleto", true));
            this.radioGroup2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.radioGroup2.Location = new System.Drawing.Point(2, 20);
            this.radioGroup2.Name = "radioGroup2";
            this.radioGroup2.Properties.Items.AddRange(new DevExpress.XtraEditors.Controls.RadioGroupItem[] {
            new DevExpress.XtraEditors.Controls.RadioGroupItem("AR", "AR"),
            new DevExpress.XtraEditors.Controls.RadioGroupItem("CS", "Carta Simples")});
            this.radioGroup2.Size = new System.Drawing.Size(962, 55);
            this.radioGroup2.TabIndex = 2;
            // 
            // groupControl13
            // 
            this.groupControl13.Controls.Add(this.radioGroup1);
            this.groupControl13.Dock = System.Windows.Forms.DockStyle.Left;
            this.groupControl13.Location = new System.Drawing.Point(428, 47);
            this.groupControl13.Name = "groupControl13";
            this.groupControl13.Size = new System.Drawing.Size(135, 77);
            this.groupControl13.TabIndex = 9;
            this.groupControl13.Text = "::Cobran�a ::";
            // 
            // radioGroup1
            // 
            this.radioGroup1.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.cONDOMINIOSBindingSource, "CONCorreioCobranca", true));
            this.radioGroup1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.radioGroup1.Location = new System.Drawing.Point(2, 20);
            this.radioGroup1.Name = "radioGroup1";
            this.radioGroup1.Properties.Items.AddRange(new DevExpress.XtraEditors.Controls.RadioGroupItem[] {
            new DevExpress.XtraEditors.Controls.RadioGroupItem("AR", "AR"),
            new DevExpress.XtraEditors.Controls.RadioGroupItem("CS", "Carta Simples")});
            this.radioGroup1.Size = new System.Drawing.Size(131, 55);
            this.radioGroup1.TabIndex = 2;
            // 
            // groupControl12
            // 
            this.groupControl12.Controls.Add(this.cONCorreioAssembleiaRadioGroup);
            this.groupControl12.Dock = System.Windows.Forms.DockStyle.Left;
            this.groupControl12.Location = new System.Drawing.Point(281, 47);
            this.groupControl12.Name = "groupControl12";
            this.groupControl12.Size = new System.Drawing.Size(147, 77);
            this.groupControl12.TabIndex = 8;
            this.groupControl12.Text = ":: Assembl�ia ::";
            // 
            // cONCorreioAssembleiaRadioGroup
            // 
            this.cONCorreioAssembleiaRadioGroup.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.cONDOMINIOSBindingSource, "CONCorreioAssembleia", true));
            this.cONCorreioAssembleiaRadioGroup.Dock = System.Windows.Forms.DockStyle.Fill;
            this.cONCorreioAssembleiaRadioGroup.Location = new System.Drawing.Point(2, 20);
            this.cONCorreioAssembleiaRadioGroup.Name = "cONCorreioAssembleiaRadioGroup";
            this.cONCorreioAssembleiaRadioGroup.Properties.Items.AddRange(new DevExpress.XtraEditors.Controls.RadioGroupItem[] {
            new DevExpress.XtraEditors.Controls.RadioGroupItem("AR", "AR"),
            new DevExpress.XtraEditors.Controls.RadioGroupItem("CS", "Carta Simples")});
            this.cONCorreioAssembleiaRadioGroup.Size = new System.Drawing.Size(143, 55);
            this.cONCorreioAssembleiaRadioGroup.TabIndex = 1;
            // 
            // panelControl1
            // 
            this.panelControl1.Controls.Add(this.checkEdit2);
            this.panelControl1.Controls.Add(this.checkEdit1);
            this.panelControl1.Controls.Add(this.cONRepassaCorreioCheckEdit);
            this.panelControl1.Dock = System.Windows.Forms.DockStyle.Top;
            this.panelControl1.Location = new System.Drawing.Point(281, 20);
            this.panelControl1.Name = "panelControl1";
            this.panelControl1.Size = new System.Drawing.Size(1248, 27);
            this.panelControl1.TabIndex = 7;
            // 
            // checkEdit2
            // 
            this.checkEdit2.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.cONDOMINIOSBindingSource, "CONCobrarSegVia", true));
            this.checkEdit2.Location = new System.Drawing.Point(341, 5);
            this.checkEdit2.Name = "checkEdit2";
            this.checkEdit2.Properties.Caption = "Cobrar 2� Via";
            this.checkEdit2.Size = new System.Drawing.Size(109, 19);
            this.checkEdit2.TabIndex = 3;
            // 
            // checkEdit1
            // 
            this.checkEdit1.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.cONDOMINIOSBindingSource, "CONRepassaDespBanc", true));
            this.checkEdit1.Location = new System.Drawing.Point(131, 5);
            this.checkEdit1.Name = "checkEdit1";
            this.checkEdit1.Properties.Caption = "Repassar as despesas banc�rias";
            this.checkEdit1.Size = new System.Drawing.Size(181, 19);
            this.checkEdit1.TabIndex = 2;
            // 
            // cONRepassaCorreioCheckEdit
            // 
            this.cONRepassaCorreioCheckEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.cONDOMINIOSBindingSource, "CONRepassaCorreio", true));
            this.cONRepassaCorreioCheckEdit.Location = new System.Drawing.Point(9, 5);
            this.cONRepassaCorreioCheckEdit.Name = "cONRepassaCorreioCheckEdit";
            this.cONRepassaCorreioCheckEdit.Properties.Caption = "Repassar correio.";
            this.cONRepassaCorreioCheckEdit.Size = new System.Drawing.Size(116, 19);
            this.cONRepassaCorreioCheckEdit.TabIndex = 1;
            // 
            // groupControl11
            // 
            this.groupControl11.Controls.Add(this.calcEdit2);
            this.groupControl11.Controls.Add(label3);
            this.groupControl11.Controls.Add(this.calcEdit1);
            this.groupControl11.Controls.Add(label1);
            this.groupControl11.Controls.Add(cONValorCartaSimplesLabel);
            this.groupControl11.Controls.Add(this.cONValorCartaSimplesCalcEdit);
            this.groupControl11.Controls.Add(cONValorARLabel);
            this.groupControl11.Controls.Add(this.cONValorARCalcEdit);
            this.groupControl11.Dock = System.Windows.Forms.DockStyle.Left;
            this.groupControl11.Location = new System.Drawing.Point(2, 20);
            this.groupControl11.Name = "groupControl11";
            this.groupControl11.Size = new System.Drawing.Size(279, 104);
            this.groupControl11.TabIndex = 2;
            this.groupControl11.Text = ":: Valor ::";
            // 
            // calcEdit2
            // 
            this.calcEdit2.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.cONDOMINIOSBindingSource, "CONCustoSegVia", true));
            this.calcEdit2.Location = new System.Drawing.Point(178, 26);
            this.calcEdit2.Name = "calcEdit2";
            this.calcEdit2.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.calcEdit2.Properties.Mask.EditMask = "c2";
            this.calcEdit2.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.calcEdit2.Size = new System.Drawing.Size(86, 20);
            this.calcEdit2.TabIndex = 7;
            // 
            // calcEdit1
            // 
            this.calcEdit1.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.cONDOMINIOSBindingSource, "CONValorBoleto", true));
            this.calcEdit1.Location = new System.Drawing.Point(45, 78);
            this.calcEdit1.Name = "calcEdit1";
            this.calcEdit1.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.calcEdit1.Properties.Mask.EditMask = "c2";
            this.calcEdit1.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.calcEdit1.Size = new System.Drawing.Size(86, 20);
            this.calcEdit1.TabIndex = 5;
            // 
            // cONValorCartaSimplesCalcEdit
            // 
            this.cONValorCartaSimplesCalcEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.cONDOMINIOSBindingSource, "CONValorCartaSimples", true));
            this.cONValorCartaSimplesCalcEdit.Location = new System.Drawing.Point(45, 52);
            this.cONValorCartaSimplesCalcEdit.Name = "cONValorCartaSimplesCalcEdit";
            this.cONValorCartaSimplesCalcEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.cONValorCartaSimplesCalcEdit.Properties.Mask.EditMask = "c2";
            this.cONValorCartaSimplesCalcEdit.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.cONValorCartaSimplesCalcEdit.Size = new System.Drawing.Size(86, 20);
            this.cONValorCartaSimplesCalcEdit.TabIndex = 3;
            // 
            // cONValorARCalcEdit
            // 
            this.cONValorARCalcEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.cONDOMINIOSBindingSource, "CONValorAR", true));
            this.cONValorARCalcEdit.Location = new System.Drawing.Point(45, 26);
            this.cONValorARCalcEdit.Name = "cONValorARCalcEdit";
            this.cONValorARCalcEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.cONValorARCalcEdit.Properties.Mask.EditMask = "c2";
            this.cONValorARCalcEdit.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.cONValorARCalcEdit.Size = new System.Drawing.Size(86, 20);
            this.cONValorARCalcEdit.TabIndex = 1;
            // 
            // groupControl15
            // 
            this.groupControl15.Controls.Add(this.gridControl1);
            this.groupControl15.Dock = System.Windows.Forms.DockStyle.Fill;
            this.groupControl15.Location = new System.Drawing.Point(0, 340);
            this.groupControl15.Name = "groupControl15";
            this.groupControl15.Size = new System.Drawing.Size(1531, 133);
            this.groupControl15.TabIndex = 10;
            this.groupControl15.Text = "Pagamentos espec�ficos";
            // 
            // gridControl1
            // 
            this.gridControl1.Cursor = System.Windows.Forms.Cursors.Default;
            this.gridControl1.DataSource = this.fKPaGamentoEspecificoCONDOMINIOSBindingSource;
            this.gridControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridControl1.Location = new System.Drawing.Point(2, 20);
            this.gridControl1.MainView = this.gridView1;
            this.gridControl1.Name = "gridControl1";
            this.gridControl1.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.LookUpPLA,
            this.LookUpUsuario,
            this.repositoryItemButtonEdit1,
            this.LookUpCTL});
            this.gridControl1.Size = new System.Drawing.Size(1527, 111);
            this.gridControl1.TabIndex = 0;
            this.gridControl1.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridView1});
            // 
            // fKPaGamentoEspecificoCONDOMINIOSBindingSource
            // 
            this.fKPaGamentoEspecificoCONDOMINIOSBindingSource.DataMember = "FK_PaGamentoEspecifico_CONDOMINIOS";
            this.fKPaGamentoEspecificoCONDOMINIOSBindingSource.DataSource = this.cONDOMINIOSBindingSource;
            // 
            // gridView1
            // 
            this.gridView1.Appearance.ColumnFilterButton.BackColor = System.Drawing.Color.Orange;
            this.gridView1.Appearance.ColumnFilterButton.BorderColor = System.Drawing.Color.Orange;
            this.gridView1.Appearance.ColumnFilterButton.ForeColor = System.Drawing.Color.DarkOrange;
            this.gridView1.Appearance.ColumnFilterButton.Options.UseBackColor = true;
            this.gridView1.Appearance.ColumnFilterButton.Options.UseBorderColor = true;
            this.gridView1.Appearance.ColumnFilterButton.Options.UseForeColor = true;
            this.gridView1.Appearance.ColumnFilterButtonActive.BackColor = System.Drawing.Color.DarkOrange;
            this.gridView1.Appearance.ColumnFilterButtonActive.BorderColor = System.Drawing.Color.DarkOrange;
            this.gridView1.Appearance.ColumnFilterButtonActive.ForeColor = System.Drawing.Color.White;
            this.gridView1.Appearance.ColumnFilterButtonActive.Options.UseBackColor = true;
            this.gridView1.Appearance.ColumnFilterButtonActive.Options.UseBorderColor = true;
            this.gridView1.Appearance.ColumnFilterButtonActive.Options.UseForeColor = true;
            this.gridView1.Appearance.Empty.BackColor = System.Drawing.Color.LightSkyBlue;
            this.gridView1.Appearance.Empty.BackColor2 = System.Drawing.Color.SkyBlue;
            this.gridView1.Appearance.Empty.GradientMode = System.Drawing.Drawing2D.LinearGradientMode.BackwardDiagonal;
            this.gridView1.Appearance.Empty.Options.UseBackColor = true;
            this.gridView1.Appearance.EvenRow.BackColor = System.Drawing.Color.Linen;
            this.gridView1.Appearance.EvenRow.BackColor2 = System.Drawing.Color.AntiqueWhite;
            this.gridView1.Appearance.EvenRow.GradientMode = System.Drawing.Drawing2D.LinearGradientMode.ForwardDiagonal;
            this.gridView1.Appearance.EvenRow.Options.UseBackColor = true;
            this.gridView1.Appearance.FilterCloseButton.BackColor = System.Drawing.Color.Orange;
            this.gridView1.Appearance.FilterCloseButton.BorderColor = System.Drawing.Color.Orange;
            this.gridView1.Appearance.FilterCloseButton.Options.UseBackColor = true;
            this.gridView1.Appearance.FilterCloseButton.Options.UseBorderColor = true;
            this.gridView1.Appearance.FilterPanel.BackColor = System.Drawing.Color.DarkOrange;
            this.gridView1.Appearance.FilterPanel.BackColor2 = System.Drawing.Color.Orange;
            this.gridView1.Appearance.FilterPanel.ForeColor = System.Drawing.Color.Black;
            this.gridView1.Appearance.FilterPanel.GradientMode = System.Drawing.Drawing2D.LinearGradientMode.BackwardDiagonal;
            this.gridView1.Appearance.FilterPanel.Options.UseBackColor = true;
            this.gridView1.Appearance.FilterPanel.Options.UseForeColor = true;
            this.gridView1.Appearance.FocusedRow.BackColor = System.Drawing.Color.RoyalBlue;
            this.gridView1.Appearance.FocusedRow.BackColor2 = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this.gridView1.Appearance.FocusedRow.ForeColor = System.Drawing.Color.White;
            this.gridView1.Appearance.FocusedRow.GradientMode = System.Drawing.Drawing2D.LinearGradientMode.Vertical;
            this.gridView1.Appearance.FocusedRow.Options.UseBackColor = true;
            this.gridView1.Appearance.FocusedRow.Options.UseForeColor = true;
            this.gridView1.Appearance.FooterPanel.BackColor = System.Drawing.Color.Orange;
            this.gridView1.Appearance.FooterPanel.BorderColor = System.Drawing.Color.Orange;
            this.gridView1.Appearance.FooterPanel.Options.UseBackColor = true;
            this.gridView1.Appearance.FooterPanel.Options.UseBorderColor = true;
            this.gridView1.Appearance.GroupButton.BackColor = System.Drawing.Color.Wheat;
            this.gridView1.Appearance.GroupButton.BorderColor = System.Drawing.Color.Wheat;
            this.gridView1.Appearance.GroupButton.Options.UseBackColor = true;
            this.gridView1.Appearance.GroupButton.Options.UseBorderColor = true;
            this.gridView1.Appearance.GroupFooter.BackColor = System.Drawing.Color.Wheat;
            this.gridView1.Appearance.GroupFooter.BorderColor = System.Drawing.Color.Wheat;
            this.gridView1.Appearance.GroupFooter.Options.UseBackColor = true;
            this.gridView1.Appearance.GroupFooter.Options.UseBorderColor = true;
            this.gridView1.Appearance.GroupPanel.BackColor = System.Drawing.Color.RoyalBlue;
            this.gridView1.Appearance.GroupPanel.ForeColor = System.Drawing.Color.White;
            this.gridView1.Appearance.GroupPanel.Options.UseBackColor = true;
            this.gridView1.Appearance.GroupPanel.Options.UseForeColor = true;
            this.gridView1.Appearance.GroupRow.BackColor = System.Drawing.Color.Wheat;
            this.gridView1.Appearance.GroupRow.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Bold);
            this.gridView1.Appearance.GroupRow.Options.UseBackColor = true;
            this.gridView1.Appearance.GroupRow.Options.UseFont = true;
            this.gridView1.Appearance.HeaderPanel.BackColor = System.Drawing.Color.Orange;
            this.gridView1.Appearance.HeaderPanel.BorderColor = System.Drawing.Color.Orange;
            this.gridView1.Appearance.HeaderPanel.Options.UseBackColor = true;
            this.gridView1.Appearance.HeaderPanel.Options.UseBorderColor = true;
            this.gridView1.Appearance.HideSelectionRow.BackColor = System.Drawing.Color.LightSlateGray;
            this.gridView1.Appearance.HideSelectionRow.Options.UseBackColor = true;
            this.gridView1.Appearance.HorzLine.BackColor = System.Drawing.Color.Tan;
            this.gridView1.Appearance.HorzLine.Options.UseBackColor = true;
            this.gridView1.Appearance.OddRow.BackColor = System.Drawing.Color.White;
            this.gridView1.Appearance.OddRow.Options.UseBackColor = true;
            this.gridView1.Appearance.Preview.BackColor = System.Drawing.Color.Khaki;
            this.gridView1.Appearance.Preview.BackColor2 = System.Drawing.Color.Cornsilk;
            this.gridView1.Appearance.Preview.Font = new System.Drawing.Font("Tahoma", 7.5F);
            this.gridView1.Appearance.Preview.GradientMode = System.Drawing.Drawing2D.LinearGradientMode.BackwardDiagonal;
            this.gridView1.Appearance.Preview.Options.UseBackColor = true;
            this.gridView1.Appearance.Preview.Options.UseFont = true;
            this.gridView1.Appearance.Row.BackColor = System.Drawing.Color.White;
            this.gridView1.Appearance.Row.Options.UseBackColor = true;
            this.gridView1.Appearance.RowSeparator.BackColor = System.Drawing.Color.LightSkyBlue;
            this.gridView1.Appearance.RowSeparator.Options.UseBackColor = true;
            this.gridView1.Appearance.VertLine.BackColor = System.Drawing.Color.Tan;
            this.gridView1.Appearance.VertLine.Options.UseBackColor = true;
            this.gridView1.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colPGE_PLA,
            this.colPGEValor,
            this.colPGEMensagem,
            this.colPGEDATAA,
            this.colPGEDATAI,
            this.colPGEA_USU,
            this.colPGEI_USU,
            this.gridColumn1,
            this.colPGE_CTL});
            this.gridView1.GridControl = this.gridControl1;
            this.gridView1.Name = "gridView1";
            this.gridView1.OptionsView.ColumnAutoWidth = false;
            this.gridView1.OptionsView.EnableAppearanceEvenRow = true;
            this.gridView1.OptionsView.EnableAppearanceOddRow = true;
            this.gridView1.OptionsView.NewItemRowPosition = DevExpress.XtraGrid.Views.Grid.NewItemRowPosition.Top;
            this.gridView1.OptionsView.ShowButtonMode = DevExpress.XtraGrid.Views.Base.ShowButtonModeEnum.ShowForFocusedRow;
            this.gridView1.OptionsView.ShowGroupPanel = false;
            this.gridView1.SortInfo.AddRange(new DevExpress.XtraGrid.Columns.GridColumnSortInfo[] {
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colPGEMensagem, DevExpress.Data.ColumnSortOrder.Ascending)});
            this.gridView1.ValidateRow += new DevExpress.XtraGrid.Views.Base.ValidateRowEventHandler(this.gridView1_ValidateRow);
            // 
            // colPGE_PLA
            // 
            this.colPGE_PLA.Caption = "Tipo";
            this.colPGE_PLA.ColumnEdit = this.LookUpPLA;
            this.colPGE_PLA.FieldName = "PGE_PLA";
            this.colPGE_PLA.Name = "colPGE_PLA";
            this.colPGE_PLA.Visible = true;
            this.colPGE_PLA.VisibleIndex = 0;
            this.colPGE_PLA.Width = 156;
            // 
            // LookUpPLA
            // 
            this.LookUpPLA.AutoHeight = false;
            this.LookUpPLA.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LookUpPLA.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("PLA", "C�digo", 37, DevExpress.Utils.FormatType.None, "", true, DevExpress.Utils.HorzAlignment.Near, DevExpress.Data.ColumnSortOrder.None, DevExpress.Utils.DefaultBoolean.Default),
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("PLADescricao", "Descri��o", 70, DevExpress.Utils.FormatType.None, "", true, DevExpress.Utils.HorzAlignment.Near, DevExpress.Data.ColumnSortOrder.None, DevExpress.Utils.DefaultBoolean.Default)});
            this.LookUpPLA.DataSource = this.pLAnocontasBindingSource;
            this.LookUpPLA.DisplayMember = "DescricaoPLA";
            this.LookUpPLA.Name = "LookUpPLA";
            this.LookUpPLA.NullText = "";
            this.LookUpPLA.PopupWidth = 400;
            this.LookUpPLA.ValueMember = "PLA";
            this.LookUpPLA.Modified += new System.EventHandler(this.LookUpPLA_Modified);
            this.LookUpPLA.Validating += new System.ComponentModel.CancelEventHandler(this.LookUpPLA_Validating);
            // 
            // pLAnocontasBindingSource
            // 
            this.pLAnocontasBindingSource.DataMember = "PLAnocontas";
            this.pLAnocontasBindingSource.DataSource = typeof(Framework.datasets.dPLAnocontas);
            // 
            // colPGEValor
            // 
            this.colPGEValor.Caption = "Valor";
            this.colPGEValor.FieldName = "PGEValor";
            this.colPGEValor.Name = "colPGEValor";
            this.colPGEValor.Visible = true;
            this.colPGEValor.VisibleIndex = 1;
            // 
            // colPGEMensagem
            // 
            this.colPGEMensagem.Caption = "Mensagem";
            this.colPGEMensagem.FieldName = "PGEMensagem";
            this.colPGEMensagem.Name = "colPGEMensagem";
            this.colPGEMensagem.Visible = true;
            this.colPGEMensagem.VisibleIndex = 2;
            this.colPGEMensagem.Width = 156;
            // 
            // colPGEDATAA
            // 
            this.colPGEDATAA.FieldName = "PGEDATAA";
            this.colPGEDATAA.Name = "colPGEDATAA";
            this.colPGEDATAA.OptionsColumn.AllowEdit = false;
            this.colPGEDATAA.Visible = true;
            this.colPGEDATAA.VisibleIndex = 6;
            this.colPGEDATAA.Width = 60;
            // 
            // colPGEDATAI
            // 
            this.colPGEDATAI.FieldName = "PGEDATAI";
            this.colPGEDATAI.Name = "colPGEDATAI";
            this.colPGEDATAI.OptionsColumn.AllowEdit = false;
            this.colPGEDATAI.Visible = true;
            this.colPGEDATAI.VisibleIndex = 4;
            this.colPGEDATAI.Width = 66;
            // 
            // colPGEA_USU
            // 
            this.colPGEA_USU.Caption = "Atualiza��o";
            this.colPGEA_USU.ColumnEdit = this.LookUpUsuario;
            this.colPGEA_USU.FieldName = "PGEA_USU";
            this.colPGEA_USU.Name = "colPGEA_USU";
            this.colPGEA_USU.OptionsColumn.AllowEdit = false;
            this.colPGEA_USU.Visible = true;
            this.colPGEA_USU.VisibleIndex = 5;
            this.colPGEA_USU.Width = 93;
            // 
            // LookUpUsuario
            // 
            this.LookUpUsuario.AutoHeight = false;
            this.LookUpUsuario.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LookUpUsuario.DataSource = this.uSUARIOSBindingSource1;
            this.LookUpUsuario.DisplayMember = "USUNome";
            this.LookUpUsuario.Name = "LookUpUsuario";
            this.LookUpUsuario.NullText = "";
            this.LookUpUsuario.ValueMember = "USU";
            // 
            // uSUARIOSBindingSource1
            // 
            this.uSUARIOSBindingSource1.DataMember = "USUarios";
            this.uSUARIOSBindingSource1.DataSource = typeof(FrameworkProc.datasets.dUSUarios);
            // 
            // colPGEI_USU
            // 
            this.colPGEI_USU.Caption = "Cadastro";
            this.colPGEI_USU.ColumnEdit = this.LookUpUsuario;
            this.colPGEI_USU.FieldName = "PGEI_USU";
            this.colPGEI_USU.Name = "colPGEI_USU";
            this.colPGEI_USU.OptionsColumn.AllowEdit = false;
            this.colPGEI_USU.Visible = true;
            this.colPGEI_USU.VisibleIndex = 3;
            this.colPGEI_USU.Width = 84;
            // 
            // gridColumn1
            // 
            this.gridColumn1.ColumnEdit = this.repositoryItemButtonEdit1;
            this.gridColumn1.Fixed = DevExpress.XtraGrid.Columns.FixedStyle.Right;
            this.gridColumn1.Name = "gridColumn1";
            this.gridColumn1.OptionsColumn.AllowMove = false;
            this.gridColumn1.OptionsColumn.AllowSize = false;
            this.gridColumn1.OptionsColumn.FixedWidth = true;
            this.gridColumn1.OptionsColumn.ShowCaption = false;
            this.gridColumn1.ShowButtonMode = DevExpress.XtraGrid.Views.Base.ShowButtonModeEnum.ShowAlways;
            this.gridColumn1.Visible = true;
            this.gridColumn1.VisibleIndex = 8;
            this.gridColumn1.Width = 40;
            // 
            // repositoryItemButtonEdit1
            // 
            this.repositoryItemButtonEdit1.AutoHeight = false;
            this.repositoryItemButtonEdit1.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(),
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Delete)});
            this.repositoryItemButtonEdit1.Name = "repositoryItemButtonEdit1";
            this.repositoryItemButtonEdit1.ButtonClick += new DevExpress.XtraEditors.Controls.ButtonPressedEventHandler(this.repositoryItemButtonEdit1_ButtonClick);
            // 
            // colPGE_CTL
            // 
            this.colPGE_CTL.Caption = "Conta";
            this.colPGE_CTL.ColumnEdit = this.LookUpCTL;
            this.colPGE_CTL.FieldName = "PGE_CTL";
            this.colPGE_CTL.Name = "colPGE_CTL";
            this.colPGE_CTL.Visible = true;
            this.colPGE_CTL.VisibleIndex = 7;
            this.colPGE_CTL.Width = 131;
            // 
            // LookUpCTL
            // 
            this.LookUpCTL.AutoHeight = false;
            this.LookUpCTL.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LookUpCTL.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("CTLTitulo", "CTL Titulo", 57, DevExpress.Utils.FormatType.None, "", true, DevExpress.Utils.HorzAlignment.Near, DevExpress.Data.ColumnSortOrder.None, DevExpress.Utils.DefaultBoolean.Default)});
            this.LookUpCTL.DataSource = this.conTasLogicasBindingSource;
            this.LookUpCTL.DisplayMember = "CTLTitulo";
            this.LookUpCTL.Name = "LookUpCTL";
            this.LookUpCTL.ShowHeader = false;
            this.LookUpCTL.ValueMember = "CTL";
            // 
            // conTasLogicasBindingSource
            // 
            this.conTasLogicasBindingSource.DataMember = "ConTasLogicas";
            this.conTasLogicasBindingSource.DataSource = this.dConta;
            // 
            // dConta
            // 
            this.dConta.DataSetName = "dConta";
            this.dConta.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // TabFinaceiro
            // 
            this.Controls.Add(this.groupControl15);
            this.Controls.Add(this.groupControl10);
            this.Controls.Add(this.groupControl7);
            this.Controls.Add(this.groupControl6);
            this.Controls.Add(this.groupControl5);
            this.Doc = System.Windows.Forms.DockStyle.Fill;
            this.LookAndFeel.UseDefaultLookAndFeel = false;
            this.Name = "TabFinaceiro";
            this.Size = new System.Drawing.Size(1531, 473);
            this.Load += new System.EventHandler(this.TabFinaceiro_Load);
            ((System.ComponentModel.ISupportInitialize)(this.StyleController_F)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl5)).EndInit();
            this.groupControl5.ResumeLayout(false);
            this.groupControl5.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.spinEdit1.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cONDOMINIOSBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lookUpTipoEnvio.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tipoEnvioArquivosBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ComboTipoContaPEDefault.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCodComunicacao.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCodigoCNR.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit8.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit7.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit6.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit5.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lookUpEdit2.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.bANCOSBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl1)).EndInit();
            this.groupControl1.ResumeLayout(false);
            this.groupControl1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.cONValorFRCalcEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cONTipoFRRadioGroup.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl4)).EndInit();
            this.groupControl4.ResumeLayout(false);
            this.groupControl4.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtRegra.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.speVencimento.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl6)).EndInit();
            this.groupControl6.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.groupControl3)).EndInit();
            this.groupControl3.ResumeLayout(false);
            this.groupControl3.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.cONValorGasCalcEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cONValorCondominioCalcEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl2)).EndInit();
            this.groupControl2.ResumeLayout(false);
            this.groupControl2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit3.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.imageComboBoxEdit2.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.imageComboBoxEdit1.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cONValorMultaCalcEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cONValorJurosCalcEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl7)).EndInit();
            this.groupControl7.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.groupControl8)).EndInit();
            this.groupControl8.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit4.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl10)).EndInit();
            this.groupControl10.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.groupControl14)).EndInit();
            this.groupControl14.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.radioGroup2.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl13)).EndInit();
            this.groupControl13.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.radioGroup1.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl12)).EndInit();
            this.groupControl12.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.cONCorreioAssembleiaRadioGroup.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl1)).EndInit();
            this.panelControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit2.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit1.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cONRepassaCorreioCheckEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl11)).EndInit();
            this.groupControl11.ResumeLayout(false);
            this.groupControl11.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.calcEdit2.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.calcEdit1.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cONValorCartaSimplesCalcEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cONValorARCalcEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl15)).EndInit();
            this.groupControl15.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gridControl1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.fKPaGamentoEspecificoCONDOMINIOSBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LookUpPLA)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pLAnocontasBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LookUpUsuario)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uSUARIOSBindingSource1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemButtonEdit1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LookUpCTL)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.conTasLogicasBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dConta)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraEditors.GroupControl groupControl5;
        private DevExpress.XtraEditors.TextEdit textEdit8;
        private DevExpress.XtraEditors.TextEdit textEdit7;
        private DevExpress.XtraEditors.TextEdit textEdit6;
        private DevExpress.XtraEditors.TextEdit textEdit5;
        private DevExpress.XtraEditors.LookUpEdit lookUpEdit2;
        private System.Windows.Forms.BindingSource cONDOMINIOSBindingSource;
        private System.Windows.Forms.BindingSource bANCOSBindingSource;
        private DevExpress.XtraEditors.GroupControl groupControl1;
        private DevExpress.XtraEditors.GroupControl groupControl4;
        private DevExpress.XtraEditors.GroupControl groupControl6;
        private DevExpress.XtraEditors.GroupControl groupControl2;
        private DevExpress.XtraEditors.RadioGroup cONTipoFRRadioGroup;
        private DevExpress.XtraEditors.CalcEdit cONValorFRCalcEdit;
        private DevExpress.XtraEditors.CalcEdit cONValorJurosCalcEdit;
        private DevExpress.XtraEditors.CalcEdit cONValorMultaCalcEdit;
        private DevExpress.XtraEditors.GroupControl groupControl3;
        private DevExpress.XtraEditors.CalcEdit cONValorGasCalcEdit;
        private DevExpress.XtraEditors.CalcEdit cONValorCondominioCalcEdit;
        private DevExpress.XtraEditors.SpinEdit speVencimento;
        private DevExpress.XtraEditors.GroupControl groupControl7;
        private DevExpress.XtraEditors.TextEdit txtRegra;
        private DevExpress.XtraEditors.GroupControl groupControl10;
        private DevExpress.XtraEditors.GroupControl groupControl11;
        private DevExpress.XtraEditors.CalcEdit cONValorCartaSimplesCalcEdit;
        private DevExpress.XtraEditors.CalcEdit cONValorARCalcEdit;
        private DevExpress.XtraEditors.GroupControl groupControl15;
        private DevExpress.XtraGrid.GridControl gridControl1;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView1;
        private System.Windows.Forms.BindingSource fKPaGamentoEspecificoCONDOMINIOSBindingSource;
        private DevExpress.XtraGrid.Columns.GridColumn colPGE_PLA;
        private DevExpress.XtraGrid.Columns.GridColumn colPGEValor;
        private DevExpress.XtraGrid.Columns.GridColumn colPGEMensagem;
        private DevExpress.XtraGrid.Columns.GridColumn colPGEDATAA;
        private DevExpress.XtraGrid.Columns.GridColumn colPGEDATAI;
        private DevExpress.XtraGrid.Columns.GridColumn colPGEA_USU;
        private DevExpress.XtraGrid.Columns.GridColumn colPGEI_USU;
        private DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit LookUpPLA;
        private System.Windows.Forms.BindingSource pLAnocontasBindingSource;
        //private Cadastros.PlanoDeContas.dPlanoContas dPlanoContas;
        //private Cadastros.PlanoDeContas.dPlanoContasTableAdapters.PLAnocontasTableAdapter pLAnocontasTableAdapter;
        private DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit LookUpUsuario;
        private System.Windows.Forms.BindingSource uSUARIOSBindingSource1;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn1;
        private DevExpress.XtraEditors.Repository.RepositoryItemButtonEdit repositoryItemButtonEdit1;
        private DevExpress.XtraEditors.PanelControl panelControl1;
        private DevExpress.XtraEditors.CheckEdit checkEdit1;
        private DevExpress.XtraEditors.CheckEdit cONRepassaCorreioCheckEdit;
        private DevExpress.XtraEditors.GroupControl groupControl14;
        private DevExpress.XtraEditors.RadioGroup radioGroup2;
        private DevExpress.XtraEditors.GroupControl groupControl13;
        private DevExpress.XtraEditors.RadioGroup radioGroup1;
        private DevExpress.XtraEditors.GroupControl groupControl12;
        private DevExpress.XtraEditors.RadioGroup cONCorreioAssembleiaRadioGroup;
        private DevExpress.XtraEditors.CalcEdit calcEdit1;
        private DevExpress.XtraEditors.TextEdit txtCodigoCNR;
        private DevExpress.XtraEditors.CheckEdit checkEdit2;
        private DevExpress.XtraEditors.CalcEdit calcEdit2;
        private DevExpress.XtraEditors.ImageComboBoxEdit imageComboBoxEdit1;
        private DevExpress.XtraEditors.ImageComboBoxEdit imageComboBoxEdit2;
        private DevExpress.XtraEditors.TextEdit txtCodComunicacao;
        private DevExpress.XtraEditors.CheckEdit checkEdit3;
        private DevExpress.XtraEditors.ImageComboBoxEdit ComboTipoContaPEDefault;
        private DevExpress.XtraEditors.GroupControl groupControl8;
        private DevExpress.XtraEditors.CheckEdit checkEdit4;
        private System.Windows.Forms.Label label5;
        private DevExpress.XtraEditors.LookUpEdit lookUpTipoEnvio;
        private System.Windows.Forms.BindingSource tipoEnvioArquivosBindingSource;
        private DevExpress.XtraGrid.Columns.GridColumn colPGE_CTL;
        private DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit LookUpCTL;
        private System.Windows.Forms.BindingSource conTasLogicasBindingSource;
        private dConta dConta;
        private DevExpress.XtraEditors.SpinEdit spinEdit1;
    }
}
