/*
MR - 14/01/2016 12:30           - Tratamento do campo de n�mero sequencial de remessa para tributos do condominio, podendo reiniciar manualmente (Altera��es indicadas por *** MRC - INICIO - TRIBUTOS (14/01/2016 12:30) ***)
*/

//*** MRC - INICIO - TRIBUTOS (14/01/2016 12:30) ***
using System.Data;
//*** MRC - TERMINO - TRIBUTOS (14/01/2016 12:30) ***

namespace Cadastros.Condominios.PaginasCondominio
{
    /// <summary>
    /// 
    /// </summary>
    public partial class TabDP : CompontesBasicos.ComponenteTabCampos
    {
        /// <summary>
        /// 
        /// </summary>
        public TabDP()
        {
            InitializeComponent();
        }

        //*** MRC - INICIO - TRIBUTOS (14/01/2016 12:30) ***
        private void cmdIniciarRemessa_Click(object sender, System.EventArgs e)
        {
            //Inicia o valor da remessa para 99998 (assim o proximo enviado sera 99999)
            txtSequencialRemessaTributos.Text = "99998";
            ((DataRowView)(cONDOMINIOSBindingSource.Current))["CONSequencialRemessaTributos"] = txtSequencialRemessaTributos.Text;       
        }
        //*** MRC - TERMINO - TRIBUTOS (14/01/2016 12:30) ***
    }
}

