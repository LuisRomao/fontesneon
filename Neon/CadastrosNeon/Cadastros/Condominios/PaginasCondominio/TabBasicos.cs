using System;
using System.ComponentModel;
using DocBacarios;
using System.Globalization;
using System.Linq;
using System.Xml.Linq;

namespace Cadastros.Condominios.PaginasCondominio
{
    /// <summary>
    /// 
    /// </summary>
    public partial class TabBasicos : CompontesBasicos.ComponenteTabCampos
    {
        /// <summary>
        /// 
        /// </summary>
        public TabBasicos()
        {
            InitializeComponent();
            cIDADESBindingSource.DataSource = Framework.datasets.dCIDADES.dCIDADESSt;
            bANCOSBindingSource.DataSource = Framework.datasets.dBANCOS.dBANCOSSt;

        }

        private cCondominiosCampos2 Chamador
        {
            get { return (cCondominiosCampos2)TabMae; }
        }

        //public string strDeTeste;

        private static void Truncar(DevExpress.XtraEditors.TextEdit Controle, string Valor)
        {

            Controle.Text = (Controle.Properties.MaxLength >= Valor.Length) ? Valor : Valor.Substring(0, Controle.Properties.MaxLength);
            Controle.EditValue = Controle.Text;
        }

        private void simpleButton1_Click(object sender, EventArgs e)
        {
            if (VirMSSQL.CEP.STCEP.BuscaPorCEP(cONCepTextEdit.Text))
            {
                Truncar(cONCepTextEdit, VirMSSQL.CEP.STCEP.CEPFormatado);
                Truncar(cONEnderecoTextEdit, VirMSSQL.CEP.STCEP.Rua);
                Chamador.linhaMae.CONEndereco = cONEnderecoTextEdit.Text;
                Truncar(cONBairroTextEdit, VirMSSQL.CEP.STCEP.Bairro);
                Chamador.linhaMae.CONBairro = cONBairroTextEdit.Text;
                if (VirMSSQL.CEP.STCEP.NovaCidade)
                    Framework.datasets.dCIDADES.dCIDADESSt.RefreshDados();
                cON_CIDLookUpEdit.EditValue = VirMSSQL.CEP.STCEP.nCidadeNeon;
                Chamador.linhaMae.CON_CID = VirMSSQL.CEP.STCEP.nCidadeNeon;
            }
        }

        private void TabBasicos_Load(object sender, EventArgs e)
        {
            groupControl2.Enabled = true;
            if (cON_LATTextEdit.Text == "0" || cON_LATTextEdit.Text == "0")
                cmdSitePINMapa.Text = "Site - Exibir PIN no Mapa";
            else
                cmdSitePINMapa.Text = "Site - Retirar PIN do Mapa";
        }

        private void cON_CIDLookUpEdit_EditValueChanged(object sender, EventArgs e)
        {
            lookUpEdit1.EditValue = cON_CIDLookUpEdit.EditValue;
        }

        private void cONCnpjTextEdit_Validating(object sender, CancelEventArgs e)
        {
            CPFCNPJ cnpj = new CPFCNPJ(cONCnpjTextEdit.Text);
            if (cnpj.Tipo == TipoCpfCnpj.INVALIDO)
            {
                cONCnpjTextEdit.ErrorText = "CNPJ inv�lido";
                e.Cancel = true;
            }
            else
                cONCnpjTextEdit.Text = cnpj.ToString(true);
        }

        private void cmdSitePINMapa_Click(object sender, EventArgs e)
        {
            if (cmdSitePINMapa.Text.Contains("Exibir"))
            {
                XDocument doc = XDocument.Load(string.Format("http://maps.googleapis.com/maps/api/geocode/xml?address={0}&sensor=false", Uri.EscapeDataString(cONEnderecoTextEdit.Text + " " + cONCepTextEdit.Text)));

                string status = doc.Descendants("status").FirstOrDefault().Value;
                if (!(status == "OVER_QUERY_LIMIT" || status == "REQUEST_DENIED"))
                {
                    var els = doc.Descendants("result").Descendants("geometry").Descendants("location").FirstOrDefault();
                    if (null != els)
                    {
                        cON_LATTextEdit.Text = Double.Parse((els.Nodes().First() as XElement).Value, new CultureInfo("en-US")).ToString();
                        cON_LNGTextEdit.Text = Double.Parse((els.Nodes().ElementAt(1) as XElement).Value, new CultureInfo("en-US")).ToString();
                    }
                }
            }
            else
                cON_LATTextEdit.Text = cON_LNGTextEdit.Text = "0";

            if (cON_LATTextEdit.Text == "0" || cON_LATTextEdit.Text == "0")
                cmdSitePINMapa.Text = "Site - Exibir PIN no Mapa";
            else
                cmdSitePINMapa.Text = "Site - Retirar PIN do Mapa";
        }
    }
}
