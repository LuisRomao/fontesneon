/*
MR - 05/08/2014 11:00 -           - Inclus�o do campo tipo de conta do condominio para pagamento eletr�nico (Altera��es indicadas por *** MRC - INICIO - PAG-FOR (05/08/2014 11:00) ***)
MR - 08/08/2014 11:00 - 14.1.4.22 - Tratamento do campo Cod.Comunica��o alertando a emiss�o de pagamentos pendentes caso a informa��o seja excluida (Altera��es indicadas por *** MRC - INICIO - PAG-FOR (08/08/2014 11:00) ***)
MR - 10/02/2015 12:00 -           - Inclus�o do campo tipo de envio (de arquivos) como um lookupedit, para pagamento eletr�nico (Altera��es indicadas por *** MRC - INICIO - PAG-FOR (10/02/2015 12:00) ***)
MR - 25/02/2016 11:00 -           - Inclus�o do campo boleto com registro como uma booleana, para remessa de arquivos de cobranca com registro (Altera��es indicadas por *** MRC - INICIO - REMESSA (25/02/2016 11:00) ***)
*/

using System;
using System.ComponentModel;
using System.Data;
using System.Windows.Forms;
using Framework;
using VirEnumeracoesNeon;
using CadastrosProc.Condominios;

namespace Cadastros.Condominios.PaginasCondominio
{
    /// <summary>
    /// 
    /// </summary>
    public partial class TabFinaceiro : CompontesBasicos.ComponenteTabCampos
    {
        /// <summary>
        /// 
        /// </summary>
        public TabFinaceiro()
        {
            InitializeComponent();
        }

        private void speVencimento_TextChanged(object sender, EventArgs e)
        {
            if (cONDOMINIOSBindingSource.Current == null)
                return;
            Int32 _Dia = Convert.ToInt32(speVencimento.Value);

            String _TipoDia = ((DataRowView)(cONDOMINIOSBindingSource.Current))["CONTipoVencimento"].ToString();

            String _OrdemContagem = ((DataRowView)(cONDOMINIOSBindingSource.Current))["CONReferenciaVencimento"].ToString();

            txtRegra.Text = RegraVencimento(_Dia, _TipoDia, _OrdemContagem);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="Dia"></param>
        /// <param name="TipoDia"></param>
        /// <param name="OrdemContagem"></param>
        /// <returns></returns>
        public static string RegraVencimento(Int32 Dia, String TipoDia, String OrdemContagem)
        {
            string _Vencto = null;

            _Vencto = Dia.ToString() + "� DIA";

            _Vencto += (TipoDia == "C" ? " CORRIDO " : " �TIL ");

            _Vencto += (OrdemContagem == "I" ? " A PARTIR DO �NICIO DO M�S " : " A PARTIR DO FINAL DO M�S ");

            return _Vencto;
        }

        private void cONDiaVencimentoSpinEdit_Properties_ButtonClick(object sender, DevExpress.XtraEditors.Controls.ButtonPressedEventArgs e)
        {
            if (e.Button.Kind == DevExpress.XtraEditors.Controls.ButtonPredefines.Ellipsis) //Incluir
            {
                this.Validate();

                this.cONDOMINIOSBindingSource.EndEdit();

                Int32 _Dia = Convert.ToInt32(((DataRowView)(cONDOMINIOSBindingSource.Current))["CONDiaVencimento"]);

                String _TipoDia = ((DataRowView)(cONDOMINIOSBindingSource.Current))["CONTipoVencimento"].ToString();

                String _OrdemContagem = ((DataRowView)(cONDOMINIOSBindingSource.Current))["CONReferenciaVencimento"].ToString();

                if (DialogResult.Cancel == ShowModulo(typeof(cVencimento), cONDOMINIOSBindingSource, false))
                    this.cONDOMINIOSBindingSource.CancelEdit();

                else
                {
                    _Dia = Convert.ToInt32(((DataRowView)(cONDOMINIOSBindingSource.Current))["CONDiaVencimento"]);

                    _TipoDia = ((DataRowView)(cONDOMINIOSBindingSource.Current))["CONTipoVencimento"].ToString();

                    _OrdemContagem = ((DataRowView)(cONDOMINIOSBindingSource.Current))["CONReferenciaVencimento"].ToString();

                    ((DataRowView)(cONDOMINIOSBindingSource.Current)).EndEdit();

                    txtRegra.Text = RegraVencimento(_Dia, _TipoDia, _OrdemContagem);
                }
            }
        }

        private cCondominiosCampos2 Pai { get { return (cCondominiosCampos2)Chamador; } }


        /*
        private cCondominiosCampos2 BuscaPai()
        {
            Pai = (cCondominiosCampos2)Chamador;
            /*
            if (Pai == null)
            {
                Control Candidato;
                for (Candidato = this.Parent; !(Candidato is cCondominiosCampos2); Candidato = Candidato.Parent) ;
                Pai = (cCondominiosCampos2)Candidato;
            };
            return Pai;
        }*/

        private int? CTLCaixa 
        {
            get 
            {
                foreach (dConta.ConTasLogicasRow row in dConta.ConTasLogicas)
                    if (row.CTLTipo == (int)CTLTipo.Caixa)
                        return row.CTL;
                return null;
            }            
        }

        /// <summary>
        /// Chamado na troca de p�gina
        /// </summary>
        /// <param name="Entrando"></param>
        public override void OnTrocaPagina(bool Entrando)
        {
            if (Entrando)
            {
                dConta.ConTasLogicasTableAdapter.FillByCON(dConta.ConTasLogicas, Pai.pk);
                foreach(dCondominiosCampos.PaGamentoEspecificoRow rowPGE in Pai.dCondominiosCampos.PaGamentoEspecifico)
                    if (rowPGE.IsPGE_CTLNull() && CTLCaixa.HasValue)
                    {
                        rowPGE.PGE_CTL = CTLCaixa.Value;
                        Pai.paGamentoEspecificoTableAdapter.Update(rowPGE);
                    }
            }
            base.OnTrocaPagina(Entrando);
        }

        private void TabFinaceiro_Load(object sender, EventArgs e)
        {            
            Enumeracoes.VirEnumTipoContaPE.CarregaEditorDaGrid(ComboTipoContaPEDefault);         
            //BuscaPai();            
            pLAnocontasBindingSource.DataSource = Framework.datasets.dPLAnocontas.dPLAnocontasStReceitas;
            pLAnocontasBindingSource.DataMember = "PLAnocontas";
            bANCOSBindingSource.DataSource = Framework.datasets.dBANCOS.dBANCOSSt;
            uSUARIOSBindingSource1.DataSource = FrameworkProc.datasets.dUSUarios.dUSUariosSt;            
            tipoEnvioArquivosBindingSource.DataSource = Framework.datasets.dTipoEnvioArquivos.dTipoEnvioArquivosSt;            
            Pai.paGamentoEspecificoTableAdapter.Fill(Pai.dCondominiosCampos.PaGamentoEspecifico, Pai.pk);
        }

        private void gridView1_ValidateRow(object sender, DevExpress.XtraGrid.Views.Base.ValidateRowEventArgs e)
        {

            DataRowView DRV = (DataRowView)e.Row;
            dCondominiosCampos.PaGamentoEspecificoRow linha = (dCondominiosCampos.PaGamentoEspecificoRow)DRV.Row;
            if (linha["PGE_PLA"] == DBNull.Value)
            {
                e.Valid = false;
                e.ErrorText = "Campo 'Tipo' obrigat�rio";

            }
            else
            {
                if (linha["PGEValor"] == DBNull.Value)
                {
                    e.Valid = false;
                    e.ErrorText = "Campo 'Valor' obrigat�rio";

                }
                else
                {

                    if (linha.RowState == DataRowState.Detached)
                    {
                        linha.PGEI_USU = CompontesBasicos.Bancovirtual.UsuarioLogado.UsuarioLogadoSt.USU;
                        linha.PGEDATAI = DateTime.Now;
                    }
                    else
                    {
                        linha.PGEA_USU = CompontesBasicos.Bancovirtual.UsuarioLogado.UsuarioLogadoSt.USU;
                        linha.PGEDATAA = DateTime.Now;
                    };

                    if ((linha["PGEMensagem"] == DBNull.Value) && (linha.PGE_PLA != ""))
                        linha.PGEMensagem = Framework.datasets.dPLAnocontas.dPLAnocontasSt.PLAnocontas.FindByPLA(linha.PGE_PLA).PLADescricaoRes;
                };
            }
        }

        private void LookUpPLA_Modified(object sender, EventArgs e)
        {
            dCondominiosCampos.PaGamentoEspecificoRow linha;
            DevExpress.XtraEditors.LookUpEdit Editor = (DevExpress.XtraEditors.LookUpEdit)sender;
            DataRowView DRV = (DataRowView)fKPaGamentoEspecificoCONDOMINIOSBindingSource.Current;
            if (DRV != null)
            {
                linha = (dCondominiosCampos.PaGamentoEspecificoRow)DRV.Row;
                linha.PGEMensagem = Editor.GetColumnValue("PLADescricao").ToString();
            }
        }

        private void LookUpPLA_Validating(object sender, CancelEventArgs e)
        {
            
        }

        private void repositoryItemButtonEdit1_ButtonClick(object sender, DevExpress.XtraEditors.Controls.ButtonPressedEventArgs e)
        {

            dCondominiosCampos.PaGamentoEspecificoRow linha;            
            DataRowView DRV = (DataRowView)fKPaGamentoEspecificoCONDOMINIOSBindingSource.Current;
            if (DRV != null)
            {
                if (this.Pai.dCondominiosCampos.HasChanges())
                    if (!this.Pai.Grava(true))
                        return;
                linha = (dCondominiosCampos.PaGamentoEspecificoRow)DRV.Row;
                switch(e.Button.Kind){
                    case DevExpress.XtraEditors.Controls.ButtonPredefines.Ellipsis:
                       
                       cPagEspecificos CPag = new cPagEspecificos(linha.PGE_CON);
                       ShowModulo(CPag);
                       break;
                    case DevExpress.XtraEditors.Controls.ButtonPredefines.Delete:
                        object oLinhas =  Pagamentos_Especificos.dPagEspecificos.PaGamentoEspecificoTableAdapter.Detalhes(linha.PGE);
                        int nLinha;
                        if(oLinhas != null)
                            nLinha = (int)oLinhas;
                        else
                            nLinha = 0;
                        if (nLinha != 0)
                            MessageBox.Show("Este pagamento consta para " + nLinha + ((nLinha > 1) ? " unidades" : " unidade"));
                        else
                        {
                            Pagamentos_Especificos.dPagEspecificos.PaGamentoEspecificoTableAdapter.ApagaBrancos(linha.PGE);
                            linha.Delete();
                        }
                        //
                        break;
                }
            }            
            
        }

        
        private cCondominiosCampos2 Chamador
        {
            get { return (cCondominiosCampos2)TabMae; }
        }

        private void txtCodComunicacao_Validating(object sender, CancelEventArgs e)
        {
            //Verifica se esta com a informacao vazia
            if (txtCodComunicacao.Text == "")
            {
                //Verifica se existe pagamento eletronico cadastrado, pendente de emissao e envio para o banco
                int? intPEPendente = VirMSSQL.TableAdapter.STTableAdapter.BuscaEscalar_int("SELECT COUNT(*) FROM CHEques WHERE (CHE_CON = @P1) AND (CHEStatus IN (@P2,@P3)) AND (CHETipo IN (@P4,@P5,@P6,@P7,@P8))", Chamador.linhaMae.CON, CHEStatus.Cadastrado, CHEStatus.AguardaEnvioPE, TipoPagPeriodico.PECreditoConta, TipoPagPeriodico.PEOrdemPagamento, TipoPagPeriodico.PETituloBancario, TipoPagPeriodico.PETituloBancarioAgrupavel, TipoPagPeriodico.PETransferencia);
                if (intPEPendente > 0)
                {
                    MessageBox.Show("Existem pagamentos eletr�nicos j� cadastrados, por�m que ainda n�o foram emitidos ou enviados para o Banco. A informa��o do campo 'C�d. Comunica��o / Tipo Envio' n�o poder� ser exclu�da neste momento.");
                    txtCodComunicacao.Undo();
                }
            }
        }
        //*** MRC - TERMINO - PAG-FOR (08/08/2014 11:00) ***        
    }
}

