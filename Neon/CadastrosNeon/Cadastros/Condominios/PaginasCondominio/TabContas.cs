/*
LH 27/05/2014 - 13.2.9.4 - Nova estrura para rentabilidade e transferir arrecadado
*/

using System;
using System.Data;
using System.Drawing;
using System.Windows.Forms;
using CompontesBasicosProc;
using Framework;
using Framework.GrupoEnumeracoes;
using VirEnumeracoesNeon;
using CadastrosProc.Condominios;

namespace Cadastros.Condominios.PaginasCondominio
{
    /// <summary>
    /// Guia para confirgura��o das contas
    /// </summary>
    public partial class TabContas : CompontesBasicos.ComponenteTabCampos
    {
        /// <summary>
        /// Construtor
        /// </summary>
        public TabContas()
        {
            InitializeComponent();
            BANCOSbindingSource.DataSource = Framework.datasets.dBANCOS.dBANCOSSt;
            virEnumCTLTipo.virEnumCTLTipoSt.CarregaEditorDaGrid(colCTLTipo);
            virEnumCCTTipo.virEnumCCTTipoSt.CarregaEditorDaGrid(colCCTTipo);
            virEnumCTLCCTTranArrecadado.virEnumCTLCCTTranArrecadadoSt.CarregaEditorDaGrid(colCTLCCTTranArrecadado);
            //gridColumn2.Visible = gridColumn3.Visible = !somenteleitura;            
        }

        private dCondominiosCampos.CONDOMINIOSRow rowPrincipal {
            get {
                try
                {
                    DataRowView DRV = (DataRowView)cONDOMINIOSBindingSource.Current;
                    return (dCondominiosCampos.CONDOMINIOSRow)DRV.Row;
                }
                catch
                {
                    return null;
                }
            }
        }

        private int CON {
            get {
                try {
                    return rowPrincipal.CON;
                }
                catch {
                    return -1;
                }
            }
        }

        /// <summary>
        /// A��es na troca de p�gina
        /// </summary>
        /// <param name="Entrando">Entrando ou saindo</param>
        public override void OnTrocaPagina(bool Entrando)
        {
            if (Entrando)
            {
                panelControl1.Visible = (rowPrincipal.IsCONDivideSaldosNull() || !rowPrincipal.CONDivideSaldos);
                dConta1.Fill(CON);
                
                if (rowPrincipal.IsCONCodigoComunicacaoNull() || rowPrincipal.CONCodigoComunicacao == "")
                    colCCTPagamentoEletronico.Visible = false;
                gridColumn1.Visible = gridColumn2.Visible = gridColumn3.Visible = !TabMae.somenteleitura;
            }
            else
            {
                if (dConta1.ContaCorrenTe.Count == 0)
                    return;
                gridView1.CloseEditor();
                dConta1.ConTasLogicasTableAdapter.Update(dConta1.ConTasLogicas);
                dConta1.ConTasLogicas.AcceptChanges();
                if (rowPrincipal.IsCONDivideSaldosNull())
                    rowPrincipal.CONDivideSaldos = false;
                else
                    if (rowPrincipal.CONDivideSaldos)
                        if (!dConta1.validaContas())
                            MessageBox.Show(dConta1.ErroRentabilidade);
            }
        }

        private void gridView1_InitNewRow(object sender, DevExpress.XtraGrid.Views.Grid.InitNewRowEventArgs e)
        {
            DataRowView DRV = (DataRowView)gridView1.GetRow(e.RowHandle);
            dConta.ContaCorrenTeRow NovaRow = (dConta.ContaCorrenTeRow)DRV.Row;
            NovaRow.CCT_CON = CON;
            NovaRow.CCTAtiva = true;
            NovaRow.CCTLote = false;
            NovaRow.CCTPrincipal = false;            
            NovaRow.CCT_BCO = rowPrincipal.CON_BCO;
            NovaRow.CCTTipo = (int)CCTTipo.ContaCorrete;
            NovaRow.CCTAplicacao = false;
            NovaRow.CCTPagamentoEletronico = false;
            NovaRow.CCTDebitoAutomatico = false;
            NovaRow.CCTRateioCredito = false;
        }

        private void gridView1_ValidateRow(object sender, DevExpress.XtraGrid.Views.Base.ValidateRowEventArgs e)
        {
            //to do - verificar se a conta f�sica est� sem conta logica para rentabilidade
            DataRowView DRV = (DataRowView)gridView1.GetRow(e.RowHandle);
            dConta.ContaCorrenTeRow NovaRow = (dConta.ContaCorrenTeRow)DRV.Row;

            if (NovaRow.IsCCT_CONNull())
            {
                e.ErrorText = "Banco n�o definido";
                e.Valid = false;
            }
            else if ((NovaRow["CCTConta"] == DBNull.Value) || NovaRow.IsCCTContaDgNull())
            {
                e.ErrorText = "conta n�o definida";
                e.Valid = false;
            }
            else if ((NovaRow["CCTAgencia"] == DBNull.Value) || NovaRow.IsCCTAgenciaDgNull())
            {
                e.ErrorText = "ag�ncia n�o definida";
                e.Valid = false;
            }
            else if (NovaRow.IsCCTTipoNull())
            {
                e.ErrorText = "Definir Tipo de conta";
                e.Valid = false;
            } 
            /*
            if (NovaRow["CCT_BCO"] == DBNull.Value)
            {
                e.ErrorText = "Banco n�o definido";
                e.Valid = false;
            }
            else if (NovaRow["CCTConta"] == DBNull.Value)
            {
                e.ErrorText = "conta n�o definida";
                e.Valid = false;
            }
            else if (NovaRow["CCTAgencia"] == DBNull.Value)
            {
                e.ErrorText = "ag�ncia n�o definida";
                e.Valid = false;
            }
            else if (NovaRow["CCTTipo"] == DBNull.Value)
            {
                e.ErrorText = "Definir Tipo de conta";
                e.Valid = false;
            }*/
            else
            {
                if ((NovaRow.RowState == DataRowState.Detached) && (NovaRow.CCTTipo != (int)CCTTipo.ContaDaADM))
                {
                    foreach (dConta.ContaCorrenTeRow RowjaCadastrada in dConta1.ContaCorrenTe)
                    {
                        if ((RowjaCadastrada.CCTAgencia == NovaRow.CCTAgencia)
                            &&
                            (RowjaCadastrada.CCTConta == NovaRow.CCTConta)
                            &&
                            (RowjaCadastrada.CCTTipo == NovaRow.CCTTipo)
                            )
                        {
                            e.ErrorText = "Conta j� cadastrada";
                            e.Valid = false;
                        }
                    }

                    dConta.ContaCorrenTeRow RowCadastrada = dConta1.getContaCadastrada(NovaRow.CCT_BCO, NovaRow.CCTAgencia, NovaRow.CCTConta, NovaRow.CCTAplicacao);

                    if (RowCadastrada != null)
                    {
                        if (RowCadastrada.IsCCT_CONNull())
                        {
                            dConta1.GravaCON(CON, NovaRow.CCT_BCO, NovaRow.CCTAgencia, NovaRow.CCTConta);
                            NovaRow.Delete();
                            dConta1.ContaCorrenTeTableAdapter.Update(dConta1.ContaCorrenTe);
                            dConta1.ContaCorrenTeTableAdapter.Fill(dConta1.ContaCorrenTe, CON);
                        }
                        else
                        {
                            if (CON != RowCadastrada.CCT_CON)
                            {
                                FrameworkProc.datasets.dCondominiosAtivos.CONDOMINIOSRow rowCON = FrameworkProc.datasets.dCondominiosAtivos.dCondominiosTodosST.CONDOMINIOS.FindByCON(RowCadastrada.CCT_CON);
                                e.ErrorText = string.Format("Conta j� cadastrada para o condom�nio ({0}) {1}", rowCON.IsCONCodigoFolha1Null() ? 0 : rowCON.CONCodigoFolha1, rowCON.CONCodigo);
                                e.Valid = false;
                            }
                        }
                    };
                }
            };
        }

        private void gridView2_InitNewRow(object sender, DevExpress.XtraGrid.Views.Grid.InitNewRowEventArgs e)
        {
            dConta.ConTasLogicasRow NovaRow = (dConta.ConTasLogicasRow)gridView2.GetDataRow(e.RowHandle);
            NovaRow.CTL_CON = CON;
            NovaRow.CTLI_USU = CompontesBasicos.FormPrincipalBase.USULogado;
            NovaRow.CTLAtiva = true;
            NovaRow.CTLDATAI = DateTime.Now;                       
        }

        private void gridView1_RowCellStyle(object sender, DevExpress.XtraGrid.Views.Grid.RowCellStyleEventArgs e)
        {
            if (e.Column == colCCTTipo)
            {
                dConta.ContaCorrenTeRow rowCCT = (dConta.ContaCorrenTeRow)gridView1.GetDataRow(e.RowHandle);
                if (rowCCT != null)
                {
                    if(!rowCCT.IsCCTTipoNull())
                    {
                        e.Appearance.BackColor = virEnumCCTTipo.virEnumCCTTipoSt.GetCor(rowCCT.CCTTipo);
                        e.Appearance.BackColor2 = Color.White;
                        e.Appearance.ForeColor = Color.Black;
                    }
                    
                }
            }
        }

        private void gridView1_RowUpdated(object sender, DevExpress.XtraGrid.Views.Base.RowObjectEventArgs e)
        {
            DataRowView DRV = (DataRowView)e.Row;
            dConta.ContaCorrenTeRow rowCCT = (dConta.ContaCorrenTeRow)DRV.Row;
            if (rowCCT != null)
            {
                rowCCT.CCTAplicacao = (((CCTTipo)rowCCT.CCTTipo).EstaNoGrupo(CCTTipo.Aplicacao, CCTTipo.Poupanca));
                if (rowCCT.IsCCT_AGENull())
                {
                    int? AGE = VirMSSQL.TableAdapter.STTableAdapter.BuscaEscalar_int("SELECT AGE FROM AGEncia where AGE_BCO = @P1 and AGENumero = @P2", rowCCT.CCT_BCO, rowCCT.CCTAgencia);
                    if (AGE.HasValue)
                        rowCCT.CCT_AGE = AGE.Value;
                }
                bool ContaPlusInvalida = false;
                if (!rowCCT.IsCCT_CCTPlusNull())
                {
                    dConta.ContaCorrenTeRow rowCCTPlus = rowCCT.ContaCorrenTeRowParent;
                    if((!rowCCTPlus.CCTAtiva) || (!rowCCTPlus.CCTAplicacao))
                        ContaPlusInvalida = true;
                    else if ((rowCCT.CCT == rowCCTPlus.CCT) || (rowCCT.CCT_BCO != rowCCTPlus.CCT_BCO))
                        ContaPlusInvalida = true;
                    else
                        foreach (dConta.ContaCorrenTeRow CCTTeste in dConta1.ContaCorrenTe)
                            if ((CCTTeste.CCT != rowCCT.CCT) && (!CCTTeste.IsCCT_CCTPlusNull()) && (CCTTeste.CCT_CCTPlus == rowCCTPlus.CCT))
                                ContaPlusInvalida = true;
                    if (ContaPlusInvalida)                    
                        rowCCT.SetCCT_CCTPlusNull();                                            
                }
                dConta1.ContaCorrenTeTableAdapter.Update(rowCCT);
                rowCCT.AcceptChanges();
                if(ContaPlusInvalida)
                    MessageBox.Show(string.Format("Conta PLUS inv�lida"));
            }
        }

        private void repositoryItemButtonEdit1_ButtonClick(object sender, DevExpress.XtraEditors.Controls.ButtonPressedEventArgs e)
        {
            dConta.ContaCorrenTeRow rowCCT = (dConta.ContaCorrenTeRow)gridView1.GetFocusedDataRow();
            if (rowCCT != null)
            {
                string comando = "delete from contacorrente where CCT = @P1";
                try
                {
                    VirMSSQL.TableAdapter.STTableAdapter.ExecutarSQLNonQuery(comando,rowCCT.CCT);
                    dConta1.ContaCorrenTeTableAdapter.Fill(dConta1.ContaCorrenTe, CON);
                }
                catch
                {
                    rowCCT.CCTAtiva = false;
                    gridView1.CloseEditor();
                }
            }
        }

        /*
        private bool validaContas()
        {
            dConta.ContaCorrenTeRow rowCCTPrincipal = null;
            dConta.ContaCorrenTeRow rowCCTMasterOculta = null;
            dConta.ContaCorrenTeRow rowCCTOculta = null;
            foreach (dConta.ContaCorrenTeRow CCTrow in dConta1.ContaCorrenTe)
            {
                if (CCTrow.IsCCTAtivaNull() || !CCTrow.CCTAtiva)
                    continue;
                switch ((CCTTipo)CCTrow.CCTTipo)
                {
                    case CCTTipo.Aplicacao:
                        break;
                    case CCTTipo.ContaCorrete:
                        if (CCTrow.CCTPrincipal)
                        {
                            if (rowCCTPrincipal == null)
                                rowCCTPrincipal = CCTrow;
                            else
                            {
                                MessageBox.Show("Erro: existe mais de uma conta Pricipal");
                                return false;
                            }
                        }
                        break;
                    case CCTTipo.ContaCorreteOculta:                        
                        if (rowCCTOculta != null)
                        {
                            MessageBox.Show("Erro: existe mais de uma conta oculta");
                            return false;
                        }
                        else
                            rowCCTOculta = CCTrow;
                        break;
                    case CCTTipo.ContaCorrete_com_Oculta:
                        if (CCTrow.CCTPrincipal)
                        {
                            if (rowCCTPrincipal == null)
                                rowCCTPrincipal = CCTrow;
                            else
                            {
                                MessageBox.Show("Erro: existe mais de uma conta Pricipal");
                                return false;
                            }
                        }
                        if (rowCCTMasterOculta != null)
                        {
                            MessageBox.Show("Erro: existe mais de uma conta com conta oculta");
                            return false;
                        }
                        else
                            rowCCTMasterOculta = CCTrow;
                        break;
                    case CCTTipo.ContaPool:
                        break;
                    case CCTTipo.Poupanca:
                        break;
                    default:
                        break;
                }
                
            }
            if (rowCCTPrincipal == null)
            {
                MessageBox.Show("Erro: N�o existe uma conta Pricipal");
                return false;
            }
            if ((rowCCTOculta != null) && (rowCCTMasterOculta == null))
            {
                MessageBox.Show("Erro: Conta oculta sem conta meste");
                return false;
            }
            if ((rowCCTOculta == null) && (rowCCTMasterOculta != null))
            {
                rowCCTOculta = dConta1.ContaCorrenTe.NewContaCorrenTeRow();
                rowCCTOculta.CCT_BCO = rowCCTMasterOculta.CCT_BCO;
                rowCCTOculta.CCT_CON = CON;
                rowCCTOculta.CCTAgencia = rowCCTMasterOculta.CCTAgencia;
                rowCCTOculta.CCTAgenciaDg = rowCCTMasterOculta.CCTAgenciaDg;
                rowCCTOculta.CCTAplicacao = false;
                rowCCTOculta.CCTAtiva = true;
                if (!rowCCTMasterOculta.IsCCTCNRNull())
                    rowCCTOculta.CCTCNR = rowCCTMasterOculta.CCTCNR;
                rowCCTOculta.CCTConta = rowCCTMasterOculta.CCTConta;
                rowCCTOculta.CCTContaDg = rowCCTMasterOculta.CCTContaDg;
                rowCCTOculta.CCTPrincipal = false;
                rowCCTOculta.CCTTipo = (int)CCTTipo.ContaCorreteOculta;
                rowCCTOculta.CCTTitulo = "Conta Oculta";
                dConta1.ContaCorrenTe.AddContaCorrenTeRow(rowCCTOculta);
                dConta1.ContaCorrenTeTableAdapter.Update(rowCCTOculta);
                rowCCTOculta.AcceptChanges();
            }
            dConta.ConTasLogicasRow rowCTLCaixa = null;
            dConta.ConTasLogicasRow rowCTLFundoReserva = null;
            foreach (dConta.ConTasLogicasRow CTLrow in dConta1.ConTasLogicas)
            {
                switch ((CTLTipo)CTLrow.CTLTipo)
                {
                    case CTLTipo.Caixa:
                        if (rowCTLCaixa != null)
                        {
                            MessageBox.Show("Erro: existe mais de uma conta CAIXA");
                            return false;
                        }
                        else
                            rowCTLCaixa = CTLrow;
                        break;
                    case CTLTipo.Fundo:
                        if (rowCTLFundoReserva != null)
                        {
                            MessageBox.Show("Erro: existe mais de uma conta \"Fundo de Reserva\"");
                            return false;
                        }
                        else
                            rowCTLFundoReserva = CTLrow;
                        break;
                    case CTLTipo.Provisao:
                        break;
                    case CTLTipo.Rateio:
                        break;
                    case CTLTipo.Terceiro:
                        break;
                    default:
                        break;
                }                
            }
            if (rowCTLCaixa == null)
            {
                rowCTLCaixa = dConta1.ConTasLogicas.NewConTasLogicasRow();
                //rowCTLCaixa.CTL_CCT = rowCCTPrincipal.CCT;
                rowCTLCaixa.CTL_CON = CON;
                rowCTLCaixa.CTLAtiva = true;
                rowCTLCaixa.CTLDATAI = DateTime.Now;
                rowCTLCaixa.CTLTipo = (int)CTLTipo.Caixa;
                rowCTLCaixa.CTLTitulo = "CAIXA";
                dConta1.ConTasLogicas.AddConTasLogicasRow(rowCTLCaixa);
                dConta1.ConTasLogicasTableAdapter.Update(rowCTLCaixa);
                rowCTLCaixa.AcceptChanges();
                //incluir uma nova linha
            }
            else
                if (!rowCTLCaixa.CTLAtiva)
                {
                    rowCTLCaixa.CTLAtiva = true;
                    dConta1.ConTasLogicasTableAdapter.Update(rowCTLCaixa);
                    rowCTLCaixa.AcceptChanges();
                };
            if (rowCTLFundoReserva == null)
            {
                rowCTLFundoReserva = dConta1.ConTasLogicas.NewConTasLogicasRow();
                //rowCTLFundoReserva.CTL_CCT = rowCCTPrincipal.CCT;
                rowCTLFundoReserva.CTL_CON = CON;
                rowCTLFundoReserva.CTLAtiva = true;
                rowCTLFundoReserva.CTLDATAI = DateTime.Now;
                rowCTLFundoReserva.CTLTipo = (int)CTLTipo.Fundo;
                rowCTLFundoReserva.CTLTitulo = "Fundo de Reserva";
                dConta1.ConTasLogicas.AddConTasLogicasRow(rowCTLFundoReserva);
                dConta1.ConTasLogicasTableAdapter.Update(rowCTLFundoReserva);
                rowCTLFundoReserva.AcceptChanges();
                //incluir uma nova linha
            } 
            else
                if (!rowCTLFundoReserva.CTLAtiva)
                {
                    rowCTLFundoReserva.CTLAtiva = true;
                    dConta1.ConTasLogicasTableAdapter.Update(rowCTLFundoReserva);
                    rowCTLFundoReserva.AcceptChanges();
                }
            if (!dConta1.VerificaRentabilidade())
            {
                MessageBox.Show(dConta1.ErroRentabilidade);
                return false;
            }
            return true;
        }
        */

        private void simpleButton1_Click(object sender, EventArgs e)
        {
            if (dConta1.validaContas())
            {
                rowPrincipal.CONDivideSaldos = true;
                panelControl1.Visible = false;
            }
            else
                MessageBox.Show(dConta1.ErroRentabilidade);
        }

        private void repositoryItemButtonEdit2_ButtonClick(object sender, DevExpress.XtraEditors.Controls.ButtonPressedEventArgs e)
        {
            DevExpress.XtraGrid.Views.Grid.GridView GV = (DevExpress.XtraGrid.Views.Grid.GridView)gridControl2.FocusedView;
            if (GV == gridView2)
            {
                dConta.ConTasLogicasRow rowCTL = (dConta.ConTasLogicasRow)gridView2.GetFocusedDataRow();
                if (rowCTL != null)
                {
                    string comando = "delete from contasLogicas where CTL = @P1";
                    using (CompontesBasicos.Espera.cEspera Esp = new CompontesBasicos.Espera.cEspera(this))
                    {
                        Esp.Espere("Removendo / desativando conta ...");
                        try
                        {
                            VirMSSQL.TableAdapter.STTableAdapter.ExecutarSQLNonQuery(comando, rowCTL.CTL);
                            dConta1.CTLxCCT.Clear();
                            dConta1.ConTasLogicasTableAdapter.FillByCON(dConta1.ConTasLogicas, CON);
                            dConta1.CTLxCCTTableAdapter.FillBy(dConta1.CTLxCCT, CON);
                        }
                        catch
                        {
                            rowCTL.CTLAtiva = false;
                            dConta1.ConTasLogicasTableAdapter.Update(rowCTL);
                            rowCTL.AcceptChanges();
                            gridView2.CloseEditor();
                        }
                    }
                }
            }
            else
            {
                dConta.CTLxCCTRow rowCTL = (dConta.CTLxCCTRow)GV.GetFocusedDataRow();
                rowCTL.Delete();
                dConta1.CTLxCCTTableAdapter.Update(rowCTL);
                dConta1.CTLxCCT.AcceptChanges();
            }
        }

        private void gridView3_InitNewRow(object sender, DevExpress.XtraGrid.Views.Grid.InitNewRowEventArgs e)
        {
            DevExpress.XtraGrid.Views.Grid.GridView GV = (DevExpress.XtraGrid.Views.Grid.GridView)gridControl2.FocusedView;
            DataRowView DRV = (DataRowView)GV.GetRow(e.RowHandle);
            if (DRV == null)
                return;
            dConta.CTLxCCTRow novarow = (dConta.CTLxCCTRow)DRV.Row;
            if (novarow == null)
                return;
            //novarow.CCT = rowCCTPrincipal.c;
            //novarow.CTL = 0;
            novarow.CTLCCTRentabilidade = false;
            novarow.CTLCCTTranArrecadado = (int)CTLCCTTranArrecadado.Nao;
        }

        private void gridView2_RowUpdated(object sender, DevExpress.XtraGrid.Views.Base.RowObjectEventArgs e)
        {
            if (e.Row == null)
                return;
            DataRowView DRV = (DataRowView)e.Row;
            dConta.ConTasLogicasRow novarow = (dConta.ConTasLogicasRow)DRV.Row;
            dConta1.ConTasLogicasTableAdapter.Update(novarow);
            novarow.AcceptChanges();
        }

        private void gridView3_RowUpdated(object sender, DevExpress.XtraGrid.Views.Base.RowObjectEventArgs e)
        {
            if (e.Row == null)
                return;
            DataRowView DRV = (DataRowView)e.Row;
            dConta.CTLxCCTRow novarow = (dConta.CTLxCCTRow)DRV.Row;
            dConta1.CTLxCCTTableAdapter.Update(novarow);
            novarow.AcceptChanges();
        }        

        private void LookUpEditPlus_ButtonClick(object sender, DevExpress.XtraEditors.Controls.ButtonPressedEventArgs e)
        {
            if (TabMae.somenteleitura)
                return;
            if (e.Button.Kind == DevExpress.XtraEditors.Controls.ButtonPredefines.Delete)
            {                
                dConta.ContaCorrenTeRow rowCCT = (dConta.ContaCorrenTeRow)gridView1.GetFocusedDataRow();
                if (rowCCT == null)
                    return;
                rowCCT.SetCCT_CCTPlusNull();
                DevExpress.XtraEditors.LookUpEdit Editor = (DevExpress.XtraEditors.LookUpEdit)sender;
                //Editor.Refresh();
                Editor.EditValue = null;
            }
        }

        private void gridView1_CustomRowCellEdit(object sender, DevExpress.XtraGrid.Views.Grid.CustomRowCellEditEventArgs e)
        {            
            if (e.Column == colCCT_CCTPlus)
            {
                dConta.ContaCorrenTeRow rowCCT = (dConta.ContaCorrenTeRow)gridView1.GetDataRow(e.RowHandle);
                if (rowCCT != null)
                    if ((rowCCT.CCT_BCO == 237) && ((CCTTipo)rowCCT.CCTTipo == CCTTipo.ContaCorrete_com_Oculta))
                    {
                        e.RepositoryItem = LookUpEditPlus;
                        if(TabMae.somenteleitura)
                            LookUpEditPlus.ReadOnly = true;
                        return;
                    }
                e.RepositoryItem = null;
            }            
        }

        

        
    }
}

