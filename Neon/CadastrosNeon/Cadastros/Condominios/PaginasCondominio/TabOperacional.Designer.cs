namespace Cadastros.Condominios.PaginasCondominio
{
    partial class TabOperacional
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.Windows.Forms.Label cONAuditor1_USULabel;
            System.Windows.Forms.Label cONAuditor2_USULabel;
            System.Windows.Forms.Label cONSeguradora_FRNLabel;
            System.Windows.Forms.Label cONCorretoraSeguros_FRNLabel;
            System.Windows.Forms.Label cONDataUltimaAssembleiaLabel;
            System.Windows.Forms.Label cONDataUltimaAssembleiaLabel1;
            System.Windows.Forms.Label cONConvocacaoAssembleiaLabel;
            System.Windows.Forms.Label cONCompetenciaVencidaLabel;
            System.Windows.Forms.Label cONLeituraGasLabel;
            System.Windows.Forms.Label label1;
            System.Windows.Forms.Label label2;
            System.Windows.Forms.Label label4;
            System.Windows.Forms.Label label5;
            System.Windows.Forms.Label label6;
            System.Windows.Forms.Label label8;
            System.Windows.Forms.Label label9;
            System.Windows.Forms.Label label3;
            System.Windows.Forms.Label label7;
            System.Windows.Forms.Label label10;
            System.Windows.Forms.Label label11;
            System.Windows.Forms.Label label12;
            DevExpress.XtraEditors.Controls.EditorButtonImageOptions editorButtonImageOptions1 = new DevExpress.XtraEditors.Controls.EditorButtonImageOptions();
            DevExpress.XtraEditors.Controls.EditorButtonImageOptions editorButtonImageOptions2 = new DevExpress.XtraEditors.Controls.EditorButtonImageOptions();
            DevExpress.XtraEditors.Controls.EditorButtonImageOptions editorButtonImageOptions3 = new DevExpress.XtraEditors.Controls.EditorButtonImageOptions();
            DevExpress.XtraEditors.Controls.EditorButtonImageOptions editorButtonImageOptions4 = new DevExpress.XtraEditors.Controls.EditorButtonImageOptions();
            DevExpress.XtraEditors.Controls.EditorButtonImageOptions editorButtonImageOptions5 = new DevExpress.XtraEditors.Controls.EditorButtonImageOptions();
            DevExpress.XtraEditors.Controls.EditorButtonImageOptions editorButtonImageOptions6 = new DevExpress.XtraEditors.Controls.EditorButtonImageOptions();
            DevExpress.XtraEditors.Controls.EditorButtonImageOptions editorButtonImageOptions7 = new DevExpress.XtraEditors.Controls.EditorButtonImageOptions();
            DevExpress.XtraEditors.Controls.EditorButtonImageOptions editorButtonImageOptions8 = new DevExpress.XtraEditors.Controls.EditorButtonImageOptions();
            DevExpress.XtraEditors.Controls.EditorButtonImageOptions editorButtonImageOptions9 = new DevExpress.XtraEditors.Controls.EditorButtonImageOptions();
            DevExpress.XtraEditors.Controls.EditorButtonImageOptions editorButtonImageOptions10 = new DevExpress.XtraEditors.Controls.EditorButtonImageOptions();
            DevExpress.XtraEditors.Controls.EditorButtonImageOptions editorButtonImageOptions11 = new DevExpress.XtraEditors.Controls.EditorButtonImageOptions();
            DevExpress.XtraEditors.Controls.EditorButtonImageOptions editorButtonImageOptions12 = new DevExpress.XtraEditors.Controls.EditorButtonImageOptions();
            DevExpress.Utils.SuperToolTip superToolTip1 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem1 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem1 = new DevExpress.Utils.ToolTipItem();
            this.groupControl1 = new DevExpress.XtraEditors.GroupControl();
            this.SBProcuracao = new DevExpress.XtraEditors.SimpleButton();
            this.lookUpEditADV = new DevExpress.XtraEditors.LookUpEdit();
            this.cONDOMINIOSBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.bindingSourceFornecedores = new System.Windows.Forms.BindingSource(this.components);
            this.aUDITORESBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.groupControl2 = new DevExpress.XtraEditors.GroupControl();
            this.lookUpEdit5 = new DevExpress.XtraEditors.LookUpEdit();
            this.conTasLogicasBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.textEditAviso = new DevExpress.XtraEditors.TextEdit();
            this.imageComboBoxEdit1 = new DevExpress.XtraEditors.ImageComboBoxEdit();
            this.groupControl3 = new DevExpress.XtraEditors.GroupControl();
            this.lookUpEdit4 = new DevExpress.XtraEditors.LookUpEdit();
            this.dPlanosSeguroConteudoBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.cONCorretoraSeguros_FRNLookUpEdit = new DevExpress.XtraEditors.LookUpEdit();
            this.cONSeguradora_FRNLookUpEdit = new DevExpress.XtraEditors.LookUpEdit();
            this.groupControl5 = new DevExpress.XtraEditors.GroupControl();
            this.cONConvocacaoAssembleiaSpinEdit = new DevExpress.XtraEditors.SpinEdit();
            this.cONDataUltimaAssembleiaDateEdit1 = new DevExpress.XtraEditors.DateEdit();
            this.cONDataUltimaAssembleiaDateEdit = new DevExpress.XtraEditors.DateEdit();
            this.cONCompetenciaVencidaCheckEdit = new DevExpress.XtraEditors.CheckEdit();
            this.cONLeituraGasCheckEdit = new DevExpress.XtraEditors.CheckEdit();
            this.groupControl6 = new DevExpress.XtraEditors.GroupControl();
            this.memoEdit1 = new DevExpress.XtraEditors.MemoEdit();
            this.groupControl7 = new DevExpress.XtraEditors.GroupControl();
            this.lookUpEdit3 = new DevExpress.XtraEditors.LookUpEdit();
            this.lookUpEdit2 = new DevExpress.XtraEditors.LookUpEdit();
            this.lookUpG2 = new DevExpress.XtraEditors.LookUpEdit();
            this.lookUpG = new DevExpress.XtraEditors.LookUpEdit();
            this.panelControl1 = new DevExpress.XtraEditors.PanelControl();
            this.imageComboBoxEdit2 = new DevExpress.XtraEditors.ImageComboBoxEdit();
            this.checkEdit1 = new DevExpress.XtraEditors.CheckEdit();
            this.spinEdit1 = new DevExpress.XtraEditors.SpinEdit();
            this.groupControl8 = new DevExpress.XtraEditors.GroupControl();
            this.textEdit2 = new DevExpress.XtraEditors.TextEdit();
            this.textEdit1 = new DevExpress.XtraEditors.TextEdit();
            this.tiposInadimplenciaBindingSource = new System.Windows.Forms.BindingSource(this.components);
            cONAuditor1_USULabel = new System.Windows.Forms.Label();
            cONAuditor2_USULabel = new System.Windows.Forms.Label();
            cONSeguradora_FRNLabel = new System.Windows.Forms.Label();
            cONCorretoraSeguros_FRNLabel = new System.Windows.Forms.Label();
            cONDataUltimaAssembleiaLabel = new System.Windows.Forms.Label();
            cONDataUltimaAssembleiaLabel1 = new System.Windows.Forms.Label();
            cONConvocacaoAssembleiaLabel = new System.Windows.Forms.Label();
            cONCompetenciaVencidaLabel = new System.Windows.Forms.Label();
            cONLeituraGasLabel = new System.Windows.Forms.Label();
            label1 = new System.Windows.Forms.Label();
            label2 = new System.Windows.Forms.Label();
            label4 = new System.Windows.Forms.Label();
            label5 = new System.Windows.Forms.Label();
            label6 = new System.Windows.Forms.Label();
            label8 = new System.Windows.Forms.Label();
            label9 = new System.Windows.Forms.Label();
            label3 = new System.Windows.Forms.Label();
            label7 = new System.Windows.Forms.Label();
            label10 = new System.Windows.Forms.Label();
            label11 = new System.Windows.Forms.Label();
            label12 = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.StyleController_F)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl1)).BeginInit();
            this.groupControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.lookUpEditADV.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cONDOMINIOSBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.bindingSourceFornecedores)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.aUDITORESBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl2)).BeginInit();
            this.groupControl2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.lookUpEdit5.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.conTasLogicasBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEditAviso.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.imageComboBoxEdit1.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl3)).BeginInit();
            this.groupControl3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.lookUpEdit4.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dPlanosSeguroConteudoBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cONCorretoraSeguros_FRNLookUpEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cONSeguradora_FRNLookUpEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl5)).BeginInit();
            this.groupControl5.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.cONConvocacaoAssembleiaSpinEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cONDataUltimaAssembleiaDateEdit1.Properties.CalendarTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cONDataUltimaAssembleiaDateEdit1.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cONDataUltimaAssembleiaDateEdit.Properties.CalendarTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cONDataUltimaAssembleiaDateEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cONCompetenciaVencidaCheckEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cONLeituraGasCheckEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl6)).BeginInit();
            this.groupControl6.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.memoEdit1.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl7)).BeginInit();
            this.groupControl7.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.lookUpEdit3.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lookUpEdit2.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lookUpG2.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lookUpG.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl1)).BeginInit();
            this.panelControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.imageComboBoxEdit2.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit1.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.spinEdit1.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl8)).BeginInit();
            this.groupControl8.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit2.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit1.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tiposInadimplenciaBindingSource)).BeginInit();
            this.SuspendLayout();
            // 
            // ToolTipController_F
            // 
            this.ToolTipController_F.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.ToolTipController_F.Appearance.ForeColor = System.Drawing.Color.Black;
            this.ToolTipController_F.Appearance.Options.UseBackColor = true;
            this.ToolTipController_F.Appearance.Options.UseFont = true;
            this.ToolTipController_F.Appearance.Options.UseForeColor = true;
            this.ToolTipController_F.Appearance.Options.UseTextOptions = true;
            this.ToolTipController_F.Appearance.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            // 
            // StyleController_F
            // 
            this.StyleController_F.AppearanceDisabled.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this.StyleController_F.AppearanceDisabled.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this.StyleController_F.AppearanceDisabled.Options.UseBackColor = true;
            this.StyleController_F.AppearanceDisabled.Options.UseForeColor = true;
            this.StyleController_F.AppearanceFocused.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.StyleController_F.AppearanceFocused.Options.UseBackColor = true;
            // 
            // cONAuditor1_USULabel
            // 
            cONAuditor1_USULabel.AutoSize = true;
            cONAuditor1_USULabel.Location = new System.Drawing.Point(7, 28);
            cONAuditor1_USULabel.Name = "cONAuditor1_USULabel";
            cONAuditor1_USULabel.Size = new System.Drawing.Size(27, 13);
            cONAuditor1_USULabel.TabIndex = 0;
            cONAuditor1_USULabel.Text = "Tipo";
            // 
            // cONAuditor2_USULabel
            // 
            cONAuditor2_USULabel.AutoSize = true;
            cONAuditor2_USULabel.Location = new System.Drawing.Point(284, 28);
            cONAuditor2_USULabel.Name = "cONAuditor2_USULabel";
            cONAuditor2_USULabel.Size = new System.Drawing.Size(33, 13);
            cONAuditor2_USULabel.TabIndex = 2;
            cONAuditor2_USULabel.Text = "Aviso";
            // 
            // cONSeguradora_FRNLabel
            // 
            cONSeguradora_FRNLabel.AutoSize = true;
            cONSeguradora_FRNLabel.Location = new System.Drawing.Point(7, 31);
            cONSeguradora_FRNLabel.Name = "cONSeguradora_FRNLabel";
            cONSeguradora_FRNLabel.Size = new System.Drawing.Size(29, 13);
            cONSeguradora_FRNLabel.TabIndex = 0;
            cONSeguradora_FRNLabel.Text = "Seg.";
            // 
            // cONCorretoraSeguros_FRNLabel
            // 
            cONCorretoraSeguros_FRNLabel.AutoSize = true;
            cONCorretoraSeguros_FRNLabel.Location = new System.Drawing.Point(284, 31);
            cONCorretoraSeguros_FRNLabel.Name = "cONCorretoraSeguros_FRNLabel";
            cONCorretoraSeguros_FRNLabel.Size = new System.Drawing.Size(54, 13);
            cONCorretoraSeguros_FRNLabel.TabIndex = 2;
            cONCorretoraSeguros_FRNLabel.Text = "Corretora";
            // 
            // cONDataUltimaAssembleiaLabel
            // 
            cONDataUltimaAssembleiaLabel.AutoSize = true;
            cONDataUltimaAssembleiaLabel.Location = new System.Drawing.Point(9, 29);
            cONDataUltimaAssembleiaLabel.Name = "cONDataUltimaAssembleiaLabel";
            cONDataUltimaAssembleiaLabel.Size = new System.Drawing.Size(36, 13);
            cONDataUltimaAssembleiaLabel.TabIndex = 0;
            cONDataUltimaAssembleiaLabel.Text = "�ltima";
            // 
            // cONDataUltimaAssembleiaLabel1
            // 
            cONDataUltimaAssembleiaLabel1.AutoSize = true;
            cONDataUltimaAssembleiaLabel1.Location = new System.Drawing.Point(285, 27);
            cONDataUltimaAssembleiaLabel1.Name = "cONDataUltimaAssembleiaLabel1";
            cONDataUltimaAssembleiaLabel1.Size = new System.Drawing.Size(45, 13);
            cONDataUltimaAssembleiaLabel1.TabIndex = 2;
            cONDataUltimaAssembleiaLabel1.Text = "Pr�xima";
            // 
            // cONConvocacaoAssembleiaLabel
            // 
            cONConvocacaoAssembleiaLabel.AutoSize = true;
            cONConvocacaoAssembleiaLabel.Location = new System.Drawing.Point(548, 29);
            cONConvocacaoAssembleiaLabel.Name = "cONConvocacaoAssembleiaLabel";
            cONConvocacaoAssembleiaLabel.Size = new System.Drawing.Size(96, 13);
            cONConvocacaoAssembleiaLabel.TabIndex = 4;
            cONConvocacaoAssembleiaLabel.Text = "Prazo Convoca��o";
            // 
            // cONCompetenciaVencidaLabel
            // 
            cONCompetenciaVencidaLabel.AutoSize = true;
            cONCompetenciaVencidaLabel.Location = new System.Drawing.Point(147, 7);
            cONCompetenciaVencidaLabel.Name = "cONCompetenciaVencidaLabel";
            cONCompetenciaVencidaLabel.Size = new System.Drawing.Size(114, 13);
            cONCompetenciaVencidaLabel.TabIndex = 7;
            cONCompetenciaVencidaLabel.Text = "Competencia a Vencer";
            // 
            // cONLeituraGasLabel
            // 
            cONLeituraGasLabel.AutoSize = true;
            cONLeituraGasLabel.Location = new System.Drawing.Point(314, 7);
            cONLeituraGasLabel.Name = "cONLeituraGasLabel";
            cONLeituraGasLabel.Size = new System.Drawing.Size(76, 13);
            cONLeituraGasLabel.TabIndex = 13;
            cONLeituraGasLabel.Text = "Leitura do G�s";
            // 
            // label1
            // 
            label1.AutoSize = true;
            label1.Location = new System.Drawing.Point(7, 29);
            label1.Name = "label1";
            label1.Size = new System.Drawing.Size(51, 13);
            label1.TabIndex = 4;
            label1.Text = "Escrit�rio";
            // 
            // label2
            // 
            label2.AutoSize = true;
            label2.Location = new System.Drawing.Point(7, 29);
            label2.Name = "label2";
            label2.Size = new System.Drawing.Size(46, 13);
            label2.TabIndex = 1;
            label2.Text = "Principal";
            // 
            // label4
            // 
            label4.AutoSize = true;
            label4.Location = new System.Drawing.Point(518, 7);
            label4.Name = "label4";
            label4.Size = new System.Drawing.Size(39, 13);
            label4.TabIndex = 15;
            label4.Text = "Malote";
            // 
            // label5
            // 
            label5.AutoSize = true;
            label5.Location = new System.Drawing.Point(284, 27);
            label5.Name = "label5";
            label5.Size = new System.Drawing.Size(53, 13);
            label5.TabIndex = 5;
            label5.Text = "Cobran�a";
            // 
            // label6
            // 
            label6.AutoSize = true;
            label6.Location = new System.Drawing.Point(7, 10);
            label6.Name = "label6";
            label6.Size = new System.Drawing.Size(48, 13);
            label6.TabIndex = 17;
            label6.Text = "Protesto";
            // 
            // label8
            // 
            label8.AutoSize = true;
            label8.Location = new System.Drawing.Point(284, 28);
            label8.Name = "label8";
            label8.Size = new System.Drawing.Size(37, 13);
            label8.TabIndex = 2;
            label8.Text = "Senha";
            // 
            // label9
            // 
            label9.AutoSize = true;
            label9.Location = new System.Drawing.Point(9, 29);
            label9.Name = "label9";
            label9.Size = new System.Drawing.Size(43, 13);
            label9.TabIndex = 0;
            label9.Text = "Usu�rio";
            // 
            // label3
            // 
            label3.AutoSize = true;
            label3.Location = new System.Drawing.Point(7, 55);
            label3.Name = "label3";
            label3.Size = new System.Drawing.Size(57, 13);
            label3.TabIndex = 7;
            label3.Text = "Assistente";
            // 
            // label7
            // 
            label7.AutoSize = true;
            label7.Location = new System.Drawing.Point(284, 53);
            label7.Name = "label7";
            label7.Size = new System.Drawing.Size(54, 13);
            label7.TabIndex = 8;
            label7.Text = "Balancete";
            // 
            // label10
            // 
            label10.AutoSize = true;
            label10.Location = new System.Drawing.Point(548, 31);
            label10.Name = "label10";
            label10.Size = new System.Drawing.Size(54, 13);
            label10.TabIndex = 4;
            label10.Text = "Conte�do";
            // 
            // label11
            // 
            label11.AutoSize = true;
            label11.Location = new System.Drawing.Point(7, 56);
            label11.Name = "label11";
            label11.Size = new System.Drawing.Size(36, 13);
            label11.TabIndex = 6;
            label11.Text = "Conta";
            // 
            // label12
            // 
            label12.AutoSize = true;
            label12.Location = new System.Drawing.Point(281, 29);
            label12.Name = "label12";
            label12.Size = new System.Drawing.Size(65, 13);
            label12.TabIndex = 6;
            label12.Text = "Procura��o:";
            // 
            // groupControl1
            // 
            this.groupControl1.Controls.Add(this.SBProcuracao);
            this.groupControl1.Controls.Add(label12);
            this.groupControl1.Controls.Add(this.lookUpEditADV);
            this.groupControl1.Controls.Add(label1);
            this.groupControl1.Dock = System.Windows.Forms.DockStyle.Top;
            this.groupControl1.Location = new System.Drawing.Point(0, 78);
            this.groupControl1.Name = "groupControl1";
            this.groupControl1.Size = new System.Drawing.Size(1294, 53);
            this.groupControl1.TabIndex = 0;
            this.groupControl1.Text = ":: Jur�dico ::";
            // 
            // SBProcuracao
            // 
            this.SBProcuracao.Location = new System.Drawing.Point(344, 24);
            this.SBProcuracao.Name = "SBProcuracao";
            this.SBProcuracao.Size = new System.Drawing.Size(184, 23);
            this.SBProcuracao.TabIndex = 7;
            this.SBProcuracao.Text = "Editar";
            this.SBProcuracao.Click += new System.EventHandler(this.SBProcuracao_Click);
            // 
            // lookUpEditADV
            // 
            this.lookUpEditADV.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.cONDOMINIOSBindingSource, "CONEscrit_FRN", true));
            this.lookUpEditADV.Location = new System.Drawing.Point(76, 26);
            this.lookUpEditADV.Name = "lookUpEditADV";
            this.lookUpEditADV.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo),
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Plus),
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Minus),
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.lookUpEditADV.Properties.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("FRNNome", "Nome", 53, DevExpress.Utils.FormatType.None, "", true, DevExpress.Utils.HorzAlignment.Near)});
            this.lookUpEditADV.Properties.DataSource = this.bindingSourceFornecedores;
            this.lookUpEditADV.Properties.DisplayMember = "FRNNome";
            this.lookUpEditADV.Properties.PopupFilterMode = DevExpress.XtraEditors.PopupFilterMode.Contains;
            this.lookUpEditADV.Properties.ShowHeader = false;
            this.lookUpEditADV.Properties.ValueMember = "FRN";
            this.lookUpEditADV.Properties.PopupFilter += new DevExpress.XtraEditors.Controls.PopupFilterEventHandler(this.lookUpEdit1_Properties_PopupFilter);
            this.lookUpEditADV.Properties.ButtonClick += new DevExpress.XtraEditors.Controls.ButtonPressedEventHandler(this.FRNLookUpEdit_ButtonClick);
            this.lookUpEditADV.Size = new System.Drawing.Size(185, 20);
            this.lookUpEditADV.TabIndex = 5;
            // 
            // cONDOMINIOSBindingSource
            // 
            this.cONDOMINIOSBindingSource.DataMember = "CONDOMINIOS";
            this.cONDOMINIOSBindingSource.DataSource = typeof(CadastrosProc.Condominios.dCondominiosCampos);
            // 
            // bindingSourceFornecedores
            // 
            this.bindingSourceFornecedores.DataMember = "FRNLookup";
            this.bindingSourceFornecedores.DataSource = typeof(CadastrosProc.Fornecedores.dFornecedoresLookup);
            // 
            // aUDITORESBindingSource
            // 
            this.aUDITORESBindingSource.DataMember = "USUarios";
            this.aUDITORESBindingSource.DataSource = typeof(FrameworkProc.datasets.dUSUarios);
            // 
            // groupControl2
            // 
            this.groupControl2.Controls.Add(this.lookUpEdit5);
            this.groupControl2.Controls.Add(label11);
            this.groupControl2.Controls.Add(this.textEditAviso);
            this.groupControl2.Controls.Add(this.imageComboBoxEdit1);
            this.groupControl2.Controls.Add(cONAuditor2_USULabel);
            this.groupControl2.Controls.Add(cONAuditor1_USULabel);
            this.groupControl2.Dock = System.Windows.Forms.DockStyle.Top;
            this.groupControl2.Location = new System.Drawing.Point(0, 131);
            this.groupControl2.Name = "groupControl2";
            this.groupControl2.Size = new System.Drawing.Size(1294, 80);
            this.groupControl2.TabIndex = 1;
            this.groupControl2.Text = ":: Acordos ::";
            // 
            // lookUpEdit5
            // 
            this.lookUpEdit5.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.cONDOMINIOSBindingSource, "CONAcordo_CTL", true));
            this.lookUpEdit5.Location = new System.Drawing.Point(76, 53);
            this.lookUpEdit5.Name = "lookUpEdit5";
            editorButtonImageOptions1.EnableTransparency = false;
            editorButtonImageOptions2.EnableTransparency = false;
            this.lookUpEdit5.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo, "", -1, true, true, false, editorButtonImageOptions1, new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), "Selecionar"),
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Delete, "", -1, true, true, false, editorButtonImageOptions2, new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), "Nenhuma")});
            this.lookUpEdit5.Properties.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("CTLTitulo", "CTL Titulo", 57, DevExpress.Utils.FormatType.None, "", true, DevExpress.Utils.HorzAlignment.Near)});
            this.lookUpEdit5.Properties.DataSource = this.conTasLogicasBindingSource;
            this.lookUpEdit5.Properties.DisplayMember = "CTLTitulo";
            this.lookUpEdit5.Properties.NullText = "-- Conta Original --";
            this.lookUpEdit5.Properties.ValueMember = "CTL";
            this.lookUpEdit5.Properties.ButtonClick += new DevExpress.XtraEditors.Controls.ButtonPressedEventHandler(this.lookUpEdit5_Properties_ButtonClick);
            this.lookUpEdit5.Size = new System.Drawing.Size(185, 20);
            this.lookUpEdit5.TabIndex = 7;
            // 
            // conTasLogicasBindingSource
            // 
            this.conTasLogicasBindingSource.DataMember = "ConTasLogicas";
            this.conTasLogicasBindingSource.DataSource = typeof(CadastrosProc.Condominios.dCondominiosCampos);
            // 
            // textEditAviso
            // 
            this.textEditAviso.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.cONDOMINIOSBindingSource, "CONAvisoAcordo", true));
            this.textEditAviso.Location = new System.Drawing.Point(343, 25);
            this.textEditAviso.Name = "textEditAviso";
            this.textEditAviso.Size = new System.Drawing.Size(451, 20);
            this.textEditAviso.TabIndex = 5;
            // 
            // imageComboBoxEdit1
            // 
            this.imageComboBoxEdit1.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.cONDOMINIOSBindingSource, "CONStatusAcordo", true));
            this.imageComboBoxEdit1.Location = new System.Drawing.Point(76, 25);
            this.imageComboBoxEdit1.Name = "imageComboBoxEdit1";
            this.imageComboBoxEdit1.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.imageComboBoxEdit1.Properties.Items.AddRange(new DevExpress.XtraEditors.Controls.ImageComboBoxItem[] {
            new DevExpress.XtraEditors.Controls.ImageComboBoxItem("Liberado", 0, -1),
            new DevExpress.XtraEditors.Controls.ImageComboBoxItem("Com Aviso", 1, -1),
            new DevExpress.XtraEditors.Controls.ImageComboBoxItem("Proibido", 2, -1)});
            this.imageComboBoxEdit1.Size = new System.Drawing.Size(185, 20);
            this.imageComboBoxEdit1.TabIndex = 4;
            // 
            // groupControl3
            // 
            this.groupControl3.Controls.Add(this.lookUpEdit4);
            this.groupControl3.Controls.Add(label10);
            this.groupControl3.Controls.Add(cONCorretoraSeguros_FRNLabel);
            this.groupControl3.Controls.Add(this.cONCorretoraSeguros_FRNLookUpEdit);
            this.groupControl3.Controls.Add(cONSeguradora_FRNLabel);
            this.groupControl3.Controls.Add(this.cONSeguradora_FRNLookUpEdit);
            this.groupControl3.Dock = System.Windows.Forms.DockStyle.Top;
            this.groupControl3.Location = new System.Drawing.Point(0, 211);
            this.groupControl3.Name = "groupControl3";
            this.groupControl3.Size = new System.Drawing.Size(1294, 55);
            this.groupControl3.TabIndex = 2;
            this.groupControl3.Text = ":: Seguro ::";
            // 
            // lookUpEdit4
            // 
            this.lookUpEdit4.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.cONDOMINIOSBindingSource, "CON_PSC", true));
            this.lookUpEdit4.Location = new System.Drawing.Point(609, 28);
            this.lookUpEdit4.Name = "lookUpEdit4";
            editorButtonImageOptions3.EnableTransparency = false;
            editorButtonImageOptions4.EnableTransparency = false;
            this.lookUpEdit4.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo, "", -1, true, true, false, editorButtonImageOptions3, new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), "Selecionar"),
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Delete, "", -1, true, true, false, editorButtonImageOptions4, new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), "Nenhuma")});
            this.lookUpEdit4.Properties.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("PSCDescricao", "Descri��o", 78, DevExpress.Utils.FormatType.None, "", true, DevExpress.Utils.HorzAlignment.Near),
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("PSCValor", "Valor", 56, DevExpress.Utils.FormatType.Numeric, "", true, DevExpress.Utils.HorzAlignment.Far)});
            this.lookUpEdit4.Properties.DataSource = this.dPlanosSeguroConteudoBindingSource;
            this.lookUpEdit4.Properties.DisplayMember = "PSCDescricao";
            this.lookUpEdit4.Properties.ValueMember = "PSC";
            this.lookUpEdit4.Properties.ButtonClick += new DevExpress.XtraEditors.Controls.ButtonPressedEventHandler(this.FRNLookUpEdit_ButtonClick);
            this.lookUpEdit4.Size = new System.Drawing.Size(185, 20);
            this.lookUpEdit4.TabIndex = 5;
            this.lookUpEdit4.ButtonClick += new DevExpress.XtraEditors.Controls.ButtonPressedEventHandler(this.lookUpEdit4_ButtonClick);
            // 
            // dPlanosSeguroConteudoBindingSource
            // 
            this.dPlanosSeguroConteudoBindingSource.DataMember = "PlanosSeguroConteudo";
            this.dPlanosSeguroConteudoBindingSource.DataSource = typeof(Framework.datasets.dPlanosSeguroConteudo);
            // 
            // cONCorretoraSeguros_FRNLookUpEdit
            // 
            this.cONCorretoraSeguros_FRNLookUpEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.cONDOMINIOSBindingSource, "CONCorretoraSeguros_FRN", true));
            this.cONCorretoraSeguros_FRNLookUpEdit.Location = new System.Drawing.Point(344, 28);
            this.cONCorretoraSeguros_FRNLookUpEdit.Name = "cONCorretoraSeguros_FRNLookUpEdit";
            editorButtonImageOptions5.EnableTransparency = false;
            editorButtonImageOptions6.EnableTransparency = false;
            editorButtonImageOptions7.EnableTransparency = false;
            editorButtonImageOptions8.EnableTransparency = false;
            this.cONCorretoraSeguros_FRNLookUpEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo, "", -1, true, true, false, editorButtonImageOptions5, new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), "Selecionar"),
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Plus, "", -1, true, true, false, editorButtonImageOptions6, new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), "Nova"),
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Minus, "", -1, true, true, false, editorButtonImageOptions7, new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), "Nenhuma"),
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Ellipsis, "", -1, true, true, false, editorButtonImageOptions8, new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), "Editar")});
            this.cONCorretoraSeguros_FRNLookUpEdit.Properties.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("FRNNome", "Nome", 53, DevExpress.Utils.FormatType.None, "", true, DevExpress.Utils.HorzAlignment.Near),
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("FRNCnpj", "CNPJ", 48, DevExpress.Utils.FormatType.None, "", true, DevExpress.Utils.HorzAlignment.Near)});
            this.cONCorretoraSeguros_FRNLookUpEdit.Properties.DataSource = this.bindingSourceFornecedores;
            this.cONCorretoraSeguros_FRNLookUpEdit.Properties.DisplayMember = "FRNNome";
            this.cONCorretoraSeguros_FRNLookUpEdit.Properties.PopupFilterMode = DevExpress.XtraEditors.PopupFilterMode.Contains;
            this.cONCorretoraSeguros_FRNLookUpEdit.Properties.ValueMember = "FRN";
            this.cONCorretoraSeguros_FRNLookUpEdit.Properties.PopupFilter += new DevExpress.XtraEditors.Controls.PopupFilterEventHandler(this.cONCorretoraSeguros_FRNLookUpEdit_Properties_PopupFilter);
            this.cONCorretoraSeguros_FRNLookUpEdit.Properties.ButtonClick += new DevExpress.XtraEditors.Controls.ButtonPressedEventHandler(this.FRNLookUpEdit_ButtonClick);
            this.cONCorretoraSeguros_FRNLookUpEdit.Size = new System.Drawing.Size(185, 20);
            this.cONCorretoraSeguros_FRNLookUpEdit.TabIndex = 3;
            // 
            // cONSeguradora_FRNLookUpEdit
            // 
            this.cONSeguradora_FRNLookUpEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.cONDOMINIOSBindingSource, "CONSeguradora_FRN", true));
            this.cONSeguradora_FRNLookUpEdit.Location = new System.Drawing.Point(76, 28);
            this.cONSeguradora_FRNLookUpEdit.Name = "cONSeguradora_FRNLookUpEdit";
            editorButtonImageOptions9.EnableTransparency = false;
            editorButtonImageOptions10.EnableTransparency = false;
            editorButtonImageOptions11.EnableTransparency = false;
            editorButtonImageOptions12.EnableTransparency = false;
            this.cONSeguradora_FRNLookUpEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo, "", -1, true, true, false, editorButtonImageOptions9, new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), "Selecionar"),
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Plus, "", -1, true, true, false, editorButtonImageOptions10, new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), "Nova"),
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Minus, "", -1, true, true, false, editorButtonImageOptions11, new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), "Nenhuma"),
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Ellipsis, "", -1, true, true, false, editorButtonImageOptions12, new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), "Editar")});
            this.cONSeguradora_FRNLookUpEdit.Properties.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("FRNNome", "Nome", 53, DevExpress.Utils.FormatType.None, "", true, DevExpress.Utils.HorzAlignment.Near),
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("FRNCnpj", "CNPJ", 48, DevExpress.Utils.FormatType.None, "", true, DevExpress.Utils.HorzAlignment.Near)});
            this.cONSeguradora_FRNLookUpEdit.Properties.DataSource = this.bindingSourceFornecedores;
            this.cONSeguradora_FRNLookUpEdit.Properties.DisplayMember = "FRNNome";
            this.cONSeguradora_FRNLookUpEdit.Properties.PopupFilterMode = DevExpress.XtraEditors.PopupFilterMode.Contains;
            this.cONSeguradora_FRNLookUpEdit.Properties.ValueMember = "FRN";
            this.cONSeguradora_FRNLookUpEdit.Properties.PopupFilter += new DevExpress.XtraEditors.Controls.PopupFilterEventHandler(this.cONSeguradora_FRNLookUpEdit_Properties_PopupFilter);
            this.cONSeguradora_FRNLookUpEdit.Properties.ButtonClick += new DevExpress.XtraEditors.Controls.ButtonPressedEventHandler(this.FRNLookUpEdit_ButtonClick);
            this.cONSeguradora_FRNLookUpEdit.Size = new System.Drawing.Size(185, 20);
            this.cONSeguradora_FRNLookUpEdit.TabIndex = 1;
            // 
            // groupControl5
            // 
            this.groupControl5.Controls.Add(cONConvocacaoAssembleiaLabel);
            this.groupControl5.Controls.Add(this.cONConvocacaoAssembleiaSpinEdit);
            this.groupControl5.Controls.Add(cONDataUltimaAssembleiaLabel1);
            this.groupControl5.Controls.Add(this.cONDataUltimaAssembleiaDateEdit1);
            this.groupControl5.Controls.Add(cONDataUltimaAssembleiaLabel);
            this.groupControl5.Controls.Add(this.cONDataUltimaAssembleiaDateEdit);
            this.groupControl5.Dock = System.Windows.Forms.DockStyle.Top;
            this.groupControl5.Location = new System.Drawing.Point(0, 266);
            this.groupControl5.Name = "groupControl5";
            this.groupControl5.Size = new System.Drawing.Size(1294, 52);
            this.groupControl5.TabIndex = 4;
            this.groupControl5.Text = ":: Assembl�ias ::";
            // 
            // cONConvocacaoAssembleiaSpinEdit
            // 
            this.cONConvocacaoAssembleiaSpinEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.cONDOMINIOSBindingSource, "CONConvocacaoAssembleia", true));
            this.cONConvocacaoAssembleiaSpinEdit.EditValue = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.cONConvocacaoAssembleiaSpinEdit.Location = new System.Drawing.Point(650, 26);
            this.cONConvocacaoAssembleiaSpinEdit.Name = "cONConvocacaoAssembleiaSpinEdit";
            this.cONConvocacaoAssembleiaSpinEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.cONConvocacaoAssembleiaSpinEdit.Size = new System.Drawing.Size(144, 20);
            this.cONConvocacaoAssembleiaSpinEdit.TabIndex = 5;
            // 
            // cONDataUltimaAssembleiaDateEdit1
            // 
            this.cONDataUltimaAssembleiaDateEdit1.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.cONDOMINIOSBindingSource, "CONDataUltimaAssembleia", true));
            this.cONDataUltimaAssembleiaDateEdit1.EditValue = null;
            this.cONDataUltimaAssembleiaDateEdit1.Location = new System.Drawing.Point(344, 26);
            this.cONDataUltimaAssembleiaDateEdit1.Name = "cONDataUltimaAssembleiaDateEdit1";
            this.cONDataUltimaAssembleiaDateEdit1.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.cONDataUltimaAssembleiaDateEdit1.Properties.CalendarTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.cONDataUltimaAssembleiaDateEdit1.Size = new System.Drawing.Size(185, 20);
            this.cONDataUltimaAssembleiaDateEdit1.TabIndex = 3;
            // 
            // cONDataUltimaAssembleiaDateEdit
            // 
            this.cONDataUltimaAssembleiaDateEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.cONDOMINIOSBindingSource, "CONDataUltimaAssembleia", true));
            this.cONDataUltimaAssembleiaDateEdit.EditValue = null;
            this.cONDataUltimaAssembleiaDateEdit.Location = new System.Drawing.Point(76, 26);
            this.cONDataUltimaAssembleiaDateEdit.Name = "cONDataUltimaAssembleiaDateEdit";
            this.cONDataUltimaAssembleiaDateEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.cONDataUltimaAssembleiaDateEdit.Properties.CalendarTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.cONDataUltimaAssembleiaDateEdit.Size = new System.Drawing.Size(185, 20);
            this.cONDataUltimaAssembleiaDateEdit.TabIndex = 1;
            // 
            // cONCompetenciaVencidaCheckEdit
            // 
            this.cONCompetenciaVencidaCheckEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.cONDOMINIOSBindingSource, "CONCompetenciaVencida", true));
            this.cONCompetenciaVencidaCheckEdit.Location = new System.Drawing.Point(119, 4);
            this.cONCompetenciaVencidaCheckEdit.Name = "cONCompetenciaVencidaCheckEdit";
            this.cONCompetenciaVencidaCheckEdit.Properties.Caption = "";
            this.cONCompetenciaVencidaCheckEdit.Size = new System.Drawing.Size(22, 19);
            this.cONCompetenciaVencidaCheckEdit.TabIndex = 8;
            // 
            // cONLeituraGasCheckEdit
            // 
            this.cONLeituraGasCheckEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.cONDOMINIOSBindingSource, "CONLeituraGas", true));
            this.cONLeituraGasCheckEdit.Location = new System.Drawing.Point(284, 5);
            this.cONLeituraGasCheckEdit.Name = "cONLeituraGasCheckEdit";
            this.cONLeituraGasCheckEdit.Properties.Caption = "";
            this.cONLeituraGasCheckEdit.Size = new System.Drawing.Size(24, 19);
            this.cONLeituraGasCheckEdit.TabIndex = 14;
            // 
            // groupControl6
            // 
            this.groupControl6.Controls.Add(this.memoEdit1);
            this.groupControl6.Dock = System.Windows.Forms.DockStyle.Fill;
            this.groupControl6.Location = new System.Drawing.Point(0, 370);
            this.groupControl6.Name = "groupControl6";
            this.groupControl6.Size = new System.Drawing.Size(1294, 384);
            this.groupControl6.TabIndex = 15;
            this.groupControl6.Text = ":: Mensagem do Boleto ::";
            // 
            // memoEdit1
            // 
            this.memoEdit1.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.cONDOMINIOSBindingSource, "CONMenBoleto", true));
            this.memoEdit1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.memoEdit1.Location = new System.Drawing.Point(2, 20);
            this.memoEdit1.Name = "memoEdit1";
            this.memoEdit1.Size = new System.Drawing.Size(1290, 362);
            this.memoEdit1.TabIndex = 0;
            // 
            // groupControl7
            // 
            this.groupControl7.Controls.Add(this.lookUpEdit3);
            this.groupControl7.Controls.Add(label7);
            this.groupControl7.Controls.Add(label3);
            this.groupControl7.Controls.Add(this.lookUpEdit2);
            this.groupControl7.Controls.Add(label5);
            this.groupControl7.Controls.Add(this.lookUpG2);
            this.groupControl7.Controls.Add(this.lookUpG);
            this.groupControl7.Controls.Add(label2);
            this.groupControl7.Dock = System.Windows.Forms.DockStyle.Top;
            this.groupControl7.Location = new System.Drawing.Point(0, 0);
            this.groupControl7.Name = "groupControl7";
            this.groupControl7.Size = new System.Drawing.Size(1294, 78);
            this.groupControl7.TabIndex = 16;
            this.groupControl7.Text = ":: Gerentes ::";
            // 
            // lookUpEdit3
            // 
            this.lookUpEdit3.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.cONDOMINIOSBindingSource, "CONBalancete_USU", true));
            this.lookUpEdit3.Location = new System.Drawing.Point(343, 50);
            this.lookUpEdit3.Name = "lookUpEdit3";
            this.lookUpEdit3.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.lookUpEdit3.Properties.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("USUNome", "Nome", 53, DevExpress.Utils.FormatType.None, "", true, DevExpress.Utils.HorzAlignment.Near)});
            this.lookUpEdit3.Properties.DataSource = this.aUDITORESBindingSource;
            this.lookUpEdit3.Properties.DisplayMember = "USUNome";
            this.lookUpEdit3.Properties.ShowHeader = false;
            this.lookUpEdit3.Properties.ValueMember = "USU";
            this.lookUpEdit3.Size = new System.Drawing.Size(185, 20);
            this.lookUpEdit3.TabIndex = 9;
            // 
            // lookUpEdit2
            // 
            this.lookUpEdit2.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.cONDOMINIOSBindingSource, "CONCobranca_USU", true));
            this.lookUpEdit2.Location = new System.Drawing.Point(343, 24);
            this.lookUpEdit2.Name = "lookUpEdit2";
            this.lookUpEdit2.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.lookUpEdit2.Properties.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("USUNome", "Nome", 53, DevExpress.Utils.FormatType.None, "", true, DevExpress.Utils.HorzAlignment.Near)});
            this.lookUpEdit2.Properties.DataSource = this.aUDITORESBindingSource;
            this.lookUpEdit2.Properties.DisplayMember = "USUNome";
            this.lookUpEdit2.Properties.ShowHeader = false;
            this.lookUpEdit2.Properties.ValueMember = "USU";
            this.lookUpEdit2.Size = new System.Drawing.Size(185, 20);
            this.lookUpEdit2.TabIndex = 6;
            // 
            // lookUpG2
            // 
            this.lookUpG2.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.cONDOMINIOSBindingSource, "CONAuditor2_USU", true));
            this.lookUpG2.Location = new System.Drawing.Point(76, 52);
            this.lookUpG2.Name = "lookUpG2";
            this.lookUpG2.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.lookUpG2.Properties.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("USUNome", "Nome", 53, DevExpress.Utils.FormatType.None, "", true, DevExpress.Utils.HorzAlignment.Near)});
            this.lookUpG2.Properties.DataSource = this.aUDITORESBindingSource;
            this.lookUpG2.Properties.DisplayMember = "USUNome";
            this.lookUpG2.Properties.ShowHeader = false;
            this.lookUpG2.Properties.ValueMember = "USU";
            this.lookUpG2.Size = new System.Drawing.Size(185, 20);
            this.lookUpG2.TabIndex = 4;
            // 
            // lookUpG
            // 
            this.lookUpG.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.cONDOMINIOSBindingSource, "CONAuditor1_USU", true));
            this.lookUpG.Location = new System.Drawing.Point(76, 26);
            this.lookUpG.Name = "lookUpG";
            this.lookUpG.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.lookUpG.Properties.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("USUNome", "Nome", 53, DevExpress.Utils.FormatType.None, "", true, DevExpress.Utils.HorzAlignment.Near)});
            this.lookUpG.Properties.DataSource = this.aUDITORESBindingSource;
            this.lookUpG.Properties.DisplayMember = "USUNome";
            this.lookUpG.Properties.ShowHeader = false;
            this.lookUpG.Properties.ValueMember = "USU";
            this.lookUpG.Size = new System.Drawing.Size(185, 20);
            this.lookUpG.TabIndex = 3;
            // 
            // panelControl1
            // 
            this.panelControl1.Controls.Add(this.imageComboBoxEdit2);
            this.panelControl1.Controls.Add(this.checkEdit1);
            this.panelControl1.Controls.Add(this.spinEdit1);
            this.panelControl1.Controls.Add(label6);
            this.panelControl1.Controls.Add(label4);
            this.panelControl1.Controls.Add(cONLeituraGasLabel);
            this.panelControl1.Controls.Add(this.cONLeituraGasCheckEdit);
            this.panelControl1.Controls.Add(this.cONCompetenciaVencidaCheckEdit);
            this.panelControl1.Controls.Add(cONCompetenciaVencidaLabel);
            this.panelControl1.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panelControl1.Location = new System.Drawing.Point(0, 754);
            this.panelControl1.Name = "panelControl1";
            this.panelControl1.Size = new System.Drawing.Size(1294, 53);
            this.panelControl1.TabIndex = 17;
            // 
            // imageComboBoxEdit2
            // 
            this.imageComboBoxEdit2.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.cONDOMINIOSBindingSource, "CONMalote", true));
            this.imageComboBoxEdit2.Location = new System.Drawing.Point(563, 3);
            this.imageComboBoxEdit2.Name = "imageComboBoxEdit2";
            this.imageComboBoxEdit2.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.imageComboBoxEdit2.Properties.DropDownRows = 15;
            this.imageComboBoxEdit2.Properties.Sorted = true;
            this.imageComboBoxEdit2.Size = new System.Drawing.Size(214, 20);
            this.imageComboBoxEdit2.TabIndex = 20;
            // 
            // checkEdit1
            // 
            this.checkEdit1.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.cONDOMINIOSBindingSource, "CONNotaAutomaica", true));
            this.checkEdit1.Location = new System.Drawing.Point(403, 4);
            this.checkEdit1.Name = "checkEdit1";
            this.checkEdit1.Properties.Caption = "Nota autom�tica";
            this.checkEdit1.Size = new System.Drawing.Size(109, 19);
            this.checkEdit1.TabIndex = 19;
            // 
            // spinEdit1
            // 
            this.spinEdit1.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.cONDOMINIOSBindingSource, "CONProtesto", true));
            this.spinEdit1.EditValue = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.spinEdit1.Location = new System.Drawing.Point(59, 5);
            this.spinEdit1.Name = "spinEdit1";
            this.spinEdit1.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.spinEdit1.Size = new System.Drawing.Size(54, 20);
            toolTipTitleItem1.Text = "Dias para protesto";
            toolTipItem1.LeftIndent = 6;
            toolTipItem1.Text = "Coloque zero para n�o protestar";
            superToolTip1.Items.Add(toolTipTitleItem1);
            superToolTip1.Items.Add(toolTipItem1);
            this.spinEdit1.SuperTip = superToolTip1;
            this.spinEdit1.TabIndex = 18;
            // 
            // groupControl8
            // 
            this.groupControl8.Controls.Add(this.textEdit2);
            this.groupControl8.Controls.Add(this.textEdit1);
            this.groupControl8.Controls.Add(label8);
            this.groupControl8.Controls.Add(label9);
            this.groupControl8.Dock = System.Windows.Forms.DockStyle.Top;
            this.groupControl8.Location = new System.Drawing.Point(0, 318);
            this.groupControl8.Name = "groupControl8";
            this.groupControl8.Size = new System.Drawing.Size(1294, 52);
            this.groupControl8.TabIndex = 18;
            this.groupControl8.Text = ":: Site da Prefeitura ::";
            // 
            // textEdit2
            // 
            this.textEdit2.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.cONDOMINIOSBindingSource, "CONSenhaPref", true));
            this.textEdit2.Location = new System.Drawing.Point(343, 27);
            this.textEdit2.Name = "textEdit2";
            this.textEdit2.Size = new System.Drawing.Size(185, 20);
            this.textEdit2.TabIndex = 4;
            // 
            // textEdit1
            // 
            this.textEdit1.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.cONDOMINIOSBindingSource, "CONUsuPref", true));
            this.textEdit1.Location = new System.Drawing.Point(76, 26);
            this.textEdit1.Name = "textEdit1";
            this.textEdit1.Size = new System.Drawing.Size(185, 20);
            this.textEdit1.TabIndex = 3;
            // 
            // tiposInadimplenciaBindingSource
            // 
            this.tiposInadimplenciaBindingSource.DataMember = "TiposInadimplencia";
            this.tiposInadimplenciaBindingSource.DataSource = typeof(CadastrosProc.Condominios.dCondominiosCampos);
            // 
            // TabOperacional
            // 
            this.Controls.Add(this.groupControl6);
            this.Controls.Add(this.groupControl8);
            this.Controls.Add(this.panelControl1);
            this.Controls.Add(this.groupControl5);
            this.Controls.Add(this.groupControl3);
            this.Controls.Add(this.groupControl2);
            this.Controls.Add(this.groupControl1);
            this.Controls.Add(this.groupControl7);
            this.Doc = System.Windows.Forms.DockStyle.Fill;
            this.LookAndFeel.UseDefaultLookAndFeel = false;
            this.Name = "TabOperacional";
            this.Size = new System.Drawing.Size(1294, 807);
            this.Load += new System.EventHandler(this.TabOperacional_Load);
            ((System.ComponentModel.ISupportInitialize)(this.StyleController_F)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl1)).EndInit();
            this.groupControl1.ResumeLayout(false);
            this.groupControl1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.lookUpEditADV.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cONDOMINIOSBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.bindingSourceFornecedores)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.aUDITORESBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl2)).EndInit();
            this.groupControl2.ResumeLayout(false);
            this.groupControl2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.lookUpEdit5.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.conTasLogicasBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEditAviso.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.imageComboBoxEdit1.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl3)).EndInit();
            this.groupControl3.ResumeLayout(false);
            this.groupControl3.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.lookUpEdit4.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dPlanosSeguroConteudoBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cONCorretoraSeguros_FRNLookUpEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cONSeguradora_FRNLookUpEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl5)).EndInit();
            this.groupControl5.ResumeLayout(false);
            this.groupControl5.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.cONConvocacaoAssembleiaSpinEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cONDataUltimaAssembleiaDateEdit1.Properties.CalendarTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cONDataUltimaAssembleiaDateEdit1.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cONDataUltimaAssembleiaDateEdit.Properties.CalendarTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cONDataUltimaAssembleiaDateEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cONCompetenciaVencidaCheckEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cONLeituraGasCheckEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl6)).EndInit();
            this.groupControl6.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.memoEdit1.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl7)).EndInit();
            this.groupControl7.ResumeLayout(false);
            this.groupControl7.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.lookUpEdit3.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lookUpEdit2.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lookUpG2.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lookUpG.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl1)).EndInit();
            this.panelControl1.ResumeLayout(false);
            this.panelControl1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.imageComboBoxEdit2.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit1.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.spinEdit1.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl8)).EndInit();
            this.groupControl8.ResumeLayout(false);
            this.groupControl8.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit2.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit1.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tiposInadimplenciaBindingSource)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraEditors.GroupControl groupControl1;
        private System.Windows.Forms.BindingSource cONDOMINIOSBindingSource;
        private DevExpress.XtraEditors.GroupControl groupControl2;
        private System.Windows.Forms.BindingSource aUDITORESBindingSource;
        private DevExpress.XtraEditors.GroupControl groupControl3;
        private DevExpress.XtraEditors.LookUpEdit cONCorretoraSeguros_FRNLookUpEdit;
        private DevExpress.XtraEditors.LookUpEdit cONSeguradora_FRNLookUpEdit;
        private DevExpress.XtraEditors.GroupControl groupControl5;
        private DevExpress.XtraEditors.DateEdit cONDataUltimaAssembleiaDateEdit1;
        private DevExpress.XtraEditors.DateEdit cONDataUltimaAssembleiaDateEdit;
        private DevExpress.XtraEditors.SpinEdit cONConvocacaoAssembleiaSpinEdit;
        private DevExpress.XtraEditors.CheckEdit cONCompetenciaVencidaCheckEdit;
        private DevExpress.XtraEditors.CheckEdit cONLeituraGasCheckEdit;
        private DevExpress.XtraEditors.GroupControl groupControl6;
        private DevExpress.XtraEditors.MemoEdit memoEdit1;
        private System.Windows.Forms.BindingSource tiposInadimplenciaBindingSource;
        private DevExpress.XtraEditors.LookUpEdit lookUpEditADV;
        private System.Windows.Forms.BindingSource bindingSourceFornecedores;
        private DevExpress.XtraEditors.ImageComboBoxEdit imageComboBoxEdit1;
        private DevExpress.XtraEditors.TextEdit textEditAviso;
        private DevExpress.XtraEditors.GroupControl groupControl7;
        private DevExpress.XtraEditors.LookUpEdit lookUpG2;
        private DevExpress.XtraEditors.LookUpEdit lookUpG;
        private DevExpress.XtraEditors.PanelControl panelControl1;
        private DevExpress.XtraEditors.LookUpEdit lookUpEdit2;
        private DevExpress.XtraEditors.SpinEdit spinEdit1;
        private DevExpress.XtraEditors.GroupControl groupControl8;
        private DevExpress.XtraEditors.TextEdit textEdit2;
        private DevExpress.XtraEditors.TextEdit textEdit1;
        private DevExpress.XtraEditors.LookUpEdit lookUpEdit3;
        private DevExpress.XtraEditors.CheckEdit checkEdit1;
        private DevExpress.XtraEditors.LookUpEdit lookUpEdit4;
        private System.Windows.Forms.BindingSource dPlanosSeguroConteudoBindingSource;
        private DevExpress.XtraEditors.LookUpEdit lookUpEdit5;
        private System.Windows.Forms.BindingSource conTasLogicasBindingSource;
        private DevExpress.XtraEditors.ImageComboBoxEdit imageComboBoxEdit2;
        private DevExpress.XtraEditors.SimpleButton SBProcuracao;
    }
}
