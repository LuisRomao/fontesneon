namespace Cadastros.Condominios.PaginasCondominio
{
    partial class TabContas
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            DevExpress.XtraGrid.GridLevelNode gridLevelNode1 = new DevExpress.XtraGrid.GridLevelNode();
            this.gridView3 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colCTLCCTRentabilidade = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colCTLCCTTranArrecadado = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemImageComboBox3 = new DevExpress.XtraEditors.Repository.RepositoryItemImageComboBox();
            this.colCCT = new DevExpress.XtraGrid.Columns.GridColumn();
            this.LookUpCCT = new DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit();
            this.contaCorrenTeBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.contasbindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.dConta1 = new Cadastros.Condominios.PaginasCondominio.dConta();
            this.gridColumn3 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemButtonEdit2 = new DevExpress.XtraEditors.Repository.RepositoryItemButtonEdit();
            this.gridControl2 = new DevExpress.XtraGrid.GridControl();
            this.gridView2 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colCTL = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colCTLAtiva = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colCTLTitulo = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colCTLTipo = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemImageComboBox1 = new DevExpress.XtraEditors.Repository.RepositoryItemImageComboBox();
            this.gridColumn2 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridControl1 = new DevExpress.XtraGrid.GridControl();
            this.gridView1 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colCCT_BCO = new DevExpress.XtraGrid.Columns.GridColumn();
            this.LookUpEditBANCOS = new DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit();
            this.BANCOSbindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.colCCTAgencia = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colCCTAgenciaDg = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colCCTConta = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colCCTContaDg = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colCCTCNR = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colCCTAtiva = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colCCTPrincipal = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colCCTLote = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colCCTLoteEmail = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colCCTAplicacao = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colCCTTitulo = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colCCTTipo = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemImageComboBox2 = new DevExpress.XtraEditors.Repository.RepositoryItemImageComboBox();
            this.gridColumn1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemButtonEdit1 = new DevExpress.XtraEditors.Repository.RepositoryItemButtonEdit();
            this.colCCTPagamentoEletronico = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colCCT_CCTPlus = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryMorto = new DevExpress.XtraEditors.Repository.RepositoryItemButtonEdit();
            this.colCCTCaixaPostal = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colCCTDebitoAutomatico = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colCCTRateioCredito = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colCCT1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colDescricaoContaCorrente = new DevExpress.XtraGrid.Columns.GridColumn();
            this.ButtonEditAg = new DevExpress.XtraEditors.Repository.RepositoryItemButtonEdit();
            this.LookUpEditPlus = new DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit();
            this.cONDOMINIOSBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.groupControl1 = new DevExpress.XtraEditors.GroupControl();
            this.splitterControl1 = new DevExpress.XtraEditors.SplitterControl();
            this.groupControl2 = new DevExpress.XtraEditors.GroupControl();
            this.panelControl1 = new DevExpress.XtraEditors.PanelControl();
            this.simpleButton1 = new DevExpress.XtraEditors.SimpleButton();
            ((System.ComponentModel.ISupportInitialize)(this.StyleController_F)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemImageComboBox3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LookUpCCT)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.contaCorrenTeBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.contasbindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dConta1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemButtonEdit2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemImageComboBox1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LookUpEditBANCOS)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.BANCOSbindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemImageComboBox2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemButtonEdit1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryMorto)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ButtonEditAg)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LookUpEditPlus)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cONDOMINIOSBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl1)).BeginInit();
            this.groupControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl2)).BeginInit();
            this.groupControl2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl1)).BeginInit();
            this.panelControl1.SuspendLayout();
            this.SuspendLayout();
            // 
            // ToolTipController_F
            // 
            this.ToolTipController_F.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.ToolTipController_F.Appearance.ForeColor = System.Drawing.Color.Black;
            this.ToolTipController_F.Appearance.Options.UseBackColor = true;
            this.ToolTipController_F.Appearance.Options.UseFont = true;
            this.ToolTipController_F.Appearance.Options.UseForeColor = true;
            this.ToolTipController_F.Appearance.Options.UseTextOptions = true;
            this.ToolTipController_F.Appearance.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            // 
            // StyleController_F
            // 
            this.StyleController_F.AppearanceDisabled.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this.StyleController_F.AppearanceDisabled.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this.StyleController_F.AppearanceDisabled.Options.UseBackColor = true;
            this.StyleController_F.AppearanceDisabled.Options.UseForeColor = true;
            this.StyleController_F.AppearanceFocused.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.StyleController_F.AppearanceFocused.Options.UseBackColor = true;
            // 
            // gridView3
            // 
            this.gridView3.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colCTLCCTRentabilidade,
            this.colCTLCCTTranArrecadado,
            this.colCCT,
            this.gridColumn3});
            this.gridView3.GridControl = this.gridControl2;
            this.gridView3.Name = "gridView3";
            this.gridView3.NewItemRowText = "Nova";
            this.gridView3.OptionsDetail.ShowDetailTabs = false;
            this.gridView3.OptionsView.ColumnAutoWidth = false;
            this.gridView3.OptionsView.NewItemRowPosition = DevExpress.XtraGrid.Views.Grid.NewItemRowPosition.Bottom;
            this.gridView3.OptionsView.ShowGroupPanel = false;
            this.gridView3.InitNewRow += new DevExpress.XtraGrid.Views.Grid.InitNewRowEventHandler(this.gridView3_InitNewRow);
            this.gridView3.RowUpdated += new DevExpress.XtraGrid.Views.Base.RowObjectEventHandler(this.gridView3_RowUpdated);
            // 
            // colCTLCCTRentabilidade
            // 
            this.colCTLCCTRentabilidade.Caption = "Rentabilidade";
            this.colCTLCCTRentabilidade.FieldName = "CTLCCTRentabilidade";
            this.colCTLCCTRentabilidade.Name = "colCTLCCTRentabilidade";
            this.colCTLCCTRentabilidade.Visible = true;
            this.colCTLCCTRentabilidade.VisibleIndex = 1;
            this.colCTLCCTRentabilidade.Width = 97;
            // 
            // colCTLCCTTranArrecadado
            // 
            this.colCTLCCTTranArrecadado.Caption = "Transferir Arrecada��o";
            this.colCTLCCTTranArrecadado.ColumnEdit = this.repositoryItemImageComboBox3;
            this.colCTLCCTTranArrecadado.FieldName = "CTLCCTTranArrecadado";
            this.colCTLCCTTranArrecadado.Name = "colCTLCCTTranArrecadado";
            this.colCTLCCTTranArrecadado.Visible = true;
            this.colCTLCCTTranArrecadado.VisibleIndex = 2;
            this.colCTLCCTTranArrecadado.Width = 281;
            // 
            // repositoryItemImageComboBox3
            // 
            this.repositoryItemImageComboBox3.AutoHeight = false;
            this.repositoryItemImageComboBox3.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemImageComboBox3.Name = "repositoryItemImageComboBox3";
            // 
            // colCCT
            // 
            this.colCCT.Caption = "Conta F�sica";
            this.colCCT.ColumnEdit = this.LookUpCCT;
            this.colCCT.FieldName = "CCT";
            this.colCCT.Name = "colCCT";
            this.colCCT.Visible = true;
            this.colCCT.VisibleIndex = 0;
            this.colCCT.Width = 267;
            // 
            // LookUpCCT
            // 
            this.LookUpCCT.AutoHeight = false;
            this.LookUpCCT.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LookUpCCT.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("CCTTitulo", "T�tulo", 60, DevExpress.Utils.FormatType.None, "", true, DevExpress.Utils.HorzAlignment.Near),
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("CCT_BCO", "Banco", 15, DevExpress.Utils.FormatType.Numeric, "", true, DevExpress.Utils.HorzAlignment.Far),
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("CCTConta", "Conta", 30, DevExpress.Utils.FormatType.Numeric, "", true, DevExpress.Utils.HorzAlignment.Far)});
            this.LookUpCCT.DataSource = this.contaCorrenTeBindingSource;
            this.LookUpCCT.DisplayMember = "CCTTitulo";
            this.LookUpCCT.Name = "LookUpCCT";
            this.LookUpCCT.NullText = "--";
            this.LookUpCCT.PopupWidth = 500;
            this.LookUpCCT.ValueMember = "CCT";
            // 
            // contaCorrenTeBindingSource
            // 
            this.contaCorrenTeBindingSource.DataMember = "ContaCorrenTe";
            this.contaCorrenTeBindingSource.DataSource = this.contasbindingSource;
            // 
            // contasbindingSource
            // 
            this.contasbindingSource.DataSource = this.dConta1;
            this.contasbindingSource.Position = 0;
            // 
            // dConta1
            // 
            this.dConta1.DataSetName = "dConta";
            this.dConta1.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // gridColumn3
            // 
            this.gridColumn3.Caption = "gridColumn3";
            this.gridColumn3.ColumnEdit = this.repositoryItemButtonEdit2;
            this.gridColumn3.Fixed = DevExpress.XtraGrid.Columns.FixedStyle.Right;
            this.gridColumn3.Name = "gridColumn3";
            this.gridColumn3.OptionsColumn.ShowCaption = false;
            this.gridColumn3.Visible = true;
            this.gridColumn3.VisibleIndex = 3;
            this.gridColumn3.Width = 25;
            // 
            // repositoryItemButtonEdit2
            // 
            this.repositoryItemButtonEdit2.AutoHeight = false;
            this.repositoryItemButtonEdit2.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Close)});
            this.repositoryItemButtonEdit2.Name = "repositoryItemButtonEdit2";
            this.repositoryItemButtonEdit2.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.HideTextEditor;
            this.repositoryItemButtonEdit2.ButtonClick += new DevExpress.XtraEditors.Controls.ButtonPressedEventHandler(this.repositoryItemButtonEdit2_ButtonClick);
            // 
            // gridControl2
            // 
            this.gridControl2.DataMember = "ConTasLogicas";
            this.gridControl2.DataSource = this.contasbindingSource;
            this.gridControl2.Dock = System.Windows.Forms.DockStyle.Fill;
            gridLevelNode1.LevelTemplate = this.gridView3;
            gridLevelNode1.RelationName = "FK_CTLxCCT_ConTasLogicas";
            this.gridControl2.LevelTree.Nodes.AddRange(new DevExpress.XtraGrid.GridLevelNode[] {
            gridLevelNode1});
            this.gridControl2.Location = new System.Drawing.Point(2, 20);
            this.gridControl2.MainView = this.gridView2;
            this.gridControl2.Name = "gridControl2";
            this.gridControl2.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemImageComboBox1,
            this.LookUpCCT,
            this.repositoryItemButtonEdit2,
            this.repositoryItemImageComboBox3});
            this.gridControl2.Size = new System.Drawing.Size(1474, 541);
            this.gridControl2.TabIndex = 0;
            this.gridControl2.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridView2,
            this.gridView3});
            // 
            // gridView2
            // 
            this.gridView2.Appearance.ColumnFilterButton.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(221)))), ((int)(((byte)(236)))), ((int)(((byte)(254)))));
            this.gridView2.Appearance.ColumnFilterButton.BackColor2 = System.Drawing.Color.FromArgb(((int)(((byte)(132)))), ((int)(((byte)(171)))), ((int)(((byte)(228)))));
            this.gridView2.Appearance.ColumnFilterButton.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(221)))), ((int)(((byte)(236)))), ((int)(((byte)(254)))));
            this.gridView2.Appearance.ColumnFilterButton.ForeColor = System.Drawing.Color.Black;
            this.gridView2.Appearance.ColumnFilterButton.GradientMode = System.Drawing.Drawing2D.LinearGradientMode.Vertical;
            this.gridView2.Appearance.ColumnFilterButton.Options.UseBackColor = true;
            this.gridView2.Appearance.ColumnFilterButton.Options.UseBorderColor = true;
            this.gridView2.Appearance.ColumnFilterButton.Options.UseForeColor = true;
            this.gridView2.Appearance.ColumnFilterButtonActive.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(247)))), ((int)(((byte)(251)))), ((int)(((byte)(255)))));
            this.gridView2.Appearance.ColumnFilterButtonActive.BackColor2 = System.Drawing.Color.FromArgb(((int)(((byte)(154)))), ((int)(((byte)(190)))), ((int)(((byte)(243)))));
            this.gridView2.Appearance.ColumnFilterButtonActive.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(247)))), ((int)(((byte)(251)))), ((int)(((byte)(255)))));
            this.gridView2.Appearance.ColumnFilterButtonActive.ForeColor = System.Drawing.Color.Black;
            this.gridView2.Appearance.ColumnFilterButtonActive.GradientMode = System.Drawing.Drawing2D.LinearGradientMode.Vertical;
            this.gridView2.Appearance.ColumnFilterButtonActive.Options.UseBackColor = true;
            this.gridView2.Appearance.ColumnFilterButtonActive.Options.UseBorderColor = true;
            this.gridView2.Appearance.ColumnFilterButtonActive.Options.UseForeColor = true;
            this.gridView2.Appearance.Empty.BackColor = System.Drawing.Color.White;
            this.gridView2.Appearance.Empty.Options.UseBackColor = true;
            this.gridView2.Appearance.EvenRow.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(231)))), ((int)(((byte)(242)))), ((int)(((byte)(254)))));
            this.gridView2.Appearance.EvenRow.ForeColor = System.Drawing.Color.Black;
            this.gridView2.Appearance.EvenRow.Options.UseBackColor = true;
            this.gridView2.Appearance.EvenRow.Options.UseForeColor = true;
            this.gridView2.Appearance.FilterCloseButton.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(221)))), ((int)(((byte)(236)))), ((int)(((byte)(254)))));
            this.gridView2.Appearance.FilterCloseButton.BackColor2 = System.Drawing.Color.FromArgb(((int)(((byte)(132)))), ((int)(((byte)(171)))), ((int)(((byte)(228)))));
            this.gridView2.Appearance.FilterCloseButton.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(221)))), ((int)(((byte)(236)))), ((int)(((byte)(254)))));
            this.gridView2.Appearance.FilterCloseButton.ForeColor = System.Drawing.Color.Black;
            this.gridView2.Appearance.FilterCloseButton.GradientMode = System.Drawing.Drawing2D.LinearGradientMode.Vertical;
            this.gridView2.Appearance.FilterCloseButton.Options.UseBackColor = true;
            this.gridView2.Appearance.FilterCloseButton.Options.UseBorderColor = true;
            this.gridView2.Appearance.FilterCloseButton.Options.UseForeColor = true;
            this.gridView2.Appearance.FilterPanel.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(62)))), ((int)(((byte)(109)))), ((int)(((byte)(185)))));
            this.gridView2.Appearance.FilterPanel.ForeColor = System.Drawing.Color.White;
            this.gridView2.Appearance.FilterPanel.Options.UseBackColor = true;
            this.gridView2.Appearance.FilterPanel.Options.UseForeColor = true;
            this.gridView2.Appearance.FixedLine.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(59)))), ((int)(((byte)(97)))), ((int)(((byte)(156)))));
            this.gridView2.Appearance.FixedLine.Options.UseBackColor = true;
            this.gridView2.Appearance.FocusedCell.BackColor = System.Drawing.Color.White;
            this.gridView2.Appearance.FocusedCell.ForeColor = System.Drawing.Color.Black;
            this.gridView2.Appearance.FocusedCell.Options.UseBackColor = true;
            this.gridView2.Appearance.FocusedCell.Options.UseForeColor = true;
            this.gridView2.Appearance.FocusedRow.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(49)))), ((int)(((byte)(106)))), ((int)(((byte)(197)))));
            this.gridView2.Appearance.FocusedRow.ForeColor = System.Drawing.Color.White;
            this.gridView2.Appearance.FocusedRow.Options.UseBackColor = true;
            this.gridView2.Appearance.FocusedRow.Options.UseForeColor = true;
            this.gridView2.Appearance.FooterPanel.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(221)))), ((int)(((byte)(236)))), ((int)(((byte)(254)))));
            this.gridView2.Appearance.FooterPanel.BackColor2 = System.Drawing.Color.FromArgb(((int)(((byte)(132)))), ((int)(((byte)(171)))), ((int)(((byte)(228)))));
            this.gridView2.Appearance.FooterPanel.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(221)))), ((int)(((byte)(236)))), ((int)(((byte)(254)))));
            this.gridView2.Appearance.FooterPanel.ForeColor = System.Drawing.Color.Black;
            this.gridView2.Appearance.FooterPanel.GradientMode = System.Drawing.Drawing2D.LinearGradientMode.Vertical;
            this.gridView2.Appearance.FooterPanel.Options.UseBackColor = true;
            this.gridView2.Appearance.FooterPanel.Options.UseBorderColor = true;
            this.gridView2.Appearance.FooterPanel.Options.UseForeColor = true;
            this.gridView2.Appearance.GroupButton.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(193)))), ((int)(((byte)(216)))), ((int)(((byte)(247)))));
            this.gridView2.Appearance.GroupButton.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(193)))), ((int)(((byte)(216)))), ((int)(((byte)(247)))));
            this.gridView2.Appearance.GroupButton.ForeColor = System.Drawing.Color.Black;
            this.gridView2.Appearance.GroupButton.Options.UseBackColor = true;
            this.gridView2.Appearance.GroupButton.Options.UseBorderColor = true;
            this.gridView2.Appearance.GroupButton.Options.UseForeColor = true;
            this.gridView2.Appearance.GroupFooter.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(193)))), ((int)(((byte)(216)))), ((int)(((byte)(247)))));
            this.gridView2.Appearance.GroupFooter.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(193)))), ((int)(((byte)(216)))), ((int)(((byte)(247)))));
            this.gridView2.Appearance.GroupFooter.ForeColor = System.Drawing.Color.Black;
            this.gridView2.Appearance.GroupFooter.Options.UseBackColor = true;
            this.gridView2.Appearance.GroupFooter.Options.UseBorderColor = true;
            this.gridView2.Appearance.GroupFooter.Options.UseForeColor = true;
            this.gridView2.Appearance.GroupPanel.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(62)))), ((int)(((byte)(109)))), ((int)(((byte)(185)))));
            this.gridView2.Appearance.GroupPanel.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(221)))), ((int)(((byte)(236)))), ((int)(((byte)(254)))));
            this.gridView2.Appearance.GroupPanel.Options.UseBackColor = true;
            this.gridView2.Appearance.GroupPanel.Options.UseForeColor = true;
            this.gridView2.Appearance.GroupRow.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(193)))), ((int)(((byte)(216)))), ((int)(((byte)(247)))));
            this.gridView2.Appearance.GroupRow.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(193)))), ((int)(((byte)(216)))), ((int)(((byte)(247)))));
            this.gridView2.Appearance.GroupRow.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Bold);
            this.gridView2.Appearance.GroupRow.ForeColor = System.Drawing.Color.Black;
            this.gridView2.Appearance.GroupRow.Options.UseBackColor = true;
            this.gridView2.Appearance.GroupRow.Options.UseBorderColor = true;
            this.gridView2.Appearance.GroupRow.Options.UseFont = true;
            this.gridView2.Appearance.GroupRow.Options.UseForeColor = true;
            this.gridView2.Appearance.HeaderPanel.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(221)))), ((int)(((byte)(236)))), ((int)(((byte)(254)))));
            this.gridView2.Appearance.HeaderPanel.BackColor2 = System.Drawing.Color.FromArgb(((int)(((byte)(132)))), ((int)(((byte)(171)))), ((int)(((byte)(228)))));
            this.gridView2.Appearance.HeaderPanel.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(221)))), ((int)(((byte)(236)))), ((int)(((byte)(254)))));
            this.gridView2.Appearance.HeaderPanel.ForeColor = System.Drawing.Color.Black;
            this.gridView2.Appearance.HeaderPanel.GradientMode = System.Drawing.Drawing2D.LinearGradientMode.Vertical;
            this.gridView2.Appearance.HeaderPanel.Options.UseBackColor = true;
            this.gridView2.Appearance.HeaderPanel.Options.UseBorderColor = true;
            this.gridView2.Appearance.HeaderPanel.Options.UseForeColor = true;
            this.gridView2.Appearance.HideSelectionRow.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(106)))), ((int)(((byte)(153)))), ((int)(((byte)(228)))));
            this.gridView2.Appearance.HideSelectionRow.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(208)))), ((int)(((byte)(224)))), ((int)(((byte)(251)))));
            this.gridView2.Appearance.HideSelectionRow.Options.UseBackColor = true;
            this.gridView2.Appearance.HideSelectionRow.Options.UseForeColor = true;
            this.gridView2.Appearance.HorzLine.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(99)))), ((int)(((byte)(127)))), ((int)(((byte)(196)))));
            this.gridView2.Appearance.HorzLine.Options.UseBackColor = true;
            this.gridView2.Appearance.OddRow.BackColor = System.Drawing.Color.White;
            this.gridView2.Appearance.OddRow.ForeColor = System.Drawing.Color.Black;
            this.gridView2.Appearance.OddRow.Options.UseBackColor = true;
            this.gridView2.Appearance.OddRow.Options.UseForeColor = true;
            this.gridView2.Appearance.Preview.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(249)))), ((int)(((byte)(252)))), ((int)(((byte)(255)))));
            this.gridView2.Appearance.Preview.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(88)))), ((int)(((byte)(129)))), ((int)(((byte)(185)))));
            this.gridView2.Appearance.Preview.Options.UseBackColor = true;
            this.gridView2.Appearance.Preview.Options.UseForeColor = true;
            this.gridView2.Appearance.Row.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(231)))), ((int)(((byte)(242)))), ((int)(((byte)(254)))));
            this.gridView2.Appearance.Row.ForeColor = System.Drawing.Color.Black;
            this.gridView2.Appearance.Row.Options.UseBackColor = true;
            this.gridView2.Appearance.Row.Options.UseForeColor = true;
            this.gridView2.Appearance.RowSeparator.BackColor = System.Drawing.Color.White;
            this.gridView2.Appearance.RowSeparator.Options.UseBackColor = true;
            this.gridView2.Appearance.SelectedRow.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(69)))), ((int)(((byte)(126)))), ((int)(((byte)(217)))));
            this.gridView2.Appearance.SelectedRow.ForeColor = System.Drawing.Color.White;
            this.gridView2.Appearance.SelectedRow.Options.UseBackColor = true;
            this.gridView2.Appearance.SelectedRow.Options.UseForeColor = true;
            this.gridView2.Appearance.VertLine.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(99)))), ((int)(((byte)(127)))), ((int)(((byte)(196)))));
            this.gridView2.Appearance.VertLine.Options.UseBackColor = true;
            this.gridView2.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colCTL,
            this.colCTLAtiva,
            this.colCTLTitulo,
            this.colCTLTipo,
            this.gridColumn2});
            this.gridView2.GridControl = this.gridControl2;
            this.gridView2.Name = "gridView2";
            this.gridView2.OptionsDetail.AllowExpandEmptyDetails = true;
            this.gridView2.OptionsDetail.ShowDetailTabs = false;
            this.gridView2.OptionsView.ColumnAutoWidth = false;
            this.gridView2.OptionsView.NewItemRowPosition = DevExpress.XtraGrid.Views.Grid.NewItemRowPosition.Bottom;
            this.gridView2.OptionsView.ShowGroupPanel = false;
            this.gridView2.SortInfo.AddRange(new DevExpress.XtraGrid.Columns.GridColumnSortInfo[] {
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colCTLTitulo, DevExpress.Data.ColumnSortOrder.Ascending)});
            this.gridView2.InitNewRow += new DevExpress.XtraGrid.Views.Grid.InitNewRowEventHandler(this.gridView2_InitNewRow);
            this.gridView2.RowUpdated += new DevExpress.XtraGrid.Views.Base.RowObjectEventHandler(this.gridView2_RowUpdated);
            // 
            // colCTL
            // 
            this.colCTL.FieldName = "CTL";
            this.colCTL.Name = "colCTL";
            this.colCTL.OptionsColumn.ReadOnly = true;
            // 
            // colCTLAtiva
            // 
            this.colCTLAtiva.Caption = "Ativa";
            this.colCTLAtiva.FieldName = "CTLAtiva";
            this.colCTLAtiva.Name = "colCTLAtiva";
            this.colCTLAtiva.Visible = true;
            this.colCTLAtiva.VisibleIndex = 2;
            this.colCTLAtiva.Width = 42;
            // 
            // colCTLTitulo
            // 
            this.colCTLTitulo.Caption = "T�tulo";
            this.colCTLTitulo.FieldName = "CTLTitulo";
            this.colCTLTitulo.Name = "colCTLTitulo";
            this.colCTLTitulo.Visible = true;
            this.colCTLTitulo.VisibleIndex = 0;
            this.colCTLTitulo.Width = 259;
            // 
            // colCTLTipo
            // 
            this.colCTLTipo.Caption = "Tipo";
            this.colCTLTipo.ColumnEdit = this.repositoryItemImageComboBox1;
            this.colCTLTipo.FieldName = "CTLTipo";
            this.colCTLTipo.Name = "colCTLTipo";
            this.colCTLTipo.Visible = true;
            this.colCTLTipo.VisibleIndex = 1;
            // 
            // repositoryItemImageComboBox1
            // 
            this.repositoryItemImageComboBox1.AutoHeight = false;
            this.repositoryItemImageComboBox1.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemImageComboBox1.Name = "repositoryItemImageComboBox1";
            // 
            // gridColumn2
            // 
            this.gridColumn2.Caption = "gridColumn2";
            this.gridColumn2.ColumnEdit = this.repositoryItemButtonEdit2;
            this.gridColumn2.Fixed = DevExpress.XtraGrid.Columns.FixedStyle.Right;
            this.gridColumn2.Name = "gridColumn2";
            this.gridColumn2.OptionsColumn.ShowCaption = false;
            this.gridColumn2.Visible = true;
            this.gridColumn2.VisibleIndex = 3;
            this.gridColumn2.Width = 25;
            // 
            // gridControl1
            // 
            this.gridControl1.Cursor = System.Windows.Forms.Cursors.Default;
            this.gridControl1.DataMember = "ContaCorrenTe";
            this.gridControl1.DataSource = this.contasbindingSource;
            this.gridControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridControl1.Location = new System.Drawing.Point(2, 20);
            this.gridControl1.MainView = this.gridView1;
            this.gridControl1.Name = "gridControl1";
            this.gridControl1.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.LookUpEditBANCOS,
            this.repositoryItemImageComboBox2,
            this.repositoryItemButtonEdit1,
            this.ButtonEditAg,
            this.LookUpEditPlus,
            this.repositoryMorto});
            this.gridControl1.ShowOnlyPredefinedDetails = true;
            this.gridControl1.Size = new System.Drawing.Size(1474, 174);
            this.gridControl1.TabIndex = 0;
            this.gridControl1.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridView1});
            // 
            // gridView1
            // 
            this.gridView1.Appearance.ColumnFilterButton.BackColor = System.Drawing.Color.Orange;
            this.gridView1.Appearance.ColumnFilterButton.BorderColor = System.Drawing.Color.Orange;
            this.gridView1.Appearance.ColumnFilterButton.ForeColor = System.Drawing.Color.DarkOrange;
            this.gridView1.Appearance.ColumnFilterButton.Options.UseBackColor = true;
            this.gridView1.Appearance.ColumnFilterButton.Options.UseBorderColor = true;
            this.gridView1.Appearance.ColumnFilterButton.Options.UseForeColor = true;
            this.gridView1.Appearance.ColumnFilterButtonActive.BackColor = System.Drawing.Color.DarkOrange;
            this.gridView1.Appearance.ColumnFilterButtonActive.BorderColor = System.Drawing.Color.DarkOrange;
            this.gridView1.Appearance.ColumnFilterButtonActive.ForeColor = System.Drawing.Color.White;
            this.gridView1.Appearance.ColumnFilterButtonActive.Options.UseBackColor = true;
            this.gridView1.Appearance.ColumnFilterButtonActive.Options.UseBorderColor = true;
            this.gridView1.Appearance.ColumnFilterButtonActive.Options.UseForeColor = true;
            this.gridView1.Appearance.Empty.BackColor = System.Drawing.Color.LightSkyBlue;
            this.gridView1.Appearance.Empty.BackColor2 = System.Drawing.Color.SkyBlue;
            this.gridView1.Appearance.Empty.GradientMode = System.Drawing.Drawing2D.LinearGradientMode.BackwardDiagonal;
            this.gridView1.Appearance.Empty.Options.UseBackColor = true;
            this.gridView1.Appearance.EvenRow.BackColor = System.Drawing.Color.Linen;
            this.gridView1.Appearance.EvenRow.BackColor2 = System.Drawing.Color.AntiqueWhite;
            this.gridView1.Appearance.EvenRow.GradientMode = System.Drawing.Drawing2D.LinearGradientMode.ForwardDiagonal;
            this.gridView1.Appearance.EvenRow.Options.UseBackColor = true;
            this.gridView1.Appearance.FilterCloseButton.BackColor = System.Drawing.Color.Orange;
            this.gridView1.Appearance.FilterCloseButton.BorderColor = System.Drawing.Color.Orange;
            this.gridView1.Appearance.FilterCloseButton.Options.UseBackColor = true;
            this.gridView1.Appearance.FilterCloseButton.Options.UseBorderColor = true;
            this.gridView1.Appearance.FilterPanel.BackColor = System.Drawing.Color.DarkOrange;
            this.gridView1.Appearance.FilterPanel.BackColor2 = System.Drawing.Color.Orange;
            this.gridView1.Appearance.FilterPanel.ForeColor = System.Drawing.Color.Black;
            this.gridView1.Appearance.FilterPanel.GradientMode = System.Drawing.Drawing2D.LinearGradientMode.BackwardDiagonal;
            this.gridView1.Appearance.FilterPanel.Options.UseBackColor = true;
            this.gridView1.Appearance.FilterPanel.Options.UseForeColor = true;
            this.gridView1.Appearance.FocusedRow.BackColor = System.Drawing.Color.RoyalBlue;
            this.gridView1.Appearance.FocusedRow.BackColor2 = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this.gridView1.Appearance.FocusedRow.ForeColor = System.Drawing.Color.White;
            this.gridView1.Appearance.FocusedRow.GradientMode = System.Drawing.Drawing2D.LinearGradientMode.Vertical;
            this.gridView1.Appearance.FocusedRow.Options.UseBackColor = true;
            this.gridView1.Appearance.FocusedRow.Options.UseForeColor = true;
            this.gridView1.Appearance.FooterPanel.BackColor = System.Drawing.Color.Orange;
            this.gridView1.Appearance.FooterPanel.BorderColor = System.Drawing.Color.Orange;
            this.gridView1.Appearance.FooterPanel.Options.UseBackColor = true;
            this.gridView1.Appearance.FooterPanel.Options.UseBorderColor = true;
            this.gridView1.Appearance.GroupButton.BackColor = System.Drawing.Color.Wheat;
            this.gridView1.Appearance.GroupButton.BorderColor = System.Drawing.Color.Wheat;
            this.gridView1.Appearance.GroupButton.Options.UseBackColor = true;
            this.gridView1.Appearance.GroupButton.Options.UseBorderColor = true;
            this.gridView1.Appearance.GroupFooter.BackColor = System.Drawing.Color.Wheat;
            this.gridView1.Appearance.GroupFooter.BorderColor = System.Drawing.Color.Wheat;
            this.gridView1.Appearance.GroupFooter.Options.UseBackColor = true;
            this.gridView1.Appearance.GroupFooter.Options.UseBorderColor = true;
            this.gridView1.Appearance.GroupPanel.BackColor = System.Drawing.Color.RoyalBlue;
            this.gridView1.Appearance.GroupPanel.ForeColor = System.Drawing.Color.White;
            this.gridView1.Appearance.GroupPanel.Options.UseBackColor = true;
            this.gridView1.Appearance.GroupPanel.Options.UseForeColor = true;
            this.gridView1.Appearance.GroupRow.BackColor = System.Drawing.Color.Wheat;
            this.gridView1.Appearance.GroupRow.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Bold);
            this.gridView1.Appearance.GroupRow.Options.UseBackColor = true;
            this.gridView1.Appearance.GroupRow.Options.UseFont = true;
            this.gridView1.Appearance.HeaderPanel.BackColor = System.Drawing.Color.Orange;
            this.gridView1.Appearance.HeaderPanel.BorderColor = System.Drawing.Color.Orange;
            this.gridView1.Appearance.HeaderPanel.Options.UseBackColor = true;
            this.gridView1.Appearance.HeaderPanel.Options.UseBorderColor = true;
            this.gridView1.Appearance.HideSelectionRow.BackColor = System.Drawing.Color.LightSlateGray;
            this.gridView1.Appearance.HideSelectionRow.Options.UseBackColor = true;
            this.gridView1.Appearance.HorzLine.BackColor = System.Drawing.Color.Tan;
            this.gridView1.Appearance.HorzLine.Options.UseBackColor = true;
            this.gridView1.Appearance.OddRow.BackColor = System.Drawing.Color.White;
            this.gridView1.Appearance.OddRow.Options.UseBackColor = true;
            this.gridView1.Appearance.Preview.BackColor = System.Drawing.Color.Khaki;
            this.gridView1.Appearance.Preview.BackColor2 = System.Drawing.Color.Cornsilk;
            this.gridView1.Appearance.Preview.Font = new System.Drawing.Font("Tahoma", 7.5F);
            this.gridView1.Appearance.Preview.GradientMode = System.Drawing.Drawing2D.LinearGradientMode.BackwardDiagonal;
            this.gridView1.Appearance.Preview.Options.UseBackColor = true;
            this.gridView1.Appearance.Preview.Options.UseFont = true;
            this.gridView1.Appearance.Row.BackColor = System.Drawing.Color.Linen;
            this.gridView1.Appearance.Row.BackColor2 = System.Drawing.Color.AntiqueWhite;
            this.gridView1.Appearance.Row.GradientMode = System.Drawing.Drawing2D.LinearGradientMode.ForwardDiagonal;
            this.gridView1.Appearance.Row.Options.UseBackColor = true;
            this.gridView1.Appearance.RowSeparator.BackColor = System.Drawing.Color.LightSkyBlue;
            this.gridView1.Appearance.RowSeparator.Options.UseBackColor = true;
            this.gridView1.Appearance.VertLine.BackColor = System.Drawing.Color.Tan;
            this.gridView1.Appearance.VertLine.Options.UseBackColor = true;
            this.gridView1.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colCCT_BCO,
            this.colCCTAgencia,
            this.colCCTAgenciaDg,
            this.colCCTConta,
            this.colCCTContaDg,
            this.colCCTCNR,
            this.colCCTAtiva,
            this.colCCTPrincipal,
            this.colCCTLote,
            this.colCCTLoteEmail,
            this.colCCTAplicacao,
            this.colCCTTitulo,
            this.colCCTTipo,
            this.gridColumn1,
            this.colCCTPagamentoEletronico,
            this.colCCT_CCTPlus,
            this.colCCTCaixaPostal,
            this.colCCTDebitoAutomatico,
            this.colCCTRateioCredito,
            this.colCCT1,
            this.colDescricaoContaCorrente});
            this.gridView1.GridControl = this.gridControl1;
            this.gridView1.Name = "gridView1";
            this.gridView1.OptionsCustomization.AllowFilter = false;
            this.gridView1.OptionsView.ColumnAutoWidth = false;
            this.gridView1.OptionsView.NewItemRowPosition = DevExpress.XtraGrid.Views.Grid.NewItemRowPosition.Bottom;
            this.gridView1.OptionsView.ShowGroupPanel = false;
            this.gridView1.RowCellStyle += new DevExpress.XtraGrid.Views.Grid.RowCellStyleEventHandler(this.gridView1_RowCellStyle);
            this.gridView1.CustomRowCellEdit += new DevExpress.XtraGrid.Views.Grid.CustomRowCellEditEventHandler(this.gridView1_CustomRowCellEdit);
            this.gridView1.InitNewRow += new DevExpress.XtraGrid.Views.Grid.InitNewRowEventHandler(this.gridView1_InitNewRow);
            this.gridView1.ValidateRow += new DevExpress.XtraGrid.Views.Base.ValidateRowEventHandler(this.gridView1_ValidateRow);
            this.gridView1.RowUpdated += new DevExpress.XtraGrid.Views.Base.RowObjectEventHandler(this.gridView1_RowUpdated);
            // 
            // colCCT_BCO
            // 
            this.colCCT_BCO.Caption = "Banco";
            this.colCCT_BCO.ColumnEdit = this.LookUpEditBANCOS;
            this.colCCT_BCO.FieldName = "CCT_BCO";
            this.colCCT_BCO.Name = "colCCT_BCO";
            this.colCCT_BCO.Visible = true;
            this.colCCT_BCO.VisibleIndex = 0;
            this.colCCT_BCO.Width = 82;
            // 
            // LookUpEditBANCOS
            // 
            this.LookUpEditBANCOS.AutoHeight = false;
            this.LookUpEditBANCOS.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LookUpEditBANCOS.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("BCO", "C�digo", 40, DevExpress.Utils.FormatType.Numeric, "", true, DevExpress.Utils.HorzAlignment.Far),
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("BCONome", "Nome", 54, DevExpress.Utils.FormatType.None, "", true, DevExpress.Utils.HorzAlignment.Near)});
            this.LookUpEditBANCOS.DataSource = this.BANCOSbindingSource;
            this.LookUpEditBANCOS.DisplayMember = "BCONome";
            this.LookUpEditBANCOS.Name = "LookUpEditBANCOS";
            this.LookUpEditBANCOS.NullText = "";
            this.LookUpEditBANCOS.ValueMember = "BCO";
            // 
            // BANCOSbindingSource
            // 
            this.BANCOSbindingSource.DataMember = "BANCOS";
            this.BANCOSbindingSource.DataSource = typeof(Framework.datasets.dBANCOS);
            // 
            // colCCTAgencia
            // 
            this.colCCTAgencia.Caption = "Ag�ncia";
            this.colCCTAgencia.FieldName = "CCTAgencia";
            this.colCCTAgencia.Name = "colCCTAgencia";
            this.colCCTAgencia.Visible = true;
            this.colCCTAgencia.VisibleIndex = 1;
            this.colCCTAgencia.Width = 62;
            // 
            // colCCTAgenciaDg
            // 
            this.colCCTAgenciaDg.FieldName = "CCTAgenciaDg";
            this.colCCTAgenciaDg.Name = "colCCTAgenciaDg";
            this.colCCTAgenciaDg.OptionsColumn.ShowCaption = false;
            this.colCCTAgenciaDg.Visible = true;
            this.colCCTAgenciaDg.VisibleIndex = 2;
            this.colCCTAgenciaDg.Width = 22;
            // 
            // colCCTConta
            // 
            this.colCCTConta.Caption = "Conta";
            this.colCCTConta.FieldName = "CCTConta";
            this.colCCTConta.Name = "colCCTConta";
            this.colCCTConta.Visible = true;
            this.colCCTConta.VisibleIndex = 3;
            this.colCCTConta.Width = 81;
            // 
            // colCCTContaDg
            // 
            this.colCCTContaDg.FieldName = "CCTContaDg";
            this.colCCTContaDg.Name = "colCCTContaDg";
            this.colCCTContaDg.OptionsColumn.ShowCaption = false;
            this.colCCTContaDg.Visible = true;
            this.colCCTContaDg.VisibleIndex = 4;
            this.colCCTContaDg.Width = 27;
            // 
            // colCCTCNR
            // 
            this.colCCTCNR.Caption = "CNR";
            this.colCCTCNR.FieldName = "CCTCNR";
            this.colCCTCNR.Name = "colCCTCNR";
            this.colCCTCNR.Visible = true;
            this.colCCTCNR.VisibleIndex = 5;
            this.colCCTCNR.Width = 62;
            // 
            // colCCTAtiva
            // 
            this.colCCTAtiva.Caption = "Ativa";
            this.colCCTAtiva.FieldName = "CCTAtiva";
            this.colCCTAtiva.Name = "colCCTAtiva";
            this.colCCTAtiva.Visible = true;
            this.colCCTAtiva.VisibleIndex = 8;
            this.colCCTAtiva.Width = 47;
            // 
            // colCCTPrincipal
            // 
            this.colCCTPrincipal.Caption = "Principal";
            this.colCCTPrincipal.FieldName = "CCTPrincipal";
            this.colCCTPrincipal.Name = "colCCTPrincipal";
            this.colCCTPrincipal.Visible = true;
            this.colCCTPrincipal.VisibleIndex = 9;
            this.colCCTPrincipal.Width = 59;
            // 
            // colCCTLote
            // 
            this.colCCTLote.Caption = "Lote";
            this.colCCTLote.FieldName = "CCTLote";
            this.colCCTLote.Name = "colCCTLote";
            this.colCCTLote.Visible = true;
            this.colCCTLote.VisibleIndex = 10;
            this.colCCTLote.Width = 41;
            // 
            // colCCTLoteEmail
            // 
            this.colCCTLoteEmail.Caption = "Lote Email";
            this.colCCTLoteEmail.FieldName = "CCTLoteEmail";
            this.colCCTLoteEmail.Name = "colCCTLoteEmail";
            this.colCCTLoteEmail.Visible = true;
            this.colCCTLoteEmail.VisibleIndex = 12;
            this.colCCTLoteEmail.Width = 166;
            // 
            // colCCTAplicacao
            // 
            this.colCCTAplicacao.Caption = "Aplica��o";
            this.colCCTAplicacao.FieldName = "CCTAplicacao";
            this.colCCTAplicacao.Name = "colCCTAplicacao";
            this.colCCTAplicacao.Width = 69;
            // 
            // colCCTTitulo
            // 
            this.colCCTTitulo.Caption = "T�tulo";
            this.colCCTTitulo.FieldName = "CCTTitulo";
            this.colCCTTitulo.Name = "colCCTTitulo";
            this.colCCTTitulo.Visible = true;
            this.colCCTTitulo.VisibleIndex = 11;
            this.colCCTTitulo.Width = 169;
            // 
            // colCCTTipo
            // 
            this.colCCTTipo.Caption = "Tipo";
            this.colCCTTipo.ColumnEdit = this.repositoryItemImageComboBox2;
            this.colCCTTipo.FieldName = "CCTTipo";
            this.colCCTTipo.Name = "colCCTTipo";
            this.colCCTTipo.Visible = true;
            this.colCCTTipo.VisibleIndex = 7;
            this.colCCTTipo.Width = 117;
            // 
            // repositoryItemImageComboBox2
            // 
            this.repositoryItemImageComboBox2.AutoHeight = false;
            this.repositoryItemImageComboBox2.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemImageComboBox2.Name = "repositoryItemImageComboBox2";
            // 
            // gridColumn1
            // 
            this.gridColumn1.Caption = "gridColumn1";
            this.gridColumn1.ColumnEdit = this.repositoryItemButtonEdit1;
            this.gridColumn1.Fixed = DevExpress.XtraGrid.Columns.FixedStyle.Right;
            this.gridColumn1.Name = "gridColumn1";
            this.gridColumn1.OptionsColumn.ShowCaption = false;
            this.gridColumn1.Visible = true;
            this.gridColumn1.VisibleIndex = 17;
            this.gridColumn1.Width = 25;
            // 
            // repositoryItemButtonEdit1
            // 
            this.repositoryItemButtonEdit1.AutoHeight = false;
            this.repositoryItemButtonEdit1.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Close)});
            this.repositoryItemButtonEdit1.Name = "repositoryItemButtonEdit1";
            this.repositoryItemButtonEdit1.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.HideTextEditor;
            this.repositoryItemButtonEdit1.ButtonClick += new DevExpress.XtraEditors.Controls.ButtonPressedEventHandler(this.repositoryItemButtonEdit1_ButtonClick);
            // 
            // colCCTPagamentoEletronico
            // 
            this.colCCTPagamentoEletronico.Caption = "Pag. Eletr�nico";
            this.colCCTPagamentoEletronico.FieldName = "CCTPagamentoEletronico";
            this.colCCTPagamentoEletronico.Name = "colCCTPagamentoEletronico";
            this.colCCTPagamentoEletronico.Visible = true;
            this.colCCTPagamentoEletronico.VisibleIndex = 13;
            this.colCCTPagamentoEletronico.Width = 84;
            // 
            // colCCT_CCTPlus
            // 
            this.colCCT_CCTPlus.Caption = "Conta Aplica��o Autom.";
            this.colCCT_CCTPlus.ColumnEdit = this.repositoryMorto;
            this.colCCT_CCTPlus.FieldName = "CCT_CCTPlus";
            this.colCCT_CCTPlus.Name = "colCCT_CCTPlus";
            this.colCCT_CCTPlus.Visible = true;
            this.colCCT_CCTPlus.VisibleIndex = 16;
            this.colCCT_CCTPlus.Width = 134;
            // 
            // repositoryMorto
            // 
            this.repositoryMorto.AutoHeight = false;
            this.repositoryMorto.Name = "repositoryMorto";
            this.repositoryMorto.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.HideTextEditor;
            // 
            // colCCTCaixaPostal
            // 
            this.colCCTCaixaPostal.Caption = "Caixa Postal";
            this.colCCTCaixaPostal.FieldName = "CCTCaixaPostal";
            this.colCCTCaixaPostal.Name = "colCCTCaixaPostal";
            this.colCCTCaixaPostal.Visible = true;
            this.colCCTCaixaPostal.VisibleIndex = 6;
            // 
            // colCCTDebitoAutomatico
            // 
            this.colCCTDebitoAutomatico.Caption = "Deb. Autom�tico";
            this.colCCTDebitoAutomatico.FieldName = "CCTDebitoAutomatico";
            this.colCCTDebitoAutomatico.Name = "colCCTDebitoAutomatico";
            this.colCCTDebitoAutomatico.Visible = true;
            this.colCCTDebitoAutomatico.VisibleIndex = 14;
            // 
            // colCCTRateioCredito
            // 
            this.colCCTRateioCredito.Caption = "Rateio de Cr�dito";
            this.colCCTRateioCredito.FieldName = "CCTRateioCredito";
            this.colCCTRateioCredito.Name = "colCCTRateioCredito";
            this.colCCTRateioCredito.Visible = true;
            this.colCCTRateioCredito.VisibleIndex = 15;
            // 
            // colCCT1
            // 
            this.colCCT1.FieldName = "CCT";
            this.colCCT1.Name = "colCCT1";
            // 
            // colDescricaoContaCorrente
            // 
            this.colDescricaoContaCorrente.FieldName = "DescricaoContaCorrente";
            this.colDescricaoContaCorrente.Name = "colDescricaoContaCorrente";
            // 
            // ButtonEditAg
            // 
            this.ButtonEditAg.AutoHeight = false;
            this.ButtonEditAg.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.ButtonEditAg.Name = "ButtonEditAg";
            this.ButtonEditAg.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.HideTextEditor;
            // 
            // LookUpEditPlus
            // 
            this.LookUpEditPlus.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo),
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Delete)});
            this.LookUpEditPlus.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("CCTTitulo", "CCT Titulo", 59, DevExpress.Utils.FormatType.None, "", true, DevExpress.Utils.HorzAlignment.Near)});
            this.LookUpEditPlus.DataSource = this.contaCorrenTeBindingSource;
            this.LookUpEditPlus.DisplayMember = "CCTTitulo";
            this.LookUpEditPlus.Name = "LookUpEditPlus";
            this.LookUpEditPlus.NullText = "--";
            this.LookUpEditPlus.ValueMember = "CCT";
            this.LookUpEditPlus.ButtonClick += new DevExpress.XtraEditors.Controls.ButtonPressedEventHandler(this.LookUpEditPlus_ButtonClick);
            // 
            // cONDOMINIOSBindingSource
            // 
            this.cONDOMINIOSBindingSource.DataMember = "CONDOMINIOS";
            this.cONDOMINIOSBindingSource.DataSource = typeof(CadastrosProc.Condominios.dCondominiosCampos);
            // 
            // groupControl1
            // 
            this.groupControl1.Controls.Add(this.gridControl1);
            this.groupControl1.Dock = System.Windows.Forms.DockStyle.Top;
            this.groupControl1.Location = new System.Drawing.Point(0, 0);
            this.groupControl1.Name = "groupControl1";
            this.groupControl1.Size = new System.Drawing.Size(1478, 196);
            this.groupControl1.TabIndex = 1;
            this.groupControl1.Text = ":: Contas F�sicas";
            // 
            // splitterControl1
            // 
            this.splitterControl1.Dock = System.Windows.Forms.DockStyle.Top;
            this.splitterControl1.Location = new System.Drawing.Point(0, 196);
            this.splitterControl1.Name = "splitterControl1";
            this.splitterControl1.Size = new System.Drawing.Size(1478, 5);
            this.splitterControl1.TabIndex = 2;
            this.splitterControl1.TabStop = false;
            // 
            // groupControl2
            // 
            this.groupControl2.Controls.Add(this.gridControl2);
            this.groupControl2.Controls.Add(this.panelControl1);
            this.groupControl2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.groupControl2.Location = new System.Drawing.Point(0, 201);
            this.groupControl2.Name = "groupControl2";
            this.groupControl2.Size = new System.Drawing.Size(1478, 620);
            this.groupControl2.TabIndex = 3;
            this.groupControl2.Text = ":: Contas L�gicas";
            // 
            // panelControl1
            // 
            this.panelControl1.Controls.Add(this.simpleButton1);
            this.panelControl1.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panelControl1.Location = new System.Drawing.Point(2, 561);
            this.panelControl1.Name = "panelControl1";
            this.panelControl1.Size = new System.Drawing.Size(1474, 57);
            this.panelControl1.TabIndex = 1;
            this.panelControl1.Visible = false;
            // 
            // simpleButton1
            // 
            this.simpleButton1.Location = new System.Drawing.Point(5, 6);
            this.simpleButton1.Name = "simpleButton1";
            this.simpleButton1.Size = new System.Drawing.Size(281, 46);
            this.simpleButton1.TabIndex = 0;
            this.simpleButton1.Text = "Ativar Controle de contas l�gicas";
            this.simpleButton1.Click += new System.EventHandler(this.simpleButton1_Click);
            // 
            // TabContas
            // 
            this.Controls.Add(this.groupControl2);
            this.Controls.Add(this.splitterControl1);
            this.Controls.Add(this.groupControl1);
            this.Doc = System.Windows.Forms.DockStyle.Fill;
            this.LookAndFeel.UseDefaultLookAndFeel = false;
            this.Name = "TabContas";
            this.Size = new System.Drawing.Size(1478, 821);
            ((System.ComponentModel.ISupportInitialize)(this.StyleController_F)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemImageComboBox3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LookUpCCT)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.contaCorrenTeBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.contasbindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dConta1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemButtonEdit2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemImageComboBox1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LookUpEditBANCOS)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.BANCOSbindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemImageComboBox2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemButtonEdit1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryMorto)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ButtonEditAg)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LookUpEditPlus)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cONDOMINIOSBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl1)).EndInit();
            this.groupControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.groupControl2)).EndInit();
            this.groupControl2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.panelControl1)).EndInit();
            this.panelControl1.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraGrid.GridControl gridControl1;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView1;
        private System.Windows.Forms.BindingSource contasbindingSource;
        private dConta dConta1;
        private DevExpress.XtraGrid.Columns.GridColumn colCCT_BCO;
        private DevExpress.XtraGrid.Columns.GridColumn colCCTAgencia;
        private DevExpress.XtraGrid.Columns.GridColumn colCCTAgenciaDg;
        private DevExpress.XtraGrid.Columns.GridColumn colCCTConta;
        private DevExpress.XtraGrid.Columns.GridColumn colCCTContaDg;
        private DevExpress.XtraGrid.Columns.GridColumn colCCTCNR;
        private DevExpress.XtraGrid.Columns.GridColumn colCCTAtiva;
        private DevExpress.XtraGrid.Columns.GridColumn colCCTPrincipal;
        private DevExpress.XtraGrid.Columns.GridColumn colCCTLote;
        private DevExpress.XtraGrid.Columns.GridColumn colCCTLoteEmail;
        private System.Windows.Forms.BindingSource cONDOMINIOSBindingSource;
        private System.Windows.Forms.BindingSource BANCOSbindingSource;
        private DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit LookUpEditBANCOS;
        private DevExpress.XtraGrid.Columns.GridColumn colCCTAplicacao;
        private DevExpress.XtraGrid.Columns.GridColumn colCCTTitulo;
        private DevExpress.XtraEditors.GroupControl groupControl1;
        private DevExpress.XtraEditors.SplitterControl splitterControl1;
        private DevExpress.XtraEditors.GroupControl groupControl2;
        private DevExpress.XtraGrid.GridControl gridControl2;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView2;
        private DevExpress.XtraGrid.Columns.GridColumn colCTL;
        private DevExpress.XtraGrid.Columns.GridColumn colCTLAtiva;
        private DevExpress.XtraGrid.Columns.GridColumn colCTLTitulo;
        private DevExpress.XtraGrid.Columns.GridColumn colCTLTipo;
        private DevExpress.XtraEditors.Repository.RepositoryItemImageComboBox repositoryItemImageComboBox1;
        private DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit LookUpCCT;
        private System.Windows.Forms.BindingSource contaCorrenTeBindingSource;
        private DevExpress.XtraGrid.Columns.GridColumn colCCTTipo;
        private DevExpress.XtraEditors.Repository.RepositoryItemImageComboBox repositoryItemImageComboBox2;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn1;
        private DevExpress.XtraEditors.Repository.RepositoryItemButtonEdit repositoryItemButtonEdit1;
        private DevExpress.XtraEditors.PanelControl panelControl1;
        private DevExpress.XtraEditors.SimpleButton simpleButton1;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn2;
        private DevExpress.XtraEditors.Repository.RepositoryItemButtonEdit repositoryItemButtonEdit2;
        private DevExpress.XtraGrid.Columns.GridColumn colCCTPagamentoEletronico;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView3;
        private DevExpress.XtraGrid.Columns.GridColumn colCTLCCTRentabilidade;
        private DevExpress.XtraGrid.Columns.GridColumn colCTLCCTTranArrecadado;
        private DevExpress.XtraEditors.Repository.RepositoryItemImageComboBox repositoryItemImageComboBox3;
        private DevExpress.XtraGrid.Columns.GridColumn colCCT;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn3;
        private DevExpress.XtraEditors.Repository.RepositoryItemButtonEdit ButtonEditAg;
        private DevExpress.XtraGrid.Columns.GridColumn colCCT_CCTPlus;
        private DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit LookUpEditPlus;
        private DevExpress.XtraEditors.Repository.RepositoryItemButtonEdit repositoryMorto;
        private DevExpress.XtraGrid.Columns.GridColumn colCCTCaixaPostal;
        private DevExpress.XtraGrid.Columns.GridColumn colCCTDebitoAutomatico;
        private DevExpress.XtraGrid.Columns.GridColumn colCCTRateioCredito;
        private DevExpress.XtraGrid.Columns.GridColumn colCCT1;
        private DevExpress.XtraGrid.Columns.GridColumn colDescricaoContaCorrente;
    }
}
