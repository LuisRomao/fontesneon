namespace Cadastros.Condominios.PaginasCondominio
{
    partial class TabBasicos
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.Windows.Forms.Label cONCepLabel;
            System.Windows.Forms.Label cONEnderecoLabel;
            System.Windows.Forms.Label cONBairroLabel;
            System.Windows.Forms.Label cON_CIDLabel;
            System.Windows.Forms.Label label1;
            System.Windows.Forms.Label label2;
            System.Windows.Forms.Label label3;
            System.Windows.Forms.Label label4;
            System.Windows.Forms.Label label5;
            System.Windows.Forms.Label label6;
            System.Windows.Forms.Label label7;
            System.Windows.Forms.Label label8;
            System.Windows.Forms.Label label10;
            System.Windows.Forms.Label label11;
            System.Windows.Forms.Label label12;
            System.Windows.Forms.Label label13;
            System.Windows.Forms.Label label9;
            System.Windows.Forms.Label label14;
            System.Windows.Forms.Label cON_LATLabel;
            System.Windows.Forms.Label cON_LNGLabel;
            this.groupControl1 = new DevExpress.XtraEditors.GroupControl();
            this.cON_LNGTextEdit = new DevExpress.XtraEditors.TextEdit();
            this.cONDOMINIOSBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.cmdSitePINMapa = new DevExpress.XtraEditors.SimpleButton();
            this.cON_LATTextEdit = new DevExpress.XtraEditors.TextEdit();
            this.textEdit13 = new DevExpress.XtraEditors.TextEdit();
            this.textEdit9 = new DevExpress.XtraEditors.TextEdit();
            this.cONCnpjTextEdit = new DevExpress.XtraEditors.TextEdit();
            this.simpleButton1 = new DevExpress.XtraEditors.SimpleButton();
            this.lookUpEdit1 = new DevExpress.XtraEditors.LookUpEdit();
            this.cIDADESBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.cON_CIDLookUpEdit = new DevExpress.XtraEditors.LookUpEdit();
            this.cONBairroTextEdit = new DevExpress.XtraEditors.TextEdit();
            this.cONEnderecoTextEdit = new DevExpress.XtraEditors.TextEdit();
            this.cONCepTextEdit = new DevExpress.XtraEditors.TextEdit();
            this.groupControl2 = new DevExpress.XtraEditors.GroupControl();
            this.cCompet1 = new Framework.objetosNeon.cCompet();
            this.checkEdit1 = new DevExpress.XtraEditors.CheckEdit();
            this.textEdit12 = new DevExpress.XtraEditors.TextEdit();
            this.textEdit11 = new DevExpress.XtraEditors.TextEdit();
            this.textEdit10 = new DevExpress.XtraEditors.TextEdit();
            this.groupControl5 = new DevExpress.XtraEditors.GroupControl();
            this.textEdit8 = new DevExpress.XtraEditors.TextEdit();
            this.textEdit7 = new DevExpress.XtraEditors.TextEdit();
            this.textEdit6 = new DevExpress.XtraEditors.TextEdit();
            this.textEdit5 = new DevExpress.XtraEditors.TextEdit();
            this.lookUpEdit2 = new DevExpress.XtraEditors.LookUpEdit();
            this.bANCOSBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.groupControl4 = new DevExpress.XtraEditors.GroupControl();
            this.textEdit4 = new DevExpress.XtraEditors.TextEdit();
            this.buscaSindicoBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.textEdit3 = new DevExpress.XtraEditors.TextEdit();
            this.textEdit2 = new DevExpress.XtraEditors.TextEdit();
            this.textEdit1 = new DevExpress.XtraEditors.TextEdit();
            this.groupControl3 = new DevExpress.XtraEditors.GroupControl();
            this.memoEdit1 = new DevExpress.XtraEditors.MemoEdit();
            cONCepLabel = new System.Windows.Forms.Label();
            cONEnderecoLabel = new System.Windows.Forms.Label();
            cONBairroLabel = new System.Windows.Forms.Label();
            cON_CIDLabel = new System.Windows.Forms.Label();
            label1 = new System.Windows.Forms.Label();
            label2 = new System.Windows.Forms.Label();
            label3 = new System.Windows.Forms.Label();
            label4 = new System.Windows.Forms.Label();
            label5 = new System.Windows.Forms.Label();
            label6 = new System.Windows.Forms.Label();
            label7 = new System.Windows.Forms.Label();
            label8 = new System.Windows.Forms.Label();
            label10 = new System.Windows.Forms.Label();
            label11 = new System.Windows.Forms.Label();
            label12 = new System.Windows.Forms.Label();
            label13 = new System.Windows.Forms.Label();
            label9 = new System.Windows.Forms.Label();
            label14 = new System.Windows.Forms.Label();
            cON_LATLabel = new System.Windows.Forms.Label();
            cON_LNGLabel = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.StyleController_F)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl1)).BeginInit();
            this.groupControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.cON_LNGTextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cONDOMINIOSBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cON_LATTextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit13.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit9.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cONCnpjTextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lookUpEdit1.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cIDADESBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cON_CIDLookUpEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cONBairroTextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cONEnderecoTextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cONCepTextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl2)).BeginInit();
            this.groupControl2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit1.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit12.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit11.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit10.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl5)).BeginInit();
            this.groupControl5.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit8.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit7.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit6.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit5.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lookUpEdit2.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.bANCOSBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl4)).BeginInit();
            this.groupControl4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit4.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.buscaSindicoBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit3.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit2.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit1.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl3)).BeginInit();
            this.groupControl3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.memoEdit1.Properties)).BeginInit();
            this.SuspendLayout();
            // 
            // ToolTipController_F
            // 
            this.ToolTipController_F.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.ToolTipController_F.Appearance.ForeColor = System.Drawing.Color.Black;
            this.ToolTipController_F.Appearance.Options.UseBackColor = true;
            this.ToolTipController_F.Appearance.Options.UseFont = true;
            this.ToolTipController_F.Appearance.Options.UseForeColor = true;
            this.ToolTipController_F.Appearance.Options.UseTextOptions = true;
            this.ToolTipController_F.Appearance.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            // 
            // StyleController_F
            // 
            this.StyleController_F.AppearanceDisabled.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this.StyleController_F.AppearanceDisabled.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this.StyleController_F.AppearanceDisabled.Options.UseBackColor = true;
            this.StyleController_F.AppearanceDisabled.Options.UseForeColor = true;
            this.StyleController_F.AppearanceFocused.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.StyleController_F.AppearanceFocused.Options.UseBackColor = true;
            // 
            // cONCepLabel
            // 
            cONCepLabel.AutoSize = true;
            cONCepLabel.Location = new System.Drawing.Point(8, 21);
            cONCepLabel.Name = "cONCepLabel";
            cONCepLabel.Size = new System.Drawing.Size(26, 13);
            cONCepLabel.TabIndex = 0;
            cONCepLabel.Text = "CEP";
            // 
            // cONEnderecoLabel
            // 
            cONEnderecoLabel.AutoSize = true;
            cONEnderecoLabel.Location = new System.Drawing.Point(249, 21);
            cONEnderecoLabel.Name = "cONEnderecoLabel";
            cONEnderecoLabel.Size = new System.Drawing.Size(52, 13);
            cONEnderecoLabel.TabIndex = 2;
            cONEnderecoLabel.Text = "Endere�o";
            // 
            // cONBairroLabel
            // 
            cONBairroLabel.AutoSize = true;
            cONBairroLabel.Location = new System.Drawing.Point(8, 44);
            cONBairroLabel.Name = "cONBairroLabel";
            cONBairroLabel.Size = new System.Drawing.Size(35, 13);
            cONBairroLabel.TabIndex = 4;
            cONBairroLabel.Text = "Bairro";
            // 
            // cON_CIDLabel
            // 
            cON_CIDLabel.AutoSize = true;
            cON_CIDLabel.Location = new System.Drawing.Point(249, 44);
            cON_CIDLabel.Name = "cON_CIDLabel";
            cON_CIDLabel.Size = new System.Drawing.Size(40, 13);
            cON_CIDLabel.TabIndex = 6;
            cON_CIDLabel.Text = "Cidade";
            // 
            // label1
            // 
            label1.AutoSize = true;
            label1.Location = new System.Drawing.Point(630, 44);
            label1.Name = "label1";
            label1.Size = new System.Drawing.Size(20, 13);
            label1.TabIndex = 8;
            label1.Text = "UF";
            // 
            // label2
            // 
            label2.AutoSize = true;
            label2.Location = new System.Drawing.Point(0, 22);
            label2.Name = "label2";
            label2.Size = new System.Drawing.Size(34, 13);
            label2.TabIndex = 0;
            label2.Text = "Nome";
            // 
            // label3
            // 
            label3.AutoSize = true;
            label3.Location = new System.Drawing.Point(360, 22);
            label3.Name = "label3";
            label3.Size = new System.Drawing.Size(49, 13);
            label3.TabIndex = 2;
            label3.Text = "Telefone";
            // 
            // label4
            // 
            label4.AutoSize = true;
            label4.Location = new System.Drawing.Point(496, 22);
            label4.Name = "label4";
            label4.Size = new System.Drawing.Size(32, 13);
            label4.TabIndex = 4;
            label4.Text = "Bloco";
            // 
            // label5
            // 
            label5.AutoSize = true;
            label5.Location = new System.Drawing.Point(640, 22);
            label5.Name = "label5";
            label5.Size = new System.Drawing.Size(30, 13);
            label5.TabIndex = 6;
            label5.Text = "Apto";
            // 
            // label6
            // 
            label6.AutoSize = true;
            label6.Location = new System.Drawing.Point(2, 22);
            label6.Name = "label6";
            label6.Size = new System.Drawing.Size(36, 13);
            label6.TabIndex = 0;
            label6.Text = "Banco";
            // 
            // label7
            // 
            label7.AutoSize = true;
            label7.Location = new System.Drawing.Point(379, 22);
            label7.Name = "label7";
            label7.Size = new System.Drawing.Size(45, 13);
            label7.TabIndex = 2;
            label7.Text = "Ag�ncia";
            // 
            // label8
            // 
            label8.AutoSize = true;
            label8.Location = new System.Drawing.Point(573, 22);
            label8.Name = "label8";
            label8.Size = new System.Drawing.Size(36, 13);
            label8.TabIndex = 5;
            label8.Text = "Conta";
            // 
            // label10
            // 
            label10.AutoSize = true;
            label10.Location = new System.Drawing.Point(10, 114);
            label10.Name = "label10";
            label10.Size = new System.Drawing.Size(84, 13);
            label10.TabIndex = 4;
            label10.Text = "Data de Ades�o";
            // 
            // label11
            // 
            label11.AutoSize = true;
            label11.Location = new System.Drawing.Point(420, 113);
            label11.Name = "label11";
            label11.Size = new System.Drawing.Size(37, 13);
            label11.TabIndex = 6;
            label11.Text = "Blocos";
            // 
            // label12
            // 
            label12.AutoSize = true;
            label12.Location = new System.Drawing.Point(504, 113);
            label12.Name = "label12";
            label12.Size = new System.Drawing.Size(75, 13);
            label12.TabIndex = 8;
            label12.Text = "Apartamentos";
            // 
            // label13
            // 
            label13.AutoSize = true;
            label13.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            label13.Location = new System.Drawing.Point(8, 72);
            label13.Name = "label13";
            label13.Size = new System.Drawing.Size(38, 16);
            label13.TabIndex = 40;
            label13.Text = "CNPJ";
            // 
            // label9
            // 
            label9.AutoSize = true;
            label9.Location = new System.Drawing.Point(630, 21);
            label9.Name = "label9";
            label9.Size = new System.Drawing.Size(14, 13);
            label9.TabIndex = 44;
            label9.Text = "N";
            // 
            // label14
            // 
            label14.AutoSize = true;
            label14.Location = new System.Drawing.Point(201, 114);
            label14.Name = "label14";
            label14.Size = new System.Drawing.Size(86, 13);
            label14.TabIndex = 11;
            label14.Text = "�ltimo Balancete";
            // 
            // cON_LATLabel
            // 
            cON_LATLabel.AutoSize = true;
            cON_LATLabel.Location = new System.Drawing.Point(785, 54);
            cON_LATLabel.Name = "cON_LATLabel";
            cON_LATLabel.Size = new System.Drawing.Size(46, 13);
            cON_LATLabel.TabIndex = 45;
            cON_LATLabel.Text = "Latitude";
            // 
            // cON_LNGLabel
            // 
            cON_LNGLabel.AutoSize = true;
            cON_LNGLabel.Location = new System.Drawing.Point(785, 77);
            cON_LNGLabel.Name = "cON_LNGLabel";
            cON_LNGLabel.Size = new System.Drawing.Size(54, 13);
            cON_LNGLabel.TabIndex = 48;
            cON_LNGLabel.Text = "Longitude";
            // 
            // groupControl1
            // 
            this.groupControl1.Controls.Add(cON_LNGLabel);
            this.groupControl1.Controls.Add(this.cON_LNGTextEdit);
            this.groupControl1.Controls.Add(this.cmdSitePINMapa);
            this.groupControl1.Controls.Add(cON_LATLabel);
            this.groupControl1.Controls.Add(this.cON_LATTextEdit);
            this.groupControl1.Controls.Add(label9);
            this.groupControl1.Controls.Add(this.textEdit13);
            this.groupControl1.Controls.Add(this.textEdit9);
            this.groupControl1.Controls.Add(this.cONCnpjTextEdit);
            this.groupControl1.Controls.Add(label13);
            this.groupControl1.Controls.Add(this.simpleButton1);
            this.groupControl1.Controls.Add(this.lookUpEdit1);
            this.groupControl1.Controls.Add(label1);
            this.groupControl1.Controls.Add(cON_CIDLabel);
            this.groupControl1.Controls.Add(this.cON_CIDLookUpEdit);
            this.groupControl1.Controls.Add(cONBairroLabel);
            this.groupControl1.Controls.Add(this.cONBairroTextEdit);
            this.groupControl1.Controls.Add(cONEnderecoLabel);
            this.groupControl1.Controls.Add(this.cONEnderecoTextEdit);
            this.groupControl1.Controls.Add(cONCepLabel);
            this.groupControl1.Controls.Add(this.cONCepTextEdit);
            this.groupControl1.Dock = System.Windows.Forms.DockStyle.Top;
            this.groupControl1.Location = new System.Drawing.Point(0, 0);
            this.groupControl1.Name = "groupControl1";
            this.groupControl1.Size = new System.Drawing.Size(0, 102);
            this.groupControl1.TabIndex = 0;
            this.groupControl1.Text = ":: Ender�o ::";
            // 
            // cON_LNGTextEdit
            // 
            this.cON_LNGTextEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.cONDOMINIOSBindingSource, "CON_LNG", true, System.Windows.Forms.DataSourceUpdateMode.OnValidation, "0", "N6"));
            this.cON_LNGTextEdit.Enabled = false;
            this.cON_LNGTextEdit.Location = new System.Drawing.Point(844, 77);
            this.cON_LNGTextEdit.Name = "cON_LNGTextEdit";
            this.cON_LNGTextEdit.Size = new System.Drawing.Size(97, 20);
            this.cON_LNGTextEdit.TabIndex = 49;
            // 
            // cONDOMINIOSBindingSource
            // 
            this.cONDOMINIOSBindingSource.DataMember = "CONDOMINIOS";
            this.cONDOMINIOSBindingSource.DataSource = typeof(CadastrosProc.Condominios.dCondominiosCampos);
            // 
            // cmdSitePINMapa
            // 
            this.cmdSitePINMapa.Location = new System.Drawing.Point(789, 25);
            this.cmdSitePINMapa.Name = "cmdSitePINMapa";
            this.cmdSitePINMapa.Size = new System.Drawing.Size(152, 22);
            this.cmdSitePINMapa.TabIndex = 47;
            this.cmdSitePINMapa.Text = "Site - Exibir PIN no Mapa";
            this.cmdSitePINMapa.Click += new System.EventHandler(this.cmdSitePINMapa_Click);
            // 
            // cON_LATTextEdit
            // 
            this.cON_LATTextEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.cONDOMINIOSBindingSource, "CON_LAT", true, System.Windows.Forms.DataSourceUpdateMode.OnValidation, "0", "N6"));
            this.cON_LATTextEdit.Enabled = false;
            this.cON_LATTextEdit.Location = new System.Drawing.Point(844, 52);
            this.cON_LATTextEdit.Name = "cON_LATTextEdit";
            this.cON_LATTextEdit.Size = new System.Drawing.Size(97, 20);
            this.cON_LATTextEdit.TabIndex = 46;
            // 
            // textEdit13
            // 
            this.textEdit13.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.cONDOMINIOSBindingSource, "CONNumero", true));
            this.textEdit13.Location = new System.Drawing.Point(653, 20);
            this.textEdit13.Name = "textEdit13";
            this.textEdit13.Properties.MaxLength = 10;
            this.textEdit13.Size = new System.Drawing.Size(83, 20);
            this.textEdit13.TabIndex = 43;
            // 
            // textEdit9
            // 
            this.textEdit9.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.cONDOMINIOSBindingSource, "CONEndereco2", true));
            this.textEdit9.Location = new System.Drawing.Point(304, 19);
            this.textEdit9.Name = "textEdit9";
            this.textEdit9.Properties.MaxLength = 40;
            this.textEdit9.Size = new System.Drawing.Size(317, 20);
            this.textEdit9.TabIndex = 42;
            // 
            // cONCnpjTextEdit
            // 
            this.cONCnpjTextEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.cONDOMINIOSBindingSource, "CONCnpj", true));
            this.cONCnpjTextEdit.Location = new System.Drawing.Point(46, 72);
            this.cONCnpjTextEdit.Name = "cONCnpjTextEdit";
            this.cONCnpjTextEdit.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.cONCnpjTextEdit.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cONCnpjTextEdit.Properties.Appearance.ForeColor = System.Drawing.Color.Blue;
            this.cONCnpjTextEdit.Properties.Appearance.Options.UseBackColor = true;
            this.cONCnpjTextEdit.Properties.Appearance.Options.UseFont = true;
            this.cONCnpjTextEdit.Properties.Appearance.Options.UseForeColor = true;
            this.cONCnpjTextEdit.Size = new System.Drawing.Size(194, 26);
            this.cONCnpjTextEdit.TabIndex = 41;
            this.cONCnpjTextEdit.Validating += new System.ComponentModel.CancelEventHandler(this.cONCnpjTextEdit_Validating);
            // 
            // simpleButton1
            // 
            this.simpleButton1.Location = new System.Drawing.Point(142, 20);
            this.simpleButton1.Name = "simpleButton1";
            this.simpleButton1.Size = new System.Drawing.Size(98, 22);
            this.simpleButton1.TabIndex = 39;
            this.simpleButton1.Text = "Buscar Endere�o";
            this.simpleButton1.Click += new System.EventHandler(this.simpleButton1_Click);
            // 
            // lookUpEdit1
            // 
            this.lookUpEdit1.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.cONDOMINIOSBindingSource, "CON_CID", true));
            this.lookUpEdit1.Enabled = false;
            this.lookUpEdit1.Location = new System.Drawing.Point(653, 45);
            this.lookUpEdit1.Name = "lookUpEdit1";
            this.lookUpEdit1.Properties.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("CIDNome", "Cidade"),
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("CIDUf", "UF")});
            this.lookUpEdit1.Properties.DataSource = this.cIDADESBindingSource;
            this.lookUpEdit1.Properties.DisplayMember = "CIDUf";
            this.lookUpEdit1.Properties.ValueMember = "CID";
            this.lookUpEdit1.Size = new System.Drawing.Size(83, 20);
            this.lookUpEdit1.TabIndex = 9;
            // 
            // cIDADESBindingSource
            // 
            this.cIDADESBindingSource.DataMember = "CIDADES";
            this.cIDADESBindingSource.DataSource = typeof(Framework.datasets.dCIDADES);
            // 
            // cON_CIDLookUpEdit
            // 
            this.cON_CIDLookUpEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.cONDOMINIOSBindingSource, "CON_CID", true));
            this.cON_CIDLookUpEdit.Location = new System.Drawing.Point(304, 45);
            this.cON_CIDLookUpEdit.Name = "cON_CIDLookUpEdit";
            this.cON_CIDLookUpEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.cON_CIDLookUpEdit.Properties.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("CIDNome", "Cidade"),
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("CIDUf", "UF")});
            this.cON_CIDLookUpEdit.Properties.DataSource = this.cIDADESBindingSource;
            this.cON_CIDLookUpEdit.Properties.DisplayMember = "CIDNome";
            this.cON_CIDLookUpEdit.Properties.ValueMember = "CID";
            this.cON_CIDLookUpEdit.Size = new System.Drawing.Size(317, 20);
            this.cON_CIDLookUpEdit.TabIndex = 7;
            this.cON_CIDLookUpEdit.EditValueChanged += new System.EventHandler(this.cON_CIDLookUpEdit_EditValueChanged);
            // 
            // cONBairroTextEdit
            // 
            this.cONBairroTextEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.cONDOMINIOSBindingSource, "CONBairro", true));
            this.cONBairroTextEdit.Location = new System.Drawing.Point(46, 45);
            this.cONBairroTextEdit.Name = "cONBairroTextEdit";
            this.cONBairroTextEdit.Size = new System.Drawing.Size(194, 20);
            this.cONBairroTextEdit.TabIndex = 5;
            // 
            // cONEnderecoTextEdit
            // 
            this.cONEnderecoTextEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.cONDOMINIOSBindingSource, "CONEndereco", true));
            this.cONEnderecoTextEdit.Location = new System.Drawing.Point(304, 70);
            this.cONEnderecoTextEdit.Name = "cONEnderecoTextEdit";
            this.cONEnderecoTextEdit.Properties.MaxLength = 40;
            this.cONEnderecoTextEdit.Size = new System.Drawing.Size(432, 20);
            this.cONEnderecoTextEdit.TabIndex = 3;
            // 
            // cONCepTextEdit
            // 
            this.cONCepTextEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.cONDOMINIOSBindingSource, "CONCep", true));
            this.cONCepTextEdit.Location = new System.Drawing.Point(46, 22);
            this.cONCepTextEdit.Name = "cONCepTextEdit";
            this.cONCepTextEdit.Size = new System.Drawing.Size(90, 20);
            this.cONCepTextEdit.TabIndex = 1;
            // 
            // groupControl2
            // 
            this.groupControl2.Controls.Add(this.cCompet1);
            this.groupControl2.Controls.Add(label14);
            this.groupControl2.Controls.Add(this.checkEdit1);
            this.groupControl2.Controls.Add(this.textEdit12);
            this.groupControl2.Controls.Add(label12);
            this.groupControl2.Controls.Add(this.textEdit11);
            this.groupControl2.Controls.Add(this.textEdit10);
            this.groupControl2.Controls.Add(label11);
            this.groupControl2.Controls.Add(label10);
            this.groupControl2.Controls.Add(this.groupControl5);
            this.groupControl2.Controls.Add(this.groupControl4);
            this.groupControl2.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.groupControl2.Enabled = false;
            this.groupControl2.Location = new System.Drawing.Point(0, -141);
            this.groupControl2.Name = "groupControl2";
            this.groupControl2.Size = new System.Drawing.Size(0, 141);
            this.groupControl2.TabIndex = 2;
            this.groupControl2.Text = ":: Dados de consulta ::";
            // 
            // cCompet1
            // 
            this.cCompet1.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.cCompet1.Appearance.Options.UseBackColor = true;
            this.cCompet1.CaixaAltaGeral = true;
            this.cCompet1.DataBindings.Add(new System.Windows.Forms.Binding("CompetenciaBind", this.cONDOMINIOSBindingSource, "CONCompFec", true, System.Windows.Forms.DataSourceUpdateMode.Never));
            this.cCompet1.Doc = System.Windows.Forms.DockStyle.None;
            this.cCompet1.Estado = CompontesBasicos.EstadosDosComponentes.Registrado;
            this.cCompet1.IsNull = false;
            this.cCompet1.Location = new System.Drawing.Point(294, 112);
            this.cCompet1.LookAndFeel.Style = DevExpress.LookAndFeel.LookAndFeelStyle.Office2003;
            this.cCompet1.LookAndFeel.UseDefaultLookAndFeel = false;
            this.cCompet1.Name = "cCompet1";
            this.cCompet1.PermiteNulo = true;
            this.cCompet1.ReadOnly = true;
            this.cCompet1.Size = new System.Drawing.Size(117, 24);
            this.cCompet1.somenteleitura = false;
            this.cCompet1.TabIndex = 12;
            this.cCompet1.Titulo = null;
            // 
            // checkEdit1
            // 
            this.checkEdit1.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.cONDOMINIOSBindingSource, "CONLeituraGas", true));
            this.checkEdit1.Location = new System.Drawing.Point(624, 115);
            this.checkEdit1.Name = "checkEdit1";
            this.checkEdit1.Properties.Caption = "Leitura de G�s";
            this.checkEdit1.Properties.ReadOnly = true;
            this.checkEdit1.Size = new System.Drawing.Size(98, 19);
            this.checkEdit1.TabIndex = 10;
            // 
            // textEdit12
            // 
            this.textEdit12.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.cONDOMINIOSBindingSource, "CONTotalApartamento", true));
            this.textEdit12.Location = new System.Drawing.Point(582, 114);
            this.textEdit12.Name = "textEdit12";
            this.textEdit12.Properties.ReadOnly = true;
            this.textEdit12.Size = new System.Drawing.Size(32, 20);
            this.textEdit12.TabIndex = 9;
            // 
            // textEdit11
            // 
            this.textEdit11.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.cONDOMINIOSBindingSource, "CONTotalBloco", true));
            this.textEdit11.Location = new System.Drawing.Point(460, 114);
            this.textEdit11.Name = "textEdit11";
            this.textEdit11.Properties.ReadOnly = true;
            this.textEdit11.Size = new System.Drawing.Size(35, 20);
            this.textEdit11.TabIndex = 7;
            // 
            // textEdit10
            // 
            this.textEdit10.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.cONDOMINIOSBindingSource, "CONDataInclusao", true));
            this.textEdit10.Location = new System.Drawing.Point(97, 114);
            this.textEdit10.Name = "textEdit10";
            this.textEdit10.Properties.DisplayFormat.FormatString = "dd/MM/yyyy";
            this.textEdit10.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Custom;
            this.textEdit10.Properties.ReadOnly = true;
            this.textEdit10.Size = new System.Drawing.Size(99, 20);
            this.textEdit10.TabIndex = 5;
            // 
            // groupControl5
            // 
            this.groupControl5.Controls.Add(this.textEdit8);
            this.groupControl5.Controls.Add(this.textEdit7);
            this.groupControl5.Controls.Add(this.textEdit6);
            this.groupControl5.Controls.Add(this.textEdit5);
            this.groupControl5.Controls.Add(label8);
            this.groupControl5.Controls.Add(label7);
            this.groupControl5.Controls.Add(this.lookUpEdit2);
            this.groupControl5.Controls.Add(label6);
            this.groupControl5.Dock = System.Windows.Forms.DockStyle.Top;
            this.groupControl5.Location = new System.Drawing.Point(1, 65);
            this.groupControl5.Name = "groupControl5";
            this.groupControl5.Size = new System.Drawing.Size(0, 47);
            this.groupControl5.TabIndex = 1;
            this.groupControl5.Text = ":: Dados Finaceiros ::";
            // 
            // textEdit8
            // 
            this.textEdit8.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.cONDOMINIOSBindingSource, "CONDigitoConta", true));
            this.textEdit8.Location = new System.Drawing.Point(699, 23);
            this.textEdit8.Name = "textEdit8";
            this.textEdit8.Properties.ReadOnly = true;
            this.textEdit8.Size = new System.Drawing.Size(33, 20);
            this.textEdit8.TabIndex = 7;
            // 
            // textEdit7
            // 
            this.textEdit7.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.cONDOMINIOSBindingSource, "CONConta", true));
            this.textEdit7.Location = new System.Drawing.Point(618, 23);
            this.textEdit7.Name = "textEdit7";
            this.textEdit7.Properties.ReadOnly = true;
            this.textEdit7.Size = new System.Drawing.Size(75, 20);
            this.textEdit7.TabIndex = 6;
            // 
            // textEdit6
            // 
            this.textEdit6.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.cONDOMINIOSBindingSource, "CONDigitoAgencia", true));
            this.textEdit6.Location = new System.Drawing.Point(514, 23);
            this.textEdit6.Name = "textEdit6";
            this.textEdit6.Properties.ReadOnly = true;
            this.textEdit6.Size = new System.Drawing.Size(33, 20);
            this.textEdit6.TabIndex = 4;
            // 
            // textEdit5
            // 
            this.textEdit5.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.cONDOMINIOSBindingSource, "CONAgencia", true));
            this.textEdit5.Location = new System.Drawing.Point(433, 23);
            this.textEdit5.Name = "textEdit5";
            this.textEdit5.Properties.ReadOnly = true;
            this.textEdit5.Size = new System.Drawing.Size(75, 20);
            this.textEdit5.TabIndex = 3;
            // 
            // lookUpEdit2
            // 
            this.lookUpEdit2.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.cONDOMINIOSBindingSource, "CON_BCO", true));
            this.lookUpEdit2.Location = new System.Drawing.Point(47, 23);
            this.lookUpEdit2.Name = "lookUpEdit2";
            this.lookUpEdit2.Properties.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("BCONome", "Nome")});
            this.lookUpEdit2.Properties.DataSource = this.bANCOSBindingSource;
            this.lookUpEdit2.Properties.DisplayMember = "BCONome";
            this.lookUpEdit2.Properties.ReadOnly = true;
            this.lookUpEdit2.Properties.ValueMember = "BCO";
            this.lookUpEdit2.Size = new System.Drawing.Size(310, 20);
            this.lookUpEdit2.TabIndex = 1;
            // 
            // bANCOSBindingSource
            // 
            this.bANCOSBindingSource.DataMember = "BANCOS";
            this.bANCOSBindingSource.DataSource = typeof(Framework.datasets.dBANCOS);
            // 
            // groupControl4
            // 
            this.groupControl4.Controls.Add(label5);
            this.groupControl4.Controls.Add(this.textEdit4);
            this.groupControl4.Controls.Add(this.textEdit3);
            this.groupControl4.Controls.Add(label4);
            this.groupControl4.Controls.Add(label3);
            this.groupControl4.Controls.Add(this.textEdit2);
            this.groupControl4.Controls.Add(label2);
            this.groupControl4.Controls.Add(this.textEdit1);
            this.groupControl4.Dock = System.Windows.Forms.DockStyle.Top;
            this.groupControl4.Location = new System.Drawing.Point(1, 20);
            this.groupControl4.Name = "groupControl4";
            this.groupControl4.Size = new System.Drawing.Size(0, 45);
            this.groupControl4.TabIndex = 0;
            this.groupControl4.Text = ":: S�ndico ::";
            // 
            // textEdit4
            // 
            this.textEdit4.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.buscaSindicoBindingSource, "DecBloco", true));
            this.textEdit4.Location = new System.Drawing.Point(537, 23);
            this.textEdit4.Name = "textEdit4";
            this.textEdit4.Properties.ReadOnly = true;
            this.textEdit4.Size = new System.Drawing.Size(100, 20);
            this.textEdit4.TabIndex = 5;
            // 
            // buscaSindicoBindingSource
            // 
            this.buscaSindicoBindingSource.DataMember = "BuscaSindico";
            this.buscaSindicoBindingSource.DataSource = typeof(CadastrosProc.Condominios.dCondominiosCampos);
            // 
            // textEdit3
            // 
            this.textEdit3.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.buscaSindicoBindingSource, "APTNumero", true));
            this.textEdit3.Location = new System.Drawing.Point(681, 23);
            this.textEdit3.Name = "textEdit3";
            this.textEdit3.Properties.ReadOnly = true;
            this.textEdit3.Size = new System.Drawing.Size(51, 20);
            this.textEdit3.TabIndex = 7;
            // 
            // textEdit2
            // 
            this.textEdit2.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.buscaSindicoBindingSource, "PESFone1", true));
            this.textEdit2.Location = new System.Drawing.Point(418, 23);
            this.textEdit2.Name = "textEdit2";
            this.textEdit2.Properties.ReadOnly = true;
            this.textEdit2.Size = new System.Drawing.Size(75, 20);
            this.textEdit2.TabIndex = 3;
            // 
            // textEdit1
            // 
            this.textEdit1.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.buscaSindicoBindingSource, "PESNome", true));
            this.textEdit1.Location = new System.Drawing.Point(44, 23);
            this.textEdit1.Name = "textEdit1";
            this.textEdit1.Properties.ReadOnly = true;
            this.textEdit1.Size = new System.Drawing.Size(313, 20);
            this.textEdit1.TabIndex = 1;
            // 
            // groupControl3
            // 
            this.groupControl3.Controls.Add(this.memoEdit1);
            this.groupControl3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.groupControl3.Location = new System.Drawing.Point(0, 102);
            this.groupControl3.Name = "groupControl3";
            this.groupControl3.Size = new System.Drawing.Size(0, 0);
            this.groupControl3.TabIndex = 1;
            this.groupControl3.Text = ":: Observa��o ::";
            // 
            // memoEdit1
            // 
            this.memoEdit1.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.cONDOMINIOSBindingSource, "CONObservacao", true));
            this.memoEdit1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.memoEdit1.Location = new System.Drawing.Point(0, 19);
            this.memoEdit1.Name = "memoEdit1";
            this.memoEdit1.Size = new System.Drawing.Size(0, 0);
            this.memoEdit1.TabIndex = 0;
            // 
            // TabBasicos
            // 
            this.Controls.Add(this.groupControl3);
            this.Controls.Add(this.groupControl2);
            this.Controls.Add(this.groupControl1);
            this.Doc = System.Windows.Forms.DockStyle.Fill;
            this.LookAndFeel.UseDefaultLookAndFeel = false;
            this.Name = "TabBasicos";
            this.Size = new System.Drawing.Size(0, 0);
            this.Load += new System.EventHandler(this.TabBasicos_Load);
            ((System.ComponentModel.ISupportInitialize)(this.StyleController_F)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl1)).EndInit();
            this.groupControl1.ResumeLayout(false);
            this.groupControl1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.cON_LNGTextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cONDOMINIOSBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cON_LATTextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit13.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit9.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cONCnpjTextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lookUpEdit1.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cIDADESBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cON_CIDLookUpEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cONBairroTextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cONEnderecoTextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cONCepTextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl2)).EndInit();
            this.groupControl2.ResumeLayout(false);
            this.groupControl2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit1.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit12.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit11.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit10.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl5)).EndInit();
            this.groupControl5.ResumeLayout(false);
            this.groupControl5.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit8.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit7.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit6.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit5.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lookUpEdit2.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.bANCOSBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl4)).EndInit();
            this.groupControl4.ResumeLayout(false);
            this.groupControl4.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit4.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.buscaSindicoBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit3.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit2.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit1.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl3)).EndInit();
            this.groupControl3.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.memoEdit1.Properties)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraEditors.GroupControl groupControl1;
        private DevExpress.XtraEditors.TextEdit cONCepTextEdit;
        private DevExpress.XtraEditors.LookUpEdit cON_CIDLookUpEdit;
        private DevExpress.XtraEditors.TextEdit cONBairroTextEdit;
        private DevExpress.XtraEditors.TextEdit cONEnderecoTextEdit;
        /// <summary>
        /// 
        /// </summary>
        public System.Windows.Forms.BindingSource cONDOMINIOSBindingSource;
        /// <summary>
        /// 
        /// </summary>
        public System.Windows.Forms.BindingSource cIDADESBindingSource;
        private DevExpress.XtraEditors.LookUpEdit lookUpEdit1;
        private DevExpress.XtraEditors.GroupControl groupControl2;
        private DevExpress.XtraEditors.GroupControl groupControl3;
        private DevExpress.XtraEditors.MemoEdit memoEdit1;
        private DevExpress.XtraEditors.GroupControl groupControl4;
        private DevExpress.XtraEditors.TextEdit textEdit1;
        private DevExpress.XtraEditors.TextEdit textEdit4;
        private DevExpress.XtraEditors.TextEdit textEdit3;
        private DevExpress.XtraEditors.TextEdit textEdit2;
        private DevExpress.XtraEditors.GroupControl groupControl5;
        private DevExpress.XtraEditors.LookUpEdit lookUpEdit2;
        private DevExpress.XtraEditors.TextEdit textEdit7;
        private DevExpress.XtraEditors.TextEdit textEdit6;
        private DevExpress.XtraEditors.TextEdit textEdit5;
        private DevExpress.XtraEditors.TextEdit textEdit8;
        private DevExpress.XtraEditors.TextEdit textEdit11;
        private DevExpress.XtraEditors.TextEdit textEdit10;
        private DevExpress.XtraEditors.TextEdit textEdit12;
        private DevExpress.XtraEditors.CheckEdit checkEdit1;
        private System.Windows.Forms.BindingSource bANCOSBindingSource;
        private System.Windows.Forms.BindingSource buscaSindicoBindingSource;
        private DevExpress.XtraEditors.SimpleButton simpleButton1;
        private DevExpress.XtraEditors.TextEdit cONCnpjTextEdit;
        private DevExpress.XtraEditors.TextEdit textEdit13;
        private DevExpress.XtraEditors.TextEdit textEdit9;
        private Framework.objetosNeon.cCompet cCompet1;
        private DevExpress.XtraEditors.TextEdit cON_LNGTextEdit;
        private DevExpress.XtraEditors.SimpleButton cmdSitePINMapa;
        private DevExpress.XtraEditors.TextEdit cON_LATTextEdit;
    }
}
