namespace Cadastros.Condominios.PaginasCondominio
{
    partial class TabConstituicao
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(TabConstituicao));
            DevExpress.XtraGrid.GridLevelNode gridLevelNode1 = new DevExpress.XtraGrid.GridLevelNode();
            DevExpress.XtraEditors.Controls.EditorButtonImageOptions editorButtonImageOptions1 = new DevExpress.XtraEditors.Controls.EditorButtonImageOptions();
            this.GridApartamentos = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colAPTNumero = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemTextEdit3 = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.colAPTFracaoIdeal = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemCalcEdit1 = new DevExpress.XtraEditors.Repository.RepositoryItemCalcEdit();
            this.colAPTProprietario_PES = new DevExpress.XtraGrid.Columns.GridColumn();
            this.LupPessoas = new DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit();
            this.pESSOASBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.colAPTInquilino_PES = new DevExpress.XtraGrid.Columns.GridColumn();
            this.LookInq = new DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit();
            this.colAPTImobiliaria_FRN = new DevExpress.XtraGrid.Columns.GridColumn();
            this.LookUpEditImobiliaria = new DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit();
            this.iMOBILIARIASBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.gridColumn2 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.BotoesApartamento = new DevExpress.XtraEditors.Repository.RepositoryItemButtonEdit();
            this.colAPTFormaPagtoProprietario_FPG = new DevExpress.XtraGrid.Columns.GridColumn();
            this.FormaPag = new DevExpress.XtraEditors.Repository.RepositoryItemImageComboBox();
            this.imageCollection1 = new DevExpress.Utils.ImageCollection(this.components);
            this.colAPTFormaPagtoInquilino_FPG = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colAPTRegistro = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colAPTTipo = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemImageComboBox1 = new DevExpress.XtraEditors.Repository.RepositoryItemImageComboBox();
            this.bLOCOSGridControl = new DevExpress.XtraGrid.GridControl();
            this.cONDOMINIOSBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.GridBlocos = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colBLOCodigo = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemTextEdit2 = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.colBLONome = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemTextEdit1 = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.colBLOTotalApartamento1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemSpinEdit1 = new DevExpress.XtraEditors.Repository.RepositoryItemSpinEdit();
            this.gridColumn1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemButtonEdit1 = new DevExpress.XtraEditors.Repository.RepositoryItemButtonEdit();
            this.repositoryItemCheckEdit1 = new DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit();
            this.bLOCOSBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.cONProcuracaoCheckEdit = new DevExpress.XtraEditors.CheckEdit();
            this.cONFracaoIdealCheckEdit = new DevExpress.XtraEditors.CheckEdit();
            this.groupControl8 = new DevExpress.XtraEditors.GroupControl();
            this.button1 = new System.Windows.Forms.Button();
            this.textEdit2 = new DevExpress.XtraEditors.TextEdit();
            this.labelControl3 = new DevExpress.XtraEditors.LabelControl();
            this.textEdit1 = new DevExpress.XtraEditors.TextEdit();
            this.labelControl2 = new DevExpress.XtraEditors.LabelControl();
            this.simpleButton2 = new DevExpress.XtraEditors.SimpleButton();
            this.TotFracao = new DevExpress.XtraEditors.TextEdit();
            this.labelControl1 = new DevExpress.XtraEditors.LabelControl();
            this.simpleButton1 = new DevExpress.XtraEditors.SimpleButton();
            this.groupControl7 = new DevExpress.XtraEditors.GroupControl();
            ((System.ComponentModel.ISupportInitialize)(this.StyleController_F)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.GridApartamentos)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEdit3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCalcEdit1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LupPessoas)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pESSOASBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LookInq)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LookUpEditImobiliaria)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.iMOBILIARIASBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.BotoesApartamento)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.FormaPag)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.imageCollection1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemImageComboBox1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.bLOCOSGridControl)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cONDOMINIOSBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.GridBlocos)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEdit2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEdit1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemSpinEdit1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemButtonEdit1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckEdit1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.bLOCOSBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cONProcuracaoCheckEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cONFracaoIdealCheckEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl8)).BeginInit();
            this.groupControl8.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit2.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit1.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TotFracao.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl7)).BeginInit();
            this.groupControl7.SuspendLayout();
            this.SuspendLayout();
            // 
            // ToolTipController_F
            // 
            this.ToolTipController_F.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.ToolTipController_F.Appearance.ForeColor = System.Drawing.Color.Black;
            this.ToolTipController_F.Appearance.Options.UseBackColor = true;
            this.ToolTipController_F.Appearance.Options.UseFont = true;
            this.ToolTipController_F.Appearance.Options.UseForeColor = true;
            this.ToolTipController_F.Appearance.Options.UseTextOptions = true;
            this.ToolTipController_F.Appearance.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            // 
            // StyleController_F
            // 
            this.StyleController_F.AppearanceDisabled.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this.StyleController_F.AppearanceDisabled.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this.StyleController_F.AppearanceDisabled.Options.UseBackColor = true;
            this.StyleController_F.AppearanceDisabled.Options.UseForeColor = true;
            this.StyleController_F.AppearanceFocused.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.StyleController_F.AppearanceFocused.Options.UseBackColor = true;
            // 
            // GridApartamentos
            // 
            this.GridApartamentos.Appearance.ColumnFilterButton.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(195)))), ((int)(((byte)(143)))), ((int)(((byte)(62)))));
            this.GridApartamentos.Appearance.ColumnFilterButton.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(195)))), ((int)(((byte)(143)))), ((int)(((byte)(62)))));
            this.GridApartamentos.Appearance.ColumnFilterButton.ForeColor = System.Drawing.Color.White;
            this.GridApartamentos.Appearance.ColumnFilterButton.Options.UseBackColor = true;
            this.GridApartamentos.Appearance.ColumnFilterButton.Options.UseBorderColor = true;
            this.GridApartamentos.Appearance.ColumnFilterButton.Options.UseForeColor = true;
            this.GridApartamentos.Appearance.ColumnFilterButtonActive.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(230)))), ((int)(((byte)(189)))), ((int)(((byte)(125)))));
            this.GridApartamentos.Appearance.ColumnFilterButtonActive.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(230)))), ((int)(((byte)(189)))), ((int)(((byte)(125)))));
            this.GridApartamentos.Appearance.ColumnFilterButtonActive.ForeColor = System.Drawing.Color.White;
            this.GridApartamentos.Appearance.ColumnFilterButtonActive.Options.UseBackColor = true;
            this.GridApartamentos.Appearance.ColumnFilterButtonActive.Options.UseBorderColor = true;
            this.GridApartamentos.Appearance.ColumnFilterButtonActive.Options.UseForeColor = true;
            this.GridApartamentos.Appearance.Empty.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(247)))), ((int)(((byte)(219)))));
            this.GridApartamentos.Appearance.Empty.BackColor2 = System.Drawing.Color.White;
            this.GridApartamentos.Appearance.Empty.Options.UseBackColor = true;
            this.GridApartamentos.Appearance.EvenRow.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(247)))), ((int)(((byte)(219)))));
            this.GridApartamentos.Appearance.EvenRow.ForeColor = System.Drawing.Color.Black;
            this.GridApartamentos.Appearance.EvenRow.Options.UseBackColor = true;
            this.GridApartamentos.Appearance.EvenRow.Options.UseForeColor = true;
            this.GridApartamentos.Appearance.FilterCloseButton.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(195)))), ((int)(((byte)(143)))), ((int)(((byte)(62)))));
            this.GridApartamentos.Appearance.FilterCloseButton.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(195)))), ((int)(((byte)(143)))), ((int)(((byte)(62)))));
            this.GridApartamentos.Appearance.FilterCloseButton.ForeColor = System.Drawing.Color.White;
            this.GridApartamentos.Appearance.FilterCloseButton.Options.UseBackColor = true;
            this.GridApartamentos.Appearance.FilterCloseButton.Options.UseBorderColor = true;
            this.GridApartamentos.Appearance.FilterCloseButton.Options.UseForeColor = true;
            this.GridApartamentos.Appearance.FilterPanel.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(212)))), ((int)(((byte)(166)))), ((int)(((byte)(93)))));
            this.GridApartamentos.Appearance.FilterPanel.BackColor2 = System.Drawing.Color.White;
            this.GridApartamentos.Appearance.FilterPanel.ForeColor = System.Drawing.Color.White;
            this.GridApartamentos.Appearance.FilterPanel.Options.UseBackColor = true;
            this.GridApartamentos.Appearance.FilterPanel.Options.UseForeColor = true;
            this.GridApartamentos.Appearance.FixedLine.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(129)))), ((int)(((byte)(120)))), ((int)(((byte)(88)))));
            this.GridApartamentos.Appearance.FixedLine.Options.UseBackColor = true;
            this.GridApartamentos.Appearance.FocusedCell.BackColor = System.Drawing.Color.White;
            this.GridApartamentos.Appearance.FocusedCell.ForeColor = System.Drawing.Color.Black;
            this.GridApartamentos.Appearance.FocusedCell.Options.UseBackColor = true;
            this.GridApartamentos.Appearance.FocusedCell.Options.UseForeColor = true;
            this.GridApartamentos.Appearance.FocusedRow.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(164)))), ((int)(((byte)(136)))), ((int)(((byte)(91)))));
            this.GridApartamentos.Appearance.FocusedRow.ForeColor = System.Drawing.Color.White;
            this.GridApartamentos.Appearance.FocusedRow.Options.UseBackColor = true;
            this.GridApartamentos.Appearance.FocusedRow.Options.UseForeColor = true;
            this.GridApartamentos.Appearance.FooterPanel.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(195)))), ((int)(((byte)(143)))), ((int)(((byte)(62)))));
            this.GridApartamentos.Appearance.FooterPanel.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(195)))), ((int)(((byte)(143)))), ((int)(((byte)(62)))));
            this.GridApartamentos.Appearance.FooterPanel.ForeColor = System.Drawing.Color.White;
            this.GridApartamentos.Appearance.FooterPanel.Options.UseBackColor = true;
            this.GridApartamentos.Appearance.FooterPanel.Options.UseBorderColor = true;
            this.GridApartamentos.Appearance.FooterPanel.Options.UseForeColor = true;
            this.GridApartamentos.Appearance.GroupButton.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(195)))), ((int)(((byte)(143)))), ((int)(((byte)(62)))));
            this.GridApartamentos.Appearance.GroupButton.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(195)))), ((int)(((byte)(143)))), ((int)(((byte)(62)))));
            this.GridApartamentos.Appearance.GroupButton.Options.UseBackColor = true;
            this.GridApartamentos.Appearance.GroupButton.Options.UseBorderColor = true;
            this.GridApartamentos.Appearance.GroupFooter.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(230)))), ((int)(((byte)(189)))), ((int)(((byte)(125)))));
            this.GridApartamentos.Appearance.GroupFooter.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(230)))), ((int)(((byte)(189)))), ((int)(((byte)(125)))));
            this.GridApartamentos.Appearance.GroupFooter.ForeColor = System.Drawing.Color.Black;
            this.GridApartamentos.Appearance.GroupFooter.Options.UseBackColor = true;
            this.GridApartamentos.Appearance.GroupFooter.Options.UseBorderColor = true;
            this.GridApartamentos.Appearance.GroupFooter.Options.UseForeColor = true;
            this.GridApartamentos.Appearance.GroupPanel.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(212)))), ((int)(((byte)(166)))), ((int)(((byte)(93)))));
            this.GridApartamentos.Appearance.GroupPanel.BackColor2 = System.Drawing.Color.White;
            this.GridApartamentos.Appearance.GroupPanel.ForeColor = System.Drawing.Color.White;
            this.GridApartamentos.Appearance.GroupPanel.Options.UseBackColor = true;
            this.GridApartamentos.Appearance.GroupPanel.Options.UseForeColor = true;
            this.GridApartamentos.Appearance.GroupRow.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(230)))), ((int)(((byte)(189)))), ((int)(((byte)(125)))));
            this.GridApartamentos.Appearance.GroupRow.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(230)))), ((int)(((byte)(189)))), ((int)(((byte)(125)))));
            this.GridApartamentos.Appearance.GroupRow.ForeColor = System.Drawing.Color.Black;
            this.GridApartamentos.Appearance.GroupRow.Options.UseBackColor = true;
            this.GridApartamentos.Appearance.GroupRow.Options.UseBorderColor = true;
            this.GridApartamentos.Appearance.GroupRow.Options.UseForeColor = true;
            this.GridApartamentos.Appearance.HeaderPanel.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(212)))), ((int)(((byte)(166)))), ((int)(((byte)(93)))));
            this.GridApartamentos.Appearance.HeaderPanel.BackColor2 = System.Drawing.Color.White;
            this.GridApartamentos.Appearance.HeaderPanel.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(212)))), ((int)(((byte)(166)))), ((int)(((byte)(93)))));
            this.GridApartamentos.Appearance.HeaderPanel.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
            this.GridApartamentos.Appearance.HeaderPanel.ForeColor = System.Drawing.Color.Black;
            this.GridApartamentos.Appearance.HeaderPanel.Options.UseBackColor = true;
            this.GridApartamentos.Appearance.HeaderPanel.Options.UseBorderColor = true;
            this.GridApartamentos.Appearance.HeaderPanel.Options.UseFont = true;
            this.GridApartamentos.Appearance.HeaderPanel.Options.UseForeColor = true;
            this.GridApartamentos.Appearance.HideSelectionRow.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(196)))), ((int)(((byte)(172)))), ((int)(((byte)(134)))));
            this.GridApartamentos.Appearance.HideSelectionRow.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(245)))), ((int)(((byte)(234)))), ((int)(((byte)(216)))));
            this.GridApartamentos.Appearance.HideSelectionRow.Options.UseBackColor = true;
            this.GridApartamentos.Appearance.HideSelectionRow.Options.UseForeColor = true;
            this.GridApartamentos.Appearance.HorzLine.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(209)))), ((int)(((byte)(169)))), ((int)(((byte)(107)))));
            this.GridApartamentos.Appearance.HorzLine.Options.UseBackColor = true;
            this.GridApartamentos.Appearance.OddRow.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(234)))), ((int)(((byte)(224)))), ((int)(((byte)(190)))));
            this.GridApartamentos.Appearance.OddRow.ForeColor = System.Drawing.Color.Black;
            this.GridApartamentos.Appearance.OddRow.Options.UseBackColor = true;
            this.GridApartamentos.Appearance.OddRow.Options.UseForeColor = true;
            this.GridApartamentos.Appearance.Preview.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(252)))), ((int)(((byte)(237)))));
            this.GridApartamentos.Appearance.Preview.Font = new System.Drawing.Font("Verdana", 7.5F);
            this.GridApartamentos.Appearance.Preview.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(129)))), ((int)(((byte)(120)))), ((int)(((byte)(88)))));
            this.GridApartamentos.Appearance.Preview.Options.UseBackColor = true;
            this.GridApartamentos.Appearance.Preview.Options.UseFont = true;
            this.GridApartamentos.Appearance.Preview.Options.UseForeColor = true;
            this.GridApartamentos.Appearance.Row.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(247)))), ((int)(((byte)(219)))));
            this.GridApartamentos.Appearance.Row.ForeColor = System.Drawing.Color.Black;
            this.GridApartamentos.Appearance.Row.Options.UseBackColor = true;
            this.GridApartamentos.Appearance.Row.Options.UseForeColor = true;
            this.GridApartamentos.Appearance.RowSeparator.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(247)))), ((int)(((byte)(219)))));
            this.GridApartamentos.Appearance.RowSeparator.BackColor2 = System.Drawing.Color.White;
            this.GridApartamentos.Appearance.RowSeparator.Options.UseBackColor = true;
            this.GridApartamentos.Appearance.SelectedRow.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(187)))), ((int)(((byte)(159)))), ((int)(((byte)(114)))));
            this.GridApartamentos.Appearance.SelectedRow.ForeColor = System.Drawing.Color.White;
            this.GridApartamentos.Appearance.SelectedRow.Options.UseBackColor = true;
            this.GridApartamentos.Appearance.SelectedRow.Options.UseForeColor = true;
            this.GridApartamentos.Appearance.TopNewRow.BackColor = System.Drawing.Color.White;
            this.GridApartamentos.Appearance.TopNewRow.Options.UseBackColor = true;
            this.GridApartamentos.Appearance.VertLine.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(209)))), ((int)(((byte)(169)))), ((int)(((byte)(107)))));
            this.GridApartamentos.Appearance.VertLine.Options.UseBackColor = true;
            this.GridApartamentos.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colAPTNumero,
            this.colAPTFracaoIdeal,
            this.colAPTProprietario_PES,
            this.colAPTInquilino_PES,
            this.colAPTImobiliaria_FRN,
            this.gridColumn2,
            this.colAPTFormaPagtoProprietario_FPG,
            this.colAPTFormaPagtoInquilino_FPG,
            this.colAPTRegistro,
            this.colAPTTipo});
            this.GridApartamentos.GridControl = this.bLOCOSGridControl;
            this.GridApartamentos.GroupSummary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridGroupSummaryItem(DevExpress.Data.SummaryItemType.Count, "", null, "")});
            this.GridApartamentos.Name = "GridApartamentos";
            this.GridApartamentos.NewItemRowText = "Clique aqui para incluir uma nova unidade";
            this.GridApartamentos.OptionsBehavior.EditorShowMode = DevExpress.Utils.EditorShowMode.MouseUp;
            this.GridApartamentos.OptionsDetail.EnableMasterViewMode = false;
            this.GridApartamentos.OptionsView.EnableAppearanceEvenRow = true;
            this.GridApartamentos.OptionsView.EnableAppearanceOddRow = true;
            this.GridApartamentos.OptionsView.NewItemRowPosition = DevExpress.XtraGrid.Views.Grid.NewItemRowPosition.Top;
            this.GridApartamentos.OptionsView.ShowGroupPanel = false;
            this.GridApartamentos.SortInfo.AddRange(new DevExpress.XtraGrid.Columns.GridColumnSortInfo[] {
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colAPTNumero, DevExpress.Data.ColumnSortOrder.Ascending)});
            this.GridApartamentos.RowCellStyle += new DevExpress.XtraGrid.Views.Grid.RowCellStyleEventHandler(this.GridApartamentos_RowCellStyle);
            this.GridApartamentos.InitNewRow += new DevExpress.XtraGrid.Views.Grid.InitNewRowEventHandler(this.GridApartamentos_InitNewRow);
            this.GridApartamentos.ValidateRow += new DevExpress.XtraGrid.Views.Base.ValidateRowEventHandler(this.ValidaLinhaAP);
            this.GridApartamentos.RowUpdated += new DevExpress.XtraGrid.Views.Base.RowObjectEventHandler(this.GridApartamentos_RowUpdated);
            this.GridApartamentos.ValidatingEditor += new DevExpress.XtraEditors.Controls.BaseContainerValidateEditorEventHandler(this.ValigaCamposAP);
            // 
            // colAPTNumero
            // 
            this.colAPTNumero.Caption = "N�";
            this.colAPTNumero.ColumnEdit = this.repositoryItemTextEdit3;
            this.colAPTNumero.FieldName = "APTNumero";
            this.colAPTNumero.Name = "colAPTNumero";
            this.colAPTNumero.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.True;
            this.colAPTNumero.OptionsColumn.FixedWidth = true;
            this.colAPTNumero.Visible = true;
            this.colAPTNumero.VisibleIndex = 0;
            this.colAPTNumero.Width = 54;
            // 
            // repositoryItemTextEdit3
            // 
            this.repositoryItemTextEdit3.AutoHeight = false;
            this.repositoryItemTextEdit3.MaxLength = 4;
            this.repositoryItemTextEdit3.Name = "repositoryItemTextEdit3";
            // 
            // colAPTFracaoIdeal
            // 
            this.colAPTFracaoIdeal.Caption = "Fra��o Ideal";
            this.colAPTFracaoIdeal.ColumnEdit = this.repositoryItemCalcEdit1;
            this.colAPTFracaoIdeal.FieldName = "APTFracaoIdeal";
            this.colAPTFracaoIdeal.Name = "colAPTFracaoIdeal";
            this.colAPTFracaoIdeal.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.True;
            this.colAPTFracaoIdeal.OptionsColumn.FixedWidth = true;
            this.colAPTFracaoIdeal.ShowButtonMode = DevExpress.XtraGrid.Views.Base.ShowButtonModeEnum.ShowOnlyInEditor;
            this.colAPTFracaoIdeal.Visible = true;
            this.colAPTFracaoIdeal.VisibleIndex = 2;
            this.colAPTFracaoIdeal.Width = 81;
            // 
            // repositoryItemCalcEdit1
            // 
            this.repositoryItemCalcEdit1.AutoHeight = false;
            this.repositoryItemCalcEdit1.Mask.EditMask = "n6";
            this.repositoryItemCalcEdit1.Mask.UseMaskAsDisplayFormat = true;
            this.repositoryItemCalcEdit1.Name = "repositoryItemCalcEdit1";
            // 
            // colAPTProprietario_PES
            // 
            this.colAPTProprietario_PES.Caption = "Propriet�rio";
            this.colAPTProprietario_PES.ColumnEdit = this.LupPessoas;
            this.colAPTProprietario_PES.FieldName = "APTProprietario_PES";
            this.colAPTProprietario_PES.Name = "colAPTProprietario_PES";
            this.colAPTProprietario_PES.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.True;
            this.colAPTProprietario_PES.ShowButtonMode = DevExpress.XtraGrid.Views.Base.ShowButtonModeEnum.ShowOnlyInEditor;
            this.colAPTProprietario_PES.Visible = true;
            this.colAPTProprietario_PES.VisibleIndex = 3;
            this.colAPTProprietario_PES.Width = 422;
            // 
            // LupPessoas
            // 
            this.LupPessoas.AutoHeight = false;
            this.LupPessoas.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo),
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Plus),
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.LupPessoas.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("PESNome", "Nome", 100, DevExpress.Utils.FormatType.None, "", true, DevExpress.Utils.HorzAlignment.Near, DevExpress.Data.ColumnSortOrder.Ascending)});
            this.LupPessoas.DataSource = this.pESSOASBindingSource;
            this.LupPessoas.DisplayMember = "PESNome";
            this.LupPessoas.Name = "LupPessoas";
            this.LupPessoas.NullText = "    --";
            this.LupPessoas.PopupFilterMode = DevExpress.XtraEditors.PopupFilterMode.Contains;
            this.LupPessoas.PopupWidth = 600;
            this.LupPessoas.ShowDropDown = DevExpress.XtraEditors.Controls.ShowDropDown.Never;
            this.LupPessoas.ShowHeader = false;
            this.LupPessoas.ValueMember = "PES";
            this.LupPessoas.ButtonClick += new DevExpress.XtraEditors.Controls.ButtonPressedEventHandler(this.LupPessoas_ButtonClick);
            // 
            // pESSOASBindingSource
            // 
            this.pESSOASBindingSource.DataMember = "PESloock";
            this.pESSOASBindingSource.DataSource = typeof(CadastrosProc.Condominios.dCondominiosCampos);
            // 
            // colAPTInquilino_PES
            // 
            this.colAPTInquilino_PES.Caption = "Inquilino";
            this.colAPTInquilino_PES.ColumnEdit = this.LookInq;
            this.colAPTInquilino_PES.FieldName = "APTInquilino_PES";
            this.colAPTInquilino_PES.Name = "colAPTInquilino_PES";
            this.colAPTInquilino_PES.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.True;
            this.colAPTInquilino_PES.ShowButtonMode = DevExpress.XtraGrid.Views.Base.ShowButtonModeEnum.ShowOnlyInEditor;
            this.colAPTInquilino_PES.Visible = true;
            this.colAPTInquilino_PES.VisibleIndex = 5;
            this.colAPTInquilino_PES.Width = 281;
            // 
            // LookInq
            // 
            this.LookInq.AutoHeight = false;
            this.LookInq.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo),
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Plus),
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Minus),
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.LookInq.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("PESNome", "Nome", 100, DevExpress.Utils.FormatType.None, "", true, DevExpress.Utils.HorzAlignment.Near, DevExpress.Data.ColumnSortOrder.Ascending)});
            this.LookInq.DataSource = this.pESSOASBindingSource;
            this.LookInq.DisplayMember = "PESNome";
            this.LookInq.Name = "LookInq";
            this.LookInq.NullText = "    --";
            this.LookInq.PopupFilterMode = DevExpress.XtraEditors.PopupFilterMode.Contains;
            this.LookInq.PopupWidth = 600;
            this.LookInq.ShowHeader = false;
            this.LookInq.ValueMember = "PES";
            this.LookInq.ButtonClick += new DevExpress.XtraEditors.Controls.ButtonPressedEventHandler(this.LupPessoas_ButtonClick);
            // 
            // colAPTImobiliaria_FRN
            // 
            this.colAPTImobiliaria_FRN.Caption = "Imobili�ria";
            this.colAPTImobiliaria_FRN.ColumnEdit = this.LookUpEditImobiliaria;
            this.colAPTImobiliaria_FRN.FieldName = "APTImobiliaria_FRN";
            this.colAPTImobiliaria_FRN.Name = "colAPTImobiliaria_FRN";
            this.colAPTImobiliaria_FRN.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.True;
            this.colAPTImobiliaria_FRN.OptionsColumn.FixedWidth = true;
            this.colAPTImobiliaria_FRN.ShowButtonMode = DevExpress.XtraGrid.Views.Base.ShowButtonModeEnum.ShowOnlyInEditor;
            this.colAPTImobiliaria_FRN.Visible = true;
            this.colAPTImobiliaria_FRN.VisibleIndex = 7;
            this.colAPTImobiliaria_FRN.Width = 189;
            // 
            // LookUpEditImobiliaria
            // 
            this.LookUpEditImobiliaria.AutoHeight = false;
            this.LookUpEditImobiliaria.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LookUpEditImobiliaria.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("FRNNome", "Nome", 100, DevExpress.Utils.FormatType.None, "", true, DevExpress.Utils.HorzAlignment.Near)});
            this.LookUpEditImobiliaria.DataSource = this.iMOBILIARIASBindingSource;
            this.LookUpEditImobiliaria.DisplayMember = "FRNNome";
            this.LookUpEditImobiliaria.Name = "LookUpEditImobiliaria";
            this.LookUpEditImobiliaria.NullText = "    --";
            this.LookUpEditImobiliaria.PopupFilterMode = DevExpress.XtraEditors.PopupFilterMode.Contains;
            this.LookUpEditImobiliaria.ShowFooter = false;
            this.LookUpEditImobiliaria.ShowHeader = false;
            this.LookUpEditImobiliaria.ValueMember = "FRN";
            this.LookUpEditImobiliaria.PopupFilter += new DevExpress.XtraEditors.Controls.PopupFilterEventHandler(this.LookUpEditImobiliaria_PopupFilter);
            // 
            // iMOBILIARIASBindingSource
            // 
            this.iMOBILIARIASBindingSource.DataMember = "FRNLookup";
            this.iMOBILIARIASBindingSource.DataSource = typeof(CadastrosProc.Fornecedores.dFornecedoresLookup);
            // 
            // gridColumn2
            // 
            this.gridColumn2.ColumnEdit = this.BotoesApartamento;
            this.gridColumn2.FieldName = "gridColumn2";
            this.gridColumn2.Name = "gridColumn2";
            this.gridColumn2.OptionsColumn.AllowMerge = DevExpress.Utils.DefaultBoolean.False;
            this.gridColumn2.OptionsColumn.AllowMove = false;
            this.gridColumn2.OptionsColumn.AllowSize = false;
            this.gridColumn2.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.False;
            this.gridColumn2.OptionsColumn.FixedWidth = true;
            this.gridColumn2.OptionsColumn.ShowCaption = false;
            this.gridColumn2.ShowButtonMode = DevExpress.XtraGrid.Views.Base.ShowButtonModeEnum.ShowForFocusedRow;
            this.gridColumn2.UnboundType = DevExpress.Data.UnboundColumnType.Object;
            this.gridColumn2.Visible = true;
            this.gridColumn2.VisibleIndex = 9;
            this.gridColumn2.Width = 49;
            // 
            // BotoesApartamento
            // 
            this.BotoesApartamento.AllowFocused = false;
            this.BotoesApartamento.AutoHeight = false;
            this.BotoesApartamento.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(),
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Delete)});
            this.BotoesApartamento.Name = "BotoesApartamento";
            this.BotoesApartamento.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor;
            this.BotoesApartamento.ButtonClick += new DevExpress.XtraEditors.Controls.ButtonPressedEventHandler(this.BotoesApartamento_ButtonClick);
            // 
            // colAPTFormaPagtoProprietario_FPG
            // 
            this.colAPTFormaPagtoProprietario_FPG.Caption = "F.P.";
            this.colAPTFormaPagtoProprietario_FPG.ColumnEdit = this.FormaPag;
            this.colAPTFormaPagtoProprietario_FPG.FieldName = "APTFormaPagtoProprietario_FPG";
            this.colAPTFormaPagtoProprietario_FPG.Name = "colAPTFormaPagtoProprietario_FPG";
            this.colAPTFormaPagtoProprietario_FPG.OptionsColumn.AllowIncrementalSearch = false;
            this.colAPTFormaPagtoProprietario_FPG.OptionsColumn.AllowMerge = DevExpress.Utils.DefaultBoolean.False;
            this.colAPTFormaPagtoProprietario_FPG.OptionsColumn.AllowMove = false;
            this.colAPTFormaPagtoProprietario_FPG.OptionsColumn.AllowSize = false;
            this.colAPTFormaPagtoProprietario_FPG.OptionsColumn.FixedWidth = true;
            this.colAPTFormaPagtoProprietario_FPG.Visible = true;
            this.colAPTFormaPagtoProprietario_FPG.VisibleIndex = 4;
            this.colAPTFormaPagtoProprietario_FPG.Width = 70;
            // 
            // FormaPag
            // 
            this.FormaPag.AllowGlyphSkinning = DevExpress.Utils.DefaultBoolean.True;
            this.FormaPag.AutoHeight = false;
            this.FormaPag.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.FormaPag.ContextImageOptions.AllowGlyphSkinning = DevExpress.Utils.DefaultBoolean.True;
            this.FormaPag.GlyphAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.FormaPag.Items.AddRange(new DevExpress.XtraEditors.Controls.ImageComboBoxItem[] {
            new DevExpress.XtraEditors.Controls.ImageComboBoxItem("Condom�nio", 4, 0),
            new DevExpress.XtraEditors.Controls.ImageComboBoxItem("Correio", 2, 1),
            new DevExpress.XtraEditors.Controls.ImageComboBoxItem("Condom�nio + e-mail", 14, 2),
            new DevExpress.XtraEditors.Controls.ImageComboBoxItem("Correio + e-mail", 12, 3),
            new DevExpress.XtraEditors.Controls.ImageComboBoxItem("e-mail + Condom�nio", 24, 4),
            new DevExpress.XtraEditors.Controls.ImageComboBoxItem("e-mail + Correio", 22, 5)});
            this.FormaPag.Name = "FormaPag";
            this.FormaPag.SmallImages = this.imageCollection1;
            this.FormaPag.ValidateOnEnterKey = true;
            this.FormaPag.EditValueChanged += new System.EventHandler(this.FormaPag_EditValueChanged);
            this.FormaPag.EditValueChanging += new DevExpress.XtraEditors.Controls.ChangingEventHandler(this.FormaPag_EditValueChanging);
            // 
            // imageCollection1
            // 
            this.imageCollection1.ImageSize = new System.Drawing.Size(32, 16);
            this.imageCollection1.ImageStream = ((DevExpress.Utils.ImageCollectionStreamer)(resources.GetObject("imageCollection1.ImageStream")));
            this.imageCollection1.TransparentColor = System.Drawing.Color.Black;
            this.imageCollection1.Images.SetKeyName(0, "FP4.bmp");
            this.imageCollection1.Images.SetKeyName(1, "FP2.bmp");
            this.imageCollection1.Images.SetKeyName(2, "FP14.bmp");
            this.imageCollection1.Images.SetKeyName(3, "FP12.bmp");
            this.imageCollection1.Images.SetKeyName(4, "FP24.bmp");
            this.imageCollection1.Images.SetKeyName(5, "FP22.bmp");
            // 
            // colAPTFormaPagtoInquilino_FPG
            // 
            this.colAPTFormaPagtoInquilino_FPG.Caption = "F.P.";
            this.colAPTFormaPagtoInquilino_FPG.ColumnEdit = this.FormaPag;
            this.colAPTFormaPagtoInquilino_FPG.FieldName = "APTFormaPagtoInquilino_FPG";
            this.colAPTFormaPagtoInquilino_FPG.Name = "colAPTFormaPagtoInquilino_FPG";
            this.colAPTFormaPagtoInquilino_FPG.OptionsColumn.AllowIncrementalSearch = false;
            this.colAPTFormaPagtoInquilino_FPG.OptionsColumn.AllowMerge = DevExpress.Utils.DefaultBoolean.False;
            this.colAPTFormaPagtoInquilino_FPG.OptionsColumn.AllowMove = false;
            this.colAPTFormaPagtoInquilino_FPG.OptionsColumn.AllowSize = false;
            this.colAPTFormaPagtoInquilino_FPG.OptionsColumn.FixedWidth = true;
            this.colAPTFormaPagtoInquilino_FPG.Visible = true;
            this.colAPTFormaPagtoInquilino_FPG.VisibleIndex = 6;
            this.colAPTFormaPagtoInquilino_FPG.Width = 70;
            // 
            // colAPTRegistro
            // 
            this.colAPTRegistro.Caption = "Registro";
            this.colAPTRegistro.FieldName = "APTRegistro";
            this.colAPTRegistro.Name = "colAPTRegistro";
            this.colAPTRegistro.Visible = true;
            this.colAPTRegistro.VisibleIndex = 8;
            this.colAPTRegistro.Width = 287;
            // 
            // colAPTTipo
            // 
            this.colAPTTipo.Caption = "Tipo";
            this.colAPTTipo.ColumnEdit = this.repositoryItemImageComboBox1;
            this.colAPTTipo.FieldName = "APTTipo";
            this.colAPTTipo.Name = "colAPTTipo";
            this.colAPTTipo.Visible = true;
            this.colAPTTipo.VisibleIndex = 1;
            this.colAPTTipo.Width = 119;
            // 
            // repositoryItemImageComboBox1
            // 
            this.repositoryItemImageComboBox1.AutoHeight = false;
            this.repositoryItemImageComboBox1.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemImageComboBox1.Name = "repositoryItemImageComboBox1";
            // 
            // bLOCOSGridControl
            // 
            this.bLOCOSGridControl.DataMember = "FK_BLOCOS_CONDOMINIOS";
            this.bLOCOSGridControl.DataSource = this.cONDOMINIOSBindingSource;
            this.bLOCOSGridControl.Dock = System.Windows.Forms.DockStyle.Fill;
            gridLevelNode1.LevelTemplate = this.GridApartamentos;
            gridLevelNode1.RelationName = "FK_APARTAMENTOS_BLOCOS";
            this.bLOCOSGridControl.LevelTree.Nodes.AddRange(new DevExpress.XtraGrid.GridLevelNode[] {
            gridLevelNode1});
            this.bLOCOSGridControl.Location = new System.Drawing.Point(2, 20);
            this.bLOCOSGridControl.MainView = this.GridBlocos;
            this.bLOCOSGridControl.Name = "bLOCOSGridControl";
            this.bLOCOSGridControl.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemCalcEdit1,
            this.LookUpEditImobiliaria,
            this.repositoryItemTextEdit1,
            this.repositoryItemButtonEdit1,
            this.LupPessoas,
            this.LookInq,
            this.repositoryItemTextEdit2,
            this.repositoryItemSpinEdit1,
            this.repositoryItemTextEdit3,
            this.BotoesApartamento,
            this.FormaPag,
            this.repositoryItemCheckEdit1,
            this.repositoryItemImageComboBox1});
            this.bLOCOSGridControl.Size = new System.Drawing.Size(1252, 696);
            this.bLOCOSGridControl.TabIndex = 111;
            this.bLOCOSGridControl.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.GridBlocos,
            this.GridApartamentos});
            // 
            // cONDOMINIOSBindingSource
            // 
            this.cONDOMINIOSBindingSource.DataMember = "CONDOMINIOS";
            this.cONDOMINIOSBindingSource.DataSource = typeof(CadastrosProc.Condominios.dCondominiosCampos);
            // 
            // GridBlocos
            // 
            this.GridBlocos.Appearance.ColumnFilterButton.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(104)))), ((int)(((byte)(184)))), ((int)(((byte)(251)))));
            this.GridBlocos.Appearance.ColumnFilterButton.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(104)))), ((int)(((byte)(184)))), ((int)(((byte)(251)))));
            this.GridBlocos.Appearance.ColumnFilterButton.ForeColor = System.Drawing.Color.White;
            this.GridBlocos.Appearance.ColumnFilterButton.Options.UseBackColor = true;
            this.GridBlocos.Appearance.ColumnFilterButton.Options.UseBorderColor = true;
            this.GridBlocos.Appearance.ColumnFilterButton.Options.UseForeColor = true;
            this.GridBlocos.Appearance.ColumnFilterButtonActive.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(170)))), ((int)(((byte)(216)))), ((int)(((byte)(254)))));
            this.GridBlocos.Appearance.ColumnFilterButtonActive.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(170)))), ((int)(((byte)(216)))), ((int)(((byte)(254)))));
            this.GridBlocos.Appearance.ColumnFilterButtonActive.ForeColor = System.Drawing.Color.Black;
            this.GridBlocos.Appearance.ColumnFilterButtonActive.Options.UseBackColor = true;
            this.GridBlocos.Appearance.ColumnFilterButtonActive.Options.UseBorderColor = true;
            this.GridBlocos.Appearance.ColumnFilterButtonActive.Options.UseForeColor = true;
            this.GridBlocos.Appearance.Empty.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(236)))), ((int)(((byte)(246)))), ((int)(((byte)(255)))));
            this.GridBlocos.Appearance.Empty.BackColor2 = System.Drawing.Color.White;
            this.GridBlocos.Appearance.Empty.Options.UseBackColor = true;
            this.GridBlocos.Appearance.EvenRow.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(247)))), ((int)(((byte)(251)))), ((int)(((byte)(255)))));
            this.GridBlocos.Appearance.EvenRow.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(247)))), ((int)(((byte)(251)))), ((int)(((byte)(255)))));
            this.GridBlocos.Appearance.EvenRow.ForeColor = System.Drawing.Color.Black;
            this.GridBlocos.Appearance.EvenRow.Options.UseBackColor = true;
            this.GridBlocos.Appearance.EvenRow.Options.UseBorderColor = true;
            this.GridBlocos.Appearance.EvenRow.Options.UseForeColor = true;
            this.GridBlocos.Appearance.FilterCloseButton.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(104)))), ((int)(((byte)(184)))), ((int)(((byte)(251)))));
            this.GridBlocos.Appearance.FilterCloseButton.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(104)))), ((int)(((byte)(184)))), ((int)(((byte)(251)))));
            this.GridBlocos.Appearance.FilterCloseButton.ForeColor = System.Drawing.Color.White;
            this.GridBlocos.Appearance.FilterCloseButton.Options.UseBackColor = true;
            this.GridBlocos.Appearance.FilterCloseButton.Options.UseBorderColor = true;
            this.GridBlocos.Appearance.FilterCloseButton.Options.UseForeColor = true;
            this.GridBlocos.Appearance.FilterPanel.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(236)))), ((int)(((byte)(246)))), ((int)(((byte)(255)))));
            this.GridBlocos.Appearance.FilterPanel.BackColor2 = System.Drawing.Color.White;
            this.GridBlocos.Appearance.FilterPanel.ForeColor = System.Drawing.Color.Black;
            this.GridBlocos.Appearance.FilterPanel.Options.UseBackColor = true;
            this.GridBlocos.Appearance.FilterPanel.Options.UseForeColor = true;
            this.GridBlocos.Appearance.FixedLine.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(59)))), ((int)(((byte)(133)))), ((int)(((byte)(195)))));
            this.GridBlocos.Appearance.FixedLine.Options.UseBackColor = true;
            this.GridBlocos.Appearance.FocusedCell.BackColor = System.Drawing.Color.White;
            this.GridBlocos.Appearance.FocusedCell.ForeColor = System.Drawing.Color.Black;
            this.GridBlocos.Appearance.FocusedCell.Options.UseBackColor = true;
            this.GridBlocos.Appearance.FocusedCell.Options.UseForeColor = true;
            this.GridBlocos.Appearance.FocusedRow.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(38)))), ((int)(((byte)(109)))), ((int)(((byte)(189)))));
            this.GridBlocos.Appearance.FocusedRow.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(59)))), ((int)(((byte)(139)))), ((int)(((byte)(206)))));
            this.GridBlocos.Appearance.FocusedRow.ForeColor = System.Drawing.Color.White;
            this.GridBlocos.Appearance.FocusedRow.Options.UseBackColor = true;
            this.GridBlocos.Appearance.FocusedRow.Options.UseBorderColor = true;
            this.GridBlocos.Appearance.FocusedRow.Options.UseForeColor = true;
            this.GridBlocos.Appearance.FooterPanel.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(104)))), ((int)(((byte)(184)))), ((int)(((byte)(251)))));
            this.GridBlocos.Appearance.FooterPanel.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(104)))), ((int)(((byte)(184)))), ((int)(((byte)(251)))));
            this.GridBlocos.Appearance.FooterPanel.ForeColor = System.Drawing.Color.Black;
            this.GridBlocos.Appearance.FooterPanel.Options.UseBackColor = true;
            this.GridBlocos.Appearance.FooterPanel.Options.UseBorderColor = true;
            this.GridBlocos.Appearance.FooterPanel.Options.UseForeColor = true;
            this.GridBlocos.Appearance.GroupButton.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(104)))), ((int)(((byte)(184)))), ((int)(((byte)(251)))));
            this.GridBlocos.Appearance.GroupButton.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(104)))), ((int)(((byte)(184)))), ((int)(((byte)(251)))));
            this.GridBlocos.Appearance.GroupButton.Options.UseBackColor = true;
            this.GridBlocos.Appearance.GroupButton.Options.UseBorderColor = true;
            this.GridBlocos.Appearance.GroupFooter.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(170)))), ((int)(((byte)(216)))), ((int)(((byte)(254)))));
            this.GridBlocos.Appearance.GroupFooter.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(170)))), ((int)(((byte)(216)))), ((int)(((byte)(254)))));
            this.GridBlocos.Appearance.GroupFooter.ForeColor = System.Drawing.Color.Black;
            this.GridBlocos.Appearance.GroupFooter.Options.UseBackColor = true;
            this.GridBlocos.Appearance.GroupFooter.Options.UseBorderColor = true;
            this.GridBlocos.Appearance.GroupFooter.Options.UseForeColor = true;
            this.GridBlocos.Appearance.GroupPanel.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(236)))), ((int)(((byte)(246)))), ((int)(((byte)(255)))));
            this.GridBlocos.Appearance.GroupPanel.BackColor2 = System.Drawing.Color.White;
            this.GridBlocos.Appearance.GroupPanel.ForeColor = System.Drawing.Color.Black;
            this.GridBlocos.Appearance.GroupPanel.Options.UseBackColor = true;
            this.GridBlocos.Appearance.GroupPanel.Options.UseForeColor = true;
            this.GridBlocos.Appearance.GroupRow.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(170)))), ((int)(((byte)(216)))), ((int)(((byte)(254)))));
            this.GridBlocos.Appearance.GroupRow.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(170)))), ((int)(((byte)(216)))), ((int)(((byte)(254)))));
            this.GridBlocos.Appearance.GroupRow.ForeColor = System.Drawing.Color.Black;
            this.GridBlocos.Appearance.GroupRow.Options.UseBackColor = true;
            this.GridBlocos.Appearance.GroupRow.Options.UseBorderColor = true;
            this.GridBlocos.Appearance.GroupRow.Options.UseForeColor = true;
            this.GridBlocos.Appearance.HeaderPanel.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(139)))), ((int)(((byte)(201)))), ((int)(((byte)(254)))));
            this.GridBlocos.Appearance.HeaderPanel.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(139)))), ((int)(((byte)(201)))), ((int)(((byte)(254)))));
            this.GridBlocos.Appearance.HeaderPanel.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
            this.GridBlocos.Appearance.HeaderPanel.ForeColor = System.Drawing.Color.Black;
            this.GridBlocos.Appearance.HeaderPanel.Options.UseBackColor = true;
            this.GridBlocos.Appearance.HeaderPanel.Options.UseBorderColor = true;
            this.GridBlocos.Appearance.HeaderPanel.Options.UseFont = true;
            this.GridBlocos.Appearance.HeaderPanel.Options.UseForeColor = true;
            this.GridBlocos.Appearance.HideSelectionRow.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(105)))), ((int)(((byte)(170)))), ((int)(((byte)(225)))));
            this.GridBlocos.Appearance.HideSelectionRow.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(83)))), ((int)(((byte)(155)))), ((int)(((byte)(215)))));
            this.GridBlocos.Appearance.HideSelectionRow.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(236)))), ((int)(((byte)(246)))), ((int)(((byte)(255)))));
            this.GridBlocos.Appearance.HideSelectionRow.Options.UseBackColor = true;
            this.GridBlocos.Appearance.HideSelectionRow.Options.UseBorderColor = true;
            this.GridBlocos.Appearance.HideSelectionRow.Options.UseForeColor = true;
            this.GridBlocos.Appearance.HorzLine.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(104)))), ((int)(((byte)(184)))), ((int)(((byte)(251)))));
            this.GridBlocos.Appearance.HorzLine.Options.UseBackColor = true;
            this.GridBlocos.Appearance.OddRow.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(236)))), ((int)(((byte)(246)))), ((int)(((byte)(255)))));
            this.GridBlocos.Appearance.OddRow.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(236)))), ((int)(((byte)(246)))), ((int)(((byte)(255)))));
            this.GridBlocos.Appearance.OddRow.ForeColor = System.Drawing.Color.Black;
            this.GridBlocos.Appearance.OddRow.Options.UseBackColor = true;
            this.GridBlocos.Appearance.OddRow.Options.UseBorderColor = true;
            this.GridBlocos.Appearance.OddRow.Options.UseForeColor = true;
            this.GridBlocos.Appearance.Preview.Font = new System.Drawing.Font("Verdana", 7.5F);
            this.GridBlocos.Appearance.Preview.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(83)))), ((int)(((byte)(155)))), ((int)(((byte)(215)))));
            this.GridBlocos.Appearance.Preview.Options.UseFont = true;
            this.GridBlocos.Appearance.Preview.Options.UseForeColor = true;
            this.GridBlocos.Appearance.Row.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(247)))), ((int)(((byte)(251)))), ((int)(((byte)(255)))));
            this.GridBlocos.Appearance.Row.ForeColor = System.Drawing.Color.Black;
            this.GridBlocos.Appearance.Row.Options.UseBackColor = true;
            this.GridBlocos.Appearance.Row.Options.UseForeColor = true;
            this.GridBlocos.Appearance.RowSeparator.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(236)))), ((int)(((byte)(246)))), ((int)(((byte)(255)))));
            this.GridBlocos.Appearance.RowSeparator.BackColor2 = System.Drawing.Color.White;
            this.GridBlocos.Appearance.RowSeparator.Options.UseBackColor = true;
            this.GridBlocos.Appearance.SelectedRow.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(83)))), ((int)(((byte)(155)))), ((int)(((byte)(215)))));
            this.GridBlocos.Appearance.SelectedRow.ForeColor = System.Drawing.Color.White;
            this.GridBlocos.Appearance.SelectedRow.Options.UseBackColor = true;
            this.GridBlocos.Appearance.SelectedRow.Options.UseForeColor = true;
            this.GridBlocos.Appearance.TopNewRow.BackColor = System.Drawing.Color.White;
            this.GridBlocos.Appearance.TopNewRow.Options.UseBackColor = true;
            this.GridBlocos.Appearance.VertLine.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(104)))), ((int)(((byte)(184)))), ((int)(((byte)(251)))));
            this.GridBlocos.Appearance.VertLine.Options.UseBackColor = true;
            this.GridBlocos.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colBLOCodigo,
            this.colBLONome,
            this.colBLOTotalApartamento1,
            this.gridColumn1});
            this.GridBlocos.GridControl = this.bLOCOSGridControl;
            this.GridBlocos.Name = "GridBlocos";
            this.GridBlocos.NewItemRowText = "Clique aqui para incluir um novo BLOCO";
            this.GridBlocos.OptionsBehavior.EditorShowMode = DevExpress.Utils.EditorShowMode.MouseUp;
            this.GridBlocos.OptionsDetail.AllowExpandEmptyDetails = true;
            this.GridBlocos.OptionsDetail.ShowDetailTabs = false;
            this.GridBlocos.OptionsDetail.SmartDetailExpandButtonMode = DevExpress.XtraGrid.Views.Grid.DetailExpandButtonMode.AlwaysEnabled;
            this.GridBlocos.OptionsMenu.EnableFooterMenu = false;
            this.GridBlocos.OptionsView.EnableAppearanceEvenRow = true;
            this.GridBlocos.OptionsView.EnableAppearanceOddRow = true;
            this.GridBlocos.OptionsView.NewItemRowPosition = DevExpress.XtraGrid.Views.Grid.NewItemRowPosition.Top;
            this.GridBlocos.OptionsView.ShowFooter = true;
            this.GridBlocos.OptionsView.ShowGroupPanel = false;
            this.GridBlocos.SortInfo.AddRange(new DevExpress.XtraGrid.Columns.GridColumnSortInfo[] {
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colBLOCodigo, DevExpress.Data.ColumnSortOrder.Ascending)});
            this.GridBlocos.ValidateRow += new DevExpress.XtraGrid.Views.Base.ValidateRowEventHandler(this.GridBlocos_ValidateRow);
            this.GridBlocos.RowUpdated += new DevExpress.XtraGrid.Views.Base.RowObjectEventHandler(this.GridBlocos_RowUpdated);
            this.GridBlocos.ValidatingEditor += new DevExpress.XtraEditors.Controls.BaseContainerValidateEditorEventHandler(this.GridBlocos_ValidatingEditor);
            // 
            // colBLOCodigo
            // 
            this.colBLOCodigo.Caption = "C�digo";
            this.colBLOCodigo.ColumnEdit = this.repositoryItemTextEdit2;
            this.colBLOCodigo.FieldName = "BLOCodigo";
            this.colBLOCodigo.Name = "colBLOCodigo";
            this.colBLOCodigo.OptionsColumn.FixedWidth = true;
            this.colBLOCodigo.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Count, "BLOCodigo", "Blocos: {0}")});
            this.colBLOCodigo.Visible = true;
            this.colBLOCodigo.VisibleIndex = 0;
            this.colBLOCodigo.Width = 103;
            // 
            // repositoryItemTextEdit2
            // 
            this.repositoryItemTextEdit2.AllowNullInput = DevExpress.Utils.DefaultBoolean.False;
            this.repositoryItemTextEdit2.MaxLength = 2;
            this.repositoryItemTextEdit2.Name = "repositoryItemTextEdit2";
            // 
            // colBLONome
            // 
            this.colBLONome.Caption = "Nome";
            this.colBLONome.ColumnEdit = this.repositoryItemTextEdit1;
            this.colBLONome.FieldName = "BLONome";
            this.colBLONome.Name = "colBLONome";
            this.colBLONome.Visible = true;
            this.colBLONome.VisibleIndex = 1;
            this.colBLONome.Width = 572;
            // 
            // repositoryItemTextEdit1
            // 
            this.repositoryItemTextEdit1.AllowNullInput = DevExpress.Utils.DefaultBoolean.False;
            this.repositoryItemTextEdit1.AutoHeight = false;
            this.repositoryItemTextEdit1.MaxLength = 60;
            this.repositoryItemTextEdit1.Name = "repositoryItemTextEdit1";
            // 
            // colBLOTotalApartamento1
            // 
            this.colBLOTotalApartamento1.Caption = "Apartamentos";
            this.colBLOTotalApartamento1.ColumnEdit = this.repositoryItemSpinEdit1;
            this.colBLOTotalApartamento1.FieldName = "BLOTotalApartamento";
            this.colBLOTotalApartamento1.Name = "colBLOTotalApartamento1";
            this.colBLOTotalApartamento1.OptionsColumn.FixedWidth = true;
            this.colBLOTotalApartamento1.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum, "BLOTotalApartamento", "APTs: {0}")});
            this.colBLOTotalApartamento1.Visible = true;
            this.colBLOTotalApartamento1.VisibleIndex = 2;
            this.colBLOTotalApartamento1.Width = 129;
            // 
            // repositoryItemSpinEdit1
            // 
            this.repositoryItemSpinEdit1.AllowNullInput = DevExpress.Utils.DefaultBoolean.False;
            this.repositoryItemSpinEdit1.AutoHeight = false;
            this.repositoryItemSpinEdit1.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.repositoryItemSpinEdit1.IsFloatValue = false;
            this.repositoryItemSpinEdit1.Mask.EditMask = "N00";
            this.repositoryItemSpinEdit1.MaxValue = new decimal(new int[] {
            1000,
            0,
            0,
            0});
            this.repositoryItemSpinEdit1.Name = "repositoryItemSpinEdit1";
            this.repositoryItemSpinEdit1.ReadOnly = true;
            // 
            // gridColumn1
            // 
            this.gridColumn1.ColumnEdit = this.repositoryItemButtonEdit1;
            this.gridColumn1.FieldName = "gridColumn1";
            this.gridColumn1.Name = "gridColumn1";
            this.gridColumn1.OptionsColumn.AllowMerge = DevExpress.Utils.DefaultBoolean.False;
            this.gridColumn1.OptionsColumn.AllowMove = false;
            this.gridColumn1.OptionsColumn.AllowSize = false;
            this.gridColumn1.OptionsColumn.FixedWidth = true;
            this.gridColumn1.OptionsColumn.ReadOnly = true;
            this.gridColumn1.OptionsColumn.ShowInCustomizationForm = false;
            this.gridColumn1.ShowButtonMode = DevExpress.XtraGrid.Views.Base.ShowButtonModeEnum.ShowForFocusedRow;
            this.gridColumn1.UnboundType = DevExpress.Data.UnboundColumnType.Object;
            this.gridColumn1.Visible = true;
            this.gridColumn1.VisibleIndex = 3;
            this.gridColumn1.Width = 31;
            // 
            // repositoryItemButtonEdit1
            // 
            this.repositoryItemButtonEdit1.AllowFocused = false;
            this.repositoryItemButtonEdit1.AutoHeight = false;
            editorButtonImageOptions1.EnableTransparency = false;
            this.repositoryItemButtonEdit1.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Delete, "", -1, true, true, false, editorButtonImageOptions1, new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), "Eliminar o Bloco")});
            this.repositoryItemButtonEdit1.Name = "repositoryItemButtonEdit1";
            this.repositoryItemButtonEdit1.ReadOnly = true;
            this.repositoryItemButtonEdit1.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor;
            this.repositoryItemButtonEdit1.ButtonClick += new DevExpress.XtraEditors.Controls.ButtonPressedEventHandler(this.repositoryItemButtonEdit1_ButtonClick);
            // 
            // repositoryItemCheckEdit1
            // 
            this.repositoryItemCheckEdit1.AutoHeight = false;
            this.repositoryItemCheckEdit1.Caption = "Check";
            this.repositoryItemCheckEdit1.Name = "repositoryItemCheckEdit1";
            // 
            // bLOCOSBindingSource
            // 
            this.bLOCOSBindingSource.DataMember = "FK_BLOCOS_CONDOMINIOS";
            this.bLOCOSBindingSource.DataSource = this.cONDOMINIOSBindingSource;
            // 
            // cONProcuracaoCheckEdit
            // 
            this.cONProcuracaoCheckEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.cONDOMINIOSBindingSource, "CONProcuracao", true));
            this.cONProcuracaoCheckEdit.Location = new System.Drawing.Point(3, 3);
            this.cONProcuracaoCheckEdit.Name = "cONProcuracaoCheckEdit";
            this.cONProcuracaoCheckEdit.Properties.Caption = "Possui procura��o";
            this.cONProcuracaoCheckEdit.Size = new System.Drawing.Size(133, 19);
            this.cONProcuracaoCheckEdit.TabIndex = 3;
            // 
            // cONFracaoIdealCheckEdit
            // 
            this.cONFracaoIdealCheckEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.cONDOMINIOSBindingSource, "CONFracaoIdeal", true));
            this.cONFracaoIdealCheckEdit.Location = new System.Drawing.Point(3, 28);
            this.cONFracaoIdealCheckEdit.Name = "cONFracaoIdealCheckEdit";
            this.cONFracaoIdealCheckEdit.Properties.Caption = "Utiliza Fra��o Ideal";
            this.cONFracaoIdealCheckEdit.Size = new System.Drawing.Size(146, 19);
            this.cONFracaoIdealCheckEdit.TabIndex = 5;
            // 
            // groupControl8
            // 
            this.groupControl8.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.groupControl8.Controls.Add(this.button1);
            this.groupControl8.Controls.Add(this.textEdit2);
            this.groupControl8.Controls.Add(this.labelControl3);
            this.groupControl8.Controls.Add(this.textEdit1);
            this.groupControl8.Controls.Add(this.labelControl2);
            this.groupControl8.Controls.Add(this.simpleButton2);
            this.groupControl8.Controls.Add(this.TotFracao);
            this.groupControl8.Controls.Add(this.labelControl1);
            this.groupControl8.Controls.Add(this.simpleButton1);
            this.groupControl8.Controls.Add(this.cONFracaoIdealCheckEdit);
            this.groupControl8.Controls.Add(this.cONProcuracaoCheckEdit);
            this.groupControl8.Dock = System.Windows.Forms.DockStyle.Top;
            this.groupControl8.Location = new System.Drawing.Point(0, 0);
            this.groupControl8.Name = "groupControl8";
            this.groupControl8.Size = new System.Drawing.Size(1256, 89);
            this.groupControl8.TabIndex = 11;
            this.groupControl8.Text = "groupControl8";
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(1087, 18);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(75, 23);
            this.button1.TabIndex = 21;
            this.button1.Text = "button1";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Visible = false;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // textEdit2
            // 
            this.textEdit2.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.cONDOMINIOSBindingSource, "CONNomeApto", true));
            this.textEdit2.Location = new System.Drawing.Point(845, 54);
            this.textEdit2.Name = "textEdit2";
            this.textEdit2.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textEdit2.Properties.Appearance.Options.UseFont = true;
            this.textEdit2.Size = new System.Drawing.Size(132, 24);
            this.textEdit2.TabIndex = 20;
            // 
            // labelControl3
            // 
            this.labelControl3.Appearance.Font = new System.Drawing.Font("Tahoma", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl3.Appearance.Options.UseFont = true;
            this.labelControl3.Location = new System.Drawing.Point(595, 57);
            this.labelControl3.Name = "labelControl3";
            this.labelControl3.Size = new System.Drawing.Size(179, 18);
            this.labelControl3.TabIndex = 19;
            this.labelControl3.Text = "Denomina��o do \"Apto\":";
            // 
            // textEdit1
            // 
            this.textEdit1.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.cONDOMINIOSBindingSource, "CONNomeBloco", true));
            this.textEdit1.Location = new System.Drawing.Point(343, 53);
            this.textEdit1.Name = "textEdit1";
            this.textEdit1.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textEdit1.Properties.Appearance.Options.UseFont = true;
            this.textEdit1.Size = new System.Drawing.Size(132, 24);
            this.textEdit1.TabIndex = 18;
            // 
            // labelControl2
            // 
            this.labelControl2.Appearance.Font = new System.Drawing.Font("Tahoma", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl2.Appearance.Options.UseFont = true;
            this.labelControl2.Location = new System.Drawing.Point(85, 56);
            this.labelControl2.Name = "labelControl2";
            this.labelControl2.Size = new System.Drawing.Size(193, 18);
            this.labelControl2.TabIndex = 17;
            this.labelControl2.Text = "Denomina��o do \"blocos\":";
            // 
            // simpleButton2
            // 
            this.simpleButton2.Appearance.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.simpleButton2.Appearance.Options.UseFont = true;
            this.simpleButton2.Location = new System.Drawing.Point(155, 5);
            this.simpleButton2.Name = "simpleButton2";
            this.simpleButton2.Size = new System.Drawing.Size(182, 45);
            this.simpleButton2.TabIndex = 16;
            this.simpleButton2.Text = "Fus�o de unidades";
            this.simpleButton2.Click += new System.EventHandler(this.simpleButton2_Click);
            // 
            // TotFracao
            // 
            this.TotFracao.EditValue = "0,00%";
            this.TotFracao.Location = new System.Drawing.Point(845, 12);
            this.TotFracao.Name = "TotFracao";
            this.TotFracao.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.TotFracao.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TotFracao.Properties.Appearance.ForeColor = System.Drawing.Color.Blue;
            this.TotFracao.Properties.Appearance.Options.UseBackColor = true;
            this.TotFracao.Properties.Appearance.Options.UseFont = true;
            this.TotFracao.Properties.Appearance.Options.UseForeColor = true;
            this.TotFracao.Properties.ReadOnly = true;
            this.TotFracao.Size = new System.Drawing.Size(134, 30);
            this.TotFracao.TabIndex = 15;
            // 
            // labelControl1
            // 
            this.labelControl1.Appearance.Font = new System.Drawing.Font("Tahoma", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl1.Appearance.Options.UseFont = true;
            this.labelControl1.Location = new System.Drawing.Point(618, 10);
            this.labelControl1.Name = "labelControl1";
            this.labelControl1.Size = new System.Drawing.Size(170, 23);
            this.labelControl1.TabIndex = 14;
            this.labelControl1.Text = "Total das fra��es:";
            // 
            // simpleButton1
            // 
            this.simpleButton1.Appearance.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.simpleButton1.Appearance.Options.UseFont = true;
            this.simpleButton1.Location = new System.Drawing.Point(343, 5);
            this.simpleButton1.Name = "simpleButton1";
            this.simpleButton1.Size = new System.Drawing.Size(269, 45);
            this.simpleButton1.TabIndex = 13;
            this.simpleButton1.Text = "Simulador de rateios / Fra��o";
            this.simpleButton1.Click += new System.EventHandler(this.simpleButton1_Click);
            // 
            // groupControl7
            // 
            this.groupControl7.Controls.Add(this.bLOCOSGridControl);
            this.groupControl7.Dock = System.Windows.Forms.DockStyle.Fill;
            this.groupControl7.Location = new System.Drawing.Point(0, 89);
            this.groupControl7.Name = "groupControl7";
            this.groupControl7.Size = new System.Drawing.Size(1256, 718);
            this.groupControl7.TabIndex = 115;
            this.groupControl7.Text = ":: Blocos :: Apatamentos ::";
            // 
            // TabConstituicao
            // 
            this.CaixaAltaGeral = false;
            this.Controls.Add(this.groupControl7);
            this.Controls.Add(this.groupControl8);
            this.Doc = System.Windows.Forms.DockStyle.Fill;
            this.LookAndFeel.UseDefaultLookAndFeel = false;
            this.Name = "TabConstituicao";
            this.Size = new System.Drawing.Size(1256, 807);
            this.Load += new System.EventHandler(this.TabConstituicao_Load);
            ((System.ComponentModel.ISupportInitialize)(this.StyleController_F)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.GridApartamentos)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEdit3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCalcEdit1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LupPessoas)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pESSOASBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LookInq)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LookUpEditImobiliaria)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.iMOBILIARIASBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.BotoesApartamento)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.FormaPag)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.imageCollection1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemImageComboBox1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.bLOCOSGridControl)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cONDOMINIOSBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.GridBlocos)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEdit2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEdit1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemSpinEdit1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemButtonEdit1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckEdit1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.bLOCOSBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cONProcuracaoCheckEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cONFracaoIdealCheckEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl8)).EndInit();
            this.groupControl8.ResumeLayout(false);
            this.groupControl8.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit2.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit1.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TotFracao.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl7)).EndInit();
            this.groupControl7.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.BindingSource cONDOMINIOSBindingSource;
        private DevExpress.XtraEditors.CheckEdit cONProcuracaoCheckEdit;
        private DevExpress.XtraEditors.CheckEdit cONFracaoIdealCheckEdit;
        private System.Windows.Forms.BindingSource bLOCOSBindingSource;
        private System.Windows.Forms.BindingSource pESSOASBindingSource;
        private System.Windows.Forms.BindingSource iMOBILIARIASBindingSource;
        private DevExpress.XtraEditors.GroupControl groupControl8;
        private DevExpress.XtraGrid.GridControl bLOCOSGridControl;
        private DevExpress.XtraGrid.Views.Grid.GridView GridApartamentos;
        private DevExpress.XtraGrid.Columns.GridColumn colAPTNumero;
        private DevExpress.XtraGrid.Columns.GridColumn colAPTFracaoIdeal;
        private DevExpress.XtraEditors.Repository.RepositoryItemCalcEdit repositoryItemCalcEdit1;
        private DevExpress.XtraGrid.Columns.GridColumn colAPTProprietario_PES;
        private DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit LupPessoas;
        private DevExpress.XtraGrid.Columns.GridColumn colAPTInquilino_PES;
        private DevExpress.XtraGrid.Columns.GridColumn colAPTImobiliaria_FRN;
        private DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit LookUpEditImobiliaria;
        private DevExpress.XtraGrid.Views.Grid.GridView GridBlocos;
        private DevExpress.XtraGrid.Columns.GridColumn colBLOCodigo;
        private DevExpress.XtraGrid.Columns.GridColumn colBLONome;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit repositoryItemTextEdit1;
        private DevExpress.XtraGrid.Columns.GridColumn colBLOTotalApartamento1;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn1;
        private DevExpress.XtraEditors.Repository.RepositoryItemButtonEdit repositoryItemButtonEdit1;
        private DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit LookInq;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit repositoryItemTextEdit2;
        private DevExpress.XtraEditors.Repository.RepositoryItemSpinEdit repositoryItemSpinEdit1;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit repositoryItemTextEdit3;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn2;
        private DevExpress.XtraEditors.Repository.RepositoryItemButtonEdit BotoesApartamento;
        private DevExpress.XtraEditors.GroupControl groupControl7;
        private DevExpress.XtraGrid.Columns.GridColumn colAPTFormaPagtoProprietario_FPG;
        private DevExpress.XtraGrid.Columns.GridColumn colAPTFormaPagtoInquilino_FPG;
        private DevExpress.Utils.ImageCollection imageCollection1;
        private DevExpress.XtraEditors.Repository.RepositoryItemImageComboBox FormaPag;
        private DevExpress.XtraGrid.Columns.GridColumn colAPTRegistro;
        private DevExpress.XtraEditors.SimpleButton simpleButton1;
        private DevExpress.XtraEditors.LabelControl labelControl1;
        private DevExpress.XtraEditors.TextEdit TotFracao;
        private DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit repositoryItemCheckEdit1;
        private DevExpress.XtraEditors.SimpleButton simpleButton2;
        private DevExpress.XtraEditors.TextEdit textEdit1;
        private DevExpress.XtraEditors.LabelControl labelControl2;
        private DevExpress.XtraEditors.TextEdit textEdit2;
        private DevExpress.XtraEditors.LabelControl labelControl3;
        private System.Windows.Forms.Button button1;
        private DevExpress.XtraGrid.Columns.GridColumn colAPTTipo;
        private DevExpress.XtraEditors.Repository.RepositoryItemImageComboBox repositoryItemImageComboBox1;
    }
}
