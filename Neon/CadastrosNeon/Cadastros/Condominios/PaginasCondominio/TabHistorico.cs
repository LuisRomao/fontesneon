using System;
using System.Windows.Forms;

namespace Cadastros.Condominios.PaginasCondominio
{
    /// <summary>
    /// 
    /// </summary>
    public partial class TabHistorico : CompontesBasicos.ComponenteTabCampos
    {
        /// <summary>
        /// 
        /// </summary>
        public TabHistorico()
        {
            InitializeComponent();
        }

        
        /// <summary>
        /// 
        /// </summary>
        /// <param name="Entrando"></param>
        public override void OnTrocaPagina(bool Entrando)
        {
            if (Entrando)
            {
                //REMOVER
                //VirMSSQL.TableAdapter.STTableAdapter.ExecutarSQLNonQuery("DELETE FROM HISTORICOS WHERE (HSTDATAI < CONVERT(DATETIME, '2008-10-01 00:00:00', 102))");
                UsuariosBindingSource.DataSource = FrameworkProc.datasets.dUSUarios.dUSUariosSt.USUarios;
                ASSuntobindingSource.DataSource = Framework.datasets.dASSunto.dASSuntoSt.ASSunto;
                hISTORICOSBindingSource.DataSource = dHistoricos.dHistoricosSt;
                hISTORICOSBindingSource.DataMember = "HISTORICOS";
                dHistoricos.dHistoricosSt.HISTORICOSTableAdapter.Fill(dHistoricos.dHistoricosSt.HISTORICOS, TabMae.pk);
                hISTORICOSBindingSource.MoveLast();
            }            
        }

        
        

        private void barButtonItem1_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            dHistoricos.HISTORICOSRow rowHST = dHistoricos.dHistoricosSt.HISTORICOS.NewHISTORICOSRow();
            rowHST.HST_CON = TabMae.pk;
            rowHST.HSTI_USU = rowHST.HSTA_USU = Framework.usuarioLogado.UsuarioLogadoNeon.UsuarioLogadoStNEON.USU;
            rowHST.HSTAceito = true;
            rowHST.HSTDATAPrometida = rowHST.HSTADATA = rowHST.HSTDATAI = DateTime.Now;
            rowHST.HSTConcluido = true;
            rowHST.HSTCritico = false;
            //rowHST.HSTDescricao = "";
            //rowHST.HST_ASS = 1;
            if (ShowModulo(typeof(cHistoricos), rowHST) == DialogResult.OK)
            {
                dHistoricos.dHistoricosSt.HISTORICOS.AddHISTORICOSRow(rowHST);
                dHistoricos.dHistoricosSt.HISTORICOSTableAdapter.Update(rowHST);
                rowHST.AcceptChanges();
                layoutView1.MoveLast();
            }
                                    
        }

        private void hISTORICOSGridControl_Click(object sender, EventArgs e)
        {

        }
    }
}

