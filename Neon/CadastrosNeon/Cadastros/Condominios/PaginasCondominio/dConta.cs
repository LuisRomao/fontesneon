﻿/*
LH - 27/05/2014       13.2.9.4  - Nova estrura para rentabilidade e transferir arrecadado
MR - 05/08/2014 11:00           - Tratamento do tipo de conta do condominio na funcao (GetContaPgEletronicoByCON) de retorno das contas para pagamento eletronico em ContaCorrenTeTableAdapter
LH - 08/09/2014 08:48 14.1.4.41 - Cadastro da agência.
MR - 10/06/2015 08:20           - Alterada a descrição da conta indicando CC ou Poupança em ContaCorrenTeTableAdapter
MR - 28/04/2016 12:00           - Corrige a descrição da conta indicando CC ou Poupança em ContaCorrenTeTableAdapter com base no campo CCTAplicacao e não mais o CCTTipo
*/

using Framework;
using System;
using VirEnumeracoesNeon;

namespace Cadastros.Condominios.PaginasCondominio
{


    partial class dConta
    {
        internal string ErroRentabilidade;

        private int CON;

        internal void Fill(int _CON)
        {
            CON = _CON;
            ContaCorrenTeTableAdapter.Fill(ContaCorrenTe, CON);
            foreach (dConta.ContaCorrenTeRow rowCCT in ContaCorrenTe)
            {
                if (rowCCT.IsCCTTipoNull())
                {
                    if (rowCCT.CCTAplicacao)
                        rowCCT.CCTTipo = (int)CCTTipo.Poupanca;
                    else
                        rowCCT.CCTTipo = (int)CCTTipo.ContaCorrete;
                    ContaCorrenTeTableAdapter.Update(rowCCT);
                    rowCCT.AcceptChanges();
                }
            }
            ConTasLogicasTableAdapter.FillByCON(ConTasLogicas, CON);
            CTLxCCTTableAdapter.FillBy(CTLxCCT, CON);
        }

        private void SomaErro(string NovoErro)
        {
            if (ErroRentabilidade == null)
                ErroRentabilidade = string.Format("Erros nas contas:\r\n\r\n{0}", NovoErro);
            else
                ErroRentabilidade = string.Format("{0}\r\n\r\n{1}", ErroRentabilidade, NovoErro);
        }

        internal bool validaContas()
        {
            return validaContas(null, null, null, null, "", null);
        }

        internal bool validaContas(int? BCO, int? Agencia, int? AgenciaDg, int? Conta, string ContaDg, int? CNR)
        {
            ErroRentabilidade = null;
            dConta.ContaCorrenTeRow rowCCTPrincipal = null;
            //dConta.ContaCorrenTeRow rowCCTMasterOculta = null;
            dConta.ContaCorrenTeRow rowCCTOculta = null;
            foreach (dConta.ContaCorrenTeRow CCTrow in ContaCorrenTe)
            {
                if (CCTrow.IsCCTAtivaNull())
                    CCTrow.CCTAtiva = false;
                if (!CCTrow.CCTAtiva)
                    continue;
                switch ((CCTTipo)CCTrow.CCTTipo)
                {
                    case CCTTipo.Aplicacao:
                        break;
                    case CCTTipo.ContaCorrete:
                        if (CCTrow.CCTPrincipal)
                        {
                            if (rowCCTPrincipal == null)
                                rowCCTPrincipal = CCTrow;
                            else
                            {
                                SomaErro("Existe mais de uma conta Pricipal");
                                return false;
                            }
                        }
                        if (CCTrow.CCT_BCO == 104)
                        {
                            if (CCTrow.IsCCTCaixaPostalNull())
                            {
                                SomaErro("Conta Corrente da CAIXA sem Caixa Postal");
                                return false;
                            }
                        }
                        break;
                    case CCTTipo.ContaCorreteOculta:
                        if (rowCCTOculta != null)
                        {
                            SomaErro("Existe mais de uma conta oculta");
                            return false;
                        }
                        else
                            rowCCTOculta = CCTrow;
                        break;
                    case CCTTipo.ContaCorrete_com_Oculta:
                        if (CCTrow.CCTPrincipal)
                        {
                            if (rowCCTPrincipal == null)
                                rowCCTPrincipal = CCTrow;
                            else
                            {
                                SomaErro("Existe mais de uma conta Pricipal");
                                return false;
                            }
                        }
                        /*
                        if (rowCCTMasterOculta != null)
                        {
                            SomaErro("Existe mais de uma conta com conta oculta");
                            return false;
                        }
                        else
                            rowCCTMasterOculta = CCTrow;*/
                        break;
                    case CCTTipo.ContaPool:
                        break;
                    case CCTTipo.Poupanca:
                        break;
                    default:
                        break;
                }

            }
            if (rowCCTPrincipal == null)
            {
                if (BCO == 104)
                {
                    SomaErro("Não existe uma conta Pricipal");
                    return false;
                }
            }
            else
            {
                if (
                     BCO.HasValue
                    &&
                    (
                     (rowCCTPrincipal.CCT_BCO != BCO.Value)
                      ||
                     (rowCCTPrincipal.CCTAgencia != Agencia.Value)
                      ||
                     (rowCCTPrincipal.CCTAgenciaDg != AgenciaDg.Value)
                      ||
                     (rowCCTPrincipal.CCTConta != Conta.Value)
                      ||
                     (rowCCTPrincipal.CCTContaDg != ContaDg)
                      ||
                     (rowCCTPrincipal.CCTCNR != CNR.Value)
                    )
                   )
                {
                    SomaErro("Dados da conta não conferem como a conta principal");
                    return false;
                }
            }
            dConta.ConTasLogicasRow rowCTLCaixa = null;
            dConta.ConTasLogicasRow rowCTLFundoReserva = null;
            dConta.ConTasLogicasRow rowCTLCreditosAnteriores = null;
            foreach (dConta.ConTasLogicasRow CTLrow in ConTasLogicas)
            {
                switch ((CTLTipo)CTLrow.CTLTipo)
                {
                    case CTLTipo.Caixa:
                        if (rowCTLCaixa != null)
                        {
                            SomaErro("Existe mais de uma conta CAIXA");
                            return false;
                        }
                        else
                            rowCTLCaixa = CTLrow;
                        break;
                    case CTLTipo.Fundo:
                        if (rowCTLFundoReserva != null)
                        {
                            SomaErro("Existe mais de uma conta \"Fundo de Reserva\"");
                            return false;
                        }
                        else
                            rowCTLFundoReserva = CTLrow;
                        break;
                    case CTLTipo.CreditosAnteriores:
                        if (rowCTLCreditosAnteriores != null)
                        {
                            SomaErro("Existe mais de uma conta \"Créditos não identificados/Antecipados\"");
                            return false;
                        }
                        else
                            rowCTLCreditosAnteriores = CTLrow;
                        break;
                    case CTLTipo.Provisao:
                        break;
                    case CTLTipo.Rateio:
                        break;
                    case CTLTipo.Terceiro:
                        break;
                    default:
                        break;
                }
            }
            if (rowCTLCaixa == null)
            {
                rowCTLCaixa = ConTasLogicas.NewConTasLogicasRow();
                rowCTLCaixa.CTL_CON = CON;
                rowCTLCaixa.CTLAtiva = true;
                rowCTLCaixa.CTLDATAI = DateTime.Now;
                rowCTLCaixa.CTLTipo = (int)CTLTipo.Caixa;
                rowCTLCaixa.CTLTitulo = "CAIXA";
                ConTasLogicas.AddConTasLogicasRow(rowCTLCaixa);
                ConTasLogicasTableAdapter.Update(rowCTLCaixa);
                rowCTLCaixa.AcceptChanges();
                //incluir uma nova linha
            }
            else
                if (!rowCTLCaixa.CTLAtiva)
            {
                rowCTLCaixa.CTLAtiva = true;
                ConTasLogicasTableAdapter.Update(rowCTLCaixa);
                rowCTLCaixa.AcceptChanges();
            };
            if (rowCTLFundoReserva == null)
            {
                rowCTLFundoReserva = ConTasLogicas.NewConTasLogicasRow();
                //rowCTLFundoReserva.CTL_CCT = rowCCTPrincipal.CCT;
                rowCTLFundoReserva.CTL_CON = CON;
                rowCTLFundoReserva.CTLAtiva = true;
                rowCTLFundoReserva.CTLDATAI = DateTime.Now;
                rowCTLFundoReserva.CTLTipo = (int)CTLTipo.Fundo;
                rowCTLFundoReserva.CTLTitulo = "Fundo de Reserva";
                ConTasLogicas.AddConTasLogicasRow(rowCTLFundoReserva);
                ConTasLogicasTableAdapter.Update(rowCTLFundoReserva);
                rowCTLFundoReserva.AcceptChanges();
                //incluir uma nova linha
            }
            else
                if (!rowCTLFundoReserva.CTLAtiva)
            {
                rowCTLFundoReserva.CTLAtiva = true;
                ConTasLogicasTableAdapter.Update(rowCTLFundoReserva);
                rowCTLFundoReserva.AcceptChanges();
            }
            if (rowCTLCreditosAnteriores == null)
            {
                /*
                rowCTLCreditosAnteriores = ConTasLogicas.NewConTasLogicasRow();
                rowCTLCreditosAnteriores.CTL_CON = CON;
                rowCTLCreditosAnteriores.CTLAtiva = true;
                rowCTLCreditosAnteriores.CTLDATAI = DateTime.Now;
                rowCTLCreditosAnteriores.CTLTipo = (int)CTLTipo.CreditosAnteriores;
                rowCTLCreditosAnteriores.CTLTitulo = "Créditos não identificados/Antecipados";
                ConTasLogicas.AddConTasLogicasRow(rowCTLCreditosAnteriores);
                ConTasLogicasTableAdapter.Update(rowCTLCreditosAnteriores);
                rowCTLCreditosAnteriores.AcceptChanges();                
                */
            }
            if (!VerificaRentabilidade())
                return false;
            return true;
        }

        private bool VerificaRentabilidade()
        {
            ErroRentabilidade = null;
            foreach (ContaCorrenTeRow CCTrow in ContaCorrenTe)
            {
                if (!CCTrow.IsCCTAtivaNull() && (!CCTrow.CCTAtiva))
                    continue;
                CCTTipo Tipo = (CCTTipo)CCTrow.CCTTipo;
                if ((Tipo == CCTTipo.Aplicacao) || (Tipo == CCTTipo.Poupanca))
                {
                    bool SemRentabilidade = true;
                    CTLxCCTRow[] CTLxCCTRows = CCTrow.GetCTLxCCTRows();
                    foreach (CTLxCCTRow CTLxCCTRowTeste in CTLxCCTRows)
                    {
                        if (CTLxCCTRowTeste.CTLCCTRentabilidade)
                        {
                            SemRentabilidade = false;
                            break;
                        }
                    }
                    if (SemRentabilidade)
                        SomaErro(string.Format("Conta {0} sem indicação de conta lógica para rentabilidade", CCTrow.IsCCTTituloNull() ? "'Sem Título'" : CCTrow.CCTTitulo));
                }
            }
            return ErroRentabilidade == null;
        }

        private dContaTableAdapters.CTLxCCTTableAdapter cTLxCCTTableAdapter;
        /// <summary>
        /// TableAdapter padrão para tabela: CTLxCCT
        /// </summary>
        public dContaTableAdapters.CTLxCCTTableAdapter CTLxCCTTableAdapter
        {
            get
            {
                if (cTLxCCTTableAdapter == null)
                {
                    cTLxCCTTableAdapter = new dContaTableAdapters.CTLxCCTTableAdapter();
                    cTLxCCTTableAdapter.TrocarStringDeConexao();
                };
                return cTLxCCTTableAdapter;
            }
        }

        private dContaTableAdapters.ConTasLogicasTableAdapter conTasLogicasTableAdapter;
        /// <summary>
        /// TableAdapter padrão para tabela: ConTasLogicas
        /// </summary>
        public dContaTableAdapters.ConTasLogicasTableAdapter ConTasLogicasTableAdapter
        {
            get
            {
                if (conTasLogicasTableAdapter == null)
                {
                    conTasLogicasTableAdapter = new dContaTableAdapters.ConTasLogicasTableAdapter();
                    conTasLogicasTableAdapter.TrocarStringDeConexao();
                };
                return conTasLogicasTableAdapter;
            }
        }

        private dContaTableAdapters.ContaCorrenTeTableAdapter contaCorrenTeTableAdapter;
        /// <summary>
        /// TableAdapter padrão para tabela: ContaCorrenTe
        /// </summary>
        public dContaTableAdapters.ContaCorrenTeTableAdapter ContaCorrenTeTableAdapter
        {
            get
            {
                if (contaCorrenTeTableAdapter == null)
                {
                    contaCorrenTeTableAdapter = new dContaTableAdapters.ContaCorrenTeTableAdapter();
                    contaCorrenTeTableAdapter.TrocarStringDeConexao();
                };
                return contaCorrenTeTableAdapter;
            }
        }

        /// <summary>
        /// Verifica se uma conta está cadastrada
        /// </summary>
        /// <param name="BCO"></param>
        /// <param name="Agencia"></param>
        /// <param name="Conta"></param>
        /// <param name="Aplicacao"></param>
        /// <returns></returns>
        public ContaCorrenTeRow getContaCadastrada(int BCO, int Agencia, int Conta, bool Aplicacao)
        {
            ContaCorrenTeDataTable TabRetorno = ContaCorrenTeTableAdapter.GetDataByConta(BCO, Agencia, Conta, Aplicacao);
            foreach (ContaCorrenTeRow rowTestar in TabRetorno)
            {
                if (rowTestar.CCTTipo != (int)CCTTipo.ContaDaADM)
                    return rowTestar;
            }
            return null;
            //if (TabRetorno.Rows.Count == 0)
            //    return null;
            //return TabRetorno[0];
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="CON"></param>
        /// <param name="BCO"></param>
        /// <param name="Agencia"></param>
        /// <param name="Conta"></param>
        /// <returns></returns>
        public bool GravaCON(int CON, int BCO, int Agencia, int Conta)
        {
            return (ContaCorrenTeTableAdapter.GravaCON(CON, BCO, Agencia, Conta) == 1);
        }
    }
}
