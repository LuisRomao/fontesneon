﻿using System;
using System.Collections.Generic;
using System.Linq;
using CompontesBasicos;

namespace Cadastros.Condominios.PaginasCondominio
{
    /// <summary>
    /// Tab para os dados do balancete
    /// </summary>
    public partial class TabBalancete : ComponenteTabCampos
    {
        /// <summary>
        /// Construtor
        /// </summary>
        public TabBalancete()
        {
            InitializeComponent();
        }

        private cCondominiosCampos2 Chamador
        {
            get { return (cCondominiosCampos2)TabMae; }
        }

        private void TabBalancete_Load(object sender, EventArgs e)
        {
            if (Chamador != null)
            {
                if (Chamador.dCondominiosCampos.ModeloBAlancete.Count == 0)
                    Chamador.dCondominiosCampos.ModeloBAlanceteTableAdapter.Fill(Chamador.dCondominiosCampos.ModeloBAlancete);
                modeloBAlanceteBindingSource.DataSource = Chamador.dCondominiosCampos;
            }
        }

        private void cONDiaContabilidadeSpinEdit_TextChanged(object sender, EventArgs e)
        {
            if ((Inicio.Value <= 0) || (Inicio.Value > 31))
                txtPerContabilFim1.Text = "";
            else
                if (Inicio.Value == 1)
                    txtPerContabilFim1.Text = "31";
                else
                    if (Inicio.Value == 31)
                        txtPerContabilFim1.Text = "1";
                    else
                        txtPerContabilFim1.Text = ((Int32)(Inicio.Value - 1)).ToString();
        }

        private void lookUpEditMBABal_ButtonClick(object sender, DevExpress.XtraEditors.Controls.ButtonPressedEventArgs e)
        {
            lookUpEditMBABal.EditValue = null;
        }

        private void lookUpEditMBABol_ButtonClick(object sender, DevExpress.XtraEditors.Controls.ButtonPressedEventArgs e)
        {
            lookUpEditMBABol.EditValue = null;
        }

        private void lookUpEditMBAAc_ButtonClick(object sender, DevExpress.XtraEditors.Controls.ButtonPressedEventArgs e)
        {
            lookUpEditMBAAc.EditValue = null;
        }

        private void lookUpEditMBABalsite1_ButtonClick(object sender, DevExpress.XtraEditors.Controls.ButtonPressedEventArgs e)
        {
            lookUpEditMBABalsite1.EditValue = null;
        }

        private void lookUpEditMBABalsite2_ButtonClick(object sender, DevExpress.XtraEditors.Controls.ButtonPressedEventArgs e)
        {
            lookUpEditMBABalsite2.EditValue = null;            
        }
        
    }
}
