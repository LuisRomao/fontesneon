namespace Cadastros.Condominios.PaginasCondominio
{
    partial class cEditaPublicacoes
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling1 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling2 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            this.XLocao = new DevExpress.XtraEditors.TextEdit();
            this.XData = new DevExpress.XtraEditors.TimeEdit();
            this.XTipo = new DevExpress.XtraEditors.ImageComboBoxEdit();
            this.labelControl1 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl2 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl3 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl4 = new DevExpress.XtraEditors.LabelControl();
            this.XArquivo = new DevExpress.XtraEditors.ButtonEdit();
            this.XDescricao = new DevExpress.XtraEditors.TextEdit();
            this.labelControl5 = new DevExpress.XtraEditors.LabelControl();
            this.openFileDialog1 = new System.Windows.Forms.OpenFileDialog();
            ((System.ComponentModel.ISupportInitialize)(this.BarManager_F)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.StyleController_F)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.XLocao.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.XData.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.XTipo.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.XArquivo.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.XDescricao.Properties)).BeginInit();
            this.SuspendLayout();
            // 
            // barComponente_F
            // 
            this.barComponente_F.OptionsBar.AllowQuickCustomization = false;
            this.barComponente_F.OptionsBar.DisableClose = true;
            this.barComponente_F.OptionsBar.DisableCustomization = true;
            this.barComponente_F.OptionsBar.DrawDragBorder = false;
            this.barComponente_F.OptionsBar.UseWholeRow = true;
            // 
            // ToolTipController_F
            // 
            this.ToolTipController_F.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.ToolTipController_F.Appearance.ForeColor = System.Drawing.Color.Black;
            this.ToolTipController_F.Appearance.Options.UseBackColor = true;
            this.ToolTipController_F.Appearance.Options.UseFont = true;
            this.ToolTipController_F.Appearance.Options.UseForeColor = true;
            this.ToolTipController_F.Appearance.Options.UseTextOptions = true;
            this.ToolTipController_F.Appearance.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            // 
            // StyleController_F
            // 
            this.StyleController_F.AppearanceDisabled.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this.StyleController_F.AppearanceDisabled.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this.StyleController_F.AppearanceDisabled.Options.UseBackColor = true;
            this.StyleController_F.AppearanceDisabled.Options.UseForeColor = true;
            this.StyleController_F.AppearanceFocused.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.StyleController_F.AppearanceFocused.Options.UseBackColor = true;
         
            // 
            // XLocao
            // 
            this.XLocao.Location = new System.Drawing.Point(131, 122);
            this.XLocao.MenuManager = this.BarManager_F;
            this.XLocao.Name = "XLocao";
            this.XLocao.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.XLocao.Properties.Appearance.Options.UseFont = true;
            this.XLocao.Properties.MaxLength = 50;
            
            this.XLocao.Size = new System.Drawing.Size(421, 26);
            
            this.XLocao.TabIndex = 2;
            // 
            // XData
            // 
            this.XData.EditValue = new System.DateTime(2009, 7, 8, 0, 0, 0, 0);
            this.XData.Location = new System.Drawing.Point(131, 58);
            this.XData.MenuManager = this.BarManager_F;
            this.XData.Name = "XData";
            this.XData.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.XData.Properties.Appearance.Options.UseFont = true;
            this.XData.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.XData.Properties.DisplayFormat.FormatString = "dd/MM/yyyy HH:mm";
            this.XData.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.XData.Properties.EditFormat.FormatString = "dd/MM/yyyy HH:mm";
            this.XData.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.XData.Properties.EditValueChangedFiringMode = DevExpress.XtraEditors.Controls.EditValueChangedFiringMode.Default;
            this.XData.Properties.Mask.EditMask = "dd/MM/yyyy HH:mm";
            this.XData.Size = new System.Drawing.Size(189, 26);
            this.XData.TabIndex = 0;
            // 
            // XTipo
            // 
            this.XTipo.Location = new System.Drawing.Point(131, 154);
            this.XTipo.MenuManager = this.BarManager_F;
            this.XTipo.Name = "XTipo";
            this.XTipo.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.XTipo.Properties.Appearance.Options.UseFont = true;
            this.XTipo.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.XTipo.Size = new System.Drawing.Size(120, 26);
            this.XTipo.TabIndex = 3;
            // 
            // labelControl1
            // 
            this.labelControl1.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl1.Appearance.Options.UseFont = true;
            this.labelControl1.Location = new System.Drawing.Point(20, 61);
            this.labelControl1.Name = "labelControl1";
            this.labelControl1.Size = new System.Drawing.Size(39, 19);
            this.labelControl1.TabIndex = 9;
            this.labelControl1.Text = "Data";
            // 
            // labelControl2
            // 
            this.labelControl2.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl2.Appearance.Options.UseFont = true;
            this.labelControl2.Location = new System.Drawing.Point(20, 93);
            this.labelControl2.Name = "labelControl2";
            this.labelControl2.Size = new System.Drawing.Size(78, 19);
            this.labelControl2.TabIndex = 10;
            this.labelControl2.Text = "Descri��o";
            // 
            // labelControl3
            // 
            this.labelControl3.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl3.Appearance.Options.UseFont = true;
            this.labelControl3.Location = new System.Drawing.Point(20, 157);
            this.labelControl3.Name = "labelControl3";
            this.labelControl3.Size = new System.Drawing.Size(36, 19);
            this.labelControl3.TabIndex = 11;
            this.labelControl3.Text = "Tipo";
            // 
            // labelControl4
            // 
            this.labelControl4.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl4.Appearance.Options.UseFont = true;
            this.labelControl4.Location = new System.Drawing.Point(20, 189);
            this.labelControl4.Name = "labelControl4";
            this.labelControl4.Size = new System.Drawing.Size(63, 19);
            this.labelControl4.TabIndex = 12;
            this.labelControl4.Text = "Arquivo";
            // 
            // XArquivo
            // 
            this.XArquivo.Location = new System.Drawing.Point(131, 186);
            this.XArquivo.MenuManager = this.BarManager_F;
            this.XArquivo.Name = "XArquivo";
            this.XArquivo.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.XArquivo.Properties.Appearance.Options.UseFont = true;
            this.XArquivo.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.XArquivo.Size = new System.Drawing.Size(421, 26);
            this.XArquivo.TabIndex = 4;
            this.XArquivo.ButtonClick += new DevExpress.XtraEditors.Controls.ButtonPressedEventHandler(this.XArquivo_ButtonClick);
            this.XArquivo.EditValueChanged += new System.EventHandler(this.XArquivo_EditValueChanged);
            // 
            // XDescricao
            // 
            this.XDescricao.Location = new System.Drawing.Point(131, 90);
            this.XDescricao.MenuManager = this.BarManager_F;
            this.XDescricao.Name = "XDescricao";
            this.XDescricao.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.XDescricao.Properties.Appearance.Options.UseFont = true;
            this.XDescricao.Properties.MaxLength = 50;
            
            this.XDescricao.Size = new System.Drawing.Size(421, 26);
            
            this.XDescricao.TabIndex = 1;
            // 
            // labelControl5
            // 
            this.labelControl5.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl5.Appearance.Options.UseFont = true;
            this.labelControl5.Location = new System.Drawing.Point(20, 125);
            this.labelControl5.Name = "labelControl5";
            this.labelControl5.Size = new System.Drawing.Size(42, 19);
            this.labelControl5.TabIndex = 15;
            this.labelControl5.Text = "Local";
            // 
            // openFileDialog1
            // 
            this.openFileDialog1.FileName = "openFileDialog1";
            this.openFileDialog1.Filter = "Todos|*.html;*.htm;*.pdf|HTML|*.html|HTM|*.htm|PDF|*.pdf";
            this.openFileDialog1.InitialDirectory = "S:\\";
            this.openFileDialog1.RestoreDirectory = true;
            // 
            // cEditaPublicacoes
            // 
            this.CaixaAltaGeral = false;
            this.Controls.Add(this.labelControl5);
            this.Controls.Add(this.XDescricao);
            this.Controls.Add(this.XArquivo);
            this.Controls.Add(this.labelControl4);
            this.Controls.Add(this.labelControl3);
            this.Controls.Add(this.labelControl2);
            this.Controls.Add(this.labelControl1);
            this.Controls.Add(this.XTipo);
            this.Controls.Add(this.XData);
            this.Controls.Add(this.XLocao);
            this.LookAndFeel.UseDefaultLookAndFeel = false;
            this.Name = "cEditaPublicacoes";
            this.Size = new System.Drawing.Size(589, 244);
            this.Controls.SetChildIndex(this.XLocao, 0);
            this.Controls.SetChildIndex(this.XData, 0);
            this.Controls.SetChildIndex(this.XTipo, 0);
            this.Controls.SetChildIndex(this.labelControl1, 0);
            this.Controls.SetChildIndex(this.labelControl2, 0);
            this.Controls.SetChildIndex(this.labelControl3, 0);
            this.Controls.SetChildIndex(this.labelControl4, 0);
            this.Controls.SetChildIndex(this.XArquivo, 0);
            this.Controls.SetChildIndex(this.XDescricao, 0);
            this.Controls.SetChildIndex(this.labelControl5, 0);
            ((System.ComponentModel.ISupportInitialize)(this.BarManager_F)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.StyleController_F)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.XLocao.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.XData.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.XTipo.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.XArquivo.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.XDescricao.Properties)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private DevExpress.XtraEditors.LabelControl labelControl1;
        private DevExpress.XtraEditors.LabelControl labelControl2;
        private DevExpress.XtraEditors.LabelControl labelControl3;
        private DevExpress.XtraEditors.LabelControl labelControl4;
        private DevExpress.XtraEditors.LabelControl labelControl5;
        /// <summary>
        /// 
        /// </summary>
        public DevExpress.XtraEditors.TextEdit XDescricao;
        /// <summary>
        /// 
        /// </summary>
        public DevExpress.XtraEditors.TimeEdit XData;
        /// <summary>
        /// 
        /// </summary>
        public DevExpress.XtraEditors.TextEdit XLocao;
        /// <summary>
        /// 
        /// </summary>
        public DevExpress.XtraEditors.ImageComboBoxEdit XTipo;
        /// <summary>
        /// 
        /// </summary>
        public DevExpress.XtraEditors.ButtonEdit XArquivo;
        private System.Windows.Forms.OpenFileDialog openFileDialog1;
    }
}
