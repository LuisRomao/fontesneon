﻿namespace Cadastros.Condominios.PaginasCondominio
{
    partial class TabBalancete
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.Label cONDiaContabilidadeLabel;
            System.Windows.Forms.Label cONValorFRLabel;
            System.Windows.Forms.Label label1;
            System.Windows.Forms.Label label2;
            System.Windows.Forms.Label label3;
            System.Windows.Forms.Label label4;
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject1 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject2 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject3 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject4 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject5 = new DevExpress.Utils.SerializableAppearanceObject();
            this.groupControl7 = new DevExpress.XtraEditors.GroupControl();
            this.groupControl1 = new DevExpress.XtraEditors.GroupControl();
            this.checkEdit3 = new DevExpress.XtraEditors.CheckEdit();
            this.cONDOMINIOSBindingSource = new System.Windows.Forms.BindingSource();
            this.groupControl9 = new DevExpress.XtraEditors.GroupControl();
            this.spinEdit1 = new DevExpress.XtraEditors.SpinEdit();
            this.groupControl8 = new DevExpress.XtraEditors.GroupControl();
            this.labelControl1 = new DevExpress.XtraEditors.LabelControl();
            this.txtPerContabilFim1 = new DevExpress.XtraEditors.TextEdit();
            this.Inicio = new DevExpress.XtraEditors.SpinEdit();
            this.memoEdit1 = new DevExpress.XtraEditors.MemoEdit();
            this.groupControl2 = new DevExpress.XtraEditors.GroupControl();
            this.lookUpEditMBABalsite2 = new DevExpress.XtraEditors.LookUpEdit();
            this.modeloBAlanceteBindingSource = new System.Windows.Forms.BindingSource();
            this.lookUpEditMBABalsite1 = new DevExpress.XtraEditors.LookUpEdit();
            this.lookUpEditMBAAc = new DevExpress.XtraEditors.LookUpEdit();
            this.lookUpEditMBABol = new DevExpress.XtraEditors.LookUpEdit();
            this.lookUpEditMBABal = new DevExpress.XtraEditors.LookUpEdit();
            this.groupControl3 = new DevExpress.XtraEditors.GroupControl();
            cONDiaContabilidadeLabel = new System.Windows.Forms.Label();
            cONValorFRLabel = new System.Windows.Forms.Label();
            label1 = new System.Windows.Forms.Label();
            label2 = new System.Windows.Forms.Label();
            label3 = new System.Windows.Forms.Label();
            label4 = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.StyleController_F)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl7)).BeginInit();
            this.groupControl7.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl1)).BeginInit();
            this.groupControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit3.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cONDOMINIOSBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl9)).BeginInit();
            this.groupControl9.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.spinEdit1.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl8)).BeginInit();
            this.groupControl8.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtPerContabilFim1.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Inicio.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.memoEdit1.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl2)).BeginInit();
            this.groupControl2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.lookUpEditMBABalsite2.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.modeloBAlanceteBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lookUpEditMBABalsite1.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lookUpEditMBAAc.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lookUpEditMBABol.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lookUpEditMBABal.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl3)).BeginInit();
            this.groupControl3.SuspendLayout();
            this.SuspendLayout();
            // 
            // ToolTipController_F
            // 
            this.ToolTipController_F.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.ToolTipController_F.Appearance.ForeColor = System.Drawing.Color.Black;
            this.ToolTipController_F.Appearance.Options.UseBackColor = true;
            this.ToolTipController_F.Appearance.Options.UseFont = true;
            this.ToolTipController_F.Appearance.Options.UseForeColor = true;
            this.ToolTipController_F.Appearance.Options.UseTextOptions = true;
            this.ToolTipController_F.Appearance.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            // 
            // StyleController_F
            // 
            this.StyleController_F.AppearanceDisabled.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this.StyleController_F.AppearanceDisabled.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this.StyleController_F.AppearanceDisabled.Options.UseBackColor = true;
            this.StyleController_F.AppearanceDisabled.Options.UseForeColor = true;
            this.StyleController_F.AppearanceFocused.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.StyleController_F.AppearanceFocused.Options.UseBackColor = true;
            // 
            // cONDiaContabilidadeLabel
            // 
            cONDiaContabilidadeLabel.AutoSize = true;
            cONDiaContabilidadeLabel.Location = new System.Drawing.Point(6, 27);
            cONDiaContabilidadeLabel.Name = "cONDiaContabilidadeLabel";
            cONDiaContabilidadeLabel.Size = new System.Drawing.Size(20, 13);
            cONDiaContabilidadeLabel.TabIndex = 0;
            cONDiaContabilidadeLabel.Text = "De";
            // 
            // cONValorFRLabel
            // 
            cONValorFRLabel.AutoSize = true;
            cONValorFRLabel.Location = new System.Drawing.Point(8, 38);
            cONValorFRLabel.Name = "cONValorFRLabel";
            cONValorFRLabel.Size = new System.Drawing.Size(54, 13);
            cONValorFRLabel.TabIndex = 2;
            cONValorFRLabel.Text = "Balancete";
            // 
            // label1
            // 
            label1.AutoSize = true;
            label1.Location = new System.Drawing.Point(8, 64);
            label1.Name = "label1";
            label1.Size = new System.Drawing.Size(37, 13);
            label1.TabIndex = 6;
            label1.Text = "Boleto";
            // 
            // label2
            // 
            label2.AutoSize = true;
            label2.Location = new System.Drawing.Point(8, 90);
            label2.Name = "label2";
            label2.Size = new System.Drawing.Size(34, 13);
            label2.TabIndex = 7;
            label2.Text = "Anual";
            // 
            // label3
            // 
            label3.AutoSize = true;
            label3.Location = new System.Drawing.Point(380, 38);
            label3.Name = "label3";
            label3.Size = new System.Drawing.Size(34, 13);
            label3.TabIndex = 8;
            label3.Text = "Site 1";
            // 
            // label4
            // 
            label4.AutoSize = true;
            label4.Location = new System.Drawing.Point(380, 64);
            label4.Name = "label4";
            label4.Size = new System.Drawing.Size(34, 13);
            label4.TabIndex = 9;
            label4.Text = "Site 2";
            // 
            // groupControl7
            // 
            this.groupControl7.Controls.Add(this.groupControl1);
            this.groupControl7.Controls.Add(this.groupControl9);
            this.groupControl7.Controls.Add(this.groupControl8);
            this.groupControl7.Dock = System.Windows.Forms.DockStyle.Top;
            this.groupControl7.Location = new System.Drawing.Point(0, 0);
            this.groupControl7.Name = "groupControl7";
            this.groupControl7.ShowCaption = false;
            this.groupControl7.Size = new System.Drawing.Size(1294, 56);
            this.groupControl7.TabIndex = 9;
            this.groupControl7.Text = "groupControl7";
            // 
            // groupControl1
            // 
            this.groupControl1.Controls.Add(this.checkEdit3);
            this.groupControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.groupControl1.Location = new System.Drawing.Point(326, 2);
            this.groupControl1.Name = "groupControl1";
            this.groupControl1.Size = new System.Drawing.Size(966, 52);
            this.groupControl1.TabIndex = 10;
            this.groupControl1.Text = " :: Relatório de Acordos :: ";
            // 
            // checkEdit3
            // 
            this.checkEdit3.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.cONDOMINIOSBindingSource, "CONRelAcordoDet", true));
            this.checkEdit3.Location = new System.Drawing.Point(6, 25);
            this.checkEdit3.Name = "checkEdit3";
            this.checkEdit3.Properties.Caption = "Rel. Acordos Detalhado";
            this.checkEdit3.Size = new System.Drawing.Size(150, 19);
            this.checkEdit3.TabIndex = 3;
            // 
            // cONDOMINIOSBindingSource
            // 
            this.cONDOMINIOSBindingSource.DataMember = "CONDOMINIOS";
            this.cONDOMINIOSBindingSource.DataSource = typeof(CadastrosProc.Condominios.dCondominiosCampos);
            // 
            // groupControl9
            // 
            this.groupControl9.Controls.Add(this.spinEdit1);
            this.groupControl9.Dock = System.Windows.Forms.DockStyle.Left;
            this.groupControl9.Location = new System.Drawing.Point(145, 2);
            this.groupControl9.Name = "groupControl9";
            this.groupControl9.Size = new System.Drawing.Size(181, 52);
            this.groupControl9.TabIndex = 8;
            this.groupControl9.Text = ":: Dia limete para balancete ::";
            // 
            // spinEdit1
            // 
            this.spinEdit1.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.cONDOMINIOSBindingSource, "CONLimiteBal", true));
            this.spinEdit1.EditValue = new decimal(new int[] {
            30,
            0,
            0,
            0});
            this.spinEdit1.Location = new System.Drawing.Point(6, 24);
            this.spinEdit1.Name = "spinEdit1";
            this.spinEdit1.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.spinEdit1.Properties.MaxValue = new decimal(new int[] {
            30,
            0,
            0,
            0});
            this.spinEdit1.Properties.MinValue = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.spinEdit1.Size = new System.Drawing.Size(100, 20);
            this.spinEdit1.TabIndex = 0;
            // 
            // groupControl8
            // 
            this.groupControl8.Controls.Add(this.labelControl1);
            this.groupControl8.Controls.Add(this.txtPerContabilFim1);
            this.groupControl8.Controls.Add(cONDiaContabilidadeLabel);
            this.groupControl8.Controls.Add(this.Inicio);
            this.groupControl8.Dock = System.Windows.Forms.DockStyle.Left;
            this.groupControl8.Location = new System.Drawing.Point(2, 2);
            this.groupControl8.Name = "groupControl8";
            this.groupControl8.Size = new System.Drawing.Size(143, 52);
            this.groupControl8.TabIndex = 7;
            this.groupControl8.Text = ":: Período Contábil ::";
            // 
            // labelControl1
            // 
            this.labelControl1.Location = new System.Drawing.Point(90, 27);
            this.labelControl1.Name = "labelControl1";
            this.labelControl1.Size = new System.Drawing.Size(6, 13);
            this.labelControl1.TabIndex = 3;
            this.labelControl1.Text = "a";
            // 
            // txtPerContabilFim1
            // 
            this.txtPerContabilFim1.EditValue = 31;
            this.txtPerContabilFim1.Location = new System.Drawing.Point(102, 24);
            this.txtPerContabilFim1.Name = "txtPerContabilFim1";
            this.txtPerContabilFim1.Size = new System.Drawing.Size(32, 20);
            this.txtPerContabilFim1.TabIndex = 2;
            // 
            // Inicio
            // 
            this.Inicio.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.cONDOMINIOSBindingSource, "CONDiaContabilidade", true));
            this.Inicio.EditValue = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.Inicio.Location = new System.Drawing.Point(32, 24);
            this.Inicio.Name = "Inicio";
            this.Inicio.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.Inicio.Properties.MaxValue = new decimal(new int[] {
            31,
            0,
            0,
            0});
            this.Inicio.Properties.MinValue = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.Inicio.Size = new System.Drawing.Size(52, 20);
            this.Inicio.TabIndex = 1;
            this.Inicio.TextChanged += new System.EventHandler(this.cONDiaContabilidadeSpinEdit_TextChanged);
            // 
            // memoEdit1
            // 
            this.memoEdit1.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.cONDOMINIOSBindingSource, "CONObsBal", true));
            this.memoEdit1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.memoEdit1.Location = new System.Drawing.Point(2, 20);
            this.memoEdit1.Name = "memoEdit1";
            this.memoEdit1.Size = new System.Drawing.Size(1290, 585);
            this.memoEdit1.TabIndex = 2;
            // 
            // groupControl2
            // 
            this.groupControl2.Controls.Add(this.lookUpEditMBABalsite2);
            this.groupControl2.Controls.Add(this.lookUpEditMBABalsite1);
            this.groupControl2.Controls.Add(label4);
            this.groupControl2.Controls.Add(label3);
            this.groupControl2.Controls.Add(label2);
            this.groupControl2.Controls.Add(label1);
            this.groupControl2.Controls.Add(this.lookUpEditMBAAc);
            this.groupControl2.Controls.Add(this.lookUpEditMBABol);
            this.groupControl2.Controls.Add(this.lookUpEditMBABal);
            this.groupControl2.Controls.Add(cONValorFRLabel);
            this.groupControl2.Dock = System.Windows.Forms.DockStyle.Top;
            this.groupControl2.Location = new System.Drawing.Point(0, 56);
            this.groupControl2.Name = "groupControl2";
            this.groupControl2.Size = new System.Drawing.Size(1294, 132);
            this.groupControl2.TabIndex = 10;
            this.groupControl2.Text = " :: Modelos de Balancete :: ";
            // 
            // lookUpEditMBABalsite2
            // 
            this.lookUpEditMBABalsite2.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.cONDOMINIOSBindingSource, "CONSite2_MBA", true));
            this.lookUpEditMBABalsite2.Location = new System.Drawing.Point(420, 61);
            this.lookUpEditMBABalsite2.Name = "lookUpEditMBABalsite2";
            this.lookUpEditMBABalsite2.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo),
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Delete, "", -1, true, true, false, DevExpress.XtraEditors.ImageLocation.MiddleCenter, null, new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), serializableAppearanceObject1, "Usar Padrão", null, null, true)});
            this.lookUpEditMBABalsite2.Properties.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("MBANome", "MBA Nome", 61, DevExpress.Utils.FormatType.None, "", true, DevExpress.Utils.HorzAlignment.Near)});
            this.lookUpEditMBABalsite2.Properties.DataSource = this.modeloBAlanceteBindingSource;
            this.lookUpEditMBABalsite2.Properties.DisplayMember = "MBANome";
            this.lookUpEditMBABalsite2.Properties.NullText = "Padrão";
            this.lookUpEditMBABalsite2.Properties.ShowHeader = false;
            this.lookUpEditMBABalsite2.Properties.ValueMember = "MBA";
            this.lookUpEditMBABalsite2.Size = new System.Drawing.Size(268, 20);
            this.lookUpEditMBABalsite2.TabIndex = 11;
            this.lookUpEditMBABalsite2.ButtonClick += new DevExpress.XtraEditors.Controls.ButtonPressedEventHandler(this.lookUpEditMBABalsite2_ButtonClick);
            // 
            // modeloBAlanceteBindingSource
            // 
            this.modeloBAlanceteBindingSource.DataMember = "ModeloBAlancete";
            this.modeloBAlanceteBindingSource.DataSource = typeof(CadastrosProc.Condominios.dCondominiosCampos);
            // 
            // lookUpEditMBABalsite1
            // 
            this.lookUpEditMBABalsite1.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.cONDOMINIOSBindingSource, "CONSite1_MBA", true));
            this.lookUpEditMBABalsite1.Location = new System.Drawing.Point(420, 35);
            this.lookUpEditMBABalsite1.Name = "lookUpEditMBABalsite1";
            this.lookUpEditMBABalsite1.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo),
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Delete, "", -1, true, true, false, DevExpress.XtraEditors.ImageLocation.MiddleCenter, null, new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), serializableAppearanceObject2, "Usar Padrão", null, null, true)});
            this.lookUpEditMBABalsite1.Properties.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("MBANome", "MBA Nome", 61, DevExpress.Utils.FormatType.None, "", true, DevExpress.Utils.HorzAlignment.Near)});
            this.lookUpEditMBABalsite1.Properties.DataSource = this.modeloBAlanceteBindingSource;
            this.lookUpEditMBABalsite1.Properties.DisplayMember = "MBANome";
            this.lookUpEditMBABalsite1.Properties.NullText = "Padrão";
            this.lookUpEditMBABalsite1.Properties.ShowHeader = false;
            this.lookUpEditMBABalsite1.Properties.ValueMember = "MBA";
            this.lookUpEditMBABalsite1.Size = new System.Drawing.Size(268, 20);
            this.lookUpEditMBABalsite1.TabIndex = 10;
            this.lookUpEditMBABalsite1.ButtonClick += new DevExpress.XtraEditors.Controls.ButtonPressedEventHandler(this.lookUpEditMBABalsite1_ButtonClick);
            // 
            // lookUpEditMBAAc
            // 
            this.lookUpEditMBAAc.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.cONDOMINIOSBindingSource, "CONAcumulado_MBA", true));
            this.lookUpEditMBAAc.Location = new System.Drawing.Point(92, 87);
            this.lookUpEditMBAAc.Name = "lookUpEditMBAAc";
            this.lookUpEditMBAAc.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo),
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Delete, "", -1, true, true, false, DevExpress.XtraEditors.ImageLocation.MiddleCenter, null, new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), serializableAppearanceObject3, "Usar Padrão", null, null, true)});
            this.lookUpEditMBAAc.Properties.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("MBANome", "MBA Nome", 61, DevExpress.Utils.FormatType.None, "", true, DevExpress.Utils.HorzAlignment.Near)});
            this.lookUpEditMBAAc.Properties.DataSource = this.modeloBAlanceteBindingSource;
            this.lookUpEditMBAAc.Properties.DisplayMember = "MBANome";
            this.lookUpEditMBAAc.Properties.NullText = "Padrão";
            this.lookUpEditMBAAc.Properties.ShowHeader = false;
            this.lookUpEditMBAAc.Properties.ValueMember = "MBA";
            this.lookUpEditMBAAc.Size = new System.Drawing.Size(268, 20);
            this.lookUpEditMBAAc.TabIndex = 5;
            this.lookUpEditMBAAc.ButtonClick += new DevExpress.XtraEditors.Controls.ButtonPressedEventHandler(this.lookUpEditMBAAc_ButtonClick);
            // 
            // lookUpEditMBABol
            // 
            this.lookUpEditMBABol.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.cONDOMINIOSBindingSource, "CONBoleto_MBA", true));
            this.lookUpEditMBABol.Location = new System.Drawing.Point(92, 61);
            this.lookUpEditMBABol.Name = "lookUpEditMBABol";
            this.lookUpEditMBABol.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo),
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Delete, "", -1, true, true, false, DevExpress.XtraEditors.ImageLocation.MiddleCenter, null, new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), serializableAppearanceObject4, "Usar Padrão", null, null, true)});
            this.lookUpEditMBABol.Properties.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("MBANome", "MBA Nome", 61, DevExpress.Utils.FormatType.None, "", true, DevExpress.Utils.HorzAlignment.Near)});
            this.lookUpEditMBABol.Properties.DataSource = this.modeloBAlanceteBindingSource;
            this.lookUpEditMBABol.Properties.DisplayMember = "MBANome";
            this.lookUpEditMBABol.Properties.NullText = "Padrão";
            this.lookUpEditMBABol.Properties.ShowHeader = false;
            this.lookUpEditMBABol.Properties.ValueMember = "MBA";
            this.lookUpEditMBABol.Size = new System.Drawing.Size(268, 20);
            this.lookUpEditMBABol.TabIndex = 4;
            this.lookUpEditMBABol.ButtonClick += new DevExpress.XtraEditors.Controls.ButtonPressedEventHandler(this.lookUpEditMBABol_ButtonClick);
            // 
            // lookUpEditMBABal
            // 
            this.lookUpEditMBABal.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.cONDOMINIOSBindingSource, "CONBalancete_MBA", true));
            this.lookUpEditMBABal.Location = new System.Drawing.Point(92, 35);
            this.lookUpEditMBABal.Name = "lookUpEditMBABal";
            this.lookUpEditMBABal.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo),
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Delete, "", -1, true, true, false, DevExpress.XtraEditors.ImageLocation.MiddleCenter, null, new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), serializableAppearanceObject5, "Usar Padrão", null, null, true)});
            this.lookUpEditMBABal.Properties.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("MBANome", "MBA Nome", 61, DevExpress.Utils.FormatType.None, "", true, DevExpress.Utils.HorzAlignment.Near)});
            this.lookUpEditMBABal.Properties.DataSource = this.modeloBAlanceteBindingSource;
            this.lookUpEditMBABal.Properties.DisplayMember = "MBANome";
            this.lookUpEditMBABal.Properties.NullText = "Padrão";
            this.lookUpEditMBABal.Properties.ShowHeader = false;
            this.lookUpEditMBABal.Properties.ValueMember = "MBA";
            this.lookUpEditMBABal.Size = new System.Drawing.Size(268, 20);
            this.lookUpEditMBABal.TabIndex = 3;
            this.lookUpEditMBABal.ButtonClick += new DevExpress.XtraEditors.Controls.ButtonPressedEventHandler(this.lookUpEditMBABal_ButtonClick);
            // 
            // groupControl3
            // 
            this.groupControl3.Controls.Add(this.memoEdit1);
            this.groupControl3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.groupControl3.Location = new System.Drawing.Point(0, 188);
            this.groupControl3.Name = "groupControl3";
            this.groupControl3.Size = new System.Drawing.Size(1294, 607);
            this.groupControl3.TabIndex = 11;
            this.groupControl3.Text = " :: obs para confecção de balancetes :: ";
            // 
            // TabBalancete
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.groupControl3);
            this.Controls.Add(this.groupControl2);
            this.Controls.Add(this.groupControl7);
            this.Doc = System.Windows.Forms.DockStyle.Fill;
            this.LookAndFeel.UseDefaultLookAndFeel = false;
            this.Name = "TabBalancete";
            this.Size = new System.Drawing.Size(1294, 795);
            this.Load += new System.EventHandler(this.TabBalancete_Load);
            ((System.ComponentModel.ISupportInitialize)(this.StyleController_F)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl7)).EndInit();
            this.groupControl7.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.groupControl1)).EndInit();
            this.groupControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit3.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cONDOMINIOSBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl9)).EndInit();
            this.groupControl9.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.spinEdit1.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl8)).EndInit();
            this.groupControl8.ResumeLayout(false);
            this.groupControl8.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtPerContabilFim1.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Inicio.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.memoEdit1.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl2)).EndInit();
            this.groupControl2.ResumeLayout(false);
            this.groupControl2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.lookUpEditMBABalsite2.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.modeloBAlanceteBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lookUpEditMBABalsite1.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lookUpEditMBAAc.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lookUpEditMBABol.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lookUpEditMBABal.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl3)).EndInit();
            this.groupControl3.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraEditors.GroupControl groupControl7;
        private DevExpress.XtraEditors.GroupControl groupControl9;
        private DevExpress.XtraEditors.SpinEdit spinEdit1;
        private DevExpress.XtraEditors.GroupControl groupControl8;
        private DevExpress.XtraEditors.LabelControl labelControl1;
        private DevExpress.XtraEditors.TextEdit txtPerContabilFim1;
        private DevExpress.XtraEditors.SpinEdit Inicio;
        private DevExpress.XtraEditors.CheckEdit checkEdit3;
        private DevExpress.XtraEditors.MemoEdit memoEdit1;
        private System.Windows.Forms.BindingSource cONDOMINIOSBindingSource;
        private DevExpress.XtraEditors.GroupControl groupControl1;
        private DevExpress.XtraEditors.GroupControl groupControl2;
        private DevExpress.XtraEditors.GroupControl groupControl3;
        private DevExpress.XtraEditors.LookUpEdit lookUpEditMBABal;
        private System.Windows.Forms.BindingSource modeloBAlanceteBindingSource;
        private DevExpress.XtraEditors.LookUpEdit lookUpEditMBAAc;
        private DevExpress.XtraEditors.LookUpEdit lookUpEditMBABol;
        private DevExpress.XtraEditors.LookUpEdit lookUpEditMBABalsite2;
        private DevExpress.XtraEditors.LookUpEdit lookUpEditMBABalsite1;

    }
}
