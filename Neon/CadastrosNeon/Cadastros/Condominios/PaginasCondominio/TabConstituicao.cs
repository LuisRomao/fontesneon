using System;
using System.Collections.Generic;
using System.Data;
using System.Windows.Forms;
using CompontesBasicosProc;
using CompontesBasicos;
using DevExpress.XtraGrid.Views.Grid;
using VirEnumeracoesNeon;
using CadastrosProc.Condominios;

namespace Cadastros.Condominios.PaginasCondominio
{
    /// <summary>
    /// 
    /// </summary>
    public partial class TabConstituicao : CompontesBasicos.ComponenteTabCampos
    {
        

        /// <summary>
        /// Construtor
        /// </summary>
        public TabConstituicao()
        {
            InitializeComponent();
            //pESSOASBindingSource.DataSource = Estatica.EdEstatico;
            //iMOBILIARIASBindingSource.DataSource = Fornecedores.dFornecedoresGrade.GetdFornecedoresGradeStd("IMO");
            
            button1.Visible = FormPrincipalBase.USULogado == 30;
            Framework.Enumeracoes.virEnumAPTTipo.virEnumAPTTipoSt.CarregaEditorDaGrid(colAPTTipo);
        }

        private cCondominiosCampos2 TabMaeC {
            get {
                return (cCondominiosCampos2)TabMae;
            }
        }
     

        private void repositoryItemButtonEdit1_ButtonClick(object sender, DevExpress.XtraEditors.Controls.ButtonPressedEventArgs e)
        {
            if (LinhaMae.CONStatus == 1)
                return;
            int napartamentos = GridBlocos.GetDataRow(GridBlocos.FocusedRowHandle).GetChildRows("FK_APARTAMENTOS_BLOCOS").Length;

            string mensagem;
            if (napartamentos > 1)
                mensagem = "Confirma a exclus�o do bloco selecionado?\r\nAo excluir um bloco, todos os " + napartamentos.ToString() + " apartamentos vinculados tamb�m ser�o exclu�dos.";
            else
                mensagem = "Confirma a exclus�o do bloco selecionado?\r\nAo excluir um bloco, o apartamentos vinculado tamb�m sera exclu�do.";
            if ((napartamentos == 0)
                ||
                (DialogResult.Yes == MessageBox.Show(mensagem,"Excluir Bloco", MessageBoxButtons.YesNo, MessageBoxIcon.Question)))
            {
                dCondominiosCampos.BLOCOSRow rowBLO = (dCondominiosCampos.BLOCOSRow)GridBlocos.GetFocusedDataRow();
                bool Apagoutodos = true;
                foreach (dCondominiosCampos.APARTAMENTOSRow rowAPT in rowBLO.GetAPARTAMENTOSRows())
                {
                    if (!ApagaLinha(rowAPT))
                        Apagoutodos = false;
                }
                if (Apagoutodos)
                {
                    rowBLO.Delete();
                    TabMaeC.bLOCOSTableAdapter.Update(rowBLO);
                    TabMaeC.dCondominiosCampos.BLOCOS.AcceptChanges();
                }
                //GridBlocos.DeleteSelectedRows();
                LinhaMae.CONTotalBloco = LinhaMae.IsCONTotalBlocoNull() ? (short)0 : (short)(LinhaMae.CONTotalBloco - 1);
            }
        }

        private void GridBlocos_ValidateRow(object sender, DevExpress.XtraGrid.Views.Base.ValidateRowEventArgs e)
        {
            DataRowView DRV = (DataRowView)e.Row;
            
            if (DRV["BLOCodigo"].ToString() == ""){
                e.ErrorText = "Campo c�digo obrigatorio\r\n";
                e.Valid = false;
                GridBlocos.FocusedColumn = GridBlocos.Columns["BLOCodigo"];
            }

            if (DRV["BLONome"].ToString() == "")
            {
                e.ErrorText = "Campo nome obrigat�rio\r\n";
                e.Valid = false;
                GridBlocos.FocusedColumn = GridBlocos.Columns["BLONome"];
            };
            
        }

        private void GridBlocos_ValidatingEditor(object sender, DevExpress.XtraEditors.Controls.BaseContainerValidateEditorEventArgs e)
        {
            if (GridBlocos.FocusedColumn.FieldName == "BLOCodigo") 
            {                                
                if (e.Value.ToString() == "")
                {
                    e.ErrorText = "C�digo inv�lido";
                    e.Valid = false;
                }
                else
                {
                    int loc = GridBlocos.LocateByValue(0,GridBlocos.FocusedColumn,e.Value.ToString());
                    if ((loc >= 0) && (loc != GridBlocos.FocusedRowHandle))
                    {                        
                        e.ErrorText = "C�digo Duplicado";
                        e.Valid = false;
                    }                
                };
            };

            if (GridBlocos.FocusedColumn.FieldName == "BLONome")
            {
                if (e.Value.ToString() == "")
                {
                    e.ErrorText = "Nome do bloco inv�lido";
                    e.Valid = false;
                };
            };

        }        
        
        private string Ultimocpf = "";

        //private Pessoas.cPessoasCampos _EditorPes;

        //private Pessoas.cPessoasCampos EditorPes { get { return _EditorPes == null ? _EditorPes = new Pessoas.cPessoasCampos() : _EditorPes; }}

        private void LupPessoas_ButtonClick(object sender, DevExpress.XtraEditors.Controls.ButtonPressedEventArgs e)
        {
            GridView GV = (GridView)bLOCOSGridControl.FocusedView;
            if (GV == null)
                return;
            DataRowView DRV = (DataRowView)GV.GetRow(GV.FocusedRowHandle);
            if ((DRV == null) || (DRV.Row == null))
                return;
            dCondominiosCampos.APARTAMENTOSRow rowAPT = (dCondominiosCampos.APARTAMENTOSRow)DRV.Row;
            string CampoFPG = (GV.FocusedColumn.FieldName == "APTProprietario_PES") ?"APTFormaPagtoProprietario_FPG":"APTFormaPagtoInquilino_FPG";                         
            DevExpress.XtraEditors.LookUpEdit Loo = (DevExpress.XtraEditors.LookUpEdit)sender;
            switch(e.Button.Kind){
                case  DevExpress.XtraEditors.Controls.ButtonPredefines.Plus:
                    if (VirInput.Input.Execute("CPF/CNPJ", ref Ultimocpf, false))
                    {
                        if (Ultimocpf.EstaNoGrupo("","0"))
                        {
                            if (MessageBox.Show("N�O SER�O EMITIDOS BOLETOS PARA ESTA UNIDADE ENQUANTO N�O FOR CADASTRADO O CPF\r\nCadastrar sem CPF", "ATEN��O", MessageBoxButtons.YesNo, MessageBoxIcon.Warning, MessageBoxDefaultButton.Button2) == DialogResult.Yes)
                                Ultimocpf = "0";
                        }
                        DocBacarios.CPFCNPJ novoCPF = new DocBacarios.CPFCNPJ(Ultimocpf);
                        if ((novoCPF.Tipo == DocBacarios.TipoCpfCnpj.INVALIDO) && (novoCPF.Valor != 0))
                        {
                            MessageBox.Show("CPF/CNPJ inv�lido");
                            return;
                        }
                        else
                        {
                            Pessoas.cPessoasCampos EditorPes = new Pessoas.cPessoasCampos();
                            if ((novoCPF.Tipo == DocBacarios.TipoCpfCnpj.INVALIDO) || ((Pessoas.dPessoasCampos.PESSOASTableAdapter.FillByCPFCNPJ(EditorPes.dPessoasCampos.PESSOAS, novoCPF.ToString()) == 0)))
                            {
                                Pessoas.dPessoasCampos.PESSOASRow novarow = EditorPes.dPessoasCampos.PESSOAS.NewPESSOASRow();
                                novarow.PESEndereco = LinhaMae.CONEndereco;
                                novarow.PES_CID = LinhaMae.CON_CID;
                                if (!LinhaMae.IsCONCepNull())
                                    novarow.PESCep = LinhaMae.CONCep;
                                novarow.PESBairro = LinhaMae.CONBairro;
                                novarow.PESNome = "";
                                novarow.PESEmail = novarow.PESFone1 = novarow.PESFone2 = novarow.PESFone3 = novarow.PESFone4 = "";
                                if (novoCPF.Tipo != DocBacarios.TipoCpfCnpj.INVALIDO)
                                {
                                    novarow.PESCpfCnpj = novoCPF.ToString();
                                    novarow.PESTipo = (novoCPF.Tipo == DocBacarios.TipoCpfCnpj.CPF ? "F" : "J");
                                };
                                novarow.PESCpfCnpjViaSite = false;
                                novarow.PESCpfErrado = false;
                                EditorPes.dPessoasCampos.PESSOAS.AddPESSOASRow(novarow);
                                EditorPes.tipoDeCarga = TipoDeCarga.navegacao;
                                if (EditorPes.VirShowModulo(EstadosDosComponentes.PopUp) == DialogResult.OK)
                                {
                                    TabMaeC.IncluiPESloo(EditorPes.pk, novarow.PESNome);                                    
                                    Loo.EditValue = EditorPes.pk;
                                    rowAPT[CampoFPG] = 4;                                    
                                }
                            }
                            else
                            {
                                TabMaeC.IncluiPESloo(EditorPes.dPessoasCampos.PESSOAS[0].PES, EditorPes.dPessoasCampos.PESSOAS[0].PESNome);
                                Loo.EditValue = EditorPes.dPessoasCampos.PESSOAS[0].PES;
                                rowAPT[CampoFPG] = 4;                                
                            };                            
                        }
                    }                        
                    break;
                case  DevExpress.XtraEditors.Controls.ButtonPredefines.Ellipsis:
                    if ((Loo.EditValue != null) && (Loo.EditValue != DBNull.Value))
                    {
                        int PES = (int)Loo.EditValue;
                        Pessoas.cPessoasCampos EditorPes = new Pessoas.cPessoasCampos();
                        EditorPes.Fill(TipoDeCarga.pk, PES, false);
                        if (EditorPes.VirShowModulo(EstadosDosComponentes.PopUp) == DialogResult.OK)
                            TabMaeC.IncluiPESloo(EditorPes.dPessoasCampos.PESSOAS[0].PES, EditorPes.dPessoasCampos.PESSOAS[0].PESNome);
                    };                         
                    break;
                case DevExpress.XtraEditors.Controls.ButtonPredefines.Minus:
                    Loo.EditValue = null;
                    rowAPT[CampoFPG] = DBNull.Value;
                    break;
            }
        }
        /*
        private void IncluiPESloo(int PES,string PESNome)
        {
            dCondominiosCampos.PESloockRow NovoPES;
            NovoPES = TabMaeC.dCondominiosCampos.PESloock.FindByPES(PES);
            if (NovoPES == null)
            {
                NovoPES = TabMaeC.dCondominiosCampos.PESloock.NewPESloockRow();
                NovoPES.PES = PES;
                NovoPES.PESNome = PESNome;
                TabMaeC.dCondominiosCampos.PESloock.AddPESloockRow(NovoPES);
            }
            else
                NovoPES.PESNome = PESNome;
        }*/

        private bool ApagaLinha(dCondominiosCampos.APARTAMENTOSRow rowAPT)
        {
            try
            {
                rowAPT.Delete();
                TabMaeC.aPARTAMENTOSTableAdapter.Update(rowAPT);
                TabMaeC.dCondominiosCampos.APARTAMENTOS.AcceptChanges();
                return true;
            }
            catch
            {
                TabMaeC.dCondominiosCampos.APARTAMENTOS.RejectChanges();
                return false;
            }
        }

        private void BotoesApartamento_ButtonClick(object sender, DevExpress.XtraEditors.Controls.ButtonPressedEventArgs e)
        {            
            DevExpress.XtraGrid.Views.Grid.GridView GV = (DevExpress.XtraGrid.Views.Grid.GridView)bLOCOSGridControl.FocusedView;
            switch(e.Button.Kind){
                case DevExpress.XtraEditors.Controls.ButtonPredefines.Delete:
                    if (LinhaMae.CONStatus == 1)
                        return;
                    if (DialogResult.Yes == MessageBox.Show("Confirma a exclus�o do apartamento selecionado?", "Excluir Apartamento", MessageBoxButtons.YesNo, MessageBoxIcon.Question))
                    {
                        //DataRowView DRV = (DataRowView)GV.SelectRow;
                        //dCondominiosCampos.APARTAMENTOSRow rowAPT = (dCondominiosCampos.APARTAMENTOSRow)DRV.Row;
                        dCondominiosCampos.APARTAMENTOSRow rowAPT = (dCondominiosCampos.APARTAMENTOSRow)GV.GetFocusedDataRow();
                        if (rowAPT == null)
                            return;
                        rowAPT.BLOCOSRow.BLOTotalApartamento = (short)(rowAPT.BLOCOSRow.BLOTotalApartamento - 1);
                        rowAPT.BLOCOSRow.CONDOMINIOSRow.CONTotalApartamento = (short)(rowAPT.BLOCOSRow.CONDOMINIOSRow.CONTotalApartamento - 1);
                        //GV.DeleteSelectedRows();
                        ApagaLinha(rowAPT);
                    }
                    break;                                                   
                case DevExpress.XtraEditors.Controls.ButtonPredefines.Ellipsis:
                    dCondominiosCampos.APARTAMENTOSRow APTrow = (dCondominiosCampos.APARTAMENTOSRow)GV.GetFocusedDataRow();                    
                    CompontesBasicos.FormPrincipalBase.FormPrincipal.ShowMoludoCampos(typeof(Cadastros.Apartamentos.cApartamentosCampos), "", APTrow.APT, false, null,CompontesBasicos.EstadosDosComponentes.PopUp);
                        TabMaeC.CarregaAPTs();
                    //cApartamentos cadAPT = new cApartamentos();
                    //cadAPT.Fill(CompontesBasicos.TipoDeCarga.pk, APTrow.APT, false);
                    //if (cadAPT.VirShowModulo(CompontesBasicos.EstadosDosComponentes.PopUp) == DialogResult.OK)
                    //    TabMaeC.CarregaAPTs();
                    
                    //Cadastros.Apartamentos.cApartamentosCampos cadAPT = new Cadastros.Apartamentos.cApartamentosCampos();
                    //cadAPT.Fill(CompontesBasicos.TipoDeCarga.pk, APTrow.APT, false);
                    //if (cadAPT.VirShowModulo(CompontesBasicos.EstadosDosComponentes.PopUp) == DialogResult.OK)
                    //    TabMaeC.CarregaAPTs();
                    //return;                    
                    break;
            }
        }
        
        private void ValigaCamposAP(object sender, DevExpress.XtraEditors.Controls.BaseContainerValidateEditorEventArgs e)
        {
            GridView GV = (DevExpress.XtraGrid.Views.Grid.GridView)bLOCOSGridControl.FocusedView;
            if (GV.FocusedColumn.FieldName == "APTNumero")
            {
                if (e.Value.ToString() == "")
                {
                    e.ErrorText = "C�digo inv�lido";
                    e.Valid = false;
                }
                else
                {
                    int loc = GV.LocateByValue(0, GV.FocusedColumn, e.Value.ToString());
                    if (loc >= 0)
                    {
                        e.ErrorText = "C�digo Duplicado";
                        e.Valid = false;
                    }
                };
                
            };
            
        }

        private void ValidaLinhaAP(object sender, DevExpress.XtraGrid.Views.Base.ValidateRowEventArgs e)
        {
            DataRowView DRV = (DataRowView)e.Row;
            GridView GV = (DevExpress.XtraGrid.Views.Grid.GridView)bLOCOSGridControl.FocusedView;
            if (DRV["APTNumero"].ToString() == "")
            {
                e.ErrorText = "Campo n�mero obrigat�rio\r\n";
                e.Valid = false;
                GV.FocusedColumn = GridBlocos.Columns["APTCodigo"];
            }
            dCondominiosCampos.APARTAMENTOSRow APTrow = (dCondominiosCampos.APARTAMENTOSRow)DRV.Row;                        
        }

        dCondominiosCampos.CONDOMINIOSRow LinhaMae {
            get {
                if (TabMae.LinhaMae_F == null)
                    return null;
                else
                    return (dCondominiosCampos.CONDOMINIOSRow)TabMae.LinhaMae_F;
            }
        }
        
        private void FormaPag_EditValueChanged(object sender, EventArgs e)
        {
            
            
            DevExpress.XtraEditors.ImageComboBoxEdit Editor = (DevExpress.XtraEditors.ImageComboBoxEdit)sender;

            if ((LinhaMae == null) || (Editor.EditValue == null))
                return;
            DevExpress.XtraGrid.Views.Grid.GridView GV = (DevExpress.XtraGrid.Views.Grid.GridView)bLOCOSGridControl.FocusedView;
            DataRowView DRV = (DataRowView)GV.GetRow(GV.FocusedRowHandle);
            dCondominiosCampos.APARTAMENTOSRow rowAPT = (dCondominiosCampos.APARTAMENTOSRow)DRV.Row;
            int PES;
            if (GV.FocusedColumn.FieldName == "APTFormaPagtoProprietario_FPG")
            {
                if (rowAPT.IsAPTProprietario_PESNull())
                    return;
                PES = rowAPT.APTProprietario_PES;
            }
            else
            {
                if (rowAPT.IsAPTInquilino_PESNull())
                    return;
                PES = rowAPT.APTInquilino_PES;
            }
            int FPG = (int)Editor.EditValue;
            dEstatico.PESSOASRow PessoaEditadaRow = Estatica.EdEstatico.PESSOAS.FindByPES(PES);
            switch (FPG) { 
                case 2:
                    if ((PessoaEditadaRow["PESEndereco"].ToString() == "")
                        ||
                        (LinhaMae.CONEndereco == PessoaEditadaRow["PESEndereco"].ToString()))
                    {
                        Pessoas.cPessoasCampos EditorPes = new Pessoas.cPessoasCampos();
                        EditorPes.Fill(CompontesBasicos.TipoDeCarga.pk, PES, false);
                        //if (TabMae.ShowModulo(EditorPes) == DialogResult.OK)
                        if (EditorPes.VirShowModulo(CompontesBasicos.EstadosDosComponentes.PopUp) == DialogResult.OK)
                        {
                            //rowAPT.EndEdit();
                            GV.CloseEditor();
                            GV.RefreshData();
                            //Estatica.EAdapPessoas.Fill(Estatica.EdEstatico.PESSOAS);
                        }
                    }
                    break;
                case 4://entrega no condominio
                    //if (LinhaMae.CONEndereco != PessoaEditadaRow["PESEndereco"].ToString())

                    //    if ((PessoaEditadaRow["PESEndereco"].ToString() == "")
                    //       ||
                    //        (MessageBox.Show("Alterar o endere�o do propriet�rio para o ender�o do pr�dio?", "Confirma��o", MessageBoxButtons.YesNo) == DialogResult.Yes))
                    //    {
                    //        PessoaEditadaRow.PESEndereco = LinhaMae.CONEndereco;
                    //        PessoaEditadaRow.PESBairro = LinhaMae.CONBairro;
                    //        if (!LinhaMae.IsCONCepNull())
                    //            PessoaEditadaRow.PESCep = LinhaMae.CONCep;
                    //        PessoaEditadaRow.PES_CID = LinhaMae.CON_CID;
                    //        PessoaEditadaRow.EndEdit();
                    //        Estatica.EAdapPessoas.Update(PessoaEditadaRow);
                    //        PessoaEditadaRow.AcceptChanges();
                    //    }
                    break;
            };
            
            
        }

        /*
        private Cadastros.Utilitarios.MSAccess.fExportToMsAccess _Export;

        private Cadastros.Utilitarios.MSAccess.fExportToMsAccess Export{
            get{
                if(_Export == null)
                    _Export = new Cadastros.Utilitarios.MSAccess.fExportToMsAccess();
                 return _Export;
            }
        }
        */

        private void GridApartamentos_RowUpdated(object sender, DevExpress.XtraGrid.Views.Base.RowObjectEventArgs e)
        {
            
            DataRowView DRV = (DataRowView)e.Row;
            if(DRV.Row != null){
                dCondominiosCampos.APARTAMENTOSRow APTrow = (dCondominiosCampos.APARTAMENTOSRow)DRV.Row;
                if (APTrow.RowState == DataRowState.Added)
                {
                    APTrow.BLOCOSRow.BLOTotalApartamento = APTrow.BLOCOSRow.IsBLOTotalApartamentoNull() ? (short)1 : (short)(APTrow.BLOCOSRow.BLOTotalApartamento + 1);
                    APTrow.BLOCOSRow.CONDOMINIOSRow.CONTotalApartamento = (short)(APTrow.BLOCOSRow.CONDOMINIOSRow.CONTotalApartamento + 1);
                }
                TabMaeC.aPARTAMENTOSTableAdapter.Update(APTrow);
                APTrow.AcceptChanges();
                //TabMaeC.ExportarAccess = true;
                

                //Export.ExportarApartamentosParaMsAccessRow(LinhaMae.CONCodigo, APTrow.BLOCOSRow.BLOCodigo, APTrow);
                Int32 Codigoforpaga = 4;
                if (!APTrow.IsAPTProprietario_PESNull())
                {
                    if (!APTrow.IsAPTFormaPagtoProprietario_FPGNull())
                        Codigoforpaga = APTrow.APTFormaPagtoProprietario_FPG;
                    else
                        APTrow.APTFormaPagtoProprietario_FPG = 4;
                    //Export.ExportarInquilinoProprietarioParaMsAccess(true, APTrow.APTProprietario_PES, LinhaMae.CONCodigo, APTrow.BLOCOSRow.BLOCodigo, APTrow.APTNumero, Codigoforpaga);
                }
                if (!APTrow.IsAPTInquilino_PESNull())
                {
                    if (!APTrow.IsAPTFormaPagtoInquilino_FPGNull())
                        Codigoforpaga = APTrow.APTFormaPagtoInquilino_FPG;
                    else
                        APTrow.APTFormaPagtoInquilino_FPG = 4;
                    //Export.ExportarInquilinoProprietarioParaMsAccess(false, APTrow.APTInquilino_PES, LinhaMae.CONCodigo, APTrow.BLOCOSRow.BLOCodigo, APTrow.APTNumero, Codigoforpaga);
                }
                //else
                //    Cadastros.Utilitarios.MSAccess.fExportToMsAccess.ApagaInquilinoDoAccess(LinhaMae.CONCodigo, APTrow.BLOCOSRow.BLOCodigo, APTrow.APTNumero);
                TabMaeC.aPARTAMENTOSTableAdapter.Update(APTrow);
                APTrow.AcceptChanges();
                SomaFracao();
            }
        }

        private void GridBlocos_RowUpdated(object sender, DevExpress.XtraGrid.Views.Base.RowObjectEventArgs e)
        {
            DataRowView DRV = (DataRowView)e.Row;
            if (DRV.Row != null)
            {
                dCondominiosCampos.BLOCOSRow BLOrow = (dCondominiosCampos.BLOCOSRow)DRV.Row;
                if (BLOrow.RowState == DataRowState.Added)
                {
                    short unidade = 1;
                    LinhaMae.CONTotalBloco = LinhaMae.IsCONTotalBlocoNull() ? unidade : (short)(LinhaMae.CONTotalBloco + unidade);
                }
                TabMaeC.bLOCOSTableAdapter.Update(BLOrow);
                BLOrow.AcceptChanges();
            }
        }

        private void simpleButton1_Click(object sender, EventArgs e)
        {
            CompontesBasicos.ComponenteBase CB = (CompontesBasicos.ComponenteBase)CompontesBasicos.ModuleInfo.ContruaObjeto("Boletos.Rateios.FracaoManual");
            CB.ChamadaGenerica("Simulador", LinhaMae.CON, LinhaMae.CONCodigo, LinhaMae.CONNome, LinhaMae.CONFracaoIdeal);
            //Boletos.Rateios.FracaoManual FM = new Boletos.Rateios.FracaoManual();
            //FM.DRateio = new Boletos.Rateios.dRateios();
            //Boletos.Rateios.dRateios.ListaFracaoDataTable TabLista = Boletos.Rateios.dRateios.ListaFracaoTableAdapter.GetData(LinhaMae.CON);
            //FM.Tabela = TabLista;
            //FM.listaFracaoDataTableBindingSource.DataSource = TabLista;
            //FM.Titulo = string.Format("Simulador - {0} {1}",LinhaMae.CONCodigo,LinhaMae.CONNome);
            //FM.chUsarFracao.Checked = LinhaMae.CONFracaoIdeal;
            //FM.VirShowModulo(CompontesBasicos.EstadosDosComponentes.JanelasAtivas);
        }        

        private void SomaFracao()
        {                        
            TotFracao.Text = string.Format("{0:n5} %", TabMaeC.dCondominiosCampos.SomaFracao());            
        }

        private void TabConstituicao_Load(object sender, EventArgs e)
        {
            simpleButton1.Enabled = true;
            SomaFracao();
            iMOBILIARIASBindingSource.DataSource = TabMaeC.DFornecedoresLookup;
            pESSOASBindingSource.DataSource = TabMaeC.dCondominiosCampos;
            TabMaeC.dCondominiosCampos.PESloockTableAdapter.Fill(TabMaeC.dCondominiosCampos.PESloock, LinhaMae.CON);
        }

        private void simpleButton2_Click(object sender, EventArgs e)
        {
            if (LinhaMae.CONCodigo != "VILLEF")
                return;
            Cadastros.Condominios.Ferramentas.cFusao cFusao1 = new Ferramentas.cFusao(LinhaMae.CON);
            if (cFusao1.VirShowModulo(CompontesBasicos.EstadosDosComponentes.PopUp) == DialogResult.OK)
            {
                MessageBox.Show("ok");
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            DataRow DR = VirMSSQL.TableAdapter.STTableAdapter.BuscaSQLRow("select * from FORMAPAGTO where FPG = 2");
            if (!VirMSSQL.TableAdapter.STTableAdapter.EstaCadastrado("select FPG from FORMAPAGTO where FPG = 12"))
            {
                DR[0] = 12;
                DR[1] = "Correio + e-mail";
                VirMSSQL.TableAdapter.STTableAdapter.Insert("FORMAPAGTO", DR, true, false);
            }
            if (!VirMSSQL.TableAdapter.STTableAdapter.EstaCadastrado("select FPG from FORMAPAGTO where FPG = 14"))
            {
                DR[0] = 14;
                DR[1] = "Condom�nio + e-mail";
                VirMSSQL.TableAdapter.STTableAdapter.Insert("FORMAPAGTO", DR, true, false);
            }
            if (!VirMSSQL.TableAdapter.STTableAdapter.EstaCadastrado("select FPG from FORMAPAGTO where FPG = 22"))
            {
                DR[0] = 22;
                DR[1] = "e-mail (Documentos por Correito)";
                VirMSSQL.TableAdapter.STTableAdapter.Insert("FORMAPAGTO", DR, true, false);
            }
            if (!VirMSSQL.TableAdapter.STTableAdapter.EstaCadastrado("select FPG from FORMAPAGTO where FPG = 24"))
            {
                DR[0] = 24;
                DR[1] = "e-mail (Documentos no Condom�nio)";
                VirMSSQL.TableAdapter.STTableAdapter.Insert("FORMAPAGTO", DR, true, false);
            }                        
        }

        private void FormaPag_EditValueChanging(object sender, DevExpress.XtraEditors.Controls.ChangingEventArgs e)
        {
            FormaPagamento NewFP = (FormaPagamento)e.NewValue;
            FormaPagamento OldFP = (e.OldValue == null) ? FormaPagamento.EntreigaPessoal : (FormaPagamento)e.OldValue;
            if (
                NewFP.EstaNoGrupo(FormaPagamento.email_Correio, FormaPagamento.email_EntreigaPessoal)
              &&
                !OldFP.EstaNoGrupo(FormaPagamento.email_Correio, FormaPagamento.email_EntreigaPessoal)
              )
            {
                MessageBox.Show("Esta forma de pagamento s� pode ser cadastrada por e-mail");
                e.Cancel = true;
            }
        }

        private void GridApartamentos_InitNewRow(object sender, DevExpress.XtraGrid.Views.Grid.InitNewRowEventArgs e)
        {
            GridView view = (GridView)sender;            
            dCondominiosCampos.APARTAMENTOSRow APTrow = (dCondominiosCampos.APARTAMENTOSRow)view.GetDataRow(e.RowHandle);
            if (APTrow != null)
            {
                APTrow.APTSeguro = false;
                APTrow.APTFormaPagtoInquilino_FPG = 4;
                APTrow.APTFormaPagtoProprietario_FPG = 4;
                APTrow.APTTipo = (int)APTTipo.Apartamento;
            }
        }

        private void LookUpEditImobiliaria_PopupFilter(object sender, DevExpress.XtraEditors.Controls.PopupFilterEventArgs e)
        {
            e.Criteria = DevExpress.Data.Filtering.CriteriaOperator.Parse("RAV = ?", (int)RAVPadrao.Imobiliaria);
        }

        private void GridApartamentos_RowCellStyle(object sender, RowCellStyleEventArgs e)
        {
            if (e.Column.Name == colAPTTipo.Name)
            {
                if (e.RowHandle < 0)
                    return;
                GridView view = (GridView)sender;
                dCondominiosCampos.APARTAMENTOSRow APTrow = (dCondominiosCampos.APARTAMENTOSRow)view.GetDataRow(e.RowHandle);
                if (APTrow != null)                   
                    Framework.Enumeracoes.virEnumAPTTipo.virEnumAPTTipoSt.CellStyle(e.CellValue, e);
            }
        }
    }
}

