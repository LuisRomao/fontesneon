namespace Cadastros.Condominios.PaginasCondominio
{
    partial class TabHistorico
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(TabHistorico));
            this.hISTORICOSBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.hISTORICOSGridControl = new DevExpress.XtraGrid.GridControl();
            this.layoutView1 = new DevExpress.XtraGrid.Views.Layout.LayoutView();
            this.colHSTAssunto = new DevExpress.XtraGrid.Columns.LayoutViewColumn();
            this.LookUpEditASSUNTO = new DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit();
            this.ASSuntobindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.Item1 = new DevExpress.XtraGrid.Views.Layout.LayoutViewField();
            this.colHSTDescricao = new DevExpress.XtraGrid.Columns.LayoutViewColumn();
            this.repositoryItemMemoEdit1 = new DevExpress.XtraEditors.Repository.RepositoryItemMemoEdit();
            this.Item2 = new DevExpress.XtraGrid.Views.Layout.LayoutViewField();
            this.colHSTCritico = new DevExpress.XtraGrid.Columns.LayoutViewColumn();
            this.Item5 = new DevExpress.XtraGrid.Views.Layout.LayoutViewField();
            this.colHSTData = new DevExpress.XtraGrid.Columns.LayoutViewColumn();
            this.Item3 = new DevExpress.XtraGrid.Views.Layout.LayoutViewField();
            this.colHSTI_USU = new DevExpress.XtraGrid.Columns.LayoutViewColumn();
            this.LookUpResp = new DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit();
            this.UsuariosBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.layoutViewField_colHSTI_USU = new DevExpress.XtraGrid.Views.Layout.LayoutViewField();
            this.layoutViewCard1 = new DevExpress.XtraGrid.Views.Layout.LayoutViewCard();
            this.barManager1 = new DevExpress.XtraBars.BarManager(this.components);
            this.bar3 = new DevExpress.XtraBars.Bar();
            this.barButtonItem1 = new DevExpress.XtraBars.BarButtonItem();
            this.barDockControlTop = new DevExpress.XtraBars.BarDockControl();
            this.barDockControlBottom = new DevExpress.XtraBars.BarDockControl();
            this.barDockControlLeft = new DevExpress.XtraBars.BarDockControl();
            this.barDockControlRight = new DevExpress.XtraBars.BarDockControl();
            ((System.ComponentModel.ISupportInitialize)(this.StyleController_F)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.hISTORICOSBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.hISTORICOSGridControl)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutView1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LookUpEditASSUNTO)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ASSuntobindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Item1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoEdit1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Item2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Item5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Item3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LookUpResp)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.UsuariosBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutViewField_colHSTI_USU)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutViewCard1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.barManager1)).BeginInit();
            this.SuspendLayout();
            // 
            // ToolTipController_F
            // 
            this.ToolTipController_F.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.ToolTipController_F.Appearance.ForeColor = System.Drawing.Color.Black;
            this.ToolTipController_F.Appearance.Options.UseBackColor = true;
            this.ToolTipController_F.Appearance.Options.UseFont = true;
            this.ToolTipController_F.Appearance.Options.UseForeColor = true;
            this.ToolTipController_F.Appearance.Options.UseTextOptions = true;
            this.ToolTipController_F.Appearance.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            // 
            // StyleController_F
            // 
            this.StyleController_F.AppearanceDisabled.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this.StyleController_F.AppearanceDisabled.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this.StyleController_F.AppearanceDisabled.Options.UseBackColor = true;
            this.StyleController_F.AppearanceDisabled.Options.UseForeColor = true;
            this.StyleController_F.AppearanceFocused.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.StyleController_F.AppearanceFocused.Options.UseBackColor = true;
     
            // 
            // hISTORICOSBindingSource
            // 
            this.hISTORICOSBindingSource.DataMember = "HISTORICOS";
            this.hISTORICOSBindingSource.DataSource = typeof(Cadastros.Condominios.dHistoricos);
            // 
            // hISTORICOSGridControl
            // 
            this.hISTORICOSGridControl.DataSource = this.hISTORICOSBindingSource;
            this.hISTORICOSGridControl.Dock = System.Windows.Forms.DockStyle.Fill;
            this.hISTORICOSGridControl.EmbeddedNavigator.Buttons.Append.Visible = false;
            this.hISTORICOSGridControl.EmbeddedNavigator.Buttons.CancelEdit.Visible = false;
            this.hISTORICOSGridControl.EmbeddedNavigator.Buttons.Edit.Visible = false;
            this.hISTORICOSGridControl.EmbeddedNavigator.Buttons.EndEdit.Visible = false;
            this.hISTORICOSGridControl.EmbeddedNavigator.Buttons.Remove.Visible = false;
            this.hISTORICOSGridControl.Location = new System.Drawing.Point(0, 0);
            this.hISTORICOSGridControl.MainView = this.layoutView1;
            this.hISTORICOSGridControl.Name = "hISTORICOSGridControl";
            this.hISTORICOSGridControl.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.LookUpResp,
            this.repositoryItemMemoEdit1,
            this.LookUpEditASSUNTO});
            this.hISTORICOSGridControl.Size = new System.Drawing.Size(1253, 517);
            this.hISTORICOSGridControl.TabIndex = 2;
            this.hISTORICOSGridControl.UseEmbeddedNavigator = true;
            this.hISTORICOSGridControl.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.layoutView1});
            this.hISTORICOSGridControl.Click += new System.EventHandler(this.hISTORICOSGridControl_Click);
            // 
            // layoutView1
            // 
            this.layoutView1.Appearance.CardCaption.BackColor = System.Drawing.Color.Orange;
            this.layoutView1.Appearance.CardCaption.BorderColor = System.Drawing.Color.Orange;
            this.layoutView1.Appearance.CardCaption.Options.UseBackColor = true;
            this.layoutView1.Appearance.CardCaption.Options.UseBorderColor = true;
            this.layoutView1.Appearance.FieldCaption.BackColor = System.Drawing.Color.Linen;
            this.layoutView1.Appearance.FieldCaption.BackColor2 = System.Drawing.Color.AntiqueWhite;
            this.layoutView1.Appearance.FieldCaption.Options.UseBackColor = true;
            this.layoutView1.Appearance.FieldValue.BackColor = System.Drawing.Color.White;
            this.layoutView1.Appearance.FieldValue.Options.UseBackColor = true;
            this.layoutView1.Appearance.FilterCloseButton.BackColor = System.Drawing.Color.Orange;
            this.layoutView1.Appearance.FilterCloseButton.BorderColor = System.Drawing.Color.Orange;
            this.layoutView1.Appearance.FilterCloseButton.Options.UseBackColor = true;
            this.layoutView1.Appearance.FilterCloseButton.Options.UseBorderColor = true;
            this.layoutView1.Appearance.FilterPanel.BackColor = System.Drawing.Color.DarkOrange;
            this.layoutView1.Appearance.FilterPanel.BackColor2 = System.Drawing.Color.Orange;
            this.layoutView1.Appearance.FilterPanel.ForeColor = System.Drawing.Color.Black;
            this.layoutView1.Appearance.FilterPanel.GradientMode = System.Drawing.Drawing2D.LinearGradientMode.BackwardDiagonal;
            this.layoutView1.Appearance.FilterPanel.Options.UseBackColor = true;
            this.layoutView1.Appearance.FilterPanel.Options.UseForeColor = true;
            this.layoutView1.Appearance.FocusedCardCaption.BackColor = System.Drawing.Color.RoyalBlue;
            this.layoutView1.Appearance.FocusedCardCaption.BackColor2 = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this.layoutView1.Appearance.FocusedCardCaption.BorderColor = System.Drawing.Color.RoyalBlue;
            this.layoutView1.Appearance.FocusedCardCaption.ForeColor = System.Drawing.Color.White;
            this.layoutView1.Appearance.FocusedCardCaption.GradientMode = System.Drawing.Drawing2D.LinearGradientMode.Vertical;
            this.layoutView1.Appearance.FocusedCardCaption.Options.UseBackColor = true;
            this.layoutView1.Appearance.FocusedCardCaption.Options.UseBorderColor = true;
            this.layoutView1.Appearance.FocusedCardCaption.Options.UseForeColor = true;
            this.layoutView1.Appearance.HideSelectionCardCaption.BackColor = System.Drawing.Color.LightSlateGray;
            this.layoutView1.Appearance.HideSelectionCardCaption.BorderColor = System.Drawing.Color.LightSlateGray;
            this.layoutView1.Appearance.HideSelectionCardCaption.Options.UseBackColor = true;
            this.layoutView1.Appearance.HideSelectionCardCaption.Options.UseBorderColor = true;
            this.layoutView1.Appearance.SeparatorLine.BackColor = System.Drawing.Color.Tan;
            this.layoutView1.Appearance.SeparatorLine.Options.UseBackColor = true;
            this.layoutView1.Appearance.ViewBackground.BackColor = System.Drawing.Color.LightSkyBlue;
            this.layoutView1.Appearance.ViewBackground.BackColor2 = System.Drawing.Color.SkyBlue;
            this.layoutView1.Appearance.ViewBackground.GradientMode = System.Drawing.Drawing2D.LinearGradientMode.BackwardDiagonal;
            this.layoutView1.Appearance.ViewBackground.Options.UseBackColor = true;
            this.layoutView1.CardCaptionFormat = "Registro [{0} de {1}]";
            this.layoutView1.CardHorzInterval = 4;
            this.layoutView1.CardMinSize = new System.Drawing.Size(660, 81);
            this.layoutView1.CardVertInterval = 4;
            this.layoutView1.Columns.AddRange(new DevExpress.XtraGrid.Columns.LayoutViewColumn[] {
            this.colHSTAssunto,
            this.colHSTDescricao,
            this.colHSTCritico,
            this.colHSTData,
            this.colHSTI_USU});
            this.layoutView1.GridControl = this.hISTORICOSGridControl;
            this.layoutView1.Name = "layoutView1";
            this.layoutView1.OptionsBehavior.Editable = false;
            this.layoutView1.OptionsItemText.TextToControlDistance = 1;
            this.layoutView1.OptionsMultiRecordMode.StretchCardToViewWidth = true;
            this.layoutView1.OptionsView.CardArrangeRule = DevExpress.XtraGrid.Views.Layout.LayoutCardArrangeRule.AllowPartialCards;
            this.layoutView1.OptionsView.ShowCardFieldBorders = true;
            this.layoutView1.OptionsView.ShowHeaderPanel = false;
            this.layoutView1.OptionsView.ViewMode = DevExpress.XtraGrid.Views.Layout.LayoutViewMode.Column;
            this.layoutView1.TemplateCard = this.layoutViewCard1;
            // 
            // colHSTAssunto
            // 
            this.colHSTAssunto.Caption = "Assunto";
            this.colHSTAssunto.ColumnEdit = this.LookUpEditASSUNTO;
            this.colHSTAssunto.FieldName = "HST_ASS";
            this.colHSTAssunto.LayoutViewField = this.Item1;
            this.colHSTAssunto.Name = "colHSTAssunto";
            // 
            // LookUpEditASSUNTO
            // 
            this.LookUpEditASSUNTO.AutoHeight = false;
            this.LookUpEditASSUNTO.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LookUpEditASSUNTO.DataSource = this.ASSuntobindingSource;
            this.LookUpEditASSUNTO.DisplayMember = "ASSDescricao";
            this.LookUpEditASSUNTO.Name = "LookUpEditASSUNTO";
            this.LookUpEditASSUNTO.ValueMember = "ASS";
            // 
            // ASSuntobindingSource
            // 
            this.ASSuntobindingSource.DataSource = typeof(Framework.datasets.dASSunto.ASSuntoDataTable);
            // 
            // Item1
            // 
            this.Item1.EditorPreferredWidth = 154;
            this.Item1.Location = new System.Drawing.Point(170, 0);
            this.Item1.Name = "Item1";
            this.Item1.Size = new System.Drawing.Size(214, 24);
            this.Item1.Spacing = new DevExpress.XtraLayout.Utils.Padding(2, 2, 2, 2);
            this.Item1.TextSize = new System.Drawing.Size(50, 13);
            this.Item1.TextToControlDistance = 1;
            // 
            // colHSTDescricao
            // 
            this.colHSTDescricao.AppearanceCell.Font = new System.Drawing.Font("Courier New", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.colHSTDescricao.AppearanceCell.Options.UseFont = true;
            this.colHSTDescricao.Caption = "Descri��o";
            this.colHSTDescricao.ColumnEdit = this.repositoryItemMemoEdit1;
            this.colHSTDescricao.FieldName = "HSTDescricao";
            this.colHSTDescricao.LayoutViewField = this.Item2;
            this.colHSTDescricao.Name = "colHSTDescricao";
            this.colHSTDescricao.OptionsFilter.AllowFilter = false;
            // 
            // repositoryItemMemoEdit1
            // 
            this.repositoryItemMemoEdit1.Name = "repositoryItemMemoEdit1";
            // 
            // Item2
            // 
            this.Item2.EditorPreferredWidth = 604;
            this.Item2.Location = new System.Drawing.Point(0, 24);
            this.Item2.Name = "Item2";
            this.Item2.Size = new System.Drawing.Size(663, 25);
            this.Item2.Spacing = new DevExpress.XtraLayout.Utils.Padding(2, 2, 2, 2);
            this.Item2.TextSize = new System.Drawing.Size(50, 13);
            this.Item2.TextToControlDistance = 1;
            // 
            // colHSTCritico
            // 
            this.colHSTCritico.Caption = "Cr�tico";
            this.colHSTCritico.FieldName = "HSTCritico";
            this.colHSTCritico.LayoutViewField = this.Item5;
            this.colHSTCritico.Name = "colHSTCritico";
            this.colHSTCritico.OptionsColumn.FixedWidth = true;
            // 
            // Item5
            // 
            this.Item5.EditorPreferredWidth = 20;
            this.Item5.Location = new System.Drawing.Point(583, 0);
            this.Item5.Name = "Item5";
            this.Item5.Size = new System.Drawing.Size(80, 24);
            this.Item5.Spacing = new DevExpress.XtraLayout.Utils.Padding(2, 2, 2, 2);
            this.Item5.TextSize = new System.Drawing.Size(50, 13);
            this.Item5.TextToControlDistance = 1;
            // 
            // colHSTData
            // 
            this.colHSTData.Caption = "Data";
            this.colHSTData.FieldName = "HSTDATAI";
            this.colHSTData.LayoutViewField = this.Item3;
            this.colHSTData.Name = "colHSTData";
            this.colHSTData.OptionsColumn.FixedWidth = true;
            // 
            // Item3
            // 
            this.Item3.EditorPreferredWidth = 110;
            this.Item3.Location = new System.Drawing.Point(0, 0);
            this.Item3.Name = "Item3";
            this.Item3.Size = new System.Drawing.Size(170, 24);
            this.Item3.Spacing = new DevExpress.XtraLayout.Utils.Padding(2, 2, 2, 2);
            this.Item3.TextSize = new System.Drawing.Size(50, 13);
            this.Item3.TextToControlDistance = 1;
            // 
            // colHSTI_USU
            // 
            this.colHSTI_USU.Caption = "Autor";
            this.colHSTI_USU.ColumnEdit = this.LookUpResp;
            this.colHSTI_USU.FieldName = "HSTI_USU";
            this.colHSTI_USU.LayoutViewField = this.layoutViewField_colHSTI_USU;
            this.colHSTI_USU.Name = "colHSTI_USU";
            // 
            // LookUpResp
            // 
            this.LookUpResp.AutoHeight = false;
            this.LookUpResp.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LookUpResp.DataSource = this.UsuariosBindingSource;
            this.LookUpResp.DisplayMember = "USUNome";
            this.LookUpResp.Name = "LookUpResp";
            this.LookUpResp.ValueMember = "USU";
            // 
            // UsuariosBindingSource
            // 
            this.UsuariosBindingSource.DataSource = typeof(FrameworkProc.datasets.dUSUarios.USUariosDataTable);
            // 
            // layoutViewField_colHSTI_USU
            // 
            this.layoutViewField_colHSTI_USU.EditorPreferredWidth = 138;
            this.layoutViewField_colHSTI_USU.Location = new System.Drawing.Point(384, 0);
            this.layoutViewField_colHSTI_USU.Name = "layoutViewField_colHSTI_USU";
            this.layoutViewField_colHSTI_USU.Size = new System.Drawing.Size(199, 24);
            this.layoutViewField_colHSTI_USU.TextSize = new System.Drawing.Size(50, 13);
            this.layoutViewField_colHSTI_USU.TextToControlDistance = 1;
            // 
            // layoutViewCard1
            // 
            this.layoutViewCard1.CustomizationFormText = "layoutViewTemplateCard";
            this.layoutViewCard1.HeaderButtonsLocation = DevExpress.Utils.GroupElementLocation.AfterText;
            this.layoutViewCard1.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.Item1,
            this.Item2,
            this.Item3,
            this.Item5,
            this.layoutViewField_colHSTI_USU});
            this.layoutViewCard1.Name = "layoutViewCard1";
            this.layoutViewCard1.OptionsItemText.TextToControlDistance = 1;
            this.layoutViewCard1.Padding = new DevExpress.XtraLayout.Utils.Padding(2, 2, 2, 2);
            this.layoutViewCard1.Text = "TemplateCard";
            this.layoutViewCard1.TextLocation = DevExpress.Utils.Locations.Default;
            // 
            // barManager1
            // 
            this.barManager1.Bars.AddRange(new DevExpress.XtraBars.Bar[] {
            this.bar3});
            this.barManager1.DockControls.Add(this.barDockControlTop);
            this.barManager1.DockControls.Add(this.barDockControlBottom);
            this.barManager1.DockControls.Add(this.barDockControlLeft);
            this.barManager1.DockControls.Add(this.barDockControlRight);
            this.barManager1.Form = this;
            this.barManager1.Items.AddRange(new DevExpress.XtraBars.BarItem[] {
            this.barButtonItem1});
            this.barManager1.MaxItemId = 1;
            this.barManager1.StatusBar = this.bar3;
            // 
            // bar3
            // 
            this.bar3.BarName = "Custom 4";
            this.bar3.CanDockStyle = DevExpress.XtraBars.BarCanDockStyle.Bottom;
            this.bar3.DockCol = 0;
            this.bar3.DockRow = 0;
            this.bar3.DockStyle = DevExpress.XtraBars.BarDockStyle.Bottom;
            this.bar3.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(this.barButtonItem1)});
            this.bar3.OptionsBar.AllowQuickCustomization = false;
            this.bar3.OptionsBar.DrawDragBorder = false;
            this.bar3.OptionsBar.UseWholeRow = true;
            this.bar3.Text = "Custom 4";
            // 
            // barButtonItem1
            // 
            this.barButtonItem1.Border = DevExpress.XtraEditors.Controls.BorderStyles.Simple;
            this.barButtonItem1.Caption = "Incluir";
            this.barButtonItem1.Glyph = global::Cadastros.Properties.Resources.BtnIncluir_F_Glyph;
            this.barButtonItem1.Id = 0;
            this.barButtonItem1.Name = "barButtonItem1";
            this.barButtonItem1.PaintStyle = DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph;
            this.barButtonItem1.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.barButtonItem1_ItemClick);
            // 
            // barDockControlTop
            // 
            this.barDockControlTop.Dock = System.Windows.Forms.DockStyle.Top;
            this.barDockControlTop.Location = new System.Drawing.Point(0, 0);
            this.barDockControlTop.Size = new System.Drawing.Size(1253, 0);
            // 
            // barDockControlBottom
            // 
            this.barDockControlBottom.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.barDockControlBottom.Location = new System.Drawing.Point(0, 517);
            this.barDockControlBottom.Size = new System.Drawing.Size(1253, 28);
            // 
            // barDockControlLeft
            // 
            this.barDockControlLeft.Dock = System.Windows.Forms.DockStyle.Left;
            this.barDockControlLeft.Location = new System.Drawing.Point(0, 0);
            this.barDockControlLeft.Size = new System.Drawing.Size(0, 517);
            // 
            // barDockControlRight
            // 
            this.barDockControlRight.Dock = System.Windows.Forms.DockStyle.Right;
            this.barDockControlRight.Location = new System.Drawing.Point(1253, 0);
            this.barDockControlRight.Size = new System.Drawing.Size(0, 517);
            // 
            // TabHistorico
            // 
            this.Controls.Add(this.hISTORICOSGridControl);
            this.Controls.Add(this.barDockControlLeft);
            this.Controls.Add(this.barDockControlRight);
            this.Controls.Add(this.barDockControlBottom);
            this.Controls.Add(this.barDockControlTop);
            this.Doc = System.Windows.Forms.DockStyle.Fill;
      
            this.LookAndFeel.UseDefaultLookAndFeel = false;
            this.Name = "TabHistorico";
            this.Size = new System.Drawing.Size(1253, 545);
            ((System.ComponentModel.ISupportInitialize)(this.StyleController_F)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.hISTORICOSBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.hISTORICOSGridControl)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutView1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LookUpEditASSUNTO)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ASSuntobindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Item1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoEdit1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Item2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Item5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Item3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LookUpResp)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.UsuariosBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutViewField_colHSTI_USU)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutViewCard1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.barManager1)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.BindingSource hISTORICOSBindingSource;
        private DevExpress.XtraGrid.GridControl hISTORICOSGridControl;
        private DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit LookUpResp;
        private System.Windows.Forms.BindingSource UsuariosBindingSource;
        private DevExpress.XtraBars.BarManager barManager1;
        private DevExpress.XtraBars.Bar bar3;
        private DevExpress.XtraBars.BarDockControl barDockControlTop;
        private DevExpress.XtraBars.BarDockControl barDockControlBottom;
        private DevExpress.XtraBars.BarDockControl barDockControlLeft;
        private DevExpress.XtraBars.BarDockControl barDockControlRight;
        private DevExpress.XtraBars.BarButtonItem barButtonItem1;
        private DevExpress.XtraGrid.Views.Layout.LayoutView layoutView1;
        private DevExpress.XtraGrid.Columns.LayoutViewColumn colHSTAssunto;
        private DevExpress.XtraGrid.Columns.LayoutViewColumn colHSTDescricao;
        private DevExpress.XtraGrid.Columns.LayoutViewColumn colHSTCritico;
        private DevExpress.XtraGrid.Columns.LayoutViewColumn colHSTData;
        private DevExpress.XtraEditors.Repository.RepositoryItemMemoEdit repositoryItemMemoEdit1;
        private DevExpress.XtraGrid.Columns.LayoutViewColumn colHSTI_USU;
        private DevExpress.XtraGrid.Views.Layout.LayoutViewField Item1;
        private DevExpress.XtraGrid.Views.Layout.LayoutViewField Item2;
        private DevExpress.XtraGrid.Views.Layout.LayoutViewField Item5;
        private DevExpress.XtraGrid.Views.Layout.LayoutViewField Item3;
        private DevExpress.XtraGrid.Views.Layout.LayoutViewField layoutViewField_colHSTI_USU;
        private DevExpress.XtraGrid.Views.Layout.LayoutViewCard layoutViewCard1;
        private System.Windows.Forms.BindingSource ASSuntobindingSource;
        private DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit LookUpEditASSUNTO;
    }
}
