namespace Cadastros.Condominios
{
    partial class TabInternet
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.Windows.Forms.Label cONExportaInternetLabel;
            System.Windows.Forms.Label cONPublicaBalanceteInternetLabel;
            System.Windows.Forms.Label cONPermiteAcordoInternetLabel;
            System.Windows.Forms.Label cONLinkBalanceteGraficoInternetLabel;
            this.cONDOMINIOSBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.cONExportaInternetCheckEdit = new DevExpress.XtraEditors.CheckEdit();
            this.groupControl1 = new DevExpress.XtraEditors.GroupControl();
            this.cONPermiteAcordoInternetCheckEdit = new DevExpress.XtraEditors.CheckEdit();
            this.cONPublicaBalanceteInternetCheckEdit = new DevExpress.XtraEditors.CheckEdit();
            this.groupControl2 = new DevExpress.XtraEditors.GroupControl();
            this.xtraTabControl1 = new DevExpress.XtraTab.XtraTabControl();
            this.PaginaEditor = new DevExpress.XtraTab.XtraTabPage();
            this.EditorInternet = new DevExpress.XtraEditors.MemoEdit();
            this.lcgVisualizadorInternet = new DevExpress.XtraTab.XtraTabPage();
            this.webBrowser1 = new System.Windows.Forms.WebBrowser();
            this.cONLinkBalanceteGraficoInternetTextEdit = new DevExpress.XtraEditors.TextEdit();
            cONExportaInternetLabel = new System.Windows.Forms.Label();
            cONPublicaBalanceteInternetLabel = new System.Windows.Forms.Label();
            cONPermiteAcordoInternetLabel = new System.Windows.Forms.Label();
            cONLinkBalanceteGraficoInternetLabel = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.StyleController_F)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cONDOMINIOSBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cONExportaInternetCheckEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl1)).BeginInit();
            this.groupControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.cONPermiteAcordoInternetCheckEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cONPublicaBalanceteInternetCheckEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl2)).BeginInit();
            this.groupControl2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.xtraTabControl1)).BeginInit();
            this.xtraTabControl1.SuspendLayout();
            this.PaginaEditor.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.EditorInternet.Properties)).BeginInit();
            this.lcgVisualizadorInternet.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.cONLinkBalanceteGraficoInternetTextEdit.Properties)).BeginInit();
            this.SuspendLayout();
            // 
            // ToolTipController_F
            // 
            this.ToolTipController_F.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.ToolTipController_F.Appearance.ForeColor = System.Drawing.Color.Black;
            this.ToolTipController_F.Appearance.Options.UseBackColor = true;
            this.ToolTipController_F.Appearance.Options.UseFont = true;
            this.ToolTipController_F.Appearance.Options.UseForeColor = true;
            this.ToolTipController_F.Appearance.Options.UseTextOptions = true;
            this.ToolTipController_F.Appearance.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            // 
            // StyleController_F
            // 
            this.StyleController_F.AppearanceDisabled.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this.StyleController_F.AppearanceDisabled.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this.StyleController_F.AppearanceDisabled.Options.UseBackColor = true;
            this.StyleController_F.AppearanceDisabled.Options.UseForeColor = true;
            this.StyleController_F.AppearanceFocused.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.StyleController_F.AppearanceFocused.Options.UseBackColor = true;
            // 
            // cONExportaInternetLabel
            // 
            cONExportaInternetLabel.AutoSize = true;
            cONExportaInternetLabel.Location = new System.Drawing.Point(43, 25);
            cONExportaInternetLabel.Name = "cONExportaInternetLabel";
            cONExportaInternetLabel.Size = new System.Drawing.Size(186, 13);
            cONExportaInternetLabel.TabIndex = 2;
            cONExportaInternetLabel.Text = "Diponibilizar o condom�nio na Internet";
            // 
            // cONPublicaBalanceteInternetLabel
            // 
            cONPublicaBalanceteInternetLabel.AutoSize = true;
            cONPublicaBalanceteInternetLabel.Location = new System.Drawing.Point(43, 50);
            cONPublicaBalanceteInternetLabel.Name = "cONPublicaBalanceteInternetLabel";
            cONPublicaBalanceteInternetLabel.Size = new System.Drawing.Size(246, 13);
            cONPublicaBalanceteInternetLabel.TabIndex = 3;
            cONPublicaBalanceteInternetLabel.Text = "Liberar balancetes mediante aprova��o do s�ndico";
            // 
            // cONPermiteAcordoInternetLabel
            // 
            cONPermiteAcordoInternetLabel.AutoSize = true;
            cONPermiteAcordoInternetLabel.Location = new System.Drawing.Point(43, 75);
            cONPermiteAcordoInternetLabel.Name = "cONPermiteAcordoInternetLabel";
            cONPermiteAcordoInternetLabel.Size = new System.Drawing.Size(223, 13);
            cONPermiteAcordoInternetLabel.TabIndex = 4;
            cONPermiteAcordoInternetLabel.Text = "Permite Acordos feitos direto pelo cond�mino";
            // 
            // cONDOMINIOSBindingSource
            // 
            this.cONDOMINIOSBindingSource.DataMember = "CONDOMINIOS";
            this.cONDOMINIOSBindingSource.DataSource = typeof(CadastrosProc.Condominios.dCondominiosCampos);
            // 
            // cONExportaInternetCheckEdit
            // 
            this.cONExportaInternetCheckEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.cONDOMINIOSBindingSource, "CONExportaInternet", true));
            this.cONExportaInternetCheckEdit.Location = new System.Drawing.Point(14, 23);
            this.cONExportaInternetCheckEdit.Name = "cONExportaInternetCheckEdit";
            this.cONExportaInternetCheckEdit.Properties.Caption = "";
            this.cONExportaInternetCheckEdit.Size = new System.Drawing.Size(21, 19);
            this.cONExportaInternetCheckEdit.TabIndex = 3;
            // 
            // groupControl1
            // 
            this.groupControl1.Controls.Add(this.cONLinkBalanceteGraficoInternetTextEdit);
            this.groupControl1.Controls.Add(cONLinkBalanceteGraficoInternetLabel);
            this.groupControl1.Controls.Add(cONPermiteAcordoInternetLabel);
            this.groupControl1.Controls.Add(this.cONPermiteAcordoInternetCheckEdit);
            this.groupControl1.Controls.Add(cONPublicaBalanceteInternetLabel);
            this.groupControl1.Controls.Add(this.cONPublicaBalanceteInternetCheckEdit);
            this.groupControl1.Controls.Add(this.cONExportaInternetCheckEdit);
            this.groupControl1.Controls.Add(cONExportaInternetLabel);
            this.groupControl1.Dock = System.Windows.Forms.DockStyle.Top;
            this.groupControl1.Location = new System.Drawing.Point(0, 0);
            this.groupControl1.Name = "groupControl1";
            this.groupControl1.Size = new System.Drawing.Size(1456, 100);
            this.groupControl1.TabIndex = 4;
            this.groupControl1.Text = ":: Par�metros ::";
            // 
            // cONPermiteAcordoInternetCheckEdit
            // 
            this.cONPermiteAcordoInternetCheckEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.cONDOMINIOSBindingSource, "CONPermiteAcordoInternet", true));
            this.cONPermiteAcordoInternetCheckEdit.Location = new System.Drawing.Point(14, 73);
            this.cONPermiteAcordoInternetCheckEdit.Name = "cONPermiteAcordoInternetCheckEdit";
            this.cONPermiteAcordoInternetCheckEdit.Properties.Caption = "";
            this.cONPermiteAcordoInternetCheckEdit.Size = new System.Drawing.Size(75, 19);
            this.cONPermiteAcordoInternetCheckEdit.TabIndex = 5;
            // 
            // cONPublicaBalanceteInternetCheckEdit
            // 
            this.cONPublicaBalanceteInternetCheckEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.cONDOMINIOSBindingSource, "CONPublicaBalanceteInternet", true));
            this.cONPublicaBalanceteInternetCheckEdit.Location = new System.Drawing.Point(14, 48);
            this.cONPublicaBalanceteInternetCheckEdit.Name = "cONPublicaBalanceteInternetCheckEdit";
            this.cONPublicaBalanceteInternetCheckEdit.Properties.Caption = "";
            this.cONPublicaBalanceteInternetCheckEdit.Size = new System.Drawing.Size(23, 19);
            this.cONPublicaBalanceteInternetCheckEdit.TabIndex = 4;
            // 
            // groupControl2
            // 
            this.groupControl2.Controls.Add(this.xtraTabControl1);
            this.groupControl2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.groupControl2.Location = new System.Drawing.Point(0, 100);
            this.groupControl2.Name = "groupControl2";
            this.groupControl2.Size = new System.Drawing.Size(1456, 721);
            this.groupControl2.TabIndex = 5;
            this.groupControl2.Text = ":: Observa��es ::";
            // 
            // xtraTabControl1
            // 
            this.xtraTabControl1.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.cONDOMINIOSBindingSource, "CONObservacaoInternet", true));
            this.xtraTabControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.xtraTabControl1.Location = new System.Drawing.Point(2, 20);
            this.xtraTabControl1.Name = "xtraTabControl1";
            this.xtraTabControl1.SelectedTabPage = this.PaginaEditor;
            this.xtraTabControl1.Size = new System.Drawing.Size(1452, 699);
            this.xtraTabControl1.TabIndex = 0;
            this.xtraTabControl1.TabPages.AddRange(new DevExpress.XtraTab.XtraTabPage[] {
            this.PaginaEditor,
            this.lcgVisualizadorInternet});
            this.xtraTabControl1.SelectedPageChanged += new DevExpress.XtraTab.TabPageChangedEventHandler(this.xtraTabControl1_SelectedPageChanged);
            // 
            // PaginaEditor
            // 
            this.PaginaEditor.Controls.Add(this.EditorInternet);
            this.PaginaEditor.Name = "PaginaEditor";
            this.PaginaEditor.Size = new System.Drawing.Size(1446, 671);
            this.PaginaEditor.Text = "Editor";
            // 
            // EditorInternet
            // 
            this.EditorInternet.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.cONDOMINIOSBindingSource, "CONObservacaoInternet", true));
            this.EditorInternet.Dock = System.Windows.Forms.DockStyle.Fill;
            this.EditorInternet.Location = new System.Drawing.Point(0, 0);
            this.EditorInternet.Name = "EditorInternet";
            this.EditorInternet.Size = new System.Drawing.Size(1446, 671);
            this.EditorInternet.TabIndex = 1;
            // 
            // lcgVisualizadorInternet
            // 
            this.lcgVisualizadorInternet.Controls.Add(this.webBrowser1);
            this.lcgVisualizadorInternet.Name = "lcgVisualizadorInternet";
            this.lcgVisualizadorInternet.Size = new System.Drawing.Size(1280, 671);
            this.lcgVisualizadorInternet.Text = "Pr�-Visualiza��o";
            // 
            // webBrowser1
            // 
            this.webBrowser1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.webBrowser1.Location = new System.Drawing.Point(0, 0);
            this.webBrowser1.MinimumSize = new System.Drawing.Size(20, 20);
            this.webBrowser1.Name = "webBrowser1";
            this.webBrowser1.Size = new System.Drawing.Size(1280, 671);
            this.webBrowser1.TabIndex = 0;
            // 
            // cONLinkBalanceteGraficoInternetTextEdit
            // 
            this.cONLinkBalanceteGraficoInternetTextEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.cONDOMINIOSBindingSource, "CONLinkBalanceteGraficoInternet", true));
            this.cONLinkBalanceteGraficoInternetTextEdit.Location = new System.Drawing.Point(324, 48);
            this.cONLinkBalanceteGraficoInternetTextEdit.Name = "cONLinkBalanceteGraficoInternetTextEdit";
            this.cONLinkBalanceteGraficoInternetTextEdit.Size = new System.Drawing.Size(912, 20);
            this.cONLinkBalanceteGraficoInternetTextEdit.TabIndex = 6;
            // 
            // cONLinkBalanceteGraficoInternetLabel
            // 
            cONLinkBalanceteGraficoInternetLabel.AutoSize = true;
            cONLinkBalanceteGraficoInternetLabel.Location = new System.Drawing.Point(321, 25);
            cONLinkBalanceteGraficoInternetLabel.Name = "cONLinkBalanceteGraficoInternetLabel";
            cONLinkBalanceteGraficoInternetLabel.Size = new System.Drawing.Size(212, 13);
            cONLinkBalanceteGraficoInternetLabel.TabIndex = 6;
            cONLinkBalanceteGraficoInternetLabel.Text = "Link para Visualiza��o Gr�fica do Balancete";
            // 
            // TabInternet
            // 
            this.Controls.Add(this.groupControl2);
            this.Controls.Add(this.groupControl1);
            this.Doc = System.Windows.Forms.DockStyle.Fill;
            this.LookAndFeel.UseDefaultLookAndFeel = false;
            this.Name = "TabInternet";
            this.Size = new System.Drawing.Size(1456, 821);
            ((System.ComponentModel.ISupportInitialize)(this.StyleController_F)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cONDOMINIOSBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cONExportaInternetCheckEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl1)).EndInit();
            this.groupControl1.ResumeLayout(false);
            this.groupControl1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.cONPermiteAcordoInternetCheckEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cONPublicaBalanceteInternetCheckEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl2)).EndInit();
            this.groupControl2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.xtraTabControl1)).EndInit();
            this.xtraTabControl1.ResumeLayout(false);
            this.PaginaEditor.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.EditorInternet.Properties)).EndInit();
            this.lcgVisualizadorInternet.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.cONLinkBalanceteGraficoInternetTextEdit.Properties)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.BindingSource cONDOMINIOSBindingSource;
        private DevExpress.XtraEditors.CheckEdit cONExportaInternetCheckEdit;
        private DevExpress.XtraEditors.GroupControl groupControl1;
        private DevExpress.XtraEditors.GroupControl groupControl2;
        private DevExpress.XtraTab.XtraTabControl xtraTabControl1;
        private DevExpress.XtraTab.XtraTabPage PaginaEditor;
        private DevExpress.XtraTab.XtraTabPage lcgVisualizadorInternet;
        private DevExpress.XtraEditors.CheckEdit cONPermiteAcordoInternetCheckEdit;
        private DevExpress.XtraEditors.CheckEdit cONPublicaBalanceteInternetCheckEdit;
        private DevExpress.XtraEditors.MemoEdit EditorInternet;
        private System.Windows.Forms.WebBrowser webBrowser1;
        private DevExpress.XtraEditors.TextEdit cONLinkBalanceteGraficoInternetTextEdit;
    }
}
