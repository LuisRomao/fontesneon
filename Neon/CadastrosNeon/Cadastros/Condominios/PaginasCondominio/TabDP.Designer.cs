//LH 16/04/2014
// troca o campo CONCodigoFolha2 por CONCodigoFolha2A
// remo��o de CONCodigoBancoCS

using CadastrosProc.Condominios;

namespace Cadastros.Condominios.PaginasCondominio
{
    partial class TabDP
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.Windows.Forms.Label cONCodigoFolha1Label;
            System.Windows.Forms.Label cONCodigoFolha2Label;
            System.Windows.Forms.Label cONCodigoBancoCCLabel;
            this.label4 = new System.Windows.Forms.Label();
            this.groupControl1 = new DevExpress.XtraEditors.GroupControl();
            this.groupControl3 = new DevExpress.XtraEditors.GroupControl();
            this.cONCodigoBancoCCSpinEdit = new DevExpress.XtraEditors.SpinEdit();
            this.cONDOMINIOSBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.groupControl2 = new DevExpress.XtraEditors.GroupControl();
            this.textEdit1 = new DevExpress.XtraEditors.TextEdit();
            this.cONCodigoFolha1SpinEdit = new DevExpress.XtraEditors.SpinEdit();
            this.groupControl4 = new DevExpress.XtraEditors.GroupControl();
            this.txtSequencialRemessaTributos = new DevExpress.XtraEditors.TextEdit();
            this.cmdIniciarRemessa = new System.Windows.Forms.Button();
            this.textEdit2 = new DevExpress.XtraEditors.TextEdit();
            this.checkEdit2 = new DevExpress.XtraEditors.CheckEdit();
            this.checkEdit1 = new DevExpress.XtraEditors.CheckEdit();
            this.groupControl5 = new DevExpress.XtraEditors.GroupControl();
            this.checkEdit4 = new DevExpress.XtraEditors.CheckEdit();
            this.checkEdit3 = new DevExpress.XtraEditors.CheckEdit();
            this.dCondominiosCampos = new CadastrosProc.Condominios.dCondominiosCampos();
            cONCodigoFolha1Label = new System.Windows.Forms.Label();
            cONCodigoFolha2Label = new System.Windows.Forms.Label();
            cONCodigoBancoCCLabel = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.StyleController_F)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl1)).BeginInit();
            this.groupControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl3)).BeginInit();
            this.groupControl3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.cONCodigoBancoCCSpinEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cONDOMINIOSBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl2)).BeginInit();
            this.groupControl2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit1.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cONCodigoFolha1SpinEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl4)).BeginInit();
            this.groupControl4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtSequencialRemessaTributos.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit2.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit2.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit1.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl5)).BeginInit();
            this.groupControl5.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit4.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit3.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dCondominiosCampos)).BeginInit();
            this.SuspendLayout();
            // 
            // ToolTipController_F
            // 
            this.ToolTipController_F.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.ToolTipController_F.Appearance.ForeColor = System.Drawing.Color.Black;
            this.ToolTipController_F.Appearance.Options.UseBackColor = true;
            this.ToolTipController_F.Appearance.Options.UseFont = true;
            this.ToolTipController_F.Appearance.Options.UseForeColor = true;
            this.ToolTipController_F.Appearance.Options.UseTextOptions = true;
            this.ToolTipController_F.Appearance.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            // 
            // StyleController_F
            // 
            this.StyleController_F.AppearanceDisabled.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this.StyleController_F.AppearanceDisabled.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this.StyleController_F.AppearanceDisabled.Options.UseBackColor = true;
            this.StyleController_F.AppearanceDisabled.Options.UseForeColor = true;
            this.StyleController_F.AppearanceFocused.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.StyleController_F.AppearanceFocused.Options.UseBackColor = true;
            // 
            // cONCodigoFolha1Label
            // 
            cONCodigoFolha1Label.AutoSize = true;
            cONCodigoFolha1Label.Location = new System.Drawing.Point(7, 32);
            cONCodigoFolha1Label.Name = "cONCodigoFolha1Label";
            cONCodigoFolha1Label.Size = new System.Drawing.Size(82, 13);
            cONCodigoFolha1Label.TabIndex = 0;
            cONCodigoFolha1Label.Text = "C�digo Principal";
            // 
            // cONCodigoFolha2Label
            // 
            cONCodigoFolha2Label.AutoSize = true;
            cONCodigoFolha2Label.Location = new System.Drawing.Point(7, 70);
            cONCodigoFolha2Label.Name = "cONCodigoFolha2Label";
            cONCodigoFolha2Label.Size = new System.Drawing.Size(96, 13);
            cONCodigoFolha2Label.TabIndex = 2;
            cONCodigoFolha2Label.Text = "C�digo Alternativo";
            // 
            // cONCodigoBancoCCLabel
            // 
            cONCodigoBancoCCLabel.AutoSize = true;
            cONCodigoBancoCCLabel.Location = new System.Drawing.Point(8, 32);
            cONCodigoBancoCCLabel.Name = "cONCodigoBancoCCLabel";
            cONCodigoBancoCCLabel.Size = new System.Drawing.Size(91, 13);
            cONCodigoBancoCCLabel.TabIndex = 0;
            cONCodigoBancoCCLabel.Text = "Cr�dito em Conta";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(203, 41);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(96, 13);
            this.label4.TabIndex = 12;
            this.label4.Text = "C�d. Comunica��o";
            // 
            // groupControl1
            // 
            this.groupControl1.Controls.Add(this.groupControl3);
            this.groupControl1.Controls.Add(this.groupControl2);
            this.groupControl1.Controls.Add(this.groupControl4);
            this.groupControl1.Controls.Add(this.groupControl5);
            this.groupControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.groupControl1.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.groupControl1.Location = new System.Drawing.Point(0, 0);
            this.groupControl1.Name = "groupControl1";
            this.groupControl1.Size = new System.Drawing.Size(552, 306);
            this.groupControl1.TabIndex = 0;
            this.groupControl1.Text = ":: Par�metros para gera��o de arquivos remessa ::";
            // 
            // groupControl3
            // 
            this.groupControl3.Controls.Add(cONCodigoBancoCCLabel);
            this.groupControl3.Controls.Add(this.cONCodigoBancoCCSpinEdit);
            this.groupControl3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.groupControl3.Location = new System.Drawing.Point(276, 20);
            this.groupControl3.Name = "groupControl3";
            this.groupControl3.Size = new System.Drawing.Size(274, 110);
            this.groupControl3.TabIndex = 1;
            this.groupControl3.Text = ":: Bradesco ::";
            // 
            // cONCodigoBancoCCSpinEdit
            // 
            this.cONCodigoBancoCCSpinEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.cONDOMINIOSBindingSource, "CONCodigoBancoCC", true));
            this.cONCodigoBancoCCSpinEdit.EditValue = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.cONCodigoBancoCCSpinEdit.Location = new System.Drawing.Point(130, 31);
            this.cONCodigoBancoCCSpinEdit.Name = "cONCodigoBancoCCSpinEdit";
            this.cONCodigoBancoCCSpinEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.cONCodigoBancoCCSpinEdit.Size = new System.Drawing.Size(120, 20);
            this.cONCodigoBancoCCSpinEdit.TabIndex = 1;
            // 
            // cONDOMINIOSBindingSource
            // 
            this.cONDOMINIOSBindingSource.DataMember = "CONDOMINIOS";
            this.cONDOMINIOSBindingSource.DataSource = typeof(CadastrosProc.Condominios.dCondominiosCampos);
            // 
            // groupControl2
            // 
            this.groupControl2.Controls.Add(this.textEdit1);
            this.groupControl2.Controls.Add(cONCodigoFolha2Label);
            this.groupControl2.Controls.Add(cONCodigoFolha1Label);
            this.groupControl2.Controls.Add(this.cONCodigoFolha1SpinEdit);
            this.groupControl2.Dock = System.Windows.Forms.DockStyle.Left;
            this.groupControl2.Location = new System.Drawing.Point(2, 20);
            this.groupControl2.Name = "groupControl2";
            this.groupControl2.Size = new System.Drawing.Size(274, 110);
            this.groupControl2.TabIndex = 0;
            this.groupControl2.Text = ":: Datamace ::";
            // 
            // textEdit1
            // 
            this.textEdit1.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.cONDOMINIOSBindingSource, "CONCodigoFolha2A", true));
            this.textEdit1.Location = new System.Drawing.Point(133, 67);
            this.textEdit1.Name = "textEdit1";
            this.textEdit1.Size = new System.Drawing.Size(120, 20);
            this.textEdit1.TabIndex = 4;
            // 
            // cONCodigoFolha1SpinEdit
            // 
            this.cONCodigoFolha1SpinEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.cONDOMINIOSBindingSource, "CONCodigoFolha1", true));
            this.cONCodigoFolha1SpinEdit.EditValue = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.cONCodigoFolha1SpinEdit.Location = new System.Drawing.Point(133, 31);
            this.cONCodigoFolha1SpinEdit.Name = "cONCodigoFolha1SpinEdit";
            this.cONCodigoFolha1SpinEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.cONCodigoFolha1SpinEdit.Size = new System.Drawing.Size(120, 20);
            this.cONCodigoFolha1SpinEdit.TabIndex = 1;
            // 
            // groupControl4
            // 
            this.groupControl4.Controls.Add(this.txtSequencialRemessaTributos);
            this.groupControl4.Controls.Add(this.cmdIniciarRemessa);
            this.groupControl4.Controls.Add(this.textEdit2);
            this.groupControl4.Controls.Add(this.label4);
            this.groupControl4.Controls.Add(this.checkEdit2);
            this.groupControl4.Controls.Add(this.checkEdit1);
            this.groupControl4.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.groupControl4.Location = new System.Drawing.Point(2, 130);
            this.groupControl4.Name = "groupControl4";
            this.groupControl4.Size = new System.Drawing.Size(548, 88);
            this.groupControl4.TabIndex = 2;
            this.groupControl4.Text = ":: Pagamentos eletr�nicos ::";
            // 
            // txtSequencialRemessaTributos
            // 
            this.txtSequencialRemessaTributos.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.cONDOMINIOSBindingSource, "CONSequencialRemessaTributos", true));
            this.txtSequencialRemessaTributos.Location = new System.Drawing.Point(424, 53);
            this.txtSequencialRemessaTributos.Name = "txtSequencialRemessaTributos";
            this.txtSequencialRemessaTributos.Properties.Appearance.BackColor = System.Drawing.Color.WhiteSmoke;
            this.txtSequencialRemessaTributos.Properties.Appearance.Options.UseBackColor = true;
            this.txtSequencialRemessaTributos.Properties.Appearance.Options.UseTextOptions = true;
            this.txtSequencialRemessaTributos.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.txtSequencialRemessaTributos.Properties.AppearanceDisabled.ForeColor = System.Drawing.Color.Black;
            this.txtSequencialRemessaTributos.Properties.AppearanceDisabled.Options.UseForeColor = true;
            this.txtSequencialRemessaTributos.Properties.ReadOnly = true;
            this.txtSequencialRemessaTributos.Size = new System.Drawing.Size(100, 20);
            this.txtSequencialRemessaTributos.TabIndex = 14;
            // 
            // cmdIniciarRemessa
            // 
            this.cmdIniciarRemessa.Location = new System.Drawing.Point(424, 30);
            this.cmdIniciarRemessa.Name = "cmdIniciarRemessa";
            this.cmdIniciarRemessa.Size = new System.Drawing.Size(100, 24);
            this.cmdIniciarRemessa.TabIndex = 13;
            this.cmdIniciarRemessa.Text = "Iniciar Remessa";
            this.cmdIniciarRemessa.UseVisualStyleBackColor = true;
            this.cmdIniciarRemessa.Click += new System.EventHandler(this.cmdIniciarRemessa_Click);
            // 
            // textEdit2
            // 
            this.textEdit2.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.cONDOMINIOSBindingSource, "CONCodigoComunicacaoTributos", true));
            this.textEdit2.Location = new System.Drawing.Point(306, 38);
            this.textEdit2.Name = "textEdit2";
            this.textEdit2.Size = new System.Drawing.Size(100, 20);
            this.textEdit2.TabIndex = 2;
            // 
            // checkEdit2
            // 
            this.checkEdit2.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.cONDOMINIOSBindingSource, "CONGPSEletronico", true));
            this.checkEdit2.Location = new System.Drawing.Point(87, 38);
            this.checkEdit2.Name = "checkEdit2";
            this.checkEdit2.Properties.Caption = "Tributos";
            this.checkEdit2.Size = new System.Drawing.Size(75, 19);
            this.checkEdit2.TabIndex = 1;
            // 
            // checkEdit1
            // 
            this.checkEdit1.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.cONDOMINIOSBindingSource, "CONPagEletronico", true));
            this.checkEdit1.Location = new System.Drawing.Point(6, 39);
            this.checkEdit1.Name = "checkEdit1";
            this.checkEdit1.Properties.Caption = "Sal�rio";
            this.checkEdit1.Size = new System.Drawing.Size(75, 19);
            this.checkEdit1.TabIndex = 0;
            // 
            // groupControl5
            // 
            this.groupControl5.Controls.Add(this.checkEdit4);
            this.groupControl5.Controls.Add(this.checkEdit3);
            this.groupControl5.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.groupControl5.Location = new System.Drawing.Point(2, 218);
            this.groupControl5.Name = "groupControl5";
            this.groupControl5.Size = new System.Drawing.Size(548, 86);
            this.groupControl5.TabIndex = 3;
            this.groupControl5.Text = ":: Outros ::";
            // 
            // checkEdit4
            // 
            this.checkEdit4.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.cONDOMINIOSBindingSource, "CONSefip", true));
            this.checkEdit4.Location = new System.Drawing.Point(133, 37);
            this.checkEdit4.Name = "checkEdit4";
            this.checkEdit4.Properties.Caption = "SEFIP";
            this.checkEdit4.Size = new System.Drawing.Size(126, 19);
            this.checkEdit4.TabIndex = 4;
            // 
            // checkEdit3
            // 
            this.checkEdit3.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.cONDOMINIOSBindingSource, "CONPlanilhaPonto", true));
            this.checkEdit3.Location = new System.Drawing.Point(8, 37);
            this.checkEdit3.Name = "checkEdit3";
            this.checkEdit3.Properties.Caption = "Planilha de ponto";
            this.checkEdit3.Size = new System.Drawing.Size(126, 19);
            this.checkEdit3.TabIndex = 3;
            // 
            // dCondominiosCampos
            // 
            this.dCondominiosCampos.DataSetName = "dCondominiosCampos";
            this.dCondominiosCampos.EnforceConstraints = false;
            this.dCondominiosCampos.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // TabDP
            // 
            this.Controls.Add(this.groupControl1);
            this.LookAndFeel.UseDefaultLookAndFeel = false;
            this.Name = "TabDP";
            this.Size = new System.Drawing.Size(552, 306);
            ((System.ComponentModel.ISupportInitialize)(this.StyleController_F)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl1)).EndInit();
            this.groupControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.groupControl3)).EndInit();
            this.groupControl3.ResumeLayout(false);
            this.groupControl3.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.cONCodigoBancoCCSpinEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cONDOMINIOSBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl2)).EndInit();
            this.groupControl2.ResumeLayout(false);
            this.groupControl2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit1.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cONCodigoFolha1SpinEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl4)).EndInit();
            this.groupControl4.ResumeLayout(false);
            this.groupControl4.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtSequencialRemessaTributos.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit2.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit2.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit1.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl5)).EndInit();
            this.groupControl5.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit4.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit3.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dCondominiosCampos)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraEditors.GroupControl groupControl1;
        private DevExpress.XtraEditors.GroupControl groupControl2;
        private DevExpress.XtraEditors.GroupControl groupControl3;
        private dCondominiosCampos dCondominiosCampos;
        private System.Windows.Forms.BindingSource cONDOMINIOSBindingSource;
        private DevExpress.XtraEditors.SpinEdit cONCodigoBancoCCSpinEdit;
        private DevExpress.XtraEditors.SpinEdit cONCodigoFolha1SpinEdit;
        private DevExpress.XtraEditors.GroupControl groupControl4;
        private DevExpress.XtraEditors.CheckEdit checkEdit2;
        private DevExpress.XtraEditors.CheckEdit checkEdit1;
        private DevExpress.XtraEditors.CheckEdit checkEdit3;
        private DevExpress.XtraEditors.GroupControl groupControl5;
        private DevExpress.XtraEditors.CheckEdit checkEdit4;
        private DevExpress.XtraEditors.TextEdit textEdit1;
        private DevExpress.XtraEditors.TextEdit textEdit2;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Button cmdIniciarRemessa;
        private DevExpress.XtraEditors.TextEdit txtSequencialRemessaTributos;
    }
}
