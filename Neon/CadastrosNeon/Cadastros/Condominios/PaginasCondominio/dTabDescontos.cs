﻿namespace Cadastros.Condominios.PaginasCondominio {


    partial class dTabDescontos
    {
        //public dTabDescontos() 
        //{
        //    VirMSSQL.TableAdapter.AjustaAutoInc(this);
        //}

        

        private dTabDescontosTableAdapters.APARTAMENTOSTableAdapter aPARTAMENTOSTableAdapter;
        /// <summary>
        /// TableAdapter padrão para tabela: APARTAMENTOS
        /// </summary>
        public dTabDescontosTableAdapters.APARTAMENTOSTableAdapter APARTAMENTOSTableAdapter
        {
            get
            {
                if (aPARTAMENTOSTableAdapter == null)
                {
                    aPARTAMENTOSTableAdapter = new dTabDescontosTableAdapters.APARTAMENTOSTableAdapter();
                    aPARTAMENTOSTableAdapter.TrocarStringDeConexao();
                };
                return aPARTAMENTOSTableAdapter;
            }
        }

        private dTabDescontosTableAdapters.DescontoAPartamentoTableAdapter descontoAPartamentoTableAdapter;
        /// <summary>
        /// TableAdapter padrão para tabela: DescontoAPartamento
        /// </summary>
        public dTabDescontosTableAdapters.DescontoAPartamentoTableAdapter DescontoAPartamentoTableAdapter
        {
            get
            {
                if (descontoAPartamentoTableAdapter == null)
                {
                    descontoAPartamentoTableAdapter = new dTabDescontosTableAdapters.DescontoAPartamentoTableAdapter();
                    descontoAPartamentoTableAdapter.TrocarStringDeConexao();
                };
                return descontoAPartamentoTableAdapter;
            }
        }


        
        
    }
}
