﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using CadastrosProc.Faturamento;

namespace Cadastros.Condominios.PaginasCondominio
{
    public partial class TabFaturamento : CompontesBasicos.ComponenteTabCampos
    {
        public TabFaturamento()
        {
            InitializeComponent();
        }

        private dInsumos _dInsumos;

        private dInsumos DInsumos => _dInsumos ?? (_dInsumos = new dInsumos());

        private cCondominiosCampos2 Chamador
        {
            get { return (cCondominiosCampos2)TabMae; }
        }

        private cCondominiosCampos2 Pai { get { return (cCondominiosCampos2)Chamador; } }

        private void TabFaturamento_Load(object sender, EventArgs e)
        {
            DInsumos.INSumosTableAdapter.Fill(DInsumos.INSumos);
            DInsumos.REPassesTableAdapter.FillByCON(DInsumos.REPasses, Pai.pk);
            foreach (dInsumos.INSumosRow INSrow in DInsumos.INSumos)
                if (INSrow.REPassesRow != null)
                {
                    INSrow.REPQuantidade = INSrow.REPassesRow.REPQuantidade;
                    if (INSrow.REPassesRow.IsREPValorDifNull())
                    {
                        INSrow.VALORefetivo = INSrow.INSValor;
                    }
                    else
                    {
                        INSrow.VALORefetivo = INSrow.REPassesRow.REPValorDif;
                        INSrow.REPValorDif = INSrow.REPassesRow.REPValorDif;
                    }
                }
                else
                    INSrow.VALORefetivo = INSrow.INSValor;
            rEPassesBindingSource.DataSource = DInsumos;
        }

        private void gridView1_RowUpdated(object sender, DevExpress.XtraGrid.Views.Base.RowObjectEventArgs e)
        {
            DataRowView DRV = (DataRowView)e.Row;
            if (DRV == null)
                return;
            dInsumos.INSumosRow INSrow = (dInsumos.INSumosRow)(DRV.Row);
            if (INSrow == null)
                return;
            dInsumos.REPassesRow REProw;
            if (INSrow.REPQuantidade == 0)
            {
                if (INSrow.REPassesRow != null)
                {
                    INSrow.REPassesRow.Delete();
                    DInsumos.REPassesTableAdapter.Update(DInsumos.REPasses);
                }
            }
            else
            {
                if (INSrow.REPassesRow == null)
                {
                    REProw = DInsumos.REPasses.NewREPassesRow();
                    REProw.REP_CON = Pai.pk;
                    REProw.REPQuantidade = INSrow.REPQuantidade;
                    REProw.REP_INS = INSrow.INS;
                    if (!INSrow.IsREPValorDifNull() && (INSrow.REPValorDif != 0))
                    {
                        REProw.REPValorDif = INSrow.REPValorDif;
                        INSrow.VALORefetivo = INSrow.REPValorDif;
                    }
                    else
                        INSrow.VALORefetivo = INSrow.INSValor;
                    DInsumos.REPasses.AddREPassesRow(REProw);
                }
                else
                {
                    REProw = INSrow.REPassesRow;
                    REProw.REPQuantidade = INSrow.REPQuantidade;
                    if (!INSrow.IsREPValorDifNull() && (INSrow.REPValorDif != 0))
                    {
                        INSrow.REPassesRow.REPValorDif = INSrow.REPValorDif;
                        INSrow.VALORefetivo = INSrow.REPValorDif;
                    }
                    else
                    {
                        INSrow.REPassesRow.SetREPValorDifNull();
                        INSrow.VALORefetivo = INSrow.INSValor;
                    }
                }
                DInsumos.REPassesTableAdapter.Update(REProw);
            }
        }

        private void repositoryItemCalcEditcomDEL_ButtonClick(object sender, DevExpress.XtraEditors.Controls.ButtonPressedEventArgs e)
        {
            if (e.Button.Kind == DevExpress.XtraEditors.Controls.ButtonPredefines.Delete)
            {
                dInsumos.INSumosRow INSrow = (dInsumos.INSumosRow)gridView1.GetFocusedDataRow();
                if (INSrow != null)
                {
                    if (INSrow.REPassesRow != null)
                    {
                        INSrow.REPassesRow.SetREPValorDifNull();
                        INSrow.VALORefetivo = INSrow.INSValor;
                    }
                }
            }
        }
    }
}
