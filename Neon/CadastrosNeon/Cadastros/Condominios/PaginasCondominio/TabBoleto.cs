/*
MR - 15/03/2016 10:00 -           - Inclus�o do campo de tamanho de fonte para Quadro Inadimplente (Altera��es indicadas por *** MRC - INICIO (15/03/2016 10:00) ***)
*/

using System;
using System.Data;

namespace Cadastros.Condominios.PaginasCondominio
{
    /// <summary>
    /// 
    /// </summary>
    public partial class TabBoleto : CompontesBasicos.ComponenteTabCampos
    {
        /// <summary>
        /// 
        /// </summary>
        public TabBoleto()
        {
            InitializeComponent();
        }

        //*** MRC - INICIO (15/03/2016 10:00) ***
        private void TabBoleto_Load(object sender, EventArgs e)
        {
            Framework.Enumeracoes.VirEnumInaTamanhoFonte.CarregaEditorDaGrid(ComboInaTamanhoFonteDefault);         
        }
        //*** MRC - TERMINO (15/03/2016 10:00) ***
        
        private void MostraExemplo() {
            string TextoUnidadesI="";
            int Agrupamento = 0;
            bool NumeroDeUnidades = chQuantidade.Checked;
            bool ValorOriginal = chVo.Checked;
            bool ValorCorrigido = chVc.Checked;
            int DestacarUnidadesInadDoMes = 0;
            int RefData = 0;
            int Carencia = 0;

            try
            {
                Agrupamento = (int)RadAgrupamento.EditValue;
                NumeroDeUnidades = chQuantidade.Checked;
                ValorOriginal = chVo.Checked;
                ValorCorrigido = chVc.Checked;
                DestacarUnidadesInadDoMes = (int)radInadMes.EditValue;
                RefData = (int)radDataRef.EditValue;
                Carencia = (int)spinEdit1.Value;
            }
            catch { };

            switch (RefData)
            {
                case 0:                    
                    memoTitulo.Text = "UNIDADES INADIMPLENTES a mais de " + Carencia.ToString() + " dias (dados apurados em " + DateTime.Today.ToString("dd/MM/yyyy") + ")";
                    break;
                case 1:
                case 2:
                    memoTitulo.Text = "UNIDADES INADIMPLENTES - Data base: XX/XX/XXXX (dados apurados em " + DateTime.Today.ToString("dd/MM/yyyy") + ")";
                    break;                
            };

            switch (Agrupamento)
            {
                case 0:
                    memoTitulo.Text = memoEdit1.Text = "";
                    return;
                case 1://Lista
                    for (int linha = 1; linha < 5; linha++)
                    {
                        for (int i = 1; i < 5; i++)
                        {
                            string novogrupo = "A"+linha.ToString()+"-000"+i.ToString();
                            if (NumeroDeUnidades)
                                novogrupo += " Boletos = " + "05";
                            if (ValorOriginal)
                                novogrupo += " Valor = R$ 10.000,00";
                            if (ValorCorrigido)
                                novogrupo += " Corrigido = R$ 10.000,00";
                            novogrupo += "; ";
                            TextoUnidadesI += novogrupo;

                        };
                        TextoUnidadesI += "\r\n";
                    }
                    break;
                    
                case 2://Blocos
                    // ConjuntoLinhaBloco ConjB = new ConjuntoLinhaBloco();

                    //foreach (LinhaQuadroInadimplente Linha in Conj)
                    //    ConjB.Add(Linha.ValorOriginal, Linha.ValorCorrigido, Linha.Bloco);
                    TextoUnidadesI = "";
                    for (int coluna =1;coluna < 5;coluna++)
                    {
                        TextoUnidadesI += "Bloco A" + coluna.ToString() + ": ";
                        if (NumeroDeUnidades)
                            TextoUnidadesI += "Unidades: 05" + " ";
                        if (ValorOriginal)
                            TextoUnidadesI += "Val.: R$ 10.000,00" + " ";
                        if (ValorCorrigido)
                            TextoUnidadesI += "com encargos: R$ 10.000,00";
                        TextoUnidadesI += "\r\n";
                    };


                    break;
                
            case 3://Numero                
                TextoUnidadesI = "";
                if (NumeroDeUnidades)
                    TextoUnidadesI += "Unidades: 06" + "\r\n";
                if (ValorOriginal)
                    TextoUnidadesI += "Valor original: R$ 10.000,00\r\n";
                if (ValorCorrigido)
                    TextoUnidadesI += "Valor original + encargos: R$ 10.000,00";
                break;
            }
            
            switch (DestacarUnidadesInadDoMes)
            {
                case 1:
                    TextoUnidadesI += "\r\nValor inadimpl�ncia do m�s: R$ 10.000,00";
                    break;
                case 2:
                    
                    TextoUnidadesI += "\r\n05 unidades inadimpl�ntes a mais de 30 dias\r\n";
                    TextoUnidadesI += "02 unidades inadimpl�tes do m�s";
                    break;
            }
            
            memoEdit1.Text = TextoUnidadesI;
        }

        private void simpleButton1_Click(object sender, EventArgs e)
        {
            MostraExemplo();
        }

        private void RadAgrupamento_EditValueChanged(object sender, EventArgs e)
        {
            MostraExemplo();
        }

        private void simpleButton1_Click_1(object sender, EventArgs e)
        {
            int Agrupamento = 0;
            bool NumeroDeUnidades = chQuantidade.Checked;
            bool ValorOriginal = chVo.Checked;
            bool ValorCorrigido = chVc.Checked;
            int DestacarUnidadesInadDoMes = 0;
            int RefData = 0;
            int Carencia = 0;

            bool Cancelar = false;

            if (RadAgrupamento.EditValue == DBNull.Value)
            {
                RadAgrupamento.ErrorText = "Indefinido";
                Cancelar = true;
            }              
            if (radInadMes.EditValue == DBNull.Value)
            {
                radInadMes.ErrorText = "Indefinido";
                Cancelar = true;                
            }
            if (radDataRef.EditValue == DBNull.Value){
                radDataRef.ErrorText = "Indefinido";
                Cancelar = true;
            }

            if (Cancelar)
                return;

            try
            {
                Agrupamento = (int)RadAgrupamento.EditValue;
                NumeroDeUnidades = chQuantidade.Checked;
                ValorOriginal = chVo.Checked;
                ValorCorrigido = chVc.Checked;
                DestacarUnidadesInadDoMes = (int)radInadMes.EditValue;
                RefData = (int)radDataRef.EditValue;
                Carencia = (int)spinEdit1.Value;
            }
            catch { };
            DateTime DataBalancete = DateTime.Today;            
            String CODCON = ((DataRowView)(cONDOMINIOSBindingSource.Current))["CONCodigo"].ToString();
            
            int CON = (int)(((DataRowView)(cONDOMINIOSBindingSource.Current))["CON"]);

            CompontesBasicos.ComponenteBase CB = (CompontesBasicos.ComponenteBase)CompontesBasicos.ModuleInfo.ContruaObjeto("Boletos.Boleto.cBoleto");
            object[] Retorno = CB.ChamadaGenerica("QuadroInad", CON, RefData, Carencia, Agrupamento, ValorOriginal,
                                                              ValorCorrigido, NumeroDeUnidades, DestacarUnidadesInadDoMes, DataBalancete, CODCON);


            //Boletos.Boleto.Impresso.dImpBoletoBal DataSet = new Boletos.Boleto.Impresso.dImpBoletoBal();
            //Framework.objetosNeon.Competencia Compet = new Framework.objetosNeon.Competencia(DateTime.Today, CON);
            //Compet.Add(-1);
            //Boletos.Boleto.Impresso.RetornoInad Ret = Boletos.Boleto.Impresso.ImpBoletoBal.CalculaUnidadesInadimplenes(RefData, Carencia, Agrupamento, ValorOriginal, ValorCorrigido, NumeroDeUnidades, DestacarUnidadesInadDoMes, 100, DataBalancete, DataSet, CODCON, Compet);
            if ((Retorno != null) && (Retorno.Length >1))
            {
                memoTitulo.Text = (string)Retorno[0];
                memoEdit1.Text = (string)Retorno[1];
            }
        }

        private void checkEdit3_CheckedChanged(object sender, EventArgs e)
        {

        }
    }
}

