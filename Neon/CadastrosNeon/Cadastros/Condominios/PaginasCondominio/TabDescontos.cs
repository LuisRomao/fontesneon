using System;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Windows.Forms;
using CompontesBasicos;

namespace Cadastros.Condominios.PaginasCondominio
{
    /// <summary>
    /// 
    /// </summary>
    public partial class TabDescontos : ComponenteTabCampos
    {
        /// <summary>
        /// 
        /// </summary>
        public TabDescontos()
        {
            InitializeComponent();
            VirMSSQL.TableAdapter.AjustaAutoInc(dTabDescontos);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="Entrando"></param>
        public override void OnTrocaPagina(bool Entrando)
        {
            if (Entrando)            
                Le();            
            else
                Grava();
        }
       
        private void simpleButton2_Click(object sender, EventArgs e)
        {
            int nNovaColuna = (int)spinNovaData.Value;
            if (dTabDescontos.APARTAMENTOS.Columns[nNovaColuna.ToString()] == null)
                CriaColuna(nNovaColuna);
        }
        
        int PosicaoInicial;

        private int DiaRef(int diaBase)
        {
            return (diaBase <= TabMaeTipada.linhaMae.CONDiaVencimento) ? 40 + diaBase : diaBase;
        }

        private void CriaColuna(int dia)
        {
            string Titulo = string.Format("At� dia {0:n0}", dia);
            DataColumn novaColuna;
            DevExpress.XtraGrid.Columns.GridColumn DvnovaColuna = null;
            novaColuna = dTabDescontos.APARTAMENTOS.Columns.Add(dia.ToString(), typeof(decimal));
            novaColuna.Caption = Titulo;
            DvnovaColuna = gridView1.Columns.Add();
            DvnovaColuna.FieldName = dia.ToString();
            DvnovaColuna.Visible = true;
            DvnovaColuna.Width = 120;
            DvnovaColuna.Caption = Titulo;
            DvnovaColuna.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            DvnovaColuna.DisplayFormat.FormatString = "n2";
            DvnovaColuna.OptionsColumn.AllowEdit = true;
            DvnovaColuna.ColumnEdit = EditorValor;
            int PosEntrada = DvnovaColuna.VisibleIndex;
            foreach(DevExpress.XtraGrid.Columns.GridColumn col in gridView1.Columns)
            {
                int diacol;
                if(int.TryParse(col.FieldName,out diacol))
                {                    
                    if(DiaRef(diacol) > DiaRef(dia))
                        if(PosEntrada > col.VisibleIndex)
                            PosEntrada = col.VisibleIndex;
                }
                
            }
            DvnovaColuna.VisibleIndex = PosEntrada;
            //Posicao++;            
        }

        private cCondominiosCampos2 TabMaeTipada
        {
            get
            {
                return (cCondominiosCampos2)TabMae;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        public void Le()
        {
            dTabDescontos.APARTAMENTOSTableAdapter.Fill(dTabDescontos.APARTAMENTOS, TabMaeTipada.linhaMae.CON);
            foreach(dTabDescontos.APARTAMENTOSRow APTRowcalc in dTabDescontos.APARTAMENTOS)
            {
                APTRowcalc.calDiaCondominio = TabMaeTipada.linhaMae.CONDiaVencimento;
                if (!APTRowcalc.IsAPTDiaDiferenteNull() && !APTRowcalc.IsAPTDiaCondominioNull() && APTRowcalc.APTDiaDiferente)
                    APTRowcalc.calDiaCondominio = APTRowcalc.APTDiaCondominio;
            }
            if (PosicaoInicial == 0)
                PosicaoInicial = dTabDescontos.APARTAMENTOS.Columns.Count;
            dTabDescontos.DescontoAPartamentoTableAdapter.FillByCON(dTabDescontos.DescontoAPartamento, TabMaeTipada.linhaMae.CON);            
            foreach (dTabDescontos.DescontoAPartamentoRow rowDAP in dTabDescontos.DescontoAPartamento)
            {
                if (dTabDescontos.APARTAMENTOS.Columns[rowDAP.DAPdia.ToString()] == null)
                    CriaColuna(rowDAP.DAPdia);               
                dTabDescontos.APARTAMENTOSRow APTRow = dTabDescontos.APARTAMENTOS.FindByAPT(rowDAP.DAP_APT);                
                APTRow[rowDAP.DAPdia.ToString()] = rowDAP.DAPValor;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        public void Grava() 
        {
            Validate();
            dTabDescontosBindingSource.EndEdit();
            foreach (dTabDescontos.APARTAMENTOSRow APTRow in dTabDescontos.APARTAMENTOS) 
            {
                if (APTRow.RowState != DataRowState.Unchanged)                
                    foreach (DataColumn ColunaX in dTabDescontos.APARTAMENTOS.Columns)
                    {

                        if (ColunaX.Ordinal >= PosicaoInicial)
                        {
                            decimal NovoValor = 0;
                            if (APTRow[ColunaX] != DBNull.Value)
                                NovoValor = (decimal)APTRow[ColunaX];
                            int DAPdia = int.Parse(ColunaX.ColumnName);
                            dTabDescontos.DescontoAPartamentoRow DAProw = null;
                            foreach (dTabDescontos.DescontoAPartamentoRow rowCandidata in dTabDescontos.DescontoAPartamento)
                            {
                                if ((rowCandidata.DAPdia == DAPdia) && (rowCandidata.DAP_APT == APTRow.APT))
                                {
                                    DAProw = rowCandidata;
                                    break;
                                }
                            };
                            if (DAProw == null)
                            {
                                if (NovoValor == 0)
                                    continue;
                                DAProw = dTabDescontos.DescontoAPartamento.NewDescontoAPartamentoRow();
                                DAProw.DAP_APT = APTRow.APT;
                                DAProw.DAPdia = DAPdia;
                                DAProw.DAPValor = 0;
                                dTabDescontos.DescontoAPartamento.AddDescontoAPartamentoRow(DAProw);
                            };
                            if (DAProw.DAPValor != NovoValor)
                                DAProw.DAPValor = NovoValor;

                        }
                    };

            };
            try
            {
                VirMSSQL.TableAdapter.AbreTrasacaoSQL("Cadastros TabDescontos - 151",dTabDescontos.DescontoAPartamentoTableAdapter);
                dTabDescontos.DescontoAPartamentoTableAdapter.Update(dTabDescontos.DescontoAPartamento);
                dTabDescontos.DescontoAPartamentoTableAdapter.Limpa0();
                VirMSSQL.TableAdapter.STTableAdapter.Commit();
            }
            catch (Exception e) 
            {
                MessageBox.Show("Erro ao gravar!\r\nA grava��o n�o foi feita");
                VirMSSQL.TableAdapter.STTableAdapter.Vircatch(e);
            }
        }

        private void gridView1_RowCellStyle(object sender, DevExpress.XtraGrid.Views.Grid.RowCellStyleEventArgs e)
        {
            if (e.Column == colcalDiaCondominio)
            {
                dTabDescontos.APARTAMENTOSRow APTRowcalc = (dTabDescontos.APARTAMENTOSRow)gridView1.GetDataRow(e.RowHandle);
                if (APTRowcalc != null)
                    if (!APTRowcalc.IsAPTDiaDiferenteNull() && !APTRowcalc.IsAPTDiaCondominioNull() && APTRowcalc.APTDiaDiferente)
                        e.Appearance.ForeColor = Color.Red;
            }
        }

        private void EditorValor_Validating(object sender, CancelEventArgs e)
        {            
            DevExpress.XtraEditors.CalcEdit Editor = (DevExpress.XtraEditors.CalcEdit)sender;
            e.Cancel = (Editor.Value < 0);
        }

        private void EditorValor_ButtonClick(object sender, DevExpress.XtraEditors.Controls.ButtonPressedEventArgs e)
        {
            if (e.Button.Kind == DevExpress.XtraEditors.Controls.ButtonPredefines.Delete)
            {
                DevExpress.XtraEditors.CalcEdit Editor = (DevExpress.XtraEditors.CalcEdit)sender;
                Editor.EditValue = null;
            };
        }

        private void simpleButton1_Click(object sender, EventArgs e)
        {
            if (calcEdit1.Value >= 0)
            {
                Array.ForEach(gridView1.GetSelectedCells(), delegate(DevExpress.XtraGrid.Views.Base.GridCell celula) 
                {
                    dTabDescontos.APARTAMENTOSRow APTRow = (dTabDescontos.APARTAMENTOSRow)gridView1.GetDataRow(celula.RowHandle);
                    APTRow[celula.Column.FieldName] = calcEdit1.Value;                    
                });
            }
        }

    }
}
