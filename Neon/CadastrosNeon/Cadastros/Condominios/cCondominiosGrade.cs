/*
MR - 30/07/2014 16:00            - Ajusta das nomenclaturas de Sal�rio Eletr�nico para n�o confundir com Pagamento Eletr�nico
*/

using Framework;
using FrameworkProc.datasets;
using System;
using System.Data;
using System.Drawing;
using System.Windows.Forms;
using VirEnumeracoesNeon;

namespace Cadastros.Condominios
{
    /// <summary>
    /// 
    /// </summary>
    public partial class cCondominiosGrade : CompontesBasicos.ComponenteGradeNavegadorP
    {
        private CadastrosProc.Fornecedores.dFornecedoresLookup dFornecedoresLookup;
        private dCondominiosAtivos dCondominiosAtivos;

        /// <summary>
        /// Construtor
        /// </summary>
        public cCondominiosGrade()
        {
            InitializeComponent();
            Carrega(false);
            BindingSource_F.DataSource = dCondominiosAtivos;
            BindingSource_F.DataMember = "CONDOMINIOS";
            sindicosBindingSource.DataSource = dCondominiosAtivos;
            sindicosBindingSource.DataMember = "Sindicos";
            dFornecedoresLookup = new CadastrosProc.Fornecedores.dFornecedoresLookup();
            dFornecedoresLookup.FRNLookupTableAdapter.FillSemRAV(dFornecedoresLookup.FRNLookup);
            //Cadastros.Fornecedores.dFornecedoresGrade ds = Cadastros.Fornecedores.Advogados.cAdvogadosGrade.DFornecedoresGradeSTD;
            bindingSourceEscADVOGADOS.DataSource = dFornecedoresLookup;
            bindingSourceUSU.DataSource = dUSUarios.dUSUariosSt;
            bindingSourcemalotes.DataSource = Framework.datasets.dMALote.dMALoteSt;
            BindingSourcedPlanosSeguroConteudo.DataSource = Framework.datasets.dPlanosSeguroConteudo.dPlanosSeguroConteudoSt;
            Enumeracoes.virEnumCONStatus.virEnumCONStatusSt.CarregaEditorDaGrid(colCONStatus);
            GridView_F.OptionsBehavior.Editable = true;
        }

        private void Carrega(Boolean Todos)
        {
            if (dCondominiosAtivos == null)
                dCondominiosAtivos = new dCondominiosAtivos();
            if (Todos)
                dCondominiosAtivos.CONDOMINIOSTableAdapter.FillByTodos(dCondominiosAtivos.CONDOMINIOS);
            else
                dCondominiosAtivos.CONDOMINIOSTableAdapter.FillByAtivos(dCondominiosAtivos.CONDOMINIOS, DSCentral.EMP);
            dCondominiosAtivos.SindicosTableAdapter.Fill(dCondominiosAtivos.Sindicos);
        }

        /// <summary>
        /// 
        /// </summary>
        protected override void Alterar()
        {
            if (LinhaMae == null)
                return;
            if ((CONStatus)LinhaMae.CONStatus == CONStatus.Ativado)
                base.Visualizar();
            else
                base.Alterar();
        }

        dCondominiosAtivos.CONDOMINIOSRow LinhaMae
        {
            get
            {
                return (LinhaMae_F == null) ? null : (dCondominiosAtivos.CONDOMINIOSRow)LinhaMae_F;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected override void BtnExcluir_F_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            MessageBox.Show("Esta funcionalidade est� desabilitada temporariamente.", "Excluir", MessageBoxButtons.OK, MessageBoxIcon.Warning);
        }

        /// <summary>
        /// 
        /// </summary>
        public override void RefreshDadosManual()
        {
            //ocorre por iniciativa do usu�rio clicando no bot�o
            if (BindingSource_F.DataSource is Type)
                return;
            if (BindingSource_F.Current == null)
                pk = -1;
            else
            {
                DataRowView DRV = (DataRowView)BindingSource_F.Current;
                if (DRV.Row != null)
                {
                    if (DRV.Row.RowState != DataRowState.Unchanged)
                        return;
                    pk = (int)DRV[0];
                }
                else
                    pk = -1;

            };
            //FrameworkProc.datasets.dCondominiosAtivos Source = (FrameworkProc.datasets.dCondominiosAtivos)BindingSource_F.DataSource;
            //Source.CarregaCondominios();
            Carrega(barButtonItem3.Down);
            //Framework.datasets.dCondominiosAtivos.dCondominiosAtivosST.CarregaCondominios(true);
            if ((pk != -1) && (tabela != null) && (tabela.Columns[0] != null))
                BindingSource_F.Position = BindingSource_F.Find(tabela.Columns[0].ColumnName, pk);
        }

        /// <summary>
        /// 
        /// </summary>
        public override void RefreshDados()
        {
            //N�O REMOVER
            //� chamado na troca de p�gina e foi desativado por ser demorado, agora s� � chamado quando um condom�nio � efetivamente alterado                        
        }

        private void cCondominiosGrade_Load(object sender, EventArgs e)
        {
            BtnExportar_F.Enabled = (CompontesBasicos.Bancovirtual.UsuarioLogado.UsuarioLogadoSt.VerificaFuncionalidade("EXPORTAR_CONDOMINIOS") >= 1);
            BtnImprimir_F.Enabled = (CompontesBasicos.Bancovirtual.UsuarioLogado.UsuarioLogadoSt.VerificaFuncionalidade("EXPORTAR_CONDOMINIOS") >= 1);
        }

        private void barButtonItem1_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            CompontesBasicos.FormPrincipalBase.FormPrincipal.ShowMoludoCampos(typeof(Cadastros.Condominios.Agenda.cAgenda), LinhaMae.CONNome, LinhaMae.CON, false, null);
        }

        private void cCondominiosGrade_cargaFinal(object sender, EventArgs e)
        {
            //FrameworkProc.datasets.dCondominiosAtivos.dCondominiosAtivosST.CarregaSindicos(false);
        }



        private void botes_ButtonClick(object sender, DevExpress.XtraEditors.Controls.ButtonPressedEventArgs e)
        {
            Alterar();
            /*
            if (LinhaMae == null)
                MessageBox.Show("Nenhum condom�nio selecionado");
            else
                CompontesBasicos.FormPrincipalBase.FormPrincipal.ShowMoludoCampos(typeof(Cadastros.Condominios.Agenda.cAgenda), LinhaMae.CONNome, LinhaMae.CON, false, null);
            */
        }

        private void barButtonItem3_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            Carrega(barButtonItem3.Down);
        }

        /*
        private void ajustaDS()
        {                        
            if (!barButtonItem3.Down)
                BindingSource_F.DataSource = FrameworkProc.datasets.dCondominiosAtivos.dCondominiosAtivosST;
            else
                BindingSource_F.DataSource = FrameworkProc.datasets.dCondominiosAtivos.dCondominiosTodosST;     
        }*/

        private void GridView_F_RowCellStyle(object sender, DevExpress.XtraGrid.Views.Grid.RowCellStyleEventArgs e)
        {
            dCondominiosAtivos.CONDOMINIOSRow linha = (dCondominiosAtivos.CONDOMINIOSRow)GridView_F.GetDataRow(e.RowHandle);
            if (linha == null)
                return;
            if ((e.Column == colCONCodigo) || (e.Column == colCONStatus))
            {
                e.Appearance.BackColor = Enumeracoes.virEnumCONStatus.virEnumCONStatusSt.GetCor(linha.CONStatus);
                e.Appearance.BackColor2 = Color.White;
                e.Appearance.GradientMode = System.Drawing.Drawing2D.LinearGradientMode.ForwardDiagonal;
                e.Appearance.ForeColor = Color.Black;
            }
        }

        private void barButtonItem4_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            if (LinhaMae == null)
                return;
            if ((CONStatus)LinhaMae.CONStatus == CONStatus.Ativado)
            {
                string Motivo = "Corre��o de dados";
                if (VirInput.Input.Execute("Motivo da desativa��o:", ref Motivo, true))
                    dCondominiosAtivos.AlteraStatus(LinhaMae.CON, CONStatus.Desativado, string.Format("{0:dd:MM:yyyy HH:mm:ss} - >> CONDOM�NIO DESATIVADO POR {1}\r\nMotivo:\r\n{2}", DateTime.Now, Framework.usuarioLogado.UsuarioLogadoNeon.UsuarioLogadoSt.USUNome, Motivo));
                    //LinhaMae.CONStatus = (int)CONStatus.Desativado;
                    //if (LinhaMae.IsCONHistoricoNull())
                    //    LinhaMae.CONHistorico = "";
                    //LinhaMae.CONHistorico += string.Format("\r\n\r\n{0:dd:MM:yyyy HH:mm:ss} - >> CONDOM�NIO DESATIVADO POR {1}\r\nMotivo:\r\n{2}",DateTime.Now,Framework.usuarioLogado.UsuarioLogadoNeon.UsuarioLogadoSt.USUNome,Motivo);
                    //dCondominiosAtivos.dCondominiosAtivosST.CONDOMINIOSTableAdapter.Update(LinhaMae);
                    //LinhaMae.AcceptChanges();
            };
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="args"></param>
        /// <returns></returns>
        public override object[] ChamadaGenerica(params object[] args)
        {
            barButtonItem3.Down = true;
            Carrega(true);
            lookupGerente1.USUSel = (int)args[0];
            if (lookupGerente1.USUSel == -1)
                GridView_F.ActiveFilterString = string.Format("CONStatus = {0}", (int)CONStatus.Desativado);
            else
                GridView_F.ActiveFilterString = string.Format("CONStatus = {0} and [CONAuditor1_USU] = {1}", (int)CONStatus.Desativado, lookupGerente1.USUSel);
            return null;
        }

        private void lookupGerente1_alterado(object sender, EventArgs e)
        {
            if (lookupGerente1.USUSel == -1)
                GridView_F.ActiveFilterString = string.Empty;
            else
                GridView_F.ActiveFilterString = string.Format("[CONAuditor1_USU] = {0}", lookupGerente1.USUSel);
        }

    }
}

