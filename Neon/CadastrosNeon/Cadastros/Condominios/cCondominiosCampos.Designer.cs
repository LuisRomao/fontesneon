namespace Cadastros.Condominios
{
    partial class cCondominiosCampos
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            DevExpress.XtraGrid.GridLevelNode gridLevelNode1 = new DevExpress.XtraGrid.GridLevelNode();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(cCondominiosCampos));
            this.grvSaldos = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colSLDNome = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSLDValorSaldoAtual = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSLDValorSaldoAnterior = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSLD = new DevExpress.XtraGrid.Columns.GridColumn();
            this.grdAplicacoes = new DevExpress.XtraGrid.GridControl();
            this.fKAPLICACOESCONDOMINIOSBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.grvAplicacoes = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colAPLNome = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colAPLConta = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colAPLDataSaldoAnterior = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colAPLValorSaldoAnterior = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colAPLDataSaldoAtual = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colAPLValorSaldoAtual = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colAPL = new DevExpress.XtraGrid.Columns.GridColumn();
            this.dCondominiosCampos = new Cadastros.Condominios.dCondominiosCampos();
            this.LayoutControl = new DevExpress.XtraLayout.LayoutControl();
            this.labelControl1 = new DevExpress.XtraEditors.LabelControl();
            this.spinEdit2 = new DevExpress.XtraEditors.SpinEdit();
            this.lookUpEdit4 = new DevExpress.XtraEditors.LookUpEdit();
            this.eMPRESASBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.WebBrowser = new System.Windows.Forms.WebBrowser();
            this.txtCodigoFolha2 = new DevExpress.XtraEditors.TextEdit();
            this.textEdit12 = new DevExpress.XtraEditors.TextEdit();
            this.textEdit7 = new DevExpress.XtraEditors.TextEdit();
            this.txtCodigoFolha1 = new DevExpress.XtraEditors.TextEdit();
            this.checkEdit7 = new DevExpress.XtraEditors.CheckEdit();
            this.textEdit14 = new DevExpress.XtraEditors.TextEdit();
            this.textEdit13 = new DevExpress.XtraEditors.TextEdit();
            this.lookUpEdit1 = new DevExpress.XtraEditors.LookUpEdit();
            this.bANCOSBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.txtCNPJ_C = new DevExpress.XtraEditors.TextEdit();
            this.DataAdesao_C = new DevExpress.XtraEditors.DateEdit();
            this.chkGas_C = new DevExpress.XtraEditors.CheckEdit();
            this.txtTotalUnidades_C = new DevExpress.XtraEditors.TextEdit();
            this.txtConta_C = new DevExpress.XtraEditors.TextEdit();
            this.txtAgencia_C = new DevExpress.XtraEditors.TextEdit();
            this.txtTelefone_C = new DevExpress.XtraEditors.TextEdit();
            this.txtSindico_C = new DevExpress.XtraEditors.TextEdit();
            this.grdCorpoDiretivo = new DevExpress.XtraGrid.GridControl();
            this.cORPODIRETIVOBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.grvCorpoDiretivo = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colPESNome = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colAPTNumero1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colBLONome1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colCGONome = new DevExpress.XtraGrid.Columns.GridColumn();
            this.grdHistoricos = new DevExpress.XtraGrid.GridControl();
            this.fKHISTORICOSCONDOMINIOSBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.grvHistoricos = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colHSTAssunto = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colHSTData = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colHSTCritico = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemCheckEdit1 = new DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit();
            this.colHSTDescricao = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemMemoExEdit1 = new DevExpress.XtraEditors.Repository.RepositoryItemMemoExEdit();
            this.rgpBalanceteInternet = new DevExpress.XtraEditors.RadioGroup();
            this.chkLiberarBalanceteInternet = new DevExpress.XtraEditors.CheckEdit();
            this.chkAcordosOnLine = new DevExpress.XtraEditors.CheckEdit();
            this.memEditorInternet = new DevExpress.XtraEditors.MemoEdit();
            this.pictureEdit1 = new DevExpress.XtraEditors.PictureEdit();
            this.chkExportarInternet = new DevExpress.XtraEditors.CheckEdit();
            this.lookUpEdit6 = new DevExpress.XtraEditors.LookUpEdit();
            this.aDVOGADOSBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.lookUpEdit5 = new DevExpress.XtraEditors.LookUpEdit();
            this.spinEdit1 = new DevExpress.XtraEditors.SpinEdit();
            this.dateEdit4 = new DevExpress.XtraEditors.DateEdit();
            this.checkEdit10 = new DevExpress.XtraEditors.CheckEdit();
            this.checkEdit9 = new DevExpress.XtraEditors.CheckEdit();
            this.checkEdit6 = new DevExpress.XtraEditors.CheckEdit();
            this.dateEdit3 = new DevExpress.XtraEditors.DateEdit();
            this.dateEdit2 = new DevExpress.XtraEditors.DateEdit();
            this.dateEdit1 = new DevExpress.XtraEditors.DateEdit();
            this.checkEdit8 = new DevExpress.XtraEditors.CheckEdit();
            this.lkpSeguradoras = new DevExpress.XtraEditors.LookUpEdit();
            this.sEGURADORASBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.lookUpEdit3 = new DevExpress.XtraEditors.LookUpEdit();
            this.aUDITORESBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.lookUpEdit2 = new DevExpress.XtraEditors.LookUpEdit();
            this.lkpCorretora = new DevExpress.XtraEditors.LookUpEdit();
            this.cORRETORASEGUROSBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.rdgBoletoCorreio = new DevExpress.XtraEditors.RadioGroup();
            this.rdgCobrancaCorreio = new DevExpress.XtraEditors.RadioGroup();
            this.rdgAssembleiaCorreio = new DevExpress.XtraEditors.RadioGroup();
            this.clcCS = new DevExpress.XtraEditors.CalcEdit();
            this.clcAR = new DevExpress.XtraEditors.CalcEdit();
            this.txtRegra = new DevExpress.XtraEditors.TextEdit();
            this.chkRepassaCorreio = new DevExpress.XtraEditors.CheckEdit();
            this.spinEdit8 = new DevExpress.XtraEditors.SpinEdit();
            this.spinEdit7 = new DevExpress.XtraEditors.SpinEdit();
            this.spinEdit6 = new DevExpress.XtraEditors.SpinEdit();
            this.spinEdit5 = new DevExpress.XtraEditors.SpinEdit();
            this.spinEdit4 = new DevExpress.XtraEditors.SpinEdit();
            this.spinEdit3 = new DevExpress.XtraEditors.SpinEdit();
            this.txtPerContabilFim = new DevExpress.XtraEditors.TextEdit();
            this.spePerContabilInicio = new DevExpress.XtraEditors.SpinEdit();
            this.speVencimento = new DevExpress.XtraEditors.SpinEdit();
            this.calcEdit9 = new DevExpress.XtraEditors.CalcEdit();
            this.calcEdit8 = new DevExpress.XtraEditors.CalcEdit();
            this.calcEdit7 = new DevExpress.XtraEditors.CalcEdit();
            this.calcEdit6 = new DevExpress.XtraEditors.CalcEdit();
            this.calcEdit5 = new DevExpress.XtraEditors.CalcEdit();
            this.radioGroup4 = new DevExpress.XtraEditors.RadioGroup();
            this.checkEdit5 = new DevExpress.XtraEditors.CheckEdit();
            this.textEdit11 = new DevExpress.XtraEditors.TextEdit();
            this.label2 = new System.Windows.Forms.Label();
            this.textEdit10 = new DevExpress.XtraEditors.TextEdit();
            this.rdgFR = new DevExpress.XtraEditors.RadioGroup();
            this.textEdit9 = new DevExpress.XtraEditors.TextEdit();
            this.textEdit8 = new DevExpress.XtraEditors.TextEdit();
            this.label1 = new System.Windows.Forms.Label();
            this.lkpBancos = new DevExpress.XtraEditors.LookUpEdit();
            this.speApartamentos = new DevExpress.XtraEditors.SpinEdit();
            this.memoEdit1 = new DevExpress.XtraEditors.MemoEdit();
            this.speBlocos = new DevExpress.XtraEditors.SpinEdit();
            this.grdApartamentos = new DevExpress.XtraGrid.GridControl();
            this.fKAPARTAMENTOSBLOCOSBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.fKBLOCOSCONDOMINIOSBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.grvApartamentos = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colAPTNumero = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colAPTFracaoIdeal = new DevExpress.XtraGrid.Columns.GridColumn();
            this.grdBlocos = new DevExpress.XtraGrid.GridControl();
            this.grvBlocos = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colBLOCodigo = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colBLONome = new DevExpress.XtraGrid.Columns.GridColumn();
            this.radioGroup2 = new DevExpress.XtraEditors.RadioGroup();
            this.checkEdit4 = new DevExpress.XtraEditors.CheckEdit();
            this.checkEdit3 = new DevExpress.XtraEditors.CheckEdit();
            this.calcEdit4 = new DevExpress.XtraEditors.CalcEdit();
            this.calcEdit3 = new DevExpress.XtraEditors.CalcEdit();
            this.calcEdit2 = new DevExpress.XtraEditors.CalcEdit();
            this.calcEdit1 = new DevExpress.XtraEditors.CalcEdit();
            this.checkEdit1 = new DevExpress.XtraEditors.CheckEdit();
            this.radioGroup1 = new DevExpress.XtraEditors.RadioGroup();
            this.textEdit1 = new DevExpress.XtraEditors.TextEdit();
            this.checkEdit2 = new DevExpress.XtraEditors.CheckEdit();
            this.lkpCidades = new DevExpress.XtraEditors.LookUpEdit();
            this.cIDADESBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.lkpUF = new DevExpress.XtraEditors.LookUpEdit();
            this.textEdit5 = new DevExpress.XtraEditors.TextEdit();
            this.textEdit4 = new DevExpress.XtraEditors.TextEdit();
            this.textEdit3 = new DevExpress.XtraEditors.TextEdit();
            this.bnvHistoricos = new System.Windows.Forms.BindingNavigator(this.components);
            this.btnAddHistorico = new System.Windows.Forms.ToolStripButton();
            this.btnEditHistorico = new System.Windows.Forms.ToolStripButton();
            this.btnPrintHistorico = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator3 = new System.Windows.Forms.ToolStripSeparator();
            this.btnDelHistorico = new System.Windows.Forms.ToolStripButton();
            this.bnvCorpoDiretivo = new System.Windows.Forms.BindingNavigator(this.components);
            this.btnAddCorpoDiretivo = new System.Windows.Forms.ToolStripButton();
            this.btnEditCorpoDiretivo = new System.Windows.Forms.ToolStripButton();
            this.btnPrintCorpoDiretivo = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator7 = new System.Windows.Forms.ToolStripSeparator();
            this.btnDelCorpoDiretivo = new System.Windows.Forms.ToolStripButton();
            this.bnvBlocos = new System.Windows.Forms.BindingNavigator(this.components);
            this.btnAddBlocos = new System.Windows.Forms.ToolStripButton();
            this.btnEditBlocos = new System.Windows.Forms.ToolStripButton();
            this.btnPrintBlocos = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator5 = new System.Windows.Forms.ToolStripSeparator();
            this.btnDelBlocos = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator6 = new System.Windows.Forms.ToolStripSeparator();
            this.btnPostBlocos = new System.Windows.Forms.ToolStripButton();
            this.btnCancelBlocos = new System.Windows.Forms.ToolStripButton();
            this.bnvApartamentos = new System.Windows.Forms.BindingNavigator(this.components);
            this.btnAddApto = new System.Windows.Forms.ToolStripButton();
            this.btnEditApto = new System.Windows.Forms.ToolStripButton();
            this.btnPrintApto = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator4 = new System.Windows.Forms.ToolStripSeparator();
            this.btnDelApto = new System.Windows.Forms.ToolStripButton();
            this.textEdit2 = new DevExpress.XtraEditors.TextEdit();
            this.txtCodcon = new DevExpress.XtraEditors.TextEdit();
            this.bnvAplicacoes = new System.Windows.Forms.BindingNavigator(this.components);
            this.btnAddAplicacao = new System.Windows.Forms.ToolStripButton();
            this.btnAddSubAplicacao = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator2 = new System.Windows.Forms.ToolStripSeparator();
            this.btnEditAplicacao = new System.Windows.Forms.ToolStripButton();
            this.btnPrintAplicacao = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            this.btnDelAplicacao = new System.Windows.Forms.ToolStripButton();
            this.LayoutControlGroup = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlItem1 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem2 = new DevExpress.XtraLayout.LayoutControlItem();
            this.tcgCondominio = new DevExpress.XtraLayout.TabbedControlGroup();
            this.layoutControlGroup1 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.emptySpaceItem1 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.layoutControlGroup43 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlGroup44 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlItem95 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem96 = new DevExpress.XtraLayout.LayoutControlItem();
            this.emptySpaceItem2 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.layoutControlGroup45 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlItem94 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem97 = new DevExpress.XtraLayout.LayoutControlItem();
            this.lcgBasico = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlGroup2 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlItem3 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem6 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem4 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem8 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem7 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlGroup4 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlItem88 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem90 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlGroup27 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlItem83 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem82 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlGroup41 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlItem85 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem86 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem91 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem84 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem92 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem89 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem87 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlGroup3 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlItem5 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlGroup5 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlItem11 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem10 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem12 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlGroup10 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlGroup6 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlItem13 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem14 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlGroup9 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlItem20 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem19 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlGroup7 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlItem16 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem15 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlGroup8 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlItem17 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem18 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlGroup11 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlItem22 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem25 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem9 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlGroup12 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlItem21 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem27 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem23 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlGroup13 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlItem24 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem81 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlGroup14 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlGroup15 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlItem26 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem28 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem29 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem30 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem32 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem33 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem34 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem35 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlGroup16 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlItem31 = new DevExpress.XtraLayout.LayoutControlItem();
            this.lciValorFR = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlGroup17 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlItem37 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem36 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem39 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlGroup19 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlItem42 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem40 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlGroup18 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlItem43 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem44 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlGroup24 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlItem41 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem48 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlGroup25 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlItem49 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem50 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem51 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem52 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem53 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlGroup26 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlItem54 = new DevExpress.XtraLayout.LayoutControlItem();
            this.lcgCorreio = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlGroup20 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlItem45 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem47 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem46 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlGroup21 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlItem55 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlGroup22 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlItem56 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlGroup23 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlItem57 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlGroup28 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlItem38 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem58 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlGroup29 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlGroup30 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlItem60 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem61 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlGroup31 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlItem62 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem59 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlGroup32 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlItem64 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem65 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem63 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlGroup33 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlItem72 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem73 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem69 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem66 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem68 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlGroup34 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlItem71 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem70 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem67 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem93 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlGroup35 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlGroup36 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlItem75 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem74 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlGroup37 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlItem79 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlGroup38 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlItem77 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem78 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlGroup39 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.tcgEditorInternet = new DevExpress.XtraLayout.TabbedControlGroup();
            this.layoutControlGroup40 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.lciEditorInternet = new DevExpress.XtraLayout.LayoutControlItem();
            this.lcgVisualizadorInternet = new DevExpress.XtraLayout.LayoutControlGroup();
            this.lciVisualizadorInternet = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlGroup42 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlItem76 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem80 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlGroup46 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlItem99 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem100 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem98 = new DevExpress.XtraLayout.LayoutControlItem();
            this.cONDOMINIOSTableAdapter = new Cadastros.Condominios.dCondominiosCamposTableAdapters.CONDOMINIOSTableAdapter();
            this.bLOCOSTableAdapter = new Cadastros.Condominios.dCondominiosCamposTableAdapters.BLOCOSTableAdapter();
            this.cIDADESTableAdapter = new Cadastros.Condominios.dCondominiosCamposTableAdapters.CIDADESTableAdapter();
            this.aPARTAMENTOSTableAdapter = new Cadastros.Condominios.dCondominiosCamposTableAdapters.APARTAMENTOSTableAdapter();
            this.bANCOSTableAdapter = new Cadastros.Condominios.dCondominiosCamposTableAdapters.BANCOSTableAdapter();
            this.aPLICACOESTableAdapter = new Cadastros.Condominios.dCondominiosCamposTableAdapters.APLICACOESTableAdapter();
            this.fKSALDOSAPLICACOESBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.sALDOSTableAdapter = new Cadastros.Condominios.dCondominiosCamposTableAdapters.SALDOSTableAdapter();
            this.aDVOGADOSTableAdapter = new Cadastros.Condominios.dCondominiosCamposTableAdapters.ADVOGADOSTableAdapter();
            
            
            this.cORPODIRETIVOTableAdapter = new Cadastros.Condominios.dCondominiosCamposTableAdapters.CORPODIRETIVOTableAdapter();
            this.eMPRESASTableAdapter = new Cadastros.Condominios.dCondominiosCamposTableAdapters.EMPRESASTableAdapter();
            this.fKBLOCOSCONDOMINIOSBindingSource1 = new System.Windows.Forms.BindingSource(this.components);
            ((System.ComponentModel.ISupportInitialize)(this.BarManager_F)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.BarAndDockingController_F)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.BindingSource_F)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.StyleController_F)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.grvSaldos)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.grdAplicacoes)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.fKAPLICACOESCONDOMINIOSBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.grvAplicacoes)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dCondominiosCampos)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LayoutControl)).BeginInit();
            this.LayoutControl.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.spinEdit2.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lookUpEdit4.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.eMPRESASBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCodigoFolha2.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit12.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit7.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCodigoFolha1.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit7.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit14.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit13.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lookUpEdit1.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.bANCOSBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCNPJ_C.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DataAdesao_C.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkGas_C.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTotalUnidades_C.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtConta_C.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtAgencia_C.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTelefone_C.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtSindico_C.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.grdCorpoDiretivo)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cORPODIRETIVOBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.grvCorpoDiretivo)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.grdHistoricos)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.fKHISTORICOSCONDOMINIOSBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.grvHistoricos)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckEdit1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoExEdit1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.rgpBalanceteInternet.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkLiberarBalanceteInternet.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkAcordosOnLine.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.memEditorInternet.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureEdit1.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkExportarInternet.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lookUpEdit6.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.aDVOGADOSBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lookUpEdit5.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.spinEdit1.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEdit4.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit10.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit9.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit6.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEdit3.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEdit2.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEdit1.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit8.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lkpSeguradoras.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sEGURADORASBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lookUpEdit3.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.aUDITORESBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lookUpEdit2.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lkpCorretora.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cORRETORASEGUROSBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.rdgBoletoCorreio.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.rdgCobrancaCorreio.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.rdgAssembleiaCorreio.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.clcCS.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.clcAR.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtRegra.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkRepassaCorreio.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.spinEdit8.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.spinEdit7.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.spinEdit6.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.spinEdit5.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.spinEdit4.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.spinEdit3.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtPerContabilFim.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.spePerContabilInicio.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.speVencimento.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.calcEdit9.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.calcEdit8.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.calcEdit7.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.calcEdit6.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.calcEdit5.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radioGroup4.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit5.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit11.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit10.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.rdgFR.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit9.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit8.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lkpBancos.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.speApartamentos.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.memoEdit1.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.speBlocos.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.grdApartamentos)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.fKAPARTAMENTOSBLOCOSBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.fKBLOCOSCONDOMINIOSBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.grvApartamentos)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.grdBlocos)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.grvBlocos)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radioGroup2.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit4.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit3.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.calcEdit4.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.calcEdit3.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.calcEdit2.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.calcEdit1.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit1.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radioGroup1.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit1.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit2.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lkpCidades.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cIDADESBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lkpUF.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit5.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit4.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit3.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.bnvHistoricos)).BeginInit();
            this.bnvHistoricos.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.bnvCorpoDiretivo)).BeginInit();
            this.bnvCorpoDiretivo.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.bnvBlocos)).BeginInit();
            this.bnvBlocos.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.bnvApartamentos)).BeginInit();
            this.bnvApartamentos.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit2.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCodcon.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.bnvAplicacoes)).BeginInit();
            this.bnvAplicacoes.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.LayoutControlGroup)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tcgCondominio)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup43)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup44)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem95)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem96)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup45)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem94)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem97)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lcgBasico)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem8)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem88)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem90)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup27)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem83)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem82)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup41)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem85)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem86)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem91)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem84)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem92)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem89)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem87)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem11)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem10)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem12)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup10)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem13)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem14)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup9)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem20)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem19)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem16)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem15)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup8)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem17)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem18)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup11)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem22)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem25)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem9)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup12)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem21)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem27)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem23)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup13)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem24)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem81)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup14)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup15)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem26)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem28)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem29)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem30)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem32)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem33)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem34)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem35)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup16)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem31)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lciValorFR)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup17)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem37)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem36)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem39)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup19)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem42)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem40)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup18)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem43)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem44)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup24)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem41)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem48)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup25)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem49)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem50)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem51)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem52)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem53)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup26)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem54)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lcgCorreio)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup20)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem45)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem47)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem46)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup21)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem55)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup22)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem56)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup23)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem57)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup28)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem38)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem58)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup29)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup30)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem60)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem61)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup31)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem62)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem59)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup32)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem64)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem65)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem63)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup33)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem72)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem73)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem69)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem66)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem68)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup34)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem71)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem70)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem67)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem93)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup35)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup36)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem75)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem74)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup37)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem79)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup38)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem77)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem78)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup39)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tcgEditorInternet)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup40)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lciEditorInternet)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lcgVisualizadorInternet)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lciVisualizadorInternet)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup42)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem76)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem80)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup46)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem99)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem100)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem98)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.fKSALDOSAPLICACOESBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.fKBLOCOSCONDOMINIOSBindingSource1)).BeginInit();
            this.SuspendLayout();
            // 
            // barStatus_F
            // 
            this.barStatus_F.OptionsBar.AllowQuickCustomization = false;
            this.barStatus_F.OptionsBar.DisableClose = true;
            this.barStatus_F.OptionsBar.DisableCustomization = true;
            this.barStatus_F.OptionsBar.DrawDragBorder = false;
            this.barStatus_F.OptionsBar.DrawSizeGrip = true;
            this.barStatus_F.OptionsBar.UseWholeRow = true;
            // 
            // barComponente_F
            // 
            this.barComponente_F.OptionsBar.AllowQuickCustomization = false;
            this.barComponente_F.OptionsBar.DisableClose = true;
            this.barComponente_F.OptionsBar.DisableCustomization = true;
            this.barComponente_F.OptionsBar.DrawDragBorder = false;
            this.barComponente_F.OptionsBar.UseWholeRow = true;
            // 
            // btnFechar_F
            // 
            this.btnFechar_F.Visibility = DevExpress.XtraBars.BarItemVisibility.Never;
            // 
            // BarAndDockingController_F
            // 
            this.BarAndDockingController_F.PropertiesBar.AllowLinkLighting = false;
            // 
            // BindingSource_F
            // 
            this.BindingSource_F.DataMember = "CONDOMINIOS";
            this.BindingSource_F.DataSource = this.dCondominiosCampos;
            // 
            // ToolTipController_F
            // 
            this.ToolTipController_F.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(222)))), ((int)(((byte)(237)))), ((int)(((byte)(250)))));
            this.ToolTipController_F.Appearance.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(20)))), ((int)(((byte)(69)))), ((int)(((byte)(141)))));
            this.ToolTipController_F.Appearance.Options.UseBackColor = true;
            this.ToolTipController_F.Appearance.Options.UseFont = true;
            this.ToolTipController_F.Appearance.Options.UseForeColor = true;
            this.ToolTipController_F.Appearance.Options.UseTextOptions = true;
            this.ToolTipController_F.Appearance.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            // 
            // StyleController_F
            // 
            this.StyleController_F.AppearanceDisabled.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.StyleController_F.AppearanceDisabled.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.StyleController_F.AppearanceDisabled.Options.UseBackColor = true;
            this.StyleController_F.AppearanceDisabled.Options.UseForeColor = true;
            this.StyleController_F.AppearanceFocused.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(253)))), ((int)(((byte)(253)))), ((int)(((byte)(181)))));
            this.StyleController_F.AppearanceFocused.Options.UseBackColor = true;
            
            // 
            // grvSaldos
            // 
            this.grvSaldos.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colSLDNome,
            this.colSLDValorSaldoAtual,
            this.colSLDValorSaldoAnterior,
            this.colSLD});
            this.grvSaldos.GridControl = this.grdAplicacoes;
            this.grvSaldos.Name = "grvSaldos";
            this.grvSaldos.OptionsBehavior.Editable = false;
            this.grvSaldos.OptionsView.ShowGroupPanel = false;
            this.grvSaldos.ViewCaption = ":: Sub-Contas ::";
            this.grvSaldos.Click += new System.EventHandler(this.grvSaldos_Click);
            this.grvSaldos.GotFocus += new System.EventHandler(this.grvSaldos_GotFocus);
            // 
            // colSLDNome
            // 
            this.colSLDNome.Caption = "Nome";
            this.colSLDNome.FieldName = "SLDNome";
            this.colSLDNome.Name = "colSLDNome";
            this.colSLDNome.Visible = true;
            this.colSLDNome.VisibleIndex = 0;
            // 
            // colSLDValorSaldoAtual
            // 
            this.colSLDValorSaldoAtual.Caption = "Vr Saldo Atual";
            this.colSLDValorSaldoAtual.DisplayFormat.FormatString = "n2";
            this.colSLDValorSaldoAtual.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.colSLDValorSaldoAtual.FieldName = "SLDValorSaldoAtual";
            this.colSLDValorSaldoAtual.Name = "colSLDValorSaldoAtual";
            this.colSLDValorSaldoAtual.Visible = true;
            this.colSLDValorSaldoAtual.VisibleIndex = 2;
            // 
            // colSLDValorSaldoAnterior
            // 
            this.colSLDValorSaldoAnterior.Caption = "Vr Saldo Anterior";
            this.colSLDValorSaldoAnterior.DisplayFormat.FormatString = "n2";
            this.colSLDValorSaldoAnterior.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.colSLDValorSaldoAnterior.FieldName = "SLDValorSaldoAnterior";
            this.colSLDValorSaldoAnterior.Name = "colSLDValorSaldoAnterior";
            this.colSLDValorSaldoAnterior.Visible = true;
            this.colSLDValorSaldoAnterior.VisibleIndex = 1;
            // 
            // colSLD
            // 
            this.colSLD.Caption = "colSLD";
            this.colSLD.FieldName = "SLD";
            this.colSLD.Name = "colSLD";
            this.colSLD.OptionsColumn.ShowInCustomizationForm = false;
            // 
            // grdAplicacoes
            // 
            this.grdAplicacoes.DataSource = this.fKAPLICACOESCONDOMINIOSBindingSource;
            this.grdAplicacoes.EmbeddedNavigator.Name = "";
            gridLevelNode1.LevelTemplate = this.grvSaldos;
            gridLevelNode1.RelationName = "FK_SALDOS_APLICACOES";
            this.grdAplicacoes.LevelTree.Nodes.AddRange(new DevExpress.XtraGrid.GridLevelNode[] {
            gridLevelNode1});
            this.grdAplicacoes.Location = new System.Drawing.Point(16, 75);
            this.grdAplicacoes.MainView = this.grvAplicacoes;
            this.grdAplicacoes.Name = "grdAplicacoes";
            this.grdAplicacoes.Size = new System.Drawing.Size(0, 0);
            this.grdAplicacoes.TabIndex = 81;
            this.grdAplicacoes.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.grvAplicacoes,
            this.grvSaldos});
            // 
            // fKAPLICACOESCONDOMINIOSBindingSource
            // 
            this.fKAPLICACOESCONDOMINIOSBindingSource.DataSource = this.BindingSource_F;
            // 
            // grvAplicacoes
            // 
            this.grvAplicacoes.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colAPLNome,
            this.colAPLConta,
            this.colAPLDataSaldoAnterior,
            this.colAPLValorSaldoAnterior,
            this.colAPLDataSaldoAtual,
            this.colAPLValorSaldoAtual,
            this.colAPL});
            this.grvAplicacoes.GridControl = this.grdAplicacoes;
            this.grvAplicacoes.Name = "grvAplicacoes";
            this.grvAplicacoes.OptionsBehavior.Editable = false;
            this.grvAplicacoes.OptionsCustomization.AllowFilter = false;
            this.grvAplicacoes.OptionsCustomization.AllowGroup = false;
            this.grvAplicacoes.OptionsMenu.EnableFooterMenu = false;
            this.grvAplicacoes.OptionsMenu.EnableGroupPanelMenu = false;
            this.grvAplicacoes.OptionsPrint.ExpandAllDetails = true;
            this.grvAplicacoes.OptionsPrint.PrintDetails = true;
            this.grvAplicacoes.OptionsPrint.PrintFilterInfo = true;
            this.grvAplicacoes.OptionsPrint.UsePrintStyles = true;
            this.grvAplicacoes.OptionsView.ShowGroupPanel = false;
            // 
            // colAPLNome
            // 
            this.colAPLNome.Caption = "Aplica��o";
            this.colAPLNome.FieldName = "APLNome";
            this.colAPLNome.Name = "colAPLNome";
            this.colAPLNome.Visible = true;
            this.colAPLNome.VisibleIndex = 1;
            // 
            // colAPLConta
            // 
            this.colAPLConta.Caption = "Conta";
            this.colAPLConta.FieldName = "APLConta";
            this.colAPLConta.Name = "colAPLConta";
            this.colAPLConta.Visible = true;
            this.colAPLConta.VisibleIndex = 0;
            // 
            // colAPLDataSaldoAnterior
            // 
            this.colAPLDataSaldoAnterior.Caption = "Dt Saldo Anterior";
            this.colAPLDataSaldoAnterior.FieldName = "APLDataSaldoAnterior";
            this.colAPLDataSaldoAnterior.Name = "colAPLDataSaldoAnterior";
            this.colAPLDataSaldoAnterior.Visible = true;
            this.colAPLDataSaldoAnterior.VisibleIndex = 2;
            // 
            // colAPLValorSaldoAnterior
            // 
            this.colAPLValorSaldoAnterior.Caption = "Vr Saldo Anterior";
            this.colAPLValorSaldoAnterior.DisplayFormat.FormatString = "n2";
            this.colAPLValorSaldoAnterior.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.colAPLValorSaldoAnterior.FieldName = "APLValorSaldoAnterior";
            this.colAPLValorSaldoAnterior.Name = "colAPLValorSaldoAnterior";
            this.colAPLValorSaldoAnterior.OptionsColumn.ReadOnly = true;
            this.colAPLValorSaldoAnterior.Visible = true;
            this.colAPLValorSaldoAnterior.VisibleIndex = 3;
            // 
            // colAPLDataSaldoAtual
            // 
            this.colAPLDataSaldoAtual.Caption = "Dt Saldo Atual";
            this.colAPLDataSaldoAtual.FieldName = "APLDataSaldoAtual";
            this.colAPLDataSaldoAtual.Name = "colAPLDataSaldoAtual";
            this.colAPLDataSaldoAtual.Visible = true;
            this.colAPLDataSaldoAtual.VisibleIndex = 4;
            // 
            // colAPLValorSaldoAtual
            // 
            this.colAPLValorSaldoAtual.Caption = "Vr Saldo Atual";
            this.colAPLValorSaldoAtual.DisplayFormat.FormatString = "n2";
            this.colAPLValorSaldoAtual.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.colAPLValorSaldoAtual.FieldName = "APLValorSaldoAtual";
            this.colAPLValorSaldoAtual.Name = "colAPLValorSaldoAtual";
            this.colAPLValorSaldoAtual.OptionsColumn.ReadOnly = true;
            this.colAPLValorSaldoAtual.Visible = true;
            this.colAPLValorSaldoAtual.VisibleIndex = 5;
            // 
            // colAPL
            // 
            this.colAPL.Caption = "colAPL";
            this.colAPL.FieldName = "APL";
            this.colAPL.Name = "colAPL";
            this.colAPL.OptionsColumn.ShowInCustomizationForm = false;
            // 
            // dCondominiosCampos
            // 
            this.dCondominiosCampos.DataSetName = "dCondominiosCampos";
            this.dCondominiosCampos.EnforceConstraints = false;
            this.dCondominiosCampos.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // LayoutControl
            // 
            this.LayoutControl.AllowCustomizationMenu = false;
            this.LayoutControl.Controls.Add(this.labelControl1);
            this.LayoutControl.Controls.Add(this.spinEdit2);
            this.LayoutControl.Controls.Add(this.lookUpEdit4);
            this.LayoutControl.Controls.Add(this.WebBrowser);
            this.LayoutControl.Controls.Add(this.txtCodigoFolha2);
            this.LayoutControl.Controls.Add(this.textEdit12);
            this.LayoutControl.Controls.Add(this.textEdit7);
            this.LayoutControl.Controls.Add(this.txtCodigoFolha1);
            this.LayoutControl.Controls.Add(this.checkEdit7);
            this.LayoutControl.Controls.Add(this.textEdit14);
            this.LayoutControl.Controls.Add(this.textEdit13);
            this.LayoutControl.Controls.Add(this.lookUpEdit1);
            this.LayoutControl.Controls.Add(this.txtCNPJ_C);
            this.LayoutControl.Controls.Add(this.DataAdesao_C);
            this.LayoutControl.Controls.Add(this.chkGas_C);
            this.LayoutControl.Controls.Add(this.txtTotalUnidades_C);
            this.LayoutControl.Controls.Add(this.txtConta_C);
            this.LayoutControl.Controls.Add(this.txtAgencia_C);
            this.LayoutControl.Controls.Add(this.txtTelefone_C);
            this.LayoutControl.Controls.Add(this.txtSindico_C);
            this.LayoutControl.Controls.Add(this.grdCorpoDiretivo);
            this.LayoutControl.Controls.Add(this.grdHistoricos);
            this.LayoutControl.Controls.Add(this.rgpBalanceteInternet);
            this.LayoutControl.Controls.Add(this.chkLiberarBalanceteInternet);
            this.LayoutControl.Controls.Add(this.chkAcordosOnLine);
            this.LayoutControl.Controls.Add(this.memEditorInternet);
            this.LayoutControl.Controls.Add(this.pictureEdit1);
            this.LayoutControl.Controls.Add(this.chkExportarInternet);
            this.LayoutControl.Controls.Add(this.lookUpEdit6);
            this.LayoutControl.Controls.Add(this.lookUpEdit5);
            this.LayoutControl.Controls.Add(this.spinEdit1);
            this.LayoutControl.Controls.Add(this.dateEdit4);
            this.LayoutControl.Controls.Add(this.checkEdit10);
            this.LayoutControl.Controls.Add(this.checkEdit9);
            this.LayoutControl.Controls.Add(this.checkEdit6);
            this.LayoutControl.Controls.Add(this.dateEdit3);
            this.LayoutControl.Controls.Add(this.dateEdit2);
            this.LayoutControl.Controls.Add(this.dateEdit1);
            this.LayoutControl.Controls.Add(this.checkEdit8);
            this.LayoutControl.Controls.Add(this.lkpSeguradoras);
            this.LayoutControl.Controls.Add(this.lookUpEdit3);
            this.LayoutControl.Controls.Add(this.lookUpEdit2);
            this.LayoutControl.Controls.Add(this.lkpCorretora);
            this.LayoutControl.Controls.Add(this.grdAplicacoes);
            this.LayoutControl.Controls.Add(this.rdgBoletoCorreio);
            this.LayoutControl.Controls.Add(this.rdgCobrancaCorreio);
            this.LayoutControl.Controls.Add(this.rdgAssembleiaCorreio);
            this.LayoutControl.Controls.Add(this.clcCS);
            this.LayoutControl.Controls.Add(this.clcAR);
            this.LayoutControl.Controls.Add(this.txtRegra);
            this.LayoutControl.Controls.Add(this.chkRepassaCorreio);
            this.LayoutControl.Controls.Add(this.spinEdit8);
            this.LayoutControl.Controls.Add(this.spinEdit7);
            this.LayoutControl.Controls.Add(this.spinEdit6);
            this.LayoutControl.Controls.Add(this.spinEdit5);
            this.LayoutControl.Controls.Add(this.spinEdit4);
            this.LayoutControl.Controls.Add(this.spinEdit3);
            this.LayoutControl.Controls.Add(this.txtPerContabilFim);
            this.LayoutControl.Controls.Add(this.spePerContabilInicio);
            this.LayoutControl.Controls.Add(this.speVencimento);
            this.LayoutControl.Controls.Add(this.calcEdit9);
            this.LayoutControl.Controls.Add(this.calcEdit8);
            this.LayoutControl.Controls.Add(this.calcEdit7);
            this.LayoutControl.Controls.Add(this.calcEdit6);
            this.LayoutControl.Controls.Add(this.calcEdit5);
            this.LayoutControl.Controls.Add(this.radioGroup4);
            this.LayoutControl.Controls.Add(this.checkEdit5);
            this.LayoutControl.Controls.Add(this.textEdit11);
            this.LayoutControl.Controls.Add(this.label2);
            this.LayoutControl.Controls.Add(this.textEdit10);
            this.LayoutControl.Controls.Add(this.rdgFR);
            this.LayoutControl.Controls.Add(this.textEdit9);
            this.LayoutControl.Controls.Add(this.textEdit8);
            this.LayoutControl.Controls.Add(this.label1);
            this.LayoutControl.Controls.Add(this.lkpBancos);
            this.LayoutControl.Controls.Add(this.speApartamentos);
            this.LayoutControl.Controls.Add(this.memoEdit1);
            this.LayoutControl.Controls.Add(this.speBlocos);
            this.LayoutControl.Controls.Add(this.grdApartamentos);
            this.LayoutControl.Controls.Add(this.grdBlocos);
            this.LayoutControl.Controls.Add(this.radioGroup2);
            this.LayoutControl.Controls.Add(this.checkEdit4);
            this.LayoutControl.Controls.Add(this.checkEdit3);
            this.LayoutControl.Controls.Add(this.calcEdit4);
            this.LayoutControl.Controls.Add(this.calcEdit3);
            this.LayoutControl.Controls.Add(this.calcEdit2);
            this.LayoutControl.Controls.Add(this.calcEdit1);
            this.LayoutControl.Controls.Add(this.checkEdit1);
            this.LayoutControl.Controls.Add(this.radioGroup1);
            this.LayoutControl.Controls.Add(this.textEdit1);
            this.LayoutControl.Controls.Add(this.checkEdit2);
            this.LayoutControl.Controls.Add(this.lkpCidades);
            this.LayoutControl.Controls.Add(this.lkpUF);
            this.LayoutControl.Controls.Add(this.textEdit5);
            this.LayoutControl.Controls.Add(this.textEdit4);
            this.LayoutControl.Controls.Add(this.textEdit3);
            this.LayoutControl.Controls.Add(this.bnvHistoricos);
            this.LayoutControl.Controls.Add(this.bnvCorpoDiretivo);
            this.LayoutControl.Controls.Add(this.bnvBlocos);
            this.LayoutControl.Controls.Add(this.bnvApartamentos);
            this.LayoutControl.Controls.Add(this.textEdit2);
            this.LayoutControl.Controls.Add(this.txtCodcon);
            this.LayoutControl.Controls.Add(this.bnvAplicacoes);
            this.LayoutControl.Dock = System.Windows.Forms.DockStyle.Top;
            this.LayoutControl.Location = new System.Drawing.Point(0, 30);
            this.LayoutControl.Name = "LayoutControl";
            this.LayoutControl.Root = this.LayoutControlGroup;
            this.LayoutControl.Size = new System.Drawing.Size(711, 427);
            this.ToolTipController_F.SetSuperTip(this.LayoutControl, null);
            this.LayoutControl.TabIndex = 4;
            // 
            // labelControl1
            // 
            this.labelControl1.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.BindingSource_F, "CON_EMP", true));
            this.labelControl1.Location = new System.Drawing.Point(352, 75);
            this.labelControl1.Name = "labelControl1";
            this.labelControl1.Size = new System.Drawing.Size(63, 13);
            this.labelControl1.StyleController = this.LayoutControl;
            this.labelControl1.TabIndex = 133;
            this.labelControl1.Text = "labelControl1";
            // 
            // spinEdit2
            // 
            this.spinEdit2.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.BindingSource_F, "CONStatus", true));
            this.spinEdit2.EditValue = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.spinEdit2.Location = new System.Drawing.Point(120, 75);
            this.spinEdit2.Name = "spinEdit2";
            this.spinEdit2.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.spinEdit2.Size = new System.Drawing.Size(0, 20);
            this.spinEdit2.StyleController = this.LayoutControl;
            this.spinEdit2.TabIndex = 132;
            // 
            // lookUpEdit4
            // 
            this.lookUpEdit4.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.BindingSource_F, "CON_EMP", true));
            this.lookUpEdit4.Location = new System.Drawing.Point(545, 10);
            this.lookUpEdit4.Name = "lookUpEdit4";
            this.lookUpEdit4.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.lookUpEdit4.Properties.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("EMPFantasia", "Empresa", 67, DevExpress.Utils.FormatType.None, "", true, DevExpress.Utils.HorzAlignment.Near)});
            this.lookUpEdit4.Properties.DataSource = this.eMPRESASBindingSource;
            this.lookUpEdit4.Properties.DisplayMember = "EMPFantasia";
            this.lookUpEdit4.Properties.ValueMember = "EMP";
            this.lookUpEdit4.Size = new System.Drawing.Size(144, 20);
            this.lookUpEdit4.StyleController = this.LayoutControl;
            this.lookUpEdit4.TabIndex = 131;
            // 
            // eMPRESASBindingSource
            // 
            this.eMPRESASBindingSource.DataMember = "EMPRESAS";
            this.eMPRESASBindingSource.DataSource = this.dCondominiosCampos;
            // 
            // WebBrowser
            // 
            this.WebBrowser.Location = new System.Drawing.Point(28, 280);
            this.WebBrowser.MinimumSize = new System.Drawing.Size(20, 20);
            this.WebBrowser.Name = "WebBrowser";
            this.WebBrowser.Size = new System.Drawing.Size(20, 20);
            this.ToolTipController_F.SetSuperTip(this.WebBrowser, null);
            this.WebBrowser.TabIndex = 106;
            // 
            // txtCodigoFolha2
            // 
            this.txtCodigoFolha2.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.BindingSource_F, "CONCodigoFolha2", true));
            this.txtCodigoFolha2.Location = new System.Drawing.Point(160, 142);
            this.txtCodigoFolha2.Name = "txtCodigoFolha2";
            this.txtCodigoFolha2.Size = new System.Drawing.Size(128, 20);
            this.txtCodigoFolha2.StyleController = this.LayoutControl;
            this.txtCodigoFolha2.TabIndex = 130;
            this.txtCodigoFolha2.Validated += new System.EventHandler(this.txtCodigoFolha_Validated);
            // 
            // textEdit12
            // 
            this.textEdit12.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.BindingSource_F, "CONCodigoBancoCS", true));
            this.textEdit12.Location = new System.Drawing.Point(444, 142);
            this.textEdit12.Name = "textEdit12";
            this.textEdit12.Size = new System.Drawing.Size(127, 20);
            this.textEdit12.StyleController = this.LayoutControl;
            this.textEdit12.TabIndex = 129;
            // 
            // textEdit7
            // 
            this.textEdit7.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.BindingSource_F, "CONCodigoBancoCC", true));
            this.textEdit7.Location = new System.Drawing.Point(305, 142);
            this.textEdit7.Name = "textEdit7";
            this.textEdit7.Size = new System.Drawing.Size(128, 20);
            this.textEdit7.StyleController = this.LayoutControl;
            this.textEdit7.TabIndex = 128;
            // 
            // txtCodigoFolha1
            // 
            this.txtCodigoFolha1.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.BindingSource_F, "CONCodigoFolha1", true));
            this.txtCodigoFolha1.Location = new System.Drawing.Point(22, 142);
            this.txtCodigoFolha1.Name = "txtCodigoFolha1";
            this.txtCodigoFolha1.Size = new System.Drawing.Size(127, 20);
            this.txtCodigoFolha1.StyleController = this.LayoutControl;
            this.txtCodigoFolha1.TabIndex = 127;
            this.txtCodigoFolha1.Validated += new System.EventHandler(this.txtCodigoFolha_Validated);
            // 
            // checkEdit7
            // 
            this.checkEdit7.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.BindingSource_F, "CONLeituraGas", true));
            this.checkEdit7.Location = new System.Drawing.Point(567, 392);
            this.checkEdit7.Name = "checkEdit7";
            this.checkEdit7.Properties.Caption = "Leitura de G�s";
            this.checkEdit7.Size = new System.Drawing.Size(0, 19);
            this.checkEdit7.StyleController = this.LayoutControl;
            this.checkEdit7.TabIndex = 126;
            // 
            // textEdit14
            // 
            this.textEdit14.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.BindingSource_F, "CONDigitoConta", true));
            this.textEdit14.Enabled = false;
            this.textEdit14.Location = new System.Drawing.Point(621, 354);
            this.textEdit14.Name = "textEdit14";
            this.textEdit14.Size = new System.Drawing.Size(0, 20);
            this.textEdit14.StyleController = this.LayoutControl;
            this.textEdit14.TabIndex = 125;
            // 
            // textEdit13
            // 
            this.textEdit13.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.BindingSource_F, "CONDigitoAgencia", true));
            this.textEdit13.Enabled = false;
            this.textEdit13.Location = new System.Drawing.Point(464, 354);
            this.textEdit13.Name = "textEdit13";
            this.textEdit13.Size = new System.Drawing.Size(0, 20);
            this.textEdit13.StyleController = this.LayoutControl;
            this.textEdit13.TabIndex = 124;
            // 
            // lookUpEdit1
            // 
            this.lookUpEdit1.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.BindingSource_F, "CON_BCO", true));
            this.lookUpEdit1.Enabled = false;
            this.lookUpEdit1.Location = new System.Drawing.Point(126, 354);
            this.lookUpEdit1.Name = "lookUpEdit1";
            this.lookUpEdit1.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.lookUpEdit1.Properties.DataSource = this.bANCOSBindingSource;
            this.lookUpEdit1.Properties.DisplayMember = "BCONome";
            this.lookUpEdit1.Properties.NullText = "";
            this.lookUpEdit1.Properties.ValueMember = "BCO";
            this.lookUpEdit1.Size = new System.Drawing.Size(0, 20);
            this.lookUpEdit1.StyleController = this.LayoutControl;
            this.lookUpEdit1.TabIndex = 123;
            // 
            // bANCOSBindingSource
            // 
            this.bANCOSBindingSource.DataMember = "BANCOS";
            this.bANCOSBindingSource.DataSource = this.dCondominiosCampos;
            // 
            // txtCNPJ_C
            // 
            this.txtCNPJ_C.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.BindingSource_F, "CONCnpj", true));
            this.txtCNPJ_C.EditValue = "00.000.000/0000-00";
            this.txtCNPJ_C.Enabled = false;
            this.txtCNPJ_C.Location = new System.Drawing.Point(79, 388);
            this.txtCNPJ_C.Name = "txtCNPJ_C";
            this.txtCNPJ_C.Size = new System.Drawing.Size(0, 20);
            this.txtCNPJ_C.StyleController = this.LayoutControl;
            this.txtCNPJ_C.TabIndex = 121;
            // 
            // DataAdesao_C
            // 
            this.DataAdesao_C.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.BindingSource_F, "CONDataInclusao", true));
            this.DataAdesao_C.EditValue = null;
            this.DataAdesao_C.Enabled = false;
            this.DataAdesao_C.Location = new System.Drawing.Point(290, 388);
            this.DataAdesao_C.Name = "DataAdesao_C";
            this.DataAdesao_C.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.DataAdesao_C.Size = new System.Drawing.Size(0, 20);
            this.DataAdesao_C.StyleController = this.LayoutControl;
            this.DataAdesao_C.TabIndex = 120;
            // 
            // chkGas_C
            // 
            this.chkGas_C.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.BindingSource_F, "CONLeituraGas", true));
            this.chkGas_C.Enabled = false;
            this.chkGas_C.Location = new System.Drawing.Point(565, 388);
            this.chkGas_C.Name = "chkGas_C";
            this.chkGas_C.Properties.Caption = "Leitura de G�s";
            this.chkGas_C.Size = new System.Drawing.Size(0, 19);
            this.chkGas_C.StyleController = this.LayoutControl;
            this.chkGas_C.TabIndex = 122;
            // 
            // txtTotalUnidades_C
            // 
            this.txtTotalUnidades_C.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.BindingSource_F, "CONTotalApartamento", true));
            this.txtTotalUnidades_C.Enabled = false;
            this.txtTotalUnidades_C.Location = new System.Drawing.Point(489, 388);
            this.txtTotalUnidades_C.Name = "txtTotalUnidades_C";
            this.txtTotalUnidades_C.Size = new System.Drawing.Size(0, 20);
            this.txtTotalUnidades_C.StyleController = this.LayoutControl;
            this.txtTotalUnidades_C.TabIndex = 119;
            // 
            // txtConta_C
            // 
            this.txtConta_C.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.BindingSource_F, "CONConta", true));
            this.txtConta_C.Enabled = false;
            this.txtConta_C.Location = new System.Drawing.Point(560, 354);
            this.txtConta_C.Name = "txtConta_C";
            this.txtConta_C.Size = new System.Drawing.Size(0, 20);
            this.txtConta_C.StyleController = this.LayoutControl;
            this.txtConta_C.TabIndex = 118;
            // 
            // txtAgencia_C
            // 
            this.txtAgencia_C.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.BindingSource_F, "CONAgencia", true));
            this.txtAgencia_C.Enabled = false;
            this.txtAgencia_C.Location = new System.Drawing.Point(403, 354);
            this.txtAgencia_C.Name = "txtAgencia_C";
            this.txtAgencia_C.Size = new System.Drawing.Size(0, 20);
            this.txtAgencia_C.StyleController = this.LayoutControl;
            this.txtAgencia_C.TabIndex = 117;
            // 
            // txtTelefone_C
            // 
            this.txtTelefone_C.Enabled = false;
            this.txtTelefone_C.Location = new System.Drawing.Point(585, 299);
            this.txtTelefone_C.Name = "txtTelefone_C";
            this.txtTelefone_C.Size = new System.Drawing.Size(0, 20);
            this.txtTelefone_C.StyleController = this.LayoutControl;
            this.txtTelefone_C.TabIndex = 115;
            // 
            // txtSindico_C
            // 
            this.txtSindico_C.Enabled = false;
            this.txtSindico_C.Location = new System.Drawing.Point(126, 299);
            this.txtSindico_C.Name = "txtSindico_C";
            this.txtSindico_C.Size = new System.Drawing.Size(0, 20);
            this.txtSindico_C.StyleController = this.LayoutControl;
            this.txtSindico_C.TabIndex = 114;
            // 
            // grdCorpoDiretivo
            // 
            this.grdCorpoDiretivo.DataSource = this.cORPODIRETIVOBindingSource;
            this.grdCorpoDiretivo.EmbeddedNavigator.Name = "";
            this.grdCorpoDiretivo.Location = new System.Drawing.Point(16, 75);
            this.grdCorpoDiretivo.MainView = this.grvCorpoDiretivo;
            this.grdCorpoDiretivo.Name = "grdCorpoDiretivo";
            this.grdCorpoDiretivo.Size = new System.Drawing.Size(0, 0);
            this.grdCorpoDiretivo.TabIndex = 111;
            this.grdCorpoDiretivo.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.grvCorpoDiretivo});
            // 
            // cORPODIRETIVOBindingSource
            // 
            this.cORPODIRETIVOBindingSource.DataMember = "CORPODIRETIVO";
            this.cORPODIRETIVOBindingSource.DataSource = this.dCondominiosCampos;
            // 
            // grvCorpoDiretivo
            // 
            this.grvCorpoDiretivo.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colPESNome,
            this.colAPTNumero1,
            this.colBLONome1,
            this.colCGONome});
            this.grvCorpoDiretivo.GridControl = this.grdCorpoDiretivo;
            this.grvCorpoDiretivo.Name = "grvCorpoDiretivo";
            // 
            // colPESNome
            // 
            this.colPESNome.Caption = "Nome";
            this.colPESNome.FieldName = "PESNome";
            this.colPESNome.Name = "colPESNome";
            this.colPESNome.Visible = true;
            this.colPESNome.VisibleIndex = 1;
            this.colPESNome.Width = 373;
            // 
            // colAPTNumero1
            // 
            this.colAPTNumero1.Caption = "Apto";
            this.colAPTNumero1.FieldName = "APTNumero";
            this.colAPTNumero1.Name = "colAPTNumero1";
            this.colAPTNumero1.Visible = true;
            this.colAPTNumero1.VisibleIndex = 3;
            this.colAPTNumero1.Width = 45;
            // 
            // colBLONome1
            // 
            this.colBLONome1.Caption = "Bloco";
            this.colBLONome1.FieldName = "BLODescricao";
            this.colBLONome1.Name = "colBLONome1";
            this.colBLONome1.Visible = true;
            this.colBLONome1.VisibleIndex = 2;
            this.colBLONome1.Width = 193;
            // 
            // colCGONome
            // 
            this.colCGONome.Caption = "Cargo";
            this.colCGONome.FieldName = "CGONome";
            this.colCGONome.Name = "colCGONome";
            this.colCGONome.Visible = true;
            this.colCGONome.VisibleIndex = 0;
            this.colCGONome.Width = 181;
            // 
            // grdHistoricos
            // 
            this.grdHistoricos.DataSource = this.fKHISTORICOSCONDOMINIOSBindingSource;
            this.grdHistoricos.EmbeddedNavigator.Name = "";
            this.grdHistoricos.Location = new System.Drawing.Point(16, 75);
            this.grdHistoricos.MainView = this.grvHistoricos;
            this.grdHistoricos.Name = "grdHistoricos";
            this.grdHistoricos.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemCheckEdit1,
            this.repositoryItemMemoExEdit1});
            this.grdHistoricos.Size = new System.Drawing.Size(0, 0);
            this.grdHistoricos.TabIndex = 107;
            this.grdHistoricos.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.grvHistoricos});
            // 
            // fKHISTORICOSCONDOMINIOSBindingSource
            // 
            this.fKHISTORICOSCONDOMINIOSBindingSource.DataMember = "HISTORICOS";
            this.fKHISTORICOSCONDOMINIOSBindingSource.DataSource = this.dCondominiosCampos;
            // 
            // grvHistoricos
            // 
            this.grvHistoricos.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colHSTAssunto,
            this.colHSTData,
            this.colHSTCritico,
            this.colHSTDescricao});
            this.grvHistoricos.GridControl = this.grdHistoricos;
            this.grvHistoricos.GroupCount = 1;
            this.grvHistoricos.Name = "grvHistoricos";
            this.grvHistoricos.SortInfo.AddRange(new DevExpress.XtraGrid.Columns.GridColumnSortInfo[] {
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colHSTData, DevExpress.Data.ColumnSortOrder.Ascending),
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colHSTCritico, DevExpress.Data.ColumnSortOrder.Descending)});
            // 
            // colHSTAssunto
            // 
            this.colHSTAssunto.Caption = "Assunto";
            this.colHSTAssunto.FieldName = "HSTAssunto";
            this.colHSTAssunto.Name = "colHSTAssunto";
            this.colHSTAssunto.OptionsColumn.AllowEdit = false;
            this.colHSTAssunto.Visible = true;
            this.colHSTAssunto.VisibleIndex = 0;
            this.colHSTAssunto.Width = 89;
            // 
            // colHSTData
            // 
            this.colHSTData.Caption = "Data";
            this.colHSTData.FieldName = "HSTData";
            this.colHSTData.Name = "colHSTData";
            this.colHSTData.OptionsColumn.AllowEdit = false;
            this.colHSTData.Visible = true;
            this.colHSTData.VisibleIndex = 0;
            this.colHSTData.Width = 89;
            // 
            // colHSTCritico
            // 
            this.colHSTCritico.Caption = "Cr�tico";
            this.colHSTCritico.ColumnEdit = this.repositoryItemCheckEdit1;
            this.colHSTCritico.FieldName = "HSTCritico";
            this.colHSTCritico.Name = "colHSTCritico";
            this.colHSTCritico.OptionsColumn.AllowEdit = false;
            this.colHSTCritico.Visible = true;
            this.colHSTCritico.VisibleIndex = 2;
            this.colHSTCritico.Width = 68;
            // 
            // repositoryItemCheckEdit1
            // 
            this.repositoryItemCheckEdit1.AutoHeight = false;
            this.repositoryItemCheckEdit1.Name = "repositoryItemCheckEdit1";
            // 
            // colHSTDescricao
            // 
            this.colHSTDescricao.Caption = "Descri��o";
            this.colHSTDescricao.ColumnEdit = this.repositoryItemMemoExEdit1;
            this.colHSTDescricao.FieldName = "HSTDescricao";
            this.colHSTDescricao.Name = "colHSTDescricao";
            this.colHSTDescricao.Visible = true;
            this.colHSTDescricao.VisibleIndex = 1;
            this.colHSTDescricao.Width = 297;
            // 
            // repositoryItemMemoExEdit1
            // 
            this.repositoryItemMemoExEdit1.AutoHeight = false;
            this.repositoryItemMemoExEdit1.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemMemoExEdit1.Name = "repositoryItemMemoExEdit1";
            this.repositoryItemMemoExEdit1.ReadOnly = true;
            this.repositoryItemMemoExEdit1.ShowIcon = false;
            // 
            // rgpBalanceteInternet
            // 
            this.rgpBalanceteInternet.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.BindingSource_F, "CONModeloBalanceteInternet", true));
            this.rgpBalanceteInternet.Location = new System.Drawing.Point(370, 183);
            this.rgpBalanceteInternet.Name = "rgpBalanceteInternet";
            this.rgpBalanceteInternet.Properties.Columns = 4;
            this.rgpBalanceteInternet.Properties.Items.AddRange(new DevExpress.XtraEditors.Controls.RadioGroupItem[] {
            new DevExpress.XtraEditors.Controls.RadioGroupItem(1, "M1"),
            new DevExpress.XtraEditors.Controls.RadioGroupItem(2, "M2"),
            new DevExpress.XtraEditors.Controls.RadioGroupItem(3, "M3"),
            new DevExpress.XtraEditors.Controls.RadioGroupItem(4, "M4")});
            this.rgpBalanceteInternet.Size = new System.Drawing.Size(0, 0);
            this.rgpBalanceteInternet.StyleController = this.LayoutControl;
            this.rgpBalanceteInternet.TabIndex = 105;
            // 
            // chkLiberarBalanceteInternet
            // 
            this.chkLiberarBalanceteInternet.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.BindingSource_F, "CONPublicaBalanceteInternet", true));
            this.chkLiberarBalanceteInternet.Location = new System.Drawing.Point(370, 129);
            this.chkLiberarBalanceteInternet.Name = "chkLiberarBalanceteInternet";
            this.chkLiberarBalanceteInternet.Properties.Caption = "Liberar balancete mediante aprova��o do s�ndico.";
            this.chkLiberarBalanceteInternet.Size = new System.Drawing.Size(0, 19);
            this.chkLiberarBalanceteInternet.StyleController = this.LayoutControl;
            this.chkLiberarBalanceteInternet.TabIndex = 104;
            // 
            // chkAcordosOnLine
            // 
            this.chkAcordosOnLine.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.BindingSource_F, "CONPermiteAcordoInternet", true));
            this.chkAcordosOnLine.Location = new System.Drawing.Point(370, 99);
            this.chkAcordosOnLine.Name = "chkAcordosOnLine";
            this.chkAcordosOnLine.Properties.Caption = "Permitir realiza��o de acordos on-line.";
            this.chkAcordosOnLine.Size = new System.Drawing.Size(0, 19);
            this.chkAcordosOnLine.StyleController = this.LayoutControl;
            this.chkAcordosOnLine.TabIndex = 103;
            // 
            // memEditorInternet
            // 
            this.memEditorInternet.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.BindingSource_F, "CONObservacaoInternet", true));
            this.memEditorInternet.Location = new System.Drawing.Point(28, 280);
            this.memEditorInternet.Name = "memEditorInternet";
            this.memEditorInternet.Size = new System.Drawing.Size(0, 0);
            this.memEditorInternet.StyleController = this.LayoutControl;
            this.memEditorInternet.TabIndex = 102;
            // 
            // pictureEdit1
            // 
            this.pictureEdit1.EditValue = ((object)(resources.GetObject("pictureEdit1.EditValue")));
            this.pictureEdit1.Location = new System.Drawing.Point(19, 78);
            this.pictureEdit1.Name = "pictureEdit1";
            this.pictureEdit1.Properties.SizeMode = DevExpress.XtraEditors.Controls.PictureSizeMode.Squeeze;
            this.pictureEdit1.Size = new System.Drawing.Size(0, 0);
            this.pictureEdit1.StyleController = this.LayoutControl;
            this.pictureEdit1.TabIndex = 100;
            // 
            // chkExportarInternet
            // 
            this.chkExportarInternet.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.BindingSource_F, "CONExportaInternet", true));
            this.chkExportarInternet.Location = new System.Drawing.Point(111, 133);
            this.chkExportarInternet.Name = "chkExportarInternet";
            this.chkExportarInternet.Properties.Caption = "Exportar para Internet";
            this.chkExportarInternet.Size = new System.Drawing.Size(0, 19);
            this.chkExportarInternet.StyleController = this.LayoutControl;
            this.chkExportarInternet.TabIndex = 101;
            this.chkExportarInternet.EditValueChanged += new System.EventHandler(this.chkExportarInternet_EditValueChanged);
            
            // 
            // lookUpEdit6
            // 
            this.lookUpEdit6.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.BindingSource_F, "CONAdvogado2_USU", true));
            this.lookUpEdit6.Location = new System.Drawing.Point(459, 96);
            this.lookUpEdit6.Name = "lookUpEdit6";
            this.lookUpEdit6.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.lookUpEdit6.Properties.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("USUNome", "Advogado 2", 53, DevExpress.Utils.FormatType.None, "", true, DevExpress.Utils.HorzAlignment.Near)});
            this.lookUpEdit6.Properties.DataSource = this.aDVOGADOSBindingSource;
            this.lookUpEdit6.Properties.DisplayMember = "USUNome";
            this.lookUpEdit6.Properties.ValueMember = "USU";
            this.lookUpEdit6.Size = new System.Drawing.Size(0, 20);
            this.lookUpEdit6.StyleController = this.LayoutControl;
            this.lookUpEdit6.TabIndex = 99;
            // 
            // aDVOGADOSBindingSource
            // 
            this.aDVOGADOSBindingSource.DataMember = "ADVOGADOS";
            this.aDVOGADOSBindingSource.DataSource = this.dCondominiosCampos;
            // 
            // lookUpEdit5
            // 
            this.lookUpEdit5.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.BindingSource_F, "CONAdvogado1_USU", true));
            this.lookUpEdit5.Location = new System.Drawing.Point(84, 96);
            this.lookUpEdit5.Name = "lookUpEdit5";
            this.lookUpEdit5.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.lookUpEdit5.Properties.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("USUNome", "Advogado 1", 53, DevExpress.Utils.FormatType.None, "", true, DevExpress.Utils.HorzAlignment.Near)});
            this.lookUpEdit5.Properties.DataSource = this.aDVOGADOSBindingSource;
            this.lookUpEdit5.Properties.DisplayMember = "USUNome";
            this.lookUpEdit5.Properties.ValueMember = "USU";
            this.lookUpEdit5.Size = new System.Drawing.Size(0, 20);
            this.lookUpEdit5.StyleController = this.LayoutControl;
            this.lookUpEdit5.TabIndex = 98;
            // 
            // spinEdit1
            // 
            this.spinEdit1.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.BindingSource_F, "CONConvocacaoAssembleia", true));
            this.spinEdit1.EditValue = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.spinEdit1.Location = new System.Drawing.Point(549, 358);
            this.spinEdit1.Name = "spinEdit1";
            this.spinEdit1.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.spinEdit1.Properties.DisplayFormat.FormatString = "0 Dia(s)";
            this.spinEdit1.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.spinEdit1.Properties.IsFloatValue = false;
            this.spinEdit1.Properties.Mask.EditMask = "N00";
            this.spinEdit1.Size = new System.Drawing.Size(0, 20);
            this.spinEdit1.StyleController = this.LayoutControl;
            this.spinEdit1.TabIndex = 97;
            // 
            // dateEdit4
            // 
            this.dateEdit4.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.BindingSource_F, "CONDataUltimaAssembleia", true));
            this.dateEdit4.EditValue = null;
            this.dateEdit4.Location = new System.Drawing.Point(194, 358);
            this.dateEdit4.Name = "dateEdit4";
            this.dateEdit4.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.dateEdit4.Size = new System.Drawing.Size(0, 20);
            this.dateEdit4.StyleController = this.LayoutControl;
            this.dateEdit4.TabIndex = 96;
            // 
            // checkEdit10
            // 
            this.checkEdit10.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.BindingSource_F, "CONCompetenciaVencida", true));
            this.checkEdit10.Location = new System.Drawing.Point(268, 392);
            this.checkEdit10.Name = "checkEdit10";
            this.checkEdit10.Properties.Caption = "Compet�ncia a Vencer";
            this.checkEdit10.Size = new System.Drawing.Size(0, 19);
            this.checkEdit10.StyleController = this.LayoutControl;
            this.checkEdit10.TabIndex = 95;
            // 
            // checkEdit9
            // 
            this.checkEdit9.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.BindingSource_F, "CONImprimeBalanceteBoleto", true));
            this.checkEdit9.Location = new System.Drawing.Point(429, 392);
            this.checkEdit9.Name = "checkEdit9";
            this.checkEdit9.Properties.Caption = "Balancete Boleto";
            this.checkEdit9.Size = new System.Drawing.Size(0, 19);
            this.checkEdit9.StyleController = this.LayoutControl;
            this.checkEdit9.TabIndex = 94;
            // 
            // checkEdit6
            // 
            this.checkEdit6.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.BindingSource_F, "CONImprimePrevisao", true));
            this.checkEdit6.Location = new System.Drawing.Point(142, 392);
            this.checkEdit6.Name = "checkEdit6";
            this.checkEdit6.Properties.Caption = "Rodar Previs�o";
            this.checkEdit6.Size = new System.Drawing.Size(0, 19);
            this.checkEdit6.StyleController = this.LayoutControl;
            this.checkEdit6.TabIndex = 92;
            // 
            // dateEdit3
            // 
            this.dateEdit3.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.BindingSource_F, "CONValidadeExtintor", true));
            this.dateEdit3.EditValue = null;
            this.dateEdit3.Location = new System.Drawing.Point(337, 301);
            this.dateEdit3.Name = "dateEdit3";
            this.dateEdit3.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.dateEdit3.Properties.DisplayFormat.FormatString = "MM/yyyy";
            this.dateEdit3.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.dateEdit3.Size = new System.Drawing.Size(0, 20);
            this.dateEdit3.StyleController = this.LayoutControl;
            this.dateEdit3.TabIndex = 91;
            // 
            // dateEdit2
            // 
            this.dateEdit2.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.BindingSource_F, "CONValidadeElevador", true));
            this.dateEdit2.EditValue = null;
            this.dateEdit2.Location = new System.Drawing.Point(84, 301);
            this.dateEdit2.Name = "dateEdit2";
            this.dateEdit2.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.dateEdit2.Properties.DisplayFormat.FormatString = "MM/yyyy";
            this.dateEdit2.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.dateEdit2.Size = new System.Drawing.Size(0, 20);
            this.dateEdit2.StyleController = this.LayoutControl;
            this.dateEdit2.TabIndex = 90;
            // 
            // dateEdit1
            // 
            this.dateEdit1.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.BindingSource_F, "CONValidadeAVCB", true));
            this.dateEdit1.EditValue = null;
            this.dateEdit1.Location = new System.Drawing.Point(532, 301);
            this.dateEdit1.Name = "dateEdit1";
            this.dateEdit1.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.dateEdit1.Properties.DisplayFormat.FormatString = "MM/yyyy";
            this.dateEdit1.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.dateEdit1.Size = new System.Drawing.Size(0, 20);
            this.dateEdit1.StyleController = this.LayoutControl;
            this.dateEdit1.TabIndex = 89;
            // 
            // checkEdit8
            // 
            this.checkEdit8.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.BindingSource_F, "CONUtilizaPrevisao", true));
            this.checkEdit8.Location = new System.Drawing.Point(16, 392);
            this.checkEdit8.Name = "checkEdit8";
            this.checkEdit8.Properties.Caption = "Utilizar Previs�o";
            this.checkEdit8.Size = new System.Drawing.Size(0, 19);
            this.checkEdit8.StyleController = this.LayoutControl;
            this.checkEdit8.TabIndex = 93;
            // 
            // lkpSeguradoras
            // 
            this.lkpSeguradoras.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.BindingSource_F, "CONSeguradora_FRN", true));
            this.lkpSeguradoras.Location = new System.Drawing.Point(123, 212);
            this.lkpSeguradoras.Name = "lkpSeguradoras";
            this.lkpSeguradoras.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo),
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Plus),
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.lkpSeguradoras.Properties.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("FRNNome", "Seguradoras", 53, DevExpress.Utils.FormatType.None, "", true, DevExpress.Utils.HorzAlignment.Near)});
            this.lkpSeguradoras.Properties.DataSource = this.sEGURADORASBindingSource;
            this.lkpSeguradoras.Properties.DisplayMember = "FRNNome";
            this.lkpSeguradoras.Properties.ValueMember = "FRN";
            this.lkpSeguradoras.Size = new System.Drawing.Size(0, 20);
            this.lkpSeguradoras.StyleController = this.LayoutControl;
            this.lkpSeguradoras.TabIndex = 88;
            
            // 
            // sEGURADORASBindingSource
            // 
            this.sEGURADORASBindingSource.DataMember = "SEGURADORAS";
            this.sEGURADORASBindingSource.DataSource = this.dCondominiosCampos;
            // 
            // lookUpEdit3
            // 
            this.lookUpEdit3.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.BindingSource_F, "CONAuditor2_USU", true));
            this.lookUpEdit3.Location = new System.Drawing.Point(459, 154);
            this.lookUpEdit3.Name = "lookUpEdit3";
            this.lookUpEdit3.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.lookUpEdit3.Properties.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("USUNome", "Auditor 2", 53, DevExpress.Utils.FormatType.None, "", true, DevExpress.Utils.HorzAlignment.Near)});
            this.lookUpEdit3.Properties.DataSource = this.aUDITORESBindingSource;
            this.lookUpEdit3.Properties.DisplayMember = "USUNome";
            this.lookUpEdit3.Properties.ValueMember = "USU";
            this.lookUpEdit3.Size = new System.Drawing.Size(0, 20);
            this.lookUpEdit3.StyleController = this.LayoutControl;
            this.lookUpEdit3.TabIndex = 87;
            // 
            // aUDITORESBindingSource
            // 
            this.aUDITORESBindingSource.DataMember = "AUDITORES";
            this.aUDITORESBindingSource.DataSource = this.dCondominiosCampos;
            // 
            // lookUpEdit2
            // 
            this.lookUpEdit2.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.BindingSource_F, "CONAuditor1_USU", true));
            this.lookUpEdit2.Location = new System.Drawing.Point(84, 154);
            this.lookUpEdit2.Name = "lookUpEdit2";
            this.lookUpEdit2.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.lookUpEdit2.Properties.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("USUNome", "Auditor 1", 53, DevExpress.Utils.FormatType.None, "", true, DevExpress.Utils.HorzAlignment.Near)});
            this.lookUpEdit2.Properties.DataSource = this.aUDITORESBindingSource;
            this.lookUpEdit2.Properties.DisplayMember = "USUNome";
            this.lookUpEdit2.Properties.ValueMember = "USU";
            this.lookUpEdit2.Size = new System.Drawing.Size(0, 20);
            this.lookUpEdit2.StyleController = this.LayoutControl;
            this.lookUpEdit2.TabIndex = 86;
            // 
            // lkpCorretora
            // 
            this.lkpCorretora.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.BindingSource_F, "CONCorretoraSeguros_FRN", true));
            this.lkpCorretora.Location = new System.Drawing.Point(123, 243);
            this.lkpCorretora.Name = "lkpCorretora";
            this.lkpCorretora.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo),
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Plus),
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.lkpCorretora.Properties.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("FRNNome", "Corretora de Seguro", 53, DevExpress.Utils.FormatType.None, "", true, DevExpress.Utils.HorzAlignment.Near)});
            this.lkpCorretora.Properties.DataSource = this.cORRETORASEGUROSBindingSource;
            this.lkpCorretora.Properties.DisplayMember = "FRNNome";
            this.lkpCorretora.Properties.ValueMember = "FRN";
            this.lkpCorretora.Size = new System.Drawing.Size(0, 20);
            this.lkpCorretora.StyleController = this.LayoutControl;
            this.lkpCorretora.TabIndex = 85;
            
            // 
            // cORRETORASEGUROSBindingSource
            // 
            this.cORRETORASEGUROSBindingSource.DataMember = "CORRETORASEGUROS";
            this.cORRETORASEGUROSBindingSource.DataSource = this.dCondominiosCampos;
            // 
            // rdgBoletoCorreio
            // 
            this.rdgBoletoCorreio.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.BindingSource_F, "CONCorreioBoleto", true));
            this.rdgBoletoCorreio.Location = new System.Drawing.Point(559, 348);
            this.rdgBoletoCorreio.Name = "rdgBoletoCorreio";
            this.rdgBoletoCorreio.Properties.Items.AddRange(new DevExpress.XtraEditors.Controls.RadioGroupItem[] {
            new DevExpress.XtraEditors.Controls.RadioGroupItem("AR", "AR"),
            new DevExpress.XtraEditors.Controls.RadioGroupItem("CS", "Carta Simples")});
            this.rdgBoletoCorreio.Size = new System.Drawing.Size(0, 0);
            this.rdgBoletoCorreio.StyleController = this.LayoutControl;
            this.rdgBoletoCorreio.TabIndex = 80;
            // 
            // rdgCobrancaCorreio
            // 
            this.rdgCobrancaCorreio.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.BindingSource_F, "CONCorreioCobranca", true));
            this.rdgCobrancaCorreio.Location = new System.Drawing.Point(440, 348);
            this.rdgCobrancaCorreio.Name = "rdgCobrancaCorreio";
            this.rdgCobrancaCorreio.Properties.Items.AddRange(new DevExpress.XtraEditors.Controls.RadioGroupItem[] {
            new DevExpress.XtraEditors.Controls.RadioGroupItem("AR", "AR"),
            new DevExpress.XtraEditors.Controls.RadioGroupItem("CS", "Carta Simples")});
            this.rdgCobrancaCorreio.Size = new System.Drawing.Size(0, 0);
            this.rdgCobrancaCorreio.StyleController = this.LayoutControl;
            this.rdgCobrancaCorreio.TabIndex = 79;
            // 
            // rdgAssembleiaCorreio
            // 
            this.rdgAssembleiaCorreio.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.BindingSource_F, "CONCorreioAssembleia", true));
            this.rdgAssembleiaCorreio.Location = new System.Drawing.Point(322, 348);
            this.rdgAssembleiaCorreio.Name = "rdgAssembleiaCorreio";
            this.rdgAssembleiaCorreio.Properties.Items.AddRange(new DevExpress.XtraEditors.Controls.RadioGroupItem[] {
            new DevExpress.XtraEditors.Controls.RadioGroupItem("AR", "AR"),
            new DevExpress.XtraEditors.Controls.RadioGroupItem("CS", "Carta Simples")});
            this.rdgAssembleiaCorreio.Size = new System.Drawing.Size(0, 0);
            this.rdgAssembleiaCorreio.StyleController = this.LayoutControl;
            this.rdgAssembleiaCorreio.TabIndex = 78;
            // 
            // clcCS
            // 
            this.clcCS.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.BindingSource_F, "CONValorCartaSimples", true));
            this.clcCS.Location = new System.Drawing.Point(222, 381);
            this.clcCS.Name = "clcCS";
            this.clcCS.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.clcCS.Size = new System.Drawing.Size(0, 20);
            this.clcCS.StyleController = this.LayoutControl;
            this.clcCS.TabIndex = 77;
            // 
            // clcAR
            // 
            this.clcAR.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.BindingSource_F, "CONValorAR", true));
            this.clcAR.Location = new System.Drawing.Point(222, 352);
            this.clcAR.Name = "clcAR";
            this.clcAR.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.clcAR.Size = new System.Drawing.Size(0, 20);
            this.clcAR.StyleController = this.LayoutControl;
            this.clcAR.TabIndex = 76;
            // 
            // txtRegra
            // 
            this.txtRegra.Enabled = false;
            this.txtRegra.Location = new System.Drawing.Point(451, 202);
            this.txtRegra.Name = "txtRegra";
            this.txtRegra.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 6.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtRegra.Properties.Appearance.Options.UseFont = true;
            this.txtRegra.Size = new System.Drawing.Size(0, 17);
            this.txtRegra.StyleController = this.LayoutControl;
            this.txtRegra.TabIndex = 75;
            // 
            // chkRepassaCorreio
            // 
            this.chkRepassaCorreio.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.BindingSource_F, "CONRepassaCorreio", true));
            this.chkRepassaCorreio.Location = new System.Drawing.Point(145, 307);
            this.chkRepassaCorreio.Name = "chkRepassaCorreio";
            this.chkRepassaCorreio.Properties.Caption = "Repassar as despesas com o correio para o condom�nio.";
            this.chkRepassaCorreio.Size = new System.Drawing.Size(0, 19);
            this.chkRepassaCorreio.StyleController = this.LayoutControl;
            this.chkRepassaCorreio.TabIndex = 74;
            this.chkRepassaCorreio.EditValueChanged += new System.EventHandler(this.chkRepassaCorreio_EditValueChanged);
            
            // 
            // spinEdit8
            // 
            this.spinEdit8.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.BindingSource_F, "CONCartaCobranca", true));
            this.spinEdit8.EditValue = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.spinEdit8.Location = new System.Drawing.Point(19, 332);
            this.spinEdit8.Name = "spinEdit8";
            this.spinEdit8.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.spinEdit8.Properties.DisplayFormat.FormatString = "00 Dia(s)";
            this.spinEdit8.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.spinEdit8.Properties.Mask.EditMask = "n00";
            this.spinEdit8.Size = new System.Drawing.Size(0, 20);
            this.spinEdit8.StyleController = this.LayoutControl;
            this.spinEdit8.TabIndex = 73;
            // 
            // spinEdit7
            // 
            this.spinEdit7.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.BindingSource_F, "CONQtdeBalanceteM5", true));
            this.spinEdit7.EditValue = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.spinEdit7.Location = new System.Drawing.Point(619, 257);
            this.spinEdit7.Name = "spinEdit7";
            this.spinEdit7.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.spinEdit7.Properties.IsFloatValue = false;
            this.spinEdit7.Properties.Mask.EditMask = "N00";
            this.spinEdit7.Size = new System.Drawing.Size(0, 20);
            this.spinEdit7.StyleController = this.LayoutControl;
            this.spinEdit7.TabIndex = 72;
            // 
            // spinEdit6
            // 
            this.spinEdit6.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.BindingSource_F, "CONQtdeBalanceteM4", true));
            this.spinEdit6.EditValue = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.spinEdit6.Location = new System.Drawing.Point(532, 257);
            this.spinEdit6.Name = "spinEdit6";
            this.spinEdit6.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.spinEdit6.Properties.IsFloatValue = false;
            this.spinEdit6.Properties.Mask.EditMask = "N00";
            this.spinEdit6.Size = new System.Drawing.Size(0, 20);
            this.spinEdit6.StyleController = this.LayoutControl;
            this.spinEdit6.TabIndex = 71;
            // 
            // spinEdit5
            // 
            this.spinEdit5.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.BindingSource_F, "CONQtdeBalanceteM3", true));
            this.spinEdit5.EditValue = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.spinEdit5.Location = new System.Drawing.Point(445, 257);
            this.spinEdit5.Name = "spinEdit5";
            this.spinEdit5.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.spinEdit5.Properties.IsFloatValue = false;
            this.spinEdit5.Properties.Mask.EditMask = "N00";
            this.spinEdit5.Size = new System.Drawing.Size(0, 20);
            this.spinEdit5.StyleController = this.LayoutControl;
            this.spinEdit5.TabIndex = 70;
            // 
            // spinEdit4
            // 
            this.spinEdit4.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.BindingSource_F, "CONQtdeBalanceteM2", true));
            this.spinEdit4.EditValue = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.spinEdit4.Location = new System.Drawing.Point(357, 257);
            this.spinEdit4.Name = "spinEdit4";
            this.spinEdit4.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.spinEdit4.Properties.IsFloatValue = false;
            this.spinEdit4.Properties.Mask.EditMask = "N00";
            this.spinEdit4.Size = new System.Drawing.Size(0, 20);
            this.spinEdit4.StyleController = this.LayoutControl;
            this.spinEdit4.TabIndex = 69;
            // 
            // spinEdit3
            // 
            this.spinEdit3.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.BindingSource_F, "CONQtdeBalanceteM1", true));
            this.spinEdit3.EditValue = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.spinEdit3.Location = new System.Drawing.Point(269, 257);
            this.spinEdit3.Name = "spinEdit3";
            this.spinEdit3.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.spinEdit3.Properties.IsFloatValue = false;
            this.spinEdit3.Properties.Mask.EditMask = "N00";
            this.spinEdit3.Size = new System.Drawing.Size(0, 20);
            this.spinEdit3.StyleController = this.LayoutControl;
            this.spinEdit3.TabIndex = 68;
            // 
            // txtPerContabilFim
            // 
            this.txtPerContabilFim.Enabled = false;
            this.txtPerContabilFim.Location = new System.Drawing.Point(191, 257);
            this.txtPerContabilFim.Name = "txtPerContabilFim";
            this.txtPerContabilFim.Size = new System.Drawing.Size(0, 20);
            this.txtPerContabilFim.StyleController = this.LayoutControl;
            this.txtPerContabilFim.TabIndex = 67;
            // 
            // spePerContabilInicio
            // 
            this.spePerContabilInicio.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.BindingSource_F, "CONDiaContabilidade", true));
            this.spePerContabilInicio.EditValue = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.spePerContabilInicio.Location = new System.Drawing.Point(84, 257);
            this.spePerContabilInicio.Name = "spePerContabilInicio";
            this.spePerContabilInicio.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.spePerContabilInicio.Properties.IsFloatValue = false;
            this.spePerContabilInicio.Properties.Mask.EditMask = "N00";
            this.spePerContabilInicio.Properties.MaxValue = new decimal(new int[] {
            31,
            0,
            0,
            0});
            this.spePerContabilInicio.Properties.MinValue = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.spePerContabilInicio.Size = new System.Drawing.Size(0, 20);
            this.spePerContabilInicio.StyleController = this.LayoutControl;
            this.spePerContabilInicio.TabIndex = 65;
            this.spePerContabilInicio.TextChanged += new System.EventHandler(this.spePerContabilInicio_TextChanged);
            // 
            // speVencimento
            // 
            this.speVencimento.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.BindingSource_F, "CONDiaVencimento", true));
            this.speVencimento.EditValue = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.speVencimento.Location = new System.Drawing.Point(367, 202);
            this.speVencimento.Name = "speVencimento";
            this.speVencimento.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(),
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Ellipsis, "", -1, true, true, false, DevExpress.Utils.HorzAlignment.Center, ((System.Drawing.Image)(resources.GetObject("speVencimento.Properties.Buttons"))))});
            this.speVencimento.Properties.IsFloatValue = false;
            this.speVencimento.Properties.Mask.EditMask = "N00";
            this.speVencimento.Properties.MaxValue = new decimal(new int[] {
            31,
            0,
            0,
            0});
            this.speVencimento.Properties.MinValue = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.speVencimento.Size = new System.Drawing.Size(0, 20);
            this.speVencimento.StyleController = this.LayoutControl;
            this.speVencimento.TabIndex = 59;
            
            this.speVencimento.ValueChanged += new System.EventHandler(this.speVencimento_ValueChanged);
            this.speVencimento.TextChanged += new System.EventHandler(this.speVencimento_TextChanged);
            // 
            // calcEdit9
            // 
            this.calcEdit9.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.BindingSource_F, "CONValorCondominio", true));
            this.calcEdit9.Location = new System.Drawing.Point(123, 202);
            this.calcEdit9.Name = "calcEdit9";
            this.calcEdit9.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.calcEdit9.Size = new System.Drawing.Size(0, 20);
            this.calcEdit9.StyleController = this.LayoutControl;
            this.calcEdit9.TabIndex = 58;
            // 
            // calcEdit8
            // 
            this.calcEdit8.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.BindingSource_F, "CONValorGas", true));
            this.calcEdit8.Location = new System.Drawing.Point(255, 202);
            this.calcEdit8.Name = "calcEdit8";
            this.calcEdit8.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.calcEdit8.Size = new System.Drawing.Size(0, 20);
            this.calcEdit8.StyleController = this.LayoutControl;
            this.calcEdit8.TabIndex = 57;
            // 
            // calcEdit7
            // 
            this.calcEdit7.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.BindingSource_F, "CONValorJuros", true));
            this.calcEdit7.Location = new System.Drawing.Point(567, 147);
            this.calcEdit7.Name = "calcEdit7";
            this.calcEdit7.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.calcEdit7.Size = new System.Drawing.Size(0, 20);
            this.calcEdit7.StyleController = this.LayoutControl;
            this.calcEdit7.TabIndex = 52;
            // 
            // calcEdit6
            // 
            this.calcEdit6.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.BindingSource_F, "CONValorFR", true));
            this.calcEdit6.Location = new System.Drawing.Point(198, 147);
            this.calcEdit6.Name = "calcEdit6";
            this.calcEdit6.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.calcEdit6.Size = new System.Drawing.Size(0, 20);
            this.calcEdit6.StyleController = this.LayoutControl;
            this.calcEdit6.TabIndex = 51;
            // 
            // calcEdit5
            // 
            this.calcEdit5.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.BindingSource_F, "CONValorMulta", true));
            this.calcEdit5.Location = new System.Drawing.Point(452, 147);
            this.calcEdit5.Name = "calcEdit5";
            this.calcEdit5.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.calcEdit5.Size = new System.Drawing.Size(0, 20);
            this.calcEdit5.StyleController = this.LayoutControl;
            this.calcEdit5.TabIndex = 50;
            // 
            // radioGroup4
            // 
            this.radioGroup4.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.BindingSource_F, "CONTipoMulta", true));
            this.radioGroup4.Location = new System.Drawing.Point(313, 127);
            this.radioGroup4.Name = "radioGroup4";
            this.radioGroup4.Properties.Items.AddRange(new DevExpress.XtraEditors.Controls.RadioGroupItem[] {
            new DevExpress.XtraEditors.Controls.RadioGroupItem("D", "Di�ria"),
            new DevExpress.XtraEditors.Controls.RadioGroupItem("M", "Mensal")});
            this.radioGroup4.Size = new System.Drawing.Size(0, 0);
            this.radioGroup4.StyleController = this.LayoutControl;
            this.radioGroup4.TabIndex = 45;
            // 
            // checkEdit5
            // 
            this.checkEdit5.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.BindingSource_F, "COND0", true));
            this.checkEdit5.Location = new System.Drawing.Point(635, 75);
            this.checkEdit5.Name = "checkEdit5";
            this.checkEdit5.Properties.Caption = "d0";
            this.checkEdit5.Size = new System.Drawing.Size(0, 19);
            this.checkEdit5.StyleController = this.LayoutControl;
            this.checkEdit5.TabIndex = 43;
            // 
            // textEdit11
            // 
            this.textEdit11.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.BindingSource_F, "CONDigitoConta", true));
            this.textEdit11.Location = new System.Drawing.Point(574, 75);
            this.textEdit11.Name = "textEdit11";
            this.textEdit11.Size = new System.Drawing.Size(0, 20);
            this.textEdit11.StyleController = this.LayoutControl;
            this.textEdit11.TabIndex = 42;
            // 
            // label2
            // 
            this.label2.Location = new System.Drawing.Point(564, 70);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(0, 0);
            this.ToolTipController_F.SetSuperTip(this.label2, null);
            this.label2.TabIndex = 41;
            this.label2.Text = "-";
            this.label2.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // textEdit10
            // 
            this.textEdit10.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.BindingSource_F, "CONDigitoAgencia", true));
            this.textEdit10.Location = new System.Drawing.Point(412, 75);
            this.textEdit10.Name = "textEdit10";
            this.textEdit10.Size = new System.Drawing.Size(0, 20);
            this.textEdit10.StyleController = this.LayoutControl;
            this.textEdit10.TabIndex = 40;
            // 
            // rdgFR
            // 
            this.rdgFR.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.BindingSource_F, "CONTipoFR", true));
            this.rdgFR.Location = new System.Drawing.Point(19, 127);
            this.rdgFR.Name = "rdgFR";
            this.rdgFR.Properties.Items.AddRange(new DevExpress.XtraEditors.Controls.RadioGroupItem[] {
            new DevExpress.XtraEditors.Controls.RadioGroupItem("P", "% Condom�nio"),
            new DevExpress.XtraEditors.Controls.RadioGroupItem("V", "Valor Fixo")});
            this.rdgFR.Size = new System.Drawing.Size(0, 0);
            this.rdgFR.StyleController = this.LayoutControl;
            this.rdgFR.TabIndex = 39;
            this.rdgFR.EditValueChanged += new System.EventHandler(this.rdgFR_EditValueChanged);
            // 
            // textEdit9
            // 
            this.textEdit9.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.BindingSource_F, "CONConta", true));
            this.textEdit9.Location = new System.Drawing.Point(508, 75);
            this.textEdit9.Name = "textEdit9";
            this.textEdit9.Size = new System.Drawing.Size(0, 20);
            this.textEdit9.StyleController = this.LayoutControl;
            this.textEdit9.TabIndex = 37;
            // 
            // textEdit8
            // 
            this.textEdit8.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.BindingSource_F, "CONAgencia", true));
            this.textEdit8.Location = new System.Drawing.Point(336, 75);
            this.textEdit8.Name = "textEdit8";
            this.textEdit8.Size = new System.Drawing.Size(0, 20);
            this.textEdit8.StyleController = this.LayoutControl;
            this.textEdit8.TabIndex = 36;
            // 
            // label1
            // 
            this.label1.Location = new System.Drawing.Point(398, 70);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(0, 0);
            this.ToolTipController_F.SetSuperTip(this.label1, null);
            this.label1.TabIndex = 38;
            this.label1.Text = "-";
            this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lkpBancos
            // 
            this.lkpBancos.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.BindingSource_F, "CON_BCO", true));
            this.lkpBancos.Location = new System.Drawing.Point(71, 75);
            this.lkpBancos.Name = "lkpBancos";
            this.lkpBancos.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo),
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Plus)});
            this.lkpBancos.Properties.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("BCONome", "Bancos", 54, DevExpress.Utils.FormatType.None, "", true, DevExpress.Utils.HorzAlignment.Near)});
            this.lkpBancos.Properties.DataSource = this.bANCOSBindingSource;
            this.lkpBancos.Properties.DisplayMember = "BCONome";
            this.lkpBancos.Properties.ValueMember = "BCO";
            this.lkpBancos.Size = new System.Drawing.Size(0, 20);
            this.lkpBancos.StyleController = this.LayoutControl;
            this.lkpBancos.TabIndex = 35;
            this.lkpBancos.ButtonClick += new DevExpress.XtraEditors.Controls.ButtonPressedEventHandler(this.lkpBancos_ButtonClick);
            // 
            // speApartamentos
            // 
            this.speApartamentos.EditValue = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.speApartamentos.Location = new System.Drawing.Point(615, 387);
            this.speApartamentos.Name = "speApartamentos";
            this.speApartamentos.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(),
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Glyph, "", -1, true, true, false, DevExpress.Utils.HorzAlignment.Center, ((System.Drawing.Image)(resources.GetObject("speApartamentos.Properties.Buttons"))))});
            this.speApartamentos.Properties.IsFloatValue = false;
            this.speApartamentos.Properties.Mask.EditMask = "N00";
            this.speApartamentos.Size = new System.Drawing.Size(0, 22);
            this.speApartamentos.StyleController = this.LayoutControl;
            this.speApartamentos.TabIndex = 34;
            this.speApartamentos.ButtonClick += new DevExpress.XtraEditors.Controls.ButtonPressedEventHandler(this.speApartamentos_ButtonClick);
            // 
            // memoEdit1
            // 
            this.memoEdit1.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.BindingSource_F, "CONObservacao", true));
            this.memoEdit1.Location = new System.Drawing.Point(19, 183);
            this.memoEdit1.Name = "memoEdit1";
            this.memoEdit1.Size = new System.Drawing.Size(0, 0);
            this.memoEdit1.StyleController = this.LayoutControl;
            this.memoEdit1.TabIndex = 31;
            // 
            // speBlocos
            // 
            this.speBlocos.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.BindingSource_F, "CONTotalBloco", true));
            this.speBlocos.EditValue = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.speBlocos.Location = new System.Drawing.Point(278, 386);
            this.speBlocos.Name = "speBlocos";
            this.speBlocos.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(),
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Glyph, "", -1, true, true, false, DevExpress.Utils.HorzAlignment.Center, ((System.Drawing.Image)(resources.GetObject("speBlocos.Properties.Buttons"))))});
            this.speBlocos.Properties.IsFloatValue = false;
            this.speBlocos.Properties.Mask.EditMask = "N00";
            this.speBlocos.Size = new System.Drawing.Size(0, 22);
            this.speBlocos.StyleController = this.LayoutControl;
            this.speBlocos.TabIndex = 30;
            this.speBlocos.ButtonClick += new DevExpress.XtraEditors.Controls.ButtonPressedEventHandler(this.speBlocos_ButtonClick);
            // 
            // grdApartamentos
            // 
            this.grdApartamentos.DataSource = this.fKAPARTAMENTOSBLOCOSBindingSource;
            this.grdApartamentos.EmbeddedNavigator.Buttons.Append.Visible = false;
            this.grdApartamentos.EmbeddedNavigator.Buttons.CancelEdit.Visible = false;
            this.grdApartamentos.EmbeddedNavigator.Buttons.Edit.Visible = false;
            this.grdApartamentos.EmbeddedNavigator.Buttons.EndEdit.Visible = false;
            this.grdApartamentos.EmbeddedNavigator.Buttons.Remove.Visible = false;
            this.grdApartamentos.EmbeddedNavigator.Name = "";
            this.grdApartamentos.Location = new System.Drawing.Point(401, 206);
            this.grdApartamentos.MainView = this.grvApartamentos;
            this.grdApartamentos.Name = "grdApartamentos";
            this.grdApartamentos.Size = new System.Drawing.Size(0, 0);
            this.grdApartamentos.TabIndex = 26;
            this.grdApartamentos.UseEmbeddedNavigator = true;
            this.grdApartamentos.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.grvApartamentos});
            // 
            // fKAPARTAMENTOSBLOCOSBindingSource
            // 
            this.fKAPARTAMENTOSBLOCOSBindingSource.DataSource = this.fKBLOCOSCONDOMINIOSBindingSource;
            // 
            // grvApartamentos
            // 
            this.grvApartamentos.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colAPTNumero,
            this.colAPTFracaoIdeal});
            this.grvApartamentos.GridControl = this.grdApartamentos;
            this.grvApartamentos.Name = "grvApartamentos";
            this.grvApartamentos.OptionsCustomization.AllowFilter = false;
            this.grvApartamentos.OptionsMenu.EnableFooterMenu = false;
            this.grvApartamentos.OptionsMenu.EnableGroupPanelMenu = false;
            this.grvApartamentos.OptionsNavigation.AutoFocusNewRow = true;
            this.grvApartamentos.OptionsView.ShowDetailButtons = false;
            this.grvApartamentos.OptionsView.ShowGroupPanel = false;
            this.grvApartamentos.InvalidRowException += new DevExpress.XtraGrid.Views.Base.InvalidRowExceptionEventHandler(this.grvApartamentos_InvalidRowException);
            // 
            // colAPTNumero
            // 
            this.colAPTNumero.Caption = "Identifica��o";
            this.colAPTNumero.FieldName = "APTNumero";
            this.colAPTNumero.Name = "colAPTNumero";
            this.colAPTNumero.Visible = true;
            this.colAPTNumero.VisibleIndex = 0;
            // 
            // colAPTFracaoIdeal
            // 
            this.colAPTFracaoIdeal.Caption = "Fra��o Ideal";
            this.colAPTFracaoIdeal.DisplayFormat.FormatString = "n4";
            this.colAPTFracaoIdeal.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.colAPTFracaoIdeal.FieldName = "APTFracaoIdeal";
            this.colAPTFracaoIdeal.Name = "colAPTFracaoIdeal";
            this.colAPTFracaoIdeal.Visible = true;
            this.colAPTFracaoIdeal.VisibleIndex = 1;
            // 
            // grdBlocos
            // 
            this.grdBlocos.DataSource = this.fKBLOCOSCONDOMINIOSBindingSource;
            this.grdBlocos.EmbeddedNavigator.Buttons.Append.Visible = false;
            this.grdBlocos.EmbeddedNavigator.Buttons.CancelEdit.Visible = false;
            this.grdBlocos.EmbeddedNavigator.Buttons.Edit.Visible = false;
            this.grdBlocos.EmbeddedNavigator.Buttons.EndEdit.Visible = false;
            this.grdBlocos.EmbeddedNavigator.Buttons.Remove.Visible = false;
            this.grdBlocos.EmbeddedNavigator.Name = "";
            this.grdBlocos.Location = new System.Drawing.Point(14, 206);
            this.grdBlocos.MainView = this.grvBlocos;
            this.grdBlocos.Name = "grdBlocos";
            this.grdBlocos.Size = new System.Drawing.Size(0, 0);
            this.grdBlocos.TabIndex = 25;
            this.grdBlocos.UseEmbeddedNavigator = true;
            this.grdBlocos.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.grvBlocos});
            // 
            // grvBlocos
            // 
            this.grvBlocos.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colBLOCodigo,
            this.colBLONome});
            this.grvBlocos.GridControl = this.grdBlocos;
            this.grvBlocos.Name = "grvBlocos";
            this.grvBlocos.OptionsBehavior.AllowIncrementalSearch = true;
            this.grvBlocos.OptionsCustomization.AllowFilter = false;
            this.grvBlocos.OptionsMenu.EnableFooterMenu = false;
            this.grvBlocos.OptionsMenu.EnableGroupPanelMenu = false;
            this.grvBlocos.OptionsView.ShowDetailButtons = false;
            this.grvBlocos.OptionsView.ShowGroupPanel = false;
            this.grvBlocos.FocusedRowChanged += new DevExpress.XtraGrid.Views.Base.FocusedRowChangedEventHandler(this.grvBlocos_FocusedRowChanged);
            this.grvBlocos.ValidateRow += new DevExpress.XtraGrid.Views.Base.ValidateRowEventHandler(this.grvBlocos_ValidateRow);
            this.grvBlocos.CellValueChanging += new DevExpress.XtraGrid.Views.Base.CellValueChangedEventHandler(this.grvBlocos_CellValueChanging);
            this.grvBlocos.InvalidRowException += new DevExpress.XtraGrid.Views.Base.InvalidRowExceptionEventHandler(this.grvBlocos_InvalidRowException);
            // 
            // colBLOCodigo
            // 
            this.colBLOCodigo.Caption = "C�digo";
            this.colBLOCodigo.FieldName = "BLOCodigo";
            this.colBLOCodigo.Name = "colBLOCodigo";
            this.colBLOCodigo.Visible = true;
            this.colBLOCodigo.VisibleIndex = 0;
            this.colBLOCodigo.Width = 45;
            // 
            // colBLONome
            // 
            this.colBLONome.Caption = "Nome";
            this.colBLONome.FieldName = "BLONome";
            this.colBLONome.Name = "colBLONome";
            this.colBLONome.Visible = true;
            this.colBLONome.VisibleIndex = 1;
            this.colBLONome.Width = 204;
            // 
            // radioGroup2
            // 
            this.radioGroup2.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.BindingSource_F, "CONTipoFinalidade", true));
            this.radioGroup2.Location = new System.Drawing.Point(570, 122);
            this.radioGroup2.Name = "radioGroup2";
            this.radioGroup2.Properties.Items.AddRange(new DevExpress.XtraEditors.Controls.RadioGroupItem[] {
            new DevExpress.XtraEditors.Controls.RadioGroupItem("C", "Comercial"),
            new DevExpress.XtraEditors.Controls.RadioGroupItem("R", "Residencial")});
            this.radioGroup2.Size = new System.Drawing.Size(0, 0);
            this.radioGroup2.StyleController = this.LayoutControl;
            this.radioGroup2.TabIndex = 24;
            // 
            // checkEdit4
            // 
            this.checkEdit4.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.BindingSource_F, "CONInquilinoPagaFR", true));
            this.checkEdit4.Location = new System.Drawing.Point(314, 157);
            this.checkEdit4.Name = "checkEdit4";
            this.checkEdit4.Properties.Caption = "Inquilino Paga";
            this.checkEdit4.Size = new System.Drawing.Size(0, 19);
            this.checkEdit4.StyleController = this.LayoutControl;
            this.checkEdit4.TabIndex = 20;
            // 
            // checkEdit3
            // 
            this.checkEdit3.Cursor = System.Windows.Forms.Cursors.Arrow;
            this.checkEdit3.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.BindingSource_F, "CONSindicoPagaFR", true));
            this.checkEdit3.Location = new System.Drawing.Point(314, 127);
            this.checkEdit3.Name = "checkEdit3";
            this.checkEdit3.Properties.Caption = "S�ndico Paga";
            this.checkEdit3.Size = new System.Drawing.Size(0, 19);
            this.checkEdit3.StyleController = this.LayoutControl;
            this.checkEdit3.TabIndex = 19;
            // 
            // calcEdit4
            // 
            this.calcEdit4.Cursor = System.Windows.Forms.Cursors.Arrow;
            this.calcEdit4.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.BindingSource_F, "CONCondominioSubSindico", true));
            this.calcEdit4.EditValue = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.calcEdit4.Location = new System.Drawing.Point(273, 157);
            this.calcEdit4.Name = "calcEdit4";
            this.calcEdit4.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.calcEdit4.Properties.DisplayFormat.FormatString = "n2";
            this.calcEdit4.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.calcEdit4.Properties.ShowCloseButton = true;
            this.calcEdit4.Size = new System.Drawing.Size(0, 20);
            this.calcEdit4.StyleController = this.LayoutControl;
            this.calcEdit4.TabIndex = 18;
            // 
            // calcEdit3
            // 
            this.calcEdit3.Cursor = System.Windows.Forms.Cursors.Arrow;
            this.calcEdit3.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.BindingSource_F, "CONCondominioSindico", true));
            this.calcEdit3.EditValue = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.calcEdit3.Location = new System.Drawing.Point(273, 127);
            this.calcEdit3.Name = "calcEdit3";
            this.calcEdit3.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.calcEdit3.Properties.DisplayFormat.FormatString = "n2";
            this.calcEdit3.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.calcEdit3.Properties.ShowCloseButton = true;
            this.calcEdit3.Size = new System.Drawing.Size(0, 20);
            this.calcEdit3.StyleController = this.LayoutControl;
            this.calcEdit3.TabIndex = 17;
            // 
            // calcEdit2
            // 
            this.calcEdit2.Cursor = System.Windows.Forms.Cursors.Arrow;
            this.calcEdit2.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.BindingSource_F, "CONRateioSubSindico", true));
            this.calcEdit2.EditValue = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.calcEdit2.Location = new System.Drawing.Point(123, 157);
            this.calcEdit2.Name = "calcEdit2";
            this.calcEdit2.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.calcEdit2.Properties.DisplayFormat.FormatString = "n2";
            this.calcEdit2.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.calcEdit2.Properties.ShowCloseButton = true;
            this.calcEdit2.Size = new System.Drawing.Size(0, 20);
            this.calcEdit2.StyleController = this.LayoutControl;
            this.calcEdit2.TabIndex = 16;
            // 
            // calcEdit1
            // 
            this.calcEdit1.Cursor = System.Windows.Forms.Cursors.Arrow;
            this.calcEdit1.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.BindingSource_F, "CONRateioSindico", true));
            this.calcEdit1.EditValue = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.calcEdit1.Location = new System.Drawing.Point(123, 127);
            this.calcEdit1.Name = "calcEdit1";
            this.calcEdit1.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.calcEdit1.Properties.DisplayFormat.FormatString = "n2";
            this.calcEdit1.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.calcEdit1.Properties.ShowCloseButton = true;
            this.calcEdit1.Size = new System.Drawing.Size(0, 20);
            this.calcEdit1.StyleController = this.LayoutControl;
            this.calcEdit1.TabIndex = 15;
            // 
            // checkEdit1
            // 
            this.checkEdit1.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.BindingSource_F, "CONProcuracao", true));
            this.checkEdit1.Location = new System.Drawing.Point(290, 75);
            this.checkEdit1.Name = "checkEdit1";
            this.checkEdit1.Properties.Caption = "Possui procura��o";
            this.checkEdit1.Size = new System.Drawing.Size(0, 19);
            this.checkEdit1.StyleController = this.LayoutControl;
            this.checkEdit1.TabIndex = 13;
            // 
            // radioGroup1
            // 
            this.radioGroup1.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.BindingSource_F, "CONTipoConstrucao", true));
            this.radioGroup1.Location = new System.Drawing.Point(452, 122);
            this.radioGroup1.Name = "radioGroup1";
            this.radioGroup1.Properties.Items.AddRange(new DevExpress.XtraEditors.Controls.RadioGroupItem[] {
            new DevExpress.XtraEditors.Controls.RadioGroupItem("V", "Vertical"),
            new DevExpress.XtraEditors.Controls.RadioGroupItem("H", "Horizontal")});
            this.radioGroup1.Size = new System.Drawing.Size(0, 0);
            this.radioGroup1.StyleController = this.LayoutControl;
            this.radioGroup1.TabIndex = 21;
            // 
            // textEdit1
            // 
            this.textEdit1.Cursor = System.Windows.Forms.Cursors.Arrow;
            this.textEdit1.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.BindingSource_F, "CONCnpj", true));
            this.textEdit1.Location = new System.Drawing.Point(120, 75);
            this.textEdit1.Name = "textEdit1";
            this.textEdit1.Properties.Mask.EditMask = "##.###.###/####-##";
            this.textEdit1.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Simple;
            this.textEdit1.Properties.Mask.PlaceHolder = ' ';
            this.textEdit1.Size = new System.Drawing.Size(0, 20);
            this.textEdit1.StyleController = this.LayoutControl;
            this.textEdit1.TabIndex = 12;
            // 
            // checkEdit2
            // 
            this.checkEdit2.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.BindingSource_F, "CONFracaoIdeal", true));
            this.checkEdit2.Location = new System.Drawing.Point(536, 75);
            this.checkEdit2.Name = "checkEdit2";
            this.checkEdit2.Properties.Caption = "Utiliza Fra��o Ideal";
            this.checkEdit2.Size = new System.Drawing.Size(0, 19);
            this.checkEdit2.StyleController = this.LayoutControl;
            this.checkEdit2.TabIndex = 14;
            // 
            // lkpCidades
            // 
            this.lkpCidades.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.BindingSource_F, "CON_CID", true));
            this.lkpCidades.Location = new System.Drawing.Point(313, 127);
            this.lkpCidades.Name = "lkpCidades";
            this.lkpCidades.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo),
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Plus)});
            this.lkpCidades.Properties.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("CIDNome", "Nome", 64, DevExpress.Utils.FormatType.None, "", true, DevExpress.Utils.HorzAlignment.Near),
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("CIDUf")});
            this.lkpCidades.Properties.DataSource = this.cIDADESBindingSource;
            this.lkpCidades.Properties.DisplayMember = "CIDNome";
            this.lkpCidades.Properties.ValueMember = "CID";
            this.lkpCidades.Size = new System.Drawing.Size(0, 20);
            this.lkpCidades.StyleController = this.LayoutControl;
            this.lkpCidades.TabIndex = 10;
            this.lkpCidades.ButtonClick += new DevExpress.XtraEditors.Controls.ButtonPressedEventHandler(this.lkpCidades_ButtonClick);
            this.lkpCidades.EditValueChanged += new System.EventHandler(this.lkpCidades_EditValueChanged);
            // 
            // cIDADESBindingSource
            // 
            this.cIDADESBindingSource.DataMember = "CIDADES";
            this.cIDADESBindingSource.DataSource = this.dCondominiosCampos;
            // 
            // lkpUF
            // 
            this.lkpUF.Enabled = false;
            this.lkpUF.Location = new System.Drawing.Point(602, 127);
            this.lkpUF.Name = "lkpUF";
            this.lkpUF.Properties.DataSource = this.cIDADESBindingSource;
            this.lkpUF.Properties.DisplayMember = "CIDUf";
            this.lkpUF.Properties.ValueMember = "CID";
            this.lkpUF.Size = new System.Drawing.Size(0, 20);
            this.lkpUF.StyleController = this.LayoutControl;
            this.lkpUF.TabIndex = 9;
            // 
            // textEdit5
            // 
            this.textEdit5.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.BindingSource_F, "CONBairro", true));
            this.textEdit5.Location = new System.Drawing.Point(74, 127);
            this.textEdit5.Name = "textEdit5";
            this.textEdit5.Size = new System.Drawing.Size(0, 20);
            this.textEdit5.StyleController = this.LayoutControl;
            this.textEdit5.TabIndex = 8;
            // 
            // textEdit4
            // 
            this.textEdit4.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.BindingSource_F, "CONEndereco", true));
            this.textEdit4.Location = new System.Drawing.Point(297, 96);
            this.textEdit4.Name = "textEdit4";
            this.textEdit4.Size = new System.Drawing.Size(0, 20);
            this.textEdit4.StyleController = this.LayoutControl;
            this.textEdit4.TabIndex = 6;
            // 
            // textEdit3
            // 
            this.textEdit3.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.BindingSource_F, "CONCep", true));
            this.textEdit3.Location = new System.Drawing.Point(74, 96);
            this.textEdit3.Name = "textEdit3";
            this.textEdit3.Properties.Mask.EditMask = "99999-999";
            this.textEdit3.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Simple;
            this.textEdit3.Properties.Mask.PlaceHolder = ' ';
            this.textEdit3.Size = new System.Drawing.Size(0, 20);
            this.textEdit3.StyleController = this.LayoutControl;
            this.textEdit3.TabIndex = 5;
            // 
            // bnvHistoricos
            // 
            this.bnvHistoricos.AddNewItem = null;
            this.bnvHistoricos.AutoSize = false;
            this.bnvHistoricos.CountItem = null;
            this.bnvHistoricos.DeleteItem = null;
            this.bnvHistoricos.Dock = System.Windows.Forms.DockStyle.None;
            this.bnvHistoricos.GripStyle = System.Windows.Forms.ToolStripGripStyle.Hidden;
            this.bnvHistoricos.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.btnAddHistorico,
            this.btnEditHistorico,
            this.btnPrintHistorico,
            this.toolStripSeparator3,
            this.btnDelHistorico});
            this.bnvHistoricos.Location = new System.Drawing.Point(16, 389);
            this.bnvHistoricos.MoveFirstItem = null;
            this.bnvHistoricos.MoveLastItem = null;
            this.bnvHistoricos.MoveNextItem = null;
            this.bnvHistoricos.MovePreviousItem = null;
            this.bnvHistoricos.Name = "bnvHistoricos";
            this.bnvHistoricos.PositionItem = null;
            this.bnvHistoricos.ShowItemToolTips = false;
            this.bnvHistoricos.Size = new System.Drawing.Size(0, 0);
            this.ToolTipController_F.SetSuperTip(this.bnvHistoricos, null);
            this.bnvHistoricos.TabIndex = 108;
            this.bnvHistoricos.Visible = false;
            this.bnvHistoricos.ItemClicked += new System.Windows.Forms.ToolStripItemClickedEventHandler(this.bnvHistoricos_ItemClicked);
            // 
            // btnAddHistorico
            // 
            this.btnAddHistorico.Image = ((System.Drawing.Image)(resources.GetObject("btnAddHistorico.Image")));
            this.btnAddHistorico.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btnAddHistorico.Name = "btnAddHistorico";
            this.btnAddHistorico.Size = new System.Drawing.Size(56, 20);
            this.btnAddHistorico.Text = "&Incluir";
            // 
            // btnEditHistorico
            // 
            this.btnEditHistorico.Image = ((System.Drawing.Image)(resources.GetObject("btnEditHistorico.Image")));
            this.btnEditHistorico.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btnEditHistorico.Name = "btnEditHistorico";
            this.btnEditHistorico.Size = new System.Drawing.Size(60, 20);
            this.btnEditHistorico.Text = "&Alterar";
            // 
            // btnPrintHistorico
            // 
            this.btnPrintHistorico.Image = ((System.Drawing.Image)(resources.GetObject("btnPrintHistorico.Image")));
            this.btnPrintHistorico.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btnPrintHistorico.Name = "btnPrintHistorico";
            this.btnPrintHistorico.Size = new System.Drawing.Size(65, 20);
            this.btnPrintHistorico.Text = "&Imprimir";
            // 
            // toolStripSeparator3
            // 
            this.toolStripSeparator3.Name = "toolStripSeparator3";
            this.toolStripSeparator3.Size = new System.Drawing.Size(6, 22);
            // 
            // btnDelHistorico
            // 
            this.btnDelHistorico.Image = ((System.Drawing.Image)(resources.GetObject("btnDelHistorico.Image")));
            this.btnDelHistorico.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btnDelHistorico.Name = "btnDelHistorico";
            this.btnDelHistorico.Size = new System.Drawing.Size(58, 20);
            this.btnDelHistorico.Text = "&Excluir";
            // 
            // bnvCorpoDiretivo
            // 
            this.bnvCorpoDiretivo.AddNewItem = null;
            this.bnvCorpoDiretivo.AutoSize = false;
            this.bnvCorpoDiretivo.CountItem = null;
            this.bnvCorpoDiretivo.DeleteItem = null;
            this.bnvCorpoDiretivo.Dock = System.Windows.Forms.DockStyle.None;
            this.bnvCorpoDiretivo.GripStyle = System.Windows.Forms.ToolStripGripStyle.Hidden;
            this.bnvCorpoDiretivo.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.btnAddCorpoDiretivo,
            this.btnEditCorpoDiretivo,
            this.btnPrintCorpoDiretivo,
            this.toolStripSeparator7,
            this.btnDelCorpoDiretivo});
            this.bnvCorpoDiretivo.Location = new System.Drawing.Point(16, 389);
            this.bnvCorpoDiretivo.MoveFirstItem = null;
            this.bnvCorpoDiretivo.MoveLastItem = null;
            this.bnvCorpoDiretivo.MoveNextItem = null;
            this.bnvCorpoDiretivo.MovePreviousItem = null;
            this.bnvCorpoDiretivo.Name = "bnvCorpoDiretivo";
            this.bnvCorpoDiretivo.PositionItem = null;
            this.bnvCorpoDiretivo.Size = new System.Drawing.Size(0, 0);
            this.ToolTipController_F.SetSuperTip(this.bnvCorpoDiretivo, null);
            this.bnvCorpoDiretivo.TabIndex = 112;
            this.bnvCorpoDiretivo.Visible = false;
            this.bnvCorpoDiretivo.ItemClicked += new System.Windows.Forms.ToolStripItemClickedEventHandler(this.bnvCorpoDiretivo_ItemClicked);
            // 
            // btnAddCorpoDiretivo
            // 
            this.btnAddCorpoDiretivo.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.btnAddCorpoDiretivo.Image = ((System.Drawing.Image)(resources.GetObject("btnAddCorpoDiretivo.Image")));
            this.btnAddCorpoDiretivo.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btnAddCorpoDiretivo.Name = "btnAddCorpoDiretivo";
            this.btnAddCorpoDiretivo.Size = new System.Drawing.Size(23, 20);
            this.btnAddCorpoDiretivo.Text = "Incluir";
            // 
            // btnEditCorpoDiretivo
            // 
            this.btnEditCorpoDiretivo.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.btnEditCorpoDiretivo.Image = ((System.Drawing.Image)(resources.GetObject("btnEditCorpoDiretivo.Image")));
            this.btnEditCorpoDiretivo.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btnEditCorpoDiretivo.Name = "btnEditCorpoDiretivo";
            this.btnEditCorpoDiretivo.Size = new System.Drawing.Size(23, 20);
            this.btnEditCorpoDiretivo.Text = "Alterar";
            // 
            // btnPrintCorpoDiretivo
            // 
            this.btnPrintCorpoDiretivo.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.btnPrintCorpoDiretivo.Image = ((System.Drawing.Image)(resources.GetObject("btnPrintCorpoDiretivo.Image")));
            this.btnPrintCorpoDiretivo.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btnPrintCorpoDiretivo.Name = "btnPrintCorpoDiretivo";
            this.btnPrintCorpoDiretivo.Size = new System.Drawing.Size(23, 20);
            this.btnPrintCorpoDiretivo.Text = "Imprimir";
            // 
            // toolStripSeparator7
            // 
            this.toolStripSeparator7.Name = "toolStripSeparator7";
            this.toolStripSeparator7.Size = new System.Drawing.Size(6, 23);
            // 
            // btnDelCorpoDiretivo
            // 
            this.btnDelCorpoDiretivo.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.btnDelCorpoDiretivo.Image = ((System.Drawing.Image)(resources.GetObject("btnDelCorpoDiretivo.Image")));
            this.btnDelCorpoDiretivo.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btnDelCorpoDiretivo.Name = "btnDelCorpoDiretivo";
            this.btnDelCorpoDiretivo.Size = new System.Drawing.Size(23, 20);
            this.btnDelCorpoDiretivo.Text = "Excluir";
            // 
            // bnvBlocos
            // 
            this.bnvBlocos.AddNewItem = null;
            this.bnvBlocos.AutoSize = false;
            this.bnvBlocos.BindingSource = this.fKBLOCOSCONDOMINIOSBindingSource;
            this.bnvBlocos.CountItem = null;
            this.bnvBlocos.DeleteItem = null;
            this.bnvBlocos.Dock = System.Windows.Forms.DockStyle.None;
            this.bnvBlocos.GripStyle = System.Windows.Forms.ToolStripGripStyle.Hidden;
            this.bnvBlocos.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.btnAddBlocos,
            this.btnEditBlocos,
            this.btnPrintBlocos,
            this.toolStripSeparator5,
            this.btnDelBlocos,
            this.toolStripSeparator6,
            this.btnPostBlocos,
            this.btnCancelBlocos});
            this.bnvBlocos.LayoutStyle = System.Windows.Forms.ToolStripLayoutStyle.Flow;
            this.bnvBlocos.Location = new System.Drawing.Point(19, 386);
            this.bnvBlocos.MoveFirstItem = null;
            this.bnvBlocos.MoveLastItem = null;
            this.bnvBlocos.MoveNextItem = null;
            this.bnvBlocos.MovePreviousItem = null;
            this.bnvBlocos.Name = "bnvBlocos";
            this.bnvBlocos.PositionItem = null;
            this.bnvBlocos.Size = new System.Drawing.Size(0, 0);
            this.ToolTipController_F.SetSuperTip(this.bnvBlocos, null);
            this.bnvBlocos.TabIndex = 109;
            this.bnvBlocos.Visible = false;
            this.bnvBlocos.RefreshItems += new System.EventHandler(this.bnvBlocos_RefreshItems);
            this.bnvBlocos.ItemClicked += new System.Windows.Forms.ToolStripItemClickedEventHandler(this.bnvBlocos_ItemClicked);
            // 
            // btnAddBlocos
            // 
            this.btnAddBlocos.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.btnAddBlocos.Image = ((System.Drawing.Image)(resources.GetObject("btnAddBlocos.Image")));
            this.btnAddBlocos.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btnAddBlocos.Name = "btnAddBlocos";
            this.btnAddBlocos.Size = new System.Drawing.Size(23, 20);
            this.btnAddBlocos.Text = "Incluir";
            // 
            // btnEditBlocos
            // 
            this.btnEditBlocos.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.btnEditBlocos.Image = ((System.Drawing.Image)(resources.GetObject("btnEditBlocos.Image")));
            this.btnEditBlocos.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btnEditBlocos.Name = "btnEditBlocos";
            this.btnEditBlocos.Size = new System.Drawing.Size(23, 20);
            this.btnEditBlocos.Text = "Alterar";
            // 
            // btnPrintBlocos
            // 
            this.btnPrintBlocos.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.btnPrintBlocos.Image = ((System.Drawing.Image)(resources.GetObject("btnPrintBlocos.Image")));
            this.btnPrintBlocos.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btnPrintBlocos.Name = "btnPrintBlocos";
            this.btnPrintBlocos.Size = new System.Drawing.Size(23, 20);
            this.btnPrintBlocos.Text = "Imprimir";
            // 
            // toolStripSeparator5
            // 
            this.toolStripSeparator5.Name = "toolStripSeparator5";
            this.toolStripSeparator5.Size = new System.Drawing.Size(6, 23);
            // 
            // btnDelBlocos
            // 
            this.btnDelBlocos.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.btnDelBlocos.Image = ((System.Drawing.Image)(resources.GetObject("btnDelBlocos.Image")));
            this.btnDelBlocos.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btnDelBlocos.Name = "btnDelBlocos";
            this.btnDelBlocos.Size = new System.Drawing.Size(23, 20);
            this.btnDelBlocos.Text = "Excluir";
            // 
            // toolStripSeparator6
            // 
            this.toolStripSeparator6.Name = "toolStripSeparator6";
            this.toolStripSeparator6.Size = new System.Drawing.Size(6, 23);
            // 
            // btnPostBlocos
            // 
            this.btnPostBlocos.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.btnPostBlocos.Enabled = false;
            this.btnPostBlocos.Image = ((System.Drawing.Image)(resources.GetObject("btnPostBlocos.Image")));
            this.btnPostBlocos.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btnPostBlocos.Name = "btnPostBlocos";
            this.btnPostBlocos.Size = new System.Drawing.Size(23, 20);
            this.btnPostBlocos.Text = "Confirmar";
            // 
            // btnCancelBlocos
            // 
            this.btnCancelBlocos.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.btnCancelBlocos.Enabled = false;
            this.btnCancelBlocos.Image = ((System.Drawing.Image)(resources.GetObject("btnCancelBlocos.Image")));
            this.btnCancelBlocos.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btnCancelBlocos.Name = "btnCancelBlocos";
            this.btnCancelBlocos.Size = new System.Drawing.Size(23, 20);
            this.btnCancelBlocos.Text = "Cancelar";
            // 
            // bnvApartamentos
            // 
            this.bnvApartamentos.AddNewItem = null;
            this.bnvApartamentos.AutoSize = false;
            this.bnvApartamentos.CountItem = null;
            this.bnvApartamentos.DeleteItem = null;
            this.bnvApartamentos.Dock = System.Windows.Forms.DockStyle.None;
            this.bnvApartamentos.GripStyle = System.Windows.Forms.ToolStripGripStyle.Hidden;
            this.bnvApartamentos.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.btnAddApto,
            this.btnEditApto,
            this.btnPrintApto,
            this.toolStripSeparator4,
            this.btnDelApto});
            this.bnvApartamentos.LayoutStyle = System.Windows.Forms.ToolStripLayoutStyle.Flow;
            this.bnvApartamentos.Location = new System.Drawing.Point(406, 387);
            this.bnvApartamentos.MoveFirstItem = null;
            this.bnvApartamentos.MoveLastItem = null;
            this.bnvApartamentos.MoveNextItem = null;
            this.bnvApartamentos.MovePreviousItem = null;
            this.bnvApartamentos.Name = "bnvApartamentos";
            this.bnvApartamentos.PositionItem = null;
            this.bnvApartamentos.Size = new System.Drawing.Size(0, 0);
            this.ToolTipController_F.SetSuperTip(this.bnvApartamentos, null);
            this.bnvApartamentos.TabIndex = 110;
            this.bnvApartamentos.Visible = false;
            this.bnvApartamentos.ItemClicked += new System.Windows.Forms.ToolStripItemClickedEventHandler(this.bnvApartamentos_ItemClicked);
            // 
            // btnAddApto
            // 
            this.btnAddApto.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.btnAddApto.Image = ((System.Drawing.Image)(resources.GetObject("btnAddApto.Image")));
            this.btnAddApto.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btnAddApto.Name = "btnAddApto";
            this.btnAddApto.Size = new System.Drawing.Size(23, 20);
            this.btnAddApto.Text = "Incluir";
            // 
            // btnEditApto
            // 
            this.btnEditApto.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.btnEditApto.Image = ((System.Drawing.Image)(resources.GetObject("btnEditApto.Image")));
            this.btnEditApto.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btnEditApto.Name = "btnEditApto";
            this.btnEditApto.Size = new System.Drawing.Size(23, 20);
            this.btnEditApto.Text = "Editar";
            // 
            // btnPrintApto
            // 
            this.btnPrintApto.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.btnPrintApto.Image = ((System.Drawing.Image)(resources.GetObject("btnPrintApto.Image")));
            this.btnPrintApto.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btnPrintApto.Name = "btnPrintApto";
            this.btnPrintApto.Size = new System.Drawing.Size(23, 20);
            this.btnPrintApto.Text = "Imprimir";
            // 
            // toolStripSeparator4
            // 
            this.toolStripSeparator4.Name = "toolStripSeparator4";
            this.toolStripSeparator4.Size = new System.Drawing.Size(6, 23);
            // 
            // btnDelApto
            // 
            this.btnDelApto.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.btnDelApto.Image = ((System.Drawing.Image)(resources.GetObject("btnDelApto.Image")));
            this.btnDelApto.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btnDelApto.Name = "btnDelApto";
            this.btnDelApto.Size = new System.Drawing.Size(23, 20);
            this.btnDelApto.Text = "Excluir";
            // 
            // textEdit2
            // 
            this.textEdit2.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.BindingSource_F, "CONNome", true));
            this.textEdit2.Location = new System.Drawing.Point(211, 10);
            this.textEdit2.Name = "textEdit2";
            this.textEdit2.Size = new System.Drawing.Size(283, 20);
            this.textEdit2.StyleController = this.LayoutControl;
            this.textEdit2.TabIndex = 4;
            // 
            // txtCodcon
            // 
            this.txtCodcon.Cursor = System.Windows.Forms.Cursors.Default;
            this.txtCodcon.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.BindingSource_F, "CONCodigo", true));
            this.txtCodcon.Location = new System.Drawing.Point(111, 10);
            this.txtCodcon.Name = "txtCodcon";
            this.txtCodcon.Size = new System.Drawing.Size(54, 20);
            this.txtCodcon.StyleController = this.LayoutControl;
            this.txtCodcon.TabIndex = 3;
            this.txtCodcon.Validated += new System.EventHandler(this.txtCodcon_Validated);
            // 
            // bnvAplicacoes
            // 
            this.bnvAplicacoes.AddNewItem = null;
            this.bnvAplicacoes.AutoSize = false;
            this.bnvAplicacoes.BindingSource = this.fKAPLICACOESCONDOMINIOSBindingSource;
            this.bnvAplicacoes.CountItem = null;
            this.bnvAplicacoes.DeleteItem = null;
            this.bnvAplicacoes.Dock = System.Windows.Forms.DockStyle.None;
            this.bnvAplicacoes.GripStyle = System.Windows.Forms.ToolStripGripStyle.Hidden;
            this.bnvAplicacoes.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.btnAddAplicacao,
            this.btnAddSubAplicacao,
            this.toolStripSeparator2,
            this.btnEditAplicacao,
            this.btnPrintAplicacao,
            this.toolStripSeparator1,
            this.btnDelAplicacao});
            this.bnvAplicacoes.Location = new System.Drawing.Point(16, 391);
            this.bnvAplicacoes.MoveFirstItem = null;
            this.bnvAplicacoes.MoveLastItem = null;
            this.bnvAplicacoes.MoveNextItem = null;
            this.bnvAplicacoes.MovePreviousItem = null;
            this.bnvAplicacoes.Name = "bnvAplicacoes";
            this.bnvAplicacoes.PositionItem = null;
            this.bnvAplicacoes.Size = new System.Drawing.Size(0, 0);
            this.ToolTipController_F.SetSuperTip(this.bnvAplicacoes, null);
            this.bnvAplicacoes.TabIndex = 84;
            this.bnvAplicacoes.Visible = false;
            this.bnvAplicacoes.RefreshItems += new System.EventHandler(this.bnvAplicacoes_RefreshItems);
            this.bnvAplicacoes.ItemClicked += new System.Windows.Forms.ToolStripItemClickedEventHandler(this.bnvAplicacoes_ItemClicked);
            // 
            // btnAddAplicacao
            // 
            this.btnAddAplicacao.Image = ((System.Drawing.Image)(resources.GetObject("btnAddAplicacao.Image")));
            this.btnAddAplicacao.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btnAddAplicacao.Name = "btnAddAplicacao";
            this.btnAddAplicacao.Size = new System.Drawing.Size(88, 20);
            this.btnAddAplicacao.Text = "Incluir Conta";
            // 
            // btnAddSubAplicacao
            // 
            this.btnAddSubAplicacao.Image = ((System.Drawing.Image)(resources.GetObject("btnAddSubAplicacao.Image")));
            this.btnAddSubAplicacao.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btnAddSubAplicacao.Name = "btnAddSubAplicacao";
            this.btnAddSubAplicacao.Size = new System.Drawing.Size(110, 20);
            this.btnAddSubAplicacao.Text = "Incluir Sub-Conta";
            // 
            // toolStripSeparator2
            // 
            this.toolStripSeparator2.Name = "toolStripSeparator2";
            this.toolStripSeparator2.Size = new System.Drawing.Size(6, 21);
            // 
            // btnEditAplicacao
            // 
            this.btnEditAplicacao.Image = ((System.Drawing.Image)(resources.GetObject("btnEditAplicacao.Image")));
            this.btnEditAplicacao.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btnEditAplicacao.Name = "btnEditAplicacao";
            this.btnEditAplicacao.Size = new System.Drawing.Size(55, 20);
            this.btnEditAplicacao.Text = "Editar";
            // 
            // btnPrintAplicacao
            // 
            this.btnPrintAplicacao.Image = ((System.Drawing.Image)(resources.GetObject("btnPrintAplicacao.Image")));
            this.btnPrintAplicacao.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btnPrintAplicacao.Name = "btnPrintAplicacao";
            this.btnPrintAplicacao.Size = new System.Drawing.Size(65, 20);
            this.btnPrintAplicacao.Text = "Imprimir";
            // 
            // toolStripSeparator1
            // 
            this.toolStripSeparator1.Name = "toolStripSeparator1";
            this.toolStripSeparator1.Size = new System.Drawing.Size(6, 21);
            // 
            // btnDelAplicacao
            // 
            this.btnDelAplicacao.Image = ((System.Drawing.Image)(resources.GetObject("btnDelAplicacao.Image")));
            this.btnDelAplicacao.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btnDelAplicacao.Name = "btnDelAplicacao";
            this.btnDelAplicacao.Size = new System.Drawing.Size(58, 20);
            this.btnDelAplicacao.Text = "Excluir";
            // 
            // LayoutControlGroup
            // 
            this.LayoutControlGroup.AllowCustomizeChildren = false;
            this.LayoutControlGroup.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItem1,
            this.layoutControlItem2,
            this.tcgCondominio,
            this.layoutControlItem98});
            this.LayoutControlGroup.Location = new System.Drawing.Point(0, 0);
            this.LayoutControlGroup.Name = "LayoutControlGroup";
            this.LayoutControlGroup.Padding = new DevExpress.XtraLayout.Utils.Padding(0, 0, 0, 0);
            this.LayoutControlGroup.Size = new System.Drawing.Size(695, 432);
            this.LayoutControlGroup.Spacing = new DevExpress.XtraLayout.Utils.Padding(0, 0, 3, 3);
            this.LayoutControlGroup.Text = "LayoutControlGroup";
            this.LayoutControlGroup.TextVisible = false;
            // 
            // layoutControlItem1
            // 
            this.layoutControlItem1.Control = this.txtCodcon;
            this.layoutControlItem1.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItem1.Name = "layoutControlItem1";
            this.layoutControlItem1.Padding = new DevExpress.XtraLayout.Utils.Padding(5, 5, 5, 5);
            this.layoutControlItem1.Size = new System.Drawing.Size(169, 31);
            this.layoutControlItem1.Spacing = new DevExpress.XtraLayout.Utils.Padding(0, 0, 0, 0);
            this.layoutControlItem1.Text = "CODCON";
            this.layoutControlItem1.TextSize = new System.Drawing.Size(99, 20);
            this.layoutControlItem1.TextVisible = true;
            // 
            // layoutControlItem2
            // 
            this.layoutControlItem2.Control = this.textEdit2;
            this.layoutControlItem2.Location = new System.Drawing.Point(169, 0);
            this.layoutControlItem2.Name = "layoutControlItem2";
            this.layoutControlItem2.Padding = new DevExpress.XtraLayout.Utils.Padding(5, 5, 5, 5);
            this.layoutControlItem2.Size = new System.Drawing.Size(329, 31);
            this.layoutControlItem2.Spacing = new DevExpress.XtraLayout.Utils.Padding(0, 0, 0, 0);
            this.layoutControlItem2.Text = "Nome";
            this.layoutControlItem2.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem2.TextSize = new System.Drawing.Size(30, 20);
            this.layoutControlItem2.TextVisible = true;
            // 
            // tcgCondominio
            // 
            this.tcgCondominio.Location = new System.Drawing.Point(0, 31);
            this.tcgCondominio.Name = "tcgCondominio";
            this.tcgCondominio.Padding = new DevExpress.XtraLayout.Utils.Padding(5, 5, 5, 5);
            this.tcgCondominio.SelectedTabPage = this.layoutControlGroup1;
            this.tcgCondominio.SelectedTabPageIndex = 8;
            this.tcgCondominio.Size = new System.Drawing.Size(693, 393);
            this.tcgCondominio.Spacing = new DevExpress.XtraLayout.Utils.Padding(1, 1, 5, 1);
            this.tcgCondominio.TabPages.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.lcgBasico,
            this.layoutControlGroup5,
            this.layoutControlGroup13,
            this.layoutControlGroup14,
            this.layoutControlGroup28,
            this.layoutControlGroup29,
            this.layoutControlGroup35,
            this.layoutControlGroup42,
            this.layoutControlGroup1,
            this.layoutControlGroup46});
            this.tcgCondominio.Text = "tcgCondominio";
            this.tcgCondominio.TextVisible = true;
            // 
            // layoutControlGroup1
            // 
            this.layoutControlGroup1.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.emptySpaceItem1,
            this.layoutControlGroup43});
            this.layoutControlGroup1.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroup1.Name = "layoutControlGroup1";
            this.layoutControlGroup1.Padding = new DevExpress.XtraLayout.Utils.Padding(0, 0, 0, 0);
            this.layoutControlGroup1.Size = new System.Drawing.Size(672, 347);
            this.layoutControlGroup1.Spacing = new DevExpress.XtraLayout.Utils.Padding(2, 2, 2, 2);
            this.layoutControlGroup1.Text = "&9-Dep. Pessoal";
            this.layoutControlGroup1.TextVisible = true;
            // 
            // emptySpaceItem1
            // 
            this.emptySpaceItem1.Location = new System.Drawing.Point(0, 104);
            this.emptySpaceItem1.Name = "emptySpaceItem1";
            this.emptySpaceItem1.Padding = new DevExpress.XtraLayout.Utils.Padding(5, 5, 5, 5);
            this.emptySpaceItem1.Size = new System.Drawing.Size(672, 243);
            this.emptySpaceItem1.Spacing = new DevExpress.XtraLayout.Utils.Padding(0, 0, 0, 0);
            this.emptySpaceItem1.Text = "emptySpaceItem1";
            this.emptySpaceItem1.TextSize = new System.Drawing.Size(0, 0);
            // 
            // layoutControlGroup43
            // 
            this.layoutControlGroup43.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlGroup44,
            this.layoutControlGroup45});
            this.layoutControlGroup43.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroup43.Name = "layoutControlGroup43";
            this.layoutControlGroup43.Padding = new DevExpress.XtraLayout.Utils.Padding(0, 0, 0, 0);
            this.layoutControlGroup43.Size = new System.Drawing.Size(672, 104);
            this.layoutControlGroup43.Spacing = new DevExpress.XtraLayout.Utils.Padding(2, 2, 2, 2);
            this.layoutControlGroup43.Text = ":: Par�metros para gera��o do arquivo de remessa ::";
            this.layoutControlGroup43.TextVisible = true;
            // 
            // layoutControlGroup44
            // 
            this.layoutControlGroup44.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItem95,
            this.layoutControlItem96,
            this.emptySpaceItem2});
            this.layoutControlGroup44.Location = new System.Drawing.Point(283, 0);
            this.layoutControlGroup44.Name = "layoutControlGroup44";
            this.layoutControlGroup44.Padding = new DevExpress.XtraLayout.Utils.Padding(0, 0, 0, 0);
            this.layoutControlGroup44.Size = new System.Drawing.Size(383, 80);
            this.layoutControlGroup44.Spacing = new DevExpress.XtraLayout.Utils.Padding(2, 2, 2, 2);
            this.layoutControlGroup44.Text = ":: Banco do Bradesco ::";
            this.layoutControlGroup44.TextVisible = true;
            // 
            // layoutControlItem95
            // 
            this.layoutControlItem95.Control = this.textEdit7;
            this.layoutControlItem95.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItem95.Name = "layoutControlItem95";
            this.layoutControlItem95.Padding = new DevExpress.XtraLayout.Utils.Padding(5, 5, 5, 5);
            this.layoutControlItem95.Size = new System.Drawing.Size(139, 56);
            this.layoutControlItem95.Spacing = new DevExpress.XtraLayout.Utils.Padding(0, 0, 0, 0);
            this.layoutControlItem95.Text = "Cr�dito em Conta";
            this.layoutControlItem95.TextLocation = DevExpress.Utils.Locations.Top;
            this.layoutControlItem95.TextSize = new System.Drawing.Size(99, 20);
            this.layoutControlItem95.TextVisible = true;
            // 
            // layoutControlItem96
            // 
            this.layoutControlItem96.Control = this.textEdit12;
            this.layoutControlItem96.Location = new System.Drawing.Point(139, 0);
            this.layoutControlItem96.Name = "layoutControlItem96";
            this.layoutControlItem96.Padding = new DevExpress.XtraLayout.Utils.Padding(5, 5, 5, 5);
            this.layoutControlItem96.Size = new System.Drawing.Size(138, 56);
            this.layoutControlItem96.Spacing = new DevExpress.XtraLayout.Utils.Padding(0, 0, 0, 0);
            this.layoutControlItem96.Text = "Cart�o Sal�rio";
            this.layoutControlItem96.TextLocation = DevExpress.Utils.Locations.Top;
            this.layoutControlItem96.TextSize = new System.Drawing.Size(99, 20);
            this.layoutControlItem96.TextVisible = true;
            // 
            // emptySpaceItem2
            // 
            this.emptySpaceItem2.Location = new System.Drawing.Point(277, 0);
            this.emptySpaceItem2.Name = "emptySpaceItem2";
            this.emptySpaceItem2.Padding = new DevExpress.XtraLayout.Utils.Padding(5, 5, 5, 5);
            this.emptySpaceItem2.Size = new System.Drawing.Size(100, 56);
            this.emptySpaceItem2.Spacing = new DevExpress.XtraLayout.Utils.Padding(0, 0, 0, 0);
            this.emptySpaceItem2.Text = "emptySpaceItem2";
            this.emptySpaceItem2.TextSize = new System.Drawing.Size(0, 0);
            // 
            // layoutControlGroup45
            // 
            this.layoutControlGroup45.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItem94,
            this.layoutControlItem97});
            this.layoutControlGroup45.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroup45.Name = "layoutControlGroup45";
            this.layoutControlGroup45.Padding = new DevExpress.XtraLayout.Utils.Padding(0, 0, 0, 0);
            this.layoutControlGroup45.Size = new System.Drawing.Size(283, 80);
            this.layoutControlGroup45.Spacing = new DevExpress.XtraLayout.Utils.Padding(2, 2, 2, 2);
            this.layoutControlGroup45.Text = ":: Datamace ::";
            this.layoutControlGroup45.TextVisible = true;
            // 
            // layoutControlItem94
            // 
            this.layoutControlItem94.Control = this.txtCodigoFolha1;
            this.layoutControlItem94.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItem94.Name = "layoutControlItem94";
            this.layoutControlItem94.Padding = new DevExpress.XtraLayout.Utils.Padding(5, 5, 5, 5);
            this.layoutControlItem94.Size = new System.Drawing.Size(138, 56);
            this.layoutControlItem94.Spacing = new DevExpress.XtraLayout.Utils.Padding(0, 0, 0, 0);
            this.layoutControlItem94.Text = "C�digo com Registro";
            this.layoutControlItem94.TextLocation = DevExpress.Utils.Locations.Top;
            this.layoutControlItem94.TextSize = new System.Drawing.Size(99, 20);
            this.layoutControlItem94.TextVisible = true;
            // 
            // layoutControlItem97
            // 
            this.layoutControlItem97.Control = this.txtCodigoFolha2;
            this.layoutControlItem97.Location = new System.Drawing.Point(138, 0);
            this.layoutControlItem97.Name = "layoutControlItem97";
            this.layoutControlItem97.Padding = new DevExpress.XtraLayout.Utils.Padding(5, 5, 5, 5);
            this.layoutControlItem97.Size = new System.Drawing.Size(139, 56);
            this.layoutControlItem97.Spacing = new DevExpress.XtraLayout.Utils.Padding(0, 0, 0, 0);
            this.layoutControlItem97.Text = "C�digo sem Registro";
            this.layoutControlItem97.TextLocation = DevExpress.Utils.Locations.Top;
            this.layoutControlItem97.TextSize = new System.Drawing.Size(99, 20);
            this.layoutControlItem97.TextVisible = true;
            // 
            // lcgBasico
            // 
            this.lcgBasico.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlGroup2,
            this.layoutControlGroup4,
            this.layoutControlGroup3});
            this.lcgBasico.Location = new System.Drawing.Point(0, 0);
            this.lcgBasico.Name = "lcgBasico";
            this.lcgBasico.Padding = new DevExpress.XtraLayout.Utils.Padding(0, 0, 0, 0);
            this.lcgBasico.Size = new System.Drawing.Size(672, 347);
            this.lcgBasico.Spacing = new DevExpress.XtraLayout.Utils.Padding(2, 2, 2, 2);
            this.lcgBasico.Text = "&1-B�sico";
            this.lcgBasico.TextVisible = true;
            // 
            // layoutControlGroup2
            // 
            this.layoutControlGroup2.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItem3,
            this.layoutControlItem6,
            this.layoutControlItem4,
            this.layoutControlItem8,
            this.layoutControlItem7});
            this.layoutControlGroup2.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroup2.Name = "layoutControlGroup2";
            this.layoutControlGroup2.Padding = new DevExpress.XtraLayout.Utils.Padding(0, 0, 0, 0);
            this.layoutControlGroup2.Size = new System.Drawing.Size(672, 86);
            this.layoutControlGroup2.Spacing = new DevExpress.XtraLayout.Utils.Padding(2, 2, 2, 2);
            this.layoutControlGroup2.Text = ":: Endere�o ::";
            this.layoutControlGroup2.TextVisible = true;
            // 
            // layoutControlItem3
            // 
            this.layoutControlItem3.Control = this.textEdit3;
            this.layoutControlItem3.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItem3.MaxSize = new System.Drawing.Size(0, 30);
            this.layoutControlItem3.MinSize = new System.Drawing.Size(85, 30);
            this.layoutControlItem3.Name = "layoutControlItem3";
            this.layoutControlItem3.Padding = new DevExpress.XtraLayout.Utils.Padding(5, 5, 5, 5);
            this.layoutControlItem3.Size = new System.Drawing.Size(174, 31);
            this.layoutControlItem3.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem3.Spacing = new DevExpress.XtraLayout.Utils.Padding(0, 0, 0, 0);
            this.layoutControlItem3.Text = "CEP";
            this.layoutControlItem3.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem3.TextSize = new System.Drawing.Size(55, 20);
            this.layoutControlItem3.TextToControlDistance = 0;
            this.layoutControlItem3.TextVisible = true;
            // 
            // layoutControlItem6
            // 
            this.layoutControlItem6.Control = this.textEdit5;
            this.layoutControlItem6.Location = new System.Drawing.Point(0, 31);
            this.layoutControlItem6.MaxSize = new System.Drawing.Size(0, 30);
            this.layoutControlItem6.MinSize = new System.Drawing.Size(85, 30);
            this.layoutControlItem6.Name = "layoutControlItem6";
            this.layoutControlItem6.Padding = new DevExpress.XtraLayout.Utils.Padding(5, 5, 5, 5);
            this.layoutControlItem6.Size = new System.Drawing.Size(254, 31);
            this.layoutControlItem6.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem6.Spacing = new DevExpress.XtraLayout.Utils.Padding(0, 0, 0, 0);
            this.layoutControlItem6.Text = "Bairro";
            this.layoutControlItem6.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem6.TextSize = new System.Drawing.Size(55, 20);
            this.layoutControlItem6.TextToControlDistance = 0;
            this.layoutControlItem6.TextVisible = true;
            // 
            // layoutControlItem4
            // 
            this.layoutControlItem4.Control = this.textEdit4;
            this.layoutControlItem4.Location = new System.Drawing.Point(174, 0);
            this.layoutControlItem4.Name = "layoutControlItem4";
            this.layoutControlItem4.Padding = new DevExpress.XtraLayout.Utils.Padding(5, 5, 5, 5);
            this.layoutControlItem4.Size = new System.Drawing.Size(492, 31);
            this.layoutControlItem4.Spacing = new DevExpress.XtraLayout.Utils.Padding(0, 0, 0, 0);
            this.layoutControlItem4.Text = "Endere�o";
            this.layoutControlItem4.TextSize = new System.Drawing.Size(99, 20);
            this.layoutControlItem4.TextVisible = true;
            // 
            // layoutControlItem8
            // 
            this.layoutControlItem8.Control = this.lkpCidades;
            this.layoutControlItem8.Location = new System.Drawing.Point(254, 31);
            this.layoutControlItem8.Name = "layoutControlItem8";
            this.layoutControlItem8.Padding = new DevExpress.XtraLayout.Utils.Padding(5, 5, 5, 5);
            this.layoutControlItem8.Size = new System.Drawing.Size(309, 31);
            this.layoutControlItem8.Spacing = new DevExpress.XtraLayout.Utils.Padding(0, 0, 0, 0);
            this.layoutControlItem8.Text = "Cidade";
            this.layoutControlItem8.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem8.TextSize = new System.Drawing.Size(35, 20);
            this.layoutControlItem8.TextVisible = true;
            // 
            // layoutControlItem7
            // 
            this.layoutControlItem7.Control = this.lkpUF;
            this.layoutControlItem7.Location = new System.Drawing.Point(563, 31);
            this.layoutControlItem7.MinSize = new System.Drawing.Size(50, 30);
            this.layoutControlItem7.Name = "layoutControlItem7";
            this.layoutControlItem7.Padding = new DevExpress.XtraLayout.Utils.Padding(5, 5, 5, 5);
            this.layoutControlItem7.Size = new System.Drawing.Size(103, 31);
            this.layoutControlItem7.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem7.Spacing = new DevExpress.XtraLayout.Utils.Padding(0, 0, 0, 0);
            this.layoutControlItem7.Text = "UF";
            this.layoutControlItem7.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem7.TextSize = new System.Drawing.Size(15, 20);
            this.layoutControlItem7.TextVisible = true;
            // 
            // layoutControlGroup4
            // 
            this.layoutControlGroup4.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItem88,
            this.layoutControlItem90,
            this.layoutControlGroup27,
            this.layoutControlGroup41,
            this.layoutControlItem89,
            this.layoutControlItem87});
            this.layoutControlGroup4.Location = new System.Drawing.Point(0, 181);
            this.layoutControlGroup4.Name = "layoutControlGroup4";
            this.layoutControlGroup4.Padding = new DevExpress.XtraLayout.Utils.Padding(0, 0, 0, 0);
            this.layoutControlGroup4.Size = new System.Drawing.Size(672, 166);
            this.layoutControlGroup4.Spacing = new DevExpress.XtraLayout.Utils.Padding(2, 2, 3, 2);
            this.layoutControlGroup4.Text = ":: Dados de Consulta ::";
            this.layoutControlGroup4.TextVisible = true;
            // 
            // layoutControlItem88
            // 
            this.layoutControlItem88.Control = this.DataAdesao_C;
            this.layoutControlItem88.Location = new System.Drawing.Point(201, 110);
            this.layoutControlItem88.MaxSize = new System.Drawing.Size(0, 30);
            this.layoutControlItem88.MinSize = new System.Drawing.Size(75, 30);
            this.layoutControlItem88.Name = "layoutControlItem88";
            this.layoutControlItem88.Padding = new DevExpress.XtraLayout.Utils.Padding(5, 5, 5, 5);
            this.layoutControlItem88.Size = new System.Drawing.Size(189, 31);
            this.layoutControlItem88.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem88.Spacing = new DevExpress.XtraLayout.Utils.Padding(0, 0, 0, 0);
            this.layoutControlItem88.Text = "Data Ades�o";
            this.layoutControlItem88.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem88.TextSize = new System.Drawing.Size(65, 20);
            this.layoutControlItem88.TextVisible = true;
            // 
            // layoutControlItem90
            // 
            this.layoutControlItem90.Control = this.chkGas_C;
            this.layoutControlItem90.Location = new System.Drawing.Point(546, 110);
            this.layoutControlItem90.Name = "layoutControlItem90";
            this.layoutControlItem90.Padding = new DevExpress.XtraLayout.Utils.Padding(5, 5, 5, 5);
            this.layoutControlItem90.Size = new System.Drawing.Size(120, 31);
            this.layoutControlItem90.Spacing = new DevExpress.XtraLayout.Utils.Padding(0, 0, 0, 0);
            this.layoutControlItem90.Text = "layoutControlItem90";
            this.layoutControlItem90.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem90.TextToControlDistance = 0;
            this.layoutControlItem90.TextVisible = false;
            // 
            // layoutControlGroup27
            // 
            this.layoutControlGroup27.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItem83,
            this.layoutControlItem82});
            this.layoutControlGroup27.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroup27.Name = "layoutControlGroup27";
            this.layoutControlGroup27.Padding = new DevExpress.XtraLayout.Utils.Padding(0, 0, 0, 0);
            this.layoutControlGroup27.Size = new System.Drawing.Size(666, 55);
            this.layoutControlGroup27.Spacing = new DevExpress.XtraLayout.Utils.Padding(2, 2, 2, 2);
            this.layoutControlGroup27.Text = ":: S�ndico ::";
            this.layoutControlGroup27.TextVisible = true;
            // 
            // layoutControlItem83
            // 
            this.layoutControlItem83.Control = this.txtSindico_C;
            this.layoutControlItem83.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItem83.Name = "layoutControlItem83";
            this.layoutControlItem83.Padding = new DevExpress.XtraLayout.Utils.Padding(5, 5, 5, 5);
            this.layoutControlItem83.Size = new System.Drawing.Size(459, 31);
            this.layoutControlItem83.Spacing = new DevExpress.XtraLayout.Utils.Padding(0, 0, 0, 0);
            this.layoutControlItem83.Text = "Nome";
            this.layoutControlItem83.TextSize = new System.Drawing.Size(99, 20);
            this.layoutControlItem83.TextVisible = true;
            // 
            // layoutControlItem82
            // 
            this.layoutControlItem82.Control = this.txtTelefone_C;
            this.layoutControlItem82.Location = new System.Drawing.Point(459, 0);
            this.layoutControlItem82.Name = "layoutControlItem82";
            this.layoutControlItem82.Padding = new DevExpress.XtraLayout.Utils.Padding(5, 5, 5, 5);
            this.layoutControlItem82.Size = new System.Drawing.Size(201, 31);
            this.layoutControlItem82.Spacing = new DevExpress.XtraLayout.Utils.Padding(0, 0, 0, 0);
            this.layoutControlItem82.Text = "Telefone";
            this.layoutControlItem82.TextSize = new System.Drawing.Size(99, 20);
            this.layoutControlItem82.TextVisible = true;
            // 
            // layoutControlGroup41
            // 
            this.layoutControlGroup41.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItem85,
            this.layoutControlItem86,
            this.layoutControlItem91,
            this.layoutControlItem84,
            this.layoutControlItem92});
            this.layoutControlGroup41.Location = new System.Drawing.Point(0, 55);
            this.layoutControlGroup41.Name = "layoutControlGroup41";
            this.layoutControlGroup41.Padding = new DevExpress.XtraLayout.Utils.Padding(0, 0, 0, 0);
            this.layoutControlGroup41.Size = new System.Drawing.Size(666, 55);
            this.layoutControlGroup41.Spacing = new DevExpress.XtraLayout.Utils.Padding(2, 2, 2, 2);
            this.layoutControlGroup41.Text = ":: Financeiro ::";
            this.layoutControlGroup41.TextVisible = true;
            // 
            // layoutControlItem85
            // 
            this.layoutControlItem85.Control = this.txtAgencia_C;
            this.layoutControlItem85.Location = new System.Drawing.Point(277, 0);
            this.layoutControlItem85.Name = "layoutControlItem85";
            this.layoutControlItem85.Padding = new DevExpress.XtraLayout.Utils.Padding(5, 5, 5, 5);
            this.layoutControlItem85.Size = new System.Drawing.Size(165, 31);
            this.layoutControlItem85.Spacing = new DevExpress.XtraLayout.Utils.Padding(0, 0, 0, 0);
            this.layoutControlItem85.Text = "Ag�ncia";
            this.layoutControlItem85.TextSize = new System.Drawing.Size(99, 20);
            this.layoutControlItem85.TextVisible = true;
            // 
            // layoutControlItem86
            // 
            this.layoutControlItem86.Control = this.txtConta_C;
            this.layoutControlItem86.Location = new System.Drawing.Point(503, 0);
            this.layoutControlItem86.Name = "layoutControlItem86";
            this.layoutControlItem86.Padding = new DevExpress.XtraLayout.Utils.Padding(5, 5, 5, 5);
            this.layoutControlItem86.Size = new System.Drawing.Size(96, 31);
            this.layoutControlItem86.Spacing = new DevExpress.XtraLayout.Utils.Padding(0, 0, 0, 0);
            this.layoutControlItem86.Text = "Conta";
            this.layoutControlItem86.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem86.TextSize = new System.Drawing.Size(30, 20);
            this.layoutControlItem86.TextVisible = true;
            // 
            // layoutControlItem91
            // 
            this.layoutControlItem91.Control = this.lookUpEdit1;
            this.layoutControlItem91.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItem91.Name = "layoutControlItem91";
            this.layoutControlItem91.Padding = new DevExpress.XtraLayout.Utils.Padding(5, 5, 5, 5);
            this.layoutControlItem91.Size = new System.Drawing.Size(277, 31);
            this.layoutControlItem91.Spacing = new DevExpress.XtraLayout.Utils.Padding(0, 0, 0, 0);
            this.layoutControlItem91.Text = "Banco";
            this.layoutControlItem91.TextSize = new System.Drawing.Size(99, 20);
            this.layoutControlItem91.TextVisible = true;
            // 
            // layoutControlItem84
            // 
            this.layoutControlItem84.Control = this.textEdit13;
            this.layoutControlItem84.Location = new System.Drawing.Point(442, 0);
            this.layoutControlItem84.Name = "layoutControlItem84";
            this.layoutControlItem84.Padding = new DevExpress.XtraLayout.Utils.Padding(5, 5, 5, 5);
            this.layoutControlItem84.Size = new System.Drawing.Size(61, 31);
            this.layoutControlItem84.Spacing = new DevExpress.XtraLayout.Utils.Padding(0, 0, 0, 0);
            this.layoutControlItem84.Text = "layoutControlItem84";
            this.layoutControlItem84.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem84.TextToControlDistance = 0;
            this.layoutControlItem84.TextVisible = false;
            // 
            // layoutControlItem92
            // 
            this.layoutControlItem92.Control = this.textEdit14;
            this.layoutControlItem92.Location = new System.Drawing.Point(599, 0);
            this.layoutControlItem92.Name = "layoutControlItem92";
            this.layoutControlItem92.Padding = new DevExpress.XtraLayout.Utils.Padding(5, 5, 5, 5);
            this.layoutControlItem92.Size = new System.Drawing.Size(61, 31);
            this.layoutControlItem92.Spacing = new DevExpress.XtraLayout.Utils.Padding(0, 0, 0, 0);
            this.layoutControlItem92.Text = "layoutControlItem92";
            this.layoutControlItem92.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem92.TextToControlDistance = 0;
            this.layoutControlItem92.TextVisible = false;
            // 
            // layoutControlItem89
            // 
            this.layoutControlItem89.Control = this.txtCNPJ_C;
            this.layoutControlItem89.Location = new System.Drawing.Point(0, 110);
            this.layoutControlItem89.Name = "layoutControlItem89";
            this.layoutControlItem89.Padding = new DevExpress.XtraLayout.Utils.Padding(5, 5, 5, 5);
            this.layoutControlItem89.Size = new System.Drawing.Size(201, 31);
            this.layoutControlItem89.Spacing = new DevExpress.XtraLayout.Utils.Padding(0, 0, 0, 0);
            this.layoutControlItem89.Text = "CNPJ";
            this.layoutControlItem89.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem89.TextSize = new System.Drawing.Size(55, 20);
            this.layoutControlItem89.TextVisible = true;
            // 
            // layoutControlItem87
            // 
            this.layoutControlItem87.Control = this.txtTotalUnidades_C;
            this.layoutControlItem87.Location = new System.Drawing.Point(390, 110);
            this.layoutControlItem87.Name = "layoutControlItem87";
            this.layoutControlItem87.Padding = new DevExpress.XtraLayout.Utils.Padding(5, 5, 5, 5);
            this.layoutControlItem87.Size = new System.Drawing.Size(156, 31);
            this.layoutControlItem87.Spacing = new DevExpress.XtraLayout.Utils.Padding(0, 0, 0, 0);
            this.layoutControlItem87.Text = "Total Unidades";
            this.layoutControlItem87.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem87.TextSize = new System.Drawing.Size(75, 20);
            this.layoutControlItem87.TextVisible = true;
            // 
            // layoutControlGroup3
            // 
            this.layoutControlGroup3.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItem5});
            this.layoutControlGroup3.Location = new System.Drawing.Point(0, 86);
            this.layoutControlGroup3.Name = "layoutControlGroup3";
            this.layoutControlGroup3.Padding = new DevExpress.XtraLayout.Utils.Padding(0, 0, 0, 0);
            this.layoutControlGroup3.Size = new System.Drawing.Size(672, 95);
            this.layoutControlGroup3.Spacing = new DevExpress.XtraLayout.Utils.Padding(2, 2, 3, 2);
            this.layoutControlGroup3.Text = ":: Observa��o ::";
            this.layoutControlGroup3.TextVisible = true;
            // 
            // layoutControlItem5
            // 
            this.layoutControlItem5.Control = this.memoEdit1;
            this.layoutControlItem5.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItem5.Name = "layoutControlItem5";
            this.layoutControlItem5.Padding = new DevExpress.XtraLayout.Utils.Padding(5, 5, 5, 5);
            this.layoutControlItem5.Size = new System.Drawing.Size(666, 70);
            this.layoutControlItem5.Spacing = new DevExpress.XtraLayout.Utils.Padding(0, 0, 0, 0);
            this.layoutControlItem5.Text = "layoutControlItem5";
            this.layoutControlItem5.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem5.TextToControlDistance = 0;
            this.layoutControlItem5.TextVisible = false;
            // 
            // layoutControlGroup5
            // 
            this.layoutControlGroup5.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItem11,
            this.layoutControlItem10,
            this.layoutControlItem12,
            this.layoutControlGroup10,
            this.layoutControlGroup11,
            this.layoutControlGroup12});
            this.layoutControlGroup5.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroup5.Name = "layoutControlGroup5";
            this.layoutControlGroup5.Padding = new DevExpress.XtraLayout.Utils.Padding(0, 0, 0, 0);
            this.layoutControlGroup5.Size = new System.Drawing.Size(672, 347);
            this.layoutControlGroup5.Spacing = new DevExpress.XtraLayout.Utils.Padding(2, 2, 2, 2);
            this.layoutControlGroup5.Text = "&2-Constitui��o";
            this.layoutControlGroup5.TextVisible = true;
            // 
            // layoutControlItem11
            // 
            this.layoutControlItem11.Control = this.checkEdit1;
            this.layoutControlItem11.Location = new System.Drawing.Point(274, 0);
            this.layoutControlItem11.Name = "layoutControlItem11";
            this.layoutControlItem11.Padding = new DevExpress.XtraLayout.Utils.Padding(5, 5, 5, 5);
            this.layoutControlItem11.Size = new System.Drawing.Size(246, 31);
            this.layoutControlItem11.Spacing = new DevExpress.XtraLayout.Utils.Padding(0, 0, 0, 0);
            this.layoutControlItem11.Text = "layoutControlItem11";
            this.layoutControlItem11.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem11.TextToControlDistance = 0;
            this.layoutControlItem11.TextVisible = false;
            // 
            // layoutControlItem10
            // 
            this.layoutControlItem10.Control = this.textEdit1;
            this.layoutControlItem10.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItem10.Name = "layoutControlItem10";
            this.layoutControlItem10.Padding = new DevExpress.XtraLayout.Utils.Padding(5, 5, 5, 5);
            this.layoutControlItem10.Size = new System.Drawing.Size(274, 31);
            this.layoutControlItem10.Spacing = new DevExpress.XtraLayout.Utils.Padding(0, 0, 0, 0);
            this.layoutControlItem10.Text = "CNPJ";
            this.layoutControlItem10.TextSize = new System.Drawing.Size(99, 20);
            this.layoutControlItem10.TextVisible = true;
            // 
            // layoutControlItem12
            // 
            this.layoutControlItem12.Control = this.checkEdit2;
            this.layoutControlItem12.Location = new System.Drawing.Point(520, 0);
            this.layoutControlItem12.Name = "layoutControlItem12";
            this.layoutControlItem12.Padding = new DevExpress.XtraLayout.Utils.Padding(5, 5, 5, 5);
            this.layoutControlItem12.Size = new System.Drawing.Size(152, 31);
            this.layoutControlItem12.Spacing = new DevExpress.XtraLayout.Utils.Padding(0, 0, 0, 0);
            this.layoutControlItem12.Text = "layoutControlItem12";
            this.layoutControlItem12.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem12.TextToControlDistance = 0;
            this.layoutControlItem12.TextVisible = false;
            // 
            // layoutControlGroup10
            // 
            this.layoutControlGroup10.GroupBordersVisible = false;
            this.layoutControlGroup10.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlGroup6,
            this.layoutControlGroup9,
            this.layoutControlGroup7,
            this.layoutControlGroup8});
            this.layoutControlGroup10.Location = new System.Drawing.Point(0, 31);
            this.layoutControlGroup10.Name = "layoutControlGroup10";
            this.layoutControlGroup10.Padding = new DevExpress.XtraLayout.Utils.Padding(0, 0, 0, 0);
            this.layoutControlGroup10.Size = new System.Drawing.Size(672, 84);
            this.layoutControlGroup10.Spacing = new DevExpress.XtraLayout.Utils.Padding(0, 0, 0, 0);
            this.layoutControlGroup10.Text = ":: Par�metros ::";
            this.layoutControlGroup10.TextVisible = false;
            // 
            // layoutControlGroup6
            // 
            this.layoutControlGroup6.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItem13,
            this.layoutControlItem14});
            this.layoutControlGroup6.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroup6.Name = "layoutControlGroup6";
            this.layoutControlGroup6.Padding = new DevExpress.XtraLayout.Utils.Padding(0, 0, 0, 0);
            this.layoutControlGroup6.Size = new System.Drawing.Size(150, 84);
            this.layoutControlGroup6.Spacing = new DevExpress.XtraLayout.Utils.Padding(2, 2, 2, 2);
            this.layoutControlGroup6.Text = ":: % Rateio ::";
            this.layoutControlGroup6.TextVisible = true;
            // 
            // layoutControlItem13
            // 
            this.layoutControlItem13.Control = this.calcEdit1;
            this.layoutControlItem13.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItem13.MaxSize = new System.Drawing.Size(144, 30);
            this.layoutControlItem13.MinSize = new System.Drawing.Size(144, 30);
            this.layoutControlItem13.Name = "layoutControlItem13";
            this.layoutControlItem13.Padding = new DevExpress.XtraLayout.Utils.Padding(5, 5, 5, 5);
            this.layoutControlItem13.Size = new System.Drawing.Size(144, 30);
            this.layoutControlItem13.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem13.Spacing = new DevExpress.XtraLayout.Utils.Padding(0, 0, 0, 0);
            this.layoutControlItem13.Text = "S�ndico";
            this.layoutControlItem13.TextSize = new System.Drawing.Size(99, 20);
            this.layoutControlItem13.TextVisible = true;
            // 
            // layoutControlItem14
            // 
            this.layoutControlItem14.Control = this.calcEdit2;
            this.layoutControlItem14.Location = new System.Drawing.Point(0, 30);
            this.layoutControlItem14.MaxSize = new System.Drawing.Size(144, 30);
            this.layoutControlItem14.MinSize = new System.Drawing.Size(144, 30);
            this.layoutControlItem14.Name = "layoutControlItem14";
            this.layoutControlItem14.Padding = new DevExpress.XtraLayout.Utils.Padding(5, 5, 5, 5);
            this.layoutControlItem14.Size = new System.Drawing.Size(144, 30);
            this.layoutControlItem14.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem14.Spacing = new DevExpress.XtraLayout.Utils.Padding(0, 0, 0, 0);
            this.layoutControlItem14.Text = "Sub-S�ndico";
            this.layoutControlItem14.TextSize = new System.Drawing.Size(99, 20);
            this.layoutControlItem14.TextVisible = true;
            // 
            // layoutControlGroup9
            // 
            this.layoutControlGroup9.AllowCustomizeChildren = false;
            this.layoutControlGroup9.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItem20,
            this.layoutControlItem19});
            this.layoutControlGroup9.Location = new System.Drawing.Point(438, 0);
            this.layoutControlGroup9.Name = "layoutControlGroup9";
            this.layoutControlGroup9.Padding = new DevExpress.XtraLayout.Utils.Padding(0, 0, 0, 0);
            this.layoutControlGroup9.Size = new System.Drawing.Size(234, 84);
            this.layoutControlGroup9.Spacing = new DevExpress.XtraLayout.Utils.Padding(2, 2, 2, 2);
            this.layoutControlGroup9.Text = ":: Constru��o e Finalidade ::";
            this.layoutControlGroup9.TextVisible = true;
            // 
            // layoutControlItem20
            // 
            this.layoutControlItem20.Control = this.radioGroup2;
            this.layoutControlItem20.Location = new System.Drawing.Point(118, 0);
            this.layoutControlItem20.Name = "layoutControlItem20";
            this.layoutControlItem20.Padding = new DevExpress.XtraLayout.Utils.Padding(0, 0, 0, 0);
            this.layoutControlItem20.Size = new System.Drawing.Size(110, 60);
            this.layoutControlItem20.Spacing = new DevExpress.XtraLayout.Utils.Padding(0, 0, 0, 0);
            this.layoutControlItem20.Text = "layoutControlItem20";
            this.layoutControlItem20.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem20.TextToControlDistance = 0;
            this.layoutControlItem20.TextVisible = false;
            // 
            // layoutControlItem19
            // 
            this.layoutControlItem19.Control = this.radioGroup1;
            this.layoutControlItem19.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItem19.Name = "layoutControlItem19";
            this.layoutControlItem19.Padding = new DevExpress.XtraLayout.Utils.Padding(0, 0, 0, 0);
            this.layoutControlItem19.Size = new System.Drawing.Size(118, 60);
            this.layoutControlItem19.Spacing = new DevExpress.XtraLayout.Utils.Padding(0, 0, 0, 0);
            this.layoutControlItem19.Text = "layoutControlItem19";
            this.layoutControlItem19.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem19.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem19.TextToControlDistance = 0;
            this.layoutControlItem19.TextVisible = false;
            // 
            // layoutControlGroup7
            // 
            this.layoutControlGroup7.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItem16,
            this.layoutControlItem15});
            this.layoutControlGroup7.Location = new System.Drawing.Point(150, 0);
            this.layoutControlGroup7.Name = "layoutControlGroup7";
            this.layoutControlGroup7.Padding = new DevExpress.XtraLayout.Utils.Padding(0, 0, 0, 0);
            this.layoutControlGroup7.Size = new System.Drawing.Size(145, 84);
            this.layoutControlGroup7.Spacing = new DevExpress.XtraLayout.Utils.Padding(2, 2, 2, 2);
            this.layoutControlGroup7.Text = ":: % Condom�nio ::";
            this.layoutControlGroup7.TextVisible = true;
            // 
            // layoutControlItem16
            // 
            this.layoutControlItem16.Control = this.calcEdit4;
            this.layoutControlItem16.Location = new System.Drawing.Point(0, 30);
            this.layoutControlItem16.MaxSize = new System.Drawing.Size(139, 30);
            this.layoutControlItem16.MinSize = new System.Drawing.Size(139, 30);
            this.layoutControlItem16.Name = "layoutControlItem16";
            this.layoutControlItem16.Padding = new DevExpress.XtraLayout.Utils.Padding(5, 5, 5, 5);
            this.layoutControlItem16.Size = new System.Drawing.Size(139, 30);
            this.layoutControlItem16.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem16.Spacing = new DevExpress.XtraLayout.Utils.Padding(0, 0, 0, 0);
            this.layoutControlItem16.Text = "Sub-S�ndico";
            this.layoutControlItem16.TextSize = new System.Drawing.Size(99, 20);
            this.layoutControlItem16.TextVisible = true;
            // 
            // layoutControlItem15
            // 
            this.layoutControlItem15.Control = this.calcEdit3;
            this.layoutControlItem15.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItem15.MaxSize = new System.Drawing.Size(139, 30);
            this.layoutControlItem15.MinSize = new System.Drawing.Size(139, 30);
            this.layoutControlItem15.Name = "layoutControlItem15";
            this.layoutControlItem15.Padding = new DevExpress.XtraLayout.Utils.Padding(5, 5, 5, 5);
            this.layoutControlItem15.Size = new System.Drawing.Size(139, 30);
            this.layoutControlItem15.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem15.Spacing = new DevExpress.XtraLayout.Utils.Padding(0, 0, 0, 0);
            this.layoutControlItem15.Text = "S�ndico";
            this.layoutControlItem15.TextSize = new System.Drawing.Size(99, 20);
            this.layoutControlItem15.TextVisible = true;
            // 
            // layoutControlGroup8
            // 
            this.layoutControlGroup8.AllowCustomizeChildren = false;
            this.layoutControlGroup8.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItem17,
            this.layoutControlItem18});
            this.layoutControlGroup8.Location = new System.Drawing.Point(295, 0);
            this.layoutControlGroup8.Name = "layoutControlGroup8";
            this.layoutControlGroup8.Padding = new DevExpress.XtraLayout.Utils.Padding(0, 0, 0, 0);
            this.layoutControlGroup8.Size = new System.Drawing.Size(143, 84);
            this.layoutControlGroup8.Spacing = new DevExpress.XtraLayout.Utils.Padding(2, 2, 2, 2);
            this.layoutControlGroup8.Text = ":: Fundo Reserva ::";
            this.layoutControlGroup8.TextVisible = true;
            // 
            // layoutControlItem17
            // 
            this.layoutControlItem17.Control = this.checkEdit3;
            this.layoutControlItem17.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItem17.Name = "layoutControlItem17";
            this.layoutControlItem17.Padding = new DevExpress.XtraLayout.Utils.Padding(5, 5, 5, 5);
            this.layoutControlItem17.Size = new System.Drawing.Size(137, 30);
            this.layoutControlItem17.Spacing = new DevExpress.XtraLayout.Utils.Padding(0, 0, 0, 0);
            this.layoutControlItem17.Text = "layoutControlItem17";
            this.layoutControlItem17.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem17.TextToControlDistance = 0;
            this.layoutControlItem17.TextVisible = false;
            // 
            // layoutControlItem18
            // 
            this.layoutControlItem18.Control = this.checkEdit4;
            this.layoutControlItem18.Location = new System.Drawing.Point(0, 30);
            this.layoutControlItem18.Name = "layoutControlItem18";
            this.layoutControlItem18.Padding = new DevExpress.XtraLayout.Utils.Padding(5, 5, 5, 5);
            this.layoutControlItem18.Size = new System.Drawing.Size(137, 30);
            this.layoutControlItem18.Spacing = new DevExpress.XtraLayout.Utils.Padding(0, 0, 0, 0);
            this.layoutControlItem18.Text = "layoutControlItem18";
            this.layoutControlItem18.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem18.TextToControlDistance = 0;
            this.layoutControlItem18.TextVisible = false;
            // 
            // layoutControlGroup11
            // 
            this.layoutControlGroup11.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItem22,
            this.layoutControlItem25,
            this.layoutControlItem9});
            this.layoutControlGroup11.Location = new System.Drawing.Point(0, 115);
            this.layoutControlGroup11.Name = "layoutControlGroup11";
            this.layoutControlGroup11.Padding = new DevExpress.XtraLayout.Utils.Padding(0, 0, 0, 0);
            this.layoutControlGroup11.Size = new System.Drawing.Size(387, 232);
            this.layoutControlGroup11.Spacing = new DevExpress.XtraLayout.Utils.Padding(2, 2, 2, 2);
            this.layoutControlGroup11.Text = ":: Blocos ::";
            this.layoutControlGroup11.TextVisible = true;
            // 
            // layoutControlItem22
            // 
            this.layoutControlItem22.Control = this.grdBlocos;
            this.layoutControlItem22.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItem22.Name = "layoutControlItem22";
            this.layoutControlItem22.Padding = new DevExpress.XtraLayout.Utils.Padding(0, 0, 0, 0);
            this.layoutControlItem22.Size = new System.Drawing.Size(381, 175);
            this.layoutControlItem22.Spacing = new DevExpress.XtraLayout.Utils.Padding(0, 0, 0, 0);
            this.layoutControlItem22.Text = "layoutControlItem22";
            this.layoutControlItem22.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem22.TextToControlDistance = 0;
            this.layoutControlItem22.TextVisible = false;
            // 
            // layoutControlItem25
            // 
            this.layoutControlItem25.Control = this.speBlocos;
            this.layoutControlItem25.Location = new System.Drawing.Point(219, 175);
            this.layoutControlItem25.Name = "layoutControlItem25";
            this.layoutControlItem25.Padding = new DevExpress.XtraLayout.Utils.Padding(5, 5, 5, 5);
            this.layoutControlItem25.Size = new System.Drawing.Size(162, 33);
            this.layoutControlItem25.Spacing = new DevExpress.XtraLayout.Utils.Padding(0, 0, 0, 0);
            this.layoutControlItem25.Text = "Gerar";
            this.layoutControlItem25.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem25.TextSize = new System.Drawing.Size(35, 20);
            this.layoutControlItem25.TextVisible = true;
            // 
            // layoutControlItem9
            // 
            this.layoutControlItem9.Control = this.bnvBlocos;
            this.layoutControlItem9.Location = new System.Drawing.Point(0, 175);
            this.layoutControlItem9.Name = "layoutControlItem9";
            this.layoutControlItem9.Padding = new DevExpress.XtraLayout.Utils.Padding(5, 5, 5, 5);
            this.layoutControlItem9.Size = new System.Drawing.Size(219, 33);
            this.layoutControlItem9.Spacing = new DevExpress.XtraLayout.Utils.Padding(0, 0, 0, 0);
            this.layoutControlItem9.Text = "layoutControlItem9";
            this.layoutControlItem9.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem9.TextToControlDistance = 0;
            this.layoutControlItem9.TextVisible = false;
            // 
            // layoutControlGroup12
            // 
            this.layoutControlGroup12.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItem21,
            this.layoutControlItem27,
            this.layoutControlItem23});
            this.layoutControlGroup12.Location = new System.Drawing.Point(387, 115);
            this.layoutControlGroup12.Name = "layoutControlGroup12";
            this.layoutControlGroup12.Padding = new DevExpress.XtraLayout.Utils.Padding(0, 0, 0, 0);
            this.layoutControlGroup12.Size = new System.Drawing.Size(285, 232);
            this.layoutControlGroup12.Spacing = new DevExpress.XtraLayout.Utils.Padding(2, 2, 2, 2);
            this.layoutControlGroup12.Text = ":: Apartamentos ::";
            this.layoutControlGroup12.TextVisible = true;
            // 
            // layoutControlItem21
            // 
            this.layoutControlItem21.Control = this.grdApartamentos;
            this.layoutControlItem21.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItem21.Name = "layoutControlItem21";
            this.layoutControlItem21.Padding = new DevExpress.XtraLayout.Utils.Padding(0, 0, 0, 0);
            this.layoutControlItem21.Size = new System.Drawing.Size(279, 176);
            this.layoutControlItem21.Spacing = new DevExpress.XtraLayout.Utils.Padding(0, 0, 0, 0);
            this.layoutControlItem21.Text = "layoutControlItem21";
            this.layoutControlItem21.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem21.TextToControlDistance = 0;
            this.layoutControlItem21.TextVisible = false;
            // 
            // layoutControlItem27
            // 
            this.layoutControlItem27.Control = this.speApartamentos;
            this.layoutControlItem27.Location = new System.Drawing.Point(169, 176);
            this.layoutControlItem27.MaxSize = new System.Drawing.Size(0, 32);
            this.layoutControlItem27.MinSize = new System.Drawing.Size(80, 32);
            this.layoutControlItem27.Name = "layoutControlItem27";
            this.layoutControlItem27.Padding = new DevExpress.XtraLayout.Utils.Padding(5, 5, 5, 5);
            this.layoutControlItem27.Size = new System.Drawing.Size(110, 32);
            this.layoutControlItem27.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem27.Spacing = new DevExpress.XtraLayout.Utils.Padding(0, 0, 0, 0);
            this.layoutControlItem27.Text = "Gerar";
            this.layoutControlItem27.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem27.TextSize = new System.Drawing.Size(35, 20);
            this.layoutControlItem27.TextVisible = true;
            // 
            // layoutControlItem23
            // 
            this.layoutControlItem23.Control = this.bnvApartamentos;
            this.layoutControlItem23.Location = new System.Drawing.Point(0, 176);
            this.layoutControlItem23.Name = "layoutControlItem23";
            this.layoutControlItem23.Padding = new DevExpress.XtraLayout.Utils.Padding(5, 5, 5, 5);
            this.layoutControlItem23.Size = new System.Drawing.Size(169, 32);
            this.layoutControlItem23.Spacing = new DevExpress.XtraLayout.Utils.Padding(0, 0, 0, 0);
            this.layoutControlItem23.Text = "layoutControlItem23";
            this.layoutControlItem23.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem23.TextToControlDistance = 0;
            this.layoutControlItem23.TextVisible = false;
            // 
            // layoutControlGroup13
            // 
            this.layoutControlGroup13.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItem24,
            this.layoutControlItem81});
            this.layoutControlGroup13.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroup13.Name = "layoutControlGroup13";
            this.layoutControlGroup13.Padding = new DevExpress.XtraLayout.Utils.Padding(0, 0, 0, 0);
            this.layoutControlGroup13.Size = new System.Drawing.Size(672, 347);
            this.layoutControlGroup13.Spacing = new DevExpress.XtraLayout.Utils.Padding(2, 2, 2, 2);
            this.layoutControlGroup13.Text = "&3-Corpo Diretivo";
            this.layoutControlGroup13.TextVisible = true;
            // 
            // layoutControlItem24
            // 
            this.layoutControlItem24.Control = this.grdCorpoDiretivo;
            this.layoutControlItem24.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItem24.Name = "layoutControlItem24";
            this.layoutControlItem24.Padding = new DevExpress.XtraLayout.Utils.Padding(5, 5, 5, 5);
            this.layoutControlItem24.Size = new System.Drawing.Size(672, 314);
            this.layoutControlItem24.Spacing = new DevExpress.XtraLayout.Utils.Padding(0, 0, 0, 0);
            this.layoutControlItem24.Text = "layoutControlItem24";
            this.layoutControlItem24.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem24.TextToControlDistance = 0;
            this.layoutControlItem24.TextVisible = false;
            // 
            // layoutControlItem81
            // 
            this.layoutControlItem81.Control = this.bnvCorpoDiretivo;
            this.layoutControlItem81.Location = new System.Drawing.Point(0, 314);
            this.layoutControlItem81.MinSize = new System.Drawing.Size(110, 32);
            this.layoutControlItem81.Name = "layoutControlItem81";
            this.layoutControlItem81.Padding = new DevExpress.XtraLayout.Utils.Padding(5, 5, 5, 5);
            this.layoutControlItem81.Size = new System.Drawing.Size(672, 33);
            this.layoutControlItem81.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem81.Spacing = new DevExpress.XtraLayout.Utils.Padding(0, 0, 0, 0);
            this.layoutControlItem81.Text = "layoutControlItem81";
            this.layoutControlItem81.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem81.TextToControlDistance = 0;
            this.layoutControlItem81.TextVisible = false;
            // 
            // layoutControlGroup14
            // 
            this.layoutControlGroup14.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlGroup15,
            this.layoutControlGroup16,
            this.layoutControlGroup17,
            this.layoutControlGroup19,
            this.layoutControlGroup18,
            this.layoutControlGroup24,
            this.layoutControlGroup25,
            this.layoutControlGroup26,
            this.lcgCorreio});
            this.layoutControlGroup14.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroup14.Name = "layoutControlGroup14";
            this.layoutControlGroup14.Padding = new DevExpress.XtraLayout.Utils.Padding(0, 0, 0, 0);
            this.layoutControlGroup14.Size = new System.Drawing.Size(672, 347);
            this.layoutControlGroup14.Spacing = new DevExpress.XtraLayout.Utils.Padding(2, 2, 2, 2);
            this.layoutControlGroup14.Text = "&4-Financeiro";
            this.layoutControlGroup14.TextVisible = true;
            // 
            // layoutControlGroup15
            // 
            this.layoutControlGroup15.GroupBordersVisible = false;
            this.layoutControlGroup15.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItem26,
            this.layoutControlItem28,
            this.layoutControlItem29,
            this.layoutControlItem30,
            this.layoutControlItem32,
            this.layoutControlItem33,
            this.layoutControlItem34,
            this.layoutControlItem35});
            this.layoutControlGroup15.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroup15.Name = "layoutControlGroup15";
            this.layoutControlGroup15.Padding = new DevExpress.XtraLayout.Utils.Padding(5, 5, 5, 5);
            this.layoutControlGroup15.Size = new System.Drawing.Size(672, 31);
            this.layoutControlGroup15.Spacing = new DevExpress.XtraLayout.Utils.Padding(2, 2, 2, 2);
            this.layoutControlGroup15.Text = "layoutControlGroup15";
            this.layoutControlGroup15.TextVisible = false;
            // 
            // layoutControlItem26
            // 
            this.layoutControlItem26.Control = this.lkpBancos;
            this.layoutControlItem26.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItem26.Name = "layoutControlItem26";
            this.layoutControlItem26.Padding = new DevExpress.XtraLayout.Utils.Padding(5, 5, 5, 5);
            this.layoutControlItem26.Size = new System.Drawing.Size(275, 31);
            this.layoutControlItem26.Spacing = new DevExpress.XtraLayout.Utils.Padding(0, 0, 0, 0);
            this.layoutControlItem26.Text = "Banco";
            this.layoutControlItem26.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem26.TextSize = new System.Drawing.Size(55, 20);
            this.layoutControlItem26.TextToControlDistance = 0;
            this.layoutControlItem26.TextVisible = true;
            // 
            // layoutControlItem28
            // 
            this.layoutControlItem28.Control = this.textEdit8;
            this.layoutControlItem28.Location = new System.Drawing.Point(275, 0);
            this.layoutControlItem28.Name = "layoutControlItem28";
            this.layoutControlItem28.Padding = new DevExpress.XtraLayout.Utils.Padding(5, 5, 5, 5);
            this.layoutControlItem28.Size = new System.Drawing.Size(112, 31);
            this.layoutControlItem28.Spacing = new DevExpress.XtraLayout.Utils.Padding(0, 0, 0, 0);
            this.layoutControlItem28.Text = "Ag�ncia";
            this.layoutControlItem28.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem28.TextSize = new System.Drawing.Size(40, 20);
            this.layoutControlItem28.TextVisible = true;
            // 
            // layoutControlItem29
            // 
            this.layoutControlItem29.Control = this.textEdit9;
            this.layoutControlItem29.Location = new System.Drawing.Point(457, 0);
            this.layoutControlItem29.Name = "layoutControlItem29";
            this.layoutControlItem29.Padding = new DevExpress.XtraLayout.Utils.Padding(5, 5, 5, 5);
            this.layoutControlItem29.Size = new System.Drawing.Size(96, 31);
            this.layoutControlItem29.Spacing = new DevExpress.XtraLayout.Utils.Padding(0, 0, 0, 0);
            this.layoutControlItem29.Text = "Conta";
            this.layoutControlItem29.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem29.TextSize = new System.Drawing.Size(30, 20);
            this.layoutControlItem29.TextVisible = true;
            // 
            // layoutControlItem30
            // 
            this.layoutControlItem30.Control = this.label1;
            this.layoutControlItem30.Location = new System.Drawing.Point(387, 0);
            this.layoutControlItem30.MinSize = new System.Drawing.Size(7, 30);
            this.layoutControlItem30.Name = "layoutControlItem30";
            this.layoutControlItem30.Padding = new DevExpress.XtraLayout.Utils.Padding(0, 0, 0, 0);
            this.layoutControlItem30.Size = new System.Drawing.Size(9, 31);
            this.layoutControlItem30.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem30.Spacing = new DevExpress.XtraLayout.Utils.Padding(0, 0, 0, 0);
            this.layoutControlItem30.Text = "layoutControlItem30";
            this.layoutControlItem30.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem30.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem30.TextToControlDistance = 0;
            this.layoutControlItem30.TextVisible = false;
            // 
            // layoutControlItem32
            // 
            this.layoutControlItem32.Control = this.textEdit10;
            this.layoutControlItem32.Location = new System.Drawing.Point(396, 0);
            this.layoutControlItem32.Name = "layoutControlItem32";
            this.layoutControlItem32.Padding = new DevExpress.XtraLayout.Utils.Padding(5, 5, 5, 5);
            this.layoutControlItem32.Size = new System.Drawing.Size(61, 31);
            this.layoutControlItem32.Spacing = new DevExpress.XtraLayout.Utils.Padding(0, 0, 0, 0);
            this.layoutControlItem32.Text = "layoutControlItem32";
            this.layoutControlItem32.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem32.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem32.TextToControlDistance = 0;
            this.layoutControlItem32.TextVisible = false;
            // 
            // layoutControlItem33
            // 
            this.layoutControlItem33.Control = this.label2;
            this.layoutControlItem33.Location = new System.Drawing.Point(553, 0);
            this.layoutControlItem33.MinSize = new System.Drawing.Size(5, 30);
            this.layoutControlItem33.Name = "layoutControlItem33";
            this.layoutControlItem33.Padding = new DevExpress.XtraLayout.Utils.Padding(0, 0, 0, 0);
            this.layoutControlItem33.Size = new System.Drawing.Size(5, 31);
            this.layoutControlItem33.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem33.Spacing = new DevExpress.XtraLayout.Utils.Padding(0, 0, 0, 0);
            this.layoutControlItem33.Text = "layoutControlItem33";
            this.layoutControlItem33.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem33.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem33.TextToControlDistance = 0;
            this.layoutControlItem33.TextVisible = false;
            // 
            // layoutControlItem34
            // 
            this.layoutControlItem34.Control = this.textEdit11;
            this.layoutControlItem34.Location = new System.Drawing.Point(558, 0);
            this.layoutControlItem34.Name = "layoutControlItem34";
            this.layoutControlItem34.Padding = new DevExpress.XtraLayout.Utils.Padding(5, 5, 5, 5);
            this.layoutControlItem34.Size = new System.Drawing.Size(61, 31);
            this.layoutControlItem34.Spacing = new DevExpress.XtraLayout.Utils.Padding(0, 0, 0, 0);
            this.layoutControlItem34.Text = "layoutControlItem34";
            this.layoutControlItem34.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem34.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem34.TextToControlDistance = 0;
            this.layoutControlItem34.TextVisible = false;
            // 
            // layoutControlItem35
            // 
            this.layoutControlItem35.Control = this.checkEdit5;
            this.layoutControlItem35.Location = new System.Drawing.Point(619, 0);
            this.layoutControlItem35.Name = "layoutControlItem35";
            this.layoutControlItem35.Padding = new DevExpress.XtraLayout.Utils.Padding(5, 5, 5, 5);
            this.layoutControlItem35.Size = new System.Drawing.Size(53, 31);
            this.layoutControlItem35.Spacing = new DevExpress.XtraLayout.Utils.Padding(0, 0, 0, 0);
            this.layoutControlItem35.Text = "layoutControlItem35";
            this.layoutControlItem35.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem35.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem35.TextToControlDistance = 0;
            this.layoutControlItem35.TextVisible = false;
            // 
            // layoutControlGroup16
            // 
            this.layoutControlGroup16.AllowCustomizeChildren = false;
            this.layoutControlGroup16.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItem31,
            this.lciValorFR});
            this.layoutControlGroup16.Location = new System.Drawing.Point(0, 31);
            this.layoutControlGroup16.Name = "layoutControlGroup16";
            this.layoutControlGroup16.Padding = new DevExpress.XtraLayout.Utils.Padding(0, 0, 0, 0);
            this.layoutControlGroup16.Size = new System.Drawing.Size(294, 75);
            this.layoutControlGroup16.Spacing = new DevExpress.XtraLayout.Utils.Padding(2, 2, 2, 2);
            this.layoutControlGroup16.Text = ":: Valor do Fundo de Reserva ::";
            this.layoutControlGroup16.TextVisible = true;
            // 
            // layoutControlItem31
            // 
            this.layoutControlItem31.Control = this.rdgFR;
            this.layoutControlItem31.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItem31.Name = "layoutControlItem31";
            this.layoutControlItem31.Padding = new DevExpress.XtraLayout.Utils.Padding(5, 5, 5, 5);
            this.layoutControlItem31.Size = new System.Drawing.Size(179, 51);
            this.layoutControlItem31.Spacing = new DevExpress.XtraLayout.Utils.Padding(0, 0, 0, 0);
            this.layoutControlItem31.Text = "layoutControlItem31";
            this.layoutControlItem31.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem31.TextToControlDistance = 0;
            this.layoutControlItem31.TextVisible = false;
            // 
            // lciValorFR
            // 
            this.lciValorFR.Control = this.calcEdit6;
            this.lciValorFR.Location = new System.Drawing.Point(179, 0);
            this.lciValorFR.Name = "lciValorFR";
            this.lciValorFR.Padding = new DevExpress.XtraLayout.Utils.Padding(5, 5, 5, 5);
            this.lciValorFR.Size = new System.Drawing.Size(109, 51);
            this.lciValorFR.Spacing = new DevExpress.XtraLayout.Utils.Padding(0, 0, 0, 0);
            this.lciValorFR.Text = "Valor";
            this.lciValorFR.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.lciValorFR.TextLocation = DevExpress.Utils.Locations.Top;
            this.lciValorFR.TextSize = new System.Drawing.Size(65, 20);
            this.lciValorFR.TextToControlDistance = 0;
            this.lciValorFR.TextVisible = true;
            // 
            // layoutControlGroup17
            // 
            this.layoutControlGroup17.AllowCustomizeChildren = false;
            this.layoutControlGroup17.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItem37,
            this.layoutControlItem36,
            this.layoutControlItem39});
            this.layoutControlGroup17.Location = new System.Drawing.Point(294, 31);
            this.layoutControlGroup17.Name = "layoutControlGroup17";
            this.layoutControlGroup17.Padding = new DevExpress.XtraLayout.Utils.Padding(0, 0, 0, 0);
            this.layoutControlGroup17.Size = new System.Drawing.Size(378, 75);
            this.layoutControlGroup17.Spacing = new DevExpress.XtraLayout.Utils.Padding(2, 2, 2, 2);
            this.layoutControlGroup17.Text = ":: Multa e Juros ::";
            this.layoutControlGroup17.TextVisible = true;
            // 
            // layoutControlItem37
            // 
            this.layoutControlItem37.Control = this.radioGroup4;
            this.layoutControlItem37.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItem37.Name = "layoutControlItem37";
            this.layoutControlItem37.Padding = new DevExpress.XtraLayout.Utils.Padding(5, 5, 5, 5);
            this.layoutControlItem37.Size = new System.Drawing.Size(139, 51);
            this.layoutControlItem37.Spacing = new DevExpress.XtraLayout.Utils.Padding(0, 0, 0, 0);
            this.layoutControlItem37.Text = "layoutControlItem37";
            this.layoutControlItem37.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem37.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem37.TextToControlDistance = 0;
            this.layoutControlItem37.TextVisible = false;
            // 
            // layoutControlItem36
            // 
            this.layoutControlItem36.Control = this.calcEdit5;
            this.layoutControlItem36.Location = new System.Drawing.Point(139, 0);
            this.layoutControlItem36.Name = "layoutControlItem36";
            this.layoutControlItem36.Padding = new DevExpress.XtraLayout.Utils.Padding(5, 5, 5, 5);
            this.layoutControlItem36.Size = new System.Drawing.Size(115, 51);
            this.layoutControlItem36.Spacing = new DevExpress.XtraLayout.Utils.Padding(0, 0, 0, 0);
            this.layoutControlItem36.Text = "Multa";
            this.layoutControlItem36.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem36.TextLocation = DevExpress.Utils.Locations.Top;
            this.layoutControlItem36.TextSize = new System.Drawing.Size(65, 20);
            this.layoutControlItem36.TextToControlDistance = 0;
            this.layoutControlItem36.TextVisible = true;
            // 
            // layoutControlItem39
            // 
            this.layoutControlItem39.Control = this.calcEdit7;
            this.layoutControlItem39.Location = new System.Drawing.Point(254, 0);
            this.layoutControlItem39.Name = "layoutControlItem39";
            this.layoutControlItem39.Padding = new DevExpress.XtraLayout.Utils.Padding(5, 5, 5, 5);
            this.layoutControlItem39.Size = new System.Drawing.Size(118, 51);
            this.layoutControlItem39.Spacing = new DevExpress.XtraLayout.Utils.Padding(0, 0, 0, 0);
            this.layoutControlItem39.Text = "Juros";
            this.layoutControlItem39.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem39.TextLocation = DevExpress.Utils.Locations.Top;
            this.layoutControlItem39.TextSize = new System.Drawing.Size(65, 20);
            this.layoutControlItem39.TextToControlDistance = 0;
            this.layoutControlItem39.TextVisible = true;
            // 
            // layoutControlGroup19
            // 
            this.layoutControlGroup19.AllowCustomizeChildren = false;
            this.layoutControlGroup19.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItem42,
            this.layoutControlItem40});
            this.layoutControlGroup19.Location = new System.Drawing.Point(0, 106);
            this.layoutControlGroup19.Name = "layoutControlGroup19";
            this.layoutControlGroup19.Padding = new DevExpress.XtraLayout.Utils.Padding(0, 0, 0, 0);
            this.layoutControlGroup19.Size = new System.Drawing.Size(328, 55);
            this.layoutControlGroup19.Spacing = new DevExpress.XtraLayout.Utils.Padding(2, 2, 2, 2);
            this.layoutControlGroup19.Text = ":: Outros Valores ::";
            this.layoutControlGroup19.TextVisible = true;
            // 
            // layoutControlItem42
            // 
            this.layoutControlItem42.Control = this.calcEdit9;
            this.layoutControlItem42.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItem42.Name = "layoutControlItem42";
            this.layoutControlItem42.Padding = new DevExpress.XtraLayout.Utils.Padding(5, 5, 5, 5);
            this.layoutControlItem42.Size = new System.Drawing.Size(181, 31);
            this.layoutControlItem42.Spacing = new DevExpress.XtraLayout.Utils.Padding(0, 0, 0, 0);
            this.layoutControlItem42.Text = "Condom�nio";
            this.layoutControlItem42.TextSize = new System.Drawing.Size(99, 20);
            this.layoutControlItem42.TextVisible = true;
            // 
            // layoutControlItem40
            // 
            this.layoutControlItem40.Control = this.calcEdit8;
            this.layoutControlItem40.Location = new System.Drawing.Point(181, 0);
            this.layoutControlItem40.Name = "layoutControlItem40";
            this.layoutControlItem40.Padding = new DevExpress.XtraLayout.Utils.Padding(5, 5, 5, 5);
            this.layoutControlItem40.Size = new System.Drawing.Size(141, 31);
            this.layoutControlItem40.Spacing = new DevExpress.XtraLayout.Utils.Padding(0, 0, 0, 0);
            this.layoutControlItem40.Text = "G�s (Kg)";
            this.layoutControlItem40.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem40.TextSize = new System.Drawing.Size(50, 20);
            this.layoutControlItem40.TextVisible = true;
            // 
            // layoutControlGroup18
            // 
            this.layoutControlGroup18.AllowCustomizeChildren = false;
            this.layoutControlGroup18.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItem43,
            this.layoutControlItem44});
            this.layoutControlGroup18.Location = new System.Drawing.Point(328, 106);
            this.layoutControlGroup18.Name = "layoutControlGroup18";
            this.layoutControlGroup18.Padding = new DevExpress.XtraLayout.Utils.Padding(0, 0, 0, 0);
            this.layoutControlGroup18.Size = new System.Drawing.Size(344, 55);
            this.layoutControlGroup18.Spacing = new DevExpress.XtraLayout.Utils.Padding(2, 2, 2, 2);
            this.layoutControlGroup18.Text = ":: Vencimento ::";
            this.layoutControlGroup18.TextVisible = true;
            // 
            // layoutControlItem43
            // 
            this.layoutControlItem43.Control = this.speVencimento;
            this.layoutControlItem43.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItem43.Name = "layoutControlItem43";
            this.layoutControlItem43.Padding = new DevExpress.XtraLayout.Utils.Padding(5, 5, 5, 5);
            this.layoutControlItem43.Size = new System.Drawing.Size(104, 31);
            this.layoutControlItem43.Spacing = new DevExpress.XtraLayout.Utils.Padding(0, 0, 0, 0);
            this.layoutControlItem43.Text = "Dia";
            this.layoutControlItem43.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem43.TextSize = new System.Drawing.Size(15, 20);
            this.layoutControlItem43.TextVisible = true;
            // 
            // layoutControlItem44
            // 
            this.layoutControlItem44.Control = this.txtRegra;
            this.layoutControlItem44.Location = new System.Drawing.Point(104, 0);
            this.layoutControlItem44.MaxSize = new System.Drawing.Size(0, 27);
            this.layoutControlItem44.MinSize = new System.Drawing.Size(20, 27);
            this.layoutControlItem44.Name = "layoutControlItem44";
            this.layoutControlItem44.Padding = new DevExpress.XtraLayout.Utils.Padding(5, 5, 5, 5);
            this.layoutControlItem44.Size = new System.Drawing.Size(234, 31);
            this.layoutControlItem44.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem44.Spacing = new DevExpress.XtraLayout.Utils.Padding(0, 0, 0, 0);
            this.layoutControlItem44.Text = "layoutControlItem44";
            this.layoutControlItem44.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem44.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem44.TextToControlDistance = 0;
            this.layoutControlItem44.TextVisible = false;
            // 
            // layoutControlGroup24
            // 
            this.layoutControlGroup24.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItem41,
            this.layoutControlItem48});
            this.layoutControlGroup24.Location = new System.Drawing.Point(0, 161);
            this.layoutControlGroup24.Name = "layoutControlGroup24";
            this.layoutControlGroup24.Padding = new DevExpress.XtraLayout.Utils.Padding(0, 0, 0, 0);
            this.layoutControlGroup24.Size = new System.Drawing.Size(230, 55);
            this.layoutControlGroup24.Spacing = new DevExpress.XtraLayout.Utils.Padding(2, 2, 2, 2);
            this.layoutControlGroup24.Text = ":: Per�odo Cont�bil ::";
            this.layoutControlGroup24.TextVisible = true;
            // 
            // layoutControlItem41
            // 
            this.layoutControlItem41.Control = this.spePerContabilInicio;
            this.layoutControlItem41.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItem41.MaxSize = new System.Drawing.Size(127, 30);
            this.layoutControlItem41.MinSize = new System.Drawing.Size(127, 30);
            this.layoutControlItem41.Name = "layoutControlItem41";
            this.layoutControlItem41.Padding = new DevExpress.XtraLayout.Utils.Padding(5, 5, 5, 5);
            this.layoutControlItem41.Size = new System.Drawing.Size(127, 31);
            this.layoutControlItem41.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem41.Spacing = new DevExpress.XtraLayout.Utils.Padding(0, 0, 0, 0);
            this.layoutControlItem41.Text = "Dia Inicial";
            this.layoutControlItem41.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem41.TextSize = new System.Drawing.Size(65, 20);
            this.layoutControlItem41.TextToControlDistance = 0;
            this.layoutControlItem41.TextVisible = true;
            // 
            // layoutControlItem48
            // 
            this.layoutControlItem48.Control = this.txtPerContabilFim;
            this.layoutControlItem48.Location = new System.Drawing.Point(127, 0);
            this.layoutControlItem48.MaxSize = new System.Drawing.Size(97, 30);
            this.layoutControlItem48.MinSize = new System.Drawing.Size(97, 30);
            this.layoutControlItem48.Name = "layoutControlItem48";
            this.layoutControlItem48.Padding = new DevExpress.XtraLayout.Utils.Padding(5, 5, 5, 5);
            this.layoutControlItem48.Size = new System.Drawing.Size(97, 31);
            this.layoutControlItem48.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem48.Spacing = new DevExpress.XtraLayout.Utils.Padding(0, 0, 0, 0);
            this.layoutControlItem48.Text = "Dia Final";
            this.layoutControlItem48.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem48.TextSize = new System.Drawing.Size(40, 20);
            this.layoutControlItem48.TextVisible = true;
            // 
            // layoutControlGroup25
            // 
            this.layoutControlGroup25.AllowCustomizeChildren = false;
            this.layoutControlGroup25.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItem49,
            this.layoutControlItem50,
            this.layoutControlItem51,
            this.layoutControlItem52,
            this.layoutControlItem53});
            this.layoutControlGroup25.Location = new System.Drawing.Point(230, 161);
            this.layoutControlGroup25.Name = "layoutControlGroup25";
            this.layoutControlGroup25.Padding = new DevExpress.XtraLayout.Utils.Padding(0, 0, 0, 0);
            this.layoutControlGroup25.Size = new System.Drawing.Size(442, 55);
            this.layoutControlGroup25.Spacing = new DevExpress.XtraLayout.Utils.Padding(2, 2, 2, 2);
            this.layoutControlGroup25.Text = ":: Impress�o de Balancetes ::";
            this.layoutControlGroup25.TextVisible = true;
            // 
            // layoutControlItem49
            // 
            this.layoutControlItem49.Control = this.spinEdit3;
            this.layoutControlItem49.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItem49.Name = "layoutControlItem49";
            this.layoutControlItem49.Padding = new DevExpress.XtraLayout.Utils.Padding(5, 5, 5, 5);
            this.layoutControlItem49.Size = new System.Drawing.Size(88, 31);
            this.layoutControlItem49.Spacing = new DevExpress.XtraLayout.Utils.Padding(0, 0, 0, 0);
            this.layoutControlItem49.Text = "M1";
            this.layoutControlItem49.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem49.TextSize = new System.Drawing.Size(15, 20);
            this.layoutControlItem49.TextVisible = true;
            // 
            // layoutControlItem50
            // 
            this.layoutControlItem50.Control = this.spinEdit4;
            this.layoutControlItem50.Location = new System.Drawing.Point(88, 0);
            this.layoutControlItem50.Name = "layoutControlItem50";
            this.layoutControlItem50.Padding = new DevExpress.XtraLayout.Utils.Padding(5, 5, 5, 5);
            this.layoutControlItem50.Size = new System.Drawing.Size(88, 31);
            this.layoutControlItem50.Spacing = new DevExpress.XtraLayout.Utils.Padding(0, 0, 0, 0);
            this.layoutControlItem50.Text = "M2";
            this.layoutControlItem50.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem50.TextSize = new System.Drawing.Size(15, 20);
            this.layoutControlItem50.TextVisible = true;
            // 
            // layoutControlItem51
            // 
            this.layoutControlItem51.Control = this.spinEdit5;
            this.layoutControlItem51.Location = new System.Drawing.Point(176, 0);
            this.layoutControlItem51.Name = "layoutControlItem51";
            this.layoutControlItem51.Padding = new DevExpress.XtraLayout.Utils.Padding(5, 5, 5, 5);
            this.layoutControlItem51.Size = new System.Drawing.Size(87, 31);
            this.layoutControlItem51.Spacing = new DevExpress.XtraLayout.Utils.Padding(0, 0, 0, 0);
            this.layoutControlItem51.Text = "M3";
            this.layoutControlItem51.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem51.TextSize = new System.Drawing.Size(15, 20);
            this.layoutControlItem51.TextVisible = true;
            // 
            // layoutControlItem52
            // 
            this.layoutControlItem52.Control = this.spinEdit6;
            this.layoutControlItem52.Location = new System.Drawing.Point(263, 0);
            this.layoutControlItem52.Name = "layoutControlItem52";
            this.layoutControlItem52.Padding = new DevExpress.XtraLayout.Utils.Padding(5, 5, 5, 5);
            this.layoutControlItem52.Size = new System.Drawing.Size(87, 31);
            this.layoutControlItem52.Spacing = new DevExpress.XtraLayout.Utils.Padding(0, 0, 0, 0);
            this.layoutControlItem52.Text = "M4";
            this.layoutControlItem52.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem52.TextSize = new System.Drawing.Size(15, 20);
            this.layoutControlItem52.TextVisible = true;
            // 
            // layoutControlItem53
            // 
            this.layoutControlItem53.Control = this.spinEdit7;
            this.layoutControlItem53.Location = new System.Drawing.Point(350, 0);
            this.layoutControlItem53.Name = "layoutControlItem53";
            this.layoutControlItem53.Padding = new DevExpress.XtraLayout.Utils.Padding(5, 5, 5, 5);
            this.layoutControlItem53.Size = new System.Drawing.Size(86, 31);
            this.layoutControlItem53.Spacing = new DevExpress.XtraLayout.Utils.Padding(0, 0, 0, 0);
            this.layoutControlItem53.Text = "M5";
            this.layoutControlItem53.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem53.TextSize = new System.Drawing.Size(15, 20);
            this.layoutControlItem53.TextVisible = true;
            // 
            // layoutControlGroup26
            // 
            this.layoutControlGroup26.AllowCustomizeChildren = false;
            this.layoutControlGroup26.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItem54});
            this.layoutControlGroup26.Location = new System.Drawing.Point(0, 216);
            this.layoutControlGroup26.Name = "layoutControlGroup26";
            this.layoutControlGroup26.Padding = new DevExpress.XtraLayout.Utils.Padding(0, 0, 0, 0);
            this.layoutControlGroup26.Size = new System.Drawing.Size(131, 131);
            this.layoutControlGroup26.Spacing = new DevExpress.XtraLayout.Utils.Padding(2, 2, 2, 2);
            this.layoutControlGroup26.Text = ":: Carta Cobran�a ::";
            this.layoutControlGroup26.TextVisible = true;
            // 
            // layoutControlItem54
            // 
            this.layoutControlItem54.Control = this.spinEdit8;
            this.layoutControlItem54.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItem54.Name = "layoutControlItem54";
            this.layoutControlItem54.Padding = new DevExpress.XtraLayout.Utils.Padding(5, 5, 5, 5);
            this.layoutControlItem54.Size = new System.Drawing.Size(125, 107);
            this.layoutControlItem54.Spacing = new DevExpress.XtraLayout.Utils.Padding(0, 0, 0, 0);
            this.layoutControlItem54.Text = "Per�odo para Envio";
            this.layoutControlItem54.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem54.TextLocation = DevExpress.Utils.Locations.Top;
            this.layoutControlItem54.TextSize = new System.Drawing.Size(55, 20);
            this.layoutControlItem54.TextToControlDistance = 0;
            this.layoutControlItem54.TextVisible = true;
            // 
            // lcgCorreio
            // 
            this.lcgCorreio.AllowCustomizeChildren = false;
            this.lcgCorreio.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlGroup20,
            this.layoutControlItem46,
            this.layoutControlGroup21,
            this.layoutControlGroup22,
            this.layoutControlGroup23});
            this.lcgCorreio.Location = new System.Drawing.Point(131, 216);
            this.lcgCorreio.Name = "lcgCorreio";
            this.lcgCorreio.Padding = new DevExpress.XtraLayout.Utils.Padding(0, 0, 0, 0);
            this.lcgCorreio.Size = new System.Drawing.Size(541, 131);
            this.lcgCorreio.Spacing = new DevExpress.XtraLayout.Utils.Padding(2, 2, 2, 2);
            this.lcgCorreio.Text = ":: Correio ::";
            this.lcgCorreio.TextVisible = true;
            // 
            // layoutControlGroup20
            // 
            this.layoutControlGroup20.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItem45,
            this.layoutControlItem47});
            this.layoutControlGroup20.Location = new System.Drawing.Point(0, 20);
            this.layoutControlGroup20.Name = "layoutControlGroup20";
            this.layoutControlGroup20.Padding = new DevExpress.XtraLayout.Utils.Padding(0, 0, 0, 0);
            this.layoutControlGroup20.Size = new System.Drawing.Size(174, 87);
            this.layoutControlGroup20.Spacing = new DevExpress.XtraLayout.Utils.Padding(2, 2, 2, 2);
            this.layoutControlGroup20.Text = ":: Valor ::";
            this.layoutControlGroup20.TextVisible = true;
            // 
            // layoutControlItem45
            // 
            this.layoutControlItem45.Control = this.clcAR;
            this.layoutControlItem45.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItem45.Name = "layoutControlItem45";
            this.layoutControlItem45.Padding = new DevExpress.XtraLayout.Utils.Padding(4, 4, 4, 4);
            this.layoutControlItem45.Size = new System.Drawing.Size(168, 29);
            this.layoutControlItem45.Spacing = new DevExpress.XtraLayout.Utils.Padding(0, 0, 0, 0);
            this.layoutControlItem45.Text = "AR";
            this.layoutControlItem45.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem45.TextSize = new System.Drawing.Size(65, 20);
            this.layoutControlItem45.TextVisible = true;
            // 
            // layoutControlItem47
            // 
            this.layoutControlItem47.Control = this.clcCS;
            this.layoutControlItem47.Location = new System.Drawing.Point(0, 29);
            this.layoutControlItem47.Name = "layoutControlItem47";
            this.layoutControlItem47.Padding = new DevExpress.XtraLayout.Utils.Padding(4, 4, 4, 4);
            this.layoutControlItem47.Size = new System.Drawing.Size(168, 34);
            this.layoutControlItem47.Spacing = new DevExpress.XtraLayout.Utils.Padding(0, 0, 0, 0);
            this.layoutControlItem47.Text = "Carta Simples";
            this.layoutControlItem47.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem47.TextSize = new System.Drawing.Size(65, 20);
            this.layoutControlItem47.TextVisible = true;
            // 
            // layoutControlItem46
            // 
            this.layoutControlItem46.Control = this.chkRepassaCorreio;
            this.layoutControlItem46.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItem46.Name = "layoutControlItem46";
            this.layoutControlItem46.Padding = new DevExpress.XtraLayout.Utils.Padding(0, 0, 0, 0);
            this.layoutControlItem46.Size = new System.Drawing.Size(535, 20);
            this.layoutControlItem46.Spacing = new DevExpress.XtraLayout.Utils.Padding(0, 0, 0, 0);
            this.layoutControlItem46.Text = "layoutControlItem46";
            this.layoutControlItem46.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem46.TextToControlDistance = 0;
            this.layoutControlItem46.TextVisible = false;
            // 
            // layoutControlGroup21
            // 
            this.layoutControlGroup21.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItem55});
            this.layoutControlGroup21.Location = new System.Drawing.Point(174, 20);
            this.layoutControlGroup21.Name = "layoutControlGroup21";
            this.layoutControlGroup21.Padding = new DevExpress.XtraLayout.Utils.Padding(0, 0, 0, 0);
            this.layoutControlGroup21.Size = new System.Drawing.Size(118, 87);
            this.layoutControlGroup21.Spacing = new DevExpress.XtraLayout.Utils.Padding(2, 2, 2, 2);
            this.layoutControlGroup21.Text = ":: Assembl�ia ::";
            this.layoutControlGroup21.TextVisible = true;
            // 
            // layoutControlItem55
            // 
            this.layoutControlItem55.Control = this.rdgAssembleiaCorreio;
            this.layoutControlItem55.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItem55.Name = "layoutControlItem55";
            this.layoutControlItem55.Padding = new DevExpress.XtraLayout.Utils.Padding(0, 0, 0, 0);
            this.layoutControlItem55.Size = new System.Drawing.Size(112, 63);
            this.layoutControlItem55.Spacing = new DevExpress.XtraLayout.Utils.Padding(0, 0, 0, 0);
            this.layoutControlItem55.Text = "layoutControlItem55";
            this.layoutControlItem55.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem55.TextToControlDistance = 0;
            this.layoutControlItem55.TextVisible = false;
            // 
            // layoutControlGroup22
            // 
            this.layoutControlGroup22.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItem56});
            this.layoutControlGroup22.Location = new System.Drawing.Point(292, 20);
            this.layoutControlGroup22.Name = "layoutControlGroup22";
            this.layoutControlGroup22.Padding = new DevExpress.XtraLayout.Utils.Padding(0, 0, 0, 0);
            this.layoutControlGroup22.Size = new System.Drawing.Size(119, 87);
            this.layoutControlGroup22.Spacing = new DevExpress.XtraLayout.Utils.Padding(2, 2, 2, 2);
            this.layoutControlGroup22.Text = ":: Cobran�a ::";
            this.layoutControlGroup22.TextVisible = true;
            // 
            // layoutControlItem56
            // 
            this.layoutControlItem56.Control = this.rdgCobrancaCorreio;
            this.layoutControlItem56.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItem56.Name = "layoutControlItem56";
            this.layoutControlItem56.Padding = new DevExpress.XtraLayout.Utils.Padding(0, 0, 0, 0);
            this.layoutControlItem56.Size = new System.Drawing.Size(113, 63);
            this.layoutControlItem56.Spacing = new DevExpress.XtraLayout.Utils.Padding(0, 0, 0, 0);
            this.layoutControlItem56.Text = "layoutControlItem56";
            this.layoutControlItem56.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem56.TextToControlDistance = 0;
            this.layoutControlItem56.TextVisible = false;
            // 
            // layoutControlGroup23
            // 
            this.layoutControlGroup23.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItem57});
            this.layoutControlGroup23.Location = new System.Drawing.Point(411, 20);
            this.layoutControlGroup23.Name = "layoutControlGroup23";
            this.layoutControlGroup23.Padding = new DevExpress.XtraLayout.Utils.Padding(0, 0, 0, 0);
            this.layoutControlGroup23.Size = new System.Drawing.Size(124, 87);
            this.layoutControlGroup23.Spacing = new DevExpress.XtraLayout.Utils.Padding(2, 2, 2, 2);
            this.layoutControlGroup23.Text = ":: Boleto ::";
            this.layoutControlGroup23.TextVisible = true;
            // 
            // layoutControlItem57
            // 
            this.layoutControlItem57.Control = this.rdgBoletoCorreio;
            this.layoutControlItem57.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItem57.Name = "layoutControlItem57";
            this.layoutControlItem57.Padding = new DevExpress.XtraLayout.Utils.Padding(0, 0, 0, 0);
            this.layoutControlItem57.Size = new System.Drawing.Size(118, 63);
            this.layoutControlItem57.Spacing = new DevExpress.XtraLayout.Utils.Padding(0, 0, 0, 0);
            this.layoutControlItem57.Text = "layoutControlItem57";
            this.layoutControlItem57.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem57.TextToControlDistance = 0;
            this.layoutControlItem57.TextVisible = false;
            // 
            // layoutControlGroup28
            // 
            this.layoutControlGroup28.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItem38,
            this.layoutControlItem58});
            this.layoutControlGroup28.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroup28.Name = "layoutControlGroup28";
            this.layoutControlGroup28.Padding = new DevExpress.XtraLayout.Utils.Padding(0, 0, 0, 0);
            this.layoutControlGroup28.Size = new System.Drawing.Size(672, 347);
            this.layoutControlGroup28.Spacing = new DevExpress.XtraLayout.Utils.Padding(2, 2, 2, 2);
            this.layoutControlGroup28.Text = "&5-Saldo";
            this.layoutControlGroup28.TextVisible = true;
            // 
            // layoutControlItem38
            // 
            this.layoutControlItem38.Control = this.grdAplicacoes;
            this.layoutControlItem38.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItem38.Name = "layoutControlItem38";
            this.layoutControlItem38.Padding = new DevExpress.XtraLayout.Utils.Padding(5, 5, 5, 5);
            this.layoutControlItem38.Size = new System.Drawing.Size(672, 316);
            this.layoutControlItem38.Spacing = new DevExpress.XtraLayout.Utils.Padding(0, 0, 0, 0);
            this.layoutControlItem38.Text = "layoutControlItem38";
            this.layoutControlItem38.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem38.TextToControlDistance = 0;
            this.layoutControlItem38.TextVisible = false;
            // 
            // layoutControlItem58
            // 
            this.layoutControlItem58.Control = this.bnvAplicacoes;
            this.layoutControlItem58.Location = new System.Drawing.Point(0, 316);
            this.layoutControlItem58.Name = "layoutControlItem58";
            this.layoutControlItem58.Padding = new DevExpress.XtraLayout.Utils.Padding(5, 5, 5, 5);
            this.layoutControlItem58.Size = new System.Drawing.Size(672, 31);
            this.layoutControlItem58.Spacing = new DevExpress.XtraLayout.Utils.Padding(0, 0, 0, 0);
            this.layoutControlItem58.Text = "layoutControlItem58";
            this.layoutControlItem58.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem58.TextToControlDistance = 0;
            this.layoutControlItem58.TextVisible = false;
            // 
            // layoutControlGroup29
            // 
            this.layoutControlGroup29.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlGroup30,
            this.layoutControlGroup31,
            this.layoutControlGroup32,
            this.layoutControlGroup33,
            this.layoutControlItem69,
            this.layoutControlItem66,
            this.layoutControlItem68,
            this.layoutControlGroup34,
            this.layoutControlItem67,
            this.layoutControlItem93});
            this.layoutControlGroup29.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroup29.Name = "layoutControlGroup29";
            this.layoutControlGroup29.Padding = new DevExpress.XtraLayout.Utils.Padding(0, 0, 0, 0);
            this.layoutControlGroup29.Size = new System.Drawing.Size(672, 347);
            this.layoutControlGroup29.Spacing = new DevExpress.XtraLayout.Utils.Padding(2, 2, 2, 2);
            this.layoutControlGroup29.Text = "&6-Operacional";
            this.layoutControlGroup29.TextVisible = true;
            // 
            // layoutControlGroup30
            // 
            this.layoutControlGroup30.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItem60,
            this.layoutControlItem61});
            this.layoutControlGroup30.Location = new System.Drawing.Point(0, 55);
            this.layoutControlGroup30.Name = "layoutControlGroup30";
            this.layoutControlGroup30.Padding = new DevExpress.XtraLayout.Utils.Padding(0, 0, 0, 0);
            this.layoutControlGroup30.Size = new System.Drawing.Size(672, 58);
            this.layoutControlGroup30.Spacing = new DevExpress.XtraLayout.Utils.Padding(2, 2, 5, 2);
            this.layoutControlGroup30.Text = ":: Auditor ::";
            this.layoutControlGroup30.TextVisible = true;
            // 
            // layoutControlItem60
            // 
            this.layoutControlItem60.Control = this.lookUpEdit2;
            this.layoutControlItem60.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItem60.Name = "layoutControlItem60";
            this.layoutControlItem60.Padding = new DevExpress.XtraLayout.Utils.Padding(5, 5, 5, 5);
            this.layoutControlItem60.Size = new System.Drawing.Size(336, 31);
            this.layoutControlItem60.Spacing = new DevExpress.XtraLayout.Utils.Padding(0, 0, 0, 0);
            this.layoutControlItem60.Text = "Auditor 1";
            this.layoutControlItem60.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem60.TextSize = new System.Drawing.Size(65, 20);
            this.layoutControlItem60.TextToControlDistance = 0;
            this.layoutControlItem60.TextVisible = true;
            // 
            // layoutControlItem61
            // 
            this.layoutControlItem61.Control = this.lookUpEdit3;
            this.layoutControlItem61.Location = new System.Drawing.Point(336, 0);
            this.layoutControlItem61.Name = "layoutControlItem61";
            this.layoutControlItem61.Padding = new DevExpress.XtraLayout.Utils.Padding(5, 5, 5, 5);
            this.layoutControlItem61.Size = new System.Drawing.Size(330, 31);
            this.layoutControlItem61.Spacing = new DevExpress.XtraLayout.Utils.Padding(0, 0, 0, 0);
            this.layoutControlItem61.Text = "Auditor 2";
            this.layoutControlItem61.TextSize = new System.Drawing.Size(99, 20);
            this.layoutControlItem61.TextVisible = true;
            // 
            // layoutControlGroup31
            // 
            this.layoutControlGroup31.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItem62,
            this.layoutControlItem59});
            this.layoutControlGroup31.Location = new System.Drawing.Point(0, 113);
            this.layoutControlGroup31.Name = "layoutControlGroup31";
            this.layoutControlGroup31.Padding = new DevExpress.XtraLayout.Utils.Padding(0, 0, 0, 0);
            this.layoutControlGroup31.Size = new System.Drawing.Size(672, 89);
            this.layoutControlGroup31.Spacing = new DevExpress.XtraLayout.Utils.Padding(2, 2, 5, 2);
            this.layoutControlGroup31.Text = ":: Seguro ::";
            this.layoutControlGroup31.TextVisible = true;
            // 
            // layoutControlItem62
            // 
            this.layoutControlItem62.Control = this.lkpSeguradoras;
            this.layoutControlItem62.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItem62.Name = "layoutControlItem62";
            this.layoutControlItem62.Padding = new DevExpress.XtraLayout.Utils.Padding(5, 5, 5, 5);
            this.layoutControlItem62.Size = new System.Drawing.Size(666, 31);
            this.layoutControlItem62.Spacing = new DevExpress.XtraLayout.Utils.Padding(0, 0, 0, 0);
            this.layoutControlItem62.Text = "Seguradora";
            this.layoutControlItem62.TextSize = new System.Drawing.Size(99, 20);
            this.layoutControlItem62.TextVisible = true;
            // 
            // layoutControlItem59
            // 
            this.layoutControlItem59.Control = this.lkpCorretora;
            this.layoutControlItem59.Location = new System.Drawing.Point(0, 31);
            this.layoutControlItem59.Name = "layoutControlItem59";
            this.layoutControlItem59.Padding = new DevExpress.XtraLayout.Utils.Padding(5, 5, 5, 5);
            this.layoutControlItem59.Size = new System.Drawing.Size(666, 31);
            this.layoutControlItem59.Spacing = new DevExpress.XtraLayout.Utils.Padding(0, 0, 0, 0);
            this.layoutControlItem59.Text = "Corretora";
            this.layoutControlItem59.TextSize = new System.Drawing.Size(99, 20);
            this.layoutControlItem59.TextVisible = true;
            // 
            // layoutControlGroup32
            // 
            this.layoutControlGroup32.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItem64,
            this.layoutControlItem65,
            this.layoutControlItem63});
            this.layoutControlGroup32.Location = new System.Drawing.Point(0, 202);
            this.layoutControlGroup32.Name = "layoutControlGroup32";
            this.layoutControlGroup32.Padding = new DevExpress.XtraLayout.Utils.Padding(0, 0, 0, 0);
            this.layoutControlGroup32.Size = new System.Drawing.Size(672, 57);
            this.layoutControlGroup32.Spacing = new DevExpress.XtraLayout.Utils.Padding(2, 2, 5, 2);
            this.layoutControlGroup32.Text = ":: Vistoria ::";
            this.layoutControlGroup32.TextVisible = true;
            // 
            // layoutControlItem64
            // 
            this.layoutControlItem64.Control = this.dateEdit2;
            this.layoutControlItem64.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItem64.MinSize = new System.Drawing.Size(150, 30);
            this.layoutControlItem64.Name = "layoutControlItem64";
            this.layoutControlItem64.Padding = new DevExpress.XtraLayout.Utils.Padding(5, 5, 5, 5);
            this.layoutControlItem64.Size = new System.Drawing.Size(208, 30);
            this.layoutControlItem64.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem64.Spacing = new DevExpress.XtraLayout.Utils.Padding(0, 0, 0, 0);
            this.layoutControlItem64.Text = "Elevador";
            this.layoutControlItem64.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem64.TextSize = new System.Drawing.Size(65, 20);
            this.layoutControlItem64.TextToControlDistance = 0;
            this.layoutControlItem64.TextVisible = true;
            // 
            // layoutControlItem65
            // 
            this.layoutControlItem65.Control = this.dateEdit3;
            this.layoutControlItem65.Location = new System.Drawing.Point(208, 0);
            this.layoutControlItem65.MinSize = new System.Drawing.Size(125, 30);
            this.layoutControlItem65.Name = "layoutControlItem65";
            this.layoutControlItem65.Padding = new DevExpress.XtraLayout.Utils.Padding(5, 5, 5, 5);
            this.layoutControlItem65.Size = new System.Drawing.Size(265, 30);
            this.layoutControlItem65.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem65.Spacing = new DevExpress.XtraLayout.Utils.Padding(0, 0, 0, 0);
            this.layoutControlItem65.Text = "Extintor de Inc�ndio";
            this.layoutControlItem65.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem65.TextSize = new System.Drawing.Size(110, 20);
            this.layoutControlItem65.TextToControlDistance = 0;
            this.layoutControlItem65.TextVisible = true;
            // 
            // layoutControlItem63
            // 
            this.layoutControlItem63.Control = this.dateEdit1;
            this.layoutControlItem63.Location = new System.Drawing.Point(473, 0);
            this.layoutControlItem63.MinSize = new System.Drawing.Size(150, 30);
            this.layoutControlItem63.Name = "layoutControlItem63";
            this.layoutControlItem63.Padding = new DevExpress.XtraLayout.Utils.Padding(5, 5, 5, 5);
            this.layoutControlItem63.Size = new System.Drawing.Size(193, 30);
            this.layoutControlItem63.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem63.Spacing = new DevExpress.XtraLayout.Utils.Padding(0, 0, 0, 0);
            this.layoutControlItem63.Text = "AVCB";
            this.layoutControlItem63.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem63.TextSize = new System.Drawing.Size(40, 20);
            this.layoutControlItem63.TextToControlDistance = 0;
            this.layoutControlItem63.TextVisible = true;
            // 
            // layoutControlGroup33
            // 
            this.layoutControlGroup33.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItem72,
            this.layoutControlItem73});
            this.layoutControlGroup33.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroup33.Name = "layoutControlGroup33";
            this.layoutControlGroup33.Padding = new DevExpress.XtraLayout.Utils.Padding(0, 0, 0, 0);
            this.layoutControlGroup33.Size = new System.Drawing.Size(672, 55);
            this.layoutControlGroup33.Spacing = new DevExpress.XtraLayout.Utils.Padding(2, 2, 2, 2);
            this.layoutControlGroup33.Text = ":: Jur�dico ::";
            this.layoutControlGroup33.TextVisible = true;
            // 
            // layoutControlItem72
            // 
            this.layoutControlItem72.Control = this.lookUpEdit5;
            this.layoutControlItem72.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItem72.Name = "layoutControlItem72";
            this.layoutControlItem72.Padding = new DevExpress.XtraLayout.Utils.Padding(5, 5, 5, 5);
            this.layoutControlItem72.Size = new System.Drawing.Size(336, 31);
            this.layoutControlItem72.Spacing = new DevExpress.XtraLayout.Utils.Padding(0, 0, 0, 0);
            this.layoutControlItem72.Text = "Advogado 1";
            this.layoutControlItem72.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem72.TextSize = new System.Drawing.Size(65, 20);
            this.layoutControlItem72.TextToControlDistance = 0;
            this.layoutControlItem72.TextVisible = true;
            // 
            // layoutControlItem73
            // 
            this.layoutControlItem73.Control = this.lookUpEdit6;
            this.layoutControlItem73.Location = new System.Drawing.Point(336, 0);
            this.layoutControlItem73.Name = "layoutControlItem73";
            this.layoutControlItem73.Padding = new DevExpress.XtraLayout.Utils.Padding(5, 5, 5, 5);
            this.layoutControlItem73.Size = new System.Drawing.Size(330, 31);
            this.layoutControlItem73.Spacing = new DevExpress.XtraLayout.Utils.Padding(0, 0, 0, 0);
            this.layoutControlItem73.Text = "Advogado 2";
            this.layoutControlItem73.TextSize = new System.Drawing.Size(99, 20);
            this.layoutControlItem73.TextVisible = true;
            // 
            // layoutControlItem69
            // 
            this.layoutControlItem69.Control = this.checkEdit10;
            this.layoutControlItem69.Location = new System.Drawing.Point(252, 317);
            this.layoutControlItem69.Name = "layoutControlItem69";
            this.layoutControlItem69.Padding = new DevExpress.XtraLayout.Utils.Padding(5, 5, 5, 5);
            this.layoutControlItem69.Size = new System.Drawing.Size(161, 30);
            this.layoutControlItem69.Spacing = new DevExpress.XtraLayout.Utils.Padding(0, 0, 0, 0);
            this.layoutControlItem69.Text = "layoutControlItem69";
            this.layoutControlItem69.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem69.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem69.TextToControlDistance = 0;
            this.layoutControlItem69.TextVisible = false;
            // 
            // layoutControlItem66
            // 
            this.layoutControlItem66.Control = this.checkEdit6;
            this.layoutControlItem66.Location = new System.Drawing.Point(126, 317);
            this.layoutControlItem66.Name = "layoutControlItem66";
            this.layoutControlItem66.Padding = new DevExpress.XtraLayout.Utils.Padding(5, 5, 5, 5);
            this.layoutControlItem66.Size = new System.Drawing.Size(126, 30);
            this.layoutControlItem66.Spacing = new DevExpress.XtraLayout.Utils.Padding(0, 0, 0, 0);
            this.layoutControlItem66.Text = "layoutControlItem66";
            this.layoutControlItem66.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem66.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem66.TextToControlDistance = 0;
            this.layoutControlItem66.TextVisible = false;
            // 
            // layoutControlItem68
            // 
            this.layoutControlItem68.Control = this.checkEdit9;
            this.layoutControlItem68.Location = new System.Drawing.Point(413, 317);
            this.layoutControlItem68.Name = "layoutControlItem68";
            this.layoutControlItem68.Padding = new DevExpress.XtraLayout.Utils.Padding(5, 5, 5, 5);
            this.layoutControlItem68.Size = new System.Drawing.Size(138, 30);
            this.layoutControlItem68.Spacing = new DevExpress.XtraLayout.Utils.Padding(0, 0, 0, 0);
            this.layoutControlItem68.Text = "layoutControlItem68";
            this.layoutControlItem68.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem68.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem68.TextToControlDistance = 0;
            this.layoutControlItem68.TextVisible = false;
            // 
            // layoutControlGroup34
            // 
            this.layoutControlGroup34.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItem71,
            this.layoutControlItem70});
            this.layoutControlGroup34.Location = new System.Drawing.Point(0, 259);
            this.layoutControlGroup34.Name = "layoutControlGroup34";
            this.layoutControlGroup34.Padding = new DevExpress.XtraLayout.Utils.Padding(0, 0, 0, 0);
            this.layoutControlGroup34.Size = new System.Drawing.Size(672, 58);
            this.layoutControlGroup34.Spacing = new DevExpress.XtraLayout.Utils.Padding(2, 2, 5, 2);
            this.layoutControlGroup34.Text = ":: Assembl�ia ::";
            this.layoutControlGroup34.TextVisible = true;
            // 
            // layoutControlItem71
            // 
            this.layoutControlItem71.Control = this.spinEdit1;
            this.layoutControlItem71.Location = new System.Drawing.Point(345, 0);
            this.layoutControlItem71.Name = "layoutControlItem71";
            this.layoutControlItem71.Padding = new DevExpress.XtraLayout.Utils.Padding(5, 5, 5, 5);
            this.layoutControlItem71.Size = new System.Drawing.Size(321, 31);
            this.layoutControlItem71.Spacing = new DevExpress.XtraLayout.Utils.Padding(0, 0, 0, 0);
            this.layoutControlItem71.Text = "N� Dias m�nimo para aviso da pr�xima";
            this.layoutControlItem71.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem71.TextSize = new System.Drawing.Size(180, 20);
            this.layoutControlItem71.TextVisible = true;
            // 
            // layoutControlItem70
            // 
            this.layoutControlItem70.Control = this.dateEdit4;
            this.layoutControlItem70.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItem70.MinSize = new System.Drawing.Size(155, 30);
            this.layoutControlItem70.Name = "layoutControlItem70";
            this.layoutControlItem70.Padding = new DevExpress.XtraLayout.Utils.Padding(5, 5, 5, 5);
            this.layoutControlItem70.Size = new System.Drawing.Size(345, 31);
            this.layoutControlItem70.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem70.Spacing = new DevExpress.XtraLayout.Utils.Padding(0, 0, 0, 0);
            this.layoutControlItem70.Text = "Data da �ltima assembl�ia realizada";
            this.layoutControlItem70.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem70.TextSize = new System.Drawing.Size(175, 20);
            this.layoutControlItem70.TextToControlDistance = 0;
            this.layoutControlItem70.TextVisible = true;
            // 
            // layoutControlItem67
            // 
            this.layoutControlItem67.Control = this.checkEdit8;
            this.layoutControlItem67.Location = new System.Drawing.Point(0, 317);
            this.layoutControlItem67.Name = "layoutControlItem67";
            this.layoutControlItem67.Padding = new DevExpress.XtraLayout.Utils.Padding(5, 5, 5, 5);
            this.layoutControlItem67.Size = new System.Drawing.Size(126, 30);
            this.layoutControlItem67.Spacing = new DevExpress.XtraLayout.Utils.Padding(0, 0, 0, 0);
            this.layoutControlItem67.Text = "layoutControlItem67";
            this.layoutControlItem67.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem67.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem67.TextToControlDistance = 0;
            this.layoutControlItem67.TextVisible = false;
            // 
            // layoutControlItem93
            // 
            this.layoutControlItem93.Control = this.checkEdit7;
            this.layoutControlItem93.Location = new System.Drawing.Point(551, 317);
            this.layoutControlItem93.Name = "layoutControlItem93";
            this.layoutControlItem93.Padding = new DevExpress.XtraLayout.Utils.Padding(5, 5, 5, 5);
            this.layoutControlItem93.Size = new System.Drawing.Size(121, 30);
            this.layoutControlItem93.Spacing = new DevExpress.XtraLayout.Utils.Padding(0, 0, 0, 0);
            this.layoutControlItem93.Text = "layoutControlItem93";
            this.layoutControlItem93.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem93.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem93.TextToControlDistance = 0;
            this.layoutControlItem93.TextVisible = false;
            // 
            // layoutControlGroup35
            // 
            this.layoutControlGroup35.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlGroup36,
            this.layoutControlGroup39});
            this.layoutControlGroup35.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroup35.Name = "layoutControlGroup35";
            this.layoutControlGroup35.Padding = new DevExpress.XtraLayout.Utils.Padding(0, 0, 0, 0);
            this.layoutControlGroup35.Size = new System.Drawing.Size(672, 347);
            this.layoutControlGroup35.Spacing = new DevExpress.XtraLayout.Utils.Padding(2, 2, 2, 2);
            this.layoutControlGroup35.Text = "&7-Internet";
            this.layoutControlGroup35.TextVisible = true;
            // 
            // layoutControlGroup36
            // 
            this.layoutControlGroup36.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItem75,
            this.layoutControlItem74,
            this.layoutControlGroup37,
            this.layoutControlGroup38});
            this.layoutControlGroup36.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroup36.Name = "layoutControlGroup36";
            this.layoutControlGroup36.Padding = new DevExpress.XtraLayout.Utils.Padding(0, 0, 0, 0);
            this.layoutControlGroup36.Size = new System.Drawing.Size(672, 154);
            this.layoutControlGroup36.Spacing = new DevExpress.XtraLayout.Utils.Padding(2, 2, 2, 2);
            this.layoutControlGroup36.Text = ":: Internet ::";
            this.layoutControlGroup36.TextVisible = false;
            // 
            // layoutControlItem75
            // 
            this.layoutControlItem75.Control = this.chkExportarInternet;
            this.layoutControlItem75.Location = new System.Drawing.Point(92, 0);
            this.layoutControlItem75.MaxSize = new System.Drawing.Size(0, 29);
            this.layoutControlItem75.MinSize = new System.Drawing.Size(20, 29);
            this.layoutControlItem75.Name = "layoutControlItem75";
            this.layoutControlItem75.Padding = new DevExpress.XtraLayout.Utils.Padding(5, 5, 5, 5);
            this.layoutControlItem75.Size = new System.Drawing.Size(256, 148);
            this.layoutControlItem75.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem75.Spacing = new DevExpress.XtraLayout.Utils.Padding(0, 0, 55, 0);
            this.layoutControlItem75.Text = "layoutControlItem75";
            this.layoutControlItem75.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem75.TextToControlDistance = 0;
            this.layoutControlItem75.TextVisible = false;
            // 
            // layoutControlItem74
            // 
            this.layoutControlItem74.Control = this.pictureEdit1;
            this.layoutControlItem74.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItem74.MinSize = new System.Drawing.Size(30, 30);
            this.layoutControlItem74.Name = "layoutControlItem74";
            this.layoutControlItem74.Padding = new DevExpress.XtraLayout.Utils.Padding(5, 5, 5, 5);
            this.layoutControlItem74.Size = new System.Drawing.Size(92, 148);
            this.layoutControlItem74.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem74.Spacing = new DevExpress.XtraLayout.Utils.Padding(0, 0, 0, 0);
            this.layoutControlItem74.Text = "layoutControlItem74";
            this.layoutControlItem74.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem74.TextToControlDistance = 0;
            this.layoutControlItem74.TextVisible = false;
            // 
            // layoutControlGroup37
            // 
            this.layoutControlGroup37.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItem79});
            this.layoutControlGroup37.Location = new System.Drawing.Point(348, 84);
            this.layoutControlGroup37.Name = "layoutControlGroup37";
            this.layoutControlGroup37.Padding = new DevExpress.XtraLayout.Utils.Padding(0, 0, 0, 0);
            this.layoutControlGroup37.Size = new System.Drawing.Size(318, 64);
            this.layoutControlGroup37.Spacing = new DevExpress.XtraLayout.Utils.Padding(2, 2, 2, 2);
            this.layoutControlGroup37.Text = ":: Modelo de Balancete ::";
            this.layoutControlGroup37.TextVisible = true;
            // 
            // layoutControlItem79
            // 
            this.layoutControlItem79.Control = this.rgpBalanceteInternet;
            this.layoutControlItem79.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItem79.MinSize = new System.Drawing.Size(20, 40);
            this.layoutControlItem79.Name = "layoutControlItem79";
            this.layoutControlItem79.Padding = new DevExpress.XtraLayout.Utils.Padding(5, 5, 5, 5);
            this.layoutControlItem79.Size = new System.Drawing.Size(312, 40);
            this.layoutControlItem79.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem79.Spacing = new DevExpress.XtraLayout.Utils.Padding(0, 0, 0, 0);
            this.layoutControlItem79.Text = "layoutControlItem79";
            this.layoutControlItem79.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem79.TextToControlDistance = 0;
            this.layoutControlItem79.TextVisible = false;
            // 
            // layoutControlGroup38
            // 
            this.layoutControlGroup38.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItem77,
            this.layoutControlItem78});
            this.layoutControlGroup38.Location = new System.Drawing.Point(348, 0);
            this.layoutControlGroup38.Name = "layoutControlGroup38";
            this.layoutControlGroup38.Padding = new DevExpress.XtraLayout.Utils.Padding(0, 0, 0, 0);
            this.layoutControlGroup38.Size = new System.Drawing.Size(318, 84);
            this.layoutControlGroup38.Spacing = new DevExpress.XtraLayout.Utils.Padding(2, 2, 2, 2);
            this.layoutControlGroup38.Text = "::Par�metros ::";
            this.layoutControlGroup38.TextVisible = true;
            // 
            // layoutControlItem77
            // 
            this.layoutControlItem77.Control = this.chkAcordosOnLine;
            this.layoutControlItem77.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItem77.Name = "layoutControlItem77";
            this.layoutControlItem77.Padding = new DevExpress.XtraLayout.Utils.Padding(5, 5, 5, 5);
            this.layoutControlItem77.Size = new System.Drawing.Size(312, 30);
            this.layoutControlItem77.Spacing = new DevExpress.XtraLayout.Utils.Padding(0, 0, 0, 0);
            this.layoutControlItem77.Text = "layoutControlItem77";
            this.layoutControlItem77.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem77.TextToControlDistance = 0;
            this.layoutControlItem77.TextVisible = false;
            // 
            // layoutControlItem78
            // 
            this.layoutControlItem78.Control = this.chkLiberarBalanceteInternet;
            this.layoutControlItem78.Location = new System.Drawing.Point(0, 30);
            this.layoutControlItem78.Name = "layoutControlItem78";
            this.layoutControlItem78.Padding = new DevExpress.XtraLayout.Utils.Padding(5, 5, 5, 5);
            this.layoutControlItem78.Size = new System.Drawing.Size(312, 30);
            this.layoutControlItem78.Spacing = new DevExpress.XtraLayout.Utils.Padding(0, 0, 0, 0);
            this.layoutControlItem78.Text = "layoutControlItem78";
            this.layoutControlItem78.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem78.TextToControlDistance = 0;
            this.layoutControlItem78.TextVisible = false;
            // 
            // layoutControlGroup39
            // 
            this.layoutControlGroup39.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.tcgEditorInternet});
            this.layoutControlGroup39.Location = new System.Drawing.Point(0, 154);
            this.layoutControlGroup39.Name = "layoutControlGroup39";
            this.layoutControlGroup39.Padding = new DevExpress.XtraLayout.Utils.Padding(0, 0, 0, 0);
            this.layoutControlGroup39.Size = new System.Drawing.Size(672, 193);
            this.layoutControlGroup39.Spacing = new DevExpress.XtraLayout.Utils.Padding(2, 2, 2, 2);
            this.layoutControlGroup39.Text = ":: Observa��o ::";
            this.layoutControlGroup39.TextVisible = true;
            // 
            // tcgEditorInternet
            // 
            this.tcgEditorInternet.Location = new System.Drawing.Point(0, 0);
            this.tcgEditorInternet.Name = "tcgEditorInternet";
            this.tcgEditorInternet.Padding = new DevExpress.XtraLayout.Utils.Padding(5, 5, 5, 5);
            this.tcgEditorInternet.SelectedTabPage = this.layoutControlGroup40;
            this.tcgEditorInternet.SelectedTabPageIndex = 0;
            this.tcgEditorInternet.Size = new System.Drawing.Size(666, 169);
            this.tcgEditorInternet.Spacing = new DevExpress.XtraLayout.Utils.Padding(1, 1, 1, 1);
            this.tcgEditorInternet.TabPages.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlGroup40,
            this.lcgVisualizadorInternet});
            this.tcgEditorInternet.Text = "tcgEditorInternet";
            this.tcgEditorInternet.TextLocation = DevExpress.Utils.Locations.Default;
            this.tcgEditorInternet.TextVisible = true;
            this.tcgEditorInternet.SelectedPageChanged += new DevExpress.XtraLayout.LayoutTabPageChangedEventHandler(this.tcgEditorInternet_SelectedPageChanged);
            // 
            // layoutControlGroup40
            // 
            this.layoutControlGroup40.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.lciEditorInternet});
            this.layoutControlGroup40.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroup40.Name = "layoutControlGroup40";
            this.layoutControlGroup40.Padding = new DevExpress.XtraLayout.Utils.Padding(0, 0, 0, 0);
            this.layoutControlGroup40.Size = new System.Drawing.Size(645, 127);
            this.layoutControlGroup40.Spacing = new DevExpress.XtraLayout.Utils.Padding(2, 2, 2, 2);
            this.layoutControlGroup40.Text = "&Editor";
            this.layoutControlGroup40.TextVisible = true;
            // 
            // lciEditorInternet
            // 
            this.lciEditorInternet.Control = this.memEditorInternet;
            this.lciEditorInternet.Location = new System.Drawing.Point(0, 0);
            this.lciEditorInternet.Name = "lciEditorInternet";
            this.lciEditorInternet.Padding = new DevExpress.XtraLayout.Utils.Padding(5, 5, 5, 5);
            this.lciEditorInternet.Size = new System.Drawing.Size(645, 127);
            this.lciEditorInternet.Spacing = new DevExpress.XtraLayout.Utils.Padding(0, 0, 0, 0);
            this.lciEditorInternet.Text = "lciEditorInternet";
            this.lciEditorInternet.TextSize = new System.Drawing.Size(0, 0);
            this.lciEditorInternet.TextToControlDistance = 0;
            this.lciEditorInternet.TextVisible = false;
            // 
            // lcgVisualizadorInternet
            // 
            this.lcgVisualizadorInternet.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.lciVisualizadorInternet});
            this.lcgVisualizadorInternet.Location = new System.Drawing.Point(0, 0);
            this.lcgVisualizadorInternet.Name = "lcgVisualizadorInternet";
            this.lcgVisualizadorInternet.Padding = new DevExpress.XtraLayout.Utils.Padding(0, 0, 0, 0);
            this.lcgVisualizadorInternet.Size = new System.Drawing.Size(645, 127);
            this.lcgVisualizadorInternet.Spacing = new DevExpress.XtraLayout.Utils.Padding(2, 2, 2, 2);
            this.lcgVisualizadorInternet.Text = "&Pr�-Visualiza��o";
            this.lcgVisualizadorInternet.TextVisible = true;
            // 
            // lciVisualizadorInternet
            // 
            this.lciVisualizadorInternet.Control = this.WebBrowser;
            this.lciVisualizadorInternet.Location = new System.Drawing.Point(0, 0);
            this.lciVisualizadorInternet.Name = "lciVisualizadorInternet";
            this.lciVisualizadorInternet.Padding = new DevExpress.XtraLayout.Utils.Padding(5, 5, 5, 5);
            this.lciVisualizadorInternet.Size = new System.Drawing.Size(645, 127);
            this.lciVisualizadorInternet.Spacing = new DevExpress.XtraLayout.Utils.Padding(0, 0, 0, 0);
            this.lciVisualizadorInternet.Text = "lciVisualizadorInternet";
            this.lciVisualizadorInternet.TextSize = new System.Drawing.Size(0, 0);
            this.lciVisualizadorInternet.TextToControlDistance = 0;
            this.lciVisualizadorInternet.TextVisible = false;
            // 
            // layoutControlGroup42
            // 
            this.layoutControlGroup42.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItem76,
            this.layoutControlItem80});
            this.layoutControlGroup42.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroup42.Name = "layoutControlGroup42";
            this.layoutControlGroup42.Padding = new DevExpress.XtraLayout.Utils.Padding(0, 0, 0, 0);
            this.layoutControlGroup42.Size = new System.Drawing.Size(672, 347);
            this.layoutControlGroup42.Spacing = new DevExpress.XtraLayout.Utils.Padding(2, 2, 2, 2);
            this.layoutControlGroup42.Text = "&8-Hist�rico";
            this.layoutControlGroup42.TextVisible = true;
            // 
            // layoutControlItem76
            // 
            this.layoutControlItem76.Control = this.grdHistoricos;
            this.layoutControlItem76.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItem76.Name = "layoutControlItem76";
            this.layoutControlItem76.Padding = new DevExpress.XtraLayout.Utils.Padding(5, 5, 5, 5);
            this.layoutControlItem76.Size = new System.Drawing.Size(672, 314);
            this.layoutControlItem76.Spacing = new DevExpress.XtraLayout.Utils.Padding(0, 0, 0, 0);
            this.layoutControlItem76.Text = "layoutControlItem76";
            this.layoutControlItem76.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem76.TextToControlDistance = 0;
            this.layoutControlItem76.TextVisible = false;
            // 
            // layoutControlItem80
            // 
            this.layoutControlItem80.Control = this.bnvHistoricos;
            this.layoutControlItem80.Location = new System.Drawing.Point(0, 314);
            this.layoutControlItem80.MinSize = new System.Drawing.Size(110, 32);
            this.layoutControlItem80.Name = "layoutControlItem80";
            this.layoutControlItem80.Padding = new DevExpress.XtraLayout.Utils.Padding(5, 5, 5, 5);
            this.layoutControlItem80.Size = new System.Drawing.Size(672, 33);
            this.layoutControlItem80.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem80.Spacing = new DevExpress.XtraLayout.Utils.Padding(0, 0, 0, 0);
            this.layoutControlItem80.Text = "layoutControlItem80";
            this.layoutControlItem80.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem80.TextToControlDistance = 0;
            this.layoutControlItem80.TextVisible = false;
            // 
            // layoutControlGroup46
            // 
            this.layoutControlGroup46.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItem99,
            this.layoutControlItem100});
            this.layoutControlGroup46.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroup46.Name = "layoutControlGroup46";
            this.layoutControlGroup46.Padding = new DevExpress.XtraLayout.Utils.Padding(0, 0, 0, 0);
            this.layoutControlGroup46.Size = new System.Drawing.Size(672, 347);
            this.layoutControlGroup46.Spacing = new DevExpress.XtraLayout.Utils.Padding(2, 2, 2, 2);
            this.layoutControlGroup46.Text = "Status";
            this.layoutControlGroup46.TextVisible = true;
            // 
            // layoutControlItem99
            // 
            this.layoutControlItem99.Control = this.spinEdit2;
            this.layoutControlItem99.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItem99.Name = "layoutControlItem99";
            this.layoutControlItem99.Padding = new DevExpress.XtraLayout.Utils.Padding(5, 5, 5, 5);
            this.layoutControlItem99.Size = new System.Drawing.Size(336, 347);
            this.layoutControlItem99.Spacing = new DevExpress.XtraLayout.Utils.Padding(0, 0, 0, 0);
            this.layoutControlItem99.Text = "layoutControlItem99";
            this.layoutControlItem99.TextSize = new System.Drawing.Size(99, 20);
            this.layoutControlItem99.TextVisible = true;
            // 
            // layoutControlItem100
            // 
            this.layoutControlItem100.Control = this.labelControl1;
            this.layoutControlItem100.Location = new System.Drawing.Point(336, 0);
            this.layoutControlItem100.Name = "layoutControlItem100";
            this.layoutControlItem100.Padding = new DevExpress.XtraLayout.Utils.Padding(5, 5, 5, 5);
            this.layoutControlItem100.Size = new System.Drawing.Size(336, 347);
            this.layoutControlItem100.Spacing = new DevExpress.XtraLayout.Utils.Padding(0, 0, 0, 0);
            this.layoutControlItem100.Text = "layoutControlItem100";
            this.layoutControlItem100.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem100.TextToControlDistance = 0;
            this.layoutControlItem100.TextVisible = false;
            // 
            // layoutControlItem98
            // 
            this.layoutControlItem98.Control = this.lookUpEdit4;
            this.layoutControlItem98.Location = new System.Drawing.Point(498, 0);
            this.layoutControlItem98.Name = "layoutControlItem98";
            this.layoutControlItem98.Padding = new DevExpress.XtraLayout.Utils.Padding(5, 5, 5, 5);
            this.layoutControlItem98.Size = new System.Drawing.Size(195, 31);
            this.layoutControlItem98.Spacing = new DevExpress.XtraLayout.Utils.Padding(0, 0, 0, 0);
            this.layoutControlItem98.Text = "Origem";
            this.layoutControlItem98.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem98.TextSize = new System.Drawing.Size(35, 20);
            this.layoutControlItem98.TextVisible = true;
            // 
            // cONDOMINIOSTableAdapter
            // 
            this.cONDOMINIOSTableAdapter.ClearBeforeFill = true;
            // 
            // bLOCOSTableAdapter
            // 
            this.bLOCOSTableAdapter.ClearBeforeFill = true;
            // 
            // cIDADESTableAdapter
            // 
            this.cIDADESTableAdapter.ClearBeforeFill = true;
            // 
            // aPARTAMENTOSTableAdapter
            // 
            this.aPARTAMENTOSTableAdapter.ClearBeforeFill = true;
            // 
            // bANCOSTableAdapter
            // 
            this.bANCOSTableAdapter.ClearBeforeFill = true;
            // 
            // aPLICACOESTableAdapter
            // 
            this.aPLICACOESTableAdapter.ClearBeforeFill = true;
            // 
            // fKSALDOSAPLICACOESBindingSource
            // 
            this.fKSALDOSAPLICACOESBindingSource.DataSource = this.fKAPLICACOESCONDOMINIOSBindingSource;
            // 
            // sALDOSTableAdapter
            // 
            this.sALDOSTableAdapter.ClearBeforeFill = true;
            // 
            // aDVOGADOSTableAdapter
            // 
            this.aDVOGADOSTableAdapter.ClearBeforeFill = true;
                        
            // 
            // cORPODIRETIVOTableAdapter
            // 
            this.cORPODIRETIVOTableAdapter.ClearBeforeFill = true;
            // 
            // eMPRESASTableAdapter
            // 
            this.eMPRESASTableAdapter.ClearBeforeFill = true;
            // 
            // cCondominiosCampos
            // 
            this.Autofill = false;
            this.Controls.Add(this.LayoutControl);
            this.Cursor = System.Windows.Forms.Cursors.Default;
            this.ImagemG = ((System.Drawing.Image)(resources.GetObject("$this.ImagemG")));
            this.ImagemP = ((System.Drawing.Image)(resources.GetObject("$this.ImagemP")));
            this.LookAndFeel.UseDefaultLookAndFeel = false;
            this.Name = "cCondominiosCampos";
            this.Size = new System.Drawing.Size(711, 480);
            this.ToolTipController_F.SetSuperTip(this, null);
            this.Load += new System.EventHandler(this.cCondominiosCampos_Load);
            this.cargaInicial += new System.EventHandler(this.cCondominiosCampos_cargaInicial);
            this.cargaFinal += new System.EventHandler(this.cCondominiosCampos_cargaFinal);
            this.Controls.SetChildIndex(this.LayoutControl, 0);
            ((System.ComponentModel.ISupportInitialize)(this.BarManager_F)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.BarAndDockingController_F)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.BindingSource_F)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.StyleController_F)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.grvSaldos)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.grdAplicacoes)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.fKAPLICACOESCONDOMINIOSBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.grvAplicacoes)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dCondominiosCampos)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LayoutControl)).EndInit();
            this.LayoutControl.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.spinEdit2.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lookUpEdit4.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.eMPRESASBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCodigoFolha2.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit12.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit7.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCodigoFolha1.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit7.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit14.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit13.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lookUpEdit1.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.bANCOSBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCNPJ_C.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DataAdesao_C.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkGas_C.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTotalUnidades_C.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtConta_C.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtAgencia_C.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTelefone_C.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtSindico_C.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.grdCorpoDiretivo)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cORPODIRETIVOBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.grvCorpoDiretivo)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.grdHistoricos)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.fKHISTORICOSCONDOMINIOSBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.grvHistoricos)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckEdit1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoExEdit1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.rgpBalanceteInternet.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkLiberarBalanceteInternet.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkAcordosOnLine.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.memEditorInternet.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureEdit1.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkExportarInternet.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lookUpEdit6.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.aDVOGADOSBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lookUpEdit5.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.spinEdit1.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEdit4.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit10.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit9.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit6.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEdit3.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEdit2.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEdit1.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit8.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lkpSeguradoras.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sEGURADORASBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lookUpEdit3.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.aUDITORESBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lookUpEdit2.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lkpCorretora.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cORRETORASEGUROSBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.rdgBoletoCorreio.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.rdgCobrancaCorreio.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.rdgAssembleiaCorreio.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.clcCS.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.clcAR.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtRegra.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkRepassaCorreio.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.spinEdit8.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.spinEdit7.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.spinEdit6.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.spinEdit5.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.spinEdit4.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.spinEdit3.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtPerContabilFim.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.spePerContabilInicio.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.speVencimento.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.calcEdit9.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.calcEdit8.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.calcEdit7.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.calcEdit6.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.calcEdit5.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radioGroup4.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit5.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit11.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit10.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.rdgFR.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit9.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit8.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lkpBancos.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.speApartamentos.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.memoEdit1.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.speBlocos.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.grdApartamentos)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.fKAPARTAMENTOSBLOCOSBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.fKBLOCOSCONDOMINIOSBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.grvApartamentos)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.grdBlocos)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.grvBlocos)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radioGroup2.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit4.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit3.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.calcEdit4.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.calcEdit3.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.calcEdit2.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.calcEdit1.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit1.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radioGroup1.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit1.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit2.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lkpCidades.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cIDADESBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lkpUF.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit5.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit4.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit3.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.bnvHistoricos)).EndInit();
            this.bnvHistoricos.ResumeLayout(false);
            this.bnvHistoricos.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.bnvCorpoDiretivo)).EndInit();
            this.bnvCorpoDiretivo.ResumeLayout(false);
            this.bnvCorpoDiretivo.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.bnvBlocos)).EndInit();
            this.bnvBlocos.ResumeLayout(false);
            this.bnvBlocos.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.bnvApartamentos)).EndInit();
            this.bnvApartamentos.ResumeLayout(false);
            this.bnvApartamentos.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit2.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCodcon.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.bnvAplicacoes)).EndInit();
            this.bnvAplicacoes.ResumeLayout(false);
            this.bnvAplicacoes.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.LayoutControlGroup)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tcgCondominio)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup43)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup44)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem95)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem96)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup45)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem94)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem97)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lcgBasico)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem8)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem88)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem90)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup27)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem83)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem82)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup41)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem85)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem86)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem91)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem84)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem92)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem89)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem87)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem11)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem10)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem12)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup10)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem13)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem14)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup9)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem20)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem19)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem16)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem15)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup8)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem17)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem18)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup11)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem22)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem25)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem9)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup12)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem21)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem27)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem23)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup13)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem24)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem81)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup14)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup15)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem26)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem28)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem29)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem30)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem32)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem33)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem34)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem35)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup16)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem31)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lciValorFR)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup17)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem37)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem36)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem39)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup19)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem42)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem40)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup18)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem43)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem44)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup24)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem41)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem48)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup25)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem49)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem50)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem51)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem52)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem53)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup26)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem54)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lcgCorreio)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup20)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem45)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem47)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem46)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup21)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem55)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup22)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem56)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup23)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem57)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup28)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem38)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem58)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup29)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup30)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem60)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem61)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup31)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem62)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem59)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup32)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem64)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem65)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem63)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup33)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem72)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem73)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem69)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem66)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem68)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup34)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem71)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem70)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem67)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem93)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup35)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup36)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem75)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem74)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup37)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem79)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup38)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem77)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem78)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup39)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tcgEditorInternet)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup40)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lciEditorInternet)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lcgVisualizadorInternet)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lciVisualizadorInternet)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup42)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem76)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem80)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup46)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem99)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem100)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem98)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.fKSALDOSAPLICACOESBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.fKBLOCOSCONDOMINIOSBindingSource1)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraLayout.LayoutControl LayoutControl;
        private DevExpress.XtraLayout.LayoutControlGroup LayoutControlGroup;
        private DevExpress.XtraEditors.TextEdit txtCodcon;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem1;
        private DevExpress.XtraEditors.TextEdit textEdit3;
        private DevExpress.XtraEditors.TextEdit textEdit2;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem2;
        private DevExpress.XtraLayout.TabbedControlGroup tcgCondominio;
        private DevExpress.XtraLayout.LayoutControlGroup lcgBasico;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem3;
        private DevExpress.XtraEditors.LookUpEdit lkpCidades;
        private DevExpress.XtraEditors.TextEdit textEdit5;
        private DevExpress.XtraEditors.TextEdit textEdit4;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup2;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem6;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup4;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup3;
        private dCondominiosCampos dCondominiosCampos;
        private Cadastros.Condominios.dCondominiosCamposTableAdapters.CONDOMINIOSTableAdapter cONDOMINIOSTableAdapter;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup5;
        private DevExpress.XtraEditors.CheckEdit checkEdit1;
        private DevExpress.XtraEditors.TextEdit textEdit1;
        private DevExpress.XtraEditors.CheckEdit checkEdit2;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem11;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem10;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem12;
        private DevExpress.XtraEditors.CalcEdit calcEdit3;
        private DevExpress.XtraEditors.CalcEdit calcEdit2;
        private DevExpress.XtraEditors.CalcEdit calcEdit1;
        private DevExpress.XtraEditors.CalcEdit calcEdit4;
        private DevExpress.XtraEditors.CheckEdit checkEdit4;
        private DevExpress.XtraEditors.CheckEdit checkEdit3;
        private DevExpress.XtraEditors.RadioGroup radioGroup1;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup10;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup6;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem13;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem14;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup9;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup7;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem16;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem15;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup8;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem17;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem18;
        private DevExpress.XtraEditors.RadioGroup radioGroup2;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem20;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem4;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem8;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem7;
        private DevExpress.XtraGrid.GridControl grdApartamentos;
        private DevExpress.XtraGrid.Views.Grid.GridView grvApartamentos;
        private DevExpress.XtraGrid.GridControl grdBlocos;
        private DevExpress.XtraGrid.Views.Grid.GridView grvBlocos;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem22;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup11;
        private DevExpress.XtraEditors.SpinEdit speBlocos;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem25;
        private DevExpress.XtraEditors.MemoEdit memoEdit1;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem5;
        private System.Windows.Forms.BindingSource fKBLOCOSCONDOMINIOSBindingSource;
        private DevExpress.XtraGrid.Columns.GridColumn colBLOCodigo;
        private DevExpress.XtraGrid.Columns.GridColumn colBLONome;
        private Cadastros.Condominios.dCondominiosCamposTableAdapters.BLOCOSTableAdapter bLOCOSTableAdapter;
        private System.Windows.Forms.BindingSource cIDADESBindingSource;
        private Cadastros.Condominios.dCondominiosCamposTableAdapters.CIDADESTableAdapter cIDADESTableAdapter;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem19;
        private DevExpress.XtraEditors.SpinEdit speApartamentos;
        private System.Windows.Forms.BindingSource fKAPARTAMENTOSBLOCOSBindingSource;
        private DevExpress.XtraGrid.Columns.GridColumn colAPTNumero;
        private DevExpress.XtraGrid.Columns.GridColumn colAPTFracaoIdeal;
        private Cadastros.Condominios.dCondominiosCamposTableAdapters.APARTAMENTOSTableAdapter aPARTAMENTOSTableAdapter;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup13;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup14;
        private System.Windows.Forms.Label label1;
        private DevExpress.XtraEditors.TextEdit textEdit9;
        private DevExpress.XtraEditors.TextEdit textEdit8;
        private DevExpress.XtraEditors.LookUpEdit lkpBancos;
        private DevExpress.XtraEditors.TextEdit textEdit10;
        private DevExpress.XtraEditors.RadioGroup rdgFR;
        private System.Windows.Forms.Label label2;
        private DevExpress.XtraEditors.TextEdit textEdit11;
        private DevExpress.XtraEditors.CheckEdit checkEdit5;
        private System.Windows.Forms.BindingSource bANCOSBindingSource;
        private Cadastros.Condominios.dCondominiosCamposTableAdapters.BANCOSTableAdapter bANCOSTableAdapter;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup15;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem26;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem28;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem29;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem30;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem32;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem33;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem34;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem35;
        private DevExpress.XtraEditors.RadioGroup radioGroup4;
        private DevExpress.XtraEditors.CalcEdit calcEdit5;
        private DevExpress.XtraEditors.CalcEdit calcEdit6;
        private DevExpress.XtraEditors.CalcEdit calcEdit7;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup16;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem31;
        private DevExpress.XtraLayout.LayoutControlItem lciValorFR;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup17;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem37;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem36;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem39;
        private DevExpress.XtraEditors.CalcEdit calcEdit9;
        private DevExpress.XtraEditors.CalcEdit calcEdit8;
        private DevExpress.XtraEditors.SpinEdit speVencimento;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup19;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem42;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup18;
        private DevExpress.XtraEditors.SpinEdit spePerContabilInicio;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup24;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem41;
        private DevExpress.XtraEditors.TextEdit txtPerContabilFim;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem48;
        private DevExpress.XtraEditors.SpinEdit spinEdit3;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem49;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup25;
        private DevExpress.XtraEditors.SpinEdit spinEdit5;
        private DevExpress.XtraEditors.SpinEdit spinEdit4;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem50;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem51;
        private DevExpress.XtraEditors.SpinEdit spinEdit7;
        private DevExpress.XtraEditors.SpinEdit spinEdit6;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem52;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem53;
        private DevExpress.XtraEditors.SpinEdit spinEdit8;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup26;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem54;
        private DevExpress.XtraEditors.CheckEdit chkRepassaCorreio;
        private DevExpress.XtraLayout.LayoutControlGroup lcgCorreio;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem40;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem43;
        private DevExpress.XtraEditors.TextEdit txtRegra;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem44;
        private DevExpress.XtraEditors.CalcEdit clcCS;
        private DevExpress.XtraEditors.CalcEdit clcAR;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup20;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem45;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem47;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem46;
        private DevExpress.XtraEditors.RadioGroup rdgAssembleiaCorreio;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup21;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem55;
        private DevExpress.XtraEditors.RadioGroup rdgBoletoCorreio;
        private DevExpress.XtraEditors.RadioGroup rdgCobrancaCorreio;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup22;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem56;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup23;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem57;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup12;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem21;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem27;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup28;
        private DevExpress.XtraGrid.GridControl grdAplicacoes;
        private DevExpress.XtraGrid.Views.Grid.GridView grvAplicacoes;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem38;
        private System.Windows.Forms.BindingSource fKAPLICACOESCONDOMINIOSBindingSource;
        private Cadastros.Condominios.dCondominiosCamposTableAdapters.APLICACOESTableAdapter aPLICACOESTableAdapter;
        private DevExpress.XtraGrid.Views.Grid.GridView grvSaldos;
        private DevExpress.XtraGrid.Columns.GridColumn colSLDNome;
        private DevExpress.XtraGrid.Columns.GridColumn colSLDValorSaldoAnterior;
        private DevExpress.XtraGrid.Columns.GridColumn colSLDValorSaldoAtual;
        private System.Windows.Forms.BindingNavigator bnvAplicacoes;
        private System.Windows.Forms.ToolStripButton btnAddAplicacao;
        private System.Windows.Forms.ToolStripButton btnAddSubAplicacao;
        private System.Windows.Forms.ToolStripButton btnDelAplicacao;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem58;
        private System.Windows.Forms.ToolStripButton btnEditAplicacao;
        private System.Windows.Forms.BindingSource fKSALDOSAPLICACOESBindingSource;
        private Cadastros.Condominios.dCondominiosCamposTableAdapters.SALDOSTableAdapter sALDOSTableAdapter;
        private DevExpress.XtraGrid.Columns.GridColumn colAPLNome;
        private DevExpress.XtraGrid.Columns.GridColumn colAPLConta;
        private DevExpress.XtraGrid.Columns.GridColumn colAPLDataSaldoAnterior;
        private DevExpress.XtraGrid.Columns.GridColumn colAPLValorSaldoAnterior;
        private DevExpress.XtraGrid.Columns.GridColumn colAPLDataSaldoAtual;
        private DevExpress.XtraGrid.Columns.GridColumn colAPLValorSaldoAtual;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator2;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator1;
        private System.Windows.Forms.ToolStripButton btnPrintAplicacao;
        private DevExpress.XtraEditors.DateEdit dateEdit1;
        private DevExpress.XtraEditors.LookUpEdit lkpSeguradoras;
        private DevExpress.XtraEditors.LookUpEdit lookUpEdit3;
        private DevExpress.XtraEditors.LookUpEdit lookUpEdit2;
        private DevExpress.XtraEditors.LookUpEdit lkpCorretora;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup29;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem60;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem62;
        private DevExpress.XtraEditors.CheckEdit checkEdit10;
        private DevExpress.XtraEditors.CheckEdit checkEdit9;
        private DevExpress.XtraEditors.CheckEdit checkEdit8;
        private DevExpress.XtraEditors.CheckEdit checkEdit6;
        private DevExpress.XtraEditors.DateEdit dateEdit3;
        private DevExpress.XtraEditors.DateEdit dateEdit2;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem64;
        private DevExpress.XtraEditors.LookUpEdit lookUpEdit6;
        private DevExpress.XtraEditors.LookUpEdit lookUpEdit5;
        private DevExpress.XtraEditors.SpinEdit spinEdit1;
        private DevExpress.XtraEditors.DateEdit dateEdit4;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup30;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup31;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem59;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup32;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem65;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem63;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup33;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem72;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem69;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem61;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem73;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem66;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem68;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup34;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem71;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem67;
        private System.Windows.Forms.BindingSource aDVOGADOSBindingSource;
        private System.Windows.Forms.BindingSource aUDITORESBindingSource;
        private Cadastros.Condominios.dCondominiosCamposTableAdapters.ADVOGADOSTableAdapter aDVOGADOSTableAdapter;
        
        private System.Windows.Forms.BindingSource sEGURADORASBindingSource;
        private System.Windows.Forms.BindingSource cORRETORASEGUROSBindingSource;
        
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem70;
        
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup35;
        private DevExpress.XtraEditors.CheckEdit chkExportarInternet;
        private DevExpress.XtraEditors.CheckEdit chkLiberarBalanceteInternet;
        private DevExpress.XtraEditors.CheckEdit chkAcordosOnLine;
        private DevExpress.XtraEditors.MemoEdit memEditorInternet;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup36;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem75;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem77;
        private DevExpress.XtraEditors.PictureEdit pictureEdit1;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem74;
        private DevExpress.XtraEditors.RadioGroup rgpBalanceteInternet;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem79;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup37;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup38;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem78;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup39;
        private DevExpress.XtraLayout.TabbedControlGroup tcgEditorInternet;
        private DevExpress.XtraLayout.LayoutControlGroup lcgVisualizadorInternet;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup40;
        private DevExpress.XtraLayout.LayoutControlItem lciEditorInternet;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup42;
        private System.Windows.Forms.WebBrowser WebBrowser;
        private DevExpress.XtraLayout.LayoutControlItem lciVisualizadorInternet;
        private System.Windows.Forms.BindingNavigator bnvHistoricos;
        private DevExpress.XtraGrid.GridControl grdHistoricos;
        private DevExpress.XtraGrid.Views.Grid.GridView grvHistoricos;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem76;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem80;
        private System.Windows.Forms.ToolStripButton btnAddHistorico;
        private System.Windows.Forms.ToolStripButton btnEditHistorico;
        private System.Windows.Forms.ToolStripButton btnPrintHistorico;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator3;
        private System.Windows.Forms.BindingSource fKHISTORICOSCONDOMINIOSBindingSource;
        private DevExpress.XtraGrid.Columns.GridColumn colHSTAssunto;
        private DevExpress.XtraGrid.Columns.GridColumn colHSTDescricao;
        private DevExpress.XtraGrid.Columns.GridColumn colHSTCritico;
        private DevExpress.XtraGrid.Columns.GridColumn colHSTData;
        
        private DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit repositoryItemCheckEdit1;
        private DevExpress.XtraEditors.Repository.RepositoryItemMemoExEdit repositoryItemMemoExEdit1;
        private System.Windows.Forms.ToolStripButton btnDelHistorico;
        private System.Windows.Forms.BindingNavigator bnvBlocos;
        private System.Windows.Forms.ToolStripButton btnAddBlocos;
        private System.Windows.Forms.ToolStripButton btnDelBlocos;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator5;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator6;
        private System.Windows.Forms.ToolStripButton btnPostBlocos;
        private System.Windows.Forms.ToolStripButton btnCancelBlocos;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem9;
        private System.Windows.Forms.ToolStripButton btnPrintBlocos;
        private System.Windows.Forms.BindingNavigator bnvApartamentos;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem23;
        private System.Windows.Forms.ToolStripButton btnAddApto;
        private System.Windows.Forms.ToolStripButton btnEditApto;
        private System.Windows.Forms.ToolStripButton btnPrintApto;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator4;
        private System.Windows.Forms.ToolStripButton btnDelApto;
        private System.Windows.Forms.ToolStripButton btnEditBlocos;
        private DevExpress.XtraGrid.Columns.GridColumn colSLD;
        private DevExpress.XtraGrid.Columns.GridColumn colAPL;
        private DevExpress.XtraGrid.GridControl grdCorpoDiretivo;
        private DevExpress.XtraGrid.Views.Grid.GridView grvCorpoDiretivo;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem24;
        private System.Windows.Forms.BindingSource cORPODIRETIVOBindingSource;
        private Cadastros.Condominios.dCondominiosCamposTableAdapters.CORPODIRETIVOTableAdapter cORPODIRETIVOTableAdapter;
        private System.Windows.Forms.BindingNavigator bnvCorpoDiretivo;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem81;
        private System.Windows.Forms.ToolStripButton btnAddCorpoDiretivo;
        private System.Windows.Forms.ToolStripButton btnEditCorpoDiretivo;
        private System.Windows.Forms.ToolStripButton btnPrintCorpoDiretivo;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator7;
        private System.Windows.Forms.ToolStripButton btnDelCorpoDiretivo;
        private DevExpress.XtraEditors.TextEdit txtTelefone_C;
        private DevExpress.XtraEditors.TextEdit txtSindico_C;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem83;
        private DevExpress.XtraEditors.TextEdit txtCNPJ_C;
        private DevExpress.XtraEditors.DateEdit DataAdesao_C;
        private DevExpress.XtraEditors.TextEdit txtTotalUnidades_C;
        private DevExpress.XtraEditors.TextEdit txtConta_C;
        private DevExpress.XtraEditors.TextEdit txtAgencia_C;
        private DevExpress.XtraEditors.CheckEdit chkGas_C;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem88;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem90;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup27;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem82;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup41;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem85;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem86;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem89;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem87;
        private DevExpress.XtraEditors.LookUpEdit lookUpEdit1;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem91;
        private DevExpress.XtraEditors.TextEdit textEdit14;
        private DevExpress.XtraEditors.TextEdit textEdit13;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem84;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem92;
        private DevExpress.XtraEditors.CheckEdit checkEdit7;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem93;
        private DevExpress.XtraGrid.Columns.GridColumn colPESNome;
        private DevExpress.XtraGrid.Columns.GridColumn colAPTNumero1;
        private DevExpress.XtraGrid.Columns.GridColumn colBLONome1;
        private DevExpress.XtraGrid.Columns.GridColumn colCGONome;
        private DevExpress.XtraEditors.LookUpEdit lkpUF;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup1;
        private DevExpress.XtraEditors.TextEdit txtCodigoFolha1;
        private DevExpress.XtraEditors.TextEdit textEdit12;
        private DevExpress.XtraEditors.TextEdit textEdit7;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem1;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup43;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup44;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem95;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem96;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem2;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup45;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem94;
        private DevExpress.XtraEditors.TextEdit txtCodigoFolha2;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem97;
        private DevExpress.XtraEditors.LookUpEdit lookUpEdit4;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem98;
        private System.Windows.Forms.BindingSource eMPRESASBindingSource;
        private Cadastros.Condominios.dCondominiosCamposTableAdapters.EMPRESASTableAdapter eMPRESASTableAdapter;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup46;
        private DevExpress.XtraEditors.SpinEdit spinEdit2;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem99;
        private System.Windows.Forms.BindingSource fKBLOCOSCONDOMINIOSBindingSource1;
        private DevExpress.XtraEditors.LabelControl labelControl1;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem100;
    }
}
