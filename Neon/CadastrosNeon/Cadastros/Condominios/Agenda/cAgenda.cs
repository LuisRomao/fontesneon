using System;
using System.Data;
using System.Drawing;


namespace Cadastros.Condominios.Agenda
{
    /// <summary>
    /// 
    /// </summary>
    public partial class cAgenda : CompontesBasicos.ComponenteCamposBase
    {

        private dAgenda.CONDOMINIOSRow LinhaMae {
            get {
                return (LinhaMae_F == null) ? null : (dAgenda.CONDOMINIOSRow)LinhaMae_F;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        public cAgenda()
        {
            InitializeComponent();
            Agendadata.MinValue = DateTime.Today;
            Agendadata.MaxValue = DateTime.Today.AddYears(1);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public override bool Update_F()
        {
            if (base.Update_F()) {
                dAgenda.HISTORICOSTableAdapter.Update(dAgenda.HISTORICOS);
                dAgenda.HISTORICOS.AcceptChanges();
                return true;
            }
            else
                return false;
        }

        private Cadastros.Condominios.Agenda.dAgenda.HISTORICOSRow HistRow;

        private bool PegaLinha() {
            return PegaLinha(advBandedGridView1.FocusedRowHandle);
        }

        private bool PegaLinha(int RowHandle)
        {
            DataRowView DRV = (DataRowView)advBandedGridView1.GetRow(RowHandle);
            if ((DRV != null) && (DRV.Row != null))
                HistRow = (Cadastros.Condominios.Agenda.dAgenda.HISTORICOSRow)DRV.Row;
            else
                HistRow = null;
            return (HistRow != null);          
        }

        private bool PegaLinha(int RowHandle, out Cadastros.Condominios.Agenda.dAgenda.HISTORICOSRow rowHST)
        {
            DataRowView DRV = (DataRowView)advBandedGridView1.GetRow(RowHandle);
            if ((DRV != null) && (DRV.Row != null))
                rowHST = (Cadastros.Condominios.Agenda.dAgenda.HISTORICOSRow)DRV.Row;
            else
                rowHST = null;
            return (rowHST != null);
        }

        private void gridView1_InitNewRow(object sender, DevExpress.XtraGrid.Views.Grid.InitNewRowEventArgs e)
        {
            PegaLinha(e.RowHandle);
            if (HistRow != null) 
            {
                string Texto = "";
                HistRow.HST_CON = LinhaMae.CON;
                HistRow.HSTCritico = false;
                HistRow.HSTDATAI = DateTime.Now;
                HistRow.HSTI_USU = CompontesBasicos.Bancovirtual.UsuarioLogado.UsuarioLogadoSt.USU;
                HistRow.HST_ASS = 0;
                HistRow.HSTDescricao = "";
                HistRow.HSTAceito = true;
                HistRow.HSTA_USU = CompontesBasicos.Bancovirtual.UsuarioLogado.UsuarioLogadoSt.USU;
                HistRow.HSTConcluido = false;
                VirInput.Input.Execute("Descri��o inicial", ref Texto, true);
                           
                apenda("Cadastro", Texto);
                //    advBandedGridView1.CloseEditor();
                    //advBandedGridView1.UpdateCurrentRow();
                
                
            };
        }

        private void cAgenda_cargaFinal(object sender, EventArgs e)
        {
            CargaH();
        }

        private void cAgenda_cargaInicial(object sender, EventArgs e)
        {
            uSUARIOSBindingSource.DataSource = FrameworkProc.datasets.dUSUarios.dUSUariosSt;
            dAgenda.ASSuntoTableAdapter.Fill(dAgenda.ASSunto);
        }

        private void cAgenda_Enter(object sender, EventArgs e)
        {
            CompontesBasicos.FormPrincipalBase.FormPrincipal.CaixaAltaGeral = false;
        }

        private void cAgenda_Leave(object sender, EventArgs e)
        {
            CompontesBasicos.FormPrincipalBase.FormPrincipal.CaixaAltaGeral = true;
        }

       

        private void gridView1_ValidateRow(object sender, DevExpress.XtraGrid.Views.Base.ValidateRowEventArgs e)
        {
            if (PegaLinha(e.RowHandle)) {
                if (HistRow.HST_ASS == 0)
                {
                    e.Valid = false;
                    e.ErrorText = "Assunto n�o informado";
                }
                else
                    e.ErrorText = "";
            }
            else
               e.Valid = false;
        }

        private void apenda(string Tipo,string Texto) {
            if (HistRow.HSTDescricao != "")
                HistRow.HSTDescricao += "\r\n\r\n";
            HistRow.HSTDescricao += "(" + DateTime.Now.ToString() + ")" + CompontesBasicos.Bancovirtual.UsuarioLogado.UsuarioLogadoSt.USUNome + " " + Tipo + "\r\n";
            if(Texto != "")
                HistRow.HSTDescricao += "--------------\r\n" + Texto;        
        }

        private void BotaoPostar_ButtonClick(object sender, DevExpress.XtraEditors.Controls.ButtonPressedEventArgs e)
        {
            string Entrando="";
            if (PegaLinha(advBandedGridView1.FocusedRowHandle))
                if (VirInput.Input.Execute("Incluir:", ref Entrando, true))
                {
                    apenda("Post", Entrando);
                    advBandedGridView1.UpdateCurrentRow();
                }
        }

        private void BuscaUSU_EditValueChanged(object sender, EventArgs e)
        {
            if (PegaLinha(advBandedGridView1.FocusedRowHandle))
            {
                DevExpress.XtraEditors.LookUpEdit Editor = (DevExpress.XtraEditors.LookUpEdit)sender;
                HistRow.HSTAceito = ((int)Editor.EditValue == CompontesBasicos.Bancovirtual.UsuarioLogado.UsuarioLogadoSt.USU);
                chAceito.ReadOnly = true;
                apenda("Encaminhado para "+Editor.Text, "");
                advBandedGridView1.CloseEditor();
            }
        }

        private void eddataprometida_EditValueChanged(object sender, EventArgs e)
        {
            if (PegaLinha(advBandedGridView1.FocusedRowHandle))
            {
                DevExpress.XtraEditors.DateEdit Editor = (DevExpress.XtraEditors.DateEdit)sender;
                
                
                apenda("Agendado para  " + Editor.Text, "");
                advBandedGridView1.CloseEditor();
            }
        }

        private void chConcluido_EditValueChanged(object sender, EventArgs e)
        {
            if (PegaLinha())
            {
                DevExpress.XtraEditors.CheckEdit Editor = (DevExpress.XtraEditors.CheckEdit)sender;

                string comentario = "";
                VirInput.Input.Execute("Coment�rio",ref comentario, false);
                apenda(Editor.Checked ? "Conclu�do" : "Reaberto", comentario);
                advBandedGridView1.CloseEditor();
            }
        }

        private void buscaAssunto_ButtonClick(object sender, DevExpress.XtraEditors.Controls.ButtonPressedEventArgs e)
        {
            DevExpress.XtraEditors.LookUpEdit Editor = (DevExpress.XtraEditors.LookUpEdit)sender;
            if (e.Button.Kind == DevExpress.XtraEditors.Controls.ButtonPredefines.Plus) {
                string novo = "";
                DataRow[] RowASSs;
                dAgenda.ASSuntoRow RowASS;
                if (VirInput.Input.Execute("Novo Assunto", ref novo, false)) {
                    novo = novo.Trim().ToUpper();
                    if (novo == "")
                        return;
                    RowASSs = dAgenda.ASSunto.Select("ASSDescricao = '"+novo+"'");
                    if (RowASSs.Length == 0) {
                        RowASS = dAgenda.ASSunto.AddASSuntoRow(novo);
                        dAgenda.ASSuntoTableAdapter.Update(RowASS);
                        RowASS.AcceptChanges();
                    }
                    else
                        RowASS = (dAgenda.ASSuntoRow)RowASSs[0];
                    Editor.EditValue = RowASS.ASS;
                };
            }
        }

        private void advBandedGridView1_FocusedRowChanged(object sender, DevExpress.XtraGrid.Views.Base.FocusedRowChangedEventArgs e)
        {
            if (dAgenda.HasChanges())
            {
                dAgenda.HISTORICOSTableAdapter.Update(dAgenda.HISTORICOS);
                dAgenda.HISTORICOS.AcceptChanges();
            }
            if (PegaLinha(e.FocusedRowHandle))
            {
                chAceito.ReadOnly = (HistRow.HSTAceito || HistRow.HSTConcluido || (CompontesBasicos.Bancovirtual.UsuarioLogado.UsuarioLogadoSt.USU != HistRow.HSTA_USU));
                buscaAssunto.ReadOnly = chConcluido.ReadOnly = chCritico.ReadOnly = (CompontesBasicos.Bancovirtual.UsuarioLogado.UsuarioLogadoSt.USU != HistRow.HSTI_USU);
                Agendadata.ReadOnly = BuscaUSU.ReadOnly = BotaoPostar.ReadOnly = HistRow.HSTConcluido;
                 
            }
            else {
                chAceito.ReadOnly = true;
                Agendadata.ReadOnly = BuscaUSU.ReadOnly = BotaoPostar.ReadOnly = buscaAssunto.ReadOnly = chConcluido.ReadOnly = chCritico.ReadOnly = false;
   
            }
           
        }

        private void buscaAssunto_PropertiesChanged(object sender, EventArgs e)
        {
            DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit Editor = (DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit)sender;
            foreach (DevExpress.XtraEditors.Controls.EditorButton Botao in Editor.Buttons) {
                Botao.Enabled = !Editor.ReadOnly;
            }
            
        }

        private void BotaoPostar_PropertiesChanged(object sender, EventArgs e)
        {
            DevExpress.XtraEditors.Repository.RepositoryItemButtonEdit Editor = (DevExpress.XtraEditors.Repository.RepositoryItemButtonEdit)sender;
            foreach (DevExpress.XtraEditors.Controls.EditorButton Botao in Editor.Buttons)
            {
                Botao.Enabled = !Editor.ReadOnly;
            }
        }

        private void advBandedGridView1_RowStyle(object sender, DevExpress.XtraGrid.Views.Grid.RowStyleEventArgs e)
        {
            Cadastros.Condominios.Agenda.dAgenda.HISTORICOSRow rowHST;
            if (PegaLinha(e.RowHandle, out rowHST))
                if (rowHST.RowState != DataRowState.Detached)
                    if (rowHST.HSTA_USU == CompontesBasicos.Bancovirtual.UsuarioLogado.UsuarioLogadoSt.USU)
                        e.Appearance.BackColor = Color.Red;
                    else
                        if (rowHST.HSTI_USU == CompontesBasicos.Bancovirtual.UsuarioLogado.UsuarioLogadoSt.USU)
                            e.Appearance.BackColor = Color.Aqua;
        }

        private void chAbertos_CheckedChanged(object sender, EventArgs e)
        {
            CargaH();
        }

        private void CargaH(){
            //int pkH = 0;
            //if (PegaLinha())
            //    pkH = HistRow.HST;
            if (chAbertos.Checked)
                dAgenda.HISTORICOSTableAdapter.Abertos(dAgenda.HISTORICOS, pk);
            else
                dAgenda.HISTORICOSTableAdapter.Fill(dAgenda.HISTORICOS, pk);
            //advBandedGridView1.LocateByValue(0, colHSTADATA, pkH);
        }

        
    }
}

