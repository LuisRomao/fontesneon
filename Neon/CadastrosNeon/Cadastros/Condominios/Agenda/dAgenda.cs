﻿namespace Cadastros.Condominios.Agenda {


    partial class dAgenda
    {
        private Cadastros.Condominios.Agenda.dAgendaTableAdapters.HISTORICOSTableAdapter hISTORICOSTableAdapter;
        private Cadastros.Condominios.Agenda.dAgendaTableAdapters.ASSuntoTableAdapter aSSuntoTableAdapter;

        /// <summary>
        /// 
        /// </summary>
        public Cadastros.Condominios.Agenda.dAgendaTableAdapters.ASSuntoTableAdapter ASSuntoTableAdapter
        {
            get {
                if (aSSuntoTableAdapter == null) {
                    aSSuntoTableAdapter = new Cadastros.Condominios.Agenda.dAgendaTableAdapters.ASSuntoTableAdapter();
                    aSSuntoTableAdapter.TrocarStringDeConexao(VirDB.Bancovirtual.TiposDeBanco.SQL);
                };
                return aSSuntoTableAdapter; 
            }
            
        }

        /// <summary>
        /// 
        /// </summary>
        public Cadastros.Condominios.Agenda.dAgendaTableAdapters.HISTORICOSTableAdapter HISTORICOSTableAdapter
        {
            get {
                if (hISTORICOSTableAdapter == null) {
                    hISTORICOSTableAdapter = new Cadastros.Condominios.Agenda.dAgendaTableAdapters.HISTORICOSTableAdapter();
                    hISTORICOSTableAdapter.TrocarStringDeConexao(VirDB.Bancovirtual.TiposDeBanco.SQL);
                };
                return hISTORICOSTableAdapter; 
            }
            
        }
    }

    
}
