namespace Cadastros.Condominios.Agenda
{
    partial class cAgenda
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling1 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(cAgenda));
            this.memoEdit1 = new DevExpress.XtraEditors.MemoEdit();
            this.dAgenda = new Cadastros.Condominios.Agenda.dAgenda();
            this.labelControl1 = new DevExpress.XtraEditors.LabelControl();
            this.cONDOMINIOSTableAdapter = new Cadastros.Condominios.Agenda.dAgendaTableAdapters.CONDOMINIOSTableAdapter();
            this.gridControl1 = new DevExpress.XtraGrid.GridControl();
            this.advBandedGridView1 = new DevExpress.XtraGrid.Views.BandedGrid.AdvBandedGridView();
            this.gridBand4 = new DevExpress.XtraGrid.Views.BandedGrid.GridBand();
            this.colHSTAssunto = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.buscaAssunto = new DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit();
            this.aSSuntoBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.colHSTCritico = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.chCritico = new DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit();
            this.gridBand1 = new DevExpress.XtraGrid.Views.BandedGrid.GridBand();
            this.colHSTDescricao = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.repositoryItemMemoEdit1 = new DevExpress.XtraEditors.Repository.RepositoryItemMemoEdit();
            this.bandedGridColumn2 = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.BotaoPostar = new DevExpress.XtraEditors.Repository.RepositoryItemButtonEdit();
            this.gridBand2 = new DevExpress.XtraGrid.Views.BandedGrid.GridBand();
            this.colHSTResponsavel_USU = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.BuscaUSU = new DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit();
            this.uSUARIOSBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.colHSTData = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.colHSTConcluido = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.chConcluido = new DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit();
            this.gridBand3 = new DevExpress.XtraGrid.Views.BandedGrid.GridBand();
            this.colHSTA_USU = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.colHSTAceito = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.chAceito = new DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit();
            this.colHSTADATA = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.Agendadata = new DevExpress.XtraEditors.Repository.RepositoryItemDateEdit();
            this.aSSuntoTableAdapter = new Cadastros.Condominios.Agenda.dAgendaTableAdapters.ASSuntoTableAdapter();
            this.panelControl1 = new DevExpress.XtraEditors.PanelControl();
            this.panelControl2 = new DevExpress.XtraEditors.PanelControl();
            this.chMeus = new DevExpress.XtraEditors.CheckEdit();
            this.chAbertos = new DevExpress.XtraEditors.CheckEdit();
            ((System.ComponentModel.ISupportInitialize)(this.BarManager_F)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.BarAndDockingController_F)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.BindingSource_F)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.StyleController_F)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.memoEdit1.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dAgenda)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.advBandedGridView1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.buscaAssunto)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.aSSuntoBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chCritico)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoEdit1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.BotaoPostar)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.BuscaUSU)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uSUARIOSBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chConcluido)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chAceito)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Agendadata)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Agendadata.VistaTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl1)).BeginInit();
            this.panelControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl2)).BeginInit();
            this.panelControl2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.chMeus.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chAbertos.Properties)).BeginInit();
            this.SuspendLayout();
            // 
            // barStatus_F
            // 
            this.barStatus_F.OptionsBar.AllowQuickCustomization = false;
            this.barStatus_F.OptionsBar.DisableClose = true;
            this.barStatus_F.OptionsBar.DisableCustomization = true;
            this.barStatus_F.OptionsBar.DrawDragBorder = false;
            this.barStatus_F.OptionsBar.DrawSizeGrip = true;
            this.barStatus_F.OptionsBar.UseWholeRow = true;
            // 
            // barComponente_F
            // 
            this.barComponente_F.OptionsBar.AllowQuickCustomization = false;
            this.barComponente_F.OptionsBar.DisableClose = true;
            this.barComponente_F.OptionsBar.DisableCustomization = true;
            this.barComponente_F.OptionsBar.DrawDragBorder = false;
            this.barComponente_F.OptionsBar.UseWholeRow = true;
            // 
            // btnFechar_F
            // 
            this.btnFechar_F.Visibility = DevExpress.XtraBars.BarItemVisibility.Never;
            // 
            // BarAndDockingController_F
            // 
            this.BarAndDockingController_F.PropertiesBar.AllowLinkLighting = false;
            // 
            // BindingSource_F
            // 
            this.BindingSource_F.DataMember = "CONDOMINIOS";
            this.BindingSource_F.DataSource = this.dAgenda;
            // 
            // ToolTipController_F
            // 
            this.ToolTipController_F.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(222)))), ((int)(((byte)(237)))), ((int)(((byte)(250)))));
            this.ToolTipController_F.Appearance.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(20)))), ((int)(((byte)(69)))), ((int)(((byte)(141)))));
            this.ToolTipController_F.Appearance.Options.UseBackColor = true;
            this.ToolTipController_F.Appearance.Options.UseFont = true;
            this.ToolTipController_F.Appearance.Options.UseForeColor = true;
            this.ToolTipController_F.Appearance.Options.UseTextOptions = true;
            this.ToolTipController_F.Appearance.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            // 
            // StyleController_F
            // 
            this.StyleController_F.AppearanceDisabled.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this.StyleController_F.AppearanceDisabled.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this.StyleController_F.AppearanceDisabled.Options.UseBackColor = true;
            this.StyleController_F.AppearanceDisabled.Options.UseForeColor = true;
            this.StyleController_F.AppearanceFocused.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.StyleController_F.AppearanceFocused.Options.UseBackColor = true;
        
            // 
            // memoEdit1
            // 
            this.memoEdit1.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.BindingSource_F, "CONObservacao", true));
            this.memoEdit1.Dock = System.Windows.Forms.DockStyle.Top;
            this.memoEdit1.Location = new System.Drawing.Point(0, 54);
            this.memoEdit1.Name = "memoEdit1";
            
            this.memoEdit1.Size = new System.Drawing.Size(1230, 176);
            
            this.memoEdit1.TabIndex = 4;
            // 
            // dAgenda
            // 
            this.dAgenda.DataSetName = "dAgenda";
            this.dAgenda.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // labelControl1
            // 
            this.labelControl1.Appearance.Font = new System.Drawing.Font("Tahoma", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl1.Appearance.Options.UseFont = true;
            this.labelControl1.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.BindingSource_F, "CONCodigo", true));
            this.labelControl1.Dock = System.Windows.Forms.DockStyle.Top;
            this.labelControl1.Location = new System.Drawing.Point(0, 29);
            this.labelControl1.Name = "labelControl1";
            this.labelControl1.Size = new System.Drawing.Size(140, 25);
            this.labelControl1.TabIndex = 5;
            this.labelControl1.Text = "labelControl1";
            // 
            // cONDOMINIOSTableAdapter
            // 
            this.cONDOMINIOSTableAdapter.ClearBeforeFill = true;
            // 
            // gridControl1
            // 
            this.gridControl1.DataMember = "HISTORICOS";
            this.gridControl1.DataSource = this.dAgenda;
            this.gridControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridControl1.EmbeddedNavigator.Name = "";
            this.gridControl1.Location = new System.Drawing.Point(4, 35);
            this.gridControl1.MainView = this.advBandedGridView1;
            this.gridControl1.Name = "gridControl1";
            this.gridControl1.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemMemoEdit1,
            this.BuscaUSU,
            this.BotaoPostar,
            this.chCritico,
            this.chConcluido,
            this.chAceito,
            this.Agendadata,
            this.buscaAssunto});
            this.gridControl1.Size = new System.Drawing.Size(1222, 42);
            this.gridControl1.TabIndex = 6;
            this.gridControl1.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.advBandedGridView1});
            // 
            // advBandedGridView1
            // 
            this.advBandedGridView1.BandPanelRowHeight = 0;
            this.advBandedGridView1.Bands.AddRange(new DevExpress.XtraGrid.Views.BandedGrid.GridBand[] {
            this.gridBand4,
            this.gridBand1,
            this.gridBand2,
            this.gridBand3});
            this.advBandedGridView1.Columns.AddRange(new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn[] {
            this.colHSTAssunto,
            this.colHSTDescricao,
            this.colHSTCritico,
            this.colHSTData,
            this.colHSTResponsavel_USU,
            this.colHSTA_USU,
            this.colHSTADATA,
            this.colHSTAceito,
            this.colHSTConcluido,
            this.bandedGridColumn2});
            this.advBandedGridView1.GridControl = this.gridControl1;
            this.advBandedGridView1.Name = "advBandedGridView1";
            this.advBandedGridView1.NewItemRowText = "Incluir nova linha";
            this.advBandedGridView1.OptionsCustomization.AllowChangeColumnParent = true;
            this.advBandedGridView1.OptionsCustomization.AllowRowSizing = true;
            this.advBandedGridView1.OptionsCustomization.ShowBandsInCustomizationForm = false;
            this.advBandedGridView1.OptionsMenu.EnableColumnMenu = false;
            this.advBandedGridView1.OptionsView.ColumnAutoWidth = true;
            this.advBandedGridView1.OptionsView.NewItemRowPosition = DevExpress.XtraGrid.Views.Grid.NewItemRowPosition.Bottom;
            this.advBandedGridView1.OptionsView.ShowGroupPanel = false;
            this.advBandedGridView1.RowHeight = 50;
            this.advBandedGridView1.FocusedRowChanged += new DevExpress.XtraGrid.Views.Base.FocusedRowChangedEventHandler(this.advBandedGridView1_FocusedRowChanged);
            this.advBandedGridView1.InitNewRow += new DevExpress.XtraGrid.Views.Grid.InitNewRowEventHandler(this.gridView1_InitNewRow);
            this.advBandedGridView1.RowStyle += new DevExpress.XtraGrid.Views.Grid.RowStyleEventHandler(this.advBandedGridView1_RowStyle);
            this.advBandedGridView1.ValidateRow += new DevExpress.XtraGrid.Views.Base.ValidateRowEventHandler(this.gridView1_ValidateRow);
            // 
            // gridBand4
            // 
            this.gridBand4.Caption = "gridBand4";
            this.gridBand4.Columns.Add(this.colHSTAssunto);
            this.gridBand4.Columns.Add(this.colHSTCritico);
            this.gridBand4.MinWidth = 20;
            this.gridBand4.Name = "gridBand4";
            this.gridBand4.OptionsBand.AllowMove = false;
            this.gridBand4.OptionsBand.AllowSize = false;
            this.gridBand4.OptionsBand.FixedWidth = true;
            this.gridBand4.OptionsBand.ShowCaption = false;
            this.gridBand4.Width = 75;
            // 
            // colHSTAssunto
            // 
            this.colHSTAssunto.AutoFillDown = true;
            this.colHSTAssunto.Caption = "Assunto";
            this.colHSTAssunto.ColumnEdit = this.buscaAssunto;
            this.colHSTAssunto.FieldName = "HST_ASS";
            this.colHSTAssunto.Name = "colHSTAssunto";
            this.colHSTAssunto.OptionsColumn.FixedWidth = true;
            this.colHSTAssunto.Visible = true;
            // 
            // buscaAssunto
            // 
            this.buscaAssunto.AutoHeight = false;
            this.buscaAssunto.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo),
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Plus)});
            this.buscaAssunto.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("ASSDescricao", "ASSDescricao", 71, DevExpress.Utils.FormatType.None, "", true, DevExpress.Utils.HorzAlignment.Near)});
            this.buscaAssunto.DataSource = this.aSSuntoBindingSource;
            this.buscaAssunto.DisplayMember = "ASSDescricao";
            this.buscaAssunto.Name = "buscaAssunto";
            this.buscaAssunto.NullText = "Indefinido";
            this.buscaAssunto.ShowHeader = false;
            this.buscaAssunto.ValueMember = "ASS";
            this.buscaAssunto.ButtonClick += new DevExpress.XtraEditors.Controls.ButtonPressedEventHandler(this.buscaAssunto_ButtonClick);
            this.buscaAssunto.PropertiesChanged += new System.EventHandler(this.buscaAssunto_PropertiesChanged);
            // 
            // aSSuntoBindingSource
            // 
            this.aSSuntoBindingSource.DataMember = "ASSunto";
            this.aSSuntoBindingSource.DataSource = this.dAgenda;
            // 
            // colHSTCritico
            // 
            this.colHSTCritico.AutoFillDown = true;
            this.colHSTCritico.Caption = "Cr�tico";
            this.colHSTCritico.ColumnEdit = this.chCritico;
            this.colHSTCritico.FieldName = "HSTCritico";
            this.colHSTCritico.Name = "colHSTCritico";
            this.colHSTCritico.OptionsColumn.FixedWidth = true;
            this.colHSTCritico.RowIndex = 1;
            this.colHSTCritico.Visible = true;
            this.colHSTCritico.Width = 72;
            // 
            // chCritico
            // 
            this.chCritico.AutoHeight = false;
            this.chCritico.Caption = "Cr�tico";
            this.chCritico.GlyphAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.chCritico.Name = "chCritico";
            // 
            // gridBand1
            // 
            this.gridBand1.Caption = "Hist�rico";
            this.gridBand1.Columns.Add(this.colHSTDescricao);
            this.gridBand1.Columns.Add(this.bandedGridColumn2);
            this.gridBand1.MinWidth = 20;
            this.gridBand1.Name = "gridBand1";
            this.gridBand1.OptionsBand.AllowMove = false;
            this.gridBand1.OptionsBand.AllowSize = false;
            this.gridBand1.OptionsBand.ShowCaption = false;
            this.gridBand1.Width = 614;
            // 
            // colHSTDescricao
            // 
            this.colHSTDescricao.AutoFillDown = true;
            this.colHSTDescricao.Caption = "Descri��o";
            this.colHSTDescricao.ColumnEdit = this.repositoryItemMemoEdit1;
            this.colHSTDescricao.FieldName = "HSTDescricao";
            this.colHSTDescricao.Name = "colHSTDescricao";
            this.colHSTDescricao.Visible = true;
            this.colHSTDescricao.Width = 556;
            // 
            // repositoryItemMemoEdit1
            // 
            this.repositoryItemMemoEdit1.Name = "repositoryItemMemoEdit1";
            this.repositoryItemMemoEdit1.ReadOnly = true;
            this.repositoryItemMemoEdit1.ScrollBars = System.Windows.Forms.ScrollBars.Both;
            // 
            // bandedGridColumn2
            // 
            this.bandedGridColumn2.AutoFillDown = true;
            this.bandedGridColumn2.Caption = "Postar";
            this.bandedGridColumn2.ColumnEdit = this.BotaoPostar;
            this.bandedGridColumn2.Name = "bandedGridColumn2";
            this.bandedGridColumn2.OptionsColumn.FixedWidth = true;
            this.bandedGridColumn2.RowCount = 2;
            this.bandedGridColumn2.ShowButtonMode = DevExpress.XtraGrid.Views.Base.ShowButtonModeEnum.ShowAlways;
            this.bandedGridColumn2.Visible = true;
            this.bandedGridColumn2.Width = 58;
            // 
            // BotaoPostar
            // 
            this.BotaoPostar.AutoHeight = false;
            this.BotaoPostar.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Plus, "Postar", -1, true, true, true, DevExpress.XtraEditors.ImageLocation.MiddleRight, null)});
            this.BotaoPostar.Name = "BotaoPostar";
            this.BotaoPostar.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.HideTextEditor;
            this.BotaoPostar.PropertiesChanged += new System.EventHandler(this.BotaoPostar_PropertiesChanged);
            this.BotaoPostar.ButtonClick += new DevExpress.XtraEditors.Controls.ButtonPressedEventHandler(this.BotaoPostar_ButtonClick);
            // 
            // gridBand2
            // 
            this.gridBand2.Caption = "Autor";
            this.gridBand2.Columns.Add(this.colHSTResponsavel_USU);
            this.gridBand2.Columns.Add(this.colHSTData);
            this.gridBand2.Columns.Add(this.colHSTConcluido);
            this.gridBand2.MinWidth = 20;
            this.gridBand2.Name = "gridBand2";
            this.gridBand2.OptionsBand.AllowMove = false;
            this.gridBand2.OptionsBand.AllowSize = false;
            this.gridBand2.OptionsBand.FixedWidth = true;
            this.gridBand2.OptionsBand.ShowCaption = false;
            this.gridBand2.Width = 200;
            // 
            // colHSTResponsavel_USU
            // 
            this.colHSTResponsavel_USU.Caption = "Autor";
            this.colHSTResponsavel_USU.ColumnEdit = this.BuscaUSU;
            this.colHSTResponsavel_USU.FieldName = "HSTI_USU";
            this.colHSTResponsavel_USU.Name = "colHSTResponsavel_USU";
            this.colHSTResponsavel_USU.OptionsColumn.AllowEdit = false;
            this.colHSTResponsavel_USU.OptionsColumn.FixedWidth = true;
            this.colHSTResponsavel_USU.Visible = true;
            this.colHSTResponsavel_USU.Width = 156;
            // 
            // BuscaUSU
            // 
            this.BuscaUSU.AutoHeight = false;
            this.BuscaUSU.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.BuscaUSU.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("USUNome", "USUNome", 53, DevExpress.Utils.FormatType.None, "", true, DevExpress.Utils.HorzAlignment.Near)});
            this.BuscaUSU.DataSource = this.uSUARIOSBindingSource;
            this.BuscaUSU.DisplayMember = "USUNome";
            this.BuscaUSU.Name = "BuscaUSU";
            this.BuscaUSU.NullText = "-";
            this.BuscaUSU.ShowHeader = false;
            this.BuscaUSU.ValueMember = "USU";
            this.BuscaUSU.EditValueChanged += new System.EventHandler(this.BuscaUSU_EditValueChanged);
            // 
            // uSUARIOSBindingSource
            // 
            this.uSUARIOSBindingSource.DataMember = "USUARIOS";
            this.uSUARIOSBindingSource.DataSource = typeof(FrameworkProc.datasets.dUSUarios);
            // 
            // colHSTData
            // 
            this.colHSTData.Caption = "Inclus�o";
            this.colHSTData.DisplayFormat.FormatString = "dd/MM/yy hh:mm:ss";
            this.colHSTData.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.colHSTData.FieldName = "HSTDATAI";
            this.colHSTData.Name = "colHSTData";
            this.colHSTData.OptionsColumn.AllowEdit = false;
            this.colHSTData.OptionsColumn.FixedWidth = true;
            this.colHSTData.RowIndex = 1;
            this.colHSTData.Visible = true;
            this.colHSTData.Width = 117;
            // 
            // colHSTConcluido
            // 
            this.colHSTConcluido.Caption = "Conclu�do";
            this.colHSTConcluido.ColumnEdit = this.chConcluido;
            this.colHSTConcluido.FieldName = "HSTConcluido";
            this.colHSTConcluido.Name = "colHSTConcluido";
            this.colHSTConcluido.RowIndex = 1;
            this.colHSTConcluido.Visible = true;
            this.colHSTConcluido.Width = 83;
            // 
            // chConcluido
            // 
            this.chConcluido.AutoHeight = false;
            this.chConcluido.Caption = "Conclu�do";
            this.chConcluido.GlyphAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.chConcluido.Name = "chConcluido";
            this.chConcluido.EditValueChanged += new System.EventHandler(this.chConcluido_EditValueChanged);
            // 
            // gridBand3
            // 
            this.gridBand3.Caption = "Responsavel";
            this.gridBand3.Columns.Add(this.colHSTA_USU);
            this.gridBand3.Columns.Add(this.colHSTAceito);
            this.gridBand3.Columns.Add(this.colHSTADATA);
            this.gridBand3.MinWidth = 20;
            this.gridBand3.Name = "gridBand3";
            this.gridBand3.OptionsBand.AllowMove = false;
            this.gridBand3.OptionsBand.AllowSize = false;
            this.gridBand3.OptionsBand.FixedWidth = true;
            this.gridBand3.OptionsBand.ShowCaption = false;
            this.gridBand3.Width = 200;
            // 
            // colHSTA_USU
            // 
            this.colHSTA_USU.Caption = "Respons�vel";
            this.colHSTA_USU.ColumnEdit = this.BuscaUSU;
            this.colHSTA_USU.FieldName = "HSTA_USU";
            this.colHSTA_USU.Name = "colHSTA_USU";
            this.colHSTA_USU.Visible = true;
            this.colHSTA_USU.Width = 109;
            // 
            // colHSTAceito
            // 
            this.colHSTAceito.Caption = "Aceito";
            this.colHSTAceito.ColumnEdit = this.chAceito;
            this.colHSTAceito.FieldName = "HSTAceito";
            this.colHSTAceito.Name = "colHSTAceito";
            this.colHSTAceito.Visible = true;
            this.colHSTAceito.Width = 91;
            // 
            // chAceito
            // 
            this.chAceito.AutoHeight = false;
            this.chAceito.Caption = "Aceito";
            this.chAceito.GlyphAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.chAceito.Name = "chAceito";
            // 
            // colHSTADATA
            // 
            this.colHSTADATA.Caption = "Data Prometida";
            this.colHSTADATA.ColumnEdit = this.Agendadata;
            this.colHSTADATA.FieldName = "HSTADATA";
            this.colHSTADATA.Name = "colHSTADATA";
            this.colHSTADATA.RowIndex = 1;
            this.colHSTADATA.Visible = true;
            this.colHSTADATA.Width = 200;
            // 
            // Agendadata
            // 
            this.Agendadata.AutoHeight = false;
            this.Agendadata.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.Agendadata.Name = "Agendadata";
            this.Agendadata.VistaTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.Agendadata.EditValueChanged += new System.EventHandler(this.eddataprometida_EditValueChanged);
            // 
            // aSSuntoTableAdapter
            // 
            this.aSSuntoTableAdapter.ClearBeforeFill = true;
            // 
            // panelControl1
            // 
            this.panelControl1.Controls.Add(this.gridControl1);
            this.panelControl1.Controls.Add(this.panelControl2);
            this.panelControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelControl1.Location = new System.Drawing.Point(0, 230);
            this.panelControl1.Name = "panelControl1";
            this.panelControl1.Size = new System.Drawing.Size(1230, 81);
            this.ToolTipController_F.SetSuperTip(this.panelControl1, null);
            this.panelControl1.TabIndex = 7;
            // 
            // panelControl2
            // 
            this.panelControl2.Controls.Add(this.chMeus);
            this.panelControl2.Controls.Add(this.chAbertos);
            this.panelControl2.Dock = System.Windows.Forms.DockStyle.Top;
            this.panelControl2.Location = new System.Drawing.Point(4, 4);
            this.panelControl2.Name = "panelControl2";
            this.panelControl2.Size = new System.Drawing.Size(1222, 31);
            this.ToolTipController_F.SetSuperTip(this.panelControl2, null);
            this.panelControl2.TabIndex = 7;
            // 
            // chMeus
            // 
            this.chMeus.EditValue = true;
            this.chMeus.Location = new System.Drawing.Point(166, 6);
            this.chMeus.Name = "chMeus";
            this.chMeus.Properties.Caption = "Somente os meus";
            this.chMeus.Size = new System.Drawing.Size(121, 18);
            this.chMeus.TabIndex = 1;
            this.chMeus.Visible = false;
            this.chMeus.CheckedChanged += new System.EventHandler(this.chAbertos_CheckedChanged);
            // 
            // chAbertos
            // 
            this.chAbertos.EditValue = true;
            this.chAbertos.Location = new System.Drawing.Point(6, 6);
            this.chAbertos.Name = "chAbertos";
            this.chAbertos.Properties.Caption = "Somene Abertos";
            this.chAbertos.Size = new System.Drawing.Size(110, 18);
            this.chAbertos.TabIndex = 0;
            this.chAbertos.CheckedChanged += new System.EventHandler(this.chAbertos_CheckedChanged);
            // 
            // cAgenda
            // 
            this.BindingSourcePrincipal = this.BindingSource_F;
            this.Controls.Add(this.panelControl1);
            this.Controls.Add(this.memoEdit1);
            this.Controls.Add(this.labelControl1);
            this.Doc = System.Windows.Forms.DockStyle.Fill;
         
            this.LookAndFeel.UseDefaultLookAndFeel = false;
            this.Name = "cAgenda";
            this.Size = new System.Drawing.Size(1230, 332);
            this.ToolTipController_F.SetSuperTip(this, null);
            this.Enter += new System.EventHandler(this.cAgenda_Enter);
            this.cargaInicial += new System.EventHandler(this.cAgenda_cargaInicial);
            this.cargaFinal += new System.EventHandler(this.cAgenda_cargaFinal);
            this.Leave += new System.EventHandler(this.cAgenda_Leave);
            this.Controls.SetChildIndex(this.labelControl1, 0);
            this.Controls.SetChildIndex(this.memoEdit1, 0);
            this.Controls.SetChildIndex(this.panelControl1, 0);
            ((System.ComponentModel.ISupportInitialize)(this.BarManager_F)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.BarAndDockingController_F)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.BindingSource_F)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.StyleController_F)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.memoEdit1.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dAgenda)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.advBandedGridView1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.buscaAssunto)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.aSSuntoBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chCritico)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoEdit1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.BotaoPostar)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.BuscaUSU)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uSUARIOSBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chConcluido)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chAceito)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Agendadata.VistaTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Agendadata)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl1)).EndInit();
            this.panelControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.panelControl2)).EndInit();
            this.panelControl2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.chMeus.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chAbertos.Properties)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private DevExpress.XtraEditors.MemoEdit memoEdit1;
        private dAgenda dAgenda;
        private DevExpress.XtraEditors.LabelControl labelControl1;
        private Cadastros.Condominios.Agenda.dAgendaTableAdapters.CONDOMINIOSTableAdapter cONDOMINIOSTableAdapter;
        private DevExpress.XtraGrid.GridControl gridControl1;
        private DevExpress.XtraEditors.Repository.RepositoryItemMemoEdit repositoryItemMemoEdit1;
        private DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit BuscaUSU;
        private System.Windows.Forms.BindingSource uSUARIOSBindingSource;
        private DevExpress.XtraGrid.Views.BandedGrid.AdvBandedGridView advBandedGridView1;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn colHSTDescricao;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn colHSTData;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn colHSTCritico;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn colHSTResponsavel_USU;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn colHSTAssunto;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn colHSTA_USU;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn colHSTADATA;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn colHSTAceito;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn colHSTConcluido;
        private DevExpress.XtraGrid.Views.BandedGrid.GridBand gridBand1;
        private DevExpress.XtraGrid.Views.BandedGrid.GridBand gridBand2;
        private DevExpress.XtraGrid.Views.BandedGrid.GridBand gridBand4;
        private DevExpress.XtraGrid.Views.BandedGrid.GridBand gridBand3;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn bandedGridColumn2;
        private DevExpress.XtraEditors.Repository.RepositoryItemButtonEdit BotaoPostar;
        private DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit chCritico;
        private DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit chConcluido;
        private DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit chAceito;
        private DevExpress.XtraEditors.Repository.RepositoryItemDateEdit Agendadata;
        private DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit buscaAssunto;
        private System.Windows.Forms.BindingSource aSSuntoBindingSource;
        private Cadastros.Condominios.Agenda.dAgendaTableAdapters.ASSuntoTableAdapter aSSuntoTableAdapter;
        private DevExpress.XtraEditors.PanelControl panelControl1;
        private DevExpress.XtraEditors.PanelControl panelControl2;
        private DevExpress.XtraEditors.CheckEdit chMeus;
        private DevExpress.XtraEditors.CheckEdit chAbertos;
    }
}
