namespace Cadastros.Condominios
{
    partial class cApartamentos
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling1 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(cApartamentos));
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling2 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling3 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling4 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling5 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject1 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling6 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            this.LayoutControl = new DevExpress.XtraLayout.LayoutControl();
            this.textEdit1 = new DevExpress.XtraEditors.TextEdit();
            this.imageComboBoxEdit1 = new DevExpress.XtraEditors.ImageComboBoxEdit();
            this.lookUpEdit1 = new DevExpress.XtraEditors.LookUpEdit();
            this.bindingSourceFRN = new System.Windows.Forms.BindingSource(this.components);
            this.checkEdit7 = new DevExpress.XtraEditors.CheckEdit();
            this.checkEdit6 = new DevExpress.XtraEditors.CheckEdit();
            this.spinEdit2 = new DevExpress.XtraEditors.SpinEdit();
            this.gridControl1 = new DevExpress.XtraGrid.GridControl();
            this.aPTPGEBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.dApartamentosCampos = new Cadastros.Condominios.dApartamentosCampos();
            this.gridView1 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colAPTPGEPaga = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemCheckEdit1 = new DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit();
            this.colPGE_PLA = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colPGEMensagem = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colPGEValor = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colAPTPGEDATAI = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colAPTPGEDATAA = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colAPTPGEA_USU = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colAPTPGEI_USU = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colAPTGEIProprietario = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemImageComboBox1 = new DevExpress.XtraEditors.Repository.RepositoryItemImageComboBox();
            this.imagensPI1 = new Framework.objetosNeon.ImagensPI();
            this.aPTSenhaInternetInquilinoTextEdit = new DevExpress.XtraEditors.TextEdit();
            this.aPTUsuarioInternetInquilinoTextEdit = new DevExpress.XtraEditors.TextEdit();
            this.aPTSenhaInternetProprietarioTextEdit = new DevExpress.XtraEditors.TextEdit();
            this.aPTUsuarioInternetProprietarioTextEdit = new DevExpress.XtraEditors.TextEdit();
            this.lkpImobiliaria = new DevExpress.XtraEditors.LookUpEdit();
            this.iMOBILIARIASBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.dCondominiosCampos = new Cadastros.Condominios.dCondominiosCampos();
            this.checkEdit1 = new DevExpress.XtraEditors.CheckEdit();
            this.lkpInquilino = new DevExpress.XtraEditors.LookUpEdit();
            this.pESSOASBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.checkEdit4 = new DevExpress.XtraEditors.CheckEdit();
            this.lkpPagtoInquilino = new DevExpress.XtraEditors.LookUpEdit();
            this.fORMAPAGTOBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.lkpProprietario = new DevExpress.XtraEditors.LookUpEdit();
            this.lkpPagtoProprietario = new DevExpress.XtraEditors.LookUpEdit();
            this.txtIdentificacao = new DevExpress.XtraEditors.TextEdit();
            this.checkEdit5 = new DevExpress.XtraEditors.CheckEdit();
            this.clcFracaoIdeal = new DevExpress.XtraEditors.CalcEdit();
            this.spinEdit1 = new DevExpress.XtraEditors.SpinEdit();
            this.LayoutControlGroup = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlGroup1 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlItem1 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem2 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlGroup4 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlGroup5 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlItem6 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlGroup6 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlItem7 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem23 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem17 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlGroup8 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlItem10 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem15 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem16 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem5 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem20 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlGroup3 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlItem3 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem13 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem14 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem4 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem21 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem18 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlGroup10 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlItem22 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlGroup9 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlItem12 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem11 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem19 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlGroup7 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlItem8 = new DevExpress.XtraLayout.LayoutControlItem();
            this.fORMAPAGTOTableAdapter = new Cadastros.Condominios.dCondominiosCamposTableAdapters.FORMAPAGTOTableAdapter();
            this.aPARTAMENTOSTableAdapter = new Cadastros.Condominios.dApartamentosCamposTableAdapters.APARTAMENTOSTableAdapter();
            this.aPTPGETableAdapter = new Cadastros.Condominios.dApartamentosCamposTableAdapters.APTPGETableAdapter();
            ((System.ComponentModel.ISupportInitialize)(this.BarManager_F)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.BarAndDockingController_F)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.BindingSource_F)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.StyleController_F)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LayoutControl)).BeginInit();
            this.LayoutControl.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit1.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.imageComboBoxEdit1.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lookUpEdit1.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.bindingSourceFRN)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit7.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit6.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.spinEdit2.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.aPTPGEBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dApartamentosCampos)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckEdit1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemImageComboBox1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.imagensPI1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.aPTSenhaInternetInquilinoTextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.aPTUsuarioInternetInquilinoTextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.aPTSenhaInternetProprietarioTextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.aPTUsuarioInternetProprietarioTextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lkpImobiliaria.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.iMOBILIARIASBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dCondominiosCampos)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit1.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lkpInquilino.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pESSOASBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit4.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lkpPagtoInquilino.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.fORMAPAGTOBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lkpProprietario.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lkpPagtoProprietario.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtIdentificacao.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit5.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.clcFracaoIdeal.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.spinEdit1.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LayoutControlGroup)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem23)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem17)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup8)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem10)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem15)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem16)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem20)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem13)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem14)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem21)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem18)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup10)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem22)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup9)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem12)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem11)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem19)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem8)).BeginInit();
            this.SuspendLayout();
            // 
            // barStatus_F
            // 
            this.barStatus_F.OptionsBar.AllowQuickCustomization = false;
            this.barStatus_F.OptionsBar.DisableClose = true;
            this.barStatus_F.OptionsBar.DisableCustomization = true;
            this.barStatus_F.OptionsBar.DrawDragBorder = false;
            this.barStatus_F.OptionsBar.DrawSizeGrip = true;
            this.barStatus_F.OptionsBar.UseWholeRow = true;
            // 
            // barComponente_F
            // 
            this.barComponente_F.OptionsBar.AllowQuickCustomization = false;
            this.barComponente_F.OptionsBar.DisableClose = true;
            this.barComponente_F.OptionsBar.DisableCustomization = true;
            this.barComponente_F.OptionsBar.DrawDragBorder = false;
            this.barComponente_F.OptionsBar.UseWholeRow = true;
            // 
            // btnFechar_F
            // 
            this.btnFechar_F.Visibility = DevExpress.XtraBars.BarItemVisibility.Never;
            // 
            // BarAndDockingController_F
            // 
            this.BarAndDockingController_F.PropertiesBar.AllowLinkLighting = false;
            this.BarAndDockingController_F.PropertiesBar.DefaultGlyphSize = new System.Drawing.Size(16, 16);
            this.BarAndDockingController_F.PropertiesBar.DefaultLargeGlyphSize = new System.Drawing.Size(32, 32);
            // 
            // BindingSource_F
            // 
            this.BindingSource_F.DataMember = "APARTAMENTOS";
            this.BindingSource_F.DataSource = this.dApartamentosCampos;
            // 
            // ToolTipController_F
            // 
            this.ToolTipController_F.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(222)))), ((int)(((byte)(237)))), ((int)(((byte)(250)))));
            this.ToolTipController_F.Appearance.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(20)))), ((int)(((byte)(69)))), ((int)(((byte)(141)))));
            this.ToolTipController_F.Appearance.Options.UseBackColor = true;
            this.ToolTipController_F.Appearance.Options.UseFont = true;
            this.ToolTipController_F.Appearance.Options.UseForeColor = true;
            this.ToolTipController_F.Appearance.Options.UseTextOptions = true;
            this.ToolTipController_F.Appearance.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            // 
            // StyleController_F
            // 
            this.StyleController_F.AppearanceDisabled.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.StyleController_F.AppearanceDisabled.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.StyleController_F.AppearanceDisabled.Options.UseBackColor = true;
            this.StyleController_F.AppearanceDisabled.Options.UseForeColor = true;
            this.StyleController_F.AppearanceFocused.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(253)))), ((int)(((byte)(253)))), ((int)(((byte)(181)))));
            this.StyleController_F.AppearanceFocused.Options.UseBackColor = true;
            // 
            // spellChecker1
            // 
            this.spellChecker1.OptionsSpelling.CheckSelectedTextFirst = DevExpress.Utils.DefaultBoolean.True;
            this.spellChecker1.OptionsSpelling.IgnoreEmails = DevExpress.Utils.DefaultBoolean.True;
            this.spellChecker1.OptionsSpelling.IgnoreMixedCaseWords = DevExpress.Utils.DefaultBoolean.False;
            this.spellChecker1.OptionsSpelling.IgnoreRepeatedWords = DevExpress.Utils.DefaultBoolean.False;
            this.spellChecker1.OptionsSpelling.IgnoreUpperCaseWords = DevExpress.Utils.DefaultBoolean.False;
            this.spellChecker1.OptionsSpelling.IgnoreUrls = DevExpress.Utils.DefaultBoolean.True;
            this.spellChecker1.OptionsSpelling.IgnoreWordsWithNumbers = DevExpress.Utils.DefaultBoolean.True;
            // 
            // LayoutControl
            // 
            this.LayoutControl.Controls.Add(this.textEdit1);
            this.LayoutControl.Controls.Add(this.imageComboBoxEdit1);
            this.LayoutControl.Controls.Add(this.lookUpEdit1);
            this.LayoutControl.Controls.Add(this.checkEdit7);
            this.LayoutControl.Controls.Add(this.checkEdit6);
            this.LayoutControl.Controls.Add(this.spinEdit2);
            this.LayoutControl.Controls.Add(this.gridControl1);
            this.LayoutControl.Controls.Add(this.aPTSenhaInternetInquilinoTextEdit);
            this.LayoutControl.Controls.Add(this.aPTUsuarioInternetInquilinoTextEdit);
            this.LayoutControl.Controls.Add(this.aPTSenhaInternetProprietarioTextEdit);
            this.LayoutControl.Controls.Add(this.aPTUsuarioInternetProprietarioTextEdit);
            this.LayoutControl.Controls.Add(this.lkpImobiliaria);
            this.LayoutControl.Controls.Add(this.checkEdit1);
            this.LayoutControl.Controls.Add(this.lkpInquilino);
            this.LayoutControl.Controls.Add(this.checkEdit4);
            this.LayoutControl.Controls.Add(this.lkpPagtoInquilino);
            this.LayoutControl.Controls.Add(this.lkpProprietario);
            this.LayoutControl.Controls.Add(this.lkpPagtoProprietario);
            this.LayoutControl.Controls.Add(this.txtIdentificacao);
            this.LayoutControl.Controls.Add(this.checkEdit5);
            this.LayoutControl.Controls.Add(this.clcFracaoIdeal);
            this.LayoutControl.Controls.Add(this.spinEdit1);
            this.LayoutControl.Dock = System.Windows.Forms.DockStyle.Fill;
            this.LayoutControl.Location = new System.Drawing.Point(0, 35);
            this.LayoutControl.Name = "LayoutControl";
            this.LayoutControl.OptionsItemText.TextAlignMode = DevExpress.XtraLayout.TextAlignMode.AlignInGroups;
            this.LayoutControl.Root = this.LayoutControlGroup;
            this.LayoutControl.Size = new System.Drawing.Size(1247, 683);
            this.LayoutControl.TabIndex = 4;
            this.LayoutControl.Text = "layoutControl1";
            // 
            // textEdit1
            // 
            this.textEdit1.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.BindingSource_F, "APTRegistro", true));
            this.textEdit1.Location = new System.Drawing.Point(322, 46);
            this.textEdit1.MenuManager = this.BarManager_F;
            this.textEdit1.Name = "textEdit1";
            this.spellChecker1.SetShowSpellCheckMenu(this.textEdit1, true);
            this.textEdit1.Size = new System.Drawing.Size(229, 20);
            this.spellChecker1.SetSpellCheckerOptions(this.textEdit1, optionsSpelling1);
            this.textEdit1.StyleController = this.LayoutControl;
            this.textEdit1.TabIndex = 21;
            // 
            // imageComboBoxEdit1
            // 
            this.imageComboBoxEdit1.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.BindingSource_F, "APTInquilinoPagaRateio", true));
            this.imageComboBoxEdit1.Location = new System.Drawing.Point(699, 326);
            this.imageComboBoxEdit1.Name = "imageComboBoxEdit1";
            this.imageComboBoxEdit1.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.True;
            this.imageComboBoxEdit1.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.imageComboBoxEdit1.Properties.Items.AddRange(new DevExpress.XtraEditors.Controls.ImageComboBoxItem[] {
            new DevExpress.XtraEditors.Controls.ImageComboBoxItem("Paga Todos", true, -1),
            new DevExpress.XtraEditors.Controls.ImageComboBoxItem("Não Paga nenhum", false, -1),
            new DevExpress.XtraEditors.Controls.ImageComboBoxItem("Segue o rateio", null, -1)});
            this.imageComboBoxEdit1.Properties.NullText = "Segue o rateio";
            this.imageComboBoxEdit1.Size = new System.Drawing.Size(497, 20);
            this.imageComboBoxEdit1.StyleController = this.LayoutControl;
            this.imageComboBoxEdit1.TabIndex = 17;
            // 
            // lookUpEdit1
            // 
            this.lookUpEdit1.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.BindingSource_F, "APTJur_FRN", true));
            this.lookUpEdit1.Location = new System.Drawing.Point(932, 46);
            this.lookUpEdit1.Name = "lookUpEdit1";
            this.lookUpEdit1.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.True;
            this.lookUpEdit1.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo),
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Delete)});
            this.lookUpEdit1.Properties.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("FRNNome", "Nome")});
            this.lookUpEdit1.Properties.DataSource = this.bindingSourceFRN;
            this.lookUpEdit1.Properties.DisplayMember = "FRNNome";
            this.lookUpEdit1.Properties.NullText = "Nenhum";
            this.lookUpEdit1.Properties.ShowHeader = false;
            this.lookUpEdit1.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.Standard;
            this.lookUpEdit1.Properties.ValueMember = "FRN";
            this.lookUpEdit1.Size = new System.Drawing.Size(291, 20);
            this.lookUpEdit1.StyleController = this.LayoutControl;
            this.lookUpEdit1.TabIndex = 5;
            this.lookUpEdit1.ButtonClick += new DevExpress.XtraEditors.Controls.ButtonPressedEventHandler(this.lookUpEdit1_ButtonClick);
            // 
            // bindingSourceFRN
            // 
            this.bindingSourceFRN.DataMember = "FORNECEDORES";
            this.bindingSourceFRN.DataSource = typeof(Cadastros.Fornecedores.dFornecedoresCampos);
            // 
            // checkEdit7
            // 
            this.checkEdit7.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.BindingSource_F, "APTBEmailP", true));
            this.checkEdit7.Location = new System.Drawing.Point(1046, 140);
            this.checkEdit7.Name = "checkEdit7";
            this.checkEdit7.Properties.Caption = "Boletos por e.mail";
            this.checkEdit7.Size = new System.Drawing.Size(177, 19);
            this.checkEdit7.StyleController = this.LayoutControl;
            this.checkEdit7.TabIndex = 10;
            // 
            // checkEdit6
            // 
            this.checkEdit6.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.BindingSource_F, "APTBEmailI", true));
            this.checkEdit6.Location = new System.Drawing.Point(1040, 268);
            this.checkEdit6.Name = "checkEdit6";
            this.checkEdit6.Properties.Caption = "Boletos por e.mail";
            this.checkEdit6.Size = new System.Drawing.Size(168, 19);
            this.checkEdit6.StyleController = this.LayoutControl;
            this.checkEdit6.TabIndex = 15;
            // 
            // spinEdit2
            // 
            this.spinEdit2.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.BindingSource_F, "APTDiaDiferenteREF", true));
            this.spinEdit2.EditValue = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.spinEdit2.Location = new System.Drawing.Point(810, 46);
            this.spinEdit2.Name = "spinEdit2";
            this.spinEdit2.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.spinEdit2.Size = new System.Drawing.Size(94, 20);
            this.spinEdit2.StyleController = this.LayoutControl;
            this.spinEdit2.TabIndex = 4;
            // 
            // gridControl1
            // 
            this.gridControl1.DataSource = this.aPTPGEBindingSource;
            this.gridControl1.Location = new System.Drawing.Point(12, 437);
            this.gridControl1.MainView = this.gridView1;
            this.gridControl1.Name = "gridControl1";
            this.gridControl1.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemImageComboBox1,
            this.repositoryItemCheckEdit1});
            this.gridControl1.Size = new System.Drawing.Size(1223, 231);
            this.gridControl1.TabIndex = 0;
            this.gridControl1.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridView1});
            // 
            // aPTPGEBindingSource
            // 
            this.aPTPGEBindingSource.DataMember = "APTPGE";
            this.aPTPGEBindingSource.DataSource = this.dApartamentosCampos;
            // 
            // dApartamentosCampos
            // 
            this.dApartamentosCampos.DataSetName = "dApartamentosCampos";
            this.dApartamentosCampos.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // gridView1
            // 
            this.gridView1.Appearance.ColumnFilterButton.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(240)))), ((int)(((byte)(177)))), ((int)(((byte)(94)))));
            this.gridView1.Appearance.ColumnFilterButton.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(240)))), ((int)(((byte)(177)))), ((int)(((byte)(94)))));
            this.gridView1.Appearance.ColumnFilterButton.ForeColor = System.Drawing.Color.Black;
            this.gridView1.Appearance.ColumnFilterButton.Options.UseBackColor = true;
            this.gridView1.Appearance.ColumnFilterButton.Options.UseBorderColor = true;
            this.gridView1.Appearance.ColumnFilterButton.Options.UseForeColor = true;
            this.gridView1.Appearance.ColumnFilterButtonActive.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(248)))), ((int)(((byte)(204)))), ((int)(((byte)(124)))));
            this.gridView1.Appearance.ColumnFilterButtonActive.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(248)))), ((int)(((byte)(204)))), ((int)(((byte)(124)))));
            this.gridView1.Appearance.ColumnFilterButtonActive.ForeColor = System.Drawing.Color.Black;
            this.gridView1.Appearance.ColumnFilterButtonActive.Options.UseBackColor = true;
            this.gridView1.Appearance.ColumnFilterButtonActive.Options.UseBorderColor = true;
            this.gridView1.Appearance.ColumnFilterButtonActive.Options.UseForeColor = true;
            this.gridView1.Appearance.Empty.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(251)))), ((int)(((byte)(244)))), ((int)(((byte)(232)))));
            this.gridView1.Appearance.Empty.BackColor2 = System.Drawing.Color.White;
            this.gridView1.Appearance.Empty.Options.UseBackColor = true;
            this.gridView1.Appearance.EvenRow.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(253)))), ((int)(((byte)(248)))), ((int)(((byte)(236)))));
            this.gridView1.Appearance.EvenRow.ForeColor = System.Drawing.Color.Black;
            this.gridView1.Appearance.EvenRow.Options.UseBackColor = true;
            this.gridView1.Appearance.EvenRow.Options.UseForeColor = true;
            this.gridView1.Appearance.FilterCloseButton.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(240)))), ((int)(((byte)(177)))), ((int)(((byte)(94)))));
            this.gridView1.Appearance.FilterCloseButton.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(240)))), ((int)(((byte)(177)))), ((int)(((byte)(94)))));
            this.gridView1.Appearance.FilterCloseButton.ForeColor = System.Drawing.Color.Black;
            this.gridView1.Appearance.FilterCloseButton.Options.UseBackColor = true;
            this.gridView1.Appearance.FilterCloseButton.Options.UseBorderColor = true;
            this.gridView1.Appearance.FilterCloseButton.Options.UseForeColor = true;
            this.gridView1.Appearance.FilterPanel.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(251)))), ((int)(((byte)(244)))), ((int)(((byte)(232)))));
            this.gridView1.Appearance.FilterPanel.BackColor2 = System.Drawing.Color.White;
            this.gridView1.Appearance.FilterPanel.ForeColor = System.Drawing.Color.Black;
            this.gridView1.Appearance.FilterPanel.Options.UseBackColor = true;
            this.gridView1.Appearance.FilterPanel.Options.UseForeColor = true;
            this.gridView1.Appearance.FixedLine.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(218)))), ((int)(((byte)(153)))), ((int)(((byte)(73)))));
            this.gridView1.Appearance.FixedLine.Options.UseBackColor = true;
            this.gridView1.Appearance.FocusedCell.BackColor = System.Drawing.Color.White;
            this.gridView1.Appearance.FocusedCell.ForeColor = System.Drawing.Color.Black;
            this.gridView1.Appearance.FocusedCell.Options.UseBackColor = true;
            this.gridView1.Appearance.FocusedCell.Options.UseForeColor = true;
            this.gridView1.Appearance.FocusedRow.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(217)))), ((int)(((byte)(154)))), ((int)(((byte)(91)))));
            this.gridView1.Appearance.FocusedRow.ForeColor = System.Drawing.Color.White;
            this.gridView1.Appearance.FocusedRow.Options.UseBackColor = true;
            this.gridView1.Appearance.FocusedRow.Options.UseForeColor = true;
            this.gridView1.Appearance.FooterPanel.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(240)))), ((int)(((byte)(177)))), ((int)(((byte)(94)))));
            this.gridView1.Appearance.FooterPanel.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(240)))), ((int)(((byte)(177)))), ((int)(((byte)(94)))));
            this.gridView1.Appearance.FooterPanel.ForeColor = System.Drawing.Color.Black;
            this.gridView1.Appearance.FooterPanel.Options.UseBackColor = true;
            this.gridView1.Appearance.FooterPanel.Options.UseBorderColor = true;
            this.gridView1.Appearance.FooterPanel.Options.UseForeColor = true;
            this.gridView1.Appearance.GroupButton.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(248)))), ((int)(((byte)(204)))), ((int)(((byte)(124)))));
            this.gridView1.Appearance.GroupButton.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(248)))), ((int)(((byte)(204)))), ((int)(((byte)(124)))));
            this.gridView1.Appearance.GroupButton.Options.UseBackColor = true;
            this.gridView1.Appearance.GroupButton.Options.UseBorderColor = true;
            this.gridView1.Appearance.GroupFooter.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(248)))), ((int)(((byte)(204)))), ((int)(((byte)(124)))));
            this.gridView1.Appearance.GroupFooter.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(248)))), ((int)(((byte)(204)))), ((int)(((byte)(124)))));
            this.gridView1.Appearance.GroupFooter.ForeColor = System.Drawing.Color.Black;
            this.gridView1.Appearance.GroupFooter.Options.UseBackColor = true;
            this.gridView1.Appearance.GroupFooter.Options.UseBorderColor = true;
            this.gridView1.Appearance.GroupFooter.Options.UseForeColor = true;
            this.gridView1.Appearance.GroupPanel.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(251)))), ((int)(((byte)(244)))), ((int)(((byte)(232)))));
            this.gridView1.Appearance.GroupPanel.BackColor2 = System.Drawing.Color.White;
            this.gridView1.Appearance.GroupPanel.ForeColor = System.Drawing.Color.Black;
            this.gridView1.Appearance.GroupPanel.Options.UseBackColor = true;
            this.gridView1.Appearance.GroupPanel.Options.UseForeColor = true;
            this.gridView1.Appearance.GroupRow.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(248)))), ((int)(((byte)(204)))), ((int)(((byte)(124)))));
            this.gridView1.Appearance.GroupRow.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(248)))), ((int)(((byte)(204)))), ((int)(((byte)(124)))));
            this.gridView1.Appearance.GroupRow.ForeColor = System.Drawing.Color.Black;
            this.gridView1.Appearance.GroupRow.Options.UseBackColor = true;
            this.gridView1.Appearance.GroupRow.Options.UseBorderColor = true;
            this.gridView1.Appearance.GroupRow.Options.UseForeColor = true;
            this.gridView1.Appearance.HeaderPanel.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(248)))), ((int)(((byte)(204)))), ((int)(((byte)(124)))));
            this.gridView1.Appearance.HeaderPanel.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(248)))), ((int)(((byte)(204)))), ((int)(((byte)(124)))));
            this.gridView1.Appearance.HeaderPanel.ForeColor = System.Drawing.Color.Black;
            this.gridView1.Appearance.HeaderPanel.Options.UseBackColor = true;
            this.gridView1.Appearance.HeaderPanel.Options.UseBorderColor = true;
            this.gridView1.Appearance.HeaderPanel.Options.UseForeColor = true;
            this.gridView1.Appearance.HideSelectionRow.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(225)))), ((int)(((byte)(183)))), ((int)(((byte)(125)))));
            this.gridView1.Appearance.HideSelectionRow.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(251)))), ((int)(((byte)(244)))), ((int)(((byte)(232)))));
            this.gridView1.Appearance.HideSelectionRow.Options.UseBackColor = true;
            this.gridView1.Appearance.HideSelectionRow.Options.UseForeColor = true;
            this.gridView1.Appearance.HorzLine.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(240)))), ((int)(((byte)(177)))), ((int)(((byte)(94)))));
            this.gridView1.Appearance.HorzLine.Options.UseBackColor = true;
            this.gridView1.Appearance.OddRow.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(251)))), ((int)(((byte)(236)))), ((int)(((byte)(208)))));
            this.gridView1.Appearance.OddRow.ForeColor = System.Drawing.Color.Black;
            this.gridView1.Appearance.OddRow.Options.UseBackColor = true;
            this.gridView1.Appearance.OddRow.Options.UseForeColor = true;
            this.gridView1.Appearance.Preview.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(254)))), ((int)(((byte)(249)))));
            this.gridView1.Appearance.Preview.Font = new System.Drawing.Font("Verdana", 7.5F);
            this.gridView1.Appearance.Preview.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(186)))), ((int)(((byte)(146)))), ((int)(((byte)(78)))));
            this.gridView1.Appearance.Preview.Options.UseBackColor = true;
            this.gridView1.Appearance.Preview.Options.UseFont = true;
            this.gridView1.Appearance.Preview.Options.UseForeColor = true;
            this.gridView1.Appearance.Row.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(253)))), ((int)(((byte)(248)))), ((int)(((byte)(236)))));
            this.gridView1.Appearance.Row.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(253)))), ((int)(((byte)(248)))), ((int)(((byte)(236)))));
            this.gridView1.Appearance.Row.ForeColor = System.Drawing.Color.Black;
            this.gridView1.Appearance.Row.Options.UseBackColor = true;
            this.gridView1.Appearance.Row.Options.UseBorderColor = true;
            this.gridView1.Appearance.Row.Options.UseForeColor = true;
            this.gridView1.Appearance.RowSeparator.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(251)))), ((int)(((byte)(244)))), ((int)(((byte)(232)))));
            this.gridView1.Appearance.RowSeparator.BackColor2 = System.Drawing.Color.White;
            this.gridView1.Appearance.RowSeparator.Options.UseBackColor = true;
            this.gridView1.Appearance.SelectedRow.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(231)))), ((int)(((byte)(167)))), ((int)(((byte)(103)))));
            this.gridView1.Appearance.SelectedRow.Options.UseBackColor = true;
            this.gridView1.Appearance.TopNewRow.BackColor = System.Drawing.Color.White;
            this.gridView1.Appearance.TopNewRow.Options.UseBackColor = true;
            this.gridView1.Appearance.VertLine.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(240)))), ((int)(((byte)(177)))), ((int)(((byte)(94)))));
            this.gridView1.Appearance.VertLine.Options.UseBackColor = true;
            this.gridView1.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colAPTPGEPaga,
            this.colPGE_PLA,
            this.colPGEMensagem,
            this.colPGEValor,
            this.colAPTPGEDATAI,
            this.colAPTPGEDATAA,
            this.colAPTPGEA_USU,
            this.colAPTPGEI_USU,
            this.colAPTGEIProprietario});
            this.gridView1.GridControl = this.gridControl1;
            this.gridView1.Name = "gridView1";
            this.gridView1.OptionsView.EnableAppearanceEvenRow = true;
            this.gridView1.OptionsView.EnableAppearanceOddRow = true;
            this.gridView1.OptionsView.ShowGroupPanel = false;
            // 
            // colAPTPGEPaga
            // 
            this.colAPTPGEPaga.Caption = "Pagar";
            this.colAPTPGEPaga.ColumnEdit = this.repositoryItemCheckEdit1;
            this.colAPTPGEPaga.FieldName = "APTPGEPaga";
            this.colAPTPGEPaga.Name = "colAPTPGEPaga";
            this.colAPTPGEPaga.Visible = true;
            this.colAPTPGEPaga.VisibleIndex = 0;
            this.colAPTPGEPaga.Width = 58;
            // 
            // repositoryItemCheckEdit1
            // 
            this.repositoryItemCheckEdit1.AutoHeight = false;
            this.repositoryItemCheckEdit1.Name = "repositoryItemCheckEdit1";
            this.repositoryItemCheckEdit1.CheckedChanged += new System.EventHandler(this.repositoryItemCheckEdit1_CheckedChanged);
            // 
            // colPGE_PLA
            // 
            this.colPGE_PLA.Caption = "Tipo";
            this.colPGE_PLA.FieldName = "PGE_PLA";
            this.colPGE_PLA.Name = "colPGE_PLA";
            this.colPGE_PLA.OptionsColumn.AllowEdit = false;
            this.colPGE_PLA.Visible = true;
            this.colPGE_PLA.VisibleIndex = 2;
            this.colPGE_PLA.Width = 100;
            // 
            // colPGEMensagem
            // 
            this.colPGEMensagem.Caption = "Mensagem";
            this.colPGEMensagem.FieldName = "PGEMensagem";
            this.colPGEMensagem.Name = "colPGEMensagem";
            this.colPGEMensagem.OptionsColumn.AllowEdit = false;
            this.colPGEMensagem.Visible = true;
            this.colPGEMensagem.VisibleIndex = 3;
            this.colPGEMensagem.Width = 187;
            // 
            // colPGEValor
            // 
            this.colPGEValor.Caption = "Valor";
            this.colPGEValor.FieldName = "PGEValor";
            this.colPGEValor.Name = "colPGEValor";
            this.colPGEValor.OptionsColumn.AllowEdit = false;
            this.colPGEValor.Visible = true;
            this.colPGEValor.VisibleIndex = 4;
            this.colPGEValor.Width = 167;
            // 
            // colAPTPGEDATAI
            // 
            this.colAPTPGEDATAI.FieldName = "APTPGEDATAI";
            this.colAPTPGEDATAI.Name = "colAPTPGEDATAI";
            this.colAPTPGEDATAI.OptionsColumn.AllowEdit = false;
            this.colAPTPGEDATAI.Visible = true;
            this.colAPTPGEDATAI.VisibleIndex = 6;
            this.colAPTPGEDATAI.Width = 110;
            // 
            // colAPTPGEDATAA
            // 
            this.colAPTPGEDATAA.FieldName = "APTPGEDATAA";
            this.colAPTPGEDATAA.Name = "colAPTPGEDATAA";
            this.colAPTPGEDATAA.OptionsColumn.AllowEdit = false;
            this.colAPTPGEDATAA.Visible = true;
            this.colAPTPGEDATAA.VisibleIndex = 8;
            this.colAPTPGEDATAA.Width = 111;
            // 
            // colAPTPGEA_USU
            // 
            this.colAPTPGEA_USU.Caption = "Operador";
            this.colAPTPGEA_USU.FieldName = "APTPGEA_USU";
            this.colAPTPGEA_USU.Name = "colAPTPGEA_USU";
            this.colAPTPGEA_USU.OptionsColumn.AllowEdit = false;
            this.colAPTPGEA_USU.Visible = true;
            this.colAPTPGEA_USU.VisibleIndex = 7;
            this.colAPTPGEA_USU.Width = 140;
            // 
            // colAPTPGEI_USU
            // 
            this.colAPTPGEI_USU.Caption = "Cadastro";
            this.colAPTPGEI_USU.FieldName = "APTPGEI_USU";
            this.colAPTPGEI_USU.Name = "colAPTPGEI_USU";
            this.colAPTPGEI_USU.OptionsColumn.AllowEdit = false;
            this.colAPTPGEI_USU.Visible = true;
            this.colAPTPGEI_USU.VisibleIndex = 5;
            this.colAPTPGEI_USU.Width = 124;
            // 
            // colAPTGEIProprietario
            // 
            this.colAPTGEIProprietario.Caption = "Destinatário";
            this.colAPTGEIProprietario.ColumnEdit = this.repositoryItemImageComboBox1;
            this.colAPTGEIProprietario.FieldName = "APTPGEProprietario";
            this.colAPTGEIProprietario.Name = "colAPTGEIProprietario";
            this.colAPTGEIProprietario.Visible = true;
            this.colAPTGEIProprietario.VisibleIndex = 1;
            this.colAPTGEIProprietario.Width = 91;
            // 
            // repositoryItemImageComboBox1
            // 
            this.repositoryItemImageComboBox1.AutoHeight = false;
            this.repositoryItemImageComboBox1.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemImageComboBox1.GlyphAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.repositoryItemImageComboBox1.Items.AddRange(new DevExpress.XtraEditors.Controls.ImageComboBoxItem[] {
            new DevExpress.XtraEditors.Controls.ImageComboBoxItem("Proprietário", true, 0),
            new DevExpress.XtraEditors.Controls.ImageComboBoxItem("Inquilino", false, 1)});
            this.repositoryItemImageComboBox1.LargeImages = this.imagensPI1;
            this.repositoryItemImageComboBox1.Name = "repositoryItemImageComboBox1";
            // 
            // imagensPI1
            // 
            this.imagensPI1.ImageStream = ((DevExpress.Utils.ImageCollectionStreamer)(resources.GetObject("imagensPI1.ImageStream")));
            // 
            // aPTSenhaInternetInquilinoTextEdit
            // 
            this.aPTSenhaInternetInquilinoTextEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.BindingSource_F, "APTSenhaInternetInquilino", true));
            this.aPTSenhaInternetInquilinoTextEdit.Enabled = false;
            this.aPTSenhaInternetInquilinoTextEdit.Location = new System.Drawing.Point(683, 268);
            this.aPTSenhaInternetInquilinoTextEdit.Name = "aPTSenhaInternetInquilinoTextEdit";
            this.spellChecker1.SetShowSpellCheckMenu(this.aPTSenhaInternetInquilinoTextEdit, true);
            this.aPTSenhaInternetInquilinoTextEdit.Size = new System.Drawing.Size(353, 20);
            this.spellChecker1.SetSpellCheckerOptions(this.aPTSenhaInternetInquilinoTextEdit, optionsSpelling2);
            this.aPTSenhaInternetInquilinoTextEdit.StyleController = this.LayoutControl;
            this.aPTSenhaInternetInquilinoTextEdit.TabIndex = 14;
            // 
            // aPTUsuarioInternetInquilinoTextEdit
            // 
            this.aPTUsuarioInternetInquilinoTextEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.BindingSource_F, "APTUsuarioInternetInquilino", true));
            this.aPTUsuarioInternetInquilinoTextEdit.Enabled = false;
            this.aPTUsuarioInternetInquilinoTextEdit.Location = new System.Drawing.Point(98, 268);
            this.aPTUsuarioInternetInquilinoTextEdit.Name = "aPTUsuarioInternetInquilinoTextEdit";
            this.spellChecker1.SetShowSpellCheckMenu(this.aPTUsuarioInternetInquilinoTextEdit, true);
            this.aPTUsuarioInternetInquilinoTextEdit.Size = new System.Drawing.Size(522, 20);
            this.spellChecker1.SetSpellCheckerOptions(this.aPTUsuarioInternetInquilinoTextEdit, optionsSpelling3);
            this.aPTUsuarioInternetInquilinoTextEdit.StyleController = this.LayoutControl;
            this.aPTUsuarioInternetInquilinoTextEdit.TabIndex = 13;
            // 
            // aPTSenhaInternetProprietarioTextEdit
            // 
            this.aPTSenhaInternetProprietarioTextEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.BindingSource_F, "APTSenhaInternetProprietario", true));
            this.aPTSenhaInternetProprietarioTextEdit.Enabled = false;
            this.aPTSenhaInternetProprietarioTextEdit.Location = new System.Drawing.Point(518, 140);
            this.aPTSenhaInternetProprietarioTextEdit.Name = "aPTSenhaInternetProprietarioTextEdit";
            this.spellChecker1.SetShowSpellCheckMenu(this.aPTSenhaInternetProprietarioTextEdit, true);
            this.aPTSenhaInternetProprietarioTextEdit.Size = new System.Drawing.Size(524, 20);
            this.spellChecker1.SetSpellCheckerOptions(this.aPTSenhaInternetProprietarioTextEdit, optionsSpelling4);
            this.aPTSenhaInternetProprietarioTextEdit.StyleController = this.LayoutControl;
            this.aPTSenhaInternetProprietarioTextEdit.TabIndex = 9;
            // 
            // aPTUsuarioInternetProprietarioTextEdit
            // 
            this.aPTUsuarioInternetProprietarioTextEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.BindingSource_F, "APTUsuarioInternetProprietario", true));
            this.aPTUsuarioInternetProprietarioTextEdit.Enabled = false;
            this.aPTUsuarioInternetProprietarioTextEdit.Location = new System.Drawing.Point(83, 140);
            this.aPTUsuarioInternetProprietarioTextEdit.Name = "aPTUsuarioInternetProprietarioTextEdit";
            this.spellChecker1.SetShowSpellCheckMenu(this.aPTUsuarioInternetProprietarioTextEdit, true);
            this.aPTUsuarioInternetProprietarioTextEdit.Size = new System.Drawing.Size(372, 20);
            this.spellChecker1.SetSpellCheckerOptions(this.aPTUsuarioInternetProprietarioTextEdit, optionsSpelling5);
            this.aPTUsuarioInternetProprietarioTextEdit.StyleController = this.LayoutControl;
            this.aPTUsuarioInternetProprietarioTextEdit.TabIndex = 8;
            // 
            // lkpImobiliaria
            // 
            this.lkpImobiliaria.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.BindingSource_F, "APTImobiliaria_FRN", true));
            this.lkpImobiliaria.Location = new System.Drawing.Point(92, 368);
            this.lkpImobiliaria.Name = "lkpImobiliaria";
            this.lkpImobiliaria.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo),
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Plus),
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Ellipsis, "", -1, true, false, false, DevExpress.XtraEditors.ImageLocation.MiddleCenter, null, new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), serializableAppearanceObject1, "", null, null, false)});
            this.lkpImobiliaria.Properties.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("FRNNome", "Imobiliária", 53, DevExpress.Utils.FormatType.None, "", true, DevExpress.Utils.HorzAlignment.Near)});
            this.lkpImobiliaria.Properties.DataSource = this.iMOBILIARIASBindingSource;
            this.lkpImobiliaria.Properties.DisplayMember = "FRNNome";
            this.lkpImobiliaria.Properties.ValueMember = "FRN";
            this.lkpImobiliaria.Size = new System.Drawing.Size(1116, 20);
            this.lkpImobiliaria.StyleController = this.LayoutControl;
            this.lkpImobiliaria.TabIndex = 20;
            this.lkpImobiliaria.ButtonClick += new DevExpress.XtraEditors.Controls.ButtonPressedEventHandler(this.lkpImobiliaria_ButtonClick);
            // 
            // iMOBILIARIASBindingSource
            // 
            this.iMOBILIARIASBindingSource.DataSource = this.dCondominiosCampos;
            this.iMOBILIARIASBindingSource.Position = 0;
            // 
            // dCondominiosCampos
            // 
            this.dCondominiosCampos.DataSetName = "dCondominiosCampos";
            this.dCondominiosCampos.EnforceConstraints = false;
            this.dCondominiosCampos.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // checkEdit1
            // 
            this.checkEdit1.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.BindingSource_F, "APTInquilinoPagaFR", true));
            this.checkEdit1.Location = new System.Drawing.Point(51, 326);
            this.checkEdit1.Name = "checkEdit1";
            this.checkEdit1.Properties.Caption = "Fundo de Reserva";
            this.checkEdit1.Size = new System.Drawing.Size(315, 19);
            this.checkEdit1.StyleController = this.LayoutControl;
            this.checkEdit1.TabIndex = 16;
            // 
            // lkpInquilino
            // 
            this.lkpInquilino.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.BindingSource_F, "APTInquilino_PES", true));
            this.lkpInquilino.Location = new System.Drawing.Point(98, 244);
            this.lkpInquilino.Name = "lkpInquilino";
            this.lkpInquilino.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo),
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Plus),
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Minus),
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.lkpInquilino.Properties.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("PESNome", "Nome", 51, DevExpress.Utils.FormatType.None, "", true, DevExpress.Utils.HorzAlignment.Near),
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("PESEndereco", "Endereço", 69, DevExpress.Utils.FormatType.None, "", true, DevExpress.Utils.HorzAlignment.Near),
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("PESBairro", "Bairro", 52, DevExpress.Utils.FormatType.None, "", true, DevExpress.Utils.HorzAlignment.Near),
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("CIDNome", "Cidade", 51, DevExpress.Utils.FormatType.None, "", true, DevExpress.Utils.HorzAlignment.Near),
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("PESUf", "UF", 35, DevExpress.Utils.FormatType.None, "", true, DevExpress.Utils.HorzAlignment.Near)});
            this.lkpInquilino.Properties.DataSource = this.pESSOASBindingSource;
            this.lkpInquilino.Properties.DisplayMember = "PESNome";
            this.lkpInquilino.Properties.ValueMember = "PES";
            this.lkpInquilino.Size = new System.Drawing.Size(671, 20);
            this.lkpInquilino.StyleController = this.LayoutControl;
            this.lkpInquilino.TabIndex = 11;
            this.lkpInquilino.ButtonClick += new DevExpress.XtraEditors.Controls.ButtonPressedEventHandler(this.lkpPESSOAS_ButtonClick);
            // 
            // pESSOASBindingSource
            // 
            this.pESSOASBindingSource.DataMember = "PESSOAS";
            this.pESSOASBindingSource.DataSource = typeof(Cadastros.dEstatico);
            // 
            // checkEdit4
            // 
            this.checkEdit4.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.BindingSource_F, "APTDiaDiferente", true));
            this.checkEdit4.Location = new System.Drawing.Point(579, 46);
            this.checkEdit4.Name = "checkEdit4";
            this.checkEdit4.Properties.Caption = "Venc. Dif..";
            this.checkEdit4.Size = new System.Drawing.Size(117, 19);
            this.checkEdit4.StyleController = this.LayoutControl;
            this.checkEdit4.TabIndex = 3;
            this.checkEdit4.Click += new System.EventHandler(this.checkEdit4_Click);
            // 
            // lkpPagtoInquilino
            // 
            this.lkpPagtoInquilino.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.BindingSource_F, "APTFormaPagtoInquilino_FPG", true));
            this.lkpPagtoInquilino.Location = new System.Drawing.Point(832, 244);
            this.lkpPagtoInquilino.Name = "lkpPagtoInquilino";
            this.lkpPagtoInquilino.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.lkpPagtoInquilino.Properties.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("FPGNome", "Forma de Pagamento", 52, DevExpress.Utils.FormatType.None, "", true, DevExpress.Utils.HorzAlignment.Near)});
            this.lkpPagtoInquilino.Properties.DataSource = this.fORMAPAGTOBindingSource;
            this.lkpPagtoInquilino.Properties.DisplayMember = "FPGNome";
            this.lkpPagtoInquilino.Properties.ValueMember = "FPG";
            this.lkpPagtoInquilino.Size = new System.Drawing.Size(376, 20);
            this.lkpPagtoInquilino.StyleController = this.LayoutControl;
            this.lkpPagtoInquilino.TabIndex = 12;
            this.lkpPagtoInquilino.EditValueChanged += new System.EventHandler(this.lkpPagtoProprietario_EditValueChanged);
            // 
            // fORMAPAGTOBindingSource
            // 
            this.fORMAPAGTOBindingSource.DataMember = "FORMAPAGTO";
            this.fORMAPAGTOBindingSource.DataSource = this.dCondominiosCampos;
            // 
            // lkpProprietario
            // 
            this.lkpProprietario.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.BindingSource_F, "APTProprietario_PES", true));
            this.lkpProprietario.Location = new System.Drawing.Point(83, 116);
            this.lkpProprietario.Name = "lkpProprietario";
            this.lkpProprietario.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo),
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Plus),
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Minus),
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.lkpProprietario.Properties.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("PESNome", "Nome", 51, DevExpress.Utils.FormatType.None, "", true, DevExpress.Utils.HorzAlignment.Near),
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("PESEndereco", "Endereço", 69, DevExpress.Utils.FormatType.None, "", true, DevExpress.Utils.HorzAlignment.Near),
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("PESBairro", "Bairro", 52, DevExpress.Utils.FormatType.None, "", true, DevExpress.Utils.HorzAlignment.Near),
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("CIDNome", "Cidade", 51, DevExpress.Utils.FormatType.None, "", true, DevExpress.Utils.HorzAlignment.Near),
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("PESUf", "UF", 35, DevExpress.Utils.FormatType.None, "", true, DevExpress.Utils.HorzAlignment.Near)});
            this.lkpProprietario.Properties.DataSource = this.pESSOASBindingSource;
            this.lkpProprietario.Properties.DisplayMember = "PESNome";
            this.lkpProprietario.Properties.ValueMember = "PES";
            this.lkpProprietario.Size = new System.Drawing.Size(684, 20);
            this.lkpProprietario.StyleController = this.LayoutControl;
            this.lkpProprietario.TabIndex = 6;
            this.lkpProprietario.ButtonClick += new DevExpress.XtraEditors.Controls.ButtonPressedEventHandler(this.lkpPESSOAS_ButtonClick);
            // 
            // lkpPagtoProprietario
            // 
            this.lkpPagtoProprietario.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.BindingSource_F, "APTFormaPagtoProprietario_FPG", true));
            this.lkpPagtoProprietario.Location = new System.Drawing.Point(830, 116);
            this.lkpPagtoProprietario.Name = "lkpPagtoProprietario";
            this.lkpPagtoProprietario.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.lkpPagtoProprietario.Properties.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("FPGNome", "Forma de Pagamento", 52, DevExpress.Utils.FormatType.None, "", true, DevExpress.Utils.HorzAlignment.Near)});
            this.lkpPagtoProprietario.Properties.DataSource = this.fORMAPAGTOBindingSource;
            this.lkpPagtoProprietario.Properties.DisplayMember = "FPGNome";
            this.lkpPagtoProprietario.Properties.ValueMember = "FPG";
            this.lkpPagtoProprietario.Size = new System.Drawing.Size(393, 20);
            this.lkpPagtoProprietario.StyleController = this.LayoutControl;
            this.lkpPagtoProprietario.TabIndex = 7;
            this.lkpPagtoProprietario.EditValueChanged += new System.EventHandler(this.lkpPagtoProprietario_EditValueChanged);
            // 
            // txtIdentificacao
            // 
            this.txtIdentificacao.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.BindingSource_F, "APTNumero", true));
            this.txtIdentificacao.Location = new System.Drawing.Point(24, 46);
            this.txtIdentificacao.Name = "txtIdentificacao";
            this.txtIdentificacao.Properties.MaxLength = 4;
            this.spellChecker1.SetShowSpellCheckMenu(this.txtIdentificacao, true);
            this.txtIdentificacao.Size = new System.Drawing.Size(115, 20);
            this.spellChecker1.SetSpellCheckerOptions(this.txtIdentificacao, optionsSpelling6);
            this.txtIdentificacao.StyleController = this.LayoutControl;
            this.txtIdentificacao.TabIndex = 0;
            // 
            // checkEdit5
            // 
            this.checkEdit5.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.BindingSource_F, "APTInquilinoPagaCondominio", true));
            this.checkEdit5.Location = new System.Drawing.Point(370, 326);
            this.checkEdit5.Name = "checkEdit5";
            this.checkEdit5.Properties.Caption = "Condomínio";
            this.checkEdit5.Size = new System.Drawing.Size(289, 19);
            this.checkEdit5.StyleController = this.LayoutControl;
            this.checkEdit5.TabIndex = 18;
            // 
            // clcFracaoIdeal
            // 
            this.clcFracaoIdeal.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.BindingSource_F, "APTFracaoIdeal", true));
            this.clcFracaoIdeal.Location = new System.Drawing.Point(143, 46);
            this.clcFracaoIdeal.Name = "clcFracaoIdeal";
            this.clcFracaoIdeal.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.clcFracaoIdeal.Properties.DisplayFormat.FormatString = "n4";
            this.clcFracaoIdeal.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.clcFracaoIdeal.Size = new System.Drawing.Size(151, 20);
            this.clcFracaoIdeal.StyleController = this.LayoutControl;
            this.clcFracaoIdeal.TabIndex = 1;
            // 
            // spinEdit1
            // 
            this.spinEdit1.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.BindingSource_F, "APTDiaCondominio", true));
            this.spinEdit1.EditValue = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.spinEdit1.Location = new System.Drawing.Point(700, 46);
            this.spinEdit1.Name = "spinEdit1";
            this.spinEdit1.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.spinEdit1.Properties.IsFloatValue = false;
            this.spinEdit1.Properties.Mask.EditMask = "N00";
            this.spinEdit1.Properties.MaxValue = new decimal(new int[] {
            31,
            0,
            0,
            0});
            this.spinEdit1.Properties.MinValue = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.spinEdit1.Size = new System.Drawing.Size(80, 20);
            this.spinEdit1.StyleController = this.LayoutControl;
            this.spinEdit1.TabIndex = 2;
            this.spinEdit1.ValueChanged += new System.EventHandler(this.spinEdit1_ValueChanged);
            // 
            // LayoutControlGroup
            // 
            this.LayoutControlGroup.CustomizationFormText = "LayoutControlGroup";
            this.LayoutControlGroup.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlGroup1,
            this.layoutControlGroup4,
            this.layoutControlGroup3,
            this.layoutControlItem18,
            this.layoutControlGroup10,
            this.layoutControlGroup9,
            this.layoutControlGroup7});
            this.LayoutControlGroup.Location = new System.Drawing.Point(0, 0);
            this.LayoutControlGroup.Name = "LayoutControlGroup";
            this.LayoutControlGroup.OptionsItemText.TextToControlDistance = 5;
            this.LayoutControlGroup.Size = new System.Drawing.Size(1247, 683);
            this.LayoutControlGroup.Spacing = new DevExpress.XtraLayout.Utils.Padding(0, 0, 3, 3);
            this.LayoutControlGroup.Text = "LayoutControlGroup";
            this.LayoutControlGroup.TextVisible = false;
            // 
            // layoutControlGroup1
            // 
            this.layoutControlGroup1.CustomizationFormText = ":: Identificação / Fração ::";
            this.layoutControlGroup1.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItem1,
            this.layoutControlItem2});
            this.layoutControlGroup1.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroup1.Name = "layoutControlGroup1";
            this.layoutControlGroup1.OptionsItemText.TextToControlDistance = 5;
            this.layoutControlGroup1.Size = new System.Drawing.Size(298, 67);
            this.layoutControlGroup1.Text = ":: Identificação / Fração ::";
            // 
            // layoutControlItem1
            // 
            this.layoutControlItem1.AllowHotTrack = false;
            this.layoutControlItem1.Control = this.txtIdentificacao;
            this.layoutControlItem1.CustomizationFormText = "Identificação";
            this.layoutControlItem1.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItem1.Name = "layoutControlItem1";
            this.layoutControlItem1.Size = new System.Drawing.Size(119, 24);
            this.layoutControlItem1.Text = "Identificação";
            this.layoutControlItem1.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem1.TextToControlDistance = 0;
            this.layoutControlItem1.TextVisible = false;
            // 
            // layoutControlItem2
            // 
            this.layoutControlItem2.AllowHotTrack = false;
            this.layoutControlItem2.Control = this.clcFracaoIdeal;
            this.layoutControlItem2.CustomizationFormText = "layoutControlItem2";
            this.layoutControlItem2.Location = new System.Drawing.Point(119, 0);
            this.layoutControlItem2.Name = "layoutControlItem2";
            this.layoutControlItem2.Size = new System.Drawing.Size(155, 24);
            this.layoutControlItem2.Text = "layoutControlItem2";
            this.layoutControlItem2.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem2.TextToControlDistance = 0;
            this.layoutControlItem2.TextVisible = false;
            // 
            // layoutControlGroup4
            // 
            this.layoutControlGroup4.AppearanceGroup.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(224)))), ((int)(((byte)(192)))));
            this.layoutControlGroup4.AppearanceGroup.Options.UseBackColor = true;
            this.layoutControlGroup4.CustomizationFormText = ":: Locação ::";
            this.layoutControlGroup4.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlGroup5});
            this.layoutControlGroup4.Location = new System.Drawing.Point(0, 161);
            this.layoutControlGroup4.Name = "layoutControlGroup4";
            this.layoutControlGroup4.OptionsItemText.TextToControlDistance = 5;
            this.layoutControlGroup4.Size = new System.Drawing.Size(1227, 243);
            this.layoutControlGroup4.Spacing = new DevExpress.XtraLayout.Utils.Padding(2, 2, 5, 2);
            this.layoutControlGroup4.Text = ":: Locação ::";
            // 
            // layoutControlGroup5
            // 
            this.layoutControlGroup5.CustomizationFormText = ":: Inquilino ::";
            this.layoutControlGroup5.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItem6,
            this.layoutControlGroup6,
            this.layoutControlGroup8,
            this.layoutControlItem15,
            this.layoutControlItem16,
            this.layoutControlItem5,
            this.layoutControlItem20});
            this.layoutControlGroup5.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroup5.Name = "layoutControlGroup5";
            this.layoutControlGroup5.OptionsItemText.TextToControlDistance = 5;
            this.layoutControlGroup5.Size = new System.Drawing.Size(1203, 197);
            this.layoutControlGroup5.Spacing = new DevExpress.XtraLayout.Utils.Padding(5, 5, 5, 5);
            this.layoutControlGroup5.Text = ":: Inquilino ::";
            // 
            // layoutControlItem6
            // 
            this.layoutControlItem6.AllowHotTrack = false;
            this.layoutControlItem6.Control = this.lkpInquilino;
            this.layoutControlItem6.CustomizationFormText = "Nome";
            this.layoutControlItem6.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItem6.Name = "layoutControlItem6";
            this.layoutControlItem6.Size = new System.Drawing.Size(734, 24);
            this.layoutControlItem6.Text = "Nome";
            this.layoutControlItem6.TextSize = new System.Drawing.Size(54, 13);
            this.layoutControlItem6.TextToControlDistance = 5;
            // 
            // layoutControlGroup6
            // 
            this.layoutControlGroup6.CustomizationFormText = ":: Responsável pelo Pagamento ::";
            this.layoutControlGroup6.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItem7,
            this.layoutControlItem23,
            this.layoutControlItem17});
            this.layoutControlGroup6.Location = new System.Drawing.Point(0, 48);
            this.layoutControlGroup6.Name = "layoutControlGroup6";
            this.layoutControlGroup6.OptionsItemText.TextToControlDistance = 5;
            this.layoutControlGroup6.Size = new System.Drawing.Size(1173, 76);
            this.layoutControlGroup6.Spacing = new DevExpress.XtraLayout.Utils.Padding(2, 2, 5, 2);
            this.layoutControlGroup6.Text = ":: Responsável pelo Pagamento ::";
            // 
            // layoutControlItem7
            // 
            this.layoutControlItem7.AllowHotTrack = false;
            this.layoutControlItem7.Control = this.checkEdit1;
            this.layoutControlItem7.CustomizationFormText = "layoutControlItem7";
            this.layoutControlItem7.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItem7.Name = "layoutControlItem7";
            this.layoutControlItem7.Size = new System.Drawing.Size(319, 30);
            this.layoutControlItem7.Text = "layoutControlItem7";
            this.layoutControlItem7.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem7.TextToControlDistance = 0;
            this.layoutControlItem7.TextVisible = false;
            // 
            // layoutControlItem23
            // 
            this.layoutControlItem23.Control = this.imageComboBoxEdit1;
            this.layoutControlItem23.CustomizationFormText = "Rateio";
            this.layoutControlItem23.Location = new System.Drawing.Point(612, 0);
            this.layoutControlItem23.Name = "layoutControlItem23";
            this.layoutControlItem23.Size = new System.Drawing.Size(537, 30);
            this.layoutControlItem23.Text = "Rateio";
            this.layoutControlItem23.TextSize = new System.Drawing.Size(31, 13);
            this.layoutControlItem23.TextToControlDistance = 5;
            // 
            // layoutControlItem17
            // 
            this.layoutControlItem17.AllowHotTrack = false;
            this.layoutControlItem17.Control = this.checkEdit5;
            this.layoutControlItem17.CustomizationFormText = "layoutControlItem17";
            this.layoutControlItem17.Location = new System.Drawing.Point(319, 0);
            this.layoutControlItem17.MinSize = new System.Drawing.Size(89, 30);
            this.layoutControlItem17.Name = "layoutControlItem17";
            this.layoutControlItem17.Size = new System.Drawing.Size(293, 30);
            this.layoutControlItem17.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem17.Text = "layoutControlItem17";
            this.layoutControlItem17.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem17.TextToControlDistance = 0;
            this.layoutControlItem17.TextVisible = false;
            // 
            // layoutControlGroup8
            // 
            this.layoutControlGroup8.CustomizationFormText = ":: Imobiliária ::";
            this.layoutControlGroup8.GroupBordersVisible = false;
            this.layoutControlGroup8.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItem10});
            this.layoutControlGroup8.Location = new System.Drawing.Point(0, 124);
            this.layoutControlGroup8.Name = "layoutControlGroup8";
            this.layoutControlGroup8.OptionsItemText.TextToControlDistance = 5;
            this.layoutControlGroup8.Size = new System.Drawing.Size(1173, 24);
            this.layoutControlGroup8.Spacing = new DevExpress.XtraLayout.Utils.Padding(2, 2, 5, 2);
            this.layoutControlGroup8.Text = ":: Imobiliária ::";
            // 
            // layoutControlItem10
            // 
            this.layoutControlItem10.AllowHotTrack = false;
            this.layoutControlItem10.Control = this.lkpImobiliaria;
            this.layoutControlItem10.CustomizationFormText = "Imobiliária";
            this.layoutControlItem10.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItem10.Name = "layoutControlItem10";
            this.layoutControlItem10.Size = new System.Drawing.Size(1173, 24);
            this.layoutControlItem10.Text = "Imobiliária";
            this.layoutControlItem10.TextSize = new System.Drawing.Size(48, 13);
            this.layoutControlItem10.TextToControlDistance = 5;
            // 
            // layoutControlItem15
            // 
            this.layoutControlItem15.AllowHotTrack = false;
            this.layoutControlItem15.Control = this.aPTUsuarioInternetInquilinoTextEdit;
            this.layoutControlItem15.CustomizationFormText = "Usuario Internet";
            this.layoutControlItem15.Location = new System.Drawing.Point(0, 24);
            this.layoutControlItem15.Name = "layoutControlItem15";
            this.layoutControlItem15.Size = new System.Drawing.Size(585, 24);
            this.layoutControlItem15.Text = "Usu. Net";
            this.layoutControlItem15.TextSize = new System.Drawing.Size(54, 13);
            this.layoutControlItem15.TextToControlDistance = 5;
            // 
            // layoutControlItem16
            // 
            this.layoutControlItem16.AllowHotTrack = false;
            this.layoutControlItem16.Control = this.aPTSenhaInternetInquilinoTextEdit;
            this.layoutControlItem16.CustomizationFormText = "Senha Internet";
            this.layoutControlItem16.Location = new System.Drawing.Point(585, 24);
            this.layoutControlItem16.Name = "layoutControlItem16";
            this.layoutControlItem16.Size = new System.Drawing.Size(416, 24);
            this.layoutControlItem16.Text = "Senha Net";
            this.layoutControlItem16.TextSize = new System.Drawing.Size(54, 13);
            this.layoutControlItem16.TextToControlDistance = 5;
            // 
            // layoutControlItem5
            // 
            this.layoutControlItem5.AllowHotTrack = false;
            this.layoutControlItem5.Control = this.lkpPagtoInquilino;
            this.layoutControlItem5.CustomizationFormText = "Pagamento";
            this.layoutControlItem5.Location = new System.Drawing.Point(734, 0);
            this.layoutControlItem5.Name = "layoutControlItem5";
            this.layoutControlItem5.Size = new System.Drawing.Size(439, 24);
            this.layoutControlItem5.Text = "Pagamento";
            this.layoutControlItem5.TextSize = new System.Drawing.Size(54, 13);
            this.layoutControlItem5.TextToControlDistance = 5;
            // 
            // layoutControlItem20
            // 
            this.layoutControlItem20.Control = this.checkEdit6;
            this.layoutControlItem20.CustomizationFormText = "layoutControlItem20";
            this.layoutControlItem20.Location = new System.Drawing.Point(1001, 24);
            this.layoutControlItem20.Name = "layoutControlItem20";
            this.layoutControlItem20.Size = new System.Drawing.Size(172, 24);
            this.layoutControlItem20.Text = "layoutControlItem20";
            this.layoutControlItem20.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem20.TextToControlDistance = 0;
            this.layoutControlItem20.TextVisible = false;
            // 
            // layoutControlGroup3
            // 
            this.layoutControlGroup3.AppearanceGroup.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.layoutControlGroup3.AppearanceGroup.Options.UseBackColor = true;
            this.layoutControlGroup3.CustomizationFormText = ":: Proprietário ::";
            this.layoutControlGroup3.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItem3,
            this.layoutControlItem13,
            this.layoutControlItem14,
            this.layoutControlItem4,
            this.layoutControlItem21});
            this.layoutControlGroup3.Location = new System.Drawing.Point(0, 67);
            this.layoutControlGroup3.Name = "layoutControlGroup3";
            this.layoutControlGroup3.OptionsItemText.TextToControlDistance = 5;
            this.layoutControlGroup3.Size = new System.Drawing.Size(1227, 94);
            this.layoutControlGroup3.Spacing = new DevExpress.XtraLayout.Utils.Padding(2, 2, 5, 2);
            this.layoutControlGroup3.Text = ":: Proprietário ::";
            // 
            // layoutControlItem3
            // 
            this.layoutControlItem3.AllowHotTrack = false;
            this.layoutControlItem3.Control = this.lkpProprietario;
            this.layoutControlItem3.CustomizationFormText = "Nome";
            this.layoutControlItem3.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItem3.Name = "layoutControlItem3";
            this.layoutControlItem3.Size = new System.Drawing.Size(747, 24);
            this.layoutControlItem3.Text = "Nome";
            this.layoutControlItem3.TextSize = new System.Drawing.Size(54, 13);
            this.layoutControlItem3.TextToControlDistance = 5;
            // 
            // layoutControlItem13
            // 
            this.layoutControlItem13.AllowHotTrack = false;
            this.layoutControlItem13.Control = this.aPTUsuarioInternetProprietarioTextEdit;
            this.layoutControlItem13.CustomizationFormText = "Usu. Net";
            this.layoutControlItem13.Location = new System.Drawing.Point(0, 24);
            this.layoutControlItem13.Name = "layoutControlItem13";
            this.layoutControlItem13.Size = new System.Drawing.Size(435, 24);
            this.layoutControlItem13.Text = "Usu. Net";
            this.layoutControlItem13.TextSize = new System.Drawing.Size(54, 13);
            this.layoutControlItem13.TextToControlDistance = 5;
            // 
            // layoutControlItem14
            // 
            this.layoutControlItem14.AllowHotTrack = false;
            this.layoutControlItem14.Control = this.aPTSenhaInternetProprietarioTextEdit;
            this.layoutControlItem14.CustomizationFormText = "Senha Net";
            this.layoutControlItem14.Location = new System.Drawing.Point(435, 24);
            this.layoutControlItem14.Name = "layoutControlItem14";
            this.layoutControlItem14.Size = new System.Drawing.Size(587, 24);
            this.layoutControlItem14.Text = "Senha Net";
            this.layoutControlItem14.TextSize = new System.Drawing.Size(54, 13);
            this.layoutControlItem14.TextToControlDistance = 5;
            // 
            // layoutControlItem4
            // 
            this.layoutControlItem4.AllowHotTrack = false;
            this.layoutControlItem4.Control = this.lkpPagtoProprietario;
            this.layoutControlItem4.CustomizationFormText = "Pagamento";
            this.layoutControlItem4.Location = new System.Drawing.Point(747, 0);
            this.layoutControlItem4.Name = "layoutControlItem4";
            this.layoutControlItem4.Size = new System.Drawing.Size(456, 24);
            this.layoutControlItem4.Text = "Pagamento";
            this.layoutControlItem4.TextSize = new System.Drawing.Size(54, 13);
            this.layoutControlItem4.TextToControlDistance = 5;
            // 
            // layoutControlItem21
            // 
            this.layoutControlItem21.Control = this.checkEdit7;
            this.layoutControlItem21.CustomizationFormText = "layoutControlItem21";
            this.layoutControlItem21.Location = new System.Drawing.Point(1022, 24);
            this.layoutControlItem21.Name = "layoutControlItem21";
            this.layoutControlItem21.Size = new System.Drawing.Size(181, 24);
            this.layoutControlItem21.Text = "layoutControlItem21";
            this.layoutControlItem21.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem21.TextToControlDistance = 0;
            this.layoutControlItem21.TextVisible = false;
            // 
            // layoutControlItem18
            // 
            this.layoutControlItem18.AllowHotTrack = false;
            this.layoutControlItem18.Control = this.gridControl1;
            this.layoutControlItem18.CustomizationFormText = "Pagamentos Específicos: ";
            this.layoutControlItem18.Location = new System.Drawing.Point(0, 404);
            this.layoutControlItem18.Name = "layoutControlItem18";
            this.layoutControlItem18.Size = new System.Drawing.Size(1227, 253);
            this.layoutControlItem18.Text = "Pag. Específicos: ";
            this.layoutControlItem18.TextLocation = DevExpress.Utils.Locations.Top;
            this.layoutControlItem18.TextSize = new System.Drawing.Size(84, 13);
            this.layoutControlItem18.TextToControlDistance = 5;
            // 
            // layoutControlGroup10
            // 
            this.layoutControlGroup10.CustomizationFormText = ":: Jurídico ::";
            this.layoutControlGroup10.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItem22});
            this.layoutControlGroup10.Location = new System.Drawing.Point(908, 0);
            this.layoutControlGroup10.Name = "layoutControlGroup10";
            this.layoutControlGroup10.OptionsItemText.TextToControlDistance = 5;
            this.layoutControlGroup10.Size = new System.Drawing.Size(319, 67);
            this.layoutControlGroup10.Text = ":: Jurídico ::";
            // 
            // layoutControlItem22
            // 
            this.layoutControlItem22.Control = this.lookUpEdit1;
            this.layoutControlItem22.CustomizationFormText = "layoutControlItem22";
            this.layoutControlItem22.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItem22.Name = "layoutControlItem22";
            this.layoutControlItem22.Size = new System.Drawing.Size(295, 24);
            this.layoutControlItem22.Text = "layoutControlItem22";
            this.layoutControlItem22.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem22.TextToControlDistance = 0;
            this.layoutControlItem22.TextVisible = false;
            // 
            // layoutControlGroup9
            // 
            this.layoutControlGroup9.CustomizationFormText = ":: Dia para Vencimento do Condomínio ::";
            this.layoutControlGroup9.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItem12,
            this.layoutControlItem11,
            this.layoutControlItem19});
            this.layoutControlGroup9.Location = new System.Drawing.Point(555, 0);
            this.layoutControlGroup9.Name = "layoutControlGroup9";
            this.layoutControlGroup9.OptionsItemText.TextToControlDistance = 5;
            this.layoutControlGroup9.Size = new System.Drawing.Size(353, 67);
            this.layoutControlGroup9.Text = ":: Dia para Vencimento do Condomínio ::";
            // 
            // layoutControlItem12
            // 
            this.layoutControlItem12.AllowHotTrack = false;
            this.layoutControlItem12.Control = this.checkEdit4;
            this.layoutControlItem12.CustomizationFormText = "layoutControlItem12";
            this.layoutControlItem12.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItem12.Name = "layoutControlItem12";
            this.layoutControlItem12.Size = new System.Drawing.Size(121, 24);
            this.layoutControlItem12.Text = "layoutControlItem12";
            this.layoutControlItem12.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem12.TextToControlDistance = 0;
            this.layoutControlItem12.TextVisible = false;
            // 
            // layoutControlItem11
            // 
            this.layoutControlItem11.AllowHotTrack = false;
            this.layoutControlItem11.Control = this.spinEdit1;
            this.layoutControlItem11.CustomizationFormText = "layoutControlItem11";
            this.layoutControlItem11.Location = new System.Drawing.Point(121, 0);
            this.layoutControlItem11.Name = "layoutControlItem11";
            this.layoutControlItem11.Size = new System.Drawing.Size(84, 24);
            this.layoutControlItem11.Text = "layoutControlItem11";
            this.layoutControlItem11.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem11.TextToControlDistance = 0;
            this.layoutControlItem11.TextVisible = false;
            // 
            // layoutControlItem19
            // 
            this.layoutControlItem19.Control = this.spinEdit2;
            this.layoutControlItem19.CustomizationFormText = "Ref.";
            this.layoutControlItem19.Location = new System.Drawing.Point(205, 0);
            this.layoutControlItem19.Name = "layoutControlItem19";
            this.layoutControlItem19.Size = new System.Drawing.Size(124, 24);
            this.layoutControlItem19.Text = "Ref.";
            this.layoutControlItem19.TextSize = new System.Drawing.Size(21, 13);
            this.layoutControlItem19.TextToControlDistance = 5;
            // 
            // layoutControlGroup7
            // 
            this.layoutControlGroup7.CustomizationFormText = "Registro";
            this.layoutControlGroup7.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItem8});
            this.layoutControlGroup7.Location = new System.Drawing.Point(298, 0);
            this.layoutControlGroup7.Name = "layoutControlGroup7";
            this.layoutControlGroup7.OptionsItemText.TextToControlDistance = 5;
            this.layoutControlGroup7.Size = new System.Drawing.Size(257, 67);
            this.layoutControlGroup7.Text = ":: Registro ::";
            // 
            // layoutControlItem8
            // 
            this.layoutControlItem8.Control = this.textEdit1;
            this.layoutControlItem8.CustomizationFormText = "Registro";
            this.layoutControlItem8.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItem8.Name = "layoutControlItem8";
            this.layoutControlItem8.Size = new System.Drawing.Size(233, 24);
            this.layoutControlItem8.Text = "layoutControlItem8";
            this.layoutControlItem8.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem8.TextToControlDistance = 0;
            this.layoutControlItem8.TextVisible = false;
            // 
            // fORMAPAGTOTableAdapter
            // 
            this.fORMAPAGTOTableAdapter.ClearBeforeFill = true;
            this.fORMAPAGTOTableAdapter.GetNovosDados = ((System.Collections.ArrayList)(resources.GetObject("fORMAPAGTOTableAdapter.GetNovosDados")));
            this.fORMAPAGTOTableAdapter.TabelaDataTable = null;
            // 
            // aPARTAMENTOSTableAdapter
            // 
            this.aPARTAMENTOSTableAdapter.ClearBeforeFill = true;
            this.aPARTAMENTOSTableAdapter.GetNovosDados = ((System.Collections.ArrayList)(resources.GetObject("aPARTAMENTOSTableAdapter.GetNovosDados")));
            this.aPARTAMENTOSTableAdapter.TabelaDataTable = null;
            // 
            // aPTPGETableAdapter
            // 
            this.aPTPGETableAdapter.ClearBeforeFill = true;
            this.aPTPGETableAdapter.GetNovosDados = ((System.Collections.ArrayList)(resources.GetObject("aPTPGETableAdapter.GetNovosDados")));
            this.aPTPGETableAdapter.TabelaDataTable = null;
            // 
            // cApartamentos
            // 
            this.Autofill = false;
            this.BindingSourcePrincipal = this.BindingSource_F;
            this.Controls.Add(this.LayoutControl);
            this.Cursor = System.Windows.Forms.Cursors.Default;
            this.Doc = System.Windows.Forms.DockStyle.Fill;
            this.LookAndFeel.UseDefaultLookAndFeel = false;
            this.Name = "cApartamentos";
            this.Size = new System.Drawing.Size(1247, 743);
            this.cargaInicial += new System.EventHandler(this.cApartamentos_cargaInicial);
            this.cargaFinal += new System.EventHandler(this.cApartamentos_cargaFinal);
            this.Load += new System.EventHandler(this.cApartamentos_Load);
            this.Controls.SetChildIndex(this.LayoutControl, 0);
            ((System.ComponentModel.ISupportInitialize)(this.BarManager_F)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.BarAndDockingController_F)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.BindingSource_F)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.StyleController_F)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LayoutControl)).EndInit();
            this.LayoutControl.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.textEdit1.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.imageComboBoxEdit1.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lookUpEdit1.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.bindingSourceFRN)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit7.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit6.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.spinEdit2.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.aPTPGEBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dApartamentosCampos)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckEdit1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemImageComboBox1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.imagensPI1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.aPTSenhaInternetInquilinoTextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.aPTUsuarioInternetInquilinoTextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.aPTSenhaInternetProprietarioTextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.aPTUsuarioInternetProprietarioTextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lkpImobiliaria.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.iMOBILIARIASBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dCondominiosCampos)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit1.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lkpInquilino.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pESSOASBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit4.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lkpPagtoInquilino.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.fORMAPAGTOBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lkpProprietario.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lkpPagtoProprietario.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtIdentificacao.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit5.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.clcFracaoIdeal.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.spinEdit1.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LayoutControlGroup)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem23)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem17)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup8)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem10)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem15)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem16)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem20)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem13)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem14)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem21)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem18)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup10)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem22)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup9)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem12)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem11)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem19)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem8)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraLayout.LayoutControl LayoutControl;
        private DevExpress.XtraLayout.LayoutControlGroup LayoutControlGroup;
        private DevExpress.XtraEditors.TextEdit txtIdentificacao;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem1;
        private DevExpress.XtraEditors.CalcEdit clcFracaoIdeal;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup1;
        private DevExpress.XtraEditors.LookUpEdit lkpProprietario;
        private DevExpress.XtraEditors.LookUpEdit lkpPagtoProprietario;
        private DevExpress.XtraEditors.LookUpEdit lkpPagtoInquilino;
        private DevExpress.XtraEditors.CheckEdit checkEdit1;
        private DevExpress.XtraEditors.LookUpEdit lkpInquilino;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup4;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup5;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem6;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup6;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem7;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup3;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem3;
        private DevExpress.XtraEditors.LookUpEdit lkpImobiliaria;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup8;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem10;
        private dCondominiosCampos dCondominiosCampos;
        
        private Cadastros.Condominios.dCondominiosCamposTableAdapters.FORMAPAGTOTableAdapter fORMAPAGTOTableAdapter;
        private DevExpress.XtraEditors.SpinEdit spinEdit1;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup9;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem11;
        private DevExpress.XtraEditors.CheckEdit checkEdit4;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem12;
        public System.Windows.Forms.BindingSource fORMAPAGTOBindingSource;
        public System.Windows.Forms.BindingSource iMOBILIARIASBindingSource;
        private dApartamentosCampos dApartamentosCampos;
        private Cadastros.Condominios.dApartamentosCamposTableAdapters.APARTAMENTOSTableAdapter aPARTAMENTOSTableAdapter;
        
        private DevExpress.XtraEditors.TextEdit aPTSenhaInternetInquilinoTextEdit;
        private DevExpress.XtraEditors.TextEdit aPTUsuarioInternetInquilinoTextEdit;
        private DevExpress.XtraEditors.TextEdit aPTSenhaInternetProprietarioTextEdit;
        private DevExpress.XtraEditors.TextEdit aPTUsuarioInternetProprietarioTextEdit;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem15;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem16;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem13;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem14;
        private System.Windows.Forms.BindingSource pESSOASBindingSource;
        private DevExpress.XtraEditors.CheckEdit checkEdit5;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem17;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem4;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem5;
        private DevExpress.XtraGrid.GridControl gridControl1;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView1;
        private System.Windows.Forms.BindingSource aPTPGEBindingSource;
        private DevExpress.XtraGrid.Columns.GridColumn colAPTPGEPaga;
        private DevExpress.XtraGrid.Columns.GridColumn colPGE_PLA;
        private DevExpress.XtraGrid.Columns.GridColumn colPGEMensagem;
        private DevExpress.XtraGrid.Columns.GridColumn colPGEValor;
        private DevExpress.XtraGrid.Columns.GridColumn colAPTPGEDATAI;
        private DevExpress.XtraGrid.Columns.GridColumn colAPTPGEDATAA;
        private DevExpress.XtraGrid.Columns.GridColumn colAPTPGEA_USU;
        private DevExpress.XtraGrid.Columns.GridColumn colAPTPGEI_USU;
        private DevExpress.XtraGrid.Columns.GridColumn colAPTGEIProprietario;
        private Cadastros.Condominios.dApartamentosCamposTableAdapters.APTPGETableAdapter aPTPGETableAdapter;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem18;
        private Framework.objetosNeon.ImagensPI imagensPI1;
        private DevExpress.XtraEditors.Repository.RepositoryItemImageComboBox repositoryItemImageComboBox1;
        private DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit repositoryItemCheckEdit1;
        private DevExpress.XtraEditors.SpinEdit spinEdit2;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem19;
        private DevExpress.XtraEditors.CheckEdit checkEdit7;
        private DevExpress.XtraEditors.CheckEdit checkEdit6;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem20;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem21;
        private DevExpress.XtraEditors.LookUpEdit lookUpEdit1;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem2;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup10;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem22;
        private System.Windows.Forms.BindingSource bindingSourceFRN;
        private DevExpress.XtraEditors.ImageComboBoxEdit imageComboBoxEdit1;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem23;
        private DevExpress.XtraEditors.TextEdit textEdit1;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup7;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem8;
    }
}
