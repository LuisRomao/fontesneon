/*
LH - 16/04/2014       -            troca o campo CONCodigoFolha2 por CONCodigoFolha2A remo��o de CONCodigoBancoCS
LH - 22/05/2014       -            reativada a exporta��o do s�ndico
LH - 27/05/2014       - 13.2.9.4 - Nova estrura para rentabilidade e transferir arrecadado
MR - 20/02/2015 20:00 -          - Tratamento de consist�ncia dos campos c�d. comunica��o e tipo de envio (de arquivos) que devem ser configurados em conjunto, para pagamento eletr�nico (Altera��es indicadas por *** MRC - INICIO - PAG-FOR (20/02/2015 20:00) ***)
MR - 25/02/2016 11:00 -          - Tratamento de consist�ncia dos campos c�d. CNR e boleto com registro para o Banco Bradesco, para cobranca com registro (Altera��es indicadas por *** MRC - INICIO - PAG-FOR (25/02/2016 11:00) ***)
                                 - Inclus�o do valor default para campo CONBoletoComRegistro
MR - 15/03/2016 10:00 -          - Inclus�o do valor default para campo CONInaTamanhoFonte (Altera��es indicadas por *** MRC - INICIO (15/03/2016 10:00) ***)
*/

using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Windows.Forms;
using Framework;
using CadastrosProc.Condominios;
using VirEnumeracoesNeon;

namespace Cadastros.Condominios
{
    /// <summary>
    /// Condom�nio Campos
    /// </summary>
    public partial class cCondominiosCampos2 : CompontesBasicos.ComponenteCamposGroupTabControl
    {
        internal void IncluiPESloo(int PES, string PESNome)
        {
            dCondominiosCampos.PESloockRow NovoPES;
            NovoPES = dCondominiosCampos.PESloock.FindByPES(PES);
            if (NovoPES == null)
            {
                NovoPES = dCondominiosCampos.PESloock.NewPESloockRow();
                NovoPES.PES = PES;
                NovoPES.PESNome = PESNome;
                dCondominiosCampos.PESloock.AddPESloockRow(NovoPES);
            }
            else
                NovoPES.PESNome = PESNome;
        }

        internal void RecarregaFRN()
        {
            if (_dFornecedoresLookup != null)
                _dFornecedoresLookup.FRNLookupTableAdapter.Fill(_dFornecedoresLookup.FRNLookup);
        }

        private CadastrosProc.Fornecedores.dFornecedoresLookup _dFornecedoresLookup;

        internal CadastrosProc.Fornecedores.dFornecedoresLookup DFornecedoresLookup
        {
            get
            {
                if (_dFornecedoresLookup == null)
                {
                    _dFornecedoresLookup = new CadastrosProc.Fornecedores.dFornecedoresLookup();
                    _dFornecedoresLookup.FRNLookupTableAdapter.Fill(_dFornecedoresLookup.FRNLookup);
                }
                return _dFornecedoresLookup;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="Dia"></param>
        /// <param name="TipoDia"></param>
        /// <param name="OrdemContagem"></param>
        /// <returns></returns>
        public static string RegraVencimento(Int32 Dia, String TipoDia, String OrdemContagem)
        {
            string _Vencto = null;

            _Vencto = Dia + "� DIA";

            _Vencto += (TipoDia == "C" ? " CORRIDO " : " �TIL ");

            _Vencto += (OrdemContagem == "I" ? " A PARTIR DO �NICIO DO M�S " : " A PARTIR DO FINAL DO M�S ");

            return _Vencto;
        }

        /// <summary>
        /// Construtor
        /// </summary>
        public cCondominiosCampos2()
        {
            InitializeComponent();
            TableAdapterPrincipal = dCondominiosCampos.CONDOMINIOSTableAdapter;
            tableAdapterSelect = TableAdapterPrincipal;
            metodoupdate = TableAdapterPrincipal.GetType().GetMethod("Update", new Type[1] { tabela.GetType() });
            metodoFill_F = TableAdapterPrincipal.GetType().GetMethod("Fill", new Type[1] { tabela.GetType() });
            metodoFillBy = TableAdapterPrincipal.GetType().GetMethod("Fill", new Type[2] { tabela.GetType(), typeof(int) });            
        }
                
        private void cCondominiosCampos2_cargaInicial(object sender, EventArgs e)
        {
            Enumeracoes.virEnumCONStatus.virEnumCONStatusSt.CarregaEditorDaGrid(imageComboBoxEdit1);
            

            dCondominiosCampos.TiposInadimplencia.Clear();
            dCondominiosCampos.TiposInadimplencia.AddTiposInadimplenciaRow(0, "N�o Mostrar", "N�o mostra quadro de inadimplentes");
            dCondominiosCampos.TiposInadimplencia.AddTiposInadimplenciaRow(1, "Lista", "Somente a lista dos inadimplentes");
            dCondominiosCampos.TiposInadimplencia.AddTiposInadimplenciaRow(2, "Valor", "Lista de inadimplentes com valor");
            dCondominiosCampos.TiposInadimplencia.AcceptChanges();
          
            
            
            //if (Estatica.EdEstatico.PESSOAS.Rows.Count == 0)
            //    Estatica.EAdapPessoas.Fill(Estatica.EdEstatico.PESSOAS);
                        
            cARGOSTableAdapter.Fill(dCondominiosCampos.CARGOS);
            formapagtoTableAdapter.Fill(dCondominiosCampos.FORMAPAGTO);
            eMPRESASBindingSource.DataSource = Framework.datasets.dEmpresas.dEmpresasSt;
            cIDADESBindingSource.DataSource = Framework.datasets.dCIDADES.dCIDADESSt;                                                                      

        }

        /// <summary>
        /// 
        /// </summary>
        public void CarregaAPTs() 
        {
            if (pk != 0)
                aPARTAMENTOSTableAdapter.FillCON(dCondominiosCampos.APARTAMENTOS, pk);
            else
                dCondominiosCampos.APARTAMENTOS.Clear();
        }

        private void cCondominiosCampos2_cargaFinal(object sender, EventArgs e)
        {
            if (pk != 0)
            {
                bLOCOSTableAdapter.Fill(dCondominiosCampos.BLOCOS, pk);
                apartamentosBlocoTableAdapter.Fill(dCondominiosCampos.ApartamentosBloco, pk);
                CarregaAPTs();
                //cORPODIRETIVO1TableAdapter.Fill(dCondominiosCampos.CORPODIRETIVO1, pk);
                buscaSindicoTableAdapter.Fill(dCondominiosCampos.BuscaSindico, pk);
                //hISTORICOSTableAdapter.Fill(dCondominiosCampos.HISTORICOS, pk);
                if (((CONStatus)linhaMae.CONStatus != CONStatus.Ativado) && (linhaMae.CON_EMP == DSCentral.EMP))
                    BtAtivar.Visible = simpleButton1.Visible = true;
                dCondominiosCampos.EQuiPamentosTableAdapter.FillBy(dCondominiosCampos.EQuiPamentos, pk);
                dCondominiosCampos.GradeEQuipamentoTableAdapter.FillByCON(dCondominiosCampos.GradeEQuipamento, pk);

            }
            else
            {
                linhaMae.CONStatus = (int)CONStatus.Implantacao;
                linhaMae.CON_EMP = DSCentral.EMP;
                linhaMae.CONProcuracao = false;
                linhaMae.CONFracaoIdeal = true;
                linhaMae.CONTipoMulta = "M";
                linhaMae.CONValorMulta = 2;
                linhaMae.CONValorJuros = 1;
                linhaMae.CONCompetenciaVencida = true;
                linhaMae.CONImprimeBalanceteBoleto = false;
                linhaMae.CONExportaInternet = false;
                linhaMae.CONPermiteAcordoInternet = true;
                linhaMae.CONPublicaBalanceteInternet = true;
                linhaMae.CONRepassaDespBanc = false;
                linhaMae.CONRepassaCorreio = false;
                linhaMae.CONCobrarSegVia = true;
                linhaMae.CONCustoSegVia = 2;
                linhaMae.CONOculto = false;
                linhaMae.CONJurosCompostos = true;
                linhaMae.CONInibirPendecias = false;
                linhaMae.CONPagEletronico = false;
                linhaMae.CONGPSEletronico = false;
                linhaMae.CONPlanilhaPonto = true;
                linhaMae.CONProtesto = 0;
                linhaMae.CONStatusAcordo = 0;
                linhaMae.CONRelAcordoDet = false;
                linhaMae.CONMenBoleto =
"-Banco n�o est� autorizado a receber ap�s 30 dias da data de vencimento.\r\n" +
"-N�o conceder abatimento.\r\n" +
"-Quita��o v�lida apenas ap�s compensa��o do cheque.\r\n" +
"-Ap�s o vencimento cobrar multa de @M%.\r\n" +
"-Boleto sujeito a protesto conforme lei n� 13.160 de 21 de julho de 2008.";
                linhaMae.CONEmiteBoletos = true;
                linhaMae.CONSefip = true;
                linhaMae.CONNotaAutomaica = true;
                linhaMae.CONReserva = false;
                linhaMae.CONOcultaPxR = false;
                linhaMae.CONOcultaSenha = false;
                linhaMae.CONNomeBloco = "Bloco";
                linhaMae.CONNomeApto = "Apto";
                linhaMae.CONJurosPrimeiroMes = false;
                linhaMae.CONTipoContaPE = 0;
                linhaMae.CONDivideCheque = false;
                linhaMae.CONSequencialRemessaTributos = 0;
                linhaMae.CONBoletoComRegistro = false;
                linhaMae.CONInaTamanhoFonte = (int)InaTamanhoFonte.Pequena;
                linhaMae.CONBoletoOcAgua = false;
                linhaMae.CONBoletoOcGas = false;
                linhaMae.CONBoletoOcLuz = false;
                linhaMae.CONDiasExpiraRegistro = 30;
                dCondominiosCampos.BLOCOS.Clear();
                dCondominiosCampos.APARTAMENTOS.Clear();
                dCondominiosCampos.CORPODIRETIVO1.Clear();
                BtAtivar.Visible = true;
            };
            GroupControl_F.BackColor = Enumeracoes.virEnumCONStatus.virEnumCONStatusSt.GetCor((int)linhaMae.CONStatus);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="Resultado"></param>
        protected override void FechaTela(DialogResult Resultado)
        {
            if(AlteracoesGravadas)
                GradeChamadora.RefreshDadosManual();
            base.FechaTela(Resultado);
        }

        private bool AlteracoesGravadas = false;

        /// <summary>
        /// 
        /// </summary>
        public bool CorpoDiretivoAlterado = false;

        /// <summary>
        /// 
        /// </summary>
        public string CorpoDiretivoJustificativas = "";

        private void RegistraCorpoDiretivo()
        {
            if (CorpoDiretivoAlterado)
            {
                string descritivo = "Novo corpo Diretivo:\r\n";
                dHistoricos.HISTORICOSRow NovoRegistro = dHistoricos.dHistoricosSt.HISTORICOS.NewHISTORICOSRow();
                NovoRegistro.HST_ASS = DSCentral.EMP == 1 ? 34 : 34;
                NovoRegistro.HST_CON = linhaMae.CON;
                NovoRegistro.HSTI_USU = NovoRegistro.HSTA_USU = Framework.usuarioLogado.UsuarioLogadoNeon.UsuarioLogadoSt.USU;
                NovoRegistro.HSTDATAI = NovoRegistro.HSTADATA = DateTime.Now;
                NovoRegistro.HSTCritico = false;
                NovoRegistro.HSTAceito = true;
                NovoRegistro.HSTConcluido = true;
                //foreach (dCondominiosCampos.CORPODIRETIVO1Row rowCDR in DTAlt)
                //    descritivo += string.Format("{0}: {1} ({2})\r\n", dCondominiosCampos.CARGOS.FindByCGO(rowCDR.CDR_CGO).CGONome,Estatica.EdEstatico.PESSOAS.FindByPES(rowCDR.CDR_PES).PESNome,rowCDR.CDR_APT);
                //descritivo += "-----\r\n";

                //dCondominiosCampos.CORPODIRETIVOTableAdapter.Fill(dCondominiosCampos.CORPODIRETIVO, row.CON);
                foreach (dCondominiosCampos.CORPODIRETIVO1Row rowC in dCondominiosCampos.CORPODIRETIVO1)
                {
                    string Apartamento = "";
                    if (!rowC.IsCDR_APTNull())
                    {
                        dCondominiosCampos.ApartamentosBlocoRow APTBLOrow = dCondominiosCampos.ApartamentosBloco.FindByAPT(rowC.CDR_APT);
                        if (APTBLOrow != null)
                            Apartamento = APTBLOrow._Bloco_AP;
                    }
                    dCondominiosCampos.PESloockRow rowPes = dCondominiosCampos.PESloock.FindByPES(rowC.CDR_PES);
                    //Cadastros.dEstatico.PESSOASRow rowPes = Estatica.EdEstatico.PESSOAS.FindByPES(rowC.CDR_PES);
                    dCondominiosCampos.CARGOSRow rowCGO = dCondominiosCampos.CARGOS.FindByCGO(rowC.CDR_CGO);
                    descritivo += string.Format("{0}: {1} ({2})\r\n", rowCGO.CGONome.PadRight(15), rowPes.PESNome, Apartamento);
                };
                if (CorpoDiretivoJustificativas != "")
                    descritivo += string.Format("\r\n\r\nJustificativas para n�o recolhimento de INSS:\r\n{0}", CorpoDiretivoJustificativas);
                NovoRegistro.HSTDescricao = descritivo;

                dHistoricos.dHistoricosSt.HISTORICOS.AddHISTORICOSRow(NovoRegistro);
                dHistoricos.dHistoricosSt.HISTORICOSTableAdapter.Update(NovoRegistro);
                NovoRegistro.AcceptChanges();
                CorpoDiretivoAlterado = false;
                CorpoDiretivoJustificativas = "";
            };

        }

        private SortedList TabelaDeNomes;

        private string DePara(object De)
        {
            if (TabelaDeNomes == null)
            {
                TabelaDeNomes = new SortedList();
                TabelaDeNomes.Add("CONCobranca_USU", "Cobran�a");
                TabelaDeNomes.Add("CONCodigo", "CODCON");
                TabelaDeNomes.Add("CONNome", "Nome");
                TabelaDeNomes.Add("CONEndereco", "Endere�o");
                TabelaDeNomes.Add("CONBairro", "Bairro");
                TabelaDeNomes.Add("CON_CID", "Cidade");
                //TabelaDeNomes.Add("CONUf","UF");
                TabelaDeNomes.Add("CONCep", "CEP");
                TabelaDeNomes.Add("CONObservacao", "Obs");
                TabelaDeNomes.Add("CONCnpj", "CNPJ");
                TabelaDeNomes.Add("CONProcuracao", "Procura��o");
                TabelaDeNomes.Add("CONFracaoIdeal", "Fra��o");
                TabelaDeNomes.Add("CONTotalBloco", "Blocos");
                TabelaDeNomes.Add("CONTotalApartamento", "Apartamentos");
                TabelaDeNomes.Add("CON_BCO", "Banco");
                TabelaDeNomes.Add("CONAgencia", "Ag�ncia");
                TabelaDeNomes.Add("CONDigitoAgencia", "dg Ag�ncia");
                TabelaDeNomes.Add("CONConta", "Conta");
                TabelaDeNomes.Add("CONDigitoConta", "dg Conta");
                //TabelaDeNomes.Add("CONTipoFR","");
                TabelaDeNomes.Add("CONValorFR", "Fundo de Reserva");
                TabelaDeNomes.Add("CONTipoMulta", "Tipo de multa");
                TabelaDeNomes.Add("CONValorMulta", "Multa");
                TabelaDeNomes.Add("CONValorJuros", "Juros");
                TabelaDeNomes.Add("CONValorCondominio", "Condom�nio (l�quido)");
                TabelaDeNomes.Add("CONValorGas", "Valor do gas");
                TabelaDeNomes.Add("CONDiaContabilidade", "Inicio balancete");
                TabelaDeNomes.Add("CONDiaVencimento", "Vencimento");
                //    TabelaDeNomes.Add("CONTipoVencimento","");
                //TabelaDeNomes.Add("CONReferenciaVencimento","");
                //    TabelaDeNomes.Add("CONCartaCobranca","");
                TabelaDeNomes.Add("CONRepassaCorreio", "Repassa Correio");
                TabelaDeNomes.Add("CONValorAR", "Valor AR");
                TabelaDeNomes.Add("CONValorCartaSimples", "Valor Correio");
                //    TabelaDeNomes.Add("CONCorreioAssembleia","");
                //TabelaDeNomes.Add("CONCorreioCobranca","");
                //  TabelaDeNomes.Add("CONCorreioBoleto","");
                TabelaDeNomes.Add("CONAdvogado1_USU", "Advogado 1");
                TabelaDeNomes.Add("CONAdvogado2_USU", "Advogado 2");
                TabelaDeNomes.Add("CONSeguradora_FRN", "Seguradora");
                TabelaDeNomes.Add("CONCorretoraSeguros_FRN", "Corretora");
                TabelaDeNomes.Add("CONCompetenciaVencida", "Compet�ncia a vencer");
                TabelaDeNomes.Add("CONAuditor1_USU", "Gerente");
                TabelaDeNomes.Add("CONAuditor2_USU", "Gerente 2");
                TabelaDeNomes.Add("CONExportaInternet", "Internet");
                TabelaDeNomes.Add("CONPermiteAcordoInternet", "Permite acordo pela internet");
                TabelaDeNomes.Add("CONPublicaBalanceteInternet", "Publica balancete na internet");
                TabelaDeNomes.Add("CONObservacaoInternet", "Obs Internet");
                TabelaDeNomes.Add("CONInclusao_USU", "inclus�o");
                TabelaDeNomes.Add("CONDataInclusao", "inclus�o");
                TabelaDeNomes.Add("CONAtualizacao_USU", "Altera��o");
                TabelaDeNomes.Add("CONDataAtualizacao", "Altera��o");
                TabelaDeNomes.Add("CONLeituraGas", "Leitura de gas");
                TabelaDeNomes.Add("CONCodigoFolha1", "codigo folha 1");
                TabelaDeNomes.Add("CONCodigoFolha2A", "codigo folha 2");
                TabelaDeNomes.Add("CONCodigoBancoCC", "DP CC");
                
                //TabelaDeNomes.Add("CON_EMP","");
                TabelaDeNomes.Add("CONStatus", "Status");
                TabelaDeNomes.Add("CONMenBoleto", "Boleto");
                TabelaDeNomes.Add("CONDataMandato", "Mandato");
                TabelaDeNomes.Add("CONRepassaDespBanc", "Repassa despesa banc�ria");
                TabelaDeNomes.Add("CONValorBoleto", "Valor por boletos");
                TabelaDeNomes.Add("CONCodigoCNR", "CNR");
                //TabelaDeNomes.Add("CONInaDataRef","");
                //   TabelaDeNomes.Add("CONInaPeriodo","");
                //     TabelaDeNomes.Add("CONInaAgrupamento","");
                //  TabelaDeNomes.Add("CONInaValorOrig","");
                //    TabelaDeNomes.Add("CONInaValorCor","");
                // TabelaDeNomes.Add("CONInaNumero","");
                //   TabelaDeNomes.Add("CONInaDestacaMes","");
                TabelaDeNomes.Add("CONCobrarSegVia", "Cobrar segunda via");
                TabelaDeNomes.Add("CONCustoSegVia", "Custo segunda via");
                TabelaDeNomes.Add("CONJurosCompostos", "Juros compostos");
                TabelaDeNomes.Add("CONEscrit_FRN", "Jur�dico");
                TabelaDeNomes.Add("CONStatusAcordo", "Acordo");
                TabelaDeNomes.Add("CONAvisoAcordo", "Aviso no acordo");
                TabelaDeNomes.Add("CONInibirPendecias", "N�o mostrar pendencias da unidade");
                TabelaDeNomes.Add("CONMalote", "Malote");
                //TabelaDeNomes.Add("CONCobranca_USU", "Cobran�a");
                TabelaDeNomes.Add("CONHistorico", "Hist�rico");
                TabelaDeNomes.Add("CONPagEletronico", "Pagamento eletr�nico");
                TabelaDeNomes.Add("CONPlanilhaPonto", "Planilha de ponto");
                TabelaDeNomes.Add("CONProtesto", "Protesto");
                TabelaDeNomes.Add("CONUsuPref", "Usu�rio no site da prefeitura");
                TabelaDeNomes.Add("CONSenhaPref", "Senha no site da prefeitura");
                TabelaDeNomes.Add("CONBalancete_USU", "Resp. pelo balancete");
                TabelaDeNomes.Add("CONObsBal", "Obs balancete");
                TabelaDeNomes.Add("CONLimiteBal", "Limite para balancete");
                TabelaDeNomes.Add("CONRelAcordoDet", "relat�rio de acordos detalhado");
                TabelaDeNomes.Add("CONGPSEletronico", "GPS eletr�nico");
            }
            if (TabelaDeNomes.ContainsKey(De))
                return TabelaDeNomes[De].ToString();
            else
                return De.ToString();
        }

        /// <summary>
        /// Grava as altera��es
        /// </summary>
        /// <param name="Recarregar"></param>
        /// <returns></returns>
        public bool Grava(bool Recarregar)
        {
            if (CorpoDiretivoAlterado)
                RegistraCorpoDiretivo();

            if (!Validate(true))
                return false;

            cONDOMINIOSBindingSource.EndEdit();


            if (!CamposObrigatoriosOk(cONDOMINIOSBindingSource))
                return false;
            if (!dCondominiosCampos.HasChanges())
                return true;



            //if ((DialogResult.Yes == MessageBox.Show("Confirma a grava��o?", "Confirmar", MessageBoxButtons.YesNo, MessageBoxIcon.Question)))
            //{
            //DataRowView DRV = (DataRowView)cONDOMINIOSBindingSource.Current;
            dCondominiosCampos.CONDOMINIOSRow row = linhaMae;
            // ExportarAccess = false;

            //*** MRC - INICIO - PAG-FOR (20/02/2015 20:00) ***
            //Trata consistencia de dados antes de salvar
            string CodComunicacao = linhaMae.IsCONCodigoComunicacaoNull() ? "" : linhaMae.CONCodigoComunicacao;
            if ((CodComunicacao != "" && linhaMae.IsCONRemessa_TEANull()) || (CodComunicacao == "" && !linhaMae.IsCONRemessa_TEANull()))
            {
                MessageBox.Show("O campo 'C�d. Comunica��o / Tipo Envio' deve ser configurado ou removido em conjunto.");
                return false;
            }
            //*** MRC - INICIO - PAG-FOR (20/02/2015 20:00) ***

            string CodigoCNR = linhaMae.IsCONCodigoCNRNull() ? "" : linhaMae.CONCodigoCNR;
            if (!linhaMae.IsCON_BCONull())
                if (linhaMae.CON_BCO == 237 && linhaMae.CONBoletoComRegistro && CodigoCNR == "")
                {
                    MessageBox.Show("Campo 'C�d. CNR' � obrigat�rio quando selecionada a op��o 'Boleto com Registro' para o Banco Bradesco.");
                    return false;
                }


            AjustaInclusaiAlteracao();

            VerificaSeguro();

            

            AlteracoesGravadas = true;


            SortedList CamposAlterados = VirDB.VirtualTableAdapter.CamposEfetivamenteAlterados(row);
            if (CamposAlterados.Count > 0)
            {
                string Just = String.Format("\r\n\r\n{0} - Cadastro alterado por {1}\r\nAltera��es:", DateTime.Now, CompontesBasicos.Bancovirtual.UsuarioLogado.UsuarioLogadoSt.USUNome);
                if (row.HasVersion(DataRowVersion.Original))
                {
                    foreach (DictionaryEntry Alterado in CamposAlterados)
                        if (Alterado.Key.ToString().Contains("_USU"))
                        {
                            FrameworkProc.datasets.dUSUarios.USUariosRow rowUSU;
                            string strOrig = "";
                            string strNovo = "";
                            if (row[Alterado.Key.ToString(), DataRowVersion.Original] != DBNull.Value)
                            {
                                rowUSU = FrameworkProc.datasets.dUSUarios.dUSUariosSt.USUarios.FindByUSU((int)row[Alterado.Key.ToString(), DataRowVersion.Original]);
                                if (rowUSU != null)
                                    strOrig = rowUSU.USUNome;
                            }
                            if (row[Alterado.Key.ToString(), DataRowVersion.Current] != DBNull.Value)
                            {
                                rowUSU = FrameworkProc.datasets.dUSUarios.dUSUariosSt.USUarios.FindByUSU((int)row[Alterado.Key.ToString(), DataRowVersion.Current]);
                                if (rowUSU != null)
                                    strNovo = rowUSU.USUNome;
                            };
                            Just += String.Format("\r\n{0}: {1} - > {2}", DePara(Alterado.Key), strOrig, strNovo);
                        }
                        else

                            Just += String.Format("\r\n{0}: {1} - > {2}", DePara(Alterado.Key), row[Alterado.Key.ToString(), DataRowVersion.Original], row[Alterado.Key.ToString(), DataRowVersion.Current]);

                    if (row.IsCONHistoricoNull())
                        row.CONHistorico = Just;
                    else

                        row.CONHistorico += Just;

                };

                dCondominiosCampos.CONDOMINIOSTableAdapter.Update(dCondominiosCampos.CONDOMINIOS);
                dCondominiosCampos.CONDOMINIOS.AcceptChanges();
            }
            bLOCOSTableAdapter.Update(dCondominiosCampos.BLOCOS);
            dCondominiosCampos.BLOCOS.AcceptChanges();
            aPARTAMENTOSTableAdapter.Update(dCondominiosCampos.APARTAMENTOS);
            dCondominiosCampos.APARTAMENTOS.AcceptChanges();
            paGamentoEspecificoTableAdapter.Update(dCondominiosCampos.PaGamentoEspecifico);
            dCondominiosCampos.PaGamentoEspecifico.AcceptChanges();

            VerificaReserva();

            string BuscaTotais =
"SELECT     COUNT(distinct BLOCOS.BLO) AS TOTB, COUNT(distinct APARTAMENTOS.APT) AS TOTA\r\n" +
"FROM         CONDOMINIOS INNER JOIN\r\n" +
"                      BLOCOS ON CONDOMINIOS.CON = BLOCOS.BLO_CON LEFT OUTER JOIN\r\n" +
"                      APARTAMENTOS ON BLOCOS.BLO = APARTAMENTOS.APT_BLO\r\n" +
"where (CONDOMINIOS.CON = @P1);";
            DataRow rowTotais = VirMSSQL.TableAdapter.STTableAdapter.BuscaSQLRow(BuscaTotais, row.CON);
            if (rowTotais != null)
            {
                if (row.IsCONTotalBlocoNull())
                    row.CONTotalBloco = 0;
                if (row.IsCONTotalApartamentoNull())
                    row.CONTotalApartamento = 0;
                if ((row.CONTotalBloco != (int)rowTotais["TOTB"]) || (row.CONTotalApartamento != (int)rowTotais["TOTA"]))
                {
                    row.CONTotalBloco = (short)((int)rowTotais["TOTB"]);
                    row.CONTotalApartamento = (short)((int)rowTotais["TOTA"]);
                };
                dCondominiosCampos.CONDOMINIOSTableAdapter.Update(row);
                row.AcceptChanges();
                Recarregar = true;
            }
            string VerificaTotais =
"SELECT     BLOCOS.BLO, COUNT(APARTAMENTOS.APT) AS TotalReal, BLOCOS.BLOTotalApartamento\r\n" +
"FROM         BLOCOS INNER JOIN\r\n" +
"                      APARTAMENTOS ON BLOCOS.BLO = APARTAMENTOS.APT_BLO\r\n" +
"where (BLOCOS.BLO_CON = @P1)\r\n" +
"GROUP BY BLOCOS.BLO, BLOCOS.BLO_CON, BLOCOS.BLOTotalApartamento\r\n" +
"having COUNT(APARTAMENTOS.APT) <> BLOCOS.BLOTotalApartamento\r\n" +
";";
            string AjustaBlocs = "update blocos set BLOTotalApartamento = @P1 where BLO = @P2";
            DataTable TabErros = VirMSSQL.TableAdapter.STTableAdapter.BuscaSQLTabela(VerificaTotais, row.CON);
            if ((TabErros != null) && (TabErros.Rows.Count > 0))
            {
                foreach (DataRow DR in TabErros.Rows)
                {
                    VirMSSQL.TableAdapter.STTableAdapter.ExecutarSQLNonQuery(AjustaBlocs, DR["TotalReal"], DR["BLO"]);
                    Recarregar = true;
                }
            }

            if (Recarregar)
            {
                pk = row.CON;
                apartamentosBlocoTableAdapter.Fill(dCondominiosCampos.ApartamentosBloco, pk);
                //cORPODIRETIVO1TableAdapter.Fill(dCondominiosCampos.CORPODIRETIVO1, pk);
                buscaSindicoTableAdapter.Fill(dCondominiosCampos.BuscaSindico, pk);
            }
            return true;
            //}
            //else {
            //    return false;
            //}

        }

        private cCondominiosGrade Chamador 
        {
            get { return (cCondominiosGrade)GradeChamadora; }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected override void  btnConfirmar_F_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e){
            try
            {
                if (Grava(false))
                {

                    //GradeChamadora.RefreshDadosManual();
                    FechaTela(DialogResult.OK);
                }
            }
            catch (DBConcurrencyException)
            {
                MessageBox.Show("Registro alterado por outro usu�rio.");
            }
            catch (NoNullAllowedException)
            {
                MessageBox.Show("Existe campo obrig�torio sem um valor informado.", "Campo Obrigat�rio", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }
            catch (ConstraintException Erro)
            {
                if (Erro.Message.ToUpper().Contains("UNIQUE"))
                    MessageBox.Show("Existe campo de valor �nico com valor duplicado.", "Campo de Valor �nico", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                else
                    throw Erro;
            }
                                                
            
        }

        //private bool TestaTab = true;

        

        private void TabControl_F_SelectedPageChanged(object sender, DevExpress.XtraTab.TabPageChangedEventArgs e)
        {
            if (!PaginasCriadas)
                return;
            if (!somenteleitura)
                Grava(true);
            /*
            if ((e.PrevPage != null) && TestaTab && !Grava(true))
            {                
                    TestaTab = false;
                    TabControl_F.SelectedTabPage = e.PrevPage;
                    TestaTab = true;                
            }*/            
        }

        /// <summary>
        /// Linha ativa
        /// </summary>
        public dCondominiosCampos.CONDOMINIOSRow linhaMae 
        {
            get 
            {
                DataRowView DRV = (DataRowView)cONDOMINIOSBindingSource.Current;
                return (dCondominiosCampos.CONDOMINIOSRow)DRV.Row;
            }
        }

        

        private void cONCodigoTextEdit_Validating(object sender, CancelEventArgs e)
        {
            

            cONCodigoTextEdit.ErrorText = "";

            if (cONCodigoTextEdit.Text.Trim() != "")
            {
                string _CODCON = cONCodigoTextEdit.EditValue.ToString();
                if (_CODCON.Length != 6)
                {
                    cONCodigoTextEdit.ErrorText = "O CODCON deve ter 6 caracteres";
                    e.Cancel = true;
                }
                else
                {
                    int pk = (linhaMae["CON"] == null) ? -1 : linhaMae.CON;
                    object connome = dCondominiosCampos.CONDOMINIOSTableAdapter.ValidaCODCON(_CODCON, pk);
                    if (connome != null)
                    {
                        cONCodigoTextEdit.ErrorText = "O CODCON informado j� pertence ao condom�nio " + connome.ToString();
                        e.Cancel = true;
                    }
                }
            }
        }



        private bool ValidoParaAtivacao()
        {
            int Teste;
            if (linhaMae.CON_EMP != DSCentral.EMP)
                return false;
            if (!linhaMae.CONBoletoComRegistro)
                linhaMae.CONBoletoComRegistro = true;
            if (linhaMae.IsCON_BCONull())
            {
                MessageBox.Show("Banco n�o definido");
                return false;
            };
            if (linhaMae.IsCONAgenciaNull() || (linhaMae.CONAgencia.Length != 4))
            {
                MessageBox.Show("Ag�ncia incorreta: deve conter 4 digitos");
                return false;
            }
            if (!int.TryParse(linhaMae.CONAgencia, out Teste))
            {
                MessageBox.Show("Ag�ncia incorreta: deve conter 4 digitos sem letras ou espa�os");
                return false;
            }
            if (linhaMae.CON_BCO == 237)
            {
                if (linhaMae.IsCONDigitoAgenciaNull())
                {
                    MessageBox.Show("D�gito da Ag�ncia incorreto");
                    return false;
                }
                if (!int.TryParse(linhaMae.CONDigitoAgencia, out Teste))
                {
                    MessageBox.Show("D�gito da Ag�ncia incorreta: Deve ser um n�mero");
                    return false;
                }
            }

            int Algarismos = (linhaMae.CON_BCO == 341) ? 5 : 6;

            if (linhaMae.IsCONContaNull() || (linhaMae.CONConta.Length != Algarismos))
            {
                MessageBox.Show(string.Format("Conta incorreta: deve conter {0} digitos", Algarismos));
                return false;
            }
            if (!int.TryParse(linhaMae.CONConta, out Teste))
            {
                MessageBox.Show(string.Format("Conta incorreta: deve conter {0} digitos sem letras ou espa�os", Algarismos));
                return false;
            }

            if (linhaMae.IsCONDigitoContaNull() || (linhaMae.CONDigitoConta.Trim() == ""))
            {
                MessageBox.Show("D�gito da Conta incorreto");
                return false;
            }
            if (linhaMae.CON_BCO != 237)
                if (!int.TryParse(linhaMae.CONDigitoConta, out Teste))
                {
                    MessageBox.Show("D�gito da conta incorreto: Deve ser um n�mero");
                    return false;
                }
            if ((linhaMae.CON_BCO != 237) && (linhaMae.CON_BCO != 341))
            {
                if (linhaMae.IsCONCodigoCNRNull() || (linhaMae.CONCodigoCNR.Length != 7))
                {
                    MessageBox.Show("C�digo CNR inv�lido");
                    return false;
                }

                if (!int.TryParse(linhaMae.CONDigitoConta, out Teste))
                {
                    MessageBox.Show("C�digo CNR inv�lido: Deve conter 7 n�meros");
                    return false;
                }
            }

            if (linhaMae.IsCONMaloteNull())
            {
                MessageBox.Show("Malote n�o definido");
                return false;
            }
            if (linhaMae.IsCONAuditor1_USUNull())
            {
                MessageBox.Show("Gerente n�o definido");
                return false;
            }
            if (linhaMae.IsCONCobranca_USUNull())
            {
                MessageBox.Show("Cobran�a n�o definida");
                return false;
            };

            if (linhaMae.IsCONDivideSaldosNull())
                linhaMae.CONDivideSaldos = false;
            //if (linhaMae.CONDivideSaldos)
            //{
                PaginasCondominio.dConta dConta1 = new PaginasCondominio.dConta();
                dConta1.Fill(linhaMae.CON);
            try
            {
                if (!dConta1.validaContas(linhaMae.CON_BCO,
                                          int.Parse(linhaMae.CONAgencia),
                                          int.Parse(linhaMae.CONDigitoAgencia),
                                          int.Parse(linhaMae.CONConta),
                                          linhaMae.CONDigitoConta,
                                          int.Parse(linhaMae.CONCodigoCNR)))
                {
                    MessageBox.Show(dConta1.ErroRentabilidade);
                    return false;
                }
            }
            catch (StrongTypingException)
            {
                MessageBox.Show("erro nos dados banc�rios");
                return false;
            }
            catch (InvalidCastException)
            {
                MessageBox.Show("erro nos dados banc�rios");
                return false;
            }
            //}

            foreach (dCondominiosCampos.APARTAMENTOSRow rowAPT in dCondominiosCampos.APARTAMENTOS)
            {
                if (rowAPT.RowState == DataRowState.Deleted)
                    continue;
                if (rowAPT.IsAPTProprietario_PESNull())
                {
                    MessageBox.Show(string.Format("Apartamento {0} - {1} sem propriet�rio", rowAPT.BLOCOSRow.BLOCodigo, rowAPT.APTNumero));
                    return false;
                };
            };

            string Ass = string.Format("AssFR{0}{1}", linhaMae.CON, linhaMae.CON % 7);
            if ((linhaMae.CON == 758) && linhaMae.CONHistorico.Contains(Ass))
            {
                //Refer�ncia OneNote: Desativa��o de fun��o. cadastros.dll 11.1.7.4
                linhaMae.CONHistorico = linhaMae.CONHistorico.Replace(Ass, "AssCancelada");
            }
            if ((linhaMae.CONFracaoIdeal) && (!linhaMae.CONHistorico.Contains(Ass)))
            {

                decimal TotalFra = dCondominiosCampos.SomaFracao();
                //if ((TotalFra < 99.9950M) || (TotalFra > 100.0049M))
                if ((TotalFra < 99.9900M) || (TotalFra > 100.0099M))
                {
                    MessageBox.Show("Fra��o ideal n�o soma 100%");
                    //A fun��o de ativa��o com fra��o errada foi desativada, ela s� era v�lida para condom�nios antigos.
                    if (Framework.usuarioLogado.UsuarioLogadoNeon.UsuarioLogadoSt.VerificaFuncionalidade("CADASTRO") > 3)
                    {
                        if (MessageBox.Show("Voc� quer ativar assim mesmo ?", "CONFIRMA��O", MessageBoxButtons.YesNo, MessageBoxIcon.Warning, MessageBoxDefaultButton.Button2) == DialogResult.Yes)
                        {
                            //Refer�ncia OneNote: Desativa��o de fun��o. cadastros.dll 11.1.7.4                            
                            if (DSCentral.EMP != 1)
                            {
                                MessageBox.Show("A fun��o de ativa��o com fra��o errada foi desativada, ela s� era v�lida para condom�nios antigos ou com o usu�rio Iris.");
                                return false;
                            }
                            if ((Framework.usuarioLogado.UsuarioLogadoNeon.UsuarioLogadoSt.USU != 30) && (Framework.usuarioLogado.UsuarioLogadoNeon.UsuarioLogadoSt.USU != 35))
                            {
                                MessageBox.Show("A fun��o de ativa��o com fra��o errada foi desativada, ela s� era v�lida para condom�nios antigos ou com o usu�rio Iris.");
                                return false;
                            }
                            string Justificativa = "";
                            if (VirInput.Input.Execute("justificativa:", ref Justificativa, true) && (Justificativa != ""))
                            {
                                linhaMae.CONHistorico += string.Format("\r\n{0:dd/MM/yyyy HH:mm} {1} - Fra��o ideal soma: {2} Justificativa:\r\n{3}\r\nAss:{4}\r\n",
                                    DateTime.Now,
                                    Framework.usuarioLogado.UsuarioLogadoNeon.UsuarioLogadoSt.USUNome,
                                    TotalFra,
                                    Justificativa,
                                    Ass);
                            }
                            else
                                return false;
                        }
                        else
                            return false;
                    }
                    else
                        return false;
                }
            }
            return true;
        }

        private void VerificaReserva()
        {
            //Seta ativacao/desativacao de reserva na tabela de funcionalidades do site
            string comando = "SELECT COUNT(*) from ST_CondominioFUncionalidade WHERE (CFU_CON = @P1)";
            if (VirMSSQL.TableAdapter.STTableAdapter.BuscaEscalar_int(comando, linhaMae.CON) == 0)
                comando = "INSERT INTO ST_CondominioFUncionalidade (CFU_CON, CFUReserva) VALUES (@P1, " + (linhaMae.CONReserva ? "1" : "0") + ")";
            else
                comando = "UPDATE ST_CondominioFUncionalidade SET CFUReserva = " + (linhaMae.CONReserva ? "1" : "0") + " WHERE (CFU_CON = @P1)";
            VirMSSQL.TableAdapter.STTableAdapter.ExecutarSQLNonQuery(comando, linhaMae.CON);
        }

        private void VerificaSeguro()
        {
            if (!linhaMae.IsCON_PSCNull())
            {
                if (linhaMae["CON_PSC", DataRowVersion.Original] == DBNull.Value)
                {
                    string comando =
"UPDATE APARTAMENTOS\r\n" +
"SET          APTSeguro = 1\r\n" +
"FROM     BLOCOS INNER JOIN\r\n" +
"                  APARTAMENTOS ON BLOCOS.BLO = APARTAMENTOS.APT_BLO\r\n" +
"WHERE  (BLOCOS.BLO_CON = @P1) AND (APARTAMENTOS.APTSeguro IS NULL) AND (NOT (APARTAMENTOS.APT_PSC IS NULL));";
                    VirMSSQL.TableAdapter.STTableAdapter.ExecutarSQLNonQuery(comando,linhaMae.CON);
                    /*
                    if (MessageBox.Show("Ades�o autom�tica de todas as unidades?", "Seguro conte�do", MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button1) == DialogResult.Yes)
                    {
                        foreach (dCondominiosCampos.BLOCOSRow rowBLO in linhaMae.GetBLOCOSRows())
                            foreach (dCondominiosCampos.APARTAMENTOSRow rowAPT in rowBLO.GetAPARTAMENTOSRows())
                            {
                                if (rowAPT.IsAPTSeguroNull())
                                {
                                    //rowAPT.APTSeguro = true;
                                    //string Historico = rowAPT.IsAPTHistoricoNull() ? "" : rowAPT.APTHistorico + "\r\n\r\n";
                                    //string Just = String.Format("{0} - Cadastro alterado por {1}\r\nCadastro autom�tico do seguro conte�do",
                                    //    DateTime.Now,
                                    //    CompontesBasicos.Bancovirtual.UsuarioLogado.UsuarioLogadoSt.USUNome);
                                    //rowAPT.APTHistorico = Historico + Just;
                                    int? PES = null;
                                    if (!rowAPT.IsAPTInquilino_PESNull())
                                        PES = rowAPT.APTInquilino_PES;
                                    else if (!rowAPT.IsAPTProprietario_PESNull())
                                        PES = rowAPT.APTProprietario_PES;
                                    if (PES.HasValue)
                                    {
                                        string PESEmail = "";
                                        if (VirMSSQL.TableAdapter.STTableAdapter.BuscaEscalar("Select PESEmail from PESsoas where PES = @P1", out PESEmail, PES))
                                        {
                                            if (PESEmail != "")
                                                EnviaEmailAdesao(PESEmail);
                                        }


                                    }
                                }
                            }
                    }
                    */
                }
            }
        }

        private bool ReativacaoCondominoVoltando()
        {
            string ComandoBoletos =
"UPDATE       dbo.BOLetos\r\n" +
"SET                BOLCancelado = 1, BOLStatus = -2\r\n" +
"WHERE        (BOL_CON = @P1) AND (BOLPagamento IS NULL) AND (BOLCancelado = 0);";
            string ComandoAcordos =
"UPDATE       dbo.ACOrdos\r\n" +
"SET                ACOStatus = 3\r\n" +
"FROM            dbo.ACOrdos INNER JOIN\r\n" +
"                         dbo.APARTAMENTOS ON dbo.ACOrdos.ACO_APT = dbo.APARTAMENTOS.APT INNER JOIN\r\n" +
"                         dbo.BLOCOS ON dbo.APARTAMENTOS.APT_BLO = dbo.BLOCOS.BLO\r\n" +
"WHERE        (dbo.BLOCOS.BLO_CON = @P1) AND (dbo.ACOrdos.ACOStatus = 1);";
            string ComandoCheques =
"UPDATE       dbo.CHEques\r\n" +
"SET                CHEStatus = -2\r\n" +
"WHERE        (CHE_CON = @P1) AND (CHEStatus IN (1, 2, 3, 4, 10, 12, 13, 15, 16, 17));";
            string ComandoPeriodicos =
"UPDATE       dbo.PaGamentosFixos\r\n" +
"SET                PGFStatus = 3\r\n" +
"WHERE        (PGF_CON = @P1) AND (PGFStatus <> 3);";
            string ComandoBalancete =
"UPDATE       dbo.BALancetes\r\n" +
"SET                BALCompet = @P2\r\n" +
"WHERE        (BAL_CON = @P1) AND (BALSTATUS IN (0, 1));";
            Framework.objetosNeon.Competencia ProxComp = Framework.objetosNeon.InputCompetencia.stInput((new Framework.objetosNeon.Competencia()).Add(1));
            if (ProxComp == null)
                return false;
            if (MessageBox.Show("Confirma a reativa��o com o cancelamento de todos os boletos e d�bitos pendentes?","Confirma��o de retorno de condom�nio",MessageBoxButtons.YesNo,MessageBoxIcon.Warning,MessageBoxDefaultButton.Button2) != DialogResult.Yes)
                return false;
            try
            {
                VirMSSQL.TableAdapter.AbreTrasacaoSQL("ReativacaoCondominoVoltando cCondominiosCampos2.cs 858");
                VirMSSQL.TableAdapter.STTableAdapter.ExecutarSQLNonQuery(ComandoBoletos,linhaMae.CON);
                VirMSSQL.TableAdapter.STTableAdapter.ExecutarSQLNonQuery(ComandoAcordos, linhaMae.CON);
                VirMSSQL.TableAdapter.STTableAdapter.ExecutarSQLNonQuery(ComandoCheques, linhaMae.CON);
                VirMSSQL.TableAdapter.STTableAdapter.ExecutarSQLNonQuery(ComandoPeriodicos, linhaMae.CON);
                VirMSSQL.TableAdapter.STTableAdapter.ExecutarSQLNonQuery(ComandoBalancete, linhaMae.CON,ProxComp.CompetenciaBind);
                VirMSSQL.TableAdapter.CommitSQL();
            }
            catch (Exception ex)
            {
                VirMSSQL.TableAdapter.VircatchSQL(ex);
                throw new Exception("Erro na reativa��o", ex);
            }
            return true;
        }

        private void BtAtivar_Click(object sender, EventArgs e)
        {
            if (linhaMae.CON_EMP != DSCentral.EMP)
                return;
            if (!ValidoParaAtivacao())
                return;
            if (linhaMae.CONStatus == (int)CONStatus.Inativo)
                if (!ReativacaoCondominoVoltando())
                    return;
            linhaMae.CONStatus = (int)CONStatus.Ativado;
            if (linhaMae.IsCONDataInclusaoNull())
            {
                linhaMae.CONDataInclusao = DateTime.Now;
                linhaMae.CONInclusao_USU = Framework.usuarioLogado.UsuarioLogadoNeon.UsuarioLogadoStNEON.USU;
            }
            if (linhaMae.IsCONOcultoNull())
                linhaMae.CONOculto = false;
            if (Grava(false))
            {                
                FrameworkProc.datasets.dCondominiosAtivos.dCondominiosAtivosST.CONDOMINIOS.Clear();
                FrameworkProc.datasets.dCondominiosAtivos.dCondominiosTodosST.CONDOMINIOS.Clear();                
                //Exporta();
                FechaTela(DialogResult.OK);
            }
        }

        /*
        //For�ar a exporta��o
        private void CorrecaoEXP()
        {
            dCondominiosCampos.CONDOMINIOSRow row = linhaMae;

            fExportToMsAccess _Export = new fExportToMsAccess();

            _Export.ExportarCondominioParaMsAccessRow(row);
            fExportToMsAccess.ExportaSindico(row.CON);

            foreach (dCondominiosCampos.APARTAMENTOSRow AProw in dCondominiosCampos.APARTAMENTOS)
            {
                if (AProw.RowState != DataRowState.Deleted)
                {
                    dCondominiosCampos.BLOCOSRow rowBloco = dCondominiosCampos.BLOCOS.FindByBLO(AProw.APT_BLO);
                    _Export.ExportarApartamentosParaMsAccessRow(row.CONCodigo, rowBloco.BLOCodigo, AProw);
                    Int32 Codigoforpaga = 4;
                    if (AProw["APTProprietario_PES"].ToString() != "")
                    {
                        if (AProw["APTFormaPagtoProprietario_FPG"] != DBNull.Value)
                            Codigoforpaga = AProw.APTFormaPagtoProprietario_FPG;
                        _Export.ExportarInquilinoProprietarioParaMsAccess(true, AProw.APTProprietario_PES, row.CONCodigo, rowBloco.BLOCodigo, AProw.APTNumero, Codigoforpaga);
                    }
                    if (AProw["APTInquilino_PES"].ToString() != "")
                    {
                        if (AProw["APTFormaPagtoInquilino_FPG"] != DBNull.Value)
                            Codigoforpaga = AProw.APTFormaPagtoInquilino_FPG;
                        _Export.ExportarInquilinoProprietarioParaMsAccess(false, AProw.APTInquilino_PES, row.CONCodigo, rowBloco.BLOCodigo, AProw.APTNumero, Codigoforpaga);
                    }
                    else
                        fExportToMsAccess.ApagaInquilinoDoAccess(row.CONCodigo, rowBloco.BLOCodigo, AProw.APTNumero);
                };
            }
            MessageBox.Show("ok");
        }

        
        private void Exporta()
        {
            
            dCondominiosCampos.CONDOMINIOSRow row = linhaMae;

            fExportToMsAccess _Export = new fExportToMsAccess();

            _Export.ExportarCondominioParaMsAccessRow(row);
            fExportToMsAccess.ExportaSindico(row.CON);

            DataTable Alteracoes = dCondominiosCampos.APARTAMENTOS.GetChanges();
            if (Alteracoes != null)
            {

                foreach (dCondominiosCampos.APARTAMENTOSRow AProw in Alteracoes.Rows)
                {
                    if (AProw.RowState != DataRowState.Deleted)
                    {
                        dCondominiosCampos.BLOCOSRow rowBloco = dCondominiosCampos.BLOCOS.FindByBLO(AProw.APT_BLO);
                        _Export.ExportarApartamentosParaMsAccessRow(row.CONCodigo, rowBloco.BLOCodigo, AProw);
                        Int32 Codigoforpaga = 4;
                        if (AProw["APTProprietario_PES"].ToString() != "")
                        {
                            if (AProw["APTFormaPagtoProprietario_FPG"] != DBNull.Value)
                                Codigoforpaga = AProw.APTFormaPagtoProprietario_FPG;
                            _Export.ExportarInquilinoProprietarioParaMsAccess(true, AProw.APTProprietario_PES, row.CONCodigo, rowBloco.BLOCodigo, AProw.APTNumero, Codigoforpaga);
                        }
                        if (AProw["APTInquilino_PES"].ToString() != "")
                        {
                            if (AProw["APTFormaPagtoInquilino_FPG"] != DBNull.Value)
                                Codigoforpaga = AProw.APTFormaPagtoInquilino_FPG;
                            _Export.ExportarInquilinoProprietarioParaMsAccess(false, AProw.APTInquilino_PES, row.CONCodigo, rowBloco.BLOCodigo, AProw.APTNumero, Codigoforpaga);
                        }
                        else
                            fExportToMsAccess.ApagaInquilinoDoAccess(row.CONCodigo, rowBloco.BLOCodigo, AProw.APTNumero);
                    };
                }
            };

            
        }*/

        private void simpleButton1_Click(object sender, EventArgs e)
        {
            if (linhaMae.CON_EMP != DSCentral.EMP)
                return;
            if (MessageBox.Show("Confirma que o condom�nio n�o est� mais ativo ?", "CONFIRMA��O", MessageBoxButtons.YesNo, MessageBoxIcon.Warning, MessageBoxDefaultButton.Button2) != DialogResult.Yes)
                return;
            linhaMae.CONStatus = (int)CONStatus.Inativo;
            if (Grava(false))
            {
                //Exporta();
                //GradeChamadora.RefreshDadosManual();
                FechaTela(DialogResult.OK);
            }
        }

        

/*
        private string conteudohtml1 =
"<html xmlns:v=\"urn:schemas-microsoft-com:vml\"\r\n" +
"xmlns:o=\"urn:schemas-microsoft-com:office:office\"\r\n" +
"xmlns:w=\"urn:schemas-microsoft-com:office:word\"\r\n" +
"xmlns:m=\"http://schemas.microsoft.com/office/2004/12/omml\"\r\n" +
"xmlns=\"http://www.w3.org/TR/REC-html40\">\r\n" +
"\r\n" +
"<head>\r\n" +
"<meta http-equiv=Content-Type content=\"text/html; charset=windows-1252\">\r\n" +
"<meta name=ProgId content=Word.Document>\r\n" +
"<meta name=Generator content=\"Microsoft Word 14\">\r\n" +
"<meta name=Originator content=\"Microsoft Word 14\">\r\n" +
"<link rel=File-List\r\n" +
"href=\"Seguro%20Conte�do-Carta%20cond�mino_arquivos/filelist.xml\">\r\n" +
"<title>Sr</title>\r\n" +
"<!--[if gte mso 9]><xml>\r\n" +
" <o:DocumentProperties>\r\n" +
"  <o:Author>taniap</o:Author>\r\n" +
"  <o:LastAuthor>Luis</o:LastAuthor>\r\n" +
"  <o:Revision>2</o:Revision>\r\n" +
"  <o:TotalTime>26</o:TotalTime>\r\n" +
"  <o:Created>2012-10-23T14:42:00Z</o:Created>\r\n" +
"  <o:LastSaved>2012-10-23T14:42:00Z</o:LastSaved>\r\n" +
"  <o:Pages>1</o:Pages>\r\n" +
"  <o:Words>337</o:Words>\r\n" +
"  <o:Characters>1823</o:Characters>\r\n" +
"  <o:Company>Microsoft</o:Company>\r\n" +
"  <o:Lines>15</o:Lines>\r\n" +
"  <o:Paragraphs>4</o:Paragraphs>\r\n" +
"  <o:CharactersWithSpaces>2156</o:CharactersWithSpaces>\r\n" +
"  <o:Version>14.00</o:Version>\r\n" +
" </o:DocumentProperties>\r\n" +
" <o:OfficeDocumentSettings>\r\n" +
"  <o:TargetScreenSize>800x600</o:TargetScreenSize>\r\n" +
" </o:OfficeDocumentSettings>\r\n" +
"</xml><![endif]-->\r\n" +
"<link rel=themeData\r\n" +
"href=\"Seguro%20Conte�do-Carta%20cond�mino_arquivos/themedata.thmx\">\r\n" +
"<link rel=colorSchemeMapping\r\n" +
"href=\"Seguro%20Conte�do-Carta%20cond�mino_arquivos/colorschememapping.xml\">\r\n" +
"<!--[if gte mso 9]><xml>\r\n" +
" <w:WordDocument>\r\n" +
"  <w:SpellingState>Clean</w:SpellingState>\r\n" +
"  <w:GrammarState>Clean</w:GrammarState>\r\n" +
"  <w:TrackMoves>false</w:TrackMoves>\r\n" +
"  <w:TrackFormatting/>\r\n" +
"  <w:HyphenationZone>21</w:HyphenationZone>\r\n" +
"  <w:PunctuationKerning/>\r\n" +
"  <w:ValidateAgainstSchemas/>\r\n" +
"  <w:SaveIfXMLInvalid>false</w:SaveIfXMLInvalid>\r\n" +
"  <w:IgnoreMixedContent>false</w:IgnoreMixedContent>\r\n" +
"  <w:AlwaysShowPlaceholderText>false</w:AlwaysShowPlaceholderText>\r\n" +
"  <w:DoNotPromoteQF/>\r\n" +
"  <w:LidThemeOther>PT-BR</w:LidThemeOther>\r\n" +
"  <w:LidThemeAsian>X-NONE</w:LidThemeAsian>\r\n" +
"  <w:LidThemeComplexScript>X-NONE</w:LidThemeComplexScript>\r\n" +
"  <w:Compatibility>\r\n" +
"   <w:BreakWrappedTables/>\r\n" +
"   <w:SnapToGridInCell/>\r\n" +
"   <w:WrapTextWithPunct/>\r\n" +
"   <w:UseAsianBreakRules/>\r\n" +
"   <w:DontGrowAutofit/>\r\n" +
"   <w:DontUseIndentAsNumberingTabStop/>\r\n" +
"   <w:FELineBreak11/>\r\n" +
"   <w:WW11IndentRules/>\r\n" +
"   <w:DontAutofitConstrainedTables/>\r\n" +
"   <w:AutofitLikeWW11/>\r\n" +
"   <w:HangulWidthLikeWW11/>\r\n" +
"   <w:UseNormalStyleForList/>\r\n" +
"   <w:DontVertAlignCellWithSp/>\r\n" +
"   <w:DontBreakConstrainedForcedTables/>\r\n" +
"   <w:DontVertAlignInTxbx/>\r\n" +
"   <w:Word11KerningPairs/>\r\n" +
"   <w:CachedColBalance/>\r\n" +
"  </w:Compatibility>\r\n" +
"  <w:BrowserLevel>MicrosoftInternetExplorer4</w:BrowserLevel>\r\n" +
"  <m:mathPr>\r\n" +
"   <m:mathFont m:val=\"Cambria Math\"/>\r\n" +
"   <m:brkBin m:val=\"before\"/>\r\n" +
"   <m:brkBinSub m:val=\"&#45;-\"/>\r\n" +
"   <m:smallFrac m:val=\"off\"/>\r\n" +
"   <m:dispDef/>\r\n" +
"   <m:lMargin m:val=\"0\"/>\r\n" +
"   <m:rMargin m:val=\"0\"/>\r\n" +
"   <m:defJc m:val=\"centerGroup\"/>\r\n" +
"   <m:wrapIndent m:val=\"1440\"/>\r\n" +
"   <m:intLim m:val=\"subSup\"/>\r\n" +
"   <m:naryLim m:val=\"undOvr\"/>\r\n" +
"  </m:mathPr></w:WordDocument>\r\n" +
"</xml><![endif]--><!--[if gte mso 9]><xml>\r\n" +
" <w:LatentStyles DefLockedState=\"false\" DefUnhideWhenUsed=\"false\"\r\n" +
"  DefSemiHidden=\"false\" DefQFormat=\"false\" LatentStyleCount=\"267\">\r\n" +
"  <w:LsdException Locked=\"false\" QFormat=\"true\" Name=\"Normal\"/>\r\n" +
"  <w:LsdException Locked=\"false\" QFormat=\"true\" Name=\"heading 1\"/>\r\n" +
"  <w:LsdException Locked=\"false\" SemiHidden=\"true\" UnhideWhenUsed=\"true\"\r\n" +
"   QFormat=\"true\" Name=\"heading 2\"/>\r\n" +
"  <w:LsdException Locked=\"false\" SemiHidden=\"true\" UnhideWhenUsed=\"true\"\r\n" +
"   QFormat=\"true\" Name=\"heading 3\"/>\r\n" +
"  <w:LsdException Locked=\"false\" SemiHidden=\"true\" UnhideWhenUsed=\"true\"\r\n" +
"   QFormat=\"true\" Name=\"heading 4\"/>\r\n" +
"  <w:LsdException Locked=\"false\" SemiHidden=\"true\" UnhideWhenUsed=\"true\"\r\n" +
"   QFormat=\"true\" Name=\"heading 5\"/>\r\n" +
"  <w:LsdException Locked=\"false\" SemiHidden=\"true\" UnhideWhenUsed=\"true\"\r\n" +
"   QFormat=\"true\" Name=\"heading 6\"/>\r\n" +
"  <w:LsdException Locked=\"false\" SemiHidden=\"true\" UnhideWhenUsed=\"true\"\r\n" +
"   QFormat=\"true\" Name=\"heading 7\"/>\r\n" +
"  <w:LsdException Locked=\"false\" SemiHidden=\"true\" UnhideWhenUsed=\"true\"\r\n" +
"   QFormat=\"true\" Name=\"heading 8\"/>\r\n" +
"  <w:LsdException Locked=\"false\" SemiHidden=\"true\" UnhideWhenUsed=\"true\"\r\n" +
"   QFormat=\"true\" Name=\"heading 9\"/>\r\n" +
"  <w:LsdException Locked=\"false\" SemiHidden=\"true\" UnhideWhenUsed=\"true\"\r\n" +
"   QFormat=\"true\" Name=\"caption\"/>\r\n" +
"  <w:LsdException Locked=\"false\" QFormat=\"true\" Name=\"Title\"/>\r\n" +
"  <w:LsdException Locked=\"false\" QFormat=\"true\" Name=\"Subtitle\"/>\r\n" +
"  <w:LsdException Locked=\"false\" QFormat=\"true\" Name=\"Strong\"/>\r\n" +
"  <w:LsdException Locked=\"false\" QFormat=\"true\" Name=\"Emphasis\"/>\r\n" +
"  <w:LsdException Locked=\"false\" Priority=\"99\" SemiHidden=\"true\"\r\n" +
"   Name=\"Placeholder Text\"/>\r\n" +
"  <w:LsdException Locked=\"false\" Priority=\"1\" QFormat=\"true\" Name=\"No Spacing\"/>\r\n" +
"  <w:LsdException Locked=\"false\" Priority=\"60\" Name=\"Light Shading\"/>\r\n" +
"  <w:LsdException Locked=\"false\" Priority=\"61\" Name=\"Light List\"/>\r\n" +
"  <w:LsdException Locked=\"false\" Priority=\"62\" Name=\"Light Grid\"/>\r\n" +
"  <w:LsdException Locked=\"false\" Priority=\"63\" Name=\"Medium Shading 1\"/>\r\n" +
"  <w:LsdException Locked=\"false\" Priority=\"64\" Name=\"Medium Shading 2\"/>\r\n" +
"  <w:LsdException Locked=\"false\" Priority=\"65\" Name=\"Medium List 1\"/>\r\n" +
"  <w:LsdException Locked=\"false\" Priority=\"66\" Name=\"Medium List 2\"/>\r\n" +
"  <w:LsdException Locked=\"false\" Priority=\"67\" Name=\"Medium Grid 1\"/>\r\n" +
"  <w:LsdException Locked=\"false\" Priority=\"68\" Name=\"Medium Grid 2\"/>\r\n" +
"  <w:LsdException Locked=\"false\" Priority=\"69\" Name=\"Medium Grid 3\"/>\r\n" +
"  <w:LsdException Locked=\"false\" Priority=\"70\" Name=\"Dark List\"/>\r\n" +
"  <w:LsdException Locked=\"false\" Priority=\"71\" Name=\"Colorful Shading\"/>\r\n" +
"  <w:LsdException Locked=\"false\" Priority=\"72\" Name=\"Colorful List\"/>\r\n" +
"  <w:LsdException Locked=\"false\" Priority=\"73\" Name=\"Colorful Grid\"/>\r\n" +
"  <w:LsdException Locked=\"false\" Priority=\"60\" Name=\"Light Shading Accent 1\"/>\r\n" +
"  <w:LsdException Locked=\"false\" Priority=\"61\" Name=\"Light List Accent 1\"/>\r\n" +
"  <w:LsdException Locked=\"false\" Priority=\"62\" Name=\"Light Grid Accent 1\"/>\r\n" +
"  <w:LsdException Locked=\"false\" Priority=\"63\" Name=\"Medium Shading 1 Accent 1\"/>\r\n" +
"  <w:LsdException Locked=\"false\" Priority=\"64\" Name=\"Medium Shading 2 Accent 1\"/>\r\n" +
"  <w:LsdException Locked=\"false\" Priority=\"65\" Name=\"Medium List 1 Accent 1\"/>\r\n" +
"  <w:LsdException Locked=\"false\" Priority=\"99\" SemiHidden=\"true\" Name=\"Revision\"/>\r\n" +
"  <w:LsdException Locked=\"false\" Priority=\"34\" QFormat=\"true\"\r\n" +
"   Name=\"List Paragraph\"/>\r\n" +
"  <w:LsdException Locked=\"false\" Priority=\"29\" QFormat=\"true\" Name=\"Quote\"/>\r\n" +
"  <w:LsdException Locked=\"false\" Priority=\"30\" QFormat=\"true\"\r\n" +
"   Name=\"Intense Quote\"/>\r\n" +
"  <w:LsdException Locked=\"false\" Priority=\"66\" Name=\"Medium List 2 Accent 1\"/>\r\n" +
"  <w:LsdException Locked=\"false\" Priority=\"67\" Name=\"Medium Grid 1 Accent 1\"/>\r\n" +
"  <w:LsdException Locked=\"false\" Priority=\"68\" Name=\"Medium Grid 2 Accent 1\"/>\r\n" +
"  <w:LsdException Locked=\"false\" Priority=\"69\" Name=\"Medium Grid 3 Accent 1\"/>\r\n" +
"  <w:LsdException Locked=\"false\" Priority=\"70\" Name=\"Dark List Accent 1\"/>\r\n" +
"  <w:LsdException Locked=\"false\" Priority=\"71\" Name=\"Colorful Shading Accent 1\"/>\r\n" +
"  <w:LsdException Locked=\"false\" Priority=\"72\" Name=\"Colorful List Accent 1\"/>\r\n" +
"  <w:LsdException Locked=\"false\" Priority=\"73\" Name=\"Colorful Grid Accent 1\"/>\r\n" +
"  <w:LsdException Locked=\"false\" Priority=\"60\" Name=\"Light Shading Accent 2\"/>\r\n" +
"  <w:LsdException Locked=\"false\" Priority=\"61\" Name=\"Light List Accent 2\"/>\r\n" +
"  <w:LsdException Locked=\"false\" Priority=\"62\" Name=\"Light Grid Accent 2\"/>\r\n" +
"  <w:LsdException Locked=\"false\" Priority=\"63\" Name=\"Medium Shading 1 Accent 2\"/>\r\n" +
"  <w:LsdException Locked=\"false\" Priority=\"64\" Name=\"Medium Shading 2 Accent 2\"/>\r\n" +
"  <w:LsdException Locked=\"false\" Priority=\"65\" Name=\"Medium List 1 Accent 2\"/>\r\n" +
"  <w:LsdException Locked=\"false\" Priority=\"66\" Name=\"Medium List 2 Accent 2\"/>\r\n" +
"  <w:LsdException Locked=\"false\" Priority=\"67\" Name=\"Medium Grid 1 Accent 2\"/>\r\n" +
"  <w:LsdException Locked=\"false\" Priority=\"68\" Name=\"Medium Grid 2 Accent 2\"/>\r\n" +
"  <w:LsdException Locked=\"false\" Priority=\"69\" Name=\"Medium Grid 3 Accent 2\"/>\r\n" +
"  <w:LsdException Locked=\"false\" Priority=\"70\" Name=\"Dark List Accent 2\"/>\r\n" +
"  <w:LsdException Locked=\"false\" Priority=\"71\" Name=\"Colorful Shading Accent 2\"/>\r\n" +
"  <w:LsdException Locked=\"false\" Priority=\"72\" Name=\"Colorful List Accent 2\"/>\r\n" +
"  <w:LsdException Locked=\"false\" Priority=\"73\" Name=\"Colorful Grid Accent 2\"/>\r\n" +
"  <w:LsdException Locked=\"false\" Priority=\"60\" Name=\"Light Shading Accent 3\"/>\r\n" +
"  <w:LsdException Locked=\"false\" Priority=\"61\" Name=\"Light List Accent 3\"/>\r\n" +
"  <w:LsdException Locked=\"false\" Priority=\"62\" Name=\"Light Grid Accent 3\"/>\r\n" +
"  <w:LsdException Locked=\"false\" Priority=\"63\" Name=\"Medium Shading 1 Accent 3\"/>\r\n" +
"  <w:LsdException Locked=\"false\" Priority=\"64\" Name=\"Medium Shading 2 Accent 3\"/>\r\n" +
"  <w:LsdException Locked=\"false\" Priority=\"65\" Name=\"Medium List 1 Accent 3\"/>\r\n" +
"  <w:LsdException Locked=\"false\" Priority=\"66\" Name=\"Medium List 2 Accent 3\"/>\r\n" +
"  <w:LsdException Locked=\"false\" Priority=\"67\" Name=\"Medium Grid 1 Accent 3\"/>\r\n" +
"  <w:LsdException Locked=\"false\" Priority=\"68\" Name=\"Medium Grid 2 Accent 3\"/>\r\n" +
"  <w:LsdException Locked=\"false\" Priority=\"69\" Name=\"Medium Grid 3 Accent 3\"/>\r\n" +
"  <w:LsdException Locked=\"false\" Priority=\"70\" Name=\"Dark List Accent 3\"/>\r\n" +
"  <w:LsdException Locked=\"false\" Priority=\"71\" Name=\"Colorful Shading Accent 3\"/>\r\n" +
"  <w:LsdException Locked=\"false\" Priority=\"72\" Name=\"Colorful List Accent 3\"/>\r\n" +
"  <w:LsdException Locked=\"false\" Priority=\"73\" Name=\"Colorful Grid Accent 3\"/>\r\n" +
"  <w:LsdException Locked=\"false\" Priority=\"60\" Name=\"Light Shading Accent 4\"/>\r\n" +
"  <w:LsdException Locked=\"false\" Priority=\"61\" Name=\"Light List Accent 4\"/>\r\n" +
"  <w:LsdException Locked=\"false\" Priority=\"62\" Name=\"Light Grid Accent 4\"/>\r\n" +
"  <w:LsdException Locked=\"false\" Priority=\"63\" Name=\"Medium Shading 1 Accent 4\"/>\r\n" +
"  <w:LsdException Locked=\"false\" Priority=\"64\" Name=\"Medium Shading 2 Accent 4\"/>\r\n" +
"  <w:LsdException Locked=\"false\" Priority=\"65\" Name=\"Medium List 1 Accent 4\"/>\r\n" +
"  <w:LsdException Locked=\"false\" Priority=\"66\" Name=\"Medium List 2 Accent 4\"/>\r\n" +
"  <w:LsdException Locked=\"false\" Priority=\"67\" Name=\"Medium Grid 1 Accent 4\"/>\r\n" +
"  <w:LsdException Locked=\"false\" Priority=\"68\" Name=\"Medium Grid 2 Accent 4\"/>\r\n" +
"  <w:LsdException Locked=\"false\" Priority=\"69\" Name=\"Medium Grid 3 Accent 4\"/>\r\n" +
"  <w:LsdException Locked=\"false\" Priority=\"70\" Name=\"Dark List Accent 4\"/>\r\n" +
"  <w:LsdException Locked=\"false\" Priority=\"71\" Name=\"Colorful Shading Accent 4\"/>\r\n" +
"  <w:LsdException Locked=\"false\" Priority=\"72\" Name=\"Colorful List Accent 4\"/>\r\n" +
"  <w:LsdException Locked=\"false\" Priority=\"73\" Name=\"Colorful Grid Accent 4\"/>\r\n" +
"  <w:LsdException Locked=\"false\" Priority=\"60\" Name=\"Light Shading Accent 5\"/>\r\n" +
"  <w:LsdException Locked=\"false\" Priority=\"61\" Name=\"Light List Accent 5\"/>\r\n" +
"  <w:LsdException Locked=\"false\" Priority=\"62\" Name=\"Light Grid Accent 5\"/>\r\n" +
"  <w:LsdException Locked=\"false\" Priority=\"63\" Name=\"Medium Shading 1 Accent 5\"/>\r\n" +
"  <w:LsdException Locked=\"false\" Priority=\"64\" Name=\"Medium Shading 2 Accent 5\"/>\r\n" +
"  <w:LsdException Locked=\"false\" Priority=\"65\" Name=\"Medium List 1 Accent 5\"/>\r\n" +
"  <w:LsdException Locked=\"false\" Priority=\"66\" Name=\"Medium List 2 Accent 5\"/>\r\n" +
"  <w:LsdException Locked=\"false\" Priority=\"67\" Name=\"Medium Grid 1 Accent 5\"/>\r\n" +
"  <w:LsdException Locked=\"false\" Priority=\"68\" Name=\"Medium Grid 2 Accent 5\"/>\r\n" +
"  <w:LsdException Locked=\"false\" Priority=\"69\" Name=\"Medium Grid 3 Accent 5\"/>\r\n" +
"  <w:LsdException Locked=\"false\" Priority=\"70\" Name=\"Dark List Accent 5\"/>\r\n" +
"  <w:LsdException Locked=\"false\" Priority=\"71\" Name=\"Colorful Shading Accent 5\"/>\r\n" +
"  <w:LsdException Locked=\"false\" Priority=\"72\" Name=\"Colorful List Accent 5\"/>\r\n" +
"  <w:LsdException Locked=\"false\" Priority=\"73\" Name=\"Colorful Grid Accent 5\"/>\r\n" +
"  <w:LsdException Locked=\"false\" Priority=\"60\" Name=\"Light Shading Accent 6\"/>\r\n" +
"  <w:LsdException Locked=\"false\" Priority=\"61\" Name=\"Light List Accent 6\"/>\r\n" +
"  <w:LsdException Locked=\"false\" Priority=\"62\" Name=\"Light Grid Accent 6\"/>\r\n" +
"  <w:LsdException Locked=\"false\" Priority=\"63\" Name=\"Medium Shading 1 Accent 6\"/>\r\n" +
"  <w:LsdException Locked=\"false\" Priority=\"64\" Name=\"Medium Shading 2 Accent 6\"/>\r\n" +
"  <w:LsdException Locked=\"false\" Priority=\"65\" Name=\"Medium List 1 Accent 6\"/>\r\n" +
"  <w:LsdException Locked=\"false\" Priority=\"66\" Name=\"Medium List 2 Accent 6\"/>\r\n" +
"  <w:LsdException Locked=\"false\" Priority=\"67\" Name=\"Medium Grid 1 Accent 6\"/>\r\n" +
"  <w:LsdException Locked=\"false\" Priority=\"68\" Name=\"Medium Grid 2 Accent 6\"/>\r\n" +
"  <w:LsdException Locked=\"false\" Priority=\"69\" Name=\"Medium Grid 3 Accent 6\"/>\r\n" +
"  <w:LsdException Locked=\"false\" Priority=\"70\" Name=\"Dark List Accent 6\"/>\r\n" +
"  <w:LsdException Locked=\"false\" Priority=\"71\" Name=\"Colorful Shading Accent 6\"/>\r\n" +
"  <w:LsdException Locked=\"false\" Priority=\"72\" Name=\"Colorful List Accent 6\"/>\r\n" +
"  <w:LsdException Locked=\"false\" Priority=\"73\" Name=\"Colorful Grid Accent 6\"/>\r\n" +
"  <w:LsdException Locked=\"false\" Priority=\"19\" QFormat=\"true\"\r\n" +
"   Name=\"Subtle Emphasis\"/>\r\n" +
"  <w:LsdException Locked=\"false\" Priority=\"21\" QFormat=\"true\"\r\n" +
"   Name=\"Intense Emphasis\"/>\r\n" +
"  <w:LsdException Locked=\"false\" Priority=\"31\" QFormat=\"true\"\r\n" +
"   Name=\"Subtle Reference\"/>\r\n" +
"  <w:LsdException Locked=\"false\" Priority=\"32\" QFormat=\"true\"\r\n" +
"   Name=\"Intense Reference\"/>\r\n" +
"  <w:LsdException Locked=\"false\" Priority=\"33\" QFormat=\"true\" Name=\"Book Title\"/>\r\n" +
"  <w:LsdException Locked=\"false\" Priority=\"37\" SemiHidden=\"true\"\r\n" +
"   UnhideWhenUsed=\"true\" Name=\"Bibliography\"/>\r\n" +
"  <w:LsdException Locked=\"false\" Priority=\"39\" SemiHidden=\"true\"\r\n" +
"   UnhideWhenUsed=\"true\" QFormat=\"true\" Name=\"TOC Heading\"/>\r\n" +
" </w:LatentStyles>\r\n" +
"</xml><![endif]-->\r\n" +
"<style>\r\n" +
"<!--\r\n" +
" @font-face\r\n" +
"	{font-family:\"Verdana\\,Bold\";\r\n" +
"	panose-1:0 0 0 0 0 0 0 0 0 0;\r\n" +
"	mso-font-charset:0;\r\n" +
"	mso-generic-font-family:swiss;\r\n" +
"	mso-font-format:other;\r\n" +
"	mso-font-pitch:auto;\r\n" +
"	mso-font-signature:3 0 0 0 1 0;}\r\n" +
"@font-face\r\n" +
"	{font-family:Verdana;\r\n" +
"	panose-1:2 11 6 4 3 5 4 4 2 4;\r\n" +
"	mso-font-charset:0;\r\n" +
"	mso-generic-font-family:swiss;\r\n" +
"	mso-font-pitch:variable;\r\n" +
"	mso-font-signature:-1593833729 1073750107 16 0 415 0;}\r\n" +
"@font-face\r\n" +
"	{font-family:ArialMT;\r\n" +
"	panose-1:0 0 0 0 0 0 0 0 0 0;\r\n" +
"	mso-font-charset:0;\r\n" +
"	mso-generic-font-family:swiss;\r\n" +
"	mso-font-format:other;\r\n" +
"	mso-font-pitch:auto;\r\n" +
"	mso-font-signature:3 0 0 0 1 0;}\r\n" +
" p.MsoNormal, li.MsoNormal, div.MsoNormal\r\n" +
"	{mso-style-unhide:no;\r\n" +
"	mso-style-qformat:yes;\r\n" +
"	mso-style-parent:\"\";\r\n" +
"	margin:0cm;\r\n" +
"	margin-bottom:.0001pt;\r\n" +
"	mso-pagination:widow-orphan;\r\n" +
"	font-size:12.0pt;\r\n" +
"	font-family:\"Times New Roman\",\"serif\";\r\n" +
"	mso-fareast-font-family:\"Times New Roman\";}\r\n" +
"a:link, span.MsoHyperlink\r\n" +
"	{mso-style-unhide:no;\r\n" +
"	color:blue;\r\n" +
"	text-decoration:underline;\r\n" +
"	text-underline:single;}\r\n" +
"a:visited, span.MsoHyperlinkFollowed\r\n" +
"	{mso-style-unhide:no;\r\n" +
"	color:purple;\r\n" +
"	mso-themecolor:followedhyperlink;\r\n" +
"	text-decoration:underline;\r\n" +
"	text-underline:single;}\r\n" +
"span.SpellE\r\n" +
"	{mso-style-name:\"\";\r\n" +
"	mso-spl-e:yes;}\r\n" +
"span.GramE\r\n" +
"	{mso-style-name:\"\";\r\n" +
"	mso-gram-e:yes;}\r\n" +
"@page WordSection1\r\n" +
"	{size:595.3pt 841.9pt;\r\n" +
"	margin:70.85pt 3.0cm 70.85pt 3.0cm;\r\n" +
"	mso-header-margin:35.4pt;\r\n" +
"	mso-footer-margin:35.4pt;\r\n" +
"	mso-paper-source:0;}\r\n" +
"div.WordSection1\r\n" +
"	{page:WordSection1;}\r\n" +
"-->\r\n" +
"</style>\r\n" +
"<!--[if gte mso 10]>\r\n" +
"<style>\r\n" +
" table.MsoNormalTable\r\n" +
"	{mso-style-name:\"Tabela normal\";\r\n" +
"	mso-tstyle-rowband-size:0;\r\n" +
"	mso-tstyle-colband-size:0;\r\n" +
"	mso-style-noshow:yes;\r\n" +
"	mso-style-unhide:no;\r\n" +
"	mso-style-parent:\"\";\r\n" +
"	mso-padding-alt:0cm 5.4pt 0cm 5.4pt;\r\n" +
"	mso-para-margin:0cm;\r\n" +
"	mso-para-margin-bottom:.0001pt;\r\n" +
"	mso-pagination:widow-orphan;\r\n" +
"	font-size:10.0pt;\r\n" +
"	font-family:\"Times New Roman\",\"serif\";}\r\n" +
"</style>\r\n" +
"<![endif]--><!--[if gte mso 9]><xml>\r\n" +
" <o:shapedefaults v:ext=\"edit\" spidmax=\"1026\"/>\r\n" +
"</xml><![endif]--><!--[if gte mso 9]><xml>\r\n" +
" <o:shapelayout v:ext=\"edit\">\r\n" +
"  <o:idmap v:ext=\"edit\" data=\"1\"/>\r\n" +
" </o:shapelayout></xml><![endif]-->\r\n" +
"</head>\r\n" +
"\r\n" +
"<body lang=PT-BR link=blue vlink=purple style='tab-interval:35.4pt'>\r\n" +
"\r\n" +
"<div class=WordSection1>\r\n" +
"\r\n" +
"<p class=MsoNormal style='mso-layout-grid-align:none;text-autospace:none'><span\r\n" +
"class=GramE><b><span style='font-size:10.0pt;font-family:\"Verdana,Bold\",\"sans-serif\";\r\n" +
"mso-bidi-font-family:\"Verdana\\,Bold\";color:black'>Sr.</span></b></span><b><span\r\n" +
"style='font-size:10.0pt;font-family:\"Verdana,Bold\",\"sans-serif\";mso-bidi-font-family:\r\n" +
"\"Verdana\\,Bold\";color:black'> Cond�mino,<o:p></o:p></span></b></p>\r\n" +
"\r\n" +
"<p class=MsoNormal style='text-align:justify;mso-layout-grid-align:none;\r\n" +
"text-autospace:none'><b><span style='font-size:10.0pt;font-family:\"Verdana,Bold\",\"sans-serif\";\r\n" +
"mso-bidi-font-family:\"Verdana\\,Bold\";color:black'><o:p>&nbsp;</o:p></span></b></p>\r\n" +
"\r\n" +
"<p class=MsoNormal style='text-align:justify;mso-layout-grid-align:none;\r\n" +
"text-autospace:none'><span style='font-size:10.0pt;font-family:\"Verdana,Bold\",\"sans-serif\";\r\n" +
"mso-bidi-font-family:\"Verdana\\,Bold\";color:black;mso-bidi-font-weight:bold'>Verificamos\r\n" +
"que v�rios condom�nios j� possuem um <b>Seguro Conte�do com Assist�ncia 24\r\n" +
"horas</b>. Em fun��o do nosso volume, conseguimos junto � <b>Mar�tima Seguros </b>esse\r\n" +
"produto numa condi��o muito especial. <o:p></o:p></span></p>\r\n" +
"\r\n" +
"<p class=MsoNormal style='text-align:justify;mso-layout-grid-align:none;\r\n" +
"text-autospace:none'><span style='font-size:10.0pt;font-family:\"Verdana,Bold\",\"sans-serif\";\r\n" +
"mso-bidi-font-family:\"Verdana\\,Bold\";color:black;mso-bidi-font-weight:bold'><o:p>&nbsp;</o:p></span></p>\r\n" +
"\r\n" +
"<p class=MsoNormal style='text-align:justify;mso-layout-grid-align:none;\r\n" +
"text-autospace:none'><span class=GramE><span style='font-size:10.0pt;\r\n" +
"font-family:\"Verdana\",\"sans-serif\";mso-bidi-font-family:Verdana;color:black'>�</span></span><span\r\n" +
"style='font-size:10.0pt;font-family:\"Verdana\",\"sans-serif\";mso-bidi-font-family:\r\n" +
"Verdana;color:black'> partir do pr�ximo boleto voc� j� poder� por <b>R$ 14.00 mensais</b>\r\n" +
"usufruir desse servi�o</span><span style='font-size:10.0pt;font-family:\"Verdana\",\"sans-serif\";\r\n" +
"mso-bidi-font-family:Verdana;color:black'>. <o:p></o:p></span></p>\r\n" +
"\r\n" +
"<p class=MsoNormal style='text-align:justify;mso-layout-grid-align:none;\r\n" +
"text-autospace:none'><span style='font-size:10.0pt;font-family:\"Verdana\",\"sans-serif\";\r\n" +
"mso-bidi-font-family:Verdana;color:black'><o:p>&nbsp;</o:p></span></p>\r\n" +
"\r\n" +
"<p class=MsoNormal style='text-align:justify;mso-layout-grid-align:none;\r\n" +
"text-autospace:none'><span style='font-size:10.0pt;font-family:\"Verdana\",\"sans-serif\";\r\n" +
"mso-bidi-font-family:Verdana;color:black'>O <b style='mso-bidi-font-weight:\r\n" +
"normal'>Seguro Conte�do </b>al�m de uma cobertura de <b style='mso-bidi-font-weight:\r\n" +
"normal'>R$ 100.000,00 (cem mil reais)</b> no caso de Inc�ndio, Queda de Raio e\r\n" +
"Explos�o oferece<span style='mso-spacerun:yes'>� </span><b style='mso-bidi-font-weight:\r\n" +
"normal'>Assist�ncia 24 horas</b>.<o:p></o:p></span></p>\r\n" +
"\r\n" +
"<p class=MsoNormal style='text-align:justify;mso-layout-grid-align:none;\r\n" +
"text-autospace:none'><span style='font-size:10.0pt;font-family:\"Verdana\",\"sans-serif\";\r\n" +
"mso-bidi-font-family:Verdana;color:black'><o:p>&nbsp;</o:p></span></p>\r\n" +
"\r\n" +
"<p class=MsoNormal style='text-align:justify;mso-layout-grid-align:none;\r\n" +
"text-autospace:none'><b style='mso-bidi-font-weight:normal'><span\r\n" +
"style='font-size:10.0pt;font-family:\"Verdana\",\"sans-serif\";mso-bidi-font-family:\r\n" +
"Verdana;color:black'>VOC� TER� DISPON�VEL:<o:p></o:p></span></b></p>\r\n" +
"\r\n" +
"<p class=MsoNormal style='text-align:justify;mso-layout-grid-align:none;\r\n" +
"text-autospace:none'><b style='mso-bidi-font-weight:normal'><span\r\n" +
"style='font-size:10.0pt;font-family:\"Verdana\",\"sans-serif\";mso-bidi-font-family:\r\n" +
"Verdana;color:black'>-CHAVEIRO<o:p></o:p></span></b></p>\r\n" +
"\r\n" +
"<p class=MsoNormal style='text-align:justify;mso-layout-grid-align:none;\r\n" +
"text-autospace:none'><b style='mso-bidi-font-weight:normal'><span\r\n" +
"style='font-size:10.0pt;font-family:\"Verdana\",\"sans-serif\";mso-bidi-font-family:\r\n" +
"Verdana;color:black'>-ELETRICISTA<o:p></o:p></span></b></p>\r\n" +
"\r\n" +
"<p class=MsoNormal style='text-align:justify;mso-layout-grid-align:none;\r\n" +
"text-autospace:none'><b style='mso-bidi-font-weight:normal'><span\r\n" +
"style='font-size:10.0pt;font-family:\"Verdana\",\"sans-serif\";mso-bidi-font-family:\r\n" +
"Verdana;color:black'>-ENCANADOR<o:p></o:p></span></b></p>\r\n" +
"\r\n" +
"<p class=MsoNormal style='text-align:justify;mso-layout-grid-align:none;\r\n" +
"text-autospace:none'><b style='mso-bidi-font-weight:normal'><span\r\n" +
"style='font-size:10.0pt;font-family:\"Verdana\",\"sans-serif\";mso-bidi-font-family:\r\n" +
"Verdana;color:black'>-M�O DE OBRA EL�TRICA<o:p></o:p></span></b></p>\r\n" +
"\r\n" +
"<p class=MsoNormal style='text-align:justify;mso-layout-grid-align:none;\r\n" +
"text-autospace:none'><b style='mso-bidi-font-weight:normal'><span\r\n" +
"style='font-size:10.0pt;font-family:\"Verdana\",\"sans-serif\";mso-bidi-font-family:\r\n" +
"Verdana;color:black'>-M�O DE HIDR�ULICA<o:p></o:p></span></b></p>\r\n" +
"\r\n" +
"<p class=MsoNormal style='text-align:justify;mso-layout-grid-align:none;\r\n" +
"text-autospace:none'><b style='mso-bidi-font-weight:normal'><span\r\n" +
"style='font-size:10.0pt;font-family:\"Verdana\",\"sans-serif\";mso-bidi-font-family:\r\n" +
"Verdana;color:black'>-VIDRACEIRO<o:p></o:p></span></b></p>\r\n" +
"\r\n" +
"<p class=MsoNormal style='text-align:justify;mso-layout-grid-align:none;\r\n" +
"text-autospace:none'><b style='mso-bidi-font-weight:normal'><span\r\n" +
"style='font-size:10.0pt;font-family:\"Verdana\",\"sans-serif\";mso-bidi-font-family:\r\n" +
"Verdana;color:black'>-ENTRE OUTROS<o:p></o:p></span></b></p>\r\n" +
"\r\n" +
"<p class=MsoNormal style='text-align:justify;mso-layout-grid-align:none;\r\n" +
"text-autospace:none'><b style='mso-bidi-font-weight:normal'><span\r\n" +
"style='font-size:10.0pt;font-family:\"Verdana\",\"sans-serif\";mso-bidi-font-family:\r\n" +
"Verdana;color:black'>24 HORAS POR DIA � 365 DIAS POR ANO.<o:p></o:p></span></b></p>\r\n" +
"\r\n" +
"<p class=MsoNormal style='text-align:justify;mso-layout-grid-align:none;\r\n" +
"text-autospace:none'><b style='mso-bidi-font-weight:normal'><span\r\n" +
"style='font-size:10.0pt;font-family:\"Verdana\",\"sans-serif\";mso-bidi-font-family:\r\n" +
"Verdana;color:black'><o:p>&nbsp;</o:p></span></b></p>\r\n" +
"\r\n" +
"<p class=MsoNormal style='text-align:justify;mso-layout-grid-align:none;\r\n" +
"text-autospace:none'><span style='font-size:10.0pt;font-family:\"Verdana\",\"sans-serif\";\r\n" +
"mso-bidi-font-family:Verdana;color:black'>Al�m dos servi�os e da cobertura de\r\n" +
"R$ 100.000,00 voc� ainda ter� no caso de inc�ndio um valor para alugar outro\r\n" +
"im�vel por um per�odo m�ximo de <span class=GramE>6</span> (seis) meses ou at�\r\n" +
"esgotar o limite da indeniza��o que � de R$ 10.000,00 (dez mil reais) � o que\r\n" +
"ocorrer primeiro.<o:p></o:p></span></p>\r\n" +
"\r\n" +
"<p class=MsoNormal style='text-align:justify;mso-layout-grid-align:none;\r\n" +
"text-autospace:none'><span style='font-size:10.0pt;font-family:\"Verdana\",\"sans-serif\";\r\n" +
"mso-bidi-font-family:Verdana;color:black'><o:p>&nbsp;</o:p></span></p>\r\n" +
"\r\n" +
"<p class=MsoNormal style='text-align:justify;mso-layout-grid-align:none;\r\n" +
"text-autospace:none'><span style='font-size:10.0pt;font-family:\"Verdana\",\"sans-serif\";\r\n" +
"mso-bidi-font-family:Verdana;color:black'>O valor ser� cobrado mensalmente no\r\n" +
"mesmo boleto de pagamento do condom�nio.<o:p></o:p></span></p>\r\n" +
"\r\n" +
"<p class=MsoNormal style='text-align:justify;mso-layout-grid-align:none;\r\n" +
"text-autospace:none'><span style='font-size:10.0pt;font-family:\"Verdana\",\"sans-serif\";\r\n" +
"mso-bidi-font-family:Verdana;color:black'><o:p>&nbsp;</o:p></span></p>\r\n" +
"\r\n" +
"<p class=MsoNormal style='text-align:justify;mso-layout-grid-align:none;\r\n" +
"text-autospace:none'><b><span style='font-family:\"Verdana,Bold\",\"sans-serif\";\r\n" +
"mso-bidi-font-family:\"Verdana\\,Bold\";color:black'>*Ressaltamos que esse seguro �\r\n" +
"OPCIONAL. Caso o <span class=SpellE><span class=GramE>Sr</span></span><span\r\n" +
"class=GramE>(</span>a) N�O<o:p></o:p></span></b></p>\r\n" +
"\r\n" +
"<p class=MsoNormal style='text-align:justify;mso-layout-grid-align:none;\r\n" +
"text-autospace:none'><span class=GramE><b><span style='font-family:\"Verdana,Bold\",\"sans-serif\";\r\n" +
"mso-bidi-font-family:\"Verdana\\,Bold\";color:black'>queira</span></b></span><b><span\r\n" +
"style='font-family:\"Verdana,Bold\",\"sans-serif\";mso-bidi-font-family:\"Verdana\\,Bold\";\r\n" +
"color:black'> fazer a ades�o, basta alterar seu cadastro no site da Neon, ligar\r\n" +
"para o Gerente do Condom�nio pelo F: ";
        private string conteudohtml2 =
" solicitando a retirada deste\r\n" +
"servi�o ou simplesmente abater no momento do pagamento do boleto que\r\n" +
"automaticamente a sua unidade ficar� isenta e <span class=SpellE>descadastrada</span>\r\n" +
"para os pr�ximos meses. <o:p></o:p></span></b></p>\r\n" +
"\r\n" +
"<p class=MsoNormal style='text-align:justify;mso-layout-grid-align:none;\r\n" +
"text-autospace:none'><span style='font-size:10.0pt;font-family:\"Verdana\",\"sans-serif\";\r\n" +
"mso-bidi-font-family:Verdana;color:black'><o:p>&nbsp;</o:p></span></p>\r\n" +
"\r\n" +
"<p class=MsoNormal style='text-align:justify;mso-layout-grid-align:none;\r\n" +
"text-autospace:none'><span style='font-size:10.0pt;font-family:\"Verdana\",\"sans-serif\";\r\n" +
"mso-bidi-font-family:Verdana;color:black'>A cobertura iniciar� no primeiro dia\r\n" +
"do m�s seguinte ao pagamento e terminar� no �ltimo dia do referido m�s. <o:p></o:p></span></p>\r\n" +
"\r\n" +
"<p class=MsoNormal style='text-align:justify;mso-layout-grid-align:none;\r\n" +
"text-autospace:none'><span style='font-size:10.0pt;font-family:\"Verdana\",\"sans-serif\";\r\n" +
"mso-bidi-font-family:Verdana;color:black'>Consulte as condi��es, garantias e\r\n" +
"outros termos da cobertura no site <a href=\"http://www.neonimoveis.com.br/\">www.neonimoveis.com.br</a><o:p></o:p></span></p>\r\n" +
"\r\n" +
"<p class=MsoNormal style='text-align:justify;mso-layout-grid-align:none;\r\n" +
"text-autospace:none'><span style='font-size:10.0pt;font-family:\"Verdana\",\"sans-serif\";\r\n" +
"mso-bidi-font-family:Verdana;color:black'><o:p>&nbsp;</o:p></span></p>\r\n" +
"\r\n" +
"<p class=MsoNormal style='text-align:justify;mso-layout-grid-align:none;\r\n" +
"text-autospace:none'><span style='font-size:10.0pt;font-family:\"Verdana\",\"sans-serif\";\r\n" +
"mso-bidi-font-family:Verdana;color:black'><o:p>&nbsp;</o:p></span></p>\r\n" +
"\r\n" +
"<p class=MsoNormal style='text-align:justify;mso-layout-grid-align:none;\r\n" +
"text-autospace:none'><b><span style='font-family:\"Verdana,Bold\",\"sans-serif\";\r\n" +
"mso-bidi-font-family:\"Verdana\\,Bold\";color:black'>Esse � um servi�o exclusivo\r\n" +
"que a<o:p></o:p></span></b></p>\r\n" +
"\r\n" +
"<p class=MsoNormal style='text-align:justify;mso-layout-grid-align:none;\r\n" +
"text-autospace:none'><span class=GramE><b><span style='font-family:\"Verdana,Bold\",\"sans-serif\";\r\n" +
"mso-bidi-font-family:\"Verdana\\,Bold\";color:black'>Neon Im�veis</span></b></span><b><span\r\n" +
"style='font-family:\"Verdana,Bold\",\"sans-serif\";mso-bidi-font-family:\"Verdana\\,Bold\";\r\n" +
"color:black'> oferece a fim de facilitar<o:p></o:p></span></b></p>\r\n" +
"\r\n" +
"<p class=MsoNormal style='text-align:justify;mso-layout-grid-align:none;\r\n" +
"text-autospace:none'><span class=GramE><b><span style='font-family:\"Verdana,Bold\",\"sans-serif\";\r\n" +
"mso-bidi-font-family:\"Verdana\\,Bold\";color:black'>sua</span></b></span><b><span\r\n" +
"style='font-family:\"Verdana,Bold\",\"sans-serif\";mso-bidi-font-family:\"Verdana\\,Bold\";\r\n" +
"color:black'> vida e assegurar seu patrim�nio.<o:p></o:p></span></b></p>\r\n" +
"\r\n" +
"<p class=MsoNormal style='text-align:justify;mso-layout-grid-align:none;\r\n" +
"text-autospace:none'><b><span style='font-family:\"Verdana,Bold\",\"sans-serif\";\r\n" +
"mso-bidi-font-family:\"Verdana\\,Bold\";color:black'><o:p>&nbsp;</o:p></span></b></p>\r\n" +
"\r\n" +
"<p class=MsoNormal style='text-align:justify;mso-layout-grid-align:none;\r\n" +
"text-autospace:none'><b><span style='font-size:14.0pt;font-family:\"Verdana,Bold\",\"sans-serif\";\r\n" +
"mso-bidi-font-family:\"Verdana\\,Bold\";color:white'>UMA EXCLUSIVIDADE <span\r\n" +
"class=GramE>DA NEON</span> PARA VOC� E SEU CONDOM�NIO<o:p></o:p></span></b></p>\r\n" +
"\r\n" +
"<p class=MsoNormal style='text-align:justify;mso-layout-grid-align:none;\r\n" +
"text-autospace:none'><span style='font-size:6.0pt;font-family:\"ArialMT\",\"sans-serif\";\r\n" +
"mso-bidi-font-family:ArialMT;color:black'>*Esse material n�o constitui um\r\n" +
"contrato de seguro, nem uma completa descri��o das condi��es, garantias,\r\n" +
"exclus�es ou outros <span class=GramE>termos</span> de cobertura. Para mais\r\n" +
"detalhes, <span class=GramE>por favor</span> consulte<o:p></o:p></span></p>\r\n" +
"\r\n" +
"<p class=MsoNormal style='text-align:justify;mso-layout-grid-align:none;\r\n" +
"text-autospace:none'><span class=GramE><span style='font-size:6.0pt;font-family:\r\n" +
"\"ArialMT\",\"sans-serif\";mso-bidi-font-family:ArialMT;color:black'>as</span></span><span\r\n" +
"style='font-size:6.0pt;font-family:\"ArialMT\",\"sans-serif\";mso-bidi-font-family:\r\n" +
"ArialMT;color:black'> condi��es gerais do produto. Processo SUSEP N�.\r\n" +
"15414.005203/2005-11 (mar�tima residencial) CNPJ: 61383493/0001-80 o registro\r\n" +
"deste plano na <span class=SpellE>sesep</span> n�o implica por parte da<o:p></o:p></span></p>\r\n" +
"\r\n" +
"<p class=MsoNormal style='text-align:justify'><span class=GramE><span\r\n" +
"style='font-size:6.0pt;font-family:\"ArialMT\",\"sans-serif\";mso-bidi-font-family:\r\n" +
"ArialMT;color:black'>autarquia</span></span><span style='font-size:6.0pt;\r\n" +
"font-family:\"ArialMT\",\"sans-serif\";mso-bidi-font-family:ArialMT;color:black'>\r\n" +
"incentivo ou recomenda��o a sua comercializa��o*.</span></p>\r\n" +
"\r\n" +
"</div>\r\n" +
"\r\n" +
"</body>\r\n" +
"\r\n" +
"</html>\r\n";
        */

    }
}

