namespace Cadastros.Condominios
{
    partial class cCondominiosGrade
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(cCondominiosGrade));
            this.colCONCodigo = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colCONNome = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colCONEndereco = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colCONBairro = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colCIDNome = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colCONCep = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colCONStatus = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemImageComboBox1 = new DevExpress.XtraEditors.Repository.RepositoryItemImageComboBox();
            this.barButtonItem1 = new DevExpress.XtraBars.BarButtonItem();
            this.colCON_EMP = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colNomeSindico = new DevExpress.XtraGrid.Columns.GridColumn();
            this.LookUpEdit1Sindico = new DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit();
            this.sindicosBindingSource = new System.Windows.Forms.BindingSource();
            this.colTelSindico = new DevExpress.XtraGrid.Columns.GridColumn();
            this.LookUpEditTelSindico = new DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit();
            this.gridColumn1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.botes = new DevExpress.XtraEditors.Repository.RepositoryItemButtonEdit();
            this.colCIDUf = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colCONCnpj = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colCONTotalBloco = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colCONTotalApartamento = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colCON_BCO = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colCONAgencia = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colCONDigitoAgencia = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colCONConta = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colCONDigitoConta = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colCONDataInclusao = new DevExpress.XtraGrid.Columns.GridColumn();
            this.barButtonItem3 = new DevExpress.XtraBars.BarButtonItem();
            this.colCONMalote = new DevExpress.XtraGrid.Columns.GridColumn();
            this.LookUpEditMalote = new DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit();
            this.bindingSourcemalotes = new System.Windows.Forms.BindingSource();
            this.colCONCobranca_USU = new DevExpress.XtraGrid.Columns.GridColumn();
            this.LookUpEditUSU = new DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit();
            this.bindingSourceUSU = new System.Windows.Forms.BindingSource();
            this.colCONAuditor1_USU = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colCONEscrit_FRN = new DevExpress.XtraGrid.Columns.GridColumn();
            this.LookUpEditADV = new DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit();
            this.bindingSourceEscADVOGADOS = new System.Windows.Forms.BindingSource();
            this.colCONProcuracao = new DevExpress.XtraGrid.Columns.GridColumn();
            this.barButtonItem4 = new DevExpress.XtraBars.BarButtonItem();
            this.panelControl1 = new DevExpress.XtraEditors.PanelControl();
            this.lookupGerente1 = new Framework.Lookup.LookupGerente();
            this.colCONCodigoFolha1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colCONCodigoCNR = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colCON = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colCON_PSC = new DevExpress.XtraGrid.Columns.GridColumn();
            this.LookUpEditPSC = new DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit();
            this.BindingSourcedPlanosSeguroConteudo = new System.Windows.Forms.BindingSource();
            this.colCONBalancete_USU = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colCONPagEletronico = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colCONAuditor2_USU = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colCONCodigoComunicacao = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemCheckEdit1 = new DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit();
            this.colCONCorretoraSeguros_FRN = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colCONSeguradora_FRN = new DevExpress.XtraGrid.Columns.GridColumn();
            ((System.ComponentModel.ISupportInitialize)(this.PopImprimir_F)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.BarManager_F)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.PopExportar_F)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.GridControl_F)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.GridView_F)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.BindingSource_F)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.StyleController_F)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemImageComboBox1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LookUpEdit1Sindico)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sindicosBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LookUpEditTelSindico)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.botes)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LookUpEditMalote)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.bindingSourcemalotes)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LookUpEditUSU)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.bindingSourceUSU)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LookUpEditADV)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.bindingSourceEscADVOGADOS)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl1)).BeginInit();
            this.panelControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.LookUpEditPSC)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.BindingSourcedPlanosSeguroConteudo)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckEdit1)).BeginInit();
            this.SuspendLayout();
            // 
            // barNavegacao_F
            // 
            this.barNavegacao_F.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(this.barButtonItem1),
            new DevExpress.XtraBars.LinkPersistInfo(this.barButtonItem3),
            new DevExpress.XtraBars.LinkPersistInfo(this.barButtonItem4)});
            this.barNavegacao_F.OptionsBar.AllowQuickCustomization = false;
            this.barNavegacao_F.OptionsBar.DisableClose = true;
            this.barNavegacao_F.OptionsBar.DisableCustomization = true;
            this.barNavegacao_F.OptionsBar.DrawDragBorder = false;
            this.barNavegacao_F.OptionsBar.DrawSizeGrip = true;
            this.barNavegacao_F.OptionsBar.RotateWhenVertical = false;
            this.barNavegacao_F.OptionsBar.UseWholeRow = true;
            // 
            // BarManager_F
            // 
            this.BarManager_F.Items.AddRange(new DevExpress.XtraBars.BarItem[] {
            this.barButtonItem1,
            this.barButtonItem3,
            this.barButtonItem4});
            this.BarManager_F.MaxItemId = 23;
            // 
            // BtnIncluir_F
            // 
            this.BtnIncluir_F.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("BtnIncluir_F.ImageOptions.Image")));
            // 
            // BtnAlterar_F
            // 
            this.BtnAlterar_F.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("BtnAlterar_F.ImageOptions.Image")));
            this.BtnAlterar_F.ImageOptions.ImageIndex = 0;
            // 
            // BtnExcluir_F
            // 
            this.BtnExcluir_F.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("BtnExcluir_F.ImageOptions.Image")));
            this.BtnExcluir_F.Visibility = DevExpress.XtraBars.BarItemVisibility.Never;
            // 
            // BtnImprimir_F
            // 
            this.BtnImprimir_F.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("BtnImprimir_F.ImageOptions.Image")));
            // 
            // BtnImprimirGrade_F
            // 
            this.BtnImprimirGrade_F.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("BtnImprimirGrade_F.ImageOptions.Image")));
            // 
            // BtnExportar_F
            // 
            this.BtnExportar_F.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("BtnExportar_F.ImageOptions.Image")));
            // 
            // btnConfirmar_F
            // 
            this.btnConfirmar_F.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("btnConfirmar_F.ImageOptions.Image")));
            // 
            // btnCancelar_F
            // 
            this.btnCancelar_F.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("btnCancelar_F.ImageOptions.Image")));
            // 
            // barDataBase_F
            // 
            this.barDataBase_F.OptionsBar.AllowQuickCustomization = false;
            this.barDataBase_F.OptionsBar.DisableClose = true;
            this.barDataBase_F.OptionsBar.DisableCustomization = true;
            this.barDataBase_F.OptionsBar.DrawDragBorder = false;
            this.barDataBase_F.OptionsBar.UseWholeRow = true;
            // 
            // GridControl_F
            // 
            this.GridControl_F.Cursor = System.Windows.Forms.Cursors.Default;
            this.GridControl_F.EmbeddedNavigator.Buttons.Append.Visible = false;
            this.GridControl_F.EmbeddedNavigator.Buttons.CancelEdit.Visible = false;
            this.GridControl_F.EmbeddedNavigator.Buttons.Edit.Visible = false;
            this.GridControl_F.EmbeddedNavigator.Buttons.EndEdit.Visible = false;
            this.GridControl_F.EmbeddedNavigator.Buttons.Remove.Visible = false;
            this.GridControl_F.Location = new System.Drawing.Point(0, 30);
            this.GridControl_F.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.LookUpEdit1Sindico,
            this.LookUpEditTelSindico,
            this.botes,
            this.LookUpEditUSU,
            this.LookUpEditADV,
            this.LookUpEditMalote,
            this.repositoryItemImageComboBox1,
            this.LookUpEditPSC,
            this.repositoryItemCheckEdit1});
            this.GridControl_F.Size = new System.Drawing.Size(1557, 716);
            // 
            // GridView_F
            // 
            this.GridView_F.Appearance.ColumnFilterButton.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(104)))), ((int)(((byte)(184)))), ((int)(((byte)(251)))));
            this.GridView_F.Appearance.ColumnFilterButton.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(104)))), ((int)(((byte)(184)))), ((int)(((byte)(251)))));
            this.GridView_F.Appearance.ColumnFilterButton.ForeColor = System.Drawing.Color.White;
            this.GridView_F.Appearance.ColumnFilterButton.Options.UseBackColor = true;
            this.GridView_F.Appearance.ColumnFilterButton.Options.UseBorderColor = true;
            this.GridView_F.Appearance.ColumnFilterButton.Options.UseForeColor = true;
            this.GridView_F.Appearance.ColumnFilterButtonActive.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(170)))), ((int)(((byte)(216)))), ((int)(((byte)(254)))));
            this.GridView_F.Appearance.ColumnFilterButtonActive.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(170)))), ((int)(((byte)(216)))), ((int)(((byte)(254)))));
            this.GridView_F.Appearance.ColumnFilterButtonActive.ForeColor = System.Drawing.Color.Black;
            this.GridView_F.Appearance.ColumnFilterButtonActive.Options.UseBackColor = true;
            this.GridView_F.Appearance.ColumnFilterButtonActive.Options.UseBorderColor = true;
            this.GridView_F.Appearance.ColumnFilterButtonActive.Options.UseForeColor = true;
            this.GridView_F.Appearance.Empty.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(236)))), ((int)(((byte)(246)))), ((int)(((byte)(255)))));
            this.GridView_F.Appearance.Empty.BackColor2 = System.Drawing.Color.White;
            this.GridView_F.Appearance.Empty.Options.UseBackColor = true;
            this.GridView_F.Appearance.EvenRow.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(247)))), ((int)(((byte)(251)))), ((int)(((byte)(255)))));
            this.GridView_F.Appearance.EvenRow.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(247)))), ((int)(((byte)(251)))), ((int)(((byte)(255)))));
            this.GridView_F.Appearance.EvenRow.ForeColor = System.Drawing.Color.Black;
            this.GridView_F.Appearance.EvenRow.Options.UseBackColor = true;
            this.GridView_F.Appearance.EvenRow.Options.UseBorderColor = true;
            this.GridView_F.Appearance.EvenRow.Options.UseForeColor = true;
            this.GridView_F.Appearance.FilterCloseButton.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(104)))), ((int)(((byte)(184)))), ((int)(((byte)(251)))));
            this.GridView_F.Appearance.FilterCloseButton.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(104)))), ((int)(((byte)(184)))), ((int)(((byte)(251)))));
            this.GridView_F.Appearance.FilterCloseButton.ForeColor = System.Drawing.Color.White;
            this.GridView_F.Appearance.FilterCloseButton.Options.UseBackColor = true;
            this.GridView_F.Appearance.FilterCloseButton.Options.UseBorderColor = true;
            this.GridView_F.Appearance.FilterCloseButton.Options.UseForeColor = true;
            this.GridView_F.Appearance.FilterPanel.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(236)))), ((int)(((byte)(246)))), ((int)(((byte)(255)))));
            this.GridView_F.Appearance.FilterPanel.BackColor2 = System.Drawing.Color.White;
            this.GridView_F.Appearance.FilterPanel.ForeColor = System.Drawing.Color.Black;
            this.GridView_F.Appearance.FilterPanel.Options.UseBackColor = true;
            this.GridView_F.Appearance.FilterPanel.Options.UseForeColor = true;
            this.GridView_F.Appearance.FixedLine.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(59)))), ((int)(((byte)(133)))), ((int)(((byte)(195)))));
            this.GridView_F.Appearance.FixedLine.Options.UseBackColor = true;
            this.GridView_F.Appearance.FocusedCell.BackColor = System.Drawing.Color.White;
            this.GridView_F.Appearance.FocusedCell.ForeColor = System.Drawing.Color.Black;
            this.GridView_F.Appearance.FocusedCell.Options.UseBackColor = true;
            this.GridView_F.Appearance.FocusedCell.Options.UseForeColor = true;
            this.GridView_F.Appearance.FocusedCell.Options.UseTextOptions = true;
            this.GridView_F.Appearance.FocusedCell.TextOptions.HotkeyPrefix = DevExpress.Utils.HKeyPrefix.Show;
            this.GridView_F.Appearance.FocusedRow.BackColor = System.Drawing.Color.DeepSkyBlue;
            this.GridView_F.Appearance.FocusedRow.BackColor2 = System.Drawing.Color.White;
            this.GridView_F.Appearance.FocusedRow.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
            this.GridView_F.Appearance.FocusedRow.ForeColor = System.Drawing.Color.Black;
            this.GridView_F.Appearance.FocusedRow.GradientMode = System.Drawing.Drawing2D.LinearGradientMode.Vertical;
            this.GridView_F.Appearance.FocusedRow.Options.UseBackColor = true;
            this.GridView_F.Appearance.FocusedRow.Options.UseFont = true;
            this.GridView_F.Appearance.FocusedRow.Options.UseForeColor = true;
            this.GridView_F.Appearance.FooterPanel.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(104)))), ((int)(((byte)(184)))), ((int)(((byte)(251)))));
            this.GridView_F.Appearance.FooterPanel.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(104)))), ((int)(((byte)(184)))), ((int)(((byte)(251)))));
            this.GridView_F.Appearance.FooterPanel.ForeColor = System.Drawing.Color.Black;
            this.GridView_F.Appearance.FooterPanel.Options.UseBackColor = true;
            this.GridView_F.Appearance.FooterPanel.Options.UseBorderColor = true;
            this.GridView_F.Appearance.FooterPanel.Options.UseForeColor = true;
            this.GridView_F.Appearance.GroupButton.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(104)))), ((int)(((byte)(184)))), ((int)(((byte)(251)))));
            this.GridView_F.Appearance.GroupButton.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(104)))), ((int)(((byte)(184)))), ((int)(((byte)(251)))));
            this.GridView_F.Appearance.GroupButton.Options.UseBackColor = true;
            this.GridView_F.Appearance.GroupButton.Options.UseBorderColor = true;
            this.GridView_F.Appearance.GroupFooter.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(170)))), ((int)(((byte)(216)))), ((int)(((byte)(254)))));
            this.GridView_F.Appearance.GroupFooter.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(170)))), ((int)(((byte)(216)))), ((int)(((byte)(254)))));
            this.GridView_F.Appearance.GroupFooter.ForeColor = System.Drawing.Color.Black;
            this.GridView_F.Appearance.GroupFooter.Options.UseBackColor = true;
            this.GridView_F.Appearance.GroupFooter.Options.UseBorderColor = true;
            this.GridView_F.Appearance.GroupFooter.Options.UseForeColor = true;
            this.GridView_F.Appearance.GroupPanel.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(236)))), ((int)(((byte)(246)))), ((int)(((byte)(255)))));
            this.GridView_F.Appearance.GroupPanel.BackColor2 = System.Drawing.Color.White;
            this.GridView_F.Appearance.GroupPanel.ForeColor = System.Drawing.Color.Black;
            this.GridView_F.Appearance.GroupPanel.Options.UseBackColor = true;
            this.GridView_F.Appearance.GroupPanel.Options.UseForeColor = true;
            this.GridView_F.Appearance.GroupRow.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(170)))), ((int)(((byte)(216)))), ((int)(((byte)(254)))));
            this.GridView_F.Appearance.GroupRow.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(170)))), ((int)(((byte)(216)))), ((int)(((byte)(254)))));
            this.GridView_F.Appearance.GroupRow.ForeColor = System.Drawing.Color.Black;
            this.GridView_F.Appearance.GroupRow.Options.UseBackColor = true;
            this.GridView_F.Appearance.GroupRow.Options.UseBorderColor = true;
            this.GridView_F.Appearance.GroupRow.Options.UseForeColor = true;
            this.GridView_F.Appearance.HeaderPanel.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(139)))), ((int)(((byte)(201)))), ((int)(((byte)(254)))));
            this.GridView_F.Appearance.HeaderPanel.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(139)))), ((int)(((byte)(201)))), ((int)(((byte)(254)))));
            this.GridView_F.Appearance.HeaderPanel.ForeColor = System.Drawing.Color.Black;
            this.GridView_F.Appearance.HeaderPanel.Options.UseBackColor = true;
            this.GridView_F.Appearance.HeaderPanel.Options.UseBorderColor = true;
            this.GridView_F.Appearance.HeaderPanel.Options.UseForeColor = true;
            this.GridView_F.Appearance.HideSelectionRow.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(105)))), ((int)(((byte)(170)))), ((int)(((byte)(225)))));
            this.GridView_F.Appearance.HideSelectionRow.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(83)))), ((int)(((byte)(155)))), ((int)(((byte)(215)))));
            this.GridView_F.Appearance.HideSelectionRow.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(236)))), ((int)(((byte)(246)))), ((int)(((byte)(255)))));
            this.GridView_F.Appearance.HideSelectionRow.Options.UseBackColor = true;
            this.GridView_F.Appearance.HideSelectionRow.Options.UseBorderColor = true;
            this.GridView_F.Appearance.HideSelectionRow.Options.UseForeColor = true;
            this.GridView_F.Appearance.HorzLine.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(104)))), ((int)(((byte)(184)))), ((int)(((byte)(251)))));
            this.GridView_F.Appearance.HorzLine.Options.UseBackColor = true;
            this.GridView_F.Appearance.OddRow.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(236)))), ((int)(((byte)(246)))), ((int)(((byte)(255)))));
            this.GridView_F.Appearance.OddRow.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(236)))), ((int)(((byte)(246)))), ((int)(((byte)(255)))));
            this.GridView_F.Appearance.OddRow.ForeColor = System.Drawing.Color.Black;
            this.GridView_F.Appearance.OddRow.Options.UseBackColor = true;
            this.GridView_F.Appearance.OddRow.Options.UseBorderColor = true;
            this.GridView_F.Appearance.OddRow.Options.UseForeColor = true;
            this.GridView_F.Appearance.Preview.Font = new System.Drawing.Font("Verdana", 7.5F);
            this.GridView_F.Appearance.Preview.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(83)))), ((int)(((byte)(155)))), ((int)(((byte)(215)))));
            this.GridView_F.Appearance.Preview.Options.UseFont = true;
            this.GridView_F.Appearance.Preview.Options.UseForeColor = true;
            this.GridView_F.Appearance.Row.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(247)))), ((int)(((byte)(251)))), ((int)(((byte)(255)))));
            this.GridView_F.Appearance.Row.ForeColor = System.Drawing.Color.Black;
            this.GridView_F.Appearance.Row.Options.UseBackColor = true;
            this.GridView_F.Appearance.Row.Options.UseForeColor = true;
            this.GridView_F.Appearance.RowSeparator.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(236)))), ((int)(((byte)(246)))), ((int)(((byte)(255)))));
            this.GridView_F.Appearance.RowSeparator.BackColor2 = System.Drawing.Color.White;
            this.GridView_F.Appearance.RowSeparator.Options.UseBackColor = true;
            this.GridView_F.Appearance.SelectedRow.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(83)))), ((int)(((byte)(155)))), ((int)(((byte)(215)))));
            this.GridView_F.Appearance.SelectedRow.ForeColor = System.Drawing.Color.White;
            this.GridView_F.Appearance.SelectedRow.Options.UseBackColor = true;
            this.GridView_F.Appearance.SelectedRow.Options.UseForeColor = true;
            this.GridView_F.Appearance.TopNewRow.BackColor = System.Drawing.Color.White;
            this.GridView_F.Appearance.TopNewRow.Options.UseBackColor = true;
            this.GridView_F.Appearance.VertLine.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(104)))), ((int)(((byte)(184)))), ((int)(((byte)(251)))));
            this.GridView_F.Appearance.VertLine.Options.UseBackColor = true;
            this.GridView_F.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colCONCodigo,
            this.colCONNome,
            this.colCONEndereco,
            this.colCIDNome,
            this.colCONBairro,
            this.colCONCep,
            this.colCONStatus,
            this.colCON_EMP,
            this.colNomeSindico,
            this.colTelSindico,
            this.gridColumn1,
            this.colCIDUf,
            this.colCONCnpj,
            this.colCONTotalBloco,
            this.colCONTotalApartamento,
            this.colCON_BCO,
            this.colCONAgencia,
            this.colCONDigitoAgencia,
            this.colCONConta,
            this.colCONDigitoConta,
            this.colCONDataInclusao,
            this.colCONMalote,
            this.colCONCobranca_USU,
            this.colCONAuditor1_USU,
            this.colCONEscrit_FRN,
            this.colCONProcuracao,
            this.colCONCodigoFolha1,
            this.colCONCodigoCNR,
            this.colCON,
            this.colCON_PSC,
            this.colCONBalancete_USU,
            this.colCONPagEletronico,
            this.colCONAuditor2_USU,
            this.colCONCodigoComunicacao,
            this.colCONCorretoraSeguros_FRN,
            this.colCONSeguradora_FRN});
            this.GridView_F.OptionsBehavior.AllowIncrementalSearch = true;
            this.GridView_F.OptionsBehavior.Editable = false;
            this.GridView_F.OptionsLayout.Columns.StoreAllOptions = true;
            this.GridView_F.OptionsLayout.Columns.StoreAppearance = true;
            this.GridView_F.OptionsLayout.StoreAllOptions = true;
            this.GridView_F.OptionsLayout.StoreAppearance = true;
            this.GridView_F.OptionsNavigation.EnterMoveNextColumn = true;
            this.GridView_F.OptionsPrint.EnableAppearanceEvenRow = true;
            this.GridView_F.OptionsPrint.EnableAppearanceOddRow = true;
            this.GridView_F.OptionsPrint.ExpandAllDetails = true;
            this.GridView_F.OptionsSelection.InvertSelection = true;
            this.GridView_F.OptionsView.ColumnAutoWidth = false;
            this.GridView_F.OptionsView.EnableAppearanceEvenRow = true;
            this.GridView_F.OptionsView.EnableAppearanceOddRow = true;
            this.GridView_F.OptionsView.GroupFooterShowMode = DevExpress.XtraGrid.Views.Grid.GroupFooterShowMode.VisibleAlways;
            this.GridView_F.OptionsView.ShowAutoFilterRow = true;
            this.GridView_F.OptionsView.ShowFooter = true;
            this.GridView_F.OptionsView.ShowGroupPanel = false;
            this.GridView_F.SortInfo.AddRange(new DevExpress.XtraGrid.Columns.GridColumnSortInfo[] {
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colCONCodigo, DevExpress.Data.ColumnSortOrder.Ascending)});
            this.GridView_F.RowCellStyle += new DevExpress.XtraGrid.Views.Grid.RowCellStyleEventHandler(this.GridView_F_RowCellStyle);
            // 
            // BindingSource_F
            // 
            this.BindingSource_F.DataMember = "CONDOMINIOS";
            this.BindingSource_F.DataSource = typeof(FrameworkProc.datasets.dCondominiosAtivos);
            // 
            // ToolTipController_F
            // 
            this.ToolTipController_F.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(222)))), ((int)(((byte)(237)))), ((int)(((byte)(250)))));
            this.ToolTipController_F.Appearance.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(20)))), ((int)(((byte)(69)))), ((int)(((byte)(141)))));
            this.ToolTipController_F.Appearance.Options.UseBackColor = true;
            this.ToolTipController_F.Appearance.Options.UseFont = true;
            this.ToolTipController_F.Appearance.Options.UseForeColor = true;
            this.ToolTipController_F.Appearance.Options.UseTextOptions = true;
            this.ToolTipController_F.Appearance.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            // 
            // StyleController_F
            // 
            this.StyleController_F.AppearanceDisabled.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this.StyleController_F.AppearanceDisabled.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this.StyleController_F.AppearanceDisabled.Options.UseBackColor = true;
            this.StyleController_F.AppearanceDisabled.Options.UseForeColor = true;
            this.StyleController_F.AppearanceFocused.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.StyleController_F.AppearanceFocused.Options.UseBackColor = true;
            this.StyleController_F.LookAndFeel.SkinName = "Lilian";
            this.StyleController_F.LookAndFeel.UseDefaultLookAndFeel = false;
            // 
            // colCONCodigo
            // 
            this.colCONCodigo.Caption = "CODCON";
            this.colCONCodigo.FieldName = "CONCodigo";
            this.colCONCodigo.Fixed = DevExpress.XtraGrid.Columns.FixedStyle.Left;
            this.colCONCodigo.Name = "colCONCodigo";
            this.colCONCodigo.OptionsColumn.AllowEdit = false;
            this.colCONCodigo.OptionsColumn.ReadOnly = true;
            this.colCONCodigo.Visible = true;
            this.colCONCodigo.VisibleIndex = 1;
            this.colCONCodigo.Width = 89;
            // 
            // colCONNome
            // 
            this.colCONNome.Caption = "Nome";
            this.colCONNome.FieldName = "CONNome";
            this.colCONNome.Fixed = DevExpress.XtraGrid.Columns.FixedStyle.Left;
            this.colCONNome.Name = "colCONNome";
            this.colCONNome.OptionsColumn.ReadOnly = true;
            this.colCONNome.Visible = true;
            this.colCONNome.VisibleIndex = 2;
            this.colCONNome.Width = 203;
            // 
            // colCONEndereco
            // 
            this.colCONEndereco.Caption = "Endere�o";
            this.colCONEndereco.FieldName = "CONEndereco";
            this.colCONEndereco.Name = "colCONEndereco";
            this.colCONEndereco.OptionsColumn.ReadOnly = true;
            this.colCONEndereco.Visible = true;
            this.colCONEndereco.VisibleIndex = 3;
            this.colCONEndereco.Width = 205;
            // 
            // colCONBairro
            // 
            this.colCONBairro.Caption = "Bairro";
            this.colCONBairro.FieldName = "CONBairro";
            this.colCONBairro.Name = "colCONBairro";
            this.colCONBairro.OptionsColumn.ReadOnly = true;
            this.colCONBairro.Visible = true;
            this.colCONBairro.VisibleIndex = 4;
            this.colCONBairro.Width = 111;
            // 
            // colCIDNome
            // 
            this.colCIDNome.Caption = "Cidade";
            this.colCIDNome.FieldName = "CIDNome";
            this.colCIDNome.Name = "colCIDNome";
            this.colCIDNome.OptionsColumn.ReadOnly = true;
            this.colCIDNome.Width = 170;
            // 
            // colCONCep
            // 
            this.colCONCep.Caption = "CEP";
            this.colCONCep.FieldName = "CONCep";
            this.colCONCep.Name = "colCONCep";
            this.colCONCep.OptionsColumn.ReadOnly = true;
            // 
            // colCONStatus
            // 
            this.colCONStatus.Caption = "Status";
            this.colCONStatus.ColumnEdit = this.repositoryItemImageComboBox1;
            this.colCONStatus.FieldName = "CONStatus";
            this.colCONStatus.Name = "colCONStatus";
            this.colCONStatus.OptionsColumn.AllowEdit = false;
            this.colCONStatus.OptionsColumn.ReadOnly = true;
            // 
            // repositoryItemImageComboBox1
            // 
            this.repositoryItemImageComboBox1.AutoHeight = false;
            this.repositoryItemImageComboBox1.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemImageComboBox1.Name = "repositoryItemImageComboBox1";
            // 
            // barButtonItem1
            // 
            this.barButtonItem1.Caption = "Detalhes";
            this.barButtonItem1.Id = 19;
            this.barButtonItem1.Name = "barButtonItem1";
            this.barButtonItem1.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.barButtonItem1_ItemClick);
            // 
            // colCON_EMP
            // 
            this.colCON_EMP.Caption = "CON_EMP";
            this.colCON_EMP.FieldName = "CON_EMP";
            this.colCON_EMP.Name = "colCON_EMP";
            this.colCON_EMP.OptionsColumn.AllowEdit = false;
            this.colCON_EMP.OptionsColumn.ReadOnly = true;
            // 
            // colNomeSindico
            // 
            this.colNomeSindico.Caption = "S�ndico";
            this.colNomeSindico.ColumnEdit = this.LookUpEdit1Sindico;
            this.colNomeSindico.FieldName = "CopiaCON";
            this.colNomeSindico.FilterMode = DevExpress.XtraGrid.ColumnFilterMode.DisplayText;
            this.colNomeSindico.Name = "colNomeSindico";
            this.colNomeSindico.OptionsColumn.ReadOnly = true;
            this.colNomeSindico.OptionsFilter.AutoFilterCondition = DevExpress.XtraGrid.Columns.AutoFilterCondition.Like;
            this.colNomeSindico.Visible = true;
            this.colNomeSindico.VisibleIndex = 5;
            this.colNomeSindico.Width = 156;
            // 
            // LookUpEdit1Sindico
            // 
            this.LookUpEdit1Sindico.AutoHeight = false;
            this.LookUpEdit1Sindico.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LookUpEdit1Sindico.DataSource = this.sindicosBindingSource;
            this.LookUpEdit1Sindico.DisplayMember = "PESNome";
            this.LookUpEdit1Sindico.Name = "LookUpEdit1Sindico";
            this.LookUpEdit1Sindico.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.Standard;
            this.LookUpEdit1Sindico.ValueMember = "CDR_CON";
            // 
            // sindicosBindingSource
            // 
            this.sindicosBindingSource.DataMember = "Sindicos";
            this.sindicosBindingSource.DataSource = typeof(FrameworkProc.datasets.dCondominiosAtivos);
            // 
            // colTelSindico
            // 
            this.colTelSindico.Caption = "Tel S�ndico";
            this.colTelSindico.ColumnEdit = this.LookUpEditTelSindico;
            this.colTelSindico.FieldName = "CopiaCON1";
            this.colTelSindico.FilterMode = DevExpress.XtraGrid.ColumnFilterMode.DisplayText;
            this.colTelSindico.Name = "colTelSindico";
            this.colTelSindico.OptionsColumn.ReadOnly = true;
            this.colTelSindico.OptionsFilter.AutoFilterCondition = DevExpress.XtraGrid.Columns.AutoFilterCondition.Like;
            this.colTelSindico.SortMode = DevExpress.XtraGrid.ColumnSortMode.DisplayText;
            this.colTelSindico.Visible = true;
            this.colTelSindico.VisibleIndex = 6;
            this.colTelSindico.Width = 106;
            // 
            // LookUpEditTelSindico
            // 
            this.LookUpEditTelSindico.AutoHeight = false;
            this.LookUpEditTelSindico.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LookUpEditTelSindico.DataSource = this.sindicosBindingSource;
            this.LookUpEditTelSindico.DisplayMember = "PESFone1";
            this.LookUpEditTelSindico.Name = "LookUpEditTelSindico";
            this.LookUpEditTelSindico.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.Standard;
            this.LookUpEditTelSindico.ValueMember = "CDR_CON";
            // 
            // gridColumn1
            // 
            this.gridColumn1.ColumnEdit = this.botes;
            this.gridColumn1.Fixed = DevExpress.XtraGrid.Columns.FixedStyle.Right;
            this.gridColumn1.Name = "gridColumn1";
            this.gridColumn1.ShowButtonMode = DevExpress.XtraGrid.Views.Base.ShowButtonModeEnum.ShowAlways;
            this.gridColumn1.Visible = true;
            this.gridColumn1.VisibleIndex = 14;
            this.gridColumn1.Width = 25;
            // 
            // botes
            // 
            this.botes.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.botes.Name = "botes";
            this.botes.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.HideTextEditor;
            this.botes.ButtonClick += new DevExpress.XtraEditors.Controls.ButtonPressedEventHandler(this.botes_ButtonClick);
            // 
            // colCIDUf
            // 
            this.colCIDUf.Caption = "UF";
            this.colCIDUf.FieldName = "CIDUf";
            this.colCIDUf.Name = "colCIDUf";
            this.colCIDUf.OptionsColumn.ReadOnly = true;
            // 
            // colCONCnpj
            // 
            this.colCONCnpj.Caption = "CNPJ";
            this.colCONCnpj.FieldName = "CONCnpj";
            this.colCONCnpj.Name = "colCONCnpj";
            this.colCONCnpj.OptionsColumn.ReadOnly = true;
            // 
            // colCONTotalBloco
            // 
            this.colCONTotalBloco.Caption = "N�mero de Blocos";
            this.colCONTotalBloco.FieldName = "CONTotalBloco";
            this.colCONTotalBloco.Name = "colCONTotalBloco";
            this.colCONTotalBloco.OptionsColumn.ReadOnly = true;
            // 
            // colCONTotalApartamento
            // 
            this.colCONTotalApartamento.Caption = "N�mero de apartamentos";
            this.colCONTotalApartamento.FieldName = "CONTotalApartamento";
            this.colCONTotalApartamento.Name = "colCONTotalApartamento";
            this.colCONTotalApartamento.OptionsColumn.ReadOnly = true;
            // 
            // colCON_BCO
            // 
            this.colCON_BCO.Caption = "Banco";
            this.colCON_BCO.FieldName = "CON_BCO";
            this.colCON_BCO.Name = "colCON_BCO";
            this.colCON_BCO.OptionsColumn.ReadOnly = true;
            // 
            // colCONAgencia
            // 
            this.colCONAgencia.Caption = "Ag�ncia";
            this.colCONAgencia.FieldName = "CONAgencia";
            this.colCONAgencia.Name = "colCONAgencia";
            this.colCONAgencia.OptionsColumn.ReadOnly = true;
            // 
            // colCONDigitoAgencia
            // 
            this.colCONDigitoAgencia.Caption = "D�gito agencia";
            this.colCONDigitoAgencia.FieldName = "CONDigitoAgencia";
            this.colCONDigitoAgencia.Name = "colCONDigitoAgencia";
            this.colCONDigitoAgencia.OptionsColumn.ReadOnly = true;
            // 
            // colCONConta
            // 
            this.colCONConta.Caption = "Conta";
            this.colCONConta.FieldName = "CONConta";
            this.colCONConta.Name = "colCONConta";
            this.colCONConta.OptionsColumn.ReadOnly = true;
            // 
            // colCONDigitoConta
            // 
            this.colCONDigitoConta.Caption = "D�gito Conta";
            this.colCONDigitoConta.FieldName = "CONDigitoConta";
            this.colCONDigitoConta.Name = "colCONDigitoConta";
            this.colCONDigitoConta.OptionsColumn.ReadOnly = true;
            // 
            // colCONDataInclusao
            // 
            this.colCONDataInclusao.Caption = "Data de Inclus�o";
            this.colCONDataInclusao.FieldName = "CONDataInclusao";
            this.colCONDataInclusao.Name = "colCONDataInclusao";
            this.colCONDataInclusao.OptionsColumn.ReadOnly = true;
            // 
            // barButtonItem3
            // 
            this.barButtonItem3.ButtonStyle = DevExpress.XtraBars.BarButtonStyle.Check;
            this.barButtonItem3.Caption = "Mostrar Todos";
            this.barButtonItem3.Id = 21;
            this.barButtonItem3.Name = "barButtonItem3";
            this.barButtonItem3.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.barButtonItem3_ItemClick);
            // 
            // colCONMalote
            // 
            this.colCONMalote.Caption = "Malote";
            this.colCONMalote.ColumnEdit = this.LookUpEditMalote;
            this.colCONMalote.FieldName = "CONMalote";
            this.colCONMalote.Name = "colCONMalote";
            this.colCONMalote.OptionsColumn.AllowEdit = false;
            this.colCONMalote.OptionsColumn.ReadOnly = true;
            this.colCONMalote.Visible = true;
            this.colCONMalote.VisibleIndex = 7;
            // 
            // LookUpEditMalote
            // 
            this.LookUpEditMalote.AutoHeight = false;
            this.LookUpEditMalote.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LookUpEditMalote.DataSource = this.bindingSourcemalotes;
            this.LookUpEditMalote.DisplayMember = "Descricao";
            this.LookUpEditMalote.Name = "LookUpEditMalote";
            this.LookUpEditMalote.NullText = "-";
            this.LookUpEditMalote.ValueMember = "Codigo";
            // 
            // bindingSourcemalotes
            // 
            this.bindingSourcemalotes.DataMember = "TiposMalote";
            this.bindingSourcemalotes.DataSource = typeof(Framework.datasets.dMALote);
            // 
            // colCONCobranca_USU
            // 
            this.colCONCobranca_USU.Caption = "Cobran�a";
            this.colCONCobranca_USU.ColumnEdit = this.LookUpEditUSU;
            this.colCONCobranca_USU.FieldName = "CONCobranca_USU";
            this.colCONCobranca_USU.Name = "colCONCobranca_USU";
            this.colCONCobranca_USU.OptionsColumn.AllowEdit = false;
            this.colCONCobranca_USU.OptionsColumn.ReadOnly = true;
            this.colCONCobranca_USU.Visible = true;
            this.colCONCobranca_USU.VisibleIndex = 8;
            // 
            // LookUpEditUSU
            // 
            this.LookUpEditUSU.AutoHeight = false;
            this.LookUpEditUSU.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LookUpEditUSU.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("USUNome", "Nome", 53, DevExpress.Utils.FormatType.None, "", true, DevExpress.Utils.HorzAlignment.Near)});
            this.LookUpEditUSU.DataSource = this.bindingSourceUSU;
            this.LookUpEditUSU.DisplayMember = "USUNome";
            this.LookUpEditUSU.Name = "LookUpEditUSU";
            this.LookUpEditUSU.NullText = "-";
            this.LookUpEditUSU.ShowHeader = false;
            this.LookUpEditUSU.ValueMember = "USU";
            // 
            // bindingSourceUSU
            // 
            this.bindingSourceUSU.DataMember = "USUarios";
            this.bindingSourceUSU.DataSource = typeof(FrameworkProc.datasets.dUSUarios);
            // 
            // colCONAuditor1_USU
            // 
            this.colCONAuditor1_USU.Caption = "Gerente";
            this.colCONAuditor1_USU.ColumnEdit = this.LookUpEditUSU;
            this.colCONAuditor1_USU.FieldName = "CONAuditor1_USU";
            this.colCONAuditor1_USU.Name = "colCONAuditor1_USU";
            this.colCONAuditor1_USU.OptionsColumn.AllowEdit = false;
            this.colCONAuditor1_USU.OptionsColumn.ReadOnly = true;
            this.colCONAuditor1_USU.Visible = true;
            this.colCONAuditor1_USU.VisibleIndex = 9;
            // 
            // colCONEscrit_FRN
            // 
            this.colCONEscrit_FRN.Caption = "Jur�dico";
            this.colCONEscrit_FRN.ColumnEdit = this.LookUpEditADV;
            this.colCONEscrit_FRN.FieldName = "CONEscrit_FRN";
            this.colCONEscrit_FRN.Name = "colCONEscrit_FRN";
            this.colCONEscrit_FRN.OptionsColumn.AllowEdit = false;
            this.colCONEscrit_FRN.OptionsColumn.ReadOnly = true;
            this.colCONEscrit_FRN.Visible = true;
            this.colCONEscrit_FRN.VisibleIndex = 11;
            this.colCONEscrit_FRN.Width = 200;
            // 
            // LookUpEditADV
            // 
            this.LookUpEditADV.AutoHeight = false;
            this.LookUpEditADV.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LookUpEditADV.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("FRNNome", "FRNNome", 53, DevExpress.Utils.FormatType.None, "", true, DevExpress.Utils.HorzAlignment.Near)});
            this.LookUpEditADV.DataSource = this.bindingSourceEscADVOGADOS;
            this.LookUpEditADV.DisplayMember = "FRNNome";
            this.LookUpEditADV.Name = "LookUpEditADV";
            this.LookUpEditADV.NullText = "-";
            this.LookUpEditADV.PopupFilterMode = DevExpress.XtraEditors.PopupFilterMode.Contains;
            this.LookUpEditADV.ShowHeader = false;
            this.LookUpEditADV.ValueMember = "FRN";
            // 
            // bindingSourceEscADVOGADOS
            // 
            this.bindingSourceEscADVOGADOS.DataMember = "FRNLookup";
            this.bindingSourceEscADVOGADOS.DataSource = typeof(CadastrosProc.Fornecedores.dFornecedoresLookup);
            // 
            // colCONProcuracao
            // 
            this.colCONProcuracao.Caption = "Procura��o";
            this.colCONProcuracao.FieldName = "CONProcuracao";
            this.colCONProcuracao.Name = "colCONProcuracao";
            this.colCONProcuracao.OptionsColumn.AllowEdit = false;
            this.colCONProcuracao.OptionsColumn.ReadOnly = true;
            this.colCONProcuracao.Visible = true;
            this.colCONProcuracao.VisibleIndex = 13;
            this.colCONProcuracao.Width = 67;
            // 
            // barButtonItem4
            // 
            this.barButtonItem4.Border = DevExpress.XtraEditors.Controls.BorderStyles.Style3D;
            this.barButtonItem4.Caption = "Desativar";
            this.barButtonItem4.Id = 22;
            this.barButtonItem4.ImageOptions.Image = global::Cadastros.Properties.Resources.deslP1;
            this.barButtonItem4.ItemAppearance.Normal.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
            this.barButtonItem4.ItemAppearance.Normal.ForeColor = System.Drawing.Color.Blue;
            this.barButtonItem4.ItemAppearance.Normal.Options.UseFont = true;
            this.barButtonItem4.ItemAppearance.Normal.Options.UseForeColor = true;
            this.barButtonItem4.Name = "barButtonItem4";
            this.barButtonItem4.PaintStyle = DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph;
            this.barButtonItem4.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.barButtonItem4_ItemClick);
            // 
            // panelControl1
            // 
            this.panelControl1.Controls.Add(this.lookupGerente1);
            this.panelControl1.Dock = System.Windows.Forms.DockStyle.Top;
            this.panelControl1.Location = new System.Drawing.Point(0, 0);
            this.panelControl1.Name = "panelControl1";
            this.panelControl1.Size = new System.Drawing.Size(1557, 30);
            this.panelControl1.TabIndex = 6;
            // 
            // lookupGerente1
            // 
            this.lookupGerente1.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.lookupGerente1.Appearance.Options.UseBackColor = true;
            this.lookupGerente1.CaixaAltaGeral = true;
            this.lookupGerente1.Doc = System.Windows.Forms.DockStyle.None;
            this.lookupGerente1.Estado = CompontesBasicos.EstadosDosComponentes.Registrado;
            this.lookupGerente1.Location = new System.Drawing.Point(5, 5);
            this.lookupGerente1.LookAndFeel.UseDefaultLookAndFeel = false;
            this.lookupGerente1.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.lookupGerente1.Name = "lookupGerente1";
            this.lookupGerente1.Size = new System.Drawing.Size(290, 20);
            this.lookupGerente1.somenteleitura = false;
            this.lookupGerente1.TabIndex = 0;
            this.lookupGerente1.Tipo = Framework.Lookup.LookupGerente.TipoUsuario.gerente;
            this.lookupGerente1.Titulo = null;
            this.lookupGerente1.USUSel = -1;
            this.lookupGerente1.alterado += new System.EventHandler(this.lookupGerente1_alterado);
            // 
            // colCONCodigoFolha1
            // 
            this.colCONCodigoFolha1.Caption = "C�digo";
            this.colCONCodigoFolha1.FieldName = "CONCodigoFolha1";
            this.colCONCodigoFolha1.Fixed = DevExpress.XtraGrid.Columns.FixedStyle.Left;
            this.colCONCodigoFolha1.Name = "colCONCodigoFolha1";
            this.colCONCodigoFolha1.OptionsColumn.ReadOnly = true;
            this.colCONCodigoFolha1.Visible = true;
            this.colCONCodigoFolha1.VisibleIndex = 0;
            this.colCONCodigoFolha1.Width = 53;
            // 
            // colCONCodigoCNR
            // 
            this.colCONCodigoCNR.Caption = "CNR";
            this.colCONCodigoCNR.FieldName = "CONCodigoCNR";
            this.colCONCodigoCNR.Name = "colCONCodigoCNR";
            this.colCONCodigoCNR.OptionsColumn.ReadOnly = true;
            // 
            // colCON
            // 
            this.colCON.Caption = "CON";
            this.colCON.FieldName = "CON";
            this.colCON.Name = "colCON";
            this.colCON.OptionsColumn.ReadOnly = true;
            // 
            // colCON_PSC
            // 
            this.colCON_PSC.Caption = "Seguro";
            this.colCON_PSC.ColumnEdit = this.LookUpEditPSC;
            this.colCON_PSC.FieldName = "CON_PSC";
            this.colCON_PSC.Name = "colCON_PSC";
            this.colCON_PSC.OptionsColumn.AllowEdit = false;
            this.colCON_PSC.OptionsColumn.ReadOnly = true;
            // 
            // LookUpEditPSC
            // 
            this.LookUpEditPSC.AutoHeight = false;
            this.LookUpEditPSC.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LookUpEditPSC.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("PSCDescricao", "PSC Descricao", 5, DevExpress.Utils.FormatType.None, "", true, DevExpress.Utils.HorzAlignment.Near),
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("PSCValor", "PSC Valor", 5, DevExpress.Utils.FormatType.Numeric, "n2", true, DevExpress.Utils.HorzAlignment.Far)});
            this.LookUpEditPSC.DataSource = this.BindingSourcedPlanosSeguroConteudo;
            this.LookUpEditPSC.DisplayMember = "PSCDescricao";
            this.LookUpEditPSC.Name = "LookUpEditPSC";
            this.LookUpEditPSC.NullText = "N�o";
            this.LookUpEditPSC.ShowHeader = false;
            this.LookUpEditPSC.ValueMember = "PSC";
            // 
            // BindingSourcedPlanosSeguroConteudo
            // 
            this.BindingSourcedPlanosSeguroConteudo.DataMember = "PlanosSeguroConteudo";
            this.BindingSourcedPlanosSeguroConteudo.DataSource = typeof(Framework.datasets.dPlanosSeguroConteudo);
            // 
            // colCONBalancete_USU
            // 
            this.colCONBalancete_USU.Caption = "Rep. Balancete";
            this.colCONBalancete_USU.ColumnEdit = this.LookUpEditUSU;
            this.colCONBalancete_USU.FieldName = "CONBalancete_USU";
            this.colCONBalancete_USU.Name = "colCONBalancete_USU";
            this.colCONBalancete_USU.OptionsColumn.AllowEdit = false;
            this.colCONBalancete_USU.OptionsColumn.ReadOnly = true;
            // 
            // colCONPagEletronico
            // 
            this.colCONPagEletronico.Caption = "Sal�rio Eletr�nico";
            this.colCONPagEletronico.FieldName = "CONPagEletronico";
            this.colCONPagEletronico.Name = "colCONPagEletronico";
            this.colCONPagEletronico.OptionsColumn.AllowEdit = false;
            this.colCONPagEletronico.OptionsColumn.ReadOnly = true;
            this.colCONPagEletronico.Width = 100;
            // 
            // colCONAuditor2_USU
            // 
            this.colCONAuditor2_USU.Caption = "Assistente";
            this.colCONAuditor2_USU.ColumnEdit = this.LookUpEditUSU;
            this.colCONAuditor2_USU.FieldName = "CONAuditor2_USU";
            this.colCONAuditor2_USU.Name = "colCONAuditor2_USU";
            this.colCONAuditor2_USU.OptionsColumn.AllowEdit = false;
            this.colCONAuditor2_USU.OptionsColumn.ReadOnly = true;
            this.colCONAuditor2_USU.Visible = true;
            this.colCONAuditor2_USU.VisibleIndex = 10;
            // 
            // colCONCodigoComunicacao
            // 
            this.colCONCodigoComunicacao.Caption = "Pag. Elet.";
            this.colCONCodigoComunicacao.ColumnEdit = this.repositoryItemCheckEdit1;
            this.colCONCodigoComunicacao.FieldName = "CONCodigoComunicacao";
            this.colCONCodigoComunicacao.Name = "colCONCodigoComunicacao";
            this.colCONCodigoComunicacao.Visible = true;
            this.colCONCodigoComunicacao.VisibleIndex = 12;
            this.colCONCodigoComunicacao.Width = 58;
            // 
            // repositoryItemCheckEdit1
            // 
            this.repositoryItemCheckEdit1.AutoHeight = false;
            this.repositoryItemCheckEdit1.Name = "repositoryItemCheckEdit1";
            this.repositoryItemCheckEdit1.ValueChecked = "154134";
            this.repositoryItemCheckEdit1.ValueUnchecked = "";
            // 
            // colCONCorretoraSeguros_FRN
            // 
            this.colCONCorretoraSeguros_FRN.Caption = "Corretora";
            this.colCONCorretoraSeguros_FRN.ColumnEdit = this.LookUpEditADV;
            this.colCONCorretoraSeguros_FRN.FieldName = "CONCorretoraSeguros_FRN";
            this.colCONCorretoraSeguros_FRN.Name = "colCONCorretoraSeguros_FRN";
            this.colCONCorretoraSeguros_FRN.Width = 200;
            // 
            // colCONSeguradora_FRN
            // 
            this.colCONSeguradora_FRN.Caption = "Seguradora";
            this.colCONSeguradora_FRN.ColumnEdit = this.LookUpEditADV;
            this.colCONSeguradora_FRN.FieldName = "CONSeguradora_FRN";
            this.colCONSeguradora_FRN.Name = "colCONSeguradora_FRN";
            this.colCONSeguradora_FRN.Width = 200;
            // 
            // cCondominiosGrade
            // 
            this.Autofill = false;
            this.BotaoExcluir = false;
            this.CampoTab = "CONCodigo";
            this.ComponenteCampos = typeof(Cadastros.Condominios.cCondominiosCampos2);
            this.Controls.Add(this.panelControl1);
            this.dataLayout = new System.DateTime(2014, 5, 22, 0, 0, 0, 0);
            this.EdicaoExterna = true;
            this.Funcionalidade = "CADASTRO";
            this.LookAndFeel.UseDefaultLookAndFeel = false;
            this.Name = "cCondominiosGrade";
            this.NivelLeitura = 3;
            this.PrefixoTab = "Cond";
            this.Size = new System.Drawing.Size(1557, 807);
            this.cargaFinal += new System.EventHandler(this.cCondominiosGrade_cargaFinal);
            this.Load += new System.EventHandler(this.cCondominiosGrade_Load);
            this.Controls.SetChildIndex(this.panelControl1, 0);
            this.Controls.SetChildIndex(this.GridControl_F, 0);
            ((System.ComponentModel.ISupportInitialize)(this.PopImprimir_F)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.BarManager_F)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.PopExportar_F)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.GridControl_F)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.GridView_F)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.BindingSource_F)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.StyleController_F)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemImageComboBox1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LookUpEdit1Sindico)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sindicosBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LookUpEditTelSindico)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.botes)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LookUpEditMalote)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.bindingSourcemalotes)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LookUpEditUSU)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.bindingSourceUSU)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LookUpEditADV)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.bindingSourceEscADVOGADOS)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl1)).EndInit();
            this.panelControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.LookUpEditPSC)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.BindingSourcedPlanosSeguroConteudo)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckEdit1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private DevExpress.XtraGrid.Columns.GridColumn colCONCodigo;
        private DevExpress.XtraGrid.Columns.GridColumn colCONNome;
        private DevExpress.XtraGrid.Columns.GridColumn colCONEndereco;
        private DevExpress.XtraGrid.Columns.GridColumn colCONBairro;
        private DevExpress.XtraGrid.Columns.GridColumn colCIDNome;
        private DevExpress.XtraGrid.Columns.GridColumn colCONCep;
        private DevExpress.XtraBars.BarButtonItem barButtonItem1;
        private DevExpress.XtraGrid.Columns.GridColumn colCONStatus;
        private DevExpress.XtraGrid.Columns.GridColumn colCON_EMP;
        private DevExpress.XtraGrid.Columns.GridColumn colNomeSindico;
        private DevExpress.XtraGrid.Columns.GridColumn colTelSindico;
        private DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit LookUpEdit1Sindico;
        private System.Windows.Forms.BindingSource sindicosBindingSource;
        private DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit LookUpEditTelSindico;
        private DevExpress.XtraEditors.Repository.RepositoryItemButtonEdit botes;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn1;
        private DevExpress.XtraGrid.Columns.GridColumn colCIDUf;
        private DevExpress.XtraGrid.Columns.GridColumn colCONCnpj;
        private DevExpress.XtraGrid.Columns.GridColumn colCONTotalBloco;
        private DevExpress.XtraGrid.Columns.GridColumn colCONTotalApartamento;
        private DevExpress.XtraGrid.Columns.GridColumn colCON_BCO;
        private DevExpress.XtraGrid.Columns.GridColumn colCONAgencia;
        private DevExpress.XtraGrid.Columns.GridColumn colCONDigitoAgencia;
        private DevExpress.XtraGrid.Columns.GridColumn colCONConta;
        private DevExpress.XtraGrid.Columns.GridColumn colCONDigitoConta;
        private DevExpress.XtraGrid.Columns.GridColumn colCONDataInclusao;
        private DevExpress.XtraBars.BarButtonItem barButtonItem3;
        private DevExpress.XtraGrid.Columns.GridColumn colCONMalote;
        private DevExpress.XtraGrid.Columns.GridColumn colCONCobranca_USU;
        private DevExpress.XtraGrid.Columns.GridColumn colCONAuditor1_USU;
        private DevExpress.XtraGrid.Columns.GridColumn colCONEscrit_FRN;
        private System.Windows.Forms.BindingSource bindingSourceEscADVOGADOS;
        private System.Windows.Forms.BindingSource bindingSourceUSU;
        private DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit LookUpEditUSU;
        private DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit LookUpEditADV;
        private System.Windows.Forms.BindingSource bindingSourcemalotes;
        private DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit LookUpEditMalote;
        private DevExpress.XtraGrid.Columns.GridColumn colCONProcuracao;
        private DevExpress.XtraEditors.Repository.RepositoryItemImageComboBox repositoryItemImageComboBox1;
        private DevExpress.XtraBars.BarButtonItem barButtonItem4;
        private DevExpress.XtraEditors.PanelControl panelControl1;
        private Framework.Lookup.LookupGerente lookupGerente1;
        private DevExpress.XtraGrid.Columns.GridColumn colCONCodigoFolha1;
        private DevExpress.XtraGrid.Columns.GridColumn colCONCodigoCNR;
        private DevExpress.XtraGrid.Columns.GridColumn colCON;
        private DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit LookUpEditPSC;
        private System.Windows.Forms.BindingSource BindingSourcedPlanosSeguroConteudo;
        private DevExpress.XtraGrid.Columns.GridColumn colCON_PSC;
        private DevExpress.XtraGrid.Columns.GridColumn colCONBalancete_USU;
        private DevExpress.XtraGrid.Columns.GridColumn colCONPagEletronico;
        private DevExpress.XtraGrid.Columns.GridColumn colCONAuditor2_USU;
        private DevExpress.XtraGrid.Columns.GridColumn colCONCodigoComunicacao;
        private DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit repositoryItemCheckEdit1;
        private DevExpress.XtraGrid.Columns.GridColumn colCONCorretoraSeguros_FRN;
        private DevExpress.XtraGrid.Columns.GridColumn colCONSeguradora_FRN;
    }
}
