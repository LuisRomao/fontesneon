using CadastrosProc.Condominios;

namespace Cadastros.Condominios
{
    partial class cVencimento
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling1 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(cVencimento));
            this.layoutControl1 = new DevExpress.XtraLayout.LayoutControl();
            this.txtRegra = new DevExpress.XtraEditors.TextEdit();
            this.rdgOrdem = new DevExpress.XtraEditors.RadioGroup();
            this.rdgTipo = new DevExpress.XtraEditors.RadioGroup();
            this.speDia = new DevExpress.XtraEditors.SpinEdit();
            this.layoutControlGroup1 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlGroup2 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlItem2 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlGroup3 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlItem1 = new DevExpress.XtraLayout.LayoutControlItem();
            this.emptySpaceItem1 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.layoutControlItem4 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlGroup4 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlItem3 = new DevExpress.XtraLayout.LayoutControlItem();
            this.dCondominiosCampos = new CadastrosProc.Condominios.dCondominiosCampos();
            this.cONDOMINIOSTableAdapter = new CadastrosProc.Condominios.dCondominiosCamposTableAdapters.CONDOMINIOSTableAdapter();
            ((System.ComponentModel.ISupportInitialize)(this.BarManager_F)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.BarAndDockingController_F)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.BindingSource_F)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.StyleController_F)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl1)).BeginInit();
            this.layoutControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtRegra.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.rdgOrdem.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.rdgTipo.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.speDia.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dCondominiosCampos)).BeginInit();
            this.SuspendLayout();
            // 
            // barStatus_F
            // 
            this.barStatus_F.OptionsBar.AllowQuickCustomization = false;
            this.barStatus_F.OptionsBar.DisableClose = true;
            this.barStatus_F.OptionsBar.DisableCustomization = true;
            this.barStatus_F.OptionsBar.DrawDragBorder = false;
            this.barStatus_F.OptionsBar.DrawSizeGrip = true;
            this.barStatus_F.OptionsBar.UseWholeRow = true;
            this.barStatus_F.Visible = false;
            // 
            // barComponente_F
            // 
            this.barComponente_F.OptionsBar.AllowQuickCustomization = false;
            this.barComponente_F.OptionsBar.DisableClose = true;
            this.barComponente_F.OptionsBar.DisableCustomization = true;
            this.barComponente_F.OptionsBar.DrawDragBorder = false;
            this.barComponente_F.OptionsBar.UseWholeRow = true;
            // 
            // btnConfirmar_F
            // 
            this.btnConfirmar_F.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.btnConfirmar_F_ItemClick);
            // 
            // btnCancelar_F
            // 
            this.btnCancelar_F.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.btnCancelar_F_ItemClick);
            // 
            // btnFechar_F
            // 
            this.btnFechar_F.Visibility = DevExpress.XtraBars.BarItemVisibility.Never;
            // 
            // BarAndDockingController_F
            // 
            this.BarAndDockingController_F.PropertiesBar.AllowLinkLighting = false;
            // 
            // BindingSource_F
            // 
            this.BindingSource_F.DataMember = "CONDOMINIOS";
            this.BindingSource_F.DataSource = this.dCondominiosCampos;
            // 
            // ToolTipController_F
            // 
            this.ToolTipController_F.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(222)))), ((int)(((byte)(237)))), ((int)(((byte)(250)))));
            this.ToolTipController_F.Appearance.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(20)))), ((int)(((byte)(69)))), ((int)(((byte)(141)))));
            this.ToolTipController_F.Appearance.Options.UseBackColor = true;
            this.ToolTipController_F.Appearance.Options.UseFont = true;
            this.ToolTipController_F.Appearance.Options.UseForeColor = true;
            this.ToolTipController_F.Appearance.Options.UseTextOptions = true;
            this.ToolTipController_F.Appearance.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            // 
            // StyleController_F
            // 
            this.StyleController_F.AppearanceDisabled.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.StyleController_F.AppearanceDisabled.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.StyleController_F.AppearanceDisabled.Options.UseBackColor = true;
            this.StyleController_F.AppearanceDisabled.Options.UseForeColor = true;
            this.StyleController_F.AppearanceFocused.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(253)))), ((int)(((byte)(253)))), ((int)(((byte)(181)))));
            this.StyleController_F.AppearanceFocused.Options.UseBackColor = true;
          
            // 
            // layoutControl1
            // 
            this.layoutControl1.Controls.Add(this.txtRegra);
            this.layoutControl1.Controls.Add(this.rdgOrdem);
            this.layoutControl1.Controls.Add(this.rdgTipo);
            this.layoutControl1.Controls.Add(this.speDia);
            this.layoutControl1.Cursor = System.Windows.Forms.Cursors.Arrow;
            this.layoutControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.layoutControl1.Location = new System.Drawing.Point(0, 30);
            this.layoutControl1.Name = "layoutControl1";
            this.layoutControl1.Root = this.layoutControlGroup1;
            this.layoutControl1.Size = new System.Drawing.Size(303, 185);
            this.layoutControl1.TabIndex = 4;
            this.layoutControl1.Text = "layoutControl1";
            // 
            // txtRegra
            // 
            this.txtRegra.EditValue = "";
            this.txtRegra.Enabled = false;
            this.txtRegra.Location = new System.Drawing.Point(11, 157);
            this.txtRegra.Name = "txtRegra";
            this.txtRegra.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 6.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtRegra.Properties.Appearance.Options.UseFont = true;
            this.txtRegra.Properties.NullText = "kdn; sa dj;klasjk";
            
            this.txtRegra.Size = new System.Drawing.Size(281, 17);
            
            this.txtRegra.StyleController = this.layoutControl1;
            this.txtRegra.TabIndex = 6;
            // 
            // rdgOrdem
            // 
            this.rdgOrdem.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.BindingSource_F, "CONReferenciaVencimento", true));
            this.rdgOrdem.Location = new System.Drawing.Point(14, 90);
            this.rdgOrdem.Name = "rdgOrdem";
            this.rdgOrdem.Properties.Items.AddRange(new DevExpress.XtraEditors.Controls.RadioGroupItem[] {
            new DevExpress.XtraEditors.Controls.RadioGroupItem("I", "Do in�cio para o fim do m�s."),
            new DevExpress.XtraEditors.Controls.RadioGroupItem("F", "Do fim para o in�cio do m�s.")});
            this.rdgOrdem.Size = new System.Drawing.Size(275, 54);
            this.rdgOrdem.StyleController = this.layoutControl1;
            this.rdgOrdem.TabIndex = 5;
            this.rdgOrdem.SelectedIndexChanged += new System.EventHandler(this.rdgOrdem_SelectedIndexChanged);
            // 
            // rdgTipo
            // 
            this.rdgTipo.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.BindingSource_F, "CONTipoVencimento", true));
            this.rdgTipo.Location = new System.Drawing.Point(113, 34);
            this.rdgTipo.Name = "rdgTipo";
            this.rdgTipo.Properties.Columns = 2;
            this.rdgTipo.Properties.Items.AddRange(new DevExpress.XtraEditors.Controls.RadioGroupItem[] {
            new DevExpress.XtraEditors.Controls.RadioGroupItem("C", "Dia Corrido"),
            new DevExpress.XtraEditors.Controls.RadioGroupItem("U", "Dia �til")});
            this.rdgTipo.Size = new System.Drawing.Size(176, 20);
            this.rdgTipo.StyleController = this.layoutControl1;
            this.rdgTipo.TabIndex = 4;
            this.rdgTipo.SelectedIndexChanged += new System.EventHandler(this.rdgTipo_SelectedIndexChanged);
            // 
            // speDia
            // 
            this.speDia.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.BindingSource_F, "CONDiaVencimento", true));
            this.speDia.EditValue = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.speDia.Location = new System.Drawing.Point(14, 34);
            this.speDia.Name = "speDia";
            this.speDia.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.speDia.Properties.IsFloatValue = false;
            this.speDia.Properties.Mask.EditMask = "N00";
            this.speDia.Properties.MaxValue = new decimal(new int[] {
            31,
            0,
            0,
            0});
            this.speDia.Properties.MinValue = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.speDia.Size = new System.Drawing.Size(52, 20);
            this.speDia.StyleController = this.layoutControl1;
            this.speDia.TabIndex = 3;
            this.speDia.TextChanged += new System.EventHandler(this.speDia_TextChanged);
            // 
            // layoutControlGroup1
            // 
            this.layoutControlGroup1.CustomizationFormText = "layoutControlGroup1";
            this.layoutControlGroup1.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlGroup2,
            this.layoutControlGroup3,
            this.emptySpaceItem1,
            this.layoutControlItem4,
            this.layoutControlGroup4});
            this.layoutControlGroup1.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroup1.Name = "layoutControlGroup1";
            this.layoutControlGroup1.Padding = new DevExpress.XtraLayout.Utils.Padding(0, 0, 0, 0);
            this.layoutControlGroup1.Size = new System.Drawing.Size(303, 185);
            this.layoutControlGroup1.Spacing = new DevExpress.XtraLayout.Utils.Padding(5, 5, 5, 5);
            this.layoutControlGroup1.Text = "layoutControlGroup1";
            this.layoutControlGroup1.TextVisible = false;
            // 
            // layoutControlGroup2
            // 
            this.layoutControlGroup2.CustomizationFormText = ":: Tipo ::";
            this.layoutControlGroup2.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItem2});
            this.layoutControlGroup2.Location = new System.Drawing.Point(99, 0);
            this.layoutControlGroup2.Name = "layoutControlGroup2";
            this.layoutControlGroup2.Padding = new DevExpress.XtraLayout.Utils.Padding(0, 0, 0, 0);
            this.layoutControlGroup2.Size = new System.Drawing.Size(192, 56);
            this.layoutControlGroup2.Text = ":: Tipo ::";
            // 
            // layoutControlItem2
            // 
            this.layoutControlItem2.Control = this.rdgTipo;
            this.layoutControlItem2.CustomizationFormText = "layoutControlItem2";
            this.layoutControlItem2.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItem2.MinSize = new System.Drawing.Size(20, 30);
            this.layoutControlItem2.Name = "layoutControlItem2";
            this.layoutControlItem2.Padding = new DevExpress.XtraLayout.Utils.Padding(5, 5, 5, 5);
            this.layoutControlItem2.Size = new System.Drawing.Size(186, 30);
            this.layoutControlItem2.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem2.Text = "layoutControlItem2";
            this.layoutControlItem2.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem2.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem2.TextToControlDistance = 0;
            this.layoutControlItem2.TextVisible = false;
            // 
            // layoutControlGroup3
            // 
            this.layoutControlGroup3.CustomizationFormText = ":: Dia ::";
            this.layoutControlGroup3.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItem1});
            this.layoutControlGroup3.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroup3.Name = "layoutControlGroup3";
            this.layoutControlGroup3.Padding = new DevExpress.XtraLayout.Utils.Padding(0, 0, 0, 0);
            this.layoutControlGroup3.Size = new System.Drawing.Size(68, 56);
            this.layoutControlGroup3.Text = ":: Dia ::";
            // 
            // layoutControlItem1
            // 
            this.layoutControlItem1.Control = this.speDia;
            this.layoutControlItem1.CustomizationFormText = "layoutControlItem1";
            this.layoutControlItem1.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItem1.Name = "layoutControlItem1";
            this.layoutControlItem1.Padding = new DevExpress.XtraLayout.Utils.Padding(5, 5, 5, 5);
            this.layoutControlItem1.Size = new System.Drawing.Size(62, 30);
            this.layoutControlItem1.Text = "layoutControlItem1";
            this.layoutControlItem1.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem1.TextToControlDistance = 0;
            this.layoutControlItem1.TextVisible = false;
            // 
            // emptySpaceItem1
            // 
            this.emptySpaceItem1.CustomizationFormText = "emptySpaceItem1";
            this.emptySpaceItem1.Location = new System.Drawing.Point(68, 0);
            this.emptySpaceItem1.Name = "emptySpaceItem1";
            this.emptySpaceItem1.Padding = new DevExpress.XtraLayout.Utils.Padding(5, 5, 5, 5);
            this.emptySpaceItem1.Size = new System.Drawing.Size(31, 56);
            this.emptySpaceItem1.Text = "emptySpaceItem1";
            this.emptySpaceItem1.TextSize = new System.Drawing.Size(0, 0);
            // 
            // layoutControlItem4
            // 
            this.layoutControlItem4.Control = this.txtRegra;
            this.layoutControlItem4.CustomizationFormText = "layoutControlItem4";
            this.layoutControlItem4.Location = new System.Drawing.Point(0, 146);
            this.layoutControlItem4.Name = "layoutControlItem4";
            this.layoutControlItem4.Padding = new DevExpress.XtraLayout.Utils.Padding(5, 5, 5, 5);
            this.layoutControlItem4.Size = new System.Drawing.Size(291, 27);
            this.layoutControlItem4.Text = "layoutControlItem4";
            this.layoutControlItem4.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem4.TextToControlDistance = 0;
            this.layoutControlItem4.TextVisible = false;
            // 
            // layoutControlGroup4
            // 
            this.layoutControlGroup4.AllowCustomizeChildren = false;
            this.layoutControlGroup4.CustomizationFormText = ":: Ordem de Contagem ::";
            this.layoutControlGroup4.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItem3});
            this.layoutControlGroup4.Location = new System.Drawing.Point(0, 56);
            this.layoutControlGroup4.Name = "layoutControlGroup4";
            this.layoutControlGroup4.Padding = new DevExpress.XtraLayout.Utils.Padding(0, 0, 0, 0);
            this.layoutControlGroup4.Size = new System.Drawing.Size(291, 90);
            this.layoutControlGroup4.Text = ":: Ordem de Contagem ::";
            // 
            // layoutControlItem3
            // 
            this.layoutControlItem3.Control = this.rdgOrdem;
            this.layoutControlItem3.CustomizationFormText = "layoutControlItem3";
            this.layoutControlItem3.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItem3.MinSize = new System.Drawing.Size(20, 20);
            this.layoutControlItem3.Name = "layoutControlItem3";
            this.layoutControlItem3.Padding = new DevExpress.XtraLayout.Utils.Padding(5, 5, 5, 5);
            this.layoutControlItem3.Size = new System.Drawing.Size(285, 64);
            this.layoutControlItem3.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem3.Text = "layoutControlItem3";
            this.layoutControlItem3.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem3.TextToControlDistance = 0;
            this.layoutControlItem3.TextVisible = false;
            // 
            // dCondominiosCampos
            // 
            this.dCondominiosCampos.DataSetName = "dCondominiosCampos";
            this.dCondominiosCampos.EnforceConstraints = false;
            this.dCondominiosCampos.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // cONDOMINIOSTableAdapter
            // 
            this.cONDOMINIOSTableAdapter.ClearBeforeFill = true;
            this.cONDOMINIOSTableAdapter.GetNovosDados = ((System.Collections.ArrayList)(resources.GetObject("cONDOMINIOSTableAdapter.GetNovosDados")));
            this.cONDOMINIOSTableAdapter.TabelaDataTable = null;
            // 
            // cVencimento
            // 
            this.Controls.Add(this.layoutControl1);
 
            this.LookAndFeel.UseDefaultLookAndFeel = false;
            this.Name = "cVencimento";
            this.Size = new System.Drawing.Size(303, 243);
            this.Controls.SetChildIndex(this.layoutControl1, 0);
            ((System.ComponentModel.ISupportInitialize)(this.BarManager_F)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.BarAndDockingController_F)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.BindingSource_F)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.StyleController_F)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl1)).EndInit();
            this.layoutControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.txtRegra.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.rdgOrdem.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.rdgTipo.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.speDia.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dCondominiosCampos)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraLayout.LayoutControl layoutControl1;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup1;
        private DevExpress.XtraEditors.SpinEdit speDia;
        private DevExpress.XtraEditors.RadioGroup rdgOrdem;
        private DevExpress.XtraEditors.RadioGroup rdgTipo;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup2;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem2;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup3;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem1;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem1;
        private DevExpress.XtraEditors.TextEdit txtRegra;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem4;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup4;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem3;
        private dCondominiosCampos dCondominiosCampos;
        private CadastrosProc.Condominios.dCondominiosCamposTableAdapters.CONDOMINIOSTableAdapter cONDOMINIOSTableAdapter;
    }
}
