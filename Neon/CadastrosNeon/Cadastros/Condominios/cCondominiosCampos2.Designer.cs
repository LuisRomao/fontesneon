

namespace Cadastros.Condominios
{
    partial class cCondominiosCampos2
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(cCondominiosCampos2));
            System.Windows.Forms.Label cONCodigoLabel;
            System.Windows.Forms.Label cONNomeLabel;
            System.Windows.Forms.Label label2;
            DevExpress.XtraEditors.Controls.EditorButtonImageOptions editorButtonImageOptions1 = new DevExpress.XtraEditors.Controls.EditorButtonImageOptions();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject1 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject2 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject3 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject4 = new DevExpress.Utils.SerializableAppearanceObject();
            CompontesBasicos.Classes_de_interface.PaginaTabInfo paginaTabInfo1 = new CompontesBasicos.Classes_de_interface.PaginaTabInfo();
            CompontesBasicos.Classes_de_interface.PaginaTabInfo paginaTabInfo2 = new CompontesBasicos.Classes_de_interface.PaginaTabInfo();
            CompontesBasicos.Classes_de_interface.PaginaTabInfo paginaTabInfo3 = new CompontesBasicos.Classes_de_interface.PaginaTabInfo();
            CompontesBasicos.Classes_de_interface.PaginaTabInfo paginaTabInfo4 = new CompontesBasicos.Classes_de_interface.PaginaTabInfo();
            CompontesBasicos.Classes_de_interface.PaginaTabInfo paginaTabInfo5 = new CompontesBasicos.Classes_de_interface.PaginaTabInfo();
            CompontesBasicos.Classes_de_interface.PaginaTabInfo paginaTabInfo6 = new CompontesBasicos.Classes_de_interface.PaginaTabInfo();
            CompontesBasicos.Classes_de_interface.PaginaTabInfo paginaTabInfo7 = new CompontesBasicos.Classes_de_interface.PaginaTabInfo();
            CompontesBasicos.Classes_de_interface.PaginaTabInfo paginaTabInfo8 = new CompontesBasicos.Classes_de_interface.PaginaTabInfo();
            CompontesBasicos.Classes_de_interface.PaginaTabInfo paginaTabInfo9 = new CompontesBasicos.Classes_de_interface.PaginaTabInfo();
            CompontesBasicos.Classes_de_interface.PaginaTabInfo paginaTabInfo10 = new CompontesBasicos.Classes_de_interface.PaginaTabInfo();
            CompontesBasicos.Classes_de_interface.PaginaTabInfo paginaTabInfo11 = new CompontesBasicos.Classes_de_interface.PaginaTabInfo();
            CompontesBasicos.Classes_de_interface.PaginaTabInfo paginaTabInfo12 = new CompontesBasicos.Classes_de_interface.PaginaTabInfo();
            CompontesBasicos.Classes_de_interface.PaginaTabInfo paginaTabInfo13 = new CompontesBasicos.Classes_de_interface.PaginaTabInfo();
            CompontesBasicos.Classes_de_interface.PaginaTabInfo paginaTabInfo14 = new CompontesBasicos.Classes_de_interface.PaginaTabInfo();
            CompontesBasicos.Classes_de_interface.PaginaTabInfo paginaTabInfo15 = new CompontesBasicos.Classes_de_interface.PaginaTabInfo();
            CompontesBasicos.Classes_de_interface.PaginaTabInfo paginaTabInfo16 = new CompontesBasicos.Classes_de_interface.PaginaTabInfo();
            CompontesBasicos.Classes_de_interface.PaginaTabInfo paginaTabInfo17 = new CompontesBasicos.Classes_de_interface.PaginaTabInfo();
            CompontesBasicos.Classes_de_interface.PaginaTabInfo paginaTabInfo18 = new CompontesBasicos.Classes_de_interface.PaginaTabInfo();
            this.dCondominiosCampos = new CadastrosProc.Condominios.dCondominiosCampos();
            this.cONDOMINIOSBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.cONCodigoTextEdit = new DevExpress.XtraEditors.TextEdit();
            this.cONNomeTextEdit = new DevExpress.XtraEditors.TextEdit();
            this.eMPRESASBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.bLOCOSTableAdapter = new CadastrosProc.Condominios.dCondominiosCamposTableAdapters.BLOCOSTableAdapter();
            this.cIDADESBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.aPARTAMENTOSTableAdapter = new CadastrosProc.Condominios.dCondominiosCamposTableAdapters.APARTAMENTOSTableAdapter();
            this.cARGOSTableAdapter = new CadastrosProc.Condominios.dCondominiosCamposTableAdapters.CARGOSTableAdapter();
            this.apartamentosBlocoTableAdapter = new CadastrosProc.Condominios.dCondominiosCamposTableAdapters.ApartamentosBlocoTableAdapter();
            this.buscaSindicoTableAdapter = new CadastrosProc.Condominios.dCondominiosCamposTableAdapters.BuscaSindicoTableAdapter();
            this.formapagtoTableAdapter = new CadastrosProc.Condominios.dCondominiosCamposTableAdapters.FORMAPAGTOTableAdapter();
            this.paGamentoEspecificoTableAdapter = new CadastrosProc.Condominios.dCondominiosCamposTableAdapters.PaGamentoEspecificoTableAdapter();
            this.imageComboBoxEdit1 = new DevExpress.XtraEditors.ImageComboBoxEdit();
            this.BtAtivar = new DevExpress.XtraEditors.SimpleButton();
            this.simpleButton1 = new DevExpress.XtraEditors.SimpleButton();
            cONCodigoLabel = new System.Windows.Forms.Label();
            cONNomeLabel = new System.Windows.Forms.Label();
            label2 = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.GroupControl_F)).BeginInit();
            this.GroupControl_F.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.TabControl_F)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.BarManager_F)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.BarAndDockingController_F)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.BindingSource_F)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.StyleController_F)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dCondominiosCampos)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cONDOMINIOSBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cONCodigoTextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cONNomeTextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.eMPRESASBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cIDADESBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.imageComboBoxEdit1.Properties)).BeginInit();
            this.SuspendLayout();
            // 
            // GroupControl_F
            // 
            this.GroupControl_F.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(128)))));
            this.GroupControl_F.Appearance.BackColor2 = System.Drawing.Color.White;
            this.GroupControl_F.Appearance.GradientMode = System.Drawing.Drawing2D.LinearGradientMode.ForwardDiagonal;
            this.GroupControl_F.Appearance.Options.UseBackColor = true;
            this.GroupControl_F.Controls.Add(this.simpleButton1);
            this.GroupControl_F.Controls.Add(this.BtAtivar);
            this.GroupControl_F.Controls.Add(this.imageComboBoxEdit1);
            this.GroupControl_F.Controls.Add(label2);
            this.GroupControl_F.Controls.Add(cONNomeLabel);
            this.GroupControl_F.Controls.Add(this.cONNomeTextEdit);
            this.GroupControl_F.Controls.Add(cONCodigoLabel);
            this.GroupControl_F.Controls.Add(this.cONCodigoTextEdit);
            this.GroupControl_F.Location = new System.Drawing.Point(0, 35);
            this.GroupControl_F.LookAndFeel.SkinName = "Lilian";
            this.GroupControl_F.LookAndFeel.Style = DevExpress.LookAndFeel.LookAndFeelStyle.Flat;
            this.GroupControl_F.LookAndFeel.UseDefaultLookAndFeel = false;
            this.GroupControl_F.Size = new System.Drawing.Size(1568, 35);
            // 
            // TabControl_F
            // 
            this.TabControl_F.Location = new System.Drawing.Point(0, 70);
            this.TabControl_F.Size = new System.Drawing.Size(1568, 420);
            this.TabControl_F.SelectedPageChanged += new DevExpress.XtraTab.TabPageChangedEventHandler(this.TabControl_F_SelectedPageChanged);
            // 
            // barStatus_F
            // 
            this.barStatus_F.OptionsBar.AllowQuickCustomization = false;
            this.barStatus_F.OptionsBar.DisableClose = true;
            this.barStatus_F.OptionsBar.DisableCustomization = true;
            this.barStatus_F.OptionsBar.DrawDragBorder = false;
            this.barStatus_F.OptionsBar.DrawSizeGrip = true;
            this.barStatus_F.OptionsBar.UseWholeRow = true;
            // 
            // barComponente_F
            // 
            this.barComponente_F.OptionsBar.AllowQuickCustomization = false;
            this.barComponente_F.OptionsBar.DisableClose = true;
            this.barComponente_F.OptionsBar.DisableCustomization = true;
            this.barComponente_F.OptionsBar.DrawDragBorder = false;
            this.barComponente_F.OptionsBar.UseWholeRow = true;
            // 
            // btnConfirmar_F
            // 
            this.btnConfirmar_F.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("btnConfirmar_F.ImageOptions.Image")));
            // 
            // btnCancelar_F
            // 
            this.btnCancelar_F.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("btnCancelar_F.ImageOptions.Image")));
            // 
            // btnFechar_F
            // 
            this.btnFechar_F.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("btnFechar_F.ImageOptions.Image")));
            // 
            // BarAndDockingController_F
            // 
            this.BarAndDockingController_F.PropertiesBar.AllowLinkLighting = false;
            this.BarAndDockingController_F.PropertiesBar.DefaultGlyphSize = new System.Drawing.Size(16, 16);
            this.BarAndDockingController_F.PropertiesBar.DefaultLargeGlyphSize = new System.Drawing.Size(32, 32);
            // 
            // ToolTipController_F
            // 
            this.ToolTipController_F.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(222)))), ((int)(((byte)(237)))), ((int)(((byte)(250)))));
            this.ToolTipController_F.Appearance.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(20)))), ((int)(((byte)(69)))), ((int)(((byte)(141)))));
            this.ToolTipController_F.Appearance.Options.UseBackColor = true;
            this.ToolTipController_F.Appearance.Options.UseFont = true;
            this.ToolTipController_F.Appearance.Options.UseForeColor = true;
            this.ToolTipController_F.Appearance.Options.UseTextOptions = true;
            this.ToolTipController_F.Appearance.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            // 
            // StyleController_F
            // 
            this.StyleController_F.AppearanceDisabled.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.StyleController_F.AppearanceDisabled.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.StyleController_F.AppearanceDisabled.Options.UseBackColor = true;
            this.StyleController_F.AppearanceDisabled.Options.UseForeColor = true;
            this.StyleController_F.AppearanceFocused.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(253)))), ((int)(((byte)(253)))), ((int)(((byte)(181)))));
            this.StyleController_F.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
            this.StyleController_F.AppearanceFocused.Options.UseBackColor = true;
            this.StyleController_F.AppearanceFocused.Options.UseFont = true;
            // 
            // cONCodigoLabel
            // 
            cONCodigoLabel.AutoSize = true;
            cONCodigoLabel.BackColor = System.Drawing.Color.Transparent;
            cONCodigoLabel.Location = new System.Drawing.Point(8, 12);
            cONCodigoLabel.Name = "cONCodigoLabel";
            cONCodigoLabel.Size = new System.Drawing.Size(51, 13);
            cONCodigoLabel.TabIndex = 0;
            cONCodigoLabel.Text = "CODCON";
            // 
            // cONNomeLabel
            // 
            cONNomeLabel.AutoSize = true;
            cONNomeLabel.BackColor = System.Drawing.Color.Transparent;
            cONNomeLabel.Location = new System.Drawing.Point(156, 12);
            cONNomeLabel.Name = "cONNomeLabel";
            cONNomeLabel.Size = new System.Drawing.Size(34, 13);
            cONNomeLabel.TabIndex = 2;
            cONNomeLabel.Text = "Nome";
            // 
            // label2
            // 
            label2.AutoSize = true;
            label2.BackColor = System.Drawing.Color.Transparent;
            label2.Location = new System.Drawing.Point(577, 12);
            label2.Name = "label2";
            label2.Size = new System.Drawing.Size(38, 13);
            label2.TabIndex = 8;
            label2.Text = "Status";
            // 
            // dCondominiosCampos
            // 
            this.dCondominiosCampos.DataSetName = "dCondominiosCampos";
            this.dCondominiosCampos.EnforceConstraints = false;
            this.dCondominiosCampos.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // cONDOMINIOSBindingSource
            // 
            this.cONDOMINIOSBindingSource.DataMember = "CONDOMINIOS";
            this.cONDOMINIOSBindingSource.DataSource = this.dCondominiosCampos;
            // 
            // cONCodigoTextEdit
            // 
            this.cONCodigoTextEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.cONDOMINIOSBindingSource, "CONCodigo", true));
            this.cONCodigoTextEdit.Location = new System.Drawing.Point(62, 6);
            this.cONCodigoTextEdit.Name = "cONCodigoTextEdit";
            this.cONCodigoTextEdit.Size = new System.Drawing.Size(85, 22);
            this.cONCodigoTextEdit.TabIndex = 1;
            this.cONCodigoTextEdit.Validating += new System.ComponentModel.CancelEventHandler(this.cONCodigoTextEdit_Validating);
            // 
            // cONNomeTextEdit
            // 
            this.cONNomeTextEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.cONDOMINIOSBindingSource, "CONNome", true));
            this.cONNomeTextEdit.Location = new System.Drawing.Point(193, 6);
            this.cONNomeTextEdit.Name = "cONNomeTextEdit";
            this.cONNomeTextEdit.Size = new System.Drawing.Size(185, 22);
            this.cONNomeTextEdit.TabIndex = 3;
            // 
            // eMPRESASBindingSource
            // 
            this.eMPRESASBindingSource.DataMember = "EMPRESAS";
            this.eMPRESASBindingSource.DataSource = typeof(Framework.datasets.dEmpresas);
            // 
            // bLOCOSTableAdapter
            // 
            this.bLOCOSTableAdapter.ClearBeforeFill = true;
            this.bLOCOSTableAdapter.GetNovosDados = ((System.Collections.ArrayList)(resources.GetObject("bLOCOSTableAdapter.GetNovosDados")));
            this.bLOCOSTableAdapter.TabelaDataTable = null;
            // 
            // cIDADESBindingSource
            // 
            this.cIDADESBindingSource.DataMember = "CIDADES";
            this.cIDADESBindingSource.DataSource = typeof(Framework.datasets.dCIDADES);
            // 
            // aPARTAMENTOSTableAdapter
            // 
            this.aPARTAMENTOSTableAdapter.ClearBeforeFill = true;
            this.aPARTAMENTOSTableAdapter.GetNovosDados = ((System.Collections.ArrayList)(resources.GetObject("aPARTAMENTOSTableAdapter.GetNovosDados")));
            this.aPARTAMENTOSTableAdapter.TabelaDataTable = null;
            // 
            // cARGOSTableAdapter
            // 
            this.cARGOSTableAdapter.ClearBeforeFill = true;
            this.cARGOSTableAdapter.GetNovosDados = ((System.Collections.ArrayList)(resources.GetObject("cARGOSTableAdapter.GetNovosDados")));
            this.cARGOSTableAdapter.TabelaDataTable = null;
            // 
            // apartamentosBlocoTableAdapter
            // 
            this.apartamentosBlocoTableAdapter.ClearBeforeFill = true;
            this.apartamentosBlocoTableAdapter.GetNovosDados = ((System.Collections.ArrayList)(resources.GetObject("apartamentosBlocoTableAdapter.GetNovosDados")));
            this.apartamentosBlocoTableAdapter.TabelaDataTable = null;
            // 
            // buscaSindicoTableAdapter
            // 
            this.buscaSindicoTableAdapter.ClearBeforeFill = true;
            this.buscaSindicoTableAdapter.GetNovosDados = ((System.Collections.ArrayList)(resources.GetObject("buscaSindicoTableAdapter.GetNovosDados")));
            this.buscaSindicoTableAdapter.TabelaDataTable = null;
            // 
            // formapagtoTableAdapter
            // 
            this.formapagtoTableAdapter.ClearBeforeFill = true;
            this.formapagtoTableAdapter.GetNovosDados = ((System.Collections.ArrayList)(resources.GetObject("formapagtoTableAdapter.GetNovosDados")));
            this.formapagtoTableAdapter.TabelaDataTable = null;
            // 
            // paGamentoEspecificoTableAdapter
            // 
            this.paGamentoEspecificoTableAdapter.ClearBeforeFill = true;
            this.paGamentoEspecificoTableAdapter.GetNovosDados = ((System.Collections.ArrayList)(resources.GetObject("paGamentoEspecificoTableAdapter.GetNovosDados")));
            this.paGamentoEspecificoTableAdapter.TabelaDataTable = null;
            // 
            // imageComboBoxEdit1
            // 
            this.imageComboBoxEdit1.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.cONDOMINIOSBindingSource, "CONStatus", true));
            this.imageComboBoxEdit1.Location = new System.Drawing.Point(621, 6);
            this.imageComboBoxEdit1.MenuManager = this.BarManager_F;
            this.imageComboBoxEdit1.Name = "imageComboBoxEdit1";
            this.imageComboBoxEdit1.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo, "", -1, true, false, false, editorButtonImageOptions1, new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), serializableAppearanceObject1, serializableAppearanceObject2, serializableAppearanceObject3, serializableAppearanceObject4, "", null, null, DevExpress.Utils.ToolTipAnchor.Default)});
            this.imageComboBoxEdit1.Properties.ReadOnly = true;
            this.imageComboBoxEdit1.Size = new System.Drawing.Size(88, 22);
            this.imageComboBoxEdit1.TabIndex = 11;
            // 
            // BtAtivar
            // 
            this.BtAtivar.Appearance.BackColor = System.Drawing.Color.Lime;
            this.BtAtivar.Appearance.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtAtivar.Appearance.Options.UseBackColor = true;
            this.BtAtivar.Appearance.Options.UseFont = true;
            this.BtAtivar.ImageOptions.Image = global::Cadastros.Properties.Resources.deslP1;
            this.BtAtivar.Location = new System.Drawing.Point(715, 7);
            this.BtAtivar.LookAndFeel.Style = DevExpress.LookAndFeel.LookAndFeelStyle.Style3D;
            this.BtAtivar.LookAndFeel.UseDefaultLookAndFeel = false;
            this.BtAtivar.Name = "BtAtivar";
            this.BtAtivar.Size = new System.Drawing.Size(90, 22);
            this.BtAtivar.TabIndex = 12;
            this.BtAtivar.Text = "Ativar";
            this.BtAtivar.Visible = false;
            this.BtAtivar.Click += new System.EventHandler(this.BtAtivar_Click);
            // 
            // simpleButton1
            // 
            this.simpleButton1.Appearance.BackColor = System.Drawing.Color.Red;
            this.simpleButton1.Appearance.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.simpleButton1.Appearance.Options.UseBackColor = true;
            this.simpleButton1.Appearance.Options.UseFont = true;
            this.simpleButton1.ImageOptions.Image = global::Cadastros.Properties.Resources.btnCancelar_F_Glyph;
            this.simpleButton1.Location = new System.Drawing.Point(811, 7);
            this.simpleButton1.LookAndFeel.Style = DevExpress.LookAndFeel.LookAndFeelStyle.Style3D;
            this.simpleButton1.LookAndFeel.UseDefaultLookAndFeel = false;
            this.simpleButton1.Name = "simpleButton1";
            this.simpleButton1.Size = new System.Drawing.Size(90, 22);
            this.simpleButton1.TabIndex = 13;
            this.simpleButton1.Text = "Inativo";
            this.simpleButton1.Visible = false;
            this.simpleButton1.Click += new System.EventHandler(this.simpleButton1_Click);
            // 
            // cCondominiosCampos2
            // 
            this.Autofill = false;
            this.AutoScroll = true;
            this.BindingSourcePrincipal = this.cONDOMINIOSBindingSource;
            this.Doc = System.Windows.Forms.DockStyle.Fill;
            this.LookAndFeel.UseDefaultLookAndFeel = false;
            this.Name = "cCondominiosCampos2";
            paginaTabInfo1.Name = "Basicos";
            paginaTabInfo1.PaginaTabType = typeof(Cadastros.Condominios.PaginasCondominio.TabBasicos);
            paginaTabInfo2.Name = "Constituição";
            paginaTabInfo2.PaginaTabType = typeof(Cadastros.Condominios.PaginasCondominio.TabConstituicao);
            paginaTabInfo3.Name = "Agenda";
            paginaTabInfo3.PaginaTabType = typeof(Cadastros.Condominios.PaginasCondominio.TabAgenda);
            paginaTabInfo4.Name = "Corpo Diretivo";
            paginaTabInfo4.PaginaTabType = typeof(Cadastros.Condominios.PaginasCondominio.TabCorpoDiretivo);
            paginaTabInfo5.Name = "Financeiro";
            paginaTabInfo5.PaginaTabType = typeof(Cadastros.Condominios.PaginasCondominio.TabFinaceiro);
            paginaTabInfo6.Name = "Operacional";
            paginaTabInfo6.PaginaTabType = typeof(Cadastros.Condominios.PaginasCondominio.TabOperacional);
            paginaTabInfo7.Name = "Boleto";
            paginaTabInfo7.PaginaTabType = typeof(Cadastros.Condominios.PaginasCondominio.TabBoleto);
            paginaTabInfo8.Name = "DP";
            paginaTabInfo8.PaginaTabType = typeof(Cadastros.Condominios.PaginasCondominio.TabDP);
            paginaTabInfo9.Name = "Contas";
            paginaTabInfo9.PaginaTabType = typeof(Cadastros.Condominios.PaginasCondominio.TabContas);
            paginaTabInfo10.Name = "Internet";
            paginaTabInfo10.PaginaTabType = typeof(Cadastros.Condominios.TabInternet);
            paginaTabInfo11.Name = "Caderninho";
            paginaTabInfo11.PaginaTabType = typeof(Cadastros.Condominios.PaginasCondominio.TabCaderninho);
            paginaTabInfo12.Name = "Publicações";
            paginaTabInfo12.PaginaTabType = typeof(Cadastros.Condominios.PaginasCondominio.TabPublicacoes);
            paginaTabInfo13.Name = "Descontos";
            paginaTabInfo13.PaginaTabType = typeof(Cadastros.Condominios.PaginasCondominio.TabDescontos);
            paginaTabInfo14.Name = "Balancete";
            paginaTabInfo14.PaginaTabType = typeof(Cadastros.Condominios.PaginasCondominio.TabBalancete);
            paginaTabInfo15.Name = "Faturamento";
            paginaTabInfo15.PaginaTabType = typeof(Cadastros.Condominios.PaginasCondominio.TabFaturamento);
            paginaTabInfo16.Name = "Reservas";
            paginaTabInfo16.PaginaTabType = typeof(Cadastros.Condominios.PaginasCondominio.TabReservas);
            paginaTabInfo17.Name = "Alterações";
            paginaTabInfo17.PaginaTabType = typeof(Cadastros.Condominios.PaginasCondominio.TabCONHistorico);
            paginaTabInfo18.Name = "Histórico";
            paginaTabInfo18.PaginaTabType = typeof(Cadastros.Condominios.PaginasCondominio.TabHistorico);
            this.PaginaTabInfoCollection.AddRange(new CompontesBasicos.Classes_de_interface.PaginaTabInfo[] {
            paginaTabInfo1,
            paginaTabInfo2,
            paginaTabInfo3,
            paginaTabInfo4,
            paginaTabInfo5,
            paginaTabInfo6,
            paginaTabInfo7,
            paginaTabInfo8,
            paginaTabInfo9,
            paginaTabInfo10,
            paginaTabInfo11,
            paginaTabInfo12,
            paginaTabInfo13,
            paginaTabInfo14,
            paginaTabInfo15,
            paginaTabInfo16,
            paginaTabInfo17,
            paginaTabInfo18});
            this.Size = new System.Drawing.Size(1568, 515);
            this.cargaInicial += new System.EventHandler(this.cCondominiosCampos2_cargaInicial);
            this.cargaFinal += new System.EventHandler(this.cCondominiosCampos2_cargaFinal);
            ((System.ComponentModel.ISupportInitialize)(this.GroupControl_F)).EndInit();
            this.GroupControl_F.ResumeLayout(false);
            this.GroupControl_F.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.TabControl_F)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.BarManager_F)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.BarAndDockingController_F)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.BindingSource_F)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.StyleController_F)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dCondominiosCampos)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cONDOMINIOSBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cONCodigoTextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cONNomeTextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.eMPRESASBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cIDADESBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.imageComboBoxEdit1.Properties)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.BindingSource cONDOMINIOSBindingSource;
        private DevExpress.XtraEditors.TextEdit cONCodigoTextEdit;
        private DevExpress.XtraEditors.TextEdit cONNomeTextEdit;        
        private System.Windows.Forms.BindingSource eMPRESASBindingSource;
        private System.Windows.Forms.BindingSource cIDADESBindingSource;
        
        /// <summary>
        /// 
        /// </summary>
        public CadastrosProc.Condominios.dCondominiosCampos dCondominiosCampos;
        
        private CadastrosProc.Condominios.dCondominiosCamposTableAdapters.ApartamentosBlocoTableAdapter apartamentosBlocoTableAdapter;
        /// <summary>
        /// 
        /// </summary>
        public CadastrosProc.Condominios.dCondominiosCamposTableAdapters.CARGOSTableAdapter cARGOSTableAdapter;
        private CadastrosProc.Condominios.dCondominiosCamposTableAdapters.BuscaSindicoTableAdapter buscaSindicoTableAdapter;
        private CadastrosProc.Condominios.dCondominiosCamposTableAdapters.FORMAPAGTOTableAdapter formapagtoTableAdapter;
        
        
        
        /// <summary>
        /// 
        /// </summary>
        public CadastrosProc.Condominios.dCondominiosCamposTableAdapters.PaGamentoEspecificoTableAdapter paGamentoEspecificoTableAdapter;
        /// <summary>
        /// 
        /// </summary>
        public CadastrosProc.Condominios.dCondominiosCamposTableAdapters.APARTAMENTOSTableAdapter aPARTAMENTOSTableAdapter;
        /// <summary>
        /// 
        /// </summary>
        public CadastrosProc.Condominios.dCondominiosCamposTableAdapters.BLOCOSTableAdapter bLOCOSTableAdapter;
        private DevExpress.XtraEditors.ImageComboBoxEdit imageComboBoxEdit1;
        private DevExpress.XtraEditors.SimpleButton BtAtivar;
        private DevExpress.XtraEditors.SimpleButton simpleButton1;
    }
}
