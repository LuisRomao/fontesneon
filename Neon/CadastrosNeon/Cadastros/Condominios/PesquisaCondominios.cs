using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace Cadastros.Condominios
{
    /// <summary>
    /// 
    /// </summary>
    public partial class PesquisaCondominios : CompontesBasicos.ComponenteBase
    {
        /// <summary>
        /// 
        /// </summary>
        public override void RefreshDados()
        {
            cCondominiosGrade1.RefreshDados();
        }

        /// <summary>
        /// 
        /// </summary>
        public PesquisaCondominios()
        {            
            InitializeComponent();
            this.Dock = DockStyle.Fill;
        }


        private void lookupCondominio_F1_alterado(object sender, EventArgs e)
        {
            if (lookupCondominio_F1.CON_sel != -1)
                cCondominiosGrade1.complementoWhere = "CON = " + lookupCondominio_F1.CON_sel.ToString();
            else
                cCondominiosGrade1.complementoWhere = "";
            cCondominiosGrade1.Pesquisar();
        }
    }
}

