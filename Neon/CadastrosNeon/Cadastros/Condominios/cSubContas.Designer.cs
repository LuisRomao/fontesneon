namespace Cadastros.Condominios
{
    partial class cSubContas
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(cSubContas));
            this.LayoutControl = new DevExpress.XtraLayout.LayoutControl();
            this.calcEdit2 = new DevExpress.XtraEditors.CalcEdit();
            this.calcEdit1 = new DevExpress.XtraEditors.CalcEdit();
            this.txtSubConta = new DevExpress.XtraEditors.TextEdit();
            this.txtConta = new DevExpress.XtraEditors.TextEdit();
            this.txtAplicacao = new DevExpress.XtraEditors.TextEdit();
            this.LayoutControlGroup = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlGroup1 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlItem2 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem1 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlGroup2 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlItem3 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlGroup3 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlItem4 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem5 = new DevExpress.XtraLayout.LayoutControlItem();
            this.dCondominiosCampos = new Cadastros.Condominios.dCondominiosCampos();
            this.sALDOSTableAdapter = new Cadastros.Condominios.dCondominiosCamposTableAdapters.SALDOSTableAdapter();
            ((System.ComponentModel.ISupportInitialize)(this.BarManager_F)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.BarAndDockingController_F)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.BindingSource_F)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.StyleController_F)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LayoutControl)).BeginInit();
            this.LayoutControl.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.calcEdit2.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.calcEdit1.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtSubConta.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtConta.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtAplicacao.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LayoutControlGroup)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dCondominiosCampos)).BeginInit();
            this.SuspendLayout();
            // 
            // barStatus_F
            // 
            this.barStatus_F.OptionsBar.AllowQuickCustomization = false;
            this.barStatus_F.OptionsBar.DisableClose = true;
            this.barStatus_F.OptionsBar.DisableCustomization = true;
            this.barStatus_F.OptionsBar.DrawDragBorder = false;
            this.barStatus_F.OptionsBar.DrawSizeGrip = true;
            this.barStatus_F.OptionsBar.UseWholeRow = true;
            this.barStatus_F.Visible = false;
            // 
            // barComponente_F
            // 
            this.barComponente_F.OptionsBar.AllowQuickCustomization = false;
            this.barComponente_F.OptionsBar.DisableClose = true;
            this.barComponente_F.OptionsBar.DisableCustomization = true;
            this.barComponente_F.OptionsBar.DrawDragBorder = false;
            this.barComponente_F.OptionsBar.UseWholeRow = true;
            // 
            // btnFechar_F
            // 
            this.btnFechar_F.Visibility = DevExpress.XtraBars.BarItemVisibility.Never;
            // 
            // BarAndDockingController_F
            // 
            this.BarAndDockingController_F.PropertiesBar.AllowLinkLighting = false;
            // 
            // BindingSource_F
            // 
            this.BindingSource_F.DataMember = "SALDOS";
            this.BindingSource_F.DataSource = this.dCondominiosCampos;
            // 
            // ToolTipController_F
            // 
            this.ToolTipController_F.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(222)))), ((int)(((byte)(237)))), ((int)(((byte)(250)))));
            this.ToolTipController_F.Appearance.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(20)))), ((int)(((byte)(69)))), ((int)(((byte)(141)))));
            this.ToolTipController_F.Appearance.Options.UseBackColor = true;
            this.ToolTipController_F.Appearance.Options.UseFont = true;
            this.ToolTipController_F.Appearance.Options.UseForeColor = true;
            this.ToolTipController_F.Appearance.Options.UseTextOptions = true;
            this.ToolTipController_F.Appearance.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            // 
            // StyleController_F
            // 
            this.StyleController_F.AppearanceDisabled.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.StyleController_F.AppearanceDisabled.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.StyleController_F.AppearanceDisabled.Options.UseBackColor = true;
            this.StyleController_F.AppearanceDisabled.Options.UseForeColor = true;
            this.StyleController_F.AppearanceFocused.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(253)))), ((int)(((byte)(253)))), ((int)(((byte)(181)))));
            this.StyleController_F.AppearanceFocused.Options.UseBackColor = true;
            
            // 
            // LayoutControl
            // 
            this.LayoutControl.Controls.Add(this.calcEdit2);
            this.LayoutControl.Controls.Add(this.calcEdit1);
            this.LayoutControl.Controls.Add(this.txtSubConta);
            this.LayoutControl.Controls.Add(this.txtConta);
            this.LayoutControl.Controls.Add(this.txtAplicacao);
            this.LayoutControl.Dock = System.Windows.Forms.DockStyle.Fill;
            this.LayoutControl.Location = new System.Drawing.Point(0, 30);
            this.LayoutControl.Name = "LayoutControl";
            this.LayoutControl.Root = this.LayoutControlGroup;
            this.LayoutControl.Size = new System.Drawing.Size(410, 208);
            this.LayoutControl.TabIndex = 4;
            this.LayoutControl.Text = "layoutControl1";
            // 
            // calcEdit2
            // 
            this.calcEdit2.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.BindingSource_F, "SLDValorSaldoAnterior", true));
            this.calcEdit2.Location = new System.Drawing.Point(112, 170);
            this.calcEdit2.Name = "calcEdit2";
            this.calcEdit2.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.calcEdit2.Size = new System.Drawing.Size(88, 20);
            this.calcEdit2.StyleController = this.LayoutControl;
            this.calcEdit2.TabIndex = 7;
            // 
            // calcEdit1
            // 
            this.calcEdit1.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.BindingSource_F, "SLDValorSaldoAtual", true));
            this.calcEdit1.Location = new System.Drawing.Point(310, 170);
            this.calcEdit1.Name = "calcEdit1";
            this.calcEdit1.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.calcEdit1.Properties.DisplayFormat.FormatString = "n2";
            this.calcEdit1.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.calcEdit1.Properties.ShowCloseButton = true;
            this.calcEdit1.Size = new System.Drawing.Size(88, 20);
            this.calcEdit1.StyleController = this.LayoutControl;
            this.calcEdit1.TabIndex = 6;
            // 
            // txtSubConta
            // 
            this.txtSubConta.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.BindingSource_F, "SLDNome", true));
            this.txtSubConta.Location = new System.Drawing.Point(109, 119);
            this.txtSubConta.Name = "txtSubConta";
            this.txtSubConta.Size = new System.Drawing.Size(292, 20);
            this.txtSubConta.StyleController = this.LayoutControl;
            this.txtSubConta.TabIndex = 5;
            // 
            // txtConta
            // 
            this.txtConta.Enabled = false;
            this.txtConta.Location = new System.Drawing.Point(109, 32);
            this.txtConta.Name = "txtConta";
            this.txtConta.Size = new System.Drawing.Size(292, 20);
            this.txtConta.StyleController = this.LayoutControl;
            this.txtConta.TabIndex = 4;
            // 
            // txtAplicacao
            // 
            this.txtAplicacao.Enabled = false;
            this.txtAplicacao.Location = new System.Drawing.Point(109, 62);
            this.txtAplicacao.Name = "txtAplicacao";
            this.txtAplicacao.Size = new System.Drawing.Size(292, 20);
            this.txtAplicacao.StyleController = this.LayoutControl;
            this.txtAplicacao.TabIndex = 3;
            // 
            // LayoutControlGroup
            // 
            this.LayoutControlGroup.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlGroup1,
            this.layoutControlGroup2});
            this.LayoutControlGroup.Location = new System.Drawing.Point(0, 0);
            this.LayoutControlGroup.Name = "LayoutControlGroup";
            this.LayoutControlGroup.Padding = new DevExpress.XtraLayout.Utils.Padding(0, 0, 0, 0);
            this.LayoutControlGroup.Size = new System.Drawing.Size(410, 208);
            this.LayoutControlGroup.Spacing = new DevExpress.XtraLayout.Utils.Padding(0, 0, 5, 5);
            this.LayoutControlGroup.Text = "LayoutControlGroup";
            this.LayoutControlGroup.TextVisible = false;
            // 
            // layoutControlGroup1
            // 
            this.layoutControlGroup1.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItem2,
            this.layoutControlItem1});
            this.layoutControlGroup1.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroup1.Name = "layoutControlGroup1";
            this.layoutControlGroup1.Padding = new DevExpress.XtraLayout.Utils.Padding(0, 0, 0, 0);
            this.layoutControlGroup1.Size = new System.Drawing.Size(408, 84);
            this.layoutControlGroup1.Spacing = new DevExpress.XtraLayout.Utils.Padding(2, 2, 2, 2);
            this.layoutControlGroup1.Text = ":: Conta ::";
            this.layoutControlGroup1.TextVisible = true;
            // 
            // layoutControlItem2
            // 
            this.layoutControlItem2.Control = this.txtConta;
            this.layoutControlItem2.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItem2.Name = "layoutControlItem2";
            this.layoutControlItem2.Padding = new DevExpress.XtraLayout.Utils.Padding(5, 5, 5, 5);
            this.layoutControlItem2.Size = new System.Drawing.Size(402, 30);
            this.layoutControlItem2.Spacing = new DevExpress.XtraLayout.Utils.Padding(0, 0, 0, 0);
            this.layoutControlItem2.Text = "Conta";
            this.layoutControlItem2.TextSize = new System.Drawing.Size(95, 20);
            this.layoutControlItem2.TextVisible = true;
            // 
            // layoutControlItem1
            // 
            this.layoutControlItem1.Control = this.txtAplicacao;
            this.layoutControlItem1.Location = new System.Drawing.Point(0, 30);
            this.layoutControlItem1.Name = "layoutControlItem1";
            this.layoutControlItem1.Padding = new DevExpress.XtraLayout.Utils.Padding(5, 5, 5, 5);
            this.layoutControlItem1.Size = new System.Drawing.Size(402, 30);
            this.layoutControlItem1.Spacing = new DevExpress.XtraLayout.Utils.Padding(0, 0, 0, 0);
            this.layoutControlItem1.Text = "Aplica��o";
            this.layoutControlItem1.TextSize = new System.Drawing.Size(95, 20);
            this.layoutControlItem1.TextVisible = true;
            // 
            // layoutControlGroup2
            // 
            this.layoutControlGroup2.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItem3,
            this.layoutControlGroup3});
            this.layoutControlGroup2.Location = new System.Drawing.Point(0, 84);
            this.layoutControlGroup2.Name = "layoutControlGroup2";
            this.layoutControlGroup2.Padding = new DevExpress.XtraLayout.Utils.Padding(0, 0, 0, 0);
            this.layoutControlGroup2.Size = new System.Drawing.Size(408, 112);
            this.layoutControlGroup2.Spacing = new DevExpress.XtraLayout.Utils.Padding(2, 2, 5, 2);
            this.layoutControlGroup2.Text = ":: Sub-Conta ::";
            this.layoutControlGroup2.TextVisible = true;
            // 
            // layoutControlItem3
            // 
            this.layoutControlItem3.Control = this.txtSubConta;
            this.layoutControlItem3.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItem3.Name = "layoutControlItem3";
            this.layoutControlItem3.Padding = new DevExpress.XtraLayout.Utils.Padding(5, 5, 5, 5);
            this.layoutControlItem3.Size = new System.Drawing.Size(402, 30);
            this.layoutControlItem3.Spacing = new DevExpress.XtraLayout.Utils.Padding(0, 0, 0, 0);
            this.layoutControlItem3.Text = "Sub-Conta";
            this.layoutControlItem3.TextSize = new System.Drawing.Size(95, 20);
            this.layoutControlItem3.TextVisible = true;
            // 
            // layoutControlGroup3
            // 
            this.layoutControlGroup3.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItem4,
            this.layoutControlItem5});
            this.layoutControlGroup3.Location = new System.Drawing.Point(0, 30);
            this.layoutControlGroup3.Name = "layoutControlGroup3";
            this.layoutControlGroup3.Padding = new DevExpress.XtraLayout.Utils.Padding(0, 0, 0, 0);
            this.layoutControlGroup3.Size = new System.Drawing.Size(402, 55);
            this.layoutControlGroup3.Spacing = new DevExpress.XtraLayout.Utils.Padding(2, 2, 2, 2);
            this.layoutControlGroup3.Text = ":: Lan�amento ::";
            this.layoutControlGroup3.TextVisible = true;
            // 
            // layoutControlItem4
            // 
            this.layoutControlItem4.Control = this.calcEdit1;
            this.layoutControlItem4.Location = new System.Drawing.Point(198, 0);
            this.layoutControlItem4.Name = "layoutControlItem4";
            this.layoutControlItem4.Padding = new DevExpress.XtraLayout.Utils.Padding(5, 5, 5, 5);
            this.layoutControlItem4.Size = new System.Drawing.Size(198, 31);
            this.layoutControlItem4.Spacing = new DevExpress.XtraLayout.Utils.Padding(0, 0, 0, 0);
            this.layoutControlItem4.Text = "Valor Saldo Atual";
            this.layoutControlItem4.TextSize = new System.Drawing.Size(95, 20);
            this.layoutControlItem4.TextVisible = true;
            // 
            // layoutControlItem5
            // 
            this.layoutControlItem5.Control = this.calcEdit2;
            this.layoutControlItem5.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItem5.Name = "layoutControlItem5";
            this.layoutControlItem5.Padding = new DevExpress.XtraLayout.Utils.Padding(5, 5, 5, 5);
            this.layoutControlItem5.Size = new System.Drawing.Size(198, 31);
            this.layoutControlItem5.Spacing = new DevExpress.XtraLayout.Utils.Padding(0, 0, 0, 0);
            this.layoutControlItem5.Text = "Valor Saldo Anterior";
            this.layoutControlItem5.TextSize = new System.Drawing.Size(95, 20);
            this.layoutControlItem5.TextVisible = true;
            // 
            // dCondominiosCampos
            // 
            this.dCondominiosCampos.DataSetName = "dCondominiosCampos";
            this.dCondominiosCampos.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // sALDOSTableAdapter
            // 
            this.sALDOSTableAdapter.ClearBeforeFill = true;
            // 
            // cSubContas
            // 
            this.Controls.Add(this.LayoutControl);
            this.ImagemG = ((System.Drawing.Image)(resources.GetObject("$this.ImagemG")));
            this.ImagemP = ((System.Drawing.Image)(resources.GetObject("$this.ImagemP")));
            this.LookAndFeel.UseDefaultLookAndFeel = false;
            this.Name = "cSubContas";
            this.Size = new System.Drawing.Size(410, 238);
            this.Load += new System.EventHandler(this.cSubContas_Load);
            this.Controls.SetChildIndex(this.LayoutControl, 0);
            ((System.ComponentModel.ISupportInitialize)(this.BarManager_F)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.BarAndDockingController_F)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.BindingSource_F)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.StyleController_F)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LayoutControl)).EndInit();
            this.LayoutControl.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.calcEdit2.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.calcEdit1.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtSubConta.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtConta.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtAplicacao.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LayoutControlGroup)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dCondominiosCampos)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraLayout.LayoutControl LayoutControl;
        private DevExpress.XtraLayout.LayoutControlGroup LayoutControlGroup;
        private DevExpress.XtraEditors.TextEdit txtSubConta;
        private DevExpress.XtraEditors.TextEdit txtConta;
        private DevExpress.XtraEditors.TextEdit txtAplicacao;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem2;
        private DevExpress.XtraEditors.CalcEdit calcEdit1;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup1;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem1;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup2;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem3;
        private dCondominiosCampos dCondominiosCampos;
        //private Cadastros.Condominios.dCondominiosCamposTableAdapters.SALDOSTableAdapter sALDOSTableAdapter;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem4;
        private DevExpress.XtraEditors.CalcEdit calcEdit2;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup3;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem5;
    }
}
