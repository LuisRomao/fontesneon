using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace Cadastros.Condominios
{
    /// <summary>
    /// Classe para inclusão de histórico
    /// </summary>
    public partial class cHistoricos : CompontesBasicos.ComponenteCamposBase
    {
        /// <summary>
        /// Construtor
        /// </summary>
        public cHistoricos()
        {
            InitializeComponent();
            ASSuntobindingSource.DataSource = Framework.datasets.dASSunto.dASSuntoSt.ASSunto;            
        }

        /// <summary>
        /// Confirmação
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected override void btnConfirmar_F_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            try
            {
                this.ValidateChildren();
                if ((dateEdit1.ErrorText != ""))
                    dateEdit1.Focus();
                else if ((lookUpEdit1.ErrorText != ""))
                    lookUpEdit1.Focus();
                else if ((memoEdit1.ErrorText != ""))
                    memoEdit1.Focus();
                else
                    FechaTela(DialogResult.OK);
            }
            catch (NoNullAllowedException)
            {
                MessageBox.Show("Existe campo obrigatório sem um valor informado.",
                                "Campo obrigatório", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }
        }

        /// <summary>
        /// Cancelamento
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected override void btnCancelar_F_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            FechaTela(DialogResult.Cancel);
        }
        
          
        private void dateEdit1_Validating(object sender, CancelEventArgs e)
        {            
            if(dateEdit1.Text == "")
                dateEdit1.ErrorText = "Campo Obrigatorio";
        }

        

        private void memoEdit1_Validating(object sender, CancelEventArgs e)
        {
            if (memoEdit1.Text == "")
                memoEdit1.ErrorText = "Campo Obrigatorio";
        }

        private void lookUpEdit1_Validating(object sender, CancelEventArgs e)
        {
            if (lookUpEdit1.EditValue == null)
                lookUpEdit1.ErrorText = "Campo Obrigatorio";
        }

        private void lookUpEdit1_ButtonClick(object sender, DevExpress.XtraEditors.Controls.ButtonPressedEventArgs e)
        {
            if (e.Button.Kind == DevExpress.XtraEditors.Controls.ButtonPredefines.Plus) { 
                string novo = "";
                if (VirInput.Input.Execute("Novo Assunto", ref novo, false, 30))
                {
                    novo = novo.Trim().ToUpper();
                    foreach (Framework.datasets.dASSunto.ASSuntoRow rowASS in Framework.datasets.dASSunto.dASSuntoSt.ASSunto)
                        if (rowASS.ASSDescricao.ToUpper() == novo)
                            return;
                    Framework.datasets.dASSunto.dASSuntoSt.ASSunto.AddASSuntoRow(novo);
                    Framework.datasets.dASSunto.dASSuntoSt.ASSuntoTableAdapter.Update(Framework.datasets.dASSunto.dASSuntoSt.ASSunto);
                    Framework.datasets.dASSunto.dASSuntoSt.ASSunto.AcceptChanges();
                    lookUpEdit1.Text = novo;
                }
            }
        }
    }
}

