using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace Cadastros.Condominios
{
    public partial class cSubContas : CompontesBasicos.ComponenteCamposBase
    {
        public cSubContas()
        {
            InitializeComponent();
        }

        protected override void btnConfirmar_F_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            this.Validate();
            this.BindingSource_F.EndEdit();
            FechaTela(DialogResult.OK);
        }

        protected override void btnCancelar_F_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            this.BindingSource_F.CancelEdit();
            FechaTela(DialogResult.Cancel);
        }

        private void cSubContas_Load(object sender, EventArgs e)
        {
            if (BindingSource_F.Current != null)
            {
                DataRow _Row = ((DataRowView)(BindingSource_F.Current)).Row;

                DataRow _ParentRow = _Row.GetParentRow("FK_SALDOS_APLICACOES");

                txtAplicacao.Text = _ParentRow["APLNome"].ToString();
                txtConta.Text = _ParentRow["APLConta"].ToString();
            }
        }
    }
}

