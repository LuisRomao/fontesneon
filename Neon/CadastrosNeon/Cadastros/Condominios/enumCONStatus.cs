using System;
using System.Collections.Generic;
using System.Text;

namespace Cadastros.Condominios
{
    

    /// <summary>
    /// virEnum para campo 
    /// </summary>
    [Obsolete("Movido para enumeracoes")]
    public class virEnumCONStatus : dllVirEnum.VirEnum
    {
        /// <summary>
        /// Construtor padrao
        /// </summary>
        /// <param name="Tipo">Tipo</param>
        public virEnumCONStatus(Type Tipo)
            :
            base(Tipo)
        {
            UsarShort = true;
        }

        private static virEnumCONStatus _virEnumCONStatusSt;

        /// <summary>
        /// VirEnum est�tico para campo CONStatus
        /// </summary>
        public static virEnumCONStatus virEnumCONStatusSt
        {
            get
            {
                if (_virEnumCONStatusSt == null)
                {
                    _virEnumCONStatusSt = new virEnumCONStatus(typeof(CONStatus));
                    _virEnumCONStatusSt.GravaNomes(CONStatus.Inativo, "INATIVO");
                    _virEnumCONStatusSt.GravaNomes(CONStatus.Ativado, "ATIVADO");
                    _virEnumCONStatusSt.GravaNomes(CONStatus.Desativado, "DESATIVADO");
                    _virEnumCONStatusSt.GravaNomes(CONStatus.Implantacao, "IMPLANTA��O");
                    _virEnumCONStatusSt.GravaCor(CONStatus.Inativo, System.Drawing.Color.Red);
                    _virEnumCONStatusSt.GravaCor(CONStatus.Ativado, System.Drawing.Color.Lime);
                    _virEnumCONStatusSt.GravaCor(CONStatus.Desativado, System.Drawing.Color.Yellow);
                    _virEnumCONStatusSt.GravaCor(CONStatus.Implantacao, System.Drawing.Color.Aqua);
                }
                return _virEnumCONStatusSt;
            }
        }
    }
}

