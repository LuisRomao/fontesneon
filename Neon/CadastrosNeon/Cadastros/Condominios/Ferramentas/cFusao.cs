﻿using System;
using System.Windows.Forms;
using CompontesBasicos;

namespace Cadastros.Condominios.Ferramentas
{
    /// <summary>
    /// 
    /// </summary>
    public partial class cFusao : ComponenteBaseDialog
    {
        /// <summary>
        /// 
        /// </summary>
        /// <param name="CON"></param>
        public cFusao(int CON)
        {
            InitializeComponent();
            dFusao.APARTAMENTOSTableAdapter.FillBy(dFusao.APARTAMENTOS, CON);
        }

        private void gridView1_FocusedRowChanged(object sender, DevExpress.XtraGrid.Views.Base.FocusedRowChangedEventArgs e)
        {
            dFusao.APARTAMENTOSRow row = (dFusao.APARTAMENTOSRow)gridView1.GetDataRow(e.FocusedRowHandle);
            if (row == null)
                gridView2.ActiveFilterString = "";
            else
                gridView2.ActiveFilterString = string.Format("([APT] <> {0}) and ([PESNome] = '{1}')",row.APT,row.PESNome);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="Resultado"></param>
        protected override void FechaTela(DialogResult Resultado)
        {
            dFusao.APARTAMENTOSRow rowdoadora = (dFusao.APARTAMENTOSRow)gridView1.GetFocusedDataRow();
            dFusao.APARTAMENTOSRow rowreceptora = (dFusao.APARTAMENTOSRow)gridView2.GetFocusedDataRow();
            if (Resultado == DialogResult.OK)
            {
                string mem = string.Format("Confirma que a unidade {0}-{1} vai deixar de existir e seus boletos e históricos serão incorporados no {2}-{3}\r\n** Esta operação é irreversível **",
                                              rowdoadora.BLOCodigo,rowdoadora.APTNumero,
                                              rowreceptora.BLOCodigo,rowreceptora.APTNumero
                                             );
                if (MessageBox.Show(mem,"ATENÇÃO",MessageBoxButtons.YesNo,MessageBoxIcon.Warning,MessageBoxDefaultButton.Button2) == DialogResult.Yes)
                {
                    try
                    {
                        VirMSSQL.TableAdapter.AbreTrasacaoSQL("Cadastros cFusao - 421");
                        VirMSSQL.TableAdapter.STTableAdapter.ExecutarSQLNonQuery("update BOletoDetalhe set BOD_APT = @P1 where BOD_APT = @P2", rowreceptora.APT, rowdoadora.APT);
                        VirMSSQL.TableAdapter.STTableAdapter.ExecutarSQLNonQuery("update BOLetos set BOL_APT = @P1 where BOL_APT = @P2", rowreceptora.APT, rowdoadora.APT);
                        //VirMSSQL.TableAdapter.STTableAdapter.ExecutarSQLNonQuery("up apartamento where APT = @P1", rowdoadora.APT);
                        VirMSSQL.TableAdapter.STTableAdapter.ExecutarSQLNonQuery("delete apartamentos where APT = @P1", rowdoadora.APT);
                        if (rowreceptora.IsAPTHistoricoNull())
                            rowreceptora.APTHistorico = "";
                        if (rowdoadora.IsAPTHistoricoNull())
                            rowdoadora.APTHistorico = "";
                        rowreceptora.APTHistorico += string.Format("{0:dd/MM/yyyy : HH:mm:ss} A unidade {1} {2} foi incorporada nesta por {3}\r\nHistórico herdado:\r\n****\r\n{4}\r\n****\r\n",
                                                      DateTime.Now ,rowdoadora.BLOCodigo,rowdoadora.APTNumero,FormPrincipalBase.USULogadoNome,rowdoadora.APTHistorico);
                        rowreceptora.APTFracaoIdeal += rowdoadora.APTFracaoIdeal;
                        dFusao.APARTAMENTOSTableAdapter.Update(rowreceptora);
                        VirMSSQL.TableAdapter.STTableAdapter.Commit();
                    }
                    catch (Exception e)
                    {
                        VirMSSQL.TableAdapter.STTableAdapter.Vircatch(e);
                        throw new Exception("Erro enviado",e);
                    }
                }
            }
            base.FechaTela(Resultado);
        }
    }

    
}
