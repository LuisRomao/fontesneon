﻿namespace Cadastros.Condominios.Ferramentas {
    
    
    public partial class dFusao {
        private dFusaoTableAdapters.APARTAMENTOSTableAdapter aPARTAMENTOSTableAdapter;
        /// <summary>
        /// TableAdapter padrão para tabela: APARTAMENTOS
        /// </summary>
        public dFusaoTableAdapters.APARTAMENTOSTableAdapter APARTAMENTOSTableAdapter
        {
            get
            {
                if (aPARTAMENTOSTableAdapter == null)
                {
                    aPARTAMENTOSTableAdapter = new dFusaoTableAdapters.APARTAMENTOSTableAdapter();
                    aPARTAMENTOSTableAdapter.TrocarStringDeConexao();
                };
                return aPARTAMENTOSTableAdapter;
            }
        }
    }
}
