using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using Cadastros.Cidades;
using Cadastros.Bancos;
using Cadastros.Fornecedores;
using DevExpress.XtraLayout;
using Cadastros.Utilitarios.MSAccess;

namespace Cadastros.Condominios
{
    public partial class cCondominiosCampos : CompontesBasicos.ComponenteCamposBase
    {
        private void InitRelation()
        {
            this.speApartamentos.DataBindings.Clear();

            //Blocos -> Condominios
            fKBLOCOSCONDOMINIOSBindingSource.DataSource = this.BindingSource_F;
            fKBLOCOSCONDOMINIOSBindingSource.DataMember = "FK_BLOCOS_CONDOMINIOS";

            //Apartamentos -> Blocos
            fKAPARTAMENTOSBLOCOSBindingSource.DataSource = this.fKBLOCOSCONDOMINIOSBindingSource;
            fKAPARTAMENTOSBLOCOSBindingSource.DataMember = "FK_APARTAMENTOS_BLOCOS";

            //Aplicacoes -> Condominios
            fKAPLICACOESCONDOMINIOSBindingSource.DataSource = this.BindingSource_F;
            fKAPLICACOESCONDOMINIOSBindingSource.DataMember = "FK_APLICACOES_CONDOMINIOS";

            //Saldos -> Aplicacoes
            fKSALDOSAPLICACOESBindingSource.DataSource = this.fKAPLICACOESCONDOMINIOSBindingSource;
            fKSALDOSAPLICACOESBindingSource.DataMember = "FK_SALDOS_APLICACOES";

            //Historicos -> Condominios
            fKHISTORICOSCONDOMINIOSBindingSource.DataSource = this.BindingSource_F;
            fKHISTORICOSCONDOMINIOSBindingSource.DataMember = "FK_CONDOMINIOS_HISTORICOS";

            //this.speApartamentos.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.fKBLOCOSCONDOMINIOSBindingSource, "BLOTotalApartamento", true));
        }

        public cCondominiosCampos()
        {           
            InitializeComponent();
            InitRelation();
        }

        private void txtCodcon_Validated(object sender, EventArgs e)
        {
            txtCodcon.ErrorText = "";

            if (txtCodcon.Text.Trim() != "")
            {
                int pk = Convert.ToInt32(((DataRowView)(BindingSource_F.Current))["CON"]);

                string concodigo = txtCodcon.EditValue.ToString();

                object connome = cONDOMINIOSTableAdapter.ValidaCODCON(concodigo, pk);

                if (connome != null)
                    txtCodcon.ErrorText = "O CODCON informado j� pertence ao condom�nio " + connome.ToString();
            }
        }

        protected override void  btnConfirmar_F_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            try
            {
                if (DialogResult.Yes == MessageBox.Show("Confirma a grava��o?", "Confirmar", MessageBoxButtons.YesNo, MessageBoxIcon.Question))
                {

                    this.Validate(true);

                    this.BindingSource_F.EndEdit();

                    cONDOMINIOSTableAdapter.Update(dCondominiosCampos.CONDOMINIOS);
                    bLOCOSTableAdapter.Update(dCondominiosCampos.BLOCOS);
                    aPARTAMENTOSTableAdapter.Update(dCondominiosCampos.APARTAMENTOS);
                    aPLICACOESTableAdapter.Update(dCondominiosCampos.APLICACOES);
                    sALDOSTableAdapter.Update(dCondominiosCampos.SALDOS);                    
                    //cORPODIRETIVOTableAdapter.Update(dCondominiosCampos.CORPODIRETIVO);

                    dCondominiosCampos.AcceptChanges();

                    try
                    {
                        DataRowView DRV = (DataRowView)BindingSource_F.Current;
                        dCondominiosCampos.CONDOMINIOSRow row = (dCondominiosCampos.CONDOMINIOSRow)DRV.Row;
                        if (row.CONStatus != 0)
                        {
                            if ((bool)eMPRESASTableAdapter.EmpAtiva((Int32)((DataRowView)(this.BindingSource_F.Current))["CON_EMP"]))
                            {

                                fExportToMsAccess _Export = new fExportToMsAccess();

                                //_Export.ExportarCondominioParaMsAccess(txtCodcon.Text);
                                _Export.ExportarApartamentosParaMsAccess(txtCodcon.Text);
                                _Export.ExportarPessoasParaMsAccess(txtCodcon.Text);
                            };
                        };
                    }
                    catch (Exception Erro)
                    {
                        MessageBox.Show(Erro.Message);
                    }
                       
                    FechaTela(DialogResult.OK);
                }
            }
            catch (Exception Erro)
            {
                if (Erro is NoNullAllowedException)
                    MessageBox.Show("Existe campo obrig�torio sem um valor informado.", "Campo Obrigat�rio", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                else
                    if ((Erro is ConstraintException) && (Erro.Message.ToUpper().IndexOf("UNIQUE") >= 0))
                        MessageBox.Show("Existe campo de valor �nico com valor duplicado.", "Campo de Valor �nico", MessageBoxButtons.OK, MessageBoxIcon.Warning);

                    else
                        MessageBox.Show("Ocorreu uma excess�o desconhecida ao tentar efetuar a opera��o:\r\n\r\n" + Erro.Message.ToUpper() + "\r\n\r\n" +
                                        "Entre em contato com o suporte da Virtual Software e informe sobre o problema.", "Excess�o Desconhecida", MessageBoxButtons.OK, MessageBoxIcon.Error);

                FechaTela(DialogResult.OK);
            }
        }

        private void speBlocos_ButtonClick(object sender, DevExpress.XtraEditors.Controls.ButtonPressedEventArgs e)
        {
            if (e.Button.Index == 1) //Gerar
            {
                if (speBlocos.Value > 0)
                {
                    Int32 iValorIni = (grvBlocos.DataRowCount);
                    Int32 iValorFim = Convert.ToInt32(speBlocos.Value);

                    if (iValorFim < iValorIni)
                    {
                        if (DialogResult.Yes == MessageBox.Show("O n�mero informado para gera��o dos blocos � menor que o total de blocos cadastrados.\r\n" +
                                                                "Este processo far� que alguns blocos sejam exclu�dos at� que seja atingido o n�mero informado.\r\n" +
                                                                "A ordem de exclus�o � do �ltimo bloco para o primeiro.\r\n\r\n" +
                                                                "Continuar com o processo?",
                                                                "Gera��o de Blocos",
                                                                MessageBoxButtons.YesNo,
                                                                MessageBoxIcon.Question,
                                                                MessageBoxDefaultButton.Button2))
                        {
                            while (iValorIni != iValorFim)
                            {
                                grvBlocos.MoveLast();
                                grvBlocos.DeleteRow(grvBlocos.FocusedRowHandle);

                                iValorIni--;
                            }
                        }
                    }
                    else
                    {
                        String sBLONome = "";
                        String sBLOCodigo = "";

                        while (iValorIni != iValorFim)
                        {
                            iValorIni++;

                            if (iValorFim == 1)
                            {
                                sBLOCodigo = "SB";
                                sBLONome = "SEM BLOCO";
                            }
                            else
                            {
                                sBLOCodigo = iValorIni.ToString("00");
                                sBLONome = "BLOCO " + sBLOCodigo;
                            }

                            fKBLOCOSCONDOMINIOSBindingSource.AddNew();
                            DataRowView _NewRow = ((DataRowView)(fKBLOCOSCONDOMINIOSBindingSource.Current));
                            grvBlocos.BeginDataUpdate();
                            _NewRow.BeginEdit();
                            _NewRow["BLOCodigo"] = sBLOCodigo;
                            _NewRow["BLONome"] = sBLONome;
                            _NewRow.EndEdit();
                            grvBlocos.EndDataUpdate();
                        }
                    }

                    //AtualizaQtdeBlocos();
                }
                else
                    MessageBox.Show("Informe um n�mero inteiro maior que 0 (zero).", "Valor Inv�lido", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }
        }

       

        private void speApartamentos_ButtonClick(object sender, DevExpress.XtraEditors.Controls.ButtonPressedEventArgs e)
        {
            if (e.Button.Index == 1)
            {
                if (speApartamentos.Value > 0)
                {
                    Int32 iValorIni = grvApartamentos.DataRowCount;
                    Int32 iValorFim = Convert.ToInt32(speApartamentos.Value);

                    if (iValorFim < iValorIni)
                    {
                        if (DialogResult.Yes == MessageBox.Show("O n�mero informado para gera��o dos apartamentos � menor que o total de apartamentos cadastrados.\r\n" +
                                                                "Este processo far� que alguns apartamentos sejam exclu�dos at� que seja atingido o n�mero informado.\r\n" +
                                                                "A ordem de exclus�o � do �ltimo apartamento para o primeiro.\r\n\r\n" +
                                                                "Continuar com o processo?",
                                                                "Gera��o de Apartamentos",
                                                                MessageBoxButtons.YesNo,
                                                                MessageBoxIcon.Question,
                                                                MessageBoxDefaultButton.Button2))
                        {
                            while (iValorIni != iValorFim)
                            {
                                grvApartamentos.MoveLast();
                                grvApartamentos.DeleteRow(grvApartamentos.FocusedRowHandle);

                                iValorIni--;
                            }
                        }
                    }
                    else
                    {
                        while (iValorIni != iValorFim)
                        {
                            iValorIni++;

                            fKAPARTAMENTOSBLOCOSBindingSource.AddNew();
                            DataRowView _NewRow = ((DataRowView)(fKAPARTAMENTOSBLOCOSBindingSource.Current));
                            grvApartamentos.BeginDataUpdate();
                            _NewRow.BeginEdit();
                            _NewRow["APTNumero"] = iValorIni.ToString("00");
                            _NewRow["APTFracaoIdeal"] = 0;
                            _NewRow.EndEdit();
                            grvApartamentos.EndDataUpdate();
                        }
                    }

                    //AtualizaQtdeApartamentos(grvApartamentos.DataRowCount);
                }
                else
                    MessageBox.Show("Informe um n�mero inteiro maior que 0 (zero).", "Valor Inv�lido", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }
        }

        

        private void cCondominiosCampos_cargaFinal(object sender, EventArgs e)
        {
            bLOCOSTableAdapter.Fill(dCondominiosCampos.BLOCOS, Convert.ToInt32(((DataRowView)(BindingSource_F.Current))["CON"]));
            aPARTAMENTOSTableAdapter.FillCON(dCondominiosCampos.APARTAMENTOS, Convert.ToInt32(((DataRowView)(BindingSource_F.Current))["CON"]));
            aPLICACOESTableAdapter.Fill(dCondominiosCampos.APLICACOES, Convert.ToInt32(((DataRowView)(BindingSource_F.Current))["CON"]));
            sALDOSTableAdapter.Fill(dCondominiosCampos.SALDOS, Convert.ToInt32(((DataRowView)(BindingSource_F.Current))["CON"]));
            
            cORPODIRETIVOTableAdapter.Fill(dCondominiosCampos.CORPODIRETIVO, Convert.ToInt32(((DataRowView)(BindingSource_F.Current))["CON"]));
            

            DataTable _dtbCorpoDiretivo = cORPODIRETIVOTableAdapter.GetDataBy(Convert.ToInt32(((DataRowView)(BindingSource_F.Current))["CON"]));

            if (_dtbCorpoDiretivo.Rows.Count > 0)
            {
                txtSindico_C.Text = _dtbCorpoDiretivo.Rows[0]["PESNome"].ToString();
                txtTelefone_C.Text = _dtbCorpoDiretivo.Rows[0]["PESFone1"].ToString();
            }
        }

        private void cCondominiosCampos_cargaInicial(object sender, EventArgs e)
        {
            cIDADESTableAdapter.Fill(dCondominiosCampos.CIDADES);
            bANCOSTableAdapter.Fill(dCondominiosCampos.BANCOS);
            aDVOGADOSTableAdapter.Fill(dCondominiosCampos.ADVOGADOS);
            
            
            eMPRESASTableAdapter.Fill(dCondominiosCampos.EMPRESAS);
        }

        private void lkpCidades_ButtonClick(object sender, DevExpress.XtraEditors.Controls.ButtonPressedEventArgs e)
        {
            if (e.Button.Index == 1) //Incluir
            {
                Int32 pk = ShowModuloAdd(typeof(cCidadesGrade));

                if (pk > 0)
                {
                    cIDADESTableAdapter.Fill(dCondominiosCampos.CIDADES);
                    lkpCidades.EditValue = pk;
                }
            }
        }

        private void lkpBancos_ButtonClick(object sender, DevExpress.XtraEditors.Controls.ButtonPressedEventArgs e)
        {
            if (e.Button.Index == 1) //Incluir
            {
                Int32 pk = ShowModuloAdd(typeof(cBancosGrade));

                if (pk > 0)
                {
                    bANCOSTableAdapter.Fill(dCondominiosCampos.BANCOS);
                    lkpBancos.EditValue = pk;
                }
            }
        }

        private void rdgFR_EditValueChanged(object sender, EventArgs e)
        {
            switch (rdgFR.SelectedIndex)
            {
                case 0: lciValorFR.Text = "Percentagem"; break;
                case 1: lciValorFR.Text = "Valor"; break;
            }
        }

        private void spePerContabilInicio_TextChanged(object sender, EventArgs e)
        {
            if ((spePerContabilInicio.Value <= 0) || (spePerContabilInicio.Value > 31))
                txtPerContabilFim.Text = "";
            else
                if (spePerContabilInicio.Value == 1)
                    txtPerContabilFim.Text = "31";
                else
                    if (spePerContabilInicio.Value == 31)
                        txtPerContabilFim.Text = "1";
                    else
                        txtPerContabilFim.Text = ((Int32)(spePerContabilInicio.Value - 1)).ToString();

        }

        public static string RegraVencimento(Int32 Dia, String TipoDia, String OrdemContagem)
        {
            string _Vencto = null;

            _Vencto = Dia.ToString() + "� DIA";

            _Vencto += (TipoDia == "C" ? " CORRIDO " : " �TIL ");

            _Vencto += (OrdemContagem == "I" ? " A PARTIR DO �NICIO DO M�S " : " A PARTIR DO FINAL DO M�S ");

            return _Vencto;
        }

        private void speVencimento_TextChanged(object sender, EventArgs e)
        {
            Int32 _Dia = Convert.ToInt32(speVencimento.Value);

            String _TipoDia = ((DataRowView)(BindingSource_F.Current))["CONTipoVencimento"].ToString();

            String _OrdemContagem = ((DataRowView)(BindingSource_F.Current))["CONReferenciaVencimento"].ToString();

            txtRegra.Text = RegraVencimento(_Dia, _TipoDia, _OrdemContagem);
        }

        private void speVencimento_ButtonClick(object sender, DevExpress.XtraEditors.Controls.ButtonPressedEventArgs e)
        {
            if (e.Button.Index == 1) //Incluir
            {
                this.Validate();

                this.BindingSource_F.EndEdit();

                Int32 _Dia = Convert.ToInt32(((DataRowView)(BindingSource_F.Current))["CONDiaVencimento"]);

                String _TipoDia = ((DataRowView)(BindingSource_F.Current))["CONTipoVencimento"].ToString();

                String _OrdemContagem = ((DataRowView)(BindingSource_F.Current))["CONReferenciaVencimento"].ToString();

                if (DialogResult.Cancel == ShowModulo(typeof(cVencimento), BindingSource_F, false))
                    this.BindingSource_F.CancelEdit();

                else
                {
                    _Dia = Convert.ToInt32(((DataRowView)(BindingSource_F.Current))["CONDiaVencimento"]);

                    _TipoDia = ((DataRowView)(BindingSource_F.Current))["CONTipoVencimento"].ToString();

                    _OrdemContagem = ((DataRowView)(BindingSource_F.Current))["CONReferenciaVencimento"].ToString();

                    ((DataRowView)(BindingSource_F.Current)).EndEdit();

                    txtRegra.Text = RegraVencimento(_Dia, _TipoDia, _OrdemContagem);
                }
            }
        }

        private void bnvAplicacoes_ItemClicked(object sender, ToolStripItemClickedEventArgs e)
        {
            if (e.ClickedItem == btnAddAplicacao)//Incluir Conta
            {
                fKAPLICACOESCONDOMINIOSBindingSource.AddNew();

                if (DialogResult.OK == ShowModulo(typeof(cContas), fKAPLICACOESCONDOMINIOSBindingSource, false))
                {
                    grvSaldos.BeginDataUpdate();

                    fKSALDOSAPLICACOESBindingSource.AddNew();

                    DataRowView _NewRow = ((DataRowView)(fKSALDOSAPLICACOESBindingSource.Current));

                    _NewRow.BeginEdit();
                    _NewRow["SLDNome"] = "SUB-CONTA PADR�O";
                    _NewRow["SLDValorSaldoAnterior"] = ((DataRowView)(fKAPLICACOESCONDOMINIOSBindingSource.Current))["APLValorSaldoAnterior"];
                    _NewRow["SLDValorSaldoAtual"] = ((DataRowView)(fKAPLICACOESCONDOMINIOSBindingSource.Current))["APLValorSaldoAtual"];
                    _NewRow.EndEdit();

                    grvSaldos.EndDataUpdate();
                }
            }
            else if (e.ClickedItem == btnAddSubAplicacao)//Incluir Sub-Conta
            {
                if (fKAPLICACOESCONDOMINIOSBindingSource.Count > 0)
                {
                    fKSALDOSAPLICACOESBindingSource.AddNew();

                    if (DialogResult.OK == ShowModulo(typeof(cSubContas), fKSALDOSAPLICACOESBindingSource, false))
                        AtualizaSaldoConta();
                }
            }
            else if (e.ClickedItem == btnEditAplicacao)
            {
                //Aplicacao
                if ((grdAplicacoes.FocusedView.Name == "grvAplicacoes") && (fKAPLICACOESCONDOMINIOSBindingSource.Count > 0))
                {
                    Decimal _VrSaldoAtualAPL = ((Decimal)((DataRowView)(fKAPLICACOESCONDOMINIOSBindingSource.Current))["APLValorSaldoAtual"]);

                    if (DialogResult.OK == ShowModulo(typeof(cContas), fKAPLICACOESCONDOMINIOSBindingSource, false))
                    {
                        grvSaldos.BeginDataUpdate();

                        Decimal _VrSaldoNovoAPL = ((Decimal)((DataRowView)(fKAPLICACOESCONDOMINIOSBindingSource.Current))["APLValorSaldoAtual"]);

                        Decimal _VrSaldoAtual = 0;
                        Decimal _VrUltimoSaldo = 0;

                        DataRow[] _SaldosRows = dCondominiosCampos.SALDOS.Select("SLD_APL = " + ((Int32)((DataRowView)(fKAPLICACOESCONDOMINIOSBindingSource.Current))["APL"]).ToString());

                        Int32 _Contador = 0;

                        foreach (DataRow _DR in _SaldosRows)
                        {
                            _Contador++;

                            _VrSaldoAtual += ((Decimal)(_DR["SLDValorSaldoAtual"]));

                            //Saldo Atual -> Saldo Anterior
                            _DR.BeginEdit();
                            _DR["SLDValorSaldoAnterior"] = _DR["SLDValorSaldoAtual"];

                            if (_Contador != _SaldosRows.Length)
                            {
                                _DR["SLDValorSaldoAtual"] = Decimal.Round(((_VrSaldoNovoAPL * ((Decimal)(_DR["SLDValorSaldoAtual"]))) / _VrSaldoAtualAPL), 2);
                                _VrUltimoSaldo += ((Decimal)(_DR["SLDValorSaldoAtual"]));
                            }
                            else
                                _DR["SLDValorSaldoAtual"] = _VrSaldoNovoAPL - _VrUltimoSaldo;

                            _DR.EndEdit();
                        }

                        grvSaldos.EndDataUpdate();
                    }
                }
                else //Saldo
                    if ((grdAplicacoes.FocusedView.Name == "grvSaldos") && (fKSALDOSAPLICACOESBindingSource.Count > 0))
                    {
                        if (DialogResult.OK == ShowModulo(typeof(cSubContas), fKSALDOSAPLICACOESBindingSource, false))
                        {
                            grvSaldos.BeginDataUpdate();
                            AtualizaSaldoConta();
                            grvSaldos.EndDataUpdate();
                        }                       
                    }
            }
            else if (e.ClickedItem == btnDelAplicacao)
            {
                if ((grdAplicacoes.FocusedView.Name == "grvAplicacoes") && (fKAPLICACOESCONDOMINIOSBindingSource.Count > 0))
                {
                    if (DialogResult.Yes == MessageBox.Show("Confirma a exclus�o da conta selecionada?\r\n\r\nExcluindo uma conta, todas as sub-contas vinculadas tamb�m ser�o exclu�das.",
                                                            "Excluir Conta", MessageBoxButtons.YesNo, MessageBoxIcon.Question))
                    {
                        grvAplicacoes.BeginDataUpdate();
                        fKAPLICACOESCONDOMINIOSBindingSource.RemoveCurrent();
                        grvAplicacoes.EndDataUpdate();
                    }
                }
                else
                    if ((grdAplicacoes.FocusedView.Name == "grvSaldos") && (fKSALDOSAPLICACOESBindingSource.Count > 0))
                    {
                        if (DialogResult.Yes == MessageBox.Show("Confirma a exclus�o da sub-conta selecionada?",
                                                                "Excluir Sub-Conta", MessageBoxButtons.YesNo, MessageBoxIcon.Question))
                        {
                            grvSaldos.BeginDataUpdate();
                            fKSALDOSAPLICACOESBindingSource.RemoveCurrent();
                            AtualizaSaldoConta();
                            grvSaldos.EndDataUpdate();
                        }
                    }
            }
            else if (e.ClickedItem == btnPrintAplicacao)
                grdAplicacoes.ShowPrintPreview();

            grdAplicacoes.RefreshDataSource();
        }

        private void AtualizaSaldoConta()
        {
            grvAplicacoes.BeginDataUpdate();

            DataRowView _APLRow = ((DataRowView)(fKAPLICACOESCONDOMINIOSBindingSource.Current));

            Int32 _APL = ((Int32)_APLRow["APL"]);

            DataRow[] _ChildRows = dCondominiosCampos.SALDOS.Select("SLD_APL = " + _APL.ToString());

            Decimal _VrSaldoAnterior = 0;
            Decimal _VrSaldoAtual = 0;

            foreach (DataRow _DR in _ChildRows)
            {
                _VrSaldoAnterior += ((Decimal)(_DR["SLDValorSaldoAnterior"]));
                _VrSaldoAtual += ((Decimal)(_DR["SLDValorSaldoAtual"]));
            }

            _APLRow.BeginEdit();
            _APLRow["APLValorSaldoAnterior"] = _VrSaldoAnterior;
            _APLRow["APLValorSaldoAtual"] = _VrSaldoAtual;
            _APLRow.EndEdit();

            grvAplicacoes.EndDataUpdate();
        }

        private void bnvAplicacoes_RefreshItems(object sender, EventArgs e)
        {
            Boolean _Habilita = (grvAplicacoes.DataRowCount > 0);

            btnAddSubAplicacao.Enabled = _Habilita;
            btnEditAplicacao.Enabled = _Habilita;
            btnDelAplicacao.Enabled = _Habilita;
            btnPrintAplicacao.Enabled = _Habilita;
        }

        

        

        private void tcgEditorInternet_SelectedPageChanged(object sender, DevExpress.XtraLayout.LayoutTabPageChangedEventArgs e)
        {
            if ((e.Page == lcgVisualizadorInternet) && (lciEditorInternet.Control != null))
                WebBrowser.DocumentText = lciEditorInternet.Control.Text;
        }

        

        private void bnvHistoricos_ItemClicked(object sender, ToolStripItemClickedEventArgs e)
        {
            if (e.ClickedItem == btnAddHistorico)
            {
                fKHISTORICOSCONDOMINIOSBindingSource.AddNew();

                ShowModulo(typeof(cHistoricos), fKHISTORICOSCONDOMINIOSBindingSource, false);
            }
            else
                if (e.ClickedItem == btnEditHistorico)
                {
                    ShowModulo(typeof(cHistoricos), fKHISTORICOSCONDOMINIOSBindingSource, false);
                }
                else
                    if (e.ClickedItem == btnPrintHistorico)
                    {
                        grdHistoricos.ShowPrintPreview();
                    }
                    else if (e.ClickedItem == btnDelHistorico)
                    {
                        grvHistoricos.BeginDataUpdate();
                        fKHISTORICOSCONDOMINIOSBindingSource.RemoveCurrent();
                        grvHistoricos.EndDataUpdate();
                    }
            grdHistoricos.RefreshDataSource();
        }

        private void bnvApartamentos_ItemClicked(object sender, ToolStripItemClickedEventArgs e)
        {
            if (e.ClickedItem == btnAddApto)
            {
                fKAPARTAMENTOSBLOCOSBindingSource.AddNew();
                ShowModulo(typeof(cApartamentos), fKAPARTAMENTOSBLOCOSBindingSource, false);
            }
            else
                if (e.ClickedItem == btnEditApto)
                    ShowModulo(typeof(cApartamentos), fKAPARTAMENTOSBLOCOSBindingSource, false);
                else
                    if (e.ClickedItem == btnPrintApto)
                        grdApartamentos.ShowPrintPreview();
                    else
                        if (e.ClickedItem == btnDelApto)
                            fKAPARTAMENTOSBLOCOSBindingSource.RemoveCurrent();

            //Setando Botoes
            Boolean _Habilita1 = (fKAPARTAMENTOSBLOCOSBindingSource.Count > 0);
            btnDelApto.Enabled = (_Habilita1);
            btnEditApto.Enabled = (_Habilita1);
            btnPrintApto.Enabled = (_Habilita1);

            //AtualizaQtdeApartamentos(grvApartamentos.DataRowCount);
        }

        private void grvApartamentos_InvalidRowException(object sender, DevExpress.XtraGrid.Views.Base.InvalidRowExceptionEventArgs e)
        {
            e.ExceptionMode = DevExpress.XtraEditors.Controls.ExceptionMode.NoAction;
        }

        private void bnvBlocos_RefreshItems(object sender, EventArgs e)
        {
            // *** APARTAMENTOS *** 
            Boolean _Habilita = ((fKBLOCOSCONDOMINIOSBindingSource.Count > 0) && (!((DataRowView)(fKBLOCOSCONDOMINIOSBindingSource.Current)).IsNew));

            foreach (ToolStripItem _Item in bnvApartamentos.Items)
                _Item.Enabled = _Habilita;
        }

        private void HabilitaBotoesBlocos(Boolean EditandoRegistro)
        {
            Boolean _HasRecord = (fKBLOCOSCONDOMINIOSBindingSource.Count > 0);
            
            btnAddBlocos.Enabled = !EditandoRegistro;
            btnEditBlocos.Enabled = ((!EditandoRegistro) && (_HasRecord));
            btnPrintBlocos.Enabled = ((!EditandoRegistro) && (_HasRecord));
            btnDelBlocos.Enabled = ((!EditandoRegistro) && (_HasRecord));
            
            btnPostBlocos.Enabled = EditandoRegistro;
            btnCancelBlocos.Enabled = EditandoRegistro;
        }

        private void grvBlocos_ValidateRow(object sender, DevExpress.XtraGrid.Views.Base.ValidateRowEventArgs e)
        {
            grvBlocos.ClearColumnErrors();

            if (grvBlocos.GetFocusedRowCellDisplayText(colBLOCodigo).Trim() == "")
                grvBlocos.SetColumnError(colBLOCodigo, "Campo obrigat�rio n�o preenchido.");

            if (grvBlocos.GetFocusedRowCellDisplayText(colBLONome).Trim() == "")
                grvBlocos.SetColumnError(colBLONome, "Campo obrigat�rio n�o preenchido.");

            if (grvBlocos.HasColumnErrors)
            {
                e.Valid = false;
                grvBlocos.ShowEditor();
            }
            else
            {
                HabilitaBotoesBlocos(false);
                e.Valid = true;
            }
        }

        private void grvBlocos_InvalidRowException(object sender, DevExpress.XtraGrid.Views.Base.InvalidRowExceptionEventArgs e)
        {
            e.ExceptionMode = DevExpress.XtraEditors.Controls.ExceptionMode.NoAction;
        }

        private void grvBlocos_CellValueChanging(object sender, DevExpress.XtraGrid.Views.Base.CellValueChangedEventArgs e)
        {
            HabilitaBotoesBlocos(true);
        }

        private void grvBlocos_FocusedRowChanged(object sender, DevExpress.XtraGrid.Views.Base.FocusedRowChangedEventArgs e)
        {
            if (e.PrevFocusedRowHandle == fKBLOCOSCONDOMINIOSBindingSource.Position)
                btnPostBlocos.PerformClick();
        }

        private void bnvBlocos_ItemClicked(object sender, ToolStripItemClickedEventArgs e)
        {
            if (e.ClickedItem ==  btnAddBlocos)
            {
                fKBLOCOSCONDOMINIOSBindingSource.AddNew();
                grvBlocos.ShowEditor();
                HabilitaBotoesBlocos(true);
            }
            else
                if (e.ClickedItem == btnEditBlocos)
                {
                    grvBlocos.ShowEditor();
                    HabilitaBotoesBlocos(true);
                }
                else
                    if (e.ClickedItem == btnPrintBlocos)
                    {
                        grdBlocos.ShowPrintPreview();
                    }
                    else
                        if (e.ClickedItem == btnDelBlocos)
                        {
                            if (DialogResult.Yes == MessageBox.Show("Confirma a exclus�o do bloco selecionado?\r\nAo excluir um bloco, todos os apartamentos vinculados tamb�m ser�o exclu�dos.",
                                                                    "Excluir Bloco", MessageBoxButtons.YesNo, MessageBoxIcon.Question))
                            {
                                fKBLOCOSCONDOMINIOSBindingSource.RemoveCurrent();
                                //AtualizaQtdeBlocos();
                                HabilitaBotoesBlocos(false);
                            }
                        }
                        else
                            if (e.ClickedItem == btnPostBlocos)
                            {
                                this.Validate();
                                if (grvBlocos.UpdateCurrentRow())
                                {
                                    try
                                    {
                                        fKBLOCOSCONDOMINIOSBindingSource.EndEdit();
                                        //grvBlocos.HideEditor();
                                        HabilitaBotoesBlocos(false);
                                        //AtualizaQtdeBlocos();
                                    }
                                    catch (ConstraintException)
                                    {
                                        MessageBox.Show("J� existe um outro bloco para o condom�nio com a sigla e/ou nome informado.",
                                                        "Campo Duplicado", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                                    }
                                }
                            }
                            else
                                if (e.ClickedItem == btnCancelBlocos)
                                {
                                    grvBlocos.CancelUpdateCurrentRow();
                                    fKBLOCOSCONDOMINIOSBindingSource.CancelEdit();
                                    //grvBlocos.HideEditor();
                                    HabilitaBotoesBlocos(false);
                                }

            //Setando Botoes
            Boolean _Habilita1 = (fKBLOCOSCONDOMINIOSBindingSource.Count > 0);
            Boolean _Habilita2 = ((e.ClickedItem == btnPostBlocos) || (e.ClickedItem == btnCancelBlocos) || (e.ClickedItem == btnDelBlocos));

            btnAddBlocos.Enabled = _Habilita2;
            btnEditBlocos.Enabled = ((_Habilita1) && (_Habilita2));
            btnPrintBlocos.Enabled = ((_Habilita1) && (_Habilita2));
            btnDelBlocos.Enabled = ((_Habilita1) && (_Habilita2));

            btnPostBlocos.Enabled = (!_Habilita2);
            btnCancelBlocos.Enabled = (!_Habilita2);

            //if (_Habilita2)
              //  AtualizaQtdeBlocos();
        }

        private DevExpress.XtraGrid.Views.Base.BaseView _ViewSaldo = null;

        private void grvSaldos_GotFocus(object sender, EventArgs e)
        {
            _ViewSaldo = grvSaldos;
        }

        private void grvSaldos_Click(object sender, EventArgs e)
        {
                DevExpress.XtraGrid.Views.Grid.GridView _SaldosView = ((DevExpress.XtraGrid.Views.Grid.GridView)(grdAplicacoes.FocusedView));
                DevExpress.XtraGrid.Views.Grid.GridView _AplicacaoView = ((DevExpress.XtraGrid.Views.Grid.GridView)(grdAplicacoes.MainView));

                if ((_SaldosView != null) && (_SaldosView.DataRowCount > 0))
                {
                    Int32 _APL = Convert.ToInt32(_SaldosView.GetDataRow(0)["SLD_APL"]);

                    _AplicacaoView.FocusedRowHandle = _AplicacaoView.LocateByDisplayText(0, colAPL, _APL.ToString());

                    fKSALDOSAPLICACOESBindingSource.Position = _SaldosView.FocusedRowHandle;
                }
        }

        

        private void bnvCorpoDiretivo_ItemClicked(object sender, ToolStripItemClickedEventArgs e)
        {
            if (e.ClickedItem == btnAddCorpoDiretivo)
                ShowModuloAdd(typeof(cCorpoDiretivo), dCondominiosCampos);
            else
                if (e.ClickedItem == btnEditCorpoDiretivo)
                    ShowModulo(typeof(cCorpoDiretivo), dCondominiosCampos, false);
                else
                    if (e.ClickedItem == btnDelCorpoDiretivo)
                    {
                        if (DialogResult.Yes == MessageBox.Show("Confirma a exclus�o para o membro do corpo diretivo selecionado?", 
                                                                "Excluir Membro do Corpo Diretivo", MessageBoxButtons.YesNo, MessageBoxIcon.Question))
                        {
                            cORPODIRETIVOBindingSource.RemoveCurrent();
                        }
                    }
        }

        private void chkRepassaCorreio_EditValueChanged(object sender, EventArgs e)
        {
            Boolean _Habilita = chkRepassaCorreio.Checked;

            clcAR.Enabled = _Habilita;
            clcCS.Enabled = _Habilita;
            rdgAssembleiaCorreio.Enabled = _Habilita;
            rdgCobrancaCorreio.Enabled = _Habilita;
            rdgBoletoCorreio.Enabled = _Habilita;
        }

        private void chkExportarInternet_EditValueChanged(object sender, EventArgs e)
        {
            Boolean _Habilita = chkExportarInternet.Checked;

            chkLiberarBalanceteInternet.Enabled = _Habilita;
            chkAcordosOnLine.Enabled = _Habilita;
            rgpBalanceteInternet.Enabled = _Habilita;
            memEditorInternet.Enabled = _Habilita;
        }

        private void cCondominiosCampos_Load(object sender, EventArgs e)
        {
            tcgCondominio.SelectedTabPage = lcgBasico;
        }

        private void speVencimento_ValueChanged(object sender, EventArgs e)
        {
            if (speVencimento.Value > 0)
            {
                foreach (DataRow _Row in dCondominiosCampos.APARTAMENTOS.Rows)
                {
                    if (_Row["APTDiaDiferente"] != DBNull.Value)
                    {

                        if (Convert.ToBoolean(_Row["APTDiaDiferente"]) == false)
                        {
                            _Row.BeginEdit();
                            _Row["APTDiaCondominio"] = Convert.ToInt32(speVencimento.Value);
                            _Row.EndEdit();
                        }
                    }
                }
            }           
        }

        private void lkpCidades_EditValueChanged(object sender, EventArgs e)
        {
            lkpUF.EditValue = lkpCidades.EditValue;

           

        }

        private void txtCodigoFolha_Validated(object sender, EventArgs e)
        {
            if (sender is TextEdit)
            {
                TextEdit txtCodigoFolha = (sender as TextEdit);

                txtCodigoFolha.ErrorText = "";

                if (txtCodigoFolha.Text.Trim() != "")
                {
                    int pk = Convert.ToInt32(((DataRowView)(BindingSource_F.Current))["CON"]);

                    int concodigo = Convert.ToInt32(txtCodigoFolha.Text);

                    object connome = cONDOMINIOSTableAdapter.ValidaCodigoFolha(concodigo, pk);

                    if (connome != null)
                        txtCodcon.ErrorText = "O Codigo DATAMACE informado j� pertence ao condom�nio " + connome.ToString();
                }
            }
        }
    }
}

