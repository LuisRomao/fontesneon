﻿namespace Cadastros.Condominios
{

    /// <summary>
    /// 
    /// </summary>
    public enum TiposAPTDOC
    {
        /// <summary>
        /// 
        /// </summary>
        escritura = 0,
        /// <summary>
        /// 
        /// </summary>
        advertencia = 1,
        /// <summary>
        /// 
        /// </summary>
        outros = 2,
        /// <summary>
        /// Documento assinado autorizando o débito automático
        /// </summary>
        debito_automatico = 3,
    }

    partial class dApartamentosCampos
    {
        partial class APARTAMENTOSDataTable
        {
        }

        private dApartamentosCamposTableAdapters.APartamentoDocTableAdapter aPartamentoDocTableAdapter;
        /// <summary>
        /// TableAdapter padrão para tabela: APartamentoDoc
        /// </summary>
        public dApartamentosCamposTableAdapters.APartamentoDocTableAdapter APartamentoDocTableAdapter
        {
            get
            {
                if (aPartamentoDocTableAdapter == null)
                {
                    aPartamentoDocTableAdapter = new dApartamentosCamposTableAdapters.APartamentoDocTableAdapter();
                    aPartamentoDocTableAdapter.TrocarStringDeConexao();
                };
                return aPartamentoDocTableAdapter;
            }
        }

        private static dllVirEnum.VirEnum virEnumTiposAPTDOC;

        /// <summary>
        /// VirEnum para TiposAPTDOC
        /// </summary>
        public static dllVirEnum.VirEnum VirEnumTiposAPTDOC
        {
            get
            {
                if (virEnumTiposAPTDOC == null)
                {
                    virEnumTiposAPTDOC = new dllVirEnum.VirEnum(typeof(TiposAPTDOC));
                    virEnumTiposAPTDOC.GravaNomes(TiposAPTDOC.advertencia, "Advertência");
                    virEnumTiposAPTDOC.GravaNomes(TiposAPTDOC.escritura, "Escritura/Contrato");
                    virEnumTiposAPTDOC.GravaNomes(TiposAPTDOC.outros, "Outros");
                    virEnumTiposAPTDOC.GravaNomes(TiposAPTDOC.debito_automatico, "Débito Automático");
                }
                return virEnumTiposAPTDOC;
            }
        }

        private dApartamentosCamposTableAdapters.APARTAMENTOSTableAdapter aPARTAMENTOSTableAdapter;
        /// <summary>
        /// TableAdapter padrão para tabela: APARTAMENTOS
        /// </summary>
        public dApartamentosCamposTableAdapters.APARTAMENTOSTableAdapter APARTAMENTOSTableAdapter
        {
            get
            {
                if (aPARTAMENTOSTableAdapter == null)
                {
                    aPARTAMENTOSTableAdapter = new dApartamentosCamposTableAdapters.APARTAMENTOSTableAdapter();
                    aPARTAMENTOSTableAdapter.TrocarStringDeConexao();
                };
                return aPARTAMENTOSTableAdapter;
            }
        }

        private dApartamentosCamposTableAdapters.CORPODIRETIVOTableAdapter cORPODIRETIVOTableAdapter;
        /// <summary>
        /// TableAdapter padrão para tabela: CORPODIRETIVO
        /// </summary>
        public dApartamentosCamposTableAdapters.CORPODIRETIVOTableAdapter CORPODIRETIVOTableAdapter
        {
            get
            {
                if (cORPODIRETIVOTableAdapter == null)
                {
                    cORPODIRETIVOTableAdapter = new dApartamentosCamposTableAdapters.CORPODIRETIVOTableAdapter();
                    cORPODIRETIVOTableAdapter.TrocarStringDeConexao();
                };
                return cORPODIRETIVOTableAdapter;
            }
        }

        private dApartamentosCamposTableAdapters.PessoasTableAdapter pessoasTableAdapter;
        /// <summary>
        /// TableAdapter padrão para tabela: Pessoas
        /// </summary>
        public dApartamentosCamposTableAdapters.PessoasTableAdapter PessoasTableAdapter
        {
            get
            {
                if (pessoasTableAdapter == null)
                {
                    pessoasTableAdapter = new dApartamentosCamposTableAdapters.PessoasTableAdapter();
                    pessoasTableAdapter.TrocarStringDeConexao();
                };
                return pessoasTableAdapter;
            }
        }

        private dApartamentosCamposTableAdapters.CodConBlocoTableAdapter codConBlocoTableAdapter;
        /// <summary>
        /// TableAdapter padrão para tabela: CodConBloco
        /// </summary>
        public dApartamentosCamposTableAdapters.CodConBlocoTableAdapter CodConBlocoTableAdapter
        {
            get
            {
                if (codConBlocoTableAdapter == null)
                {
                    codConBlocoTableAdapter = new dApartamentosCamposTableAdapters.CodConBlocoTableAdapter();
                    codConBlocoTableAdapter.TrocarStringDeConexao();
                };
                return codConBlocoTableAdapter;
            }
        }

        private dApartamentosCamposTableAdapters.APTPGETableAdapter aPTPGETableAdapter;
        /// <summary>
        /// TableAdapter padrão para tabela: APTPGE
        /// </summary>
        public dApartamentosCamposTableAdapters.APTPGETableAdapter APTPGETableAdapter
        {
            get
            {
                if (aPTPGETableAdapter == null)
                {
                    aPTPGETableAdapter = new dApartamentosCamposTableAdapters.APTPGETableAdapter();
                    aPTPGETableAdapter.TrocarStringDeConexao();
                };
                return aPTPGETableAdapter;
            }
        }
    }
}
