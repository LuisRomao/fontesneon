using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using Cadastros.Cargos;

namespace Cadastros.Condominios
{
    public partial class cCorpoDiretivo : CompontesBasicos.ComponenteCamposBase
    {
        public cCorpoDiretivo()
        {
            InitializeComponent();
        }

        private void cCorpoDiretivo_Load(object sender, EventArgs e)
        {
            lkpConCodigo.EditValue = ((DataRowView)(cONDOMINIOSBindingSource.Current))["CON"];
            lkpConNome.EditValue = ((DataRowView)(cONDOMINIOSBindingSource.Current))["CON"];
        }

        private void lkpBloCodigo_EditValueChanged(object sender, EventArgs e)
        {
            if (sender is DevExpress.XtraEditors.LookUpEdit)
            {
                lkpBloCodigo.EditValue = (sender as DevExpress.XtraEditors.LookUpEdit).EditValue;
                lkpBloNome.EditValue = (sender as DevExpress.XtraEditors.LookUpEdit).EditValue;

                if (fKBLOCOSCONDOMINIOSBindingSource.SupportsSearching)
                {
                    Int32 _FoundIndex = fKBLOCOSCONDOMINIOSBindingSource.Find("BLO", lkpBloCodigo.EditValue);
                    fKBLOCOSCONDOMINIOSBindingSource.Position = _FoundIndex;
                }
            }
        }

        private void lkpConCodigo_EditValueChanged(object sender, EventArgs e)
        {
            if (sender is DevExpress.XtraEditors.LookUpEdit)
            {
                lkpConCodigo.EditValue = (sender as DevExpress.XtraEditors.LookUpEdit).EditValue;
                lkpConNome.EditValue = (sender as DevExpress.XtraEditors.LookUpEdit).EditValue;
            }
        }

        private void lkpApto_EditValueChanged(object sender, EventArgs e)
        {
           
            if ( (lkpApto.Text.Trim() != "") && ((lkpBloCodigo.EditValue == null)||(lkpBloNome.EditValue == null)))
            {
                lkpBloCodigo.EditValue = ((DataRowView)(fKAPARTAMENTOSBLOCOSBindingSource.Current))["APT_BLO"];
            }
        }

        private void lkpPessoa_Enter(object sender, EventArgs e)
        {
            //pESSOASTableAdapter.Fill(dCondominiosCampos.PESSOAS);

            /* Filtrando binding para mostrar apenas pessoas envolvidas no apto */           
            String _Filter = "";

            String _Proprietario = ((DataRowView)(fKAPARTAMENTOSBLOCOSBindingSource.Current))["APTProprietario_PES"].ToString();
            String _Inquilino = ((DataRowView)(fKAPARTAMENTOSBLOCOSBindingSource.Current))["APTInquilino_PES"].ToString();
            String _Pessoa = ((DataRowView)(BindingSource_F.Current))["CDR_PES"].ToString();

            if (_Proprietario.ToString().Trim() != "")
                _Filter += (_Filter.Trim() == "" ? " PES = " + _Proprietario.ToString() : " OR PES = " + _Proprietario.ToString());
            if (_Inquilino.ToString().Trim() != "")
                _Filter += (_Filter.Trim() == "" ? " PES = " + _Inquilino.ToString() : " OR PES = " + _Inquilino.ToString());
            if (_Pessoa.ToString().Trim() != "")
                _Filter += (_Filter.Trim() == "" ? " PES = " + lkpPessoa.EditValue.ToString() : " OR PES = " + lkpPessoa.EditValue.ToString());

            if (_Filter.Trim() != "")
                pESSOASBindingSource.Filter = _Filter;
            else
                lkpPessoa.Properties.Buttons[1].Visible = false;
            
        }

        private void lkpPessoa_ButtonClick(object sender, DevExpress.XtraEditors.Controls.ButtonPressedEventArgs e)
        {
            if (e.Button.Index == 0)
            {
                //pESSOASTableAdapter.Fill(((dCondominiosCampos.PESSOASDataTable)((DataSet)(BindingSource_F.DataSource)).Tables["PESSOAS"]));
            }
            else
                if (e.Button.Index == 1)
                {
                    String _Filter = "";

                    String _Proprietario = ((DataRowView)(fKAPARTAMENTOSBLOCOSBindingSource.Current))["APTProprietario_PES"].ToString();
                    String _Inquilino = ((DataRowView)(fKAPARTAMENTOSBLOCOSBindingSource.Current))["APTInquilino_PES"].ToString();
                    String _Pessoa = ((DataRowView)(BindingSource_F.Current))["CDR_PES"].ToString();

                    if (_Proprietario.ToString().Trim() != "")
                        _Filter += (_Filter.Trim() == "" ? " PES = " + _Proprietario.ToString() : " OR PES = " + _Proprietario.ToString());
                    if (_Inquilino.ToString().Trim() != "")
                        _Filter += (_Filter.Trim() == "" ? " PES = " + _Inquilino.ToString() : " OR PES = " + _Inquilino.ToString());
                    if (_Pessoa.ToString().Trim() != "")
                        _Filter += (_Filter.Trim() == "" ? " PES = " + lkpPessoa.EditValue.ToString() : " OR PES = " + lkpPessoa.EditValue.ToString());

                    if (_Filter.Trim() != "")
                        pESSOASBindingSource.Filter = _Filter;
                    else
                        lkpPessoa.Properties.Buttons[1].Visible = false;
                }

        }

        private void lkpCargos_ButtonClick(object sender, DevExpress.XtraEditors.Controls.ButtonPressedEventArgs e)
        {
            if (e.Button.Index == 1) //Incluir
            {
                int pk = ShowModuloAdd(typeof(cCargosGrade));

                if (pk > 0)
                {
                    cARGOSTableAdapter.Fill(dCondominiosCampos.CARGOS);

                    lkpCargos.EditValue = pk;
                }
            }
        }
    }
}

