using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace Cadastros.Condominios
{
    public partial class cContas : CompontesBasicos.ComponenteCamposBase
    {
        public cContas()
        {
            InitializeComponent();
        }

        protected override void btnConfirmar_F_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            try
            {
                this.Validate();

                this.BindingSource_F.EndEdit();

                FechaTela(DialogResult.OK);
            }
            catch (NoNullAllowedException)
            {
                MessageBox.Show("Existem campo obrigat�rio sem um valor informado.",
                                "Campo obrigat�rio", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }
            catch (ConstraintException)
            {
                MessageBox.Show("J� existe uma outra conta para este condom�nio com o nome informado.",
                                "Conta Duplicada", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }
        }

        protected override void btnCancelar_F_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            this.BindingSource_F.CancelEdit();
            
            FechaTela(DialogResult.Cancel);
        }

    }
}

