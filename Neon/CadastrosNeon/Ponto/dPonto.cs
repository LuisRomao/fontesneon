﻿namespace Ponto {


    partial class dPonto
    {
        private static dPonto _dPontoSt;

        /// <summary>
        /// dataset estático:dPonto
        /// </summary>
        public static dPonto dPontoSt
        {
            get
            {
                if (_dPontoSt == null)
                    _dPontoSt = new dPonto();
                return _dPontoSt;
            }
        }


        private dPontoTableAdapters.PONtoTableAdapter pONtoTableAdapter;
        /// <summary>
        /// TableAdapter padrão para tabela: PONto
        /// </summary>
        public dPontoTableAdapters.PONtoTableAdapter PONtoTableAdapter
        {
            get
            {
                if (pONtoTableAdapter == null)
                {
                    pONtoTableAdapter = new dPontoTableAdapters.PONtoTableAdapter();
                    pONtoTableAdapter.TrocarStringDeConexao();
                };
                return pONtoTableAdapter;
            }
        }

        private dPontoTableAdapters.DIGitaisTableAdapter dIGitaisTableAdapter;
        /// <summary>
        /// TableAdapter padrão para tabela: DIGitais
        /// </summary>
        public dPontoTableAdapters.DIGitaisTableAdapter DIGitaisTableAdapter
        {
            get
            {
                if (dIGitaisTableAdapter == null)
                {
                    dIGitaisTableAdapter = new dPontoTableAdapters.DIGitaisTableAdapter();
                    dIGitaisTableAdapter.TrocarStringDeConexao();
                };
                return dIGitaisTableAdapter;
            }
        }
    }
}
