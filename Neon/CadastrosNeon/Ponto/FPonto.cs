using System;
using System.Windows.Forms;
using CompontesBasicos;
using Digital;

namespace Ponto
{
    /// <summary>
    /// 
    /// </summary>
    public partial class FPonto : FormBase
    {
        private static FPonto _FPontoST;

        /// <summary>
        /// Instancia est�tica
        /// </summary>
        public static FPonto FPontoST
        {
            get 
            {
                return _FPontoST ?? (_FPontoST = new FPonto());
            }
        }

        //public string erro = null;

        /// <summary>
        /// 
        /// </summary>
        public FPonto()
        {
            //MessageBox.Show("1");
            //MessageBox.Show(erro.ToString());
            InitializeComponent();
            dPonto.dPontoSt.DIGitaisTableAdapter.Fill(dPonto.dPontoSt.DIGitais);
            FFinger.FFingerSt.SetBancoInterno(dPonto.dPontoSt.DIGitais);
            FFinger.FFingerSt.DigitalLida += new System.EventHandler(LeituraFeita);
        }

        DateTime Desligar;

        private void timer1_Tick(object sender, EventArgs e)
        {
            Lhora.Text = DateTime.Now.ToString("hh:mm:ss");
            LData.Text = DateTime.Now.Date.ToString("dd/MM/yyyy");
            if (DateTime.Now > Desligar)
            {
                if (!Focused)
                    FFinger.FFingerSt.Ligar(false);
                ResetTempoDesligar();
            }
        }

        private void ResetTempoDesligar()
        {
            Desligar = DateTime.Now.AddMinutes(5);
        }

        private fRetorno _FRetorno;
        private fRetorno FRetorno
        {
            get
            {
                //if (_FRetorno == null)
                //{
                    _FRetorno = new fRetorno();
                    _FRetorno.bindingSourcePonto.DataSource = dPonto.dPontoSt;
                //};
                return _FRetorno;
            }
        }

        private void LeituraFeita(object sender, EventArgs e) {
            ResetTempoDesligar();
            int USU = Digital.FFinger.FFingerSt.Busca();
            if (USU != -1)
            {
                labelControl1.Text = "...";
                DateTime Agora = DateTime.Now;
                //VirMSSQL.TableAdapter.STTableAdapter.BuscaEscalar("select getdate()", out Agora);
                string Campo;
                string Justificativa = "";
                dPonto.PONtoRow PONrow;
                int lidos = dPonto.dPontoSt.PONtoTableAdapter.Fill(dPonto.dPontoSt.PONto, USU);
                if (lidos == 0)
                {                    
                    VirMSSQL.TableAdapter.STTableAdapter.ExecutarSQLNonQuery("insert into PONto (PON_USU,PONData) values (@P1, @P2)", USU,Agora.Date);
                    if (dPonto.dPontoSt.PONtoTableAdapter.Fill(dPonto.dPontoSt.PONto, USU) == 0)
                        return;

                    PONrow = dPonto.dPontoSt.PONto[0];
                    Campo = "PONEntrada";
                }
                else
                {
                    PONrow = dPonto.dPontoSt.PONto[0];
                    if (PONrow.IsPONEntradaNull())
                        Campo = "PONEntrada";
                    else
                        if (PONrow.IsPONAlmocoNull())
                        {
                            Campo = "PONAlmoco";
                            if (PONrow.PONEntrada.AddMinutes(15) > Agora)
                                Campo = "";
                        }
                        else
                            if (PONrow.IsPONAlmocoRetNull())
                            {
                                Campo = "PONAlmocoRet";
                                if (PONrow.PONAlmoco.AddMinutes(15) > Agora)
                                    Campo = "";
                            }
                            else
                                if (PONrow.IsPONSaidaNull())
                                {
                                    Campo = "PONSaida";
                                    if (PONrow.PONAlmocoRet.AddMinutes(15) > Agora)
                                        Campo = "";
                                }
                                else
                                    Campo = "";
                };
                if (Campo != "")
                {
                    VirMSSQL.TableAdapter.STTableAdapter.ExecutarSQLNonQuery("update PONto set " + Campo + " = @P1 where PON = @P2",Agora, PONrow.PON);
                    dPonto.dPontoSt.PONtoTableAdapter.Fill(dPonto.dPontoSt.PONto, USU);
                    PONrow = dPonto.dPontoSt.PONto[0];
                }
                if (Campo == "PONEntrada")
                {                    
                    
                    if ((Campo == "PONEntrada") && (PONrow.PONEntrada > DateTime.Today.AddHours(8).AddMinutes(15)))
                    {
                        VirInput.Input.Execute("Justificativa", ref Justificativa, true);
                        if (Justificativa != "")
                        {
                            TimeSpan Atraso = Agora - DateTime.Today.AddHours(8);
                            Justificativa = "ATRASO " + Atraso.Hours.ToString("00") + ":" + Atraso.Minutes.ToString("00") + " minutos\r\n" + Justificativa;
                            if (!PONrow.IsPONJustificativaNull())
                                Justificativa = PONrow.PONJustificativa + "\r\n" + Justificativa;
                            VirMSSQL.TableAdapter.STTableAdapter.ExecutarSQLNonQuery("update PONto set PONJustificativa = @P1 where PON = @P2", Justificativa, PONrow.PON);
                            //dPonto.dPontoSt.PONtoTableAdapter.Fill(dPonto.dPontoSt.PONto, USU);
                            //PONrow = dPonto.dPontoSt.PONto[0];
                        }
                    }
                }
                FRetorno.Show();
            }
            else
                labelControl1.Text = string.Format("{0:mm:ss} N�o encontrado",DateTime.Now);
        }


        private bool Logando = false;

        private void timerTiraQuarentena_Tick(object sender, EventArgs e)
        {            
            FFinger.FFingerSt.Ligar(true);
            Logando = false;
            if(FFinger.FFingerSt.Ligado)
                 timerTiraQuarentena.Enabled = false;
        }

        

       

        private void FPonto_Load(object sender, EventArgs e)
        {
            
        }

        private void FPonto_Deactivate(object sender, EventArgs e)
        {            
            ResetTempoDesligar();
        }

        private void FPonto_Activated(object sender, EventArgs e)
        {
            if (!Logando)
            {
                FFinger.FFingerSt.Ligar(true);
                Desligar = DateTime.Now.AddDays(10);
            }
        }

        /// <summary>
        /// Desliga o leitor
        /// </summary>
        public void DesligarLeitor()
        {
            FFinger.FFingerSt.Ligar(false);
            //par nao permitir que o evento de FPonto_Activated ligue novamento
            Logando = true;
        }

        private void FPonto_FormClosed(object sender, FormClosedEventArgs e)
        {
            FFinger.FFingerSt.Ligar(false);
        }

        /// <summary>
        /// Abrea a janela do ponto sem o botao de login
        /// </summary>
        public void ShowPonto()
        {
            panelControl1.Visible = false;
            ControlBox = false;
            Show();
        }

        private void simpleButton1_Click(object sender, EventArgs e)
        {
            try
            {
                FFinger.FFingerSt.Ligar(false);
                Logando = true;
                int USU = FFinger.FFingerSt.Busca();
                if (USU > 0)
                {
                    new Framework.usuarioLogado.UsuarioLogadoNeon(USU);
                    DialogResult = DialogResult.OK;
                    Close();
                }
                else
                {
                    ResetTempoDesligar();
                    FFinger.FFingerSt.Ligar(true);
                }
            }
            finally
            {
                Logando = false;
            }
        }
    }
}