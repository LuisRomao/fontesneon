namespace Ponto
{
    partial class fRetorno
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(fRetorno));
            this.labelControl1 = new DevExpress.XtraEditors.LabelControl();
            this.bindingSourcePonto = new System.Windows.Forms.BindingSource(this.components);
            this.labelControl2 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl3 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl4 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl5 = new DevExpress.XtraEditors.LabelControl();
            this.timeEdit1 = new DevExpress.XtraEditors.TimeEdit();
            this.timeEdit2 = new DevExpress.XtraEditors.TimeEdit();
            this.timeEdit3 = new DevExpress.XtraEditors.TimeEdit();
            this.timeEdit4 = new DevExpress.XtraEditors.TimeEdit();
            this.simpleButton1 = new DevExpress.XtraEditors.SimpleButton();
            this.picFoto = new DevExpress.XtraEditors.PictureEdit();
            this.memoEdit1 = new DevExpress.XtraEditors.MemoEdit();
            this.timer1 = new System.Windows.Forms.Timer(this.components);
            ((System.ComponentModel.ISupportInitialize)(this.bindingSourcePonto)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.timeEdit1.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.timeEdit2.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.timeEdit3.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.timeEdit4.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.picFoto.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.memoEdit1.Properties)).BeginInit();
            this.SuspendLayout();
            // 
            // labelControl1
            // 
            this.labelControl1.Appearance.Font = new System.Drawing.Font("Tahoma", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl1.Appearance.ForeColor = System.Drawing.Color.Blue;
            this.labelControl1.Appearance.Options.UseFont = true;
            this.labelControl1.Appearance.Options.UseForeColor = true;
            this.labelControl1.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.bindingSourcePonto, "USUNome", true));
            this.labelControl1.Location = new System.Drawing.Point(12, 133);
            this.labelControl1.Name = "labelControl1";
            this.labelControl1.Size = new System.Drawing.Size(69, 25);
            this.labelControl1.TabIndex = 0;
            this.labelControl1.Text = "Nome:";
            // 
            // bindingSourcePonto
            // 
            this.bindingSourcePonto.DataMember = "PONto";
            this.bindingSourcePonto.DataSource = typeof(Ponto.dPonto);
            // 
            // labelControl2
            // 
            this.labelControl2.Appearance.Font = new System.Drawing.Font("Tahoma", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl2.Appearance.ForeColor = System.Drawing.Color.Blue;
            this.labelControl2.Appearance.Options.UseFont = true;
            this.labelControl2.Appearance.Options.UseForeColor = true;
            this.labelControl2.Location = new System.Drawing.Point(12, 167);
            this.labelControl2.Name = "labelControl2";
            this.labelControl2.Size = new System.Drawing.Size(91, 25);
            this.labelControl2.TabIndex = 1;
            this.labelControl2.Text = "Entrada:";
            // 
            // labelControl3
            // 
            this.labelControl3.Appearance.Font = new System.Drawing.Font("Tahoma", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl3.Appearance.ForeColor = System.Drawing.Color.Blue;
            this.labelControl3.Appearance.Options.UseFont = true;
            this.labelControl3.Appearance.Options.UseForeColor = true;
            this.labelControl3.Location = new System.Drawing.Point(12, 205);
            this.labelControl3.Name = "labelControl3";
            this.labelControl3.Size = new System.Drawing.Size(85, 25);
            this.labelControl3.TabIndex = 2;
            this.labelControl3.Text = "Almo�o:";
            // 
            // labelControl4
            // 
            this.labelControl4.Appearance.Font = new System.Drawing.Font("Tahoma", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl4.Appearance.ForeColor = System.Drawing.Color.Blue;
            this.labelControl4.Appearance.Options.UseFont = true;
            this.labelControl4.Appearance.Options.UseForeColor = true;
            this.labelControl4.Location = new System.Drawing.Point(12, 243);
            this.labelControl4.Name = "labelControl4";
            this.labelControl4.Size = new System.Drawing.Size(92, 25);
            this.labelControl4.TabIndex = 3;
            this.labelControl4.Text = "Retorno:";
            // 
            // labelControl5
            // 
            this.labelControl5.Appearance.Font = new System.Drawing.Font("Tahoma", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl5.Appearance.ForeColor = System.Drawing.Color.Blue;
            this.labelControl5.Appearance.Options.UseFont = true;
            this.labelControl5.Appearance.Options.UseForeColor = true;
            this.labelControl5.Location = new System.Drawing.Point(12, 281);
            this.labelControl5.Name = "labelControl5";
            this.labelControl5.Size = new System.Drawing.Size(66, 25);
            this.labelControl5.TabIndex = 4;
            this.labelControl5.Text = "Sa�da:";
            // 
            // timeEdit1
            // 
            this.timeEdit1.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.bindingSourcePonto, "PONEntrada", true));
            this.timeEdit1.EditValue = new System.DateTime(2008, 11, 11, 0, 0, 0, 0);
            this.timeEdit1.Location = new System.Drawing.Point(118, 164);
            this.timeEdit1.Name = "timeEdit1";
            this.timeEdit1.Properties.AllowFocused = false;
            this.timeEdit1.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.timeEdit1.Properties.Appearance.Options.UseFont = true;
            this.timeEdit1.Properties.ReadOnly = true;
            this.timeEdit1.Size = new System.Drawing.Size(132, 32);
            this.timeEdit1.TabIndex = 5;
            this.timeEdit1.TabStop = false;
            // 
            // timeEdit2
            // 
            this.timeEdit2.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.bindingSourcePonto, "PONAlmoco", true));
            this.timeEdit2.EditValue = new System.DateTime(2008, 11, 11, 0, 0, 0, 0);
            this.timeEdit2.Location = new System.Drawing.Point(118, 202);
            this.timeEdit2.Name = "timeEdit2";
            this.timeEdit2.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.timeEdit2.Properties.Appearance.Options.UseFont = true;
            this.timeEdit2.Properties.ReadOnly = true;
            this.timeEdit2.Size = new System.Drawing.Size(132, 32);
            this.timeEdit2.TabIndex = 6;
            this.timeEdit2.TabStop = false;
            // 
            // timeEdit3
            // 
            this.timeEdit3.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.bindingSourcePonto, "PONAlmocoRet", true));
            this.timeEdit3.EditValue = new System.DateTime(2008, 11, 11, 0, 0, 0, 0);
            this.timeEdit3.Location = new System.Drawing.Point(118, 240);
            this.timeEdit3.Name = "timeEdit3";
            this.timeEdit3.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.timeEdit3.Properties.Appearance.Options.UseFont = true;
            this.timeEdit3.Properties.ReadOnly = true;
            this.timeEdit3.Size = new System.Drawing.Size(132, 32);
            this.timeEdit3.TabIndex = 7;
            this.timeEdit3.TabStop = false;
            // 
            // timeEdit4
            // 
            this.timeEdit4.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.bindingSourcePonto, "PONSaida", true));
            this.timeEdit4.EditValue = new System.DateTime(2008, 11, 11, 0, 0, 0, 0);
            this.timeEdit4.Location = new System.Drawing.Point(118, 278);
            this.timeEdit4.Name = "timeEdit4";
            this.timeEdit4.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.timeEdit4.Properties.Appearance.Options.UseFont = true;
            this.timeEdit4.Properties.ReadOnly = true;
            this.timeEdit4.Size = new System.Drawing.Size(132, 32);
            this.timeEdit4.TabIndex = 8;
            this.timeEdit4.TabStop = false;
            // 
            // simpleButton1
            // 
            this.simpleButton1.Location = new System.Drawing.Point(12, 428);
            this.simpleButton1.Name = "simpleButton1";
            this.simpleButton1.Size = new System.Drawing.Size(338, 23);
            this.simpleButton1.TabIndex = 9;
            this.simpleButton1.Text = "OK";
            this.simpleButton1.Click += new System.EventHandler(this.simpleButton1_Click);
            // 
            // picFoto
            // 
            this.picFoto.Location = new System.Drawing.Point(127, 3);
            this.picFoto.Name = "picFoto";
            this.picFoto.Properties.SizeMode = DevExpress.XtraEditors.Controls.PictureSizeMode.Zoom;
            this.picFoto.Size = new System.Drawing.Size(96, 122);
            this.picFoto.TabIndex = 18;
            // 
            // memoEdit1
            // 
            this.memoEdit1.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.bindingSourcePonto, "PONJustificativa", true));
            this.memoEdit1.Location = new System.Drawing.Point(12, 316);
            this.memoEdit1.Name = "memoEdit1";
            this.memoEdit1.Properties.AllowFocused = false;
            this.memoEdit1.Properties.ReadOnly = true;
            this.memoEdit1.Size = new System.Drawing.Size(338, 106);
            this.memoEdit1.TabIndex = 19;
            // 
            // timer1
            // 
            this.timer1.Interval = 5000;
            this.timer1.Tick += new System.EventHandler(this.timer1_Tick);
            // 
            // fRetorno
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(362, 463);
            this.ControlBox = false;
            this.Controls.Add(this.memoEdit1);
            this.Controls.Add(this.picFoto);
            this.Controls.Add(this.simpleButton1);
            this.Controls.Add(this.timeEdit4);
            this.Controls.Add(this.timeEdit3);
            this.Controls.Add(this.timeEdit2);
            this.Controls.Add(this.timeEdit1);
            this.Controls.Add(this.labelControl5);
            this.Controls.Add(this.labelControl4);
            this.Controls.Add(this.labelControl3);
            this.Controls.Add(this.labelControl2);
            this.Controls.Add(this.labelControl1);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "fRetorno";
            this.Opacity = 0.9;
            this.ShowInTaskbar = false;
            this.Text = "Apontamentos";
            this.TopMost = true;
            this.Load += new System.EventHandler(this.fRetorno_Load);
            ((System.ComponentModel.ISupportInitialize)(this.bindingSourcePonto)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.timeEdit1.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.timeEdit2.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.timeEdit3.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.timeEdit4.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.picFoto.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.memoEdit1.Properties)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private DevExpress.XtraEditors.LabelControl labelControl1;
        private DevExpress.XtraEditors.LabelControl labelControl2;
        private DevExpress.XtraEditors.LabelControl labelControl3;
        private DevExpress.XtraEditors.LabelControl labelControl4;
        private DevExpress.XtraEditors.LabelControl labelControl5;
        private DevExpress.XtraEditors.TimeEdit timeEdit1;
        private DevExpress.XtraEditors.TimeEdit timeEdit2;
        private DevExpress.XtraEditors.TimeEdit timeEdit3;
        private DevExpress.XtraEditors.TimeEdit timeEdit4;
        /// <summary>
        /// 
        /// </summary>
        public System.Windows.Forms.BindingSource bindingSourcePonto;
        private DevExpress.XtraEditors.SimpleButton simpleButton1;
        private DevExpress.XtraEditors.PictureEdit picFoto;
        private DevExpress.XtraEditors.MemoEdit memoEdit1;
        private System.Windows.Forms.Timer timer1;
    }
}