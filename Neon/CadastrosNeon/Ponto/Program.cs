using System;
using System.Collections.Generic;
using System.Windows.Forms;
using VirDB.Bancovirtual;

namespace Ponto
{
    static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main()
        {
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);

#if (DEBUG)
                    //MessageBox.Show("DEBUG");
                    BancoVirtual.Popular("Lap", @"Data Source=Lapvirtual2017\SQLEXPRESS;Initial Catalog=Neon;Persist Security Info=True;User ID=sa;Password=venus");
                    BancoVirtual.Popular("Lap REAL", @"Data Source=Lapvirtual8\SQLEXPRESS;Initial Catalog=NeonReal;Persist Security Info=True;User ID=sa;Password=venus");
                    //BancoVirtual.Popular("Lap", @"Data Source=Lapvirtual8\SQLEXPRESS;Initial Catalog=Neon;Integrated Security=True;User ID=sa;Password=VENUS");
                    BancoVirtual.Popular("Servidor", @"Data Source=Servidor\SQLEXPRESS;Initial Catalog=Neon;Integrated Security=True;User ID=sa;Password=VENUS");
                    //BancoVirtual.Popular("Lapsa", @"Data Source=Lapvirtual2017\SQLEXPRESS;Initial Catalog=Neon;Persist Security Info=True;User ID=sa;Password=venus");
                    BancoVirtual.Popular("NEON04 (cuidado)", @"Data Source=1.151\SQLEXPRESS;Initial Catalog=Neon;Persist Security Info=True;User ID=sa;Password=7778");
                    BancoVirtual.Popular("NEON (cuidado)", @"Data Source=NEON02\SQLEXPRESS;Initial Catalog=Neon;Persist Security Info=True;User ID=sa;Password=7778");
                    BancoVirtual.PopularAcces("NEON Access (cuidado)", @"Provider=Microsoft.Jet.OLEDB.4.0;Data Source=G:\dados.mdb;Persist Security Info=True;Password=96333018;User ID=NEON;Jet OLEDB:System database=G:\neon.mda;Jet OLEDB:Database Password=96333018");
                    BancoVirtual.PopularAcces("SBC LOCAL", @"Provider=Microsoft.Jet.OLEDB.4.0;Data Source=C:\clientes\neon\sbc\dados.mdb;Persist Security Info=True;Password=96333018;User ID=NEON;Jet OLEDB:System database=C:\clientes\neon\sbc\neon.mda;Jet OLEDB:Database Password=96333018");
                    BancoVirtual.PopularAcces("SA LOCAL", @"Provider=Microsoft.Jet.OLEDB.4.0;Data Source=c:\Clientes\neon\SA\dados.MDB;Persist Security Info=True;User ID=CPD;Password=7778;Jet OLEDB:System database=c:\Clientes\neon\SA\neon.MDA;Jet OLEDB:Database Password=7778");
                    BancoVirtual.PopularAccesH("SBC LOCAL", @"Provider=Microsoft.Jet.OLEDB.4.0;Data Source=C:\clientes\neon\sbc\hist.mdb;Persist Security Info=True;Password=96333018;User ID=NEON;Jet OLEDB:System database=C:\clientes\neon\sbc\neon.mda;Jet OLEDB:Database Password=96333018");
                    BancoVirtual.PopularAccesH("SA LOCAL", @"Provider=Microsoft.Jet.OLEDB.4.0;Data Source=c:\Clientes\neon\SA\hist.MDB;Persist Security Info=True;User ID=CPD;Password=7778;Jet OLEDB:System database=c:\Clientes\neon\SA\neon.MDA;Jet OLEDB:Database Password=7778");
                    BancoVirtual.PopularAccesH("LOCAL", @"Provider=Microsoft.Jet.OLEDB.4.0;Data Source=c:\Clientes\neon\hist.MDB;Persist Security Info=True;User ID=CPD;Password=7778;Jet OLEDB:System database=c:\Clientes\neon\neon.MDA;Jet OLEDB:Database Password=7778");
                    BancoVirtual.PopularAccesH("NEON AccessH", @"Provider=Microsoft.Jet.OLEDB.4.0;Data Source=G:\hist.mdb ;Persist Security Info=True;Password=96333018;User ID=NEON;Jet OLEDB:System database=C:\neon.mda;Jet OLEDB:Database Password=96333018");
                    BancoVirtual.PopularInternet("INTERNET Local", @"Data Source=Lapvirtual8\sqlexpress;Initial Catalog=neoninternet;Integrated Security=True", 1);
                    BancoVirtual.PopularInternet("Lap2", @"Data Source=Lapvirtual8\sqlexpress;Initial Catalog=Neonimoveis_2;Integrated Security=True", 2);

#endif

            if (!System.IO.File.Exists(Application.StartupPath + @"\Neon.NEO"))
            {
                BancoVirtual.Popular("NEON", @"Data Source=NEON02\SQLEXPRESS;Initial Catalog=Neon;Persist Security Info=True;User ID=sa;Connect Timeout=30;Password=7778");                
            }
            else
            {                                                       
                BancoVirtual.Popular("NEON SA REAL", @"Data Source=10.135.1.151\SQLEXPRESS;Initial Catalog=Neon;User ID=sa;Password=7778;Connect Timeout=30");                                
            };

            //Application.ThreadException += new System.Threading.ThreadExceptionEventHandler(TrataErro.OnThreadException);
            //Autenticacao Usuario
            System.Threading.Thread.CurrentThread.CurrentUICulture = new System.Globalization.CultureInfo("pt-BR");


            Application.Run(new FPonto());
        }
    }
}