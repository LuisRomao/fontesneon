namespace Ponto
{
    partial class FPonto
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FPonto));
            this.LData = new DevExpress.XtraEditors.LabelControl();
            this.Lhora = new DevExpress.XtraEditors.LabelControl();
            this.timer1 = new System.Windows.Forms.Timer(this.components);
            this.timerTiraQuarentena = new System.Windows.Forms.Timer(this.components);
            this.labelControl1 = new DevExpress.XtraEditors.LabelControl();
            this.panelControl1 = new DevExpress.XtraEditors.PanelControl();
            this.simpleButton1 = new DevExpress.XtraEditors.SimpleButton();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl1)).BeginInit();
            this.panelControl1.SuspendLayout();
            this.SuspendLayout();
            // 
            // LData
            // 
            this.LData.Appearance.Font = new System.Drawing.Font("Tahoma", 48F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LData.Appearance.ForeColor = System.Drawing.Color.Black;
            this.LData.Appearance.Options.UseFont = true;
            this.LData.Appearance.Options.UseForeColor = true;
            this.LData.Appearance.Options.UseTextOptions = true;
            this.LData.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.LData.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.Vertical;
            this.LData.Dock = System.Windows.Forms.DockStyle.Top;
            this.LData.Location = new System.Drawing.Point(0, 0);
            this.LData.Name = "LData";
            this.LData.Size = new System.Drawing.Size(449, 77);
            this.LData.TabIndex = 5;
            this.LData.Text = "--/--/----";
            // 
            // Lhora
            // 
            this.Lhora.Appearance.Font = new System.Drawing.Font("Tahoma", 48F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Lhora.Appearance.ForeColor = System.Drawing.Color.Red;
            this.Lhora.Appearance.Options.UseFont = true;
            this.Lhora.Appearance.Options.UseForeColor = true;
            this.Lhora.Appearance.Options.UseTextOptions = true;
            this.Lhora.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.Lhora.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.Vertical;
            this.Lhora.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.Lhora.Location = new System.Drawing.Point(0, 78);
            this.Lhora.Name = "Lhora";
            this.Lhora.Size = new System.Drawing.Size(449, 77);
            this.Lhora.TabIndex = 4;
            this.Lhora.Text = "--:--:--";
            // 
            // timer1
            // 
            this.timer1.Enabled = true;
            this.timer1.Interval = 1000;
            this.timer1.Tick += new System.EventHandler(this.timer1_Tick);
            // 
            // timerTiraQuarentena
            // 
            this.timerTiraQuarentena.Enabled = true;
            this.timerTiraQuarentena.Interval = 5000;
            this.timerTiraQuarentena.Tick += new System.EventHandler(this.timerTiraQuarentena_Tick);
            // 
            // labelControl1
            // 
            this.labelControl1.Appearance.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl1.Appearance.ForeColor = System.Drawing.Color.Black;
            this.labelControl1.Appearance.Options.UseFont = true;
            this.labelControl1.Appearance.Options.UseForeColor = true;
            this.labelControl1.Appearance.Options.UseTextOptions = true;
            this.labelControl1.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.labelControl1.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.Vertical;
            this.labelControl1.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.labelControl1.Location = new System.Drawing.Point(0, 155);
            this.labelControl1.Name = "labelControl1";
            this.labelControl1.Size = new System.Drawing.Size(449, 16);
            this.labelControl1.TabIndex = 6;
            this.labelControl1.Text = "...";
            // 
            // panelControl1
            // 
            this.panelControl1.Controls.Add(this.simpleButton1);
            this.panelControl1.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panelControl1.Location = new System.Drawing.Point(0, 171);
            this.panelControl1.Name = "panelControl1";
            this.panelControl1.Size = new System.Drawing.Size(449, 66);
            this.panelControl1.TabIndex = 7;
            // 
            // simpleButton1
            // 
            this.simpleButton1.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.simpleButton1.Appearance.Options.UseFont = true;
            this.simpleButton1.Image = global::Ponto.Properties.Resources.cadeado;
            this.simpleButton1.Location = new System.Drawing.Point(12, 6);
            this.simpleButton1.Name = "simpleButton1";
            this.simpleButton1.Size = new System.Drawing.Size(425, 55);
            this.simpleButton1.TabIndex = 0;
            this.simpleButton1.Text = "Sistema Neon";
            this.simpleButton1.Click += new System.EventHandler(this.simpleButton1_Click);
            // 
            // FPonto
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(449, 237);
            this.Controls.Add(this.Lhora);
            this.Controls.Add(this.labelControl1);
            this.Controls.Add(this.LData);
            this.Controls.Add(this.panelControl1);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "FPonto";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "PONTO";
            this.Deactivate += new System.EventHandler(this.FPonto_Deactivate);
            this.Load += new System.EventHandler(this.FPonto_Load);
            this.Activated += new System.EventHandler(this.FPonto_Activated);
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.FPonto_FormClosed);
            ((System.ComponentModel.ISupportInitialize)(this.panelControl1)).EndInit();
            this.panelControl1.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraEditors.LabelControl LData;
        private DevExpress.XtraEditors.LabelControl Lhora;
        private System.Windows.Forms.Timer timer1;
        private System.Windows.Forms.Timer timerTiraQuarentena;
        private DevExpress.XtraEditors.LabelControl labelControl1;
        private DevExpress.XtraEditors.PanelControl panelControl1;
        private DevExpress.XtraEditors.SimpleButton simpleButton1;

    }
}

