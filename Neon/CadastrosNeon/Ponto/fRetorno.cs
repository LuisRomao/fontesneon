using System;
using System.Data;
using System.Drawing;
using System.IO;
using System.Windows.Forms;

namespace Ponto
{
    /// <summary>
    /// 
    /// </summary>
    public partial class fRetorno : Form
    {
        /// <summary>
        /// 
        /// </summary>
        public fRetorno()
        {
            InitializeComponent();
        }

        private void simpleButton1_Click(object sender, EventArgs e)
        {
            DialogResult = DialogResult.OK;
        }

        private void fRetorno_Load(object sender, EventArgs e)
        {
            DataRowView DRV = (DataRowView)(bindingSourcePonto.Current);
            dPonto.PONtoRow LinhaMae = (dPonto.PONtoRow)DRV.Row;
            if (!LinhaMae.IsUSUFotoNull())
            {
                //Byte[] _arrImage = (Byte[])LinhaMae.USUFoto;
                MemoryStream _picStream = new MemoryStream(LinhaMae.USUFoto);
                picFoto.Image = Image.FromStream(_picStream);
            };
            timer1.Enabled = true;
        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            timer1.Enabled = false;
            Close();
        }
    }
}