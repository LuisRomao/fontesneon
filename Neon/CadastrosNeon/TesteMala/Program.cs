using System;
using System.Collections.Generic;
using System.Windows.Forms;
using VirDB.Bancovirtual;

namespace TesteMala
{
    static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main()
        {
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);

            VirDB.Bancovirtual.BancoVirtual.Popular("Lap", @"Data Source=Lapvirtual8\SQLEXPRESS;Initial Catalog=Neon;Persist Security Info=True;User ID=sa;Password=venus");
            VirDB.Bancovirtual.BancoVirtual.Popular("C�pia Neon", @"Data Source=Lapvirtual8\SQLEXPRESS;Initial Catalog=NeonReal;Persist Security Info=True;User ID=sa;Password=venus");
            VirDB.Bancovirtual.BancoVirtual.PopularMorto("Lap morto", @"Data Source=Lapvirtual8\SQLEXPRESS;Initial Catalog=NeonMorto;Integrated Security=True;User ID=sa;Password=VENUS");


            VirDB.Bancovirtual.BancoVirtual.PopularAcces("SBC LOCAL", @"Provider=Microsoft.Jet.OLEDB.4.0;Data Source=C:\clientes\neon\sbc\dados.mdb;Persist Security Info=True;Password=96333018;User ID=NEON;Jet OLEDB:System database=C:\clientes\neon\sbc\neon.mda;Jet OLEDB:Database Password=96333018");
            VirDB.Bancovirtual.BancoVirtual.PopularAcces("SA LOCAL", @"Provider=Microsoft.Jet.OLEDB.4.0;Data Source=c:\Clientes\neon\SA\dados.MDB;Persist Security Info=True;User ID=CPD;Password=7778;Jet OLEDB:System database=c:\Clientes\neon\SA\neon.MDA;Jet OLEDB:Database Password=7778");
            VirDB.Bancovirtual.BancoVirtual.PopularAcces("LOCAL", @"Provider=Microsoft.Jet.OLEDB.4.0;Data Source=c:\Clientes\neon\dados.MDB;Persist Security Info=True;User ID=CPD;Password=7778;Jet OLEDB:System database=c:\Clientes\neon\neon.MDA;Jet OLEDB:Database Password=7778");

            VirDB.Bancovirtual.BancoVirtual.PopularAccesH("SBC LOCAL", @"Provider=Microsoft.Jet.OLEDB.4.0;Data Source=C:\clientes\neon\sbc\hist.mdb;Persist Security Info=True;Password=96333018;User ID=NEON;Jet OLEDB:System database=C:\clientes\neon\sbc\neon.mda;Jet OLEDB:Database Password=96333018");
            VirDB.Bancovirtual.BancoVirtual.PopularAccesH("SA LOCAL", @"Provider=Microsoft.Jet.OLEDB.4.0;Data Source=c:\Clientes\neon\SA\hist.MDB;Persist Security Info=True;User ID=CPD;Password=7778;Jet OLEDB:System database=c:\Clientes\neon\SA\neon.MDA;Jet OLEDB:Database Password=7778");
            VirDB.Bancovirtual.BancoVirtual.PopularAccesH("LOCAL", @"Provider=Microsoft.Jet.OLEDB.4.0;Data Source=c:\Clientes\neon\hist.MDB;Persist Security Info=True;User ID=CPD;Password=7778;Jet OLEDB:System database=c:\Clientes\neon\neon.MDA;Jet OLEDB:Database Password=7778");
            
            Application.Run(new Form1());
        }
    }
}