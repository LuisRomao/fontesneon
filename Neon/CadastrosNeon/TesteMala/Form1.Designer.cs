namespace TesteMala
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.gridControl1 = new DevExpress.XtraGrid.GridControl();
            this.dadosDestinatariosBindingSource = new System.Windows.Forms.BindingSource();
            this.gridView1 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colCONNome = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colCONEndereco = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colCON = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colBLOCodigo = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colAPTNumero = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colAPTFormaPagtoProprietario_FPG = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colAPTFormaPagtoInquilino_FPG = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colPESNome = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colPESEndereco = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colPESBairro = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colPESCep = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colBOLNome = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colBOLEndereco = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colCIDNome = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colCIDUf = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colAPTProprietario_PES = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colAPTInquilino_PES = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colCorreio = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colBLONome = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colProprietario = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colCONCodigo = new DevExpress.XtraGrid.Columns.GridColumn();
            this.simpleButton1 = new DevExpress.XtraEditors.SimpleButton();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dadosDestinatariosBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).BeginInit();
            this.SuspendLayout();
            // 
            // gridControl1
            // 
            this.gridControl1.DataSource = this.dadosDestinatariosBindingSource;
            this.gridControl1.Location = new System.Drawing.Point(12, 72);
            this.gridControl1.MainView = this.gridView1;
            this.gridControl1.Name = "gridControl1";
            this.gridControl1.Size = new System.Drawing.Size(1092, 491);
            this.gridControl1.TabIndex = 0;
            this.gridControl1.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridView1});
            // 
            // dadosDestinatariosBindingSource
            // 
            this.dadosDestinatariosBindingSource.DataMember = "DadosDestinatarios";
            this.dadosDestinatariosBindingSource.DataSource = typeof(maladireta.dDestinatarios);
            // 
            // gridView1
            // 
            this.gridView1.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colCONNome,
            this.colCONEndereco,
            this.colCON,
            this.colBLOCodigo,
            this.colAPTNumero,
            this.colAPTFormaPagtoProprietario_FPG,
            this.colAPTFormaPagtoInquilino_FPG,
            this.colPESNome,
            this.colPESEndereco,
            this.colPESBairro,
            this.colPESCep,
            this.colBOLNome,
            this.colBOLEndereco,
            this.colCIDNome,
            this.colCIDUf,
            this.colAPTProprietario_PES,
            this.colAPTInquilino_PES,
            this.colCorreio,
            this.colBLONome,
            this.colProprietario,
            this.colCONCodigo});
            this.gridView1.GridControl = this.gridControl1;
            this.gridView1.Name = "gridView1";
            // 
            // colCONNome
            // 
            this.colCONNome.FieldName = "CONNome";
            this.colCONNome.Name = "colCONNome";
            this.colCONNome.Visible = true;
            this.colCONNome.VisibleIndex = 0;
            // 
            // colCONEndereco
            // 
            this.colCONEndereco.FieldName = "CONEndereco";
            this.colCONEndereco.Name = "colCONEndereco";
            this.colCONEndereco.Visible = true;
            this.colCONEndereco.VisibleIndex = 1;
            // 
            // colCON
            // 
            this.colCON.FieldName = "CON";
            this.colCON.Name = "colCON";
            this.colCON.OptionsColumn.ReadOnly = true;
            this.colCON.Visible = true;
            this.colCON.VisibleIndex = 2;
            // 
            // colBLOCodigo
            // 
            this.colBLOCodigo.FieldName = "BLOCodigo";
            this.colBLOCodigo.Name = "colBLOCodigo";
            this.colBLOCodigo.Visible = true;
            this.colBLOCodigo.VisibleIndex = 3;
            // 
            // colAPTNumero
            // 
            this.colAPTNumero.FieldName = "APTNumero";
            this.colAPTNumero.Name = "colAPTNumero";
            this.colAPTNumero.Visible = true;
            this.colAPTNumero.VisibleIndex = 4;
            // 
            // colAPTFormaPagtoProprietario_FPG
            // 
            this.colAPTFormaPagtoProprietario_FPG.FieldName = "APTFormaPagtoProprietario_FPG";
            this.colAPTFormaPagtoProprietario_FPG.Name = "colAPTFormaPagtoProprietario_FPG";
            this.colAPTFormaPagtoProprietario_FPG.Visible = true;
            this.colAPTFormaPagtoProprietario_FPG.VisibleIndex = 5;
            // 
            // colAPTFormaPagtoInquilino_FPG
            // 
            this.colAPTFormaPagtoInquilino_FPG.FieldName = "APTFormaPagtoInquilino_FPG";
            this.colAPTFormaPagtoInquilino_FPG.Name = "colAPTFormaPagtoInquilino_FPG";
            this.colAPTFormaPagtoInquilino_FPG.Visible = true;
            this.colAPTFormaPagtoInquilino_FPG.VisibleIndex = 6;
            // 
            // colPESNome
            // 
            this.colPESNome.FieldName = "PESNome";
            this.colPESNome.Name = "colPESNome";
            this.colPESNome.Visible = true;
            this.colPESNome.VisibleIndex = 7;
            // 
            // colPESEndereco
            // 
            this.colPESEndereco.FieldName = "PESEndereco";
            this.colPESEndereco.Name = "colPESEndereco";
            this.colPESEndereco.Visible = true;
            this.colPESEndereco.VisibleIndex = 8;
            // 
            // colPESBairro
            // 
            this.colPESBairro.FieldName = "PESBairro";
            this.colPESBairro.Name = "colPESBairro";
            this.colPESBairro.Visible = true;
            this.colPESBairro.VisibleIndex = 9;
            // 
            // colPESCep
            // 
            this.colPESCep.FieldName = "PESCep";
            this.colPESCep.Name = "colPESCep";
            this.colPESCep.Visible = true;
            this.colPESCep.VisibleIndex = 10;
            // 
            // colBOLNome
            // 
            this.colBOLNome.FieldName = "BOLNome";
            this.colBOLNome.Name = "colBOLNome";
            this.colBOLNome.OptionsColumn.ReadOnly = true;
            this.colBOLNome.Visible = true;
            this.colBOLNome.VisibleIndex = 11;
            // 
            // colBOLEndereco
            // 
            this.colBOLEndereco.FieldName = "BOLEndereco";
            this.colBOLEndereco.Name = "colBOLEndereco";
            this.colBOLEndereco.Visible = true;
            this.colBOLEndereco.VisibleIndex = 12;
            // 
            // colCIDNome
            // 
            this.colCIDNome.FieldName = "CIDNome";
            this.colCIDNome.Name = "colCIDNome";
            this.colCIDNome.Visible = true;
            this.colCIDNome.VisibleIndex = 13;
            // 
            // colCIDUf
            // 
            this.colCIDUf.FieldName = "CIDUf";
            this.colCIDUf.Name = "colCIDUf";
            this.colCIDUf.Visible = true;
            this.colCIDUf.VisibleIndex = 14;
            // 
            // colAPTProprietario_PES
            // 
            this.colAPTProprietario_PES.FieldName = "APTProprietario_PES";
            this.colAPTProprietario_PES.Name = "colAPTProprietario_PES";
            this.colAPTProprietario_PES.Visible = true;
            this.colAPTProprietario_PES.VisibleIndex = 15;
            // 
            // colAPTInquilino_PES
            // 
            this.colAPTInquilino_PES.FieldName = "APTInquilino_PES";
            this.colAPTInquilino_PES.Name = "colAPTInquilino_PES";
            this.colAPTInquilino_PES.Visible = true;
            this.colAPTInquilino_PES.VisibleIndex = 16;
            // 
            // colCorreio
            // 
            this.colCorreio.FieldName = "Correio";
            this.colCorreio.Name = "colCorreio";
            this.colCorreio.Visible = true;
            this.colCorreio.VisibleIndex = 17;
            // 
            // colBLONome
            // 
            this.colBLONome.FieldName = "BLONome";
            this.colBLONome.Name = "colBLONome";
            this.colBLONome.Visible = true;
            this.colBLONome.VisibleIndex = 18;
            // 
            // colProprietario
            // 
            this.colProprietario.FieldName = "Proprietario";
            this.colProprietario.Name = "colProprietario";
            this.colProprietario.OptionsColumn.ReadOnly = true;
            this.colProprietario.Visible = true;
            this.colProprietario.VisibleIndex = 19;
            // 
            // colCONCodigo
            // 
            this.colCONCodigo.FieldName = "CONCodigo";
            this.colCONCodigo.Name = "colCONCodigo";
            this.colCONCodigo.Visible = true;
            this.colCONCodigo.VisibleIndex = 20;
            // 
            // simpleButton1
            // 
            this.simpleButton1.Location = new System.Drawing.Point(43, 13);
            this.simpleButton1.Name = "simpleButton1";
            this.simpleButton1.Size = new System.Drawing.Size(75, 23);
            this.simpleButton1.TabIndex = 1;
            this.simpleButton1.Text = "simpleButton1";
            this.simpleButton1.Click += new System.EventHandler(this.simpleButton1_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1126, 575);
            this.Controls.Add(this.simpleButton1);
            this.Controls.Add(this.gridControl1);
            this.Name = "Form1";
            this.Text = "Form1";
            ((System.ComponentModel.ISupportInitialize)(this.gridControl1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dadosDestinatariosBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraGrid.GridControl gridControl1;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView1;
        private System.Windows.Forms.BindingSource dadosDestinatariosBindingSource;
        private DevExpress.XtraGrid.Columns.GridColumn colCONNome;
        private DevExpress.XtraGrid.Columns.GridColumn colCONEndereco;
        private DevExpress.XtraGrid.Columns.GridColumn colCON;
        private DevExpress.XtraGrid.Columns.GridColumn colBLOCodigo;
        private DevExpress.XtraGrid.Columns.GridColumn colAPTNumero;
        private DevExpress.XtraGrid.Columns.GridColumn colAPTFormaPagtoProprietario_FPG;
        private DevExpress.XtraGrid.Columns.GridColumn colAPTFormaPagtoInquilino_FPG;
        private DevExpress.XtraGrid.Columns.GridColumn colPESNome;
        private DevExpress.XtraGrid.Columns.GridColumn colPESEndereco;
        private DevExpress.XtraGrid.Columns.GridColumn colPESBairro;
        private DevExpress.XtraGrid.Columns.GridColumn colPESCep;
        private DevExpress.XtraGrid.Columns.GridColumn colBOLNome;
        private DevExpress.XtraGrid.Columns.GridColumn colBOLEndereco;
        private DevExpress.XtraGrid.Columns.GridColumn colCIDNome;
        private DevExpress.XtraGrid.Columns.GridColumn colCIDUf;
        private DevExpress.XtraGrid.Columns.GridColumn colAPTProprietario_PES;
        private DevExpress.XtraGrid.Columns.GridColumn colAPTInquilino_PES;
        private DevExpress.XtraGrid.Columns.GridColumn colCorreio;
        private DevExpress.XtraGrid.Columns.GridColumn colBLONome;
        private DevExpress.XtraGrid.Columns.GridColumn colProprietario;
        private DevExpress.XtraGrid.Columns.GridColumn colCONCodigo;
        private DevExpress.XtraEditors.SimpleButton simpleButton1;
    }
}

