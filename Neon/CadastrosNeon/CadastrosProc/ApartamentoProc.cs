﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using FrameworkProc;
using VirMSSQL;
using AbstratosNeon;
using Abstratos;
using VirEnumeracoesNeon;
using DocBacarios;

namespace CadastrosProc
{    
    /// <summary>
    /// 
    /// </summary>
    public class ApartamentoProc : ABS_Apartamento, IEMPTProc
    {
        private EMPTProc _EMPTProc1;

        /// <summary>
        /// Tipo de EMP: local ou filial
        /// </summary>
        public EMPTProc EMPTProc1
        {
            get
            {
                if (_EMPTProc1 == null)
                {
                    if (TableAdapter.Servidor_De_Processos)
                        throw new Exception("uso de construtor sem EMPTProc em Servidor de Processos");
                    else
                        _EMPTProc1 = new EMPTProc(EMPTipo.Local);
                }
                return _EMPTProc1;
            }
            set => _EMPTProc1 = value;
        }

        protected ABS_Pessoa proprietario;

        protected ABS_Pessoa inquilino;

        private dApartamentos _dApartamentos;

        /// <summary>
        /// 
        /// </summary>
        protected dApartamentos DApartamentos
        {
            get
            {
                if (_dApartamentos == null)
                {
                    if (linhaMae == null)
                        _dApartamentos = new dApartamentos(EMPTProc1);
                    else
                        _dApartamentos = (dApartamentos)linhaMae.Table.DataSet;
                }
                return _dApartamentos;
            }
        }

        /// <summary>
        /// Linha mãe
        /// </summary>
        protected dApartamentos.APARTAMENTOSRow linhaMae;

        /// <summary>
        /// Linha mãe
        /// </summary>
        public dApartamentos.APARTAMENTOSRow LinhaMae
        {
            get { return linhaMae; }
        }        

        #region Construtores
        /// <summary>
        /// Construtor
        /// </summary>
        /// <param name="_EMPTProc"></param>
        public ApartamentoProc(EMPTProc _EMPTProc = null)
        {
            if (_EMPTProc != null)
                EMPTProc1 = _EMPTProc;
        }
        /// <summary>
        /// Construtor para uma nota já existente. verifique depois se "encotrada" está como true
        /// </summary>
        /// <param name="NOA"></param>
        /// <param name="_EMPTProc1"></param>
        public ApartamentoProc(int APT, EMPTProc _EMPTProc1 = null)
            : this(_EMPTProc1)
        {            
            EMPTProc1.EmbarcaEmTrans(DApartamentos.APARTAMENTOSTableAdapter);
            if (DApartamentos.APARTAMENTOSTableAdapter.FillCompleto(DApartamentos.APARTAMENTOS, APT) > 0)
                linhaMae = DApartamentos.APARTAMENTOS[0];
                
        }
        
        /// <summary>
        /// Construtor
        /// </summary>
        /// <param name="_LinhaMae"></param>
        /// <param name="_EMPTProc1"></param>
        public ApartamentoProc(dApartamentos.APARTAMENTOSRow _LinhaMae, EMPTProc _EMPTProc1=null)
            : this(_EMPTProc1)
        {
            linhaMae = _LinhaMae;
        }
        #endregion
        /// <summary>
        /// ID
        /// </summary>       
        public override int APT => LinhaMae.APT; 
         

        public override void AbrecApartamento(bool? Proprietario)
        {
            throw new NotImplementedException();
        }

        public override bool Alugado => throw new NotImplementedException();        

        public override bool APTDiaDiferente => throw new NotImplementedException();

        public override string APTNumero => throw new NotImplementedException();

        public override bool APTSeguro => throw new NotImplementedException();

        public override string APTSenhaInternet(bool Proprietario)
        {
            throw new NotImplementedException();
        }

        public override string APTUsuarioInternet(bool Proprietario)
        {
            throw new NotImplementedException();
        }

        public override int BLO => throw new NotImplementedException();

        public override string BLOCodigo => throw new NotImplementedException();

        public override string BLONome => throw new NotImplementedException();

        public override int CON => throw new NotImplementedException();

        public override string CONCodigo => throw new NotImplementedException();

        public override ABS_Condominio Condominio { get => base.Condominio; set => base.Condominio = value; }

        public override ABS_ContaCorrente ContaDebito(bool Proprietario)
        {
            throw new NotImplementedException();
        }

        public override DateTime DataVencimento(ABS_Competencia competencia)
        {
            throw new NotImplementedException();
        }

        public override string EmailResponsavel => throw new NotImplementedException();

        public override bool encontrado => throw new NotImplementedException();

        public override FormaPagamento FormaPagamentoEfetivo(FormaPagamento? FormaSolicitada, bool Proprietario, bool ForcaPapel)
        {
            throw new NotImplementedException();
        }

        public override TipoPagIP InquilinoPagaRateio => throw new NotImplementedException();

        public override string PESCEP(FormaPagamento? FormaSolicitada, bool Proprietario)
        {
            throw new NotImplementedException();
        }

        public override CPFCNPJ PESCpfCnpj(bool Proprietario)
        {
            throw new NotImplementedException();
        }

        public override string PESEmail(bool Proprietario)
        {
            throw new NotImplementedException();
        }

        public override string PESNome(bool Proprietario)
        {
            throw new NotImplementedException();
        }

        public override string PESEndereco(FormaPagamento? FormaSolicitada, bool Proprietario, bool ForcaPapel)
        {
            throw new NotImplementedException();
        }

        public override int PES_INSS => throw new NotImplementedException();

        public override bool QuerEmail(FormaPagamento? FormaSolicitada, bool Proprietario)
        {
            throw new NotImplementedException();
        }

        public override bool QuerImpresso(FormaPagamento? FormaSolicitada, bool Proprietario)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// Status da cobrança
        /// </summary>
        /// <returns></returns>
        public override APTStatusCobranca StatusCobranca
        {
            get => (APTStatusCobranca)linhaMae.APTStatusCobranca;
            set
            {
                if (linhaMae.APTStatusCobranca != (int)value)
                {
                    EMPTProc1.EmbarcaEmTrans(DApartamentos.APARTAMENTOSTableAdapter);
                    linhaMae.APTStatusCobranca = (int)value;
                    DApartamentos.APARTAMENTOSTableAdapter.Update(linhaMae);
                }
            }
        }

        public override ABS_Pessoa Proprietario => throw new NotImplementedException();

        public override ABS_Pessoa Inquilino => throw new NotImplementedException();


    }

}
