﻿using FrameworkProc;

namespace CadastrosProc.Faturamento
{


    partial class dInsumos : IEMPTProc
    {

        private EMPTProc _EMPTProc1;

        /// <summary>
        /// Tipo de EMP: local ou filial
        /// </summary>
        public EMPTProc EMPTProc1
        {
            get
            {
                if (_EMPTProc1 == null)
                    _EMPTProc1 = new EMPTProc(EMPTipo.Local);
                return _EMPTProc1;
            }
            set
            {
                _EMPTProc1 = value;
            }
        }

        /// <summary>
        /// Construtor com EMPTipo
        /// </summary>
        /// <param name="_EMPTProc1"></param>
        public dInsumos(EMPTProc _EMPTProc1)
        : this()
        {
            if (_EMPTProc1 == null)
            {
                if (VirMSSQL.TableAdapter.Servidor_De_Processos)
                    throw new System.Exception("uso de construtor sem EMPTProc em Servidor de Processos");
                EMPTProc1 = new EMPTProc(EMPTipo.Local);
            }
            else
                EMPTProc1 = _EMPTProc1;
        }

        #region Adapters        

        private dInsumosTableAdapters.INSumosTableAdapter iNSumosTableAdapter;
        /// <summary>
        /// TableAdapter padrão para tabela: INSumos
        /// </summary>
        public dInsumosTableAdapters.INSumosTableAdapter INSumosTableAdapter
        {
            get
            {
                if (iNSumosTableAdapter == null)
                {
                    iNSumosTableAdapter = new dInsumosTableAdapters.INSumosTableAdapter();
                    iNSumosTableAdapter.TrocarStringDeConexao(EMPTProc1.TBanco);
                };
                return iNSumosTableAdapter;
            }
        }

        private dInsumosTableAdapters.REPassesTableAdapter rEPassesTableAdapter;
        /// <summary>
        /// TableAdapter padrão para tabela: REPasses
        /// </summary>
        public dInsumosTableAdapters.REPassesTableAdapter REPassesTableAdapter
        {
            get
            {
                if (rEPassesTableAdapter == null)
                {
                    rEPassesTableAdapter = new dInsumosTableAdapters.REPassesTableAdapter();
                    rEPassesTableAdapter.TrocarStringDeConexao(EMPTProc1.TBanco);
                };
                return rEPassesTableAdapter;
            }
        }

        private dInsumosTableAdapters.PLAnocontasTableAdapter pLAnocontasTableAdapter;
        /// <summary>
        /// TableAdapter padrão para tabela: PLAnocontas
        /// </summary>
        public dInsumosTableAdapters.PLAnocontasTableAdapter PLAnocontasTableAdapter
        {
            get
            {
                if (pLAnocontasTableAdapter == null)
                {
                    pLAnocontasTableAdapter = new dInsumosTableAdapters.PLAnocontasTableAdapter();
                    pLAnocontasTableAdapter.TrocarStringDeConexao(EMPTProc1.TBanco);
                };
                return pLAnocontasTableAdapter;
            }
        }

        #endregion

    }
}
