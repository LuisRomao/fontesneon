﻿using FrameworkProc;
using System;


namespace CadastrosProc
{


    partial class dApartamentos : IEMPTProc
    {
        private EMPTProc _EMPTProc1;

        /// <summary>
        /// Tipo de EMP: local ou filial
        /// </summary>
        public EMPTProc EMPTProc1
        {
            get
            {
                if (_EMPTProc1 == null)
                    _EMPTProc1 = new EMPTProc(FrameworkProc.EMPTipo.Local);
                return _EMPTProc1;
            }
            set
            {
                _EMPTProc1 = value;
            }
        }

        /// <summary>
        /// Construtor com EMPTipo
        /// </summary>
        /// <param name="_EMPTProc1"></param>
        public dApartamentos(EMPTProc _EMPTProc1)
        : this()
        {
            if (_EMPTProc1 == null)
            {
                if (VirMSSQL.TableAdapter.Servidor_De_Processos)
                    throw new Exception("uso de construtor sem EMPTProc em Servidor de Processos");
                EMPTProc1 = new EMPTProc(EMPTipo.Local);
            }
            else
                EMPTProc1 = _EMPTProc1;
        }

        #region Adapters

        private dApartamentosTableAdapters.APARTAMENTOSTableAdapter aPARTAMENTOSTableAdapter;
        /// <summary>
        /// TableAdapter padrão para tabela: APARTAMENTOS
        /// </summary>
        public dApartamentosTableAdapters.APARTAMENTOSTableAdapter APARTAMENTOSTableAdapter
        {
            get
            {
                if (aPARTAMENTOSTableAdapter == null)
                {
                    aPARTAMENTOSTableAdapter = new dApartamentosTableAdapters.APARTAMENTOSTableAdapter();
                    aPARTAMENTOSTableAdapter.TrocarStringDeConexao(EMPTProc1.TBanco);
                };
                return aPARTAMENTOSTableAdapter;
            }
        }

        private dApartamentosTableAdapters.PessoasTableAdapter pessoasTableAdapter;
        /// <summary>
        /// TableAdapter padrão para tabela: Pessoas
        /// </summary>
        public dApartamentosTableAdapters.PessoasTableAdapter PessoasTableAdapter
        {
            get
            {
                if (pessoasTableAdapter == null)
                {
                    pessoasTableAdapter = new dApartamentosTableAdapters.PessoasTableAdapter();
                    pessoasTableAdapter.TrocarStringDeConexao(EMPTProc1.TBanco);
                };
                return pessoasTableAdapter;
            }
        }

        private dApartamentosTableAdapters.CORPODIRETIVOTableAdapter cORPODIRETIVOTableAdapter;
        /// <summary>
        /// TableAdapter padrão para tabela: CORPODIRETIVO
        /// </summary>
        public dApartamentosTableAdapters.CORPODIRETIVOTableAdapter CORPODIRETIVOTableAdapter
        {
            get
            {
                if (cORPODIRETIVOTableAdapter == null)
                {
                    cORPODIRETIVOTableAdapter = new dApartamentosTableAdapters.CORPODIRETIVOTableAdapter();
                    cORPODIRETIVOTableAdapter.TrocarStringDeConexao(EMPTProc1.TBanco);
                };
                return cORPODIRETIVOTableAdapter;
            }
        }

        private dApartamentosTableAdapters.APTPGETableAdapter aPTPGETableAdapter;
        /// <summary>
        /// TableAdapter padrão para tabela: APTPGE
        /// </summary>
        public dApartamentosTableAdapters.APTPGETableAdapter APTPGETableAdapter
        {
            get
            {
                if (aPTPGETableAdapter == null)
                {
                    aPTPGETableAdapter = new dApartamentosTableAdapters.APTPGETableAdapter();
                    aPTPGETableAdapter.TrocarStringDeConexao(EMPTProc1.TBanco);
                };
                return aPTPGETableAdapter;
            }
        }

        private dApartamentosTableAdapters.APartamentoDocTableAdapter aPartamentoDocTableAdapter;
        /// <summary>
        /// TableAdapter padrão para tabela: APartamentoDoc
        /// </summary>
        public dApartamentosTableAdapters.APartamentoDocTableAdapter APartamentoDocTableAdapter
        {
            get
            {
                if (aPartamentoDocTableAdapter == null)
                {
                    aPartamentoDocTableAdapter = new dApartamentosTableAdapters.APartamentoDocTableAdapter();
                    aPartamentoDocTableAdapter.TrocarStringDeConexao(EMPTProc1.TBanco);
                };
                return aPartamentoDocTableAdapter;
            }
        }

        private dApartamentosTableAdapters.CodConBlocoTableAdapter codConBlocoTableAdapter;
        /// <summary>
        /// TableAdapter padrão para tabela: CodConBloco
        /// </summary>
        public dApartamentosTableAdapters.CodConBlocoTableAdapter CodConBlocoTableAdapter
        {
            get
            {
                if (codConBlocoTableAdapter == null)
                {
                    codConBlocoTableAdapter = new dApartamentosTableAdapters.CodConBlocoTableAdapter();
                    codConBlocoTableAdapter.TrocarStringDeConexao(EMPTProc1.TBanco);
                };
                return codConBlocoTableAdapter;
            }
        }

        #endregion

    }

}
