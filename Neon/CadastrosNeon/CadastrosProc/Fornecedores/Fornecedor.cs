﻿using System;
using Abstratos;
using AbstratosNeon;
using DocBacarios;
using VirEnumeracoes;
using VirEnumeracoesNeon;
using FrameworkProc;


namespace CadastrosProc.Fornecedores
{

    /// <summary>
    /// Fornecedor
    /// </summary>
    public class FornecedorProc : ABS_Fornecedor, IEMPTProc
    {

        private EMPTProc _EMPTProc1;

        /// <summary>
        /// Tipo de EMP: local ou filial
        /// </summary>
        public EMPTProc EMPTProc1
        {
            get
            {
                if (_EMPTProc1 == null)
                    _EMPTProc1 = new EMPTProc(FrameworkProc.EMPTipo.Local);
                return _EMPTProc1;
            }
            set
            {
                _EMPTProc1 = value;
            }
        }

        /// <summary>
        /// Código
        /// </summary>
        public override int FRN { get { return FRNrow.FRN; } }

        
        private dFornecedoresCampos.FORNECEDORESRow FRNrow;
        private dFornecedoresCampos dFornecedoresCampos;

        /// <summary>
        /// Coletivo significa que o dataset veio de fora e não temos total controle sobre ele para fazer fill apenas para inclusão e consulta
        /// </summary>
        private bool Coletivo = false;

        #region Construtores
        /// <summary>
        /// Construtor
        /// </summary>
        /// <param name="_EMPTProc"></param>
        public FornecedorProc(EMPTProc _EMPTProc = null)
        {
            if (_EMPTProc != null)
                EMPTProc1 = _EMPTProc;
            else
            {
                if (VirMSSQL.TableAdapter.Servidor_De_Processos)
                    throw new Exception("uso de construtor sem EMPTProc em Servidor de Processos");
            }
        }

        public bool encontrdo => FRNrow != null;

        /// <summary>
        /// Construtor
        /// </summary>
        /// <param name="_FRN"></param>
        /// <param name="_EMPTProc"></param>
        public FornecedorProc(int _FRN, EMPTProc _EMPTProc1 = null) : this(_EMPTProc1)
        {
            dFornecedoresCampos = new dFornecedoresCampos(_EMPTProc1);
            EMPTProc1.EmbarcaEmTrans(dFornecedoresCampos.FORNECEDORESTableAdapter);
            if (dFornecedoresCampos.FORNECEDORESTableAdapter.Fill(dFornecedoresCampos.FORNECEDORES, _FRN) > 0)
                FRNrow = dFornecedoresCampos.FORNECEDORES[0];
        }

        /// <summary>
        /// Construtor
        /// </summary>
        /// <param name="_FRNrow"></param>
        /// <param name="_EMPTProc"></param>
        public FornecedorProc(dFornecedoresCampos.FORNECEDORESRow _FRNrow, EMPTProc _EMPTProc1 = null) : this(_EMPTProc1)
        {
            FRNrow = _FRNrow;
            dFornecedoresCampos = (dFornecedoresCampos)FRNrow.Table.DataSet;
            Coletivo = true;
        } 
        #endregion

        /// <summary>
        /// Cria um objeto
        /// </summary>
        /// <param name="CNPJ"></param>        
        /// <returns></returns>
        public static FornecedorProc GetFornecedorProc(CPFCNPJ CNPJ)
        {
            dFornecedoresCampos dFornecedoresCampos = new dFornecedoresCampos();
            if (dFornecedoresCampos.FORNECEDORESTableAdapter.FillByCNPJ(dFornecedoresCampos.FORNECEDORES, CNPJ.ToString()) == 0)
                return null;
            else
                return new FornecedorProc(dFornecedoresCampos.FORNECEDORES[0]);
        }

        /// <summary>
        /// Nome do Fornecedor
        /// </summary>
        public override string FRNNome
        {
            get
            {
                return FRNrow.FRNNome;
            }
        }

        private CPFCNPJ _CNPJ;

        /// <summary>
        /// CNPJ
        /// </summary>
        public override CPFCNPJ CNPJ
        {
            get
            {
                if(_CNPJ == null)
                    _CNPJ = new CPFCNPJ(FRNrow.FRNCnpj);
                return _CNPJ;
            }
        }

        /// <summary>
        /// Endereço
        /// </summary>
        public override string Endereco(ModeloEndereco Mod)
        {
            switch (Mod)
            {
                case ModeloEndereco.UmaLinha:                   
                case ModeloEndereco.DuasLinhas:                    
                case ModeloEndereco.TresLinhas:
                    if (FRNrow.IsFRNCepNull() || FRNrow.IsCIDNomeNull() || FRNrow.IsCIDUfNull())
                        return "";
                    else
                        return String.Format("{0} {2}\r\n{3} - {4}\r\nCEP: {1}", FRNrow.FRNEndereco, FRNrow.FRNCep, FRNrow.FRNBairro, FRNrow.CIDNome, FRNrow.CIDUf);
                default:
                    return "";
            }            
        }

        private ContaCorrenteNeon ContaPadrao;

        /// <summary>
        /// Conta
        /// </summary>
        public ContaCorrenteNeon Conta
        {
            get
            {
                if (ContaPadrao == null)
                {                    
                    ContaPadrao = new ContaCorrenteNeon(FRN, TipoChaveContaCorrenteNeon.FRN);                    
                }
                return ContaPadrao;
            }
        }

        /// <summary>
        /// Conta do fornecedor
        /// </summary>
        public override ABS_ContaCorrente ABSConta
        {
            get
            {                
                return Conta;
            }
        }

        public bool incluiRAV(RAVPadrao RAVP)
        {
            return incluiRAV((int)RAVP);
        }

        public bool incluiRAV(int RAV)
        {
            EMPTProc1.EmbarcaEmTrans(dFornecedoresCampos.FRNxRAVTableAdapter);            
            if (!ContemRAV(RAV))
            {
                dFornecedoresCampos.FRNxRAVRow novarow = dFornecedoresCampos.FRNxRAV.NewFRNxRAVRow();
                novarow.FRN = FRN;
                novarow.RAV = RAV;
                dFornecedoresCampos.FRNxRAV.AddFRNxRAVRow(novarow);                                                    
                dFornecedoresCampos.FRNxRAVTableAdapter.Update(novarow);
                return true;
            }
            return false;
        }

        public bool ContemRAV(RAVPadrao RAVP)
        {
            return ContemRAV((int)RAVP);
        }

        public bool ContemRAV(int RAV)
        {
            if (!Coletivo && (dFornecedoresCampos.FRNxRAV.Count == 0))
            {
                EMPTProc1.EmbarcaEmTrans(dFornecedoresCampos.FRNxRAVTableAdapter);
                dFornecedoresCampos.FRNxRAVTableAdapter.Fill(dFornecedoresCampos.FRNxRAV, FRN);
            }
            return (dFornecedoresCampos.FRNxRAV.FindByFRNRAV(FRN, RAV) != null);
        }        

        /// <summary>
        /// Nome do modelo de impresso para procuração
        /// </summary>
        public override string ModeloProcuracao => string.Format("Proc {0}", FRN);

        private bool? _TemProcuracaoCadastrada;       

        /// <summary>
        /// Tem ou não procuração cadastrada
        /// </summary>
        public override bool TemProcuracaoCadastrada
        {
            get
            {
                if (!_TemProcuracaoCadastrada.HasValue)
                {
                    _TemProcuracaoCadastrada = EMPTProc1.STTA.EstaCadastrado("SELECT IMS FROM IMpreSsos WHERE IMSNome = @P1", ModeloProcuracao);
                }
                return _TemProcuracaoCadastrada.Value;
            }
        }
    }
}
