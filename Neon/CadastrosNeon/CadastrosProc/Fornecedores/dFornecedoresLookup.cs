﻿
namespace CadastrosProc.Fornecedores
{


    partial class dFornecedoresLookup
    {
        private dFornecedoresLookupTableAdapters.FRNLookupTableAdapter fRNLookupTableAdapter;
        /// <summary>
        /// TableAdapter padrão para tabela: FRNLookup
        /// </summary>
        public dFornecedoresLookupTableAdapters.FRNLookupTableAdapter FRNLookupTableAdapter
        {
            get
            {
                if (fRNLookupTableAdapter == null)
                {
                    fRNLookupTableAdapter = new dFornecedoresLookupTableAdapters.FRNLookupTableAdapter();
                    fRNLookupTableAdapter.TrocarStringDeConexao();
                };
                return fRNLookupTableAdapter;
            }
        }
    }


}
