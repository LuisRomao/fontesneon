﻿using FrameworkProc;
using System;

namespace CadastrosProc.Fornecedores
{


    partial class dFornecedoresCampos : IEMPTProc
    {
        private EMPTProc _EMPTProc1;

        /// <summary>
        /// Tipo de EMP: local ou filial
        /// </summary>
        public EMPTProc EMPTProc1
        {
            get
            {
                if (_EMPTProc1 == null)
                    _EMPTProc1 = new EMPTProc(FrameworkProc.EMPTipo.Local);
                return _EMPTProc1;
            }
            set
            {
                _EMPTProc1 = value;
            }
        }

        /// <summary>
        /// Construtor
        /// </summary>
        /// <param name="_EMPTProc1"></param>
        public dFornecedoresCampos(EMPTProc _EMPTProc1) : this()
        {
            if (_EMPTProc1 == null)
            {
                if (VirMSSQL.TableAdapter.Servidor_De_Processos)
                    throw new Exception("uso de construtor sem EMPTProc em Servidor de Processos");
                EMPTProc1 = new EMPTProc(EMPTipo.Local);
            }
            else
                EMPTProc1 = _EMPTProc1;
        }

        private dFornecedoresCamposTableAdapters.FORNECEDORESTableAdapter fORNECEDORESTableAdapter;
        /// <summary>
        /// TableAdapter padrão para tabela: FORNECEDORES
        /// </summary>
        public dFornecedoresCamposTableAdapters.FORNECEDORESTableAdapter FORNECEDORESTableAdapter
        {
            get
            {
                if (fORNECEDORESTableAdapter == null)
                {
                    fORNECEDORESTableAdapter = new dFornecedoresCamposTableAdapters.FORNECEDORESTableAdapter();
                    fORNECEDORESTableAdapter.TrocarStringDeConexao(EMPTProc1.TBanco);
                };
                return fORNECEDORESTableAdapter;
            }
        }

        private dFornecedoresCamposTableAdapters.FRNxRAVTableAdapter fRNxRAVTableAdapter;
        /// <summary>
        /// TableAdapter padrão para tabela: FRNxRAV
        /// </summary>
        public dFornecedoresCamposTableAdapters.FRNxRAVTableAdapter FRNxRAVTableAdapter
        {
            get
            {
                if (fRNxRAVTableAdapter == null)
                {
                    fRNxRAVTableAdapter = new dFornecedoresCamposTableAdapters.FRNxRAVTableAdapter();
                    fRNxRAVTableAdapter.TrocarStringDeConexao(EMPTProc1.TBanco);
                };
                return fRNxRAVTableAdapter;
            }
        }
    }
}

namespace CadastrosProc.Fornecedores.dFornecedoresCamposTableAdapters
{

    /// <summary>
    /// 
    /// </summary>
    public partial class FRNxRAVTableAdapter
    {
    }
}
