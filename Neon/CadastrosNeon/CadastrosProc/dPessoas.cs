﻿namespace CadastrosProc
{


    partial class dPessoas
    {
        private dPessoasTableAdapters.PESSOASTableAdapter pESSOASTableAdapter;
        /// <summary>
        /// TableAdapter padrão para tabela: PESSOAS
        /// </summary>
        public dPessoasTableAdapters.PESSOASTableAdapter PESSOASTableAdapter
        {
            get
            {
                if (pESSOASTableAdapter == null)
                {
                    pESSOASTableAdapter = new dPessoasTableAdapters.PESSOASTableAdapter();
                    pESSOASTableAdapter.TrocarStringDeConexao();
                };
                return pESSOASTableAdapter;
            }
        }

        private dPessoasTableAdapters.CIDADESTableAdapter cIDADESTableAdapter;
        /// <summary>
        /// TableAdapter padrão para tabela: CIDADES
        /// </summary>
        public dPessoasTableAdapters.CIDADESTableAdapter CIDADESTableAdapter
        {
            get
            {
                if (cIDADESTableAdapter == null)
                {
                    cIDADESTableAdapter = new dPessoasTableAdapters.CIDADESTableAdapter();
                    cIDADESTableAdapter.TrocarStringDeConexao();
                };
                return cIDADESTableAdapter;
            }
        }
    }
}
