﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using AbstratosNeon;
using DocBacarios;
using VirEnumeracoesNeon;

namespace CadastrosProc
{
    public class PessoaProc:ABS_Pessoa
    {
        public static PessoaProc GetSindico(int CON)
        {
            PessoaProc retorno = new PessoaProc();
            if(retorno.BuscaSindico(CON))
                return retorno;
            else
                return null;
        }

        public PessoaProc()
        {            
        }

        public PessoaProc(int PES)
        {
            dPessoas.PESSOASTableAdapter.FillByPES(dPessoas.PESSOAS,PES);
            PESrow = dPessoas.PESSOAS[0];
        }

        internal bool BuscaSindico(int CON)
        {
            //int teste = dPessoas.PESSOASTableAdapter.FillBySindico(dPessoas.PESSOAS, CON);
            if (dPessoas.PESSOASTableAdapter.FillBySindico(dPessoas.PESSOAS, CON) == 1)
            {
                PESrow = dPessoas.PESSOAS[0];
                return true;
            }
            return false;
        }

        private dPessoas _dPessoas;

        private dPessoas.PESSOASRow PESrow;

        private dPessoas.CIDADESRow _CIDrow;

        private dPessoas.CIDADESRow CIDrow
        {
            get
            {
                if (_CIDrow == null)
                {
                    if (PESrow.CIDADESRow != null)
                        _CIDrow = PESrow.CIDADESRow;
                    else
                    {
                        dPessoas.CIDADESTableAdapter.ClearBeforeFill = false;
                        dPessoas.CIDADESTableAdapter.Fill(dPessoas.CIDADES, PESrow.PES_CID);
                        if (PESrow.CIDADESRow != null)
                            _CIDrow = PESrow.CIDADESRow;
                    }
                }
                return _CIDrow;
            }
        }

        private dPessoas dPessoas { get => _dPessoas ?? (_dPessoas = new dPessoas()); }

        public override string Nome => PESrow.PESNome;

        public override CPFCNPJ CPF => new CPFCNPJ(PESrow.PESCpfCnpj);

        /// <summary>
        /// Endereço
        /// </summary>
        public override string Endereco(ModeloEndereco Mod)
        {
            if (!PESrow.IsPESEnderecoNull() && !PESrow.IsPESBairroNull() && !PESrow.IsPESCepNull() && !CIDrow.IsCIDUfNull())
                switch (Mod)
                {
                    case ModeloEndereco.UmaLinha:
                        return String.Format("{0} - {1} - {2} {3} - CEP: {4}",
                                              PESrow.PESEndereco,
                                              PESrow.PESBairro,
                                              CIDrow.CIDNome,
                                              CIDrow.CIDUf,
                                              PESrow.PESCep);
                    case ModeloEndereco.DuasLinhas:
                    case ModeloEndereco.TresLinhas:
                        return "";
                    default:
                        return "";
                }
            else
            {
                return "";
            }
        }
    }
}
