﻿using System;
using FrameworkProc;

namespace CadastrosProc
{
    partial class dContaCorrente : IEMPTProc
    {
        private EMPTProc _EMPTProc1;

        /// <summary>
        /// Tipo de EMP: local ou filial
        /// </summary>
        public EMPTProc EMPTProc1
        {
            get
            {
                if (_EMPTProc1 == null)
                    _EMPTProc1 = new EMPTProc(FrameworkProc.EMPTipo.Local);
                return _EMPTProc1;
            }
            set
            {
                _EMPTProc1 = value;
            }
        }

        /// <summary>
        /// Construtor com EMPTipo
        /// </summary>
        /// <param name="_EMPTProc1"></param>
        public dContaCorrente(EMPTProc _EMPTProc1)
        : this()
        {
            if (_EMPTProc1 == null)
            {
                if (VirMSSQL.TableAdapter.Servidor_De_Processos)
                    throw new Exception("uso de construtor sem EMPTProc em Servidor de Processos");
                EMPTProc1 = new EMPTProc(EMPTipo.Local);
            }
            else
                EMPTProc1 = _EMPTProc1;
        }

        private static dContaCorrente _dContaCorrenteSt;

        [Obsolete("Não utilizar como estático")]
        /// <summary>
        /// dataset estático:dContaCorrente
        /// </summary>
        public static dContaCorrente dContaCorrenteSt
        {
            get
            {
                if (_dContaCorrenteSt == null)
                    _dContaCorrenteSt = new dContaCorrente();
                return _dContaCorrenteSt;
            }
        }

        #region Adapters



        private dContaCorrenteTableAdapters.CONDOMINIOSTableAdapter cONDOMINIOSTableAdapter;
        /// <summary>
        /// TableAdapter padrão para tabela: CONDOMINIOS
        /// </summary>
        public dContaCorrenteTableAdapters.CONDOMINIOSTableAdapter CONDOMINIOSTableAdapter
        {
            get
            {
                if (cONDOMINIOSTableAdapter == null)
                {
                    cONDOMINIOSTableAdapter = new dContaCorrenteTableAdapters.CONDOMINIOSTableAdapter();
                    cONDOMINIOSTableAdapter.TrocarStringDeConexao(EMPTProc1.TBanco);
                };
                return cONDOMINIOSTableAdapter;
            }
        }

        private dContaCorrenteTableAdapters.ContaCorrenTeTableAdapter contaCorrenTeTableAdapter;
        /// <summary>
        /// TableAdapter padrão para tabela: ContaCorrenTe
        /// </summary>
        public dContaCorrenteTableAdapters.ContaCorrenTeTableAdapter ContaCorrenTeTableAdapter
        {
            get
            {
                if (contaCorrenTeTableAdapter == null)
                {
                    contaCorrenTeTableAdapter = new dContaCorrenteTableAdapters.ContaCorrenTeTableAdapter();
                    contaCorrenTeTableAdapter.TrocarStringDeConexao(EMPTProc1.TBanco);
                };
                return contaCorrenTeTableAdapter;
            }
        }

        private dContaCorrenteTableAdapters.ContaCorrenteGeralTableAdapter contaCorrenteGeralTableAdapter;
        /// <summary>
        /// TableAdapter padrão para tabela: ContaCorrenteGeral
        /// </summary>
        public dContaCorrenteTableAdapters.ContaCorrenteGeralTableAdapter ContaCorrenteGeralTableAdapter
        {
            get
            {
                if (contaCorrenteGeralTableAdapter == null)
                {
                    contaCorrenteGeralTableAdapter = new dContaCorrenteTableAdapters.ContaCorrenteGeralTableAdapter();
                    contaCorrenteGeralTableAdapter.TrocarStringDeConexao(EMPTProc1.TBanco);
                };
                return contaCorrenteGeralTableAdapter;
            }
        }

        private dContaCorrenteTableAdapters.FORNECEDORESTableAdapter fORNECEDORESTableAdapter;
        /// <summary>
        /// TableAdapter padrão para tabela: FORNECEDORES
        /// </summary>
        public dContaCorrenteTableAdapters.FORNECEDORESTableAdapter FORNECEDORESTableAdapter
        {
            get
            {
                if (fORNECEDORESTableAdapter == null)
                {
                    fORNECEDORESTableAdapter = new dContaCorrenteTableAdapters.FORNECEDORESTableAdapter();
                    fORNECEDORESTableAdapter.TrocarStringDeConexao(EMPTProc1.TBanco);
                };
                return fORNECEDORESTableAdapter;
            }
        }

        #endregion
    }
}
