﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Abstratos;
using AbstratosNeon;
using VirEnumeracoes;
using VirEnumeracoesNeon;
using DocBacarios;
using FrameworkProc;
using VirMSSQL;

namespace CadastrosProc.Condominios
{   
    /// <summary>
    /// Componente representante dos condomínios (Proc)
    /// </summary>    
    public class CondominioProc : ABS_Condominio, IEMPTProc
    {
        private EMPTProc _EMPTProc1;

        /// <summary>
        /// Tipo de EMP: local ou filial
        /// </summary>
        public EMPTProc EMPTProc1
        {
            get
            {
                if (_EMPTProc1 == null)
                {
                    if (TableAdapter.Servidor_De_Processos)
                        throw new Exception("uso de construtor sem EMPTProc em Servidor de Processos");
                    else
                        _EMPTProc1 = new EMPTProc(EMPTipo.Local);
                }
                return _EMPTProc1;
            }
            set => _EMPTProc1 = value;
        }




        private PessoaProc sindico;

        /// <summary>
        /// Sindico
        /// </summary>
        public override ABS_Pessoa Sindico => sindico ?? (sindico = CadastrosProc.PessoaProc.GetSindico(CON));

        /// <summary>
        /// 
        /// </summary>
        private dCondominiosCampos dCondominiosCampos;

        /// <summary>
        /// 
        /// </summary>
        public dCondominiosCampos DCondominiosCampos
        {
            get
            {
                if (dCondominiosCampos == null)
                {
                    if (CONrow == null)
                        dCondominiosCampos = new dCondominiosCampos(EMPTProc1);
                    else
                        dCondominiosCampos = (dCondominiosCampos)CONrow.Table.DataSet;
                }
                return dCondominiosCampos;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        protected dCondominiosCampos.CONDOMINIOSRow CONrow;

        private dCondominiosCampos.CIDADESRow _CIDrow;

        private dCondominiosCampos.CIDADESRow CIDrow
        {
            get
            {
                if (_CIDrow == null)
                {
                    if (CONrow.CIDADESRow != null)
                        _CIDrow = CONrow.CIDADESRow;
                    else
                    {
                        dCondominiosCampos.CIDADESTableAdapter.ClearBeforeFill = false;
                        dCondominiosCampos.CIDADESTableAdapter.Fill(dCondominiosCampos.CIDADES, CONrow.CON_CID);
                        if (CONrow.CIDADESRow != null)
                            _CIDrow = CONrow.CIDADESRow;
                    }
                }
                return _CIDrow;
            }
        }


        #region Construtores

        /// <summary>
        /// Construtor
        /// </summary>
        /// <param name="_EMPTProc"></param>
        public CondominioProc(EMPTProc _EMPTProc = null)
        {
            if (_EMPTProc != null)
                EMPTProc1 = _EMPTProc;
        }

        /// <summary>
        /// Construtor
        /// </summary>
        /// <param name="CON"></param>
        public CondominioProc(int CON, EMPTProc _EMPTProc1 = null)
            : this(_EMPTProc1)
        {
            EMPTProc1.EmbarcaEmTrans(DCondominiosCampos.CONDOMINIOSTableAdapter);
            if (DCondominiosCampos.CONDOMINIOSTableAdapter.Fill(DCondominiosCampos.CONDOMINIOS, CON) > 0)
                CONrow = DCondominiosCampos.CONDOMINIOS[0];
        }

        /// <summary>
        /// Construtor
        /// </summary>        
        public CondominioProc(dCondominiosCampos.CONDOMINIOSRow _CONRow, EMPTProc _EMPTProc1 = null)
            : this(_EMPTProc1)
        {
            CONrow = _CONRow;
            dCondominiosCampos = (dCondominiosCampos)CONrow.Table.DataSet;
        }
        #endregion

        private Fornecedores.FornecedorProc _escritorioAdvocacia;

        /// <summary>
        /// Escritório de advocacia
        /// </summary>
        public Fornecedores.FornecedorProc EscritorioAdvocacia_FornecedorProc
        {
            get
            {                
                if (CONrow.IsCONEscrit_FRNNull())
                    return null;
                else
                    return _escritorioAdvocacia ?? (_escritorioAdvocacia = new Fornecedores.FornecedorProc(CONrow.CONEscrit_FRN));
            }
        }

        /// <summary>
        /// Escritório de advocacia tipado para ABS
        /// </summary>
        public override ABS_Fornecedor EscritorioAdvocacia => EscritorioAdvocacia_FornecedorProc;

        /// <summary>
        /// 
        /// </summary>
        public override decimal CONValorJuros { get { return CONrow.CONValorJuros; } }

        /// <summary>
        /// 
        /// </summary>
        public override decimal CustoSegudaVia { get { return CONrow.CONCustoSegVia; } }

        /// <summary>
        /// 
        /// </summary>
        public override string IdentificacaoApartamento { get { return CONrow.CONNomeApto; } }

        /// <summary>
        /// 
        /// </summary>
        public override string IdentificacaoBloco { get { return CONrow.CONNomeBloco; } }

        /// <summary>
        /// 
        /// </summary>
        public override string Nome { get { return CONrow.CONNome; } }

        /// <summary>
        /// 
        /// </summary>
        public override bool OcultaSenhaBoleto { get { return CONrow.CONOcultaSenha; } }

        /// <summary>
        /// Designação para os apartamentos
        /// </summary>
        public override string CONNomeApto => CONrow.CONNomeApto;

        /// <summary>
        /// Designação para os blocos
        /// </summary>
        public override string CONNomeBloco => CONrow.CONNomeBloco;

        /// <summary>
        /// Endereço
        /// </summary>
        public override string Endereco(ModeloEndereco Mod)
        {
            switch (Mod)
            {
                case ModeloEndereco.UmaLinha_End:
                    return String.Format("{0}, {1}",
                                          CONrow.CONEndereco2,
                                          CONrow.CONNumero);
                case ModeloEndereco.UmaLinha_End_CEP:
                    return String.Format("{0}, {1} CEP {2}",
                                          CONrow.CONEndereco2,
                                          CONrow.CONNumero,                                          
                                          CONrow.CONCep);
                case ModeloEndereco.UmaLinha:
                    return String.Format("{0}, {1} - {2} - {3} {4} - CEP: {5}",
                                          CONrow.CONEndereco2,
                                          CONrow.CONNumero,
                                          CONrow.CONBairro,
                                          CIDrow.CIDNome,
                                          CIDrow.CIDUf,
                                          CONrow.CONCep);
                case ModeloEndereco.DuasLinhas:
                case ModeloEndereco.TresLinhas:
                    return "";
                default:
                    return "";
            }
        }

        /// <summary>
        /// 
        /// </summary>
        public override int? CodigoSeguroConteudo { get { return CONrow.IsCON_PSCNull() ? (int?)null : CONrow.CON_PSC; } }


        /// <summary>
        /// ID
        /// </summary>
        public override int CON => CONrow.CON; 

        /// <summary>
        /// 
        /// </summary>
        public override string CONCodigo { get { return CONrow.CONCodigo; } }

        /// <summary>
        /// 
        /// </summary>
        public override int? CONCodigoFolha1 { get { return CONrow.IsCONCodigoFolha1Null() ? (int?)null : CONrow.CONCodigoFolha1; } }


        /// <summary>
        /// 
        /// </summary>
        public override bool CONDivideSaldos { get { return CONrow.CONDivideSaldos; } }

        /// <summary>
        /// 
        /// </summary>
        public override bool CONJurosCompostos { get { return CONrow.CONJurosCompostos; } }

        /// <summary>
        /// 
        /// </summary>
        public override bool CONJurosPrimeiroMes { get { return CONrow.CONJurosPrimeiroMes; } }

        /// <summary>
        /// Malote
        /// </summary>
        public override int CONMalote => CONrow.CONMalote;

        /// <summary>
        /// Procuração
        /// </summary>
        public override bool CONProcuracao => CONrow.CONProcuracao;
        
        /// <summary>
        /// Status do condominio
        /// </summary>
        public override  CONStatus Status => (CONStatus)CONrow.CONStatus;

        /// <summary>
        /// Apartamentos
        /// </summary>
        public override SortedList<int, ABS_Apartamento> Apartamentos => ABS_Apartamento.ABSGetApartamentos(CON);

        /// <summary>
        /// CNPJ do condomínio
        /// </summary>
        public override CPFCNPJ CNPJ => CONrow.IsCONCnpjNull() ? null : new CPFCNPJ(CONrow.CONCnpj);

        /// <summary>
        /// Conta Corrente do condomínio
        /// </summary>
        public override ABS_ContaCorrente ABSConta => Conta;

        private ContaCorrenteNeon ContaPadrao;

        /// <summary>
        /// 
        /// </summary>        
        /// <returns></returns>
        public ContaCorrenteNeon Conta
        {
            get
            {
                if (ContaPadrao == null)
                {
                    int? CNR = null;
                    if (!CONrow.IsCONCodigoCNRNull() && CONrow.CONCodigoCNR.Trim() != "")
                        CNR = int.Parse(CONrow.CONCodigoCNR);
                    ContaPadrao = new ContaCorrenteNeon(CON, TipoChaveContaCorrenteNeon.CON);                    
                }
                return ContaPadrao;
            }
        }

        protected void CarregaContaLote(dContaCorrente.ContaCorrenTeRow rowCCT)
        {
            ContaPadrao = new ContaCorrenteNeon(rowCCT);
        }

        

    }
}
