﻿/*
LH - 16/04/2014                 - troca o campo CONCodigoFolha2 por CONCodigoFolha2A
                                  remoção de CONCodigoBancoCS
MR - 28/05/2014 12:30           - Inclusão de cadastro de regras para os Equipamento (Alterações indicadas por *** MRC - INICIO - RESERVA (28/05/2014 12:30) ***)
MR - 05/08/2014 11:00           - Inclusão do campo tipo de conta do condomínio (CONTipoContaPE) no CONDOMINIOSTableAdapter
MR - 08/08/2014 11:00           - Inclusão do campo que indica se cheques de valor alto devem ser divididos na nota (CONDivideCheque) no CONDOMINIOSTableAdapter
MR - 26/08/2014 15:00           - Inclusão do campo que indica se o equipamento deve tratar apenas reserva unitaria, ou seja, uma por vez (EQPReservaUnitaria) no EQuiPamentosTableAdapter
MR - 02/09/2014 16:00           - Inclusão do campo que indica a qunatidade máxima de convidados para o uso do espaço a ser usando na montagem da lista (EQPMaxConvidados) no EQuiPamentosTableAdapter
MR - 12/11/2014 10:00           - Inclusão do campo com cod. de comunicacao para tributos, usando no envio de arquivo para Bradesco (CONCodigoComunicacaoTributos) no CONDOMINIOSTableAdapter
MR - 10/02/2015 12:00           - Inclusão do campo com tipo de envio de arquivos para envio de remessa de pagamento eletronico (CONRemessa_TEA) no CONDOMINIOSTableAdapter
MR - 14/01/2016 12:30           - Inclusão do campo com número sequencial de remessa para tributos (CONSequencialRemessaTributos) no CONDOMINIOSTableAdapter
MR - 14/01/2016 12:30           - Inclusão do campo para reserva individual (EQPReservaUnitariaIndividual) no EQuiPamentosTableAdapter
MR - 25/02/2016 11:00           - Inclusão do campo de boleto com registro (CONBoletoComRegistro) no CONDOMINIOSTableAdapter
MR - 15/03/2016 10:00           - Inclusão do campo de tamanho de fonte para quadro de inadimplentes (CONInaTamanhoFonte) no CONDOMINIOSTableAdapter
MR - 15/08/2016 12:30           - Inclusão do campo para prazo minimo de reserva (EQPDiasReservar) no EQuiPamentosTableAdapter
                                  Inclusão do campo para tipo de grade, tipo de cancelamento e tipo de cobrança (EQPTipoGrade, EQPTipoCancelamento, EQPTipoCobranca) no EQuiPamentosTableAdapter
*/

using System;
using FrameworkProc;

namespace CadastrosProc.Condominios
{


    partial class dCondominiosCampos : IEMPTProc
    {
        private EMPTProc _EMPTProc1;

        /// <summary>
        /// Tipo de EMP: local ou filial
        /// </summary>
        public EMPTProc EMPTProc1
        {
            get
            {
                if (_EMPTProc1 == null)
                    _EMPTProc1 = new EMPTProc(FrameworkProc.EMPTipo.Local);
                return _EMPTProc1;
            }
            set
            {
                _EMPTProc1 = value;
            }
        }

        /// <summary>
        /// Construtor com EMPTipo
        /// </summary>
        /// <param name="_EMPTProc1"></param>
        public dCondominiosCampos(EMPTProc _EMPTProc1)
        : this()
        {
            if (_EMPTProc1 == null)
            {
                if (VirMSSQL.TableAdapter.Servidor_De_Processos)
                    throw new Exception("uso de construtor sem EMPTProc em Servidor de Processos");
                EMPTProc1 = new EMPTProc(EMPTipo.Local);
            }
            else
                EMPTProc1 = _EMPTProc1;
        }

        #region Adapters

        private dCondominiosCamposTableAdapters.PESloockTableAdapter pESloockTableAdapter;
        /// <summary>
        /// TableAdapter padrão para tabela: PESloock
        /// </summary>
        public dCondominiosCamposTableAdapters.PESloockTableAdapter PESloockTableAdapter
        {
            get
            {
                if (pESloockTableAdapter == null)
                {
                    pESloockTableAdapter = new dCondominiosCamposTableAdapters.PESloockTableAdapter();
                    pESloockTableAdapter.TrocarStringDeConexao(EMPTProc1.TBanco);
                };
                return pESloockTableAdapter;
            }
        }

        private dCondominiosCamposTableAdapters.PUBlicacaoTableAdapter pUBlicacaoTableAdapter;
        /// <summary>
        /// TableAdapter padrão para tabela: PUBlicacao
        /// </summary>
        public dCondominiosCamposTableAdapters.PUBlicacaoTableAdapter PUBlicacaoTableAdapter
        {
            get
            {
                if (pUBlicacaoTableAdapter == null)
                {
                    pUBlicacaoTableAdapter = new dCondominiosCamposTableAdapters.PUBlicacaoTableAdapter();
                    pUBlicacaoTableAdapter.TrocarStringDeConexao(EMPTProc1.TBanco);
                };
                return pUBlicacaoTableAdapter;
            }
        }

        private dCondominiosCamposTableAdapters.CIDADESTableAdapter cIDADESTableAdapter;

        /// <summary>
        /// TableAdapter padrão para tabela: CIDADES
        /// </summary>
        public dCondominiosCamposTableAdapters.CIDADESTableAdapter CIDADESTableAdapter
        {
            get
            {
                if (cIDADESTableAdapter == null)
                {
                    cIDADESTableAdapter = new dCondominiosCamposTableAdapters.CIDADESTableAdapter();
                    cIDADESTableAdapter.TrocarStringDeConexao(EMPTProc1.TBanco);
                };
                return cIDADESTableAdapter;
            }
        }

        private dCondominiosCamposTableAdapters.CONDOMINIOSTableAdapter cONDOMINIOSTableAdapter;
        /// <summary>
        /// TableAdapter padrão para tabela: CONDOMINIOS
        /// </summary>
        public dCondominiosCamposTableAdapters.CONDOMINIOSTableAdapter CONDOMINIOSTableAdapter
        {
            get
            {
                if (cONDOMINIOSTableAdapter == null)
                {
                    cONDOMINIOSTableAdapter = new dCondominiosCamposTableAdapters.CONDOMINIOSTableAdapter();
                    cONDOMINIOSTableAdapter.TrocarStringDeConexao(EMPTProc1.TBanco);
                };
                return cONDOMINIOSTableAdapter;
            }
        }

        private dCondominiosCamposTableAdapters.ModeloBAlanceteTableAdapter modeloBAlanceteTableAdapter;
        /// <summary>
        /// TableAdapter padrão para tabela: ModeloBAlancete
        /// </summary>
        public dCondominiosCamposTableAdapters.ModeloBAlanceteTableAdapter ModeloBAlanceteTableAdapter
        {
            get
            {
                if (modeloBAlanceteTableAdapter == null)
                {
                    modeloBAlanceteTableAdapter = new dCondominiosCamposTableAdapters.ModeloBAlanceteTableAdapter();
                    modeloBAlanceteTableAdapter.TrocarStringDeConexao(EMPTProc1.TBanco);
                };
                return modeloBAlanceteTableAdapter;
            }
        }

        private dCondominiosCamposTableAdapters.ConTasLogicasTableAdapter conTasLogicasTableAdapter;
        /// <summary>
        /// TableAdapter padrão para tabela: ConTasLogicas
        /// </summary>
        public dCondominiosCamposTableAdapters.ConTasLogicasTableAdapter ConTasLogicasTableAdapter
        {
            get
            {
                if (conTasLogicasTableAdapter == null)
                {
                    conTasLogicasTableAdapter = new dCondominiosCamposTableAdapters.ConTasLogicasTableAdapter();
                    conTasLogicasTableAdapter.TrocarStringDeConexao(EMPTProc1.TBanco);
                };
                return conTasLogicasTableAdapter;
            }
        }

        private dCondominiosCamposTableAdapters.GradeEQuipamentoTableAdapter gradeEQuipamentoTableAdapter;
        /// <summary>
        /// TableAdapter padrão para tabela: GradeEQuipamento
        /// </summary>
        public dCondominiosCamposTableAdapters.GradeEQuipamentoTableAdapter GradeEQuipamentoTableAdapter
        {
            get
            {
                if (gradeEQuipamentoTableAdapter == null)
                {
                    gradeEQuipamentoTableAdapter = new dCondominiosCamposTableAdapters.GradeEQuipamentoTableAdapter();
                    gradeEQuipamentoTableAdapter.TrocarStringDeConexao(EMPTProc1.TBanco);
                };
                return gradeEQuipamentoTableAdapter;
            }
        }

        //*** MRC - INICIO - RESERVA (28/05/2014 12:30) ***
        private dCondominiosCamposTableAdapters.ReGrasEquipamentoTableAdapter reGrasEquipamentoTableAdapter;
        /// <summary>
        /// TableAdapter padrão para tabela: ReGrasEquipamento
        /// </summary>
        public dCondominiosCamposTableAdapters.ReGrasEquipamentoTableAdapter ReGrasEquipamentoTableAdapter
        {
            get
            {
                if (reGrasEquipamentoTableAdapter == null)
                {
                    reGrasEquipamentoTableAdapter = new dCondominiosCamposTableAdapters.ReGrasEquipamentoTableAdapter();
                    reGrasEquipamentoTableAdapter.TrocarStringDeConexao(EMPTProc1.TBanco);
                };
                return reGrasEquipamentoTableAdapter;
            }
        }
        //*** MRC - TERMINO - RESERVA (28/05/2014 12:30) ***

        private dCondominiosCamposTableAdapters.EQuiPamentosTableAdapter eQuiPamentosTableAdapter;
        /// <summary>
        /// TableAdapter padrão para tabela: EQuiPamentos
        /// </summary>
        public dCondominiosCamposTableAdapters.EQuiPamentosTableAdapter EQuiPamentosTableAdapter
        {
            get
            {
                if (eQuiPamentosTableAdapter == null)
                {
                    eQuiPamentosTableAdapter = new dCondominiosCamposTableAdapters.EQuiPamentosTableAdapter();
                    eQuiPamentosTableAdapter.TrocarStringDeConexao(EMPTProc1.TBanco);
                };
                return eQuiPamentosTableAdapter;
            }
        }

        private dCondominiosCamposTableAdapters.CORPODIRETIVO1TableAdapter cORPODIRETIVO1TableAdapter;
        /// <summary>
        /// TableAdapter padrão para tabela: CORPODIRETIVO1
        /// </summary>
        public dCondominiosCamposTableAdapters.CORPODIRETIVO1TableAdapter CORPODIRETIVO1TableAdapter
        {
            get
            {
                if (cORPODIRETIVO1TableAdapter == null)
                {
                    cORPODIRETIVO1TableAdapter = new dCondominiosCamposTableAdapters.CORPODIRETIVO1TableAdapter();
                    cORPODIRETIVO1TableAdapter.TrocarStringDeConexao(EMPTProc1.TBanco);
                };
                return cORPODIRETIVO1TableAdapter;
            }
        }

        private dCondominiosCamposTableAdapters.AGendaBaseTableAdapter aGendaBaseTableAdapter;
        /// <summary>
        /// TableAdapter padrão para tabela: AGendaBase
        /// </summary>
        public dCondominiosCamposTableAdapters.AGendaBaseTableAdapter AGendaBaseTableAdapter
        {
            get
            {
                if (aGendaBaseTableAdapter == null)
                {
                    aGendaBaseTableAdapter = new dCondominiosCamposTableAdapters.AGendaBaseTableAdapter();
                    aGendaBaseTableAdapter.TrocarStringDeConexao(EMPTProc1.TBanco);
                };
                return aGendaBaseTableAdapter;
            }
        }

        #endregion

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public decimal SomaFracao()
        {
            decimal TotalFra = 0;
            foreach (APARTAMENTOSRow APTrow in APARTAMENTOS)
                if (APTrow.RowState != System.Data.DataRowState.Deleted)
                    if (!APTrow.IsAPTFracaoIdealNull())
                        TotalFra += APTrow.APTFracaoIdeal;
            return TotalFra;
        }
    }
}

