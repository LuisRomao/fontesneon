namespace DIRF
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            DevExpress.XtraGrid.GridLevelNode gridLevelNode1 = new DevExpress.XtraGrid.GridLevelNode();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Form1));
            this.gridView2 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colTipo = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colV13 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.Calc = new DevExpress.XtraEditors.Repository.RepositoryItemCalcEdit();
            this.colV21 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colV31 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colV41 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colV51 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colV61 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colV71 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colV81 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colV91 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colV101 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colV111 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colV121 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridControl1 = new DevExpress.XtraGrid.GridControl();
            this.iRBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.gridView1 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colCNPJ = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colNome = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colV1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colV2 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colV3 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colV4 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colV5 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colV6 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colV7 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colV8 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colV9 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colV10 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colV11 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colV12 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colI1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colI2 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colI3 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colI4 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colI5 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colI6 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colI7 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colI8 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colI9 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colI10 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colI11 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colI12 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colCodigo = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colV131 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.panelControl1 = new DevExpress.XtraEditors.PanelControl();
            this.simpleButton3 = new DevExpress.XtraEditors.SimpleButton();
            this.simpleButton2 = new DevExpress.XtraEditors.SimpleButton();
            this.simpleButton1 = new DevExpress.XtraEditors.SimpleButton();
            this.xtraTabControl1 = new DevExpress.XtraTab.XtraTabControl();
            this.xtraTabPageCapa = new DevExpress.XtraTab.XtraTabPage();
            this.groupControl3 = new DevExpress.XtraEditors.GroupControl();
            this.spinEditCalendario = new DevExpress.XtraEditors.SpinEdit();
            this.spinEditReferencia = new DevExpress.XtraEditors.SpinEdit();
            this.labelControl8 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl9 = new DevExpress.XtraEditors.LabelControl();
            this.groupControl2 = new DevExpress.XtraEditors.GroupControl();
            this.labelControl10 = new DevExpress.XtraEditors.LabelControl();
            this.cpfDec = new DevExpress.XtraEditors.TextEdit();
            this.cnpjDec = new DevExpress.XtraEditors.TextEdit();
            this.NomeDeclarante = new DevExpress.XtraEditors.TextEdit();
            this.labelControl11 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl12 = new DevExpress.XtraEditors.LabelControl();
            this.groupControl1 = new DevExpress.XtraEditors.GroupControl();
            this.labelControl6 = new DevExpress.XtraEditors.LabelControl();
            this.EmailResp = new DevExpress.XtraEditors.TextEdit();
            this.RamalResp = new DevExpress.XtraEditors.SpinEdit();
            this.FaxResp = new DevExpress.XtraEditors.SpinEdit();
            this.FoneResp = new DevExpress.XtraEditors.SpinEdit();
            this.DDDResp = new DevExpress.XtraEditors.SpinEdit();
            this.cpfResp = new DevExpress.XtraEditors.TextEdit();
            this.NomeResp = new DevExpress.XtraEditors.TextEdit();
            this.labelControl5 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl4 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl3 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl2 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl1 = new DevExpress.XtraEditors.LabelControl();
            this.xtraTabPageIR = new DevExpress.XtraTab.XtraTabPage();
            ((System.ComponentModel.ISupportInitialize)(this.gridView2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Calc)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.iRBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl1)).BeginInit();
            this.panelControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.xtraTabControl1)).BeginInit();
            this.xtraTabControl1.SuspendLayout();
            this.xtraTabPageCapa.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl3)).BeginInit();
            this.groupControl3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.spinEditCalendario.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.spinEditReferencia.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl2)).BeginInit();
            this.groupControl2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.cpfDec.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cnpjDec.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.NomeDeclarante.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl1)).BeginInit();
            this.groupControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.EmailResp.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.RamalResp.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.FaxResp.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.FoneResp.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DDDResp.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cpfResp.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.NomeResp.Properties)).BeginInit();
            this.xtraTabPageIR.SuspendLayout();
            this.SuspendLayout();
            // 
            // gridView2
            // 
            this.gridView2.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colTipo,
            this.colV13,
            this.colV21,
            this.colV31,
            this.colV41,
            this.colV51,
            this.colV61,
            this.colV71,
            this.colV81,
            this.colV91,
            this.colV101,
            this.colV111,
            this.colV121});
            this.gridView2.GridControl = this.gridControl1;
            this.gridView2.Name = "gridView2";
            this.gridView2.OptionsView.ColumnAutoWidth = false;
            this.gridView2.ViewCaption = "Complementos";
            // 
            // colTipo
            // 
            this.colTipo.Caption = "Tipo";
            this.colTipo.FieldName = "Tipo";
            this.colTipo.Name = "colTipo";
            this.colTipo.Visible = true;
            this.colTipo.VisibleIndex = 0;
            // 
            // colV13
            // 
            this.colV13.Caption = "01";
            this.colV13.ColumnEdit = this.Calc;
            this.colV13.FieldName = "V1";
            this.colV13.Name = "colV13";
            this.colV13.Visible = true;
            this.colV13.VisibleIndex = 1;
            this.colV13.Width = 100;
            // 
            // Calc
            // 
            this.Calc.AutoHeight = false;
            this.Calc.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.Calc.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.Calc.Name = "Calc";
            // 
            // colV21
            // 
            this.colV21.Caption = "02";
            this.colV21.ColumnEdit = this.Calc;
            this.colV21.FieldName = "V2";
            this.colV21.Name = "colV21";
            this.colV21.Visible = true;
            this.colV21.VisibleIndex = 2;
            this.colV21.Width = 100;
            // 
            // colV31
            // 
            this.colV31.Caption = "03";
            this.colV31.ColumnEdit = this.Calc;
            this.colV31.FieldName = "V3";
            this.colV31.Name = "colV31";
            this.colV31.Visible = true;
            this.colV31.VisibleIndex = 3;
            this.colV31.Width = 100;
            // 
            // colV41
            // 
            this.colV41.Caption = "04";
            this.colV41.ColumnEdit = this.Calc;
            this.colV41.FieldName = "V4";
            this.colV41.Name = "colV41";
            this.colV41.Visible = true;
            this.colV41.VisibleIndex = 4;
            this.colV41.Width = 100;
            // 
            // colV51
            // 
            this.colV51.Caption = "05";
            this.colV51.ColumnEdit = this.Calc;
            this.colV51.FieldName = "V5";
            this.colV51.Name = "colV51";
            this.colV51.Visible = true;
            this.colV51.VisibleIndex = 5;
            this.colV51.Width = 100;
            // 
            // colV61
            // 
            this.colV61.Caption = "06";
            this.colV61.ColumnEdit = this.Calc;
            this.colV61.FieldName = "V6";
            this.colV61.Name = "colV61";
            this.colV61.Visible = true;
            this.colV61.VisibleIndex = 6;
            this.colV61.Width = 100;
            // 
            // colV71
            // 
            this.colV71.Caption = "07";
            this.colV71.ColumnEdit = this.Calc;
            this.colV71.FieldName = "V7";
            this.colV71.Name = "colV71";
            this.colV71.Visible = true;
            this.colV71.VisibleIndex = 7;
            this.colV71.Width = 100;
            // 
            // colV81
            // 
            this.colV81.Caption = "08";
            this.colV81.ColumnEdit = this.Calc;
            this.colV81.FieldName = "V8";
            this.colV81.Name = "colV81";
            this.colV81.Visible = true;
            this.colV81.VisibleIndex = 8;
            this.colV81.Width = 100;
            // 
            // colV91
            // 
            this.colV91.Caption = "09";
            this.colV91.ColumnEdit = this.Calc;
            this.colV91.FieldName = "V9";
            this.colV91.Name = "colV91";
            this.colV91.Visible = true;
            this.colV91.VisibleIndex = 9;
            this.colV91.Width = 100;
            // 
            // colV101
            // 
            this.colV101.Caption = "10";
            this.colV101.ColumnEdit = this.Calc;
            this.colV101.FieldName = "V10";
            this.colV101.Name = "colV101";
            this.colV101.Visible = true;
            this.colV101.VisibleIndex = 10;
            this.colV101.Width = 100;
            // 
            // colV111
            // 
            this.colV111.Caption = "11";
            this.colV111.ColumnEdit = this.Calc;
            this.colV111.FieldName = "V11";
            this.colV111.Name = "colV111";
            this.colV111.Visible = true;
            this.colV111.VisibleIndex = 11;
            this.colV111.Width = 100;
            // 
            // colV121
            // 
            this.colV121.Caption = "12";
            this.colV121.ColumnEdit = this.Calc;
            this.colV121.FieldName = "V12";
            this.colV121.Name = "colV121";
            this.colV121.Visible = true;
            this.colV121.VisibleIndex = 12;
            this.colV121.Width = 100;
            // 
            // gridControl1
            // 
            this.gridControl1.DataMember = "IR";
            this.gridControl1.DataSource = this.iRBindingSource;
            this.gridControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            gridLevelNode1.LevelTemplate = this.gridView2;
            gridLevelNode1.RelationName = "IR_IRComp";
            this.gridControl1.LevelTree.Nodes.AddRange(new DevExpress.XtraGrid.GridLevelNode[] {
            gridLevelNode1});
            this.gridControl1.Location = new System.Drawing.Point(0, 0);
            this.gridControl1.MainView = this.gridView1;
            this.gridControl1.Name = "gridControl1";
            this.gridControl1.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.Calc});
            this.gridControl1.Size = new System.Drawing.Size(803, 421);
            this.gridControl1.TabIndex = 0;
            this.gridControl1.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridView1,
            this.gridView2});
            // 
            // iRBindingSource
            // 
            this.iRBindingSource.DataSource = typeof(dllDirf2011.dDirf);
            this.iRBindingSource.Position = 0;
            // 
            // gridView1
            // 
            this.gridView1.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colCNPJ,
            this.colNome,
            this.colV1,
            this.colV2,
            this.colV3,
            this.colV4,
            this.colV5,
            this.colV6,
            this.colV7,
            this.colV8,
            this.colV9,
            this.colV10,
            this.colV11,
            this.colV12,
            this.colI1,
            this.colI2,
            this.colI3,
            this.colI4,
            this.colI5,
            this.colI6,
            this.colI7,
            this.colI8,
            this.colI9,
            this.colI10,
            this.colI11,
            this.colI12,
            this.colCodigo,
            this.colV131});
            this.gridView1.GridControl = this.gridControl1;
            this.gridView1.GroupSummary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridGroupSummaryItem(DevExpress.Data.SummaryItemType.Sum, "V1", this.colV1, "{0:n2}"),
            new DevExpress.XtraGrid.GridGroupSummaryItem(DevExpress.Data.SummaryItemType.Sum, "V2", this.colV2, "{0:n2}"),
            new DevExpress.XtraGrid.GridGroupSummaryItem(DevExpress.Data.SummaryItemType.Sum, "V3", this.colV3, "{0:n2}"),
            new DevExpress.XtraGrid.GridGroupSummaryItem(DevExpress.Data.SummaryItemType.Sum, "V4", this.colV4, "{0:n2}"),
            new DevExpress.XtraGrid.GridGroupSummaryItem(DevExpress.Data.SummaryItemType.Sum, "V5", this.colV5, "{0:n2}"),
            new DevExpress.XtraGrid.GridGroupSummaryItem(DevExpress.Data.SummaryItemType.Sum, "V6", this.colV6, "{0:n2}"),
            new DevExpress.XtraGrid.GridGroupSummaryItem(DevExpress.Data.SummaryItemType.Sum, "V7", this.colV7, "{0:n2}"),
            new DevExpress.XtraGrid.GridGroupSummaryItem(DevExpress.Data.SummaryItemType.Sum, "V8", this.colV8, "{0:n2}"),
            new DevExpress.XtraGrid.GridGroupSummaryItem(DevExpress.Data.SummaryItemType.Sum, "V9", this.colV9, "{0:n2}"),
            new DevExpress.XtraGrid.GridGroupSummaryItem(DevExpress.Data.SummaryItemType.Sum, "V10", this.colV10, "{0:n2}"),
            new DevExpress.XtraGrid.GridGroupSummaryItem(DevExpress.Data.SummaryItemType.Sum, "V11", this.colV11, "{0:n2}"),
            new DevExpress.XtraGrid.GridGroupSummaryItem(DevExpress.Data.SummaryItemType.Sum, "V12", this.colV12, "{0:n2}"),
            new DevExpress.XtraGrid.GridGroupSummaryItem(DevExpress.Data.SummaryItemType.Sum, "I1", this.colI1, "{0:n2}"),
            new DevExpress.XtraGrid.GridGroupSummaryItem(DevExpress.Data.SummaryItemType.Sum, "I2", this.colI2, "{0:n2}"),
            new DevExpress.XtraGrid.GridGroupSummaryItem(DevExpress.Data.SummaryItemType.Sum, "I3", this.colI3, "{0:n2}"),
            new DevExpress.XtraGrid.GridGroupSummaryItem(DevExpress.Data.SummaryItemType.Sum, "I4", this.colI4, "{0:n2}"),
            new DevExpress.XtraGrid.GridGroupSummaryItem(DevExpress.Data.SummaryItemType.Sum, "I5", this.colI5, "{0:n2}"),
            new DevExpress.XtraGrid.GridGroupSummaryItem(DevExpress.Data.SummaryItemType.Sum, "I6", this.colI6, "{0:n2}"),
            new DevExpress.XtraGrid.GridGroupSummaryItem(DevExpress.Data.SummaryItemType.Sum, "I7", this.colI7, "{0:n2}"),
            new DevExpress.XtraGrid.GridGroupSummaryItem(DevExpress.Data.SummaryItemType.Sum, "I8", this.colI8, "{0:n2}"),
            new DevExpress.XtraGrid.GridGroupSummaryItem(DevExpress.Data.SummaryItemType.Sum, "I9", this.colI9, "{0:n2}"),
            new DevExpress.XtraGrid.GridGroupSummaryItem(DevExpress.Data.SummaryItemType.Sum, "I10", this.colI10, "{0:n2}"),
            new DevExpress.XtraGrid.GridGroupSummaryItem(DevExpress.Data.SummaryItemType.Sum, "I11", this.colI11, "{0:n2}"),
            new DevExpress.XtraGrid.GridGroupSummaryItem(DevExpress.Data.SummaryItemType.Sum, "I12", this.colI12, "{0:n2}")});
            this.gridView1.Name = "gridView1";
            this.gridView1.OptionsView.ColumnAutoWidth = false;
            this.gridView1.OptionsView.GroupFooterShowMode = DevExpress.XtraGrid.Views.Grid.GroupFooterShowMode.VisibleAlways;
            this.gridView1.OptionsView.NewItemRowPosition = DevExpress.XtraGrid.Views.Grid.NewItemRowPosition.Bottom;
            this.gridView1.OptionsView.ShowAutoFilterRow = true;
            this.gridView1.SortInfo.AddRange(new DevExpress.XtraGrid.Columns.GridColumnSortInfo[] {
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colCodigo, DevExpress.Data.ColumnSortOrder.Ascending)});
            // 
            // colCNPJ
            // 
            this.colCNPJ.FieldName = "CNPJ";
            this.colCNPJ.Name = "colCNPJ";
            this.colCNPJ.Visible = true;
            this.colCNPJ.VisibleIndex = 0;
            this.colCNPJ.Width = 194;
            // 
            // colNome
            // 
            this.colNome.FieldName = "Nome";
            this.colNome.Name = "colNome";
            this.colNome.Visible = true;
            this.colNome.VisibleIndex = 1;
            this.colNome.Width = 378;
            // 
            // colV1
            // 
            this.colV1.Caption = "01";
            this.colV1.ColumnEdit = this.Calc;
            this.colV1.FieldName = "V1";
            this.colV1.Name = "colV1";
            this.colV1.Visible = true;
            this.colV1.VisibleIndex = 3;
            this.colV1.Width = 100;
            // 
            // colV2
            // 
            this.colV2.Caption = "02";
            this.colV2.ColumnEdit = this.Calc;
            this.colV2.FieldName = "V2";
            this.colV2.Name = "colV2";
            this.colV2.Visible = true;
            this.colV2.VisibleIndex = 4;
            this.colV2.Width = 100;
            // 
            // colV3
            // 
            this.colV3.Caption = "03";
            this.colV3.ColumnEdit = this.Calc;
            this.colV3.FieldName = "V3";
            this.colV3.Name = "colV3";
            this.colV3.Visible = true;
            this.colV3.VisibleIndex = 5;
            this.colV3.Width = 100;
            // 
            // colV4
            // 
            this.colV4.Caption = "04";
            this.colV4.ColumnEdit = this.Calc;
            this.colV4.FieldName = "V4";
            this.colV4.Name = "colV4";
            this.colV4.Visible = true;
            this.colV4.VisibleIndex = 6;
            this.colV4.Width = 100;
            // 
            // colV5
            // 
            this.colV5.Caption = "05";
            this.colV5.ColumnEdit = this.Calc;
            this.colV5.FieldName = "V5";
            this.colV5.Name = "colV5";
            this.colV5.Visible = true;
            this.colV5.VisibleIndex = 7;
            this.colV5.Width = 100;
            // 
            // colV6
            // 
            this.colV6.Caption = "06";
            this.colV6.ColumnEdit = this.Calc;
            this.colV6.FieldName = "V6";
            this.colV6.Name = "colV6";
            this.colV6.Visible = true;
            this.colV6.VisibleIndex = 8;
            this.colV6.Width = 100;
            // 
            // colV7
            // 
            this.colV7.Caption = "07";
            this.colV7.ColumnEdit = this.Calc;
            this.colV7.FieldName = "V7";
            this.colV7.Name = "colV7";
            this.colV7.Visible = true;
            this.colV7.VisibleIndex = 9;
            this.colV7.Width = 100;
            // 
            // colV8
            // 
            this.colV8.Caption = "08";
            this.colV8.ColumnEdit = this.Calc;
            this.colV8.FieldName = "V8";
            this.colV8.Name = "colV8";
            this.colV8.Visible = true;
            this.colV8.VisibleIndex = 10;
            this.colV8.Width = 100;
            // 
            // colV9
            // 
            this.colV9.Caption = "09";
            this.colV9.ColumnEdit = this.Calc;
            this.colV9.FieldName = "V9";
            this.colV9.Name = "colV9";
            this.colV9.Visible = true;
            this.colV9.VisibleIndex = 11;
            this.colV9.Width = 100;
            // 
            // colV10
            // 
            this.colV10.Caption = "10";
            this.colV10.ColumnEdit = this.Calc;
            this.colV10.FieldName = "V10";
            this.colV10.Name = "colV10";
            this.colV10.Visible = true;
            this.colV10.VisibleIndex = 12;
            this.colV10.Width = 100;
            // 
            // colV11
            // 
            this.colV11.Caption = "11";
            this.colV11.ColumnEdit = this.Calc;
            this.colV11.FieldName = "V11";
            this.colV11.Name = "colV11";
            this.colV11.Visible = true;
            this.colV11.VisibleIndex = 13;
            this.colV11.Width = 100;
            // 
            // colV12
            // 
            this.colV12.Caption = "12";
            this.colV12.ColumnEdit = this.Calc;
            this.colV12.FieldName = "V12";
            this.colV12.Name = "colV12";
            this.colV12.Visible = true;
            this.colV12.VisibleIndex = 14;
            this.colV12.Width = 100;
            // 
            // colI1
            // 
            this.colI1.Caption = "Ret 01";
            this.colI1.ColumnEdit = this.Calc;
            this.colI1.FieldName = "I1";
            this.colI1.Name = "colI1";
            this.colI1.Visible = true;
            this.colI1.VisibleIndex = 16;
            this.colI1.Width = 100;
            // 
            // colI2
            // 
            this.colI2.Caption = "Ret 02";
            this.colI2.ColumnEdit = this.Calc;
            this.colI2.FieldName = "I2";
            this.colI2.Name = "colI2";
            this.colI2.Visible = true;
            this.colI2.VisibleIndex = 17;
            this.colI2.Width = 100;
            // 
            // colI3
            // 
            this.colI3.Caption = "Ret 03";
            this.colI3.ColumnEdit = this.Calc;
            this.colI3.FieldName = "I3";
            this.colI3.Name = "colI3";
            this.colI3.Visible = true;
            this.colI3.VisibleIndex = 18;
            this.colI3.Width = 100;
            // 
            // colI4
            // 
            this.colI4.Caption = "Ret 04";
            this.colI4.ColumnEdit = this.Calc;
            this.colI4.FieldName = "I4";
            this.colI4.Name = "colI4";
            this.colI4.Visible = true;
            this.colI4.VisibleIndex = 19;
            this.colI4.Width = 100;
            // 
            // colI5
            // 
            this.colI5.Caption = "Ret 05";
            this.colI5.ColumnEdit = this.Calc;
            this.colI5.FieldName = "I5";
            this.colI5.Name = "colI5";
            this.colI5.Visible = true;
            this.colI5.VisibleIndex = 20;
            this.colI5.Width = 100;
            // 
            // colI6
            // 
            this.colI6.Caption = "Ret 06";
            this.colI6.ColumnEdit = this.Calc;
            this.colI6.FieldName = "I6";
            this.colI6.Name = "colI6";
            this.colI6.Visible = true;
            this.colI6.VisibleIndex = 21;
            this.colI6.Width = 100;
            // 
            // colI7
            // 
            this.colI7.Caption = "Ret 07";
            this.colI7.ColumnEdit = this.Calc;
            this.colI7.FieldName = "I7";
            this.colI7.Name = "colI7";
            this.colI7.Visible = true;
            this.colI7.VisibleIndex = 22;
            this.colI7.Width = 100;
            // 
            // colI8
            // 
            this.colI8.Caption = "Ret 08";
            this.colI8.ColumnEdit = this.Calc;
            this.colI8.FieldName = "I8";
            this.colI8.Name = "colI8";
            this.colI8.Visible = true;
            this.colI8.VisibleIndex = 23;
            this.colI8.Width = 100;
            // 
            // colI9
            // 
            this.colI9.Caption = "Ret 09";
            this.colI9.ColumnEdit = this.Calc;
            this.colI9.FieldName = "I9";
            this.colI9.Name = "colI9";
            this.colI9.Visible = true;
            this.colI9.VisibleIndex = 24;
            this.colI9.Width = 100;
            // 
            // colI10
            // 
            this.colI10.Caption = "Ret 10";
            this.colI10.ColumnEdit = this.Calc;
            this.colI10.FieldName = "I10";
            this.colI10.Name = "colI10";
            this.colI10.Visible = true;
            this.colI10.VisibleIndex = 25;
            this.colI10.Width = 100;
            // 
            // colI11
            // 
            this.colI11.Caption = "Ret 11";
            this.colI11.ColumnEdit = this.Calc;
            this.colI11.FieldName = "I11";
            this.colI11.Name = "colI11";
            this.colI11.Visible = true;
            this.colI11.VisibleIndex = 26;
            this.colI11.Width = 100;
            // 
            // colI12
            // 
            this.colI12.Caption = "Ret 12";
            this.colI12.ColumnEdit = this.Calc;
            this.colI12.FieldName = "I12";
            this.colI12.Name = "colI12";
            this.colI12.Visible = true;
            this.colI12.VisibleIndex = 27;
            this.colI12.Width = 100;
            // 
            // colCodigo
            // 
            this.colCodigo.Caption = "C�digo";
            this.colCodigo.FieldName = "Codigo";
            this.colCodigo.Name = "colCodigo";
            this.colCodigo.Visible = true;
            this.colCodigo.VisibleIndex = 2;
            // 
            // colV131
            // 
            this.colV131.Caption = "13�";
            this.colV131.ColumnEdit = this.Calc;
            this.colV131.FieldName = "V13";
            this.colV131.Name = "colV131";
            this.colV131.Visible = true;
            this.colV131.VisibleIndex = 15;
            this.colV131.Width = 100;
            // 
            // panelControl1
            // 
            this.panelControl1.Controls.Add(this.simpleButton3);
            this.panelControl1.Controls.Add(this.simpleButton2);
            this.panelControl1.Controls.Add(this.simpleButton1);
            this.panelControl1.Dock = System.Windows.Forms.DockStyle.Top;
            this.panelControl1.Location = new System.Drawing.Point(0, 0);
            this.panelControl1.Name = "panelControl1";
            this.panelControl1.Size = new System.Drawing.Size(809, 76);
            this.panelControl1.TabIndex = 0;
            // 
            // simpleButton3
            // 
            this.simpleButton3.AllowFocus = false;
            this.simpleButton3.ImageOptions.Image = global::DIRF.Properties.Resources.DIRF;
            this.simpleButton3.Location = new System.Drawing.Point(327, 6);
            this.simpleButton3.Name = "simpleButton3";
            this.simpleButton3.Size = new System.Drawing.Size(155, 65);
            this.simpleButton3.TabIndex = 2;
            this.simpleButton3.Text = "Gerar Arquivo";
            this.simpleButton3.Click += new System.EventHandler(this.simpleButton3_Click);
            // 
            // simpleButton2
            // 
            this.simpleButton2.AllowFocus = false;
            this.simpleButton2.ImageOptions.Image = global::DIRF.Properties.Resources.excel3;
            this.simpleButton2.Location = new System.Drawing.Point(166, 5);
            this.simpleButton2.Name = "simpleButton2";
            this.simpleButton2.Size = new System.Drawing.Size(155, 65);
            this.simpleButton2.TabIndex = 1;
            this.simpleButton2.Text = "Importar IR";
            this.simpleButton2.Click += new System.EventHandler(this.simpleButton2_Click);
            // 
            // simpleButton1
            // 
            this.simpleButton1.AllowFocus = false;
            this.simpleButton1.ImageOptions.Image = global::DIRF.Properties.Resources._48px_Gnome_edit_clear_svg;
            this.simpleButton1.Location = new System.Drawing.Point(5, 5);
            this.simpleButton1.Name = "simpleButton1";
            this.simpleButton1.Size = new System.Drawing.Size(155, 65);
            this.simpleButton1.TabIndex = 0;
            this.simpleButton1.Text = "Apaga Dados";
            this.simpleButton1.Click += new System.EventHandler(this.simpleButton1_Click);
            // 
            // xtraTabControl1
            // 
            this.xtraTabControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.xtraTabControl1.Location = new System.Drawing.Point(0, 76);
            this.xtraTabControl1.Name = "xtraTabControl1";
            this.xtraTabControl1.SelectedTabPage = this.xtraTabPageCapa;
            this.xtraTabControl1.Size = new System.Drawing.Size(809, 449);
            this.xtraTabControl1.TabIndex = 1;
            this.xtraTabControl1.TabPages.AddRange(new DevExpress.XtraTab.XtraTabPage[] {
            this.xtraTabPageCapa,
            this.xtraTabPageIR});
            // 
            // xtraTabPageCapa
            // 
            this.xtraTabPageCapa.Controls.Add(this.groupControl3);
            this.xtraTabPageCapa.Controls.Add(this.groupControl2);
            this.xtraTabPageCapa.Controls.Add(this.groupControl1);
            this.xtraTabPageCapa.Name = "xtraTabPageCapa";
            this.xtraTabPageCapa.Size = new System.Drawing.Size(803, 421);
            this.xtraTabPageCapa.Text = "Capa";
            // 
            // groupControl3
            // 
            this.groupControl3.Appearance.Font = new System.Drawing.Font("Tahoma", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupControl3.Appearance.Options.UseFont = true;
            this.groupControl3.AppearanceCaption.Font = new System.Drawing.Font("Tahoma", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupControl3.AppearanceCaption.Options.UseFont = true;
            this.groupControl3.Controls.Add(this.spinEditCalendario);
            this.groupControl3.Controls.Add(this.spinEditReferencia);
            this.groupControl3.Controls.Add(this.labelControl8);
            this.groupControl3.Controls.Add(this.labelControl9);
            this.groupControl3.Dock = System.Windows.Forms.DockStyle.Top;
            this.groupControl3.Location = new System.Drawing.Point(0, 316);
            this.groupControl3.Name = "groupControl3";
            this.groupControl3.Size = new System.Drawing.Size(803, 103);
            this.groupControl3.TabIndex = 3;
            this.groupControl3.Text = "Ano";
            // 
            // spinEditCalendario
            // 
            this.spinEditCalendario.EditValue = new decimal(new int[] {
            2017,
            0,
            0,
            0});
            this.spinEditCalendario.Location = new System.Drawing.Point(94, 69);
            this.spinEditCalendario.Name = "spinEditCalendario";
            this.spinEditCalendario.Properties.MaxValue = new decimal(new int[] {
            2019,
            0,
            0,
            0});
            this.spinEditCalendario.Properties.MinValue = new decimal(new int[] {
            2010,
            0,
            0,
            0});
            this.spinEditCalendario.Size = new System.Drawing.Size(156, 20);
            this.spinEditCalendario.TabIndex = 12;
            // 
            // spinEditReferencia
            // 
            this.spinEditReferencia.EditValue = new decimal(new int[] {
            2018,
            0,
            0,
            0});
            this.spinEditReferencia.Location = new System.Drawing.Point(94, 43);
            this.spinEditReferencia.Name = "spinEditReferencia";
            this.spinEditReferencia.Properties.MaxValue = new decimal(new int[] {
            2020,
            0,
            0,
            0});
            this.spinEditReferencia.Properties.MinValue = new decimal(new int[] {
            2011,
            0,
            0,
            0});
            this.spinEditReferencia.Size = new System.Drawing.Size(156, 20);
            this.spinEditReferencia.TabIndex = 11;
            // 
            // labelControl8
            // 
            this.labelControl8.Appearance.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl8.Appearance.Options.UseFont = true;
            this.labelControl8.Location = new System.Drawing.Point(10, 70);
            this.labelControl8.Name = "labelControl8";
            this.labelControl8.Size = new System.Drawing.Size(73, 16);
            this.labelControl8.TabIndex = 1;
            this.labelControl8.Text = "Calend�rio:";
            // 
            // labelControl9
            // 
            this.labelControl9.Appearance.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl9.Appearance.Options.UseFont = true;
            this.labelControl9.Location = new System.Drawing.Point(10, 44);
            this.labelControl9.Name = "labelControl9";
            this.labelControl9.Size = new System.Drawing.Size(75, 16);
            this.labelControl9.TabIndex = 0;
            this.labelControl9.Text = "Refer�ncia:";
            // 
            // groupControl2
            // 
            this.groupControl2.Appearance.Font = new System.Drawing.Font("Tahoma", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupControl2.Appearance.Options.UseFont = true;
            this.groupControl2.AppearanceCaption.Font = new System.Drawing.Font("Tahoma", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupControl2.AppearanceCaption.Options.UseFont = true;
            this.groupControl2.Controls.Add(this.labelControl10);
            this.groupControl2.Controls.Add(this.cpfDec);
            this.groupControl2.Controls.Add(this.cnpjDec);
            this.groupControl2.Controls.Add(this.NomeDeclarante);
            this.groupControl2.Controls.Add(this.labelControl11);
            this.groupControl2.Controls.Add(this.labelControl12);
            this.groupControl2.Dock = System.Windows.Forms.DockStyle.Top;
            this.groupControl2.Location = new System.Drawing.Point(0, 185);
            this.groupControl2.Name = "groupControl2";
            this.groupControl2.Size = new System.Drawing.Size(803, 131);
            this.groupControl2.TabIndex = 2;
            this.groupControl2.Text = "Declarante";
            // 
            // labelControl10
            // 
            this.labelControl10.Appearance.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl10.Appearance.Options.UseFont = true;
            this.labelControl10.Location = new System.Drawing.Point(10, 96);
            this.labelControl10.Name = "labelControl10";
            this.labelControl10.Size = new System.Drawing.Size(67, 16);
            this.labelControl10.TabIndex = 14;
            this.labelControl10.Text = "CPF Resp.:";
            // 
            // cpfDec
            // 
            this.cpfDec.Location = new System.Drawing.Point(94, 95);
            this.cpfDec.Name = "cpfDec";
            this.cpfDec.Size = new System.Drawing.Size(156, 20);
            this.cpfDec.TabIndex = 13;
            this.cpfDec.Validating += new System.ComponentModel.CancelEventHandler(this.ValidaCPF);
            // 
            // cnpjDec
            // 
            this.cnpjDec.Location = new System.Drawing.Point(94, 69);
            this.cnpjDec.Name = "cnpjDec";
            this.cnpjDec.Size = new System.Drawing.Size(156, 20);
            this.cnpjDec.TabIndex = 6;
            this.cnpjDec.Validating += new System.ComponentModel.CancelEventHandler(this.ValidaCNPJ);
            // 
            // NomeDeclarante
            // 
            this.NomeDeclarante.Location = new System.Drawing.Point(94, 43);
            this.NomeDeclarante.Name = "NomeDeclarante";
            this.NomeDeclarante.Properties.MaxLength = 60;
            this.NomeDeclarante.Size = new System.Drawing.Size(689, 20);
            this.NomeDeclarante.TabIndex = 5;
            // 
            // labelControl11
            // 
            this.labelControl11.Appearance.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl11.Appearance.Options.UseFont = true;
            this.labelControl11.Location = new System.Drawing.Point(10, 70);
            this.labelControl11.Name = "labelControl11";
            this.labelControl11.Size = new System.Drawing.Size(35, 16);
            this.labelControl11.TabIndex = 1;
            this.labelControl11.Text = "CNPJ:";
            // 
            // labelControl12
            // 
            this.labelControl12.Appearance.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl12.Appearance.Options.UseFont = true;
            this.labelControl12.Location = new System.Drawing.Point(10, 44);
            this.labelControl12.Name = "labelControl12";
            this.labelControl12.Size = new System.Drawing.Size(40, 16);
            this.labelControl12.TabIndex = 0;
            this.labelControl12.Text = "Nome:";
            // 
            // groupControl1
            // 
            this.groupControl1.Appearance.Font = new System.Drawing.Font("Tahoma", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupControl1.Appearance.Options.UseFont = true;
            this.groupControl1.AppearanceCaption.Font = new System.Drawing.Font("Tahoma", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupControl1.AppearanceCaption.Options.UseFont = true;
            this.groupControl1.Controls.Add(this.labelControl6);
            this.groupControl1.Controls.Add(this.EmailResp);
            this.groupControl1.Controls.Add(this.RamalResp);
            this.groupControl1.Controls.Add(this.FaxResp);
            this.groupControl1.Controls.Add(this.FoneResp);
            this.groupControl1.Controls.Add(this.DDDResp);
            this.groupControl1.Controls.Add(this.cpfResp);
            this.groupControl1.Controls.Add(this.NomeResp);
            this.groupControl1.Controls.Add(this.labelControl5);
            this.groupControl1.Controls.Add(this.labelControl4);
            this.groupControl1.Controls.Add(this.labelControl3);
            this.groupControl1.Controls.Add(this.labelControl2);
            this.groupControl1.Controls.Add(this.labelControl1);
            this.groupControl1.Dock = System.Windows.Forms.DockStyle.Top;
            this.groupControl1.Location = new System.Drawing.Point(0, 0);
            this.groupControl1.Name = "groupControl1";
            this.groupControl1.Size = new System.Drawing.Size(803, 185);
            this.groupControl1.TabIndex = 1;
            this.groupControl1.Text = "Respons�vel pelo preenchimento";
            // 
            // labelControl6
            // 
            this.labelControl6.Appearance.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl6.Appearance.Options.UseFont = true;
            this.labelControl6.Location = new System.Drawing.Point(267, 96);
            this.labelControl6.Name = "labelControl6";
            this.labelControl6.Size = new System.Drawing.Size(44, 16);
            this.labelControl6.TabIndex = 12;
            this.labelControl6.Text = "Ramal:";
            // 
            // EmailResp
            // 
            this.EmailResp.Location = new System.Drawing.Point(94, 147);
            this.EmailResp.Name = "EmailResp";
            this.EmailResp.Properties.MaxLength = 50;
            this.EmailResp.Size = new System.Drawing.Size(689, 20);
            this.EmailResp.TabIndex = 11;
            // 
            // RamalResp
            // 
            this.RamalResp.EditValue = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.RamalResp.Location = new System.Drawing.Point(323, 95);
            this.RamalResp.Name = "RamalResp";
            this.RamalResp.Size = new System.Drawing.Size(55, 20);
            this.RamalResp.TabIndex = 10;
            // 
            // FaxResp
            // 
            this.FaxResp.EditValue = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.FaxResp.Location = new System.Drawing.Point(131, 121);
            this.FaxResp.Name = "FaxResp";
            this.FaxResp.Size = new System.Drawing.Size(119, 20);
            this.FaxResp.TabIndex = 9;
            // 
            // FoneResp
            // 
            this.FoneResp.EditValue = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.FoneResp.Location = new System.Drawing.Point(131, 95);
            this.FoneResp.Name = "FoneResp";
            this.FoneResp.Size = new System.Drawing.Size(119, 20);
            this.FoneResp.TabIndex = 8;
            // 
            // DDDResp
            // 
            this.DDDResp.EditValue = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.DDDResp.Location = new System.Drawing.Point(94, 95);
            this.DDDResp.Name = "DDDResp";
            this.DDDResp.Size = new System.Drawing.Size(31, 20);
            this.DDDResp.TabIndex = 7;
            // 
            // cpfResp
            // 
            this.cpfResp.Location = new System.Drawing.Point(94, 69);
            this.cpfResp.Name = "cpfResp";
            this.cpfResp.Size = new System.Drawing.Size(156, 20);
            this.cpfResp.TabIndex = 6;
            this.cpfResp.Validating += new System.ComponentModel.CancelEventHandler(this.ValidaCPF);
            // 
            // NomeResp
            // 
            this.NomeResp.Location = new System.Drawing.Point(94, 43);
            this.NomeResp.Name = "NomeResp";
            this.NomeResp.Properties.MaxLength = 60;
            this.NomeResp.Size = new System.Drawing.Size(689, 20);
            this.NomeResp.TabIndex = 5;
            // 
            // labelControl5
            // 
            this.labelControl5.Appearance.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl5.Appearance.Options.UseFont = true;
            this.labelControl5.Location = new System.Drawing.Point(10, 148);
            this.labelControl5.Name = "labelControl5";
            this.labelControl5.Size = new System.Drawing.Size(41, 16);
            this.labelControl5.TabIndex = 4;
            this.labelControl5.Text = "E.mail:";
            // 
            // labelControl4
            // 
            this.labelControl4.Appearance.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl4.Appearance.Options.UseFont = true;
            this.labelControl4.Location = new System.Drawing.Point(10, 122);
            this.labelControl4.Name = "labelControl4";
            this.labelControl4.Size = new System.Drawing.Size(26, 16);
            this.labelControl4.TabIndex = 3;
            this.labelControl4.Text = "Fax:";
            // 
            // labelControl3
            // 
            this.labelControl3.Appearance.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl3.Appearance.Options.UseFont = true;
            this.labelControl3.Location = new System.Drawing.Point(10, 96);
            this.labelControl3.Name = "labelControl3";
            this.labelControl3.Size = new System.Drawing.Size(60, 16);
            this.labelControl3.TabIndex = 2;
            this.labelControl3.Text = "Telefone:";
            // 
            // labelControl2
            // 
            this.labelControl2.Appearance.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl2.Appearance.Options.UseFont = true;
            this.labelControl2.Location = new System.Drawing.Point(10, 70);
            this.labelControl2.Name = "labelControl2";
            this.labelControl2.Size = new System.Drawing.Size(27, 16);
            this.labelControl2.TabIndex = 1;
            this.labelControl2.Text = "CPF:";
            // 
            // labelControl1
            // 
            this.labelControl1.Appearance.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl1.Appearance.Options.UseFont = true;
            this.labelControl1.Location = new System.Drawing.Point(10, 44);
            this.labelControl1.Name = "labelControl1";
            this.labelControl1.Size = new System.Drawing.Size(40, 16);
            this.labelControl1.TabIndex = 0;
            this.labelControl1.Text = "Nome:";
            // 
            // xtraTabPageIR
            // 
            this.xtraTabPageIR.Controls.Add(this.gridControl1);
            this.xtraTabPageIR.Name = "xtraTabPageIR";
            this.xtraTabPageIR.Size = new System.Drawing.Size(803, 421);
            this.xtraTabPageIR.Text = "Valores";
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(809, 525);
            this.Controls.Add(this.xtraTabControl1);
            this.Controls.Add(this.panelControl1);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "Form1";
            this.Text = "Gera DIRF 2017 17.1.4.85";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.Form1_FormClosing);
            ((System.ComponentModel.ISupportInitialize)(this.gridView2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Calc)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.iRBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl1)).EndInit();
            this.panelControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.xtraTabControl1)).EndInit();
            this.xtraTabControl1.ResumeLayout(false);
            this.xtraTabPageCapa.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.groupControl3)).EndInit();
            this.groupControl3.ResumeLayout(false);
            this.groupControl3.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.spinEditCalendario.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.spinEditReferencia.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl2)).EndInit();
            this.groupControl2.ResumeLayout(false);
            this.groupControl2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.cpfDec.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cnpjDec.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.NomeDeclarante.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl1)).EndInit();
            this.groupControl1.ResumeLayout(false);
            this.groupControl1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.EmailResp.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.RamalResp.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.FaxResp.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.FoneResp.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DDDResp.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cpfResp.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.NomeResp.Properties)).EndInit();
            this.xtraTabPageIR.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraEditors.PanelControl panelControl1;
        private DevExpress.XtraTab.XtraTabControl xtraTabControl1;
        private DevExpress.XtraTab.XtraTabPage xtraTabPageCapa;
        private DevExpress.XtraTab.XtraTabPage xtraTabPageIR;
        private DevExpress.XtraGrid.GridControl gridControl1;
        private System.Windows.Forms.BindingSource iRBindingSource;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView1;
        private DevExpress.XtraGrid.Columns.GridColumn colCNPJ;
        private DevExpress.XtraGrid.Columns.GridColumn colNome;
        private DevExpress.XtraGrid.Columns.GridColumn colV1;
        private DevExpress.XtraGrid.Columns.GridColumn colV2;
        private DevExpress.XtraGrid.Columns.GridColumn colV3;
        private DevExpress.XtraGrid.Columns.GridColumn colV4;
        private DevExpress.XtraGrid.Columns.GridColumn colV5;
        private DevExpress.XtraGrid.Columns.GridColumn colV6;
        private DevExpress.XtraGrid.Columns.GridColumn colV7;
        private DevExpress.XtraGrid.Columns.GridColumn colV8;
        private DevExpress.XtraGrid.Columns.GridColumn colV9;
        private DevExpress.XtraGrid.Columns.GridColumn colV10;
        private DevExpress.XtraGrid.Columns.GridColumn colV11;
        private DevExpress.XtraGrid.Columns.GridColumn colV12;
        private DevExpress.XtraGrid.Columns.GridColumn colI1;
        private DevExpress.XtraGrid.Columns.GridColumn colI2;
        private DevExpress.XtraGrid.Columns.GridColumn colI3;
        private DevExpress.XtraGrid.Columns.GridColumn colI4;
        private DevExpress.XtraGrid.Columns.GridColumn colI5;
        private DevExpress.XtraGrid.Columns.GridColumn colI6;
        private DevExpress.XtraGrid.Columns.GridColumn colI7;
        private DevExpress.XtraGrid.Columns.GridColumn colI8;
        private DevExpress.XtraGrid.Columns.GridColumn colI9;
        private DevExpress.XtraGrid.Columns.GridColumn colI10;
        private DevExpress.XtraGrid.Columns.GridColumn colI11;
        private DevExpress.XtraGrid.Columns.GridColumn colI12;
        private DevExpress.XtraEditors.Repository.RepositoryItemCalcEdit Calc;
        private DevExpress.XtraEditors.GroupControl groupControl1;
        private DevExpress.XtraEditors.LabelControl labelControl5;
        private DevExpress.XtraEditors.LabelControl labelControl4;
        private DevExpress.XtraEditors.LabelControl labelControl3;
        private DevExpress.XtraEditors.LabelControl labelControl2;
        private DevExpress.XtraEditors.LabelControl labelControl1;
        private DevExpress.XtraEditors.TextEdit NomeResp;
        private DevExpress.XtraEditors.TextEdit cpfResp;
        private DevExpress.XtraEditors.LabelControl labelControl6;
        private DevExpress.XtraEditors.TextEdit EmailResp;
        private DevExpress.XtraEditors.SpinEdit RamalResp;
        private DevExpress.XtraEditors.SpinEdit FaxResp;
        private DevExpress.XtraEditors.SpinEdit FoneResp;
        private DevExpress.XtraEditors.SpinEdit DDDResp;
        private DevExpress.XtraEditors.GroupControl groupControl2;
        private DevExpress.XtraEditors.TextEdit cnpjDec;
        private DevExpress.XtraEditors.TextEdit NomeDeclarante;
        private DevExpress.XtraEditors.LabelControl labelControl11;
        private DevExpress.XtraEditors.LabelControl labelControl12;
        private DevExpress.XtraEditors.TextEdit cpfDec;
        private DevExpress.XtraEditors.LabelControl labelControl10;
        private DevExpress.XtraEditors.SimpleButton simpleButton1;
        private DevExpress.XtraEditors.SimpleButton simpleButton2;
        private DevExpress.XtraEditors.SimpleButton simpleButton3;
        private DevExpress.XtraGrid.Columns.GridColumn colCodigo;
        private DevExpress.XtraEditors.GroupControl groupControl3;
        private DevExpress.XtraEditors.LabelControl labelControl8;
        private DevExpress.XtraEditors.LabelControl labelControl9;
        private DevExpress.XtraEditors.SpinEdit spinEditCalendario;
        private DevExpress.XtraEditors.SpinEdit spinEditReferencia;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView2;
        private DevExpress.XtraGrid.Columns.GridColumn colTipo;
        private DevExpress.XtraGrid.Columns.GridColumn colV13;
        private DevExpress.XtraGrid.Columns.GridColumn colV21;
        private DevExpress.XtraGrid.Columns.GridColumn colV31;
        private DevExpress.XtraGrid.Columns.GridColumn colV41;
        private DevExpress.XtraGrid.Columns.GridColumn colV51;
        private DevExpress.XtraGrid.Columns.GridColumn colV61;
        private DevExpress.XtraGrid.Columns.GridColumn colV71;
        private DevExpress.XtraGrid.Columns.GridColumn colV81;
        private DevExpress.XtraGrid.Columns.GridColumn colV91;
        private DevExpress.XtraGrid.Columns.GridColumn colV101;
        private DevExpress.XtraGrid.Columns.GridColumn colV111;
        private DevExpress.XtraGrid.Columns.GridColumn colV121;
        private DevExpress.XtraGrid.Columns.GridColumn colV131;
    }
}

