using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using dllDirf2011;
using System.IO;
using DocBacarios;

namespace DIRF
{
    public partial class Form1 : Form
    {
        dDirf dDirf;
        string Arquivo;

        public Form1()
        {
            InitializeComponent();
            dDirf = new dDirf();
            string caminho = string.Format("{0}\\XML", Path.GetDirectoryName(Application.ExecutablePath));
            if (!Directory.Exists(caminho))
                Directory.CreateDirectory(caminho);
            Arquivo = string.Format("{0}\\dados.xml", caminho);
            if (File.Exists(Arquivo))
                Le();
            iRBindingSource.DataSource = dDirf;
        }

        private void Le()
        {
            try
            {
                dDirf.ReadXml(Arquivo);
                NomeResp.Text = dDirf.NomeResponsavel;
                cpfResp.Text = dDirf.cpfResponsavel.ToString();
                DDDResp.Value = dDirf.DDD;
                FoneResp.Value = dDirf.Fone;
                FaxResp.Value = dDirf.Fax;
                RamalResp.Value = dDirf.Ramal;
                EmailResp.Text = dDirf.Email;
                NomeDeclarante.Text = dDirf.NomeDeclarante;
                cpfDec.Text = dDirf.cpfDeclarante.ToString();
                cnpjDec.Text = dDirf.cnpjDeclarante.ToString();
                spinEditCalendario.Value = dDirf.AnoCalendario;
                spinEditReferencia.Value = dDirf.AnoReferencia;
            }
            catch { };
        }

        private void Grava()
        {
            dDirf.NomeResponsavel = NomeResp.Text;
            dDirf.cpfResponsavel = new DocBacarios.CPFCNPJ(cpfResp.Text);
            dDirf.DDD = (int)DDDResp.Value;
            dDirf.Fone = (int)FoneResp.Value;
            dDirf.Fax = (int)FaxResp.Value;
            dDirf.Ramal = (int)RamalResp.Value;
            dDirf.Email = EmailResp.Text;
            dDirf.NomeDeclarante = NomeDeclarante.Text;
            dDirf.cpfDeclarante = new DocBacarios.CPFCNPJ(cpfDec.Text);
            dDirf.cnpjDeclarante = new DocBacarios.CPFCNPJ(cnpjDec.Text);
            dDirf.AnoCalendario = (int)spinEditCalendario.Value;
            dDirf.AnoReferencia = (int)spinEditReferencia.Value;
            dDirf.WriteXml(Arquivo);
        }

       

        private void Form1_FormClosing(object sender, FormClosingEventArgs e)
        {
            Grava();
        }

        



        private void ValidaCPF(object sender, CancelEventArgs e)
        {
            DevExpress.XtraEditors.TextEdit Editor = (DevExpress.XtraEditors.TextEdit)sender;
            DocBacarios.CPFCNPJ entrada = new DocBacarios.CPFCNPJ(Editor.Text);
            if (entrada.Tipo == TipoCpfCnpj.CPF)
                Editor.Text = entrada.ToString();
            else
            {
                Editor.ErrorText = "CPF inv�lido";
                e.Cancel = true;
            }
        }

        private void ValidaCNPJ(object sender, CancelEventArgs e)
        {
            DevExpress.XtraEditors.TextEdit Editor = (DevExpress.XtraEditors.TextEdit)sender;
            DocBacarios.CPFCNPJ entrada = new DocBacarios.CPFCNPJ(Editor.Text);
            if (entrada.Tipo == TipoCpfCnpj.CNPJ)
                Editor.Text = entrada.ToString();
            else
            {
                Editor.ErrorText = "CNPJ inv�lido";
                e.Cancel = true;
            }
        }

        private void simpleButton1_Click(object sender, EventArgs e)
        {
            if (MessageBox.Show("Apagar todos os dados?", "Confirma��o", MessageBoxButtons.YesNo, MessageBoxIcon.Warning, MessageBoxDefaultButton.Button2) == DialogResult.Yes)
            {
                //File.Delete(Arquivo);
                //dDirf = new dDirf();
                dDirf.IR.Clear();
                dDirf.WriteXml(Arquivo);
                //Le();
                //iRBindingSource.DataSource = dDirf;
            }
        }

        private dDirf.IRCompRow rowComplementar(dDirf.IRRow rowPrincipal,string Tipo)
        {
            dDirf.IRCompRow retorno = dDirf.IRComp.FindByCNPJCodigoTipo(rowPrincipal.CNPJ, rowPrincipal.Codigo, Tipo);
            if (retorno == null)
            {
                retorno = dDirf.IRComp.NewIRCompRow();
                retorno.CNPJ = rowPrincipal.CNPJ;
                retorno.Codigo = rowPrincipal.Codigo;
                retorno.Tipo = Tipo;
                for (int i = 1; i < 13; i++)
                    retorno[string.Format("V{0}", i)] = 0;
                dDirf.IRComp.AddIRCompRow(retorno);
            }
            return retorno;
        }

        private void simpleButton2_Click(object sender, EventArgs e)
        {
            string strCNPJ;
            long valCNPJ;
            int Codigo;
            VirInput.Importador.cImportador Imp = new VirInput.Importador.cImportador("IR");
            Imp.AddColunaImp("CNPJ", "CNPJ", typeof(string));
            Imp.AddColunaImp("Nome", "Nome", typeof(string));
            Imp.AddColunaImp("Mes", "M�s", typeof(DateTime));
            Imp.AddColunaImp("Valor", "Valor", typeof(decimal));
            Imp.AddColunaImp("Codigo", "C�digo", typeof(int));
            Imp.AddColunaImp("IR1", "IMP Retido 1", typeof(decimal));
            Imp.AddColunaImp("IR2", "IMP Retido 2", typeof(decimal));
            Imp.AddColunaImp("IR3", "IMP Retido 3", typeof(decimal));
            Imp.AddColunaImp("IR4", "IMP Retido 4", typeof(decimal));
            Imp.AddColunaImp("RTPO", "Previd�ncia oficial", typeof(decimal));
            Imp.AddColunaImp("RTPP", "Previd�ncia Privada", typeof(decimal));
            Imp.AddColunaImp("RTPA", "Pens�o Aliment�cea", typeof(decimal));
            Imp.AddColunaImp("RTDP", "Dependentes", typeof(decimal));
            Imp.AddColunaImp("13", "13�", typeof(decimal));
            Imp.Carrega();
            if (Imp.ShowEmPopUp() == DialogResult.OK)
            {
                if (!Imp.IsColunaImplementada("CNPJ"))
                    return;
                if (!Imp.IsColunaImplementada("Nome"))
                    return;
                if (!Imp.IsColunaImplementada("Mes"))
                    return;
                if (!Imp.IsColunaImplementada("Valor"))
                    return;

                bool primeira = true;
                foreach (System.Data.DataRow DR in Imp._DataTable.Rows)
                {
                    if (primeira)
                    {
                        primeira = false;
                        continue;
                    }
                    strCNPJ = (string)DR["CNPJ"];
                    Codigo = (DR["Codigo"] == DBNull.Value) ? 0 : (int)DR["Codigo"];
                    //Codigo = (int)DR["Codigo"];
                    if (strCNPJ == "")
                        continue;
                    if (Codigo == 0)
                        continue;
                    if (DR["Valor"] == DBNull.Value)
                        continue;
                    if (DR["Nome"] == DBNull.Value)
                        continue;
                    CPFCNPJ cnpj = new CPFCNPJ(strCNPJ);
                    if ((cnpj.Tipo == DocBacarios.TipoCpfCnpj.CNPJ) || (cnpj.Tipo == DocBacarios.TipoCpfCnpj.CPF))
                    {
                        valCNPJ = cnpj.Valor;
                        dDirf.IRRow rowIR = dDirf.IR.FindByCNPJCodigo(valCNPJ, Codigo);
                        if (rowIR == null)
                        {
                            rowIR = dDirf.IR.NewIRRow();
                            rowIR.CNPJ = valCNPJ;
                            rowIR.Codigo = Codigo;
                            rowIR.Nome = DR["Nome"].ToString();
                            dDirf.IR.AddIRRow(rowIR);
                            for (int i = 1; i < 13; i++)
                            {
                                rowIR["V" + i.ToString()] = 0;
                                rowIR["I" + i.ToString()] = 0;
                            }
                        }
                        DateTime datames = (DateTime)DR["Mes"];
                        int Mes = datames.Month;
                        decimal valorAnterior = (rowIR["V" + Mes.ToString()] == DBNull.Value) ? 0 : (decimal)rowIR["V" + Mes.ToString()];
                        rowIR["V" + Mes.ToString()] = valorAnterior + (decimal)DR["Valor"];
                        if ((Imp.IsColunaImplementada("13")) && (DR["13"] != DBNull.Value) && ((decimal)DR["13"] != 0))
                            rowIR["V13"] = (decimal)DR["13"];
                        valorAnterior = (rowIR["I" + Mes.ToString()] == DBNull.Value) ? 0 : (decimal)rowIR["I" + Mes.ToString()];
                        if ((Imp.IsColunaImplementada("IR1")) && (DR["IR1"] != DBNull.Value))
                            valorAnterior += (decimal)DR["IR1"];
                        if ((Imp.IsColunaImplementada("IR2")) && (DR["IR2"] != DBNull.Value))
                            valorAnterior += (decimal)DR["IR2"];
                        if ((Imp.IsColunaImplementada("IR3")) && (DR["IR3"] != DBNull.Value))
                            valorAnterior += (decimal)DR["IR3"];
                        if ((Imp.IsColunaImplementada("IR4")) && (DR["IR4"] != DBNull.Value))
                            valorAnterior += (decimal)DR["IR4"];

                        rowIR["I" + Mes.ToString()] = valorAnterior;

                        if (cnpj.Tipo == DocBacarios.TipoCpfCnpj.CPF)
                            foreach (string CodigoDirf in new string[] { "RTPO", "RTPP", "RTDP", "RTPA" })
                            {
                                if ((Imp.IsColunaImplementada(CodigoDirf)) && (DR[CodigoDirf] != DBNull.Value))
                                {
                                    dDirf.IRCompRow rowC = rowComplementar(rowIR, CodigoDirf);
                                    rowC[string.Format("V{0}", Mes)] = (decimal)rowC[string.Format("V{0}", Mes)] + (decimal)DR[CodigoDirf];
                                }
                            }
                    }
                }
            }
        }

        private void simpleButton3_Click(object sender, EventArgs e)
        {
            Grava();
            System.Windows.Forms.SaveFileDialog OF = new SaveFileDialog();
            OF.DefaultExt = ".txt";
            if (OF.ShowDialog() == DialogResult.OK)
            {
                dllDirf2011.DIRF dirf = new dllDirf2011.DIRF();
                if (dirf.gerarArquivo(OF.FileName, dDirf))
                    MessageBox.Show(string.Format("Arquivo gerado"));
                else
                    if (dirf.UltimoErro == null)
                        MessageBox.Show(string.Format("Erro Arquivo n�o gravado"));
                    else
                        MessageBox.Show(string.Format("Erro:{0}\r\n\r\n{1}", dirf.UltimoErro.Message, dirf.UltimoErro.StackTrace));
            }
        }
    }
}