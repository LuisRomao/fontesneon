using System;
using System.Collections.Generic;
using System.Text;
using System.Xml;
using System.Xml.Schema;
using System.Xml.Serialization;
using System.IO;
using System.Security.Cryptography.Xml;
using System.Security.Cryptography.X509Certificates;

namespace GeraNFS_e
{
    public class Class1
    {
        public static bool isValid;
        public static string Erro;

        public static void MyValidationEventHandler(object sender,
                                            ValidationEventArgs args)
        {
            isValid = false;
            Erro += string.Format("Validation event\n\n{0} - {1}\r\n\r\n",args.Message,args.Exception.Message);
        }

        public void Assina()
        {
            System.Security.Cryptography.X509Certificates.X509Certificate2 x509 = new System.Security.Cryptography.X509Certificates.X509Certificate2();
        }

        public bool teste()
        {
            EnviarLoteRpsEnvio TesteInicial = new EnviarLoteRpsEnvio();

            //TesteInicial.Cnpj = "55055651000170";
            TesteInicial.Cnpj = "06888260000121";
            //TesteInicial.InscricaoMunicipal = "46653";
            TesteInicial.InscricaoMunicipal = "161504";
            TesteInicial.NumeroLote = 1;
            TesteInicial.QuantidadeRps = 1;
            
            TcRps Rps = new TcRps();
            Rps.IdentificacaoRps = new tcIdentificacaoRps();
            
            Rps.IdentificacaoRps.Numero = 1;
            Rps.IdentificacaoRps.Serie = "A";
            Rps.IdentificacaoRps.Tipo = 1;
            Rps.DataEmissao = new DateTime(2011,2,26);
            Rps.NaturezaOperacao = 1;
            Rps.OptanteSimplesNacional = 2;
            Rps.IncentivadorCultural = 2;
            Rps.Status = 1;
            Rps.Servico = new TcDadosServico();
            Rps.Servico.Valores = new TcValores();
            Rps.Servico.Valores.ValorServicos = "500";
            Rps.Servico.Valores.IssRetido = 1;
            Rps.Servico.Valores.ValorInss = "10";
            Rps.Servico.Valores.BaseCalculo = "500";
            Rps.Servico.Valores.Aliquota = 0.02M;
            Rps.Servico.Valores.ValorLiquidoNfse = "90";
            Rps.Servico.Valores.ValorIssRetido = "10";
            Rps.Servico.ItemListaServico = "1";
            Rps.Servico.Discriminacao = "Administracao";
            Rps.Servico.MunicipioPrestacaoServico = 3557006;
            Rps.Prestador = new tcIdentificacaoPrestador();
            Rps.Prestador.Cnpj = "55055651000170";
            Rps.Prestador.InscricaoMunicipal = "46653";
            Rps.Tomador = new tcDadosTomador();
            Rps.Tomador.IdentificacaoTomador = new tcIdentificacaoTomador();
            Rps.Tomador.IdentificacaoTomador.CpfCnpj = new TcCpfCnpj();
            Rps.Tomador.IdentificacaoTomador.CpfCnpj.Item = "05389942000127"; 
            Rps.Tomador.RazaoSocial = "COND. ED. MANSAO DAS ACACIAS";
            TesteInicial.ListaRps = new TcRps[] { Rps };
            //TesteInicial.Signature = new SignatureType();
           
            /*
            TesteInicial.Signature.SignatureValue = new SignatureValueType();
            TesteInicial.Signature.SignedInfo = new SignedInfoType();

            TesteInicial.Signature.SignedInfo.SignatureMethod = new SignatureMethodType();
            TesteInicial.Signature.SignedInfo.SignatureMethod.Algorithm = "x";
            TesteInicial.Signature.SignedInfo.CanonicalizationMethod = new CanonicalizationMethodType();
            TesteInicial.Signature.SignedInfo.CanonicalizationMethod.Algorithm = "x";
            TesteInicial.Signature.SignedInfo.Reference = new ReferenceType[] { new ReferenceType()};
            TesteInicial.Signature.SignedInfo.Reference[0].Transforms = new TransformType[] { new TransformType() };
            TesteInicial.Signature.SignedInfo.Reference[0].Transforms[0].Algorithm = "x";
            TesteInicial.Signature.SignedInfo.Reference[0].DigestMethod = new DigestMethodType();
            TesteInicial.Signature.SignedInfo.Reference[0].DigestMethod.Algorithm = "";
            TesteInicial.Signature.SignedInfo.Reference[0].DigestValue = new byte[0];
            */

            XmlSerializer serializer = new XmlSerializer(typeof(EnviarLoteRpsEnvio));
            using (FileStream fs = new FileStream("c:\\lixo\\testeinicial.xml", FileMode.Create))
            {

                serializer.Serialize(fs, TesteInicial);
            }


            StreamReader SR;
            string _stringXml;
            SR = File.OpenText("c:\\lixo\\testeinicial.xml");
            _stringXml = SR.ReadToEnd();
            SR.Close();
            
            //
            // cria cert
            //
            //X509Certificate2 cert = new X509Certificate2();
            X509Certificate2 cert;
            //
            // seleciona certificado do repositório MY do windows
            //
            Certificado certificado = new Certificado();
            cert = certificado.BuscaNome("");


            XmlDocument Assinado = AssinaXML2.AssinarMensagem(_stringXml, cert);
            //Assinado.Save("c:\\lixo\\testeinicial_assinado2.xml");
            Assinado.Save("c:\\lixo\\testeinicial_assinadoSA.xml");


            /*
            //Outra forma

            //
            // realiza assinatura
            //
            AssinaturaDigital AD = new AssinaturaDigital();

            int resultado = AD.Assinar(_stringXml, "EnviarLoteRpsEnvio", cert);
            if (resultado == 0)
            {
                //
                // grava arquivo assinado
                //
                StreamWriter SW;
                SW = File.CreateText("c:\\lixo\\testeinicial_assinado1.xml");
                SW.Write(AD.XMLStringAssinado);
                SW.Close();
            }

            else 
            {
                Erro = AD.mensagemResultado;
                return false;
            }


            Erro = AD.mensagemResultado;
            */


            //XmlTextReader r = new XmlTextReader("c:\\lixo\\testeinicial_assinado2.xml");
            XmlTextReader r = new XmlTextReader("c:\\lixo\\testeinicial_assinadoSA.xml");

            XmlValidatingReader v = new XmlValidatingReader(r);
            v.ValidationType = ValidationType.Schema;

            v.Schemas.Add("http://www.ginfes.com.br/servico_enviar_lote_rps_envio", "c:\\lixo\\servico_enviar_lote_rps_envio_v02.xsd");
            v.ValidationEventHandler += new ValidationEventHandler(MyValidationEventHandler);


            isValid = true;
            Erro = "";

            while (v.Read())
            {
                // Can add code here to process the content.
            }
            v.Close();

            //XmlSchemaSet sc = new XmlSchemaSet();
            //sc.Add(

            //XmlReaderSettings settings = new XmlReaderSettings();
            //settings.ValidationType = ValidationType.Schema;
            //settings.Schemas.Add("http://www.ginfes.com.br/servico_enviar_lote_rps_envio", "c:\\lixo\\servico_enviar_lote_rps_envio_v02.xsd");
            //settings.ValidationEventHandler += new ValidationEventHandler(ValidationCallBack);
            //XmlReader reader = XmlReader.Create(Ms, settings);

            // Parse the file. 
            //while (reader.Read()) ;


            return isValid;
        }
    }
}
