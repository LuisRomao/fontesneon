/*
14/01/2015 14.1.4.113  LH Ajuste para 2015 - Registro DIRF
*/

using System;
using ArquivosTXT;
using DocBacarios;

namespace dllDirf2011
{
    /// <summary>
    /// Gerador de arquivos para DIRF
    /// </summary>
    public class DIRF
    {
        private cTabelaTXT ArquivoSaida;
        /// <summary>
        /// Registro DIRF
        /// </summary>
        public RegDIRF regDIRF;
        /// <summary>
        /// Registro RESPO
        /// </summary>
        public RegRESPO regRESPO;
        /// <summary>
        /// DECPJ
        /// </summary>
        public RegDECPJ regDECPJ;
        private RegIDREC regIDREC;
        private RegBPJDEC regBPJDEC;
        private RegBPFDEC regBPFDEC;
        private RegRT regRT;
        private RegFIMDIRF regFIMDIRF;
        /// <summary>
        /// �ltimo erro
        /// </summary>
        public Exception UltimoErro;
        //public dDirf dDirf;
       
        /// <summary>
        /// Gera arquivo
        /// </summary>
        /// <param name="Nome"></param>
        /// <param name="dDirf"></param>
        /// <returns></returns>
        public bool gerarArquivo(string Nome, dDirf dDirf)
        {
            bool retorno = true;
            try
            {
                ArquivoSaida = new cTabelaTXT(ModelosTXT.delimitado);
                ArquivoSaida.Separador = new char[] { '|' };
                ArquivoSaida.Formatodata = FormatoData.yyyyMMdd;
                ArquivoSaida.IniciaGravacao(Nome);
                regDIRF = new RegDIRF(ArquivoSaida, null, dDirf);
                regRESPO = new RegRESPO(ArquivoSaida, null);
                regDECPJ = new RegDECPJ(ArquivoSaida, null);
                regIDREC = new RegIDREC(ArquivoSaida, null);
                regBPJDEC = new RegBPJDEC(ArquivoSaida, null);
                regBPFDEC = new RegBPFDEC(ArquivoSaida, null);
                regRT = new RegRT(ArquivoSaida, null);
                regFIMDIRF = new RegFIMDIRF(ArquivoSaida, null);


                //if (dDirf != null)
                //{
                //if (regDIRF == null)
                //{
                //    UltimoErro = new Exception("Arquivo n�o aberto");
                //    return false;
                //};
                regDIRF.GravaLinha();
                regRESPO.Configura(dDirf.cpfResponsavel, dDirf.NomeResponsavel, dDirf.DDD, dDirf.Fone, dDirf.Ramal, dDirf.Fax, dDirf.Email);
                regRESPO.GravaLinha();
                regDECPJ.Configura(dDirf.cnpjDeclarante, dDirf.NomeDeclarante, dDirf.cpfDeclarante);
                regDECPJ.GravaLinha();


                if (dDirf.IR.Count > 0)
                {
                    System.Collections.SortedList Ordem = new System.Collections.SortedList();
                    foreach (dDirf.IRRow row in dDirf.IR)
                        Ordem.Add(string.Format("{0:0000} - {1:00000000000000}", row.Codigo, row.CNPJ), row);

                    int CodigoAnterior = 0;
                    foreach (System.Collections.DictionaryEntry DE in Ordem)
                    {
                        dDirf.IRRow rowIR = (dDirf.IRRow)DE.Value;
                        if (CodigoAnterior != rowIR.Codigo)
                        {
                            CodigoAnterior = rowIR.Codigo;
                            regIDREC.Configura(CodigoAnterior);
                            regIDREC.GravaLinha();
                        };
                        CPFCNPJ cpfcnpj = new CPFCNPJ(rowIR.CNPJ);
                        if ((cpfcnpj.Tipo == TipoCpfCnpj.CNPJ) || (cpfcnpj.Valor == 0))
                        {
                            regBPJDEC.Configura(rowIR.CNPJ, rowIR.Nome);
                            regBPJDEC.GravaLinha();
                        }
                        else
                        {
                            regBPFDEC.Configura(rowIR.CNPJ, rowIR.Nome);
                            regBPFDEC.GravaLinha();
                        }
                        if (rowIR.IsV13Null())
                            rowIR.V13 = 0;
                        regRT.Configura("RTRT", new decimal[13] { rowIR.V1, rowIR.V2, rowIR.V3, rowIR.V4, rowIR.V5, rowIR.V6, rowIR.V7, rowIR.V8, rowIR.V9, rowIR.V10, rowIR.V11, rowIR.V12, rowIR.V13 });
                        regRT.GravaLinha();
                        regRT.Configura("RTIRF", new decimal[12] { rowIR.I1, rowIR.I2, rowIR.I3, rowIR.I4, rowIR.I5, rowIR.I6, rowIR.I7, rowIR.I8, rowIR.I9, rowIR.I10, rowIR.I11, rowIR.I12 });
                        if (!regRT.zerado())
                            regRT.GravaLinha();
                        foreach(dDirf.IRCompRow rowd in rowIR.GetIRCompRows())
                        {
                            regRT.Configura(rowd.Tipo, new decimal[12] { rowd.V1, rowd.V2, rowd.V3, rowd.V4, rowd.V5, rowd.V6, rowd.V7, rowd.V8, rowd.V9, rowd.V10, rowd.V11, rowd.V12 });
                            regRT.GravaLinha();
                        }
                    }




                }



                regFIMDIRF.GravaLinha();
            }
            catch (Exception e)
            {
                UltimoErro = e;
                retorno = false;
            }
            finally
            {
                if (!ArquivoSaida.TerminaGravacao())
                    retorno = false;
            }
            //}
            //else
            //    return false;
            return retorno;
        }
    }

    /*
    internal enum TiposRegistro
    {
        Invalido = -1,
        DIEF = 0,
        RESPO = 1,
        HeaderEmpresaAdd = 12,
        RegistroTrabalhador = 30
    }
    */

    /// <summary>
    /// Registro DIRF
    /// </summary>
    public class RegDIRF : RegistroBase
    {
        /// <summary>
        /// Construtor
        /// </summary>
        /// <param name="_cTabTXT"></param>
        /// <param name="Lido"></param>
        /// <param name="dDirf"></param>
        public RegDIRF(cTabelaTXT _cTabTXT, string Lido, dDirf dDirf)
            : base(_cTabTXT)
        {
            CarregaLinha(Lido);
            this.dDirf = dDirf;
        }

        private dDirf dDirf;

        /// <summary>
        /// Ano ref
        /// </summary>
        public int AnoReferencia
        {
            get { return (dDirf != null) ? dDirf.AnoReferencia : 0; }
        }

        /// <summary>
        /// Ano calendario
        /// </summary>
        public int AnoCalendario
        {
            get { return (dDirf != null) ? dDirf.AnoCalendario : 0; }
        }

        /// <summary>
        /// Mapa
        /// </summary>
        protected override void RegistraMapa()
        {                                
            base.RegistraMapa();
            RegistraMapa("TipoRegistro", 1, 4, "Dirf");
            RegistraFortementeTipado("AnoReferencia", 2, 4);     //2011);
            RegistraFortementeTipado("AnoCalendario", 3, 4);     //2010);
            RegistraMapa("Retificadora", 4, 1, "N");
            RegistraMapa("NumeroRecibo", 5, 0, "");
            //RegistraMapa("Estrutura", 6, 7, "7C2DE7J"); // vers�o 2013                        
            //RegistraMapa("Estrutura", 6, 7, "F8UCL6S"); // vers�o 2014
            //RegistraMapa("Estrutura", 6, 7, "M1LB5V2"); // vers�o 2015
            //RegistraMapa("Estrutura", 6, 7, "L35QJS2"); // vers�o 2016
            //RegistraMapa("Estrutura", 6, 7, "P49VS72"); // vers�o 2017
            RegistraMapa("Estrutura", 6, 7, "Q84FV63"); // vers�o 2018
        }
    }

    /// <summary>
    /// Registro RESPO
    /// </summary>
    public class RegRESPO : RegistroBase
    {
        /// <summary>
        /// Construtor
        /// </summary>
        /// <param name="_cTabTXT"></param>
        /// <param name="Lido"></param>
        public RegRESPO(cTabelaTXT _cTabTXT, string Lido)
            : base(_cTabTXT)
        {
            CarregaLinha(Lido);
        }

        /// <summary>
        /// 
        /// </summary>
        public bool Configurado;               

        /// <summary>
        /// 
        /// </summary>
        /// <param name="cpfResp"></param>
        /// <param name="NomeResp"></param>
        /// <param name="DDDResp"></param>
        /// <param name="TelefoneResp"></param>
        /// <param name="RamalResp"></param>
        /// <param name="FaxResp"></param>
        /// <param name="EmailResp"></param>
        public void Configura(CPFCNPJ cpfResp,string NomeResp,int DDDResp,int TelefoneResp,int RamalResp,int FaxResp,string EmailResp)
        {
            Configurado = true;
            CPF = cpfResp.Valor;
            Nome = NomeResp;
            DDD = DDDResp;
            Telefone = TelefoneResp;
            Ramal = RamalResp;
            Fax = FaxResp;
            Email = EmailResp;
        }

        private long _CPF;

        /// <summary>
        /// 
        /// </summary>
        public long CPF
        {
            get
            {
                return _CPF;
            }
            set
            {
                _CPF = value;
            }
        }

        private string nome;

        /// <summary>
        /// 
        /// </summary>
        public string Nome
        {
            get { return nome; }
            set { nome = value; }
        }

        private int dDD;

        /// <summary>
        /// 
        /// </summary>
        public int DDD 
        {
            get { return dDD; }
            set { dDD = value; }
        }

        private int telefone;

        /// <summary>
        /// 
        /// </summary>
        public int Telefone
        {
            get { return telefone; }
            set { telefone = value; }
        }

        private int ramal;

        /// <summary>
        /// 
        /// </summary>
        public int Ramal
        {
            get { return ramal; }
            set { ramal = value; }
        }

        private int fax;

        /// <summary>
        /// 
        /// </summary>
        public int Fax
        {
            get { return fax; }
            set { fax = value; }
        }

        private string email;

        /// <summary>
        /// 
        /// </summary>
        public string Email
        {
            get { return email; }
            set { email = value; }
        }

        /// <summary>
        /// 
        /// </summary>
        protected override void RegistraMapa()
        {
            base.RegistraMapa();
            RegistraMapa("TipoRegistro", 1, 5, "RESPO");
            RegistraFortementeTipado("CPF", 2, 11);
            RegistraFortementeTipado("Nome", 3, 60,true);
            RegistraFortementeTipado("DDD", 4, 2);
            RegistraFortementeTipado("Telefone", 5, 8);
            RegistraFortementeTipado("Ramal", 6,6,true);
            RegistraFortementeTipado("Fax", 7,8);
            RegistraFortementeTipado("Email", 8,50,true);                          
        }
    }

    /// <summary>
    /// 
    /// </summary>
    public class RegDECPJ : RegistroBase
    {
        /// <summary>
        /// 
        /// </summary>
        public RegDECPJ(cTabelaTXT _cTabTXT, string Lido)
            : base(_cTabTXT)
        {
            CarregaLinha(Lido);
        }

        /// <summary>
        /// 
        /// </summary>
        public bool Configurado;

        /// <summary>
        /// 
        /// </summary>
        public void Configura(CPFCNPJ CNPJDec, string NomeDec, CPFCNPJ CPFDec)
        {
            Configurado = true;
            CNPJ = CNPJDec.Valor;
            Nome = NomeDec;
            CPF = CPFDec.Valor;
        }

        private long _CNPJ;

        /// <summary>
        /// 
        /// </summary>
        public long CNPJ
        {
            get
            {
                return _CNPJ;
            }
            set
            {
                _CNPJ = value;
            }
        }

        private long _CPF;

        /// <summary>
        /// 
        /// </summary>
        public long CPF
        {
            get
            {
                return _CPF;
            }
            set
            {
                _CPF = value;
            }
        }

        private string nome;

        /// <summary>
        /// 
        /// </summary>
        public string Nome
        {
            get { return nome; }
            set { nome = value; }
        }

        /// <summary>
        /// 
        /// </summary>
        protected override void RegistraMapa()
        {
            base.RegistraMapa();
            RegistraMapa("TipoRegistro", 1, 5, "DECPJ");
            RegistraFortementeTipado("CNPJ", 2, 14);
            RegistraFortementeTipado("Nome", 3, 150, true);
            RegistraMapa("Natureza", 4, 1, 0);
            RegistraFortementeTipado("CPF", 5, 11);
            RegistraMapa("Ostensivo", 6, 1, "N");
            RegistraMapa("Depositario", 7, 1, "N");
            RegistraMapa("Administradora", 8, 1, "N");
            RegistraMapa("Exterior", 9, 1, "N");
            RegistraMapa("PlanoSaude", 10, 1, "N");
            //RegistraMapa("Copa", 11, 1, "N"); //removido em 2017
            RegistraMapa("Olimpiada", 11, 1, "N");
            RegistraMapa("Siaf", 12, 1, "N");
            RegistraMapa("Especial", 13, 1, "N");
            RegistraMapa("DataEvento", 14, 0, "");
        }
    }

    /// <summary>
    /// 
    /// </summary>
    public class RegIDREC : RegistroBase
    {
        /// <summary>
        /// 
        /// </summary>
        public RegIDREC(cTabelaTXT _cTabTXT, string Lido)
            : base(_cTabTXT)
        {
            CarregaLinha(Lido);
        }

        /// <summary>
        /// 
        /// </summary>
        public bool Configurado;

        /// <summary>
        /// 
        /// </summary>
        public void Configura(int IDCodigo)
        {
            Configurado = true;
            Codigo = IDCodigo;
        }

        private int codigo;

        /// <summary>
        /// 
        /// </summary>
        public int Codigo
        {
            get
            {
                return codigo;
            }
            set
            {
                codigo = value;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        protected override void RegistraMapa()
        {
            base.RegistraMapa();
            RegistraMapa("TipoRegistro", 1, 5, "IDREC");
            RegistraFortementeTipado("Codigo", 2, 4);            
        }
    }

    /// <summary>
    /// 
    /// </summary>
    public class RegBPJDEC : RegistroBase
    {
        /// <summary>
        /// 
        /// </summary>
        public RegBPJDEC(cTabelaTXT _cTabTXT, string Lido)
            : base(_cTabTXT)
        {
            CarregaLinha(Lido);
        }

        /// <summary>
        /// 
        /// </summary>
        public void Configura(long CNPJben,string Nomeben)
        {
            CNPJ = CNPJben;
            Nome = Nomeben;
        }

        private long _CNPJ;

        /// <summary>
        /// 
        /// </summary>
        public long CNPJ
        {
            get
            {
                return _CNPJ;
            }
            set
            {
                _CNPJ = value;
            }
        }       

        private string nome;

        /// <summary>
        /// 
        /// </summary>
        public string Nome
        {
            get { return nome; }
            set { nome = value; }
        }

        /// <summary>
        /// 
        /// </summary>
        protected override void RegistraMapa()
        {
            base.RegistraMapa();
            RegistraMapa("TipoRegistro", 1, 6, "BPJDEC");
            RegistraFortementeTipado("CNPJ", 2, 14);
            RegistraFortementeTipado("Nome", 3, 150, true);        
        }
    }

    /// <summary>
    /// 
    /// </summary>
    public class RegBPFDEC : RegistroBase
    {
        /// <summary>
        /// 
        /// </summary>
        public RegBPFDEC(cTabelaTXT _cTabTXT, string Lido)
            : base(_cTabTXT)
        {
            CarregaLinha(Lido);
        }

        /// <summary>
        /// 
        /// </summary>
        public void Configura(long CNPJben, string Nomeben)
        {
            CPF = CNPJben;
            Nome = Nomeben;
        }

        private long _CPF;

        /// <summary>
        /// 
        /// </summary>
        public long CPF
        {
            get
            {
                return _CPF;
            }
            set
            {
                _CPF = value;
            }
        }

        private string nome;

        /// <summary>
        /// 
        /// </summary>
        public string Nome
        {
            get { return nome; }
            set { nome = value; }
        }

        /// <summary>
        /// 
        /// </summary>
        protected override void RegistraMapa()
        {
            base.RegistraMapa();
            RegistraMapa("TipoRegistro", 1, 6, "BPFDEC");
            RegistraFortementeTipado("CPF", 2, 11);
            RegistraFortementeTipado("Nome", 3, 60, true);
            RegistraMapa("Branco", 4, 0, "");
            RegistraMapa("N1", 5, 1, "N");
            RegistraMapa("N2", 6, 1, "N");
        }
    }

    /// <summary>
    /// 
    /// </summary>
    public class RegRT : RegistroBase
    {
        /// <summary>
        /// 
        /// </summary>
        public RegRT(cTabelaTXT _cTabTXT, string Lido)
            : base(_cTabTXT)
        {
            CarregaLinha(Lido);
        }

        internal bool zerado()
        {
            for (int i = 0; i < valores.Length; i++)
                if (valores[i] != 0)
                    return false;
            return true;
        }

        /// <summary>
        /// 
        /// </summary>
        public void Configura(string TipoRT, decimal[] ValoresRT)
        {
            TipoRegistro = TipoRT;
            valores = ValoresRT;
        }

        private string tipo;

        /// <summary>
        /// 
        /// </summary>
        public string TipoRegistro
        {
            get
            {
                return tipo;
            }
            set
            {
                tipo = value;
            }
        }

        private decimal RetornoValor(int i)
        {
            if (valores == null)
                return 0;
            if (i >= valores.Length)
                return 0;
            return valores[i];

        }

        private decimal[] valores;

        /// <summary>
        /// 
        /// </summary>
        public decimal V1 { get { return RetornoValor(0); } }
        /// <summary>
        /// 
        /// </summary>
        public decimal V2 { get { return RetornoValor(1); } }
        /// <summary>
        /// 
        /// </summary>
        public decimal V3 { get { return RetornoValor(2); } }
        /// <summary>
        /// 
        /// </summary>
        public decimal V4 { get { return RetornoValor(3); } }
        /// <summary>
        /// 
        /// </summary>
        public decimal V5 { get { return RetornoValor(4); } }
        /// <summary>
        /// 
        /// </summary>
        public decimal V6 { get { return RetornoValor(5); } }
        /// <summary>
        /// 
        /// </summary>
        public decimal V7 { get { return RetornoValor(6); } }
        /// <summary>
        /// 
        /// </summary>
        public decimal V8 { get { return RetornoValor(7); } }
        /// <summary>
        /// 
        /// </summary>
        public decimal V9 { get { return RetornoValor(8); } }
        /// <summary>
        /// 
        /// </summary>
        public decimal V10 { get { return RetornoValor(9); } }
        /// <summary>
        /// 
        /// </summary>
        public decimal V11 { get { return RetornoValor(10); } }
        /// <summary>
        /// 
        /// </summary>
        public decimal V12 { get { return RetornoValor(11); } }
        /// <summary>
        /// 
        /// </summary>
        public string V13
        { 
            get 
            {
                if (RetornoValor(12) == 0)
                    return "";
                else 
                    return string.Format("{0,3:f0}", 100*RetornoValor(12)); 
            } 
        }

        /// <summary>
        /// 
        /// </summary>
        protected override void RegistraMapa()
        {
            base.RegistraMapa();
            RegistraFortementeTipado("TipoRegistro", 1, 5, true);
            RegistraFortementeTipado("V1", 2, 15, true);
            RegistraFortementeTipado("V2", 3, 15, true);
            RegistraFortementeTipado("V3", 4, 15, true);
            RegistraFortementeTipado("V4", 5, 15, true);
            RegistraFortementeTipado("V5", 6, 15, true);
            RegistraFortementeTipado("V6", 7, 15, true);
            RegistraFortementeTipado("V7", 8, 15, true);
            RegistraFortementeTipado("V8", 9, 15, true);
            RegistraFortementeTipado("V9", 10, 15, true);
            RegistraFortementeTipado("V10", 11, 15, true);
            RegistraFortementeTipado("V11", 12, 15, true);
            RegistraFortementeTipado("V12", 13, 15, true);
            RegistraFortementeTipado("V13", 14, 15, true);            
            //RegistraMapa("V13", 14, 0,"");
        }
    }

    /// <summary>
    /// 
    /// </summary>
    public class RegFIMDIRF : RegistroBase
    {
        /// <summary>
        /// 
        /// </summary>
        public RegFIMDIRF(cTabelaTXT _cTabTXT, string Lido)
            : base(_cTabTXT)
        {
            CarregaLinha(Lido);
        }

        /// <summary>
        /// 
        /// </summary>
        protected override void RegistraMapa()
        {            
            base.RegistraMapa();
            RegistraMapa("TipoRegistro", 1, 7, "FIMDirf");            
        }
    }
}
