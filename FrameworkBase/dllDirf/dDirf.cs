﻿using DocBacarios;

namespace dllDirf2011
{


    partial class dDirf
    {
        private DadosGeraisRow PegaRegistro(string Nome)
        {
            DadosGeraisRow Registro = DadosGerais.FindByCampo(Nome);
            if (Registro == null)
            {
                Registro = DadosGerais.NewDadosGeraisRow();
                Registro.Campo = Nome;
                Registro.Valor = "";
                DadosGerais.AddDadosGeraisRow(Registro);
            }
            return Registro;
        }

        private int LeRegistro(string Nome)
        {
            DadosGeraisRow Registro = PegaRegistro(Nome);
            int intLido;
            if (!int.TryParse(Registro.Valor, out intLido))
            {
                intLido = 0;
                Registro.Valor = "0";
            }
            return intLido;
        }

        private long LeRegistrolong(string Nome)
        {
            DadosGeraisRow Registro = PegaRegistro(Nome);
            long longLido;
            if (!long.TryParse(Registro.Valor, out longLido))
            {
                longLido = 0;
                Registro.Valor = "0";
            }
            return longLido;
        }

        private CPFCNPJ LeRegistroCPFCNPJ(string Nome)
        {
            return new CPFCNPJ(LeRegistrolong(Nome));
        }

        /// <summary>
        /// 
        /// </summary>
        public DocBacarios.CPFCNPJ cpfResponsavel
        {
            get { return LeRegistroCPFCNPJ("cpfResponsavel"); }
            set { PegaRegistro("cpfResponsavel").Valor = value.ToString(false); }
        }

        /// <summary>
        /// 
        /// </summary>
        public string NomeResponsavel
        {
            get { return PegaRegistro("NomeResponsavel").Valor; }
            set { PegaRegistro("NomeResponsavel").Valor = value; }
        }

        /// <summary>
        /// 
        /// </summary>
        public int DDD
        {
            get { return LeRegistro("DDD"); }
            set { PegaRegistro("DDD").Valor = value.ToString(); }
        }

        /// <summary>
        /// 
        /// </summary>
        public int Fone
        {
            get { return LeRegistro("Fone"); }
            set { PegaRegistro("Fone").Valor = value.ToString(); }
        }

        /// <summary>
        /// 
        /// </summary>
        public int Ramal
        {
            get { return LeRegistro("Ramal"); }
            set { PegaRegistro("Ramal").Valor = value.ToString(); }
        }

        /// <summary>
        /// 
        /// </summary>
        public int Fax
        {
            get { return LeRegistro("Fax"); }
            set { PegaRegistro("Fax").Valor = value.ToString(); }
        }

        /// <summary>
        /// 
        /// </summary>
        public int AnoReferencia
        {
            get { return LeRegistro("AnoReferencia"); }
            set { PegaRegistro("AnoReferencia").Valor = value.ToString(); }
        }

        /// <summary>
        /// 
        /// </summary>
        public int AnoCalendario
        {
            get { return LeRegistro("AnoCalendario"); }
            set { PegaRegistro("AnoCalendario").Valor = value.ToString(); }
        }

        /// <summary>
        /// 
        /// </summary>
        public string Email
        {
            get { return PegaRegistro("Email").Valor; }
            set { PegaRegistro("Email").Valor = value; }
        }

        /// <summary>
        /// 
        /// </summary>
        public DocBacarios.CPFCNPJ cnpjDeclarante
        {
            get { return LeRegistroCPFCNPJ("cnpjDeclarante"); }
            set { PegaRegistro("cnpjDeclarante").Valor = value.ToString(false); }
        }

        /// <summary>
        /// 
        /// </summary>
        public DocBacarios.CPFCNPJ cpfDeclarante
        {
            get { return LeRegistroCPFCNPJ("cpfDeclarante"); }
            set { PegaRegistro("cpfDeclarante").Valor = value.ToString(false); }
        }

        /// <summary>
        /// 
        /// </summary>
        public string NomeDeclarante
        {
            get { return PegaRegistro("NomeDeclarante").Valor; }
            set { PegaRegistro("NomeDeclarante").Valor = value; }
        }
    }
}
