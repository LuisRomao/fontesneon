using System;
using System.Collections;
using System.IO;
using System.Reflection;
using System.Xml;
using System.Xml.Schema;
using System.Xml.Serialization;
//using CompontesBasicos.ObjetosEstaticos;
using CompontesBasicosProc;
using DocBacarios;

namespace dllEmissorNFS_e
{
    /// <summary>
    /// 
    /// </summary>
    public enum VerssaoXSD
    {
        /// <summary>
        /// 
        /// </summary>
        v202,
        /// <summary>
        /// 
        /// </summary>
        v300 
    }

    /// <summary>
    /// 
    /// </summary>
    public enum StatusLote
    {
        /// <summary>
        /// pode incluir notas
        /// </summary>
        aberto = 0,
        /// <summary>
        /// Gerado para arquivo
        /// </summary>
        geradoManual = 1,
        /// <summary>
        /// Gerado pelo WS
        /// </summary>
        geradoWS = 2,
        /// <summary>
        /// Retornado
        /// </summary>
        retornado = 3
    }

    /// <summary>
    /// 
    /// </summary>
    public enum StatusRPS
    {
        /// <summary>
        /// 
        /// </summary>
        NaoEnviada = 0,
        /// <summary>
        /// 
        /// </summary>
        ComErro = 1,
        /// <summary>
        /// 
        /// </summary>
        Convertida = 2,
        /// <summary>
        /// 
        /// </summary>
        Enviada = 3,
        /// <summary>
        /// 
        /// </summary>
        Cancelada = 4
    }

    /// <summary>
    /// 
    /// </summary>
    public enum ArquivoNaoAssinado
    {
        /// <summary>
        /// 
        /// </summary>
        Gravar,
        /// <summary>
        /// 
        /// </summary>
        RAM,
        /// <summary>
        /// 
        /// </summary>
        NaoAssinar
    }

    /// <summary>
    /// 
    /// </summary>
    public class LoteRPS
    {
        //public string NomeCertificado = "";
        
        private dllEmissorNFS_e_v202.EnviarLoteRpsEnvio Lote;
        private dllEmissorNFS_e_v300.EnviarLoteRpsEnvio Lote3;

        internal dllEmissorNFS_e_v202.tcIdentificacaoPrestador Prestador;
        internal dllEmissorNFS_e_v300.tcIdentificacaoPrestador Prestador3;            

        private VerssaoXSD Ver;      

        private TListaRps ListaRps;

        /// <summary>
        /// 
        /// </summary>
        public bool Erro;
        /// <summary>
        /// 
        /// </summary>
        public string strErro;
        /// <summary>
        /// 
        /// </summary>
        public long protocolo;
        /// <summary>
        /// 
        /// </summary>
        public string Avisos = "";
        /// <summary>
        /// 
        /// </summary>
        public DateTime DataEnvio;
        /// <summary>
        /// 
        /// </summary>
        public string ItemListaServico = "1712";// "1252"; //LC 116/2003
        /// <summary>
        /// 
        /// </summary>
        public int CodigoCnae = 1702;
        /// <summary>
        /// 
        /// </summary>
        public string CodigoTributacaoMunicipio = "17.11/142002/1252";

        internal System.Globalization.CultureInfo Cultura;

        //internal decimal AliquotaIss;
        internal int Municipio;

        private long NLote;        

        /// <summary>
        /// 
        /// </summary>
        /// <param name="Ver"></param>
        /// <param name="cnpj"></param>
        /// <param name="InscricaoMunicipal"></param>
        /// <param name="Municipio"></param>
        /// <param name="NLote"></param>
        public LoteRPS(VerssaoXSD Ver, CPFCNPJ cnpj, int InscricaoMunicipal, int Municipio, long NLote)
        {                        
            this.Ver = Ver;
            this.Municipio = Municipio;
            if (Municipio == 3547809)
            {
                // tentativa 1 "17.12" "17.12/17.11 / 682260000"
                // tentativa 2 "1712" "17.12/17.11 / 682260000"
                // tentativa 3 "1712" "17.12/17.11/682260000"
                // tentativa 4 "1712" "17.12/17.11/68226000"
                // tentativa 5 "17.12" "17.11 / 682260000"
                // tentativa 5 "1712" "17.11 / 682260000"
                ItemListaServico = "1712";// antigo "1711";
                CodigoCnae = 1702;
                CodigoTributacaoMunicipio = "17.11 / 682260000";// antigo "682260000";
            }
            this.NLote = NLote;
            Cultura = new System.Globalization.CultureInfo("en-us");
            ListaRps = new TListaRps();
            Erro = false;
            strErro = "";      
            if (cnpj.Tipo != TipoCpfCnpj.CNPJ)
            {
                Erro = true;
                strErro = string.Format("CNPJ inv�lido. Tipo: {0} Valor: {1}",cnpj.Tipo,cnpj);
                return;
            }
            if (Ver == VerssaoXSD.v202)
            {
                Lote = new dllEmissorNFS_e_v202.EnviarLoteRpsEnvio();
                Lote.NumeroLote = NLote;
                Lote.Cnpj = cnpj.ToString(false);                
                Lote.InscricaoMunicipal = InscricaoMunicipal.ToString();
                //Lote.QuantidadeRps //Gerado automaticamente                
                //Lote.ListaRps      //Gerado a partir dos RPSs
                Prestador = new dllEmissorNFS_e_v202.tcIdentificacaoPrestador();                
                Prestador.Cnpj = cnpj.ToString(false);
                Prestador.InscricaoMunicipal = InscricaoMunicipal.ToString();
            }
            else if (Ver == VerssaoXSD.v300)
            {
                Lote3 = new dllEmissorNFS_e_v300.EnviarLoteRpsEnvio();
                Lote3.LoteRps = new dllEmissorNFS_e_v300.tcLoteRps();
                Lote3.LoteRps.Id = NLote.ToString();
                Lote3.LoteRps.NumeroLote = NLote.ToString();
                Lote3.LoteRps.Cnpj = cnpj.ToString(false);
                Lote3.LoteRps.InscricaoMunicipal = InscricaoMunicipal.ToString();
                //Lote3.LoteRps.QuantidadeRps //Gerado automaticamente                
                //Lote3.LoteRps.ListaRps      //Gerado a partir dos RPSs

                Prestador3 = new dllEmissorNFS_e_v300.tcIdentificacaoPrestador();
                Prestador3.Cnpj = cnpj.ToString(false);
                Prestador3.InscricaoMunicipal = InscricaoMunicipal.ToString();
            }            
        }

        /// <summary>
        /// Ativa ou n�o o log
        /// </summary>
        public bool LogAtivado = true;
        private System.Text.StringBuilder log;

        /// <summary>
        /// 
        /// </summary>
        /// <param name="Caminho"></param>
        /// <param name="Gravar"></param>
        /// <returns></returns>
        public bool GeraXML(string Caminho,ArquivoNaoAssinado Gravar)
        {
            //System.Windows.Forms.MessageBox.Show("TESTE");
            log = new System.Text.StringBuilder();
            log.AppendFormat("Inicio {0} caminho {1}\r\n", DateTime.Now, Caminho);
            try
            {
                log.AppendFormat("ListaRps.Count = {0}\r\n",  ListaRps.Count);
                if (ListaRps.Count == 0)
                {
                    Erro = true;
                    strErro = "Nenhuma nota incluida no lote";
                    return false;
                };
                MemoryStream Ms = new MemoryStream();
                if (Ver == VerssaoXSD.v202)
                {
                    Lote.QuantidadeRps = ListaRps.Count;
                    Lote.ListaRps = ListaRps.ToArrayV2();
                    XmlSerializer serializer = new XmlSerializer(typeof(dllEmissorNFS_e_v202.EnviarLoteRpsEnvio));
                    if (Gravar != ArquivoNaoAssinado.RAM)
                        using (FileStream fs = new FileStream(string.Format("{0}{1}_{2}_$.xml", Caminho, Lote.Cnpj, Lote.NumeroLote), FileMode.Create))
                        {
                            serializer.Serialize(fs, Lote);
                        };
                    if (Gravar == ArquivoNaoAssinado.NaoAssinar)
                        return true;
                    serializer.Serialize(Ms, Lote);
                }
                else if (Ver == VerssaoXSD.v300)
                {
                    Lote3.LoteRps.QuantidadeRps = ListaRps.Count;
                    Lote3.LoteRps.ListaRps = ListaRps.ToArrayV3();
                    XmlSerializer serializer3 = new XmlSerializer(typeof(dllEmissorNFS_e_v300.EnviarLoteRpsEnvio));
                    if (Gravar != ArquivoNaoAssinado.RAM)
                        using (FileStream fs = new FileStream(string.Format("{0}{1}_{2}_V3$.xml", Caminho, Lote3.LoteRps.Cnpj, Lote3.LoteRps.NumeroLote), FileMode.Create))
                        {
                            serializer3.Serialize(fs, Lote3);
                        };
                    if (Gravar == ArquivoNaoAssinado.NaoAssinar)
                        return true;
                    serializer3.Serialize(Ms, Lote3);
                };
                XmlDocument xmlDoc = new XmlDocument();
                xmlDoc.PreserveWhitespace = true;
                Ms.Position = 0;

                //Onde queremos que fique uma quebra de linha colocamos o marcador |Quebra|
                Ms.Position = 0;
                string str_xml = Ms.Limpa(FuncoesBasicasProc.TiposLimpesa.RemoveBrancosDuplicados, FuncoesBasicasProc.TiposLimpesa.RemoveQuebrasLinha);
                str_xml = str_xml.Replace("|Quebra|","\r\n");
                xmlDoc.LoadXml(str_xml);


                //xmlDoc.LoadXml(StringEdit.Limpa(Ms, new StringEdit.TiposLimpesa[] { StringEdit.TiposLimpesa.RemoveBrancosDuplicados, StringEdit.TiposLimpesa.RemoveQuebrasLinha }));
                if (Ver == VerssaoXSD.v300)
                    Apoio.ApoioST.AjustaDecimaisDatas(xmlDoc, typeof(dllEmissorNFS_e_v300.EnviarLoteRpsEnvio), true, false, true);

                XmlDocument Assinado;
                if (Certificado.CertificadoST.certificado == null)
                    return false;
                log.AppendFormat("Vai assinar\r\n");
                if (Ver == VerssaoXSD.v202)
                    Assinado = Certificado.CertificadoST.AssinarMensagem(xmlDoc);
                else
                {
                    Assinado = Certificado.CertificadoST.AssinarTag(xmlDoc, "InfRps");
                    //Assinado.Save("c:\\lixo\\umaass.xml");
                    Assinado = Certificado.CertificadoST.AssinarTag(Assinado, "LoteRps");
                }
                if (Assinado == null)
                {
                    strErro = "N�o assinado";
                    return false;
                }
                log.AppendFormat("Assinado\r\n");
                string Arquivo;
                if (Ver == VerssaoXSD.v202)
                    Arquivo = string.Format("{0}{1}_{2}_V2.xml", Caminho, Lote.Cnpj, Lote.NumeroLote);
                else
                    Arquivo = string.Format("{0}{1}_{2}_V3.xml", Caminho, Lote3.LoteRps.Cnpj, Lote3.LoteRps.NumeroLote);

                if ((Ver == VerssaoXSD.v202) || (Gravar == ArquivoNaoAssinado.Gravar) || (Apoio.FazerValidacao))
                    Assinado.Save(Arquivo);

                log.AppendFormat("Gravado Vai validar\r\n");

                if (Apoio.FazerValidacao)
                {
                    log.AppendFormat("Validando\r\n");
                    if (!Certificado.CertificadoST.ValidarAssinatura(Assinado))
                    {
                        strErro = "Erro na valida��o da assinatura digital";
                        return false;
                    };
                    if (!Apoio.ApoioST.Valida(Ver == VerssaoXSD.v202 ? Apoio.TipoServico.servico_enviar_lote_rps_envio_v02 : Apoio.TipoServico.servico_enviar_lote_rps_envio_v03, Arquivo))
                    {
                        strErro = Apoio.ApoioST.strErro;
                        string ArquivoErro = string.Format("{0}\\{1}_erro.xml", Path.GetDirectoryName(Arquivo), Path.GetFileNameWithoutExtension(Arquivo));
                        if (File.Exists(ArquivoErro))
                            File.Delete(ArquivoErro);
                        File.Move(Arquivo, ArquivoErro);

                        string ArquivoErroTXT = string.Format("{0}\\{1}_erro.txt", Path.GetDirectoryName(Arquivo), Path.GetFileNameWithoutExtension(Arquivo));
                        if (File.Exists(ArquivoErroTXT))
                            File.Delete(ArquivoErroTXT);
                        System.IO.File.WriteAllText(ArquivoErroTXT, strErro);
                        return false;
                    }
                }
                if (Ver == VerssaoXSD.v202)
                    return true;
                else
                {
                    log.AppendFormat("Vai enviar\r\n");
                    string ArquivoResposta = string.Format("{0}\\{1}_Retorno.xml", Path.GetDirectoryName(Arquivo), Path.GetFileNameWithoutExtension(Arquivo));
                    log.AppendFormat("Arquivo resposta = {0}\r\n", ArquivoResposta);
                    return EnviarLote(Assinado.OuterXml, ArquivoResposta, Gravar);
                }
            }
            finally
            {
                if (LogAtivado)
                {                    
                    string NomeLog = string.Format(@"c:\sistema neon\arquivos\log{0:yyyy_MM_dd_HH_mm_ss}.log",DateTime.Now);                    
                    File.WriteAllText(NomeLog, log.ToString());
                }
            }

        }
        
        /// <summary>
        /// 
        /// </summary>
        /// <param name="XMLLote"></param>
        /// <param name="NomeArquivoResposta"></param>
        /// <param name="Gravar"></param>
        /// <returns></returns>
        public bool EnviarLote(string XMLLote,string NomeArquivoResposta, ArquivoNaoAssinado Gravar)
        {
            log.AppendFormat("EnviarLote :{0}:{1}:{2}\r\n", XMLLote, NomeArquivoResposta, Gravar);
            if (!Certificado.CertificadoST.VaiAssinar)
            {
                protocolo = 1;
                DataEnvio = DateTime.Now;
                return true;
            }
            protocolo = 0;
            log.AppendFormat("Apoio.ApoioST.virWSEnvia\r\n");
            string resposta = Apoio.ApoioST.virWSEnvia(Apoio.TipoServico.servico_enviar_lote_rps_envio_v03, XMLLote, NomeArquivoResposta, Gravar);
            log.AppendFormat("Resposta ----------------------------\r\n{0}\r\n----------------------------\r\n", resposta);
            
            //resposta = resposta.Replace("<ListaMensagemRetorno>", "<ns2:ListaMensagemRetorno>");
            //resposta = resposta.Replace("</ListaMensagemRetorno>", "</ns2:ListaMensagemRetorno>");
            XmlSerializer serializer = new XmlSerializer(typeof(dllEmissorNFS_e_v300.EnviarLoteRpsResposta));
            System.IO.MemoryStream Ms = new MemoryStream();
            StreamWriter Sw = new StreamWriter(Ms);
            Sw.Write(resposta);
            Sw.Flush();
            Ms.Position = 0;
            log.AppendFormat("RespostaEnviarLote 1\r\n");
            dllEmissorNFS_e_v300.EnviarLoteRpsResposta RespostaEnviarLote = (dllEmissorNFS_e_v300.EnviarLoteRpsResposta)serializer.Deserialize(Ms);
            log.AppendFormat("RespostaEnviarLote 2\r\n");
            if (RespostaEnviarLote.ItemsElementName != null)
            {
                log.AppendFormat("RespostaEnviarLote.ItemsElementName.Length {0}\r\n", RespostaEnviarLote.ItemsElementName.Length);
                for (int i = 0; i < RespostaEnviarLote.ItemsElementName.Length; i++)
                    switch (RespostaEnviarLote.ItemsElementName[i])
                    {
                        case dllEmissorNFS_e_v300.ItemsChoiceType3.NumeroLote:
                            log.AppendFormat("Nlote i={0} para converter para inteiro |{1}|\r\n",i, RespostaEnviarLote.Items[i].ToString());
                            long Loteretornado = long.Parse(RespostaEnviarLote.Items[i].ToString());
                            if (NLote != Loteretornado)
                                Avisos += string.Format("Diverg�ncia no n�mero do lote: Enviado = {0} Retornado = {1}\r\n", NLote, Loteretornado);
                            break;
                        case dllEmissorNFS_e_v300.ItemsChoiceType3.Protocolo:
                            log.AppendFormat("protocolo i={0} para converter para inteiro |{1}|\r\n", i, RespostaEnviarLote.Items[i].ToString());
                            protocolo = long.Parse(RespostaEnviarLote.Items[i].ToString());
                            break;
                        case dllEmissorNFS_e_v300.ItemsChoiceType3.DataRecebimento:
                            DataEnvio = (DateTime)RespostaEnviarLote.Items[i];
                            break;
                        case dllEmissorNFS_e_v300.ItemsChoiceType3.ListaMensagemRetorno:
                            Avisos += Apoio.GeraAviso((dllEmissorNFS_e_v300.ListaMensagemRetorno)RespostaEnviarLote.Items[i]);                            
                            break;
                    }

                return (protocolo != 0);
            }
            else
                return false;
        }        

        /// <summary>
        /// 
        /// </summary>
        public enum RetIss
        {
            /// <summary>
            /// 
            /// </summary>
            comRetencao,
            /// <summary>
            /// 
            /// </summary>
            semRetencao
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="Valor"></param>
        /// <param name="AliquotaIss"></param>
        /// <param name="Ret"></param>
        /// <param name="Tomador"></param>
        /// <param name="NumeroRPS"></param>
        /// <param name="Descricao"></param>
        /// <param name="DataEmissaoRPS"></param>
        /// <param name="ValorPIS"></param>
        /// <param name="ValorCofins"></param>
        /// <param name="ValorCsll"></param>
        /// <param name="ValorIR"></param>
        /// <returns></returns>
        public bool IncluirRPS(decimal Valor, 
                               decimal AliquotaIss, 
                               RetIss Ret, 
                               virTomador Tomador, 
                               long NumeroRPS, 
                               string Descricao, 
                               DateTime DataEmissaoRPS, 
                               decimal? ValorPIS = null,
                               decimal? ValorCofins = null,
                               decimal? ValorCsll = null,
                               decimal? ValorIR = null)
        {
            if (Valor <= 0)
            {
                Erro = true;
                strErro = "Valor inv�lido";
                return false;
            }
            virNota NovaNota = new virNota(this, NumeroRPS, Valor, AliquotaIss / 100, Ret, Tomador,Descricao,DataEmissaoRPS,ValorPIS,ValorCofins,ValorCsll,ValorIR);
            ListaRps.Add(NovaNota);
            return true;
        }                

        /// <summary>
        /// 
        /// </summary>
        public class virNota
        {
            /// <summary>
            /// 
            /// </summary>
            public dllEmissorNFS_e_v202.TcRps Nota;
            /// <summary>
            /// 
            /// </summary>
            public dllEmissorNFS_e_v300.tcRps Nota3;

            /// <summary>
            /// 
            /// </summary>
            public virNota()
            { 
            }

            /// <summary>
            /// 
            /// </summary>
            /// <param name="Mestre"></param>
            /// <param name="NumeroRPS"></param>
            /// <param name="ValorServico"></param>
            /// <param name="AliquotaIss"></param>
            /// <param name="Ret"></param>
            /// <param name="Tomador"></param>
            /// <param name="Descricao"></param>
            /// <param name="DataEmissaoRPS"></param>
            /// <param name="ValorPIS"></param>
            /// <param name="ValorCofins"></param>
            /// <param name="ValorCsll"></param>
            /// <param name="ValorIR"></param>
            public virNota(LoteRPS Mestre, 
                           long NumeroRPS, 
                           decimal ValorServico, 
                           decimal AliquotaIss, 
                           RetIss Ret, 
                           virTomador Tomador, 
                           string Descricao, 
                           DateTime DataEmissaoRPS,
                           decimal? ValorPIS,
                           decimal? ValorCofins,
                           decimal? ValorCsll,
                           decimal? ValorIR
                           )
            {
                decimal Retencoes = ValorPIS.GetValueOrDefault(0) + ValorCofins.GetValueOrDefault(0) + ValorCsll.GetValueOrDefault(0) + ValorIR.GetValueOrDefault(0);
                if (Mestre.Ver == VerssaoXSD.v202)
                {
                    Nota = new dllEmissorNFS_e_v202.TcRps();

                    Nota.IdentificacaoRps = new dllEmissorNFS_e_v202.tcIdentificacaoRps();
                    Nota.IdentificacaoRps.Numero = NumeroRPS;
                    Nota.IdentificacaoRps.Serie = "1"; //Equipamento emissor (seria o caixa ou algo do tipo)
                    Nota.IdentificacaoRps.Tipo = 1; // 1 - RPS                    
                    //Nota.DataEmissao = new DateTime(DateTime.Today.Ticks, DateTimeKind.Unspecified);
                    Nota.DataEmissao = new DateTime(DateTime.Today.Ticks, DateTimeKind.Unspecified);
                    Nota.NaturezaOperacao = 1; //tributa��o no municipio
                    //Nota.RegimeEspecialTributacao
                    Nota.OptanteSimplesNacional = 2;
                    Nota.IncentivadorCultural = 2;
                    Nota.Status = 1; //Normal (2 = cancelada) 
                    //Nota.RpsSubstituido

                    Nota.Servico = new dllEmissorNFS_e_v202.TcDadosServico();
                    Nota.Servico.Valores = new dllEmissorNFS_e_v202.TcValores();

                    Nota.Servico.Valores.ValorServicos = ValorServico.ToString("F2", Mestre.Cultura);
                    //Nota.Servico.Valores.ValorDeducoes
                    if (ValorPIS.HasValue)
                        Nota.Servico.Valores.ValorPis = ValorPIS.Value.ToString("F2", Mestre.Cultura);
                    if (ValorCofins.HasValue)
                        Nota.Servico.Valores.ValorCofins = ValorCofins.Value.ToString("F2", Mestre.Cultura);
                    //Nota.Servico.Valores.ValorInss
                    if (ValorIR.HasValue)
                        Nota.Servico.Valores.ValorIr = ValorIR.Value.ToString("F2", Mestre.Cultura);
                    if (ValorCsll.HasValue)
                        Nota.Servico.Valores.ValorCsll = ValorCsll.Value.ToString("F2", Mestre.Cultura);                                   
                    decimal ValorISS = Math.Round(ValorServico * AliquotaIss, 2, MidpointRounding.AwayFromZero);
                    Nota.Servico.Valores.ValorIss = ValorISS.ToString("F2", Mestre.Cultura);
                    decimal RetencaoISS = (Ret == RetIss.comRetencao ? ValorISS : 0);
                    //Nota.Servico.Valores.OutrasRetencoes
                    Nota.Servico.Valores.BaseCalculo = Nota.Servico.Valores.ValorServicos;
                    Nota.Servico.Valores.Aliquota = AliquotaIss;
                    Nota.Servico.Valores.ValorLiquidoNfse = ((decimal)(ValorServico - RetencaoISS - Retencoes)).ToString("F2", Mestre.Cultura);
                    if (Ret == RetIss.comRetencao)
                        Nota.Servico.Valores.ValorIssRetido = RetencaoISS.ToString("F2", Mestre.Cultura);                    

                    Nota.Servico.ItemListaServico = Mestre.ItemListaServico;// "1712";//  LC 116/2003
                    Nota.Servico.CodigoCnae = Mestre.CodigoCnae;// 1702;
                    Nota.Servico.CodigoTributacaoMunicipio = Mestre.CodigoTributacaoMunicipio;// "17.11/142002/1252";// "1711";
                    //Nota.Servico.CodigoTributacaoMunicipio = testeCT;
                    Nota.Servico.Discriminacao = Descricao; 
                    Nota.Servico.MunicipioPrestacaoServico = Mestre.Municipio; //sbc 3557006
                    Nota.Prestador = Mestre.Prestador;
                    //Nota.Prestador = new dllEmissorNFS_e_v202.tcIdentificacaoPrestador();
                    //Nota.Prestador.Cnpj = Mestre.Lote.Cnpj;
                    //Nota.Prestador.InscricaoMunicipal = Mestre.Lote.InscricaoMunicipal;
                    Nota.Tomador = Tomador.Tomador;
                    //Nota.IntermediarioServico
                    //Nota.ConstrucaoCivil                
                }
                else if (Mestre.Ver == VerssaoXSD.v300)
                {
                    Nota3 = new dllEmissorNFS_e_v300.tcRps();
                    Nota3.InfRps = new dllEmissorNFS_e_v300.tcInfRps();
                    Nota3.InfRps.Id = "RPS" + NumeroRPS.ToString();
                    Nota3.InfRps.IdentificacaoRps = new dllEmissorNFS_e_v300.tcIdentificacaoRps();
                    Nota3.InfRps.IdentificacaoRps.Numero = NumeroRPS.ToString();
                    Nota3.InfRps.IdentificacaoRps.Serie = "1";//Equipamento emissor (seria o caixa ou algo do tipo)
                    Nota3.InfRps.IdentificacaoRps.Tipo = 1; // 1 - RPS
                    //Nota3.InfRps.DataEmissao = new DateTime(DateTime.Today.Ticks, DateTimeKind.Unspecified);
                    Nota3.InfRps.DataEmissao = DataEmissaoRPS;
                    Nota3.InfRps.NaturezaOperacao = 1; //tributa��o no municipio
                    //Nota3.InfRps.RegimeEspecialTributacao // C�digo de identifica��o do regime especial de tributa��o
                    Nota3.InfRps.OptanteSimplesNacional = 2;
                    Nota3.InfRps.IncentivadorCultural = 2;
                    Nota3.InfRps.Status = 1;
                    //Nota3.InfRps.RpsSubstituido
                    Nota3.InfRps.Servico = new dllEmissorNFS_e_v300.tcDadosServico();
                    Nota3.InfRps.Servico.Valores = new dllEmissorNFS_e_v300.tcValores();
                    Nota3.InfRps.Servico.Valores.ValorServicos = Math.Round(ValorServico,2,MidpointRounding.AwayFromZero);
                    //Nota3.InfRps.Servico.Valores.ValorDeducoes
                    if (ValorPIS.HasValue)
                    {
                        Nota3.InfRps.Servico.Valores.ValorPis = ValorPIS.Value;
                        Nota3.InfRps.Servico.Valores.ValorPisSpecified = true;
                    };
                    if (ValorCofins.HasValue)
                    {
                        Nota3.InfRps.Servico.Valores.ValorCofinsSpecified = true;
                        Nota3.InfRps.Servico.Valores.ValorCofins = ValorCofins.Value;
                    };
                    //Nota3.InfRps.Servico.Valores.ValorInss
                    if (ValorIR.HasValue)
                    {
                        Nota3.InfRps.Servico.Valores.ValorIrSpecified = true;
                        Nota3.InfRps.Servico.Valores.ValorIr = ValorIR.Value;
                    };
                    if (ValorCsll.HasValue)
                    {
                        Nota3.InfRps.Servico.Valores.ValorCsllSpecified = true;
                        Nota3.InfRps.Servico.Valores.ValorCsll = ValorCsll.Value;
                    }
                    Nota3.InfRps.Servico.Valores.IssRetido = (Ret == RetIss.comRetencao ? (sbyte)1 : (sbyte)2);
                    decimal ValorISS = Math.Round(ValorServico * AliquotaIss, 2, MidpointRounding.AwayFromZero);
                    Nota3.InfRps.Servico.Valores.ValorIssSpecified = true;
                    Nota3.InfRps.Servico.Valores.ValorIss = ValorISS;
                    decimal Retencao = (Ret == RetIss.comRetencao ? ValorISS : 0);
                    //Nota3.InfRps.Servico.Valores.OutrasRetencoes                    
                    Nota3.InfRps.Servico.Valores.BaseCalculo = Nota3.InfRps.Servico.Valores.ValorServicos;
                    Nota3.InfRps.Servico.Valores.Aliquota = AliquotaIss;
                    Nota3.InfRps.Servico.Valores.ValorLiquidoNfseSpecified = true;
                    Nota3.InfRps.Servico.Valores.ValorLiquidoNfse = ValorServico - Retencao - Retencoes;
                    if (Ret == RetIss.comRetencao)
                    {
                        Nota3.InfRps.Servico.Valores.ValorIssRetidoSpecified = true;
                        Nota3.InfRps.Servico.Valores.ValorIssRetido = Retencao;
                    }

                    //Nota3.InfRps.Servico.Valores.v

                    Nota3.InfRps.Servico.ItemListaServico = Mestre.ItemListaServico;// "1712";// "1252"; //LC 116/2003
                    Nota3.InfRps.Servico.CodigoCnae = Mestre.CodigoCnae;// 1702;
                    Nota3.InfRps.Servico.CodigoTributacaoMunicipio = Mestre.CodigoTributacaoMunicipio;// "17.11/142002/1252";
                    Nota3.InfRps.Servico.Discriminacao = Descricao;
                    Nota3.InfRps.Servico.CodigoMunicipio = Mestre.Municipio;
                    Nota3.InfRps.Prestador = Mestre.Prestador3;                    
                    Nota3.InfRps.Tomador = Tomador.Tomador3;
                    //Nota3.InfRps.IntermediarioServico
                    //Nota3.InfRps.ConstrucaoCivil
                        /*
                    
                    
                    
                    Nota.Servico = new dllEmissorNFS_e_v202.TcDadosServico();
                    
                    Nota.Prestador = new dllEmissorNFS_e_v202.tcIdentificacaoPrestador();
                    Nota.Prestador.Cnpj = Mestre.Lote.Cnpj;
                    Nota.Prestador.InscricaoMunicipal = Mestre.Lote.InscricaoMunicipal;
                    Nota.Tomador = Tomador.Tomador;
                    
                    */
                }
            }
        }

        /// <summary>
        /// 
        /// </summary>
        public class virTomador
        {
            internal dllEmissorNFS_e_v202.tcDadosTomador Tomador;
            internal dllEmissorNFS_e_v300.tcDadosTomador Tomador3;

            /// <summary>
            /// 
            /// </summary>
            /// <param name="Versao"></param>
            /// <param name="cnpjTomador"></param>
            /// <param name="RazaoSocial"></param>
            /// <param name="Endereco"></param>
            /// <param name="Numero"></param>
            /// <param name="Bairro"></param>
            /// <param name="Municipio"></param>
            /// <param name="Estado"></param>
            /// <param name="Cep"></param>
            /// <param name="Telefone"></param>
            /// <param name="Email"></param>
            public virTomador(VerssaoXSD Versao, CPFCNPJ cnpjTomador, string RazaoSocial, string Endereco, string Numero, string Bairro, int Municipio, string Estado, int Cep, string Telefone, string Email)
            {
                if (Versao == VerssaoXSD.v202)
                {
                    Tomador = new dllEmissorNFS_e_v202.tcDadosTomador();
                    Tomador.IdentificacaoTomador = new dllEmissorNFS_e_v202.tcIdentificacaoTomador();
                    Tomador.IdentificacaoTomador.CpfCnpj = new dllEmissorNFS_e_v202.TcCpfCnpj();
                    Tomador.IdentificacaoTomador.CpfCnpj.Item = cnpjTomador.ToString(false);
                    Tomador.IdentificacaoTomador.CpfCnpj.ItemElementName = dllEmissorNFS_e_v202.ItemChoiceType.Cnpj;
                    //Tomador.IdentificacaoTomador.InscricaoMunicipal
                    Tomador.RazaoSocial = RazaoSocial;
                    Tomador.Endereco = new dllEmissorNFS_e_v202.TcEndereco();
                    Tomador.Endereco.Endereco = Endereco;
                    Tomador.Endereco.Numero = Numero;
                    //Tomador.Endereco.Complemento
                    Tomador.Endereco.Bairro = Bairro;
                    if (Municipio != 0)
                    {
                        Tomador.Endereco.CidadeSpecified = true;
                        Tomador.Endereco.Cidade = Municipio;
                    }
                    else
                        Tomador.Endereco.CidadeSpecified = false;
                    Tomador.Endereco.Estado = Estado;
                    Tomador.Endereco.Cep = Cep.ToString("00000000");
                    Tomador.Contato = new dllEmissorNFS_e_v202.TcContato();
                    //Tomador.Contato.gTelefone = Telefone;
                    Tomador.Contato.Email = Email;
                }
                else if (Versao == VerssaoXSD.v300)
                {
                    Tomador3 = new dllEmissorNFS_e_v300.tcDadosTomador();
                    Tomador3.IdentificacaoTomador = new dllEmissorNFS_e_v300.tcIdentificacaoTomador();
                    Tomador3.IdentificacaoTomador.CpfCnpj = new dllEmissorNFS_e_v300.tcCpfCnpj();
                    Tomador3.IdentificacaoTomador.CpfCnpj.Item = cnpjTomador.ToString(false);
                    Tomador3.IdentificacaoTomador.CpfCnpj.ItemElementName = dllEmissorNFS_e_v300.ItemChoiceType.Cnpj;
                    //Tomador3.IdentificacaoTomador.InscricaoMunicipal
                    Tomador3.RazaoSocial = RazaoSocial;
                    Tomador3.Endereco = new dllEmissorNFS_e_v300.tcEndereco();
                    Tomador3.Endereco.Endereco = Endereco;
                    Tomador3.Endereco.Numero = Numero;
                    //Tomador3.Endereco.Complemento
                    Tomador3.Endereco.Bairro = Bairro;
                    Tomador3.Endereco.CodigoMunicipio = Municipio;
                    Tomador3.Endereco.CodigoMunicipioSpecified = true;
                    Tomador3.Endereco.Uf = Estado;
                    Tomador3.Endereco.CepSpecified = true;
                    Tomador3.Endereco.Cep = Cep;
                    Tomador3.Contato = new dllEmissorNFS_e_v300.tcContato();
                    //Tomador3.Contato.Telefone;                                      
                    Tomador3.Contato.Email = Email;
                }
            }
        }

        private class TListaRps : CollectionBase
        {
            /// <summary>
            /// 
            /// </summary>
            /// <param name="index"></param>
            /// <returns></returns>
            public virNota this[int index]
            {
                get
                {
                    return ((virNota)List[index]);
                }
                set
                {
                    List[index] = value;
                }
            }

            /// <summary>
            /// 
            /// </summary>
            /// <param name="value"></param>
            /// <returns></returns>
            public int Add(virNota value)
            {
                return (List.Add(value));
            }

            /// <summary>
            /// 
            /// </summary>
            /// <param name="value"></param>
            /// <returns></returns>
            public int IndexOf(virNota value)
            {
                return (List.IndexOf(value));
            }

            /// <summary>
            /// 
            /// </summary>
            /// <param name="index"></param>
            /// <param name="value"></param>
            public void Insert(int index, virNota value)
            {
                List.Insert(index, value);
            }

            /// <summary>
            /// 
            /// </summary>
            /// <param name="value"></param>
            public void Remove(virNota value)
            {
                List.Remove(value);
            }

            /// <summary>
            /// 
            /// </summary>
            /// <param name="value"></param>
            /// <returns></returns>
            public bool Contains(virNota value)
            {
                // If value is not of type Int16, this will return false.
                return (List.Contains(value));
            }

            /// <summary>
            /// 
            /// </summary>
            /// <returns></returns>
            public dllEmissorNFS_e_v202.TcRps[] ToArrayV2()
            {
                dllEmissorNFS_e_v202.TcRps[] Retorno = new dllEmissorNFS_e_v202.TcRps[List.Count];
                for (int i = 0; i < List.Count; i++)
                    Retorno[i] = this[i].Nota;
                return Retorno;
            }

            /// <summary>
            /// 
            /// </summary>
            /// <returns></returns>
            public dllEmissorNFS_e_v300.tcRps[] ToArrayV3()
            {
                dllEmissorNFS_e_v300.tcRps[] Retorno = new dllEmissorNFS_e_v300.tcRps[List.Count];
                for (int i = 0; i < List.Count; i++)
                    Retorno[i] = this[i].Nota3;
                return Retorno;
            }

            /// <summary>
            /// 
            /// </summary>
            /// <param name="value"></param>
            protected override void OnValidate(Object value)
            {
                if (value.GetType() != typeof(virNota))
                    throw new ArgumentException("value must be of type virNota.", "value");
            }

        }
    }

    /// <summary>
    /// 
    /// </summary>
    public class NotaGerada
    {
        /// <summary>
        /// 
        /// </summary>
        public long RPS;
        /// <summary>
        /// 
        /// </summary>
        public long CNPJ;
        /// <summary>
        /// 
        /// </summary>
        public int IM;
        /// <summary>
        /// 
        /// </summary>
        public string CodigoVerificacao;
        /// <summary>
        /// 
        /// </summary>
        public DateTime DataEmissao;
        /// <summary>
        /// 
        /// </summary>
        public DateTime Competencia;
        /// <summary>
        /// 
        /// </summary>
        public long Numero;
        /// <summary>
        /// 
        /// </summary>
        public string Aviso;

        /// <summary>
        /// 
        /// </summary>
        /// <param name="CNPJ"></param>
        /// <param name="IM"></param>
        public NotaGerada(long CNPJ, int IM)
        {
            this.CNPJ = CNPJ;
            this.IM = IM;
            Aviso = "";
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="Ver"></param>
        /// <param name="Caminho"></param>
        /// <param name="RPS"></param>
        /// <param name="Gravar"></param>
        /// <returns></returns>
        public bool ConsultaRPS(VerssaoXSD Ver,string Caminho, long RPS, ArquivoNaoAssinado Gravar)
        {            
            this.RPS = RPS;
            Aviso = "";
            if (Ver == VerssaoXSD.v300)
            {
                string Arquivo = string.Format("{0}BuscaNota_{1}_{2}_V3.xml", Caminho, CNPJ, RPS);
                dllEmissorNFS_e_v300.ConsultarNfseRpsEnvio Consulta = new dllEmissorNFS_e_v300.ConsultarNfseRpsEnvio();
                Consulta.IdentificacaoRps = new dllEmissorNFS_e_v300.tcIdentificacaoRps();
                Consulta.IdentificacaoRps.Numero = RPS.ToString();
                Consulta.IdentificacaoRps.Serie = "1";
                Consulta.IdentificacaoRps.Tipo = 1;
                Consulta.Prestador = new dllEmissorNFS_e_v300.tcIdentificacaoPrestador();
                Consulta.Prestador.Cnpj = CNPJ.ToString("00000000000000");// "55055651000170";
                Consulta.Prestador.InscricaoMunicipal = IM.ToString();// "46653";
                XmlSerializer serializer = new XmlSerializer(typeof(dllEmissorNFS_e_v300.ConsultarNfseRpsEnvio));
                MemoryStream Ms = new MemoryStream();
                serializer.Serialize(Ms, Consulta);
                XmlDocument xmlDoc = new XmlDocument();
                xmlDoc.PreserveWhitespace = true;
                Ms.Position = 0;
                xmlDoc.Load(Ms);
                XmlDocument Assinado = Certificado.CertificadoST.AssinarMensagem(xmlDoc);
                if (Assinado == null)
                {
                    Aviso = "N�o assinado";
                    return false;
                }
                if ((Gravar == ArquivoNaoAssinado.Gravar) || (Apoio.FazerValidacao))
                    Assinado.Save(Arquivo);
                if (Apoio.FazerValidacao)
                    if (!Apoio.ApoioST.Valida(Apoio.TipoServico.servico_consultar_nfse_rps_envio_v03, string.Format(Arquivo)))
                        return false;
                string ArquivoResposta = string.Format("{0}\\{1}_Retorno.xml", Path.GetDirectoryName(Arquivo), Path.GetFileNameWithoutExtension(Arquivo));
                string resposta = Apoio.ApoioST.virWSEnvia(Apoio.TipoServico.servico_consultar_nfse_rps_envio_v03, Assinado.OuterXml, ArquivoResposta, Gravar);
                //resposta = resposta.Replace("<ListaMensagemRetorno>", "<ns2:ListaMensagemRetorno>");
                //resposta = resposta.Replace("</ListaMensagemRetorno>", "</ns2:ListaMensagemRetorno>");
                XmlDocument xmlRetorno = new XmlDocument();
                xmlRetorno.LoadXml(resposta);
                Apoio.ApoioST.AjustaDecimaisDatas(xmlRetorno, typeof(dllEmissorNFS_e_v300.tcInfNfse), false, true, false);
                Ms = new MemoryStream();
                xmlRetorno.Save(Ms);
                Ms.Position = 0;
                if (resposta.Contains("ConsultarNfseRpsResposta"))
                {
                    XmlSerializer serializerREC = new XmlSerializer(typeof(dllEmissorNFS_e_v300.ConsultarNfseRpsResposta));
                    dllEmissorNFS_e_v300.ConsultarNfseRpsResposta Resp = (dllEmissorNFS_e_v300.ConsultarNfseRpsResposta)serializerREC.Deserialize(Ms);
                    if (Resp.Item.GetType() == typeof(dllEmissorNFS_e_v300.tcCompNfse))
                    {
                        dllEmissorNFS_e_v300.tcCompNfse CompNfse = (dllEmissorNFS_e_v300.tcCompNfse)Resp.Item;
                        CodigoVerificacao = CompNfse.Nfse.InfNfse.CodigoVerificacao;
                        Competencia = CompNfse.Nfse.InfNfse.Competencia;
                        DataEmissao = CompNfse.Nfse.InfNfse.DataEmissao;
                        Numero = long.Parse(CompNfse.Nfse.InfNfse.Numero.ToString());
                        return true;
                    }
                    else if (Resp.Item.GetType() == typeof(dllEmissorNFS_e_v300.ListaMensagemRetorno))
                    {
                        Aviso = Apoio.GeraAviso((dllEmissorNFS_e_v300.ListaMensagemRetorno)Resp.Item);
                        return false;
                    };
                }
                else
                    if (resposta.Contains("EnviarLoteRpsResposta"))
                    {
                        XmlSerializer serializer2 = new XmlSerializer(typeof(dllEmissorNFS_e_v300.EnviarLoteRpsResposta));                        
                        dllEmissorNFS_e_v300.EnviarLoteRpsResposta RespostaEnviarLote = (dllEmissorNFS_e_v300.EnviarLoteRpsResposta)serializer2.Deserialize(Ms);
                        if (RespostaEnviarLote.ItemsElementName != null)
                        {
                            for (int i = 0; i < RespostaEnviarLote.ItemsElementName.Length; i++)
                                switch (RespostaEnviarLote.ItemsElementName[i])
                                {                                    
                                    case dllEmissorNFS_e_v300.ItemsChoiceType3.ListaMensagemRetorno:
                                        Aviso += Apoio.GeraAviso((dllEmissorNFS_e_v300.ListaMensagemRetorno)RespostaEnviarLote.Items[i]);
                                        break;
                                }

                            return false;
                        }
                        else
                            return false;
                    }
                return false;
            };
            return false;
        }
    }
    
    /// <summary>
    /// 
    /// </summary>
    public class Apoio
    {
        /// <summary>
        /// 
        /// </summary>
        public enum TipoServico
        {
            /// <summary>
            /// 
            /// </summary>
            servico_enviar_lote_rps_envio_v02,
            /// <summary>
            /// 
            /// </summary>
            servico_enviar_lote_rps_envio_v03,
            /// <summary>
            /// 
            /// </summary>
            servico_consultar_lote_rps_envio_v03,
            /// <summary>
            /// 
            /// </summary>
            servico_consultar_nfse_rps_envio_v03
        }

        private static Apoio apoioST;

        /// <summary>
        /// 
        /// </summary>
        public static Apoio ApoioST
        {
            get { return (apoioST == null ? (apoioST = new Apoio()) : apoioST); }
        }

        /// <summary>
        /// 
        /// </summary>
        public static System.Net.IWebProxy Proxy;

        private WS_NFSeV3.ServiceGinfesImplService _WSH;

        private WS_NFSeV3.ServiceGinfesImplService WSH
        {
            get 
            { 
                if(_WSH == null)
                {
                    _WSH = new dllEmissorNFS_e.WS_NFSeV3.ServiceGinfesImplService();
                    if (Proxy != null)
                        _WSH.Proxy = Proxy;
                }
                return _WSH; 
            }
        }

        private WS_NFSeV3Prod.ServiceGinfesImplService _WSProd;

        private WS_NFSeV3Prod.ServiceGinfesImplService WSProd
        {
            get 
            { 
                if(_WSProd == null)
                {
                    _WSProd = new dllEmissorNFS_e.WS_NFSeV3Prod.ServiceGinfesImplService();
                    _WSProd.Proxy = Proxy;
                }
                return _WSProd; 
            }
        }

        private string _stringCab;

        private string stringCab
        {
            get 
            {
                if (_stringCab == null)
                {
                    dllEmissorNFS_e_v300.cabecalho cabecalho = new dllEmissorNFS_e_v300.cabecalho();
                    cabecalho.versao = "3";
                    cabecalho.versaoDados = "3";
                    XmlSerializer serializercabec = new XmlSerializer(typeof(dllEmissorNFS_e_v300.cabecalho));
                    MemoryStream Mscab = new MemoryStream();
                    serializercabec.Serialize(Mscab, cabecalho);
                    Mscab.Position = 0;                    
                    StreamReader sr = new StreamReader(Mscab);
                    _stringCab = sr.ReadToEnd();
                }
                return _stringCab;
            }
        }

        internal static string GeraAviso(dllEmissorNFS_e_v300.ListaMensagemRetorno ListaMen)
        {
            string Avisos = "";
            foreach (dllEmissorNFS_e_v300.tcMensagemRetorno Men in ListaMen.MensagemRetorno)
                Avisos += string.Format("Erro c�digo: {0}\r\nMensagem: {1}\r\nCorre��o:{2}\r\n", Men.Codigo, Men.Mensagem, Men.Correcao);
            return Avisos;
        }                   

        internal string virWSEnvia(TipoServico Serv, string strXML, string Arquivo, ArquivoNaoAssinado Gravar)
        {
            string retorno = "";
            if (CompontesBasicosProc.FuncoesBasicasProc.EstaEmProducao)            
                WSProd.ClientCertificates.Add(Certificado.CertificadoST.certificado);                            
            else            
                WSH.ClientCertificates.Add(Certificado.CertificadoST.certificado);

            switch (Serv)
            {
                case TipoServico.servico_enviar_lote_rps_envio_v03:
                    if (CompontesBasicosProc.FuncoesBasicasProc.EstaEmProducao)
                        retorno = WSProd.RecepcionarLoteRpsV3(stringCab, strXML);
                    else
                        retorno = WSH.RecepcionarLoteRpsV3(stringCab, strXML);
                    break;
                case TipoServico.servico_consultar_nfse_rps_envio_v03:
                    if (CompontesBasicosProc.FuncoesBasicasProc.EstaEmProducao)
                        retorno = WSProd.ConsultarNfsePorRpsV3(stringCab, strXML);
                    else
                        retorno = WSH.ConsultarNfsePorRpsV3(stringCab, strXML);
                    break;
            }            
            if (Gravar == ArquivoNaoAssinado.Gravar)
            {
                File.WriteAllText(Arquivo, retorno);
            }

            //remove bug
            if (retorno.Contains("<ListaMensagemRetorno>"))
            {
                XmlDocument xmldoc = new XmlDocument();
                xmldoc.LoadXml(retorno);
                string prefixo = xmldoc.DocumentElement.GetPrefixOfNamespace("http://www.ginfes.com.br/tipos_v03.xsd");                
                if (prefixo != null)
                    retorno = retorno.Replace("ListaMensagemRetorno>", string.Format("{0}:ListaMensagemRetorno>", prefixo));                                    
            }

            


            return retorno;
        }

        #region Validacao
        /// <summary>
        /// 
        /// </summary>
        public static bool FazerValidacao = false;
        /// <summary>
        /// 
        /// </summary>
        public string strErro;

        private bool isValid;

        /// <summary>
        /// 
        /// </summary>
        /// <param name="Tipo"></param>
        /// <param name="Arquivo"></param>
        /// <returns></returns>
        public bool Valida(TipoServico Tipo, string Arquivo)
        {
            string[] ArquivoXSD;

            switch (Tipo)
            {
                case TipoServico.servico_enviar_lote_rps_envio_v02:
                    ArquivoXSD = new string[] { String.Format("{0}XML\\servico_enviar_lote_rps_envio_v02.xsd", System.AppDomain.CurrentDomain.BaseDirectory), 
                                                @"c:\fontesVS\documenta��o\NFS-e\schemas_v202\servico_enviar_lote_rps_envio_v02.xsd",
                                                "http://www.ginfes.com.br/servico_enviar_lote_rps_envio"};
                    break;
                case TipoServico.servico_enviar_lote_rps_envio_v03:
                    ArquivoXSD = new string[] { String.Format("{0}XML\\servico_enviar_lote_rps_envio_v03.xsd", System.AppDomain.CurrentDomain.BaseDirectory), 
                                                @"c:\fontesVS\documenta��o\NFS-e\schemas_v300\servico_enviar_lote_rps_envio_v03.xsd",
                                                "http://www.ginfes.com.br/servico_enviar_lote_rps_envio_v03.xsd"};
                    break;
                case TipoServico.servico_consultar_lote_rps_envio_v03:
                    ArquivoXSD = new string[] { String.Format("{0}XML\\servico_consultar_lote_rps_envio_v03.xsd", System.AppDomain.CurrentDomain.BaseDirectory), 
                                                @"c:\fontesVS\documenta��o\NFS-e\schemas_v300\servico_consultar_lote_rps_envio_v03.xsd",
                                                "http://www.ginfes.com.br/servico_consultar_lote_rps_envio_v03.xsd"};
                    break;
                case TipoServico.servico_consultar_nfse_rps_envio_v03:
                    ArquivoXSD = new string[] { String.Format("{0}XML\\servico_consultar_nfse_rps_envio_v03.xsd", System.AppDomain.CurrentDomain.BaseDirectory), 
                                                @"c:\fontesVS\documenta��o\NFS-e\schemas_v300\servico_consultar_nfse_rps_envio_v03.xsd",
                                                "http://www.ginfes.com.br/servico_consultar_nfse_rps_envio_v03.xsd"};
                    break;
                default:
                    return false;
            }

            XmlReaderSettings settings = new XmlReaderSettings();
            settings.ValidationType = ValidationType.Schema;
            int iArquivo = 0;
            if (!File.Exists(ArquivoXSD[iArquivo]))
            {
                iArquivo = 1;
                if (!File.Exists(ArquivoXSD[iArquivo]))
                {
                    //throw new Exception(String.Format("Arquivo para valida��o n�o encontrado"));
                    return isValid;
                }
            }
            settings.Schemas.Add(ArquivoXSD[2], ArquivoXSD[iArquivo]);
            settings.ValidationEventHandler += new ValidationEventHandler(ValidationCallBack);
            strErro = "";
            using (XmlReader reader = XmlReader.Create(Arquivo, settings))
            {
                // Parse the file.
                isValid = true;
                while (reader.Read()) ;
            };
            return isValid;
        }

        private void ValidationCallBack(object sender, ValidationEventArgs e)
        {
            strErro += string.Format("Erro tipo: {0}\r\n{1}\r\n", e.Severity, e.Message);
            isValid = false;           
        } 
        #endregion

        #region AjustaDecimaisDatas
        private ArrayList TiposVerificados;
        private ArrayList ListaPropriedadesDec;
        private ArrayList ListaPropriedadesDateTime;

        /// <summary>
        /// 
        /// </summary>
        /// <param name="xmldoc"></param>
        /// <param name="TipoReferencia"></param>
        /// <param name="Decimais"></param>
        /// <param name="Datas"></param>
        /// <param name="colocarUTF8"></param>
        public void AjustaDecimaisDatas(XmlDocument xmldoc, Type TipoReferencia, bool Decimais, bool Datas, bool colocarUTF8)
        {
            AjustaDecimaisDatas(xmldoc, TipoReferencia, Decimais, Datas, "http://www.ginfes.com.br/tipos_v03.xsd", colocarUTF8);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="xmldoc"></param>
        /// <param name="TipoReferencia"></param>
        /// <param name="Decimais"></param>
        /// <param name="Datas"></param>
        /// <param name="Ns"></param>
        /// <param name="colocarUTF8"></param>
        public void AjustaDecimaisDatas(XmlDocument xmldoc, Type TipoReferencia, bool Decimais, bool Datas, string Ns, bool colocarUTF8)
        {
            TiposVerificados = new ArrayList();
            ListaPropriedadesDec = new ArrayList();
            ListaPropriedadesDateTime = new ArrayList();
            AjustaDecimaisRec(TipoReferencia);
            if (Decimais)
                foreach (PropertyInfo Propriedade in ListaPropriedadesDec)
                {
                    XmlNodeList Nos = xmldoc.GetElementsByTagName(Propriedade.Name, Ns);
                    foreach (XmlNode No in Nos)
                    {
                        string Valor = No.InnerText;
                        if ((Valor.Length > 2) && (Valor[Valor.Length - 2] == '.'))
                            No.InnerText = Valor + "0";
                    }
                };
            if (Datas)
                foreach (PropertyInfo Propriedade in ListaPropriedadesDateTime)
                {
                    XmlNodeList Nos = xmldoc.GetElementsByTagName(Propriedade.Name, Ns);
                    foreach (XmlNode No in Nos)
                    {
                        string Valor = No.InnerText;
                        if (Valor.Length == 6)
                            No.InnerText = String.Format("{0}-{1}-01", Valor.Substring(0, 4), Valor.Substring(4, 2));
                    }
                };
            if (colocarUTF8)
            {
                XmlDeclaration xmldecl = (XmlDeclaration)xmldoc.FirstChild;
                xmldecl.Encoding = "UTF-8";
                xmldecl.Standalone = "yes";
            }
        }

        private void AjustaDecimaisRec(Type TipoReferencia)
        {
            if (TiposVerificados.Contains(TipoReferencia))
                return;
            else
                TiposVerificados.Add(TipoReferencia);
            BindingFlags bf = BindingFlags.Instance | BindingFlags.Public;
            foreach (PropertyInfo Propriedade in TipoReferencia.GetProperties(bf))
            {
                if (Propriedade.PropertyType.BaseType == typeof(Array))
                {
                    AjustaDecimaisRec(Propriedade.PropertyType.GetElementType());
                }
                else
                {
                    if (Propriedade.PropertyType == typeof(decimal))
                    {
                        if (!ListaPropriedadesDec.Contains(Propriedade))
                            ListaPropriedadesDec.Add(Propriedade);
                    }
                    else if (Propriedade.PropertyType == typeof(DateTime))
                    {
                        if (!ListaPropriedadesDateTime.Contains(Propriedade))
                            ListaPropriedadesDateTime.Add(Propriedade);
                    }
                    else
                        if (Propriedade.PropertyType.Namespace == TipoReferencia.Namespace)
                            AjustaDecimaisRec(Propriedade.PropertyType);
                }
            }

        } 
        #endregion
    }
    
}