using System;
using ArquivosTXT;

namespace dllEmissorNFS_e
{
    /// <summary>
    /// 
    /// </summary>
    public class giss
    {
        private cTabelaTXT ArquivoSaida;
        /// <summary>
        /// 
        /// </summary>
        public Exception UltimoErro;
        /// <summary>
        /// 
        /// </summary>
        public dgiss dgiss;

        private dgiss _dgissF;

        /// <summary>
        /// 
        /// </summary>
        public dgiss dgissF
        { get { return _dgissF ?? (_dgissF = new dgiss()); } }

        /// <summary>
        /// Construtor
        /// </summary>
        public giss()
        {
            dgiss = new dgiss();         
            ArquivoSaida = new cTabelaTXT(dgiss.NotasP, ModelosTXT.delimitado);
            ArquivoSaida.Formatodata = FormatoData.dd_MM_YYYY;
            //ArquivoSaida.Encoding = Encoding.UTF8;
            ArquivoSaida.DefaultTamanhoVariavel = true;
            ArquivoSaida.Separador = new char[] { '|', '|' };
            ArquivoSaida.RegistraMapa("CD_INDICADOR", 1, 1, "NF");
            ArquivoSaida.RegistraMapa("NR_LAYOUT", 2, 1, "NF");//1 ou 2
            ArquivoSaida.RegistraMapa("DT_EMISSAO_NF", 3, 10, "NF");
            ArquivoSaida.RegistraMapa("NR_DOC_NF_INICIAL", 4, 10, "NF").TamanhoVariavel = false;
            ArquivoSaida.RegistraMapa("NR_DOC_NF_SERIE", 5, 10, "NF", "A").TamanhoVariavel = false;
            ArquivoSaida.RegistraMapa("NR_DOC_NF_FINAL", 6, 10, "NF").TamanhoVariavel = false;
            ArquivoSaida.RegistraMapa("TP_DOC_NF", 7, 1, "NF"); //Tributada
            ArquivoSaida.RegistraMapa("VL_DOC_NF", 8, 12, "NF");
            ArquivoSaida.RegistraMapa("VL_BASE_CALCULO", 9, 12, "NF");
            ArquivoSaida.RegistraMapa("CD_ATIVIDADE", 10, 20, "NF");           
            ArquivoSaida.RegistraMapa("CD_PREST_TOM_ESTABELECIDO", 11, 1, "NF");
            ArquivoSaida.RegistraMapa("CD_LOCAL_PRESTACAO", 12, 1, "NF");
            ArquivoSaida.RegistraMapa("NM_RAZAO_SOCIAL", 13, 100, "NF");
            ArquivoSaida.RegistraMapa("NR_CNPJ_CPF", 14, 14, "NF");
            ArquivoSaida.RegistraMapa("CD_TIPO_CADASTRO", 15, 1, "NF","2");
            ArquivoSaida.RegistraMapa("NR_INSCRICAO_MUNICIPAL", 16, 10, "NF");
            ArquivoSaida.RegistraMapa("NM_INSCRICAO_MUNICIPAL_DV", 17, 2, "NF");
            ArquivoSaida.RegistraMapa("NR_INSCRICAO_ESTADUAL", 18, 15, "NF");
            ArquivoSaida.RegistraMapa("NM_TIPO_LOGRADOURO", 19, 5, "NF","RUA");
            ArquivoSaida.RegistraMapa("NM_TITULO_LOGRADOURO",20, 5, "NF","");
            ArquivoSaida.RegistraMapa("NM_LOGRADOURO", 21, 50, "NF");
            ArquivoSaida.RegistraMapa("NM_COMPL_LOGRADOURO", 22, 40, "NF", "");
            ArquivoSaida.RegistraMapa("NR_LOGRADOURO", 23, 10, "NF");
            ArquivoSaida.RegistraMapa("CD_CEP", 24, 8, "NF", "");
            ArquivoSaida.RegistraMapa("NM_BAIRRO", 25, 50, "NF");
            ArquivoSaida.RegistraMapa("CD_ESTADO", 26, 2, "NF");
            ArquivoSaida.RegistraMapa("NM_CIDADE", 27, 50, "NF");
            ArquivoSaida.RegistraMapa("CD_PAIS", 28, 2, "NF", "BR");
            ArquivoSaida.RegistraMapa("NM_OBSERVACAO", 29, 4000, "NF", "");
            ArquivoSaida.RegistraMapa("CD_PLANO_CONTA", 30, 80, "NF", "");
            ArquivoSaida.RegistraMapa("CD_ALVARA", 31, 15, "NF", "");
            ArquivoSaida.RegistraMapa("IC_ORIGEM_DADOS", 32, 1, "NF", "R");
            ArquivoSaida.RegistraMapa("IC_ENQUADRAMENTO", 33, 1, "NF","");
            ArquivoSaida.RegistraMapa("CD_PLANO_CONTA_PAI", 34, 80, "NF");
            ArquivoSaida.RegistraMapa("IC_RECOLHE_IMPOSTO", 35, 1, "NF", 1);
            ArquivoSaida.RegistraMapa("VL_ALIQUOTA", 36, 22, "NF");            
            ArquivoSaida.RegistraMapa("FL_ISENTO", 37, 1, "NF");
            ArquivoSaida.RegistraMapa("FL_SIMPLES", 38, 1, "NF");                      
        }

        /// <summary>
        /// Gera o arquivo
        /// </summary>
        /// <returns></returns>
        public bool GeraArquivo(string Arquivo)
        {
            ArquivoSaida.IniciaGravacao(Arquivo);
            ArquivoSaida.GravaCabecalho("NF");
            if (dgiss.NotasP.Count != 0)
                ArquivoSaida.GravaLinhas("NF", dgiss.NotasP);            
            return ArquivoSaida.TerminaGravacao();
        }
    }
}
