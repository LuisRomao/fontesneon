﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Abstratos;

namespace dllEmissorNFS_e
{
    /// <summary>
    /// Dados lidos na nota fical
    /// </summary>
    public class DadosNFe
    {
        /// <summary>
        /// 
        /// </summary>
        public int NumeroNF = 0;
        /// <summary>
        /// 
        /// </summary>
        public string Verificacao = "";
        /// <summary>
        /// 
        /// </summary>
        public DateTime Emissao = DateTime.MinValue;
        /// <summary>
        /// 
        /// </summary>
        public decimal Valor;
        /// <summary>
        /// 
        /// </summary>
        public decimal RetencaISS = 0;
        /// <summary>
        /// 
        /// </summary>
        public decimal RetencaoPIS = 0;
        /// <summary>
        /// 
        /// </summary>
        public decimal RetencaoCOF = 0;
        /// <summary>
        /// 
        /// </summary>
        public decimal RetencaoIR = 0;
        /// <summary>
        /// 
        /// </summary>
        public decimal RetencaoINSS = 0;
        /// <summary>
        /// 
        /// </summary>
        public decimal RetencaoCSLL = 0;
        /// <summary>
        /// 
        /// </summary>
        public string Descricao = "";
        /// <summary>
        /// 
        /// </summary>
        public DocBacarios.CPFCNPJ CNPJTomador = null;
        /// <summary>
        /// 
        /// </summary>
        public DocBacarios.CPFCNPJ CNPJPrestador = null;
        /// <summary>
        /// 
        /// </summary>
        public ABS_Competencia competencia = null;
        /// <summary>
        /// 
        /// </summary>
        public string RazaoP;
        /// <summary>
        /// 
        /// </summary>
        public string RazaoT;
        /// <summary>
        /// 
        /// </summary>
        public int InsMunP = 0;
        /// <summary>
        /// 
        /// </summary>
        public int InsMunT = 0;
        /// <summary>
        /// 
        /// </summary>
        public string ArquivoTMP;

        /// <summary>
        /// 
        /// </summary>
        public string Relatorio()
        {
            return string.Format(
"nNota \t {0}\r\n" +
"Verificacao  \t {1}\r\n" +
"Emissao \t {2}\r\n" +
"Valor \t {3}\r\n" +
"RetencaISS  \t {4}\r\n" +
"RetencaoPIS  \t {5}\r\n" +
"RetencaoCOF  \t {6}\r\n" +
"RetencaoIR  \t {7}\r\n" +
"RetencaoINSS  \t {8}\r\n" +
"RetencaoCSLL  \t {9}\r\n" +
"Descricao \t {10}\r\n" +
"CNPJTomador  \t {11}\r\n" +
"CNPJPrestador \t {12}\r\n" +
"competencia  \t {13}\r\n" +
"RazaoP \t {14}",
NumeroNF,
Verificacao,
Emissao,
Valor,
RetencaISS,
RetencaoPIS,
RetencaoCOF,
RetencaoIR,
RetencaoINSS,
RetencaoCSLL,
Descricao,
CNPJTomador,
CNPJPrestador,
competencia,
RazaoP,
RazaoT,
InsMunP);
        }
    }
}
