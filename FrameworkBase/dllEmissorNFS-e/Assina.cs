using System;
using System.IO;
using System.Security.Cryptography.X509Certificates;
using System.Security.Cryptography.Xml;
using System.Xml;
using VirEnumeracoes;

namespace dllEmissorNFS_e
{
    /// <summary>
    /// 
    /// </summary>
    public class Certificado
    {
        /// <summary>
        /// 
        /// </summary>
        public static string NomeCertificado = "";

        private static Certificado _CertificadoST;

        /// <summary>
        /// 
        /// </summary>
        public static Certificado CertificadoST
        {
            get
            {
                if(_CertificadoST == null)
                    _CertificadoST = new Certificado();
                return _CertificadoST;
            }
            
        }

        private X509Certificate2 _certificado;

        /// <summary>
        /// 
        /// </summary>
        public X509Certificate2 certificado
        {
            get
            {
                if(_certificado == null)
                    _certificado = BuscaNome(NomeCertificado);
                return _certificado;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        public Exception UltimoErro = null;

        /// <summary>
        /// 
        /// </summary>
        /// <param name="Nome"></param>
        /// <returns></returns>
        public X509Certificate2 BuscaNome(string Nome)
        {
            X509Certificate2 _X509Cert = null;// = new X509Certificate2();
            try
            {

                X509Store store = new X509Store("MY", StoreLocation.CurrentUser);
                store.Open(OpenFlags.ReadOnly | OpenFlags.OpenExistingOnly);
                X509Certificate2Collection collection = (X509Certificate2Collection)store.Certificates;
                //X509Certificate2Collection collection1 = (X509Certificate2Collection)collection.Find(X509FindType.FindByTimeValid, DateTime.Now, false);
                X509Certificate2Collection collection2 = (X509Certificate2Collection)collection.Find(X509FindType.FindByKeyUsage, X509KeyUsageFlags.DigitalSignature, false);
                if (Nome == "")
                {
                    X509Certificate2Collection scollection = X509Certificate2UI.SelectFromCollection(collection2, "Certificado(s) Digital(is) dispon�vel(is)", "Selecione o Certificado Digital para uso no aplicativo", X509SelectionFlag.SingleSelection);
                    if (scollection.Count == 0)                                            
                        UltimoErro = new Exception("Nenhum certificado escolhido");                    
                    else                    
                        _X509Cert = scollection[0];                    
                }
                else
                {
                    X509Certificate2Collection scollection = (X509Certificate2Collection)collection2.Find(X509FindType.FindBySubjectDistinguishedName, Nome, false);
                    if (scollection.Count == 0)                    
                        UltimoErro = new Exception("Nenhum certificado v�lido foi encontrado com o nome informado.");                                            
                    else                    
                        _X509Cert = scollection[0];                   
                }
                store.Close();
                return _X509Cert;
            }
            catch (System.Exception ex)
            {
                UltimoErro = ex;
                return null;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="NroSerie"></param>
        /// <returns></returns>
        public X509Certificate2 BuscaNroSerie(string NroSerie)
        {
            X509Certificate2 _X509Cert = null;
            try
            {

                X509Store store = new X509Store("MY", StoreLocation.CurrentUser);
                store.Open(OpenFlags.ReadOnly | OpenFlags.OpenExistingOnly);
                X509Certificate2Collection collection = (X509Certificate2Collection)store.Certificates;
                X509Certificate2Collection collection1 = (X509Certificate2Collection)collection.Find(X509FindType.FindByTimeValid, DateTime.Now, true);
                X509Certificate2Collection collection2 = (X509Certificate2Collection)collection1.Find(X509FindType.FindByKeyUsage, X509KeyUsageFlags.DigitalSignature, true);
                if (NroSerie == "")
                {
                    X509Certificate2Collection scollection = X509Certificate2UI.SelectFromCollection(collection2, "Certificados Digitais", "Selecione o Certificado Digital para uso no aplicativo", X509SelectionFlag.SingleSelection);
                    if (scollection.Count == 0)
                    {
                        //_X509Cert.Reset();
                        UltimoErro = new Exception("Nenhum certificado v�lido foi encontrado com o n�mero de s�rie informado: " + NroSerie);
                    }
                    else
                    {
                        _X509Cert = scollection[0];
                    }
                }
                else
                {
                    X509Certificate2Collection scollection = (X509Certificate2Collection)collection2.Find(X509FindType.FindBySerialNumber, NroSerie, true);
                    if (scollection.Count == 0)
                    {
                        //_X509Cert.Reset();
                        UltimoErro = new Exception("Nenhum certificado v�lido foi encontrado com o n�mero de s�rie informado: " + NroSerie);
                    }
                    else
                    {
                        _X509Cert = scollection[0];
                    }
                }
                store.Close();
                return _X509Cert;
            }
            catch (System.Exception ex)
            {
                UltimoErro = ex;
                return _X509Cert;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="mensagemXML"></param>
        /// <returns></returns>
        public XmlDocument AssinarMensagem(string mensagemXML)
        {
            XmlDocument xmlDoc = new System.Xml.XmlDocument();
            xmlDoc.LoadXml(mensagemXML);
            return AssinarMensagem(xmlDoc);
        }

        private SimNao _VaiAssinar = SimNao.indefinido;


        //refer�ncia OneNote NaoAssina                
        internal bool VaiAssinar
        {
            get 
            {
                if (CompontesBasicosProc.FuncoesBasicasProc.EstaEmProducao)
                    return true;
                if (_VaiAssinar == SimNao.indefinido)
                {
                    bool Vai = System.Windows.Forms.MessageBox.Show("Vai assinar?", "ASSINATURA", System.Windows.Forms.MessageBoxButtons.YesNo, System.Windows.Forms.MessageBoxIcon.Question, System.Windows.Forms.MessageBoxDefaultButton.Button2) == System.Windows.Forms.DialogResult.Yes;
                    _VaiAssinar = Vai ? SimNao.Sim : SimNao.Nao;
                };
                return _VaiAssinar == SimNao.Sim;
            }
        }


        /// <summary>
        /// Faz a assinatura digital de um documento.
        /// </summary>
        /// <param name="xmlDoc"></param>
        /// <returns></returns>
        public XmlDocument AssinarMensagem(XmlDocument xmlDoc)
        {
            
            //refer�ncia OneNote NaoAssina                
            if (!VaiAssinar)
                return xmlDoc;
            
            if (certificado == null)
                return null;            
            //RSACryptoServiceProvider Key;// = new RSACryptoServiceProvider();
            SignedXml SignedDocument;
            KeyInfo keyInfo = new KeyInfo();            
            //Retira chave privada ligada ao certificado
            //Key = (RSACryptoServiceProvider)certificado.PrivateKey;
            
            //Adiciona Certificado ao Key Info
            keyInfo.AddClause(new KeyInfoX509Data(certificado));
            SignedDocument = new SignedXml(xmlDoc);
            //Seta chaves
            SignedDocument.SigningKey = certificado.PrivateKey;
            if (SignedDocument.SigningKey == null)
            {
                UltimoErro = new Exception("Certificado sem chave prim�ria");
                return null;
            };
            SignedDocument.KeyInfo = keyInfo;
            // Cria referencia
            Reference reference = new Reference();
            reference.Uri = String.Empty;
            //Adiciona transformacao a referencia
            reference.AddTransform(new XmlDsigEnvelopedSignatureTransform());
            reference.AddTransform(new XmlDsigC14NTransform(false));
            //Adiciona referencia ao xml
            SignedDocument.AddReference(reference);
            //Calcula Assinatura
            SignedDocument.ComputeSignature();
            //Pega representa��o da assinatura
            XmlElement xmlDigitalSignature = SignedDocument.GetXml();
            //Adiciona ao doc XML
            xmlDoc.DocumentElement.AppendChild(xmlDoc.ImportNode(xmlDigitalSignature, true));
            return xmlDoc;
        }

        /// <summary>
        /// Faz assinatura digital da tag solicitada
        /// </summary>
        /// <param name="xmlDoc"></param>
        /// <param name="RefUri"></param>
        /// <returns></returns>
        public XmlDocument AssinarTag(XmlDocument xmlDoc, string RefUri)
        {
            //refer�ncia OneNote NaoAssina                
            if (!VaiAssinar)
                return xmlDoc;
            
            xmlDoc.PreserveWhitespace = true;
            foreach (XmlNode NoAssinar in xmlDoc.GetElementsByTagName(RefUri))
            {
                // Create a SignedXml object.
                //SignedXml signedXml = new SignedXml(xmlDoc);
                // Add the key to the SignedXml document
                //signedXml.SigningKey = certificado.PrivateKey;

                SignedXmlWithId signedXmlWithId = new SignedXmlWithId(xmlDoc);
                signedXmlWithId.SigningKey = certificado.PrivateKey;

                if (signedXmlWithId.SigningKey == null)
                {
                    UltimoErro = new Exception("Certificado sem chave prim�ria");
                    return null;
                }
                //signedXmlWithId.SigningKey = SignatureID;
                Reference reference = new Reference();
                XmlAttributeCollection _Uri = NoAssinar.Attributes;
                reference.Uri = "";
                foreach (XmlAttribute _atributo in _Uri)                
                    if (_atributo.Name == "Id")
                        reference.Uri = "#" + _atributo.InnerText;                                                
                reference.AddTransform(new XmlDsigEnvelopedSignatureTransform());                
                reference.AddTransform(new XmlDsigC14NTransform());
                signedXmlWithId.AddReference(reference);             
                KeyInfo keyInfo = new KeyInfo();                
                keyInfo.AddClause(new KeyInfoX509Data(certificado));
                signedXmlWithId.KeyInfo = keyInfo;
                signedXmlWithId.ComputeSignature();
                XmlElement xmlDigitalSignature = signedXmlWithId.GetXml();                
                NoAssinar.ParentNode.AppendChild(xmlDoc.ImportNode(xmlDigitalSignature, true));
            }
            return xmlDoc;
        }

        private class SignedXmlWithId : SignedXml
        {
            public SignedXmlWithId(XmlDocument xml) : base(xml)
            {
            }

            public SignedXmlWithId(XmlElement xmlElement)
                : base(xmlElement)
            {
            }

            private XmlElement BuscaNo(object oNo , string ID)
            {
                if (oNo is XmlElement)
                {
                    XmlElement No = (XmlElement)oNo;
                    foreach (XmlAttribute Atrib in No.Attributes)
                        if ((Atrib.Name == "Id") && (Atrib.Value == ID))
                            return No;
                    XmlElement Encontrado = null;
                    foreach (object oCandidato in No.ChildNodes)
                    {
                        Encontrado = BuscaNo(oCandidato, ID);
                        if (Encontrado != null)
                            return Encontrado;
                    }
                }
                return null;
            }

            public override XmlElement GetIdElement(XmlDocument doc, string id)
            {
                //doc.Save("c:\\lixo\\tete.xml");

                // check to see if it's a standard ID reference
                XmlElement idElem = base.GetIdElement(doc, id);

                if (idElem == null)
                {
                    foreach (object oNo in doc.ChildNodes)
                    {
                        idElem = BuscaNo(oNo, id);
                        if (idElem != null)
                            break;
                    }
                    //idElem = (XmlElement)doc.ChildNodes[1].ChildNodes[1];
                    /*
                    XmlNamespaceManager nsManager = new XmlNamespaceManager(doc.NameTable);
                    // nsManager.AddNamespace("wsu", "http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-utility-1.0.xsd");
                    nsManager.AddNamespace("wsu", "http://www.ginfes.com.br/servico_enviar_lote_rps_envio_v03.xsd");
                    idElem = doc.SelectSingleNode("//*[@wsu:Id=\"" + id + "\"]", nsManager) as XmlElement;
                    if (idElem == null)
                    {
                        nsManager = new XmlNamespaceManager(doc.NameTable);
                        nsManager.AddNamespace("wsu", "http://www.w3.org/2001/XMLSchema");
                        idElem = doc.SelectSingleNode("//*[@wsu:Id=\"" + id + "\"]", nsManager) as XmlElement;
                    }
                    if (idElem == null)
                    {
                        nsManager = new XmlNamespaceManager(doc.NameTable);                        
                        nsManager.AddNamespace("wsu", "http://www.w3.org/2001/XMLSchema-instance");
                        idElem = doc.SelectSingleNode("//*[@wsu:Id=\"" + id + "\"]", nsManager) as XmlElement;
                    }
                    */
                }

                return idElem;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="Arquivo"></param>
        /// <returns></returns>
        public bool ValidarAssinaturaArquivo(string Arquivo)
        {
            StreamReader SR;
            string _stringXml;
            SR = File.OpenText(Arquivo);
            _stringXml = SR.ReadToEnd();
            SR.Close();
            return ValidarAssinatura(_stringXml);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="xml"></param>
        /// <returns></returns>
        public bool ValidarAssinatura(string xml)
        {
            //Carrega o xml assinado 
            XmlDocument xmlDoc = new XmlDocument();
            xmlDoc.PreserveWhitespace = true;
            xmlDoc.LoadXml(xml);
            return ValidarAssinatura(xmlDoc);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="xmlDoc"></param>
        /// <returns></returns>
        public bool ValidarAssinatura(XmlDocument xmlDoc)
        {
            if (!VaiAssinar)
                return true;
            XmlNodeList nodeList = xmlDoc.GetElementsByTagName("Signature");
            if (nodeList.Count == 0)
                return false;
            foreach (XmlNode node in nodeList)
            {
                SignedXml signedXmlNode = new SignedXml(xmlDoc);
                signedXmlNode.LoadXml((XmlElement)node);                
                if (!signedXmlNode.CheckSignature())                
                    return false;                
            }
            return true;
        }

    }

    /*
    public class AssinaturaDigital
    {

        public int AssinarArquivo(string Arquivo, string RefUri, X509Certificate2 X509Cert)
        {
            StreamReader SR;
            string _stringXml;
            SR = File.OpenText(Arquivo);
            _stringXml = SR.ReadToEnd();
            SR.Close();
            return Assinar(_stringXml,RefUri,X509Cert);
        }

        public int Assinar(string XMLString, string RefUri, X509Certificate2 X509Cert)
        /*
        * Entradas:
        * XMLString: string XML a ser assinada
        * RefUri : Refer�ncia da URI a ser assinada (Ex. infNFe
        * X509Cert : certificado digital a ser utilizado na assinatura digital
        *
        * Retornos:
        * Assinar : 0 - Assinatura realizada com sucesso
        * 1 - Erro: Problema ao acessar o certificado digital - %exce��o%
        * 2 - Problemas no certificado digital
        * 3 - XML mal formado + exce��o
        * 4 - A tag de assinatura %RefUri% inexiste
        * 5 - A tag de assinatura %RefUri% n�o � unica
        * 6 - Erro Ao assinar o documento - ID deve ser string %RefUri(Atributo)%
        * 7 - Erro: Ao assinar o documento - %exce��o%
        *
        * XMLStringAssinado : string XML assinada
        *
        * XMLDocAssinado : XMLDocument do XML assinado
        
        {
            int resultado = 0;
            msgResultado = "Assinatura realizada com sucesso";
            try
            {
                // certificado para ser utilizado na assinatura
                //
                string _xnome = "";
                if (X509Cert != null)
                {
                    _xnome = X509Cert.Subject.ToString();
                }
                X509Certificate2 _X509Cert = new X509Certificate2();
                X509Store store = new X509Store("MY", StoreLocation.CurrentUser);
                store.Open(OpenFlags.ReadOnly | OpenFlags.OpenExistingOnly);
                X509Certificate2Collection collection = (X509Certificate2Collection)store.Certificates;
                X509Certificate2Collection collection1 = (X509Certificate2Collection)collection.Find(X509FindType.FindBySubjectDistinguishedName, _xnome, false);
                if (collection1.Count == 0)
                {
                    resultado = 2;
                    msgResultado = "Problemas no certificado digital";
                }
                else
                {
                    // certificado ok
                    _X509Cert = collection1[0];
                    string x;
                    x = _X509Cert.GetKeyAlgorithm().ToString();
                    // Create a new XML document.
                    XmlDocument doc = new XmlDocument();

                    // Format the document to ignore white spaces.
                    doc.PreserveWhitespace = true;

                    // Load the passed XML file using it�s name.
                    try
                    {
                        doc.LoadXml(XMLString);

                        // Verifica se a tag a ser assinada existe � �nica
                        int qtdeRefUri = doc.GetElementsByTagName(RefUri).Count;

                        if (qtdeRefUri == 0)
                        {
                            // a URI indicada n�o existe
                            resultado = 4;
                            msgResultado = "A tag de assinatura " + RefUri.Trim() + " inexiste";
                        }
                        // Exsiste mais de uma tag a ser assinada
                        else
                        {
                            if (qtdeRefUri > 1)
                            {
                                // existe mais de uma URI indicada
                                resultado = 5;
                                msgResultado = "A tag de assinatura " + RefUri.Trim() + " n�o � unica";

                            }
                            //else if (_listaNum.IndexOf(doc.GetElementsByTagName(RefUri).Item(0).Attributes.ToString().Substring(1,1))>0)
                            //{
                            // resultado = 6;
                            // msgResultado = "Erro: Ao assinar o documento - ID deve ser string (" + doc.GetElementsByTagName(RefUri).Item(0).Attributes + ")";
                            //}
                            else
                            {
                                try
                                {

                                    // Create a SignedXml object.
                                    SignedXml signedXml = new SignedXml(doc);

                                    // Add the key to the SignedXml document
                                    signedXml.SigningKey = _X509Cert.PrivateKey;

                                    if (signedXml.SigningKey == null)
                                        //erro
                                        signedXml.SigningKey = new RSACryptoServiceProvider();

                                    // Create a reference to be signed
                                    Reference reference = new Reference();
                                    // pega o uri que deve ser assinada
                                    XmlAttributeCollection _Uri = doc.GetElementsByTagName(RefUri)[0].Attributes;
                                    bool semID = true;
                                    foreach (XmlAttribute _atributo in _Uri)
                                    {
                                        if (_atributo.Name == "Id")
                                        {
                                            reference.Uri = "#" + _atributo.InnerText;
                                            semID = false;
                                        }
                                    }

                                    if (semID)
                                    {
                                        reference.Uri = "";
                                    }

                                    // Add an enveloped transformation to the reference.
                                    XmlDsigEnvelopedSignatureTransform env = new XmlDsigEnvelopedSignatureTransform();
                                    reference.AddTransform(env);

                                    XmlDsigC14NTransform c14 = new XmlDsigC14NTransform();
                                    reference.AddTransform(c14);

                                    // Add the reference to the SignedXml object.
                                    signedXml.AddReference(reference);

                                    // Create a new KeyInfo object
                                    KeyInfo keyInfo = new KeyInfo();

                                    // Load the certificate into a KeyInfoX509Data object
                                    // and add it to the KeyInfo object.
                                    keyInfo.AddClause(new KeyInfoX509Data(_X509Cert));

                                    // Add the KeyInfo object to the SignedXml object.
                                    signedXml.KeyInfo = keyInfo;

                                    signedXml.ComputeSignature();

                                    // Get the XML representation of the signature and save
                                    // it to an XmlElement object.
                                    XmlElement xmlDigitalSignature = signedXml.GetXml();

                                    // Append the element to the XML document.
                                    doc.DocumentElement.AppendChild(doc.ImportNode(xmlDigitalSignature, true));
                                    XMLDoc = new XmlDocument();
                                    XMLDoc.PreserveWhitespace = true;
                                    XMLDoc = doc;
                                }
                                catch (Exception caught)
                                {
                                    resultado = 7;
                                    msgResultado = "Erro: Ao assinar o documento - " + caught.Message;
                                }
                            }
                        }
                    }
                    catch (Exception caught)
                    {
                        resultado = 3;
                        msgResultado = "Erro: XML mal formado - " + caught.Message;
                    }
                }
            }
            catch (Exception caught)
            {
                resultado = 1;
                msgResultado = "Erro: Problema ao acessar o certificado digital" + caught.Message;
            }

            return resultado;
        }

        //
        // mensagem de Retorno
        //
        private string msgResultado;
        private XmlDocument XMLDoc;

        public XmlDocument XMLDocAssinado
        {
            get { return XMLDoc; }
        }

        public string XMLStringAssinado
        {
            get { return XMLDoc.OuterXml; }
        }

        public string mensagemResultado
        {
            get { return msgResultado; }
        }
    }

    */


}