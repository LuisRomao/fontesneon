﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using System.Windows.Forms;
using iTextSharp.text;
using iTextSharp.text.pdf;
using iTextSharp.text.pdf.parser;
using iTextSharp.tool.xml;
using System.ComponentModel;
using CompontesBasicosProc;



namespace dllEmissorNFS_e
{
    /// <summary>
    /// Componente para busca de notas eletrônicas nas prefeituras
    /// </summary>
    public class BuscaNFE
    {
        enum ProximaAcao { nenhuma, Ajusta_Clica, SalvaPDF }

        /// <summary>
        /// Status da busca porque o brouser atua de forma assíncrona
        /// </summary>
        public  enum statusBusca
        {
            /// <summary>
            /// Não tem processos em andamento
            /// </summary>
            Livre ,
            /// <summary>
            /// Está aguardando respostas do brouser
            /// </summary>
            Aguardando ,
            /// <summary>
            /// Operação concluída com sucesso
            /// </summary>
            Pronto,
            /// <summary>
            /// Operação concluída com erro
            /// </summary>
            Erro,
            /// <summary>
            /// Operação concluída por TimeOut
            /// </summary>
            TimeOut
        }

        ProximaAcao PAcao = ProximaAcao.nenhuma;

        /// <summary>
        /// 
        /// </summary>
        public statusBusca status;

        private WebBrowser webBrowser1;

        private DadosNFe _DadosNFeLido;

        /// <summary>
        /// Dados Lidos
        /// </summary>
        public DadosNFe DadosNFeLido => _DadosNFeLido ?? (_DadosNFeLido = new DadosNFe());

        /// <summary>
        /// 
        /// </summary>
        public Prefeituras Prefeitura;

        /// <summary>
        /// Construtor
        /// </summary>
        public BuscaNFE():this(new WebBrowser())
        {
        }

        /// <summary>
        /// Construtor
        /// </summary>
        /// <param name="_webBrowser1"></param>
        public BuscaNFE(WebBrowser _webBrowser1)
        {
            webBrowser1 = _webBrowser1;
            webBrowser1.DocumentCompleted += DocumentCompleted;
            status = statusBusca.Livre;
        }

        private void DocumentCompleted(object sender, WebBrowserDocumentCompletedEventArgs e)
        {
            switch (PAcao)
            {
                case ProximaAcao.Ajusta_Clica:
                    if (AjustaPagina())
                    {
                        PAcao = ProximaAcao.SalvaPDF;
                        Clica();
                    }
                    else
                    {
                        status = statusBusca.Erro;
                        PAcao = ProximaAcao.nenhuma;
                    }
                    break;
                case ProximaAcao.SalvaPDF:
                    PAcao = ProximaAcao.nenhuma;
                    DadosNFeLido.ArquivoTMP = ArquivoTMP;
                    salvaPDF(webBrowser1.Url.AbsoluteUri, DadosNFeLido.ArquivoTMP);
                    status = statusBusca.Pronto;
                    OnCadastraNota(new EventArgs());                    
                    break;
            };            
        }

        private bool AjustaPagina()
        {
            if (webBrowser1.Document.Forms.Count == 0)
                return false;
            string xs = webBrowser1.Document.Forms[0].OuterHtml;
            if (xs.Contains("Nota fiscal inexistente"))
                return false;
            else
            {
                xs = xs.Replace("target=_blank", "");
                webBrowser1.Document.Forms[0].OuterHtml = xs;
                return true;
            }
        }

        private void Clica()
        {
            foreach (HtmlElement html in webBrowser1.Document.GetElementsByTagName("a"))
            {
                if (html.InnerText == "Exportar PDF")
                {
                    html.InvokeMember("click");
                }
            }
        }

        private string ArquivoTMP
        {
            get
            {
                string CaminhoBase;
                CaminhoBase = System.IO.Path.GetDirectoryName(Application.ExecutablePath) + "\\TMP\\";
                if (!Directory.Exists(CaminhoBase))
                    Directory.CreateDirectory(CaminhoBase);
                return string.Format("{0}{1:PDFrec_yyyy_MM_dd_HH_mm_fff}.pdf", CaminhoBase, DateTime.Now);
            }
        }

        private void salvaPDF(string Url, string FileName)
        {
            using (var wc = new System.Net.WebClient())
            {
                wc.DownloadFile(Url, FileName);
            }
            ExtractTextFromPdf(FileName);
        }

        private void SalvaPDFIMG(int strCCM, int strNF, string strCod)
        {

            string contents = String.Format(
                            "<html><head><meta name='viewport' content='width=device-width, minimum-scale=0.1' />" +
                            "<title>Nf - e</title></head><body style='margin: 0px;'>" +
                            "<img align = 'middle' style = 'border-width: 0px; ' src='https://nfe.prefeitura.sp.gov.br/contribuinte/notaprintimg.aspx?ccm={0}&amp;nf={1}&amp;cod={2}&amp;imprimir=1' />" +
                            "</body></html>",
                            strCCM, strNF, strCod);
            DadosNFeLido.ArquivoTMP = ArquivoTMP;
            using (Document doc = new Document())
            {
                using (FileStream arquivoPdf = new FileStream(DadosNFeLido.ArquivoTMP, FileMode.Create))
                {
                    PdfWriter writer = PdfWriter.GetInstance(doc, arquivoPdf);
                    doc.Open();
                    StringReader sr = new StringReader(contents);
                    XMLWorkerHelper.GetInstance().ParseXHtml(writer, doc, sr);
                    doc.Close();
                }
            }
            OnCadastraNota(new EventArgs());
        }

        private bool validaURL(string URL)
        {
            if (URL.Contains("santoandre.ginfes.com.br/birt/frameset?__report=nfs_ver13.rptdesign&cdVerificacao="))
            {
                Prefeitura = Prefeituras.SA;
                return true;
            }
            else if (URL.Contains("saocaetano.ginfes.com.br/birt/frameset?__report=nfs_ver15.rptdesign&cdVerificacao="))
            {
                Prefeitura = Prefeituras.SCS;
                return true;
            }
            else if (URL.Contains("nfse.ginfes.com.br/birt/frameset?__report=nfs_sao_bernardo_campo.rptdesign&cdVerificacao="))
            {
                Prefeitura = Prefeituras.SBC;
                return true;
            }
            else if (URL.Contains("maua.ginfes.com.br/birt/frameset?__report=nfs_ver4.rptdesign&cdVerificacao="))
            {
                Prefeitura = Prefeituras.Maua;
                return true;
            }
            else if (URL.Contains("nfe.prefeitura.sp.gov.br/nfe.aspx?ccm="))
            {
                Prefeitura = Prefeituras.SP;
                return true;
            }
            else
                return false;
        }

        private bool ParseLinkSP(string TextoComLink)
        {
            _DadosNFeLido = null;
            string[] Linhas = TextoComLink.Split(new string[] { "\r\n" }, StringSplitOptions.RemoveEmptyEntries);
            foreach (string Linha in Linhas)
            {
                if (Linha.Contains("https://nfe.prefeitura.sp.gov.br/nfe.aspx"))
                {
                    string[] Elementos = Linha.Split(new string[] { "?", "&" }, StringSplitOptions.RemoveEmptyEntries);
                    foreach (string elemento in Elementos)
                    {
                        if (elemento.StartsWith("ccm="))
                            int.TryParse(elemento.Substring(4), out DadosNFeLido.InsMunP);
                        if (elemento.StartsWith("nf="))
                            int.TryParse(elemento.Substring(3), out DadosNFeLido.NumeroNF);
                        if (elemento.StartsWith("cod="))
                            DadosNFeLido.Verificacao = elemento.Substring(4);
                    }
                }
                else if (Linha.StartsWith("CNPJ:"))
                {
                    DadosNFeLido.CNPJPrestador = new DocBacarios.CPFCNPJ(Linha.Substring(6));
                }
            }
            return ((DadosNFeLido.InsMunP != 0) && (DadosNFeLido.NumeroNF != 0) && (DadosNFeLido.Verificacao != ""));
        }

        /// <summary>
        /// Time out para recuperação do arquivo quando síncrono.
        /// </summary>
        public int TimeOutSegundos = 30;

        private bool AguardaTarefaAssincrona()
        {
            DateTime TimeOut = DateTime.Now.AddSeconds(TimeOutSegundos);
            while (DateTime.Now < TimeOut)
            {
                Application.DoEvents();
                if (status.EstaNoGrupo(statusBusca.Pronto, statusBusca.Livre))
                    return true;
                if (status == statusBusca.Erro)
                    return false;
            }
            status = statusBusca.TimeOut;
            PAcao = ProximaAcao.nenhuma;
            return false;
        }

        /// <summary>
        /// Busca uma nota na prefeitura e dispara o evento CadastraNota
        /// </summary>        
        /// <param name="_Prefeitura"></param>
        /// <param name="NumeroNota"></param>
        /// <param name="Verificador"></param>
        /// <param name="InsMunP"></param>
        /// <param name="Sincrono">Só retorna após o termino da recuperação do PDF</param>
        /// <returns></returns>
        public bool Busca(Prefeituras _Prefeitura, int NumeroNota,string Verificador,int? InsMunP = null,bool Sincrono = false)
        {
            if (status == statusBusca.Aguardando)
                return false;            
            Prefeitura = _Prefeitura;
            _DadosNFeLido = null;            
            string URL = "";                        
                string mascara = "";
                switch (Prefeitura)
                {
                    case Prefeituras.SA:
                        mascara = "http://santoandre.ginfes.com.br/birt/frameset?__report=nfs_ver13.rptdesign&cdVerificacao={0}&numNota={1}";
                        PAcao = ProximaAcao.Ajusta_Clica;
                        break;
                    case Prefeituras.SBC:
                        mascara = "http://nfse.ginfes.com.br/birt/frameset?__report=nfs_sao_bernardo_campo.rptdesign&cdVerificacao={0}&numNota={1}";
                        PAcao = ProximaAcao.Ajusta_Clica;
                        break;
                    case Prefeituras.SCS:
                        mascara = "http://saocaetano.ginfes.com.br/birt/frameset?__report=nfs_ver15.rptdesign&cdVerificacao={0}&numNota={1}";
                        PAcao = ProximaAcao.Ajusta_Clica;
                        break;
                    case Prefeituras.Maua:
                        mascara = "http://maua.ginfes.com.br/birt/frameset?__report=nfs_ver4.rptdesign&cdVerificacao={0}&numNota={1}";
                        PAcao = ProximaAcao.Ajusta_Clica;
                        break;
                    case Prefeituras.SP:
                        URL = "";
                        if (InsMunP.HasValue)
                        {
                            DadosNFeLido.InsMunP = InsMunP.Value;
                            DadosNFeLido.NumeroNF = NumeroNota;
                            DadosNFeLido.Verificacao = Verificador.Replace("-", "").Trim().ToUpper();
                            SalvaPDFIMG(DadosNFeLido.InsMunP, DadosNFeLido.NumeroNF, DadosNFeLido.Verificacao);                            
                        }
                        break;
                }
                URL = string.Format(mascara, Verificador, NumeroNota);

            if (URL != "")
            {
                status = statusBusca.Aguardando;
                webBrowser1.Navigate(URL);
                if (Sincrono)
                    return AguardaTarefaAssincrona();
                else
                    return true;
            }
            else
            {
                status = statusBusca.Erro;
                return false;
            }
        }        

        /// <summary>
        /// Busca uma nota na prefeitura e dispara o evento CadastraNota
        /// </summary>        
        /// <param name="Link"></param>
        /// <param name="Sincrono">Só retorna após o termino da recuperação do PDF</param>
        /// <returns></returns>
        public bool Busca(string Link, bool Sincrono = false)
        {
            if (status == statusBusca.Aguardando)
                return false;            
            if (!validaURL(Link))
            {
                status = statusBusca.Erro;
                return false;
            }
            switch (Prefeitura)
            {
                case Prefeituras.SA:
                case Prefeituras.SBC:
                case Prefeituras.SCS:
                case Prefeituras.Maua:
                    PAcao = ProximaAcao.Ajusta_Clica;
                    status = statusBusca.Aguardando;
                    webBrowser1.Navigate(Link);
                    if (Sincrono)
                        return AguardaTarefaAssincrona();
                    else
                        return true;
                case Prefeituras.SP:
                    if (ParseLinkSP(Link))
                    {
                        status = statusBusca.Aguardando;
                        SalvaPDFIMG(DadosNFeLido.InsMunP, DadosNFeLido.NumeroNF, DadosNFeLido.Verificacao);
                        if (Sincrono)
                            return AguardaTarefaAssincrona();
                        else
                            return true;
                    }
                    status = statusBusca.Erro;
                    PAcao = ProximaAcao.nenhuma;
                    return false;
                default:
                    return false;
            }

        }

        /// <summary>
        /// Cadastrar Nota
        /// </summary>
        [Category("Virtual Software")]
        [Description("Evento para a carga das tabelas antes da principal")]
        public event EventHandler CadastraNota;

        private void OnCadastraNota(EventArgs e)
        {
            if (CadastraNota != null)
                try
                {
                    CadastraNota(this, e);
                }
                catch (Exception ex)
                {
                    PAcao = ProximaAcao.nenhuma;
                    status = statusBusca.Erro;
                    throw new Exception("Erro no cadastro da nota",ex);                    
                }
        }

        #region Extract

        enum Bandas
        {
            cabecalho,
            Prestador,
            Tomandor,
            Discriminacao,
            Servico,
            Retencao,
            Valores
        }

        private string Parse(string path)
        {
            StringBuilder text = new StringBuilder();
            using (PdfReader reader = new PdfReader(path))
            {
                for (int i = 1; i <= reader.NumberOfPages; i++)
                    text.Append(PdfTextExtractor.GetTextFromPage(reader, i));
            }
            return text.ToString();
        }

        private DadosNFe ExtractTextFromPdf_SA(string[] linhas)
        {
            Bandas banda = Bandas.cabecalho;
            string[] Palavras;
            for (int i = 0; i < linhas.Length; i++)
            {
                if (linhas[i].Contains("Prestador de Serviço"))
                    banda = Bandas.Prestador;
                else if (linhas[i].Contains("Tomador de Serviço"))
                    banda = Bandas.Tomandor;
                else if (linhas[i].Contains("Discriminação do Serviço"))
                    banda = Bandas.Discriminacao;
                else if (linhas[i].Contains("Código do Serviço / Atividade"))
                    banda = Bandas.Servico;
                else if (linhas[i].Contains("Tributos Federais"))
                    banda = Bandas.Retencao;
                else if (linhas[i].Contains("Detalhamento de Valores - Prestador do Serviço"))
                    banda = Bandas.Valores;
                else
                    switch (banda)
                    {
                        case Bandas.cabecalho:
                            if (linhas[i].Contains("NOTA FISCAL ELETRÔNICA DE SERVIÇO - NFS-e"))
                                int.TryParse(linhas[i + 1], out DadosNFeLido.NumeroNF);
                            else if (linhas[i].Contains("Data e Hora da Emissão"))
                            {
                                DateTime.TryParse(linhas[i].Substring(23, 19), out DadosNFeLido.Emissao);
                                //string teste = linhas[i].Substring(23, 9).Trim();
                                string[] strComp = linhas[i].Substring(23, 10).Trim().Split('/');
                                if (strComp.Length == 3)
                                {
                                    int mes = 0;
                                    int ano = 0;
                                    if (int.TryParse(strComp[1], out mes) && (int.TryParse(strComp[2], out ano)))
                                        DadosNFeLido.competencia = new Abstratos.ABS_Competencia(mes, ano);
                                };
                                DadosNFeLido.Verificacao = linhas[i].Substring(linhas[i].IndexOf("Verificação") + 12).Trim();
                            }
                            break;
                        case Bandas.Prestador:
                            if (linhas[i].Contains("Razão Social/Nome"))
                                DadosNFeLido.RazaoP = linhas[i].Substring(19).Trim();
                            else if (linhas[i].StartsWith(" CNPJ/CPF"))
                            {
                                Palavras = linhas[i].Split(' ');
                                if (Palavras.Length >= 5)
                                {
                                    DadosNFeLido.CNPJPrestador = new DocBacarios.CPFCNPJ(Palavras[2]);
                                    int.TryParse(Palavras[5], out DadosNFeLido.InsMunP);
                                }
                            }
                            break;
                        case Bandas.Tomandor:
                            if (linhas[i].Contains("Razão Social/Nome"))
                                DadosNFeLido.RazaoT = linhas[i].Substring(19).Trim();
                            else if (linhas[i].StartsWith(" CNPJ/CPF"))
                            {
                                Palavras = linhas[i].Split(' ');
                                if (Palavras.Length >= 5)
                                {
                                    DadosNFeLido.CNPJTomador = new DocBacarios.CPFCNPJ(Palavras[2]);
                                    int.TryParse(Palavras[5], out DadosNFeLido.InsMunT);
                                }
                            }
                            break;
                        case Bandas.Servico:
                            string[] partes = linhas[i].Split('-');
                            if (partes.Length == 2)
                                DadosNFeLido.Descricao = partes[1].Trim();
                            break;
                        case Bandas.Retencao:
                            Palavras = linhas[i].Split(' ');
                            for (int j = 0; j < Palavras.Length - 1; j++)
                            {
                                if (Palavras[j] == "PIS(R$)")
                                    decimal.TryParse(Palavras[j + 1], out DadosNFeLido.RetencaoPIS);
                                else if (Palavras[j] == "COFINS(R$)")
                                    decimal.TryParse(Palavras[j + 1], out DadosNFeLido.RetencaoCOF);
                                else if (Palavras[j] == "IR(R$)")
                                    decimal.TryParse(Palavras[j + 1], out DadosNFeLido.RetencaoIR);
                                else if (Palavras[j] == "INSS(R$)")
                                    decimal.TryParse(Palavras[j + 1], out DadosNFeLido.RetencaoINSS);
                                else if (Palavras[j] == "CSLL(R$)")
                                    decimal.TryParse(Palavras[j + 1], out DadosNFeLido.RetencaoCSLL);
                            }
                            break;
                        case Bandas.Valores:
                            if (linhas[i].Contains("Valor do Serviço"))
                            {
                                Palavras = linhas[i].Split(new string[] { " " }, StringSplitOptions.RemoveEmptyEntries);
                                decimal.TryParse(Palavras[4], out DadosNFeLido.Valor);
                            }
                            else if (linhas[i].Contains("(-) ISSQN Retido"))
                            {
                                Palavras = linhas[i].Split(' ');
                                decimal.TryParse(Palavras[3], out DadosNFeLido.RetencaISS);
                            }
                            break;
                    }
            }
            return DadosNFeLido;
        }

        private DadosNFe ExtractTextFromPdf_SCS(string[] linhas)
        {
            Bandas banda = Bandas.cabecalho;
            string[] Palavras;
            for (int i = 0; i < linhas.Length; i++)
            {
                if (linhas[i].Contains("Prestador de Serviço"))
                    banda = Bandas.Prestador;
                else if (linhas[i].Contains("Tomador de Serviço"))
                    banda = Bandas.Tomandor;
                else if (linhas[i].Contains("Discriminação do Serviço"))
                    banda = Bandas.Discriminacao;
                else if (linhas[i].Contains("Código do Serviço / Atividade"))
                    banda = Bandas.Servico;
                else if (linhas[i].Contains("Tributos Federais"))
                    banda = Bandas.Retencao;
                else if (linhas[i].Contains("Detalhamento de Valores - Prestador do Serviço"))
                    banda = Bandas.Valores;
                else
                    switch (banda)
                    {
                        case Bandas.cabecalho:
                            if (linhas[i].Contains("NOTA FISCAL ELETRÔNICA DE SERVIÇO - NFS-e"))
                                int.TryParse(linhas[i + 1], out DadosNFeLido.NumeroNF);
                            else if (linhas[i].Contains("Data e Hora da Emissão"))
                            {
                                DateTime.TryParse(linhas[i].Substring(23, 19), out DadosNFeLido.Emissao);
                                //string teste = linhas[i].Substring(23, 9).Trim();
                                string[] strComp = linhas[i].Substring(23, 10).Trim().Split('/');
                                if (strComp.Length == 3)
                                {
                                    int mes = 0;
                                    int ano = 0;
                                    if (int.TryParse(strComp[1], out mes) && (int.TryParse(strComp[2], out ano)))
                                        DadosNFeLido.competencia = new Abstratos.ABS_Competencia(mes, ano);
                                };
                                DadosNFeLido.Verificacao = linhas[i].Substring(linhas[i].IndexOf("Verificação") + 12).Trim();
                            }
                            break;
                        case Bandas.Prestador:
                            if (linhas[i].Contains("Razão Social/Nome"))
                                DadosNFeLido.RazaoP = linhas[i].Substring(19).Trim();
                            else if (linhas[i].StartsWith(" CNPJ/CPF"))
                            {
                                Palavras = linhas[i].Split(' ');
                                if (Palavras.Length >= 5)
                                {
                                    DadosNFeLido.CNPJPrestador = new DocBacarios.CPFCNPJ(Palavras[2]);
                                    int.TryParse(Palavras[5], out DadosNFeLido.InsMunP);
                                }
                            }
                            break;
                        case Bandas.Tomandor:
                            if (linhas[i].Contains("Razão Social/Nome"))
                                DadosNFeLido.RazaoT = linhas[i].Substring(19).Trim();
                            else if (linhas[i].StartsWith(" CNPJ/CPF"))
                            {
                                Palavras = linhas[i].Split(' ');
                                if (Palavras.Length >= 5)
                                {
                                    DadosNFeLido.CNPJTomador = new DocBacarios.CPFCNPJ(Palavras[2]);
                                    int.TryParse(Palavras[5], out DadosNFeLido.InsMunT);
                                }
                            }
                            break;
                        case Bandas.Servico:
                            string[] partes = linhas[i].Split('-');
                            if (partes.Length == 2)
                                DadosNFeLido.Descricao = partes[1].Trim();
                            break;
                        case Bandas.Retencao:
                            Palavras = linhas[i].Split(' ');
                            for (int j = 0; j < Palavras.Length - 2; j++)
                            {
                                if (Palavras[j] == "PIS")
                                    decimal.TryParse(Palavras[j + 2], out DadosNFeLido.RetencaoPIS);
                                else if (Palavras[j] == "COFINS")
                                    decimal.TryParse(Palavras[j + 2], out DadosNFeLido.RetencaoCOF);
                                else if (Palavras[j] == "IR")
                                    decimal.TryParse(Palavras[j + 2], out DadosNFeLido.RetencaoIR);
                                else if (Palavras[j] == "INSS")
                                    decimal.TryParse(Palavras[j + 2], out DadosNFeLido.RetencaoINSS);
                                else if (Palavras[j] == "CSLL")
                                    decimal.TryParse(Palavras[j + 2], out DadosNFeLido.RetencaoCSLL);
                            }
                            break;
                        case Bandas.Valores:
                            if (linhas[i].Contains("Valor do Serviço"))
                            {
                                Palavras = linhas[i].Split(new string[] { " " }, StringSplitOptions.RemoveEmptyEntries);
                                decimal.TryParse(Palavras[4], out DadosNFeLido.Valor);
                            }
                            else if (linhas[i].Contains("(-) ISSQN Retido"))
                            {
                                Palavras = linhas[i].Split(' ');
                                decimal.TryParse(Palavras[3], out DadosNFeLido.RetencaISS);
                            }
                            break;
                    }
            }
            return DadosNFeLido;
        }

        private DadosNFe ExtractTextFromPdf_Maua(string[] linhas)
        {
            Bandas banda = Bandas.cabecalho;
            string[] Palavras;
            for (int i = 0; i < linhas.Length; i++)
            {
                if (linhas[i].Contains("Dados do Prestador de Serviços"))
                    banda = Bandas.Prestador;
                else if (linhas[i].Contains("Dados do Tomador de Serviços"))
                    banda = Bandas.Tomandor;
                else if (linhas[i].Contains("Discriminação dos Serviços"))
                    banda = Bandas.Discriminacao;
                else if (linhas[i].Contains("Código do Serviço / Atividade"))
                    banda = Bandas.Servico;
                else if (linhas[i].Contains("Tributos Federais"))
                    banda = Bandas.Retencao;
                else if (linhas[i].Contains("Detalhamento de Valores - Prestador dos Serviços"))
                    banda = Bandas.Valores;
                else
                    switch (banda)
                    {
                        case Bandas.cabecalho:
                            if (linhas[i].Contains("NOTA FISCAL ELETRÔNICA DE SERVIÇO - NFS-e"))
                                int.TryParse(linhas[i + 1], out DadosNFeLido.NumeroNF);
                            else if (linhas[i].Contains("Data e Hora da Emissão"))
                            {
                                DateTime.TryParse(linhas[i].Substring(23, 19), out DadosNFeLido.Emissao);
                                string[] strComp = linhas[i].Substring(23, 10).Trim().Split('/');
                                if (strComp.Length == 3)
                                {
                                    int mes = 0;
                                    int ano = 0;
                                    if (int.TryParse(strComp[1], out mes) && (int.TryParse(strComp[2], out ano)))
                                        DadosNFeLido.competencia = new Abstratos.ABS_Competencia(mes, ano);
                                };
                                //DadosNFeLido.Verificacao = linhas[i].Substring(linhas[i].IndexOf("Verificação") + 12).Trim();
                            }
                            else if (linhas[i].Contains("Código de Verificação"))
                            {
                                Palavras = linhas[i + 1].Split(new string[] { " " }, StringSplitOptions.RemoveEmptyEntries);
                                DadosNFeLido.Verificacao = Palavras[Palavras.Length - 1];
                            }
                            break;
                        case Bandas.Prestador:
                            if (linhas[i].Contains("Razão Social/Nome"))
                                DadosNFeLido.RazaoP = linhas[i].Substring(18).Trim();
                            else if (linhas[i].StartsWith("CNPJ/CPF"))
                            {
                                Palavras = linhas[i].Split(' ');
                                if (Palavras.Length >= 5)
                                {
                                    DadosNFeLido.CNPJPrestador = new DocBacarios.CPFCNPJ(Palavras[1]);
                                    int.TryParse(Palavras[5], out DadosNFeLido.InsMunP);
                                }
                            }
                            break;
                        case Bandas.Tomandor:
                            if (linhas[i].Contains("Razão Social/Nome"))
                                DadosNFeLido.RazaoT = linhas[i].Substring(19).Trim();
                            else if (linhas[i].StartsWith("CNPJ/CPF"))
                            {
                                Palavras = linhas[i].Split(' ');
                                if (Palavras.Length >= 5)
                                {
                                    DadosNFeLido.CNPJTomador = new DocBacarios.CPFCNPJ(Palavras[1]);
                                    int.TryParse(Palavras[5], out DadosNFeLido.InsMunT);
                                }
                            }
                            break;
                        case Bandas.Servico:
                            string[] partes = linhas[i].Split('-');
                            if (partes.Length == 2)
                                DadosNFeLido.Descricao = partes[1].Trim();
                            break;
                        case Bandas.Retencao:
                            Palavras = linhas[i].Split(' ');
                            for (int j = 0; j < Palavras.Length - 1; j++)
                            {
                                if (Palavras[j] == "PIS")
                                    decimal.TryParse(Palavras[j + 1], out DadosNFeLido.RetencaoPIS);
                                else if (Palavras[j] == "COFINS")
                                    decimal.TryParse(Palavras[j + 1], out DadosNFeLido.RetencaoCOF);
                                else if (Palavras[j] == "IR(R$)")
                                    decimal.TryParse(Palavras[j + 1], out DadosNFeLido.RetencaoIR);
                                else if (Palavras[j] == "INSS(R$)")
                                    decimal.TryParse(Palavras[j + 1], out DadosNFeLido.RetencaoINSS);
                                else if (Palavras[j] == "CSLL(R$)")
                                    decimal.TryParse(Palavras[j + 1], out DadosNFeLido.RetencaoCSLL);
                            }
                            break;
                        case Bandas.Valores:
                            if (linhas[i].Contains("Valor dos Serviços"))
                            {
                                Palavras = linhas[i].Split(new string[] { " " }, StringSplitOptions.RemoveEmptyEntries);
                                decimal.TryParse(Palavras[4], out DadosNFeLido.Valor);
                            }
                            else if ((linhas[i].Contains("(-) ISS Retido")) && (linhas[i].Contains("(X) Sim")))
                            {
                                Palavras = linhas[i].Split(' ');
                                decimal.TryParse(Palavras[3], out DadosNFeLido.RetencaISS);
                            }
                            break;
                    }
            }
            return DadosNFeLido;
        }

        private DadosNFe ExtractTextFromPdf_SBC(string[] linhas)
        {
            Bandas banda = Bandas.cabecalho;
            string[] Palavras;
            for (int i = 0; i < linhas.Length; i++)
            {
                if (linhas[i].Contains("Dados do Prestador de Serviços"))
                    banda = Bandas.Prestador;
                else if (linhas[i].Contains("Dados do Tomador de Serviços"))
                    banda = Bandas.Tomandor;
                else if (linhas[i].Contains("Discriminação dos Serviços"))
                    banda = Bandas.Discriminacao;
                else if (linhas[i].Contains("Codificação do Serviço Prestado"))
                    banda = Bandas.Servico;
                else if (linhas[i].Contains("Retenção de Tributos Federais (R$)"))
                    banda = Bandas.Retencao;
                else if (linhas[i].Contains("Detalhamento de Valores dos Serviços"))
                    banda = Bandas.Valores;
                else
                    switch (banda)
                    {
                        case Bandas.cabecalho:
                            if (linhas[i].Contains("NOTA FISCAL DE SERVIÇOS ELETRÔNICA - NFS-e"))
                                int.TryParse(linhas[i + 1], out DadosNFeLido.NumeroNF);
                            else if (linhas[i].Contains("Data e Hora da Emissão"))
                            {
                                DateTime.TryParse(linhas[i + 1].Substring(0, 19), out DadosNFeLido.Emissao);
                                //string teste = linhas[i + 1].Substring(32, 7).Trim();
                                string[] strComp = linhas[i + 1].Substring(32, 7).Trim().Split('/');
                                if (strComp.Length == 2)
                                {
                                    int mes = 0;
                                    int ano = 0;
                                    if (int.TryParse(strComp[0], out mes) && (int.TryParse(strComp[1], out ano)))
                                        DadosNFeLido.competencia = new Abstratos.ABS_Competencia(mes, ano);
                                };
                                DadosNFeLido.Verificacao = linhas[i + 1].Substring(linhas[i + 1].IndexOf("verificação") + 12).Trim();
                            }
                            break;
                        case Bandas.Prestador:
                            if (linhas[i].Contains("Razão Social / Nome"))
                                DadosNFeLido.RazaoP = linhas[i].Substring(20).Trim();
                            else if (linhas[i].StartsWith("CNPJ/CPF"))
                            {
                                Palavras = linhas[i].Split(' ');
                                if (Palavras.Length >= 5)
                                {
                                    DadosNFeLido.CNPJPrestador = new DocBacarios.CPFCNPJ(Palavras[1]);
                                    int.TryParse(Palavras[5], out DadosNFeLido.InsMunP);
                                }
                            }
                            break;
                        case Bandas.Tomandor:
                            if (linhas[i].Contains("Razão Social / Nome"))
                                DadosNFeLido.RazaoT = linhas[i].Substring(21).Trim();
                            else if (linhas[i].StartsWith("CNPJ/CPF"))
                            {
                                Palavras = linhas[i].Split(' ');
                                if (Palavras.Length >= 5)
                                {
                                    DadosNFeLido.CNPJTomador = new DocBacarios.CPFCNPJ(Palavras[1]);
                                    int.TryParse(Palavras[5], out DadosNFeLido.InsMunT);
                                }
                            }
                            break;
                        case Bandas.Servico:
                            string[] partes = linhas[i].Split('-');
                            if (partes.Length == 2)
                                DadosNFeLido.Descricao = partes[1].Trim();
                            break;
                        case Bandas.Retencao:
                            Palavras = linhas[i].Split(' ');
                            for (int j = 0; j < Palavras.Length - 2; j++)
                            {
                                if (Palavras[j] == "PIS")
                                    decimal.TryParse(Palavras[j + 2], out DadosNFeLido.RetencaoPIS);
                                else if (Palavras[j] == "COFINS")
                                    decimal.TryParse(Palavras[j + 2], out DadosNFeLido.RetencaoCOF);
                                else if (Palavras[j] == "IR")
                                    decimal.TryParse(Palavras[j + 2], out DadosNFeLido.RetencaoIR);
                                else if (Palavras[j] == "INSS")
                                    decimal.TryParse(Palavras[j + 2], out DadosNFeLido.RetencaoINSS);
                                else if (Palavras[j] == "CSLL")
                                    decimal.TryParse(Palavras[j + 2], out DadosNFeLido.RetencaoCSLL);
                            }
                            break;
                        case Bandas.Valores:
                            if (linhas[i].Contains("Valor dos Serviços R$"))
                            {
                                Palavras = linhas[i].Split(' ');
                                decimal.TryParse(Palavras[4], out DadosNFeLido.Valor);
                            }
                            else if ((linhas[i].Contains("(-) ISS Retido")) && (linhas[i].Contains("(X) Sim")))
                            {
                                Palavras = linhas[i].Split(' ');
                                decimal.TryParse(Palavras[3], out DadosNFeLido.RetencaISS);
                            }
                            break;
                    }
            }
            return DadosNFeLido;
        }

        private DadosNFe ExtractTextFromPdf(string path)
        {
            string[] linhas = Parse(path).Split(new string[] { "\n" }, StringSplitOptions.RemoveEmptyEntries);

            switch (Prefeitura)
            {
                case Prefeituras.SA:
                    ExtractTextFromPdf_SA(linhas);
                    break;
                case Prefeituras.SCS:
                    ExtractTextFromPdf_SCS(linhas);
                    break;
                case Prefeituras.SBC:
                    ExtractTextFromPdf_SBC(linhas);
                    break;
                case Prefeituras.Maua:
                    ExtractTextFromPdf_Maua(linhas);
                    break;
                default:
                    throw new NotImplementedException(string.Format("Prefeitura não implementada:{0}", Prefeitura));
            }            
            return DadosNFeLido;
        }

        #endregion
    }

    /// <summary>
    /// Prefeitura homologadas
    /// </summary>
    public enum Prefeituras
    {
        /// <summary>
        /// Santo André
        /// </summary>
        SA = 0,
        /// <summary>
        /// São Bernardo
        /// </summary>
        SBC = 1,
        /// <summary>
        /// São Caetano do Sul
        /// </summary>
        SCS = 2,
        /// <summary>
        /// São Paulo
        /// </summary>
        SP = 3,
        /// <summary>
        /// Mauá
        /// </summary>
        Maua = 4
    }
}
