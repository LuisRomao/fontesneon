using System;
using System.Threading;
using System.Windows.Forms;


namespace VirException
{
    /// <summary>
    /// Classe para reportar Erros
    /// </summary>
    public class VirException:VirExceptionProc.VirExceptionProc
    {
        /// <summary>
        /// Servidor SMTP
        /// </summary>
        public string SMTP;
        /// <summary>
        /// Usu�rio smtp
        /// </summary>
        public string Usuario; 
        /// <summary>
        /// Semha smtp
        /// </summary>
        public string Senha;
        /// <summary>
        /// Remetente
        /// </summary>
        public string Remetente;
        /// <summary>
        /// Identifica a empresa.
        /// </summary>
        /// <remarks>N�o � setado automaticamente.</remarks>
        public static string EMP;

        private static VirException virExceptionSt;

        /// <summary>
        /// Instancia ST
        /// </summary>
        public static VirException VirExceptionSt
        {
            set { virExceptionSt = value; }
            get{ return virExceptionSt ?? (virExceptionSt = new VirException());}
        }


        /// <summary>
        /// Construtor
        /// </summary>
        /// <param name="_SMTP"></param>
        /// <param name="_Usuario"></param>
        /// <param name="_Senha"></param>
        /// <param name="_Remetente"></param>
        /// <param name="_EMP">Empresa</param>
        public VirException(string _SMTP, string _Usuario, string _Senha, string _Remetente,string _EMP = null)
        {
            SMTP = _SMTP;
            Usuario = _Usuario;
            Senha = _Senha;
            Remetente = _Remetente;
            if (_EMP != null)
                EMP = _EMP;
        }

        /// <summary>
        /// Construtor padr�o
        /// </summary>
        protected VirException()
        {
            
        }

        /// <summary>
        /// ultimoemail
        /// </summary>
        public bool ultimoemail;

        /// <summary>
        /// M�todo de envio de e.mail
        /// </summary>
        /// <param name="DadosDoErro"></param>
        /// <param name="Assunto">Assunto</param>
        protected virtual void EnviaEmail(string DadosDoErro,string Assunto) {
            if (SMTP == null)
                return;
            VirEmail.EmailDireto Email = new VirEmail.EmailDireto(SMTP, Usuario, Senha, Remetente);            
            try
            {
                ultimoemail = Email.Enviar("Erros@virweb.com.br", DadosDoErro, Assunto);
            }
            catch
            {
                ultimoemail = Email.Enviar("Erros@virweb.com.br", DadosDoErro, "Erro no assunto");
            }
        }                

        /*
        /// <summary>
        /// Dados do erro com os innes
        /// </summary>
        /// <param name="ex">erro</param>
        /// <param name="IncluirAssemby">Inclui a lista de assemblies</param>
        /// <returns></returns>
        public static string RelatorioDeErro(Exception ex, bool IncluirAssemby = true)
        {            
            string DadosDoErro = "";
            Exception Xe = ex;
            while (Xe != null)
            {
                if (DadosDoErro != "")
                    DadosDoErro += "............Inner.............\r\n";
                else
                    DadosDoErro = string.Format("{0}\r\n", CabecalhoRetorno());
                DadosDoErro += "Classe do erro: " + Xe.GetType().ToString();
                DadosDoErro += String.Format("\r\n{0}\r\n\r\nStack Trace:\r\n{1}\r\n", Xe.Message, Xe.StackTrace);
                Xe = Xe.InnerException;
            };
            if (IncluirAssemby)
                DadosDoErro += "\r\n\r\n" + VersaoAssembles();
            return DadosDoErro;
        }*/

        

        /// <summary>
        /// Envia uma notifica��o do erro
        /// </summary>
        /// <param name="_Xe">Exce��o</param>
        /// <remarks>
        /// Pode ser chamado automaticamente pelo evento ou manualmente
        /// Dever� ser chamado manualmente se a exe��o nao for interroper o fluxo com throw
        /// </remarks>
        public void NotificarErro(Exception _Xe) {
            try
            {
                bool Reportar = true;
                bool AvisarUsuario = true;
                bool Vircath = false;
                string Mensagem = "";
                bool Abortar = false;
                string ReferenciaVirtual = "";
                if (_Xe.GetType() == typeof(cVirException))
                {
                    cVirException cXe = (cVirException)_Xe;
                    Reportar = cXe.Reportar;
                    AvisarUsuario = cXe.AvisarUsuario;
                    Vircath = cXe.Vircath;
                    Mensagem = cXe.Mensagem;
                    Abortar = cXe.Abortar;
                };
                string DadosDoErro = string.Format("{0}{1}",                     
                    Vircath ? "  **VIRCATH**\r\n":"",
                    ReferenciaVirtual == "" ? "" : "REFERENCIA:"+ReferenciaVirtual+"\r\n");
                DadosDoErro += RelatorioDeErro(_Xe,true, true,true);                
                string Arquivo = "";
                if(System.Windows.Forms.Application.StartupPath != "")
                    Arquivo = string.Format("{0}\\Ultimo Erro.txt",System.Windows.Forms.Application.StartupPath);
                else
                    Arquivo = string.Format("Ultimo Erro.txt");
                System.IO.File.WriteAllText(Arquivo, DadosDoErro);
                if (Reportar)
                    EnviaEmail(DadosDoErro, string.Format("Notifica��o de erro classe: {0}", _Xe.GetType()));

                if (AvisarUsuario)
                    if (Mensagem != "")
                        MessageBox.Show(Mensagem);
                    else
                        ShowThreadExceptionDialog(_Xe);
                if(Abortar)
                    Application.Exit();
            }
            catch (Exception e)
            {
                try
                {
                    Exception Xe = e;
                    while (Xe != null)
                    {
                        MessageBox.Show(string.Format("Erro: {0}\r\nTrace\r\n{1}", Xe.Message, Xe.StackTrace), "Erro Secund�rio", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                        Xe = Xe.InnerException;
                    }
                }
                finally
                {
                    Application.Exit();
                }
            }

            // Exits the program when the user clicks Abort.
            //if (result == DialogResult.Abort)
            //    Application.Exit();
        }


        /// <summary>
        /// Evento do erro
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="t"></param>
        public void OnThreadException(object sender, ThreadExceptionEventArgs t)
        {
            NotificarErro(t.Exception);             
        }

        /// <summary>
        /// Tela para o usu�rio
        /// </summary>
        /// <param name="e"></param>
        /// <returns></returns>
        private static DialogResult ShowThreadExceptionDialog(Exception e)
        {            
            return MessageBox.Show(string.Format("Relatorio de Erro:\r\n{0}\r\nAviso de erro enviado.", VirExceptionProc.VirExceptionProc.RelatorioDeErroUsuario(e)), "Erro");
        }
    }
}
