using System;
using System.Collections.Generic;
using System.Text;

namespace VirException
{
    /// <summary>
    /// Classe para tratamento de erro
    /// </summary>
    public class cVirException:Exception
    {
        /// <summary>
        /// Enviar e.mail
        /// </summary>
        public bool Reportar = true;
        /// <summary>
        /// Mostrar erro para o usu�rio
        /// </summary>
        public bool AvisarUsuario = true;
        /// <summary>
        /// A excess�o foi criadaem vircath
        /// </summary>
        public bool Vircath = false;
        /// <summary>
        /// Mensagem a ser mostrata ao usu�rio ao inves de e.message
        /// </summary>
        public string Mensagem = "";
        /// <summary>
        /// Fechar o programa
        /// </summary>
        public bool Abortar = false;
        /// <summary>
        /// string de referencia pra ajudar no debug
        /// </summary>
        public string ReferenciaVirtual = "";

        /// <summary>
        /// Construtor
        /// </summary>
        /// <param name="message"></param>
        public cVirException(string message):base(message)
        {
            
        }

        /// <summary>
        /// Construtor
        /// </summary>
        /// <param name="message"></param>
        /// <param name="innerExcepton"></param>
        public cVirException(string message, Exception innerExcepton)
            : base(message,innerExcepton)
        {

        }
        
    }

    
}
