﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Forms;
using VirDB.Bancovirtual;

namespace TesteVirDB
{
    static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main()
        {
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);            
            BancoVirtual.Popular("Local", @"Data Source=Lapvirtual8\SQLEXPRESS;Initial Catalog=Neon;Persist Security Info=True;User ID=sa;Password=venus");
            BancoVirtual.Popular("copia SBC 1", @"Data Source=Lapvirtual8\SQLEXPRESS;Initial Catalog=NeonSBC;Persist Security Info=True;User ID=sa;Password=venus");
            BancoVirtual.Popular("copia SBC 2", @"Data Source=Lapvirtual8\SQLEXPRESS;Initial Catalog=NeonSBC2;Persist Security Info=True;User ID=sa;Password=venus");
            BancoVirtual.Popular("copia SBC 3", @"Data Source=Lapvirtual8\SQLEXPRESS;Initial Catalog=NeonSBC3;Persist Security Info=True;User ID=sa;Password=venus");
            BancoVirtual.Popular("QAS", @"Data Source=cp.neonimoveis.com;Initial Catalog=NeonH;Persist Security Info=True;User ID=NEONT;Connect Timeout=30;Password=TPC4P2DT");            
            //Application.ThreadException += VirException.VirException.VirExceptionSt.OnThreadException;
            Application.ThreadException += VirExcepitionNeon.VirExceptionNeon.VirExceptionNeonSt.OnThreadException;            
            Application.Run(new Form1());
        }
    }
}
