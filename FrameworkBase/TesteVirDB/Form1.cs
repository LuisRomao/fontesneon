﻿using System;
using System.Windows.Forms;
using VirMSSQL;

namespace TesteVirDB
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
            new Framework.usuarioLogado.UsuarioLogadoNeon(30);
        }

        private DataSet1TableAdapters.ApagarTableAdapter TA;
        private DataSet1 DS;
        private string GeraErro = null;

        private void button1_Click(object sender, EventArgs e)
        {
            Ciclo(false,false);
        }

        private void Ciclo(bool Erro,bool interno)
        {
            
            if (TA == null)
            {
                TA = new DataSet1TableAdapters.ApagarTableAdapter();
                TA.TrocarStringDeConexao();
                DS = new DataSet1();
            }
            try
            {
                TableAdapter.AbreTrasacaoSQL("TesteVirDB Form1 38 Ciclo", TA);                
                int lidos = TA.Fill(DS.Apagar);
                textBox1.Text += string.Format("Lidos: {0}\r\n", lidos);
                if (Erro)
                    textBox1.Text += string.Format("Erro: {0}\r\n", GeraErro.Substring(1, 1));
                VirMSSQL.TableAdapter.ST().Commit();
            }
            catch (Exception ex)
            {
                VirMSSQL.TableAdapter.ST().Vircatch(ex);
                textBox1.Text += string.Format("Erro: {0}\r\n", ex.Message);
            }
            finally
            {
                if (chLog.Checked && !interno)
                    textBox1.Text += string.Format("\r\nLog:\r\n{0}\r\n", VirMSSQL.TableAdapter.ST().Log);
            }
        }

        private void button4_Click(object sender, EventArgs e)
        {
            Ciclo(true,false);
        }

        private void CicloDuplo(bool Erro, bool interno)
        {
            
            if (TA == null)
            {
                TA = new DataSet1TableAdapters.ApagarTableAdapter();
                TA.TrocarStringDeConexao();
                DS = new DataSet1();
            }
            try
            {
                VirMSSQL.TableAdapter.AbreTrasacaoSQL("TesteVirDB Form1 67 CicloDuplo",TA);
                Ciclo(Erro,true);                
                TableAdapter.ST().Commit();
            }
            catch (Exception ex)
            {
                TableAdapter.ST().Vircatch(ex);
                textBox1.Text += string.Format("\r\nErro duplo: {0}\r\n", ex.Message);
            }
            finally
            {
                if (chLog.Checked && !interno)
                    textBox1.Text += string.Format("\r\nLog:\r\n{0}\r\n", TableAdapter.ST().Log);
            }
        }

        private void CicloTriplo(bool Erro)
        {
            
            if (TA == null)
            {
                TA = new DataSet1TableAdapters.ApagarTableAdapter();
                TA.TrocarStringDeConexao();
                DS = new DataSet1();
            }
            try
            {
                VirMSSQL.TableAdapter.AbreTrasacaoSQL("TesteVirDB Form1 67 CicloTriplo", TA);
                CicloDuplo(Erro,true);
                TableAdapter.ST().Commit();
            }
            catch (Exception ex)
            {
                TableAdapter.ST().Vircatch(ex);
                textBox1.Text += string.Format("\r\nErro duplo: {0}\r\n", ex.Message);
            }
            finally
            {
                if (chLog.Checked)
                    textBox1.Text += string.Format("\r\nLog:\r\n{0}\r\n", TableAdapter.ST().Log);
            }
        }

        private void button5_Click(object sender, EventArgs e)
        {
            CicloDuplo(false,false);
        }

        private void button6_Click(object sender, EventArgs e)
        {
            CicloDuplo(true,false);
        }

        private void checkBox1_CheckedChanged(object sender, EventArgs e)
        {
            TableAdapter.ST().GeraLog = chLog.Checked;
        }

        private void button7_Click(object sender, EventArgs e)
        {            
            textBox1.Text = "";
            if(TableAdapter.ST().STTransAtiva())
                textBox1.Text = string.Format("Transação ativa : {0}\r\n{1}\r\n", TableAdapter.ST().GetSubTrans, TableAdapter.ST().Log);
            //TableAdapter.ST().Log = "";
        }

        private void button9_Click(object sender, EventArgs e)
        {
            CicloTriplo(false);
        }

        private void button8_Click(object sender, EventArgs e)
        {
            CicloTriplo(true);
        }

        private void button2_Click(object sender, EventArgs e)
        {
            TableAdapter.AbreTrasacaoSQL("Abertura independente");
        }

        private void button3_Click(object sender, EventArgs e)
        {
            TableAdapter.ST().Commit();
            textBox1.Text = string.Format("Transação ativa : {0}\r\n{1}\r\n", TableAdapter.ST().GetSubTrans, TableAdapter.ST().Log);
        }
        

        private void button10_Click(object sender, EventArgs e)
        {
            try
            {
                string erro = "12";
                MessageBox.Show(erro.Substring(4, 3));
            }
            catch (Exception ex)
            {
                VirMSSQL.TableAdapter.VircatchSQL(ex);
            }
        }

        private void button11_Click(object sender, EventArgs e)
        {
            WS_BCB.BuscaIndices BuscaINPC = new WS_BCB.BuscaIndices();
            string resposta = BuscaINPC.lista(new DateTime(2000, 01, 01), DateTime.Today,WS_BCB.BuscaIndices.TiposBusca.INPC);          
            MessageBox.Show(string.Format("Lido:\r\n{0}", resposta));
            Clipboard.SetText(resposta);
            if (BuscaINPC.BuscaUltimo(WS_BCB.BuscaIndices.TiposBusca.INPC))
                MessageBox.Show(string.Format("Lido:{0} - {1:n4}", BuscaINPC.Comp, BuscaINPC.Valor));
            
        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            if (TableAdapter.ST().LogVircatchRetornando != "")
            {
                textBox1.Text += "ERRO LogVircatchRetornando:\r\n\r\n" + TableAdapter.ST().LogVircatchRetornando;
                //TableAdapter.ST().LogVircatchRetornando = "";
            }
            DateTime? DateTimeInicioTrans = TableAdapter.ST().DateTimeInicioTrans;
            if (DateTime.Now.AddSeconds(-30) > DateTimeInicioTrans.GetValueOrDefault(DateTime.Now))
            {
                TimeSpan TS = (TimeSpan)(DateTime.Now - DateTimeInicioTrans.Value);
                string DadosDoProblema = string.Format("{0}\r\nIdentificação:{1}\r\nTempo: {2:n4}s\r\nLOG\r\n{4}\r\nVersão:{3}",
                                                       VirException.VirException.CabecalhoRetorno(),
                                                       TableAdapter.ST().IdentificadorChamada,
                                                       TS.TotalSeconds,
                                                       VirException.VirException.VersaoAssembles(),
                                                       TableAdapter.ST().Log
                                                       );
                textBox1.Text = DadosDoProblema;
            }
        }

        private void button12_Click(object sender, EventArgs e)
        {
            if (TA == null)
            {
                TA = new DataSet1TableAdapters.ApagarTableAdapter();
                TA.TrocarStringDeConexao();
                DS = new DataSet1();
            }
            try
            {
                TableAdapter.AbreTrasacaoSQL("Testa Trava", TA);
                int lidos = TA.Fill(DS.Apagar);
                textBox1.Text += string.Format("Lidos: {0}\r\n", lidos);
                Nivel2();
                TableAdapter.ST().Commit();
            }
            catch (Exception ex)
            {
                //TableAdapter.ST().Vircatch(ex);
                textBox1.Text += string.Format("Erro: {0}\r\n", ex.Message);
            }
            finally
            {
                if (chLog.Checked)
                    textBox1.Text += string.Format("\r\nLog:\r\n{0}\r\n", TableAdapter.ST().Log);
            }
        }

        private void Nivel2()
        {
            if (TA == null)
            {
                TA = new DataSet1TableAdapters.ApagarTableAdapter();
                TA.TrocarStringDeConexao();
                DS = new DataSet1();
            }
            try
            {
                TableAdapter.AbreTrasacaoSQL("Testa Trava", TA);
                int lidos = TA.Fill(DS.Apagar);
                textBox1.Text += string.Format("Lidos: {0}\r\n", lidos);
                textBox1.Text += string.Format("Erro: {0}\r\n", GeraErro.Substring(1, 1));
                TableAdapter.ST().Commit();
            }
            catch (Exception ex)
            {
                TableAdapter.ST().Vircatch(ex);
                textBox1.Text += string.Format("Erro: {0}\r\n", ex.Message);
            }            
        }

        private void button13_Click(object sender, EventArgs e)
        {
            DateTime Liberar = DateTime.Now.AddSeconds(30);
            textBox1.Text += "Travado\r\n";
            Console.WriteLine("Travado");
            while (DateTime.Now < Liberar);
            textBox1.Text += "Liberado\r\n";
            Console.WriteLine("Liberado");
        }

        
    }
}
