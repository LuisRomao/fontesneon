using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace Teste2
{
    public partial class UserControl1 : CompontesBasicos.ComponenteCamposBase
    {
        public UserControl1()
        {
            InitializeComponent();
        }

        private void calcEdit1_Properties_Click(object sender, EventArgs e)
        {
            MessageBox.Show("Clicou");
        }

        private string CheckHorizontal(int Banco,int Ag,int cc,decimal valor,int operacao) {
            decimal dadosfav = cc + Ag * 10000000000M + Banco * 100000000000000M;

            decimal retorno = (dadosfav + valor * 100M) * operacao;

            retorno = retorno % 1000000000000000000M;
            return retorno.ToString("F0").PadRight(18, '0');
        } 

        private void simpleButton1_Click(object sender, EventArgs e)
        {
            //MessageBox.Show(CheckHorizontal(999,961,102444,150150.21M,9).ToString());
            //MessageBox.Show(CheckHorizontal(409,48,118772,12.13M,5).ToString());
            DataTable Tabela = new DataTable();            
            Tabela.Columns.Add("NumeroDaAgenciaCliente", typeof(int));            
            Tabela.Columns.Add("NumeroDaContaCliente", typeof(int));
            Tabela.Columns.Add("DigitoVerificadorCC", typeof(int));
            Tabela.Columns.Add("NumeroDaAgenciaFavorecido", typeof(int));
            Tabela.Columns.Add("NumeroDaContaFavorecido", typeof(int));
            Tabela.Columns.Add("ValorCredito", typeof(decimal));
            Tabela.Columns.Add("DataCredito", typeof(DateTime));
            Tabela.Columns.Add("NomeFavorecido", typeof(string));
            Tabela.Columns.Add("CPFCNPJ", typeof(Int64));
            Tabela.Columns.Add("DigitoCCFavorecido", typeof(int));
            Tabela.Columns.Add("CheckHorizontal", typeof(string));

            Tabela.Rows.Add(091, 235968, 4, 48, 118772, 12.13M, DateTime.Today.AddDays(2), "Luis Henrique", 13553185805, 2, CheckHorizontal(409, 48, 118772, 12.13M, 5));
            Tabela.Rows.Add(091, 235968, 4, 48, 118772, 100M, DateTime.Today.AddDays(3), "Luis Henrique", 13553185805, 2, CheckHorizontal(409, 48, 118772, 100M, 5));
            Tabela.Rows.Add(813, 142737, 4, 48, 118772, 1000M, DateTime.Today.AddDays(4), "Luis Henrique", 13553185805, 2, CheckHorizontal(409, 48, 118772, 1000M, 5));
            //Tabela.Rows.Add(813, 142737, 2, 961, 102444, 150150.21M, DateTime.Today.AddDays(4), "Luis Henrique", 13553185805, 2, CheckHorizontal(409, 961, 102444, 150150.21M, 5));
            DataTable TabelaH = new DataTable();
            TabelaH.Columns.Add("NumeroDaAgenciaCliente", typeof(int));
            TabelaH.Columns.Add("NomeCliente", typeof(string));
            TabelaH.Columns.Add("NumeroDaContaCliente", typeof(int));
            TabelaH.Columns.Add("DigitoVerificadorCC", typeof(int));
            
            
            //TabelaH.Columns.Add("NomeCliente", typeof(string));
            TabelaH.Rows.Add(813, "NEON IMOVEIS", 142737,2);
            ArquivosTXT.cTabelaTXT Remessa = new ArquivosTXT.cTabelaTXT(Tabela, ArquivosTXT.ModelosTXT.layout);
            Remessa.rowCabecalho = TabelaH.Rows[0];
            Remessa.RegistraMapaDeA("CodigoDeRegistro", 1, 1, ArquivosTXT.TipoMapa.cabecalho, 0);
            Remessa.RegistraMapaDeA("CodigoDeRemessa", 2, 2, ArquivosTXT.TipoMapa.cabecalho, 1);
            Remessa.RegistraMapaDeA("LiteralRemessa", 3, 9, ArquivosTXT.TipoMapa.cabecalho, "REMESSA");
            Remessa.RegistraMapaDeA("CodigoDeServico", 10, 11, ArquivosTXT.TipoMapa.cabecalho, 8);
            Remessa.RegistraMapaDeA("Identificacao", 12, 25, ArquivosTXT.TipoMapa.cabecalho, "CONTAS A PAGAR");
            Remessa.RegistraMapaDeA("NumeroDaAgenciaCliente", 26, 29, ArquivosTXT.TipoMapa.cabecalho,null);
            Remessa.RegistraMapaDeA("NumeroDaContaCliente", 30, 35, ArquivosTXT.TipoMapa.cabecalho, null);
            Remessa.RegistraMapaDeA("DigitoVerificadorCC", 36, 36, ArquivosTXT.TipoMapa.cabecalho, null);
            Remessa.RegistraMapaDeA("NomeCliente", 37,66, ArquivosTXT.TipoMapa.cabecalho, null);
            Remessa.RegistraMapaDeA("Banco", 67, 69, ArquivosTXT.TipoMapa.cabecalho, "409");
            Remessa.RegistraMapaDeA("NomeBanco", 70, 84, ArquivosTXT.TipoMapa.cabecalho, "UNIBANCO");
            Remessa.RegistraMapaDeA("DataArquivo", 85, 90, ArquivosTXT.TipoMapa.cabecalho,ArquivosTXT.TiposDeCampo.AutoData, null);
            Remessa.RegistraMapaDeA("Sequencial", 315, 320, ArquivosTXT.TipoMapa.cabecalho, ArquivosTXT.TiposDeCampo.Sequencial,null);


            Remessa.RegistraMapaDeA("CodigoDeRegistro", 1, 1, ArquivosTXT.TipoMapa.detalhe, 2);
            Remessa.RegistraMapaDeA("NumeroDaAgenciaCliente", 2, 5, ArquivosTXT.TipoMapa.detalhe, null);
            Remessa.RegistraMapaDeA("NumeroDaContaCliente", 6, 11, ArquivosTXT.TipoMapa.detalhe, null);
            Remessa.RegistraMapaDeA("DigitoVerificadorCC", 12, 12, ArquivosTXT.TipoMapa.detalhe, null);
            Remessa.RegistraMapaDeA("BancoFavorercido", 33, 36, ArquivosTXT.TipoMapa.detalhe, 409);
            Remessa.RegistraMapaDeA("NumeroDaAgenciaFavorecido", 37, 40, ArquivosTXT.TipoMapa.detalhe,null);
            Remessa.RegistraMapaDeA("NumeroDaContaFavorecido", 41, 50, ArquivosTXT.TipoMapa.detalhe, null);
            Remessa.RegistraMapaDeA("MeioDeRepasse", 51, 51, ArquivosTXT.TipoMapa.detalhe, 4);
            Remessa.RegistraMapaDeA("Aviso", 52, 52, ArquivosTXT.TipoMapa.detalhe, 1);
            Remessa.RegistraMapaDeA("TipoOper", 53, 53, ArquivosTXT.TipoMapa.detalhe, 5);
            Remessa.RegistraMapaDeA("TipoServico", 54, 54, ArquivosTXT.TipoMapa.detalhe, 1);
            Remessa.CampoSomatorio = "ValorCredito";
            Remessa.RegistraMapaDeA(Remessa.CampoSomatorio, 55, 67, ArquivosTXT.TipoMapa.detalhe, null);            
            Remessa.RegistraMapaDeA("Ident", 68, 82, ArquivosTXT.TipoMapa.detalhe, 0);
            Remessa.RegistraMapaDeA("Data", 83, 88, ArquivosTXT.TipoMapa.detalhe, ArquivosTXT.TiposDeCampo.AutoData, null);
            Remessa.RegistraMapaDeA("DataCredito", 89, 94, ArquivosTXT.TipoMapa.detalhe, ArquivosTXT.TiposDeCampo.normal, DateTime.Today.AddDays(1));
            Remessa.RegistraMapaDeA("NomeFavorecido", 95, 124, ArquivosTXT.TipoMapa.detalhe, null);
            Remessa.RegistraMapaDeA("Moeda", 235, 236, ArquivosTXT.TipoMapa.detalhe, 14);
            Remessa.RegistraMapaDeA("MossoNumero", 248, 258, ArquivosTXT.TipoMapa.detalhe, 0);
            Remessa.RegistraMapaDeA("Ocorrencia", 259, 260, ArquivosTXT.TipoMapa.detalhe, 1);
            Remessa.RegistraMapaDeA("MesmaTit", 261, 261, ArquivosTXT.TipoMapa.detalhe, 0);
            Remessa.RegistraMapaDeA("CheckHorizontal", 263, 280, ArquivosTXT.TipoMapa.detalhe, "");
            Remessa.RegistraMapaDeA("TipoCPFCNPJ", 281, 282, ArquivosTXT.TipoMapa.detalhe, 1);
            Remessa.RegistraMapaDeA("CPFCNPJ", 283, 296, ArquivosTXT.TipoMapa.detalhe, null);
            //acertar ??? pode ser zero
            Remessa.RegistraMapaDeA("CodigoHistorico", 307, 311, ArquivosTXT.TipoMapa.detalhe, 0);
            Remessa.RegistraMapaDeA("DigitoCCFavorecido", 314, 314, ArquivosTXT.TipoMapa.detalhe, null);
            Remessa.RegistraMapaDeA("Sequencial", 315, 320, ArquivosTXT.TipoMapa.detalhe,ArquivosTXT.TiposDeCampo.Sequencial,null);


            Remessa.RegistraMapaDeA("CodigoDeRegistro", 1, 1, ArquivosTXT.TipoMapa.rodape, ArquivosTXT.TiposDeCampo.normal, 9);
            Remessa.RegistraMapaDeA("QuantidadeDePagamentos", 2, 7, ArquivosTXT.TipoMapa.rodape, ArquivosTXT.TiposDeCampo.NDetalhe, null);
            Remessa.RegistraMapaDeA("ValorDosPagamentos", 8, 20, ArquivosTXT.TipoMapa.rodape, ArquivosTXT.TiposDeCampo.Somatorio, null);
            //acertar
            Remessa.RegistraMapaDeA("QuantidadeDeRegistros", 21, 26, ArquivosTXT.TipoMapa.rodape, ArquivosTXT.TiposDeCampo.Sequencial, 0M);
            Remessa.RegistraMapaDeA("Sequencial", 315, 320, ArquivosTXT.TipoMapa.rodape, ArquivosTXT.TiposDeCampo.Sequencial, null);

            if (Remessa.Salva(@"C:\teste.txt"))
                MessageBox.Show("OK");
            else
                MessageBox.Show("falha:"+Remessa.UltimoErro);

            UnibancoEDI.dRemessaCC.TabelaDeCreditosDataTable TabelaUni = new UnibancoEDI.dRemessaCC.TabelaDeCreditosDataTable();
            
            TabelaUni.AddTabelaDeCreditosRow(091, 235968, 4, 48, 118772, 12.13M, DateTime.Today.AddDays(2), "Luis Henrique", 13553185805, 2,"");
            TabelaUni.Rows.Add(091, 235968, 4, 48, 118772, 100M, DateTime.Today.AddDays(3), "Luis Henrique", 13553185805, 2, "");
            TabelaUni.Rows.Add(813, 142737, 4, 48, 118772, 1000M, DateTime.Today.AddDays(4), "Luis Henrique", 13553185805, 2, "");
            UnibancoEDI.RemessaCC RemessaUni = new UnibancoEDI.RemessaCC(TabelaUni, 813, 142737, 2,"NEON IMOVEIS");
            if (RemessaUni.Salva(@"C:\TesteUni.txt"))
                MessageBox.Show("OK");
            else
                MessageBox.Show("falha:" + Remessa.UltimoErro);

        }

        private void simpleButton2_Click(object sender, EventArgs e)
        {
           
        }

        private void simpleButton3_Click(object sender, EventArgs e)
        {
            CompontesBasicos.Performance.Performance.PerformanceST.Gergistra("T1");
            MessageBox.Show("Parada");
            CompontesBasicos.Performance.Performance.PerformanceST.Gergistra("T2");
            MessageBox.Show("Parada");
            CompontesBasicos.Performance.Performance.PerformanceST.Gergistra("T1");
            MessageBox.Show("Parada");
            CompontesBasicos.Performance.Performance.PerformanceST.Gergistra("T2");
            MessageBox.Show("Parada");
            CompontesBasicos.Performance.Performance.PerformanceST.Gergistra("T3");
            MessageBox.Show("Parada");
            CompontesBasicos.Performance.Performance.PerformanceST.Gergistra("T1");
            MessageBox.Show("Parada");
            CompontesBasicos.Performance.Performance.PerformanceST.Gergistra(null);
            MessageBox.Show("Parada");
            MessageBox.Show(CompontesBasicos.Performance.Performance.PerformanceST.Relatorio());
        }

        private void simpleButton4_Click(object sender, EventArgs e)
        {
            string TMPdir = @"C:\sistema neon\UNIBANCO\TMP";
            if(System.IO.Directory.Exists(TMPdir))
                foreach(string nome in System.IO.Directory.GetFiles(TMPdir))
                    System.IO.File.Delete(nome);
            else
                System.IO.Directory.CreateDirectory(TMPdir);
            System.Diagnostics.Process process = new System.Diagnostics.Process();            
            process.StartInfo.FileName = @"c:\7z.exe";
            process.StartInfo.Arguments = @"e -o"""+TMPdir+@""" -y C:\lixo\UNIBANCO\055055651000170.zip";
            //process.StartInfo.Arguments = @"e -oC:\lixo\UNIBANCO C:\lixo\UNIBANCO\055055651000170.zip";
            process.StartInfo.Verb = "Open";
            process.StartInfo.WindowStyle = System.Diagnostics.ProcessWindowStyle.Hidden;
            //process.StartInfo.WindowStyle = System.Diagnostics.ProcessWindowStyle.Normal;            
            process.Start();
            //process.WaitForExit(60000);
            while (!process.HasExited)
                Application.DoEvents();
            MessageBox.Show("acabei");
        }

        private void simpleButton5_Click(object sender, EventArgs e)
        {
            VirEmail.EmailDireto VirTeste = new VirEmail.EmailDireto("200.234.205.147", "luis@virweb.com.br", "venus", "luis@virweb.com.br");
            for (int i = 1; i <= 10; i++)
                VirTeste.EnviarArquivos("virtual@virweb.com.br", "Fogando " + i.ToString(), "Fogando " + i.ToString(), @"c:\tmp\00nn1");

        }

        private void simpleButton6_Click(object sender, EventArgs e)
        {
            DataSet1 DS = new DataSet1();
            DataSet1TableAdapters.USUARIOSTableAdapter TA = new Teste2.DataSet1TableAdapters.USUARIOSTableAdapter();
            TA.TrocarStringDeConexao();
            System.Reflection.PropertyInfo[] pro = typeof(DataSet1TableAdapters.USUARIOSTableAdapter).GetProperties();
        }
    }
}

