using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace Teste2
{
    public partial class Form5try : Form
    {
        public Form5try()
        {
            InitializeComponent();
        }

        int ContadorGeral;

        private void button1_Click(object sender, EventArgs e)
        {
            ContadorGeral = 1;
            memoEdit1.Text = "";
            try
            {
                VirMSSQL.TableAdapter.STTableAdapter.AbreTrasacaoST();
                VirOleDb.TableAdapter.STTableAdapter.AbreTrasacaoST();
                memoEdit1.Text = string.Format("In�cio s{0}\r\n",VirMSSQL.TableAdapter.STTableAdapter);
                memoEdit1.Text += string.Format("In�cio s{0}\r\n", VirOleDb.TableAdapter.STTableAdapter.GetSubTrans);
                Sub(1);
                VirMSSQL.TableAdapter.STTableAdapter.Commit();
                VirOleDb.TableAdapter.STTableAdapter.Commit();
                memoEdit1.Text += string.Format("commit s{0}\r\n", VirMSSQL.TableAdapter.STTableAdapter.GetSubTrans);
                memoEdit1.Text += string.Format("commit s{0}\r\n", VirOleDb.TableAdapter.STTableAdapter.GetSubTrans);
            }
            catch (Exception Ex)
            {
                memoEdit1.Text += string.Format("catch - {0} s{1}\r\n", Ex.Message, VirMSSQL.TableAdapter.STTableAdapter.GetSubTrans);
                memoEdit1.Text += string.Format("catch - {0} s{1}\r\n", Ex.Message, VirOleDb.TableAdapter.STTableAdapter.GetSubTrans);
                VirOleDb.TableAdapter.STTableAdapter.Vircatch(null);                
                VirMSSQL.TableAdapter.STTableAdapter.Vircatch(Ex);                
            }
        }

        private VirMSSQL.TableAdapter Ts = new VirMSSQL.TableAdapter();
        private VirOleDb.TableAdapter Ts1 = new VirOleDb.TableAdapter();

        private void Sub(int nivel)
        {
            int MeuContador = ContadorGeral++;

            string Recuo = "";
            for (int i = 1; i <= nivel; i++)
                Recuo += "     ";
            try
            {                
                VirMSSQL.TableAdapter.STTableAdapter.AbreTrasacaoST();
                memoEdit1.Text += string.Format("{0}In�cio({1}) - {2} s{3}\r\n", Recuo, nivel, MeuContador, VirMSSQL.TableAdapter.STTableAdapter.GetSubTrans);                
                if ((nivel % 2) == 0)
                {                    
                    VirOleDb.TableAdapter.STTableAdapter.AbreTrasacaoST();
                    memoEdit1.Text += string.Format("{0}In�cio({1}) - {2} s{3}\r\n", Recuo, nivel, MeuContador, VirOleDb.TableAdapter.STTableAdapter.GetSubTrans);
                }
                if (nivel < 5)
                {
                    Sub(nivel + 1);
                    if(nivel == 2)
                        Sub(nivel + 1);
                    if (nivel == 2)
                        Sub(nivel + 1);
                }                
                if (MeuContador == spinEdit1.Value) 
                {                    
                    VirMSSQL.TableAdapter.STTableAdapter.RollBack("Cancelamento planejado");
                    memoEdit1.Text += string.Format("{0}rollback({1}) - {2} s{3}\r\n", Recuo, nivel, MeuContador, VirMSSQL.TableAdapter.STTableAdapter.GetSubTrans);
                }
                else if (MeuContador == spinEdit2.Value)
                {
                    memoEdit1.Text += string.Format("{0}Gerar excessao({1}) - {2} s{3}\r\n", Recuo, nivel, MeuContador, VirMSSQL.TableAdapter.STTableAdapter.GetSubTrans);
                    GeraExcessao();
                }
                else 
                {
                    VirMSSQL.TableAdapter.STTableAdapter.Commit();                   
                    memoEdit1.Text += string.Format("{0}comit({1}) - {2} s{3}\r\n", Recuo, nivel, MeuContador, VirMSSQL.TableAdapter.STTableAdapter.GetSubTrans);
                    if ((nivel % 2) == 0)
                    {
                        VirOleDb.TableAdapter.STTableAdapter.Commit();
                        memoEdit1.Text += string.Format("{0}comit({1}) - {2} s{3}\r\n", Recuo, nivel, MeuContador, VirOleDb.TableAdapter.STTableAdapter.GetSubTrans);
                    }
                }
                
            }
            catch (Exception Ex)
            {
                memoEdit1.Text += string.Format("{0}catch({1}) - {2} - {3} s{4}\r\n", Recuo, nivel, MeuContador, Ex.Message, VirMSSQL.TableAdapter.STTableAdapter.GetSubTrans);
                if ((nivel % 2) == 0)
                {
                    memoEdit1.Text += string.Format("{0}catch({1}) - {2} - {3} s{4}\r\n", Recuo, nivel, MeuContador, Ex.Message, VirOleDb.TableAdapter.STTableAdapter.GetSubTrans);
                    VirOleDb.TableAdapter.STTableAdapter.Vircatch(null);
                };
                VirMSSQL.TableAdapter.STTableAdapter.Vircatch(Ex);                
            }
        }

        public object o;
        public string s;

        private void GeraExcessao()
        {
            string s = o.ToString();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            memoEdit1.Text = string.Format("ST commit\r\n");                
            TestaIndividual(VirMSSQL.TableAdapter.STTableAdapter, Teste.Commit,0);
            memoEdit1.Text += string.Format("\r\nST rollback\r\n");
            TestaIndividual(VirMSSQL.TableAdapter.STTableAdapter, Teste.rollback,0);
            memoEdit1.Text += string.Format("\r\nST Erro\r\n");
            TestaIndividual(VirMSSQL.TableAdapter.STTableAdapter, Teste.erro,0);

            VirMSSQL.TableAdapter T1 = new VirMSSQL.TableAdapter();
            try
            {
                VirMSSQL.TableAdapter.STTableAdapter.AbreTrasacaoST();    
            
                memoEdit1.Text += string.Format("\r\n\r\nT commit\r\n");
                TestaIndividual(T1, Teste.Commit, 0);
                memoEdit1.Text += string.Format("\r\nT roolback\r\n");
                TestaIndividual(T1, Teste.rollback, 0);
                memoEdit1.Text += string.Format("\r\nT erro\r\n");
                TestaIndividual(T1, Teste.erro, 0);

                VirMSSQL.TableAdapter.STTableAdapter.Commit();

            }
            catch (Exception Ex)
            {
                VirMSSQL.TableAdapter.STTableAdapter.Vircatch(Ex);
            }

        }

        enum Teste { Commit,rollback,erro }

        private void TestaIndividual(VirMSSQL.TableAdapter T,Teste Tipo,int nivel) 
        {
            string Recuo = "";
            for (int i = 1; i <= nivel; i++)
                Recuo += "     ";
            try
            {
                T.AbreTrasacao();
                memoEdit1.Text += string.Format("{0}In�cio s{1}\r\n", Recuo, T.GetSubTrans);                
                switch (Tipo)
                {
                    case Teste.Commit:
                        //T.FimTrasacaoCInd();
                        T.Commit();
                        memoEdit1.Text += string.Format("{0}comit s{1}\r\n", Recuo, T.GetSubTrans);
                        break;
                    case Teste.rollback:
                        T.RollBack("Cancelamento");
                        memoEdit1.Text += string.Format("{0}rollback s{1}\r\n", Recuo, T.GetSubTrans);
                        break;
                    case Teste.erro:
                        memoEdit1.Text += string.Format("{0}Gerar excessao s{1}\r\n", Recuo, T.GetSubTrans);
                        GeraExcessao();
                        break;                    
                }
                
            }
            catch (Exception Ex)
            {
                memoEdit1.Text += string.Format("{0}catch {1} s{2}\r\n", Recuo, Ex.Message, T.GetSubTrans);
                T.Vircatch(Ex);
            }
        }


        private void ChamadaDireta(VirMSSQL.TableAdapter T, VirOleDb.TableAdapter T1)
        {
            ContadorGeral = 1;            
            try
            {
                T.AbreTrasacao();
                T1.AbreTrasacao();
                memoEdit1.Text += string.Format("In�cio s{0}\r\n", T.GetSubTrans);
                memoEdit1.Text += string.Format("In�cio s{0}\r\n", T1.GetSubTrans);
                SubDireto(1,T,T1);
                T.Commit();
                T1.Commit();
                memoEdit1.Text += string.Format("commit s{0}\r\n", T.GetSubTrans);
                memoEdit1.Text += string.Format("commit s{0}\r\n", T1.GetSubTrans);
            }
            catch (Exception Ex)
            {
                memoEdit1.Text += string.Format("catch - {0} s{1}\r\n", Ex.Message, T.GetSubTrans);
                memoEdit1.Text += string.Format("catch - {0} s{1}\r\n", Ex.Message, T1.GetSubTrans);
                T1.Vircatch(null);
                T.Vircatch(Ex);
            }
        }

        private void button4_Click(object sender, EventArgs e)
        {
            memoEdit1.Text = string.Format("Teste ST\r\n"); 
            ChamadaDireta(VirMSSQL.TableAdapter.STTableAdapter,VirOleDb.TableAdapter.STTableAdapter);
            memoEdit1.Text += string.Format("\r\n\r\nTeste com new\r\n"); 
            ChamadaDireta(new VirMSSQL.TableAdapter(),new VirOleDb.TableAdapter());
        }

        private void SubDireto(int nivel,VirMSSQL.TableAdapter T,VirOleDb.TableAdapter T1)
        {
            int MeuContador = ContadorGeral++;

            string Recuo = "";
            for (int i = 1; i <= nivel; i++)
                Recuo += "     ";
            try
            {
                T.AbreTrasacao();
                memoEdit1.Text += string.Format("{0}In�cio({1}) - {2} s{3}\r\n", Recuo, nivel, MeuContador, T.GetSubTrans);
                if ((nivel % 2) == 0)
                {
                    T1.AbreTrasacao();
                    memoEdit1.Text += string.Format("{0}In�cio({1}) - {2} s{3}\r\n", Recuo, nivel, MeuContador, T1.GetSubTrans);
                }
                if (nivel < 5)
                {
                    SubDireto(nivel + 1,T,T1);
                    if (nivel == 2)
                        SubDireto(nivel + 1, T, T1);
                    if (nivel == 2)
                        SubDireto(nivel + 1, T, T1);
                }
                if (MeuContador == spinEdit1.Value)
                {
                    T.RollBack("Cancelamento planejado");
                    memoEdit1.Text += string.Format("{0}rollback({1}) - {2} s{3}\r\n", Recuo, nivel, MeuContador, T.GetSubTrans);
                }
                else if (MeuContador == spinEdit2.Value)
                {
                    memoEdit1.Text += string.Format("{0}Gerar excessao({1}) - {2} s{3}\r\n", Recuo, nivel, MeuContador, T.GetSubTrans);
                    GeraExcessao();
                }
                else
                {
                    T.Commit();
                    memoEdit1.Text += string.Format("{0}comit({1}) - {2} s{3}\r\n", Recuo, nivel, MeuContador, T.GetSubTrans);
                    if ((nivel % 2) == 0)
                    {
                        T1.Commit();
                        memoEdit1.Text += string.Format("{0}comit({1}) - {2} s{3}\r\n", Recuo, nivel, MeuContador, T1.GetSubTrans);
                    }
                }

            }
            catch (Exception Ex)
            {
                memoEdit1.Text += string.Format("{0}catch({1}) - {2} - {3} s{4}\r\n", Recuo, nivel, MeuContador, Ex.Message, T.GetSubTrans);
                if ((nivel % 2) == 0)
                {
                    memoEdit1.Text += string.Format("{0}catch({1}) - {2} - {3} s{4}\r\n", Recuo, nivel, MeuContador, Ex.Message, T1.GetSubTrans);
                    T1.Vircatch(null);
                };
                T.Vircatch(Ex);
            }
        }

        private void button3_Click(object sender, EventArgs e)
        {
            VirMSSQL.TableAdapter T = new VirMSSQL.TableAdapter();
            try 
            {
                memoEdit1.Text += string.Format("In�cio s{0}\r\n",  T.GetSubTrans);                
                T.AbreTrasacao();
                memoEdit1.Text += string.Format("In�cio s{0}\r\n", T.GetSubTrans);
                try
                {
                    memoEdit1.Text += string.Format("In�cio s{0}\r\n", T.GetSubTrans);
                    GeraExcessao();
                    T.AbreTrasacao();
                    memoEdit1.Text += string.Format("In�cio s{0}\r\n", T.GetSubTrans);
                    GeraExcessao();
                }
                catch (Exception Ex)
                {
                    memoEdit1.Text += string.Format("catch {0} s{1}\r\n", Ex.Message, T.GetSubTrans);
                    T.Vircatch(Ex);
                }
                GeraExcessao();
            }
            catch(Exception Ex) 
            {
                memoEdit1.Text += string.Format("catch {0} s{1}\r\n", Ex.Message, T.GetSubTrans);
                T.Vircatch(Ex);
            }
        }

    }
}