using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace Teste2
{
    public partial class Form3 : CompontesBasicos.FormPrincipalBase
    {
        public Form3()
        {
            InitializeComponent();
        }

        private void simpleButton1_Click(object sender, EventArgs e)
        {
            
        }

        private void navBarItem1_LinkClicked(object sender, DevExpress.XtraNavBar.NavBarLinkEventArgs e)
        {
            ShowMoludo(typeof(UserControl1), "Titulo", CompontesBasicos.TipoDeCarga.navegacao, 0, false);
            //ShowModulo(typeof(CompontesBasicos.ComponenteCamposBase), true);
            //ShowModulo(typeof(CompontesBasicos.ComponenteCamposBase), true);
            //CompontesBasicos.ComponenteCamposBase a = new CompontesBasicos.ComponenteCamposBase();
            //ShowModuloCamposNovo(typeof(CompontesBasicos), "", this);
        }

        public object oErro;

        private void navBarItem3_LinkClicked(object sender, DevExpress.XtraNavBar.NavBarLinkEventArgs e)
        {
            MessageBox.Show(oErro.ToString());
        }

        private void navBarGroup1_ItemChanged(object sender, EventArgs e)
        {

        }

        private void navBarItem4_LinkClicked(object sender, DevExpress.XtraNavBar.NavBarLinkEventArgs e)
        {
            VirMSSQL.CEP testaCEP = new VirMSSQL.CEP();
            if (testaCEP.BuscaPorCEP("09010130"))
                MessageBox.Show(testaCEP.Cidade + " " + testaCEP.Estado + " " + testaCEP.Bairro + " " + testaCEP.Rua);
            else
                MessageBox.Show("N�o encontrado");
        }

        private void Importa_LinkClicked(object sender, DevExpress.XtraNavBar.NavBarLinkEventArgs e)
        {
            VirInput.Importador.cImportador Imp = new VirInput.Importador.cImportador();
            Imp.AddColunaImp("testei", "teste I", typeof(int));
            Imp.AddColunaImp("tested", "teste D", typeof(decimal));
            Imp.AddColunaImp("testes", "teste S", typeof(string));
            Imp.Carrega();
            Imp.VirShowModulo(CompontesBasicos.EstadosDosComponentes.PopUp);
        }

        private void navBarItem5_LinkClicked(object sender, DevExpress.XtraNavBar.NavBarLinkEventArgs e)
        {
           
        }

        private void navBarItem6_LinkClicked(object sender, DevExpress.XtraNavBar.NavBarLinkEventArgs e)
        {
            string relatorio = "";
            int nOk = 0;
            int nErro = 0;
            foreach (string arquivo in System.IO.Directory.GetFiles(@"c:\fontesvs\frameworkbase\dllcnab\arquivos"))
            {
                dllCNAB.CNAB240 testaCnab = new dllCNAB.CNAB240();
                if (testaCnab.Carrega(arquivo))
                    nOk++;
                else
                    nErro++;
                relatorio += testaCnab.Relatorio();                
                relatorio += "\r\n\r\n\r\n\r\n\r\n";
            }
            Clipboard.SetText(relatorio);
            MessageBox.Show(string.Format("Arquivos:{0}\r\n\r\nOK:{1}\r\nErro:{2}",nErro+nOk,nOk,nErro));
        }

        private void navBarItem8_LinkClicked(object sender, DevExpress.XtraNavBar.NavBarLinkEventArgs e)
        {
            dllEmissorNFE.Emissor Emissor = new dllEmissorNFE.Emissor();
            if (Emissor.gerarArquivo("c:\\fontesvs\\testenfe.txt", 10191,DateTime.Today,"Nome Cliente","cliente@x.com","ISENTO",01124173000184,"RUA","n 1","sala1","simus",3557006,"Votorantim","SP",18055185,32225277, dllEmissorNFE.Emissor.TipoDePag.prazo))
                MessageBox.Show("ok");
            else
            { 
                MessageBox.Show(Emissor.UltimoErro.Message); 
            }
        }

        private void navBarItem9_LinkClicked(object sender, DevExpress.XtraNavBar.NavBarLinkEventArgs e)
        {
            /*
            String Caminho = System.IO.Path.GetDirectoryName(Application.ExecutablePath);

            DocBacarios.CPFCNPJ cnpjSBC = new DocBacarios.CPFCNPJ("55.055.651/0001-70");
            DocBacarios.CPFCNPJ cnpjSA = new DocBacarios.CPFCNPJ("06.888.260/0001-21");

            

            dllEmissorNFS_e.LoteRPS Lote = new dllEmissorNFS_e.LoteRPS(dllEmissorNFS_e.VerssaoXSD.v202, cnpjSBC, 46653, 3557006, 1,1);
            //dllEmissorNFS_e.LoteRPS Lote = new dllEmissorNFS_e.LoteRPS(dllEmissorNFS_e.VerssaoXSD.v202, cnpjSA, 161504, 3557006, 1, 1);

            if (Lote.Erro)
            {
                MessageBox.Show("erro no lote\r\n\r\n" + Lote.strErro);
                Clipboard.SetText("erro no lote\r\n\r\n" + Lote.strErro);
            }
            else
            {
                dllEmissorNFS_e.LoteRPS.virTomador Tomador = new dllEmissorNFS_e.LoteRPS.virTomador(new DocBacarios.CPFCNPJ(1124173000184), "Nome cond", "Rua XXX", "10", "BAIRRO", 3557006, "SP","18055185","32225277","luis@virweb.com.br");
               
                if (!Lote.IncluirNota(2000.1M, 2, dllEmissorNFS_e.LoteRPS.RetIss.comRetencao, Tomador,""))
                {
                    MessageBox.Show("erro na nota\r\n\r\n" + Lote.strErro);
                    Clipboard.SetText("erro na nota\r\n\r\n" + Lote.strErro);
                }
                else
                    if (!Lote.IncluirNota(200M, 2, dllEmissorNFS_e.LoteRPS.RetIss.semRetencao, Tomador, ""))
                    {
                        MessageBox.Show("erro na nota\r\n\r\n" + Lote.strErro);
                        Clipboard.SetText("erro na nota\r\n\r\n" + Lote.strErro);
                    }
                    else
                        if (Lote.GeraXML(@"c:\lixo\", dllEmissorNFS_e.LoteRPS.ArquivoNaoAssinado.Gravar))
                            MessageBox.Show("ok");
                        else
                        {
                            //Teste += string.Format("{0:F} {1} {2} {3}\r\n", 1.01M, 2M, DateTime.Today, DateTime.Now);
                            //MessageBox.Show(Teste);
                            MessageBox.Show("erro na gera��o\r\n\r\n" + Lote.strErro);
                            Clipboard.SetText("erro na gera��o\r\n\r\n" + Lote.strErro);
                            //Application.CurrentCulture.NumberFormat.NumberDecimalSeparator 
                        }
            }*/
        }
    }
}

