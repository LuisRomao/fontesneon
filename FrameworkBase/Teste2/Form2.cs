using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;


namespace Teste2
{
    public partial class Form2 : Form
    {
        public Form2()
        {
            InitializeComponent();
            
        }

        private void simpleButton1_Click(object sender, EventArgs e)
        {
            
        }

        DocBacarios.ImpCheque _ImpC;

        DocBacarios.ImpCheque ImpC
        {
            get
            {
                if (_ImpC == null)
                    _ImpC = new DocBacarios.ImpCheque(DocBacarios.ImpCheque.Modelos.Perto);
                return _ImpC;
            }
        }

        private void simpleButton1_Click_1(object sender, EventArgs e)
        {

            if (ImpC.Imprime(409, 129.1199M, "��S�o Paulo��", "Lis de Souza", DateTime.Today))
                MessageBox.Show("OK");
            else
                MessageBox.Show("erro:"+ImpC.StatusPerto());
            
            
            //string arquivo = @"C:\clientes\neon\CB2809A00.RET";
            //ArquivosTXT.cTabelaTXT ATXT = new ArquivosTXT.cTabelaTXT(null, ArquivosTXT.ModelosTXT.layout);

        }

        private void simpleButton2_Click(object sender, EventArgs e)
        {
            ImpC.Abrir();
            //MessageBox.Show(ImpC.VerificaStatus().ToString());
        }

        private void simpleButton3_Click(object sender, EventArgs e)
        {
            if (!ImpC.Imprime("Linha1", "Linha2", "linha3", "Lu�s Henrique Mendes Rom�o", "", "final", "abcdefghijklmnopqrstuvxyz0123456789.,", "1234567890.........2.........3.........4.........5.........6.........7.........8.........9", "9", "10", "11", "12", "13", "14", "15"))
                MessageBox.Show("Erro: " + ImpC.StatusPerto());

        }

        private void simpleButton4_Click(object sender, EventArgs e)
        {
            if (ImpC.LeDadosDoCheque())
                MessageBox.Show("OK\r\nBanco:" + ImpC.CMC7Banco.ToString() + "\r\nAg:" + ImpC.CMC7Agencia.ToString() + "\r\nConta:" + ImpC.CMC7Conta.ToString() + "\r\nN�mero:" + ImpC.CMC7Numero.ToString());
            else
                MessageBox.Show("Erro:" + ImpC.StatusPerto());
        }

        private void simpleButton5_Click(object sender, EventArgs e)
        {
            if(ImpC.RemoveCheque())
                MessageBox.Show("OK");
            else
                MessageBox.Show("Erro:" + ImpC.StatusPerto());
        }

        CodBar.Impress _ImpTeste;

        CodBar.Impress ImpTeste {
            get {
                if (_ImpTeste == null)
                    _ImpTeste = new CodBar.Impress("");
                return _ImpTeste;
            }
        }

        private void simpleButton6_Click(object sender, EventArgs e)
        {

            ImpTeste.ImprimeDOC(memoEdit1.Text);
        }

        private void simpleButton7_Click(object sender, EventArgs e)
        {
            //XtraReport1 X = new XtraReport1();
            //X.ShowPreview();
        }

        private void simpleButton8_Click(object sender, EventArgs e)
        {

            // ImpTeste.CarregaPCX("DARFxF", "c:\\darfF.pcx");                        
            string nome = "Teste";
            VirInput.Input.Execute("Nome", ref nome);
            System.Windows.Forms.OpenFileDialog dia = new OpenFileDialog();
            if (dia.ShowDialog() == DialogResult.OK)
                ImpTeste.CarregaPCX(nome, dia.FileName);

        }
    }
}