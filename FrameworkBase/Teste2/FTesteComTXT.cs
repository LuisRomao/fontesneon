using System;
using System.Windows.Forms;

namespace Teste2
{
    public partial class FTesteComTXT : Form
    {
        internal enum TiposRegistro
        {
            Invalido = -1,
            HeaderArquivo = 0,
            HeaderEmpresa = 10,
            HeaderEmpresaAdd = 12,
            RegistroTrabalhador = 30
        }

        public FTesteComTXT()
        {
            InitializeComponent();
        }

        private void simpleButton1_Click(object sender, EventArgs e)
        {
            
            ArquivosTXT.cTabelaTXT Arquivo = new ArquivosTXT.cTabelaTXT(ArquivosTXT.ModelosTXT.layout);
            ArquivosTXT.cTabelaTXT ArquivoSaida = new ArquivosTXT.cTabelaTXT(ArquivosTXT.ModelosTXT.layout);
            Arquivo.Formatodata = ArquivosTXT.FormatoData.yyyyMM;
            Reg30 reg30 = new Reg30(Arquivo, null);
            using (OpenFileDialog OF = new OpenFileDialog())
            {
                //OF.FileName = @"c:\FontesVS\neon\Documentacao\FGTS\sefip.re";
                OF.FileName = @"c:\FontesVS\neon\Documentacao\FGTS\sefipteste.re";
                long CNPJ = 0;
                if (OF.ShowDialog() == DialogResult.OK)
                {
                    Arquivo.NomeArquivo = OF.FileName;
                    ArquivoSaida.IniciaGravacao(Arquivo.NomeArquivo + "A");
                    Arquivo.RegistraMapa("Tipo", 1, 2, "BASE", 0);
                    ArquivosTXT.RegistroBase RegBase = new ArquivosTXT.RegistroBase(Arquivo);
                    RegBase.MapaReg = Arquivo.Mapas("BASE");
                    for (string Lido = Arquivo.ProximaLinha(); Lido != null; Lido = Arquivo.ProximaLinha())
                    {
                        //1126447000174
                        
                        RegBase.Decodificalinha(Lido);
                        TiposRegistro Tipo;
                        try
                        {
                            Tipo = (TiposRegistro)RegBase.Valores["Tipo"];
                        }
                        catch
                        {
                            Tipo = TiposRegistro.Invalido;
                        }
                        switch (Tipo)
                        {
                            case TiposRegistro.HeaderArquivo:
                                Reg00 reg00 = new Reg00(Arquivo, Lido);
                                memoEdit1.Text += string.Format("Identificado ({0}) -> Competencia: {1}\r\n", Tipo, reg00.competencia);
                                break;
                            case TiposRegistro.RegistroTrabalhador:
                                //Reg30 reg30 = new Reg30(Arquivo, Lido);
                                reg30.Decodificalinha(Lido);
                                memoEdit1.Text += string.Format("Identificado ({0})\r\n", Tipo);
                                memoEdit1.Text += string.Format("PIS:       {0,30}\r\n", reg30.PIS);
                                memoEdit1.Text += string.Format("Categoria: {0,30}\r\n", reg30.Categoria);
                                break;
                            case TiposRegistro.HeaderEmpresa:
                                if (CNPJ == 1126447000174)
                                {
                                    Reg30 reg30s = new Reg30(ArquivoSaida,null);
                                    reg30s.PIS = 12469516686;
                                    reg30s.Categoria = 13;
                                    reg30s.Nome = "Nome Teste";
                                    reg30s.Valor = 123.45M;
                                    reg30s.CNPJ = CNPJ; 
                                    reg30s.GravaLinha();
                                };
                                Reg10 reg10 = new Reg10(Arquivo, Lido);
                                memoEdit1.Text += string.Format("Identificado ({0})\r\n", Tipo);
                                memoEdit1.Text += string.Format("CNPJ: {0,30}\r\n", reg10.CNPJ);
                                CNPJ = reg10.CNPJ;
                                break;
                            case TiposRegistro.Invalido:
                                memoEdit1.Text += string.Format("?????????? => {0}\r\n", RegBase.Valores["Tipo"]);
                                break;
                        }
                        memoEdit1.Text += string.Format("{0,50}    ", "");
                        for (int i = 0; i < 3; i++)
                            memoEdit1.Text += string.Format("{1,-10}{2,-10}{3,-10}{4,-10}{5,-10}{6,-10}{7,-10}{8,-10}{9,-10}{10,-10}", "", 0, 1, 2, 3, 4, 5, 6, 7, 8, 9);
                        memoEdit1.Text += "\r\n";
                        memoEdit1.Text += string.Format("{0,50}    {1}{1}{1}{1}{1}{1}{1}{1}{1}{1}{1}{1}{1}{1}{1}{1}{1}{1}{1}{1}{1}{1}{1}{1}{1}{1}{1}{1}{1}{1}\r\n", "", "1234567890");
                        memoEdit1.Text += string.Format("{0,50} -> {1}\r\n", "", Lido);
                        ArquivoSaida.GravaLinha(Lido);
                    }
                    ArquivoSaida.TerminaGravacao();
                }
            }
        }

        private void simpleButton2_Click(object sender, EventArgs e)
        {
            //CompontesBasicos.Importador.ComponeteBaseImportador cimp = new CompontesBasicos.Importador.ComponeteBaseImportador();
        }
    }

    class Reg00 : ArquivosTXT.RegistroBase
    {
        public Reg00(ArquivosTXT.cTabelaTXT _cTabTXT,string Lido):base(_cTabTXT,Lido)
        {            
        }

        public DateTime competencia;

        protected override void RegistraMapa()
        {
            base.RegistraMapa();
            RegistraFortementeTipado("competencia", 292, 6);

            //if (cTabTXT.Mapas(GetType().Name).Count == 0)
            //{
            //    cTabTXT.RegistraMapa("Competencia", 292, 6, GetType().Name, DateTime.MinValue);
            //};
            //base.RegistraMapa();
        }

        //protected override void DecodificarValores()
        //{
            //competencia = (DateTime)Valores["Competencia"];
        //}
    }

    class Reg10 : ArquivosTXT.RegistroBase
    {
        public Reg10(ArquivosTXT.cTabelaTXT _cTabTXT, string Lido)
            : base(_cTabTXT, Lido)
        {
        }

        public long CNPJ;

        protected override void RegistraMapa()
        {            
            base.RegistraMapa();
            RegistraFortementeTipado("CNPJ", 4, 14);
        }

        protected override void DecodificarValores()
        {
            //competencia = (DateTime)Valores["Competencia"];
        }
    }

    class Reg30 : ArquivosTXT.RegistroBase
    {
        public Reg30(ArquivosTXT.cTabelaTXT _cTabTXT, string Lido)
            : base(_cTabTXT, Lido)
        {
            
        }

        
        public Int64 PIS;
        public int Categoria;
        //public int TipoCNPJ = 1;
        public long CNPJ;
        public string Nome;
        public decimal Valor;

        protected override void RegistraMapa()
        {
            base.RegistraMapa();
            //if()
            //if (cTabTXT.Mapas(GetType().Name).Count == 0)
            RegistraMapa("TipoRegistro", 1, 2, 30);
            RegistraMapa("TipoCNPJ", 3, 1, 1);
            //RegistraFortementeTipado("TipoCNPJ", 3, 1);
            RegistraFortementeTipado("CNPJ",4,14);
            RegistraMapa("Z", 4, 29,0);
            RegistraFortementeTipado("PIS", 33, 11);
            RegistraFortementeTipado("Categoria", 52, 2);
            RegistraFortementeTipado("Nome", 54, 70);
            //RegistraMapa("Z1", 124, 23, 0);
            RegistraMapa("CBO", 163, 5, "05101");
            RegistraFortementeTipado("Valor", 168, 15);
            RegistraMapa("Valor13", 183, 15,0.0M);
            RegistraMapa("Z2", 202, 30, 0);
            RegistraMapa("Z3", 232, 30, 0);
            RegistraMapa("*", 360, 1, "*");
            //}
            
        }

       
    }
     
   
}