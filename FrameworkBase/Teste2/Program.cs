using System;
using System.Collections.Generic;
using System.Windows.Forms;
using System.Threading;

namespace Teste2
{
    static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main()
        {
            VirDB.Bancovirtual.BancoVirtual.Popular("Lap", @"Data Source=LAPVIRTUAL\SQLEXPRESS;Initial Catalog=NEONReal;User ID=sa;Password=venus");
            //VirDB.Bancovirtual.BancoVirtual.Popular("Lap", @"Data Source=lapvirtual\SQLEXPRESS;Initial Catalog=Neon;Persist Security Info=True;User ID=sa;Password=venus");
            VirDB.Bancovirtual.BancoVirtual.PopularAcces("SBC LOCAL", @"Provider=Microsoft.Jet.OLEDB.4.0;Data Source=C:\clientes\neon\sbc\dados.mdb;Persist Security Info=True;Password=96333018;User ID=NEON;Jet OLEDB:System database=C:\clientes\neon\sbc\neon.mda;Jet OLEDB:Database Password=96333018");
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            VirException.VirException TrataErro = new VirException.VirException("smtp.virweb.com.br", "virtual@virweb.com.br", "venus", "virtual@virweb.com.br");
            Application.ThreadException += TrataErro.OnThreadException;
            //impostos
            //Application.Run(new Form4());
            //Application.Run(new Form2());//impressos, etiqueta e codbar
            Application.Run(new Form3()); //menu
            //Application.Run(new FTesteBotao());
            //Application.Run(new Form5try());
            //Application.Run(new FTesteComTXT());// teste com TXT
            //Application.Run(new Form1()); //digital
        }
    }
}