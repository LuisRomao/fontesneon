namespace Teste2
{
    partial class XtraReport1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            DevExpress.XtraPrinting.BarCode.Interleaved2of5Generator interleaved2of5Generator13 = new DevExpress.XtraPrinting.BarCode.Interleaved2of5Generator();
            DevExpress.XtraPrinting.BarCode.Interleaved2of5Generator interleaved2of5Generator14 = new DevExpress.XtraPrinting.BarCode.Interleaved2of5Generator();
            DevExpress.XtraPrinting.BarCode.Interleaved2of5Generator interleaved2of5Generator15 = new DevExpress.XtraPrinting.BarCode.Interleaved2of5Generator();
            DevExpress.XtraPrinting.BarCode.Interleaved2of5Generator interleaved2of5Generator12 = new DevExpress.XtraPrinting.BarCode.Interleaved2of5Generator();
            DevExpress.XtraPrinting.BarCode.Interleaved2of5Generator interleaved2of5Generator11 = new DevExpress.XtraPrinting.BarCode.Interleaved2of5Generator();
            DevExpress.XtraPrinting.BarCode.Interleaved2of5Generator interleaved2of5Generator10 = new DevExpress.XtraPrinting.BarCode.Interleaved2of5Generator();
            DevExpress.XtraPrinting.BarCode.Interleaved2of5Generator interleaved2of5Generator9 = new DevExpress.XtraPrinting.BarCode.Interleaved2of5Generator();
            DevExpress.XtraPrinting.BarCode.Interleaved2of5Generator interleaved2of5Generator8 = new DevExpress.XtraPrinting.BarCode.Interleaved2of5Generator();
            DevExpress.XtraPrinting.BarCode.Interleaved2of5Generator interleaved2of5Generator7 = new DevExpress.XtraPrinting.BarCode.Interleaved2of5Generator();
            DevExpress.XtraPrinting.BarCode.Interleaved2of5Generator interleaved2of5Generator6 = new DevExpress.XtraPrinting.BarCode.Interleaved2of5Generator();
            DevExpress.XtraPrinting.BarCode.Interleaved2of5Generator interleaved2of5Generator5 = new DevExpress.XtraPrinting.BarCode.Interleaved2of5Generator();
            DevExpress.XtraPrinting.BarCode.Interleaved2of5Generator interleaved2of5Generator4 = new DevExpress.XtraPrinting.BarCode.Interleaved2of5Generator();
            DevExpress.XtraPrinting.BarCode.Interleaved2of5Generator interleaved2of5Generator3 = new DevExpress.XtraPrinting.BarCode.Interleaved2of5Generator();
            DevExpress.XtraPrinting.BarCode.Interleaved2of5Generator interleaved2of5Generator2 = new DevExpress.XtraPrinting.BarCode.Interleaved2of5Generator();
            DevExpress.XtraPrinting.BarCode.Interleaved2of5Generator interleaved2of5Generator1 = new DevExpress.XtraPrinting.BarCode.Interleaved2of5Generator();
            this.Detail = new DevExpress.XtraReports.UI.DetailBand();
            this.xrBarCode8 = new DevExpress.XtraReports.UI.XRBarCode();
            this.xrBarCode7 = new DevExpress.XtraReports.UI.XRBarCode();
            this.xrBarCode1 = new DevExpress.XtraReports.UI.XRBarCode();
            this.PageHeader = new DevExpress.XtraReports.UI.PageHeaderBand();
            this.PageFooter = new DevExpress.XtraReports.UI.PageFooterBand();
            this.xrBarCode3 = new DevExpress.XtraReports.UI.XRBarCode();
            this.xrBarCode4 = new DevExpress.XtraReports.UI.XRBarCode();
            this.xrBarCode2 = new DevExpress.XtraReports.UI.XRBarCode();
            this.xrBarCode5 = new DevExpress.XtraReports.UI.XRBarCode();
            this.xrBarCode6 = new DevExpress.XtraReports.UI.XRBarCode();
            this.xrBarCode9 = new DevExpress.XtraReports.UI.XRBarCode();
            this.xrBarCode10 = new DevExpress.XtraReports.UI.XRBarCode();
            this.xrBarCode11 = new DevExpress.XtraReports.UI.XRBarCode();
            this.xrBarCode12 = new DevExpress.XtraReports.UI.XRBarCode();
            this.xrBarCode13 = new DevExpress.XtraReports.UI.XRBarCode();
            this.xrBarCode14 = new DevExpress.XtraReports.UI.XRBarCode();
            this.xrBarCode15 = new DevExpress.XtraReports.UI.XRBarCode();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            // 
            // Detail
            // 
            this.Detail.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrBarCode15,
            this.xrBarCode14,
            this.xrBarCode13,
            this.xrBarCode12,
            this.xrBarCode11,
            this.xrBarCode10,
            this.xrBarCode9,
            this.xrBarCode6,
            this.xrBarCode5,
            this.xrBarCode2,
            this.xrBarCode4,
            this.xrBarCode3,
            this.xrBarCode8,
            this.xrBarCode7,
            this.xrBarCode1});
            this.Detail.Dpi = 254F;
            this.Detail.Height = 1270;
            this.Detail.Name = "Detail";
            this.Detail.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            this.Detail.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrBarCode8
            // 
            this.xrBarCode8.Dpi = 254F;
            this.xrBarCode8.Location = new System.Drawing.Point(678, 528);
            this.xrBarCode8.Module = 3.81F;
            this.xrBarCode8.Name = "xrBarCode8";
            this.xrBarCode8.PaddingInfo = new DevExpress.XtraPrinting.PaddingInfo(25, 25, 0, 0, 254F);
            this.xrBarCode8.Size = new System.Drawing.Size(741, 148);
            interleaved2of5Generator13.WideNarrowRatio = 3F;
            this.xrBarCode8.Symbology = interleaved2of5Generator13;
            this.xrBarCode8.Text = "012302";
            // 
            // xrBarCode7
            // 
            this.xrBarCode7.Dpi = 254F;
            this.xrBarCode7.Location = new System.Drawing.Point(85, 423);
            this.xrBarCode7.Module = 2.54F;
            this.xrBarCode7.Name = "xrBarCode7";
            this.xrBarCode7.PaddingInfo = new DevExpress.XtraPrinting.PaddingInfo(25, 25, 0, 0, 254F);
            this.xrBarCode7.Size = new System.Drawing.Size(444, 147);
            interleaved2of5Generator14.WideNarrowRatio = 3F;
            this.xrBarCode7.Symbology = interleaved2of5Generator14;
            this.xrBarCode7.Text = "1010000067";
            // 
            // xrBarCode1
            // 
            this.xrBarCode1.Dpi = 254F;
            this.xrBarCode1.Location = new System.Drawing.Point(85, 233);
            this.xrBarCode1.Module = 1.2F;
            this.xrBarCode1.Name = "xrBarCode1";
            this.xrBarCode1.PaddingInfo = new DevExpress.XtraPrinting.PaddingInfo(25, 25, 0, 0, 254F);
            this.xrBarCode1.Size = new System.Drawing.Size(488, 170);
            interleaved2of5Generator15.WideNarrowRatio = 3F;
            this.xrBarCode1.Symbology = interleaved2of5Generator15;
            this.xrBarCode1.Text = "1010000067";
            // 
            // PageHeader
            // 
            this.PageHeader.Dpi = 254F;
            this.PageHeader.Height = 76;
            this.PageHeader.Name = "PageHeader";
            this.PageHeader.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            this.PageHeader.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // PageFooter
            // 
            this.PageFooter.Dpi = 254F;
            this.PageFooter.Height = 76;
            this.PageFooter.Name = "PageFooter";
            this.PageFooter.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            this.PageFooter.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrBarCode3
            // 
            this.xrBarCode3.Dpi = 254F;
            this.xrBarCode3.Location = new System.Drawing.Point(678, 361);
            this.xrBarCode3.Module = 3.048F;
            this.xrBarCode3.Name = "xrBarCode3";
            this.xrBarCode3.PaddingInfo = new DevExpress.XtraPrinting.PaddingInfo(25, 25, 0, 0, 254F);
            this.xrBarCode3.Size = new System.Drawing.Size(742, 147);
            interleaved2of5Generator12.WideNarrowRatio = 3F;
            this.xrBarCode3.Symbology = interleaved2of5Generator12;
            this.xrBarCode3.Text = "012302";
            // 
            // xrBarCode4
            // 
            this.xrBarCode4.Dpi = 254F;
            this.xrBarCode4.Location = new System.Drawing.Point(678, 190);
            this.xrBarCode4.Module = 2.032F;
            this.xrBarCode4.Name = "xrBarCode4";
            this.xrBarCode4.PaddingInfo = new DevExpress.XtraPrinting.PaddingInfo(25, 25, 0, 0, 254F);
            this.xrBarCode4.Size = new System.Drawing.Size(742, 147);
            interleaved2of5Generator11.WideNarrowRatio = 3F;
            this.xrBarCode4.Symbology = interleaved2of5Generator11;
            this.xrBarCode4.Text = "012302";
            // 
            // xrBarCode2
            // 
            this.xrBarCode2.Dpi = 254F;
            this.xrBarCode2.Location = new System.Drawing.Point(678, 20);
            this.xrBarCode2.Module = 1.27F;
            this.xrBarCode2.Name = "xrBarCode2";
            this.xrBarCode2.PaddingInfo = new DevExpress.XtraPrinting.PaddingInfo(25, 25, 0, 0, 254F);
            this.xrBarCode2.Size = new System.Drawing.Size(742, 147);
            interleaved2of5Generator10.WideNarrowRatio = 3F;
            this.xrBarCode2.Symbology = interleaved2of5Generator10;
            this.xrBarCode2.Text = "012302";
            // 
            // xrBarCode5
            // 
            this.xrBarCode5.Dpi = 254F;
            this.xrBarCode5.Location = new System.Drawing.Point(678, 698);
            this.xrBarCode5.Module = 4.572F;
            this.xrBarCode5.Name = "xrBarCode5";
            this.xrBarCode5.PaddingInfo = new DevExpress.XtraPrinting.PaddingInfo(25, 25, 0, 0, 254F);
            this.xrBarCode5.Size = new System.Drawing.Size(742, 147);
            interleaved2of5Generator9.WideNarrowRatio = 3F;
            this.xrBarCode5.Symbology = interleaved2of5Generator9;
            this.xrBarCode5.Text = "012302";
            // 
            // xrBarCode6
            // 
            this.xrBarCode6.Dpi = 254F;
            this.xrBarCode6.Location = new System.Drawing.Point(64, 910);
            this.xrBarCode6.Module = 2.54F;
            this.xrBarCode6.Name = "xrBarCode6";
            this.xrBarCode6.PaddingInfo = new DevExpress.XtraPrinting.PaddingInfo(25, 25, 0, 0, 254F);
            this.xrBarCode6.Size = new System.Drawing.Size(444, 147);
            interleaved2of5Generator8.WideNarrowRatio = 3F;
            this.xrBarCode6.Symbology = interleaved2of5Generator8;
            this.xrBarCode6.Text = "5";
            // 
            // xrBarCode9
            // 
            this.xrBarCode9.Dpi = 254F;
            this.xrBarCode9.Location = new System.Drawing.Point(529, 910);
            this.xrBarCode9.Module = 2.54F;
            this.xrBarCode9.Name = "xrBarCode9";
            this.xrBarCode9.PaddingInfo = new DevExpress.XtraPrinting.PaddingInfo(25, 25, 0, 0, 254F);
            this.xrBarCode9.Size = new System.Drawing.Size(444, 147);
            interleaved2of5Generator7.WideNarrowRatio = 3F;
            this.xrBarCode9.Symbology = interleaved2of5Generator7;
            this.xrBarCode9.Text = "55";
            // 
            // xrBarCode10
            // 
            this.xrBarCode10.Dpi = 254F;
            this.xrBarCode10.Location = new System.Drawing.Point(1016, 910);
            this.xrBarCode10.Module = 2.54F;
            this.xrBarCode10.Name = "xrBarCode10";
            this.xrBarCode10.PaddingInfo = new DevExpress.XtraPrinting.PaddingInfo(25, 25, 0, 0, 254F);
            this.xrBarCode10.Size = new System.Drawing.Size(444, 147);
            interleaved2of5Generator6.WideNarrowRatio = 3F;
            this.xrBarCode10.Symbology = interleaved2of5Generator6;
            this.xrBarCode10.Text = "555";
            // 
            // xrBarCode11
            // 
            this.xrBarCode11.Dpi = 254F;
            this.xrBarCode11.Location = new System.Drawing.Point(64, 1101);
            this.xrBarCode11.Module = 2.54F;
            this.xrBarCode11.Name = "xrBarCode11";
            this.xrBarCode11.PaddingInfo = new DevExpress.XtraPrinting.PaddingInfo(25, 25, 0, 0, 254F);
            this.xrBarCode11.Size = new System.Drawing.Size(444, 147);
            interleaved2of5Generator5.WideNarrowRatio = 3F;
            this.xrBarCode11.Symbology = interleaved2of5Generator5;
            this.xrBarCode11.Text = "5555";
            // 
            // xrBarCode12
            // 
            this.xrBarCode12.Dpi = 254F;
            this.xrBarCode12.Location = new System.Drawing.Point(529, 1101);
            this.xrBarCode12.Module = 2.54F;
            this.xrBarCode12.Name = "xrBarCode12";
            this.xrBarCode12.PaddingInfo = new DevExpress.XtraPrinting.PaddingInfo(25, 25, 0, 0, 254F);
            this.xrBarCode12.Size = new System.Drawing.Size(444, 147);
            interleaved2of5Generator4.WideNarrowRatio = 3F;
            this.xrBarCode12.Symbology = interleaved2of5Generator4;
            this.xrBarCode12.Text = "55555";
            // 
            // xrBarCode13
            // 
            this.xrBarCode13.Dpi = 254F;
            this.xrBarCode13.Location = new System.Drawing.Point(1016, 1080);
            this.xrBarCode13.Module = 2.54F;
            this.xrBarCode13.Name = "xrBarCode13";
            this.xrBarCode13.PaddingInfo = new DevExpress.XtraPrinting.PaddingInfo(25, 25, 0, 0, 254F);
            this.xrBarCode13.Size = new System.Drawing.Size(444, 147);
            interleaved2of5Generator3.WideNarrowRatio = 3F;
            this.xrBarCode13.Symbology = interleaved2of5Generator3;
            this.xrBarCode13.Text = "555555";
            // 
            // xrBarCode14
            // 
            this.xrBarCode14.Dpi = 254F;
            this.xrBarCode14.Location = new System.Drawing.Point(85, 42);
            this.xrBarCode14.Module = 1.1F;
            this.xrBarCode14.Name = "xrBarCode14";
            this.xrBarCode14.PaddingInfo = new DevExpress.XtraPrinting.PaddingInfo(25, 25, 0, 0, 254F);
            this.xrBarCode14.Size = new System.Drawing.Size(488, 170);
            interleaved2of5Generator2.WideNarrowRatio = 3F;
            this.xrBarCode14.Symbology = interleaved2of5Generator2;
            this.xrBarCode14.Text = "1010000067";
            // 
            // xrBarCode15
            // 
            this.xrBarCode15.Dpi = 254F;
            this.xrBarCode15.Location = new System.Drawing.Point(64, 720);
            this.xrBarCode15.Module = 2.54F;
            this.xrBarCode15.Name = "xrBarCode15";
            this.xrBarCode15.PaddingInfo = new DevExpress.XtraPrinting.PaddingInfo(25, 25, 0, 0, 254F);
            this.xrBarCode15.Size = new System.Drawing.Size(444, 147);
            interleaved2of5Generator1.WideNarrowRatio = 3F;
            this.xrBarCode15.Symbology = interleaved2of5Generator1;
            // 
            // XtraReport1
            // 
            this.Bands.AddRange(new DevExpress.XtraReports.UI.Band[] {
            this.Detail,
            this.PageHeader,
            this.PageFooter});
            this.Dpi = 254F;
            this.Margins = new System.Drawing.Printing.Margins(254, 254, 254, 254);
            this.PageHeight = 2969;
            this.PageWidth = 2101;
            this.PaperKind = System.Drawing.Printing.PaperKind.A4;
            this.ReportUnit = DevExpress.XtraReports.UI.ReportUnit.TenthsOfAMillimeter;
            this.Version = "8.2";
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();

        }

        #endregion

        private DevExpress.XtraReports.UI.DetailBand Detail;
        private DevExpress.XtraReports.UI.PageHeaderBand PageHeader;
        private DevExpress.XtraReports.UI.PageFooterBand PageFooter;
        private DevExpress.XtraReports.UI.XRBarCode xrBarCode1;
        private DevExpress.XtraReports.UI.XRBarCode xrBarCode8;
        private DevExpress.XtraReports.UI.XRBarCode xrBarCode7;
        private DevExpress.XtraReports.UI.XRBarCode xrBarCode5;
        private DevExpress.XtraReports.UI.XRBarCode xrBarCode2;
        private DevExpress.XtraReports.UI.XRBarCode xrBarCode4;
        private DevExpress.XtraReports.UI.XRBarCode xrBarCode3;
        private DevExpress.XtraReports.UI.XRBarCode xrBarCode13;
        private DevExpress.XtraReports.UI.XRBarCode xrBarCode12;
        private DevExpress.XtraReports.UI.XRBarCode xrBarCode11;
        private DevExpress.XtraReports.UI.XRBarCode xrBarCode10;
        private DevExpress.XtraReports.UI.XRBarCode xrBarCode9;
        private DevExpress.XtraReports.UI.XRBarCode xrBarCode6;
        private DevExpress.XtraReports.UI.XRBarCode xrBarCode15;
        private DevExpress.XtraReports.UI.XRBarCode xrBarCode14;
    }
}
