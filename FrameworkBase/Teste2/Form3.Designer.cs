namespace Teste2
{
    partial class Form3
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Form3));
            CompontesBasicos.ModuleInfo moduleInfo2 = new CompontesBasicos.ModuleInfo();
            this.simpleButton1 = new DevExpress.XtraEditors.SimpleButton();
            this.navBarGroup1 = new DevExpress.XtraNavBar.NavBarGroup();
            this.Importa = new DevExpress.XtraNavBar.NavBarItem();
            this.navBarItem4 = new DevExpress.XtraNavBar.NavBarItem();
            this.navBarItem3 = new DevExpress.XtraNavBar.NavBarItem();
            this.navBarItem5 = new DevExpress.XtraNavBar.NavBarItem();
            this.navBarItem6 = new DevExpress.XtraNavBar.NavBarItem();
            this.navBarItem8 = new DevExpress.XtraNavBar.NavBarItem();
            this.navBarItem9 = new DevExpress.XtraNavBar.NavBarItem();
            this.navBarItem1 = new DevExpress.XtraNavBar.NavBarItem();
            this.navBarItem2 = new DevExpress.XtraNavBar.NavBarItem();
            this.navBarItem7 = new DevExpress.XtraNavBar.NavBarItem();
            this.notifyIcon1 = new System.Windows.Forms.NotifyIcon(this.components);
            ((System.ComponentModel.ISupportInitialize)(this.PictureEdit_F.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.NavBarControl_F)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.BarAndDockingController_F)).BeginInit();
            this.DockPanel_FContainer.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.TabControl_F)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.BarManager_F)).BeginInit();
            this.DockPanel_F.SuspendLayout();
            this.SuspendLayout();
            // 
            // PictureEdit_F
            // 
            // 
            // NavBarControl_F
            // 
            this.NavBarControl_F.ActiveGroup = this.navBarGroup1;
            this.NavBarControl_F.Groups.AddRange(new DevExpress.XtraNavBar.NavBarGroup[] {
            this.navBarGroup1});
            this.NavBarControl_F.Items.AddRange(new DevExpress.XtraNavBar.NavBarItem[] {
            this.navBarItem1,
            this.navBarItem2,
            this.navBarItem3,
            this.navBarItem4,
            this.Importa,
            this.navBarItem5,
            this.navBarItem6,
            this.navBarItem7,
            this.navBarItem8,
            this.navBarItem9});
            this.NavBarControl_F.LookAndFeel.SkinName = "Money Twins";
            this.NavBarControl_F.LookAndFeel.Style = DevExpress.LookAndFeel.LookAndFeelStyle.Flat;
            this.NavBarControl_F.LookAndFeel.UseDefaultLookAndFeel = false;
            this.NavBarControl_F.LookAndFeel.UseWindowsXPTheme = true;
            this.NavBarControl_F.OptionsNavPane.ExpandedWidth = 181;
            this.NavBarControl_F.PaintStyleKind = DevExpress.XtraNavBar.NavBarViewKind.ExplorerBar;
            this.NavBarControl_F.SkinExplorerBarViewScrollStyle = DevExpress.XtraNavBar.SkinExplorerBarViewScrollStyle.ScrollBar;
            this.NavBarControl_F.ToolTipController = null;
            this.NavBarControl_F.View = new DevExpress.XtraNavBar.ViewInfo.StandardSkinExplorerBarViewInfoRegistrator("Caramel");
            // 
            // BarAndDockingController_F
            // 
            this.BarAndDockingController_F.LookAndFeel.SkinName = "Black";
            this.BarAndDockingController_F.LookAndFeel.Style = DevExpress.LookAndFeel.LookAndFeelStyle.Flat;
            this.BarAndDockingController_F.LookAndFeel.UseDefaultLookAndFeel = false;
            this.BarAndDockingController_F.LookAndFeel.UseWindowsXPTheme = true;
            this.BarAndDockingController_F.PaintStyleName = "OfficeXP";
            // 
            // DefaultLookAndFeel_F
            // 
            this.DefaultLookAndFeel_F.LookAndFeel.SkinName = "The Asphalt World";
            this.DefaultLookAndFeel_F.LookAndFeel.Style = DevExpress.LookAndFeel.LookAndFeelStyle.Flat;
            this.DefaultLookAndFeel_F.LookAndFeel.UseWindowsXPTheme = true;
            // 
            // DefaultToolTipController_F
            // 
            // 
            // 
            // 
            this.DefaultToolTipController_F.DefaultController.Rounded = true;
            // 
            // barStatus_F
            // 
            this.barStatus_F.OptionsBar.AllowQuickCustomization = false;
            this.barStatus_F.OptionsBar.DisableClose = true;
            this.barStatus_F.OptionsBar.DisableCustomization = true;
            this.barStatus_F.OptionsBar.DrawDragBorder = false;
            this.barStatus_F.OptionsBar.DrawSizeGrip = true;
            this.barStatus_F.OptionsBar.UseWholeRow = true;
            // 
            // DockPanel_FContainer
            // 
            this.DefaultToolTipController_F.SetAllowHtmlText(this.DockPanel_FContainer, DevExpress.Utils.DefaultBoolean.Default);
            // 
            // TabControl_F
            // 
            this.TabControl_F.ClosePageButtonShowMode = DevExpress.XtraTab.ClosePageButtonShowMode.InActiveTabPageHeader;
            // 
            // DockPanel_F
            // 
            this.DockPanel_F.Appearance.BackColor = System.Drawing.Color.White;
            this.DockPanel_F.Appearance.Options.UseBackColor = true;
            this.DockPanel_F.Options.AllowDockFill = false;
            this.DockPanel_F.Options.ShowCloseButton = false;
            this.DockPanel_F.Options.ShowMaximizeButton = false;
            // 
            // simpleButton1
            // 
            this.simpleButton1.Location = new System.Drawing.Point(123, 127);
            this.simpleButton1.Name = "simpleButton1";
            this.simpleButton1.Size = new System.Drawing.Size(75, 23);
            this.simpleButton1.TabIndex = 0;
            this.simpleButton1.Text = "simpleButton1";
            this.simpleButton1.Click += new System.EventHandler(this.simpleButton1_Click);
            // 
            // navBarGroup1
            // 
            this.navBarGroup1.Caption = "CEP";
            this.navBarGroup1.Expanded = true;
            this.navBarGroup1.GroupCaptionUseImage = DevExpress.XtraNavBar.NavBarImage.Large;
            this.navBarGroup1.GroupStyle = DevExpress.XtraNavBar.NavBarGroupStyle.LargeIconsText;
            this.navBarGroup1.ItemLinks.AddRange(new DevExpress.XtraNavBar.NavBarItemLink[] {
            new DevExpress.XtraNavBar.NavBarItemLink(this.Importa),
            new DevExpress.XtraNavBar.NavBarItemLink(this.navBarItem4),
            new DevExpress.XtraNavBar.NavBarItemLink(this.navBarItem3),
            new DevExpress.XtraNavBar.NavBarItemLink(this.navBarItem5),
            new DevExpress.XtraNavBar.NavBarItemLink(this.navBarItem6),
            new DevExpress.XtraNavBar.NavBarItemLink(this.navBarItem8),
            new DevExpress.XtraNavBar.NavBarItemLink(this.navBarItem9),
            new DevExpress.XtraNavBar.NavBarItemLink(this.navBarItem1)});
            this.navBarGroup1.Name = "navBarGroup1";
            this.navBarGroup1.ItemChanged += new System.EventHandler(this.navBarGroup1_ItemChanged);
            // 
            // Importa
            // 
            this.Importa.Caption = "Importa";
            this.Importa.Name = "Importa";
            this.Importa.LinkClicked += new DevExpress.XtraNavBar.NavBarLinkEventHandler(this.Importa_LinkClicked);
            // 
            // navBarItem4
            // 
            this.navBarItem4.Caption = "CEP";
            this.navBarItem4.Name = "navBarItem4";
            this.navBarItem4.LinkClicked += new DevExpress.XtraNavBar.NavBarLinkEventHandler(this.navBarItem4_LinkClicked);
            // 
            // navBarItem3
            // 
            this.navBarItem3.Caption = "ERRO";
            this.navBarItem3.Name = "navBarItem3";
            this.navBarItem3.LinkClicked += new DevExpress.XtraNavBar.NavBarLinkEventHandler(this.navBarItem3_LinkClicked);
            // 
            // navBarItem5
            // 
            this.navBarItem5.Caption = "DIRF";
            this.navBarItem5.Name = "navBarItem5";
            this.navBarItem5.LinkClicked += new DevExpress.XtraNavBar.NavBarLinkEventHandler(this.navBarItem5_LinkClicked);
            // 
            // navBarItem6
            // 
            this.navBarItem6.Caption = "CNAB";
            this.navBarItem6.Name = "navBarItem6";
            this.navBarItem6.LinkClicked += new DevExpress.XtraNavBar.NavBarLinkEventHandler(this.navBarItem6_LinkClicked);
            // 
            // navBarItem8
            // 
            this.navBarItem8.Caption = "nfe";
            this.navBarItem8.Name = "navBarItem8";
            this.navBarItem8.LinkClicked += new DevExpress.XtraNavBar.NavBarLinkEventHandler(this.navBarItem8_LinkClicked);
            // 
            // navBarItem9
            // 
            this.navBarItem9.Caption = "NFS-e";
            this.navBarItem9.Name = "navBarItem9";
            this.navBarItem9.LinkClicked += new DevExpress.XtraNavBar.NavBarLinkEventHandler(this.navBarItem9_LinkClicked);
            // 
            // navBarItem1
            // 
            this.navBarItem1.Caption = "navBarItem1";
            this.navBarItem1.LargeImage = global::Teste2.Properties.Resources.BtnExportar_F_Glyph;
            this.navBarItem1.Name = "navBarItem1";
            this.navBarItem1.LinkClicked += new DevExpress.XtraNavBar.NavBarLinkEventHandler(this.navBarItem1_LinkClicked);
            // 
            // navBarItem2
            // 
            this.navBarItem2.Caption = "navBarItem2";
            this.navBarItem2.Name = "navBarItem2";
            // 
            // navBarItem7
            // 
            this.navBarItem7.Caption = "navBarItem7";
            this.navBarItem7.Name = "navBarItem7";
            // 
            // notifyIcon1
            // 
            this.notifyIcon1.Icon = ((System.Drawing.Icon)(resources.GetObject("notifyIcon1.Icon")));
            this.notifyIcon1.Text = "notifyIcon1";
            this.notifyIcon1.Visible = true;
            // 
            // Form3
            // 
            this.DefaultToolTipController_F.SetAllowHtmlText(this, DevExpress.Utils.DefaultBoolean.Default);
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.ClientSize = new System.Drawing.Size(782, 556);
            moduleInfo2.Grupo = 0;
            moduleInfo2.ImagemG = null;
            moduleInfo2.ImagemP = null;
            moduleInfo2.ModuleType = null;
            moduleInfo2.ModuleTypestr = "DocBacarios.cConfigCheque";
            moduleInfo2.Name = "cheque";
            this.ModuleInfoCollection.AddRange(new CompontesBasicos.ModuleInfo[] {
            moduleInfo2});
            this.Name = "Form3";
            this.OcultarTabs = false;
            ((System.ComponentModel.ISupportInitialize)(this.PictureEdit_F.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.NavBarControl_F)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.BarAndDockingController_F)).EndInit();
            this.DockPanel_FContainer.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.TabControl_F)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.BarManager_F)).EndInit();
            this.DockPanel_F.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraEditors.SimpleButton simpleButton1;
        private DevExpress.XtraNavBar.NavBarGroup navBarGroup1;
        private DevExpress.XtraNavBar.NavBarItem navBarItem1;
        private DevExpress.XtraNavBar.NavBarItem navBarItem2;
        private DevExpress.XtraNavBar.NavBarItem navBarItem3;
        private DevExpress.XtraNavBar.NavBarItem navBarItem4;
        private DevExpress.XtraNavBar.NavBarItem Importa;
        private DevExpress.XtraNavBar.NavBarItem navBarItem5;
        private DevExpress.XtraNavBar.NavBarItem navBarItem6;
        private DevExpress.XtraNavBar.NavBarItem navBarItem7;
        private DevExpress.XtraNavBar.NavBarItem navBarItem8;
        private DevExpress.XtraNavBar.NavBarItem navBarItem9;
        private System.Windows.Forms.NotifyIcon notifyIcon1;
    }
}
