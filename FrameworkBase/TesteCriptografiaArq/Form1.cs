﻿/*
MR - 04/12/2014 10:00 -           - Ajuste no erro do looping de arquivos (Alterações indicadas por *** MRC - INICIO - TRIBUTOS (04/12/2014 10:00) ***)
*/

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.IO;

namespace TesteCriptografiaArq
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void cmdCriptografa_Click(object sender, EventArgs e)
        {
            textBox1.Text = "";
            if (openFileDialog1.ShowDialog() == System.Windows.Forms.DialogResult.OK)
            {
                //*** MRC - INICIO - TRIBUTOS (04/12/2014 10:00) ***
                foreach (string arquivo in openFileDialog1.FileNames)
                {
                    string retorno = arquivo.Replace(Path.GetFileName(arquivo), @"CRIPTO_" + Path.GetFileName(arquivo));
                    if (CriptografiaArq.Bradesco.CriptografaArquivo(txtArquivoCripto.Text, txtSenhaCripto.Text, arquivo, ref retorno) == 1)
                        textBox1.Text += "\r\n\r\nSUCESSO\r\n" + retorno;
                    else
                        textBox1.Text += "\r\n\r\nERRO\r\n" + retorno; 
                }
                //*** MRC - TERMINO - TRIBUTOS (04/12/2014 10:00) ***
            }
        }

        private void cmdDescriptografa_Click(object sender, EventArgs e)
        {
            textBox1.Text = "";
            if (openFileDialog1.ShowDialog() == System.Windows.Forms.DialogResult.OK)
            {
                //*** MRC - INICIO - TRIBUTOS (04/12/2014 10:00) ***
                foreach (string arquivo in openFileDialog1.FileNames)
                {
                    string retorno = arquivo.Replace(Path.GetFileName(arquivo), @"DESCRIPTO_" + Path.GetFileName(arquivo));
                    if (CriptografiaArq.Bradesco.DescriptografaArquivo(txtArquivoCripto.Text, txtSenhaCripto.Text, arquivo, ref retorno) == 1)
                        textBox1.Text += "\r\n\r\nSUCESSO\r\n" + retorno;
                    else
                        textBox1.Text += "\r\n\r\nERRO\r\n" + retorno; 
                }
                //*** MRC - TERMINO - TRIBUTOS (04/12/2014 10:00) ***
            }
        }
    }
}
