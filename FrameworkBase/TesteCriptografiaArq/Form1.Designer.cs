﻿namespace TesteCriptografiaArq
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.panel1 = new System.Windows.Forms.Panel();
            this.txtSenhaCripto = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.txtArquivoCripto = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.cmdDescriptografa = new System.Windows.Forms.Button();
            this.cmdCriptografa = new System.Windows.Forms.Button();
            this.textBox1 = new System.Windows.Forms.TextBox();
            this.openFileDialog1 = new System.Windows.Forms.OpenFileDialog();
            this.panel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.txtSenhaCripto);
            this.panel1.Controls.Add(this.label2);
            this.panel1.Controls.Add(this.txtArquivoCripto);
            this.panel1.Controls.Add(this.label1);
            this.panel1.Controls.Add(this.cmdDescriptografa);
            this.panel1.Controls.Add(this.cmdCriptografa);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel1.Location = new System.Drawing.Point(0, 0);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(798, 77);
            this.panel1.TabIndex = 2;
            // 
            // txtSenhaCripto
            // 
            this.txtSenhaCripto.Location = new System.Drawing.Point(370, 43);
            this.txtSenhaCripto.Name = "txtSenhaCripto";
            this.txtSenhaCripto.Size = new System.Drawing.Size(107, 20);
            this.txtSenhaCripto.TabIndex = 3;
            this.txtSenhaCripto.Text = "neonsa01@";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(222, 46);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(97, 13);
            this.label2.TabIndex = 5;
            this.label2.Text = "Senha Criptografia:";
            // 
            // txtArquivoCripto
            // 
            this.txtArquivoCripto.Location = new System.Drawing.Point(370, 14);
            this.txtArquivoCripto.Name = "txtArquivoCripto";
            this.txtArquivoCripto.Size = new System.Drawing.Size(416, 20);
            this.txtArquivoCripto.TabIndex = 2;
            this.txtArquivoCripto.Text = "C:\\Temp\\Neon\\WEBTA\\Robo\\criptografia201410211454.bin";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(222, 17);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(142, 13);
            this.label1.TabIndex = 3;
            this.label1.Text = "Chave Criptografia (Arquivo):";
            // 
            // cmdDescriptografa
            // 
            this.cmdDescriptografa.Location = new System.Drawing.Point(12, 41);
            this.cmdDescriptografa.Name = "cmdDescriptografa";
            this.cmdDescriptografa.Size = new System.Drawing.Size(168, 23);
            this.cmdDescriptografa.TabIndex = 1;
            this.cmdDescriptografa.Text = "Descriptografa Arquivo";
            this.cmdDescriptografa.UseVisualStyleBackColor = true;
            this.cmdDescriptografa.Click += new System.EventHandler(this.cmdDescriptografa_Click);
            // 
            // cmdCriptografa
            // 
            this.cmdCriptografa.Location = new System.Drawing.Point(12, 12);
            this.cmdCriptografa.Name = "cmdCriptografa";
            this.cmdCriptografa.Size = new System.Drawing.Size(168, 23);
            this.cmdCriptografa.TabIndex = 0;
            this.cmdCriptografa.Text = "Criptografa Arquivo";
            this.cmdCriptografa.UseVisualStyleBackColor = true;
            this.cmdCriptografa.Click += new System.EventHandler(this.cmdCriptografa_Click);
            // 
            // textBox1
            // 
            this.textBox1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.textBox1.Font = new System.Drawing.Font("Courier New", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBox1.Location = new System.Drawing.Point(0, 77);
            this.textBox1.MaxLength = 0;
            this.textBox1.Multiline = true;
            this.textBox1.Name = "textBox1";
            this.textBox1.ScrollBars = System.Windows.Forms.ScrollBars.Both;
            this.textBox1.Size = new System.Drawing.Size(798, 478);
            this.textBox1.TabIndex = 4;
            this.textBox1.WordWrap = false;
            // 
            // openFileDialog1
            // 
            this.openFileDialog1.FileName = "openFileDialog1";
            this.openFileDialog1.Multiselect = true;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(798, 555);
            this.Controls.Add(this.textBox1);
            this.Controls.Add(this.panel1);
            this.Name = "Form1";
            this.Text = "Form1";
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Button cmdDescriptografa;
        private System.Windows.Forms.Button cmdCriptografa;
        private System.Windows.Forms.TextBox textBox1;
        private System.Windows.Forms.OpenFileDialog openFileDialog1;
        private System.Windows.Forms.TextBox txtSenhaCripto;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox txtArquivoCripto;
        private System.Windows.Forms.Label label1;
    }
}