using System;
using System.Collections.Generic;
using System.Text;
using System.IO;
using System.IO.Compression;

namespace VirZIP
{
    /// <summary>
    /// 
    /// </summary>
    public enum Erros {
       /// <summary>
       /// 
       /// </summary>        
       Leitura,
       /// <summary>
       /// 
       /// </summary>
       Dados_Invalidos,
       /// <summary>
       /// 
       /// </summary>
       ArquivoNEncontrado

    }

    /// <summary>
    /// 
    /// </summary>
    public class zip
    {
        /// <summary>
        /// 
        /// </summary>
        static public Erros Ultimoerro;

        /// <summary>
        /// 
        /// </summary>
        static public string MenErro;

        /// <summary>
        /// 
        /// </summary>
        static public Int32 TamBuf = 1024;

        /// <summary>
        /// 
        /// </summary>
        /// <param name="Unzipado"></param>
        /// <param name="Zipado"></param>
        /// <param name="Tarefa"></param>
        /// <returns></returns>
        static public bool Execute(string Unzipado, string Zipado, CompressionMode Tarefa)
        {
            FileStream Unzipfile;
            FileStream Zip__file;
            bool retorno = false;
            if(Tarefa == CompressionMode.Compress)
                using (Unzipfile = new FileStream(Unzipado, FileMode.Open, FileAccess.Read, FileShare.Read))
                    using (Zip__file = new FileStream(Zipado, FileMode.Create, FileAccess.Write))
                        retorno = Execute(Unzipfile, Zip__file,Tarefa);
            else
                using (Zip__file = new FileStream(Zipado, FileMode.Open, FileAccess.Read, FileShare.Read))
                    using (Unzipfile = new FileStream(Unzipado, FileMode.Create, FileAccess.Write))
                        retorno = Execute(Unzipfile, Zip__file, Tarefa);
            return retorno;            
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="Unzipfile"></param>
        /// <param name="Zipado"></param>
        /// <param name="Tarefa"></param>
        /// <returns></returns>
        static public bool Execute(Stream Unzipfile, string Zipado, CompressionMode Tarefa)
        {
            FileStream Zip__file;
            bool retorno = false;
            using (Zip__file = new FileStream(Zipado, FileMode.Create, FileAccess.Write))
                retorno = Execute(Unzipfile, Zip__file, Tarefa);
            return retorno;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="Unzipado"></param>
        /// <param name="Zip__file"></param>
        /// <param name="Tarefa"></param>
        /// <returns></returns>
        static public bool Execute(string Unzipado, Stream Zip__file, CompressionMode Tarefa)
        {
            FileStream Unzipfile;
            bool retorno = false;
            using (Unzipfile = new FileStream(Unzipado, FileMode.Open, FileAccess.Read, FileShare.Read))
                retorno = Execute(Unzipfile, Zip__file, Tarefa);
            return retorno;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="Unzipfile"></param>
        /// <param name="Zip__file"></param>
        /// <param name="Tarefa"></param>
        /// <returns></returns>
        static public bool Execute(Stream Unzipfile, Stream Zip__file,CompressionMode Tarefa)
        {
            try
            {
                byte[] buffer = new byte[TamBuf];
                GZipStream compressedzipStream;
                if (Tarefa == CompressionMode.Compress)
                {
                    compressedzipStream = new GZipStream(Zip__file, CompressionMode.Compress, true);
                    for (int Lidos = -1; Lidos != 0; Lidos = Unzipfile.Read(buffer, 0, TamBuf))
                        if (Lidos > 0)
                            compressedzipStream.Write(buffer, 0, Lidos);
                }
                else
                {
                    compressedzipStream = new GZipStream(Zip__file, CompressionMode.Decompress, true);
                    for (int totalCount = -1; totalCount != 0; totalCount = compressedzipStream.Read(buffer, 0, TamBuf))
                        if (totalCount > 0)
                            Unzipfile.Write(buffer, 0, totalCount);
                }
                compressedzipStream.Close();
                return true;
            } // end try
            catch (InvalidDataException)
            {
                Ultimoerro = Erros.Dados_Invalidos;
                return false;
            }
            catch (FileNotFoundException)
            {
                Ultimoerro = Erros.ArquivoNEncontrado;
                return false;
            }
            catch (ArgumentException e)
            {
                //Console.WriteLine("Error: path is a zero-length string, contains only white space, or contains one or more invalid characters");
                MenErro = e.Message;
                if (e.InnerException != null)
                    MenErro += (" - " + e.InnerException.Message);
                Ultimoerro = Erros.Leitura;
                return false;
            }
            catch (PathTooLongException)
            {
                //Console.WriteLine("Error: The specified path, file name, or both exceed the system-defined maximum length. For example, on Windows-based platforms, paths must be less than 248 characters, and file names must be less than 260 characters.");
                Ultimoerro = Erros.Leitura;
                return false;
            }
            catch (DirectoryNotFoundException)
            {
                //Console.WriteLine("Error: The specified path is invalid, such as being on an unmapped drive.");
                Ultimoerro = Erros.Leitura;
                return false;
            }
            catch (IOException)
            {
                //Console.WriteLine("Error: An I/O error occurred while opening the file.");
                Ultimoerro = Erros.Leitura;
                return false;
            }
            catch (UnauthorizedAccessException)
            {
                //Console.WriteLine("Error: path specified a file that is read-only, the path is a directory, or caller does not have the required permissions.");
                Ultimoerro = Erros.Leitura;
                return false;
            }
            catch (IndexOutOfRangeException)
            {
                // Console.WriteLine("Error: You must provide parameters for MyGZIP.");
                Ultimoerro = Erros.Leitura;
                return false;
            }
        }
        

    }
}
