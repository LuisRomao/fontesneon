﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.SqlClient;

namespace VirExceptionProc
{
    /// <summary>
    /// Tratamento virtual das Exception
    /// </summary>
    public class VirExceptionProc
    {
        /// <summary>
        /// Gera um cabeçalho para o retorno identificando o usuário
        /// </summary>
        /// <returns></returns>
        public static string CabecalhoRetorno()
        {
            string Cabecalho = String.Format("====================================================\r\n"+
                                             "               RELATÓRIO DE ERRO                    \r\n" +
                                             "====================================================\r\n" +
                                             "Data:{0:dd/MM/yyyy HH:mm:ss}\r\n"+
                                             "Usuário: {1} - {2}\r\n"+
                                             "Aplicativo:{3}\r\n"+
                                             "====================================================\r\n",            
                                                           DateTime.Now,
                                                           Environment.UserName,
                                                           Environment.MachineName,
                                                           /*Application.ExecutablePath ?? */Environment.CommandLine);            
            return Cabecalho;
        }

        /// <summary>
        /// Relatóro de erro para o usuário
        /// </summary>
        /// <param name="e"></param>
        /// <returns></returns>
        public static string RelatorioDeErroUsuario(Exception e)
        {
            try
            {
                string Retorno = "";
                int i = 1;
                Exception ex = e;
                while (ex != null)
                {                    
                    Retorno += string.Format("\r\n{0}) - {1}\r\n", i++, ex.Message.Replace("\r\n", "\r\n          "));                                        
                    ex = ex.InnerException;
                }                
                return Retorno;
            }
            catch (Exception e1)
            {
                return string.Format("ERRO SECUNDARIO:{0}\r\n{1}\r\n\r\nErro Anteriror:{2}\r\n{3}", e1.Message, e1.StackTrace, e.Message, e.StackTrace);
            }
        }

        /// <summary>
        /// Gera relatório de erro
        /// </summary>
        /// <param name="e"></param>
        /// <param name="Cabecalho"></param>
        /// <param name="Rastrear"></param>
        /// <param name="IncluirAssemby"></param>
        /// <returns></returns>
        public static string RelatorioDeErro(Exception e,bool Cabecalho, bool Rastrear, bool IncluirAssemby = true)
        {
            try
            {
                string Retorno = Cabecalho ? CabecalhoRetorno() : "";
                int i = 1;
                Exception ex = e;
                while (ex != null)
                {
                    int codigo = 0;
                    if (ex is SqlException)
                        codigo = ((SqlException)ex).Number;
                    Retorno += string.Format("\r\n{0}) - {1}\r\n{2} Código:{3}\r\n", i, ex.Message.Replace("\r\n", "\r\n          "), ex.GetType(), codigo);
                    if (Rastrear && (ex.StackTrace != null))
                    {
                        string[] linhas = ex.StackTrace.Split(new string[] { "\r\n" }, StringSplitOptions.RemoveEmptyEntries);
                        foreach (string linha in linhas)
                            Retorno += string.Format("  ->  {0}\r\n", linha);
                    }
                    i++;
                    ex = ex.InnerException;
                }
                if (IncluirAssemby)
                    Retorno += VersaoAssembles();
                Retorno += "====================================================\r\n";
                return Retorno;
            }
            catch (Exception e1)
            {
                return string.Format("ERRO SECUNDARIO:{0}\r\n{1}\r\n\r\nErro Anteriror:{2}\r\n{3}", e1.Message, e1.StackTrace, e.Message, e.StackTrace);
            }
        }
        
        /// <summary>
        /// 
        /// </summary>
        /// <param name="e"></param>
        /// <returns></returns>
        /// <remarks>
        /// Codigos de erros https://msdn.microsoft.com/en-us/library/cc645611.aspx
        /// </remarks>
        public static VirTipoDeErro IdentificaTipoDeErro(Exception e)
        {
            Exception ex = e;
            while (ex != null)
            {
                if (ex is System.Data.DBConcurrencyException)
                    return VirTipoDeErro.concorrencia;
                else if (ex is SqlException)
                    switch (((SqlException)ex).Number)
                    {
                        case -2: //Time out
                        case 1205: // deadlocked
                            return VirTipoDeErro.recuperavel;
                    }
                ex = ex.InnerException;
            }
            return VirTipoDeErro.efetivo_bug;
        }

        /// <summary>
        /// Lista dos assembles carregados com a versão
        /// </summary>
        /// <returns></returns>
        public static string VersaoAssembles()
        {
            string DadosDoErro = "====================================================\r\n"+
                                 "Assemblies:\r\n"+
                                 "====================================================\r\n";
            System.Reflection.Assembly[] Ass = AppDomain.CurrentDomain.GetAssemblies();
            SortedList<string, System.Version> Lista = new SortedList<string, System.Version>();
            if (Ass != null)
            {
                int Contador = 1;
                foreach (System.Reflection.Assembly As in Ass)                                                       
                    Lista.Add(string.Format("{0} ({1})", As.GetName().Name,Contador++), As.GetName().Version);                
                foreach (string nome in Lista.Keys)
                    DadosDoErro += string.Format("{0,10} - {1}\r\n", Lista[nome], nome);
            };            
            return DadosDoErro;
        }
    }

    /// <summary>
    /// Classificação do erro em função do tratamento
    /// </summary>
    public enum VirTipoDeErro
    {
        /// <summary>
        /// Não é um erro.
        /// </summary>
        sem_erro,
        /// <summary>
        /// Supostamente um bug a ser rastreado e tratado
        /// </summary>
        efetivo_bug,
        /// <summary>
        /// Erro decorrente de uma situação local: rede, memoria etc
        /// </summary>
        local,
        /// <summary>
        /// erro recuperavel: time-out, deadlock etc
        /// </summary>
        recuperavel,
        /// <summary>
        /// Erro de concorrencia deve ter um tratamento especial
        /// </summary>
        concorrencia,
    }
}
