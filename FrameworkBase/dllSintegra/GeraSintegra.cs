using System;
using System.Collections.Generic;
using System.Text;
using ArquivosTXT;
using System.Collections;

namespace dllSintegra
{
    public class GeraSintegra
    {
        public string NomeArquivo;

        public GeraSintegra(string NomeArquivo) {
            this.NomeArquivo = NomeArquivo;
        }

        private SortedList Dados1099;
        private SortedList Dados11;
        public SortedList ValDados1099 {
            get{
                if (Dados1099 == null)
                    Dados1099 = new SortedList();
                return Dados1099;
            }
        }
        public SortedList ValDados11 
        {
            get {
                if (Dados11 == null)
                    Dados11 = new SortedList();
                return Dados11;
            }
        }
        private dDados _dDados;
        public dDados DDados {
            get {
                if (_dDados == null)
                    _dDados = new dDados();
                return _dDados;
            }
            set {
                _dDados = value;
            }
        }

        public bool Grava() {
            if ((NomeArquivo == null) || (NomeArquivo == ""))
            {
                System.Windows.Forms.SaveFileDialog Dia = new System.Windows.Forms.SaveFileDialog();
                Dia.DefaultExt = ".txt";
                Dia.Filter = "Arquivo TXT|*.txt";
                Dia.FilterIndex = 0;
                if (Dia.ShowDialog() != System.Windows.Forms.DialogResult.OK)
                    return false;
                else
                    NomeArquivo = Dia.FileName;
            };
            cTabelaTXT Arquivo = new cTabelaTXT(null, ModelosTXT.layout);
            Arquivo.Formatodata = FormatoData.yyyyMMdd;
            Arquivo.RegistraMapa("Tipo"      , 1,   2, "10","10");
            Arquivo.RegistraMapa("CNPJ"      , 3,  14, "10");
            Arquivo.RegistraMapa("IE"        , 17, 14, "10");
            Arquivo.RegistraMapa("Nome"      , 31, 35, "10");
            Arquivo.RegistraMapa("Cidade"    , 66, 30, "10");
            Arquivo.RegistraMapa("UF"        , 96,  2, "10");
            Arquivo.RegistraMapa("FAX"       , 98, 10, "10");
            Arquivo.RegistraMapa("DataIni"   , 108, 8, "10");
            Arquivo.RegistraMapa("DataFim"   , 116, 8, "10");
            Arquivo.RegistraMapa("CodLayout" , 124, 1, "10",3);
            Arquivo.RegistraMapa("Natureza"  , 125, 1, "10",3);
            Arquivo.RegistraMapa("Finalidade", 126, 1, "10",1);

            Arquivo.RegistraMapa("Tipo"       , 1,  2, "11", "11");
            Arquivo.RegistraMapa("Logradouro" , 3, 34, "11");
            Arquivo.RegistraMapa("Numero"     , 37, 5, "11");
            Arquivo.RegistraMapa("Complemento", 42,22, "11");
            Arquivo.RegistraMapa("Bairro"     , 64,15, "11");
            Arquivo.RegistraMapa("CEP"        , 79, 8, "11");
            Arquivo.RegistraMapa("Contato"    , 87,28, "11");
            Arquivo.RegistraMapa("Telefone"   ,115,12, "11");

            Arquivo.RegistraMapa("Tipo"                            ,1  ,2, "50","50");
            Arquivo.RegistraMapa(DDados.Notas50.CNPJColumn         ,3  ,14,"50");
            Arquivo.RegistraMapa(DDados.Notas50.IEColumn           ,17 ,14,"50");
            Arquivo.RegistraMapa(DDados.Notas50.DataNotaColumn     ,31 ,8 ,"50");            
            Arquivo.RegistraMapa(DDados.Notas50.UFColumn           ,39 ,2 ,"50");
            Arquivo.RegistraMapa(DDados.Notas50.ModeloColumn       ,41 ,2 ,"50");
            Arquivo.RegistraMapa(DDados.Notas50.SerieColumn        ,43 ,3 ,"50");
            Arquivo.RegistraMapa(DDados.Notas50.NumeroColumn       ,46 ,6 ,"50");
            Arquivo.RegistraMapa(DDados.Notas50.CFOPColumn         ,52 ,4 ,"50");
            Arquivo.RegistraMapa("Emitente"                        ,56 ,1 ,"50", "T");
            Arquivo.RegistraMapa(DDados.Notas50.ValorTotalColumn   ,57 ,13,"50");
            Arquivo.RegistraMapa(DDados.Notas50.BaseICMSColumn     ,70 ,13,"50");
            Arquivo.RegistraMapa(DDados.Notas50.ValorICMSColumn    ,83 ,13,"50");
            Arquivo.RegistraMapa(DDados.Notas50.ValorIsentaNTColumn,96 ,13,"50");
            Arquivo.RegistraMapa(DDados.Notas50.ValorOutrasColumn  ,109,13,"50");
            Arquivo.RegistraMapa(DDados.Notas50.AliquotaColumn     ,122,4 ,"50");
            Arquivo.RegistraMapa("Situacao"                        ,126,1 ,"50", "N");

            Arquivo.RegistraMapa("Tipo"                            , 1,  2 ,"54", "54");
            Arquivo.RegistraMapa(DDados.Itens54.CNPJColumn         , 3, 14 ,"54");
            Arquivo.RegistraMapa(DDados.Itens54.ModeloColumn       ,17,  2 ,"54");
            Arquivo.RegistraMapa(DDados.Itens54.SerieColumn        ,19,  3 ,"54");
            Arquivo.RegistraMapa(DDados.Itens54.NumeroColumn       ,22,  6, "54");
            Arquivo.RegistraMapa(DDados.Itens54.CFOPColumn         ,28,  4, "54");
            Arquivo.RegistraMapa(DDados.Itens54.CSTColumn          ,32,  3, "54");
            Arquivo.RegistraMapa(DDados.Itens54.NColumn            ,35,  3, "54");
            Arquivo.RegistraMapa(DDados.Itens54.CodProdColumn      ,38 ,14, "54");
            Arquivo.RegistraMapa(DDados.Itens54.QuantidadeColumn   ,52 ,11, "54");
            Arquivo.RegistraMapa(DDados.Itens54.ValorTotalColumn   ,63 ,12, "54");
            Arquivo.RegistraMapa(DDados.Itens54.ValorDescontoColumn,75 ,12, "54");
            Arquivo.RegistraMapa(DDados.Itens54.BaseICMSColumn     ,87 ,12, "54");
            Arquivo.RegistraMapa(DDados.Itens54.BaseSubTColumn     ,99 ,12, "54");
            Arquivo.RegistraMapa(DDados.Itens54.ValorIPIColumn     ,111,12, "54");
            Arquivo.RegistraMapa(DDados.Itens54.AliquotaColumn     ,123, 4, "54");

            Arquivo.RegistraMapa("Tipo"                              ,   1,  2, "75", "75");
            Arquivo.RegistraMapa(DDados.Produtos75.DataIColumn       ,   3,  8, "75");
            Arquivo.RegistraMapa(DDados.Produtos75.DataFColumn       ,  11,  8, "75");
            Arquivo.RegistraMapa(DDados.Produtos75.CodProdColumn     ,  19, 14, "75");
            Arquivo.RegistraMapa(DDados.Produtos75.NCMColumn         ,  33,  8, "75");
            Arquivo.RegistraMapa(DDados.Produtos75.DescricaoColumn   ,  41, 53, "75");
            Arquivo.RegistraMapa(DDados.Produtos75.UnidadeColumn     ,  94,  6, "75");
            //Arquivo.RegistraMapa(DDados.Produtos75.STColumn          , 100,  3, "75");
            Arquivo.RegistraMapa(DDados.Produtos75.AliquotaIPIColumn , 100,  5, "75");
            Arquivo.RegistraMapa(DDados.Produtos75.AliquotaICMSColumn, 105,  4, "75");
            Arquivo.RegistraMapa(DDados.Produtos75.RedBaseICMSColumn , 109,  5, "75");
            Arquivo.RegistraMapa(DDados.Produtos75.BaseICMSSTColumn  , 114, 13, "75");


            Arquivo.RegistraMapa("Tipo",   1,  2, "90", "90");
            Arquivo.RegistraMapa("CNPJ",  3,  14, "90");
            Arquivo.RegistraMapa("IE",    17, 14, "90");

            int Totalizadores = 0;
            

            Arquivo.RegistraMapa("N90",  126,  1, "90", 1);

            
 
            int TotalRegistros = 3;
            if (Dados1099 != null)
                Dados1099.Remove("Tot99");           
                
            Arquivo.IniciaGravacao(NomeArquivo);
            if (Dados1099 != null)
                Arquivo.GravaLinha("10", Dados1099);
            else
                if (DDados.Empresa10_11.Rows.Count > 0)
                    Arquivo.GravaLinha("10", DDados.Empresa10_11[0]);
                else
                    return false;
            if (Dados11 != null)
                Arquivo.GravaLinha("11", Dados11);
            else
                if (DDados.Empresa10_11.Rows.Count > 0)
                    Arquivo.GravaLinha("11", DDados.Empresa10_11[0]);
                else
                    return false;
            if (DDados.Notas50.Rows.Count > 0)
            {
                Arquivo.GravaLinhas("50", DDados.Notas50);
                Arquivo.RegistraMapa("Tipo50", 31 + (Totalizadores * 10), 2, "90", "50");
                Arquivo.RegistraMapa("Tot50" , 33 + (Totalizadores * 10), 8, "90", DDados.Notas50.Rows.Count);
                Totalizadores++;
                TotalRegistros += DDados.Notas50.Rows.Count;
            };
            if (DDados.Itens54.Rows.Count > 0)
            {
                Arquivo.GravaLinhas("54", DDados.Itens54);
                Arquivo.RegistraMapa("Tipo54", 31 + (Totalizadores * 10), 2, "90", "54");
                Arquivo.RegistraMapa("Tot54" , 33 + (Totalizadores * 10), 8, "90", DDados.Itens54.Rows.Count);
                Totalizadores++;
                TotalRegistros += DDados.Itens54.Rows.Count;
            };
            if (DDados.Produtos75.Rows.Count > 0)
            {
                Arquivo.GravaLinhas("75", DDados.Produtos75);
                Arquivo.RegistraMapa("Tipo75", 31 + (Totalizadores * 10), 2, "90", "75");
                Arquivo.RegistraMapa("Tot75", 33 + (Totalizadores * 10), 8, "90", DDados.Produtos75.Rows.Count);
                Totalizadores++;
                TotalRegistros += DDados.Produtos75.Rows.Count;
            };
            Arquivo.RegistraMapa("Tipo99", 31 + (Totalizadores * 10), 2, "90", "99");
            Arquivo.RegistraMapa("Tot99" , 33 + (Totalizadores * 10), 8, "90");
            if (Dados1099 != null)
            {
                Dados1099.Add("Tot99", TotalRegistros);
                Arquivo.GravaLinha("90", Dados1099);
            }
            else
                if (DDados.Empresa10_11.Rows.Count > 0)
                {
                    DDados.Empresa10_11[0].Tot99 = TotalRegistros;
                    Arquivo.GravaLinha("90", DDados.Empresa10_11[0]);
                }
            
            
            Arquivo.TerminaGravacao();
            return true;
        }
    }
}
