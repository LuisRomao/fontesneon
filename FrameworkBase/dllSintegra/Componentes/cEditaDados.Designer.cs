namespace dllSintegra.Componentes
{
    partial class cEditaDados
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            DevExpress.XtraGrid.GridLevelNode gridLevelNode1 = new DevExpress.XtraGrid.GridLevelNode();
            DevExpress.XtraGrid.GridLevelNode gridLevelNode2 = new DevExpress.XtraGrid.GridLevelNode();
            this.gridView2 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colCNPJ1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colModelo1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSerie1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colNumero1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colCFOP1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colCST = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colN = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colCodProd = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colQuantidade = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colValorTotal1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colValorDesconto = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colBaseICMS1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colBaseSubT = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colValorIPI = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colAliquota1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn2 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryDEL = new DevExpress.XtraEditors.Repository.RepositoryItemButtonEdit();
            this.gridControl1 = new DevExpress.XtraGrid.GridControl();
            this.notas50BindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.bindingSourceDados = new System.Windows.Forms.BindingSource(this.components);
            this.gridView1 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colCNPJ = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colIE = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colDataNota = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colUF = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colModelo = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSerie = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colNumero = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colCFOP = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colValorTotal = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colBaseICMS = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colValorICMS = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colValorIsentaNT = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colValorOutras = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colAliquota = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colCodFornecedor = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colnumeroPedido = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridView3 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colCodProd1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colDataI = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colDataF = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colNCM = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colDescricao = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colUnidade = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colAliquotaIPI = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colAliquotaICMS = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colRedBaseICMS = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colBaseICMSST = new DevExpress.XtraGrid.Columns.GridColumn();
            this.xtraTabControl1 = new DevExpress.XtraTab.XtraTabControl();
            this.xtraTabPage2 = new DevExpress.XtraTab.XtraTabPage();
            this.gridControl2 = new DevExpress.XtraGrid.GridControl();
            this.empresa1011BindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.cardView1 = new DevExpress.XtraGrid.Views.Card.CardView();
            this.colCNPJ2 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colIE1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colNome = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colCidade = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colUF1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colFAX = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colDataIni = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colDataFim = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colLogradouro = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colNumero2 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colComplemento = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colBairro = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colCEP = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colContato = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colTelefone = new DevExpress.XtraGrid.Columns.GridColumn();
            this.PageNotas = new DevExpress.XtraTab.XtraTabPage();
            this.xtraTabPage1 = new DevExpress.XtraTab.XtraTabPage();
            this.gridControl3 = new DevExpress.XtraGrid.GridControl();
            this.gridView5 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.gridColumn16 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn17 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn18 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn19 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn20 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn21 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn22 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn23 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn24 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn25 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.panelControl1 = new DevExpress.XtraEditors.PanelControl();
            this.simpleButton3 = new DevExpress.XtraEditors.SimpleButton();
            this.simpleButton2 = new DevExpress.XtraEditors.SimpleButton();
            this.simpleButton1 = new DevExpress.XtraEditors.SimpleButton();
            ((System.ComponentModel.ISupportInitialize)(this.StyleController_F)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryDEL)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.notas50BindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.bindingSourceDados)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xtraTabControl1)).BeginInit();
            this.xtraTabControl1.SuspendLayout();
            this.xtraTabPage2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.empresa1011BindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cardView1)).BeginInit();
            this.PageNotas.SuspendLayout();
            this.xtraTabPage1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl1)).BeginInit();
            this.panelControl1.SuspendLayout();
            this.SuspendLayout();
            // 
            // ToolTipController_F
            // 
            this.ToolTipController_F.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.ToolTipController_F.Appearance.ForeColor = System.Drawing.Color.Black;
            this.ToolTipController_F.Appearance.Options.UseBackColor = true;
            this.ToolTipController_F.Appearance.Options.UseFont = true;
            this.ToolTipController_F.Appearance.Options.UseForeColor = true;
            this.ToolTipController_F.Appearance.Options.UseTextOptions = true;
            this.ToolTipController_F.Appearance.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            // 
            // StyleController_F
            // 
            this.StyleController_F.AppearanceDisabled.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this.StyleController_F.AppearanceDisabled.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this.StyleController_F.AppearanceDisabled.Options.UseBackColor = true;
            this.StyleController_F.AppearanceDisabled.Options.UseForeColor = true;
            this.StyleController_F.AppearanceFocused.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.StyleController_F.AppearanceFocused.Options.UseBackColor = true;
            // 
            // spellChecker1
            // 
            this.spellChecker1.OptionsSpelling.CheckSelectedTextFirst = DevExpress.Utils.DefaultBoolean.True;
            this.spellChecker1.OptionsSpelling.IgnoreEmails = DevExpress.Utils.DefaultBoolean.True;
            this.spellChecker1.OptionsSpelling.IgnoreMixedCaseWords = DevExpress.Utils.DefaultBoolean.False;
            this.spellChecker1.OptionsSpelling.IgnoreRepeatedWords = DevExpress.Utils.DefaultBoolean.False;
            this.spellChecker1.OptionsSpelling.IgnoreUpperCaseWords = DevExpress.Utils.DefaultBoolean.False;
            this.spellChecker1.OptionsSpelling.IgnoreUrls = DevExpress.Utils.DefaultBoolean.True;
            this.spellChecker1.OptionsSpelling.IgnoreWordsWithNumbers = DevExpress.Utils.DefaultBoolean.True;
            // 
            // gridView2
            // 
            this.gridView2.Appearance.ColumnFilterButton.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(162)))), ((int)(((byte)(194)))), ((int)(((byte)(102)))));
            this.gridView2.Appearance.ColumnFilterButton.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(162)))), ((int)(((byte)(194)))), ((int)(((byte)(102)))));
            this.gridView2.Appearance.ColumnFilterButton.ForeColor = System.Drawing.Color.Black;
            this.gridView2.Appearance.ColumnFilterButton.Options.UseBackColor = true;
            this.gridView2.Appearance.ColumnFilterButton.Options.UseBorderColor = true;
            this.gridView2.Appearance.ColumnFilterButton.Options.UseForeColor = true;
            this.gridView2.Appearance.ColumnFilterButtonActive.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(223)))), ((int)(((byte)(240)))), ((int)(((byte)(163)))));
            this.gridView2.Appearance.ColumnFilterButtonActive.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(223)))), ((int)(((byte)(240)))), ((int)(((byte)(163)))));
            this.gridView2.Appearance.ColumnFilterButtonActive.ForeColor = System.Drawing.Color.Black;
            this.gridView2.Appearance.ColumnFilterButtonActive.Options.UseBackColor = true;
            this.gridView2.Appearance.ColumnFilterButtonActive.Options.UseBorderColor = true;
            this.gridView2.Appearance.ColumnFilterButtonActive.Options.UseForeColor = true;
            this.gridView2.Appearance.Empty.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(207)))), ((int)(((byte)(229)))), ((int)(((byte)(128)))));
            this.gridView2.Appearance.Empty.BackColor2 = System.Drawing.Color.White;
            this.gridView2.Appearance.Empty.Options.UseBackColor = true;
            this.gridView2.Appearance.EvenRow.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(248)))), ((int)(((byte)(249)))), ((int)(((byte)(173)))));
            this.gridView2.Appearance.EvenRow.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(248)))), ((int)(((byte)(249)))), ((int)(((byte)(173)))));
            this.gridView2.Appearance.EvenRow.ForeColor = System.Drawing.Color.Black;
            this.gridView2.Appearance.EvenRow.Options.UseBackColor = true;
            this.gridView2.Appearance.EvenRow.Options.UseBorderColor = true;
            this.gridView2.Appearance.EvenRow.Options.UseForeColor = true;
            this.gridView2.Appearance.FilterCloseButton.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(162)))), ((int)(((byte)(194)))), ((int)(((byte)(102)))));
            this.gridView2.Appearance.FilterCloseButton.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(162)))), ((int)(((byte)(194)))), ((int)(((byte)(102)))));
            this.gridView2.Appearance.FilterCloseButton.ForeColor = System.Drawing.Color.Black;
            this.gridView2.Appearance.FilterCloseButton.Options.UseBackColor = true;
            this.gridView2.Appearance.FilterCloseButton.Options.UseBorderColor = true;
            this.gridView2.Appearance.FilterCloseButton.Options.UseForeColor = true;
            this.gridView2.Appearance.FilterPanel.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(207)))), ((int)(((byte)(229)))), ((int)(((byte)(128)))));
            this.gridView2.Appearance.FilterPanel.BackColor2 = System.Drawing.Color.White;
            this.gridView2.Appearance.FilterPanel.ForeColor = System.Drawing.Color.Black;
            this.gridView2.Appearance.FilterPanel.Options.UseBackColor = true;
            this.gridView2.Appearance.FilterPanel.Options.UseForeColor = true;
            this.gridView2.Appearance.FixedLine.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(139)))), ((int)(((byte)(159)))), ((int)(((byte)(69)))));
            this.gridView2.Appearance.FixedLine.Options.UseBackColor = true;
            this.gridView2.Appearance.FocusedCell.BackColor = System.Drawing.Color.White;
            this.gridView2.Appearance.FocusedCell.ForeColor = System.Drawing.Color.Black;
            this.gridView2.Appearance.FocusedCell.Options.UseBackColor = true;
            this.gridView2.Appearance.FocusedCell.Options.UseForeColor = true;
            this.gridView2.Appearance.FocusedRow.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(129)))), ((int)(((byte)(152)))), ((int)(((byte)(49)))));
            this.gridView2.Appearance.FocusedRow.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(144)))), ((int)(((byte)(167)))), ((int)(((byte)(62)))));
            this.gridView2.Appearance.FocusedRow.ForeColor = System.Drawing.Color.White;
            this.gridView2.Appearance.FocusedRow.Options.UseBackColor = true;
            this.gridView2.Appearance.FocusedRow.Options.UseBorderColor = true;
            this.gridView2.Appearance.FocusedRow.Options.UseForeColor = true;
            this.gridView2.Appearance.FooterPanel.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(162)))), ((int)(((byte)(194)))), ((int)(((byte)(102)))));
            this.gridView2.Appearance.FooterPanel.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(162)))), ((int)(((byte)(194)))), ((int)(((byte)(102)))));
            this.gridView2.Appearance.FooterPanel.ForeColor = System.Drawing.Color.Black;
            this.gridView2.Appearance.FooterPanel.Options.UseBackColor = true;
            this.gridView2.Appearance.FooterPanel.Options.UseBorderColor = true;
            this.gridView2.Appearance.FooterPanel.Options.UseForeColor = true;
            this.gridView2.Appearance.GroupButton.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(162)))), ((int)(((byte)(194)))), ((int)(((byte)(102)))));
            this.gridView2.Appearance.GroupButton.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(162)))), ((int)(((byte)(194)))), ((int)(((byte)(102)))));
            this.gridView2.Appearance.GroupButton.ForeColor = System.Drawing.Color.Black;
            this.gridView2.Appearance.GroupButton.Options.UseBackColor = true;
            this.gridView2.Appearance.GroupButton.Options.UseBorderColor = true;
            this.gridView2.Appearance.GroupButton.Options.UseForeColor = true;
            this.gridView2.Appearance.GroupFooter.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(207)))), ((int)(((byte)(229)))), ((int)(((byte)(128)))));
            this.gridView2.Appearance.GroupFooter.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(207)))), ((int)(((byte)(229)))), ((int)(((byte)(128)))));
            this.gridView2.Appearance.GroupFooter.ForeColor = System.Drawing.Color.Black;
            this.gridView2.Appearance.GroupFooter.Options.UseBackColor = true;
            this.gridView2.Appearance.GroupFooter.Options.UseBorderColor = true;
            this.gridView2.Appearance.GroupFooter.Options.UseForeColor = true;
            this.gridView2.Appearance.GroupPanel.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(207)))), ((int)(((byte)(229)))), ((int)(((byte)(128)))));
            this.gridView2.Appearance.GroupPanel.BackColor2 = System.Drawing.Color.White;
            this.gridView2.Appearance.GroupPanel.ForeColor = System.Drawing.Color.Black;
            this.gridView2.Appearance.GroupPanel.Options.UseBackColor = true;
            this.gridView2.Appearance.GroupPanel.Options.UseForeColor = true;
            this.gridView2.Appearance.GroupRow.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(207)))), ((int)(((byte)(229)))), ((int)(((byte)(128)))));
            this.gridView2.Appearance.GroupRow.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(207)))), ((int)(((byte)(229)))), ((int)(((byte)(128)))));
            this.gridView2.Appearance.GroupRow.ForeColor = System.Drawing.Color.Black;
            this.gridView2.Appearance.GroupRow.Options.UseBackColor = true;
            this.gridView2.Appearance.GroupRow.Options.UseBorderColor = true;
            this.gridView2.Appearance.GroupRow.Options.UseForeColor = true;
            this.gridView2.Appearance.HeaderPanel.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(180)))), ((int)(((byte)(214)))), ((int)(((byte)(115)))));
            this.gridView2.Appearance.HeaderPanel.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(180)))), ((int)(((byte)(214)))), ((int)(((byte)(115)))));
            this.gridView2.Appearance.HeaderPanel.ForeColor = System.Drawing.Color.Black;
            this.gridView2.Appearance.HeaderPanel.Options.UseBackColor = true;
            this.gridView2.Appearance.HeaderPanel.Options.UseBorderColor = true;
            this.gridView2.Appearance.HeaderPanel.Options.UseForeColor = true;
            this.gridView2.Appearance.HideSelectionRow.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(155)))), ((int)(((byte)(176)))), ((int)(((byte)(84)))));
            this.gridView2.Appearance.HideSelectionRow.ForeColor = System.Drawing.Color.White;
            this.gridView2.Appearance.HideSelectionRow.Options.UseBackColor = true;
            this.gridView2.Appearance.HideSelectionRow.Options.UseForeColor = true;
            this.gridView2.Appearance.HorzLine.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(162)))), ((int)(((byte)(194)))), ((int)(((byte)(102)))));
            this.gridView2.Appearance.HorzLine.Options.UseBackColor = true;
            this.gridView2.Appearance.OddRow.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(223)))), ((int)(((byte)(240)))), ((int)(((byte)(163)))));
            this.gridView2.Appearance.OddRow.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(223)))), ((int)(((byte)(240)))), ((int)(((byte)(163)))));
            this.gridView2.Appearance.OddRow.ForeColor = System.Drawing.Color.Black;
            this.gridView2.Appearance.OddRow.Options.UseBackColor = true;
            this.gridView2.Appearance.OddRow.Options.UseBorderColor = true;
            this.gridView2.Appearance.OddRow.Options.UseForeColor = true;
            this.gridView2.Appearance.Preview.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(252)))), ((int)(((byte)(255)))), ((int)(((byte)(220)))));
            this.gridView2.Appearance.Preview.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(252)))), ((int)(((byte)(255)))), ((int)(((byte)(220)))));
            this.gridView2.Appearance.Preview.Font = new System.Drawing.Font("Verdana", 7.5F);
            this.gridView2.Appearance.Preview.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(157)))), ((int)(((byte)(177)))), ((int)(((byte)(90)))));
            this.gridView2.Appearance.Preview.Options.UseBackColor = true;
            this.gridView2.Appearance.Preview.Options.UseBorderColor = true;
            this.gridView2.Appearance.Preview.Options.UseFont = true;
            this.gridView2.Appearance.Preview.Options.UseForeColor = true;
            this.gridView2.Appearance.Row.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(248)))), ((int)(((byte)(249)))), ((int)(((byte)(173)))));
            this.gridView2.Appearance.Row.ForeColor = System.Drawing.Color.Black;
            this.gridView2.Appearance.Row.Options.UseBackColor = true;
            this.gridView2.Appearance.Row.Options.UseForeColor = true;
            this.gridView2.Appearance.RowSeparator.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(207)))), ((int)(((byte)(229)))), ((int)(((byte)(128)))));
            this.gridView2.Appearance.RowSeparator.BackColor2 = System.Drawing.Color.White;
            this.gridView2.Appearance.RowSeparator.Options.UseBackColor = true;
            this.gridView2.Appearance.SelectedRow.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(144)))), ((int)(((byte)(167)))), ((int)(((byte)(62)))));
            this.gridView2.Appearance.SelectedRow.Options.UseBackColor = true;
            this.gridView2.Appearance.TopNewRow.BackColor = System.Drawing.Color.White;
            this.gridView2.Appearance.TopNewRow.Options.UseBackColor = true;
            this.gridView2.Appearance.VertLine.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(162)))), ((int)(((byte)(194)))), ((int)(((byte)(102)))));
            this.gridView2.Appearance.VertLine.Options.UseBackColor = true;
            this.gridView2.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colCNPJ1,
            this.colModelo1,
            this.colSerie1,
            this.colNumero1,
            this.colCFOP1,
            this.colCST,
            this.colN,
            this.colCodProd,
            this.colQuantidade,
            this.colValorTotal1,
            this.colValorDesconto,
            this.colBaseICMS1,
            this.colBaseSubT,
            this.colValorIPI,
            this.colAliquota1,
            this.gridColumn2});
            this.gridView2.GridControl = this.gridControl1;
            this.gridView2.Name = "gridView2";
            this.gridView2.OptionsDetail.AllowExpandEmptyDetails = true;
            this.gridView2.OptionsView.ColumnAutoWidth = false;
            this.gridView2.OptionsView.NewItemRowPosition = DevExpress.XtraGrid.Views.Grid.NewItemRowPosition.Bottom;
            this.gridView2.OptionsView.ShowGroupPanel = false;
            this.gridView2.ViewCaption = "Itens da nota";
            // 
            // colCNPJ1
            // 
            this.colCNPJ1.Caption = "CNPJ";
            this.colCNPJ1.FieldName = "CNPJ";
            this.colCNPJ1.Name = "colCNPJ1";
            this.colCNPJ1.OptionsColumn.ReadOnly = true;
            this.colCNPJ1.Width = 133;
            // 
            // colModelo1
            // 
            this.colModelo1.Caption = "Modelo";
            this.colModelo1.FieldName = "Modelo";
            this.colModelo1.Name = "colModelo1";
            // 
            // colSerie1
            // 
            this.colSerie1.Caption = "Serie";
            this.colSerie1.FieldName = "Serie";
            this.colSerie1.Name = "colSerie1";
            // 
            // colNumero1
            // 
            this.colNumero1.Caption = "Numero";
            this.colNumero1.FieldName = "Numero";
            this.colNumero1.Name = "colNumero1";
            this.colNumero1.OptionsColumn.ReadOnly = true;
            // 
            // colCFOP1
            // 
            this.colCFOP1.Caption = "CFOP";
            this.colCFOP1.FieldName = "CFOP";
            this.colCFOP1.Name = "colCFOP1";
            this.colCFOP1.OptionsColumn.ReadOnly = true;
            this.colCFOP1.Visible = true;
            this.colCFOP1.VisibleIndex = 0;
            this.colCFOP1.Width = 99;
            // 
            // colCST
            // 
            this.colCST.Caption = "CST";
            this.colCST.FieldName = "CST";
            this.colCST.Name = "colCST";
            this.colCST.Visible = true;
            this.colCST.VisibleIndex = 1;
            this.colCST.Width = 90;
            // 
            // colN
            // 
            this.colN.Caption = "N";
            this.colN.FieldName = "N";
            this.colN.Name = "colN";
            this.colN.Visible = true;
            this.colN.VisibleIndex = 2;
            // 
            // colCodProd
            // 
            this.colCodProd.Caption = "CodProd";
            this.colCodProd.FieldName = "CodProd";
            this.colCodProd.Name = "colCodProd";
            this.colCodProd.Visible = true;
            this.colCodProd.VisibleIndex = 3;
            this.colCodProd.Width = 113;
            // 
            // colQuantidade
            // 
            this.colQuantidade.Caption = "Quantidade";
            this.colQuantidade.FieldName = "Quantidade";
            this.colQuantidade.Name = "colQuantidade";
            this.colQuantidade.Visible = true;
            this.colQuantidade.VisibleIndex = 4;
            // 
            // colValorTotal1
            // 
            this.colValorTotal1.Caption = "ValorTotal";
            this.colValorTotal1.FieldName = "ValorTotal";
            this.colValorTotal1.Name = "colValorTotal1";
            this.colValorTotal1.Visible = true;
            this.colValorTotal1.VisibleIndex = 5;
            this.colValorTotal1.Width = 79;
            // 
            // colValorDesconto
            // 
            this.colValorDesconto.Caption = "ValorDesconto";
            this.colValorDesconto.FieldName = "ValorDesconto";
            this.colValorDesconto.Name = "colValorDesconto";
            this.colValorDesconto.Visible = true;
            this.colValorDesconto.VisibleIndex = 6;
            // 
            // colBaseICMS1
            // 
            this.colBaseICMS1.Caption = "BaseICMS";
            this.colBaseICMS1.FieldName = "BaseICMS";
            this.colBaseICMS1.Name = "colBaseICMS1";
            this.colBaseICMS1.Visible = true;
            this.colBaseICMS1.VisibleIndex = 7;
            // 
            // colBaseSubT
            // 
            this.colBaseSubT.Caption = "BaseSubT";
            this.colBaseSubT.FieldName = "BaseSubT";
            this.colBaseSubT.Name = "colBaseSubT";
            this.colBaseSubT.Visible = true;
            this.colBaseSubT.VisibleIndex = 8;
            // 
            // colValorIPI
            // 
            this.colValorIPI.Caption = "ValorIPI";
            this.colValorIPI.FieldName = "ValorIPI";
            this.colValorIPI.Name = "colValorIPI";
            this.colValorIPI.Visible = true;
            this.colValorIPI.VisibleIndex = 9;
            // 
            // colAliquota1
            // 
            this.colAliquota1.Caption = "Aliquota";
            this.colAliquota1.FieldName = "Aliquota";
            this.colAliquota1.Name = "colAliquota1";
            this.colAliquota1.OptionsColumn.ReadOnly = true;
            this.colAliquota1.Visible = true;
            this.colAliquota1.VisibleIndex = 10;
            // 
            // gridColumn2
            // 
            this.gridColumn2.Caption = "gridColumn2";
            this.gridColumn2.ColumnEdit = this.repositoryDEL;
            this.gridColumn2.Fixed = DevExpress.XtraGrid.Columns.FixedStyle.Right;
            this.gridColumn2.Name = "gridColumn2";
            this.gridColumn2.OptionsColumn.FixedWidth = true;
            this.gridColumn2.OptionsColumn.ShowCaption = false;
            this.gridColumn2.Visible = true;
            this.gridColumn2.VisibleIndex = 11;
            this.gridColumn2.Width = 25;
            // 
            // repositoryDEL
            // 
            this.repositoryDEL.AllowFocused = false;
            this.repositoryDEL.AutoHeight = false;
            this.repositoryDEL.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Delete)});
            this.repositoryDEL.Name = "repositoryDEL";
            this.repositoryDEL.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.HideTextEditor;
            this.repositoryDEL.ButtonClick += new DevExpress.XtraEditors.Controls.ButtonPressedEventHandler(this.repositoryDEL_ButtonClick);
            // 
            // gridControl1
            // 
            this.gridControl1.DataSource = this.notas50BindingSource;
            this.gridControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            gridLevelNode1.LevelTemplate = this.gridView2;
            gridLevelNode2.LevelTemplate = this.gridView3;
            gridLevelNode2.RelationName = "Itens54_Produtos75";
            gridLevelNode1.Nodes.AddRange(new DevExpress.XtraGrid.GridLevelNode[] {
            gridLevelNode2});
            gridLevelNode1.RelationName = "Notas50_Itens54";
            this.gridControl1.LevelTree.Nodes.AddRange(new DevExpress.XtraGrid.GridLevelNode[] {
            gridLevelNode1});
            this.gridControl1.Location = new System.Drawing.Point(0, 0);
            this.gridControl1.MainView = this.gridView1;
            this.gridControl1.Name = "gridControl1";
            this.gridControl1.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryDEL});
            this.gridControl1.Size = new System.Drawing.Size(967, 401);
            this.gridControl1.TabIndex = 0;
            this.gridControl1.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridView1,
            this.gridView3,
            this.gridView2});
            // 
            // notas50BindingSource
            // 
            this.notas50BindingSource.DataMember = "Notas50";
            this.notas50BindingSource.DataSource = this.bindingSourceDados;
            // 
            // bindingSourceDados
            // 
            this.bindingSourceDados.DataSource = typeof(dllSintegra.dDados);
            this.bindingSourceDados.Position = 0;
            // 
            // gridView1
            // 
            this.gridView1.Appearance.ColumnFilterButton.BackColor = System.Drawing.Color.Orange;
            this.gridView1.Appearance.ColumnFilterButton.BorderColor = System.Drawing.Color.Orange;
            this.gridView1.Appearance.ColumnFilterButton.ForeColor = System.Drawing.Color.DarkOrange;
            this.gridView1.Appearance.ColumnFilterButton.Options.UseBackColor = true;
            this.gridView1.Appearance.ColumnFilterButton.Options.UseBorderColor = true;
            this.gridView1.Appearance.ColumnFilterButton.Options.UseForeColor = true;
            this.gridView1.Appearance.ColumnFilterButtonActive.BackColor = System.Drawing.Color.DarkOrange;
            this.gridView1.Appearance.ColumnFilterButtonActive.BorderColor = System.Drawing.Color.DarkOrange;
            this.gridView1.Appearance.ColumnFilterButtonActive.ForeColor = System.Drawing.Color.White;
            this.gridView1.Appearance.ColumnFilterButtonActive.Options.UseBackColor = true;
            this.gridView1.Appearance.ColumnFilterButtonActive.Options.UseBorderColor = true;
            this.gridView1.Appearance.ColumnFilterButtonActive.Options.UseForeColor = true;
            this.gridView1.Appearance.Empty.BackColor = System.Drawing.Color.LightSkyBlue;
            this.gridView1.Appearance.Empty.BackColor2 = System.Drawing.Color.SkyBlue;
            this.gridView1.Appearance.Empty.GradientMode = System.Drawing.Drawing2D.LinearGradientMode.BackwardDiagonal;
            this.gridView1.Appearance.Empty.Options.UseBackColor = true;
            this.gridView1.Appearance.EvenRow.BackColor = System.Drawing.Color.Linen;
            this.gridView1.Appearance.EvenRow.BackColor2 = System.Drawing.Color.AntiqueWhite;
            this.gridView1.Appearance.EvenRow.GradientMode = System.Drawing.Drawing2D.LinearGradientMode.ForwardDiagonal;
            this.gridView1.Appearance.EvenRow.Options.UseBackColor = true;
            this.gridView1.Appearance.FilterCloseButton.BackColor = System.Drawing.Color.Orange;
            this.gridView1.Appearance.FilterCloseButton.BorderColor = System.Drawing.Color.Orange;
            this.gridView1.Appearance.FilterCloseButton.Options.UseBackColor = true;
            this.gridView1.Appearance.FilterCloseButton.Options.UseBorderColor = true;
            this.gridView1.Appearance.FilterPanel.BackColor = System.Drawing.Color.DarkOrange;
            this.gridView1.Appearance.FilterPanel.BackColor2 = System.Drawing.Color.Orange;
            this.gridView1.Appearance.FilterPanel.ForeColor = System.Drawing.Color.Black;
            this.gridView1.Appearance.FilterPanel.GradientMode = System.Drawing.Drawing2D.LinearGradientMode.BackwardDiagonal;
            this.gridView1.Appearance.FilterPanel.Options.UseBackColor = true;
            this.gridView1.Appearance.FilterPanel.Options.UseForeColor = true;
            this.gridView1.Appearance.FocusedRow.BackColor = System.Drawing.Color.RoyalBlue;
            this.gridView1.Appearance.FocusedRow.BackColor2 = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this.gridView1.Appearance.FocusedRow.ForeColor = System.Drawing.Color.White;
            this.gridView1.Appearance.FocusedRow.GradientMode = System.Drawing.Drawing2D.LinearGradientMode.Vertical;
            this.gridView1.Appearance.FocusedRow.Options.UseBackColor = true;
            this.gridView1.Appearance.FocusedRow.Options.UseForeColor = true;
            this.gridView1.Appearance.FooterPanel.BackColor = System.Drawing.Color.Orange;
            this.gridView1.Appearance.FooterPanel.BorderColor = System.Drawing.Color.Orange;
            this.gridView1.Appearance.FooterPanel.Options.UseBackColor = true;
            this.gridView1.Appearance.FooterPanel.Options.UseBorderColor = true;
            this.gridView1.Appearance.GroupButton.BackColor = System.Drawing.Color.Wheat;
            this.gridView1.Appearance.GroupButton.BorderColor = System.Drawing.Color.Wheat;
            this.gridView1.Appearance.GroupButton.Options.UseBackColor = true;
            this.gridView1.Appearance.GroupButton.Options.UseBorderColor = true;
            this.gridView1.Appearance.GroupFooter.BackColor = System.Drawing.Color.Wheat;
            this.gridView1.Appearance.GroupFooter.BorderColor = System.Drawing.Color.Wheat;
            this.gridView1.Appearance.GroupFooter.Options.UseBackColor = true;
            this.gridView1.Appearance.GroupFooter.Options.UseBorderColor = true;
            this.gridView1.Appearance.GroupPanel.BackColor = System.Drawing.Color.RoyalBlue;
            this.gridView1.Appearance.GroupPanel.ForeColor = System.Drawing.Color.White;
            this.gridView1.Appearance.GroupPanel.Options.UseBackColor = true;
            this.gridView1.Appearance.GroupPanel.Options.UseForeColor = true;
            this.gridView1.Appearance.GroupRow.BackColor = System.Drawing.Color.Wheat;
            this.gridView1.Appearance.GroupRow.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Bold);
            this.gridView1.Appearance.GroupRow.Options.UseBackColor = true;
            this.gridView1.Appearance.GroupRow.Options.UseFont = true;
            this.gridView1.Appearance.HeaderPanel.BackColor = System.Drawing.Color.Orange;
            this.gridView1.Appearance.HeaderPanel.BorderColor = System.Drawing.Color.Orange;
            this.gridView1.Appearance.HeaderPanel.Options.UseBackColor = true;
            this.gridView1.Appearance.HeaderPanel.Options.UseBorderColor = true;
            this.gridView1.Appearance.HideSelectionRow.BackColor = System.Drawing.Color.LightSlateGray;
            this.gridView1.Appearance.HideSelectionRow.Options.UseBackColor = true;
            this.gridView1.Appearance.HorzLine.BackColor = System.Drawing.Color.Tan;
            this.gridView1.Appearance.HorzLine.Options.UseBackColor = true;
            this.gridView1.Appearance.OddRow.BackColor = System.Drawing.Color.Linen;
            this.gridView1.Appearance.OddRow.BackColor2 = System.Drawing.Color.AntiqueWhite;
            this.gridView1.Appearance.OddRow.GradientMode = System.Drawing.Drawing2D.LinearGradientMode.Vertical;
            this.gridView1.Appearance.OddRow.Options.UseBackColor = true;
            this.gridView1.Appearance.Preview.BackColor = System.Drawing.Color.Khaki;
            this.gridView1.Appearance.Preview.BackColor2 = System.Drawing.Color.Cornsilk;
            this.gridView1.Appearance.Preview.Font = new System.Drawing.Font("Tahoma", 7.5F);
            this.gridView1.Appearance.Preview.GradientMode = System.Drawing.Drawing2D.LinearGradientMode.BackwardDiagonal;
            this.gridView1.Appearance.Preview.Options.UseBackColor = true;
            this.gridView1.Appearance.Preview.Options.UseFont = true;
            this.gridView1.Appearance.Row.BackColor = System.Drawing.Color.Linen;
            this.gridView1.Appearance.Row.BackColor2 = System.Drawing.Color.AntiqueWhite;
            this.gridView1.Appearance.Row.Options.UseBackColor = true;
            this.gridView1.Appearance.RowSeparator.BackColor = System.Drawing.Color.LightSkyBlue;
            this.gridView1.Appearance.RowSeparator.Options.UseBackColor = true;
            this.gridView1.Appearance.VertLine.BackColor = System.Drawing.Color.Tan;
            this.gridView1.Appearance.VertLine.Options.UseBackColor = true;
            this.gridView1.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colCNPJ,
            this.colIE,
            this.colDataNota,
            this.colUF,
            this.colModelo,
            this.colSerie,
            this.colNumero,
            this.colCFOP,
            this.colValorTotal,
            this.colBaseICMS,
            this.colValorICMS,
            this.colValorIsentaNT,
            this.colValorOutras,
            this.colAliquota,
            this.colCodFornecedor,
            this.colnumeroPedido,
            this.gridColumn1});
            this.gridView1.GridControl = this.gridControl1;
            this.gridView1.Name = "gridView1";
            this.gridView1.OptionsDetail.AllowExpandEmptyDetails = true;
            this.gridView1.OptionsView.ColumnAutoWidth = false;
            this.gridView1.OptionsView.NewItemRowPosition = DevExpress.XtraGrid.Views.Grid.NewItemRowPosition.Bottom;
            this.gridView1.OptionsView.ShowAutoFilterRow = true;
            this.gridView1.OptionsView.ShowGroupPanel = false;
            this.gridView1.SortInfo.AddRange(new DevExpress.XtraGrid.Columns.GridColumnSortInfo[] {
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colDataNota, DevExpress.Data.ColumnSortOrder.Ascending),
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colCNPJ, DevExpress.Data.ColumnSortOrder.Ascending)});
            this.gridView1.ViewCaption = "Notas";
            // 
            // colCNPJ
            // 
            this.colCNPJ.Caption = "CNPJ";
            this.colCNPJ.FieldName = "CNPJ";
            this.colCNPJ.Name = "colCNPJ";
            this.colCNPJ.Visible = true;
            this.colCNPJ.VisibleIndex = 0;
            this.colCNPJ.Width = 113;
            // 
            // colIE
            // 
            this.colIE.Caption = "IE";
            this.colIE.FieldName = "IE";
            this.colIE.Name = "colIE";
            this.colIE.Visible = true;
            this.colIE.VisibleIndex = 1;
            this.colIE.Width = 118;
            // 
            // colDataNota
            // 
            this.colDataNota.Caption = "Data Nota";
            this.colDataNota.FieldName = "DataNota";
            this.colDataNota.Name = "colDataNota";
            this.colDataNota.Visible = true;
            this.colDataNota.VisibleIndex = 6;
            this.colDataNota.Width = 77;
            // 
            // colUF
            // 
            this.colUF.Caption = "UF";
            this.colUF.FieldName = "UF";
            this.colUF.Name = "colUF";
            this.colUF.Visible = true;
            this.colUF.VisibleIndex = 8;
            this.colUF.Width = 41;
            // 
            // colModelo
            // 
            this.colModelo.Caption = "Modelo";
            this.colModelo.FieldName = "Modelo";
            this.colModelo.Name = "colModelo";
            this.colModelo.Visible = true;
            this.colModelo.VisibleIndex = 2;
            this.colModelo.Width = 55;
            // 
            // colSerie
            // 
            this.colSerie.Caption = "Serie";
            this.colSerie.FieldName = "Serie";
            this.colSerie.Name = "colSerie";
            this.colSerie.Visible = true;
            this.colSerie.VisibleIndex = 3;
            this.colSerie.Width = 50;
            // 
            // colNumero
            // 
            this.colNumero.Caption = "Numero";
            this.colNumero.FieldName = "Numero";
            this.colNumero.Name = "colNumero";
            this.colNumero.Visible = true;
            this.colNumero.VisibleIndex = 4;
            this.colNumero.Width = 57;
            // 
            // colCFOP
            // 
            this.colCFOP.Caption = "CFOP";
            this.colCFOP.FieldName = "CFOP";
            this.colCFOP.Name = "colCFOP";
            this.colCFOP.Visible = true;
            this.colCFOP.VisibleIndex = 5;
            this.colCFOP.Width = 58;
            // 
            // colValorTotal
            // 
            this.colValorTotal.Caption = "Valor Total";
            this.colValorTotal.DisplayFormat.FormatString = "n2";
            this.colValorTotal.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.colValorTotal.FieldName = "ValorTotal";
            this.colValorTotal.Name = "colValorTotal";
            this.colValorTotal.Visible = true;
            this.colValorTotal.VisibleIndex = 9;
            this.colValorTotal.Width = 79;
            // 
            // colBaseICMS
            // 
            this.colBaseICMS.Caption = "Base ICMS";
            this.colBaseICMS.DisplayFormat.FormatString = "n2";
            this.colBaseICMS.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.colBaseICMS.FieldName = "BaseICMS";
            this.colBaseICMS.Name = "colBaseICMS";
            this.colBaseICMS.Visible = true;
            this.colBaseICMS.VisibleIndex = 10;
            this.colBaseICMS.Width = 81;
            // 
            // colValorICMS
            // 
            this.colValorICMS.Caption = "Valor ICMS";
            this.colValorICMS.DisplayFormat.FormatString = "n2";
            this.colValorICMS.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.colValorICMS.FieldName = "ValorICMS";
            this.colValorICMS.Name = "colValorICMS";
            this.colValorICMS.Visible = true;
            this.colValorICMS.VisibleIndex = 11;
            this.colValorICMS.Width = 73;
            // 
            // colValorIsentaNT
            // 
            this.colValorIsentaNT.Caption = "ValorIsentaNT";
            this.colValorIsentaNT.DisplayFormat.FormatString = "n2";
            this.colValorIsentaNT.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.colValorIsentaNT.FieldName = "ValorIsentaNT";
            this.colValorIsentaNT.Name = "colValorIsentaNT";
            this.colValorIsentaNT.Visible = true;
            this.colValorIsentaNT.VisibleIndex = 12;
            this.colValorIsentaNT.Width = 92;
            // 
            // colValorOutras
            // 
            this.colValorOutras.Caption = "Valor Outras";
            this.colValorOutras.DisplayFormat.FormatString = "n2";
            this.colValorOutras.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.colValorOutras.FieldName = "ValorOutras";
            this.colValorOutras.Name = "colValorOutras";
            this.colValorOutras.Visible = true;
            this.colValorOutras.VisibleIndex = 13;
            this.colValorOutras.Width = 89;
            // 
            // colAliquota
            // 
            this.colAliquota.Caption = "Al�quota";
            this.colAliquota.FieldName = "Aliquota";
            this.colAliquota.Name = "colAliquota";
            this.colAliquota.Visible = true;
            this.colAliquota.VisibleIndex = 7;
            this.colAliquota.Width = 62;
            // 
            // colCodFornecedor
            // 
            this.colCodFornecedor.Caption = "c�digo Fornecedor";
            this.colCodFornecedor.FieldName = "CodFornecedor";
            this.colCodFornecedor.Name = "colCodFornecedor";
            this.colCodFornecedor.Visible = true;
            this.colCodFornecedor.VisibleIndex = 14;
            this.colCodFornecedor.Width = 112;
            // 
            // colnumeroPedido
            // 
            this.colnumeroPedido.Caption = "Pedido";
            this.colnumeroPedido.FieldName = "numeroPedido";
            this.colnumeroPedido.Name = "colnumeroPedido";
            this.colnumeroPedido.Visible = true;
            this.colnumeroPedido.VisibleIndex = 15;
            this.colnumeroPedido.Width = 74;
            // 
            // gridColumn1
            // 
            this.gridColumn1.Caption = "gridColumn1";
            this.gridColumn1.ColumnEdit = this.repositoryDEL;
            this.gridColumn1.Fixed = DevExpress.XtraGrid.Columns.FixedStyle.Right;
            this.gridColumn1.Name = "gridColumn1";
            this.gridColumn1.OptionsColumn.FixedWidth = true;
            this.gridColumn1.OptionsColumn.ShowCaption = false;
            this.gridColumn1.Visible = true;
            this.gridColumn1.VisibleIndex = 16;
            this.gridColumn1.Width = 25;
            // 
            // gridView3
            // 
            this.gridView3.Appearance.ColumnFilterButton.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(243)))), ((int)(((byte)(244)))), ((int)(((byte)(250)))));
            this.gridView3.Appearance.ColumnFilterButton.BackColor2 = System.Drawing.Color.FromArgb(((int)(((byte)(154)))), ((int)(((byte)(153)))), ((int)(((byte)(182)))));
            this.gridView3.Appearance.ColumnFilterButton.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(243)))), ((int)(((byte)(244)))), ((int)(((byte)(250)))));
            this.gridView3.Appearance.ColumnFilterButton.ForeColor = System.Drawing.Color.Black;
            this.gridView3.Appearance.ColumnFilterButton.GradientMode = System.Drawing.Drawing2D.LinearGradientMode.Vertical;
            this.gridView3.Appearance.ColumnFilterButton.Options.UseBackColor = true;
            this.gridView3.Appearance.ColumnFilterButton.Options.UseBorderColor = true;
            this.gridView3.Appearance.ColumnFilterButton.Options.UseForeColor = true;
            this.gridView3.Appearance.ColumnFilterButtonActive.BackColor = System.Drawing.Color.White;
            this.gridView3.Appearance.ColumnFilterButtonActive.BackColor2 = System.Drawing.Color.FromArgb(((int)(((byte)(198)))), ((int)(((byte)(198)))), ((int)(((byte)(215)))));
            this.gridView3.Appearance.ColumnFilterButtonActive.BorderColor = System.Drawing.Color.White;
            this.gridView3.Appearance.ColumnFilterButtonActive.ForeColor = System.Drawing.Color.Black;
            this.gridView3.Appearance.ColumnFilterButtonActive.GradientMode = System.Drawing.Drawing2D.LinearGradientMode.Vertical;
            this.gridView3.Appearance.ColumnFilterButtonActive.Options.UseBackColor = true;
            this.gridView3.Appearance.ColumnFilterButtonActive.Options.UseBorderColor = true;
            this.gridView3.Appearance.ColumnFilterButtonActive.Options.UseForeColor = true;
            this.gridView3.Appearance.Empty.BackColor = System.Drawing.Color.White;
            this.gridView3.Appearance.Empty.Options.UseBackColor = true;
            this.gridView3.Appearance.EvenRow.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(243)))), ((int)(((byte)(244)))), ((int)(((byte)(250)))));
            this.gridView3.Appearance.EvenRow.ForeColor = System.Drawing.Color.Black;
            this.gridView3.Appearance.EvenRow.Options.UseBackColor = true;
            this.gridView3.Appearance.EvenRow.Options.UseForeColor = true;
            this.gridView3.Appearance.FilterCloseButton.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(243)))), ((int)(((byte)(244)))), ((int)(((byte)(250)))));
            this.gridView3.Appearance.FilterCloseButton.BackColor2 = System.Drawing.Color.FromArgb(((int)(((byte)(154)))), ((int)(((byte)(153)))), ((int)(((byte)(182)))));
            this.gridView3.Appearance.FilterCloseButton.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(243)))), ((int)(((byte)(244)))), ((int)(((byte)(250)))));
            this.gridView3.Appearance.FilterCloseButton.GradientMode = System.Drawing.Drawing2D.LinearGradientMode.Vertical;
            this.gridView3.Appearance.FilterCloseButton.Options.UseBackColor = true;
            this.gridView3.Appearance.FilterCloseButton.Options.UseBorderColor = true;
            this.gridView3.Appearance.FilterPanel.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(133)))), ((int)(((byte)(131)))), ((int)(((byte)(161)))));
            this.gridView3.Appearance.FilterPanel.ForeColor = System.Drawing.Color.White;
            this.gridView3.Appearance.FilterPanel.Options.UseBackColor = true;
            this.gridView3.Appearance.FilterPanel.Options.UseForeColor = true;
            this.gridView3.Appearance.FixedLine.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(124)))), ((int)(((byte)(124)))), ((int)(((byte)(148)))));
            this.gridView3.Appearance.FixedLine.Options.UseBackColor = true;
            this.gridView3.Appearance.FocusedRow.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(178)))), ((int)(((byte)(180)))), ((int)(((byte)(191)))));
            this.gridView3.Appearance.FocusedRow.ForeColor = System.Drawing.Color.Black;
            this.gridView3.Appearance.FocusedRow.Options.UseBackColor = true;
            this.gridView3.Appearance.FocusedRow.Options.UseForeColor = true;
            this.gridView3.Appearance.FooterPanel.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(243)))), ((int)(((byte)(244)))), ((int)(((byte)(250)))));
            this.gridView3.Appearance.FooterPanel.BackColor2 = System.Drawing.Color.FromArgb(((int)(((byte)(154)))), ((int)(((byte)(153)))), ((int)(((byte)(182)))));
            this.gridView3.Appearance.FooterPanel.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(243)))), ((int)(((byte)(244)))), ((int)(((byte)(250)))));
            this.gridView3.Appearance.FooterPanel.GradientMode = System.Drawing.Drawing2D.LinearGradientMode.Vertical;
            this.gridView3.Appearance.FooterPanel.Options.UseBackColor = true;
            this.gridView3.Appearance.FooterPanel.Options.UseBorderColor = true;
            this.gridView3.Appearance.GroupButton.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(215)))), ((int)(((byte)(215)))), ((int)(((byte)(229)))));
            this.gridView3.Appearance.GroupButton.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(215)))), ((int)(((byte)(215)))), ((int)(((byte)(229)))));
            this.gridView3.Appearance.GroupButton.ForeColor = System.Drawing.Color.Black;
            this.gridView3.Appearance.GroupButton.Options.UseBackColor = true;
            this.gridView3.Appearance.GroupButton.Options.UseBorderColor = true;
            this.gridView3.Appearance.GroupButton.Options.UseForeColor = true;
            this.gridView3.Appearance.GroupFooter.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(215)))), ((int)(((byte)(215)))), ((int)(((byte)(229)))));
            this.gridView3.Appearance.GroupFooter.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(215)))), ((int)(((byte)(215)))), ((int)(((byte)(229)))));
            this.gridView3.Appearance.GroupFooter.ForeColor = System.Drawing.Color.Black;
            this.gridView3.Appearance.GroupFooter.Options.UseBackColor = true;
            this.gridView3.Appearance.GroupFooter.Options.UseBorderColor = true;
            this.gridView3.Appearance.GroupFooter.Options.UseForeColor = true;
            this.gridView3.Appearance.GroupPanel.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(133)))), ((int)(((byte)(131)))), ((int)(((byte)(161)))));
            this.gridView3.Appearance.GroupPanel.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(243)))), ((int)(((byte)(244)))), ((int)(((byte)(250)))));
            this.gridView3.Appearance.GroupPanel.Options.UseBackColor = true;
            this.gridView3.Appearance.GroupPanel.Options.UseForeColor = true;
            this.gridView3.Appearance.GroupRow.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(215)))), ((int)(((byte)(215)))), ((int)(((byte)(229)))));
            this.gridView3.Appearance.GroupRow.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(215)))), ((int)(((byte)(215)))), ((int)(((byte)(229)))));
            this.gridView3.Appearance.GroupRow.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Bold);
            this.gridView3.Appearance.GroupRow.ForeColor = System.Drawing.Color.Black;
            this.gridView3.Appearance.GroupRow.Options.UseBackColor = true;
            this.gridView3.Appearance.GroupRow.Options.UseBorderColor = true;
            this.gridView3.Appearance.GroupRow.Options.UseFont = true;
            this.gridView3.Appearance.GroupRow.Options.UseForeColor = true;
            this.gridView3.Appearance.HeaderPanel.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(243)))), ((int)(((byte)(244)))), ((int)(((byte)(250)))));
            this.gridView3.Appearance.HeaderPanel.BackColor2 = System.Drawing.Color.FromArgb(((int)(((byte)(154)))), ((int)(((byte)(153)))), ((int)(((byte)(182)))));
            this.gridView3.Appearance.HeaderPanel.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(243)))), ((int)(((byte)(244)))), ((int)(((byte)(250)))));
            this.gridView3.Appearance.HeaderPanel.ForeColor = System.Drawing.Color.Black;
            this.gridView3.Appearance.HeaderPanel.GradientMode = System.Drawing.Drawing2D.LinearGradientMode.Vertical;
            this.gridView3.Appearance.HeaderPanel.Options.UseBackColor = true;
            this.gridView3.Appearance.HeaderPanel.Options.UseBorderColor = true;
            this.gridView3.Appearance.HeaderPanel.Options.UseForeColor = true;
            this.gridView3.Appearance.HideSelectionRow.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(218)))), ((int)(((byte)(219)))), ((int)(((byte)(226)))));
            this.gridView3.Appearance.HideSelectionRow.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(133)))), ((int)(((byte)(131)))), ((int)(((byte)(161)))));
            this.gridView3.Appearance.HideSelectionRow.Options.UseBackColor = true;
            this.gridView3.Appearance.HideSelectionRow.Options.UseForeColor = true;
            this.gridView3.Appearance.HorzLine.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(164)))), ((int)(((byte)(164)))), ((int)(((byte)(188)))));
            this.gridView3.Appearance.HorzLine.Options.UseBackColor = true;
            this.gridView3.Appearance.OddRow.BackColor = System.Drawing.Color.White;
            this.gridView3.Appearance.OddRow.ForeColor = System.Drawing.Color.Black;
            this.gridView3.Appearance.OddRow.Options.UseBackColor = true;
            this.gridView3.Appearance.OddRow.Options.UseForeColor = true;
            this.gridView3.Appearance.Preview.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(250)))), ((int)(((byte)(250)))), ((int)(((byte)(253)))));
            this.gridView3.Appearance.Preview.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(163)))), ((int)(((byte)(165)))), ((int)(((byte)(177)))));
            this.gridView3.Appearance.Preview.Options.UseBackColor = true;
            this.gridView3.Appearance.Preview.Options.UseForeColor = true;
            this.gridView3.Appearance.Row.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(243)))), ((int)(((byte)(244)))), ((int)(((byte)(250)))));
            this.gridView3.Appearance.Row.ForeColor = System.Drawing.Color.Black;
            this.gridView3.Appearance.Row.Options.UseBackColor = true;
            this.gridView3.Appearance.Row.Options.UseForeColor = true;
            this.gridView3.Appearance.RowSeparator.BackColor = System.Drawing.Color.White;
            this.gridView3.Appearance.RowSeparator.Options.UseBackColor = true;
            this.gridView3.Appearance.SelectedRow.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(195)))), ((int)(((byte)(197)))), ((int)(((byte)(205)))));
            this.gridView3.Appearance.SelectedRow.ForeColor = System.Drawing.Color.Black;
            this.gridView3.Appearance.SelectedRow.Options.UseBackColor = true;
            this.gridView3.Appearance.SelectedRow.Options.UseForeColor = true;
            this.gridView3.Appearance.VertLine.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(164)))), ((int)(((byte)(164)))), ((int)(((byte)(188)))));
            this.gridView3.Appearance.VertLine.Options.UseBackColor = true;
            this.gridView3.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colCodProd1,
            this.colDataI,
            this.colDataF,
            this.colNCM,
            this.colDescricao,
            this.colUnidade,
            this.colAliquotaIPI,
            this.colAliquotaICMS,
            this.colRedBaseICMS,
            this.colBaseICMSST});
            this.gridView3.GridControl = this.gridControl1;
            this.gridView3.Name = "gridView3";
            this.gridView3.OptionsView.ColumnAutoWidth = false;
            this.gridView3.OptionsView.NewItemRowPosition = DevExpress.XtraGrid.Views.Grid.NewItemRowPosition.Bottom;
            this.gridView3.OptionsView.ShowGroupPanel = false;
            this.gridView3.ViewCaption = "Dados do produto";
            // 
            // colCodProd1
            // 
            this.colCodProd1.Caption = "CodProd";
            this.colCodProd1.FieldName = "CodProd";
            this.colCodProd1.Name = "colCodProd1";
            this.colCodProd1.OptionsColumn.ReadOnly = true;
            // 
            // colDataI
            // 
            this.colDataI.Caption = "DataI";
            this.colDataI.FieldName = "DataI";
            this.colDataI.Name = "colDataI";
            this.colDataI.Visible = true;
            this.colDataI.VisibleIndex = 0;
            // 
            // colDataF
            // 
            this.colDataF.Caption = "DataF";
            this.colDataF.FieldName = "DataF";
            this.colDataF.Name = "colDataF";
            this.colDataF.Visible = true;
            this.colDataF.VisibleIndex = 1;
            // 
            // colNCM
            // 
            this.colNCM.Caption = "NCM";
            this.colNCM.FieldName = "NCM";
            this.colNCM.Name = "colNCM";
            this.colNCM.Visible = true;
            this.colNCM.VisibleIndex = 2;
            // 
            // colDescricao
            // 
            this.colDescricao.Caption = "Descricao";
            this.colDescricao.FieldName = "Descricao";
            this.colDescricao.Name = "colDescricao";
            this.colDescricao.Visible = true;
            this.colDescricao.VisibleIndex = 3;
            this.colDescricao.Width = 229;
            // 
            // colUnidade
            // 
            this.colUnidade.Caption = "Unidade";
            this.colUnidade.FieldName = "Unidade";
            this.colUnidade.Name = "colUnidade";
            this.colUnidade.Visible = true;
            this.colUnidade.VisibleIndex = 4;
            // 
            // colAliquotaIPI
            // 
            this.colAliquotaIPI.Caption = "AliquotaIPI";
            this.colAliquotaIPI.FieldName = "AliquotaIPI";
            this.colAliquotaIPI.Name = "colAliquotaIPI";
            this.colAliquotaIPI.Visible = true;
            this.colAliquotaIPI.VisibleIndex = 5;
            // 
            // colAliquotaICMS
            // 
            this.colAliquotaICMS.Caption = "AliquotaICMS";
            this.colAliquotaICMS.FieldName = "AliquotaICMS";
            this.colAliquotaICMS.Name = "colAliquotaICMS";
            this.colAliquotaICMS.Visible = true;
            this.colAliquotaICMS.VisibleIndex = 6;
            // 
            // colRedBaseICMS
            // 
            this.colRedBaseICMS.Caption = "RedBaseICMS";
            this.colRedBaseICMS.FieldName = "RedBaseICMS";
            this.colRedBaseICMS.Name = "colRedBaseICMS";
            this.colRedBaseICMS.Visible = true;
            this.colRedBaseICMS.VisibleIndex = 7;
            // 
            // colBaseICMSST
            // 
            this.colBaseICMSST.Caption = "BaseICMSST";
            this.colBaseICMSST.FieldName = "BaseICMSST";
            this.colBaseICMSST.Name = "colBaseICMSST";
            this.colBaseICMSST.Visible = true;
            this.colBaseICMSST.VisibleIndex = 8;
            // 
            // xtraTabControl1
            // 
            this.xtraTabControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.xtraTabControl1.Location = new System.Drawing.Point(0, 48);
            this.xtraTabControl1.Name = "xtraTabControl1";
            this.xtraTabControl1.SelectedTabPage = this.xtraTabPage2;
            this.xtraTabControl1.Size = new System.Drawing.Size(974, 430);
            this.xtraTabControl1.TabIndex = 0;
            this.xtraTabControl1.TabPages.AddRange(new DevExpress.XtraTab.XtraTabPage[] {
            this.xtraTabPage2,
            this.PageNotas,
            this.xtraTabPage1});
            // 
            // xtraTabPage2
            // 
            this.xtraTabPage2.Controls.Add(this.gridControl2);
            this.xtraTabPage2.Name = "xtraTabPage2";
            this.xtraTabPage2.Size = new System.Drawing.Size(967, 401);
            this.xtraTabPage2.Text = "Capa";
            // 
            // gridControl2
            // 
            this.gridControl2.DataSource = this.empresa1011BindingSource;
            this.gridControl2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridControl2.Location = new System.Drawing.Point(0, 0);
            this.gridControl2.MainView = this.cardView1;
            this.gridControl2.Name = "gridControl2";
            this.gridControl2.Size = new System.Drawing.Size(967, 401);
            this.gridControl2.TabIndex = 0;
            this.gridControl2.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.cardView1});
            // 
            // empresa1011BindingSource
            // 
            this.empresa1011BindingSource.DataMember = "Empresa10_11";
            this.empresa1011BindingSource.DataSource = this.bindingSourceDados;
            // 
            // cardView1
            // 
            this.cardView1.Appearance.CardCaption.BackColor = System.Drawing.Color.Orange;
            this.cardView1.Appearance.CardCaption.BorderColor = System.Drawing.Color.Orange;
            this.cardView1.Appearance.CardCaption.Options.UseBackColor = true;
            this.cardView1.Appearance.CardCaption.Options.UseBorderColor = true;
            this.cardView1.Appearance.EmptySpace.BackColor = System.Drawing.Color.LightSkyBlue;
            this.cardView1.Appearance.EmptySpace.BackColor2 = System.Drawing.Color.SkyBlue;
            this.cardView1.Appearance.EmptySpace.GradientMode = System.Drawing.Drawing2D.LinearGradientMode.BackwardDiagonal;
            this.cardView1.Appearance.EmptySpace.Options.UseBackColor = true;
            this.cardView1.Appearance.FieldCaption.BackColor = System.Drawing.Color.Linen;
            this.cardView1.Appearance.FieldCaption.BackColor2 = System.Drawing.Color.AntiqueWhite;
            this.cardView1.Appearance.FieldCaption.Options.UseBackColor = true;
            this.cardView1.Appearance.FieldValue.BackColor = System.Drawing.Color.White;
            this.cardView1.Appearance.FieldValue.Options.UseBackColor = true;
            this.cardView1.Appearance.FilterCloseButton.BackColor = System.Drawing.Color.Orange;
            this.cardView1.Appearance.FilterCloseButton.BorderColor = System.Drawing.Color.Orange;
            this.cardView1.Appearance.FilterCloseButton.Options.UseBackColor = true;
            this.cardView1.Appearance.FilterCloseButton.Options.UseBorderColor = true;
            this.cardView1.Appearance.FilterPanel.BackColor = System.Drawing.Color.DarkOrange;
            this.cardView1.Appearance.FilterPanel.BackColor2 = System.Drawing.Color.Orange;
            this.cardView1.Appearance.FilterPanel.ForeColor = System.Drawing.Color.Black;
            this.cardView1.Appearance.FilterPanel.GradientMode = System.Drawing.Drawing2D.LinearGradientMode.BackwardDiagonal;
            this.cardView1.Appearance.FilterPanel.Options.UseBackColor = true;
            this.cardView1.Appearance.FilterPanel.Options.UseForeColor = true;
            this.cardView1.Appearance.FocusedCardCaption.BackColor = System.Drawing.Color.RoyalBlue;
            this.cardView1.Appearance.FocusedCardCaption.BackColor2 = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this.cardView1.Appearance.FocusedCardCaption.BorderColor = System.Drawing.Color.RoyalBlue;
            this.cardView1.Appearance.FocusedCardCaption.ForeColor = System.Drawing.Color.White;
            this.cardView1.Appearance.FocusedCardCaption.GradientMode = System.Drawing.Drawing2D.LinearGradientMode.Vertical;
            this.cardView1.Appearance.FocusedCardCaption.Options.UseBackColor = true;
            this.cardView1.Appearance.FocusedCardCaption.Options.UseBorderColor = true;
            this.cardView1.Appearance.FocusedCardCaption.Options.UseForeColor = true;
            this.cardView1.Appearance.HideSelectionCardCaption.BackColor = System.Drawing.Color.LightSlateGray;
            this.cardView1.Appearance.HideSelectionCardCaption.BorderColor = System.Drawing.Color.LightSlateGray;
            this.cardView1.Appearance.HideSelectionCardCaption.Options.UseBackColor = true;
            this.cardView1.Appearance.HideSelectionCardCaption.Options.UseBorderColor = true;
            this.cardView1.Appearance.SeparatorLine.BackColor = System.Drawing.Color.Tan;
            this.cardView1.Appearance.SeparatorLine.Options.UseBackColor = true;
            this.cardView1.CardInterval = 4;
            this.cardView1.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colCNPJ2,
            this.colIE1,
            this.colNome,
            this.colCidade,
            this.colUF1,
            this.colFAX,
            this.colDataIni,
            this.colDataFim,
            this.colLogradouro,
            this.colNumero2,
            this.colComplemento,
            this.colBairro,
            this.colCEP,
            this.colContato,
            this.colTelefone});
            this.cardView1.FocusedCardTopFieldIndex = 0;
            this.cardView1.GridControl = this.gridControl2;
            this.cardView1.MaximumCardColumns = 1;
            this.cardView1.MaximumCardRows = 1;
            this.cardView1.Name = "cardView1";
            this.cardView1.OptionsBehavior.FieldAutoHeight = true;
            this.cardView1.OptionsView.ShowCardCaption = false;
            this.cardView1.OptionsView.ShowCardExpandButton = false;
            this.cardView1.OptionsView.ShowQuickCustomizeButton = false;
            this.cardView1.VertScrollVisibility = DevExpress.XtraGrid.Views.Base.ScrollVisibility.Auto;
            // 
            // colCNPJ2
            // 
            this.colCNPJ2.Caption = "CNPJ";
            this.colCNPJ2.FieldName = "CNPJ";
            this.colCNPJ2.Name = "colCNPJ2";
            this.colCNPJ2.Visible = true;
            this.colCNPJ2.VisibleIndex = 0;
            // 
            // colIE1
            // 
            this.colIE1.Caption = "IE";
            this.colIE1.FieldName = "IE";
            this.colIE1.Name = "colIE1";
            this.colIE1.Visible = true;
            this.colIE1.VisibleIndex = 1;
            // 
            // colNome
            // 
            this.colNome.Caption = "Nome";
            this.colNome.FieldName = "Nome";
            this.colNome.Name = "colNome";
            this.colNome.Visible = true;
            this.colNome.VisibleIndex = 2;
            // 
            // colCidade
            // 
            this.colCidade.Caption = "Cidade";
            this.colCidade.FieldName = "Cidade";
            this.colCidade.Name = "colCidade";
            this.colCidade.Visible = true;
            this.colCidade.VisibleIndex = 3;
            // 
            // colUF1
            // 
            this.colUF1.Caption = "UF";
            this.colUF1.FieldName = "UF";
            this.colUF1.Name = "colUF1";
            this.colUF1.Visible = true;
            this.colUF1.VisibleIndex = 4;
            // 
            // colFAX
            // 
            this.colFAX.Caption = "FAX";
            this.colFAX.FieldName = "FAX";
            this.colFAX.Name = "colFAX";
            this.colFAX.Visible = true;
            this.colFAX.VisibleIndex = 5;
            // 
            // colDataIni
            // 
            this.colDataIni.Caption = "DataIni";
            this.colDataIni.FieldName = "DataIni";
            this.colDataIni.Name = "colDataIni";
            this.colDataIni.Visible = true;
            this.colDataIni.VisibleIndex = 6;
            // 
            // colDataFim
            // 
            this.colDataFim.Caption = "DataFim";
            this.colDataFim.FieldName = "DataFim";
            this.colDataFim.Name = "colDataFim";
            this.colDataFim.Visible = true;
            this.colDataFim.VisibleIndex = 7;
            // 
            // colLogradouro
            // 
            this.colLogradouro.Caption = "Logradouro";
            this.colLogradouro.FieldName = "Logradouro";
            this.colLogradouro.Name = "colLogradouro";
            this.colLogradouro.Visible = true;
            this.colLogradouro.VisibleIndex = 8;
            // 
            // colNumero2
            // 
            this.colNumero2.Caption = "Numero";
            this.colNumero2.FieldName = "Numero";
            this.colNumero2.Name = "colNumero2";
            this.colNumero2.Visible = true;
            this.colNumero2.VisibleIndex = 9;
            // 
            // colComplemento
            // 
            this.colComplemento.Caption = "Complemento";
            this.colComplemento.FieldName = "Complemento";
            this.colComplemento.Name = "colComplemento";
            this.colComplemento.Visible = true;
            this.colComplemento.VisibleIndex = 10;
            // 
            // colBairro
            // 
            this.colBairro.Caption = "Bairro";
            this.colBairro.FieldName = "Bairro";
            this.colBairro.Name = "colBairro";
            this.colBairro.Visible = true;
            this.colBairro.VisibleIndex = 11;
            // 
            // colCEP
            // 
            this.colCEP.Caption = "CEP";
            this.colCEP.FieldName = "CEP";
            this.colCEP.Name = "colCEP";
            this.colCEP.Visible = true;
            this.colCEP.VisibleIndex = 12;
            // 
            // colContato
            // 
            this.colContato.Caption = "Contato";
            this.colContato.FieldName = "Contato";
            this.colContato.Name = "colContato";
            this.colContato.Visible = true;
            this.colContato.VisibleIndex = 13;
            // 
            // colTelefone
            // 
            this.colTelefone.Caption = "Telefone";
            this.colTelefone.FieldName = "Telefone";
            this.colTelefone.Name = "colTelefone";
            this.colTelefone.Visible = true;
            this.colTelefone.VisibleIndex = 14;
            // 
            // PageNotas
            // 
            this.PageNotas.Controls.Add(this.gridControl1);
            this.PageNotas.Name = "PageNotas";
            this.PageNotas.Size = new System.Drawing.Size(967, 401);
            this.PageNotas.Text = "Notas";
            // 
            // xtraTabPage1
            // 
            this.xtraTabPage1.Controls.Add(this.gridControl3);
            this.xtraTabPage1.Name = "xtraTabPage1";
            this.xtraTabPage1.Size = new System.Drawing.Size(967, 401);
            this.xtraTabPage1.Text = "Produtos";
            // 
            // gridControl3
            // 
            this.gridControl3.DataMember = "Produtos75";
            this.gridControl3.DataSource = this.bindingSourceDados;
            this.gridControl3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridControl3.Location = new System.Drawing.Point(0, 0);
            this.gridControl3.MainView = this.gridView5;
            this.gridControl3.Name = "gridControl3";
            this.gridControl3.Size = new System.Drawing.Size(967, 401);
            this.gridControl3.TabIndex = 1;
            this.gridControl3.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridView5});
            // 
            // gridView5
            // 
            this.gridView5.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.gridColumn16,
            this.gridColumn17,
            this.gridColumn18,
            this.gridColumn19,
            this.gridColumn20,
            this.gridColumn21,
            this.gridColumn22,
            this.gridColumn23,
            this.gridColumn24,
            this.gridColumn25});
            this.gridView5.GridControl = this.gridControl3;
            this.gridView5.Name = "gridView5";
            this.gridView5.OptionsView.NewItemRowPosition = DevExpress.XtraGrid.Views.Grid.NewItemRowPosition.Bottom;
            this.gridView5.OptionsView.ShowGroupPanel = false;
            this.gridView5.ViewCaption = "Dados do produto";
            // 
            // gridColumn16
            // 
            this.gridColumn16.Caption = "CodProd";
            this.gridColumn16.FieldName = "CodProd";
            this.gridColumn16.Name = "gridColumn16";
            this.gridColumn16.OptionsColumn.ReadOnly = true;
            this.gridColumn16.Visible = true;
            this.gridColumn16.VisibleIndex = 0;
            // 
            // gridColumn17
            // 
            this.gridColumn17.Caption = "DataI";
            this.gridColumn17.FieldName = "DataI";
            this.gridColumn17.Name = "gridColumn17";
            this.gridColumn17.Visible = true;
            this.gridColumn17.VisibleIndex = 1;
            // 
            // gridColumn18
            // 
            this.gridColumn18.Caption = "DataF";
            this.gridColumn18.FieldName = "DataF";
            this.gridColumn18.Name = "gridColumn18";
            this.gridColumn18.Visible = true;
            this.gridColumn18.VisibleIndex = 2;
            // 
            // gridColumn19
            // 
            this.gridColumn19.Caption = "NCM";
            this.gridColumn19.FieldName = "NCM";
            this.gridColumn19.Name = "gridColumn19";
            this.gridColumn19.Visible = true;
            this.gridColumn19.VisibleIndex = 3;
            // 
            // gridColumn20
            // 
            this.gridColumn20.Caption = "Descricao";
            this.gridColumn20.FieldName = "Descricao";
            this.gridColumn20.Name = "gridColumn20";
            this.gridColumn20.Visible = true;
            this.gridColumn20.VisibleIndex = 4;
            // 
            // gridColumn21
            // 
            this.gridColumn21.Caption = "Unidade";
            this.gridColumn21.FieldName = "Unidade";
            this.gridColumn21.Name = "gridColumn21";
            this.gridColumn21.Visible = true;
            this.gridColumn21.VisibleIndex = 5;
            // 
            // gridColumn22
            // 
            this.gridColumn22.Caption = "AliquotaIPI";
            this.gridColumn22.FieldName = "AliquotaIPI";
            this.gridColumn22.Name = "gridColumn22";
            this.gridColumn22.Visible = true;
            this.gridColumn22.VisibleIndex = 6;
            // 
            // gridColumn23
            // 
            this.gridColumn23.Caption = "AliquotaICMS";
            this.gridColumn23.FieldName = "AliquotaICMS";
            this.gridColumn23.Name = "gridColumn23";
            this.gridColumn23.Visible = true;
            this.gridColumn23.VisibleIndex = 7;
            // 
            // gridColumn24
            // 
            this.gridColumn24.Caption = "RedBaseICMS";
            this.gridColumn24.FieldName = "RedBaseICMS";
            this.gridColumn24.Name = "gridColumn24";
            this.gridColumn24.Visible = true;
            this.gridColumn24.VisibleIndex = 8;
            // 
            // gridColumn25
            // 
            this.gridColumn25.Caption = "BaseICMSST";
            this.gridColumn25.FieldName = "BaseICMSST";
            this.gridColumn25.Name = "gridColumn25";
            this.gridColumn25.Visible = true;
            this.gridColumn25.VisibleIndex = 9;
            // 
            // panelControl1
            // 
            this.panelControl1.Controls.Add(this.simpleButton3);
            this.panelControl1.Controls.Add(this.simpleButton2);
            this.panelControl1.Controls.Add(this.simpleButton1);
            this.panelControl1.Dock = System.Windows.Forms.DockStyle.Top;
            this.panelControl1.Location = new System.Drawing.Point(0, 0);
            this.panelControl1.Name = "panelControl1";
            this.panelControl1.Size = new System.Drawing.Size(974, 48);
            this.panelControl1.TabIndex = 1;
            // 
            // simpleButton3
            // 
            this.simpleButton3.Location = new System.Drawing.Point(485, 3);
            this.simpleButton3.Name = "simpleButton3";
            this.simpleButton3.Size = new System.Drawing.Size(234, 23);
            this.simpleButton3.TabIndex = 2;
            this.simpleButton3.Text = "Gera Arquivo Sintegra";
            this.simpleButton3.Click += new System.EventHandler(this.simpleButton3_Click);
            // 
            // simpleButton2
            // 
            this.simpleButton2.Location = new System.Drawing.Point(245, 5);
            this.simpleButton2.Name = "simpleButton2";
            this.simpleButton2.Size = new System.Drawing.Size(234, 23);
            this.simpleButton2.TabIndex = 1;
            this.simpleButton2.Text = "Carregar XML";
            this.simpleButton2.Click += new System.EventHandler(this.simpleButton2_Click);
            // 
            // simpleButton1
            // 
            this.simpleButton1.Location = new System.Drawing.Point(5, 5);
            this.simpleButton1.Name = "simpleButton1";
            this.simpleButton1.Size = new System.Drawing.Size(234, 23);
            this.simpleButton1.TabIndex = 0;
            this.simpleButton1.Text = "Salvar em XML";
            this.simpleButton1.Click += new System.EventHandler(this.simpleButton1_Click);
            // 
            // cEditaDados
            // 
            this.Controls.Add(this.xtraTabControl1);
            this.Controls.Add(this.panelControl1);
            this.Doc = System.Windows.Forms.DockStyle.Fill;
            this.LookAndFeel.UseDefaultLookAndFeel = false;
            this.Name = "cEditaDados";
            this.Size = new System.Drawing.Size(974, 478);
            ((System.ComponentModel.ISupportInitialize)(this.StyleController_F)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryDEL)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.notas50BindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.bindingSourceDados)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xtraTabControl1)).EndInit();
            this.xtraTabControl1.ResumeLayout(false);
            this.xtraTabPage2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gridControl2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.empresa1011BindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cardView1)).EndInit();
            this.PageNotas.ResumeLayout(false);
            this.xtraTabPage1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gridControl3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl1)).EndInit();
            this.panelControl1.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraTab.XtraTabControl xtraTabControl1;
        private DevExpress.XtraTab.XtraTabPage PageNotas;
        private DevExpress.XtraTab.XtraTabPage xtraTabPage2;
        private System.Windows.Forms.BindingSource notas50BindingSource;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView1;
        private DevExpress.XtraGrid.Columns.GridColumn colCNPJ;
        private DevExpress.XtraGrid.Columns.GridColumn colIE;
        private DevExpress.XtraGrid.Columns.GridColumn colDataNota;
        private DevExpress.XtraGrid.Columns.GridColumn colUF;
        private DevExpress.XtraGrid.Columns.GridColumn colModelo;
        private DevExpress.XtraGrid.Columns.GridColumn colSerie;
        private DevExpress.XtraGrid.Columns.GridColumn colNumero;
        private DevExpress.XtraGrid.Columns.GridColumn colCFOP;
        private DevExpress.XtraGrid.Columns.GridColumn colValorTotal;
        private DevExpress.XtraGrid.Columns.GridColumn colBaseICMS;
        private DevExpress.XtraGrid.Columns.GridColumn colValorICMS;
        private DevExpress.XtraGrid.Columns.GridColumn colValorIsentaNT;
        private DevExpress.XtraGrid.Columns.GridColumn colValorOutras;
        private DevExpress.XtraGrid.Columns.GridColumn colAliquota;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView2;
        private DevExpress.XtraGrid.Columns.GridColumn colCNPJ1;
        private DevExpress.XtraGrid.Columns.GridColumn colModelo1;
        private DevExpress.XtraGrid.Columns.GridColumn colSerie1;
        private DevExpress.XtraGrid.Columns.GridColumn colNumero1;
        private DevExpress.XtraGrid.Columns.GridColumn colCFOP1;
        private DevExpress.XtraGrid.Columns.GridColumn colCST;
        private DevExpress.XtraGrid.Columns.GridColumn colN;
        private DevExpress.XtraGrid.Columns.GridColumn colCodProd;
        private DevExpress.XtraGrid.Columns.GridColumn colQuantidade;
        private DevExpress.XtraGrid.Columns.GridColumn colValorTotal1;
        private DevExpress.XtraGrid.Columns.GridColumn colValorDesconto;
        private DevExpress.XtraGrid.Columns.GridColumn colBaseICMS1;
        private DevExpress.XtraGrid.Columns.GridColumn colBaseSubT;
        private DevExpress.XtraGrid.Columns.GridColumn colValorIPI;
        private DevExpress.XtraGrid.Columns.GridColumn colAliquota1;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView3;
        private DevExpress.XtraGrid.Columns.GridColumn colCodProd1;
        private DevExpress.XtraGrid.Columns.GridColumn colDataI;
        private DevExpress.XtraGrid.Columns.GridColumn colDataF;
        private DevExpress.XtraGrid.Columns.GridColumn colNCM;
        private DevExpress.XtraGrid.Columns.GridColumn colDescricao;
        private DevExpress.XtraGrid.Columns.GridColumn colUnidade;
        private DevExpress.XtraGrid.Columns.GridColumn colAliquotaIPI;
        private DevExpress.XtraGrid.Columns.GridColumn colAliquotaICMS;
        private DevExpress.XtraGrid.Columns.GridColumn colRedBaseICMS;
        private DevExpress.XtraGrid.Columns.GridColumn colBaseICMSST;
        public DevExpress.XtraEditors.PanelControl panelControl1;
        public DevExpress.XtraGrid.GridControl gridControl1;
        private DevExpress.XtraEditors.SimpleButton simpleButton2;
        private DevExpress.XtraEditors.SimpleButton simpleButton1;
        private System.Windows.Forms.BindingSource bindingSourceDados;
        private DevExpress.XtraEditors.SimpleButton simpleButton3;
        private DevExpress.XtraGrid.GridControl gridControl2;
        private System.Windows.Forms.BindingSource empresa1011BindingSource;
        private DevExpress.XtraGrid.Views.Card.CardView cardView1;
        private DevExpress.XtraGrid.Columns.GridColumn colCNPJ2;
        private DevExpress.XtraGrid.Columns.GridColumn colIE1;
        private DevExpress.XtraGrid.Columns.GridColumn colNome;
        private DevExpress.XtraGrid.Columns.GridColumn colCidade;
        private DevExpress.XtraGrid.Columns.GridColumn colUF1;
        private DevExpress.XtraGrid.Columns.GridColumn colFAX;
        private DevExpress.XtraGrid.Columns.GridColumn colDataIni;
        private DevExpress.XtraGrid.Columns.GridColumn colDataFim;
        private DevExpress.XtraGrid.Columns.GridColumn colLogradouro;
        private DevExpress.XtraGrid.Columns.GridColumn colNumero2;
        private DevExpress.XtraGrid.Columns.GridColumn colComplemento;
        private DevExpress.XtraGrid.Columns.GridColumn colBairro;
        private DevExpress.XtraGrid.Columns.GridColumn colCEP;
        private DevExpress.XtraGrid.Columns.GridColumn colContato;
        private DevExpress.XtraGrid.Columns.GridColumn colTelefone;
        private DevExpress.XtraTab.XtraTabPage xtraTabPage1;
        public DevExpress.XtraGrid.GridControl gridControl3;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView5;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn16;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn17;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn18;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn19;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn20;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn21;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn22;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn23;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn24;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn25;
        private DevExpress.XtraGrid.Columns.GridColumn colCodFornecedor;
        private DevExpress.XtraGrid.Columns.GridColumn colnumeroPedido;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn1;
        private DevExpress.XtraEditors.Repository.RepositoryItemButtonEdit repositoryDEL;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn2;        
    }
}
