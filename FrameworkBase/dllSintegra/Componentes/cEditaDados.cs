using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace dllSintegra.Componentes
{
    public partial class cEditaDados : CompontesBasicos.ComponenteBase
    {
        public cEditaDados()
        {
            InitializeComponent();
        }

        private GeraSintegra _GS;

        public GeraSintegra GS {
            get {
                if (_GS == null)
                    GS = new GeraSintegra("");
                return _GS;
            }
            set {
                _GS = value;
                bindingSourceDados.DataSource = _GS.DDados;
            }
        }

        private void simpleButton1_Click(object sender, EventArgs e)
        {            
            System.Windows.Forms.SaveFileDialog Dia = new SaveFileDialog();
            Dia.DefaultExt = ".xls";
            Dia.Filter = "Arquivo XML|*.xls";
            Dia.FilterIndex = 0;
            if (Dia.ShowDialog() == DialogResult.OK) {
                GS.DDados.WriteXml(Dia.FileName);
            }
        }

        private void simpleButton2_Click(object sender, EventArgs e)
        {
            System.Windows.Forms.OpenFileDialog Dia = new OpenFileDialog();
            Dia.DefaultExt = ".xls";
            Dia.Filter = "Arquivo XML|*.xls";
            Dia.FilterIndex = 0;
            if (Dia.ShowDialog() == DialogResult.OK)
            {
                foreach (DataTable T in GS.DDados.Tables)
                    T.Clear();
                GS.DDados.ReadXml(Dia.FileName);
            }
        }

        private void simpleButton3_Click(object sender, EventArgs e)
        {
            if (GS.Grava())
                MessageBox.Show("Gravado");
        }

        private void DelItem(dDados.Itens54Row Iten)
        {
            if (Iten == null)
                return;
            dDados.Produtos75Row Prod = Iten.GetProdutos75Rows()[0];
            Iten.Delete();
            foreach (dDados.Itens54Row rowB in GS.DDados.Itens54)
                if (rowB.RowState != DataRowState.Deleted)
                    if (rowB.CodProd == Prod.CodProd)
                        return;
            Prod.Delete();
        }


        private void repositoryDEL_ButtonClick(object sender, DevExpress.XtraEditors.Controls.ButtonPressedEventArgs e)
        {

            DevExpress.XtraGrid.Views.Grid.GridView View = (DevExpress.XtraGrid.Views.Grid.GridView)gridControl1.FocusedView;

            if (View != null)
            {
                if (View.Name == "gridView2")
                {
                    dDados.Itens54Row Iten = (dDados.Itens54Row)View.GetFocusedDataRow();
                    DelItem(Iten);
                }
                else if (View == gridView1) {
                    dDados.Notas50Row LinhaNota = (dDados.Notas50Row)gridView1.GetFocusedDataRow();
                    if (LinhaNota != null)
                    {
                        dDados.Itens54Row[] Itens = LinhaNota.GetItens54Rows();
                        foreach (dDados.Itens54Row Iten in Itens)
                            DelItem(Iten);
                        LinhaNota.Delete();
                    }
                }
            }
            /*

            if (sender == gridView1)
            {
                dDados.Notas50Row LinhaNota = (dDados.Notas50Row)gridView1.GetFocusedDataRow();
                if (LinhaNota != null)
                {
                    dDados.Itens54Row[] Itens = LinhaNota.GetItens54Rows();
                    foreach (dDados.Itens54Row Iten in Itens)
                        Iten.Delete();
                    LinhaNota.Delete();
                }
            }
            else {

                DevExpress.XtraGrid.Views.Grid.GridView View = (DevExpress.XtraGrid.Views.Grid.GridView)(gridView1.GetDetailView(gridView1.FocusedRowHandle, 0));
                if (View != null)
                {
                    dDados.Itens54Row Iten = (dDados.Itens54Row)View.GetFocusedDataRow();
                    if (Iten != null)
                        Iten.Delete();
                }
            }
             */ 
        }
    }
}

