﻿using System;
using System.Windows.Forms;
using CodBar;

namespace dllConfigImpressoras
{
    /// <summary>
    /// Configura impressora térmica
    /// </summary>
    public partial class cConfImpCopia : CompontesBasicos.ComponenteBase
    {
        private bool Travado = true;

        /// <summary>
        /// Construtor
        /// </summary>
        public cConfImpCopia()
        {
            InitializeComponent();
            radioGroup1.Properties.Items.Clear();
            radioGroup1.Properties.Items.Add(new DevExpress.XtraEditors.Controls.RadioGroupItem(CodBar.LinguagemCodBar.EPL2, "Zebra",true));
            radioGroup1.Properties.Items.Add(new DevExpress.XtraEditors.Controls.RadioGroupItem(CodBar.LinguagemCodBar.DPL, "DataMax", true));
            radioGroup1.Properties.Items.Add(new DevExpress.XtraEditors.Controls.RadioGroupItem(CodBar.LinguagemCodBar.PM42, "Bematech", false));
            radioGroup1.Properties.Items.Add(new DevExpress.XtraEditors.Controls.RadioGroupItem(CodBar.LinguagemCodBar.Windows, "Windows", true));
            textImpressora.Text = CompontesBasicos.dEstacao.dEstacaoSt.GetValor("Impressora de cópias de Cheque");            
            try
            {
                radioGroup1.EditValue = Enum.Parse(typeof(CodBar.LinguagemCodBar), CompontesBasicos.dEstacao.dEstacaoSt.GetValor("Linguagem codbar"));
            }
            catch
            {
                radioGroup1.EditValue = CodBar.LinguagemCodBar.Indefinida;
            }
            //BGabarito.Visible = (!CompontesBasicos.FormPrincipalBase.strEmProducao || CompontesBasicos.FormPrincipalBase.USULogado == 30);
            cb_CorteAutomatico.Checked = CompontesBasicos.dEstacao.dEstacaoSt.GetValor("Corte Automatico", true);
            Travado = false;
        }

        private void simpleButton3_Click(object sender, EventArgs e)
        {
            PrintDialog PD = new PrintDialog();
            if (PD.ShowDialog() == DialogResult.OK)
            {
                textImpressora.Text = PD.PrinterSettings.PrinterName;
                CompontesBasicos.dEstacao.dEstacaoSt.SetValor("Impressora de cópias de Cheque", textImpressora.Text);                
                Impress._NomeImpressoraSt = textImpressora.Text;
            }
            else
            {
                if (!CompontesBasicosProc.FuncoesBasicasProc.EstaEmProducao || CompontesBasicos.FormPrincipalBase.USULogado == 30)
                    textImpressora.Text = "TELA";
            }         
        }

        /*
        private CodBar.Impress _Impress;

        private CodBar.Impress Impress1
        {
            get
            {
                if(_Impress == null)
                {
                    CodBar.LinguagemCodBar Linguagem = (CodBar.LinguagemCodBar)Enum.Parse(typeof(CodBar.LinguagemCodBar), CompontesBasicos.dEstacao.dEstacaoSt.GetValor("Linguagem codbar"));
                    _Impress = new CodBar.Impress(Linguagem);
                    _Impress.NomeImpressora = textImpressora.Text;
                }
                return _Impress;
            }
        }*/

        private void simpleButton4_Click(object sender, EventArgs e)
        {            
            new CodBar.Impress().ajustaTemperatura((int)spinEdit1.Value);            
        }

        private void radioGroup1_EditValueChanged(object sender, EventArgs e)
        {
            if (Travado)
                return;
            CompontesBasicos.dEstacao.dEstacaoSt.SetValor("Linguagem codbar", radioGroup1.EditValue.ToString());            
        }

        private void BGabarito_Click(object sender, EventArgs e)
        {
            new Impress().ImprimeGabarito();
        }        

        private void cb_CorteAutomatico_CheckedChanged(object sender, EventArgs e)
        {
            if (Travado)
                return;
            CompontesBasicos.dEstacao.dEstacaoSt.SetValor("Corte Automatico", cb_CorteAutomatico.Checked);
        }
    }
}
