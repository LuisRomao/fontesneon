﻿using System;
using System.Reflection;
using System.Runtime.Serialization;

namespace Abstratos
{
    /// <summary>
    /// Classe abstrata base.
    /// </summary>
    [DataContract(Name = "ABS_Base", Namespace = "Abstratos")]
    public abstract class ABS_Base
    {

        /// <summary>
        /// Localiza um tipo pelo nome
        /// </summary>
        /// <param name="NomeComponente"></param>
        /// <returns></returns>
        private static Type BuscaType(string NomeComponente)
        {

            int PosPonto = NomeComponente.IndexOf('.');
            if (PosPonto <= 0)
                return null;
            System.Reflection.Assembly Ass;
            try
            {
                Ass = System.Reflection.Assembly.Load(NomeComponente.Substring(0, PosPonto));
            }
            catch
            {
                Ass = System.Reflection.Assembly.LoadFrom(@"C:\FontesVS\Geral Releases\DLLS\" + NomeComponente.Substring(0, PosPonto) + ".dll");
            }
            return Ass.GetType(NomeComponente);

        }

        /*
        /// <summary>
        /// Chama um método estático da classe derivada
        /// </summary>
        /// <param name="NomeClasse"></param>
        /// <param name="NomeMetodo"></param>
        /// <param name="parametros"></param>
        /// <param name="TiposParametros"></param>
        /// <returns></returns>
        protected static object ChamaMetodoEstatico(string NomeClasse, string NomeMetodo, object[] parametros, Type[] TiposParametros = null)
        {
            if (NomeClasse != null)
            {
                Type Tipo = BuscaType(NomeClasse);
                if (Tipo == null)
                    return null;

                Type[] Tipos;
                if (TiposParametros != null)
                    Tipos = TiposParametros;
                else
                {
                    Tipos = new Type[parametros.Length];
                    for (int i = 0; i < parametros.Length; i++)
                        Tipos[i] = parametros[i].GetType();
                }

                MethodInfo mInfo = Tipo.GetMethod(NomeMetodo, Tipos);
                if (mInfo != null)
                {
                    object Retorno = mInfo.Invoke(null, parametros);
                    if (Retorno != null)
                        return Retorno;
                }
            };
            return null;
        }*/

        /// <summary>
        /// Constrói um objeto a partir de seu nome. Isso evita a carga da dll desnecessariamente
        /// Caso a classe possua um método estático chamado VirConstrutor ele será chamado prioritariamente
        /// </summary>
        /// <param name="Nome">Nome completo do objeto (com Namespace)</param>
        /// <param name="parametros">Parâmetros do construtor</param>
        /// <param name="TiposParametros">Tipos dos parâmetros. Não é obrigatório mas permite a utilização de null nos parâmetros</param>
        /// <returns>Objeto construído</returns>        
        protected static object ContruaObjeto(string Nome, object[] parametros, Type[] TiposParametros = null)
        {
            if (Nome != null)
            {
                Type Tipo = BuscaType(Nome);
                if (Tipo == null)
                    return null;

                Type[] Tipos = null;
                if (TiposParametros != null)
                    Tipos = TiposParametros;
                else
                {
                    if (parametros != null)
                    {
                        Tipos = new Type[parametros.Length];
                        for (int i = 0; i < parametros.Length; i++)
                            Tipos[i] = parametros[i].GetType();
                    }
                }

                MethodInfo mInfo = Tipos == null ? Tipo.GetMethod("VirConstrutor") : Tipo.GetMethod("VirConstrutor", Tipos);
                if (mInfo != null)
                {
                    object Retorno = mInfo.Invoke(null, parametros);
                    if (Retorno != null)
                        return Retorno;
                }

                if ((parametros == null) || (parametros.Length == 0))
                {
                    ConstructorInfo constructorInfoObj = Tipo.GetConstructor(Type.EmptyTypes);
                    if (constructorInfoObj == null)
                        return null;
                    return constructorInfoObj.Invoke(null);
                }
                else
                {
                    System.Reflection.ConstructorInfo constructorInfoObjP = Tipo.GetConstructor(Tipos);
                    if (constructorInfoObjP == null)
                        return null;
                    return constructorInfoObjP.Invoke(parametros);
                }
            }
            else
                return null;
        }

        /// <summary>
        /// Chama o método estático do tipo filho
        /// </summary>
        /// <param name="Nome"></param>
        /// <param name="Metodo"></param>
        /// <param name="parametros"></param>
        /// <param name="TiposParametros"></param>
        /// <returns></returns>
        protected static object MetodoEstatico(string Nome, string Metodo, object[] parametros = null, Type[] TiposParametros = null)
        {
            if (Nome != null)
            {
                Type Tipo = BuscaType(Nome);
                if (Tipo == null)
                    return null;

                Type[] Tipos;
                if (TiposParametros != null)
                    Tipos = TiposParametros;
                else
                    if (parametros == null)
                    Tipos = System.Type.EmptyTypes;
                else
                {
                    Tipos = new Type[parametros.Length];
                    for (int i = 0; i < parametros.Length; i++)
                        Tipos[i] = parametros[i].GetType();
                }

                MethodInfo mInfo = Tipo.GetMethod(Metodo, Tipos);
                if (mInfo != null)
                {
                    object Retorno = mInfo.Invoke(null, parametros);
                    if (Retorno != null)
                        return Retorno;
                }
            };
            return null;
        }
    }
}
