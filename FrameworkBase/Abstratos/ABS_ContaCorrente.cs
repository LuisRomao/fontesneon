﻿using VirEnumeracoes;

namespace Abstratos
{
    /// <summary>
    /// Classe abstrata para conta conrrente
    /// </summary>
    public abstract class ABS_ContaCorrente : ABS_Base
    {
        private static string NomeObjeto = "CompontesBasicosProc.ContaCorrente";

        /// <summary>
        /// Cria um objeto real tipo Boleto (real)
        /// </summary>
        /// <returns></returns>
        public static ABS_ContaCorrente GetConta()
        {
            return (ABS_ContaCorrente)ContruaObjeto(NomeObjeto, new object[] { });
        }

        /// <summary>
        /// Cria um objeto real do tipo ContaCorrenteNeon
        /// </summary>
        /// <param name="Chave"></param>
        /// <param name="TipoCh"></param>
        /// <param name="UsarCCG"></param>
        /// <returns></returns>
        public static ABS_ContaCorrente GetContaCorrenteNeon(int Chave, TipoChaveContaCorrenteNeon TipoCh, bool UsarCCG)
        {
            return (ABS_ContaCorrente)ABS_Base.MetodoEstatico(NomeObjeto, "GetContaCorrenteNeon_CCG", 
                                                             new object[] { Chave,TipoCh,UsarCCG }, 
                                                             new System.Type[] { typeof(int),typeof(TipoChaveContaCorrenteNeon),typeof(bool) });                 
        }

        /// <summary>
        /// Agencia
        /// </summary>
        public int Agencia;
        /// <summary>
        /// Digito Agencia
        /// </summary>
        public int? AgenciaDg;
        /// <summary>
        /// Banco
        /// </summary>
        public int BCO;
        /// <summary>
        /// Caixa Postal no caso da Caixa
        /// </summary>
        public string CaixaPostal;
        /// <summary>
        /// Código CNR
        /// </summary>
        public int? CNR;
        /// <summary>
        /// CPF ou  CNPJ
        /// </summary>
        public DocBacarios.CPFCNPJ CPF_CNPJ;

        /// <summary>
        /// Número da conta
        /// </summary>
        public int NumeroConta;
        /// <summary>
        /// Digito da conta
        /// </summary>
        public string NumeroDg;
        /// <summary>
        /// Tipo da conta
        /// </summary>
        public TipoConta Tipo;
        /// <summary>
        /// Titular da conta
        /// </summary>
        public string Titular;
        /// <summary>
        /// Permite débito automático
        /// </summary>
        public bool DebitoAutomatico = false;
        /// <summary>
        /// Permite Rateio de Crédito
        /// </summary>
        public bool RateioCredito = false;
        /// <summary>
        /// Indica se faz pagamento eletrônico
        /// </summary>
        public bool PagamentoEletronico = false;

        /// <summary>
        /// Uso da ContaCorrenteNeon
        /// </summary>
        public abstract int? CCG { get; }

        /// <summary>
        /// Uso da ContaCorrenteNeon
        /// </summary>
        public abstract int? CCT { get; }
    }
}
