﻿using System;
using System.Collections.Generic;
using System.Text;
using DocBacarios;

namespace Abstratos
{
    /// <summary>
    /// Objeto abstrato para o tributo
    /// </summary>
    public abstract class ABS_Tributo : ABS_Base
    {
        private static string NomeObjeto = "Tributo.GrTributo.Cheque";

        /// <summary>
        /// Cria um objeto real tipo tributo (real)
        /// </summary>
        /// <returns></returns>
        public static ABS_Tributo GetTributo()
        {
            return (ABS_Tributo)ContruaObjeto(NomeObjeto, new object[] { });
        }

        /// <summary>
        /// Indica se está cadastrado no banco de dados
        /// </summary>
        public bool encontrado
        {
            get { return true; }
        }

        private int _TipoTributo;
        /// <summary>
        /// Tipo Tributo
        /// </summary>
        public virtual int TipoTributo
        {
            get { return _TipoTributo; }
            set { _TipoTributo = value; }
        }

        private string _IDDocumento;
        /// <summary>
        /// ID do Documento
        /// </summary>
        public virtual string IDDocumento
        {
            get { return _IDDocumento; }
            set { _IDDocumento = value; }
        }

        private string _TipoMovimento;
        /// <summary>
        /// Tipo de Movimento
        /// </summary>
        public virtual string TipoMovimento
        {
            get { return _TipoMovimento; }
            set { _TipoMovimento = value; }
        }

        private CPFCNPJ _CPFCNPJContribuinte;
        /// <summary>
        /// CPF ou CNPJ do Contribuinte
        /// </summary>
        public virtual CPFCNPJ CPFCNPJContribuinte
        {
            get { return (_CPFCNPJContribuinte == null) ? new CPFCNPJ(0) : _CPFCNPJContribuinte; }
            set { _CPFCNPJContribuinte = value; }
        }

        private string _NomeContribuinte;
        /// <summary>
        /// Nome do Contribuinte
        /// </summary>
        public virtual string NomeContribuinte
        {
            get { return (_NomeContribuinte == null) ? "" : _NomeContribuinte; }
            set { _NomeContribuinte = value; }
        }

        private string _EnderecoContribuinte;
        /// <summary>
        /// Endereco do Contribuinte
        /// </summary>
        public virtual string EnderecoContribuinte
        {
            get { return (_EnderecoContribuinte == null) ? "" : _EnderecoContribuinte; }
            set { _EnderecoContribuinte = value; }
        }

        private string _CEPContribuinte;
        /// <summary>
        /// CEP do Contribuinte
        /// </summary>
        public virtual string CEPContribuinte
        {
            get { return (_CEPContribuinte == null) ? "" : _CEPContribuinte; }
            set { _CEPContribuinte = value; }
        }

        private int _DAAgencia;
        /// <summary>
        /// Agencia para Debito da Empresa Administradora
        /// </summary>
        public virtual int DAAgencia
        {
            get { return _DAAgencia; }
            set { _DAAgencia = value; }
        }

        private string _DADigAgencia;
        /// <summary>
        /// Digito da Agencia para Debito da Empresa Administradora
        /// </summary>
        public virtual string DADigAgencia
        {
            get { return (_DADigAgencia == null) ? "" : _DADigAgencia; }
            set { _DADigAgencia = value; }
        }

        private int _DANumConta;
        /// <summary>
        /// Conta para Debito da Empresa Administradora
        /// </summary>
        public virtual int DANumConta
        {
            get { return _DANumConta; }
            set { _DANumConta = value; }
        }

        private string _DADigNumConta;
        /// <summary>
        /// Digito da Conta para Debito da Empresa Administradora
        /// </summary>
        public virtual string DADigNumConta
        {
            get { return (_DADigNumConta == null) ? "" : _DADigNumConta; }
            set { _DADigNumConta = value; }
        }

        private DateTime _DataDebito;
        /// <summary>
        /// Data de Debito
        /// </summary>
        public virtual DateTime DataDebito
        {
            get { return (_DataDebito == null) ? new DateTime() : _DataDebito; }
            set { _DataDebito = value; }
        }

        private decimal _Valor;
        /// <summary>
        /// Valor
        /// </summary>
        public virtual decimal Valor
        {
            get { return _Valor; }
            set { _Valor = value; }
        }

        private decimal _Juros;
        /// <summary>
        /// Valor dos Juros / Encargos
        /// </summary>
        public virtual decimal Juros
        {
            get { return _Juros; }
            set { _Juros = value; }
        }

        private decimal _Multa;
        /// <summary>
        /// Valor da Multa
        /// </summary>
        public virtual decimal Multa
        {
            get { return _Multa; }
            set { _Multa = value; }
        }

        private decimal _OutrasEntidades;
        /// <summary>
        /// Valor de Outras Entidades
        /// </summary>
        public virtual decimal OutrasEntidades
        {
            get { return _OutrasEntidades; }
            set { _OutrasEntidades = value; }
        }

        private decimal _ValorTotal;
        /// <summary>
        /// Valor Total
        /// </summary>
        public virtual decimal ValorTotal
        {
            get { return _ValorTotal; }
            set { _ValorTotal = value; }
        }

        private DateTime _DataVencimento;
        /// <summary>
        /// Data de Vencimento
        /// </summary>
        public virtual DateTime DataVencimento
        {
            get { return (_DataVencimento == null) ? new DateTime() : _DataVencimento; }
            set { _DataVencimento = value; }
        }

        private int _CodReceita;
        /// <summary>
        /// Código da Receita
        /// </summary>
        public virtual int CodReceita
        {
            get { return _CodReceita; }
            set { _CodReceita = value; }
        }

        private int _CodINSS;
        /// <summary>
        /// Código do INSS para Pagamento
        /// </summary>
        public virtual int CodINSS
        {
            get { return _CodINSS; }
            set { _CodINSS = value; }
        }

        private string _CodigoBarra;
        /// <summary>
        /// Código de Barra do Pagamento
        /// </summary>
        public virtual string CodigoBarra
        {
            get { return _CodigoBarra; }
            set { _CodigoBarra = value; }
        }

        private DateTime _PeriodoApuracao;
        /// <summary>
        /// Período de Apuração
        /// </summary>
        public virtual DateTime PeriodoApuracao
        {
            get { return (_PeriodoApuracao == null) ? new DateTime() : _PeriodoApuracao; }
            set { _PeriodoApuracao = value; }
        }

        private int _CompetenciaMes;
        /// <summary>
        /// Mês da Competência
        /// </summary>
        public virtual int CompetenciaMes
        {
            get { return _CompetenciaMes; }
            set { _CompetenciaMes = value; }
        }

        private int _CompetenciaAno;
        /// <summary>
        /// Ano da Competência
        /// </summary>
        public virtual int CompetenciaAno
        {
            get { return _CompetenciaAno; }
            set { _CompetenciaAno = value; }
        }

        private decimal _Percentual;
        /// <summary>
        /// Percentual
        /// </summary>
        public virtual decimal Percentual
        {
            get { return _Percentual; }
            set { _Percentual = value; }
        }

        private long _Referencia;
        /// <summary>
        /// Número de Referência
        /// </summary>
        public virtual long Referencia
        {
            get { return _Referencia; }
            set { _Referencia = value; }
        }

        private decimal _ValorReceitaBruta;
        /// <summary>
        /// Valor da Receita Bruta
        /// </summary>
        public virtual decimal ValorReceitaBruta
        {
            get { return _ValorReceitaBruta; }
            set { _ValorReceitaBruta = value; }
        }

        private string _CodConsistenciaArq;
        /// <summary>
        /// Código de Consistência do Arquivo
        /// </summary>
        public virtual string CodConsistenciaArq
        {
            get { return (_CodConsistenciaArq == null) ? "" : _CodConsistenciaArq; }
            set { _CodConsistenciaArq = value; }
        }

        private string _UsoEmpresa;
        /// <summary>
        /// Uso Empresa
        /// </summary>
        public virtual string UsoEmpresa
        {
            get { return (_UsoEmpresa == null) ? "" : _UsoEmpresa; }
            set { _UsoEmpresa = value; }
        }

        private string _Autenticacao;
        /// <summary>
        /// Número da Autenticacao
        /// </summary>
        public virtual string Autenticacao
        {
            get { return (_Autenticacao == null) ? "" : _Autenticacao; }
            set { _Autenticacao = value; }
        }
    }
}
