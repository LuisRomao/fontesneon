﻿/*
13/06/2014 11:00 MR 13.2.9.13 - Inclusão do campo Data de Emissão (Alterações indicadas por *** MRC - INICIO - PAG-FOR (13/06/2014 11:00) ***)
*/

using DocBacarios;
using System;
using System.Collections.Generic;
using System.IO;

namespace Abstratos
{
    /// <summary>
    /// Objeto abstrato para o cheque
    /// </summary>
    public abstract class ABS_Cheque : ABS_Base
    {
        private static string NomeObjeto = "dllCheques.Cheque";

        private decimal _Acrescimo;

        private int _Agencia_Credito;

        private int _Banco_Credito;

        private int _Carteira;

        private string _CEP;

        private int _CodAreaEmpresa;

        private int _CodLancamento;

        private int _CodMovimento;

        private int _Conta_Credito;

        //private int _ContaComplementar;

        private CPFCNPJ _CPFCNPJ_Credito;
        
        private DateTime _DataEmissao;

        private DateTime _DataVencimento;

        private decimal _Desconto;

        private string _DigAgencia_Credito;

        private string _DigConta_Credito;

        private string _Endereco_Credito;

        private int _FatorVencimento;

        private int _FinalidadeDOCTED;

        private string _HorarioConsultaSaldo;

        private string _InstrucaoChequeOP;

        private string _LinhaDigitavel;

        private int _ModalidadePagamento;

        private string _Nome;

        private int _NossoNumero;

        private int _NumDocumento;

        private int _NumeroPagamento;

        private string _SerieDocumento;

        private string _SeuNumero;

        private int _SituacaoAgendamento;

        private int _TipoConta_Credito;

        private int _TipoContaDOCTED;

        private int _TipoDocumento;

        private int _TipoMovimento;

        private string _UsoEmpresa;

        private decimal _Valor;

        /// <summary>
        /// Abre os comprovantes na tela.
        /// </summary>
        public virtual void AbreComprovante()
        {
            throw new NotImplementedException("AbreComprovante não implementado para este tipo de cheque");
        }

        /// <summary>
        /// Cria um objeto real tipo cheque (real)
        /// </summary>
        /// <returns></returns>
        public static ABS_Cheque GetCheque()
        {
            return (ABS_Cheque)ContruaObjeto(NomeObjeto, new object[] { });
        }

        /// <summary>
        /// Cria um objeto real tipo Cheque (real)
        /// </summary>
        /// <param name="CHE">CHE</param>
        /// <returns></returns>
        public static ABS_Cheque GetCheque(int CHE)
        {
            return (ABS_Cheque)ContruaObjeto(NomeObjeto, new object[] { CHE }, new Type[] { typeof(int) });
        }        

        /// <summary>
        /// Junta todos os PDFs dos comprovantes em um único documento temporário.
        /// </summary>
        /// <param name="CHEs"></param>
        /// <returns></returns>
        public static string MergeComprovantes(List<int> CHEs)
        {
            return (string)MetodoEstatico(NomeObjeto, "MergeComprovantes", new object[] { CHEs });
        }

        /// <summary>
        /// Junta todos os PDFs dos comprovantes em um único documento temporário.
        /// </summary>
        /// <param name="CHEs"></param>
        /// <returns></returns>
        public static string MergeComprovantes(List<ABS_Cheque> CHEs)
        {
            return (string)MetodoEstatico(NomeObjeto, "MergeComprovantes", new object[] { CHEs });
        }

        /// <summary>
        /// Valor do Acrescimo
        /// </summary>
        public virtual decimal Acrescimo
        {
            get { return _Acrescimo; }
            set { _Acrescimo = value; }
        }
        /// <summary>
        /// Agencia de crédito do cheque 
        /// </summary>
        public virtual int Agencia_Credito
        {
            get { return _Agencia_Credito; }
            set { _Agencia_Credito = value; }
        }
        /// <summary>
        /// Banco de crédito do cheque
        /// </summary>
        public virtual int Banco_Credito
        {
            get { return _Banco_Credito; }
            set { _Banco_Credito = value; }
        }
        /// <summary>
        /// Carteira
        /// </summary>
        public virtual int Carteira
        {
            get { return _Carteira; }
            set { _Carteira = value; }
        }
        /// <summary>
        /// CEP
        /// </summary>
        public virtual string CEP
        {
            get { return (_CEP == null) ? string.Empty : _CEP; }
            set { _CEP = value; }
        }
        /// <summary>
        /// Codigo de Area da Empresa
        /// </summary>
        public virtual int CodAreaEmpresa
        {
            get { return _CodAreaEmpresa; }
            set { _CodAreaEmpresa = value; }
        }
        /// <summary>
        /// Codigo de Lancamento
        /// </summary>
        public virtual int CodLancamento
        {
            get { return _CodLancamento; }
            set { _CodLancamento = value; }
        }
        /// <summary>
        /// Código Movimento
        /// </summary>
        public virtual int CodMovimento
        {
            get { return _CodMovimento; }
            set { _CodMovimento = value; }
        }
        /// <summary>
        /// Conta de crédito do cheque
        /// </summary>
        public virtual int Conta_Credito
        {
            get { return _Conta_Credito; }
            set { _Conta_Credito = value; }
        }

        /*
        /// <summary>
        /// Conta Complementar
        /// </summary>
        public virtual int ContaComplementar
        {
            get { return _ContaComplementar; }
            set { _ContaComplementar = value; }
        }*/

        /// <summary>
        /// CPF ou CNPJ de crédito do cheque
        /// </summary>
        public virtual CPFCNPJ CPFCNPJ_Credito
        {
            get { return (_CPFCNPJ_Credito == null) ? new CPFCNPJ(0) : _CPFCNPJ_Credito; }
            set { _CPFCNPJ_Credito = value; }
        }
        /// <summary>
        /// Data de Emissão
        /// </summary>
        public virtual DateTime DataEmissao
        {
            get { return (_DataEmissao == null) ? new DateTime() : _DataEmissao; }
            set { _DataEmissao = value; }
        }
        /// <summary>
        /// Data de Vencimento
        /// </summary>
        public virtual DateTime DataVencimento
        {
            get { return (_DataVencimento == null) ? new DateTime() : _DataVencimento; }
            set { _DataVencimento = value; }
        }
        /// <summary>
        /// Valor do Desconto
        /// </summary>
        public virtual decimal Desconto
        {
            get { return _Desconto; }
            set { _Desconto = value; }
        }
        /// <summary>
        /// Digito da Agencia
        /// </summary>
        public virtual string DigAgencia_Credito
        {
            get { return (_DigAgencia_Credito == null) ? string.Empty : _DigAgencia_Credito; }
            set { _DigAgencia_Credito = value; }
        }
        /// <summary>
        /// Digito da Conta de crédito
        /// </summary>
        public virtual string DigConta_Credito
        {
            get { return (_DigConta_Credito == null) ? string.Empty : _DigConta_Credito; }
            set { _DigConta_Credito = value; }
        }
        /// <summary>
        /// Endereco do fornecedor (crédito)
        /// </summary>
        public virtual string Endereco_Credito
        {
            get { return (_Endereco_Credito == null) ? string.Empty : _Endereco_Credito; }
            set { _Endereco_Credito = value; }
        }
        /// <summary>
        /// Fator de Vencimento
        /// </summary>
        public virtual int FatorVencimento
        {
            get { return _FatorVencimento; }
            set { _FatorVencimento = value; }
        }
        /// <summary>
        /// Finalidade DOC/TED
        /// </summary>
        public virtual int FinalidadeDOCTED
        {
            get { return _FinalidadeDOCTED; }
            set { _FinalidadeDOCTED = value; }
        }
        /// <summary>
        /// Horario de Consulta do Saldo
        /// </summary>
        public virtual string HorarioConsultaSaldo
        {
            get { return (_HorarioConsultaSaldo == null) ? string.Empty : _HorarioConsultaSaldo; }
            set { _HorarioConsultaSaldo = value; }
        }
        /// <summary>
        /// Instrucao Cheque OP
        /// </summary>
        public virtual string InstrucaoChequeOP
        {
            get { return (_InstrucaoChequeOP == null) ? string.Empty : _InstrucaoChequeOP; }
            set { _InstrucaoChequeOP = value; }
        }
        /// <summary>
        /// Linha Digitavel
        /// </summary>
        public virtual string LinhaDigitavel
        {
            get { return (_LinhaDigitavel == null) ? string.Empty : _LinhaDigitavel; }
            set { _LinhaDigitavel = value; }
        }
        /// <summary>
        /// Modalidade Pagamento
        /// </summary>
        public virtual int ModalidadePagamento
        {
            get { return _ModalidadePagamento; }
            set { _ModalidadePagamento = value; }
        }
        /// <summary>
        /// Nome do beneficiário
        /// </summary>
        public virtual string Nome
        {
            get { return (_Nome == null) ? string.Empty : _Nome; }
            set { _Nome = value; }
        }
        /// <summary>
        /// Nosso Numero
        /// </summary>
        public virtual int NossoNumero
        {
            get { return _NossoNumero; }
            set { _NossoNumero = value; }
        }
        /// <summary>
        /// Numero de Documento
        /// </summary>
        public virtual int NumDocumento
        {
            get { return _NumDocumento; }
            set { _NumDocumento = value; }
        }
        /// <summary>
        /// Numero Pagamento
        /// </summary>
        public virtual int NumeroPagamento
        {
            get { return _NumeroPagamento; }
            set { _NumeroPagamento = value; }
        }
        /// <summary>
        /// Serie de Documento
        /// </summary>
        public virtual string SerieDocumento
        {
            get { return _SerieDocumento; }
            set { _SerieDocumento = value; }
        }
        /// <summary>
        /// Seu Numero
        /// </summary>
        public virtual string SeuNumero
        {
            get { return (_SeuNumero == null) ? string.Empty : _SeuNumero; }
            set { _SeuNumero = value; }
        }
        /// <summary>
        /// Situacao Agendamento
        /// </summary>
        public virtual int SituacaoAgendamento
        {
            get { return _SituacaoAgendamento; }
            set { _SituacaoAgendamento = value; }
        }
        /// <summary>
        /// Tipo de Conta para crédito
        /// </summary>
        public virtual int TipoConta_Credito
        {
            get { return _TipoConta_Credito; }
            set { _TipoConta_Credito = value; }
        }
        /// <summary>
        /// Tipo Conta DOC/TED
        /// </summary>
        public virtual int TipoContaDOCTED
        {
            get { return _TipoContaDOCTED; }
            set { _TipoContaDOCTED = value; }
        }
        /// <summary>
        /// Tipo de Documento
        /// </summary>
        public virtual int TipoDocumento
        {
            get { return _TipoDocumento; }
            set { _TipoDocumento = value; }
        }
        /// <summary>
        /// Tipo do Movimento
        /// </summary>
        public virtual int TipoMovimento
        {
            get { return _TipoMovimento; }
            set { _TipoMovimento = value; }
        }
        /// <summary>
        /// Uso Empresa
        /// </summary>
        public virtual string UsoEmpresa
        {
            get { return (_UsoEmpresa == null) ? string.Empty : _UsoEmpresa; }
            set { _UsoEmpresa = value; }
        }
        /// <summary>
        /// Valor
        /// </summary>
        public virtual decimal Valor
        {
            get { return _Valor; }
            set { _Valor = value; }
        }
    }
}
