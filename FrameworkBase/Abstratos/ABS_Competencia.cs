﻿using System;
using System.Runtime.Serialization;

namespace Abstratos
{
    /// <summary>
    /// Objeto de referencia para competência
    /// </summary>
    [DataContract(Name = "ABS_Competencia", Namespace = "Abstratos")]
    public class ABS_Competencia : ICloneable
    {

        /// <summary>
        /// Construtor
        /// </summary>
        public ABS_Competencia()
        {            
        }

        /// <summary>
        /// Construtor
        /// </summary>
        /// <param name="_mes">mes</param>
        /// <param name="_ano">ano</param>        
        public ABS_Competencia(int _mes, int _ano)
        {
            Mes = _mes;
            Ano = _ano;            
        }

        /// <summary>
        /// Mes da competência
        /// </summary>        
        private int mes;

        /// <summary>
        /// Mes da competência
        /// </summary>
        [DataMember]
        public int Mes
        {
            get { return mes; }
            set
            {
                mes = value;
                AjustaCompMes();
            }
        }
        /// <summary>
        /// Ano da competência
        /// </summary>
        private int ano;

        /// <summary>
        /// Ano da competência
        /// </summary>
        [DataMember]
        public int Ano
        {
            get { return ano; }
            set { ano = value; }
        }

        /// <summary>
        /// Ajusta a copetencia caso o mes tenha saido do intervalo 1-12
        /// </summary>
        /// <returns>Campetencia ajustada</returns>
        protected void AjustaCompMes()
        {
            while ((mes < 1) || (mes > 12))
            {
                if (mes < 1)
                {
                    ano--;
                    mes += 12;
                };
                if (mes > 12)
                {
                    ano++;
                    mes -= 12;
                }
            }
        }

        /// <summary>
        /// Competência na formatado para banco de dados
        /// </summary>
        public int CompetenciaBind
        {
            get
            {
                return Ano * 100 + Mes;
            }
            set
            {
                Ano = value / 100;
                Mes = value % 100;
            }
        }



        #region ICloneable Members
        /// <summary>
        /// Clonagem
        /// </summary>
        /// <returns></returns>
        public virtual object Clone()
        {
            return new ABS_Competencia(Mes, Ano);
        }


        #endregion

        /// <summary>
        /// Cria um clone
        /// </summary>
        /// <param name="meses"></param>
        /// <returns></returns>
        public virtual ABS_Competencia ABS_CloneCompet(int meses)
        {
            throw new NotImplementedException();
        }
    }
}
