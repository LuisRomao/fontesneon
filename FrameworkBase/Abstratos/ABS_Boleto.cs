﻿/*
09/05/2014 LH 13.2.8.47 - Registro do envio de e-mails no boleto
*/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Abstratos
{
    /// <summary>
    /// Objeto abstrato para boleto
    /// </summary>
    public abstract class ABS_Boleto : ABS_Base
    {
        private static string NomeObjeto = "Boletos.Boleto.Boleto";
        private static string NomeObjetoProc = "BoletosProc.Boleto";

        /// <summary>
        /// Cria um objeto real tipo Boleto (real)
        /// </summary>
        /// <returns></returns>
        public static ABS_Boleto GetBoleto()
        {
            return (ABS_Boleto)ContruaObjeto(NomeObjeto, new object[] { });
        }

        /// <summary>
        /// Cria um objeto real tipo BoletoProc (real)
        /// </summary>
        /// <returns></returns>
        public static ABS_Boleto GetBoletoProc()
        {
            return (ABS_Boleto)ContruaObjeto(NomeObjetoProc, new object[] { });
        }

        /// <summary>
        /// Cria um objeto real tipo Boleto (real)
        /// </summary>
        /// <param name="BOL">BOL</param>
        /// <returns></returns>
        public static ABS_Boleto GetBoleto(int BOL)
        {
            return (ABS_Boleto)ContruaObjeto(NomeObjeto, new object[] { BOL }, new Type[] { typeof(int) });
        }

        /// <summary>
        /// Indica se o boleto foi encontrado no banco de dados
        /// </summary>
        public bool Encontrado;

        /// <summary>
        /// Grava no histórico do boleto
        /// </summary>
        /// <param name="Obs"></param>
        /// <param name="EmailJuridico"></param>
        public abstract void GravaJustificativa(string Obs, string EmailJuridico);

        /// <summary>
        /// Dados da conta corrente do boleto
        /// </summary>
        public abstract ABS_ContaCorrente ABSConta { get; set; }

        /// <summary>
        /// Competência do boleto
        /// </summary>
        public abstract ABS_Competencia ABSCompetencia { get; }

        /// <summary>
        /// Vencimento original
        /// </summary>
        public abstract DateTime VencimentoOriginal { get; }

        /// <summary>
        /// Valor original
        /// </summary>
        public abstract decimal ValorOriginal { get; }

        /// <summary>
        /// Valor corrigido
        /// </summary>
        public abstract decimal ValorCorrigido { get; }

        /// <summary>
        /// Multa
        /// </summary>
        public abstract decimal Valormulta { get; }

        /// <summary>
        /// Juros
        /// </summary>
        public abstract decimal Valorjuros { get; }

        /// <summary>
        /// Valor final do boleto
        /// </summary>
        public abstract decimal ValorFinal { get; }

    }
    
}
