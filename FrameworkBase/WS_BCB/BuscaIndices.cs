﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using virCompet;

namespace WS_BCB
{
    /// <summary>
    /// Busca valores no banco central
    /// </summary>
    public class BuscaIndices
    {                
        private System.Net.WebProxy Proxy;

        private WS_BCB.br.gov.bcb.www3.FachadaWSSGSService _FachadaWSSGSService1;

        private WS_BCB.br.gov.bcb.www3.FachadaWSSGSService FachadaWSSGSService1
        {
            get
            {
                if (_FachadaWSSGSService1 == null)
                {
                    _FachadaWSSGSService1 = new br.gov.bcb.www3.FachadaWSSGSService();
                    if (Proxy != null)
                        _FachadaWSSGSService1.Proxy = Proxy;
                }
                return _FachadaWSSGSService1;
            }
        }        

        /// <summary>
        /// Tipos
        /// </summary>
        public enum TiposBusca:long
        { 
            /// <summary>
            /// INPC
            /// </summary>
            INPC = 188,
            /// <summary>
            /// Salario Mínimo
            /// </summary>
            SalarioMinimo = 1619
        }

        /// <summary>
        /// Construtor
        /// </summary>
        /// <param name="_Proxy"></param>
        public BuscaIndices(System.Net.WebProxy _Proxy = null)
        {
            Proxy = _Proxy;
        }

        /// <summary>
        /// Competencia lida
        /// </summary>
        public virCompetencia Comp;
        /// <summary>
        /// Valor lido
        /// </summary>
        public decimal Valor;
        /// <summary>
        /// Le o ultimimo INPC ou salário mínimo
        /// </summary>
        /// <param name="Tipo">Tipo</param>
        /// <returns></returns>
        public bool BuscaUltimo(TiposBusca Tipo)
        {
            try
            {                
                WS_BCB.br.gov.bcb.www3.WSSerieVO WSSerieVO1 = FachadaWSSGSService1.getUltimoValorVO((long)Tipo);
                WS_BCB.br.gov.bcb.www3.WSValorSerieVO WSValorSerieVO1 = WSSerieVO1.ultimoValor;
                Comp = new virCompetencia(WSValorSerieVO1.mes, WSValorSerieVO1.ano);
                string Manobra = WSValorSerieVO1.svalor.Replace(".","");
                Valor = decimal.Parse(Manobra) / 100;
                return true;
            }
            catch
            {
                return false;
            }
        }        
        
        /// <summary>
        /// Retorna uma lista
        /// </summary>
        /// <param name="DI"></param>
        /// <param name="DF"></param>
        /// <param name="Tipo">Tipo</param>
        /// <returns></returns>
        public string lista(DateTime DI,DateTime DF,TiposBusca Tipo)
        {
            string retorno = "";
            //WS_BCB.br.gov.bcb.www3.WSSerieVO[] Grupo = FachadaWSSGSService1.getValoresSeriesVO(new long[] { 188 }, DI.ToString("dd/MM/yyyy"), DF.ToString("dd/MM/yyyy"));
            WS_BCB.br.gov.bcb.www3.WSSerieVO[] Grupo = FachadaWSSGSService1.getValoresSeriesVO(new long[] { (long)Tipo }, DI.ToString("dd/MM/yyyy"), DF.ToString("dd/MM/yyyy"));
            if (Grupo.Length != 0)
                foreach (WS_BCB.br.gov.bcb.www3.WSValorSerieVO V in Grupo[0].valores)
                {
                    retorno += string.Format("{0}/{1}:\t{2}\r\n",V.mes,V.ano,V.svalor);
                }
            return retorno;
        }

    }
}
