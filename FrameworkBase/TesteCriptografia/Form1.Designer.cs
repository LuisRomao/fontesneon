﻿namespace TesteCriptografia
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.button1 = new System.Windows.Forms.Button();
            this.label2 = new System.Windows.Forms.Label();
            this.TSaida = new System.Windows.Forms.TextBox();
            this.Ttorre = new System.Windows.Forms.TextBox();
            this.TUnidade = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(12, 12);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(264, 23);
            this.button1.TabIndex = 0;
            this.button1.Text = "Criptografar";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(9, 120);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(76, 17);
            this.label2.TabIndex = 2;
            this.label2.Text = "Resultado:";
            // 
            // TSaida
            // 
            this.TSaida.Location = new System.Drawing.Point(105, 117);
            this.TSaida.Name = "TSaida";
            this.TSaida.Size = new System.Drawing.Size(654, 22);
            this.TSaida.TabIndex = 4;
            // 
            // Ttorre
            // 
            this.Ttorre.Location = new System.Drawing.Point(104, 75);
            this.Ttorre.MaxLength = 2;
            this.Ttorre.Name = "Ttorre";
            this.Ttorre.Size = new System.Drawing.Size(43, 22);
            this.Ttorre.TabIndex = 6;
            // 
            // TUnidade
            // 
            this.TUnidade.Location = new System.Drawing.Point(242, 73);
            this.TUnidade.MaxLength = 4;
            this.TUnidade.Name = "TUnidade";
            this.TUnidade.Size = new System.Drawing.Size(83, 22);
            this.TUnidade.TabIndex = 7;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(9, 78);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(47, 17);
            this.label3.TabIndex = 8;
            this.label3.Text = "Torre:";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(171, 78);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(65, 17);
            this.label1.TabIndex = 9;
            this.label1.Text = "Unidade:";
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(787, 171);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.TUnidade);
            this.Controls.Add(this.Ttorre);
            this.Controls.Add(this.TSaida);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.button1);
            this.Name = "Form1";
            this.Text = "Teste Criptografia";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox TSaida;
        private System.Windows.Forms.TextBox Ttorre;
        private System.Windows.Forms.TextBox TUnidade;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label1;
    }
}

