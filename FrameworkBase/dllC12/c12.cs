﻿using System;
using System.Collections.Generic;


namespace dllC12
{
    
    /// <summary>
    /// Calculo de juros compostos
    /// </summary>
    public class c12
    {
        /// <summary>
        /// Tipos de juros
        /// </summary>
        public enum TipoJuros 
        {
            /// <summary>
            /// 
            /// </summary>
            simples,
            /// <summary>
            /// 
            /// </summary>
            compostos
        }

        private decimal taxa;

        /// <summary>
        /// 
        /// </summary>
        public TipoJuros Tipoj;

        private void IniciarVariaveis()
        {
            Parcelas = new List<parcela>();
            Tipoj = TipoJuros.compostos;
        }

        /// <summary>
        /// Construtor
        /// </summary>
        public c12()
        {
            IniciarVariaveis();
        }

        /// <summary>
        /// Parcela, tem que ter as entradas e saida, tido a 12C somando tudo tem que zerar.
        /// </summary>
        public List<parcela> Parcelas;

        private DateTime MenorData()
        {
            if (Parcelas.Count == 0)
                return DateTime.MinValue;
            DateTime retorno = Parcelas[0].data;
            foreach (parcela p in Parcelas)
                if (retorno > p.data)
                    retorno = p.data;
            return retorno;
        }

        private double CalculaErro(double taxaTeste)
        {
            //taxaTeste += 1;
            if(taxaTeste == -1)
                return 0;
            double erro = 0;
            DateTime data0 = MenorData();
            foreach (parcela p in Parcelas)
            {
                int n = (p.data - data0).Days;
                if (Tipoj == TipoJuros.compostos)
                    erro += ((double)p.Valor) / Math.Pow(1+taxaTeste, n);
                else
                    erro += ((double)p.Valor) / (1+(taxaTeste* n));
            }
            return erro;
        }

        /// <summary>
        /// Taxa
        /// </summary>
        public decimal Taxa
        {
            get { return taxa; }
            set { taxa = value; }
        }
        
 
        /// <summary>
        /// Ajusta a taxa para as parcelas cadastradas
        /// </summary>
        /// <param name="VBase"></param>
        /// <param name="VTeto"></param>
        /// <param name="Tolerancia"></param>
        /// <returns></returns>
        public bool calcularTaxaParcelas(double VBase,double VTeto,double Tolerancia)
        {
            if (Parcelas.Count == 0)
                return false;
            double erroTeto = CalculaErro(VTeto);
            double erroBase = CalculaErro(VBase);
            if (erroTeto * erroBase > 0)
                return false;

            while ((Math.Abs(erroTeto - erroBase) > Tolerancia) || (VTeto == VBase))
            {
                //double VInter = (erroBase * (VTeto - VBase) / (erroBase - erroTeto)) + VBase;
                //if((VInter == VBase) || (VInter == VTeto))
                double VInter = ((VBase + VTeto) / 2);
                double ErroInter = CalculaErro(VInter);
                if ((ErroInter * erroBase) > 0) //sinais iguais
                {
                    VBase = VInter;
                    erroBase = ErroInter;
                }
                else
                {
                    VTeto = VInter;
                    erroTeto = ErroInter;
                }
            }

            Taxa = (decimal)((VBase + VTeto) / 2);

            return true;
        }

        /// <summary>
        /// Parcela
        /// </summary>
        public class parcela
        {
            /// <summary>
            /// 
            /// </summary>
            public DateTime data;
            /// <summary>
            /// 
            /// </summary>
            public decimal Valor;

            /// <summary>
            /// parcela
            /// </summary>
            /// <param name="_Data"></param>
            /// <param name="_Valor"></param>
            public parcela(DateTime _Data,decimal _Valor)
            {
                data = _Data;
                Valor = _Valor;
            }
        }
    }
}
