﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace dllC12
{
    /*
    Descritivo: (ver planilha de exemplo na documentação)
       - recebemos os itens que devem ser agrupados em grupos que somem os valores solicitos
       - montamos 3 vetores:
            vD contém todos os itens em ordem crescentes e serão apontados pelos vO
            vO vetor resultado, no fim do processo terá a lista dos indices que apontam para o vO ordenados por grupos
            vG os grupos
    */

    /// <summary>
    /// Componente que agrupa uma lista de valores
    /// </summary>
    public class Agrupador
    {
        /// <summary>
        /// Gera um log na área de transferência para debugar
        /// </summary>
        private bool GerarLog = false;
        private bool LogCurto = true;

        /// <summary>
        /// Cria um arquivo para o log
        /// </summary>
        /// <param name="Arquivo"></param>
        public void IniciaLog(string Arquivo)
        {
            GerarLog = true;
            ArqLog = new System.IO.StreamWriter(Arquivo);
        }
        /// <summary>
        /// Tempo máximo em segundos
        /// </summary>
        public int? TempoMaximo = null;

        /// <summary>
        /// Tempo total do cálculo
        /// </summary>
        public decimal TempoTotal;

        private class obItensD 
        {
            //identificação do item para o chamador
            public int id;
            public decimal val;
            //indice do vO, é usado para saber se o item ja está alocado
            public int ivO;

            public obItensD(int _id,decimal valor)
            {
                id = _id;
                val = valor;
                ivO = -1;
            }
        }

        private class obItensO
        {
            public int ivG;
            public int ivD;
            public decimal valac;
            public decimal ultimoTestado;

            public obItensO()
            {
                ivG = -1;
                ivD = -1;
                valac = 0;
            }
        }

        private class obGrupo
        {
            public int id;
            public decimal val;

            public obGrupo(int _id,decimal valor)
            {
                id = _id;
                val = valor;
            }
        }

        /// <summary>
        /// Carrega os dados
        /// </summary>
        private bool CarregarVetores(SortedList<int,decimal> Totais,SortedList<int, decimal> Itens)
        {
            decimal ValidaTotG = 0;
            decimal ValidaTotD = 0;
            if (GerarLog)
            {
                //SBDebug = new StringBuilder();
                //SBDebug.AppendLine("Debug:\r\n\r\nCarga inicial");
                ArqLog.WriteLine("Debug:\r\n\r\nCarga inicial");
            }
            TotItens = Itens.Count;
            vO = new obItensO[TotItens];
            vD = new obItensD[TotItens];
            vG = new obGrupo[Totais.Count];
            for (int ivD = 0; ivD < TotItens; ivD++)
            {
                obItensD Novo = new obItensD(Itens.Keys[ivD], Itens.Values[ivD]);
                ValidaTotD += Novo.val;
                for (int ivO1 = 0; ; ivO1++)
                {
                    if (vD[ivO1] == null)
                    {
                        vD[ivO1] = Novo;
                        break;
                    }
                    else
                    {
                        if (vD[ivO1].val > Novo.val)
                        {
                            for (int ivO2 = TotItens -1; ivO2 > ivO1; ivO2--)
                                vD[ivO2] = vD[ivO2 - 1];
                            vD[ivO1] = Novo;
                            break;
                        }
                    }

                }
            }
            for (int ivO = 0; ivO < TotItens; ivO++)
                vO[ivO] = new obItensO();
            //for (int ivG = 0; ivG < Totais.Count; ivG++)
            //    vG[ivG] = new obGrupo(Totais.Keys[ivG], Totais.Values[ivG]);
            for (int ivG = 0; ivG < Totais.Count; ivG++)
            {
                obGrupo Novo = new obGrupo(Totais.Keys[ivG], Totais.Values[ivG]);
                ValidaTotG += Novo.val;
                for (int ivG1 = 0; ; ivG1++)
                {
                    if (vG[ivG1] == null)
                    {
                        vG[ivG1] = Novo;
                        break;
                    }
                    else
                    {
                        if (vG[ivG1].val > Novo.val)
                        {
                            for (int ivG2 = Totais.Count - 1; ivG2 > ivG1; ivG2--)
                                vG[ivG2] = vG[ivG2 - 1];
                            vG[ivG1] = Novo;
                            break;
                        }
                    }

                }
            }
            if (GerarLog)
            {
                ArqLog.WriteLine("vD:");
                int i = 1;
                for (int ivD = 0; ivD < TotItens; ivD++)
                    ArqLog.WriteLine(string.Format("{0,3} - {1,8:n2}\t",i++, vD[ivD].val));
                ArqLog.Write("\r\n");
                ArqLog.WriteLine("vG:");
                i = 1;
                for (int ivG = 0; ivG < vG.Length; ivG++)
                    ArqLog.WriteLine(string.Format("{0,3} - {1,8:n2}\t", i++, vG[ivG].val));
                ArqLog.Write("\r\n");
                //for (int ivD = 0; ivD < TotItens; ivD++)
                //    ArqLog.Write(string.Format("{0}\t", vD[ivD].val));
                //ArqLog.Write("\r\n");
                ArqLog.WriteLine("------------------------------");
                if(ValidaTotD != ValidaTotG)
                    ArqLog.WriteLine(string.Format("Erro nos dados \r\n Total itens = {0:n2} Grupos = {1:n2}", ValidaTotD, ValidaTotG));
            };            
            return ValidaTotD == ValidaTotG;
        }

        private void StatusO(int Teto)
        {
            if (LogCurto)
            {
                if (Teto < 0)
                    return;
                ArqLog.Write("ivG\t");
                for (int ivO = 0; ivO < Teto; ivO++)
                    ArqLog.Write(string.Format("{0,3}\t", vO[ivO].ivG));
                ArqLog.Write("\r\nivD\t");
                for (int ivO = 0; ivO < Teto; ivO++)
                    ArqLog.Write(string.Format("{0,3}\t", vO[ivO].ivD));                                                
                ArqLog.Write("\r\n");
            }
            else
            {
                if (Teto < 0)
                    return;
                ArqLog.Write("ivG\t");
                for (int ivO = 0; ivO < Teto; ivO++)
                    ArqLog.Write(string.Format("{0,3}-{1,8:n2}\t", vO[ivO].ivG, vG[vO[ivO].ivG].val));
                ArqLog.Write("\r\nivD\t");
                for (int ivO = 0; ivO < Teto; ivO++)
                    ArqLog.Write(string.Format("{0,12}\t", vO[ivO].ivD));
                ArqLog.Write("\r\nval D\t");
                for (int ivO = 0; ivO < Teto; ivO++)
                    ArqLog.Write(string.Format("{0,12:n2}\t", vD[vO[ivO].ivD].val));
                ArqLog.Write("\r\nvalac\t");
                for (int ivO = 0; ivO < Teto; ivO++)
                    ArqLog.Write(string.Format("{0,12:n2}\t", vO[ivO].valac + vD[vO[ivO].ivD].val));
                ArqLog.Write("\r\n");
                ArqLog.Write("\r\n");
            }
        }

        private int TotItens;
        private obItensD[] vD; //Destino - vetor com a lista dos itens a serem apontados
        private obItensO[] vO; //Origem - vetor que aponta os destinos
        private obGrupo[]  vG; //Grupos        
        private System.IO.StreamWriter ArqLog;
        private DateTime Inicio;
        private bool TerminadoOk;
        private bool Timeout;

        /// <summary>
        /// Encontra um subgrupo que soma o total
        /// </summary>
        /// <param name="Total">Total a somar</param>
        /// <param name="Itens">Itens candidatos: chave / valor</param>
        /// <returns>Lista com as chaves</returns>
        public List<int> Agrupar(decimal Total, SortedList<int, decimal> Itens)
        {
            if (Itens.Count == 0)
                return null;
            decimal TotalGeral = 0;
            foreach (decimal Vit in Itens.Values)
                TotalGeral += Vit;
            if (TotalGeral < Total)
                return null;
            else if (TotalGeral == Total)
            {
                List<int> retorno = new List<int>();
                foreach (int id in Itens.Keys)
                    retorno.Add(id);
                return retorno;
            }
            SortedList<int, decimal> Totais = new SortedList<int, decimal>();
            Totais.Add(0, Total);
            Totais.Add(1, TotalGeral - Total);
            SortedList<int, List<int>> Resposta = Agrupar(Totais, Itens);
            if ((Resposta == null) || (!Resposta.ContainsKey(0)))
                return null;
            else
                return Resposta[0];

        }

        /// <summary>
        /// Agrupa os itens
        /// </summary>
        /// <param name="Totais">Totais: chave / Valor</param>
        /// <param name="Itens">Itens:chave / valor </param>
        /// <returns>Itens agrupados: chave(total) / lista de chaves dos itens </returns>
        public SortedList<int, List<int>> Agrupar(SortedList<int,decimal> Totais,SortedList<int,decimal> Itens)
        {
            try
            {
                TerminadoOk = false;
                bool OkLinha = false;
                if (!CarregarVetores(Totais, Itens))
                    return null;
                Inicio = DateTime.Now;
                int ivOcalcular = 0;
                vO[0].ivG = 0;
                vO[0].ivD = -1;
                vO[0].valac = 0;
                vO[0].ultimoTestado = -1;
                for (; ; )
                {
                    if (TempoMaximo.HasValue)
                        if ((DateTime.Now - Inicio).TotalSeconds > TempoMaximo.Value)
                        {
                            Timeout = true;
                            break;
                        }
                    if (GerarLog)
                        StatusO(ivOcalcular);
                    //procura o proximo iten livre que nao estore. se chegar no fim nao deu certo e tem que voltar a calcular a linha anterior
                    OkLinha = false;
                    
                    for (int ivD = vO[ivOcalcular].ivD + 1; ivD < TotItens; ivD++)
                    {
                        if (vO[ivOcalcular].ivD >= 0)
                        {
                            if(vD[vO[ivOcalcular].ivD].ivO == ivOcalcular)
                                vD[vO[ivOcalcular].ivD].ivO = -1;                            
                        }
                        
                        if (vD[ivD].ivO >= 0)
                            continue;
                        if (vO[ivOcalcular].ultimoTestado == vD[ivD].val)
                            continue;
                        else
                            vO[ivOcalcular].ultimoTestado = vD[ivD].val;

                        if (vO[ivOcalcular].valac + vD[ivD].val > vG[vO[ivOcalcular].ivG].val)
                        {
                            //estourou volta para linha anterior
                            break;
                        }
                        else if (vO[ivOcalcular].valac + vD[ivD].val < vG[vO[ivOcalcular].ivG].val)
                        {
                            vO[ivOcalcular].ivD = ivD;
                            vD[ivD].ivO = ivOcalcular;

                            vO[ivOcalcular + 1].ivG = vO[ivOcalcular].ivG;
                            vO[ivOcalcular + 1].valac = vO[ivOcalcular].valac + vD[ivD].val;
                            vO[ivOcalcular + 1].ivD = ivD;
                            vO[ivOcalcular + 1].ultimoTestado = -1;
                            ivOcalcular++;
                            OkLinha = true;
                            break;
                        }
                        else //fechou o grupo
                        {
                            vO[ivOcalcular].ivD = ivD;
                            vD[ivD].ivO = ivOcalcular;
                            if (vO[ivOcalcular].ivG == (vG.Length - 1))
                            {
                                TerminadoOk = true;
                                break;
                            };                            
                            vO[ivOcalcular + 1].ivG = vO[ivOcalcular].ivG + 1;
                            vO[ivOcalcular + 1].valac = 0;
                            vO[ivOcalcular + 1].ivD = -1;
                            vO[ivOcalcular + 1].ultimoTestado = -1;
                            ivOcalcular++;
                            OkLinha = true;
                            break;
                        }
                    };
                    if (TerminadoOk)
                        break;
                    else
                    {
                        if (!OkLinha)
                        {
                            if ((vO[ivOcalcular].ivD >= 0) && (vD[vO[ivOcalcular].ivD].ivO == ivOcalcular))
                                vD[vO[ivOcalcular].ivD].ivO = -1;                            
                            ivOcalcular--;
                            if (ivOcalcular < 0)
                                break;
                        }
                    }
                }
                if (!TerminadoOk)
                    return null;

                if (GerarLog)
                    StatusO(TotItens);

                SortedList<int, List<int>> Retorno = new SortedList<int, List<int>>();

                int ultimo_ivG = -1;
                List<int> ultimaLista = null;
                for (int ivO = 0; ivO < TotItens; ivO++)
                {
                    if (vO[ivO].ivG != ultimo_ivG)
                    {
                        ultimo_ivG = vO[ivO].ivG;
                        ultimaLista = new List<int>();
                        Retorno.Add(vG[vO[ivO].ivG].id, ultimaLista);
                    }
                    ultimaLista.Add(vD[vO[ivO].ivD].id);
                }

                return Retorno;
            }
            catch (Exception e)
            {
                if (GerarLog)
                {
                    ArqLog.Write(e.Message);
                    return null;
                }
                else
                    throw e;
            }
            finally
            {
                if (GerarLog)
                {
                    TempoTotal = (decimal)(DateTime.Now - Inicio).TotalSeconds;
                    if (Timeout)
                        ArqLog.WriteLine("TIME OUT");
                    ArqLog.WriteLine(string.Format("\r\n{0} Tempo Total: {1:n4} s", TerminadoOk ? "ok" : "falha", TempoTotal));
                    ArqLog.Close();
                    ArqLog.Dispose();
                }
            }
        }
    }
}
