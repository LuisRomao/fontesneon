using System;
using System.Collections;
using System.Collections.Generic;

namespace VirDB.Bancovirtual
{
    /// <summary>
    /// Tipos de Banco de dados
    /// </summary>
    public enum TiposDeBanco
    {
        /// <summary>
        /// Banco de dados MDB
        /// </summary>
        Access,
        /// <summary>
        /// Banco MSSQL
        /// </summary>
        SQL,
        /// <summary>
        /// Banco da filial
        /// </summary>
        Filial,
        /// <summary>
        /// Banco MSSQL ArquivoMorto
        /// </summary>
        SQLMorto,
        /// <summary>
        /// Banco MSSQL Remoto 2000
        /// </summary>
        Internet1,
        /// <summary>
        /// Banco MSSQL Remoto 2005
        /// </summary>
        Internet2,
        /// <summary>
        /// Banco MDB Historico
        /// </summary>
        AccessH,
        /// <summary>
        /// Banco FireBird
        /// </summary>
        FB,        
        //EmailREt,
    }



    /// <summary>
    /// Objeto que representa os bancos de dados dispon�ves
    /// </summary>
    /// <remarks>� usado na classe virtual 'BancoVirtual'</remarks>
    public class Bancos : Object
    {
        /// <summary>
        /// Nome do banco
        /// </summary>
        public string Nome;
        /// <summary>
        /// String de conex�o
        /// </summary>
        public string StringDeConexao;

        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="Nome">Nome do banco na lista de escolha</param>
        /// <param name="StringDeConexao">String de conexao</param>
        public Bancos(string Nome, string StringDeConexao)
        {
            this.Nome = Nome;
            this.StringDeConexao = StringDeConexao;
        }

        /// <summary>
        /// Sobregava o m�todo
        /// </summary>
        /// <remarks>Necess�rio para ser usado para popular listas e combos</remarks>
        /// <returns>Nome do banco</returns>
        public override string ToString()
        {
            return Nome;
        }
    }


    /// <summary>
    /// Represenata qual o banco de dados ser� usado
    /// </summary>
    public static class BancoVirtual
    {
        /// <summary>
        /// Lista dos possiveis bancos
        /// </summary>
        private static List<Bancos> Strings;

        /// <summary>
        /// Lista dos possiveis bancos
        /// </summary>
        private static List<Bancos> StringsFilial;        

        /// <summary>
        /// Lista dos possiveis bancos (CEP)
        /// </summary>
        private static List<Bancos> StringsCEP;

        /// <summary>
        /// Lista dos possiveis bancos para o arquivo morto
        /// </summary>
        private static List<Bancos> StringsMorto;

        /// <summary>
        /// Lista dos possiveis bancos Access
        /// </summary>
        private static List<Bancos> StringsAccess;

        /// <summary>
        /// Lista dos possiveis bancos AccessH
        /// </summary>
        private static List<Bancos> StringsAccessH;

        /// <summary>
        /// Lista dos possiveis bancos Internet 1
        /// </summary>
        private static List<Bancos> StringsInternet1;

        /// <summary>
        /// Lista dos possiveis bancos Internet 2
        /// </summary>
        private static List<Bancos> StringsInternet2;

        /// <summary>
        /// String de conexao escolhido
        /// </summary>
        public static int StringEscolhido=-1;

        /// <summary>
        /// String de conexao escolhido
        /// </summary>
        public static int? StringEscolhidoFilial = null;

        /// <summary>
        /// String de conexao escolhido para EmailRet
        /// </summary>
        public static int? StringEscolhidoEmailRet = null;

        /// <summary>
        /// String de conexao escolhido CEP
        /// </summary>
        public static int StringEscolhidoCEP = -1;

        /// <summary>
        /// String de conexao escolhido para o arquivo morto
        /// </summary>
        public static int StringEscolhidoMorto = -1;

        /// <summary>
        /// String de conexao escolhido Access
        /// </summary>
        public static int StringEscolhidoAccess = -1;

        /// <summary>
        /// String de conexao escolhido AccessH
        /// </summary>
        public static int StringEscolhidoAccessH = -1;

        /// <summary>
        /// String de conexao escolhido Internet 1
        /// </summary>
        public static int StringEscolhidoInternet1 = -1;

        /// <summary>
        /// String de conexao escolhido Internet 2
        /// </summary>
        public static int StringEscolhidoInternet2 = -1;

        /// <summary>
        /// Retorna se os strings foram configurados
        /// </summary>
        /// <param name="Tipo">Tipo de banco</param>
        /// <returns></returns>
        /// <remarks>Evita erro durante a apresenta��o de componentes no studio</remarks>
        public static bool Configurado(TiposDeBanco Tipo) {
            switch (Tipo)
            {
                case TiposDeBanco.Access:
                    return (StringsAccess != null) && (StringsAccess.Count > 0);
                case TiposDeBanco.SQL:
                case TiposDeBanco.FB:
                    return (Strings != null) && (Strings.Count > 0);
                case TiposDeBanco.SQLMorto:
                    return false;
                case TiposDeBanco.Internet1:
                    return (StringsInternet1 != null) && (StringsInternet1.Count > 0);
                case TiposDeBanco.Internet2:
                    return (StringsInternet2 != null) && (StringsInternet2.Count > 0);
                case TiposDeBanco.AccessH:
                    return (StringsAccessH != null) && (StringsAccessH.Count > 0);   
                case TiposDeBanco.Filial :
                    return (StringsFilial != null) && (StringsFilial.Count > 0);   
                default:
                    throw new NotImplementedException(string.Format("Enumera��o n�o tratada: {0}",Tipo));
            }
        }

        /// <summary>
        /// Retorna o string escolhido
        /// </summary>
        public static string StringConeFinal(TiposDeBanco Tipo)
        {
            String Retorno = "";
            switch (Tipo)
            {
                case TiposDeBanco.SQL:
                case TiposDeBanco.FB:
                    if (StringEscolhido == -1)
                        EscolherString();
                    if (StringEscolhido != -1)
                        Retorno = ((Bancos)Strings[StringEscolhido]).StringDeConexao;
                    break;
                case TiposDeBanco.SQLMorto:
                    if (StringEscolhidoMorto == -1)
                        EscolherStringMorto();
                    if (StringEscolhidoMorto != -1)
                        Retorno = ((Bancos)StringsMorto[StringEscolhidoMorto]).StringDeConexao;
                    break;
                case TiposDeBanco.Internet1:
                    if (StringEscolhidoInternet1 == -1)
                        EscolherStringInternet1();
                    if (StringEscolhidoInternet1 != -1)
                        Retorno = ((Bancos)StringsInternet1[StringEscolhidoInternet1]).StringDeConexao;
                    break;
                case TiposDeBanco.Internet2:
                    if (StringEscolhidoInternet2 == -1)
                        EscolherStringInternet2();
                    if (StringEscolhidoInternet2 != -1)
                        Retorno = ((Bancos)StringsInternet2[StringEscolhidoInternet2]).StringDeConexao;
                    break;
                case TiposDeBanco.Access:
                    if (StringEscolhidoAccess == -1)
                        EscolherStringAccess();
                    if (StringEscolhidoAccess != -1)
                        Retorno = ((Bancos)StringsAccess[StringEscolhidoAccess]).StringDeConexao;
                    break;
                case TiposDeBanco.AccessH:
                    if (StringEscolhidoAccessH == -1)
                        EscolherStringAccessH();
                    if (StringEscolhidoAccessH != -1)
                        Retorno = ((Bancos)StringsAccessH[StringEscolhidoAccessH]).StringDeConexao;
                    break;
                case TiposDeBanco.Filial:
                    if (!StringEscolhidoFilial.HasValue)
                        EscolherStringFilial();
                    if (StringEscolhidoFilial.HasValue)
                        Retorno = (StringsFilial[StringEscolhidoFilial.Value]).StringDeConexao;
                    break;
                default:
                    throw new NotImplementedException(string.Format("Enumera��o n�o tratada: {0}", Tipo));
            };
            return Retorno;
        }

        

        /// <summary>
        /// Metodo chamado para que o usu�rio escolha um string (banco) - gen�rico
        /// </summary>
        /// <remarks>Se s� houver um na lista a janela nao � aberta. (Caso do uso em prodo��o)</remarks>
        /// <returns>indice do strig</returns>
        private static int EscolherString(List<Bancos> Lista)
        {
            if (Lista == null)
                return -1;
            else                
                    if (Lista.Count == 1)
                        return 0;
                    else
                        using (SelStringDeConexao form = new SelStringDeConexao())
                        {
                            form.comboBox1.Items.AddRange(Lista.ToArray());
                            //form.Selecao.Properties.Items.AddRange(Lista);
                            form.ShowDialog();
                            return form.comboBox1.SelectedIndex;
                        };            

        }                

        /// <summary>
        /// Metodo chamado para que o usu�rio escolha um string (banco)
        /// </summary>
        /// <remarks>Se s� houver um na lista a janela nao � aberta. (Caso do uso em prodo��o)</remarks>
        /// <returns>indice do strig</returns>
        public static int EscolherString()
        {
            if (StringEscolhido == -1){
                StringEscolhido = EscolherString(Strings);
            };             
            return StringEscolhido;                        
        }

        /// <summary>
        /// Metodo chamado para que o usu�rio escolha um string (Filial)
        /// </summary>
        /// <returns></returns>
        public static int EscolherStringFilial()
        {
            if (!StringEscolhidoFilial.HasValue)
            {
                StringEscolhidoFilial = EscolherString(StringsFilial);
            };
            return StringEscolhidoFilial.Value;
        }

        /// <summary>
        /// Metodo chamado para que o usu�rio escolha um string (banco) do arquivo Morto
        /// </summary>
        /// <remarks>Se s� houver um na lista a janela nao � aberta. (Caso do uso em prodo��o)</remarks>
        /// <returns>indice do strig</returns>
        public static int EscolherStringMorto()
        {
            if (StringEscolhidoMorto == -1)
            {
                StringEscolhidoMorto = EscolherString(StringsMorto);
            };
            return StringEscolhidoMorto;
        }

        

        /// <summary>
        /// Metodo chamado para que o usu�rio escolha um string (banco Access)
        /// </summary>
        /// <remarks>Se s� houver um na lista a janela nao � aberta. (Caso do uso em prodo��o)</remarks>
        /// <returns>indice do strig Access</returns>
        public static int EscolherStringAccess()
        {
            if (StringEscolhidoAccess == -1)
            {
                StringEscolhidoAccess = EscolherString(StringsAccess);
            };
            return StringEscolhidoAccess;                        
        }

        /// <summary>
        /// Metodo chamado para que o usu�rio escolha um string (banco AccessH)
        /// </summary>
        /// <remarks>Se s� houver um na lista a janela nao � aberta. (Caso do uso em prodo��o)</remarks>
        /// <returns>indice do strig AccessH</returns>
        public static int EscolherStringAccessH()
        {
            if (StringEscolhidoAccessH == -1)
            {
                StringEscolhidoAccessH = EscolherString(StringsAccessH);
            };
            return StringEscolhidoAccessH;
        }

        /// <summary>
        /// Metodo chamado para que o usu�rio escolha um string (banco Internet 1)
        /// </summary>
        /// <remarks>Se s� houver um na lista a janela nao � aberta. (Caso do uso em produ��o)</remarks>
        /// <returns>indice do strig Internet 1</returns>
        public static int EscolherStringInternet1()
        {
            if (StringEscolhidoInternet1 == -1)
            {
                StringEscolhidoInternet1 = EscolherString(StringsInternet1);
            };
            return StringEscolhidoInternet1;
        }

        /// <summary>
        /// Metodo chamado para que o usu�rio escolha um string (banco Internet 2)
        /// </summary>
        /// <remarks>Se s� houver um na lista a janela nao � aberta. (Caso do uso em produ��o)</remarks>
        /// <returns>indice do strig Internet 2</returns>
        public static int EscolherStringInternet2()
        {
            if (StringEscolhidoInternet2 == -1)
            {
                StringEscolhidoInternet2 = EscolherString(StringsInternet2);
            };
            return StringEscolhidoInternet2;
        }

        /// <summary>
        /// Prepara a lista dos possiveis strings (Bancos)
        /// </summary>
        public static void Popular(string Nome, string StringDeConexao)
        {
            if (Strings == null)
            {
                Strings = new List<Bancos>();
                StringEscolhido = -1;
            };
            Strings.Add(new Bancos(Nome,StringDeConexao));
        }

        /// <summary>
        /// Prepara a lista dos possiveis strings (Filial)
        /// </summary>
        /// <param name="Nome"></param>
        /// <param name="StringDeConexao"></param>
        public static void PopularFilial(string Nome, string StringDeConexao)
        {
            if (StringsFilial == null)            
                StringsFilial = new List<Bancos>();                            
            StringsFilial.Add(new Bancos(Nome, StringDeConexao));
        }

        /// <summary>
        /// Prepara a lista dos possiveis strings (CEP)
        /// </summary>
        public static void PopularCEP(string Nome, string StringDeConexao)
        {
            if (StringsCEP == null)
            {
                StringsCEP = new List<Bancos>();
                StringEscolhidoCEP = -1;
            };
            StringsCEP.Add(new Bancos(Nome, StringDeConexao));
        }

        /// <summary>
        /// Prepara a lista dos possiveis strings (Bancos) para o aquivo morto
        /// </summary>
        public static void PopularMorto(string Nome, string StringDeConexao)
        {
            if (StringsMorto == null)
            {
                StringsMorto = new List<Bancos>();
                StringEscolhidoMorto = -1;
            };
            StringsMorto.Add(new Bancos(Nome, StringDeConexao));
        }

        /// <summary>
        /// Prepara a lista dos possiveis strings (Bancos Access)
        /// </summary>
        public static void PopularAcces(string Nome, string StringDeConexao)
        {
            if (StringsAccess == null)
            {
                StringsAccess = new List<Bancos>();
                StringEscolhidoAccess = -1;
            };
            StringsAccess.Add(new Bancos(Nome, StringDeConexao));
        }

        /// <summary>
        /// Prepara a lista dos possiveis strings (Bancos AccessH)
        /// </summary>
        public static void PopularAccesH(string Nome, string StringDeConexaoH)
        {
            if (StringsAccessH == null)
            {
                StringsAccessH = new List<Bancos>();
                StringEscolhidoAccessH = -1;
            };
            StringsAccessH.Add(new Bancos(Nome, StringDeConexaoH));
        }

        /// <summary>
        /// Prepara a lista dos possiveis strings (Bancos Access)
        /// </summary>
        public static void PopularInternet(string Nome, string StringDeConexao,int numero)
        {
            if (numero == 1)
            {
                if (StringsInternet1 == null)
                {
                    StringsInternet1 = new List<Bancos>();
                    StringEscolhidoInternet1 = -1;
                };
                StringsInternet1.Add(new Bancos(Nome, StringDeConexao));
            }
            else
            {
                if (StringsInternet2 == null)
                {
                    StringsInternet2 = new List<Bancos>();
                    StringEscolhidoInternet2 = -1;
                };
                StringsInternet2.Add(new Bancos(Nome, StringDeConexao));
            }
        }
    }

    


}
