using System;
using System.Windows.Forms;

namespace VirDB.Bancovirtual
{
    /// <summary>
    /// Formul�rio para ecolha do string de conex�o
    /// </summary>
    public partial class SelStringDeConexao : Form
    {
        /// <summary>
        /// Constructor
        /// </summary>
        public SelStringDeConexao()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            DialogResult = DialogResult.OK;
        }                

        private void Selecao_KeyDown(object sender, KeyEventArgs e)
        {
            if(e.KeyCode == Keys.Enter)
                DialogResult = DialogResult.OK;
        }
    }
}