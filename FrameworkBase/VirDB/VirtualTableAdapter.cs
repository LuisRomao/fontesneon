using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Reflection;
using VirDB.Bancovirtual;


namespace VirDB
{
    

    /// <summary>
    /// Classe para gerar os TableAdapteres
    /// </summary>
    /// <remarks>Esta classe deveria ser abstrata</remarks>
    public abstract class VirtualTableAdapter : System.ComponentModel.Component
    {

        #region Variaveis de rastreamento
        /// <summary>
        /// Data e hora do in�cio da transa��o
        /// </summary>
        public DateTime? DateTimeInicioTrans;
        /// <summary>
        /// Identifica��o da chamada
        /// </summary>
        public SortedList<int,string> IdentificadorChamada;
        /// <summary>
        /// Gera um log das transa��es - usar para debug ou monitoramento
        /// </summary>
        public bool GeraLog = true;
        /// <summary>
        /// Log das transa��es
        /// </summary>
        public System.Text.StringBuilder Log = new System.Text.StringBuilder();
        /// <summary>
        /// Log do erro em Vircatch
        /// </summary>
        public string LogVircatchRetornando = "";
        /// <summary>
        /// Ultimo erro
        /// </summary>
        public Exception UltimoErro;
        #endregion

        #region CamposEfetivamenteAlterados
        /// <summary>
        /// Criar uma lista com os campos efetivamente alterados
        /// </summary>
        /// <param name="row">Linha a ser verificada</param>
        /// <returns>Campos alterados:key = nome do campo e valor = novo valor</returns>
        public static SortedList CamposEfetivamenteAlterados(DataRow row)
        {
            SortedList retorno = new SortedList();
            if (row != null)

                foreach (DataColumn coluna in row.Table.Columns)
                {

                    if ((!row.HasVersion(DataRowVersion.Original))
                        ||
                        (row[coluna.ColumnName, DataRowVersion.Original] == null)
                        ||
                        (!row[coluna.ColumnName, DataRowVersion.Original].Equals(row[coluna.ColumnName, DataRowVersion.Current]))
                       )
                        retorno.Add(coluna.ColumnName, row[coluna.ColumnName, DataRowVersion.Current]);
                };
            return retorno;
        }

        /// <summary>
        /// Retorna se o valor de algum campo foi alterado
        /// </summary>
        /// <param name="row">Linha a ser testada</param>
        /// <returns>Se foi alterado</returns>
        /// <remarks>Usa a funcao CamposEfetivamenteAlterados</remarks>
        public static bool EfetivamenteAlterados(DataRow row)
        {
            return CamposEfetivamenteAlterados(row).Count != 0;
        }

        /// <summary>
        /// Retorna se o valor de algum campo foi alterado
        /// </summary>
        /// <param name="rows">Linhas a serem testadas</param>
        /// <returns>Se foi alterado</returns>
        /// <remarks>Usa a funcao CamposEfetivamenteAlterados</remarks>
        public static bool EfetivamenteAlterados(DataRow[] rows)
        {
            bool retorno = false;
            Array.ForEach(rows,
            delegate(DataRow row)
            {
                if (EfetivamenteAlterados(row))
                    retorno = true;
            });
            return retorno;
        } 
        #endregion

        /// <summary>
        /// Pega a data do servidor
        /// </summary>
        /// <returns></returns>
        public abstract DateTime DataServidor();

        /// <summary>
        /// Tipos busca SQL
        /// </summary>
        public enum ComandosBusca
        {
            /// <summary>
            /// Select com retorndo de DataTable
            /// </summary>
            DataTable,
            /// <summary>
            /// Select com retorndo de um valor
            /// </summary>
            Escalar,
            /// <summary>
            /// Select com retorndo de um unico registro, os campos retornam como membros de uma arrayList
            /// </summary>
            ArrayList,
            /// <summary>
            /// Select, retorna um reader. No esquecer de fecha-lo
            /// </summary>
            Reader,
            /// <summary>
            /// Executa um NonQuery (delete,update)
            /// </summary>
            NonQuery,
            /// <summary>
            /// Select com retorndo de um unico registro, retorna um dataRow
            /// </summary>
            DataRow
        }

        /// <summary>
        /// Tipo do banco
        /// </summary>
        public TiposDeBanco Tipo = TiposDeBanco.SQL;

        /// <summary>
        /// Fun�ao est�tica para ajustar o incremento para 10 000
        /// </summary>
        /// <param name="DataSet">DataSet para ser ajustado</param>
        public static void AjustaAutoInc(DataSet DataSet)
        {
            foreach (DataTable Tabela in DataSet.Tables)
                foreach (DataColumn Coluna in Tabela.Columns)
                    if (Coluna.AutoIncrement)
                    {
                        if (Tabela.Rows.Count > 0)
                            Coluna.AutoIncrementStep = 10000;
                        else
                        {
                            Coluna.AutoIncrementSeed = -1;
                            Coluna.AutoIncrementStep = -1;
                        }
                    }
        }

        /// <summary>
        /// Retorna se os strings foram configurados
        /// </summary>
        /// <param name="Tipo">Tipo de banco</param>
        /// <returns></returns>
        /// <remarks>Evita erro durante a apresenta��o de componentes no studio</remarks>
        private static bool Configurado(TiposDeBanco Tipo)
        {
            return BancoVirtual.Configurado(Tipo);
        }

        /// <summary>
        /// Retorna se os strings foram configurados
        /// </summary>        
        /// <returns></returns>
        /// <remarks>Evita erro durante a apresenta��o de componentes no studio</remarks>
        public virtual bool Configurado()
        {
            return Configurado(Tipo);
        }

        //protected bool Derivado = true;

        #region TrocaString
        /// <summary>
        /// Troca o string de conexao pelo do parametro
        /// </summary>
        /// <param name="novo">novo string de conexao</param>
        public virtual void TrocarStringDeConexao(string novo)
        {
            if (!Configurado())
                return;
            PropertyInfo PropCon = GetType().GetProperty("Connection");
            if (PropCon != null)
            {
                switch (Tipo)
                {
                    case TiposDeBanco.SQL:
                    case TiposDeBanco.SQLMorto:
                    case TiposDeBanco.Internet1:
                    case TiposDeBanco.Internet2:
                    case TiposDeBanco.Filial:
                        System.Data.SqlClient.SqlConnection cone2 = (System.Data.SqlClient.SqlConnection)PropCon.GetValue(this, null);
                        cone2.ConnectionString = novo;
                        PropCon = GetType().GetProperty("CommandCollection");
                        if (PropCon != null)
                        {
                            System.Data.SqlClient.SqlCommand[] _commandCollection = (System.Data.SqlClient.SqlCommand[])PropCon.GetValue(this, null);
                            Array.ForEach(_commandCollection,
                            delegate(System.Data.SqlClient.SqlCommand Comando)
                            {
                                Comando.Connection = cone2;
                            });
                        };
                        BindingFlags BF = BindingFlags.NonPublic | BindingFlags.Instance;

                        PropertyInfo PropCom = GetType().GetProperty("CommandCollection", BF);
                        if (PropCom != null)
                        {
                            System.Data.SqlClient.SqlCommand[] Comandos = (System.Data.SqlClient.SqlCommand[])PropCom.GetValue(this, null);
                            Array.ForEach(Comandos,
                            delegate(System.Data.SqlClient.SqlCommand Comando)
                            {
                                Comando.Connection = cone2;
                            });
                        }
                        break;
                    case TiposDeBanco.Access:
                    case TiposDeBanco.AccessH:
                        System.Data.OleDb.OleDbConnection coneB = (System.Data.OleDb.OleDbConnection)PropCon.GetValue(this, null);
                        coneB.ConnectionString = novo;
                        break;
                    case TiposDeBanco.FB:
                    default:
                        throw new NotImplementedException(string.Format("Enumera��o n�o tratada: {0}", Tipo));
                };


            };
        }

        /// <summary>
        /// Troca o string de conexao pelos do parametro cadastrados
        /// </summary>
        /// <remarks>O tipo ser� o padr�o
        /// Os strings s�o cadastrados na tela pricipal ou em program.cs com o comando 
        ///   CompontesBasicos.Bancovirtual.BancoVirtual.Popular(...
        /// </remarks>
        public void TrocarStringDeConexao()
        {
            TrocarStringDeConexao(BancoVirtual.StringConeFinal(Tipo));
        }

        /// <summary>
        /// Troca o string de conexao pelos do parametro cadastrados
        /// </summary>
        /// <remarks>Os strings s�o cadastrados na tela pricipal ou em program.cs com o comando 
        ///   CompontesBasicos.Bancovirtual.BancoVirtual.Popular(...
        /// </remarks>
        /// <param name="TB">Tipo de banco de dados</param>
        public void TrocarStringDeConexao(TiposDeBanco TB)                   
        {
            Tipo = TB;
            TrocarStringDeConexao();            
        }
        #endregion

        #region BuscaSQLLinha
        /// <summary>
        /// Retorna um unico registro
        /// </summary>
        /// <remarks>O comando SQL deve retornar dados</remarks>
        /// <param name="comando">Comando SQL</param>
        /// <returns>DataRow do registro</returns>
        public ArrayList BuscaSQLLinha(string comando)
        {
            return (ArrayList)ExecutarSQL(comando, ComandosBusca.ArrayList, null);
        }

        /// <summary>
        /// Retorna um unico registro
        /// </summary>
        /// <remarks>O comando SQL deve retornar dados</remarks>
        /// <param name="comando">Comando SQL</param>
        /// <param name="Parametros">Parametros do comando SQL marcados como @P1,@P2... </param>
        /// <returns>DataRow do registro</returns>
        public ArrayList BuscaSQLLinha(string comando, params object[] Parametros)
        {
            return (ArrayList)ExecutarSQL(comando, ComandosBusca.ArrayList, Parametros);
        }

        #endregion

        #region BuscaSQLLinhaRow
        /// <summary>
        /// Retorna um unico registro em forma de row
        /// </summary>
        /// <remarks>O comando SQL deve retornar dados</remarks>
        /// <param name="comando">Comando SQL</param>
        /// <returns>DataRow do registro</returns>
        public DataRow BuscaSQLRow(string comando)
        {
            return (DataRow)ExecutarSQL(comando, ComandosBusca.DataRow, null);
        }

        /// <summary>
        /// Retorna um unico registro em forma de row
        /// </summary>
        /// <remarks>O comando SQL deve retornar dados</remarks>
        /// <param name="comando">Comando SQL</param>
        /// <param name="Parametros">Parametros do comando SQL marcados como @P1,@P2... </param>
        /// <returns>DataRow do registro</returns>
        public DataRow BuscaSQLRow(string comando, params object[] Parametros)
        {
            return (DataRow)ExecutarSQL(comando, ComandosBusca.DataRow, Parametros);
        }

        #endregion

        #region BuscaSQLTabela
        /// <summary>
        /// Retorna um DataTable a partir de um comando Select
        /// </summary>
        /// <remarks>O comando select deve retornar registros</remarks>
        /// <returns>Resultado do select</returns>
        public DataTable BuscaSQLTabela(string comando)
        {
            DataTable retorno = (DataTable)ExecutarSQL(comando, ComandosBusca.DataTable);
            return retorno;
        }

        /// <summary>
        /// Retorna um DataTable a partir de um comando Select
        /// </summary>
        /// <remarks>O comando select deve retornar registros
        /// Os paramentos devem ser nomeados como @P1,@P2...
        /// </remarks>
        /// <param name="comando">Comado SQL</param>
        /// <param name="Parametros">Lista de parametros</param>
        /// <returns>Resultado do select</returns>
        public DataTable BuscaSQLTabela(string comando, params object[] Parametros)
        {
            DataTable retorno = (DataTable)ExecutarSQL(comando, ComandosBusca.DataTable, Parametros);
            return retorno;
        }
        #endregion

        #region BuscaEscalar

        /// <summary>
        /// Busca escalar
        /// </summary>
        /// <param name="comando"></param>
        /// <param name="Paramentros"></param>
        /// <returns></returns>
        public bool? BuscaEscalar_bool(string comando, params object[] Paramentros)
        {
            object oRetorno = ExecutarSQL(comando, ComandosBusca.Escalar, Paramentros);
            if (oRetorno != null)
            {

                try
                {
                    return Convert.ToBoolean(oRetorno);
                }
                catch
                {
                    return null;
                }
            }
            return null;
        }

        /// <summary>
        /// Busca escalar
        /// </summary>
        /// <param name="comando"></param>
        /// <param name="Paramentros"></param>
        /// <returns></returns>
        public DateTime? BuscaEscalar_DateTime(string comando, params object[] Paramentros)
        {
            object oRetorno = ExecutarSQL(comando, ComandosBusca.Escalar, Paramentros);
            if (oRetorno != null)
            {

                try
                {
                    return Convert.ToDateTime(oRetorno);
                }
                catch
                {
                    return null;
                }
            }
            return null;
        }

        /// <summary>
        /// Busca Escalar string
        /// </summary>
        /// <param name="comando"></param>
        /// <param name="Paramentros"></param>
        /// <returns></returns>
        public string BuscaEscalar_string(string comando, params object[] Paramentros)
        {
            object oRetorno = ExecutarSQL(comando, ComandosBusca.Escalar, Paramentros);
            if (oRetorno != null)                            
               return oRetorno.ToString();                            
            return null;
        }

        /// <summary>
        /// Busca escalar
        /// </summary>
        /// <param name="comando"></param>
        /// <param name="Paramentros"></param>
        /// <returns></returns>
        public int? BuscaEscalar_int(string comando, params object[] Paramentros)
        {
            object oRetorno = ExecutarSQL(comando,ComandosBusca.Escalar,Paramentros);
            if (oRetorno != null)
            {

                try
                {
                    return Convert.ToInt32(oRetorno);
                }
                catch
                {
                    return null;
                }
            }
            return null;
        }

        /// <summary>
        /// Busca escalar
        /// </summary>
        /// <param name="comando"></param>
        /// <param name="Paramentros"></param>
        /// <returns></returns>
        public decimal? BuscaEscalar_decimal(string comando, params object[] Paramentros)
        {
            object oRetorno = ExecutarSQL(comando, ComandosBusca.Escalar, Paramentros);
            if (oRetorno != null)
            {

                try
                {
                    return Convert.ToDecimal(oRetorno);
                }
                catch
                {
                    return null;
                }
            }
            return null;
        }

        /// <summary>
        /// Executa uma busca escalar
        /// </summary>
        /// <remarks>O valor retorna no parametro out </remarks>
        /// <param name="comando">Comando SQL que deve retornar apenas um campo</param>
        /// <param name="retorno">Indica se foi encontrado um valor</param>
        /// <returns>retorna se a busca retornou um valor v�lido</returns>
        public bool BuscaEscalar(string comando, out object retorno)
        {
            return BuscaEscalar(comando, out retorno, null);
        }

        /// <summary>
        /// Executa uma busca escalar
        /// </summary>
        /// <remarks>O valor retorna no parametro out </remarks>
        /// <param name="comando">Comando SQL que deve retornar apenas um campo</param>
        /// <param name="retorno">Indica se foi encontrado um valor</param>
        /// <returns>retorna se a busca retornou um valor v�lido</returns>
        public bool BuscaEscalar(string comando, out bool retorno)
        {
            return BuscaEscalar(comando, out retorno, null);
        }

        /// <summary>
        /// Executa uma busca escalar
        /// </summary>
        /// <remarks>O valor retorna no parametro out </remarks>
        /// <param name="comando">Comando SQL que deve retornar apenas um campo</param>
        /// <param name="retorno">Indica se foi encontrado um valor</param>
        /// <returns>retorna se a busca retornou um valor v�lido</returns>
        public bool BuscaEscalar(string comando, out Int16 retorno)
        {
            return BuscaEscalar(comando, out retorno, null);
        }

        /// <summary>
        /// Executa uma busca escalar
        /// </summary>
        /// <remarks>O valor retorna no parametro out </remarks>
        /// <param name="comando">Comando SQL que deve retornar apenas um campo</param>
        /// <param name="retorno">Indica se foi encontrado um valor</param>
        /// <returns>retorna se a busca retornou um valor v�lido</returns>
        public bool BuscaEscalar(string comando, out Int32 retorno)
        {
            return BuscaEscalar(comando, out retorno, null);
        }

        /// <summary>
        /// Executa uma busca escalar
        /// </summary>
        /// <remarks>O valor retorna no parametro out </remarks>
        /// <param name="comando">Comando SQL que deve retornar apenas um campo</param>
        /// <param name="retorno">Indica se foi encontrado um valor</param>
        /// <returns>retorna se a busca retornou um valor v�lido</returns>
        public bool BuscaEscalar(string comando, out Int64 retorno)
        {
            return BuscaEscalar(comando, out retorno, null);
        }

        /// <summary>
        /// Executa uma busca escalar
        /// </summary>
        /// <remarks>O valor retorna no parametro out </remarks>
        /// <param name="comando">Comando SQL que deve retornar apenas um campo</param>
        /// <param name="retorno">Indica se foi encontrado um valor</param>
        /// <returns>retorna se a busca retornou um valor v�lido</returns>
        public bool BuscaEscalar(string comando, out string retorno)
        {
            return BuscaEscalar(comando, out retorno, null);
        }

        /// <summary>
        /// Executa uma busca escalar
        /// </summary>
        /// <remarks>O valor retorna no parametro out </remarks>
        /// <param name="comando">Comando SQL que deve retornar apenas um campo</param>
        /// <param name="retorno">Indica se foi encontrado um valor</param>
        /// <returns>retorna se a busca retornou um valor v�lido</returns>
        public bool BuscaEscalar(string comando, out decimal retorno)
        {
            return BuscaEscalar(comando, out retorno, null);
        }

        /// <summary>
        /// Executa uma busca escalar
        /// </summary>
        /// <remarks>O valor retorna no parametro out </remarks>
        /// <param name="comando">Comando SQL que deve retornar apenas um campo</param>
        /// <param name="retorno">Indica se foi encontrado um valor</param>
        /// <param name="Paramentros">Parametos do camando marcados como @P1,@P2... </param>
        /// <returns>retorna se a busca retornou um valor v�lido</returns>
        public bool BuscaEscalar(string comando, out object retorno, params object[] Paramentros)
        {
            retorno = ExecutarSQL(comando, ComandosBusca.Escalar, Paramentros);
            return (retorno != null);
        }

        /// <summary>
        /// Executa uma busca escalar
        /// </summary>
        /// <remarks>O valor retorna no parametro out </remarks>
        /// <param name="comando">Comando SQL que deve retornar apenas um campo</param>
        /// <param name="retorno">Indica se foi encontrado um valor</param>
        /// <param name="Paramentros">Parametos do camando marcados como @P1,@P2... </param>
        /// <returns>retorna se a busca retornou um valor v�lido</returns>
        public bool BuscaEscalar(string comando, out Int16 retorno, params object[] Paramentros)
        {
            retorno = 0;
            object oRet = ExecutarSQL(comando, ComandosBusca.Escalar, Paramentros);
            if (oRet != null)
            {
                try
                {
                    retorno = Convert.ToInt16(oRet);
                    return true;
                }
                catch (InvalidCastException)
                {
                    return false;
                }
            }
            else
                return false;
        }

        /// <summary>
        /// Executa uma busca escalar
        /// </summary>
        /// <remarks>O valor retorna no parametro out </remarks>
        /// <param name="comando">Comando SQL que deve retornar apenas um campo</param>
        /// <param name="retorno">Indica se foi encontrado um valor</param>
        /// <param name="Paramentros">Parametos do camando marcados como @P1,@P2... </param>
        /// <returns>retorna se a busca retornou um valor v�lido</returns>
        public bool BuscaEscalar(string comando, out Int32 retorno, params object[] Paramentros)
        {
            retorno = 0;
            object oRet = ExecutarSQL(comando, ComandosBusca.Escalar, Paramentros);
            if (oRet != null)
            {
                try
                {
                    retorno = Convert.ToInt32(oRet);
                    return true;
                }
                catch (InvalidCastException)
                {
                    return false;
                }
            }
            else
                return false;
        }

        /// <summary>
        /// Executa uma busca escalar
        /// </summary>
        /// <remarks>O valor retorna no parametro out </remarks>
        /// <param name="comando">Comando SQL que deve retornar apenas um campo</param>
        /// <param name="retorno">Indica se foi encontrado um valor</param>
        /// <param name="Paramentros">Parametos do camando marcados como @P1,@P2... </param>
        /// <returns>retorna se a busca retornou um valor v�lido</returns>
        public bool BuscaEscalar(string comando, out Int64 retorno, params object[] Paramentros)
        {
            retorno = 0;
            object oRet = ExecutarSQL(comando, ComandosBusca.Escalar, Paramentros);
            if (oRet != null)
            {
                try
                {
                    retorno = Convert.ToInt64(oRet);
                    return true;
                }
                catch (InvalidCastException)
                {
                    return false;
                }
            }
            else
                return false;
        }

        /// <summary>
        /// Executa uma busca escalar
        /// </summary>
        /// <remarks>O valor retorna no parametro out </remarks>
        /// <param name="comando">Comando SQL que deve retornar apenas um campo</param>
        /// <param name="retorno">Indica se foi encontrado um valor</param>
        /// <param name="Paramentros">Parametos do camando marcados como @P1,@P2... </param>
        /// <returns>retorna se a busca retornou um valor v�lido</returns>
        public bool BuscaEscalar(string comando, out string retorno, params object[] Paramentros)
        {
            retorno = "";
            object oRet = ExecutarSQL(comando, ComandosBusca.Escalar, Paramentros);
            if (oRet != null)
            {
                retorno = oRet.ToString();
                return true;
            }
            else
                return false;
        }

        /// <summary>
        /// Executa uma busca escalar
        /// </summary>
        /// <remarks>O valor retorna no parametro out </remarks>
        /// <param name="comando">Comando SQL que deve retornar apenas um campo</param>
        /// <param name="retorno">Indica se foi encontrado um valor</param>
        /// <param name="Paramentros">Parametos do camando marcados como @P1,@P2... </param>
        /// <returns>retorna se a busca retornou um valor v�lido</returns>
        public bool BuscaEscalar(string comando, out decimal retorno, params object[] Paramentros)
        {
            retorno = 0;
            object oRet = ExecutarSQL(comando, ComandosBusca.Escalar, Paramentros);
            if (oRet != null)
            {
                try
                {
                    retorno = Convert.ToDecimal(oRet);
                    return true;
                }
                catch (InvalidCastException)
                {
                    return false;
                }
            }
            else
                return false;
        }

        /// <summary>
        /// Executa uma busca escalar
        /// </summary>
        /// <remarks>O valor retorna no parametro out </remarks>
        /// <param name="comando">Comando SQL que deve retornar apenas um campo</param>
        /// <param name="retorno">Indica se foi encontrado um valor</param>
        /// <param name="Paramentros">Parametos do camando marcados como @P1,@P2... </param>
        /// <returns>retorna se a busca retornou um valor v�lido</returns>
        public bool BuscaEscalar(string comando, out bool retorno, params object[] Paramentros)
        {
            retorno = false;
            object oRet = ExecutarSQL(comando, ComandosBusca.Escalar, Paramentros);
            if ((oRet != null) && (oRet != DBNull.Value))
            {
                try
                {
                    retorno = Convert.ToBoolean(oRet);
                    return true;
                }
                catch (InvalidCastException)
                {
                    return false;
                }
            }
            else
                return false;
        }

        /// <summary>
        /// Executa uma busca escalar
        /// </summary>
        /// <remarks>O valor retorna no parametro out </remarks>
        /// <param name="comando">Comando SQL que deve retornar apenas um campo</param>
        /// <param name="retorno">Indica se foi encontrado um valor</param>
        /// <param name="Paramentros">Parametos do camando marcados como @P1,@P2... </param>
        /// <returns>retorna se a busca retornou um valor v�lido</returns>
        public bool BuscaEscalar(string comando, out DateTime retorno, params object[] Paramentros)
        {
            retorno = DateTime.MinValue;
            object oRet = ExecutarSQL(comando, ComandosBusca.Escalar, Paramentros);
            if (oRet != null)
            {
                try
                {
                    retorno = Convert.ToDateTime(oRet);
                    return true;
                }
                catch (InvalidCastException)
                {
                    return false;
                }
            }
            else
                return false;
        }

        /// <summary>
        /// Executa uma busca escalar
        /// </summary>
        /// <remarks>O valor retorna no parametro out </remarks>
        /// <param name="comando">Comando SQL que deve retornar apenas um campo</param>
        /// <param name="retorno">Indica se foi encontrado um valor</param>        
        /// <returns>retorna se a busca retornou um valor v�lido</returns>
        public bool BuscaEscalar(string comando, out DateTime retorno)
        {
            return BuscaEscalar(comando, out retorno, null);
        }

        /// <summary>
        /// Executa uma busca escalar
        /// </summary>
        /// <remarks>O comando deve ser um select que retorna 1  </remarks>
        /// <example>Select 1 from tabela where campo = valor</example>
        /// <param name="comando">Comando SQL que deve retornar apenas um campo</param>        
        /// <param name="Paramentros">Parametos do camando marcados como @P1,@P2... </param>
        /// <returns>retorna se o registro existe</returns>
        public bool EstaCadastrado(string comando, params object[] Paramentros)
        {
            object oRet = ExecutarSQL(comando, ComandosBusca.Escalar, Paramentros);
            return (oRet != null);
        }

        /// <summary>
        /// Executa uma busca escalar
        /// </summary>
        /// <remarks>O comando deve ser um select que retorna 1  </remarks>
        /// <example>Select 1 from tabela where campo = valor</example>
        /// <param name="comando">Comando SQL que deve retornar apenas um campo</param>                
        /// <returns>retorna se o registro existe</returns>
        public bool EstaCadastrado(string comando)
        {
            object oRet = ExecutarSQL(comando, ComandosBusca.Escalar);
            return (oRet != null);
        }


        #endregion

        #region ExecutarNonQuery

        /// <summary>
        /// Executa um comando SQL
        /// </summary>
        /// <returns>N�mero de registros afetados</returns>
        public int ExecutarSQLNonQuery(string comando)
        {
            return ExecutarSQLNonQuery(comando, null);
        }

        /// <summary>
        /// Executa um comando SQL
        /// </summary>
        /// <remarks>Os paramentros devem obrigatoriamentes ser nomeados como @P1, @P2 ...</remarks>
        /// <param name="comando">Comando SQL: select, update ou delete</param>
        /// <param name="Parametros">Parametos do camando marcados como @P1,@P2... </param>
        /// <returns>N�mero de registros afetados</returns>
        public int ExecutarSQLNonQuery(string comando, params object[] Parametros)
        {
            return (int)ExecutarSQL(comando, ComandosBusca.NonQuery, Parametros);
        }




        #endregion

        #region Conexao
        /// <summary>
        /// Cria uma nova conexao manualmente
        /// </summary>
        /// <param name="StringDeConexao">"string de conexao"</param>
        public abstract void CriaCone(string StringDeConexao);

        /// <summary>
        /// Se > do que zero nao fecha a conex�o
        /// </summary>
        protected int ManterConeAberta;

        /// <summary>
        /// Fecha a conexao
        /// </summary>
        /// <returns></returns>
        protected abstract bool FechaCone();

        /// <summary>
        /// Abre a conexao
        /// </summary>
        /// <returns></returns>
        protected abstract bool AbreCone();

        #endregion

        #region Comandos insert

        /// <summary>
        /// Insere todos os registro
        /// </summary>
        /// <param name="NomeTabela">Nome da tabela destino</param>
        /// <param name="Tabela">Tabela com os dados</param>
        /// <returns>n�mero de registros inclu�dos</returns>
        public int Insert(string NomeTabela, DataTable Tabela)
        {
            return Insert(NomeTabela, Tabela, false, true);
        }

        /// <summary>
        /// Total de itens a processar
        /// </summary>
        public int Total;
        /// <summary>
        /// Itens j� processados
        /// </summary>
        public int n_Total;
        /// <summary>
        /// Falhas no processamento
        /// </summary>
        public int falhas;
        /// <summary>
        /// Itens processados com sucesso
        /// </summary>
        public int ok;

        /// <summary>
        /// Insere todos os registro
        /// </summary>
        /// <param name="NomeTabela">Nome da tabela destino</param>
        /// <param name="Tabela">Tabela com os dados</param>
        /// <param name="DesligarAutoinc"></param>
        /// <param name="IgnorarNomes"></param>
        /// <returns>n�mero de registros inclu�dos</returns>
        public int Insert(string NomeTabela, DataTable Tabela,bool DesligarAutoinc, bool IgnorarNomes)
        {
            Total = Tabela.Rows.Count;
            n_Total = falhas = ok = 0;
            foreach (DataRow DR in Tabela.Rows)
            {
                n_Total++;
                if (Insert(NomeTabela, DR, DesligarAutoinc, IgnorarNomes))
                    ok++;
                else
                    falhas++;
            }
            return n_Total;
        }

        /// <summary>
        /// Insere todos os registro detro de um try para cada um. afeta as vari�veis n_Total, ok e falhas
        /// </summary>
        /// <param name="NomeTabela"></param>
        /// <param name="Tabela"></param>
        /// <param name="DesligarAutoinc"></param>
        /// <param name="IgnorarNomes"></param>
        /// <returns></returns>
        public int Insert_try(string NomeTabela, DataTable Tabela, bool DesligarAutoinc, bool IgnorarNomes)
        {
            Total = Tabela.Rows.Count;
            n_Total = 0;
            ok = 0;
            falhas = 0;
            foreach (DataRow DR in Tabela.Rows)
            {
                try
                {
                    n_Total++;
                    if (Insert(NomeTabela, DR, DesligarAutoinc, IgnorarNomes))
                        ok++;
                }
                catch
                {
                    falhas++;                   
                }
            }
            return n_Total;
        }

        /// <summary>
        /// Insere os dados da linha
        /// </summary>
        /// <param name="NomeTabela">Nome da tabela destino</param>
        /// <param name="DR">DataRow com os dados</param>        
        public bool Insert(string NomeTabela, DataRow DR)
        {
            return Insert(NomeTabela, DR,false,true);                   
        }

        /// <summary>
        /// Insere um registro com os dados da row
        /// </summary>        
        /// <param name="DR"></param>
        /// <param name="DesligarAutoinc">Desliga os autoincrementos</param>
        /// <param name="IgnorarNomes">Insere os campos da row na ordem ignorando os nomes dos campos</param>
        /// <returns>se a linha foi inserida</returns>
        public bool Insert(DataRow DR, bool DesligarAutoinc, bool IgnorarNomes)
        {
            return Insert("", DR, DesligarAutoinc, IgnorarNomes);
        }

        /// <summary>
        /// Insere um registro com os dados da row
        /// </summary>
        /// <param name="Tabela">Tabela destino</param>
        /// <param name="DR"></param>
        /// <param name="DesligarAutoinc">Desliga os autoincrementos</param>
        /// <param name="IgnorarNomes">Insere os campos da row na ordem ignorando os nomes dos campos</param>
        /// <returns>se a linha foi inserida</returns>
        public bool Insert(string Tabela, DataRow DR, bool DesligarAutoinc, bool IgnorarNomes/*,bool UsarSource*/)
        {
            ArrayList Parametros = new ArrayList();
            if (Tabela == "")
                Tabela = DR.Table.TableName;
            if (IgnorarNomes)
                return InsertValores(Tabela, DR.ItemArray) > 0;
            else
            {
                string Virgula = "";
                string strCampos = "";
                string strValores = "";
                //object[] paramentros = new object[];
                //string comando = "insert into " + Tabela + " VALUES(";                
                int nPar = 1;
                for (int i = 0; i < DR.ItemArray.Length; i++)
                {
                    if (DR[i] == DBNull.Value)
                        continue;
                    if (DR.Table.Columns[i].AutoIncrement && !DesligarAutoinc)
                        continue;
                    strCampos += Virgula + DR.Table.Columns[i].ColumnName;
                    strValores += String.Format("{0}@P{1}", Virgula, nPar);
                    Parametros.Add(DR[i]);
                    Virgula = ",";
                    nPar++;
                };
                string comando = String.Format("insert into {0} ({1}) values({2})", Tabela, strCampos, strValores);
                int retorno;
                try
                {
                    ManterConeAberta++;
                    if (DesligarAutoinc)                                            
                        ExecutarSQLNonQuery(string.Format("SET IDENTITY_INSERT {0} ON;",Tabela));                    
                    retorno = ExecutarSQLNonQuery(comando, Parametros.ToArray());                    
                }
                finally
                {
                    ManterConeAberta--;
                    if (DesligarAutoinc)
                        ExecutarSQLNonQuery(string.Format("SET IDENTITY_INSERT {0} OFF;", Tabela));
                }
                return retorno > 0;
            }
        }



        /// <summary>
        /// Insere um registro como os dados dos parametros
        /// </summary>
        /// <param name="Tabela">Nome da tabela destino</param>
        /// <param name="Valores">Dados a inserir</param>
        /// <returns>linha inseridas</returns>
        public int InsertValores(string Tabela, params object[] Valores)
        {
            string comando = String.Format("insert into {0} VALUES(", Tabela);
            for (int i = 1; i < Valores.Length + 1; i++)
            {
                if (i != 1)
                    comando += ",";
                comando += String.Format("@P{0}", i);
            };
            comando += ")";
            return ExecutarSQLNonQuery(comando, Valores);
        }

        /// <summary>
        /// Executa o comando e retorna o ID
        /// </summary>
        /// <param name="comando">comando SQL</param>
        /// <param name="Parametros">Parametos do camando marcados como @P1,@P2... </param>
        /// <remarks>O comando deve ser do tipo insert e com um campo auto inc na tabela</remarks>
        /// <returns>ID do novo registro. Retorna -1 se der falha</returns>
        public abstract int IncluirAutoInc(string comando, params object[] Parametros);

        /// <summary>
        /// Executa o comando e retorna o ID
        /// </summary>
        /// <param name="comando"></param>
        /// <returns>ID do novo registro. Retorna -1 se der falha</returns>
        public int IncluirAutoInc(string comando)
        {
            return IncluirAutoInc(comando, null);
        } 
        #endregion

        #region Transacao
        /// <summary>
        /// Indica quantas vezes foram solicitadas aberturas de trans
        /// </summary>
        protected int SubTrans;

        /// <summary>
        /// Nives de transa��o
        /// </summary>
        public int GetSubTrans 
        {
            get 
            {
                return SubTrans;
            }
        }

        /// <summary>
        /// Indica se esta na tras ST ou na transa��o de doutro adapter
        /// </summary>
        protected abstract bool EstaEmTransST { get; }
        
        /// <summary>
        /// 
        /// </summary>
        protected ArrayList VirtualTableAdaptersImplantados = new ArrayList();        

        

        /*
        /// <summary>
        /// Inicia uma transacao Local e implanta nos VirtualTableAdapters, Usar DENTRO do try com vircath
        /// Implementado na sobrecarga
        /// </summary>        
        /// <param name="Identificador">Identifica a chamada</param>
        /// <param name="VirtualTableAdapters">VirtualTableAdapters a serem implantados</param>
        /// <returns></returns>        
        public virtual bool AbreTrasacaoLF(string Identificador, params VirtualTableAdapter[] VirtualTableAdapters)
        {
            return AbreTrasacaoLocal(Identificador, IsolationLevel.ReadCommitted, VirtualTableAdapters);
        }*/
        


        /// <summary>
        /// Inicia uma transacao Local e implanta nos VirtualTableAdapters, Usar DENTRO do try com vircath
        /// </summary>
        /// <param name="Identificador">Identifica a chamada</param>        
        /// <param name="VirtualTableAdapters">VirtualTableAdapters a serem implantados</param>
        /// <returns></returns>        
        public bool AbreTrasacaoLocal(string Identificador, params VirtualTableAdapter[] VirtualTableAdapters)
        {
            if (VircatchRetornando)
            {
                LogVircatchRetornando = Log.ToString();
                Vircatch(new Exception("TRAVA VircatchRetornando"));                
            }
            if (AbreTrasacao(Identificador, IsolationLevel.ReadCommitted))
            {
                Array.ForEach(VirtualTableAdapters,
                delegate(VirtualTableAdapter TaFilha)
                {
                    if (TaFilha != this)
                        if (TaFilha.EntrarEmTrans(this))
                            VirtualTableAdaptersImplantados.Add(TaFilha);
                });
                return true;
            }
            else
                return false;
        }

        /// <summary>
        /// Retorna se a transa�ao est�tica esta ativa
        /// </summary>
        /// <returns></returns>
        public abstract bool STTransAtiva(); 

       /*
        /// <summary>
        /// Entra na transa��o se existir.
        /// </summary>
        /// <param name="VirtualTableAdapters">lista de tableadapters</param>
        /// <returns>se estava em transa��o</returns>
        [Obsolete("usar metodo est�tico")]
        public bool EmbarcaEmTrans(params VirtualTableAdapter[] VirtualTableAdapters)
        {
            if (EstaEmTransST)
                throw new Exception("uso incorreto de EmbarcaEmTrans.");
            if (SubTrans == 0)
                return false;
            Array.ForEach(VirtualTableAdapters,
                delegate(VirtualTableAdapter TaFilha)
                {
                    if (TaFilha.EntrarEmTrans(this))
                        VirtualTableAdaptersImplantados.Add(TaFilha);
                });
            return true;
        }*/

        /*
        /// <summary>
        /// Entra na transa��o ST se existir.
        /// </summary>        
        /// <returns>se estava em transa��o</returns>
        public virtual bool EmbarcaEmTransST()
        {            
            return STTableAdapterEspecifico.EmbarcaEmTrans(this);            
        }*/

        private void RegistraIdentificadorChamada(int Nivel, string Identificador)
        {
            if (IdentificadorChamada == null)
                IdentificadorChamada = new SortedList<int, string>();
            if (IdentificadorChamada.ContainsKey(Nivel))
                IdentificadorChamada[Nivel] = Identificador;
            else
                IdentificadorChamada.Add(Nivel, Identificador);
        }

        /// <summary>
        /// Inicia uma transacao, Usar DENTRO do try com vircath
        /// </summary>
        /// <remarks>Se uma transa��o j� estiver em processo incrementa a vari�vel SubTrans</remarks>
        /// <param name="Identificador">Identifica a origem da transa��o</param>
        /// <param name="Nivel">Tipo de transa��o</param>
        /// <returns>Se a transa��o foi inicianda</returns>
        protected bool AbreTrasacao(string Identificador = "N�o identificado 3", IsolationLevel Nivel = IsolationLevel.ReadCommitted)
        {
            bool retorno;
            if (SubTrans == 0)
            {
                Log.Clear();
                retorno = IniciaTrasacaoReal(Nivel);
                DateTimeInicioTrans = DateTime.Now;
                RegistraIdentificadorChamada(0, Identificador);
                if (GeraLog)
                    Log.AppendFormat("AbreTrasacao\tIdentificador:{0}\r\n", Identificador);
            }
            else
            {
                RegistraIdentificadorChamada(SubTrans, Identificador);
                if (GeraLog)
                    Log.AppendFormat("* AbreTrasacao\tIdentificador:{0}\tNivel:{1}\r\n", Identificador, SubTrans);
                retorno = true;
            }
            if (retorno)
                SubTrans++;
            return retorno;
        }

        /// <summary>
        /// Inicia uma transacao
        /// </summary>
        /// <remarks>este m�todo deveria se abstrato pois est� implementado nos componentes filhos</remarks>
        /// <param name="Nivel">Tipo de transa��o</param>
        protected abstract bool IniciaTrasacaoReal(IsolationLevel Nivel);

        /*
        /// <summary>
        /// Inicia uma trasacao com nivel padrao, Usar DENTRO do try com vircath
        /// </summary>
        /// <returns></returns>
        public bool AbreTrasacao()
        {
            return AbreTrasacao(IsolationLevel.ReadCommitted);
        }*/

        /// <summary>
        /// TableAdapter est�tico
        /// </summary>
        protected abstract VirtualTableAdapter STTableAdapterEspecifico { get;}

        /*
        /// <summary>
        /// Inicia uma trasacao Estatica. Na verdade coloca o componente na transacao de STTableAdapter
        /// </summary>
        /// <remarks>Este m�todo � para que v�rios objetos participem da mesma transacao: A transa��o de STTableAdapter\r\nGera exce��o se STTableAdapter nao estiver em transacao</remarks>                               
        private bool IniciaTrasacaoST()
        {
            return EntraEmSTTrans(STTableAdapterEspecifico);
        }
        */

        /// <summary>
        /// Coloca o componente na transacao de Dono_Trans que pode ser STTableAdapter
        /// </summary>
        /// <param name="Dono_Trans">Tableadapte que ja esta em transa��o</param>
        /// <returns></returns>
        protected abstract bool EntrarEmTrans(VirtualTableAdapter Dono_Trans);

        /// <summary>
        /// Coloca o adapter sob a transacao Tran ajustando todos os Command internos
        /// </summary>
        /// <param name="Inicia">Inicio ou termino</param>
        protected abstract void ImplantaTrans(bool Inicia);

        /// <summary>
        /// Termina uma transa��o com commit. Usar para grupo (st)
        /// </summary>        
        /// <returns>Retorna true se a opera��o foi executada</returns>
        /// <remarks>Se estiver em sub-transacao decrementa SubTrans.</remarks>        
        public bool Commit()
        {
            if (EstaEmTransST)
            {
                if (GeraLog)
                    Log.AppendFormat("Uso icorreto\tIdentificador:{0}\r\n", IdentificadorChamada[SubTrans]);
                throw new Exception(string.Format("Erro em Commit. Uso incorreto Identificador: {0}", IdentificadorChamada));
                //N�o deveria ocorrer. Seria o caso de chamar o commit masis vezes do que o numero de subtrans.
            }
            if (SubTrans == 0)
            {
                if (GeraLog)
                    Log.AppendFormat("??? Commit n�o previsto\r\n");
                //throw new Exception(string.Format("Erro em Commit. Identificador: {0}", IdentificadorChamada));
                //n�o gerar erro porque pode estar correto
                return false;
            }
            SubTrans--;
            if (SubTrans == 0)
            {
                bool ok = FimTrasacaoReal(true);
                if (ok)
                    foreach (VirtualTableAdapter TaFilha in VirtualTableAdaptersImplantados)
                        TaFilha.FimTrasacaoElementoLista(true);
                VirtualTableAdaptersImplantados.Clear();
                if (GeraLog)
                {
                    TimeSpan TS = (TimeSpan)(DateTime.Now - DateTimeInicioTrans);
                    Log.AppendFormat("Commit\tIdentificador:{0}\tTempo:\t{1:n4}\ts\r\n", IdentificadorChamada[SubTrans], TS.TotalSeconds);
                }
                DateTimeInicioTrans = null;
                IdentificadorChamada = null;
                //Log.Clear();
                return ok;
            }
            else
            {
                if (GeraLog)
                    Log.AppendFormat("* Commit\tIdentificador:{0}\tNivel:{1}\r\n", IdentificadorChamada[SubTrans], SubTrans);
                return true;
            }
        }

        /// <summary>
        /// Termina uma transa��o com commit. N�o grupo
        /// </summary>        
        /// <returns>Retorna true se a opera��o foi executada</returns>
        /// <remarks>Se estiver em sub-transacao decrementa SubTrans.</remarks>        
        private bool FimTrasacaoElementoLista(bool Commitar)
        {
            //ser� sim quando chamado por um VirtualTableAdapter que nao � o ST e est� em transa��o
            if (!EstaEmTransST)
                throw new Exception("Uso incorreto de FimTrasacaoCInd");            
            //EstaEmTransST = false;
            return FimTrasacaoReal(Commitar);
            
        }

        /// <summary>
        /// Termina uma transa��o
        /// </summary>        
        /// <returns>Retorna true se a opera��o foi executada</returns>
        /// <remarks>Se estiver em sub-transacao decrementa SubTrans no caso de commit e gera uma exce��o no caso de rollback (para provocar rollback na transa�o principal)\r\nDeve estar em bloco protegido try-cauth</remarks>        
        public bool RollBack(string Motivo)
        {
            //ser� sim quando chamado por um VirtualTableAdapter que nao � o ST e est� em transa��o
            if (EstaEmTransST)            
                throw new Exception("Uso incorreto de FimTrasacaoRGrupo"); 
            
            
            //N�o deveria ocorrer. Seria o caso de chamar o commit masis vezes do que o numero de subtrans.
            if (SubTrans == 0)
                    return true;
            SubTrans--;
            if (SubTrans == 0)
            {
                bool ok = FimTrasacaoReal(false);
                if (ok)
                    foreach (VirtualTableAdapter TaFilha in VirtualTableAdaptersImplantados)
                        TaFilha.FimTrasacaoElementoLista(false);
                VirtualTableAdaptersImplantados.Clear();
                DateTimeInicioTrans = null;
                IdentificadorChamada = null;
                //Log.Clear();
                return ok;
            }
            else
            {
                SubTrans++;
                //Vai decrementar novamente pois vamos re-direcionar pra o cath da mesma sub-trans
                throw new Exception(Motivo ?? "Cancelado!");
            }
            
        }

        /// <summary>
        /// Vari�vel que indica que ocorreu uma excess�o e est� em um processo de retorno via vircatch at� o primeiro
        /// </summary>
        public bool VircatchRetornando = false;

        /// <summary>
        /// Termina uma transa��o com Rollback - Usar em bloco cath
        /// A fun��o do virch � fazer o Rollback quando ocorrer um exception (pode ter sido provoda por uma subtran)
        /// </summary>        
        /// <returns>Retorna true se a opera��o foi executada</returns>
        /// <param name="e">Exception anterior - usar null se for a primeira de transa��o dupla (dois bancos)</param>
        /// <remarks>Se estiver em sub-transacao decrementa SubTrans</remarks>        
        public bool Vircatch(Exception e)
        {
            if (e != null)
                UltimoErro = e;
            if (SubTrans == 0) // n�o esta mais em transa��o
            {
                if (VircatchRetornando)
                    throw new Exception("Erro VircatchRetornando 1");
                return true;
            }
            SubTrans--;
            if (SubTrans == 0)
            {
                bool ok = FimTrasacaoReal(false);
                if (ok)
                    foreach (VirtualTableAdapter TaFilha in VirtualTableAdaptersImplantados)
                        TaFilha.FimTrasacaoElementoLista(false);
                VirtualTableAdaptersImplantados.Clear();
                if (GeraLog && (e!=null))
                {
                    TimeSpan TS = (TimeSpan)(DateTime.Now - DateTimeInicioTrans);
                    Log.AppendFormat("Vircatch\tIdentificador:{0}\tTempo:\t{1:n4}\ts\t{2}\r\n", IdentificadorChamada[SubTrans], TS.TotalSeconds, e.Message);
                }
                DateTimeInicioTrans = null;
                IdentificadorChamada.Clear();
                VircatchRetornando = false;
                return ok;
            }
            else
            {
                VircatchRetornando = true;
                try
                {
                    Log.AppendFormat("* Vircatch\tIdentificador:{0}\tSubtans {1}\t{2}\r\n", IdentificadorChamada[SubTrans], SubTrans, e == null ? "NULL" : e.Message);
                }
                catch (Exception sub_erro)
                {
                    Log.AppendFormat("* Vircatch com suberro\tSubtans {0}\t{1}\r\n", SubTrans, sub_erro == null ? "NULL" : sub_erro.Message);
                }
                finally
                {
                    if (e != null) //o e sera nulo no caso de dois bancos. so o segundo deve gerar o throw (o primeiro foi chamado com e = null)
                        throw new Exception("Erro em transa��o - " + e.Message, e);                                            
                }
                return true;
            }
        }

        /// <summary>
        /// Termina uma transa��o
        /// </summary>
        /// <remarks>este m�todo deveria se abstrato pois est� implementado nos componentes filhos</remarks>
        protected abstract bool FimTrasacaoReal(bool Commit);
        

        #endregion

        #region ExecutarSQL Gen�rico
        /// <summary>
        /// Executa um comando SQL
        /// </summary>
        /// <remarks>Os paramentros devem obrigatoriamentes ser nomeados como @P1, @P2 ...</remarks>
        /// <param name="comando">Comando SQL: select, update ou delete</param>
        /// <param name="TipoComando">
        /// Tipo de a��o/retorno
        /// pode retornar um unico registro em forma de ArrayList,
        /// escalar,
        /// DataTable
        /// ou ArrayList com os valores do primeiro registro
        /// </param>        
        public object ExecutarSQL(string comando, ComandosBusca TipoComando)
        {
            return ExecutarSQL(comando, TipoComando, null);
        }


        /// <summary>
        /// este m�todo deveria se abstrato pois est� implementado nos componentes filhos
        /// </summary>
        public abstract object ExecutarSQL(string comando, ComandosBusca TipoComando, params object[] Paramentros);
        




        #endregion

        #region Tabela Viva

        /// <summary>
        /// Ultima leitura
        /// </summary>
        private DateTime UltimaLeitura = DateTime.MinValue;
        //private MethodInfo _GetNovosDados;
        private ArrayList _GetNovosDados;
        private DataTable _Tabela;

        /// <summary>
        /// M�todo de busca de altera��es. O default � GetAtualizacao
        /// </summary>
        public ArrayList GetNovosDados {
            get 
            {
                if (_GetNovosDados == null)
                {
                    _GetNovosDados = new ArrayList();
                    int Contador = 0;
                    MethodInfo GetNovosDadosPadrao;
                    do
                    {
                        GetNovosDadosPadrao = GetType().GetMethod(String.Format("GetAtualizacao{0}", (Contador == 0 ? "" : "_" + Contador.ToString())), new Type[] { typeof(DateTime) });
                        Contador++;
                        if (GetNovosDadosPadrao != null)
                            _GetNovosDados.Add(GetNovosDadosPadrao);
                    } while (GetNovosDadosPadrao != null);
                    
                    
                }
                if (_GetNovosDados.Count == 0)
                    Array.ForEach(GetType().GetMethods(),
                    delegate(MethodInfo mif)
                    {
                        if ((mif.ReturnType == typeof(DataTable)) || (mif.ReturnType.IsSubclassOf(typeof(DataTable))))
                            if (mif.GetParameters().Length == 1)
                                if (mif.GetParameters()[0].ParameterType == typeof(DateTime))
                                    _GetNovosDados.Add(mif);
                    });                                                                    
                return _GetNovosDados; 
            }
            set { _GetNovosDados = value; }
        }

        /// <summary>
        /// Tabela que � carregada por este TableAdapter
        /// </summary>
        /// <remarks> forncer na cria��o da instancia</remarks>
        public DataTable TabelaDataTable
        {
            get {                
                return _Tabela; 
            }
            set { _Tabela = value; }
        }

        /// <summary>
        /// Atuliza os registros desde a ultima leitura
        /// </summary>
        /// <returns>� melhor usar o m�todo AtivarAtualizacao</returns>
        public int AtualizaDados()
        {
            int NNovos = 0;
            if ((UltimaLeitura == DateTime.MinValue) || (TabelaDataTable == null) || (GetNovosDados == null))
                return 0;
            DataRow RowAntiga;
            DateTime NovaUltimaLeitura = DataBanco();

            //bool primeiro = true;
            foreach (MethodInfo MetodoGet in GetNovosDados)
            {
                DataTable Novos = (DataTable)MetodoGet.Invoke(this, new object[] { UltimaLeitura });
                NNovos += Novos.Rows.Count;
                foreach (DataRow novaRow in Novos.Rows)
                {
                    RowAntiga = TabelaDataTable.Rows.Find(novaRow[0]);
                    if (RowAntiga == null)
                        RowAntiga = TabelaDataTable.NewRow();
                    if ((RowAntiga.RowState == DataRowState.Unchanged) || (RowAntiga.RowState == DataRowState.Detached))
                    {
                        foreach (DataColumn Coluna in TabelaDataTable.Columns)
                            if (!Coluna.ReadOnly)
                                if (novaRow[Coluna.ColumnName] != DBNull.Value)
                                    RowAntiga[Coluna.ColumnName] = novaRow[Coluna.ColumnName];
                        if (RowAntiga.RowState == DataRowState.Detached)
                            TabelaDataTable.Rows.Add(RowAntiga);
                        RowAntiga.AcceptChanges();
                    }
                };
                //primeiro = false;
            };

            UltimaLeitura = NovaUltimaLeitura;
            return NNovos;
        }

        private ArrayList _Controladores;
        private ArrayList Controladores {
            get {
                if (_Controladores == null)
                    _Controladores = new ArrayList();
                return _Controladores;
            }
        }

        /*
        private System.Windows.Forms.Timer _TimerAtualizacao;
        private System.Windows.Forms.Timer TimerAtualizacao
        {
            get {
                if (_TimerAtualizacao == null) {
                    _TimerAtualizacao = new System.Windows.Forms.Timer();
                    _TimerAtualizacao.Interval = 10000;
                    _TimerAtualizacao.Tick += OnTimedEvent;

                }
                return _TimerAtualizacao;
            }
        }*/

        /*
        /// <summary>
        /// Ativa ou desativa a atualiza��o da tabela
        /// </summary>
        /// <param name="Ligar"></param>
        /// <param name="Controlador"></param>
        private void AtivarAtualizacao(bool Ligar,int Controlador) 
        {
            if (Ligar)
            {
                if (!Controladores.Contains(Controlador))
                    Controladores.Add(Controlador);
            }
            else
                Controladores.Remove(Controlador);
            //TimerAtualizacao.Enabled = (Controladores.Count > 0);
            TimerAtualizacao.Enabled = false;
            //funcao desligada
        }

        bool EvitaReentrada;
        */                    
        /*
        private void OnTimedEvent(object source, EventArgs e)
        {
            //funcao desligada
            return;
            if (EvitaReentrada)
                return;
            EvitaReentrada = true;
            AtualizaDados();
            EvitaReentrada = false;
        }
        */
        /// <summary>
        /// Marca a data de corte
        /// </summary>
        /// <returns>� sobregravado para pegar a hora do servidor data do servidor</returns>
        public DateTime ResetAtualizacao() 
        {
            UltimaLeitura = DataBanco();
            return UltimaLeitura;
        }

        /// <summary>
        /// 5 segundos
        /// </summary>
        /// <returns></returns>
        protected virtual DateTime DataBanco()
        {
            return DateTime.Now.AddSeconds(-5);            
        }

        #endregion

        /// <summary>
        /// Ajusta os campos autoinc com valore do banco. aplicavel para FB
        /// </summary>
        /// <param name="rowNova"></param>
        /// <returns></returns>
        public virtual int IniciaLinha(DataRow rowNova)
        {
            return 0;
        }

    }
}
