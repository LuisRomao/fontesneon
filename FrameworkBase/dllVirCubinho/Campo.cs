﻿using System;
using System.Collections.Generic;
using System.Text;


namespace dllVirCubinho
{
    public class Campo
    {
        private static List<Campo> _ConjutoCampos;

        public static List<Campo> ConjutoCampos
        {
            get { return _ConjutoCampos ?? (_ConjutoCampos = new List<Campo>()); }
        }

        public static bool ConjutoCamposDefinido
        {
            get { return (_ConjutoCampos != null); }
        }

        public static Campo GetCampo(string Nome)
        {
            if (_ConjutoCampos != null)
                foreach (Campo Candidato in ConjutoCampos)
                    if (Candidato.NomeCampo == Nome)
                        return Candidato;
            return null;

        }

        public string Descricao;        
        public string NomeCampo;
        public Type TypeCampo;
        public int LarguraColuna;
        public GrupoCampos Grupo;
        

        public Campo(string _Descricao,string _NomeCampo,Type _TypeCampo,int _LarguarColuna,GrupoCampos _Grupo)
        {
            Descricao = _Descricao;
            NomeCampo = _NomeCampo;
            TypeCampo = _TypeCampo;
            LarguraColuna = _LarguarColuna;
            if (_Grupo != null)
            {
                Grupo = _Grupo;
                Grupo.CamposdoGrupo.Add(this); 
            }
        }

        public override string ToString()
        {
            if (Grupo != null)
                return string.Format("{0}: {1}", Grupo, Descricao);
            else
                return string.Format("{0}", Descricao);
        }
    }

    public class GrupoCampos
    {
        private static List<GrupoCampos> _ConjutoGrupos;

        public static List<GrupoCampos> ConjutoGrupos
        {
            get { return _ConjutoGrupos ?? (_ConjutoGrupos = new List<GrupoCampos>()); }
        }

        public static bool GruposDefinidos
        {
            get { return (_ConjutoGrupos != null); }
        }

        public static GrupoCampos GrupoDetalhar;

        public string Descricao;
        public List<Campo> CamposdoGrupo;

        public GrupoCampos(string _Descricao)
        {
            Descricao = _Descricao;
            CamposdoGrupo = new List<Campo>();
        }

        public override string ToString()
        {
            return Descricao;            
        }
    }
}
