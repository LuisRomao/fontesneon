﻿namespace dllVirCubinho
{
    partial class cVirCubinho
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.panelControl1 = new DevExpress.XtraEditors.PanelControl();
            this.comboBoxEdit1 = new DevExpress.XtraEditors.ComboBoxEdit();
            this.labelControl5 = new DevExpress.XtraEditors.LabelControl();
            this.simpleButton2 = new DevExpress.XtraEditors.SimpleButton();
            this.simpleButton1 = new DevExpress.XtraEditors.SimpleButton();
            this.DataF = new DevExpress.XtraEditors.DateEdit();
            this.DataI = new DevExpress.XtraEditors.DateEdit();
            this.BCalcular = new DevExpress.XtraEditors.SimpleButton();
            this.panelControl2 = new DevExpress.XtraEditors.PanelControl();
            this.panelControl5 = new DevExpress.XtraEditors.PanelControl();
            this.listBoxFunil = new DevExpress.XtraEditors.ListBoxControl();
            this.pictureFinil = new DevExpress.XtraEditors.PictureEdit();
            this.pictureLixo = new DevExpress.XtraEditors.PictureEdit();
            this.labelControl4 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl3 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl2 = new DevExpress.XtraEditors.LabelControl();
            this.listBoxConteudo = new DevExpress.XtraEditors.ListBoxControl();
            this.listBoxColuna = new DevExpress.XtraEditors.ListBoxControl();
            this.listBoxLinha = new DevExpress.XtraEditors.ListBoxControl();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.panelControl3 = new DevExpress.XtraEditors.PanelControl();
            this.listBoxCampos = new DevExpress.XtraEditors.ListBoxControl();
            this.panelControl4 = new DevExpress.XtraEditors.PanelControl();
            this.chAgrupar = new DevExpress.XtraEditors.CheckEdit();
            this.labelControl1 = new DevExpress.XtraEditors.LabelControl();
            ((System.ComponentModel.ISupportInitialize)(this.StyleController_F)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl1)).BeginInit();
            this.panelControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.comboBoxEdit1.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DataF.Properties.VistaTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DataF.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DataI.Properties.VistaTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DataI.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl2)).BeginInit();
            this.panelControl2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl5)).BeginInit();
            this.panelControl5.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.listBoxFunil)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureFinil.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureLixo.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.listBoxConteudo)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.listBoxColuna)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.listBoxLinha)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl3)).BeginInit();
            this.panelControl3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.listBoxCampos)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl4)).BeginInit();
            this.panelControl4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.chAgrupar.Properties)).BeginInit();
            this.SuspendLayout();
            // 
            // ToolTipController_F
            // 
            this.ToolTipController_F.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.ToolTipController_F.Appearance.ForeColor = System.Drawing.Color.Black;
            this.ToolTipController_F.Appearance.Options.UseBackColor = true;
            this.ToolTipController_F.Appearance.Options.UseFont = true;
            this.ToolTipController_F.Appearance.Options.UseForeColor = true;
            this.ToolTipController_F.Appearance.Options.UseTextOptions = true;
            this.ToolTipController_F.Appearance.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            // 
            // StyleController_F
            // 
            this.StyleController_F.AppearanceDisabled.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this.StyleController_F.AppearanceDisabled.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this.StyleController_F.AppearanceDisabled.Options.UseBackColor = true;
            this.StyleController_F.AppearanceDisabled.Options.UseForeColor = true;
            this.StyleController_F.AppearanceFocused.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.StyleController_F.AppearanceFocused.Options.UseBackColor = true;
            // 
            // spellChecker1
            // 
            this.spellChecker1.OptionsSpelling.CheckSelectedTextFirst = DevExpress.Utils.DefaultBoolean.True;
            this.spellChecker1.OptionsSpelling.IgnoreEmails = DevExpress.Utils.DefaultBoolean.True;
            this.spellChecker1.OptionsSpelling.IgnoreMixedCaseWords = DevExpress.Utils.DefaultBoolean.False;
            this.spellChecker1.OptionsSpelling.IgnoreRepeatedWords = DevExpress.Utils.DefaultBoolean.False;
            this.spellChecker1.OptionsSpelling.IgnoreUpperCaseWords = DevExpress.Utils.DefaultBoolean.False;
            this.spellChecker1.OptionsSpelling.IgnoreUrls = DevExpress.Utils.DefaultBoolean.True;
            this.spellChecker1.OptionsSpelling.IgnoreWordsWithNumbers = DevExpress.Utils.DefaultBoolean.True;
            // 
            // panelControl1
            // 
            this.panelControl1.Controls.Add(this.comboBoxEdit1);
            this.panelControl1.Controls.Add(this.labelControl5);
            this.panelControl1.Controls.Add(this.simpleButton2);
            this.panelControl1.Controls.Add(this.simpleButton1);
            this.panelControl1.Controls.Add(this.DataF);
            this.panelControl1.Controls.Add(this.DataI);
            this.panelControl1.Controls.Add(this.BCalcular);
            this.panelControl1.Dock = System.Windows.Forms.DockStyle.Top;
            this.panelControl1.Location = new System.Drawing.Point(0, 0);
            this.panelControl1.Name = "panelControl1";
            this.panelControl1.Size = new System.Drawing.Size(1226, 104);
            this.panelControl1.TabIndex = 0;
            // 
            // comboBoxEdit1
            // 
            this.comboBoxEdit1.Location = new System.Drawing.Point(493, 9);
            this.comboBoxEdit1.Name = "comboBoxEdit1";
            this.comboBoxEdit1.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.comboBoxEdit1.Properties.Appearance.Options.UseFont = true;
            this.comboBoxEdit1.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.comboBoxEdit1.Size = new System.Drawing.Size(367, 29);
            this.comboBoxEdit1.TabIndex = 25;
            this.comboBoxEdit1.EditValueChanged += new System.EventHandler(this.comboBoxEdit1_EditValueChanged);
            // 
            // labelControl5
            // 
            this.labelControl5.Appearance.Font = new System.Drawing.Font("Tahoma", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl5.Appearance.ForeColor = System.Drawing.Color.Blue;
            this.labelControl5.Location = new System.Drawing.Point(424, 12);
            this.labelControl5.Name = "labelControl5";
            this.labelControl5.Size = new System.Drawing.Size(63, 23);
            this.labelControl5.TabIndex = 24;
            this.labelControl5.Text = "Nome:";
            // 
            // simpleButton2
            // 
            this.simpleButton2.Image = global::dllVirCubinho.Properties.Resources.Retorno;
            this.simpleButton2.Location = new System.Drawing.Point(493, 40);
            this.simpleButton2.Name = "simpleButton2";
            this.simpleButton2.Size = new System.Drawing.Size(367, 58);
            this.simpleButton2.TabIndex = 21;
            this.simpleButton2.Text = "Salvar";
            this.simpleButton2.Click += new System.EventHandler(this.simpleButton2_Click);
            // 
            // simpleButton1
            // 
            this.simpleButton1.Image = global::dllVirCubinho.Properties.Resources._48px_Gnome_edit_clear_svg;
            this.simpleButton1.Location = new System.Drawing.Point(9, 5);
            this.simpleButton1.Name = "simpleButton1";
            this.simpleButton1.Size = new System.Drawing.Size(135, 93);
            this.simpleButton1.TabIndex = 20;
            this.simpleButton1.Text = "Limpa";
            this.simpleButton1.Click += new System.EventHandler(this.simpleButton1_Click);
            // 
            // DataF
            // 
            this.DataF.EditValue = new System.DateTime(2012, 1, 15, 0, 0, 0, 0);
            this.DataF.Location = new System.Drawing.Point(299, 43);
            this.DataF.Name = "DataF";
            this.DataF.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.DataF.Properties.VistaTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.DataF.Size = new System.Drawing.Size(100, 20);
            this.DataF.TabIndex = 19;
            // 
            // DataI
            // 
            this.DataI.EditValue = new System.DateTime(2012, 1, 1, 0, 0, 0, 0);
            this.DataI.Location = new System.Drawing.Point(299, 17);
            this.DataI.Name = "DataI";
            this.DataI.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.DataI.Properties.VistaTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.DataI.Size = new System.Drawing.Size(100, 20);
            this.DataI.TabIndex = 18;
            // 
            // BCalcular
            // 
            this.BCalcular.Image = global::dllVirCubinho.Properties.Resources.startPQ;
            this.BCalcular.Location = new System.Drawing.Point(158, 5);
            this.BCalcular.Name = "BCalcular";
            this.BCalcular.Size = new System.Drawing.Size(135, 93);
            this.BCalcular.TabIndex = 17;
            this.BCalcular.Text = "Calcular";
            this.BCalcular.Click += new System.EventHandler(this.Calculo);
            // 
            // panelControl2
            // 
            this.panelControl2.Controls.Add(this.panelControl5);
            this.panelControl2.Controls.Add(this.panelControl3);
            this.panelControl2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelControl2.Location = new System.Drawing.Point(0, 104);
            this.panelControl2.Name = "panelControl2";
            this.panelControl2.Size = new System.Drawing.Size(1226, 680);
            this.panelControl2.TabIndex = 1;
            // 
            // panelControl5
            // 
            this.panelControl5.Controls.Add(this.listBoxFunil);
            this.panelControl5.Controls.Add(this.pictureFinil);
            this.panelControl5.Controls.Add(this.pictureLixo);
            this.panelControl5.Controls.Add(this.labelControl4);
            this.panelControl5.Controls.Add(this.labelControl3);
            this.panelControl5.Controls.Add(this.labelControl2);
            this.panelControl5.Controls.Add(this.listBoxConteudo);
            this.panelControl5.Controls.Add(this.listBoxColuna);
            this.panelControl5.Controls.Add(this.listBoxLinha);
            this.panelControl5.Controls.Add(this.pictureBox1);
            this.panelControl5.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelControl5.Location = new System.Drawing.Point(212, 2);
            this.panelControl5.Name = "panelControl5";
            this.panelControl5.Size = new System.Drawing.Size(1012, 676);
            this.panelControl5.TabIndex = 1;
            // 
            // listBoxFunil
            // 
            this.listBoxFunil.AllowDrop = true;
            this.listBoxFunil.Location = new System.Drawing.Point(277, 415);
            this.listBoxFunil.Name = "listBoxFunil";
            this.listBoxFunil.Size = new System.Drawing.Size(371, 118);
            this.listBoxFunil.TabIndex = 25;
            this.listBoxFunil.Visible = false;
            this.listBoxFunil.DragDrop += new System.Windows.Forms.DragEventHandler(this.DropEfetivoFunil);
            this.listBoxFunil.DragEnter += new System.Windows.Forms.DragEventHandler(this.TestaPermiteDropFunil);
            this.listBoxFunil.MouseDown += new System.Windows.Forms.MouseEventHandler(this.IniciarDrag);
            this.listBoxFunil.MouseMove += new System.Windows.Forms.MouseEventHandler(this.IniciarDragEfetivo);
            // 
            // pictureFinil
            // 
            this.pictureFinil.AllowDrop = true;
            this.pictureFinil.EditValue = global::dllVirCubinho.Properties.Resources.funilT;
            this.pictureFinil.Location = new System.Drawing.Point(145, 396);
            this.pictureFinil.Name = "pictureFinil";
            this.pictureFinil.Properties.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.pictureFinil.Properties.Appearance.Options.UseBackColor = true;
            this.pictureFinil.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.pictureFinil.Properties.SizeMode = DevExpress.XtraEditors.Controls.PictureSizeMode.Zoom;
            this.pictureFinil.Size = new System.Drawing.Size(126, 149);
            this.pictureFinil.TabIndex = 22;
            this.pictureFinil.DragDrop += new System.Windows.Forms.DragEventHandler(this.DropEfetivoFunil);
            this.pictureFinil.DragEnter += new System.Windows.Forms.DragEventHandler(this.TestaPermiteDropFunil);
            // 
            // pictureLixo
            // 
            this.pictureLixo.AllowDrop = true;
            this.pictureLixo.EditValue = global::dllVirCubinho.Properties.Resources._07_trash;
            this.pictureLixo.Location = new System.Drawing.Point(13, 396);
            this.pictureLixo.Name = "pictureLixo";
            this.pictureLixo.Properties.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.pictureLixo.Properties.Appearance.Options.UseBackColor = true;
            this.pictureLixo.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.pictureLixo.Properties.SizeMode = DevExpress.XtraEditors.Controls.PictureSizeMode.Zoom;
            this.pictureLixo.Size = new System.Drawing.Size(126, 149);
            this.pictureLixo.TabIndex = 24;
            this.pictureLixo.DragEnter += new System.Windows.Forms.DragEventHandler(this.TestaPermiteDrop);
            // 
            // labelControl4
            // 
            this.labelControl4.Appearance.Font = new System.Drawing.Font("Tahoma", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl4.Appearance.ForeColor = System.Drawing.Color.Blue;
            this.labelControl4.Location = new System.Drawing.Point(212, 161);
            this.labelControl4.Name = "labelControl4";
            this.labelControl4.Size = new System.Drawing.Size(92, 23);
            this.labelControl4.TabIndex = 20;
            this.labelControl4.Text = "Conteúdo";
            // 
            // labelControl3
            // 
            this.labelControl3.Appearance.Font = new System.Drawing.Font("Tahoma", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl3.Appearance.ForeColor = System.Drawing.Color.Blue;
            this.labelControl3.Location = new System.Drawing.Point(19, 107);
            this.labelControl3.Name = "labelControl3";
            this.labelControl3.Size = new System.Drawing.Size(62, 23);
            this.labelControl3.TabIndex = 19;
            this.labelControl3.Text = "Linhas";
            // 
            // labelControl2
            // 
            this.labelControl2.Appearance.Font = new System.Drawing.Font("Tahoma", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl2.Appearance.ForeColor = System.Drawing.Color.Blue;
            this.labelControl2.Location = new System.Drawing.Point(145, 7);
            this.labelControl2.Name = "labelControl2";
            this.labelControl2.Size = new System.Drawing.Size(76, 23);
            this.labelControl2.TabIndex = 18;
            this.labelControl2.Text = "Colunas";
            // 
            // listBoxConteudo
            // 
            this.listBoxConteudo.AllowDrop = true;
            this.listBoxConteudo.Location = new System.Drawing.Point(212, 190);
            this.listBoxConteudo.Name = "listBoxConteudo";
            this.listBoxConteudo.Size = new System.Drawing.Size(386, 175);
            this.listBoxConteudo.TabIndex = 17;
            this.listBoxConteudo.DragDrop += new System.Windows.Forms.DragEventHandler(this.DropEfetivo);
            this.listBoxConteudo.DragEnter += new System.Windows.Forms.DragEventHandler(this.TestaPermiteDrop);
            this.listBoxConteudo.MouseDown += new System.Windows.Forms.MouseEventHandler(this.IniciarDrag);
            this.listBoxConteudo.MouseMove += new System.Windows.Forms.MouseEventHandler(this.IniciarDragEfetivo);
            // 
            // listBoxColuna
            // 
            this.listBoxColuna.AllowDrop = true;
            this.listBoxColuna.Location = new System.Drawing.Point(145, 36);
            this.listBoxColuna.Name = "listBoxColuna";
            this.listBoxColuna.Size = new System.Drawing.Size(503, 94);
            this.listBoxColuna.TabIndex = 15;
            this.listBoxColuna.DragDrop += new System.Windows.Forms.DragEventHandler(this.DropEfetivo);
            this.listBoxColuna.DragEnter += new System.Windows.Forms.DragEventHandler(this.TestaPermiteDrop);
            this.listBoxColuna.MouseDown += new System.Windows.Forms.MouseEventHandler(this.IniciarDrag);
            this.listBoxColuna.MouseMove += new System.Windows.Forms.MouseEventHandler(this.IniciarDragEfetivo);
            // 
            // listBoxLinha
            // 
            this.listBoxLinha.AllowDrop = true;
            this.listBoxLinha.Location = new System.Drawing.Point(19, 136);
            this.listBoxLinha.Name = "listBoxLinha";
            this.listBoxLinha.Size = new System.Drawing.Size(120, 254);
            this.listBoxLinha.TabIndex = 14;
            this.listBoxLinha.DragDrop += new System.Windows.Forms.DragEventHandler(this.DropEfetivo);
            this.listBoxLinha.DragEnter += new System.Windows.Forms.DragEventHandler(this.TestaPermiteDrop);
            this.listBoxLinha.MouseDown += new System.Windows.Forms.MouseEventHandler(this.IniciarDrag);
            this.listBoxLinha.MouseMove += new System.Windows.Forms.MouseEventHandler(this.IniciarDragEfetivo);
            // 
            // pictureBox1
            // 
            this.pictureBox1.Image = global::dllVirCubinho.Properties.Resources.grade;
            this.pictureBox1.Location = new System.Drawing.Point(145, 136);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(503, 254);
            this.pictureBox1.TabIndex = 16;
            this.pictureBox1.TabStop = false;
            // 
            // panelControl3
            // 
            this.panelControl3.Controls.Add(this.listBoxCampos);
            this.panelControl3.Controls.Add(this.panelControl4);
            this.panelControl3.Dock = System.Windows.Forms.DockStyle.Left;
            this.panelControl3.Location = new System.Drawing.Point(2, 2);
            this.panelControl3.Name = "panelControl3";
            this.panelControl3.Size = new System.Drawing.Size(210, 676);
            this.panelControl3.TabIndex = 0;
            // 
            // listBoxCampos
            // 
            this.listBoxCampos.AllowDrop = true;
            this.listBoxCampos.Dock = System.Windows.Forms.DockStyle.Fill;
            this.listBoxCampos.Location = new System.Drawing.Point(2, 60);
            this.listBoxCampos.Name = "listBoxCampos";
            this.listBoxCampos.Size = new System.Drawing.Size(206, 614);
            this.listBoxCampos.TabIndex = 10;
            this.listBoxCampos.MouseDown += new System.Windows.Forms.MouseEventHandler(this.IniciarDrag);
            this.listBoxCampos.MouseMove += new System.Windows.Forms.MouseEventHandler(this.IniciarDragEfetivo);
            // 
            // panelControl4
            // 
            this.panelControl4.Controls.Add(this.chAgrupar);
            this.panelControl4.Controls.Add(this.labelControl1);
            this.panelControl4.Dock = System.Windows.Forms.DockStyle.Top;
            this.panelControl4.Location = new System.Drawing.Point(2, 2);
            this.panelControl4.Name = "panelControl4";
            this.panelControl4.Size = new System.Drawing.Size(206, 58);
            this.panelControl4.TabIndex = 0;
            // 
            // chAgrupar
            // 
            this.chAgrupar.EditValue = true;
            this.chAgrupar.Location = new System.Drawing.Point(6, 33);
            this.chAgrupar.Name = "chAgrupar";
            this.chAgrupar.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.chAgrupar.Properties.Appearance.Options.UseFont = true;
            this.chAgrupar.Properties.Caption = "agrupar campos";
            this.chAgrupar.Size = new System.Drawing.Size(156, 19);
            this.chAgrupar.TabIndex = 1;
            this.chAgrupar.CheckedChanged += new System.EventHandler(this.checkEdit1_CheckedChanged);
            // 
            // labelControl1
            // 
            this.labelControl1.Appearance.Font = new System.Drawing.Font("Tahoma", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl1.Appearance.ForeColor = System.Drawing.Color.Blue;
            this.labelControl1.Location = new System.Drawing.Point(5, 5);
            this.labelControl1.Name = "labelControl1";
            this.labelControl1.Size = new System.Drawing.Size(190, 23);
            this.labelControl1.TabIndex = 0;
            this.labelControl1.Text = "Campos disponíveis";
            // 
            // cVirCubinho
            // 
            this.Controls.Add(this.panelControl2);
            this.Controls.Add(this.panelControl1);
            this.Doc = System.Windows.Forms.DockStyle.Fill;
            this.LookAndFeel.UseDefaultLookAndFeel = false;
            this.Name = "cVirCubinho";
            this.Size = new System.Drawing.Size(1226, 784);
            ((System.ComponentModel.ISupportInitialize)(this.StyleController_F)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl1)).EndInit();
            this.panelControl1.ResumeLayout(false);
            this.panelControl1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.comboBoxEdit1.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DataF.Properties.VistaTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DataF.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DataI.Properties.VistaTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DataI.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl2)).EndInit();
            this.panelControl2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.panelControl5)).EndInit();
            this.panelControl5.ResumeLayout(false);
            this.panelControl5.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.listBoxFunil)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureFinil.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureLixo.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.listBoxConteudo)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.listBoxColuna)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.listBoxLinha)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl3)).EndInit();
            this.panelControl3.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.listBoxCampos)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl4)).EndInit();
            this.panelControl4.ResumeLayout(false);
            this.panelControl4.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.chAgrupar.Properties)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraEditors.PanelControl panelControl1;
        private DevExpress.XtraEditors.DateEdit DataF;
        private DevExpress.XtraEditors.DateEdit DataI;
        private DevExpress.XtraEditors.SimpleButton BCalcular;
        private DevExpress.XtraEditors.PanelControl panelControl2;
        private DevExpress.XtraEditors.PanelControl panelControl5;
        private DevExpress.XtraEditors.LabelControl labelControl4;
        private DevExpress.XtraEditors.LabelControl labelControl3;
        private DevExpress.XtraEditors.LabelControl labelControl2;
        private DevExpress.XtraEditors.ListBoxControl listBoxConteudo;
        private DevExpress.XtraEditors.ListBoxControl listBoxColuna;
        private DevExpress.XtraEditors.ListBoxControl listBoxLinha;
        private System.Windows.Forms.PictureBox pictureBox1;
        private DevExpress.XtraEditors.PanelControl panelControl3;
        private DevExpress.XtraEditors.ListBoxControl listBoxCampos;
        private DevExpress.XtraEditors.PanelControl panelControl4;
        private DevExpress.XtraEditors.LabelControl labelControl1;
        private DevExpress.XtraEditors.PictureEdit pictureLixo;
        private DevExpress.XtraEditors.PictureEdit pictureFinil;
        private DevExpress.XtraEditors.ListBoxControl listBoxFunil;
        private DevExpress.XtraEditors.SimpleButton simpleButton1;
        private DevExpress.XtraEditors.CheckEdit chAgrupar;
        private DevExpress.XtraEditors.SimpleButton simpleButton2;
        private DevExpress.XtraEditors.LabelControl labelControl5;
        protected DevExpress.XtraEditors.ComboBoxEdit comboBoxEdit1;
    }
}
