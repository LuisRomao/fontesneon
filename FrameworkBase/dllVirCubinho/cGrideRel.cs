﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Text;
using System.Windows.Forms;
using DevExpress.XtraGrid;
using DevExpress.Data;

namespace dllVirCubinho
{
    public partial class cGrideRel : CompontesBasicos.ComponenteBase
    {
        public cVirCubinho Chamador;

        public cGrideRel()
        {
            InitializeComponent();
            UltimaBanda = gridBandLinhas;
        }

        private void simpleButton1_Click(object sender, EventArgs e)
        {
            if (saveFileDialog1.ShowDialog() == DialogResult.OK)
            {
                if (System.IO.Path.GetExtension(saveFileDialog1.FileName).ToUpper() == ".XLSX")
                    gridControl1.ExportToXlsx(saveFileDialog1.FileName);
                else
                    gridControl1.ExportToXls(saveFileDialog1.FileName);
            }
            if (MessageBox.Show("Abrir o arquivo?", "Exportar Para...", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
            {
                try
                {
                    using (System.Diagnostics.Process process = new System.Diagnostics.Process())
                    {
                        process.StartInfo.FileName = saveFileDialog1.FileName;
                        process.StartInfo.Verb = "Open";
                        process.StartInfo.WindowStyle = System.Diagnostics.ProcessWindowStyle.Maximized;
                        process.Start();
                    }
                }
                catch
                {
                    MessageBox.Show(this, "Não foi encontrado em seu sistema operacional um aplicativo apropriado para abrir o arquivo com os dados exportados.", Application.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            }
        }

        private DevExpress.XtraGrid.Views.BandedGrid.GridBand UltimaBanda;

        public void CriaBanda(string Titulo,string NomeBanda)
        {
            UltimaBanda = new DevExpress.XtraGrid.Views.BandedGrid.GridBand();
            UltimaBanda.Caption = Titulo;


            UltimaBanda.Name = NomeBanda;
            //UltimaBanda.Width = 100 * marcados;
            //UltimaBanda.OptionsBand.FixedWidth = true;
            UltimaBanda.AppearanceHeader.Options.UseTextOptions = true;
            UltimaBanda.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            bandedGridView1.Bands.Add(UltimaBanda);
        }

        public void CriaColunaNaGrid(Campo CampoCriar, bool ComTotais)
        {
            CriaColunaNaGrid(CampoCriar.ToString(),CampoCriar.NomeCampo,CampoCriar.TypeCampo, CampoCriar.LarguraColuna, ComTotais);
        }

        public void CriaColunaNaGrid(string _TituloColuna,string NomeCampo,Type TypeCampo, int LarguraColuna, bool ComTotais)
        {
            string TituloColuna = _TituloColuna;
            DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn novaColuna = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            //colunasNovas[k].AppearanceCell.BackColor = conjunto.conjuntos[j].co.Color;
            //colunasNovas[k].AppearanceCell.Options.UseBackColor = true;
            novaColuna.AppearanceHeader.Options.UseTextOptions = true;
            novaColuna.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            novaColuna.Caption = TituloColuna;

            //colunasNovas[k].CustomizationCaption = string.Format("{0} - ({1})", colunasNovas[k].Caption, comp);
            novaColuna.Name = NomeCampo;
            novaColuna.Visible = true;
            novaColuna.OptionsColumn.ReadOnly = true;
            
            //colunasNovas[k].Visible = conjunto.conjuntos[j].Visivel;
            if (LarguraColuna > 0)
                novaColuna.Width = LarguraColuna;
            //colunasNovas[k].OptionsColumn.FixedWidth = true;
            //novaColuna.ColumnEdit = this.repositoryItemCalcEdit1;
            // if ((Editando) && ((Tipo == Tipos.entradaR) || (Tipo == Tipos.saidaR) || (Tipo == Tipos.estoqueIR)))
            //     colunasNovas[k].ColumnEdit = this.repositoryItemCalcEdit2;
            // else
            //     colunasNovas[k].ColumnEdit = this.repositoryItemCalcEdit1;
            if (TypeCampo == typeof(decimal))
            {
                novaColuna.ColumnEdit = this.repositoryItemCalcEdit1;
                novaColuna.AppearanceCell.BackColor = Color.Salmon;
                novaColuna.AppearanceCell.Options.UseBackColor = true;
                if (ComTotais)
                {
                    novaColuna.Summary.Add(new GridColumnSummaryItem(SummaryItemType.Sum, NomeCampo, "{0:n2}"));
                    bandedGridView1.GroupSummary.Add(new GridGroupSummaryItem(SummaryItemType.Sum, NomeCampo, novaColuna, "{0:n2}"));
                }
            }
            else if (TypeCampo == typeof(int))
            {
                novaColuna.ColumnEdit = this.repositoryItemCalcEdit2;
                novaColuna.AppearanceCell.BackColor = Color.Aqua;
                novaColuna.AppearanceCell.Options.UseBackColor = true;
                if (ComTotais)
                {
                    novaColuna.Summary.Add(new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum, NomeCampo, "{0:n0}"));
                    bandedGridView1.GroupSummary.Add(new GridGroupSummaryItem(SummaryItemType.Sum, NomeCampo, novaColuna, "{0:n0}"));
                }
            }
            else if (TypeCampo == typeof(string))
            {
                //novaColuna.ColumnEdit = this.repositoryItemCalcEdit2;
                novaColuna.AppearanceCell.BackColor = Color.Silver;
                novaColuna.AppearanceCell.Options.UseBackColor = true;
            }
            novaColuna.FieldName = NomeCampo;
            bandedGridView1.Columns.Add(novaColuna);
            UltimaBanda.Columns.Add(novaColuna);
        }

        private void simpleButton2_Click(object sender, EventArgs e)
        {
            FechaTela(DialogResult.OK);
        }

        private void simpleButton3_Click(object sender, EventArgs e)
        {
            if (Chamador == null)
                return;
            DataRow DR = bandedGridView1.GetFocusedDataRow();
            if (DR == null)
                return;
            Chamador.Detalhar(DR);
        }
    }
}
