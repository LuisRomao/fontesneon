﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Text;
using System.Windows.Forms;
using CompontesBasicos;

namespace dllVirCubinho
{
    public partial class cDiagFiltro : ComponenteBaseDialog
    {
        /// <summary>
        /// Cosntrutor padrão
        /// </summary>
        public cDiagFiltro()
        {
            InitializeComponent();            
        }

        public cDiagFiltro(Campo _Campo)
        {
            InitializeComponent();
            labelCampo.Text = string.Format("\"{0}\"", _Campo.Descricao);
        }

        public override bool CanClose()
        {
            if ((radioGroup1.EditValue == null) || (calcEdit1.EditValue == null))
            {
                return false;
            }
            return base.CanClose();
        }

        public TiposFiltro TipoSelecionado;
        public decimal ValorSelecionado;

        protected override void FechaTela(DialogResult Resultado)
        {
            if (Resultado == DialogResult.OK)
            {
                TipoSelecionado = (TiposFiltro)radioGroup1.EditValue;
                ValorSelecionado = (decimal)calcEdit1.EditValue;
            };            
            base.FechaTela(Resultado);
        }
    }
}
