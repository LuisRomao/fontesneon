﻿using System;
using System.Collections.Generic;
using System.Text;

namespace dllVirCubinho
{
    internal abstract class PortaDrag
    {
        public object Sender;

        public abstract string Gravar(Quadro Q);

        
    }

    internal enum Quadro { Linha = 0, Coluna = 1, Conteudo = 2, Funil = 3 }

    internal class PortaItem: PortaDrag
    {
        /// <summary>
        /// Campo
        /// </summary>
        public Campo Campo;                       
        
                
        public PortaItem(Campo _Campo)
        {        
            Campo = _Campo;
        }

        /// <summary>
        /// Construtor
        /// </summary>
        /// <param name="Modelo"></param>
        public PortaItem(PortaItem Modelo)
        {
            Campo = Modelo.Campo;                       
        }

        /// <summary>
        /// ToString
        /// </summary>
        /// <returns></returns>
        public override string ToString()
        {
            if (Campo.Grupo == null)
                return string.Format("{0}", Campo.Descricao);
            else
                return string.Format("{0}: {1}",Campo.Grupo,  Campo.Descricao);
        }

        

        public override string Gravar(Quadro Q)
        {
            return string.Format("{0}\tI\t{1}\r\n",(int)Q, Campo.NomeCampo);
        }

    }

    internal class PortaGrupo: PortaDrag
    {
        
        public GrupoCampos Grupo;
        

        public PortaGrupo(GrupoCampos _Grupo)
        {
            Grupo = _Grupo;
        }

        

        /// <summary>
        /// ToString
        /// </summary>
        /// <returns></returns>
        public override string ToString()
        {
            
            return string.Format("{0}", Grupo.Descricao);
            
        }

        public override string Gravar(Quadro Q)
        {
            return "";
        }

    }

    internal class PortaIntemFunil : PortaItem
    {
        public TiposFiltro Tipo;
        public object Argumento;

        public PortaIntemFunil(Campo _Campo,TiposFiltro _Tipo,object _Argumento)
            : base(_Campo)
        {
            Tipo = _Tipo;
            Argumento = _Argumento;
        }

        public override string ToString()
        {
            string strTipo = "???";
            switch (Tipo)
            {
                case TiposFiltro.diferente:
                    strTipo = "<>";
                    break;
                case TiposFiltro.igual:
                    strTipo = "=";
                    break;
                case TiposFiltro.maior:
                    strTipo = ">";
                    break;
                case TiposFiltro.menor:
                    strTipo = "<";
                    break;
                case TiposFiltro.menorigual:
                    strTipo = "<=";
                    break;
                case TiposFiltro.maiorigual:
                    strTipo = ">=";
                    break;                
                case TiposFiltro.contem:
                    strTipo = "contém";
                    break;
            }
            return string.Format("\"{0}\" {1} {2}", Campo.Descricao ,strTipo,Argumento);
        }

        public override string Gravar(Quadro Q)
        {
            return string.Format("{0}\tF\t{1}\t{2}\t{3}\r\n", (int)Q, Campo.NomeCampo,(int)Tipo,Argumento);
        }
    }

    /// <summary>
    /// Tipos de filtro
    /// </summary>
    public enum TiposFiltro
    {
        igual = 0,
        maior = 1,
        menor = 2,
        diferente = 3,
        contem = 4,
        maiorigual = 5,
        menorigual = 6
    }

}
