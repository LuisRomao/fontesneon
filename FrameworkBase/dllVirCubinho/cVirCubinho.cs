﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Text;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using CompontesBasicos;

namespace dllVirCubinho
{
    public partial class cVirCubinho : ComponenteBase
    {
        int Contador = 1;
        bool NomeAtribuido = false;

        private void NomePadrao()
        {
            if (NomeAtribuido)
            {
                NomeAtribuido = false;
                Contador++;
            }
            try
            {
                BLOCRecuperaConfiguracaoDoBanco = true;
                comboBoxEdit1.Text = string.Format("Consulta {0}", Contador);
            }
            finally
            {
                BLOCRecuperaConfiguracaoDoBanco = false;
            }
        }

        //public object oERRO = null;

        public cVirCubinho()
        {
            InitializeComponent();
            DataF.Properties.MaxValue = DateTime.Today;
            DataF.DateTime = DateTime.Today;
            if (!DesignMode)
                CadastraCampos();
            PopulaList();
            PopulaCombo();
            NomePadrao();
            BLOCRecuperaConfiguracaoDoBanco = false;
            //teste = oERRO.ToString();
        }

        protected void AjustaLimites(DateTime dataInicialLimite, DateTime dataFinalLimite, DateTime dataInicial, DateTime dataFinal)
        {
            DataI.Properties.MaxValue = DataF.Properties.MaxValue = dataFinalLimite;
            DataI.Properties.MinValue = DataF.Properties.MinValue = dataInicialLimite;
            DataI.DateTime = dataInicial;
            DataF.DateTime = dataFinal;
        }

        private void PopulaList()
        {
            listBoxCampos.Items.Clear();
            if (!Campo.ConjutoCamposDefinido)
                return;
            if (chAgrupar.Checked && (GrupoCampos.ConjutoGrupos != null))
                foreach (GrupoCampos Grr in GrupoCampos.ConjutoGrupos)
                    listBoxCampos.Items.Add(new PortaGrupo(Grr));
            else
                foreach (Campo cam in Campo.ConjutoCampos)
                    listBoxCampos.Items.Add(new PortaItem(cam));
        }

        /// <summary>
        /// Metodo onde são cadastrados os campos
        /// </summary>
        protected virtual void CadastraCampos()
        {
            MessageBox.Show("Fazer override do método 'CadastraCampos' popular ConjutoCampos");
        }

        protected virtual DataTable CarregaTabela(DateTime DataIni, DateTime DataFinal)
        {
            throw new NotImplementedException("Fazer override do método 'CarregaTabela'");
        }

        protected virtual object Tradutor(Campo Cam, DataRow row)
        {
            throw new NotImplementedException("Fazer override do método 'Tradutor'");
        }

        protected virtual bool TemTotal(Campo CampoTestar)
        {
            return true;
        }

        private int indexOfItemUnderMouseToDrag;
        private Rectangle dragBoxFromMouseDown;
        public DataTable TabelaFonte;


        #region Funções para drag drop

        #region Origem

        private void IniciarDrag(object sender, MouseEventArgs e)
        {
            ListBoxControl chamador = (ListBoxControl)sender;

            indexOfItemUnderMouseToDrag = chamador.IndexFromPoint(e.Location);

            if (indexOfItemUnderMouseToDrag != ListBox.NoMatches)
            {
                Size dragSize = SystemInformation.DragSize;
                dragBoxFromMouseDown = new Rectangle(new Point(e.X - (dragSize.Width / 2),
                                                               e.Y - (dragSize.Height / 2)), dragSize);
            }
            else
                dragBoxFromMouseDown = Rectangle.Empty;
        }
        private void IniciarDragEfetivo(object sender, MouseEventArgs e)
        {
            if ((e.Button & MouseButtons.Left) == MouseButtons.Left)
            {
                ListBoxControl ListDragSource = (ListBoxControl)sender;
                // If the mouse moves outside the rectangle, start the drag.
                if (dragBoxFromMouseDown != Rectangle.Empty && !dragBoxFromMouseDown.Contains(e.X, e.Y))
                {
                    PortaDrag PG = (PortaDrag)ListDragSource.Items[indexOfItemUnderMouseToDrag];
                    PG.Sender = sender;
                    
                    //CHAMADA EFETIVA                    
                    DragDropEffects dropEffect = ListDragSource.DoDragDrop(PG, DragDropEffects.Move | DragDropEffects.Copy);
                    if ((dropEffect == DragDropEffects.Move) || ((dropEffect == DragDropEffects.Copy)))
                    {
                        if (ListDragSource != listBoxCampos)
                        {
                            NomePadrao();
                            ListDragSource.Items.RemoveAt(indexOfItemUnderMouseToDrag);
                            // Selects the previous item in the list as long as the list has an item.
                            if (indexOfItemUnderMouseToDrag > 0)
                                ListDragSource.SelectedIndex = indexOfItemUnderMouseToDrag - 1;

                            else if (ListDragSource.Items.Count > 0)
                                // Selects the first item.
                                ListDragSource.SelectedIndex = 0;
                            if ((ListDragSource == listBoxFunil) && (ListDragSource.Items.Count == 0))
                                listBoxFunil.Visible = false;
                        }
                    }


                }
            }
        }

        #endregion

        #region Destino
        private void TestaPermiteDrop(object sender, DragEventArgs e)
        {
            if (e.Data.GetDataPresent(typeof(PortaItem)))
            {
                PortaItem PI = (PortaItem)e.Data.GetData(typeof(PortaItem));
                if ((PI.Sender == null) || (PI.Sender == sender))
                    e.Effect = DragDropEffects.None;
                else
                    if (PI.Sender == listBoxCampos)
                    {
                        if (sender == pictureLixo)
                            e.Effect = DragDropEffects.None;
                        else
                            e.Effect = DragDropEffects.Copy;
                    }
                    else
                        e.Effect = DragDropEffects.Move;
                if ((sender == listBoxConteudo)
                    &&
                    ((PI.Campo.TypeCampo != typeof(decimal))
                       &&
                      (PI.Campo.TypeCampo != typeof(int))
                    )
                   )
                    e.Effect = DragDropEffects.None;
            }
            else if (e.Data.GetDataPresent(typeof(PortaGrupo)))
            {
                PortaGrupo PG = (PortaGrupo)e.Data.GetData(typeof(PortaGrupo));
                if ((PG.Sender == null) || (PG.Sender == sender))
                    e.Effect = DragDropEffects.None;
                else
                    if (PG.Sender == listBoxCampos)
                    {
                        if (sender == pictureLixo)
                            e.Effect = DragDropEffects.None;
                        else
                            e.Effect = DragDropEffects.Copy;
                    }
                    else
                        e.Effect = DragDropEffects.Move;                
            }
            else if (e.Data.GetDataPresent(typeof(PortaIntemFunil)))
            {
                PortaIntemFunil PIF = (PortaIntemFunil)e.Data.GetData(typeof(PortaIntemFunil));
                if ((PIF.Sender != null) && (PIF.Sender == listBoxFunil))
                    e.Effect = DragDropEffects.Move;
                else
                    e.Effect = DragDropEffects.None;
            }
            else
                e.Effect = DragDropEffects.None;
        }

        

        private void TestaPermiteDropFunil(object sender, DragEventArgs e)
        {
            if (e.Data.GetDataPresent(typeof(PortaItem)))
            {
                PortaItem PI = (PortaItem)e.Data.GetData(typeof(PortaItem));
                if (PI.Sender != listBoxCampos)
                    e.Effect = DragDropEffects.None;
                else
                    e.Effect = DragDropEffects.Copy;                                    
            }
            else if (e.Data.GetDataPresent(typeof(PortaGrupo)))
                e.Effect = DragDropEffects.Copy;                                    
            else
                e.Effect = DragDropEffects.None;
        }

        private void DropEfetivo(object sender, DragEventArgs e)
        {
            ListBoxControl ListDragTarget = (ListBoxControl)sender;
            if (e.Data.GetDataPresent(typeof(PortaItem)))                       
                AddPortaItem(ListDragTarget, (PortaItem)e.Data.GetData((typeof(PortaItem))));            
            else if (e.Data.GetDataPresent(typeof(PortaGrupo)))
            {
                PortaGrupo PG = (PortaGrupo)e.Data.GetData((typeof(PortaGrupo)));
                cItemGrupo.TipoSelecao Multiplos = cItemGrupo.TipoSelecao.multiplos;
                cItemGrupo.Enables enables = cItemGrupo.Enables.Todos;
                if (ListDragTarget == listBoxColuna)
                    Multiplos = cItemGrupo.TipoSelecao.radio;
                if (ListDragTarget == listBoxConteudo)
                    enables = cItemGrupo.Enables.numericos;
                cItemGrupo cItemGrupo1 = new cItemGrupo(PG.Grupo,Multiplos,enables);
                if (cItemGrupo1.VirShowModulo(EstadosDosComponentes.PopUp) == DialogResult.OK)
                    foreach (Campo camposel in cItemGrupo1.Selecoes)
                    {
                        AddPortaItem(ListDragTarget, new PortaItem(camposel));
                    }
            }
        }

        private void DropEfetivoFunil(object sender, DragEventArgs e)
        {
            if (e.Data.GetDataPresent(typeof(PortaItem)))
            {
                PortaItem PoI = (PortaItem)e.Data.GetData((typeof(PortaItem)));                
                PortaIntemFunil novoPoiF = LeFiltro(PoI.Campo);
                if (novoPoiF != null)
                    AddPortaItemFunil(novoPoiF);
            }
            if (e.Data.GetDataPresent(typeof(PortaGrupo)))
            {
                PortaGrupo PoG = (PortaGrupo)e.Data.GetData((typeof(PortaGrupo)));
                cItemGrupo cItemGrupo1 = new cItemGrupo(PoG.Grupo,cItemGrupo.TipoSelecao.radio, cItemGrupo.Enables.Todos);
                if (cItemGrupo1.VirShowModulo(EstadosDosComponentes.PopUp) == DialogResult.OK)
                {
                    PortaIntemFunil novoPoiF = LeFiltro(cItemGrupo1.Selecao);
                    if (novoPoiF != null)
                        AddPortaItemFunil(novoPoiF);
                }



                
            }
        }

        #endregion

        private void AddPortaItem(ListBoxControl Lista, PortaItem novoPoI)
        {
            if ((Lista == listBoxConteudo) || (Lista == listBoxLinha))
            {
                foreach (PortaItem PoI in Lista.Items)
                    if (PoI.Campo.NomeCampo == novoPoI.Campo.NomeCampo)                                            
                        return;                    
                Lista.Items.Add(novoPoI);
                NomePadrao();
            }
            else
            {
                Lista.Items.Clear();
                Lista.Items.Add(novoPoI);
                NomePadrao();
            }
        }

        private void AddPortaItemFunil(PortaIntemFunil novoPoIF)
        {
            //foreach (PortaIntemFunil PoIF in listBoxFunil.Items)
            //        if (PoIF.Campo.Descricao == novoPoIF.Campo.Descricao)
            //            return;
            listBoxFunil.Items.Add(novoPoIF);
            listBoxFunil.Visible = true;
            NomePadrao();
        }

        private PortaIntemFunil LeFiltro(Campo _Campo)
        {
            if (_Campo.TypeCampo == typeof(string))
            {
                cDiagFiltrostr Dialogostr = new cDiagFiltrostr(_Campo);
                if (Dialogostr.VirShowModulo(EstadosDosComponentes.PopUp) == DialogResult.OK)
                    return new PortaIntemFunil(_Campo, Dialogostr.TipoSelecionado, Dialogostr.ValorSelecionado);
                else
                    return null;
            }
            else
            {
                cDiagFiltro Dialogo = new cDiagFiltro(_Campo);
                if (Dialogo.VirShowModulo(EstadosDosComponentes.PopUp) == DialogResult.OK)
                    return new PortaIntemFunil(_Campo, Dialogo.TipoSelecionado, Dialogo.ValorSelecionado);
                else
                    return null;
            }
        }

        #endregion

        //private Campo CampoLinha;
        protected List<Campo> CamposLinha;
        private Campo CampoColuna;
        private List<Campo> CamposConteudo;
        private cGrideRel novaGrid;
        private List<PortaIntemFunil> ItensFunil;

        private void LimparDados()
        {
            listBoxLinha.Items.Clear();
            listBoxColuna.Items.Clear();
            listBoxFunil.Items.Clear();
            listBoxConteudo.Items.Clear();
            listBoxFunil.Visible = false;
        }

        public void Detalhar(DataRow DR)
        {
            if ((DR == null) || (GrupoCampos.GrupoDetalhar == null))
                return;
            LimparDados();
            List<Campo> CamposLinhaOriginal = CamposLinha;
            CamposLinha = new List<Campo>();            
            listBoxFunil.Visible = true;
            foreach (Campo CampoDet in GrupoCampos.GrupoDetalhar.CamposdoGrupo)
            {
                CamposLinha.Add(CampoDet);
                listBoxLinha.Items.Add(new PortaItem(CampoDet));
            }
            CampoColuna = null;
            CamposConteudo = new List<Campo>();
            if (ItensFunil == null)
                ItensFunil = new List<PortaIntemFunil>();
            foreach (Campo CampoL in CamposLinhaOriginal)
            {
                PortaIntemFunil PoF = new PortaIntemFunil(CampoL, TiposFiltro.igual, DR[CampoL.NomeCampo]);
                ItensFunil.Add(PoF);
                listBoxFunil.Items.Add(PoF);
            }
            CalculoEfetivo();
        }

        private void prepararCalculo()
        {
            CamposLinha = new List<Campo>();
            foreach (PortaItem PI in listBoxLinha.Items)
                CamposLinha.Add(PI.Campo);
            if (listBoxColuna.Items.Count != 0)
                CampoColuna = ((PortaItem)listBoxColuna.Items[0]).Campo;
            else
                CampoColuna = null;
            CamposConteudo = new List<Campo>();
            foreach (PortaItem PI in listBoxConteudo.Items)
                CamposConteudo.Add(PI.Campo);                        
        }

        private void PreparaFiltro()
        {
            ItensFunil = new List<PortaIntemFunil>();
            foreach (PortaIntemFunil PoF in listBoxFunil.Items)
                ItensFunil.Add(PoF);
        }

        private void CalculoEfetivo()
        {
            using (CompontesBasicos.Espera.cEspera est = new CompontesBasicos.Espera.cEspera(this))
            {
                novaGrid = new cGrideRel();
                novaGrid.Chamador = this;
                est.Motivo.Text = "Obtendo dados";
                Application.DoEvents();
                TabelaFonte = CarregaTabela(DataI.DateTime, DataF.DateTime);
                if (TabelaFonte.Rows.Count == 0)
                {
                    MessageBox.Show("Dados não encontrados");
                    return;
                }


                DataSet DS = new DataSet();
                DataTable Tabela = DS.Tables.Add();

                foreach (Campo CampoLinha in CamposLinha)
                {
                    Tabela.Columns.Add(CampoLinha.NomeCampo, CampoLinha.TypeCampo);
                    novaGrid.CriaColunaNaGrid(CampoLinha, TemTotal(CampoLinha));
                }
                //Tabela.Columns.Add("CampoLinha", CampoLinha.TypeCampo);
                //novaGrid.CriaColunaNaGrid(CampoLinha.Descricao, "CampoLinha",typeof(string));


                SortedList<string, DataColumn> Colunas = new SortedList<string, DataColumn>();
                SortedList<string, DataRow> Linhas = new SortedList<string, DataRow>();
                //object ValorCampoLinha;
                SortedList<string, object> ValoresCampoLinha = new SortedList<string, object>();
                string strValorCampoLinha;
                object ValorCampoColuna;
                string strValorCampoColuna;
                est.Motivo.Text = "Agrupando dados";
                Application.DoEvents();

                foreach (DataRow row in TabelaFonte.Rows)
                {
                    if (!PassaNosFiltros(row))
                        continue;

                    //Linha
                    strValorCampoLinha = "";
                    ValoresCampoLinha.Clear();
                    foreach (Campo CampoLinha in CamposLinha)
                    {
                        object ValorCampoLinha = Tradutor(CampoLinha, row);
                        ValoresCampoLinha.Add(CampoLinha.NomeCampo, ValorCampoLinha);
                        strValorCampoLinha += ValorCampoLinha.ToString().ToUpper().Trim() + "|";
                    }
                    DataRow rownova;
                    if (Linhas.ContainsKey(strValorCampoLinha))
                        rownova = Linhas[strValorCampoLinha];
                    else
                    {
                        rownova = Tabela.NewRow();
                        Tabela.Rows.Add(rownova);
                        foreach (Campo CampoLinha in CamposLinha)
                            rownova[CampoLinha.NomeCampo] = ValoresCampoLinha[CampoLinha.NomeCampo];
                        Linhas.Add(strValorCampoLinha, rownova);
                    }

                    //Coluna                    
                    ValorCampoColuna = (CampoColuna == null) ? "???" : Tradutor(CampoColuna, row);
                    strValorCampoColuna = ValorCampoColuna.ToString().ToUpper().Trim();
                    bool PrimeiraSub = true;
                    foreach (Campo CampoConteudo in CamposConteudo)
                    {
                        string strSubColuna = string.Format("{0}|{1}", strValorCampoColuna, CampoConteudo.NomeCampo);

                        DataColumn SubColuna;


                        if (Colunas.ContainsKey(strSubColuna))
                            SubColuna = Colunas[strSubColuna];
                        else
                        {
                            SubColuna = Tabela.Columns.Add(strSubColuna, typeof(decimal));
                            if (PrimeiraSub)
                            {
                                novaGrid.CriaBanda(strValorCampoColuna, strValorCampoColuna);
                                PrimeiraSub = false;
                            }
                            novaGrid.CriaColunaNaGrid(CampoConteudo.Descricao, strSubColuna, CampoConteudo.TypeCampo, CampoConteudo.LarguraColuna, true);
                            Colunas.Add(strSubColuna, SubColuna);
                        }
                        decimal ValorLido = (decimal)Tradutor(CampoConteudo, row);
                        if (rownova[SubColuna] == DBNull.Value)
                            rownova[SubColuna] = ValorLido;
                        else
                            rownova[SubColuna] = (decimal)rownova[SubColuna] + ValorLido;
                    }

                }

                novaGrid.Titulo = comboBoxEdit1.Text;
                novaGrid.gridControl1.DataSource = Tabela;
                novaGrid.VirShowModulo(EstadosDosComponentes.JanelasAtivas);
            }
        }

        private void Calculo(object sender, EventArgs e)
        {
            NomeAtribuido = true;
            if (listBoxLinha.ItemCount == 0)
                return;

            prepararCalculo();

            PreparaFiltro();

            CalculoEfetivo();
        }

        private bool PassaNosFiltros(DataRow row)
        {
            foreach (PortaIntemFunil PoF in ItensFunil)
            {
                object ValorCampoLinha = Tradutor(PoF.Campo, row);
                if (ValorCampoLinha == null)
                    return false;
                if (PoF.Campo.TypeCampo == typeof(string))
                {
                    string strValor = ((string)ValorCampoLinha).Trim();
                    string strArgumento = (string)PoF.Argumento;
                    switch (PoF.Tipo)
                    {
                        case TiposFiltro.igual:
                            if (strValor != strArgumento)
                                return false;
                            break;                        
                        case TiposFiltro.diferente:
                            if (strValor == strArgumento)
                                return false;
                            break;
                        case TiposFiltro.contem:
                            if (!strValor.Contains(strArgumento))
                                return false;
                            break;
                        default:
                            break;
                    }
                }
                else if (PoF.Campo.TypeCampo == typeof(decimal))
                {
                    decimal decValor = (decimal)ValorCampoLinha;
                    decimal decArgumento = (decimal)PoF.Argumento;
                    switch (PoF.Tipo)
                    {
                        case TiposFiltro.igual:
                            if (decValor != decArgumento)
                                return false;
                            break;
                        case TiposFiltro.diferente:
                            if (decValor == decArgumento)
                                return false;
                            break;
                        case TiposFiltro.maior:
                            if (decValor <= decArgumento)
                                return false;
                            break;
                        case TiposFiltro.menor:
                            if (decValor >= decArgumento)
                                return false;
                            break;
                        case TiposFiltro.maiorigual:
                            if (decValor < decArgumento)
                                return false;
                            break;
                        case TiposFiltro.menorigual:
                            if (decValor > decArgumento)
                                return false;
                            break;
                        default:
                            break;
                    }
                }
                else if (PoF.Campo.TypeCampo == typeof(int))
                {
                    int intValor = (int)ValorCampoLinha;
                    int intArgumento = (int)PoF.Argumento;
                    switch (PoF.Tipo)
                    {
                        case TiposFiltro.igual:
                            if (intValor != intArgumento)
                                return false;
                            break;
                        case TiposFiltro.diferente:
                            if (intValor == intArgumento)
                                return false;
                            break;
                        case TiposFiltro.maior:
                            if (intValor <= intArgumento)
                                return false;
                            break;
                        case TiposFiltro.menor:
                            if (intValor >= intArgumento)
                                return false;
                            break;
                        case TiposFiltro.maiorigual:
                            if (intValor < intArgumento)
                                return false;
                            break;
                        case TiposFiltro.menorigual:
                            if (intValor > intArgumento)
                                return false;
                            break;
                        default:
                            break;
                    }
                }                
            }
            return true;
        }

        private void simpleButton1_Click(object sender, EventArgs e)
        {
            if (MessageBox.Show("Confirmação", "Confirma?", MessageBoxButtons.YesNo, MessageBoxIcon.Warning, MessageBoxDefaultButton.Button2) == DialogResult.Yes)
                LimparDados();
        }

        private void checkEdit1_CheckedChanged(object sender, EventArgs e)
        {
            PopulaList();
        }

        string teste;

        protected string Salvar()
        {
            string Gravacao = "";
            foreach (PortaItem P in listBoxColuna.Items)
                Gravacao += P.Gravar(Quadro.Coluna);
            foreach (PortaItem P in listBoxLinha.Items)
                Gravacao += P.Gravar(Quadro.Linha);
            foreach (PortaItem P in listBoxConteudo.Items)
                Gravacao += P.Gravar(Quadro.Conteudo);
            foreach (PortaIntemFunil F in listBoxFunil.Items)
                Gravacao += F.Gravar(Quadro.Funil);
            return Gravacao;            
        }

        protected void Restaura(string Dados)
        {
            LimparDados();
            try
            {                
                string[] linhas = Dados.Split(new char[] { '\r', '\n' }, StringSplitOptions.RemoveEmptyEntries);
                foreach (string linha in linhas)
                {
                    string[] partes = linha.Split(new char[] { '\t' }, StringSplitOptions.RemoveEmptyEntries);
                    if (partes.Length < 3)
                        continue;

                    ListBoxControl Lista = null;

                    switch ((Quadro)int.Parse(partes[0]))
                    {
                        case Quadro.Coluna:
                            Lista = listBoxColuna;
                            break;
                        case Quadro.Linha:
                            Lista = listBoxLinha;
                            break;
                        case Quadro.Conteudo:
                            Lista = listBoxConteudo;
                            break;
                        case Quadro.Funil:
                            Lista = listBoxFunil;
                            break;
                        default:
                            continue;
                    }

                    switch (partes[1])
                    {
                        case "I":
                            Lista.Items.Add(new PortaItem(Campo.GetCampo(partes[2])));
                            break;
                        case "F":
                            Campo C = Campo.GetCampo(partes[2]);
                            object Argumento = null;
                            if (C.TypeCampo == typeof(string))
                                Argumento = partes[4];
                            else if ((C.TypeCampo == typeof(int)) || (C.TypeCampo == typeof(decimal)))
                                Argumento = decimal.Parse(partes[4]);
                            //else if(C.TypeCampo == typeof(decimal))
                            //    Argumento = decimal.Parse(partes[4]);
                            Lista.Items.Add(new PortaIntemFunil(C, (TiposFiltro)int.Parse(partes[3]), Argumento));
                            Lista.Visible = true;
                            break;
                    }
                }
            }
            catch 
            { 
            }
        }

        protected virtual void SalvarNoBanco(string Nome, string Gravacao, bool publica)
        { 
        }

        private void simpleButton2_Click(object sender, EventArgs e)
        {
            bool publica = false;
            bool cancelar = false;
            string Gravacao = Salvar();
            if (Gravacao != "")
            {
                if ((comboBoxEdit1.Text == "") || (comboBoxEdit1.Text.ToUpper().StartsWith("CONSULTA ")))
                {
                    try
                    {
                        BLOCRecuperaConfiguracaoDoBanco = true;
                        string novoNome = "";

                        if ((VirInput.Input.Execute("Nome da consulta:", ref novoNome)) && (novoNome != null))
                        {
                            comboBoxEdit1.Text = novoNome;
                            switch (MessageBox.Show("Salvar para todos os usuários ?", "Salvar", MessageBoxButtons.YesNoCancel, MessageBoxIcon.Question, MessageBoxDefaultButton.Button2))
                            {
                                case DialogResult.Yes:
                                    publica = true;
                                    break;
                                case DialogResult.No:
                                    publica = false;
                                    break;
                                case DialogResult.Cancel:
                                default:
                                    cancelar = true;
                                    break;
                            }

                        }
                        else
                            cancelar = true;
                    }
                    finally
                    {
                        BLOCRecuperaConfiguracaoDoBanco = false;
                    }
                }
                if (!cancelar)
                    SalvarNoBanco(comboBoxEdit1.Text, Gravacao, publica);
            }
        }

        

        private bool BLOCRecuperaConfiguracaoDoBanco = true;

        private void comboBoxEdit1_EditValueChanged(object sender, EventArgs e)
        {
            if (BLOCRecuperaConfiguracaoDoBanco)
                return;
            Restaura(RecuperaConfiguracaoDoBanco(comboBoxEdit1.Text));
        }

        /// <summary>
        /// Metodo a ser sobrescrito que popula os itens
        /// </summary>
        protected virtual void PopulaCombo()
        {
        }

        protected virtual string RecuperaConfiguracaoDoBanco(string nome)
        {
            return "";
        }

        
    }
}
