﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Text;
using System.Windows.Forms;
using CompontesBasicos;

namespace dllVirCubinho
{
    public partial class cDiagFiltrostr : ComponenteBaseDialog
    {
        /// <summary>
        /// Cosntrutor padrão
        /// </summary>
        public cDiagFiltrostr()
        {
            InitializeComponent();
        }

        public cDiagFiltrostr(Campo _Campo)
        {
            InitializeComponent();
            labelCampo.Text = string.Format("\"{0}\"", _Campo.Descricao);
        }

        public override bool CanClose()
        {
            if ((radioGroup1.EditValue == null) || (textEdit1.Text == null) || (textEdit1.Text == ""))
            {
                return false;
            }
            return base.CanClose();
        }

        public TiposFiltro TipoSelecionado;
        public string ValorSelecionado;

        protected override void FechaTela(DialogResult Resultado)
        {
            if (Resultado == DialogResult.OK)
            {
                TipoSelecionado = (TiposFiltro)radioGroup1.EditValue;
                ValorSelecionado = textEdit1.Text;
            };
            base.FechaTela(Resultado);
        }
    }
}
