﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Text;
using System.Windows.Forms;
using CompontesBasicos;
using System.Collections.Generic;
using DevExpress.XtraEditors;
using DevExpress.XtraEditors.Controls;

namespace dllVirCubinho
{
    public partial class cItemGrupo : ComponenteBaseDialog
    {
        public cItemGrupo()
        {
            InitializeComponent();
        }

        private List<CheckEdit> chs;

        public enum TipoSelecao { radio,multiplos}
        public enum Enables { Todos,numericos}

        public cItemGrupo(GrupoCampos Grupo,TipoSelecao tipo, Enables enables)
        {
            InitializeComponent();
            int i = 1;
            chs = new System.Collections.Generic.List<DevExpress.XtraEditors.CheckEdit>();
            //int checkEditDistance = panelControl1.Font.Height + 5;
            int checkEditDistance = panelControl1.Height / (Grupo.CamposdoGrupo.Count + 1);
            foreach (Campo campo in Grupo.CamposdoGrupo)
            {                
                CheckEdit newCheckEdit = new DevExpress.XtraEditors.CheckEdit();
                newCheckEdit.Text = campo.ToString();
                newCheckEdit.Parent = panelControl1;
                newCheckEdit.Top = i++ * checkEditDistance + panelControl1.DisplayRectangle.Y;
                newCheckEdit.Width = panelControl1.Width - 20;
                newCheckEdit.Tag = campo;
                newCheckEdit.Properties.RadioGroupIndex = tipo == TipoSelecao.multiplos ? -1 : 0;
                newCheckEdit.Properties.CheckStyle = tipo == TipoSelecao.multiplos ? CheckStyles.Standard : CheckStyles.Radio;
                newCheckEdit.Font = checkEdit1.Font;
                if (enables == Enables.numericos)
                {
                    if (campo.TypeCampo == typeof(string))
                        newCheckEdit.Enabled = false;
                    if (campo.TypeCampo == typeof(DateTime))
                        newCheckEdit.Enabled = false;
                }
                chs.Add(newCheckEdit);                
            }
            if (tipo == TipoSelecao.radio)
                checkEdit1.Visible = false;
        }

        public Campo Selecao;
        
        public List<Campo> Selecoes;
                            
        protected override void FechaTela(DialogResult Resultado)
        {
            if (Resultado == DialogResult.OK)
            {
                Selecoes = new System.Collections.Generic.List<Campo>();
                foreach (CheckEdit ch in chs)
                    if (ch.Checked)
                        Selecoes.Add((Campo)ch.Tag);
                if (Selecoes.Count > 0)
                    Selecao = Selecoes[0];
                else
                    Resultado = DialogResult.No;
            }
            base.FechaTela(Resultado);
        }

        private void checkEdit1_CheckedChanged(object sender, EventArgs e)
        {
            foreach (CheckEdit ch in chs)
                if (ch.Enabled)
                    ch.Checked = checkEdit1.Checked;
        }
    }

    
}
