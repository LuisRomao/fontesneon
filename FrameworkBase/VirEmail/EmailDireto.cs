// 29/04/2014 LH FIX destinat�rio = "" 13.2.8.37

using System;
using System.ComponentModel;
using DevExpress.XtraReports.UI;
using System.Net.Mail;

namespace VirEmail
{    
    /// <summary>
    /// Classe para envio dos relatorios via e.mail diretamente para o servidor SMTP
    /// </summary>
    public class EmailDireto
    {
        /// <summary>
        /// Servidor SMTP
        /// </summary>
        protected string SMTP;
        /// <summary>
        /// Conta
        /// </summary>
        protected string Usuario;
        /// <summary>
        /// Semha
        /// </summary>
        protected string Senha;
        /// <summary>
        /// Remetente
        /// </summary>
        protected string Remetente;

        /// <summary>
        /// Email para copia (problema com o anti-virus)
        /// </summary>
        protected string Copia;

        /// <summary>
        /// Erro no envio do e.mail
        /// </summary>
        public Exception UltimoErro;

        /// <summary>
        /// Construtor
        /// </summary>
        /// <param name="SMTP">Servidor SMTP</param>
        /// <param name="Usuario">Conta SMTP</param>
        /// <param name="Senha">Senha SMTP</param>
        /// <param name="Remetente">Remetente</param>
        public EmailDireto(string SMTP,string Usuario,string Senha,string Remetente) { 
            this.SMTP=SMTP;
            this.Usuario = Usuario;
            this.Senha = Senha;
            this.Remetente = Remetente;
        }

        /// <summary>
        /// Construtor 
        /// </summary>
        public EmailDireto() {}

        /// <summary>
        /// Ender�o default
        /// </summary>
        public string DestinatarioDefault = "";

        /// <summary>
        /// Endere�o do �ltimo envio
        /// </summary>
        public string UltimoEndereco;

        private static void SendCompletedCallback(object sender, AsyncCompletedEventArgs e)
        {
            // Get the unique identifier for this asynchronous operation.
            String token = (string)e.UserState;

            if (e.Cancelled)
            {
                System.Windows.Forms.MessageBox.Show("Cancelado:"+token);
            }
            if (e.Error != null)
            {
                System.Windows.Forms.MessageBox.Show("Erro:" + token);
            }
            else
            {
                System.Windows.Forms.MessageBox.Show("Enviado:" + token);
            }
           
        }

        /// <summary>
        /// Indica se o envio deve ser sincrono
        /// </summary>
        /// <remarks>n�o est� testado para asincrono</remarks>
        public bool Sincrono = true;

        /// <summary>
        /// para ser usado no envio asincrono se for implementado
        /// </summary>
        private int contador = 0;

        /// <summary>
        /// Indica se pode exibir messageBox
        /// </summary>
        public bool PermiteDialogos = false;

        /// <summary>
        /// HTML
        /// </summary>
        public bool IsBodyHtml = false;

        /// <summary>
        /// Porta
        /// </summary>
        public static int Porta = 0;

        private SmtpClient ClientLote;

        /// <summary>
        /// Abre um lote para envio de v�rios e-mails com uma conex�o - usar try finally com o FechaLote
        /// </summary>
        public void AbreLote()
        {
            ClientLote = NovoSmtpClient();            
        }

        /// <summary>
        /// Fecha o lote aberto com AbreLote - deve ser colocado no finally
        /// </summary>
        public void FechaLote()
        {
            if (ClientLote != null)
            {
                ClientLote.Dispose();
                ClientLote = null;
            }
        }

        /// <summary>
        /// Envio de e.mail
        /// </summary>
        /// <param name="Destinatario">destinatario</param>        
        /// <param name="datas">Anexos</param>
        /// <param name="ConteudoEmail">corpo do emais</param>
        /// <param name="Subject">assunto</param>
        /// <returns></returns>
        public virtual bool Enviar(string Destinatario, string ConteudoEmail, string Subject, params Attachment[] datas)
        {
            return _Enviar(false, Destinatario, ConteudoEmail, Subject, datas);
        }

        /// <summary>
        /// m�todo abstrato para ser implementado conforma a interface Win / Web / app etc
        /// </summary>
        /// <param name="Destinatario"></param>
        /// <param name="DestinatarioDefault"></param>
        /// <returns></returns>
        protected virtual bool ConsultaUsuario(ref string Destinatario, string DestinatarioDefault)        
        {
            return false;
        }

        private SmtpClient NovoSmtpClient()
        {
            SmtpClient client;
            if (SMTP == null)
            {
                client = new SmtpClient();
                if (client.Host == null)
                    throw new Exception("network host n�o configurado no .config");
            }
            else
            {
                if (Porta == 0)
                    client = new SmtpClient(SMTP);
                else                
                    client = new SmtpClient(SMTP, Porta);
                client.UseDefaultCredentials = false;
                client.Credentials = new System.Net.NetworkCredential(Usuario, Senha);
            }            
            client.Timeout = 200000;
            return client;
        }

        /// <summary>
        /// Envio de e.mail efetivo
        /// </summary>
        /// <param name="Assistido">Informa se existe um operador ou seja se pode usara di�logos - Note que "PermiteDialogos" � est�tico enquanto este parametro se refere a chamada</param>
        /// <param name="Destinatario"></param>
        /// <param name="ConteudoEmail"></param>
        /// <param name="Subject"></param>
        /// <param name="datas">Anexos</param>
        /// <returns></returns>
        protected bool _Enviar(bool Assistido, string Destinatario, string ConteudoEmail, string Subject, params Attachment[] datas)
        {
            
            UltimoErro = null;

            if ((Destinatario == "") && Assistido)
            {
                if (ConsultaUsuario(ref Destinatario, DestinatarioDefault))
                {
                    PermiteDialogos = true;
                }
                else
                {
                    UltimoEndereco = "";
                    return false;
                }
            }
            if (Destinatario == "")                            
                return false;
            
            if (Assistido)
                UltimoEndereco = Destinatario;
            Destinatario = Destinatario.Replace(" ", "");
            if (Destinatario == "")
            {
                UltimoErro = new Exception(string.Format("Endere�o inv�lido\r\n Destinat�rio:{0}",                  
                    Destinatario ?? "nulo"));
                return false;
            }
            MailMessage mensagem = null;
            
            try
            {
                if (Remetente != null)
                    mensagem = new MailMessage(Remetente, Destinatario.Replace(';', ','));
                else
                {
                    mensagem = new MailMessage();
                    mensagem.To.Add(Destinatario.Replace(';', ','));
                }
            }
            catch (System.FormatException e)
            {
                if (PermiteDialogos && Assistido)
                {
                    bool ok = false;
                    while (!ok)
                    {
                        System.Windows.Forms.MessageBox.Show(String.Format("<{0}> n�o � um e.mail v�lido", Destinatario));
                        //if (!VirInput.Input.Execute("E.mail:", ref Destinatario))
                        if (!ConsultaUsuario(ref Destinatario, ""))
                        {
                            Destinatario = UltimoEndereco = "";
                            return false;
                        }
                        else
                            try
                            {
                                mensagem = new MailMessage(Remetente, Destinatario.Replace(';', ','));
                                ok = true;
                            }
                            catch { }
                    }                    
                }
                else
                {
                    UltimoErro = new Exception(string.Format("Endere�o inv�lido\r\n Rementente:{0}\r\nDestinat�rio:{1}",
                    Remetente ?? "nulo",
                    Destinatario ?? "nulo"), e);
                    return false;
                }
            }
            catch (Exception e)
            {
                UltimoErro = new Exception(string.Format("Rementente:{0}\r\nDestinat�rio:{1}",
                    Remetente ?? "nulo",
                    Destinatario ?? "nulo"), e);
                return false;
            }
            //};
            mensagem.IsBodyHtml = IsBodyHtml;
            mensagem.Body = ConteudoEmail;
            mensagem.Subject = Subject;

            foreach (Attachment data in datas)
                mensagem.Attachments.Add(data);



            SmtpClient client;
            if (ClientLote == null)
            {
                client = NovoSmtpClient();                
            }
            else
                client = ClientLote;
            try
            {
                //client.SendCompleted += new SendCompletedEventHandler(SendCompletedCallback);
                //client.SendAsync(mensagem,NomeArquivo);
                if (Sincrono)
                    client.Send(mensagem);
                else
                    client.SendAsync(mensagem, contador++);



                if ((Sincrono) && (Copia != null) && (Copia != ""))
                {
                    //client = new SmtpClient(SMTP,587);
                    //client.UseDefaultCredentials = false;
                    //client.Credentials = new System.Net.NetworkCredential(Usuario, Senha);
                    mensagem.Attachments.Clear();
                    mensagem.To.Clear();
                    mensagem.To.Add(Copia);
                    client.Send(mensagem);
                }

                mensagem.Dispose();
                mensagem = null;
                //client = new System.Net.Mail.SmtpClient(SMTP);
                client = null;
                UltimoErro = null;
                return true;
            }
            catch (Exception e)
            {
                UltimoErro = e;
                //mensagem.Dispose();
                //mensagem = null;
                ClientLote = null;
                return false;
            };

        }

        /// <summary>
        /// Envia de e.mail com anexos
        /// </summary>
        /// <param name="Destinatario">Destinatario</param>        
        /// <param name="ConteudoEmail">Corpo do e.mail</param>
        /// <param name="Subject">Assunto</param>
        /// <param name="Anexos">Arquivos</param>
        /// <returns></returns>
        public virtual bool EnviarArquivos(string Destinatario, string ConteudoEmail, string Subject, params string[] Anexos)
        {
            if (Anexos.Length > 0)
            {
                Attachment[] datas = new Attachment[Anexos.Length];
                for (int i = 0; i < datas.Length; i++)
                    datas[i] = new Attachment(Anexos[i]);
                return Enviar(Destinatario, ConteudoEmail, Subject, datas);
            }
            else
                return Enviar(Destinatario, ConteudoEmail, Subject);
        }

        /// <summary>
        /// Envia o relatorio por e.mail
        /// </summary>
        /// <param name="Destinatario">Destinatario</param>
        /// <param name="NomeArquivo">Nome do arquivo pdf</param>
        /// <param name="Impresso">Relatorio a ser enviado</param>
        /// <param name="ConteudoEmail">Corpo do e.mail</param>
        /// <param name="Subject">Assunto</param>
        /// <param name="_DestinatarioDefault">Destinat�rio Default</param>
        /// <returns></returns>
        public virtual bool Enviar(string Destinatario, string NomeArquivo, XtraReport Impresso, string ConteudoEmail, string Subject, string _DestinatarioDefault = "")
        {
            DestinatarioDefault = _DestinatarioDefault;
            return Enviar(Destinatario, ConteudoEmail, Subject, GeraPDF(Impresso,NomeArquivo));            
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="Impresso"></param>
        /// <param name="NomeArquivo"></param>
        /// <returns></returns>
        protected Attachment GeraPDF(XtraReport Impresso, string NomeArquivo)
        {
            string Path = System.Windows.Forms.Application.StartupPath + @"\TMP\";
            string ArquivoPDF = String.Format("{0}{1}.pdf", Path, NomeArquivo);
            if (!System.IO.Directory.Exists(Path))
                System.IO.Directory.CreateDirectory(Path);
            if (System.IO.File.Exists(ArquivoPDF))
                try
                {
                    System.IO.File.Delete(ArquivoPDF);
                }
                catch
                {
                    ArquivoPDF = String.Format("{0}{1:ddMMyyhhmmss}.pdf", Path, DateTime.Now);
                };
            //ArquivoPDF = Path + DateTime.Now.ToString("ddMMyyhhmmss") + ".pdf";
            Impresso.ExportOptions.Pdf.Compressed = true;
            Impresso.ExportOptions.Pdf.ImageQuality = DevExpress.XtraPrinting.PdfJpegImageQuality.Lowest;
            Impresso.ExportToPdf(ArquivoPDF);
            return new Attachment(ArquivoPDF);               
        }        

    }


    /*
     * a abordagem utilizada apresenta o problema de ser vuneravel a troca de vers�o da framework
     * 
    public static class MailMessageExt
    {
        public static void Save(this MailMessage Message, string FileName)
        {
            Assembly assembly = typeof(SmtpClient).Assembly;
            Type _mailWriterType =
              assembly.GetType("System.Net.Mail.MailWriter");

            using (FileStream _fileStream =
                   new FileStream(FileName, FileMode.Create))
            {
                // Get reflection info for MailWriter contructor
                ConstructorInfo _mailWriterContructor =
                    _mailWriterType.GetConstructor(
                        BindingFlags.Instance | BindingFlags.NonPublic,
                        null,
                        new Type[] { typeof(Stream) },
                        null);

                // Construct MailWriter object with our FileStream
                object _mailWriter =
                  _mailWriterContructor.Invoke(new object[] { _fileStream });

                // Get reflection info for Send() method on MailMessage
                MethodInfo _sendMethod =
                    typeof(MailMessage).GetMethod(
                        "Send",
                        BindingFlags.Instance | BindingFlags.NonPublic);

                // Call method passing in MailWriter
                _sendMethod.Invoke(
                    Message,
                    BindingFlags.Instance | BindingFlags.NonPublic,
                    null,
                    new object[] { _mailWriter, true },
                    null);

                // Finally get reflection info for Close() method on our MailWriter
                MethodInfo _closeMethod =
                    _mailWriter.GetType().GetMethod(
                        "Close",
                        BindingFlags.Instance | BindingFlags.NonPublic);

                // Call close method
                _closeMethod.Invoke(
                    _mailWriter,
                    BindingFlags.Instance | BindingFlags.NonPublic,
                    null,
                    new object[] { },
                    null);
            }
        }
    }
     * 
     * outra abordagem seria gravar em arquivo:
     * 
     SmtpClient client = new SmtpClient("mysmtphost"); 
     client.DeliveryMethod = SmtpDeliveryMethod.SpecifiedPickupDirectory; 
     client.PickupDirectoryLocation = @"C:\somedirectory"; 
     client.Send(message);
    */

}
