using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;

namespace VirInput
{
    /// <summary>
    /// Classe est�tica para input
    /// </summary>
    public static class Input
    {
        /// <summary>
        /// Abre a tela de input para decimal
        /// </summary>
        /// <param name="Pergunta">Pergunta</param>
        /// <param name="Leitura">Retorno</param>       
        /// <returns>Retorna true se a tela foi fechada com OK</returns>
        public static bool Execute(string Pergunta, ref decimal Leitura)
        {
            return Execute(Pergunta, ref Leitura, 2);
        }


        /// <summary>
        /// Abre a tela de input para decimal
        /// </summary>
        /// <param name="Pergunta">Pergunta</param>
        /// <param name="Leitura">Retorno</param>    
        /// <param name="decimais">numero de casa decimais</param>
        /// <returns>Retorna true se a tela foi fechada com OK</returns>
        public static bool Execute(string Pergunta, ref decimal Leitura,int decimais)
        {
            Formularios.fMoney form = new VirInput.Formularios.fMoney();
            
            form.Pergunta.Text = Pergunta;
            form.RetornoMoney.Properties.Precision = decimais;
            form.RetornoMoney.Value = Leitura;
            if (form.ShowDialog() == System.Windows.Forms.DialogResult.OK)
            {
                
                    Leitura = form.RetornoMoney.Value;
                    return true;
                
            }
            else
            {
                Leitura = 0;
                return false;
            }
        }

        /// <summary>
        /// Abre a tela de input para decimal
        /// </summary>
        /// <param name="Pergunta">Pergunta</param>
        /// <param name="Leitura">Retorno</param>    
        /// <param name="decimais">numero de casa decimais</param>
        /// <param name="UsarSpin"></param>
        /// <returns>Retorna true se a tela foi fechada com OK</returns>
        public static bool Execute(string Pergunta, ref decimal Leitura, int decimais,bool UsarSpin)
        {
            if (!UsarSpin)
                return Execute(Pergunta, ref Leitura, decimais);
            Formularios.fInteger form = new VirInput.Formularios.fInteger();

            form.Pergunta.Text = Pergunta;
            form.RetornoInt.Properties.Increment = 0.01M;
            form.RetornoInt.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            form.RetornoInt.Properties.DisplayFormat.FormatString = "n2";
            form.RetornoInt.Value = Leitura;
            if (form.ShowDialog() == System.Windows.Forms.DialogResult.OK)
            {

                Leitura = form.RetornoInt.Value;
                return true;

            }
            else
            {
                Leitura = 0;
                return false;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="Pergunta"></param>
        /// <param name="Leitura"></param>
        /// <returns></returns>
        public static bool Execute(string Pergunta, ref string Leitura)
        {
            return Execute(Pergunta, ref Leitura, false);            
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="Pergunta"></param>
        /// <param name="Leitura"></param>
        /// <param name="Memo"></param>
        /// <returns></returns>
        public static bool Execute(string Pergunta,ref string Leitura, bool Memo) {
            return Execute(Pergunta,ref Leitura,Memo,-1);
        }


        /// <summary>
        /// Solicita��o de senha
        /// </summary>
        /// <param name="Pergunta">Pegunta</param>
        /// <returns>Senha digitada</returns>
        public static string Senha(string Pergunta)
        {
            Formularios.fString form = new Formularios.fString(-1);
            form.Pergunta.Text = Pergunta;
            form.RetornoStr.Properties.PasswordChar = '*';
            if (form.ShowDialog() == System.Windows.Forms.DialogResult.OK)
                return form.Retorno.ToUpper();
            else
                return "";
            
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="Pergunta"></param>
        /// <param name="Leitura"></param>
        /// <param name="Memo"></param>
        /// <param name="maximo"></param>
        /// <returns></returns>
        public static bool Execute(string Pergunta,ref string Leitura, bool Memo,int maximo) {
            Formularios.fBase form;
            
            if (Memo)
                form = new VirInput.Formularios.fMemo(maximo);
            else
                form = new Formularios.fString(maximo);
            
            form.Pergunta.Text = Pergunta;
            form.Retorno = Leitura;
            if (form.ShowDialog() == System.Windows.Forms.DialogResult.OK)
            {
                Leitura = form.Retorno;
                return true;
            }
            else
            {
                Leitura = "";
                return false;
            }
        }

        /// <summary>
        /// N�o usar obsoleto - usar DateTime?
        /// </summary>
        /// <param name="Pergunta"></param>
        /// <param name="Leitura"></param>
        /// <returns></returns>
        [ObsoleteAttribute("usar DateTime?")]        
        public static bool Execute(string Pergunta, ref DateTime Leitura)
        {
            Formularios.fData form = new VirInput.Formularios.fData();            
            form.Pergunta.Text = Pergunta;
            form.RetornoData.DateTime = Leitura;
            if (form.ShowDialog() == System.Windows.Forms.DialogResult.OK)
            {
                Leitura = form.RetornoData.DateTime;
                return true;
            }
            else
            {
                Leitura = DateTime.MinValue;
                return false;
            }
        }

        /// <summary>
        /// Entrada de Data
        /// </summary>
        /// <param name="Pergunta"></param>
        /// <param name="Leitura"></param>
        /// <param name="Minimo"></param>
        /// <param name="Maximo"></param>
        /// <returns></returns>
        public static bool Execute(string Pergunta, ref DateTime? Leitura,DateTime? Minimo,DateTime? Maximo)
        {
            Formularios.fData form = new VirInput.Formularios.fData();
            form.Pergunta.Text = Pergunta;
            if (Leitura.HasValue)
                form.RetornoData.DateTime = Leitura.Value;
            if (Minimo.HasValue)
                form.RetornoData.Properties.MinValue = Minimo.Value;
            if (Maximo.HasValue)
                form.RetornoData.Properties.MaxValue = Maximo.Value;
            if ((form.ShowDialog() == System.Windows.Forms.DialogResult.OK) && (form.RetornoData.EditValue != null))
            {
                Leitura = form.RetornoData.DateTime;
                return form.RetornoData != null;
            }
            else
            {
                Leitura = null;
                return false;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="Pergunta"></param>
        /// <param name="Leitura"></param>
        /// <param name="Alternativas"></param>
        /// <returns></returns>
        public static bool Execute(string Pergunta, ref string Leitura, System.Collections.ArrayList Alternativas)
        {
            Formularios.fComboString form = new VirInput.Formularios.fComboString();
            form.Pergunta.Text = Pergunta;
            form.RetornoCombo.Properties.Items.Clear();
            foreach (string Linha in Alternativas)
                form.RetornoCombo.Properties.Items.Add(Linha);
            if (form.ShowDialog() == System.Windows.Forms.DialogResult.OK)
            {
                Leitura = form.RetornoCombo.Text;
                return true;
            }
            else
            {
                Leitura = "";
                return false;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="Pergunta"></param>
        /// <param name="Leitura"></param>
        /// <returns></returns>
        public static bool Execute(string Pergunta, out Int32 Leitura)
        {
            return Execute(Pergunta, out Leitura, 0);
            /*
            Formularios.fInteger form = new VirInput.Formularios.fInteger();
            form.Pergunta.Text = Pergunta;
            //form.RetornoInt.Value = Leitura;
            if (form.ShowDialog() == System.Windows.Forms.DialogResult.OK)
            {
                Leitura = (int)form.RetornoInt.Value;
                return true;
            }
            else
            {
                Leitura = 0;
                return false;
            }*/
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="Pergunta"></param>
        /// <param name="Leitura"></param>
        /// <param name="Default"></param>
        /// <returns></returns>
        public static bool Execute(string Pergunta, out Int32 Leitura, Int32 Default)
        {
            Formularios.fInteger form = new VirInput.Formularios.fInteger();
            form.Pergunta.Text = Pergunta;
            form.RetornoInt.Value = Default;
            if (form.ShowDialog() == System.Windows.Forms.DialogResult.OK)
            {
                Leitura = (int)form.RetornoInt.Value;
                return true;
            }
            else
            {
                Leitura = 0;
                return false;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="Pergunta"></param>
        /// <param name="Leitura"></param>
        /// <returns></returns>
        public static bool Execute(string Pergunta, out Int64 Leitura)
        {
            Formularios.fInteger form = new VirInput.Formularios.fInteger();
            form.Pergunta.Text = Pergunta;
            //form.RetornoInt.Value = Leitura;
            if (form.ShowDialog() == System.Windows.Forms.DialogResult.OK)
            {
                Leitura = (Int64)form.RetornoInt.Value;
                return true;
            }
            else
            {
                Leitura = 0;
                return false;
            }
        }

        /// <summary>
        /// Dialogo input padr�o para selecionar um �tem em uma lista
        /// </summary>
        /// <param name="Pergunta">pegunta</param>
        /// <param name="Alternativas">alternativas (usar inteiros na chave)</param>
        /// <param name="Selecionado">chave selecionada</param>
        /// <returns></returns>
        public static bool Execute(string Pergunta, SortedList Alternativas, out int Selecionado)
        {
            Selecionado = -1;
            object oSelecionado;
            if (Execute(Pergunta, Alternativas, out oSelecionado, Formularios.cRadioCh.TipoSelecao.radio))
            {
                if (oSelecionado is int)
                {
                    Selecionado = (int)oSelecionado;
                    return true;
                }              
            }
            return false;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="Pergunta"></param>
        /// <param name="Alternativas"></param>
        /// <param name="Selecionados"></param>
        /// <returns></returns>
        public static bool Execute(string Pergunta, SortedList Alternativas, out List<int> Selecionados)
        {
            return Execute("", Pergunta, Alternativas, out Selecionados);
        }

        /// <summary>
        /// Dialogo input padr�o para selecionar v�rios itens em uma lista
        /// </summary>
        /// <param name="Pergunta">pegunta</param>
        /// <param name="Alternativas">lista com as alternativas, as chaves devem ser inteiras</param>
        /// <param name="Selecionados">array list com as chaves selecionadas</param>
        /// <param name="Titulo"></param>
        /// <returns></returns>
        public static bool Execute(string Titulo, string Pergunta, SortedList Alternativas, out List<int> Selecionados)
        {
            Selecionados = new List<int>();
            object oSelecionados;
            if (Execute(Titulo, Pergunta, Alternativas, out oSelecionados, Formularios.cRadioCh.TipoSelecao.multiplos))
            {
                try
                {
                    if (oSelecionados is ArrayList)
                    {
                        ArrayList arrSelecionados = (ArrayList)oSelecionados;
                        foreach (int selecionado in arrSelecionados)
                            Selecionados.Add(selecionado);
                        return true;
                    }
                }
                catch
                {                    
                }
            }
            return false;
        }

        /// <summary>
        /// Dialogo input padr�o para selecionar em uma lista
        /// </summary>
        /// <param name="Pergunta"></param>
        /// <param name="Alternativas"></param>
        /// <param name="Selecionado"></param>
        /// <param name="Tipo"></param>
        /// <returns></returns>
        public static bool Execute(string Pergunta, SortedList Alternativas, out object Selecionado, Formularios.cRadioCh.TipoSelecao Tipo)
        {
            return Execute("", Pergunta, Alternativas, out Selecionado, Tipo);
        }

        /// <summary>
        /// Dialogo input padr�o para selecionar em uma lista
        /// </summary>
        /// <param name="Pergunta">pergunta</param>
        /// <param name="Alternativas">lista com as alternativas</param>
        /// <param name="Selecionado">arraylist com as chaves ou a chave dependento do tipo</param>
        /// <param name="Tipo">multipla sele��o ou r�dio</param>
        /// <param name="Titulo">Titulo do form</param>
        /// <returns></returns>
        public static bool Execute(string Titulo, string Pergunta, SortedList Alternativas, out object Selecionado,Formularios.cRadioCh.TipoSelecao Tipo)
        {
            Formularios.cRadioCh cRadioCh1 = new Formularios.cRadioCh(Titulo, Pergunta, Alternativas, Tipo);
            if (cRadioCh1.VirShowModulo(CompontesBasicos.EstadosDosComponentes.PopUp) == System.Windows.Forms.DialogResult.OK)
            {
                if (Tipo == Formularios.cRadioCh.TipoSelecao.multiplos)
                    Selecionado = cRadioCh1.Selecoes;
                else
                    Selecionado = cRadioCh1.Selecao;
                return true;
            }
            else
            {
                Selecionado = null;
                return false;
            }
        }

    }
}
