namespace VirInput.Formularios
{
    partial class fInteger
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.RetornoInt = new DevExpress.XtraEditors.SpinEdit();
            ((System.ComponentModel.ISupportInitialize)(this.RetornoInt.Properties)).BeginInit();
            this.SuspendLayout();
            // 
            // Pergunta
            // 
            this.Pergunta.Appearance.Font = new System.Drawing.Font("Verdana", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Pergunta.Location = new System.Drawing.Point(13, 15);
            this.Pergunta.Margin = new System.Windows.Forms.Padding(3, 5, 3, 5);
            // 
            // simpleButton1
            // 
            this.simpleButton1.Location = new System.Drawing.Point(38, 100);
            this.simpleButton1.Margin = new System.Windows.Forms.Padding(3, 5, 3, 5);
            // 
            // simpleButton2
            // 
            this.simpleButton2.Location = new System.Drawing.Point(269, 100);
            this.simpleButton2.Margin = new System.Windows.Forms.Padding(3, 5, 3, 5);
            // 
            // RetornoInt
            // 
            this.RetornoInt.EditValue = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.RetornoInt.EnterMoveNextControl = true;
            this.RetornoInt.Location = new System.Drawing.Point(111, 42);
            this.RetornoInt.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.RetornoInt.Name = "RetornoInt";
            this.RetornoInt.Properties.Appearance.Font = new System.Drawing.Font("Verdana", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.RetornoInt.Properties.Appearance.Options.UseFont = true;
            this.RetornoInt.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.RetornoInt.Size = new System.Drawing.Size(168, 32);
            this.RetornoInt.TabIndex = 0;
            // 
            // fInteger
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(399, 143);
            this.Controls.Add(this.RetornoInt);
            this.Margin = new System.Windows.Forms.Padding(3, 5, 3, 5);
            this.Name = "fInteger";
            this.Text = " ";
            this.Load += new System.EventHandler(this.fInteger_Load);
            this.Controls.SetChildIndex(this.simpleButton1, 0);
            this.Controls.SetChildIndex(this.simpleButton2, 0);
            this.Controls.SetChildIndex(this.Pergunta, 0);
            this.Controls.SetChildIndex(this.RetornoInt, 0);
            ((System.ComponentModel.ISupportInitialize)(this.RetornoInt.Properties)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        /// <summary>
        /// 
        /// </summary>
        public DevExpress.XtraEditors.SpinEdit RetornoInt;

    }
}