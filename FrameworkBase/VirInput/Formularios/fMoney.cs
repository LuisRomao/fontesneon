using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace VirInput.Formularios
{
    /// <summary>
    /// Tela de input para decimal (Money)
    /// </summary>
    public partial class fMoney : VirInput.Formularios.fBase
    {
        /// <summary>
        /// Construtor principal
        /// </summary>
        public fMoney()
        {
            InitializeComponent();
            RetornoMoney.Focus();
        }
    }
}

