namespace VirInput.Formularios
{
    partial class fComboString
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.RetornoCombo = new DevExpress.XtraEditors.ComboBoxEdit();
            ((System.ComponentModel.ISupportInitialize)(this.RetornoCombo.Properties)).BeginInit();
            this.SuspendLayout();
            // 
            // Pergunta
            // 
            this.Pergunta.Appearance.Font = new System.Drawing.Font("Verdana", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Pergunta.Appearance.Options.UseFont = true;
            // 
            // RetornoCombo
            // 
            this.RetornoCombo.EnterMoveNextControl = true;
            this.RetornoCombo.Location = new System.Drawing.Point(143, 20);
            this.RetornoCombo.Name = "RetornoCombo";
            this.RetornoCombo.Properties.Appearance.Font = new System.Drawing.Font("Verdana", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.RetornoCombo.Properties.Appearance.Options.UseFont = true;
            this.RetornoCombo.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.RetornoCombo.Properties.Items.AddRange(new object[] {
            "Adv 1",
            "Adv 2"});
            this.RetornoCombo.Properties.Sorted = true;
            this.RetornoCombo.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor;
            this.RetornoCombo.Size = new System.Drawing.Size(435, 26);
            this.RetornoCombo.TabIndex = 0;
            // 
            // fComboString
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(608, 95);
            this.Controls.Add(this.RetornoCombo);
            this.Name = "fComboString";
            this.Text = "dComboString";
            this.Controls.SetChildIndex(this.RetornoCombo, 0);
            this.Controls.SetChildIndex(this.simpleButton1, 0);
            this.Controls.SetChildIndex(this.simpleButton2, 0);
            this.Controls.SetChildIndex(this.Pergunta, 0);
            ((System.ComponentModel.ISupportInitialize)(this.RetornoCombo.Properties)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        /// <summary>
        /// 
        /// </summary>
        public DevExpress.XtraEditors.ComboBoxEdit RetornoCombo;

    }
}