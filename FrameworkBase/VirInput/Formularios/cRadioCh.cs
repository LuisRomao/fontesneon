﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Windows.Forms;
using CompontesBasicos;
using DevExpress.XtraEditors;
using DevExpress.XtraEditors.Controls;

namespace VirInput.Formularios
{
    /// <summary>
    /// 
    /// </summary>
    public partial class cRadioCh : ComponenteBaseDialog
    {
        private List<CheckEdit> chs;

        /// <summary>
        /// 
        /// </summary>
        public object Selecao;

        /// <summary>
        /// 
        /// </summary>
        public ArrayList Selecoes;

        /// <summary>
        /// 
        /// </summary>
        public enum TipoSelecao 
        {
            /// <summary>
            /// 
            /// </summary>
            radio,
            /// <summary>
            /// 
            /// </summary>
            multiplos 
        }

        /// <summary>
        /// Padrão
        /// </summary>
        public cRadioCh()
        {
            InitializeComponent();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="Alternativas"></param>
        /// <param name="tipo"></param>
        public cRadioCh(SortedList Alternativas, TipoSelecao tipo)
        {
            InitializeComponent();
            AjusteIniciais(Alternativas, tipo);            
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="Titulo"></param>
        /// <param name="Pergunta"></param>
        /// <param name="Alternativas"></param>
        /// <param name="tipo"></param>
        public cRadioCh(string Titulo, string Pergunta, SortedList Alternativas, TipoSelecao tipo)
        {
            InitializeComponent();
            AjusteIniciais(Alternativas, tipo);
            this.Titulo = Titulo;
            if (Pergunta != "")
            {
                labelPergunta.Text = Pergunta;
                panelPergunta.Visible = true;
            }            
        }

        private void AjusteIniciais(SortedList Alternativas, TipoSelecao tipo)
        {
            int i = 1;
            chs = new System.Collections.Generic.List<DevExpress.XtraEditors.CheckEdit>();
            int checkEditDistance = panelControl1.Height / (Alternativas.Count + 1);
            foreach (DictionaryEntry DE in Alternativas)
            {
                CheckEdit newCheckEdit = new DevExpress.XtraEditors.CheckEdit();
                newCheckEdit.Text = DE.Value.ToString();
                newCheckEdit.Parent = panelControl1;
                newCheckEdit.Top = i++ * checkEditDistance + panelControl1.DisplayRectangle.Y;
                newCheckEdit.Width = panelControl1.Width - 20;
                newCheckEdit.Tag = DE.Key;
                newCheckEdit.Properties.RadioGroupIndex = tipo == TipoSelecao.multiplos ? -1 : 0;
                newCheckEdit.Properties.CheckStyle = tipo == TipoSelecao.multiplos ? CheckStyles.Standard : CheckStyles.Radio;
                newCheckEdit.Font = checkEdit1.Font;
                chs.Add(newCheckEdit);
            }
            if (tipo == TipoSelecao.radio)
                checkEdit1.Visible = false;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="Resultado"></param>
        protected override void FechaTela(DialogResult Resultado)
        {
            if (Resultado == DialogResult.OK)
            {
                Selecoes = new ArrayList();
                foreach (CheckEdit ch in chs)
                    if (ch.Checked)
                        Selecoes.Add(ch.Tag);
                if (Selecoes.Count > 0)
                    Selecao = Selecoes[0];
                else
                    Resultado = DialogResult.No;
            }
            base.FechaTela(Resultado);
        }

        private void checkEdit1_CheckedChanged(object sender, EventArgs e)
        {
            foreach (CheckEdit ch in chs)
                if (ch.Enabled)
                    ch.Checked = checkEdit1.Checked;
        }

        private void cRadioCh_Load(object sender, EventArgs e)
        {
            foreach (CheckEdit ch in chs)
                if (ch.Enabled)
                    ch.CheckState = CheckState.Unchecked;
        }
    }
}
