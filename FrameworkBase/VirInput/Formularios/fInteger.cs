using System;
using System.Windows.Forms;

namespace VirInput.Formularios
{
    /// <summary>
    /// 
    /// </summary>
    public partial class fInteger : VirInput.Formularios.fBase
    {
        /// <summary>
        /// 
        /// </summary>
        public fInteger()
        {
            InitializeComponent();
            Text = Application.ProductName;
        }

        private void fInteger_Load(object sender, EventArgs e)
        {
            RetornoInt.Focus();
        }
    }
}