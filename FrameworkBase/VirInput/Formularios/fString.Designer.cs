namespace VirInput.Formularios
{
    partial class fString
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.RetornoStr = new DevExpress.XtraEditors.TextEdit();
            ((System.ComponentModel.ISupportInitialize)(this.RetornoStr.Properties)).BeginInit();
            this.SuspendLayout();
            // 
            // Pergunta
            // 
            this.Pergunta.Appearance.Font = new System.Drawing.Font("Verdana", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Pergunta.Appearance.Options.UseFont = true;
            // 
            // simpleButton1
            // 
            this.simpleButton1.Location = new System.Drawing.Point(34, 79);
            // 
            // simpleButton2
            // 
            this.simpleButton2.Location = new System.Drawing.Point(480, 79);
            // 
            // RetornoStr
            // 
            this.RetornoStr.EnterMoveNextControl = true;
            this.RetornoStr.Location = new System.Drawing.Point(12, 47);
            this.RetornoStr.Name = "RetornoStr";
            this.RetornoStr.Properties.Appearance.Font = new System.Drawing.Font("Verdana", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.RetornoStr.Properties.Appearance.Options.UseFont = true;
            this.RetornoStr.Size = new System.Drawing.Size(566, 26);
            this.RetornoStr.TabIndex = 0;
            // 
            // fString
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.ClientSize = new System.Drawing.Size(601, 115);
            this.Controls.Add(this.RetornoStr);
            this.Name = "fString";
            this.Controls.SetChildIndex(this.simpleButton1, 0);
            this.Controls.SetChildIndex(this.simpleButton2, 0);
            this.Controls.SetChildIndex(this.Pergunta, 0);
            this.Controls.SetChildIndex(this.RetornoStr, 0);
            ((System.ComponentModel.ISupportInitialize)(this.RetornoStr.Properties)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        /// <summary>
        /// 
        /// </summary>
        public DevExpress.XtraEditors.TextEdit RetornoStr;

    }
}
