using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace VirInput.Formularios
{
    /// <summary>
    /// Tela de input para data
    /// </summary>
    public partial class fData : VirInput.Formularios.fBase
    {
        /// <summary>
        /// construtor principal
        /// </summary>
        public fData()
        {
            InitializeComponent();
            RetornoData.Focus();
        }

        
    }
}