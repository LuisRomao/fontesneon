
namespace VirInput.Formularios
{
    /// <summary>
    /// 
    /// </summary>
    public partial class fMemo : VirInput.Formularios.fBase
    {
        /// <summary>
        /// 
        /// </summary>
        /// <param name="maximo"></param>
        public fMemo(int maximo)
        {
            InitializeComponent();
            if (maximo != -1)
                memoEdit1.Properties.MaxLength = maximo;
            memoEdit1.Focus();
        }

        /// <summary>
        /// 
        /// </summary>
        public override string Retorno
        {
            get
            {
                return this.memoEdit1.Text;
            }
            set
            {
                memoEdit1.Text = value;
            }
        }
    }
}

