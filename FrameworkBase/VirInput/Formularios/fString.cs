
namespace VirInput.Formularios
{
    /// <summary>
    /// 
    /// </summary>
    public partial class fString : VirInput.Formularios.fBase
    {
        /// <summary>
        /// 
        /// </summary>
        /// <param name="maximo"></param>
        public fString(int maximo)
        {
            InitializeComponent();
            if (maximo != -1)
                RetornoStr.Properties.MaxLength = maximo;
            RetornoStr.Focus();
        }

        /// <summary>
        /// 
        /// </summary>
        public override string Retorno
        {
            get
            {
                return RetornoStr.Text;
            }
            set
            {
                RetornoStr.Text = value;
            }
        }
    }
}

