namespace VirInput.Formularios
{
    partial class fMoney
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.RetornoMoney = new DevExpress.XtraEditors.CalcEdit();
            ((System.ComponentModel.ISupportInitialize)(this.RetornoMoney.Properties)).BeginInit();
            this.SuspendLayout();
            // 
            // Pergunta
            // 
            this.Pergunta.Appearance.Font = new System.Drawing.Font("Verdana", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            // 
            // simpleButton1
            // 
            this.simpleButton1.Location = new System.Drawing.Point(34, 91);
            // 
            // simpleButton2
            // 
            this.simpleButton2.Location = new System.Drawing.Point(499, 91);
            // 
            // RetornoMoney
            // 
            this.RetornoMoney.Location = new System.Drawing.Point(233, 51);
            this.RetornoMoney.Name = "RetornoMoney";
            this.RetornoMoney.Properties.Appearance.Font = new System.Drawing.Font("Verdana", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.RetornoMoney.Properties.Appearance.Options.UseFont = true;
            this.RetornoMoney.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.RetornoMoney.Properties.DisplayFormat.FormatString = "n2";
            this.RetornoMoney.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.RetornoMoney.Properties.EditFormat.FormatString = "n2";
            this.RetornoMoney.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.RetornoMoney.Properties.Precision = 2;
            this.RetornoMoney.Size = new System.Drawing.Size(142, 26);
            this.RetornoMoney.TabIndex = 3;
            // 
            // fMoney
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.ClientSize = new System.Drawing.Size(608, 126);
            this.Controls.Add(this.RetornoMoney);
            this.Name = "fMoney";
            this.Controls.SetChildIndex(this.simpleButton1, 0);
            this.Controls.SetChildIndex(this.Pergunta, 0);
            this.Controls.SetChildIndex(this.simpleButton2, 0);
            this.Controls.SetChildIndex(this.RetornoMoney, 0);
            ((System.ComponentModel.ISupportInitialize)(this.RetornoMoney.Properties)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        /// <summary>
        /// 
        /// </summary>
        public DevExpress.XtraEditors.CalcEdit RetornoMoney;

    }
}
