using System;
using System.Data;
using System.Drawing;
using System.Windows.Forms;
using System.Collections;
using System.Globalization;

namespace VirInput.Importador
{
    /// <summary>
    /// Objeto para importa��o de dados em forma de planilha
    /// </summary>
    public partial class cImportador : CompontesBasicos.ComponenteBaseDialog
    {
        /// <summary>
        /// Construtor padr�o
        /// </summary>
        public cImportador()
        {
            Iniciar();            
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="_NomeArq"></param>
        public cImportador(string _NomeArq)
        {            
            Iniciar();
            string Caminho = string.Format("{0}\\XML", System.IO.Path.GetDirectoryName(Application.ExecutablePath));
            if (!System.IO.Directory.Exists(Caminho))
                System.IO.Directory.CreateDirectory(Caminho);
            NomeArq = string.Format("{0}\\{1}.XML",Caminho,_NomeArq);
            dImpDefault = new dImportador();
            if (System.IO.File.Exists(NomeArq))
                dImpDefault.ReadXml(NomeArq);
        }

        private void Iniciar()
        {
            InitializeComponent();
            AddColunaImp("SEM USU", "SEM USU", typeof(string));
        }

        private dImportador dImpDefault;



        private void Salvar()
        {
            if (dImpDefault != null)
            {
                dImpDefault.Default.Clear();
                foreach (ItenColunaImp it in conjuntoColunasImp)
                    if (it.Coluna >= 0)
                    {
                        dImpDefault.Default.AddDefaultRow(it.Nome, it.Coluna);
                    }
                dImpDefault.WriteXml(NomeArq);
            }
        }

        private string NomeArq;

        private ConjuntoColunasImp _conjuntoColunasImp;

        private ConjuntoColunasImp conjuntoColunasImp
        {
            get
            {
                return _conjuntoColunasImp ?? (_conjuntoColunasImp = new ConjuntoColunasImp());
            }
        }

        /// <summary>
        /// Verifica se a coluna foi informada pelo usu�rio
        /// </summary>
        /// <param name="NomeColuna"></param>
        /// <returns></returns>
        public bool IsColunaImplementada(string NomeColuna)
        {
            foreach (ItenColunaImp it in conjuntoColunasImp)
                if (it.Nome == NomeColuna)
                    return (it.Coluna >= 0);
            return false;
        }

        /// <summary>
        /// DataTable que vai conter os dados lidos
        /// </summary>
        public DataTable _DataTable;

        private void CriaColuna(DataColumn NovaColuna, bool SomenteLeitura)
        {
            DevExpress.XtraGrid.Columns.GridColumn DvnovaColuna = gridView1.Columns.Add();
            DvnovaColuna.FieldName = NovaColuna.ColumnName;
            DvnovaColuna.Visible = true;
            DvnovaColuna.Width = 160;
            DvnovaColuna.OptionsColumn.AllowEdit = !SomenteLeitura;
            if (SomenteLeitura)
                DvnovaColuna.Fixed = DevExpress.XtraGrid.Columns.FixedStyle.Right;
        }

        /// <summary>
        /// Carega os dados da �rea de transfer�ncia
        /// </summary>
        public bool Carrega()
        {
            Char[] QuebraDeLinha = new Char[2] { '\r', '\n' };
            Char[] QuebraDeColuna = new Char[1] { '\t' };
            gridView1.Columns.Clear();


            if (Clipboard.GetData(System.Windows.Forms.DataFormats.Text) == null)
                return false;
            string Clip = Clipboard.GetData(System.Windows.Forms.DataFormats.Text).ToString();
            String[] Linhas = Clip.Split(QuebraDeLinha, StringSplitOptions.RemoveEmptyEntries);
            if (Linhas.Length == 0)
                return false;
            _DataTable = new DataTable();
            int colunas = 1;
            string[] Campos = Linhas[0].Split(QuebraDeColuna);

            CriaColuna(_DataTable.Columns.Add("Coluna 1"), false);
            //CriaColuna(_DataTable.Columns.Add("Coluna 2"), false);
            //CriaColuna
            //DataRow rowTitulo = _DataTable.Rows.Add(new string[2] { "BLOCO", "APARTAMENTO" });
            DataRow rowTitulo = _DataTable.Rows.Add("");




            foreach (string Linha in Linhas)
            {
                string[] Campos1 = Linha.Split(QuebraDeColuna);
                for (int i = colunas; i < Campos1.Length; i++, colunas++)
                    CriaColuna(_DataTable.Columns.Add("Coluna " + ((int)(i + 1)).ToString()), false);
                _DataTable.Rows.Add(Campos1);
            };
            CriaColuna(_DataTable.Columns.Add("Status"), true);
            _DataTable.Columns.Add("OK", typeof(bool));
            //_DataTable.Columns.Add("APT", typeof(int));
            rowTitulo["Status"] = " - ";
            rowTitulo["OK"] = false;
            foreach (ItenColunaImp Iten in conjuntoColunasImp)
                _DataTable.Columns.Add(Iten.Nome, Iten.Tipo);
            gridControl1.DataSource = _DataTable;



            if (dImpDefault != null)
            {
                if (dImpDefault.Default.Count == 0)
                {
                    foreach (ItenColunaImp it in conjuntoColunasImp)
                        if (it.Coluna >= 0)
                        {
                            dImpDefault.Default.AddDefaultRow(it.Nome, it.Coluna);
                        }
                }
                foreach (dImportador.DefaultRow row in dImpDefault.Default)
                    //if (_DataTable.Columns.Count > (row.Coluna + 1))
                    if (colunas >= (row.Coluna + 1))
                    {
                        if (_DataTable.Columns[row.Coluna].DataType == typeof(string))
                        {
                            
                            ItenColunaImp IC = conjuntoColunasImp.GetItenColunaImp(row.Nome);
                            if (IC != null)
                            {
                                IC.Coluna = row.Coluna;
                                _DataTable.Rows[0][row.Coluna] = IC.Titulo;
                            }
                        }
                    }

            };
            

            //ValidarDados();
            btnConfirmar_F.Enabled = false;
            BtValida.Enabled = true;
            return true;
        }

        private bool ValidarDados()
        {
            foreach (ItenColunaImp Iten in conjuntoColunasImp)
                if (Iten.ColunaObrigatoria && (Iten.Coluna == -1))
                {
                    MessageBox.Show(String.Format("Coluna obrigat�ria nao informada: '{0}'", Iten.Titulo));
                    return false;
                };
            bool primeira = true;
            spinEditV.Value = spinEditNV.Value = 0;
            int V = 0;
            int NV = 0;
            using (CompontesBasicos.Espera.cEspera Esp = new CompontesBasicos.Espera.cEspera(this))
            {
                if (Esp.Abortar)
                    return false;
                Esp.Espere("Validando linhas");
                Esp.AtivaGauge(_DataTable.Rows.Count);
                DateTime horajuste = DateTime.Now.AddSeconds(3);
                foreach (DataRow DR in _DataTable.Rows)
                {
                    if (!primeira)
                        if (ValidarRow(DR))
                            V++;
                        else
                            NV++;
                    primeira = false;
                    if (DateTime.Now > horajuste)
                    {
                        horajuste = DateTime.Now.AddSeconds(3);
                        Esp.Gauge(V + NV);
                        Application.DoEvents();                        
                    }
                }
            }
            spinEditV.Value = V;
            spinEditNV.Value = NV;
            btnConfirmar_F.Enabled = true;            
            BtValida.Enabled = false;
            return true;
        }

        #region Validadores

        private DateTimeFormatInfo _FormataData;

        /// <summary>
        /// 
        /// </summary>
        public DateTimeFormatInfo FormataData
        {
            get
            {
                if (_FormataData == null)
                    _FormataData = CultureInfo.CurrentCulture.DateTimeFormat;
                return _FormataData;
            }
            set
            {
                if (_FormataData == value)
                    return;
                _FormataData = value;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        public enum TratamentoErro
        {
            /// <summary>
            /// 
            /// </summary>
            TratarBrancoComoZero,
            /// <summary>
            /// 
            /// </summary>
            TratarBrancoComoErro,
            /// <summary>
            /// 
            /// </summary>
            TratarErrocomoZero,
            /// <summary>
            /// 
            /// </summary>
            TratarBrancocomoNulo
        }

        /// <summary>
        /// 
        /// </summary>
        public static TratamentoErro Validacao_decimal = TratamentoErro.TratarBrancoComoZero;

        private static void Validardecimal(DataRow DR, ItenColunaImp Iten)
        {
            if (DR[Iten.Coluna].ToString().Replace(" ", "") == "")
                if (Validacao_decimal == TratamentoErro.TratarBrancoComoZero)
                    DR[Iten.Coluna] = "0";
                else if (Validacao_decimal == TratamentoErro.TratarBrancocomoNulo)
                {
                    DR[Iten.Coluna] = DBNull.Value;
                    return;
                }
            decimal retorno;
            bool erronocampo = false;
            if (!decimal.TryParse(DR[Iten.Coluna].ToString().Replace(" ", ""), out retorno))
            {
                if (Validacao_decimal == TratamentoErro.TratarErrocomoZero)
                    DR[Iten.Nome] = 0M;
                else
                {
                    DR.SetColumnError(Iten.Coluna, "Valor inv�lido");
                    DR["Status"] += (String.Format("|Valor para '{0}' inv�lido", Iten.Titulo));
                    DR.RowError = "Valores inv�lidos";
                    erronocampo = true;
                }
            }
            else
            {

                if ((Iten.Minimo != null) && (retorno < (decimal)Iten.Minimo))
                {
                    DR.SetColumnError(Iten.Coluna, String.Format("Valor inv�lido (m�nimo = '{0}')", Iten.Minimo));
                    DR["Status"] += (String.Format("|Valor para '{0}' inv�lido", Iten.Titulo));
                    DR.RowError = "Valores inv�lidos";
                    erronocampo = true;
                }
                if ((Iten.Maximo != null) && (retorno > (decimal)Iten.Maximo))
                {
                    DR.SetColumnError(Iten.Coluna, String.Format("Valor inv�lido (m�ximo = '{0}')", Iten.Maximo));
                    DR["Status"] += (String.Format("|Valor para '{0}' inv�lido", Iten.Titulo));
                    DR.RowError = "Valores inv�lidos";
                    erronocampo = true;
                }
            };
            if (!erronocampo)
                DR[Iten.Nome] = retorno;
            else
                DR[Iten.Nome] = DBNull.Value;
        }

        private void ValidarData(DataRow DR, ItenColunaImp Iten)
        {
            DateTime retorno;
            bool erronocampo = false;
            if (!DateTime.TryParse(DR[Iten.Coluna].ToString(), FormataData, DateTimeStyles.AllowWhiteSpaces, out retorno))
            {
                DR.SetColumnError(Iten.Coluna, "Valor inv�lido");
                DR["Status"] += (String.Format("|Valor para '{0}' inv�lido", Iten.Titulo));
                DR.RowError = "Valores inv�lidos";
                erronocampo = true;
            }
            else
            {
                if ((Iten.Minimo != null) && (retorno < (DateTime)Iten.Minimo))
                {
                    DR.SetColumnError(Iten.Coluna, String.Format("Valor inv�lido (m�nimo = '{0}')", Iten.Minimo));
                    DR["Status"] += (String.Format("|Valor para '{0}' inv�lido", Iten.Titulo));
                    DR.RowError = "Valores inv�lidos";
                    erronocampo = true;
                }
                if ((Iten.Maximo != null) && (retorno > (DateTime)Iten.Maximo))
                {
                    DR.SetColumnError(Iten.Coluna, String.Format("Valor inv�lido (m�ximo = '{0}')", Iten.Maximo));
                    DR["Status"] += (String.Format("|Valor para '{0}' inv�lido", Iten.Titulo));
                    DR.RowError = "Valores inv�lidos";
                    erronocampo = true;
                }
            };
            if (!erronocampo)
                DR[Iten.Nome] = retorno;
            else
                DR[Iten.Nome] = DBNull.Value;
        }

        private static void Validardouble(DataRow DR, ItenColunaImp Iten)
        {
            double retorno;
            bool erronocampo = false;
            if (!double.TryParse(DR[Iten.Coluna].ToString(), out retorno))
            {
                DR.SetColumnError(Iten.Coluna, "Valor inv�lido");
                DR["Status"] += (String.Format("|Valor para '{0}' inv�lido", Iten.Titulo));
                DR.RowError = "Valores inv�lidos";
                erronocampo = true;
            }
            else
            {
                if ((Iten.Minimo != null) && (retorno < (double)Iten.Minimo))
                {
                    DR.SetColumnError(Iten.Coluna, String.Format("Valor inv�lido (m�nimo = '{0}')", Iten.Minimo));
                    DR["Status"] += (String.Format("|Valor para '{0}' inv�lido", Iten.Titulo));
                    DR.RowError = "Valores inv�lidos";
                    erronocampo = true;
                }
                if ((Iten.Maximo != null) && (retorno > (double)Iten.Maximo))
                {
                    DR.SetColumnError(Iten.Coluna, String.Format("Valor inv�lido (m�ximo = '{0}')", Iten.Maximo));
                    DR["Status"] += (String.Format("|Valor para '{0}' inv�lido", Iten.Titulo));
                    DR.RowError = "Valores inv�lidos";
                    erronocampo = true;
                }
            };
            if (!erronocampo)
                DR[Iten.Nome] = retorno;
            else
                DR[Iten.Nome] = DBNull.Value;

        }

        private static void Validarstring(DataRow DR, ItenColunaImp Iten)
        {
            bool erronocampo = false;
            string retorno = DR[Iten.Coluna].ToString();
            if ((Iten.Minimo != null) && (retorno.Length < (int)Iten.Minimo))
            {
                DR.SetColumnError(Iten.Coluna, String.Format("Valor inv�lido (m�nimo = '{0}' caracteres)", Iten.Minimo));
                DR["Status"] += ("|" + "Valor para '" + Iten.Titulo + "' inv�lido");
                DR.RowError = "Valores inv�lidos";
                erronocampo = true;
            }
            if ((Iten.Maximo != null) && (retorno.Length > (int)Iten.Maximo))
            {
                DR.SetColumnError(Iten.Coluna, "Valor inv�lido (m�ximo = '" + Iten.Maximo.ToString() + "') caracteres");
                DR["Status"] += ("|" + "Valor para '" + Iten.Titulo + "' inv�lido");
                DR.RowError = "Valores inv�lidos";
                erronocampo = true;
            };
            if (!erronocampo)
                DR[Iten.Nome] = retorno;
            else
                DR[Iten.Nome] = DBNull.Value;
        }

        /// <summary>
        /// 
        /// </summary>
        public static bool PremiteIntNegativo = false;

        private static void Validarint(DataRow DR, ItenColunaImp Iten)
        {
            int retorno;
            bool erronocampo = false;

            if ((!Iten.ColunaObrigatoria) && ((DR[Iten.Coluna] == DBNull.Value) || ((string)DR[Iten.Coluna] == "")))
            {
                DR[Iten.Nome] = DBNull.Value;
            }

            else
            {
                NumberStyles styles = NumberStyles.AllowLeadingWhite | NumberStyles.AllowThousands | NumberStyles.AllowDecimalPoint;
                if (PremiteIntNegativo)
                    styles |= NumberStyles.AllowLeadingSign;
                if (!int.TryParse(DR[Iten.Coluna].ToString(),styles, CultureInfo.CurrentCulture, out retorno))
                {
                    DR.SetColumnError(Iten.Coluna, "Valor inv�lido");
                    DR["Status"] += ("|" + "Valor para '" + Iten.Titulo + "' inv�lido");
                    DR.RowError = "Valores inv�lidos";
                    erronocampo = true;
                }
                else
                {
                    if ((Iten.Minimo != null) && (retorno < (int)Iten.Minimo))
                    {
                        DR.SetColumnError(Iten.Coluna, "Valor inv�lido (m�nimo = '" + Iten.Minimo.ToString() + "')");
                        DR["Status"] += ("|" + "Valor para '" + Iten.Titulo + "' inv�lido");
                        DR.RowError = "Valores inv�lidos";
                        erronocampo = true;
                    }
                    if ((Iten.Maximo != null) && (retorno > (int)Iten.Maximo))
                    {
                        DR.SetColumnError(Iten.Coluna, "Valor inv�lido (m�ximo = '" + Iten.Maximo.ToString() + "')");
                        DR["Status"] += ("|" + "Valor para '" + Iten.Titulo + "' inv�lido");
                        DR.RowError = "Valores inv�lidos";
                        erronocampo = true;
                    }
                };
                if (!erronocampo)
                    DR[Iten.Nome] = retorno;
                else
                    DR[Iten.Nome] = DBNull.Value;
            }
        }

        

        private static void Validarlong(DataRow DR, ItenColunaImp Iten)
        {
            
            bool erronocampo = false;
            long retorno;
            if (!long.TryParse(DR[Iten.Coluna].ToString(), out retorno))
            {
                DR.SetColumnError(Iten.Coluna, "Valor inv�lido");
                DR["Status"] += (String.Format("|Valor para '{0}' inv�lido", Iten.Titulo));
                DR.RowError = "Valores inv�lidos";
                erronocampo = true;
            }
            else
            {
                if ((Iten.Minimo != null) && (retorno < (long)Iten.Minimo))
                {
                    DR.SetColumnError(Iten.Coluna, String.Format("Valor inv�lido (m�nimo = '{0}')", Iten.Minimo));
                    DR["Status"] += (String.Format("|Valor para '{0}' inv�lido", Iten.Titulo));
                    DR.RowError = "Valores inv�lidos";
                    erronocampo = true;
                }
                if ((Iten.Maximo != null) && (retorno > (long)Iten.Maximo))
                {
                    DR.SetColumnError(Iten.Coluna, String.Format("Valor inv�lido (m�ximo = '{0}')", Iten.Maximo));
                    DR["Status"] += (String.Format("|Valor para '{0}' inv�lido", Iten.Titulo));
                    DR.RowError = "Valores inv�lidos";
                    erronocampo = true;
                }
            };
            if (!erronocampo)
                DR[Iten.Nome] = retorno;
            else
                DR[Iten.Nome] = DBNull.Value;
        }

        private bool ValidarRow(DataRow DR)
        {
            if (DR["Status"].ToString() == " - ")
                return false;
            DR.RowError = "";
            DR.ClearErrors();
            DR["Status"] = "";
            //Validar colunas extra
            
            foreach (ItenColunaImp Iten in conjuntoColunasImp)
                if (Iten.Coluna != -1)
                {
                    if (Iten.Tipo == typeof(decimal))
                        Validardecimal(DR, Iten);
                    else if (Iten.Tipo == typeof(DateTime))
                        ValidarData(DR, Iten);
                    else if (Iten.Tipo == typeof(double))
                        Validardouble(DR, Iten);
                    else if (Iten.Tipo == typeof(string))
                        Validarstring(DR, Iten);
                    else if (Iten.Tipo == typeof(int))
                        Validarint(DR, Iten);
                    else if (Iten.Tipo == typeof(long))
                        Validarlong(DR, Iten);
                    else
                        MessageBox.Show(String.Format("Tipo '{0}' n�o implementado", Iten.Tipo));
                };
            
            if (DR["Status"].ToString() == "")
            {
                DR["OK"] = true;
                return true;
            }
            else
            {
                DR["OK"] = false;
                return false;
            }
        }


        #endregion

        private void repositoryItemComboBox1_EditValueChanged(object sender, EventArgs e)
        {
            gridView1.ValidateEditor();
            Validate();
            AjustaColunas();
        }

        private void gridView1_ValidateRow(object sender, DevExpress.XtraGrid.Views.Base.ValidateRowEventArgs e)
        {
            ValidarRow(((DataRowView)e.Row).Row);
        }

        private void gridView1_RowCellStyle(object sender, DevExpress.XtraGrid.Views.Grid.RowCellStyleEventArgs e)
        {
            if ((e.RowHandle == 0) && (e.Column.Caption != "Status"))
            {
                e.Appearance.BackColor = Color.Blue;
                e.Appearance.ForeColor = Color.White;
            };
        }

        private void gridView1_CustomRowCellEditForEditing(object sender, DevExpress.XtraGrid.Views.Grid.CustomRowCellEditEventArgs e)
        {
            if ((e.RowHandle == 0) && (e.Column.Caption != "Status"))
                e.RepositoryItem = repositoryItemComboBox1; 
        }

        private void AjustaColunas()
        {            
            foreach (ItenColunaImp Iten in conjuntoColunasImp)
            {
                Iten.Coluna = -1;
            };
            for (int i = 0; i < _DataTable.Columns.Count; i++)
                foreach (ItenColunaImp Iten in conjuntoColunasImp)
                {
                    if (Iten.Titulo == _DataTable.Rows[0].ItemArray[i].ToString())
                        Iten.Coluna = i;
                };
            
            btnConfirmar_F.Enabled = false;
            BtValida.Enabled = true;
            //ValidarDados();

        }



        /// <summary>
        /// Cria uma coluna de importa��o
        /// </summary>
        /// <param name="_Nome">Nome da coluna</param>
        /// <param name="_Titulo">T�tulo da coluna</param>
        /// <param name="_Tipo">Tipo de dado - usar typeOf()</param>        
        public void AddColunaImp(string _Nome, string _Titulo, Type _Tipo)
        {
            AddColunaImp(_Nome, _Titulo, _Tipo, false);
        }

        /// <summary>
        /// Cria uma coluna de importa��o
        /// </summary>
        /// <param name="_Nome">Nome da coluna</param>
        /// <param name="_Titulo">T�tulo da coluna</param>
        /// <param name="_Tipo">Tipo de dado - usar typeOf()</param>  
        /// <param name="ColunaObrigatoria">Campo Obrigat�rio</param>
        public void AddColunaImp(string _Nome, string _Titulo, Type _Tipo,bool ColunaObrigatoria)
        {
            AddColunaImp(_Nome, _Titulo, _Tipo, null, null,ColunaObrigatoria);
        }

        /// <summary>
        /// Cria uma coluna de importa��o
        /// </summary>
        /// <param name="_Nome">Nome da coluna</param>
        /// <param name="_Titulo">T�tulo da coluna</param>
        /// <param name="_Tipo">Tipo de dado - usar typeOf()</param>  
        /// <param name="ColunaObrigatoria">Campo Obrigat�rio</param>
        /// <param name="Coluna"></param>
        public void AddColunaImp(string _Nome, string _Titulo, Type _Tipo, bool ColunaObrigatoria,int Coluna)
        {
            AddColunaImp(_Nome, _Titulo, _Tipo, null, null, ColunaObrigatoria,Coluna);
        }

        /// <summary>
        /// Cria uma coluna de importa��o
        /// </summary>
        /// <param name="_Nome">Nome da coluna</param>
        /// <param name="_Titulo">T�tulo da coluna</param>
        /// <param name="_Tipo">Tipo de dado - usar typeOf()</param>
        /// <param name="_Maximo">Valor m�ximo</param>
        /// <param name="_Minimo">Valor m�nimi</param>
        public void AddColunaImp(string _Nome, string _Titulo, Type _Tipo, object _Maximo, object _Minimo)
        {
            AddColunaImp(_Nome, _Titulo, _Tipo, _Maximo, _Minimo, false);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="_Nome"></param>
        /// <param name="_Titulo"></param>
        /// <param name="_Tipo"></param>
        /// <param name="_Maximo"></param>
        /// <param name="_Minimo"></param>
        /// <param name="ColunaObrigatoria"></param>
        public void AddColunaImp(string _Nome, string _Titulo, Type _Tipo, object _Maximo, object _Minimo, bool ColunaObrigatoria)
        {
            AddColunaImp(_Nome, _Titulo, _Tipo, _Maximo, _Minimo, ColunaObrigatoria, -1);
        }

        /// <summary>
        /// Cria uma coluna de importa��o
        /// </summary>
        /// <param name="_Nome">Nome da coluna</param>
        /// <param name="_Titulo">T�tulo da coluna</param>
        /// <param name="_Tipo">Tipo de dado - usar typeOf()</param>
        /// <param name="_Maximo">Valor m�ximo</param>
        /// <param name="_Minimo">Valor m�nimi</param>
        /// <param name="ColunaObrigatoria">Campo Obrigat�rio</param>
        /// <param name="Coluna">Coluna default</param>
        public void AddColunaImp(string _Nome, string _Titulo, Type _Tipo, object _Maximo, object _Minimo,bool ColunaObrigatoria,int Coluna)
        {
            if (_Tipo == typeof(string))
            {
                if ((_Maximo != null) && (_Maximo.GetType() != typeof(int)))
                {
                    _Maximo = null;
                };
                if ((_Minimo != null) && (_Minimo.GetType() != typeof(int)))
                {
                    _Minimo = null;
                };
            }
            else
            {
                if ((_Maximo != null) && (_Maximo.GetType() != _Tipo))
                {
                    _Maximo = null;
                };
                if ((_Minimo != null) && (_Minimo.GetType() != _Tipo))
                {
                    _Minimo = null;
                };
            }
            conjuntoColunasImp.Add(_Nome, _Titulo, _Tipo, _Maximo, _Minimo,ColunaObrigatoria,Coluna);
            DevExpress.XtraEditors.Controls.ComboBoxItem ItemCombo = new DevExpress.XtraEditors.Controls.ComboBoxItem(_Titulo);
            repositoryItemComboBox1.Items.Add(ItemCombo);            
        }

        /// <summary>
        /// Fecha a tela
        /// </summary>
        /// <param name="Resultado"></param>
        protected override void FechaTela(DialogResult Resultado)
        {
            if ((Resultado == DialogResult.OK) && (dImpDefault != null))
                Salvar();
            base.FechaTela(Resultado);
        }

        private void BtValida_Click(object sender, EventArgs e)
        {
            ValidarDados();
        }



    }

    internal class ItenColunaImp
    {
        /// <summary>
        /// 
        /// </summary>
        public string Nome;
        /// <summary>
        /// 
        /// </summary>
        public string Titulo;
        /// <summary>
        /// 
        /// </summary>
        public Type Tipo;
        /// <summary>
        /// 
        /// </summary>
        public object Maximo;
        /// <summary>
        /// 
        /// </summary>
        public object Minimo;
        /// <summary>
        /// 
        /// </summary>
        public bool ColunaObrigatoria;
        /// <summary>
        /// 
        /// </summary>
        public int Coluna;
        /// <summary>
        /// 
        /// </summary>
        /// <param name="_Nome"></param>
        /// <param name="_Titulo"></param>
        /// <param name="_Tipo"></param>
        /// <param name="_Maximo"></param>
        /// <param name="_Minimo"></param>
        /// <param name="_ColunaObrigatoria"></param>
        /// <param name="_Coluna"></param>
        public ItenColunaImp(string _Nome, string _Titulo, Type _Tipo, object _Maximo, object _Minimo,bool _ColunaObrigatoria,int _Coluna)
        {
            Nome = _Nome;
            Titulo = _Titulo;
            Tipo = _Tipo;
            Maximo = _Maximo;
            Minimo = _Minimo;
            ColunaObrigatoria = _ColunaObrigatoria;
            Coluna = _Coluna;
        }
    }

    internal class ConjuntoColunasImp : ArrayList
    {
        /// <summary>
        /// 
        /// </summary>
        /// <param name="_Nome"></param>
        /// <param name="_Titulo"></param>
        /// <param name="_Tipo"></param>
        /// <param name="ColunaObrigatoria"></param>
        public void Add(string _Nome, string _Titulo, Type _Tipo, bool ColunaObrigatoria)
        {
            Add(_Nome, _Titulo, _Tipo, null, null, ColunaObrigatoria);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="_Nome"></param>
        /// <param name="_Titulo"></param>
        /// <param name="_Tipo"></param>
        /// <param name="_Maximo"></param>
        /// <param name="_Minimo"></param>
        /// <param name="ColunaObrigatoria"></param>
        public void Add(string _Nome, string _Titulo, Type _Tipo, object _Maximo, object _Minimo,bool ColunaObrigatoria)
        {
            Add(new ItenColunaImp(_Nome, _Titulo, _Tipo, _Maximo, _Minimo,ColunaObrigatoria,-1));
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="_Nome"></param>
        /// <param name="_Titulo"></param>
        /// <param name="_Tipo"></param>
        /// <param name="_Maximo"></param>
        /// <param name="_Minimo"></param>
        /// <param name="ColunaObrigatoria"></param>
        /// <param name="Coluna"></param>
        public void Add(string _Nome, string _Titulo, Type _Tipo, object _Maximo, object _Minimo, bool ColunaObrigatoria, int Coluna)
        {
            Add(new ItenColunaImp(_Nome, _Titulo, _Tipo, _Maximo, _Minimo, ColunaObrigatoria,Coluna));
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="NomeColuna"></param>
        /// <returns></returns>
        public ItenColunaImp GetItenColunaImp(string NomeColuna)
        {
            foreach (ItenColunaImp it in this)
                if (it.Nome == NomeColuna)
                    return it;
            return null;
        }
    }
}

