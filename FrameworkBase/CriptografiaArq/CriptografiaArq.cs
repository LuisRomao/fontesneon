﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.InteropServices;
using System.IO;

namespace CriptografiaArq
{
    /// <summary>
    /// Criptografia de arquivos - Bradesco
    /// </summary>
    public class Bradesco
    {
        /// <summary>
        /// 
        /// </summary>
        /// <param name="filepath"></param>
        /// <param name="senha"></param>
        /// <param name="chaveCripto"></param>
        /// <param name="msgErro"></param>
        /// <returns></returns>
        [DllImport("WEBTAEncoderLib.dll", CharSet = CharSet.Ansi, CallingConvention = CallingConvention.Cdecl)]
        public static extern Int32 fDecodeKeyFile([MarshalAs(UnmanagedType.LPStr)]string filepath, [MarshalAs(UnmanagedType.LPStr)]string senha, byte[] chaveCripto, [MarshalAs(UnmanagedType.LPStr)]StringBuilder msgErro);
        /// <summary>
        /// 
        /// </summary>
        /// <param name="filepath"></param>
        /// <param name="senha"></param>
        /// <param name="idCliente"></param>
        /// <param name="chaveTransferencia"></param>
        /// <param name="msgErro"></param>
        /// <returns></returns>
        [DllImport("WEBTAEncoderLib.dll", CharSet = CharSet.Ansi, CallingConvention = CallingConvention.Cdecl)]
        public static extern Int32 fDecodeKeyFileEx([MarshalAs(UnmanagedType.LPStr)]string filepath, [MarshalAs(UnmanagedType.LPStr)]string senha, [MarshalAs(UnmanagedType.LPStr)]StringBuilder idCliente, byte[] chaveTransferencia, [MarshalAs(UnmanagedType.LPStr)]StringBuilder msgErro);
        [DllImport("WEBTAEncoderLib.dll", CharSet = CharSet.Ansi, CallingConvention = CallingConvention.Cdecl)]
        private static extern IntPtr fInitEncoder([MarshalAs(UnmanagedType.LPStr)]string filename, [MarshalAs(UnmanagedType.LPStr)]string directory, byte[] chaveCripto, [MarshalAs(UnmanagedType.LPStr)]StringBuilder msgErro);
        [DllImport("WEBTAEncoderLib.dll", CharSet = CharSet.Ansi, CallingConvention = CallingConvention.Cdecl)]
        private static extern Int32 fWriteData(IntPtr handle, byte[] data, int dataLen, [MarshalAs(UnmanagedType.LPStr)]StringBuilder msgErro);
        [DllImport("WEBTAEncoderLib.dll", CharSet = CharSet.Ansi, CallingConvention = CallingConvention.Cdecl)]
        private static extern Int32 fCloseEncoder(IntPtr handle, [MarshalAs(UnmanagedType.LPStr)]StringBuilder msgErro);
        [DllImport("WEBTAEncoderLib.dll", CharSet = CharSet.Ansi, CallingConvention = CallingConvention.Cdecl)]
        private static extern IntPtr fInitDecoder([MarshalAs(UnmanagedType.LPStr)]string fileName, [MarshalAs(UnmanagedType.LPStr)]string directory, byte[] chaveCripto, [MarshalAs(UnmanagedType.LPStr)]StringBuilder msgErro);
        [DllImport("WEBTAEncoderLib.dll", CharSet = CharSet.Ansi, CallingConvention = CallingConvention.Cdecl)]
        private static extern Int32 fReadData(IntPtr handle, byte[] data, int dataLen, [MarshalAs(UnmanagedType.LPStr)]StringBuilder msgErro);
        [DllImport("WEBTAEncoderLib.dll", CharSet = CharSet.Ansi, CallingConvention = CallingConvention.Cdecl)]
        private static extern Int32 fCloseDecoder(IntPtr handle, [MarshalAs(UnmanagedType.LPStr)]StringBuilder msgErro);
        /// <summary>
        /// 
        /// </summary>
        /// <param name="bufferIn"></param>
        /// <param name="sizeIn"></param>
        /// <param name="bufferOut"></param>
        /// <param name="sizeOut"></param>
        /// <param name="key"></param>
        /// <returns></returns>
        [DllImport("WEBTAEncoderLib.dll", CharSet = CharSet.Ansi, CallingConvention = CallingConvention.Cdecl)]
        public static extern Int32 fEncodeBuffer(byte[] bufferIn, int sizeIn, byte[] bufferOut, out int sizeOut, byte[] key);
        [DllImport("WEBTAEncoderLib.dll", CharSet = CharSet.Ansi, CallingConvention = CallingConvention.Cdecl)]
        private static extern Int32 fDecodeBuffer(byte[] bufferIn, int sizeIn, byte[] bufferOut, out int sizeOut, byte[] key);
        
        /// <summary>
        /// Criptograva arquivo texto para envio ao Bradesco
        /// </summary>
        /// <param name="arquivoCripto">Arquivo com chave de criptografia</param>
        /// <param name="senhaCripto">Senha de criptografia</param>
        /// <param name="arquivo">Caminho completo do arquivo para criptografar</param>
        /// <param name="retorno">Caminho completo do arquivo criptografado de retorno</param>
        /// <returns></returns>
        public static int CriptografaArquivo(string arquivoCripto, string senhaCripto, string arquivo, ref string retorno)
        {
            byte[] chaveCripto = new byte[16];
            IntPtr criptoHandle = new IntPtr();
            byte[] bufferDados = new byte[1002];
            StringBuilder msgErro = new StringBuilder(512);

            //Decodifica chave para criptografar arquivo
            if (fDecodeKeyFile(arquivoCripto, senhaCripto, chaveCripto, msgErro) == 0)
            {
                retorno = "Erro ao decodificar Chave de Criptografia - mensagem de erro: " + msgErro;
                return 0;
            }

            //Cria handle para criptografar arquivo
            criptoHandle = fInitEncoder(Path.GetFileName(retorno), Path.GetDirectoryName(retorno), chaveCripto, msgErro);
            if (criptoHandle == null)
            {
                retorno = "Erro na iniciação da criptografia do arquivo - mensagem de erro: " + msgErro;
                return 0;
            }

            //Criptografa enquanto houver dados
            string[] linhas = System.IO.File.ReadAllLines(arquivo);
            foreach (string linha in linhas)
            {
                //Recebe linha
                bufferDados = Encoding.UTF8.GetBytes(linha + "\r\n");

                //Criptografa os dados do arquivo
                if (fWriteData(criptoHandle, bufferDados, bufferDados.Length, msgErro) == 0)
                {
                    retorno = "Erro na criptografia do arquivo - mensagem de erro: " + msgErro;
                    fCloseEncoder(criptoHandle, msgErro);
                    return 0;
                }
            }

            //Finaliza processamento encerrando o handle
            fCloseEncoder(criptoHandle, msgErro);

            //Retorna sucesso
            return 1;
        }

        /// <summary>
        /// Descriptograva arquivo recebido do Bradesco para texto
        /// </summary>
        /// <param name="arquivoCripto">Arquivo com chave de criptografia</param>
        /// <param name="senhaCripto">Senha de criptografia</param>
        /// <param name="arquivo">Caminho completo do arquivo para descriptografar</param>
        /// <param name="retorno">Caminho completo do arquivo descriptografado de retorno</param>
        /// <returns></returns>
        public static int DescriptografaArquivo(string arquivoCripto, string senhaCripto, string arquivo, ref string retorno)
        {
            byte[] chaveCripto = new byte[16];
            IntPtr criptoHandle = new IntPtr();
            byte[] bufferAux = new byte[1002];
            int bytesLidos = 1;
            StringBuilder msgErro = new StringBuilder(512);

            //Decodifica chave pafra descriptografar arquivo
            if (fDecodeKeyFile(arquivoCripto, senhaCripto, chaveCripto, msgErro) == 0)
            {
                retorno = "Erro ao decodificar Chave de Criptografia - mensagem de erro: " + msgErro;
                return 0;
            }

            //Cria handle para descriptografar arquivo
            criptoHandle = fInitDecoder(Path.GetFileName(arquivo), Path.GetDirectoryName(arquivo), chaveCripto, msgErro);
            if (criptoHandle == null)
            {
                retorno = "Erro na iniciação da descriptografia do arquivo - mensagem de erro: " + msgErro;
                return 0;
            }

            //Descriptografa enquanto houver dados
            StreamWriter wr = new StreamWriter(retorno, true);
            try
            {
                //Insere dados
                while (bytesLidos > 0)
                {
                    //Descriptografa e descomprimi os dados do arquivo
                    bytesLidos = fReadData(criptoHandle, bufferAux, bufferAux.Length, msgErro);
                    if (bytesLidos > 0)
                        wr.Write(Encoding.UTF8.GetString(bufferAux, 0, bytesLidos));
                        //wr.WriteLine(Encoding.UTF8.GetString(bufferAux, 0, bytesLidos).Replace("\r\n", ""));
                }
                wr.Close();
            }
            catch (Exception ex)
            {
                wr.Close();
                retorno = "Erro ao descriptografar o arquivo - mensagem de erro: " + ex.Message;
                return 0;
            }

            //Verifica o que determinou a saida do loop
            if (bytesLidos == 0)
            {
                retorno = "Erro na descriptografia do arquivo - mensagem de erro: " + msgErro;
                return 0;
            }
            
            //Finaliza processamento encerrando o handle
            fCloseDecoder(criptoHandle, msgErro);

            //Retorna sucesso
            return 1;
        }
    }
}
