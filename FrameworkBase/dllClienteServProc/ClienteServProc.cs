﻿using System;
using System.Collections;
using CompontesBasicos;
using System.Data;
using CompontesBasicosProc;


namespace dllClienteServProc
{
    /// <summary>
    /// Classe para clientes de servidor de processos
    /// </summary>
    public static class ClienteServProc
    {
        private static TiposServidorProc _TipoServidoSelecionado = TiposServidorProc.Indefinido;

        /// <summary>
        /// Tipo de servidor de processos
        /// </summary>
        public static TiposServidorProc TipoServidoSelecionado
        {
            get
            {
                //Console.Write("\r\n************************ TipoServidoSelecionado TipoServidoSelecionado ************************\r\n");
                if (_TipoServidoSelecionado == TiposServidorProc.Indefinido)
                {
                    if (!FuncoesBasicasProc.AmbienteDesenvolvimento)
                        _TipoServidoSelecionado = CompontesBasicosProc.FuncoesBasicasProc.EstaEmProducao ? TiposServidorProc.Producao : TiposServidorProc.QAS;
                    else
                    {
                        System.Collections.SortedList Alternativas = new SortedList();
                        Alternativas.Add(TiposServidorProc.Simulador, "Simulador");
                        Alternativas.Add(TiposServidorProc.Local, "Seridor local");
                        Alternativas.Add(TiposServidorProc.QAS, "Servidor QAS");
                        Alternativas.Add(TiposServidorProc.Producao, "(cuidado) Servidor Produção");
                        Alternativas.Add(TiposServidorProc.SemServidor, "Processo antigo");
                        object oSelecionado;
                        VirInput.Input.Execute("Tipo Sevidor de processos", Alternativas, out oSelecionado, VirInput.Formularios.cRadioCh.TipoSelecao.radio);
                        _TipoServidoSelecionado = (TiposServidorProc)oSelecionado;
                    }
                }
                return _TipoServidoSelecionado;
            }
        }

        /// <summary>
        /// Remove o Timezone dos campos datetipe para poder mandar como parâmentro serializado
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="DS"></param>
        /// <returns></returns>
        public static T RemoveTimezoneDataSet<T>(T DS)
        {
            DataSet ds = DS as DataSet;
            foreach (DataTable dt in ds.Tables)
            {
                foreach (DataColumn dc in dt.Columns)
                {

                    if (dc.DataType == typeof(DateTime))
                    {
                        dc.DateTimeMode = DataSetDateTime.Unspecified;
                    }
                }
            }
            return DS;
        }
    }
}
