using System;
using System.Collections.Generic;
using System.Text;
using System.Net;
using System.Net.Sockets;

namespace VirWeb
{
    /// <summary>
    /// Class that holds the Pack information
    /// </summary>
    public class IcmpPacket
    {
        /// <summary>
        /// 
        /// </summary>
        public Byte Type; // type of message
        /// <summary>
        /// 
        /// </summary>
        public Byte SubCode; // type of sub code
        /// <summary>
        /// 
        /// </summary>
        public UInt16 CheckSum; // ones complement checksum of struct
        /// <summary>
        /// 
        /// </summary>
        public UInt16 Identifier; // identifier
        /// <summary>
        /// 
        /// </summary>
        public UInt16 SequenceNumber; // sequence number 
        /// <summary>
        /// 
        /// </summary>
        public Byte[] Data;
    }

    /// <summary>
    /// 
    /// </summary>
    public class FTP : Object
    {

        /// <summary>
        /// 
        /// </summary>
        public NetworkCredential Credenciais;
        private WebProxy _PRX;
        /// <summary>
        /// 
        /// </summary>
        public string ErroWEB = "";

        /// <summary>
        /// 
        /// </summary>
        public FTP(string Usuario, string Senha)
        {
            
            
            _PRX = new WebProxy();
            _PRX.UseDefaultCredentials = true;
            
            Credenciais = new NetworkCredential(Usuario, Senha);
        
        }

        /// <summary>
        /// 
        /// </summary>
        public FTP(NetworkCredential Cre)
        {

            _PRX = new WebProxy();
            _PRX.UseDefaultCredentials = true;
            

            Credenciais = Cre;

        }

        private Int64 FTPTamanho(Uri _URI)
        {
        /*    Int64 retorno = -1;
            ErroWEB = "";


            FtpWebRequest _FTPRequest = ((FtpWebRequest)(WebRequest.Create(_URI)));

            _FTPRequest.Credentials = Credenciais;
            _FTPRequest.Proxy = _PRX;
            _FTPRequest.UseBinary = true;
            _FTPRequest.KeepAlive = true;
            FtpWebResponse _FTPResponse = null;

            try
            {
                //WebRequestMethods.Ftp.ListDirectory
                _FTPRequest.Method = WebRequestMethods.Ftp.GetFileSize;
                _FTPResponse = ((FtpWebResponse)(_FTPRequest.GetResponse()));
                retorno = _FTPResponse.ContentLength;
            }
            catch (System.Net.WebException e)
            {
                switch (e.Status)
                {
                    case WebExceptionStatus.CacheEntryNotFound:
                    case WebExceptionStatus.MessageLengthLimitExceeded:
                    case WebExceptionStatus.Pending:
                    case WebExceptionStatus.PipelineFailure:
                    case WebExceptionStatus.ReceiveFailure:
                    case WebExceptionStatus.RequestProhibitedByCachePolicy:
                    case WebExceptionStatus.SecureChannelFailure:
                    case WebExceptionStatus.SendFailure:
                    case WebExceptionStatus.ServerProtocolViolation:
                    case WebExceptionStatus.TrustFailure:
                        ErroWEB = "Erro na conex�o como o servidor Cooper.";
                        break;
                    case WebExceptionStatus.ConnectFailure:
                        ErroWEB = "Erro de conex�o. Verifique se o comutador com acesso � internet.";
                        break;
                    case WebExceptionStatus.ConnectionClosed:
                    case WebExceptionStatus.KeepAliveFailure:
                        ErroWEB = "Erro de conex�o perdida.";
                        break;
                    case WebExceptionStatus.NameResolutionFailure:
                        ErroWEB = "Erro na resolu��o de nome 'DNS' verifique a configura��o da rede local.";
                        break;
                    case WebExceptionStatus.ProtocolError:
                        ErroWEB = "Erro de login. Verifique o Usu�rio/Senha.";
                        break;
                    case WebExceptionStatus.ProxyNameResolutionFailure:
                        ErroWEB = "Erro com proxy. Verifique a configura��o da rede local.";
                        break;
                    case WebExceptionStatus.RequestCanceled:
                        ErroWEB = "Conex�o abortada.";
                        break;
                    case WebExceptionStatus.RequestProhibitedByProxy:
                        ErroWEB = "Erro com proxy. Verifique a configura��o da rede local.";
                        break;
                    case WebExceptionStatus.Timeout:
                        ErroWEB = "Servidor n�o responde.";
                        break;
                    case WebExceptionStatus.UnknownError:
                        ErroWEB = "Erro desconhecido.";
                        break;
                };
                ErroWEB += "(" + e.Status.ToString() + ")";
            }
            finally
            {
                if (_FTPResponse != null)
                    _FTPResponse.Close();
            };
            return retorno;
         */
            return -1;
        }

        /// <summary>
        /// 
        /// </summary>
        public string[] FTPDiretorio(Uri _URI)
        {
            
            ErroWEB = "";
            string[] retorno = null;

            FtpWebRequest _FTPRequest = ((FtpWebRequest)(WebRequest.Create(_URI)));

            _FTPRequest.Credentials = Credenciais;
            _FTPRequest.Proxy = _PRX;
            _FTPRequest.UseBinary = true;
            _FTPRequest.KeepAlive = true;
            FtpWebResponse _FTPResponse = null;

            try
            {

                _FTPRequest.Method = WebRequestMethods.Ftp.ListDirectory;
                _FTPResponse = ((FtpWebResponse)(_FTPRequest.GetResponse()));
                System.IO.StreamReader theReader = new System.IO.StreamReader(_FTPResponse.GetResponseStream());
                string manobra = theReader.ReadToEnd();
                string[] theFilesListing = manobra.Split(new string[] {Environment.NewLine}, StringSplitOptions.RemoveEmptyEntries);


                retorno = theFilesListing;
            }
            catch (System.Net.WebException e)
            {
                switch (e.Status)
                {
                    case WebExceptionStatus.CacheEntryNotFound:
                    case WebExceptionStatus.MessageLengthLimitExceeded:
                    case WebExceptionStatus.Pending:
                    case WebExceptionStatus.PipelineFailure:
                    case WebExceptionStatus.ReceiveFailure:
                    case WebExceptionStatus.RequestProhibitedByCachePolicy:
                    case WebExceptionStatus.SecureChannelFailure:
                    case WebExceptionStatus.SendFailure:
                    case WebExceptionStatus.ServerProtocolViolation:
                    case WebExceptionStatus.TrustFailure:
                        ErroWEB = "Erro na conex�o como o servidor Cooper.";
                        break;
                    case WebExceptionStatus.ConnectFailure:
                        ErroWEB = "Erro de conex�o. Verifique se o comutador est� com acesso � internet.";
                        break;
                    case WebExceptionStatus.ConnectionClosed:
                    case WebExceptionStatus.KeepAliveFailure:
                        ErroWEB = "Erro de conex�o perdida.";
                        break;
                    case WebExceptionStatus.NameResolutionFailure:
                        ErroWEB = "Erro na resolu��o de nome 'DNS' verifique a configura��o da rede local.";
                        break;
                    case WebExceptionStatus.ProtocolError:
                        ErroWEB = "Erro de login. Verifique o Usu�rio/Senha.";
                        break;
                    case WebExceptionStatus.ProxyNameResolutionFailure:
                        ErroWEB = "Erro com proxy. Verifique a configura��o da rede local.";
                        break;
                    case WebExceptionStatus.RequestCanceled:
                        ErroWEB = "Conex�o abortada.";
                        break;
                    case WebExceptionStatus.RequestProhibitedByProxy:
                        ErroWEB = "Erro com proxy. Verifique a configura��o da rede local.";
                        break;
                    case WebExceptionStatus.Timeout:
                        ErroWEB = "Servidor n�o responde.";
                        break;
                    case WebExceptionStatus.UnknownError:
                        ErroWEB = "Erro desconhecido.";
                        break;
                };
                ErroWEB += "(" + e.Status.ToString() + ")";
            }
            finally
            {
                if (_FTPResponse != null)
                    _FTPResponse.Close();
            };
            return retorno;
        }

    }

    /// <summary>
    /// 
    /// </summary>
    public class Ping : Object
    {
        const int SOCKET_ERROR = -1;
        const int ICMP_ECHO = 8;

        /// <summary>
        /// 
        /// </summary>
        public string Erro = "";
        /// <summary>
        /// 
        /// </summary>
        public int dwStop;

        static Ping ePing;
        /// <summary>
        /// 
        /// </summary>
        static public bool PingarHost(string host)
        {
            if (ePing == null)
                ePing = new Ping();
            return ePing.Pingar(host);
        }
        

        /// <summary>
        /// This Method has the algorithm to make a checksum 
        /// </summary>
        public static UInt16 checksum(UInt16[] buffer, int size)
        {
            Int32 cksum = 0;
            int counter;
            counter = 0;
            while (size > 0)
            {
                UInt16 val = buffer[counter];
                cksum += Convert.ToInt32(buffer[counter]);
                counter += 1;
                size -= 1;
            }
            cksum = (cksum >> 16) + (cksum & 0xffff);
            cksum += (cksum >> 16);
            return (UInt16)(~cksum);
        }

        /// <summary>
        /// This method get the Packet and calculates the total size 
        /// of the Pack by converting it to byte array
        /// </summary>
        public static Int32 Serialize(IcmpPacket packet, Byte[] Buffer,
        Int32 PacketSize, Int32 PingData)
        {
            Int32 cbReturn = 0;
            // serialize the struct into the array
            int Index = 0;
            Byte[] b_type = new Byte[1];
            b_type[0] = (packet.Type);
            Byte[] b_code = new Byte[1];
            b_code[0] = (packet.SubCode);
            Byte[] b_cksum = BitConverter.GetBytes(packet.CheckSum);
            Byte[] b_id = BitConverter.GetBytes(packet.Identifier);
            Byte[] b_seq = BitConverter.GetBytes(packet.SequenceNumber);
            // Console.WriteLine("Serialize type ");
            Array.Copy(b_type, 0, Buffer, Index, b_type.Length);
            Index += b_type.Length;
            // Console.WriteLine("Serialize code ");
            Array.Copy(b_code, 0, Buffer, Index, b_code.Length);
            Index += b_code.Length;
            // Console.WriteLine("Serialize cksum ");
            Array.Copy(b_cksum, 0, Buffer, Index, b_cksum.Length);
            Index += b_cksum.Length;
            // Console.WriteLine("Serialize id ");
            Array.Copy(b_id, 0, Buffer, Index, b_id.Length);
            Index += b_id.Length;
            Array.Copy(b_seq, 0, Buffer, Index, b_seq.Length);
            Index += b_seq.Length;
            // copy the data 
            Array.Copy(packet.Data, 0, Buffer, Index, PingData);
            Index += PingData;
            if (Index != PacketSize/* sizeof(IcmpPacket) */)
            {
                cbReturn = -1;
                return cbReturn;
            }
            cbReturn = Index;
            return cbReturn;
        }

        /// <summary>
        /// 
        /// </summary>
        public bool Pingar(string host)
        {
            try
            {
                Erro = "";
                //Declare the IPHostEntry 
                IPHostEntry serverHE, fromHE;
                int nBytes = 0;
                int dwStart = dwStop = 0;
                //Initilize a Socket of the Type ICMP
                Socket socket =
                new Socket(AddressFamily.InterNetwork, SocketType.Raw, ProtocolType.Icmp);
                // Get the server endpoint
                try
                {
                    serverHE = Dns.GetHostEntry(host);

                }
                catch (Exception)
                {
                    Erro = "Host not found"; // fail
                    return false;
                }
                // Convert the server IP_EndPoint to an EndPoint
                IPEndPoint ipepServer = new IPEndPoint(serverHE.AddressList[0], 0);
                EndPoint epServer = (ipepServer);
                // Set the receiving endpoint to the client machine
                fromHE = Dns.GetHostEntry(Dns.GetHostName());
                IPEndPoint ipEndPointFrom = new IPEndPoint(fromHE.AddressList[0], 0);
                EndPoint EndPointFrom = (ipEndPointFrom);
                int PacketSize = 0;
                IcmpPacket packet = new IcmpPacket();
                // Construct the packet to send
                packet.Type = ICMP_ECHO; //8
                packet.SubCode = 0;
                packet.CheckSum = UInt16.Parse("0");
                packet.Identifier = UInt16.Parse("45");
                packet.SequenceNumber = UInt16.Parse("0");
                int PingData = 32; // sizeof(IcmpPacket) - 8;
                packet.Data = new Byte[PingData];
                //Initilize the Packet.Data
                for (int i = 0; i < PingData; i++)
                {
                    packet.Data[i] = (byte)'#';
                }
                //Variable to hold the total Packet size
                PacketSize = PingData + 8;
                Byte[] icmp_pkt_buffer = new Byte[PacketSize];
                Int32 Index = 0;
                //Call a Method Serialize which counts
                //The total number of Bytes in the Packet
                Index = Serialize(packet, icmp_pkt_buffer, PacketSize, PingData);
                //Error in Packet Size
                if (Index == -1)
                {
                    Erro = "Error in Making Packet";
                    return false;
                }
                // now get this critter into a UInt16 array
                //Get the Half size of the Packet
                Double double_length = Convert.ToDouble(Index);
                Double dtemp = Math.Ceiling(double_length / 2);
                int cksum_buffer_length = Convert.ToInt32(dtemp);
                //Create a Byte Array
                UInt16[] cksum_buffer = new UInt16[cksum_buffer_length];
                //Code to initialize the Uint16 array 
                int icmp_header_buffer_index = 0;
                for (int i = 0; i < cksum_buffer_length; i++)
                {
                    cksum_buffer[i] =
                    BitConverter.ToUInt16(icmp_pkt_buffer, icmp_header_buffer_index);
                    icmp_header_buffer_index += 2;
                }
                //Call a method which will return a checksum 
                UInt16 u_cksum = checksum(cksum_buffer, cksum_buffer_length);
                //Save the checksum to the Packet
                packet.CheckSum = u_cksum;
                // Now that we have the checksum, serialize the packet again
                Byte[] sendbuf = new Byte[PacketSize];
                //again check the packet size
                Index = Serialize(packet, sendbuf, PacketSize, PingData);
                //if there is a error report it
                if (Index == -1)
                {
                    Erro = "Error in Making Packet";
                    return false;
                }
                dwStart = System.Environment.TickCount; // Start timing
                //send the Pack over the socket
                if ((nBytes = socket.SendTo(sendbuf, PacketSize, 0, epServer)) == SOCKET_ERROR)
                {
                    Erro = "Socket Error cannot Send Packet";
                    return false;
                }
                // Initialize the buffers. The receive buffer is the size of the
                // ICMP header plus the IP header (20 bytes)
                Byte[] ReceiveBuffer = new Byte[256];
                nBytes = 0;
                //Receive the bytes
                bool recd = false;
                int timeout = 0;
                //loop for checking the time of the server responding 
                while (!recd)
                {
                    nBytes = socket.ReceiveFrom(ReceiveBuffer, 256, 0, ref EndPointFrom);
                    if (nBytes == SOCKET_ERROR)
                    {
                        //Console.WriteLine("Host not Responding");
                        recd = true;
                        break;
                    }
                    else if (nBytes > 0)
                    {
                        dwStop = System.Environment.TickCount - dwStart; // stop timing

                        //Console.WriteLine("Reply from " + epServer.ToString() + " in " + dwStop + "MS :Bytes Received" + nBytes);
                        recd = true;
                        break;
                    }
                    timeout = System.Environment.TickCount - dwStart;
                    if (timeout > 1000)
                    {
                        Erro = "Time Out";
                        recd = true;
                    }
                }
                //close the socket
                socket.Close();
                return (Erro == "");
            }
            catch (Exception e) 
            {
                Erro = e.Message;
                return false;
            }
        }
         
    }
}
