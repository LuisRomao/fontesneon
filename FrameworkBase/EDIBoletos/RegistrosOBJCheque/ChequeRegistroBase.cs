﻿/*
MR - 05/01/2016 13:30            - Tipo de conta crédito diferenciado entre corrente e poupança (Alterações indicadas por *** MRC - INICIO (05/01/2016 13:30) ***)
*/

using System;
using System.Collections.Generic;
using ArquivosTXT;

namespace EDIBoletos.RegistrosOBJCheque
{
    /// <summary>
    /// 
    /// </summary>
    public class ChequeRegistroBase : RegistroBase
    {
        /// <summary>
        /// 
        /// </summary>
        public Exception UltimoErro;

        #region Propriedades tipadas

        /// <summary>
        /// 
        /// </summary>
        public int Registro;
        /// <summary>
        /// 
        /// </summary>
        public int Sequencial;

        #endregion

        /// <summary>
        /// 
        /// </summary>
        public ChequeRegistroBase()
            : base()
        {
        }

        /// <summary>
        /// 
        /// </summary>
        public ChequeRegistroBase(cTabelaTXT _cTabelaTXT, string Linha)
            : base(_cTabelaTXT)
        {
            CarregaLinha(Linha);
        }

        /// <summary>
        /// 
        /// </summary>
        public enum TiposInscricao
        {
            /// <summary>
            /// 
            /// </summary>
            CPF = 1,
            /// <summary>
            /// 
            /// </summary>
            CNPJ = 2,
            /// <summary>
            /// 
            /// </summary>
            Outros = 3
        }

        /// <summary>
        /// 
        /// </summary>
        public enum TiposProcessamento
        {
            /// <summary>
            /// 
            /// </summary>
            RastreamentoTitulos = 1,
            /// <summary>
            /// 
            /// </summary>
            ConfirmacaoAgendamento = 2,
            /// <summary>
            /// 
            /// </summary>
            ConfirmacaoPagamento = 3
        }

        /// <summary>
        /// 
        /// </summary>
        public enum TiposDocumento
        {
            /// <summary>
            /// 
            /// </summary>
            NotaFiscalFatura = 1,
            /// <summary>
            /// 
            /// </summary>
            Fatura = 2,
            /// <summary>
            /// 
            /// </summary>
            NotaFiscal = 3,
            /// <summary>
            /// 
            /// </summary>
            Duplicata = 4,
            /// <summary>
            /// 
            /// </summary>
            Outros = 5
        }

        /// <summary>
        /// 
        /// </summary>
        public enum TiposModalidadePagamento
        {
            /// <summary>
            /// 
            /// </summary>
            CreditoContaCorrentePoupanca = 1,
            /// <summary>
            /// 
            /// </summary>
            CreditoContaCorrenteRealTime = 5,
            /// <summary>
            /// 
            /// </summary>
            ChequeOrdemPagamento = 2,
            /// <summary>
            /// 
            /// </summary>
            DOCCompe = 3,
            /// <summary>
            /// 
            /// </summary>
            TED = 8,
            /// <summary>
            /// 
            /// </summary>
            RastreamentoTitulos = 30,
            /// <summary>
            /// 
            /// </summary>
            TitulosTerceiros = 31
        }

        /// <summary>
        /// 
        /// </summary>
        public enum TiposSituacaoAgendamento
        {
            /// <summary>
            /// 
            /// </summary>
            NaoPago = 1,
            /// <summary>
            /// 
            /// </summary>
            Pago = 2,
            /// <summary>
            /// 
            /// </summary>
            BaixaCobrancaSemPagamento = 5,
            /// <summary>
            /// 
            /// </summary>
            BaixaCobrancaComPagamento = 6,
            /// <summary>
            /// 
            /// </summary>
            ComInstrucaoProtesto = 7,
            /// <summary>
            /// 
            /// </summary>
            TransfCartorio = 8,
            /// <summary>
            /// 
            /// </summary>
            BaixadoPeloDesconto = 9,
            /// <summary>
            /// 
            /// </summary>
            ChequeOPEstornado = 11,
            /// <summary>
            /// 
            /// </summary>
            ChequeOPEmitido = 22
        }

        /// <summary>
        /// 
        /// </summary>
        public enum TiposMovimento
        {
            /// <summary>
            /// 
            /// </summary>
            Inclusao = 0,
            /// <summary>
            /// 
            /// </summary>
            Alteracao = 5,
            /// <summary>
            /// 
            /// </summary>
            Exclusao = 9,
            /// <summary>
            /// 
            /// </summary>
            InclusaoTituloCartorio = 1,
            /// <summary>
            /// 
            /// </summary>
            AlteracaoTitulo = 2,
            /// <summary>
            /// 
            /// </summary>
            BaixaTituloCartorio = 3
        }

        /// <summary>
        /// 
        /// </summary>
        public enum TiposCodigoMovimento
        {
            /// <summary>
            /// 
            /// </summary>
            AutorizaAgendamento = 0,
            /// <summary>
            /// 
            /// </summary>
            DesautorizaAgendamento = 25,
            /// <summary>
            /// 
            /// </summary>
            EfetuarAlegacao = 3
        }

        /// <summary>
        /// 
        /// </summary>
        public enum TiposNivelInformacaoRetorno
        {
            /// <summary>
            /// 
            /// </summary>
            InconsistenciaInvalidaArquivo = 1,
            /// <summary>
            /// 
            /// </summary>
            InconsistenciaInvalidaRegistro = 2,
            /// <summary>
            /// 
            /// </summary>
            ConsistenciaTarefaExecutada = 3
        }

        /// <summary>
        /// 
        /// </summary>     
        public enum TiposContaFornecedor
        {
            /// <summary>
            /// 
            /// </summary>
            ContaCorrente = 1,
            /// <summary>
            /// 
            /// </summary>
            ContaPoupanca = 2
        }

        //*** MRC - INICIO (05/01/2016 13:30) ***
        /// <summary>
        /// 
        /// </summary>     
        public enum TiposFinalidadeDOCTED
        {
            /// <summary>
            /// 
            /// </summary>
            CreditoContaCorrente = 1,
            /// <summary>
            /// 
            /// </summary>
            CreditoContaPoupanca = 11
        }
        //*** MRC - TERMINO (05/01/2016 13:30) ***

        /// <summary>
        /// 
        /// </summary>
        protected override void RegistraMapa()
        {
            RegistraFortementeTipado("Registro", 1, 1);
            RegistraFortementeTipado("Sequencial", 495, 6);
        }
    }

    /// <summary>
    /// 
    /// </summary>
    public class regHeader_0 : ChequeRegistroBase
    {
        #region Variaveis Tipadas

        /// <summary>
        /// 
        /// </summary>
        public int CodComunicacao;

        private string _NomeEmpresa;
        /// <summary>
        /// 
        /// </summary>
        public string NomeEmpresa
        {
            get { return (_NomeEmpresa == null) ? "" : _NomeEmpresa.ToUpper(); }
            set { _NomeEmpresa = value; }
        }

        /// <summary>
        /// 
        /// </summary>
        public int TipoServico;
        /// <summary>
        /// 
        /// </summary>
        public int CodOrigemArq;
        /// <summary>
        /// 
        /// </summary>
        public int NumRetorno;
        /// <summary>
        /// 
        /// </summary>
        public String DataGeracao;
        /// <summary>
        /// 
        /// </summary>
        public String HoraGeracao;
        /// <summary>
        /// 
        /// </summary>
        public int TipoProcessamento;

        private string _UsoEmpresa;
        /// <summary>
        /// 
        /// </summary>
        public string UsoEmpresa
        {
            get { return (_UsoEmpresa == null) ? "" : _UsoEmpresa.ToUpper(); }
            set { _UsoEmpresa = value; }
        }

        /// <summary>
        /// 
        /// </summary>
        public string NumListaDebito;
        //public int Sequencial;

        #endregion

        /// <summary>
        /// 
        /// </summary>
        public List<regTransacao_1> Transacoes;
        /// <summary>
        /// 
        /// </summary>
        public regTrailer_9 Trailer;

        /// <summary>
        /// 
        /// </summary>
        public regHeader_0(cTabelaTXT _cTabelaTXT, string Linha)
            : base(_cTabelaTXT, Linha)
        {
            Registro = 0;
            TipoServico = 20;
            Transacoes = new List<regTransacao_1>();
        }

        /// <summary>
        /// 
        /// </summary>
        public bool GravaArquivo(string NomeArquivo)
        {
            bool resultado = false;
            try
            {
                cTabTXT.IniciaGravacao(NomeArquivo);
                GravaLinha();
                if (Transacoes != null)
                    foreach (regTransacao_1 trans in Transacoes)
                        trans.GravaLinha();
                if (Trailer != null)
                    Trailer.GravaLinha();
                resultado = true;
            }
            catch (Exception ex)
            {
                throw new ArgumentException(ex.Message, ex.InnerException);
            }
            finally
            {
                cTabTXT.TerminaGravacao();
            }
            return resultado;
        }

        /// <summary>
        /// 
        /// </summary>
        protected override void RegistraMapa()
        {
            base.RegistraMapa();
            RegistraFortementeTipado("CodComunicacao", 2, 8);
            RegistraFortementeTipado("NomeEmpresa", 26, 40);
            RegistraFortementeTipado("TipoServico", 66, 2);
            RegistraFortementeTipado("CodOrigemArq", 68, 1);
            RegistraFortementeTipado("NumRetorno", 74, 5);
            RegistraFortementeTipado("DataGeracao", 79, 8);
            RegistraFortementeTipado("HoraGeracao", 87, 6);
            RegistraFortementeTipado("TipoProcessamento", 106, 1);
            RegistraFortementeTipado("UsoEmpresa", 107, 74);
            RegistraFortementeTipado("NumListaDebito", 478, 9);
        }
    }

    /// <summary>
    /// 
    /// </summary>
    public class regTransacao_1 : ChequeRegistroBase
    {
        #region Variaveis Tipadas

        private string _NomeForn;
        /// <summary>
        /// 
        /// </summary>
        public string NomeForn
        {
            get { return (_NomeForn == null) ? "" : _NomeForn.ToUpper(); }
            set { _NomeForn = value; }
        }

        private string _EnderecoForn;
        /// <summary>
        /// 
        /// </summary>
        public string EnderecoForn
        {
            get { return (_EnderecoForn == null) ? "" : _EnderecoForn.ToUpper(); }
            set { _EnderecoForn = value; }
        }

        private string _CepForn;
        /// <summary>
        /// 
        /// </summary>
        public string CepForn
        {
            get
            {
                long lngCep = 0;
                if (_CepForn != null) { _CepForn = _CepForn.Replace("-", ""); }
                return (long.TryParse(_CepForn, out lngCep)) ? _CepForn.PadLeft(8, '0') : "00000000";
            }
            set { _CepForn = value; }
        }

        /// <summary>
        /// 
        /// </summary>
        public int NumeroPagamento;
        /// <summary>
        /// 
        /// </summary>
        public string SeuNumero;
        /// <summary>
        /// 
        /// </summary>
        public decimal Desconto;
        /// <summary>
        /// 
        /// </summary>
        public decimal Acrescimo;

        private int _TipoDocumento;
        /// <summary>
        /// 
        /// </summary>
        public int TipoDocumento
        {
            get { return (_TipoDocumento == 0) ? (int)ChequeRegistroBase.TiposDocumento.Outros : _TipoDocumento; }
            set { _TipoDocumento = value; }
        }

        /// <summary>
        /// 
        /// </summary>
        public int NumDocumento;
        /// <summary>
        /// 
        /// </summary>
        public string SerieDocumento;
        /// <summary>
        /// 
        /// </summary>
        public int ModalidadePagamento;
        /// <summary>
        /// 
        /// </summary>
        public int SituacaoAgendamento;
        /// <summary>
        /// 
        /// </summary>
        public int TipoMovimento;
        /// <summary>
        /// 
        /// </summary>
        public int CodMovimento;
        /// <summary>
        /// 
        /// </summary>
        public string HorarioConsultaSaldo;

        private string _SacAvalista;
        /// <summary>
        /// 
        /// </summary>
        public string SacAvalista
        {
            get { return (_SacAvalista == null) ? "" : _SacAvalista.ToUpper(); }
            set { _SacAvalista = value; }
        }

        /// <summary>
        /// 
        /// </summary>
        public int CodAreaEmpresa;

        private string _UsoEmpresa;
        /// <summary>
        /// 
        /// </summary>
        public string UsoEmpresa
        {
            get { return (_UsoEmpresa == null) ? "" : _UsoEmpresa.ToUpper(); }
            set { _UsoEmpresa = value; }
        }

        /// <summary>
        /// 
        /// </summary>
        public int CodLancamento;
        /// <summary>
        /// 
        /// </summary>
        public int TipoContaForn;
        /// <summary>
        /// 
        /// </summary>
        public int ContaComplementar;

        #endregion

        /// <summary>
        /// 
        /// </summary>
        public regTransacao_1(cTabelaTXT _cTabelaTXT, string Linha)
            : base(_cTabelaTXT, Linha)
        {
            Registro = 1;
        }

        /// <summary>
        /// 
        /// </summary>
        protected override void RegistraMapa()
        {
            base.RegistraMapa();
            RegistraFortementeTipado("NomeForn", 18, 30);
            RegistraFortementeTipado("EnderecoForn", 48, 40);
            RegistraFortementeTipado("CepForn", 88, 8);
            RegistraFortementeTipado("NumeroPagamento", 120, 16);
            RegistraFortementeTipado("SeuNumero", 151, 15);
            RegistraFortementeTipado("Desconto", 220, 15);
            RegistraFortementeTipado("Acrescimo", 235, 15);
            RegistraFortementeTipado("TipoDocumento", 250, 2);
            RegistraFortementeTipado("NumDocumento", 252, 10);
            RegistraFortementeTipado("SerieDocumento", 262, 2);
            RegistraFortementeTipado("ModalidadePagamento", 264, 2);
            RegistraFortementeTipado("SituacaoAgendamento", 277, 2);
            RegistraFortementeTipado("TipoMovimento", 289, 1);
            RegistraFortementeTipado("CodMovimento", 290, 2);
            RegistraFortementeTipado("HorarioConsultaSaldo", 292, 4);
            RegistraFortementeTipado("SacAvalista", 332, 40);
            RegistraFortementeTipado("CodAreaEmpresa", 414, 2);
            RegistraFortementeTipado("UsoEmpresa", 416, 35);
            RegistraFortementeTipado("CodLancamento", 473, 5);
            RegistraFortementeTipado("TipoContaForn", 479, 1);
            RegistraFortementeTipado("ContaComplementar", 480, 7);
        }
    }

    /// <summary>
    /// 
    /// </summary>
    public class regTrailer_9 : ChequeRegistroBase
    {
        #region Variaveis Tipadas

        /// <summary>
        /// 
        /// </summary>
        public int QTDRegistros;
        /// <summary>
        /// 
        /// </summary>
        public decimal TotalValorPago;

        #endregion

        /// <summary>
        /// 
        /// </summary>
        public regTrailer_9(cTabelaTXT _cTabelaTXT, string Linha)
            : base(_cTabelaTXT, Linha)
        {
            Registro = 9;
        }

        /// <summary>
        /// 
        /// </summary>
        protected override void RegistraMapa()
        {
            base.RegistraMapa();
            RegistraFortementeTipado("QTDRegistros", 2, 6);
            RegistraFortementeTipado("TotalValorPago", 8, 17);
        }
    }
}