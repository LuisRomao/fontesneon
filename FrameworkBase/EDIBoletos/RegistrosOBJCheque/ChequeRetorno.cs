﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Collections;
using ArquivosTXT;
using DocBacarios;
using Abstratos;
using EDIBoletos.RegistrosOBJCheque;

namespace EDIBoletos
{
   /// <summary>
   /// Objeto Cheque (derivado do ABS_Cheque) usado para tratamento de retorno de arquivo
   /// </summary>
   public class ChequeRetorno: ABS_Cheque 
   {
   }
}
