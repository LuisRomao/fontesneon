﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Collections;
using ArquivosTXT;
using DocBacarios;
using Abstratos;


namespace EDIBoletos.RegistrosOBJCheque
{
    /// <summary>
    /// 
    /// </summary>
   public abstract class ChequeRemessaBase: ChequeRegistroBase 
   {
      //public Exception UltimoErro;
       /// <summary>
       /// 
       /// </summary>
      public ColCheques Cheques;
      /// <summary>
      /// 
      /// </summary>
      public cTabelaTXT ArquivoSaida;

      /// <summary>
      /// 
      /// </summary>
      public int CodComunicacao;

      /// <summary>
      /// 
      /// </summary>
      public int ContaDebito;

      /// <summary>
      /// 
      /// </summary>
      public string NomeEmpresa;

      /// <summary>
      /// 
      /// </summary>
      public CPFCNPJ CPFCNPJ;

      /// <summary>
      /// 
      /// </summary>
      public string UsoEmpresa;

      /// <summary>
      /// 
      /// </summary>
      public int SequencialRemessa;

      /// <summary>
      /// 
      /// </summary>
      public int SequencialRemessaArquivo;

      /// <summary>
      /// 
      /// </summary>
      public abstract void IniciaRemessa();

      /// <summary>
      /// 
      /// </summary>
      public abstract string GravaArquivo(string Path);
   }

   /// <summary>
   /// 
   /// </summary>
   public class ColCheques : CollectionBase
   {
       /// <summary>
       /// 
       /// </summary>
      public ABS_Cheque this[int index]
      {
         get
         {
             return ((ABS_Cheque)List[index]);
         }
         set
         {
            List[index] = value;
         }
      }

      /// <summary>
      /// 
      /// </summary>
      public int Add(ABS_Cheque value)
      {
         return (List.Add(value));
      }

      /// <summary>
      /// 
      /// </summary>
      public int IndexOf(ABS_Cheque value)
      {
         return (List.IndexOf(value));
      }

      /// <summary>
      /// 
      /// </summary>
      public void Insert(int index, ABS_Cheque value)
      {
         List.Insert(index, value);
      }

      /// <summary>
      /// 
      /// </summary>
      public void Remove(ABS_Cheque value)
      {
         List.Remove(value);
      }

      /// <summary>
      /// 
      /// </summary>
      public bool Contains(ABS_Cheque value)
      {
         // If value is not of type Int16, this will return false.
         return (List.Contains(value));
      }
   }
}
