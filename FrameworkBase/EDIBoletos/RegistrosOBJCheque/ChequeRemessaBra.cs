﻿/* 
MR - 28/04/2014 09:45 13.2.8.35 - Retirada de calculo automatico de Digitos de Agencia e Conta (Alterações indicadas por *** MRC - INICIO - PAG-FOR (28/04/2014 09:45) ***)
MR - 13/06/2014 11:00           - Envio de Data de Emissão que antes era opcional (Alterações indicadas por *** MRC - INICIO - PAG-FOR (13/06/2014 11:00) ***)
MR - 05/01/2016 13:30           - Tipo de conta crédito diferenciado entre corrente e poupança (Alterações indicadas por *** MRC - INICIO (05/01/2016 13:30) ***)
*/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Collections;
using ArquivosTXT;
using BoletosCalc;
using DocBacarios;
using EDIBoletos.RegistrosOBJCheque;
using Abstratos;

namespace EDIBoletos
{
    /// <summary>
    /// 
    /// </summary>
    public class ChequeRemessaBra : ChequeRemessaBase
    {
        private regHeader_0_1_PAGFOR_BRA Header;
        private regTrailer_9_1_PAGFOR_BRA Trailer;
        private decimal TotalValorPagamento;

        /// <summary>
        /// 
        /// </summary>
        public override void IniciaRemessa()
        {
            Cheques = new ColCheques();
            ArquivoSaida = new cTabelaTXT(ModelosTXT.layout);
            ArquivoSaida.Formatodata = FormatoData.ddMMyyyy;
        }

        /// <summary>
        /// 
        /// </summary>
        public void InsereCheques()
        {
            TotalValorPagamento = 0;
            foreach (ABS_Cheque cheque in Cheques)
            {
                regTransacao_1_1_PAGFOR_BRA Cheque = new regTransacao_1_1_PAGFOR_BRA(Header);
                Cheque.CPFCNPJForn = cheque.CPFCNPJ_Credito;
                Cheque.NomeForn = cheque.Nome;
                Cheque.EnderecoForn = cheque.Endereco_Credito;
                Cheque.CepForn = cheque.CEP;
                Cheque.InstrucaoChequeOP = cheque.InstrucaoChequeOP;
                Cheque.FinalidadeDOCTED = cheque.FinalidadeDOCTED;
                Cheque.TipoContaDOCTED = cheque.TipoContaDOCTED;
                Cheque.LinhaDigitavel = cheque.LinhaDigitavel;
                Cheque.BancoForn = cheque.Banco_Credito;
                Cheque.AgenciaForn = cheque.Agencia_Credito;
                Cheque.DigAgenciaForn = cheque.DigAgencia_Credito;
                Cheque.ContaForn = cheque.Conta_Credito;
                Cheque.DigContaForn = cheque.DigConta_Credito;
                Cheque.NumeroPagamento = cheque.NumeroPagamento;
                Cheque.Carteira = cheque.Carteira;
                Cheque.NossoNumero = cheque.NossoNumero;
                Cheque.SeuNumero = cheque.SeuNumero;
                Cheque.DataVencimento = cheque.DataVencimento;                
                Cheque.DataEmissao = cheque.DataEmissao;                
                Cheque.FatorVencimento = cheque.FatorVencimento;
                if (cheque.LinhaDigitavel == null || cheque.LinhaDigitavel == "") Cheque.Valor = cheque.Valor;
                else Cheque.Valor = Convert.ToDecimal(cheque.LinhaDigitavel.Substring(cheque.LinhaDigitavel.Length - 10)) / 100;
                Cheque.Desconto = cheque.Desconto;
                Cheque.Acrescimo = cheque.Acrescimo;
                Cheque.TipoDocumento = cheque.TipoDocumento;
                Cheque.NumDocumento = cheque.NumDocumento;
                Cheque.SerieDocumento = cheque.SerieDocumento;
                Cheque.ModalidadePagamento = cheque.ModalidadePagamento;
                Cheque.SituacaoAgendamento = cheque.SituacaoAgendamento;
                Cheque.TipoMovimento = cheque.TipoMovimento;
                Cheque.CodMovimento = cheque.CodMovimento;
                Cheque.HorarioConsultaSaldo = cheque.HorarioConsultaSaldo;
                Cheque.CodAreaEmpresa = cheque.CodAreaEmpresa;
                Cheque.UsoEmpresa = cheque.UsoEmpresa;
                Cheque.CodLancamento = cheque.CodLancamento;
                Cheque.TipoContaForn = cheque.TipoConta_Credito;
                Cheque.ContaComplementar = ContaDebito;
                TotalValorPagamento = TotalValorPagamento + (Cheque.Valor - Cheque.Desconto + Cheque.Acrescimo);
            }
        }

        private void CriaHeader()
        {
            Header = new regHeader_0_1_PAGFOR_BRA(ArquivoSaida);
            Header.CodComunicacao = CodComunicacao;
            Header.CPFCNPJ = CPFCNPJ;
            Header.NomeEmpresa = NomeEmpresa;
            Header.SequencialRem = SequencialRemessa;
            Header.UsoEmpresa = UsoEmpresa;
        }

        private void CriaTrailer()
        {
            Trailer = new regTrailer_9_1_PAGFOR_BRA(Header, TotalValorPagamento);
        }

        /// <summary>
        /// 
        /// </summary>
        public override string GravaArquivo(string Path)
        {
            try
            {
                string NomeArquivo = Path + "PG" + DateTime.Today.ToString("ddMMyyyy_") + SequencialRemessaArquivo.ToString("00") + ".REM";
                CriaHeader();
                if (Header == null)
                    return "";
                else
                {
                    InsereCheques();
                    CriaTrailer();
                    if (Header.GravaArquivo(NomeArquivo))
                        return NomeArquivo;
                    else
                        return "";
                }
            }
            catch (Exception ex)
            {
                UltimoErro = ex;
                return "";
            }
        }
    }

    /// <summary>
    /// 
    /// </summary>
    public class regHeader_0_1_PAGFOR_BRA : regHeader_0
    {
        #region Variaveis Tipadas

        private CPFCNPJ _CPFCNPJ;
        /// <summary>
        /// 
        /// </summary>
        public CPFCNPJ CPFCNPJ
        {
            get { return _CPFCNPJ; }
            set { _CPFCNPJ = value; }
        }

        /// <summary>
        /// 
        /// </summary>
        public int TipoInscricao
        {
            get
            {
                if (CPFCNPJ == null) { return (int)ChequeRemessaBra.TiposInscricao.Outros; }
                else
                {
                    switch (CPFCNPJ.Tipo)
                    {
                        case TipoCpfCnpj.CNPJ: return (int)ChequeRemessaBra.TiposInscricao.CNPJ;
                        case TipoCpfCnpj.CPF: return (int)ChequeRemessaBra.TiposInscricao.CPF;
                        case TipoCpfCnpj.INVALIDO: return (int)ChequeRemessaBra.TiposInscricao.Outros;
                        default: return (int)ChequeRemessaBra.TiposInscricao.Outros;
                    }
                }
            }
        }

        /// <summary>
        /// 
        /// </summary>
        public Int64 NumInscricao
        {
            get
            {
                if (CPFCNPJ == null) { return 0; }
                else { return CPFCNPJ.Tipo == TipoCpfCnpj.INVALIDO ? 0 : CPFCNPJ.Valor; }
            }
        }

        /// <summary>
        /// 
        /// </summary>
        public int SequencialRem;

        #endregion

        /// <summary>
        /// 
        /// </summary>
        public regHeader_0_1_PAGFOR_BRA(cTabelaTXT _cTabelaTXT)
            : base(_cTabelaTXT, null)
        {
            CodOrigemArq = 1;
            DataGeracao = DateTime.Now.ToString("yyyyMMdd");
            HoraGeracao = DateTime.Now.ToString("HHmmss");
            Sequencial = 1;
        }

        /// <summary>
        /// 
        /// </summary>
        protected override void RegistraMapa()
        {
            base.RegistraMapa();
            RegistraFortementeTipado("TipoInscricao", 10, 1);
            RegistraFortementeTipado("NumInscricao", 11, 15);
            RegistraFortementeTipado("SequencialRem", 69, 5);
        }
    }

    /// <summary>
    /// 
    /// </summary>
    public class regTransacao_1_1_PAGFOR_BRA : regTransacao_1
    {
        #region Variaveis Tipadas

        private CPFCNPJ CPFCNPJPag;

        private CPFCNPJ _CPFCNPJForn;
        /// <summary>
        /// 
        /// </summary>
        public CPFCNPJ CPFCNPJForn
        {
            get { return _CPFCNPJForn; }
            set { _CPFCNPJForn = value; }
        }

        /// <summary>
        /// 
        /// </summary>
        public int TipoInscricaoForn
        {
            get
            {
                if (CPFCNPJForn == null || CPFCNPJForn.Valor == 0) { return (int)ChequeRemessaBra.TiposInscricao.Outros; }
                else
                {
                    switch (CPFCNPJForn.Tipo)
                    {
                        case TipoCpfCnpj.CNPJ: return (int)ChequeRemessaBra.TiposInscricao.CNPJ;
                        case TipoCpfCnpj.CPF: return (int)ChequeRemessaBra.TiposInscricao.CPF;
                        case TipoCpfCnpj.INVALIDO: return (int)ChequeRemessaBra.TiposInscricao.Outros;
                        default: return (int)ChequeRemessaBra.TiposInscricao.Outros;
                    }
                }
            }
        }

        /// <summary>
        /// 
        /// </summary>
        public Int64 NumInscricaoForn
        {
            get
            {
                if (CPFCNPJForn == null || CPFCNPJForn.Valor == 0) { return 1; }
                else
                {
                    switch (CPFCNPJForn.Tipo)
                    {
                        case TipoCpfCnpj.CPF: return Convert.ToInt64(CPFCNPJForn.Valor.ToString().Substring(0, CPFCNPJForn.Valor.ToString().Length - 2) + "0000" + CPFCNPJForn.Valor.ToString().Substring(CPFCNPJForn.Valor.ToString().Length - 2));
                        default: return CPFCNPJForn.Valor;
                    }
                }
            }
        }

        /// <summary>
        /// 
        /// </summary>
        public string LinhaDigitavel;
        /// <summary>
        /// 
        /// </summary>
        public string InstrucaoChequeOP;
        /// <summary>
        /// 
        /// </summary>
        public int FinalidadeDOCTED;
        /// <summary>
        /// 
        /// </summary>
        public int TipoContaDOCTED;

        /// <summary>
        /// 
        /// </summary>
        public int BancoForn;
        private int _BcoForn;
        /// <summary>
        /// 
        /// </summary>
        public int BcoForn
        {
            get
            {
                if (ModalidadePagamento == 1 || ModalidadePagamento == 2 || ModalidadePagamento == 5 || ModalidadePagamento == 30) { return 237; }
                else { return BancoForn; }
            }
            set { _BcoForn = value; }
        }

        /// <summary>
        /// 
        /// </summary>
        public int AgenciaForn;
        //*** MRC - INICIO - PAG-FOR (28/04/2014 09:45) ***
        /// <summary>
        /// 
        /// </summary>
        public string DigAgenciaForn;
        //private string _DigAgenciaForn;
        //public string DigAgenciaForn
        //{
        //   get { return (ModalidadePagamento != 3 && ModalidadePagamento != 8) ? (AgenciaForn == 0 ? "0" : BoletoCalc.DigitoBradesco(AgenciaForn.ToString().PadLeft(11, '0'))) : _DigAgenciaForn; }
        //   set { _DigAgenciaForn = value; }
        //}
        //*** MRC - TERMINO - PAG-FOR (28/04/2014 09:45) ***

        /// <summary>
        /// 
        /// </summary>
        public int ContaForn;
        //*** MRC - INICIO - PAG-FOR (28/04/2014 09:45) ***
        /// <summary>
        /// 
        /// </summary>
        public string DigContaForn;
        //private string _DigContaForn;
        //public string DigContaForn
        //{
        //   get { return (ModalidadePagamento != 3 && ModalidadePagamento != 8) ? (ContaForn == 0 ? "0" : BoletoCalc.DigitoBradesco(ContaForn.ToString().PadLeft(11, '0'))) : _DigContaForn; }
        //   set { _DigContaForn = value; }
        //}
        //*** MRC - INICIO - PAG-FOR (28/04/2014 09:45) ***

        /// <summary>
        /// 
        /// </summary>
        public int Carteira;
        /// <summary>
        /// 
        /// </summary>
        public long NossoNumero;

        /// <summary>
        /// 
        /// </summary>
        public DateTime DataVencimento;
        private string _DtVencimento;
        /// <summary>
        /// 
        /// </summary>
        public string DtVencimento
        {
            get
            {
                return ((DataVencimento.Year == 1) || (_DtVencimento == "")) ? "00000000" : DataVencimento.Year.ToString("0000") + DataVencimento.Month.ToString("00") + DataVencimento.Day.ToString("00");
            }
            set { _DtVencimento = value; }
        }

        //*** MRC - INICIO - PAG-FOR (13/06/2014 11:00) ***
        /// <summary>
        /// 
        /// </summary>
        public DateTime DataEmissao;
        private string _DtEmissao;
        /// <summary>
        /// 
        /// </summary>
        public string DtEmissao
        {
            get
            {
                return ((DataEmissao.Year == 1) || (_DtEmissao == "")) ? "00000000" : DataEmissao.Year.ToString("0000") + DataEmissao.Month.ToString("00") + DataEmissao.Day.ToString("00"); ;
            }
            set { _DtEmissao = value; }
        }
        //*** MRC - TERMINO - PAG-FOR (13/06/2014 11:00) ***

        private string _DtLimDesconto;
        /// <summary>
        /// 
        /// </summary>
        public string DtLimDesconto
        {
            get
            {
                return (Desconto == 0) ? "00000000" : ((DataVencimento.Year == 1) || (_DtVencimento == "")) ? "00000000" : DataVencimento.Year.ToString("0000") + DataVencimento.Month.ToString("00") + DataVencimento.Day.ToString("00");
            }
            set { _DtLimDesconto = value; }
        }

        private string _DtEfetivaPagamento;
        /// <summary>
        /// 
        /// </summary>
        public string DtEfetivaPagamento
        {
            get
            {
                return ((DataVencimento.Year == 1) || (_DtVencimento == "")) ? "00000000" : DataVencimento.Year.ToString("0000") + DataVencimento.Month.ToString("00") + DataVencimento.Day.ToString("00");
            }
            set { _DtEfetivaPagamento = value; }
        }

        /// <summary>
        /// 
        /// </summary>
        public int FatorVencimento;
        /// <summary>
        /// 
        /// </summary>
        public decimal Valor;

        private decimal _ValorPagamento;
        /// <summary>
        /// 
        /// </summary>
        public decimal ValorPagamento
        {
            get
            {
                return Valor - Desconto + Acrescimo;
            }
            set { _ValorPagamento = value; }
        }

        private string _InfoComplementar;
        /// <summary>
        /// 
        /// </summary>
        public string InfoComplementar
        {
            get
            {
                string Aux;
                string CampoLivre;
                string CodigoMoeda;
                string DigVerCodBarras;
                DateTime DataRef = new DateTime(1997, 10, 7);
                switch (ModalidadePagamento)
                {
                    case (int)TiposModalidadePagamento.ChequeOrdemPagamento: return InstrucaoChequeOP;
                    case (int)TiposModalidadePagamento.DOCCompe:
                    case (int)TiposModalidadePagamento.TED:
                        {
                            if (CPFCNPJForn.Valor == CPFCNPJPag.Valor) { Aux = "D"; } else { Aux = "C"; }
                            //*** MRC - INICIO (05/01/2016 13:30) ***
                            return Aux + "000000" + ((FinalidadeDOCTED == 0) ? Convert.ToInt32(TiposFinalidadeDOCTED.CreditoContaCorrente).ToString("00") : FinalidadeDOCTED.ToString("00")) + ((TipoContaDOCTED == 0) ? Convert.ToInt32(TiposContaFornecedor.ContaCorrente).ToString("00") : TipoContaDOCTED.ToString("00"));
                            //*** MRC - TERMINO (05/01/2016 13:30) ***
                        }
                    case (int)TiposModalidadePagamento.TitulosTerceiros:
                        {
                            Aux = LinhaDigitavel.Replace(".", "").Replace(" ", "");
                            CampoLivre = Aux.Substring(4, 5) + Aux.Substring(10, 10) + Aux.Substring(21, 10);
                            CodigoMoeda = Aux.Substring(3, 1);
                            DigVerCodBarras = Aux.Substring(32, 1);
                            BancoForn = Convert.ToInt32(Aux.Substring(0, 3));
                            FatorVencimento = Convert.ToInt32(Aux.Substring(33, 4));
                            DataVencimento = DataRef.AddDays(FatorVencimento);
                            Valor = Convert.ToDecimal(Aux.Substring(37, 10)) / 100;
                            if (BancoForn == 237)
                            {
                                AgenciaForn = Convert.ToInt32(Aux.Substring(4, 4));
                                //*** MRC - INICIO - PAG-FOR (28/04/2014 09:45) ***
                                DigAgenciaForn = BoletoCalc.DigitoBradesco(AgenciaForn.ToString().PadLeft(11, '0'));
                                //*** MRC - TERMINO - PAG-FOR (28/04/2014 09:45) ***
                                Carteira = Convert.ToInt32(Aux.Substring(8, 1) + Aux.Substring(10, 1));
                                NossoNumero = Convert.ToInt64(Aux.Substring(11, 9) + Aux.Substring(21, 2));
                                ContaForn = Convert.ToInt32(Aux.Substring(23, 7));
                                //*** MRC - INICIO - PAG-FOR (28/04/2014 09:45) ***
                                DigContaForn = BoletoCalc.DigitoBradesco(ContaForn.ToString().PadLeft(11, '0'));
                                //*** MRC - TERMINO - PAG-FOR (28/04/2014 09:45) ***
                            }
                            //*** MRC - INICIO - PAG-FOR (28/04/2014 09:45) ***
                            else
                            {
                                DigAgenciaForn = "0";
                                DigContaForn = "0";
                            }
                            //*** MRC - TERMINO - PAG-FOR (28/04/2014 09:45) ***
                            return CampoLivre + DigVerCodBarras + CodigoMoeda;
                        }
                    default: return "";
                }
            }
            set { _InfoComplementar = value; }
        }

        #endregion

        private regHeader_0_1_PAGFOR_BRA regPai;

        /// <summary>
        /// 
        /// </summary>
        public regTransacao_1_1_PAGFOR_BRA(regHeader_0_1_PAGFOR_BRA _regPai)
            : base(_regPai.cTabTXT, null)
        {
            regPai = _regPai;
            regPai.Transacoes.Add(this);

            SituacaoAgendamento = (int)ChequeRemessaBra.TiposSituacaoAgendamento.NaoPago;
            CPFCNPJPag = regPai.CPFCNPJ;

            Sequencial = regPai.Transacoes.Count + 1;
        }

        /// <summary>
        /// 
        /// </summary>
        protected override void RegistraMapa()
        {
            base.RegistraMapa();
            RegistraFortementeTipado("InfoComplementar", 374, 40);
            RegistraFortementeTipado("TipoInscricaoForn", 2, 1);
            RegistraFortementeTipado("NumInscricaoForn", 3, 15);
            RegistraFortementeTipado("BcoForn", 96, 3);
            RegistraFortementeTipado("AgenciaForn", 99, 5);
            RegistraFortementeTipado("DigAgenciaForn", 104, 1);
            RegistraFortementeTipado("ContaForn", 105, 13);
            RegistraFortementeTipado("DigContaForn", 118, 2);
            RegistraFortementeTipado("Carteira", 136, 3);
            RegistraFortementeTipado("NossoNumero", 139, 12);
            RegistraFortementeTipado("DtVencimento", 166, 8);
            RegistraFortementeTipado("DtEmissao", 174, 8);
            RegistraFortementeTipado("DtLimDesconto", 182, 8);
            RegistraMapa("Zero", 190, 1, 0);
            RegistraFortementeTipado("FatorVencimento", 191, 4);
            RegistraFortementeTipado("Valor", 195, 10);
            RegistraFortementeTipado("ValorPagamento", 205, 15);
            RegistraFortementeTipado("DtEfetivaPagamento", 266, 8);
        }
    }

    /// <summary>
    /// 
    /// </summary>
    public class regTrailer_9_1_PAGFOR_BRA : regTrailer_9
    {
        private regHeader_0_1_PAGFOR_BRA regPai;

        /// <summary>
        /// 
        /// </summary>
        public regTrailer_9_1_PAGFOR_BRA(regHeader_0_1_PAGFOR_BRA _regPai, Decimal TotalValorPagamento)
            : base(_regPai.cTabTXT, null)
        {
            regPai = _regPai;
            regPai.Trailer = this;
            Sequencial = regPai.Transacoes.Count + 2;
            QTDRegistros = Sequencial;
            TotalValorPago = TotalValorPagamento;
        }
    }
}
