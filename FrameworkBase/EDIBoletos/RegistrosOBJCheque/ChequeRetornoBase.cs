/*
MR - 16/04/2014 14:30             - Carregamento de arquivo com um ou mais blocos header/transacao/trailer (Altera��es indicadas por *** MRC - INICIO - PAG-FOR (16/04/2014 14:30) ***)
MR - 15/05/2014 15:00 - 13.2.8.50 - Carregamento de arquivo com tratamento para mais de um tipo de layout header/transacao/trailer (Altera��es indicadas por *** MRC - INICIO - PAG-FOR (15/05/2014 15:00) ***)
MR - 18/08/2015 10:00             - Considera o header apenas da primeira linha, porque pode ter mais de um header no arquivo (Altera��es indicadas por *** MRC - INICIO (18/08/2015 10:00) ***)
*/

using System;
using System.Collections.Generic;
using System.Text;
using System.Collections;
using ArquivosTXT;
using DocBacarios;

namespace EDIBoletos.RegistrosOBJCheque
{
    /// <summary>
    /// 
    /// </summary>
    public abstract class ChequeRetornoBase : ChequeRegistroBase
    {
        /// <summary>
        /// 
        /// </summary>
        public regHeader_0_2 Header;
        /// <summary>
        /// 
        /// </summary>
        public regTrailer_9_2 Trailer;
        /// <summary>
        /// 
        /// </summary>
        public ColCheques_2 Cheques;
        /// <summary>
        /// 
        /// </summary>
        private ChequeRegistroBase reg;
        private int nLinha = 0;
        private bool CargaOk = false;

        private cTabelaTXT _Arquivo;
        private cTabelaTXT Arquivo
        {
            get
            {
                if (_Arquivo == null) _Arquivo = new cTabelaTXT(ModelosTXT.layout);
                _Arquivo.Formatodata = FormatoData.ddMMyyyy;
                return _Arquivo;
            }
        }

        private ChequeRegistroBase ProximoRegistro(string RegistroEsperado)
        {
            string Linha = Arquivo.ProximaLinha();
            
            if (Linha.Length != 500)
            {
                UltimoErro = new Exception(string.Format("Linha com tamanho incorreto. Esperado 500 encontrado {0}\r\nLinha:{1} <{2}>", Linha.Length, nLinha, Linha));
                return null;
            }
            if ((Linha == null) || (Linha == ""))
            {
                UltimoErro = new Exception(string.Format("T�rmino inesperado de arquivo\r\nRegisto(s) esperado(s): {0}", RegistroEsperado));
                return null;
            }
            if (reg == null)
                reg = new ChequeRegistroBase(Arquivo, Linha);
            else
                reg.Decodificalinha(Linha);
            nLinha++;
            return reg;
        }

        /// <summary>
        /// 
        /// </summary>
        public bool Carrega(string NomeArquivo)
        {
            UltimoErro = null;
            try
            {
                CargaOk = _Carrega(NomeArquivo);
            }
            finally
            {
                if (_Arquivo != null) _Arquivo.LiberaArquivo();
            }
            return CargaOk;
        }

        /// <summary>
        /// 
        /// </summary>
        public string NomeArquivo
        {
            get { return Arquivo.NomeArquivo; }
        }

        //*** MRC - INICIO - PAG-FOR (15/05/2014 15:00) ***
        //*** MRC - INICIO - PAG-FOR (16/04/2014 14:30) ***         
        private bool _Carrega(string NomeArquivo)
        {
            Cheques = new ColCheques_2();
            Arquivo.NomeArquivo = NomeArquivo;
            bool booHeader = false;
            int nSequencial = 0;
            int nRegistro = 1;
            if (ProximoRegistro("0") == null)
                return false;
            nSequencial++;
            do
            {
                if (reg.Registro == 0)
                {
                    //*** MRC - INICIO (18/08/2015 10:00) ***
                    if (!booHeader)
                    {
                        Header = new regHeader_0_2(Arquivo, reg.LinhaLida);
                        booHeader = true;
                    }
                    //*** MRC - TERMINO (18/08/2015 10:00) ***
                    if (ProximoRegistro("1 9") == null)
                        return false;
                    nSequencial++;
                }
                else
                {
                    UltimoErro = new Exception(string.Format("Registro inicial incorreto. Esperado 0 encontrado {0}\r\n Linha {1} <{2}>", reg.Registro, nLinha, reg.LinhaLida));
                    return false;
                }
                while (reg.Registro != 9 && reg.Registro != 0)
                {
                    regTransacao_1_2 regTrans = new regTransacao_1_2(Arquivo, reg.LinhaLida);
                    Cheques.Add(regTrans);
                    nRegistro++;
                    if (regTrans.Sequencial != nSequencial)
                    {
                        UltimoErro = new Exception(string.Format("Registro fora de ordem: Esperado: {0} Encontrado: {1}\r\nLinha:{2} <{3}>", nRegistro, regTrans.Sequencial, nLinha, reg.LinhaLida));
                        return false;
                    };
                    if (ProximoRegistro("0 1 9") == null)
                        return false;
                    nSequencial++;
                }
                if (reg.Registro == 9)
                {
                    Trailer = new regTrailer_9_2(Arquivo, reg.LinhaLida);
                    if (Arquivo.PreviaProximaLinha != null)
                    {
                        nSequencial = 0;
                        if (ProximoRegistro("0") == null)
                            return false;
                        nSequencial++;
                    }
                }
            }
            while (Arquivo.PreviaProximaLinha != null);
            return true;
        }
        //*** MRC - TERMINO - PAG-FOR (16/04/2014 14:30) *** 
        //*** MRC - TERMINO - PAG-FOR (15/05/2014 15:00) ***

        /// <summary>
        /// 
        /// </summary>
        public string Relatorio()
        {
            string relatorio = "";
            relatorio += String.Format("Arquivo {0}: ", Arquivo.NomeArquivo);

            if (CargaOk)
            {
                relatorio += String.Format("\r\n\r\n{0}\r\n\r\nTipo Inscri��o: {1}\r\nN�mero Inscri��o: {2}\r\nNome Empresa: {3}\r\nTipo Servi�o: {4}\r\nCodigo Origem Arquivo: {5}\r\nN�mero Retorno: {6}\r\nData Gera��o: {7:dd/MM/yyyy}\r\nHora Gera��o: {8}\r\nTipo Processamento: {9}\r\nUso Empresa: {10}\r\nN�mero Lista D�bito: {11}\r\nSequencial: {12}\r\n\r\nCHEQUES: {13}\r\n\r\n",
                                           Header.CodComunicacao,
                                           Header.TipoInscricao,
                                           Header.NumInscricao,
                                           Header.NomeEmpresa,
                                           Header.TipoServico,
                                           Header.CodOrigemArq,
                                           Header.NumRetorno,
                                           Header.DataGeracao,
                                           Header.HoraGeracao,
                                           Header.TipoProcessamento,
                                           Header.UsoEmpresa,
                                           Header.NumListaDebito,
                                           Header.Sequencial,
                                           Cheques.Count);

                foreach (regTransacao_1_2 trans in Cheques)
                {
                    relatorio += String.Format("        CHE {0} Tipo Inscr. Forn. {1} N�mero Inscr. Forn. {2} Nome Forn. {3} End. Forn {4} Cep Forn. {5} Banco Forn. {6} Ag�ncia Forn. {7}-{8} Conta Forn. {9}-{10} N�mero Pagamento {11} Carteira {12} Seu N�mero {13} Vencimento {14:dd/MM/yyyy} Emiss�o {15:dd/MM/yyyy} Data Lim. Desconto {16:dd/MM/yyyy} Fator Vencimento {17} Valor {18:n2} Valor Pagamento {19:n2} Desconto {20:n2} Acr�scimo {21:n2} Tipo Documento {22} Documento {23}-{24} Modalidade Pagamento {25} Data Efetiva Pagto {26:dd/MM/yyyy} Sit. Agendamento {27} Infos Retorno {28}-{29}-{30}-{31}-{32} Tipo Movimento {33} C�d Movimento {34} Hor�rio Consulta Saldo {35} Saldo {36:n2} Taxa {37:n2} Sac. Avalista {38} Nivel Info Retorno {39} Info Complem. {40} C�d. �rea Empresa {41} Uso Empresa {42} C�d. Lan�amento {43} Tipo Conta Forn {44} Conta Complementar {45} Sequencial {46}\r\n\r\n",
                                               trans.NumeroPagamento,
                                               trans.TipoInscricaoForn,
                                               trans.NumInscricaoForn,
                                               trans.NomeForn,
                                               trans.EnderecoForn,
                                               trans.CepForn,
                                               trans.BancoForn,
                                               trans.AgenciaForn,
                                               trans.DigAgenciaForn,
                                               trans.ContaForn,
                                               trans.DigContaForn,
                                               trans.NumeroPagamento,
                                               trans.Carteira,
                                               trans.SeuNumero,
                                               trans.DataVencimento,
                                               trans.DataEmissao,
                                               trans.DataLimDesconto,
                                               trans.FatorVencimento,
                                               trans.Valor,
                                               trans.ValorPagamento,
                                               trans.Desconto,
                                               trans.Acrescimo,
                                               trans.TipoDocumento,
                                               trans.NumDocumento,
                                               trans.SerieDocumento,
                                               trans.ModalidadePagamento,
                                               trans.DataEfetivaPagamento,
                                               trans.SituacaoAgendamento,
                                               trans.InfoRetorno1,
                                               trans.InfoRetorno2,
                                               trans.InfoRetorno3,
                                               trans.InfoRetorno4,
                                               trans.InfoRetorno5,
                                               trans.TipoMovimento,
                                               trans.CodMovimento,
                                               trans.HorarioConsultaSaldo,
                                               trans.Saldo,
                                               trans.ValorTaxa,
                                               trans.SacAvalista,
                                               trans.NivelInfoRetorno,
                                               trans.InfoComplementar,
                                               trans.CodAreaEmpresa,
                                               trans.UsoEmpresa,
                                               trans.CodLancamento,
                                               trans.TipoContaForn,
                                               trans.ContaComplementar,
                                               trans.Sequencial);
                }

                relatorio += String.Format("Qtd Registros: {0}\r\nTotal Valor Pagamento: {1:n2}\r\nSequencial: {2}",
                                           Trailer.QTDRegistros,
                                           Trailer.TotalValorPago,
                                           Trailer.Sequencial);
            }
            else
            {
                relatorio += String.Format("Erro\r\n{0}", UltimoErro.Message);
            }
            return relatorio;
        }
    }


    /// <summary>
    /// 
    /// </summary>
    public class regHeader_0_2 : regHeader_0
    {
        #region Variaveis Tipadas

        /// <summary>
        /// 
        /// </summary>
        public long NumInscricao;
        /// <summary>
        /// 
        /// </summary>
        public int TipoInscricao;

        #endregion

        /// <summary>
        /// 
        /// </summary>
        public regHeader_0_2(cTabelaTXT _cTabelaTXT, string Linha)
            : base(_cTabelaTXT, Linha)
        {
        }

        /// <summary>
        /// 
        /// </summary>
        protected override void RegistraMapa()
        {
            base.RegistraMapa();
            RegistraFortementeTipado("TipoInscricao", 10, 1);
            RegistraFortementeTipado("NumInscricao", 11, 15);
        }
    }

    /// <summary>
    /// 
    /// </summary>
    public class regTransacao_1_2 : regTransacao_1
    {
        #region Variaveis Tipadas

        /// <summary>
        /// 
        /// </summary>
        public int TipoInscricaoForn;
        /// <summary>
        /// 
        /// </summary>
        public long NumInscricaoForn;
        /// <summary>
        /// 
        /// </summary>
        public int BancoForn;
        /// <summary>
        /// 
        /// </summary>
        public int AgenciaForn;
        /// <summary>
        /// 
        /// </summary>
        public string DigAgenciaForn;
        /// <summary>
        /// 
        /// </summary>
        public int ContaForn;
        /// <summary>
        /// 
        /// </summary>
        public string DigContaForn;
        /// <summary>
        /// 
        /// </summary>
        public int Carteira;
        /// <summary>
        /// 
        /// </summary>
        public int NossoNumero;
        /// <summary>
        /// 
        /// </summary>
        public String DataVencimento;
        /// <summary>
        /// 
        /// </summary>
        public String DataEmissao;
        /// <summary>
        /// 
        /// </summary>
        public String DataLimDesconto;
        /// <summary>
        /// 
        /// </summary>
        public int FatorVencimento;
        /// <summary>
        /// 
        /// </summary>
        public decimal Valor;
        /// <summary>
        /// 
        /// </summary>
        public decimal ValorPagamento;
        /// <summary>
        /// 
        /// </summary>
        public String DataEfetivaPagamento;
        /// <summary>
        /// 
        /// </summary>
        public string InfoRetorno1;
        /// <summary>
        /// 
        /// </summary>
        public string InfoRetorno2;
        /// <summary>
        /// 
        /// </summary>
        public string InfoRetorno3;
        /// <summary>
        /// 
        /// </summary>
        public string InfoRetorno4;
        /// <summary>
        /// 
        /// </summary>
        public string InfoRetorno5;
        /// <summary>
        /// 
        /// </summary>
        public decimal Saldo;
        /// <summary>
        /// 
        /// </summary>
        public decimal ValorTaxa;
        /// <summary>
        /// 
        /// </summary>
        public string NivelInfoRetorno;

        private string _InfoComplementar;
        /// <summary>
        /// 
        /// </summary>
        public string InfoComplementar
        {
            get { return (_InfoComplementar == null) ? "" : _InfoComplementar.ToUpper(); }
            set { _InfoComplementar = value; }
        }

        #endregion

        /// <summary>
        /// 
        /// </summary>
        public regTransacao_1_2(cTabelaTXT _cTabelaTXT, string Linha)
            : base(_cTabelaTXT, Linha)
        {
        }

        /// <summary>
        /// 
        /// </summary>
        protected override void RegistraMapa()
        {
            base.RegistraMapa();
            RegistraFortementeTipado("TipoInscricaoForn", 2, 1);
            RegistraFortementeTipado("NumInscricaoForn", 3, 15);
            RegistraFortementeTipado("BancoForn", 96, 3);
            RegistraFortementeTipado("AgenciaForn", 99, 5);
            RegistraFortementeTipado("DigAgenciaForn", 104, 1);
            RegistraFortementeTipado("ContaForn", 105, 13);
            RegistraFortementeTipado("DigContaForn", 118, 2);
            RegistraFortementeTipado("Carteira", 136, 3);
            RegistraFortementeTipado("NossoNumero", 139, 12);
            RegistraFortementeTipado("DataVencimento", 166, 8);
            RegistraFortementeTipado("DataEmissao", 174, 8);
            RegistraFortementeTipado("DataLimDesconto", 182, 8);
            RegistraFortementeTipado("FatorVencimento", 191, 4);
            RegistraFortementeTipado("Valor", 195, 10);
            RegistraFortementeTipado("ValorPagamento", 205, 15);
            RegistraFortementeTipado("DataEfetivaPagamento", 266, 8);
            RegistraFortementeTipado("InfoRetorno1", 279, 2);
            RegistraFortementeTipado("InfoRetorno2", 281, 2);
            RegistraFortementeTipado("InfoRetorno3", 283, 2);
            RegistraFortementeTipado("InfoRetorno4", 285, 2);
            RegistraFortementeTipado("InfoRetorno5", 287, 2);
            RegistraFortementeTipado("Saldo", 296, 15);
            RegistraFortementeTipado("ValorTaxa", 311, 15);
            RegistraFortementeTipado("NivelInfoRetorno", 373, 1);
            RegistraFortementeTipado("InfoComplementar", 374, 40);
        }
    }

    /// <summary>
    /// 
    /// </summary>
    public class regTrailer_9_2 : regTrailer_9
    {
        #region Variaveis Tipadas

        #endregion

        /// <summary>
        /// 
        /// </summary>
        public regTrailer_9_2(cTabelaTXT _cTabelaTXT, string Linha)
            : base(_cTabelaTXT, Linha)
        {
        }

        /// <summary>
        /// 
        /// </summary>
        protected override void RegistraMapa()
        {
            base.RegistraMapa();
        }
    }

    /// <summary>
    /// 
    /// </summary>
    public class ColCheques_2 : CollectionBase
    {
        /// <summary>
        /// 
        /// </summary>
        public regTransacao_1_2 this[int index]
        {
            get
            {
                return ((regTransacao_1_2)List[index]);
            }
            set
            {
                List[index] = value;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        public int Add(regTransacao_1_2 value)
        {
            return (List.Add(value));
        }

        /// <summary>
        /// 
        /// </summary>
        public int IndexOf(regTransacao_1_2 value)
        {
            return (List.IndexOf(value));
        }

        /// <summary>
        /// 
        /// </summary>
        public void Insert(int index, regTransacao_1_2 value)
        {
            List.Insert(index, value);
        }

        /// <summary>
        /// 
        /// </summary>
        public void Remove(regTransacao_1_2 value)
        {
            List.Remove(value);
        }

        /// <summary>
        /// 
        /// </summary>
        public bool Contains(regTransacao_1_2 value)
        {
            // If value is not of type Int16, this will return false.
            return (List.Contains(value));
        }

        /// <summary>
        /// 
        /// </summary>
        protected override void OnValidate(Object value)
        {
            if (value.GetType() != typeof(regTransacao_1_2))
                throw new ArgumentException("Value must be of type regTransacao_1_2.", "value");
        }
    }
}
