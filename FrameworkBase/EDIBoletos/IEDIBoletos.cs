﻿using VirEnumeracoes;

namespace EDIBoletos
{
    /// <summary>
    /// Interface para registro de boletos
    /// </summary>
    public interface IEDIBoletos
    {
        /// <summary>
        /// Registra Boleto
        /// </summary>
        /// <param name="Remessa"></param>
        /// <returns></returns>
        bool Registrar(BoletoRemessaBase Remessa);

        /// <summary>
        /// Altera o status de registro do boleto
        /// </summary>
        void AlteraStatusRegistro(StatusRegistroBoleto intStatusRegistro,string informacao);
    }
}
