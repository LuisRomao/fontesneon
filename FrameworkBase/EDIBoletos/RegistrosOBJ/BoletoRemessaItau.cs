﻿/*
MR - 25/02/2016 11:00 -           - Padronização Arquivo de Cobrança Itau (Alterações indicadas por *** MRC - INICIO - REMESSA (25/02/2016 11:00) ***)
MR - 21/03/2016 20:00 -           - GravaArquivo permite usar um arquivo indicado nos parametros (Alterações indicadas por *** MRC - INICIO (21/03/2016 20:00) ***)
                                  - Inclusão do FinalizaArquivo
                                  - Retirada de Sequencial Remessa do Header, não usado no Itaú e ajuste no nome do arquivo 
MR - 27/05/2016 12:00 -           - Registro de linha opcional de multa para Itaú (Alterações indicadas por *** MRC - INICIO (27/05/2016 12:00) ***)
*/

using System;
using ArquivosTXT;
using DocBacarios;
using EDIBoletos.RegistrosOBJ;

namespace EDIBoletos
{
    /// <summary>
    /// Remessa Bradesco - Registro de boletos
    /// </summary>
    public class BoletoRemessaItau : BoletoRemessaBase
    {
        /// <summary>
        /// Header
        /// </summary>
        public regHeader_0_1_ITAU Header;

        /// <summary>
        /// Trailer
        /// </summary>
        public regTrailer_9_1_ITAU Trailer;

        //*** MRC - INICIO - REMESSA (25/02/2016 11:00) ***
        #region Enumerações
        /// <summary>
        /// Ocorrencia
        /// </summary>
        public enum TiposOcorrencia
        {
            /// <summary>
            /// Registrar o boleto
            /// </summary>
            Remessa = 1,
            /// <summary>
            /// Não usamos
            /// </summary>
            PedidoBaixa = 2,
            /// <summary>
            /// Concessao Abatimento
            /// </summary>
            ConcessaoAbatimento = 4,
            /// <summary>
            /// Não usamos
            /// </summary>
            CancelamentoAbatimentoConcedido = 5,
            /// <summary>
            /// Usamos o AlteracaoOutrosDados
            /// </summary>
            AlteracaoVencimento = 6,
            /// <summary>
            /// Não usamos
            /// </summary>
            AlteracaoControleParticipante = 7,
            /// <summary>
            /// Não usamos
            /// </summary>
            AlteracaoNumero = 8,
            /// <summary>
            /// Não usamos
            /// </summary>
            Protestar = 9,
            /// <summary>
            /// Não usamos
            /// </summary>
            NaoProtestar = 10,
            /// <summary>
            /// Não usamos
            /// </summary>
            ProtestoFinsFalimentares = 11,
            /// <summary>
            /// Não usamos
            /// </summary>
            SustarProtesto = 18,
            /// <summary>
            /// Não usamos
            /// </summary>
            ExclusaoSacadorAvalista = 30,
            /// <summary>
            /// Altera os dados do Boleto registrado
            /// </summary>
            AlteracaoOutrosDados = 31,
            /// <summary>
            /// Não usamos
            /// </summary>
            BaixaPorPagamentoDiretoBeneficiario = 34,
            /// <summary>
            /// Não usamos
            /// </summary>
            CancelamentoInstrucao = 35,
            /// <summary>
            /// Não usamos
            /// </summary>
            AlteracaoVencimentoSustarProtesto = 37,
            /// <summary>
            /// Não usamos
            /// </summary>
            BeneficiarioNaoConcordaAlegacaoPagador = 38,
            /// <summary>
            /// Não usamos
            /// </summary>
            BeneficiarioSolicitaDispensaJuros = 47
        }

        /// <summary>
        /// Tipos de Título
        /// </summary>
        public enum TiposEspecieTitulo
        {
            /// <summary>
            /// 
            /// </summary>
            Duplicata = 1,
            /// <summary>
            /// 
            /// </summary>
            NotaPromissoria = 2,
            /// <summary>
            /// 
            /// </summary>
            NotaSeguro = 3,
            /// <summary>
            /// 
            /// </summary>
            MensalidadeEscolar = 4,
            /// <summary>
            /// 
            /// </summary>
            Recibo = 5,
            /// <summary>
            /// 
            /// </summary>
            Contrato = 6,
            /// <summary>
            /// 
            /// </summary>
            Cosseguros = 7,
            /// <summary>
            /// 
            /// </summary>
            DuplicataServico = 8,
            /// <summary>
            /// 
            /// </summary>
            LetrasCambio = 9,
            /// <summary>
            /// 
            /// </summary>
            NotaDebito = 13,
            /// <summary>
            /// 
            /// </summary>
            DocumentoDivida = 15,
            /// <summary>
            /// 
            /// </summary>
            Condominio = 16,
            /// <summary>
            /// 
            /// </summary>
            ContaPrestacaoServicos = 17,
            /// <summary>
            /// 
            /// </summary>
            Outros = 99,
        }

        /// <summary>
        /// 
        /// </summary>
        public enum TiposInscricao
        {
            /// <summary>
            /// 
            /// </summary>
            CPF = 1,
            /// <summary>
            /// 
            /// </summary>
            CNPJ = 2,
            /// <summary>
            /// 
            /// </summary>
            NaoTem = 00,
        }

        //*** MRC - INICIO (27/05/2016 12:00) ***
        /// <summary>
        /// 
        /// </summary>
        public enum TiposCodigoMulta
        {
            /// <summary>
            /// 
            /// </summary>
            NaoRegistraMulta = 0,
            /// <summary>
            /// 
            /// </summary>
            ValorFixo = 1,
            /// <summary>
            /// 
            /// </summary>
            Percentual = 2,
        }
        //*** MRC - INICIO (27/05/2016 12:00) ***
        #endregion
        //*** MRC - TERMINO - REMESSA (25/02/2016 11:00) ***

        /*
        public override void IniciaRemessa()
        {
            
        }*/

        //*** MRC - INICIO - REMESSA (25/02/2016 11:00) ***
        /// <summary>
        /// 
        /// </summary>
        public void InsereBoletos()
        {
            foreach (BoletoReg boleto in Boletos)
            {
                regTransacao_1_1_ITAU Boleto = new regTransacao_1_1_ITAU(Header) { Carteira = Carteira,
                                                                                   CNPJ = boleto.CNPJ,
                                                                                   Agencia = int.Parse(boleto.Agencia),
                                                                                   Conta = int.Parse(boleto.Conta),
                                                                                   DigConta = boleto.DigConta,
                                                                                   ControlePart = boleto.ControlePart,
                                                                                   NossoNumero = boleto.NossoNumero,
                                                                                   IDOcorrencia = boleto.IDOcorrencia,
                                                                                   NumDocumento = boleto.NumDocumento,
                                                                                   Vencimento = boleto.Vencimento,
                                                                                   Valor = boleto.Valor,
                                                                                   EspecieTitulo = boleto.EspecieTitulo,
                                                                                   Emissao = boleto.Emissao,
                                                                                   Mora = boleto.Mora,
                                                                                   LimDesconto = boleto.LimDesconto,
                                                                                   Desconto = boleto.Desconto,
                                                                                   IOF = boleto.IOF,
                                                                                   Abatimento = boleto.Abatimento,
                                                                                   CPF = boleto.CPF,
                                                                                   Nome = boleto.NomeSacado,
                                                                                   Endereco = boleto.EnderecoSacado,
                                                                                   Bairro = boleto.BairroSacado,
                                                                                   CEP = boleto.CEPSacado,
                                                                                   Cidade = boleto.CidadeSacado,
                                                                                   Estado = boleto.EstadoSacado,
                                                                                   Mensagem1 = boleto.Mensagem1 };
                
                regTransacao_2_1_ITAU Boleto2 = new regTransacao_2_1_ITAU(Header);
                Boleto2.CobrarMulta = boleto.CobrarMulta;
                Boleto2.PercMulta = boleto.PercMulta;
                Boleto2.Multa = boleto.Vencimento;
            }
        }
        //*** MRC - TERMINO - REMESSA (25/02/2016 11:00) ***

        //*** MRC - INICIO - REMESSA (25/02/2016 11:00) ***
        /// <summary>
        /// 
        /// </summary>
        public void CriaHeader()
        {
            Header = new regHeader_0_1_ITAU(ArquivoSaida);            
            Header.CodigoRemRet = 1;
            Header.LiteralArquivo = "REMESSA";
            Header.CodServico = 1;
            Header.LiteralServico = "COBRANCA";
            Header.AgenciaMae = AgenciaMae;
            Header.ContaMae = ContaMae;
            Header.ContaMaeDg = ContaMaeDg;
            Header.NomeEmpresa = NomeEmpresa;
            Header.DataGeracao = DateTime.Today.ToString("ddMMyy");
            Header.Sequencial = 1;
        }
        //*** MRC - TERMINO - REMESSA (25/02/2016 11:00) ***

        /// <summary>
        /// 
        /// </summary>
        public void CriaTrailer()
        {
            Trailer = new regTrailer_9_1_ITAU(Header);
        }

        /// <summary>
        /// Prefixo do arquivo
        /// </summary>
        public override string PrefixoArquivo
        {
            get
            {
                return "CN";
            }
        }

        //*** MRC - INICIO (21/03/2016 20:00) ***
        /// <summary>
        /// Grava o arquivo formato ITAU (efetivo)
        /// </summary>
        //public override string GravaArquivo(string Path)
        public override string GravaArquivo(string Path, string Arquivo = "")
        {
            try
            {
                string NomeArquivo = Arquivo;
                int? Remessa;
                if (NomeArquivo == "")
                    NomeArquivo = ProximoNome(out Remessa);    
                CriaHeader();
                if (Header == null)
                    return "";
                else
                {
                    InsereBoletos();
                    CriaTrailer();                    
                    if (Header.GravaArquivo(NomeArquivo, false, true))                    
                        return NomeArquivo;
                    else
                        return "";
                }
            }
            catch (Exception ex)
            {
                UltimoErro = ex;
                return "";
            }
        }

        
        /// <summary>
        /// Finaliza o Arquivo
        /// </summary>
        /// <param name="NomeArquivo"></param>
        /// <returns></returns>
        public override string FinalizaArquivo(string NomeArquivo)
        {
            try
            {
                if (Header.FinalizaArquivo(NomeArquivo))
                    return NomeArquivo;
                else
                    return "";
            }
            catch (Exception ex)
            {
                UltimoErro = ex;
                return "";
            }
        }
        
    }

    //*** MRC - INICIO - REMESSA (25/02/2016 11:00) ***
    /// <summary>
    /// 
    /// </summary>
    public class regHeader_0_1_ITAU : regHeader_0
    {
        #region Variaveis Tipadas

        /// <summary>
        /// Agencia Mãe (somente Itau)
        /// </summary>
        public int AgenciaMae;

        /// <summary>
        /// Conta Mãe (somente Itau)
        /// </summary>
        public int ContaMae;

        /// <summary>
        /// Digito da Conta Mãe (somente Itau)
        /// </summary>
        public string ContaMaeDg;

        #endregion

        /// <summary>
        /// 
        /// </summary>
        public regHeader_0_1_ITAU(cTabelaTXT _cTabelaTXT)
            : base(_cTabelaTXT, null)
        {
            NumeroBanco = 341;
            NomeBanco = "BANCO ITAU SA";
        }

        /// <summary>
        /// 
        /// </summary>
        protected override void RegistraMapa()
        {
            base.RegistraMapa();
            RegistraFortementeTipado("AgenciaMae"   ,  27,  4);
            RegistraMapa            ("Zero"         ,  31,  2, 0);
            RegistraFortementeTipado("ContaMae"     ,  33,  5);
            RegistraFortementeTipado("ContaMaeDg"   ,  38,  1);            
        }
    }
    //*** MRC - TERMINO - REMESSA (25/02/2016 11:00) ***

    //*** MRC - INICIO - REMESSA (25/02/2016 11:00) ***
    /// <summary>
    /// 
    /// </summary>
    public class regTransacao_1_1_ITAU : regTransacao_1
    {
        #region Variaveis Tipadas
        
        /// <summary>
        /// CNPJ do condominio
        /// </summary>
        internal CPFCNPJ CNPJ { get; set; }

        /// <summary>
        /// Tipo CNPJ Condominio
        /// </summary>
        public int TipoCNPJ
        {
            get
            {
                if (CNPJ == null) { return (int)BoletoRemessaItau.TiposInscricao.NaoTem; }
                else
                {
                    switch (CNPJ.Tipo)
                    {
                        case TipoCpfCnpj.CNPJ: return (int)BoletoRemessaItau.TiposInscricao.CNPJ;
                        case TipoCpfCnpj.CPF: return (int)BoletoRemessaItau.TiposInscricao.CPF;
                        case TipoCpfCnpj.INVALIDO: return (int)BoletoRemessaItau.TiposInscricao.NaoTem;
                        default: return (int)BoletoRemessaItau.TiposInscricao.NaoTem;
                    }
                }
            }
        }

        /// <summary>
        /// Valor Numérico do CNPJ
        /// </summary>
        public Int64 NumCNPJ
        {
            get
            {
                if (CNPJ == null) { return 0; }
                else { return CNPJ.Tipo == TipoCpfCnpj.INVALIDO ? 0 : CNPJ.Valor; }
            }
        }

        /// <summary>
        /// 
        /// </summary>
        public int Agencia;

        private int _Conta;
        /// <summary>
        /// 
        /// </summary>
        public int Conta
        {
            get { return Convert.ToInt32(_Conta.ToString("00000").Substring(0,5)); }
            set { _Conta = value; }
        }

        /// <summary>
        /// 
        /// </summary>
        public string DigConta;

        /// <summary>
        /// 
        /// </summary>
        public int NossoNumero;

        /// <summary>
        /// 
        /// </summary>
        public string Carteira;

        private string _CodCarteira;
        /// <summary>
        /// 
        /// </summary>
        public string CodCarteira
        {
            get { return (Carteira == "150" ? "U" : (Carteira == "191" ? "1" : (Carteira == "147" ? "E" : "I"))); }
            set { _CodCarteira = value; }
        }

        private int _IDOcorrencia;
        /// <summary>
        /// 
        /// </summary>
        public int IDOcorrencia
        {
            get { return (_IDOcorrencia == 0) ? (int)BoletoRemessaItau.TiposOcorrencia.Remessa : _IDOcorrencia; }
            set { _IDOcorrencia = value; }
        }

        /// <summary>
        /// 
        /// </summary>
        public string NumDocumento;

        /// <summary>
        /// 
        /// </summary>
        public DateTime Vencimento;
        private string _DataVencimento;
        /// <summary>
        /// 
        /// </summary>
        public string DataVencimento
        {
            get
            {
                return ((Vencimento.Year == 1) || (_DataVencimento == "")) ? "000000" : Vencimento.Day.ToString("00") + Vencimento.Month.ToString("00") + Vencimento.Year.ToString().Substring(2, 2);
            }
            set { _DataVencimento = value; }
        }

        /// <summary>
        /// 
        /// </summary>
        public decimal Valor;

        private int _EspecieTitulo;
        /// <summary>
        /// 
        /// </summary>
        public int EspecieTitulo
        {
            get { return (_EspecieTitulo == 0) ? (int)BoletoRemessaItau.TiposEspecieTitulo.Condominio : _EspecieTitulo; }
            set { _EspecieTitulo = value; }
        }

        /// <summary>
        /// 
        /// </summary>
        public DateTime Emissao;
        private string _DataEmissao;
        /// <summary>
        /// 
        /// </summary>
        public string DataEmissao
        {
            get
            {
                return ((Emissao.Year == 1) || (_DataEmissao == "")) ? "000000" : Emissao.Day.ToString("00") + Emissao.Month.ToString("00") + Emissao.Year.ToString().Substring(2, 2);
            }
            set { _DataEmissao = value; }
        }

        /// <summary>
        /// 
        /// </summary>
        public decimal Mora;

        /// <summary>
        /// 
        /// </summary>
        public DateTime LimDesconto;
        private string _DataLimDesconto;
        /// <summary>
        /// 
        /// </summary>
        public string DataLimDesconto
        {
            get
            {
                return ((LimDesconto.Year == 1) || (_DataLimDesconto == "")) ? "000000" : LimDesconto.Day.ToString("00") + LimDesconto.Month.ToString("00") + LimDesconto.Year.ToString().Substring(2, 2);
            }
            set { _DataLimDesconto = value; }
        }

        /// <summary>
        /// 
        /// </summary>
        public decimal Desconto;

        /// <summary>
        /// 
        /// </summary>
        public decimal IOF;

        /// <summary>
        /// 
        /// </summary>
        public decimal Abatimento;

        private CPFCNPJ _CPF;
        /// <summary>
        /// 
        /// </summary>
        public CPFCNPJ CPF
        {
            get { return _CPF; }
            set { _CPF = value; }
        }

        /// <summary>
        /// 
        /// </summary>
        public int TipoInscricao
        {
            get
            {
                if (CPF == null) { return (int)BoletoRemessaItau.TiposInscricao.NaoTem; }
                else
                {
                    switch (CPF.Tipo)
                    {
                        case TipoCpfCnpj.CNPJ: return (int)BoletoRemessaItau.TiposInscricao.CNPJ;
                        case TipoCpfCnpj.CPF: return (int)BoletoRemessaItau.TiposInscricao.CPF;
                        case TipoCpfCnpj.INVALIDO: return (int)BoletoRemessaItau.TiposInscricao.NaoTem;
                        default: return (int)BoletoRemessaItau.TiposInscricao.NaoTem;
                    }
                }
            }
        }

        /// <summary>
        /// 
        /// </summary>
        public Int64 NumInscricao
        {
            get
            {
                if (CPF == null) { return 0; }
                else { return CPF.Tipo == TipoCpfCnpj.INVALIDO ? 0 : CPF.Valor; }
            }
        }

        private string _Nome;
        /// <summary>
        /// 
        /// </summary>
        public string Nome
        {
            get { return (_Nome == null) ? "" : _Nome.ToUpper(); }
            set { _Nome = value; }
        }

        private string _Endereco;
        /// <summary>
        /// 
        /// </summary>
        public string Endereco
        {
            get { return (_Endereco == null) ? "" : _Endereco.ToUpper(); }
            set { _Endereco = value; }
        }

        private string _Bairro;
        /// <summary>
        /// 
        /// </summary>
        public string Bairro
        {
            get { return (_Bairro == null) ? "" : _Bairro.PadRight(12, ' ').Substring(0, 12).ToUpper(); }
            set { _Bairro = value; }
        }

        private string _CEP;
        /// <summary>
        /// 
        /// </summary>
        public string CEP
        {
            get
            {
                long lngCep = 0;
                if (_CEP != null) { _CEP = _CEP.Replace("-", ""); }
                return (long.TryParse(_CEP, out lngCep)) ? _CEP.PadLeft(8, '0') : "00000000";
            }
            set { _CEP = value; }
        }

        private string _Cidade;
        /// <summary>
        /// 
        /// </summary>
        public string Cidade
        {
            get { return (_Cidade == null) ? "" : _Cidade.PadRight(15, ' ').Substring(0, 15).ToUpper(); }
            set { _Cidade = value; }
        }

        private string _Estado;
        /// <summary>
        /// 
        /// </summary>
        public string Estado
        {
            get { return (_Estado == null) ? "" : _Estado.Substring(0, 2).ToUpper(); }
            set { _Estado = value; }
        }

        private string _Mensagem1;
        /// <summary>
        /// 
        /// </summary>
        public string Mensagem1
        {
            get { return (_Mensagem1 == null) ? "" : _Mensagem1.ToUpper(); }
            set { _Mensagem1 = value; }
        }

        #endregion

        private regHeader_0_1_ITAU regPai;

        /// <summary>
        /// 
        /// </summary>
        public regTransacao_1_1_ITAU(regHeader_0_1_ITAU _regPai)
            : base(_regPai.cTabTXT, null)
        {
            regPai = _regPai;
            regPai.Transacoes.Add(this);

            // *** MRC - INICIO (27/05/2016 12:00) ***
            Sequencial = regPai.Transacoes.Count + regPai.Transacoes2_Multa.Count + 1;
            // *** MRC - TERMINO (27/05/2016 12:00) ***
        }

        /// <summary>
        /// 
        /// </summary>
        protected override void RegistraMapa()
        {
            base.RegistraMapa();
            RegistraFortementeTipado("TipoCNPJ"       ,   2,  2);
            RegistraFortementeTipado("NumCNPJ"        ,   4, 14);
            RegistraFortementeTipado("Agencia"        ,  18,  4);
            RegistraMapa            ("Zero"           ,  22,  2, 0);
            RegistraFortementeTipado("Conta"          ,  24,  5);
            RegistraFortementeTipado("DigConta"       ,  29,  1);
            RegistraMapa            ("Zero1"          ,  34,  4, 0);
            RegistraFortementeTipado("NossoNumero"    ,  63,  8);
            RegistraMapa            ("Zero2"          ,  71, 13, 0);
            RegistraFortementeTipado("Carteira"       ,  84,  3);
            RegistraFortementeTipado("CodCarteira"    , 108,  1);
            RegistraFortementeTipado("IDOcorrencia"   , 109,  2);
            RegistraFortementeTipado("NumDocumento"   , 111, 10);
            RegistraFortementeTipado("DataVencimento" , 121,  6);
            RegistraFortementeTipado("Valor"          , 127, 13);
            RegistraMapa            ("BcoCobranca"    , 140,  3, 341);
            RegistraMapa            ("AgDepositaria"  , 143,  5, 0);
            RegistraFortementeTipado("EspecieTitulo"  , 148,  2);
            RegistraMapa            ("IDAceite"       , 150,  1, "N");
            RegistraFortementeTipado("DataEmissao"    , 151,  6);
            RegistraMapa            ("Instrucao1Fixa" , 157,  2, 3);
            RegistraMapa            ("Instrucao2Fixa" , 159,  2, 10);
            RegistraFortementeTipado("Mora"           , 161, 13);
            RegistraFortementeTipado("DataLimDesconto", 174,  6);
            RegistraFortementeTipado("Desconto"       , 180, 13);
            RegistraFortementeTipado("IOF"            , 193, 13);
            RegistraFortementeTipado("Abatimento"     , 206, 13);
            RegistraFortementeTipado("TipoInscricao"  , 219,  2);
            RegistraFortementeTipado("NumInscricao"   , 221, 14);
            RegistraFortementeTipado("Nome"           , 235, 40);
            RegistraFortementeTipado("Endereco"       , 275, 40);
            RegistraFortementeTipado("Bairro"         , 315, 12);
            RegistraFortementeTipado("CEP"            , 327,  8);
            RegistraFortementeTipado("Cidade"         , 335, 15);
            RegistraFortementeTipado("Estado"         , 350,  2);
            RegistraFortementeTipado("Mensagem1"      , 352, 30);
            RegistraMapa            ("Zero3"          , 386,  6, 0);
            RegistraMapa            ("Zero4"          , 392,  2, 0);
        }
    }
    //*** MRC - TERMINO - REMESSA (25/02/2016 11:00) ***

    //*** MRC - INICIO (27/05/2016 12:00) ***
    /// <summary>
    /// 
    /// </summary>
    public class regTransacao_2_1_ITAU : regTransacao_2
    {
        #region Variaveis Tipadas

        /// <summary>
        /// 
        /// </summary>
        public bool CobrarMulta;

        private int _CampoMulta;
        /// <summary>
        /// 
        /// </summary>
        public int CampoMulta
        {
            get { return CobrarMulta ? (int)BoletoRemessaItau.TiposCodigoMulta.Percentual : 0; }
            set { _CampoMulta = value; }
        }

        private decimal _PercMulta;
        /// <summary>
        /// 
        /// </summary>
        public decimal PercMulta
        {
            get { return CobrarMulta ? _PercMulta : 0; }
            set { _PercMulta = value; }
        }

        /// <summary>
        /// 
        /// </summary>
        public DateTime Multa;
        private string _DataMulta;
        /// <summary>
        /// 
        /// </summary>
        public string DataMulta
        {
            get
            {
                Multa = Multa.AddDays(1);
                return ((Multa.Year == 1) || (_DataMulta == "")) ? "000000" : Multa.Day.ToString("00") + Multa.Month.ToString("00") + Multa.Year.ToString();
            }
            set { _DataMulta = value; }
        }

        #endregion

        private regHeader_0_1_ITAU regPai;

        /// <summary>
        /// 
        /// </summary>
        public regTransacao_2_1_ITAU(regHeader_0_1_ITAU _regPai)
            : base(_regPai.cTabTXT, null)
        {
            regPai = _regPai;
            regPai.Transacoes2_Multa.Add(this);

            // *** MRC - INICIO (27/05/2016 12:00) ***
            Sequencial = regPai.Transacoes.Count + regPai.Transacoes2_Multa.Count + 1;
            // *** MRC - TERMINO (27/05/2016 12:00) ***
        }

        /// <summary>
        /// 
        /// </summary>
        protected override void RegistraMapa()
        {
            base.RegistraMapa();
            RegistraFortementeTipado("CampoMulta", 2,  1);
            RegistraFortementeTipado("DataMulta",  3,  8);
            RegistraFortementeTipado("PercMulta", 11, 13);
        }
    }
    //*** MRC - TERMINO (27/05/2016 12:00) ***

    /// <summary>
    /// 
    /// </summary>
    public class regTrailer_9_1_ITAU : regTrailer_9
    {
        private regHeader_0_1_ITAU regPai;

        /// <summary>
        /// 
        /// </summary>
        public regTrailer_9_1_ITAU(regHeader_0_1_ITAU _regPai)
            : base(_regPai.cTabTXT, null)
        {
            regPai = _regPai;
            regPai.Trailer = this;

            // *** MRC - INICIO (27/05/2016 12:00) ***
            Sequencial = regPai.Transacoes.Count + regPai.Transacoes2_Multa.Count + 2;
            // *** MRC - TERMINO (27/05/2016 12:00) ***
        }
    }
}