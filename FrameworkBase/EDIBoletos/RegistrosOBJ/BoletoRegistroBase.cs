﻿/*
MR - 27/05/2016 12:00 -           - Transacao Opcional Multa (Alterações indicadas por *** MRC - INICIO (27/05/2016 12:00) ***)
*/

using System;
using System.Collections.Generic;
using System.Text;
using System.Collections;
using ArquivosTXT;
using DocBacarios;

namespace EDIBoletos.RegistrosOBJ
{
    /// <summary>
    /// 
    /// </summary>
    public class BoletoRegistroBase : RegistroBase
    {
        /// <summary>
        /// 
        /// </summary>
        public Exception UltimoErro;

        #region Propriedades tipadas

        private int _Registro;
        /// <summary>
        /// 
        /// </summary>
        public int Registro
        {
            get { return _Registro; }
            set { _Registro = value; }
        }

        private int _Sequencial;
        /// <summary>
        /// 
        /// </summary>
        public int Sequencial
        {
            get { return _Sequencial; }
            set { _Sequencial = value; }
        }

        #endregion

        /*
        /// <summary>
        /// 
        /// </summary>
        public BoletoRegistroBase(cTabelaTXT _cTabelaTXT, string Linha)
            : base(_cTabelaTXT)
        {
            CarregaLinha(Linha);
        }*/

        /// <summary>
        /// 
        /// </summary>
        public BoletoRegistroBase(cTabelaTXT _cTabelaTXT)
            : base(_cTabelaTXT)
        {            
        }

        /// <summary>
        /// 
        /// </summary>
        protected override void RegistraMapa()
        {
            RegistraFortementeTipado("Registro"  ,   1,  1);
            RegistraFortementeTipado("Sequencial", 395,  6);
        }
    }

    /// <summary>
    /// 
    /// </summary>
    public class regHeader_0 : BoletoRegistroBase
    {
        #region Variaveis Tipadas

        private int _CodigoRemRet;
        /// <summary>
        /// 
        /// </summary>
        public int CodigoRemRet
        {
            get { return _CodigoRemRet; }
            set { _CodigoRemRet = value; }
        }

        /// <summary>
        /// 
        /// </summary>
        public string LiteralArquivo;

        /// <summary>
        /// 
        /// </summary>
        public int CodServico;

        /// <summary>
        /// 
        /// </summary>
        public string LiteralServico;

        private string _NomeEmpresa;
        /// <summary>
        /// 
        /// </summary>
        public string NomeEmpresa
        {
            get { return (_NomeEmpresa == null) ? "" : _NomeEmpresa.ToUpper(); }
            set { _NomeEmpresa = value; }
        }

        /// <summary>
        /// 
        /// </summary>
        public int NumeroBanco;

        /// <summary>
        /// 
        /// </summary>
        public string NomeBanco;

        /// <summary>
        /// 
        /// </summary>
        public string DataGeracao;
        
        #endregion

        /// <summary>
        /// 
        /// </summary>
        public List<BoletoRegistroBase> Transacoes;

        //*** MRC - INICIO - REMESSA (25/02/2016 11:00) ***
        /// <summary>
        /// 
        /// </summary>
        public List<BoletoRegistroBase> Transacoes2_Multa;
        //*** MRC - TERMINO - REMESSA (25/02/2016 11:00) ***

        /// <summary>
        /// 
        /// </summary>
        public regTrailer_9 Trailer;

        /// <summary>
        /// 
        /// </summary>
        public regHeader_0(cTabelaTXT _cTabelaTXT, string Linha)
            : base(_cTabelaTXT)
        {
            CarregaLinha(Linha);
            Registro = 0;
            Transacoes = new List<BoletoRegistroBase>();
            // *** MRC - INICIO (27/05/2016 12:00) ***
            Transacoes2_Multa = new List<BoletoRegistroBase>();
            // *** MRC - TERMINO (27/05/2016 12:00) ***
        }

        // *** MRC - INICIO (27/05/2016 12:00) ***
        /// <summary>
        /// Grava Arquivo
        /// </summary>
        /// <param name="NomeArquivo"></param>
        /// <param name="Apend"></param>
        /// <param name="AdicionalMulta"></param>
        /// <returns></returns>
        //public bool GravaArquivo(string NomeArquivo, bool Apend)
        public bool GravaArquivo(string NomeArquivo, bool Apend, bool AdicionalMulta = false)
        // *** MRC - TERMINO (27/05/2016 12:00) ***
        {
            bool resultado = false;
            try
            {
                cTabTXT.IniciaGravacao(NomeArquivo, Apend);
                GravaLinha();
                // *** MRC - INICIO (27/05/2016 12:00) ***
                int i = 0;
                if (Transacoes != null)
                    foreach (BoletoRegistroBase trans in Transacoes)
                    {
                        trans.GravaLinha();
                        if (AdicionalMulta) Transacoes2_Multa[i].GravaLinha();
                        i++;
                    }
                // *** MRC - TERMINO (27/05/2016 12:00) ***
                if (Trailer != null)
                    Trailer.GravaLinha();
                resultado = true;
            }
            catch (Exception ex)
            {
                throw new ArgumentException(ex.Message, ex.InnerException);
            }
            finally
            {
                if (!Apend) 
                    cTabTXT.TerminaGravacao();
            }
            return resultado;
        }

        /// <summary>
        /// Finaliza Arquivo
        /// </summary>
        /// <param name="NomeArquivo"></param>
        /// <returns></returns>
        public bool FinalizaArquivo(string NomeArquivo)
        {
            bool resultado = false;
            try
            {
                cTabTXT.TerminaGravacao();
                resultado = true;
            }
            catch (Exception ex)
            {
                throw new ArgumentException(ex.Message, ex.InnerException);
            }
            return resultado;
        }

        /// <summary>
        /// 
        /// </summary>
        protected override void RegistraMapa()
        {
            base.RegistraMapa();
            RegistraFortementeTipado("CodigoRemRet"  ,   2,  1);
            RegistraFortementeTipado("LiteralArquivo",   3,  7);
            RegistraFortementeTipado("CodServico"    ,  10,  2);
            RegistraFortementeTipado("LiteralServico",  12, 15);
            RegistraFortementeTipado("NomeEmpresa"   ,  47, 30);
            RegistraFortementeTipado("NumeroBanco"   ,  77,  3);
            RegistraFortementeTipado("NomeBanco"     ,  80, 15);
            RegistraFortementeTipado("DataGeracao"   ,  95,  6);
        }
    }

    /// <summary>
    /// 
    /// </summary>
    public class regTransacao_1 : BoletoRegistroBase
    {
        #region Variaveis Tipadas

        /// <summary>
        /// 
        /// </summary>
        public string ControlePart;

        #endregion

        /// <summary>
        /// 
        /// </summary>
        public regTransacao_1(cTabelaTXT _cTabelaTXT, string Linha)
            : base(_cTabelaTXT)
        {
            Registro = 1;
            CarregaLinha(Linha);
        }

        /// <summary>
        /// 
        /// </summary>
        protected override void RegistraMapa()
        {
            base.RegistraMapa();
            RegistraFortementeTipado("ControlePart", 38, 25);
        }
    }

    // *** MRC - INICIO (27/05/2016 12:00) ***
    /// <summary>
    /// 
    /// </summary>
    public class regTransacao_2 : BoletoRegistroBase
    {
        #region Variaveis Tipadas

        #endregion

        /// <summary>
        /// 
        /// </summary>
        public regTransacao_2(cTabelaTXT _cTabelaTXT, string Linha)
            : base(_cTabelaTXT)
        {
            Registro = 2;
            CarregaLinha(Linha);
        }

        /// <summary>
        /// 
        /// </summary>
        protected override void RegistraMapa()
        {
            base.RegistraMapa();
        }
    }
    // *** MRC - TERMINO (27/05/2016 12:00) ***

    /// <summary>
    /// Objeto para rateio de crédito - Base
    /// </summary>
    public class regTransacao_3 : BoletoRegistroBase
    {
        #region Variaveis Tipadas

        #endregion

        /// <summary>
        /// 
        /// </summary>
        public regTransacao_3(cTabelaTXT _cTabelaTXT, string Linha)
            : base(_cTabelaTXT)
        {
            Registro = 3;
            CarregaLinha(Linha);
        }

        /// <summary>
        /// 
        /// </summary>
        protected override void RegistraMapa()
        {
            base.RegistraMapa();
        }
    }

    /// <summary>
    /// 
    /// </summary>
    public class regTrailer_9 : BoletoRegistroBase
    {
        /// <summary>
        /// 
        /// </summary>
        public regTrailer_9(cTabelaTXT _cTabelaTXT, string Linha)
            : base(_cTabelaTXT)
        {
            Registro = 9;
            CarregaLinha(Linha);
        }
    }
}