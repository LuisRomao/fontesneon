﻿/*
MR - 25/02/2016 11:00 -           - Padronização Arquivo de Cobrança Bradesco (Alterações indicadas por *** MRC - INICIO - REMESSA (25/02/2016 11:00) ***)
MR - 21/03/2016 20:00 -           - GravaArquivo permite usar um arquivo indicado nos parametros (Alterações indicadas por *** MRC - INICIO (21/03/2016 20:00) ***)
                                  - Inclusão do FinalizaArquivo

*/

using System;
using ArquivosTXT;
using BoletosCalc;
using DocBacarios;
using System.Collections.Generic;
using EDIBoletos.RegistrosOBJ;

namespace EDIBoletos
{
    /// <summary>
    /// Remessa Bradesco - Registro de boletos
    /// </summary>
    public class BoletoRemessaBra : BoletoRemessaBase
    {
        /// <summary>
        /// Header
        /// </summary>
        public regHeader_0_1_BRA Header;

        /// <summary>
        /// Trailer
        /// </summary>
        public regTrailer_9_1_BRA Trailer;

        #region Enumerações
        /// <summary>
        /// Emissor
        /// </summary>
        public enum TiposEmissaoBoleto
        {
            /// <summary>
            /// Banco
            /// </summary>
            Banco = 1,
            /// <summary>
            /// Cliente
            /// </summary>
            Cliente = 2
        }

        /// <summary>
        /// TiposAviso
        /// </summary>
        public enum TiposAvisoDebitoAutomatico
        {
            /// <summary>
            /// 
            /// </summary>
            AvisoEnderecoSacado = 1,
            /// <summary>
            /// 
            /// </summary>
            Desativado = 2,
            /// <summary>
            /// 
            /// </summary>
            AvisoEnderecoCadastroBanco = 3
        }

        /// <summary>
        /// Ocorrencia
        /// </summary>
        public enum TiposOcorrencia
        {
            /// <summary>
            /// Registrar o boleto
            /// </summary>
            Remessa = 1,
            /// <summary>
            /// Não usamos
            /// </summary>
            PedidoBaixa = 2,
            /// <summary>
            /// Concessao Abatimento
            /// </summary>
            ConcessaoAbatimento = 4,
            /// <summary>
            /// Não usamos
            /// </summary>
            CancelamentoAbatimentoConcedido = 5,
            /// <summary>
            /// Usamos o AlteracaoOutrosDados
            /// </summary>
            AlteracaoVencimento = 6,
            /// <summary>
            /// Não usamos
            /// </summary>
            AlteracaoControleParticipante = 7,
            /// <summary>
            /// Não usamos
            /// </summary>
            AlteracaoNumero = 8,
            /// <summary>
            /// Não usamos
            /// </summary>
            PedidoProtesto = 9,
            /// <summary>
            /// Não usamos
            /// </summary>
            SustarProtestoBaixarTitulo = 18,
            /// <summary>
            /// Não usamos
            /// </summary>
            SustarProtestoManterEmCarteira = 19,
            /// <summary>
            /// Não usamos
            /// </summary>
            TransferenciaCessaoCredito = 22,
            /// <summary>
            /// Não usamos
            /// </summary>
            TransferenciaEntreCarteiras = 23,
            /// <summary>
            /// Não usamos
            /// </summary>
            DevTransferenciaEntreCarteiras = 24,
            /// <summary>
            /// Altera os dados do Boleto registrado
            /// </summary>
            AlteracaoOutrosDados = 31,
            /// <summary>
            /// Não usamos
            /// </summary>
            DesagendamentoDebitoAutomatico = 35,
            /// <summary>
            /// Não usamos
            /// </summary>
            AcertoDadosRateioCredito = 68,
            /// <summary>
            /// Não usamos
            /// </summary>
            CancelamentoRateioCredito = 69
        }

        /// <summary>
        /// Tipos de Título
        /// </summary>
        public enum TiposEspecieTitulo
        {
            /// <summary>
            /// 
            /// </summary>
            Duplicata = 1,
            /// <summary>
            /// 
            /// </summary>
            NotaPromissoria = 2,
            /// <summary>
            /// 
            /// </summary>
            NotaSeguro = 3,
            /// <summary>
            /// 
            /// </summary>
            CobrancaSeriada = 4,
            /// <summary>
            /// 
            /// </summary>
            Recibo = 5,
            /// <summary>
            /// 
            /// </summary>
            LetrasCambio = 10,
            /// <summary>
            /// 
            /// </summary>
            NotaDebito = 11,
            /// <summary>
            /// 
            /// </summary>
            DuplicataServico = 12,
            /// <summary>
            /// 
            /// </summary>
            Outros = 99,
        }

        /// <summary>
        /// 
        /// </summary>
        public enum TiposInstrucao
        {
            /// <summary>
            /// 
            /// </summary>
            Protestar = 6,
            /// <summary>
            /// 
            /// </summary>
            NaoCobrarMora = 8,
            /// <summary>
            /// 
            /// </summary>
            NaoReceberAposVencimento = 9,
            /// <summary>
            /// 
            /// </summary>
            Multa10PorcentoApos4DiasVencimento = 10,
            /// <summary>
            /// 
            /// </summary>
            NaoReceberApos8DiasVencimento = 11,
            /// <summary>
            /// 
            /// </summary>
            CobrarEncargosApos5DiasVencimento = 12,
            /// <summary>
            /// 
            /// </summary>
            CobrarEncargosApos10DiasVencimento = 13,
            /// <summary>
            /// 
            /// </summary>
            CobrarEncargosApos15DiasVencimento = 14,
            /// <summary>
            /// 
            /// </summary>
            ConcederDescontoSempre = 15,
            /// <summary>
            /// 
            /// </summary>
            BaixaDecursoPrazo = 18
        }

        /// <summary>
        /// 
        /// </summary>
        public enum TiposInscricao
        {
            /// <summary>
            /// 
            /// </summary>
            CPF = 1,
            /// <summary>
            /// 
            /// </summary>
            CNPJ = 2,
            /// <summary>
            /// 
            /// </summary>
            PISPASEP = 3,
            /// <summary>
            /// 
            /// </summary>
            NaoTem = 98,
            /// <summary>
            /// 
            /// </summary>
            Outros = 99
        } 
        #endregion

        /*
        public override void IniciaRemessa()
        {
            
        }*/

        /// <summary>
        /// 
        /// </summary>
        public void InsereBoletos()
        {
            foreach (BoletoReg boleto in Boletos)
            {
                regTransacao_1_1_BRA regTransacao1 = new regTransacao_1_1_BRA(Header);
                regTransacao1.Carteira = Carteira;
                regTransacao1.IDEmpresa = string.Format("0{0}{1}{2}{3}", Carteira.PadLeft(3, '0'), boleto.Agencia.PadLeft(5, '0'), boleto.Conta.PadLeft(7, '0'), boleto.DigConta.PadLeft(1, '0'));
                regTransacao1.DAAgencia = boleto.DAAgencia;
                regTransacao1.DADigAgencia = boleto.DADigAgencia;
                regTransacao1.DARazaoConta = boleto.DARazaoConta;
                regTransacao1.DANumConta = boleto.DANumConta;
                regTransacao1.DADigNumConta = boleto.DADigNumConta;
                regTransacao1.ControlePart = boleto.ControlePart;
                regTransacao1.CobrarMulta = boleto.CobrarMulta;
                regTransacao1.PercMulta = boleto.PercMulta;
                regTransacao1.NossoNumero = boleto.NossoNumero;
                regTransacao1.DescontoBonDia = boleto.DescontoBonDia;
                regTransacao1.EmissaoBoleto = boleto.EmissaoBoleto;
                regTransacao1.RegistrarDA = boleto.RegistrarDA;
                regTransacao1.AvisoDA = boleto.AvisoDA;
                regTransacao1.RateioContratado = boleto.RateioContratado;
                regTransacao1.IDOcorrencia = boleto.IDOcorrencia;
                regTransacao1.NumDocumento = boleto.NumDocumento;
                regTransacao1.Vencimento = boleto.Vencimento;
                regTransacao1.Valor = boleto.Valor;
                regTransacao1.EspecieTitulo = boleto.EspecieTitulo;
                regTransacao1.Emissao = boleto.Emissao;
                regTransacao1.Instrucao1 = boleto.Instrucao1;
                regTransacao1.Instrucao2 = boleto.Instrucao2;
                regTransacao1.Mora = boleto.Mora;
                regTransacao1.LimDesconto = boleto.LimDesconto;
                regTransacao1.Desconto = boleto.Desconto;
                regTransacao1.IOF = boleto.IOF;
                regTransacao1.Abatimento = boleto.Abatimento;
                regTransacao1.CPF = boleto.CPF;
                regTransacao1.Nome = boleto.NomeSacado;
                regTransacao1.Endereco = boleto.EnderecoSacado;
                regTransacao1.Mensagem1 = boleto.Mensagem1;
                regTransacao1.CEP = boleto.CEPSacado;
                regTransacao1.Mensagem2 = boleto.Mensagem2;
                if (boleto.RateioContratado)
                {
                    regTransacao_3_1_BRA reg = null;
                    for (int i = 1; i <= boleto.RateioCreditos.Count; i++)
                    {                                                    
                        switch (i % 3)
                        {
                            case 1:
                                reg = new regTransacao_3_1_BRA(regTransacao1);
                                reg.AgRat1 = boleto.RateioCreditos[i - 1].AgRat;
                                reg.DgAgRat1 = boleto.RateioCreditos[i - 1].DgAgRat;
                                reg.ContaRat1 = boleto.RateioCreditos[i - 1].ContaRat;
                                reg.DgContaRat1 = boleto.RateioCreditos[i - 1].DgContaRat;
                                reg.ValRat1 = boleto.RateioCreditos[i - 1].ValRat;
                                reg.NomeRat1 = boleto.RateioCreditos[i - 1].NomeRat;
                                break;
                            case 2:
                                reg.AgRat2 = boleto.RateioCreditos[i - 1].AgRat;
                                reg.DgAgRat2 = boleto.RateioCreditos[i - 1].DgAgRat;
                                reg.ContaRat2 = boleto.RateioCreditos[i - 1].ContaRat;
                                reg.DgContaRat2 = boleto.RateioCreditos[i - 1].DgContaRat;
                                reg.ValRat2 = boleto.RateioCreditos[i - 1].ValRat;
                                reg.NomeRat2 = boleto.RateioCreditos[i - 1].NomeRat;
                                break;
                            case 0:
                                reg.AgRat3 = boleto.RateioCreditos[i - 1].AgRat;
                                reg.DgAgRat3 = boleto.RateioCreditos[i - 1].DgAgRat;
                                reg.ContaRat3 = boleto.RateioCreditos[i - 1].ContaRat;
                                reg.DgContaRat3 = boleto.RateioCreditos[i - 1].DgContaRat;
                                reg.ValRat3 = boleto.RateioCreditos[i - 1].ValRat;
                                reg.NomeRat3 = boleto.RateioCreditos[i - 1].NomeRat;
                                break;
                        }                                                                            
                    }
                };
            }
        }

        //*** MRC - INICIO - REMESSA (25/02/2016 11:00) ***
        /// <summary>
        /// 
        /// </summary>
        public void CriaHeader()
        {
            Header = new regHeader_0_1_BRA(ArquivoSaida);                        
            Header.CodigoRemRet = 1;
            Header.LiteralArquivo = "REMESSA";
            Header.CodServico = 1;
            Header.LiteralServico = "COBRANCA";
            Header.CodEmpresa = CodEmpresa;
            Header.NomeEmpresa = NomeEmpresa;
            Header.DataGeracao = DateTime.Today.ToString("ddMMyy");
            Header.SequencialRem = SequencialRemessa;
            Header.Sequencial = 1;
        }
        //*** MRC - TERMINO - REMESSA (25/02/2016 11:00) ***

        /// <summary>
        /// 
        /// </summary>
        public void CriaTrailer()
        {
            Trailer = new regTrailer_9_1_BRA(Header);
        }

        /// <summary>
        /// Prefixo Arquivo
        /// </summary>
        public override string PrefixoArquivo
        {
            get
            {
                return "COBN";
            }
        }

        //*** MRC - INICIO (21/03/2016 20:00) ***      
        /// <summary>
        /// Grava o arquivo formato BRA (efetivo)
        /// </summary>
        //public override string GravaArquivo(string Path)
        public override string GravaArquivo(string Path, string Arquivo = "")
        {
            try
            {                
                string NomeArquivo = Arquivo;
                if (NomeArquivo == "")
                    NomeArquivo = string.Format("{0}{1}{2:ddMM}{3:00}.REM", Path, PrefixoArquivo, DateTime.Today, SequencialRemessaArquivo);
        //*** MRC - TERMINO (21/03/2016 20:00) ***      
                CriaHeader();
                if (Header == null)
                    return "";
                else
                {
                    InsereBoletos();
                    CriaTrailer();
                    if (Header.GravaArquivo(NomeArquivo, false))
                        return NomeArquivo;
                    else
                        return "";
                }
            }
            catch (Exception ex)
            {
                UltimoErro = ex;
                return "";
            }
        }

        //*** MRC - INICIO (21/03/2016 20:00) ***      
        /// <summary>
        /// Finaliza o Arquivo
        /// </summary>
        /// <param name="NomeArquivo"></param>
        /// <returns></returns>
        public override string FinalizaArquivo(string NomeArquivo)
        {
            try
            {
                if (Header.FinalizaArquivo(NomeArquivo))
                    return NomeArquivo;
                else
                    return "";
            }
            catch (Exception ex)
            {
                UltimoErro = ex;
                return "";
            }
        }
        //*** MRC - TERMINO (21/03/2016 20:00) ***      
    }

    //*** MRC - INICIO - REMESSA (25/02/2016 11:00) ***
    /// <summary>
    /// 
    /// </summary>
    public class regHeader_0_1_BRA : regHeader_0
    {
        #region Variaveis Tipadas

        /// <summary>
        /// Codigo da Empresa (somente Bradesco)
        /// </summary>
        public int CodEmpresa;

        /// <summary>
        /// 
        /// </summary>
        public string IDSistema;
        /// <summary>
        /// 
        /// </summary>
        public int SequencialRem;

        #endregion

        /// <summary>
        /// 
        /// </summary>
        public regHeader_0_1_BRA(cTabelaTXT _cTabelaTXT)
            : base(_cTabelaTXT, null)
        {            
            NumeroBanco = 237;
            NomeBanco = "BRADESCO";
            IDSistema = "MX";
        }

        /// <summary>
        /// 
        /// </summary>
        protected override void RegistraMapa()
        {
            base.RegistraMapa();
            RegistraFortementeTipado("CodEmpresa"   ,  27, 20);
            RegistraFortementeTipado("IDSistema"    , 109,  2);
            RegistraFortementeTipado("SequencialRem", 111,  7);
        }
    }
    //*** MRC - TERMINO - REMESSA (25/02/2016 11:00) ***

    /// <summary>
    /// Ojeto que representa uma linha trasaçao 1 - boleto
    /// </summary>
    public class regTransacao_1_1_BRA : regTransacao_1
    {
        #region Variaveis Tipadas

        /// <summary>
        /// 
        /// </summary>
        public string Carteira;

        private string _DAAgencia;
        /// <summary>
        /// 
        /// </summary>
        public string DAAgencia
        {
            get { return (_DAAgencia == null) ? "" : _DAAgencia.PadLeft(5, '0'); }
            set { _DAAgencia = value; }
        }

        private string _DADigAgencia;
        /// <summary>
        /// 
        /// </summary>
        public string DADigAgencia
        {
            get { return _DADigAgencia ?? ""; }
            set { _DADigAgencia = value; }
        }

        /// <summary>
        /// 
        /// </summary>
        public int DARazaoConta;

        private string _DANumConta;
        /// <summary>
        /// 
        /// </summary>
        public string DANumConta
        {
            get { return (_DANumConta == null) ? "" : _DANumConta.PadLeft(7, '0'); }
            set { _DANumConta = value; }
        }

        private string _DADigNumConta;
        /// <summary>
        /// 
        /// </summary>
        public string DADigNumConta
        {
            get { return _DADigNumConta ?? ""; }
            set { _DADigNumConta = value; }
        }

        //*** MRC - INICIO - REMESSA (25/02/2016 11:00) ***
        /// <summary>
        /// 
        /// </summary>
        public string IDEmpresa;
        //*** MRC - TERMINO - REMESSA (25/02/2016 11:00) ***

        
        /// <summary>
        /// 
        /// </summary>
        public int DANumBanco
        {
            get { return (_DANumConta == null) ? 0 : 237; }            
        }

        /// <summary>
        /// 
        /// </summary>
        public bool CobrarMulta;

        private int _CampoMulta;
        /// <summary>
        /// 
        /// </summary>
        public int CampoMulta
        {
            get { return CobrarMulta ? 2 : 0; }
            set { _CampoMulta = value; }
        }

        private decimal _PercMulta;
        /// <summary>
        /// 
        /// </summary>
        public decimal PercMulta
        {
            get { return CobrarMulta ? _PercMulta : 0; }
            set { _PercMulta = value; }
        }

        /// <summary>
        /// 
        /// </summary>
        public int NossoNumero;

        private string _DigNossoNumero;
        /// <summary>
        /// 
        /// </summary>
        public string DigNossoNumero
        {
            get { return NossoNumero == 0 ? "0" : BoletoCalc.DigitoBradesco(Carteira + NossoNumero.ToString().PadLeft(11, '0')); }
            set { _DigNossoNumero = value; }
        }

        /// <summary>
        /// 
        /// </summary>
        public decimal DescontoBonDia;

        /// <summary>
        /// 
        /// </summary>
        public int EmissaoBoleto;

        /// <summary>
        /// 
        /// </summary>
        public bool RegistrarDA;
        private string _RegistroDA;
        /// <summary>
        /// 
        /// </summary>
        public string RegistroDA
        {
            get { return RegistrarDA ? "S" : "N"; }
            set { _RegistroDA = value; }
        }

        /// <summary>
        /// 
        /// </summary>
        public bool RateioContratado;

        private string _IndicadorRateio;
        /// <summary>
        /// 
        /// </summary>
        public string IndicadorRateio
        {
            get { return _IndicadorRateio ?? ((RateioContratado) ? "R" : ""); }
            set { _IndicadorRateio = value; }
        }

        private int _AvisoDA;
        /// <summary>
        /// 
        /// </summary>
        public int AvisoDA
        {
            get { return (_AvisoDA == 0) ? (int)BoletoRemessaBra.TiposAvisoDebitoAutomatico.Desativado : _AvisoDA; }
            set { _AvisoDA = value; }
        }

        private int _IDOcorrencia;
        /// <summary>
        /// 
        /// </summary>
        public int IDOcorrencia
        {
            get { return (_IDOcorrencia == 0) ? (int)BoletoRemessaBra.TiposOcorrencia.Remessa : _IDOcorrencia; }
            set { _IDOcorrencia = value; }
        }

        /// <summary>
        /// 
        /// </summary>
        public string NumDocumento;

        /// <summary>
        /// 
        /// </summary>
        public DateTime Vencimento;
        private string _DataVencimento;
        /// <summary>
        /// 
        /// </summary>
        public string DataVencimento
        {
            get
            {
                return ((Vencimento.Year == 1) || (_DataVencimento == "")) ? "000000" : Vencimento.Day.ToString("00") + Vencimento.Month.ToString("00") + Vencimento.Year.ToString().Substring(2, 2);
            }
            set { _DataVencimento = value; }
        }

        /// <summary>
        /// 
        /// </summary>
        public decimal Valor;

        private int _EspecieTitulo;
        /// <summary>
        /// 
        /// </summary>
        public int EspecieTitulo
        {
            get { return (_EspecieTitulo == 0) ? (int)BoletoRemessaBra.TiposEspecieTitulo.Duplicata : _EspecieTitulo; }
            set { _EspecieTitulo = value; }
        }

        /// <summary>
        /// 
        /// </summary>
        public DateTime Emissao;
        private string _DataEmissao;
        /// <summary>
        /// 
        /// </summary>
        public string DataEmissao
        {
            get
            {
                return ((Emissao.Year == 1) || (_DataEmissao == "")) ? "000000" : Emissao.Day.ToString("00") + Emissao.Month.ToString("00") + Emissao.Year.ToString().Substring(2, 2);
            }
            set { _DataEmissao = value; }
        }

        /// <summary>
        /// 
        /// </summary>
        public int Instrucao1;

        /// <summary>
        /// 
        /// </summary>
        public int Instrucao2;

        /// <summary>
        /// 
        /// </summary>
        public decimal Mora;

        /// <summary>
        /// 
        /// </summary>
        public DateTime LimDesconto;
        private string _DataLimDesconto;
        /// <summary>
        /// 
        /// </summary>
        public string DataLimDesconto
        {
            get
            {
                return ((LimDesconto.Year == 1) || (_DataLimDesconto == "")) ? "000000" : LimDesconto.Day.ToString("00") + LimDesconto.Month.ToString("00") + LimDesconto.Year.ToString().Substring(2, 2);
            }
            set { _DataLimDesconto = value; }
        }

        /// <summary>
        /// 
        /// </summary>
        public decimal Desconto;

        /// <summary>
        /// 
        /// </summary>
        public decimal IOF;

        /// <summary>
        /// 
        /// </summary>
        public decimal Abatimento;

        private CPFCNPJ _CPF;
        /// <summary>
        /// 
        /// </summary>
        public CPFCNPJ CPF
        {
            get { return _CPF; }
            set { _CPF = value; }
        }

        /// <summary>
        /// 
        /// </summary>
        public int TipoInscricao
        {
            get
            {
                if (CPF == null) { return (int)BoletoRemessaBra.TiposInscricao.NaoTem; }
                else
                {
                    switch (CPF.Tipo)
                    {
                        case TipoCpfCnpj.CNPJ: return (int)BoletoRemessaBra.TiposInscricao.CNPJ;
                        case TipoCpfCnpj.CPF: return (int)BoletoRemessaBra.TiposInscricao.CPF;
                        case TipoCpfCnpj.INVALIDO: return (int)BoletoRemessaBra.TiposInscricao.NaoTem;
                        default: return (int)BoletoRemessaBra.TiposInscricao.NaoTem;
                    }
                }
            }
        }

        /// <summary>
        /// 
        /// </summary>
        public Int64 NumInscricao
        {
            get
            {
                if (CPF == null) { return 0; }
                else { return CPF.Tipo == TipoCpfCnpj.INVALIDO ? 0 : CPF.Valor; }
            }
        }

        private string _Nome;
        /// <summary>
        /// 
        /// </summary>
        public string Nome
        {
            get { return (_Nome == null) ? "" : _Nome.ToUpper(); }
            set { _Nome = value; }
        }

        private string _Endereco;
        /// <summary>
        /// 
        /// </summary>
        public string Endereco
        {
            get { return (_Endereco == null) ? "" : _Endereco.ToUpper(); }
            set { _Endereco = value; }
        }

        private string _Mensagem1;
        /// <summary>
        /// 
        /// </summary>
        public string Mensagem1
        {
            get { return (_Mensagem1 == null) ? "" : _Mensagem1.ToUpper(); }
            set { _Mensagem1 = value; }
        }

        private string _CEP;
        /// <summary>
        /// 
        /// </summary>
        public string CEP
        {
            get
            {
                long lngCep = 0;
                if (_CEP != null) { _CEP = _CEP.Replace("-", ""); }
                return (long.TryParse(_CEP, out lngCep)) ? _CEP.PadLeft(8, '0') : "00000000";
            }
            set { _CEP = value; }
        }

        private string _Mensagem2;
        /// <summary>
        /// 
        /// </summary>
        public string Mensagem2
        {
            get { return (_Mensagem2 == null) ? "" : _Mensagem2.ToUpper(); }
            set { _Mensagem2 = value; }
        }

        #endregion

        internal regHeader_0_1_BRA regPai;
        internal List<regTransacao_3_1_BRA> rateios;

        /// <summary>
        /// 
        /// </summary>
        public regTransacao_1_1_BRA(regHeader_0_1_BRA _regPai)
            : base(_regPai.cTabTXT, null)
        {
            regPai = _regPai;
            regPai.Transacoes.Add(this);
            Sequencial = regPai.Transacoes.Count + 1;
            rateios = new List<regTransacao_3_1_BRA>();
        }

        /// <summary>
        /// Faz o mapeamento
        /// </summary>
        protected override void RegistraMapa()
        {
            base.RegistraMapa();
            //*** MRC - INICIO - REMESSA (25/02/2016 11:00) ***
            RegistraFortementeTipado("DAAgencia"      ,   2,  5);
            RegistraFortementeTipado("DADigAgencia"   ,   7,  1);
            RegistraFortementeTipado("DARazaoConta"   ,   8,  5);
            RegistraFortementeTipado("DANumConta"     ,  13,  7);
            RegistraFortementeTipado("DADigNumConta"  ,  20,  1);
            RegistraFortementeTipado("IDEmpresa"      ,  21, 17);
            RegistraFortementeTipado("DANumBanco"     ,  63,  3);
            RegistraFortementeTipado("CampoMulta"     ,  66,  1);
            RegistraFortementeTipado("PercMulta"      ,  67,  4);
            RegistraFortementeTipado("NossoNumero"    ,  71, 11);
            RegistraFortementeTipado("DigNossoNumero" ,  82,  1);
            RegistraFortementeTipado("DescontoBonDia" ,  83, 10);
            RegistraFortementeTipado("EmissaoBoleto"  ,  93,  1);
            RegistraFortementeTipado("RegistroDA"     ,  94,  1);
            RegistraFortementeTipado("IndicadorRateio", 105,  1);
            RegistraFortementeTipado("AvisoDA"        , 106,  1);
            RegistraFortementeTipado("IDOcorrencia"   , 109,  2);
            RegistraFortementeTipado("NumDocumento"   , 111, 10);
            RegistraFortementeTipado("DataVencimento" , 121,  6);
            RegistraFortementeTipado("Valor"          , 127, 13);
            RegistraMapa            ("BcoCobranca"    , 140,  3, 0);
            RegistraMapa            ("AgDepositaria"  , 143,  5, 0);
            RegistraFortementeTipado("EspecieTitulo"  , 148,  2);
            RegistraMapa            ("IDAceite"       , 150, 1, "N");
            RegistraFortementeTipado("DataEmissao"    , 151,  6);
            RegistraFortementeTipado("Instrucao1"     , 157,  2);
            RegistraFortementeTipado("Instrucao2"     , 159,  2);
            RegistraFortementeTipado("Mora"           , 161, 13);
            RegistraFortementeTipado("DataLimDesconto", 174,  6);
            RegistraFortementeTipado("Desconto"       , 180, 13);
            RegistraFortementeTipado("IOF"            , 193, 13);
            RegistraFortementeTipado("Abatimento"     , 206, 13);
            RegistraFortementeTipado("TipoInscricao"  , 219,  2);
            RegistraFortementeTipado("NumInscricao"   , 221, 14);
            RegistraFortementeTipado("Nome"           , 235, 40);
            RegistraFortementeTipado("Endereco"       , 275, 40);
            RegistraFortementeTipado("Mensagem1"      , 315, 12);
            RegistraFortementeTipado("CEP"            , 327,  8);
            RegistraFortementeTipado("Mensagem2"      , 335, 60);
            //*** MRC - TERMINO - REMESSA (25/02/2016 11:00) ***
        }
    }

    /// <summary>
    /// Objeto que represnta o rateiro de crédito
    /// </summary>
    public class regTransacao_3_1_BRA:regTransacao_3
    {

        #region Variaveis Tipadas

        /// <summary>
        /// Identificação de empresa
        /// </summary>
        public string IDEmpresa { get { return regT1Pai == null ? "" : regT1Pai.IDEmpresa.Substring(1, 16); } }

        /// <summary>
        /// Nosso Numero 
        /// </summary>
        public int NossoNumero { get { return regT1Pai == null ? 0 : regT1Pai.NossoNumero; } }

        /// <summary>
        /// Dígito NossoNumero
        /// </summary>
        public string DigNossoNumero { get { return regT1Pai == null ? "" : regT1Pai.DigNossoNumero; } }

        /// <summary>
        /// Agencia Rateio
        /// </summary>
        public int AgRat1;
        /// <summary>
        /// Digito Agencia Rateio
        /// </summary>
        public string DgAgRat1;
        /// <summary>
        /// Conta Rateio
        /// </summary>
        public int ContaRat1;
        /// <summary>
        /// Digito conta rateio
        /// </summary>
        public string DgContaRat1;
        /// <summary>
        /// Valor ratear
        /// </summary>
        public decimal ValRat1;
        /// <summary>
        /// Favorecido rateio
        /// </summary>
        public string NomeRat1;

        /// <summary>
        /// Agencia Rateio
        /// </summary>
        public int AgRat2;
        /// <summary>
        /// Digito Agencia Rateio
        /// </summary>
        public string DgAgRat2;
        /// <summary>
        /// Conta Rateio
        /// </summary>
        public int ContaRat2;
        /// <summary>
        /// Digito conta rateio
        /// </summary>
        public string DgContaRat2;
        /// <summary>
        /// Valor ratear
        /// </summary>
        public decimal ValRat2;
        /// <summary>
        /// Favorecido rateio
        /// </summary>
        public string NomeRat2;

        /// <summary>
        /// Agencia Rateio
        /// </summary>
        public int AgRat3;
        /// <summary>
        /// Digito Agencia Rateio
        /// </summary>
        public string DgAgRat3;
        /// <summary>
        /// Conta Rateio
        /// </summary>
        public int ContaRat3;
        /// <summary>
        /// Digito conta rateio
        /// </summary>
        public string DgContaRat3;
        /// <summary>
        /// Valor ratear
        /// </summary>
        public decimal ValRat3;
        /// <summary>
        /// Favorecido rateio
        /// </summary>
        public string NomeRat3;

        #endregion

        private regTransacao_1_1_BRA regT1Pai;

        /// <summary>
        /// Construitor
        /// </summary>
        public regTransacao_3_1_BRA(regTransacao_1_1_BRA _regT1Pai) 
            : base(_regT1Pai.regPai.cTabTXT, null)
        {
            regT1Pai = _regT1Pai;
            regT1Pai.regPai.Transacoes.Add(this);
            regT1Pai.rateios.Add(this);
            Sequencial = regT1Pai.regPai.Transacoes.Count + 1;
        }

        /// <summary>
        /// Registra mapa
        /// </summary>
        protected override void RegistraMapa()
        {
            base.RegistraMapa();
            RegistraFortementeTipado("IDEmpresa"     ,  2, 16);
            RegistraFortementeTipado("NossoNumero"   , 18, 11);
            RegistraFortementeTipado("DigNossoNumero", 29,  1);
            RegistraMapa            ("ConfiguraRat"  , 30,  2, "22");
            RegistraMapa            ("Banco1"        , 44,  3, 237);
            RegistraFortementeTipado("AgRat1"        , 47,  5);
            RegistraFortementeTipado("DgAgRat1"      , 52,  1);
            RegistraFortementeTipado("ContaRat1"     , 53, 12);
            RegistraFortementeTipado("DgContaRat1"   , 65,  1);
            RegistraFortementeTipado("ValRat1"       , 66, 15);
            RegistraFortementeTipado("NomeRat1"      , 81, 40);
            RegistraMapa            ("ParcRat1"      ,152,  6, "");
            RegistraMapa            ("DiasRat1"      ,158,  3, 0);
            RegistraMapa            ("Banco2"        ,161,  3, 237);
            RegistraFortementeTipado("AgRat2"        ,164,  5);
            RegistraFortementeTipado("DgAgRat2"      ,169,  1);
            RegistraFortementeTipado("ContaRat2"     ,170, 12);
            RegistraFortementeTipado("DgContaRat2"   ,182,  1);
            RegistraFortementeTipado("ValRat2"       ,183, 15);
            RegistraFortementeTipado("NomeRat2"      ,198, 40);
            RegistraMapa            ("ParcRat2"      ,269, 6, "");
            RegistraMapa            ("DiasRat2"      ,275, 3, 1);
            RegistraMapa            ("Banco3"        ,278, 3, 237);
            RegistraFortementeTipado("AgRat3"        ,281, 5);
            RegistraFortementeTipado("DgAgRat3"      ,286, 1);
            RegistraFortementeTipado("ContaRat3"     ,287, 12);
            RegistraFortementeTipado("DgContaRat3"   ,299, 1);
            RegistraFortementeTipado("ValRat3"       ,300, 15);
            RegistraFortementeTipado("NomeRat3"      ,315, 40);
            RegistraMapa(            "ParcRat3"      ,386, 6, "");
            RegistraMapa(            "DiasRat3"      ,392, 3, 1);
        }
    }

    /// <summary>
    /// Objeto que representa a linha Trailer
    /// </summary>
    public class regTrailer_9_1_BRA : regTrailer_9
    {
        private regHeader_0_1_BRA regPai;

        /// <summary>
        /// 
        /// </summary>
        public regTrailer_9_1_BRA(regHeader_0_1_BRA _regPai)
            : base(_regPai.cTabTXT, null)
        {
            regPai = _regPai;
            regPai.Trailer = this;
            Sequencial = regPai.Transacoes.Count + 2;
        }
    }
}
