﻿/*
MR - 12/11/2014 10:00 -           - Ajuste no campo IDDocumento para ser string porém preenchida com zeros à esquerda (Alterações indicadas por *** MRC - INICIO - TRIBUTOS (12/11/2014 10:00) ***)
*/

using System;
using System.Collections.Generic;
using System.Text;
using System.Collections;
using ArquivosTXT;
using DocBacarios;

namespace EDIBoletos.RegistrosOBJTributo
{
   public class TributoRegistroBase: RegistroBase
   {
      public Exception UltimoErro;

      #region Propriedades tipadas

         public int Registro;
         public int CodComunicacao;
         public int SequencialRemessa;
         public String DataGeracao;
         public String HoraGeracao;
         public string CodConsistenciaArq;
         public int Sequencial;

      #endregion

      public TributoRegistroBase()
         : base()
      {
      }

      public TributoRegistroBase(cTabelaTXT _cTabelaTXT, string Linha)
         : base(_cTabelaTXT, Linha)
      {
      }

      public enum TiposTipoTributo
      {
          DARF = 1,
          GPS = 5,
          CodigoBarras = 6
      }

      public enum TiposInscricao
      {
         CPF = 1,
         CNPJ = 2,
         Outros = 3
      }

      public enum TiposCodOrigemArq
      {
          Remessa = 1,
          RetornoConsistencias = 2,
          RetornoFinal = 3
      }

      public enum TiposConsistenciaArq
      {
          ArquivoAceitoTotalmente = 1,
          ArquivoAceitoParcialmente = 2,
          ArquivoRejeitado = 3
      }

      public enum TiposTipoDarf
      {
          Simples = 1,
          PretoEuropa = 2
      }

      protected override void RegistraMapa()
      {
         RegistraFortementeTipado("Registro", 1, 1);
         RegistraFortementeTipado("CodComunicacao", 2, 8);
         RegistraFortementeTipado("SequencialRemessa", 25, 5);
         RegistraFortementeTipado("DataGeracao", 30, 8);
         RegistraFortementeTipado("CodConsistenciaArq", 900, 10);
         RegistraFortementeTipado("Sequencial", 995, 6);
      }
   }

   public class regHeader_0 : TributoRegistroBase
   {
      #region Variaveis Tipadas

         private string _NomeEmpresa;
         public string NomeEmpresa
         {
            get { return (_NomeEmpresa == null) ? "" : _NomeEmpresa.ToUpper(); }
            set { _NomeEmpresa = value; }
         }

         public int CodOrigemArq;

      #endregion

      public List<regTransacao_1> Transacoes;
      public regTrailer_9 Trailer;

      public regHeader_0(cTabelaTXT _cTabelaTXT, string Linha)
         : base(_cTabelaTXT, Linha)
      {
         Registro = 0;
         Transacoes = new List<regTransacao_1>();
      }

      public bool GravaArquivo(string NomeArquivo)
      {
         bool resultado = false;
         try
         {
            cTabTXT.IniciaGravacao(NomeArquivo, true);
            GravaLinha();
            if (Transacoes != null)
               foreach (regTransacao_1 trans in Transacoes)
                  trans.GravaLinha();
            if (Trailer != null)
               Trailer.GravaLinha();
            resultado = true;
         }
         catch(Exception ex)
         {
            throw new ArgumentException(ex.Message, ex.InnerException);
         }
         return resultado;
      }

      public bool FinalizaArquivo(string NomeArquivo)
      {
          bool resultado = false;
          try
          {
              cTabTXT.TerminaGravacao(); 
              resultado = true;
          }
          catch (Exception ex)
          {
              throw new ArgumentException(ex.Message, ex.InnerException);
          }
          return resultado;
      }

      protected override void RegistraMapa()
      {
         base.RegistraMapa();
         RegistraFortementeTipado("TipoInscricao", 38, 1);
         RegistraFortementeTipado("NomeEmpresa", 39, 40);
         RegistraFortementeTipado("CodOrigemArq", 79, 1);
         RegistraFortementeTipado("HoraGeracao", 80, 6);
      }
   }

   public class regTransacao_1 : TributoRegistroBase
   {
      #region Variaveis Tipadas

         public long NumInscricao;
         public int TipoTributo;

         //*** MRC - INICIO - TRIBUTOS (12/11/2014 10:00) ***
         private string _IDDocumento;
         public string IDDocumento
         {
             get { return (_IDDocumento == null) ? "00000000000000000000000000" : _IDDocumento.PadLeft(26, '0'); }
             set { _IDDocumento = value; }
         }
         //*** MRC - TERMINO - TRIBUTOS (12/11/2014 10:00) ***

         public string TipoMovimento;

         private string _UsoEmpresa;
         public string UsoEmpresa
         {
            get { return (_UsoEmpresa == null) ? "" : _UsoEmpresa.ToUpper(); }
            set { _UsoEmpresa = value; }
         }
                                                      
      #endregion
         
      public regTransacao_1(cTabelaTXT _cTabelaTXT, string Linha)
         : base(_cTabelaTXT, Linha)
      {
         Registro = 1;
      }

      protected override void RegistraMapa()
      {
         base.RegistraMapa();
         RegistraFortementeTipado("TipoTributo", 38, 3);
         RegistraFortementeTipado("IDDocumento", 41, 26);
         RegistraFortementeTipado("TipoMovimento", 67, 1);
         RegistraFortementeTipado("UsoEmpresa", 952, 16);
      }
   }

   public class regTransacao_1_TRIBUTO_001 : regTransacao_1
   {
       #region Variaveis Tipadas

       private string _NomeContribuinte;
       public string NomeContribuinte
       {
           get { return (_NomeContribuinte == null) ? "" : _NomeContribuinte.ToUpper(); }
           set { _NomeContribuinte = value; }
       }

       private string _EnderecoContribuinte;
       public string EnderecoContribuinte
       {
           get { return (_EnderecoContribuinte == null) ? "" : _EnderecoContribuinte.ToUpper(); }
           set { _EnderecoContribuinte = value; }
       }

       private string _CepContribuinte;
       public string CepContribuinte
       {
           get
           {
               long lngCep = 0;
               if (_CepContribuinte != null) { _CepContribuinte = _CepContribuinte.Replace("-", ""); }
               return (long.TryParse(_CepContribuinte, out lngCep)) ? _CepContribuinte.PadLeft(8, '0') : "00000000";
           }
           set { _CepContribuinte = value; }
       }

       public int DAAgencia;
       public string DADigAgencia;
       public int DARazaoConta;
       public int DANumConta;
       public string DADigNumConta;
       public string AutorizacaoDebito;
       public decimal Valor;
       public decimal Juros;
       public decimal Multa;
       public int CodReceita;

       #endregion

       public regTransacao_1_TRIBUTO_001(cTabelaTXT _cTabelaTXT, string Linha)
           : base(_cTabelaTXT, Linha)
       {
       }

       protected override void RegistraMapa()
       {
           base.RegistraMapa();
           RegistraFortementeTipado("TipoInscricaoContribuinte", 68, 1);
           RegistraFortementeTipado("NumInscricaoContribuinte", 69, 15);
           RegistraFortementeTipado("NomeContribuinte", 84, 40);
           RegistraFortementeTipado("EnderecoContribuinte", 124, 40);
           RegistraFortementeTipado("CepContribuinte", 164, 8);
           RegistraFortementeTipado("DAAgencia", 172, 4);
           RegistraFortementeTipado("DADigAgencia", 176, 1);
           RegistraFortementeTipado("DARazaoConta", 177, 4);
           RegistraFortementeTipado("DANumConta", 181, 7);
           RegistraFortementeTipado("DADigNumConta", 188, 1);
           RegistraFortementeTipado("AutorizacaoDebito", 197, 1);
           RegistraFortementeTipado("Valor", 198, 15);
           RegistraFortementeTipado("Juros", 213, 15);
           RegistraFortementeTipado("Multa", 228, 15);
           RegistraFortementeTipado("ValorTotal", 243, 15);
           RegistraFortementeTipado("CodReceita", 344, 4);
           RegistraFortementeTipado("TipoDarf", 348, 1);
       }
   }

   public class regTransacao_1_TRIBUTO_005 : regTransacao_1
   {
       #region Variaveis Tipadas

       private string _NomeContribuinte;
       public string NomeContribuinte
       {
           get { return (_NomeContribuinte == null) ? "" : _NomeContribuinte.ToUpper(); }
           set { _NomeContribuinte = value; }
       }

       private string _EnderecoContribuinte;
       public string EnderecoContribuinte
       {
           get { return (_EnderecoContribuinte == null) ? "" : _EnderecoContribuinte.ToUpper(); }
           set { _EnderecoContribuinte = value; }
       }

       private string _CepContribuinte;
       public string CepContribuinte
       {
           get
           {
               long lngCep = 0;
               if (_CepContribuinte != null) { _CepContribuinte = _CepContribuinte.Replace("-", ""); }
               return (long.TryParse(_CepContribuinte, out lngCep)) ? _CepContribuinte.PadLeft(8, '0') : "00000000";
           }
           set { _CepContribuinte = value; }
       }

       public int DAAgencia;
       public string DADigAgencia;
       public int DARazaoConta;
       public int DANumConta;
       public string DADigNumConta;
       public string AutorizacaoDebito;
       public decimal Valor;
       public decimal Juros;
       public decimal OutrasEntidades;
       public int CodINSS;

       #endregion

       public regTransacao_1_TRIBUTO_005(cTabelaTXT _cTabelaTXT, string Linha)
           : base(_cTabelaTXT, Linha)
       {
       }

       protected override void RegistraMapa()
       {
           base.RegistraMapa();
           RegistraFortementeTipado("NomeContribuinte", 68, 40);
           RegistraFortementeTipado("EnderecoContribuinte", 108, 40);
           RegistraFortementeTipado("CepContribuinte", 148, 8);
           RegistraFortementeTipado("DAAgencia", 171, 4);
           RegistraFortementeTipado("DADigAgencia", 175, 1);
           RegistraFortementeTipado("DARazaoConta", 176, 4);
           RegistraFortementeTipado("DANumConta", 180, 7);
           RegistraFortementeTipado("DADigNumConta", 187, 1);
           RegistraFortementeTipado("AutorizacaoDebito", 196, 1);
           RegistraFortementeTipado("Valor", 197, 15);
           RegistraFortementeTipado("Juros", 212, 15);
           RegistraFortementeTipado("OutrasEntidades", 227, 15);
           RegistraFortementeTipado("ValorTotal", 242, 15);
           RegistraFortementeTipado("CodINSS", 257, 4);
           RegistraFortementeTipado("TipoInscricaoContribuinte", 261, 3);
           RegistraFortementeTipado("NumInscricaoContribuinte", 264, 14);
       }
   }

   public class regTransacao_1_TRIBUTO_006 : regTransacao_1
   {
       #region Variaveis Tipadas

       public decimal Valor;
       public int DAAgencia;
       public string DADigAgencia;
       public int DARazaoConta;
       public int DANumConta;
       public string DADigNumConta;
       public string AutorizacaoDebito;

       #endregion

       public regTransacao_1_TRIBUTO_006(cTabelaTXT _cTabelaTXT, string Linha)
           : base(_cTabelaTXT, Linha)
       {
       }

       protected override void RegistraMapa()
       {
           base.RegistraMapa();
           RegistraFortementeTipado("Valor", 120, 15);
           RegistraFortementeTipado("DAAgencia", 135, 4);
           RegistraFortementeTipado("DADigAgencia", 139, 1);
           RegistraFortementeTipado("DARazaoConta", 140, 4);
           RegistraFortementeTipado("DANumConta", 144, 7);
           RegistraFortementeTipado("DADigNumConta", 151, 1);
           RegistraFortementeTipado("AutorizacaoDebito", 152, 1);
       }
   }

   public class regTrailer_9 : TributoRegistroBase
   {
      #region Variaveis Tipadas

         public long NumInscricao;
         public int QTDRegistros;
         public decimal TotalValorPago;

      #endregion

      public regTrailer_9(cTabelaTXT _cTabelaTXT, string Linha)
         : base(_cTabelaTXT, Linha)
      {
          Registro = 9;
      }

      protected override void RegistraMapa()
      {
         base.RegistraMapa();
         RegistraFortementeTipado("QTDRegistros", 38, 6);
         RegistraFortementeTipado("TotalValorPago", 44, 17);
      }
   }
}