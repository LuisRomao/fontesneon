/*
MR - 21/03/2016 20:00 -           - Substituicao do RegistroBaseBra por BoletoRegistroBase
                                  - Padroniza��o Arquivo de Cobran�a Bradesco / Itau (Altera��es indicadas por *** MRC - INICIO (21/03/2016 20:00) ***)
*/

using System;
using System.Collections.Generic;
using System.Text;
using System.Collections;
using ArquivosTXT;
using DocBacarios;
using EDIBoletos.RegistrosOBJ;

namespace EDIBoletos
{
    //*** MRC - INICIO (21/03/2016 20:00) ***
    /// <summary>
    /// Classe para retorno de cobran�a
    /// </summary>    
    public abstract class BoletoRetorno
    {
        /// <summary>
        /// 
        /// </summary>
        public Exception UltimoErro;

        /// <summary>
        /// 
        /// </summary>
        public abstract bool Carrega(string NomeArquivo);

        /// <summary>
        /// 
        /// </summary>
        public abstract string Relatorio();
    }

    /// <summary>
    /// 
    /// </summary>
    public class regHeader_0_2 : regHeader_0
    {
        #region Variaveis Tipadas

        /// <summary>
        /// 
        /// </summary>
        public int NumSeqRetorno;

        #endregion

        /// <summary>
        /// 
        /// </summary>
        public regHeader_0_2(cTabelaTXT _cTabelaTXT, string Linha)
            : base(_cTabelaTXT, Linha)
        {
        }

        /// <summary>
        /// 
        /// </summary>
        protected override void RegistraMapa()
        {
            base.RegistraMapa();
            RegistraFortementeTipado("NumSeqRetorno", 109, 5);
        }
    }

    /// <summary>
    /// 
    /// </summary>
    public class regTransacao_1_2 : regTransacao_1
    {
        #region Variaveis Tipadas
        
        /// <summary>
        /// 
        /// </summary>
        public int IDOcorrencia;
        /// <summary>
        /// 
        /// </summary>
        public string DataOcorrencia;
        /// <summary>
        /// 
        /// </summary>
        public string NumDocumento;
        /// <summary>
        /// 
        /// </summary>
        public string Vencimento;
        /// <summary>
        /// 
        /// </summary>
        public decimal Valor;
        /// <summary>
        /// 
        /// </summary>
        public int BcoCobrador;
        /// <summary>
        /// 
        /// </summary>
        public int AgCobradora;
        /// <summary>
        /// 
        /// </summary>
        public decimal Tarifa1;
        /// <summary>
        /// 
        /// </summary>
        public decimal IOF;
        /// <summary>
        /// 
        /// </summary>
        public decimal Abatimento;
        /// <summary>
        /// 
        /// </summary>
        public decimal Desconto;
        /// <summary>
        /// 
        /// </summary>
        public decimal ValorPago;
        /// <summary>
        /// 
        /// </summary>
        public decimal Mora;
        /// <summary>
        /// 
        /// </summary>
        public decimal Creditos;
        /// <summary>
        /// 
        /// </summary>
        public string DataCredito;

        #endregion

        /// <summary>
        /// 
        /// </summary>
        public regTransacao_1_2(cTabelaTXT _cTabelaTXT, string Linha)
            : base(_cTabelaTXT, Linha)
        {
        }

        /// <summary>
        /// 
        /// </summary>
        protected override void RegistraMapa()
        {
            base.RegistraMapa();
            RegistraFortementeTipado("IDOcorrencia"    , 109,  2);
            RegistraFortementeTipado("DataOcorrencia"  , 111,  6);
            RegistraFortementeTipado("NumDocumento"    , 117, 10);
            RegistraFortementeTipado("Vencimento"      , 147,  6);
            RegistraFortementeTipado("Valor"           , 153, 13);
            RegistraFortementeTipado("BcoCobrador"    , 166,  3);
            RegistraFortementeTipado("AgCobradora"    , 169,  5);
            RegistraFortementeTipado("Tarifa1"         , 176, 13);
            RegistraFortementeTipado("IOF"             , 215, 13);
            RegistraFortementeTipado("Abatimento"      , 228, 13);
            RegistraFortementeTipado("Desconto"        , 241, 13);
            RegistraFortementeTipado("ValorPago"       , 254, 13);
            RegistraFortementeTipado("Mora"            , 267, 13);
            RegistraFortementeTipado("Creditos"        , 280, 13);
            RegistraFortementeTipado("DataCredito"     , 296,  6);
        }
    }

    /*
     ATEN��O o retonro do rateio de cr�dito foi implantado diretamento no BoletoRetornoBRA caso venha a ser feito no Ita� poder� ser criado objeto regTransacao_3_2 : regTransacao_3
    */

    /// <summary>
    /// 
    /// </summary>
    public class regTrailer_9_2 : BoletoRegistroBase
    {
        #region Variaveis Tipadas

        /// <summary>
        /// 
        /// </summary>
        public int CodigoRet;
        /// <summary>
        /// 
        /// </summary>
        public int IDTipoRegistro;
        /// <summary>
        /// 
        /// </summary>
        public int NumeroBanco;
        /// <summary>
        /// 
        /// </summary>
        public int QtdTitulos;
        /// <summary>
        /// 
        /// </summary>
        public decimal ValorTitulos;
        /// <summary>
        /// 
        /// </summary>
        public string NumAvisoBanco;

        #endregion

        /// <summary>
        /// 
        /// </summary>
        public regTrailer_9_2(cTabelaTXT _cTabelaTXT, string Linha)
            : base(_cTabelaTXT)
        {
            CarregaLinha(Linha);
        }

        /// <summary>
        /// 
        /// </summary>
        protected override void RegistraMapa()
        {
            base.RegistraMapa();
            RegistraFortementeTipado("CodigoRet"          ,   2,  1);
            RegistraFortementeTipado("IDTipoRegistro"     ,   3,  2);
            RegistraFortementeTipado("NumeroBanco"        ,   5,  3);
            RegistraFortementeTipado("QtdTitulos"         ,  18,  8);
            RegistraFortementeTipado("ValorTitulos"       ,  26, 14);
            RegistraFortementeTipado("NumAvisoBanco"      ,  40,  8);
        }
    }
    //*** MRC - TERMINO (21/03/2016 20:00) ***

    /// <summary>
    /// 
    /// </summary>
    public class ColBoletos_2 : CollectionBase
    {
        /// <summary>
        /// 
        /// </summary>
        public regTransacao_1_2 this[int index]
        {
            get
            {
                return ((regTransacao_1_2)List[index]);
            }
            set
            {
                List[index] = value;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        public int Add(regTransacao_1_2 value)
        {
            return (List.Add(value));
        }

        /// <summary>
        /// 
        /// </summary>
        public int IndexOf(regTransacao_1_2 value)
        {
            return (List.IndexOf(value));
        }

        /// <summary>
        /// 
        /// </summary>
        public void Insert(int index, regTransacao_1_2 value)
        {
            List.Insert(index, value);
        }

        /// <summary>
        /// 
        /// </summary>
        public void Remove(regTransacao_1_2 value)
        {
            List.Remove(value);
        }

        /// <summary>
        /// 
        /// </summary>
        public bool Contains(regTransacao_1_2 value)
        {
            // If value is not of type Int16, this will return false.
            return (List.Contains(value));
        }

        /// <summary>
        /// 
        /// </summary>
        protected override void OnValidate(Object value)
        {
            if (value.GetType() != typeof(regTransacao_1_2))
                throw new ArgumentException("Value must be of type regTransacao_1_2.", "value");
        }
    }
}