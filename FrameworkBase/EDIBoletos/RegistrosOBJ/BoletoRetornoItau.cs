﻿/*
MR - 28/04/2016 12:00             - Considera o header apenas da primeira linha, porque pode ter mais de um header no arquivo (Alterações indicadas por *** MRC - INICIO (28/04/2016 12:00) ***)
*/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Collections;
using ArquivosTXT;
using EDIBoletos.RegistrosOBJ;

namespace EDIBoletos
{
    /// <summary>
    /// Classe para retorno de cobrança
    /// </summary>
    /// <remarks>O tratamento do retorno hoje está sendo feito diretamente dentro da dllBanco / francesinha</remarks>
   public class BoletoRetornoItau: BoletoRetorno 
   {
       /// <summary>
       /// Ocorrencias do retorno
       /// </summary>
       public enum TiposOcorrencia
       {
           /// <summary>
           /// Registrado
           /// </summary>
           EntradaConfirmada = 2,
           /// <summary>
           /// Erro no registro
           /// </summary>
           EntradaRejeitada = 3,
           /// <summary>
           /// Alteração confirmada
           /// </summary>
           AlteracaoConfirmada = 4,
           /// <summary>
           /// Alteração confimada - baixa
           /// </summary>
           AlteracaoBaixa = 5,
           /// <summary>
           /// Boleto pago
           /// </summary>
           LiquidacaoNormal = 6,
           /// <summary>
           /// Não usado
           /// </summary>
           LiquidacaoParcial = 7,
           /// <summary>
           /// Não usado
           /// </summary>
           LiquidacaoCartorio = 8,
           /// <summary>
           /// Não usado
           /// </summary>
           BaixaSimples = 9,
           /// <summary>
           /// Não usado
           /// </summary>
           BaixaLiquidacao = 10,
           /// <summary>
           /// Não usado
           /// </summary>
           EmSer = 11,
           /// <summary>
           /// Não usado
           /// </summary>
           AbatimentoConcedido = 12,
           /// <summary>
           /// Não usado
           /// </summary>
           AbatimentoCancelado = 13,
           /// <summary>
           /// Utilizamos a alteração de outros dados
           /// </summary>
           VencimentoAlterado = 14,
           /// <summary>
           /// Não usado
           /// </summary>
           BaixaRejeitada = 15,
           /// <summary>
           /// Alteração do boleto rejeitada
           /// </summary>
           InstrucaoRejeitada = 16,
           /// <summary>
           /// Alteração do boleto rejeitada
           /// </summary>
           AlteracaoExclusaoRejeitada = 17,
           /// <summary>
           /// Não usado
           /// </summary>
           CobrancaContratualInstrucaoAlteracaoRejeitada = 18,
           /// <summary>
           /// Não usado
           /// </summary>
           ConfRecProtesto = 19,
           /// <summary>
           /// Não usado
           /// </summary>
           ConfRecInstProtesto = 20,
           /// <summary>
           /// Confirma recebimento de instrução de não protesto
           /// </summary>
           ConfRecInstNaoProtesto = 21,
           /// <summary>
           /// Não usado
           /// </summary>
           EntradaTituloCartorio = 23,
           /// <summary>
           /// Não usado
           /// </summary>
           InstProtestoRejeitada = 24,
           /// <summary>
           /// Não usado
           /// </summary>
           AlegacaoPagador = 25,
           /// <summary>
           /// Não usado
           /// </summary>
           TarifaAvisoCobranca = 26,
           /// <summary>
           /// Não usado
           /// </summary>
           TarifaExtratoPosicao = 27,
           /// <summary>
           /// Não usado
           /// </summary>
           TarifaRelacaoLiquidacao = 28,
           /// <summary>
           /// Não usado
           /// </summary>
           TarifaManutencaoTitulosVencidos = 29,
           /// <summary>
           /// Não usado
           /// </summary>
           TarifasMensais = 30,
           /// <summary>
           /// Não usado
           /// </summary>
           BaixaPorProtesto = 32,
           /// <summary>
           /// Não usado
           /// </summary>
           CustasProtesto = 33,
           /// <summary>
           /// Não usado
           /// </summary>
           CustasSustacao = 34,
           /// <summary>
           /// Não usado
           /// </summary>
           CustasCartorioDistribuidor = 35,
           /// <summary>
           /// Não usado
           /// </summary>
           CustasEdital = 36,
           /// <summary>
           /// Não usado
           /// </summary>
           TarifaEmissaoBoletoOuDuplicata = 37,
           /// <summary>
           /// Não usado
           /// </summary>
           TarifaInstrucao = 38,
           /// <summary>
           /// Não usado
           /// </summary>
           TarifaOcorrencia = 39,
           /// <summary>
           /// Não usado
           /// </summary>
           TarifaMensalEmissaoBoletoOuDuplicata = 40,
           /// <summary>
           /// Não usado
           /// </summary>
           TarifaMensalExtratoPosicao = 41,
           /// <summary>
           /// Não usado
           /// </summary>
           TarifaMensalInstrucoes = 42,
           /// <summary>
           /// Não usado
           /// </summary>
           TarifaMensalManutencaoTitulosVencidos = 43,
           /// <summary>
           /// Não usado
           /// </summary>
           TarifaMensalOcorrencias = 44,
           /// <summary>
           /// Não usado
           /// </summary>
           TarifaMensalProtesto = 45,
           /// <summary>
           /// Não usado
           /// </summary>
           TarifaMensalSustacaoProtesto = 46,
           /// <summary>
           /// Não usado
           /// </summary>
           BaixaTransferenciaDesconto = 47,
           /// <summary>
           /// Não usado
           /// </summary>
           CustasSustacaoJudicial = 48,
           /// <summary>
           /// Não usado
           /// </summary>
           TarifaMensalEntradaBancosCarteira = 51,
           /// <summary>
           /// Não usado
           /// </summary>
           TarifaMensalBaixasCarteira = 52,
           /// <summary>
           /// Não usado
           /// </summary>
           TarifaMensalBaixasBancosCarteira = 53,
           /// <summary>
           /// Não usado
           /// </summary>
           TarifaMensalLiquidacaoCarteira = 54,
           /// <summary>
           /// Não usado
           /// </summary>
           TarifaMensalLiquidacaoBancosCarteira = 55,
           /// <summary>
           /// Não usado
           /// </summary>
           CustasIrregularidade = 56,
           /// <summary>
           /// Não usado
           /// </summary>
           InstrucaoCancelada = 57,
           /// <summary>
           /// Não usado
           /// </summary>
           BaixaCreditoSISPAG = 59,
           /// <summary>
           /// Não usado
           /// </summary>
           EntradaRejeitadaCarne = 60,
           /// <summary>
           /// Não usado
           /// </summary>
           TarifaEmissaoAvisoMovimentacao = 61,
           /// <summary>
           /// Não usado
           /// </summary>
           TarifaMensalEmissaoAvisoMovimentacao = 62,
           /// <summary>
           /// Não usado
           /// </summary>
           SustadoJudicial = 63,
           /// <summary>
           /// Não Usado
           /// </summary>
           EntradaConfirmadaRateio = 64,
           /// <summary>
           /// Não Usado
           /// </summary>
           ChequeDevolvido = 69,
           /// <summary>
           /// Não Usado
           /// </summary>
           EntradaConfirmadaAvaliacao = 71,
           /// <summary>
           /// Não usado
           /// </summary>
           BaixaCreditoSISPAGSemTitulo = 72,
           /// <summary>
           /// Não usado
           /// </summary>
           EntradaConfirmadaCobrancaSimples = 73,
           /// <summary>
           /// Não Usado
           /// </summary>
           ChequeCompensado = 76,
       }

       /// <summary>
       /// 
       /// </summary>
       public regHeader_0_2_ITAU Header;
       /// <summary>
       /// 
       /// </summary>
       public regTrailer_9_2_ITAU Trailer;
       /// <summary>
       /// 
       /// </summary>
       public ColBoletos_2_ITAU Boletos;

       private BoletoRegistroBase reg;
       private int nLinha = 0;
       private bool CargaOk = false;

       private cTabelaTXT _Arquivo;
       private cTabelaTXT Arquivo
       {
           get
           {
               if (_Arquivo == null) _Arquivo = new cTabelaTXT(ModelosTXT.layout);
               _Arquivo.Formatodata = FormatoData.ddMMyyyy;
               return _Arquivo;
           }
       }

       private BoletoRegistroBase ProximoRegistro(string RegistroEsperado)
       {
           string Linha = Arquivo.ProximaLinha();
           if (Linha.Length != 400)
           {
               UltimoErro = new Exception(string.Format("Linha com tamanho incorreto. Esperado 400 encontrado {0}\r\nLinha:{1} <{2}>", Linha.Length, nLinha, Linha));
               return null;
           }
           if ((Linha == null) || (Linha == ""))
           {
               UltimoErro = new Exception(string.Format("Término inesperado de arquivo\r\nRegisto(s) esperado(s): {0}", RegistroEsperado));
               return null;
           }
            if (reg == null)
            {
                reg = new BoletoRegistroBase(Arquivo);
                reg.CarregaLinha(Linha);
            }
            else
                reg.Decodificalinha(Linha);
           nLinha++;
           return reg;
       }

       /// <summary>
       /// 
       /// </summary>
       public override bool Carrega(string NomeArquivo)
       {
           UltimoErro = null;
           try
           {
               CargaOk = _Carrega(NomeArquivo);
           }
           finally
           {
               if (_Arquivo != null) _Arquivo.LiberaArquivo();
           }
           return CargaOk;
       }

       /// <summary>
       /// 
       /// </summary>
       public string NomeArquivo
       {
           get { return Arquivo.NomeArquivo; }
       }

       private bool _Carrega(string NomeArquivo)
       {
           Boletos = new ColBoletos_2_ITAU();
           Arquivo.NomeArquivo = NomeArquivo;
           //*** MRC - INICIO (28/04/2016 12:00) ***
           bool booHeader = false;
           //*** MRC - TERMINO (28/04/2016 12:00) ***
           int nSequencial = 0;
           int nRegistro = 1;
           if (ProximoRegistro("0") == null)
               return false;
           nSequencial++;
           do
           {
               if (reg.Registro == 0)
               {
                   //*** MRC - INICIO (28/04/2016 12:00) ***
                   if (!booHeader)
                   {
                       Header = new regHeader_0_2_ITAU(Arquivo, reg.LinhaLida);
                       booHeader = true;
                   }
                   //*** MRC - TERMINO (28/04/2016 12:00) ***
                   if (ProximoRegistro("1 9") == null)
                       return false;
                   nSequencial++;
               }
               else
               {
                   UltimoErro = new Exception(string.Format("Registro inicial incorreto. Esperado 0 encontrado {0}\r\n Linha {1} <{2}>", reg.Registro, nLinha, reg.LinhaLida));
                   return false;
               }
               while (reg.Registro != 9 && reg.Registro != 0)
               {
                   regTransacao_1_2_ITAU regTrans = new regTransacao_1_2_ITAU(Arquivo, reg.LinhaLida);
                   Boletos.Add(regTrans);
                   nRegistro++;
                   if (regTrans.Sequencial != nSequencial)
                   {
                       UltimoErro = new Exception(string.Format("Registro fora de ordem: Esperado: {0} Encontrado: {1}\r\nLinha:{2} <{3}>", nRegistro, regTrans.Sequencial, nLinha, reg.LinhaLida));
                       return false;
                   };
                   if (ProximoRegistro("1 9") == null)
                       return false;
                   nSequencial++;
               }
               if (reg.Registro == 9)
               {
                   Trailer = new regTrailer_9_2_ITAU(Arquivo, reg.LinhaLida);
                   //if (Arquivo.TamanhoArquivo > Arquivo.bytesLidos + 400)
                   if (Arquivo.BytesProximaLinha() >= 400)
                   {
                       nSequencial = 0;
                       if (ProximoRegistro("0") == null)
                           return false;
                       nSequencial++;
                   }
               }
           }
           while (Arquivo.BytesProximaLinha() >= 400);
           return true;
       }

       /// <summary>
       /// Relatório
       /// </summary>
       /// <returns></returns>
       public override string Relatorio()
       {
           string relatorio = "";
           relatorio += String.Format("Arquivo {0}: ", Arquivo.NomeArquivo);

           if (CargaOk)
           {
               relatorio += String.Format("\r\n\r\n{0}\r\n\r\nBanco: {1}\r\nEmpresa: {4} - {5}\r\nGeracao:{2:dd/MM/yyyy}\r\nCredito:{3:dd/MM/yyyy}\r\nRegistros: {6}\r\n\r\n",
                                          Header.CodigoRemRet == 1 ? "REMESSA" : "RETORNO",
                                          Header.NumeroBanco,
                                          Header.DataGeracao,
                                          Header.DataCredito,
                                          Header.AgenciaMae.ToString() + "/" + Header.ContaMae.ToString() + "-" + Header.ContaMaeDg,
                                          Header.NomeEmpresa,
                                          Boletos.Count);

               foreach (regTransacao_1_2_ITAU trans in Boletos)
               {
                   relatorio += String.Format("        BOL {0} Vencimento {1:dd/MM/yyyy} Valor {2:n2} Tarifas {3:n2} Data Crédito {4:dd/MM/yyyy} Valor Pago {5:n2} Carteira {7} Agencia/Conta {8}/{9}-{10} Informacao Adicional {6}\r\n",
                                              trans.NossoNumero,
                                              trans.Vencimento,
                                              trans.Valor,
                                              trans.Tarifa1,
                                              trans.DataCredito,
                                              trans.ValorPago,
                                              trans.Nome,
                                              trans.Carteira,
                                              trans.Agencia,
                                              trans.Conta,
                                              trans.DigConta);
                   relatorio += String.Format("        -> IdOcorrencia - Motivo : {0} - {1}\r\n", trans.IDOcorrencia, trans.MotivoOcorrencia);
               }

               relatorio += String.Format("\r\nQtd Titulos: {0}\r\nValor Titulos: {1:n2}\r\n",
                                          Trailer.QtdTitulos,
                                          Trailer.ValorTitulos);
               relatorio += String.Format("\r\nQtd Titulos - Cobrança Vinculada: {0}\r\nValor Titulos - Cobrança Vinculada: {1:n2}\r\n",
                                          Trailer.QtdTitVinculada,
                                          Trailer.ValorTitVinculada);
               relatorio += String.Format("\r\nQtd Titulos - Cobrança Escritural: {0}\r\nValor Titulos - Cobrança Escritural: {1:n2}\r\n",
                                          Trailer.QtdTitEscritural,
                                          Trailer.ValorTitEscritural);
               relatorio += String.Format("\r\nQtd Titulos no Arquivo: {0}\r\nValor Total dos Títulos no Arquivo: {1:n2}\r\n",
                                          Trailer.QtdTitArquivo,
                                          Trailer.ValorTitArquivo);
           }
           else
           {
               relatorio += String.Format("Erro\r\n{0}", UltimoErro.Message);
           }
           return relatorio;
       }
   }

   /// <summary>
   /// 
   /// </summary>
   public class regHeader_0_2_ITAU : regHeader_0_2
   {
       #region Variaveis Tipadas

       /// <summary>
       /// Agencia Mãe (somente Itau)
       /// </summary>
       public int AgenciaMae;

       /// <summary>
       /// Conta Mãe (somente Itau)
       /// </summary>
       public int ContaMae;

       /// <summary>
       /// Digito da Conta Mãe (somente Itau)
       /// </summary>
       public string ContaMaeDg;

       /// <summary>
       /// 
       /// </summary>
       public string DataCredito;

       #endregion

       /// <summary>
       /// 
       /// </summary>
       public regHeader_0_2_ITAU(cTabelaTXT _cTabelaTXT, string Linha)
           : base(_cTabelaTXT, Linha)
       {
       }

       /// <summary>
       /// 
       /// </summary>
       protected override void RegistraMapa()
       {
           base.RegistraMapa();
           RegistraFortementeTipado("AgenciaMae", 27, 4);
           RegistraFortementeTipado("ContaMae", 33, 5);
           RegistraFortementeTipado("ContaMaeDg", 38, 1);
           RegistraFortementeTipado("DataCredito", 114, 6);
       }
   }

   /// <summary>
   /// 
   /// </summary>
   public class regTransacao_1_2_ITAU : regTransacao_1_2
   {
       #region Variaveis Tipadas

       /// <summary>
       /// 
       /// </summary>
       public int TipoCNPJ;
       /// <summary>
       /// 
       /// </summary>
       public int NumCNPJ;
       /// <summary>
       /// 
       /// </summary>
       public int Agencia;
       /// <summary>
       /// 
       /// </summary>
       public int Conta;
       /// <summary>
       /// 
       /// </summary>
       public string DigConta;
       /// <summary>
       /// 
       /// </summary>
       public string NossoNumero;
       /// <summary>
       /// 
       /// </summary>
       public int Carteira;
       /// <summary>
       /// 
       /// </summary>
       public string CodCarteira;
       /// <summary>
       /// 
       /// </summary>
       public int EspecieTitulo;
       /// <summary>
       /// 
       /// </summary>
       public int InstrCancelada;
       /// <summary>
       /// 
       /// </summary>
       public string Nome;
       /// <summary>
       /// 
       /// </summary>
       public string MotivoOcorrencia;
       /// <summary>
       /// 
       /// </summary>
       public string CodLiquidacao;

       #endregion

       /// <summary>
       /// 
       /// </summary>
       public regTransacao_1_2_ITAU(cTabelaTXT _cTabelaTXT, string Linha)
           : base(_cTabelaTXT, Linha)
       {
       }

       /// <summary>
       /// 
       /// </summary>
       protected override void RegistraMapa()
       {
           base.RegistraMapa();
           RegistraFortementeTipado("TipoCNPJ", 2, 2);
           RegistraFortementeTipado("NumCNPJ", 4, 14);
           RegistraFortementeTipado("Agencia", 18, 4);
           RegistraFortementeTipado("Conta", 24, 5);
           RegistraFortementeTipado("DigConta", 29, 1);
           RegistraFortementeTipado("NossoNumero", 63, 8);
           RegistraFortementeTipado("Carteira", 83, 3);
           RegistraFortementeTipado("CodCarteira", 108, 1);
           RegistraFortementeTipado("EspecieTitulo", 174, 2);
           RegistraFortementeTipado("InstrCancelada", 302, 4);
           RegistraFortementeTipado("Nome", 325, 30);
           RegistraFortementeTipado("MotivoOcorrencia", 378, 8);
           RegistraFortementeTipado("CodLiquidacao", 393, 7);
       }
   }

   /// <summary>
   /// 
   /// </summary>
   public class regTrailer_9_2_ITAU : regTrailer_9_2
   {
       #region Variaveis Tipadas

       /// <summary>
       /// 
       /// </summary>
       public int QtdTitVinculada;
       /// <summary>
       /// 
       /// </summary>
       public decimal ValorTitVinculada;
       /// <summary>
       /// 
       /// </summary>
       public string NumAvisoBanco2;
       /// <summary>
       /// 
       /// </summary>
       public int QtdTitEscritural;
       /// <summary>
       /// 
       /// </summary>
       public decimal ValorTitEscritural;
       /// <summary>
       /// 
       /// </summary>
       public string NumAvisoBanco3;
       /// <summary>
       /// 
       /// </summary>
       public int NumSeqRetorno;
       /// <summary>
       /// 
       /// </summary>
       public int QtdTitArquivo;
       /// <summary>
       /// 
       /// </summary>
       public decimal ValorTitArquivo;

       #endregion

       /// <summary>
       /// 
       /// </summary>
       public regTrailer_9_2_ITAU(cTabelaTXT _cTabelaTXT, string Linha)
           : base(_cTabelaTXT, Linha)
       {
       }

       /// <summary>
       /// 
       /// </summary>
       protected override void RegistraMapa()
       {
           base.RegistraMapa();
           RegistraFortementeTipado("QtdTitVinculada", 58, 8);
           RegistraFortementeTipado("ValorTitVinculada", 66, 14);
           RegistraFortementeTipado("NumAvisoBanco2", 80, 8);
           RegistraFortementeTipado("QtdTitEscritural", 178, 8);
           RegistraFortementeTipado("ValorTitEscritural", 186, 14);
           RegistraFortementeTipado("NumAvisoBanco3", 200, 8);
           RegistraFortementeTipado("NumSeqRetorno", 208, 5);
           RegistraFortementeTipado("QtdTitArquivo", 213, 8);
           RegistraFortementeTipado("ValorTitArquivo", 221, 14);
       }
   }

   /// <summary>
   /// 
   /// </summary>
   public class ColBoletos_2_ITAU : CollectionBase
   {
       /// <summary>
       /// 
       /// </summary>
       public regTransacao_1_2_ITAU this[int index]
       {
           get
           {
               return ((regTransacao_1_2_ITAU)List[index]);
           }
           set
           {
               List[index] = value;
           }
       }

       /// <summary>
       /// 
       /// </summary>
       public int Add(regTransacao_1_2_ITAU value)
       {
           return (List.Add(value));
       }

       /// <summary>
       /// 
       /// </summary>
       public int IndexOf(regTransacao_1_2_ITAU value)
       {
           return (List.IndexOf(value));
       }

       /// <summary>
       /// 
       /// </summary>
       public void Insert(int index, regTransacao_1_2_ITAU value)
       {
           List.Insert(index, value);
       }

       /// <summary>
       /// 
       /// </summary>
       public void Remove(regTransacao_1_2_ITAU value)
       {
           List.Remove(value);
       }

       /// <summary>
       /// 
       /// </summary>
       public bool Contains(regTransacao_1_2_ITAU value)
       {
           // If value is not of type Int16, this will return false.
           return (List.Contains(value));
       }

       /// <summary>
       /// 
       /// </summary>
       protected override void OnValidate(Object value)
       {
           if (value.GetType() != typeof(regTransacao_1_2_ITAU))
               throw new ArgumentException("Value must be of type regTransacao_1_2_ITAU.", "value");
       }
   }
}
