﻿/*
MR - 25/02/2016 11:00 -           - Padronização Arquivo de Cobrança Bradesco / Itau (Alterações indicadas por *** MRC - INICIO - REMESSA (25/02/2016 11:00) ***)
MR - 21/03/2016 20:00 -           - GravaArquivo permite usar um arquivo indicado nos parametros (Alterações indicadas por *** MRC - INICIO (21/03/2016 20:00) ***)
                                  - Inclusão do FinalizaArquivo
*/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Collections;
using ArquivosTXT;
using EDIBoletos.RegistrosOBJ;

namespace EDIBoletos
{
    /// <summary>
    /// 
    /// </summary>
    public abstract class BoletoRemessaBase
    {
        /// <summary>
        ///Prefixo para os arquivos
        /// </summary>
        abstract public string PrefixoArquivo { get; }
        /// <summary>
        /// 
        /// </summary>
        public Exception UltimoErro;
        /// <summary>
        /// 
        /// </summary>
        public ColBoletos Boletos;
        /// <summary>
        /// 
        /// </summary>
        public cTabelaTXT ArquivoSaida;

        //*** MRC - INICIO - REMESSA (25/02/2016 11:00) ***
        /// <summary>
        /// Codigo da Empresa (somente Bradesco)
        /// </summary>
        public int CodEmpresa = 0;
        /// <summary>
        /// Agencia Mãe (somente Itau)
        /// </summary>
        public int AgenciaMae = 0;
        /// <summary>
        /// Conta Mãe (somente Itau)
        /// </summary>
        public int ContaMae = 0;
        /// <summary>
        /// Digito da Conta Mãe (somente Itau)
        /// </summary>
        public string ContaMaeDg = "0";
        /// <summary>
        /// 
        /// </summary>
        public string NomeEmpresa;

        /// <summary>
        /// CNPJ
        /// </summary>
        public long nCNPJ;
        /// <summary>
        /// 
        /// </summary>
        public string Carteira;
        /// <summary>
        /// 
        /// </summary>
        public int SequencialRemessa;
        /// <summary>
        /// 
        /// </summary>
        public int SequencialRemessaArquivo;

        /// <summary>
        /// Inicia os dados do cabeçalho
        /// </summary>
        /// <param name="AgenciaMae"></param>
        /// <param name="ContaMae"></param>
        /// <param name="ContaMaeDg"></param>
        /// <param name="NomeEmpresa"></param>
        /// <param name="cnpj"></param>
        /// <param name="Carteira"></param>
        /// <param name="CodEmpresa"></param>
        /// <param name="SequencialRemessa"></param>
        public void Configura(int AgenciaMae, int ContaMae, string ContaMaeDg, string NomeEmpresa, DocBacarios.CPFCNPJ cnpj, string Carteira, int CodEmpresa, int SequencialRemessa)
        {
            this.AgenciaMae = AgenciaMae;
            this.ContaMae = ContaMae;
            this.ContaMaeDg = ContaMaeDg;
            this.NomeEmpresa = NomeEmpresa;
            this.nCNPJ = cnpj.Valor;
            this.Carteira = Carteira;
            this.CodEmpresa = CodEmpresa;
            this.SequencialRemessa = SequencialRemessa;
        }

        /// <summary>
        /// 
        /// </summary>
        public void IniciaRemessa()
        {
            Boletos = new ColBoletos();
            ArquivoSaida = new cTabelaTXT(ModelosTXT.layout)
            {
                Formatodata = FormatoData.ddMMyyyy,
                defaultremoveacentos = true,
                defaultlimpaAZ09 = true,
                Encoding = Encoding.ASCII
            };
        }

        /// <summary>
        /// 
        /// </summary>
        //public abstract string GravaArquivo(string Path);
        public abstract string GravaArquivo(string Path, string Arquivo = "");

        /// <summary>
        /// 
        /// </summary>
        public abstract string FinalizaArquivo(string Arquivo);

        /// <summary>
        /// Nome do proximo arquivo a ser enviado
        /// </summary>
        /// <returns></returns>
        protected string ProximoNome(out int? Remessa)
        {
            const string strPathTemp = @"C:\Modulo servidor\TMP\";
            string strPathBackup = string.Format(@"{0}BKP\{1:yyyyMMdd}\", strPathTemp, DateTime.Now);
            if (!System.IO.Directory.Exists(strPathTemp)) System.IO.Directory.CreateDirectory(strPathTemp);
            if (!System.IO.Directory.Exists(strPathBackup)) System.IO.Directory.CreateDirectory(strPathBackup);
            int Testei = 0;
            string NomeArquivoTeste;
            string NomeArquivo;
            do
            {
                NomeArquivoTeste = string.Format("{0}{1}_{2:ddMM}{3:00}.REM", strPathBackup, PrefixoArquivo, DateTime.Today, ++Testei);
                NomeArquivo = string.Format("{0}{1}_{2:ddMM}{3:00}.REM", strPathTemp, PrefixoArquivo, DateTime.Today, Testei);
            } while ((System.IO.File.Exists(NomeArquivoTeste)) || (System.IO.File.Exists(NomeArquivo)));
            Remessa = Testei;
            return NomeArquivo;
        }
    }

    /// <summary>
    /// Tipos de ocorrência padrão
    /// </summary>
    public enum OcorrenciasPadrao
    {
        /// <summary>
        /// Registrar Boleto
        /// </summary>
        Registrar,
        /// <summary>
        /// Registrar com débito automático
        /// </summary>
        RegistrarComDebitoAutomatico,
        /// <summary>
        /// Alteração na data de vencimento
        /// </summary>
        AlteracaoDeData,
        /// <summary>
        /// Concessão de abatimento
        /// </summary>
        ConcessaoAbatimento,

        CancelametoDeRegistro,
    }

    /// <summary>
    /// 
    /// </summary>
    public class ColBoletos : CollectionBase
    {
        /// <summary>
        /// 
        /// </summary>
        public BoletoReg this[int index]
        {
            get
            {
                return ((BoletoReg)List[index]);
            }
            set
            {
                List[index] = value;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        public int Add(BoletoReg value)
        {
            return (List.Add(value));
        }

        /// <summary>
        /// 
        /// </summary>
        public int IndexOf(BoletoReg value)
        {
            return (List.IndexOf(value));
        }

        /// <summary>
        /// 
        /// </summary>
        public void Insert(int index, BoletoReg value)
        {
            List.Insert(index, value);
        }

        /// <summary>
        /// 
        /// </summary>
        public void Remove(BoletoReg value)
        {
            List.Remove(value);
        }

        /// <summary>
        /// 
        /// </summary>
        public bool Contains(BoletoReg value)
        {
            // If value is not of type Int16, this will return false.
            return (List.Contains(value));
        }

    }
}
