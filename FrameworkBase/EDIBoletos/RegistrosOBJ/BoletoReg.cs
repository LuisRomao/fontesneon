﻿/*
MR - 25/02/2016 11:00 -           - Padronização Arquivo de Cobrança Bradesco / Itau (Alterações indicadas por *** MRC - INICIO - REMESSA (25/02/2016 11:00) ***)
*/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Collections;
using ArquivosTXT;
using DocBacarios;
using EDIBoletos.RegistrosOBJ;

namespace EDIBoletos
{
    /// <summary>
    /// 
    /// </summary>
    public class BoletoReg
    {
        /// <summary>
        /// Número de dias válidos após o vencimento
        /// </summary>
        public int DiasAposVencimento = 30;
        /// <summary>
        /// Ocorrência
        /// </summary>
        public OcorrenciasPadrao Ocorrencia = OcorrenciasPadrao.Registrar;
        /// <summary>
        /// Banco
        /// </summary>
        public int Banco;
        /// <summary>
        /// Nome do condomínio
        /// </summary>
        public string NomeEmpresa;
        /// <summary>
        /// CNPJ do condomínio
        /// </summary>
        public CPFCNPJ CNPJ;
        /// <summary>
        /// Agência do condomínio
        /// </summary>
        public string Agencia;
        /// <summary>
        /// Conta do condomínio
        /// </summary>
        public string Conta;
        /// <summary>
        /// Dígito da Conta do condomínio
        /// </summary>
        public string DigConta;
        /// <summary>
        /// Uso da Empresa (número de controle do participante, informação incluída na remessa será devolvida no retorno)
        /// </summary>
        public string ControlePart;
        /// <summary>
        /// 
        /// </summary>
        public int Convenio;
        /// <summary>
        /// 
        /// </summary>
        public bool RateioContratado { get { return _RateioCreditos != null; } }
        /// <summary>
        /// 
        /// </summary>
        public string DAAgencia;
        /// <summary>
        /// 
        /// </summary>
        public string DADigAgencia;
        /// <summary>
        /// 
        /// </summary>
        public int DARazaoConta;
        /// <summary>
        /// 
        /// </summary>
        public string DANumConta;
        /// <summary>
        /// 
        /// </summary>
        public string DADigNumConta;
        /// <summary>
        /// 
        /// </summary>
        public bool CobrarMulta = false;
        /// <summary>
        /// 
        /// </summary>
        public decimal PercMulta;
        /// <summary>
        /// 
        /// </summary>
        public int NossoNumero;
        /// <summary>
        /// 
        /// </summary>
        public decimal DescontoBonDia;
        /// <summary>
        /// Tipo de Emissão do Boleto (somente Bradesco)
        /// </summary>
        public int EmissaoBoleto;
        /// <summary>
        /// 
        /// </summary>
        public bool RegistrarDA = false;
        /// <summary>
        /// 
        /// </summary>
        public int AvisoDA;
        /// <summary>
        /// 
        /// </summary>
        public int IDOcorrencia;
        /// <summary>
        /// 
        /// </summary>
        public string NumDocumento;
        /// <summary>
        /// 
        /// </summary>
        public DateTime Vencimento;
        /// <summary>
        /// 
        /// </summary>
        public decimal Valor;
        /// <summary>
        /// 
        /// </summary>
        public int EspecieTitulo;
        /// <summary>
        /// 
        /// </summary>
        public string ID;
        /// <summary>
        /// 
        /// </summary>
        public DateTime Emissao;
        /// <summary>
        /// Instrução 1 (Somente Bradesco)
        /// </summary>
        public int Instrucao1;
        /// <summary>
        /// Instrução 2 (Somente Bradesco)
        /// </summary>
        public int Instrucao2;
        /// <summary>
        /// 
        /// </summary>
        public decimal Mora;
        /// <summary>
        /// 
        /// </summary>
        public DateTime LimDesconto;
        /// <summary>
        /// 
        /// </summary>
        public decimal Desconto;
        /// <summary>
        /// 
        /// </summary>
        public decimal IOF;
        /// <summary>
        /// 
        /// </summary>
        public decimal Abatimento;
        /// <summary>
        /// 
        /// </summary>
        public CPFCNPJ CPF;
        /// <summary>
        /// 
        /// </summary>
        public string NomeSacado;
        /// <summary>
        /// 
        /// </summary>
        public string EnderecoSacado;
        /// <summary>
        /// Bairro do Sacado (Somente Itaú)
        /// </summary>
        public string BairroSacado;
        /// <summary>
        /// Cidade do Sacado (Somente Itaú)
        /// </summary>
        public string CidadeSacado;
        /// <summary>
        /// Estado do Sacado (Somente Itaú)
        /// </summary>
        public string EstadoSacado;
        /// <summary>
        /// 
        /// </summary>
        public string Mensagem1;
        /// <summary>
        /// 
        /// </summary>
        public string CEPSacado;
        /// <summary>
        /// Mensagem 2 (Somente Bradesco)
        /// </summary>
        public string Mensagem2;

        private List<RateioCredito> _RateioCreditos;

        /// <summary>
        /// Creditos a ratear
        /// </summary>
        public List<RateioCredito> RateioCreditos { get { return _RateioCreditos ?? (_RateioCreditos = new List<RateioCredito>()); }}

        /// <summary>
        /// Registrar boletos
        /// </summary>
        /// <param name="Remessa"></param>
        /// <returns></returns>
        public bool Registrar(BoletoRemessaBase Remessa)
        {
            if (Remessa == null)
                throw new ArgumentNullException("Remessa");
            if (Vencimento < DateTime.Today)
                throw new ArgumentOutOfRangeException("Vencimento", "Tentativa de registro de boleto vencido");
            
            bool SoRegistra = (Remessa != null);

            //Agencia = Remessa.Age
            //Conta = Remessa.
            //DigConta = Remessa.
            //CNPJ = Remessa.nCNPJ,
            //NomeEmpresa = rowCON.CONNome,
            //CobrarMulta = true,
            //PercMulta = rowPrincipal.BOLMulta,
            //NossoNumero = rowPrincipal.BOL,
            EmissaoBoleto = (int)BoletoRemessaBra.TiposEmissaoBoleto.Cliente;
            //Convenio
                        
            switch (Ocorrencia)
            {
                case OcorrenciasPadrao.Registrar:
                    IDOcorrencia = (Banco == 237) ? (int)BoletoRemessaBra.TiposOcorrencia.Remessa : (int)BoletoRemessaItau.TiposOcorrencia.Remessa;                    
                    break;
                case OcorrenciasPadrao.RegistrarComDebitoAutomatico:
                    if (Banco != 237)
                        throw new ArgumentException("Ocorrencia",string.Format("Débito automático só para o Bradesco! NossoNumero (ID) = {0}", NossoNumero));
                    IDOcorrencia = (int)BoletoRemessaBra.TiposOcorrencia.Remessa;                    
                    DARazaoConta = 07050;
                    RegistrarDA = true;
                    break;
                case OcorrenciasPadrao.AlteracaoDeData:
                    IDOcorrencia = (Banco == 237) ? (int)BoletoRemessaBra.TiposOcorrencia.AlteracaoVencimento : (int)BoletoRemessaItau.TiposOcorrencia.AlteracaoVencimento;                    
                    break;
                case OcorrenciasPadrao.ConcessaoAbatimento:
                    IDOcorrencia = (Banco == 237) ? (int)BoletoRemessaBra.TiposOcorrencia.ConcessaoAbatimento : (int)BoletoRemessaItau.TiposOcorrencia.ConcessaoAbatimento;                                        
                    break;                
                default:
                    break;
            }
            EspecieTitulo = (Banco == 237) ? (int)BoletoRemessaBra.TiposEspecieTitulo.Outros : (int)BoletoRemessaItau.TiposEspecieTitulo.Condominio;            
            Instrucao1 = (int)BoletoRemessaBra.TiposInstrucao.BaixaDecursoPrazo;
            Instrucao2 = DiasAposVencimento;                    
           
            Remessa.Boletos.Add(this);            
            return true;
        }
    }

    /// <summary>
    /// Rateio de crédito
    /// </summary>
    public class RateioCredito
    {
        /// <summary>
        /// Agencia Rateio
        /// </summary>
        public int AgRat;
        /// <summary>
        /// Digito Agencia Rateio
        /// </summary>
        public string DgAgRat;
        /// <summary>
        /// Conta Rateio
        /// </summary>
        public int ContaRat;
        /// <summary>
        /// Digito conta rateio
        /// </summary>
        public string DgContaRat;
        /// <summary>
        /// Valor ratear
        /// </summary>
        public decimal ValRat;
        /// <summary>
        /// Favorecido rateio
        /// </summary>
        public string NomeRat;

        /// <summary>
        /// Construtor
        /// </summary>
        /// <param name="_AgRat"></param>
        /// <param name="_DgAgRat"></param>
        /// <param name="_ContaRat"></param>
        /// <param name="_DgContaRat"></param>
        /// <param name="_ValRat"></param>
        /// <param name="_NomeRat"></param>
        public RateioCredito(int _AgRat, int? _DgAgRat, int _ContaRat, string _DgContaRat, decimal _ValRat, string _NomeRat)
        {
            AgRat = _AgRat;
            DgAgRat = _DgAgRat.HasValue ? _DgAgRat.ToString() : " ";
            ContaRat = _ContaRat;
            DgContaRat = _DgContaRat ?? " ";
            ValRat = _ValRat;
            NomeRat = _NomeRat;
        }
    }
}
