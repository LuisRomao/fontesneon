﻿/*
MR - 25/02/2016 11:00 -           - Padronização Arquivo de Cobrança Bradesco / Itau
MR - 21/03/2016 20:00 -           - Padronização Arquivo de Cobrança Bradesco / Itau (Alterações indicadas por *** MRC - INICIO (21/03/2016 20:00) ***)
*/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
//*** MRC - INICIO (21/03/2016 20:00) ***
using System.Collections;
using ArquivosTXT;
//*** MRC - TERMINO (21/03/2016 20:00) ***
using EDIBoletos.RegistrosOBJ;

namespace EDIBoletos
{
    /// <summary>
    /// Classe para retorno de cobrança
    /// </summary>
    /// <remarks>O tratamento do retorno hoje está sendo feito diretamente dentro da dllBanco / francesinha</remarks>
    public class BoletoRetornoBra : BoletoRetorno
    {
        /// <summary>
        /// Ocorrencias do retorno
        /// </summary>
        public enum TiposOcorrencia
        {
            /// <summary>
            /// Registrado
            /// </summary>
            EntradaConfirmada = 2,
            /// <summary>
            /// Erro no registro
            /// </summary>
            EntradaRejeitada = 3,
            /// <summary>
            /// Boleto pago
            /// </summary>
            LiquidacaoNormal = 6,
            /// <summary>
            /// Não usado
            /// </summary>
            BaixadoAutomatVArquivo = 9,
            /// <summary>
            /// Não usado
            /// </summary>
            BaixadoAgencia = 10,
            /// <summary>
            /// Não usado
            /// </summary>
            Pendente = 11,
            /// <summary>
            /// Não usado
            /// </summary>
            AbatimentoConcedido = 12,
            /// <summary>
            /// Não usado
            /// </summary>
            AbatimentoCancelado = 13,
            /// <summary>
            /// Utilizamos a alteração de outros dados
            /// </summary>
            VencimentoAlterado = 14,
            /// <summary>
            /// Utilizamos a alteração de outros dados
            /// </summary>
            LiquidacaoCartorio = 15,
            /// <summary>
            /// Não usado
            /// </summary>
            TituloChequeVinculado = 16,
            /// <summary>
            /// Não usado
            /// </summary>
            LiquidacaoNaoRegistrado = 17,
            /// <summary>
            /// Não usado
            /// </summary>
            AcertoDepositaria = 18,
            /// <summary>
            /// Não usado
            /// </summary>
            ConfRecProtesto = 19,
            /// <summary>
            /// Não usado
            /// </summary>
            ConfRecInstProtesto = 20,
            /// <summary>
            /// Não usado
            /// </summary>
            AcertoParticipante = 21,
            /// <summary>
            /// Não usado
            /// </summary>
            TituloComPagamentoCancelado = 22,
            /// <summary>
            /// Não usado
            /// </summary>
            EntradaTituloCartorio = 23,
            /// <summary>
            /// Não usado
            /// </summary>
            EntradaRejeitadaCEP = 24,
            /// <summary>
            /// Não usado
            /// </summary>
            ConfRecInstProtestoF = 25,
            /// <summary>
            /// Não usado
            /// </summary>
            BaixaRejeitada = 27,
            /// <summary>
            /// Não usado
            /// </summary>
            DebitoTarifas = 28,
            /// <summary>
            /// Não usado
            /// </summary>
            Sacado = 29,
            /// <summary>
            /// Alteração do boleto rejeitada
            /// </summary>
            AlteracaoOutrosRejeitada = 30,
            /// <summary>
            /// Alteração do boleto rejeitada
            /// </summary>
            InstrucaoRejeitada = 32,
            /// <summary>
            /// Alteração confirmada
            /// </summary>
            ConfPedidoAlterOutros = 33,
            /// <summary>
            /// Não usado
            /// </summary>
            RetiradoCartorio = 34,
            /// <summary>
            /// Não usado
            /// </summary>
            DesagendamentoDebitoAutomatico = 35,
            /// <summary>
            /// Não usado
            /// </summary>
            Estorno = 40,
            /// <summary>
            /// Não usado
            /// </summary>
            SustadoJudicial = 55,
            /// <summary>
            /// Não usado
            /// </summary>
            AcertoRateioCredito = 68,
            /// <summary>
            /// Não usado
            /// </summary>
            CancelamentDadosRateio = 69,
            /// <summary>
            /// protesto aceito
            /// </summary>
            ProtestoAceito = 73,
            /// <summary>
            /// Cancelamento do protesto aceito 
            /// </summary>
            CanProtestoAceito = 74,
        }

        
        /// <summary>
        /// 
        /// </summary>
        public regHeader_0_2_BRA Header;
        /// <summary>
        /// 
        /// </summary>
        public regTrailer_9_2_BRA Trailer;
        /// <summary>
        /// 
        /// </summary>
        public ColBoletos_2_BRA Boletos;

        private BoletoRegistroBase reg;
        private int nLinha = 0;
        private bool CargaOk = false;

        private cTabelaTXT _Arquivo;
        private cTabelaTXT Arquivo
        {
            get
            {
                if (_Arquivo == null) _Arquivo = new cTabelaTXT(ModelosTXT.layout);
                _Arquivo.Formatodata = FormatoData.ddMMyyyy;
                return _Arquivo;
            }
        }

        private BoletoRegistroBase ProximoRegistro(string RegistroEsperado)
        {
            string Linha = Arquivo.ProximaLinha();
            if (Linha.Length != 400)
            {
                UltimoErro = new Exception(string.Format("Linha com tamanho incorreto. Esperado 400 encontrado {0}\r\nLinha:{1} <{2}>", Linha.Length, nLinha, Linha));
                return null;
            }
            if ((Linha == null) || (Linha == ""))
            {
                UltimoErro = new Exception(string.Format("Término inesperado de arquivo\r\nRegisto(s) esperado(s): {0}", RegistroEsperado));
                return null;
            }
            if (reg == null)
            {
                reg = new BoletoRegistroBase(Arquivo);
                reg.CarregaLinha(Linha);
            }
            else
                reg.Decodificalinha(Linha);
            nLinha++;
            return reg;
        }

        /// <summary>
        /// 
        /// </summary>
        public override bool Carrega(string NomeArquivo)
        {
            UltimoErro = null;
            try
            {
                CargaOk = _Carrega(NomeArquivo);
            }
            finally
            {
                if (_Arquivo != null) _Arquivo.LiberaArquivo();
            }
            return CargaOk;
        }

        /// <summary>
        /// 
        /// </summary>
        public string NomeArquivo
        {
            get { return Arquivo.NomeArquivo; }
        }

        private bool _Carrega(string NomeArquivo)
        {
            Boletos = new ColBoletos_2_BRA();
            Arquivo.NomeArquivo = NomeArquivo;
            int nRegistro = 1;
            if (ProximoRegistro("0") == null)
                return false;
            if (reg.Registro == 0)
            {
                Header = new regHeader_0_2_BRA(Arquivo, reg.LinhaLida);
                if (ProximoRegistro("1 9") == null)
                    return false;
                //CompontesBasicos.Performance.Performance.PerformanceST.Gergistra("Geral");
                regTransacao_1_2_BRA regTrans = null;
                while (reg.Registro != 9)
                {
                    switch (reg.Registro)
                    {
                        case 1:
                            //CompontesBasicos.Performance.Performance.PerformanceST.Gergistra("Cria regTrans");
                            regTrans = new regTransacao_1_2_BRA(Arquivo, reg.LinhaLida);
                            //CompontesBasicos.Performance.Performance.PerformanceST.Gergistra("add boll");
                            Boletos.Add(regTrans);
                            //CompontesBasicos.Performance.Performance.PerformanceST.Gergistra("Geral");
                            nRegistro++;
                            if (regTrans.Sequencial != nRegistro)
                            {
                                UltimoErro = new Exception(string.Format("Registro fora de ordem: Esperado: {0} Encontrado: {1}\r\nLinha:{2} <{3}>", nRegistro, regTrans.Sequencial, nLinha, reg.LinhaLida));
                                return false;
                            };
                            break;
                        case 3:
                            if (regTrans == null)
                            {
                                UltimoErro = new Exception(string.Format("Registro fora de ordem. Esperado = 1 encontrado = {0}\r\nLinha:{1} <{2}>", reg.Registro, nLinha, reg.LinhaLida));
                                return false;
                            }
                            regTransacao_3_2_BRA regRateio = new regTransacao_3_2_BRA(Arquivo, reg.LinhaLida);
                            regTrans.LinhasRateios.Add(regRateio);
                            nRegistro++;
                            if (regRateio.Sequencial != nRegistro)
                            {
                                UltimoErro = new Exception(string.Format("Registro fora de ordem: Esperado: {0} Encontrado: {1}\r\nLinha:{2} <{3}>", nRegistro, regTrans.Sequencial, nLinha, reg.LinhaLida));
                                return false;
                            };
                            break;
                        default:
                            UltimoErro = new Exception(string.Format("Registro fora de ordem. Esperado = 1 encontrado = {0}\r\nLinha:{1} <{2}>", reg.Registro, nLinha, reg.LinhaLida));
                            return false;
                    }
                    if (ProximoRegistro("1 3 9") == null)
                        return false;
                    //if (Teste && nRegistro > 100)
                    //    break;
                }
                Trailer = new regTrailer_9_2_BRA(Arquivo, reg.LinhaLida);
                //System.Windows.Forms.Clipboard.SetText(CompontesBasicos.Performance.Performance.PerformanceST.Relatorio());
                return true;
            }
            else
            {
                UltimoErro = new Exception(string.Format("Registro inicial incorreto. Esperado 0 encontrado {0}\r\n Linha {1} <{2}>", reg.Registro, nLinha, reg.LinhaLida));
                return false;
            }
        }

        /// <summary>
        /// Relatório
        /// </summary>
        /// <returns></returns>
        public override string Relatorio()
        {
            StringBuilder relatorio = new StringBuilder();
            relatorio.AppendFormat("Arquivo {0}: ", Arquivo.NomeArquivo);

            if (CargaOk)
            {
                relatorio.AppendFormat("\r\n\r\n{0}\r\n\r\nBanco: {1}\r\nEmpresa: {4} - {5}\r\nGeracao:{2:dd/MM/yyyy}\r\nCredito:{3:dd/MM/yyyy}\r\nRegistros: {6}\r\n\r\n",
                                           Header.CodigoRemRet == 1 ? "REMESSA" : "RETORNO",
                                           Header.NumeroBanco,
                                           Header.DataGeracao,
                                           Header.DataCredito,
                                           Header.CodEmpresa,
                                           Header.NomeEmpresa,
                                           Boletos.Count);

                foreach (regTransacao_1_2_BRA trans in Boletos)
                {
                    relatorio.AppendFormat("        BOL {0}-{13} Vencimento {1:dd/MM/yyyy} Valor {2:n2} Tarifas {3:n2} {4:n2} Data Crédito {5:dd/MM/yyyy} Valor Pago {6:n2} ID Empresa {7} Carteira {9} Agencia/Conta {10}/{11}-{12} Informacao Adicional {8}\r\n",
                                               trans.NossoNumero,
                                               trans.Vencimento,
                                               trans.Valor,
                                               trans.Tarifa1,
                                               trans.Tarifa2,
                                               trans.DataCredito,
                                               trans.ValorPago,
                                               trans.IDEmpresa,
                                               trans.IndicadorRateio,
                                               trans.Carteira,
                                               trans.Agencia,
                                               trans.Conta,
                                               trans.DigConta,
                                               trans.DigNossoNumero);
                    relatorio.AppendFormat("        -> IdOOcorrencia - Motivo : {0} - {1}\r\n", trans.IDOcorrencia, trans.MotivoOcorrencia);
                    if (trans.LinhasRateios.Count > 0)
                        foreach (RateioCred Rat in trans.Rateios)
                            switch (Rat.StatusRat)
                            {
                                case 0:
                                    relatorio.AppendFormat("             -> Rateio Cadastrado :  Valor {0,3:n2}\r\n", Rat.Valor);
                                    break;
                                case 38:
                                    relatorio.AppendFormat("             -> Rateio Programado : Data {0:dd/MM/yyyy} Valor {1,3:n2}\r\n",Rat.DataCred.Value,Rat.Valor);
                                    break;
                                case 39:
                                    relatorio.AppendFormat("             -> Rateio Efetuado : Data {0:dd/MM/yyyy} Valor {1,3:n2}\r\n", Rat.DataCred.Value, Rat.Valor);
                                    break;
                                default:
                                    relatorio.AppendFormat("             -> Rateio não tratado : Código {0} Data {1:dd/MM/yyyy} Valor {2,3:n2}\r\n", Rat.StatusRat, Rat.DataCred.GetValueOrDefault(DateTime.MinValue), Rat.Valor);
                                    break;
                            }
                                                            
                }

                relatorio.AppendFormat("\r\nQtd Titulos: {0}\r\nValor Titulos: {1:n2}\r\n",
                                           Trailer.QtdTitulos,
                                           Trailer.ValorTitulos);
                relatorio.AppendFormat("\r\nQtd Entrada: {0}\r\nValor Entrada: {1:n2}\r\n",
                                           Trailer.QtdEntrada,
                                           Trailer.ValorEntrada);
                relatorio.AppendFormat("\r\nQtd Liquidacao: {0}\r\nValor Liquidação: {1:n2}\r\n",
                                           Trailer.QtdLiquidacao,
                                           Trailer.ValorLiquidacao);
                relatorio.AppendFormat("\r\nQtd Baixa: {0}\r\nValor Baixa: {1:n2}\r\n",
                                           Trailer.QtdTitulosBaixa,
                                           Trailer.ValorTitulosBaixa);
                relatorio.AppendFormat("\r\nQtd Vencimento Alterado: {0}\r\nValor Vencimento Alterado: {1:n2}\r\n",
                                           Trailer.QtdVencAlterado,
                                           Trailer.ValorVencAlterado);
                relatorio.AppendFormat("\r\nQtd Abatimento Concedido: {0}\r\nValor Abatimento Concedido: {1:n2}\r\n",
                                           Trailer.QtdAbateConcedido,
                                           Trailer.ValorAbateConcedido);
                relatorio.AppendFormat("\r\nQtd Protesto: {0}\r\nValor Protesto: {1:n2}\r\n",
                                           Trailer.QtdProtesto,
                                           Trailer.ValorProtesto);
            }
            else
            {
                relatorio.AppendFormat("Erro\r\n{0}", UltimoErro.Message);
            }
            return relatorio.ToString();
        }
    }

   /// <summary>
   /// 
   /// </summary>
   public class regHeader_0_2_BRA : regHeader_0_2
   {
       #region Variaveis Tipadas

       /// <summary>
       /// Codigo da Empresa (somente Bradesco)
       /// </summary>
       public int CodEmpresa;

       /// <summary>
       /// 
       /// </summary>
       public string DataCredito;

       #endregion

       /// <summary>
       /// 
       /// </summary>
       public regHeader_0_2_BRA(cTabelaTXT _cTabelaTXT, string Linha)
           : base(_cTabelaTXT, Linha)
       {
       }

       /// <summary>
       /// 
       /// </summary>
       protected override void RegistraMapa()
       {
           base.RegistraMapa();
           RegistraFortementeTipado("CodEmpresa", 27, 20);
           RegistraFortementeTipado("DataCredito", 380, 6);
       }
   }

    /// <summary>
    /// Objeto que representa uma linha transação 1 - boleto no retorno
    /// </summary>
    public class regTransacao_1_2_BRA : regTransacao_1_2
    {
        private List<regTransacao_3_2_BRA> _LinhasRateios;

        /// <summary>
        /// 
        /// </summary>
        public List<regTransacao_3_2_BRA> LinhasRateios { get { return _LinhasRateios ?? (_LinhasRateios = new List<regTransacao_3_2_BRA>()); } }

        private List<RateioCred> _Rateios;

        private void UnidadeRateio(decimal _Valor,int _StatusRat,DateTime _DataCred)
        {
            if (_StatusRat == 43)
                return;
            switch (IDOcorrencia)
            {
                case 2:
                    _Rateios.Add(new RateioCred() { StatusRat = _StatusRat, Valor = _Valor });
                    break;
                case 6:
                case 15:
                    _Rateios.Add(new RateioCred() { StatusRat = _StatusRat , Valor = _Valor, DataCred = _DataCred});
                    break;
            }                            
        }

        /// <summary>
        /// Rateios de Credito
        /// </summary>
        public List<RateioCred> Rateios
        {
            get
            {
                if (_Rateios == null)
                {
                    _Rateios = new List<RateioCred>();
                    foreach (regTransacao_3_2_BRA regRat in LinhasRateios)
                    {
                        UnidadeRateio(regRat.Valor1, regRat.StatusRat1, regRat.DataCred1);
                        UnidadeRateio(regRat.Valor2, regRat.StatusRat2, regRat.DataCred2);
                        UnidadeRateio(regRat.Valor3, regRat.StatusRat3, regRat.DataCred3);
                    }
                };
                return _Rateios;
            }
        }

        #region Variaveis Tipadas

        /// <summary>
        /// 
        /// </summary>
        public int TipoInscricao;
        /// <summary>
        /// 
        /// </summary>
        public int NumInscricao;
        /// <summary>
        /// 
        /// </summary>
        public string IDEmpresa;
        /// <summary>
        /// 
        /// </summary>
        public int Agencia;
        /// <summary>
        /// 
        /// </summary>
        public int Conta;
        /// <summary>
        /// 
        /// </summary>
        public string DigConta;
        /// <summary>
        /// 
        /// </summary>
        public string NossoNumero;
        /// <summary>
        /// 
        /// </summary>
        public string DigNossoNumero;
        /// <summary>
        /// 
        /// </summary>
        public string IndicadorRateio;
        /// <summary>
        /// 
        /// </summary>
        public int Carteira;
        /// <summary>
        /// 
        /// </summary>
        public decimal Tarifa2;
        /// <summary>
        /// 
        /// </summary>
        public decimal Juros;
        /// <summary>
        /// 
        /// </summary>
        public string MotivoProtesto;
        /// <summary>
        /// 
        /// </summary>
        public int OrigemPagto;
        /// <summary>
        /// 
        /// </summary>
        public int ChequeBanco;
        /// <summary>
        /// 
        /// </summary>
        public string MotivoOcorrencia;
        /// <summary>
        /// 
        /// </summary>
        public int Cartorio;
        /// <summary>
        /// 
        /// </summary>
        public string Protocolo;

        #endregion

        /// <summary>
        /// 
        /// </summary>
        public regTransacao_1_2_BRA(cTabelaTXT _cTabelaTXT, string Linha)
            : base(_cTabelaTXT, Linha)
        {
        }

        /// <summary>
        /// 
        /// </summary>
        protected override void RegistraMapa()
        {
            base.RegistraMapa();
            RegistraFortementeTipado("TipoInscricao", 2, 2);
            RegistraFortementeTipado("NumInscricao", 4, 14);
            RegistraFortementeTipado("IDEmpresa", 21, 17);
            RegistraFortementeTipado("Agencia", 25, 5);
            RegistraFortementeTipado("Conta", 30, 7);
            RegistraFortementeTipado("DigConta", 37, 1);
            RegistraFortementeTipado("NossoNumero", 71, 11);
            RegistraFortementeTipado("DigNossoNumero", 82, 1);
            RegistraFortementeTipado("IndicadorRateio", 105, 1);
            RegistraFortementeTipado("Carteira", 108, 1);
            RegistraFortementeTipado("Tarifa2", 189, 13);
            RegistraFortementeTipado("Juros", 202, 13);
            RegistraFortementeTipado("MotivoProtesto", 295, 1);
            RegistraFortementeTipado("OrigemPagto", 302, 3);
            RegistraFortementeTipado("ChequeBanco", 315, 4);
            RegistraFortementeTipado("MotivoOcorrencia", 319, 10);
            RegistraFortementeTipado("Cartorio", 369, 2);
            RegistraFortementeTipado("Protocolo", 371, 10);
        }
    }

    /// <summary>
    /// RateioDeCredito
    /// </summary>
    public class RateioCred
    {
        /// <summary>
        /// Valor
        /// </summary>
        public decimal Valor;
        /// <summary>
        /// Status
        /// </summary>
        public int StatusRat;
        /// <summary>
        /// Data o crédito
        /// </summary>
        public DateTime? DataCred;
    }

    /*
      ATENÇÂO o retonro do rateio de crédito foi implantado diretamento no BoletoRetornoBRA caso venha a ser feito no Itaú poderá ser criado objeto regTransacao_3_2 : regTransacao_3
    */

    /// <summary>
    /// Objeto que represnta o rateiro de crédito - retorno
    /// </summary>
    public class regTransacao_3_2_BRA : regTransacao_3
    {
        #region Variaveis Tipadas

        /// <summary>
        /// Valor efetivo 1
        /// </summary>
        public decimal Valor1;

        /// <summary>
        /// Valor efetivo 2
        /// </summary>
        public decimal Valor2;

        /// <summary>
        /// Valor efetivo 3
        /// </summary>
        public decimal Valor3;

        /// <summary>
        /// Data 1
        /// </summary>
        public DateTime DataCred1;

        /// <summary>
        /// Data 2
        /// </summary>
        public DateTime DataCred2;

        /// <summary>
        /// Data 3
        /// </summary>
        public DateTime DataCred3;

        /// <summary>
        /// Status 1
        /// </summary>
        public int StatusRat1;

        /// <summary>
        /// Status 2
        /// </summary>
        public int StatusRat2;

        /// <summary>
        /// Status 3
        /// </summary>
        public int StatusRat3;

        #endregion

        /// <summary>
        /// Construtor
        /// </summary>
        /// <param name="_cTabelaTXT"></param>
        /// <param name="Linha"></param>
        public regTransacao_3_2_BRA(cTabelaTXT _cTabelaTXT, string Linha) : base(_cTabelaTXT, Linha)
        {            
        }

        /// <summary>
        /// 
        /// </summary>
        protected override void RegistraMapa()
        {
            base.RegistraMapa();
            RegistraFortementeTipado("Valor1"    ,  66, 15);
            RegistraFortementeTipado("DataCred1" , 151,  8);
            RegistraFortementeTipado("StatusRat1", 159,  2);
            RegistraFortementeTipado("Valor2"    , 183, 15);
            RegistraFortementeTipado("DataCred2" , 268,  8);
            RegistraFortementeTipado("StatusRat2", 276,  2);
            RegistraFortementeTipado("Valor3"    , 300, 15);
            RegistraFortementeTipado("DataCred3" , 385,  8);
            RegistraFortementeTipado("StatusRat3", 393,  2);
        }
    }

    /// <summary>
    /// 
    /// </summary>
    public class regTrailer_9_2_BRA : regTrailer_9_2
   {
       #region Variaveis Tipadas

       /// <summary>
       /// 
       /// </summary>
       public int QtdEntrada;
       /// <summary>
       /// 
       /// </summary>
       public decimal ValorEntrada;
       /// <summary>
       /// 
       /// </summary>
       public decimal ValorLiquidacao;
       /// <summary>
       /// 
       /// </summary>
       public int QtdLiquidacao;
       /// <summary>
       /// 
       /// </summary>
       public decimal ValorOcorr6;
       /// <summary>
       /// 
       /// </summary>
       public int QtdTitulosBaixa;
       /// <summary>
       /// 
       /// </summary>
       public decimal ValorTitulosBaixa;
       /// <summary>
       /// 
       /// </summary>
       public int QtdAbateCancelado;
       /// <summary>
       /// 
       /// </summary>
       public decimal ValorAbateCancelado;
       /// <summary>
       /// 
       /// </summary>
       public int QtdVencAlterado;
       /// <summary>
       /// 
       /// </summary>
       public decimal ValorVencAlterado;
       /// <summary>
       /// 
       /// </summary>
       public int QtdAbateConcedido;
       /// <summary>
       /// 
       /// </summary>
       public decimal ValorAbateConcedido;
       /// <summary>
       /// 
       /// </summary>
       public int QtdProtesto;
       /// <summary>
       /// 
       /// </summary>
       public decimal ValorProtesto;
       /// <summary>
       /// 
       /// </summary>
       public decimal ValorRateio;
       /// <summary>
       /// 
       /// </summary>
       public int QtdRateio;

       #endregion

       /// <summary>
       /// 
       /// </summary>
       public regTrailer_9_2_BRA(cTabelaTXT _cTabelaTXT, string Linha)
           : base(_cTabelaTXT, Linha)
       {
       }

       /// <summary>
       /// 
       /// </summary>
       protected override void RegistraMapa()
       {
           base.RegistraMapa();
           RegistraFortementeTipado("QtdEntrada", 58, 5);
           RegistraFortementeTipado("ValorEntrada", 63, 12);
           RegistraFortementeTipado("ValorLiquidacao", 75, 12);
           RegistraFortementeTipado("QtdLiquidacao", 87, 5);
           RegistraFortementeTipado("ValorOcorr6", 92, 12);
           RegistraFortementeTipado("QtdTitulosBaixa", 104, 5);
           RegistraFortementeTipado("ValorTitulosBaixa", 109, 12);
           RegistraFortementeTipado("QtdAbateCancelado", 121, 5);
           RegistraFortementeTipado("ValorAbateCancelado", 126, 12);
           RegistraFortementeTipado("QtdVencAlterado", 138, 5);
           RegistraFortementeTipado("ValorVencAlterado", 143, 12);
           RegistraFortementeTipado("QtdAbateConcedido", 155, 5);
           RegistraFortementeTipado("ValorAbateConcedido", 160, 12);
           RegistraFortementeTipado("QtdProtesto", 172, 5);
           RegistraFortementeTipado("ValorProtesto", 177, 12);
           RegistraFortementeTipado("ValorRateio", 363, 15);
           RegistraFortementeTipado("QtdRateio", 378, 8);
       }
   }

   /// <summary>
   /// 
   /// </summary>
   public class ColBoletos_2_BRA : CollectionBase
   {
       /// <summary>
       /// 
       /// </summary>
       public regTransacao_1_2_BRA this[int index]
       {
           get
           {
               return ((regTransacao_1_2_BRA)List[index]);
           }
           set
           {
               List[index] = value;
           }
       }

       /// <summary>
       /// 
       /// </summary>
       public int Add(regTransacao_1_2_BRA value)
       {
           return (List.Add(value));
       }

       /// <summary>
       /// 
       /// </summary>
       public int IndexOf(regTransacao_1_2_BRA value)
       {
           return (List.IndexOf(value));
       }

       /// <summary>
       /// 
       /// </summary>
       public void Insert(int index, regTransacao_1_2_BRA value)
       {
           List.Insert(index, value);
       }

       /// <summary>
       /// 
       /// </summary>
       public void Remove(regTransacao_1_2_BRA value)
       {
           List.Remove(value);
       }

       /// <summary>
       /// 
       /// </summary>
       public bool Contains(regTransacao_1_2_BRA value)
       {
           // If value is not of type Int16, this will return false.
           return (List.Contains(value));
       }

       /// <summary>
       /// 
       /// </summary>
       protected override void OnValidate(Object value)
       {
           if (value.GetType() != typeof(regTransacao_1_2_BRA))
               throw new ArgumentException("Value must be of type regTransacao_1_2_BRA.", "value");
       }
   } 
  //*** MRC - TERMINO (21/03/2016 20:00) ***
}
