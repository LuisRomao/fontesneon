﻿/*
MR - 03/12/2014 10:00 -           - Ajuste no nome do arquivo para utilizar via Robo (Alterações indicadas por *** MRC - INICIO - TRIBUTOS (03/12/2014 10:00) ***)
MR - 04/12/2014 10:00 -           - Retirada de warnings de XML
*/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Collections;
using ArquivosTXT;
using BoletosCalc;
using DocBacarios;
using EDIBoletos.RegistrosOBJTributo;
using Abstratos;

namespace EDIBoletos
{
   /// <summary>
   /// Classe Tributo Remessa Bradesco
   /// </summary>
   public class TributoRemessaBra : TributoRemessaBase 
   {
      private regHeader_0_1_TRIBUTO_BRA Header;
      private regTrailer_9_1_TRIBUTO_BRA Trailer;
      private decimal TotalValorPagamento;

      /// <summary>
      /// Inicia Remessa
      /// </summary>
      public override void IniciaRemessa()
      {
         Tributos = new ColTributos();
         DataGeracao = DateTime.Now.ToString("yyyyMMdd");
         HoraGeracao = DateTime.Now.ToString("HHmmss");
         ArquivoSaida = new cTabelaTXT(ModelosTXT.layout);
         ArquivoSaida.Formatodata = FormatoData.ddMMyyyy;
      }

      /// <summary>
      /// Insere Tributo
      /// </summary>
      public void InsereTributos()
      {
         TotalValorPagamento = 0;
         foreach (ABS_Tributo tributo in Tributos)
         {
            switch (tributo.TipoTributo)
            {
                case (int)TributoRemessaBra.TiposTipoTributo.DARF:
                    regTransacao_1_1_TRIBUTO_001_BRA Tributo001 = new regTransacao_1_1_TRIBUTO_001_BRA(Header);
                    Tributo001.TipoTributo = tributo.TipoTributo;
                    Tributo001.IDDocumento = tributo.IDDocumento;
                    Tributo001.TipoMovimento = tributo.TipoMovimento;
                    Tributo001.CPFCNPJContribuinte = tributo.CPFCNPJContribuinte;
                    Tributo001.NomeContribuinte = tributo.NomeContribuinte;
                    Tributo001.EnderecoContribuinte = tributo.EnderecoContribuinte;
                    Tributo001.CepContribuinte = tributo.CEPContribuinte;
                    Tributo001.DAAgencia = tributo.DAAgencia;
                    Tributo001.DADigAgencia = tributo.DADigAgencia;
                    Tributo001.DANumConta = tributo.DANumConta;
                    Tributo001.DADigNumConta = tributo.DADigNumConta;
                    Tributo001.DataDebito = tributo.DataDebito;
                    Tributo001.Valor = tributo.Valor;
                    Tributo001.Juros = tributo.Juros;
                    Tributo001.Multa = tributo.Multa;
                    Tributo001.DataVencimento = tributo.DataVencimento;
                    Tributo001.CodReceita = tributo.CodReceita;
                    Tributo001.PeriodoApuracao = tributo.PeriodoApuracao;
                    Tributo001.Percentual = tributo.Percentual;
                    Tributo001.Referencia = tributo.Referencia;
                    Tributo001.ValorReceitaBruta = tributo.ValorReceitaBruta;
                    Tributo001.UsoEmpresa = tributo.UsoEmpresa;
                    TotalValorPagamento = TotalValorPagamento + (Tributo001.Valor + Tributo001.Juros + Tributo001.Multa);
                    break;
                case (int)TributoRemessaBra.TiposTipoTributo.GPS:
                    regTransacao_1_1_TRIBUTO_005_BRA Tributo005 = new regTransacao_1_1_TRIBUTO_005_BRA(Header);
                    Tributo005.TipoTributo = tributo.TipoTributo;
                    Tributo005.IDDocumento = tributo.IDDocumento;
                    Tributo005.TipoMovimento = tributo.TipoMovimento;
                    Tributo005.NomeContribuinte = tributo.NomeContribuinte;
                    Tributo005.EnderecoContribuinte = tributo.EnderecoContribuinte;
                    Tributo005.CepContribuinte = tributo.CEPContribuinte;
                    Tributo005.DAAgencia = tributo.DAAgencia;
                    Tributo005.DADigAgencia = tributo.DADigAgencia;
                    Tributo005.DANumConta = tributo.DANumConta;
                    Tributo005.DADigNumConta = tributo.DADigNumConta;
                    Tributo005.DataDebito = tributo.DataDebito;
                    Tributo005.Valor = tributo.Valor;
                    Tributo005.Juros = tributo.Juros;
                    Tributo005.OutrasEntidades = tributo.OutrasEntidades;
                    Tributo005.CodINSS = tributo.CodINSS;
                    Tributo005.CPFCNPJContribuinte = tributo.CPFCNPJContribuinte;
                    Tributo005.DataVencimento = tributo.DataVencimento;
                    Tributo005.CompetenciaMes = tributo.CompetenciaMes;
                    Tributo005.CompetenciaAno = tributo.CompetenciaAno;
                    Tributo005.UsoEmpresa = tributo.UsoEmpresa;
                    TotalValorPagamento = TotalValorPagamento + (Tributo005.Valor + Tributo005.Juros + Tributo005.OutrasEntidades);
                    break;
                case (int)TributoRemessaBra.TiposTipoTributo.CodigoBarras:
                    regTransacao_1_1_TRIBUTO_006_BRA Tributo006 = new regTransacao_1_1_TRIBUTO_006_BRA(Header);
                    Tributo006.TipoTributo = tributo.TipoTributo;
                    Tributo006.IDDocumento = tributo.IDDocumento;
                    Tributo006.TipoMovimento = tributo.TipoMovimento;
                    Tributo006.CodigoBarra = tributo.CodigoBarra;
                    Tributo006.DataDebito = tributo.DataDebito;
                    Tributo006.Valor = tributo.Valor;
                    Tributo006.DAAgencia = tributo.DAAgencia;
                    Tributo006.DADigAgencia = tributo.DADigAgencia;
                    Tributo006.DANumConta = tributo.DANumConta;
                    Tributo006.DADigNumConta = tributo.DADigNumConta;
                    Tributo006.DataVencimento = tributo.DataVencimento;
                    Tributo006.UsoEmpresa = tributo.UsoEmpresa;
                    TotalValorPagamento = TotalValorPagamento + (Tributo006.Valor);
                    break;
                default:
                    break;
            }
         }
      }

      private void CriaHeader()
      {
         Header = new regHeader_0_1_TRIBUTO_BRA(ArquivoSaida);
         Header.CodComunicacao = CodComunicacao;
         Header.CPFCNPJ = CPFCNPJ;
         Header.NomeEmpresa = NomeEmpresa;
         Header.DataGeracao = DataGeracao;
         Header.HoraGeracao = HoraGeracao;
         Header.SequencialRemessa = SequencialRemessa;
      }

      private void CriaTrailer()
      {
         Trailer = new regTrailer_9_1_TRIBUTO_BRA(Header, TotalValorPagamento);
      }

      /// <summary>
      /// Grava o arquivo
      /// </summary>
      /// <param name="Path">Path do Arquivo</param>
      /// <param name="Arquivo">Nome do Arquivo</param>
      /// <returns></returns>
      public override string GravaArquivo(string Path, string Arquivo)
      {
         try
         {
            string NomeArquivo = Arquivo;
            if (NomeArquivo == "")
                //*** MRC - INICIO - TRIBUTOS (03/12/2014 10:00) ***
                NomeArquivo = Path + "PTRB" + DateTime.Today.ToString("ddMM") + SequencialRemessaArquivo.ToString("00") + ".REM";
                //*** MRC - TERMINO - TRIBUTOS (03/12/2014 10:00) ***
            CriaHeader();
            if (Header == null)
               return "";
            else
            {
               InsereTributos();
               CriaTrailer();
               if (Header.GravaArquivo(NomeArquivo))
                   return NomeArquivo;
               else
                  return "";
            }
         }
         catch (Exception ex)
         {
            UltimoErro = ex;
            return "";
         }
      }

      /// <summary>
      /// Finaliza o Arquivo
      /// </summary>
      /// <param name="NomeArquivo"></param>
      /// <returns></returns>
      public override string FinalizaArquivo(string NomeArquivo)
      {
          try
          {
              if (Header.FinalizaArquivo(NomeArquivo))
                  return NomeArquivo;
              else
                  return "";
          }
          catch (Exception ex)
          {
              UltimoErro = ex;
              return "";
          }
      }
   }

   /// <summary>
   /// Classe Header Tributo Bradesco
   /// </summary>
   public class regHeader_0_1_TRIBUTO_BRA : regHeader_0
   {
      #region Variaveis Tipadas

         private CPFCNPJ _CPFCNPJ;
         /// <summary>
         /// CPF ou CNPJ
         /// </summary>
         public CPFCNPJ CPFCNPJ
         {
             get { return _CPFCNPJ; }
             set { _CPFCNPJ = value; }
         }

         /// <summary>
         /// Número de Inscrição
         /// </summary>
         public long NumInscricao
         {
             get
             {
                 if (CPFCNPJ == null || CPFCNPJ.Valor == 0) { return 1; }
                 else
                 {
                     switch (CPFCNPJ.Tipo)
                     {
                         case TipoCpfCnpj.CPF: return Convert.ToInt64(CPFCNPJ.Valor.ToString().Substring(0, CPFCNPJ.Valor.ToString().Length - 2) + "0000" + CPFCNPJ.Valor.ToString().Substring(CPFCNPJ.Valor.ToString().Length - 2));
                         default: return CPFCNPJ.Valor;
                     }
                 }
             }
         }

         /// <summary>
         /// Tipo do Número de Inscrição
         /// </summary>
         public int TipoInscricao
         {
             get
             {
                 if (CPFCNPJ == null) { return (int)TributoRemessaBra.TiposInscricao.Outros; }
                 else
                 {
                     switch (CPFCNPJ.Tipo)
                     {
                         case TipoCpfCnpj.CNPJ: return (int)TributoRemessaBra.TiposInscricao.CNPJ;
                         case TipoCpfCnpj.CPF: return (int)TributoRemessaBra.TiposInscricao.CPF;
                         case TipoCpfCnpj.INVALIDO: return (int)TributoRemessaBra.TiposInscricao.Outros;
                         default: return (int)TributoRemessaBra.TiposInscricao.Outros;
                     }
                 }
             }
         }

      #endregion

      /// <summary>
      /// Classe Header Tributo Bradesco (construtor)
      /// </summary>
      /// <param name="_cTabelaTXT"></param>
      public regHeader_0_1_TRIBUTO_BRA(cTabelaTXT _cTabelaTXT)
         : base(_cTabelaTXT, null)
      {
         CodOrigemArq = (int)TiposCodOrigemArq.Remessa;
         Sequencial = 1;
      }

      /// <summary>
      /// Registra Mapa
      /// </summary>
      protected override void RegistraMapa()
      {
         base.RegistraMapa();
         RegistraFortementeTipado("NumInscricao", 10, 15);
         RegistraMapa("Zero", 86, 2, 0);
      }
   }

   /// <summary>
   /// Classe Transação Tributo 001 (DARF) Bradesco
   /// </summary>
   public class regTransacao_1_1_TRIBUTO_001_BRA : regTransacao_1_TRIBUTO_001
   {
      #region Variaveis Tipadas

         private CPFCNPJ _CPFCNPJContribuinte;
         /// <summary>
         /// CPF ou CNPJ do Contribuinte
         /// </summary>
         public CPFCNPJ CPFCNPJContribuinte
         {
            get { return _CPFCNPJContribuinte; }
            set { _CPFCNPJContribuinte = value; }
         }

         /// <summary>
         /// Tipo de Inscrição do Contribuinte
         /// </summary>
         public int TipoInscricaoContribuinte
         {
            get
            {
               if (CPFCNPJContribuinte == null || CPFCNPJContribuinte.Valor == 0) { return (int)TributoRemessaBra.TiposInscricao.Outros; }
               else
               {
                  switch (CPFCNPJContribuinte.Tipo)
                  {
                     case TipoCpfCnpj.CNPJ: return (int)TributoRemessaBra.TiposInscricao.CNPJ;
                     case TipoCpfCnpj.CPF: return (int)TributoRemessaBra.TiposInscricao.CPF;
                     case TipoCpfCnpj.INVALIDO: return (int)TributoRemessaBra.TiposInscricao.Outros;
                     default: return (int)TributoRemessaBra.TiposInscricao.Outros;
                  }
               }
            }
         }

         /// <summary>
         /// Número de Incrição do Contribuinte
         /// </summary>
         public long NumInscricaoContribuinte
         {
            get
            {
               if (CPFCNPJContribuinte == null || CPFCNPJContribuinte.Valor == 0) { return 1; }
               else 
               {
                  switch (CPFCNPJContribuinte.Tipo)
                  {
                     case TipoCpfCnpj.CPF: return Convert.ToInt64(CPFCNPJContribuinte.Valor.ToString().Substring(0, CPFCNPJContribuinte.Valor.ToString().Length - 2) + "0000" + CPFCNPJContribuinte.Valor.ToString().Substring(CPFCNPJContribuinte.Valor.ToString().Length - 2));
                     default: return CPFCNPJContribuinte.Valor;
                  }
               }
            }
         }

         /// <summary>
         /// Data de Débito
         /// </summary>
         public DateTime DataDebito;
         private string _DtDebito;
         /// <summary>
         /// Data de Débito (com tratamento para o arquivo)
         /// </summary>
         public string DtDebito
         {
             get
             {
                 return ((DataDebito.Year == 1) || (_DtDebito == "")) ? "00000000" : DataDebito.Year.ToString("0000") + DataDebito.Month.ToString("00") + DataDebito.Day.ToString("00");
             }
             set { _DtDebito = value; }
         }

         private decimal _ValorTotal;
         /// <summary>
         /// Valor Total
         /// </summary>
         public decimal ValorTotal
         {
             get
             {
                 return Valor + Juros + Multa;
             }
             set { _ValorTotal = value; }
         }

         /// <summary>
         /// Data de Vencimento
         /// </summary>
         public DateTime DataVencimento;
         private string _DtVencimento;
         /// <summary>
         /// Data de Vencimento (com tratamento para o arquivo)
         /// </summary>
         public string DtVencimento
         {
             get
             {
                 return ((DataVencimento.Year == 1) || (_DtVencimento == "")) ? "00000000" : DataVencimento.Year.ToString("0000") + DataVencimento.Month.ToString("00") + DataVencimento.Day.ToString("00");
             }
             set { _DtVencimento = value; }
         }

         private int _TipoDarf;
         /// <summary>
         /// Tipo de DARF
         /// </summary>
         public int TipoDarf
         {
             get
             {
                 return (CodReceita == 6106) ? (int)TributoRemessaBra.TiposTipoDarf.Simples : (int)TributoRemessaBra.TiposTipoDarf.PretoEuropa;
             }
             set { _TipoDarf = value; }
         }

         /// <summary>
         /// Período de Apuração
         /// </summary>
         public DateTime PeriodoApuracao;
         private string _DtPeriodoApuracao;
         /// <summary>
         /// Período de Apuração (com tratamento para o arquivo)
         /// </summary>
         public string DtPeriodoApuracao
         {
             get
             {
                 return ((PeriodoApuracao.Year == 1) || (_DtPeriodoApuracao == "")) ? "00000000" : PeriodoApuracao.Year.ToString("0000") + PeriodoApuracao.Month.ToString("00") + PeriodoApuracao.Day.ToString("00");
             }
             set { _DtPeriodoApuracao = value; }
         }

         /// <summary>
         /// Percentual
         /// </summary>
         public decimal Percentual;
         private decimal _NrPercentual;
         /// <summary>
         /// Percentual (com tratamento para o arquivo)
         /// </summary>
         public decimal NrPercentual
         {
             get
             {
                 return (TipoDarf == (int)TributoRemessaBra.TiposTipoDarf.PretoEuropa) ? 0 : Percentual;
             }
             set { _NrPercentual = value; }
         }

         /// <summary>
         /// Referência
         /// </summary>
         public long Referencia;
         private long _NrReferencia;
         /// <summary>
         /// Referência (com tratamento para o arquivo)
         /// </summary>
         public long NrReferencia
         {
             get
             {
                 return (TipoDarf == (int)TributoRemessaBra.TiposTipoDarf.Simples) ? 0 : Referencia;
             }
             set { _NrReferencia = value; }
         }

         /// <summary>
         /// Valor da Receita Bruta
         /// </summary>
         public decimal ValorReceitaBruta;
         private decimal _VrValorReceitaBruta;
         /// <summary>
         /// Valor da Receita Bruta (com tratamento para o arquivo)
         /// </summary>
         public decimal VrValorReceitaBruta
         {
             get
             {
                 return (TipoDarf == (int)TributoRemessaBra.TiposTipoDarf.PretoEuropa) ? 0 : ValorReceitaBruta;
             }
             set { _VrValorReceitaBruta = value; }
         }

      #endregion

      private regHeader_0_1_TRIBUTO_BRA regPai;

      /// <summary>
      /// Classe Transação Tributo 001 (DARF) Bradesco (construtor)
      /// </summary>
      /// <param name="_regPai">Registro Pai (Header)</param>
      public regTransacao_1_1_TRIBUTO_001_BRA(regHeader_0_1_TRIBUTO_BRA _regPai)
         : base(_regPai.cTabTXT, null)
      {
         regPai = _regPai;
         regPai.Transacoes.Add(this);
         CodComunicacao = regPai.CodComunicacao;
         NumInscricao = regPai.NumInscricao;
         SequencialRemessa = regPai.SequencialRemessa;
         DataGeracao = regPai.DataGeracao;
         DARazaoConta = 705;
         AutorizacaoDebito = "S";       
         Sequencial = regPai.Transacoes.Count + 1;
      }

      /// <summary>
      /// Regsitra Mapa
      /// </summary>
      protected override void RegistraMapa()
      {
         base.RegistraMapa();
         RegistraFortementeTipado("NumInscricao", 10, 15);
         RegistraFortementeTipado("DtDebito", 189, 8);
         RegistraFortementeTipado("DtVencimento", 336, 8);
         RegistraFortementeTipado("DtPeriodoApuracao", 349, 8);
         RegistraFortementeTipado("NrPercentual", 357, 4);
         RegistraFortementeTipado("NrReferencia", 361, 17);
         RegistraFortementeTipado("VrValorReceitaBruta", 378, 15);
      }
   }

   /// <summary>
   /// Classe Transação Tributo 005 (GPS) Bradesco
   /// </summary>
   public class regTransacao_1_1_TRIBUTO_005_BRA : regTransacao_1_TRIBUTO_005
   {
       #region Variaveis Tipadas

       /// <summary>
       /// Data de Débito
       /// </summary>
       public DateTime DataDebito;
       private string _DtDebito;
       /// <summary>
       /// Data de Débito (com tratamento para o arquivo)
       /// </summary>
       public string DtDebito
       {
           get
           {
               return ((DataDebito.Year == 1) || (_DtDebito == "")) ? "00000000" : DataDebito.Year.ToString("0000") + DataDebito.Month.ToString("00") + DataDebito.Day.ToString("00");
           }
           set { _DtDebito = value; }
       }

       private decimal _ValorTotal;
       /// <summary>
       /// Valor Total
       /// </summary>
       public decimal ValorTotal
       {
           get
           {
               return Valor + Juros + OutrasEntidades;
           }
           set { _ValorTotal = value; }
       }

       private CPFCNPJ _CPFCNPJContribuinte;
       /// <summary>
       /// CPF ou CNPJ do Contribuinte
       /// </summary>
       public CPFCNPJ CPFCNPJContribuinte
       {
           get { return _CPFCNPJContribuinte; }
           set { _CPFCNPJContribuinte = value; }
       }

       /// <summary>
       /// Tipo de Inscrição do Contribuinte
       /// </summary>
       public string TipoInscricaoContribuinte
       {
           get
           {
               if (CPFCNPJContribuinte == null || CPFCNPJContribuinte.Valor == 0) { return ""; }
               else
               {
                   switch (CPFCNPJContribuinte.Tipo)
                   {
                       case TipoCpfCnpj.CNPJ: return "CGC";
                       case TipoCpfCnpj.CPF: return "CPF";
                       case TipoCpfCnpj.INVALIDO: return "";
                       default: return "";
                   }
               }
           }
       }

       /// <summary>
       /// Número de Inscrição do Contribuinte
       /// </summary>
       public long NumInscricaoContribuinte
       {
           get
           {
               if (CPFCNPJContribuinte == null || CPFCNPJContribuinte.Valor == 0) { return 1; }
               else
               {
                   switch (CPFCNPJContribuinte.Tipo)
                   {
                       case TipoCpfCnpj.CPF: return CPFCNPJContribuinte.Valor;
                       default: return CPFCNPJContribuinte.Valor;
                   }
               }
           }
       }

       /// <summary>
       /// Data de Vencimento
       /// </summary>
       public DateTime DataVencimento;
       private string _DtVencimento;
       /// <summary>
       /// Data de Vencimento (com tratamento para o arquivo)
       /// </summary>
       public string DtVencimento
       {
           get
           {
               return ((DataVencimento.Year == 1) || (_DtVencimento == "")) ? "00000000" : DataVencimento.Year.ToString("0000") + DataVencimento.Month.ToString("00") + DataVencimento.Day.ToString("00");
           }
           set { _DtVencimento = value; }
       }

       /// <summary>
       /// Competência - Mês
       /// </summary>
       public int CompetenciaMes;
       /// <summary>
       /// Competência - Ano
       /// </summary>
       public int CompetenciaAno;

       private string _Competencia;
       /// <summary>
       /// Competência
       /// </summary>
       public string Competencia
       {
           get
           {
               return CompetenciaAno.ToString("0000") + CompetenciaMes.ToString("00");
           }
           set { _Competencia = value; }
       }

       #endregion

       private regHeader_0_1_TRIBUTO_BRA regPai;

       /// <summary>
       /// Classe Transação Tributo 005 (GPS) Bradesco (construtor)
       /// </summary>
       /// <param name="_regPai"></param>
       public regTransacao_1_1_TRIBUTO_005_BRA(regHeader_0_1_TRIBUTO_BRA _regPai)
           : base(_regPai.cTabTXT, null)
       {
           regPai = _regPai;
           regPai.Transacoes.Add(this);
           CodComunicacao = regPai.CodComunicacao;
           NumInscricao = regPai.NumInscricao;
           SequencialRemessa = regPai.SequencialRemessa;
           DataGeracao = regPai.DataGeracao;
           DARazaoConta = 705;
           AutorizacaoDebito = "S";
           Sequencial = regPai.Transacoes.Count + 1;
       }

       /// <summary>
       /// Regsitra Mapa
       /// </summary>
       protected override void RegistraMapa()
       {
           base.RegistraMapa();
           RegistraFortementeTipado("NumInscricao", 10, 15);
           RegistraFortementeTipado("DtDebito", 188, 8);
           RegistraFortementeTipado("DtVencimento", 336, 8);
           RegistraFortementeTipado("Competencia", 347, 6);
       }
   }

   /// <summary>
   /// Classe Transação Tributo 006 (FGTS ou Conta Consumo) Bradesco
   /// </summary>
   public class regTransacao_1_1_TRIBUTO_006_BRA : regTransacao_1_TRIBUTO_006
   {
       #region Variaveis Tipadas

       /// <summary>
       /// Código de Barra
       /// </summary>
       public string CodigoBarra;
       private string _nrCodigoBarra;
       /// <summary>
       /// Código de Barras (com tratamento para o arquivo)
       /// </summary>
       public string nrCodigoBarra
       {
           get
           {
               if (CodigoBarra == null) { return ""; }
               else { return CodigoBarra.Substring(0, 11) + CodigoBarra.Substring(12, 11) + CodigoBarra.Substring(24, 11) + CodigoBarra.Substring(36, 11); }
           }
           set { _nrCodigoBarra = value; }
       }

       /// <summary>
       /// Data de Débito
       /// </summary>
       public DateTime DataDebito;
       private string _DtDebito;
       /// <summary>
       /// Data de Débito (com tratamento para o arquivo)
       /// </summary>
       public string DtDebito
       {
           get
           {
               return ((DataDebito.Year == 1) || (_DtDebito == "")) ? "00000000" : DataDebito.Year.ToString("0000") + DataDebito.Month.ToString("00") + DataDebito.Day.ToString("00");
           }
           set { _DtDebito = value; }
       }

       /// <summary>
       /// Data de Vencimento
       /// </summary>
       public DateTime DataVencimento;
       private string _DtVencimento;
       /// <summary>
       /// Data de Vencimento (com tratamento para o arquivo)
       /// </summary>
       public string DtVencimento
       {
           get
           {
               return "00000000";                
           }
           set { _DtVencimento = value; }
       }

       #endregion

       private regHeader_0_1_TRIBUTO_BRA regPai;

       /// <summary>
       /// Classe Transação Tributo 006 (FGTS ou Conta Consumo) Bradesco (construtor)
       /// </summary>
       /// <param name="_regPai"></param>
       public regTransacao_1_1_TRIBUTO_006_BRA(regHeader_0_1_TRIBUTO_BRA _regPai)
           : base(_regPai.cTabTXT, null)
       {
           regPai = _regPai;
           regPai.Transacoes.Add(this);
           CodComunicacao = regPai.CodComunicacao;
           NumInscricao = regPai.NumInscricao;
           SequencialRemessa = regPai.SequencialRemessa;
           DataGeracao = regPai.DataGeracao;
           DARazaoConta = 705;
           AutorizacaoDebito = "S";
           Sequencial = regPai.Transacoes.Count + 1;
       }

       /// <summary>
       /// Registra Mapa
       /// </summary>
       protected override void RegistraMapa()
       {
           base.RegistraMapa();
           RegistraFortementeTipado("NumInscricao", 10, 15);
           RegistraFortementeTipado("nrCodigoBarra", 68, 44); 
           RegistraFortementeTipado("DtDebito", 112, 8);
           RegistraMapa("Zero", 153, 3, 0);
           RegistraMapa("Zero1", 236, 6, 0);
           RegistraFortementeTipado("DtVencimento", 282, 8);
           RegistraMapa("Zero2", 290, 79, 0);
           RegistraMapa("Zero3", 456, 8, 0);
           RegistraMapa("Zero4", 580, 15, 0);
           RegistraMapa("Zero5", 595, 20, 0);
           RegistraMapa("Zero7", 623, 12, 0);
       }
   }

   /// <summary>
   /// Classe Trailer Tributo Bradesco
   /// </summary>
   public class regTrailer_9_1_TRIBUTO_BRA : regTrailer_9
   {
      private regHeader_0_1_TRIBUTO_BRA regPai;

      /// <summary>
      /// Classe Trailer Tributo Bradesco (construtor)
      /// </summary>
      /// <param name="_regPai">Registro Pai (Header)</param>
      /// <param name="TotalValorPagamento">Valor Total de Pagamento</param>
      public regTrailer_9_1_TRIBUTO_BRA(regHeader_0_1_TRIBUTO_BRA _regPai, Decimal TotalValorPagamento)
         : base(_regPai.cTabTXT, null)
      {
         regPai = _regPai;
         regPai.Trailer = this;
         CodComunicacao = regPai.CodComunicacao;
         NumInscricao = regPai.NumInscricao;
         SequencialRemessa = regPai.SequencialRemessa;
         DataGeracao = regPai.DataGeracao;
         Sequencial = regPai.Transacoes.Count + 2;
         QTDRegistros = Sequencial;
         TotalValorPago = TotalValorPagamento;
      }

      /// <summary>
      /// Registra Mapa
      /// </summary>
      protected override void RegistraMapa()
      {
          base.RegistraMapa();
          RegistraFortementeTipado("NumInscricao", 10, 15);
      }
   }
}
