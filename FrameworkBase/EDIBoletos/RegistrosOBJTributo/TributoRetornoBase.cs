/*
MR - 28/04/2016 12:00             - Considera o header apenas da primeira linha, porque pode ter mais de um header no arquivo (Altera��es indicadas por *** MRC - INICIO (28/04/2016 12:00) ***)
*/

using System;
using System.Collections.Generic;
using System.Text;
using System.Collections;
using ArquivosTXT;
using DocBacarios;

namespace EDIBoletos.RegistrosOBJTributo
{
    /// <summary>
    /// 
    /// </summary>
    public abstract class TributoRetornoBase : TributoRegistroBase
    {
        /// <summary>
        /// 
        /// </summary>
        public regHeader_0_2_TRIBUTO_BRA Header;
        /// <summary>
        /// 
        /// </summary>
        public regTrailer_9_2_TRIBUTO_BRA Trailer;
        /// <summary>
        /// 
        /// </summary>
        public ColTributos_2_001 Tributos1;
        /// <summary>
        /// 
        /// </summary>
        public ColTributos_2_005 Tributos5;
        /// <summary>
        /// 
        /// </summary>
        public ColTributos_2_006 Tributos6;
        private TributoRegistroBase reg;
        private int nLinha = 0;
        private bool CargaOk = false;

        private cTabelaTXT _Arquivo;
        private cTabelaTXT Arquivo
        {
            get
            {
                if (_Arquivo == null) _Arquivo = new cTabelaTXT(ModelosTXT.layout);
                _Arquivo.Formatodata = FormatoData.ddMMyyyy;
                return _Arquivo;
            }
        }

        private TributoRegistroBase ProximoRegistro(string RegistroEsperado)
        {
            string Linha = Arquivo.ProximaLinha();
            if (Linha.Length != 1000)
            {
                UltimoErro = new Exception(string.Format("Linha com tamanho incorreto. Esperado 1000 encontrado {0}\r\nLinha:{1} <{2}>", Linha.Length, nLinha, Linha));
                return null;
            }
            if ((Linha == null) || (Linha == ""))
            {
                UltimoErro = new Exception(string.Format("T�rmino inesperado de arquivo\r\nRegisto(s) esperado(s): {0}", RegistroEsperado));
                return null;
            }
            if (reg == null)
                reg = new TributoRegistroBase(Arquivo, Linha);
            else
                reg.Decodificalinha(Linha);
            nLinha++;
            return reg;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="NomeArquivo"></param>
        /// <returns></returns>
        public bool Carrega(string NomeArquivo)
        {
            UltimoErro = null;
            try
            {
                CargaOk = _Carrega(NomeArquivo);
            }
            finally
            {
                if (_Arquivo != null) _Arquivo.LiberaArquivo();
            }
            return CargaOk;
        }

        /// <summary>
        /// 
        /// </summary>
        public string NomeArquivo
        {
            get { return Arquivo.NomeArquivo; }
        }

        private bool _Carrega(string NomeArquivo)
        {
            Tributos1 = new ColTributos_2_001();
            Tributos5 = new ColTributos_2_005();
            Tributos6 = new ColTributos_2_006();
            Arquivo.NomeArquivo = NomeArquivo;
            //*** MRC - INICIO (28/04/2016 12:00) ***
            bool booHeader = false;
            //*** MRC - TERMINO (28/04/2016 12:00) ***
            int nSequencial = 0;
            int nRegistro = 1;
            if (ProximoRegistro("0") == null)
                return false;
            nSequencial++;
            do
            {
                if (reg.Registro == 0)
                {
                    //*** MRC - INICIO (28/04/2016 12:00) ***
                    if (!booHeader)
                    {
                        Header = new regHeader_0_2_TRIBUTO_BRA(Arquivo, reg.LinhaLida);
                        booHeader = true;
                    }
                    //*** MRC - TERMINO (28/04/2016 12:00) ***
                    if (ProximoRegistro("1 9") == null)
                        return false;
                    nSequencial++;
                }
                else
                {
                    UltimoErro = new Exception(string.Format("Registro inicial incorreto. Esperado 0 encontrado {0}\r\n Linha {1} <{2}>", reg.Registro, nLinha, reg.LinhaLida));
                    return false;
                }
                while (reg.Registro != 9 && reg.Registro != 0)
                {
                    switch (Convert.ToInt16(reg.LinhaLida.Substring(37, 3)))
                    {
                        case 1:
                            {
                                regTransacao_1_2_TRIBUTO_001_BRA regTrans = new regTransacao_1_2_TRIBUTO_001_BRA(Arquivo, reg.LinhaLida);
                                Tributos1.Add(regTrans);
                                nRegistro++;
                                if (regTrans.Sequencial != nSequencial)
                                {
                                    UltimoErro = new Exception(string.Format("Registro fora de ordem: Esperado: {0} Encontrado: {1}\r\nLinha:{2} <{3}>", nRegistro, regTrans.Sequencial, nLinha, reg.LinhaLida));
                                    return false;
                                };
                                break;
                            }
                        case 5:
                            {
                                regTransacao_1_2_TRIBUTO_005_BRA regTrans = new regTransacao_1_2_TRIBUTO_005_BRA(Arquivo, reg.LinhaLida);
                                Tributos5.Add(regTrans);
                                nRegistro++;
                                if (regTrans.Sequencial != nSequencial)
                                {
                                    UltimoErro = new Exception(string.Format("Registro fora de ordem: Esperado: {0} Encontrado: {1}\r\nLinha:{2} <{3}>", nRegistro, regTrans.Sequencial, nLinha, reg.LinhaLida));
                                    return false;
                                };
                                break;
                            }
                        case 6:
                            {
                                regTransacao_1_2_TRIBUTO_006_BRA regTrans = new regTransacao_1_2_TRIBUTO_006_BRA(Arquivo, reg.LinhaLida);
                                Tributos6.Add(regTrans);
                                nRegistro++;
                                if (regTrans.Sequencial != nSequencial)
                                {
                                    UltimoErro = new Exception(string.Format("Registro fora de ordem: Esperado: {0} Encontrado: {1}\r\nLinha:{2} <{3}>", nRegistro, regTrans.Sequencial, nLinha, reg.LinhaLida));
                                    return false;
                                };
                                break;
                            }
                    }
                    if (ProximoRegistro("0 1 9") == null)
                        return false;
                    nSequencial++;
                }
                if (reg.Registro == 9)
                {
                    Trailer = new regTrailer_9_2_TRIBUTO_BRA(Arquivo, reg.LinhaLida);
                    if (Arquivo.BytesProximaLinha() >= 1000)
                    {
                        nSequencial = 0;
                        if (ProximoRegistro("0") == null)
                            return false;
                        nSequencial++;
                    }
                }
            }
            while (Arquivo. BytesProximaLinha() >= 1000);
            return true;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public string Relatorio()
        {
            string relatorio = "";
            relatorio += String.Format("Arquivo {0}: ", Arquivo.NomeArquivo);

            if (CargaOk)
            {
                relatorio += String.Format("\r\n\r\n{0}\r\n\r\nC�d. Comunica��o: {0}\r\nTipo Inscri��o: {1}\r\nN�mero Inscri��o: {2}\r\nNome Empresa: {3}\r\nCodigo Origem Arquivo: {4}\r\nData Gera��o: {5:dd/MM/yyyy}\r\nHora Gera��o: {6}\r\nConsist. Arquivo: {7}\r\nCod. Consist. Arquivo: {8}\r\nSequencial Remessa: {9}\r\nSequencial: {10}\r\n\r\n",
                                          Header.CodComunicacao,
                                          Header.TipoInscricao,
                                          Header.NumInscricao,
                                          Header.NomeEmpresa,
                                          Header.CodOrigemArq,
                                          Header.DataGeracao,
                                          Header.HoraGeracao,
                                          Header.ConsistenciaArq,
                                          Header.CodConsistenciaArq,
                                          Header.SequencialRemessa,
                                          Header.Sequencial);

                foreach (regTransacao_1_2_TRIBUTO_001_BRA trans in Tributos1)
                {
                    relatorio += String.Format("TRIBUTO 001        C�d. Comunica��o {0} Num. Inscri��o {1} Sequencial Remessa {2} Data Gera��o {3:dd/MM/yyyy} Tipo Tributo {4} ID Documento {5} Tipo Movimento {6} Tipo Inscr. Contr. {7} N�mero Inscr. Contr. {8} Nome Contr. {9} End. Contr. {10} Cep Contr. {11} Ag�ncia {12}-{13} Razao Conta {14} Conta {15}-{16} Data D�bito {17:dd/MM/yyyy} Autoriza��o D�bito {18} Valor {19:n2} Juros {20:n2} Multa {21:n2} ValorTotal {22:n2} Vencimento {23:dd/MM/yyyy} C�d. Receita {24} Per. Apura��o {25:dd/MM/yyyy} Percentual {26:n2} Referencia {27} Valores Receita Bruta {28:n2} C�d. Consist. Arquivo {29} Uso Empresa {30} Autentica��o {31} Sequencial {32}\r\n\r\n",
                                               trans.CodComunicacao,
                                               trans.NumInscricao,
                                               trans.SequencialRemessa,
                                               trans.DataGeracao,
                                               trans.TipoTributo,
                                               trans.IDDocumento,
                                               trans.TipoMovimento,
                                               trans.TipoInscricaoContribuinte,
                                               trans.NumInscricaoContribuinte,
                                               trans.NomeContribuinte,
                                               trans.EnderecoContribuinte,
                                               trans.CepContribuinte,
                                               trans.DAAgencia,
                                               trans.DADigAgencia,
                                               trans.DARazaoConta,
                                               trans.DANumConta,
                                               trans.DADigNumConta,
                                               trans.DataDebito,
                                               trans.AutorizacaoDebito,
                                               trans.Valor,
                                               trans.Juros,
                                               trans.Multa,
                                               trans.ValorTotal,
                                               trans.DataVencimento,
                                               trans.CodReceita,
                                               trans.PeriodoApuracao,
                                               trans.Percentual,
                                               trans.Referencia,
                                               trans.ValorReceitaBruta,
                                               trans.CodConsistenciaArq,
                                               trans.UsoEmpresa,
                                               trans.Autenticacao,
                                               trans.Sequencial);
                }
                foreach (regTransacao_1_2_TRIBUTO_005_BRA trans in Tributos5)
                {
                    relatorio += String.Format("TRIBUTO 005        C�d. Comunica��o {0} Num. Inscri��o {1} Sequencial Remessa {2} Data Gera��o {3:dd/MM/yyyy} Tipo Tributo {4} ID Documento {5} Tipo Movimento {6} Tipo Inscr. Contr. {7} N�mero Inscr. Contr. {8} Nome Contr. {9} End. Contr. {10} Cep Contr. {11} Ag�ncia {12}-{13} Razao Conta {14} Conta {15}-{16} Data D�bito {17:dd/MM/yyyy} Autoriza��o D�bito {18} Valor {19:n2} Juros {20:n2} Outras Entidades {21:n2} ValorTotal {22:n2} Vencimento {23:dd/MM/yyyy} C�d. INSS {24} Compet�ncia Ano {25} Compet�ncia M�s {26:} C�d. Consist. Arquivo {27} Uso Empresa {28} Autentica��o {29} Sequencial {30}\r\n\r\n",
                                               trans.CodComunicacao,
                                               trans.NumInscricao,
                                               trans.SequencialRemessa,
                                               trans.DataGeracao,
                                               trans.TipoTributo,
                                               trans.IDDocumento,
                                               trans.TipoMovimento,
                                               trans.TipoInscricaoContribuinte,
                                               trans.NumInscricaoContribuinte,
                                               trans.NomeContribuinte,
                                               trans.EnderecoContribuinte,
                                               trans.CepContribuinte,
                                               trans.DAAgencia,
                                               trans.DADigAgencia,
                                               trans.DARazaoConta,
                                               trans.DANumConta,
                                               trans.DADigNumConta,
                                               trans.DataDebito,
                                               trans.AutorizacaoDebito,
                                               trans.Valor,
                                               trans.Juros,
                                               trans.OutrasEntidades,
                                               trans.ValorTotal,
                                               trans.DataVencimento,
                                               trans.CodINSS,
                                               trans.CompetenciaAno,
                                               trans.CompetenciaMes,
                                               trans.CodConsistenciaArq,
                                               trans.UsoEmpresa,
                                               trans.Autenticacao,
                                               trans.Sequencial);
                }
                foreach (regTransacao_1_2_TRIBUTO_006_BRA trans in Tributos6)
                {
                    relatorio += String.Format("TRIBUTO 006        C�d. Comunica��o {0} Num. Inscri��o {1} Sequencial Remessa {2} Data Gera��o {3:dd/MM/yyyy} Tipo Tributo {4} ID Documento {5} Tipo Movimento {6} C�digo Barras {7} Ag�ncia {8}-{9} Razao Conta {10} Conta {11}-{12} Data D�bito {13:dd/MM/yyyy} Autoriza��o D�bito {14} C�d. Retorno {15} Msg Retorno {16} ID Comprovante {17} Agencia {18} Nome Agencia {19} Vencimento {20:dd/MM/yyyy} Valor Tributo {21:n2} Valor Pago {22:n2} Multa {23:n2} Juros {24:n2} Desconto {25:n2} C�d. Tributo {26} ID Pagamento {27} ID Doc Pago {28} Num Identificacao {29} Autentica��o C�d. Barras {30} Ano Exerc�cio {31} Parcela {32} NumNR {33} Modalidade {34} Identificador {35} C�d. Consist. Arquivo {36} Uso Empresa {37} Autentica��o {38} Sequencial {39}\r\n\r\n",
                                               trans.CodComunicacao,
                                               trans.NumInscricao,
                                               trans.SequencialRemessa,
                                               trans.DataGeracao,
                                               trans.TipoTributo,
                                               trans.IDDocumento,
                                               trans.TipoMovimento,
                                               trans.CodigoBarra,
                                               trans.DAAgencia,
                                               trans.DADigAgencia,
                                               trans.DARazaoConta,
                                               trans.DANumConta,
                                               trans.DADigNumConta,
                                               trans.DataDebito,
                                               trans.AutorizacaoDebito,
                                               trans.CodRetorno,
                                               trans.MsgRetorno,
                                               trans.IDComprovante,
                                               trans.Agencia,
                                               trans.NomeAgencia,
                                               trans.DataVencimento,
                                               trans.ValorTributo,
                                               trans.ValorPago,
                                               trans.Multa,
                                               trans.Juros,
                                               trans.Desconto,
                                               trans.CodTributo,
                                               trans.IDPagamento,
                                               trans.IDDocumentoPago,
                                               trans.NumIdentificacao,
                                               trans.AutenticacaoCodBarra,
                                               trans.AnoExercicio,
                                               trans.Parcela,
                                               trans.NumNR,
                                               trans.Modalidade,
                                               trans.Identificador,
                                               trans.CodConsistenciaArq,
                                               trans.UsoEmpresa,
                                               trans.Autenticacao,
                                               trans.Sequencial);
                }

                relatorio += String.Format("Qtd Registros: {0}\r\nTotal Valor Pagamento: {1:n2}\r\nSequencial: {2}",
                                           Trailer.QTDRegistros,
                                           Trailer.TotalValorPago,
                                           Trailer.Sequencial);
            }
            else
            {
                relatorio += String.Format("Erro\r\n{0}", UltimoErro.Message);
            }
            return relatorio;
        }
    }

    /// <summary>
    /// 
    /// </summary>
    public class regHeader_0_2_TRIBUTO_BRA : regHeader_0
    {
        #region Variaveis Tipadas

        /// <summary>
        /// 
        /// </summary>
        public long NumInscricao;
        /// <summary>
        /// 
        /// </summary>
        public int TipoInscricao;
        /// <summary>
        /// 
        /// </summary>
        public int ConsistenciaArq;

        #endregion
        /// <summary>
        /// 
        /// </summary>
        /// <param name="_cTabelaTXT"></param>
        /// <param name="Linha"></param>
        public regHeader_0_2_TRIBUTO_BRA(cTabelaTXT _cTabelaTXT, string Linha)
            : base(_cTabelaTXT, Linha)
        {
        }

        /// <summary>
        /// 
        /// </summary>
        protected override void RegistraMapa()
        {
            base.RegistraMapa();
            RegistraFortementeTipado("NumInscricao", 10, 15);
            RegistraFortementeTipado("ConsistenciaArq", 86, 2);
        }
    }

    /// <summary>
    /// 
    /// </summary>
    public class regTransacao_1_2_TRIBUTO_001_BRA : regTransacao_1_TRIBUTO_001
    {
        #region Variaveis Tipadas

        /// <summary>
        /// 
        /// </summary>
        public int TipoInscricaoContribuinte;
        /// <summary>
        /// 
        /// </summary>
        public long NumInscricaoContribuinte;

        /// <summary>
        /// 
        /// </summary>
        public String DtDebito;
        /// <summary>
        /// 
        /// </summary>
        public String DataDebito
        {
            get
            {
                return DtDebito.Substring(6, 2) + "/" + DtDebito.Substring(4, 2) + "/" + DtDebito.Substring(0, 4);
            }
        }

        /// <summary>
        /// 
        /// </summary>
        public String DtVencimento;
        /// <summary>
        /// 
        /// </summary>
        public String DataVencimento
        {
            get
            {
                return (DtVencimento == "00010101") ? "" : DtVencimento.Substring(6, 2) + "/" + DtVencimento.Substring(4, 2) + "/" + DtVencimento.Substring(0, 4);
            }
        }

        /// <summary>
        /// 
        /// </summary>
        public decimal ValorTotal;
        /// <summary>
        /// 
        /// </summary>
        public int TipoDarf;

        /// <summary>
        /// 
        /// </summary>
        public String DtPeriodoApuracao;
        /// <summary>
        /// 
        /// </summary>
        public String PeriodoApuracao
        {
            get
            {
                return DtPeriodoApuracao.Substring(6, 2) + "/" + DtPeriodoApuracao.Substring(4, 2) + "/" + DtPeriodoApuracao.Substring(0, 4);
            }
        }

        /// <summary>
        /// 
        /// </summary>
        public decimal Percentual;

        /// <summary>
        /// 
        /// </summary>
        public int nrReferencia;
        /// <summary>
        /// 
        /// </summary>
        public String Referencia
        {
            get
            {
                return (nrReferencia == 0) ? "" : nrReferencia.ToString();
            }
        }

        /// <summary>
        /// 
        /// </summary>
        public decimal ValorReceitaBruta;
        /// <summary>
        /// 
        /// </summary>
        public string Autenticacao;

        #endregion

        /// <summary>
        /// 
        /// </summary>
        /// <param name="_cTabelaTXT"></param>
        /// <param name="Linha"></param>
        public regTransacao_1_2_TRIBUTO_001_BRA(cTabelaTXT _cTabelaTXT, string Linha)
            : base(_cTabelaTXT, Linha)
        {
        }

        /// <summary>
        /// 
        /// </summary>
        protected override void RegistraMapa()
        {
            base.RegistraMapa();
            RegistraFortementeTipado("NumInscricao", 10, 15);
            RegistraFortementeTipado("DtDebito", 189, 8);
            RegistraFortementeTipado("DtVencimento", 336, 8);
            RegistraFortementeTipado("DtPeriodoApuracao", 349, 8);
            RegistraFortementeTipado("Percentual", 357, 4);
            RegistraFortementeTipado("nrReferencia", 361, 17);
            RegistraFortementeTipado("ValorReceitaBruta", 378, 15);
            RegistraFortementeTipado("Autenticacao", 968, 27);
        }
    }

    /// <summary>
    /// 
    /// </summary>
    public class regTransacao_1_2_TRIBUTO_005_BRA : regTransacao_1_TRIBUTO_005
    {
        #region Variaveis Tipadas

        /// <summary>
        /// 
        /// </summary>
        public String DtDebito;
        /// <summary>
        /// 
        /// </summary>
        public String DataDebito
        {
            get
            {
                return DtDebito.Substring(6, 2) + "/" + DtDebito.Substring(4, 2) + "/" + DtDebito.Substring(0, 4);
            }
        }

        /// <summary>
        /// 
        /// </summary>
        public decimal ValorTotal;
        /// <summary>
        /// 
        /// </summary>
        public string TipoInscricaoContribuinte;
        /// <summary>
        /// 
        /// </summary>
        public long NumInscricaoContribuinte;
        /// <summary>
        /// 
        /// </summary>
        public String DtVencimento;

        /// <summary>
        /// 
        /// </summary>
        public String DataVencimento
        {
            get
            {
                return (DtVencimento == "00010101") ? "" : DtVencimento.Substring(6, 2) + "/" + DtVencimento.Substring(4, 2) + "/" + DtVencimento.Substring(0, 4);
            }
        }

        /// <summary>
        /// 
        /// </summary>
        public int CompetenciaMes;
        /// <summary>
        /// 
        /// </summary>
        public int CompetenciaAno;
        /// <summary>
        /// 
        /// </summary>
        public string Autenticacao;

        #endregion

        /// <summary>
        /// 
        /// </summary>
        /// <param name="_cTabelaTXT"></param>
        /// <param name="Linha"></param>
        public regTransacao_1_2_TRIBUTO_005_BRA(cTabelaTXT _cTabelaTXT, string Linha)
            : base(_cTabelaTXT, Linha)
        {
        }

        /// <summary>
        /// 
        /// </summary>
        protected override void RegistraMapa()
        {
            base.RegistraMapa();
            RegistraFortementeTipado("NumInscricao", 10, 15);
            RegistraFortementeTipado("DtDebito", 188, 8);
            RegistraFortementeTipado("DtVencimento", 336, 8);
            RegistraFortementeTipado("CompetenciaAno", 347, 4);
            RegistraFortementeTipado("CompetenciaMes", 351, 2);
            RegistraFortementeTipado("Autenticacao", 968, 27);
        }
    }

    /// <summary>
    /// 
    /// </summary>
    public class regTransacao_1_2_TRIBUTO_006_BRA : regTransacao_1_TRIBUTO_006
    {
        #region Variaveis Tipadas

        /// <summary>
        /// 
        /// </summary>
        public string CodigoBarra;

        /// <summary>
        /// 
        /// </summary>
        public string CodConvenio
        {
            get { return CodigoBarra.Substring(15, 4); }
        }

        /// <summary>
        /// 
        /// </summary>
        public int CompetenciaMes
        {
            get
            {
                DateTime DataRef = new DateTime(1967, 01, 01);
                int FatorCompetencia = Convert.ToInt32(CodigoBarra.Substring(25, 3)) - 1;
                DateTime DataCompetencia = DataRef.AddMonths(FatorCompetencia);
                return Convert.ToInt32(DataCompetencia.ToString("dd/MM/yyyy").Substring(3, 2));
            }
        }

        /// <summary>
        /// 
        /// </summary>
        public int CompetenciaAno
        {
            get
            {
                DateTime DataRef = new DateTime(1967, 01, 01);
                int FatorCompetencia = Convert.ToInt32(CodigoBarra.Substring(25, 3)) - 1;
                DateTime DataCompetencia = DataRef.AddMonths(FatorCompetencia);
                return Convert.ToInt32(DataCompetencia.ToString("dd/MM/yyyy").Substring(6, 4));
            }
        }

        /// <summary>
        /// 
        /// </summary>
        public string DataValidade
        {
            get
            {
                return CodigoBarra.Substring(23, 2) + "/" + CodigoBarra.Substring(21, 2) + "/20" + CodigoBarra.Substring(19, 2);
            }
        }

        /// <summary>
        /// 
        /// </summary>
        public String DtDebito;
        /// <summary>
        /// 
        /// </summary>
        public String DataDebito
        {
            get
            {
                return DtDebito.Substring(6, 2) + "/" + DtDebito.Substring(4, 2) + "/" + DtDebito.Substring(0, 4);
            }
        }

        /// <summary>
        /// 
        /// </summary>
        public int CodRetorno;
        /// <summary>
        /// 
        /// </summary>
        public string MsgRetorno;
        /// <summary>
        /// 
        /// </summary>
        public int IDComprovante;
        /// <summary>
        /// 
        /// </summary>
        public int Agencia;
        /// <summary>
        /// 
        /// </summary>
        public string NomeAgencia;

        /// <summary>
        /// 
        /// </summary>
        public String DtVencimento;
        /// <summary>
        /// 
        /// </summary>
        public String DataVencimento
        {
            get
            {
                return (DtVencimento == "00010101") ? "" : DtVencimento.Substring(6, 2) + "/" + DtVencimento.Substring(4, 2) + "/" + DtVencimento.Substring(0, 4);
            }
        }

        /// <summary>
        /// 
        /// </summary>
        public decimal ValorTributo;
        /// <summary>
        /// 
        /// </summary>
        public decimal ValorPago;
        /// <summary>
        /// 
        /// </summary>
        public decimal Multa;
        /// <summary>
        /// 
        /// </summary>
        public decimal Juros;
        /// <summary>
        /// 
        /// </summary>
        public decimal Desconto;
        /// <summary>
        /// 
        /// </summary>
        public int CodTributo;
        /// <summary>
        /// 
        /// </summary>
        public string IDPagamento;
        /// <summary>
        /// 
        /// </summary>
        public string IDDocumentoPago;
        /// <summary>
        /// 
        /// </summary>
        public string NumIdentificacao;
        /// <summary>
        /// 
        /// </summary>
        public string AutenticacaoCodBarra;
        /// <summary>
        /// 
        /// </summary>
        public int AnoExercicio;
        /// <summary>
        /// 
        /// </summary>
        public int Parcela;
        /// <summary>
        /// 
        /// </summary>
        public int NumNR;
        /// <summary>
        /// 
        /// </summary>
        public int Modalidade;
        /// <summary>
        /// 
        /// </summary>
        public string Identificador;
        /// <summary>
        /// 
        /// </summary>
        public string Autenticacao;

        #endregion

        /// <summary>
        /// 
        /// </summary>
        /// <param name="_cTabelaTXT"></param>
        /// <param name="Linha"></param>
        public regTransacao_1_2_TRIBUTO_006_BRA(cTabelaTXT _cTabelaTXT, string Linha)
            : base(_cTabelaTXT, Linha)
        {
        }

        /// <summary>
        /// 
        /// </summary>
        protected override void RegistraMapa()
        {
            base.RegistraMapa();
            RegistraFortementeTipado("NumInscricao", 10, 15);
            RegistraFortementeTipado("CodigoBarra", 68, 44);
            RegistraFortementeTipado("DtDebito", 112, 8);
            RegistraFortementeTipado("CodRetorno", 153, 3);
            RegistraFortementeTipado("MsgRetorno", 156, 80);
            RegistraFortementeTipado("IDComprovante", 236, 1);
            RegistraFortementeTipado("Agencia", 237, 5);
            RegistraFortementeTipado("NomeAgencia", 242, 40);
            RegistraFortementeTipado("DtVencimento", 282, 8);
            RegistraFortementeTipado("ValorTributo", 290, 15);
            RegistraFortementeTipado("ValorPago", 305, 15);
            RegistraFortementeTipado("Multa", 320, 15);
            RegistraFortementeTipado("Juros", 335, 15);
            RegistraFortementeTipado("Desconto", 350, 15);
            RegistraFortementeTipado("CodTributo", 365, 4);
            RegistraFortementeTipado("IDPagamento", 369, 40);
            RegistraFortementeTipado("IDDocumentoPago", 409, 23);
            RegistraFortementeTipado("NumIdentificacao", 432, 15);
            RegistraFortementeTipado("AutenticacaoCodBarra", 447, 9);
            RegistraFortementeTipado("AnoExercicio", 456, 4);
            RegistraFortementeTipado("Parcela", 460, 2);
            RegistraFortementeTipado("NumNR", 462, 2);
            RegistraFortementeTipado("Modalidade", 464, 1);
            RegistraFortementeTipado("Identificador", 595, 20);
            RegistraFortementeTipado("Autenticacao", 968, 27);
        }
    }

    /// <summary>
    /// 
    /// </summary>
    public class regTrailer_9_2_TRIBUTO_BRA : regTrailer_9
    {
        #region Variaveis Tipadas

        #endregion

        /// <summary>
        /// 
        /// </summary>
        /// <param name="_cTabelaTXT"></param>
        /// <param name="Linha"></param>
        public regTrailer_9_2_TRIBUTO_BRA(cTabelaTXT _cTabelaTXT, string Linha)
            : base(_cTabelaTXT, Linha)
        {
        }

        /// <summary>
        /// 
        /// </summary>
        protected override void RegistraMapa()
        {
            base.RegistraMapa();
            RegistraFortementeTipado("NumInscricao", 10, 15);
        }
    }

    /// <summary>
    /// 
    /// </summary>
    public class ColTributos_2_001 : CollectionBase
    {
        /// <summary>
        /// 
        /// </summary>
        /// <param name="index"></param>
        /// <returns></returns>
        public regTransacao_1_2_TRIBUTO_001_BRA this[int index]
        {
            get
            {
                return ((regTransacao_1_2_TRIBUTO_001_BRA)List[index]);
            }
            set
            {
                List[index] = value;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="value"></param>
        /// <returns></returns>
        public int Add(regTransacao_1_2_TRIBUTO_001_BRA value)
        {
            return (List.Add(value));
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="value"></param>
        /// <returns></returns>
        public int IndexOf(regTransacao_1_2_TRIBUTO_001_BRA value)
        {
            return (List.IndexOf(value));
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="index"></param>
        /// <param name="value"></param>
        public void Insert(int index, regTransacao_1_2_TRIBUTO_001_BRA value)
        {
            List.Insert(index, value);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="value"></param>
        public void Remove(regTransacao_1_2_TRIBUTO_001_BRA value)
        {
            List.Remove(value);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="value"></param>
        /// <returns></returns>
        public bool Contains(regTransacao_1_2_TRIBUTO_001_BRA value)
        {
            // If value is not of type Int16, this will return false.
            return (List.Contains(value));
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="value"></param>
        protected override void OnValidate(Object value)
        {
            if (value.GetType() != typeof(regTransacao_1_2_TRIBUTO_001_BRA))
                throw new ArgumentException("Value must be of type regTransacao_1_2.", "value");
        }
    }

    /// <summary>
    /// 
    /// </summary>
    public class ColTributos_2_005 : CollectionBase
    {
        /// <summary>
        /// 
        /// </summary>
        public regTransacao_1_2_TRIBUTO_005_BRA this[int index]
        {
            get
            {
                return ((regTransacao_1_2_TRIBUTO_005_BRA)List[index]);
            }
            set
            {
                List[index] = value;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        public int Add(regTransacao_1_2_TRIBUTO_005_BRA value)
        {
            return (List.Add(value));
        }

        /// <summary>
        /// 
        /// </summary>
        public int IndexOf(regTransacao_1_2_TRIBUTO_005_BRA value)
        {
            return (List.IndexOf(value));
        }

        /// <summary>
        /// 
        /// </summary>
        public void Insert(int index, regTransacao_1_2_TRIBUTO_005_BRA value)
        {
            List.Insert(index, value);
        }

        /// <summary>
        /// 
        /// </summary>
        public void Remove(regTransacao_1_2_TRIBUTO_005_BRA value)
        {
            List.Remove(value);
        }

        /// <summary>
        /// 
        /// </summary>
        public bool Contains(regTransacao_1_2_TRIBUTO_005_BRA value)
        {
            // If value is not of type Int16, this will return false.
            return (List.Contains(value));
        }

        /// <summary>
        /// 
        /// </summary>
        protected override void OnValidate(Object value)
        {
            if (value.GetType() != typeof(regTransacao_1_2_TRIBUTO_005_BRA))
                throw new ArgumentException("Value must be of type regTransacao_1_2.", "value");
        }
    }

    /// <summary>
    /// 
    /// </summary>
    public class ColTributos_2_006 : CollectionBase
    {
        /// <summary>
        /// 
        /// </summary>
        public regTransacao_1_2_TRIBUTO_006_BRA this[int index]
        {
            get
            {
                return ((regTransacao_1_2_TRIBUTO_006_BRA)List[index]);
            }
            set
            {
                List[index] = value;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        public int Add(regTransacao_1_2_TRIBUTO_006_BRA value)
        {
            return (List.Add(value));
        }

        /// <summary>
        /// 
        /// </summary>
        public int IndexOf(regTransacao_1_2_TRIBUTO_006_BRA value)
        {
            return (List.IndexOf(value));
        }

        /// <summary>
        /// 
        /// </summary>
        public void Insert(int index, regTransacao_1_2_TRIBUTO_006_BRA value)
        {
            List.Insert(index, value);
        }

        /// <summary>
        /// 
        /// </summary>
        public void Remove(regTransacao_1_2_TRIBUTO_006_BRA value)
        {
            List.Remove(value);
        }

        /// <summary>
        /// 
        /// </summary>
        public bool Contains(regTransacao_1_2_TRIBUTO_006_BRA value)
        {
            // If value is not of type Int16, this will return false.
            return (List.Contains(value));
        }

        /// <summary>
        /// 
        /// </summary>
        protected override void OnValidate(Object value)
        {
            if (value.GetType() != typeof(regTransacao_1_2_TRIBUTO_006_BRA))
                throw new ArgumentException("Value must be of type regTransacao_1_2.", "value");
        }
    }
}
