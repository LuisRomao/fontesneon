﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Collections;
using ArquivosTXT;
using DocBacarios;
using Abstratos;

namespace EDIBoletos.RegistrosOBJTributo
{
    /// <summary>
    /// 
    /// </summary>
    public abstract class TributoRemessaBase : TributoRegistroBase
    {
        /// <summary>
        /// 
        /// </summary>
        public ColTributos Tributos;
        /// <summary>
        /// 
        /// </summary>
        public cTabelaTXT ArquivoSaida;

        /// <summary>
        /// 
        /// </summary>
        public CPFCNPJ CPFCNPJ;
        /// <summary>
        /// 
        /// </summary>
        public string NomeEmpresa;
        /// <summary>
        /// 
        /// </summary>
        public int SequencialRemessaArquivo;

        /// <summary>
        /// 
        /// </summary>
        public abstract void IniciaRemessa();

        /// <summary>
        /// 
        /// </summary>
        public abstract string GravaArquivo(string Path, string Arquivo);

        /// <summary>
        /// 
        /// </summary>
        public abstract string FinalizaArquivo(string Arquivo);
    }

    /// <summary>
    /// 
    /// </summary>
    public class ColTributos : CollectionBase
    {
        /// <summary>
        /// 
        /// </summary>
        public ABS_Tributo this[int index]
        {
            get
            {
                return ((ABS_Tributo)List[index]);
            }
            set
            {
                List[index] = value;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        public int Add(ABS_Tributo value)
        {
            return (List.Add(value));
        }

        /// <summary>
        /// 
        /// </summary>
        public int IndexOf(ABS_Tributo value)
        {
            return (List.IndexOf(value));
        }

        /// <summary>
        /// 
        /// </summary>
        public void Insert(int index, ABS_Tributo value)
        {
            List.Insert(index, value);
        }

        /// <summary>
        /// 
        /// </summary>
        public void Remove(ABS_Tributo value)
        {
            List.Remove(value);
        }

        /// <summary>
        /// 
        /// </summary>
        public bool Contains(ABS_Tributo value)
        {
            // If value is not of type Int16, this will return false.
            return (List.Contains(value));
        }
    }
}
