﻿/*
MR - 03/12/2014 10:00 -           - Ajuste no campo IDDocumento para ser string porém preenchida com zeros à esquerda (Alterações indicadas por *** MRC - INICIO - TRIBUTOS (03/12/2014 10:00) ***)
MR - 04/12/2014 10:00 -           - Cria e utiliza função que retira acentuação e caracteres de campos de nome e endereço para permitir criptografia e envio via robo (Alterações indicadas por *** MRC - INICIO - TRIBUTOS (04/12/2014 10:00) ***)
                                    Retirada de warnings de XML
MR - 10/01/2015 10:00 -           - Transforma Digítos de Agência e Conta de "P" para "0" (Alterações indicadas por *** MRC - INICIO - TRIBUTOS (10/01/2015 10:00) ***)
                                    Inclui mais alguns simbolos na função que retira acentuação e caracteres                                    
MR - 12/01/2015 19:00 -           - Correção de BUG na leitura de arquivo do retorno
MR - 10/02/2015 12:00 -           - Inclui mais alguns simbolos na função que retira acentuação e caracteres
*/

using System;
using System.Collections.Generic;
using System.Text;
using System.Collections;
using ArquivosTXT;
using DocBacarios;

namespace EDIBoletos.RegistrosOBJTributo
{
   /// <summary>
   /// Classe Tributo Registro Base
   /// </summary>
   public class TributoRegistroBase: RegistroBase
   {
      /// <summary>
      /// Último erro
      /// </summary>
      public Exception UltimoErro;

      #region Propriedades tipadas

         /// <summary>
         /// Registro
         /// </summary>
         public int Registro;
         /// <summary>
         /// Código de Comunicação
         /// </summary>
         public int CodComunicacao;
         /// <summary>
         /// Sequencial da Remessa
         /// </summary>
         public int SequencialRemessa;
         /// <summary>
         /// Data de Geração do Arquivo
         /// </summary>
         public String DataGeracao;
         /// <summary>
         /// Hora de Geração do Arquivo
         /// </summary>
         public String HoraGeracao;
         /// <summary>
         /// Códigos de Consistência do Arquivo
         /// </summary>
         public string CodConsistenciaArq;
         /// <summary>
         /// Sequencial
         /// </summary>
         public int Sequencial;

      #endregion

      /// <summary>
      /// Tributo Regsitro Base (construtor)
      /// </summary>
      public TributoRegistroBase()
         : base()
      {
      }

      /// <summary>
      /// Tributo Regsitro Base (construtor)
      /// </summary>
      /// <param name="_cTabelaTXT">Tabela</param>
      /// <param name="Linha">Linha</param>
      public TributoRegistroBase(cTabelaTXT _cTabelaTXT, string Linha)
         : base(_cTabelaTXT)
      {
            CarregaLinha(Linha);
      }

      /// <summary>
      /// Enumeração com tipos de tributos
      /// </summary>
      public enum TiposTipoTributo
      {
          /// <summary>
          /// DARF
          /// </summary>
          DARF = 1,
          /// <summary>
          /// GPS
          /// </summary>
          GPS = 5,
          /// <summary>
          /// Código de Barras (FGTS e Contas Consumo)
          /// </summary>
          CodigoBarras = 6
      }

      /// <summary>
      /// Enumeração com tipos de inscrição
      /// </summary>
      public enum TiposInscricao
      {
         /// <summary>
         /// CPF
         /// </summary>
         CPF = 1,
         /// <summary>
         /// CNPJ
         /// </summary>
         CNPJ = 2,
         /// <summary>
         /// Outros
         /// </summary>
         Outros = 3
      }

      /// <summary>
      /// Enumeração com tipos de códigos de origem de arquivo
      /// </summary>
      public enum TiposCodOrigemArq
      {
          /// <summary>
          /// Remessa
          /// </summary>
          Remessa = 1,
          /// <summary>
          /// Retorno de Inconsistências
          /// </summary>
          RetornoConsistencias = 2,
          /// <summary>
          /// Retorno
          /// </summary>
          RetornoFinal = 3
      }

      /// <summary>
      /// Enumeração com tipos de consistência de arquivo
      /// </summary>
      public enum TiposConsistenciaArq
      {
          /// <summary>
          /// Aceito Totalmente
          /// </summary>
          ArquivoAceitoTotalmente = 1,
          /// <summary>
          /// Aceito Parcialmente
          /// </summary>
          ArquivoAceitoParcialmente = 2,
          /// <summary>
          /// Rejeitado
          /// </summary>
          ArquivoRejeitado = 3
      }

      /// <summary>
      /// Enumeração com tipos de darfs
      /// </summary>
      public enum TiposTipoDarf
      {
          /// <summary>
          /// DARF Simples
          /// </summary>
          Simples = 1,
          /// <summary>
          /// DARF Preto Europa
          /// </summary>
          PretoEuropa = 2
      }

      /// <summary>
      /// Registra Mapa
      /// </summary>
      protected override void RegistraMapa()
      {
         RegistraFortementeTipado("Registro", 1, 1);
         RegistraFortementeTipado("CodComunicacao", 2, 8);
         RegistraFortementeTipado("SequencialRemessa", 25, 5);
         RegistraFortementeTipado("DataGeracao", 30, 8);
         RegistraFortementeTipado("CodConsistenciaArq", 900, 10);
         RegistraFortementeTipado("Sequencial", 995, 6);
      }

      //*** MRC - INICIO - TRIBUTOS (04/12/2014 10:00) ***
      /// <summary>
      /// Função para remover acentos e caracteres
      /// </summary>
      /// <param name="texto"></param>
      /// <returns></returns>
      public static string RemoverAcentosCaracteres(string texto)
      {
          //*** MRC - INICIO - TRIBUTOS (10/01/2015 10:00) ***
          string comAcentosCaracteres = "ÄÅÁÂÀÃÉÊËÈÍÎÏÌÖÓÔÒÕÜÚÛÙÇ°ºª/'´\"";
          string semAcentosCaracteres = "AAAAAAEEEEIIIIOOOOOUUUUC.oa    ";
          //*** MRC - TERMINO - TRIBUTOS (10/01/2015 10:00) ***

          for (int i = 0; i < comAcentosCaracteres.Length; i++)
          {
              texto = texto.Replace(comAcentosCaracteres[i].ToString(), semAcentosCaracteres[i].ToString());
          }
          return texto;
      }
      //*** MRC - TERMINO - TRIBUTOS (04/12/2014 10:00) ***
   }

   /// <summary>
   /// Classe Header Base
   /// </summary>
   public class regHeader_0 : TributoRegistroBase
   {
      #region Variaveis Tipadas

         private string _NomeEmpresa;
         /// <summary>
         /// Nome da Empresa
         /// </summary>
         public string NomeEmpresa
         {
            //*** MRC - INICIO - TRIBUTOS (04/12/2014 10:00) ***
            get { return (_NomeEmpresa == null) ? "" : RemoverAcentosCaracteres(_NomeEmpresa.ToUpper()); }
            //*** MRC - TERMINO - TRIBUTOS (04/12/2014 10:00) ***
            set { _NomeEmpresa = value; }
         }

         /// <summary>
         /// Código de Origem do Arquivo
         /// </summary>
         public int CodOrigemArq;

      #endregion

      /// <summary>
      /// Lista de Transações (Tributos)
      /// </summary>
      public List<regTransacao_1> Transacoes;
      /// <summary>
      /// Trailer
      /// </summary>
      public regTrailer_9 Trailer;

      /// <summary>
      /// Classe Header Base (construtor)
      /// </summary>
      /// <param name="_cTabelaTXT">Tabela</param>
      /// <param name="Linha">Linha</param>
      public regHeader_0(cTabelaTXT _cTabelaTXT, string Linha)
         : base(_cTabelaTXT, Linha)
      {
         Registro = 0;
         Transacoes = new List<regTransacao_1>();
      }

      /// <summary>
      /// Grava Arquivo
      /// </summary>
      /// <param name="NomeArquivo"></param>
      /// <returns></returns>
      public bool GravaArquivo(string NomeArquivo)
      {
         bool resultado = false;
         try
         {
            cTabTXT.IniciaGravacao(NomeArquivo, true);
            GravaLinha();
            if (Transacoes != null)
               foreach (regTransacao_1 trans in Transacoes)
                  trans.GravaLinha();
            if (Trailer != null)
               Trailer.GravaLinha();
            resultado = true;
         }
         catch(Exception ex)
         {
            throw new ArgumentException(ex.Message, ex.InnerException);
         }
         return resultado;
      }

      /// <summary>
      /// Finaliza Arquivo
      /// </summary>
      /// <param name="NomeArquivo"></param>
      /// <returns></returns>
      public bool FinalizaArquivo(string NomeArquivo)
      {
          bool resultado = false;
          try
          {
              cTabTXT.TerminaGravacao(); 
              resultado = true;
          }
          catch (Exception ex)
          {
              throw new ArgumentException(ex.Message, ex.InnerException);
          }
          return resultado;
      }

      /// <summary>
      /// Registra Mapa
      /// </summary>
      protected override void RegistraMapa()
      {
         base.RegistraMapa();
         RegistraFortementeTipado("TipoInscricao", 38, 1);
         RegistraFortementeTipado("NomeEmpresa", 39, 40);
         RegistraFortementeTipado("CodOrigemArq", 79, 1);
         RegistraFortementeTipado("HoraGeracao", 80, 6);
      }
   }

   /// <summary>
   /// Classe Transação Base
   /// </summary>
   public class regTransacao_1 : TributoRegistroBase
   {
      #region Variaveis Tipadas

         /// <summary>
         /// Número de Inscrição
         /// </summary>
         public long NumInscricao;
         /// <summary>
         /// Tipo de Tributo
         /// </summary>
         public int TipoTributo;

         //*** MRC - INICIO - TRIBUTOS (03/12/2014 10:00) ***
         private string _IDDocumento;
         /// <summary>
         /// Id do Documento
         /// </summary>
         public string IDDocumento
         {
             get { return (_IDDocumento == null) ? "00000000000000000000000000" : _IDDocumento.PadLeft(26, '0'); }
             set { _IDDocumento = value; }
         }
         //*** MRC - TERMINO - TRIBUTOS (03/12/2014 10:00) ***

         /// <summary>
         /// Tipo de Movimento
         /// </summary>
         public string TipoMovimento;

         private string _UsoEmpresa;
         /// <summary>
         /// Uso da Empresa
         /// </summary>
         public string UsoEmpresa
         {
            get { return (_UsoEmpresa == null) ? "" : _UsoEmpresa.ToUpper(); }
            set { _UsoEmpresa = value; }
         }
                                                      
      #endregion

      /// <summary>
      /// Classe Transação Base (construtor)
      /// </summary>
      public regTransacao_1(cTabelaTXT _cTabelaTXT, string Linha)
         : base(_cTabelaTXT, Linha)
      {
         Registro = 1;
      }

      /// <summary>
      /// Registra Mapa
      /// </summary>
      protected override void RegistraMapa()
      {
         base.RegistraMapa();
         RegistraFortementeTipado("TipoTributo", 38, 3);
         RegistraFortementeTipado("IDDocumento", 41, 26);
         RegistraFortementeTipado("TipoMovimento", 67, 1);
         RegistraFortementeTipado("UsoEmpresa", 952, 16);
      }
   }

   /// <summary>
   /// Classe Transação Tributo 001 (DARF) Base
   /// </summary>
   public class regTransacao_1_TRIBUTO_001 : regTransacao_1
   {
       #region Variaveis Tipadas

       private string _NomeContribuinte;
       /// <summary>
       /// Nome do Contribuinte
       /// </summary>
       public string NomeContribuinte
       {
           //*** MRC - INICIO - TRIBUTOS (04/12/2014 10:00) ***
           get { return (_NomeContribuinte == null) ? "" : RemoverAcentosCaracteres(_NomeContribuinte.ToUpper()); }
           //*** MRC - TERMINO - TRIBUTOS (04/12/2014 10:00) ***
           set { _NomeContribuinte = value; }
       }

       private string _EnderecoContribuinte;
       /// <summary>
       /// Endereço do Contribuinte
       /// </summary>
       public string EnderecoContribuinte
       {
           //*** MRC - INICIO - TRIBUTOS (04/12/2014 10:00) ***
           get { return (_EnderecoContribuinte == null) ? "" : RemoverAcentosCaracteres(_EnderecoContribuinte.ToUpper()); }
           //*** MRC - TERMINO - TRIBUTOS (04/12/2014 10:00) ***
           set { _EnderecoContribuinte = value; }
       }

       private string _CepContribuinte;
       /// <summary>
       /// CEP do Contribuinte
       /// </summary>
       public string CepContribuinte
       {
           get
           {
               long lngCep = 0;
               if (_CepContribuinte != null) { _CepContribuinte = _CepContribuinte.Replace("-", ""); }
               return (long.TryParse(_CepContribuinte, out lngCep)) ? _CepContribuinte.PadLeft(8, '0') : "00000000";
           }
           set { _CepContribuinte = value; }
       }

       /// <summary>
       /// Agência
       /// </summary>
       public int DAAgencia;
       //*** MRC - INICIO - TRIBUTOS (10/01/2015 10:00) ***
       private string _DADigAgencia;
       /// <summary>
       /// Digito da Agência
       /// </summary>
       public string DADigAgencia
       {
           get { return (_DADigAgencia == null) ? "" : (_DADigAgencia.ToUpper() == "P") ? "0" : _DADigAgencia; }          
           set { _DADigAgencia = value; }
       }
       //*** MRC - TERMINO - TRIBUTOS (10/01/2015 10:00) ***

       /// <summary>
       /// Razão da Conta
       /// </summary>
       public int DARazaoConta;
       /// <summary>
       /// Número da Conta
       /// </summary>
       public int DANumConta;
       //*** MRC - INICIO - TRIBUTOS (10/01/2015 10:00) ***
       private string _DADigNumConta;
       /// <summary>
       /// Dígito do Número da Conta
       /// </summary>
       public string DADigNumConta
       {
           get { return (_DADigNumConta == null) ? "" : (_DADigNumConta.ToUpper() == "P") ? "0" : _DADigNumConta; }          
           set { _DADigNumConta = value; }
       }
       //*** MRC - TERMINO - TRIBUTOS (10/01/2015 10:00) ***

       /// <summary>
       /// Autorização de Débito
       /// </summary>
       public string AutorizacaoDebito;
       /// <summary>
       /// Valor Principal
       /// </summary>
       public decimal Valor;
       /// <summary>
       /// Valor de Juros
       /// </summary>
       public decimal Juros;
       /// <summary>
       /// Valor de Multa
       /// </summary>
       public decimal Multa;
       /// <summary>
       /// Código da Receita
       /// </summary>
       public int CodReceita;

       #endregion

       /// <summary>
       /// Classe Transação Tributo 001 (DARF) Base (construtor)
       /// </summary>
       public regTransacao_1_TRIBUTO_001(cTabelaTXT _cTabelaTXT, string Linha)
           : base(_cTabelaTXT, Linha)
       {
       }

       /// <summary>
       /// Registra Mapa
       /// </summary>
       protected override void RegistraMapa()
       {
           base.RegistraMapa();
           RegistraFortementeTipado("TipoInscricaoContribuinte", 68, 1);
           RegistraFortementeTipado("NumInscricaoContribuinte", 69, 15);
           RegistraFortementeTipado("NomeContribuinte", 84, 40);
           RegistraFortementeTipado("EnderecoContribuinte", 124, 40);
           RegistraFortementeTipado("CepContribuinte", 164, 8);
           RegistraFortementeTipado("DAAgencia", 172, 4);
           RegistraFortementeTipado("DADigAgencia", 176, 1);
           RegistraFortementeTipado("DARazaoConta", 177, 4);
           RegistraFortementeTipado("DANumConta", 181, 7);
           RegistraFortementeTipado("DADigNumConta", 188, 1);
           RegistraFortementeTipado("AutorizacaoDebito", 197, 1);
           RegistraFortementeTipado("Valor", 198, 15);
           RegistraFortementeTipado("Juros", 213, 15);
           RegistraFortementeTipado("Multa", 228, 15);
           RegistraFortementeTipado("ValorTotal", 243, 15);
           RegistraFortementeTipado("CodReceita", 344, 4);
           RegistraFortementeTipado("TipoDarf", 348, 1);
       }
   }

   /// <summary>
   /// Classe Transação Tributo 005 (GPS) Base
   /// </summary>
   public class regTransacao_1_TRIBUTO_005 : regTransacao_1
   {
       #region Variaveis Tipadas

       private string _NomeContribuinte;
       /// <summary>
       /// Nome do Contribuinte
       /// </summary>
       public string NomeContribuinte
       {
           //*** MRC - INICIO - TRIBUTOS (04/12/2014 10:00) ***
           get { return (_NomeContribuinte == null) ? "" : RemoverAcentosCaracteres(_NomeContribuinte.ToUpper()); }
           //*** MRC - TERMINO - TRIBUTOS (04/12/2014 10:00) ***
           set { _NomeContribuinte = value; }
       }

       private string _EnderecoContribuinte;
       /// <summary>
       /// Endereço do Contribuinte
       /// </summary>
       public string EnderecoContribuinte
       {
           //*** MRC - INICIO - TRIBUTOS (04/12/2014 10:00) ***
           get { return (_EnderecoContribuinte == null) ? "" : RemoverAcentosCaracteres(_EnderecoContribuinte.ToUpper()); }
           //*** MRC - TERMINO - TRIBUTOS (04/12/2014 10:00) ***
           set { _EnderecoContribuinte = value; }
       }

       private string _CepContribuinte;
       /// <summary>
       /// CEP do Contribuinte
       /// </summary>
       public string CepContribuinte
       {
           get
           {
               long lngCep = 0;
               if (_CepContribuinte != null) { _CepContribuinte = _CepContribuinte.Replace("-", ""); }
               return (long.TryParse(_CepContribuinte, out lngCep)) ? _CepContribuinte.PadLeft(8, '0') : "00000000";
           }
           set { _CepContribuinte = value; }
       }

       /// <summary>
       /// Agência
       /// </summary>
       public int DAAgencia;
       //*** MRC - INICIO - TRIBUTOS (10/01/2015 10:00) ***
       private string _DADigAgencia;
       /// <summary>
       /// Digito da Agência
       /// </summary>
       public string DADigAgencia
       {
           get { return (_DADigAgencia == null) ? "" : (_DADigAgencia.ToUpper() == "P") ? "0" : _DADigAgencia; }          
           set { _DADigAgencia = value; }
       }
       //*** MRC - TERMINO - TRIBUTOS (10/01/2015 10:00) ***

       /// <summary>
       /// Razão da Conta
       /// </summary>
       public int DARazaoConta;
       /// <summary>
       /// Número da Conta
       /// </summary>
       public int DANumConta;
       //*** MRC - INICIO - TRIBUTOS (10/01/2015 10:00) ***
       private string _DADigNumConta;
       /// <summary>
       /// Dígito do Número da Conta
       /// </summary>
       public string DADigNumConta
       {
           get { return (_DADigNumConta == null) ? "" : (_DADigNumConta.ToUpper() == "P") ? "0" : _DADigNumConta; }          
           set { _DADigNumConta = value; }
       }
       //*** MRC - TERMINO - TRIBUTOS (10/01/2015 10:00) ***

       /// <summary>
       /// Autorização de Débito
       /// </summary>
       public string AutorizacaoDebito;
       /// <summary>
       /// Valor Principal
       /// </summary>
       public decimal Valor;
       /// <summary>
       /// Valor de Juros
       /// </summary>
       public decimal Juros;
       /// <summary>
       /// Valor de Outras Entidades
       /// </summary>
       public decimal OutrasEntidades;
       /// <summary>
       /// Código de INSS
       /// </summary>
       public int CodINSS;

       #endregion

       /// <summary>
       /// Classe Transação Tributo 005 (GPS) Base (construtor)
       /// </summary>
       public regTransacao_1_TRIBUTO_005(cTabelaTXT _cTabelaTXT, string Linha)
           : base(_cTabelaTXT, Linha)
       {
       }

       /// <summary>
       /// Registra Mapa
       /// </summary>
       protected override void RegistraMapa()
       {
           base.RegistraMapa();
           RegistraFortementeTipado("NomeContribuinte", 68, 40);
           RegistraFortementeTipado("EnderecoContribuinte", 108, 40);
           RegistraFortementeTipado("CepContribuinte", 148, 8);
           RegistraFortementeTipado("DAAgencia", 171, 4);
           RegistraFortementeTipado("DADigAgencia", 175, 1);
           RegistraFortementeTipado("DARazaoConta", 176, 4);
           RegistraFortementeTipado("DANumConta", 180, 7);
           RegistraFortementeTipado("DADigNumConta", 187, 1);
           RegistraFortementeTipado("AutorizacaoDebito", 196, 1);
           RegistraFortementeTipado("Valor", 197, 15);
           RegistraFortementeTipado("Juros", 212, 15);
           RegistraFortementeTipado("OutrasEntidades", 227, 15);
           RegistraFortementeTipado("ValorTotal", 242, 15);
           RegistraFortementeTipado("CodINSS", 257, 4);
           RegistraFortementeTipado("TipoInscricaoContribuinte", 261, 3);
           RegistraFortementeTipado("NumInscricaoContribuinte", 264, 14);
       }
   }

   /// <summary>
   /// Classe Transação Tributo 006 (FGTS ou Conta Consumo) Base
   /// </summary>
   public class regTransacao_1_TRIBUTO_006 : regTransacao_1
   {
       #region Variaveis Tipadas

       /// <summary>
       /// Valor
       /// </summary>
       public decimal Valor;
       /// <summary>
       /// Agência
       /// </summary>
       public int DAAgencia;
       //*** MRC - INICIO - TRIBUTOS (10/01/2015 10:00) ***
       private string _DADigAgencia;
       /// <summary>
       /// Digito da Agência
       /// </summary>
       public string DADigAgencia
       {
           get { return (_DADigAgencia == null) ? "" : (_DADigAgencia.ToUpper() == "P") ? "0" : _DADigAgencia; }          
           set { _DADigAgencia = value; }
       }
       //*** MRC - TERMINO - TRIBUTOS (10/01/2015 10:00) ***

       /// <summary>
       /// Razão da Conta
       /// </summary>
       public int DARazaoConta;
       /// <summary>
       /// Número da Conta
       /// </summary>
       public int DANumConta;
       //*** MRC - INICIO - TRIBUTOS (10/01/2015 10:00) ***
       private string _DADigNumConta;
       /// <summary>
       /// Dígito do Número da Conta
       /// </summary>
       public string DADigNumConta
       {
           get { return (_DADigNumConta == null) ? "" : (_DADigNumConta.ToUpper() == "P") ? "0" : _DADigNumConta; }          
           set { _DADigNumConta = value; }
       }
       //*** MRC - TERMINO - TRIBUTOS (10/01/2015 10:00) ***

       /// <summary>
       /// Autorização de Débito
       /// </summary>
       public string AutorizacaoDebito;

       #endregion

       /// <summary>
       /// Classe Transação Tributo 006 (FGTS ou Conta Consumo) Base
       /// </summary>
       public regTransacao_1_TRIBUTO_006(cTabelaTXT _cTabelaTXT, string Linha)
           : base(_cTabelaTXT, Linha)
       {
       }

       /// <summary>
       /// Registra Mapa
       /// </summary>
       protected override void RegistraMapa()
       {
           base.RegistraMapa();
           RegistraFortementeTipado("Valor", 120, 15);
           RegistraFortementeTipado("DAAgencia", 135, 4);
           RegistraFortementeTipado("DADigAgencia", 139, 1);
           RegistraFortementeTipado("DARazaoConta", 140, 4);
           RegistraFortementeTipado("DANumConta", 144, 7);
           RegistraFortementeTipado("DADigNumConta", 151, 1);
           RegistraFortementeTipado("AutorizacaoDebito", 152, 1);
       }
   }

   /// <summary>
   /// Classe Trailer Base
   /// </summary>
   public class regTrailer_9 : TributoRegistroBase
   {
      #region Variaveis Tipadas

         /// <summary>
         /// Número de Inscrição
         /// </summary>
         public long NumInscricao;
         /// <summary>
         /// Quantidade de Registros
         /// </summary>
         public int QTDRegistros;
         /// <summary>
         /// Valor Total do Pagamento
         /// </summary>
         public decimal TotalValorPago;

      #endregion

      /// <summary>
         /// Classe Trailer Base (construtor)
      /// </summary>
      /// <param name="_cTabelaTXT">Tabela</param>
      /// <param name="Linha">Linha</param>
      public regTrailer_9(cTabelaTXT _cTabelaTXT, string Linha)
         : base(_cTabelaTXT, Linha)
      {
          Registro = 9;
      }
      
      /// <summary>
      /// Registra Mapa
      /// </summary>
      protected override void RegistraMapa()
      {
         base.RegistraMapa();
         RegistraFortementeTipado("QTDRegistros", 38, 6);
         RegistraFortementeTipado("TotalValorPago", 44, 17);
      }
   }
}