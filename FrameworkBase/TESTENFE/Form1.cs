﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using System.IO;

namespace TESTENFE
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void simpleButton1_Click(object sender, EventArgs e)
        {
            
            OpenFileDialog Open = new OpenFileDialog();
            Open.Filter = "*.xml|*.xml";
            if (Open.ShowDialog() == DialogResult.OK)
            {
                
                TNfeProc Nota;
                using (FileStream fs = new FileStream(Open.FileName, FileMode.Open))
                {
                    
                    
                    try
                    {
                        //System.Xml.Serialization.XmlSerializer XmlSerializer = new System.Xml.Serialization.XmlSerializer(typeof(TNFe));
                        Nota = (TNfeProc)new System.Xml.Serialization.XmlSerializer(typeof(TNfeProc)).Deserialize(fs);                        
                        spinEdit1.EditValue = Nota.NFe.infNFe.ide.cNF;
                        CNPJ.Text = Nota.NFe.infNFe.emit.Item;

                    }
                    catch (System.Exception ex)
                    {
                        MessageBox.Show("Nota eletronica inválida:" + ex.Message);
                    }
                }
            }
        }

        private void simpleButton2_Click(object sender, EventArgs e)
        {
            OpenFileDialog Open = new OpenFileDialog();
            Open.Filter = "*.xml|*.xml";
            if (Open.ShowDialog() == DialogResult.OK)
            {
                string lido = System.IO.File.ReadAllText(Open.FileName);
                lido = lido.Replace("\r", "");
                lido = lido.Replace("\n", "");
                System.IO.File.WriteAllText(@"c:\\lixo\limpo.xml", lido);                
            }
        }
    }
}
