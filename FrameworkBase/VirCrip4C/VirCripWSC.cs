﻿using System;
using System.Text;
using System.Security.Cryptography;
using System.IO;

namespace VirCrip4C
{
    /// <summary>
    /// 
    /// </summary>
    public static class VirCripWS
    {
        private static byte[] aChave
        {
            get
            {
                string Chave = String.Format("Tartaruga Verde{0:dd}", DateTime.Today);
                byte[] retorno = new byte[8];
                Array.Copy(new SHA1CryptoServiceProvider().ComputeHash(Encoding.Unicode.GetBytes(Chave)), retorno, 8);
                return retorno;
            }
        }

        private static byte[] Vector = new byte[] { 32, 43, 218, 16, 53, 69, 118, 24 };

        private static DESCryptoServiceProvider _des;

        private static DESCryptoServiceProvider des
        {
            get
            {
                if (_des == null)
                    _des = new DESCryptoServiceProvider();
                return _des;
            }
        }

        /// <summary>
        /// Criptografa
        /// </summary>
        /// <param name="Condominio">Condominio</param>
        /// <param name="Torre">Torre</param>
        /// <param name="Unidade">Unidade</param>
        /// <returns></returns>
        public static string Crip(string Condominio, string Torre,string Unidade)
        {
            string entrada = string.Format("<valida> {0:dd MM yyyy HH} {1} {2} {3}",
                                     DateTime.Now,
                                     Condominio,
                                     Torre,
                                     Unidade); 
            Byte[] ret;
            using (MemoryStream arqSaida = new MemoryStream())
            {
                using (CryptoStream crStream = new CryptoStream(arqSaida, des.CreateEncryptor(aChave, Vector), CryptoStreamMode.Write))
                {
                    byte[] Gravar = Encoding.Unicode.GetBytes(entrada);
                    crStream.Write(Gravar, 0, Gravar.Length);
                    crStream.Close();
                }                
                ret = arqSaida.ToArray();                                
            }
            string Retorno = "";
            foreach (Byte b in ret)
                Retorno += string.Format("{0:000}", (int)b);
            return Retorno;
        }        
    }
}
