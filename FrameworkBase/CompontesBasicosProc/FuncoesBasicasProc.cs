﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using System.Windows.Forms;

namespace CompontesBasicosProc
{
    /// <summary>
    /// Funcoes básicas de processos
    /// </summary>
    public static class FuncoesBasicasProc
    {
        #region Extension
        /// <summary>
        /// Funciana como um conjunto de if ou um in grupo
        /// </summary>
        /// <typeparam name="T">Faz com que possa ser usado em qualquer tipo</typeparam>
        /// <param name="item">objeto a ser comparado</param>
        /// <param name="items">alternativas</param>
        /// <returns></returns>
        public static bool EstaNoGrupo<T>(this T item, params T[] items)
        {
            if (items == null)
                return false;
            for (int i = 0; i < items.Length; i++)
                if (item.Equals(items[i]))
                    return true;
            return false;
        }

        /// <summary>
        /// Funciana como um conjunto de if ou um in grupo. Não precisa do cast
        /// </summary>
        /// <typeparam name="T">Faz com que possa ser usado em qualquer tipo</typeparam>
        /// <param name="item">objeto a ser comparado</param>
        /// <param name="items">alternativas</param>
        /// <returns></returns>
        public static bool EstaNoGrupo<T>(this T item, params object[] items)
        {
            if (items == null)
                return false;
            for (int i = 0; i < items.Length; i++)
                if (items[i] is T)
                    if (item.Equals((T)items[i]))
                        return true;
            return false;
        }

        /// <summary>
        /// Limpa o string
        /// </summary>
        /// <param name="Entrada"></param>
        /// <param name="Tipos"></param>
        /// <remarks>o conteúdo da entrada é alterado</remarks>
        /// <returns></returns>
        public static string Limpa(this string Entrada, params TiposLimpesa[] Tipos)
        {
            bool RemoveBrancosDuplicados = false;
            bool RemoveQuebrasLinha = false;
            bool SoNumeros = false;
            bool RemoveAcentos = false;
            bool Maiusculas = false;
            bool PrimeiroCaracMaiusculo = false;
            bool NomeArquivo = false;
            foreach (TiposLimpesa Tipo in Tipos)
                if (Tipo == TiposLimpesa.RemoveBrancosDuplicados)
                    RemoveBrancosDuplicados = true;
                else if (Tipo == TiposLimpesa.RemoveQuebrasLinha)
                    RemoveQuebrasLinha = true;
                else if (Tipo == TiposLimpesa.SoNumeros)
                    SoNumeros = true;
                else if (Tipo == TiposLimpesa.RemoveAcentos)
                    RemoveAcentos = true;
                else if (Tipo == TiposLimpesa.Maiusculas)
                    Maiusculas = true;
                else if (Tipo == TiposLimpesa.PrimeiroCaracMaiusculo)
                    PrimeiroCaracMaiusculo = true;
                else if (Tipo == TiposLimpesa.NomeArquivo)
                {
                    NomeArquivo = true;
                    RemoveBrancosDuplicados = true;
                }
            StringBuilder SB = new StringBuilder();
            bool eBranco = false;
            bool InicioPal = true;
            char novoc;
            foreach (char c in Entrada)
            {
                if (RemoveAcentos)
                    switch (c)
                    {
                        case 'á':
                        case 'à':
                        case 'ã':
                        case 'â':
                            novoc = 'a';
                            break;
                        case 'é':
                        case 'è':
                        case 'ê':
                            novoc = 'e';
                            break;
                        case 'í':
                        case 'ì':
                        case 'î':
                            novoc = 'i';
                            break;
                        case 'ó':
                        case 'ò':
                        case 'õ':
                        case 'ô':
                            novoc = 'o';
                            break;
                        case 'ú':
                        case 'ù':
                        case 'û':
                        case 'ü':
                            novoc = 'u';
                            break;
                        case 'ç':
                            novoc = 'c';
                            break;
                        case 'Á':
                        case 'À':
                        case 'Ã':
                        case 'Â':
                            novoc = 'A';
                            break;
                        case 'É':
                        case 'È':
                        case 'Ê':
                            novoc = 'E';
                            break;
                        case 'Í':
                        case 'Ì':
                        case 'Î':
                            novoc = 'I';
                            break;
                        case 'Ó':
                        case 'Ò':
                        case 'Õ':
                        case 'Ô':
                            novoc = 'O';
                            break;
                        case 'Ú':
                        case 'Ù':
                        case 'Û':
                        case 'Ü':
                            novoc = 'U';
                            break;
                        case 'Ç':
                            novoc = 'C';
                            break;
                        default:
                            novoc = c;
                            break;
                    }
                else
                    novoc = c;
                if (PrimeiroCaracMaiusculo)
                {
                    if (c == ' ')
                        InicioPal = true;
                    else
                        if (InicioPal)
                    {
                        novoc = char.ToUpper(novoc);
                        InicioPal = false;
                    }
                }
                if (SoNumeros)
                    if (!char.IsDigit(novoc))
                        continue;
                if (NomeArquivo)
                    if ((c == '\\') || (c == '/') || (c == '.'))
                        novoc = '_';
                if (RemoveBrancosDuplicados)
                {
                    if (novoc == ' ')
                    {
                        if (eBranco)
                            continue;
                        else
                            eBranco = true;
                    }
                    else
                        eBranco = false;
                }
                if (RemoveQuebrasLinha)
                    if ((novoc == '\r') || (novoc == '\n'))
                        continue;
                SB.Append(novoc);
            }
            Entrada = SB.ToString();
            if (RemoveBrancosDuplicados)
                Entrada = Entrada.Trim();
            if (Maiusculas)
                return Entrada.ToUpper();
            return Entrada;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="Str"></param>
        /// <param name="Tipos"></param>
        /// <returns></returns>
        public static string Limpa(this Stream Str, params TiposLimpesa[] Tipos)
        {
            Str.Position = 0;
            string strLido = new StreamReader(Str).ReadToEnd();
            return Limpa(strLido, Tipos);
        }


        /// <summary>
        /// Tipo de limpeza de string
        /// </summary>
        public enum TiposLimpesa
        {
            /// <summary>
            /// 
            /// </summary>
            RemoveBrancosDuplicados,
            /// <summary>
            /// 
            /// </summary>
            RemoveQuebrasLinha,
            /// <summary>
            /// 
            /// </summary>
            SoNumeros,
            /// <summary>
            /// 
            /// </summary>
            RemoveAcentos,
            /// <summary>
            /// 
            /// </summary>
            Maiusculas,
            /// <summary>
            /// 
            /// </summary>
            PrimeiroCaracMaiusculo,
            /// <summary>
            /// 
            /// </summary>
            NomeArquivo
        }

        #endregion

        /// <summary>
        /// Indica se o programa está em produção
        /// </summary>
        /// <remarks>
        /// Deve ser setado no formulário principal
        /// Nos programas de teste dos módulos NÂO deve ser setado
        /// </remarks>
        public static bool EstaEmProducao = false;

        private const int WM_SETREDRAW = 0x000B;

        private static void Suspend(Control control)
        {
            Message msgSuspendUpdate = Message.Create(control.Handle, WM_SETREDRAW, IntPtr.Zero,
                IntPtr.Zero);

            NativeWindow window = NativeWindow.FromHandle(control.Handle);
            window.DefWndProc(ref msgSuspendUpdate);
        }

        private static void Resume(Control control, bool Filhos)
        {
            // Create a C "true" boolean as an IntPtr
            IntPtr wparam = new IntPtr(1);
            Message msgResumeUpdate = Message.Create(control.Handle, WM_SETREDRAW, wparam,
                IntPtr.Zero);

            NativeWindow window = NativeWindow.FromHandle(control.Handle);
            window.DefWndProc(ref msgResumeUpdate);

            control.Invalidate(Filhos);
        }

        /// <summary>
        /// Não setar em produção
        /// </summary>
        public static bool AmbienteDesenvolvimento = false;
                
        /// <summary>
        /// Executa a tarefa congelando o controle
        /// </summary>
        /// <param name="Tarefa">Tarefa a ser executada</param>
        /// <param name="control">Controle a ser travado</param>
        /// <param name="filhos">Paint para os controles filhos</param>
        public static void TravaPaint(Action Tarefa, Control control, bool filhos)
        {
            try
            {
                Suspend(control);
                Tarefa();
            }
            finally
            {
                Resume(control, filhos);
            }
        }
    }

    /// <summary>
    /// Objeto para retorno de informações nas chamadas do servidor de processos
    /// </summary>
    [Serializable()]
    public class RetornoServidorProcessos
    {
        /// <summary>
        /// Indica se o sesultado foi ok
        /// </summary>
        public bool ok;

        /// <summary>
        /// Caso tenha ocorrido um erro ele é classificado como recuperável ou nao (ver destrição dos tipos)
        /// </summary>
        public VirExceptionProc.VirTipoDeErro TipoDeErro;

        /// <summary>
        /// Mensagem de retorno
        /// </summary>
        public string Mensagem;

        /// <summary>
        /// 
        /// </summary>
        public string MensagemSimplificada;

        /// <summary>
        /// Construtor
        /// </summary>
        /// <param name="mensagem"></param>
        /// <param name="mensagemSimplificada"></param>
        public RetornoServidorProcessos(string mensagemSimplificada, string mensagem)
        {
            ok = true;
            Mensagem = mensagem;
            MensagemSimplificada = mensagemSimplificada;
            TipoDeErro = VirExceptionProc.VirTipoDeErro.sem_erro;
        }
    }

    /// <summary>
    /// Tipos de servidor de processos
    /// </summary>
    public enum TiposServidorProc
    {
        /// <summary>
        /// Chamada direta
        /// </summary>
        SemServidor = -1,
        /// <summary>
        /// Ainda nao definido
        /// </summary>
        Indefinido = 0,
        /// <summary>
        /// Faz uma chamda como faria o servidor para fins de debug
        /// </summary>
        Simulador = 1,
        /// <summary>
        /// Chama o servidor de processos local - Deve ser iniciado antes
        /// </summary>
        Local = 2,
        /// <summary>
        /// Servidor QAS
        /// </summary>
        QAS = 3,
        /// <summary>
        /// Produção
        /// </summary>
        Producao = 4
    }
}
