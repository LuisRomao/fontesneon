﻿using iTextSharp.text;
using iTextSharp.text.pdf;
using System;
using System.IO;

namespace CompontesBasicosProc
{
    /// <summary>
    /// Classe para agrupara pdf em um documento único para impressão
    /// </summary>
    public class AcumulaPDF : IDisposable
    {
        private PdfCopy pdfCopyProvider;
        private Document sourceDocument;
        private string NomeTMP;
        private int Paginas;

        /// <summary>
        /// Construtor
        /// </summary>
        public AcumulaPDF()
        {                        
        }

        private void CriaSaida()
        {
            sourceDocument = new Document();
            NomeTMP = string.Format("{0}\\TMP\\TMP{1:yyyy_MM_dd_HH_mm_ss_fff}.pdf", Path.GetDirectoryName(Environment.CommandLine.Replace("\"", "")), DateTime.Now);
            pdfCopyProvider = new PdfCopy(sourceDocument, new FileStream(NomeTMP, FileMode.Create));
            sourceDocument.Open();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="NomeArquivo"></param>
        /// <param name="pagIni"></param>
        /// <param name="pagFim"></param>
        public void add(string NomeArquivo, int pagIni = 1, int pagFim = 1)
        {
            if (Paginas == 0)
                CriaSaida();
            PdfReader reader = new PdfReader(NomeArquivo);
            add(reader, pagIni, pagFim);
            pagFim = Paginas = pagFim - pagIni + 1;
        }

        /// <summary>
        /// inclui um pdf
        /// </summary>
        /// <param name="reader"></param>
        /// <param name="pagIni"></param>
        /// <param name="pagFim"></param>
        public void add(PdfReader reader, int pagIni = 1,int pagFim = 1)
        {            
            try
            {                
                for (int pag = pagIni; pag <= pagFim; pag++)                                    
                    pdfCopyProvider.AddPage(pdfCopyProvider.GetImportedPage(reader, pag));                
            }
            finally
            {
                if (reader != null)
                    reader.Close();
            }
        }

        /// <summary>
        /// Termina o processo
        /// </summary>
        /// <param name="ok">Indica se deve abrir o arquivo ou só matar o TMP</param>
        public int Termina(bool ok)
        {            
            if ((ok) && (Paginas > 0))
            {
                sourceDocument.Close();
                System.Diagnostics.Process.Start(NomeTMP);
                return Paginas;
            }
            else
            {
                try { File.Delete(NomeTMP); } catch { }
                return 0;
            }            
        }

        /// <summary>
        /// Dispose
        /// </summary>
        public void Dispose()
        {
            if (Paginas != 0)
            {
                pdfCopyProvider.Dispose();
                sourceDocument.Dispose();
            }
        }
    }
}
