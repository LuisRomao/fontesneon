﻿using Abstratos;
using VirEnumeracoes;

namespace CompontesBasicosProc
{
    /// <summary>
    /// Conta corrente
    /// </summary>
    public class ContaCorrente : ABS_ContaCorrente
    {
        
        /// <summary>
        /// Indica se os dados foram encontrados
        /// </summary>
        public bool Encontrado = false;
        


        /// <summary>
        /// Construtor básico
        /// </summary>
        public ContaCorrente()
        {
            Encontrado = false;
        }

        /// <summary>
        /// Construtor
        /// </summary>
        /// <param name="Tipo">Tipo da conta</param>
        /// <param name="BCO"></param>
        /// <param name="Agencia"></param>
        /// <param name="AgenciaDg"></param>
        /// <param name="NumeroConta"></param>
        /// <param name="NumeroDg"></param>
        /// <param name="CNR"></param>
        /// <param name="CaixaPostal"></param>
        /// <param name="Titular"></param>
        /// <param name="CPF_CNPJ"></param>
        public ContaCorrente(TipoConta Tipo, int BCO, int Agencia, int? AgenciaDg, int NumeroConta, string NumeroDg, int? CNR = null, string CaixaPostal = "", string Titular = "", DocBacarios.CPFCNPJ CPF_CNPJ = null) : this()
        {
            CarregaDados(Tipo, BCO, Agencia, AgenciaDg, NumeroConta, NumeroDg, CNR, CaixaPostal,Titular,CPF_CNPJ);
            Encontrado = true;
        }

        /// <summary>
        /// Carrega os dados
        /// </summary>
        /// <param name="Tipo"></param>
        /// <param name="BCO"></param>
        /// <param name="Agencia"></param>
        /// <param name="AgenciaDg"></param>
        /// <param name="NumeroConta"></param>
        /// <param name="NumeroDg"></param>
        /// <param name="CNR"></param>
        /// <param name="CaixaPostal"></param>
        /// <param name="Titular"></param>
        /// <param name="CPF_CNPJ"></param>
        protected void CarregaDados(TipoConta Tipo, int BCO, int Agencia, int? AgenciaDg, int NumeroConta, string NumeroDg, int? CNR = null, string CaixaPostal = "", string Titular = "", DocBacarios.CPFCNPJ CPF_CNPJ = null)
        {
            this.Tipo = Tipo;
            this.BCO = BCO;
            this.Agencia = Agencia;
            this.AgenciaDg = AgenciaDg;
            this.NumeroConta = NumeroConta;
            this.NumeroDg = NumeroDg;
            this.CNR = CNR;
            this.CaixaPostal = CaixaPostal;
            this.Titular = Titular;
            this.CPF_CNPJ = CPF_CNPJ;
        }


        

        #region Representação str
        /// <summary>
        /// Numero em string
        /// </summary>
        public string strNumeroConta
        {
            get
            {
                switch (BCO)
                {
                    case 104:
                    case 237:
                        return NumeroConta.ToString("000000");
                    case 341:
                        return NumeroConta.ToString("00000");
                    default:
                        return CNR.ToString();
                }
            }
        }

        /// <summary>
        /// CNR em string
        /// </summary>
        public string strCNR
        {
            get
            {
                switch (BCO)
                {
                    case 104:
                        return CNR.HasValue ? CNR.Value.ToString("0000000") : string.Empty;
                    case 237:
                        return CNR.HasValue ? CNR.Value.ToString("000000") : string.Empty;
                    default:
                        return CNR.ToString();
                }
            }
        }

        /// <summary>
        /// Agência em string
        /// </summary>
        public string strAgencia { get { return Agencia.ToString("0000"); } }


        #endregion

        /// <summary>
        /// Valida conta corrente
        /// </summary>        
        public bool validaConta()
        {
            string conta = NumeroConta.ToString();
            int digito;
            //bool notificar = false;
            try
            {
                digito = int.Parse(NumeroDg);
            }
            catch
            {
                digito = 10;
                //notificar = true;
            }
            string pesoBradesco = "32765432";
            string pesoSantander = "97310097131973";
            string pesoBB = "98765432";
            //string pesoCaixa = "876543298765432";
            int soma = 0;
            int resto = 0;
            int Digito = 0;
            int x = 0;

            switch (BCO)
            {
                case 1: //BCO DO BRASIL S/A testado ok
                    x = pesoBB.Length - conta.Length;
                    for (int j = 0; j < x; j++) conta = conta.Insert(0, "0");                    
                    for (int i = 0; i < pesoBB.Length; i++)                    
                        if (i < x)                        
                            soma += int.Parse(pesoBB.Substring(i, 1)) * 0;                        
                        else                        
                            soma += int.Parse(pesoBB.Substring(i, 1)) * int.Parse(conta.Substring(i, 1).ToString());                                            
                    resto = soma % 11;
                    Digito = 11 - resto;
                    if (Digito == 11) { Digito = 0; } else if (Digito == 10) { Digito = 20; }
                    if (digito == 0 && Digito == 10)
                        return true;
                    else return (digito == Digito);                        
                    
                case 33: //SANTANDER testado ok
                    conta = string.Format("{0:0000}00{1:00000000}", Agencia, NumeroConta);
                    //x = pesoSantander.Length - conta.Length;
                    //for (int j = 0; j < x; j++) conta = conta.Insert(0, "0");                    
                    //if (x == 0) { x = conta.Length; }
                    for (int i = 0; i < pesoSantander.Length; i++)                    
                        //if (i < x && pesoSantander.Length != x)                        
                        //    soma += int.Parse(pesoSantander.Substring(i, 1)) * 0;                        
                        //else                        
                            soma += int.Parse(pesoSantander.Substring(i, 1)) * int.Parse(conta.Substring(i, 1).ToString());                                            
                    resto = soma % 10;
                    if (resto == 0)                    
                        Digito = 0;                    
                    else                    
                        Digito = 10 - resto;
                    return digito == Digito;                    
                                        
                case 104: //CAIXA ECONOMICA FEDERAL ( Validação desligada porque precisaria no tipo de conta )
                    return true;
                    /*
                    conta = string.Format("{0:0000}020{1:00000000}", Agencia, NumeroConta);
                    soma = 0;
                    for (int i = 0; i < pesoCaixa.Length; i++)                        
                        soma += int.Parse(pesoCaixa.Substring(i, 1)) * int.Parse(conta.Substring(i, 1).ToString());
                    soma *= 10;
                    resto = soma % 11;
                    if (resto == 10)
                        Digito = 0;
                    else
                        Digito = resto;
                    return digito == Digito;
                    */
                case 237: //BRADESCO S/A                     
                    x = pesoBradesco.Length - conta.Length;
                    for (int j = 0; j < x; j++) conta = conta.Insert(0, "0");                    
                    for (int i = 0; i < pesoBradesco.Length; i++)                    
                        if (i < x)                        
                            soma += int.Parse(pesoBradesco.Substring(i, 1)) * 0;                        
                        else                        
                            soma += int.Parse(pesoBradesco.Substring(i, 1)) * int.Parse(conta.Substring(i, 1).ToString());                                            
                    resto = soma % 11;
                    Digito = resto == 0 ? 0 : 11 - resto;                    
                    if (Digito == 11)                    
                        Digito = 0;                    
                    return (digito == Digito);
                                                                                    
                case 341: //BCO ITAU S/A          
                    conta = string.Format("{0:0000}{1:00000}", Agencia, NumeroConta);                    
                    for (int i = 0; i < conta.Length; i++)
                    {                        
                        int parc = int.Parse(conta.Substring(i, 1)) * (i % 2 == 0 ? 2 : 1);
                        if (parc >= 10)
                            parc = parc % 10 + parc / 10;
                        soma += parc;
                    }
                    resto = soma % 10;
                    Digito = 10 - resto;
                    if (Digito == 10)
                            Digito = 0;
                    return (digito == Digito);                        
                default:
                    return true;
            }            
        }

        /// <summary>
        /// Valida agência bancaria
        /// </summary>
        /// <returns></returns>        
        public bool validaAgencia()
        {
            string pesoBradesco = "5432";
            string pesoBB = "5432";
            string agencia = Agencia.ToString("0000");
            //string erro = agencia.Substring(10, 10);
            int soma = 0;
            int resto = 0;
            int Digito = 0;

            switch (BCO)
            {
                case 1: //Banco do Brasil
                    for (int i = 0; i < pesoBB.Length; i++)                    
                        soma += int.Parse(pesoBB.Substring(i, 1)) * int.Parse(agencia.Substring(i, 1));                    
                    resto = soma % 11;
                    Digito = 11 - resto;
                    if (Digito == 11) { Digito = 0; } else if (Digito == 10) { Digito = 20; }

                    return (AgenciaDg.Value == Digito);
                    
                case 237: //Bradesco
                    for (int i = 0; i < pesoBradesco.Length; i++)                    
                        soma += int.Parse(pesoBradesco.Substring(i, 1)) * int.Parse(agencia.Substring(i, 1));                    
                    resto = soma % 11;
                    Digito = 11 - resto;
                    if (Digito >= 10)
                        Digito = 0;
                    return AgenciaDg.Value == Digito;
                case 341:
                case 33:
                    //Não tem validador de agência
                    return true;
                default:
                    return true;
            }         
        }

        /// <summary>
        /// Representação string
        /// </summary>
        /// <returns></returns>
        public override string ToString()
        {
            if (BCO != 0)
                switch ((CodigoBancos)BCO)
                {                                            
                    case CodigoBancos.Itau:
                        return string.Format("{0:000} {1:0000} {3:00000}-{4} {5}", BCO, Agencia, AgenciaDg, NumeroConta, NumeroDg, Tipo == TipoConta.CC ? "C.C." : "Poupança");
                    case CodigoBancos.Bradesco:
                    case CodigoBancos.Caixa:
                    default:
                        return string.Format("{0:000} {1:0000}-{2} {3:000000}-{4} {5}", BCO, Agencia, AgenciaDg, NumeroConta, NumeroDg, Tipo == TipoConta.CC ? "C.C." : "Poupança");
                }
            else
                return " ____-_ ______-_ ";
        }

        /// <summary>
        /// Uso de ContaCorrenteNeon
        /// </summary>
        public override int? CCG => throw new System.NotImplementedException("Uso de ContaCorrenteNeon");

        /// <summary>
        /// Uso de ContaCorrenteNeon
        /// </summary>
        public override int? CCT => throw new System.NotImplementedException("Uso de ContaCorrenteNeon");
    }
}
