/*
LH - 07/10/2014       - 14.1.4.60 - Inclus�o de IRPF na enumera��o TipoImposto
MR - 26/12/2014 10:00 -           - Inclus�o de FGTS na enumera��o TipoImposto e revis�o dos outros m�todos referentes a esse novo imposto (Altera��es indicadas por *** MRC - INICIO - TRIBUTOS (26/12/2014 10:00) ***)
LH - 07/10/2014       - 14.2.4.2  - Inclus�o de IRPF na propriedade Guia
LH - 12/02/2015       - 14.2.4.21 - Teto do INSS
MR - 27/04/2015 12:00             - Nova enumera��o para origem do imposto, folha ou nota (Altera��es indicadas por *** MRC - INICIO - TRIBUTO ELETRONICO (27/04/2015 12:00) ***)
 */

using System;
using CompontesBasicosProc;
using VirEnumeracoes;




namespace dllImpostos
{    

    /// <summary>
    /// Imposto em geral
    /// </summary>
    public class Imposto
    {        
        /// <summary>
        /// 
        /// </summary>
        public TipoImposto tipo;

        /// <summary>
        /// 
        /// </summary>
        protected decimal valorBase;

        /// <summary>
        /// Valor Base de calculo
        /// </summary>
        public decimal ValorBase
        {
            get { return valorBase; }
            set { valorBase = value; }
        }

        /// <summary>
        /// 
        /// </summary>
        protected DateTime dataNota;

        /// <summary>
        /// Data da NF
        /// </summary>
        public DateTime DataNota
        {
            get { return dataNota; }
            set { dataNota = value; }
        }

        /// <summary>
        /// Data do pagamento da Nota N�O do imposto
        /// </summary>
        protected DateTime dataPagamento;

        /// <summary>
        /// Data do pagamento da Nota N�O do imposto
        /// </summary>
        public DateTime DataPagamento
        {
            get { return dataPagamento; }
            set { dataPagamento = value; }
        }

        /// <summary>
        /// Crit�rios para c�lculo do vecimento
        /// </summary>
        protected enum TipoVencimento
        {
            /// <summary>
            /// Pr�xima quinzena
            /// </summary>
            ProximaQuizena = 0,
            /// <summary>
            /// dia 25 do pr�ximo m�s
            /// </summary>
            Dia25doProximoMes = 25,
            /// <summary>
            /// dia 20 do pr�ximo m�s
            /// </summary>
            Dia20doProximoMes = 20,
            /// <summary>
            /// dia 15 do pr�ximo m�s
            /// </summary>
            Dia15doProximoMes = 15,
            //*** MRC - INICIO - TRIBUTOS (26/12/2014 10:00) ***                               
            /// <summary>
            /// dia 07 do pr�ximo m�s
            /// </summary>
            Dia07doProximoMes = 7,
            //*** MRC - TERMINO - TRIBUTOS (26/12/2014 10:00) ***                               
            /// <summary>
            /// �ltimo dia do pr�ximo m�s
            /// </summary>
            UltimoDiaProximoMes = 8,
        }

        /// <summary>
        /// Tipo de vencimento
        /// </summary>
        protected TipoVencimento TipoVenc
        {
            get
            {
                switch (tipo)
                {
                    case TipoImposto.IR:
                    case TipoImposto.IRPF:
                    case TipoImposto.ISS_SA:
                    case TipoImposto.PIS_COFINS_CSLL:
                        return TipoVencimento.Dia20doProximoMes;
                    case TipoImposto.PIS:
                        return TipoVencimento.Dia25doProximoMes;
                    case TipoImposto.COFINS:
                    case TipoImposto.CSLL:
                        return TipoVencimento.ProximaQuizena;
                    /*
                case TipoImposto.PIS_COFINS_CSLL:
                    if (DataNota < new DateTime(2015, 6, 22))
                        return TipoVencimento.ProximaQuizena;
                    else
                        return TipoVencimento.UltimoDiaProximoMes;
                    */
                    case TipoImposto.ISSQN:
                        return TipoVencimento.Dia15doProximoMes;
                    case TipoImposto.INSS:
                    case TipoImposto.INSSpf:
                    case TipoImposto.INSSpfEmp:
                    case TipoImposto.INSSpfRet:
                        return TipoVencimento.Dia20doProximoMes;
                    //*** MRC - INICIO - TRIBUTOS (26/12/2014 10:00) ***                               
                    case TipoImposto.FGTS:
                        return TipoVencimento.Dia07doProximoMes;
                    //*** MRC - TERMINO - TRIBUTOS (26/12/2014 10:00) ***                               
                    default: throw new NotImplementedException();
                }
            }
        }

        /// <summary>
        /// 
        /// </summary>
        public DateTime DataBase
        {
            get
            {
                switch (tipo)
                {
                    case TipoImposto.PIS:
                    case TipoImposto.COFINS:
                    case TipoImposto.CSLL:
                    case TipoImposto.PIS_COFINS_CSLL:
                        return dataPagamento;
                    case TipoImposto.ISSQN:
                    case TipoImposto.ISS_SA:
                    case TipoImposto.INSS:
                    case TipoImposto.INSSpf:
                    case TipoImposto.INSSpfEmp:
                    case TipoImposto.INSSpfRet:
                    case TipoImposto.IR:
                    case TipoImposto.IRPF:
                    //*** MRC - INICIO - TRIBUTOS (26/12/2014 10:00) ***                               
                    case TipoImposto.FGTS:
                        //*** MRC - TERMINO - TRIBUTOS (26/12/2014 10:00) ***                               
                        return dataNota;
                    default: throw new NotImplementedException();
                }
            }
            set
            {
                switch (tipo)
                {
                    case TipoImposto.PIS:
                    case TipoImposto.COFINS:
                    case TipoImposto.CSLL:
                    case TipoImposto.PIS_COFINS_CSLL:
                        dataPagamento = value;
                        break;
                    case TipoImposto.ISSQN:
                    case TipoImposto.ISS_SA:
                    case TipoImposto.INSS:
                    case TipoImposto.INSSpf:
                    case TipoImposto.INSSpfEmp:
                    case TipoImposto.INSSpfRet:
                    case TipoImposto.IR:
                    case TipoImposto.IRPF:
                    //*** MRC - INICIO - TRIBUTOS (26/12/2014 10:00) ***                               
                    case TipoImposto.FGTS:
                        //*** MRC - TERMINO - TRIBUTOS (26/12/2014 10:00) ***                               
                        dataNota = value;
                        break;
                }
            }
        }

        /// <summary>
        /// Dia de vencimento       
        /// </summary>
        public virtual DateTime Vencimento
        {
            get
            {
                DateTime Manobra;
                switch (TipoVenc)
                {
                    case TipoVencimento.ProximaQuizena:
                        Manobra = new DateTime(DataBase.Year, DataBase.Month, 1);
                        if (DataBase.Day <= 15)
                            return Manobra = Manobra.AddMonths(1).AddDays(-1);
                        else
                            return Manobra = Manobra.AddMonths(1).AddDays(14);

                    case TipoVencimento.Dia20doProximoMes:
                        return new DateTime(DataBase.Year, DataBase.Month, 20).AddMonths(1);
                    case TipoVencimento.Dia25doProximoMes:
                        return new DateTime(DataBase.Year, DataBase.Month, 25).AddMonths(1);
                    case TipoVencimento.Dia15doProximoMes:
                        return new DateTime(DataBase.Year, DataBase.Month, 15).AddMonths(1);
                    //*** MRC - INICIO - TRIBUTOS (26/12/2014 10:00) ***                               
                    case TipoVencimento.Dia07doProximoMes:
                        return new DateTime(DataBase.Year, DataBase.Month, 7).AddMonths(1);
                    //*** MRC - TERMINO - TRIBUTOS (26/12/2014 10:00) ***                               
                    case TipoVencimento.UltimoDiaProximoMes:
                        return new DateTime(DataBase.Year, DataBase.Month, 1).AddMonths(2).AddDays(-1);
                    default: throw new NotImplementedException();
                }
            }
        }

        /// <summary>
        /// Data de apura��o para o DARF
        /// </summary>
        public virtual DateTime Apuracao
        {
            get
            {
                DateTime Manobra;

                switch (TipoVenc)
                {
                    case TipoVencimento.ProximaQuizena:
                        if (DataBase.Day <= 15)
                        {
                            Manobra = new DateTime(DataBase.Year, DataBase.Month, 15);
                        }
                        else
                        {
                            Manobra = new DateTime(DataBase.Year, DataBase.Month, 1);
                            Manobra = Manobra.AddMonths(1).AddDays(-1);
                        }
                        return Manobra;
                    case TipoVencimento.Dia25doProximoMes:
                    case TipoVencimento.Dia20doProximoMes:
                    case TipoVencimento.Dia15doProximoMes:
                    case TipoVencimento.Dia07doProximoMes:
                    case TipoVencimento.UltimoDiaProximoMes:
                        Manobra = new DateTime(Vencimento.Year, Vencimento.Month, 1);
                        Manobra = Manobra.AddDays(-1);
                        return Manobra;
                    default:
                        return DateTime.MinValue;
                }
            }
        }

        /// <summary>
        /// Indica falha no auto-teste
        /// </summary>
        public static bool Falha = false;

        /// <summary>
        /// Relatorio do auto teste
        /// </summary>
        public static string Relatorio;

        /// <summary>
        /// Efetua o auto teste
        /// </summary>
        /// <returns></returns>
        public static String AutoTeste() {
            Falha = false;
            Relatorio = "RELAT�RIO DE TESTE IR\r\n---------------------------\r\n\r\n";
            Imposto Imposto1 = new Imposto(TipoImposto.IR);
            Relatorio += Imposto1.TesteIndividual(new DateTime(2008, 1, 1), new DateTime(2008, 2, 1), new DateTime(2008, 3, 20),100,1.5M);
            Relatorio += Imposto1.TesteIndividual(new DateTime(2008, 1, 14), new DateTime(2008, 2, 14), new DateTime(2008, 3, 20), 200, 3);
            Relatorio += Imposto1.TesteIndividual(new DateTime(2008, 1, 15), new DateTime(2008, 2, 15), new DateTime(2008, 3, 20), 1, 0.02M);
            Relatorio += Imposto1.TesteIndividual(new DateTime(2008, 1, 16), new DateTime(2008, 2, 16), new DateTime(2008, 3, 20), 0.66M, 0.01M);
            Relatorio += Imposto1.TesteIndividual(new DateTime(2008, 1, 29), new DateTime(2008, 2, 29), new DateTime(2008, 3, 20), 0.67M, 0.02M);
            Relatorio += Imposto1.TesteIndividual(new DateTime(2008, 2, 1), new DateTime(2008, 3, 1), new DateTime(2008, 4, 18), 100, 1.5M);
            Relatorio += Imposto1.TesteIndividual(new DateTime(2010, 1, 1), new DateTime(2010, 10, 30), new DateTime(2010, 11, 19), 1000, 15);
            Relatorio += "\r\n\r\nRELAT�RIO DE TESTE Contribui�oes\r\n---------------------------\r\n\r\n";
            Imposto1 = new Imposto(TipoImposto.PIS_COFINS_CSLL);
            Relatorio += Imposto1.TesteIndividual(new DateTime(2008, 1, 1), new DateTime(2008, 2, 1), new DateTime(2008, 2, 29), 100, 4.65M);
            Relatorio += Imposto1.TesteIndividual(new DateTime(2008, 1, 14), new DateTime(2008, 2, 14), new DateTime(2008, 2, 29), 200, 9.30M);
            Relatorio += Imposto1.TesteIndividual(new DateTime(2008, 1, 15), new DateTime(2008, 2, 15), new DateTime(2008, 2, 29), 1, 0.05M);
            Relatorio += Imposto1.TesteIndividual(new DateTime(2008, 1, 16), new DateTime(2008, 2, 16), new DateTime(2008, 3, 14), 0.66M, 0.04M);
            Relatorio += Imposto1.TesteIndividual(new DateTime(2008, 1, 29), new DateTime(2008, 2, 29), new DateTime(2008, 3, 14), 0.67M, 0.04M);
            Relatorio += Imposto1.TesteIndividual(new DateTime(2008, 2, 1), new DateTime(2008, 3, 1), new DateTime(2008, 3, 31), 100, 4.65M);
            Relatorio += Imposto1.TesteIndividual(new DateTime(2010, 1, 1), new DateTime(2010, 10, 30), new DateTime(2010, 11, 12), 1000, 46.50M);                        
            return Relatorio;
        }

        /// <summary>
        /// Guia para recolhimento
        /// </summary>
        public guias Guia
        {
            get
            {
                switch (Tipo)
                {
                    case TipoImposto.COFINS:
                    case TipoImposto.CSLL:
                    case TipoImposto.IR:
                    case TipoImposto.IRPF: //incluido em 14.2.4.2
                    case TipoImposto.PIS:
                    case TipoImposto.PIS_COFINS_CSLL:
                        return guias.DARF;
                    case TipoImposto.INSS:
                    case TipoImposto.INSSpf:
                    case TipoImposto.INSSpfEmp:
                    case TipoImposto.INSSpfRet:
                        return guias.GPS;
                    case TipoImposto.ISSQN:
                    case TipoImposto.ISS_SA:
                    //*** MRC - INICIO - TRIBUTOS (26/12/2014 10:00) ***                               
                    case TipoImposto.FGTS:
                    //*** MRC - TERMINO - TRIBUTOS (26/12/2014 10:00) ***                               
                    default:
                        return guias.Nulo;
                }
            }
        }

        /// <summary>
        /// Nominal do cheque
        /// </summary>
        /// <returns></returns>
        public string Nominal() 
        {
            switch (Tipo)
            {
                case TipoImposto.IR:
                case TipoImposto.IRPF:
                case TipoImposto.PIS:                   
                case TipoImposto.COFINS:                  
                case TipoImposto.CSLL:                   
                case TipoImposto.PIS_COFINS_CSLL:
                    return "Secretaria da Receita Federal";                    
                case TipoImposto.ISSQN:
                    //return "Prefeitura Municipal de Sao Bernardo";
                    return "";
                case TipoImposto.ISS_SA:
                    return "Prefeitura Municipal de Santo Andre";
                case TipoImposto.INSS:
                case TipoImposto.INSSpfEmp:
                case TipoImposto.INSSpfRet:
                case TipoImposto.INSSpf:
                    return "INSS";
                //*** MRC - INICIO - TRIBUTOS (26/12/2014 10:00) ***                               
                case TipoImposto.FGTS:
                    return "FGTS";
                //*** MRC - TERMINO - TRIBUTOS (26/12/2014 10:00) ***                               
                default:
                    return "";
            }
        }

        

        /// <summary>
        /// Faz um teste do auto-teste
        /// </summary>
        /// <param name="_DataNota"></param>
        /// <param name="_DataPagamento">Data do pagamento da nota geradora - N�O do imposto</param>
        /// <param name="ResultadoPrevisto"></param>
        /// <param name="_ValorBase"></param>
        /// <param name="ValorEsperado"></param>
        /// <returns></returns>
        protected string TesteIndividual(DateTime _DataNota,DateTime _DataPagamento,DateTime ResultadoPrevisto,decimal _ValorBase,decimal ValorEsperado){              
            string Relatorio = "";
            DataNota = _DataNota;
            DataPagamento = _DataPagamento;
            ValorBase = _ValorBase;
            DateTime Venc = Vencimento;
            bool ok = true;
            if (Venc != ResultadoPrevisto)                
            {
                Relatorio += String.Format("FALHA Esperado: {0:dd/MM/yyyy} | ", ResultadoPrevisto);
                ok = false;
            };
            if (ValorImposto != ValorEsperado)
            {
                Relatorio += String.Format("FALHA Esperado: {0:n2} | ", ValorEsperado);
                ok = false;
            };
            if(ok)
                Relatorio += "OK    ";
            else
                Falha = true;
            Relatorio += String.Format("nota {0:dd/MM/yyyy}-{1:n2} pagamento {2:dd/MM/yyyy} -> {3:dd/MM/yyyy}-{4:n2}\r\n", DataNota, _ValorBase, DataPagamento, Venc, ValorImposto);
            return Relatorio;
        }

       

        

        

        /// <summary>
        /// Municipio do contribuinte
        /// </summary>
        public int CodMunicipioContribuinte;

        /// <summary>
        /// Municipio do Fornecedor
        /// </summary>
        public int CodMunicipioFornecedor;

        

        /// <summary>
        /// Tipo de imposto
        /// </summary>
        public TipoImposto Tipo
        {
            get { return tipo; }
            set { tipo = value; }
        }

        /// <summary>
        /// Construtor
        /// </summary>
        /// <param name="_Tipo">Tipo de imposto</param>
        public Imposto(TipoImposto _Tipo) 
        {
            tipo = _Tipo;
        }

        /// <summary>
        /// Construtor
        /// </summary>
        public Imposto()
        {            
        }

        /// <summary>
        /// 
        /// </summary>
        public string Codigo 
        {
            get 
            {
                switch (Tipo)
                {
                    case TipoImposto.IR:
                        return "1708"; 
                    case TipoImposto.IRPF:
                        return "0588";
                    case TipoImposto.PIS:
                        break;
                    case TipoImposto.COFINS:
                        break;
                    case TipoImposto.CSLL:
                        break;
                    case TipoImposto.PIS_COFINS_CSLL:
                        return "5952";
                    case TipoImposto.ISSQN:
                    case TipoImposto.ISS_SA:
                        break;
                    case TipoImposto.INSS:
                        return "2631";
                    case TipoImposto.INSSpfRet:                    
                    case TipoImposto.INSSpfEmp:
                    case TipoImposto.INSSpf:
                        return "2100";
                    //*** MRC - INICIO - TRIBUTOS (26/12/2014 10:00) ***                               
                    case TipoImposto.FGTS:
                        break;
                    //*** MRC - TERMINO - TRIBUTOS (26/12/2014 10:00) ***                               
                    default:
                        break;
                }
                return "";
            }
        }

        /// <summary>
        /// 
        /// </summary>
        public string DescImposto
        {
            get
            {
                switch (Tipo)
                {
                    case TipoImposto.IR:
                        return "DARF-IR PJ";
                    case TipoImposto.IRPF:
                        return "DARF-IR PF";
                    case TipoImposto.PIS:
                        break;
                    case TipoImposto.COFINS:
                        break;
                    case TipoImposto.CSLL:
                        break;
                    case TipoImposto.PIS_COFINS_CSLL:
                        return "DARF-CSLL/PIS/COFINS";
                    case TipoImposto.ISSQN:
                    case TipoImposto.ISS_SA:
                        return "ISS";
                    case TipoImposto.INSS:
                        return "GPS-Mao de obra";
                    case TipoImposto.INSSpfEmp:
                        return "GPS-P.F Tomador";
                    case TipoImposto.INSSpfRet:
                        return "GPS-P.F. Prestador";
                    case TipoImposto.INSSpf:
                        return "GPS-P.F. Prestador/Tomador";
                    //*** MRC - INICIO - TRIBUTOS (26/12/2014 10:00) ***                               
                    case TipoImposto.FGTS:
                        return "FGTS";
                    //*** MRC - TERMINO - TRIBUTOS (26/12/2014 10:00) ***                               
                    default:
                        break;
                }
                return "";
            }
        }

        /// <summary>
        /// descricao reduzida do imposto
        /// </summary>
        /// <returns></returns>
        public static string DescRedImp(TipoImposto Tipo)
        {
            switch (Tipo)
            {
                case TipoImposto.IR:
                case TipoImposto.IRPF:
                    return "IR";
                case TipoImposto.PIS:
                    return "PIS";
                case TipoImposto.COFINS:
                    return "COFINS";
                case TipoImposto.CSLL:
                    return "CSLL";
                case TipoImposto.PIS_COFINS_CSLL:
                    return "CSLL/PIS/COFINS";
                case TipoImposto.ISSQN:
                case TipoImposto.ISS_SA:
                    return "ISS";
                case TipoImposto.INSS:
                    return "M�o de obra";
                case TipoImposto.INSSpfEmp:
                    return "Tomador";
                case TipoImposto.INSSpfRet:
                    return "Prestador";
                case TipoImposto.INSSpf:
                    return "Prestador/Tomador";
                //*** MRC - INICIO - TRIBUTOS (26/12/2014 10:00) ***                               
                case TipoImposto.FGTS:
                    return "FGTS";
                //*** MRC - TERMINO - TRIBUTOS (26/12/2014 10:00) ***                               
                default:
                    return "";
            }            
        }

        /// <summary>
        /// 
        /// </summary>
        public string DescRedImposto
        {
            get
            {
                return DescRedImp(Tipo);
            }
        }

        /// <summary>
        /// 
        /// </summary>
        public string DescCodigo
        {
            get
            {
                switch (Tipo)
                {
                    case TipoImposto.IR:
                        return "Remunera��o de servi�os prestados por P Jur�dica";
                    case TipoImposto.IRPF:
                        return "Remunera��o de servi�os prestados por P F�sica";                        
                    case TipoImposto.PIS:
                        break;
                    case TipoImposto.COFINS:
                        break;
                    case TipoImposto.CSLL:
                        break;
                    case TipoImposto.PIS_COFINS_CSLL:
                        return "CSL/PIS/COFINS";
                    case TipoImposto.ISSQN:
                    case TipoImposto.ISS_SA:
                        break;
                    case TipoImposto.INSS:
                        break;
                    //*** MRC - INICIO - TRIBUTOS (26/12/2014 10:00) ***                               
                    case TipoImposto.FGTS:
                        break;
                    //*** MRC - TERMINO - TRIBUTOS (26/12/2014 10:00) ***                               
                    default:
                        break;
                }
                return "";
            }
        }        

        private DateTime VencimentoEfetivoManual = DateTime.MinValue;

        /// <summary>
        /// �ltimo dia util para pagamento do tributo.(j� embute a regra de postergar ou n�o)
        /// </summary>
        public DateTime VencimentoEfetivo
        {
            get {
                if (VencimentoEfetivoManual != DateTime.MinValue)
                    return VencimentoEfetivoManual;
                switch (TipoVenc)
                {
                    case TipoVencimento.ProximaQuizena:
                    case TipoVencimento.Dia20doProximoMes:
                    case TipoVencimento.Dia25doProximoMes:
                    case TipoVencimento.Dia15doProximoMes:                    
                    case TipoVencimento.Dia07doProximoMes:
                    case TipoVencimento.UltimoDiaProximoMes:
                        return AjustaParaDiaUtil(Vencimento, false);                                                                                        
                    default: throw new NotImplementedException();
                }
            }
            set 
            {
                VencimentoEfetivoManual = value;
                switch (TipoVenc)
                {
                    case TipoVencimento.ProximaQuizena:
                        if (VencimentoEfetivoManual.Day > 15) 
                            DataBase = new DateTime(VencimentoEfetivoManual.Year, VencimentoEfetivoManual.Month, 1);
                        else
                        {
                            DataBase = new DateTime(VencimentoEfetivoManual.Year, VencimentoEfetivoManual.Month, 1);
                            DataBase = DataBase.AddMonths(-1).AddDays(16);
                        }
                        break;
                    case TipoVencimento.Dia25doProximoMes:                        
                    case TipoVencimento.Dia20doProximoMes:                       
                    case TipoVencimento.Dia15doProximoMes:                    
                    case TipoVencimento.Dia07doProximoMes:     
                    case TipoVencimento.UltimoDiaProximoMes:
                        DataBase = new DateTime(VencimentoEfetivoManual.Year, VencimentoEfetivoManual.Month, 1);
                        DataBase = DataBase.AddMonths(-1);
                        break;
                    default:
                        break;
                }
            }
        }

        private decimal Aliquota
        {
            get
            {
                return AliquotaBase(Tipo);
            }
        }

        /// <summary>
        /// CNPJ da guia do prestador (ou do tomador)
        /// </summary>
        /// <param name="Tipo"></param>
        /// <returns></returns>
        public static bool Guia_Nome_Prestador(TipoImposto Tipo)
        {
            return Tipo == TipoImposto.INSS;
        }

        /// <summary>
        /// Desconta do pagamento? (por dentro)
        /// </summary>
        /// <param name="Tipo"></param>
        /// <returns></returns>
        public static bool Desconta(TipoImposto Tipo)
        {
            return Tipo != TipoImposto.INSSpfEmp;
        }

        /// <summary>
        /// CNPJ da guia do prestador (ou do tomador)
        /// </summary>       
        /// <returns></returns>
        public bool Guia_Nome_Prestador()
        {
            return Guia_Nome_Prestador(Tipo);
        }

        /// <summary>
        /// Desconta do pagamento? (por dentro)
        /// </summary>     
        /// <returns></returns>
        public bool Desconta()
        {
            return Desconta(Tipo);
        }

        //public static bool 

        /// <summary>
        /// Valor da aliquota
        /// </summary>
        public static decimal AliquotaBase(TipoImposto tipo)
        {

            switch (tipo)
            {
                case TipoImposto.IR:
                case TipoImposto.IRPF:
                    return 0.015M;
                case TipoImposto.PIS:
                    throw new NotImplementedException();
                case TipoImposto.COFINS:
                    throw new NotImplementedException();
                case TipoImposto.CSLL:
                    throw new NotImplementedException();
                case TipoImposto.PIS_COFINS_CSLL:
                    return 0.0465M;
                case TipoImposto.ISSQN:
                    return 0.02M;
                case TipoImposto.ISS_SA:
                    return 0.03M;
                case TipoImposto.INSS:
                case TipoImposto.INSSpfRet:
                    return 0.11M;
                case TipoImposto.INSSpf:
                    return 0.31M;
                case TipoImposto.INSSpfEmp:
                    return 0.2M;
                //*** MRC - INICIO - TRIBUTOS (26/12/2014 10:00) ***                               
                case TipoImposto.FGTS:
                    return 0.08M;
                //*** MRC - TERMINO - TRIBUTOS (26/12/2014 10:00) ***                               
                default:
                    throw new NotImplementedException();
            }

        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public decimal Teto()
        {
            return Teto(Tipo);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="tipo"></param>
        /// <returns></returns>
        public static decimal Teto(TipoImposto tipo)
        {

            switch (tipo)
            {
                case TipoImposto.INSSpfRet:
                    //return 406.09M;
                    //return 430.78M;
                    //return 482.93M;
                    //return 570.88M;
                    //return 608.44M;
                    return 621.04M;
                case TipoImposto.IR:
                case TipoImposto.IRPF:
                case TipoImposto.PIS:                   
                case TipoImposto.COFINS:                
                case TipoImposto.CSLL:                    
                case TipoImposto.PIS_COFINS_CSLL:                
                case TipoImposto.ISSQN:
                case TipoImposto.ISS_SA:               
                case TipoImposto.INSS:                                   
                case TipoImposto.INSSpf:         
                case TipoImposto.INSSpfEmp:
                //*** MRC - INICIO - TRIBUTOS (26/12/2014 10:00) ***                               
                case TipoImposto.FGTS:
                //*** MRC - TERMINO - TRIBUTOS (26/12/2014 10:00) ***                                             
                default:
                    return decimal.MaxValue;
            }

        }

        /// <summary>
        /// Valor m�nimo da base de c�lculo para gerar reten��o
        /// </summary>
        /// <param name="tipo"></param>
        /// <returns></returns>
        public static decimal PisoBaseCalculo(TipoImposto tipo)
        {

            switch (tipo)
            {
                case TipoImposto.PIS_COFINS_CSLL:
                    return 215.17M;
                case TipoImposto.INSSpfRet:                    
                case TipoImposto.IR:
                case TipoImposto.IRPF:
                case TipoImposto.PIS:
                case TipoImposto.COFINS:
                case TipoImposto.CSLL:                
                case TipoImposto.ISSQN:
                case TipoImposto.ISS_SA:
                case TipoImposto.INSS:
                case TipoImposto.INSSpf:
                case TipoImposto.INSSpfEmp:                
                case TipoImposto.FGTS:                
                default:
                    return decimal.MaxValue;
            }

        }

        /// <summary>
        /// 
        /// </summary>
        public decimal ValorImpostoManual;

        /// <summary>
        /// Valor calculado do imposto
        /// </summary>
        public decimal ValorImposto {
            get 
            {
                decimal retorno = (valorBase!=0) ? Math.Round(0.004999M + valorBase * Aliquota,2) : ValorImpostoManual;
                if (retorno > Teto())
                    retorno = Teto();
                return retorno;
            }
            set 
            {
                ValorImpostoManual = value;
            }
        }

        private DateTime AjustaParaDiaUtil(DateTime data, bool Adiantar)
        {            
            int passo = Adiantar ? 1 : -1;
            while ((data.DayOfWeek == DayOfWeek.Sunday) || (data.DayOfWeek == DayOfWeek.Saturday) || (IsFeriado(data)))
                data = data.AddDays(passo);
            return data;
        }

        /// <summary>
        /// Indica se o dia � feriado
        /// </summary>
        /// <param name="Data">data</param>
        /// <returns></returns>
        protected virtual bool IsFeriado(DateTime Data)
        {            
            if (Data.Day == 1)
                if (Data.Month == 1)
                    return true;
            if (Data.Day == 15)
                if (Data.Month == 11)
                    return true;
            return false;
        }

        //public void ImprimirGuia() { 
        //}
    }

    /// <summary>
    /// 
    /// </summary>
    public class SolicitaRetencao
    {
        /// <summary>
        /// 
        /// </summary>
        public TipoImposto Tipo;
        /// <summary>
        /// 
        /// </summary>
        public decimal Valor;
        /// <summary>
        /// 
        /// </summary>
        public SimNao Descontar;
        /// <summary>
        /// 
        /// </summary>
        /// <param name="Tipo"></param>
        /// <param name="Valor"></param>
        /// <param name="Descontar"></param>
        public SolicitaRetencao(TipoImposto Tipo, decimal Valor,SimNao Descontar)
        {
            this.Tipo = Tipo;
            this.Valor = Math.Round(Valor, 2, MidpointRounding.AwayFromZero);
            this.Descontar = Descontar;
        }
    }

}
