/*
MR - 01/07/2014 19:00 - 13.2.9.22 - Converte o c�digo de barras para linha digitavel (Altera��es indicadas por *** MRC - INICIO - PAG-FOR (01/07/2014 19:00) ***)
MR - 11/07/2014 10:00 - 14.1.4.0  - Inclui a conferencia do dac do codigo de barras no VerificaLinhaDigitavel (Altera��es indicadas por *** MRC - INICIO - PAG-FOR (11/07/2014 10:00) ***)
LH - 18/07/2014 15:00 - 14.1.4.5  - inclusao da carteira 16
MR - 25/02/2016 11:00 -           - Arquivo de Cobran�a Itau (Altera��es indicadas por *** MRC - INICIO - REMESSA (25/02/2016 11:00) ***)
LH - 31/08/2016 11:00 - 16.1.5.4  - Santander
*/

using System;
using System.Collections.Generic;
using System.Text;

namespace BoletosCalc
{
    /// <summary>
    /// Calculos dos boletos
    /// </summary>
    public class BoletoCalc
    {
        /// <summary>
        /// Modulo 10
        /// </summary>
        /// <param name="entrada"></param>
        /// <returns></returns>
        public static string Modulo10(string entrada)
        {
            Int32 soma = 0;
            Int32 y = 2;
            Int32 NUM1;
            for (int x = entrada.Length - 1; x >= 0; x--)
            {
                NUM1 = Int32.Parse(entrada.Substring(x, 1)) * y;
                if (NUM1 >= 10)
                    soma += (NUM1 / 10) + (NUM1 - (NUM1 / 10) * 10);
                else
                    soma += NUM1;
                y--;
                if (y == 0)
                    y = 2;
            };
            if (((Int32)(soma / 10)) == (soma / 10.0))
                return "0";
            else
                return ((Int32)((((soma / 10) * 10) + 10) - soma)).ToString();
        }

        /// <summary>
        /// Modulo 11 2/9 do HSBC
        /// </summary>
        /// <param name="entrada"></param>
        /// <returns></returns>
        public static string Modulo11_2_9HSBC(string entrada)
        {
            Int32 soma = 0;
            Int32 y = 9;
            Int32 resto;
            for (int x = entrada.Length - 1; x >= 0; x--)
            {
                soma += Int32.Parse(entrada.Substring(x, 1)) * y;
                y--;
                if (y == 1)
                    y = 9;
            };
            resto = (soma % 11);
            if ((resto == 0) || (resto > 9))
                return "0";
            else
                return resto.ToString();
        }

        /// <summary>
        /// Modulo 11 2/9
        /// </summary>
        /// <param name="entrada"></param>
        /// <param name="DigitoNN"></param>
        /// <returns></returns>
        public static string Modulo11_2_9(string entrada, bool DigitoNN)
        {
            //entrada = "11x" + entrada;
            //VirEmailNeon.EmailDiretoNeon.EmalST.EmTeste();
            Int32 soma = 0;
            Int32 y = 2;
            Int32 resto;
            for (int x = entrada.Length - 1; x >= 0; x--)
            {
                try
                {
                    soma += Int32.Parse(entrada.Substring(x, 1)) * y;
                }
                catch (Exception e)
                {
                    throw new Exception(string.Format("Erro no c�lculo do digito:'{0}' posi��o:{1}", entrada, x), e);
                }
                y++;
                if (y == 10)
                    y = 2;
            };
            resto = 11 - (soma % 11);
            if (DigitoNN)
            {
                if ((resto == 0) || (resto > 9))
                    return "0";
                else
                    return resto.ToString();
            }
            else
            {
                if ((resto == 0) || (resto == 1) || (resto > 9))
                    return "1";
                else
                    return resto.ToString();
            }
        }
        
        /// <summary>
        /// Verifica Linha Digit�vel, conferindo os DACs
        /// </summary>
        /// <param name="entrada"></param>
        /// <returns></returns>
        public static bool VerificaLinhaDigitavel(string entrada)
        {
            //Verifica bloco 1
            string intBloco1 = BoletoCalc.Modulo10(entrada.Substring(0, 9));
            if (intBloco1 != entrada.Substring(9, 1)) return false;

            //Verifica bloco 2
            string intBloco2 = BoletoCalc.Modulo10(entrada.Substring(10, 10));
            if (intBloco2 != entrada.Substring(20, 1)) return false;

            //Verifica bloco 3
            string intBloco3 = BoletoCalc.Modulo10(entrada.Substring(21, 10));
            if (intBloco3 != entrada.Substring(31, 1)) return false;
            
            //Verifica dac codigo de barras se apresenta fator de vencimento e valor
            if ((Convert.ToInt32(entrada.Substring(33, 4)) != 0) && (Convert.ToInt32(entrada.Substring(entrada.Length - 10, 10)) != 0))
            {
               string intDacCodigoBarras = BoletoCalc.Modulo11_2_9(entrada.Substring(0, 4) + entrada.Substring(33, 14) + entrada.Substring(4, 5) + entrada.Substring(10, 10) + entrada.Substring(21, 10), false);
               if (intDacCodigoBarras != entrada.Substring(32, 1)) return false;
            }

            //Retorna que conferiu
            return true;
        }
        //*** MRC - FIM - PAG-FOR (1) ***
        
        /// <summary>
        /// Converte o C�digo de Barras para Linha Digit�vel
        /// </summary>
        /// <param name="entrada"></param>
        /// <returns></returns>
        public static string ConverteCodigoBarrasEmLinhaDigitavel(string entrada)
        {
           //Inicia bloco 1
           string strBloco1 = entrada.Substring(0, 4) + entrada.Substring(19, 5);

           //Verifica digito bloco 1
           string digBloco1 = BoletoCalc.Modulo10(strBloco1);

           //Inicia bloco 2
           string strBloco2 = entrada.Substring(24, 10);

           //Verifica digito bloco 2
           string digBloco2 = BoletoCalc.Modulo10(strBloco2);

           //Inicia bloco 3
           string strBloco3 = entrada.Substring(34, 10);

           //Verifica digito bloco 3
           string digBloco3 = BoletoCalc.Modulo10(strBloco3);

           //Retorna linha digitavel
           return strBloco1 + digBloco1 + strBloco2 + digBloco2 + strBloco3 + digBloco3 + entrada.Substring(4, 15);
        }        

        /// <summary>
        /// Digito Bradesco
        /// </summary>
        /// <param name="entrada"></param>
        /// <returns></returns>
        public static string DigitoBradesco(string entrada)
        {
            Int32 Soma = 0;
            Int32 y = 2;
            Int32 resto;
            for (int x = entrada.Length - 1; x >= 0; x--)
            {
                Soma += Int32.Parse(entrada.Substring(x, 1)) * y;
                y++;
                if (y == 8)
                    y = 2;
            };
            resto = Soma % 11;
            switch (resto)
            {
                case 1: return "P";
                case 0: return "0";
                default: return ((Int16)(11 - resto)).ToString();
            };
        }

        private int BCO;
        private int BOL;
        private string Agencia;
        private string Conta;
        private string DigitoConta;
        private int CodigoCNR;
        private int Carteira;
        private string DigitoNossoNumero = "";
        
        /// <summary>
        /// 
        /// </summary>
        public string NossoNumeroStr = "";

        /// <summary>
        /// 
        /// </summary>
        public string codigoBarras;

        /// <summary>
        /// 
        /// </summary>
        public string[] CAMPOs;

        /// <summary>
        /// 
        /// </summary>
        public string linhadigitavel;

        /// <summary>
        /// Construtor
        /// </summary>
        /// <param name="_BCO"></param>
        /// <param name="_BOL"></param>
        /// <param name="_Agencia"></param>
        /// <param name="_Conta"></param>
        /// <param name="_DigitoConta"></param>
        /// <param name="_CodigoCNR"></param>
        /// <param name="_Carteira">Carteira 6(sem registro Bradesco) ou 9(com registro Bradesco) ou 175(sem registro Itau) ou 109(com registro Itau) ou 16</param>
        public BoletoCalc(int _BCO, int _BOL, string _Agencia, string _Conta, string _DigitoConta, string _CodigoCNR, int _Carteira)
        {
            BCO = _BCO;
            BOL = _BOL;
            Agencia = _Agencia;
            Conta = _Conta;
            DigitoConta = _DigitoConta;
            CodigoCNR = 0;
            Carteira = _Carteira;
            int.TryParse(_CodigoCNR, out CodigoCNR);            
            CAMPOs = new string[5];
        }
        

        private void CalculaNossoNumero()
        {            
            switch (BCO)
            {
                case 104:
                    NossoNumeroStr = string.Format("{0}4{1:000000000000000}", Carteira != 6 ? "1" : "2", BOL);
                    NossoNumeroStr += "-" + Modulo11_2_9(NossoNumeroStr, true);
                    break;
                case 237:
                    NossoNumeroStr = string.Format("{0:00}00{1:000000000}", Carteira, BOL);
                    DigitoNossoNumero = DigitoBradesco(NossoNumeroStr);
                    NossoNumeroStr = string.Format("{0}-{1}", NossoNumeroStr.Insert(2, "/"), DigitoBradesco(NossoNumeroStr));
                    break;
                case 341:                    
                    NossoNumeroStr = string.Format("{0}{1}{2}{3:00000000}", Agencia, Conta, Carteira, BOL);
                    DigitoNossoNumero = Modulo10(NossoNumeroStr);
                    NossoNumeroStr = string.Format("{0}-{1:00000000}-{2}", Carteira, BOL, DigitoNossoNumero);                    
                    break;
                case 33:
                    NossoNumeroStr = string.Format("{0:000000000000}", BOL);
                    NossoNumeroStr += Modulo11_2_9(NossoNumeroStr,true);
                    break;
                default:
                    throw new NotImplementedException(string.Format("BoletoCalc - N�o implementado para banco = {0}", BCO));
                    /*
                case 409:
                    //NossoNumeroStr = "001" + NossoNumero.ToString("0000000000") + Modulo11_2_9(NossoNumero.ToString());
                    //DigitoNossoNumero = Modulo11_2_9(NossoNumeroStr);
                    NossoNumeroStr = BOL.ToString("00000000000000");
                    DigitoNossoNumero = Modulo11_2_9(NossoNumeroStr, true);
                    NossoNumeroStr = BOL.ToString("00000000000000") + "/" + DigitoNossoNumero;
                    break;                    
                case 399:
                    NossoNumeroStr = BOL.ToString("0");
                    //NossoNumeroStr = "39104766";
                    NossoNumeroStr += (Modulo11_2_9HSBC(NossoNumeroStr) + "4");
                    DateTime Vencimento = rowPrincipal.BOLVencto;
                    //Vencimento = new DateTime(2000, 7, 4);
                    Int64 manobravenc = Vencimento.Day * 10000 + Vencimento.Month * 100 + (Vencimento.Year % 100);
                    Int64 manobra = Int64.Parse(NossoNumeroStr) + 3693872 + manobravenc;
                    NossoNumeroStr += BoletoCalc.Modulo11_2_9HSBC(manobra.ToString());
                    //NossoNumeroStr = "175-" + NN.ToString("00000000") + "-" + DigitoNossoNumero;
                    break;
                     */
            }
        }

        /// <summary>
        /// Calcula linha digit�vel
        /// </summary>
        /// <param name="DtVencimento"></param>
        /// <param name="Valor"></param>
        /// <returns></returns>
        public string CalculaLinhaDigitavel(DateTime DtVencimento, decimal Valor)
        {
            DateTime DataRef = new DateTime(1997, 10, 7);
            if (DtVencimento < DataRef)
                return "";
            Int32 Fatorvenc = ((TimeSpan)(DtVencimento - DataRef)).Days;
            string numero = string.Format("{0:000}9{1:0000}{2:0000000000}",
                                          BCO,
                                          Fatorvenc,
                                          (double)(100 * Valor));
            CalculaNossoNumero();
            switch (BCO)
            {
                case 33:
                    numero += string.Format("9{0:0000000}{1}0101", 
                                            CodigoCNR,
                                            NossoNumeroStr);
                    break;
                case 104:                    
                    string verificadorcodigocliente = BoletoCalc.Modulo11_2_9(CodigoCNR.ToString("000000"), true);
                    string livre = string.Format("{0:000000}{1}{2}{3}{4}4{5}", CodigoCNR, verificadorcodigocliente, NossoNumeroStr.Substring(2, 3), Carteira != 6 ? "1" : "2", NossoNumeroStr.Substring(5, 3), NossoNumeroStr.Substring(8, 9));
                    livre += BoletoCalc.Modulo11_2_9(livre, true);
                    numero += livre;                    
                    //CAMPOs[0] = String.Format("1049{0}", livre.Substring(0, 5));
                    //CAMPOs[1] = livre.Substring(5, 10);
                    //CAMPOs[2] = livre.Substring(15, 10);                    
                    break;
                case 237:
                    numero += String.Format("{0}{3:00}{1}0{2}0", 
                                            Agencia, 
                                            NossoNumeroStr.Substring(3, 11), 
                                            Conta, 
                                            Carteira);                    
                    break;
                case 341:
                    numero += string.Format("{0:000}{1:00000000}{2}{3}{4}{5}000", 
                                           Carteira,                                           
                                           BOL,
                                           DigitoNossoNumero,
                                           Agencia,
                                           Conta,
                                           DigitoConta);                    
                    //CAMPOs[1] = numero.Substring(24, 6) + DigitoNossoNumero + Agencia.Substring(0, 3);
                    //CAMPOs[2] = string.Format("{0}{1}{2}000", Agencia.Substring(3, 1), Conta.Substring(0, 5), DigitoConta.Substring(0, 1));                    
                    break;                
                default:
                    throw new NotImplementedException(string.Format("BoletoCalc - N�o implementado para banco = {0}", BCO));
                    /*
                case 399:
                    //1-3(3) codigo do banco 399
                    //4-4(1) Real 9 
                    //5-5(1) Verificador que sera colocado depois (pular)
                    //6-9(4) fator de vencimento
                    //10-19(10) valor
                    //20-26(7) Codigo do cedente
                    //27-39(13) Numero do boleto BOL
                    //40-43(4) Data em formato juliano DDDY
                    //44-44(1) numero 2
                    numero = "3999" + Fatorvenc.ToString("0000") + ((double)(100 * Valor)).ToString("0000000000") +
                             "3693872" +
                                          "000" + NN.ToString("0000000000") +
                                          DtVencimento.DayOfYear.ToString("000") + DtVencimento.Year.ToString().Substring(3, 1)
                                           + "2";
                    numero = numero.Insert(4, BoletoCalc.Modulo11_2_9(numero, false));
                    CAMPOs[0] = numero.Substring(0, 4) + numero.Substring(19, 5);
                    CAMPOs[1] = numero.Substring(24, 10);
                    CAMPOs[2] = numero.Substring(34, 10);
                    CAMPOs[3] = numero.Substring(4, 1);
                    CAMPOs[4] = numero.Substring(5, 14);
                    break;
                    
                case 409:
                    //int codigodocliente;
                    //if (rowComplementar.IsCONCodigoCNRNull())
                    //    throw new Exception("Codigo de cobran�a CNR n�o cadastrado");
                    //else
                    //    codigodocliente = int.Parse(rowComplementar.CONCodigoCNR);
                    numero = "4099" + Fatorvenc.ToString("0000") + ((double)(100 * Valor)).ToString("0000000000") + "5" +
                                          CodigoCNR.ToString("0000000") + "00" + BOL.ToString("00000000000000") + DigitoNossoNumero;
                    //};
                    numero = numero.Insert(4, BoletoCalc.Modulo11_2_9(numero, false));

                    CAMPOs[0] = "40995" + CodigoCNR.ToString("0000000").Substring(0, 4);
                    CAMPOs[1] = CodigoCNR.ToString("0000000").Substring(4, 3) + "00" + numero.Substring(29, 5);
                    CAMPOs[2] = numero.Substring(34, 10);
                    CAMPOs[3] = numero.Substring(4, 1);
                    CAMPOs[4] = numero.Substring(5, 14);
                    break;*/

            };
            numero = numero.Insert(4, BoletoCalc.Modulo11_2_9(numero, false));
            CAMPOs[0] = numero.Substring(0, 4) + numero.Substring(19, 5);
            CAMPOs[1] = numero.Substring(24, 10);
            CAMPOs[2] = numero.Substring(34, 10);
            CAMPOs[3] = numero.Substring(4, 1);
            CAMPOs[4] = numero.Substring(5, 14);
            codigoBarras = numero;
            //   CalculaBarras(numero);
            for (int i = 0; i <= 2; i++)
                CAMPOs[i] += BoletoCalc.Modulo10(CAMPOs[i]);
            linhadigitavel =
                   string.Format("{0}  {1}  {2}  {3}  {4}", 
                                  CAMPOs[0].Insert(5, "."), 
                                  CAMPOs[1].Insert(5, "."), 
                                  CAMPOs[2].Insert(5, "."), 
                                  CAMPOs[3].Substring(0, 1), 
                                  CAMPOs[4]);
            return linhadigitavel;
        }
    }
}
