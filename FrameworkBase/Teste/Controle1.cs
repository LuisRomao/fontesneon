using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace Teste
{
    public partial class Controle1 : CompontesBasicos.ComponenteBase
    {
        public Controle1()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            ShowModulo(typeof(TesteCampos),uSUARIOSBindingSource,false);
        }

        private void button2_Click(object sender, EventArgs e)
        {
            MessageBox.Show(ShowModuloAdd(typeof(TesteCampos)).ToString());
        }

        private void uSUARIOSBindingNavigatorSaveItem_Click(object sender, EventArgs e)
        {
            this.Validate();
            this.uSUARIOSBindingSource.EndEdit();
            this.uSUARIOSTableAdapter.Update(this.dataSet1.USUARIOS);

        }

        private void button3_Click(object sender, EventArgs e)
        {
            uSUARIOSTableAdapter.Fill(dataSet1.USUARIOS);
        }

        private void desliga_Click(object sender, EventArgs e)
        {
         //   bindingSource1.DataSource = null;

        }

        private void button5_Click(object sender, EventArgs e)
        {
            bindingSource1.DataSource = uSUARIOSBindingSource;
        }

        private void button4_Click(object sender, EventArgs e)
        {
            bindingSource1.Position = uSUARIOSBindingSource.Position;
        }

        private void button6_Click(object sender, EventArgs e)
        {
            uSUARIOSBindingSource.AddNew();
        }

        private void button7_Click(object sender, EventArgs e)
        {
            bindingSource1.AddNew();
        }
    }
}

