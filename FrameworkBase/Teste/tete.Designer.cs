namespace Teste
{
    partial class tete
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.Windows.Forms.Label cON_EMPLabel;
            System.Windows.Forms.Label cON_EMPLabel1;
            System.Windows.Forms.Label cON_CIDLabel;
            System.Windows.Forms.Label cON_CIDLabel1;
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(tete));
            CompontesBasicos.Classes_de_interface.PaginaTabInfo paginaTabInfo1 = new CompontesBasicos.Classes_de_interface.PaginaTabInfo();
            this.xtraTabPage1 = new DevExpress.XtraTab.XtraTabPage();
            this.cON_CIDLookUpEdit = new DevExpress.XtraEditors.LookUpEdit();
            this.cONDOMINIOSBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.dataSet1 = new Teste.DataSet1();
            this.dataTable1BindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.cON_CIDSpinEdit = new DevExpress.XtraEditors.SpinEdit();
            this.button1 = new System.Windows.Forms.Button();
            this.cON_EMPLookUpEdit = new DevExpress.XtraEditors.LookUpEdit();
            this.eMPRESASBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.cON_EMPSpinEdit = new DevExpress.XtraEditors.SpinEdit();
            this.cONDOMINIOSTableAdapter = new Teste.DataSet1TableAdapters.CONDOMINIOSTableAdapter();
            this.eMPRESASTableAdapter = new Teste.DataSet1TableAdapters.EMPRESASTableAdapter();
            this.dataTable1TableAdapter = new Teste.DataSet1TableAdapters.DataTable1TableAdapter();
            cON_EMPLabel = new System.Windows.Forms.Label();
            cON_EMPLabel1 = new System.Windows.Forms.Label();
            cON_CIDLabel = new System.Windows.Forms.Label();
            cON_CIDLabel1 = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.GroupControl_F)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TabControl_F)).BeginInit();
            this.TabControl_F.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.BarManager_F)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.BarAndDockingController_F)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.BindingSource_F)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.StyleController_F)).BeginInit();
            this.xtraTabPage1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.cONDOMINIOSBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataTable1BindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.eMPRESASBindingSource)).BeginInit();
            this.SuspendLayout();
            // 
            // GroupControl_F
            // 
            this.GroupControl_F.LookAndFeel.SkinName = "Lilian";
            this.GroupControl_F.Size = new System.Drawing.Size(977, 64);
            this.ToolTipController_F.SetSuperTip(this.GroupControl_F, null);
            // 
            // TabControl_F
            // 
            this.TabControl_F.SelectedTabPage = this.xtraTabPage1;
            this.TabControl_F.Size = new System.Drawing.Size(977, 402);
            this.TabControl_F.TabPages.AddRange(new DevExpress.XtraTab.XtraTabPage[] {
            this.xtraTabPage1});
            // 
            // barStatus_F
            // 
            this.barStatus_F.OptionsBar.AllowQuickCustomization = false;
            this.barStatus_F.OptionsBar.DisableClose = true;
            this.barStatus_F.OptionsBar.DisableCustomization = true;
            this.barStatus_F.OptionsBar.DrawDragBorder = false;
            this.barStatus_F.OptionsBar.DrawSizeGrip = true;
            this.barStatus_F.OptionsBar.UseWholeRow = true;
            // 
            // barComponente_F
            // 
            this.barComponente_F.OptionsBar.AllowQuickCustomization = false;
            this.barComponente_F.OptionsBar.DisableClose = true;
            this.barComponente_F.OptionsBar.DisableCustomization = true;
            this.barComponente_F.OptionsBar.DrawDragBorder = false;
            this.barComponente_F.OptionsBar.UseWholeRow = true;
            // 
            // BarAndDockingController_F
            // 
            this.BarAndDockingController_F.PropertiesBar.AllowLinkLighting = false;
            // 
            // ToolTipController_F
            // 
            this.ToolTipController_F.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(222)))), ((int)(((byte)(237)))), ((int)(((byte)(250)))));
            this.ToolTipController_F.Appearance.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(20)))), ((int)(((byte)(69)))), ((int)(((byte)(141)))));
            this.ToolTipController_F.Appearance.Options.UseBackColor = true;
            this.ToolTipController_F.Appearance.Options.UseFont = true;
            this.ToolTipController_F.Appearance.Options.UseForeColor = true;
            this.ToolTipController_F.Appearance.Options.UseTextOptions = true;
            this.ToolTipController_F.Appearance.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            // 
            // StyleController_F
            // 
            this.StyleController_F.AppearanceDisabled.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.StyleController_F.AppearanceDisabled.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.StyleController_F.AppearanceDisabled.Options.UseBackColor = true;
            this.StyleController_F.AppearanceDisabled.Options.UseForeColor = true;
            this.StyleController_F.AppearanceFocused.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(253)))), ((int)(((byte)(253)))), ((int)(((byte)(181)))));
            this.StyleController_F.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
            this.StyleController_F.AppearanceFocused.Options.UseBackColor = true;
            this.StyleController_F.AppearanceFocused.Options.UseFont = true;
            // 
            // DefaultLookAndFeel_F
            // 
            this.DefaultLookAndFeel_F.LookAndFeel.SkinName = "The Asphalt World";
            // 
            // cON_EMPLabel
            // 
            cON_EMPLabel.AutoSize = true;
            cON_EMPLabel.Location = new System.Drawing.Point(67, 27);
            cON_EMPLabel.Name = "cON_EMPLabel";
            cON_EMPLabel.Size = new System.Drawing.Size(56, 13);
            this.ToolTipController_F.SetSuperTip(cON_EMPLabel, null);
            cON_EMPLabel.TabIndex = 0;
            cON_EMPLabel.Text = "CON EMP:";
            // 
            // cON_EMPLabel1
            // 
            cON_EMPLabel1.AutoSize = true;
            cON_EMPLabel1.Location = new System.Drawing.Point(67, 76);
            cON_EMPLabel1.Name = "cON_EMPLabel1";
            cON_EMPLabel1.Size = new System.Drawing.Size(56, 13);
            this.ToolTipController_F.SetSuperTip(cON_EMPLabel1, null);
            cON_EMPLabel1.TabIndex = 2;
            cON_EMPLabel1.Text = "CON EMP:";
            // 
            // cON_CIDLabel
            // 
            cON_CIDLabel.AutoSize = true;
            cON_CIDLabel.Location = new System.Drawing.Point(316, 47);
            cON_CIDLabel.Name = "cON_CIDLabel";
            cON_CIDLabel.Size = new System.Drawing.Size(54, 13);
            this.ToolTipController_F.SetSuperTip(cON_CIDLabel, null);
            cON_CIDLabel.TabIndex = 5;
            cON_CIDLabel.Text = "CON CID:";
            // 
            // cON_CIDLabel1
            // 
            cON_CIDLabel1.AutoSize = true;
            cON_CIDLabel1.Location = new System.Drawing.Point(332, 105);
            cON_CIDLabel1.Name = "cON_CIDLabel1";
            cON_CIDLabel1.Size = new System.Drawing.Size(54, 13);
            this.ToolTipController_F.SetSuperTip(cON_CIDLabel1, null);
            cON_CIDLabel1.TabIndex = 7;
            cON_CIDLabel1.Text = "CON CID:";
            // 
            // xtraTabPage1
            // 
            this.xtraTabPage1.Controls.Add(cON_CIDLabel1);
            this.xtraTabPage1.Controls.Add(this.cON_CIDLookUpEdit);
            this.xtraTabPage1.Controls.Add(cON_CIDLabel);
            this.xtraTabPage1.Controls.Add(this.cON_CIDSpinEdit);
            this.xtraTabPage1.Controls.Add(this.button1);
            this.xtraTabPage1.Controls.Add(cON_EMPLabel1);
            this.xtraTabPage1.Controls.Add(this.cON_EMPLookUpEdit);
            this.xtraTabPage1.Controls.Add(cON_EMPLabel);
            this.xtraTabPage1.Controls.Add(this.cON_EMPSpinEdit);
            this.xtraTabPage1.Name = "xtraTabPage1";
            this.xtraTabPage1.Size = new System.Drawing.Size(968, 372);
            this.xtraTabPage1.Text = "xtraTabPage1";
            // 
            // cON_CIDLookUpEdit
            // 
            this.cON_CIDLookUpEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.cONDOMINIOSBindingSource, "CON_CID", true));
            this.cON_CIDLookUpEdit.Location = new System.Drawing.Point(392, 102);
            this.cON_CIDLookUpEdit.Name = "cON_CIDLookUpEdit";
            this.cON_CIDLookUpEdit.Size = new System.Drawing.Size(100, 20);
            this.cON_CIDLookUpEdit.TabIndex = 8;
            // 
            // cONDOMINIOSBindingSource
            // 
            this.cONDOMINIOSBindingSource.DataMember = "CONDOMINIOS";
            this.cONDOMINIOSBindingSource.DataSource = this.dataSet1;
            // 
            // dataSet1
            // 
            this.dataSet1.DataSetName = "DataSet1";
            this.dataSet1.EnforceConstraints = false;
            this.dataSet1.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // dataTable1BindingSource
            // 
            this.dataTable1BindingSource.DataMember = "DataTable1";
            this.dataTable1BindingSource.DataSource = this.dataSet1;
            // 
            // cON_CIDSpinEdit
            // 
            this.cON_CIDSpinEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.cONDOMINIOSBindingSource, "CON_CID", true));
            this.cON_CIDSpinEdit.EditValue = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.cON_CIDSpinEdit.Location = new System.Drawing.Point(376, 44);
            this.cON_CIDSpinEdit.Name = "cON_CIDSpinEdit";
            this.cON_CIDSpinEdit.Size = new System.Drawing.Size(100, 20);
            this.cON_CIDSpinEdit.TabIndex = 6;
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(129, 140);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(75, 23);
            this.ToolTipController_F.SetSuperTip(this.button1, null);
            this.button1.TabIndex = 4;
            this.button1.Text = "button1";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // cON_EMPLookUpEdit
            // 
            this.cON_EMPLookUpEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.cONDOMINIOSBindingSource, "CON_EMP", true));
            this.cON_EMPLookUpEdit.Location = new System.Drawing.Point(129, 73);
            this.cON_EMPLookUpEdit.Name = "cON_EMPLookUpEdit";
            this.cON_EMPLookUpEdit.Size = new System.Drawing.Size(100, 20);
            this.cON_EMPLookUpEdit.TabIndex = 3;
            // 
            // eMPRESASBindingSource
            // 
            this.eMPRESASBindingSource.DataMember = "EMPRESAS";
            this.eMPRESASBindingSource.DataSource = this.dataSet1;
            // 
            // cON_EMPSpinEdit
            // 
            this.cON_EMPSpinEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.cONDOMINIOSBindingSource, "CON_EMP", true));
            this.cON_EMPSpinEdit.EditValue = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.cON_EMPSpinEdit.Location = new System.Drawing.Point(129, 24);
            this.cON_EMPSpinEdit.Name = "cON_EMPSpinEdit";
            this.cON_EMPSpinEdit.Size = new System.Drawing.Size(100, 20);
            this.cON_EMPSpinEdit.TabIndex = 1;
            // 
            // cONDOMINIOSTableAdapter
            // 
            this.cONDOMINIOSTableAdapter.ClearBeforeFill = true;
            // 
            // eMPRESASTableAdapter
            // 
            this.eMPRESASTableAdapter.ClearBeforeFill = true;
            // 
            // dataTable1TableAdapter
            // 
            this.dataTable1TableAdapter.ClearBeforeFill = true;
            // 
            // tete
            // 
            this.BindingSourcePrincipal = this.cONDOMINIOSBindingSource;
            this.Doc = System.Windows.Forms.DockStyle.Fill;
            this.ImagemG = ((System.Drawing.Image)(resources.GetObject("$this.ImagemG")));
            this.ImagemP = ((System.Drawing.Image)(resources.GetObject("$this.ImagemP")));
            this.LookAndFeel.UseDefaultLookAndFeel = false;
            this.Name = "tete";
            paginaTabInfo1.Name = "nome";
            paginaTabInfo1.PaginaTabType = typeof(Teste.umatab);
            this.PaginaTabInfoCollection.AddRange(new CompontesBasicos.Classes_de_interface.PaginaTabInfo[] {
            paginaTabInfo1});
            this.Size = new System.Drawing.Size(977, 519);
            this.ToolTipController_F.SetSuperTip(this, null);
            this.Load += new System.EventHandler(this.tete_Load);
            ((System.ComponentModel.ISupportInitialize)(this.GroupControl_F)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TabControl_F)).EndInit();
            this.TabControl_F.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.BarManager_F)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.BarAndDockingController_F)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.BindingSource_F)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.StyleController_F)).EndInit();
            this.xtraTabPage1.ResumeLayout(false);
            this.xtraTabPage1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.cONDOMINIOSBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataTable1BindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.eMPRESASBindingSource)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraTab.XtraTabPage xtraTabPage1;
        private DevExpress.XtraEditors.LookUpEdit cON_EMPLookUpEdit;
        private System.Windows.Forms.BindingSource cONDOMINIOSBindingSource;
        private DataSet1 dataSet1;
        private System.Windows.Forms.BindingSource eMPRESASBindingSource;
        private DevExpress.XtraEditors.SpinEdit cON_EMPSpinEdit;
        private Teste.DataSet1TableAdapters.CONDOMINIOSTableAdapter cONDOMINIOSTableAdapter;
        private Teste.DataSet1TableAdapters.EMPRESASTableAdapter eMPRESASTableAdapter;
        private System.Windows.Forms.Button button1;
        private DevExpress.XtraEditors.LookUpEdit cON_CIDLookUpEdit;
        private System.Windows.Forms.BindingSource dataTable1BindingSource;
        private DevExpress.XtraEditors.SpinEdit cON_CIDSpinEdit;
        private Teste.DataSet1TableAdapters.DataTable1TableAdapter dataTable1TableAdapter;
    }
}
