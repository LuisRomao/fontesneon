namespace Teste
{
    partial class TesteGridSimples
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(TesteGridSimples));
            this.dataSet1 = new Teste.DataSet1();
            this.colUSU = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colUSULogin = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colUSUSenha = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colUSUNome = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colUSUEndereco = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colUSUBairro = new DevExpress.XtraGrid.Columns.GridColumn();
            this.button1 = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.PopImprimir_F)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.BarManager_F)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.PopExportar_F)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.GridControl_F)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.GridView_F)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.BindingSource_F)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.StyleController_F)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet1)).BeginInit();
            this.SuspendLayout();
            // 
            // barNavegacao_F
            // 
            this.barNavegacao_F.OptionsBar.AllowQuickCustomization = false;
            this.barNavegacao_F.OptionsBar.DisableClose = true;
            this.barNavegacao_F.OptionsBar.DisableCustomization = true;
            this.barNavegacao_F.OptionsBar.DrawDragBorder = false;
            this.barNavegacao_F.OptionsBar.DrawSizeGrip = true;
            this.barNavegacao_F.OptionsBar.RotateWhenVertical = false;
            this.barNavegacao_F.OptionsBar.UseWholeRow = true;
            // 
            // BtnAlterar_F
            // 
            this.BtnAlterar_F.Enabled = false;
            // 
            // BtnExcluir_F
            // 
            this.BtnExcluir_F.Enabled = false;
            this.BtnExcluir_F.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.BtnExcluir_F_ItemClick);
            // 
            // BtnImprimir_F
            // 
            this.BtnImprimir_F.Enabled = false;
            // 
            // BtnExportar_F
            // 
            this.BtnExportar_F.Enabled = false;
            // 
            // barDataBase_F
            // 
            this.barDataBase_F.OptionsBar.AllowQuickCustomization = false;
            this.barDataBase_F.OptionsBar.DisableClose = true;
            this.barDataBase_F.OptionsBar.DisableCustomization = true;
            this.barDataBase_F.OptionsBar.DrawDragBorder = false;
            this.barDataBase_F.OptionsBar.UseWholeRow = true;
            // 
            // GridControl_F
            // 
            // 
            // 
            // 
            this.GridControl_F.EmbeddedNavigator.Buttons.Append.Visible = false;
            this.GridControl_F.EmbeddedNavigator.Buttons.CancelEdit.Visible = false;
            this.GridControl_F.EmbeddedNavigator.Buttons.Edit.Visible = false;
            this.GridControl_F.EmbeddedNavigator.Buttons.EndEdit.Visible = false;
            this.GridControl_F.EmbeddedNavigator.Buttons.Remove.Visible = false;
            this.GridControl_F.EmbeddedNavigator.Name = "";
            this.GridControl_F.Size = new System.Drawing.Size(976, 132);
            this.GridControl_F.Load += new System.EventHandler(this.GridControl_F_Load);
            // 
            // GridView_F
            // 
            this.GridView_F.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colUSU,
            this.colUSULogin,
            this.colUSUSenha,
            this.colUSUNome,
            this.colUSUEndereco,
            this.colUSUBairro});
            this.GridView_F.OptionsBehavior.AllowIncrementalSearch = true;
            this.GridView_F.OptionsBehavior.Editable = false;
            this.GridView_F.OptionsLayout.Columns.StoreAllOptions = true;
            this.GridView_F.OptionsLayout.Columns.StoreAppearance = true;
            this.GridView_F.OptionsLayout.StoreAllOptions = true;
            this.GridView_F.OptionsLayout.StoreAppearance = true;
            this.GridView_F.OptionsNavigation.EnterMoveNextColumn = true;
            this.GridView_F.OptionsPrint.EnableAppearanceEvenRow = true;
            this.GridView_F.OptionsPrint.EnableAppearanceOddRow = true;
            this.GridView_F.OptionsPrint.ExpandAllDetails = true;
            this.GridView_F.OptionsSelection.InvertSelection = true;
            // 
            // BindingSource_F
            // 
            this.BindingSource_F.DataMember = "USUARIOS";
            this.BindingSource_F.DataSource = this.dataSet1;
            // 
            // ToolTipController_F
            // 
            this.ToolTipController_F.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(222)))), ((int)(((byte)(237)))), ((int)(((byte)(250)))));
            this.ToolTipController_F.Appearance.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(20)))), ((int)(((byte)(69)))), ((int)(((byte)(141)))));
            this.ToolTipController_F.Appearance.Options.UseBackColor = true;
            this.ToolTipController_F.Appearance.Options.UseFont = true;
            this.ToolTipController_F.Appearance.Options.UseForeColor = true;
            this.ToolTipController_F.Appearance.Options.UseTextOptions = true;
            this.ToolTipController_F.Appearance.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            // 
            // StyleController_F
            // 
            this.StyleController_F.AppearanceFocused.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.StyleController_F.AppearanceFocused.Options.UseBackColor = true;
            // 
            // DefaultLookAndFeel_F
            // 
            this.DefaultLookAndFeel_F.LookAndFeel.SkinName = "The Asphalt World";
            // 
            // dataSet1
            // 
            this.dataSet1.DataSetName = "DataSet1";
            this.dataSet1.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // colUSU
            // 
            this.colUSU.Caption = "USU";
            this.colUSU.FieldName = "USU";
            this.colUSU.Name = "colUSU";
            this.colUSU.OptionsColumn.ReadOnly = true;
            this.colUSU.Visible = true;
            this.colUSU.VisibleIndex = 0;
            // 
            // colUSULogin
            // 
            this.colUSULogin.Caption = "USULogin";
            this.colUSULogin.FieldName = "USULogin";
            this.colUSULogin.Name = "colUSULogin";
            this.colUSULogin.Visible = true;
            this.colUSULogin.VisibleIndex = 1;
            // 
            // colUSUSenha
            // 
            this.colUSUSenha.Caption = "USUSenha";
            this.colUSUSenha.FieldName = "USUSenha";
            this.colUSUSenha.Name = "colUSUSenha";
            this.colUSUSenha.Visible = true;
            this.colUSUSenha.VisibleIndex = 2;
            // 
            // colUSUNome
            // 
            this.colUSUNome.Caption = "USUNome";
            this.colUSUNome.FieldName = "USUNome";
            this.colUSUNome.Name = "colUSUNome";
            this.colUSUNome.Visible = true;
            this.colUSUNome.VisibleIndex = 3;
            // 
            // colUSUEndereco
            // 
            this.colUSUEndereco.Caption = "USUEndereco";
            this.colUSUEndereco.FieldName = "USUEndereco";
            this.colUSUEndereco.Name = "colUSUEndereco";
            this.colUSUEndereco.Visible = true;
            this.colUSUEndereco.VisibleIndex = 4;
            // 
            // colUSUBairro
            // 
            this.colUSUBairro.Caption = "USUBairro";
            this.colUSUBairro.FieldName = "USUBairro";
            this.colUSUBairro.Name = "colUSUBairro";
            this.colUSUBairro.Visible = true;
            this.colUSUBairro.VisibleIndex = 5;
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(316, 200);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(168, 78);
            this.button1.TabIndex = 5;
            this.button1.Text = "button1";
            this.button1.UseVisualStyleBackColor = true;
            // 
            // TesteGridSimples
            // 
            this.Controls.Add(this.button1);
            this.Cursor = System.Windows.Forms.Cursors.AppStarting;
            this.ImagemG = ((System.Drawing.Image)(resources.GetObject("$this.ImagemG")));
            this.ImagemP = ((System.Drawing.Image)(resources.GetObject("$this.ImagemP")));
            this.LookAndFeel.UseDefaultLookAndFeel = false;
            this.Name = "TesteGridSimples";
            this.Size = new System.Drawing.Size(976, 187);
            this.Titulo = "Luis";
            this.Load += new System.EventHandler(this.TesteGridSimples_Load);
            this.cargaPrincipal += new System.EventHandler(this.TesteGridSimples_cargaInicial);
            this.Controls.SetChildIndex(this.button1, 0);
            this.Controls.SetChildIndex(this.GridControl_F, 0);
            ((System.ComponentModel.ISupportInitialize)(this.PopImprimir_F)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.BarManager_F)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.PopExportar_F)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.GridControl_F)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.GridView_F)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.BindingSource_F)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.StyleController_F)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet1)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraGrid.Columns.GridColumn colUSU;
        private DevExpress.XtraGrid.Columns.GridColumn colUSULogin;
        private DevExpress.XtraGrid.Columns.GridColumn colUSUSenha;
        private DevExpress.XtraGrid.Columns.GridColumn colUSUNome;
        private DevExpress.XtraGrid.Columns.GridColumn colUSUEndereco;
        private DevExpress.XtraGrid.Columns.GridColumn colUSUBairro;
        private DataSet1 dataSet1;
        private Teste.DataSet1TableAdapters.USUARIOSTableAdapter uSUARIOSTableAdapter;
        private System.Windows.Forms.Button button1;
    }
}
