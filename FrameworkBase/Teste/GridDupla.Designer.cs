namespace Teste
{
    partial class GridDupla
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            DevExpress.XtraGrid.GridLevelNode gridLevelNode1 = new DevExpress.XtraGrid.GridLevelNode();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(GridDupla));
            this.gridView2 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colAPT_BLO = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colAPTProprietario_PES = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colAPTInquilino_PES = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colAPTImobiliaria_FRN = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colAPTFracaoIdeal = new DevExpress.XtraGrid.Columns.GridColumn();
            this.bLOCOSGridControl = new DevExpress.XtraGrid.GridControl();
            this.bLOCOSBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.dataSet1 = new Teste.DataSet1();
            this.gridView1 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colBLO = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colBLO_CON = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colBLOCodigo = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colBLONome = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colBLOTotalApartamento = new DevExpress.XtraGrid.Columns.GridColumn();
            this.bLOCOSTableAdapter = new Teste.DataSet1TableAdapters.BLOCOSTableAdapter();
            this.apartamentosTableAdapter1 = new Teste.DataSet1TableAdapters.APARTAMENTOSTableAdapter();
            this.button1 = new System.Windows.Forms.Button();
            this.barButtonItem1 = new DevExpress.XtraBars.BarButtonItem();
            ((System.ComponentModel.ISupportInitialize)(this.BarManager_F)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.BarAndDockingController_F)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.BindingSource_F)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.StyleController_F)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.bLOCOSGridControl)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.bLOCOSBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).BeginInit();
            this.SuspendLayout();
            // 
            // barStatus_F
            // 
            this.barStatus_F.OptionsBar.AllowQuickCustomization = false;
            this.barStatus_F.OptionsBar.DisableClose = true;
            this.barStatus_F.OptionsBar.DisableCustomization = true;
            this.barStatus_F.OptionsBar.DrawDragBorder = false;
            this.barStatus_F.OptionsBar.DrawSizeGrip = true;
            this.barStatus_F.OptionsBar.UseWholeRow = true;
            // 
            // barComponente_F
            // 
            this.barComponente_F.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(this.barButtonItem1)});
            this.barComponente_F.OptionsBar.AllowQuickCustomization = false;
            this.barComponente_F.OptionsBar.DisableClose = true;
            this.barComponente_F.OptionsBar.DisableCustomization = true;
            this.barComponente_F.OptionsBar.DrawDragBorder = false;
            this.barComponente_F.OptionsBar.UseWholeRow = true;
            // 
            // btnFechar_F
            // 
            this.btnFechar_F.Visibility = DevExpress.XtraBars.BarItemVisibility.Never;
            this.btnFechar_F.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.btnFechar_F_ItemClick_1);
            // 
            // BarManager_F
            // 
            this.BarManager_F.Items.AddRange(new DevExpress.XtraBars.BarItem[] {
            this.barButtonItem1});
            this.BarManager_F.MaxItemId = 10;
            // 
            // BarAndDockingController_F
            // 
            this.BarAndDockingController_F.PropertiesBar.AllowLinkLighting = false;
            // 
            // ToolTipController_F
            // 
            this.ToolTipController_F.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(222)))), ((int)(((byte)(237)))), ((int)(((byte)(250)))));
            this.ToolTipController_F.Appearance.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(20)))), ((int)(((byte)(69)))), ((int)(((byte)(141)))));
            this.ToolTipController_F.Appearance.Options.UseBackColor = true;
            this.ToolTipController_F.Appearance.Options.UseFont = true;
            this.ToolTipController_F.Appearance.Options.UseForeColor = true;
            this.ToolTipController_F.Appearance.Options.UseTextOptions = true;
            this.ToolTipController_F.Appearance.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            // 
            // StyleController_F
            // 
            this.StyleController_F.AppearanceDisabled.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.StyleController_F.AppearanceDisabled.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.StyleController_F.AppearanceDisabled.Options.UseBackColor = true;
            this.StyleController_F.AppearanceDisabled.Options.UseForeColor = true;
            this.StyleController_F.AppearanceFocused.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(253)))), ((int)(((byte)(253)))), ((int)(((byte)(181)))));
            this.StyleController_F.AppearanceFocused.Options.UseBackColor = true;
            // 
            // gridView2
            // 
            this.gridView2.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colAPT_BLO,
            this.colAPTProprietario_PES,
            this.colAPTInquilino_PES,
            this.colAPTImobiliaria_FRN,
            this.colAPTFracaoIdeal});
            this.gridView2.GridControl = this.bLOCOSGridControl;
            this.gridView2.Name = "gridView2";
            this.gridView2.MasterRowExpanded += new DevExpress.XtraGrid.Views.Grid.CustomMasterRowEventHandler(this.gridView2_MasterRowExpanded);
            // 
            // colAPT_BLO
            // 
            this.colAPT_BLO.Caption = "APT_BLO";
            this.colAPT_BLO.FieldName = "APT_BLO";
            this.colAPT_BLO.Name = "colAPT_BLO";
            this.colAPT_BLO.Visible = true;
            this.colAPT_BLO.VisibleIndex = 0;
            // 
            // colAPTProprietario_PES
            // 
            this.colAPTProprietario_PES.Caption = "APTProprietario_PES";
            this.colAPTProprietario_PES.FieldName = "APTProprietario_PES";
            this.colAPTProprietario_PES.Name = "colAPTProprietario_PES";
            this.colAPTProprietario_PES.Visible = true;
            this.colAPTProprietario_PES.VisibleIndex = 1;
            // 
            // colAPTInquilino_PES
            // 
            this.colAPTInquilino_PES.Caption = "APTInquilino_PES";
            this.colAPTInquilino_PES.FieldName = "APTInquilino_PES";
            this.colAPTInquilino_PES.Name = "colAPTInquilino_PES";
            this.colAPTInquilino_PES.Visible = true;
            this.colAPTInquilino_PES.VisibleIndex = 2;
            // 
            // colAPTImobiliaria_FRN
            // 
            this.colAPTImobiliaria_FRN.Caption = "APTImobiliaria_FRN";
            this.colAPTImobiliaria_FRN.FieldName = "APTImobiliaria_FRN";
            this.colAPTImobiliaria_FRN.Name = "colAPTImobiliaria_FRN";
            this.colAPTImobiliaria_FRN.Visible = true;
            this.colAPTImobiliaria_FRN.VisibleIndex = 3;
            // 
            // colAPTFracaoIdeal
            // 
            this.colAPTFracaoIdeal.Caption = "APTFracaoIdeal";
            this.colAPTFracaoIdeal.FieldName = "APTFracaoIdeal";
            this.colAPTFracaoIdeal.Name = "colAPTFracaoIdeal";
            this.colAPTFracaoIdeal.Visible = true;
            this.colAPTFracaoIdeal.VisibleIndex = 4;
            // 
            // bLOCOSGridControl
            // 
            this.bLOCOSGridControl.DataSource = this.bLOCOSBindingSource;
            // 
            // 
            // 
            this.bLOCOSGridControl.EmbeddedNavigator.Name = "";
            gridLevelNode1.LevelTemplate = this.gridView2;
            gridLevelNode1.RelationName = "FK_APARTAMENTOS_BLOCOS";
            this.bLOCOSGridControl.LevelTree.Nodes.AddRange(new DevExpress.XtraGrid.GridLevelNode[] {
            gridLevelNode1});
            this.bLOCOSGridControl.Location = new System.Drawing.Point(17, 58);
            this.bLOCOSGridControl.MainView = this.gridView1;
            this.bLOCOSGridControl.Name = "bLOCOSGridControl";
            this.bLOCOSGridControl.Size = new System.Drawing.Size(605, 220);
            this.bLOCOSGridControl.TabIndex = 4;
            this.bLOCOSGridControl.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridView1,
            this.gridView2});
            // 
            // bLOCOSBindingSource
            // 
            this.bLOCOSBindingSource.DataMember = "BLOCOS";
            this.bLOCOSBindingSource.DataSource = this.dataSet1;
            // 
            // dataSet1
            // 
            this.dataSet1.DataSetName = "DataSet1";
            this.dataSet1.EnforceConstraints = false;
            this.dataSet1.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // gridView1
            // 
            this.gridView1.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colBLO,
            this.colBLO_CON,
            this.colBLOCodigo,
            this.colBLONome,
            this.colBLOTotalApartamento});
            this.gridView1.GridControl = this.bLOCOSGridControl;
            this.gridView1.Name = "gridView1";
            this.gridView1.MasterRowExpanded += new DevExpress.XtraGrid.Views.Grid.CustomMasterRowEventHandler(this.gridView1_MasterRowExpanded);
            // 
            // colBLO
            // 
            this.colBLO.Caption = "BLO";
            this.colBLO.FieldName = "BLO";
            this.colBLO.Name = "colBLO";
            this.colBLO.OptionsColumn.ReadOnly = true;
            this.colBLO.Visible = true;
            this.colBLO.VisibleIndex = 0;
            // 
            // colBLO_CON
            // 
            this.colBLO_CON.Caption = "BLO_CON";
            this.colBLO_CON.FieldName = "BLO_CON";
            this.colBLO_CON.Name = "colBLO_CON";
            this.colBLO_CON.Visible = true;
            this.colBLO_CON.VisibleIndex = 1;
            // 
            // colBLOCodigo
            // 
            this.colBLOCodigo.Caption = "BLOCodigo";
            this.colBLOCodigo.FieldName = "BLOCodigo";
            this.colBLOCodigo.Name = "colBLOCodigo";
            this.colBLOCodigo.Visible = true;
            this.colBLOCodigo.VisibleIndex = 2;
            // 
            // colBLONome
            // 
            this.colBLONome.Caption = "BLONome";
            this.colBLONome.FieldName = "BLONome";
            this.colBLONome.Name = "colBLONome";
            this.colBLONome.Visible = true;
            this.colBLONome.VisibleIndex = 3;
            // 
            // colBLOTotalApartamento
            // 
            this.colBLOTotalApartamento.Caption = "BLOTotalApartamento";
            this.colBLOTotalApartamento.FieldName = "BLOTotalApartamento";
            this.colBLOTotalApartamento.Name = "colBLOTotalApartamento";
            this.colBLOTotalApartamento.Visible = true;
            this.colBLOTotalApartamento.VisibleIndex = 4;
            // 
            // bLOCOSTableAdapter
            // 
            this.bLOCOSTableAdapter.ClearBeforeFill = true;
            // 
            // apartamentosTableAdapter1
            // 
            this.apartamentosTableAdapter1.ClearBeforeFill = true;
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(353, 305);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(75, 23);
            this.button1.TabIndex = 5;
            this.button1.Text = "button1";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            this.button1.MouseMove += new System.Windows.Forms.MouseEventHandler(this.button1_MouseMove);
            // 
            // barButtonItem1
            // 
            this.barButtonItem1.Caption = "barButtonItem1";
            this.barButtonItem1.Id = 9;
            this.barButtonItem1.Name = "barButtonItem1";
            this.barButtonItem1.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.barButtonItem1_ItemClick);
            // 
            // GridDupla
            // 
            this.Autofill = false;
            this.BindingSourcePrincipal = this.bLOCOSBindingSource;
            this.Controls.Add(this.button1);
            this.Controls.Add(this.bLOCOSGridControl);
            this.Doc = System.Windows.Forms.DockStyle.Fill;
            this.ImagemG = ((System.Drawing.Image)(resources.GetObject("$this.ImagemG")));
            this.ImagemP = ((System.Drawing.Image)(resources.GetObject("$this.ImagemP")));
            this.LookAndFeel.UseDefaultLookAndFeel = false;
            this.Name = "GridDupla";
            this.Size = new System.Drawing.Size(976, 483);
            this.ControlAdded += new System.Windows.Forms.ControlEventHandler(this.GridDupla_ControlAdded);
            this.cargaFinal += new System.EventHandler(this.GridDupla_cargaFinal);
            this.Controls.SetChildIndex(this.bLOCOSGridControl, 0);
            this.Controls.SetChildIndex(this.button1, 0);
            ((System.ComponentModel.ISupportInitialize)(this.BarManager_F)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.BarAndDockingController_F)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.BindingSource_F)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.StyleController_F)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.bLOCOSGridControl)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.bLOCOSBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private DataSet1 dataSet1;
        private System.Windows.Forms.BindingSource bLOCOSBindingSource;
        private Teste.DataSet1TableAdapters.BLOCOSTableAdapter bLOCOSTableAdapter;
        private DevExpress.XtraGrid.GridControl bLOCOSGridControl;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView1;
        private DevExpress.XtraGrid.Columns.GridColumn colBLO;
        private DevExpress.XtraGrid.Columns.GridColumn colBLO_CON;
        private DevExpress.XtraGrid.Columns.GridColumn colBLOCodigo;
        private DevExpress.XtraGrid.Columns.GridColumn colBLONome;
        private DevExpress.XtraGrid.Columns.GridColumn colBLOTotalApartamento;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView2;
        private DevExpress.XtraGrid.Columns.GridColumn colAPT_BLO;
        private DevExpress.XtraGrid.Columns.GridColumn colAPTProprietario_PES;
        private DevExpress.XtraGrid.Columns.GridColumn colAPTInquilino_PES;
        private DevExpress.XtraGrid.Columns.GridColumn colAPTImobiliaria_FRN;
        private DevExpress.XtraGrid.Columns.GridColumn colAPTFracaoIdeal;
        private Teste.DataSet1TableAdapters.APARTAMENTOSTableAdapter apartamentosTableAdapter1;
        private System.Windows.Forms.Button button1;
        private DevExpress.XtraBars.BarButtonItem barButtonItem1;
    }
}
