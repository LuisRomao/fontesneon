using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using System.Reflection;

namespace Teste
{
    public partial class GridDupla : CompontesBasicos.ComponenteCamposBase
    {
        public GridDupla()
        {
            InitializeComponent();
        }

        private void GridDupla_cargaFinal(object sender, EventArgs e)
        {
            apartamentosTableAdapter1.Fill(dataSet1.APARTAMENTOS);
        }

        private void gridView2_MasterRowExpanded(object sender, DevExpress.XtraGrid.Views.Grid.CustomMasterRowEventArgs e)
        {
            MessageBox.Show("Agora");
        }

        private void gridView1_MasterRowExpanded(object sender, DevExpress.XtraGrid.Views.Grid.CustomMasterRowEventArgs e)
        {
   
   //         CompontesBasicos.FormBase pai = (CompontesBasicos.FormBase)this.Parent.Parent.Parent;
     //       pai.InstalaEventos(this, true, true, CamposObrigatorios);
            
        }

        private void GridDupla_ControlAdded(object sender, ControlEventArgs e)
        {
        
            
        }

        private void button1_Click(object sender, EventArgs e)
        {
            CompontesBasicos.FormBase pai = (CompontesBasicos.FormBase)this.Parent.Parent.Parent;
            pai.InstalaEventos(this, true, true, CamposObrigatorios);
        }

        private void button1_MouseMove(object sender, MouseEventArgs e)
        {
            button1.Enabled = true;
        }

        private void btnFechar_F_ItemClick_1(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            button1.Enabled = true;
        }

        private void barButtonItem1_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
   //         MessageBox.Show("x");
   //         CompontesBasicos.FormBase pai = (CompontesBasicos.FormBase)this.Parent.Parent.Parent;
   //         pai.InstalaEventos(bLOCOSGridControl, true, true, CamposObrigatorios);
            BindingFlags bf =  BindingFlags.Instance | BindingFlags.NonPublic | BindingFlags.Public;

            foreach (FieldInfo fi in (this.GetType()).GetFields(bf))
            {
                if (fi.FieldType == typeof(DevExpress.XtraGrid.Views.Grid.GridView))
                {
                    DevExpress.XtraGrid.Views.Grid.GridView GV = (DevExpress.XtraGrid.Views.Grid.GridView)fi.GetValue(this);
                    GV.OptionsBehavior.Editable = false;
                }
            };
        }
    }
}

