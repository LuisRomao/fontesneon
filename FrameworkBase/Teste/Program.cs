using System;
using System.Collections.Generic;
using System.Windows.Forms;
using CompontesBasicos;

namespace Teste
{
    static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main()
        {
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            // call the module registration before run the main form
            //ModulesRegistration.Register();
            //CategoriesInfo.Add("Cagegory 1", 0);
            //ModuleInfoCollection.Add("Controle1", typeof(Controle1), CategoriesInfo.Instance["Cagegory 1"], 0);
            //ModuleInfoCollection.Add("Controle2", typeof(Controle2), CategoriesInfo.Instance["Cagegory 1"], 1);
            //ModuleInfoCollection.Add("File Explorer", typeof(FileFolderModule), CategoriesInfo.Instance["Cagegory 1"], 0);
            Application.Run(new Principal());
        }
    }
}