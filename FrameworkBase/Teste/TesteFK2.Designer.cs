namespace Teste
{
    partial class TesteFK2
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.dataSet1 = new Teste.DataSet1();
            this.cONDOMINIOSTableAdapter = new Teste.DataSet1TableAdapters.CONDOMINIOSTableAdapter();
            this.cONDOMINIOSGridControl = new DevExpress.XtraGrid.GridControl();
            this.gridView1 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colCON = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colCONCodigo = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colCONNome = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colCONEndereco = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colCONBairro = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colCONCidade = new DevExpress.XtraGrid.Columns.GridColumn();
            this.bLOCOSBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.fKCONDOMINIOSBLOCOSBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.bLOCOSTableAdapter = new Teste.DataSet1TableAdapters.BLOCOSTableAdapter();
            this.bLOCOSGridControl = new DevExpress.XtraGrid.GridControl();
            this.gridView2 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colBLO = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colBLO_CON = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colBLOCodigo = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colBLONome = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colBLOTotalApartamento = new DevExpress.XtraGrid.Columns.GridColumn();
            this.fKCONDOMINIOSBLOCOSBindingSource1 = new System.Windows.Forms.BindingSource(this.components);
            ((System.ComponentModel.ISupportInitialize)(this.BindingSource_F)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.StyleController_F)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cONDOMINIOSGridControl)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.bLOCOSBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.fKCONDOMINIOSBLOCOSBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.bLOCOSGridControl)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.fKCONDOMINIOSBLOCOSBindingSource1)).BeginInit();
            this.SuspendLayout();
            // 
            // BindingSource_F
            // 
            this.BindingSource_F.DataMember = "CONDOMINIOS";
            this.BindingSource_F.DataSource = this.dataSet1;
            // 
            // ToolTipController_F
            // 
            this.ToolTipController_F.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.ToolTipController_F.Appearance.ForeColor = System.Drawing.Color.Black;
            this.ToolTipController_F.Appearance.Options.UseBackColor = true;
            this.ToolTipController_F.Appearance.Options.UseFont = true;
            this.ToolTipController_F.Appearance.Options.UseForeColor = true;
            this.ToolTipController_F.Appearance.Options.UseTextOptions = true;
            this.ToolTipController_F.Appearance.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            // 
            // StyleController_F
            // 
            this.StyleController_F.AppearanceFocused.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.StyleController_F.AppearanceFocused.Options.UseBackColor = true;
            // 
            // DefaultLookAndFeel_F
            // 
            this.DefaultLookAndFeel_F.LookAndFeel.SkinName = "The Asphalt World";
            // 
            // dataSet1
            // 
            this.dataSet1.DataSetName = "DataSet1";
            this.dataSet1.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // cONDOMINIOSTableAdapter
            // 
            this.cONDOMINIOSTableAdapter.ClearBeforeFill = true;
            // 
            // cONDOMINIOSGridControl
            // 
            this.cONDOMINIOSGridControl.DataSource = this.BindingSource_F;
            // 
            // 
            // 
            this.cONDOMINIOSGridControl.EmbeddedNavigator.Name = "";
            this.cONDOMINIOSGridControl.Location = new System.Drawing.Point(23, 81);
            this.cONDOMINIOSGridControl.MainView = this.gridView1;
            this.cONDOMINIOSGridControl.Name = "cONDOMINIOSGridControl";
            this.cONDOMINIOSGridControl.Size = new System.Drawing.Size(300, 220);
            this.cONDOMINIOSGridControl.TabIndex = 0;
            this.cONDOMINIOSGridControl.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridView1});
            // 
            // gridView1
            // 
            this.gridView1.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colCON,
            this.colCONCodigo,
            this.colCONNome,
            this.colCONEndereco,
            this.colCONBairro,
            this.colCONCidade});
            this.gridView1.GridControl = this.cONDOMINIOSGridControl;
            this.gridView1.Name = "gridView1";
            // 
            // colCON
            // 
            this.colCON.Caption = "CON";
            this.colCON.FieldName = "CON";
            this.colCON.Name = "colCON";
            this.colCON.OptionsColumn.ReadOnly = true;
            this.colCON.Visible = true;
            this.colCON.VisibleIndex = 0;
            // 
            // colCONCodigo
            // 
            this.colCONCodigo.Caption = "CONCodigo";
            this.colCONCodigo.FieldName = "CONCodigo";
            this.colCONCodigo.Name = "colCONCodigo";
            this.colCONCodigo.Visible = true;
            this.colCONCodigo.VisibleIndex = 1;
            // 
            // colCONNome
            // 
            this.colCONNome.Caption = "CONNome";
            this.colCONNome.FieldName = "CONNome";
            this.colCONNome.Name = "colCONNome";
            this.colCONNome.Visible = true;
            this.colCONNome.VisibleIndex = 2;
            // 
            // colCONEndereco
            // 
            this.colCONEndereco.Caption = "CONEndereco";
            this.colCONEndereco.FieldName = "CONEndereco";
            this.colCONEndereco.Name = "colCONEndereco";
            this.colCONEndereco.Visible = true;
            this.colCONEndereco.VisibleIndex = 3;
            // 
            // colCONBairro
            // 
            this.colCONBairro.Caption = "CONBairro";
            this.colCONBairro.FieldName = "CONBairro";
            this.colCONBairro.Name = "colCONBairro";
            this.colCONBairro.Visible = true;
            this.colCONBairro.VisibleIndex = 4;
            // 
            // colCONCidade
            // 
            this.colCONCidade.Caption = "CONCidade";
            this.colCONCidade.FieldName = "CONCidade";
            this.colCONCidade.Name = "colCONCidade";
            this.colCONCidade.Visible = true;
            this.colCONCidade.VisibleIndex = 5;
            // 
            // bLOCOSBindingSource
            // 
            this.bLOCOSBindingSource.DataSource = this.fKCONDOMINIOSBLOCOSBindingSource;
            // 
            // fKCONDOMINIOSBLOCOSBindingSource
            // 
            this.fKCONDOMINIOSBLOCOSBindingSource.DataSource = this.BindingSource_F;
            // 
            // bLOCOSTableAdapter
            // 
            this.bLOCOSTableAdapter.ClearBeforeFill = true;
            // 
            // bLOCOSGridControl
            // 
            this.bLOCOSGridControl.DataSource = this.fKCONDOMINIOSBLOCOSBindingSource1;
            // 
            // 
            // 
            this.bLOCOSGridControl.EmbeddedNavigator.Name = "";
            this.bLOCOSGridControl.Location = new System.Drawing.Point(381, 89);
            this.bLOCOSGridControl.MainView = this.gridView2;
            this.bLOCOSGridControl.Name = "bLOCOSGridControl";
            this.bLOCOSGridControl.Size = new System.Drawing.Size(422, 220);
            this.bLOCOSGridControl.TabIndex = 1;
            this.bLOCOSGridControl.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridView2});
            // 
            // gridView2
            // 
            this.gridView2.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colBLO,
            this.colBLO_CON,
            this.colBLOCodigo,
            this.colBLONome,
            this.colBLOTotalApartamento});
            this.gridView2.GridControl = this.bLOCOSGridControl;
            this.gridView2.Name = "gridView2";
            // 
            // colBLO
            // 
            this.colBLO.Caption = "BLO";
            this.colBLO.FieldName = "BLO";
            this.colBLO.Name = "colBLO";
            this.colBLO.OptionsColumn.ReadOnly = true;
            this.colBLO.Visible = true;
            this.colBLO.VisibleIndex = 0;
            // 
            // colBLO_CON
            // 
            this.colBLO_CON.Caption = "BLO_CON";
            this.colBLO_CON.FieldName = "BLO_CON";
            this.colBLO_CON.Name = "colBLO_CON";
            this.colBLO_CON.Visible = true;
            this.colBLO_CON.VisibleIndex = 1;
            // 
            // colBLOCodigo
            // 
            this.colBLOCodigo.Caption = "BLOCodigo";
            this.colBLOCodigo.FieldName = "BLOCodigo";
            this.colBLOCodigo.Name = "colBLOCodigo";
            this.colBLOCodigo.Visible = true;
            this.colBLOCodigo.VisibleIndex = 2;
            // 
            // colBLONome
            // 
            this.colBLONome.Caption = "BLONome";
            this.colBLONome.FieldName = "BLONome";
            this.colBLONome.Name = "colBLONome";
            this.colBLONome.Visible = true;
            this.colBLONome.VisibleIndex = 3;
            // 
            // colBLOTotalApartamento
            // 
            this.colBLOTotalApartamento.Caption = "BLOTotalApartamento";
            this.colBLOTotalApartamento.FieldName = "BLOTotalApartamento";
            this.colBLOTotalApartamento.Name = "colBLOTotalApartamento";
            this.colBLOTotalApartamento.Visible = true;
            this.colBLOTotalApartamento.VisibleIndex = 4;
            // 
            // fKCONDOMINIOSBLOCOSBindingSource1
            // 
            this.fKCONDOMINIOSBLOCOSBindingSource1.DataMember = "FK_CONDOMINIOS_BLOCOS";
            this.fKCONDOMINIOSBLOCOSBindingSource1.DataSource = this.fKCONDOMINIOSBLOCOSBindingSource;
            // 
            // TesteFK2
            // 
            this.Controls.Add(this.bLOCOSGridControl);
            this.Controls.Add(this.cONDOMINIOSGridControl);
            this.Cursor = System.Windows.Forms.Cursors.Default;
            this.LookAndFeel.UseDefaultLookAndFeel = false;
            this.Name = "TesteFK2";
            this.Size = new System.Drawing.Size(844, 385);
            ((System.ComponentModel.ISupportInitialize)(this.BindingSource_F)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.StyleController_F)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cONDOMINIOSGridControl)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.bLOCOSBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.fKCONDOMINIOSBLOCOSBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.bLOCOSGridControl)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.fKCONDOMINIOSBLOCOSBindingSource1)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private DataSet1 dataSet1;
        private Teste.DataSet1TableAdapters.CONDOMINIOSTableAdapter cONDOMINIOSTableAdapter;
        private DevExpress.XtraGrid.GridControl cONDOMINIOSGridControl;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView1;
        private DevExpress.XtraGrid.Columns.GridColumn colCON;
        private DevExpress.XtraGrid.Columns.GridColumn colCONCodigo;
        private DevExpress.XtraGrid.Columns.GridColumn colCONNome;
        private DevExpress.XtraGrid.Columns.GridColumn colCONEndereco;
        private DevExpress.XtraGrid.Columns.GridColumn colCONBairro;
        private DevExpress.XtraGrid.Columns.GridColumn colCONCidade;
        private System.Windows.Forms.BindingSource bLOCOSBindingSource;
        private Teste.DataSet1TableAdapters.BLOCOSTableAdapter bLOCOSTableAdapter;
        private DevExpress.XtraGrid.GridControl bLOCOSGridControl;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView2;
        private DevExpress.XtraGrid.Columns.GridColumn colBLO;
        private DevExpress.XtraGrid.Columns.GridColumn colBLO_CON;
        private DevExpress.XtraGrid.Columns.GridColumn colBLOCodigo;
        private DevExpress.XtraGrid.Columns.GridColumn colBLONome;
        private DevExpress.XtraGrid.Columns.GridColumn colBLOTotalApartamento;
        private System.Windows.Forms.BindingSource fKCONDOMINIOSBLOCOSBindingSource;
        private System.Windows.Forms.BindingSource fKCONDOMINIOSBLOCOSBindingSource1;
    }
}
