namespace Teste
{
    partial class CamposTab
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(CamposTab));
            CompontesBasicos.Classes_de_interface.PaginaTabInfo paginaTabInfo1 = new CompontesBasicos.Classes_de_interface.PaginaTabInfo();
            CompontesBasicos.Classes_de_interface.PaginaTabInfo paginaTabInfo2 = new CompontesBasicos.Classes_de_interface.PaginaTabInfo();
            this.xtraTabPage1 = new DevExpress.XtraTab.XtraTabPage();
            this.uSUARIOSGridControl = new DevExpress.XtraGrid.GridControl();
            this.uSUARIOSBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.dataSet1 = new Teste.DataSet1();
            this.gridView1 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colUSU = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colUSULogin = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colUSUSenha = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colUSUNome = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colUSUEndereco = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colUSUBairro = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colUSUCep = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colUSUFone1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colUSUFone2 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colUSUEmail = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colUSUDataInclusao = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colUSUInclusao_USU = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colUSUDataAtualizacao = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colUSUAtualizacao_USU = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colUSURamal = new DevExpress.XtraGrid.Columns.GridColumn();
            this.uSUARIOSTableAdapter = new Teste.DataSet1TableAdapters.USUARIOSTableAdapter();
            this.button1 = new System.Windows.Forms.Button();
            this.textEdit1 = new DevExpress.XtraEditors.TextEdit();
            ((System.ComponentModel.ISupportInitialize)(this.GroupControl_F)).BeginInit();
            this.GroupControl_F.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.TabControl_F)).BeginInit();
            this.TabControl_F.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.BarManager_F)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.BarAndDockingController_F)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.BindingSource_F)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.StyleController_F)).BeginInit();
            this.xtraTabPage1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uSUARIOSGridControl)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uSUARIOSBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit1.Properties)).BeginInit();
            this.SuspendLayout();
            // 
            // GroupControl_F
            // 
            this.GroupControl_F.Controls.Add(this.textEdit1);
            this.GroupControl_F.Controls.Add(this.button1);
            this.GroupControl_F.LookAndFeel.SkinName = "Lilian";
            // 
            // TabControl_F
            // 
            this.TabControl_F.SelectedTabPage = this.xtraTabPage1;
            this.TabControl_F.Size = new System.Drawing.Size(519, 308);
            this.TabControl_F.TabPages.AddRange(new DevExpress.XtraTab.XtraTabPage[] {
            this.xtraTabPage1});
            // 
            // barStatus_F
            // 
            this.barStatus_F.OptionsBar.AllowQuickCustomization = false;
            this.barStatus_F.OptionsBar.DisableClose = true;
            this.barStatus_F.OptionsBar.DisableCustomization = true;
            this.barStatus_F.OptionsBar.DrawDragBorder = false;
            this.barStatus_F.OptionsBar.DrawSizeGrip = true;
            this.barStatus_F.OptionsBar.UseWholeRow = true;
            // 
            // barComponente_F
            // 
            this.barComponente_F.OptionsBar.AllowQuickCustomization = false;
            this.barComponente_F.OptionsBar.DisableClose = true;
            this.barComponente_F.OptionsBar.DisableCustomization = true;
            this.barComponente_F.OptionsBar.DrawDragBorder = false;
            this.barComponente_F.OptionsBar.UseWholeRow = true;
            // 
            // BarAndDockingController_F
            // 
            this.BarAndDockingController_F.PropertiesBar.AllowLinkLighting = false;
            // 
            // ToolTipController_F
            // 
            this.ToolTipController_F.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(222)))), ((int)(((byte)(237)))), ((int)(((byte)(250)))));
            this.ToolTipController_F.Appearance.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(20)))), ((int)(((byte)(69)))), ((int)(((byte)(141)))));
            this.ToolTipController_F.Appearance.Options.UseBackColor = true;
            this.ToolTipController_F.Appearance.Options.UseFont = true;
            this.ToolTipController_F.Appearance.Options.UseForeColor = true;
            this.ToolTipController_F.Appearance.Options.UseTextOptions = true;
            this.ToolTipController_F.Appearance.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            // 
            // StyleController_F
            // 
            this.StyleController_F.AppearanceDisabled.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.StyleController_F.AppearanceDisabled.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.StyleController_F.AppearanceDisabled.Options.UseBackColor = true;
            this.StyleController_F.AppearanceDisabled.Options.UseForeColor = true;
            this.StyleController_F.AppearanceFocused.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(253)))), ((int)(((byte)(253)))), ((int)(((byte)(181)))));
            this.StyleController_F.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
            this.StyleController_F.AppearanceFocused.Options.UseBackColor = true;
            this.StyleController_F.AppearanceFocused.Options.UseFont = true;
            // 
            // DefaultLookAndFeel_F
            // 
            this.DefaultLookAndFeel_F.LookAndFeel.SkinName = "The Asphalt World";
            // 
            // xtraTabPage1
            // 
            this.xtraTabPage1.AutoScroll = true;
            this.xtraTabPage1.Controls.Add(this.uSUARIOSGridControl);
            this.xtraTabPage1.Name = "xtraTabPage1";
            this.xtraTabPage1.Size = new System.Drawing.Size(510, 278);
            this.xtraTabPage1.Text = "xtraTabPage1";
            // 
            // uSUARIOSGridControl
            // 
            this.uSUARIOSGridControl.DataSource = this.uSUARIOSBindingSource;
            this.uSUARIOSGridControl.EmbeddedNavigator.Name = "";
            this.uSUARIOSGridControl.Location = new System.Drawing.Point(29, 20);
            this.uSUARIOSGridControl.MainView = this.gridView1;
            this.uSUARIOSGridControl.Name = "uSUARIOSGridControl";
            this.uSUARIOSGridControl.Size = new System.Drawing.Size(300, 220);
            this.uSUARIOSGridControl.TabIndex = 0;
            this.uSUARIOSGridControl.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridView1});
            // 
            // uSUARIOSBindingSource
            // 
            this.uSUARIOSBindingSource.DataMember = "USUARIOS";
            this.uSUARIOSBindingSource.DataSource = this.dataSet1;
            // 
            // dataSet1
            // 
            this.dataSet1.DataSetName = "DataSet1";
            this.dataSet1.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // gridView1
            // 
            this.gridView1.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colUSU,
            this.colUSULogin,
            this.colUSUSenha,
            this.colUSUNome,
            this.colUSUEndereco,
            this.colUSUBairro,
            this.colUSUCep,
            this.colUSUFone1,
            this.colUSUFone2,
            this.colUSUEmail,
            this.colUSUDataInclusao,
            this.colUSUInclusao_USU,
            this.colUSUDataAtualizacao,
            this.colUSUAtualizacao_USU,
            this.colUSURamal});
            this.gridView1.GridControl = this.uSUARIOSGridControl;
            this.gridView1.Name = "gridView1";
            // 
            // colUSU
            // 
            this.colUSU.Caption = "USU";
            this.colUSU.FieldName = "USU";
            this.colUSU.Name = "colUSU";
            this.colUSU.OptionsColumn.ReadOnly = true;
            this.colUSU.Visible = true;
            this.colUSU.VisibleIndex = 0;
            // 
            // colUSULogin
            // 
            this.colUSULogin.Caption = "USULogin";
            this.colUSULogin.FieldName = "USULogin";
            this.colUSULogin.Name = "colUSULogin";
            this.colUSULogin.Visible = true;
            this.colUSULogin.VisibleIndex = 1;
            // 
            // colUSUSenha
            // 
            this.colUSUSenha.Caption = "USUSenha";
            this.colUSUSenha.FieldName = "USUSenha";
            this.colUSUSenha.Name = "colUSUSenha";
            this.colUSUSenha.Visible = true;
            this.colUSUSenha.VisibleIndex = 2;
            // 
            // colUSUNome
            // 
            this.colUSUNome.Caption = "USUNome";
            this.colUSUNome.FieldName = "USUNome";
            this.colUSUNome.Name = "colUSUNome";
            this.colUSUNome.Visible = true;
            this.colUSUNome.VisibleIndex = 3;
            // 
            // colUSUEndereco
            // 
            this.colUSUEndereco.Caption = "USUEndereco";
            this.colUSUEndereco.FieldName = "USUEndereco";
            this.colUSUEndereco.Name = "colUSUEndereco";
            this.colUSUEndereco.Visible = true;
            this.colUSUEndereco.VisibleIndex = 4;
            // 
            // colUSUBairro
            // 
            this.colUSUBairro.Caption = "USUBairro";
            this.colUSUBairro.FieldName = "USUBairro";
            this.colUSUBairro.Name = "colUSUBairro";
            this.colUSUBairro.Visible = true;
            this.colUSUBairro.VisibleIndex = 5;
            // 
            // colUSUCep
            // 
            this.colUSUCep.Caption = "USUCep";
            this.colUSUCep.FieldName = "USUCep";
            this.colUSUCep.Name = "colUSUCep";
            this.colUSUCep.Visible = true;
            this.colUSUCep.VisibleIndex = 6;
            // 
            // colUSUFone1
            // 
            this.colUSUFone1.Caption = "USUFone1";
            this.colUSUFone1.FieldName = "USUFone1";
            this.colUSUFone1.Name = "colUSUFone1";
            this.colUSUFone1.Visible = true;
            this.colUSUFone1.VisibleIndex = 7;
            // 
            // colUSUFone2
            // 
            this.colUSUFone2.Caption = "USUFone2";
            this.colUSUFone2.FieldName = "USUFone2";
            this.colUSUFone2.Name = "colUSUFone2";
            this.colUSUFone2.Visible = true;
            this.colUSUFone2.VisibleIndex = 8;
            // 
            // colUSUEmail
            // 
            this.colUSUEmail.Caption = "USUEmail";
            this.colUSUEmail.FieldName = "USUEmail";
            this.colUSUEmail.Name = "colUSUEmail";
            this.colUSUEmail.Visible = true;
            this.colUSUEmail.VisibleIndex = 9;
            // 
            // colUSUDataInclusao
            // 
            this.colUSUDataInclusao.Caption = "USUDataInclusao";
            this.colUSUDataInclusao.FieldName = "USUDataInclusao";
            this.colUSUDataInclusao.Name = "colUSUDataInclusao";
            this.colUSUDataInclusao.Visible = true;
            this.colUSUDataInclusao.VisibleIndex = 10;
            // 
            // colUSUInclusao_USU
            // 
            this.colUSUInclusao_USU.Caption = "USUInclusao_USU";
            this.colUSUInclusao_USU.FieldName = "USUInclusao_USU";
            this.colUSUInclusao_USU.Name = "colUSUInclusao_USU";
            this.colUSUInclusao_USU.Visible = true;
            this.colUSUInclusao_USU.VisibleIndex = 11;
            // 
            // colUSUDataAtualizacao
            // 
            this.colUSUDataAtualizacao.Caption = "USUDataAtualizacao";
            this.colUSUDataAtualizacao.FieldName = "USUDataAtualizacao";
            this.colUSUDataAtualizacao.Name = "colUSUDataAtualizacao";
            this.colUSUDataAtualizacao.Visible = true;
            this.colUSUDataAtualizacao.VisibleIndex = 12;
            // 
            // colUSUAtualizacao_USU
            // 
            this.colUSUAtualizacao_USU.Caption = "USUAtualizacao_USU";
            this.colUSUAtualizacao_USU.FieldName = "USUAtualizacao_USU";
            this.colUSUAtualizacao_USU.Name = "colUSUAtualizacao_USU";
            this.colUSUAtualizacao_USU.Visible = true;
            this.colUSUAtualizacao_USU.VisibleIndex = 13;
            // 
            // colUSURamal
            // 
            this.colUSURamal.Caption = "USURamal";
            this.colUSURamal.FieldName = "USURamal";
            this.colUSURamal.Name = "colUSURamal";
            this.colUSURamal.Visible = true;
            this.colUSURamal.VisibleIndex = 14;
            // 
            // uSUARIOSTableAdapter
            // 
            this.uSUARIOSTableAdapter.ClearBeforeFill = true;
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(60, 7);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(75, 23);
            this.button1.TabIndex = 0;
            this.button1.Text = "button1";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // textEdit1
            // 
            this.textEdit1.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.uSUARIOSBindingSource, "USUNome", true));
            this.textEdit1.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.uSUARIOSBindingSource, "USUNome", true));
            this.textEdit1.Location = new System.Drawing.Point(251, 9);
            this.textEdit1.Name = "textEdit1";
            this.textEdit1.Size = new System.Drawing.Size(100, 20);
            this.textEdit1.TabIndex = 1;
            // 
            // CamposTab
            // 
            this.BindingSourcePrincipal = this.uSUARIOSBindingSource;
            this.ImagemG = ((System.Drawing.Image)(resources.GetObject("$this.ImagemG")));
            this.ImagemP = ((System.Drawing.Image)(resources.GetObject("$this.ImagemP")));
            this.LookAndFeel.UseDefaultLookAndFeel = false;
            this.Name = "CamposTab";
            paginaTabInfo1.Name = "pa1";
            paginaTabInfo1.PaginaTabType = typeof(Teste.umatab);
            paginaTabInfo2.Name = "pb";
            paginaTabInfo2.PaginaTabType = typeof(Teste.umatab);
            this.PaginaTabInfoCollection.AddRange(new CompontesBasicos.Classes_de_interface.PaginaTabInfo[] {
            paginaTabInfo1,
            paginaTabInfo2});
            this.Size = new System.Drawing.Size(519, 425);
            ((System.ComponentModel.ISupportInitialize)(this.GroupControl_F)).EndInit();
            this.GroupControl_F.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.TabControl_F)).EndInit();
            this.TabControl_F.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.BarManager_F)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.BarAndDockingController_F)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.BindingSource_F)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.StyleController_F)).EndInit();
            this.xtraTabPage1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.uSUARIOSGridControl)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uSUARIOSBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit1.Properties)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button button1;
        private DevExpress.XtraTab.XtraTabPage xtraTabPage1;
        private DevExpress.XtraGrid.GridControl uSUARIOSGridControl;
        private System.Windows.Forms.BindingSource uSUARIOSBindingSource;
        private DataSet1 dataSet1;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView1;
        private DevExpress.XtraGrid.Columns.GridColumn colUSU;
        private DevExpress.XtraGrid.Columns.GridColumn colUSULogin;
        private DevExpress.XtraGrid.Columns.GridColumn colUSUSenha;
        private DevExpress.XtraGrid.Columns.GridColumn colUSUNome;
        private DevExpress.XtraGrid.Columns.GridColumn colUSUEndereco;
        private DevExpress.XtraGrid.Columns.GridColumn colUSUBairro;
        private DevExpress.XtraGrid.Columns.GridColumn colUSUCep;
        private DevExpress.XtraGrid.Columns.GridColumn colUSUFone1;
        private DevExpress.XtraGrid.Columns.GridColumn colUSUFone2;
        private DevExpress.XtraGrid.Columns.GridColumn colUSUEmail;
        private DevExpress.XtraGrid.Columns.GridColumn colUSUDataInclusao;
        private DevExpress.XtraGrid.Columns.GridColumn colUSUInclusao_USU;
        private DevExpress.XtraGrid.Columns.GridColumn colUSUDataAtualizacao;
        private DevExpress.XtraGrid.Columns.GridColumn colUSUAtualizacao_USU;
        private DevExpress.XtraGrid.Columns.GridColumn colUSURamal;
        private Teste.DataSet1TableAdapters.USUARIOSTableAdapter uSUARIOSTableAdapter;
        private DevExpress.XtraEditors.TextEdit textEdit1;
    }
}
