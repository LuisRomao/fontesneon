using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace Teste
{
    public partial class Controle2 : CompontesBasicos.ComponenteBase
    {
        public Controle2()
        {
            InitializeComponent();
        }

        private void simpleButton1_Click(object sender, EventArgs e)
        {
            //FormComC2 F2 = new FormComC2();
            //F2.controle21.cONDOMINIOSBindingSource.DataSource = this.cONDOMINIOSBindingSource;
            //F2.Show();
            //ShowModuloAdd(typeof(TesteCampos), "USUsenha", 30);
            //ShowModuloAdd(typeof(TesteCampos), "USUsenha", "30");
            //ShowModulo(typeof(TesteGridSimples),false);
            MessageBox.Show(ShowModuloAdd(typeof(TesteGridSimples)).ToString());
            MessageBox.Show(ShowModuloAdd(typeof(TesteCampos)).ToString());
        }

        private void cONDOMINIOSBindingNavigatorSaveItem_Click(object sender, EventArgs e)
        {
            this.Validate();
            this.cONDOMINIOSBindingSource.EndEdit();
            this.cONDOMINIOSTableAdapter.Update(this.dataSet1.CONDOMINIOS);

        }

        private void Controle2_Load(object sender, EventArgs e)
        {
            this.cONDOMINIOSTableAdapter.Fill(dataSet1.CONDOMINIOS);
        }
    }
}

