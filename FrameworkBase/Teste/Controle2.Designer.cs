namespace Teste
{
    partial class Controle2
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.Windows.Forms.Label cONCidadeLabel;
            System.Windows.Forms.Label cONNomeLabel;
            System.Windows.Forms.Label cONCodigoLabel;
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Controle2));
            this.simpleButton1 = new DevExpress.XtraEditors.SimpleButton();
            this.label1 = new System.Windows.Forms.Label();
            this.dataSet1 = new Teste.DataSet1();
            this.cONDOMINIOSBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.bindingSource1 = new System.Windows.Forms.BindingSource(this.components);
            this.cONDOMINIOSTableAdapter = new Teste.DataSet1TableAdapters.CONDOMINIOSTableAdapter();
            this.cONDOMINIOSBindingNavigator = new System.Windows.Forms.BindingNavigator(this.components);
            this.bindingNavigatorAddNewItem = new System.Windows.Forms.ToolStripButton();
            this.bindingNavigatorCountItem = new System.Windows.Forms.ToolStripLabel();
            this.bindingNavigatorDeleteItem = new System.Windows.Forms.ToolStripButton();
            this.bindingNavigatorMoveFirstItem = new System.Windows.Forms.ToolStripButton();
            this.bindingNavigatorMovePreviousItem = new System.Windows.Forms.ToolStripButton();
            this.bindingNavigatorSeparator = new System.Windows.Forms.ToolStripSeparator();
            this.bindingNavigatorPositionItem = new System.Windows.Forms.ToolStripTextBox();
            this.bindingNavigatorSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            this.bindingNavigatorMoveNextItem = new System.Windows.Forms.ToolStripButton();
            this.bindingNavigatorMoveLastItem = new System.Windows.Forms.ToolStripButton();
            this.bindingNavigatorSeparator2 = new System.Windows.Forms.ToolStripSeparator();
            this.cONDOMINIOSBindingNavigatorSaveItem = new System.Windows.Forms.ToolStripButton();
            this.cONCidadeTextEdit = new DevExpress.XtraEditors.TextEdit();
            this.cONNomeTextEdit = new DevExpress.XtraEditors.TextEdit();
            this.cONCodigoTextEdit = new DevExpress.XtraEditors.TextEdit();
            cONCidadeLabel = new System.Windows.Forms.Label();
            cONNomeLabel = new System.Windows.Forms.Label();
            cONCodigoLabel = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.StyleController_F)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cONDOMINIOSBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.bindingSource1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cONDOMINIOSBindingNavigator)).BeginInit();
            this.cONDOMINIOSBindingNavigator.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.cONCidadeTextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cONNomeTextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cONCodigoTextEdit.Properties)).BeginInit();
            this.SuspendLayout();
            // 
            // ToolTipController_F
            // 
            this.ToolTipController_F.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.ToolTipController_F.Appearance.ForeColor = System.Drawing.Color.Black;
            this.ToolTipController_F.Appearance.Options.UseBackColor = true;
            this.ToolTipController_F.Appearance.Options.UseFont = true;
            this.ToolTipController_F.Appearance.Options.UseForeColor = true;
            this.ToolTipController_F.Appearance.Options.UseTextOptions = true;
            this.ToolTipController_F.Appearance.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            // 
            // StyleController_F
            // 
            this.StyleController_F.AppearanceFocused.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.StyleController_F.AppearanceFocused.Options.UseBackColor = true;
            // 
            // DefaultLookAndFeel_F
            // 
            this.DefaultLookAndFeel_F.LookAndFeel.SkinName = "The Asphalt World";
            // 
            // cONCidadeLabel
            // 
            cONCidadeLabel.AutoSize = true;
            cONCidadeLabel.Location = new System.Drawing.Point(233, 124);
            cONCidadeLabel.Name = "cONCidadeLabel";
            cONCidadeLabel.Size = new System.Drawing.Size(66, 13);
            cONCidadeLabel.TabIndex = 4;
            cONCidadeLabel.Text = "CONCidade:";
            // 
            // cONNomeLabel
            // 
            cONNomeLabel.AutoSize = true;
            cONNomeLabel.Location = new System.Drawing.Point(300, 179);
            cONNomeLabel.Name = "cONNomeLabel";
            cONNomeLabel.Size = new System.Drawing.Size(61, 13);
            cONNomeLabel.TabIndex = 5;
            cONNomeLabel.Text = "CONNome:";
            // 
            // cONCodigoLabel
            // 
            cONCodigoLabel.AutoSize = true;
            cONCodigoLabel.Location = new System.Drawing.Point(130, 173);
            cONCodigoLabel.Name = "cONCodigoLabel";
            cONCodigoLabel.Size = new System.Drawing.Size(66, 13);
            cONCodigoLabel.TabIndex = 7;
            cONCodigoLabel.Text = "CONCodigo:";
            // 
            // simpleButton1
            // 
            this.simpleButton1.Location = new System.Drawing.Point(200, 31);
            this.simpleButton1.Name = "simpleButton1";
            this.simpleButton1.Size = new System.Drawing.Size(75, 23);
            this.simpleButton1.TabIndex = 0;
            this.simpleButton1.Text = "simpleButton1";
            this.simpleButton1.Click += new System.EventHandler(this.simpleButton1_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(29, 31);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(69, 16);
            this.label1.TabIndex = 3;
            this.label1.Text = "Contrle 2";
            // 
            // dataSet1
            // 
            this.dataSet1.DataSetName = "DataSet1";
            this.dataSet1.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // cONDOMINIOSBindingSource
            // 
            this.cONDOMINIOSBindingSource.DataSource = this.bindingSource1;
            // 
            // bindingSource1
            // 
            this.bindingSource1.DataMember = "CONDOMINIOS";
            this.bindingSource1.DataSource = this.dataSet1;
            // 
            // cONDOMINIOSTableAdapter
            // 
            this.cONDOMINIOSTableAdapter.ClearBeforeFill = true;
            // 
            // cONDOMINIOSBindingNavigator
            // 
            this.cONDOMINIOSBindingNavigator.AddNewItem = this.bindingNavigatorAddNewItem;
            this.cONDOMINIOSBindingNavigator.BindingSource = this.cONDOMINIOSBindingSource;
            this.cONDOMINIOSBindingNavigator.CountItem = this.bindingNavigatorCountItem;
            this.cONDOMINIOSBindingNavigator.DeleteItem = this.bindingNavigatorDeleteItem;
            this.cONDOMINIOSBindingNavigator.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.bindingNavigatorMoveFirstItem,
            this.bindingNavigatorMovePreviousItem,
            this.bindingNavigatorSeparator,
            this.bindingNavigatorPositionItem,
            this.bindingNavigatorCountItem,
            this.bindingNavigatorSeparator1,
            this.bindingNavigatorMoveNextItem,
            this.bindingNavigatorMoveLastItem,
            this.bindingNavigatorSeparator2,
            this.bindingNavigatorAddNewItem,
            this.bindingNavigatorDeleteItem,
            this.cONDOMINIOSBindingNavigatorSaveItem});
            this.cONDOMINIOSBindingNavigator.Location = new System.Drawing.Point(0, 0);
            this.cONDOMINIOSBindingNavigator.MoveFirstItem = this.bindingNavigatorMoveFirstItem;
            this.cONDOMINIOSBindingNavigator.MoveLastItem = this.bindingNavigatorMoveLastItem;
            this.cONDOMINIOSBindingNavigator.MoveNextItem = this.bindingNavigatorMoveNextItem;
            this.cONDOMINIOSBindingNavigator.MovePreviousItem = this.bindingNavigatorMovePreviousItem;
            this.cONDOMINIOSBindingNavigator.Name = "cONDOMINIOSBindingNavigator";
            this.cONDOMINIOSBindingNavigator.PositionItem = this.bindingNavigatorPositionItem;
            this.cONDOMINIOSBindingNavigator.Size = new System.Drawing.Size(568, 25);
            this.cONDOMINIOSBindingNavigator.TabIndex = 4;
            this.cONDOMINIOSBindingNavigator.Text = "bindingNavigator1";
            // 
            // bindingNavigatorAddNewItem
            // 
            this.bindingNavigatorAddNewItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.bindingNavigatorAddNewItem.Image = ((System.Drawing.Image)(resources.GetObject("bindingNavigatorAddNewItem.Image")));
            this.bindingNavigatorAddNewItem.Name = "bindingNavigatorAddNewItem";
            this.bindingNavigatorAddNewItem.RightToLeftAutoMirrorImage = true;
            this.bindingNavigatorAddNewItem.Size = new System.Drawing.Size(23, 22);
            this.bindingNavigatorAddNewItem.Text = "Add new";
            // 
            // bindingNavigatorCountItem
            // 
            this.bindingNavigatorCountItem.Name = "bindingNavigatorCountItem";
            this.bindingNavigatorCountItem.Size = new System.Drawing.Size(36, 22);
            this.bindingNavigatorCountItem.Text = "of {0}";
            this.bindingNavigatorCountItem.ToolTipText = "Total number of items";
            // 
            // bindingNavigatorDeleteItem
            // 
            this.bindingNavigatorDeleteItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.bindingNavigatorDeleteItem.Image = ((System.Drawing.Image)(resources.GetObject("bindingNavigatorDeleteItem.Image")));
            this.bindingNavigatorDeleteItem.Name = "bindingNavigatorDeleteItem";
            this.bindingNavigatorDeleteItem.RightToLeftAutoMirrorImage = true;
            this.bindingNavigatorDeleteItem.Size = new System.Drawing.Size(23, 22);
            this.bindingNavigatorDeleteItem.Text = "Delete";
            // 
            // bindingNavigatorMoveFirstItem
            // 
            this.bindingNavigatorMoveFirstItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.bindingNavigatorMoveFirstItem.Image = ((System.Drawing.Image)(resources.GetObject("bindingNavigatorMoveFirstItem.Image")));
            this.bindingNavigatorMoveFirstItem.Name = "bindingNavigatorMoveFirstItem";
            this.bindingNavigatorMoveFirstItem.RightToLeftAutoMirrorImage = true;
            this.bindingNavigatorMoveFirstItem.Size = new System.Drawing.Size(23, 22);
            this.bindingNavigatorMoveFirstItem.Text = "Move first";
            // 
            // bindingNavigatorMovePreviousItem
            // 
            this.bindingNavigatorMovePreviousItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.bindingNavigatorMovePreviousItem.Image = ((System.Drawing.Image)(resources.GetObject("bindingNavigatorMovePreviousItem.Image")));
            this.bindingNavigatorMovePreviousItem.Name = "bindingNavigatorMovePreviousItem";
            this.bindingNavigatorMovePreviousItem.RightToLeftAutoMirrorImage = true;
            this.bindingNavigatorMovePreviousItem.Size = new System.Drawing.Size(23, 22);
            this.bindingNavigatorMovePreviousItem.Text = "Move previous";
            // 
            // bindingNavigatorSeparator
            // 
            this.bindingNavigatorSeparator.Name = "bindingNavigatorSeparator";
            this.bindingNavigatorSeparator.Size = new System.Drawing.Size(6, 25);
            // 
            // bindingNavigatorPositionItem
            // 
            this.bindingNavigatorPositionItem.AccessibleName = "Position";
            this.bindingNavigatorPositionItem.AutoSize = false;
            this.bindingNavigatorPositionItem.Name = "bindingNavigatorPositionItem";
            this.bindingNavigatorPositionItem.Size = new System.Drawing.Size(50, 21);
            this.bindingNavigatorPositionItem.Text = "0";
            this.bindingNavigatorPositionItem.ToolTipText = "Current position";
            // 
            // bindingNavigatorSeparator1
            // 
            this.bindingNavigatorSeparator1.Name = "bindingNavigatorSeparator1";
            this.bindingNavigatorSeparator1.Size = new System.Drawing.Size(6, 25);
            // 
            // bindingNavigatorMoveNextItem
            // 
            this.bindingNavigatorMoveNextItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.bindingNavigatorMoveNextItem.Image = ((System.Drawing.Image)(resources.GetObject("bindingNavigatorMoveNextItem.Image")));
            this.bindingNavigatorMoveNextItem.Name = "bindingNavigatorMoveNextItem";
            this.bindingNavigatorMoveNextItem.RightToLeftAutoMirrorImage = true;
            this.bindingNavigatorMoveNextItem.Size = new System.Drawing.Size(23, 22);
            this.bindingNavigatorMoveNextItem.Text = "Move next";
            // 
            // bindingNavigatorMoveLastItem
            // 
            this.bindingNavigatorMoveLastItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.bindingNavigatorMoveLastItem.Image = ((System.Drawing.Image)(resources.GetObject("bindingNavigatorMoveLastItem.Image")));
            this.bindingNavigatorMoveLastItem.Name = "bindingNavigatorMoveLastItem";
            this.bindingNavigatorMoveLastItem.RightToLeftAutoMirrorImage = true;
            this.bindingNavigatorMoveLastItem.Size = new System.Drawing.Size(23, 22);
            this.bindingNavigatorMoveLastItem.Text = "Move last";
            // 
            // bindingNavigatorSeparator2
            // 
            this.bindingNavigatorSeparator2.Name = "bindingNavigatorSeparator2";
            this.bindingNavigatorSeparator2.Size = new System.Drawing.Size(6, 25);
            // 
            // cONDOMINIOSBindingNavigatorSaveItem
            // 
            this.cONDOMINIOSBindingNavigatorSaveItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.cONDOMINIOSBindingNavigatorSaveItem.Image = ((System.Drawing.Image)(resources.GetObject("cONDOMINIOSBindingNavigatorSaveItem.Image")));
            this.cONDOMINIOSBindingNavigatorSaveItem.Name = "cONDOMINIOSBindingNavigatorSaveItem";
            this.cONDOMINIOSBindingNavigatorSaveItem.Size = new System.Drawing.Size(23, 22);
            this.cONDOMINIOSBindingNavigatorSaveItem.Text = "Save Data";
            this.cONDOMINIOSBindingNavigatorSaveItem.Click += new System.EventHandler(this.cONDOMINIOSBindingNavigatorSaveItem_Click);
            // 
            // cONCidadeTextEdit
            // 
            this.cONCidadeTextEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.cONDOMINIOSBindingSource, "CONCidade", true));
            this.cONCidadeTextEdit.Location = new System.Drawing.Point(305, 121);
            this.cONCidadeTextEdit.Name = "cONCidadeTextEdit";
            this.cONCidadeTextEdit.Size = new System.Drawing.Size(100, 20);
            this.cONCidadeTextEdit.TabIndex = 5;
            // 
            // cONNomeTextEdit
            // 
            this.cONNomeTextEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.cONDOMINIOSBindingSource, "CONNome", true));
            this.cONNomeTextEdit.Location = new System.Drawing.Point(367, 176);
            this.cONNomeTextEdit.Name = "cONNomeTextEdit";
            this.cONNomeTextEdit.Size = new System.Drawing.Size(100, 20);
            this.cONNomeTextEdit.TabIndex = 6;
            // 
            // cONCodigoTextEdit
            // 
            this.cONCodigoTextEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.cONDOMINIOSBindingSource, "CONCodigo", true));
            this.cONCodigoTextEdit.Location = new System.Drawing.Point(202, 170);
            this.cONCodigoTextEdit.Name = "cONCodigoTextEdit";
            this.cONCodigoTextEdit.Size = new System.Drawing.Size(100, 20);
            this.cONCodigoTextEdit.TabIndex = 8;
            // 
            // Controle2
            // 
            this.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.Controls.Add(cONCodigoLabel);
            this.Controls.Add(this.cONCodigoTextEdit);
            this.Controls.Add(cONNomeLabel);
            this.Controls.Add(this.cONNomeTextEdit);
            this.Controls.Add(cONCidadeLabel);
            this.Controls.Add(this.cONCidadeTextEdit);
            this.Controls.Add(this.cONDOMINIOSBindingNavigator);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.simpleButton1);
            this.ImagemG = ((System.Drawing.Image)(resources.GetObject("$this.ImagemG")));
            this.ImagemP = ((System.Drawing.Image)(resources.GetObject("$this.ImagemP")));
            this.LookAndFeel.UseDefaultLookAndFeel = false;
            this.Name = "Controle2";
            this.Size = new System.Drawing.Size(568, 289);
            this.Load += new System.EventHandler(this.Controle2_Load);
            ((System.ComponentModel.ISupportInitialize)(this.StyleController_F)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cONDOMINIOSBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.bindingSource1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cONDOMINIOSBindingNavigator)).EndInit();
            this.cONDOMINIOSBindingNavigator.ResumeLayout(false);
            this.cONDOMINIOSBindingNavigator.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.cONCidadeTextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cONNomeTextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cONCodigoTextEdit.Properties)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private DevExpress.XtraEditors.SimpleButton simpleButton1;
        private System.Windows.Forms.Label label1;
        private DataSet1 dataSet1;
        private System.Windows.Forms.BindingSource cONDOMINIOSBindingSource;
        private Teste.DataSet1TableAdapters.CONDOMINIOSTableAdapter cONDOMINIOSTableAdapter;
        private System.Windows.Forms.ToolStripButton bindingNavigatorAddNewItem;
        private System.Windows.Forms.ToolStripLabel bindingNavigatorCountItem;
        private System.Windows.Forms.ToolStripButton bindingNavigatorDeleteItem;
        private System.Windows.Forms.ToolStripButton bindingNavigatorMoveFirstItem;
        private System.Windows.Forms.ToolStripButton bindingNavigatorMovePreviousItem;
        private System.Windows.Forms.ToolStripSeparator bindingNavigatorSeparator;
        private System.Windows.Forms.ToolStripTextBox bindingNavigatorPositionItem;
        private System.Windows.Forms.ToolStripSeparator bindingNavigatorSeparator1;
        private System.Windows.Forms.ToolStripButton bindingNavigatorMoveNextItem;
        private System.Windows.Forms.ToolStripButton bindingNavigatorMoveLastItem;
        private System.Windows.Forms.ToolStripSeparator bindingNavigatorSeparator2;
        private System.Windows.Forms.ToolStripButton cONDOMINIOSBindingNavigatorSaveItem;
        private DevExpress.XtraEditors.TextEdit cONCidadeTextEdit;
        private DevExpress.XtraEditors.TextEdit cONNomeTextEdit;
        private DevExpress.XtraEditors.TextEdit cONCodigoTextEdit;
        private System.Windows.Forms.BindingSource bindingSource1;
        public System.Windows.Forms.BindingNavigator cONDOMINIOSBindingNavigator;
    }
}
