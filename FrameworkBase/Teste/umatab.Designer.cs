namespace Teste
{
    partial class umatab
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.Windows.Forms.Label cON_CIDLabel;
            System.Windows.Forms.Label cON_CIDLabel1;
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(umatab));
            this.dataSet1 = new Teste.DataSet1();
            this.cONDOMINIOSBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.cONDOMINIOSBindingNavigator = new System.Windows.Forms.BindingNavigator(this.components);
            this.bindingNavigatorAddNewItem = new System.Windows.Forms.ToolStripButton();
            this.bindingNavigatorCountItem = new System.Windows.Forms.ToolStripLabel();
            this.bindingNavigatorDeleteItem = new System.Windows.Forms.ToolStripButton();
            this.bindingNavigatorMoveFirstItem = new System.Windows.Forms.ToolStripButton();
            this.bindingNavigatorMovePreviousItem = new System.Windows.Forms.ToolStripButton();
            this.bindingNavigatorSeparator = new System.Windows.Forms.ToolStripSeparator();
            this.bindingNavigatorPositionItem = new System.Windows.Forms.ToolStripTextBox();
            this.bindingNavigatorSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            this.bindingNavigatorMoveNextItem = new System.Windows.Forms.ToolStripButton();
            this.bindingNavigatorMoveLastItem = new System.Windows.Forms.ToolStripButton();
            this.bindingNavigatorSeparator2 = new System.Windows.Forms.ToolStripSeparator();
            this.cONDOMINIOSBindingNavigatorSaveItem = new System.Windows.Forms.ToolStripButton();
            this.cON_CIDSpinEdit = new DevExpress.XtraEditors.SpinEdit();
            this.cON_CIDLookUpEdit = new DevExpress.XtraEditors.LookUpEdit();
            this.dataTable1BindingSource = new System.Windows.Forms.BindingSource(this.components);
            cON_CIDLabel = new System.Windows.Forms.Label();
            cON_CIDLabel1 = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.StyleController_F)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cONDOMINIOSBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cONDOMINIOSBindingNavigator)).BeginInit();
            this.cONDOMINIOSBindingNavigator.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.cON_CIDSpinEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cON_CIDLookUpEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataTable1BindingSource)).BeginInit();
            this.SuspendLayout();
            // 
            // ToolTipController_F
            // 
            this.ToolTipController_F.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.ToolTipController_F.Appearance.ForeColor = System.Drawing.Color.Black;
            this.ToolTipController_F.Appearance.Options.UseBackColor = true;
            this.ToolTipController_F.Appearance.Options.UseFont = true;
            this.ToolTipController_F.Appearance.Options.UseForeColor = true;
            this.ToolTipController_F.Appearance.Options.UseTextOptions = true;
            this.ToolTipController_F.Appearance.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            // 
            // StyleController_F
            // 
            this.StyleController_F.AppearanceFocused.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.StyleController_F.AppearanceFocused.Options.UseBackColor = true;
            // 
            // DefaultLookAndFeel_F
            // 
            this.DefaultLookAndFeel_F.LookAndFeel.SkinName = "The Asphalt World";
            // 
            // cON_CIDLabel
            // 
            cON_CIDLabel.AutoSize = true;
            cON_CIDLabel.Location = new System.Drawing.Point(91, 42);
            cON_CIDLabel.Name = "cON_CIDLabel";
            cON_CIDLabel.Size = new System.Drawing.Size(54, 13);
            cON_CIDLabel.TabIndex = 1;
            cON_CIDLabel.Text = "CON CID:";
            // 
            // cON_CIDLabel1
            // 
            cON_CIDLabel1.AutoSize = true;
            cON_CIDLabel1.Location = new System.Drawing.Point(98, 121);
            cON_CIDLabel1.Name = "cON_CIDLabel1";
            cON_CIDLabel1.Size = new System.Drawing.Size(54, 13);
            cON_CIDLabel1.TabIndex = 3;
            cON_CIDLabel1.Text = "CON CID:";
            // 
            // dataSet1
            // 
            this.dataSet1.DataSetName = "DataSet1";
            this.dataSet1.EnforceConstraints = false;
            this.dataSet1.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // cONDOMINIOSBindingSource
            // 
            this.cONDOMINIOSBindingSource.DataMember = "CONDOMINIOS";
            this.cONDOMINIOSBindingSource.DataSource = typeof(Teste.DataSet1);
            // 
            // cONDOMINIOSBindingNavigator
            // 
            this.cONDOMINIOSBindingNavigator.AddNewItem = this.bindingNavigatorAddNewItem;
            this.cONDOMINIOSBindingNavigator.BindingSource = this.cONDOMINIOSBindingSource;
            this.cONDOMINIOSBindingNavigator.CountItem = this.bindingNavigatorCountItem;
            this.cONDOMINIOSBindingNavigator.DeleteItem = this.bindingNavigatorDeleteItem;
            this.cONDOMINIOSBindingNavigator.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.bindingNavigatorMoveFirstItem,
            this.bindingNavigatorMovePreviousItem,
            this.bindingNavigatorSeparator,
            this.bindingNavigatorPositionItem,
            this.bindingNavigatorCountItem,
            this.bindingNavigatorSeparator1,
            this.bindingNavigatorMoveNextItem,
            this.bindingNavigatorMoveLastItem,
            this.bindingNavigatorSeparator2,
            this.bindingNavigatorAddNewItem,
            this.bindingNavigatorDeleteItem,
            this.cONDOMINIOSBindingNavigatorSaveItem});
            this.cONDOMINIOSBindingNavigator.Location = new System.Drawing.Point(0, 0);
            this.cONDOMINIOSBindingNavigator.MoveFirstItem = this.bindingNavigatorMoveFirstItem;
            this.cONDOMINIOSBindingNavigator.MoveLastItem = this.bindingNavigatorMoveLastItem;
            this.cONDOMINIOSBindingNavigator.MoveNextItem = this.bindingNavigatorMoveNextItem;
            this.cONDOMINIOSBindingNavigator.MovePreviousItem = this.bindingNavigatorMovePreviousItem;
            this.cONDOMINIOSBindingNavigator.Name = "cONDOMINIOSBindingNavigator";
            this.cONDOMINIOSBindingNavigator.PositionItem = this.bindingNavigatorPositionItem;
            this.cONDOMINIOSBindingNavigator.Size = new System.Drawing.Size(976, 25);
            this.cONDOMINIOSBindingNavigator.TabIndex = 0;
            this.cONDOMINIOSBindingNavigator.Text = "bindingNavigator1";
            // 
            // bindingNavigatorAddNewItem
            // 
            this.bindingNavigatorAddNewItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.bindingNavigatorAddNewItem.Image = ((System.Drawing.Image)(resources.GetObject("bindingNavigatorAddNewItem.Image")));
            this.bindingNavigatorAddNewItem.Name = "bindingNavigatorAddNewItem";
            this.bindingNavigatorAddNewItem.RightToLeftAutoMirrorImage = true;
            this.bindingNavigatorAddNewItem.Size = new System.Drawing.Size(23, 22);
            this.bindingNavigatorAddNewItem.Text = "Add new";
            // 
            // bindingNavigatorCountItem
            // 
            this.bindingNavigatorCountItem.Name = "bindingNavigatorCountItem";
            this.bindingNavigatorCountItem.Size = new System.Drawing.Size(36, 22);
            this.bindingNavigatorCountItem.Text = "of {0}";
            this.bindingNavigatorCountItem.ToolTipText = "Total number of items";
            // 
            // bindingNavigatorDeleteItem
            // 
            this.bindingNavigatorDeleteItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.bindingNavigatorDeleteItem.Image = ((System.Drawing.Image)(resources.GetObject("bindingNavigatorDeleteItem.Image")));
            this.bindingNavigatorDeleteItem.Name = "bindingNavigatorDeleteItem";
            this.bindingNavigatorDeleteItem.RightToLeftAutoMirrorImage = true;
            this.bindingNavigatorDeleteItem.Size = new System.Drawing.Size(23, 22);
            this.bindingNavigatorDeleteItem.Text = "Delete";
            // 
            // bindingNavigatorMoveFirstItem
            // 
            this.bindingNavigatorMoveFirstItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.bindingNavigatorMoveFirstItem.Image = ((System.Drawing.Image)(resources.GetObject("bindingNavigatorMoveFirstItem.Image")));
            this.bindingNavigatorMoveFirstItem.Name = "bindingNavigatorMoveFirstItem";
            this.bindingNavigatorMoveFirstItem.RightToLeftAutoMirrorImage = true;
            this.bindingNavigatorMoveFirstItem.Size = new System.Drawing.Size(23, 22);
            this.bindingNavigatorMoveFirstItem.Text = "Move first";
            // 
            // bindingNavigatorMovePreviousItem
            // 
            this.bindingNavigatorMovePreviousItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.bindingNavigatorMovePreviousItem.Image = ((System.Drawing.Image)(resources.GetObject("bindingNavigatorMovePreviousItem.Image")));
            this.bindingNavigatorMovePreviousItem.Name = "bindingNavigatorMovePreviousItem";
            this.bindingNavigatorMovePreviousItem.RightToLeftAutoMirrorImage = true;
            this.bindingNavigatorMovePreviousItem.Size = new System.Drawing.Size(23, 22);
            this.bindingNavigatorMovePreviousItem.Text = "Move previous";
            // 
            // bindingNavigatorSeparator
            // 
            this.bindingNavigatorSeparator.Name = "bindingNavigatorSeparator";
            this.bindingNavigatorSeparator.Size = new System.Drawing.Size(6, 25);
            // 
            // bindingNavigatorPositionItem
            // 
            this.bindingNavigatorPositionItem.AccessibleName = "Position";
            this.bindingNavigatorPositionItem.AutoSize = false;
            this.bindingNavigatorPositionItem.Name = "bindingNavigatorPositionItem";
            this.bindingNavigatorPositionItem.Size = new System.Drawing.Size(50, 21);
            this.bindingNavigatorPositionItem.Text = "0";
            this.bindingNavigatorPositionItem.ToolTipText = "Current position";
            // 
            // bindingNavigatorSeparator1
            // 
            this.bindingNavigatorSeparator1.Name = "bindingNavigatorSeparator1";
            this.bindingNavigatorSeparator1.Size = new System.Drawing.Size(6, 25);
            // 
            // bindingNavigatorMoveNextItem
            // 
            this.bindingNavigatorMoveNextItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.bindingNavigatorMoveNextItem.Image = ((System.Drawing.Image)(resources.GetObject("bindingNavigatorMoveNextItem.Image")));
            this.bindingNavigatorMoveNextItem.Name = "bindingNavigatorMoveNextItem";
            this.bindingNavigatorMoveNextItem.RightToLeftAutoMirrorImage = true;
            this.bindingNavigatorMoveNextItem.Size = new System.Drawing.Size(23, 22);
            this.bindingNavigatorMoveNextItem.Text = "Move next";
            // 
            // bindingNavigatorMoveLastItem
            // 
            this.bindingNavigatorMoveLastItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.bindingNavigatorMoveLastItem.Image = ((System.Drawing.Image)(resources.GetObject("bindingNavigatorMoveLastItem.Image")));
            this.bindingNavigatorMoveLastItem.Name = "bindingNavigatorMoveLastItem";
            this.bindingNavigatorMoveLastItem.RightToLeftAutoMirrorImage = true;
            this.bindingNavigatorMoveLastItem.Size = new System.Drawing.Size(23, 22);
            this.bindingNavigatorMoveLastItem.Text = "Move last";
            // 
            // bindingNavigatorSeparator2
            // 
            this.bindingNavigatorSeparator2.Name = "bindingNavigatorSeparator2";
            this.bindingNavigatorSeparator2.Size = new System.Drawing.Size(6, 25);
            // 
            // cONDOMINIOSBindingNavigatorSaveItem
            // 
            this.cONDOMINIOSBindingNavigatorSaveItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.cONDOMINIOSBindingNavigatorSaveItem.Enabled = false;
            this.cONDOMINIOSBindingNavigatorSaveItem.Image = ((System.Drawing.Image)(resources.GetObject("cONDOMINIOSBindingNavigatorSaveItem.Image")));
            this.cONDOMINIOSBindingNavigatorSaveItem.Name = "cONDOMINIOSBindingNavigatorSaveItem";
            this.cONDOMINIOSBindingNavigatorSaveItem.Size = new System.Drawing.Size(23, 22);
            this.cONDOMINIOSBindingNavigatorSaveItem.Text = "Save Data";
            // 
            // cON_CIDSpinEdit
            // 
            this.cON_CIDSpinEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.cONDOMINIOSBindingSource, "CON_CID", true));
            this.cON_CIDSpinEdit.EditValue = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.cON_CIDSpinEdit.Location = new System.Drawing.Point(151, 39);
            this.cON_CIDSpinEdit.Name = "cON_CIDSpinEdit";
            this.cON_CIDSpinEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.cON_CIDSpinEdit.Size = new System.Drawing.Size(100, 20);
            this.cON_CIDSpinEdit.TabIndex = 2;
            // 
            // cON_CIDLookUpEdit
            // 
            this.cON_CIDLookUpEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.cONDOMINIOSBindingSource, "CON_CID", true));
            this.cON_CIDLookUpEdit.Location = new System.Drawing.Point(158, 118);
            this.cON_CIDLookUpEdit.Name = "cON_CIDLookUpEdit";
            this.cON_CIDLookUpEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.cON_CIDLookUpEdit.Properties.DataSource = this.dataTable1BindingSource;
            this.cON_CIDLookUpEdit.Properties.DisplayMember = "CIDNome";
            this.cON_CIDLookUpEdit.Properties.ValueMember = "CID";
            this.cON_CIDLookUpEdit.Size = new System.Drawing.Size(100, 20);
            this.cON_CIDLookUpEdit.TabIndex = 4;
            // 
            // dataTable1BindingSource
            // 
            this.dataTable1BindingSource.DataMember = "DataTable1";
            this.dataTable1BindingSource.DataSource = typeof(Teste.DataSet1);
            // 
            // umatab
            // 
            this.Controls.Add(cON_CIDLabel1);
            this.Controls.Add(this.cON_CIDLookUpEdit);
            this.Controls.Add(cON_CIDLabel);
            this.Controls.Add(this.cON_CIDSpinEdit);
            this.Controls.Add(this.cONDOMINIOSBindingNavigator);
            this.Doc = System.Windows.Forms.DockStyle.Fill;
            this.ImagemG = ((System.Drawing.Image)(resources.GetObject("$this.ImagemG")));
            this.ImagemP = ((System.Drawing.Image)(resources.GetObject("$this.ImagemP")));
            this.LookAndFeel.UseDefaultLookAndFeel = false;
            this.Name = "umatab";
            this.Size = new System.Drawing.Size(976, 318);
            ((System.ComponentModel.ISupportInitialize)(this.StyleController_F)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cONDOMINIOSBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cONDOMINIOSBindingNavigator)).EndInit();
            this.cONDOMINIOSBindingNavigator.ResumeLayout(false);
            this.cONDOMINIOSBindingNavigator.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.cON_CIDSpinEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cON_CIDLookUpEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataTable1BindingSource)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private DataSet1 dataSet1;
        private System.Windows.Forms.BindingSource cONDOMINIOSBindingSource;
        private System.Windows.Forms.BindingNavigator cONDOMINIOSBindingNavigator;
        private System.Windows.Forms.ToolStripButton bindingNavigatorAddNewItem;
        private System.Windows.Forms.ToolStripLabel bindingNavigatorCountItem;
        private System.Windows.Forms.ToolStripButton bindingNavigatorDeleteItem;
        private System.Windows.Forms.ToolStripButton bindingNavigatorMoveFirstItem;
        private System.Windows.Forms.ToolStripButton bindingNavigatorMovePreviousItem;
        private System.Windows.Forms.ToolStripSeparator bindingNavigatorSeparator;
        private System.Windows.Forms.ToolStripTextBox bindingNavigatorPositionItem;
        private System.Windows.Forms.ToolStripSeparator bindingNavigatorSeparator1;
        private System.Windows.Forms.ToolStripButton bindingNavigatorMoveNextItem;
        private System.Windows.Forms.ToolStripButton bindingNavigatorMoveLastItem;
        private System.Windows.Forms.ToolStripSeparator bindingNavigatorSeparator2;
        private System.Windows.Forms.ToolStripButton cONDOMINIOSBindingNavigatorSaveItem;
        private DevExpress.XtraEditors.SpinEdit cON_CIDSpinEdit;
        private DevExpress.XtraEditors.LookUpEdit cON_CIDLookUpEdit;
        private System.Windows.Forms.BindingSource dataTable1BindingSource;

    }
}
