namespace Teste
{
    partial class Principal
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Principal));
            CompontesBasicos.ModuleInfo moduleInfo12 = new CompontesBasicos.ModuleInfo();
            CompontesBasicos.ModuleInfo moduleInfo13 = new CompontesBasicos.ModuleInfo();
            CompontesBasicos.ModuleInfo moduleInfo14 = new CompontesBasicos.ModuleInfo();
            CompontesBasicos.ModuleInfo moduleInfo15 = new CompontesBasicos.ModuleInfo();
            CompontesBasicos.ModuleInfo moduleInfo16 = new CompontesBasicos.ModuleInfo();
            CompontesBasicos.ModuleInfo moduleInfo17 = new CompontesBasicos.ModuleInfo();
            CompontesBasicos.ModuleInfo moduleInfo18 = new CompontesBasicos.ModuleInfo();
            CompontesBasicos.ModuleInfo moduleInfo19 = new CompontesBasicos.ModuleInfo();
            CompontesBasicos.ModuleInfo moduleInfo20 = new CompontesBasicos.ModuleInfo();
            CompontesBasicos.ModuleInfo moduleInfo21 = new CompontesBasicos.ModuleInfo();
            CompontesBasicos.ModuleInfo moduleInfo22 = new CompontesBasicos.ModuleInfo();
            this.navBarGroup1 = new DevExpress.XtraNavBar.NavBarGroup();
            this.navBarGroup2 = new DevExpress.XtraNavBar.NavBarGroup();
            this.navBarItem1 = new DevExpress.XtraNavBar.NavBarItem();
            this.navBarGroup3 = new DevExpress.XtraNavBar.NavBarGroup();
            this.navBarItem4 = new DevExpress.XtraNavBar.NavBarItem();
            this.navBarItem3 = new DevExpress.XtraNavBar.NavBarItem();
            this.navBarItem2 = new DevExpress.XtraNavBar.NavBarItem();
            this.textBox1 = new System.Windows.Forms.TextBox();
            this.xtraTabPage1 = new DevExpress.XtraTab.XtraTabPage();
            this.memoEdit1 = new DevExpress.XtraEditors.MemoEdit();
            this.dateEdit1 = new DevExpress.XtraEditors.DateEdit();
            this.button2 = new System.Windows.Forms.Button();
            this.button1 = new System.Windows.Forms.Button();
            this.testeC11 = new ComponentesTeste.TesteC1();
            this.dataSet11 = new Teste.DataSet1();
            ((System.ComponentModel.ISupportInitialize)(this.PictureEdit_F.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.NavBarControl_F)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.BarAndDockingController_F)).BeginInit();
            this.DockPanel_FContainer.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.TabControl_F)).BeginInit();
            this.TabControl_F.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.BarManager_F)).BeginInit();
            this.DockPanel_F.SuspendLayout();
            this.xtraTabPage1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.memoEdit1.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEdit1.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet11)).BeginInit();
            this.SuspendLayout();
            // 
            // PictureEdit_F
            // 
            // 
            // NavBarControl_F
            // 
            this.NavBarControl_F.ActiveGroup = this.navBarGroup3;
            this.NavBarControl_F.Groups.AddRange(new DevExpress.XtraNavBar.NavBarGroup[] {
            this.navBarGroup1,
            this.navBarGroup2,
            this.navBarGroup3});
            this.NavBarControl_F.Items.AddRange(new DevExpress.XtraNavBar.NavBarItem[] {
            this.navBarItem1,
            this.navBarItem2,
            this.navBarItem3,
            this.navBarItem4});
            // 
            // BarAndDockingController_F
            // 
            this.BarAndDockingController_F.PropertiesBar.AllowLinkLighting = false;
            // 
            // DefaultLookAndFeel_F
            // 
            this.DefaultLookAndFeel_F.LookAndFeel.SkinName = "The Asphalt World";
            // 
            // DefaultToolTipController_F
            // 
            // 
            // 
            // 
            this.DefaultToolTipController_F.DefaultController.Rounded = true;
            // 
            // barStatus_F
            // 
            this.barStatus_F.OptionsBar.AllowQuickCustomization = false;
            this.barStatus_F.OptionsBar.DisableClose = true;
            this.barStatus_F.OptionsBar.DisableCustomization = true;
            this.barStatus_F.OptionsBar.DrawDragBorder = false;
            this.barStatus_F.OptionsBar.DrawSizeGrip = true;
            this.barStatus_F.OptionsBar.UseWholeRow = true;
            // 
            // DockPanel_FContainer
            // 
            this.ToolTipController_F.SetSuperTip(this.DockPanel_FContainer, null);
            // 
            // TabControl_F
            // 
            this.TabControl_F.SelectedTabPage = this.xtraTabPage1;
            this.TabControl_F.TabPages.AddRange(new DevExpress.XtraTab.XtraTabPage[] {
            this.xtraTabPage1});
            this.TabControl_F.Tag = "tag";
            // 
            // DockPanel_F
            // 
            this.DockPanel_F.Options.AllowDockFill = false;
            this.DockPanel_F.Options.ShowCloseButton = false;
            this.DockPanel_F.Options.ShowMaximizeButton = false;
            // 
            // navBarGroup1
            // 
            this.navBarGroup1.Caption = "navBarGroup1";
            this.navBarGroup1.Expanded = true;
            this.navBarGroup1.Name = "navBarGroup1";
            // 
            // navBarGroup2
            // 
            this.navBarGroup2.Caption = "navBarGroup2";
            this.navBarGroup2.Expanded = true;
            this.navBarGroup2.Name = "navBarGroup2";
            // 
            // navBarItem1
            // 
            this.navBarItem1.Caption = "navBarItem1";
            this.navBarItem1.Name = "navBarItem1";
            this.navBarItem1.SmallImage = global::Teste.Properties.Resources.DOOR_1;
            // 
            // navBarGroup3
            // 
            this.navBarGroup3.Caption = "Avulsos";
            this.navBarGroup3.Expanded = true;
            this.navBarGroup3.ItemLinks.AddRange(new DevExpress.XtraNavBar.NavBarItemLink[] {
            new DevExpress.XtraNavBar.NavBarItemLink(this.navBarItem4),
            new DevExpress.XtraNavBar.NavBarItemLink(this.navBarItem3),
            new DevExpress.XtraNavBar.NavBarItemLink(this.navBarItem2)});
            this.navBarGroup3.Name = "navBarGroup3";
            // 
            // navBarItem4
            // 
            this.navBarItem4.Caption = "Teste Campos";
            this.navBarItem4.Name = "navBarItem4";
            this.navBarItem4.LinkClicked += new DevExpress.XtraNavBar.NavBarLinkEventHandler(this.navBarItem4_LinkClicked);
            // 
            // navBarItem3
            // 
            this.navBarItem3.Caption = "Componente 1";
            this.navBarItem3.Name = "navBarItem3";
            this.navBarItem3.LinkClicked += new DevExpress.XtraNavBar.NavBarLinkEventHandler(this.navBarItem3_LinkClicked);
            // 
            // navBarItem2
            // 
            this.navBarItem2.Caption = "Camponente 2";
            this.navBarItem2.Name = "navBarItem2";
            this.navBarItem2.LinkClicked += new DevExpress.XtraNavBar.NavBarLinkEventHandler(this.navBarItem2_LinkClicked);
            // 
            // textBox1
            // 
            this.textBox1.Location = new System.Drawing.Point(68, 106);
            this.textBox1.Name = "textBox1";
            this.textBox1.Size = new System.Drawing.Size(100, 20);
            this.ToolTipController_F.SetSuperTip(this.textBox1, null);
            this.textBox1.TabIndex = 0;
            // 
            // xtraTabPage1
            // 
            this.xtraTabPage1.Controls.Add(this.memoEdit1);
            this.xtraTabPage1.Controls.Add(this.dateEdit1);
            this.xtraTabPage1.Controls.Add(this.button2);
            this.xtraTabPage1.Controls.Add(this.button1);
            this.xtraTabPage1.Controls.Add(this.testeC11);
            this.xtraTabPage1.Name = "xtraTabPage1";
            this.xtraTabPage1.Size = new System.Drawing.Size(376, 324);
            this.xtraTabPage1.Text = "xtraTabPage1";
            this.xtraTabPage1.Resize += new System.EventHandler(this.xtraTabPage1_Resize);
            // 
            // memoEdit1
            // 
            this.memoEdit1.Location = new System.Drawing.Point(38, 243);
            this.memoEdit1.Name = "memoEdit1";
            this.memoEdit1.Size = new System.Drawing.Size(100, 96);
            this.memoEdit1.TabIndex = 4;
            // 
            // dateEdit1
            // 
            this.dateEdit1.EditValue = 4;
            this.dateEdit1.Enabled = false;
            this.dateEdit1.Location = new System.Drawing.Point(264, 237);
            this.dateEdit1.Name = "dateEdit1";
            this.dateEdit1.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.dateEdit1.Size = new System.Drawing.Size(100, 20);
            this.dateEdit1.TabIndex = 3;
            // 
            // button2
            // 
            this.button2.Location = new System.Drawing.Point(101, 139);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(299, 67);
            this.ToolTipController_F.SetSuperTip(this.button2, null);
            this.button2.TabIndex = 2;
            this.button2.Text = "button2";
            this.button2.UseVisualStyleBackColor = true;
            this.button2.Click += new System.EventHandler(this.button1_Click_2);
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(101, 47);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(299, 67);
            this.ToolTipController_F.SetSuperTip(this.button1, null);
            this.button1.TabIndex = 1;
            this.button1.Text = "button1";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click_1);
            // 
            // testeC11
            // 
            this.testeC11.Doc = System.Windows.Forms.DockStyle.Fill;
            this.testeC11.Dock = System.Windows.Forms.DockStyle.Fill;
            this.testeC11.Estado = CompontesBasicos.EstadosDosComponentes.Registrado;
            this.testeC11.Icone = null;
            this.testeC11.ImagemG = ((System.Drawing.Image)(resources.GetObject("testeC11.ImagemG")));
            this.testeC11.ImagemP = ((System.Drawing.Image)(resources.GetObject("testeC11.ImagemP")));
            this.testeC11.Location = new System.Drawing.Point(0, 0);
            this.testeC11.LookAndFeel.UseDefaultLookAndFeel = false;
            this.testeC11.Name = "testeC11";
            this.testeC11.Size = new System.Drawing.Size(376, 324);
            this.ToolTipController_F.SetSuperTip(this.testeC11, null);
            this.testeC11.TabIndex = 0;
            this.testeC11.Titulo = null;
            this.testeC11.Layout += new System.Windows.Forms.LayoutEventHandler(this.testeC11_Layout);
            this.testeC11.Resize += new System.EventHandler(this.testeC11_Resize);
            // 
            // dataSet11
            // 
            this.dataSet11.DataSetName = "DataSet1";
            this.dataSet11.EnforceConstraints = false;
            this.dataSet11.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // Principal
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoSize = true;
            this.ClientSize = new System.Drawing.Size(782, 556);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.HelpButton = true;
            this.Location = new System.Drawing.Point(1, 0);
            moduleInfo12.Grupo = 0;
            moduleInfo12.ImagemG = null;
            moduleInfo12.ImagemP = global::Teste.Properties.Resources.DOOR_1;
            moduleInfo12.ModuleType = typeof(Teste.CamposTab);
            moduleInfo12.ModuleTypestr = "Controle1";
            moduleInfo12.Name = "Teste Tab";
            moduleInfo13.Grupo = 0;
            moduleInfo13.ImagemG = null;
            moduleInfo13.ImagemP = global::Teste.Properties.Resources.house2;
            moduleInfo13.ModuleType = typeof(Teste.PesqTeste);
            moduleInfo13.ModuleTypestr = "Controle1";
            moduleInfo13.Name = "Pesq Teste";
            moduleInfo14.Grupo = 1;
            moduleInfo14.ImagemG = null;
            moduleInfo14.ImagemP = null;
            moduleInfo14.ModuleType = typeof(ComponentesTeste.TesteC1);
            moduleInfo14.ModuleTypestr = "Controle2";
            moduleInfo14.Name = "c1";
            moduleInfo15.Grupo = 1;
            moduleInfo15.ImagemG = null;
            moduleInfo15.ImagemP = null;
            moduleInfo15.ModuleType = typeof(ComponentesTeste.TesteC2);
            moduleInfo15.ModuleTypestr = "PesqTeste";
            moduleInfo15.Name = "C2";
            moduleInfo16.Grupo = 0;
            moduleInfo16.ImagemG = null;
            moduleInfo16.ImagemP = null;
            moduleInfo16.ModuleType = typeof(Teste.TesteCampos);
            moduleInfo16.ModuleTypestr = null;
            moduleInfo16.Name = "teste campos";
            moduleInfo17.Grupo = 0;
            moduleInfo17.ImagemG = null;
            moduleInfo17.ImagemP = null;
            moduleInfo17.ModuleType = typeof(Teste.PesqTeste);
            moduleInfo17.ModuleTypestr = null;
            moduleInfo17.Name = "PesqTeste";
            moduleInfo18.Grupo = 0;
            moduleInfo18.ImagemG = null;
            moduleInfo18.ImagemP = null;
            moduleInfo18.ModuleType = typeof(Teste.TesteGridSimples);
            moduleInfo18.ModuleTypestr = null;
            moduleInfo18.Name = "GrupoSimples";
            moduleInfo19.Grupo = 0;
            moduleInfo19.ImagemG = null;
            moduleInfo19.ImagemP = null;
            moduleInfo19.ModuleType = typeof(Teste.TesteBind);
            moduleInfo19.ModuleTypestr = null;
            moduleInfo19.Name = "Teste Binding";
            moduleInfo20.Grupo = 0;
            moduleInfo20.ImagemG = null;
            moduleInfo20.ImagemP = null;
            moduleInfo20.ModuleType = typeof(Teste.TesteFK2);
            moduleInfo20.ModuleTypestr = null;
            moduleInfo20.Name = "fk2";
            moduleInfo21.Grupo = 0;
            moduleInfo21.ImagemG = null;
            moduleInfo21.ImagemP = null;
            moduleInfo21.ModuleType = typeof(Teste.tete);
            moduleInfo21.ModuleTypestr = null;
            moduleInfo21.Name = "tete";
            moduleInfo22.Grupo = 0;
            moduleInfo22.ImagemG = null;
            moduleInfo22.ImagemP = null;
            moduleInfo22.ModuleType = typeof(Teste.GridCondominio);
            moduleInfo22.ModuleTypestr = null;
            moduleInfo22.Name = "gridcondominios";
            this.ModuleInfoCollection.AddRange(new CompontesBasicos.ModuleInfo[] {
            moduleInfo12,
            moduleInfo13,
            moduleInfo14,
            moduleInfo15,
            moduleInfo16,
            moduleInfo17,
            moduleInfo18,
            moduleInfo19,
            moduleInfo20,
            moduleInfo21,
            moduleInfo22});
            this.Name = "Principal";
            this.SizeGripStyle = System.Windows.Forms.SizeGripStyle.Show;
            this.ToolTipController_F.SetSuperTip(this, null);
            this.WindowState = System.Windows.Forms.FormWindowState.Normal;
            ((System.ComponentModel.ISupportInitialize)(this.PictureEdit_F.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.NavBarControl_F)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.BarAndDockingController_F)).EndInit();
            this.DockPanel_FContainer.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.TabControl_F)).EndInit();
            this.TabControl_F.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.BarManager_F)).EndInit();
            this.DockPanel_F.ResumeLayout(false);
            this.xtraTabPage1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.memoEdit1.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEdit1.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet11)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraNavBar.NavBarGroup navBarGroup1;
        private DevExpress.XtraNavBar.NavBarGroup navBarGroup2;
        private DevExpress.XtraNavBar.NavBarItem navBarItem1;
        private DevExpress.XtraNavBar.NavBarGroup navBarGroup3;
        private DevExpress.XtraNavBar.NavBarItem navBarItem3;
        private DevExpress.XtraNavBar.NavBarItem navBarItem2;
        private DevExpress.XtraNavBar.NavBarItem navBarItem4;
        private System.Windows.Forms.TextBox textBox1;
        private DevExpress.XtraTab.XtraTabPage xtraTabPage1;
        private ComponentesTeste.TesteC1 testeC11;
        private System.Windows.Forms.Button button1;
        private DataSet1 dataSet11;
        private System.Windows.Forms.Button button2;
        private DevExpress.XtraEditors.DateEdit dateEdit1;
        private DevExpress.XtraEditors.MemoEdit memoEdit1;

    }
}
