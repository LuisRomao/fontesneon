namespace Teste
{
    partial class FormComC2
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FormComC2));
            this.controle21 = new Teste.Controle2();
            this.SuspendLayout();
            // 
            // controle21
            // 
            this.controle21.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.controle21.Icone = null;
            this.controle21.ImagemG = ((System.Drawing.Image)(resources.GetObject("controle21.ImagemG")));
            this.controle21.ImagemP = ((System.Drawing.Image)(resources.GetObject("controle21.ImagemP")));
            this.controle21.Location = new System.Drawing.Point(37, 29);
            this.controle21.LookAndFeel.UseDefaultLookAndFeel = false;
            this.controle21.Name = "controle21";
            this.controle21.Size = new System.Drawing.Size(568, 289);
            this.controle21.TabIndex = 0;
            // 
            // FormComC2
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(615, 329);
            this.Controls.Add(this.controle21);
            this.Name = "FormComC2";
            this.Text = "FormComC2";
            this.ResumeLayout(false);

        }

        #endregion

        public Controle2 controle21;

    }
}