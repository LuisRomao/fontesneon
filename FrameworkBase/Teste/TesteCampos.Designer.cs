namespace Teste
{
    partial class TesteCampos
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.Windows.Forms.Label cONCodigoLabel;
            System.Windows.Forms.Label cONNomeLabel;
            System.Windows.Forms.Label cONCidadeLabel;
            System.Windows.Forms.Label uSULabel;
            System.Windows.Forms.Label uSUNomeLabel;
            System.Windows.Forms.Label uSULoginLabel;
            System.Windows.Forms.Label uSUSenhaLabel;
            System.Windows.Forms.Label uSUEnderecoLabel;
            System.Windows.Forms.Label uSU_PERLabel;
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(TesteCampos));
            this.dataSet1 = new Teste.DataSet1();
            this.cONCodigoTextEdit = new DevExpress.XtraEditors.TextEdit();
            this.cONNomeTextEdit = new DevExpress.XtraEditors.TextEdit();
            this.cONCidadeTextEdit = new DevExpress.XtraEditors.TextEdit();
            this.button1 = new System.Windows.Forms.Button();
            this.uSUARIOSTableAdapter = new Teste.DataSet1TableAdapters.USUARIOSTableAdapter();
            this.uSUSpinEdit = new DevExpress.XtraEditors.SpinEdit();
            this.uSUARIOSBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.uSUNomeTextEdit = new DevExpress.XtraEditors.TextEdit();
            this.button2 = new System.Windows.Forms.Button();
            this.uSULoginTextEdit = new DevExpress.XtraEditors.TextEdit();
            this.uSUSenhaTextEdit = new DevExpress.XtraEditors.TextEdit();
            this.uSUEnderecoTextEdit = new DevExpress.XtraEditors.TextEdit();
            this.uSU_PERSpinEdit = new DevExpress.XtraEditors.SpinEdit();
            cONCodigoLabel = new System.Windows.Forms.Label();
            cONNomeLabel = new System.Windows.Forms.Label();
            cONCidadeLabel = new System.Windows.Forms.Label();
            uSULabel = new System.Windows.Forms.Label();
            uSUNomeLabel = new System.Windows.Forms.Label();
            uSULoginLabel = new System.Windows.Forms.Label();
            uSUSenhaLabel = new System.Windows.Forms.Label();
            uSUEnderecoLabel = new System.Windows.Forms.Label();
            uSU_PERLabel = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.BarManager_F)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.BarAndDockingController_F)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.BindingSource_F)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.StyleController_F)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cONCodigoTextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cONNomeTextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cONCidadeTextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uSUSpinEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uSUARIOSBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uSUNomeTextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uSULoginTextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uSUSenhaTextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uSUEnderecoTextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uSU_PERSpinEdit.Properties)).BeginInit();
            this.SuspendLayout();
            // 
            // barStatus_F
            // 
            this.barStatus_F.OptionsBar.AllowQuickCustomization = false;
            this.barStatus_F.OptionsBar.DisableClose = true;
            this.barStatus_F.OptionsBar.DisableCustomization = true;
            this.barStatus_F.OptionsBar.DrawDragBorder = false;
            this.barStatus_F.OptionsBar.DrawSizeGrip = true;
            this.barStatus_F.OptionsBar.UseWholeRow = true;
            this.barStatus_F.Visible = false;
            // 
            // barComponente_F
            // 
            this.barComponente_F.OptionsBar.AllowQuickCustomization = false;
            this.barComponente_F.OptionsBar.DisableClose = true;
            this.barComponente_F.OptionsBar.DisableCustomization = true;
            this.barComponente_F.OptionsBar.DrawDragBorder = false;
            this.barComponente_F.OptionsBar.UseWholeRow = true;
            // 
            // btnFechar_F
            // 
            this.btnFechar_F.Visibility = DevExpress.XtraBars.BarItemVisibility.Never;
            this.btnFechar_F.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.btnFechar_F_ItemClick_1);
            // 
            // BarAndDockingController_F
            // 
            this.BarAndDockingController_F.PropertiesBar.AllowLinkLighting = false;
            // 
            // ToolTipController_F
            // 
            this.ToolTipController_F.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(222)))), ((int)(((byte)(237)))), ((int)(((byte)(250)))));
            this.ToolTipController_F.Appearance.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(20)))), ((int)(((byte)(69)))), ((int)(((byte)(141)))));
            this.ToolTipController_F.Appearance.Options.UseBackColor = true;
            this.ToolTipController_F.Appearance.Options.UseFont = true;
            this.ToolTipController_F.Appearance.Options.UseForeColor = true;
            this.ToolTipController_F.Appearance.Options.UseTextOptions = true;
            this.ToolTipController_F.Appearance.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            // 
            // StyleController_F
            // 
            this.StyleController_F.AppearanceDisabled.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.StyleController_F.AppearanceDisabled.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.StyleController_F.AppearanceDisabled.Options.UseBackColor = true;
            this.StyleController_F.AppearanceDisabled.Options.UseForeColor = true;
            this.StyleController_F.AppearanceFocused.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(253)))), ((int)(((byte)(253)))), ((int)(((byte)(181)))));
            this.StyleController_F.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
            this.StyleController_F.AppearanceFocused.Options.UseBackColor = true;
            this.StyleController_F.AppearanceFocused.Options.UseFont = true;
            // 
            // DefaultLookAndFeel_F
            // 
            this.DefaultLookAndFeel_F.LookAndFeel.SkinName = "The Asphalt World";
            // 
            // cONCodigoLabel
            // 
            cONCodigoLabel.AutoSize = true;
            cONCodigoLabel.Location = new System.Drawing.Point(19, 20);
            cONCodigoLabel.Name = "cONCodigoLabel";
            cONCodigoLabel.Size = new System.Drawing.Size(66, 13);
            this.ToolTipController_F.SetSuperTip(cONCodigoLabel, null);
            cONCodigoLabel.TabIndex = 0;
            cONCodigoLabel.Text = "CONCodigo:";
            // 
            // cONNomeLabel
            // 
            cONNomeLabel.AutoSize = true;
            cONNomeLabel.Location = new System.Drawing.Point(247, 19);
            cONNomeLabel.Name = "cONNomeLabel";
            cONNomeLabel.Size = new System.Drawing.Size(61, 13);
            this.ToolTipController_F.SetSuperTip(cONNomeLabel, null);
            cONNomeLabel.TabIndex = 2;
            cONNomeLabel.Text = "CONNome:";
            // 
            // cONCidadeLabel
            // 
            cONCidadeLabel.AutoSize = true;
            cONCidadeLabel.Location = new System.Drawing.Point(19, 55);
            cONCidadeLabel.Name = "cONCidadeLabel";
            cONCidadeLabel.Size = new System.Drawing.Size(66, 13);
            this.ToolTipController_F.SetSuperTip(cONCidadeLabel, null);
            cONCidadeLabel.TabIndex = 4;
            cONCidadeLabel.Text = "CONCidade:";
            // 
            // uSULabel
            // 
            uSULabel.AutoSize = true;
            uSULabel.Location = new System.Drawing.Point(81, 55);
            uSULabel.Name = "uSULabel";
            uSULabel.Size = new System.Drawing.Size(31, 13);
            this.ToolTipController_F.SetSuperTip(uSULabel, null);
            uSULabel.TabIndex = 4;
            uSULabel.Text = "USU:";
            // 
            // uSUNomeLabel
            // 
            uSUNomeLabel.AutoSize = true;
            uSUNomeLabel.Location = new System.Drawing.Point(101, 244);
            uSUNomeLabel.Name = "uSUNomeLabel";
            uSUNomeLabel.Size = new System.Drawing.Size(58, 13);
            this.ToolTipController_F.SetSuperTip(uSUNomeLabel, null);
            uSUNomeLabel.TabIndex = 10;
            uSUNomeLabel.Text = "USUNome:";
            // 
            // uSULoginLabel
            // 
            uSULoginLabel.AutoSize = true;
            uSULoginLabel.Location = new System.Drawing.Point(82, 97);
            uSULoginLabel.Name = "uSULoginLabel";
            uSULoginLabel.Size = new System.Drawing.Size(56, 13);
            this.ToolTipController_F.SetSuperTip(uSULoginLabel, null);
            uSULoginLabel.TabIndex = 12;
            uSULoginLabel.Text = "USULogin:";
            // 
            // uSUSenhaLabel
            // 
            uSUSenhaLabel.AutoSize = true;
            uSUSenhaLabel.Location = new System.Drawing.Point(75, 131);
            uSUSenhaLabel.Name = "uSUSenhaLabel";
            uSUSenhaLabel.Size = new System.Drawing.Size(61, 13);
            this.ToolTipController_F.SetSuperTip(uSUSenhaLabel, null);
            uSUSenhaLabel.TabIndex = 13;
            uSUSenhaLabel.Text = "USUSenha:";
            // 
            // uSUEnderecoLabel
            // 
            uSUEnderecoLabel.AutoSize = true;
            uSUEnderecoLabel.Location = new System.Drawing.Point(73, 167);
            uSUEnderecoLabel.Name = "uSUEnderecoLabel";
            uSUEnderecoLabel.Size = new System.Drawing.Size(76, 13);
            this.ToolTipController_F.SetSuperTip(uSUEnderecoLabel, null);
            uSUEnderecoLabel.TabIndex = 14;
            uSUEnderecoLabel.Text = "USUEndereco:";
            // 
            // uSU_PERLabel
            // 
            uSU_PERLabel.AutoSize = true;
            uSU_PERLabel.Location = new System.Drawing.Point(323, 121);
            uSU_PERLabel.Name = "uSU_PERLabel";
            uSU_PERLabel.Size = new System.Drawing.Size(53, 13);
            this.ToolTipController_F.SetSuperTip(uSU_PERLabel, null);
            uSU_PERLabel.TabIndex = 15;
            uSU_PERLabel.Text = "USU PER:";
            // 
            // dataSet1
            // 
            this.dataSet1.DataSetName = "DataSet1";
            this.dataSet1.EnforceConstraints = false;
            this.dataSet1.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // cONCodigoTextEdit
            // 
            this.cONCodigoTextEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.BindingSource_F, "CONCodigo", true));
            this.cONCodigoTextEdit.Location = new System.Drawing.Point(91, 17);
            this.cONCodigoTextEdit.Name = "cONCodigoTextEdit";
            this.cONCodigoTextEdit.Size = new System.Drawing.Size(100, 20);
            this.cONCodigoTextEdit.TabIndex = 1;
            // 
            // cONNomeTextEdit
            // 
            this.cONNomeTextEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.BindingSource_F, "CONNome", true));
            this.cONNomeTextEdit.Location = new System.Drawing.Point(314, 16);
            this.cONNomeTextEdit.Name = "cONNomeTextEdit";
            this.cONNomeTextEdit.Size = new System.Drawing.Size(100, 20);
            this.cONNomeTextEdit.TabIndex = 3;
            // 
            // cONCidadeTextEdit
            // 
            this.cONCidadeTextEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.BindingSource_F, "CONCidade", true));
            this.cONCidadeTextEdit.Location = new System.Drawing.Point(91, 52);
            this.cONCidadeTextEdit.Name = "cONCidadeTextEdit";
            this.cONCidadeTextEdit.Size = new System.Drawing.Size(100, 20);
            this.cONCidadeTextEdit.TabIndex = 5;
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(347, 55);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(144, 36);
            this.ToolTipController_F.SetSuperTip(this.button1, null);
            this.button1.TabIndex = 6;
            this.button1.Text = "button1";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // uSUARIOSTableAdapter
            // 
            this.uSUARIOSTableAdapter.ClearBeforeFill = true;
            // 
            // uSUSpinEdit
            // 
            this.uSUSpinEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.uSUARIOSBindingSource, "USU", true));
            this.uSUSpinEdit.EditValue = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.uSUSpinEdit.Location = new System.Drawing.Point(120, 52);
            this.uSUSpinEdit.Name = "uSUSpinEdit";
            this.uSUSpinEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.uSUSpinEdit.Size = new System.Drawing.Size(100, 20);
            this.uSUSpinEdit.TabIndex = 5;
            // 
            // uSUARIOSBindingSource
            // 
            this.uSUARIOSBindingSource.DataMember = "USUARIOS";
            this.uSUARIOSBindingSource.DataSource = this.dataSet1;
            // 
            // uSUNomeTextEdit
            // 
            this.uSUNomeTextEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.uSUARIOSBindingSource, "USUNome", true));
            this.uSUNomeTextEdit.Location = new System.Drawing.Point(168, 241);
            this.uSUNomeTextEdit.Name = "uSUNomeTextEdit";
            this.uSUNomeTextEdit.Size = new System.Drawing.Size(100, 20);
            this.uSUNomeTextEdit.TabIndex = 11;
            // 
            // button2
            // 
            this.button2.Location = new System.Drawing.Point(558, 52);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(75, 20);
            this.ToolTipController_F.SetSuperTip(this.button2, null);
            this.button2.TabIndex = 12;
            this.button2.Text = "button2";
            this.button2.UseVisualStyleBackColor = true;
            this.button2.Click += new System.EventHandler(this.button2_Click);
            // 
            // uSULoginTextEdit
            // 
            this.uSULoginTextEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.uSUARIOSBindingSource, "USULogin", true));
            this.uSULoginTextEdit.Location = new System.Drawing.Point(144, 94);
            this.uSULoginTextEdit.Name = "uSULoginTextEdit";
            this.uSULoginTextEdit.Size = new System.Drawing.Size(100, 20);
            this.uSULoginTextEdit.TabIndex = 13;
            // 
            // uSUSenhaTextEdit
            // 
            this.uSUSenhaTextEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.uSUARIOSBindingSource, "USUSenha", true));
            this.uSUSenhaTextEdit.Location = new System.Drawing.Point(142, 128);
            this.uSUSenhaTextEdit.Name = "uSUSenhaTextEdit";
            this.uSUSenhaTextEdit.Size = new System.Drawing.Size(100, 20);
            this.uSUSenhaTextEdit.TabIndex = 14;
            // 
            // uSUEnderecoTextEdit
            // 
            this.uSUEnderecoTextEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.uSUARIOSBindingSource, "USUEndereco", true));
            this.uSUEnderecoTextEdit.Location = new System.Drawing.Point(155, 164);
            this.uSUEnderecoTextEdit.Name = "uSUEnderecoTextEdit";
            this.uSUEnderecoTextEdit.Size = new System.Drawing.Size(100, 20);
            this.uSUEnderecoTextEdit.TabIndex = 15;
            // 
            // uSU_PERSpinEdit
            // 
            this.uSU_PERSpinEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.uSUARIOSBindingSource, "USU_PER", true));
            this.uSU_PERSpinEdit.EditValue = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.uSU_PERSpinEdit.Location = new System.Drawing.Point(382, 118);
            this.uSU_PERSpinEdit.Name = "uSU_PERSpinEdit";
            this.uSU_PERSpinEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.uSU_PERSpinEdit.Size = new System.Drawing.Size(100, 20);
            this.uSU_PERSpinEdit.TabIndex = 16;
            // 
            // TesteCampos
            // 
            this.BindingSourcePrincipal = this.uSUARIOSBindingSource;
            this.Controls.Add(uSU_PERLabel);
            this.Controls.Add(this.uSU_PERSpinEdit);
            this.Controls.Add(uSUEnderecoLabel);
            this.Controls.Add(this.uSUEnderecoTextEdit);
            this.Controls.Add(uSUSenhaLabel);
            this.Controls.Add(this.uSUSenhaTextEdit);
            this.Controls.Add(uSULoginLabel);
            this.Controls.Add(this.uSULoginTextEdit);
            this.Controls.Add(this.button2);
            this.Controls.Add(uSUNomeLabel);
            this.Controls.Add(this.uSUNomeTextEdit);
            this.Controls.Add(uSULabel);
            this.Controls.Add(this.uSUSpinEdit);
            this.Cursor = System.Windows.Forms.Cursors.Default;
            this.ImagemG = ((System.Drawing.Image)(resources.GetObject("$this.ImagemG")));
            this.ImagemP = ((System.Drawing.Image)(resources.GetObject("$this.ImagemP")));
            this.LookAndFeel.UseDefaultLookAndFeel = false;
            this.Name = "TesteCampos";
            this.Size = new System.Drawing.Size(684, 338);
            this.ToolTipController_F.SetSuperTip(this, null);
            this.Titulo = "Titulo campos";
            this.Load += new System.EventHandler(this.TesteCampos_Load);
            this.Controls.SetChildIndex(this.uSUSpinEdit, 0);
            this.Controls.SetChildIndex(uSULabel, 0);
            this.Controls.SetChildIndex(this.uSUNomeTextEdit, 0);
            this.Controls.SetChildIndex(uSUNomeLabel, 0);
            this.Controls.SetChildIndex(this.button2, 0);
            this.Controls.SetChildIndex(this.uSULoginTextEdit, 0);
            this.Controls.SetChildIndex(uSULoginLabel, 0);
            this.Controls.SetChildIndex(this.uSUSenhaTextEdit, 0);
            this.Controls.SetChildIndex(uSUSenhaLabel, 0);
            this.Controls.SetChildIndex(this.uSUEnderecoTextEdit, 0);
            this.Controls.SetChildIndex(uSUEnderecoLabel, 0);
            this.Controls.SetChildIndex(this.uSU_PERSpinEdit, 0);
            this.Controls.SetChildIndex(uSU_PERLabel, 0);
            ((System.ComponentModel.ISupportInitialize)(this.BarManager_F)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.BarAndDockingController_F)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.BindingSource_F)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.StyleController_F)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cONCodigoTextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cONNomeTextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cONCidadeTextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uSUSpinEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uSUARIOSBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uSUNomeTextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uSULoginTextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uSUSenhaTextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uSUEnderecoTextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uSU_PERSpinEdit.Properties)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private DevExpress.XtraEditors.TextEdit cONCodigoTextEdit;
        private DataSet1 dataSet1;
        private DevExpress.XtraEditors.TextEdit cONCidadeTextEdit;
        private DevExpress.XtraEditors.TextEdit cONNomeTextEdit;
        private System.Windows.Forms.Button button1;
        private Teste.DataSet1TableAdapters.USUARIOSTableAdapter uSUARIOSTableAdapter;
        private DevExpress.XtraEditors.SpinEdit uSUSpinEdit;
        private DevExpress.XtraEditors.TextEdit uSUNomeTextEdit;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.BindingSource uSUARIOSBindingSource;
        private DevExpress.XtraEditors.TextEdit uSULoginTextEdit;
        private DevExpress.XtraEditors.TextEdit uSUSenhaTextEdit;
        private DevExpress.XtraEditors.TextEdit uSUEnderecoTextEdit;
        private DevExpress.XtraEditors.SpinEdit uSU_PERSpinEdit;
    }
}
