//MR - 06/11/2014 15:00 -           - Novo construtor IniciaGravacao para arquivos de Tributos (Altera��es indicadas por *** MRC - INICIO - TRIBUTOS (06/11/2014 15:00) ***)

using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.Collections;

namespace ArquivosTXT
{
    #region Enumeracoes

    /// <summary>
    /// Tipos dos campos
    /// </summary>
    public enum TiposDeCampo
    {
        /// <summary>
        /// Campo a ser carregado dos dados de entrada
        /// </summary>
        normal,
        /// <summary>
        /// Data de gravacao
        /// </summary>
        AutoData,
        /// <summary>
        /// Numero da linha
        /// </summary>
        Sequencial,
        /// <summary>
        /// Numero de registros detalhe
        /// </summary>
        NDetalhe,
        /// <summary>
        /// Somatorio do campo Valor
        /// </summary>
        Somatorio
    }

    /// <summary>
    /// Classe de mapeamento
    /// </summary>
    public class UnidadeMapa
    {
        /// <summary>
        /// Tipo do campo
        /// </summary>
        public TiposDeCampo Tipo;
        /// <summary>
        /// inicio do campo
        /// </summary>
        public int inicio;

        /// <summary>
        /// tamanho do campo
        /// </summary>
        public int tamanho;

        /// <summary>
        /// Descritor do campo
        /// </summary>
        public DataColumn Campo;

        /// <summary>
        /// 
        /// </summary>
        public string NomeCampo;

        /// <summary>
        /// 
        /// </summary>
        public Type TipoDeDado;

        /// <summary>
        /// 
        /// </summary>
        public object Padrao;

        /// <summary>
        /// Remove todos os acentos
        /// </summary>
        public bool removeacentos = false;
        /// <summary>
        /// 
        /// </summary>
        public bool soAZ09 = false;
        /// <summary>
        /// substitui tudo que nao for az AZ 09 por espa�o
        /// </summary>
        public bool limpaAZ09 = false;
        /// <summary>
        /// 
        /// </summary>
        public int EliminaRepique = 0;
        /// <summary>
        /// Usa o trim. S� � aplicavel em arquivo delimitado.
        /// </summary>
        public bool TamanhoVariavel = false;
        /// <summary>
        /// 
        /// </summary>
        public bool TamanhoVariavelTeto = false;
        /// <summary>
        /// 
        /// </summary>
        /// <param name="valor"></param>
        public void SetaTamanhoVariavelTeto(bool valor)
        {
            TamanhoVariavel = TamanhoVariavelTeto = valor;
        }
        /// <summary>
        /// 
        /// </summary>
        public FormatoData Formato = FormatoData.Padrao;

        /// <summary>
        /// Construtor
        /// </summary>
        /// <param name="_inicio">inicio</param>
        /// <param name="_tamanho">tamanho</param>
        /// <param name="_Campo">Coluna</param>
        public UnidadeMapa(int _inicio, int _tamanho, DataColumn _Campo)
        {
            inicio = _inicio;
            tamanho = _tamanho;
            Campo = _Campo;
            TipoDeDado = Campo.DataType;
            Tipo = TiposDeCampo.normal;
            this.NomeCampo = Campo.ColumnName;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="_inicio"></param>
        /// <param name="_tamanho"></param>
        /// <param name="NomeCampo"></param>
        public UnidadeMapa(int _inicio, int _tamanho, string NomeCampo)
        {
            inicio = _inicio;
            tamanho = _tamanho;
            Campo = null;
            Tipo = TiposDeCampo.normal;
            this.NomeCampo = NomeCampo;
        }

        /// <summary>
        /// Construtor
        /// </summary>
        /// <param name="_inicio">inicio</param>
        /// <param name="_tamanho">tamanho</param>
        /// <param name="_Campo">Coluna</param>
        /// <param name="_TipoCampo">Tipo do campo, define o funcionamento</param>
        /// <param name="_Padrao">Valor default</param>
        public UnidadeMapa(int _inicio, int _tamanho, DataColumn _Campo, TiposDeCampo _TipoCampo, object _Padrao)
        {
            inicio = _inicio;
            tamanho = _tamanho;
            Campo = _Campo;
            if (Campo != null)
                TipoDeDado = Campo.DataType;
            Padrao = _Padrao;
            Tipo = _TipoCampo;
            this.NomeCampo = Campo.ColumnName;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="_inicio"></param>
        /// <param name="_tamanho"></param>
        /// <param name="NomeCampo"></param>
        /// <param name="_TipoCampo"></param>
        /// <param name="_Padrao"></param>
        public UnidadeMapa(int _inicio, int _tamanho, string NomeCampo, TiposDeCampo _TipoCampo, object _Padrao)
        {
            inicio = _inicio;
            tamanho = _tamanho;
            Campo = null;            
            Padrao = _Padrao;
            if(Padrao != null)
                TipoDeDado = Padrao.GetType();
            Tipo = _TipoCampo;
            this.NomeCampo = NomeCampo;
        }
    }

    /// <summary>
    /// Tipos de arquivo
    /// </summary>
    public enum ModelosTXT
    {
        /// <summary>
        /// arquivo delimitado
        /// </summary>
        delimitado,
        /// <summary>
        /// arquivo mapeado
        /// </summary>
        layout,
        /// <summary>
        /// Tabela
        /// </summary>
        tabela
    }

    /// <summary>
    /// Identificaocao do mapeamento
    /// </summary>
    public enum TipoMapa
    {
        /// <summary>
        /// header
        /// </summary>
        cabecalho,
        /// <summary>
        /// detalhe
        /// </summary>
        detalhe,
        /// <summary>
        /// trailler
        /// </summary>
        rodape,
        /// <summary>
        /// 
        /// </summary>
        detalhe2
    }

    /// <summary>
    /// Formato da data
    /// </summary>
    public enum FormatoData
    {
        /// <summary>
        /// Padrao
        /// </summary>
        Padrao,
        /// <summary>
        /// 
        /// </summary>
        ddMMyy,
        /// <summary>
        /// 
        /// </summary>
        dd_MM_YYYY,
        /// <summary>
        /// 
        /// </summary>
        yyMMdd,
        /// <summary>
        /// 
        /// </summary>
        yyyyMMdd,
        /// <summary>
        /// 
        /// </summary>
        yyyyMM,
        /// <summary>
        /// 
        /// </summary>
        ddMMyyyy,
        /// <summary>
        /// 
        /// </summary>
        yyyy_MM_dd,
        /// <summary>
        /// 
        /// </summary>
        MMddyyyy
    }

    /// <summary>
    /// 
    /// </summary>
    public enum TipoMapeamento
    {
        /// <summary>
        /// 
        /// </summary>
        automaticoTabela,
        /// <summary>
        /// 
        /// </summary>
        automaticoPrimeiraLinha,
        /// <summary>
        /// 
        /// </summary>
        automaticoPrimeiraLinhaCamposNaoObrigatorios,
        /// <summary>
        /// 
        /// </summary>
        manual
    }

    /// <summary>
    /// 
    /// </summary>
    public enum ComportamentoCamposNaoEncontrados
    {
        /// <summary>
        /// 
        /// </summary>
        NaoMapear,
        /// <summary>
        /// 
        /// </summary>
        NaoMapearRegistrarErro,
        /// <summary>
        /// 
        /// </summary>
        AbortarSeCampoObrigatorio,
        /// <summary>
        /// 
        /// </summary>
        Abortar
    }

    #endregion

    /// <summary>
    /// 
    /// </summary>
    public class cTabelaTXT
    {
        private Encoding _Encoding;
        /// <summary>
        /// Formato da data
        /// </summary>
        public FormatoData Formatodata = FormatoData.ddMMyy;

        /// <summary>
        /// 
        /// </summary>
        public bool DefaultTamanhoVariavel = false;

        /// <summary>
        /// 
        /// </summary>
        public string PontoDecima = "";
        /// <summary>
        /// Remove todos os acentos
        /// </summary>
        public bool defaultremoveacentos = false;
        /// <summary>
        /// Remove tudo que nao for AZ09
        /// </summary>
        public bool defaultsoAZ09 = false;
        /// <summary>
        /// Substitui tudo que nao fora az AZ 09 espa�o
        /// </summary>
        public bool defaultlimpaAZ09 = false;
        private string strFormatoData {
            get {
                switch (Formatodata)
                {                                            
                    case FormatoData.dd_MM_YYYY:
                        return "dd/MM/yyyy";
                    case FormatoData.yyMMdd:
                        return "yyMMdd";
                    case FormatoData.yyyyMMdd:
                        return "yyyyMMdd";
                    case FormatoData.yyyyMM:
                        return "yyyyMM";
                    case FormatoData.ddMMyyyy:
                        return "ddMMyyyy";
                    case FormatoData.MMddyyyy:
                        return "MMddyyyy";
                    case FormatoData.yyyy_MM_dd:
                        return "yyyy-MM-dd";
                    case FormatoData.Padrao:
                    case FormatoData.ddMMyy:
                    default :
                        return "ddMMyy";
                }
            }
        }
        /// <summary>
        /// 
        /// </summary>
        public Encoding Encoding{
            get
            {
                if (_Encoding == null)
                    _Encoding = Encoding.GetEncoding(1252);
                    //_Encoding = Encoding.UTF8;
                return _Encoding;
            }
            set{
                _Encoding = value;
            }
        }        
        /// <summary>
        /// 
        /// </summary>
        protected internal ModelosTXT Modelo;
        /// <summary>
        /// 
        /// </summary>
        protected DataTable DataTable;
        /// <summary>
        /// 
        /// </summary>
        public DataTable DataTable2;
        /// <summary>
        /// 
        /// </summary>
        public DataRow rowCabecalho;
        /// <summary>
        /// 
        /// </summary>
        public DataRow rowRodape;
        /// <summary>
        /// 
        /// </summary>
        public string CampoSomatorio;
        /// <summary>
        /// 
        /// </summary>
        public ComportamentoCamposNaoEncontrados ComportamentoCamposNaoEncontradosPadrao = ComportamentoCamposNaoEncontrados.Abortar;

        /// <summary>
        /// Caractere separador para formato delimitado
        /// </summary>
        public char[] Separador;
        /// <summary>
        /// 
        /// </summary>
        public bool GravaSeparadorFinal = true;
        private System.IO.StreamReader entrada;
        /// <summary>
        /// 
        /// </summary>
        public System.IO.Stream StreamArquivo; 
        private System.IO.StreamReader Entrada {
            get {
                if (entrada == null) 
                {
                    if (NomeArquivo == null)
                    {
                        if (StreamArquivo != null)
                        {
                            TamanhoArquivo = StreamArquivo.Length;
                            //bytesLidos = 0;
                            entrada = new System.IO.StreamReader(StreamArquivo,Encoding);
                        }
                        else
                            return null;
                    }
                    else
                    {
                        TamanhoArquivo = new System.IO.FileInfo(NomeArquivo).Length;
                        //bytesLidos = 0;
                        entrada = new System.IO.StreamReader(NomeArquivo, Encoding);
                    };
                };                
                return entrada;
            }          
        }
        private System.IO.StreamWriter Saida;
        private TipoMapeamento tipoMapeamento;
        /// <summary>
        /// 
        /// </summary>
        public string NomeArquivo;
        private int Sequencial=1;
        private int NDetalhe = 0;
        private decimal Somatorio = 0;
        private SortedList _Mapas;
        /// <summary>
        /// 
        /// </summary>
        /// <param name="Nome"></param>
        /// <returns></returns>
        public Mapa Mapas(string Nome) {
            if (_Mapas == null)
                _Mapas = new SortedList();
            if (_Mapas[Nome] == null) 
                _Mapas.Add(Nome, new Mapa(Nome));
            return (Mapa)_Mapas[Nome];            
        }

        /// <summary>
        /// Grava o nome dos campos na primeira linha em arquivos delimitados
        /// </summary>
        public bool GravarCamposNaPrimeiraLinha;
        //private Mapa Mapeamento;
        //private Mapa Mapeamento2;
        //private Mapa MapeamentoCab;
        //private Mapa MapeamentoRod;
        /// <summary>
        /// 
        /// </summary>
        public int TamanhoDoRegistro = 0;
        /// <summary>
        /// 
        /// </summary>
        public string UltimoErro = "";
        /// <summary>
        /// 
        /// </summary>
        public long TamanhoArquivo;

        /*
        /// <summary>
        /// 
        /// </summary>
        [Obsolete("O valor desta propriedade n�o � confi�vel. Utilizar PreviaProximaLinha ou BytesProximaLinha para checar se o arquivo terminou")]
        public long bytesLidos;
        */

        private ArrayList _CamposNaoEncontrados;
        /// <summary>
        /// 
        /// </summary>
        public ArrayList CamposNaoEncontrados {
            get {
                if (_CamposNaoEncontrados == null)
                    _CamposNaoEncontrados = new ArrayList();
                return _CamposNaoEncontrados;
            }
        }

        private static string Acentos(string Valor)
        {
            Valor = Valor.Replace('�', 'a');
            Valor = Valor.Replace('�', 'a');
            Valor = Valor.Replace('�', 'a');
            Valor = Valor.Replace('�', 'a');
            Valor = Valor.Replace('�', 'e');
            Valor = Valor.Replace('�', 'e');
            Valor = Valor.Replace('�', 'i');
            Valor = Valor.Replace('�', 'i');
            Valor = Valor.Replace('�', 'o');
            Valor = Valor.Replace('�', 'o');
            Valor = Valor.Replace('�', 'o');
            Valor = Valor.Replace('�', 'u');
            Valor = Valor.Replace('�', 'c');

            Valor = Valor.Replace('�', 'A');
            Valor = Valor.Replace('�', 'A');
            Valor = Valor.Replace('�', 'A');
            Valor = Valor.Replace('�', 'A');
            Valor = Valor.Replace('�', 'E');
            Valor = Valor.Replace('�', 'E');
            Valor = Valor.Replace('�', 'I');
            Valor = Valor.Replace('�', 'I');
            Valor = Valor.Replace('�', 'O');
            Valor = Valor.Replace('�', 'O');
            Valor = Valor.Replace('�', 'O');
            Valor = Valor.Replace('�', 'U');
            Valor = Valor.Replace('�', 'C');

            return Valor;
        }

        private static bool isaz09(char c)
        {
            if (('a' <= c) && (c <= 'z'))
                return true;
            if (('A' <= c) && (c <= 'Z'))
                return true;
            if (('0' <= c) && (c <= '9'))
                return true;            
            return false;
        }

        private static List<char> PontSt;

        private static bool isaz09Pontuacao(char c)
        {
            if (('a' <= c) && (c <= 'z'))
                return true;
            if (('A' <= c) && (c <= 'Z'))
                return true;
            if (('0' <= c) && (c <= '9'))
                return true;
            if(PontSt == null)
                PontSt = new List<char> { '/', '\\', ',', '.', '-', ':', ';', ' ', '(', ')', '\'', '"', '&', '#', '$', '+', '*', '%', '<' , '>','|','{','}'};
            if(PontSt.Contains(c))
                return true;
            return false;
        }

        private static string paraAZ09(string Valor, int EliminaRepique)
        {
            string retorno = "";
            //bool branco = true;
            char Anterior = ' ';
            int repique = 0;
            foreach (char c in Valor)
                if (isaz09(c) || char.IsWhiteSpace(c))
                {
                    if ((!char.IsWhiteSpace(Anterior)) || (!char.IsWhiteSpace(c)))
                        if ((Anterior != c) || ((repique + 1) < EliminaRepique))
                        {
                            retorno += c;
                            if (Anterior == c)
                                repique++;
                            else
                                repique = 1;
                            Anterior = c;
                        }
                }
            return retorno;
        }

        /// <summary>
        /// mantem somente az AZ 09 e espa�os e pontua��o
        /// </summary>
        /// <param name="Valor"></param>        
        /// <returns></returns>
        private static string limpaAZ09(string Valor)
        {
            string retorno = "";                                    
            foreach (char c in Valor)
                if (isaz09Pontuacao(c))                
                    retorno += c;                    
                else
                    retorno += ' ';                    
            return retorno;
        }

        private void MapearPelaTabela()
        {
            int pos = 1;
            foreach (DataColumn coluna in DataTable.Columns)
            {
                RegistraMapa(coluna, pos, pos);
                pos++;
            }
        }

        #region Construtores
        /// <summary>
        /// Contrutor
        /// </summary>
        /// <param name="_DataTable">Tabela que ir� receber os dados</param>
        /// <param name="Mod">Tipo de arquivo</param>
        public cTabelaTXT(DataTable _DataTable, ModelosTXT Mod)
        {
            Modelo = Mod;
            DataTable = _DataTable;
            tipoMapeamento = TipoMapeamento.manual;
        }

        
        /// <summary>
        /// 
        /// </summary>
        /// <param name="_DataTable"></param>
        /// <param name="Mod"></param>
        /// <param name="TM"></param>
        public cTabelaTXT(DataTable _DataTable, ModelosTXT Mod,TipoMapeamento TM)
        {
            Modelo = Mod;
            DataTable = _DataTable;
            tipoMapeamento = TM;
            if(Mod == ModelosTXT.delimitado)
                switch(TM)
                {
                    case TipoMapeamento.automaticoTabela:
                        MapearPelaTabela();
                        break;
                    case TipoMapeamento.automaticoPrimeiraLinhaCamposNaoObrigatorios:
                        ComportamentoCamposNaoEncontradosPadrao = ComportamentoCamposNaoEncontrados.AbortarSeCampoObrigatorio;
                        tipoMapeamento = TipoMapeamento.automaticoPrimeiraLinha;
                        break;

                    //case TipoMapeamento.automaticoPrimeiraLinha:
                    //    MapeiaPelaPrimeiraLinha(NomeArquivo);
                    //    break;
                }
            
                
        }

        /// <summary>
        /// Construtor para uso em arquivos com diferentes tipos de registros, nao ser� carregado em tabela
        /// </summary>
        /// <param name="Mod">Modelo do arquivo</param>
        public cTabelaTXT(ModelosTXT Mod)
        {
            Modelo = Mod;           
        }

        /// <summary>
        /// Contrutor padr�o N�O USAR. Feito so para herdar no RemessaCC
        /// </summary>        
        public cTabelaTXT()
        {
        } 
        #endregion

        /// <summary>
        /// Le uma linha do arquivo e a envia sem interpreta��o
        /// </summary>
        /// <remarks>Se outro arquivo estava em processo ele � liberado
        /// Se o nome do arquivo for o mesmo ou nulo ent�o � lida a proxima linha</remarks>
        /// <param name="arquivo">Nome do aquivo</param>
        /// <returns>Linha lida</returns>
        public string LeCabecalho(string arquivo) {
            if (entrada != null) 
            { 
                //if((arquivo != "") && (arquivo != NomeArquivo))
                if (arquivo != "")
                {
                    //limpa a entrada que sera carregada novamente na letura da propriedade Endarada
                    entrada.Close();
                    entrada.Dispose();
                    entrada = null;
                };
            };
            NomeArquivo = arquivo;
            string Lido = "";
            while (Lido == "")
                Lido = ProximaLinha();
            return Lido;
        }

        /// <summary>
        /// Fecha o arquivo de entrada
        /// </summary>
        public void LiberaArquivo()
        {
            if (entrada != null)
            {
                try
                {
                    entrada.Close();
                    entrada.Dispose();
                }
                finally
                {
                    entrada = null;
                }
            }
        }

        private string previaProximaLinha;

        /// <summary>
        /// Checa a pr�xima linha sem alterar o que ser� retornado na proxima chamada de ProximaLinha
        /// </summary>
        public string PreviaProximaLinha
        {
            get
            {
                if ((previaProximaLinha == null) && (!Entrada.EndOfStream))
                {
                    previaProximaLinha = Entrada.ReadLine();
                    //if (previaProximaLinha != null)
                    //   bytesLidos += previaProximaLinha.Length;
                }
                return previaProximaLinha;
            }
        }

        /// <summary>
        /// Comprimento �til da proxima linha a ser lida
        /// </summary>
        /// <returns></returns>
        public long BytesProximaLinha()
        {
            if (PreviaProximaLinha == null)
                return 0;
            else
                return PreviaProximaLinha.Length;
        }

        /// <summary>
        /// Le a proxima linha.
        /// </summary>
        /// <returns></returns>
        public string ProximaLinha()
        {
            string Leitura;
            if(previaProximaLinha != null)
            {
                Leitura = previaProximaLinha;
                previaProximaLinha = null;
            }
            else
                Leitura = Entrada.ReadLine();
            //if (Leitura != null)
                //bytesLidos += Leitura.Length;
            return Leitura;
        }

        /// <summary>
        /// Apaga o mapeamento anterior
        /// </summary>        
        public void LimpaMapa() {
            if (_Mapas != null)
            {
                _Mapas.Clear();
                _Mapas = null;
            };
        }

        #region RegistraMapa

        /// <summary>
        /// 
        /// </summary>
        /// <param name="CampoColumn"></param>
        /// <param name="inicio"></param>
        /// <param name="tamanho"></param>
        /// <returns></returns>
        public UnidadeMapa RegistraMapa(DataColumn CampoColumn, int inicio, int tamanho)
        {
            return RegistraMapa(CampoColumn, inicio, tamanho, "Mapeamento");
        }

        /// <summary>
        /// Registra os mapeamentos
        /// </summary>
        /// <param name="CampoColumn"></param>
        /// <param name="inicio"></param>
        /// <param name="tamanho"></param>
        /// <param name="NomeMap"></param>
        /// <returns></returns>
        public UnidadeMapa RegistraMapa(DataColumn CampoColumn, int inicio, int tamanho, string NomeMap)
        {            
            UnidadeMapa Nova = new UnidadeMapa(inicio, tamanho, CampoColumn, TiposDeCampo.normal, null);
            Mapas(NomeMap).Add(CampoColumn.ColumnName, Nova);
            Mapas(NomeMap).Preparado = false;
            return Nova;
        }

        

        /// <summary>
        /// Registra os mapeamentos
        /// </summary>
        /// <param name="Campo">Nome do campo</param>
        /// <param name="inicio">Posicao inicial</param>
        /// <param name="tamanho">Tamanho</param>
        public UnidadeMapa RegistraMapa(string Campo, int inicio, int tamanho)
        {
            return RegistraMapa(Campo, inicio, tamanho, "Mapeamento", TiposDeCampo.normal, null);
        }

        /// <summary>
        /// Registra os mapeamento
        /// </summary>
        /// <param name="Campo">Nome do campo</param>
        /// <param name="inicio">Posicao inicial</param>
        /// <param name="tamanho">Tamanho</param>
        /// <param name="Tip">Indica se o mapeamento � do cabecalho,detalhe ou rodap�</param>
        /// <param name="Padrao">Valor default</param>
        /// <remarks>Os dados (Tabela,rowcabecalho,rowrodape) j� devem ter sido fornecidos</remarks>        
        public UnidadeMapa RegistraMapa(string Campo, int inicio, int tamanho, TipoMapa Tip, object Padrao)
        {
            return RegistraMapa(Campo, inicio, tamanho, Tip, TiposDeCampo.normal, Padrao);
        }

        /// <summary>
        /// Registra os mapeamento
        /// </summary>
        /// <param name="Campo">Nome do campo</param>
        /// <param name="inicio">Posicao inicial</param>
        /// <param name="tamanho">Tamanho</param>
        /// <param name="Tip">Indica se o mapeamento � do cabecalho,detalhe ou rodap�</param>
        /// <param name="TipoCampo">Tipo do campo, define o funcionamento</param>
        /// <param name="Padrao">Valor default</param>
        /// <remarks>Os dados (Tabela,rowcabecalho,rowrodape) j� devem ter sido fornecidos</remarks>        
        public UnidadeMapa RegistraMapa(string Campo, int inicio, int tamanho, TipoMapa Tip, TiposDeCampo TipoCampo, object Padrao)
        {
            switch (Tip)
            {
                case TipoMapa.cabecalho:
                    return RegistraMapa(Campo, inicio, tamanho, "MapeamentoCab", TipoCampo, Padrao);

                case TipoMapa.detalhe:
                    return RegistraMapa(Campo, inicio, tamanho, "Mapeamento", TipoCampo, Padrao);

                case TipoMapa.rodape:
                    return RegistraMapa(Campo, inicio, tamanho, "MapeamentoRod", TipoCampo, Padrao);

                case TipoMapa.detalhe2:
                    return RegistraMapa(Campo, inicio, tamanho, "Mapeamento2", TipoCampo, Padrao);
                default: return null;
            }
        }

        /// <summary>
        /// Registra um campo no mapeamento
        /// </summary>
        /// <param name="Campo"></param>
        /// <param name="inicio"></param>
        /// <param name="tamanho"></param>
        /// <param name="NomeMap"></param>
        /// <returns></returns>
        public UnidadeMapa RegistraMapa(string Campo, int inicio, int tamanho, string NomeMap)
        {
            return RegistraMapa(Campo, inicio, tamanho, NomeMap, TiposDeCampo.normal, null);
        }

        /// <summary>
        /// Registra um campo no mapeamento
        /// </summary>
        /// <param name="Campo"></param>
        /// <param name="inicio"></param>
        /// <param name="tamanho"></param>
        /// <param name="NomeMap"></param>
        /// <param name="Padrao"></param>
        /// <returns></returns>
        public UnidadeMapa RegistraMapa(string Campo, int inicio, int tamanho, string NomeMap, object Padrao)
        {
            return RegistraMapa(Campo, inicio, tamanho, NomeMap, TiposDeCampo.normal, Padrao);
        }

        /// <summary>
        /// Registra um campo no mapeamento
        /// </summary>
        /// <param name="Campo">Nome do campo</param>
        /// <param name="inicio"></param>
        /// <param name="tamanho"></param>
        /// <param name="NomeMap">Nome do mapeamento</param>
        /// <param name="TipoCampo"></param>
        /// <param name="Padrao"></param>
        /// <returns></returns>
        public UnidadeMapa RegistraMapa(string Campo, int inicio, int tamanho, string NomeMap, TiposDeCampo TipoCampo, object Padrao)
        {            
            DataTable TabelaRef = null;
            if (NomeMap == "Mapeamento")
                TabelaRef = DataTable;
            else if (NomeMap == "MapeamentoCab")
                TabelaRef = rowCabecalho.Table;
            else if (NomeMap == "MapeamentoRod")
            {
                if (rowRodape != null)
                    TabelaRef = rowRodape.Table;
            }
            else if (NomeMap == "Mapeamento2")
            {
                if (DataTable2 != null)
                    TabelaRef = DataTable2;
            }
            DataColumn DC = null;
            if (TabelaRef != null)
            {
                DC = TabelaRef.Columns[Campo];
                if (DC == null)
                    CamposNaoEncontrados.Add(Campo);
            };
            UnidadeMapa Nova;
            if (DC == null)
                Nova = new UnidadeMapa(inicio, tamanho, Campo, TipoCampo, Padrao);
            else
                Nova = new UnidadeMapa(inicio, tamanho, DC, TipoCampo, Padrao);
            Mapas(NomeMap).Add(Campo, Nova);
            Mapas(NomeMap).Preparado = false;
            Nova.TamanhoVariavel = DefaultTamanhoVariavel;
            return Nova;
        }


        

        /// <summary>
        /// Registra os mapeamento
        /// </summary>
        /// <param name="Campo">Nome do campo</param>
        /// <param name="inicio">Posicao inicial</param>
        /// <param name="fim">Posicao Final</param>
        /// <param name="Tip">Indica se o mapeamento � do cabecalho,detalhe ou rodap�</param>
        /// <param name="Padrao">Valor default</param>
        /// <remarks>Os dados (Tabela,rowcabecalho,rowrodape) j� devem ter sido fornecidos</remarks>        
        public void RegistraMapaDeA(string Campo, int inicio, int fim, TipoMapa Tip, object Padrao)
        {
            if (fim < inicio)
                throw new Exception("Parmetros inv�lidos (fim < inicio) Inicio:" + inicio.ToString() + " Fim:" + fim.ToString());
            RegistraMapa(Campo,inicio, fim - inicio + 1, Tip, Padrao);
        }

        /// <summary>
        /// Registra os mapeamento
        /// </summary>
        /// <param name="Campo">Nome do campo</param>
        /// <param name="inicio">Posicao inicial</param>
        /// <param name="fim">Posicao Final</param>
        /// <param name="Tip">Indica se o mapeamento � do cabecalho,detalhe ou rodap�</param>
        /// <param name="TipoCampo">Tipo do campo, define o funcionamento</param>
        /// <param name="Padrao">Valor default</param>
        /// <remarks>Os dados (Tabela,rowcabecalho,rowrodape) j� devem ter sido fornecidos</remarks>        
        public void RegistraMapaDeA(string Campo, int inicio, int fim, TipoMapa Tip, TiposDeCampo TipoCampo, object Padrao)
        {
            if (fim < inicio)
                throw new Exception("Parmetros inv�lidos (fim < inicio) Inicio:"+inicio.ToString()+" Fim:"+fim.ToString());
            RegistraMapa(Campo, inicio, fim - inicio + 1, Tip, TipoCampo,Padrao);
        }

        /// <summary>
        /// Registra os mapeamentow
        /// </summary>
        /// <param name="Campo">Nome do campo</param>
        /// <param name="inicio">Posicao inicial</param>
        /// <param name="tamanho">Tamanho</param>
        /// <param name="Tip">Indica se o mapeamento � do cabecalho,detalhe ou rodap�</param>
        /// <remarks>Os dados (Tabela,rowcabecalho,rowrodape) j� devem ter sido fornecidos</remarks>        
        public void RegistraMapa(string Campo, int inicio, int tamanho, TipoMapa Tip)
        {
            RegistraMapa(Campo, inicio, tamanho, Tip, null);
        }

        private SortedList equivalenciaDeNomes;

        /// <summary>
        /// Equivalencia entre os nomes dos campos e o cabe�alho do arquivo de importa��o
        /// </summary>
        public SortedList EquivalenciaDeNomes
        {
            get { return equivalenciaDeNomes ?? (equivalenciaDeNomes = new SortedList()); }
        }

        #endregion

        /// <summary>
        /// 
        /// </summary>
        public enum TipoParametro 
        {
            /// <summary>
            /// 
            /// </summary>
            Arquivo,
            /// <summary>
            /// 
            /// </summary>
            Linha
        }

        

        /// <summary>
        /// Cria o mapeamento comparando os t�tulos da primeira linha com o nome dos campos da tabela
        /// </summary>
        /// <param name="arquivo">Nome do arquivo</param>
        /// <returns>Retorna falso se algum dos compos n�o for encontrado</returns>
        /// <remarks>Grava no campo 'UltimoErro' qual a coluna n�o encontrada</remarks>
        /// <param name="TP">Indica se o paremetro � o nome do arquivo ou uma linha com os campos</param>
        /// <param name="Comportamento"></param>
        public bool MapeiaPelaPrimeiraLinha(string arquivo,TipoParametro TP,ComportamentoCamposNaoEncontrados Comportamento)
        {
            bool NaoEncontrado;
            string[] titulos;
            if (TP == TipoParametro.Arquivo)
                titulos = LeCabecalho(arquivo).Split(Separador, StringSplitOptions.None);
            else
                titulos = arquivo.Split(Separador, StringSplitOptions.None);

            foreach (DataColumn Coluna in DataTable.Columns)
            {
                string NomeCampo = Coluna.ColumnName.ToUpper().Trim();
                if (EquivalenciaDeNomes != null)
                    if (equivalenciaDeNomes.ContainsKey(NomeCampo))
                        NomeCampo = ((string)equivalenciaDeNomes[NomeCampo]).ToUpper().Trim();
                int Posicao = 1;
                NaoEncontrado = true;
                foreach (string titulo in titulos)
                {
                    if (titulo.ToUpper().Trim() == NomeCampo)
                    {
                        NaoEncontrado = false;
                        Mapas("Mapeamento").Add(Coluna.ColumnName.ToUpper().Trim(), new UnidadeMapa(Posicao, 0, Coluna));
                        break;
                    };
                    Posicao++;
                }
                if (NaoEncontrado)
                {
                    if ((Comportamento == ComportamentoCamposNaoEncontrados.Abortar) || (Comportamento == ComportamentoCamposNaoEncontrados.NaoMapearRegistrarErro))
                        UltimoErro = "Coluna " + Coluna.ColumnName + " n�o encontrada.";
                    if ((Comportamento == ComportamentoCamposNaoEncontrados.Abortar))
                        return false;
                    if ((Comportamento == ComportamentoCamposNaoEncontrados.AbortarSeCampoObrigatorio) && (!Coluna.AllowDBNull))
                    {
                        UltimoErro = "Coluna obrigat�ria " + Coluna.ColumnName + " n�o encontrada.";
                        return false;
                    }
                }
            };
            return true;
        }

        
        /// <summary>
        /// N�o usar assim
        /// </summary>
        /// <param name="arquivo"></param>
        /// <returns></returns>
        [Obsolete("Usar com parametro: TipoParametro TP",true)]
        public bool MapeiaPelaPrimeiraLinha(string arquivo)
        {
            bool NaoEncontrado;
            //Mapas("Mapeamento") = new ArrayList();
            string[] titulos = LeCabecalho(arquivo).Split(Separador, StringSplitOptions.None);
            
            foreach (DataColumn Coluna in DataTable.Columns)
            {
                string NomeCampo = Coluna.ColumnName.ToUpper().Trim();
                if (EquivalenciaDeNomes != null)
                    if (equivalenciaDeNomes.ContainsKey(NomeCampo))
                        NomeCampo = ((string)equivalenciaDeNomes[NomeCampo]).ToUpper().Trim();
                int Posicao = 1; 
                NaoEncontrado = true;
                foreach (string titulo in titulos)
                {                    
                    if (titulo.ToUpper().Trim() == NomeCampo)
                    {
                        NaoEncontrado = false;
                        Mapas("Mapeamento").Add(Coluna.ColumnName.ToUpper().Trim(),new UnidadeMapa(Posicao, 0, Coluna));
                        break;
                    };
                    Posicao++;
                }
                if(NaoEncontrado){
                    UltimoErro = "Coluna "+Coluna.ColumnName+" n�o encontrada.";
                    return false;
                }
            };
            return true;
        }

        /// <summary>
        /// 
        /// </summary>
        public bool PararEm9 = true;

        /// <summary>
        /// Carrega os dados do arquivo para a tabela
        /// </summary>
        /// <param name="Linhas">N�meo de linhas para ler</param>
        /// <returns>sum�rio se existir</returns>
        /// <remarks>Linhas != 0 le todas as linhas</remarks>
        public string Carrega(int Linhas) {
            string LinhaLida;
            int LinhasLidas = 0;            
            for (LinhaLida = Entrada.ReadLine();(!Entrada.EndOfStream) && (LinhaLida != null); LinhaLida = Entrada.ReadLine())
            {
                if (LinhaLida == "")
                    continue;
                LinhasLidas++;
                //bytesLidos += LinhaLida.Length;
                if (PararEm9 && (LinhaLida.Substring(0, 1) == "9"))
                    break;
                DataRow DR = DataTable.NewRow();
                switch (this.Modelo)
                {
                    case ModelosTXT.layout:
                        foreach (DictionaryEntry DE in Mapas("Mapeamento"))
                        {
                            UnidadeMapa Uni = (UnidadeMapa)DE.Value;
                            ParseCampo(DR, Uni, LinhaLida.Substring(Uni.inicio - 1, Uni.tamanho));
                        }
                        break;
                    case ModelosTXT.delimitado:
                        string[] ValoreLidos = LinhaLida.Split(Separador, StringSplitOptions.None);
                        foreach (DictionaryEntry DE in Mapas("Mapeamento"))
                            try
                            {
                                UnidadeMapa Uni = (UnidadeMapa)DE.Value;
                                ParseCampo(DR, Uni, ValoreLidos[Uni.inicio - 1]);
                            }
                            catch { };
                        break;
                }
                try
                {
                    DataTable.Rows.Add(DR);
                }
                catch { };
                if (Linhas == LinhasLidas)
                    break;
            };
            return LinhaLida;
        }

        /// <summary>
        /// Carrega um arquivo
        /// </summary>
        /// <param name="_NomeArquivo">Nome do arquivo</param>
        /// <param name="inicio">Caracter de in�cio de linha. Pode ser nulo para aceitar todas as linhas</param>
        /// <returns></returns>
        public string Carrega(string _NomeArquivo, string inicio) 
        {            
            NomeArquivo = _NomeArquivo;
            if (inicio == null)
                inicio = "";
            return Carrega(inicio);
        }

        /// <summary>
        /// Carrega um arquivo
        /// </summary>
        /// <param name="inicio">Caracter de in�cio de linha. Pode ser nulo para aceitar todas as linhas</param>
        /// <returns></returns>
        public string Carrega(string inicio)
        {
            string LinhaLida;
            int LinhasLidas = 0;

            for (LinhaLida = Entrada.ReadLine(); (LinhaLida != "") && (LinhaLida != null); LinhaLida = Entrada.ReadLine())
            {
                LinhasLidas++;
                //bytesLidos += LinhaLida.Length;
                if ((LinhasLidas == 1) && (tipoMapeamento == TipoMapeamento.automaticoPrimeiraLinha))
                {
                    if (_Mapas == null)
                        MapeiaPelaPrimeiraLinha(LinhaLida, TipoParametro.Linha, ComportamentoCamposNaoEncontradosPadrao);
                    continue;
                }
                if (inicio != "")
                    if (LinhaLida.Substring(0, 1) != inicio)
                        continue;
                DataRow DR = DataTable.NewRow();
                switch (this.Modelo)
                {
                    case ModelosTXT.layout:
                        foreach (DictionaryEntry DE in Mapas("Mapeamento"))
                        {
                            UnidadeMapa Uni = (UnidadeMapa)DE.Value;                                                        
                            ParseCampo(DR, Uni, (LinhaLida.Length >= Uni.inicio + Uni.tamanho-1) ? LinhaLida.Substring(Uni.inicio - 1, Uni.tamanho): 
                                                (LinhaLida.Length >= Uni.inicio-1)               ? LinhaLida.Substring(Uni.inicio - 1)             :"");                            
                        }
                        break;
                    case ModelosTXT.delimitado:
                        string[] ValoreLidos = LinhaLida.Split(Separador, StringSplitOptions.None);
                        foreach (DictionaryEntry DE in Mapas("Mapeamento"))
                        {
                            UnidadeMapa Uni = (UnidadeMapa)DE.Value;
                            ParseCampo(DR, Uni, ValoreLidos[Uni.inicio - 1]);
                        }
                        break;
                }
                DataTable.Rows.Add(DR);
                
            };

            Entrada.Close();
            return LinhaLida;
        }

        /// <summary>
        /// Carrega os dados do arquivo para a tabela
        /// </summary>
        /// <returns>sum�rio se existir</returns>
        public string Carrega()
        {
            return Carrega(-1);
        }

        /// <summary>
        /// 
        /// </summary>
        public decimal DivDecimal = 100;
        /// <summary>
        /// 
        /// </summary>
        public bool LimparDecimais = false;
        /// <summary>
        /// 
        /// </summary>
        public bool UsarTrim = false;

        internal bool ParseValor(UnidadeMapa UM, string Valor,out object oValor)
        {
            oValor = null;
            try
            {
                if (UM.TipoDeDado == typeof(bool))
                {
                    if ((Valor.ToUpper() == "TRUE") || (Valor.ToUpper() == "SIM") || (Valor.ToUpper() == "1"))
                        oValor = true;
                    else
                        oValor = false;
                }
                else if (UM.TipoDeDado == typeof(string))
                {
                    if (UsarTrim)
                        oValor = Valor.Trim();
                    else
                        oValor = Valor;
                }
                else if (UM.TipoDeDado == typeof(int))
                    oValor = int.Parse(Valor.Trim());
                else if (UM.TipoDeDado == typeof(decimal))
                {
                    String sValor = Valor.Trim();
                    if (LimparDecimais)
                        sValor = sValor.Replace(".", "");
                    oValor = decimal.Parse(sValor) / DivDecimal;
                }
                else if (UM.TipoDeDado == typeof(Int64))
                    oValor = Int64.Parse(Valor.Trim());
                else if (UM.TipoDeDado == typeof(Int16))
                    oValor = Int16.Parse(Valor.Trim());
                else if (UM.TipoDeDado == typeof(DateTime))
                {
                    int dia = 0;
                    int mes = 0;
                    int ano = 0;
                    FormatoData Formato = Formatodata;
                    if (UM.Formato != FormatoData.Padrao)
                        Formato = UM.Formato;
                    switch (Formato)
                    {
                        case FormatoData.ddMMyy:
                            dia = int.Parse(Valor.Substring(0, 2));
                            mes = int.Parse(Valor.Substring(2, 2));
                            ano = 2000 + int.Parse(Valor.Substring(4, 2));
                            break;
                        case FormatoData.ddMMyyyy:
                            dia = int.Parse(Valor.Substring(0, 2));
                            mes = int.Parse(Valor.Substring(2, 2));
                            ano = int.Parse(Valor.Substring(4, 4));
                            break;
                        case FormatoData.MMddyyyy:
                            mes = int.Parse(Valor.Substring(0, 2));
                            dia = int.Parse(Valor.Substring(2, 2));
                            ano = int.Parse(Valor.Substring(4, 4));
                            break;
                        case FormatoData.dd_MM_YYYY:
                            dia = int.Parse(Valor.Substring(0, 2));
                            mes = int.Parse(Valor.Substring(3, 2));
                            ano = int.Parse(Valor.Substring(6, 4));
                            break;
                        case FormatoData.yyMMdd:
                            dia = int.Parse(Valor.Substring(4, 2));
                            mes = int.Parse(Valor.Substring(2, 2));
                            ano = 2000 + int.Parse(Valor.Substring(0, 2));
                            break;
                        case FormatoData.yyyyMMdd:
                            dia = int.Parse(Valor.Substring(6, 2));
                            mes = int.Parse(Valor.Substring(4, 2));
                            ano = int.Parse(Valor.Substring(0, 4));
                            break;
                        case FormatoData.yyyyMM:
                            dia = 1;
                            mes = int.Parse(Valor.Substring(4, 2));
                            ano = int.Parse(Valor.Substring(0, 4));
                            break;
                        case FormatoData.yyyy_MM_dd:
                            dia = int.Parse(Valor.Substring(8, 2));
                            mes = int.Parse(Valor.Substring(5, 2));
                            ano = int.Parse(Valor.Substring(0, 4));
                            break;
                    };
                    oValor = new DateTime(ano, mes, dia);
                }                                                   
                return true;
            }
            catch
            {
                if (UM.TipoDeDado == typeof(string))
                    oValor = "";
                else if (UM.TipoDeDado == typeof(int))
                    oValor = 0;
                else if (UM.TipoDeDado == typeof(decimal))
                    oValor = 0M;
                else oValor = null;
                //else if (UM.Campo.DataType == typeof(DateTime))
                //    DR[UM.Campo] = DateTime.Today;
                return false;
            }
        }

        private bool ParseCampo(DataRow DR, UnidadeMapa UM, string Valor) {
            object oValor;
            bool Retorno = ParseValor(UM, Valor, out oValor);
            DR[UM.Campo] = oValor ?? DBNull.Value;
            return Retorno;             
        }
        
        /*
        private int MaxInicio(ArrayList Map)
        {
            int max = 0;
            foreach (UnidadeMapa Uni in Map)
                max = Math.Max(max, Uni.inicio);
            return max;
        }*/

        private UnidadeMapa BuscaUnidadeMapa(Mapa Map, int _Inicio) {
            foreach (DictionaryEntry DE in Map) {
                UnidadeMapa Uni = (UnidadeMapa)DE.Value;
                if (Uni.inicio == _Inicio)
                    return Uni;
            }
            return null;
        }



        private void RegistraUnidadePreparada(string NomeMap,int Sequencia, UnidadeMapa DaVez)
        {
            Mapas(NomeMap).Add(Sequencia, DaVez);

            if ((DaVez.Tipo == TiposDeCampo.normal) && (DaVez.Padrao == null))
            {
                if (DaVez.Campo == null)
                    DaVez.Padrao = "";                
                else if (DaVez.Campo.DataType == typeof(string))
                    DaVez.Padrao = "";
                else if (DaVez.Campo.DataType == typeof(byte[]))
                    DaVez.Padrao = "";
                else if (DaVez.Campo.DataType == typeof(int))
                    DaVez.Padrao = 0;
                else if (DaVez.Campo.DataType == typeof(Int16))
                    DaVez.Padrao = 0;
                else if (DaVez.Campo.DataType == typeof(Int64))
                    DaVez.Padrao = 0;
                else if (DaVez.Campo.DataType == typeof(decimal))
                    DaVez.Padrao = 0.0M;
                else if (DaVez.Campo.DataType == typeof(DateTime))
                    DaVez.Padrao = DateTime.Today;
                else
                    throw new Exception("Procedimento n�o implementado para tipo:" + DaVez.Campo.DataType.ToString());
            };
        }

        private void PreparaMapaGravacao(string NomeMap) {
            if (!_Mapas.ContainsKey(NomeMap))
                return;
            Mapa Original = Mapas(NomeMap);
            _Mapas.Remove(NomeMap);
            if (Modelo == ModelosTXT.delimitado)
            {
                int maxSequencia = 0;
                foreach (DictionaryEntry DE in Original)
                {
                    UnidadeMapa Uni = (UnidadeMapa)DE.Value;
                    maxSequencia = Math.Max(maxSequencia, Uni.inicio);
                }
                for (int Sequencia = 1; Sequencia <= maxSequencia; Sequencia++)
                {
                    UnidadeMapa DaVez = BuscaUnidadeMapa(Original, Sequencia);
                    if (DaVez != null)                    
                        RegistraUnidadePreparada(NomeMap, Sequencia,DaVez); 
                    else
                        Mapas(NomeMap).Add(Sequencia, new UnidadeMapa(Sequencia, 0, "", TiposDeCampo.normal, ""));
                }
            }
            else
            {
                TamanhoDoRegistro = 0;
                foreach (DictionaryEntry DE in Original)
                {
                    UnidadeMapa Uni = (UnidadeMapa)DE.Value;
                    TamanhoDoRegistro = Math.Max(TamanhoDoRegistro, Uni.inicio + Uni.tamanho - 1);
                }

                int Sequencia = 0;
                for (int pos = 1; pos <= TamanhoDoRegistro; )
                {
                    UnidadeMapa DaVez = BuscaUnidadeMapa(Original, pos);
                    if (DaVez != null)
                    {
                        RegistraUnidadePreparada(NomeMap, Sequencia++, DaVez);                                                
                        pos += DaVez.tamanho;
                    }
                    else
                    {
                        int NovoTamano;
                        for (NovoTamano = 1; (pos + NovoTamano) < TamanhoDoRegistro; NovoTamano++)
                            if (BuscaUnidadeMapa(Original, pos + NovoTamano) != null)
                                break;
                        Mapas(NomeMap).Add(Sequencia++, new UnidadeMapa(pos, NovoTamano, "", TiposDeCampo.normal, ""));
                        pos += NovoTamano;
                    };
                }
            }
        }

        private string Formatar(object Valor, int Tamanho) 
        {
            return Formatar(Valor, Tamanho, null);
        }

        private string Formatar(object Valor,int Tamanho,UnidadeMapa Uni) 
        {
            if ((Modelo == ModelosTXT.delimitado) && (Uni.TamanhoVariavel))
            {
                if (Valor.GetType() == typeof(string))
                {
                    string manobra = Valor.ToString().Trim();
                    if (Uni != null)
                    {
                        if (Uni.removeacentos)
                            manobra = Acentos(manobra);
                        if (Uni.soAZ09)
                            manobra = paraAZ09(manobra, Uni.EliminaRepique);
                        if (Uni.limpaAZ09)
                            manobra = limpaAZ09(manobra);
                    }

                    if(Uni.TamanhoVariavelTeto)
                        if (manobra.Length <= Tamanho)
                            return manobra;
                        else
                            return manobra.Substring(0, Tamanho);

                    return manobra;
                    
                }
                else if (Valor.GetType() == typeof(int))
                    return ((int)Valor).ToString();
                else if (Valor.GetType() == typeof(Int16))
                    return ((Int16)Valor).ToString();
                else if (Valor.GetType() == typeof(Int64))
                    return ((Int64)Valor).ToString();
                else if (Valor.GetType() == typeof(DateTime))
                    return ((DateTime)Valor).ToString(strFormatoData);
                else if (Valor.GetType() == typeof(decimal))
                {
                    if (PontoDecima == "")
                        return ((int)decimal.Round((decimal)Valor * 100M)).ToString("D3");
                    else
                        return ((decimal)Valor).ToString("F2").Replace(",",PontoDecima);
                }
                else if (Valor.GetType() == typeof(double))
                {
                    decimal Valord = (decimal)((double)Valor);
                    if (PontoDecima == "")
                        return ((int)decimal.Round(Valord * 100M)).ToString("D3");
                    else
                        return (Valord).ToString("F2").Replace(",", PontoDecima);
                }
                else if (Valor.GetType() == typeof(byte[]))
                {
                    byte[] Valorb = (byte[])Valor;

                    string manobra = Encoding.GetString(Valorb);
                    if (Uni != null)
                    {
                        if (Uni.removeacentos)
                            manobra = Acentos(manobra);
                        if (Uni.soAZ09)
                            manobra = paraAZ09(manobra, Uni.EliminaRepique);
                    }

                    manobra = manobra.Replace("\r\n", "\t");

                    if (Uni.TamanhoVariavelTeto)
                        if (manobra.Length <= Tamanho)
                            return manobra;
                        else
                            return manobra.Substring(0, Tamanho);

                    return manobra;
                }
                else throw new Exception("Procedimento n�o implementado para tipo:" + Valor.GetType().ToString());
            }
            else
            {
                if (Valor.GetType() == typeof(string))
                {
                    string manobra = Valor.ToString();
                    if (Uni != null)
                    {
                        if (Uni.removeacentos)
                            manobra = Acentos(manobra);
                        if (Uni.soAZ09)
                            manobra = paraAZ09(manobra, Uni.EliminaRepique);
                        if (Uni.limpaAZ09)
                            manobra = limpaAZ09(manobra);
                    }
                    if (manobra.Length <= Tamanho)
                        return manobra.PadRight(Tamanho);
                    else
                        return manobra.Substring(0, Tamanho);
                }
                else if (Valor.GetType() == typeof(int))
                    return ((int)Valor).ToString().PadLeft(Tamanho, '0');
                else if (Valor.GetType() == typeof(Int16))
                    return ((Int16)Valor).ToString().PadLeft(Tamanho, '0');
                else if (Valor.GetType() == typeof(Int64))
                    return ((Int64)Valor).ToString().PadLeft(Tamanho, '0');
                else if (Valor.GetType() == typeof(DateTime))
                    return ((DateTime)Valor).ToString(strFormatoData).PadLeft(Tamanho);
                else if (Valor.GetType() == typeof(decimal))
                {
                    if (PontoDecima == "")
                        return ((int)decimal.Round((decimal)Valor * 100M)).ToString("D" + Tamanho.ToString());
                    else
                        return ((int)decimal.Round((decimal)Valor * 100M)).ToString("D" + (Tamanho - 1).ToString()).Insert(Tamanho - 3, PontoDecima);
                }
                else if (Valor.GetType() == typeof(byte[]))
                {
                    byte[] Valorb = (byte[])Valor;
                    return Encoding.GetString(Valorb);
                }
                else throw new Exception("Procedimento n�o implementado para tipo:" + Valor.GetType().ToString());
            }
        }

        private string LinhaGravacao(Mapa Map, DataRow row) {
            return LinhaGravacao(Map, row, null);
        }

        private string LinhaGravacao(Mapa Map, SortedList Lista) {
            return LinhaGravacao(Map, null, Lista);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="NomeMap"></param>
        /// <returns></returns>
        public bool GravaCabecalho(string NomeMap)
        {            
            Saida.WriteLine(GravaCabecalho(Mapas(NomeMap)));
            return true;
        }

        private string GravaCabecalho(Mapa Map)
        {
            string retorno = "";
            if (!Map.Preparado)
            {
                Map.Preparado = true;
                PreparaMapaGravacao(Map.Nome);
                Map = Mapas(Map.Nome);
            };
            int contador = 0;
            foreach (DictionaryEntry DE in Map)
            {
                UnidadeMapa Uni = (UnidadeMapa)DE.Value;
                if (Modelo == ModelosTXT.delimitado)
                {
                    string NomeGravar;             
                    if (Uni.Campo == null)
                        NomeGravar = Uni.NomeCampo;
                    else
                        NomeGravar = Uni.Campo.ToString();
                    if (EquivalenciaDeNomes.Contains(NomeGravar))
                        NomeGravar = (string)EquivalenciaDeNomes[NomeGravar];
                    retorno += NomeGravar;
                    contador++;
                    if (GravaSeparadorFinal || (contador < Map.Count))
                        foreach (char c in Separador)
                            retorno += c;
                }
                else
                {
                    if (Uni.Campo == null)
                        retorno += Formatar(Uni.NomeCampo, Uni.tamanho, Uni);
                    else
                        retorno += Formatar(Uni.Campo, Uni.tamanho, Uni);
                }
                
                
                    
            };
            return retorno;
        }

        

        private string LinhaGravacao(Mapa Map,DataRow row,SortedList Lista) { 
            string retorno = "";
            if(!Map.Preparado){
                Map.Preparado = true;
                PreparaMapaGravacao(Map.Nome);
                Map = Mapas(Map.Nome);
            };
            int contador = 0;
            foreach (DictionaryEntry DE in Map)
            {
                contador++;
                UnidadeMapa Uni = (UnidadeMapa)DE.Value;
                switch (Uni.Tipo)
                {
                    case TiposDeCampo.Sequencial:
                        retorno += Formatar(Sequencial, Uni.tamanho, Uni);
                        break;
                    case TiposDeCampo.NDetalhe:
                        retorno += Formatar(NDetalhe, Uni.tamanho, Uni);
                        break;
                    case TiposDeCampo.Somatorio:
                        retorno += Formatar(Somatorio, Uni.tamanho, Uni);
                        break;
                    case TiposDeCampo.AutoData:
                        retorno += Formatar(DateTime.Today, 6, Uni);
                        break;
                    case TiposDeCampo.normal:
                        bool encontrado = false;
                        if (row != null)
                        {
                            if (Uni.Campo == null)
                            {
                                if ((Uni.NomeCampo != null) && (row.Table.Columns.Contains(Uni.NomeCampo)) && (row[Uni.NomeCampo] != null) && (row[Uni.NomeCampo] != DBNull.Value))
                                {
                                    retorno += Formatar(row[Uni.NomeCampo], Uni.tamanho,Uni);
                                    encontrado = true;
                                }                                
                            }
                            else
                            {
                                if (row[Uni.Campo] != DBNull.Value)
                                {
                                    retorno += Formatar(row[Uni.Campo], Uni.tamanho,Uni);
                                    encontrado = true;
                                }
                            }
                        }
                        if(!encontrado)
                            if ((Lista != null) && (Lista[Uni.NomeCampo] != null))
                            {
                                retorno += Formatar(Lista[Uni.NomeCampo], Uni.tamanho,Uni);
                                encontrado = true;
                            }
                        if(!encontrado)
                            retorno += Formatar(Uni.Padrao, Uni.tamanho,Uni);
                        break;
                }
                if (Modelo == ModelosTXT.delimitado)
                    if (GravaSeparadorFinal || (contador < Map.Count))
                    {
                        foreach (char c in Separador)
                            retorno += c;
                    };
            };
            Sequencial++;
            return retorno;
        }

        /// <summary>
        /// Inicia a grava��o
        /// </summary>
        /// <remarks>Deve ser usada em conjunfo com GravaLinha,GravaTabela e TerminaGrava��o de preferencia com try/finaly para liberar recursos</remarks>
        /// <param name="arquivo"></param>
        /// <returns></returns>
        public bool IniciaGravacao(string arquivo)
        {
            Saida = new System.IO.StreamWriter(arquivo, false, Encoding);
            return (Saida != null);
        }

        //*** MRC - INICIO - TRIBUTOS (06/11/2014 15:00) ***
        /// <summary>
        /// Inicia a grava��o (verificando se permite incluir linha em arquivo j� aberto)
        /// </summary>
        /// <remarks>Deve ser usada em conjunfo com GravaLinha,GravaTabela e TerminaGrava��o de preferencia com try/finaly para liberar recursos</remarks>
        /// <param name="arquivo"></param>
        /// <param name="apend"></param>
        /// <returns></returns>
        public bool IniciaGravacao(string arquivo, bool apend)
        {
            if (apend && Saida != null) return true;
            Saida = new System.IO.StreamWriter(arquivo, apend, Encoding);
            return (Saida != null);
        }
        //*** MRC - TERMINO - TRIBUTOS (06/11/2014 15:00) ***

        /// <summary>
        /// 
        /// </summary>
        /// <param name="NomeMap"></param>
        /// <param name="Linharow"></param>
        /// <param name="LinhaLista"></param>
        /// <returns></returns>
        public bool GravaLinha(string NomeMap, DataRow Linharow, SortedList LinhaLista)
        {
            Saida.WriteLine(LinhaGravacao(Mapas(NomeMap), Linharow, LinhaLista));
            return true;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="NomeMap"></param>
        /// <param name="Linha"></param>
        /// <returns></returns>
        public bool GravaLinha(string NomeMap, DataRow Linha) {
            if (Saida == null)
                IniciaGravacao(NomeArquivo);
            Saida.WriteLine(LinhaGravacao(Mapas(NomeMap), Linha));
            return true;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="NomeMap"></param>
        /// <param name="Tabela"></param>
        /// <returns></returns>
        public bool GravaLinhas(string NomeMap, DataTable Tabela)
        {
            foreach (DataRow Linha in Tabela.Rows)
            {
                if (Linha.RowState != DataRowState.Deleted)
                {
                    Saida.WriteLine(LinhaGravacao(Mapas(NomeMap), Linha));
                };
            };
            return true;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="NomeMap"></param>
        /// <param name="Linha"></param>
        /// <returns></returns>
        public bool GravaLinha(string NomeMap, SortedList Linha)
        {
            Saida.WriteLine(LinhaGravacao(Mapas(NomeMap), Linha));
            return true;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="Literal"></param>
        /// <returns></returns>
        public bool GravaLinha(string Literal)
        {
            Saida.WriteLine(Literal);
            return true;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public bool TerminaGravacao() {
            if (Saida == null)
                return false;
            Saida.Close();
            Saida.Dispose();
            Saida = null;
            return true;
        }



        /// <summary>
        /// Gera um arquivo com os dados armazenados
        /// </summary>
        /// <param name="arquivo">Nome do Arquivo</param>
        /// <returns></returns>
        public virtual bool Salva(string arquivo)
        {  
            //PreparaMapaGravacao("MapeamentoCab");
            //PreparaMapaGravacao("Mapeamento");
            //PreparaMapaGravacao("MapeamentoRod");
            //PreparaMapaGravacao("Mapeamento2");
            Sequencial = 1;
            NDetalhe = 0;
            Somatorio = 0;
            using (Saida = new System.IO.StreamWriter(arquivo, false, Encoding))
            {
                try
                {
                    if (_Mapas.ContainsKey("MapeamentoCab"))
                        Saida.WriteLine(LinhaGravacao(Mapas("MapeamentoCab"),rowCabecalho));
                    if (GravarCamposNaPrimeiraLinha)
                        GravaCabecalho("Mapeamento");
                    //nucleo
                    if(_Mapas.ContainsKey("Mapeamento"))
                        foreach (DataRow row in DataTable.Rows)
                        {
                            if (row.RowState != DataRowState.Deleted)
                            {
                                NDetalhe++;
                                if ((CampoSomatorio != null) && (CampoSomatorio != ""))
                                    Somatorio += (decimal)row[CampoSomatorio];
                                Saida.WriteLine(LinhaGravacao(Mapas("Mapeamento"), row));
                            }
                        }
                    if (_Mapas.ContainsKey("Mapeamento2"))
                        foreach (DataRow row in DataTable2.Rows)
                        {
                            if (row.RowState != DataRowState.Deleted)
                            {
                                NDetalhe++;
                                if ((CampoSomatorio != null) && (CampoSomatorio != ""))
                                    Somatorio += (decimal)row[CampoSomatorio];
                                Saida.WriteLine(LinhaGravacao(Mapas("Mapeamento2"), row));
                            }
                        }
                    if (_Mapas.ContainsKey("MapeamentoRod"))
                        Saida.WriteLine(LinhaGravacao(Mapas("MapeamentoRod"), rowRodape));
                    return true;
                }
                catch (Exception e)
                {
                    UltimoErro = e.Message;
                    return false;
                };
            }
        }
    }

    /// <summary>
    /// 
    /// </summary>
    public class Mapa : SortedList {
        /// <summary>
        /// 
        /// </summary>
        public string Nome;
        internal bool Preparado;
        /// <summary>
        /// Construtor
        /// </summary>
        /// <param name="Nome"></param>
        public Mapa(string Nome) {
            this.Nome = Nome;
            Preparado = false;
        }
    }

    /// <summary>
    /// Objeto base a ser derivado para cada tipo de registro
    /// </summary>
    public class RegistroBase
    {
        /// <summary>
        /// 
        /// </summary>
        public string LinhaLida;
        private Mapa MapaRegistro;        
        /// <summary>
        /// 
        /// </summary>
        public cTabelaTXT cTabTXT;
        private ArrayList CamposFortementeTipados;
        /// <summary>
        /// 
        /// </summary>
        public SortedList Valores;
        private bool PrimeiraInstancia = true;

        /// <summary>
        /// 
        /// </summary>
        /// <param name="Field"></param>
        /// <param name="Inicio"></param>
        /// <param name="Tamanho"></param>
        /// <returns></returns>
        protected UnidadeMapa RegistraFortementeTipado(string Field, int Inicio, int Tamanho)
        {            
            UnidadeMapa retorno = RegistraFortementeTipado(Field, Inicio, Tamanho, false);
            if ((retorno != null) && (cTabTXT != null))
            {
                retorno.removeacentos = cTabTXT.defaultremoveacentos;
                retorno.soAZ09 = cTabTXT.defaultsoAZ09;
                retorno.limpaAZ09 = cTabTXT.defaultlimpaAZ09;
            }
            return retorno;

        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="Field"></param>
        /// <param name="Inicio"></param>
        /// <param name="Tamanho"></param>
        /// <param name="TamanhoVariavel"></param>
        /// <returns></returns>
        protected UnidadeMapa RegistraFortementeTipado(string Field, int Inicio, int Tamanho, bool TamanhoVariavel)
        {
            UnidadeMapa Retorno = null;
            if (PrimeiraInstancia)
            {
                object Padrao = null;
                if (GetType().GetField(Field) != null)
                {
                    Padrao = GetType().GetField(Field).GetValue(this);
                    if ((Padrao == null) && (GetType().GetField(Field).FieldType == typeof(string)))
                    {
                        Padrao = "";
                    }
                }
                else if (GetType().GetProperty(Field) != null)
                {
                    Padrao = GetType().GetProperty(Field).GetValue(this, null);
                    if ((Padrao == null) && (GetType().GetProperty(Field).PropertyType == typeof(string)))
                    {
                        Padrao = "";
                    }
                }
                Retorno = cTabTXT.RegistraMapa(Field, Inicio, Tamanho, GetType().Name, Padrao);
                Retorno.TamanhoVariavel = TamanhoVariavel;                
            }
            if (CamposFortementeTipados == null)
                CamposFortementeTipados = new ArrayList();
            CamposFortementeTipados.Add(Field);
            return Retorno;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="Field"></param>
        /// <param name="Inicio"></param>
        /// <param name="Tamanho"></param>
        /// <param name="Padrao"></param>
        /// <returns></returns>
        protected UnidadeMapa RegistraMapa(string Field, int Inicio, int Tamanho, object Padrao)
        {
            if (PrimeiraInstancia)
                return cTabTXT.RegistraMapa(Field, Inicio, Tamanho, GetType().Name, Padrao);
            else
                return null;
        }

        /// <summary>
        /// Construtor padr�o
        /// </summary>
        public RegistroBase()
        {            
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="_cTabTXT"></param>
        public RegistroBase(cTabelaTXT _cTabTXT)
        {
            cTabTXT = _cTabTXT;            
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="_cTabTXT"></param>
        /// <param name="Linha"></param>
        [Obsolete("Chamar outro contrutor e sem seguida o CarregaLinha")]
        public RegistroBase(cTabelaTXT _cTabTXT,string Linha)
        {
            LinhaLida = Linha;
            cTabTXT = _cTabTXT;
            MapaReg = cTabTXT.Mapas(GetType().Name);
                            
            PrimeiraInstancia = (MapaReg.Count == 0);  
            RegistraMapa();
            
            if (Linha != null)
                Decodificalinha(Linha);
        }

        /// <summary>
        /// Carrega a linha. este m�todo deve ser chamado ap�s o contrutor e substitui o obsoleto contrutor com a linha porque n�o devemos chamar m�todos virtuais no construtor
        /// </summary>
        /// <param name="Linha"></param>
        public void CarregaLinha(string Linha)
        {
            LinhaLida = Linha;
            MapaReg = cTabTXT.Mapas(GetType().Name);

            PrimeiraInstancia = (MapaReg.Count == 0);

            //Chamada do m�todo virtual RegistraMapa
            RegistraMapa();

            if (Linha != null)
                Decodificalinha(Linha);
        }

        /// <summary>
        /// Metodo para tipar os valore lidos. N�o � necessario chamar se os Campos forem mapegados com a fun��o RegistraFortementeTipado
        /// </summary>
        protected virtual void DecodificarValores()
        {
            //Metodo abstrato para tipar valores
        }

        /// <summary>
        /// 
        /// </summary>
        public virtual void GravaLinha()
        {
            GravaLinha(null);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="Linharow"></param>
        public void GravaLinha(DataRow Linharow)
        {
            Valores = new SortedList();
            if (CamposFortementeTipados != null)
                foreach (string CampoTipado in CamposFortementeTipados)
                {
                    object oValor;
                    if (GetType().GetField(CampoTipado) != null)
                        oValor = GetType().GetField(CampoTipado).GetValue(this);
                    else
                        oValor = GetType().GetProperty(CampoTipado).GetValue(this,null);
                    Valores.Add(CampoTipado, oValor);
                }
            cTabTXT.GravaLinha(GetType().Name,Linharow, Valores);
        }

        /// <summary>
        /// usar uma sequencia de cTabTXT.RegistraMapa("Competencia", 292, 6, GetType().Name, DateTime.MinValue);
        /// </summary>
        protected virtual void RegistraMapa()
        { 
            //Metodo abstrato para registrar o mas e suas unidades de mapeamento                        
        }

        /// <summary>
        /// 
        /// </summary>
        public virtual Mapa MapaReg 
        {
            get { return MapaRegistro; }
            set { MapaRegistro = value; }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="linha"></param>
        public void Decodificalinha(string linha)
        {
            LinhaLida = linha;
            Valores = new SortedList();
            object oValor;
            switch (cTabTXT.Modelo)
            {
                case ModelosTXT.layout:
                    foreach (DictionaryEntry DE in MapaRegistro)
                    {
                        UnidadeMapa Uni = (UnidadeMapa)DE.Value;
                        cTabTXT.ParseValor(Uni, linha.Substring(Uni.inicio - 1, Uni.tamanho), out oValor);
                        Valores.Add(DE.Key, oValor);
                    }
                    break;
                case ModelosTXT.delimitado:
                    string[] ValoreLidos = linha.Split(cTabTXT.Separador, StringSplitOptions.None);
                    foreach (DictionaryEntry DE in MapaRegistro)
                    {
                        UnidadeMapa Uni = (UnidadeMapa)DE.Value;
                        cTabTXT.ParseValor(Uni, ValoreLidos[Uni.inicio - 1], out oValor);
                        Valores.Add(DE.Key, oValor);
                    }
                    break;
            };
            if (CamposFortementeTipados != null)
                foreach (string CampoTipado in CamposFortementeTipados)
                    if (GetType().GetField(CampoTipado) != null)
                        GetType().GetField(CampoTipado).SetValue(this, Valores[CampoTipado]);
                    else
                        GetType().GetProperty(CampoTipado).SetValue(this, Valores[CampoTipado],null);
            DecodificarValores();                    
        }
    }
}
