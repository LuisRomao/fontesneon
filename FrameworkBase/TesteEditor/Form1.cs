﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace TesteEditor
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            dataSet1.DataTable1.AddDataTable1Row(201311);
            dataSet1.DataTable1.AddDataTable1Row(201312);
            dataSet1.DataTable1.AddDataTable1Row(201401);
            dataSet1.DataTable1.AddDataTable1Row(201402);
            dataSet1.DataTable1.AddDataTable1Row(201403);
            dataSet1.DataTable1.AddDataTable1Row(201404);
            dataSet1.DataTable1.AddDataTable1Row(201405);
            dataSet1.DataTable1.AddDataTable1Row(201406);
            dataSet1.DataTable1.AddDataTable1Row(201407);
            DataSet1.DataTable1Row rowZero = dataSet1.DataTable1.NewDataTable1Row();
            dataSet1.DataTable1.AddDataTable1Row(rowZero);
        }

        private string Format(int Original)
        {            
            return string.Format("{0:00}/{1:0000}", Original % 100, Original / 100);
        }

        private void repositoryItemTextEdit1_FormatEditValue(object sender, DevExpress.XtraEditors.Controls.ConvertEditValueEventArgs e)
        {
            if (e.Value == null)
                return;
            if (!(e.Value is int))
                return;
            //int Original = (int)e.Value;                        
            //e.Value = string.Format("{0:00}/{1:0000}", Original % 100, Original / 100);
            e.Value = Format((int)e.Value);
            e.Handled = true;
        }

        private void repositoryItemTextEdit1_ParseEditValue(object sender, DevExpress.XtraEditors.Controls.ConvertEditValueEventArgs e)
        {
            if (e.Value == null)
            {
                //Console.Write("NULO\r\n");
                e.Value = DBNull.Value;
                e.Handled = true;
                return;
            }
            if (!(e.Value is string))
            {
                //Console.Write("não é string : {0}\r\n",e.Value.GetType());
                //e.Value = DBNull.Value;
                e.Handled = true;
                return;
            }
            string Original = (string)e.Value;
            if (Original.Length != 7)
            {
                //Console.Write("Tamanho: {0}\r\n", Original.Length);
                e.Value = DBNull.Value;
                e.Handled = true;
                return;
            }
            int mes;
            int ano;
            if (int.TryParse(Original.Substring(0, 2), out mes) && int.TryParse(Original.Substring(3, 4), out ano))
            {
                int Formatado = ano * 100 + mes;
                //Console.Write("{0} -> {1}\r\n", Original, Formatado);
                e.Value = Formatado;
                e.Handled = true;
            }
        }

        private void repositoryItemButtonEdit1_ButtonClick(object sender, DevExpress.XtraEditors.Controls.ButtonPressedEventArgs e)
        {
            int mes;
            int ano;
            DevExpress.XtraEditors.ButtonEdit Editor = (DevExpress.XtraEditors.ButtonEdit)sender;
            if (Editor.EditValue is int)
            {
                int Original = (int)Editor.EditValue;
                mes = Original % 100;
                ano = Original / 100;
            }
            else if (Editor.EditValue is string)
            {
                string Original = (string)Editor.EditValue;
                if ((Original.Length != 7) || !int.TryParse(Original.Substring(0, 2), out mes) || !int.TryParse(Original.Substring(3, 4), out ano))
                    return;
            }
            else
                return;
            switch (e.Button.Kind)
            {
                case DevExpress.XtraEditors.Controls.ButtonPredefines.Left:
                    mes--;
                    if (mes == 0)
                    {
                        mes = 12;
                        ano--;
                    }
                    break;
                case DevExpress.XtraEditors.Controls.ButtonPredefines.Right:
                    mes++;
                    if (mes == 13)
                    {
                        mes = 1;
                        ano++;
                    }
                    break;
            }
            int Valor = mes + ano * 100;
            Editor.EditValue = Valor;
            Editor.Text = Format(Valor);
        }

        private void repositoryItemCompetenciaEdit1_PropertiesChanged(object sender, EventArgs e)
        {
            for (int i = 0; i < repositoryItemCompetenciaEdit1.Buttons.Count; i++)
                repositoryItemCompetenciaEdit1.Buttons[i].Enabled = !repositoryItemCompetenciaEdit1.ReadOnly;
            
                
        }

        private void gridView1_FocusedRowChanged(object sender, DevExpress.XtraGrid.Views.Base.FocusedRowChangedEventArgs e)
        {
            repositoryItemCompetenciaEdit1.ReadOnly = (e.FocusedRowHandle % 2 == 0);
        }

        private void repositoryItemCompetenciaEdit1_Enter(object sender, EventArgs e)
        {
            for (int i = 0; i < repositoryItemCompetenciaEdit1.Buttons.Count; i++)
                repositoryItemCompetenciaEdit1.Buttons[i].Enabled = !repositoryItemCompetenciaEdit1.ReadOnly;
        }
    }
}
