﻿using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using DevExpress.XtraReports.UI;

namespace dllImpostos.Comprovantes
{
    /// <summary>
    /// Classe do Modelo de Comprovante de DARF PRETO (BRADESCO)
    /// </summary>
    public partial class ComprDARFPT_Bradesco : DevExpress.XtraReports.UI.XtraReport
    {
        /// <summary>
        /// Modelo de Comprovante de DARF PRETO (BRADESCO)
        /// </summary>
        public ComprDARFPT_Bradesco()
        {
            InitializeComponent();
        }

    }
}
