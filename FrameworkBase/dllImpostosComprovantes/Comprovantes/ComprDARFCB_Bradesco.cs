﻿using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using DevExpress.XtraReports.UI;

namespace dllImpostos.Comprovantes
{
    /// <summary>
    /// Classe do Modelo de Comprovante de DARF CÓDIGO DE BARRAS (BRADESCO)
    /// </summary>
    public partial class ComprDARFCB_Bradesco : DevExpress.XtraReports.UI.XtraReport
    {
        /// <summary>
        /// Modelo de Comprovante de DARF CÓDIGO DE BARRAS (BRADESCO)
        /// </summary>
        public ComprDARFCB_Bradesco()
        {
            InitializeComponent();
        }
    }
}
