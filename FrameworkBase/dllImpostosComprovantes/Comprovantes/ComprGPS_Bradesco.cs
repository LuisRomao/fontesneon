﻿using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using DevExpress.XtraReports.UI;

namespace dllImpostos.Comprovantes
{
    /// <summary>
    /// Classe do Modelo de Comprovante de GPS (BRADESCO)
    /// </summary>
    public partial class ComprGPS_Bradesco : DevExpress.XtraReports.UI.XtraReport
    {
        /// <summary>
        /// Modelo de Comprovante de GPS (BRADESCO)
        /// </summary>
        public ComprGPS_Bradesco()
        {
            InitializeComponent();
        }

    }
}
