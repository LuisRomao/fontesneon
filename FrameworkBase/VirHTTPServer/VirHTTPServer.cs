﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using Microsoft.Win32;
using System.ComponentModel;

namespace VirHTTPServer
{
    /// <summary>
    /// Objeto para HTTPServer embedded
    /// </summary>
    public class VirHTTPServer : CsHTTPServer
    {        

        /// <summary>
        /// Local dos arquivos
        /// </summary>
        public string RepositorioPadrao;

        /// <summary>
        /// delegate para chamada
        /// </summary>
        /// <param name="Args"></param>
        /// <param name="PostArg"></param>
        /// <param name="Arquivo"></param>
        /// <param name="Conteudo"></param>
        /// <returns></returns>
        public delegate bool del_chamada(SortedList<string, string> Args, SortedList<string, string> PostArg, out string Conteudo, out string Arquivo);

        /// <summary>
        /// Tratamento de chamada
        /// </summary>        
        private event del_chamada Chamada;

        /// <summary>
        /// Somente leitura
        /// </summary>
        private bool TratarChamadaIncial = false;        

        /// <summary>
        /// Construtor
        /// </summary>
        /// <param name="_RepositorioPadrao"></param>
        /// <param name="Porta"></param>
        /// <param name="_chamada"></param>
        /// <param name="_TratarChamadaIncial"></param>
        public VirHTTPServer(string _RepositorioPadrao = "", int Porta = 8080, del_chamada _chamada = null, bool _TratarChamadaIncial = false)
            : base(Porta)
        {
            if (_RepositorioPadrao != "")
                RepositorioPadrao = _RepositorioPadrao;
            else
            {
                RepositorioPadrao = Path.GetDirectoryName(Environment.CommandLine.Replace("\"", "")) + @"\ModelosHTML";
                if (!Directory.Exists(RepositorioPadrao))
                    Directory.CreateDirectory(RepositorioPadrao);
            }
            if (_chamada != null)
                Chamada = _chamada;
            TratarChamadaIncial = _TratarChamadaIncial;
        }

        /// <summary>
        /// Permite que o usuáro navegue na pasta
        /// </summary>
        public bool PermiteNavegar = false;

        private static Encoding _Encoding1252;

        /// <summary>
        /// Encoding
        /// </summary>
        public static Encoding Encoding1252 { get { return _Encoding1252 == null ? _Encoding1252 = Encoding.GetEncoding("Windows-1252") : _Encoding1252; } }

        /// <summary>
        /// Colocao o string dentro de uma página html
        /// </summary>
        /// <param name="conteudo"></param>
        /// <returns></returns>
        public static string GeraPaginaSimples(string conteudo)
        {            
            conteudo = System.Web.HttpUtility.HtmlEncode(conteudo);            
            

            return                          "<!DOCTYPE HTML PUBLIC \"-//W3C//DTD HTML 4.0 Transitional//EN\">\n" +
                                            "<HTML><HEAD>\n" +
                                            "<META http-equiv=Content-Type content=\"text/html; charset=windows-1252\">\n" +
                                            "</HEAD>\n" +
                                            "<BODY>" +
                                            conteudo +
                                            "</BODY></HTML>\n";           
        }

        /// <summary>
        /// Gera uma página com o conteúdo
        /// </summary>
        /// <param name="conteudo"></param>
        /// <returns></returns>
        public static byte[] GeraPaginaSimples_bytes(string conteudo)
        {
            conteudo = System.Web.HttpUtility.HtmlEncode(conteudo);


            return Encoding1252.GetBytes(   "<!DOCTYPE HTML PUBLIC \"-//W3C//DTD HTML 4.0 Transitional//EN\">\n" +
                                            "<HTML><HEAD>\n" +
                                            "<META http-equiv=Content-Type content=\"text/html; charset=windows-1252\">\n" +
                                            "</HEAD>\n" +
                                            "<BODY>" +
                                            conteudo +
                                            "</BODY></HTML>\n");
        }

        //private bool testeAndroid = true;

        /// <summary>
        /// Tratamento
        /// </summary>
        /// <param name="rq"></param>
        /// <param name="rp"></param>
        public override void OnResponse(ref HTTPRequestStruct rq, ref HTTPResponseStruct rp)
        {
            string URL = rq.URL.Replace("/", "\\");
            if (URL.StartsWith("\\"))
                URL = URL.Substring(1, URL.Length - 1);
            WriteLog(NivelLog.compacto, ">>> URL: <{0}>", URL);
            if (rq.Args != null)
            {
                WriteLog(NivelLog.completo,"   >>>Argumentos");
                foreach (string chave in rq.Args.Keys)
                    WriteLog(NivelLog.completo,"       {0}:{1}", chave, rq.Args[chave]);
            }
            if (rq.PostArg != null)
            {
                WriteLog(NivelLog.completo,"   >>>Post Argumentos");
                foreach (string chave in rq.PostArg.Keys)
                    WriteLog(NivelLog.completo, "       {0}:{1}", chave, rq.PostArg[chave]);
            }
            string path = string.Format("{0}\\{1}", RepositorioPadrao, URL);
            if ((Chamada != null) && (URL == "VIRTUAL"))
            {
                WriteLog(NivelLog.compacto, ">>> VIRTUAL");
                string ConteudoEnviar;
                string ArquivoEnviar;
                if (Chamada(rq.Args, rq.PostArg, out ConteudoEnviar, out ArquivoEnviar))
                {
                    if (ConteudoEnviar != null)
                    {
                        WriteLog(NivelLog.compacto, ">>> Tratado conteúdo");
                        rp.BodyData = VirHTTPServer.Encoding1252.GetBytes(ConteudoEnviar);
                        return;
                    }
                    if (ArquivoEnviar != null)
                    {
                        WriteLog(NivelLog.compacto, ">>> Tratado arquivo");
                        if(ArquivoEnviar.Substring(0,1) == "\\")
                            path = string.Format("{0}{1}", RepositorioPadrao, ArquivoEnviar);
                        else
                            path = ArquivoEnviar;
                    }
                }
                else
                {
                    WriteLog(NivelLog.compacto, ">>> Não tratado");   
                    rp.BodyData = Encoding1252.GetBytes(GeraPaginaSimples("Não tratado"));
                    rp.status = (int)RespState.NOT_FOUND;
                    return;
                }
            }
            
            

            if (Directory.Exists(path))
            {
                if (File.Exists(path + "default.htm"))
                    path += "default.htm";
                else
                {
                    WriteLog(NivelLog.compacto,">>> PASTA");
                    if (PermiteNavegar)
                    {
                        string[] dirs = Directory.GetDirectories(path);
                        string[] files = Directory.GetFiles(path);

                        string bodyStr = "<p>Folder listing, to do not see this add a 'default.htm' document\n<p>\n";
                        for (int i = 0; i < dirs.Length; i++)
                            bodyStr += "<br><a href = \"" + rq.URL + Path.GetFileName(dirs[i]) + "/\">[" + Path.GetFileName(dirs[i]) + "]</a>\n";
                        for (int i = 0; i < files.Length; i++)
                            bodyStr += "<br><a href = \"" + rq.URL + Path.GetFileName(files[i]) + "\">" + Path.GetFileName(files[i]) + "</a>\n";
                        rp.BodyData = GeraPaginaSimples_bytes(bodyStr);
                        
                    }
                    else
                    {
                        rp.status = (int)RespState.BAD_REQUEST;
                        rp.BodyData = GeraPaginaSimples_bytes("Navegação não permitida");
                    }                    
                    return;
                }
            }

            if (File.Exists(path))
            {
                WriteLog(NivelLog.compacto, ">>> ARQUIVO OK: " + path);              
                string s = MimeMapping.GetMimeMapping(Path.GetExtension(path));

                // Open the stream and read it back.
                rp.fs = File.Open(path, FileMode.Open);
                if (s != "")
                    rp.Headers["Content-type"] = s;
                //if (testeAndroid)
                //    rp.Headers["Content-Disposition"] = "attachment; filename=\"TESTE.PDF\"";
            }
            else
            {
                WriteLog(NivelLog.compacto, ">>> ARQUIVO NÃO ENCONTRADO: <{0}>", path);
                rp.status = (int)RespState.NOT_FOUND;
                rp.BodyData = GeraPaginaSimples_bytes("Arquivo não encontrado");

            }
        }

    }

    

    internal class MimeMapping
    {
        private static Dictionary<string, string> ExtensionMap = new Dictionary<string, string>();

        static MimeMapping()
        {
            ExtensionMap.Add(".323", "text/h323");
            ExtensionMap.Add(".asx", "video/x-ms-asf");
            ExtensionMap.Add(".acx", "application/internet-property-stream");
            ExtensionMap.Add(".ai", "application/postscript");
            ExtensionMap.Add(".aif", "audio/x-aiff");
            ExtensionMap.Add(".aiff", "audio/aiff");
            ExtensionMap.Add(".axs", "application/olescript");
            ExtensionMap.Add(".aifc", "audio/aiff");
            ExtensionMap.Add(".asr", "video/x-ms-asf");
            ExtensionMap.Add(".avi", "video/x-msvideo");
            ExtensionMap.Add(".asf", "video/x-ms-asf");
            ExtensionMap.Add(".au", "audio/basic");
            ExtensionMap.Add(".application", "application/x-ms-application");
            ExtensionMap.Add(".bin", "application/octet-stream");
            ExtensionMap.Add(".bas", "text/plain");
            ExtensionMap.Add(".bcpio", "application/x-bcpio");
            ExtensionMap.Add(".bmp", "image/bmp");
            ExtensionMap.Add(".cdf", "application/x-cdf");
            ExtensionMap.Add(".cat", "application/vndms-pkiseccat");
            ExtensionMap.Add(".crt", "application/x-x509-ca-cert");
            ExtensionMap.Add(".c", "text/plain");
            ExtensionMap.Add(".css", "text/css");
            ExtensionMap.Add(".cer", "application/x-x509-ca-cert");
            ExtensionMap.Add(".crl", "application/pkix-crl");
            ExtensionMap.Add(".cmx", "image/x-cmx");
            ExtensionMap.Add(".csh", "application/x-csh");
            ExtensionMap.Add(".cod", "image/cis-cod");
            ExtensionMap.Add(".cpio", "application/x-cpio");
            ExtensionMap.Add(".clp", "application/x-msclip");
            ExtensionMap.Add(".crd", "application/x-mscardfile");
            ExtensionMap.Add(".deploy", "application/octet-stream");
            ExtensionMap.Add(".dll", "application/x-msdownload");
            ExtensionMap.Add(".dot", "application/msword");
            ExtensionMap.Add(".doc", "application/msword");
            ExtensionMap.Add(".dvi", "application/x-dvi");
            ExtensionMap.Add(".dir", "application/x-director");
            ExtensionMap.Add(".dxr", "application/x-director");
            ExtensionMap.Add(".der", "application/x-x509-ca-cert");
            ExtensionMap.Add(".dib", "image/bmp");
            ExtensionMap.Add(".dcr", "application/x-director");
            ExtensionMap.Add(".disco", "text/xml");
            ExtensionMap.Add(".exe", "application/octet-stream");
            ExtensionMap.Add(".etx", "text/x-setext");
            ExtensionMap.Add(".evy", "application/envoy");
            ExtensionMap.Add(".eml", "message/rfc822");
            ExtensionMap.Add(".eps", "application/postscript");
            ExtensionMap.Add(".flr", "x-world/x-vrml");
            ExtensionMap.Add(".fif", "application/fractals");
            ExtensionMap.Add(".gtar", "application/x-gtar");
            ExtensionMap.Add(".gif", "image/gif");
            ExtensionMap.Add(".gz", "application/x-gzip");
            ExtensionMap.Add(".hta", "application/hta");
            ExtensionMap.Add(".htc", "text/x-component");
            ExtensionMap.Add(".htt", "text/webviewhtml");
            ExtensionMap.Add(".h", "text/plain");
            ExtensionMap.Add(".hdf", "application/x-hdf");
            ExtensionMap.Add(".hlp", "application/winhlp");
            ExtensionMap.Add(".html", "text/html");
            ExtensionMap.Add(".htm", "text/html");
            ExtensionMap.Add(".hqx", "application/mac-binhex40");
            ExtensionMap.Add(".isp", "application/x-internet-signup");
            ExtensionMap.Add(".iii", "application/x-iphone");
            ExtensionMap.Add(".ief", "image/ief");
            ExtensionMap.Add(".ivf", "video/x-ivf");
            ExtensionMap.Add(".ins", "application/x-internet-signup");
            ExtensionMap.Add(".ico", "image/x-icon");
            ExtensionMap.Add(".jpg", "image/jpeg");
            ExtensionMap.Add(".jfif", "image/pjpeg");
            ExtensionMap.Add(".jpe", "image/jpeg");
            ExtensionMap.Add(".jpeg", "image/jpeg");
            ExtensionMap.Add(".js", "application/x-javascript");
            ExtensionMap.Add(".lsx", "video/x-la-asf");
            ExtensionMap.Add(".latex", "application/x-latex");
            ExtensionMap.Add(".lsf", "video/x-la-asf");
            ExtensionMap.Add(".manifest", "application/x-ms-manifest");
            ExtensionMap.Add(".mhtml", "message/rfc822");
            ExtensionMap.Add(".mny", "application/x-msmoney");
            ExtensionMap.Add(".mht", "message/rfc822");
            ExtensionMap.Add(".mid", "audio/mid");
            ExtensionMap.Add(".mpv2", "video/mpeg");
            ExtensionMap.Add(".man", "application/x-troff-man");
            ExtensionMap.Add(".mvb", "application/x-msmediaview");
            ExtensionMap.Add(".mpeg", "video/mpeg");
            ExtensionMap.Add(".m3u", "audio/x-mpegurl");
            ExtensionMap.Add(".mdb", "application/x-msaccess");
            ExtensionMap.Add(".mpp", "application/vnd.ms-project");
            ExtensionMap.Add(".m1v", "video/mpeg");
            ExtensionMap.Add(".mpa", "video/mpeg");
            ExtensionMap.Add(".me", "application/x-troff-me");
            ExtensionMap.Add(".m13", "application/x-msmediaview");
            ExtensionMap.Add(".movie", "video/x-sgi-movie");
            ExtensionMap.Add(".m14", "application/x-msmediaview");
            ExtensionMap.Add(".mpe", "video/mpeg");
            ExtensionMap.Add(".mp2", "video/mpeg");
            ExtensionMap.Add(".mov", "video/quicktime");
            ExtensionMap.Add(".mp3", "audio/mpeg");
            ExtensionMap.Add(".mpg", "video/mpeg");
            ExtensionMap.Add(".ms", "application/x-troff-ms");
            ExtensionMap.Add(".nc", "application/x-netcdf");
            ExtensionMap.Add(".nws", "message/rfc822");
            ExtensionMap.Add(".oda", "application/oda");
            ExtensionMap.Add(".ods", "application/oleobject");
            ExtensionMap.Add(".pmc", "application/x-perfmon");
            ExtensionMap.Add(".p7r", "application/x-pkcs7-certreqresp");
            ExtensionMap.Add(".p7b", "application/x-pkcs7-certificates");
            ExtensionMap.Add(".p7s", "application/pkcs7-signature");
            ExtensionMap.Add(".pmw", "application/x-perfmon");
            ExtensionMap.Add(".ps", "application/postscript");
            ExtensionMap.Add(".p7c", "application/pkcs7-mime");
            ExtensionMap.Add(".pbm", "image/x-portable-bitmap");
            ExtensionMap.Add(".ppm", "image/x-portable-pixmap");
            ExtensionMap.Add(".pub", "application/x-mspublisher");
            ExtensionMap.Add(".pnm", "image/x-portable-anymap");
            ExtensionMap.Add(".pml", "application/x-perfmon");
            ExtensionMap.Add(".p10", "application/pkcs10");
            ExtensionMap.Add(".pfx", "application/x-pkcs12");
            ExtensionMap.Add(".p12", "application/x-pkcs12");
            ExtensionMap.Add(".pdf", "application/pdf");
            ExtensionMap.Add(".pps", "application/vnd.ms-powerpoint");
            ExtensionMap.Add(".p7m", "application/pkcs7-mime");
            ExtensionMap.Add(".pko", "application/vndms-pkipko");
            ExtensionMap.Add(".ppt", "application/vnd.ms-powerpoint");
            ExtensionMap.Add(".pmr", "application/x-perfmon");
            ExtensionMap.Add(".pma", "application/x-perfmon");
            ExtensionMap.Add(".pot", "application/vnd.ms-powerpoint");
            ExtensionMap.Add(".prf", "application/pics-rules");
            ExtensionMap.Add(".pgm", "image/x-portable-graymap");
            ExtensionMap.Add(".qt", "video/quicktime");
            ExtensionMap.Add(".ra", "audio/x-pn-realaudio");
            ExtensionMap.Add(".rgb", "image/x-rgb");
            ExtensionMap.Add(".ram", "audio/x-pn-realaudio");
            ExtensionMap.Add(".rmi", "audio/mid");
            ExtensionMap.Add(".ras", "image/x-cmu-raster");
            ExtensionMap.Add(".roff", "application/x-troff");
            ExtensionMap.Add(".rtf", "application/rtf");
            ExtensionMap.Add(".rtx", "text/richtext");
            ExtensionMap.Add(".sv4crc", "application/x-sv4crc");
            ExtensionMap.Add(".spc", "application/x-pkcs7-certificates");
            ExtensionMap.Add(".setreg", "application/set-registration-initiation");
            ExtensionMap.Add(".snd", "audio/basic");
            ExtensionMap.Add(".stl", "application/vndms-pkistl");
            ExtensionMap.Add(".setpay", "application/set-payment-initiation");
            ExtensionMap.Add(".stm", "text/html");
            ExtensionMap.Add(".shar", "application/x-shar");
            ExtensionMap.Add(".sh", "application/x-sh");
            ExtensionMap.Add(".sit", "application/x-stuffit");
            ExtensionMap.Add(".spl", "application/futuresplash");
            ExtensionMap.Add(".sct", "text/scriptlet");
            ExtensionMap.Add(".scd", "application/x-msschedule");
            ExtensionMap.Add(".sst", "application/vndms-pkicertstore");
            ExtensionMap.Add(".src", "application/x-wais-source");
            ExtensionMap.Add(".sv4cpio", "application/x-sv4cpio");
            ExtensionMap.Add(".tex", "application/x-tex");
            ExtensionMap.Add(".tgz", "application/x-compressed");
            ExtensionMap.Add(".t", "application/x-troff");
            ExtensionMap.Add(".tar", "application/x-tar");
            ExtensionMap.Add(".tr", "application/x-troff");
            ExtensionMap.Add(".tif", "image/tiff");
            ExtensionMap.Add(".txt", "text/plain");
            ExtensionMap.Add(".texinfo", "application/x-texinfo");
            ExtensionMap.Add(".trm", "application/x-msterminal");
            ExtensionMap.Add(".tiff", "image/tiff");
            ExtensionMap.Add(".tcl", "application/x-tcl");
            ExtensionMap.Add(".texi", "application/x-texinfo");
            ExtensionMap.Add(".tsv", "text/tab-separated-values");
            ExtensionMap.Add(".ustar", "application/x-ustar");
            ExtensionMap.Add(".uls", "text/iuls");
            ExtensionMap.Add(".vcf", "text/x-vcard");
            ExtensionMap.Add(".wps", "application/vnd.ms-works");
            ExtensionMap.Add(".wav", "audio/wav");
            ExtensionMap.Add(".wrz", "x-world/x-vrml");
            ExtensionMap.Add(".wri", "application/x-mswrite");
            ExtensionMap.Add(".wks", "application/vnd.ms-works");
            ExtensionMap.Add(".wmf", "application/x-msmetafile");
            ExtensionMap.Add(".wcm", "application/vnd.ms-works");
            ExtensionMap.Add(".wrl", "x-world/x-vrml");
            ExtensionMap.Add(".wdb", "application/vnd.ms-works");
            ExtensionMap.Add(".wsdl", "text/xml");
            ExtensionMap.Add(".xml", "text/xml");
            ExtensionMap.Add(".xlm", "application/vnd.ms-excel");
            ExtensionMap.Add(".xaf", "x-world/x-vrml");
            ExtensionMap.Add(".xla", "application/vnd.ms-excel");
            ExtensionMap.Add(".xls", "application/vnd.ms-excel");
            ExtensionMap.Add(".xof", "x-world/x-vrml");
            ExtensionMap.Add(".xlt", "application/vnd.ms-excel");
            ExtensionMap.Add(".xlc", "application/vnd.ms-excel");
            ExtensionMap.Add(".xsl", "text/xml");
            ExtensionMap.Add(".xbm", "image/x-xbitmap");
            ExtensionMap.Add(".xlw", "application/vnd.ms-excel");
            ExtensionMap.Add(".xpm", "image/x-xpixmap");
            ExtensionMap.Add(".xwd", "image/x-xwindowdump");
            ExtensionMap.Add(".xsd", "text/xml");
            ExtensionMap.Add(".z", "application/x-compress");
            ExtensionMap.Add(".zip", "application/x-zip-compressed");
            ExtensionMap.Add(".*", "application/octet-stream");
        }

        public static string GetMimeMapping(string fileExtension)
        {
            if (ExtensionMap.ContainsKey(fileExtension))
                return ExtensionMap[fileExtension];
            else
                return ExtensionMap[".*"];
        }
    }

}

