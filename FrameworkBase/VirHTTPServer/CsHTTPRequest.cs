using System;
using System.Net.Sockets;
using System.Text;
using System.Threading;
using System.Collections;
using System.Collections.Generic;
using System.Globalization;
using System.Web;

namespace VirHTTPServer
{
	enum RState
	{
		METHOD, URL, URLPARM, URLVALUE, VERSION, 
		HEADERKEY, HEADERVALUE, BODY, OK
	};

	enum RespState
	{
		OK = 200, 
		BAD_REQUEST = 400,
		NOT_FOUND = 404
	}

    internal enum Methods
    { GET,POST,ERRO,INDEFINIDO}

    /// <summary>
    /// Request
    /// </summary>
	public struct HTTPRequestStruct
	{
        /// <summary>
        /// 
        /// </summary>
		public string Method;
        /// <summary>
        /// 
        /// </summary>
		public string URL;
        /// <summary>
        /// 
        /// </summary>
		public string Version;
        /// <summary>
        /// 
        /// </summary>
        public SortedList<string, string> Args;
        /// <summary>
        /// 
        /// </summary>
		public bool Execute;
        /// <summary>
        /// 
        /// </summary>
        public SortedList<string, string> Headers;
        /// <summary>
        /// 
        /// </summary>
		public int BodySize;
        /// <summary>
        /// 
        /// </summary>
		public byte[] BodyData;
        /// <summary>
        /// Parametros via post
        /// </summary>
        public SortedList<string,string> PostArg;
	}

    /// <summary>
    /// Response
    /// </summary>
    public struct HTTPResponseStruct
	{
        /// <summary>
        /// 
        /// </summary>
		public int status;
        /// <summary>
        /// 
        /// </summary>
		public string version;
        /// <summary>
        /// 
        /// </summary>
		public Hashtable Headers;
        /// <summary>
        /// 
        /// </summary>
		public int BodySize;
        /// <summary>
        /// 
        /// </summary>
		public byte[] BodyData;
        /// <summary>
        /// 
        /// </summary>
		public System.IO.FileStream fs;
	}

	/// <summary>
	/// Summary description for CsHTTPRequest.
	/// </summary>
	public class CsHTTPRequest
	{
		private TcpClient client;

		private RState ParserState;

		private HTTPRequestStruct HTTPRequest;

		private HTTPResponseStruct HTTPResponse;

        internal Methods Metodo
        {
            get 
            {
                if (HTTPRequest.Method == null)
                    return Methods.INDEFINIDO;
                else if (HTTPRequest.Method.ToUpper() == "GET")
                    return Methods.GET;
                else if (HTTPRequest.Method.ToUpper() == "POST")
                    return Methods.POST;
                else return Methods.ERRO;
            }
        }

		byte[] myReadBuffer;

		CsHTTPServer Parent;

        /// <summary>
        /// Construtor
        /// </summary>
        /// <param name="client"></param>
        /// <param name="Parent"></param>
		public CsHTTPRequest(TcpClient client, CsHTTPServer Parent) 
		{
			this.client = client;
			this.Parent = Parent;

			this.HTTPResponse.BodySize = 0;
		}

        private void GuardaValor(SortedList<string, string> Colecao, string hKey, string hValue)
        {
            hKey = HttpUtility.UrlDecode(hKey);
            hValue = HttpUtility.UrlDecode(hValue);
            if (Colecao.ContainsKey(hKey))
                Colecao[hKey] += ", " + hValue;
            else
                Colecao.Add(hKey, hValue);
        }

        /// <summary>
        /// Process
        /// </summary>
		public void Process()
		{
			myReadBuffer = new byte[client.ReceiveBufferSize];
			String myCompleteMessage = "";
			int numberOfBytesRead = 0;

			Parent.WriteLog(CsHTTPServer.NivelLog.compacto, "Connection accepted. Buffer: " + client.ReceiveBufferSize.ToString());
			NetworkStream ns = client.GetStream();

			string hValue = "";
			string hKey = "";

			try 
			{
				// binary data buffer index
				int bfndx = 0;

				// Incoming message may be larger than the buffer size.
				do
				{
					numberOfBytesRead = ns.Read(myReadBuffer, 0, myReadBuffer.Length);  
					myCompleteMessage = 
						String.Concat(myCompleteMessage, Encoding.ASCII.GetString(myReadBuffer, 0, numberOfBytesRead));  
					
					// read buffer index
					int ndx = 0;
					do
					{
						switch ( ParserState )
						{
							case RState.METHOD:
								if (myReadBuffer[ndx] != ' ')
									HTTPRequest.Method += (char)myReadBuffer[ndx++];
								else 
								{
									ndx++;
									ParserState = RState.URL;
								}
								break;
							case RState.URL:
								if (myReadBuffer[ndx] == '?')
								{
									ndx++;
									hKey = "";
									HTTPRequest.Execute = true;
									//HTTPRequest.Args = new Hashtable();
                                    HTTPRequest.Args = new SortedList<string, string>();
									ParserState = RState.URLPARM;
								}
								else if (myReadBuffer[ndx] != ' ')
									HTTPRequest.URL += (char)myReadBuffer[ndx++];
								else
								{
									ndx++;
									HTTPRequest.URL = HttpUtility.UrlDecode(HTTPRequest.URL);
									ParserState = RState.VERSION;
								}
								break;
							case RState.URLPARM:
								if (myReadBuffer[ndx] == '=')
								{
									ndx++;
									hValue="";
									ParserState = RState.URLVALUE;
								}
								else if (myReadBuffer[ndx] == ' ')
								{
									ndx++;

									HTTPRequest.URL = HttpUtility.UrlDecode(HTTPRequest.URL);
									ParserState = RState.VERSION;
								}
								else
								{
									hKey += (char)myReadBuffer[ndx++];
								}
								break;
							case RState.URLVALUE:
								if (myReadBuffer[ndx] == '&')
								{
									ndx++;
                                    GuardaValor(HTTPRequest.Args,hKey,hValue);
                                    /*
									hKey=HttpUtility.UrlDecode(hKey);
									hValue=HttpUtility.UrlDecode(hValue);
                                    if (HTTPRequest.Args.ContainsKey(hKey))
                                        HTTPRequest.Args[hKey] += ", " + hValue;
                                    else
                                        HTTPRequest.Args.Add(hKey, hValue);
                                    */
									hKey="";
									ParserState = RState.URLPARM;
								}
								else if (myReadBuffer[ndx] == ' ')
								{
									ndx++;
                                    GuardaValor(HTTPRequest.Args, hKey, hValue);
									
									HTTPRequest.URL = HttpUtility.UrlDecode(HTTPRequest.URL);
									ParserState = RState.VERSION;
								}
								else
								{
									hValue += (char)myReadBuffer[ndx++];
								}
								break;
							case RState.VERSION:
								if (myReadBuffer[ndx] == '\r') 
									ndx++;
								else if (myReadBuffer[ndx] != '\n') 
									HTTPRequest.Version += (char)myReadBuffer[ndx++];
								else 
								{
									ndx++;
									hKey = "";
                                    HTTPRequest.Headers = new SortedList<string, string>();
									ParserState = RState.HEADERKEY;
								}
								break;
							case RState.HEADERKEY:
								if (myReadBuffer[ndx] == '\r') 
									ndx++;
								else if (myReadBuffer[ndx] == '\n')
								{
									ndx++;
									if (HTTPRequest.Headers.ContainsKey("Content-Length"))
									{
										HTTPRequest.BodySize = Convert.ToInt32(HTTPRequest.Headers["Content-Length"]);
										this.HTTPRequest.BodyData = new byte[this.HTTPRequest.BodySize];
										ParserState = RState.BODY;
									}
									else
										ParserState = RState.OK;
									
								}
								else if (myReadBuffer[ndx] == ':')
									ndx++;
								else if (myReadBuffer[ndx] != ' ')
									hKey += (char)myReadBuffer[ndx++];
								else 
								{
									ndx++;
									hValue = "";
									ParserState = RState.HEADERVALUE;
								}
								break;
							case RState.HEADERVALUE:
								if (myReadBuffer[ndx] == '\r') 
									ndx++;
								else if (myReadBuffer[ndx] != '\n')
									hValue += (char)myReadBuffer[ndx++];
								else 
								{
									ndx++;
									HTTPRequest.Headers.Add(hKey, hValue);
									hKey = "";
									ParserState = RState.HEADERKEY;
								}
								break;
							case RState.BODY:
								// Append to request BodyData
								Array.Copy(myReadBuffer, ndx, this.HTTPRequest.BodyData, bfndx, numberOfBytesRead - ndx);
								bfndx += numberOfBytesRead - ndx;
								ndx = numberOfBytesRead;
								if ( this.HTTPRequest.BodySize <=  bfndx)
								{
									ParserState = RState.OK;
								}
								break;
								//default:
								//	ndx++;
								//	break;

						}
					}
					while(ndx < numberOfBytesRead);

				}
				while(ns.DataAvailable);

                if (Metodo == Methods.POST)
                {
                    HTTPRequest.PostArg = new SortedList<string, string>();
                    string LinhaCompleta = Encoding.ASCII.GetString(HTTPRequest.BodyData, 0, HTTPRequest.BodySize);
                    string[] Parametros = LinhaCompleta.Split(new char[]{'&'},StringSplitOptions.RemoveEmptyEntries);
                    foreach (string Parametro in Parametros)
                    {
                        string[] Partes = Parametro.Split(new char[] {'='},StringSplitOptions.RemoveEmptyEntries);
                        if (Partes.Length == 2)
                            HTTPRequest.PostArg.Add(Partes[0], Partes[1]);
                    }
                    
                }

				// Print out the received message to the console.
				Parent.WriteLog(CsHTTPServer.NivelLog.completo, "ORIGINAL :{0}", myCompleteMessage);
				
				HTTPResponse.version = "HTTP/1.1";

				if (ParserState != RState.OK)
					HTTPResponse.status = (int)RespState.BAD_REQUEST;
				else
					HTTPResponse.status = (int)RespState.OK;

				this.HTTPResponse.Headers = new Hashtable();
				this.HTTPResponse.Headers.Add("Server", Parent.Name);
				this.HTTPResponse.Headers.Add("Date", DateTime.Now.ToString("r"));
				
				// if (HTTPResponse.status == (int)RespState.OK)
				this.Parent.OnResponse(ref this.HTTPRequest, ref this.HTTPResponse);

				string HeadersString = this.HTTPResponse.version + " " + this.Parent.respStatus[this.HTTPResponse.status] + "\n";
				
				foreach (DictionaryEntry Header in this.HTTPResponse.Headers) 
				{
					HeadersString += Header.Key + ": " + Header.Value + "\n";
				}

				HeadersString += "\n";
				byte[] bHeadersString = Encoding.ASCII.GetBytes(HeadersString);

				// Send headers	
				ns.Write(bHeadersString, 0, bHeadersString.Length);
                Parent.WriteLog(CsHTTPServer.NivelLog.completo, "Cabe�alho:\r\n" + HeadersString);
	
				// Send body
				if (this.HTTPResponse.BodyData != null)
				ns.Write(this.HTTPResponse.BodyData, 0, this.HTTPResponse.BodyData.Length);

				if (this.HTTPResponse.fs != null)
					using (this.HTTPResponse.fs) 
					{
						byte[] b = new byte[client.SendBufferSize];
						int bytesRead;
						while ((bytesRead = this.HTTPResponse.fs.Read(b,0,b.Length)) > 0) 
						{
							ns.Write(b, 0, bytesRead);
						}
						
						this.HTTPResponse.fs.Close();
					}
	
			}
			catch (Exception e) 
			{
				Parent.WriteLog(CsHTTPServer.NivelLog.soerros, e.ToString()+"\r\n"+e.StackTrace);
			}
			finally 
			{
				ns.Close();
				client.Close();
				if (this.HTTPResponse.fs != null)
					this.HTTPResponse.fs.Close();
                
				Thread.CurrentThread.Abort();
                
			}
		}
				
	}
}
