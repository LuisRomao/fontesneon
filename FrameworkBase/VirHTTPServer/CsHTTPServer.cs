using System;
using System.Net.Sockets;
using System.Threading;
using System.Collections;
using System.Net;



namespace VirHTTPServer
{
	/// <summary>
	/// CsHTTPServer.
	/// </summary>
	public abstract class CsHTTPServer
	{
		private int portNum = 8080;
		private TcpListener listener;
		System.Threading.Thread Thread;

        /// <summary>
        /// URL para chamar o servi�o
        /// </summary>
        /// <returns></returns>
        public string URL()
        {
            if (portNum != 80)
                return string.Format("http://{0}:{1}", Endereco(), portNum);
            else
                return string.Format("http://{0}", Endereco());
        }

        /// <summary>
        /// IP da esta��o
        /// </summary>
        /// <returns></returns>
        public static string Endereco()
        {
            IPHostEntry host;            
            host = Dns.GetHostEntry(Dns.GetHostName());
            foreach (IPAddress ip in host.AddressList)
            {
                if (ip.AddressFamily == AddressFamily.InterNetwork)                
                    return ip.ToString();
                
            }
            return "";
        }

        /// <summary>
        /// 
        /// </summary>
		public Hashtable respStatus;

        /// <summary>
        /// Nome
        /// </summary>
		public string Name = "VirHTTPServer";

        /// <summary>
        /// Ativo
        /// </summary>
		public bool IsAlive
		{
			get 
			{
				return this.Thread.IsAlive; 
			}
		}

        /*
        /// <summary>
        /// Construtor
        /// </summary>
		public CsHTTPServer()
		{
			//
			respStatusInit();
		}*/

        /// <summary>
        /// Construtor
        /// </summary>
        /// <param name="thePort"></param>
		public CsHTTPServer(int thePort)
		{
			portNum = thePort;
			respStatusInit();
		}

		private void respStatusInit()
		{
			respStatus = new Hashtable();
			
			respStatus.Add(200, "200 Ok");
			respStatus.Add(201, "201 Created");
			respStatus.Add(202, "202 Accepted");
			respStatus.Add(204, "204 No Content");

			respStatus.Add(301, "301 Moved Permanently");
			respStatus.Add(302, "302 Redirection");
			respStatus.Add(304, "304 Not Modified");
			
			respStatus.Add(400, "400 Bad Request");
			respStatus.Add(401, "401 Unauthorized");
			respStatus.Add(403, "403 Forbidden");
			respStatus.Add(404, "404 Not Found");

			respStatus.Add(500, "500 Internal Server Error");
			respStatus.Add(501, "501 Not Implemented");
			respStatus.Add(502, "502 Bad Gateway");
			respStatus.Add(503, "503 Service Unavailable");
		}

        /// <summary>
        /// Listen
        /// </summary>
		public void Listen() 
		{

			bool done = false;

            listener = new TcpListener(System.Net.IPAddress.Any,portNum);
			
			listener.Start();

			WriteLog(NivelLog.compacto, "Listening On: " + portNum.ToString());

			while (!done) 
			{
				WriteLog(NivelLog.compacto, "Waiting for connection...");                
				CsHTTPRequest newRequest = new CsHTTPRequest(listener.AcceptTcpClient(),this);                
				Thread Thread = new Thread(new ThreadStart(newRequest.Process));
				Thread.Name = "HTTP Request";                
				Thread.Start();                
			}

		}

        /// <summary>
        /// Niveis do log
        /// </summary>
        public enum NivelLog 
        { 
            /// <summary>
            /// 
            /// </summary>
            desligado = 0,
            /// <summary>
            /// 
            /// </summary>
            soerros = 1,
            /// <summary>
            /// 
            /// </summary>
            compacto = 2,
            /// <summary>
            /// 
            /// </summary>
            completo = 3
        }

        /// <summary>
        /// N�vel do log
        /// </summary>
        public NivelLog NivelDoLog = NivelLog.soerros;

        /// <summary>
        /// Grava log
        /// </summary>
        /// <param name="Nivel"></param>
        /// <param name="EventMessage"></param>
        /// <param name="arg"></param>
        public void WriteLog(NivelLog Nivel,string EventMessage,params object[] arg)
		{
            if (NivelDoLog >= Nivel)
            {
                
                Console.WriteLine("");
                Console.WriteLine("-----------------------");
                Console.WriteLine(EventMessage, arg);
                Console.WriteLine("-----------------------");
                Console.WriteLine("");
            }
		}

        /// <summary>
        /// Start
        /// </summary>
		public void Start()
		{
			// CsHTTPServer HTTPServer = new CsHTTPServer(portNum);
			this.Thread = new Thread(new ThreadStart(this.Listen));
			this.Thread.Start();
		}

        /// <summary>
        /// Stop
        /// </summary>
		public void Stop()
		{
            if (IsAlive)
            {
                listener.Stop();
                this.Thread.Abort();
            }
		}

        /*
        /// <summary>
        /// Suspend
        /// </summary>
		public void Suspend()
		{
			this.Thread.Suspend();
		}

        /// <summary>
        /// Resume
        /// </summary>
		public void Resume()
		{
			this.Thread.Resume();
		}*/

        /// <summary>
        /// OnResponse
        /// </summary>
        /// <param name="rq"></param>
        /// <param name="rp"></param>
		public abstract void OnResponse(ref HTTPRequestStruct rq, ref HTTPResponseStruct rp);

	}
}
