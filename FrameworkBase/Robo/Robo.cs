﻿using System;
using System.IO;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.InteropServices;

namespace Robo
{
    /// <summary>
    /// Robo - Bradesco
    /// </summary>
    public class Bradesco
    {
        [DllImport("kernel32.dll", EntryPoint = "GetPrivateProfileString")]
        private static extern int GetPrivateProfileString(string lpAppName, string lpKeyName, string lpDefault, StringBuilder lpReturnedString, int nSize, string lpFileName);

        private static int TAMANHO_LL = 4;
        private static int LL_MAX = 524288;

        private static WSWEBTA.WSWEBTAService ws;
        private static bool EmProducao;
        private static int EMP;

        /// <summary>
        /// Construtor
        /// </summary>
        public Bradesco(bool _EmProducao, int _EMP)
        {
            EmProducao = _EmProducao;
            EMP = _EMP;
            ws = new WSWEBTA.WSWEBTAService();
            if (EmProducao)
            {
                ws.Url = @"https://www.webtatransferenciadearquivos.bradesco.com.br/webta/services/WSWEBTA";
                //if (Framework.usuarioLogado.UsuarioLogadoNeon.UsuarioLogadoStNEON != null)
                //    ws.Proxy = Framework.usuarioLogado.UsuarioLogadoNeon.UsuarioLogadoStNEON.Proxy;
            }
            else 
                ws.Url = @"https://localhost/webta/services/WSWEBTA";
        }

        /// <summary>
        /// Processa arquivos
        /// </summary>
        /// <param name="pathRobo">Diretório de instalação (se não for indicado interrompe processo)</param>
        /// <param name="pathEntrada">Diretório de entrada de arquivos (se não for indicado não processa Recepção de Arquivos)</param>
        /// <param name="pathSaida">Diretório de saída de arquivos (se não for indicado não processa Transmissão de Arquivos)</param>
        /// <returns></returns>
        public string Processa(string pathRobo, string pathEntrada, string pathSaida)
        {
            StringBuilder arquivoTransfIni = new StringBuilder(100);
            StringBuilder arquivoCriptoIni = new StringBuilder(100);
            StringBuilder senhaCriptoIni = new StringBuilder(15);
            string arquivoTransf = "";
            string arquivoCripto = "";
            string senhaCripto = "";
            StringBuilder idClienteTransAutom = new StringBuilder(21); 
            byte[] chaveTransferencia = new byte[16];
            byte[] desafioCripto = new byte[100];
            int desafioCriptoLength;
            StringBuilder msgErro = new StringBuilder(512);
            string strArquivo = "";
            string strArquivoFinal = "";
            FileStream fs = null;
            long offSet = 0;
            int recebidos = 0;
            int enviados = 0;
            int erros = 0;

            //Inicia retorno do processamento
            string retorno = "";

            //Pega parametros configurados no "robo_bradesco.ini" se existente
            if (!File.Exists(pathRobo + "robo_bradesco.ini"))
                return string.Format("Robo Bradesco: Não configurado.\r\n");
            else
            {
                GetPrivateProfileString("Arquivos", "Transferencia", "", arquivoTransfIni, 100, pathRobo + "robo_bradesco.ini");
                arquivoTransf = pathRobo + arquivoTransfIni.ToString();
                GetPrivateProfileString("Arquivos", "Criptografia", "", arquivoCriptoIni, 100, pathRobo + "robo_bradesco.ini");
                arquivoCripto = pathRobo + arquivoCriptoIni.ToString();
                GetPrivateProfileString("Senha", "Senha", "", senhaCriptoIni, 15, pathRobo + "robo_bradesco.ini");
                senhaCripto = senhaCriptoIni.ToString();
            }

            //Decriptografa arquivo que contem id do cliente e chave de criptografia do desafio
            if (CriptografiaArq.Bradesco.fDecodeKeyFileEx(arquivoTransf, senhaCripto, idClienteTransAutom, chaveTransferencia, msgErro) == 0)
                return string.Format("Robo Bradesco: Erro ao decodificar Id do Cliente e Chave de Criptografia - mensagem de erro: {0}", msgErro);

            try 
            {
                //Abre sessao recebendo desafio e identificador de sessao
                WSWEBTA.WSSessaoTO wsSessaoTO = ws.criarSessao(idClienteTransAutom.ToString());

                //Criptografa desafio recebido na abertura de sessao
                CriptografiaArq.Bradesco.fEncodeBuffer(Encoding.UTF8.GetBytes(wsSessaoTO.desafio), Encoding.UTF8.GetBytes(wsSessaoTO.desafio).Length, desafioCripto, out desafioCriptoLength, chaveTransferencia);

                //Envia desafio criptografado para o servidor
                byte[] desafioCriptoFinal = new byte[desafioCriptoLength];
                Array.Copy(desafioCripto, desafioCriptoFinal, desafioCriptoLength);
                ws.habilitarSessao(wsSessaoTO.CTRL, desafioCriptoFinal);

                //Verifica se recebe arquivos de retorno
                if (pathEntrada != "")
                {    
                    try
                    {
                        //Verifica arquivos enquanto houver retorno                
                        while (true)
                        {
                            //Obtem primeiro bloco do arquivo
                            WSWEBTA.WSRetornoTO wsRetornoTO = ws.obterBlocoRetorno(wsSessaoTO.CTRL, 0, 0, 8192);

                            //Verifica se nao retornou arquivo para recepcao
                            if (wsRetornoTO.nomeLogicoArquivo == null) 
                                break;
                            else
                            {
                                //Cria o arquivo
                                strArquivo = pathEntrada + @"CRIPTO\" + wsRetornoTO.nomeLogicoArquivo;
                                fs = new FileStream(strArquivo, FileMode.Create);
                        
                                //Monta LL da informacao
                                int tamBuffer = wsRetornoTO.conteudo.Length;
                                byte[] ll = new byte[TAMANHO_LL];
                                for (int i = 0; i < TAMANHO_LL; i++) 
                                {
                                    int offset = (ll.Length - 1 - i) * 8;
                                    ll[i] = (byte) ((tamBuffer >> offset) & 0xFF);
                                }

                                //Grava LL e dados
                                fs.Write(ll,0,ll.Length);
                                fs.Write(wsRetornoTO.conteudo, 0, wsRetornoTO.conteudo.Length);

                                //Verifica quantidade de bytes lidos e total de bytes do arquivo a ser lido 
                                offSet = wsRetornoTO.quantidadeBytesLidos;
                                long totalSize = wsRetornoTO.quantidadeBytesArquivo;

                                //Loop para receber demais blocos do arquivo
                                while (offSet < totalSize) 
                                {
                                    //Pega proximo bloco
                                    wsRetornoTO = ws.obterBlocoRetorno(wsSessaoTO.CTRL, wsRetornoTO.numeroArquivo, offSet, 8192);
                                    tamBuffer = wsRetornoTO.conteudo.Length;

                                    //Monta LL da informacao
                                    for (int i = 0; i < TAMANHO_LL; i++) 
                                    {
                                        int offset = (TAMANHO_LL - 1 - i) * 8;
                                        ll[i] = (byte) ((tamBuffer >> offset) & 0xFF);
                                    }

                                    //Grava LL e dados
                                    fs.Write(ll, 0, ll.Length);
                                    fs.Write(wsRetornoTO.conteudo, 0, wsRetornoTO.conteudo.Length);

                                    //Verifica quantidade de bytes lidos
                                    offSet += wsRetornoTO.quantidadeBytesLidos;
                                }

                                //Fecha o arquivo
                                if (fs != null)
                                    fs.Close();
                        
                                //Descriptografa arquivo
                                strArquivoFinal = strArquivo.Replace(@"CRIPTO\", ""); 
                                if (CriptografiaArq.Bradesco.DescriptografaArquivo(arquivoCripto, senhaCripto, strArquivo, ref strArquivoFinal) == 1)
                                    recebidos += 1;
                                else
                                {
                                    erros += 1;
                                    retorno += string.Format("Erro no recebimento de arquivo retorno ({0}) - mensagem de erro: Erro ao descriptografar arquivo.\r\n", strArquivo);
                                }
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        erros += 1;
                        retorno += string.Format("Erro no recebimento de conjunto de arquivos retorno - mensagem de erro: {0}", ex.Message);
                    }
                }

                //Verifica se envia arquivos de remessa
                if (pathSaida != "")
                {
                    //Verifica arquivos enquanto houver remessa
                    DirectoryInfo Dir = new DirectoryInfo(pathSaida);
                    FileInfo[] Files = Dir.GetFiles("*", SearchOption.TopDirectoryOnly);
                    foreach (FileInfo arquivo in Files)
                    {
                        try
                        {
                            //Criptografa arquivo
                            strArquivo = pathSaida + arquivo.Name;
                            strArquivoFinal = pathSaida + @"CRIPTO\" + arquivo.Name;
                            if (CriptografiaArq.Bradesco.CriptografaArquivo(arquivoCripto, senhaCripto, strArquivo, ref strArquivoFinal) == 0)
                            {
                                erros += 1;
                                retorno += string.Format("Erro no envio de arquivo remessa ({0}) - mensagem de erro: Erro ao criptografar arquivo.\r\n", strArquivo);
                                continue;
                            }

                            //Inicia processo de transmissao
                            WSWEBTA.WSRemessaTO wsRemessaTO = ws.obterReinicioTxArquivoRemessa(wsSessaoTO.CTRL, Path.GetFileName(strArquivoFinal));

                            //Le o arquivo
                            fs = new FileStream(strArquivoFinal, FileMode.Open);

                            //Inicia variaveis para envio dos blocos
                            offSet = wsRemessaTO.quantidadeBytesArquivo;
                            int numBloco = 1;
                            bool booUltimoBloco = false;
                            byte[] bloco = null;

                            //Loop de leitura e transmissao do arquivo
                            while (fs.Length - fs.Position > 0) 
                            {
                                //Le ll do bloco de dados
                                int tamLL = obterTamanhoProximoBloco(fs);
                                if ((fs.Length - fs.Position) <= tamLL) 
                                {
                                    booUltimoBloco = true;
                                }
                                bloco = new byte[tamLL];

                                //Le dados
                                fs.Read(bloco, 0, bloco.Length);
                                if (numBloco > wsRemessaTO.ultimoBlocoRecebido) 
                                {
                                    //Transmite demais blocos do arquivo
                                    wsRemessaTO = ws.transmitirBlocoArquivoRemessa(wsSessaoTO.CTRL, Path.GetFileName(strArquivoFinal), bloco, offSet, numBloco, booUltimoBloco);
                                    offSet = wsRemessaTO.offSet;
                                }
                                numBloco++;
                            }

                            //Coloca arquivo original (que foi enviado) no backup, mantendo sempre o ultimo
                            if (File.Exists(pathSaida + @"BACKUP\" + arquivo.Name))
                                File.Delete(pathSaida + @"BACKUP\" + arquivo.Name); 
                            File.Move(strArquivo, pathSaida + @"BACKUP\" + arquivo.Name);
                            enviados += 1;
                        }
                        catch (Exception ex)
                        {
                            erros += 1;
                            retorno += string.Format("Erro no envio de arquivo remessa ({0}) - mensagem de erro: {1}", strArquivo, ex.Message);
                        }
                    }
                }

                //Encerra a sessao
                ws.encerrarSessao(wsSessaoTO.CTRL);

                //Seta mensagem de retorno com resultado do processamento
                retorno = string.Format("Robo Bradesco: Recebidos: {0} - Enviados: {1} - Erros: {2}\r\n{3}", recebidos, enviados, erros, retorno);
            } 
            catch (Exception ex) 
            {
                retorno += string.Format("Robo Bradesco: Erro ao estabelecer conexão Robo Bradesco x WEBTA - mensagem de erro: {0}", ex.Message);
            }

            //Retorna processamento
            return retorno;
        }

        private static int obterTamanhoProximoBloco(Stream bis) 
        {
            //Le o tamanho do proximo bloco
            int read = 0;
            int tamBloco = 0;
            byte[] logicalLength = new byte[TAMANHO_LL];
            
            //Le ll
            read = bis.Read(logicalLength, 0, logicalLength.Length);
            
            //Tamanho minimo do ll
            if (read < TAMANHO_LL)
                throw new IOException("Arquivo inválido - campo LL.\r\n");

            //Converte byte[] em int
            for (int i = 0; i < TAMANHO_LL; i++) 
            {
                int shift = (TAMANHO_LL - 1 - i) * 8;
                tamBloco += (logicalLength[i] & 0x000000FF) << shift;
            }
            if ((tamBloco > LL_MAX) || (tamBloco == 0))
                throw new IOException("Arquivo inválido - tamanho do bloco.\r\n");

            return tamBloco;
        }
    }
}
