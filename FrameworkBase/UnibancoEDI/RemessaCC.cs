using System;
using System.Collections.Generic;
using System.Text;
using ArquivosTXT;
using System.Data;

namespace UnibancoEDI
{
    public class RemessaCC : cTabelaTXT
    {
        //de forem criadas outras classes Remessa passar este procedimendo para uma classe intermediária
        private string CheckHorizontal(int Ag, int cc, decimal valor)
        {
            int Banco = 409;
            int operacao = 5;

            decimal dadosfav = cc + Ag * 10000000000M + Banco * 100000000000000M;

            decimal retorno = (dadosfav + valor * 100M) * operacao;

            retorno = retorno % 1000000000000000000M;
            return retorno.ToString("F0").PadRight(18, '0');
        }

        public dRemessaCC.TabelaDeCreditosDataTable TabelaCreditos {
            get {
                if (DataTable == null) {
                    DataTable = new dRemessaCC.TabelaDeCreditosDataTable();
                }
                return (dRemessaCC.TabelaDeCreditosDataTable)DataTable;
            }
            set {
                DataTable = value;
            }
        }

        public RemessaCC(int Agencia, int conta, int contaDg, string Nome)
        {
            IniciaCampos(Agencia, conta, contaDg, Nome);
        }

        //public void DadosConta(){
        //    //TabelaH.Rows.Add(813, "NEON IMOVEIS", 142737, 2);
        //    TabelaH.Rows.Add(Agencia, Nome, conta, contaDg);
        //    rowCabecalho = TabelaH.Rows[0];
        //}

        private DataTable TabelaH;

        private void IniciaCampos(int Agencia, int conta, int contaDg, string Nome)
        {
            DataTable = TabelaCreditos;
            Modelo = ModelosTXT.layout;            
            TabelaH = new DataTable();
            TabelaH.Columns.Add("NumeroDaAgenciaCliente", typeof(int));
            TabelaH.Columns.Add("NomeCliente", typeof(string));
            TabelaH.Columns.Add("NumeroDaContaCliente", typeof(int));
            TabelaH.Columns.Add("DigitoVerificadorCC", typeof(int));
            //ESTES DADOS NAO DEVEM SER TRAVADOS
            TabelaH.Rows.Add(Agencia, Nome, conta, contaDg);
            rowCabecalho = TabelaH.Rows[0];
            //TabelaH.Rows.Add(813, "NEON IMOVEIS", 142737, 2);            
            RegistraMapaDeA("CodigoDeRegistro", 1, 1, ArquivosTXT.TipoMapa.cabecalho, 0);
            RegistraMapaDeA("CodigoDeRemessa", 2, 2, ArquivosTXT.TipoMapa.cabecalho, 1);
            RegistraMapaDeA("LiteralRemessa", 3, 9, ArquivosTXT.TipoMapa.cabecalho, "REMESSA");
            RegistraMapaDeA("CodigoDeServico", 10, 11, ArquivosTXT.TipoMapa.cabecalho, 8);
            RegistraMapaDeA("Identificacao", 12, 25, ArquivosTXT.TipoMapa.cabecalho, "CONTAS A PAGAR");
            RegistraMapaDeA("NumeroDaAgenciaCliente", 26, 29, ArquivosTXT.TipoMapa.cabecalho, null);
            RegistraMapaDeA("NumeroDaContaCliente", 30, 35, ArquivosTXT.TipoMapa.cabecalho, null);
            RegistraMapaDeA("DigitoVerificadorCC", 36, 36, ArquivosTXT.TipoMapa.cabecalho, null);
            RegistraMapaDeA("NomeCliente", 37, 66, ArquivosTXT.TipoMapa.cabecalho, null);
            RegistraMapaDeA("Banco", 67, 69, ArquivosTXT.TipoMapa.cabecalho, "409");
            RegistraMapaDeA("NomeBanco", 70, 84, ArquivosTXT.TipoMapa.cabecalho, "UNIBANCO");
            RegistraMapaDeA("DataArquivo", 85, 90, ArquivosTXT.TipoMapa.cabecalho, ArquivosTXT.TiposDeCampo.AutoData, null);
            RegistraMapaDeA("Sequencial", 315, 320, ArquivosTXT.TipoMapa.cabecalho, ArquivosTXT.TiposDeCampo.Sequencial, null);


            RegistraMapaDeA("CodigoDeRegistro", 1, 1, ArquivosTXT.TipoMapa.detalhe, 2);
            RegistraMapaDeA("NumeroDaAgenciaCliente", 2, 5, ArquivosTXT.TipoMapa.detalhe, null);
            RegistraMapaDeA("NumeroDaContaCliente", 6, 11, ArquivosTXT.TipoMapa.detalhe, null);
            RegistraMapaDeA("DigitoVerificadorCC", 12, 12, ArquivosTXT.TipoMapa.detalhe, null);
            RegistraMapaDeA("BancoFavorercido", 33, 36, ArquivosTXT.TipoMapa.detalhe, 409);
            RegistraMapaDeA("NumeroDaAgenciaFavorecido", 37, 40, ArquivosTXT.TipoMapa.detalhe, null);
            RegistraMapaDeA("NumeroDaContaFavorecido", 41, 50, ArquivosTXT.TipoMapa.detalhe, null);
            RegistraMapaDeA("MeioDeRepasse", 51, 51, ArquivosTXT.TipoMapa.detalhe, 4);
            RegistraMapaDeA("Aviso", 52, 52, ArquivosTXT.TipoMapa.detalhe, 1);
            RegistraMapaDeA("TipoOper", 53, 53, ArquivosTXT.TipoMapa.detalhe, 5);
            RegistraMapaDeA("TipoServico", 54, 54, ArquivosTXT.TipoMapa.detalhe, 1);
            CampoSomatorio = "ValorCredito";
            RegistraMapaDeA(CampoSomatorio, 55, 67, ArquivosTXT.TipoMapa.detalhe, null);
            RegistraMapaDeA("Ident", 68, 82, ArquivosTXT.TipoMapa.detalhe, 0);
            RegistraMapaDeA("Data", 83, 88, ArquivosTXT.TipoMapa.detalhe, ArquivosTXT.TiposDeCampo.AutoData, null);
            RegistraMapaDeA("DataCredito", 89, 94, ArquivosTXT.TipoMapa.detalhe, ArquivosTXT.TiposDeCampo.normal, DateTime.Today.AddDays(1));
            RegistraMapaDeA("NomeFavorecido", 95, 124, ArquivosTXT.TipoMapa.detalhe, null);
            RegistraMapaDeA("Moeda", 235, 236, ArquivosTXT.TipoMapa.detalhe, 14);
            RegistraMapaDeA("MossoNumero", 248, 258, ArquivosTXT.TipoMapa.detalhe, 0);
            RegistraMapaDeA("Ocorrencia", 259, 260, ArquivosTXT.TipoMapa.detalhe, 1);
            RegistraMapaDeA("MesmaTit", 261, 261, ArquivosTXT.TipoMapa.detalhe, 0);
            RegistraMapaDeA("CheckHorizontal", 263, 280, ArquivosTXT.TipoMapa.detalhe, "");
            RegistraMapaDeA("TipoCPFCNPJ", 281, 282, ArquivosTXT.TipoMapa.detalhe, 1);
            RegistraMapaDeA("CPFCNPJ", 283, 296, ArquivosTXT.TipoMapa.detalhe, null);
            //acertar ??? pode ser zero
            RegistraMapaDeA("CodigoHistorico", 307, 311, ArquivosTXT.TipoMapa.detalhe, 0);
            RegistraMapaDeA("DigitoCCFavorecido", 314, 314, ArquivosTXT.TipoMapa.detalhe, null);
            RegistraMapaDeA("Sequencial", 315, 320, ArquivosTXT.TipoMapa.detalhe, ArquivosTXT.TiposDeCampo.Sequencial, null);


            RegistraMapaDeA("CodigoDeRegistro", 1, 1, ArquivosTXT.TipoMapa.rodape, ArquivosTXT.TiposDeCampo.normal, 9);
            RegistraMapaDeA("QuantidadeDePagamentos", 2, 7, ArquivosTXT.TipoMapa.rodape, ArquivosTXT.TiposDeCampo.NDetalhe, null);
            RegistraMapaDeA("ValorDosPagamentos", 8, 20, ArquivosTXT.TipoMapa.rodape, ArquivosTXT.TiposDeCampo.Somatorio, null);
            //acertar
            RegistraMapaDeA("QuantidadeDeRegistros", 21, 26, ArquivosTXT.TipoMapa.rodape, ArquivosTXT.TiposDeCampo.Sequencial, 0M);
            RegistraMapaDeA("Sequencial", 315, 320, ArquivosTXT.TipoMapa.rodape, ArquivosTXT.TiposDeCampo.Sequencial, null);
            
        }

        public RemessaCC(dRemessaCC.TabelaDeCreditosDataTable Tabela,int Agencia, int conta, int contaDg, string Nome)
        {
            TabelaCreditos = Tabela;
            IniciaCampos(Agencia, conta, contaDg, Nome);            
        }



        private void CalculaCheckHorizontal() {
            foreach (dRemessaCC.TabelaDeCreditosRow row in TabelaCreditos) {
                row.CheckHorizontal = CheckHorizontal(row.NumeroDaAgenciaFavorecido, row.NumeroDaContaFavorecido, row.ValorCredito);
            }
        }

        /// <summary>
        /// Gera um arquivo com os dados armazenados
        /// </summary>
        /// <param name="arquivo">Nome do Arquivo</param>
        /// <returns></returns>
        public override bool Salva(string arquivo)
        {
            CalculaCheckHorizontal();
            return base.Salva(arquivo);
        }

        
    }
}
