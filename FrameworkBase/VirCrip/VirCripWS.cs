using System;
using System.Text;
using System.Security.Cryptography;
using System.IO;

namespace VirCrip
{
    /// <summary>
    /// 
    /// </summary>
    public static class VirCripWS
    {
        private static byte[] aChave
        {
            get
            {
                string Chave = String.Format("Tartaruga Verde{0:dd}", DateTime.Today);
                byte[] retorno = new byte[8];
                Array.Copy(new SHA1CryptoServiceProvider().ComputeHash(Encoding.Unicode.GetBytes(Chave)), retorno, 8);
                return retorno;
            }
        }

        private static byte[] Vector = new byte[] { 32, 43, 218, 16, 53, 69, 118, 24 };

        private static DESCryptoServiceProvider _des;

        private static DESCryptoServiceProvider des
        {
            get
            {
                if (_des == null)
                    _des = new DESCryptoServiceProvider();
                return _des;
            }
        }

        /// <summary>
        /// Criptografa
        /// </summary>
        /// <param name="entrada">entrada</param>
        /// <returns></returns>
        public static byte[] Crip(string entrada)
        {
            using (MemoryStream arqSaida = new MemoryStream())
            {
                using (CryptoStream crStream = new CryptoStream(arqSaida, des.CreateEncryptor(aChave, Vector), CryptoStreamMode.Write))
                {
                    byte[] Gravar = Encoding.Unicode.GetBytes(entrada);
                    crStream.Write(Gravar, 0, Gravar.Length);
                    crStream.Close();
                }
                return arqSaida.ToArray();
            }
        }

        /// <summary>
        /// Descriptografa
        /// </summary>
        /// <param name="entrada"></param>
        /// <returns></returns>
        public static string Uncrip(byte[] entrada)
        {
            using (MemoryStream arqSaida = new MemoryStream())
            {
                using (CryptoStream crStream = new CryptoStream(arqSaida, des.CreateDecryptor(aChave, Vector), CryptoStreamMode.Write))
                {
                    crStream.Write(entrada, 0, entrada.Length);
                    crStream.Close();
                }
                return Encoding.Unicode.GetString(arqSaida.ToArray());
            }
        }
    }
}
