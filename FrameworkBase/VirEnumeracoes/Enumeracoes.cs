﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace VirEnumeracoes
{
    /// <summary>
    /// Enumeração completa para sim ou nao
    /// </summary>
    public enum SimNao
    {
        /// <summary>
        /// sim
        /// </summary>
        Sim,
        /// <summary>
        /// nao
        /// </summary>
        Nao,
        /// <summary>
        /// usar o padrão
        /// </summary>
        Padrao,
        /// <summary>
        /// indefinido
        /// </summary>
        indefinido,
        /// <summary>
        /// inválido
        /// </summary>
        invalido
    }

    /// <summary>
    /// Códigos dos bancos
    /// </summary>
    public enum CodigoBancos : int
    {
        /// <summary>
        /// Santander
        /// </summary>
        Santander = 33,
        /// <summary>
        /// Caixa
        /// </summary>
        Caixa = 104,
        /// <summary>
        /// Bradesco
        /// </summary>
        Bradesco = 237,
        /// <summary>
        /// Itaú
        /// </summary>
        Itau = 341
    }

    /// <summary>
    /// Status do registro do boleto
    /// </summary>
    public enum StatusRegistroBoleto
    {
        /// <summary>
        /// Boleto sem registro ou registro com erro (rejeitado)
        /// </summary>
        BoletoSemRegistro = 0,
        /// <summary>
        /// Boteto registrando (será gerado aquivo para registro pelo servidor)
        /// </summary>
        BoletoRegistrando = 1,
        /// <summary>
        /// Boleto aguardando retorno
        /// </summary>
        BoletoAguardandoRetorno = 2,
        /// <summary>
        /// Boleto registrado
        /// </summary>
        BoletoRegistrado = 3,
        /// <summary>
        /// Boleto registrado dda
        /// </summary>
        BoletoRegistradoDDA = 4,
        /// <summary>
        /// Boleto com registro (carteira 9) mas sem registrar efetivamente
        /// </summary>
        ComregistroNaoRegistrado = 5,
        /// <summary>
        /// O registro deve ser corrigido a data
        /// </summary>
        AlterarRegistroData = 6,
        /// <summary>
        /// Erro no registro
        /// </summary>
        ErroNoRegistro = 7,
        /// <summary>
        /// O registro deve ser alterado corrigindo o valor com um desconto.
        /// </summary>
        AlterarRegistroValor = 8,
        /// <summary>
        /// Boteto registrando DA (será gerado aquivo para registro pelo servidor)
        /// </summary>
        BoletoRegistrandoDA = 9,
        /// <summary>
        /// Boleto aguardando retorno DA
        /// </summary>
        BoletoAguardandoRetornoDA = 10,
        /// <summary>
        /// Boleto registrado DA
        /// </summary>
        BoletoRegistradoDA = 11,
        /// <summary>
        /// O registro venceu
        /// </summary>
        BoletoRegistroVencido = 12,
        /// <summary>
        /// Baixado por quitação
        /// </summary>
        BoletoQuitado = 13,
        /// <summary>
        /// Baixado por quitação DDA
        /// </summary>
        BoletoQuitadoDDA = 14,
        /// <summary>
        /// Baixado por quitação DA
        /// </summary>
        BoletoQuitadoDA = 15,
    }

    /// <summary>
    /// Tipo da conta
    /// </summary>
    public enum TipoConta
    {
        /// <summary>
        /// Conta Corrente
        /// </summary>
        CC = 0,
        /// <summary>
        /// Poupança
        /// </summary>
        Poupanca = 1
    }

    /// <summary>
    /// Guias
    /// </summary>
    public enum guias
    {
        /// <summary>
        /// 
        /// </summary>
        DARF,
        /// <summary>
        /// 
        /// </summary>
        GPS,
        /// <summary>
        /// 
        /// </summary>
        Nulo
    }

    /// <summary>
    /// Tipos de imposto
    /// </summary>
    public enum TipoImposto
    {
        /// <summary>
        /// Imposto de renda Retenção PJ
        /// </summary>
        IR = 1,
        /// <summary>
        /// Imposto de renda Retenção PF
        /// </summary>
        IRPF = 12,
        /// <summary>
        /// PIS
        /// </summary>
        PIS = 2,
        /// <summary>
        /// Cofins
        /// </summary>
        COFINS = 3,
        /// <summary>
        /// Csll
        /// </summary>
        CSLL = 4,
        /// <summary>
        /// Imposto combinado
        /// </summary>
        PIS_COFINS_CSLL = 5,
        /// <summary>
        /// Imposto Municipal
        /// </summary>
        ISSQN = 6,
        /// <summary>
        /// Inss
        /// </summary>
        INSS = 7,
        /// <summary>
        /// INSS pessoa fisica parte retida
        /// </summary>
        INSSpfRet = 8,
        /// <summary>
        /// INSS pessoa fisica parte empregador
        /// </summary>
        INSSpfEmp = 9,
        /// <summary>
        /// INSS pessoa fisica empregador e empregado
        /// </summary>
        INSSpf = 10,
        /// <summary>
        /// ISS Santo Andre
        /// </summary>
        ISS_SA = 11,
        /// <summary>
        /// FGTS
        /// </summary>
        FGTS = 13,
        /// <summary>
        /// Embora as contas de consumo não sejam um imposto o pagamento é feito da mesma forma
        /// </summary>
        CONTA_CONSUMO = 14
    }

    
    /// <summary>
    /// Origem do imposto
    /// </summary>
    public enum OrigemImposto
    {
        /// <summary>
        /// Folha
        /// </summary>
        Folha = 1,
        /// <summary>
        /// Nota
        /// </summary>
        Nota = 2
    }

    /// <summary>
    /// Tipo da chave
    /// </summary>
    public enum TipoChaveContaCorrenteNeon
    {
        /// <summary>
        /// Condominio
        /// </summary>
        CON,
        /// <summary>
        /// Conta Corrente
        /// </summary>
        CCT,
        /// <summary>
        /// Conta Corrente Geral
        /// </summary>
        CCG,
        /// <summary>
        /// Fornecedor
        /// </summary>
        FRN
    }

}
