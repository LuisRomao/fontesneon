﻿using System.Collections.Generic;
using EDIBoletos;
using dllCNAB;
using DocBacarios;
using System;


namespace SGCParse
{
    public class Remessa
    {
        public Exception UltimoErro;
        public int Banco;
        public Tipo_Arquivos Tipo_Arquivo;
        private SortedList<int, BoletoReg> _Boletos;
        public SortedList<int, BoletoReg> Boletos => _Boletos ?? (_Boletos = new SortedList<int, BoletoReg>()); 
        public bool GeraException = true;

        public int Agencia;
        public int Conta;
        public string DigitoConta;
        public int? CodigoConvenio;
        public string NomeEmissor;
        public string CNPJEmissor;
        public int CodigoEmissor;
        public string Carteira;
        public int SequencialRemessa;
                                     

        public bool GerarRemessaBoletos(int Banco,
                                        int Agencia,
                                        int Conta,
                                        string DigitoConta,
                                        int? CodigoConvenio,
                                        string NomeEmissor,
                                        string CNPJEmissor,
                                        int CodigoEmissor,
                                        string Carteira,
                                        int SequencialRemessa,
                                        string NomeArquivo)
        {
            this.Banco = Banco;
            this.Agencia = Agencia;
            this.Conta = Conta;
            this.DigitoConta = DigitoConta;
            this.CodigoConvenio = CodigoConvenio;
            this.NomeEmissor = NomeEmissor;
            this.CodigoEmissor = CodigoEmissor;
            this.Carteira = Carteira;
            this.SequencialRemessa = SequencialRemessa;
            UltimoErro = null;
            CPFCNPJ _CNPJEmissor = new CPFCNPJ(CNPJEmissor);
            if (_CNPJEmissor.Tipo == TipoCpfCnpj.INVALIDO)
                throw new System.ArgumentException("CNPJ inválido", "CNPJEmissor");
            if ((Banco != 237) && (!CodigoConvenio.HasValue))
                throw new System.ArgumentNullException("CodigoConvenio", "CodigoConvenio obrigatório para este banco");
            Tipo_Arquivo = Tipo_Arquivos.CobrancaREM;
            BoletoRemessaBase BoletoRemessa = null;
            switch (Tipo_Arquivo)
            {
                case Tipo_Arquivos.CobrancaREM:
                    try
                    {
                        switch (Banco)
                        {
                            case 237:
                                BoletoRemessa = new BoletoRemessaBra();
                                break;
                            case 341:
                                BoletoRemessa = new BoletoRemessaItau();
                                break;
                            case 33:
                                throw new System.NotImplementedException();
                                //BoletoRemessa = new BoletoRemessaCNAB_SAN();                               
                            case 104:
                                BoletoRemessa = new BoletoRemessaCNAB_CX();
                                break;
                            default:
                                throw new System.NotImplementedException(string.Format("Remessa: Banco {0} não configurado para registro de boleto\r\n", Banco));
                        }

                        BoletoRemessa.IniciaRemessa();                        

                        switch (Banco)
                        {
                            case 237:                                
                                //BoletoRemessa.SequencialRemessaArquivo = SequencialRemessa;
                                BoletoRemessa.SequencialRemessa = SequencialRemessa;                                
                                BoletoRemessa.CodEmpresa = CodigoEmissor;
                                BoletoRemessa.NomeEmpresa = NomeEmissor;
                                BoletoRemessa.Carteira = Carteira;                                                              
                                break;                                
                            case 341:
                            case 104:
                                BoletoRemessa.Configura(Agencia,
                                                        Conta,
                                                        DigitoConta,
                                                        NomeEmissor,
                                                        _CNPJEmissor,
                                                        Carteira,
                                                        CodigoConvenio.Value,
                                                        SequencialRemessa);
                                                                                                                                                                                                        
                                break;
                            case 33:                                
                                //BoletoRemessa = new BoletoRemessaCNAB_SAN();                                
                            default:
                                throw new System.NotImplementedException(string.Format("Remessa: Banco {0} não configurado para registro de boleto\r\n", Banco));
                        }
                        //Insere boletos na remessa
                        foreach (BoletoReg Bol in Boletos.Values)
                        {
                            if (Bol.Agencia != "")
                            {
                                Bol.Agencia = Agencia.ToString();
                                Bol.Conta = Conta.ToString();
                                Bol.DigConta = DigitoConta;
                                Bol.CNPJ = _CNPJEmissor;
                                Bol.NomeEmpresa = NomeEmissor;
                                //Bol.CobrarMulta = true,
                                //PercMulta = rowPrincipal.BOLMulta,
                                //NossoNumero = rowPrincipal.BOL,
                            }
                            Bol.Registrar(BoletoRemessa);
                        }
                        BoletoRemessa.GravaArquivo("",NomeArquivo);
                    }
                    catch (Exception ex)
                    {
                        UltimoErro = ex;
                        if (GeraException)
                            throw ex;
                        return false;
                    }
                    break;
            }
            return false;
        }
    }
}
