﻿using System;
using System.Text;
using System.Security.Cryptography;
using System.IO;

namespace VirCrip2D
{
    /// <summary>
    /// 
    /// </summary>
    public static class VirCripWS
    {
        private static byte[] aChave
        {
            get
            {
                string Chave = String.Format("Tartaruga Verde{0:dd}", DateTime.Today);
                byte[] retorno = new byte[8];
                Array.Copy(new SHA1CryptoServiceProvider().ComputeHash(Encoding.Unicode.GetBytes(Chave)), retorno, 8);
                return retorno;
            }
        }

        private static byte[] Vector = new byte[] { 32, 43, 218, 16, 53, 69, 118, 24 };

        private static DESCryptoServiceProvider _des;

        private static DESCryptoServiceProvider des
        {
            get
            {
                if (_des == null)
                    _des = new DESCryptoServiceProvider();
                return _des;
            }
        }        

        /// <summary>
        /// Descriptografa
        /// </summary>
        /// <param name="entrada"></param>
        /// <returns></returns>
        private static string uncrip(string entrada)
        {
            int nbytes = entrada.Length / 3;            
            byte[] entradab = new byte[nbytes];
            for(int i = 0;i<nbytes;i++)
            {
                int ib = int.Parse(entrada.Substring(i*3,3));
                entradab[i] = (byte)ib;
            }            
            using (MemoryStream arqSaida = new MemoryStream())
            {
                using (CryptoStream crStream = new CryptoStream(arqSaida, des.CreateDecryptor(aChave, Vector), CryptoStreamMode.Write))
                {
                    crStream.Write(entradab, 0, entradab.Length);
                    crStream.Close();
                }
                return Encoding.Unicode.GetString(arqSaida.ToArray());
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="entrada"></param>
        /// <returns></returns>
        public static string[] Uncrip(string entrada)
        {
            string Aberto = uncrip(entrada);            
            string[] Retorno = new string[3];
            string[] picotado = Aberto.Split(new string[] { " " }, StringSplitOptions.RemoveEmptyEntries);
            if ((picotado.Length < 8) || (picotado[0] != "<valida>"))
                return null;
            DateTime HoraChamada = new DateTime(int.Parse(picotado[3]), int.Parse(picotado[2]), int.Parse(picotado[1])).AddHours(int.Parse(picotado[4]));
            TimeSpan delta = HoraChamada - DateTime.Now;
            if (Math.Abs(delta.TotalHours) > 24)
                return null;
            Retorno[0] = picotado[5];
            Retorno[1] = picotado[6];
            Retorno[2] = picotado[7];
            return Retorno;            
        }
    }
}