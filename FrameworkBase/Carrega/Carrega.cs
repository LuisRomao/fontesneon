using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using System.Diagnostics;

namespace Carrega
{
    public partial class Carrega : Form
    {
        public Carrega()
        {
            InitializeComponent();
        }
        private void Form1_Load(object sender, EventArgs e)
        {
            
        }

        private bool Copiar(String Path,String ext)
        {
            bool resultado = false;
            bool copiar;
            string nomearquivoLocal;
            if (System.IO.Directory.Exists(CaminhoServidor + "\\" + Path))
            {
                foreach (string arquivo in System.IO.Directory.GetFiles(CaminhoServidor + "\\" + Path, "*." + ext))
                {
                    copiar = false;
                    nomearquivoLocal = Application.StartupPath + "\\" + Path + System.IO.Path.GetFileName(arquivo);
                    if (!System.IO.File.Exists(nomearquivoLocal))
                        copiar = true;
                    else
                    {
                        FileVersionInfo VerServidor = FileVersionInfo.GetVersionInfo(arquivo);
                        FileVersionInfo VerLocal = FileVersionInfo.GetVersionInfo(nomearquivoLocal);
                        if ((VerServidor.FileVersion == null) && (VerLocal.FileVersion == null))
                        {
                            if (System.IO.File.GetLastWriteTime(arquivo) != System.IO.File.GetLastWriteTime(nomearquivoLocal))
                                copiar = true;
                        }
                        else
                        {
                            if (VerServidor.FileVersion != VerLocal.FileVersion)
                                copiar = true;
                        }
                    }
                    if (copiar)
                        try
                        {
                            System.IO.File.Copy(arquivo, nomearquivoLocal, true);
                            resultado = true;
                        }
                        catch
                        {

                        }

                }

            }
            return resultado;
        }




        private string CaminhoServidor = "";
        private string Aplicativo;

        protected virtual string AplicativoDef{
            get {
                return "ConsoleIndices.exe";
            }
        }

        protected virtual string CaminhoServidorDef
        {
            get
            {
                return @"\\SO_mi12\exe\indices";
            }
        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            Aplicativo = AplicativoDef;
            CaminhoServidor = CaminhoServidorDef;
            timer1.Enabled = false;
            DateTime dinicio = DateTime.Now;
            dConfig.DadosConfigRow linhaMae = null;
            
            try
            {
                if(!System.IO.Directory.Exists(Application.StartupPath + "\\xml"))
                    System.IO.Directory.CreateDirectory(Application.StartupPath + "\\XML");
                dConfig DConfig = new dConfig();
                if (System.IO.File.Exists(Application.StartupPath + "\\xml\\Config.xml"))
                {
                    
                    DConfig.DadosConfig.ReadXml(Application.StartupPath + "\\xml\\Config.xml");
                    linhaMae = DConfig.DadosConfig[0];
                    if (linhaMae != null)
                    {
                        CaminhoServidor = linhaMae.Repositorio;
                        Aplicativo = linhaMae.Aplicativo;
                    }
                }
                if (CaminhoServidor == "")
                {
                    System.Windows.Forms.FolderBrowserDialog Busca = new FolderBrowserDialog();
                    Busca.Description = "Repositório";
                    if (Busca.ShowDialog() == DialogResult.OK)
                    {
                        CaminhoServidor = Busca.SelectedPath;
                        if (linhaMae == null)
                        {
                            linhaMae = DConfig.DadosConfig.NewDadosConfigRow();
                            linhaMae.Aplicativo = Aplicativo;
                        }
                        linhaMae.Repositorio = CaminhoServidor;
                    }
                    else
                        return;

                }
                else
                {
                    if (linhaMae == null)
                    {
                        linhaMae = DConfig.DadosConfig.NewDadosConfigRow();                      
                        linhaMae.Aplicativo = Aplicativo;
                        linhaMae.Repositorio = CaminhoServidor;
                    }
                }
                if (linhaMae.RowState == DataRowState.Detached)
                    DConfig.DadosConfig.AddDadosConfigRow(linhaMae);
                DConfig.DadosConfig.WriteXml(Application.StartupPath + "\\xml\\Config.xml");
                
                //if (!System.IO.File.Exists(Application.StartupPath + @"\Neon.NEO"))
                //    CaminhoServidor = @"\\Neon02\Sistema NEON\";
                //else
                //    CaminhoServidor = @"\\10.135.1.151\Sistema NEON\";
                //if (System.IO.File.Exists(Application.StartupPath + @"\NeonL.NEO"))
                //    CaminhoServidor = @"c:\publicar\";


                
                //if (!System.IO.Directory.Exists("c:\\Sistema NEON"))
                //    System.IO.Directory.CreateDirectory("c:\\sistema NEON");
                if (!System.IO.Directory.Exists(Application.StartupPath+"\\PT-BR"))
                    System.IO.Directory.CreateDirectory(Application.StartupPath+"\\PT-BR");
                if (!System.IO.Directory.Exists(Application.StartupPath + "\\TMP"))
                    System.IO.Directory.CreateDirectory(Application.StartupPath + "\\TMP");
                //string ArqVersao = string.Format("{0}\\versao.txt", CaminhoServidor);
                if (Copiar("", "txt"))
                {
                    Copiar("", "dll");
                    Copiar("", "exe");
                    Copiar("", "pdb");
                    Copiar("pt-br\\", "dll");
                    Copiar("pt-br\\", "exe");
                    Copiar("", "dic");
                    Copiar("", "aff");
                }
                if (System.IO.File.Exists(Application.StartupPath+"\\"+Aplicativo))
                    System.Diagnostics.Process.Start(Application.StartupPath+"\\"+Aplicativo);
            }
            finally
            {
                while (((TimeSpan)(DateTime.Now - dinicio)).Seconds < 2)
                {
                    Application.DoEvents();
                };
                Application.Exit();
            }
            
        }

        

    
    }
}