﻿using System;
using System.Collections.Generic;
using System.Text;

namespace virCompet
{
    /// <summary>
    /// 
    /// </summary>
    public class virBuscaLimitesCompDCorte : virBuscaLimitesComp
    {
        /// <summary>
        /// 
        /// </summary>
        public enum Pega_dias_mes
        {
            /// <summary>
            /// 
            /// </summary>
            passado,
            /// <summary>
            /// 
            /// </summary>
            futuro
        }

        /// <summary>
        /// 
        /// </summary>
        public static int DiaCortePadrao = 1;
        /// <summary>
        /// 
        /// </summary>
        public static Pega_dias_mes sentidoPadrao = Pega_dias_mes.passado;
        /// <summary>
        /// 
        /// </summary>
        public int DiaCorte = DiaCortePadrao;
        /// <summary>
        /// 
        /// </summary>
        public Pega_dias_mes sentido = sentidoPadrao;
        /// <summary>
        /// 
        /// </summary>
        /// <param name="_DiaCorte"></param>
        /// <param name="_sentido"></param>
        public virBuscaLimitesCompDCorte(int _DiaCorte, Pega_dias_mes _sentido)
        {
            DiaCorte = _DiaCorte;
            sentido = _sentido;
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="Mes"></param>
        /// <param name="Ano"></param>
        /// <param name="regime"></param>
        /// <returns></returns>
        public override DateTime DataI(int Mes, int Ano, Regime regime)
        {
            if (regime == Regime.mes)
                return base.DataI(Mes, Ano, regime);
            else
            {
                if (sentido == Pega_dias_mes.futuro)
                    return new DateTime(Ano, Mes, Dia_ver_30(DiaCorte, Mes, Ano));
                else
                {
                    virCompetencia CompCalendario = new virCompetencia(Mes, Ano, Regime.mes).Add(-1);
                    return new DateTime(Ano, Mes, Dia_ver_30(DiaCorte, Mes, Ano)).AddDays(1);
                }
            }
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="Mes"></param>
        /// <param name="Ano"></param>
        /// <param name="regime"></param>
        /// <returns></returns>
        public override DateTime DataF(int Mes, int Ano, Regime regime)
        {
            if (regime == Regime.mes)
                return base.DataF(Mes, Ano, regime);
            else
            {
                if (sentido == Pega_dias_mes.passado)
                    return new DateTime(Ano, Mes, Dia_ver_30(DiaCorte, Mes, Ano));
                else
                {
                    virCompetencia CompCalendario = new virCompetencia(Mes, Ano, Regime.mes).Add(1);
                    return new DateTime(Ano, Mes, Dia_ver_30(DiaCorte, Mes, Ano)).AddDays(-1);
                }
            }
        }
    }
}
