﻿using System;
using System.Collections.Generic;
using System.Text;

namespace virCompet
{
    /// <summary>
    /// 
    /// </summary>
    public class virCompetencia : ICloneable
    {
        /// <summary>
        /// Controla as datas da competência
        /// </summary>                
        public static virBuscaLimitesComp BuscaLimitesCompPadrao;

        private virBuscaLimitesComp buscaLimitesComp;

        /// <summary>
        /// 
        /// </summary>
        public virBuscaLimitesComp BuscaLimitesComp
        {
            get { return (buscaLimitesComp ?? BuscaLimitesCompPadrao ?? (BuscaLimitesCompPadrao = new virBuscaLimitesComp())); }
            set { buscaLimitesComp = value; }
        }

        /// <summary>
        /// Primero dia da competencia
        /// </summary>
        public virtual DateTime DataI
        {
            get { return BuscaLimitesComp.DataI(Mes,Ano,regime);}            
        }

        /// <summary>
        /// Ultimo dia da competencia
        /// </summary>
        public virtual DateTime DataF
        {
            get { return BuscaLimitesComp.DataF(Mes,Ano,regime);}            
        }

        #region Campos internos

        /// <summary>
        /// 
        /// </summary>
        public static Regime regimePadrao = Regime.mes;

        /// <summary>
        /// Mes da competencia
        /// </summary>
        private int mes;

        /// <summary>
        /// Mes da competencia
        /// </summary>
        public int Mes
        {
            get
            {
                return mes;
            }
            set
            {
                mes = value;
                AjustaCompMes();
            }
        }

        /// <summary>
        /// Ano da competencia
        /// </summary>
        private int ano;

        /// <summary>
        /// Ano da competencia
        /// </summary>
        public int Ano
        {
            get { return ano; }
            set { ano = value; }
        }

        /// <summary>
        /// Competencia na forma compacta yyyyMM para banco de dados
        /// </summary>
        public int CompetenciaBind
        {
            get
            {
                return Ano * 100 + mes;
            }
            set
            {
                Ano = value / 100;
                Mes = value % 100;
            }
        }
        /// <summary>
        /// 
        /// </summary>
        public Regime regime;

        #endregion

        #region Opeardores

        /// <summary>
        /// Retorna se igual
        /// </summary>
        public override bool Equals(object obj)
        {
            if (obj == null)
                return false;
            if (!(obj is virCompetencia))
                return false;
            return (this == (virCompetencia)obj);
        }

        /// <summary>
        /// Retorna GetHashCode
        /// </summary>
        public override int GetHashCode()
        {
            return base.GetHashCode();
        }

        /// <summary>
        /// operador ==
        /// </summary>
        public static bool operator ==(virCompetencia a, virCompetencia b)
        {
            if (object.Equals(a, null) && (object.Equals(b,null)))
                return true;
            if (object.Equals(a, null) || (object.Equals(b, null)))
                return false;
            try
            {
                return ((a.Mes == b.Mes) && (a.Ano == b.Ano));
            }
            catch
            {
                return false;
            }
        }

        /// <summary>
        /// operador !=
        /// </summary>
        public static bool operator !=(virCompetencia a, virCompetencia b)
        {
            if (object.Equals(a, null) && (object.Equals(b, null)))
                return false;
            if (object.Equals(a, null) || (object.Equals(b, null)))
                return true;
            try
            {
                return ((a.Mes != b.Mes) || (a.Ano != b.Ano));
            }
            catch
            {
                return true;
            }
        }

        
        /// <summary>
        /// Operador ++
        /// </summary>
        public static virCompetencia operator ++(virCompetencia Comp){
            virCompetencia Manobra = Comp.CloneCompet(1);            
            return Manobra;            
        }        

        /// <summary>
        /// Operador --
        /// </summary>
        public static virCompetencia operator --(virCompetencia Comp)
        {
            virCompetencia Manobra = Comp.CloneCompet(-1);
            return Manobra;
        }

        /// <summary>
        /// operador Menor
        /// </summary>
        public static bool operator <(virCompetencia a, virCompetencia b)
        {
            if ((object.Equals(a, null)) && (object.Equals(b, null)))
                return false;
            if (object.Equals(a, null))
                return true;
            if (object.Equals(b, null))
                return false;
            if (a.ano < b.ano)
                return true;
            else if (a.ano == b.ano)
                return a.mes < b.mes;
            return false;

        }

        /// <summary>
        /// operador >
        /// </summary>
        public static bool operator >(virCompetencia a, virCompetencia b)
        {
            if ((object.Equals(a, null)) && (object.Equals(b, null)))
                return false;
            if (object.Equals(a, null))
                return false;
            if (object.Equals(b, null))
                return true;
            if (a.ano > b.ano)
                return true;
            else if (a.ano == b.ano)
                return a.mes > b.mes;
            return false;

        }

        /// <summary>
        /// operador menor ou igual
        /// <param name="a"></param>
        /// <param name="b"></param>
        /// </summary>
        public static bool operator <=(virCompetencia a, virCompetencia b)
        {
            if ((object.Equals(a, null)) && (object.Equals(b, null)))
                return true;
            if (object.Equals(a, null))
                return true;
            if (object.Equals(b, null))
                return false;
            if (a.ano < b.ano)
                return true;
            else if (a.ano == b.ano)
                return a.mes <= b.mes;
            return false;

        }

        /// <summary>
        /// operador >=
        /// </summary>
        public static bool operator >=(virCompetencia a, virCompetencia b)
        {
            if ((object.Equals(a, null)) && (object.Equals(b, null)))
                return true;
            if (object.Equals(a, null))
                return false;
            if (object.Equals(b, null))
                return true;
            if (a.ano > b.ano)
                return true;
            else if (a.ano == b.ano)
                return a.mes >= b.mes;
            return false;

        }        

        /// <summary>
        /// operador +
        /// </summary>
        /// <param name="a"></param>
        /// <param name="b"></param>
        /// <returns></returns>
        public static virCompetencia operator +(virCompetencia a, int b)
        {
            return a.CloneCompet(b);
        }        

        /// <summary>
        /// operador -
        /// </summary>
        /// <param name="a"></param>
        /// <param name="b"></param>
        /// <returns></returns>
        public static virCompetencia operator -(virCompetencia a, int b)
        {
            return a.CloneCompet(b);
        }

        /// <summary>
        /// operador -
        /// </summary>
        /// <param name="a"></param>
        /// <param name="b"></param>
        /// <returns></returns>
        public static int operator -(virCompetencia a, virCompetencia b)
        {
            return b.TotalMeses() - a.TotalMeses();
        }

        /// <summary>
        /// Soma meses na cometencia equivale a +=
        /// </summary>
        /// <param name="meses">meses a serem somados (pode ser negativo)</param>
        /// <returns>competencia calculada (this)</returns>
        public virCompetencia Add(int meses)
        {
            mes += meses;
            AjustaCompMes();
            return this;
        }

        /// <summary>
        /// Copia os campos de outra instancia, equivava a usar o clone so que nao cria outra instancia
        /// </summary>
        /// <param name="Origem">Instancia para cópia</param>
        public void CopiaValores(virCompetencia Origem)
        {
            this.Ano = Origem.Ano;
            this.Mes = Origem.Mes;
            this.regime = Origem.regime;
        }

        private int TotalMeses()
        {
            return Ano * 12 + Mes;
        }

        #endregion
        
        /// <summary>
        /// virCompetencia no formato MM/yyyy
        /// </summary>
        /// <remarks>dar a preferencia ao formato bind yyyyMM</remarks>
        /// <returns>MM/yyyy</returns>
        public override string ToString()
        {
            return mes.ToString("00") + "/" + ano.ToString();
        }

        /// <summary>
        /// virCompetencia no formato MMyyyy (somente numeros)
        /// </summary>
        /// <remarks>formato usado no banco access</remarks>
        /// <returns>MMyyyyy</returns>
        public string ToStringN()
        {
            return mes.ToString("00") + ano.ToString("0000");
        }
                  
        /// <summary>
        /// Ajusta a copetencia caso o mes tenha saido do intervalo 1-12
        /// </summary>
        /// <returns>Campetencia ajustada</returns>
        private void AjustaCompMes(){
            while ((mes < 1) || (mes > 12))
            {
                if (mes < 1)
                {
                    ano--;
                    mes += 12;
                };
                if (mes > 12)
                {
                    ano++;
                    mes -= 12;
                }
            }
        }

        #region Cosntrutores

        /// <summary>
        /// Os campos Mes e Ano foram ajustados pelo calendario e agora vai ser feita a correção pelo regime.
        /// </summary>
        /// <remarks>Esta funcao so deve ser chamda pelo contrutor</remarks>
        private void AjustaDataCalendarioParaCompetencia(DateTime Data)
        {        
            if (regime == Regime.mes)
                return;
            else
            {
                while (Data < DataI)
                    Add(-1);
                while (Data > DataF)
                    Add(1);
            }
        }

        /// <summary>
        /// Constructor - competencia na data atual
        /// </summary>
        public virCompetencia()
        {
            regime = regimePadrao;
            mes = DateTime.Today.Month;
            ano = DateTime.Today.Year;
            AjustaDataCalendarioParaCompetencia(DateTime.Today);
        }

        /// <summary>
        /// Construtor
        /// </summary>
        /// <param name="_regime"></param>
        public virCompetencia(Regime _regime)
        {
            mes = DateTime.Today.Month;
            ano = DateTime.Today.Year;
            regime = _regime;
            AjustaDataCalendarioParaCompetencia(DateTime.Today);
        }

        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="data"></param>
        public virCompetencia(DateTime data)
        {
            mes = data.Month;
            ano = data.Year;
            regime = regimePadrao;
            AjustaDataCalendarioParaCompetencia(data);
        }

        /// <summary>
        /// Constutor
        /// </summary>
        /// <param name="data"></param>
        /// <param name="_regime"></param>
        public virCompetencia(DateTime data,Regime _regime)
        {
            mes = data.Month;
            ano = data.Year;
            regime = _regime;
            AjustaDataCalendarioParaCompetencia(data);
        }

        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="_mes">mes</param>
        /// <param name="_ano">ano</param>
        public virCompetencia(int _mes, int _ano)
        {
            mes = _mes;
            ano = _ano;
            regime = regimePadrao;
        }

        /// <summary>
        /// Construtor
        /// </summary>
        /// <param name="_mes"></param>
        /// <param name="_ano"></param>
        /// <param name="_regime"></param>
        public virCompetencia(int _mes, int _ano, Regime _regime)
        {
            mes = _mes;
            ano = _ano;
            regime = _regime;
        }

        /// <summary>
        /// Construtor
        /// </summary>
        /// <param name="CompetenciaAAAAMM">Competicia na forma numerica do banco</param>        
        public virCompetencia(int CompetenciaAAAAMM)
        {
            CompetenciaBind = CompetenciaAAAAMM;
            regime = regimePadrao;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="CompetenciaAAAAMM"></param>
        /// <param name="_regime"></param>
        public virCompetencia(int CompetenciaAAAAMM,Regime _regime)
        {
            CompetenciaBind = CompetenciaAAAAMM;
            regime = _regime;
        }

        #endregion

        
       
        /*
        private int DiaCorteCopetencia 
        {
            get 
            {
                if (CON <= 0)
                    return 1;
                object retorno = Framework.Buscas.dBuscas.CONDOMINIOSTableAdapter.DiaCotabilidade(CON);
                if ((retorno == null) || (retorno == DBNull.Value))
                    return 1;
                else
                    return (int)((Int16)retorno);
            }
        }
        */


        /// <summary>
        /// Verifica se o mes tem o dia, caso nao tenha retrocede
        /// </summary>
        /// <param name="dia"></param>
        /// <returns></returns>
        private int Dia_ver_30(int dia)
        {
            return (dia <= DateTime.DaysInMonth(Ano, Mes)) ? dia : DateTime.DaysInMonth(Ano, Mes);
        }

        /// <summary>
        /// Data dentro da competecia
        /// </summary>
        /// <param name="dia">dia</param>
        /// <param name="_regime">Regime calendario ou competencia</param>
        /// <returns>data</returns>        
        /// <remarks>se o dia for maior do que o numero de dias do mês, retorna o ultimo dia do mês</remarks>
        public DateTime DataNaCompetencia(int dia,Regime _regime) 
        {
            DateTime retorno = new DateTime(Ano, Mes, Dia_ver_30(dia));
            if (_regime == Regime.balancete)
            {
                virCompetencia CompCalendario = new virCompetencia(Mes, Ano, Regime.mes);
                while (retorno < CompCalendario.DataI)
                    CompCalendario++;
                while (retorno > CompCalendario.DataF)
                    CompCalendario--;
                retorno = CompCalendario.DataNaCompetencia(dia);
            }
            return retorno;            
        }
      
        /// <summary>
        /// Data dentro da competecia
        /// </summary>
        /// <param name="dia">dia</param>
        /// <returns>data</returns>
        /// <remarks>se o dia for maior do que o numero de dias do mês, retorna o ultimo dia do mês</remarks>
        public DateTime DataNaCompetencia(int dia) 
        {
            return DataNaCompetencia(dia, regime);
        }

          

        /*
        private static Int32 nmes;
        private static Int32 nano;

        /// <summary>
        /// Funcao estatica que ajusta a variavel estatica mes
        /// </summary>
        /// <returns></returns>
        private static void AjustaMes()
        {
            while ((nmes < 1) || (nmes > 12))
            {
                if (nmes < 1)
                {
                    nano--;
                    nmes += 12;
                };
                if (nmes > 12)
                {
                    nano++;
                    nmes -= 12;
                }
            }
        }
        */

        //private static System.Collections.ArrayList _Feriados;

       
        
        /*
        /// <summary>
        /// Identifica se a data é feriado
        /// </summary>
        /// <param name="Datateste">Data</param>
        /// <returns></returns>
        [Obsolete("usar Framework.datasets.dFERiados.dFERiadosSt.IsFeriado")]
        public static bool IsFeriado(DateTime Datateste)
        {
            return datasets.dFERiados.IsFeriado(Datateste);
        }*/

        /*
        private static bool IsUtil(DateTime data)
        {
            if ((data.DayOfWeek == DayOfWeek.Saturday) || (data.DayOfWeek == DayOfWeek.Sunday) || IsFeriado(data))
                return false;            
            return true;
        }
        */
        
        /// <summary>
        /// Cria um objeto competencia a partir de um string
        /// </summary>
        /// <param name="entrada">string</param>
        /// <returns>competencia</returns>
        public static virCompetencia Parse(string entrada) 
        {
            try
            {
                entrada = entrada.Replace("/", "");                
                if (entrada.Length == 6)
                {
                    int mes = int.Parse(entrada.Substring(0, 2));
                    int ano = int.Parse(entrada.Substring(2, 4));
                    return new virCompetencia(mes, ano);
                }
                else
                    return null;
            }
            catch 
            {
                return null;
            }

        }

        /// <summary>
        /// Cria um objeto competencia a partir de um string
        /// </summary>
        /// <param name="entrada">string</param>
        /// <param name="result">retorno - pode ser null</param>
        /// <returns>indica se a conversão foi feita</returns>
        public static bool TryParse(string entrada, out virCompetencia result)
        {
            result = virCompetencia.Parse(entrada);
            return (result != null);
        }

        

        #region ICloneable Members

        /// <summary>
        /// Clonagem
        /// </summary>
        /// <remarks>Implementação da interface</remarks>
        /// <returns>Objeto clone</returns>
        public virtual object Clone()
        {
            return CloneCompet();
        }

        /// <summary>
        /// Clone da competência (Tipado)
        /// </summary>
        /// <remarks>O mesmo que Close só que já tipado</remarks>
        /// <returns>Clone da competência</returns>
        public virtual virCompetencia CloneCompet()
        {
            return new virCompetencia(this.Mes, this.Ano, regime);
        }

        /// <summary>
        /// Clona e soma meses na cometencia clonada
        /// </summary>
        /// <param name="meses">meses a serem somados (pode ser negativo)</param>
        /// <returns>competencia calculada</returns>
        public virCompetencia CloneCompet(int meses)
        {
            virCompetencia CompRetorno = CloneCompet();
            CompRetorno.Add(meses);
            return CompRetorno;
        }

        #endregion
    }
}
