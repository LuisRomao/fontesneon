﻿using System;
using System.Collections.Generic;
using System.Text;

namespace virCompet
{
    /// <summary>
    /// 
    /// </summary>
    public class virBuscaLimitesComp
    {
        /// <summary>
        /// 
        /// </summary>
        /// <param name="dia"></param>
        /// <param name="Mes"></param>
        /// <param name="Ano"></param>
        /// <returns></returns>
        protected int Dia_ver_30(int dia, int Mes, int Ano)
        {
            return (dia <= DateTime.DaysInMonth(Ano, Mes)) ? dia : DateTime.DaysInMonth(Ano, Mes);
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="Mes"></param>
        /// <param name="Ano"></param>
        /// <param name="regime"></param>
        /// <returns></returns>
        public virtual DateTime DataI(int Mes, int Ano, Regime regime)
        {
            return new DateTime(Ano, Mes, 1);
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="Mes"></param>
        /// <param name="Ano"></param>
        /// <param name="regime"></param>
        /// <returns></returns>
        public virtual DateTime DataF(int Mes, int Ano, Regime regime)
        {
            return new DateTime(Ano, Mes, 1).AddMonths(1).AddDays(-1);
        }
    }
}
