﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.InteropServices;

namespace RoboBradesco
{
    class Cripto
    {
        [DllImport("WEBTAEncoderLib.dll")]
        private static extern IntPtr fInitEncoder(ref string fileName, ref string directory, ref string key, ref string msgErro);        
        [DllImport("WEBTAEncoderLib.dll")]
        private static extern Int32 fWriteData(IntPtr handle, byte[] data, int dataLen, ref string msgErro);
        [DllImport("WEBTAEncoderLib.dll")]
        private static extern UInt32 fCloseEncoder(IntPtr handle, ref string msgErro);        
        [DllImport("WEBTAEncoderLib.dll")]
        private static extern IntPtr fInitDecoder(ref string fileName, ref string directory, ref string key, ref string msgErro);         
        [DllImport("WEBTAEncoderLib.dll")]
        private static extern Int32 fReadData(IntPtr handle, byte[] data, int dataLen, ref string msgErro);
        [DllImport("WEBTAEncoderLib.dll")]
        private static extern Int32 fCloseDecoder(IntPtr handle, ref string msgErro);  
        [DllImport("WEBTAEncoderLib.dll")]
        private static extern Int32 fDecodeKeyFile(ref string filepath, ref string senha, ref string output, ref string msgErro);
        [DllImport("WEBTAEncoderLib.dll")]
        private static extern Int32 fDecodeKeyFileEx(ref string filepath, ref string senha,ref string id, ref string key, ref string msgErro);
        [DllImport("WEBTAEncoderLib.dll")]
        private static extern Int32 fEncodeBuffer(ref string bufferIn,Int32 sizeIn,ref string bufferOut,Int32 sizeOut,ref string key);
        [DllImport("WEBTAEncoderLib.dll")]
        private static extern Int32 fDecodeBuffer(ref string bufferIn,Int32 sizeIn,ref string bufferOut,Int32 sizeOut,ref string key);
        
        void Teste()
        {
         
        }
    


       
    }
}
