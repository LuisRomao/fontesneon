using System;
using System.Collections.Generic;
using System.Text;

namespace DocBacarios
{
    /// <summary>
    /// PIS
    /// </summary>
    public class PIS
    {
     #region Campos
        /// <summary>
        /// Valor como n�mero
        /// </summary>
        private Int64 valor;        
     #endregion
    
     #region Construtores
        /// <summary>
        /// Constuctor
        /// </summary>
        /// <param name="pis">PIS/NIT - S� ser�o considerados os algarismos, os demais caracteres ser�o ignorados sem gerar erro.</param>
        public PIS(string pis)
        {   
            try{
                Valor = Int64.Parse(LimpaString(pis));
                if(!IsValid(Valor))
                    Valor = 0;
            }
            catch{
                Valor = 0;
            }
        }

        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="pis">CNPF ou CPF</param>
        public PIS(Int64 pis)
        {
            Valor = pis;
        } 
        #endregion

     #region propriedades
        
        /// <summary>
        /// valor num�rico do PIS
        /// </summary>
        public Int64 Valor
        {
            get { return valor; }
            set { valor = value; }
        }
     #endregion

     #region Metodos
        /// <summary>
        /// Retira todos os caracteres n�o num�rico
        /// </summary>
        /// <returns>string num�rico puro</returns>
        private static string LimpaString(string entrada)
        {
            StringBuilder Aux = new StringBuilder();
            for (int i = 0; i < entrada.Length; i++)
                if (char.IsDigit(entrada[i]))
                    Aux.Append(entrada[i]);
            return Aux.ToString();
        }

        /// <summary>
        /// Representa��o String do PIS formatado
        /// </summary>
        /// <remarks>Sobreescreve o ToString original</remarks>
        /// <returns>Representa��o String do PIS formatado</returns>
        public override string ToString()
        {
            return ToString(true);
        }

        /// <summary>
        /// Representa��o String do PIS formatado
        /// </summary>
        /// <param name="Formato">Usar F para formatar</param>
        /// <remarks>Sobreescreve o ToString original</remarks>
        /// <returns>Representa��o String do PIS formatado</returns>
        public string ToString(string Formato)
        {
            return ToString(Formato.ToUpper() == "F");
        }

        /// <summary>
        /// Representa��o String do PIS
        /// </summary>
        /// <param name="Formatado">
        /// Formatado: 000.00000.00-0        
        /// </param>
        /// <returns>PIS</returns>
        public string ToString(bool Formatado)
        {
            string Aux;
            Aux = valor.ToString("00000000000");
            if (Formatado)
                        return string.Format("{0:3}.{1:5}.{2:2}-{3:1}", Aux.Substring(0, 3), Aux.Substring(3, 5), Aux.Substring(8, 2), Aux.Substring(10, 1));
            else
                        return Aux;

        }
        #endregion

     #region Metodos Estaticos
        /// <summary>
        /// M�todo estatico que verifica a validade de um PIS
        /// </summary>
        /// <remarks>Para obter a representa��o formatada crie uma inst�ncia ao inv�s de m�todo estatico</remarks>
        /// <returns>Se � valido</returns>
        public static bool IsValid(string Numero)
        {
            Numero = LimpaString(Numero).PadLeft(11,'0');
            
            string Num = Numero.Substring(0, 11);
              
            Int32 k;
            Int32 Soma;
            Int32 Digito;
              
            
            k = 3;
            Soma = 0;
            for (int i = 0; i <= 9; i++)// converte o d�gito para num�rico , multiplica e soma
            {
                Soma += (int.Parse(Num[i].ToString()) * k);
                k--;
                if (k == 1)
                    k = 9;
            };
            Digito = 11 - Soma % 11;
            if (Digito >= 10)
                    Digito = 0;
            return (Digito == int.Parse(Num[10].ToString()));
        }
        /// <summary>
        /// M�todo estatico que verifica a validade de um PIS
        /// </summary>
        /// <remarks>Para obter a representa��o formatada crie uma inst�ncia ao inv�s de m�todo estatico</remarks>
        /// <returns>Se � valido</returns>
        public static bool IsValid(long Numero)
        {
            return IsValid(Numero.ToString());
        }
     #endregion
    }


}
