using System;
using System.Collections.Generic;
using System.Text;
using System.IO.Ports;

namespace DocBacarios
{
    /// <summary>
    /// Classe para impressao de cheques
    /// </summary>
    public class ImpCheque
    {
        private static ImpCheque _ImpChequeSt;

        /// <summary>
        /// Instancia est�tica
        /// </summary>
        public static ImpCheque ImpChequeSt
        {
            get
            {
                if (_ImpChequeSt == null) 
                {
                    DocBacarios.ImpCheque.Modelos Modelo;
                    string ImpressoraCheque = CompontesBasicos.dEstacao.dEstacaoSt.GetValor("Impressora de Cheques: Tela = 0, Check_Pronto = 1,Perto = 2");
                    if (ImpressoraCheque == "")
                    {
                        Modelo = DocBacarios.ImpCheque.Modelos.Perto;                        
                        CompontesBasicos.dEstacao.dEstacaoSt.SetValor("Impressora de Cheques: Tela = 0, Check_Pronto = 1,Perto = 2", ((int)Modelo).ToString());
                    }
                    else
                        Modelo = (DocBacarios.ImpCheque.Modelos)int.Parse(ImpressoraCheque);
                    _ImpChequeSt = new ImpCheque(Modelo);
                }
                    
                return _ImpChequeSt;
            }
        }

        public bool Teste = false;
        

        private string _PortaCOM;

        private string PortaCOM
        {
            get
            {
                if (_PortaCOM == null)
                    _PortaCOM = CompontesBasicos.dEstacao.dEstacaoSt.GetValor("Porta serial da Impressora de Cheque");
                if (_PortaCOM == "")
                {
                    _PortaCOM = "COM1";
                    CompontesBasicos.dEstacao.dEstacaoSt.SetValor("Porta serial da Impressora de Cheque", _PortaCOM);
                }                
                return _PortaCOM;
            }
        }


        #region Enumera�oes
        /// <summary>
        /// Modelos de impressora
        /// </summary>
        public enum Modelos
        {
            /// <summary>
            /// Mosta uma mensagem na tela
            /// Usado em testes
            /// </summary>
            Tela = 0,
            /// <summary>
            /// Check-Pronto
            /// </summary>
            Check_Pronto = 1,
            /// <summary>
            /// Perto check
            /// </summary>
            Perto = 2
        }

        /// <summary>
        /// Status da impressora
        /// </summary>
        [Flags]        
        public enum Status : byte { 
            /// <summary>
            /// ok
            /// </summary>
            ok = 0x00,
            /// <summary>
            /// Buffer cheio
            /// </summary>
            Buffer_Cheio = 0x02,
            /// <summary>
            /// Sem papel
            /// </summary>
            sem_papel = 0x04,
            /// <summary>
            /// imprimindo
            /// </summary>
            imprimindo = 0x08,
            /// <summary>
            /// falha na margem
            /// </summary>
            falha_na_margem = 0x10,
            /// <summary>
            /// falha no motor
            /// </summary>
            falha_no_motor = 0x40,
            /// <summary>
            /// Erro de paridade
            /// </summary>
            erro_no_cabo = 0x80,
            /// <summary>
            /// Valor indefinido
            /// </summary>
            Indefinido = 0xff
        }

        #endregion

        #region Constantes
        private int TamanoBuf = 1024;
        private byte bImprimir = 176;
        private byte bFavorecido = 160;
        private byte bLocal = 161;
        private byte bBanco =  162;
        private byte bValor =  163;
        private byte bData =  164;
        private byte bESC =  27;
        private byte bCR =  13;
        private byte bLF = 10;
        private byte bPedeStatus = 0;
        private byte bSTX = 2;
        private byte bETX = 3;
        private byte bACK = 6;
        
        //private byte bNAK = 21;

        #endregion

        #region Funcoes p�blicas

        /// <summary>
        /// O cheque esta dentro da impressora e o codigo cmc7 j� foi lido
        /// </summary>
        public bool ChequeDentro {
            get {
                return _ChequeDentro;
            }
        }

        /// <summary>
        /// Usado para Perto
        /// </summary>
        public string Retorno;
        /// <summary>
        /// Banco do cheque
        /// </summary>
        public int CMC7Banco;
        /// <summary>
        /// Agencia do cheque
        /// </summary>
        public int CMC7Agencia;
        /// <summary>
        /// Conta do cheque
        /// </summary>
        public int CMC7Conta;
        public int CMC7ContaD;
        /// <summary>
        /// N�mero do cheque
        /// </summary>
        public int CMC7Numero;

        /// <summary>
        /// Abre a serial
        /// </summary>
        public void Abrir()
        {
            if(Modelo == Modelos.Tela)
                return;
            else
                if (!SerialPort1.IsOpen)
                     SerialPort1.Open();                            
        }

        /// <summary>
        /// fecha a serial
        /// </summary>
        public void Fechar()
        {
            if (Modelo == Modelos.Tela)
                return;
            if (SerialPort1.IsOpen)
                SerialPort1.Close();
        }

        /// <summary>
        /// Construtor
        /// </summary>        
        /// Modelo
        /// <param name="Mod"></param>
        public ImpCheque(Modelos Mod)
        {            
            Modelo = Mod;
            if (Mod != Modelos.Tela)
            {
                SerialPort1 = new SerialPort(PortaCOM);
                SerialPort1.DataReceived += new SerialDataReceivedEventHandler(SerialPort1_DataReceived);
            };
            switch(Mod){
                case Modelos.Perto: 
                    SerialPort1.DtrEnable = SerialPort1.RtsEnable = true;
                    Tradutor = Encoding.GetEncoding(1253);
                break;
                case Modelos.Check_Pronto:
                    Tradutor = Encoding.GetEncoding(860);
                    break;
            };
                        
            BufSaida = new byte[TamanoBuf];
            BufEntrada = new byte[TamanoBuf];
            FimSaida = FimEntrada = 0;
            
            UltimoStatus = Status.Indefinido;
        }

        public bool ProgramaLayout(int Banco, int ValorX, int ValorY, int extensoX, int extensoY1, int extensoY2, int AnoX, int AnoY, int DataX,int BeneficiarioY,int Tamanho)
        {
            string paramentro = string.Format("1{0:000}{1:000}{2:000}{3:000}{4:000}{5:000}{6:000}{7:000}{8:000}{9:000}{10:000}",
                    Banco, ValorX, extensoX, AnoX, DataX, ValorY, extensoY1, extensoY2, BeneficiarioY, AnoY, Tamanho);
            if (Modelo == Modelos.Perto)
            {
                ProgramaFormatado(0x42, paramentro);
                if (!Imprime())
                {
                    System.Windows.Forms.MessageBox.Show(string.Format("Erro: Parametro: <{0}>\r\nC�digo:{1}", paramentro,ErroPetp));
                    Retorno += "Erro ao programar";
                    return false;
                }
                else
                    return true;
            }
            else
            {
                System.Windows.Forms.MessageBox.Show(string.Format("Parametro: <{0}>", paramentro));
                return true;
            }
        }

        public bool LeLayout(int Banco, ref int ValorX, ref  int ValorY, ref int extensoX, ref int extensoY1, ref int extensoY2, ref int AnoX, ref int AnoY, ref int DataX, ref int BeneficiarioY, ref int Tamanho)
        {
            if (Modelo == Modelos.Perto)
            {
                ProgramaFormatado(0x42, string.Format("2{0:000}",Banco));
                if(Teste)
                    System.Windows.Forms.MessageBox.Show(string.Format("3 - Leitura"));
                if (Imprime())
                {
                    if (Teste)
                        System.Windows.Forms.MessageBox.Show(string.Format("4 - Leitura ok Retorno:{0}", Retorno));
                    if (Retorno.Length < 38)
                        return false;
                    ValorX = int.Parse(Retorno.Substring(8, 3));
                    extensoX = int.Parse(Retorno.Substring(11, 3));
                    AnoX = int.Parse(Retorno.Substring(14, 3));
                    DataX = int.Parse(Retorno.Substring(17, 3));
                    ValorY = int.Parse(Retorno.Substring(20, 3));
                    extensoY1 = int.Parse(Retorno.Substring(23, 3));
                    extensoY2 = int.Parse(Retorno.Substring(26, 3));
                    BeneficiarioY = int.Parse(Retorno.Substring(29, 3));
                    AnoY = int.Parse(Retorno.Substring(32, 3));
                    Tamanho = int.Parse(Retorno.Substring(35, 3));
                    return true;
                }
                else
                {
                    if (Teste)
                        System.Windows.Forms.MessageBox.Show(string.Format("4 - Erro na leitura: {0}\r\nRetorno <{1}>", ErroPetp,Retorno));
                    return false;
                }
            }
            else
            {
                Retorno = "B2errbbb111222333444555666777888999000";
                ValorX = int.Parse(Retorno.Substring(8, 3));
                extensoX = int.Parse(Retorno.Substring(11, 3));
                AnoX = int.Parse(Retorno.Substring(14, 3));
                DataX = int.Parse(Retorno.Substring(17, 3));
                ValorY = int.Parse(Retorno.Substring(20, 3));
                extensoY1 = int.Parse(Retorno.Substring(23, 3));
                extensoY2 = int.Parse(Retorno.Substring(26, 3));
                BeneficiarioY = int.Parse(Retorno.Substring(29, 3));
                AnoY = int.Parse(Retorno.Substring(32, 3));
                Tamanho = int.Parse(Retorno.Substring(35, 3));
                return true;
            }
        }

        /// <summary>
        /// Imprime um cheque
        /// </summary>
        /// <param name="Banco"></param>
        /// <param name="Valor"></param>
        /// <param name="Local"></param>
        /// <param name="Favorecido"></param>
        /// <param name="Data"></param>       
        /// <returns>resultado</returns>
        public bool Imprime(int Banco, decimal Valor, string Local, string Favorecido, DateTime Data)
        {
            switch (Modelo)
            {
                case Modelos.Check_Pronto:
                    if (
                        ProgramaFormatado(bFavorecido, Favorecido)
                        &&
                        ProgramaFormatado(bLocal, Local)
                        &&
                        ProgramaFormatado(bBanco, Banco.ToString("000"))
                        &&
                        ProgramaFormatado(bValor, Valor.ToString("0.00"))
                        &&
                        ProgramaFormatado(bData, Data.ToString("dd/MM/yy"))
                        &&
                        ProgramaFormatado(bImprimir, "")
                        )
                    {
                        Imprime();
                        return true;
                    }
                    else
                        return false;
                case Modelos.Perto:
                    if (Favorecido.Length > 65)
                        Favorecido = Favorecido.Substring(0, 65);
                    if (Local.Length > 20)
                        Local = Local.Substring(0, 20);
                    //data
                    ProgramaFormatado(0x21,Data.ToString("ddMMyy"));
                    if (!Imprime())
                    {
                        Retorno += "Erro ao programar a data";
                        return false;
                    };
                    //Cidade
                    ProgramaFormatado(0x23, Local.ToUpper());
                    if (!Imprime())
                    {
                        Retorno += "Erro ao programar o local";
                        return false;
                    };
                    //Favorecido 
                    if (Favorecido == "")
                        Favorecido = " ";
                    ProgramaFormatado(0x25, Favorecido.ToUpper());
                    if (!Imprime())
                    {
                        Retorno += "Erro ao programar o Favorecido";
                        return false;
                    }
                    Valor *= 100;
                    byte ComandoImp = ChequeDentro ? (byte)0x3b : (byte)0x24;            
                    ProgramaFormatado(ComandoImp, "4"+Valor.ToString("000000000000")+Banco.ToString("000"));
                    if (Imprime()) {
                        _ChequeDentro = false;
                        return true;
                    }
                    else
                        return false;
                case Modelos.Tela:
                    string Mostrar = "Banco = " + Banco.ToString("000") + "\r\n";
                    Mostrar += "Favorecido: " + Favorecido + "\r\n";
                    Mostrar += "Valor: " + Valor.ToString("0.00") + "\r\n";
                    Mostrar += "Data: " + Data.ToString("dd/MM/yyyy") + "\r\n";
                    Mostrar += "Local: " + Local + "\r\n";
                    System.Windows.Forms.MessageBox.Show(Mostrar);
                    return true;
                default:
                    return false;
            }
        }

        /// <summary>
        /// Imprime em modo texto
        /// </summary>
        /// <param name="Texto"></param>
        /// <returns>resultado</returns>
        public bool Imprime(params string[] Texto)
        {
            if (Texto.Length == 0)
                return false;
            switch (Modelo)
            {
                case Modelos.Check_Pronto:
                    foreach (string Linha in Texto)
                        AddStringln(Linha);
                    Imprime();
                    return true;

                case Modelos.Perto:
                    // no lugar do marcador 255 � usado o 000 para evitar problema com o encoding WIN 1253
                    string texto = "";
                    for (int linha = 0; linha <= 13; linha++)
                    {
                        string Manobra = (linha < Texto.Length ? Texto[linha].ToUpper() : "");
                        if (Manobra.Length > 78)
                            Manobra = Manobra.Substring(0, 78);
                        texto += Manobra + '\x00';
                    }
                    if (ChequeDentro)
                        ProgramaFormatado(0x5A, texto);
                    else
                        ProgramaFormatado(0x58, texto);
                    if (Imprime())
                    {
                        _ChequeDentro = false;
                        return true;
                    }
                    else
                    {
                        return false;
                    };
                    

                case Modelos.Tela:
                    string Mostrar = "";
                    foreach (string Linha in Texto)
                        Mostrar += Linha + "\r\n";
                    System.Windows.Forms.MessageBox.Show(Mostrar);
                    return true;
                default:
                    return false;
            }
        }

        /// <summary>
        /// Imprime em modo texto
        /// </summary>
        /// <param name="Texto"></param>
        /// <returns></returns>
        public bool Imprime(System.Collections.ArrayList Texto) {
            string[] Strs = new string[Texto.Count];
            for (int i = 0; i < Texto.Count; i++)
                Strs[i] = Texto[i].ToString();
            return Imprime(Strs);
        }


        /// <summary>
        /// Retorna o Status
        /// </summary>
        /// <returns>Status</returns>
        /// <remarks>S� � v�lido para check pronto</remarks>
        public Status VerificaStatus()
        {
            if (Modelo == Modelos.Check_Pronto)
            {
                Addbytes(bPedeStatus);
                Imprime();
                UltimoStatus = Status.Indefinido;
                DateTime Em1Segundo = DateTime.Now.AddSeconds(10);
                while ((UltimoStatus == Status.Indefinido) && (DateTime.Now < Em1Segundo))
                    System.Windows.Forms.Application.DoEvents();
                return UltimoStatus;
            }
            else
                return Status.ok;
        }

        /// <summary>
        /// Le o CMC7 do cheque
        /// </summary>
        /// <returns></returns>
        public bool LeDadosDoCheque() {
            if (Modelo == Modelos.Perto)
            {
                ProgramaFormatado(0x3d, "");
                CMC7Agencia = CMC7Banco = CMC7Numero = CMC7Conta = 0;
                if (Imprime())
                {
                    if (Retorno.Length < 27)
                    {
                        ErroPetp = "001";
                        return false;
                    }
                    CMC7Banco = int.Parse(Retorno.Substring(4, 3));
                    CMC7Agencia = int.Parse(Retorno.Substring(7, 4));
                    if (CMC7Banco == 341)
                        CMC7Conta = int.Parse(Retorno.Substring(15, 5));
                    else
                        CMC7Conta = int.Parse(Retorno.Substring(14, 6));
                    CMC7ContaD = int.Parse(Retorno.Substring(20, 1));
                    CMC7Numero = int.Parse(Retorno.Substring(21, 6));
                    _ChequeDentro = true;
                    return true;
                }
                else
                    return false;
            }
            return true;
        }

        /// <summary>
        /// Remove o cheque da impressora
        /// </summary>
        public bool RemoveCheque() {
            ProgramaFormatado(0x3e, "");
            if (Imprime())
            {
                _ChequeDentro = false;
                return true;
            }
            else
                return false;
        }

        /// <summary>
        /// Retorna a descri��o do �ltimo erro perto
        /// </summary>
        /// <returns></returns>
        public string StatusPerto()
        {
            if (ErroPetp == "")
                return "---";
            return ErrosPerto[ErroPetp].ToString();
        }

        /// <summary>
        /// Modelo
        /// </summary>
        public Modelos ModeloImpressora {
            get {
                return Modelo;
            }
        }

        #endregion


        #region Campos privadados
        private Modelos Modelo;
        private SerialPort SerialPort1;
        private byte[] BufSaida;
        private int FimSaida;
        private byte[] BufEntrada;
        private int FimEntrada;
        private Encoding Tradutor;
        private Status UltimoStatus;
        private System.Collections.SortedList _ErrosPerto;
        private System.Collections.SortedList ErrosPerto
        {
            get
            {
                if (_ErrosPerto == null)
                {
                    _ErrosPerto = new System.Collections.SortedList();
                    _ErrosPerto.Add("000", "Sucesso na execu��o do comando.");
                    _ErrosPerto.Add("001", "Mensagem com dados inv�lidos.");
                    _ErrosPerto.Add("002", "Tamanho de mensagem inv�lido.");
                    _ErrosPerto.Add("005", "Leitura dos caracteres magn�ticos inv�lida.");
                    _ErrosPerto.Add("006", "Problemas no acionamento do motor 1.");
                    _ErrosPerto.Add("008", "Problemas no acionamento do motor 2.");
                    _ErrosPerto.Add("009", "Banco diferente do solicitado.");
                    _ErrosPerto.Add("011", "Sensor 1 obstru�do.");
                    _ErrosPerto.Add("012", "Sensor 2 obstru�do.");
                    _ErrosPerto.Add("013", "Sensor 4 obstru�do.");
                    _ErrosPerto.Add("014", "Erro no posicionamento da cabe�a de impress�o (relativo a S4).");
                    _ErrosPerto.Add("016", "D�gito verificador do cheque n�o confere.");
                    _ErrosPerto.Add("017", "Aus�ncia de caracteres magn�ticos ou cheque na posi��o errada.");
                    _ErrosPerto.Add("018", "Tempo esgotado.");
                    _ErrosPerto.Add("019", "Documento mal inserido.");
                    _ErrosPerto.Add("020", "Cheque preso durante o alinhamento (S1 e S2 desobstru�dos).");
                    _ErrosPerto.Add("021", "Cheque preso durante o alinhamento (S1 obstru�do e S2 desobstru�do).");
                    _ErrosPerto.Add("022", "Cheque preso durante o alinhamento (S1 desobstru�do e S2 obstru�do).");
                    _ErrosPerto.Add("023", "Cheque preso durante o alinhamento (S1 e S2 obstru�dos).");
                    _ErrosPerto.Add("024", "Cheque preso durante o preenchimento (S1 e S2 desobstru�dos).");
                    _ErrosPerto.Add("025", "Cheque preso durante o preenchimento (S1 obstru�do e S2 desobstru�do).");
                    _ErrosPerto.Add("026", "Cheque preso durante o preenchimento (S1 desobstru�do e S2 obstru�do).");
                    _ErrosPerto.Add("027", "Cheque preso durante o preenchimento (S1 e S2 obstru�dos).");
                    _ErrosPerto.Add("028", "Caractere inexistente.");
                    _ErrosPerto.Add("030", "N�o h� cheques na mem�ria.");
                    _ErrosPerto.Add("031", "Lista negra interna cheia");
                    _ErrosPerto.Add("042", "Cheque ausente.");
                    _ErrosPerto.Add("043", "PINPad ou teclado ausente.");
                    _ErrosPerto.Add("050", "Erro de transmiss�o.");
                    _ErrosPerto.Add("051", "Erro de transmiss�o: Impressora offline, desconectada ou ocupada.");
                    _ErrosPerto.Add("052", "Erro no pin pad.");
                    _ErrosPerto.Add("060", "Cheque na lista negra.");
                    _ErrosPerto.Add("073", "Cheque n�o encontrado na lista negra.");
                    _ErrosPerto.Add("074", "Comando cancelado.");
                    _ErrosPerto.Add("084", "Arquivo de layout�s cheio ");
                    _ErrosPerto.Add("085", "Layout inexistente na mem�ria.");
                    _ErrosPerto.Add("091", "Leitura de cart�o inv�lida.");
                    _ErrosPerto.Add("092", "Erro na leitura da trilha 1 (somente para leitora 2 trilhas)");
                    _ErrosPerto.Add("093", "Erro na leitura da trilha 2 (somente para leitora 2 trilhas)");
                    _ErrosPerto.Add("094", "Erro na leitura da trilha 3 (somente para leitora 2 trilhas)");
                    _ErrosPerto.Add("097", "Cheque na posi��o errada.");
                    _ErrosPerto.Add("111", "PINPad n�o retornou EOT.");
                    _ErrosPerto.Add("150", "PINPad n�o retornou ACK.");
                    _ErrosPerto.Add("155", "PINPad n�o responder.");
                    _ErrosPerto.Add("171", "Tempo esgotado na resposta do PINPad.");
                    _ErrosPerto.Add("253", "Erro em equipamento fiscal (Sem cidade, Falta redu��o Z, etc....)");
                    _ErrosPerto.Add("255", "Comando inexistente.");
                };
                return _ErrosPerto;
            }
        }
        private string ErroPetp = "";
        private bool _ChequeDentro = false;

        
        #endregion

        
     
       


        /// <summary>
        /// Grava no buffer de saida um comando formatado
        /// </summary>
        /// <param name="Comando"></param>
        /// <param name="Parametro"></param>
        /// <returns></returns>
        private bool ProgramaFormatado(byte Comando, String Parametro)
        {
            switch (Modelo)
            {
                case Modelos.Check_Pronto:
                    return Addbytes(bESC) && Addbytes(Comando) && AddString(Parametro) && Addbytes(bCR);
                case Modelos.Perto:
                    return Addbytes(bSTX) && Addbytes(Comando) && AddString(Parametro) && Addbytes(bETX) && AddBCC();
                default:
                    return false;
            }
        }

        private bool AddBCC()
        {
            byte BCC = 0;
            for (int i = 0; i < FimSaida; i++)
                BCC = (byte)(BCC ^ BufSaida[i]);            
            return Addbytes(BCC);
        }

        /// <summary>
        /// Inclui uma linha ao buffer de saida
        /// </summary>
        /// <param name="Linha"></param>
        /// <returns></returns>
        private bool AddStringln(string Linha)
        {
            return AddString(Linha) && Addbytes(bLF);
        }

        /// <summary>
        /// Inclui dados ao buffer de saida
        /// </summary>
        /// <param name="Linha"></param>
        /// <remarks>Corrige os bytes errados na tabela</remarks>
        /// <returns></returns>
        private bool AddString(string Linha)
        {
            return Addbytes(Traducao(Linha));
        }

        /// <summary>
        /// Corrige os caracteres que nao batem com a tabela
        /// </summary>
        /// <param name="Argumento"></param>
        /// <returns></returns>
        private byte[] Traducao(string Argumento) {
            byte[] bArg;
            bArg = Tradutor.GetBytes(Argumento);
            switch(Modelo){
                case Modelos.Check_Pronto:
                   for (int i = 0; i < bArg.Length; i++)
                       if (bArg[i] == 139)
                          bArg[i] = 141;                
                       else if (bArg[i] == 150)
                          bArg[i] = 151;
                       else if (bArg[i] == 145)
                          bArg[i] = 150;
                  break;
                case Modelos.Perto:
                    for (int i = 0; i < bArg.Length; i++)
                        if (bArg[i] == 0)
                            bArg[i] = 255;
                        else if (bArg[i] == 176)
                            bArg[i] = 32;
                        else if (bArg[i] == 63)
                            bArg[i] = 32;
                    break;
            };
            return bArg;                
        }

        /// <summary>
        /// Inclui bytes ao buffer de saida
        /// </summary>
        /// <param name="Enviar"></param>
        /// <returns></returns>
        private bool Addbytes(params byte[] Enviar)
        {
            if (Enviar.Length + FimSaida > TamanoBuf)
                return false;
            else
            {
                Enviar.CopyTo(BufSaida, FimSaida);
                FimSaida += Enviar.Length;
                return true;
            }
        }

        /// <summary>
        /// Para uso no retorno Perto
        /// </summary>
        /// <returns></returns>
        private bool EntradaCompleta() {
            if (FimEntrada < 4)
                return false;
            else
                return (BufEntrada[FimEntrada - 2] == bETX);
        }

        /// <summary>
        /// Imprime o buffer de saida
        /// </summary>
        /// <returns></returns>     
        private bool Imprime() {
            bool EstavaAberta = SerialPort1.IsOpen;
            try
            {
                if (!EstavaAberta)
                    SerialPort1.Open();
                if (FimSaida > 0)
                {
                    SerialPort1.Write(BufSaida, 0, FimSaida);
                    FimSaida = 0;
                    if (Modelo == Modelos.Perto)
                    {
                        Retorno = "";
                        ErroPetp = "";
                        FimEntrada = 0;
                        bool ACKRecebido = false;
                        DateTime TimeOut = DateTime.Now.AddSeconds(3);


                        while (!EntradaCompleta())
                        {
                            if (!ACKRecebido && (FimEntrada > 0))
                            {
                                if (BufEntrada[0] != bACK)
                                {
                                    ErroPetp = "050";
                                    return false;
                                }
                                ACKRecebido = true;
                                TimeOut = DateTime.Now.AddMinutes(2);
                            };

                            if (DateTime.Now > TimeOut)
                            {
                                Retorno = "Sem resposta";
                                ErroPetp = ACKRecebido ? "050" : "051";
                                return false;
                            };
                            if (SerialPort1.BytesToRead > 0)
                                FimEntrada += SerialPort1.Read(BufEntrada, FimEntrada, SerialPort1.BytesToRead);
                        };

                        if (BufEntrada[0] != bACK)
                        {
                            ErroPetp = "050";
                            return false;
                        }
                        for (int i = 2; i < (FimEntrada - 2); i++)
                            Retorno += (char)BufEntrada[i];
                        BufSaida[0] = bACK;
                        SerialPort1.Write(BufSaida, 0, 1);
                        if ((Retorno.Length < 4) || (Retorno[0] != BufSaida[1]))
                        {
                            ErroPetp = "050";
                            return false;
                        }
                        else
                        {
                            if (Retorno.Substring(0, 1) == "B")
                            {
                                ErroPetp = Retorno.Substring(2, 3);
                                return (ErroPetp == "000");
                            }
                            else
                            {
                                ErroPetp = Retorno.Substring(1, 3);
                                return (ErroPetp == "000");
                            }
                        }

                    }
                    else
                        return true;
                }
                else
                    return false;
            }
            finally
            {
                if (!EstavaAberta)
                    SerialPort1.Close();
            }
        }

        /// <summary>
        /// Retorno da seria
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        /// <remarks>N�o � v�lido para perto pois o trabalho � sincrono</remarks>
        private void SerialPort1_DataReceived(object sender, SerialDataReceivedEventArgs e)
        {
            if (Modelo != Modelos.Check_Pronto)
                return;
            if (e.EventType == System.IO.Ports.SerialData.Chars)
            {
                FimEntrada = 0;
                int paraler = SerialPort1.BytesToRead;
                FimEntrada += SerialPort1.Read(BufEntrada, FimEntrada, paraler);
                if (FimEntrada > 0)
                    UltimoStatus = (Status)BufEntrada[0];

                //estamos desabilitando o retorno (retorna sempre ok)
                UltimoStatus = Status.ok;
               // if (SerialPort1.BytesToRead > 0)
                 //   System.Windows.Forms.MessageBox.Show("aind tem: " + SerialPort1.BytesToRead.ToString());
            };
        }

    }
}
