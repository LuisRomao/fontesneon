using System;
using System.Collections.Generic;
using System.Text;

namespace DocBacarios
{
    /// <summary>
    /// CNPJ ou CPF
    /// </summary>
    /// <remarks>Valida e Formata</remarks>
    public class CPFCNPJ
    {
        #region Campos
        /// <summary>
        /// Valor como n�mero
        /// </summary>
        private Int64 valor;
        /// <summary>
        /// Tipo CFP,CNPJ ou inv�lido
        /// </summary>
        private TipoCpfCnpj tipo;

        private bool ForcarCNPJ = false;
        #endregion
        
        #region Construtores
        /// <summary>
        /// Constuctor
        /// </summary>
        /// <param name="cpfcnpj">CNPJ ou CPF - S� ser�o considerados os algarismos, os demais caracteres ser�o ignorados sem gerar erro.</param>
        public CPFCNPJ(string cpfcnpj)
        {   
            try
            {
                string cpfcnpjLimpo = LimpaString(cpfcnpj);
                ForcarCNPJ = (cpfcnpjLimpo.Length > 11);
                Valor = Int64.Parse(cpfcnpjLimpo);
            }
            catch{
                Valor = 0;
            }
        }

        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="cpfcnpj">CNPF ou CPF</param>
        public CPFCNPJ(Int64 cpfcnpj)
        {
            Valor = cpfcnpj;
        } 
        #endregion

        #region propriedades
        /// <summary>
        /// Indica o tipo CNPJ,CPF ou Invalido
        /// </summary>
        /// <value>CNPJ,CPF ou Invalido</value>
        public TipoCpfCnpj Tipo
        {
            get { return tipo; }
        }

        /// <summary>
        /// valor num�rico do CNPJ/CPF
        /// </summary>
        public Int64 Valor
        {
            get => valor; 
            set
            {                 
                valor = value;
                string strvalor = valor.ToString();
                //if ((strvalor.Length < 13) && (!IsValid(strvalor)))
                if(ForcarCNPJ || !IsValid(strvalor))
                    strvalor = strvalor.PadLeft(13, '0');
                if (IsValid(strvalor))
                {
                    if (strvalor.Length > 11)
                        tipo = TipoCpfCnpj.CNPJ;
                    else
                        tipo = TipoCpfCnpj.CPF;
                }
                else
                    tipo = TipoCpfCnpj.INVALIDO;
            }
        } 
        #endregion

        #region Metodos
        /// <summary>
        /// Retira todos os caracteres n�o num�rico
        /// </summary>
        /// <returns>string num�rico puro</returns>
        private static string LimpaString(string entrada)
        {
            StringBuilder Aux = new StringBuilder();
            for (int i=0;i<entrada.Length;i++)
                if(char.IsDigit(entrada[i]))
                    Aux.Append(entrada[i]);
            return Aux.ToString();
        }

        /// <summary>
        /// Representa��o String do CNPJ/CPF formatado
        /// </summary>
        /// <remarks>Sobreescreve o ToString original</remarks>
        /// <returns>Representa��o String do CNPJ/CPF formatado</returns>
        public override string ToString()
        {
            return ToString(true);
        }

        /// <summary>
        /// Representa��o String do CNPJ/CPF formatado
        /// </summary>
        /// <param name="Formato">Usar F para formatar</param>
        /// <remarks>Sobreescreve o ToString original</remarks>
        /// <returns>Representa��o String do CNPJ/CPF formatado</returns>
        public string ToString(string Formato)
        {
            return ToString(Formato.ToUpper() == "F");
        }

        /// <summary>
        /// Representa��o String do CNPJ/CPF
        /// </summary>
        /// <param name="Formatado">
        /// Formatado: 00.000.000/0000-00 ou 000.000.000-00
        /// n�o formatado: 00000000000000 ou 00000000000
        /// </param>
        /// <returns>CNPJ/CPF</returns>
        public string ToString(bool Formatado)
        {
            string Aux;
            switch (tipo){
                case TipoCpfCnpj.CNPJ:   
                    Aux = valor.ToString("00000000000000");
                    if(Formatado)
                       return string.Format("{0:2}.{1:3}.{2:3}/{3:4}-{4:2}",Aux.Substring(0,2),Aux.Substring(2,3),Aux.Substring(5,3),Aux.Substring(8,4),Aux.Substring(12,2));
                    else
                       return Aux;
                    
                case TipoCpfCnpj.CPF:
                    Aux = valor.ToString("00000000000");
                    if (Formatado)
                        return string.Format("{0:3}.{1:3}.{2:3}-{3:2}", Aux.Substring(0, 3), Aux.Substring(3, 3), Aux.Substring(6, 3), Aux.Substring(9, 2));
                    else
                        return Aux;
                    
                default:
                    return "";
            };
        
        } 
        #endregion

        //public bool TratarZerosComoInvalido = false;

        #region Metodos Estaticos
        /// <summary>
        /// M�todo est�tico que verifica a validade de um CNPJ ou CPF
        /// </summary>
        /// <remarks>Para obter a representa��o formatada crie uma inst�ncia ao inv�s de m�todo est�tico</remarks>
        /// <returns>Se � valido</returns>
        public static bool IsValid(string Numero)
        {
            Numero=LimpaString(Numero);            
            bool cnpj = (Numero.Length > 11);
            for (; Numero.Length < (cnpj?14:11);)
                    Numero = "0" + Numero;
            if ((Numero == "00000000000") || (Numero == "00000000000000"))
                return false;
            string Num = Numero.Substring(0, cnpj?12:9);
            string Resultado=Num;
            Int32 k;
            Int32 Soma;
            Int32 Digito;
            Int32 DigitoTotal = 0;
            for (int j = 0; j < 2; j++) // calcula duas vezes
            {
                k = 2;
                Soma = 0;
                for (int i = Resultado.Length - 1; i >= 0; i--)// converte o d�gito para num�rico , multiplica e soma
                {
                    Soma += (int.Parse(Resultado[i].ToString()) * k);
                    k++;
                    if ((k > 9) && cnpj)
                     k=2;
                };
                Digito = 11  - Soma % 11;
                if (Digito >= 10)
                   Digito = 0;
               Resultado += Convert.ToChar(Digito.ToString());
               DigitoTotal += Digito * (j == 0 ? 10 : 1);
            };
            return (DigitoTotal == Int32.Parse(Numero.Substring(cnpj?12:9,2)));
        }
        /// <summary>
        /// M�todo estatico que verifica a validade de um CNPJ ou CPF
        /// </summary>
        /// <remarks>Para obter a representa��o formatada crie uma inst�ncia ao inv�s de m�todo estatico</remarks>
        /// <returns>Se � valido</returns>
        public static bool IsValid(long Numero)
        {
            return IsValid(Numero.ToString());
        }
        #endregion

        
    }

    /// <summary>
    /// Tipos de c�digos
    /// </summary>
    public enum TipoCpfCnpj
    {
        /// <summary>
        /// CPF
        /// </summary>
        CPF,
        /// <summary>
        /// CNPJ
        /// </summary>
        CNPJ,
        /// <summary>
        /// Valor Inv�lido
        /// </summary>
        INVALIDO,
    }
}
