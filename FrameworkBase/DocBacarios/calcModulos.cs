using System;
using System.Collections.Generic;
using System.Text;

namespace DocBacarios
{
    /// <summary>
    /// Calculo de Verificadores
    /// </summary>
    public class calcModulos
    {
        /// <summary>
        /// Modulo11_2_9
        /// </summary>
        /// <param name="entrada"></param>
        /// <returns></returns>
        public static int Modulo11_2_9(string entrada)
        {
            //entrada = "11x" + entrada;
            //VirEmailNeon.EmailDiretoNeon.EmalST.EmTeste();
            Int32 soma = 0;
            Int32 y = 2;
            Int32 resto;
            for (int x = entrada.Length - 1; x >= 0; x--)
            {
                try
                {
                    soma += Int32.Parse(entrada.Substring(x, 1)) * y;
                }
                catch (Exception e)
                {
                    throw new Exception("Erro no c�lculo do digito:'" + entrada + "' posi��o:" + x.ToString(), e);
                }
                y++;
                if (y == 10)
                    y = 2;
            };
            resto = 11 - (soma % 11);
            
            if ((resto == 0) || (resto > 9))
                    return 0;
            else
                    return resto;
            
        }
    }
}
