namespace DocBacarios
{
    partial class cConfigCheque
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.panelControl1 = new DevExpress.XtraEditors.PanelControl();
            this.spincda = new DevExpress.XtraEditors.SpinEdit();
            this.spinlbe = new DevExpress.XtraEditors.SpinEdit();
            this.spinlan = new DevExpress.XtraEditors.SpinEdit();
            this.spincan = new DevExpress.XtraEditors.SpinEdit();
            this.label6 = new System.Windows.Forms.Label();
            this.spince1 = new DevExpress.XtraEditors.SpinEdit();
            this.label5 = new System.Windows.Forms.Label();
            this.spinle2 = new DevExpress.XtraEditors.SpinEdit();
            this.spinle1 = new DevExpress.XtraEditors.SpinEdit();
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.spincvn = new DevExpress.XtraEditors.SpinEdit();
            this.spinlvn = new DevExpress.XtraEditors.SpinEdit();
            this.label2 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.simpleButton1 = new DevExpress.XtraEditors.SimpleButton();
            this.simpleButton2 = new DevExpress.XtraEditors.SimpleButton();
            this.spinbanco = new DevExpress.XtraEditors.SpinEdit();
            this.spincpr = new DevExpress.XtraEditors.SpinEdit();
            ((System.ComponentModel.ISupportInitialize)(this.StyleController_F)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl1)).BeginInit();
            this.panelControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.spincda.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.spinlbe.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.spinlan.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.spincan.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.spince1.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.spinle2.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.spinle1.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.spincvn.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.spinlvn.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.spinbanco.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.spincpr.Properties)).BeginInit();
            this.SuspendLayout();
            // 
            // ToolTipController_F
            // 
            this.ToolTipController_F.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.ToolTipController_F.Appearance.ForeColor = System.Drawing.Color.Black;
            this.ToolTipController_F.Appearance.Options.UseBackColor = true;
            this.ToolTipController_F.Appearance.Options.UseFont = true;
            this.ToolTipController_F.Appearance.Options.UseForeColor = true;
            this.ToolTipController_F.Appearance.Options.UseTextOptions = true;
            this.ToolTipController_F.Appearance.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            // 
            // StyleController_F
            // 
            this.StyleController_F.AppearanceDisabled.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this.StyleController_F.AppearanceDisabled.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this.StyleController_F.AppearanceDisabled.Options.UseBackColor = true;
            this.StyleController_F.AppearanceDisabled.Options.UseForeColor = true;
            this.StyleController_F.AppearanceFocused.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.StyleController_F.AppearanceFocused.Options.UseBackColor = true;
            // 
            // spellChecker1
            // 
            this.spellChecker1.OptionsSpelling.CheckSelectedTextFirst = DevExpress.Utils.DefaultBoolean.True;
            this.spellChecker1.OptionsSpelling.IgnoreEmails = DevExpress.Utils.DefaultBoolean.True;
            this.spellChecker1.OptionsSpelling.IgnoreMixedCaseWords = DevExpress.Utils.DefaultBoolean.False;
            this.spellChecker1.OptionsSpelling.IgnoreRepeatedWords = DevExpress.Utils.DefaultBoolean.False;
            this.spellChecker1.OptionsSpelling.IgnoreUpperCaseWords = DevExpress.Utils.DefaultBoolean.False;
            this.spellChecker1.OptionsSpelling.IgnoreUrls = DevExpress.Utils.DefaultBoolean.True;
            this.spellChecker1.OptionsSpelling.IgnoreWordsWithNumbers = DevExpress.Utils.DefaultBoolean.True;
            // 
            // panelControl1
            // 
            this.panelControl1.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.panelControl1.Appearance.Options.UseBackColor = true;
            this.panelControl1.Controls.Add(this.spincda);
            this.panelControl1.Controls.Add(this.spinlbe);
            this.panelControl1.Controls.Add(this.spinlan);
            this.panelControl1.Controls.Add(this.spincan);
            this.panelControl1.Controls.Add(this.label6);
            this.panelControl1.Controls.Add(this.spince1);
            this.panelControl1.Controls.Add(this.label5);
            this.panelControl1.Controls.Add(this.spinle2);
            this.panelControl1.Controls.Add(this.spinle1);
            this.panelControl1.Controls.Add(this.label4);
            this.panelControl1.Controls.Add(this.label3);
            this.panelControl1.Controls.Add(this.spincvn);
            this.panelControl1.Controls.Add(this.spinlvn);
            this.panelControl1.Controls.Add(this.label2);
            this.panelControl1.Location = new System.Drawing.Point(21, 60);
            this.panelControl1.LookAndFeel.Style = DevExpress.LookAndFeel.LookAndFeelStyle.UltraFlat;
            this.panelControl1.LookAndFeel.UseDefaultLookAndFeel = false;
            this.panelControl1.Name = "panelControl1";
            this.panelControl1.Size = new System.Drawing.Size(519, 179);
            this.panelControl1.TabIndex = 3;
            // 
            // spincda
            // 
            this.spincda.EditValue = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.spincda.Location = new System.Drawing.Point(299, 136);
            this.spincda.Name = "spincda";
            this.spincda.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.spincda.Properties.SpinStyle = DevExpress.XtraEditors.Controls.SpinStyles.Horizontal;
            this.spincda.Size = new System.Drawing.Size(61, 20);
            this.spincda.TabIndex = 14;
            // 
            // spinlbe
            // 
            this.spinlbe.EditValue = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.spinlbe.Location = new System.Drawing.Point(147, 116);
            this.spinlbe.Name = "spinlbe";
            this.spinlbe.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.spinlbe.Size = new System.Drawing.Size(52, 20);
            this.spinlbe.TabIndex = 13;
            // 
            // spinlan
            // 
            this.spinlan.EditValue = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.spinlan.Location = new System.Drawing.Point(465, 116);
            this.spinlan.Name = "spinlan";
            this.spinlan.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.spinlan.Size = new System.Drawing.Size(52, 20);
            this.spinlan.TabIndex = 12;
            // 
            // spincan
            // 
            this.spincan.EditValue = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.spincan.Location = new System.Drawing.Point(422, 136);
            this.spincan.Name = "spincan";
            this.spincan.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.spincan.Properties.SpinStyle = DevExpress.XtraEditors.Controls.SpinStyles.Horizontal;
            this.spincan.Size = new System.Drawing.Size(61, 20);
            this.spincan.TabIndex = 11;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.Location = new System.Drawing.Point(242, 117);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(216, 16);
            this.label6.TabIndex = 10;
            this.label6.Text = "Cidade  ,31 de  janeiro  de  2010";
            // 
            // spince1
            // 
            this.spince1.EditValue = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.spince1.Location = new System.Drawing.Point(35, 21);
            this.spince1.Name = "spince1";
            this.spince1.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.spince1.Properties.SpinStyle = DevExpress.XtraEditors.Controls.SpinStyles.Horizontal;
            this.spince1.Size = new System.Drawing.Size(61, 20);
            this.spince1.TabIndex = 9;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(6, 117);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(83, 16);
            this.label5.TabIndex = 8;
            this.label5.Text = "beneficiário";
            // 
            // spinle2
            // 
            this.spinle2.EditValue = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.spinle2.Location = new System.Drawing.Point(147, 69);
            this.spinle2.Name = "spinle2";
            this.spinle2.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.spinle2.Size = new System.Drawing.Size(52, 20);
            this.spinle2.TabIndex = 7;
            // 
            // spinle1
            // 
            this.spinle1.EditValue = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.spinle1.Location = new System.Drawing.Point(147, 43);
            this.spinle1.Name = "spinle1";
            this.spinle1.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.spinle1.Size = new System.Drawing.Size(52, 20);
            this.spinle1.TabIndex = 6;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(6, 70);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(109, 16);
            this.label4.TabIndex = 5;
            this.label4.Text = "Extenso Linha 2";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(32, 44);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(109, 16);
            this.label3.TabIndex = 4;
            this.label3.Text = "Extenso Linha 1";
            // 
            // spincvn
            // 
            this.spincvn.EditValue = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.spincvn.Location = new System.Drawing.Point(397, 40);
            this.spincvn.Name = "spincvn";
            this.spincvn.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.spincvn.Properties.SpinStyle = DevExpress.XtraEditors.Controls.SpinStyles.Horizontal;
            this.spincvn.Size = new System.Drawing.Size(61, 20);
            this.spincvn.TabIndex = 3;
            // 
            // spinlvn
            // 
            this.spinlvn.EditValue = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.spinlvn.Location = new System.Drawing.Point(336, 20);
            this.spinlvn.Name = "spinlvn";
            this.spinlvn.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.spinlvn.Size = new System.Drawing.Size(52, 20);
            this.spinlvn.TabIndex = 2;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(395, 22);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(64, 16);
            this.label2.TabIndex = 1;
            this.label2.Text = "1.000,00";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.Location = new System.Drawing.Point(27, 19);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(52, 16);
            this.label7.TabIndex = 4;
            this.label7.Text = "Banco:";
            // 
            // simpleButton1
            // 
            this.simpleButton1.Location = new System.Drawing.Point(21, 245);
            this.simpleButton1.Name = "simpleButton1";
            this.simpleButton1.Size = new System.Drawing.Size(169, 23);
            this.simpleButton1.TabIndex = 6;
            this.simpleButton1.Text = "Ler configuração atual";
            this.simpleButton1.Click += new System.EventHandler(this.simpleButton1_Click);
            // 
            // simpleButton2
            // 
            this.simpleButton2.Location = new System.Drawing.Point(21, 274);
            this.simpleButton2.Name = "simpleButton2";
            this.simpleButton2.Size = new System.Drawing.Size(169, 23);
            this.simpleButton2.TabIndex = 7;
            this.simpleButton2.Text = "Configurar";
            this.simpleButton2.Click += new System.EventHandler(this.simpleButton2_Click);
            // 
            // spinbanco
            // 
            this.spinbanco.EditValue = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.spinbanco.Location = new System.Drawing.Point(85, 18);
            this.spinbanco.Name = "spinbanco";
            this.spinbanco.Properties.SpinStyle = DevExpress.XtraEditors.Controls.SpinStyles.Horizontal;
            this.spinbanco.Size = new System.Drawing.Size(51, 20);
            this.spinbanco.TabIndex = 10;
            // 
            // spincpr
            // 
            this.spincpr.EditValue = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.spincpr.Location = new System.Drawing.Point(537, 245);
            this.spincpr.Name = "spincpr";
            this.spincpr.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.spincpr.Properties.SpinStyle = DevExpress.XtraEditors.Controls.SpinStyles.Horizontal;
            this.spincpr.Size = new System.Drawing.Size(61, 20);
            this.spincpr.TabIndex = 11;
            // 
            // cConfigCheque
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.spincpr);
            this.Controls.Add(this.spinbanco);
            this.Controls.Add(this.simpleButton2);
            this.Controls.Add(this.simpleButton1);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.panelControl1);
            this.LookAndFeel.UseDefaultLookAndFeel = false;
            this.Name = "cConfigCheque";
            this.Size = new System.Drawing.Size(632, 313);
            ((System.ComponentModel.ISupportInitialize)(this.StyleController_F)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl1)).EndInit();
            this.panelControl1.ResumeLayout(false);
            this.panelControl1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.spincda.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.spinlbe.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.spinlan.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.spincan.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.spince1.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.spinle2.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.spinle1.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.spincvn.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.spinlvn.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.spinbanco.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.spincpr.Properties)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private DevExpress.XtraEditors.PanelControl panelControl1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private DevExpress.XtraEditors.SpinEdit spincvn;
        private DevExpress.XtraEditors.SpinEdit spinlvn;
        private System.Windows.Forms.Label label5;
        private DevExpress.XtraEditors.SpinEdit spinle2;
        private DevExpress.XtraEditors.SpinEdit spinle1;
        private System.Windows.Forms.Label label4;
        private DevExpress.XtraEditors.SpinEdit spince1;
        private DevExpress.XtraEditors.SpinEdit spinlan;
        private DevExpress.XtraEditors.SpinEdit spincan;
        private System.Windows.Forms.Label label6;
        private DevExpress.XtraEditors.SpinEdit spinlbe;
        private System.Windows.Forms.Label label7;
        private DevExpress.XtraEditors.SpinEdit spincda;
        private DevExpress.XtraEditors.SimpleButton simpleButton1;
        private DevExpress.XtraEditors.SimpleButton simpleButton2;
        private DevExpress.XtraEditors.SpinEdit spinbanco;
        private DevExpress.XtraEditors.SpinEdit spincpr;
    }
}
