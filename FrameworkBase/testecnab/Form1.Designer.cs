﻿namespace testecnab
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.button1 = new System.Windows.Forms.Button();
            this.panel1 = new System.Windows.Forms.Panel();
            this.button5 = new System.Windows.Forms.Button();
            this.button4 = new System.Windows.Forms.Button();
            this.textEdit1 = new DevExpress.XtraEditors.TextEdit();
            this.button2 = new System.Windows.Forms.Button();
            this.chSoErro = new DevExpress.XtraEditors.CheckEdit();
            this.cmdDiarioComprovante = new System.Windows.Forms.Button();
            this.cmdDiarioRemessa = new System.Windows.Forms.Button();
            this.cmdDiarioRetorno = new System.Windows.Forms.Button();
            this.cmdPagForRemessa = new System.Windows.Forms.Button();
            this.cmdPagForRetorno = new System.Windows.Forms.Button();
            this.cmdCobrancaRemessa = new System.Windows.Forms.Button();
            this.cmdCobrancaRetorno = new System.Windows.Forms.Button();
            this.button3 = new System.Windows.Forms.Button();
            this.textBox1 = new System.Windows.Forms.TextBox();
            this.openFileDialog1 = new System.Windows.Forms.OpenFileDialog();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit1.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chSoErro.Properties)).BeginInit();
            this.SuspendLayout();
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(12, 12);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(75, 23);
            this.button1.TabIndex = 0;
            this.button1.Text = "Le arquivo";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.button5);
            this.panel1.Controls.Add(this.button4);
            this.panel1.Controls.Add(this.textEdit1);
            this.panel1.Controls.Add(this.button2);
            this.panel1.Controls.Add(this.chSoErro);
            this.panel1.Controls.Add(this.cmdDiarioComprovante);
            this.panel1.Controls.Add(this.cmdDiarioRemessa);
            this.panel1.Controls.Add(this.cmdDiarioRetorno);
            this.panel1.Controls.Add(this.cmdPagForRemessa);
            this.panel1.Controls.Add(this.cmdPagForRetorno);
            this.panel1.Controls.Add(this.cmdCobrancaRemessa);
            this.panel1.Controls.Add(this.cmdCobrancaRetorno);
            this.panel1.Controls.Add(this.button3);
            this.panel1.Controls.Add(this.button1);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel1.Location = new System.Drawing.Point(0, 0);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(1091, 103);
            this.panel1.TabIndex = 1;
            // 
            // button5
            // 
            this.button5.Location = new System.Drawing.Point(329, 14);
            this.button5.Name = "button5";
            this.button5.Size = new System.Drawing.Size(75, 23);
            this.button5.TabIndex = 14;
            this.button5.Text = "Serv Proc";
            this.button5.UseVisualStyleBackColor = true;
            this.button5.Click += new System.EventHandler(this.button5_Click);
            // 
            // button4
            // 
            this.button4.Location = new System.Drawing.Point(846, 12);
            this.button4.Name = "button4";
            this.button4.Size = new System.Drawing.Size(210, 23);
            this.button4.TabIndex = 13;
            this.button4.Text = "Teste Digito";
            this.button4.UseVisualStyleBackColor = true;
            this.button4.Click += new System.EventHandler(this.button4_Click);
            // 
            // textEdit1
            // 
            this.textEdit1.Location = new System.Drawing.Point(210, 43);
            this.textEdit1.Name = "textEdit1";
            this.textEdit1.Size = new System.Drawing.Size(194, 20);
            this.textEdit1.TabIndex = 12;
            // 
            // button2
            // 
            this.button2.Location = new System.Drawing.Point(12, 74);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(392, 23);
            this.button2.TabIndex = 11;
            this.button2.Text = "Teste ABSTRATO";
            this.button2.UseVisualStyleBackColor = true;
            this.button2.Click += new System.EventHandler(this.button2_Click);
            // 
            // chSoErro
            // 
            this.chSoErro.Location = new System.Drawing.Point(93, 16);
            this.chSoErro.Name = "chSoErro";
            this.chSoErro.Properties.Caption = "Detalhar somente com erro";
            this.chSoErro.Size = new System.Drawing.Size(191, 19);
            this.chSoErro.TabIndex = 10;
            // 
            // cmdDiarioComprovante
            // 
            this.cmdDiarioComprovante.Location = new System.Drawing.Point(846, 70);
            this.cmdDiarioComprovante.Name = "cmdDiarioComprovante";
            this.cmdDiarioComprovante.Size = new System.Drawing.Size(210, 23);
            this.cmdDiarioComprovante.TabIndex = 9;
            this.cmdDiarioComprovante.Text = "Gera comprovante modelo (tributo)";
            this.cmdDiarioComprovante.UseVisualStyleBackColor = true;
            this.cmdDiarioComprovante.Click += new System.EventHandler(this.cmdDiarioComprovante_Click);
            // 
            // cmdDiarioRemessa
            // 
            this.cmdDiarioRemessa.Location = new System.Drawing.Point(410, 70);
            this.cmdDiarioRemessa.Name = "cmdDiarioRemessa";
            this.cmdDiarioRemessa.Size = new System.Drawing.Size(211, 23);
            this.cmdDiarioRemessa.TabIndex = 8;
            this.cmdDiarioRemessa.Text = "Grava arquivo remessa diário ( tributos )";
            this.cmdDiarioRemessa.UseVisualStyleBackColor = true;
            this.cmdDiarioRemessa.Click += new System.EventHandler(this.cmdDiarioRemessa_Click);
            // 
            // cmdDiarioRetorno
            // 
            this.cmdDiarioRetorno.Location = new System.Drawing.Point(630, 70);
            this.cmdDiarioRetorno.Name = "cmdDiarioRetorno";
            this.cmdDiarioRetorno.Size = new System.Drawing.Size(210, 23);
            this.cmdDiarioRetorno.TabIndex = 7;
            this.cmdDiarioRetorno.Text = "Le arquivo retorno diário ( tributos )";
            this.cmdDiarioRetorno.UseVisualStyleBackColor = true;
            this.cmdDiarioRetorno.Click += new System.EventHandler(this.cmdDiarioRetorno_Click);
            // 
            // cmdPagForRemessa
            // 
            this.cmdPagForRemessa.Location = new System.Drawing.Point(410, 41);
            this.cmdPagForRemessa.Name = "cmdPagForRemessa";
            this.cmdPagForRemessa.Size = new System.Drawing.Size(211, 23);
            this.cmdPagForRemessa.TabIndex = 6;
            this.cmdPagForRemessa.Text = "Grava arquivo remessa pag-for";
            this.cmdPagForRemessa.UseVisualStyleBackColor = true;
            this.cmdPagForRemessa.Click += new System.EventHandler(this.cmdPagForRemessa_Click);
            // 
            // cmdPagForRetorno
            // 
            this.cmdPagForRetorno.Location = new System.Drawing.Point(630, 41);
            this.cmdPagForRetorno.Name = "cmdPagForRetorno";
            this.cmdPagForRetorno.Size = new System.Drawing.Size(210, 23);
            this.cmdPagForRetorno.TabIndex = 5;
            this.cmdPagForRetorno.Text = "Le arquivo retorno pag-for";
            this.cmdPagForRetorno.UseVisualStyleBackColor = true;
            this.cmdPagForRetorno.Click += new System.EventHandler(this.cmdPagForRetorno_Click);
            // 
            // cmdCobrancaRemessa
            // 
            this.cmdCobrancaRemessa.Location = new System.Drawing.Point(410, 12);
            this.cmdCobrancaRemessa.Name = "cmdCobrancaRemessa";
            this.cmdCobrancaRemessa.Size = new System.Drawing.Size(211, 23);
            this.cmdCobrancaRemessa.TabIndex = 4;
            this.cmdCobrancaRemessa.Text = "Grava arquivo remessa cobrança";
            this.cmdCobrancaRemessa.UseVisualStyleBackColor = true;
            this.cmdCobrancaRemessa.Click += new System.EventHandler(this.cmdCobrancaRemessa_Click_1);
            // 
            // cmdCobrancaRetorno
            // 
            this.cmdCobrancaRetorno.Location = new System.Drawing.Point(630, 12);
            this.cmdCobrancaRetorno.Name = "cmdCobrancaRetorno";
            this.cmdCobrancaRetorno.Size = new System.Drawing.Size(210, 23);
            this.cmdCobrancaRetorno.TabIndex = 3;
            this.cmdCobrancaRetorno.Text = "Le arquivo retorno cobrança";
            this.cmdCobrancaRetorno.UseVisualStyleBackColor = true;
            this.cmdCobrancaRetorno.Click += new System.EventHandler(this.cmdCobrancaRetorno_Click);
            // 
            // button3
            // 
            this.button3.Location = new System.Drawing.Point(12, 41);
            this.button3.Name = "button3";
            this.button3.Size = new System.Drawing.Size(168, 23);
            this.button3.TabIndex = 2;
            this.button3.Text = "Grava arquivo salários caixa";
            this.button3.UseVisualStyleBackColor = true;
            this.button3.Click += new System.EventHandler(this.button3_Click);
            // 
            // textBox1
            // 
            this.textBox1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.textBox1.Font = new System.Drawing.Font("Courier New", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBox1.Location = new System.Drawing.Point(0, 103);
            this.textBox1.Multiline = true;
            this.textBox1.Name = "textBox1";
            this.textBox1.ScrollBars = System.Windows.Forms.ScrollBars.Both;
            this.textBox1.Size = new System.Drawing.Size(1091, 473);
            this.textBox1.TabIndex = 2;
            this.textBox1.WordWrap = false;
            // 
            // openFileDialog1
            // 
            this.openFileDialog1.FileName = "openFileDialog1";
            this.openFileDialog1.Multiselect = true;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1091, 576);
            this.Controls.Add(this.textBox1);
            this.Controls.Add(this.panel1);
            this.Name = "Form1";
            this.Text = "Form1";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.panel1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.textEdit1.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chSoErro.Properties)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.TextBox textBox1;
        private System.Windows.Forms.OpenFileDialog openFileDialog1;
        private System.Windows.Forms.Button button3;
        private System.Windows.Forms.Button cmdCobrancaRemessa;
        private System.Windows.Forms.Button cmdCobrancaRetorno;
        private System.Windows.Forms.Button cmdPagForRemessa;
        private System.Windows.Forms.Button cmdPagForRetorno;
        private System.Windows.Forms.Button cmdDiarioRemessa;
        private System.Windows.Forms.Button cmdDiarioRetorno;
        private System.Windows.Forms.Button cmdDiarioComprovante;
        private DevExpress.XtraEditors.CheckEdit chSoErro;
        private System.Windows.Forms.Button button2;
        private DevExpress.XtraEditors.TextEdit textEdit1;
        private System.Windows.Forms.Button button4;
        private System.Windows.Forms.Button button5;
    }
}

