﻿/*
MR - 28/04/2014 09:45 -           - Envio manual de Digitos de Agencia e Conta no teste de modalidade de transferencia (Alterações indicadas por *** MRC - INICIO - PAG-FOR (28/04/2014 09:45) ***)
MR - 06/11/2014 15:00 -           - Tratamento de arquivo de Remessa e Retorno para pagamento de tributos (Alterações indicadas por *** MRC - INICIO - TRIBUTOS (06/11/2014 15:00) ***)
MR - 10/12/2014 12:00 -           - Formatacao de campos ao gerar Comprovantes para Tributos (Alterações indicadas por *** MRC - INICIO - TRIBUTOS (10/12/2014 12:00) ***)
MR - 25/02/2016 11:00 -           - Arquivo de Remessa Cobrança Itau (Alterações indicadas por *** MRC - INICIO - REMESSA (25/02/2016 11:00) ***)
MR - 21/03/2016 20:00 -           - Arquivo de Retorno Cobrança Itau (Alterações indicadas por *** MRC - INICIO - RETORNO (21/03/2016 20:00) ***)
*/

using System;
using System.Text;
using System.Windows.Forms;
using System.IO;
using dllCNAB;
using EDIBoletos;
using dllCheques;
using dllChequesProc;
using VirEnumeracoes;

namespace testecnab
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            /*
            string ERRO = "";
            try
            {
                ERRO = ERRO.Substring(1);
            }
            catch (Exception ex)
            {
                string teste = VirExceptionProc.VirExceptionProc.RelatorioDeErro(ex,true,true,true);
                MessageBox.Show(teste);
                return;
            }
            */

            StringBuilder SB = new StringBuilder();            
            if (openFileDialog1.ShowDialog() == System.Windows.Forms.DialogResult.OK)
            {
                foreach (string arquivo in openFileDialog1.FileNames)
                {
                    CNAB240 cnab = new CNAB240();
                    if (!cnab.Carrega(arquivo) || !chSoErro.Checked)
                        SB.Append(cnab.Relatorio());
                    else
                        SB.AppendFormat("{0} ok\r\n", arquivo);
                }
                textBox1.Text = SB.ToString();
                Clipboard.SetText(SB.ToString());
            }
        }

        private void button3_Click(object sender, EventArgs e)
        {
            dllCNAB.CaixaSalarioRem CaixaSalarioRem1 = new CaixaSalarioRem();
            CaixaSalarioRem1.EmTeste = true;
            CaixaSalarioRem1.IniciaCabecalho(
                                               new DocBacarios.CPFCNPJ(01124173000184),
                                               1,
                                               2,
                                               3,
                                               4,
                                               5,
                                               6,
                                               "7",
                                               "NomeEmpresa",
                                               8,
                                               "identificador");

            CaixaSalarioRem1.AbreLote(new DocBacarios.CPFCNPJ(13553185805),
                                          1,
                                          2, 3, 4, 5, "A", "Condominio 1", "Rua", 151, "Sorocaba", 1855, 185, "SP");

            CaixaSalarioRem1.incluipagamento(104, 1, 2, 3, "A", "Funcionario1", new DateTime(2012, 12, 1), 100, new DocBacarios.CPFCNPJ(13553185805), "Rua1", 4, "Cidade1", 18055, 185, "SP");

            CaixaSalarioRem1.incluipagamento(104, 1, 2, 3, "A", "Funcionario2", new DateTime(2012, 12, 1), 100.1M, new DocBacarios.CPFCNPJ(13553185805), "Rua1", 4, "Cidade1", 18055, 185, "SP");


            CaixaSalarioRem1.AbreLote(new DocBacarios.CPFCNPJ(13553185805),
                                          1,
                                          2, 3, 4, 5, "A", "Condominio 2", "Rua", 151, "Sorocaba", 1855, 185, "SP");

            CaixaSalarioRem1.incluipagamento(104, 1, 2, 3, "A", "Funcionario1", new DateTime(2012, 12, 1), 100.11M, new DocBacarios.CPFCNPJ(13553185805), "Rua1", 4, "Cidade1", 18055, 185, "SP");

            CaixaSalarioRem1.incluipagamento(104, 1, 2, 3, "A", "Funcionario2", new DateTime(2012, 12, 1), 100, new DocBacarios.CPFCNPJ(13553185805), "Rua1", 4, "Cidade1", 18055, 185, "SP");
            

            //CaixaSalarioRem1.TerminaGerarArquivo();
                

            CaixaSalarioRem1.GravaArquivo(@"c:\temp\testecaixa.txt");
        }

        //*** MRC - INICIO - REMESSA (25/02/2016 11:00) ***
        private enum TipoTeste { bra, itau, cnab_cx, cnab_itau}
        //*** MRC - TEMINO - REMESSA (25/02/2016 11:00) ***

        private void CobrancaRemessaX(TipoTeste Tipo)
        {
            //Cria Arquivo
            BoletoRemessaBase BoletoRemessaX = null;
            switch (Tipo)
            {
                case TipoTeste.bra:
                    BoletoRemessaX = new BoletoRemessaBra();
                    break;
                //*** MRC - INICIO - REMESSA (25/02/2016 11:00) ***
                case TipoTeste.itau:
                    BoletoRemessaX = new BoletoRemessaItau();
                    break;
                //*** MRC - TERMINO - REMESSA (25/02/2016 11:00) ***
                case TipoTeste.cnab_cx:
                    BoletoRemessaX = new BoletoRemessaCNAB_CX();
                    break;
                case TipoTeste.cnab_itau:
                    BoletoRemessaX = new BoletoRemessaCNAB_ITAU();
                    break;
            }

            //Inicia Remessa            
            BoletoRemessaX.IniciaRemessa();

            //*** MRC - INICIO - REMESSA (25/02/2016 11:00) ***
            switch (Tipo)
            {
                case TipoTeste.bra:
                    BoletoRemessaX.CodEmpresa = 4562997;
                    BoletoRemessaX.NomeEmpresa = "Neon Imoveis";
                    BoletoRemessaX.Carteira = "9";
                    break;
                case TipoTeste.itau:
                    BoletoRemessaX.AgenciaMae = 17;
                    BoletoRemessaX.ContaMae = 30671;
                    BoletoRemessaX.ContaMaeDg = "3";
                    BoletoRemessaX.NomeEmpresa = "Tiradentes Bl.08 Ed. Esmeralda";
                    BoletoRemessaX.Carteira = "109";
                    break;
            }
            BoletoRemessaX.SequencialRemessa = 1;
            BoletoRemessaX.SequencialRemessaArquivo = 1;
            //*** MRC - TEMINO - REMESSA (25/02/2016 11:00) ***

            //Teste especial Bra
            BoletoReg boleto1 = new BoletoReg();
            boleto1.NomeEmpresa = "Empresa";
            boleto1.CNPJ = new DocBacarios.CPFCNPJ(01124173000184);
            boleto1.DARazaoConta = 123;
            boleto1.Agencia = "0109";
            boleto1.Conta = "255718";
            boleto1.DigConta = "5";
            boleto1.NossoNumero = 21507701;
            boleto1.EmissaoBoleto = Convert.ToInt32(BoletoRemessaBra.TiposEmissaoBoleto.Cliente);
            boleto1.IDOcorrencia = Convert.ToInt32(BoletoRemessaBra.TiposOcorrencia.Remessa);
            boleto1.NumDocumento = "21507701";
            boleto1.Vencimento = new DateTime(2013, 5, 21);
            boleto1.Valor = 300.00M;
            boleto1.EspecieTitulo = Convert.ToInt32(BoletoRemessaBra.TiposEspecieTitulo.Outros);
            boleto1.Emissao = new DateTime(2013, 3, 19);
            boleto1.Instrucao1 = Convert.ToInt32(BoletoRemessaBra.TiposInstrucao.BaixaDecursoPrazo);
            boleto1.Instrucao2 = 30;
            boleto1.NomeSacado = "Márcia 3º D´a";
            boleto1.EnderecoSacado = "1<>(){}|fim";
            boleto1.CEPSacado = "04016-001";
            boleto1.CPF = new DocBacarios.CPFCNPJ(13553185805);
            boleto1.CobrarMulta = true;
            boleto1.PercMulta = 2;
            boleto1.RateioCreditos.Add(new RateioCredito(109, 0, 255718, "5", 2.01M, "NEON IMOVEIS"));
            boleto1.RateioCreditos.Add(new RateioCredito(109, 0, 255718, "5", 2.02M, "NEON IMOVEIS"));
            boleto1.RateioCreditos.Add(new RateioCredito(109, 0, 255718, "5", 2.03M, "NEON IMOVEIS"));
            boleto1.RateioCreditos.Add(new RateioCredito(109, 0, 255718, "5", 2.04M, "NEON IMOVEIS"));
            BoletoRemessaX.Boletos.Add(boleto1);
            /*
            //Teste especial Bra
            BoletoReg boleto2 = new BoletoReg();
            boleto2.NomeEmpresa = "Empresa";
            boleto2.CNPJ = new DocBacarios.CPFCNPJ(01124173000184);
            boleto2.DARazaoConta = 123;
            boleto2.Agencia = "0109";
            boleto2.Conta = "255718";
            boleto2.DigConta = "5";
            boleto2.NossoNumero = 21502992;
            boleto2.EmissaoBoleto = Convert.ToInt32(BoletoRemessaBra.TiposEmissaoBoleto.Cliente);
            boleto2.IDOcorrencia = Convert.ToInt32(BoletoRemessaBra.TiposOcorrencia.AlteracaoVencimento);
            boleto2.NumDocumento = "C 02 2013";
            boleto2.Vencimento = new DateTime(2013, 11, 30);
            boleto2.Valor = 400.04M;
            boleto2.EspecieTitulo = Convert.ToInt32(BoletoRemessaBra.TiposEspecieTitulo.Outros);
            boleto2.Emissao = new DateTime(2013, 11, 01);
            BoletoRemessaX.Boletos.Add(boleto2);

            //Teste especial Bra
            BoletoReg boleto3 = new BoletoReg();
            boleto3.NomeEmpresa = "Empresa";
            boleto3.CNPJ = new DocBacarios.CPFCNPJ(13553185805);
            boleto3.DARazaoConta = 123;
            boleto3.Agencia = "0109";
            boleto3.Conta = "255718";
            boleto3.DigConta = "5";
            boleto3.NossoNumero = 21223441;
            boleto3.EmissaoBoleto = Convert.ToInt32(BoletoRemessaBra.TiposEmissaoBoleto.Cliente);
            boleto3.IDOcorrencia = Convert.ToInt32(BoletoRemessaBra.TiposOcorrencia.AlteracaoOutrosDados);
            boleto3.NumDocumento = "C 03 2013";
            boleto3.Vencimento = new DateTime(2013, 11, 24);
            boleto3.Valor = 2000.00M;
            boleto3.EspecieTitulo = Convert.ToInt32(BoletoRemessaBra.TiposEspecieTitulo.Outros);
            boleto3.Emissao = new DateTime(2013, 11, 01);
            BoletoRemessaX.Boletos.Add(boleto3);

            //Teste especial Itau
            BoletoReg boleto4 = new BoletoReg();
            boleto4.NomeEmpresa = "V-Commerce";
            boleto4.CNPJ = new DocBacarios.CPFCNPJ(04518919000122);
            boleto4.Agencia = "0081";
            boleto4.Conta = "62603";
            boleto4.DigConta = "8";
            //boleto4.ControlePart = "061273";
            boleto4.NossoNumero = 212973;
            //boleto4.IDOcorrencia = Convert.ToInt32(BoletoRemessaItau.TiposOcorrencia.AlteracaoVencimento);
            boleto4.NumDocumento = "732921";
            boleto4.Vencimento = new DateTime(2016, 3, 10);
            boleto4.Valor = 500.54M;
            //boleto4.EspecieTitulo = Convert.ToInt32(BoletoRemessaItau.TiposEspecieTitulo.Duplicata);
            boleto4.Emissao = new DateTime(2016, 3, 1);
            //boleto4.Mora = 400.43M;
            //boleto4.LimDesconto = new DateTime(2016, 3, 5);
            //boleto4.Desconto = 300.32M;
            //boleto4.IOF = 200.21M;
            //boleto4.Abatimento = 100.10M;
            boleto4.CPF = new DocBacarios.CPFCNPJ(17267401880);
            boleto4.NomeSacado = "Márcia Rodino";
            boleto4.EnderecoSacado = "Rua França Pinto 246 c8";
            //boleto4.BairroSacado = "Vl Mariana";
            boleto4.CEPSacado = "04016-001";
            //boleto4.CidadeSacado = "São Paulo";
            //boleto4.EstadoSacado = "SP";
            //boleto4.Mensagem1 = "Msg 1";
            BoletoRemessaX.Boletos.Add(boleto4);
            */
            //Grava arquivo
            string path = "";
            switch (Tipo)
            {
                case TipoTeste.bra:
                    path = string.Format(@"C:\Temp\Neon\ArquivosBradesco\");
                    break;
                //*** MRC - INICIO - REMESSA (25/02/2016 11:00) ***
                case TipoTeste.itau:
                    path = string.Format(@"C:\Temp\Neon\ArquivosItau\");
                    break;
                //*** MRC - TERMINO - REMESSA (25/02/2016 11:00) ***
                case TipoTeste.cnab_cx:
                    path = string.Format(@"C:\Temp\Neon\ArquivosCNAB_CX\");
                    break;
                case TipoTeste.cnab_itau:
                    path = string.Format(@"C:\Temp\Neon\ArquivosCNAB_ITAU\");
                    break;
            }
            string strNomeArquivo = BoletoRemessaX.GravaArquivo(path);
            if (strNomeArquivo == "")
            {
                Exception Ex = BoletoRemessaX.UltimoErro;
                textBox1.Text += "\r\n\r\n*** E R R O ***\r\n\r\n";
                while (Ex != null)
                {
                    textBox1.Text += string.Format("Erro ao gerar arquivo: {0}\r\n", Ex.Message);
                    textBox1.Text += string.Format("{0}\r\n\r\n", Ex.StackTrace);
                    Ex = Ex.InnerException;
                }
            }
            else
            { 
                textBox1.Text += string.Format("\r\nArquivo Remessa gerado em: {0}",  strNomeArquivo); 
            }
        }

        private void cmdCobrancaRemessa_Click_1(object sender, EventArgs e)
        {
            textBox1.Text = "Gerando arquivos de teste:\r\n\r\n";
            CobrancaRemessaX(TipoTeste.bra);
            //*** MRC - INICIO - REMESSA (25/02/2016 11:00) ***
            //CobrancaRemessaX(TipoTeste.itau);
            //*** MRC - TERMNIO - REMESSA (25/02/2016 11:00) ***
            //CobrancaRemessaX(TipoTeste.cnab_cx);
            //CobrancaRemessaX(TipoTeste.cnab_itau);           
        }

        private void cmdCobrancaRetorno_Click(object sender, EventArgs e)
        {
           //Seleciona arquivo de retorno
           textBox1.Text = "";
           if (openFileDialog1.ShowDialog() == System.Windows.Forms.DialogResult.OK)
           {
              foreach (string arquivo in openFileDialog1.FileNames)
              {
                 //*** MRC - INICIO - RETORNO (21/03/2016 20:00) ***                
                 //Inicia Retorno
                 BoletoRetorno BoletoRetorno;
                 switch ((Path.GetFileName(arquivo).Substring(0,2)))
                 {
                     case "CB":
                         BoletoRetorno = new BoletoRetornoBra();
                         break;
                     case "CN":
                         BoletoRetorno = new BoletoRetornoItau();
                         break;
                     default:
                         BoletoRetorno = new BoletoRetornoBra();
                         break;
                 }

                 //Carrega Arquivo
                 BoletoRetorno.Carrega(arquivo);

                 //Gera Relatorio
                 textBox1.Text += BoletoRetorno.Relatorio();
                 //*** MRC - TERMINO - RETORNO (21/03/2016 20:00) ***     
              }

              //Exibe relatorio
              Clipboard.SetText(textBox1.Text);
           }
        }
        
        //*** MRC - INICIO - PAG-FOR (2) ***
        private void cmdPagForRemessa_Click(object sender, EventArgs e)
        {
           //Inicia Remessa
           ChequeRemessaBra ChequeRemessaBra = new ChequeRemessaBra();
           ChequeRemessaBra.IniciaRemessa();

           //Seta propriedades remessa
           ChequeRemessaBra.CodComunicacao = 121728;
           ChequeRemessaBra.ContaDebito = 255718;
           ChequeRemessaBra.NomeEmpresa = "Neon Imoveis";
           ChequeRemessaBra.CPFCNPJ = new DocBacarios.CPFCNPJ("55.055.651/0001-70");
           ChequeRemessaBra.SequencialRemessa = 14;
           ChequeRemessaBra.SequencialRemessaArquivo = 1;
           dCheque.CHEquesRow rowCheque = null;

           ////**** TESTE 1 (Remessa 7) - Seta e insere cheque (modalidade 1)
           ////ChequeEletronico cheque1 = new ChequeEletronico();
           //Cheque cheque1 = new Cheque(rowCheque);
           //cheque1.ModalidadePagamento = Convert.ToInt32(ChequeRemessaBra.TiposModalidadePagamento.CreditoContaCorrentePoupanca);
           //cheque1.TipoMovimento = Convert.ToInt32(ChequeRemessaBra.TiposMovimento.Inclusao);
           //cheque1.CodMovimento = Convert.ToInt32(ChequeRemessaBra.TiposCodigoMovimento.DesautorizaAgendamento);
           //cheque1.SituacaoAgendamento = Convert.ToInt32(ChequeRemessaBra.TiposSituacaoAgendamento.NaoPago);
           //cheque1.NumeroPagamento = "0000000000000117";
           //cheque1.CPFCNPJ = new DocBacarios.CPFCNPJ("349.775.568-01");
           //cheque1.Nome = "Iris Peres Mendes";
           ////cheque1.Endereco = "Rua Franca Pinto, 246 - C. 08";
           ////cheque1.CEP = "04016-001";
           //cheque1.TipoConta = Convert.ToInt32(ChequeRemessaBra.TiposContaFornecedor.ContaCorrente);
           //cheque1.Agencia = 1755;
           //cheque1.Conta = 308820;
           //cheque1.DataVencimento = new DateTime(2014, 03, 27);
           //cheque1.Valor = 10.07M;
           //cheque1.TipoDocumento = Convert.ToInt32(ChequeRemessaBra.TiposDocumento.NotaFiscalFatura);
           //cheque1.NumDocumento = 123;
           //ChequeRemessaBra.Cheques.Add(cheque1); 

           ////**** TESTE 1 (Remessa 9) - Seta e insere cheque (modalidade 1)
           ////ChequeEletronico cheque1 = new ChequeEletronico();
           //Cheque cheque1 = new Cheque(rowCheque);
           //cheque1.ModalidadePagamento = Convert.ToInt32(ChequeRemessaBra.TiposModalidadePagamento.CreditoContaCorrentePoupanca);
           //cheque1.TipoMovimento = Convert.ToInt32(ChequeRemessaBra.TiposMovimento.Inclusao);
           //cheque1.CodMovimento = Convert.ToInt32(ChequeRemessaBra.TiposCodigoMovimento.DesautorizaAgendamento);
           //cheque1.SituacaoAgendamento = Convert.ToInt32(ChequeRemessaBra.TiposSituacaoAgendamento.NaoPago);
           //cheque1.NumeroPagamento = 119;
           //cheque1.CPFCNPJ = new DocBacarios.CPFCNPJ("349.775.568-01");
           //cheque1.Nome = "Iris Peres Mendes";
           ////cheque1.Endereco = "Rua Franca Pinto, 246 - C. 08";
           ////cheque1.CEP = "04016-001";
           //cheque1.TipoConta = Convert.ToInt32(ChequeRemessaBra.TiposContaFornecedor.ContaCorrente);
           //cheque1.Agencia = 1755;
           //cheque1.Conta = 308820;
           //cheque1.DataVencimento = new DateTime(2014, 03, 13);
           //cheque1.Valor = 10.09M;
           //cheque1.TipoDocumento = Convert.ToInt32(ChequeRemessaBra.TiposDocumento.NotaFiscalFatura);
           //cheque1.NumDocumento = 123;
           //ChequeRemessaBra.Cheques.Add(cheque1); 

           //**** TESTE 1 (Remessa 14) - Seta e insere cheque (modalidade 1)
           //ChequeEletronico cheque1 = new ChequeEletronico();
           Cheque cheque1 = new Cheque(rowCheque);
           cheque1.ModalidadePagamento = Convert.ToInt32(ChequeRemessaBra.TiposModalidadePagamento.CreditoContaCorrentePoupanca);
           cheque1.TipoMovimento = Convert.ToInt32(ChequeRemessaBra.TiposMovimento.Inclusao);
           cheque1.CodMovimento = Convert.ToInt32(ChequeRemessaBra.TiposCodigoMovimento.DesautorizaAgendamento);
           cheque1.SituacaoAgendamento = Convert.ToInt32(ChequeRemessaBra.TiposSituacaoAgendamento.NaoPago);
           cheque1.NumeroPagamento = 214;
           cheque1.CPFCNPJ_Credito = new DocBacarios.CPFCNPJ("349.775.568-01");
           cheque1.Nome = "Iris Peres Mendes 3º (3ª)";
           cheque1.TipoConta_Credito = Convert.ToInt32(ChequeRemessaBra.TiposContaFornecedor.ContaCorrente);
           //*** MRC - INICIO - PAG-FOR (28/04/2014 09:45) ***
           cheque1.Agencia_Credito = 1755;
           cheque1.DigAgencia_Credito = "8";
           cheque1.Conta_Credito = 308820;
           cheque1.DigConta_Credito = "0";
           //*** MRC - TERMINO - PAG-FOR (28/04/2014 09:45) ***
           cheque1.DataVencimento = new DateTime(2014, 04, 09);
           cheque1.Valor = 10.14M;
           cheque1.UsoEmpresa = "NUM PAGTO 214";
           ChequeRemessaBra.Cheques.Add(cheque1);

           ////Seta e insere cheque (modalidade 2)
           ////ChequeEletronico cheque2 = new ChequeEletronico();
           //Cheque cheque2 = new Cheque(rowCheque);
           //cheque2.ModalidadePagamento = Convert.ToInt32(ChequeRemessaBra.TiposModalidadePagamento.ChequeOrdemPagamento);
           //cheque2.TipoMovimento = Convert.ToInt32(ChequeRemessaBra.TiposMovimento.Inclusao);
           //cheque2.CodMovimento = Convert.ToInt32(ChequeRemessaBra.TiposCodigoMovimento.DesautorizaAgendamento);
           //cheque2.SituacaoAgendamento = Convert.ToInt32(ChequeRemessaBra.TiposSituacaoAgendamento.NaoPago);
           //cheque2.NumeroPagamento = "102";
           //cheque2.CPFCNPJ = new DocBacarios.CPFCNPJ("172.674.018-80");
           //cheque2.Nome = "Marcia Rodino Cerqueira";
           //cheque2.Endereco = "Rua Franca Pinto, 246 - C. 08";
           //cheque2.CEP = "04016-001";
           //cheque2.InstrucaoChequeOP = "Instrucao para liberacao do cheque op";
           //cheque2.Agencia = 2981;
           //cheque2.DataVencimento = new DateTime(2013, 11, 12);
           //cheque2.Valor = 550.00M;
           //ChequeRemessaBra.Cheques.Add(cheque2); 

           //**** TESTE 2 (Remessa 8) - Seta e insere cheque (modalidade 3)
           ////ChequeEletronico cheque3 = new ChequeEletronico();
           //Cheque cheque3 = new Cheque(rowCheque);
           //cheque3.ModalidadePagamento = Convert.ToInt32(ChequeRemessaBra.TiposModalidadePagamento.DOCCompe);
           //cheque3.TipoMovimento = Convert.ToInt32(ChequeRemessaBra.TiposMovimento.Inclusao);
           //cheque3.CodMovimento = Convert.ToInt32(ChequeRemessaBra.TiposCodigoMovimento.DesautorizaAgendamento);
           //cheque3.SituacaoAgendamento = Convert.ToInt32(ChequeRemessaBra.TiposSituacaoAgendamento.NaoPago);
           //cheque3.NumeroPagamento = 118;
           //cheque3.CPFCNPJ = new DocBacarios.CPFCNPJ("55.055.651/0001-70");
           //cheque3.Nome = "Neon Imoveis";
           ////cheque3.FinalidadeDOCTED = 2; //** Criar Tipo Enumerado (lista página 30) **
           ////cheque3.TipoContaDOCTED = 11; //** Criar Tipo Enumerado (lista página 32) **
           //cheque3.Banco = 341;
           //cheque3.Agencia = 8813;
           //cheque3.DigAgencia = "0";
           //cheque3.Conta = 01667;
           //cheque3.DigConta = "5";
           //cheque3.DataVencimento = new DateTime(2014, 03, 28);
           //cheque3.Valor = 10.08M;
           //cheque3.TipoDocumento = Convert.ToInt32(ChequeRemessaBra.TiposDocumento.Fatura);
           //ChequeRemessaBra.Cheques.Add(cheque3); 

           ////**** TESTE 2 (Remessa 12) - Seta e insere cheque (modalidade 3)
           ////ChequeEletronico cheque3 = new ChequeEletronico();
           //Cheque cheque3 = new Cheque(rowCheque);
           //cheque3.ModalidadePagamento = Convert.ToInt32(ChequeRemessaBra.TiposModalidadePagamento.DOCCompe);
           //cheque3.TipoMovimento = Convert.ToInt32(ChequeRemessaBra.TiposMovimento.Inclusao);
           //cheque3.CodMovimento = Convert.ToInt32(ChequeRemessaBra.TiposCodigoMovimento.DesautorizaAgendamento);
           //cheque3.SituacaoAgendamento = Convert.ToInt32(ChequeRemessaBra.TiposSituacaoAgendamento.NaoPago);
           //cheque3.NumeroPagamento = 212;
           //cheque3.CPFCNPJ = new DocBacarios.CPFCNPJ("55.055.651/0001-70");
           //cheque3.Nome = "Neon Imoveis";
           ////cheque3.FinalidadeDOCTED = 2; //** Criar Tipo Enumerado (lista página 30) **
           ////cheque3.TipoContaDOCTED = 11; //** Criar Tipo Enumerado (lista página 32) **
           //cheque3.Banco = 341;
           //cheque3.Agencia = 8813;
           //cheque3.DigAgencia = "0";
           //cheque3.Conta = 01667;
           //cheque3.DigConta = "5";
           //cheque3.DataVencimento = new DateTime(2014, 03, 14);
           //cheque3.Valor = 7.00M;
           //cheque3.TipoDocumento = Convert.ToInt32(ChequeRemessaBra.TiposDocumento.Fatura);
           //ChequeRemessaBra.Cheques.Add(cheque3); 

           ////Seta e insere cheque (modalidade 5)
           ////ChequeEletronico cheque4 = new ChequeEletronico();
           //Cheque cheque4 = new Cheque(rowCheque);
           //cheque4.ModalidadePagamento = Convert.ToInt32(ChequeRemessaBra.TiposModalidadePagamento.CreditoContaCorrenteRealTime);
           //cheque4.TipoMovimento = Convert.ToInt32(ChequeRemessaBra.TiposMovimento.Inclusao);
           //cheque4.CodMovimento = Convert.ToInt32(ChequeRemessaBra.TiposCodigoMovimento.DesautorizaAgendamento);
           //cheque4.SituacaoAgendamento = Convert.ToInt32(ChequeRemessaBra.TiposSituacaoAgendamento.NaoPago);
           //cheque4.NumeroPagamento = "104";
           //cheque4.CPFCNPJ = new DocBacarios.CPFCNPJ("172.674.018-80");
           //cheque4.Nome = "Marcia Rodino Cerqueira";
           //cheque4.TipoConta = Convert.ToInt32(ChequeRemessaBra.TiposContaFornecedor.ContaPoupanca);
           //cheque4.Agencia = 2374;
           //cheque4.Conta = 63002;
           //cheque4.DataVencimento = new DateTime(2013, 11, 14);
           //cheque4.Valor = 100.00M;
           //cheque4.TipoDocumento = Convert.ToInt32(ChequeRemessaBra.TiposDocumento.NotaFiscal);
           //cheque4.NumDocumento = 456;
           //cheque4.SerieDocumento = "31";
           //ChequeRemessaBra.Cheques.Add(cheque4); 

           ////Seta e insere cheque (modalidade 8)
           ////ChequeEletronico cheque5 = new ChequeEletronico();
           //Cheque cheque5 = new Cheque(rowCheque);
           //cheque5.ModalidadePagamento = Convert.ToInt32(ChequeRemessaBra.TiposModalidadePagamento.TED);
           //cheque5.TipoMovimento = Convert.ToInt32(ChequeRemessaBra.TiposMovimento.Inclusao);
           //cheque5.CodMovimento = Convert.ToInt32(ChequeRemessaBra.TiposCodigoMovimento.DesautorizaAgendamento);
           //cheque5.SituacaoAgendamento = Convert.ToInt32(ChequeRemessaBra.TiposSituacaoAgendamento.NaoPago);
           //cheque5.NumeroPagamento = "105";
           //cheque5.CPFCNPJ = new DocBacarios.CPFCNPJ("172.674.018-80");
           //cheque5.Nome = "Marcia Rodino Cerqueira";
           //cheque5.Banco = 33;
           //cheque5.Agencia = 3412;
           //cheque5.DigAgencia = "3";
           //cheque5.Conta = 1000310;
           //cheque5.DigConta = "9";
           //cheque5.DataVencimento = new DateTime(2013, 11, 15);
           //cheque5.Valor = 1020.12M;
           //cheque5.Acrescimo = 0.03M;
           //cheque5.TipoDocumento = Convert.ToInt32(ChequeRemessaBra.TiposDocumento.Duplicata);
           //ChequeRemessaBra.Cheques.Add(cheque5); 

           ////**** TESTE 3 (Remessa 11) - Seta e insere cheque (modalidade 31 - demais bancos - Linha Digitavel Ex = 34191.75009 05410.940174 64769.430006 2 57350000010000)
           ////ChequeEletronico cheque6 = new ChequeEletronico();
           Cheque cheque6 = new Cheque(rowCheque);
           cheque6.ModalidadePagamento = Convert.ToInt32(ChequeRemessaBra.TiposModalidadePagamento.TitulosTerceiros);
           cheque6.TipoMovimento = Convert.ToInt32(ChequeRemessaBra.TiposMovimento.Inclusao);
           cheque6.CodMovimento = Convert.ToInt32(ChequeRemessaBra.TiposCodigoMovimento.DesautorizaAgendamento);
           cheque6.SituacaoAgendamento = Convert.ToInt32(ChequeRemessaBra.TiposSituacaoAgendamento.NaoPago);
           cheque6.NumeroPagamento = 211;
           //cheque6.CPFCNPJ = new DocBacarios.CPFCNPJ("04.518.919/0001-22");
           cheque6.Nome = "SBC Center Com Mateirais P/Construca";
           cheque6.LinhaDigitavel = "34191.75215 93062.638817 30166.750007 5 60020000000600";
           //cheque6.TipoDocumento = Convert.ToInt32(ChequeRemessaBra.TiposDocumento.Outros);
           ChequeRemessaBra.Cheques.Add(cheque6);

           //**** TESTE 4 (Remessa 10) - Seta e insere cheque (modalidade 31 - banco 237 - Linha Digitavel Ex = 23792.37403 90099.242837 61006.300208 5 56990000106389)
           ////ChequeEletronico cheque7 = new ChequeEletronico();
           Cheque cheque7 = new Cheque(rowCheque);
           cheque7.ModalidadePagamento = Convert.ToInt32(ChequeRemessaBra.TiposModalidadePagamento.TitulosTerceiros);
           cheque7.TipoMovimento = Convert.ToInt32(ChequeRemessaBra.TiposMovimento.Inclusao);
           cheque7.CodMovimento = Convert.ToInt32(ChequeRemessaBra.TiposCodigoMovimento.DesautorizaAgendamento);
           cheque7.SituacaoAgendamento = Convert.ToInt32(ChequeRemessaBra.TiposSituacaoAgendamento.NaoPago);
           cheque7.NumeroPagamento = 210;
           //cheque7.CPFCNPJ = new DocBacarios.CPFCNPJ("48.041.735/0001-90");
           cheque7.Nome = "Residencial California";
           cheque7.LinhaDigitavel = "23790.10909 90002.193069 25025.571800 2 60020000000500";
           //cheque7.Desconto = 0.89M;
           ChequeRemessaBra.Cheques.Add(cheque7); 

           //Seta e insere cheque (modalidade 1 - exclusao)
           ////ChequeEletronico cheque8 = new ChequeEletronico();
           //Cheque cheque8 = new Cheque(rowCheque);
           //cheque8.ModalidadePagamento = Convert.ToInt32(ChequeRemessaBra.TiposModalidadePagamento.CreditoContaCorrentePoupanca);
           //cheque8.TipoMovimento = Convert.ToInt32(ChequeRemessaBra.TiposMovimento.Exclusao);
           //cheque8.SituacaoAgendamento = Convert.ToInt32(ChequeRemessaBra.TiposSituacaoAgendamento.NaoPago);
           //cheque8.NumeroPagamento = "109";
           //cheque8.CPFCNPJ = new DocBacarios.CPFCNPJ("172.674.018-80");
           //cheque8.Nome = "Marcia Rodino Cerqueira";
           //cheque8.Endereco = "Rua Franca Pinto, 246 - C. 08";
           //cheque8.CEP = "04016-001"; 
           //cheque8.Agencia = 2981;
           //cheque8.Conta = 4053;
           //cheque8.DataVencimento = new DateTime(2013, 11, 11);
           //cheque8.Valor = 123.40M;
           //cheque8.TipoConta = Convert.ToInt32(ChequeRemessaBra.TiposContaFornecedor.ContaCorrente);
           //ChequeRemessaBra.Cheques.Add(cheque8);

           //Grava arquivo
           string strNomeArquivo = ChequeRemessaBra.GravaArquivo(@"C:\Temp\Neon\ArquivosBradesco\");
           if (strNomeArquivo == "")
           { textBox1.Text = "Erro ao gerar arquivo: " + ChequeRemessaBra.UltimoErro.InnerException.Message; }
           else
           { textBox1.Text = "Arquivo Remessa gerado em: " + strNomeArquivo; }
        }

        private void cmdPagForRetorno_Click(object sender, EventArgs e)
        {
           //Seleciona arquivo de retorno
           textBox1.Text = "";
           if (openFileDialog1.ShowDialog() == System.Windows.Forms.DialogResult.OK)
           {
              foreach (string arquivo in openFileDialog1.FileNames)
              {
                 //Inicia Retorno
                 ChequeRetornoBra ChequeRetornoBra = new ChequeRetornoBra();

                 //Carrega Arquivo
                 ChequeRetornoBra.Carrega(arquivo);

                 //Gera Relatorio
                 textBox1.Text += ChequeRetornoBra.Relatorio();
              }

              //Exibe relatorio
              Clipboard.SetText(textBox1.Text);
           }
        }
        //*** MRC - FIM - PAG-FOR (2) ***

        //*** MRC - INICIO - TRIBUTOS (06/11/2014 15:00) ***
        private void cmdDiarioRemessa_Click(object sender, EventArgs e)
        {
            //Inicia Remessa
            TributoRemessaBra TributoRemessaBra = new TributoRemessaBra();
            TributoRemessaBra.IniciaRemessa();
            TributoRemessaBra.SequencialRemessaArquivo = 2;
            string strNomeArquivo = "";

            ////Seta propriedades remessa 1
            //TributoRemessaBra.CodComunicacao = 117045;
            //TributoRemessaBra.CPFCNPJ = new DocBacarios.CPFCNPJ("55.061.758/0001-21");
            //TributoRemessaBra.NomeEmpresa = "Cond Ed Vila Carolina";
            //TributoRemessaBra.SequencialRemessa = 1504;
            //TributoRemessaBra.Tributos.Clear();

            ////**** Vila Carolina - DARF 0561 (Sequencial 1500)
            //TributoEletronico tributo1 = new TributoEletronico();
            //tributo1.TipoTributo = Convert.ToInt32(TributoRemessaBra.TiposTipoTributo.DARF);
            //tributo1.TipoMovimento = "I";
            //tributo1.CPFCNPJContribuinte = new DocBacarios.CPFCNPJ("55.061.758/0001-21");
            //tributo1.NomeContribuinte = "Cond Ed Vila Carolina";
            //tributo1.DAAgencia = 109;
            //tributo1.DADigAgencia = "0";
            //tributo1.DANumConta = 264747;
            //tributo1.DADigNumConta = "8";
            //tributo1.DataDebito = new DateTime(2014, 10, 14);
            //tributo1.Valor = 196.74M;
            //tributo1.DataVencimento = new DateTime(2014, 10, 20);
            //tributo1.CodReceita = 561;
            //tributo1.PeriodoApuracao = new DateTime(2014, 09, 30);
            //tributo1.UsoEmpresa = "6001";
            //TributoRemessaBra.Tributos.Add(tributo1);

            ////**** Vila Carolina - DARF 8301 (Sequencial 1500)
            //TributoEletronico tributo2 = new TributoEletronico();
            //tributo2.TipoTributo = Convert.ToInt32(TributoRemessaBra.TiposTipoTributo.DARF);
            //tributo2.TipoMovimento = "I";
            //tributo2.CPFCNPJContribuinte = new DocBacarios.CPFCNPJ("55.061.758/0001-21");
            //tributo2.NomeContribuinte = "Cond Ed Vila Carolina";
            //tributo2.DAAgencia = 109;
            //tributo2.DADigAgencia = "0";
            //tributo2.DANumConta = 264747;
            //tributo2.DADigNumConta = "8";
            //tributo2.DataDebito = new DateTime(2014, 10, 14);
            //tributo2.Valor = 119.40M;
            //tributo2.DataVencimento = new DateTime(2014, 10, 24);
            //tributo2.CodReceita = 8301;
            //tributo2.PeriodoApuracao = new DateTime(2014, 09, 30);
            //tributo2.UsoEmpresa = "6002";
            //TributoRemessaBra.Tributos.Add(tributo2);

            ////**** Vila Carolina - GPS (Sequencial 1502)
            //TributoEletronico tributo3 = new TributoEletronico();
            //tributo3.TipoTributo = Convert.ToInt32(TributoRemessaBra.TiposTipoTributo.GPS);
            //tributo3.TipoMovimento = "I";
            //tributo3.CPFCNPJContribuinte = new DocBacarios.CPFCNPJ("55.061.758/0001-21");
            //tributo3.NomeContribuinte = "Cond Ed Vila Carolina";
            //tributo3.DAAgencia = 109;
            //tributo3.DADigAgencia = "0";
            //tributo3.DANumConta = 264747;
            //tributo3.DADigNumConta = "8";
            //tributo3.DataDebito = new DateTime(2014, 10, 17);
            //tributo3.Valor = 3963.73M;
            //tributo3.OutrasEntidades = 537.33M;
            //tributo3.DataVencimento = new DateTime(2014, 10, 20);
            //tributo3.CodINSS = 2100;
            //tributo3.CompetenciaAno = 2014;
            //tributo3.CompetenciaMes = 9;
            //tributo3.UsoEmpresa = "6005";
            //TributoRemessaBra.Tributos.Add(tributo3);

            ////**** Vila Carolina - FGTS (Sequencial 1504)
            //TributoEletronico tributo3 = new TributoEletronico();
            //tributo3.TipoTributo = Convert.ToInt32(TributoRemessaBra.TiposTipoTributo.CodigoBarras);
            //tributo3.TipoMovimento = "I";
            //tributo3.CPFCNPJContribuinte = new DocBacarios.CPFCNPJ("55.061.758/0001-21");
            //tributo3.NomeContribuinte = "Cond Ed Vila Carolina";
            //tributo3.DAAgencia = 109;
            //tributo3.DADigAgencia = "0";
            //tributo3.DANumConta = 264747;
            //tributo3.DADigNumConta = "8";
            //tributo3.DataDebito = new DateTime(2014, 10, 28);
            //tributo3.CodigoBarra = "858800000091879101791416107574050853506175800015";
            //tributo3.Valor = 987.91M;
            //tributo3.DataVencimento = new DateTime(2014, 11, 07);
            //tributo3.UsoEmpresa = "6007";
            //TributoRemessaBra.Tributos.Add(tributo3);

            ////Grava arquivo
            //strNomeArquivo = TributoRemessaBra.GravaArquivo(@"C:\Temp\Neon\ArquivosBradesco\", strNomeArquivo);
            //if (strNomeArquivo == "")
            //{
            //    textBox1.Text = "Erro ao gerar arquivo: " + TributoRemessaBra.UltimoErro.Message;
            //    return;
            //}

            //Seta propriedades remessa 2
            TributoRemessaBra.CodComunicacao = 117045;
            TributoRemessaBra.CPFCNPJ = new DocBacarios.CPFCNPJ("50.940.386/0001-90");
            TributoRemessaBra.NomeEmpresa = "Cond Ed Grasiela";
            TributoRemessaBra.SequencialRemessa = 1505;
            TributoRemessaBra.Tributos.Clear();

            ////**** Grasiela - DARF 0561 (Sequencial 1501)
            //TributoEletronico tributo4 = new TributoEletronico();
            //tributo4.TipoTributo = Convert.ToInt32(TributoRemessaBra.TiposTipoTributo.DARF);
            //tributo4.TipoMovimento = "I";
            //tributo4.CPFCNPJContribuinte = new DocBacarios.CPFCNPJ("50.940.386/0001-90");
            //tributo4.NomeContribuinte = "Cond Ed Grasiela";
            //tributo4.DAAgencia = 109;
            //tributo4.DADigAgencia = "0";
            //tributo4.DANumConta = 258238;
            //tributo4.DADigNumConta = "4";
            //tributo4.DataDebito = new DateTime(2014, 10, 16);
            //tributo4.Valor = 27.65M;
            //tributo4.DataVencimento = new DateTime(2014, 10, 20);
            //tributo4.CodReceita = 561;
            //tributo4.PeriodoApuracao = new DateTime(2014, 09, 30);
            //tributo4.UsoEmpresa = "6003";
            //TributoRemessaBra.Tributos.Add(tributo4);

            ////**** Grasiela - DARF 8301 (Sequencial 1501)
            //TributoEletronico tributo5 = new TributoEletronico();
            //tributo5.TipoTributo = Convert.ToInt32(TributoRemessaBra.TiposTipoTributo.DARF);
            //tributo5.TipoMovimento = "I";
            //tributo5.CPFCNPJContribuinte = new DocBacarios.CPFCNPJ("50.940.386/0001-90");
            //tributo5.NomeContribuinte = "Cond Ed Grasiela";
            //tributo5.DAAgencia = 109;
            //tributo5.DADigAgencia = "0";
            //tributo5.DANumConta = 258238;
            //tributo5.DADigNumConta = "4";
            //tributo5.DataDebito = new DateTime(2014, 10, 16);
            //tributo5.Valor = 89.66M;
            //tributo5.DataVencimento = new DateTime(2014, 10, 24);
            //tributo5.CodReceita = 8301;
            //tributo5.PeriodoApuracao = new DateTime(2014, 09, 30);
            //tributo5.UsoEmpresa = "6004";
            //TributoRemessaBra.Tributos.Add(tributo5);

            ////**** Grasiela - GPS (Sequencial 1503)
            //TributoEletronico tributo6 = new TributoEletronico();
            //tributo6.TipoTributo = Convert.ToInt32(TributoRemessaBra.TiposTipoTributo.GPS);
            //tributo6.TipoMovimento = "I";
            //tributo6.CPFCNPJContribuinte = new DocBacarios.CPFCNPJ("50.940.386/0001-90");
            //tributo6.NomeContribuinte = "Cond Ed Grasiela";
            //tributo6.DAAgencia = 109;
            //tributo6.DADigAgencia = "0";
            //tributo6.DANumConta = 258238;
            //tributo6.DADigNumConta = "4";
            //tributo6.DataDebito = new DateTime(2014, 10, 17);
            //tributo6.Valor = 2727.08M;
            //tributo6.OutrasEntidades = 403.49M;
            //tributo6.DataVencimento = new DateTime(2014, 10, 20);
            //tributo6.CodINSS = 2100;
            //tributo6.CompetenciaAno = 2014;
            //tributo6.CompetenciaMes = 9;
            //tributo6.UsoEmpresa = "6006";
            //TributoRemessaBra.Tributos.Add(tributo6);

            //**** Grasiela - FGTS (Sequencial 1505)
            TributoEletronico tributo6 = new TributoEletronico();
            tributo6.TipoTributo = Convert.ToInt32(TributoRemessaBra.TiposTipoTributo.CodigoBarras);
            tributo6.TipoMovimento = "I";
            tributo6.CPFCNPJContribuinte = new DocBacarios.CPFCNPJ("50.940.386/0001-90");
            tributo6.NomeContribuinte = "Cond Ed Grasiela";
            tributo6.DAAgencia = 109;
            tributo6.DADigAgencia = "0";
            tributo6.DANumConta = 258238;
            tributo6.DADigNumConta = "4";
            tributo6.DataDebito = new DateTime(2014, 10, 28);
            tributo6.CodigoBarra = "858500000088682701791414107574050853094038600011";
            tributo6.Valor = 868.27M;
            tributo6.DataVencimento = new DateTime(2014, 11, 07);
            tributo6.UsoEmpresa = "6008";
            TributoRemessaBra.Tributos.Add(tributo6);

            //Grava arquivo
            strNomeArquivo = TributoRemessaBra.GravaArquivo(@"C:\Temp\Neon\ArquivosBradesco\", strNomeArquivo);
            if (strNomeArquivo == "")
            {
                textBox1.Text = "Erro ao gerar arquivo: " + TributoRemessaBra.UltimoErro.Message;
                return;
            }

            strNomeArquivo = TributoRemessaBra.FinalizaArquivo(strNomeArquivo);
            if (strNomeArquivo == "")
            { textBox1.Text = "Erro ao gerar arquivo: " + TributoRemessaBra.UltimoErro.Message; }
            else
            { textBox1.Text = "Arquivo Remessa gerado em: " + strNomeArquivo; }
        }

        private void cmdDiarioRetorno_Click(object sender, EventArgs e)
        {
            //Seleciona arquivo de retorno
            textBox1.Text = "";
            if (openFileDialog1.ShowDialog() == System.Windows.Forms.DialogResult.OK)
            {
                foreach (string arquivo in openFileDialog1.FileNames)
                {
                    //Inicia Retorno
                    TributoRetornoBra TributoRetornoBra = new TributoRetornoBra();

                    //Carrega Arquivo
                    TributoRetornoBra.Carrega(arquivo);

                    //Gera Relatorio
                    textBox1.Text += TributoRetornoBra.Relatorio();
                }

                //Exibe relatorio
                Clipboard.SetText(textBox1.Text);
            }
        }

        private void cmdDiarioComprovante_Click(object sender, EventArgs e)
        {
            //Seleciona arquivo de retorno
            textBox1.Text = "";
            if (openFileDialog1.ShowDialog() == System.Windows.Forms.DialogResult.OK)
            {
                foreach (string arquivo in openFileDialog1.FileNames)
                {
                    //Inicia Retorno
                    TributoRetornoBra TributoRetornoBra = new TributoRetornoBra();

                    //Carrega Arquivo
                    TributoRetornoBra.Carrega(arquivo);

                    //Gera Comprovante - DARF (001) ** Usando apenas modelo DARF PRETA **
                    foreach (EDIBoletos.RegistrosOBJTributo.regTransacao_1_2_TRIBUTO_001_BRA tributo in TributoRetornoBra.Tributos1)
                    {
                        //Verifica se 62 = Débito Efetuado
                        if (Convert.ToInt16(tributo.CodConsistenciaArq) == 62)
                        {
                            dllImpostos.Comprovantes.ComprDARFPT_Bradesco Comprovante = new dllImpostos.Comprovantes.ComprDARFPT_Bradesco();
                            Comprovante.dImpCompr.DARFPT_Bradesco.Clear();
                            //*** MRC - INICIO - TRIBUTOS (10/12/2014 12:00) ***
                            Comprovante.dImpCompr.DARFPT_Bradesco.AddDARFPT_BradescoRow(tributo.NomeContribuinte.Trim(), tributo.PeriodoApuracao,
                                                                                        (new DocBacarios.CPFCNPJ(tributo.NumInscricaoContribuinte)).ToString(),
                                                                                        tributo.CodReceita.ToString("0000"), tributo.Referencia.ToString(), tributo.DataVencimento, 
                                                                                        tributo.Valor, tributo.Multa, tributo.Juros, tributo.ValorTotal,
                                                                                        tributo.Autenticacao, tributo.DataDebito, tributo.IDDocumento,
                                                                                        tributo.DAAgencia.ToString().PadLeft(4, '0') + "-" + tributo.DADigAgencia,
                                                                                        tributo.NomeContribuinte.Trim() + " - ID. " + tributo.UsoEmpresa);
                            //*** MRC - TERMINO - TRIBUTOS (10/12/2014 12:00) ***
                            Comprovante.CreateDocument();
                            Comprovante.ExportToPdf(@"C:\Temp\Neon\ArquivosBradesco\recibo_tributo_" + tributo.UsoEmpresa.Trim() + ".pdf");

                            //Indica comprovante gerado
                            textBox1.Text += "COMPROVANTE GERADO: " + @"C:\Temp\Neon\ArquivosBradesco\recibo_tributo_" + tributo.UsoEmpresa.Trim() + ".pdf\r\n";
                        }
                    }

                    //Gera Comprovante - GPS (005)
                    foreach (EDIBoletos.RegistrosOBJTributo.regTransacao_1_2_TRIBUTO_005_BRA tributo in TributoRetornoBra.Tributos5)
                    {
                        //Verifica se 62 = Débito Efetuado
                        if (Convert.ToInt16(tributo.CodConsistenciaArq) == 62)
                        {
                            dllImpostos.Comprovantes.ComprGPS_Bradesco Comprovante = new dllImpostos.Comprovantes.ComprGPS_Bradesco();
                            Comprovante.dImpCompr.GPS_Bradesco.Clear();
                            //*** MRC - INICIO - TRIBUTOS (10/12/2014 12:00) ***
                            Comprovante.dImpCompr.GPS_Bradesco.AddGPS_BradescoRow(tributo.NomeContribuinte.Trim() + (tributo.EnderecoContribuinte.Trim() == "" ? "" : " / " + tributo.EnderecoContribuinte.Trim()) + (tributo.CepContribuinte == "00000000" ? "" : " - " + tributo.CepContribuinte),
                                                                                  tributo.DataVencimento, tributo.CodINSS.ToString(), tributo.CompetenciaMes.ToString().PadLeft(2, '0') + "/" + tributo.CompetenciaAno,
                                                                                  (new DocBacarios.CPFCNPJ(tributo.NumInscricaoContribuinte)).ToString(),
                                                                                  tributo.Valor, tributo.OutrasEntidades, tributo.Juros, tributo.ValorTotal,
                                                                                  tributo.Autenticacao, tributo.DataDebito, tributo.IDDocumento, 
                                                                                  tributo.DAAgencia.ToString().PadLeft(4, '0') + "-" + tributo.DADigAgencia,
                                                                                  tributo.NomeContribuinte.Trim() + " - ID. " + tributo.UsoEmpresa);
                            //*** MRC - TERMINO - TRIBUTOS (10/12/2014 12:00) ***
                            Comprovante.CreateDocument();
                            Comprovante.ExportToPdf(@"C:\Temp\Neon\ArquivosBradesco\recibo_tributo_" + tributo.UsoEmpresa.Trim() + ".pdf");

                            //Indica comprovante gerado
                            textBox1.Text += "COMPROVANTE GERADO: " + @"C:\Temp\Neon\ArquivosBradesco\recibo_tributo_" + tributo.UsoEmpresa.Trim() + ".pdf\r\n";
                        }
                    }

                    //Gera Comprovante - FGTS (006)
                    foreach (EDIBoletos.RegistrosOBJTributo.regTransacao_1_2_TRIBUTO_006_BRA tributo in TributoRetornoBra.Tributos6)
                    { 
                        //Verifica se 62 = Débito Efetuado
                        if (Convert.ToInt16(tributo.CodConsistenciaArq) == 62) 
                        {
                            dllImpostos.Comprovantes.ComprFGTS_Bradesco Comprovante = new dllImpostos.Comprovantes.ComprFGTS_Bradesco();
                            Comprovante.dImpCompr.FGTS_Bradesco.Clear();
                            Comprovante.dImpCompr.FGTS_Bradesco.AddFGTS_BradescoRow(tributo.CodigoBarra, (new DocBacarios.CPFCNPJ(tributo.NumInscricao)).ToString(),
                                                                                    tributo.CodConvenio, tributo.NumIdentificacao, tributo.CompetenciaMes.ToString().PadLeft(2,'0') + "/" + tributo.CompetenciaAno,
                                                                                    tributo.DataValidade, tributo.IDPagamento, tributo.IDPagamento, tributo.DataDebito, tributo.DataVencimento, 
                                                                                    tributo.ValorTributo, tributo.Juros, tributo.Desconto, tributo.Multa, tributo.ValorPago, 
                                                                                    tributo.AutenticacaoCodBarra, tributo.Autenticacao, tributo.IDDocumento, 
                                                                                    tributo.DAAgencia.ToString().PadLeft(4,'0') + "-" + tributo.DADigAgencia,
                                                                                    "");                    
                                                                                    //"COND. EDIF. VILLA CAROLINA (CAROL.)");
                                                                                    //"COND. EDIF. GRASIELA (GRASIE)");

                            Comprovante.CreateDocument();
                            Comprovante.ExportToPdf(@"C:\Temp\Neon\ArquivosBradesco\recibo_tributo_" + tributo.UsoEmpresa.Trim() + ".pdf");

                            //Indica comprovante gerado
                            textBox1.Text += "COMPROVANTE GERADO: " + @"C:\Temp\Neon\ArquivosBradesco\recibo_tributo_" + tributo.UsoEmpresa.Trim() + ".pdf\r\n";
                        }
                    }
                }
            }
        }

        private void button2_Click(object sender, EventArgs e)
        {
            AbstratosNeon.ABS_Fornecedor Fornecedor;
            DocBacarios.CPFCNPJ cnpj = new DocBacarios.CPFCNPJ(textEdit1.Text);
            if (cnpj.Tipo != DocBacarios.TipoCpfCnpj.INVALIDO)
            {
                Fornecedor = AbstratosNeon.ABS_Fornecedor.ABSGetFornecedor(cnpj, true);
                if (Fornecedor != null)
                {
                    MessageBox.Show(string.Format("Achei:{0}", Fornecedor.FRN));
                }
                else
                {
                    MessageBox.Show("Não Achei");
                }
            }
        }

        private void button4_Click(object sender, EventArgs e)
        {
            CompontesBasicosProc.ContaCorrente Conta;
            Conta = new CompontesBasicosProc.ContaCorrente(TipoConta.CC, 341, 3817, 0, 11877, "5");
            MessageBox.Show(Conta.validaConta().ToString());
            Conta = new CompontesBasicosProc.ContaCorrente(TipoConta.CC, 341, 8703, 0, 14599, "8");
            MessageBox.Show(Conta.validaConta().ToString());
            Conta = new CompontesBasicosProc.ContaCorrente(TipoConta.CC, 341, 8470, 0, 10783, "3");
            MessageBox.Show(Conta.validaConta().ToString());
            /*
            Conta = new CompontesBasicosProc.ContaCorrente(CompontesBasicosProc.ContaCorrente.TipoConta.CC, 237, 1191, 0, 1, "1");
            MessageBox.Show(Conta.validaAgencia().ToString());
            Conta = new CompontesBasicosProc.ContaCorrente(CompontesBasicosProc.ContaCorrente.TipoConta.CC, 237, 49, 0, 1, "1");
            MessageBox.Show(Conta.validaAgencia().ToString());
            Conta = new CompontesBasicosProc.ContaCorrente(CompontesBasicosProc.ContaCorrente.TipoConta.CC, 237, 591, 0, 1, "1");
            MessageBox.Show(Conta.validaAgencia().ToString());*/
        }

        private void button5_Click(object sender, EventArgs e)
        {
            CompontesBasicosProc.TiposServidorProc TipoS = dllClienteServProc.ClienteServProc.TipoServidoSelecionado;
            MessageBox.Show(string.Format("Tipo = {0}", TipoS));

        }

        
        
    }
}
