﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Text;
using System.Windows.Forms;
using CompontesBasicos;
using virCompet;

namespace virCompetComp
{
    /// <summary>
    /// Parte visual do componente de competencias
    /// </summary>
    public partial class cvirCompetencia : ComponenteBase
    {
        /// <summary>
        /// 
        /// </summary>
        public virCompetencia Comp;

        private virCompetencia compMinima;
        private virCompetencia compMaxima;

        private bool _PermiteNulo;

        /// <summary>
        /// 
        /// </summary>
        [Category("Virtual Software")]
        [Description("Permite competencia nula")]
        public bool PermiteNulo
        {
            get
            {
                return _PermiteNulo;
            }
            set
            {
                _PermiteNulo = value;
                if (!_PermiteNulo)
                    IsNull = false;
                AjustaTela();
            }
        }

        private bool _IsNull;

        /// <summary>
        /// 
        /// </summary>
        public bool IsNull
        {
            get
            {
                return _IsNull;
            }
            set
            {
                if (PermiteNulo)
                {
                    if (_IsNull != value)
                    {
                        _IsNull = value;
                        AjustaTela();
                        DisparaEventoOnChange();
                    }
                }
            }
        }

        /// <summary>
        /// 
        /// </summary>
        [Bindable(true)]
        [Category("Virtual Software")]
        [Description("Competência em forma int para o banco de dados")]
        public object CompetenciaBind
        {
            get
            {
                if (IsNull)
                    return DBNull.Value;
                else
                    if (Comp == null)
                        return DBNull.Value;
                    else
                        return Comp.CompetenciaBind;
            }
            set
            {
                if (value is Int32)
                {
                    IsNull = false;
                    if (Comp != null)                        
                        Comp.CompetenciaBind = (Int32)value;
                }
                else
                    IsNull = true;
                AjustaTela();
            }
        }

        /// <summary>
        /// 
        /// </summary>
        protected enum FormaBotoes
        {
            /// <summary>
            /// 
            /// </summary>
            maismenos,
            /// <summary>
            /// 
            /// </summary>
            maismenosdel,
            /// <summary>
            /// 
            /// </summary>
            ativar
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="Forma"></param>
        protected void AjustaBotao(FormaBotoes Forma)
        {
            switch (Forma)
            {
                case FormaBotoes.maismenos:
                    buttonEdit1.Properties.Buttons[0].Visible = true;
                    buttonEdit1.Properties.Buttons[1].Visible = true;
                    buttonEdit1.Properties.Buttons[2].Visible = false;
                    buttonEdit1.Properties.Buttons[3].Visible = false;
                    anoCompet.Enabled = mesCompet.Enabled = true;
                    break;
                case FormaBotoes.maismenosdel:
                    buttonEdit1.Properties.Buttons[0].Visible = true;
                    buttonEdit1.Properties.Buttons[1].Visible = true;
                    buttonEdit1.Properties.Buttons[2].Visible = true;
                    buttonEdit1.Properties.Buttons[3].Visible = false;
                    anoCompet.Enabled = mesCompet.Enabled = true;
                    break;
                case FormaBotoes.ativar:
                    buttonEdit1.Properties.Buttons[0].Visible = false;
                    buttonEdit1.Properties.Buttons[1].Visible = false;
                    buttonEdit1.Properties.Buttons[2].Visible = false;
                    buttonEdit1.Properties.Buttons[3].Visible = true;
                    anoCompet.Enabled = mesCompet.Enabled = false;
                    break;
                default:
                    break;
            }
        }

        /// <summary>
        /// Minima Competencia
        /// </summary>
        public virCompetencia CompMinima
        {
            get
            {
                return compMinima ?? (compMinima = new virCompetencia(1, 2000));
            }
            set
            {
                compMinima = value;
                AjustaTela();
            }
        }

        /// <summary>
        /// Máxima Competencia
        /// </summary>
        public virCompetencia CompMaxima
        {
            get
            {
                return compMaxima ?? (compMaxima = new virCompetencia().Add(24));
            }
            set
            {
                compMaxima = value;
                AjustaTela();
            }
        }

        private bool Editando = false;

        /// <summary>
        /// Ajusta Competencia
        /// </summary>
        /// <param name="Origem">Instancia para cópia</param>
        public void SetCompetencia(virCompetencia Origem)
        {
            Comp.CopiaValores(Origem);
            AjustaTela();
        }

        /// <summary>
        /// Ajusta a tela para a competencia Comp
        /// </summary>
        public void AjustaTela()
        {
            Editando = true;
            try
            {
                if (PermiteNulo)
                {
                    if (IsNull)
                        AjustaBotao(FormaBotoes.ativar);
                    else
                        AjustaBotao(FormaBotoes.maismenosdel);
                }
                else
                    AjustaBotao(FormaBotoes.maismenos);
                if (IsNull)
                {
                    mesCompet.Text = anoCompet.Text = string.Empty;
                    mesCompet.EditValue = anoCompet.EditValue = null;
                }
                else
                {
                    if ((Comp == null) || (Comp < CompMinima))
                    {
                        if (Comp == null)
                            Comp = CompMinima.CloneCompet();
                        Comp.Ano = CompMinima.Ano;
                        Comp.Mes = CompMinima.Mes;
                    }
                    else if (Comp > CompMaxima)
                    {
                        Comp.Ano = CompMaxima.Ano;
                        Comp.Mes = CompMaxima.Mes;
                    }
                    mesCompet.Value = Comp.Mes;
                    anoCompet.Value = Comp.Ano;
                }
            }
            finally
            {
                Editando = false;
            }
        }

        /// <summary>
        /// Construtor padrão
        /// </summary>
        public cvirCompetencia()
        {
            InitializeComponent();
            if (CompontesBasicos.FormPrincipalBase.FormPrincipal != null)
            {
                DataBindings.DefaultDataSourceUpdateMode = DataSourceUpdateMode.Never;
                Validated += GravaCompetenciaBind;
                Comp = new virCompetencia(DateTime.Now.Month, DateTime.Now.Year);
                GravaCompetenciaBind();
                AjustaTela();
            }
        }

        private void GravaCompetenciaBind(object sender, EventArgs e)
        {
            GravaCompetenciaBind();
        }

        /// <summary>
        /// 
        /// </summary>
        public void GravaCompetenciaBind()
        {
            if (CompontesBasicos.FormPrincipalBase.FormPrincipal != null)
            {
                Binding B = DataBindings["CompetenciaBind"];
                if (B != null)
                    B.WriteValue();
            }
        }

        private void spinEdit2_Spin(object sender, DevExpress.XtraEditors.Controls.SpinEventArgs e)
        {

            Comp.Add(e.IsSpinUp ? 1 : -1);
            AjustaTela();
        }

        private void anoCompet_ButtonClick(object sender, DevExpress.XtraEditors.Controls.ButtonPressedEventArgs e)
        {

            switch (e.Button.Kind)
            {
                case DevExpress.XtraEditors.Controls.ButtonPredefines.Right:
                    Comp.Add(1);
                    AjustaTela();
                    DisparaEventoOnChange();
                    break;
                case DevExpress.XtraEditors.Controls.ButtonPredefines.Left:
                    Comp.Add(-1);
                    AjustaTela();
                    DisparaEventoOnChange();
                    break;
                case DevExpress.XtraEditors.Controls.ButtonPredefines.Ellipsis:
                    IsNull = false;
                    break;
                case DevExpress.XtraEditors.Controls.ButtonPredefines.Undo:
                    IsNull = true;
                    break;
            }
        }

        private void mesCompet_EditValueChanged(object sender, EventArgs e)
        {

            if (!Editando)
            {
                Editando = true;
                Comp.Mes = (int)mesCompet.Value;
                Comp.Ano = (int)anoCompet.Value;
                Editando = false;
                AjustaTela();
                DisparaEventoOnChange();
            }
        }

        /// <summary>
        /// Retorna a competencia (Clone)
        /// </summary>
        /// <remarks>Retorna um clone</remarks>
        /// <returns>Cometencia</returns>
        public virCompetencia GetCompetencia()
        {
            return Comp.CloneCompet();
        }

        /// <summary>
        /// Evento que ocorre após a ateração da competência
        /// </summary>        
        [Category("Virtual Software")]
        [Description("Alterção da competência")]
        public event EventHandler OnChange;

        private void DisparaEventoOnChange()
        {
            if (OnChange != null)
                OnChange(this, new EventArgs());
        }

        /// <summary>
        /// 
        /// </summary>
        public override bool somenteleitura
        {
            get
            {
                return base.somenteleitura;
            }
            set
            {
                buttonEdit1.Visible = !value;
                mesCompet.Properties.ReadOnly = anoCompet.Properties.ReadOnly = value;
                base.somenteleitura = value;
            }
        }

        
    }
}
