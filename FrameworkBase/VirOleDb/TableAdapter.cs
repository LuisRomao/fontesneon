using System;
using System.Data;
using System.Data.OleDb;
using VirDB;
using VirDB.Bancovirtual;


namespace VirOleDb
{
    /// <summary>
    /// TableAdapter para OleDb
    /// </summary>
    public class TableAdapter : VirtualTableAdapter
    {
        /// <summary>
        /// Retorna a data e hora da esta��o
        /// </summary>
        /// <returns></returns>
        public override DateTime DataServidor()
        {
            return DateTime.Now;
        }

        /// <summary>
        /// N�o implementado
        /// </summary>
        /// <param name="StringDeConexao"></param>
        public override void CriaCone(string StringDeConexao)
        {
            throw new Exception("The method or operation is not implemented.");
        }

        /// <summary>
        /// N�o implementado
        /// </summary>
        /// <param name="comando"></param>
        /// <param name="Parametros"></param>
        /// <returns></returns>
        public override int IncluirAutoInc(string comando, params object[] Parametros)
        {
            throw new Exception("The method or operation is not implemented.");
        }

        /// <summary>
        /// Tableadapter est�tico
        /// </summary>
        protected override VirtualTableAdapter STTableAdapterEspecifico
        {
            get { return STTableAdapter; }
        }

        /// <summary>
        /// Est� em transa��o
        /// </summary>
        protected override bool EstaEmTransST
        {
            get { return (GuardaConeLocal != null); }
        }

        /// <summary>
        /// Adapter do objeto derivado
        /// </summary>        
        /// <remarks>Para os objetos staticos retorna null</remarks>
        public OleDbDataAdapter VirAdapter
        {
            get
            {
                System.Reflection.BindingFlags BF = System.Reflection.BindingFlags.NonPublic | System.Reflection.BindingFlags.Instance;
                System.Reflection.PropertyInfo PropAdap = GetType().GetProperty("Adapter", BF);
                if (PropAdap != null)
                    return (OleDbDataAdapter)PropAdap.GetValue(this, null);
                else
                    return null;
            }
        }

        #region Conexao
        /// <summary>
        /// Conex�o est�tica
        /// </summary>
        private OleDbConnection _Cone;

        private OleDbConnection Cone
        {
            get
            {
                if (_Cone == null)
                {
                    System.Reflection.PropertyInfo PropCon = GetType().GetProperty("Connection");
                    if (PropCon != null)
                        _Cone = (OleDbConnection)PropCon.GetValue(this, null);
                    else
                        _Cone = new OleDbConnection(BancoVirtual.StringConeFinal(Tipo));
                };
                return _Cone;
            }
            set
            {
                _Cone = value;

                System.Reflection.PropertyInfo PropCon = GetType().GetProperty("Connection");
                if (PropCon != null)
                    PropCon.SetValue(this, value, null);


            }
        }

        /// <summary>
        /// Fecha conexao
        /// </summary>
        /// <returns></returns>
        protected override bool FechaCone()
        {
            if (_Cone != null)
            {
                if (Cone.State != ConnectionState.Closed)
                    Cone.Close();
            }
            return true;
        }

        /// <summary>
        /// Abre conexao
        /// </summary>
        /// <returns></returns>
        protected override bool AbreCone()
        {
            if (Cone.State == ConnectionState.Closed)
                Cone.Open();
            return true;
        }

        #endregion

        /// <summary>
        /// Inicia uma transacao Local e implanta nos VirtualTableAdapters, Usar DENTRO do try com vircath
        /// </summary>
        /// <param name="Identificador">Identificador</param>
        /// <param name="VirtualTableAdapters">TableAdapters envolvidos</param>
        /// <returns></returns>
        public static bool AbreTrasacaoSQL(string Identificador, params VirOleDb.TableAdapter[] VirtualTableAdapters)
        {
            return STTableAdapter.AbreTrasacaoLocal(Identificador, VirtualTableAdapters);
        }

        private static TableAdapter stTableAdapter;
        private static TableAdapter stTableAdapterH;

        /// <summary>
        /// Fornece a instancia est�tinca para OleDb
        /// </summary>
        public static TableAdapter STTableAdapter
        {
            get
            {                
                    if (stTableAdapter == null)
                    {
                        stTableAdapter = new TableAdapter(TiposDeBanco.Access);
                    };
                    return stTableAdapter;
            }
            
        }

        /// <summary>
        /// Inicia uma transacao Local e implanta nos VirtualTableAdapters, Usar DENTRO do try com vircath
        /// </summary>
        /// <param name="Identificador">Identificador</param>
        /// <param name="VirtualTableAdapters">TableAdapters envolvidos</param>
        /// <returns></returns>
        public static bool AbreTrasacaoOLEDB(string Identificador, params VirOleDb.TableAdapter[] VirtualTableAdapters)
        {
            return STTableAdapter.AbreTrasacaoLocal(Identificador, VirtualTableAdapters);
        }

        /// <summary>
        /// Fornece a instancia est�tinca para o tipo definido
        /// </summary>
        /// <param name="TB">Tipo 
        /// S� s�o v�idos os Access e AccessH
        /// </param>
        /// <returns>Instancia est�tica</returns>
        public static TableAdapter STTableAdapterTipo(TiposDeBanco TB)
        {
            switch(TB){
                case TiposDeBanco.Access:
                    if (stTableAdapter == null)
                      stTableAdapter = new TableAdapter(TiposDeBanco.Access);
                    return stTableAdapter;
                case TiposDeBanco.AccessH:
                    if (stTableAdapterH == null)
                        stTableAdapterH = new TableAdapter(TiposDeBanco.AccessH);
                    return stTableAdapterH;
                case TiposDeBanco.FB:
                case TiposDeBanco.Internet1:
                case TiposDeBanco.Internet2:
                case TiposDeBanco.SQL:
                case TiposDeBanco.SQLMorto:
                default: 
                    throw new Exception("Tipo de banco inv�lido"); 
            }

        }

        /// <summary>
        /// TableAdapter para OleDb
        /// </summary> 
        public TableAdapter()
        {
            Tipo = TiposDeBanco.Access;
        }

        /// <summary>
        /// Construtor
        /// </summary>
        /// <param name="_Tipo">Tipo 
        /// S� s�o v�idos os Access e AccessH
        /// </param>
        public TableAdapter(TiposDeBanco _Tipo)
        {
            Tipo = _Tipo;
            if ((Tipo != TiposDeBanco.Access) && (Tipo != TiposDeBanco.AccessH))
                throw new Exception("Parametros inv�lidos");
        }

        

        #region Transacao

        /// <summary>
        /// Transa��o est�tica
        /// </summary>
        public OleDbTransaction Tran;

        /// <summary>
        /// Quarda a conexao real do componente para incorpor�lo na transaco STD
        /// </summary>
        private OleDbConnection GuardaConeLocal;

        /// <summary>
        /// Coloca o adapter sob a transacao Tran
        /// </summary>
        /// <param name="Inicia">Inicio ou termino</param>
        protected override void ImplantaTrans(bool Inicia)
        {
            if (VirAdapter == null)
                return;
            OleDbTransaction Tranx = Inicia ? Tran : null;
            if (VirAdapter.InsertCommand != null)
                VirAdapter.InsertCommand.Transaction = Tranx;

            if (VirAdapter.DeleteCommand != null)
                VirAdapter.DeleteCommand.Transaction = Tranx;

            if (VirAdapter.UpdateCommand != null)
                VirAdapter.UpdateCommand.Transaction = Tranx;

            if (VirAdapter.SelectCommand != null)
                VirAdapter.SelectCommand.Transaction = Tranx;
            System.Reflection.BindingFlags BF = System.Reflection.BindingFlags.NonPublic | System.Reflection.BindingFlags.Instance;
            System.Reflection.PropertyInfo PropCom = GetType().GetProperty("CommandCollection", BF);
            if (PropCom != null)
            {
                OleDbCommand[] Comandos = (OleDbCommand[])PropCom.GetValue(this, null);
                for (int i = 0; (i < Comandos.Length); i = (i + 1))
                    if ((Comandos[i] != null))
                        Comandos[i].Transaction = Tranx;
            }
        }


        /// <summary>
        /// Inicia Transacao
        /// </summary>
        /// <param name="Nivel">Nivel de isolamento</param>
        /// <returns>Se a trasacao foi iniciada</returns>       
        protected override bool IniciaTrasacaoReal(IsolationLevel Nivel)
        {
            if (Tran != null)
            {
                FimTrasacaoReal(false);
                throw new InvalidOperationException(String.Format("Estado inv�lido: SubTrans = {0}\r\n Tentativa de apertura de transa��o com outra j� em andamento", SubTrans));
            }
            else
            {
                if (!AbreCone())
                    return false;
                Tran = Cone.BeginTransaction(Nivel);
                ImplantaTrans(true);
                return true;
            }
        }

        /// <summary>
        /// Termina trasacao
        /// </summary>
        /// <param name="Commit">True para commit e false para rollback</param>
        /// <returns>Esta retornando sempre true</returns>
        protected override bool FimTrasacaoReal(bool Commit)
        {
            ImplantaTrans(false);
            if (GuardaConeLocal != null)
            {
                Cone = GuardaConeLocal;
                GuardaConeLocal = null;
                Tran = null;
            }
            else
            {
                if (Tran == null)
                    throw new InvalidOperationException(String.Format("Estado inv�lido: SubTrans = {0}\r\n Tentativa de termino de transa��o com Trans = null", SubTrans));
                if (Commit)
                    Tran.Commit();
                else
                    Tran.Rollback();
                Tran = null;
                FechaCone();
            };
            return true;
        }

        /// <summary>
        ///Coloca o componente na transacao de _Dono_Trans que pode ser STTableAdapter
        /// </summary>
        /// <remarks>Este m�todo � para que v�rios objetos participem da mesma transacao: A transa��o de STTableAdapter\r\nGera exce��o se STTableAdapter nao estiver em transacao</remarks>       
        protected override bool EntrarEmTrans(VirtualTableAdapter _Dono_Trans)
        {
            TableAdapter Dono_Trans = (TableAdapter)_Dono_Trans;
            if (GuardaConeLocal != null)
                return false;
            if (Tran != null)
                throw new Exception("Objeto em trasa��o local");
            else
            {
                //EstaEmTransST = true;
                if (Dono_Trans.Tran == null)
                    throw new Exception("STTableAdapter n�o est� em transacao");
                Tran = Dono_Trans.Tran;
                GuardaConeLocal = Cone;
                Cone = Dono_Trans.Cone;
                ImplantaTrans(true);
                return true;
            }
        }

        /*
        /// <summary>
        /// Inicia uma trasacao Estatica. Na verdade coloca o componente na transacao de STTableAdapter
        /// </summary>
        /// <remarks>Este m�todo � para que v�rios objetos participem da mesma transacao: A transa��o de STTableAdapter\r\nGera exce��o se STTableAdapter nao estiver em transacao</remarks>       
        protected override bool IniciaTrasacaoST()
        {
            if (GuardaConeLocal != null)
                return false;
            if (Tran != null)
                throw new Exception("Objeto em trasa��o local");
            else
            {
                EstaEmTransST = true;
                if (VirOleDb.TableAdapter.stTableAdapter.Tran == null)
                    throw new Exception("STTableAdapter n�o est� em transacao");
                Tran = VirOleDb.TableAdapter.STTableAdapter.Tran;
                GuardaConeLocal = Cone;
                Cone = VirOleDb.TableAdapter.STTableAdapter.Cone;
                ImplantaTrans(true);
                return true;
            }
        }
        */

        /// <summary>
        /// Retorna se a transa�ao est�tica esta ativa
        /// </summary>
        /// <returns></returns>
        public override bool STTransAtiva()
        {
            return (VirOleDb.TableAdapter.STTableAdapter.Tran != null);
        }

        #endregion
        



        /// <summary>
        /// Executa um comando SQL.
        /// </summary>
        /// <remarks>Os paramentros devem obrigatoriamentes ser nomeados como @P1, @P2 ...</remarks>
        /// <param name="comando">Comando SQL: select, update ou delete</param>
        /// <param name="TipoComando">
        /// Tipo de a��o/retorno
        /// pode retornar um unico registro em forma de ArrayList,
        /// escalar,
        /// DataTable
        /// ou ArrayList com os valores do primeiro registro
        /// </param>
        /// <param name="Paramentros">
        /// Paramentros do comando SQL usar sintaxe:        
        /// </param>
        public override object ExecutarSQL(string comando, ComandosBusca TipoComando, params object[] Paramentros)
        {
            object oRetorno;
            if (!AbreCone())
                return null;

            using (OleDbCommand Comando = new OleDbCommand(comando, Cone, Tran))
            {
                if (Paramentros != null)
                {
                    int i = 1;
                    foreach (object Parametro in Paramentros)
                    {
                        Comando.Parameters.AddWithValue("@P" + i.ToString(), Parametro);
                        i++;
                    }
                }
                ; ;
                switch (TipoComando)
                {
                    case ComandosBusca.ArrayList:
                        System.Collections.ArrayList retorno1 = new System.Collections.ArrayList();
                        OleDbDataReader RespostaSQL1 = Comando.ExecuteReader();
                        if (RespostaSQL1.Read())
                            for (int i = 0; i < RespostaSQL1.FieldCount; i++)
                                retorno1.Add(RespostaSQL1[i]);
                        RespostaSQL1.Close();
                        oRetorno = retorno1;
                        break;
                    case ComandosBusca.DataRow:
                        DataTable TabelaRetorno2 = new DataTable();
                        int registros = (new OleDbDataAdapter(Comando).Fill(TabelaRetorno2));
                        if (registros > 0)
                            oRetorno = TabelaRetorno2.Rows[0];
                        else
                            oRetorno = null;
                        break;
                    case ComandosBusca.DataTable:
                        DataTable TabelaRetorno1 = new DataTable();
                        using (OleDbDataAdapter oleDbDataAdapter = new OleDbDataAdapter(Comando))
                        {
                            oleDbDataAdapter.Fill(TabelaRetorno1);
                        }
                        oRetorno = TabelaRetorno1;
                        break;
                    case ComandosBusca.Escalar:
                        oRetorno = Comando.ExecuteScalar();
                        break;
                    case ComandosBusca.Reader:
                        oRetorno = Comando.ExecuteReader();
                        break;
                    case ComandosBusca.NonQuery:
                        oRetorno = Comando.ExecuteNonQuery();
                        break;
                    default:
                        oRetorno = null;
                        break;
                }
            }
            if (Tran == null)
                FechaCone();
            return oRetorno;

        }


        //public string erro = "";
        /*
        public bool IniciaTrasacao()
        {
            try
            {
                System.Reflection.PropertyInfo PropCon = this.GetType().GetProperty("Connection");
                SQLCone = (SqlConnection)PropCon.GetValue(this, null);
                if (SQLCone.State == ConnectionState.Closed)
                    SQLCone.Open();
                FbTran = SQLCone.BeginTransaction(FbTransactionOptions.ReadCommitted);
                return true;
            }
            catch (Exception e)
            {
                erro = e.Message;
                return false;
            }
        }

        public int ExecutaEmTransacao(string comando, params object[] Paramentros)
        {

            FbCommand coSQLFB = new FbCommand(comando, coneFBTrans, FbTran);
            if (Paramentros != null)
            {
                int i = 1;
                foreach (object Parametro in Paramentros)
                {
                    coSQLFB.Parameters.AddWithValue("@P" + i.ToString(), Parametro);
                    i++;
                }
            };
            return coSQLFB.ExecuteNonQuery();
        }

        public bool Comita()
        {
            try
            {
                FbTran.Commit();
                FbTran = null;
                coneFBTrans.Close();
                return true;
            }
            catch
            {
                FbTran = null;
                return false;
            }

        }

        /// <summary>
        /// TableAdapter para FireBird
        /// </summary> 
        public TableAdapter()
        {
            Tipo = CompontesBasicos.Bancovirtual.TiposDeBanco.FB;
        }

        public override void TrocarStringDeConexao(string novo)
        {
            System.Reflection.PropertyInfo PropCon = this.GetType().GetProperty("Connection");
            if (PropCon == null)
                throw new Exception("Connection n�o encontrado.(" + this.ToString() + ")");
            else
            {
                FbConnection cone = (FbConnection)PropCon.GetValue(this, null);
                cone.ConnectionString = novo;
            };
        }

        /// <summary>
        /// Executa um comando SQL
        /// </summary>        
        /// <returns>DataTable,DataReader,escalar ou registros afetados</returns>
        public override object BuscaSQL(string comando, VirtualTableAdapter.ComandosBusca TipoComando, params object[] Paramentros)
        {
            System.Reflection.PropertyInfo PropCon = this.GetType().GetProperty("Connection");
            FbConnection coneFB = (FbConnection)PropCon.GetValue(this, null);
            FbCommand coSQLFB = new FbCommand(comando, coneFB);
            if (coneFB.State == ConnectionState.Closed)
                coneFB.Open();
            if (Paramentros != null)
            {
                int i = 1;
                foreach (object Parametro in Paramentros)
                {
                    coSQLFB.Parameters.AddWithValue("@P" + i.ToString(), Parametro);
                    i++;
                }
            }
            switch (TipoComando)
            {
                case ComandosBusca.ArrayList:
                    System.Collections.ArrayList retorno = new System.Collections.ArrayList();
                    FbDataReader RespostaSQL = coSQLFB.ExecuteReader();
                    if (RespostaSQL.Read())
                        for (int i = 0; i < RespostaSQL.FieldCount; i++)
                            retorno.Add(RespostaSQL[i]);
                    RespostaSQL.Close();
                    return retorno;
                case ComandosBusca.DataTable:
                    DataTable TabelaRetorno = new DataTable();
                    FbDataReader RespostaSQL1 = coSQLFB.ExecuteReader();
                    while (RespostaSQL1.Read())
                    {
                        TabelaRetorno.Rows.Add(RespostaSQL1);
                        //for (int i = 0; i < RespostaSQL.FieldCount; i++)
                        //    retorno.Add(RespostaSQL[i]);
                    }
                    RespostaSQL1.Close();
                    return TabelaRetorno;
                case ComandosBusca.Escalar:
                    return coSQLFB.ExecuteScalar();
                case ComandosBusca.Reader:
                    return coSQLFB.ExecuteReader();
                case ComandosBusca.NonQuery:
                    return coSQLFB.ExecuteNonQuery();
                default: return null;
            };
        }



        /// <summary>
        /// Nome da tabela no banco de dados
        /// </summary>
        /// <remarks>Usado para auto incremeto em bancos FB</remarks>
        public string Tabela;

        /// <summary>
        /// Pega numeros de auto incremento em bancos FB
        /// </summary>
        /// <remarks>O campo Tabela deve estar preenchido, caso contr�rio uma excess�o sera gerada</remarks>
        /// <returns>Novo n�mero</returns>
        public Int32 PegaNumeroFB()
        {
            if ((Tabela == "") || (Tabela == null))
                throw new Exception("Tabela n�o defina para o VirtualTableAdapter");
            return PegaNumeroFB(Tabela);
        }

        /// <summary>
        /// Pega numeros de auto incremento em bancos FB
        /// </summary>
        /// <param name="Tabela">Nome da tabela no banco de dados</param>
        /// <returns>Auto incremento</returns>
        public Int32 PegaNumeroFB(string Tabela)
        {
            //object reto = BuscaSQL("select GEN_ID(gen_" + Tabela + "_id,1) from rdb$database",true);
            //string tipo = reto.GetType().ToString();
            return (Int32)(Int64)BuscaSQL("select GEN_ID(gen_" + Tabela + "_id,1) from rdb$database", ComandosBusca.Escalar);
        }*/
    }
}
