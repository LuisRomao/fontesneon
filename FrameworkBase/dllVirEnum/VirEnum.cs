using System;
using System.Drawing;
using DevExpress.XtraEditors;
using DevExpress.XtraEditors.Controls;
using DevExpress.XtraEditors.Repository;
using System.Collections;
using System.Collections.Generic;

namespace dllVirEnum
{
    /// <summary>
    /// Enumera��es para banco de dados
    /// </summary>
    public class VirEnum
    {
        private Type TipoEnum;

        /// <summary>
        /// Cosntrutor pagdr�o
        /// </summary>
        /// <param name="_TipoEnum"></param>
        public VirEnum(Type _TipoEnum)
        {
            TipoEnum = _TipoEnum;
        }

        /// <summary>
        /// Indica se s� s�o carregdos os itens identificados com GravaNomes ou todos usando o nome padr�o
        /// </summary>
        public bool CarregarDefault = true;

        /// <summary>
        /// Traduz o nome do iten
        /// </summary>
        /// <param name="item"></param>
        /// <returns></returns>
        public string Descritivo(object item)
        {
            return Descritivo((int)item);
        }

        /// <summary>
        /// Cor cadastrada
        /// </summary>
        /// <param name="item"></param>
        /// <returns></returns>
        public System.Drawing.Color GetCor(object item)
        {
            return (System.Drawing.Color)Cores[(int)item];
        }

        /// <summary>
        /// Traduz o nome do iten
        /// </summary>
        /// <param name="item"></param>
        /// <returns></returns>
        public string Descritivo(int item)
        {
            if (Nomes.ContainsKey(item))
                return Nomes[item].ToString();
            else
                return "???";
        }

        private SortedList cores;

        private SortedList Cores
        {
            get
            {
                if (cores == null)
                {
                    cores = new SortedList();
                    foreach (object o in Enum.GetValues(TipoEnum))
                        cores.Add((int)o, System.Drawing.Color.Transparent);
                }
                return cores;
            }
        }

        /// <summary>
        /// Cor para cadas item da enumera��o
        /// </summary>
        /// <param name="codigo"></param>
        /// <param name="Cor"></param>
        public void GravaCor(object codigo, Color Cor)
        {
            if (Cores.ContainsKey((int)codigo))
                Cores[(int)codigo] = Cor;
            else
                Cores.Add((int)codigo, Cor);
        }

        private SortedList<int,string> nomes;

        /// <summary>
        /// Nomes Cadastrados
        /// </summary>
        public SortedList<int, string> Nomes
        {
            get
            {
                if (nomes == null)
                {
                    nomes = new SortedList<int, string>();
                    if ((TipoEnum != null) && CarregarDefault)
                        foreach (object o in Enum.GetValues(TipoEnum))
                            nomes.Add((int)o, o.ToString());
                }
                return nomes;
            }
        }

        /// <summary>
        /// Os nomes a serem mostrados para cada item da enumeracao
        /// </summary>
        /// <param name="codigo">item da enumera��o</param>
        /// <param name="Nome">Nome</param>
        /// <remarks>Esta funcao deve ser chamada para cada item da enumera��o</remarks>
        public void GravaNomes(object codigo, string Nome)
        {
            if (Nomes.ContainsKey((int)codigo))
                Nomes[(int)codigo] = Nome;
            else
                Nomes.Add((int)codigo, Nome);
        }

        /// <summary>
        /// Nome para cada item da enumera��o
        /// </summary>
        /// <param name="codigo"></param>
        /// <param name="Nome"></param>
        /// <param name="Cor"></param>
        public void GravaNomes(object codigo, string Nome, Color Cor)
        {
            GravaNomes(codigo, Nome);
            GravaCor(codigo, Cor);
        }

        /// <summary>
        /// Nome para cada item da enumera��o
        /// </summary>
        /// <param name="codigo">Valor da enumera��o</param>
        /// <param name="Nome"></param>
        /// <param name="Cor"></param>
        /// <param name="ch"></param>
        public void GravaNomes(object codigo, string Nome, Color Cor, CheckEdit ch)
        {
            GravaNomes(codigo, Nome, Cor);
            GravaCHs(codigo, ch);
        }

        /// <summary>
        /// Nome para cada item da enumera��o
        /// </summary>
        /// <param name="codigo">Valor da enumera��o</param>
        /// <param name="Nome"></param>
        /// <param name="Cor"></param>
        /// <param name="L">Label da Legenda</param>
        public void GravaNomes(object codigo, string Nome, Color Cor, LabelControl L)
        {
            GravaNomes(codigo, Nome, Cor);
            GravaLabels(codigo, L);
        }


        #region Legenda

        /// <summary>
        /// Tipo de legenda
        /// </summary>
        public enum TipoLegenda
        {
            /// <summary>
            /// Com checkBox
            /// </summary>
            CheckEdit,
            /// <summary>
            /// S� o t�tulo
            /// </summary>
            Label
        }

        /// <summary>
        /// Gera a legenda
        /// </summary>
        /// <param name="L">Objeto Legenda do tipo dllVirEnum.Legenda.Legenda</param>
        public void GeraLegenda(Legenda.Legenda L)
        {
            foreach (object o in Enum.GetValues(TipoEnum))
                L.Registra(Descritivo(o), GetCor(o));
            L.Popula();
        }

        private SortedList cHs;
        private SortedList<int, LabelControl> labelLegenda;

        private SortedList CHs
        {
            get
            {
                if (cHs == null)
                {
                    cHs = new SortedList();
                    foreach (object o in Enum.GetValues(TipoEnum))
                        cHs.Add((int)o, null);
                }
                return cHs;
            }
        }

        private SortedList<int, LabelControl> LabelLegenda
        {
            get
            {
                if (labelLegenda == null)
                {
                    labelLegenda = new SortedList<int, LabelControl>();
                    foreach (object o in Enum.GetValues(TipoEnum))
                        labelLegenda.Add((int)o, null);
                }
                return labelLegenda;
            }
        }

        /// <summary>
        /// Armazena um CheckEdit
        /// </summary>
        /// <param name="codigo"></param>
        /// <param name="ch"></param>
        public void GravaCHs(object codigo, CheckEdit ch)
        {
            ch.BackColor = (Color)Cores[(int)codigo];
            if (CHs.ContainsKey((int)codigo))
                CHs[(int)codigo] = ch;
            else
                CHs.Add((int)codigo, ch);
        }

        /// <summary>
        /// Armazena um Label da legenda
        /// </summary>
        /// <param name="codigo">Valor da enumera��o</param>
        /// <param name="L"></param>
        public void GravaLabels(object codigo, LabelControl L)
        {
            L.BackColor = (Color)Cores[(int)codigo];
            L.Text = Descritivo((int)codigo);
            if (LabelLegenda.ContainsKey((int)codigo))
                LabelLegenda[(int)codigo] = L;
            else
                LabelLegenda.Add((int)codigo, L);
        }

        /// <summary>
        /// Retorna o checkEdit - melhorar usando a legenda
        /// </summary>
        /// <param name="codigo"></param>
        /// <returns></returns>
        public CheckEdit GetCh(object codigo)
        {
            if (CHs.ContainsKey((int)codigo))
                return (CheckEdit)CHs[(int)codigo];
            else
                return null;
        } 

        #endregion

        /// <summary>
        /// Pinta a c�luca da grid conforme o status
        /// </summary>
        /// <param name="oValor"></param>
        /// <param name="e"></param>
        public void CellStyle(object oValor, DevExpress.XtraGrid.Views.Grid.RowCellStyleEventArgs e)
        {
            e.Appearance.BackColor = GetCor(oValor);
            e.Appearance.ForeColor = Color.Black;
        }

        /// <summary>
        /// Trata o n�mero da enumera��o como short (compatibilidade com o banco de dados)
        /// </summary>
        public bool UsarShort = false;

        /// <summary>
        /// Prepara o editor da grid para traduzir os dados do banco para a enumera��o
        /// </summary>
        /// <param name="Coluna"></param>
        /// <returns></returns>
        public bool CarregaEditorDaGrid(DevExpress.XtraGrid.Columns.GridColumn Coluna)
        {
            if ((Coluna == null) || (nomes == null))
                return false;
            if (Coluna.ColumnEdit == null)
                throw new Exception(string.Format("Editor da coluna <{0}> n�o cadastrado",Coluna.FieldName));
            Type TipoEditor = Coluna.ColumnEdit.GetType();
            if ((TipoEditor == typeof(RepositoryItemComboBox)) || (TipoEditor.IsSubclassOf(typeof(RepositoryItemComboBox))))
            {
                
                RepositoryItemComboBox Editor = (RepositoryItemComboBox)Coluna.ColumnEdit;
                return CarregaEditorDaGrid(Editor);                
            }
            else
                throw new Exception(string.Format("Tipo do editor da coluna <{0}> incorreto", Coluna.FieldName));           
        }

        /// <summary>
        /// Prepara o editor para traduzir os dados do banco para a enumera��o
        /// </summary>
        /// <param name="Editor"></param>
        /// <returns></returns>
        public bool CarregaEditorDaGrid(ImageComboBoxEdit Editor)
        {
            return CarregaEditorDaGrid(Editor.Properties);            
        }

        /// <summary>
        /// Prepara o editor para traduzir os dados do banco para a enumera��o
        /// </summary>
        /// <param name="Editor"></param>
        /// <returns></returns>
        public bool CarregaEditorDaGrid(RepositoryItemComboBox Editor)
        {
            if (nomes == null)
                return false;
            foreach (int CodigoReal in Nomes.Keys)
            {
                object oCodigoReal;
                if (UsarShort)
                {
                    short sCodigo = (short)CodigoReal;
                    oCodigoReal = (object)sCodigo;
                }
                else
                    oCodigoReal = (object)CodigoReal;
                ImageComboBoxItem nova = new ImageComboBoxItem(Nomes[CodigoReal], oCodigoReal);
                Editor.Items.Add(nova);
            };
            return true;
        }

    }
}
