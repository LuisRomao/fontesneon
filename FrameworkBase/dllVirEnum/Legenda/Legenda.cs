﻿using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Windows.Forms;
using DevExpress.XtraEditors;

namespace dllVirEnum.Legenda
{
    public partial class Legenda : DevExpress.XtraEditors.XtraUserControl
    {
        private class Unid 
        {
            public Unid(string _Titulo, Color _Cor)
            {
                Titulo = _Titulo;
                Cor = _Cor;
            }

            public string Titulo;
            public Color Cor;
            private PanelControl panelControl1;
            private LabelControl labelControl1;

            internal Control Popula(int i,Point Local,Size Tamanho)
            {
                panelControl1 = new DevExpress.XtraEditors.PanelControl();
                panelControl1.Appearance.BackColor = Cor;
                panelControl1.Appearance.Options.UseBackColor = true;
                panelControl1.Location = Local;              
                panelControl1.Name = string.Format("PMenu_{0}", i);
                panelControl1.Size = Tamanho;                
                labelControl1 = new DevExpress.XtraEditors.LabelControl();
                labelControl1.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
                labelControl1.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
                labelControl1.Appearance.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
                labelControl1.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
                labelControl1.Dock = System.Windows.Forms.DockStyle.Fill;
                //labelControl1.Location = new System.Drawing.Point(2, 2);
                labelControl1.Name = string.Format("TMenu_{0}", i); ;
                //labelControl1.Size = new System.Drawing.Size(79, 20);
                labelControl1.Text = Titulo;
                panelControl1.Controls.Add(labelControl1);
                return panelControl1;
            }
        }

        /// <summary>
        /// Construtor
        /// </summary>
        public Legenda()
        {
            InitializeComponent();
            Unidades = new List<Unid>();
            if (VirEnum1 != null)
                VirEnum1.GeraLegenda(this);
        }

        /// <summary>
        /// Enumeração
        /// </summary>
        public VirEnum VirEnum1;

        private int margem;
        private bool chekBox;

        #region Propriedades expostas
        /// <summary>
        /// Contem ou não o checkbox
        /// </summary>
        [Category("Virtual Software"), Description("Contem ou não o checkbox")]
        [DefaultValue(false)]
        public bool ChekBox
        {
            get { return chekBox; }
            set { chekBox = value; }
        }

        /// <summary>
        /// Margem
        /// </summary>
        [Category("Virtual Software"), Description("Margem")]
        [DefaultValue(typeof(int), "3")]
        public int Margem
        {
            get { return margem; }
            set { margem = value; }
        } 
        #endregion

        private List<Unid> Unidades;

        /// <summary>
        /// Registra uma unidade
        /// </summary>
        /// <param name="Titulo"></param>
        /// <param name="Cor"></param>
        public void Registra(string Titulo,Color Cor)
        {
            Unidades.Add(new Unid(Titulo, Cor));
        }

        /// <summary>
        /// Cria o menu
        /// </summary>
        public void Popula()
        {
            int X = Margem;
            int i = 0;
            int W = (Width - Margem * 2 * Unidades.Count) / Unidades.Count;
            foreach (Unid Uni in Unidades)
            {
                Controls.Add(Uni.Popula(i, new Point(X, Margem), new Size(W, Height - 2 * Margem)));
                X += 2 * Margem + W;                
                i++;
            }            
        }
    }
}
