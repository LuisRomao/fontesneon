﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace TesteHTTPServer
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private VirHTTPServer.VirHTTPServer HTTPServer;

        private bool Chamada(SortedList<string, string> Args, SortedList<string, string> PostArg, out string Conteudo, out string Arquivo)
        {
            Arquivo = Conteudo = null;
            if (Args != null)
            {
                if (Args.ContainsKey("C"))
                {
                    Conteudo = "Teste com acentos ção (conteúdo)";
                    return true;
                }
                else 
                {
                    Arquivo = "D:\\1\\tela.JPG";
                    return true;
                }
            }
            else
            {
                return false;
            }
            //return VirHTTPServer.VirHTTPServer.GeraPaginaSimples("Teste com acentos ção");                    
        }

        private void button1_Click(object sender, EventArgs e)
        {
            
            HTTPServer = new VirHTTPServer.VirHTTPServer("",8080,Chamada,false);
            HTTPServer.Start();
        }

        private void Form1_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (HTTPServer != null)
                HTTPServer.Stop();                        
        }

        private void button2_Click(object sender, EventArgs e)
        {
            Console.WriteLine(VirHTTPServer.VirHTTPServer.Endereco());
        }
    }
}
