using System;
using System.Collections;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Windows.Forms;

namespace RtfEditor
{
	/// <summary>
	/// Summary description for RtfEditor.
	/// </summary>
	//public class RtfEditor : System.Windows.Forms.UserControl
    public class RtfEditor : CompontesBasicos.ComponenteBase
	{
        /// <summary>
        /// Conte�do RTF
        /// </summary>
        [Category("Virtual Software"), Description("Conte�do RTF")]
        [DefaultValue(typeof(string), "")]
        [DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
        public string Rtf {
            get {
                return m_textBox.Rtf;
            }
            set {
                m_textBox.Rtf = value;
            }
        }

		private System.Windows.Forms.GroupBox groupBox1;
		private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.RichTextBox m_textBox;
		private System.Windows.Forms.CheckBox m_bold;
		private System.Windows.Forms.CheckBox m_italic;
		private System.Windows.Forms.CheckBox m_under;
		private System.Windows.Forms.CheckBox m_left;
		private System.Windows.Forms.CheckBox m_center;
		private System.Windows.Forms.CheckBox m_right;
        private System.Windows.Forms.GroupBox groupBox3;
		private System.Windows.Forms.FontDialog fontDialog1;
		private System.ComponentModel.IContainer components;		
        private DevExpress.XtraEditors.ComboBoxEdit m_fontSize2;
        private DevExpress.XtraEditors.ColorEdit colorEditFore;
        private DevExpress.XtraEditors.FontEdit fontEdit1;
        private DevExpress.XtraEditors.SimpleButton simpleButton1;

		private System.Windows.Forms.ToolTip m_toolTip;

        /// <summary>
        /// 
        /// </summary>
		public RtfEditor()
		{
			// This call is required by the Windows.Forms Form Designer.
			InitializeComponent();			      	
			m_textBox.SelectionChanged += new EventHandler( OnTxtSelectionChanged);
            Bloqueio = false;
		}


		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose( bool disposing )
		{
			if( disposing )
			{
				if( components != null )
					components.Dispose();
			}
			base.Dispose( disposing );
		}

		#region Component Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify 
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
            this.components = new System.ComponentModel.Container();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling2 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(RtfEditor));
            this.m_textBox = new System.Windows.Forms.RichTextBox();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.m_bold = new System.Windows.Forms.CheckBox();
            this.m_italic = new System.Windows.Forms.CheckBox();
            this.m_under = new System.Windows.Forms.CheckBox();
            this.m_left = new System.Windows.Forms.CheckBox();
            this.m_center = new System.Windows.Forms.CheckBox();
            this.m_right = new System.Windows.Forms.CheckBox();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.fontDialog1 = new System.Windows.Forms.FontDialog();
            this.m_toolTip = new System.Windows.Forms.ToolTip(this.components);
            this.m_fontSize2 = new DevExpress.XtraEditors.ComboBoxEdit();
            this.colorEditFore = new DevExpress.XtraEditors.ColorEdit();
            this.fontEdit1 = new DevExpress.XtraEditors.FontEdit();
            this.simpleButton1 = new DevExpress.XtraEditors.SimpleButton();
            ((System.ComponentModel.ISupportInitialize)(this.StyleController_F)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.m_fontSize2.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.colorEditFore.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.fontEdit1.Properties)).BeginInit();
            this.SuspendLayout();
            // 
            // ToolTipController_F
            // 
            this.ToolTipController_F.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.ToolTipController_F.Appearance.ForeColor = System.Drawing.Color.Black;
            this.ToolTipController_F.Appearance.Options.UseBackColor = true;
            this.ToolTipController_F.Appearance.Options.UseFont = true;
            this.ToolTipController_F.Appearance.Options.UseForeColor = true;
            this.ToolTipController_F.Appearance.Options.UseTextOptions = true;
            this.ToolTipController_F.Appearance.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            // 
            // StyleController_F
            // 
            this.StyleController_F.AppearanceDisabled.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this.StyleController_F.AppearanceDisabled.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this.StyleController_F.AppearanceDisabled.Options.UseBackColor = true;
            this.StyleController_F.AppearanceDisabled.Options.UseForeColor = true;
            this.StyleController_F.AppearanceFocused.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.StyleController_F.AppearanceFocused.Options.UseBackColor = true;
          
            // 
            // m_textBox
            // 
            this.m_textBox.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.m_textBox.Font = new System.Drawing.Font("Verdana", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.m_textBox.Location = new System.Drawing.Point(0, 24);
            this.m_textBox.Name = "m_textBox";
            this.m_textBox.Size = new System.Drawing.Size(628, 243);
            
            this.m_textBox.TabIndex = 0;
            this.m_textBox.Text = "";
            // 
            // groupBox1
            // 
            this.groupBox1.Location = new System.Drawing.Point(160, -8);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(8, 32);
            this.groupBox1.TabIndex = 6;
            this.groupBox1.TabStop = false;
            // 
            // groupBox2
            // 
            this.groupBox2.Location = new System.Drawing.Point(240, -8);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(8, 32);
            this.groupBox2.TabIndex = 7;
            this.groupBox2.TabStop = false;
            // 
            // m_bold
            // 
            this.m_bold.Appearance = System.Windows.Forms.Appearance.Button;
            this.m_bold.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.m_bold.Image = ((System.Drawing.Image)(resources.GetObject("m_bold.Image")));
            this.m_bold.Location = new System.Drawing.Point(168, 0);
            this.m_bold.Name = "m_bold";
            this.m_bold.Size = new System.Drawing.Size(21, 21);
            this.m_bold.TabIndex = 12;
            this.m_bold.CheckedChanged += new System.EventHandler(this.m_bold_CheckedChanged);
            // 
            // m_italic
            // 
            this.m_italic.Appearance = System.Windows.Forms.Appearance.Button;
            this.m_italic.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.m_italic.Image = ((System.Drawing.Image)(resources.GetObject("m_italic.Image")));
            this.m_italic.Location = new System.Drawing.Point(192, 0);
            this.m_italic.Name = "m_italic";
            this.m_italic.Size = new System.Drawing.Size(21, 21);
            this.m_italic.TabIndex = 13;
            this.m_italic.CheckedChanged += new System.EventHandler(this.m_italic_CheckedChanged);
            // 
            // m_under
            // 
            this.m_under.Appearance = System.Windows.Forms.Appearance.Button;
            this.m_under.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.m_under.Image = ((System.Drawing.Image)(resources.GetObject("m_under.Image")));
            this.m_under.Location = new System.Drawing.Point(216, 0);
            this.m_under.Name = "m_under";
            this.m_under.Size = new System.Drawing.Size(21, 21);
            this.m_under.TabIndex = 14;
            this.m_under.CheckedChanged += new System.EventHandler(this.m_under_CheckedChanged);
            // 
            // m_left
            // 
            this.m_left.Appearance = System.Windows.Forms.Appearance.Button;
            this.m_left.Checked = true;
            this.m_left.CheckState = System.Windows.Forms.CheckState.Checked;
            this.m_left.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.m_left.Image = ((System.Drawing.Image)(resources.GetObject("m_left.Image")));
            this.m_left.Location = new System.Drawing.Point(248, 0);
            this.m_left.Name = "m_left";
            this.m_left.Size = new System.Drawing.Size(21, 21);
            this.m_left.TabIndex = 15;
            this.m_left.CheckedChanged += new System.EventHandler(this.m_left_CheckedChanged);
            // 
            // m_center
            // 
            this.m_center.Appearance = System.Windows.Forms.Appearance.Button;
            this.m_center.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.m_center.Image = ((System.Drawing.Image)(resources.GetObject("m_center.Image")));
            this.m_center.Location = new System.Drawing.Point(272, 0);
            this.m_center.Name = "m_center";
            this.m_center.Size = new System.Drawing.Size(21, 21);
            this.m_center.TabIndex = 16;
            this.m_center.CheckedChanged += new System.EventHandler(this.m_center_CheckedChanged);
            // 
            // m_right
            // 
            this.m_right.Appearance = System.Windows.Forms.Appearance.Button;
            this.m_right.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.m_right.Image = ((System.Drawing.Image)(resources.GetObject("m_right.Image")));
            this.m_right.Location = new System.Drawing.Point(296, 0);
            this.m_right.Name = "m_right";
            this.m_right.Size = new System.Drawing.Size(21, 21);
            this.m_right.TabIndex = 17;
            this.m_right.CheckedChanged += new System.EventHandler(this.m_right_CheckedChanged);
            // 
            // groupBox3
            // 
            this.groupBox3.Location = new System.Drawing.Point(320, -8);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(8, 32);
            this.groupBox3.TabIndex = 19;
            this.groupBox3.TabStop = false;
            // 
            // m_fontSize2
            // 
            this.m_fontSize2.Location = new System.Drawing.Point(122, 0);
            this.m_fontSize2.Name = "m_fontSize2";
            this.m_fontSize2.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.m_fontSize2.Properties.Items.AddRange(new object[] {
            "6",
            "8",
            "10",
            "12",
            "14",
            "16",
            "18",
            "20",
            "22"});
            this.m_fontSize2.Size = new System.Drawing.Size(35, 20);
            this.m_fontSize2.TabIndex = 22;
            this.m_fontSize2.EditValueChanged += new System.EventHandler(this.m_fontSize2_EditValueChanged);
            // 
            // colorEditFore
            // 
            this.colorEditFore.EditValue = System.Drawing.Color.Black;
            this.colorEditFore.Location = new System.Drawing.Point(331, 1);
            this.colorEditFore.Name = "colorEditFore";
            this.colorEditFore.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.colorEditFore.Size = new System.Drawing.Size(82, 20);
            this.colorEditFore.TabIndex = 23;
            this.colorEditFore.ColorChanged += new System.EventHandler(this.colorEditFore_ColorChanged);
            // 
            // fontEdit1
            // 
            this.fontEdit1.Location = new System.Drawing.Point(0, 0);
            this.fontEdit1.Name = "fontEdit1";
            this.fontEdit1.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.fontEdit1.Properties.Appearance.Options.UseFont = true;
            this.fontEdit1.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.fontEdit1.Properties.EditValueChanged += new System.EventHandler(this.fontEdit1_Properties_EditValueChanged);
            this.fontEdit1.Size = new System.Drawing.Size(118, 20);
            this.fontEdit1.TabIndex = 24;
            // 
            // simpleButton1
            // 
            this.simpleButton1.Location = new System.Drawing.Point(419, 0);
            this.simpleButton1.Name = "simpleButton1";
            this.simpleButton1.Size = new System.Drawing.Size(93, 21);
            this.simpleButton1.TabIndex = 25;
            this.simpleButton1.Text = "Corretor";
            this.simpleButton1.Click += new System.EventHandler(this.simpleButton1_Click);
            // 
            // RtfEditor
            // 
            this.Controls.Add(this.simpleButton1);
            this.Controls.Add(this.fontEdit1);
            this.Controls.Add(this.m_fontSize2);
            this.Controls.Add(this.colorEditFore);
            this.Controls.Add(this.groupBox3);
            this.Controls.Add(this.m_right);
            this.Controls.Add(this.m_center);
            this.Controls.Add(this.m_left);
            this.Controls.Add(this.m_under);
            this.Controls.Add(this.m_italic);
            this.Controls.Add(this.m_bold);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.m_textBox);
            this.LookAndFeel.UseDefaultLookAndFeel = false;
            this.Name = "RtfEditor";
            this.Size = new System.Drawing.Size(631, 270);
            ((System.ComponentModel.ISupportInitialize)(this.StyleController_F)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.m_fontSize2.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.colorEditFore.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.fontEdit1.Properties)).EndInit();
            this.ResumeLayout(false);

		}
		#endregion

		#region Handle Control Events

        private bool Bloqueio = true;

        private void ChangeGeral() {
            if (!Bloqueio)
            {
                try
                {
                    FontStyle style = FontStyle.Regular;

                    if (m_bold.Checked)
                        style |= FontStyle.Bold;
                    if (m_italic.Checked)
                        style |= FontStyle.Italic;
                    if (m_under.Checked)
                        style |= FontStyle.Underline;

                    float size = float.Parse(m_fontSize2.EditValue.ToString());

                    m_textBox.SelectionFont = new Font(
                        fontEdit1.EditValue.ToString(),
                        size,
                        style);
                    m_textBox.SelectionColor = colorEditFore.Color;
                    
                }
                catch { }
                finally {
                    m_textBox.Focus();
                }
            }
        }


		private void m_fontName_SelectedIndexChanged(object sender, System.EventArgs e)
		{
            ChangeGeral();			
		}

		

		private void m_bold_CheckedChanged(object sender, System.EventArgs e)
		{
            ChangeGeral();			
		}

		private void m_italic_CheckedChanged(object sender, System.EventArgs e)
		{
            ChangeGeral();			
		}

		private void m_under_CheckedChanged(object sender, System.EventArgs e)
		{
            ChangeGeral();			
		}

		private void m_left_CheckedChanged(object sender, System.EventArgs e)
		{
			if( m_left.Checked)
			{
				m_textBox.SelectionAlignment = HorizontalAlignment.Left;

				m_center.Checked = false;
				m_right.Checked = false;
			}
			m_textBox.Focus();
		}

		private void m_center_CheckedChanged(object sender, System.EventArgs e)
		{
			if( m_center.Checked)
			{
				m_textBox.SelectionAlignment = HorizontalAlignment.Center;

				m_left.Checked = false;
				m_right.Checked = false;
			}
			m_textBox.Focus();
		}

		private void m_right_CheckedChanged(object sender, System.EventArgs e)
		{
			if( m_right.Checked)
			{
				m_textBox.SelectionAlignment = HorizontalAlignment.Right;

				m_center.Checked = false;
				m_left.Checked = false;
			}
			m_textBox.Focus();
		}

		
		
		#endregion


        /// <summary>
        /// Evento altera��o de sele��o. Ajusta os componetes para as caracter�sticas da sele��o
        /// </summary>
        /// <param name="sender">Chamador</param>
        /// <param name="e">EventArgs</param>
		private void OnTxtSelectionChanged( object sender, EventArgs e)
		{

            
            try
            {
                Bloqueio = true;
                if (m_textBox.SelectionAlignment == HorizontalAlignment.Center)
                {
                    m_center.Checked = true;
                }
                if (m_textBox.SelectionAlignment == HorizontalAlignment.Left)
                {
                    m_left.Checked = true;
                }
                if (m_textBox.SelectionAlignment == HorizontalAlignment.Right)
                {
                    m_right.Checked = true;
                }
                m_fontSize2.EditValue = m_textBox.SelectionFont.Size;
                if (m_textBox.SelectionColor != Color.Empty)
                    colorEditFore.Color = m_textBox.SelectionColor;
                fontEdit1.EditValue = m_textBox.SelectionFont.FontFamily.Name;
                if (m_textBox.SelectionFont.Bold)
                    m_bold.Checked = true;
                else
                    m_bold.Checked = false;

                if (m_textBox.SelectionFont.Italic)
                    m_italic.Checked = true;
                else
                    m_italic.Checked = false;

                if (m_textBox.SelectionFont.Underline)
                    m_under.Checked = true;
                else
                    m_under.Checked = false;
            }
            catch {
                
            }
            finally
            {
                Bloqueio = false;
            };

			           

			
			
		}

        private void m_fontSize2_EditValueChanged(object sender, EventArgs e)
        {
            ChangeGeral();
        }

        private void colorEditFore_ColorChanged(object sender, EventArgs e)
        {
            ChangeGeral();
            
        }

        private void fontEdit1_Properties_EditValueChanged(object sender, EventArgs e)
        {
            ChangeGeral();
        }

        private void simpleButton1_Click(object sender, EventArgs e)
        {
            if (CompontesBasicos.virSpell.StstspellChecker != null)
                CompontesBasicos.virSpell.StstspellChecker.Check(m_textBox);
        }
	}
}
