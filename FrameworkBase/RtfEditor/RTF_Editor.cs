﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using CompontesBasicos;

namespace RtfEditor
{
    /// <summary>
    /// Editor RTF
    /// </summary>
    public partial class RTF_Editor : ComponenteBase
    {
        /// <summary>
        /// Construtor
        /// </summary>
        public RTF_Editor()
        {
            InitializeComponent();           
            ribbonControl1.SelectedPage = homeRibbonPage1;
            somenteleituraChange += new EventHandler(RTF_Editor_somenteleituraChange);
        }

        private void RTF_Editor_somenteleituraChange(object sender, EventArgs e)
        {           
            richEditControl1.Enabled = !somenteleitura;
        }

        private bool? _SpellAutomatico;

        /// <summary>
        /// Habilita ou não o spellChecker1
        /// </summary>
        [Category("Virtual Software"), Description("Conteúdo RTF")]
        [DefaultValue(false)]        
        public bool SpellAutomatico
        {
            get 
            {
                return richEditControl1.SpellChecker != null;
            }
            set 
            {
                _SpellAutomatico = value;
                if (value)
                    richEditControl1.SpellChecker = virSpell.StstspellChecker;
                else
                    richEditControl1.SpellChecker = null;
            }
        }

        /// <summary>
        /// RTFstring
        /// </summary>
        public string RTFstring
        {
            get 
            {
                return richEditControl1.RtfText;
            }
            set
            {
                if(!_SpellAutomatico.HasValue)
                    richEditControl1.SpellChecker = null;
                if (value == null)
                {
                    richEditControl1.ResetText();
                    richEditControl1.Document.DefaultCharacterProperties.FontName = "Times New Roman";
                    richEditControl1.Document.DefaultCharacterProperties.Bold = true;
                    richEditControl1.Document.DefaultCharacterProperties.FontSize = 20;
                }
                else
                {
                    if (richEditControl1.RtfText != value)
                    {
                        richEditControl1.ResetText();
                        richEditControl1.RtfText = value;
                    }
                }
            }
        }

        private void richEditControl1_UnhandledException(object sender, DevExpress.XtraRichEdit.RichEditUnhandledExceptionEventArgs e)
        {
            //System.IO.File.AppendAllText("d:\\log.txt", string.Format("Erro: {0}\r\n{1}\r\n", e.Exception.Message,e.Exception.StackTrace));
            Exception E2 = e.Exception.InnerException;
            while (E2 != null)
            {
                //System.IO.File.AppendAllText("d:\\log.txt", string.Format("Erro: {0}\r\n{1}\r\n", E2.Message, E2.StackTrace));
                E2 = E2.InnerException;
            }
            e.Handled = true;
            //richEditControl1.ResetText();
            //System.IO.File.AppendAllText("d:\\log.txt", string.Format("Reset\r\n"));
            // Add your code to handle the exception. 
//            BeginInvoke(new MethodInvoker(delegate() {
//                                                        MessageBox.Show(e.Exception.Message, "The command is not completed!");
//                                                     }
//                                         )
//                       );
        }

        
    }
}
